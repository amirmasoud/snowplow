---
title: >-
    بخش ۳
---
# بخش ۳

<div class="b" id="bn1"><div class="m1"><p>سکندر چو بشنید کامد سپاه</p></div>
<div class="m2"><p>پذیره شدن را بپیمود راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان دو لشکر دو فرسنگ ماند</p></div>
<div class="m2"><p>سکندر گرانمایگان را بخواند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو سیر آمد از گفتهٔ رهنمای</p></div>
<div class="m2"><p>چنین گفت کاکنون جزین نیست رای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که من چون فرستاده‌ای پیش اوی</p></div>
<div class="m2"><p>شوم برگرایم کم و بیش اوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمر خواست پرگوهر شاهوار</p></div>
<div class="m2"><p>یکی خسروی جامهٔ زرنگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببردند بالای زرین ستام</p></div>
<div class="m2"><p>به زین اندرون تیغ زرین نیام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سواری ده از رومیان برگزید</p></div>
<div class="m2"><p>که دانند هرگونه گفت و شنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز لشکر بیامد سپیده دمان</p></div>
<div class="m2"><p>خود و نامداران ابا ترجمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو آمد به نزدیک دارا فراز</p></div>
<div class="m2"><p>پیاده شد و برد پیشش نماز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهاندار دارا مر او را بخواند</p></div>
<div class="m2"><p>بپرسید و بر زیر گاهش نشاند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه نامداران فروماندند</p></div>
<div class="m2"><p>بروبر نهان آفرین خواندند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز دیدار آن فر و فرهنگ او</p></div>
<div class="m2"><p>ز بالا و از شاخ و آهنگ او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همانگه چو بنشست بر پای خاست</p></div>
<div class="m2"><p>پیام سکندر بیاراست راست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نخست آفرین کرد بر شهریار</p></div>
<div class="m2"><p>که جاوید بادا سر تاج‌دار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سکندر چنین گفت کای نیک‌نام</p></div>
<div class="m2"><p>به گیتی بهرجای گسترده کام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا آرزو نیست با شاه جنگ</p></div>
<div class="m2"><p>نه بر بوم ایران گرفتن درنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برآنم که گرد زمین اندکی</p></div>
<div class="m2"><p>بگردم ببینم جهان را یکی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه راستی خواهم و نیکویی</p></div>
<div class="m2"><p>به ویژه که سالار ایران تویی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر خاک داری تو از من دریغ</p></div>
<div class="m2"><p>نشاید سپردن هوا را چو میغ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنین با سپاه آمدی پیش من</p></div>
<div class="m2"><p>نه آگاهی از رای کم بیش من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو رزم آوری باتو رزم آورم</p></div>
<div class="m2"><p>ازین بوم بی‌رزم برنگذرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گزین کن یکی روزگار نبرد</p></div>
<div class="m2"><p>برین باش و زین آرزو برمگرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که من سر نپیچم ز جنگ سران</p></div>
<div class="m2"><p>وگر چند باشد سپاهی گران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو دارا بدید آن دل و رای او</p></div>
<div class="m2"><p>سخن گفتن و فر و بالای او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو گفتی که داراست بر تخت عاج</p></div>
<div class="m2"><p>ابا یاره و طوق و با فر و تاج</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدو گفت نام و نژاد تو چیست</p></div>
<div class="m2"><p>که بر فر و شاخت نشان کییست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از اندازهٔ کهتران برتری</p></div>
<div class="m2"><p>من ایدون گمانم که اسکندری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدین فر و بالا و گفتار و چهر</p></div>
<div class="m2"><p>مگر تخت را پروریدت سپهر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنین داد پاسخ که این کس نکرد</p></div>
<div class="m2"><p>نه در آشتی و نه اندر نبرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه گویندگان بر درش کمترند</p></div>
<div class="m2"><p>که بر تارک بخردان افسرند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کجا خود پیام آرد از خویشتن</p></div>
<div class="m2"><p>چنان شهریاری سر انجمن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سکندر بدان مایه دارد خرد</p></div>
<div class="m2"><p>که از رای پیشینگان بگذرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پیامم سپهبد بدین گونه داد</p></div>
<div class="m2"><p>بگفتم به شاه آنچ او کرد یاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیاراستندش یکی جایگاه</p></div>
<div class="m2"><p>چنانچون بود درخور پایگاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سپهدار ایران چو بنهاد خوان</p></div>
<div class="m2"><p>به سالار فرمود کو را بخوان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو نان خورده شد مجلس آراستند</p></div>
<div class="m2"><p>می و رود و رامشگران خواستند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سکندر چو خوردی می خوشگوار</p></div>
<div class="m2"><p>نهادی سبک جام را بر کنار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنین تا می و جام چندی بگشت</p></div>
<div class="m2"><p>نهادن ز اندازه اندر گذشت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دهنده بیامد به دارا بگفت</p></div>
<div class="m2"><p>که رومی شد امروز با جام جفت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بفرمود تا زو بپرسند شاه</p></div>
<div class="m2"><p>که جام نبید از چه داری نگاه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدو گفت ساقی که ای شیر فش</p></div>
<div class="m2"><p>چه داری همی جام زرین به کش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سکندر چنین داد پاسخ که جام</p></div>
<div class="m2"><p>فرستاده را باشد ای نیک‌نام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر آیین ایران جز اینست راه</p></div>
<div class="m2"><p>ببر جام زرین سوی گنج شاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بخندید از آیین او شهریار</p></div>
<div class="m2"><p>یکی جام پرگوهر شاهوار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بفرمود تا بر کفش برنهند</p></div>
<div class="m2"><p>یکی سرخ یاقوت بر سر نهند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هم‌اندر زمان باژ خواهان روم</p></div>
<div class="m2"><p>کجا رفته بودند زان مرز و بوم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز خانه بدان بزمگاه آمدند</p></div>
<div class="m2"><p>خرامان به نزدیک شاه آمدند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فرستاده روی سکندر بدید</p></div>
<div class="m2"><p>بر شاه رفت آفرین گسترید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بدو گفت کاین مهتر اسکندرست</p></div>
<div class="m2"><p>که بر تخت با گرز و با افسرست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدانگه که ما را بفرمود شاه</p></div>
<div class="m2"><p>برفتیم نزدیک او باژخواه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>برآشفت و ما را بدان خوار کرد</p></div>
<div class="m2"><p>به گفتار با شاه پیکار کرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو از پادشاهیش بگریختم</p></div>
<div class="m2"><p>شب تیره اسپان برانگیختم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ندیدیم مانندهٔ او به روم</p></div>
<div class="m2"><p>دلیر آمدست اندرین مرز و بوم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همی برگراید سپاه ترا</p></div>
<div class="m2"><p>همان گنج و تخت و کلاه ترا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو گفت فرستاده بشنید شاه</p></div>
<div class="m2"><p>فزون کرد سوی سکندر نگاه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سکندر بدانست کاندر نهان</p></div>
<div class="m2"><p>چه گفتند با شهریار جهان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همی بود تا تیره‌تر گشت روز</p></div>
<div class="m2"><p>سوی باختر گشت گیتی‌فروز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بیامد به دهلیز پرده‌سرای</p></div>
<div class="m2"><p>دلاور به اسپ اندر آورد پای</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چنین گفت پس با سواران خویش</p></div>
<div class="m2"><p>بلنداختر و نامداران خویش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که ما را کنون جان به اسپ اندرست</p></div>
<div class="m2"><p>چو سستی کند باد ماند به دست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>همه بادپایان برانگیختند</p></div>
<div class="m2"><p>ز پیش جهاندار بگریختند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو دارا سر و افسر او ندید</p></div>
<div class="m2"><p>به تاریکی از چشم شد ناپدید</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نگهبان فرستاد هم در زمان</p></div>
<div class="m2"><p>به نزدیکی خیمهٔ بدگمان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو رفتند بیداردل رفته بود</p></div>
<div class="m2"><p>نه بخت چنان پادشا خفته بود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پس او فرستاد دارا سوار</p></div>
<div class="m2"><p>دلیران و پرخاشجویان هزار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو باد از پس او همی تاختند</p></div>
<div class="m2"><p>شب تیرهٔ بد راه نشناختند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>طلایه بدیدند گشتند باز</p></div>
<div class="m2"><p>نبد سود جز رنج و راه دراز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو اسکندر آمد به پرده‌سرای</p></div>
<div class="m2"><p>برفتند گردان رومی ز جای</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بدیدند شب شاه را شادکام</p></div>
<div class="m2"><p>به پیش اندرون پرگهر چار جام</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به گردان چنین گفت کاباد بید</p></div>
<div class="m2"><p>بدین فرخی فال ما شاد بید</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>که این جام پیروزی جان ماست</p></div>
<div class="m2"><p>سر اختران زیر فرمان ماست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هم از لشکرش برگرفتم شمار</p></div>
<div class="m2"><p>فراوان کم است از شنیده سوار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>همه جنگ را تیغها برکشید</p></div>
<div class="m2"><p>وزین دشت هامون سر اندرکشید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو در جنگ تن را به رنج آورید</p></div>
<div class="m2"><p>ازان رنج شاهی و گنج آورید</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>جهان آفریننده یار منست</p></div>
<div class="m2"><p>سر اختر اندر کنار منست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بزرگان برو خواندند آفرین</p></div>
<div class="m2"><p>که آباد بادا به قیصر زمین</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>فدای تو بادا تن و جان ما</p></div>
<div class="m2"><p>برینست جاوید پیمان ما</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز شاهان که یارد بدن یار تو</p></div>
<div class="m2"><p>به مردی و بالا و دیدار تو</p></div></div>