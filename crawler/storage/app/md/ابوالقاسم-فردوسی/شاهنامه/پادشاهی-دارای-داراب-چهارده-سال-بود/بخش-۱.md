---
title: >-
    بخش ۱
---
# بخش ۱

<div class="b" id="bn1"><div class="m1"><p>چو دارا به دل سوک داراب داشت</p></div>
<div class="m2"><p>به خورشید تاج مهی برفراشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی مرد بر تیز و برنا و تند</p></div>
<div class="m2"><p>شده با زبان و دلش تیغ کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بنشست برگاه گفت ای سران</p></div>
<div class="m2"><p>سرافراز گردان و کنداوران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سری را نخواهم که افتد به چاه</p></div>
<div class="m2"><p>نه از چاه خوانم سوی تخت و گاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی کو ز فرمان من بگذرد</p></div>
<div class="m2"><p>سرش را همی تن به سر نشمرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وگر هیچ تاب اندر آرد به دل</p></div>
<div class="m2"><p>به شمشیر باشم ورا دلگسل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز از ما هرانکس که دارند گنج</p></div>
<div class="m2"><p>نخواهم کس شاددل ما به رنج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نخواهم که باشد مرا رهنمای</p></div>
<div class="m2"><p>منم رهنمای و منم دلگشای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گیتی خور و بخش و پیمان مراست</p></div>
<div class="m2"><p>بزرگی و شاهی و فرمان مراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دبیر خردمند را پیش خواند</p></div>
<div class="m2"><p>ز هر در فراوان سخنها براند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی نامه بنوشت فرخ دبیر</p></div>
<div class="m2"><p>ز دارای داراب بن اردشیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهر سو که بد شاه و خودکامه‌ای</p></div>
<div class="m2"><p>بفرمود چون خنجری نامه‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که هرکو ز رای و ز فرمان من</p></div>
<div class="m2"><p>بپیچد ببیند سرافشان من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه گوش یکسر به فرمان نهید</p></div>
<div class="m2"><p>اگر جان ستانید اگر جان دهید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سر گنجهای پدر برگشاد</p></div>
<div class="m2"><p>سپه را همه خواند و روزی بداد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز چار اندرآمد درم تا بهشت</p></div>
<div class="m2"><p>یکی را بجام و یکی را به تشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>درم داد و دینار و برگستوان</p></div>
<div class="m2"><p>همان جوشن و تیغ و گرز گران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرانکس که بد کار دیده سری</p></div>
<div class="m2"><p>ببخشید بر هر سری کشوری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی را ز گردنکشان مرز داد</p></div>
<div class="m2"><p>سپه را همه چیز باارز داد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرستاده آمد ز هر کشوری</p></div>
<div class="m2"><p>ز هر نامداری و هر مهتری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز هند و ز خاقان و فغفور چین</p></div>
<div class="m2"><p>ز روم و ز هر کشوری همچنین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه پاک با هدیه و باژ و ساو</p></div>
<div class="m2"><p>نه پی بود با او کسی را نه تاو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی شارستان کرد نوشاد نام</p></div>
<div class="m2"><p>به اهواز گشتند زو شادکام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کسی را که درویش بد داد داد</p></div>
<div class="m2"><p>به خواهندگان گنج و بنیاد داد</p></div></div>