---
title: >-
    بخش ۸
---
# بخش ۸

<div class="b" id="bn1"><div class="m1"><p>چو شد روزگار تهمتن به سر</p></div>
<div class="m2"><p>به پیش آورم داستانی دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گشتاسپ را تیره شد روی بخت</p></div>
<div class="m2"><p>بیاورد جاماسپ را پیش تخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفت کز کار اسفندیار</p></div>
<div class="m2"><p>چنان داغ دل گشتم و سوکوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که روزی نبد زندگانیم خوش</p></div>
<div class="m2"><p>دژم بودم از اختر کینه‌کش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس از من کنون شاه بهمن بود</p></div>
<div class="m2"><p>همان رازدارش پشوتن بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مپیچید سرها ز فرمان اوی</p></div>
<div class="m2"><p>مگیرید دوری ز پیمان اوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکایک بویدش نماینده راه</p></div>
<div class="m2"><p>که اویست زیبای تخت و کلاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدو داد پس گنجها را کلید</p></div>
<div class="m2"><p>یکی باد سرد از جگر برکشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدو گفت کار من اندر گذشت</p></div>
<div class="m2"><p>هم از تارکم آب برتر گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشستم به شاهی صد و بیست سال</p></div>
<div class="m2"><p>ندیدم به گیتی کسی را همال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو اکنون همی کوش و با داد باش</p></div>
<div class="m2"><p>چو داد آوری از غم آزاد باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خردمند را شاد و نزدیک دار</p></div>
<div class="m2"><p>جهان بر بداندیش تاریک دار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه راستی کن که از راستی</p></div>
<div class="m2"><p>بپیچد سر از کژی و کاستی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپردم ترا تخت و دیهیم و گنج</p></div>
<div class="m2"><p>ازان سپ که بردم بسی گرم و رنج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بفگت این و شد روزگارش به سر</p></div>
<div class="m2"><p>زمان گذشته نیامد به بر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی دخمه کردندش از شیز و عاج</p></div>
<div class="m2"><p>برآویختند از بر گاه تاج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همین بودش از رنج و ز گنج بهر</p></div>
<div class="m2"><p>بدید از پس نوش و تریاک زهر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر بودن اینست شادی چراست</p></div>
<div class="m2"><p>شد از مرگ درویش با شاه راست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بخور هرچ برزی و بد را مکوش</p></div>
<div class="m2"><p>به مرد خردمند بسپار گوش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گذر کرد همراه و ما ماندیم</p></div>
<div class="m2"><p>ز کار گذشته بسی خواندیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به منزل رسید آنک پوینده بود</p></div>
<div class="m2"><p>رهی یافت آن کس که جوینده بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نگیرد ترا دست جز نیکوی</p></div>
<div class="m2"><p>گر از پیر دانا سخن بشنوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنون رنج در کار بهمن بریم</p></div>
<div class="m2"><p>خرد پیش دانا پشوتن بریم</p></div></div>