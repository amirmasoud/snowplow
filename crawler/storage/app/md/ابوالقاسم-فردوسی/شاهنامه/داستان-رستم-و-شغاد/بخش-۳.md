---
title: >-
    بخش ۳
---
# بخش ۳

<div class="b" id="bn1"><div class="m1"><p>بداختر چو از شهر کابل برفت</p></div>
<div class="m2"><p>بدان دشت نخچیر شد شاه تفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببرد از میان لشکری چاه‌کن</p></div>
<div class="m2"><p>کجا نام بردند زان انجمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سراسر همه دشت نخچیرگاه</p></div>
<div class="m2"><p>همه چاه بد کنده در زیر راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زده حربه‌ها را بن اندر زمین</p></div>
<div class="m2"><p>همان نیز ژوپین و شمشیر کین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خاشاک کرده سر چاه کور</p></div>
<div class="m2"><p>که مردم ندیدی نه چشم ستور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو رستم دمان سر برفتن نهاد</p></div>
<div class="m2"><p>سواری برافگند پویان شغاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که آمد گو پیلتن با سپاه</p></div>
<div class="m2"><p>بیا پیش وزان کرده زنهار خواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپهدار کابل بیامد ز شهر</p></div>
<div class="m2"><p>زبان پرسخن دل پر از کین و زهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو چشمش به روی تهمتن رسید</p></div>
<div class="m2"><p>پیاده شد از باره کو را بدید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز سرشارهٔ هندوی برگرفت</p></div>
<div class="m2"><p>برهنه شد و دست بر سر گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همان موزه از پای بیرون کشید</p></div>
<div class="m2"><p>به زاری ز مژگان همی خون کشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو رخ را به خاک سیه بر نهاد</p></div>
<div class="m2"><p>همی کرد پوزش ز کار شغاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که گر مست شد بنده از بیهشی</p></div>
<div class="m2"><p>نمود اندران بیهشی سرکشی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سزد گر ببخشی گناه مرا</p></div>
<div class="m2"><p>کنی تازه آیین و راه مرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی رفت پیشش برهنه دو پای</p></div>
<div class="m2"><p>سری پر ز کینه دلی پر ز رای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ببخشید رستم گناه ورا</p></div>
<div class="m2"><p>بیفزود زان پایگاه ورا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بفرمود تا سر بپوشید و پای</p></div>
<div class="m2"><p>به زین بر نشست و بیامد ز جای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر شهر کابل یکی جای بود</p></div>
<div class="m2"><p>ز سبزی زمینش دلارای بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدو اندرون چشمه بود و درخت</p></div>
<div class="m2"><p>به شادی نهادند هرجای تخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسی خوردنیها بیاورد شاه</p></div>
<div class="m2"><p>بیاراست خرم یکی جشنگاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>می آورد و رامشگران را بخواند</p></div>
<div class="m2"><p>مهان را به تخت مهی بر نشاند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ازان سپ به رستم چنین گفت شاه</p></div>
<div class="m2"><p>که چون رایت آید به نخچیرگاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی جای دارم برین دشت و کوه</p></div>
<div class="m2"><p>به هر جای نخچیر گشته گروه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه دشت غرمست و آهو و گور</p></div>
<div class="m2"><p>کسی را که باشد تگاور ستور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به چنگ آیدش گور و آهو به دشت</p></div>
<div class="m2"><p>ازان دشت خرم نشاید گذشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز گفتار او رستم آمد به شور</p></div>
<div class="m2"><p>ازان دشت پرآب و نخچیرگور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به چیزی که آید کسی را زمان</p></div>
<div class="m2"><p>بپیچد دلش کور گردد گمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنین است کار جهان جهان</p></div>
<div class="m2"><p>نخواهد گشادن بمابر نهان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به دریا نهنگ و به هامون پلنگ</p></div>
<div class="m2"><p>همان شیر جنگاور تیزچنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ابا پشه و مور در چنگ مرگ</p></div>
<div class="m2"><p>یکی باشد ایدر بدن نیست برگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بفرمود تا رخش را زین کنند</p></div>
<div class="m2"><p>همه دشت پر باز و شاهین کنند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کمان کیانی به زه بر نهاد</p></div>
<div class="m2"><p>همی راند بر دشت او با شغاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زواره همی رفت با پیلتن</p></div>
<div class="m2"><p>تنی چند ازان نامدار انجمن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به نخچیر لشکر پراگنده شد</p></div>
<div class="m2"><p>اگر کنده گر سوی آگنده شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زواره تهمتن بران راه بود</p></div>
<div class="m2"><p>ز بهر زمان کاندران چاه بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همی رخش زان خاک می‌یافت بوی</p></div>
<div class="m2"><p>تن خویش را کرد چون گردگوی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همی جست و ترسان شد از بوی خاک</p></div>
<div class="m2"><p>زمین را به نعلش همی کرد چاک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بزد گام رخش تگاور به راه</p></div>
<div class="m2"><p>چنین تا بیامد میان دو چاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دل رستم از رخش شد پر ز خشم</p></div>
<div class="m2"><p>زمانش خرد را بپوشید چشم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یکی تازیانه برآورد نرم</p></div>
<div class="m2"><p>بزد نیک دل رخش را کرد گرم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو او تنگ شد در میان دو چاه</p></div>
<div class="m2"><p>ز چنگ زمانه همی جست راه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دو پایش فروشد به یک چاهسار</p></div>
<div class="m2"><p>نبد جای آویزش و کارزار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بن چاه پر حربه و تیغ تیز</p></div>
<div class="m2"><p>نبد جای مردی و راه گریز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدرید پهلوی رخش سترگ</p></div>
<div class="m2"><p>بر و پای آن پهلوان بزرگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به مردی تن خویش را برکشید</p></div>
<div class="m2"><p>دلیر از بن چاه بر سر کشید</p></div></div>