---
title: >-
    بخش ۸
---
# بخش ۸

<div class="b" id="bn1"><div class="m1"><p>بسی برنیامد برین روزگار</p></div>
<div class="m2"><p>که سرو سهی چون گل آمد به بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نه ماه بگذشت بر ماه‌روی</p></div>
<div class="m2"><p>یکی کودک آمد به بالای اوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو گفتی که بازآمد اسفندیار</p></div>
<div class="m2"><p>وگر نامدار اردشیر سوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ورا نام شاپور کرد اورمزد</p></div>
<div class="m2"><p>که سروی بد اندر میان فرزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین تا برآمد برین هفت سال</p></div>
<div class="m2"><p>ببود اورمزد از جهان بی‌همال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز هرکس نهانش همی داشتند</p></div>
<div class="m2"><p>به جایی ببازیش نگذاشتند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نخچیر شد هفت روز اردشیر</p></div>
<div class="m2"><p>بشد نیز شاپور نخچیرگیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهان اورمزد از میان گروه</p></div>
<div class="m2"><p>بیامد کز آموختن شد ستوه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوان شد به میدان شاه اردشیر</p></div>
<div class="m2"><p>کمانی به یک دست و دیگر دو تیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابا کودکان چند و چوگان و گوی</p></div>
<div class="m2"><p>به میدان شاه اندر آمد ز کوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهاندار هم در زمان با سپاه</p></div>
<div class="m2"><p>به میدان بیامد ز نخچیرگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ابا موبدان موبد تیزویر</p></div>
<div class="m2"><p>به نزدیک ایوان رسید اردشیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بزد کودکی نیز چوگان ز راه</p></div>
<div class="m2"><p>بشد گوی گردان به نزدیک شاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نرفتند زیشان پس گوی کس</p></div>
<div class="m2"><p>بماندند بر جای ناکام بس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دوان اورمزد از میانه برفت</p></div>
<div class="m2"><p>به پیش جهاندار چون باد تفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز پیش نیا زود برداشت گوی</p></div>
<div class="m2"><p>ازو گشت لشکر پر از گفت‌وگوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ازان پس خروشی برآورد سخت</p></div>
<div class="m2"><p>کزو خیره شد شاه پیروز بخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به موبد چنین گفت کین پاک‌زاد</p></div>
<div class="m2"><p>نگه کن که تا از که دارد نژاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بپرسید موبد ندانست کس</p></div>
<div class="m2"><p>همه خامشی برگزیدند و بس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به موبد چنین گفت پس شهریار</p></div>
<div class="m2"><p>که بردارش از خاک و نزد من آر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بشد موبد و برگرفتش ز گرد</p></div>
<div class="m2"><p>ببردش بر شاه آزادمرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو گفت شاه این گرانمایه خرد</p></div>
<div class="m2"><p>ترا از نژاد که باید شمرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نترسید کودک به آواز گفت</p></div>
<div class="m2"><p>که نام نژادم نباید نهفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>منم پور شاپور کو پور تست</p></div>
<div class="m2"><p>ز فرزند مهرک نژاد درست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فروماند زان کار گیتی شگفت</p></div>
<div class="m2"><p>بخندید و اندیشه اندر گرفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بفرمود تا رفت شاپور پیش</p></div>
<div class="m2"><p>به پرسش گرفتش ز اندازه بیش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بترسید شاپور آزادمرد</p></div>
<div class="m2"><p>دلش گشت پردرد و رخساره زرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بخندید زو نامور شهریار</p></div>
<div class="m2"><p>بدو گفت فرزند پنهان مدار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پسر باید از هرک باشد رواست</p></div>
<div class="m2"><p>که گویند کاین بچه پادشاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدو گفت شاپور نوشه بدی</p></div>
<div class="m2"><p>جهان را به دیدار توشه بدی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز پشت منست این و نام اورمزد</p></div>
<div class="m2"><p>درخشنده چون لاله اندر فرزد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نهان داشتم چندش از شهریار</p></div>
<div class="m2"><p>بدان تا برآید بر از میوه‌دار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گرانمایه از دختر مهرک است</p></div>
<div class="m2"><p>ز پشت منست این مرا بی‌شکست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز آب و ز چاه آن کجا رفته بود</p></div>
<div class="m2"><p>پسر گفت و پرسید و چندی شنود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز گفتار او شاد شد اردشیر</p></div>
<div class="m2"><p>به ایوان خرامید خود با وزیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرفته دلاویز را بر کنار</p></div>
<div class="m2"><p>ز ایوان سوی تخت شد شهریار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بیاراست زرین یکی زیرگاه</p></div>
<div class="m2"><p>یکی طوق فرمود و زرین کلاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سر خرد کودک بیاراستند</p></div>
<div class="m2"><p>بس از گنج در و گهر خواستند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همی ریخت تا شد سرش ناپدید</p></div>
<div class="m2"><p>تنش را نیا زان میان برکشید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بسی زر و گوهر به درویش داد</p></div>
<div class="m2"><p>خردمند را خواسته بیش داد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به دیبا بیاراست آتشکده</p></div>
<div class="m2"><p>هم ایوان نوروز و کاخ سده</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی بزمگه ساخت با مهتران</p></div>
<div class="m2"><p>نشستند هرجای رامشگران</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنین گفت با نامداران شهر</p></div>
<div class="m2"><p>هرانکس که او از خرد داشت بهر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که از گفت دانا ستاره شمر</p></div>
<div class="m2"><p>نباید که هرگز کند کس گذر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چنین گفته بد کید هندی که بخت</p></div>
<div class="m2"><p>نگردد ترا ساز و خرم به تخت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نه کشور نه افسر نه گنج و سپاه</p></div>
<div class="m2"><p>نه دیهیم شاهی نه فر کلاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مگر تخمهٔ مهرک نوش‌زاد</p></div>
<div class="m2"><p>بیامیزد آن دوده با ان نژاد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کنون سالیان اندر آمد به هشت</p></div>
<div class="m2"><p>که جز به آرزو چرخ بر ما نگشت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو شاپور رفت اندر آرام خویش</p></div>
<div class="m2"><p>ز گیتی ندیده به جز کام خویش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زمین هفت کشور مرا گشت راست</p></div>
<div class="m2"><p>دلم یافت از بخت چیزی که خواست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وزان پس بر کارداران اوی</p></div>
<div class="m2"><p>شهنشاه کردند عنوان اوی</p></div></div>