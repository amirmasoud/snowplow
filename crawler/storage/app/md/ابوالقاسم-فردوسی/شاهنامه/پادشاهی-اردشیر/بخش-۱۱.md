---
title: >-
    بخش ۱۱
---
# بخش ۱۱

<div class="b" id="bn1"><div class="m1"><p>به گفتار این نامدار اردشیر</p></div>
<div class="m2"><p>همه گوش دارید برنا و پیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرانکس که داند که دادار هست</p></div>
<div class="m2"><p>نباشد مگر پاک و یزدان پرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگر آنک دانش مگیرید خوار</p></div>
<div class="m2"><p>اگر زیردستست و گر شهریار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سه دیگر بدانی که هرگز سخن</p></div>
<div class="m2"><p>نگردد بر مرد دانا کهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهارم چنان دان که بیم گناه</p></div>
<div class="m2"><p>فزون باشد از بند و زندان شاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پنجم سخن مردم زشت‌گوی</p></div>
<div class="m2"><p>نگیرد به نزد کسان آب‌روی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگویم یکی تازه اندرز نیز</p></div>
<div class="m2"><p>کجا برتر از دیده و جان و چیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خنک آنک آباد دارد جهان</p></div>
<div class="m2"><p>بود آشکارای او چون نهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر آنک دارند آواز نرم</p></div>
<div class="m2"><p>خرد دارد و شرم و گفتار گرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پیش کسان سیم از بهر لاف</p></div>
<div class="m2"><p>به بیهوده بپراگند بر گزاف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز مردم ندارد کسی زان سپاس</p></div>
<div class="m2"><p>نبپسندد آن مرد یزدان شناس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میانه گزینی بمانی به جای</p></div>
<div class="m2"><p>خردمند خوانند و پاکیزه‌رای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کزین بگذری پنج رایست پیش</p></div>
<div class="m2"><p>کجا تازه گردد ترا دین وکیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تن آسانی و شادی افزایدت</p></div>
<div class="m2"><p>که با شهد او زهر نگزایدت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی آنک از بخشش دادگر</p></div>
<div class="m2"><p>به آز و به کوشش نیابی گذر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>توانگر شود هرک خرسند گشت</p></div>
<div class="m2"><p>گل نوبهارش برومند گشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دگر بشکنی گردن آز را</p></div>
<div class="m2"><p>نگویی به پیش زنان راز را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سه دگیر ننازی به ننگ و نبرد</p></div>
<div class="m2"><p>که ننگ ونبرد آورد رنج و درد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چهارم که دل دور داری ز غم</p></div>
<div class="m2"><p>ز نا آمده دل نداری دژم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه پیچی به کاری که کار تو نیست</p></div>
<div class="m2"><p>نتازی بدان کو شکار تو نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه گوش دارید پند مرا</p></div>
<div class="m2"><p>سخن گفتن سودمند مرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بود بر دل هرکسی ارجمند</p></div>
<div class="m2"><p>که یابند ازو ایمنی از گزند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زمانی میاسای ز آموختن</p></div>
<div class="m2"><p>اگر جان همی خواهی افروختن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو فرزند باشد به فرهنگ دار</p></div>
<div class="m2"><p>زمانه ز بازی برو تنگ دار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه یاد دارید گفتار ما</p></div>
<div class="m2"><p>کشیدن بدین کار تیمار ما</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرآن کس که با داد و روشن دلید</p></div>
<div class="m2"><p>از آمیزش یکدگر مگسلید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دل آرام دارید بر چار چیز</p></div>
<div class="m2"><p>کزو خوبی و سودمندیست نیز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی بیم و آزرم و شرم خدای</p></div>
<div class="m2"><p>که باشد ترا یاور و رهنمای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دگر داد دادن تن خویش را</p></div>
<div class="m2"><p>نگه داشتن دامن خویش را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به فرمان یزدان دل آراستن</p></div>
<div class="m2"><p>مرا چون تن خویشتن خواستن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سه دیگر که پیدا کنی راستی</p></div>
<div class="m2"><p>بدور افگنی کژی و کاستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چهارم که از رای شاه جهان</p></div>
<div class="m2"><p>نپیچی دلت آشکار و نهان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ورا چون تن خویش خواهی به مهر</p></div>
<div class="m2"><p>به فرمان او تازه گردد سپهر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دلت بسته داری به پیمان اوی</p></div>
<div class="m2"><p>روان را نپیچی ز فرمان اوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برو مهر داری چو بر جان خویش</p></div>
<div class="m2"><p>چو با داد بینی نگهبان خویش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>غم پادشاهی جهانجوی راست</p></div>
<div class="m2"><p>ز گیتی فزونی سگالد نه کاست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر از کارداران وز لشکرش</p></div>
<div class="m2"><p>بداند که رنجست بر کشورش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نیازد به داد او جهاندار نیست</p></div>
<div class="m2"><p>برو تاج شاهی سزاوار نیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سیه کرد منشور شاهنشهی</p></div>
<div class="m2"><p>ازان پس نباشد ورا فرهی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنان دان که بیدادگر شهریار</p></div>
<div class="m2"><p>بود شیر درنده در مرغزار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همان زیردستی که فرمان شاه</p></div>
<div class="m2"><p>به رنج و به کوشش ندارد نگاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بود زندگانیش با درد و رنج</p></div>
<div class="m2"><p>نگردد کهن در سرای سپنج</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر مهتری یابد و بهتری</p></div>
<div class="m2"><p>نیابد به زفتی و کنداوری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دل زیردستان ما شاد باد</p></div>
<div class="m2"><p>هم از داد ماگیتی آباد باد</p></div></div>