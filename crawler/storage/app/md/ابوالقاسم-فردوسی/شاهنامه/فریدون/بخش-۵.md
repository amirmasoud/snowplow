---
title: >-
    بخش ۵
---
# بخش ۵

<div class="b" id="bn1"><div class="m1"><p>نهفته چو بیرون کشید از نهان</p></div>
<div class="m2"><p>به سه بخش کرد آفریدون جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی روم و خاور دگر ترک و چین</p></div>
<div class="m2"><p>سیم دشت گردان و ایران‌زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخستین به سلم اندرون بنگرید</p></div>
<div class="m2"><p>همه روم و خاور مراو را سزید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به فرزند تا لشکری برگزید</p></div>
<div class="m2"><p>گرازان سوی خاور اندرکشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تخت کیان اندر آورد پای</p></div>
<div class="m2"><p>همی خواندندیش خاور خدای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر تور را داد توران زمین</p></div>
<div class="m2"><p>ورا کرد سالار ترکان و چین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی لشکری نا مزد کرد شاه</p></div>
<div class="m2"><p>کشید آنگهی تور لشکر به راه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیامد به تخت کئی برنشست</p></div>
<div class="m2"><p>کمر بر میان بست و بگشاد دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بزرگان برو گوهر افشاندند</p></div>
<div class="m2"><p>همی پاک توران شهش خواندند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ایشان چو نوبت به ایرج رسید</p></div>
<div class="m2"><p>مر او را پدر شاه ایران گزید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم ایران و هم دشت نیزه‌وران</p></div>
<div class="m2"><p>هم آن تخت شاهی و تاج سران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو داد کورا سزا بود تاج</p></div>
<div class="m2"><p>همان کرسی و مهر و آن تخت عاج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشستند هر سه به آرام و شاد</p></div>
<div class="m2"><p>چنان مرزبانان فرخ نژاد</p></div></div>