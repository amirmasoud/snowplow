---
title: >-
    بخش ۱۰
---
# بخش ۱۰

<div class="b" id="bn1"><div class="m1"><p>چو برداشت پرده ز پیش آفتاب</p></div>
<div class="m2"><p>سپیده برآمد به پالود خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو بیهوده را دل بدان کار گرم</p></div>
<div class="m2"><p>که دیده بشویند هر دو ز شرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برفتند هر دو گرازان ز جای</p></div>
<div class="m2"><p>نهادند سر سوی پرده‌سرای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو از خیمه ایرج به ره بنگرید</p></div>
<div class="m2"><p>پر از مهر دل پیش ایشان دوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برفتند با او به خیمه درون</p></div>
<div class="m2"><p>سخن بیشتر بر چرا رفت و چون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو گفت تور ار تو از ماکهی</p></div>
<div class="m2"><p>چرا برنهادی کلاه مهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا باید ایران و تخت کیان</p></div>
<div class="m2"><p>مرا بر در ترک بسته میان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برادر که مهتر به خاور به رنج</p></div>
<div class="m2"><p>به سر بر ترا افسر و زیر گنج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین بخششی کان جهانجوی کرد</p></div>
<div class="m2"><p>همه سوی کهتر پسر روی کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه تاج کیان مانم اکنون نه گاه</p></div>
<div class="m2"><p>نه نام بزرگی نه ایران سپاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو از تور بشنید ایرج سخن</p></div>
<div class="m2"><p>یکی پاکتر پاسخ افگند بن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو گفت کای مهتر کام جوی</p></div>
<div class="m2"><p>اگر کام دل خواهی آرام جوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من ایران نخواهم نه خاور نه چین</p></div>
<div class="m2"><p>نه شاهی نه گسترده روی زمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بزرگی که فرجام او تیرگیست</p></div>
<div class="m2"><p>برآن مهتری بر بباید گریست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپهر بلند ار کشد زین تو</p></div>
<div class="m2"><p>سرانجام خشتست بالین تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا تخت ایران اگر بود زیر</p></div>
<div class="m2"><p>کنون گشتم از تاج و از تخت سیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپردم شما را کلاه و نگین</p></div>
<div class="m2"><p>بدین روی با من مدارید کین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا با شما نیست ننگ و نبرد</p></div>
<div class="m2"><p>روان را نباید برین رنجه کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زمانه نخواهم به آزارتان</p></div>
<div class="m2"><p>اگر دورمانم ز دیدارتان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جز از کهتری نیست آیین من</p></div>
<div class="m2"><p>مباد آز و گردن‌کشی دین من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو بشنید تور از برادر چنین</p></div>
<div class="m2"><p>به ابرو ز خشم اندر آورد چین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نیامدش گفتار ایرج پسند</p></div>
<div class="m2"><p>نبد راستی نزد او ارجمند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به کرسی به خشم اندر آورد پای</p></div>
<div class="m2"><p>همی گفت و برجست هزمان ز جای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکایک برآمد ز جای نشست</p></div>
<div class="m2"><p>گرفت آن گران کرسی زر به دست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بزد بر سر خسرو تاجدار</p></div>
<div class="m2"><p>از او خواست ایرج به جان زینهار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نیایدت گفت ایچ بیم از خدای</p></div>
<div class="m2"><p>نه شرم از پدر خود همین است رای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مکش مر مرا کت سرانجام کار</p></div>
<div class="m2"><p>بپیچاند از خون من کردگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مکن خویشتن را ز مردم‌کشان</p></div>
<div class="m2"><p>کزین پس نیابی ز من خود نشان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بسنده کنم ز این جهان گوشه‌ای</p></div>
<div class="m2"><p>به کوشش فراز آورم توشه‌ای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به خون برادر چه بندی کمر</p></div>
<div class="m2"><p>چه سوزی دل پیر گشته پدر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهان خواستی یافتی خون مریز</p></div>
<div class="m2"><p>مکن با جهاندار یزدان ستیز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سخن را چو بشنید پاسخ نداد</p></div>
<div class="m2"><p>همان گفتن آمد همان سرد باد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی خنجر آبگون برکشید</p></div>
<div class="m2"><p>سراپای او چادر خون کشید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدان تیز زهرآبگون خنجرش</p></div>
<div class="m2"><p>همی کرد چاک آن کیانی برش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فرود آمد از پای سرو سهی</p></div>
<div class="m2"><p>گسست آن کمرگاه شاهنشهی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>روان خون از آن چهرهٔ ارغوان</p></div>
<div class="m2"><p>شد آن نامور شهریار جوان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جهانا بپروردیش در کنار</p></div>
<div class="m2"><p>وز آن پس ندادی به جان زینهار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نهانی ندانم ترا دوست کیست</p></div>
<div class="m2"><p>بدین آشکارت بباید گریست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سر تاجور ز آن تن پیلوار</p></div>
<div class="m2"><p>به خنجر جدا کرد و برگشت کار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بیاگند مغزش به مشک و عبیر</p></div>
<div class="m2"><p>فرستاد نزد جهان‌بخش پیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنین گفت کاینت سر آن نیاز</p></div>
<div class="m2"><p>که تاج نیاگان بدو گشت باز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کنون خواه تاجش ده و خواه تخت</p></div>
<div class="m2"><p>شد آن سایه‌گستر نیازی درخت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برفتند باز آن دو بیداد شوم</p></div>
<div class="m2"><p>یکی سوی ترک و یکی سوی روم</p></div></div>