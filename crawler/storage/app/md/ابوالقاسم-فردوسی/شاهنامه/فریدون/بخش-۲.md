---
title: >-
    بخش ۲
---
# بخش ۲

<div class="b" id="bn1"><div class="m1"><p>ز سالش چو یک پنجه اندر کشید</p></div>
<div class="m2"><p>سه فرزندش آمد گرامی پدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بخت جهاندار هر سه پسر</p></div>
<div class="m2"><p>سه خسرو نژاد از در تاج زر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بالا چو سرو و به رخ چون بهار</p></div>
<div class="m2"><p>به هر چیز مانندهٔ شهریار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از این سه دو پاکیزه از شهرناز</p></div>
<div class="m2"><p>یکی کهتر از خوب چهر ارنواز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پدر نوز ناکرده از ناز نام</p></div>
<div class="m2"><p>همی پیش پیلان نهادند گام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فریدون از آن نامداران خویش</p></div>
<div class="m2"><p>یکی را گرانمایه‌تر خواند پیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجا نام او جندل پرهنر</p></div>
<div class="m2"><p>به هر کار دلسوز بر شاه بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدو گفت برگرد گرد جهان</p></div>
<div class="m2"><p>سه دختر گزین از نژاد مهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سه خواهر ز یک مادر و یک پدر</p></div>
<div class="m2"><p>پری چهره و پاک و خسرو گهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خوبی سزای سه فرزند من</p></div>
<div class="m2"><p>چنان چون بشاید به پیوند من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به بالا و دیدار هر سه یکی</p></div>
<div class="m2"><p>که این را ندانند ازان اندکی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو بشنید جندل ز خسرو سخن</p></div>
<div class="m2"><p>یکی رای پاکیزه افگند بن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که بیدار دل بود و پاکیزه مغز</p></div>
<div class="m2"><p>زبان چرب و شایستهٔ کار نغز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز پیش سپهبد برون شد به راه</p></div>
<div class="m2"><p>ابا چند تن مر ورا نیکخواه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکایک ز ایران سراندر کشید</p></div>
<div class="m2"><p>پژوهید و هرگونه گفت و شنید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به هر کشوری کز جهان مهتری</p></div>
<div class="m2"><p>به پرده درون داشتن دختری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نهفته بجستی همه رازشان</p></div>
<div class="m2"><p>شنیدی همه نام و آوازشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز دهقان پر مایه کس را ندید</p></div>
<div class="m2"><p>که پیوستهٔ آفریدون سزید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خردمند و روشن‌دل و پاک‌تن</p></div>
<div class="m2"><p>بیامد بر سرو شاه یمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نشان یافت جندل مر اورا درست</p></div>
<div class="m2"><p>سه دختر چنان چون فریدون بجست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خرامان بیامد به نزدیک سرو</p></div>
<div class="m2"><p>چنان چون به پیش گل اندر تذرو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زمین را ببوسید و چربی نمود</p></div>
<div class="m2"><p>برآن کهتری آفرین برفزود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به جندل چنین گفت شاه یمن</p></div>
<div class="m2"><p>که بی‌آفرینت مبادا دهن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه پیغام داری چه فرمان دهی</p></div>
<div class="m2"><p>فرستاده‌ای گر گرامی رهی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدو گفت جندل که خرم بدی</p></div>
<div class="m2"><p>همیشه ز تو دور دست بدی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از ایران یکی کهترم چون شمن</p></div>
<div class="m2"><p>پیام آوریده به شاه یمن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>درود فریدون فرخ دهم</p></div>
<div class="m2"><p>سخن هر چه پرسند پاسخ دهم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ترا آفرین از فریدون گرد</p></div>
<div class="m2"><p>بزرگ آنکسی کو نداردش خرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا گفت شاه یمن را بگوی</p></div>
<div class="m2"><p>که بر گاه تا مشک بوید ببوی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدان ای سر مایهٔ تازیان</p></div>
<div class="m2"><p>کز اختر بدی جاودان بی‌زیان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرا پادشاهی آباد هست</p></div>
<div class="m2"><p>همان گنج و مردی و نیروی دست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سه فرزند شایستهٔ تاج و گاه</p></div>
<div class="m2"><p>اگر داستان را بود گاه ماه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز هر کام و هر خواسته بی‌نیاز</p></div>
<div class="m2"><p>به هر آرزو دست ایشان دراز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مر این سه گرانمایه را در نهفت</p></div>
<div class="m2"><p>بباید کنون شاهزاده سه جفت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز کار آگهان آگهی یافتم</p></div>
<div class="m2"><p>بدین آگهی تیز بشتافتم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کجا از پس پرده پوشیده روی</p></div>
<div class="m2"><p>سه پاکیزه داری تو ای نامجوی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مران هرسه را نوز ناکرده نام</p></div>
<div class="m2"><p>چو بشنیدم این دل شدم شادکام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که ما نیز نام سه فرخ نژاد</p></div>
<div class="m2"><p>چو اندر خور آید نکردیم یاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کنون این گرامی دو گونه گهر</p></div>
<div class="m2"><p>بباید برآمیخت با یکدگر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سه پوشیده رخ را سه دیهیم جوی</p></div>
<div class="m2"><p>سزا را سزاوار بی‌گفت‌وگوی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فریدون پیامم بدین گونه داد</p></div>
<div class="m2"><p>تو پاسخ گزار آنچه آیدت یاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پیامش چو بشنید شاه یمن</p></div>
<div class="m2"><p>بپژمرد چون زاب کنده سمن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همی گفت گر پیش بالین من</p></div>
<div class="m2"><p>نبیند سه ماه این جهان‌بین من</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرا روز روشن بود تاره شب</p></div>
<div class="m2"><p>بباید گشادن به پاسخ دو لب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سراینده را گفت کای نامجوی</p></div>
<div class="m2"><p>زمان باید اندر چنین گفت‌گوی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شتابت نباید بپاسخ کنون</p></div>
<div class="m2"><p>مرا چند رازست با رهنمون</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>فرستاده را زود جایی گزید</p></div>
<div class="m2"><p>پس آنگه به کار اندرون بنگرید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بیامد در بار دادن ببست</p></div>
<div class="m2"><p>به انبوه اندیشگان در نشست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فراوان کس از دشت نیزه‌وران</p></div>
<div class="m2"><p>بر خویش خواند آزموده سران</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نهفته برون آورید از نهفت</p></div>
<div class="m2"><p>همه رازها پیش ایشان بگفت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>که ما را به گیتی ز پیوند خویش</p></div>
<div class="m2"><p>سه شمع‌ست روشن به دیدار پیش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>فریدون فرستاد زی من پیام</p></div>
<div class="m2"><p>بگسترد پیشم یکی خوب دام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همی کرد خواهد ز چشمم جدا</p></div>
<div class="m2"><p>یکی رای بایدزدن با شما</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فرستاده گوید چنین گفت شاه</p></div>
<div class="m2"><p>که ما را سه شاهست زیبای گاه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گراینده هر سه به پیوند من</p></div>
<div class="m2"><p>به سه روی پوشیده فرزند من</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اگر گویم آری و دل زان تهی</p></div>
<div class="m2"><p>دروغم نه اندر خورد با مهی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>وگر آرزوها سپارم بدوی</p></div>
<div class="m2"><p>شود دل پر آتش پر از آب روی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>وگر سر بپیچم ز فرمان او</p></div>
<div class="m2"><p>به یک سو گرایم ز پیمان او</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کسی کو بود شهریار زمین</p></div>
<div class="m2"><p>نه بازیست با او سگالید کین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شنیدستم از مردم راه‌جوی</p></div>
<div class="m2"><p>که ضحاک را زو چه آمد بروی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ازین در سخن هر چه دارید یاد</p></div>
<div class="m2"><p>سراسر به من بر بباید گشاد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>جهان آزموده دلاور سران</p></div>
<div class="m2"><p>گشادند یک‌یک به پاسخ زبان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که ما همگنان آن نبینیم رای</p></div>
<div class="m2"><p>که هر باد را تو بجنبی ز جای</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>اگر شد فریدون جهان شهریار</p></div>
<div class="m2"><p>نه ما بندگانیم با گوشوار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سخن‌گفتن و کوشش آیین ماست</p></div>
<div class="m2"><p>عنان و سنان تافتن دین ماست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به خنجر زمین را میستان کنیم</p></div>
<div class="m2"><p>به نیزه هوا را نیستان کنیم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سه فرزند اگر بر تو هست ارجمند</p></div>
<div class="m2"><p>سربدره بگشای و لب را ببند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>و گر چارهٔ کار خواهی همی</p></div>
<div class="m2"><p>بترسی ازین پادشاهی همی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ازو آرزوهای پرمایه جوی</p></div>
<div class="m2"><p>که کردار آنرا نبینند روی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو بشنید از آن نامداران سخن</p></div>
<div class="m2"><p>نه سردید آن را به گیتی نه بن</p></div></div>