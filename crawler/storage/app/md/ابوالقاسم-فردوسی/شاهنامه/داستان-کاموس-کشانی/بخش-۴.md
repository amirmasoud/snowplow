---
title: >-
    بخش ۴
---
# بخش ۴

<div class="b" id="bn1"><div class="m1"><p>باسپ عقاب اندر آورد پای</p></div>
<div class="m2"><p>برانگیخت آن بارگی را ز جای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو گفتی یکی بارهٔ آهنست</p></div>
<div class="m2"><p>وگر کوه البرز در جوشنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پیش سپاه اندر آمد بجنگ</p></div>
<div class="m2"><p>یکی خشت رخشان گرفته بچنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجنبید طوس سپهبد ز جای</p></div>
<div class="m2"><p>جهان پر شد از نالهٔ کر نای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهومان چنین گفت کای شوربخت</p></div>
<div class="m2"><p>ز پالیز کین برنیامد درخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمودم بارژنگ یک دست برد</p></div>
<div class="m2"><p>که بود از شما نامبردار و گرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو اکنون همانا بکین آمدی</p></div>
<div class="m2"><p>که با خشت بر پشت زین آمدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجان و سر شاه ایران سپاه</p></div>
<div class="m2"><p>که بی‌جوشن و گرز و رومی کلاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجنگ تو آیم بسان پلنگ</p></div>
<div class="m2"><p>که از کوه یازد بنخچیر چنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ببینی تو پیکار مردان مرد</p></div>
<div class="m2"><p>چو آورد گیرم بدشت نبرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین پاسخ آورد هومان بدوی</p></div>
<div class="m2"><p>که بیشی نه خوبست بیشی مجوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر ایدونک بیچاره‌ای را زمان</p></div>
<div class="m2"><p>بدست تو آمد مشو در گمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بجنگ من ارژنگ روز نبرد</p></div>
<div class="m2"><p>کجا داشتی خویشتن را بمرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلیران لشکر ندارند شرم</p></div>
<div class="m2"><p>نجوشد یکی را برگ خون گرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که پیکار ایشان سپهبد کند</p></div>
<div class="m2"><p>برزم اندرون دستشان بد کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کجا بیژن و گیو آزادگان</p></div>
<div class="m2"><p>جهانگیر گودرز کشوادگان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو گر پهلوانی ز قلب سپاه</p></div>
<div class="m2"><p>چرا آمدستی بدین رزمگاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خردمند بیگانه خواند ترا</p></div>
<div class="m2"><p>هشیوار دیوانه خواند ترا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو شو اختر کاویانی بدار</p></div>
<div class="m2"><p>سپهبد نیاید سوی کارزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگه کن که خلعت کرا داد شاه</p></div>
<div class="m2"><p>ز گردان که جوید نگین و کلاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بفرمای تا جنگ شیر آورند</p></div>
<div class="m2"><p>زبردست را دست زیر آورند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر تو شوی کشته بر دست من</p></div>
<div class="m2"><p>بد آید بدان نامدار انجمن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپاه تو بی‌یار و بیجان شوند</p></div>
<div class="m2"><p>اگر زنده مانند پیچان شوند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>و دیگر که گر بشنوی گفت راست</p></div>
<div class="m2"><p>روان و دلم بر زبانم گواست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که پر درد باشم ز مردان مرد</p></div>
<div class="m2"><p>که پیش من آیند روز نبرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس از رستم زال سام سوار</p></div>
<div class="m2"><p>ندیدم چو تو نیز یک نامدار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پدر بر پدر نامبردار و شاه</p></div>
<div class="m2"><p>چو تو جنگجویی نیاید سپاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو شو تا ز لشکر یکی نامجوی</p></div>
<div class="m2"><p>بیاید بروی اندر آریم روی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدو گفت طوس ای سرافراز مرد</p></div>
<div class="m2"><p>سپهبد منم هم سوار نبرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو هم نامداری ز توران سپاه</p></div>
<div class="m2"><p>چرا رای کردی به آوردگاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دلت گر پذیرد یکی پند من</p></div>
<div class="m2"><p>بجویی بدین کار پیوند من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کزین کینه تا زنده ماند یکی</p></div>
<div class="m2"><p>نیاسود خواهد سپاه اندکی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو با خویش وپیوند و چندین سوار</p></div>
<div class="m2"><p>همه پهلوان و همه نامدار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بخیره مده خویشتن را بباد</p></div>
<div class="m2"><p>بباید که پند من آیدت یاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سزاوار کشتن هرآنکس که هست</p></div>
<div class="m2"><p>بمان تا بیازند بر کینه دست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کزین کینه مرد گنهکار هیچ</p></div>
<div class="m2"><p>رهایی نیابد خرد را مپیچ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرا شاه ایران چنین داد پند</p></div>
<div class="m2"><p>که پیران نباید که یابد گزند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که او ویژه پروردگار منست</p></div>
<div class="m2"><p>جهاندیده و دوستدار منست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به بیداد بر خیره با او مکوش</p></div>
<div class="m2"><p>نگه کن که دارد بپند تو گوش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنین گفت هومان به بیداد و داد</p></div>
<div class="m2"><p>چو فرمان دهد شاه فرخ نژاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بران رفت باید ببیچارگی</p></div>
<div class="m2"><p>سپردن بدو دل بیکبارگی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همان رزم پیران نه بر آرزوست</p></div>
<div class="m2"><p>که او راد و آزاده و نیک خوست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدین گفت و گوی اندرون بود طوس</p></div>
<div class="m2"><p>که شد گیو را روی چون سندروس</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز لشکر بیامد بکردار باد</p></div>
<div class="m2"><p>چنین گفت کای طوس فرخ نژاد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فریبنده هومان میان دو صف</p></div>
<div class="m2"><p>بیامد دمان بر لب آورده کف</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کنون با تو چندین چه گوید براز</p></div>
<div class="m2"><p>میان دو صف گفت و گوی دراز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سخن جز بشمشیر با او مگوی</p></div>
<div class="m2"><p>مجوی از در آشتی هیچ روی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو بشنید هومان برآشفت سخت</p></div>
<div class="m2"><p>چنین گفت با گیو بیدار بخت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ایا گم شده بخت آزادگان</p></div>
<div class="m2"><p>که گم باد گودرز کشوادگان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فراوان مرا دیده‌ای روز جنگ</p></div>
<div class="m2"><p>به آوردگه تیغ هندی بچنگ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کس از تخم کشواد جنگی نماند</p></div>
<div class="m2"><p>که منشور تیغ مرا برنخواند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ترا بخت چون روی آهرمنست</p></div>
<div class="m2"><p>بخان تو تا جاودان شیونست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اگر من شوم کشته بر دست طوس</p></div>
<div class="m2"><p>نه برخیزد آیین گوپال و کوس</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بجایست پیران و افراسیاب</p></div>
<div class="m2"><p>بخواهد شدن خون من رود آب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نه گیتی شود پاک ویران ز من</p></div>
<div class="m2"><p>سخن راند باید بدین انجمن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وگر طوس گردد بدستم تباه</p></div>
<div class="m2"><p>یکی ره نیابند ز ایران سپاه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو اکنون بمرگ برادر گری</p></div>
<div class="m2"><p>چه با طوس نوذر کنی داوری</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بدو گفت طوس این چه آشفتنست</p></div>
<div class="m2"><p>بدین دشت پیکار تو با منست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بیا تا بگردیم و کین آوریم</p></div>
<div class="m2"><p>بجنگ ابروان پر ز چین آوریم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بدو گفت هومان که دادست مرگ</p></div>
<div class="m2"><p>سری زیر تاج و سری زیر ترگ</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>اگر مرگ باشد مرا بی‌گمان</p></div>
<div class="m2"><p>به آوردگه به که آید زمان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بدست سواری که دارد هنر</p></div>
<div class="m2"><p>سپهبدسر و گرد و پرخاشخر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گرفتند هر دو عمود گران</p></div>
<div class="m2"><p>همی حمله برد آن برین این بران</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز می گشت گردان و شد روز تار</p></div>
<div class="m2"><p>یکی ابر بست از بر کارزار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تو گفتی شب آمد بریشان بروز</p></div>
<div class="m2"><p>نهان گشت خورشید گیتی فروز</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ازان چاک چاک عمود گران</p></div>
<div class="m2"><p>سرانشان چو سندان آهنگران</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بابر اندرون بانگ پولاد خاست</p></div>
<div class="m2"><p>بدریای شهد اندرون باد خاست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز خون بر کف شیر کفشیر بود</p></div>
<div class="m2"><p>همه دشت پر بانگ شمشیر بود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خم آورد رویین عمود گران</p></div>
<div class="m2"><p>شد آهن به کردار چاچی کمان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تو گفتی که سنگ است سر زیر ترگ</p></div>
<div class="m2"><p>سیه شد ز خم یلان روی مرگ</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گرفتند شمشیر هندی بچنگ</p></div>
<div class="m2"><p>فرو ریخت آتش ز پولاد و سنگ</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز نیروی گردنکشان تیغ تیز</p></div>
<div class="m2"><p>خم آورد و در زخم شد ریز ریز</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>همه کام پرخاک و پر خاک سر</p></div>
<div class="m2"><p>گرفتند هر دو دوال کمر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز نیروی گردان گران شد رکیب</p></div>
<div class="m2"><p>یکی را نیامد سر اندر نشیب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>سپهبد سوی ترکش آورد چنگ</p></div>
<div class="m2"><p>کمان را بزه کرد و تیر خدنگ</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بران نامور تیرباران گرفت</p></div>
<div class="m2"><p>چپ و راست جنگ سواران گرفت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز پولاد پیکان و پر عقاب</p></div>
<div class="m2"><p>سپر کرد بر پیش روی آفتاب</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>جهان چون ز شب رفته دو پاس گشت</p></div>
<div class="m2"><p>همه روی کشور پر الماس گشت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ز تیر خدنگ اسپ هومان بخست</p></div>
<div class="m2"><p>تن بارگی گشت با خاک پست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سپر بر سر آورد و ننمود روی</p></div>
<div class="m2"><p>نگه داشت هومان سر از تیر اوی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو او را پیاده بران رزمگاه</p></div>
<div class="m2"><p>بدیدند گفتند توران سپاه</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>که پردخت ماند کنون جای اوی</p></div>
<div class="m2"><p>ببردند پرمایه بالای اوی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چو هومان بران زین توزی نشست</p></div>
<div class="m2"><p>یکی تیغ بگرفت هندی بدست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>که آید دگر باره بر جنگ طوس</p></div>
<div class="m2"><p>شد از شب جهان تیره چون آبنوس</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>همه نامداران پرخاشجوی</p></div>
<div class="m2"><p>یکایک بدو در نهادند روی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو شد روز تاریک و بیگاه گشت</p></div>
<div class="m2"><p>ز جنگ یلان دست کوتاه گشت</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بپیچید هومان جنگی عنان</p></div>
<div class="m2"><p>سپهبد بدو راست کرده سنان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بنزدیک پیران شد از رزمگاه</p></div>
<div class="m2"><p>خروشی برآمد ز توران سپاه</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز تو خشم گردنکشان دور باد</p></div>
<div class="m2"><p>درین جنگ فرجام ما سور باد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>که چون بود رزم تو ای نامجوی</p></div>
<div class="m2"><p>چو با طوس روی اندر آمد بروی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>همه پاک ما دل پر از خون بدیم</p></div>
<div class="m2"><p>جز ایزد نداند که ما چون بدیم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بلشکر چنین گفت هومان شیر</p></div>
<div class="m2"><p>که ای رزم دیده سران دلیر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چو روشن شود تیره شب روز ماست</p></div>
<div class="m2"><p>که این اختر گیتی افروز ماست</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>شما را همه شادکامی بود</p></div>
<div class="m2"><p>مرا خوبی و نیکنامی بود</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ز لشکر همی برخروشید طوس</p></div>
<div class="m2"><p>شب تیره تا گاه بانگ خروس</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>همی گفت هومان چه مرد منست</p></div>
<div class="m2"><p>که پیل ژیان هم نبرد منست</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چو چرخ بلند از شبه تاج کرد</p></div>
<div class="m2"><p>شمامه پراگند بر لاژورد</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>طلایه ز هر سو برون تاختند</p></div>
<div class="m2"><p>بهر پرده‌ای پاسبان ساختند</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چو برزد سر از برج خرچنگ شید</p></div>
<div class="m2"><p>جهان گشت چون روی رومی سپید</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>تبیره برآمد ز هر دو سرای</p></div>
<div class="m2"><p>جهان شد پر از نالهٔ کر نای</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>هوا تیره گشت از فروغ درفش</p></div>
<div class="m2"><p>طبر خون و شبگون و زرد و بنفش</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>کشیده همه تیغ و گرز و سنان</p></div>
<div class="m2"><p>همه جنگ را گرد کرده عنان</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>تو گفتی سپهر و زمان و زمین</p></div>
<div class="m2"><p>بپوشد همی چادر آهنین</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بپرده درون شد خور تابناک</p></div>
<div class="m2"><p>ز جوش سواران و از گرد و خاک</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ز هرای اسپان و آوای کوس</p></div>
<div class="m2"><p>همی آسمان بر زمین داد بوس</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>سپهدار هومان دمان پیش صف</p></div>
<div class="m2"><p>یکی خشت رخشان گرفته بکف</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>همی گفت چون من برایم بجوش</p></div>
<div class="m2"><p>برانگیزم اسپ و برارم خروش</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>شما یک بیک تیغها برکشید</p></div>
<div class="m2"><p>سپرهای چینی بسر در کشید</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>مبینید جز یال اسپ و عنان</p></div>
<div class="m2"><p>نشاید کمان و نباید سنان</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>عنان پاک بر یال اسپان نهید</p></div>
<div class="m2"><p>بدانسان که آید خورید و دهید</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بپیران چنین گفت کای پهلوان</p></div>
<div class="m2"><p>تو بگشای بند سلیح گوان</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>ابا گنج دینار جفتی مکن</p></div>
<div class="m2"><p>ز بهر سلیح ایچ زفتی مکن</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>که امروز گردیم پیروزگر</p></div>
<div class="m2"><p>بیابد دل از اختر نیک بر</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>وزین روی لشکر سپهدار طوس</p></div>
<div class="m2"><p>بیاراست برسان چشم خروش</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>بروبر یلان آفرین خواندند</p></div>
<div class="m2"><p>ورا پهلوان زمین خواندند</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>که پیروزگر بود روز نبرد</p></div>
<div class="m2"><p>ز هومان ویسه برآورد گرد</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>سپهبد بگودرز کشواد گفت</p></div>
<div class="m2"><p>که این راز بر کس نباید نهفت</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>اگر لشکر ما پذیره شوند</p></div>
<div class="m2"><p>سواران بدخواه چیره شوند</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>همه دست یکسر بیزدان زنیم</p></div>
<div class="m2"><p>منی از تن خویش بفگنیم</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>مگر دست گیرد جهاندار ما</p></div>
<div class="m2"><p>وگر نه بد است اختر کار ما</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>کنون نامداران زرینه کفش</p></div>
<div class="m2"><p>بباشید با کاویانی درفش</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>ازین کوه پایه مجنبید هیچ</p></div>
<div class="m2"><p>نه روز نبرد است و گاه بسیچ</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>همانا که از ما بهر یک دویست</p></div>
<div class="m2"><p>فزونست بدخواه اگر بیش نیست</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>بدو گفت گودرز اگر کردگار</p></div>
<div class="m2"><p>بگرداند از ما بد روزگار</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>به بیشی و کمی نباشد سخن</p></div>
<div class="m2"><p>دل و مغز ایرانیان بد مکن</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>اگر بد بود بخشش آسمان</p></div>
<div class="m2"><p>بپرهیز و بیشی نگردد زمان</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>تو لشکر بیارای و از بودنی</p></div>
<div class="m2"><p>روان را مکن هیچ بشخودنی</p></div></div>