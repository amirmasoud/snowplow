---
title: >-
    بخش ۸
---
# بخش ۸

<div class="b" id="bn1"><div class="m1"><p>فرستاده نزدیک پیران رسید</p></div>
<div class="m2"><p>بجوشید چون گفت هومان شنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیامد شب تیره هنگام خواب</p></div>
<div class="m2"><p>همی راند لشکر بکردار آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خورشید زان چادر قیرگون</p></div>
<div class="m2"><p>غمی شد بدرید و آمد برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپهبد بکوه هماون رسید</p></div>
<div class="m2"><p>ز گرد سپه کوه شد ناپدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهومان چنین گفت کز رزمگاه</p></div>
<div class="m2"><p>مجنب و مجنبان از ایدر سپاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوم تا سپهدار ایرانیان</p></div>
<div class="m2"><p>چه دارد بپا اختر کاویان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکوه هماون که دادش نوید</p></div>
<div class="m2"><p>بدین بودن اکنون چه دارد امید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیامد بنزدیک ایران سپاه</p></div>
<div class="m2"><p>سری پر ز کینه دلی پرگناه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خروشید کای نامبردار طوس</p></div>
<div class="m2"><p>خداوند پیلان و گوپال و کوس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنون ماهیان اندر آمد به پنج</p></div>
<div class="m2"><p>که تا تو همی رزم جویی برنج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز گودرزیان آن کجا مهترند</p></div>
<div class="m2"><p>بدان رزمگاهت همه بی‌سرند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو چون غرم رفتستی اندر کمر</p></div>
<div class="m2"><p>پر از داوری دل پر از کینه سر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گریزان و لشکر پس اندر دمان</p></div>
<div class="m2"><p>بدام اندر آیی همی بی‌گمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین داد پاسخ سرافراز طوس</p></div>
<div class="m2"><p>که من بر دروغ تو دارم فسوس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پی کین تو افگندی اندر جهان</p></div>
<div class="m2"><p>ز بهر سیاوش میان مهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برین گونه تا چند گویی دروغ</p></div>
<div class="m2"><p>دروغت بر ما نگیرد فروغ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>علف تنگ بود اندران رزمگاه</p></div>
<div class="m2"><p>ازان بر هماون کشیدم سپاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنون آگهی شد بشاه جهان</p></div>
<div class="m2"><p>بیاید زمان تا زمان ناگهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بزرگان لشکر شدند انجمن</p></div>
<div class="m2"><p>چو دستان و چون رستم پیلتن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو جنبیدن شاه کردم درست</p></div>
<div class="m2"><p>نمانم بتوران بر و بوم و رست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کنون کامدی کار مردان ببین</p></div>
<div class="m2"><p>نه گاه فریبست و روز کمین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو بشنید پیران ز هر سو سپاه</p></div>
<div class="m2"><p>فرستاد و بگرفت بر کوه راه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بهر سو ز توران بیامد گروه</p></div>
<div class="m2"><p>سپاه انجمن کرد بر گرد کوه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بریشان چو راه علف تنگ شد</p></div>
<div class="m2"><p>سپهبد سوی چارهٔ جنگ شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنین گفت هومان بپیران گرد</p></div>
<div class="m2"><p>که ما را پی کوه باید سپرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی جنگ سازیم کایرانیان</p></div>
<div class="m2"><p>نبندند ازین پس بکینه میان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدو گفت پیران که بر ماست باد</p></div>
<div class="m2"><p>نکردست با باد کس رزم یاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز جنگ پیاده بپیچید سر</p></div>
<div class="m2"><p>شود تیره دیدار پرخاشخر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو راه علف تنگ شد بر سپاه</p></div>
<div class="m2"><p>کسی کوه خارا ندارد نگاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه لشکر آید بزنهار ما</p></div>
<div class="m2"><p>ازین پس نجویند پیکار ما</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بریشان کنون جای بخشایش است</p></div>
<div class="m2"><p>نه هنگام پیکار و آرایش است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رسید این سگالش بگودرز و طوس</p></div>
<div class="m2"><p>سر سرکشان خیره گشت از فسوس</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنین گفت با طوس گودرز پیر</p></div>
<div class="m2"><p>که ما را کنون جنگ شد ناگزیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سه روز ار بود خوردنی بیش نیست</p></div>
<div class="m2"><p>ز یکسو گشاده رهی پیش نیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه خورد و نه چیز و نه بار و بنه</p></div>
<div class="m2"><p>چنین چند باشد سپه گرسنه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کنون چون شود روی خورشید زرد</p></div>
<div class="m2"><p>پدید آید آن چادر لاژورد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بباید گزیدن سواران مرد</p></div>
<div class="m2"><p>ز بالا شدن سوی دشت نبرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسان شبیخون یکی رزم سخت</p></div>
<div class="m2"><p>بسازیم تا چون بود یار بخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر یک بیک تن بکشتن دهیم</p></div>
<div class="m2"><p>وگر تاج گردنکشان برنهیم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنین است فرجام آوردگاه</p></div>
<div class="m2"><p>یکی خاک یابد یکی تاج و گاه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز گودرز بشنید طوس این سخن</p></div>
<div class="m2"><p>سرش گشت پردرد و کین کهن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز یک سوی لشکر به بیژن سپرد</p></div>
<div class="m2"><p>دگر سو بشیدوش و خراد گرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>درفش خجسته بگستهم داد</p></div>
<div class="m2"><p>بسی پند و اندرزها کرد یاد</p></div></div>