---
title: >-
    بخش ۲۵
---
# بخش ۲۵

<div class="b" id="bn1"><div class="m1"><p>وزان روی رستم به ایوان رسید</p></div>
<div class="m2"><p>مر او را بران گونه دستان بدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زواره فرامرز گریان شدند</p></div>
<div class="m2"><p>ازان خستگیهاش بریان شدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سربر همی کند رودابه موی</p></div>
<div class="m2"><p>بر آواز ایشان همی خست روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زواره به زودی گشادش میان</p></div>
<div class="m2"><p>ازو برکشیدند ببر بیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرانکس که دانا بد از کشورش</p></div>
<div class="m2"><p>نشستند یکسر همه بر درش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بفرمود تا رخش را پیش اوی</p></div>
<div class="m2"><p>ببردند و هرکس که بد چاره‌جوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرانمایه دستان همی کند موی</p></div>
<div class="m2"><p>بران خستگیها بمالید روی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی گفت من زنده با پیر سر</p></div>
<div class="m2"><p>بدیدم بدین سان گرامی پسر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدو گفت رستم کزین غم چه سود</p></div>
<div class="m2"><p>که این ز آسمان بودنی کار بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پیش است کاری که دشوارتر</p></div>
<div class="m2"><p>وزو جان من پر ز تیمارتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که هرچند من بیش پوزش کنم</p></div>
<div class="m2"><p>که این شیردل را فروزش کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نجوید همی جز همه ناخوشی</p></div>
<div class="m2"><p>به گفتار و کردار و گردنکشی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رسیدم ز هر سو به گرد جهان</p></div>
<div class="m2"><p>خبر یافتم ز آشکار و نهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرفتم کمربند دیو سپید</p></div>
<div class="m2"><p>زدم بر زمین همچو یک شاخ بید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نتابم همی سر ز اسفندیار</p></div>
<div class="m2"><p>ازان زور و آن بخشش کارزار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خدنگم ز سندان گذر یافتی</p></div>
<div class="m2"><p>زبون داشتی گر سپر یافتی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زدم چند بر گبر اسفندیار</p></div>
<div class="m2"><p>گراینده دست مرا داشت خوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان تیغ من گر بدیدی پلنگ</p></div>
<div class="m2"><p>نهان داشتی خویشتن زیر سنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نبرد همی جوشن اندر برش</p></div>
<div class="m2"><p>نه آن پارهٔ پرنیان بر سرش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپاسم ز یزدان که شب تیره شد</p></div>
<div class="m2"><p>دران تیرگی چشم او خیره شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برستم من از چنگ آن اژدها</p></div>
<div class="m2"><p>ندانم کزین خسته آیم رها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه اندیشم اکنون جزین نیست رای</p></div>
<div class="m2"><p>که فردا بگردانم از رخش پای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به جایی شوم کو نیاید نشان</p></div>
<div class="m2"><p>به زابلستان گر کند سرفشان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سرانجام ازان کار سیر آید او</p></div>
<div class="m2"><p>اگرچه ز بد سیر دیر آید او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدو گفت زال ای پسر گوش دار</p></div>
<div class="m2"><p>سخن چون به یاد آوری هوش دار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همه کارهای جهان را در است</p></div>
<div class="m2"><p>مگر مرگ کانرا دری دیگر است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی چاره دانم من این را گزین</p></div>
<div class="m2"><p>که سیمرغ را یار خوانم برین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر او باشدم زین سخن رهنمای</p></div>
<div class="m2"><p>بماند به ما کشور و بوم و جای</p></div></div>