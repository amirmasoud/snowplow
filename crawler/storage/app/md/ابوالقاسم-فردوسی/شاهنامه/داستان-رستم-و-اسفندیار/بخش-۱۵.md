---
title: >-
    بخش ۱۵
---
# بخش ۱۵

<div class="b" id="bn1"><div class="m1"><p>چنین گفت با رستم اسفندیار</p></div>
<div class="m2"><p>که ای نیک دل مهتر نامدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ایدون شنیدستم از بخردان</p></div>
<div class="m2"><p>بزرگان و بیداردل موبدان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان برگذشته نیاکان تو</p></div>
<div class="m2"><p>سرافراز و دین‌دار و پاکان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که دستان بدگوهر دیوزاد</p></div>
<div class="m2"><p>به گیتی فزونی ندارد نژاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فراوان ز سامش نهان داشتند</p></div>
<div class="m2"><p>همی رستخیز جهان داشتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنش تیره بد موی و رویش سپید</p></div>
<div class="m2"><p>چو دیدش دل سام شد ناامید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بفرمود تا پیش دریا برند</p></div>
<div class="m2"><p>مگر مرغ و ماهی ورا بشکرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیامد بگسترد سیمرغ پر</p></div>
<div class="m2"><p>ندید اندرو هیچ آیین و فر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببردش به جایی که بودش کنام</p></div>
<div class="m2"><p>ز دستان مر او را خورش بود کام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر چند سیمرغ ناهار بود</p></div>
<div class="m2"><p>تن زال پیش اندرش خوار بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بینداختش پس به پیش کنام</p></div>
<div class="m2"><p>به دیدار او کس نبد شادکام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی خورد افگنده مردار اوی</p></div>
<div class="m2"><p>ز جامه برهنه تن خوار اوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو افگند سیمرغ بر زال مهر</p></div>
<div class="m2"><p>برو گشت زین گونه چندی سپهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازان پس که مردار چندی چشید</p></div>
<div class="m2"><p>برهنه سوی سیستانش کشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پذیرفت سامش ز بی‌بچگی</p></div>
<div class="m2"><p>ز نادانی و دیوی و غرچگی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خجسته بزرگان و شاهان من</p></div>
<div class="m2"><p>نیای من و نیکخواهان من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ورا برکشیدند و دادند چیز</p></div>
<div class="m2"><p>فراوان برین سال بگذشت نیز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی سرو بد نابسوده سرش</p></div>
<div class="m2"><p>چو با شاخ شد رستم آمد برش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز مردی و بالا و دیدار اوی</p></div>
<div class="m2"><p>به گردون برآمد چنین کار اوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برین گونه ناپارسایی گرفت</p></div>
<div class="m2"><p>ببالید و پس پادشاهی گرفت</p></div></div>