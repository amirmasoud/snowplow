---
title: >-
    بخش ۲۸
---
# بخش ۲۸

<div class="b" id="bn1"><div class="m1"><p>بدانست رستم که لابه به کار</p></div>
<div class="m2"><p>نیاید همی پیش اسفندیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمان را به زه کرد و آن تیر گز</p></div>
<div class="m2"><p>که پیکانش را داده بد آب رز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی راند تیر گز اندر کمان</p></div>
<div class="m2"><p>سر خویش کرده سوی آسمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی گفت کای پاک دادار هور</p></div>
<div class="m2"><p>فزایندهٔ دانش و فر و زور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی بینی این پاک جان مرا</p></div>
<div class="m2"><p>توان مرا هم روان مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که چندین بپیچم که اسفندیار</p></div>
<div class="m2"><p>مگر سر بپیچاند از کارزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو دانی که بیداد کوشد همی</p></div>
<div class="m2"><p>همی جنگ و مردی فروشد همی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بادافره این گناهم مگیر</p></div>
<div class="m2"><p>توی آفرینندهٔ ماه و تیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خودکامه جنگی بدید آن درنگ</p></div>
<div class="m2"><p>که رستم همی دیر شد سوی جنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدو گفت کای سگزی بدگمان</p></div>
<div class="m2"><p>نشد سیر جانت ز تیر و کمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببینی کنون تیر گشتاسپی</p></div>
<div class="m2"><p>دل شیر و پیکان لهراسپی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی تیر بر ترگ رستم بزد</p></div>
<div class="m2"><p>چنان کز کمان سواران سزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تهمتن گز اندر کمان راند زود</p></div>
<div class="m2"><p>بران سان که سیمرغ فرموده بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بزد تیر بر چشم اسفندیار</p></div>
<div class="m2"><p>سیه شد جهان پیش آن نامدار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خم آورد بالای سرو سهی</p></div>
<div class="m2"><p>ازو دور شد دانش و فرهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگون شد سر شاه یزدان‌پرست</p></div>
<div class="m2"><p>بیفتاد چاچی کمانش ز دست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرفته بش و یال اسپ سیاه</p></div>
<div class="m2"><p>ز خون لعل شد خاک آوردگاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنین گفت رستم به اسفندیار</p></div>
<div class="m2"><p>که آوردی آن تخم زفتی به بار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو آنی که گفتی که رویین تنم</p></div>
<div class="m2"><p>بلند آسمان بر زمین بر زنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من از شست تو هشت تیر خدنگ</p></div>
<div class="m2"><p>بخوردم ننالیدم از نام و ننگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به یک تیر برگشتی از کارزار</p></div>
<div class="m2"><p>بخفتی بران بارهٔ نامدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هم‌اکنون به خاک اندر آید سرت</p></div>
<div class="m2"><p>بسوزد دل مهربان مادرت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هم‌انگه سر نامبردار شاه</p></div>
<div class="m2"><p>نگون اندر آمد ز پشت سپاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زمانی همی بود تا یافت هوش</p></div>
<div class="m2"><p>بر خاک بنشست و بگشاد گوش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سر تیر بگرفت و بیرون کشید</p></div>
<div class="m2"><p>همی پر و پیکانش در خون کشید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همانگه به بهمن رسید آگهی</p></div>
<div class="m2"><p>که تیره شد آن فر شاهنشهی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بیامد به پیش پشوتن بگفت</p></div>
<div class="m2"><p>که پیکار ما گشت با درد جفت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تن ژنده پیل اندر آمد به خاک</p></div>
<div class="m2"><p>دل ما ازین درد کردند چاک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برفتد هر دو پیاده دوان</p></div>
<div class="m2"><p>ز پیش سپه تا بر پهلوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدیدند جنگی برش پر ز خون</p></div>
<div class="m2"><p>یکی تیر پرخون به دست اندرون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پشوتن بر و جامه را کرد چاک</p></div>
<div class="m2"><p>خروشان به سر بر همی کرد خاک</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همی گشت بهمن به خاک اندرون</p></div>
<div class="m2"><p>بمالید رخ را بدان گرم خون</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پشوتن همی گفت راز جهان</p></div>
<div class="m2"><p>که داند ز دین‌آوران و مهان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو اسفندیاری که از بهر دین</p></div>
<div class="m2"><p>به مردی برآهیخت شمشیر کین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جهان کرد پاک از بد بت‌پرست</p></div>
<div class="m2"><p>به بد کار هرگز نیازید دست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به روز جوانی هلاک آمدش</p></div>
<div class="m2"><p>سر تاجور سوی خاک آمدش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدی را کزو هست گیتی به درد</p></div>
<div class="m2"><p>پرآزار ازو جان آزاد مرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فراوان برو بگذرد روزگار</p></div>
<div class="m2"><p>که هرگز نبیند بد کارزار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جوانان گرفتندش اندر کنار</p></div>
<div class="m2"><p>همی خون ستردند زان شهریار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پشوتن بروبر همی مویه کرد</p></div>
<div class="m2"><p>رخی پر ز خون و دلی پر ز درد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همی گفت زار ای یل اسفندیار</p></div>
<div class="m2"><p>جهانجوی و از تخمهٔ شهریار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که کند این چنین کوه جنگی ز جای</p></div>
<div class="m2"><p>که افگند شیر ژیان را ز پای</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که کند این پسندیده دندان پیل</p></div>
<div class="m2"><p>که آگند با موج دریای نیل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چه آمد برین تخمه از چشم بد</p></div>
<div class="m2"><p>که بر بدکنش بی‌گمان بد رسد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کجا شد به رزم اندرون ساز تو</p></div>
<div class="m2"><p>کجا شد به بزم آن خوش آواز تو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کجا شد دل و هوش و آیین تو</p></div>
<div class="m2"><p>توانایی و اختر و دین تو</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو کردی جهان را ز بدخواه پاک</p></div>
<div class="m2"><p>نیامدت از پیل وز شیر باک</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کنون آمدت سودمندی به کار</p></div>
<div class="m2"><p>که در خاک بیند ترا روزگار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که نفرین برین تاج و این تخت باد</p></div>
<div class="m2"><p>بدین کوشش بیش و این بخت باد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که چو تو سواری دلیر و جوان</p></div>
<div class="m2"><p>سرافراز و دانا و روشن‌روان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدین سان شود کشته در کارزار</p></div>
<div class="m2"><p>به زاری سرآید برو روزگار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>که مه تاج بادا و مه تخت شاه</p></div>
<div class="m2"><p>مه گشتاسپ و جاماسپ و آن بارگاه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چنین گفت پر دانش اسفندیار</p></div>
<div class="m2"><p>که ای مرد دانای به روزگار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مکن خویشتن پیش من بر تباه</p></div>
<div class="m2"><p>چنین بود بهر من از تاج و گاه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تن کشته را خاک باشد نهال</p></div>
<div class="m2"><p>تو از کشتن من بدین سان منال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کجا شد فریدون و هوشنگ و جم</p></div>
<div class="m2"><p>ز باد آمده باز گردد به دم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همان پاک‌زاده نیاکان ما</p></div>
<div class="m2"><p>گزیده سرافراز و پاکان ما</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>برفتند و ما را سپردند جای</p></div>
<div class="m2"><p>نماند کس اندر سپنجی سرای</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>فراوان بکوشیدم اندر جهان</p></div>
<div class="m2"><p>چه در آشکار و چه اندر نهان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که تا رای یزدان به جای آورم</p></div>
<div class="m2"><p>خرد را بدین رهنمای آورم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو از من گرفت ای سخن روشنی</p></div>
<div class="m2"><p>ز بد بسته شد راه آهرمنی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زمانه بیازید چنگال تیز</p></div>
<div class="m2"><p>نبد زو مرا روزگار گریز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>امید من آنست کاندر بهشت</p></div>
<div class="m2"><p>دل‌افروز من بدرود هرچ کشت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به مردی مرا پور دستان نکشت</p></div>
<div class="m2"><p>نگه کن بدین گز که دارم به مشت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بدین چوب شد روزگارم به سر</p></div>
<div class="m2"><p>ز سیمرغ وز رستم چاره‌گر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>فسونها و نیرنگها زال ساخت</p></div>
<div class="m2"><p>که اروند و بند جهان او شناخت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو اسفندیار این سخن یاد کرد</p></div>
<div class="m2"><p>بپیچید و بگریست رستم به درد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چنین گفت کز دیو ناسازگار</p></div>
<div class="m2"><p>ترا بهره رنج من آمد به کار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چنانست کو گفت یکسر سخن</p></div>
<div class="m2"><p>ز مردی به کژی نیفگند بن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>که تا من به گیتی کمر بسته‌ام</p></div>
<div class="m2"><p>بسی رزم گردنکشان جسته‌ام</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سواری ندیدم چو اسفندیار</p></div>
<div class="m2"><p>زره‌دار با جوشن کارزار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو بیچاره برگشتم از دست اوی</p></div>
<div class="m2"><p>بدیدم کمان و بر و شست اوی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سوی چاره گشتم ز بیچارگی</p></div>
<div class="m2"><p>بدادم بدو سر به یکبارگی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>زمان ورا در کمان ساختم</p></div>
<div class="m2"><p>چو روزش سرآمد بینداختم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گر او را همی روز باز آمدی</p></div>
<div class="m2"><p>مرا کار گز کی فراز آمدی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ازین خاک تیره بباید شدن</p></div>
<div class="m2"><p>به پرهیز یک دم نشاید زدن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>همانست کز گز بهانه منم</p></div>
<div class="m2"><p>وزین تیرگی در فسانه منم</p></div></div>