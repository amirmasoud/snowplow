---
title: >-
    بخش ۱ - آغاز داستان
---
# بخش ۱ - آغاز داستان

<div class="b" id="bn1"><div class="m1"><p>کنون خورد باید می خوشگوار</p></div>
<div class="m2"><p>که می‌بوی مشک آید از جویبار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوا پر خروش و زمین پر ز جوش</p></div>
<div class="m2"><p>خنک آنک دل شاد دارد به نوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درم دارد و نقل و جام نبید</p></div>
<div class="m2"><p>سر گوسفندی تواند برید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا نیست فرخ مر آن را که هست</p></div>
<div class="m2"><p>ببخشای بر مردم تنگدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه بوستان زیر برگ گلست</p></div>
<div class="m2"><p>همه کوه پرلاله و سنبلست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پالیز بلبل بنالد همی</p></div>
<div class="m2"><p>گل از نالهٔ او ببالد همی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو از ابر بینم همی باد و نم</p></div>
<div class="m2"><p>ندانم که نرگس چرا شد دژم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب تیره بلبل نخسپد همی</p></div>
<div class="m2"><p>گل از باد و باران بجنبد همی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخندد همی بلبل از هر دوان</p></div>
<div class="m2"><p>چو بر گل نشیند گشاید زبان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندانم که عاشق گل آمد گر ابر</p></div>
<div class="m2"><p>چو از ابر بینم خروش هژبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدرد همی باد پیراهنش</p></div>
<div class="m2"><p>درفشان شود آتش اندر تنش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به عشق هوا بر زمین شد گوا</p></div>
<div class="m2"><p>به نزدیک خورشید فرمانروا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که داند که بلبل چه گوید همی</p></div>
<div class="m2"><p>به زیر گل اندر چه موید همی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نگه کن سحرگاه تا بشنوی</p></div>
<div class="m2"><p>ز بلبل سخن گفتنی پهلوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی نالد از مرگ اسفندیار</p></div>
<div class="m2"><p>ندارد به جز ناله زو یادگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو آواز رستم شب تیره ابر</p></div>
<div class="m2"><p>بدرد دل و گوش غران هژبر</p></div></div>