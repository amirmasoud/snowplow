---
title: >-
    بخش ۱۴
---
# بخش ۱۴

<div class="b" id="bn1"><div class="m1"><p>نشست از بر رخش چون پیل مست</p></div>
<div class="m2"><p>یکی گرزهٔ گاو پیکر به دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیامد دمان تا به نزدیک آب</p></div>
<div class="m2"><p>سپه را به دیدار او بد شتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرانکس که از لشکر او را بدید</p></div>
<div class="m2"><p>دلش مهر و پیوند او برگزید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی گفت هرکس که این نامدار</p></div>
<div class="m2"><p>نماند به کس جز به سام سوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برین کوههٔ زین که آهنست</p></div>
<div class="m2"><p>همان رخش گویی که آهرمنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر هم نبردش بود ژنده پیل</p></div>
<div class="m2"><p>برافشاند از تارک پیل نیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی مرد ازین سان به گیتی ندید</p></div>
<div class="m2"><p>نه از نامداران پیشین شنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرد نیست اندر سر شهریار</p></div>
<div class="m2"><p>که جوید ازین نامور کارزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برین سان همی از پی تاج و گاه</p></div>
<div class="m2"><p>به کشتن دهد نامداری چو ماه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پیری سوی گنج یازان ترست</p></div>
<div class="m2"><p>به مهر و به دیهیم نازان ترست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی آمد از دور رستم چو شیر</p></div>
<div class="m2"><p>به زیر اندرون اژدهای دلیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو آمد به نزدیک اسفندیار</p></div>
<div class="m2"><p>هم‌انگه پذیره شدش نامدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو گفت رستم که ای پهلوان</p></div>
<div class="m2"><p>نوآیین و نوساز و فرخ جوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرامی نیرزید مهمان تو</p></div>
<div class="m2"><p>چنین بود تا بود پیمان تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سخن هرچ گویم همه یاد گیر</p></div>
<div class="m2"><p>مشو تیز با پیر بر خیره خیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همی خویشتن را بزرگ آیدت</p></div>
<div class="m2"><p>وزین نامداران سترگ آیدت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همانا به مردی سبک داریم</p></div>
<div class="m2"><p>به رای و به دانش تنک داریم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به گیتی چنان دان که رستم منم</p></div>
<div class="m2"><p>فروزندهٔ تخم نیرم منم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بخاید ز من چنگ دیو سپید</p></div>
<div class="m2"><p>بسی جادوان را کنم ناامید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بزرگان که دیدند ببر مرا</p></div>
<div class="m2"><p>همان رخش غران هژبر مرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو کاموس جنگی چو خاقان چین</p></div>
<div class="m2"><p>سواران جنگی و مردان کین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که از پشت زینشان به خم کمند</p></div>
<div class="m2"><p>ربودم سر و پای کردم به بند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نگهدار ایران و توران منم</p></div>
<div class="m2"><p>به هر جای پشت دلیران منم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ازین خواهش من مشو بدگمان</p></div>
<div class="m2"><p>مدان خویشتن برتر از آسمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من از بهر این فر و اورند تو</p></div>
<div class="m2"><p>بجویم همی رای و پیوند تو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نخواهم که چون تو یکی شهریار</p></div>
<div class="m2"><p>تبه دارد از چنگ من روزگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که من سام یل را بخوانم دلیر</p></div>
<div class="m2"><p>کزو بیشه بگذاشتی نره شیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به گیتی منم زو کنون یادگار</p></div>
<div class="m2"><p>دگر شاهزاده یل اسفندیار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بسی پهلوان جهان بوده‌ام</p></div>
<div class="m2"><p>سخنها ز هر گونه بشنوده‌ام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سپاسم ز یزدان که بگذشت سال</p></div>
<div class="m2"><p>بدیدم یکی شاه فرخ همال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که کین خواهد از مرد ناپاک دین</p></div>
<div class="m2"><p>جهانی برو بر کنند آفرین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>توی نامور پرهنر شهریار</p></div>
<div class="m2"><p>به جنگ اندرون افسر کارزار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بخندید از رستم اسفندیار</p></div>
<div class="m2"><p>بدو گفت کای پور سام سوار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شدی تنگدل چون نیامد خرام</p></div>
<div class="m2"><p>نجستم همی زین سخن کام و نام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنین گرم بد روز و راه دراز</p></div>
<div class="m2"><p>نکردم ترا رنجه تندی مساز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همی گفتم از بامداد پگاه</p></div>
<div class="m2"><p>به پوزش بسازم سوی داد راه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به دیدار دستان شوم شادمان</p></div>
<div class="m2"><p>به تو شاد دارم روان یک زمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کنون تو بدین رنج برداشتی</p></div>
<div class="m2"><p>به دشت آمدی خانه بگذاشتی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به آرام بنشین و بردار جام</p></div>
<div class="m2"><p>ز تندی و تیزی مبر هیچ نام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به دست چپ خویش بر جای کرد</p></div>
<div class="m2"><p>ز رستم همی مجلس آرای کرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جهاندیده گفت این نه جای منست</p></div>
<div class="m2"><p>بجایی نشینم که رای منست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به بهمن بفرمود کز دست راست</p></div>
<div class="m2"><p>نشستی بیارای ازان کم سزاست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنین گفت با شاهزاده به خشم</p></div>
<div class="m2"><p>که آیین من بین و بگشای چشم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هنر بین و این نامور گوهرم</p></div>
<div class="m2"><p>که از تخمهٔ سام کنداورم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هنر باید از مرد و فر و نژاد</p></div>
<div class="m2"><p>کفی راد دارد دلی پر ز داد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سزاوار من گر ترا نیست جای</p></div>
<div class="m2"><p>مرا هست پیروزی و هوش و رای</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ازان پس بفرمود فرزند شاه</p></div>
<div class="m2"><p>که کرسی زرین نهد پیش گاه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدان تا گو نامور پهلوان</p></div>
<div class="m2"><p>نشیند بر شهریار جوان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بیامد بران کرسی زر نشست</p></div>
<div class="m2"><p>پر از خشم بویا ترنجی بدست</p></div></div>