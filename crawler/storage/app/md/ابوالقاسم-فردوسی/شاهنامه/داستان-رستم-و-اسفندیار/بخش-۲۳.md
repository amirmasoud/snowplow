---
title: >-
    بخش ۲۳
---
# بخش ۲۳

<div class="b" id="bn1"><div class="m1"><p>بدانگه که رزم یلان شد دراز</p></div>
<div class="m2"><p>همی دیر شد رستم سرفراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زواره بیاورد زان سو سپاه</p></div>
<div class="m2"><p>یکی لشکری داغ‌دل کینه‌خواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ایرانیان گفت رستم کجاست</p></div>
<div class="m2"><p>برین روز بیهوده خامش چراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شما سوی رستم به جنگ آمدید</p></div>
<div class="m2"><p>خرامان به چنگ نهنگ آمدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی دست رستم نخواهید بست</p></div>
<div class="m2"><p>برین رزمگه بر نشاید نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زواره به دشنام لب برگشاد</p></div>
<div class="m2"><p>همی کرد گفتار ناخوب یاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآشفت ازان پور اسفندیار</p></div>
<div class="m2"><p>سواری بد اسپ‌افگن و نامدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوانی که نوش آذرش بود نام</p></div>
<div class="m2"><p>سرافراز و جنگاور و شادکام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برآشفت با سگزی آن نامدار</p></div>
<div class="m2"><p>زبان را به دشنام بگشاد خوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین گفت کآری گو برمنش</p></div>
<div class="m2"><p>به فرمان شاهان کند بدکنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفرمود ما را یل اسفندیار</p></div>
<div class="m2"><p>چنین با سگان ساختن کارزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که پیچد سر از رای و فرمان او</p></div>
<div class="m2"><p>که یارد گذشتن ز پیمان او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر جنگ بر نادرستی کنید</p></div>
<div class="m2"><p>به کار اندرون پیش دستی کنید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ببینید پیکار جنگاوران</p></div>
<div class="m2"><p>به تیغ و سنان و به گرز گران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زواره بفرمود کاندر نهید</p></div>
<div class="m2"><p>سران را ز خون بر سر افسر نهید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زواره بیامد به پیش سپاه</p></div>
<div class="m2"><p>دهاده برآمد ز آوردگاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بکشتند ز ایرانیان بی‌شمار</p></div>
<div class="m2"><p>چو نوش‌آذر آن دید بر ساخت کار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سمند سرافراز را بر نشست</p></div>
<div class="m2"><p>بیامد یکی تیغ هندی به دست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی نامور بود الوای نام</p></div>
<div class="m2"><p>سرافراز و اسپ‌افگن و شادکام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کجا نیزهٔ رستم او داشتی</p></div>
<div class="m2"><p>پس پشت او هیچ نگذاشتی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو از دور نوش‌آذر او را بدید</p></div>
<div class="m2"><p>بزد دست و تیغ از میان برکشید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی تیغ زد بر سر و گردنش</p></div>
<div class="m2"><p>بدو نیمه شد پیل‌پیکر تنش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زواره برانگیخت اسپ نبرد</p></div>
<div class="m2"><p>به تندی به نوش‌آذر آواز کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که او را فگندی کنون پای دار</p></div>
<div class="m2"><p>چو الوای را من نخوانم سوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زواره یکی نیزه زد بر برش</p></div>
<div class="m2"><p>به خاک اندر آمد همانگه سرش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو نوش‌آذر نامور کشته شد</p></div>
<div class="m2"><p>سپه را همه روز برگشته شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برادرش گریان و دل پر ز جوش</p></div>
<div class="m2"><p>جوانی که بد نام او مهرنوش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غمی شد دل مرد شمشیرزن</p></div>
<div class="m2"><p>برانگیخت آن بارهٔ پیلتن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برفت از میان سپه پیش صف</p></div>
<div class="m2"><p>ز درد جگر بر لب آورده کف</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وزان سو فرامرز چون پیل مست</p></div>
<div class="m2"><p>بیامد یکی تیغ هندی به دست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برآویخت با او همی مهرنوش</p></div>
<div class="m2"><p>دو رویه ز لشکر برآمد خروش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گرامی دو پرخاشجوی جوان</p></div>
<div class="m2"><p>یکی شاهزاده دگر پهلوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو شیران جنگی برآشوفتند</p></div>
<div class="m2"><p>همی بر سر یکدگر کوفتند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در آوردگه تیز شد مهرنوش</p></div>
<div class="m2"><p>نبودش همی با فرامرز توش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بزد تیغ بر گردن اسپ خویش</p></div>
<div class="m2"><p>سر بادپای اندرافگند پیش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فرامرز کردش پیاده تباه</p></div>
<div class="m2"><p>ز خون لعل شد خاک آوردگاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو بهمن برادرش را کشته دید</p></div>
<div class="m2"><p>زمین زیر او چون گل آغشته دید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بیامد دوان نزد اسفندیار</p></div>
<div class="m2"><p>به جایی که بود آتش کارزار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدو گفت کای نره شیر ژیان</p></div>
<div class="m2"><p>سپاهی به جنگ آمد از سگزیان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دو پور تو نوش‌آذر و مهرنوش</p></div>
<div class="m2"><p>به خواری به سگزی سپردند هوش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو اندر نبردی و ما پر ز درد</p></div>
<div class="m2"><p>جوانان و کی‌زادگان زیر گرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برین تخمه این ننگ تا جاودان</p></div>
<div class="m2"><p>بماند ز کردار نابخردان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دل مرد بیدارتر شد ز خشم</p></div>
<div class="m2"><p>پر از تاب مغز و پر از آب چشم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به رستم چنین گفت کای بدنشان</p></div>
<div class="m2"><p>چنین بود پیمان گردنکشان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو گفتی که لشکر نیارم به جنگ</p></div>
<div class="m2"><p>ترا نیست آرایش نام و ننگ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نداری ز من شرم وز کردگار</p></div>
<div class="m2"><p>نترسی که پرسند روز شمار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ندانی که مردان پیمان‌شکن</p></div>
<div class="m2"><p>ستوده نباشد بر انجمن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دو سگزی دو پور مرا کشته‌اند</p></div>
<div class="m2"><p>بران خیرگی باز برگشته‌اند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو بشنید رستم غمی گشت سخت</p></div>
<div class="m2"><p>بلرزید برسان شاخ درخت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به جان و سر شاه سوگند خورد</p></div>
<div class="m2"><p>به خورشید و شمشیر و دشت نبرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>که من جنگ هرگز نفرموده‌ام</p></div>
<div class="m2"><p>کسی کین چنین کرد نستوده‌ام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ببندم دو دست برادر کنون</p></div>
<div class="m2"><p>گر او بود اندر بدی رهنمون</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فرامرز را نیز بسته دو دست</p></div>
<div class="m2"><p>بیارم بر شاه یزدان‌پرست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به خون گرانمایگانشان بکش</p></div>
<div class="m2"><p>مشوران ازین رای بیهوده هش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چنین گفت با رستم اسفندیار</p></div>
<div class="m2"><p>که بر کین طاوس نر خون مار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بریزیم ناخوب و ناخوش بود</p></div>
<div class="m2"><p>نه آیین شاهان سرکش بود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو ای بدنشان چارهٔ خویش ساز</p></div>
<div class="m2"><p>که آمد زمانت به تنگی فراز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر رخش با هردو رانت به تیر</p></div>
<div class="m2"><p>برآمیزم اکنون چو با آب شیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بدان تا کس از بندگان زین سپس</p></div>
<div class="m2"><p>نجویند کین خداوند کس</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>وگر زنده مانی ببندمت چنگ</p></div>
<div class="m2"><p>به نزدیک شاهت برم بی‌درنگ</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بدو گفت رستم کزین گفت و گوی</p></div>
<div class="m2"><p>چه باشد مگر کم شود آبروی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به یزدان پناه و به یزدان گرای</p></div>
<div class="m2"><p>که اویست بر نیک و بد رهنمای</p></div></div>