---
title: >-
    بخش ۱۷
---
# بخش ۱۷

<div class="b" id="bn1"><div class="m1"><p>چنان دید گودرز یک شب به خواب</p></div>
<div class="m2"><p>که ابری برآمد ز ایران پرآب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بران ابر باران خجسته سروش</p></div>
<div class="m2"><p>به گودرز گفتی که بگشای گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خواهی که یابی ز تنگی رها</p></div>
<div class="m2"><p>وزین نامور ترک نر اژدها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به توران یکی نامداری نوست</p></div>
<div class="m2"><p>کجا نام آن شاه کیخسروست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز پشت سیاوش یکی شهریار</p></div>
<div class="m2"><p>هنرمند و از گوهر نامدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین تخمه از گوهر کیقباد</p></div>
<div class="m2"><p>ز مادر سوی تور دارد نژاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو آید به ایران پی فرخش</p></div>
<div class="m2"><p>ز چرخ آنچ پرسد دهد پاسخش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان را ببندد به کین پدر</p></div>
<div class="m2"><p>کند کشور تور زیر و زبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دریای قلزم به جوش آرد آب</p></div>
<div class="m2"><p>نخارد سر از کین افراسیاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه ساله در جوشن کین بود</p></div>
<div class="m2"><p>شب و روز در جنگ بر زین بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز گردان ایران و گردنکشان</p></div>
<div class="m2"><p>نیابد جز از گیو ازو کس نشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین است فرمان گردان سپهر</p></div>
<div class="m2"><p>بدو دارد از داد گسترده مهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو از خواب گودرز بیدار شد</p></div>
<div class="m2"><p>نیایش کنان پیش دادار شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بمالید بر خاک ریش سپید</p></div>
<div class="m2"><p>ز شاه جهاندار شد پرامید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو خورشید پیدا شد از پشت زاغ</p></div>
<div class="m2"><p>برآمد به کردار زرین چراغ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپهبد نشست از بر تخت عاج</p></div>
<div class="m2"><p>بیاراست ایوان به کرسی ساج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پر اندیشه مر گیو را پیش خواند</p></div>
<div class="m2"><p>وزان خواب چندی سخنها براند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدو گفت فرخ پی و روز تو</p></div>
<div class="m2"><p>همان اختر گیتی افروز تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو تا زادی از مادر به آفرین</p></div>
<div class="m2"><p>پر از آفرین شد سراسر زمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به فرمان یزدان خجسته سروش</p></div>
<div class="m2"><p>مرا روی بنمود در خواب دوش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشسته بر ابری پر از باد و نم</p></div>
<div class="m2"><p>بشستی جهان را سراسر ز غم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا دید و گفت این همه غم چراست</p></div>
<div class="m2"><p>جهانی پر از کین و بی‌نم چراست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ازیرا که بی‌فر و برزست شاه</p></div>
<div class="m2"><p>ندارد همی راه شاهان نگاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو کیخسرو آید ز توران زمین</p></div>
<div class="m2"><p>سوی دشمنان افگند رنج و کین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نبیند کس او را ز گردان نیو</p></div>
<div class="m2"><p>مگر نامور پور گودرز گیو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنین کرد بخشش سپهر بلند</p></div>
<div class="m2"><p>که از تو گشاید غم و رنج بند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همی نام جستی میان دو صف</p></div>
<div class="m2"><p>کنون نام جاویدت آمد به کف</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که تا در جهان مردمست و سخن</p></div>
<div class="m2"><p>چنین نام هرگز نگردد کهن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زمین را همان با سپهر بلند</p></div>
<div class="m2"><p>به دست تو خواهد گشادن ز بند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به رنجست گنج و به نامست رنج</p></div>
<div class="m2"><p>همانا که نامت به آید ز گنج</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر جاودانه نمانی بجای</p></div>
<div class="m2"><p>همی نام به زین سپنجی سرای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جهان را یکی شهریار آوری</p></div>
<div class="m2"><p>درخت وفا را به بار آوری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدو گفت گیو ای پدر بنده‌ام</p></div>
<div class="m2"><p>بکوشم به رای تو تا زنده‌ام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خریدارم این را گر آید بجای</p></div>
<div class="m2"><p>به فرخنده نام و پی رهنمای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به ایوان شد و ساز رفتن گرفت</p></div>
<div class="m2"><p>ز خواب پدر مانده اندر شگفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو خورشید رخشنده آمد پدید</p></div>
<div class="m2"><p>زمین شد بسان گل شنبلید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بیامد کمربسته گیو دلیر</p></div>
<div class="m2"><p>یکی بارکش بادپایی به زیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به گودرز گفت ای جهان پهلوان</p></div>
<div class="m2"><p>دلیر و سرافراز و روشن روان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کمندی و اسپی مرا یار بس</p></div>
<div class="m2"><p>نشاید کشیدن بدان مرز کس</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو مردم برم خواستار آیدم</p></div>
<div class="m2"><p>ازان پس مگر کارزار آیدم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مرا دشت و کوهست یک چند جای</p></div>
<div class="m2"><p>مگر پیشم آید یکی رهنمای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به پیرزو بخت جهان پهلوان</p></div>
<div class="m2"><p>نیایم جز از شاد و روشن روان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو مر بیژن خرد را در کنار</p></div>
<div class="m2"><p>بپرور نگهدارش از روزگار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ندانم که دیدار باشد جزین</p></div>
<div class="m2"><p>که داند چنین جز جهان آفرین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو پدرود باش و مرا یاد دار</p></div>
<div class="m2"><p>روان را ز درد من آزاد دار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو شویی ز بهر پرستش رخان</p></div>
<div class="m2"><p>به من بر جهان آفرین را بخوان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مگر باشدم دادگر رهنمای</p></div>
<div class="m2"><p>به نزدیک آن نامور کدخدای</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به فرمان بیاراست و آمد برون</p></div>
<div class="m2"><p>پدر دل پر از درد و رخ پر ز خون</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پدر پیر سر بود و برنا دلیر</p></div>
<div class="m2"><p>دهن جنگ را باز کرده چو شیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ندانست کاو باز بیند پسر</p></div>
<div class="m2"><p>ز رفتن دلش بود زیر و زبر</p></div></div>