---
title: >-
    بخش ۱۸
---
# بخش ۱۸

<div class="b" id="bn1"><div class="m1"><p>بسا رنجها کز جهان دیده‌اند</p></div>
<div class="m2"><p>ز بهر بزرگی پسندیده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرانجام بستر جز از خاک نیست</p></div>
<div class="m2"><p>ازو بهره زهرست و تریاک نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دانی که ایدر نمانی دراز</p></div>
<div class="m2"><p>به تارک چرا بر نهی تاج آز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همان آز را زیر خاک آوری</p></div>
<div class="m2"><p>سرش را سر اندر مغاک آوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا زین جهان شادمانی بس است</p></div>
<div class="m2"><p>کجا رنج تو بهر دیگر کس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو رنجی و آسان دگر کس خورد</p></div>
<div class="m2"><p>سوی گور و تابوت تو ننگرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برو نیز شادی سرآید همی</p></div>
<div class="m2"><p>سرش زیر گرد اندر آید همی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز روز گذر کردن اندیشه کن</p></div>
<div class="m2"><p>پرستیدن دادگر پیشه کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بترس از خدا و میازار کس</p></div>
<div class="m2"><p>ره رستگاری همین است و بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنون ای خردمند بیدار دل</p></div>
<div class="m2"><p>مشو در گمان پای درکش ز گل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترا کردگارست پروردگار</p></div>
<div class="m2"><p>توی بنده و کردهٔ کردگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو گردن به اندیشه زیر آوری</p></div>
<div class="m2"><p>ز هستی مکن پرسش و داوری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشاید خور و خواب با آن نشست</p></div>
<div class="m2"><p>که خستو نباشد بیزدان که هست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلش کور باشد سرش بی‌خرد</p></div>
<div class="m2"><p>خردمندش از مردمان نشمرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز هستی نشانست بر آب و خاک</p></div>
<div class="m2"><p>ز دانش منش را مکن در مغاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>توانا و دانا و دارنده اوست</p></div>
<div class="m2"><p>خرد را و جان را نگارنده اوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهان آفرید و مکان و زمان</p></div>
<div class="m2"><p>پی پشهٔ خرد و پیل گران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو سالار ترکان به دل گفت من</p></div>
<div class="m2"><p>به بیشی برآرم سر از انجمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنان شاهزاده جوان را بکشت</p></div>
<div class="m2"><p>ندانست جز گنج و شمشیر پشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم از پشت او روشن کردگار</p></div>
<div class="m2"><p>درختی برآورد یازان به بار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که با او بگفت آنک جز تو کس است</p></div>
<div class="m2"><p>که اندر جهان کردگار او بس است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خداوند خورشید و کیوان و ماه</p></div>
<div class="m2"><p>کزویست پیروزی و دستگاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خداوند هستی و هم راستی</p></div>
<div class="m2"><p>نخواهد ز تو کژی و کاستی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جز از رای و فرمان او راه نیست</p></div>
<div class="m2"><p>خور و ماه ازین دانش آگاه نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پسر را بفرمود گودرز پیر</p></div>
<div class="m2"><p>به توران شدن کار را ناگریز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به فرمان او گیو بسته میان</p></div>
<div class="m2"><p>بیامد به کردار شیر ژیان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همی تاخت تا مرز توران رسید</p></div>
<div class="m2"><p>هر آنکس که در راه تنها بدید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زبان را به ترکی بیاراستی</p></div>
<div class="m2"><p>ز کیخسرو از وی نشان خواستی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو گفتی ندارم ز شاه آگهی</p></div>
<div class="m2"><p>تنش را ز جان زود کردی تهی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به خم کمندش بیاویختی</p></div>
<div class="m2"><p>سبک از برش خاک بربیختی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدان تا نداند کسی راز او</p></div>
<div class="m2"><p>همان نشنود نام و آواز او</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی را همی برد با خویشتن</p></div>
<div class="m2"><p>ورا رهنمون بود زان انجمن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همی رفت بیدار با او به راه</p></div>
<div class="m2"><p>برو راز نگشاد تا چندگاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدو گفت روزی که اندر جهان</p></div>
<div class="m2"><p>سخن پرسم از تو یکی در نهان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر ایدونک یابم ز تو راستی</p></div>
<div class="m2"><p>بشویی به دانش دل از کاستی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ببخشم ترا هرچ خواهی ز من</p></div>
<div class="m2"><p>ندارم دریغ از تو پرمایه تن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنین داد پاسخ که دانش بسست</p></div>
<div class="m2"><p>ولیکن پراگنده با هر کسست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر زانک پرسیم هست آگهی</p></div>
<div class="m2"><p>ز پاسخ زبان را نیابی تهی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدو گفت کیخسرو اکنون کجاست</p></div>
<div class="m2"><p>بباید به من برگشادنت راست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنین داد پاسخ که نشنیده‌ام</p></div>
<div class="m2"><p>چنین نام هرگز نپرسیده‌ام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو پاسخ چنین یافت از رهنمون</p></div>
<div class="m2"><p>بزد تیغ و انداختش سرنگون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به توران همی رفت چون بیهشان</p></div>
<div class="m2"><p>مگر یابد از شاه جایی نشان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنین تا برآمد برین هفت سال</p></div>
<div class="m2"><p>میان سوده از تیغ و بند دوال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خورش گور و پوشش هم از چرم گور</p></div>
<div class="m2"><p>گیا خوردن باره و آب شور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همی گشت گرد بیابان و کوه</p></div>
<div class="m2"><p>به رنج و به سختی و دور از گروه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چنان بد که روزی پراندیشه بود</p></div>
<div class="m2"><p>به پیشش یکی بارور بیشه بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدان مرغزار اندر آمد دژم</p></div>
<div class="m2"><p>جهان خرم و مرد را دل به غم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زمین سبز و چشمه پر از آب دید</p></div>
<div class="m2"><p>همی جای آرامش و خواب دید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فرود آمد و اسپ را برگذاشت</p></div>
<div class="m2"><p>بخفت و همی بر دل اندیشه داشت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همی گفت مانا که دیو پلید</p></div>
<div class="m2"><p>بر پهلوان بد که آن خواب دید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز کیخسرو ایدر نبینم نشان</p></div>
<div class="m2"><p>چه دارم همی خویشتن را کشان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کنون گر به رزم‌اند یاران من</p></div>
<div class="m2"><p>به بزم اندرون غمگساران من</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>یکی نامجوی و یکی شادروز</p></div>
<div class="m2"><p>مرا بخت بر گنبد افشاند گوز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همی برفشانم به خیره روان</p></div>
<div class="m2"><p>خمیدست پشتم چو خم کمان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همانا که خسرو ز مادر نزاد</p></div>
<div class="m2"><p>وگر زاد دادش زمانه به باد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز جستن مرا رنج و سختیست بهر</p></div>
<div class="m2"><p>انوشه کسی کاو بمیرد به زهر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سرش پر ز غم گرد آن مرغزار</p></div>
<div class="m2"><p>همی گشت شه را کنان خواستار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یکی چشمه‌ای دید تابان ز دور</p></div>
<div class="m2"><p>یکی سرو بالا دل آرام پور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یکی جام پر می گرفته به چنگ</p></div>
<div class="m2"><p>به سر بر زده دستهٔ بوی و رنگ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز بالای او فرهٔ ایزدی</p></div>
<div class="m2"><p>پدید آمد و رایت بخردی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تو گفتی منوچهر بر تخت عاج</p></div>
<div class="m2"><p>نشستست بر سر ز پیروزه تاج</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همی بوی مهر آمد از روی او</p></div>
<div class="m2"><p>همی زیب تاج آمد از موی او</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به دل گفت گیو این به جز شاه نیست</p></div>
<div class="m2"><p>چنین چهره جز در خور گاه نیست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>پیاده بدو تیز بنهاد روی</p></div>
<div class="m2"><p>چو تنگ اندر آمد گو شاه‌جوی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گره سست شد بر در رنج او</p></div>
<div class="m2"><p>پدید آمد آن نامور گنج او</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو کیخسرو از چشمه او را بدید</p></div>
<div class="m2"><p>بخندید و شادان دلش بردمید</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به دل گفت کاین گرد جز گیو نیست</p></div>
<div class="m2"><p>بدین مرز خود زین نشان نیونیست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مرا کرد خواهد همی خواستار</p></div>
<div class="m2"><p>به ایران برد تا کند شهریار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو آمد برش گیو بردش نماز</p></div>
<div class="m2"><p>بدو گفت کای نامور سرافراز</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>برانم که پور سیاوش توی</p></div>
<div class="m2"><p>ز تخم کیانی و کیخسروی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چنین داد پاسخ ورا شهریار</p></div>
<div class="m2"><p>که تو گیو گودرزی ای نامدار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بدو گفت گیو ای سر راستان</p></div>
<div class="m2"><p>ز گودرز با تو که زد داستان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ز کشواد و گیوت که داد آگهی</p></div>
<div class="m2"><p>که با خرمی بادی و فرهی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بدو گفت کیخسرو ای شیر مرد</p></div>
<div class="m2"><p>مرا مادر این از پدر یاد کرد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>که از فر یزدان گشادی سخن</p></div>
<div class="m2"><p>بدانگه که اندرزش آمد به بن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>همی گفت با نامور مادرم</p></div>
<div class="m2"><p>کز ایدر چه آید ز بد بر سرم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سرانجام کیخسرو آید پدید</p></div>
<div class="m2"><p>بجا آورد بندها را کلید</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بدانگه که گردد جهاندار نیو</p></div>
<div class="m2"><p>ز ایران بیاید سرافراز گیو</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مر او را سوی تخت ایران برد</p></div>
<div class="m2"><p>بر نامداران و شیران برد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>جهان را به مردی به پای آورد</p></div>
<div class="m2"><p>همان کین ما را بجای آورد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بدو گفت گیو ای سر سرکشان</p></div>
<div class="m2"><p>ز فر بزرگی چه داری نشان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نشان سیاوش پدیدار بود</p></div>
<div class="m2"><p>چو بر گلستان نقطهٔ قار بود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>تو بگشای و بنمای بازو به من</p></div>
<div class="m2"><p>نشان تو پیداست بر انجمن</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>برهنه تن خویش بنمود شاه</p></div>
<div class="m2"><p>نگه کرد گیو آن نشان سیاه</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>که میراث بود از گه کیقباد</p></div>
<div class="m2"><p>درستی بدان بد کیان را نژاد</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو گیو آن نشان دید بردش نماز</p></div>
<div class="m2"><p>همی ریخت آب و همی گفت راز</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گرفتش به بر شهریار زمین</p></div>
<div class="m2"><p>ز شادی برو بر گرفت آفرین</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>از ایران بپرسید و ز تخت و گاه</p></div>
<div class="m2"><p>ز گودرز وز رستم نیک‌خواه</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بدو گفت گیو ای جهاندار کی</p></div>
<div class="m2"><p>سرافراز و بیدار و فرخنده پی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>جهاندار دارندهٔ خوب و زشت</p></div>
<div class="m2"><p>مراگر نمودی سراسر بهشت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>همان هفت کشور به شاهنشهی</p></div>
<div class="m2"><p>نهاد بزرگی و تاج مهی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>نبودی دل من بدین خرمی</p></div>
<div class="m2"><p>که روی تو دیدم به توران ز می</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>که داند به گیتی که من زنده‌ام</p></div>
<div class="m2"><p>به خاکم و گر بآتش افگنده‌ام</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>سپاس از جهاندار کاین رنج سخت</p></div>
<div class="m2"><p>به شادی و خوبی سرآورد بخت</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>برفتند زان بیشه هر دو به راه</p></div>
<div class="m2"><p>بپرسید خسرو ز کاووس شاه</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>وزان هفت ساله غم و درد او</p></div>
<div class="m2"><p>ز گستردن و خواب وز خورد او</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>همی گفت با شاه یکسر سخن</p></div>
<div class="m2"><p>که دادار گیتی چه افگند بن</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>همان خواب گودرز و رنج دراز</p></div>
<div class="m2"><p>خور و پوشش و درد و آرام و ناز</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ز کاووس کش سال بفگند فر</p></div>
<div class="m2"><p>ز درد پسر گشت بی پای و پر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ز ایران پراکنده شد رنگ و بوی</p></div>
<div class="m2"><p>سراسر به ویرانی آورد روی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>دل خسرو از درد و رنجش بسوخت</p></div>
<div class="m2"><p>به کردار آتش رخش برفروخت</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بدو گفت کاکنون ز رنج دراز</p></div>
<div class="m2"><p>ترا بردهد بخت آرام و ناز</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>مرا چون پدر باش و با کس مگوی</p></div>
<div class="m2"><p>ببین تا زمانه چه آرد به روی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>سپهبد نشست از بر اسپ گیو</p></div>
<div class="m2"><p>پیاده همی رفت بر پیش نیو</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>یکی تیغ هندی گرفته به چنگ</p></div>
<div class="m2"><p>هر آنکس که پیش آمدی بی‌درنگ</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>زدی گیو بیدار دل گردنش</p></div>
<div class="m2"><p>به زیر گل و خاک کردی تنش</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>برفتند سوی سیاووش گرد</p></div>
<div class="m2"><p>چو آمد دو تن را دل و هوش گرد</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>فرنگیس را نیز کردند یار</p></div>
<div class="m2"><p>نهانی بران بر نهادند کار</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>که هر سه به راه اندر آرند روی</p></div>
<div class="m2"><p>نهان از دلیران پرخاشجوی</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>فرنگیس گفت ار درنگ آوریم</p></div>
<div class="m2"><p>جهان بر دل خویش تنگ آوریم</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ازین آگهی یابد افراسیاب</p></div>
<div class="m2"><p>نسازد بخورد و نیازد به خواب</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بیاید به کردار دیو سپید</p></div>
<div class="m2"><p>دل از جان شیرین شود ناامید</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>یکی را ز ما زنده اندر جهان</p></div>
<div class="m2"><p>نبیند کسی آشکار و نهان</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>جهان پر ز بدخواه و پردشمنست</p></div>
<div class="m2"><p>همه مرز ما جای آهرمنست</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>تو ای بافرین شاه فرزند من</p></div>
<div class="m2"><p>نگر تا نیوشی یکی پند من</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>که گر آگهی یابد آن مرد شوم</p></div>
<div class="m2"><p>برانگیزد آتش ز آباد بوم</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>یکی مرغزارست ز ایدر نه دور</p></div>
<div class="m2"><p>به یکسو ز راه سواران تور</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>همان جویبارست و آب روان</p></div>
<div class="m2"><p>که از دیدنش تازه گردد روان</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>تو بر گیر زین و لگام سیاه</p></div>
<div class="m2"><p>برو سوی آن مرغزاران پگاه</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چو خورشید بر تیغ گنبد شود</p></div>
<div class="m2"><p>گه خواب و خورد سپهبد شود</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>گله هرچ هست اندر آن مرغزار</p></div>
<div class="m2"><p>به آبشخور آید سوی جویبار</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>به بهزاد بنمای زین و لگام</p></div>
<div class="m2"><p>چو او رام گردد تو بگذار گام</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چو آیی برش نیک بنمای چهر</p></div>
<div class="m2"><p>بیارای و ببسای رویش به مهر</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>سیاوش چو گشت از جهان ناامید</p></div>
<div class="m2"><p>برو تیره شد روی روز سپید</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>چنین گفت شبرنگ بهزاد را</p></div>
<div class="m2"><p>که فرمان مبر زین سپس باد را</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>همی باش بر کوه و در مرغزار</p></div>
<div class="m2"><p>چو کیخسرو آید ترا خواستار</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>ورا بارگی باش و گیتی بکوب</p></div>
<div class="m2"><p>ز دشمن زمین را به نعلت بروب</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>نشست از بر اسپ سالار نیو</p></div>
<div class="m2"><p>پیاده همی رفت بر پیش گیو</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>بدان تند بالا نهادند روی</p></div>
<div class="m2"><p>چنان چون بود مردم چاره‌جوی</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>فسیله چو آمد به تنگی فراز</p></div>
<div class="m2"><p>بخوردند سیراب و گشتند باز</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>نگه کرد بهزاد و کی را بدید</p></div>
<div class="m2"><p>یکی باد سرد از جگر برکشید</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>بدید آن نشست سیاوش پلنگ</p></div>
<div class="m2"><p>رکیب دراز و جناغ خدنگ</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>همی داشت در آبخور پای خویش</p></div>
<div class="m2"><p>از آنجا که بد دست ننهاد پیش</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>چو کیخسرو او را به آرام یافت</p></div>
<div class="m2"><p>بپویید و با زین سوی او شتافت</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بمالید بر چشم او دست و روی</p></div>
<div class="m2"><p>بر و یال ببسود و بشخود موی</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>لگامش بدو داد و زین بر نهاد</p></div>
<div class="m2"><p>بسی از پدر کرد با درد یاد</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>چو بنشست بر باره بفشارد ران</p></div>
<div class="m2"><p>برآمد ز جا آن هیون گران</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>به کردار باد هوا بردمید</p></div>
<div class="m2"><p>بپرید وز گیو شد ناپدید</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>غمی شد دل گیو و خیره بماند</p></div>
<div class="m2"><p>بدان خیرگی نام یزدان بخواند</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>همی گفت کاهرمن چاره‌جوی</p></div>
<div class="m2"><p>یکی بارگی گشت و بنمود روی</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>کنون جان خسرو شد و رنج من</p></div>
<div class="m2"><p>همین رنج بد در جهان گنج من</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>چو یک نیمه ببرید زان کوه شاه</p></div>
<div class="m2"><p>گران کرد باز آن عنان سیاه</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>همی بود تاپیش او رفت گیو</p></div>
<div class="m2"><p>چنین گفت بیدار دل شاه نیو</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>که شاید که اندیشهٔ پهلوان</p></div>
<div class="m2"><p>کنم آشکارا به روشن روان</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>بدو گفت گیو ای شه سرفراز</p></div>
<div class="m2"><p>سزد کاشکارا بود بر تو راز</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>تو از ایزدی فر و برز کیان</p></div>
<div class="m2"><p>به موی اندر آیی ببینی میان</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>بدو گفت زین اسپ فرخ نژاد</p></div>
<div class="m2"><p>یکی بر دل اندیشه آمدت یاد</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>چنین بود اندیشهٔ پهلوان</p></div>
<div class="m2"><p>که اهریمن آمد بر این جوان</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>کنون رفت و رنج مرا باد کرد</p></div>
<div class="m2"><p>دل شاد من سخت ناشاد کرد</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>ز اسپ اندر آمد جهاندیده گیو</p></div>
<div class="m2"><p>همی آفرین خواند بر شاه نیو</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>که روز و شبان بر تو فرخنده باد</p></div>
<div class="m2"><p>سر بدسگالان تو کنده باد</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>که با برز و اورندی و رای و فر</p></div>
<div class="m2"><p>ترا داد داور هنر با گهر</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>ز بالا به ایوان نهادند روی</p></div>
<div class="m2"><p>پراندیشه مغز و روان راه‌جوی</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>چو نزد فرنگیس رفتند باز</p></div>
<div class="m2"><p>سخن رفت چندی ز راه دراز</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>بدان تا نهانی بود کارشان</p></div>
<div class="m2"><p>نباشد کسی آگه از رازشان</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>فرنگیس چون روی بهزاد دید</p></div>
<div class="m2"><p>شد از آب دیده رخش ناپدید</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>دو رخ را به یال و برش بر نهاد</p></div>
<div class="m2"><p>ز درد سیاوش بسی کرد یاد</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>چو آب دو دیده پراگنده کرد</p></div>
<div class="m2"><p>سبک سر سوی گنج آگنده کرد</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>به ایوان یکی گنج بودش نهان</p></div>
<div class="m2"><p>نبد زان کسی آگه اندر جهان</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>یکی گنج آگنده دینار بود</p></div>
<div class="m2"><p>زره بود و یاقوت بسیار بود</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>همان گنج گوپال و برگستوان</p></div>
<div class="m2"><p>همان خنجر و تیغ و گرز گران</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>در گنج بگشاد پیش پسر</p></div>
<div class="m2"><p>پر از خون رخ از درد خسته جگر</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>چنین گفت با گیو کای برده رنج</p></div>
<div class="m2"><p>ببین تا ز گوهر چه خواهی ز گنج</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>ز دینار وز گوهر شاهوار</p></div>
<div class="m2"><p>ز یاقوت وز تاج گوهرنگار</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>ببوسید پیشش زمین پهلوان</p></div>
<div class="m2"><p>بدو گفت کای مهتر بانوان</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>همه پاسبانیم و گنج آن تست</p></div>
<div class="m2"><p>فدی کردن جان و رنج آن تست</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>زمین از تو گردد بهار بهشت</p></div>
<div class="m2"><p>سپهر از تو زاید همی خوب و زشت</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>جهان پیش فرزند تو بنده باد</p></div>
<div class="m2"><p>سر بدسگالانش افگنده باد</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>چو افتاد بر خواسته چشم گیو</p></div>
<div class="m2"><p>گزین کرد درع سیاووش نیو</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>ز گوهر که پرمایه‌تر یافتند</p></div>
<div class="m2"><p>ببردند چندانک برتافتند</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>همان ترگ و پرمایه برگستوان</p></div>
<div class="m2"><p>سلیحی که بود از در پهلوان</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>سر گنج را شاه کرد استوار</p></div>
<div class="m2"><p>به راه بیابان برآراست کار</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>چو این کرده شد برنهادند زین</p></div>
<div class="m2"><p>بران باد پایان باآفرین</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>فرنگیس ترگی به سر بر نهاد</p></div>
<div class="m2"><p>برفتند هر سه به کردار باد</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>سران سوی ایران نهادند گرم</p></div>
<div class="m2"><p>نهانی چنان چون بود نرم نرم</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>بشد شهر یکسر پر از گفت و گوی</p></div>
<div class="m2"><p>که خسرو به ایران نهادست روی</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>نماند این سخن یک زمان در نهفت</p></div>
<div class="m2"><p>کس آمد به نزدیک پیران بگفت</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>که آمد ز ایران سرافراز گیو</p></div>
<div class="m2"><p>به نزدیک بیدار دل شاه نیو</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>سوی شهر ایران نهادند روی</p></div>
<div class="m2"><p>فرنگیس و شاه و گو جنگ‌جوی</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>چو بشنید پیران غمی گشت سخت</p></div>
<div class="m2"><p>بلرزید برسان برگ درخت</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>ز گردان گزین کرد کلباد را</p></div>
<div class="m2"><p>چو نستیهن و گرد پولاد را</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>بفرمود تا ترک سیصد سوار</p></div>
<div class="m2"><p>برفتند تازان بران کارزار</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>سر گیو بر نیزه سازید گفت</p></div>
<div class="m2"><p>فرنگیس را خاک باید نهفت</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>ببندید کیخسرو شوم را</p></div>
<div class="m2"><p>بداختر پی او بر و بوم را</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>سپاهی برین گونه گرد و جوان</p></div>
<div class="m2"><p>برفتند بیدار دو پهلوان</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>فرنگیس با رنج دیده پسر</p></div>
<div class="m2"><p>به خواب اندر آورده بودند سر</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>ز پیمودن راه و رنج شبان</p></div>
<div class="m2"><p>جهانجوی را گیو بد پاسبان</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>دو تن خفته و گیو با رنج و خشم</p></div>
<div class="m2"><p>به راه سواران نهاده دو چشم</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>به برگستوان اندرون اسپ گیو</p></div>
<div class="m2"><p>چنان چون بود ساز مردان نیو</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>زره در بر و بر سرش بود ترگ</p></div>
<div class="m2"><p>دل ارغنده و تن نهاده به مرگ</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>چو از دور گرد سپه را بدید</p></div>
<div class="m2"><p>بزد دست و تیغ از میان برکشید</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>خروشی برآورد برسان ابر</p></div>
<div class="m2"><p>که تاریک شد مغز و چشم هژبر</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>میان سواران بیامد چو گرد</p></div>
<div class="m2"><p>ز پرخاش او خاک شد لاژورد</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>زمانی به خنجر زمانی به گرز</p></div>
<div class="m2"><p>همی ریخت آهن ز بالای برز</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>ازان زخم گوپال گیو دلیر</p></div>
<div class="m2"><p>سران را همی شد سر از جنگ سیر</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>دل گیو خندان شد از زور خشم</p></div>
<div class="m2"><p>که چون چشمه بودیش دریا به چشم</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>ازان پس گرفتندش اندر میان</p></div>
<div class="m2"><p>چنان لشکری همچو شیر ژیان</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>ز نیزه نیستان شد آوردگاه</p></div>
<div class="m2"><p>بپوشید دیدار خورشید و ماه</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>غمی شد دل شیر در نیستان</p></div>
<div class="m2"><p>ز خون نیستان کرد چون میستان</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>ازیشان بیفگند بسیار گیو</p></div>
<div class="m2"><p>ستوه آمدند آن سواران ز نیو</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>به نستیهن گرد کلباد گفت</p></div>
<div class="m2"><p>که این کوه خاراست نه یال و سفت</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>همه خسته و بسته گشتند باز</p></div>
<div class="m2"><p>به نزدیک پیران گردن فراز</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>همه غار و هامون پر از کشته بود</p></div>
<div class="m2"><p>ز خون خاک چون ارغوان گشته بود</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>چو نزدیک کیخسرو آمد دلیر</p></div>
<div class="m2"><p>پر از خون بر و چنگ برسان شیر</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>بدو گفت کای شاه دل شاد دار</p></div>
<div class="m2"><p>خرد را ز اندیشه آزاد دار</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>یکی لشکر آمد بر ما به جنگ</p></div>
<div class="m2"><p>چو کلباد و نستیهن تیز چنگ</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>چنان بازگشتند آن کس که زیست</p></div>
<div class="m2"><p>که بر یال و برشان بباید گریست</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>گذشته ز رستم به ایران سوار</p></div>
<div class="m2"><p>ندانم که با من کند کارزار</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>ازو شاد شد خسرو پاک‌دین</p></div>
<div class="m2"><p>ستودش فراوان و کرد آفرین</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>بخوردند چیزی کجا یافتند</p></div>
<div class="m2"><p>سوی راه بی راه بشتافتند</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>چو ترکان به نزدیک پیران شدند</p></div>
<div class="m2"><p>چنان خسته و زار و گریان شدند</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>برآشفت پیران به کلباد گفت</p></div>
<div class="m2"><p>که چونین شگفتی نشاید نهفت</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>چه کردید با گیو و خسرو کجاست</p></div>
<div class="m2"><p>سخن بر چه سانست برگوی راست</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>بدو گفت کلباد کای پهلوان</p></div>
<div class="m2"><p>به پیش تو گر برگشایم زبان</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>که گیو دلاور به گردان چه کرد</p></div>
<div class="m2"><p>دلت سیر گردد به دشت نبرد</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>فراوان به لشکر مرا دیده‌ای</p></div>
<div class="m2"><p>نبرد مرا هم پسندیده‌ای</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>همانا که گوپال بیش از هزار</p></div>
<div class="m2"><p>گرفتی ز دست من آن نامدار</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>سرش ویژه گفتی که سندان شدست</p></div>
<div class="m2"><p>بر و ساعدش پیل دندان شدست</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>من آورد رستم بسی دیده‌ام</p></div>
<div class="m2"><p>ز جنگ آوران نیز بشنیده‌ام</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>به زخمش ندیدم چنین پایدار</p></div>
<div class="m2"><p>نه در کوشش و پیچش کارزار</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>همی هر زمان تیز و جوشان بدی</p></div>
<div class="m2"><p>به نوی چو پیلی خروشان بدی</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>برآشفت پیران بدو گفت بس</p></div>
<div class="m2"><p>که ننگست ازین یاد کردن به کس</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>نه از یک سوارست چندین سخن</p></div>
<div class="m2"><p>تو آهنگ آورد مردان مکن</p></div></div>
<div class="b" id="bn224"><div class="m1"><p>تو رفتی و نستیهن نامور</p></div>
<div class="m2"><p>سپاهی به کردار شیران نر</p></div></div>
<div class="b" id="bn225"><div class="m1"><p>کنون گیو را ساختی پیل مست</p></div>
<div class="m2"><p>میان یلان گشت نام تو پست</p></div></div>
<div class="b" id="bn226"><div class="m1"><p>چو زین یابد افراسیاب آگهی</p></div>
<div class="m2"><p>بیندازد آن تاج شاهنشهی</p></div></div>
<div class="b" id="bn227"><div class="m1"><p>که دو پهلوان دلیر و سوار</p></div>
<div class="m2"><p>چنین لشکری از در کارزار</p></div></div>
<div class="b" id="bn228"><div class="m1"><p>ز پیش سواری نمودید پشت</p></div>
<div class="m2"><p>بسی از دلیران ترکان بکشت</p></div></div>
<div class="b" id="bn229"><div class="m1"><p>گواژه بسی باشدت بافسوس</p></div>
<div class="m2"><p>نه مرد نبردی و گوپال و کوس</p></div></div>