---
title: >-
    بخش ۲۱
---
# بخش ۲۱

<div class="b" id="bn1"><div class="m1"><p>چو با گیو کیخسرو آمد به زم</p></div>
<div class="m2"><p>جهان چند ازو شاد و چندی دژم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوندی به هر سو برافگند گیو</p></div>
<div class="m2"><p>یکی نامه از شاه وز گیو نیو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که آمد ز توران جهاندار شاد</p></div>
<div class="m2"><p>سر تخمهٔ نامور کیقباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرستادهٔ بختیار و سوار</p></div>
<div class="m2"><p>خردمند و بینادل و دوستدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گزین کرد ازان نامداران زم</p></div>
<div class="m2"><p>بگفت آنچ بشنید از بیش و کم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو گفت ایدر برو به اصفهان</p></div>
<div class="m2"><p>بر نیو گودرز کشوادگان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگویش که کیخسرو آمد به زم</p></div>
<div class="m2"><p>که بادی نجست از بر او دژم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی نامه نزدیک کاووس شاه</p></div>
<div class="m2"><p>فرستاده‌ای چست بگرفت راه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیونان کفک افگن بادپای</p></div>
<div class="m2"><p>بجستند برسان آتش ز جای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرستادهٔ گیو روشن روان</p></div>
<div class="m2"><p>نخستین بیامد بر پهلوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیامش همی گفت و نامه بداد</p></div>
<div class="m2"><p>جهان پهلوان نامه بر سر نهاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بهر سیاووش ببارید آب</p></div>
<div class="m2"><p>همی کرد نفرین بر افراسیاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرستاده شد نزد کاووس کی</p></div>
<div class="m2"><p>ز یال هیونان بپالود خوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو آمد به نزدیک کاووس شاه</p></div>
<div class="m2"><p>ز شادی خروش آمد از بارگاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خبر شد به گیتی که فرزند شاه</p></div>
<div class="m2"><p>جهانجوی کیخسرو آمد ز راه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپهبد فرستاده را پیش خواند</p></div>
<div class="m2"><p>بران نامهٔ گیو گوهر فشاند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهانی به شادی بیاراستند</p></div>
<div class="m2"><p>بهر جای رامشگران خواستند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازان پس ز کشور مهان جهان</p></div>
<div class="m2"><p>برفتند یکسر سوی اصفهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیاراست گودرز کاخ بلند</p></div>
<div class="m2"><p>همه دیبهٔ خسروانی فگند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی تخت بنهاد پیکر به زر</p></div>
<div class="m2"><p>بدو اندرون چند گونه گهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی تاج با یاره و گوشوار</p></div>
<div class="m2"><p>یکی طوق پر گوهر شاهوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به زر و به گوهر بیاراست گاه</p></div>
<div class="m2"><p>چنان چون بباید سزاوار شاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سراسر همه شهر آیین ببست</p></div>
<div class="m2"><p>بیاراست میدان و جای نشست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مهان سرافراز برخاستند</p></div>
<div class="m2"><p>پذیره شدن را بیاراستند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برفتند هشتاد فرسنگ پیش</p></div>
<div class="m2"><p>پذیره شدندش به آیین خویش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو چشم سپهبد برآمد به شاه</p></div>
<div class="m2"><p>همان گیو را دید با او به راه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو آمد پدیدار با شاه گیو</p></div>
<div class="m2"><p>پیاده شدند آن سواران نیو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرو ریخت از دیدگان آب زرد</p></div>
<div class="m2"><p>ز درد سیاوش بسی یاد کرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ستودش فراوان و کرد آفرین</p></div>
<div class="m2"><p>چنین گفت کای شهریار زمین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز تو چشم بدخواه تو دور باد</p></div>
<div class="m2"><p>روان سیاوش پر از نور باد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهاندار یزدان گوای منست</p></div>
<div class="m2"><p>که دیدار تو رهنمای منست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سیاووش را زنده گر دیدمی</p></div>
<div class="m2"><p>بدین گونه از دل نخندیدمی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بزرگان ایران همه پیش اوی</p></div>
<div class="m2"><p>یکایک نهادند بر خاک روی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وزان جایگه شاد گشتند باز</p></div>
<div class="m2"><p>فروزنده شد بخت گردن فراز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ببوسید چشم و سر گیو گفت</p></div>
<div class="m2"><p>که بیرون کشیدی سپهر از نهفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گزارندهٔ خواب و جنگی توی</p></div>
<div class="m2"><p>گه چاره مرد درنگی توی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سوی خانهٔ پهلوان آمدند</p></div>
<div class="m2"><p>همه شاد و روشن روان آمدند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ببودند یک هفته با می بدست</p></div>
<div class="m2"><p>بیاراسته بزمگاه و نشست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به هشتم سوی شهر کاووس شاه</p></div>
<div class="m2"><p>همه شاددل برگرفتند راه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو کیخسرو آمد بر شهریار</p></div>
<div class="m2"><p>جهان گشت پر بوی و رنگ و نگار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر آیین جهانی شد آراسته</p></div>
<div class="m2"><p>در و بام و دیوار پرخواسته</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نشسته به هر جای رامشگران</p></div>
<div class="m2"><p>گلاب و می و مشک با زعفران</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همه یال اسپان پر از مشک و می</p></div>
<div class="m2"><p>درم با شکر ریخته زیر پی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو کاووس کی روی خسرو بدید</p></div>
<div class="m2"><p>سرشکش ز مژگان به رخ بر چکید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فرود آمد از تخت و شد پیش اوی</p></div>
<div class="m2"><p>بمالید بر چشم او چشم و روی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جوان جهانجوی بردش نماز</p></div>
<div class="m2"><p>گرازان سوی تخت رفتند باز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>فراوان ز ترکان بپرسید شاه</p></div>
<div class="m2"><p>هم از تخت سالار توران سپاه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چنین پاسخ آورد کان کم خرد</p></div>
<div class="m2"><p>به بد روی گیتی همی بسپرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرا چند ببسود و چندی بگفت</p></div>
<div class="m2"><p>خرد با هنر کردم اندر نهفت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بترسیدم از کار و کردار او</p></div>
<div class="m2"><p>بپیچیدم از رنج و تیمار او</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر ویژه ابری شود در بار</p></div>
<div class="m2"><p>کشنده پدر چون بود دوستدار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نخواند مرا موبد از آب پاک</p></div>
<div class="m2"><p>که بپرستم او را پدر زیر خاک</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کنون گیو چندی به سختی ببود</p></div>
<div class="m2"><p>به توران مرا جست و رنج آزمود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر نیز رنجی نبودی جزین</p></div>
<div class="m2"><p>که با من بیامد ز توران زمین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سرافراز دو پهلوان با سپاه</p></div>
<div class="m2"><p>پس ما بیامد چو آتش به راه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>من آن دیدم از گیو کز پیل مست</p></div>
<div class="m2"><p>نبیند به هندوستان بت پرست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گمانی نبردم که هرگز نهنگ</p></div>
<div class="m2"><p>ز دریا بران سان برآید به جنگ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ازان پس که پیران بیامد چو شیر</p></div>
<div class="m2"><p>میان بسته و بادپایی به زیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به آب اندر آمد بسان نهنگ</p></div>
<div class="m2"><p>که گفتی زمین را بسوزد به جنگ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بینداخت بر یال او بر کمند</p></div>
<div class="m2"><p>سر پهلوان اندر آمد به بند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بخواهشگری رفتم ای شهریار</p></div>
<div class="m2"><p>وگرنه به کندی سرش را ز بار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بدان کاو ز درد پدر خسته بود</p></div>
<div class="m2"><p>ز بد گفتن ما زبان بسته بود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چنین تا لب رود جیحون به جنگ</p></div>
<div class="m2"><p>نیاسود با گرزهٔ گاورنگ</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سرانجام بگذاشت جیحون به خشم</p></div>
<div class="m2"><p>به آب و کشتی نیفگند چشم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کسی را که چون او بود پهلوان</p></div>
<div class="m2"><p>بود جاودان شاد و روشن روان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یکی کاخ کشواد بد در صطخر</p></div>
<div class="m2"><p>که آزادگان را بدو بود فخر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو از تخت کاووس برخاستند</p></div>
<div class="m2"><p>به ایوان نو رفتن آراستند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همی رفت گودرز با شهریار</p></div>
<div class="m2"><p>چو آمد بدان گلشن زرنگار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بر اورنگ زرینش بنشاندند</p></div>
<div class="m2"><p>برو بر بسی آفرین خواندند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ببستند گردان ایران کمر</p></div>
<div class="m2"><p>بجز طوس نوذر که پیچید سر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>که او بود با کوس و زرینه کفش</p></div>
<div class="m2"><p>هم او داشتی کاویانی درفش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ازان کار گودرز شد تیز مغز</p></div>
<div class="m2"><p>بر او پیامی فرستاد نغز</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>پیمبر سرافراز گیو دلیر</p></div>
<div class="m2"><p>که چنگ یلان داشت و بازوی شیر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بدو گفت با طوس نوذر بگوی</p></div>
<div class="m2"><p>که هنگام شادی بهانه مجوی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بزرگان و گردان ایران زمین</p></div>
<div class="m2"><p>همه شاه را خواندند آفرین</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چرا سر کشی تو به فرمان دیو</p></div>
<div class="m2"><p>نبینی همی فر گیهان خدیو</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اگر تو بپیچی ز فرمان شاه</p></div>
<div class="m2"><p>مرا با تو کین خیزد و رزمگاه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>فرستاده گیوست پیغام من</p></div>
<div class="m2"><p>به دستوری نامدار انجمن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ز پیش پدر گیو بنمود پشت</p></div>
<div class="m2"><p>دلش پر ز گفتارهای درشت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بیامد به طوس سپهبد بگفت</p></div>
<div class="m2"><p>که این رای را با تو دیوست جفت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو بشنید پاسخ چنین داد طوس</p></div>
<div class="m2"><p>که بر ما نه خوبست کردن فسوس</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به ایران پس از رستم پیلتن</p></div>
<div class="m2"><p>سرافرازتر کس منم ز انجمن</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نبیره منوچهر شاه دلیر</p></div>
<div class="m2"><p>که گیتی به تیغ اندر آورد زیر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>همان شیر پرخاشجویم به جنگ</p></div>
<div class="m2"><p>بدرم دل پیل و چنگ پلنگ</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>همی بی من آیین و رای آورید</p></div>
<div class="m2"><p>جهان را به نو کدخدای آورید</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>نباشم بدین کار همداستان</p></div>
<div class="m2"><p>ز خسرو مزن پیش من داستان</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>جهاندار کز تخم افراسیاب</p></div>
<div class="m2"><p>نشانیم بخت اندر آید به خواب</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>نخواهیم شاه از نژاد پشنگ</p></div>
<div class="m2"><p>فسیله نه نیکو بود با پلنگ</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>تو این رنجها را که بردی برست</p></div>
<div class="m2"><p>که خسرو جوانست و کندآورست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>کسی کاو بود شهریار زمین</p></div>
<div class="m2"><p>هنر باید و گوهر و فر و دین</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>فریبرز کاووس فرزند شاه</p></div>
<div class="m2"><p>سزاوارتر کس به تخت و کلاه</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بهرسو ز دشمن ندارد نژاد</p></div>
<div class="m2"><p>همش فر و برزست و هم نام و داد</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>دژم گیو برخاست از پیش او</p></div>
<div class="m2"><p>که خام آمدش دانش و کیش او</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بیامد به گودرز کشواد گفت</p></div>
<div class="m2"><p>که فر و خرد نیست با طوس جفت</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>دو چشمش تو گویی نبیند همی</p></div>
<div class="m2"><p>فریبرز را برگزیند همی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>برآشفت گودرز و گفت از مهان</p></div>
<div class="m2"><p>همی طوس کم باد اندر جهان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>نبیره پسر داشت هفتاد و هشت</p></div>
<div class="m2"><p>بزد کوس ز ایوان به میدان گذشت</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>سواران جنگی ده و دو هزار</p></div>
<div class="m2"><p>برون رفت بر گستوان‌ور سوار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>وزان رو بیامد سپهدار طوس</p></div>
<div class="m2"><p>ببستند بر کوههٔ پیل کوس</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ببستند گردان ایران میان</p></div>
<div class="m2"><p>به پیش سپاه اختر کاویان</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چو گودرز را دید و چندان سپاه</p></div>
<div class="m2"><p>کزو تیره شد روی خورشید و ماه</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>یکی تخت بر کوههٔ ژنده پیل</p></div>
<div class="m2"><p>ز پیروزه تابان به کردار نیل</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>جهانجوی کیخسرو تاج ور</p></div>
<div class="m2"><p>نشسته بران تخت و بسته کمر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به گرد اندرش ژنده‌پیلان دویست</p></div>
<div class="m2"><p>تو گفتی به گیتی جز آن جای نیست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>همی تافت زان تخت خسرو چو ماه</p></div>
<div class="m2"><p>ز یاقوت رخشنده بر سر کلاه</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>غمی شد دل طوس و اندیشه کرد</p></div>
<div class="m2"><p>که امروز اگر من بسازم نبرد</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بسی کشته آید ز هر دو سپاه</p></div>
<div class="m2"><p>ز ایران نه برخیزد این کینه‌گاه</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>نباشد جز از کام افراسیاب</p></div>
<div class="m2"><p>سر بخت ترکان برآید ز خواب</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بدیشان رسد تخت شاهنشهی</p></div>
<div class="m2"><p>سرآید به ما روزگار مهی</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>خردمند مردی و جوینده راه</p></div>
<div class="m2"><p>فرستاد نزدیک کاووس شاه</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>که از ما یکی گر برین دشت جنگ</p></div>
<div class="m2"><p>نهد بر کمان پر تیر خدنگ</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>یکی کینه خیزد که افراسیاب</p></div>
<div class="m2"><p>هم امشب همی آن ببیند به خواب</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چو بشنید زین‌گونه گفتار شاه</p></div>
<div class="m2"><p>بفرمود تا بازگردد به راه</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بر طوس و گودرز کشوادگان</p></div>
<div class="m2"><p>گزیده سرافراز آزادگان</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>که بر درگه آیند بی‌انجمن</p></div>
<div class="m2"><p>چنان چون بباید به نزدیک من</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بشد طوس و گودرز نزدیک شاه</p></div>
<div class="m2"><p>زبان برگشادند بر پیش گاه</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>بدو گفت شاه ای خردمند پیر</p></div>
<div class="m2"><p>منه زهر برنده بر جام شیر</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بنه تیغ و بگشای ز آهن میان</p></div>
<div class="m2"><p>نباید کزین سود دارد زیان</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چنین گفت طوس سپهبد به شاه</p></div>
<div class="m2"><p>که گر شاه سیر آید از تخت و گاه</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>به فرزند باید که ماند جهان</p></div>
<div class="m2"><p>بزرگی و دیهیم و تخت مهان</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>چو فرزند باشد نبیره کلاه</p></div>
<div class="m2"><p>چرا برنهد برنشیند به گاه</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بدو گفت گودرز کای کم خرد</p></div>
<div class="m2"><p>ترا بخرد از مردمان نشمرد</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>به گیتی کسی چون سیاوش نبود</p></div>
<div class="m2"><p>چنو راد و آزاد و خامش نبود</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>کنون این جهانجوی فرزند اوست</p></div>
<div class="m2"><p>همویست گویی به چهر و به پوست</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>گر از تور دارد ز مادر نژاد</p></div>
<div class="m2"><p>هم از تخم شاهی نپیچد ز داد</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>به توران و ایران چنو نیو کیست</p></div>
<div class="m2"><p>چنین خام گفتارت از بهر چیست</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>دو چشمت نبیند همی چهر او</p></div>
<div class="m2"><p>چنان برز و بالا و آن مهر او</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>به جیحون گذر کرد و کشتی نجست</p></div>
<div class="m2"><p>به فر کیانی و رای درست</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>بسان فریدون کز اروند رود</p></div>
<div class="m2"><p>گذشت و به کشتی نیامد فرود</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>ز مردی و از فرهٔ ایزدی</p></div>
<div class="m2"><p>ازو دور شد چشم و دست بدی</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>تو نوذر نژادی نه بیگانه‌ای</p></div>
<div class="m2"><p>پدر تیز بود و تو دیوانه‌ای</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>سلیح من ار با منستی کنون</p></div>
<div class="m2"><p>بر و یالت آغشته گشتی به خون</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>بدو گفت طوس ای جهاندیده پیر</p></div>
<div class="m2"><p>سخن گوی لیکن همه دلپذیر</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>اگر تیغ تو هست سندان شکاف</p></div>
<div class="m2"><p>سنانم بدرد دل کوه قاف</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>وگر گرز تو هست با سنگ و تاب</p></div>
<div class="m2"><p>خدنگم بدوزد دل آفتاب</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>و گر تو ز کشواد داری نژاد</p></div>
<div class="m2"><p>منم طوس نوذر مه و شاهزاد</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>بدو گفت گودرز چندین مگوی</p></div>
<div class="m2"><p>که چندین نبینم ترا آب روی</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>به کاووس گفت ای جهاندار شاه</p></div>
<div class="m2"><p>تو دل را مگردان ز آیین و راه</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>دو فرزند پرمایه را پیش خوان</p></div>
<div class="m2"><p>سزاوار گاهند و هر دو جوان</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>ببین تا ز هر دو سزاوار کیست</p></div>
<div class="m2"><p>که با برز و با فرهٔ ایزدیست</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>بدو تاج بسپار و دل شاد دار</p></div>
<div class="m2"><p>چو فرزند بینی همی شهریار</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>بدو گفت کاووس کاین رای نیست</p></div>
<div class="m2"><p>که فرزند هر دو به دل بر یکیست</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>یکی را چو من کرده باشم گزین</p></div>
<div class="m2"><p>دل دیگر از من شود پر ز کین</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>یکی کار سازم که هر دو ز من</p></div>
<div class="m2"><p>نگیرند کین اندرین انجمن</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>دو فرزند ما را کنون بر دو خیل</p></div>
<div class="m2"><p>بباید شدن تا در اردبیل</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>به مرزی که آنجا دژ بهمنست</p></div>
<div class="m2"><p>همه ساله پرخاش آهرمنست</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>برنجست ز آهرمن آتش پرست</p></div>
<div class="m2"><p>نباشد بران مرز کس را نشست</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>ازیشان یکی کان بگیرد به تیغ</p></div>
<div class="m2"><p>ندارم ازو تخت شاهی دریغ</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>چو بشنید گودرز و طوس این سخن</p></div>
<div class="m2"><p>که افگند سالار هشیار بن</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>برین هر دو گشتند همداستان</p></div>
<div class="m2"><p>ندانست ازین به کسی داستان</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>برین یک سخن دل بیاراستند</p></div>
<div class="m2"><p>ز پیش جهاندار برخاستند</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>چو خورشید برزد سر از برج شیر</p></div>
<div class="m2"><p>سپهر اندر آورد شب را به زیر</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>فریبرز با طوس نوذر دمان</p></div>
<div class="m2"><p>به نزدیک شاه آمدند آن زمان</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>چنین گفت با شاه هشیار طوس</p></div>
<div class="m2"><p>که من با سپهبد برم پیل و کوس</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>همان من کشم کاویانی درفش</p></div>
<div class="m2"><p>رخ لعل دشمن کنم چون بنفش</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>کنون همچنین من ز درگاه شاه</p></div>
<div class="m2"><p>بنه برنهم برنشانم سپاه</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>پس اندر فریبرز و کوس و درفش</p></div>
<div class="m2"><p>هوا کرده از سم اسپان بنفش</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>چو فرزند را فر و برز کیان</p></div>
<div class="m2"><p>بباشد نبیره نبندد میان</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>بدو گفت شاه ار تو رانی ز پیش</p></div>
<div class="m2"><p>زمانه نگردد ز آیین خویش</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>برای خداوند خورشید و ماه</p></div>
<div class="m2"><p>توان ساخت پیروزی و دستگاه</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>فریبرز را گر چنین است رای</p></div>
<div class="m2"><p>تو لشکر بیارای و منشین ز پای</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>بشد طوس با کاویانی درفش</p></div>
<div class="m2"><p>به پا اندرون کرده زرینه کفش</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>فریبرز کاووس در قلبگاه</p></div>
<div class="m2"><p>به پیش اندرون طوس و پیل و سپاه</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>چو نزدیک بهمن دژ اندر رسید</p></div>
<div class="m2"><p>زمین همچو آتش همی بردمید</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>بشد طوس با لشکری جنگجوی</p></div>
<div class="m2"><p>به تندی سوی دژ نهادند روی</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>سر بارهٔ دژ بد اندر هوا</p></div>
<div class="m2"><p>ندیدند جنگ هوا کس روا</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>سنانها ز گرمی همی برفروخت</p></div>
<div class="m2"><p>میان زره مرد جنگی بسوخت</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>جهان سر به سر گفتی از آتش است</p></div>
<div class="m2"><p>هوا دام آهرمن سرکش است</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>سپهبد فریبرز را گفت مرد</p></div>
<div class="m2"><p>به چیزی چو آید به دشت نبرد</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>به گرز گران و به تیغ و کمند</p></div>
<div class="m2"><p>بکوشد که آرد به چیزی گزند</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>به پیرامن دژ یکی راه نیست</p></div>
<div class="m2"><p>ز آتش کسی را دل ای شاه نیست</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>میان زیر جوشن بسوزد همی</p></div>
<div class="m2"><p>تن بارکش برفروزد همی</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>بگشتند یک هفته گرد اندرش</p></div>
<div class="m2"><p>بدیده ندیدند جای درش</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>به نومیدی از جنگ گشتند باز</p></div>
<div class="m2"><p>نیامد بر از رنج راه دراز</p></div></div>