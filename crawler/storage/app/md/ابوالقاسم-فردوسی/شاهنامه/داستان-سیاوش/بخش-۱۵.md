---
title: >-
    بخش ۱۵
---
# بخش ۱۵

<div class="b" id="bn1"><div class="m1"><p>چو لشکر بیامد ز دشت نبرد</p></div>
<div class="m2"><p>تنان پر ز خون و سران پر ز گرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبر شد ز ترکان به افراسیاب</p></div>
<div class="m2"><p>که بیدار بخت اندرآمد به خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان سرخه نامور کشته شد</p></div>
<div class="m2"><p>چنان دولت تیز برگشته شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بریده سرش را نگونسار کرد</p></div>
<div class="m2"><p>تنش را به خون غرقه بر دار کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه شهر ایران جگر خسته‌اند</p></div>
<div class="m2"><p>به کین سیاوش کمر بسته‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگون شد سر و تاج افراسیاب</p></div>
<div class="m2"><p>همی کند موی و همی ریخت آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی گفت رادا سرا موبدا</p></div>
<div class="m2"><p>ردا نامدارا یلا بخردا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دریغ ارغوانی رخت همچو ماه</p></div>
<div class="m2"><p>دریغ آن کیی برز و بالای شاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خروشان به سر بر پراگند خاک</p></div>
<div class="m2"><p>همه جامه ها کرد بر خویش چاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین گفت با لشکر افراسیاب</p></div>
<div class="m2"><p>که مارا بر آمد سر از خورد و خواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه کینه را چشم روشن کنید</p></div>
<div class="m2"><p>نهالی ز خفتان و جوشن کنید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو برخاست آوای کوس از درش</p></div>
<div class="m2"><p>بجنبید بر بارگه لشکرش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بزد نای رویین و بربست کوس</p></div>
<div class="m2"><p>همی آسمان بر زمین داد بوس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به گردنکشان خسرو آواز کرد</p></div>
<div class="m2"><p>که ای نامداران روز نبرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو برخیزد آوای کوس از دو روی</p></div>
<div class="m2"><p>نجوید زمان مرد پرخاشجوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه رزم را دل پر از کین کنید</p></div>
<div class="m2"><p>به ایرانیان پاک نفرین کنید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خروش آمد و نالهٔ کرنای</p></div>
<div class="m2"><p>دم نای رویین و هندی درای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زمین آمد از سم اسپان به جوش</p></div>
<div class="m2"><p>به ابر اندر آمد فغان و خروش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو برخاست از دشت گرد سپاه</p></div>
<div class="m2"><p>کس آمد بر رستم از دیده‌گاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که آمد سپاهی چو کوه گران</p></div>
<div class="m2"><p>همه رزم جویان کندآوران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز تیغ دلیران هوا شد بنفش</p></div>
<div class="m2"><p>برفتند با کاویانی درفش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برآمد خروش سپاه از دو روی</p></div>
<div class="m2"><p>جهان شد پر از مردم جنگجوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خور و ماه گفتی به رنگ اندرست</p></div>
<div class="m2"><p>ستاره به چنگ نهنگ اندرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپهدار ترکان برآراست جنگ</p></div>
<div class="m2"><p>گرفتند گوپال و خنجر به چنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بیامد سوی میمنه بارمان</p></div>
<div class="m2"><p>سپاهی ز ترکان دنان و دمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سوی میسره کهرم تیغ‌زن</p></div>
<div class="m2"><p>به قلب اندرون شاه با انجمن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وزین روی رستم سپه برکشید</p></div>
<div class="m2"><p>هوا شد ز تیغ یلان ناپدید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیاراست بر میمنه گیو و طوس</p></div>
<div class="m2"><p>سواران بیدار با پیل و کوس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو گودرز کشواد بر میسره</p></div>
<div class="m2"><p>هجیر و گرانمایگان یکسره</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به قلب اندرون رستم زابلی</p></div>
<div class="m2"><p>زره‌دار با خنجر کابلی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو گفتی نه شب بود پیدا نه روز</p></div>
<div class="m2"><p>نهان گشت خورشید گیتی‌فروز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شد از سم اسپان زمین سنگ رنگ</p></div>
<div class="m2"><p>ز نیزه هوا همچو پشت پلنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو گفتی هوا کوه آهن شدست</p></div>
<div class="m2"><p>سر کوه پر ترگ و جوشن شدست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به ابر اندر آمد سنان و درفش</p></div>
<div class="m2"><p>درفشیدن تیغهای بنفش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بیامد ز قلب سپه پیلسم</p></div>
<div class="m2"><p>دلش پر ز خون کرده چهره دژم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنین گفت با شاه توران سپاه</p></div>
<div class="m2"><p>که‌ای پرهنر خسرو نیک‌خواه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر ایدونک از من نداری دریغ</p></div>
<div class="m2"><p>یکی باره و جوشن و گرز و تیغ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ابا رستم امروز جنگ آورم</p></div>
<div class="m2"><p>همه نام او زیر ننگ آورم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به پیش تو آرم سر و رخش او</p></div>
<div class="m2"><p>همان خود و تیغ جهان بخش او</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ازو شاد شد جان افراسیاب</p></div>
<div class="m2"><p>سر نیزه بگذاشت از آفتاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدو گفت کای نام بردار شیر</p></div>
<div class="m2"><p>همانا که پیلت نیارد به زیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر پیلتن را به چنگ آوری</p></div>
<div class="m2"><p>زمانه برآساید از داوری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به توران چو تو کس نباشد به جاه</p></div>
<div class="m2"><p>به گنج و به تیغ و به تخت و کلاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به گردان سپهر اندرآری سرم</p></div>
<div class="m2"><p>سپارم ترا دختر و کشورم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از ایران و توران دو بهر آن تست</p></div>
<div class="m2"><p>همان گوهر و گنج و شهر آن تست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو بشنید پیران غمی گشت سخت</p></div>
<div class="m2"><p>بیامد بر شاه خورشید بخت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدو گفت کاین مرد برنا و تیز</p></div>
<div class="m2"><p>همی بر تن خویش دارد ستیز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همی در گمان افتد از نام خویش</p></div>
<div class="m2"><p>نیندیشد از کار فرجام خویش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کسی سوی دوزخ نپوید به پا</p></div>
<div class="m2"><p>و گر خیره سوی دم اژدها</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر او با تهمتن نبرد آورد</p></div>
<div class="m2"><p>سر خویش را زیر گرد آورد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شکسته شود دل گوان را به جنگ</p></div>
<div class="m2"><p>بود این سخن نیز بر شاه ننگ</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>برادر تو دانی که کهتر بود</p></div>
<div class="m2"><p>فزون‌تر برو مهر مهتر بود</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به پیران چنین گفت پس پیلسم</p></div>
<div class="m2"><p>کزین پهلوان دل ندارد دژم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که گر من کنم جنگ جنگی نهنگ</p></div>
<div class="m2"><p>نیارم به بخت تو بر شاه ننگ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به پیش تو با نامور چار گرد</p></div>
<div class="m2"><p>چه کردم تو دیدی ز من دست برد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همانا کنون زورم افزونترست</p></div>
<div class="m2"><p>شکستن دل من نه اندرخورست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>برآید به دست من این کارکرد</p></div>
<div class="m2"><p>به گرد در اختر بد مگرد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو بشنید زو این سخن شهریار</p></div>
<div class="m2"><p>یکی اسپ شایستهٔ کارزار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بدو داد با تیغ و بر گستوان</p></div>
<div class="m2"><p>همان نیزه و درع و خود گوان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بیاراست آن جنگ را پیلسم</p></div>
<div class="m2"><p>همی راند چون شیر با باد و دم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به ایرانیان گفت رستم کجاست</p></div>
<div class="m2"><p>که گوید که او روز جنگ اژدهاست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو بشنید گیو این سخن بردمید</p></div>
<div class="m2"><p>بزد دست و تیغ از میان برکشید</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بدو گفت رستم به یک ترک جنگ</p></div>
<div class="m2"><p>نسازد همانا که آیدش ننگ</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>برآویختند آن دو جنگی به هم</p></div>
<div class="m2"><p>دمان گیو گودرز با پیلسم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>یکی نیزه زد گیو را کز نهیب</p></div>
<div class="m2"><p>برون آمدش هر دو پا از رکیب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>فرامرز چون دید یار آمدش</p></div>
<div class="m2"><p>همی یار جنگی به کار آمدش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یکی تیغ بر نیزهٔ پیلسم</p></div>
<div class="m2"><p>بزد نیزه از تیغ او شد قلم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دگر باره زد بر سر ترگ اوی</p></div>
<div class="m2"><p>شکسته شد آن تیغ پرخاشجوی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همی گشت با آن دو یل پیلسم</p></div>
<div class="m2"><p>به میدان به کردار شیر دژم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تهمتن ز قلب سپه بنگرید</p></div>
<div class="m2"><p>دو گرد دلیر و گرانمایه دید</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>برآویخته با یکی شیرمرد</p></div>
<div class="m2"><p>به ابر اندر آورده از باد گرد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بدانست رستم که جز پیلسم</p></div>
<div class="m2"><p>ز ترکان ندارد کس آن زور و دم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>و دیگر که از نامور بخردان</p></div>
<div class="m2"><p>ز گفت ستاره‌شمر موبدان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز اختر بد و نیک بشنوده بود</p></div>
<div class="m2"><p>جهان را چپ و راست پیموده بود</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>که گر پیلسم از بد روزگار</p></div>
<div class="m2"><p>خرد یابد و بند آموزگار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>نبرده چنو در جهان سر به سر</p></div>
<div class="m2"><p>به ایران و توران نبندد کمر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>همانا که او را زمان آمدست</p></div>
<div class="m2"><p>که ایدر به چنگم دمان آمدست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به لشکر بفرمود کز جای خویش</p></div>
<div class="m2"><p>مگر ناورند اندکی پای پیش</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>شوم برگرایم تن پیلسم</p></div>
<div class="m2"><p>ببینم که دارد پی و شاخ و دم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>یکی نیزهٔ بارکش برگرفت</p></div>
<div class="m2"><p>بیفشارد ران ترگ بر سر گرفت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گران شد رکیب و سبک شد عنان</p></div>
<div class="m2"><p>به چشم اندر آورد رخشان سنان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>غمی گشت و بر لب برآورد کف</p></div>
<div class="m2"><p>همی تاخت از قلب تا پیش صف</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چنین گفت کای نامور پیلسم</p></div>
<div class="m2"><p>مرا خواستی تا بسوزی به دم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>همی گفت و می‌تاخت برسان گرد</p></div>
<div class="m2"><p>یکی کرد با او سخن در نبرد</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>یکی نیزه زد بر کمرگاه اوی</p></div>
<div class="m2"><p>ز زین برگرفتش به کردار گوی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>همی تاخت تا قلب توران سپاه</p></div>
<div class="m2"><p>بینداختش خوار در قلبگاه</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چنین گفت کاین را به دیبای زرد</p></div>
<div class="m2"><p>بپوشید کز گرد شد لاژورد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>عنان را بپیچید زان جایگاه</p></div>
<div class="m2"><p>بیامد دمان تا به قلب سپاه</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ببارید پیران ز مژگان سرشک</p></div>
<div class="m2"><p>تن پیلسم دور دید از پزشک</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>دل لشکر و شاه توران سپاه</p></div>
<div class="m2"><p>شکسته شد و تیره شد رزمگاه</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>خروش آمد از لشکر هر دو سوی</p></div>
<div class="m2"><p>ده و دار گردان پرخاشجوی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>خروشیدن کوس بر پشت پیل</p></div>
<div class="m2"><p>ز هر سو همی رفت تا چند میل</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>زمین شد ز نعل ستوران ستوه</p></div>
<div class="m2"><p>همه کوه دریا شد و دشت کوه</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ز بس نعره و نالهٔ کره‌نای</p></div>
<div class="m2"><p>همی آسمان اندر آمد ز جای</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>همی سنگ مرجان شد و خاک خون</p></div>
<div class="m2"><p>سراسر سر سروران شد نگون</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بکشتند چندان ز هردو گروه</p></div>
<div class="m2"><p>که شد خاک دریا و هامون چو کوه</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>یکی باد برخاست از رزمگاه</p></div>
<div class="m2"><p>هوا را بپوشید گرد سپاه</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>دو لشکر به هامون همی تاختند</p></div>
<div class="m2"><p>یک از دیگران بازنشناختند</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>جهان چون شب تیره تاریک شد</p></div>
<div class="m2"><p>تو گفتی به شب روز نزدیک شد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چنین گفت با لشکر افراسیاب</p></div>
<div class="m2"><p>که بیدار بخت اندر آمد به خواب</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>اگر سستی آرید یک تن به جنگ</p></div>
<div class="m2"><p>نماند مرا روزگار درنگ</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بریشان ز هر سو کمین آورید</p></div>
<div class="m2"><p>به نیزه خور اندر زمین آورید</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بیامد خود از قلب توران سپاه</p></div>
<div class="m2"><p>بر طوس شد داغ دل کینه‌خواه</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>از ایران فراوان سپه را بکشت</p></div>
<div class="m2"><p>غمی شد دل طوس و بنمود پشت</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بر رستم آمد یکی چاره‌جوی</p></div>
<div class="m2"><p>که امروز ازین رزم شد رنگ و بوی</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>همه رزمگه شد چو دریای خون</p></div>
<div class="m2"><p>درفش سپهدار ایران نگون</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بیامد ز قلب سپه پیلتن</p></div>
<div class="m2"><p>پس او فرامرز با انجمن</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>سپردار بسیار در پیش بود</p></div>
<div class="m2"><p>که دلشان ز رستم بداندیش بود</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>همه خویش و پیوند افراسیاب</p></div>
<div class="m2"><p>همه دل پر از کین و سر پرشتاب</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>تهمتن فراوان ازیشان بکشت</p></div>
<div class="m2"><p>فرامرز و طوس اندر آمد به پشت</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چو افراسیاب آن درفش بنفش</p></div>
<div class="m2"><p>نگه کرد بر جایگاه درفش</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بدانست کان پیلتن رستمست</p></div>
<div class="m2"><p>سرافراز وز تخمهٔ نیرمست</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>برآشفت برسان جنگی پلنگ</p></div>
<div class="m2"><p>بیفشارد ران پیش او شد به جنگ</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چو رستم درفش سیه را بدید</p></div>
<div class="m2"><p>به کردار شیر ژیان بردمید</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>به جوش آمد آن نامبردار گرد</p></div>
<div class="m2"><p>عنان بارهٔ تیزتگ را سپرد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>برآویخت با سرکش افراسیاب</p></div>
<div class="m2"><p>به پیگار خون رفت چون رود آب</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>یکی نیزه سالار توران سپاه</p></div>
<div class="m2"><p>بزد بر بر رستم کینه‌خواه</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>سنان اندر آمد ببند کمر</p></div>
<div class="m2"><p>به ببر بیان بر نبد کارگر</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>تهمتن به کین اندر آورد روی</p></div>
<div class="m2"><p>یکی نیزه زد بر سر اسپ اوی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>تگاور ز درد اندر آمد به سر</p></div>
<div class="m2"><p>بیفتاد زو شاه پرخاشخر</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>همی جست رستم کمرگاه او</p></div>
<div class="m2"><p>که از رزم کوته کند راه او</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>نگه کرد هومان بدید از کران</p></div>
<div class="m2"><p>به گردن برآورد گرز گران</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>بزد بر سر شانهٔ پیلتن</p></div>
<div class="m2"><p>به لشکر خروش آمد از انجمن</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>ز پس کرد رستم همانگه نگاه</p></div>
<div class="m2"><p>بجست از کفش نامبردار شاه</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>برآشفت گردافگن تاج‌بخش</p></div>
<div class="m2"><p>بدنبال هومان برانگیخت رخش</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>بتازید چندی و چندی شتافت</p></div>
<div class="m2"><p>زمانه بدش مانده او را نیافت</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>سپهدار ترکان نشد زیر دست</p></div>
<div class="m2"><p>یکی بارهٔ تیزتگ برنشست</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>چو از جنگ رستم بپیچید روی</p></div>
<div class="m2"><p>گریزان همی رفت پرخاشجوی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>برآمد ز هر سو دم کرنای</p></div>
<div class="m2"><p>همی آسمان اندر آمد ز جای</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>به ابر اندر آمد خروش سران</p></div>
<div class="m2"><p>گراییدن گرزهای گران</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>گوان سر به سر نعره برداشتند</p></div>
<div class="m2"><p>سنانها به ابر اندر افراشتند</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>زمین سربسر کشته و خسته بود</p></div>
<div class="m2"><p>وگر لاله بر زعفران رسته بود</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>سپردند اسپان همی خون به نعل</p></div>
<div class="m2"><p>شده پای پیل از دل کشته لعل</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>هزیمت گرفتند ترکان چو باد</p></div>
<div class="m2"><p>که رستم ز بازو همی داد داد</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>سه فرسنگ چون اژدهای دمان</p></div>
<div class="m2"><p>تهمتن همی شد پس بدگمان</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>وزان جایگه پیلتن بازگشت</p></div>
<div class="m2"><p>سپه یکسر از جنگ ناساز گشت</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>ز رستم بپرسید پرمایه طوس</p></div>
<div class="m2"><p>که چون یافت شیر از یکی گور کوس</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بدو گفت رستم که گرز گران</p></div>
<div class="m2"><p>چو یاد آرد از یال جنگ‌آوران</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>دل سنگ و سندان نماند درست</p></div>
<div class="m2"><p>بر و یال کوبنده باید نخست</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>عمودی که کوبنده هومان بود</p></div>
<div class="m2"><p>تو آهن مخوانش که موم آن بود</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>به لشکرگه خویش گشتند باز</p></div>
<div class="m2"><p>سپه یکسر از خواسته بی‌نیاز</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>همه دشت پر آهن و سیم و زر</p></div>
<div class="m2"><p>سنان و ستام و کلاه و کمر</p></div></div>