---
title: >-
    بخش ۱۹
---
# بخش ۱۹

<div class="b" id="bn1"><div class="m1"><p>سواران گزین کرد پیران هزار</p></div>
<div class="m2"><p>همه جنگجوی و همه نامدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدیشان چنین گفت پیران که زود</p></div>
<div class="m2"><p>عنان تگاور بباید بسود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب و روز رفتن چو شیر ژیان</p></div>
<div class="m2"><p>نباید گشادن به ره بر میان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که گر گیو و خسرو به ایران شوند</p></div>
<div class="m2"><p>زنان اندر ایران چه شیران شوند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نماند برین بوم و بر خاک و آب</p></div>
<div class="m2"><p>وزین داغ دل گردد افراسیاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به گفتار او سر برافراختند</p></div>
<div class="m2"><p>شب و روز یکسر همی تاختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نجستند روز و شب آرام و خواب</p></div>
<div class="m2"><p>وزین آگهی شد به افراسیاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین تا بیامد یکی ژرف رود</p></div>
<div class="m2"><p>سپه شد پراگنده چون تار و پود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنش ژرف و پهناش کوتاه بود</p></div>
<div class="m2"><p>بدو بر به رفتن دژآگاه بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشسته فرنگیس بر پاس گاه</p></div>
<div class="m2"><p>به دیگر کران خفته بد گیو و شاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرنگیس زان جایگه بنگرید</p></div>
<div class="m2"><p>درفش سپهدار توران بدید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دوان شد بر گیو و آگاه کرد</p></div>
<div class="m2"><p>بران خفتگان خواب کوتاه کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو گفت کای مرد با رنج خیز</p></div>
<div class="m2"><p>که آمد ترا روزگار گریز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ترا گر بیابند بیجان کنند</p></div>
<div class="m2"><p>دل ما ز درد تو پیچان کنند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا با پسر دیده گردد پرآب</p></div>
<div class="m2"><p>برد بسته تا پیش افراسیاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وزان پس ندانم چه آید گزند</p></div>
<div class="m2"><p>نداند کسی راز چرخ بلند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو گفت گیو ای مه بانوان</p></div>
<div class="m2"><p>چرا رنجه کردی بدینسان روان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو با شاه برشو به بالای تند</p></div>
<div class="m2"><p>ز پیران و لشکر مشو هیچ کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهاندار پیروز یار منست</p></div>
<div class="m2"><p>سر اختر اندر کنار منست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدوگفت کیخسرو ای رزمساز</p></div>
<div class="m2"><p>کنون بر تو بر کار من شد دراز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز دام بلا یافتم من رها</p></div>
<div class="m2"><p>تو چندین مشو در دم اژدها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هامون مرارفت باید کنون</p></div>
<div class="m2"><p>فشاندن به شمشیر بر شید خون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو گفت گیو ای شه سرفراز</p></div>
<div class="m2"><p>جهان را به نام تو آمد نیاز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پدر پهلوانست و من پهلوان</p></div>
<div class="m2"><p>به شاهی نپیچیم جان و روان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برادر مرا هست هفتاد و هشت</p></div>
<div class="m2"><p>جهان شد چو نام تو اندر گذشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بسی پهلوانست شاه اندکی</p></div>
<div class="m2"><p>چه باشد چو پیدا نباشد یکی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر من شوم کشته دیگر بود</p></div>
<div class="m2"><p>سر تاجور باشد افسر بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر تو شوی دور از ایدر تباه</p></div>
<div class="m2"><p>نبینم کسی از در تاج و گاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شود رنج من هفت ساله به باد</p></div>
<div class="m2"><p>دگر آنک ننگ آورم بر نژاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو بالا گزین و سپه را ببین</p></div>
<div class="m2"><p>مرا یاد باشد جهان آفرین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بپوشید درع و بیامد چو شیر</p></div>
<div class="m2"><p>همان باره دستکش را به زیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ازین سوی شه بود ز آنسو سپاه</p></div>
<div class="m2"><p>میانچی شده رود و بر بسته راه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو رعد بهاران بغرید گیو</p></div>
<div class="m2"><p>ز سالار لشکر همی جست نیو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو بشنید پیرانش دشنام داد</p></div>
<div class="m2"><p>بدو گفت کای بد رگ دیوزاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو تنها بدین رزمگاه آمدی</p></div>
<div class="m2"><p>دلاور به پیش سپاه آمدی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کنون خوردنت نوک ژوپین بود</p></div>
<div class="m2"><p>برت را کفن چنگ شاهین بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر کوه آهن بود یک سوار</p></div>
<div class="m2"><p>چو مور اندر آید به گردش هزار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شود خیره سر گرچه خردست مور</p></div>
<div class="m2"><p>نه مورست پوشیده مرد و ستور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کنند این زره بر تنش چاک چاک</p></div>
<div class="m2"><p>چو مردار گردد کشندش به خاک</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یکی داستان زد هژبر دمان</p></div>
<div class="m2"><p>که چون بر گوزنی سرآید زمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زمانه برو دم همی بشمرد</p></div>
<div class="m2"><p>بیاید دمان پیش من بگذرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زمان آوریدت کنون پیش من</p></div>
<div class="m2"><p>همان پیش این نامدار انجمن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدو گفت گیو ای سپهدار شیر</p></div>
<div class="m2"><p>سزد گر به آب اندر آیی دلیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ببینی کزین پرهنر یک سوار</p></div>
<div class="m2"><p>چه آید ترا بر سر ای نامدار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هزارید و من نامور یک دلیر</p></div>
<div class="m2"><p>سر سرکشان اندر آرم به زیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو من گرزهٔ سرگرای آورم</p></div>
<div class="m2"><p>سران را همه زیر پای آورم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو بشنید پیران برآورد خشم</p></div>
<div class="m2"><p>دلش گشت پرخون و پرآب چشم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برانگیخت اسپ و بیفشارد ران</p></div>
<div class="m2"><p>به گردن برآورد گرز گران</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو کشتی ز دشت اندر آمد به رود</p></div>
<div class="m2"><p>همی داد نیکی دهش را درود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نکرد ایچ گیو آزمون را شتاب</p></div>
<div class="m2"><p>بدان تا برآمد سپهبد ز آب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز بالا به پستی بپیچید گیو</p></div>
<div class="m2"><p>گریزان همی شد ز سالار نیو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو از آب وز لشکرش دور کرد</p></div>
<div class="m2"><p>به زین اندر افگند گرز نبرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گریزان ازو پهلوان بلند</p></div>
<div class="m2"><p>ز فتراک بگشاد پیچان کمند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هم‌آورد با گیو نزدیک شد</p></div>
<div class="m2"><p>جهان چون شب تیره تاریک شد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بپیچید گیو سرافراز یال</p></div>
<div class="m2"><p>کمند اندرافگند و کردش دوال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سر پهلوان اندر آمد به بند</p></div>
<div class="m2"><p>ز زین برگرفتش به خم کمند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>پیاده به پیش اندر افگند خوار</p></div>
<div class="m2"><p>ببردش دمان تا لب رودبار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بیفگند بر خاک و دستش ببست</p></div>
<div class="m2"><p>سلیحش بپوشید و خود بر نشست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>درفشش گرفته به چنگ اندرون</p></div>
<div class="m2"><p>بشد تا لب آب گلزریون</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو ترکان درفش سپهدار خویش</p></div>
<div class="m2"><p>بدیدند رفتند ناچار پیش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خروش آمد و نالهٔ کرنای</p></div>
<div class="m2"><p>دم نای رویین و هندی درای</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>جهاندیده گیو اندر آمد به آب</p></div>
<div class="m2"><p>چو کشتی که از باد گیرد شتاب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>برآورد گرز گران را به کفت</p></div>
<div class="m2"><p>سپه ماند از کار او در شگفت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سبک شد عنان وگران شد رکیب</p></div>
<div class="m2"><p>سر سرکشان خیره گشت از نهیب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به شمشیر و با نیزهٔ سرگرای</p></div>
<div class="m2"><p>همی کشت ازیشان یل رهنمای</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از افگنده شد روی هامون چون کوه</p></div>
<div class="m2"><p>ز یک تن شدند آن دلیران ستوه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>قفای یلان سوی او شد همه</p></div>
<div class="m2"><p>چو شیر اندر آمد به پیش رمه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو لشکر هزیمت شد از پیش گیو</p></div>
<div class="m2"><p>چنان لشکری گشن و مردان نیو</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چنان خیره برگشت و بگذاشت آب</p></div>
<div class="m2"><p>که گفتی ندیدست لشکر به خواب</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>دمان تا به نزدیک پیران رسید</p></div>
<div class="m2"><p>همی خواست از تن سرش را برید</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به خواری پیاده ببردش کشان</p></div>
<div class="m2"><p>دمان و پر از درد چون بیهشان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چنین گفت کاین بددل و بی‌وفا</p></div>
<div class="m2"><p>گرفتار شد در دم اژدها</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سیاوش به گفتار او سر بداد</p></div>
<div class="m2"><p>گر او باد شد این شود نیز باد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ابر شاه پیران گرفت آفرین</p></div>
<div class="m2"><p>خروشان ببوسید روی زمین</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>همی گفت کای شاه دانش پژوه</p></div>
<div class="m2"><p>چو خورشید تابان میان گروه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تو دانسته‌ای درد و تیمار من</p></div>
<div class="m2"><p>ز بهر تو با شاه پیگار من</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سزد گر من از چنگ این اژدها</p></div>
<div class="m2"><p>به بخت و به فر تو یابم رها</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به کیخسرو اندر نگه کرد گیو</p></div>
<div class="m2"><p>بدان تا چه فرمان دهد شاه نیو</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>فرنگیس را دید دیده پرآب</p></div>
<div class="m2"><p>زبان پر ز نفرین افراسیاب</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به گیو آن زمان گفت کای سرافراز</p></div>
<div class="m2"><p>کشیدی بسی رنج راه دراز</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چنان دان که این پیرسر پهلوان</p></div>
<div class="m2"><p>خردمند و رادست و روشن روان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>پس از داور دادگر رهنمون</p></div>
<div class="m2"><p>بدان کاو رهانید ما را ز خون</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز بد مهر او پردهٔ جان ماست</p></div>
<div class="m2"><p>وزین کردهٔ خویش زنهار خواست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بدو گفت گیو ای سر بانوان</p></div>
<div class="m2"><p>انوشه روان باش تا جاودان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>یکی سخت سوگند خوردم به ماه</p></div>
<div class="m2"><p>به تاج و به تخت شه نیک‌خواه</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>که گر دست یابم برو روز کین</p></div>
<div class="m2"><p>کنم ارغوانی ز خونش زمین</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بدو گفت کیخسرو ای شیرفش</p></div>
<div class="m2"><p>زبان را ز سوگند یزدان مکش</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>کنونش به سوگند گستاخ کن</p></div>
<div class="m2"><p>به خنجر وراگوش سوراخ کن</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو از خنجرت خون چکد بر زمین</p></div>
<div class="m2"><p>هم از مهر یاد آیدت هم ز کین</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بشد گیو و گوشش به خنجر بسفت</p></div>
<div class="m2"><p>ز سوگند برتر درشتی نگفت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چنین گفت پیران ازان پس به شاه</p></div>
<div class="m2"><p>که کلباد شد بی‌گمان با سپاه</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بفرمای کاسپم دهد باز نیز</p></div>
<div class="m2"><p>چنان دان که بخشیده‌ای جان و چیز</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بدو گفت گیو ای دلیر سپاه</p></div>
<div class="m2"><p>چرا سست گشتی به آوردگاه</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>به سوگند یابی مگر باره باز</p></div>
<div class="m2"><p>دو دستت ببندم به بند دراز</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>که نگشاید این بند تو هیچکس</p></div>
<div class="m2"><p>گشاینده گلشهر خواهیم و بس</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>کجا مهتر بانوان تو اوست</p></div>
<div class="m2"><p>وزو نیست پیدا ترا مغز و پوست</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بدان گشت همداستان پهلوان</p></div>
<div class="m2"><p>به سوگند بخرید اسپ و روان</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>که نگشاید آن بند را کس به راه</p></div>
<div class="m2"><p>ز گلشهر سازد وی آن دستگاه</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بدو داد اسپ و دو دستش ببست</p></div>
<div class="m2"><p>ازان پس بفرمود تا برنشست</p></div></div>