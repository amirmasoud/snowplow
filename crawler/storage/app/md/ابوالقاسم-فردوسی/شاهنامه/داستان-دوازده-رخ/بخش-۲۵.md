---
title: >-
    بخش ۲۵
---
# بخش ۲۵

<div class="b" id="bn1"><div class="m1"><p>به پنجم چو رهام گودرز بود</p></div>
<div class="m2"><p>که با بارمان او نبرد آزمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمان برگرفتند و تیر خدنگ</p></div>
<div class="m2"><p>برآمد خروش سواران جنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمانها همه پاک بر هم شکست</p></div>
<div class="m2"><p>سوی نیزه بردند چون باد دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو جنگی و هر دو دلیر و سوار</p></div>
<div class="m2"><p>هشیوار و دیده بسی کارزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشتند بسیار یک با دگر</p></div>
<div class="m2"><p>بپیچید رهام پرخاشخر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی نیزه انداخت بر ران اوی</p></div>
<div class="m2"><p>کز اسب اندر آمد به فرمان اوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جدا شد ز باره هم آنگاه ترک</p></div>
<div class="m2"><p>ز اسب اندر افتاد ترک سترگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پشت اندرش نیزه‌ای زد دگر</p></div>
<div class="m2"><p>سنان اندر آمد میان جگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرود آمد از باره کرد آفرین</p></div>
<div class="m2"><p>ز دادار بر بخت شاه زمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به کین سیاوش کشیدش نگون</p></div>
<div class="m2"><p>ز کینه بمالید بر روی خون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زین اندر آهخت و بستش چو سنگ</p></div>
<div class="m2"><p>سر آویخته پایها زیر تنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نشست از بر زین و اسبش کشان</p></div>
<div class="m2"><p>بیامد دوان تا به جای نشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به بالا برآمد شده شاد دل</p></div>
<div class="m2"><p>ز درد و غمان گشته آزاددل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به پیروزی شاه و تخت بلند</p></div>
<div class="m2"><p>به کام آمده زیر بخت بلند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی آفرین خواند سالار شاه</p></div>
<div class="m2"><p>ابر شاه کیخسرو و تاج و گاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که پیروزگر شاه پیروز باد</p></div>
<div class="m2"><p>همه روزگارانش نوروز باد</p></div></div>