---
title: >-
    بخش ۸
---
# بخش ۸

<div class="b" id="bn1"><div class="m1"><p>نشست از بر زین سپیده‌دمان</p></div>
<div class="m2"><p>چو شیر ژیان با یکی ترجمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیامد به نزدیک ایران سپاه</p></div>
<div class="m2"><p>پر از جنگ دل سر پر از کین شاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو پیران بدانست کو شد بجنگ</p></div>
<div class="m2"><p>بر او بر جهان گشت ز اندوه تنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجوشیدش از درد هومان جگر</p></div>
<div class="m2"><p>یکی داستان یاد کرد از پدر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که دانا به هر کار سازد درنگ</p></div>
<div class="m2"><p>سر اندر نیارد به پیکار و ننگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبکسار تندی نماید نخست</p></div>
<div class="m2"><p>به فرجام کار انده آرد درست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبانی که اندر سرش مغز نیست</p></div>
<div class="m2"><p>اگر در بارد همان نغز نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو هومان بدین رزم تندی نمود</p></div>
<div class="m2"><p>ندانم چه آرد به فرجام سود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان داورش باد فریادرس</p></div>
<div class="m2"><p>جز اویش نبینم همی یار کس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو هومان ویسه بدان رزمگاه</p></div>
<div class="m2"><p>که گودرز کشواد بد با سپاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیامد که جوید ز گردان نبرد</p></div>
<div class="m2"><p>نگهبان لشکر بدو بازخورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طلایه بیامد بر ترجمان</p></div>
<div class="m2"><p>سواران ایران همه بدگمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بپرسید کین مرد پرخاشجوی</p></div>
<div class="m2"><p>به خیره به دشت اندر آورده روی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کجا رفت خواهد همی چون نوند</p></div>
<div class="m2"><p>به چنگ اندرون گرز و بر زین کمند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به ایرانیان گفت پس ترجمان</p></div>
<div class="m2"><p>که آمد گه گرز و تیر و کمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که این شیردل نامبردار مرد</p></div>
<div class="m2"><p>همی با شما کرد خواهد نبرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر ویسگان است هومان به نام</p></div>
<div class="m2"><p>که تیغش دل شیر دارد نیام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو دیدند ایرانیان گرز اوی</p></div>
<div class="m2"><p>کمر بستن خسروی برز اوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه دست نیزه گزاران ز کار</p></div>
<div class="m2"><p>فروماند از فر آن نامدار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه یکسره بازگشتند از اوی</p></div>
<div class="m2"><p>سوی ترجمانش نهادند روی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که رو پیش هومان به ترکی زبان</p></div>
<div class="m2"><p>همه گفتهٔ ما بر او بر بخوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که ما را به جنگ تو آهنگ نیست</p></div>
<div class="m2"><p>ز گودرز دستوری جنگ نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر جنگ جوید گشاده است راه</p></div>
<div class="m2"><p>سوی نامور پهلوان سپاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز سالار گردان و گردنکشان</p></div>
<div class="m2"><p>به هومان بدادند یک یک نشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که گردان کجایند و مهتر کجاست</p></div>
<div class="m2"><p>که دارد چپ لشکر و دست راست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وز آن پس هیونی تگاور دمان</p></div>
<div class="m2"><p>طلایه برافگند زی پهلوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که هومان از آن رزمگه چون پلنگ</p></div>
<div class="m2"><p>سوی پهلوان آمد ایدر بجنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو هومان ز نزد سواران برفت</p></div>
<div class="m2"><p>بیامد به نزدیک رهام تفت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وز آنجا خروشی برآورد سخت</p></div>
<div class="m2"><p>که ای پور سالار بیدار بخت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چپ لشکر و چنگ شیران توی</p></div>
<div class="m2"><p>نگهبان سالار ایران توی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بجنبان عنان اندر این رزمگاه</p></div>
<div class="m2"><p>میان دو صف برکشیده سپاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به آورد با من ببایدت گشت</p></div>
<div class="m2"><p>سوی رود خواهی وگر سوی دشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وگر تو نیابی مگر گستهم</p></div>
<div class="m2"><p>بیاید دمان با فروهل به هم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که جوید نبردم ز جنگاوران</p></div>
<div class="m2"><p>به تیغ و سنان و به گرز گران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر آن کس که پیش من آید به کین</p></div>
<div class="m2"><p>زمانه بر او بر نوردد زمین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وگر تیغ ما را ببیند به جنگ</p></div>
<div class="m2"><p>بدرد دل شیر و چرم پلنگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنین داد رهام پاسخ بدوی</p></div>
<div class="m2"><p>که ای نامور گرد پرخاشجوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز ترکان ترا بخرد انگاشتم</p></div>
<div class="m2"><p>از این سان که هستی نپنداشتم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که تنها بدین رزمگاه آمدی</p></div>
<div class="m2"><p>دلاور به پیش سپاه آمدی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر آنی که اندر جهان تیغ‌دار</p></div>
<div class="m2"><p>نبندد کمر چون تو دیگر سوار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یکی داستان از کیان یاد کن</p></div>
<div class="m2"><p>ز فام خرد گردن آزاد کن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که هر کو به جنگ اندر آید نخست</p></div>
<div class="m2"><p>ره بازگشتن ببایدش جست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از اینها که تو نام بردی بجنگ</p></div>
<div class="m2"><p>همه جنگ را تیز دارند چنگ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ولیکن چو فرمان سالار شاه</p></div>
<div class="m2"><p>نباشد نسازد کسی رزمگاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگر جنگ گردان بجویی همی</p></div>
<div class="m2"><p>سوی پهلوان چون بپویی همی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز گودرز دستوری جنگ خواه</p></div>
<div class="m2"><p>پس از ما به جنگ اندر آهنگ خواه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدو گفت هومان که خیره مگوی</p></div>
<div class="m2"><p>بدین روی با من بهانه مجوی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تو این رزم را جای مردان گزین</p></div>
<div class="m2"><p>نه مرد سوارانی و دشت کین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>وز آنجا به قلب سپه برگذشت</p></div>
<div class="m2"><p>دمان تا بدان روی لشکر گذشت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به نزد فریبرز با ترجمان</p></div>
<div class="m2"><p>بیامد به کردار باد دمان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یکی برخروشید کای بدنشان</p></div>
<div class="m2"><p>فروبرده گردن ز گردنکشان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سواران و پیلان و زرینه کفش</p></div>
<div class="m2"><p>ترا بود با کاویانی درفش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به ترکان سپردی به روز نبرد</p></div>
<div class="m2"><p>یلانت به ایران نخوانند مرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو سالار باشی شوی زیردست</p></div>
<div class="m2"><p>کمر بندگی را ببایدت بست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سیاووش رد را برادر توی</p></div>
<div class="m2"><p>به گوهر ز سالار برتر توی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تو باشی سزاوار کین خواستن</p></div>
<div class="m2"><p>به کینه ترا باید آراستن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یکی با من اکنون به آوردگاه</p></div>
<div class="m2"><p>ببایدت گشتن به پیش سپاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به خورشید تابان برآیدت نام</p></div>
<div class="m2"><p>که پیش من اندر گذاری تو گام</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>وگر تو نیایی به جنگم رواست</p></div>
<div class="m2"><p>زواره گرازه نگر تا کجاست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کسی را ز گردان به پیش من آر</p></div>
<div class="m2"><p>که باشد ز ایرانیان نامدار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چنین داد پاسخ فریبرز باز</p></div>
<div class="m2"><p>که با شیر درنده کینه مساز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چنینست فرجام روز نبرد</p></div>
<div class="m2"><p>یکی شاد و پیروز و دیگر به درد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به پیروزی اندر بترس از گزند</p></div>
<div class="m2"><p>که یکسان نگردد سپهر بلند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>درفش ار ز من شاه بستد رواست</p></div>
<div class="m2"><p>بدان داد پیلان و لشکر که خواست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به کین سیاوش پس از کیقباد</p></div>
<div class="m2"><p>کسی کو کلاه مهی برنهاد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کمر بست تا گیتی آباد کرد</p></div>
<div class="m2"><p>سپهدار گودرز کشواد کرد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>همیشه به پیش کیان کینه‌خواه</p></div>
<div class="m2"><p>پدر بر پدر نیو و سالار شاه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>و دیگر که از گرز او بی‌گمان</p></div>
<div class="m2"><p>سرآید به سالارتان بر زمان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>سپه را به ویسه است فرمان جنگ</p></div>
<div class="m2"><p>بدو بازگردد همه نام و ننگ</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>اگر با توام جنگ فرمان دهد</p></div>
<div class="m2"><p>دلم پر ز دردست درمان دهد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ببینی که من سر چگونه ز ننگ</p></div>
<div class="m2"><p>برآرم چو پای اندر آرم بجنگ</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چنین پاسخش داد هومان که بس</p></div>
<div class="m2"><p>به گفتار بینم ترا دسترس</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بدین تیغ کاندر میان بسته‌ای</p></div>
<div class="m2"><p>گیا بر که از جنگ خود رسته‌ای</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بدین گرز جویی همی کارزار</p></div>
<div class="m2"><p>که بر ترگ و جوشن نیاید به کار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>وز آنجا بدان خیرگی بازگشت</p></div>
<div class="m2"><p>تو گفتی مگر شیر بدساز گشت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>کمربستهٔ کین آزادگان</p></div>
<div class="m2"><p>به نزدیک گودرز کشوادگان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بیامد یکی بانگ برزد بلند</p></div>
<div class="m2"><p>که ای برمنش مهتر دیوبند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>شنیدم همه هرچ گفتی به شاه</p></div>
<div class="m2"><p>وز آن پس کشیدی سپه را به راه</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چنین بود با شاه پیمان تو</p></div>
<div class="m2"><p>به پیران سالار فرمان تو</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>فرستاده کامد به توران سپاه</p></div>
<div class="m2"><p>گزین پور تو گیو لشکرپناه</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>از آن پس که سوگند خوردی به گاه</p></div>
<div class="m2"><p>به خورشید و ماه و به تخت و کلاه</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>که گر چشم من در گه کارزار</p></div>
<div class="m2"><p>به پیران برافتد برآرم دمار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چو شیر ژیان لشکر آراستی</p></div>
<div class="m2"><p>همی بآرزو جنگ ما خواستی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>کنون از پس کوه چون مستمند</p></div>
<div class="m2"><p>نشستی به کردار غرم نژند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به کردار نخچیر کز شرزه شیر</p></div>
<div class="m2"><p>گریزان و شیر از پس اندر دلیر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گزیند به بیشه درون جای تنگ</p></div>
<div class="m2"><p>نجوید ز تیمار جان نام و ننگ</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>یکی لشکرت را به هامون گذار</p></div>
<div class="m2"><p>چه داری سپاه از پس کوهسار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چنین بود پیمانت با شهریار</p></div>
<div class="m2"><p>که بر کینه گه کوه گیری حصار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بدو گفت گودرز کاندیشه کن</p></div>
<div class="m2"><p>که باشد سزا با تو گفتن سخن</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چو پاسخ بیابی کنون ز انجمن</p></div>
<div class="m2"><p>به بی دانشی بر نهی این سخن</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>تو بشناس کز شاه فرمان من</p></div>
<div class="m2"><p>همین بود سوگند و پیمان من</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>کنون آمدم با سپاهی گران</p></div>
<div class="m2"><p>از ایران گزیده دلاور سران</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>شما هم به کردار روباه پیر</p></div>
<div class="m2"><p>به بیشه در از بیم نخچیرگیر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>همی چاره سازید و دستان و بند</p></div>
<div class="m2"><p>گریزان ز گرز و سنان و کمند</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>دلیری مکن جنگ ما را مخواه</p></div>
<div class="m2"><p>که روباه با شیر ناید به راه</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چو هومان ز گودرز پاسخ شنید</p></div>
<div class="m2"><p>چو شیر اندر آن رزمگه بردمید</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>به گودرز گفت ار نیایی بجنگ</p></div>
<div class="m2"><p>تو با من نه زانست کایدت ننگ</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>از آن پس که جنگ پشن دیده‌ای</p></div>
<div class="m2"><p>سر از رزم ترکان بپیچیده‌ای</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>به لاون به جنگ آزمودی مرا</p></div>
<div class="m2"><p>به آوردگه بر ستودی مرا</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ار ایدونک هست اینک گویی همی</p></div>
<div class="m2"><p>وز این کینه کردار جویی همی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>یکی برگزین از میان سپاه</p></div>
<div class="m2"><p>که با من بگردد به آوردگاه</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>که من از فریبرز و رهام جنگ</p></div>
<div class="m2"><p>بجستم به سان دلاور پلنگ</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بگشتم سراسر همه انجمن</p></div>
<div class="m2"><p>نیاید ز گردان کسی پیش من</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به گودرز بد بند پیکارشان</p></div>
<div class="m2"><p>شنیدن نه ارزید گفتارشان</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>تو آنی که گویی به روز نبرد</p></div>
<div class="m2"><p>به خنجر کنم لاله بر کوه زرد</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>یکی با من اکنون بدین رزمگاه</p></div>
<div class="m2"><p>بگرد و به گرز گران کینه‌خواه</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>فراوان پسر داری ای نامور</p></div>
<div class="m2"><p>همه بسته بر جنگ ما بر کمر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>یکی را فرستی بر من به جنگ</p></div>
<div class="m2"><p>اگر جنگ‌جویی چه جویی درنگ</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>پس اندیشه کرد اندران پهلوان</p></div>
<div class="m2"><p>که پیشش که آید به جنگ از گوان</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>گر از نامداران هژبری دمان</p></div>
<div class="m2"><p>فرستم به نزدیک این بدگمان</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>شود کشته هومان بر این رزمگاه</p></div>
<div class="m2"><p>ز ترکان نیاید کسی کینه‌خواه</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>دل پهلوانش بپیچد به درد</p></div>
<div class="m2"><p>از آن پس به تندی نجوید نبرد</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>سپاهش به کوه کنابد شود</p></div>
<div class="m2"><p>به جنگ اندرون دست ما بد شود</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>ور از نامداران این انجمن</p></div>
<div class="m2"><p>یکی کم شود گم شود نام من</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>شکسته شود دل گوان را به جنگ</p></div>
<div class="m2"><p>نسازند زان پس به جایی درنگ</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>همان به که با او نسازیم کین</p></div>
<div class="m2"><p>بر او بر ببندیم راه کمین</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>مگر خیره گردند و جویند جنگ</p></div>
<div class="m2"><p>سپاه اندر آرند زان جای تنگ</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چنین داد پاسخ به هومان که رو</p></div>
<div class="m2"><p>به گفتار تندی و در کار نو</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چو در پیش من برگشادی زبان</p></div>
<div class="m2"><p>بدانستم از آشکارت نهان</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>که کس را ز ترکان نباشد خرد</p></div>
<div class="m2"><p>کز اندیشهٔ خویش رامش برد</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ندانی که شیر ژیان روز جنگ</p></div>
<div class="m2"><p>نیالاید از بن به روباه چنگ</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>و دیگر دو لشکر چنین ساخته</p></div>
<div class="m2"><p>همه بادپایان سر افراخته</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>به کینه دو تن پیش سازند جنگ</p></div>
<div class="m2"><p>همه نامداران بخایند چنگ</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>سپه را همه پیش باید شدن</p></div>
<div class="m2"><p>به انبوه زخمی بباید زدن</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>تو اکنون سوی لشکرت باز شو</p></div>
<div class="m2"><p>برافراز گردن به سالار نو</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>کز ایرانیان چند جستم نبرد</p></div>
<div class="m2"><p>نزد پیش من کس جز از باد سرد</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>بدان رزمگه بر شود نام تو</p></div>
<div class="m2"><p>ز پیران برآید همه کام تو</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>بدو گفت هومان به بانگ بلند</p></div>
<div class="m2"><p>که بی کردن کار گفتار چند</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>یکی داستان زد جهاندار شاه</p></div>
<div class="m2"><p>به یاد آورم اندرین کینه‌گاه</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>که تخت کیان جست خواهی مجوی</p></div>
<div class="m2"><p>چو جویی از آتش مبرتاب روی</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>ترا آرزو جنگ و پیکار نیست</p></div>
<div class="m2"><p>وگر گل چنی راه بی‌خار نیست</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>نداری ز ایران یکی شیرمرد</p></div>
<div class="m2"><p>که با من کند پیش لشکر نبرد</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>به چاره همی بازگردانیم</p></div>
<div class="m2"><p>نگیرم فریبت اگر دانیم</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>همه نامداران پرخاشجوی</p></div>
<div class="m2"><p>به گودرز گفتند کاینست روی</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>که از ما یکی را به آوردگاه</p></div>
<div class="m2"><p>فرستی به نزدیک او کینه‌خواه</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>چنین داد پاسخ که امروز روی</p></div>
<div class="m2"><p>ندارد شدن جنگ را پیش اوی</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>چو هومان ز گودرز برگشت چیر</p></div>
<div class="m2"><p>برآشفت بر سان شیر دلیر</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بخندید و روی از سپهبد بتافت</p></div>
<div class="m2"><p>سوی روزبانان لشکر شتافت</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>کمان را به زه کرد و ز ایشان چهار</p></div>
<div class="m2"><p>بیفگند ز اسب اندر آن مرغزار</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چو آن روزبانان لشکر ز دور</p></div>
<div class="m2"><p>بدیدند زخم سرافراز تور</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>رهش بازدادند و بگریختند</p></div>
<div class="m2"><p>به آورد با او نیاویختند</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>به بالا برآمد به کردار مست</p></div>
<div class="m2"><p>خروشش همی کوه را کرد پست</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>همی نیزه برگاشت بر گرد سر</p></div>
<div class="m2"><p>که هومان ویسه است پیروزگر</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>خروشیدن نای رویین ز دشت</p></div>
<div class="m2"><p>برآمد چو نیزه ز بالا بگشت</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>ز شادی دلیران توران سپاه</p></div>
<div class="m2"><p>همی ترگ سودند بر چرخ ماه</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>چو هومان بیامد بدان چیرگی</p></div>
<div class="m2"><p>بپیچید گودرز زان خیرگی</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>سپهبد پر از شرم گشته دژم</p></div>
<div class="m2"><p>گرفته بر او خشم و تندی ستم</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>به ننگ از دلیران بپالود خوی</p></div>
<div class="m2"><p>سپهبد یکی اختر افگند پی</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>کز ایشان بد این پیشدستی به خون</p></div>
<div class="m2"><p>بدانند و هم بر بدی رهنمون</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>از آن پس به گردنکشان بنگرید</p></div>
<div class="m2"><p>که تا جنگ او را که آید پدید</p></div></div>