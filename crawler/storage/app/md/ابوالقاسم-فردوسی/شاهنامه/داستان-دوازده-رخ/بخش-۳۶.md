---
title: >-
    بخش ۳۶
---
# بخش ۳۶

<div class="b" id="bn1"><div class="m1"><p>چو از روز نه ساعت اندر گذشت</p></div>
<div class="m2"><p>خور از گنبد چرخ گردان بگشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهاندار خسرو به نزد سپاه</p></div>
<div class="m2"><p>بیامد بدان دشت آوردگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پذیره شدندش سراسر سران</p></div>
<div class="m2"><p>همه نامداران و جنگاوران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر او خواندند آفرین بخردان</p></div>
<div class="m2"><p>که ای شهریار و سر موبدان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان هم همی بود بر اسب شاه</p></div>
<div class="m2"><p>بدان تا ببینند رویش سپاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر ایشان همی خواند شاه آفرین</p></div>
<div class="m2"><p>که آباد بادا به گردان زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به آیین پس پشت لشکر چو کوه</p></div>
<div class="m2"><p>همی رفت گودرز با آن گروه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر کشتگان را فگنده نگون</p></div>
<div class="m2"><p>سلیح و تن و جامه هاشان به خون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان ده مبارز کز آوردگاه</p></div>
<div class="m2"><p>بیاورده بودند گردان شاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس لشکر اندر همی راندند</p></div>
<div class="m2"><p>ابر شهریار آفرین خواندند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو گودرز نزدیک خسرو رسید</p></div>
<div class="m2"><p>پیاده شد از دور کو را بدید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ستایش کنان پهلوان سپاه</p></div>
<div class="m2"><p>بیامد بغلتید در پیش شاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه کشتگان را به خسرو نمود</p></div>
<div class="m2"><p>بگفتش که همرزم هر کس که بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گروی زره را بیاورد گیو</p></div>
<div class="m2"><p>دمان با سپهدار پیران نیو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز اسب اندر آمد سبک شهریار</p></div>
<div class="m2"><p>نیایش همی کرد بر کردگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز یزدان سپاس و بدویم پناه</p></div>
<div class="m2"><p>که او داد پیروزی و دستگاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز دادار بر پهلوان آفرین</p></div>
<div class="m2"><p>همی خواند و بر لشکرش همچنین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که ای نامداران فرخنده پی</p></div>
<div class="m2"><p>شما آتش و دشمنان خشک نی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپهدار گودرز با دودمان</p></div>
<div class="m2"><p>ز بهر دل من چو آتش دمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه جان و تن ها فدا کرده‌اند</p></div>
<div class="m2"><p>دم از شهر توران برآورده‌اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کنون گنج و شاهی مرا با شماست</p></div>
<div class="m2"><p>ندارم دریغ از شما دست راست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از آن پس بدان کشتگان بنگرید</p></div>
<div class="m2"><p>چو روی سپهدار پیران بدید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فروریخت آب از دو دیده به درد</p></div>
<div class="m2"><p>که کردار نیکی همی یاد کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به پیرانش بر دل از آن سان بسوخت</p></div>
<div class="m2"><p>تو گفتی به دلش آتشی برفروخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکی داستان زد پس از مرگ اوی</p></div>
<div class="m2"><p>به خون دو دیده بیالود روی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که بخت بد است اژدهای دژم</p></div>
<div class="m2"><p>به دام آورد شیر شرزه به دم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به مردی نیابد کسی زو رها</p></div>
<div class="m2"><p>چنین آمد این تیزچنگ اژدها</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کشیدی همه ساله تیمار من</p></div>
<div class="m2"><p>میان بسته بودی به پیکار من</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز خون سیاوش پر از درد بود</p></div>
<div class="m2"><p>بدانگه کسی را نیازرد بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان مهربان بود دژخیم شد</p></div>
<div class="m2"><p>وز او شهر ایران پر از بیم شد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مر او را ببرد اهرمن دل ز جای</p></div>
<div class="m2"><p>دگرگونه پیش اندر آورد پای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فراوان همی خیره دادمش پند</p></div>
<div class="m2"><p>نیامدش گفتار من سودمند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از افراسیابش نه برگشت سر</p></div>
<div class="m2"><p>کنون شهریارش چنین داد بر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مکافات او ما جز این خواستیم</p></div>
<div class="m2"><p>همی گاه و دیهیمش آراستیم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از اندیشهٔ ما سخن درگذشت</p></div>
<div class="m2"><p>فلک بر سرش بر دگرگونه گشت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به دل بر جفا کرد بر جای مهر</p></div>
<div class="m2"><p>بدین سر دگرگونه بنمود چهر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کنون پند گودرز و فرمان من</p></div>
<div class="m2"><p>بیفگند گفتار و پیمان من</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تبه کرد مهر دل پاک را</p></div>
<div class="m2"><p>به زهر اندر آمیخت تریاک را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که آمد به جنگ شما با سپاه</p></div>
<div class="m2"><p>که چندان شد از شهر ایران تباه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز توران بسیچید و آمد دمان</p></div>
<div class="m2"><p>که ژوپین گودرز بودش زمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پسر با برادر کلاه و کمر</p></div>
<div class="m2"><p>سلیح و سپاه و همه بوم و بر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بداد از پی مهر افراسیاب</p></div>
<div class="m2"><p>زمانه بر او کرد چندین شتاب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بفرمود تا مشک و کافور ناب</p></div>
<div class="m2"><p>به عنبر برآمیخته با گلاب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تنش را بیالود زان سر به سر</p></div>
<div class="m2"><p>به کافور و مشکش بیاگند سر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به دیبای رومی تن پاک اوی</p></div>
<div class="m2"><p>بپوشید آن جان ناپاک اوی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یکی دخمه فرمود خسرو به مهر</p></div>
<div class="m2"><p>بر آورده سر تا به گردان سپهر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نهاد اندر او تختهای گران</p></div>
<div class="m2"><p>چنانچون بود در خور مهتران</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نهادند مر پهلوان را به گاه</p></div>
<div class="m2"><p>کمر بر میان و به سر بر کلاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چنین است کردار این پر فریب</p></div>
<div class="m2"><p>چه مایه فراز است و چندی نشیب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خردمند را دل ز کردار اوی</p></div>
<div class="m2"><p>بماند همی خیره از کار اوی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از آن پس گروی زره را بدید</p></div>
<div class="m2"><p>یکی باد سرد از جگر برکشید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نگه کرد خسرو بدان زشت روی</p></div>
<div class="m2"><p>چو دیوی به سر بر فروهشته موی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همی گفت کای کردگار جهان</p></div>
<div class="m2"><p>تو دانی همی آشکار و نهان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همانا که کاووس بد کرده بود</p></div>
<div class="m2"><p>به پاداش از او زهر و کین آزمود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>که دیوی چنین بر سیاوش گماشت</p></div>
<div class="m2"><p>ندانم جز این کینه بر دل چه داشت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ولیکن به پیروزی یک خدای</p></div>
<div class="m2"><p>جهاندار نیکی ده و رهنمای</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که خون سیاوش ز افراسیاب</p></div>
<div class="m2"><p>بخواهم بدین کینه گیرم شتاب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گروی زره را گره تا گره</p></div>
<div class="m2"><p>بفرمود تا برکشیدند زه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو بندش جدا شد سرش را ز بند</p></div>
<div class="m2"><p>بریدند همچون سر گوسفند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بفرمود او را فگندن به آب</p></div>
<div class="m2"><p>بگفتا چنین بینم افراسیاب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ببد شاه چندی بر آن رزمگاه</p></div>
<div class="m2"><p>بدان تا کند ساز کار سپاه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دهد پادشاهی که را در خور است</p></div>
<div class="m2"><p>کسی کز در خلعت و افسر است</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به گودرز داد آن زمان اصفهان</p></div>
<div class="m2"><p>کلاه بزرگی و تخت مهان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به اندازه اندرخور کارشان</p></div>
<div class="m2"><p>بیاراست خلعت سزاوارشان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از آنها که بودند مانده به جای</p></div>
<div class="m2"><p>که پیرانشان بد سر و کدخدای</p></div></div>