---
title: >-
    بخش ۱۴
---
# بخش ۱۴

<div class="b" id="bn1"><div class="m1"><p>کس آمد به ماهوی سوری بگفت</p></div>
<div class="m2"><p>که شاه جهان گشت با خاک جفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سکوبا و قسیس و رهبان روم</p></div>
<div class="m2"><p>همه سوگواران آن مرز و بوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برفتند با مویه برنا و پیر</p></div>
<div class="m2"><p>تن شاه بردند زان آبگیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی دخمه کردند او رابه باغ</p></div>
<div class="m2"><p>بلند و بزرگیش برتر ز راغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین گفت ماهوی بدبخت و شوم</p></div>
<div class="m2"><p>که ایران نبد پیش از این خویش روم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرستاد تا هر که آن دخمه کرد</p></div>
<div class="m2"><p>هم آنکس کزان کار تیمار خورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکشتند و تاراج کردند مرز</p></div>
<div class="m2"><p>چنین بود ماهوی را کام و ارز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازان پس بگرد جهان بنگرید</p></div>
<div class="m2"><p>ز تخم بزرگان کسی را ندید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان تاج با او بد و مهر شاه</p></div>
<div class="m2"><p>شبان زاده را آرزو کردگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه رازدارانش را پیش خواند</p></div>
<div class="m2"><p>سخن هرچ بودش به دل در براند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دستور گفت ای جهاندیده مرد</p></div>
<div class="m2"><p>فراز آمد آن روز ننگ و نبرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه گنجست بامن نه نام و نژاد</p></div>
<div class="m2"><p>همی‌داد خواهم سرخود بباد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر انگشتری یزدگردست نام</p></div>
<div class="m2"><p>به شمشیر بر من نگردند رام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه شهر ایران ورا بنده بود</p></div>
<div class="m2"><p>اگر خویش بد ار پراگنده بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نخواند مرا مرد داننده شاه</p></div>
<div class="m2"><p>نه بر مهرم آرام گیرد سپاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جزین بود چاره مرا در نهان</p></div>
<div class="m2"><p>چرا ریختم خون شاه جهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه شب ز اندیشه پر خون بدم</p></div>
<div class="m2"><p>جهاندار داند که من چون بدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدو رای زن گفت که اکنون گذشت</p></div>
<div class="m2"><p>ازین کار گیتی پر آواز گشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنون بازجویی همی کارخویش</p></div>
<div class="m2"><p>که بگسستی آن رشتهٔ تار خویش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کنون او بدخمه درون خاک شد</p></div>
<div class="m2"><p>روان ورا زهر تریاک شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهاندیدگان را همه گرد کن</p></div>
<div class="m2"><p>زبان تیز گردان به شیرین سخن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنین گوی کاین تاج انگشتری</p></div>
<div class="m2"><p>مرا شاه داد از پی مهتری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو دانست کامد ز ترکان سپاه</p></div>
<div class="m2"><p>چوشب تیره‌تر شد مرا خواند شاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا گفت چون خاست باد نبرد</p></div>
<div class="m2"><p>که داند به گیتی که برکیست گرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تواین تاج و انگشتری را بدار</p></div>
<div class="m2"><p>بود روز کین تاجت آید به کار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرانیست چیزی جزین در جهان</p></div>
<div class="m2"><p>همانا که هست این ز تازی نهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو زین پس به دشمن مده گاه من</p></div>
<div class="m2"><p>نگه دار هم زین نشان راه من</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من این تاج میراث دارم ز شاه</p></div>
<div class="m2"><p>به فرمان او بر نشینم به گاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدین چاره ده بند بد را فروغ</p></div>
<div class="m2"><p>که داند که این راستست از دروغ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چوبشنید ماهوی گفتا که زه</p></div>
<div class="m2"><p>تو دستوری و بر تو بر نیست مه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همه مهتران را ز لشکر بخواند</p></div>
<div class="m2"><p>وزین گونه چندین سخنها براند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدانست لشکر که این نیست راست</p></div>
<div class="m2"><p>به شوخی ورا سر بریدن سزاست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی پهلوان گفت کاین کار تست</p></div>
<div class="m2"><p>سخن گر درستست گر نادرست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چوبشنید بر تخت شاهی نشست</p></div>
<div class="m2"><p>به افسون خراسانش آمد بدست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ببخشید روی زمین بر مهان</p></div>
<div class="m2"><p>منم گفت با مهر شاه جهان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جهان را سراسر به بخشش گرفت</p></div>
<div class="m2"><p>ستاره نظاره برو ای شگفت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به مهتر پسر داد بلخ و هری</p></div>
<div class="m2"><p>فرستاد بر هر سوی لشکری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بد اندیشگان را همه برکشید</p></div>
<div class="m2"><p>بدانسان که از گوهر او سزید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدان را بهرجای سالار کرد</p></div>
<div class="m2"><p>خردمند را سرنگونسارکرد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو زیراندر آمد سر راستی</p></div>
<div class="m2"><p>پدید آمد از هر سوی کاستی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چولشکر فراوان شد و خواسته</p></div>
<div class="m2"><p>دل مرد بی تن شد آراسته</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سپه را درم داد و آباد کرد</p></div>
<div class="m2"><p>سر دوده خویش پرباد کرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به آموی شد پهلو پیش رو</p></div>
<div class="m2"><p>ابا لشکری جنگ سازان نو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>طلایه به پیش سپاه اندرون</p></div>
<div class="m2"><p>جهان دیده‌ای نام او گرستون</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به شهر بخارا نهادند روی</p></div>
<div class="m2"><p>چنان ساخته لشکری جنگجوی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدو گفت ما را سمرقند و چاچ</p></div>
<div class="m2"><p>بباید گرفتن بدین مهر و تاج</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به فرمان شاه جهان یزدگرد</p></div>
<div class="m2"><p>که سالار بد او بر این هفت گرد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز بیژن بخواهم به شمشیر کین</p></div>
<div class="m2"><p>کزو تیره شد بخت ایران زمین</p></div></div>