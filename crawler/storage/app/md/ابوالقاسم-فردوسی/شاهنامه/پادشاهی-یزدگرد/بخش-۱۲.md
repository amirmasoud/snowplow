---
title: >-
    بخش ۱۲
---
# بخش ۱۲

<div class="b" id="bn1"><div class="m1"><p>چو بشنید ماهوی بیدادگر</p></div>
<div class="m2"><p>سخن‌ها کجا گفت او را پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین گفت با آسیابان که خیز</p></div>
<div class="m2"><p>سواران ببر خون دشمن بریز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بشنید از او آسیابان سخن</p></div>
<div class="m2"><p>نه سر دید از آن کار پیدا نه بن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبانگاه نیران خرداد ماه</p></div>
<div class="m2"><p>سوی آسیا رفت نزدیک شاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز درگاه ماهوی چون شد برون</p></div>
<div class="m2"><p>دو دیده پر از آب، دل پر ز خون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سواران فرستاد ماهوی زود</p></div>
<div class="m2"><p>پس آسیابان به کردار دود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بفرمود تا تاج و آن گوشوار</p></div>
<div class="m2"><p>همان مهر و آن جامهٔ شاه‌وار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نباید که یک‌سر پر از خون کنند</p></div>
<div class="m2"><p>ز تن جامهٔ شاه بیرون کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بشد آسیابان دو دیده پر آب</p></div>
<div class="m2"><p>به زردی دو رخساره چون آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همی گفت کای روشن کردگار</p></div>
<div class="m2"><p>تویی برتر از گردش روزگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو زین ناپسندیده فرمان او</p></div>
<div class="m2"><p>هم اکنون بپیچان تن و جان او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر شاه شد دل پر از شرم و باک</p></div>
<div class="m2"><p>رخانش پر آب و دهانش چو خاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به نزدیک تنگ اندر آمد به هوش</p></div>
<div class="m2"><p>چنان چون کسی راز گوید به گوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی دشنه زد بر تهیگاه شاه</p></div>
<div class="m2"><p>رهاشد به زخم اندر از شاه آه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به خاک اندر آمد سر و افسرش</p></div>
<div class="m2"><p>همان نان کشکین به پیش اندرش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر راه یابد کسی زین جهان</p></div>
<div class="m2"><p>بباشد، ندارد خرد در نهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز پرورده سیر آید این هفت گرد</p></div>
<div class="m2"><p>شود کشته بر بی‌گنه یزدگرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر این گونه بر تاجداری بمرد</p></div>
<div class="m2"><p>که از لشکر او سواری نبرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خرد نیست با گرد گردان سپهر</p></div>
<div class="m2"><p>نه پیدا بود رنج و خشمش ز مهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همان به که گیتی نبینی به چشم</p></div>
<div class="m2"><p>نداری ز کردار او مهر و خشم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سواران ماهوی شوریده بخت</p></div>
<div class="m2"><p>بدیدند کآن خسروانی درخت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز تخت و ز آوردگه آرمید</p></div>
<div class="m2"><p>بشد هر کسی روی او را بدید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گشادند بند قبای بنفش</p></div>
<div class="m2"><p>همان افسر و طوق و زرّینه کفش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فگنده تن شاه ایران به خاک</p></div>
<div class="m2"><p>پر از خون و پهلو به شمشیر چاک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز پیش شهنشاه برخاستند</p></div>
<div class="m2"><p>زبان را به نفرین بیاراستند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که ماهوی را باد تن همچنین</p></div>
<div class="m2"><p>پر از خون فگنده به روی زمین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به نزدیک ماهوی رفتند زود</p></div>
<div class="m2"><p>ابا یاره و گوهر نابسود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به ماهوی گفتند کآن شهریار</p></div>
<div class="m2"><p>بر آمد ز آرام و از کارزار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بفرمود کو را به هنگام خواب</p></div>
<div class="m2"><p>از آن آسیا افگنند اندر آب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بشد تیز بد مهر دو پیشکار</p></div>
<div class="m2"><p>کشیدند پر خون تن شهریار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کجا ارج آن کشته نشناختند</p></div>
<div class="m2"><p>به گرداب زرق اندر انداختند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو شب روز شد مردم آمد پدید</p></div>
<div class="m2"><p>دو مرد گرانمایه آنجا رسید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از آن سوگواران پرهیزگار</p></div>
<div class="m2"><p>بیامد یکی بر لب جویبار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تن او برهنه بدید اندر آب</p></div>
<div class="m2"><p>بشورید و آمد هم اندر شتاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنین تا در خانه راهب رسید</p></div>
<div class="m2"><p>بدان سوگواران بگفت آن چه دید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که شاه زمانه به غرق اندرست</p></div>
<div class="m2"><p>برهنه به گرداب زرق اندرست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>برفتند زان سوگواران بسی</p></div>
<div class="m2"><p>سکوبا و رهبان ز هر در کسی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خروشی بر آمد ز راهب به درد</p></div>
<div class="m2"><p>که ای تاجور شاه آزاد مرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنین گفت راهب که این کس ندید</p></div>
<div class="m2"><p>نه پیش از مسیح این سخن کس شنید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که بر شهریاری زند بنده‌ای</p></div>
<div class="m2"><p>یکی بد نژادی و افگنده‌ای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بپرورد تا بر تنش بد رسد</p></div>
<div class="m2"><p>از این بهر ماهوی نفرین سزد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دریغ آن سر و تاج و بالای تو</p></div>
<div class="m2"><p>دریغ آن دل و دانش و رای تو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دریغ آن سر تخمهٔ اردشیر</p></div>
<div class="m2"><p>دریغ این جوان و سوار هژیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تنومند بودی خرد با روان</p></div>
<div class="m2"><p>ببردی خبر زین به نوشین‌روان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که در آسیا ماه روی تو را</p></div>
<div class="m2"><p>جهاندار و دیهیم جوی تو را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به دشنه جگرگاه بشکافتند</p></div>
<div class="m2"><p>برهنه به آب اندر انداختند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سکوبا از آن سوگواران چهار</p></div>
<div class="m2"><p>برهنه شدند اندران جویبار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گشاده تن شهریار جوان</p></div>
<div class="m2"><p>نبیره‌ی جهاندار نوشین‌روان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به خشکی کشیدند زان آبگیر</p></div>
<div class="m2"><p>بسی مویه کردند برنا و پیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به باغ اندرون دخمه‌ای ساختند</p></div>
<div class="m2"><p>سرش را به ابر اندر افراختند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سر زخم آن دشنه کردند خشک</p></div>
<div class="m2"><p>به دبق و به قیر و به کافور و مشک</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بیاراستندش به دیبای زرد</p></div>
<div class="m2"><p>قصب زیر و دستی زبر لاژورد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>می و مشک و کافور و چندی گلاب</p></div>
<div class="m2"><p>سکوبا بیندود بر جای خواب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چه گفت آن گرانمایه دهقان مرو</p></div>
<div class="m2"><p>که بنهفت بالای آن زاد سرو</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>که بخشش ز کوشش بود در نهان</p></div>
<div class="m2"><p>که خشنود بیرون شود زین جهان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دگر گفت اگر چند خندان بود</p></div>
<div class="m2"><p>چنان دان که از دردمندان بود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که از چرخ گردان پذیرد فریب</p></div>
<div class="m2"><p>که او را نماید فراز و شیب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دگر گفت کآن را تو دانا مخوان</p></div>
<div class="m2"><p>که تن را پرستد نه راه روان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همی‌خواسته جوید و نام بد</p></div>
<div class="m2"><p>بترسد روانش ز فرجام بد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دگر گفت اگر شاه لب را ببست</p></div>
<div class="m2"><p>نبیند همی تاج و تخت نشست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نه مهر و پرستندهٔ بارگاه</p></div>
<div class="m2"><p>نه افسر نه کشور نه تاج و کلاه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دگر گفت کز خوب گفتار اوی</p></div>
<div class="m2"><p>ستایش ندارم سزاوار اوی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همی سرو کشت او به باغ بهشت</p></div>
<div class="m2"><p>ببیند روانش درختی که کشت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دگر گفت یزدان روانت ببرد</p></div>
<div class="m2"><p>تنت را بدین سوگواران سپرد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>روان تو را سودمند این بود</p></div>
<div class="m2"><p>تن بد کنش را گزند این بود</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کنون در بهشت است بازار شاه</p></div>
<div class="m2"><p>به دوزخ کند جان بدخواه راه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دگر گفت کای شاه دانش پذیر</p></div>
<div class="m2"><p>که با شهریاری و با اردشیر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>درودی همان بر که کشتی به باغ</p></div>
<div class="m2"><p>درفشان شد آن خسروانی چراغ</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دگر گفت کای شهریار جوان</p></div>
<div class="m2"><p>بخفتی و بیدار بودت روان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>لبت خامش و جان به چندین گله</p></div>
<div class="m2"><p>برفت و تنت ماند ایدر یله</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تو بیکاری و جان به کار اندر است</p></div>
<div class="m2"><p>تن بد سگالت به بار اندر است</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بگوید روان گر زبان بسته شد</p></div>
<div class="m2"><p>بیاسود جان گر تنت خسته شد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>اگر دست بیکار گشت از عنان</p></div>
<div class="m2"><p>روانت به چنگ اندر آرد سنان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دگر گفت کای نامبردار نو</p></div>
<div class="m2"><p>تو رفتی و کردار شد پیش رو</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>تو را در بهشت است تخت این بس است</p></div>
<div class="m2"><p>زمین بلا بهر دیگر کس است</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دگر گفت کآن کس که او چون تو کشت</p></div>
<div class="m2"><p>ببیند کنون روزگار درشت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سقف گفت ما بندگان تویم</p></div>
<div class="m2"><p>نیایش کن پاک جان تویم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>که این دخمه پر لاله باغ تو باد</p></div>
<div class="m2"><p>کفن دشت شادی و راغ تو باد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بگفتند و تابوت برداشتند</p></div>
<div class="m2"><p>ز هامون سوی دخمه بگذاشتند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بر آن خوابگه رفت ناکام شاه</p></div>
<div class="m2"><p>سر آمد بر او رنج و تخت و کلاه</p></div></div>