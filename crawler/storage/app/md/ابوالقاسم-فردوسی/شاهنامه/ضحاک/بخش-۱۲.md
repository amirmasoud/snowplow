---
title: >-
    بخش ۱۲
---
# بخش ۱۲

<div class="b" id="bn1"><div class="m1"><p>جهاندار ضحاک ازان گفت‌گوی</p></div>
<div class="m2"><p>به جوش آمد و زود بنهاد روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو شب گردش روز پرگار زد</p></div>
<div class="m2"><p>فروزنده را مهره در قار زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفرمود تا برنهادند زین</p></div>
<div class="m2"><p>بران باد پایان باریک بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیامد دمان با سپاهی گران</p></div>
<div class="m2"><p>همه نره دیوان جنگ آوران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بی‌راه مر کاخ را بام و در</p></div>
<div class="m2"><p>گرفت و به کین اندر آورد سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپاه فریدون چو آگه شدند</p></div>
<div class="m2"><p>همه سوی آن راه بی‌ره شدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اسپان جنگی فرو ریختند</p></div>
<div class="m2"><p>در آن جای تنگی برآویختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه بام و در مردم شهر بود</p></div>
<div class="m2"><p>کسی کش ز جنگ آوری بهر بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه در هوای فریدون بدند</p></div>
<div class="m2"><p>که از درد ضحاک پرخون بدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز دیوارها خشت و ز بام سنگ</p></div>
<div class="m2"><p>به کوی اندرون تیغ و تیر و خدنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببارید چون ژاله ز ابر سیاه</p></div>
<div class="m2"><p>پئی را نبد بر زمین جایگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به شهر اندرون هر که برنا بدند</p></div>
<div class="m2"><p>چه پیران که در جنگ دانا بدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوی لشکر آفریدون شدند</p></div>
<div class="m2"><p>ز نیرنگ ضحاک بیرون شدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خروشی برآمد ز آتشکده</p></div>
<div class="m2"><p>که بر تخت اگر شاه باشد دده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه پیر و برناش فرمان بریم</p></div>
<div class="m2"><p>یکایک ز گفتار او نگذریم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نخواهیم برگاه ضحاک را</p></div>
<div class="m2"><p>مرآن اژدهادوش ناپاک را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپاهی و شهری به کردار کوه</p></div>
<div class="m2"><p>سراسر به جنگ اندر آمد گروه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آن شهر روشن یکی تیره گرد</p></div>
<div class="m2"><p>برآمد که خورشید شد لاجورد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پس آنگاه ضحاک شد چاره جوی</p></div>
<div class="m2"><p>ز لشکر سوی کاخ بنهاد روی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به آهن سراسر بپوشید تن</p></div>
<div class="m2"><p>بدان تا نداند کسش ز انجمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به چنگ اندرون شست یازی کمند</p></div>
<div class="m2"><p>برآمد بر بام کاخ بلند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدید آن سیه نرگس شهرناز</p></div>
<div class="m2"><p>پر از جادویی با فریدون به راز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو رخساره روز و دو زلفش چو شب</p></div>
<div class="m2"><p>گشاده به نفرین ضحاک لب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به مغز اندرش آتش رشک خاست</p></div>
<div class="m2"><p>به ایوان کمند اندر افگند راست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه از تخت یاد و نه جان ارجمند</p></div>
<div class="m2"><p>فرود آمد از بام کاخ بلند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به دست اندرش آبگون دشنه بود</p></div>
<div class="m2"><p>به خون پری چهرگان تشنه بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز بالا چو پی بر زمین برنهاد</p></div>
<div class="m2"><p>بیامد فریدون به کردار باد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بران گرزهٔ گاوسر دست برد</p></div>
<div class="m2"><p>بزد بر سرش ترگ بشکست خرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیامد سروش خجسته دمان</p></div>
<div class="m2"><p>مزن گفت کاو را نیامد زمان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیدون شکسته ببندش چو سنگ</p></div>
<div class="m2"><p>ببر تا دو کوه آیدت پیش تنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به کوه اندرون به بود بند او</p></div>
<div class="m2"><p>نیاید برش خویش و پیوند او</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فریدون چو بشنید ناسود دیر</p></div>
<div class="m2"><p>کمندی بیاراست از چرم شیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به تندی ببستش دو دست و میان</p></div>
<div class="m2"><p>که نگشاید آن بند پیل ژیان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نشست از بر تخت زرین او</p></div>
<div class="m2"><p>بیفگند ناخوب آیین او</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بفرمود کردن به در بر خروش</p></div>
<div class="m2"><p>که هر کس که دارید بیدار هوش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نباید که باشید با ساز جنگ</p></div>
<div class="m2"><p>نه زین گونه جوید کسی نام و ننگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سپاهی نباید که به پیشه‌ور</p></div>
<div class="m2"><p>به یک روی جویند هر دو هنر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی کارورز و یکی گرزدار</p></div>
<div class="m2"><p>سزاوار هر کس پدیدست کار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو این کار آن جوید آن کار این</p></div>
<div class="m2"><p>پرآشوب گردد سراسر زمین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به بند اندرست آنکه ناپاک بود</p></div>
<div class="m2"><p>جهان را ز کردار او باک بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شما دیر مانید و خرم بوید</p></div>
<div class="m2"><p>به رامش سوی ورزش خود شوید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شنیدند یکسر سخنهای شاه</p></div>
<div class="m2"><p>ازان مرد پرهیز با دستگاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وزان پس همه نامداران شهر</p></div>
<div class="m2"><p>کسی کش بد از تاج وز گنج بهر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>برفتند با رامش و خواسته</p></div>
<div class="m2"><p>همه دل به فرمانش آراسته</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فریدون فرزانه بنواختشان</p></div>
<div class="m2"><p>براندازه بر پایگه ساختشان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همی پندشان داد و کرد آفرین</p></div>
<div class="m2"><p>همی یاد کرد از جهان آفرین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همی گفت کاین جایگاه منست</p></div>
<div class="m2"><p>به نیک اختر بومتان روشنست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که یزدان پاک از میان گروه</p></div>
<div class="m2"><p>برانگیخت ما را ز البرز کوه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بدان تا جهان از بد اژدها</p></div>
<div class="m2"><p>بفرمان گرز من آید رها</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو بخشایش آورد نیکی دهش</p></div>
<div class="m2"><p>به نیکی بباید سپردن رهش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>منم کدخدای جهان سر به سر</p></div>
<div class="m2"><p>نشاید نشستن به یک جای بر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>وگرنه من ایدر همی بودمی</p></div>
<div class="m2"><p>بسی با شما روز پیمودمی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مهان پیش او خاک دادند بوس</p></div>
<div class="m2"><p>ز درگاه برخاست آوای کوس</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دمادم برون رفت لشکر ز شهر</p></div>
<div class="m2"><p>وزان شهر نایافته هیچ بهر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ببردند ضحاک را بسته خوار</p></div>
<div class="m2"><p>به پشت هیونی برافگنده زار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همی راند ازین گونه تا شیرخوان</p></div>
<div class="m2"><p>جهان را چو این بشنوی پیر خوان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بسا روزگارا که بر کوه و دشت</p></div>
<div class="m2"><p>گذشتست و بسیار خواهد گذشت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بران گونه ضحاک را بسته سخت</p></div>
<div class="m2"><p>سوی شیر خوان برد بیدار بخت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همی راند او را به کوه اندرون</p></div>
<div class="m2"><p>همی خواست کارد سرش را نگون</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بیامد هم آنگه خجسته سروش</p></div>
<div class="m2"><p>به خوبی یکی راز گفتش به گوش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>که این بسته را تا دماوند کوه</p></div>
<div class="m2"><p>ببر همچنان تازیان بی‌گروه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مبر جز کسی را که نگزیردت</p></div>
<div class="m2"><p>به هنگام سختی به بر گیردت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بیاورد ضحاک را چون نوند</p></div>
<div class="m2"><p>به کوه دماوند کردش ببند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به کوه اندرون تنگ جایش گزید</p></div>
<div class="m2"><p>نگه کرد غاری بنش ناپدید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بیاورد مسمارهای گران</p></div>
<div class="m2"><p>به جایی که مغزش نبود اندران</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>فرو بست دستش بر آن کوه باز</p></div>
<div class="m2"><p>بدان تا بماند به سختی دراز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ببستش بران گونه آویخته</p></div>
<div class="m2"><p>وزو خون دل بر زمین ریخته</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ازو نام ضحاک چون خاک شد</p></div>
<div class="m2"><p>جهان از بد او همه پاک شد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گسسته شد از خویش و پیوند او</p></div>
<div class="m2"><p>بمانده بدان گونه در بند او</p></div></div>