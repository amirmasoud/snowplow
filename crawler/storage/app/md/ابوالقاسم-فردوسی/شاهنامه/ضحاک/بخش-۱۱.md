---
title: >-
    بخش ۱۱
---
# بخش ۱۱

<div class="b" id="bn1"><div class="m1"><p>چوکشور ز ضحاک بودی تهی</p></div>
<div class="m2"><p>یکی مایه ور بد بسان رهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که او داشتی گنج و تخت و سرای</p></div>
<div class="m2"><p>شگفتی به دل سوزگی کدخدای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ورا کندرو خواندندی بنام</p></div>
<div class="m2"><p>به کندی زدی پیش بیداد گام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کاخ اندر آمد دوان کند رو</p></div>
<div class="m2"><p>در ایوان یکی تاجور دید نو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشسته به آرام در پیشگاه</p></div>
<div class="m2"><p>چو سرو بلند از برش گرد ماه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز یک دست سرو سهی شهرناز</p></div>
<div class="m2"><p>به دست دگر ماه‌روی ار نواز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه شهر یکسر پر از لشکرش</p></div>
<div class="m2"><p>کمربستگان صف زده بر درش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه آسیمه گشت و نه پرسید راز</p></div>
<div class="m2"><p>نیایش کنان رفت و بردش نماز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو آفرین کرد کای شهریار</p></div>
<div class="m2"><p>همیشه بزی تا بود روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خجسته نشست تو با فرهی</p></div>
<div class="m2"><p>که هستی سزاوار شاهنشهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان هفت کشور ترا بنده باد</p></div>
<div class="m2"><p>سرت برتر از ابر بارنده باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فریدونش فرمود تا رفت پیش</p></div>
<div class="m2"><p>بکرد آشکارا همه راز خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بفرمود شاه دلاور بدوی</p></div>
<div class="m2"><p>که رو آلت تخت شاهی بجوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نبیذ آر و رامشگران را بخوان</p></div>
<div class="m2"><p>بپیمای جام و بیارای خوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کسی کاو به رامش سزای منست</p></div>
<div class="m2"><p>به دانش همان دلزدای منست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیار انجمن کن بر تخت من</p></div>
<div class="m2"><p>چنان چون بود در خور بخت من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بشنید از او این سخن کدخدای</p></div>
<div class="m2"><p>بکرد آنچه گفتش بدو رهنمای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>می روشن آورد و رامشگران</p></div>
<div class="m2"><p>همان در خورش باگهر مهتران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فریدون غم افکند و رامش گزید</p></div>
<div class="m2"><p>شبی کرد جشنی چنان چون سزید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو شد رام گیتی دوان کندرو</p></div>
<div class="m2"><p>برون آمد از پیش سالار نو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشست از بر بارهٔ راه جوی</p></div>
<div class="m2"><p>سوی شاه ضحاک بنهاد روی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بیامد چو پیش سپهبد رسید</p></div>
<div class="m2"><p>سراسر بگفت آنچه دید و شنید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو گفت کای شاه گردنکشان</p></div>
<div class="m2"><p>به برگشتن کارت آمد نشان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سه مرد سرافراز با لشکری</p></div>
<div class="m2"><p>فراز آمدند از دگر کشوری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ازان سه یکی کهتر اندر میان</p></div>
<div class="m2"><p>به بالای سرو و به چهر کیان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به سالست کهتر فزونیش بیش</p></div>
<div class="m2"><p>از آن مهتران او نهد پای پیش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی گرز دارد چو یک لخت کوه</p></div>
<div class="m2"><p>همی تابد اندر میان گروه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به اسپ اندر آمد بایوان شاه</p></div>
<div class="m2"><p>دو پرمایه با او همیدون براه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیامد به تخت کئی بر نشست</p></div>
<div class="m2"><p>همه بند و نیرنگ تو کرد پست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر آنکس که بود اندر ایوان تو</p></div>
<div class="m2"><p>ز مردان مرد و ز دیوان تو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سر از پای یکسر فروریختشان</p></div>
<div class="m2"><p>همه مغز با خون برامیختشان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدو گفت ضحاک شاید بدن</p></div>
<div class="m2"><p>که مهمان بود شاد باید بدن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنین داد پاسخ ورا پیشکار</p></div>
<div class="m2"><p>که مهمان ابا گرزهٔ گاوسار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به مردی نشیند به آرام تو</p></div>
<div class="m2"><p>زتاج و کمر بسترد نام تو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به آیین خویش آورد ناسپاس</p></div>
<div class="m2"><p>چنین گر تو مهمان شناسی شناس</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدو گفت ضحاک چندین منال</p></div>
<div class="m2"><p>که مهمان گستاخ بهتر به فال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنین داد پاسخ بدو کندرو</p></div>
<div class="m2"><p>که آری شنیدم تو پاسخ شنو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گرین نامور هست مهمان تو</p></div>
<div class="m2"><p>چه کارستش اندر شبستان تو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که با دختران جهاندار جم</p></div>
<div class="m2"><p>نشیند زند رای بر بیش و کم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به یک دست گیرد رخ شهرناز</p></div>
<div class="m2"><p>به دیگر عقیق لب ارنواز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شب تیره گون خود بترزین کند</p></div>
<div class="m2"><p>به زیر سر از مشک بالین کند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چومشک آن دو گیسوی دو ماه تو</p></div>
<div class="m2"><p>که بودند همواره دلخواه تو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بگیرد ببرشان چو شد نیم مست</p></div>
<div class="m2"><p>بدین گونه مهمان نباید بدست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>برآشفت ضحاک برسان کرگ</p></div>
<div class="m2"><p>شنید آن سخن کارزو کرد مرگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به دشنام زشت و به آواز سخت</p></div>
<div class="m2"><p>شگفتی بشورید با شوربخت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدو گفت هرگز تو در خان من</p></div>
<div class="m2"><p>ازین پس نباشی نگهبان من</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چنین داد پاسخ ورا پیشکار</p></div>
<div class="m2"><p>که ایدون گمانم من ای شهریار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کزان بخت هرگز نباشدت بهر</p></div>
<div class="m2"><p>به من چون دهی کدخدایی شهر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو بی‌بهره باشی ز گاه مهی</p></div>
<div class="m2"><p>مرا کار سازندگی چون دهی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چرا تو نسازی همی کار خویش</p></div>
<div class="m2"><p>که هرگز نیامدت ازین کار پیش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز تاج بزرگی چو موی از خمیر</p></div>
<div class="m2"><p>برون آمدی مهترا چاره‌گیر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ترا دشمن آمد به گه برنشست</p></div>
<div class="m2"><p>یکی گرزهٔ گاوپیکر به دست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همه بند و نیرنگت از رنگ برد</p></div>
<div class="m2"><p>دلارام بگرفت و گاهت سپرد</p></div></div>