---
title: >-
    بخش ۲
---
# بخش ۲

<div class="b" id="bn1"><div class="m1"><p>چنان بد که هر شب دو مرد جوان</p></div>
<div class="m2"><p>چه کهتر چه از تخمهٔ پهلوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشگر ببردی به ایوان شاه</p></div>
<div class="m2"><p>همی ساختی راه درمان شاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکشتی و مغزش بپرداختی</p></div>
<div class="m2"><p>مر آن اژدها را خورش ساختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو پاکیزه از گوهر پادشا</p></div>
<div class="m2"><p>دو مرد گرانمایه و پارسا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی نام ارمایل پاک‌دین</p></div>
<div class="m2"><p>دگر نام گرمایل پیشبین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان بد که بودند روزی به هم</p></div>
<div class="m2"><p>سخن رفت هر گونه از بیش و کم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بیدادگر شاه و ز لشکرش</p></div>
<div class="m2"><p>و زان رسم‌های بد اندر خورش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی گفت ما را به خوالیگری</p></div>
<div class="m2"><p>بباید بر شاه رفت آوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و زان پس یکی چاره‌ای ساختن</p></div>
<div class="m2"><p>ز هر گونه اندیشه انداختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر زین دو تن را که ریزند خون</p></div>
<div class="m2"><p>یکی را توان آوریدن برون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برفتند و خوالیگری ساختند</p></div>
<div class="m2"><p>خورش‌ها و اندازه بشناختند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خورش خانهٔ پادشاه جهان</p></div>
<div class="m2"><p>گرفت آن دو بیدار دل در نهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو آمد به هنگام خون ریختن</p></div>
<div class="m2"><p>به شیرین روان اندر آویختن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آن روزبانان مردم‌کشان</p></div>
<div class="m2"><p>گرفته دو مرد جوان را کشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زنان پیش خوالیگران تاختند</p></div>
<div class="m2"><p>ز بالا به روی اندر انداختند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پر از درد خوالیگران را جگر</p></div>
<div class="m2"><p>پر از خون دو دیده پر از کینه سر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همی بنگرید این بدان آن بدین</p></div>
<div class="m2"><p>ز کردار بیداد شاه زمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آن دو یکی را بپرداختند</p></div>
<div class="m2"><p>جز این چاره‌ای نیز نشناختند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برون کرد مغز سر گوسفند</p></div>
<div class="m2"><p>بیامیخت با مغز آن ارجمند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی را به جان داد زنهار و گفت</p></div>
<div class="m2"><p>نگر تا بیاری سر اندر نهفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نگر تا نباشی به آباد شهر</p></div>
<div class="m2"><p>تو را از جهان دشت و کوه است بهر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به جای سرش زان سری بی‌بها</p></div>
<div class="m2"><p>خورش ساختند از پی اژدها</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از این گونه هر ماهیان سی‌جوان</p></div>
<div class="m2"><p>از ایشان همی یافتندی روان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو گرد آمدی مرد از ایشان دویست</p></div>
<div class="m2"><p>بر آن سان که نشناختندی که کیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خورشگر بدیشان بزی چند و میش</p></div>
<div class="m2"><p>سپردی و صحرا نهادند پیش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کنون کُرد از آن تخمه دارد نژاد</p></div>
<div class="m2"><p>که ز آباد ناید به دل برش یاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس آیین ضحاک وارونه خوی</p></div>
<div class="m2"><p>چنان بد که چون می‌بدش آرزوی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز مردان جنگی یکی خواستی</p></div>
<div class="m2"><p>به کشتی چو با دیو برخاستی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کجا نامور دختری خوبروی</p></div>
<div class="m2"><p>به پرده درون بود بی‌گفت‌گوی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پرستنده کردیش بر پیش خویش</p></div>
<div class="m2"><p>نه بر رسم دین و نه بر رسم کیش</p></div></div>