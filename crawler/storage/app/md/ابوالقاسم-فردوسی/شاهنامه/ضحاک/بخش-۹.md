---
title: >-
    بخش ۹
---
# بخش ۹

<div class="b" id="bn1"><div class="m1"><p>چو آمد به نزدیک اروندرود</p></div>
<div class="m2"><p>فرستاد زی رودبانان درود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بران رودبان گفت پیروز شاه</p></div>
<div class="m2"><p>که کشتی برافگن هم اکنون به راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا با سپاهم بدان سو رسان</p></div>
<div class="m2"><p>از اینها کسی را بدین سو ممان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان تا گذر یابم از روی آب</p></div>
<div class="m2"><p>به کشتی و زورق هم اندر شتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیاورد کشتی نگهبان رود</p></div>
<div class="m2"><p>نیامد بگفت فریدون فرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین داد پاسخ که شاه جهان</p></div>
<div class="m2"><p>چنین گفت با من سخن در نهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که مگذار یک پشه را تا نخست</p></div>
<div class="m2"><p>جوازی بیابی و مهری درست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریدون چو بشنید شد خشمناک</p></div>
<div class="m2"><p>ازان ژرف دریا نیامدش باک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم آنگه میان کیانی ببست</p></div>
<div class="m2"><p>بران بارهٔ تیزتک بر نشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرش تیز شد کینه و جنگ را</p></div>
<div class="m2"><p>به آب اندر افگند گلرنگ را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببستند یارانش یکسر کمر</p></div>
<div class="m2"><p>همیدون به دریا نهادند سر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر آن باد پایان با آفرین</p></div>
<div class="m2"><p>به آب اندرون غرقه کردند زین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خشکی رسیدند سر کینه جوی</p></div>
<div class="m2"><p>به بیت‌المقدس نهادند روی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که بر پهلوانی زبان راندند</p></div>
<div class="m2"><p>همی کنگ دژهودجش خواندند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بتازی کنون خانهٔ پاک دان</p></div>
<div class="m2"><p>برآورده ایوان ضحاک دان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو از دشت نزدیک شهر آمدند</p></div>
<div class="m2"><p>کزان شهر جوینده بهر آمدند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز یک میل کرد آفریدون نگاه</p></div>
<div class="m2"><p>یکی کاخ دید اندر آن شهر شاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فروزنده چون مشتری بر سپهر</p></div>
<div class="m2"><p>همه جای شادی و آرام و مهر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که ایوانش برتر ز کیوان نمود</p></div>
<div class="m2"><p>که گفتی ستاره بخواهد بسود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدانست کان خانهٔ اژدهاست</p></div>
<div class="m2"><p>که جای بزرگی و جای بهاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به یارانش گفت آنکه بر تیره خاک</p></div>
<div class="m2"><p>برآرد چنین بر ز جای از مغاک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بترسم همی زانکه با او جهان</p></div>
<div class="m2"><p>مگر راز دارد یکی در نهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیاید که ما را بدین جای تنگ</p></div>
<div class="m2"><p>شتابیدن آید به روز درنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگفت و به گرز گران دست برد</p></div>
<div class="m2"><p>عنان بارهٔ تیزتک را سپرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو گفتی یکی آتشستی درست</p></div>
<div class="m2"><p>که پیش نگهبان ایوان برست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گران گرز برداشت از پیش زین</p></div>
<div class="m2"><p>تو گفتی همی بر نوردد زمین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کس از روزبانان بدر بر نماند</p></div>
<div class="m2"><p>فریدون جهان آفرین را بخواند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به اسب اندر آمد به کاخ بزرگ</p></div>
<div class="m2"><p>جهان ناسپرده جوان سترگ</p></div></div>