---
title: >-
    بخش ۵
---
# بخش ۵

<div class="b" id="bn1"><div class="m1"><p>بگفت این و زان جایگه برگرفت</p></div>
<div class="m2"><p>ازان مرز تا روم لشکر گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپهبد طلایه به داراب داد</p></div>
<div class="m2"><p>طلایه سنان را به زهر آب داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم‌انگه طلایه بیامد ز روم</p></div>
<div class="m2"><p>وزین سو نگهدار این مرز و بوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زناگه دو لشکر بهم بازخورد</p></div>
<div class="m2"><p>برآمد هم‌آنگاه گرد نبرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه یک به دیگر برآمیختند</p></div>
<div class="m2"><p>چو رود روان خون همی ریختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو داراب دید آن سپاه نبرد</p></div>
<div class="m2"><p>به پیش اندر آمد به کردار گرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازان لشکر روم چندان بکشت</p></div>
<div class="m2"><p>که گفتی فلک تیغ دارد به مشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی رفت زان گونه بر سان شیر</p></div>
<div class="m2"><p>نهنگی به چنگ اژدهایی به زیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین تا به لشکرگه رومیان</p></div>
<div class="m2"><p>همی تاخت بر سان شیر ژیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمین شد ز رومی چو دریای خون</p></div>
<div class="m2"><p>جهانجوی را تیغ شد رهنمون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پیروزی از رومیان گشت باز</p></div>
<div class="m2"><p>به نزدیک سالار گردنفراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسی آفرین یافت از رشنواد</p></div>
<div class="m2"><p>که این لشکر شاه بی‌تو مباد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو ما بازگردیم زین رزم روم</p></div>
<div class="m2"><p>سپاه اندر آید به آباد بوم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو چندان نوازش بیابی ز شاه</p></div>
<div class="m2"><p>ز اسپ و ز مهر و ز تیغ و کلاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه شب همی لشکر آراستند</p></div>
<div class="m2"><p>سلیح سواران بپیراستند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو خورشید برزد سر از تیره راغ</p></div>
<div class="m2"><p>زمین شد به کردار روشن چراغ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بهم بازخوردند هر دو سپاه</p></div>
<div class="m2"><p>شد از گرد خورشید تابان سیاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو داراب پیش آمد و حمله برد</p></div>
<div class="m2"><p>عنان را به اسپ تگاور سپرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به پیش صف رومیان کس نماند</p></div>
<div class="m2"><p>ز گردان شمشیرزن بس نماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به قلب سپاه اندر آمد چو گرگ</p></div>
<div class="m2"><p>پراگنده کرد آن سپاه بزرگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وزان جایگه شد سوی میمنه</p></div>
<div class="m2"><p>بیاورد چندی سلیح و بنه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه لشکر روم برهم درید</p></div>
<div class="m2"><p>کسی از یلان خویشتن را ندید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دلیران ایران به کردار شیر</p></div>
<div class="m2"><p>همی تاختند از پس اندر دلیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بکشتند چندان ز رومی سپاه</p></div>
<div class="m2"><p>که گل شد ز خون خاک آوردگاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چهل جاثلیق از دلیران بکشت</p></div>
<div class="m2"><p>بیامد صلیبی گرفته به مشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو زو رشنواد آن شگفتی بدید</p></div>
<div class="m2"><p>ز شادی دل پهلوان بردمید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برو آفرین کرد و چندی ستود</p></div>
<div class="m2"><p>بران آفرین مهربانی فزود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شب آمد جهان قیرگون شد به رنگ</p></div>
<div class="m2"><p>همی بازگشتند یکسر ز جنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سپهبد به لشکرگه رومیان</p></div>
<div class="m2"><p>برآسود و بگشاد بند میان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ببخشید در شب بسی خواسته</p></div>
<div class="m2"><p>شد از خواسته لشکر آراسته</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فرستاد نزدیک داراب کس</p></div>
<div class="m2"><p>که ای شیردل مرد فریادرس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نگه کن کنون تا پسند تو چیست</p></div>
<div class="m2"><p>وزی خواسته سودمند تو چیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نگه دار چیزی که رای آیدت</p></div>
<div class="m2"><p>ببخش آنچ دل رهنمای آیدت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هرآنچ آن پسندت نیاید ببخش</p></div>
<div class="m2"><p>تو نامی‌تری از خداوند رخش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو آن دید داراب شد شادکام</p></div>
<div class="m2"><p>یکی نیزه برداشت از بهر نام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فرستاد دیگر سوی رشنواد</p></div>
<div class="m2"><p>بدو گفت پیروز بادی و شاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو از باختر تیره شد روی مهر</p></div>
<div class="m2"><p>بپوشید دیبای مشکین سپهر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همان پاس از تیره شب درگذشت</p></div>
<div class="m2"><p>طلایه پراگنده بر گرد دشت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>غو پاسبان خاست چون زلزله</p></div>
<div class="m2"><p>همی شد چو اواز شیر یله</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو زرین سپر برگرفت آفتاب</p></div>
<div class="m2"><p>سر جنگجویان برآمد ز خواب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ببستند گردان ایران میان</p></div>
<div class="m2"><p>همی تاختند از پس رومیان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به شمشیر تیز آتش افروختند</p></div>
<div class="m2"><p>همه شهرها را همی سوختند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز روم و ز رومی برانگیخت گرد</p></div>
<div class="m2"><p>کس از بوم و بر یاد دیگر نکرد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خروشی به زاری برآمد ز روم</p></div>
<div class="m2"><p>که بگذاشتند آن دلارام بوم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به قیصر بر از کین جهان تنگ شد</p></div>
<div class="m2"><p>رخ نامدارانش بی‌رنگ شد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فرستاده آمد بر رشنواد</p></div>
<div class="m2"><p>که گر دادگر سر نپیچد ز داد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شدند آنک جنگی بد از جنگ سیر</p></div>
<div class="m2"><p>سر بخت روم اندرآمد به زیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که گر باژ خواهید فرمان کنیم</p></div>
<div class="m2"><p>بنوی یکی باز پیمان کنیم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فرستاد قیصر ز هر گونه چیز</p></div>
<div class="m2"><p>ابا برده‌ها بدره بسیار نیز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سپهبد پذیرفت زو آنچ بود</p></div>
<div class="m2"><p>ز دینار وز گوهر نابسود</p></div></div>