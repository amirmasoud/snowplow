---
title: >-
    بخش ۴
---
# بخش ۴

<div class="b" id="bn1"><div class="m1"><p>کنون شیون باربد گوش دار</p></div>
<div class="m2"><p>سر مهتران را به آغوش دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آگاه شد بار بد زانک شاه</p></div>
<div class="m2"><p>بپرداخت بیداد و بی‌کام گاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جهرم بیامد سوی طیسفون</p></div>
<div class="m2"><p>پر از آب مژگان و دل پر ز خون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیامد بدان خانه او را بدید</p></div>
<div class="m2"><p>شده لعل رخسار او شنبلید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانی همی‌بود در پیش شاه</p></div>
<div class="m2"><p>خروشان بیامد سوی بارگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی پهلوانی برو مویه کرد</p></div>
<div class="m2"><p>دو رخساره زرد و دلی پر ز درد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان بد که زاریش بشنید شاه</p></div>
<div class="m2"><p>همان کس کجا داشت او را نگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگهبان که بودند گریان شدند</p></div>
<div class="m2"><p>چو بر آتش مهر بریان شدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی‌گفت الا یا ر دا خسروا</p></div>
<div class="m2"><p>بزرگا سترگا تناور گوا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجات آن بزرگی و آن دستگاه</p></div>
<div class="m2"><p>کجات آن همه فر و تخت و کلاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کجات آن همه برز و بالا و تاج</p></div>
<div class="m2"><p>کجات آن همه یاره و تخت عاج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کجات آن همه مردی و زور و فر</p></div>
<div class="m2"><p>جهان را همی‌داشتی زیر پر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کجا آن شبستان و رامشگران</p></div>
<div class="m2"><p>کجا آن بر و بارگاه سران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کجا افسر و کاویانی درفش</p></div>
<div class="m2"><p>کجا آن همه تیغهای بنفش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کجا آن دلیران جنگ آوران</p></div>
<div class="m2"><p>کجا آن رد و موبد و مهتران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کجا آن همه بزم وساز شکار</p></div>
<div class="m2"><p>کجا آن خرامیدن کارزار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کجا آن غلامان زرین کمر</p></div>
<div class="m2"><p>کجا آن همه رای وآیین وفر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کجا آن سرافراز جان و سپار</p></div>
<div class="m2"><p>که با تخت زر بود و با گوشوار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کجا آن همه لشکر و بوم و بر</p></div>
<div class="m2"><p>کجا آن سرافرازی و تخت زر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کجا آن سر خود و زرین زره</p></div>
<div class="m2"><p>ز گوهر فگنده گره بر گره</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کجا اسپ شبدیز و زرین رکیب</p></div>
<div class="m2"><p>که زیر تو اندر بدی ناشکیب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کجا آن سواران زرین ستام</p></div>
<div class="m2"><p>که دشمن بدی تیغشان را نیام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کجا آن همه رازوان بخردی</p></div>
<div class="m2"><p>کجا آن همه فره ایزدی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کجا آن همه بخشش روز بزم</p></div>
<div class="m2"><p>کجا آن همه کوشش روز رزم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کجا آن همه راهوار استران</p></div>
<div class="m2"><p>عماری زرین و فرمانبران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هیونان و بالا وپیل سپید</p></div>
<div class="m2"><p>همه گشته از جان تو ناامید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کجاآن سخنها به شیرین زبان</p></div>
<div class="m2"><p>کجا آن دل و رای و روشن روان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز هر چیز تنها چرا ماندی</p></div>
<div class="m2"><p>ز دفتر چنین روز کی خواندی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مبادا که گستاخ باشی به دهر</p></div>
<div class="m2"><p>که زهرش فزون آمد از پای زهر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پسر خواستی تابود یار و پشت</p></div>
<div class="m2"><p>کنون از پسر رنجت آمد به مشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز فرزند شاهان به نیرو شوند</p></div>
<div class="m2"><p>ز رنج زمانه بی آهو شوند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شهنشاه را چونک نیرو بکاست</p></div>
<div class="m2"><p>چو بالای فرزند او گشت راست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر آنکس که او کار خسرو شنود</p></div>
<div class="m2"><p>به گیتی نبایدش گستاخ بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همه بوم ایران تو ویران شمر</p></div>
<div class="m2"><p>کنام پلنگان و شیران شمر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سر تخم ساسانیان بود شاه</p></div>
<div class="m2"><p>که چون اونبیند دگر تاج و گاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شد این تخمهٔ ویران و ایران همان</p></div>
<div class="m2"><p>برآمد همه کامهٔ بدگمان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فزون زین نباشد کسی را سپاه</p></div>
<div class="m2"><p>ز لشکر که آمدش فریادخواه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گزند آمد از پاسبان بزرگ</p></div>
<div class="m2"><p>کنون اندر آید سوی رخنه گرگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نباشد سپاه تو هم پایدار</p></div>
<div class="m2"><p>چو برخیزد از چار سو کار زار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>روان تو را دادگر یار باد</p></div>
<div class="m2"><p>سر بد سگالان نگونسار باد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به یزدان و نام تو ای شهریار</p></div>
<div class="m2"><p>به نوروز و مهر و بخرم بهار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که گر دست من زین سپس نیز رود</p></div>
<div class="m2"><p>بساید مبادا به من بر درود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بسوزم همه آلت خویش را</p></div>
<div class="m2"><p>بدان تا نبینم بداندیش را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ببرید هر چارانگشت خویش</p></div>
<div class="m2"><p>بریده همی‌داشت در مشت خویش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو در خانه شد آتشی بر فروخت</p></div>
<div class="m2"><p>همه آلت خویش یکسر بسوخت</p></div></div>