---
title: >-
    پادشاهی بهرام شاپور
---
# پادشاهی بهرام شاپور

<div class="b" id="bn1"><div class="m1"><p>خردمند و شایسته بهرامشاه</p></div>
<div class="m2"><p>همی داشت سوک پدر چندگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بنشست بر جایگاه مهی</p></div>
<div class="m2"><p>چنین گفت بر تخت شاهنشهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که هر شاه کز داد گنج آگند</p></div>
<div class="m2"><p>بدانید کان گنج نپراگند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ما ایزد پاک خشنود باد</p></div>
<div class="m2"><p>بداندیش را دل پر از دود باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه دانش اوراست ما بنده‌ایم</p></div>
<div class="m2"><p>که کاهنده و هم فزاینده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهاندار یزدان بود داد و راست</p></div>
<div class="m2"><p>که نفزود در پادشاهی نه کاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی کو به بخشش توانا بود</p></div>
<div class="m2"><p>خردمند و بیدار و دانا بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نباید که بندد در گنج سخت</p></div>
<div class="m2"><p>به ویژه خداوند دیهیم و تخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگر چند بخشی ز گنج سخن</p></div>
<div class="m2"><p>برافشان که دانش نیاید به بن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز نیک و بدیها به یزدان گرای</p></div>
<div class="m2"><p>چو خواهی که نیکیت ماند به جای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر زو شناسی همه خوب و زشت</p></div>
<div class="m2"><p>بیابی به پاداش خرم بهشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وگر برگزینی ز گیتی هوا</p></div>
<div class="m2"><p>بمانی به چنگ هوا بی‌نوا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو داردت یزدان بدو دست یاز</p></div>
<div class="m2"><p>بدان تا نمانی به گرم و گداز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین است امیدم به یزدان پاک</p></div>
<div class="m2"><p>که چون سر بیارم بدین تیره‌خاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهاندار پیروز دارد مرا</p></div>
<div class="m2"><p>همان گیتی افروز دارد مرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر اندر جهان داد بپراگنم</p></div>
<div class="m2"><p>ازان به که بیداد گنج آگنم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که ایدر بماند همه رنج ما</p></div>
<div class="m2"><p>به دشمن رسد بی‌گمان گنج ما</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که تخت بزرگی نماند به کس</p></div>
<div class="m2"><p>جهاندار باشد ترا یار بس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بد و نیک ماند ز ما یادگار</p></div>
<div class="m2"><p>تو تخم بدی تا توانی مکار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو شد سال آن پادشا بر دو هفت</p></div>
<div class="m2"><p>به پالیز آن سرو یازان بخفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به یک چندگه دیر بیمار بود</p></div>
<div class="m2"><p>دل کهتران پر ز تیمار بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نبودش پسر پنج دخترش بود</p></div>
<div class="m2"><p>یکی کهتر از وی برادرش بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو داد ناگاه گنج و سپاه</p></div>
<div class="m2"><p>همان مهر شاهی و تخت و کلاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جهاندار برنا ز گیتی برفت</p></div>
<div class="m2"><p>برو سالیان برگذشته دو هفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ایا شست و سه ساله مرد کهن</p></div>
<div class="m2"><p>تو از باد تا چند رانی سخن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همان روز تو ناگهان بگذرد</p></div>
<div class="m2"><p>در توبه بگزین و راه خرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهاندار زین پیر خشنود باد</p></div>
<div class="m2"><p>خرد مایه باد و سخن سود باد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر در سخن موی کافد همی</p></div>
<div class="m2"><p>به تاریکی اندر ببافد همی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر او این سخن‌ها که اندرگرفت</p></div>
<div class="m2"><p>به پیری سرآرد نباشد شگفت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به نام شهنشاه شمشیرزن</p></div>
<div class="m2"><p>به بالا سرش برتر از انجمن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زمانه به کام شهنشاه باد</p></div>
<div class="m2"><p>سر تخت او افسر ماه باد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کزویست کام و بدویست نام</p></div>
<div class="m2"><p>ورا باد تاج کیی شادکام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بزرگی و دانش ورا راه باد</p></div>
<div class="m2"><p>وزو دست بدخواه کوتاه باد</p></div></div>