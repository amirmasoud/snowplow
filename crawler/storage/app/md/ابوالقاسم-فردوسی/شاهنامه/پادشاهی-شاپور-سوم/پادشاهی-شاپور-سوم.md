---
title: >-
    پادشاهی شاپور سوم
---
# پادشاهی شاپور سوم

<div class="b" id="bn1"><div class="m1"><p>چو شاپور بنشست بر جای عم</p></div>
<div class="m2"><p>از ایران بسی شاد و بهری دژم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین گفت کای نامور بخردان</p></div>
<div class="m2"><p>جهاندیده و رای‌زن موبدان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدانید کان کس که گوید دروغ</p></div>
<div class="m2"><p>نگیرد ازین پس بر ما فروغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دروغ از بر ما نباشد ز رای</p></div>
<div class="m2"><p>که از رای باشد بزرگی به جای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان مر تن سفله را دوستدار</p></div>
<div class="m2"><p>نیابی به باغ اندرون چون نگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سری را کجا مغز باشد بسی</p></div>
<div class="m2"><p>گواژه نباید زدن بر کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبان را نگهدار باید بدن</p></div>
<div class="m2"><p>نباید روان را به زهر آژدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که بر انجمن مرد بسیار گوی</p></div>
<div class="m2"><p>بکاهد به گفتار خود آب‌روی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر دانشی مرد راند سخن</p></div>
<div class="m2"><p>تو بشنو که دانش نگردد کهن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل مرد مطمع بود پر ز درد</p></div>
<div class="m2"><p>به گرد طمع تا توانی مگرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مکن دوستی با دروغ آزمای</p></div>
<div class="m2"><p>همان نیز با مرد ناپاک‌رای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرشت تن از چار گوهر بود</p></div>
<div class="m2"><p>گذر زین چهارانش کمتر بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر سفله‌گر مرد با شرم و راد</p></div>
<div class="m2"><p>به آزادگی یک دل و یک نهاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیم کو میانه گزیند ز کار</p></div>
<div class="m2"><p>بسند آیدش بخشش کردگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چهارم که بپراگند بر گزاف</p></div>
<div class="m2"><p>همی دانشی نام جوید ز لاف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دو گیتی بیابد دل مرد راد</p></div>
<div class="m2"><p>نباشد دل سفله یک روز شاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدین گیتی او را بود نام زشت</p></div>
<div class="m2"><p>بدان گیتی‌اندر نیابد بهشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دو گیتی نیابد دل مرد لاف</p></div>
<div class="m2"><p>که بپراگند خواسته بر گزاف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ستوده کسی کو میانه گزید</p></div>
<div class="m2"><p>تن خویش را آفرین گسترید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شما را جهان‌آفرین یار باد</p></div>
<div class="m2"><p>همیشه سر بخت بیدار باد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهاندارمان باد فریادرس</p></div>
<div class="m2"><p>که تخت بزرگی نماند به کس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگفت این و از پیش برخاستند</p></div>
<div class="m2"><p>ز یزدان برو آفرین خواستند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو شد سالیان پنج بر چار ماه</p></div>
<div class="m2"><p>بشد شاه روزی به نخچیرگاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جهان شد پر از یوز و باران و سگ</p></div>
<div class="m2"><p>چه پرنده و چند تازان به تگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ستاره زدند از پی خوابگاه</p></div>
<div class="m2"><p>چو چیزی بخورد و بیاسود شاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سه جام می خسروانی بخورد</p></div>
<div class="m2"><p>پراندیشه شد سر سوی خواب کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پراگنده گشتند لشکر همه</p></div>
<div class="m2"><p>چو در خواب شد شهریار رمه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بخفت او و از دشت برخاست باد</p></div>
<div class="m2"><p>که کس باد ازان سان ندارد به یاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فروبرده چوب ستاره بکند</p></div>
<div class="m2"><p>بزد بر سر شهریار بلند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جهانجوی شاپور جنگی بمرد</p></div>
<div class="m2"><p>کلاه کیی دیگری را سپرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>میاز و مناز و متاز و مرنج</p></div>
<div class="m2"><p>چه تازی به کین و چه نازی به گنج</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که بهر تو اینست زین تیره‌گوی</p></div>
<div class="m2"><p>هنر جوی و راز جهان را مجوی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که گر بازیابی به پیچی بدرد</p></div>
<div class="m2"><p>پژوهش مکن گرد رازش مگرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنین است کردار این چرخ تیر</p></div>
<div class="m2"><p>چه با مرد برنا چه با مردپیر</p></div></div>