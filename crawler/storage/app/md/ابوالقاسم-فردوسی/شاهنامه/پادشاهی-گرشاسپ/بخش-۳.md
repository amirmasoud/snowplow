---
title: >-
    بخش ۳
---
# بخش ۳

<div class="b" id="bn1"><div class="m1"><p>بزد مهره در جام بر پشت پیل</p></div>
<div class="m2"><p>ازو برشد آواز تا چند میل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خروشیدن کوس با کرنای</p></div>
<div class="m2"><p>همان ژنده پیلان و هندی درای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآمد ز زاولستان رستخیز</p></div>
<div class="m2"><p>زمین خفته را بانگ برزد که خیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیش اندرون رستم پهلوان</p></div>
<div class="m2"><p>پس پشت او سالخورده گوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان شد ز لشکر در و دشت و راغ</p></div>
<div class="m2"><p>که بر سر نیارست پرید زاغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تبیره زدندی همی شست جای</p></div>
<div class="m2"><p>جهان را نه سر بود پیدا نه پای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هنگام بشکوفهٔ گلستان</p></div>
<div class="m2"><p>بیاورد لشکر ز زابلستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز زال آگهی یافت افراسیاب</p></div>
<div class="m2"><p>برآمد ز آرام و از خورد و خواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیاورد لشکر سوی خوار ری</p></div>
<div class="m2"><p>بران مرغزاری که بد آب و نی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز ایران بیامد دمادم سپاه</p></div>
<div class="m2"><p>ز راه بیابان سوی رزمگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز لشکر به لشکر دو فرسنگ ماند</p></div>
<div class="m2"><p>سپهبد جهاندیدگان را بخواند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدیشان چنین گفت کای بخردان</p></div>
<div class="m2"><p>جهاندیده و کارکرده ردان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم ایدر من این لشکر آراستم</p></div>
<div class="m2"><p>بسی سروری و مهی خواستم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پراگنده شد رای بی تخت شاه</p></div>
<div class="m2"><p>همه کار بی‌روی و بی‌سر سپاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو بر تخت بنشست فرخنده زو</p></div>
<div class="m2"><p>ز گیتی یکی آفرین خاست نو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شهی باید اکنون ز تخم کیان</p></div>
<div class="m2"><p>به تخت کیی بر کمر بر میان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شهی کاو باورنگ دارد ز می</p></div>
<div class="m2"><p>که بی‌سر نباشد تن آدمی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نشان داد موبد مرا در زمان</p></div>
<div class="m2"><p>یکی شاه با فر و بخت جوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز تخم فریدون یل کیقباد</p></div>
<div class="m2"><p>که با فر و برزست و با رای و داد</p></div></div>