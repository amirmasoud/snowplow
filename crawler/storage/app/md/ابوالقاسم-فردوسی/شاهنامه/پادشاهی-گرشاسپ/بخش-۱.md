---
title: >-
    بخش ۱
---
# بخش ۱

<div class="b" id="bn1"><div class="m1"><p>پسر بود زو را یکی خویش کام</p></div>
<div class="m2"><p>پدر کرده بودیش گرشاسپ نام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیامد نشست از بر تخت و گاه</p></div>
<div class="m2"><p>به سر بر نهاد آن کیانی کلاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بنشست بر تخت و گاه پدر</p></div>
<div class="m2"><p>جهان را همی داشت با زیب و فر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین تا برآمد برین روزگار</p></div>
<div class="m2"><p>درخت بلا کینه آورد بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ترکان خبر شد که زو درگذشت</p></div>
<div class="m2"><p>بران سان که بد تخت بی‌کار گشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیامد به خوار ری افراسیاب</p></div>
<div class="m2"><p>ببخشید گیتی و بگذاشت آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیاورد یک تن درود پشنگ</p></div>
<div class="m2"><p>سرش پر ز کین بود و دل پر ز جنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلش خود ز تخت و کله گشته بود</p></div>
<div class="m2"><p>به تیمار اغریرث آغشته بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدو روی ننمود هرگز پشنگ</p></div>
<div class="m2"><p>شد آن تیغ روشن پر از تیره زنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرستاده رفتی به نزدیک اوی</p></div>
<div class="m2"><p>بدو سال و مه هیچ ننمود روی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی گفت اگر تخت را سر بدی</p></div>
<div class="m2"><p>چو اغریرثش یار درخور بدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو خون برادر بریزی همی</p></div>
<div class="m2"><p>ز پرورده مرغی گریزی همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا با تو تا جاودان کار نیست</p></div>
<div class="m2"><p>به نزد منت راه دیدار نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پرآواز شد گوش ازین آگهی</p></div>
<div class="m2"><p>که بی‌کار شد تخت شاهنشهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیامی بیامد به کردار سنگ</p></div>
<div class="m2"><p>به افراسیاب از دلاور پشنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که بگذار جیحون و برکش سپاه</p></div>
<div class="m2"><p>ممان تا کسی برنشیند به گاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی لشکری ساخت افراسیاب</p></div>
<div class="m2"><p>ز دشت سپنجاب تا رود آب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که گفتی زمین شد سپهر روان</p></div>
<div class="m2"><p>همی بارد از تیغ هندی روان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکایک به ایران رسید آگهی</p></div>
<div class="m2"><p>که آمد خریدار تخت مهی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سوی زابلستان نهادند روی</p></div>
<div class="m2"><p>جهان شد سراسر پر از گفت‌وگوی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگفتند با زال چندی درشت</p></div>
<div class="m2"><p>که گیتی بس آسان گرفتی به مشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس از سام تا تو شدی پهلوان</p></div>
<div class="m2"><p>نبودیم یک روز روشن روان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپاهی ز جیحون بدین سو کشید</p></div>
<div class="m2"><p>که شد آفتاب از جهان ناپدید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر چاره دانی مراین را بساز</p></div>
<div class="m2"><p>که آمد سپهبد به تنگی فراز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنین گفت پس نامور زال زر</p></div>
<div class="m2"><p>که تا من ببستم به مردی کمر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سواری چو من پای بر زین نگاشت</p></div>
<div class="m2"><p>کسی تیغ و گرز مرا برنداشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به جایی که من پای بفشاردم</p></div>
<div class="m2"><p>عنان سواران شدی پاردم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شب و روز در جنگ یکسان بدم</p></div>
<div class="m2"><p>ز پیری همه ساله ترسان بدم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کنون چنبری گشت یال یلی</p></div>
<div class="m2"><p>نتابد همی خنجر کابلی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کنون گشت رستم چو سرو سهی</p></div>
<div class="m2"><p>بزیبد برو بر کلاه مهی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یکی اسپ جنگیش باید همی</p></div>
<div class="m2"><p>کزین تازی اسپان نشاید همی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بجویم یکی بارهٔ پیلتن</p></div>
<div class="m2"><p>بخواهم ز هر سو که هست انجمن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بخوانم به رستم بر این داستان</p></div>
<div class="m2"><p>که هستی برین کار همداستان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که بر کینهٔ تخمهٔ زادشم</p></div>
<div class="m2"><p>ببندی میان و نباشی دژم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه شهر ایران ز گفتار اوی</p></div>
<div class="m2"><p>ببودند شادان دل و تازه روی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز هر سو هیونی تکاور بتاخت</p></div>
<div class="m2"><p>سلیح سواران جنگی بساخت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به رستم چنین گفت کای پیلتن</p></div>
<div class="m2"><p>به بالا سرت برتر از انجمن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی کار پیشست و رنجی دراز</p></div>
<div class="m2"><p>کزو بگسلد خواب و آرام و ناز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ترا نوز پورا گه رزم نیست</p></div>
<div class="m2"><p>چه سازم که هنگامهٔ بزم نیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هنوز از لبت شیر بوید همی</p></div>
<div class="m2"><p>دلت ناز و شادی بجوید همی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چگونه فرستم به دشت نبرد</p></div>
<div class="m2"><p>ترا پیش ترکان پر کین و درد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چه گویی چه سازی چه پاسخ دهی</p></div>
<div class="m2"><p>که جفت تو بادا مهی و بهی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنین گفت رستم به دستان سام</p></div>
<div class="m2"><p>که من نیستم مرد آرام و جام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چنین یال و این چنگهای دراز</p></div>
<div class="m2"><p>نه والا بود پروریدن به ناز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگر دشت کین آید و رزم سخت</p></div>
<div class="m2"><p>بود یار یزدان پیروزبخت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ببینی که در جنگ من چون شوم</p></div>
<div class="m2"><p>چو اندر پی ریزش خون شوم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یکی ابر دارم به چنگ اندرون</p></div>
<div class="m2"><p>که همرنگ آبست و بارانش خون</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همی آتش افروزد از گوهرش</p></div>
<div class="m2"><p>همی مغز پیلان بساید سرش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یکی باره باید چو کوه بلند</p></div>
<div class="m2"><p>چنان چون من آرم به خم کمند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یکی گرز خواهم چو یک لخت کوه</p></div>
<div class="m2"><p>گرآیند پیشم ز توران گروه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سرانشان بکوبم بدان گرز بر</p></div>
<div class="m2"><p>نیاید برم هیچ پرخاشخر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>که روی زمین را کنم بی‌سپاه</p></div>
<div class="m2"><p>که خون بارد ابر اندر آوردگاه</p></div></div>