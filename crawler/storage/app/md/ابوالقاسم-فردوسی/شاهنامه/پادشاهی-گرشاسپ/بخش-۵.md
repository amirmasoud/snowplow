---
title: >-
    بخش ۵
---
# بخش ۵

<div class="b" id="bn1"><div class="m1"><p>ز ترکان طلایه بسی بد براه</p></div>
<div class="m2"><p>رسید اندر ایشان یل صف پناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآویخت با نامداران جنگ</p></div>
<div class="m2"><p>یکی گرزهٔ گاو پیکر به چنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلیران توران برآویختند</p></div>
<div class="m2"><p>سرانجام از رزم بگریختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهادند سر سوی افراسیاب</p></div>
<div class="m2"><p>همه دل پر از خون و دیده پر آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتند وی را همه بیش و کم</p></div>
<div class="m2"><p>سپهبد شد از کار ایشان دژم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بفرمود تا نزد او شد قلون</p></div>
<div class="m2"><p>ز ترکان دلیری گوی پرفسون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفت بگزین ز لشکر سوار</p></div>
<div class="m2"><p>وز ایدر برو تا در کوهسار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلیر و خردمند و هشیار باش</p></div>
<div class="m2"><p>به پاس اندرون نیز بیدار باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که ایرانیان مردمی ریمنند</p></div>
<div class="m2"><p>همی ناگهان بر طلایه زنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برون آمد از نزد خسرو قلون</p></div>
<div class="m2"><p>به پیش اندرون مردم رهنمون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر راه بر نامداران ببست</p></div>
<div class="m2"><p>به مردان جنگی و پیلان مست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وزان روی رستم دلیر و گزین</p></div>
<div class="m2"><p>بپیمود زی شاه ایران زمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی میل ره تا به البرز کوه</p></div>
<div class="m2"><p>یکی جایگه دید برنا شکوه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درختان بسیار و آب روان</p></div>
<div class="m2"><p>نشستنگه مردم نوجوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی تخت بنهاده نزدیک آب</p></div>
<div class="m2"><p>برو ریخته مشک ناب و گلاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جوانی به کردار تابنده ماه</p></div>
<div class="m2"><p>نشسته بران تخت بر سایه‌گاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رده برکشیده بسی پهلوان</p></div>
<div class="m2"><p>به رسم بزرگان کمر بر میان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیاراسته مجلسی شاهوار</p></div>
<div class="m2"><p>بسان بهشتی به رنگ و نگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو دیدند مر پهلوان را به راه</p></div>
<div class="m2"><p>پذیره شدندش ازان سایه‌گاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که ما میزبانیم و مهمان ما</p></div>
<div class="m2"><p>فرود آی ایدر به فرمان ما</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدان تا همه دست شادی بریم</p></div>
<div class="m2"><p>به یاد رخ نامور می خوریم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تهمتن بدیشان چنین گفت باز</p></div>
<div class="m2"><p>که ای نامداران گردن فراز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرا رفت باید به البرز کوه</p></div>
<div class="m2"><p>به کاری که بسیار دارد شکوه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نباید به بالین سر و دست ناز</p></div>
<div class="m2"><p>که پیشست بسیار رنج دراز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سر تخت ایران ابی شهریار</p></div>
<div class="m2"><p>مرا باده خوردن نیاید به کار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نشانی دهیدم سوی کیقباد</p></div>
<div class="m2"><p>کسی کز شما دارد او را به یاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سر آن دلیران زبان برگشاد</p></div>
<div class="m2"><p>که دارم نشانی من از کیقباد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر آیی فرود و خوری نان ما</p></div>
<div class="m2"><p>بیفروزی از روی خود جان ما</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگوییم یکسر نشان قباد</p></div>
<div class="m2"><p>که او را چگونست رستم و نهاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تهمتن ز رخش اندر آمد چو باد</p></div>
<div class="m2"><p>چو بشنید از وی نشان قباد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بیامد دمان تا لب رودبار</p></div>
<div class="m2"><p>نشستند در زیر آن سایه‌دار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جوان از بر تخت خود برنشست</p></div>
<div class="m2"><p>گرفته یکی دست رستم به دست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به دست دگر جام پر باده کرد</p></div>
<div class="m2"><p>وزو یاد مردان آزاده کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دگر جام بر دست رستم سپرد</p></div>
<div class="m2"><p>بدو گفت کای نامبردار و گرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بپرسیدی از من نشان قباد</p></div>
<div class="m2"><p>تو این نام را از که داری به یاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدو گفت رستم که از پهلوان</p></div>
<div class="m2"><p>پیام آوریدم به روشن روان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سر تخت ایران بیاراستند</p></div>
<div class="m2"><p>بزرگان به شاهی ورا خواستند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پدرم آن گزین یلان سر به سر</p></div>
<div class="m2"><p>که خوانند او را همی زال زر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرا گفت رو تا به البرز کوه</p></div>
<div class="m2"><p>قباد دلاور ببین با گروه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به شاهی برو آفرین کن یکی</p></div>
<div class="m2"><p>نباید که سازی درنگ اندکی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بگویش که گردان ترا خواستند</p></div>
<div class="m2"><p>به شادی جهانی بیاراستند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نشان ار توانی و دانی مرا</p></div>
<div class="m2"><p>دهی و به شاهی رسانی ورا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز گفتار رستم دلیر جوان</p></div>
<div class="m2"><p>بخندید و گفتش که ای پهلوان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز تخم فریدون منم کیقباد</p></div>
<div class="m2"><p>پدر بر پدر نام دارم به یاد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو بشنید رستم فرو برد سر</p></div>
<div class="m2"><p>به خدمت فرود آمد از تخت زر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که ای خسرو خسروان جهان</p></div>
<div class="m2"><p>پناه بزرگان و پشت مهان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سر تخت ایران به کام تو باد</p></div>
<div class="m2"><p>تن ژنده پیلان به دام تو باد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نشست تو بر تخت شاهنشهی</p></div>
<div class="m2"><p>همت سرکشی باد و هم فرهی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>درودی رسانم به شاه جهان</p></div>
<div class="m2"><p>ز زال گزین آن یل پهلوان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر شاه فرمان دهد بنده را</p></div>
<div class="m2"><p>که بگشایم از بند گوینده را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>قباد دلاور برآمد ز جای</p></div>
<div class="m2"><p>ز گفتار رستم دل و هوش و رای</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تهمتن همانگه زبان برگشاد</p></div>
<div class="m2"><p>پیام سپهدار ایران بداد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سخن چون به گوش سپهبد رسید</p></div>
<div class="m2"><p>ز شادی دل اندر برش برطپید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بیازید جامی لبالب نبید</p></div>
<div class="m2"><p>بیاد تهمتن به دم درکشید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تهمتن همیدون یکی جام می</p></div>
<div class="m2"><p>بخورد آفرین کرد بر جان کی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>برآمد خروش از دل زیر و بم</p></div>
<div class="m2"><p>فراوان شده شادی اندوه کم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شهنشه چنین گفت با پهلوان</p></div>
<div class="m2"><p>که خوابی بدیدم به روشن روان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>که از سوی ایران دو باز سپید</p></div>
<div class="m2"><p>یکی تاج رخشان به کردار شید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>خرامان و نازان شدندی برم</p></div>
<div class="m2"><p>نهادندی آن تاج را بر سرم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو بیدار گشتم شدم پرامید</p></div>
<div class="m2"><p>ازان تاج رخشان و باز سپید</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بیاراستم مجلسی شاهوار</p></div>
<div class="m2"><p>برین سان که بینی بدین مرغزار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تهمتن مرا شد چو باز سپید</p></div>
<div class="m2"><p>ز تاج بزرگان رسیدم نوید</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تهمتن چو بشنید از خواب شاه</p></div>
<div class="m2"><p>ز باز و ز تاج فروزان چو ماه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چنین گفت با شاه کنداوران</p></div>
<div class="m2"><p>نشانست خوابت ز پیغمبران</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کنون خیز تا سوی ایران شویم</p></div>
<div class="m2"><p>به یاری به نزد دلیران شویم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>قباد اندر آمد چو آتش ز جای</p></div>
<div class="m2"><p>ببور نبرد اندر آورد پای</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کمر برمیان بست رستم چو باد</p></div>
<div class="m2"><p>بیامد گرازان پس کیقباد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شب و روز از تاختن نغنوید</p></div>
<div class="m2"><p>چنین تا به نزد طلایه رسید</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>قلون دلاور شد آگه ز کار</p></div>
<div class="m2"><p>چو آتش بیامد سوی کارزار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>شهنشاه ایران چو زان گونه دید</p></div>
<div class="m2"><p>برابر همی خواست صف برکشید</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تهمتن بدو گفت کای شهریار</p></div>
<div class="m2"><p>ترا رزم جستن نیاید بکار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>من و رخش و کوپال و برگستوان</p></div>
<div class="m2"><p>همانا ندارند با من توان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بگفت این و از جای برکرد رخش</p></div>
<div class="m2"><p>به زخمی سواری همی کرد پخش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>قلون دید دیوی بجسته ز بند</p></div>
<div class="m2"><p>به دست اندرون گرز و برزین کمند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>برو حمله آورد مانند باد</p></div>
<div class="m2"><p>بزد نیزه و بند جوشن گشاد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تهمتن بزد دست و نیزه گرفت</p></div>
<div class="m2"><p>قلون از دلیریش مانده شگفت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ستد نیزه از دست او نامدار</p></div>
<div class="m2"><p>بغرید چون تندر از کوهسار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بزد نیزه و برگرفتش ز زین</p></div>
<div class="m2"><p>نهاد آن بن نیزه را بر زمین</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>قلون گشت چون مرغ با بابزن</p></div>
<div class="m2"><p>بدیدند لشکر همه تن به تن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>هزیمت شد از وی سپاه قلون</p></div>
<div class="m2"><p>به یکبارگی بخت بد را زبون</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تهمتن گذشت از طلایه سوار</p></div>
<div class="m2"><p>بیامد شتابان سوی کوهسار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>کجا بد علفزار و آب روان</p></div>
<div class="m2"><p>فرود آمد آن جایگه پهلوان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چنین تا شب تیره آمد فراز</p></div>
<div class="m2"><p>تهمتن همی کرد هرگونه ساز</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>از آرایش جامهٔ پهلوی</p></div>
<div class="m2"><p>همان تاج و هم بارهٔ خسروی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چو شب تیره شد پهلو پیش‌بین</p></div>
<div class="m2"><p>برآراست باشاه ایران زمین</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>به نزدیک زال آوریدش به شب</p></div>
<div class="m2"><p>به آمد شدن هیچ نگشاد لب</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نشستند یک هفته با رای زن</p></div>
<div class="m2"><p>شدند اندران موبدان انجمن</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بهشتم بیاراست پس تخت عاج</p></div>
<div class="m2"><p>برآویختند از بر عاج تاج</p></div></div>