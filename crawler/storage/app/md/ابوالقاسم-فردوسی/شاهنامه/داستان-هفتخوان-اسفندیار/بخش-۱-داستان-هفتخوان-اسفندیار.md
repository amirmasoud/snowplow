---
title: >-
    بخش ۱ - داستان هفتخوان اسفندیار
---
# بخش ۱ - داستان هفتخوان اسفندیار

<div class="b" id="bn1"><div class="m1"><p>کنون زین سپس هفتخوان آورم</p></div>
<div class="m2"><p>سخنهای نغز و جوان آورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر بخت یکباره یاری کند</p></div>
<div class="m2"><p>برو طبع من کامگاری کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگویم به تأیید محمود شاه</p></div>
<div class="m2"><p>بدان فر و آن خسروانی کلاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که شاه جهان جاودان زنده باد</p></div>
<div class="m2"><p>بزرگان گیتی ورا بنده باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو خورشید بر چرخ بنمود چهر</p></div>
<div class="m2"><p>بیاراست روی زمین را به مهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به برج حمل تاج بر سر نهاد</p></div>
<div class="m2"><p>ازو خاور و باختر گشت شاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پر از غلغل و رعد شد کوهسار</p></div>
<div class="m2"><p>پر از نرگس و لاله شد جویبار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز لاله فریب و ز نرگس نهیب</p></div>
<div class="m2"><p>ز سنبل عتاب و ز گلنار زیب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پر آتش دل ابر و پر آب چشم</p></div>
<div class="m2"><p>خروش مغانی و پرتاب خشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آتش نماید بپالاید آب</p></div>
<div class="m2"><p>ز آواز او سر برآید ز خواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو بیدار گردی جهان را ببین</p></div>
<div class="m2"><p>که دیباست گر نقش مانی به چین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو رخشنده گردد جهان ز آفتاب</p></div>
<div class="m2"><p>رخ نرگس و لاله بینی پر آب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخندد بدو گوید ای شوخ چشم</p></div>
<div class="m2"><p>به عشق تو گریان نه از درد و خشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نخندد زمین تا نگرید هوا</p></div>
<div class="m2"><p>هوا را نخوانم کف پادشا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که باران او در بهاران بود</p></div>
<div class="m2"><p>نه چون همت شهریاران بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خورشید ماند همی دست شاه</p></div>
<div class="m2"><p>چو اندر حمل برفرازد کلاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر گنج پیش آید از خاک خشک</p></div>
<div class="m2"><p>وگر آب دریا و گر در و مشک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ندارد همی روشناییش باز</p></div>
<div class="m2"><p>ز درویش وز شاه گردن فراز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کف شاه ابوالقاسم آن پادشا</p></div>
<div class="m2"><p>چنین است با پاک و ناپارسا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دریغش نیاید ز بخشیدن ایچ</p></div>
<div class="m2"><p>نه آرام گیرد به روز بسیچ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو جنگ آیدش پیش جنگ آورد</p></div>
<div class="m2"><p>سر شهریاران به چنگ آورد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدان کس که گردن نهد گنج خویش</p></div>
<div class="m2"><p>ببخشد نیندیشد از رنج خویش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهان را جهاندار محمود باد</p></div>
<div class="m2"><p>ازو بخشش و داد موجود باد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز رویین دژ اکنون جهاندیده پیر</p></div>
<div class="m2"><p>نگر تا چه گوید ازو یاد گیر</p></div></div>