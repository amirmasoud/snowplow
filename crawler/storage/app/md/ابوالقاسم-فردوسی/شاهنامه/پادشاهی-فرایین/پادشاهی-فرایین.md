---
title: >-
    پادشاهی فرایین
---
# پادشاهی فرایین

<div class="b" id="bn1"><div class="m1"><p>فرایین چو تاج کیان برنهاد</p></div>
<div class="m2"><p>همی‌گفت چیزی که آمدش یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی‌گفت شاهی کنم یک زمان</p></div>
<div class="m2"><p>نشینم برین تخت بر شادمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به از بندگی توختن شست سال</p></div>
<div class="m2"><p>برآورده رنج و فرو برده یال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس از من پسر بر نشیند بگاه</p></div>
<div class="m2"><p>نهد بر سر آن خسروانی کلاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهانی بدو گفت مهتر پسر</p></div>
<div class="m2"><p>که اکنون به گیتی توی تاجور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مباش ایمن و گنج را چاره کن</p></div>
<div class="m2"><p>جهان بان شدی کار یکباره کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو از تخمهٔ شهریاران کسی</p></div>
<div class="m2"><p>بیاید نمانی تو ایدر بسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وزان پس چنین گفت کهتر پسر</p></div>
<div class="m2"><p>که اکنون به گیتی توی تاجور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سزاوار شاهی سپاهست و گنج</p></div>
<div class="m2"><p>چو با گنج باشی نمانی به رنج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فریدون که بد آبتینش پدر</p></div>
<div class="m2"><p>مر او را که بد پیش او تاجور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان را بسه پور فرخنده داد</p></div>
<div class="m2"><p>که اندر جهان او بد از داد شاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به مرد و به گنج این جهان را بدار</p></div>
<div class="m2"><p>نزاید ز مادر کسی شهریار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ورا خوش نیامد بدین سان سخن</p></div>
<div class="m2"><p>به مهتر پسر گفت خامی مکن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عرض را به دیوان شاهی نشاند</p></div>
<div class="m2"><p>سپه را سراسر به درگاه خواند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شب تیره تا روز دینار داد</p></div>
<div class="m2"><p>بسی خلعت ناسزاوار داد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به دو هفته از گنج شاه اردشیر</p></div>
<div class="m2"><p>نماند از بهایی یکی پر تیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر آنگه که رفتی به می سوی باغ</p></div>
<div class="m2"><p>نبردی جز از شمع عنبر چراغ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان تشت زرین و سیمین بدی</p></div>
<div class="m2"><p>چو زرین بدی گوهر آگین بدی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو هشتاد در پیش و هشتاد پس</p></div>
<div class="m2"><p>پس شمع یاران فریادرس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه شب بدی خوردن آیین اوی</p></div>
<div class="m2"><p>دل مهتران پرشد ازکین اوی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شب تیره همواره گردان بدی</p></div>
<div class="m2"><p>به پالیزها گر به میدان بدی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نماندش به ایران یکی دوستدار</p></div>
<div class="m2"><p>شکست اندر آمد به آموزگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرایین همان ناجوانمرد گشت</p></div>
<div class="m2"><p>ابی داد و بی‌بخشش و خورد گشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی زر بر چشم بر دوختی</p></div>
<div class="m2"><p>جهان را به دینار بفروختی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی‌ریخت خون سر بی‌گناه</p></div>
<div class="m2"><p>از آن پس برآشفت به روی سپاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به دشنام لبها بیاراستند</p></div>
<div class="m2"><p>جهانی همه مرگ او خواستند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شب تیره هر مزد شهران گراز</p></div>
<div class="m2"><p>سخنها همی‌گفت چندان به راز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گزیده سواری ز شهر صطخر</p></div>
<div class="m2"><p>که آن مهتران را بدو بود فخر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به ایرانیان گفت کای مهتران</p></div>
<div class="m2"><p>شد این روزگار فرایین گران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همی‌دارد او مهتران را سبک</p></div>
<div class="m2"><p>چرا شد چنین مغز و دلتان تنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همه دیده‌ها زو شده پر سرشک</p></div>
<div class="m2"><p>جگر پر ز خون شد بباید پزشک</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنین داد پاسخ مرا او را سپاه</p></div>
<div class="m2"><p>که چون کس نماند از در پیشگاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه کس را همی‌آید از رشک یاد</p></div>
<div class="m2"><p>که پردازدی دل به دین بد نژاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدیشان چنین گفت شهران گراز</p></div>
<div class="m2"><p>که این کار ایرانیان شد دراز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر ایدون که بر من نسازید بد</p></div>
<div class="m2"><p>کنید آنک از داد و گردی سزد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هم اکنون به نیروی یزدان پاک</p></div>
<div class="m2"><p>مر او را ز باره در آرم به خاک</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنین یافت پاسخ ز ایرانیان</p></div>
<div class="m2"><p>که بر تو مبادا که آید زیان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه لشکر امروز یار توایم</p></div>
<div class="m2"><p>گرت زین بد آید حصار توایم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو بشنید ز ایشان ز ترکش نخست</p></div>
<div class="m2"><p>یکی تیر پولاد پیکان بجست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برانگیخت از جای اسپ سیاه</p></div>
<div class="m2"><p>همی‌داشت لشکر مر او را نگاه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کمان رابه بازو همی‌درکشید</p></div>
<div class="m2"><p>گهی در بروگاه بر سرکشید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به شورش‌گری تیر بازه ببست</p></div>
<div class="m2"><p>چو شد غرفه پیکانش بگشاد شست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بزد تیر ناگاه بر پشت اوی</p></div>
<div class="m2"><p>بیفتاد تازانه از مشت اوی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همه تیرتا پر در خون گذشت</p></div>
<div class="m2"><p>سرآهن ازناف بیرون گذشت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز باره در افتاد سرسرنگون</p></div>
<div class="m2"><p>روان گشت زان زخم او جوی خون</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بپیچید و برزد یکی باد سرد</p></div>
<div class="m2"><p>به زاری بران خاک دل پر ز درد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سپه تیغها بر کشیدند پاک</p></div>
<div class="m2"><p>برآمد شب تیره از دشت خاک</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همه شب همی خنجر انداختند</p></div>
<div class="m2"><p>یکی از دگر باز نشناختند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همی این از آن بستد و آن ازین</p></div>
<div class="m2"><p>یکی یافت نفرین دگر آفرین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پراگنده گشت آن سپاه بزرگ</p></div>
<div class="m2"><p>چومیشان بد دل که بینند گرگ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فراوان بماندند بی شهریار</p></div>
<div class="m2"><p>نیامد کسی تاج را خواستار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بجستند فرزند شاهان بسی</p></div>
<div class="m2"><p>ندیدند زان نامداران کسی</p></div></div>