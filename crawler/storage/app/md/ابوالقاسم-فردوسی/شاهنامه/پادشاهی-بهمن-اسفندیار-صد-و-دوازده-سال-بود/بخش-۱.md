---
title: >-
    بخش ۱
---
# بخش ۱

<div class="b" id="bn1"><div class="m1"><p>چو بهمن به تخت نیا بر نشست</p></div>
<div class="m2"><p>کمر با میان بست و بگشاد دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپه را درم داد و دینار داد</p></div>
<div class="m2"><p>همان کشور و مرز بسیار داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی انجمن ساخت از بخردان</p></div>
<div class="m2"><p>بزرگان و کار آزموه ردان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین گفت کز کار اسفندیار</p></div>
<div class="m2"><p>ز نیک و بد گردش روزگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه یاد دارید پیر و جوان</p></div>
<div class="m2"><p>هرانکس که هستید روشن‌روان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که رستم گه زندگانی چه کرد</p></div>
<div class="m2"><p>همان زال افسونگر آن پیرمرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرامرز جز کین ما در جهان</p></div>
<div class="m2"><p>نجوید همی آشکار و نهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرم پر ز دردست و دل پر ز خون</p></div>
<div class="m2"><p>جز از کین ندارم به مغز اندرون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو جنگی چو نوش‌آذر و مهرنوش</p></div>
<div class="m2"><p>که از درد ایشان برآمد خروش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو اسفندیاری که اندر جهان</p></div>
<div class="m2"><p>بدو تازه بد روزگار مهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زابلستان زان نشان کشته شد</p></div>
<div class="m2"><p>ز دردش دد و دام سرگشته شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همانا که بر خون اسفندیار</p></div>
<div class="m2"><p>به زاری بگرید به ایوان نگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم از خون آن نامداران ما</p></div>
<div class="m2"><p>جوانان و جنگی سواران ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر آنکس که او باشد از آب پاک</p></div>
<div class="m2"><p>نیارد سر گوهر اندر مغاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به کردار شاه آفریدون بود</p></div>
<div class="m2"><p>چو خونین بباشد همایون بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که ضحاک را از پی خون جم</p></div>
<div class="m2"><p>ز نام‌آوران جهان کرد کم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>منوچهر با سلم و تور سترگ</p></div>
<div class="m2"><p>بیاورد ز آمل سپاهی بزرگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به چین رفت و کین نیا بازخواست</p></div>
<div class="m2"><p>مرا همچنان داستانست راست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو کیخسرو آمد از افراسیاب</p></div>
<div class="m2"><p>ز خون کرد گیتی چو دریای آب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پدرم آمد و کین لهراسپ خواست</p></div>
<div class="m2"><p>ز کشته زمین کرد با کوه راست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرامرز کز بهر خون پدر</p></div>
<div class="m2"><p>به خورشید تابان برآورد سر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به کابل شد و کین رستم بخواست</p></div>
<div class="m2"><p>همه بوم و بر کرد با خاک راست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زمین را ز خون بازنشناختند</p></div>
<div class="m2"><p>همی باره بر کشتگان تاختند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به کینه سزاوارتر کس منم</p></div>
<div class="m2"><p>که بر شیر درنده اسپ افگنم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر بشمری در جهان نامدار</p></div>
<div class="m2"><p>سواری نبینی چو اسفندیار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه بیند و این را چه پاسخ دهید</p></div>
<div class="m2"><p>بکوشید تا رای فرخ نهید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو بشنید گفتار بهمن سپاه</p></div>
<div class="m2"><p>هرانکس که بد شاه را نیکخواه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به آواز گفتند ما بنده‌ایم</p></div>
<div class="m2"><p>همه دل به مهر تو آگنده‌ایم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز کار گذشته تو داناتری</p></div>
<div class="m2"><p>ز مردان جنگی تواناتری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به گیتی همان کن که کام آیدت</p></div>
<div class="m2"><p>وگر زان سخن فر و نام آیدت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نپیچد کسی سر ز فرمان تو</p></div>
<div class="m2"><p>که یارد گذشتن ز پیمان تو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو پاسخ چنین یافت از لشکرش</p></div>
<div class="m2"><p>به کین اندرون تیزتر شد سرش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه سیستان را بیاراستند</p></div>
<div class="m2"><p>برین بر نهادند و برخاستند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به شبگیر برخاست آوای کوس</p></div>
<div class="m2"><p>شد از گرد لشکر سپهر آبنوس</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همی رفت زان لشکر نامدار</p></div>
<div class="m2"><p>سواران شمشیرزن صد هزار</p></div></div>