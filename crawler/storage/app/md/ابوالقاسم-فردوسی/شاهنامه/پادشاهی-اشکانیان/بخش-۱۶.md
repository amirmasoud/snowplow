---
title: >-
    بخش ۱۶
---
# بخش ۱۶

<div class="b" id="bn1"><div class="m1"><p>ز شهر کجاران برآمد نفیر</p></div>
<div class="m2"><p>برفتند با نیزه و تیغ و تیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیم رفت پیش اندرون هفتواد</p></div>
<div class="m2"><p>به جنگ اندرون داد مردی بداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شهر بگرفت و او را بکشت</p></div>
<div class="m2"><p>بسی گوهر و گنجش آمد به مشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نزدیک او مردم انبوه شد</p></div>
<div class="m2"><p>ز شهر کجاران سوی کوه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی دژ بکرد از بر تیغ کوه</p></div>
<div class="m2"><p>شد آن شهر با او همه همگروه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهاد اندران دژ دری آهنین</p></div>
<div class="m2"><p>هم آرامگه بود هم جای کین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی چشمه‌ای بود بر کوهسار</p></div>
<div class="m2"><p>ز تخت اندرآمد میان حصار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی باره‌ای کرد گرداندرش</p></div>
<div class="m2"><p>که بینا به دیده ندیدی سرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو آن کرم را گشت صندوق تنگ</p></div>
<div class="m2"><p>یکی حوض کردند بر کوه سنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو ساروج و سنگ از هوا گشت گرم</p></div>
<div class="m2"><p>نهادند کرم اندرو نرم نرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان بد که دارنده هر بامداد</p></div>
<div class="m2"><p>برفتی دوان از بر هفتواد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گزیدی به رنجش علف ساختی</p></div>
<div class="m2"><p>تن آگنده کرم آن پرداختی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر آمد برین کار بر پنج سال</p></div>
<div class="m2"><p>چو پیلی شد آن کرم با شاخ و یال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو یک چند بگذشت بر هفتواد</p></div>
<div class="m2"><p>بر آواز آن کرم کرمان نهاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همان دخت خرم نگهدار کرم</p></div>
<div class="m2"><p>پدر گشته جنگی سپهدار کرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیاراستندش وزیر و دبیر</p></div>
<div class="m2"><p>به رنجش بدی خوردن و شهد و شیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپهبد بدی بر دژ هفتواد</p></div>
<div class="m2"><p>همان پرسش کار بیداد و داد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سپاهی و دستور و سالار بار</p></div>
<div class="m2"><p>هران چیز کاید شهان را به کار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه هرچ بایستش آراستند</p></div>
<div class="m2"><p>چنانچون شهان را بپیراستند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به کشور پراگنده شد لشکرش</p></div>
<div class="m2"><p>همه گشت آراسته کشورش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز دریای چین تا به کرمان رسید</p></div>
<div class="m2"><p>همه روی کشور سپه گسترید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پسر هفت با تیغ‌زن ده هزار</p></div>
<div class="m2"><p>همان گنج با آلت کارزار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هران پادشا کو کشیدی به جنگ</p></div>
<div class="m2"><p>چو رفتی سپاهش بر کرم تنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شکسته شدی لشکری کامدی</p></div>
<div class="m2"><p>چو آواز این داستان بشندی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنان شد دژ نامور هفتواد</p></div>
<div class="m2"><p>که گردش نیارست جنبید باد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی گشت هر روز برترش بخت</p></div>
<div class="m2"><p>یکی خویشتن را بیاراست سخت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همی خواندندی ورا شهریار</p></div>
<div class="m2"><p>سر مرد بخرد ازو در خمار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپهبد که بودی به مرز اندرون</p></div>
<div class="m2"><p>به یک چنگ در جنگ کردش زبون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نتابید با او کسی بر به جنگ</p></div>
<div class="m2"><p>برآمد برین نیز چندی درنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حصاری شدش پر ز گنج و سپاه</p></div>
<div class="m2"><p>ندیدی بران باره‌بر باد راه</p></div></div>