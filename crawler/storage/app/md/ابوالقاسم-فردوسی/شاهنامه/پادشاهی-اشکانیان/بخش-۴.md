---
title: >-
    بخش ۴
---
# بخش ۴

<div class="b" id="bn1"><div class="m1"><p>چو نه ماه بگذشت بر ماه‌چهر</p></div>
<div class="m2"><p>یکی کودک آمد چو تابنده مهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مانندهٔ نامدار اردشیر</p></div>
<div class="m2"><p>فزاینده و فرخ و دلپذیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان اردشیرش پدر کرد نام</p></div>
<div class="m2"><p>نیا شد به دیدار او شادکام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی پروریدش به بربر به ناز</p></div>
<div class="m2"><p>برآمد برین روزگاری دراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مر او را کنون مردم تیزویر</p></div>
<div class="m2"><p>همی خواندش بابکان اردشیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیاموختندش هنر هرچ بود</p></div>
<div class="m2"><p>هنر نیز بر گوهرش بر فزود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان شد به دیدار و فرهنگ و چهر</p></div>
<div class="m2"><p>که گفتی همی زو فروزد سپهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس آگاهی آمد سوی اردوان</p></div>
<div class="m2"><p>ز فرهنگ وز دانش آن جوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که شیر ژیانست هنگام رزم</p></div>
<div class="m2"><p>به ناهید ماند همی روز بزم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی نامه بنوشت پس اردوان</p></div>
<div class="m2"><p>سوی بابک نامور پهلوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که ای مرد بادانش و رهنمای</p></div>
<div class="m2"><p>سخن‌گوی و با نام و پاکیزه‌رای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شنیدم که فرزند تو اردشیر</p></div>
<div class="m2"><p>سواریست گوینده و یادگیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو نامه بخوانی هم‌اندر زمان</p></div>
<div class="m2"><p>فرستش به نزدیک ما شادمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز بایسته‌ها بی‌نیازش کنم</p></div>
<div class="m2"><p>میان یلان سرفرازش کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو باشد به نزدیک فرزند ما</p></div>
<div class="m2"><p>نگوییم کو نیست پیوند ما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو آن نامهٔ شاه بابک بخواند</p></div>
<div class="m2"><p>بسی خون مژگان به رخ برفشاند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بفرمود تا پیش او شد دبیر</p></div>
<div class="m2"><p>همان نورسیده جوان اردشیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدو گفت کاین نامهٔ اردوان</p></div>
<div class="m2"><p>بخوان و نگه‌کن به روشن روان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من اینک یکی نامه نزدیک شاه</p></div>
<div class="m2"><p>نویسم فرستم یکی نیک‌خواه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگویم که اینک دل و دیده را</p></div>
<div class="m2"><p>دلاور جوان پسندیده را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرستادم و دادمش نیز پند</p></div>
<div class="m2"><p>چو آید بدان بارگاه بلند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو آن کن که از رسم شاهان سزد</p></div>
<div class="m2"><p>نباید که بادی برو بر وزد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در گنج بگشاد بابک چو باد</p></div>
<div class="m2"><p>جوان را ز هرگونه‌ای کرد شاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز زرین ستام و ز گوپال و تیغ</p></div>
<div class="m2"><p>ز فرزند چیزش نیامد دریغ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز دینار و دیبا و اسپ و رهی</p></div>
<div class="m2"><p>ز چینی و زربفت شاهنشهی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیاورد و بنهاد پیش جوان</p></div>
<div class="m2"><p>جوان شد پرستندهٔ اردوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بسی هدیه‌ها نیز با اردشیر</p></div>
<div class="m2"><p>ز دیبا و دینار و مشک و عبیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز پیش نیا کودک نیک پی</p></div>
<div class="m2"><p>به درگاه شاه اردوان شد بری</p></div></div>