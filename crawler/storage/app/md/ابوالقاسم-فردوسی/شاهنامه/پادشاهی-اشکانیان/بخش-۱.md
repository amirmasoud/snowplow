---
title: >-
    بخش ۱
---
# بخش ۱

<div class="b" id="bn1"><div class="m1"><p>کنون پادشاه جهان را ستای</p></div>
<div class="m2"><p>به رزم و به بزم و به دانش گرای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرافراز محمود فرخنده‌رای</p></div>
<div class="m2"><p>کزویست نام بزرگی به جای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهاندار ابوالقاسم پر خرد</p></div>
<div class="m2"><p>که رایش همی از خرد برخورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی باد تا جاودان شاد دل</p></div>
<div class="m2"><p>ز رنج و ز غم گشته آزاد دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهنشاه ایران و زابلستان</p></div>
<div class="m2"><p>ز قنوج تا مرز کابلستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برو آفرین باد و بر لشکرش</p></div>
<div class="m2"><p>چه بر خویش و بر دوده و کشورش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهاندار سالار او میر نصر</p></div>
<div class="m2"><p>کزو شادمانست گردنده عصر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دریغش نیاید ز بخشیدن ایچ</p></div>
<div class="m2"><p>نه آرام گیرد به روز بیسچ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو جنگ آیدش پیش جنگ آورد</p></div>
<div class="m2"><p>سر شهریاران به چنگ آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برآنکس که بخشش کند گنج خویش</p></div>
<div class="m2"><p>ببخشد نه‌اندیشد از رنج خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان تاجهاندار محمود باد</p></div>
<div class="m2"><p>وزو بخشش و داد موجود باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپهدار چون بوالمظفر بود</p></div>
<div class="m2"><p>سرلشکر از ماه برتر بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که پیروز نامست و پیروزبخت</p></div>
<div class="m2"><p>همی بگذرد تیر او بر درخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همیشه تن شاه بی‌رنج باد</p></div>
<div class="m2"><p>نشستش همه بر سر گنج باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همیدون سپهدار او شاد باد</p></div>
<div class="m2"><p>دلش روشن و گنجش آباد باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنین تا به پایست گردان سپهر</p></div>
<div class="m2"><p>ازین تخمه هرگز مبراد مهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پدر بر پدر بر پسر بر پسر</p></div>
<div class="m2"><p>همه تاجدارند و پیروزگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گذشته ز شوال ده با چهار</p></div>
<div class="m2"><p>یکی آفرین باد بر شهریار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کزین مژده دادیم رسم خراج</p></div>
<div class="m2"><p>که فرمان بد از شاه با فر و تاج</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که سالی خراجی نخواهند بیش</p></div>
<div class="m2"><p>ز دین‌دار بیدار وز مرد کیش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدین عهد نوشین‌روان تازه شد</p></div>
<div class="m2"><p>همه کار بر دیگر اندازه شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو آمد بران روزگاری دراز</p></div>
<div class="m2"><p>همی بفگند چادر داد باز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ببینی بدین داد و نیکی گمان</p></div>
<div class="m2"><p>که او خلعتی یابد از آسمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که هرگز نگردد کهن بر برش</p></div>
<div class="m2"><p>بماند کلاه کیان بر سرش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سرش سبز باد و تنش بی‌گزند</p></div>
<div class="m2"><p>منش برگذشته ز چرخ بلند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ندارد کسی خوار فال مرا</p></div>
<div class="m2"><p>کجا بشمرد ماه و سال مرا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نگه کن که این نامه تا جاودان</p></div>
<div class="m2"><p>درفشی بود بر سر بخردان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بماند بسی روزگاران چنین</p></div>
<div class="m2"><p>که خوانند هرکس برو آفرین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنین گفت نوشین روان قباد</p></div>
<div class="m2"><p>که چون شاه را دل بپیچد ز داد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کند چرخ منشور او را سپاه</p></div>
<div class="m2"><p>ستاره نخواند ورا نیز شاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ستم نامهٔ عزل شاهان بود</p></div>
<div class="m2"><p>چو درد دل بیگناهان بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بماناد تا جاودان این گهر</p></div>
<div class="m2"><p>هنرمند و بادانش و دادگر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نباشد جهان بر کسی پایدار</p></div>
<div class="m2"><p>همه نام نیکو بود یادگار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کجا شد فریدون و ضحاک و جم</p></div>
<div class="m2"><p>مهان عرب خسروان عجم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کجا آن بزرگان ساسانیان</p></div>
<div class="m2"><p>ز بهرامیان تا به سامانیان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نکوهیده‌تر شاه ضحاک بود</p></div>
<div class="m2"><p>که بیدادگر بود و ناپاک بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فریدون فرخ ستایش ببرد</p></div>
<div class="m2"><p>بمرد او و جاوید نامش نمرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سخن ماند اندر جهان یادگار</p></div>
<div class="m2"><p>سخن بهتر از گوهر شاهوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ستایش نبرد آنک بیداد بود</p></div>
<div class="m2"><p>به گنج و به تخت مهی شاد بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گسسته شود در جهان کام اوی</p></div>
<div class="m2"><p>نخواند به گیتی کسی نام اوی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ازین نامهٔ شاه دشمن‌گداز</p></div>
<div class="m2"><p>که بادا همه ساله بر تخت ناز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همه مردم از خانها شد به دشت</p></div>
<div class="m2"><p>نیایش همی ز آسمان برگذشت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که جاوید بادا سر تاجدار</p></div>
<div class="m2"><p>خجسته برو گردش روزگار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز گیتی مبیناد جز کام خویش</p></div>
<div class="m2"><p>نوشته بر ایوانها نام خویش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همان دوده و لشکر و کشورش</p></div>
<div class="m2"><p>همان خسروی قامت و منظرش</p></div></div>