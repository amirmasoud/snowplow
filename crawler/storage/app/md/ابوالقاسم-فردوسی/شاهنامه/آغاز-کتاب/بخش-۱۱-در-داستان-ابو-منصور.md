---
title: >-
    بخش ۱۱ - در داستان ابو منصور
---
# بخش ۱۱ - در داستان ابو منصور

<div class="b" id="bn1"><div class="m1"><p>بدین نامه چون دست کردم دراز</p></div>
<div class="m2"><p>یکی مهتری بود گردن‌فراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوان بود و از گوهر پهلوان</p></div>
<div class="m2"><p>خردمند و بیدار و روشن روان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خداوند رای و خداوند شرم</p></div>
<div class="m2"><p>سخن گفتن خوب و آوای نرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا گفت کز من چه باید همی</p></div>
<div class="m2"><p>که جانت سخن برگراید همی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چیزی که باشد مرا دسترس</p></div>
<div class="m2"><p>بکوشم نیازت نیارم به کس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی داشتم چون یکی تازه سیب</p></div>
<div class="m2"><p>که از باد نامد به من بر نهیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کیوان رسیدم ز خاک نژند</p></div>
<div class="m2"><p>از آن نیک‌دل نامدار ارجمند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به چشمش همان خاک و هم سیم و زر</p></div>
<div class="m2"><p>کریمی بدو یافته زیب و فر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سراسر جهان پیش او خوار بود</p></div>
<div class="m2"><p>جوانمرد بود و وفادار بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان نامور گم شد از انجمن</p></div>
<div class="m2"><p>چو در باغ سرو سهی از چمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه ز او زنده بینم نه مرده نشان</p></div>
<div class="m2"><p>به دست نهنگان مردم کشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دریغ آن کمربند و آن گِردگاه</p></div>
<div class="m2"><p>دریغ آن کِیی برز و بالای شاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرفتار ز او دل شده نا‌امید</p></div>
<div class="m2"><p>نوان لرز لرزان به کردار بید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی پند آن شاه یاد آوریم</p></div>
<div class="m2"><p>ز کژی روان سوی داد آوریم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا گفت کاین نامهٔ شهریار</p></div>
<div class="m2"><p>گرت گفته آید به شاهان سپار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدین نامه من دست بردم فراز</p></div>
<div class="m2"><p>به نام شهنشاه گردن‌فراز</p></div></div>