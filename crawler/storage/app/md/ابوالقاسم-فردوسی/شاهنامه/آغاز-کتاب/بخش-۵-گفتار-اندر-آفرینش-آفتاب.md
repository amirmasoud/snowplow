---
title: >-
    بخش ۵ - گفتار اندر آفرینش آفتاب
---
# بخش ۵ - گفتار اندر آفرینش آفتاب

<div class="b" id="bn1"><div class="m1"><p>ز یاقوت سرخ است چرخ کبود</p></div>
<div class="m2"><p>نه از آب و گرد و نه از باد و دود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چندین فروغ و به چندین چراغ</p></div>
<div class="m2"><p>بیاراسته چون به نوروز باغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روان اندر او گوهر دل‌فروز</p></div>
<div class="m2"><p>کز او روشنایی گرفته است روز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خاور بر آید سوی باختر</p></div>
<div class="m2"><p>نباشد از این یک روش راست‌تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایا آن که تو آفتابی همی</p></div>
<div class="m2"><p>چه بودت که بر من نتابی همی</p></div></div>