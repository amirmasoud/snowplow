---
title: >-
    بخش ۲ - ستایش خرد
---
# بخش ۲ - ستایش خرد

<div class="b" id="bn1"><div class="m1"><p>کنون ای خردمند وصف خرد</p></div>
<div class="m2"><p>بدین جایگه گفتن اندر خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنون تا چه داری بیار از خرد</p></div>
<div class="m2"><p>که گوش نیوشنده ز او بر خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرد بهتر از هر چه ایزد بداد</p></div>
<div class="m2"><p>ستایش خرد را به از راه داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرد رهنمای و خرد دلگشای</p></div>
<div class="m2"><p>خرد دست گیرد به هر دو سرای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از او شادمانی و ز اویت غمی است</p></div>
<div class="m2"><p>و ز اویت فزونی و ز اویت کمی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرد تیره و مرد روشن روان</p></div>
<div class="m2"><p>نباشد همی شادمان یک زمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه گفت آن خردمند مرد خرد</p></div>
<div class="m2"><p>که دانا ز گفتار او بر خورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی کو خرد را ندارد ز پیش</p></div>
<div class="m2"><p>دلش گردد از کردهٔ خویش ریش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هشیوار دیوانه خواند ورا</p></div>
<div class="m2"><p>همان خویش بیگانه داند ورا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از اویی به هر دو سرای ارجمند</p></div>
<div class="m2"><p>گسسته خرد پای دارد به بند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خرد چشم جان است چون بنگری</p></div>
<div class="m2"><p>تو بی‌چشم شادان جهان نسپری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخست آفرینش خرد را شناس</p></div>
<div class="m2"><p>نگهبان جان است و آن سه پاس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سه پاس تو چشم است و گوش و زبان</p></div>
<div class="m2"><p>کز این سه رسد نیک و بد بی‌گمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرد را و جان را که یارد ستود</p></div>
<div class="m2"><p>و گر من ستایم که یارد شنود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حکیما چو کس نیست گفتن چه سود</p></div>
<div class="m2"><p>از این پس بگو کآفرینش چه بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تویی کردهٔ کردگار جهان</p></div>
<div class="m2"><p>ببینی همی آشکار و نهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به گفتار دانندگان راه جوی</p></div>
<div class="m2"><p>به گیتی بپوی و به هر کس بگوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز هر دانشی چون سخن بشنوی</p></div>
<div class="m2"><p>از آموختن یک زمان نغنوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو دیدار یابی به شاخ سخن</p></div>
<div class="m2"><p>بدانی که دانش نیاید به بن</p></div></div>