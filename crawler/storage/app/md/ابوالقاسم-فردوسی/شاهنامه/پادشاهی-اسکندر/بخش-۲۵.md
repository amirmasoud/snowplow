---
title: >-
    بخش ۲۵
---
# بخش ۲۵

<div class="b" id="bn1"><div class="m1"><p>سکندر بیامد دلی همچو کوه</p></div>
<div class="m2"><p>رها گشته از شاه دانش پژوه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبودش ز قیدافه چین در به روی</p></div>
<div class="m2"><p>نبرداشت هرگز دل از آرزوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببود آن شب و بامداد پگاه</p></div>
<div class="m2"><p>ز ایوان بیامد به نزدیک شاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپهدار در خان پیل‌استه بود</p></div>
<div class="m2"><p>همه گرد بر گرد او رسته بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر خانه را پیکر از جزع و زر</p></div>
<div class="m2"><p>به زر اندرون چند گونه گهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیش اندرون دستهٔ مشک بوی</p></div>
<div class="m2"><p>دو فرزند بایسته در پیش اوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو طینوش اسپ‌افگن و قیدروش</p></div>
<div class="m2"><p>نهاده به گفتار قیدافه گوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مادر چنین گفت کهتر پسر</p></div>
<div class="m2"><p>که ای شاه نیک اختر و دادگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان کن که از پیش تو بیطقون</p></div>
<div class="m2"><p>شود شاد و خشنود با رهنمون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بره بر کسی تا نیازاردش</p></div>
<div class="m2"><p>ور از دشمنان نیز نشماردش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که زنده کن پاک جان من اوست</p></div>
<div class="m2"><p>برآنم که روشن روان من اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو گفت مادر که ایدون کنم</p></div>
<div class="m2"><p>که او را بزرگی بر افزون کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به اسکندر نامور شاه گفت</p></div>
<div class="m2"><p>که پیدا کن اکنون نهان از نهفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه خواهی و رای سکندر به چیست</p></div>
<div class="m2"><p>چه رانی تو از شاه و دستور کیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سکندر بدو گفت کای سرفراز</p></div>
<div class="m2"><p>به نزد تو شد بودن من دراز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا گفت رو باژ مرزش بخواه</p></div>
<div class="m2"><p>وگر دیر مانی بیارم سپاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نمانم بدو کشور و تاج و تخت</p></div>
<div class="m2"><p>نه زور و نه شاهی نه گنج و نه بخت</p></div></div>