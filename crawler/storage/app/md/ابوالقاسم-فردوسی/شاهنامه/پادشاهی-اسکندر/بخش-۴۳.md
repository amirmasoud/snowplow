---
title: >-
    بخش ۴۳
---
# بخش ۴۳

<div class="b" id="bn1"><div class="m1"><p>به بابل هم‌ان روز شد دردمند</p></div>
<div class="m2"><p>بدانست کامد به تنگی گزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دبیر جهاندیده را پیش خواند</p></div>
<div class="m2"><p>هرانچش به دل بود با او براند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مادر یکی نامه فرمود و گفت</p></div>
<div class="m2"><p>که آگاهی مرگ نتوان نهفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز گیتی مرا بهره این بد که بود</p></div>
<div class="m2"><p>زمان چون نکاهد نشاید فزود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو از مرگ من هیچ غمگین مشو</p></div>
<div class="m2"><p>که اندر جهان این سخن نیست نو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرانکس که زاید ببایدش مرد</p></div>
<div class="m2"><p>اگر شهریارست گر مرد خرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگویم کنون با بزرگان روم</p></div>
<div class="m2"><p>که چون بازگردند زین مرز و بوم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نجویند جز رای و فرمان تو</p></div>
<div class="m2"><p>کسی برنگردد ز پیمان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرانکس که بودند ز ایرانیان</p></div>
<div class="m2"><p>کزیشان بدی رومیان را زیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپردم به هر مهتری کشوری</p></div>
<div class="m2"><p>که گردد بر آن پادشاهی سری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همانا نیازش نیاید به روم</p></div>
<div class="m2"><p>برآساید آن کشور و مرز و بوم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا مرده در خاک مصر آگنید</p></div>
<div class="m2"><p>ز گفتار من هیچ مپراگنید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به سالی ز دینار من صدهزار</p></div>
<div class="m2"><p>ببخشید بر مردم خیش‌کار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر آید یکی روشنک را پسر</p></div>
<div class="m2"><p>بود بی‌گمان زنده نام پدر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نباید که باشد جزو شاه روم</p></div>
<div class="m2"><p>که او تازه گرداند آن مرز و بوم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وگر دختر آید به هنگام بوس</p></div>
<div class="m2"><p>به پیوند با تخمهٔ فیلقوس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو فرزند خوانش نه داماد من</p></div>
<div class="m2"><p>بدو تازه کن در جهان یاد من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دگر دختر کید را بی‌گزند</p></div>
<div class="m2"><p>فرستید نزد پدر ارجمند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ابا یاره و برده و نیک‌خواه</p></div>
<div class="m2"><p>عمار بسیچید بااو به راه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همان افسر و گوهر و سیم و زر</p></div>
<div class="m2"><p>که آورده بود او ز پیش پدر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به رفتن چنو گشت همداستان</p></div>
<div class="m2"><p>فرستید با او به هندوستان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من ایدر همه کار کردم به برگ</p></div>
<div class="m2"><p>به بیچارگی دل نهادم به مرگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نخست آنک تابوت زرین کنند</p></div>
<div class="m2"><p>کفن بر تنم عنبر آگین کنند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز زربفت چینی سزاوار من</p></div>
<div class="m2"><p>کسی کو بپیچد ز تیمار من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در و بند تابوت ما را به قیر</p></div>
<div class="m2"><p>بگیرند و کافور و مشک و عبیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نخست آگنند اندرو انگبین</p></div>
<div class="m2"><p>زبر انگبین زیر دیبای چین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ازان پس تن من نهند اندران</p></div>
<div class="m2"><p>سرآمد سخن چون برآمد روان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو پند من ای مادر پرخرد</p></div>
<div class="m2"><p>نگه‌دار تا روز من بگذرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز چیزی که آوردم از هند و چین</p></div>
<div class="m2"><p>ز توران و ایران و مکران زمین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدار و ببخش آنچ افزون بود</p></div>
<div class="m2"><p>وز اندازهٔ خویش بیرون بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به تو حاجت آنستم ای مهربان</p></div>
<div class="m2"><p>که بیدار باشی و روشن‌روان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نداری تن خویش را رنجه بس</p></div>
<div class="m2"><p>که اندر جهان نیست جاوید کس</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>روانم روان ترا بی‌گمان</p></div>
<div class="m2"><p>ببیند چو تنگ اندر آید زمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شکیبایی از مهر نامی‌تر است</p></div>
<div class="m2"><p>سبکسر بود هرک او کهتر است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ترا مهر بد بر تنم سال و ماه</p></div>
<div class="m2"><p>کنون جان پاکم ز یزدان بخواه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدین خواستن باش فریادرس</p></div>
<div class="m2"><p>که فریادرس باشدم دست‌رس</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نگر تا که بینی به گرد جهان</p></div>
<div class="m2"><p>که او نیست از مرگ خسته‌روان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو نامه به مهر اندر آورد و بند</p></div>
<div class="m2"><p>بفرمود تا بر ستور نوند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز بابل به روم آورند آگهی</p></div>
<div class="m2"><p>که تیره شد آن فر شاهنشهی</p></div></div>