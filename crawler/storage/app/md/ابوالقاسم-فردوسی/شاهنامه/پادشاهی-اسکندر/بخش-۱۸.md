---
title: >-
    بخش ۱۸
---
# بخش ۱۸

<div class="b" id="bn1"><div class="m1"><p>چو اسکندر آمد به نزدیک فور</p></div>
<div class="m2"><p>بدید آن سپه این سپه را ز دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خروش آمد و گرد رزم او دو روی</p></div>
<div class="m2"><p>برفتند گردان پرخاشجوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به اسپ و به نفط آتش اندر زدند</p></div>
<div class="m2"><p>همه لشکر فور برهم زدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آتش برافروخت نفط سیاه</p></div>
<div class="m2"><p>بجنبید ازان کاهنین بد سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو پیلان بدیدند ز آتش گریز</p></div>
<div class="m2"><p>برفتند با لشکر از جای تیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز لشکر برآمد سراسر خروش</p></div>
<div class="m2"><p>به زخم آوریدند پیلان به جوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خرطومهاشان بر آتش گرفت</p></div>
<div class="m2"><p>بماندند زان پیلبانان شگفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه لشکر هند گشتند باز</p></div>
<div class="m2"><p>همان ژنده پیلان گردن فراز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سکندر پس لشکر بدگمان</p></div>
<div class="m2"><p>همی تاخت بر سان باددمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین تا هوا نیلگون شد به رنگ</p></div>
<div class="m2"><p>سپه را نماند آن زمان جای جنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهانجوی با رومیان همگروه</p></div>
<div class="m2"><p>فرود آمد اندر میان دو کوه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طلایه فرستاد هر سو به راه</p></div>
<div class="m2"><p>همی داشت لشکر ز دشمن نگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو پیدا شد آن شوشهٔ تاج شید</p></div>
<div class="m2"><p>جهان شد بسان بلور سپید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برآمد خروش از بر گاودم</p></div>
<div class="m2"><p>دم نای سرغین و رویینه خم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپه با سپه جنگ برساختند</p></div>
<div class="m2"><p>سنانها به ابر اندر افراختند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سکندر بیامد میان دو صف</p></div>
<div class="m2"><p>یکی تیغ رومی گرفته به کف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سواری فرستاد نزدیک فور</p></div>
<div class="m2"><p>که او را بخواند بگوید ز دور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که آمد سکندر به پیش سپاه</p></div>
<div class="m2"><p>به دیدار جوید همی با تو راه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سخن گوید و گفت تو بشنود</p></div>
<div class="m2"><p>اگر دادگویی بدان بگرود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو بشنید زو فور هندی برفت</p></div>
<div class="m2"><p>به پیش سپاه آمد از قلب تفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سکندر بدو گفت کای نامدار</p></div>
<div class="m2"><p>دو لشکر شکسته شد از کارزار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همی دام و دد مغز مردم خورد</p></div>
<div class="m2"><p>همی نعل اسپ استخوان بسپرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو مردیم هر دو دلیر و جوان</p></div>
<div class="m2"><p>سخن گوی و با مغز دو پهلوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دلیران لشکر همه کشته‌اند</p></div>
<div class="m2"><p>وگر زنده از رزم برگشته‌اند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چرا بهر لشکر همه کشتن است</p></div>
<div class="m2"><p>وگر زنده از رزم برگشتن است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>میان را ببندیم و جنگ آوریم</p></div>
<div class="m2"><p>چو باید که کشور به چنگ آوریم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز ما هرک او گشت پیروز بخت</p></div>
<div class="m2"><p>بدو ماند این لشکر و تاج و تخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز رومی سخنها چو بشنید فور</p></div>
<div class="m2"><p>خریدار شد رزم او را به سور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تن خویش را دید با زور شیر</p></div>
<div class="m2"><p>یکی باره چون اژدهای دلیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سکندر سواری بسان قلم</p></div>
<div class="m2"><p>سلیحی سبک بادپایی دژم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدوگفت کاینست آیین و راه</p></div>
<div class="m2"><p>بگردیم یک با دگر بی‌سپاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دو خنجر گرفتند هر دو به کف</p></div>
<div class="m2"><p>بگشتند چندان میان دو صف</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سکندر چو دید آن تن پیل مست</p></div>
<div class="m2"><p>یکی کوه زیر اژدهایی به دست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به آورد ازو ماند اندر شگفت</p></div>
<div class="m2"><p>غمی شد دل از جان خود برگرفت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همی گشت با او به آوردگاه</p></div>
<div class="m2"><p>خروشی برآمد ز پشت سپاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دل فور پر درد شد زان خروش</p></div>
<div class="m2"><p>بران سو کشیدش دل و چشم و گوش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سکندر چو باد اندر آمد ز گرد</p></div>
<div class="m2"><p>بزد تیغ تیزی بران شیر مرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ببرید پی بر بر و گردنش</p></div>
<div class="m2"><p>ز بالا به خاک اندر آمد تنش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سر لشکر روم شد به آسمان</p></div>
<div class="m2"><p>برفتند گردان لشکر دمان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یکی کوس بودش ز چرم هژبر</p></div>
<div class="m2"><p>که آواز او برگذشتی ز ابر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>برآمد دم بوق و آواس کوس</p></div>
<div class="m2"><p>زمین آهنین شد هوا آبنوس</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بران هم نشان هندوان رزمجوی</p></div>
<div class="m2"><p>به تنگی به روی اندر آورده روی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خروش آمد از روم کای دوستان</p></div>
<div class="m2"><p>سر مایهٔ مرز هندوستان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سر فور هندی به خاک اندرست</p></div>
<div class="m2"><p>تن پیلوارش به چاک اندرست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شما را کنون از پی کیست جنگ</p></div>
<div class="m2"><p>چنین زخم شمشیر و چندین درنگ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سکندر شما را چنان شد که فور</p></div>
<div class="m2"><p>ازو جست باید همی رزم و سور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>برفتند گردان هندوستان</p></div>
<div class="m2"><p>به آواز گشتند همداستان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تن فور دیدند پر خون و خاک</p></div>
<div class="m2"><p>بر و تنش کرده به شمشیر چاک</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خروشی برآمد ز لشکر به زار</p></div>
<div class="m2"><p>فرو ریختند آلت کارزار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پر از درد نزدیک قیصر شدند</p></div>
<div class="m2"><p>پر از ناله و خاک بر سر شدند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سکندر سلیح گوان بازداد</p></div>
<div class="m2"><p>به خوبی ز هرگونه آواز داد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چنین گفت کز هند مردی به مرد</p></div>
<div class="m2"><p>شما را به غم دل نباید سپرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نوزاش کنون من به افزون کنم</p></div>
<div class="m2"><p>بکوشم که غم نیز بیرون کنم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ببخشم شما را همه گنج اوی</p></div>
<div class="m2"><p>حرامست بر لشکرم رنج اوی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همه هندوان را توانگر کنم</p></div>
<div class="m2"><p>بکوشم که با تخت و افسر کنم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وزان جایگه شد بر تخت فور</p></div>
<div class="m2"><p>بران جشن ماتم برین جشن سور</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چنین است رسم سرای سپنج</p></div>
<div class="m2"><p>بخواهد که مانی بدو در به رنج</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بخور هرچ داری منه بازپس</p></div>
<div class="m2"><p>تو رنجی چرا ماند باید به کس</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همی بود بر تخت قیصر دو ماه</p></div>
<div class="m2"><p>ببخشید گنجش همه بر سپاه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>یکی با گهر بود نامش سورگ</p></div>
<div class="m2"><p>ز هندوستان پهلوانی سترگ</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سر تخت شاهی بدو داد و گفت</p></div>
<div class="m2"><p>که دینار هرگز مکن در نهفت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ببخش و بخور هرچ آید فراز</p></div>
<div class="m2"><p>بدین تاج و تخت سپنجی مناز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که گاهی سکندر بود گاه فور</p></div>
<div class="m2"><p>گهی درد و خشمست و گه کام و سور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>درم داد و دینار لشکرش را</p></div>
<div class="m2"><p>بیاراست گردان کشورش را</p></div></div>