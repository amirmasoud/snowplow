---
title: >-
    بخش ۳۰
---
# بخش ۳۰

<div class="b" id="bn1"><div class="m1"><p>وزان جایگه رفت خورشیدفش</p></div>
<div class="m2"><p>بیامد دمان تا زمین حبش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مردم زمین بود چون پر زاغ</p></div>
<div class="m2"><p>سیه گشته و چشمها چون چراغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تناور یکی لشکری زورمند</p></div>
<div class="m2"><p>برهنه تن و پوست و بالابلند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو از دور دیدند گرد سپاه</p></div>
<div class="m2"><p>خروشی برآمد ز ابر سیاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپاه انجمن شد هزاران هزار</p></div>
<div class="m2"><p>وران تیره شد دیدهٔ شهریار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سوی سکندر نهادند سر</p></div>
<div class="m2"><p>بکشتند بسیار پرخاشخر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جای سنان استخوان داشتند</p></div>
<div class="m2"><p>همی بر تن مرد بگذاشتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به لشکر بفرمود پس شهریار</p></div>
<div class="m2"><p>که برداشتند آلت کارزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برهنه به جنگ اندر آمد حبش</p></div>
<div class="m2"><p>غمی گشت زان لشکر شیرفش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بکشتند زیشان فزون از شمار</p></div>
<div class="m2"><p>بپیچید دیگر سر از کارزار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز خون ریختن گشت روی زمین</p></div>
<div class="m2"><p>سراسر به کردار دریای چین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو از خون در و دشت آلوده شد</p></div>
<div class="m2"><p>ز کشته به هر جای بر توده شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بر توده خاشاکها برزدند</p></div>
<div class="m2"><p>بفرمود تا آتش اندر زدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو شب گشت بشنید آواز گرگ</p></div>
<div class="m2"><p>سکندر بپوشید خفتان و ترگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی پیش رو بود مهتر ز پیل</p></div>
<div class="m2"><p>به سر بر سرو داشت همرنگ نیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ازین نامداران فراوان بکشت</p></div>
<div class="m2"><p>بسی حمله بردند و ننمود پشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بکشتند فرجام کارش به تیر</p></div>
<div class="m2"><p>یکی آهنین کوه بد پیل گیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وزان جایگه تیز لشکر براند</p></div>
<div class="m2"><p>بسی نام دادار گیهان بخواند</p></div></div>