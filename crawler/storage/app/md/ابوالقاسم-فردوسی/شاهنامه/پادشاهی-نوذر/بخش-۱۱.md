---
title: >-
    بخش ۱۱
---
# بخش ۱۱

<div class="b" id="bn1"><div class="m1"><p>سوی شاه ترکان رسید آگهی</p></div>
<div class="m2"><p>کزان نامداران جهان شد تهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلش گشت پر آتش از درد و غم</p></div>
<div class="m2"><p>دو رخ را به خون جگر داد نم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآشفت و گفتا که نوذر کجاست</p></div>
<div class="m2"><p>کزو ویسه خواهد همی کینه خواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه چاره است جز خون او ریختن</p></div>
<div class="m2"><p>یکی کینهٔ نو برانگیختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دژخیم فرمود کو را کشان</p></div>
<div class="m2"><p>ببر تا بیاموزد او سرفشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپهدار نوذر چو آگاه شد</p></div>
<div class="m2"><p>بدانست کش روز کوتاه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپاهی پر از غلغل و گفت و گوی</p></div>
<div class="m2"><p>سوی شاه نوذر نهادند روی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببستند بازوش با بند تنگ</p></div>
<div class="m2"><p>کشیدندش از جای پیش نهنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دشت آوریدندش از خیمه خوار</p></div>
<div class="m2"><p>برهنه سر و پای و برگشته کار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو از دور دیدش زبان برگشاد</p></div>
<div class="m2"><p>ز کین نیاگان همی کرد یاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز تور و ز سلم اندر آمد نخست</p></div>
<div class="m2"><p>دل و دیده از شرم شاهان بشست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو گفت هر بد که آید سزاست</p></div>
<div class="m2"><p>بگفت و برآشفت و شمشیر خواست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بزد گردن خسرو تاجدار</p></div>
<div class="m2"><p>تنش را بخاک اندر افگند خوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد آن یادگار منوچهر شاه</p></div>
<div class="m2"><p>تهی ماند ایران ز تخت و کلاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ایا دانشی مرد بسیار هوش</p></div>
<div class="m2"><p>همه چادر آزمندی مپوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که تخت و کله چون تو بسیار دید</p></div>
<div class="m2"><p>چنین داستان چند خواهی شنید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رسیدی به جایی که بشتافتی</p></div>
<div class="m2"><p>سرآمد کزو آرزو یافتی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه جویی از این تیره خاک نژند</p></div>
<div class="m2"><p>که هم بازگرداندت مستمند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که گر چرخ گردان کشد زین تو</p></div>
<div class="m2"><p>سرانجام خاکست بالین تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس آن بستگان را کشیدند خوار</p></div>
<div class="m2"><p>به جان خواستند آنگهی زینهار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو اغریرث پرهنر آن بدید</p></div>
<div class="m2"><p>دل او ببر در چو آتش دمید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همی گفت چندین سر بی‌گناه</p></div>
<div class="m2"><p>ز تن دور ماند به فرمان شاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیامد خروشان به خواهشگری</p></div>
<div class="m2"><p>بیاراست با نامور داوری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که چندین سرافراز گرد و سوار</p></div>
<div class="m2"><p>نه با ترگ و جوشن نه در کارزار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرفتار کشتن نه والا بود</p></div>
<div class="m2"><p>نشیبست جایی که بالا بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سزد گر نیاید به جانشان گزند</p></div>
<div class="m2"><p>سپاری همیدون به من شان ببند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بریشان یکی غار زندان کنم</p></div>
<div class="m2"><p>نگهدارشان هوشمندان کنم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به ساری به زاری برآرند هوش</p></div>
<div class="m2"><p>تو از خون به کش دست و چندین مکوش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ببخشید جان‌شان به گفتار اوی</p></div>
<div class="m2"><p>چو بشنید با درد پیکار اوی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بفرمودشان تا به ساری برند</p></div>
<div class="m2"><p>به غل و به مسمار و خواری برند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو این کرده شد ساز رفتن گرفت</p></div>
<div class="m2"><p>زمین زیر اسپان نهفتن گرفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز پیش دهستان سوی ری کشید</p></div>
<div class="m2"><p>از اسپان به رنج و به تک خوی کشید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کلاه کیانی به سر بر نهاد</p></div>
<div class="m2"><p>به دینار دادن در اندرگشاد</p></div></div>