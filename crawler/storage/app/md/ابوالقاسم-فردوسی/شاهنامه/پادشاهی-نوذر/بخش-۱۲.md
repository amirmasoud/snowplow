---
title: >-
    بخش ۱۲
---
# بخش ۱۲

<div class="b" id="bn1"><div class="m1"><p>به گستهم و طوس آمد این آگهی</p></div>
<div class="m2"><p>که تیره شد آن فر شاهنشهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شمشیر تیز آن سر تاجدار</p></div>
<div class="m2"><p>به زاری بریدند و برگشت کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکندند موی و شخودند روی</p></div>
<div class="m2"><p>از ایران برآمد یکی های‌وهوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر سرکشان گشت پرگرد و خاک</p></div>
<div class="m2"><p>همه دیده پر خون همه جامه چاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوی زابلستان نهادند روی</p></div>
<div class="m2"><p>زبان شاه‌گوی و روان شاه‌جوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر زال رفتند با سوگ و درد</p></div>
<div class="m2"><p>رخان پر ز خون و سران پر ز گرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که زارا دلیرا شها نوذرا</p></div>
<div class="m2"><p>گوا تاجدارا مها مهترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگهبان ایران و شاه جهان</p></div>
<div class="m2"><p>سر تاجداران و پشت مهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرت افسر از خاک جوید همی</p></div>
<div class="m2"><p>زمین خون شاهان ببوید همی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گیایی که روید بران بوم و بر</p></div>
<div class="m2"><p>نگون دارد از شرم خورشید سر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی داد خواهیم و زاری کنیم</p></div>
<div class="m2"><p>به خون پدر سوگواری کنیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نشان فریدون بدو زنده بود</p></div>
<div class="m2"><p>زمین نعل اسپ ورا بنده بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به زاری و خواری سرش را ز تن</p></div>
<div class="m2"><p>بریدند با نامدار انجمن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه تیغ زهرآبگون برکشید</p></div>
<div class="m2"><p>به کین جستن آیید و دشمن کشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همانا برین سوگ با ما سپهر</p></div>
<div class="m2"><p>ز دیده فرو باردی خون به مهر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شما نیز دیده پر از خون کنید</p></div>
<div class="m2"><p>همه جامهٔ ناز بیرون کنید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که با کین شاهان نشاید که چشم</p></div>
<div class="m2"><p>نباشد پر از آب و دل پر ز خشم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه انجمن زار و گریان شدند</p></div>
<div class="m2"><p>چو بر آتش تیز بریان شدند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زبان داد دستان که تا رستخیز</p></div>
<div class="m2"><p>نبیند نیام مرا تیغ تیز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چمان چرمه در زیر تخت منست</p></div>
<div class="m2"><p>سنان‌دار نیزه درخت منست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رکابست پای مرا جایگاه</p></div>
<div class="m2"><p>یکی ترگ تیره سرم را کلاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برین کینه آرامش و خواب نیست</p></div>
<div class="m2"><p>همی چون دو چشمم به جوی آب نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روان چنان شهریار جهان</p></div>
<div class="m2"><p>درخشنده بادا میان مهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شما را به داد جهان آفرین</p></div>
<div class="m2"><p>دل ارمیده بادا به آیین و دین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز مادر همه مرگ را زاده‌ایم</p></div>
<div class="m2"><p>برینیم و گردن ورا داده‌ایم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو گردان سوی کینه بشتافتند</p></div>
<div class="m2"><p>به ساری سران آگهی یافتند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ازیشان بشد خورد و آرام و خواب</p></div>
<div class="m2"><p>پر از بیم گشتند از افراسیاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ازان پس به اغریرث آمد پیام</p></div>
<div class="m2"><p>که ای پرمنش مهتر نیک‌نام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به گیتی به گفتار تو زنده‌ایم</p></div>
<div class="m2"><p>همه یک به یک مر ترا بنده‌ایم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو دانی که دستان به زابلستان</p></div>
<div class="m2"><p>به جایست با شاه کابلستان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو برزین و چون قارن رزم‌زن</p></div>
<div class="m2"><p>چو خراد و کشواد لشکرشکن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یلانند با چنگهای دراز</p></div>
<div class="m2"><p>ندارند از ایران چنین دست باز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو تابند گردان ازین سو عنان</p></div>
<div class="m2"><p>به چشم اندر آرند نوک سنان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ازان تیز گردد رد افراسیاب</p></div>
<div class="m2"><p>دلش گردد از بستگان پرشتاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پس آنگه سر یک رمه بی‌گناه</p></div>
<div class="m2"><p>به خاک اندر آرد ز بهر کلاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر بیند اغریرث هوشمند</p></div>
<div class="m2"><p>مر این بستگان را گشاید ز بند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پراگنده گردیم گرد جهان</p></div>
<div class="m2"><p>زبان برگشاییم پیش مهان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به پیش بزرگان ستایش کنیم</p></div>
<div class="m2"><p>همان پیش یزدان نیایش کنیم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنین گفت اغریرث پرخرد</p></div>
<div class="m2"><p>کزین گونه گفتار کی درخورد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز من آشکارا شود دشمنی</p></div>
<div class="m2"><p>بجوشد سر مرد آهرمنی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یکی چاره سازم دگرگونه زین</p></div>
<div class="m2"><p>که با من نگردد برادر به کین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر ایدون که دستان شود تیزچنگ</p></div>
<div class="m2"><p>یکی لشکر آرد بر ما به جنگ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو آرد به نزدیک ساری رمه</p></div>
<div class="m2"><p>به دستان سپارم شما را همه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بپردازم آمل نیایم به جنگ</p></div>
<div class="m2"><p>سرم را ز نام اندرآرم به ننگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بزرگان ایران ز گفتار اوی</p></div>
<div class="m2"><p>بروی زمین برنهادند روی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو از آفرینش بپرداختند</p></div>
<div class="m2"><p>نوندی ز ساری برون تاختند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بپویید نزدیک دستان سام</p></div>
<div class="m2"><p>بیاورد ازان نامداران پیام</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که بخشود بر ما جهاندار ما</p></div>
<div class="m2"><p>شد اغریرث پر خرد یار ما</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یکی سخت پیمان فگندیم بن</p></div>
<div class="m2"><p>بران برنهادیم یکسر سخن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کز ایران چو دستان آزادمرد</p></div>
<div class="m2"><p>بیایند و جویند با وی نبرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گرانمایه اغریرث نیک پی</p></div>
<div class="m2"><p>ز آمل گذارد سپه را به ری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مگر زنده از چنگ این اژدها</p></div>
<div class="m2"><p>تن یک جهان مردم آید رها</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو پوینده در زابلستان رسید</p></div>
<div class="m2"><p>سراینده در پیش دستان رسید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بزرگان و جنگ‌آوران را بخواند</p></div>
<div class="m2"><p>پیام یلان پیش ایشان براند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ازان پس چنین گفت کای سروران</p></div>
<div class="m2"><p>پلنگان جنگی و نام‌آوران</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کدامست مردی کنارنگ دل</p></div>
<div class="m2"><p>به مردی سیه کرده در جنگ دل</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خریدار این جنگ و این تاختن</p></div>
<div class="m2"><p>به خورشید گردن برافراختن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ببر زد بران کار کشواد دست</p></div>
<div class="m2"><p>منم گفت یازان بدین داد دست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>برو آفرین کرد فرخنده زال</p></div>
<div class="m2"><p>که خرم بدی تا بود ماه و سال</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سپاهی ز گردان پرخاشجوی</p></div>
<div class="m2"><p>ز زابل به آمل نهادند روی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو از پیش دستان برون شد سپاه</p></div>
<div class="m2"><p>خبر شد به اغریرث نیک خواه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همه بستگان را به ساری بماند</p></div>
<div class="m2"><p>بزد نای رویین و لشکر براند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو گشواد فرخ به ساری رسید</p></div>
<div class="m2"><p>پدید آمد آن بندها را کلید</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>یکی اسپ مر هر یکی را بساخت</p></div>
<div class="m2"><p>ز ساری سوی زابلستان بتاخت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو آمد به دستان سام آگهی</p></div>
<div class="m2"><p>که برگشت گشواد با فرهی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یکی گنج ویژه به درویش داد</p></div>
<div class="m2"><p>سراینده را جامهٔ خویش داد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو گشواد نزدیک زابل رسید</p></div>
<div class="m2"><p>پذیره شدش زال زر چون سزید</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بران بستگان زار بگریست دیر</p></div>
<div class="m2"><p>کجا مانده بودند در چنگ شیر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>پس از نامور نوذر شهریار</p></div>
<div class="m2"><p>به سر خاک بر کرد و بگریست زار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به شهر اندر آوردشان ارجمند</p></div>
<div class="m2"><p>بیاراست ایوانهای بلند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چنان هم که هنگام نوذر بدند</p></div>
<div class="m2"><p>که با تاج و با تخت و افسر بدند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بیاراست دستان همه دستگاه</p></div>
<div class="m2"><p>شد از خواسته بی‌نیاز آن سپاه</p></div></div>