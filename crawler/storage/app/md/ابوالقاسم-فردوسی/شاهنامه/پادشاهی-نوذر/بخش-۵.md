---
title: >-
    بخش ۵
---
# بخش ۵

<div class="b" id="bn1"><div class="m1"><p>برآسود پس لشکر از هر دو روی</p></div>
<div class="m2"><p>برفتند روز دوم جنگجوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رده برکشیدند ایرانیان</p></div>
<div class="m2"><p>چنان چون بود ساز جنگ کیان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو افراسیاب آن سپه را بدید</p></div>
<div class="m2"><p>بزد کوس رویین و صف برکشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان شد ز گرد سواران جهان</p></div>
<div class="m2"><p>که خورشید گفتی شد اندر نهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهاده برآمد ز هر دو گروه</p></div>
<div class="m2"><p>بیابان نبود ایچ پیدا ز کوه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برانسان سپه بر هم آویختند</p></div>
<div class="m2"><p>چو رود روان خون همی ریختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر سو که قارن شدی رزمخواه</p></div>
<div class="m2"><p>فرو ریختی خون ز گرد سیاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا خاستی گرد افراسیاب</p></div>
<div class="m2"><p>همه خون شدی دشت چون رود آب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرانجام نوذر ز قلب سپاه</p></div>
<div class="m2"><p>بیامد به نزدیک او رزمخواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان نیزه بر نیزه انداختند</p></div>
<div class="m2"><p>سنان یک به دیگر برافراختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که بر هم نپیچد بران گونه مار</p></div>
<div class="m2"><p>شهان را چنین کی بود کارزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین تا شب تیره آمد به تنگ</p></div>
<div class="m2"><p>برو خیره شد دست پور پشنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از ایران سپه بیشتر خسته شد</p></div>
<div class="m2"><p>وزان روی پیکار پیوسته شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به بیچارگی روی برگاشتند</p></div>
<div class="m2"><p>به هامون برافگنده بگذاشتند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل نوذر از غم پر از درد بود</p></div>
<div class="m2"><p>که تاجش ز اختر پر از گرد بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو از دشت بنشست آوای کوس</p></div>
<div class="m2"><p>بفرمود تا پیش او رفت طوس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بشد طوس و گستهم با او به هم</p></div>
<div class="m2"><p>لبان پر ز باد و روان پر ز غم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفت آنک در دل مرا درد چیست</p></div>
<div class="m2"><p>همی گفت چندی و چندی گریست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از اندرز فرخ پدر یاد کرد</p></div>
<div class="m2"><p>پر از خون جگر لب پر از باد سرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کجا گفته بودش که از ترک و چین</p></div>
<div class="m2"><p>سپاهی بیاید به ایران زمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ازیشان ترا دل شود دردمند</p></div>
<div class="m2"><p>بسی بر سپاه تو آید گزند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز گفتار شاه آمد اکنون نشان</p></div>
<div class="m2"><p>فراز آمد آن روز گردنکشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کس از نامهٔ نامداران نخواند</p></div>
<div class="m2"><p>که چندین سپه کس ز ترکان براند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شما را سوی پارس باید شدن</p></div>
<div class="m2"><p>شبستان بیاوردن و آمدن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وزان جا کشیدن سوی زاوه کوه</p></div>
<div class="m2"><p>بران کوه البرز بردن گروه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ازیدر کنون زی سپاهان روید</p></div>
<div class="m2"><p>وزین لشکر خویش پنهان روید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز کار شما دل شکسته شوند</p></div>
<div class="m2"><p>برین خستگی نیز خسته شوند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز تخم فریدون مگر یک دو تن</p></div>
<div class="m2"><p>برد جان ازین بی‌شمار انجمن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ندانم که دیدار باشد جزین</p></div>
<div class="m2"><p>یک امشب بکوشیم دست پسین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شب و روز دارید کارآگهان</p></div>
<div class="m2"><p>بجویید هشیار کار جهان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ازین لشکر ار بد دهند آگهی</p></div>
<div class="m2"><p>شود تیره این فر شاهنشهی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شما دل مدارید بس مستمند</p></div>
<div class="m2"><p>که باید چنین بد ز چرخ بلند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی را به جنگ اندر آید زمان</p></div>
<div class="m2"><p>یکی با کلاه مهی شادمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تن کشته با مرده یکسان شود</p></div>
<div class="m2"><p>تپد یک زمان بازش آسان شود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدادش مران پندها چون سزید</p></div>
<div class="m2"><p>پس آن دست شاهانه بیرون کشید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرفت آن دو فرزند را در کنار</p></div>
<div class="m2"><p>فرو ریخت آب از مژه شهریار</p></div></div>