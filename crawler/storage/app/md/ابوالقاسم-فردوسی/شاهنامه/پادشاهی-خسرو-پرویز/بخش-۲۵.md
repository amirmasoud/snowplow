---
title: >-
    بخش ۲۵
---
# بخش ۲۵

<div class="b" id="bn1"><div class="m1"><p>چو خورشید گردنده بی‌رنگ شد</p></div>
<div class="m2"><p>ستاره به برج شباهنگ شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به فرمود قیصر به نیرنگ ساز</p></div>
<div class="m2"><p>که پیش آرد اندیشه‌های دراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسازید جای شگفتی طلسم</p></div>
<div class="m2"><p>که کس بازنشناسد او را به جسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشسته زنی خوب برتخت ناز</p></div>
<div class="m2"><p>پراز شرم با جامه‌های طراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین روی و زان رو پرستندگان</p></div>
<div class="m2"><p>پس پشت و پیش اندرش بندگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشسته بران تخت بی گفت وگوی</p></div>
<div class="m2"><p>بگریان زنی ماند آن خوب روی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمان تا زمان دست برآفتی</p></div>
<div class="m2"><p>سرشکی ز مژگان بینداختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرآنکس که دیدی مر او را ز دور</p></div>
<div class="m2"><p>زنی یافتی شیفته پر ز نور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که بگریستی بر مسیحا بزار</p></div>
<div class="m2"><p>دو رخ زرد و مژگان چو ابر بهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طلسم بزرگان چو آمد بجای</p></div>
<div class="m2"><p>بر قیصر آمد یکی رهنمای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز دانا چو بشنید قیصر برفت</p></div>
<div class="m2"><p>به پیش طلسم آمد آنگاه تفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازان جادویی در شگفتی بماند</p></div>
<div class="m2"><p>فرستاد و گستهم را پیش خواند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگستهم گفت ای گو نامدار</p></div>
<div class="m2"><p>یکی دختری داشتم چون نگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ببالید و آمدش هنگام شوی</p></div>
<div class="m2"><p>یکی خویش بد مرو را نامجوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به راه مسیحا بدو دادمش</p></div>
<div class="m2"><p>ز بی‌دانشی روی بگشادمش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرستادم او رابخان جوان</p></div>
<div class="m2"><p>سوی آسمان شد روان جوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کنون او نشستست با سوک و درد</p></div>
<div class="m2"><p>شده روز روشن برو لاژورد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه پندم پذیرد نه گوید سخن</p></div>
<div class="m2"><p>جهان نو از رنج او شد کهن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی رنج بردار و او راببین</p></div>
<div class="m2"><p>سخنهای دانندگان برگزین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جوانی و از گوهر پهلوان</p></div>
<div class="m2"><p>مگر با تو او برگشاید زبان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدو گفت گستهم کایدون کنم</p></div>
<div class="m2"><p>مگر از دلش رنج بیرون کنم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بنزد طلسم آمد آن نامدار</p></div>
<div class="m2"><p>گشاده دل و بر سخن کامگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چوآمد به نزدیک تختش فراز</p></div>
<div class="m2"><p>طلسم از بر تخت بردش نماز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرانمایه گستهم بنشست خوار</p></div>
<div class="m2"><p>سخن گفت با دختر سوکوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دلاور نخست اندر آمد بپند</p></div>
<div class="m2"><p>سخنها که او را بدی سودمند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدو گفت کای دخت قیصر نژاد</p></div>
<div class="m2"><p>خردمند نخروشد از کار داد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رهانیست از مرگ پران عقاب</p></div>
<div class="m2"><p>چه در بیشه شیر و چه ماهی در آب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه باد بد گفتن پهلوان</p></div>
<div class="m2"><p>که زن بی‌زبان بود و تن بی‌روان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به انگشت خود هر زمانی سرشک</p></div>
<div class="m2"><p>بینداختی پیش گویا پزشک</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چوگستهم ازو در شگفتی بماند</p></div>
<div class="m2"><p>فرستاد قیصر کس او را بخواند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه دیدی بدوگفت از دخترم</p></div>
<div class="m2"><p>کزو تیره گردد همی افسرم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدو گفت بسیار دادمش پند</p></div>
<div class="m2"><p>نبد پند من پیش او کاربند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دگر روز قیصر به بالوی گفت</p></div>
<div class="m2"><p>که امروز با اندیان باش جفت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همان نیز شاپور مهتر نژاد</p></div>
<div class="m2"><p>کند جان ما رابدین دخت شاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شوی پیش این دختر سوکوار</p></div>
<div class="m2"><p>سخن گویی ازنامور شهریار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مگر پاسخی یابی از دخترم</p></div>
<div class="m2"><p>کزو آتش آید همی برسرم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مگر بشنود پند و اندرزتان</p></div>
<div class="m2"><p>بداند سرماهی وارزتان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برآنم که امروز پاسخ دهد</p></div>
<div class="m2"><p>چوپاسخ به آواز فرخ دهد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شود رسته زین انده سوکوار</p></div>
<div class="m2"><p>که خوناب بارد همی برکنار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برفت آن گرامی سه آزادمرد</p></div>
<div class="m2"><p>سخن گوی وهریک بننگ نبرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ازیشان کسی روی پاسخ ندید</p></div>
<div class="m2"><p>زن بی‌زبان خامشی برگزید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ازان چاره نزدیک قیصر شدند</p></div>
<div class="m2"><p>ببیچارگی نزد داور شدند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که هرچند گفتیم ودادیم پند</p></div>
<div class="m2"><p>نبد پند ما مر ورا سودمند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چنین گفت قیصر که بد روزگار</p></div>
<div class="m2"><p>که ما سوکواریم زین سوکوار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ازان نامداران چو چاره نیافت</p></div>
<div class="m2"><p>سوی رای خراد بر زین شتاف</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدو گفت کای نامدار دبیر</p></div>
<div class="m2"><p>گزین سر تخمهٔ اردشیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یکی سوی این دختر اندر شوی</p></div>
<div class="m2"><p>مگر یک ره آواز او بشنوی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فرستاد با او یکی استوار</p></div>
<div class="m2"><p>ز ایوان به نزدیک آن سوکوار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چوخراد بر زین بیامد برش</p></div>
<div class="m2"><p>نگه کرد روی و سر و افسرش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همی‌بود پیشش زمانی دراز</p></div>
<div class="m2"><p>طلسم فریبنده بردش نماز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بسی گفت و زن هیچ پاسخ نداد</p></div>
<div class="m2"><p>پراندیشه شد مرد مهتر نژاد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سراپای زن راهمی‌بنگرید</p></div>
<div class="m2"><p>پرستندگان را بر او بدید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همی‌گفت گر زن زغم بیهش است</p></div>
<div class="m2"><p>پرستنده باری چرا خامش است</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر خود سرشکست در چشم اوی</p></div>
<div class="m2"><p>سزیدی اگر کم شدی خشم اوی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به پیش برش بر چکاند همی</p></div>
<div class="m2"><p>چپ وراست جنبش نداند همی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سرشکش که انداخت یک جای رفت</p></div>
<div class="m2"><p>نه جنبان شدش دست ونه پای رفت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اگرخود درین کالبد جان بدی</p></div>
<div class="m2"><p>جز از دست جاییش جنبان بدی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سرشکش سوی دیگر انداختی</p></div>
<div class="m2"><p>وگر دست جای دگر آختی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نبینم همی جنبش جان و جسم</p></div>
<div class="m2"><p>نباشد جز از فیلسوفی طلسم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بر قیصر آمد بخندید وگفت</p></div>
<div class="m2"><p>که این ماه رخ را خرد نیست جفت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>طلسمست کاین رومیان ساختند</p></div>
<div class="m2"><p>که بالوی و گستهم نشناختند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بایرانیان بربخندی همی</p></div>
<div class="m2"><p>وگر چشم ما را ببندی همی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چواین بشنود شاه خندان شود</p></div>
<div class="m2"><p>گشاده رخ و سیم دندان شود</p></div></div>