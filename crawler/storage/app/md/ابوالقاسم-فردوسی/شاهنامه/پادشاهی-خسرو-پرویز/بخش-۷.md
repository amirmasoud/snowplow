---
title: >-
    بخش ۷
---
# بخش ۷

<div class="b" id="bn1"><div class="m1"><p>وزان روی شد شهریار جوان</p></div>
<div class="m2"><p>چوبگذشت شاد از پل نهروان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه مهتران را زلشکر بخواند</p></div>
<div class="m2"><p>سزاوار بر تخت شاهی نشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گفت کای نیکدل سروران</p></div>
<div class="m2"><p>جهاندیده و کار کرده سران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشاهی مرا این نخستین سرست</p></div>
<div class="m2"><p>جز از آزمایش نه اندرخورست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجای کسی نیست ما را سپاس</p></div>
<div class="m2"><p>وگر چند هستیم نیکی شناس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمارا زما هیچ نیکی نبود</p></div>
<div class="m2"><p>که چندین غم ورنج باید فزود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیاکان ما را پرستیده‌اید</p></div>
<div class="m2"><p>بسی شور و تلخ جهان دیده‌اید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخواهم گشادن یکی راز خویش</p></div>
<div class="m2"><p>نهان دارم از لشکر آواز خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن گفتن من بایرانیان</p></div>
<div class="m2"><p>نباید که بیرون برند ازمیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کزین گفتن اندیشه من تباه</p></div>
<div class="m2"><p>شود چون بگویند پیش سپاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من امشب سگالیده‌ام تاختن</p></div>
<div class="m2"><p>سپه را به جنگ اندر انداختند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که بهرام را دیده‌ام در سخن</p></div>
<div class="m2"><p>سواریست اسپ افگن وکارکن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی کودکی بی‌خرد داندم</p></div>
<div class="m2"><p>بگرز و بشمشیر ترساندم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نداند که من شب شبیخون کنم</p></div>
<div class="m2"><p>برزم اندرون بیم بیرون کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگریار باشید بامن به جنگ</p></div>
<div class="m2"><p>چو شب تیره گردد نسازم درنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو شوید بعنبر شب تیره روی</p></div>
<div class="m2"><p>بیفشاند این گیسوی مشکبوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شما برنشینید با ساز جنگ</p></div>
<div class="m2"><p>همه گرز و خنجر گرفته بچنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بران برنهادند یکسر سپاه</p></div>
<div class="m2"><p>که یک تن نگردد زفرمان شاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو خسرو بیامد بپرده سرای</p></div>
<div class="m2"><p>زبیگانه مردم بپردخت جای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیاورد گستهم وبندوی را</p></div>
<div class="m2"><p>جهاندیده و گرد گردوی را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه کارزار شبیخون بگفت</p></div>
<div class="m2"><p>که با او مگر یار باشند و جفت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو گفت گستهم کای شهریار</p></div>
<div class="m2"><p>چرایی چنین ایمن از روزگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو با لشکر اکنون شبیخون کنی</p></div>
<div class="m2"><p>ز دلها مگر مهر بیرون کنی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپاه تو با لشکر دشمنند</p></div>
<div class="m2"><p>ابا او همه یک دل ویک تنند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز یک سو نبیره ز یک سو نیا</p></div>
<div class="m2"><p>به مغز اندرون کی بود کیمیا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ازین سو برادر وزان سو پدر</p></div>
<div class="m2"><p>همه پاک بسته یک اندر دگر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پدر چون کند با پسر کارزار</p></div>
<div class="m2"><p>بدین آروز کام دشمن مخار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نبایست گفت این سخن با سپاه</p></div>
<div class="m2"><p>چو گفتی کنون کار گردد تباه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدو گفت گردوی کاین خود گذشت</p></div>
<div class="m2"><p>گذشته همه باد گردد به دشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>توانایی و کام وگنج وسپاه</p></div>
<div class="m2"><p>سر مرد بینا نپیچد ز راه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدین رزمگه امشب اندر مباش</p></div>
<div class="m2"><p>ممان تا شود گنج و لشکر به لاش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که من بی‌گمانم کزین راز ما</p></div>
<div class="m2"><p>وزین ساختن در نهان سازما</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدان لشکر اکنون رسید آگهی</p></div>
<div class="m2"><p>نباید که تو سر بدشمن دهی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چوبشنید خسرو پسند آمدش</p></div>
<div class="m2"><p>به دل رای او سودمند آمدش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گزین کرد زان سرکشان مرد چند</p></div>
<div class="m2"><p>که باشند برنیک وبد یارمند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو خرداد برزین و گستهم شیر</p></div>
<div class="m2"><p>چوشاپور و چون اندیان دلیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو بندوی خراد لشکر فروز</p></div>
<div class="m2"><p>چو نستود لشکرکش نیوسوز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تلی بود پر سبزه وجای سور</p></div>
<div class="m2"><p>سپه را همی‌دید خسرو ز دور</p></div></div>