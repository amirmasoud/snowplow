---
title: >-
    بخش ۱۹
---
# بخش ۱۹

<div class="b" id="bn1"><div class="m1"><p>چوآمد بران شارستان شهریار</p></div>
<div class="m2"><p>سوار آمد از قیصر نامدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چیزی کزین مرز باید بخواه</p></div>
<div class="m2"><p>مدار آرزو را ز شاهان نگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که هرچند این پادشاهی مراست</p></div>
<div class="m2"><p>تو را با تن خویش داریم راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بران شارستان ایمن و شاد باش</p></div>
<div class="m2"><p>ز هر بد که اندیشی آزاد باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه روم یکسر تو را کهترند</p></div>
<div class="m2"><p>اگر چند گردنکش و مهترند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو را تا نسازم سلیح و سپاه</p></div>
<div class="m2"><p>نجویم خور و خواب و آرام گاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بشنید خسرو بدان شاد گشت</p></div>
<div class="m2"><p>روانش از اندیشه آزاد گشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بفرمود گستهم و بالوی را</p></div>
<div class="m2"><p>همان اندیان جهانجوی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخراد برزین وشاپور شیر</p></div>
<div class="m2"><p>چنین گفت پس شهریار دلیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که اسپان چو روشن شود زین کنید</p></div>
<div class="m2"><p>ببالای آن زین زرین کنید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بپوشید زربفت چینی قبای</p></div>
<div class="m2"><p>همه یک دلانید و پاکیزه رای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازین شارستان سوی قیصر شوید</p></div>
<div class="m2"><p>بگویید و گفتار او بشنوید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خردمند باشید وروشن روان</p></div>
<div class="m2"><p>نیوشنده و چرب و شیرین زبان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر ای دون که قیصر به میدان شود</p></div>
<div class="m2"><p>کمان خواهد ار نی به چوگان شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بکوشید با مرد خسروپرست</p></div>
<div class="m2"><p>بدان تا شما را نیاید شکست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سواری بداند کز ایران برند</p></div>
<div class="m2"><p>دلیری و نیرو ز شیران برند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بخراد برزین بفرمود شاه</p></div>
<div class="m2"><p>که چینی حریرآر و مشک سیاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به قیصر یکی نامه باید نوشت</p></div>
<div class="m2"><p>چو خورشید تابان بخرم بهشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سخنهای کوتاه و معنی بسی</p></div>
<div class="m2"><p>که آن یاد گیرد دل هر کسی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که نزدیک او فیلسوفان بوند</p></div>
<div class="m2"><p>بدان کوش تا یاوه‌ای نشنوند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چونامه بخواند زبان برگشای</p></div>
<div class="m2"><p>به گفتار با تو ندارند پای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ببالوی گفت آنچ قیصر ز من</p></div>
<div class="m2"><p>گشاید زبان بر سرانجمن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز فرمان و سوگند و پیمان و عهد</p></div>
<div class="m2"><p>تو اندر سخن یاد کن همچو شهد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدان انجمن تو زبان منی</p></div>
<div class="m2"><p>بهر نیک و بد ترجمان منی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به چیزی که برما نیاید شکست</p></div>
<div class="m2"><p>بکوشید و با آن بسایید دست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو پیمان گفتار من در پذیر</p></div>
<div class="m2"><p>سخن هرچ گفتم همه یادگیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شنیدند آواز فرخ جوان</p></div>
<div class="m2"><p>جهاندیده گردان روشن روان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه خواندند آفرین سر به سر</p></div>
<div class="m2"><p>که جز تو مبادا کسی تاجور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به نزدیک قیصر نهادند روی</p></div>
<div class="m2"><p>بزرگان روشن دل و راست گوی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو بشنید قیصر کز ایران مهان</p></div>
<div class="m2"><p>فرستادهٔ شهریار جهان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رسیدند نزدیک ایوان ز راه</p></div>
<div class="m2"><p>پذیره فرستاد چندی سپاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بیاراست کاخی به دیبای روم</p></div>
<div class="m2"><p>همه پیکرش گوهر و زر بوم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نشست از بر نامور تخت عاج</p></div>
<div class="m2"><p>به سر برنهاد آن دل افروز تاج</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بفرمود تا پرده برداشتند</p></div>
<div class="m2"><p>ز دهلیزشان تیز بگذاشتند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گرانمایه گستهم بد پیشرو</p></div>
<div class="m2"><p>پس او چوبالوی و شاپور گو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو خراد برزین و گرد اندیان</p></div>
<div class="m2"><p>همه تاج بر سر کمر برمیان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رسیدند نزدیک قیصر فراز</p></div>
<div class="m2"><p>چو دیدند بردند پیشش نماز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه یک زبان آفرین خواندند</p></div>
<div class="m2"><p>بران تخت زر گوهر افشاندند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نخستین بپرسید قیصر ز شاه</p></div>
<div class="m2"><p>از ایران وز لشکر و رنج راه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو بشنید خراد به رزین برفت</p></div>
<div class="m2"><p>برتخت با نامهٔ شاه تفت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بفرمان آن نامور شهریار</p></div>
<div class="m2"><p>نهادند کرسی زرین چهار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نشست این سه پرمایهٔ نیک رای</p></div>
<div class="m2"><p>همی‌بود خراد برزین بپای</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بفرمود قیصر که بر زیرگاه</p></div>
<div class="m2"><p>نشیند کسی کو بپیمود راه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چنین گفت خراد برزین که شاه</p></div>
<div class="m2"><p>مرا در بزرگی ندادست راه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که در پیش قیصر بیارم نشست</p></div>
<div class="m2"><p>چنین نامهٔ شاه ایران بدست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مگر بندگی را پسند آیمت</p></div>
<div class="m2"><p>به پیغام او سودمند آیمت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدو گفت قیصر که بگشای راز</p></div>
<div class="m2"><p>چه گفت آن خردمند گردن فراز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نخست آفرین بر جهاندار کرد</p></div>
<div class="m2"><p>جهان را بدان آفرین خوارکرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که اویست برتر زهر برتری</p></div>
<div class="m2"><p>توانا و داننده از هر دری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بفرمان او گردد این آسمان</p></div>
<div class="m2"><p>کجا برترست از مکان و زمان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سپهر و ستاره همه کرده‌اند</p></div>
<div class="m2"><p>بدین چرخ گردان برآورده‌اند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو از خاک مرجانور بنده کرد</p></div>
<div class="m2"><p>نخستین کیومرث را زنده کرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چنان تا بشاه آفریدون رسید</p></div>
<div class="m2"><p>کزان سرفرازان و را برگزید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پدید آمد آن تخمهٔ اندرجهان</p></div>
<div class="m2"><p>ببود آشکار آنچ بودی نهان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همی‌رو چنین تا سر کی قباد</p></div>
<div class="m2"><p>که تاج بزرگی به سر برنهاد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نیامد بدین دوده هرگز بدی</p></div>
<div class="m2"><p>نگه داشتندی ره ایزدی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کنون بنده‌ای ناسزاوار وگست</p></div>
<div class="m2"><p>بیامد بتخت کیان برنشست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همی‌داد خواهم ز بیدادگر</p></div>
<div class="m2"><p>نه افسر نه تخت و کلاه و کمر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هرآنکس که او برنشیند بتخت</p></div>
<div class="m2"><p>خرد باید و نامداری و بخت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شناسد که این تخت و این فرهی</p></div>
<div class="m2"><p>کرا بود و دیهیم شاهنشهی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مرا اندرین کار یاری کنید</p></div>
<div class="m2"><p>برین بی‌وفا کامگاری کنید</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>که پوینده گشتیم گرد جهان</p></div>
<div class="m2"><p>بشرم آمدیم از کهان ومهان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چوقیصر بران سان سخنها شنید</p></div>
<div class="m2"><p>برخساره شد چون گل شنبلید</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گل شنبلیدش پر از ژاله شد</p></div>
<div class="m2"><p>زبان و روانش پر ازناله شد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چوآن نامه برخواند بفزود درد</p></div>
<div class="m2"><p>شد آن تخت برچشم او لاژورد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بخراد بر زین جهاندار گفت</p></div>
<div class="m2"><p>که این نیست برمرد دانا نهفت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مرا خسرو از خویش و پیوند بیش</p></div>
<div class="m2"><p>ز جان سخن گوی دارمش پیش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سلیح است و هم گنج و هم لشکرست</p></div>
<div class="m2"><p>شما را ببین تا چه اندر خورست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اگر دیده خواهی ندارم دریغ</p></div>
<div class="m2"><p>که دیده به از گنج دینار و تیغ</p></div></div>