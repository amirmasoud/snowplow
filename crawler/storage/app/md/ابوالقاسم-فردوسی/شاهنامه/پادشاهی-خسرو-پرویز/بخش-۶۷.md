---
title: >-
    بخش ۶۷
---
# بخش ۶۷

<div class="b" id="bn1"><div class="m1"><p>چو آگاهی آمد ز خسرو به راه</p></div>
<div class="m2"><p>به نزد بزرگان و نزد سپاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که شیرین به مشکوی خسرو شدست</p></div>
<div class="m2"><p>کهن بود کار جهان نوشدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شهر زان کار غمگین شدند</p></div>
<div class="m2"><p>پر اندیشه و درد و نفرین شدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرفتند نزدیک خسرو سه روز</p></div>
<div class="m2"><p>چهارم چو بفروخت گیتی فروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرستاد خسرو مهان را بخواند</p></div>
<div class="m2"><p>بگاه گران مایگان برنشاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدیشان چنین گفت کاین روز چند</p></div>
<div class="m2"><p>ندیدم شما را شدم مستمند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیازردم از بهر آزارتان</p></div>
<div class="m2"><p>پراندیشه گشتم ز تیمارتان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی‌گفت و پاسخ نداد ایچ‌کس</p></div>
<div class="m2"><p>ز گفتن زبانها ببستند بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرآنکس که او داشت آزار و خشم</p></div>
<div class="m2"><p>یکایک به موبد نمودند چشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو موبد چنان دید برپای خاست</p></div>
<div class="m2"><p>به خسرو چنین گفت کای راد وراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به روز جوانی شدی شهریار</p></div>
<div class="m2"><p>بسی نیک و بد دیدی از روزگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شنیدی بسی نیک و بد در جهان</p></div>
<div class="m2"><p>ز کار بزرگان و کار مهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنون تخمهٔ مهتر آلوده شد</p></div>
<div class="m2"><p>بزرگی ازین تخمهٔ پالوده شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پدر پاک و مادر بود بی‌هنر</p></div>
<div class="m2"><p>چنان دان که پاکی نیاید ببر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز کژی نجوید کسی راستی</p></div>
<div class="m2"><p>که از راستی برکنی کاستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل ما غمی شد ز دیو سترگ</p></div>
<div class="m2"><p>که شد یار با شهریار بزرگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به ایران اگر زن نبودی جزین</p></div>
<div class="m2"><p>که خسرو بدو خواندی آفرین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نبودی چو شیرین به مشکوی او</p></div>
<div class="m2"><p>بهر جای روشن بدی روی او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نیاکانت آن دانشی راستان</p></div>
<div class="m2"><p>نکردند یاد از چنین داستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چوگشت آن سخنهای موبد دراز</p></div>
<div class="m2"><p>شهنشاه پاسخ نداد ایچ‌باز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین گفت موبد که فردا پگاه</p></div>
<div class="m2"><p>بیاییم یکسر بدین بارگاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مگر پاسخ شاه یابیم باز</p></div>
<div class="m2"><p>که امروزمان شد سخنها دراز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دگر روز شبگیر برخاستند</p></div>
<div class="m2"><p>همه بندگی را بیاراستند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی گفت موبد ندانست گفت</p></div>
<div class="m2"><p>دگر گفت کان با خرد بود جفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سیوم گفت که امروز پاسخ دهد</p></div>
<div class="m2"><p>سزد زو که آواز فرخ نهد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همه موبدان برگرفتند راه</p></div>
<div class="m2"><p>خرامان برفتند نزدیک شاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بزرگان گزیدند جای نشست</p></div>
<div class="m2"><p>بیامد یکی مرد تشتی بدست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو خورشید رخشنده پالوده گشت</p></div>
<div class="m2"><p>یکایک بران مهتران برگذشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بتشت اندرون ریختش خون گرم</p></div>
<div class="m2"><p>چو نزدیک شد تشت بنهاد نرم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از آن تشت هرکس بپیچید روی</p></div>
<div class="m2"><p>همه انجمن گشت پر گفت و گوی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همی‌کرد هر کس به خسرو نگاه</p></div>
<div class="m2"><p>همه انجمن خیره از بیم شاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به ایرانیان گفت کاین خون کیست</p></div>
<div class="m2"><p>نهاده بتشت اندر از بهر چیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدو گفت موبد که خون پلید</p></div>
<div class="m2"><p>کزو دشمنش گشت هرکش بدید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چوموبد چنین گفت برداشتش</p></div>
<div class="m2"><p>همه دست بردست بگذاشتش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز خون تشت پر مایه کردند پاک</p></div>
<div class="m2"><p>ببستند روشن به آب و به خاک</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو روشن شد و پاک تشت پلید</p></div>
<div class="m2"><p>بکرد آنک او شسته بد پرنبید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بمی بر پراگند مشک وگلاب</p></div>
<div class="m2"><p>شد آن تشت بی‌رنگ چون آفتاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز شیرین بران تشت بد رهنمون</p></div>
<div class="m2"><p>که آغاز چون بود و فرجام چون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به موبد چنین گفت خسرو که تشت</p></div>
<div class="m2"><p>همانا بد این گر دگرگونه گشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدو گفت موبد که نوشه بدی</p></div>
<div class="m2"><p>پدیدار شد نیکوی از بدی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بفرمان ز دوزخ توکردی بهشت</p></div>
<div class="m2"><p>همان خوب کردی تو کردار زشت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنین گفت خسرو که شیرین بشهر</p></div>
<div class="m2"><p>چنان بد که آن بی‌منش تشت زهر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کنون تشت می شد به مشکوی ما</p></div>
<div class="m2"><p>برین گونه پربو شد ازبوی ما</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز من گشت بدنام شیرین نخست</p></div>
<div class="m2"><p>ز پرمایگان نامداری نجست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همه مهتران خواندند آفرین</p></div>
<div class="m2"><p>که بی‌تاج وتختت مبادا زمین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بهی آن فزاید که تو به کنی</p></div>
<div class="m2"><p>مه آن شد بگیتی که تومه کنی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که هم شاه وهم موبد وهم ردی</p></div>
<div class="m2"><p>مگر بر زمین سایهٔ ایزدی</p></div></div>