---
title: >-
    بخش ۱۷
---
# بخش ۱۷

<div class="b" id="bn1"><div class="m1"><p>چو بگذشت لشکر بران تازه بوم</p></div>
<div class="m2"><p>بتندی همی‌راند تا مرز روم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین تا بیامد بران شارستان</p></div>
<div class="m2"><p>که قیصر ورا خواندی کارستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چواز دور ترسا بدید آن سپاه</p></div>
<div class="m2"><p>برفتند پویان به آبی راه و راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان باره اندر کشیدند رخت</p></div>
<div class="m2"><p>در شارستان را ببستند سخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فروماند زان شاه گیتی فروز</p></div>
<div class="m2"><p>به بیرون بماندند لشکر سه روز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرستاد روز چهارم کسی</p></div>
<div class="m2"><p>که نزدیک ما نیست لشکر بسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشها فرستید و یاری کنید</p></div>
<div class="m2"><p>چه برما همی کامگاری کنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نزدیک ایشان سخن خوار بود</p></div>
<div class="m2"><p>سپاهش همه سست و ناهار بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم آنگه برآمد یکی تیره ابر</p></div>
<div class="m2"><p>بغرید برسان جنگی هژبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز ابر اندران شارستان باد خاست</p></div>
<div class="m2"><p>بهر بر زنی بانگ و فریاد خاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چونیمی ز تیره شب اندر کشید</p></div>
<div class="m2"><p>ز باره یکی بهره شد ناپدید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه شارستان ماند اندر شگفت</p></div>
<div class="m2"><p>به یزدان سقف پوزش اندر گرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهر بر زنی بر علف ساختند</p></div>
<div class="m2"><p>سه پیر سکوبا برون تاختند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز چیزی که بود اندران تازه بوم</p></div>
<div class="m2"><p>همان جامه هایی که خیزد ز روم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببردند بالا به نزدیک شاه</p></div>
<div class="m2"><p>که پیدا شد ای شاه برما گناه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو خسرو جوان بود و برتر منش</p></div>
<div class="m2"><p>بدیشان نکرد از بدی سرزنش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدان شارستان دریکی کاخ بود</p></div>
<div class="m2"><p>که بالاش با ابر گستاخ بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فراوان بدو اندرون برده بود</p></div>
<div class="m2"><p>همان جای قیصر برآورده بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز دشت اندرآمد بدانجا گذشت</p></div>
<div class="m2"><p>فراوان بدان شارستان دربگشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه رومیان آفرین خواندند</p></div>
<div class="m2"><p>بپا اندرش گوهر افشاندند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو آباد جایی به چنگ آمدش</p></div>
<div class="m2"><p>برآسود و چندی درنگ آمدش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به قیصر یکی نامه بنوشت شاه</p></div>
<div class="m2"><p>ازان باد وباران وابر سیاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وزان شارستان سوی مانوی راند</p></div>
<div class="m2"><p>که آن را جهاندار مانوی خواند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زما نوییان هرک بیدار بود</p></div>
<div class="m2"><p>خردمند و راد و جهاندار بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سکوبا و رهبان سوی شهریار</p></div>
<div class="m2"><p>برفتند با هدیه و با نثار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی‌رفت با شاه چندی سخن</p></div>
<div class="m2"><p>ز باران و آن شارستان کهن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همی‌گفت هرکس که ما بنده‌ایم</p></div>
<div class="m2"><p>به گفتار خسرو سر افگنده‌ایم</p></div></div>