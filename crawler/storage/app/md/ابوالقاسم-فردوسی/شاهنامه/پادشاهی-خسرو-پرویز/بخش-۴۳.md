---
title: >-
    بخش ۴۳
---
# بخش ۴۳

<div class="b" id="bn1"><div class="m1"><p>چنین تا خبرها به ایران رسید</p></div>
<div class="m2"><p>بر پادشاه دلیران رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بهرام را پادشاهی و گنج</p></div>
<div class="m2"><p>ازان تو بیش است نابرده رنج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پراز درد و غم شد ز تیمار اوی</p></div>
<div class="m2"><p>دلش گشت پیچان ز کردار اوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی رای زد با بزرگان بهم</p></div>
<div class="m2"><p>بسی گفت و انداخت از بیش و کم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب تیره فرمود تا شد دبیر</p></div>
<div class="m2"><p>سرخامه را کرد پیکان تیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خاقان چینی یکی نامه کرد</p></div>
<div class="m2"><p>تو گفتی که از خنجرش خامه کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخست آفرین کرد بر کردگار</p></div>
<div class="m2"><p>توانا و دانا و به روزگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برازندهٔ هور و کیوان و ماه</p></div>
<div class="m2"><p>نشاننده شاه بر پیش گاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گزایندهٔ هرکه جوید بدی</p></div>
<div class="m2"><p>فزایندهٔ دانش ایزدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز نادانی و دانش وراستی</p></div>
<div class="m2"><p>ز کمی و کژی و از کاستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیابی چو گویی که یزدان یکیست</p></div>
<div class="m2"><p>ورا یار وهمتا و انباز نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیابد هر آنکس که نیکی بجست</p></div>
<div class="m2"><p>مباد آنک او دست بد را بشست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی بنده بد شاه را ناسپاس</p></div>
<div class="m2"><p>نه مهتر شناس و نه یزدان شناس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی خرد و بیکار و بی‌نام بود</p></div>
<div class="m2"><p>پدر بر کشیدش که هنگام بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نهان نیست کردار او در جهان</p></div>
<div class="m2"><p>میان کهان و میان مهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کس او را نپذیرفت کش مایه بود</p></div>
<div class="m2"><p>وگر در خرد برترین پایه بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بنزد تو آمد بپذرفتیش</p></div>
<div class="m2"><p>چو پر مایگان دست بگرفتیش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کس این راه برگیرد از راستان ؟</p></div>
<div class="m2"><p>نیم من بدین کار هم داستان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو این نامه آرند نزدیک تو</p></div>
<div class="m2"><p>پر اندیشه کن رای تاریک تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر آن بنده را پای کرده ببند</p></div>
<div class="m2"><p>فرستی بر ما شوی سودمند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وگر نه فرستم ز ایران سپاه</p></div>
<div class="m2"><p>به توران کنم روز روشن سیاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چوآن نامه نزدیک خاقان رسید</p></div>
<div class="m2"><p>بران گونه گفتار خسرو شنید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرستاده را گفت فردا پگاه</p></div>
<div class="m2"><p>چو آیی بدر پاسخ نامه خواه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فرستاده آمد دلی پر شتاب</p></div>
<div class="m2"><p>نبد زان سپس جای آرام و خواب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی‌بود تا شمع رخشان بدید</p></div>
<div class="m2"><p>به درگاه خاقان چینی دوید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیاورد خاقان هم آنگه دبیر</p></div>
<div class="m2"><p>ابا خامه و مشک و چینی حریر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به پاسخ نوشت آفرین نهان</p></div>
<div class="m2"><p>ز من بنده بر کردگار جهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دگر گفت کان نامه برخواندم</p></div>
<div class="m2"><p>فرستاده را پیش بنشاندم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>توبا بندگان گوی زین سان سخن</p></div>
<div class="m2"><p>نزیبد از آن خاندان کهن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که مه را ندارند یکسر به مه</p></div>
<div class="m2"><p>نه که را شناسند بر جای که</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همه چین و توران سراسر مراست</p></div>
<div class="m2"><p>به هیتال بر نیز فرمان رواست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نیم تا بدم مرد پیمان شکن</p></div>
<div class="m2"><p>تو با من چنین داستانها مزن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو من دست بهرام گیرم بدست</p></div>
<div class="m2"><p>وزان پس به مهر اندرم آرم شکست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نخواند مرا داور از آب پاک</p></div>
<div class="m2"><p>جز از پاک ایزد مرا نیست باک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو را گر بزرگی بیفزایدی</p></div>
<div class="m2"><p>خرد بیشتر زین بدی شایدی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بران نامه بر مهر بنهاد و گفت</p></div>
<div class="m2"><p>که با باد باید که باشید جفت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فرستاده آمد به نزدیک شاه</p></div>
<div class="m2"><p>به یک ماه کمتر بپیمود راه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو برخواند آن نامه را شهریار</p></div>
<div class="m2"><p>بپیچید و ترسان شد از روزگار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فرستاد و ایرانیان را بخواند</p></div>
<div class="m2"><p>سخن های خاقان سراسر براند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همان نامه بنمود و برخواندند</p></div>
<div class="m2"><p>بزرگان به اندیشه درماندند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنین یافت پاسخ ز ایرانیان</p></div>
<div class="m2"><p>که ای فر و آوند تاج کیان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنین کارها بر دل آسان مگیر</p></div>
<div class="m2"><p>یکی رای زن با خردمند پیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به نامه چنین کار آسان مکن</p></div>
<div class="m2"><p>مکن تیره این فر و شمع کهن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گزین کن از ایران یکی مرد پیر</p></div>
<div class="m2"><p>خردمند و زیبا و گرد و دبیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کز ایدر به نزدیک خاقان شود</p></div>
<div class="m2"><p>سخن گوید و راه او بشنود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بگوید که بهرام روز نخست</p></div>
<div class="m2"><p>که بود و پس از پهلوانی چه جست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همی تا کار او گشت راست</p></div>
<div class="m2"><p>خداوند را زان سپس بنده خواست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو نیکو گردد به یک ماه‌کار</p></div>
<div class="m2"><p>تمامی بسالی برد روزگار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو بهرام داماد خاقان بود</p></div>
<div class="m2"><p>ازو بد سرودن نه آسان بود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به خوبی سخن گفت باید بسی</p></div>
<div class="m2"><p>نهانی نباید که داند کسی</p></div></div>