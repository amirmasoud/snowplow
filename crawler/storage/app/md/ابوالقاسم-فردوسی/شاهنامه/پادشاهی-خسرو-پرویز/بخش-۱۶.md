---
title: >-
    بخش ۱۶
---
# بخش ۱۶

<div class="b" id="bn1"><div class="m1"><p>همی‌تاخت خسرو به پیش اندرون</p></div>
<div class="m2"><p>نه آب وگیا بود و نه رهنمون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنان را بدان باره کرده یله</p></div>
<div class="m2"><p>همی‌راند ناکام تا به اهله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پذیره شدندش بزرگان شهر</p></div>
<div class="m2"><p>کسی را که از مردمی بود بهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو خسرو به نزدیک ایشان رسید</p></div>
<div class="m2"><p>بران شهر لشکر فرود آورید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان چون فرود آمد اندر زمان</p></div>
<div class="m2"><p>نوندی بیامد ز ایران دمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بهرام چوبین یکی نامه داشت</p></div>
<div class="m2"><p>همان نامه پوشیده در جامه داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوشته سوی مهتری باهله</p></div>
<div class="m2"><p>که گرلشکر آید مکنشان یله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپاه من اینک پس اندر دمان</p></div>
<div class="m2"><p>بشهر تو آید زمان تا زمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مهتر برانگونه برنامه دید</p></div>
<div class="m2"><p>هم اندر زمان پیش خسرو دوید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چوخسرو نگه کرد و نامه بخواند</p></div>
<div class="m2"><p>ز کار جهان در شگفتی بماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بترسید که آید پس او سپاه</p></div>
<div class="m2"><p>بران نامه بر تنگدل گشت شاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازان شهر هم در زمان برنشست</p></div>
<div class="m2"><p>میان کیی تاختن را ببست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی‌تاخت تا پیش آب فرات</p></div>
<div class="m2"><p>ندید اندرو هیچ جای نبات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شده گرسنه مرد پیر وجوان</p></div>
<div class="m2"><p>یکی بیشه دیدند و آب روان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چوخسرو به پیش اندرون بیشه دید</p></div>
<div class="m2"><p>سپه را بران سبزه اندر کشید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شده گرسنه مرد ناهاروسست</p></div>
<div class="m2"><p>کمان را بزه کرد نخچیر جست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ندیدند چیزی بجایی دوان</p></div>
<div class="m2"><p>درخت و گیا بود و آب روان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پدید آمد اندر زمان کاروان</p></div>
<div class="m2"><p>شتر بود و پیش اندرون ساروان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو آن ساربان روی خسرو بدید</p></div>
<div class="m2"><p>بدان نامدار آفرین گسترید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدو گفت خسرو که نام توچیست</p></div>
<div class="m2"><p>کجا رفت خواهی و کام تو چیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدو گفت من قیس بن حارثم</p></div>
<div class="m2"><p>ز آزادگان عرب وارثم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز مصر آمدم با یکی کاروان</p></div>
<div class="m2"><p>برین کاروان بر منم ساروان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به آب فراتست بنگاه من</p></div>
<div class="m2"><p>از انجا بدین بیشه بد راه من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفت خسروکه از خوردنی</p></div>
<div class="m2"><p>چه داری هم از چیز گستردنی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که ما ماندگانیم و هم گرسنه</p></div>
<div class="m2"><p>نه توشست ما را نه بار و بنه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدو گفت تازی که ایدر بایست</p></div>
<div class="m2"><p>مرا با تو چیز و تن جان یکیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو بر شاه تازی بگسترد مهر</p></div>
<div class="m2"><p>بیاورد فربه یکی ماده سهر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بکشتند و آتش بر افروختند</p></div>
<div class="m2"><p>ترو خشک هیزم همی‌سوختند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر آتش پراگند چندی کباب</p></div>
<div class="m2"><p>بخوردن گرفتند یاران شتاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گرفتند واژ آنک بد دین پژوه</p></div>
<div class="m2"><p>بخوردن شتابید دیگر گروه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بخوردند بی‌نان فراوان کباب</p></div>
<div class="m2"><p>بیاراست هر مهتری جای خواب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زمانی بخفتند و برخاستند</p></div>
<div class="m2"><p>یکی آفرین نو آراستند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدان دادگر کو جهان آفرید</p></div>
<div class="m2"><p>توانایی و ناتوان آفرید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ازان پس به یاران چنین گفت شاه</p></div>
<div class="m2"><p>که هرکس که او بیش دارد گناه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به پیش من آنکس گرامی ترست</p></div>
<div class="m2"><p>وزان کهتران نیز نامی ترست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هرآنکس کجا بیش دارد بدی</p></div>
<div class="m2"><p>بگشت از من و از ره بخردی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بما بیش باید که دارد امید</p></div>
<div class="m2"><p>سراسر به نیکی دهیدش نوید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گرفتند یاران برو آفرین</p></div>
<div class="m2"><p>که ای پاک دل خسرو پاک دین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بپرسید زان مرد تازی که راه</p></div>
<div class="m2"><p>کدامست و من چون شوم با سپاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدو گفت هفتاد فرسنگ بیش</p></div>
<div class="m2"><p>شما را بیابان و کوهست پیش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چودستور باشی من ازگوشت و آب</p></div>
<div class="m2"><p>به راه آورم گر نسازی شتاب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بدو گفت خسرو جزین نیست رای</p></div>
<div class="m2"><p>که با توشه باشیم و با رهنمای</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هیونی بر افگند تازی به راه</p></div>
<div class="m2"><p>بدان تا برد راه پیش سپاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همی‌تاخت اندر بیابان و کوه</p></div>
<div class="m2"><p>پر از رنج و تیمار با آن گروه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یکی کاروان نیز دیگر به راه</p></div>
<div class="m2"><p>پدید آمد از دور پیش سپاه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یکی مرد بازارگان مایه دار</p></div>
<div class="m2"><p>بیامد هم آنگه بر شهریار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدو گفت شاه از کجایی بگوی</p></div>
<div class="m2"><p>کجا رفت خواهی چنین پوی پوی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدو گفت کز خرهٔ اردشیر</p></div>
<div class="m2"><p>یکی مرد بازارگانم دبیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بدو گفت نامت چه کرد آنک زاد</p></div>
<div class="m2"><p>چنین داد پاسخ که مهران ستاد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ازو توشه جست آن زمان شهریار</p></div>
<div class="m2"><p>بدو گفت سالار کای نامدار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خورش هست چندانک اندازه نیست</p></div>
<div class="m2"><p>اگر چهره بازارگان تازه نیست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بدو گفت خسرو که مهمان به راه</p></div>
<div class="m2"><p>بیابی فزونی شود دستگاه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سر بار بگشاد بازارگان</p></div>
<div class="m2"><p>درمگان به آمد ز دینارگان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خورش بر دو بنشست خود بر زمین</p></div>
<div class="m2"><p>همی‌خواند بر شهریار آفرین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چونان خورده شد مرد مهمان پرست</p></div>
<div class="m2"><p>بیامد گرفت آبدستان بدست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو از دور خراد بر زین بدید</p></div>
<div class="m2"><p>ز جایی که بد پیش خسرو دوید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز بازارگان بستد آن آب گرم</p></div>
<div class="m2"><p>بدن تا ندارد جهاندار شرم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پس آن مرد بازارگان پر شتاب</p></div>
<div class="m2"><p>می‌آورد برسان روشن گلاب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دگر باره خراد بر زین ز راه</p></div>
<div class="m2"><p>ازو بستد آن جام و شد نزد شاه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>پرستش پرستنده را داشت سود</p></div>
<div class="m2"><p>بران برتری برتریها فزود</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ازان پس ببازارگان گفت شاه</p></div>
<div class="m2"><p>که اکنون سپه را کدامست راه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نشست تو در خره اردشیر</p></div>
<div class="m2"><p>کجا باشد ای مرد مهمان‌پذیر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بدو گفت کای شاه با داد ورای</p></div>
<div class="m2"><p>ز بازارگانان منم پاک رای</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نشانش یکایک به خسرو بگفت</p></div>
<div class="m2"><p>همه رازها برگشاد از نهفت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بفرمود تا نام برنا و ده</p></div>
<div class="m2"><p>نویسد نویسندهٔ روزبه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ببازارگان گفت پدرود باش</p></div>
<div class="m2"><p>خرد را به دل تار و هم پود باش</p></div></div>