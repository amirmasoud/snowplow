---
title: >-
    بخش ۱۵
---
# بخش ۱۵

<div class="b" id="bn1"><div class="m1"><p>چو رستم ز مازندران گشت باز</p></div>
<div class="m2"><p>شه اندر زمان رزم را کرد ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سراپرده از شهر بیرون کشید</p></div>
<div class="m2"><p>سپه را همه سوی هامون کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپاهی که خورشید شد ناپدید</p></div>
<div class="m2"><p>چو گرد سیاه از میان بردمید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه دریا پدید و نه هامون و کوه</p></div>
<div class="m2"><p>زمین آمد از پای اسپان ستوه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی راند لشکر بران سان دمان</p></div>
<div class="m2"><p>نجست ایچ هنگام رفتن زمان</p></div></div>