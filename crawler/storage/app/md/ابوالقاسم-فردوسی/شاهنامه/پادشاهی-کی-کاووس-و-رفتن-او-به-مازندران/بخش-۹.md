---
title: >-
    بخش ۹
---
# بخش ۹

<div class="b" id="bn1"><div class="m1"><p>وزانجا سوی راه بنهاد روی</p></div>
<div class="m2"><p>چنان چون بود مردم راه‌جوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی رفت پویان به جایی رسید</p></div>
<div class="m2"><p>که اندر جهان روشنایی ندید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب تیره چون روی زنگی سیاه</p></div>
<div class="m2"><p>ستاره نه پیدا نه خورشید و ماه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خورشید گفتی به بند اندرست</p></div>
<div class="m2"><p>ستاره به خم کمند اندرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنان رخش را داد و بنهاد روی</p></div>
<div class="m2"><p>نه افراز دید از سیاهی نه جوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وزانجا سوی روشنایی رسید</p></div>
<div class="m2"><p>زمین پرنیان دید و یکسر خوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهانی ز پیری شده نوجوان</p></div>
<div class="m2"><p>همه سبزه و آبهای روان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه جامه بر برش چون آب بود</p></div>
<div class="m2"><p>نیازش به آسایش و خواب بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برون کرد ببر بیان از برش</p></div>
<div class="m2"><p>به خوی اندرون غرقه بد مغفرش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگسترد هر دو بر آفتاب</p></div>
<div class="m2"><p>به خواب و به آسایش آمد شتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لگام از سر رخش برداشت خوار</p></div>
<div class="m2"><p>رها کرد بر خوید در کشتزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بپوشید چون خشک شد خود و ببر</p></div>
<div class="m2"><p>گیاکرد بستر بسان هژبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخفت و بیاسود از رنج تن</p></div>
<div class="m2"><p>هم از رخش غم بد هم از خویشتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو در سبزه دید اسپ را دشتوان</p></div>
<div class="m2"><p>گشاده زبان سوی او شد دوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سوی رستم و رخش بنهاد روی</p></div>
<div class="m2"><p>یکی چوب زد گرم بر پای اوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو از خواب بیدار شد پیلتن</p></div>
<div class="m2"><p>بدو دشتوان گفت کای اهرمن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چرا اسپ بر خوید بگذاشتی</p></div>
<div class="m2"><p>بر رنج نابرده برداشتی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز گفتار او تیز شد مرد هوش</p></div>
<div class="m2"><p>بجست و گرفتش یکایک دو گوش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیفشرد و برکند هر دو ز بن</p></div>
<div class="m2"><p>نگفت از بد و نیک با او سخن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سبک دشتبان گوش را برگرفت</p></div>
<div class="m2"><p>غریوان و مانده ز رستم شگفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدان مرز اولاد بد پهلوان</p></div>
<div class="m2"><p>یکی نامجوی دلیر و جوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بشد دشتبان پیش او با خروش</p></div>
<div class="m2"><p>پر از خون به دستش گرفته دو گوش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو گفت مردی چو دیو سیاه</p></div>
<div class="m2"><p>پلنگینه جوشن از آهن کلاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه دشت سرتاسر آهرمنست</p></div>
<div class="m2"><p>وگر اژدها خفته بر جوشنست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برفتم که اسپش برانم ز کشت</p></div>
<div class="m2"><p>مرا خود به اسپ و به کشته نهشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرا دید برجست و یافه نگفت</p></div>
<div class="m2"><p>دو گوشم بکند و همانجا بخفت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو بشنید اولاد برگشت زود</p></div>
<div class="m2"><p>برون آمد از درد دل همچو دود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که تا بنگرد کاو چه مردست خود</p></div>
<div class="m2"><p>ابا او ز بهر چه کردست بد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی گشت اولاد در مرغزار</p></div>
<div class="m2"><p>ابا نامداران ز بهر شکار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو از دشتبان این شگفتی شنید</p></div>
<div class="m2"><p>به نخچیر گه بر پی شیر دید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عنان را بتابید با سرکشان</p></div>
<div class="m2"><p>بدان سو که بود از تهمتن نشان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو آمد به تنگ اندرون جنگجوی</p></div>
<div class="m2"><p>تهمتن سوی رخش بنهاد روی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نشست از بر رخش و رخشنده تیغ</p></div>
<div class="m2"><p>کشید و بیامد چو غرنده میغ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدو گفت اولاد نام تو چیست</p></div>
<div class="m2"><p>چه مردی و شاه و پناه تو کیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نبایست کردن برین ره گذر</p></div>
<div class="m2"><p>ره نره دیوان پرخاشخر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنین گفت رستم که نام من ابر</p></div>
<div class="m2"><p>اگر ابر باشد به زور هژبر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همه نیزه و تیغ بار آورد</p></div>
<div class="m2"><p>سران را سر اندر کنار آورد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به گوش تو گر نام من بگذرد</p></div>
<div class="m2"><p>دم و جان و خون و دلت بفسرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نیامد به گوشت به هر انجمن</p></div>
<div class="m2"><p>کمند و کمان گو پیلتن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هران مام کاو چون تو زاید پسر</p></div>
<div class="m2"><p>کفن دوز خوانیمش ار مویه‌گر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو با این سپه پیش من رانده‌ای</p></div>
<div class="m2"><p>همی گو ز برگنبد افشانده‌ای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نهنگ بلا برکشید از نیام</p></div>
<div class="m2"><p>بیاویخت از پیش زین خم خام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو شیر اندر آمد میان بره</p></div>
<div class="m2"><p>همه رزمگه شد ز کشته خره</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به یک زخم دو دو سرافگند خوار</p></div>
<div class="m2"><p>همی یافت از تن به یک تن چهار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سران را ز زخمش به خاک آورید</p></div>
<div class="m2"><p>سر سرکشان زیر پی گسترید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در و دشت شد پر ز گرد سوار</p></div>
<div class="m2"><p>پراگنده گشتند بر کوه و غار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همی گشت رستم چو پیل دژم</p></div>
<div class="m2"><p>کمندی به بازو درون شصت خم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به اولاد چون رخش نزدیک شد</p></div>
<div class="m2"><p>به کردار شب روز تاریک شد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بیفگند رستم کمند دراز</p></div>
<div class="m2"><p>به خم اندر آمد سر سرفراز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از اسپ اندر آمد دو دستش ببست</p></div>
<div class="m2"><p>بپیش اندر افگند و خود برنشست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدو گفت اگر راست گویی سخن</p></div>
<div class="m2"><p>ز کژی نه سر یابم از تو نه بن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نمایی مرا جای دیو سپید</p></div>
<div class="m2"><p>همان جای پولاد غندی و بید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به جایی که بستست کاووس کی</p></div>
<div class="m2"><p>کسی کاین بدیها فگندست پی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نمایی و پیدا کنی راستی</p></div>
<div class="m2"><p>نیاری به کار اندرون کاستی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>من این تخت و این تاج و گرز گران</p></div>
<div class="m2"><p>بگردانم از شاه مازندران</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تو باشی برین بوم و بر شهریار</p></div>
<div class="m2"><p>ار ایدونک کژی نیاری بکار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدو گفت اولاد دل را ز خشم</p></div>
<div class="m2"><p>بپرداز و بگشای یکباره چشم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تن من مپرداز خیره ز جان</p></div>
<div class="m2"><p>بیابی ز من هرچ خواهی همان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ترا خانهٔ بید و دیو سپید</p></div>
<div class="m2"><p>نمایم من این را که دادی نوید</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به جایی که بستست کاووس شاه</p></div>
<div class="m2"><p>بگویم ترا یک به یک شهر و راه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از ایدر به نزدیک کاووس کی</p></div>
<div class="m2"><p>صد افگنده بخشیده فرسنگ پی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وزانجا سوی دیو فرسنگ صد</p></div>
<div class="m2"><p>بیاید یکی راه دشوار و بد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>میان دو صد چاهساری شگفت</p></div>
<div class="m2"><p>به پیمایش اندازه نتوان گرفت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>میان دو کوهست این هول جای</p></div>
<div class="m2"><p>نپرید بر آسمان بر همای</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز دیوان جنگی ده و دو هزار</p></div>
<div class="m2"><p>به شب پاسبانند بر چاهسار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو پولاد غندی سپهدار اوی</p></div>
<div class="m2"><p>چو بیدست و سنجه نگهدار اوی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یکی کوه یابی مر او را به تن</p></div>
<div class="m2"><p>بر و کتف و یالش بود ده رسن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ترا با چنین یال و دست و عنان</p></div>
<div class="m2"><p>گذارندهٔ گرز و تیغ و سنان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چنین برز و بالا و این کار کرد</p></div>
<div class="m2"><p>نه خوب است با دیو جستن نبرد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کزو بگذری سنگلاخست و دشت</p></div>
<div class="m2"><p>که آهو بران ره نیارد گذشت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو زو بگذری رود آبست پیش</p></div>
<div class="m2"><p>که پهنای او بر دو فرسنگ بیش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>کنارنگ دیوی نگهدار اوی</p></div>
<div class="m2"><p>همه نره دیوان به فرمان اوی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>وزان روی بزگوش تا نرم پای</p></div>
<div class="m2"><p>چو فرسنگ سیصد کشیده سرای</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز بزگوش تا شاه مازندران</p></div>
<div class="m2"><p>رهی زشت و فرسنگهای گران</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>پراگنده در پادشاهی سوار</p></div>
<div class="m2"><p>همانا که هستند سیصدهزار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ز پیلان جنگی هزار و دویست</p></div>
<div class="m2"><p>کزیشان به شهر اندرون جای نیست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>نتابی تو تنها و گر ز آهنی</p></div>
<div class="m2"><p>بسایدت سوهان آهرمنی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چنان لشکری با سلیح و درم</p></div>
<div class="m2"><p>نبینی ازیشان یکی را دژم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بخندید رستم ز گفتار اوی</p></div>
<div class="m2"><p>بدو گفت اگر با منی راه جوی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ببینی کزین یک تن پیلتن</p></div>
<div class="m2"><p>چه آید بران نامدار انجمن</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به نیروی یزدان پیروزگر</p></div>
<div class="m2"><p>به بخت و به شمشیر تیز و هنر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو بینند تاو بر و یال من</p></div>
<div class="m2"><p>به جنگ اندرون زخم گوپال من</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به درد پی و پوستشان از نهیب</p></div>
<div class="m2"><p>عنان را ندانند باز از رکیب</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ازان سو کجا هست کاووس کی</p></div>
<div class="m2"><p>مرا راه بنمای و بردار پی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نیاسود تیره شب و پاک روز</p></div>
<div class="m2"><p>همی راند تا پیش کوه اسپروز</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بدانجا که کاووس لشکر کشید</p></div>
<div class="m2"><p>ز دیوان جادو بدو بد رسید</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چو یک نیمه بگذشت از تیره شب</p></div>
<div class="m2"><p>خروش آمد از دشت و بانگ جلب</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به مازندران آتش افروختند</p></div>
<div class="m2"><p>به هر جای شمعی همی سوختند</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>تهمتن به اولاد گفت آن کجاست</p></div>
<div class="m2"><p>که آتش برآمد همی چپ و راست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>در شهر مازندران است گفت</p></div>
<div class="m2"><p>که از شب دو بهره نیارند خفت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بدان جایگه باشد ارژنگ دیو</p></div>
<div class="m2"><p>که هزمان برآید خروش و غریو</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بخفت آن زمان رستم جنگجوی</p></div>
<div class="m2"><p>چو خورشید تابنده بنمود روی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بپیچید اولاد را بر درخت</p></div>
<div class="m2"><p>به خم کمندش درآویخت سخت</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>به زین اندر افگند گرز نیا</p></div>
<div class="m2"><p>همی رفت یکدل پر از کیمیا</p></div></div>