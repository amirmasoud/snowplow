---
title: >-
    بخش ۱
---
# بخش ۱

<div class="b" id="bn1"><div class="m1"><p>چو بر تخت بنشست بهرام گور</p></div>
<div class="m2"><p>برو آفرین کرد بهرام و هور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرستش گرفت آفریننده را</p></div>
<div class="m2"><p>جهاندار و بیدار و بیننده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خداوند پیروزی و برتری</p></div>
<div class="m2"><p>خداوند افزونی و کمتری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خداوند داد و خداوند رای</p></div>
<div class="m2"><p>کزویست گیتی سراسر به پای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازان پس چنین گفت کاین تاج و تخت</p></div>
<div class="m2"><p>ازو یافتم کافریدست بخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو هستم امید و هم زو هراس</p></div>
<div class="m2"><p>وزو دارم از نیکویها سپاس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شما هم بدو نیز نازش کنید</p></div>
<div class="m2"><p>بکوشید تا عهد او نشکنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبان برگشادند ایرانیان</p></div>
<div class="m2"><p>که بستیم ما بندگی را میان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که این تاج بر شاه فرخنده باد</p></div>
<div class="m2"><p>همیشه دل و بخت او زنده باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وزان پس همه آفرین خواندند</p></div>
<div class="m2"><p>همه بر سرش گوهر افشاندند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین گفت بهرام کای سرکشان</p></div>
<div class="m2"><p>ز نیک و بد روز دیده نشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه بندگانیم و ایزد یکیست</p></div>
<div class="m2"><p>پرستش جز او را سزاوار نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بد روز بی‌بیم داریمتان</p></div>
<div class="m2"><p>به بدخواه حاجت نیاریمتان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفت این و از پیش برخاستند</p></div>
<div class="m2"><p>برو آفرین نو آراستند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شب تیره بودند با گفت‌وگوی</p></div>
<div class="m2"><p>چو خورشید بر چرخ بنمود روی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به آرام بنشست بر گاه شاه</p></div>
<div class="m2"><p>برفتند ایرانیان بارخواه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنین گفت بهرام با مهتران</p></div>
<div class="m2"><p>که این نیکنامان و نیک‌اختران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به یزدان گراییم و رامش کنیم</p></div>
<div class="m2"><p>بتازیم و دل زین جهان برکنیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفت این و اسپ کیان خواستند</p></div>
<div class="m2"><p>کیی بارگاهش بیاراستند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سه دیگر چو بنشست بر تخت گفت</p></div>
<div class="m2"><p>که رسم پرستش نباید نهفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به هستی یزدان گوایی دهیم</p></div>
<div class="m2"><p>روان را بدین آشنایی دهیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بهشتست و هم دوزخ و رستخیز</p></div>
<div class="m2"><p>ز نیک و ز بد نیست راه گریز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کسی کو نگرود به روز شمار</p></div>
<div class="m2"><p>مر او را تو بادین و دانا مدار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به روز چهارم چو بر تخت عاج</p></div>
<div class="m2"><p>بسر بر نهاد آن پسندیده تاج</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنین گفت کز گنج من یک زمان</p></div>
<div class="m2"><p>نیم شاد کز مردم شادمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نیم خواستار سرای سپنج</p></div>
<div class="m2"><p>نه از بازگشتن به تیمار و رنج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که آنست جاوید و این ره‌گذار</p></div>
<div class="m2"><p>تو از آز پرهیز و انده مدار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به پنجم چنین گفت کز رنج کس</p></div>
<div class="m2"><p>نیم شاد تا باشدم دست‌رس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به کوشش بجوییم خرم بهشت</p></div>
<div class="m2"><p>خنک آنک جز تخم نیکی نکشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ششم گفت بر مردم زیردست</p></div>
<div class="m2"><p>مبادا که هرگز بجویم شکست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهان را ز دشمن تن‌آسان کنیم</p></div>
<div class="m2"><p>بداندیشگان را هراسان کنیم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به هفتم چو بنشست گفت ای مهان</p></div>
<div class="m2"><p>خردمند و بیدار و دیده جهان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو با مردم زفت زفتی کنیم</p></div>
<div class="m2"><p>همی با خردمند جفتی کنیم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هرانکس که با ما نسازند گرم</p></div>
<div class="m2"><p>بدی بیش ازان بیند او کز پدرم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هرانکس که فرمان ما برگزید</p></div>
<div class="m2"><p>غم و درد و رنجش نباید کشید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به هشتم چو بنشست فرمود شاه</p></div>
<div class="m2"><p>جوانوی را خواندن از بارگاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدو گفت نزدیک هر مهتری</p></div>
<div class="m2"><p>به هر نامداری و هر کشوری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی نامه بنویس با مهر و داد</p></div>
<div class="m2"><p>که بهرام بنشست بر تخت شاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خداوند بخشایش و راستی</p></div>
<div class="m2"><p>گریزنده از کژی و کاستی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که با فر و برزست و با مهر و داد</p></div>
<div class="m2"><p>نگیرد جز از پاک دادار یاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پذیرفتم آن را که فرمان برد</p></div>
<div class="m2"><p>گناه آن سگالد که درمان برد؟</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نشستم برین تخت فرخ پدر</p></div>
<div class="m2"><p>بر آیین طهمورث دادگر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به داد از نیاکان فزونی کنم</p></div>
<div class="m2"><p>شما را به دین رهنمونی کنم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جز از راستی نیست با هرکسی</p></div>
<div class="m2"><p>اگر چند ازو کژی آید بسی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بران دین زردشت پیغمبرم</p></div>
<div class="m2"><p>ز راه نیاکان خود نگذرم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نهم گفت زردشت پیشین بروی</p></div>
<div class="m2"><p>براهیم پیغمبر راست‌گوی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همه پادشاهید بر چیز خویش</p></div>
<div class="m2"><p>نگهبان مرز و نگهبان کیش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به فرزند و زن نیز هم پادشا</p></div>
<div class="m2"><p>خنک مردم زیرک و پارسا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نخواهیم آگندن زر به گنج</p></div>
<div class="m2"><p>که از گنج درویش ماند به رنج</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر ایزد مرا زندگانی دهد</p></div>
<div class="m2"><p>برین اختران کامرانی دهد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یکی رامشی نامه خوانید نیز</p></div>
<div class="m2"><p>کزان جاودان ارج یابید و چیز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز ما بر همه پادشاهی درود</p></div>
<div class="m2"><p>به ویژه که مهرش بود تار و پود</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نهادند بر نامه‌ها بر نگین</p></div>
<div class="m2"><p>فرستادگان خواست با آفرین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>برفتند با نامه‌ها موبدان</p></div>
<div class="m2"><p>سواران بینادل و بخردان</p></div></div>