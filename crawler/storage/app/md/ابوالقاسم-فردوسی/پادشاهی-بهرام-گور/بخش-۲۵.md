---
title: >-
    بخش ۲۵
---
# بخش ۲۵

<div class="b" id="bn1"><div class="m1"><p>سیوم روز بزم ردان ساختند</p></div>
<div class="m2"><p>نویسنده را پیش بنشاختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به می خوردن اندر چو بگشاد چهر</p></div>
<div class="m2"><p>یکی نامه بنوشت شادان به مهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر نامه کرد آفرین از نخست</p></div>
<div class="m2"><p>بران کو روان را به شادی بشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرد بر دل خویش پیرایه کرد</p></div>
<div class="m2"><p>به رنج تن از مردمی مایه کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه نیکویها ز یزدان شناخت</p></div>
<div class="m2"><p>خرد جست و با مرد دانا بساخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدانید کز داد جز نیکویی</p></div>
<div class="m2"><p>نیاید نکوبد در بدخویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرانکس که از کارداران ما</p></div>
<div class="m2"><p>سرافراز و جنگی سواران ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنالد نه بیند به جز چاه و دار</p></div>
<div class="m2"><p>وگر کشته بر خاک افگنده خوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بکوشید تا رنجها کم کنید</p></div>
<div class="m2"><p>دل غمگنان شاد و بی‌غم کنید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که گیتی فراوان نماند به کس</p></div>
<div class="m2"><p>بی‌آزاری و داد جویید و بس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدین گیتی اندر نشانه منم</p></div>
<div class="m2"><p>سر راستی را بهانه منم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که چندان سپه کرد آهنگ من</p></div>
<div class="m2"><p>هم آهنگ این نامدار انجمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از ایدر برفتم به اندک سپاه</p></div>
<div class="m2"><p>شدند آنک بدخواه بد نیک خواه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی نامداری چو خاقان چین</p></div>
<div class="m2"><p>جهاندار با تاج و تخت و نگین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به دست من‌اندر گرفتار شد</p></div>
<div class="m2"><p>سر بخت ترکان نگونسار شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا کرد پیروز یزدان پاک</p></div>
<div class="m2"><p>سر دشمنان رفت در زیر خاک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جز از بندگی پیشهٔ من مباد</p></div>
<div class="m2"><p>جز از راست اندیشهٔ من مباد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نخواهم خراج از جهان هفت سال</p></div>
<div class="m2"><p>اگر زیردستی بود گر همال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به هر کارداری و خودکامه‌ای</p></div>
<div class="m2"><p>نوشتند بر پهلوی نامه‌ای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که از زیردستان جز از رسم و داد</p></div>
<div class="m2"><p>نرانید و از بد نگیرید یاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرانکس که درویش باشد به شهر</p></div>
<div class="m2"><p>که از روز شادی نیابند بهر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرستید نزدیک ما نامشان</p></div>
<div class="m2"><p>برآریم زان آرزو کامشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دگر هرک هستند پهلونژاد</p></div>
<div class="m2"><p>که گیرند از رفتن رنج یاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هم از گنج ما بی‌نیازی دهید</p></div>
<div class="m2"><p>خردمند را سرفرازی دهید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کسی را که فامست و دستش تهیست</p></div>
<div class="m2"><p>به هر کار بی‌ارج و بی فرهیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هم از گنج‌ماشان بتوزید فام</p></div>
<div class="m2"><p>به دیوانهایشان نویسید نام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز یزدان بخواهید تا هم چنین</p></div>
<div class="m2"><p>دل ما بدارد به آیین و دین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدین مهر ما شادمانی کنید</p></div>
<div class="m2"><p>بران مهتران مهربانی کنید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همان بندگان را مدارید خوار</p></div>
<div class="m2"><p>که هستند هم بندهٔ کردگار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کسی کش بود پایهٔ سنگیان</p></div>
<div class="m2"><p>دهد کودکان را به فرهنگیان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به دانش روان را توانگر کنید</p></div>
<div class="m2"><p>خرد را ز تن بر سر افسر کنید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز چیز کسان دور دارید دست</p></div>
<div class="m2"><p>بی‌آزار باشید و یزدان‌پرست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بکوشید و پیمان ما مشکنید</p></div>
<div class="m2"><p>پی و بیخ و پیوند بد برکنید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به یزدان پناهید و فرمان کنید</p></div>
<div class="m2"><p>روان را به مهرش گروگان کنید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مجویید آزار همسایگان</p></div>
<div class="m2"><p>هم آن بزرگان و پرمایگان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هرانکس که ناچیز بد چیره گشت</p></div>
<div class="m2"><p>وز اندازهٔ کهتری برگذشت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بزرگش مخوانید کان برتری</p></div>
<div class="m2"><p>سبک بازگردد سوی کهتری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز درویش چیزی مدارید باز</p></div>
<div class="m2"><p>هرانکس که هست از شما بی‌نیاز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به پاکان گرایید و نیکی کنید</p></div>
<div class="m2"><p>دل و پشت خواهندگان مشکنید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هران چیز کان دور گشت از پسند</p></div>
<div class="m2"><p>بدان چیز نزدیک باشد گزند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز دارنده بر جان آنکس درود</p></div>
<div class="m2"><p>که از مردمی باشدش تار و پود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو اندر نوشتند چینی حریر</p></div>
<div class="m2"><p>سر خامه را کرد مشکین دبیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به عنوان برش شاه گیتی نوشت</p></div>
<div class="m2"><p>دل داد و دانندهٔ خوب و زشت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خداوند بخشایش و فر و زور</p></div>
<div class="m2"><p>شهنشاه بخشنده بهرام گور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سوی مرزبانان فرمانبران</p></div>
<div class="m2"><p>خردمند و دانا و جنگی سران</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به هر سو نوند و سوار و هیون</p></div>
<div class="m2"><p>همی رفت با نامهٔ رهنمون</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو آن نامه آمد به هر کشوری</p></div>
<div class="m2"><p>به هر نامداری و هر مهتری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همی گفت هرکس که یزدان سپاس</p></div>
<div class="m2"><p>که هست این جهاندار یزدان شناس</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زن و مرد و کودک به هامون شدند</p></div>
<div class="m2"><p>به هر کشور از خانه بیرون شدند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همی خواندند آفرین نهان</p></div>
<div class="m2"><p>بران دادگر شهریار جهان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ازان پس به خوردن بیاراستند</p></div>
<div class="m2"><p>می و رود و رامشگران خواستند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یکی نیمه از روز خوردن بدی</p></div>
<div class="m2"><p>دگر نیمه زو کارکردن بدی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همی نو به هر بامدادی پگاه</p></div>
<div class="m2"><p>خروشی بدی پیش درگاه شاه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که هرکس که دارد خورید و دهید</p></div>
<div class="m2"><p>سپاسی ز خوردن به خود برنهید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کسی کش نیازست آید به گنج</p></div>
<div class="m2"><p>ستاند ز گنج درم سخته پنج</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سه من تافته بادهٔ سالخورده</p></div>
<div class="m2"><p>به رنگ گل نار و با رنگ زرد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هانی به رامش نهادند روی</p></div>
<div class="m2"><p>پرآواز میخواره شد شهر و کوی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چنان بد که از بید و گل افسری</p></div>
<div class="m2"><p>ز دیدار او خواستندی کری</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یکی شاخ نرگس به تای درم</p></div>
<div class="m2"><p>خریدی کسی زان نگشتی دژم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز شادی جوان شد دل مرد پیر</p></div>
<div class="m2"><p>به چشمه درون آبها گشت شیر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جهانجوی کرد از جهاندار یاد</p></div>
<div class="m2"><p>که یکسر جهان دید زان‌گونه شاد</p></div></div>