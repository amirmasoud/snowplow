---
title: >-
    بخش ۳۱
---
# بخش ۳۱

<div class="b" id="bn1"><div class="m1"><p>چو نزدیکی نرم‌پایان رسید</p></div>
<div class="m2"><p>نگه کرد و مردم بی‌اندازه دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه اسپ و نه جوشن نه تیغ و نه گرز</p></div>
<div class="m2"><p>ازان هر یکی چون یکی سرو برز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو رعد خروشان برآمد غریو</p></div>
<div class="m2"><p>برهنه سپاهی به کردار دیو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی سنگ‌باران بکردند سخت</p></div>
<div class="m2"><p>چو باد خزان برزند بر درخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تیر و به تیغ اندر آمد سپاه</p></div>
<div class="m2"><p>تو گفتی که شد روز روشن سیاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو از نرم‌پایان فراوان بماند</p></div>
<div class="m2"><p>سکندر برآسود و لشکر براند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشد تازیان تا به شهری رسید</p></div>
<div class="m2"><p>که آن را کران و میانه ندید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به آیین همه پیش باز آمدند</p></div>
<div class="m2"><p>گشاده‌دل و بی‌نیاز آمدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببردند هرگونه گستردنی</p></div>
<div class="m2"><p>ز پوشیدنیها و از خوردنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سکندر بپرسید و بنواختشان</p></div>
<div class="m2"><p>براندازه بر پایگه ساختشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کشیدند بر دشت پرده‌سرای</p></div>
<div class="m2"><p>سپاهش نجست اندر آن شهر جای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر اندر ستاره یکی کوه دید</p></div>
<div class="m2"><p>تو گفتی که گردون بخواهد کشید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بران کوه مردم بدی اندکی</p></div>
<div class="m2"><p>شب تیره زیشان نماندی یکی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بپرسید ازیشان سکندر که راه</p></div>
<div class="m2"><p>کدامست و چون راند باید سپاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه یکسره خواندند آفرین</p></div>
<div class="m2"><p>که ای نامور شهریار زمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به رفتن برین کوه بودی گذر</p></div>
<div class="m2"><p>اگر برگذشتی برو راه‌بر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی اژدهایست زان روی کوه</p></div>
<div class="m2"><p>که مرغ آید از رنج زهرش ستوه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیارد گذشتن بروبر سپاه</p></div>
<div class="m2"><p>همی دود زهرش برآید به ماه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی آتش افروزد از کام اوی</p></div>
<div class="m2"><p>دو گیسو بود پیل را دام اوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه شهر با او نداریم تاو</p></div>
<div class="m2"><p>خورش بایدش هر شبی پنج گاو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بجوییم و بر کوه خارا بریم</p></div>
<div class="m2"><p>پر اندیشه و پر مدارا بریم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدان تا نیاید بدین روی کوه</p></div>
<div class="m2"><p>نینجامید از ما گروها گروه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بفرمود سالار دیهیم جوی</p></div>
<div class="m2"><p>که آن روز ندهند چیز بدوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو گاه خورش درگذشت اژدها</p></div>
<div class="m2"><p>بیامد چو آتش بران تند جا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سکندر بفرمود تا لشکرش</p></div>
<div class="m2"><p>یکی تیرباران کنند ازبرش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بزد یک دم آن اژدهای پلید</p></div>
<div class="m2"><p>تنی چند ازیشان به دم درکشید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بفرمود اسکندر فیلقوس</p></div>
<div class="m2"><p>تبیره به زخم آوریدند و کوس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همان بی‌کران آتش افروختند</p></div>
<div class="m2"><p>به هرجای مشعل همی سوختند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو کوه از تبیره پرآواز گشت</p></div>
<div class="m2"><p>بترسید ازان اژدها بازگشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو خورشید برزد سر از برج گاو</p></div>
<div class="m2"><p>ز گلزاربرخاست بانگ چکاو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو آن اژدها را خورش بود گاه</p></div>
<div class="m2"><p>ز مردان لشکر گزین کرد شاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>درم داد سالار چندی ز گنج</p></div>
<div class="m2"><p>بیاورد با خویشتن گاو پنج</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بکشت و ز سرشان برآهخت پوست</p></div>
<div class="m2"><p>بدان جادوی داده دل مرد دوست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیاگند چرمش به زهر و به نفت</p></div>
<div class="m2"><p>سوی اژدها روی بنهاد تفت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مران چرمها را پر از باد کرد</p></div>
<div class="m2"><p>ز دادار نیکی دهش یاد کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بفرمود تا پوست برداشتند</p></div>
<div class="m2"><p>همی دست بر دست بگذاشتند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو نزدیکی اژدها رفت شاه</p></div>
<div class="m2"><p>بسان یکی ابر دیدش سپاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زبانش کبود و دو چشمش چو خون</p></div>
<div class="m2"><p>همی آتش آمد ز کامش برون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو گاو از سر کوه بنداختند</p></div>
<div class="m2"><p>بران اژدها دل بپرداختند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فرو برد چون باد گاو اژدها</p></div>
<div class="m2"><p>چو آمد ز چنگ دلیران رها</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو از گاو پیوندش آگنده شد</p></div>
<div class="m2"><p>بر اندام زهرش پراگنده شد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همه رودگانیش سوراخ کرد</p></div>
<div class="m2"><p>به مغز و به پی راه گستاخ کرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همی زد سرش را بران کوه سنگ</p></div>
<div class="m2"><p>چنین تا برآمد زمانی درنگ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سپاهی بروبر ببارید تیر</p></div>
<div class="m2"><p>به پای آمد آن کوه نخچیرگیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وزان جایگه تیز لشکر براند</p></div>
<div class="m2"><p>تن اژدها را هم‌انجا بماند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بیاورد لشکر به کوهی دگر</p></div>
<div class="m2"><p>کزان خیره شد مرد پرخاشخر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بلندیش بینا همی دیر دید</p></div>
<div class="m2"><p>سر کوه چون تیغ و شمشیر دید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یکی تخت زرین بران تیغ کوه</p></div>
<div class="m2"><p>ز انبوه یکسو و دور از گروه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یکی مرده مرد اندران تخت‌بر</p></div>
<div class="m2"><p>همانا که بودش پس از مرگ فر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز دیبا کشیده برو چادری</p></div>
<div class="m2"><p>ز هر گوهری بر سرش افسری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همه گرد بر گرد او سیم و زر</p></div>
<div class="m2"><p>کسی را نبودی بروبر گذر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هرآنکس که رفتی بران کوهسار</p></div>
<div class="m2"><p>که از مرده چیزی کند خواستار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بران کوه از بیم لرزان شدی</p></div>
<div class="m2"><p>به مردی و بر جای ریزان شدی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سکندر برآمد بران کوه‌سر</p></div>
<div class="m2"><p>نظاره بران مرد با سیم و زر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یکی بانگ بشنید کای شهریار</p></div>
<div class="m2"><p>بسی بردی اندر جهان روزگار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بسی تخت شاهان بپرداختی</p></div>
<div class="m2"><p>سرت را به گردون برافراختی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بسی دشمن و دوست کردی تباه</p></div>
<div class="m2"><p>ز گیتی کنون بازگشتست گاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>رخ شاه ز آواز شد چون چراغ</p></div>
<div class="m2"><p>ازان کوه برگشت دل پر ز داغ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همی رفت با نامداران روم</p></div>
<div class="m2"><p>بدان شارستان شد که خوانی هروم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که آن شهر یکسر زنان داشتند</p></div>
<div class="m2"><p>کسی را دران شهر نگذاشتند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سوی راست پستان چو آن زنان</p></div>
<div class="m2"><p>بسان یکی نار بر پرنیان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سوی چپ به کردار جوینده مرد</p></div>
<div class="m2"><p>که جوشن بپوشد به روز نبرد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو آمد به نزدیک شهر هروم</p></div>
<div class="m2"><p>سرافراز با نامداران روم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>یکی نامه بنوشت با رسم و داد</p></div>
<div class="m2"><p>چنانچون بود مرد فرخ‌نژاد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به عنوان بر از شاه ایران و روم</p></div>
<div class="m2"><p>سوی آنک دارند مرز هروم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سر نامه از کردگار سپهر</p></div>
<div class="m2"><p>کزویست بخشایش و داد و مهر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هرانکس که دارد روانش خرد</p></div>
<div class="m2"><p>جهان را به عمری همی بسپرد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شنید آنک ما در جهان کرده‌ایم</p></div>
<div class="m2"><p>سر مهتری بر کجا برده‌ایم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کسی کو ز فرمان ما سر بتافت</p></div>
<div class="m2"><p>نهالی به جز خاک تیره نیافت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>نخواهم که جایی بود در جهان</p></div>
<div class="m2"><p>که دیدار آن باشد از من نهان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گر آیم مرا با شما نیست رزم</p></div>
<div class="m2"><p>به دل آشتی دارم و رای بزم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>اگر هیچ دارید داننده‌ای</p></div>
<div class="m2"><p>خردمند و بیدار خواننده‌ای</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو برخواند این نامهٔ پندمند</p></div>
<div class="m2"><p>برآنکس که هست از شما ارجمند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ببندید پیش آمدن را میان</p></div>
<div class="m2"><p>کزین آمدن کس ندارد زیان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بفرمود تا فیلسوفی ز روم</p></div>
<div class="m2"><p>برد نامه نزدیک شهر هروم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بسی نیز شیرین سخنها بگفت</p></div>
<div class="m2"><p>فرستاده خود با خرد بود جفت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو دانا به نزدیک ایشان رسید</p></div>
<div class="m2"><p>همه شهر زن دید و مردی ندید</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>همه لشکر از شهر بیرون شدند</p></div>
<div class="m2"><p>به دیدار رومی به هامون شدند</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بران نامه‌بر شد جهان انجمن</p></div>
<div class="m2"><p>ازیشان هرانکس که بد رای زن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو این نامه برخواند دانای شهر</p></div>
<div class="m2"><p>ز رای دل شاه برداشت بهر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>نشستند و پاسخ نوشتند باز</p></div>
<div class="m2"><p>که دایم بزی شاه گردن فراز</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>فرستاده را پیش بنشاندیم</p></div>
<div class="m2"><p>یکایک همه نامه برخواندیم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نخستین که گفتی ز شاهان سخن</p></div>
<div class="m2"><p>ز پیروزی و رزمهای کهن</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>اگر لشکر آری به شهر هروم</p></div>
<div class="m2"><p>نبینی ز نعل و پی اسپ بوم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بی‌اندازه در شهر ما برزنست</p></div>
<div class="m2"><p>بهر برزنی بر هزاران زنست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>همه شب به خفتان جنگ اندریم</p></div>
<div class="m2"><p>ز بهر فزونی به تنگ اندریم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ز چندین یکی را نبودست شوی</p></div>
<div class="m2"><p>که دوشیزگانیم و پوشیده‌روی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز هر سو که آیی برین بوم و بر</p></div>
<div class="m2"><p>بجز ژرف دریا نبینی گذر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز ما هر زنی کو گراید بشوی</p></div>
<div class="m2"><p>ازان پس کس او را نه‌بینیم روی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بباید گذشتن به دریای ژرف</p></div>
<div class="m2"><p>اگر خوش و گر نیز باریده برف</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>اگر دختر آیدش چون کردشوی</p></div>
<div class="m2"><p>زن‌آسا و جویندهٔ رنگ و بوی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>هم آن خانه جاوید جای وی است</p></div>
<div class="m2"><p>بلند آسمانش هوای وی است</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>وگر مردوش باشد و سرفراز</p></div>
<div class="m2"><p>بسوی هرومش فرستند باز</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>وگر زو پسر زاید آنجا که هست</p></div>
<div class="m2"><p>بباشد نباشد بر ماش دست</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ز ما هرک او روزگار نبرد</p></div>
<div class="m2"><p>از اسپ اندر آرد یکی شیرمرد</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>یکی تاج زرینش بر سر نهیم</p></div>
<div class="m2"><p>همان تخت او بر دو پیکر نهیم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>همانا ز ما زن بود سی‌هزار</p></div>
<div class="m2"><p>که با تاج زرند و با گوشوار</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>که مردی ز گردنکشان روز جنگ</p></div>
<div class="m2"><p>به چنگال او خاک شد بی‌درنگ</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>تو مردی بزرگی و نامت بلند</p></div>
<div class="m2"><p>در نام بر خویشتن در مبند</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>که گویند با زن برآویختنی</p></div>
<div class="m2"><p>ز آویختن نیز بگریختی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>یکی ننگ باشد ترا زین سخن</p></div>
<div class="m2"><p>که تا هست گیتی نگردد کهن</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چه خواهی که با نامداران روم</p></div>
<div class="m2"><p>بیایی بگردی به مرز هروم</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چو با راستی باشی و مردمی</p></div>
<div class="m2"><p>نبینی جز از خوبی و خرمی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به پیش تو آریم چندان سپاه</p></div>
<div class="m2"><p>که تیره شود بر تو خورشید و ماه</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چو آن پاسخ نامه شد اسپری</p></div>
<div class="m2"><p>زنی بود گویا به پیغمبری</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ابا تاج و با جامهٔ شاهوار</p></div>
<div class="m2"><p>همی رفت با خوب‌رخ ده سوار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو آمد خرامان به نزدیک شاه</p></div>
<div class="m2"><p>پذیره فرستاد چندی به راه</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>زن نامبردار نامه بداد</p></div>
<div class="m2"><p>پیام دلیران همه کرد یاد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>سکندر چو آن پاسخ نامه دید</p></div>
<div class="m2"><p>خردمند و بینادلی برگزید</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بدیشان پیامی فرستاد و گفت</p></div>
<div class="m2"><p>که با مغز مردم خرد باد جفت</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>به گرد جهان شهریاری نماند</p></div>
<div class="m2"><p>همان بر زمین نامداری نماند</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>که نه سربسر پیش من کهترند</p></div>
<div class="m2"><p>وگرچه بلندند و نیک‌اخترند</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>مرا گرد کافور و خاک سیاه</p></div>
<div class="m2"><p>همانست و هم بزم و هم رزمگاه</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>نه من جنگ را آمدم تازیان</p></div>
<div class="m2"><p>به پیلان و کوس و تبیره زنان</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>سپاهی برین سان که هامون و کوه</p></div>
<div class="m2"><p>همی گردد از سم اسپان ستوه</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>مرا رای دیدار شهر شماست</p></div>
<div class="m2"><p>گر آیید نزدیک ما هم رواست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چو دیدار باشد برانم سپاه</p></div>
<div class="m2"><p>نباشم فراوان بدین جایگاه</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>ببینیم تا چیستتان رای و فر</p></div>
<div class="m2"><p>سواری و زیبایی و پای و پر</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>ز کار زهشتان بپرسم نهان</p></div>
<div class="m2"><p>که بی‌مرد زن چون بود در جهان</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>اگر مرگ باشد فزونی ز کیست</p></div>
<div class="m2"><p>به بینم که فرجام این کار چیست</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>فرستاده آمد سخنها بگفت</p></div>
<div class="m2"><p>همه راز بیرون کشید از نهفت</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بزرگان یکی انجمن ساختند</p></div>
<div class="m2"><p>ز گفتار دل را بپرداختند</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>که ما برگزیدیم زن دو هزار</p></div>
<div class="m2"><p>سخن‌گوی و داننده و هوشیار</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>ابا هر صدی بسته ده تاج زر</p></div>
<div class="m2"><p>بدو در نشانده فراوان گهر</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>چو گرد آید آن تاج باشد دویست</p></div>
<div class="m2"><p>که هر یک جز اندر خور شاه نیست</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>یکایک بسختیم و کردیم تل</p></div>
<div class="m2"><p>اباگوهران هر یکی سی رطل</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>چو دانیم کامد به نزدیک شاه</p></div>
<div class="m2"><p>یکایک پذیره شویمش به راه</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>چو آمد به نزدیک ما آگهی</p></div>
<div class="m2"><p>ز دانایی شاه وز فرهی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>فرستاده برگشت و پاسخ بگفت</p></div>
<div class="m2"><p>سخنها همه با خرد بود جفت</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>سکندر ز منزل سپه برگرفت</p></div>
<div class="m2"><p>ز کار زنان مانده اندر شگفت</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>دو منزل بیامد یکی باد خاست</p></div>
<div class="m2"><p>وزو برف با کوه و درگشت راست</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>تبه شد بسی مردم پایکار</p></div>
<div class="m2"><p>ز سرما و برف اندر آن روزگار</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>برآمد یکی ابر و دودی سیاه</p></div>
<div class="m2"><p>بر آتش همی رفت گفتی سپاه</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>زره کتف آزادگان را بسوخت</p></div>
<div class="m2"><p>ز نعل سواران زمین برفروخت</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بدین هم نشان تا به شهری رسید</p></div>
<div class="m2"><p>که مردم بسان شب تیره دید</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>فروهشته لفچ و برآورده کفچ</p></div>
<div class="m2"><p>به کردار قیر و شبه کفچ و لفچ</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>همه دیده‌هاشان به کردار خون</p></div>
<div class="m2"><p>همی از دهان آتش آمد برون</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بسی پیل بردند پیشش به راه</p></div>
<div class="m2"><p>همان هدیه مردمان سیاه</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>بگفتند کین برف و باد دمان</p></div>
<div class="m2"><p>ز ما بود کامد شما را زیان</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>که هرگز بدین شهر نگذشت کس</p></div>
<div class="m2"><p>ترا و سپاه تو دیدیم و بس</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>ببود اندر آن شهر یک ماه شاه</p></div>
<div class="m2"><p>چو آسوده گشتند شاه و سپاه</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>ازنجا بیامد دمان و دنان</p></div>
<div class="m2"><p>دل‌آراسته سوی شهر زنان</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>ز دریا گذر کرد زن دو هزار</p></div>
<div class="m2"><p>همه پاک با افسر و گوشوار</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>یکی بیشه بد پر ز آب و درخت</p></div>
<div class="m2"><p>همه جای روشن‌دل و نیکبخت</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>خورش گرد کردند بر مرغزار</p></div>
<div class="m2"><p>ز گستردنیها به رنگ و نگار</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>چو آمد سکندر به شهر هروم</p></div>
<div class="m2"><p>زنان پیش رفتند ز آباد بوم</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>ببردند پس تاجها پیش اوی</p></div>
<div class="m2"><p>همان جامه و گوهر و رنگ و بوی</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>سکندر بپذرفت و بنواختشان</p></div>
<div class="m2"><p>بران خرمی جایگه ساختشان</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>چو شب روز شد اندرآمد به شهر</p></div>
<div class="m2"><p>به دیدار برداشت زان شهر بهر</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>کم و بیش ایشان همی بازجست</p></div>
<div class="m2"><p>همی بود تا رازها شد درست</p></div></div>