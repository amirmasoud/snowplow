---
title: >-
    پادشاهی پوران دخت
---
# پادشاهی پوران دخت

<div class="b" id="bn1"><div class="m1"><p>یکی دختری بود پوران بنام</p></div>
<div class="m2"><p>چو زن شاه شد کارها گشت خام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بران تخت شاهیش بنشاندند</p></div>
<div class="m2"><p>بزرگان برو گوهر افشاندند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گفت پس دخت پوران که من</p></div>
<div class="m2"><p>نخواهم پراگندن انجمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی راکه درویش باشد ز گنج</p></div>
<div class="m2"><p>توانگر کنم تانماند به رنج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مبادا ز گیتی کسی مستمند</p></div>
<div class="m2"><p>که از درد او بر من آید گزند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز کشور کنم دور بدخواه را</p></div>
<div class="m2"><p>بر آیین شاهان کنم گاه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشانی ز پیروز خسرو بجست</p></div>
<div class="m2"><p>بیاورد ناگاه مردی درست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خبر چون به نزدیک پوران رسید</p></div>
<div class="m2"><p>ز لشکر بسی نامور برگزید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببردند پیروز راپیش اوی</p></div>
<div class="m2"><p>بدو گفت کای بد تن کینه جوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز کاری که کردی بیابی جزا</p></div>
<div class="m2"><p>چنانچون بود در خور ناسزا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مکافات یابی ز کرده کنون</p></div>
<div class="m2"><p>برانم ز گردن تو را جوی خون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز آخر هم آنگه یکی کره خواست</p></div>
<div class="m2"><p>به زین اندرون نوز نابوده راست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ببستش بران باره بر همچوسنگ</p></div>
<div class="m2"><p>فگنده به گردن درون پالهنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان کرهٔ تیز نادیده زین</p></div>
<div class="m2"><p>به میدان کشید آن خداوند کین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سواران به میدان فرستاد چند</p></div>
<div class="m2"><p>به فتراک بر گرد کرده کمند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که تا کره او را همی‌تاختی</p></div>
<div class="m2"><p>زمان تا زمانش بینداختی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زدی هر زمان خویشتن بر زمین</p></div>
<div class="m2"><p>بران کره بربود چند آفرین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنین تا برو بر بدرید چرم</p></div>
<div class="m2"><p>همی‌رفت خون از برش نرم نرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سرانجام جانش به خواری به داد</p></div>
<div class="m2"><p>چرا جویی از کار بیداد داد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همی‌داشت این زن جهان را به مهر</p></div>
<div class="m2"><p>نجست از بر خاک باد سپهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو شش ماه بگذشت بر کار اوی</p></div>
<div class="m2"><p>ببد ناگهان کژ پرگار اوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به یک هفته بیمار گشت و بمرد</p></div>
<div class="m2"><p>ابا خویشتن نام نیکی ببرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین است آیین چرخ روان</p></div>
<div class="m2"><p>توانا بهرکار و ما ناتوان</p></div></div>