---
title: >-
    بخش ۹
---
# بخش ۹

<div class="b" id="bn1"><div class="m1"><p>چو پالیزبان گفت و موبد شنید</p></div>
<div class="m2"><p>به روشن روان مرد دانا بدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که آن شیردل مرد جز شاه نیست</p></div>
<div class="m2"><p>همان چهر او جز در گاه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرستاده‌ای جست روشن‌روان</p></div>
<div class="m2"><p>فرستاد موبد بر پهلوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که پیدا شد آن فر شاپور شاه</p></div>
<div class="m2"><p>تو از هر سوی انجمن کن سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرستادهٔ موبد آمد دوان</p></div>
<div class="m2"><p>ز جایی که بد تا در پهلوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفت آنک در باغ شادی و بخت</p></div>
<div class="m2"><p>شکفته شد آن خسروانی درخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهبد ز گفتار او گشت شاد</p></div>
<div class="m2"><p>دلش پر ز کین گشت و لب پر ز باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دادار گفت ای جهاندار راست</p></div>
<div class="m2"><p>پرستش کنی جز ترا ناسزاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که دانست هرگز که شاپور شاه</p></div>
<div class="m2"><p>ببیند سپه نیز و او را سپاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپاس از تو ای دادگر یک خدای</p></div>
<div class="m2"><p>جهاندار و بر نیکویی رهنمای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شب برکشید آن درفش سیاه</p></div>
<div class="m2"><p>ستاره پدید آمد از گرد ماه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فراز آمد از هر سوی لشکری</p></div>
<div class="m2"><p>به جایی که بد در جهان مهتری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوی سورستان سربرافراختند</p></div>
<div class="m2"><p>یگان و دوگانه همی تاختند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به درگاه پالیزبان آمدند</p></div>
<div class="m2"><p>به شادی بر میزبان آمدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو لشکر شد آسوده بر درسرای</p></div>
<div class="m2"><p>به نزدیک شاه آمد آن پاک‌رای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به شاه جهان گفت پس میزبان</p></div>
<div class="m2"><p>خجستست بر ماه پالیزبان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپاه انجمن شد بدین درسرای</p></div>
<div class="m2"><p>نگه کن کنون تا چه آیدت رای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بفرمود تا برگشادند راه</p></div>
<div class="m2"><p>اگر چه فرومایه بد جایگاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو رفتند نزدیک آن نامجوی</p></div>
<div class="m2"><p>یکایک نهادند بر خاک روی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مهان را همه شاه در بر گرفت</p></div>
<div class="m2"><p>ز بدها خروشیدن اندر گرفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگفت آنک از چرم خر دیده بود</p></div>
<div class="m2"><p>سخنهای قیصر که بشنیده بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هم آزادی آن بت خوب‌چهر</p></div>
<div class="m2"><p>بگفت آنچ او کرد پیدا ز مهر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کزو یافتم جان و از کردگار</p></div>
<div class="m2"><p>که فرخنده بادا برو روزگار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وگر شهریاری و فرخنده‌ای</p></div>
<div class="m2"><p>بود بندهٔ پرهنر بنده‌ای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>منم بنده این مهربان بنده را</p></div>
<div class="m2"><p>گشاده‌دل و نازپرورده را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز هر سو که اکنون سپاه منست</p></div>
<div class="m2"><p>وگر پادشاهی و راه منست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه کس فرستید و آگه کنید</p></div>
<div class="m2"><p>طلایه پراگنده بر ره کنید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ببندید ویژه ره طیسفون</p></div>
<div class="m2"><p>نباید که آگاهی آید برون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو قیصر بیابد ز ما آگهی</p></div>
<div class="m2"><p>که بیدار شد فر شاهنشهی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بیاید سپاه مرا برکند</p></div>
<div class="m2"><p>دل و پشت ایرانیان بشکند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کنون ما نداریم پایاب اوی</p></div>
<div class="m2"><p>نه پیچیم با بخت شاداب اوی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو موبد بیاید بیارد سپاه</p></div>
<div class="m2"><p>ز لشکر ببندیم بر پشه راه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بسازیم و آرایشی نو کنیم</p></div>
<div class="m2"><p>نهانی مگر باغ بی‌خو کنیم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بباید به هر گوشه‌ای دیده‌بان</p></div>
<div class="m2"><p>طلایه به روز و به شب پاسبان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ازان پس نمانیم از رومیان</p></div>
<div class="m2"><p>کسی خسپد ایمن گشاده‌میان</p></div></div>