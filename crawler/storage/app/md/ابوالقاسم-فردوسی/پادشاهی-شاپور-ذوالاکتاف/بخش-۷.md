---
title: >-
    بخش ۷
---
# بخش ۷

<div class="b" id="bn1"><div class="m1"><p>چو بر زد سر از برج شیر آفتاب</p></div>
<div class="m2"><p>ببالید روز و بپالود خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جشن آمدند آنک بودی به شهر</p></div>
<div class="m2"><p>بزرگان جوینده از جشن بهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنیزک سوی چاره بنهاد روی</p></div>
<div class="m2"><p>چنانچون بود مردم چاره‌جوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو ایوان خالی به چنگ آمدش</p></div>
<div class="m2"><p>دل شیر و چنگ و پلنگ آمدش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو اسپ گرانمایه ز آخر ببرد</p></div>
<div class="m2"><p>گزیده سلیح سواران گرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دینار چندانک بایست نیز</p></div>
<div class="m2"><p>ز خوشاب و یاقوت و هرگونه چیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو آمد همه ساز رفتن به جای</p></div>
<div class="m2"><p>شب آمد دو تن راست کردند رای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوی شهر ایران نهادند روی</p></div>
<div class="m2"><p>دو خرم نهان شاد و آرامجوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب و روز یکسر همی تاختند</p></div>
<div class="m2"><p>به خواب و به خوردن نپرداختند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برین‌گونه از شهر بر خورستان</p></div>
<div class="m2"><p>همی راند تا کشور سورستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو اسب و تن از تاختن گشت سست</p></div>
<div class="m2"><p>فرود آمدن را همی جای جست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دهی خرم آمد به پیشش به راه</p></div>
<div class="m2"><p>پر از باغ و میدان و پر جشنگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تن از رنج خسته گریزان ز بد</p></div>
<div class="m2"><p>بیامد در باغبانی بزد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیامد دمان مرد پالیزبان</p></div>
<div class="m2"><p>که هم نیک‌دل بود و هم میزبان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دو تن دیده با نیزه و درع و خود</p></div>
<div class="m2"><p>ز شاپور پرسید هست این درود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدین بیگهی از کجا خاستی</p></div>
<div class="m2"><p>چنین تاختن را بیاراستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو گفت شاپور کای نیک‌خواه</p></div>
<div class="m2"><p>سخن چند پرسی ز گم کرده راه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک مرد ایرانیم راه‌جوی</p></div>
<div class="m2"><p>گریزان بدین مرز بنهاده روی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پر از دردم از قیصر و لشکرش</p></div>
<div class="m2"><p>مبادا که بینم سر و افسرش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر امشب مرا میزبانی کنی</p></div>
<div class="m2"><p>هشیواری و مرزبانی کنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برآنم که روزی به کار آیدت</p></div>
<div class="m2"><p>درختی که کشتی به بار آیدت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو باغبان گفت کین خان تست</p></div>
<div class="m2"><p>تن باغبان نیز مهمان تست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدان چیز کاید مرا دست‌رس</p></div>
<div class="m2"><p>بکوشم بیارم نگویم به کس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فرود آمد از باره شاپور شاه</p></div>
<div class="m2"><p>کنیزک همی رفت با او به راه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خورش ساخت چندان زن باغبان</p></div>
<div class="m2"><p>ز هر گونه چندانک بودش توان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو نان خورده شد کار می ساختند</p></div>
<div class="m2"><p>سبک مایه جایی بپرداختند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سبک باغبان می به شاپور داد</p></div>
<div class="m2"><p>که بردار ازان کس که آیدت یاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدو گفت شاپور کای میزبان</p></div>
<div class="m2"><p>سخن‌گوی و پرمایه پالیزبان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کسی کو می آرد نخست او خورد</p></div>
<div class="m2"><p>چو بیشش بود سالیان و خرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو از من به سال اندکی برتری</p></div>
<div class="m2"><p>تو باید که چون می دهی می خوری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدو باغبان گفت کای پرهنر</p></div>
<div class="m2"><p>نخست آن خورد می که با زیب‌تر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو باید که باشی برین پیش رو</p></div>
<div class="m2"><p>که پیری به فرهنگ و بر سال نو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همی بود تاج آید از موی تو</p></div>
<div class="m2"><p>همی رنگ عاج آید از روی تو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بخندید شاپور و بستد نبید</p></div>
<div class="m2"><p>یکی باد سرد از جگر برکشید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به پالیزبان گفت کای پاک‌دین</p></div>
<div class="m2"><p>چه آگاهی استت ز ایران زمین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنین دادپاسخ که ای برمنش</p></div>
<div class="m2"><p>ز تو دور بادا بد بدکنش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به بدخواه ما باد چندان زیان</p></div>
<div class="m2"><p>که از قیصر آمد به ایرانیان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از ایران پراگنده شد هرک بود</p></div>
<div class="m2"><p>نماند اندران بوم کشت و درود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز بس غارت و کشتن مرد و زن</p></div>
<div class="m2"><p>پراگنده گشت آن بزرگ انجمن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وزیشان بسی نیز ترسا شدند</p></div>
<div class="m2"><p>به زنار پیش سکوبا شدند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بس جاثلیقی به سر بر کلاه</p></div>
<div class="m2"><p>به دور از بر و بوم و آرامگاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بدو گفت شاپور شاه اورمزد</p></div>
<div class="m2"><p>که رخشان بدی همچو ماه اورمزد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کجا شد که قیصر چنین چیره شد</p></div>
<div class="m2"><p>ز بخت آب ایرانیان تیره شد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدو باغبان گفت کای سرفراز</p></div>
<div class="m2"><p>ترا جاودان مهتری باد و ناز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ازو مرده و زنده جایی نشان</p></div>
<div class="m2"><p>نیامد به ایران بدان سرکشان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هرانکس که بودند ز آبادبوم</p></div>
<div class="m2"><p>اسیرند سرتاسر اکنون به روم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>برین زار بگریست پالیزبان</p></div>
<div class="m2"><p>که بود آن زمان شاه را میزبان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدو میزان گفت کایدر سه روز</p></div>
<div class="m2"><p>بباشی بود خانه گیتی فروز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که دانا زد این داستان از نخست</p></div>
<div class="m2"><p>که هرکس که آزرم مهمان نجست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نباشد خرد هیچ نزدیک اوی</p></div>
<div class="m2"><p>نیاز آورد بخت تاریک اوی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بباش و بیاسای و می خور به کام</p></div>
<div class="m2"><p>چو گردد دلت رام بر گوی نام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بدو گفت شاپور کری رواست</p></div>
<div class="m2"><p>به مابر کنون میزبان پادشاست</p></div></div>