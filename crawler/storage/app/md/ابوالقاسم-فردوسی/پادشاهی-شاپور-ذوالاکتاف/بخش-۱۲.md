---
title: >-
    بخش ۱۲
---
# بخش ۱۲

<div class="b" id="bn1"><div class="m1"><p>عرض‌گاه و دیوان بیاراستند</p></div>
<div class="m2"><p>کلید در گنجها خواستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپاه انجمن شد چو روزی بداد</p></div>
<div class="m2"><p>سرش پر ز کین و دلش پر ز باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ایران همی راند تا مرز روم</p></div>
<div class="m2"><p>هرانکس که بود اندران مرز و بوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکشتند و خانش همی سوختند</p></div>
<div class="m2"><p>جهانی به آتش برافروختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آگاهی آمد ز ایران به روم</p></div>
<div class="m2"><p>که ویران شد آن مرز آباد بوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفتار شد قیصر نامدار</p></div>
<div class="m2"><p>شب تیره اندر صف کارزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سراسر همه روم گریان شدند</p></div>
<div class="m2"><p>وز آواز شاپور بریان شدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی گفت هرکس که این بد که کرد</p></div>
<div class="m2"><p>مگر قیصر آن ناجوانمرد مرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز قیصر یکی که برادرش بود</p></div>
<div class="m2"><p>پدر مرده و زنده مادرش بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جوانی کجا یانسش بود نام</p></div>
<div class="m2"><p>جهانجوی و بخشنده و شادکام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شدند انجمن لشکری بر درش</p></div>
<div class="m2"><p>درم داد پرخاشجو مادرش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو گفت کین برادر بخواه</p></div>
<div class="m2"><p>نبینی که آمد ز ایران سپاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بشنید یانس بجوشید و گفت</p></div>
<div class="m2"><p>که کین برادر نشاید نهفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بزد کوس و آورد بیرون صلیب</p></div>
<div class="m2"><p>صلیب بزرگ و سپاهی مهیب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپه را چو روی اندرآمد به روی</p></div>
<div class="m2"><p>بی‌آرام شد مردم کینه‌جوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رده برکشیدند و برخاست غو</p></div>
<div class="m2"><p>بیامد دوان یانس پیش رو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برآمد یکی ابر و گردی سیاه</p></div>
<div class="m2"><p>کزان تیرگی دیده گم کرد راه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سپه را به یک روی بر کوه بود</p></div>
<div class="m2"><p>دگر آب زانسو که انبوه بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدین گونه تا گشت خورشید زرد</p></div>
<div class="m2"><p>ز هر سو همی خاست گرد نبرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بکشتند چندانک روی زمین</p></div>
<div class="m2"><p>شد از جوشن کشتگان آهنین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو از قلب شاپور لشکر براند</p></div>
<div class="m2"><p>چپ و راستش ویژگان را بخواند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو با مهتران گرم کرد اسپ شاه</p></div>
<div class="m2"><p>زمین گشت جنبان و پیچان سپاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سوی لشکر رومیان حمله برد</p></div>
<div class="m2"><p>بزرگش یکی بود با مرد خرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدانست یانس که پایاب شاه</p></div>
<div class="m2"><p>ندارد گریزان بشد با سپاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پس‌اندر همی تاخت شاپور گرد</p></div>
<div class="m2"><p>به گرد از هوا روشنایی ببرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به هر جایگه بر یکی توده کرد</p></div>
<div class="m2"><p>گیاها به مغز سر آلوده کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ازان لشکر روم چندان بکشت</p></div>
<div class="m2"><p>که یک دشت سر بود بی‌پای و پشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به هامون سپاه و چلیپا نماند</p></div>
<div class="m2"><p>به دژها صلیب و سکوبا نماند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز هر جای چندان غنیمت گرفت</p></div>
<div class="m2"><p>که لشکر همی ماند زو در شگفت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ببخشید یکسر همه بر سپاه</p></div>
<div class="m2"><p>جز از گنج قیصر نبد بهر شاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کجا دیده‌بد رنج از گنج اوی</p></div>
<div class="m2"><p>نه هم گوشه بد گنج با رنج اوی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه لشکر روم گرد آمدند</p></div>
<div class="m2"><p>ز قیصر همی داستانها زدند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که ما را چنو نیز مهتر مباد</p></div>
<div class="m2"><p>به روم اندرون نام قیصر مباد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به روم اندرون جای مذبح نماند</p></div>
<div class="m2"><p>صلیب و مسیح و موشح نماند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو زنار قسیس شد سوخته</p></div>
<div class="m2"><p>چلیپا و مطران برافروخته</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کنون روم و قنوج ما را یکیست</p></div>
<div class="m2"><p>چو آواز دین مسیح اندکیست</p></div></div>