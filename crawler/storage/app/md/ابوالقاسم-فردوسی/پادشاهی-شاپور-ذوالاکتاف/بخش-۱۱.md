---
title: >-
    بخش ۱۱
---
# بخش ۱۱

<div class="b" id="bn1"><div class="m1"><p>چو شب دامن روز اندر کشید</p></div>
<div class="m2"><p>درفش خور آمد ز بالا پدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفرمود شاپور تا شد دبیر</p></div>
<div class="m2"><p>قلم خواست و انقاس و مشک و حریر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوشتند نامه به هر مهتری</p></div>
<div class="m2"><p>به هر پادشاهی و هر کشوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرنامه کرد آفرین مهان</p></div>
<div class="m2"><p>ز ما بنده بر کردگار جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که اوراست بر نیکویی دست‌رس</p></div>
<div class="m2"><p>به نیرو نیازش نیاید به کس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همو آفرینندهٔ روزگار</p></div>
<div class="m2"><p>به نیکی همو باشد آموزگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو قیصر که فرمان یزدان بهشت</p></div>
<div class="m2"><p>به ایران به جز تخم زشتی نکشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به زاری همی بند ساید کنون</p></div>
<div class="m2"><p>چو جان را نبودش خرد رهنمون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان تاج ایران بدو در سپرد</p></div>
<div class="m2"><p>ز گیتی به جز نام زشتی نبرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گسسته شد آن لشکر و بارگاه</p></div>
<div class="m2"><p>به نیروی یزدان که بنمود راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرانکس که باشد ز رومی به شهر</p></div>
<div class="m2"><p>ز شمشیر باید که یابند بهر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه داد جویید و فرمان کنید</p></div>
<div class="m2"><p>به خوبی ز سر باز پیمان کنید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیونی بر آمد ز هر سو دمان</p></div>
<div class="m2"><p>ابا نامهٔ شاه روشن روان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز لشکرگه آمد سوی طیسفون</p></div>
<div class="m2"><p>بی‌آزار بنشست با رهنمون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو تاج نیاکانش بر سر نهاد</p></div>
<div class="m2"><p>ز دادار نیکی دهش کرد یاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بفرمود تا شد به زندان دبیر</p></div>
<div class="m2"><p>به انقاس بنوشت نام اسیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هزار و صد و ده برآمد شمار</p></div>
<div class="m2"><p>بزرگان روم آنک بد نامدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه خویش و پیوند قیصر بدند</p></div>
<div class="m2"><p>به روم اندرون ویژه مهتر بدند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهاندار ببریدشان دست و پای</p></div>
<div class="m2"><p>هرانکس که بد بر بدی رهنمای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بفرمود تا قیصر روم را</p></div>
<div class="m2"><p>بیارند سالار آن بوم را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بشد روزبان دست قیصرکشان</p></div>
<div class="m2"><p>ز زندان بیاورد چون بیهشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جفادیده چون روی شاپور دید</p></div>
<div class="m2"><p>سرشکش ز دیده به رخ بر چکید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بمالید رنگین رخش بر زمین</p></div>
<div class="m2"><p>همی کرد بر تاج و تخت آفرین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زمین را سراسر به مژگان برفت</p></div>
<div class="m2"><p>به موی و به روی گشت با خاک جفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدو گفت شاه ای سراسر بدی</p></div>
<div class="m2"><p>که ترسایی و دشمن ایزدی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پسر گویی آنرا کش انباز نیست</p></div>
<div class="m2"><p>ز گیتیش فرجام و آغاز نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ندانی تو گفتن سخن جز دروغ</p></div>
<div class="m2"><p>دروغ آتشی بد بود بی‌فروغ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر قیصری شرم و رایت کجاست</p></div>
<div class="m2"><p>به خوبی دل رهنمایت کجاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چرا بندم از چرم خر ساختی</p></div>
<div class="m2"><p>بزرگی به خاک اندر انداختی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو بازارگانان به بزم آمدم</p></div>
<div class="m2"><p>نه با کوس و لشکر به رزم آمدم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو مهمان به چرم خر اندر کنی</p></div>
<div class="m2"><p>به ایران گرایی و لشکر کنی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ببینی کنون جنگ مردان مرد</p></div>
<div class="m2"><p>کزان پس نجویی به ایران نبرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدو گفت قیصر که ای شهریار</p></div>
<div class="m2"><p>ز فرمان یزدان که یابد گذار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز من بخت شاها خرد دور کرد</p></div>
<div class="m2"><p>روانم بر دیو مزدور کرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مکافات بد گر کنی نیکوی</p></div>
<div class="m2"><p>به گیتی درون داستانی شوی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که هرگز نگردد کهن نام تو</p></div>
<div class="m2"><p>برآید به مردی همه کام تو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر یابم از تو به جان زینهار</p></div>
<div class="m2"><p>به چشمم شود گنج و دینار خوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی بنده باشم به درگاه تو</p></div>
<div class="m2"><p>نجویم جز آرایش گاه تو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدو شاه گفت ای بد بی‌هنر</p></div>
<div class="m2"><p>چرا کردی این بوم زیر و زبر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کنون هرک بردی ز ایران اسیر</p></div>
<div class="m2"><p>همه باز خواهم ز تو ناگزیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دگر خواسته هرچ بردی به روم</p></div>
<div class="m2"><p>مبادا که بینی تو آن بوم شوم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همه یکسر از خانه بازآوری</p></div>
<div class="m2"><p>بدین لشکر سرفراز آوری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از ایران هرانجا که ویران شدست</p></div>
<div class="m2"><p>کنام پلنگان و شیران شدست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سراسر برآری به دینار خویش</p></div>
<div class="m2"><p>بیابی مکافات کردار خویش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دگر هرک کشتی ز ایرانیان</p></div>
<div class="m2"><p>بجویی ز روم از نژاد کیان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به یک تن ده از روم تاوان دهی</p></div>
<div class="m2"><p>روان را به پیمان گروگان دهی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نخواهم به جز مرد قیصرنژاد</p></div>
<div class="m2"><p>که باشند با ما بدین بوم شاد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دگر هرچ ز ایران بریدی درخت</p></div>
<div class="m2"><p>نبرد درخت گشن نیک‌بخت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بکاری و دیوارها برکنی</p></div>
<div class="m2"><p>ز دلها مگر خشم کمتر کنی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کنون من به بندی ببندم ترا</p></div>
<div class="m2"><p>ز چرم خران کی پسندم ترا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گرین هرچ گفتم نیاری به جای</p></div>
<div class="m2"><p>بدرند چرمت ز سر تا به پای</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دو گوشش به خنجر بدو شاخ کرد</p></div>
<div class="m2"><p>به یک جای بینیش سوراخ کرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مهاری به بینی او برنهاد</p></div>
<div class="m2"><p>چو شاپور زان چرم خر کرد یاد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دو بند گران برنهادش به پای</p></div>
<div class="m2"><p>ببردش همان روزبان باز جای</p></div></div>