---
title: >-
    بخش ۱۵
---
# بخش ۱۵

<div class="b" id="bn1"><div class="m1"><p>ببین این شگفتی که دهقان چه گفت</p></div>
<div class="m2"><p>بدانگه که بگشاد راز از نهفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شهر کجاران به دریای پارس</p></div>
<div class="m2"><p>چه گوید ز بالا و پهنای پارس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی شهر بد تنگ و مردم بسی</p></div>
<div class="m2"><p>ز کوشش بدی خوردن هر کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان شهر دختر فراوان بدی</p></div>
<div class="m2"><p>که بی‌کام جویندهٔ نان بدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یک روی نزدیک او بود کوه</p></div>
<div class="m2"><p>شدندی همه دختران همگروه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازان هر یکی پنبه بردی به سنگ</p></div>
<div class="m2"><p>یکی دوکدانی ز چوب خدنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دروازه دختر شدی همگروه</p></div>
<div class="m2"><p>خرامان ازین شهر تا پیش کوه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برآمیختندی خورشها بهم</p></div>
<div class="m2"><p>نبودی به خورد اندرون بیش و کم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نرفتی سخن گفتن از خواب و خورد</p></div>
<div class="m2"><p>ازان پنبه‌شان بود ننگ و نبرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شدندی شبانگه سوی خانه باز</p></div>
<div class="m2"><p>شده پنبه‌شان ریسمان طراز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدان شهر بی‌چیز و خرم نهاد</p></div>
<div class="m2"><p>یکی مرد بد نام او هفتواد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برین‌گونه بر نام او از چه رفت</p></div>
<div class="m2"><p>ازیراک او را پسر بود هفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرامی یکی دخترش بود و بس</p></div>
<div class="m2"><p>که نشمردی او دختران را به کس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان بد که روزی همه همگروه</p></div>
<div class="m2"><p>نشستند با دوک در پیش کوه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برآمیختند آن کجا داشتند</p></div>
<div class="m2"><p>به گاه خورش دوک بگذاشتند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنان بد که این دختر نیک‌بخت</p></div>
<div class="m2"><p>یکی سیب افگنده باد از درخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به ره بر بدید و سبک برگرفت</p></div>
<div class="m2"><p>ز من بشنو این داستان شگفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو آن خوب رخ میوه اندرگزید</p></div>
<div class="m2"><p>یکی در میان کرم آگنده دید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به انگشت زان سیب برداشتش</p></div>
<div class="m2"><p>بدان دوکدان نرم بگذاشتش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو برداشت زان دوکدان پنبه گفت</p></div>
<div class="m2"><p>به نام خداوند بی‌یار و جفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من امروز بر اختر کرم سیب</p></div>
<div class="m2"><p>به رشتن نمایم شما را نهیب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه دختران شاد و خندان شدند</p></div>
<div class="m2"><p>گشاده‌رخ و سیم دندان شدند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو چندان که رشتی به روزی برشت</p></div>
<div class="m2"><p>شمارش همی بر زمین برنوشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وزانجا بیامد به کردار دود</p></div>
<div class="m2"><p>به مادر نمود آن کجا رشته بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برو آفرین کرد مادر به مهر</p></div>
<div class="m2"><p>که برخوردی از مادر ای خوب‌چهر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به شبگیر چون ریسمان برشمرد</p></div>
<div class="m2"><p>دو چندانک هر بار بردی ببرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو آمد بدان چاره‌جوی انجمن</p></div>
<div class="m2"><p>به رشتن نهاده دل و گوش و تن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنین گفت با نامور دختران</p></div>
<div class="m2"><p>که ای ماه‌رویان و نیک‌اختران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من از اختر کرم چندان طراز</p></div>
<div class="m2"><p>بریسم که نیزم نیاید نیاز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به رشت آنکجا برده بد پیش ازین</p></div>
<div class="m2"><p>به کار آمدی گر بدی بیش ازین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سوی خانه برد آن طرازی که رشت</p></div>
<div class="m2"><p>دل مام او شد چو خرم بهشت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همی لختکی سیب هر بامداد</p></div>
<div class="m2"><p>پری‌روی دختر بران کرم داد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ازان پنبه هرچند کردی فزون</p></div>
<div class="m2"><p>برشتی همی دختر پرفسون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنان بد که یک روز مام و پدر</p></div>
<div class="m2"><p>بگفتند با دختر پرهنر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که چندین بریسی مگر با پری</p></div>
<div class="m2"><p>گرفتستی ای پاک تن خواهری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سبک سیم تن پیش مادر بگفت</p></div>
<div class="m2"><p>ازان سیب و آن کرمک اندر نهفت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همان کرم فرخ بدیشان نمود</p></div>
<div class="m2"><p>زن و شوی را روشنایی فزود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به فالی گرفت آن سخن هفتواد</p></div>
<div class="m2"><p>ز کاری نکردی به دل نیز یاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنین تا برآمد برین روزگار</p></div>
<div class="m2"><p>فروزنده‌تر گشت هر روز کار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مگر ز اختر کرم گفتی سخن</p></div>
<div class="m2"><p>برو نو شدی روزگار کهن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مر این کرم را خوار نگذاشتند</p></div>
<div class="m2"><p>بخوردنش نیکو همی داشتند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تن‌آور شد آن کرم و نیرو گرفت</p></div>
<div class="m2"><p>سر و پشت او رنگ نیکو گرفت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همی تنگ شد دوکدان بر تنش</p></div>
<div class="m2"><p>چو مشک سیه گشت پیراهنش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به مشک اندرون پیکر زعفران</p></div>
<div class="m2"><p>برو پشت او از کران تا کران</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یکی پاک صندوق کردش سیاه</p></div>
<div class="m2"><p>بدو اندرون ساخته جایگاه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چنان شد که در شهر بی‌هفتواد</p></div>
<div class="m2"><p>نگفتی سخن کس به بیداد و داد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>فراز آمدش ارج و آزرم و چیز</p></div>
<div class="m2"><p>توانگر شد آن هفت فرزند نیز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یکی میر بد اندر آن شهراوی</p></div>
<div class="m2"><p>سرافراز با لشکر و رنگ و بوی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بهانه همی ساخت بر هفتواد</p></div>
<div class="m2"><p>که دینار بستاند از بدنژاد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ازان آگهی مرد شد در نهیب</p></div>
<div class="m2"><p>بیامد ازان شهر دل با شکیب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همان هفت فرزند پیش اندرون</p></div>
<div class="m2"><p>پر از درد دل دیدگان پر ز خون</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز هر سو برانگیخت بانگ و نفیر</p></div>
<div class="m2"><p>برو انجمن گشت برنا و پیر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هرانجا که بایست دینار داد</p></div>
<div class="m2"><p>به کنداوران چیز بسیار داد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یکی لشکری شد بر او انجمن</p></div>
<div class="m2"><p>همه نامداران شمشیرزن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همه یکسره پیش فرزند اوی</p></div>
<div class="m2"><p>برفتند و گشتند پیکارجوی</p></div></div>