---
title: >-
    بخش ۹
---
# بخش ۹

<div class="b" id="bn1"><div class="m1"><p>چو شب روز شد بامداد پگاه</p></div>
<div class="m2"><p>بفرمود تا بازگردد سپاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیامد دو رخساره همرنگ نی</p></div>
<div class="m2"><p>چو شب تیره گشت اندر آمد بری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی نامه بنوشت نزد پسر</p></div>
<div class="m2"><p>که کژی به باغ اندر آورد بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان شد ز بالین ما اردشیر</p></div>
<div class="m2"><p>کزان سان نجست از کمان ایچ تیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوی پارس آمد بجویش نهان</p></div>
<div class="m2"><p>مگوی این سخن با کسی در جهان</p></div></div>