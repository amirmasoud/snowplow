---
title: >-
    بخش ۸
---
# بخش ۸

<div class="b" id="bn1"><div class="m1"><p>چنان بد که بی‌ماه روی اردوان</p></div>
<div class="m2"><p>نبودی شب و روز روشن‌روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دیبا نبرداشتی دوش و یال</p></div>
<div class="m2"><p>مگر چهر گلنار دیدی به فال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آمدش هنگام برخاستن</p></div>
<div class="m2"><p>به دیبا سر گاهش آراستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنیزک نیامد به بالین اوی</p></div>
<div class="m2"><p>برآشفت و پیچان شد از کین اوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدربر سپاه ایستاده به پای</p></div>
<div class="m2"><p>بیاراسته تخت و تاج و سرای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز درگاه برخاست سالار بار</p></div>
<div class="m2"><p>بیامد بر نامور شهریار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفت گردنکشان بر درند</p></div>
<div class="m2"><p>هر آنکس کجا مهتر کشورند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرستندگان را چنین گفت شاه</p></div>
<div class="m2"><p>که گلنار چون راه و آیین نگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندارد نیاید به بالین من</p></div>
<div class="m2"><p>که داند بدین داستان دین من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیامد هم‌انگاه مهتر دبیر</p></div>
<div class="m2"><p>که رفتست بیگاه دوش اردشیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وز آخر ببردست خنگ و سیاه</p></div>
<div class="m2"><p>که بد بارهٔ نامبردار شاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم‌انگاه شد شاه را دلپذیر</p></div>
<div class="m2"><p>که گنجور او رفت با اردشیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل مرد جنگی برآمد ز جای</p></div>
<div class="m2"><p>برآشفت و زود اندر آمد به پای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سواران جنگی فراوان ببرد</p></div>
<div class="m2"><p>تو گفتی همی باره آتش سپرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بره‌بر یکی نامور دید جای</p></div>
<div class="m2"><p>بسی اندرو مردم و چارپای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بپرسید زیشان که شبگیر هور</p></div>
<div class="m2"><p>شنیدی شما بانگ نعل ستور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی گفت زیشان که اندر گذشت</p></div>
<div class="m2"><p>دو تن بر دو باره درآمد به دشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی برگذشتند پویان به راه</p></div>
<div class="m2"><p>یکی بارهٔ خنگ و دیگر سیاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به دم سواران یکی غرم پاک</p></div>
<div class="m2"><p>چو اسپی همی بر پراگند خاک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به دستور گفت آن زمان اردوان</p></div>
<div class="m2"><p>که این غرم باری چرا شد دوان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین داد پاسخ که آن فر اوست</p></div>
<div class="m2"><p>به شاهی و نیک‌اختری پر اوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر این غرم دریابد او را متاز</p></div>
<div class="m2"><p>که این کار گردد بمابر دراز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرود آمد آن جایگه اردوان</p></div>
<div class="m2"><p>بخورد و برآسود و آمد دوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی تاختند از پس اردشیر</p></div>
<div class="m2"><p>به پیش اندرون اردوان و وزیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جوان با کنیزک چو باد دمان</p></div>
<div class="m2"><p>نپردخت از تاختن یک زمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کرا یار باشد سپهر بلند</p></div>
<div class="m2"><p>بروبر ز دشمن نیاید گزند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ازان تاختن رنجه شد اردشیر</p></div>
<div class="m2"><p>بدید از بلندی یکی آبگیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جوانمرد پویان به گلنار گفت</p></div>
<div class="m2"><p>که اکنون که با رنج گشتیم جفت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بباید بدین چشمه آمد فرود</p></div>
<div class="m2"><p>که شد باره و مرد بی‌تار و پود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بباشیم بر آب و چیزی خوریم</p></div>
<div class="m2"><p>ازان پس بر آسودگی بگذریم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو هر دو رسیدند نزدیک آب</p></div>
<div class="m2"><p>به زردی دو رخساره چون آفتاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همی خواست کاید فرود اردشیر</p></div>
<div class="m2"><p>دو مرد جوان دید بر آبگیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جوانان به آواز گفتند زود</p></div>
<div class="m2"><p>عنان و رکیبت بباید بسود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که رستی ز کام و دم اژدها</p></div>
<div class="m2"><p>کنون آب خوردن نیارد بها</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نباید که آیی به خوردن فرود</p></div>
<div class="m2"><p>تن خویش را داد باید درود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو از پندگوی آن شنید اردشیر</p></div>
<div class="m2"><p>به گلنار گفت این سخن یادگیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رکیبش گران شد سبک شد عنان</p></div>
<div class="m2"><p>به گردن برآورد رخشان سنان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پس‌اندر چو باد دمان اردوان</p></div>
<div class="m2"><p>همی تاخت با رنج و تیره‌روان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدانگه که بگذشت نیمی ز روز</p></div>
<div class="m2"><p>فلک را بپیمود گیتی فروز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یکی شارستان دید با رنگ و بوی</p></div>
<div class="m2"><p>بسی مردم آمد به نزدیک اوی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنین گفت با موبدان نامدار</p></div>
<div class="m2"><p>که کی برگذشت آن دلاور سوار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنین داد پاسخ بدو رهنمای</p></div>
<div class="m2"><p>که ای شاه نیک‌اختر و پاک‌رای</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدانگه که خورشید برگشت زرد</p></div>
<div class="m2"><p>بگسترد شب چادر لاژورد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدین شهر بگذشت پویان دو تن</p></div>
<div class="m2"><p>پر از گرد وبی‌آب گشته دهن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یکی غرم بود از پس یک سوار</p></div>
<div class="m2"><p>که چون او ندیدم به ایوان نگار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چنین گفت با اردوان کدخدای</p></div>
<div class="m2"><p>کز ایدر مگر بازگردی به جای</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سپه سازی و ساز جنگ آوری</p></div>
<div class="m2"><p>که اکنون دگرگونه شد داوری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که بختش پس پشت او برنشست</p></div>
<div class="m2"><p>ازین تاختن باد ماند به دست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یکی نامه بنویس نزد پسر</p></div>
<div class="m2"><p>به نامه بگوی این سخن در به در</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نشانی مگر یابد از اردشیر</p></div>
<div class="m2"><p>نباید که او دو شد از غرم شیر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو بشنید زو اردوان این سخن</p></div>
<div class="m2"><p>بدانست کآواز او شد کهن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بدان شارستان اندر آمد فرود</p></div>
<div class="m2"><p>همی داد نیکی دهش را درود</p></div></div>