---
title: >-
    بخش ۴
---
# بخش ۴

<div class="b" id="bn1"><div class="m1"><p>جز از گوی و میدان نبودیش کار</p></div>
<div class="m2"><p>گهی زخم چوگان و گاهی شکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان بد که یک روز بی‌انجمن</p></div>
<div class="m2"><p>به نخچیرگه رفت با چنگ زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا نام آن رومی آزاده بود</p></div>
<div class="m2"><p>که رنگ رخانش به می داده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پشت هیون چمان برنشست</p></div>
<div class="m2"><p>ابا سرو آزاده چنگی به دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلارام او بود و هم کام اوی</p></div>
<div class="m2"><p>همیشه به لب داشتی نام اوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به روز شکارش هیون خواستی</p></div>
<div class="m2"><p>که پشتش به دیبا بیاراستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فروهشته زو چار بودی رکیب</p></div>
<div class="m2"><p>همی تاختی در فراز و نشیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رکابش دو زرین دو سیمین بدی</p></div>
<div class="m2"><p>همان هر یکی گوهر آگین بدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان زیر ترکش کمان مهره داشت</p></div>
<div class="m2"><p>دلاور ز هر دانشی بهره داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پیش اندر آمدش آهو دو جفت</p></div>
<div class="m2"><p>جوانمرد خندان به آزاده گفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که ای ماه من چون کمان را به زه</p></div>
<div class="m2"><p>برآرم به شست اندر آرم گره</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کدام آهو افگنده خواهی به تیر</p></div>
<div class="m2"><p>که ماده جوانست و همتاش پیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو گفت آزاده کای شیرمرد</p></div>
<div class="m2"><p>به آهو نجویند مردان نبرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو آن ماده را نر گردان به تیر</p></div>
<div class="m2"><p>شود ماده از تیر تو نر پیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازان پس هیون را برانگیز تیز</p></div>
<div class="m2"><p>چو آهو ز چنگ تو گیرد گریز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کمان مهره انداز تا گوش خویش</p></div>
<div class="m2"><p>نهد هم‌چنان خوار بر دوش خویش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هم‌انگه ز مهره بخاردش گوش</p></div>
<div class="m2"><p>بی‌آزار پایش برآرد به دوش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به پیکان سر و پای و گوشش بدوز</p></div>
<div class="m2"><p>چو خواهی که خوانمت گیتی فروز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کمان را به زه کرد بهرام گور</p></div>
<div class="m2"><p>برانگیخت از دشت آرام شور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دو پیکان به ترکش یکی تیر داشت</p></div>
<div class="m2"><p>به دشت اندر از بهر نخچیر داشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هم‌انگه چو آهو شد اندر گریز</p></div>
<div class="m2"><p>سپهبد سروهای آن نره تیز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به تیر دو پیکان ز سر برگرفت</p></div>
<div class="m2"><p>کنیزک بدو ماند اندر شگفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هم‌اندر زمان نر چون ماده گشت</p></div>
<div class="m2"><p>سرش زان سروی سیه ساده گشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همان در سروگاه ماده دو تیر</p></div>
<div class="m2"><p>بزد همچنان مرد نخچیرگیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دو پیکان به جای سرو در سرش</p></div>
<div class="m2"><p>به خون اندرون لعل گشته برش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هیون را سوی جفت دیگر بتاخت</p></div>
<div class="m2"><p>به خم کمان مهره در مهره ساخت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به گوش یکی آهو اندر فکند</p></div>
<div class="m2"><p>پسند آمد و بود جای پسند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بخارید گوش آهو اندر زمان</p></div>
<div class="m2"><p>به تیر اندر آورد جادو کمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سر و گوش و پایش به پیکان بدوخت</p></div>
<div class="m2"><p>بدان آهو آزاده را دل بسوخت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بزد دست بهرام و او را ز زین</p></div>
<div class="m2"><p>نگونسار برزد به روی زمین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هیون از بر ماه‌چهره براند</p></div>
<div class="m2"><p>برو دست و چنگش به خون درفشاند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنین گفت کای بی‌خرد چنگ‌زن</p></div>
<div class="m2"><p>چه بایست جستن به من برشکن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر کند بودی گشاد برم</p></div>
<div class="m2"><p>ازین زخم ننگی شدی گوهرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو او زیر پای هیون در سپرد</p></div>
<div class="m2"><p>به نخچیر زان پس کنیزک نبرد</p></div></div>