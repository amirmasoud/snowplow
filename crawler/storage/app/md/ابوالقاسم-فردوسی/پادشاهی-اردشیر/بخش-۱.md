---
title: >-
    بخش ۱
---
# بخش ۱

<div class="b" id="bn1"><div class="m1"><p>به بغداد بنشست بر تخت عاج</p></div>
<div class="m2"><p>به سر برنهاد آن دلفروز تاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمر بسته و گرز شاهان به دست</p></div>
<div class="m2"><p>بیاراسته جایگاه نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهنشاه خواندند زان پس ورا</p></div>
<div class="m2"><p>ز گشتاسپ نشناختی کس ورا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو تاج بزرگی به سر برنهاد</p></div>
<div class="m2"><p>چنین کرد بر تخت پیروزه یاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که اندر جهان داد گنج منست</p></div>
<div class="m2"><p>جهان زنده از بخت و رنج منست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس این گنج نتواند از من ستد</p></div>
<div class="m2"><p>بد آید به مردم ز کردار بد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خشنود باشد جهاندار پاک</p></div>
<div class="m2"><p>ندارد دریغ از من این تیره خاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان سر به سر در پناه منست</p></div>
<div class="m2"><p>پسندیدن داد راه منست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نباید که از کارداران من</p></div>
<div class="m2"><p>ز سرهنگ و جنگی سواران من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخسپد کسی دل پر از آرزوی</p></div>
<div class="m2"><p>گر از بنده گر مردم نیک‌خوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گشادست بر هرکس این بارگاه</p></div>
<div class="m2"><p>ز بدخواه وز مردم نیک‌خواه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه انجمن خواندند آفرین</p></div>
<div class="m2"><p>که آباد بادا به دادت زمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرستاد بر هر سوی لشکری</p></div>
<div class="m2"><p>که هرجا که باشد ز دشمن سری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر کینه‌ورشان به راه آورید</p></div>
<div class="m2"><p>گر آیین شمشیر و گاه آورید</p></div></div>