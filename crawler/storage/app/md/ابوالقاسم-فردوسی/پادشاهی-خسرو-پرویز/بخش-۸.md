---
title: >-
    بخش ۸
---
# بخش ۸

<div class="b" id="bn1"><div class="m1"><p>وزین روی بنشست بهرام گرد</p></div>
<div class="m2"><p>بزرگان برفتند با او وخرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپهبد بپرسید زان سرکشان</p></div>
<div class="m2"><p>که آمد زخویشان شما را نشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرستید هرکس که دارید خویش</p></div>
<div class="m2"><p>که باشند یکدل به گفتار وکیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریشان بیایند وفرمان کنند</p></div>
<div class="m2"><p>به پیمان روان را گروگان کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپه ماند از بردع واردبیل</p></div>
<div class="m2"><p>از ارمنیه نیز بی‌مرد وخیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازیشان برزم اندرون نیست باک</p></div>
<div class="m2"><p>چه مردان بردع چه یک مشت خاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شنیدند گردنکشان این سخن</p></div>
<div class="m2"><p>که بهرام جنگ آور افگند بن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلشکر گزیدند مردی دبیر</p></div>
<div class="m2"><p>سخن گوی و داننده ویادگیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیامد گوی با دلی پر ز راز</p></div>
<div class="m2"><p>همی‌بود پویان شب دیریاز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفت آنچ بشنید زان مهتران</p></div>
<div class="m2"><p>ازان نامداران وکنداوران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ایرانیان پاسخ ایدون شنید</p></div>
<div class="m2"><p>که تا رزم لشکر نیاید پدید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی مازخسرو نگردیم باز</p></div>
<div class="m2"><p>بترسیم کین کارگردد دراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مباشید ایمن بران رزمگاه</p></div>
<div class="m2"><p>که خسرو شبیخون کند با سپاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو پاسخ شنید آن فرستاده مرد</p></div>
<div class="m2"><p>سوی لشکر پهلوان شد چو گرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه لشکرآتش برافروختند</p></div>
<div class="m2"><p>بهر جای شمعی همی‌سوختند</p></div></div>