---
title: >-
    بخش ۷۵
---
# بخش ۷۵

<div class="b" id="bn1"><div class="m1"><p>همان زاد فرخ بدرگاه بر</p></div>
<div class="m2"><p>همی‌بود و کس را ندادی گذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که آگه شدی زان سخن شهریار</p></div>
<div class="m2"><p>به درگاه بر بود چون پرده دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو پژمرده شد چادر آفتاب</p></div>
<div class="m2"><p>همی‌ساخت هر مهتری جای خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفرمود تا پاسبانان شهر</p></div>
<div class="m2"><p>هر آنکس که از مهتری داشت بهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برفتند یکسر سوی بارگاه</p></div>
<div class="m2"><p>بدان جای شادی و آرام شاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدیشان چنین گفت کامشب خروش</p></div>
<div class="m2"><p>دگرگونه‌تر کرد باید ز دوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه پاسبانان بنام قباد</p></div>
<div class="m2"><p>همی‌کرد باید بهر پاس یاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین داد پاسخ که ای دون کنم</p></div>
<div class="m2"><p>ز سر نام پرویز بیرون کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شب چادر قیرگون کرد نو</p></div>
<div class="m2"><p>ز شهر و ز بازار برخاست غو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه پاسبانان بنام قباد</p></div>
<div class="m2"><p>چو آواز دادند کردند یاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب تیره شاه جهان خفته بود</p></div>
<div class="m2"><p>چو شیرین به بالینش بر جفته بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو آواز آن پاسبانان شنید</p></div>
<div class="m2"><p>غمی گشت و زیشان دلش بردمید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو گفت شاها چه شاید بدن</p></div>
<div class="m2"><p>برین داستانی بباید زدن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آواز او شاه بیدار شد</p></div>
<div class="m2"><p>دلش زان سخن پر ز آزار شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به شیرین چنین گفت کای ماه روی</p></div>
<div class="m2"><p>چه داری بخواب اندرون گفت وگوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدو گفت شیرین که بگشای گوش</p></div>
<div class="m2"><p>خروشیدن پاسبانان نیوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو خسرو بدان گونه آوا شنید</p></div>
<div class="m2"><p>به رخساره شد چون گل شنبلید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنین گفت کز شب گذشته سه پاس</p></div>
<div class="m2"><p>بیابید گفتار اخترشناس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که این بد گهر تا ز مادر بزاد</p></div>
<div class="m2"><p>نهانی و را نام کردم قباد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به آواز شیرویه گفتم همی</p></div>
<div class="m2"><p>دگر نامش اندر نهفتم همی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ورا نام شیروی بد آشکار</p></div>
<div class="m2"><p>قبادش همی‌خواند این پیشکار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شب تیره باید شدن سوی چین</p></div>
<div class="m2"><p>وگر سوی ما چین و مکران زمین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بریشان به افسون بگیریم راه</p></div>
<div class="m2"><p>ز فغفور چینی بخواهم سپاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ازان کاخترش به آسمان تیره بود</p></div>
<div class="m2"><p>سخنهای او بر زمین خیره بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شب تیره افسون نیامد به کار</p></div>
<div class="m2"><p>همی‌آمدش کار دشوار خوار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به شیرین چنین گفت که آمد زمان</p></div>
<div class="m2"><p>بر افسون ما چیره شد بدگمان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدو گفت شیرین که نوشه بدی</p></div>
<div class="m2"><p>همیشه ز تو دور دست بدی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدانش کنون چارهٔ خویش ساز</p></div>
<div class="m2"><p>مبادا که آید به دشمن نیاز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو روشن شود دشمن چاره جوی</p></div>
<div class="m2"><p>نهد بی‌گمان سوی این کاخ روی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هم آنگه زره خواست از گنج شاه</p></div>
<div class="m2"><p>دو شمشیر هندی و رومی کلاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همان ترکش تیرو زرین سپر</p></div>
<div class="m2"><p>یکی بندهٔ گرد و پرخاشخر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شب تیره‌گون اندر آمد به باغ</p></div>
<div class="m2"><p>بدان گه که برخیزد ازخواب زاغ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به باغ بزرگ اندر از بس درخت</p></div>
<div class="m2"><p>نبد شاه را در چمن جای تخت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیاویخت از شاخ زرین سپر</p></div>
<div class="m2"><p>بجایی کزو دور بودی گذر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نشست از برنرگس و زعفران</p></div>
<div class="m2"><p>یکی تیغ در زیر زانو گران</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو خورشید برزد سنان از فراز</p></div>
<div class="m2"><p>سوی کاخ شد دشمن دیو ساز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یکایک بگشتند گرد سرای</p></div>
<div class="m2"><p>تهی بد ز شاه سرافراز جای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به تاراج دادند گنج ورا</p></div>
<div class="m2"><p>نکرد ایچ کس یاد رنج ورا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه باز گشتنددیده پرآب</p></div>
<div class="m2"><p>گرفته ز کار زمانه شتاب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چه جوییم ازین گنبد تیزگرد</p></div>
<div class="m2"><p>که هرگز نیاساید از کارکرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یک را همی تاج شاهی دهد</p></div>
<div class="m2"><p>یکی رابه دریا به ماهی دهد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی را برهنه سر و پای و سفت</p></div>
<div class="m2"><p>نه آرام و خورد و نه جای نهفت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی را دهد نوشه و شهد و شیر</p></div>
<div class="m2"><p>بپوشد به دیبا و خز و حریر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سرانجام هردو به خاک اندرند</p></div>
<div class="m2"><p>به تاریک دام هلاک اندرند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگر خود نزادی خردمند مرد</p></div>
<div class="m2"><p>نبودی ورا روز ننگ و نبرد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ندیدی جهان از بنه به بدی</p></div>
<div class="m2"><p>اگر که بدی مرد اگر مه بدی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کنون رنج در کار خسرو بریم</p></div>
<div class="m2"><p>بخواننده آگاهی نوبریم</p></div></div>