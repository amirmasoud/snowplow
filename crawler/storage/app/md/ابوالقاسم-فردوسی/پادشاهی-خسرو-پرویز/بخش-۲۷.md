---
title: >-
    بخش ۲۷
---
# بخش ۲۷

<div class="b" id="bn1"><div class="m1"><p>وزان پس چو دانست کامد سپاه</p></div>
<div class="m2"><p>جهان شد ز گرد سواران سیاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گزین کرد زان رومیان صدهزار</p></div>
<div class="m2"><p>همه نامدار ازدرکارزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلیح و درم خواست واسپان جنگ</p></div>
<div class="m2"><p>سرآمد برو روزگار درنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی دخترش بود مریم بنام</p></div>
<div class="m2"><p>خردمند و با سنگ و با رای وکام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخسرو فرستاد به آیین دین</p></div>
<div class="m2"><p>همی‌خواست ازکردگار آفرین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپذرفت دخترش گستهم گرد</p></div>
<div class="m2"><p>به آیین نیکو بخسرو سپرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وزان پس بیاورد چندان جهیز</p></div>
<div class="m2"><p>کزان کند شد بارگیهای تیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز زرینه و گوهر شاهوار</p></div>
<div class="m2"><p>ز یاقوت وز جامهٔ زرنگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گستردنیها و دیبای روم</p></div>
<div class="m2"><p>به زر پیکر و از بریشمش بوم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان یاره و طوق با گوشوار</p></div>
<div class="m2"><p>سه تاج گرانمایه گوهرنگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عماری بیاراست زرین چهار</p></div>
<div class="m2"><p>جلیلش پر ازگوهر شاهوا ر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چهل مهد دیگر بد از آبنوس</p></div>
<div class="m2"><p>ز گوهر درفشان چو چشم خروس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ازان پس پرستنده ماه روی</p></div>
<div class="m2"><p>زایوان برفتند با رنگ وبوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خردمند و بیدار پانصد غلام</p></div>
<div class="m2"><p>بیامد بزرین وسیمین ستام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز رومی همان نیز خادم چهل</p></div>
<div class="m2"><p>پری چهره و شهره ودلگسل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وزان فیلسوفان رومی چهار</p></div>
<div class="m2"><p>خردمند و با دانش ونامدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدیشان بگفت آنچ بایست گفت</p></div>
<div class="m2"><p>همان نیز با مریم اندرنهفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آرام وز کام و بایستگی</p></div>
<div class="m2"><p>همان بخشش و خورد و شایستگی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پس از خواسته کرد رومی شمار</p></div>
<div class="m2"><p>فزون بد ز سیصد هزاران هزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرستاد هر کس که بد بردرش</p></div>
<div class="m2"><p>ز گوهر نگار افسری بر سرش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مهان را همان اسپ و دینار داد</p></div>
<div class="m2"><p>ز شایسته هر چیز بسیار داد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنین گفت کای زیردستان شاه</p></div>
<div class="m2"><p>سزد گر بر آرید گردن بماه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز گستهم شایسته‌تر در جهان</p></div>
<div class="m2"><p>نخیزد کسی از میان مهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چوشاپور مهتر کرانجی بود</p></div>
<div class="m2"><p>که اندر سخنها میانجی بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یک راز دارست بالوی نیز</p></div>
<div class="m2"><p>که نفروشد آزادگان را بچیز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چوخراد برزین نبیند کسی</p></div>
<div class="m2"><p>اگر چند ماند بگیتی بسی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بران آفریدش خدای جهان</p></div>
<div class="m2"><p>که تا آشکارا شود زو نهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو خورشید تابنده او بی‌بدیست</p></div>
<div class="m2"><p>همه کار و کردار او ایزدیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همه یاد کرد این به نامه درون</p></div>
<div class="m2"><p>برفتند با دانش و رهنمون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ستاره شمر پیش با رهنمای</p></div>
<div class="m2"><p>که تارفتنش کی به آید ز جای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به جنبید قیصر به بهرام روز</p></div>
<div class="m2"><p>به نیک اختر و فال گیتی فروز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دو منزل همی‌رفت قیصر به راه</p></div>
<div class="m2"><p>سدیگر بیامد به پیش سیاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به فرمود تا مریم آمد به پیش</p></div>
<div class="m2"><p>سخن گفت با او ز اندازه بیش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدو گفت دامن ز ایرانیان</p></div>
<div class="m2"><p>نگه دار و مگشای بند ازمیان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برهنه نباید که خسرو تو را</p></div>
<div class="m2"><p>ببیند که کاری رسد نو تو را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگفت این و بدرود کردش به مهر</p></div>
<div class="m2"><p>که یار تو بادا برفتن سپهر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نیا طوس جنگی برادرش بود</p></div>
<div class="m2"><p>بدان جنگ سالار لشکرش بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدو گفت مریم به خون خویش تست</p></div>
<div class="m2"><p>بران برنهادم که هم کیش تست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سپردم تو را دختر وخواسته</p></div>
<div class="m2"><p>سپاهی برین گونه آراسته</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نیاطوس یکسر پذیرفت از وی</p></div>
<div class="m2"><p>بگفت و گریان بپیچید روی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همی‌رفت لشکر به راه وریغ</p></div>
<div class="m2"><p>نیا طوس در پیش با گرز وتیغ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو بشنید خسروکه آمد سپاه</p></div>
<div class="m2"><p>ازان شارستان برد لشکر به راه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو آمد پدیدار گرد سران</p></div>
<div class="m2"><p>درفش سواران جوشن وران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همی‌رفت لشکر بکردار گرد</p></div>
<div class="m2"><p>سواران بیدار و مردان مرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دل خسرو از لشکر نامدار</p></div>
<div class="m2"><p>بخندید چون گل بوقت بهار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دل روشن راد راتیز کرد</p></div>
<div class="m2"><p>مران باره را پاشنه خیز کرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نیاطوس را دید و در برگرفت</p></div>
<div class="m2"><p>بپرسیدن آزادی اندرگرفت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز قیصر که برداشت زانگونه رنج</p></div>
<div class="m2"><p>ابا رنج دیگر تهی کرد گنج</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>وزانجای سوی عماری کشید</p></div>
<div class="m2"><p>بپرده درون روی مریم بدید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بپرسید و بر دست او بوس داد</p></div>
<div class="m2"><p>ز دیدار آن خوب رخ گشت شاد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بیاورد لشکر به پرده سرای</p></div>
<div class="m2"><p>نهفته یکی ماه را ساخت جای</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سخن گفت و بنشست بااوسه روز</p></div>
<div class="m2"><p>چهارم چو بفروخت گیتی فروز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گزیده سرایی بیاراستند</p></div>
<div class="m2"><p>نیاطوس را پیش اوخواستند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ابا سرگس و کوت جنگی بهم</p></div>
<div class="m2"><p>سران سپه را همه بیش و کم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدیشان چنین گفت کاکنون سران</p></div>
<div class="m2"><p>کدامند و مردان جنگاوران</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نیاطوس بگزید هفتاد مرد</p></div>
<div class="m2"><p>که آورد گیرند روز نبرد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که زیر درفشش برفتی هزار</p></div>
<div class="m2"><p>گزیده سواران خنجر گزار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو خسرو بدید آن گزیده سپاه</p></div>
<div class="m2"><p>سواران گردنکش ورزمخواه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همی‌خواند بر کردگار آفرین</p></div>
<div class="m2"><p>که چرخ آفرید و زمان و زمین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همان بر نیاطوس وبر لشکرش</p></div>
<div class="m2"><p>چه برنامور قیصر وکشورش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بدان مهتران گفت اگر کردگار</p></div>
<div class="m2"><p>مرا یارباشد گه کارزار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>توانایی خویش پیداکنم</p></div>
<div class="m2"><p>زمین رابکوکب ثریاکنم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نباشد جزاندیشهٔ دوستان</p></div>
<div class="m2"><p>فلک یارومهر ردان بوستان</p></div></div>