---
title: >-
    بخش ۱
---
# بخش ۱

<div class="b" id="bn1"><div class="m1"><p>چو بگذشت زو شاه شد یزدگرد</p></div>
<div class="m2"><p>به ماه سفندار مذ روز ارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه گفت آن سخنگوی مرد دلیر</p></div>
<div class="m2"><p>چو از گردش روز برگشت سیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که باری نزادی مرا مادرم</p></div>
<div class="m2"><p>نگشتی سپهر بلند از برم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پرگار تنگ و میان دو گوی</p></div>
<div class="m2"><p>چه گویم جز از خامشی نیست روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه روز بزرگی نه روز نیاز</p></div>
<div class="m2"><p>نماند همی برکسی بر دراز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمانه زمانیست چون بنگری</p></div>
<div class="m2"><p>ندارد کسی آلت داوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یارای خوان و به پیمای جام</p></div>
<div class="m2"><p>ز تیمار گیتی مبر هیچ نام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چرخ گردان کشد زین تو</p></div>
<div class="m2"><p>سرانجام خاکست بالین تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلت را به تیمار چندین مبند</p></div>
<div class="m2"><p>بس ایمن مشو بر سپهر بلند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که با پیل و با شیربازی کند</p></div>
<div class="m2"><p>چنان دان که از بی‌نیازی کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو بیجان شوی او بماند دراز</p></div>
<div class="m2"><p>درازست گفتار چندین مناز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو از آفریدون فزونتر نه ای</p></div>
<div class="m2"><p>چو پرویز باتخت و افسر نه ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به ژرفی نگه کن که با یزدگرد</p></div>
<div class="m2"><p>چه کرد این برافراخته هفت گرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو بر خسروی گاه بنشست شاد</p></div>
<div class="m2"><p>کلاه بزرگی به سر برنهاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنین گفت کز دور چرخ روان</p></div>
<div class="m2"><p>منم پاک فرزند نوشین روان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پدر بر پدر پادشاهی مراست</p></div>
<div class="m2"><p>خور و خوشه و برج ماهی مراست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بزرگی دهم هر که کهتر بود</p></div>
<div class="m2"><p>نیازارم آن راکه مهتر بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نجویم بزرگی و فرزانگی</p></div>
<div class="m2"><p>همان رزم و تندی و مردانگی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که برکس نماند همی زور و بخت</p></div>
<div class="m2"><p>نه گنج و نه دیهیم شاهی نه تخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همی نام جاوید باید نه کام</p></div>
<div class="m2"><p>بینداز کام و برافراز نام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برین گونه تا سال شد بر دو هشت</p></div>
<div class="m2"><p>همی ماه و خورشید بر سر گذشت</p></div></div>