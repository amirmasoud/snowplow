---
title: >-
    بخش ۸
---
# بخش ۸

<div class="b" id="bn1"><div class="m1"><p>یکی نامه بنوشت دیگر بطوس</p></div>
<div class="m2"><p>پر از خون دل و روی چون سندروس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخست آفرین کرد بر دادگر</p></div>
<div class="m2"><p>کزو دید نیرو و بخت و هنر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خداوند پیروزی و فرهی</p></div>
<div class="m2"><p>خداوند دیهیم شاهنشهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پی پشه تا پر و چنگ عقاب</p></div>
<div class="m2"><p>به خشکی چو پیل و نهنگ اندر آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز پیمان و فرمان او نگذرد</p></div>
<div class="m2"><p>دم خویش بی رای او نشمرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شاه جهان یزدگرد بزرگ</p></div>
<div class="m2"><p>پدر نامور شهریار سترگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهدار یزدان پیروزگر</p></div>
<div class="m2"><p>نگهبان جنبده و بوم و بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز تخم بزرگان یزدان شناس</p></div>
<div class="m2"><p>که از تاج دارند از اختر سپاس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کزیشان شد آباد روی زمین</p></div>
<div class="m2"><p>فروزندهٔ تاج و تخت و نگین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوی مرزبانان با گنج و گاه</p></div>
<div class="m2"><p>که با فرو برزند و با داد و راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمیران و رویین دژ و رابه کوه</p></div>
<div class="m2"><p>کلات از دگر دست و دیگر گروه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگهبان ما باد پروردگار</p></div>
<div class="m2"><p>شما بی‌گزند از بد روزگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مبادا گزند سپهر بلند</p></div>
<div class="m2"><p>مه پیکار آهرمن پرگزند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همانا شنیدند گردنکشان</p></div>
<div class="m2"><p>خنیده شد اندر جهان این نشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که بر کارزای و مرد نژاد</p></div>
<div class="m2"><p>دل ما پر آزرم و مهرست و داد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به ویژه نژاد شما را که رنج</p></div>
<div class="m2"><p>فزونست نزدیک شاهان ز گنج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بهرام چوبینه آمد پدید</p></div>
<div class="m2"><p>ز فرمان دیهیم ما سرکشید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شما را دل از شهرهای فراخ</p></div>
<div class="m2"><p>به پیچید وز باغ و میدان و کاخ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برین باستان راع و کوه بلند</p></div>
<div class="m2"><p>کده ساختید از نهیب گزند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر ای دون که نیرو دهد کردگار</p></div>
<div class="m2"><p>به کام دل ما شود روزگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز پاداش نیکی فزایش کنیم</p></div>
<div class="m2"><p>برین پیش دستی نیایش کنیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همانا که آمد شما را خبر</p></div>
<div class="m2"><p>که ما را چه آمد ز اختر به سر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ازین مارخوار اهرمن چهرگان</p></div>
<div class="m2"><p>ز دانایی و شرم بی بهرگان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه گنج و نه نام و نه تخت و نژاد</p></div>
<div class="m2"><p>همی‌داد خواهند گیتی بباد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بسی گنج و گوهر پراگنده شد</p></div>
<div class="m2"><p>بسی سر به خاک اندر آگنده شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنین گشت پرگار چرخ بلند</p></div>
<div class="m2"><p>که آید بدین پادشاهی گزند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ازین زاغ ساران بی‌آب و رنگ</p></div>
<div class="m2"><p>نه هوش و نه دانش نه نام و نه ننگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که نوشین روان دیده بود این به خواب</p></div>
<div class="m2"><p>کزین تخت به پراگند رنگ و آب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنان دید کز تازیان صد هزار</p></div>
<div class="m2"><p>هیونان مست و گسسته مهار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گذر یافتندی به  اروند رود</p></div>
<div class="m2"><p>نماندی برین بوم بر تار و پود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به ایران و بابل نه کشت و درود</p></div>
<div class="m2"><p>به چرخ زحل برشدی تیره دود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هم آتش به مردی به آتشکده</p></div>
<div class="m2"><p>شدی تیره نوروز و جشن سده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از ایوان شاه جهان کنگره</p></div>
<div class="m2"><p>فتادی به میدان او یکسره</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کنون خواب راپاسخ آمد پدید</p></div>
<div class="m2"><p>ز ما بخت گردن بخواهد کشید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شود خوار هرکس که هست ارجمند</p></div>
<div class="m2"><p>فرومایه را بخت گردد بلند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پراگنده گردد بدی در جهان</p></div>
<div class="m2"><p>گزند آشکارا و خوبی نهان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بهر کشوری در ستمگاره‌ای</p></div>
<div class="m2"><p>پدید آید و زشت پتیاره‌ای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نشان شب تیره آمد پدید</p></div>
<div class="m2"><p>همی روشنایی بخواهد پرید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کنون ما به دستوری رهنمای</p></div>
<div class="m2"><p>همه پهلوانان پاکیزه رای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به سوی خراسان نهادیم روی</p></div>
<div class="m2"><p>بر مرزبانان دیهیم جوی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ببینیم تا گردش روزگار</p></div>
<div class="m2"><p>چه گوید بدین رای نا استوار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پس اکنون ز بهر کنارنگ طوس</p></div>
<div class="m2"><p>بدین سو کشیدیم پیلان وکوس</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فرخ زاد با ما ز یک پوستست</p></div>
<div class="m2"><p>به پیوستگی نیز هم دوستست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بالتونیه‌ست او کنون رزمجوی</p></div>
<div class="m2"><p>سوی جنگ دشمن نهادست روی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کنون کشمگان پور آن رزمخواه</p></div>
<div class="m2"><p>بر ما بیامد بدین بارگاه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بگفت آنچ آمد ز شایستگی</p></div>
<div class="m2"><p>هم ازبندگی هم ز بایستگی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شیندیم زین مرزها هرچ گفت</p></div>
<div class="m2"><p>بلندی و پستی و غار و نهفت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دژ گنبدین کوه تا خرمنه</p></div>
<div class="m2"><p>دگر لاژوردین ز بهر بنه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز هر گونه بنمود آن دل گسل</p></div>
<div class="m2"><p>ز خوبی نمود آنچ بودش به دل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وزین جایگه شد بهر جای کس</p></div>
<div class="m2"><p>پژوهنده شد کارها پیش وپس</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چنین لشکری گشن ما را که هست</p></div>
<div class="m2"><p>برین تنگ دژها نشاید نشست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نشستیم و گفتنیم با رای زن</p></div>
<div class="m2"><p>همه پهلوانان شدند انجمن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز هر گونه گفتیم و انداختیم</p></div>
<div class="m2"><p>سر انجام یکسر برین ساختیم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که از تاج و ز تخت و مهر و نگین</p></div>
<div class="m2"><p>همان جامهٔ روم و کشمیر و چین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز پر مایه چیزی که آمد بدست</p></div>
<div class="m2"><p>ز روم و ز طایف همه هرچ هست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همان هرچه از ماپراگند نیست</p></div>
<div class="m2"><p>گر از پوشش است ار ز افگند نیست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز زرینه و جامهٔ نابرید</p></div>
<div class="m2"><p>ز چیزی که آن رانشاید کشید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هم از خوردنیها ز هر گونه ساز</p></div>
<div class="m2"><p>که ما را بیاید برو بر نیاز</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز گاوان گردون کشان چل هزار</p></div>
<div class="m2"><p>که رنج آورد تا که آید به کار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به خروار زان پس ده و دو هزار</p></div>
<div class="m2"><p>به خوشه درون گندم آرد ببار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>همان ارزن و پسته و ناردان</p></div>
<div class="m2"><p>بیارد یکی موبدی کاردان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شتروار زین هریکی ده هزار</p></div>
<div class="m2"><p>هیونان بختی بیارند بار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همان گاو گردون هزار از نمک</p></div>
<div class="m2"><p>بیارند تا بر چه گردد فلک</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز خرما هزار و ز شکر هزار</p></div>
<div class="m2"><p>بود سخته و راست کرده شمار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ده و دو هزار انگبین کندره</p></div>
<div class="m2"><p>بدژها کشند آن همه یکسره</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نمک خورده سرپوست چون چل هزار</p></div>
<div class="m2"><p>بیارند آن راکه آید به کار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شتروار سیصد ز زربفت شاه</p></div>
<div class="m2"><p>بیارند بر بارها تا دو ماه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بیاید یکی موبدی با گروه</p></div>
<div class="m2"><p>ز گاه شمیران و از را به کوه</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به دیدار پیران و فرهنگیان</p></div>
<div class="m2"><p>بزرگان که‌اند از کنارنگیان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به دو روز نامه به دژها نهند</p></div>
<div class="m2"><p>یکی نامه گنجور ما را دهند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دگر خود بدارند با خویشتن</p></div>
<div class="m2"><p>بزرگان که باشند زان انجمن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>همانا بران راغ و کوه بلند</p></div>
<div class="m2"><p>ز ترک و ز تازی نیاید گزند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>شما را بدین روزگار سترگ</p></div>
<div class="m2"><p>یکی دست باشد بر ما بزرگ</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هنرمند گوینده دستور ما</p></div>
<div class="m2"><p>بفرماید اکنون به گنج‌ور ما</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>که هرکس این را ندارد به رنج</p></div>
<div class="m2"><p>فرستد ورا پارسی جامه پنج</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>یکی خوب سربند پیکر به زر</p></div>
<div class="m2"><p>بیابند فرجام زین کار بر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بدین روزگار تباه و دژم</p></div>
<div class="m2"><p>بیابد ز گنجور ما چل درم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به سنگ کسی کو بود زیردست</p></div>
<div class="m2"><p>یکی زین درمها گر اید بدست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>از آن شست بر سر شش و چاردانگ</p></div>
<div class="m2"><p>بیارد نبشته بخواند به بانگ</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بیک روی برنام یزدان پاک</p></div>
<div class="m2"><p>کزویست امید و زو ترس وباک</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>دگر پیکرش افسر و چهر ما</p></div>
<div class="m2"><p>زمین بارور گشته از مهر ما</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به نوروز و مهر آن هم آراستست</p></div>
<div class="m2"><p>دو جشن بزرگست و با خواستست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>درود جهان بر کم آزار مرد</p></div>
<div class="m2"><p>کسی کو ز دیهیم ما یاد کرد</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بلند اختری نامجویی سوار</p></div>
<div class="m2"><p>بیامد به کف نامهٔ شهریار</p></div></div>