---
title: >-
    بخش ۷
---
# بخش ۷

<div class="b" id="bn1"><div class="m1"><p>دبیر جهاندیده راپیش خواند</p></div>
<div class="m2"><p>دل آگنده بودش همه برفشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهاندار چون کرد آهنگ مرو</p></div>
<div class="m2"><p>به ماهوی سوری کنارنگ مرو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی نامه بنوشت با درد و خشم</p></div>
<div class="m2"><p>پر از آرزو دل پر از آب چشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخست آفرین کرد بر کردگار</p></div>
<div class="m2"><p>خداوند دانا و پروردگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خداوند گردنده بهرام وهور</p></div>
<div class="m2"><p>خداوند پیل و خداوند مور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کند چون بخواهد ز ناچیز چیز</p></div>
<div class="m2"><p>که آموزگارش نباید به نیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفت آنک ما را چه آمد بروی</p></div>
<div class="m2"><p>وزین پادشاهی بشد رنگ و بوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز رستم کجا کشته شد روز جنگ</p></div>
<div class="m2"><p>ز تیمار بر ما جهان گشت تنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدست یکی سعد وقاص نام</p></div>
<div class="m2"><p>نه بوم و نژاد و نه دانش نه کام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنون تا در طیسفون لشکرست</p></div>
<div class="m2"><p>همین زاغ پیسه به پیش اندرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو با لشکرت رزم را سازکن</p></div>
<div class="m2"><p>سپه را برین برهم آواز کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من اینک پس نامه برسان باد</p></div>
<div class="m2"><p>بیایم به نزد تو ای پاک وراد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرستادهٔ دیگر از انجمن</p></div>
<div class="m2"><p>گزین کرد بینا دل و رای زن</p></div></div>