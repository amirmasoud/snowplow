---
title: >-
    بخش ۲
---
# بخش ۲

<div class="b" id="bn1"><div class="m1"><p>عمر سعد وقاس را با سپاه</p></div>
<div class="m2"><p>فرستاد تا جنگ جوید ز شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آگاه شد زان سخن یزگرد</p></div>
<div class="m2"><p>ز هر سو سپاه اندر آورد گرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفرمود تا پور هرمزد، راه</p></div>
<div class="m2"><p>به پیماید و بر کشد با سپاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که رستم بدش نام و بیدار بود</p></div>
<div class="m2"><p>خردمند و گرد و جهاندار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستاره شمر بود و بسیار هوش</p></div>
<div class="m2"><p>به گفتارش موبد نهاده دو گوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برفت و گرانمایگان راببرد</p></div>
<div class="m2"><p>هر آنکس که بودند بیدار و گرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برین گونه تا ماه بگذشت سی</p></div>
<div class="m2"><p>همی رزم جستند در قادسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسی کشته شد لشکر از هر دو سوی</p></div>
<div class="m2"><p>سپه یک ز دیگر نه برگاشت روی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدانست رستم شمار سپهر</p></div>
<div class="m2"><p>ستاره شمر بود و با داد و مهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همی‌گفت کاین رزم را روی نیست</p></div>
<div class="m2"><p>ره آب شاهان بدین جوی نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیاورد صلاب و اختر گرفت</p></div>
<div class="m2"><p>ز روز بلا دست بر سر گرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی نامه سوی برادر به درد</p></div>
<div class="m2"><p>نوشت و سخنها همه یاد کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نخست آفرین کرد بر کردگار</p></div>
<div class="m2"><p>کزو دید نیک و بد روزگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دگر گفت کز گردش آسمان</p></div>
<div class="m2"><p>پژوهنده مردم شود بدگمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گنهکارتر در زمانه منم</p></div>
<div class="m2"><p>ازی را گرفتار آهرمنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که این خانه از پادشاهی تهیست</p></div>
<div class="m2"><p>نه هنگام پیروزی و فرهیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز چارم همی‌بنگرد آفتاب</p></div>
<div class="m2"><p>کزین جنگ ما را بد آید شتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز بهرام و زهره‌ست ما را گزند</p></div>
<div class="m2"><p>نشاید گذشتن ز چرخ بلند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان تیر و کیوان برابر شدست</p></div>
<div class="m2"><p>عطارد به برج دو پیکر شدست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنین است و کاری بزرگست پیش</p></div>
<div class="m2"><p>همی سیر گردد دل از جان خویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه بودنیها ببینم همی</p></div>
<div class="m2"><p>وزان خامشی برگزینم همی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر ایرانیان زار و گریان شدم</p></div>
<div class="m2"><p>ز ساسانیان نیز بریان شدم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دریغ این سر و تاج و این داد و تخت</p></div>
<div class="m2"><p>دریغ این بزرگی و این فر و بخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کزین پس شکست آید از تازیان</p></div>
<div class="m2"><p>ستاره نگردد مگر بر زیان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برین سالیان چار صد بگذرد</p></div>
<div class="m2"><p>کزین تخمه گیتی کسی نشمرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ازیشان فرستاده آمد به من</p></div>
<div class="m2"><p>سخن رفت هر گونه بر انجمن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که از قادسی تا لب جویبار</p></div>
<div class="m2"><p>زمین را ببخشیم با شهریار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وزان سو یکی برگشاییم راه</p></div>
<div class="m2"><p>به شهری کجاهست بازارگاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدان تا خریم و فروشیم چیز</p></div>
<div class="m2"><p>ازین پس فزونی نجوییم نیز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پذیریم ما ساو و باژ گران</p></div>
<div class="m2"><p>نجوییم دیهیمِ کُنداوران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شهنشاه رانیز فرمان بریم</p></div>
<div class="m2"><p>گر از ما بخواهد گروگان بریم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنین است گفتار و کردار نیست</p></div>
<div class="m2"><p>جز از گردش کژ پرگار نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برین نیز جنگی بود هر زمان</p></div>
<div class="m2"><p>که کشته شود صد هژبر دمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بزرگان که بامن به جنگ اندرند</p></div>
<div class="m2"><p>به گفتار ایشان همی‌ننگرند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو میروی طبری و چون ارمنی</p></div>
<div class="m2"><p>به جنگ‌اند با کیش آهرمنی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو کلبوی سوری و این مهتران</p></div>
<div class="m2"><p>که گوپال دارند و گرز گران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همی سر فرازند که ایشان کیند</p></div>
<div class="m2"><p>به ایران و مازنداران برچیند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگرمرز و راهست اگر نیک و بد</p></div>
<div class="m2"><p>به گرز و به شمشیر باید ستد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بکوشیم و مردی به کار آوریم</p></div>
<div class="m2"><p>به ریشان جهان تنگ و تار آوریم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نداند کسی راز گردان سپهر</p></div>
<div class="m2"><p>دگر گونه‌تر گشت برما به مهر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو نامه بخوانی خرد را مران</p></div>
<div class="m2"><p>بپرداز و بر ساز با مهتران</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همه گردکن خواسته هرچ هست</p></div>
<div class="m2"><p>پرستنده و جامهٔ برنشست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همی تاز تا آذر آبادگان</p></div>
<div class="m2"><p>به جای بزرگان و آزادگان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همی دون گله هرچ داری زاسپ</p></div>
<div class="m2"><p>ببر سوی گنجور آذرگشسپ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز زابلستان گر ز ایران سپاه</p></div>
<div class="m2"><p>هرآنکس که آیند زنهار خواه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدار و به پوش و بیارای مهر</p></div>
<div class="m2"><p>نگه کن بدین گردگردان سپهر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ازو شادمانی و زو در نهیب</p></div>
<div class="m2"><p>زمانی فرازست و روزی نشیب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سخن هرچ گفتم به مادر بگوی</p></div>
<div class="m2"><p>نبیند همانا مرانیز روی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>درودش ده ازما و بسیار پند</p></div>
<div class="m2"><p>بدان تا نباشد به گیتی نژند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گراز من بد آگاهی آرد کسی</p></div>
<div class="m2"><p>مباش اندرین کار غمگین بسی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چنان دان که اندر سرای سپنج</p></div>
<div class="m2"><p>کسی کو نهد گنج با دست رنج</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چوگاه آیدش زین جهان بگذرد</p></div>
<div class="m2"><p>از آن رنج او دیگری برخورد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همیشه به یزدان پرستان گرای</p></div>
<div class="m2"><p>بپرداز دل زین سپنجی سرای</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که آمد به تنگ اندرون روزگار</p></div>
<div class="m2"><p>نبیند مرا زین سپس شهریار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو با هر که از دودهٔ ما بود</p></div>
<div class="m2"><p>اگر پیر اگر مرد برنا بود</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همه پیش یزدان نیایش کنید</p></div>
<div class="m2"><p>شب تیره او را ستایش کنید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بکوشید و بخشنده باشید نیز</p></div>
<div class="m2"><p>ز خوردن به فردا ممانید چیز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>که من با سپاهی به سختی درم</p></div>
<div class="m2"><p>به رنج و غم و شوربختی درم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>رهایی نیابم سرانجام ازین</p></div>
<div class="m2"><p>خوشا باد نوشین ایران زمین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو گیتی شود تنگ بر شهریار</p></div>
<div class="m2"><p>تو گنج و تن و جان گرامی مدار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کزین تخمهٔ نامدار ارجمند</p></div>
<div class="m2"><p>نماندست جز شهریار بلند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز کوشش مکن هیچ سستی به کار</p></div>
<div class="m2"><p>به گیتی جزو نیستمان یادگار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ز ساسانیان یادگار اوست بس</p></div>
<div class="m2"><p>کزین پس نبینند زین تخمهٔ کس</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دریغ این سر و تاج و این مهر و داد</p></div>
<div class="m2"><p>که خواهدشد این تخت شاهی بباد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تو پدرود باش و بی‌آزار باش</p></div>
<div class="m2"><p>ز بهر تن شه به تیمار باش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گراو رابد آید تو شو پیش اوی</p></div>
<div class="m2"><p>به شمشیر بسپار پرخاشجوی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو با تخت منبر برابر کنند</p></div>
<div class="m2"><p>همه نام بوبکر و عمر کنند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تبه گردد این رنجهای دراز</p></div>
<div class="m2"><p>نشیبی درازست پیش فراز</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نه تخت و نه دیهیم بینی نه شهر</p></div>
<div class="m2"><p>ز اختر همه تازیان راست بهر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو روز اندر آید به روز دراز</p></div>
<div class="m2"><p>شود ناسزا شاه گردن فراز</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بپوشد ازیشان گروهی سیاه</p></div>
<div class="m2"><p>ز دیبا نهند از بر سر کلاه</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نه تخت ونه تاج و نه زرینه کفش</p></div>
<div class="m2"><p>نه گوهر نه افسر نه بر سر درفش</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به رنج یکی دیگری بر خورد</p></div>
<div class="m2"><p>به داد و به بخشش همی‌ننگرد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شب آید یکی چشمه رخشان کند</p></div>
<div class="m2"><p>نهفته کسی را خروشان کند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ستانندهٔ روزشان دیگرست</p></div>
<div class="m2"><p>کمر بر میان و کله بر سرست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ز پیمان بگردند وز راستی</p></div>
<div class="m2"><p>گرامی شود کژی و کاستی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>پیاده شود مردم جنگجوی</p></div>
<div class="m2"><p>سوار آنک لاف آرد و گفت وگوی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>کشاورز جنگی شود بی‌هنر</p></div>
<div class="m2"><p>نژاد و هنر کمتر آید ببر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>رباید همی این ازآن آن ازین</p></div>
<div class="m2"><p>ز نفرین ندانند باز آفرین</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نهان بدتر از آشکارا شود</p></div>
<div class="m2"><p>دل شاهشان سنگ خارا شود</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بداندیش گردد پدر بر پسر</p></div>
<div class="m2"><p>پسر بر پدر هم چنین چاره‌گر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>شود بندهٔ بی‌هنر شهریار</p></div>
<div class="m2"><p>نژاد و بزرگی نیاید به کار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به گیتی کسی رانماند وفا</p></div>
<div class="m2"><p>روان و زبانها شود پر جفا</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>از ایران وز ترک وز تازیان</p></div>
<div class="m2"><p>نژادی پدید آید اندر میان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نه دهقان نه ترک و نه تازی بود</p></div>
<div class="m2"><p>سخنها به کردار بازی بود</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>همه گنجها زیر دامن نهند</p></div>
<div class="m2"><p>بمیرند و کوشش به دشمن دهند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بود دانشومند و زاهد به نام</p></div>
<div class="m2"><p>بکوشد ازین تا که آید به کام</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چنان فاش گردد غم و رنج و شور</p></div>
<div class="m2"><p>که شادی به هنگام بهرام گور</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نه جشن ونه رامش نه کوشش نه کام</p></div>
<div class="m2"><p>همه چارهٔ ورزش و ساز دام</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>پدر با پسر کین سیم آورد</p></div>
<div class="m2"><p>خورش کشک و پوشش گلیم آورد</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>زیان کسان از پی سود خویش</p></div>
<div class="m2"><p>بجویند و دین اندر آرند پیش</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>نباشد بهار و زمستان پدید</p></div>
<div class="m2"><p>نیارند هنگام رامش نبید</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چو بسیار ازین داستان بگذرد</p></div>
<div class="m2"><p>کسی سوی آزادگی ننگرد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بریزند خون ازپی خواسته</p></div>
<div class="m2"><p>شود روزگار مهان کاسته</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>دل من پر از خون شد و روی زرد</p></div>
<div class="m2"><p>دهن خشک و لبها شده لاژورد</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>که تامن شدم پهلوان از میان</p></div>
<div class="m2"><p>چنین تیره شد بخت ساسانیان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چنین بی‌وفا گشت گردان سپهر</p></div>
<div class="m2"><p>دژم گشت و ز ما ببرید مهر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>مرا تیز پیکان آهن گذار</p></div>
<div class="m2"><p>همی بر برهنه نیاید به کار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>همان تیغ کز گردن پیل و شیر</p></div>
<div class="m2"><p>نگشتی به آورد زان زخم سیر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>نبرد همی پوست بر تازیان</p></div>
<div class="m2"><p>ز دانش زیان آمدم بر زیان</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>مرا کاشکی این خرد نیستی</p></div>
<div class="m2"><p>گر اندیشه نیک و بد نیستی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بزرگان که در قادسی بامنند</p></div>
<div class="m2"><p>درشتند و بر تازیان دشمنند</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>گمانند کاین بیش بیرون شود</p></div>
<div class="m2"><p>ز دشمن زمین رود جیحون شود</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ز راز سپهری کس آگاه نیست</p></div>
<div class="m2"><p>ندانند کاین رنج کوتاه نیست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چو برتخمه‌ای بگذرد روزگار</p></div>
<div class="m2"><p>چه سود آید از رنج و ز کارزار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>تو را ای برادر تن آباد باد</p></div>
<div class="m2"><p>دل شاه ایران به تو شاد باد</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>که این قادسی گورگاه منست</p></div>
<div class="m2"><p>کفن جوشن و خون کلاه منست</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چنین است راز سپهر بلند</p></div>
<div class="m2"><p>تو دل را به درد من اندر مبند</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>دو دیده زشاه جهان برمدار</p></div>
<div class="m2"><p>فدی کن تن خویش در کارزار</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>که زود آید این روز آهرمنی</p></div>
<div class="m2"><p>چو گردون گردان کند دشمنی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چو نامه به مهر اندر آورد گفت</p></div>
<div class="m2"><p>که پوینده با آفرین باد جفت</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>که این نامه نزد برادر برد</p></div>
<div class="m2"><p>بگوید جزین هرچ اندر خورد</p></div></div>