---
title: >-
    مدح اثیرالدین امین‌الملک زین‌الدوله ابومنصور نصر بن علی
---
# مدح اثیرالدین امین‌الملک زین‌الدوله ابومنصور نصر بن علی

<div class="b" id="bn1"><div class="m1"><p>نگار من چو بر سیمین میان زرین کمر بندد</p></div>
<div class="m2"><p>هر آن کاو را ببیند کی دل اندر سیم و زر بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طمع باید برید از جان شیرین چون من آن کس را</p></div>
<div class="m2"><p>که بیهوده دل اندر عشق آن شیرین پسر بندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی از مشک زلف او حمایل در گل آویزد</p></div>
<div class="m2"><p>گهی از قیر جعد او سلاسل بر قمر بندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عشق او جهان من شود چون حلقهٔ خاتم</p></div>
<div class="m2"><p>چو زلف او ز عنبر حلقه اندر یکدگر بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو تیر و چون کمان گردد دهن باز و خمیده قد</p></div>
<div class="m2"><p>چون آن مشکین زره عمدا بر آن سیمین سپر بندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود چون شمع زرین روی و ریزان اشک و سوزان دل</p></div>
<div class="m2"><p>هر آن کاو دل در آن شمع بتان کاشغر بندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آنم چون گل و نرگس سلب چاک و سرافگنده</p></div>
<div class="m2"><p>که او بر سوسن تازه همی شمشادتر بندد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدان زنجیر مشکین و عقیق شکرین همچون</p></div>
<div class="m2"><p>دل من صدهزاران دل به روزی بیشتر بندد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی خونم بدان زلف دوتاه پر شکن ریز</p></div>
<div class="m2"><p>گهی خوابم بدان چشم سیاه دل شکر بندد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شود چون شکر از آب و چو مشک از آتش آن کاو دل</p></div>
<div class="m2"><p>در آن زنجیر پر مشک و عقیق پر شکر بندد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گه از سنبل حجابی بر فراز پرنیان پوشد</p></div>
<div class="m2"><p>گه از عنبر نقابی بر طراز شوشتر بندد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز شوق روی او آید ز گِل هر ساله پیدا گل</p></div>
<div class="m2"><p>چو بیند روی او از شرم او بار سفر بندد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به چشم مردمان گردد چو سیم قلب خوار آن کس</p></div>
<div class="m2"><p>کامید اندر وصال آن نگار سیمبر بندد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عزیز آن کس بود نزدیک خاص و عام کاو خاطر</p></div>
<div class="m2"><p>چو من پیوسته در مدح عمید نامور بندد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اثیر دین امین ملک زین دولت آن صدری</p></div>
<div class="m2"><p>که بر درگاه او دولت میان هر روز در بندد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ابومنصور نصر بن علی کز رایش ار یابد</p></div>
<div class="m2"><p>اجازت آسمان پش وی از جوزا کمر بندد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو مه زانگشت پیغمبر حجر بشکافد از بیمش</p></div>
<div class="m2"><p>اگر دور از تو گاه خشم چشم اندر حجر بندد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جهانی با کمال است و نباشد عقل آن کامل</p></div>
<div class="m2"><p>که همت با وجودش در جهان مختصر بندد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در ارحام از برای کثرت اتباع او دایم</p></div>
<div class="m2"><p>همی از نطفهٔ ماء معین ایزد صور بندد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سمند او چو آرد حمله فرق فرقدان شاید</p></div>
<div class="m2"><p>کمند او چو گردد حلقه حلق شیر نر بندد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خیال هیبتش در دست شمشیر اجل گیرد</p></div>
<div class="m2"><p>همای همتش بر پای منشور ظفر بندد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فلک امید بست اندر دوام عمر او چونان</p></div>
<div class="m2"><p>که حربا وهم در شمس و صدف دل در مطر بندد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شود آتش بر ایشان چون بر ابراهیم بر ریحان</p></div>
<div class="m2"><p>اگر گاه کرم همت در اصحاب سقر بندد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به دین اندر همی از علم ترتیب علی سازد</p></div>
<div class="m2"><p>به ملک اندر همی از عدل آیین عمر بندد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر هر مهتر از بهر هوای نفس جان و دل</p></div>
<div class="m2"><p>در آلات ملاهی و در انواع بطر بندد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خرد وی را بر آن دارد همه کاندیشه و همت</p></div>
<div class="m2"><p>در اسباب معانی و در ارباب هنر بندد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به پای عزم پیوسته همی فرق قضا کوبد</p></div>
<div class="m2"><p>به دست عزم همواره همی پای قدر بندد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>الا ای نامور صدری که توقیعات کلک تو</p></div>
<div class="m2"><p>تفاخر را همی روح الامین بر فرق سر بندد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وگر این مرتبت وی را شود حاصل ز رشک او</p></div>
<div class="m2"><p>نقاب شرم دست آسمان بر روی خور بندد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بود بر هیأت زرین عماری دار سال و مه</p></div>
<div class="m2"><p>به طمع آن که مرکب دارت او را بر ستر بندد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر آن شاعر که یک ره مدح تو گوید شود کاره</p></div>
<div class="m2"><p>کز آن پس خاطر اندر مدح سادات بشر بندد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو گردون بسیط آمد که را رای زمین خیزد</p></div>
<div class="m2"><p>چو دریای محیط آمد که را دل در شمر بندد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر گوری ستایش را دهان پیش تو بگشاید</p></div>
<div class="m2"><p>وگر موری پرستش را میان پیش تو در بندد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی از حشمت تو شرزه شیران را زبون گیرد</p></div>
<div class="m2"><p>یکی در دولت گرزه ماران را زفر بندد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نگردد از فنا معزول جاویدان حواس آن</p></div>
<div class="m2"><p>که از بهر مدیح تو حواس اندر فکر بندد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زمانه خامهٔ مدحت صلف را در بنان گیرد</p></div>
<div class="m2"><p>ستاره نامهٔ فتحت شرف را بر بصر بندد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به جود ار چند مشهور است حاتم هر که جودت را</p></div>
<div class="m2"><p>ببیند زو عجب آید که فهم اندر سمر بندد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به حلم ار چند مذکور است احنف هر که حکمت را</p></div>
<div class="m2"><p>بداند زو غریب آید که وهم اندر خبر بندد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شود باز سپید او را به تأیید تو خدمتگر</p></div>
<div class="m2"><p>اگر تیهو مثال عالیت بر بال و پر بندد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ایا در نظم مدحت بسته طبع من چنان فکرت</p></div>
<div class="m2"><p>که عابد دایم اندیشه در اوقات سحر بندد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گه اصناف بدایع را در الفاظ ظرف دارد</p></div>
<div class="m2"><p>گه اوصاف روایع را بر ابیات عزر بندد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کنون پرداخت مدحی چون عروسی ساخته کاو را</p></div>
<div class="m2"><p>به گردن بر مشاطه عقدهای پر گهر بندد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بر آن منوال کاستاد مقدم لامعی گوید</p></div>
<div class="m2"><p>ز تیره شب همی پرده به روی روز بر بندد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>الا تا ابر بارنده ز در و لعل و پیروزه</p></div>
<div class="m2"><p>قلاید در مه آزار بر شاخ شجر بندد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>محل تو چنان بادا که هر بنده که او کهتر</p></div>
<div class="m2"><p>کمر در پیش از پیروزه و لعل و درر بندد</p></div></div>