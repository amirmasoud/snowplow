---
title: >-
    تهنیت فتح عراق و مدح سلطان سنجر
---
# تهنیت فتح عراق و مدح سلطان سنجر

<div class="b" id="bn1"><div class="m1"><p>این اشارتها که ظاهر شد ز لطف کردگار</p></div>
<div class="m2"><p>وین بشارتها که صادر شد به فتح شهریار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یافت خواهد ملت از اندازهٔ آن دستگاه</p></div>
<div class="m2"><p>گشت خواهد دولت از آوازهٔ آن پایدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه سلطان را فراوان فتحها حاصل شده‌ست</p></div>
<div class="m2"><p>کز حصول آن خلایق را فزوده‌ست اعتبار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نامهٔ فتحت که خواهد ماند زآن اندر جهان</p></div>
<div class="m2"><p>صدهزاران قصه از شهنامه خوش‌تر یادگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به باطل سر برآوردند قومی در عراق</p></div>
<div class="m2"><p>شد فریضه دفعشان بر پادشاه حق‌گزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور برای قمع ایشان رایت منصور او</p></div>
<div class="m2"><p>در زمستان از خراسان کرد تحویل اختیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لشکری بودند چون عفریت و خوک و غول و خرس</p></div>
<div class="m2"><p>تیره‌رای و خیره‌روی و عمرکاه و غمزکار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر به سر غافل ز تقدیر خدای مستعان</p></div>
<div class="m2"><p>یک به یک غره به اقبال جهان مستعار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از شجاعت بوده با شیر ژیان اندر قران</p></div>
<div class="m2"><p>وز ضلالت بوده با دیو سپید اندر قطار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مدت سالی همی‌کردند در عالم طواف</p></div>
<div class="m2"><p>تا به یک ره مجتمع گشتند مردی صدهزار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود شور انگیختن پیوسته ایشان را عمل</p></div>
<div class="m2"><p>بود رنگ آمیختن همواره ایشان مستعار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که را دریافتندی از وضیع و از شریف</p></div>
<div class="m2"><p>سر بریدندی به تیغ و تن کشیدندی به دار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گه غریبان را ز بی‌رحمی همی‌کردند بند</p></div>
<div class="m2"><p>گه اسیران را ز نامردی همی کشتند زار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گه مسلمان را همی‌خواندند کافر بر ملا</p></div>
<div class="m2"><p>گه موحد را همی‌گفتند ملحد آشکار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر چه از بیداد و غارتشان به شرق و غرب بود</p></div>
<div class="m2"><p>در ممالک اضطراب و در مسالک اضطرار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاه عالم زآن قبل تا خون نباید ریختن</p></div>
<div class="m2"><p>کرد ایشان را ز هر نوعی نصحیت چند بار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون نصیحت رد شد و یزدان چنان تقدیر کرد</p></div>
<div class="m2"><p>کاعتقاد بد برآرد عاقبت زیشان دمار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لشکر منصور ناگاهی بر آنان کوفتند</p></div>
<div class="m2"><p>چون شهاب دیوسوز و چون سحاب تندبار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون شدند آمیخته بر یکدگر هر دو سپاه</p></div>
<div class="m2"><p>جنگ را چنگ آخته چون شیر شرزه در شکار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شد هوا از پارهای گرد تاری چون دخان</p></div>
<div class="m2"><p>شد زمین از قطره‌های خون جاری چون شرار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خیل سلطان را کرامت با سلامت متصل</p></div>
<div class="m2"><p>اهل عصیان را عزیمت بر هزیمت استوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از هزاهز چون رخ معلول قرص آفتاب</p></div>
<div class="m2"><p>وز زلازل چون تن مفلوج جرم کوهسار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر زمین زرنیخ‌رنگ از روی بدخواهان نبات</p></div>
<div class="m2"><p>بر هوا شنگرف‌گون از خون گمراهان بخار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اسب تازان باد شکل و گرد گردان ابر وصف</p></div>
<div class="m2"><p>تیغ رخشان برق سان و کوس نالان رعدوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گاه پیچش هر کمند و وقت کوشش هر سمند</p></div>
<div class="m2"><p>اژدهای بی‌قرار و آسمان بامدار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لعلگون پشت زمین و نیلگون روی هوا</p></div>
<div class="m2"><p>این ز الماس حسام و آن ز انقاس غبار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون دل عشاق و جان عاشقان از مرد و گرز</p></div>
<div class="m2"><p>مرکز اشباح تنگ و مقصد ارواح تار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>موضعی با زینت ذات‌البروج از تیغ و درع</p></div>
<div class="m2"><p>موقفی با هیبت یوم‌الخروج از گیر و دار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گاوپیچان در زمین از نعل اسب شیر روز</p></div>
<div class="m2"><p>شیر بی‌جان بر سپهر از بیم گرز گاوسار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پشت مرد از درع میناگون چو روی آسمان</p></div>
<div class="m2"><p>روی تیغ از قطره‌های خون چو پشت سوسمار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گه چو گردون از تغیر گشته هامون با شتاب</p></div>
<div class="m2"><p>گه چو هامون از تغیر گشته گردون باوقار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وز فراوان خون غداران و مکاران که رفت</p></div>
<div class="m2"><p>در طرفهای جبال و در کنفهای بحار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا ابد بیجاده‌رنگ و لعلگون خواهند زاد</p></div>
<div class="m2"><p>زین یکی در یتیم و زان یکی زر عیار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ایستاده پیش صف سلطان و زیر ران او</p></div>
<div class="m2"><p>بارهٔ گردون‌تن هامون‌کن جیحون‌گذار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ماه سیری ماهی‌اندامی که کردی هر زمان</p></div>
<div class="m2"><p>پشت ماهی را نعال نو به ماه نونگار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>غار گشتی گر در او رفتی ز شخص وی چو کوه</p></div>
<div class="m2"><p>کوه گشتی گر بر او جستی ز نعل وی چو غار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون فلک در دور و از گردش فلک را رخ سیاه</p></div>
<div class="m2"><p>چون سمک در آب و از گامش سمک را تن فکار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرکبی چون دلدل آورده بر این سان زیر زین</p></div>
<div class="m2"><p>وز نیام آهخته شمشیری به سان ذوالفقار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا بدان گاهی که زخم تیغ او تسلیم کرد</p></div>
<div class="m2"><p>جان اعدا را به دست مالک دارالبوار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر چه آن لشکر ز غداری و بسیاری بدند</p></div>
<div class="m2"><p>همچو ماران بی‌وفا و همچو موران بی‌شمار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در هزیمت گر توانستی از ایشان هر یکی</p></div>
<div class="m2"><p>پر بر آوردی چو مور و پوست بفگندی چو مار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر چه اعدا را همه انواع شوکت جمع بود</p></div>
<div class="m2"><p>از ستور و از ستام و از سلاح و از سوار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون قضا از چار جانبشان گرفت اندر میان</p></div>
<div class="m2"><p>گاه حاجتشان نیامد سودمند آن هر چهار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ور چه سلطان داشت هر آلت که باید ساخته</p></div>
<div class="m2"><p>از سپاه بی‌نهایت وز مصاف بی‌کنار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شر ایشان را کفایت کرد بی هیچ آلتی</p></div>
<div class="m2"><p>بر او با بندگان و سر او با کردگار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر اجازت یافتندی زو ز بهر تهنیت</p></div>
<div class="m2"><p>چون میسر کرد فتح او خدای بردبار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آمدی شمس‌الضحی پش وی از ذات‌الحنک</p></div>
<div class="m2"><p>وآمدی روح‌الامین نزد وی از دارالقرار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ای هوای رزمگاهت چون زمین هاویه</p></div>
<div class="m2"><p>وی زمین بزمگاهت چون هوای نوبهار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خصم را زنهار دادن در جهان آیین توست</p></div>
<div class="m2"><p>زین قبل دارد همی یزدان تو را در زینهار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کردی از آزردن خصمان مجهول احتراز</p></div>
<div class="m2"><p>گر چه بود آزار تو مقصودشان از کارزار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گر چه گه گه پشه دل مشغول دارد پیل را</p></div>
<div class="m2"><p>پیل دارد گاه جنگ از انتقام پشه عار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ای به خاک پای تو شاهان عالم را یمین</p></div>
<div class="m2"><p>وی ز جود دست تو اعقاب آدم را یسار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دین و دنیا را ز فر رای و فتح رایتت</p></div>
<div class="m2"><p>یمن حاضر بر یمین و یسر حاصل بر یسار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>باز با تیهو ز عدلت خفته در یک آشیان</p></div>
<div class="m2"><p>شیر با آهو ز امنت رفته در یک مرغزار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بس که بگرفتی بلاد و بس که بشکستی مصاف</p></div>
<div class="m2"><p>بس که بربستی عدو و بس که بگشادی حصار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>این به فضل ذوالجلال و آن به حسن اعتقاد</p></div>
<div class="m2"><p>این به سعد آسمان و آن به سعی روزگار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شکر کن یزدان عالم را که یک نعمت نماند</p></div>
<div class="m2"><p>کاو نکرد آن گاه قسمت در ازل بر تو نثار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>وز جهان نگذشت هرگز بر همایون خاطرت</p></div>
<div class="m2"><p>هیچ کامی کآن تو را حاصل نشد بی‌انتظار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>لاجرم حال کسی باشد چنین کاو را بود</p></div>
<div class="m2"><p>سیرت محمود جفت و دولت مسعود یار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا به ترکیب مزاج و جوهر و خلقت بود</p></div>
<div class="m2"><p>تند باد و رام خاک و پاک آب و تیز نار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>باد اعدای تو را چون نار و آب و باد و خاک</p></div>
<div class="m2"><p>روی زرد و قدر پست و عزم سست و نقش خار</p></div></div>