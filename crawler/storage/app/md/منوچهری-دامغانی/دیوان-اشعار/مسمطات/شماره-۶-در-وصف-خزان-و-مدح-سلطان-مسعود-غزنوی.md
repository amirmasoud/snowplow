---
title: >-
    شمارهٔ  ۶ - در وصف خزان و مدح سلطان مسعود غزنوی
---
# شمارهٔ  ۶ - در وصف خزان و مدح سلطان مسعود غزنوی

<div class="b" id="bn1"><div class="m1"><p>باز دگر باره مهر ماه در آمد</p></div>
<div class="m2"><p>جشن فریدون آبتین به بر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر خوش دختران رز به سر آمد</p></div>
<div class="m2"><p>کشتنیان را سیاستی دگر آمد</p></div></div>
<div class="b2" id="bn3"><p>دهقان در بوستان همی سحر آمد</p>
<p>تا ببرد جانشان به ناخن و چنگال</p></div>
<div class="b" id="bn4"><div class="m1"><p>دخترکان سیاه زنگی‌زاده</p></div>
<div class="m2"><p>پیش وضیع و شریف روی گشاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مادرشان هیچگون به دایه نداده</p></div>
<div class="m2"><p>وز در گهواره‌شان به در ننهاده</p></div></div>
<div class="b2" id="bn6"><p>بر سر گهواره‌شان به روی فتاده</p>
<p>مروحهٔ سبز در دو دست همهٔ سال</p></div>
<div class="b" id="bn7"><div class="m1"><p>دخترکان بیست بیست خفته به هر سو</p></div>
<div class="m2"><p>پهلو بنهاده بیست بیست به پهلو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گیسو در بسته بیست بیست به گیسو</p></div>
<div class="m2"><p>گیسوشان سبز و گیسو از سر زانو</p></div></div>
<div class="b2" id="bn9"><p>هر یکی از ساعدین مادر و بازو</p>
<p>خویشتن آویخته به اکحل و قیفال</p></div>
<div class="b" id="bn10"><div class="m1"><p>شیر دهدشان به پای، مادر آژیر</p></div>
<div class="m2"><p>کودک دیدی کجا به پای خورد شیر؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مادرشان سرسپید و جمله شده پیر</p></div>
<div class="m2"><p>و ایشان پستان او گرفته به زنجیر</p></div></div>
<div class="b2" id="bn12"><p>دهقان روزی ز در درآید شبگیر</p>
<p>گوید: کان دختران گربز محتال</p></div>
<div class="b" id="bn13"><div class="m1"><p>مادرتان پیر گشت و پشت به خم کرد</p></div>
<div class="m2"><p>موی سر او سپید گشت و رخش زرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا کی ازین گنده‌پیر، شیر توان خورد</p></div>
<div class="m2"><p>سرد بود لامحاله هر چه بود سرد</p></div></div>
<div class="b2" id="bn15"><p>من نه مسلمانم و نه مرد جوانمرد</p>
<p>گر سرتان نگسلم زدوش به کوپال</p></div>
<div class="b" id="bn16"><div class="m1"><p>آنگه رزبانش را بخواند دهقان</p></div>
<div class="m2"><p>دو پسر خویش را، دو پسر رزبان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر یک داسی بیاورند یتیمان</p></div>
<div class="m2"><p>برده به آتش درون و کرده به سوهان</p></div></div>
<div class="b2" id="bn18"><p>حنجره و حلقشان ببرند ایشان</p>
<p>نادره باشد گلو بریدن اطفال!</p></div>
<div class="b" id="bn19"><div class="m1"><p>نادره‌تر آنکه طفلکان نخروشند</p></div>
<div class="m2"><p>خون ز گلو بر نیاورند و نجوشند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وان کشندگان سختکوش بکوشند</p></div>
<div class="m2"><p>پس به کواره فرو نهند و بپوشند</p></div></div>
<div class="b2" id="bn21"><p>در طمع آنکه کشته را بفروشند</p>
<p>اینت عجایب حدیث و اینت عجب حال</p></div>
<div class="b" id="bn22"><div class="m1"><p>آنگه آرند کشته را به کواره</p></div>
<div class="m2"><p>بر سر بازارکان نهند به زاره</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آید بر کشتگان هزار نظاره</p></div>
<div class="m2"><p>پره کشند و بایستند کناره</p></div></div>
<div class="b2" id="bn24"><p>نه به قصاصش کنند خلق اشاره</p>
<p>نه به دیت پادشاه خواهد ازو مال</p></div>
<div class="b" id="bn25"><div class="m1"><p>بلکه بخرند کشته را ز کشنده</p></div>
<div class="m2"><p>گه به درشتی و گه به خواهش و خنده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای عجبی تا بوند ایشان زنده</p></div>
<div class="m2"><p>نایدشان مشتری تمام و بسنده</p></div></div>
<div class="b2" id="bn27"><p>راست چو کشته شوند و زار فکنده</p>
<p>آیدشان مشتری و آید دلال</p></div>
<div class="b" id="bn28"><div class="m1"><p>زود بخرندشان ز حال نگشته</p></div>
<div class="m2"><p>هرگز که خریده بود دختر کشته!</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کشته و برکشته چند روز گذشته</p></div>
<div class="m2"><p>در کفنی هیچ کشته را ننبشته</p></div></div>
<div class="b2" id="bn30"><p>روز دگر آنگهی به ناوه و پشته</p>
<p>در بن چرخشتشان بمالد حمال</p></div>
<div class="b" id="bn31"><div class="m1"><p>باز لگد کوبشان کنند همیدون</p></div>
<div class="m2"><p>پوست کنند از تن یکایک بیرون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به سرشان برنهند و پشت و ستیخون</p></div>
<div class="m2"><p>سخت گران سنگی از هزار من افزون</p></div></div>
<div class="b2" id="bn33"><p>تا برود قطره قطره از تنشان خون</p>
<p>پس فکند خونشان به خم در قتال</p></div>
<div class="b" id="bn34"><div class="m1"><p>چون به خم اندر ز خشم او بخروشد</p></div>
<div class="m2"><p>تیر زند بی‌کمان و سخت بکوشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرد سر خمش استوار بپوشد</p></div>
<div class="m2"><p>تا بچگان از میان خم بنجوشد</p></div></div>
<div class="b2" id="bn36"><p>آید هر ساعتی و پس بنیوشد</p>
<p>تا شنود هیچ قیل و تا شنود قال</p></div>
<div class="b" id="bn37"><div class="m1"><p>چون بنشیند زمی معنبر جوشه</p></div>
<div class="m2"><p>گوید کایدون نماند جای به نوشه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در فکند سرخ مل به رطل دو گوشه</p></div>
<div class="m2"><p>روشن گردد چهار گوشهٔ گوشه</p></div></div>
<div class="b2" id="bn39"><p>گوید کاین می مرا نگردد نوشه</p>
<p>تا نخورم یاد شهریار عدومال</p></div>
<div class="b" id="bn40"><div class="m1"><p>بار خدای جهان خلیفهٔ معبود</p></div>
<div class="m2"><p>نیکو مولود و نیک طالع مولود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گویی محمود بود بیش ز مسعود؟</p></div>
<div class="m2"><p>نی‌نی مسعود هست بیش ز محمود</p></div></div>
<div class="b2" id="bn42"><p>همچو سلیمان که بیش بود ز داوود</p>
<p>بیشتر از زال بود رستم بن زال</p></div>
<div class="b" id="bn43"><div class="m1"><p>باش! که آن پادشه هنوز جوانست</p></div>
<div class="m2"><p>نیمرسیده یکی هزبر دمانست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>این رمهٔ گوسفند سخت کلانست</p></div>
<div class="m2"><p>یک تنه تنها بدین حظیره شبانست</p></div></div>
<div class="b2" id="bn45"><p>گرگ بر اطراف این حظیره روانست</p>
<p>گرگ بود بر لب حظیره علی حال</p></div>
<div class="b" id="bn46"><div class="m1"><p>گرگ یکایک توان گرفت، شبان را</p></div>
<div class="m2"><p>صبر همی‌باید این فلان و فلان را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هر که همی‌خواهد از نخست جهان را</p></div>
<div class="m2"><p>دل بنهد کارهای صعب و گران را</p></div></div>
<div class="b2" id="bn48"><p>هر که بجنباند این درخت کلان را</p>
<p>از بر او مرغکان زنند پر و بال</p></div>
<div class="b" id="bn49"><div class="m1"><p>عاقبت کار نیک باید فردا</p></div>
<div class="m2"><p>عاقبت کار، نیک باشد حقا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>روی نهاده‌ست کار شاه به بالا</p></div>
<div class="m2"><p>دیدهٔ ما روشنست و کار هویدا</p></div></div>
<div class="b2" id="bn51"><p>ایزد کرده‌ست وعده با ملک ما</p>
<p>کش برساند به هر مراد دل امسال</p></div>
<div class="b" id="bn52"><div class="m1"><p>مملکت خانیان همه بستاند</p></div>
<div class="m2"><p>بر در ما چین خلیفتی بنشاند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مرز خراسان به مرز روم رساند</p></div>
<div class="m2"><p>لشگر شرق ار عراق در گذراند</p></div></div>
<div class="b2" id="bn54"><p>باز ندارد عنان و باز نماند</p>
<p>تا نزند در یمن سناجق اقبال</p></div>
<div class="b" id="bn55"><div class="m1"><p>زود شود چون بهشت گیتی ویران</p></div>
<div class="m2"><p>بگذرد این روزگار سختی از ایران</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>روی به رامش نهد امیر امیران</p></div>
<div class="m2"><p>شاد و بدو شاد این خجسته وزیران</p></div></div>
<div class="b2" id="bn57"><p>دست به می شاه را و دل به هژیران</p>
<p>دیده به روی نکو و گوش به قوال</p></div>
<div class="b" id="bn58"><div class="m1"><p>ای ملک! ایزد جهان برای تو کرده‌ست</p></div>
<div class="m2"><p>ما همه را از پی هوای تو کرده‌ست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر چه بکرد ای ملک سزای تو کرده‌ست</p></div>
<div class="m2"><p>نیکوکاری که او به جای تو کرده‌ست</p></div></div>
<div class="b2" id="bn60"><p>عالم خاک کف دو پای تو کرده‌ست</p>
<p>عز و جل ایزد مهیمن متعال</p></div>
<div class="b" id="bn61"><div class="m1"><p>هر چه تو اندیشه کردی ای ملک از پیش</p></div>
<div class="m2"><p>آنهمه ایزد ترا بداد و از آن بیش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هر چه بخواهی کنون بخواه و میندیش</p></div>
<div class="m2"><p>کت برساند به کام و آرزوی خویش</p></div></div>
<div class="b2" id="bn63"><p>ای ملک این ملک را تو دانی معنیش</p>
<p>ملک بگیر و سر خوارج بفتال</p></div>
<div class="b" id="bn64"><div class="m1"><p>بنشین در بزم بر سریر به ایوان</p></div>
<div class="m2"><p>خرگه برتر زن از سرادق کیوان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در کن ز آهنگ رزم خصم زمیدان</p></div>
<div class="m2"><p>درگذر این تیر دلشکاف ز سندان</p></div></div>
<div class="b2" id="bn66"><p>از دل گردان برآر زهره به پیکان</p>
<p>در سر مردم بکوب مغز، به کوپال</p></div>
<div class="b" id="bn67"><div class="m1"><p>سال هزاران هزار شاد همی‌باش</p></div>
<div class="m2"><p>یاد همی دارمان و یاد همی‌باش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>با دهش دست و دین و داد همی‌باش</p></div>
<div class="m2"><p>میر همی‌باش و میرزاد همی‌باش</p></div></div>
<div class="b2" id="bn69"><p>جمله برین رسم و این نهاد همی‌باش</p>
<p>قدر تو هر روز و روزگار تو چون فال</p></div>