---
title: >-
    شمارهٔ ۳۸ - در مدح سلطان مسعود غزنوی
---
# شمارهٔ ۳۸ - در مدح سلطان مسعود غزنوی

<div class="b" id="bn1"><div class="m1"><p>ای خداوند خراسان و شهنشاه عراق</p></div>
<div class="m2"><p>ای به مردی و به شاهی برده از شاهان سباق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سپاهت را سپاهان رایتت را ری مکان</p></div>
<div class="m2"><p>ای ز ایران تا به توران بندگانت را وثاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای جهان را تازه کرده رسم و آیین پدر</p></div>
<div class="m2"><p>ای برون آورده ماه مملکت را از محاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ملک مسعود بن محمود کاحرار زمان</p></div>
<div class="m2"><p>بر خداوندی و شاهی تو دارند اتفاق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم بدان رو کاشتقاق فعل از فاعل بود</p></div>
<div class="m2"><p>چرخ و سعد از کنیت و نام تو گیرند اشتقاق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از همه شاهان چنین لشکر که آورد و که برد</p></div>
<div class="m2"><p>از عراق اندر خراسان وز خراسان در عراق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچنان باز از خراسان آمدی بر پشت پیل</p></div>
<div class="m2"><p>کاحمد مرسل به سوی جنت آمد بر براق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای فراق تو دل ما بندگان را سوخته</p></div>
<div class="m2"><p>صدهزاران شکر یزدان را که رستیم از فراق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین جهانداران و شاهان و خداوندان ملک</p></div>
<div class="m2"><p>هر که نبود بندهٔ تو بی‌ریا و بی‌نفاق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر یکی را مال، گردد بی ربا دادن، حرام</p></div>
<div class="m2"><p>هر یکی را زن، شود بی‌هیچ گفتاری، طلاق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آسمان نیلگون، زیرش زمین بی‌سکون</p></div>
<div class="m2"><p>گر نیاید پیش اندر عهد و پیمان و وثاق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آفتابش گردد از گرز گرانت منکسف</p></div>
<div class="m2"><p>اخترانش یابد از شمشیر تیزت احتراق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدسگالت گر برآرد از گریبان سر برون</p></div>
<div class="m2"><p>چون کمند تو، گریبانش فروگیرد خناق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای خداوندی که نصرت گرد لشکرگاه تست</p></div>
<div class="m2"><p>چترت ایوانست و پیلت منظر و فحلت رواق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا سفرهای تو دیدند و هنرهای تو خلق</p></div>
<div class="m2"><p>برنهادند از تعجب قصهٔ شاهان به طاق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روزگار شادی آمد، مطربان باید کنون</p></div>
<div class="m2"><p>گاه ناز و گاه راز و گاه بوس و گه عناق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا بیاید آسمان را تیرگی و روشنی</p></div>
<div class="m2"><p>تا بباشد اختران را اجتماع و احتراق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاد باش و می ستان از ریدکان و ساقیان</p></div>
<div class="m2"><p>ساقیان سیم ساعد، ریدکان سیم ساق</p></div></div>