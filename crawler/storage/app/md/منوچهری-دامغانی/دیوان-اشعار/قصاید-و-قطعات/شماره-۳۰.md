---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>با رخت ای دلبر عیار یار</p></div>
<div class="m2"><p>نیست مرا نیز به گل کار کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا رخ گلنار تو رخشنده گشت</p></div>
<div class="m2"><p>بر دل من ریخته گلنار نار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم تو خونخواره و هر جادویی</p></div>
<div class="m2"><p>مانده از آن چشمک خونخوار خوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنده وفادار و هواخواه تست</p></div>
<div class="m2"><p>بنده هواخواه و وفادار دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داد کن ای کودک و بردار جور</p></div>
<div class="m2"><p>منبر پیش آور و بردار دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای تو دل‌آزار و من آزرده‌دل</p></div>
<div class="m2"><p>دل شده ز آزار دل آزار، زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردل من باز ببخشی به من</p></div>
<div class="m2"><p>جور مکن لشکر تیمار مار</p></div></div>