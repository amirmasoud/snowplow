---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>گرفتمت که رسیدی بدانچه می‌طلبی</p></div>
<div class="m2"><p>گرفتمت که شدی آنچنان که می‌بایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه هر چه یافت کمال از پیش بود نقصان</p></div>
<div class="m2"><p>نه هر چه داد، ستد باز چرخ مینایی؟!</p></div></div>