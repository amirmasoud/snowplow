---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>دوستان! وقت عصیرست و کباب</p></div>
<div class="m2"><p>راه را گرد نشانده‌ست سحاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی رز باید رفتن به صبوح</p></div>
<div class="m2"><p>خویشتن کردن مستان و خراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیمجوشیده عصیر از سر خم</p></div>
<div class="m2"><p>درکشیدن، که چنینست صواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رادمردان را هنگام عصیر</p></div>
<div class="m2"><p>شاید ار می‌نبود صافی و ناب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دو سه روز درین سایهٔ رز</p></div>
<div class="m2"><p>آب انگور گساریم به آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بفروزیم همی آتش رز</p></div>
<div class="m2"><p>گسترانیم بر او سرخ کباب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاک رز باشدمان شاسپرم</p></div>
<div class="m2"><p>برگ رز باشد دستار شراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقل ما خوشهٔ انگور بود</p></div>
<div class="m2"><p>از بر سر بر چون پرعقاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بانگ جوشیدن می باشدمان</p></div>
<div class="m2"><p>نالهٔ بر بط و طنبور و رباب</p></div></div>