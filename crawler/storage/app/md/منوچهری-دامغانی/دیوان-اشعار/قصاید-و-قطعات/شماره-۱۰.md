---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>الا وقت صبوحست، نه گرمست و نه سردست</p></div>
<div class="m2"><p>نه ابرست و نه خورشید، نه بادست و نه گردست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیار ای بت کشمیر، شراب کهن پیر</p></div>
<div class="m2"><p>بده پر و تهی گیر که مان ننگ و نبردست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن باده که زردست و نزارست ولیکن</p></div>
<div class="m2"><p>نه از عشق نزارست و نه از محنت زردست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جان اندر قوتست و به مغز اندر مشکست</p></div>
<div class="m2"><p>به چشم اندرنورست و به روی اندر، وردست</p></div></div>