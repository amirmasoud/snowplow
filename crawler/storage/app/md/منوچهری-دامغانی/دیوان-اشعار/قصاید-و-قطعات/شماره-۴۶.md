---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>ای دل چو هست حاصل کار جهان عدم</p></div>
<div class="m2"><p>بر دل منه ز بهر جهان هیچ بار غم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افکنده همچو سفره مباش از برای نان</p></div>
<div class="m2"><p>همچون تنور گرم مشو از پی شکم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو مست خواب غفلتی و از برای تو</p></div>
<div class="m2"><p>ایزد فکنده خوان کرم در سپیده دم</p></div></div>