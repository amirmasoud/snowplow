---
title: >-
    شمارهٔ ۷۷ - در مدح سلطان مسعود غزنوی
---
# شمارهٔ ۷۷ - در مدح سلطان مسعود غزنوی

<div class="b" id="bn1"><div class="m1"><p>ای ترک من امروز نگویی به کجایی</p></div>
<div class="m2"><p>تا کس نفرستیم و نخوانیم نیایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکس که نباید بر ما زودتر آید</p></div>
<div class="m2"><p>تو دیرتر آیی به بر ما که ببایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن روز که من شیفته‌تر باشم برتو</p></div>
<div class="m2"><p>عذری بنهی بر خود و نازی بفزایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون با دگری من بگشایم، تو ببندی</p></div>
<div class="m2"><p>ور با دگری هیچ ببندم، بگشایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویی: به رخ کس منگر جز به رخ من</p></div>
<div class="m2"><p>ای ترک چنین شیفتهٔ خویش چرایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترسی که کسی نیز دل من برباید</p></div>
<div class="m2"><p>کس دل نرباید به ستم، چون تو ربایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من در دگران زان نگرم تا به حقیقت</p></div>
<div class="m2"><p>قدر تو بدانم که ز خوبی به چه جایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چند بدین سعتریان درنگرم من</p></div>
<div class="m2"><p>حقا که به چشمم ز همه خوبتر آیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با تو ندهد دل که جفایی کنم از پیش</p></div>
<div class="m2"><p>هر چند به خدمت در، تقصیر نمایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور زانکه به خدمت نکنی بهتر ازین جهد</p></div>
<div class="m2"><p>هر چند مرایی، به حقیقت نه مرایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌خدمت و بی‌جهد به نزد ملک شرق</p></div>
<div class="m2"><p>کس را نبود مرتبت و کامروایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه ملکان پیشرو بارخدایان</p></div>
<div class="m2"><p>ز ایزد ملکی یافته و بارخدایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مسعود ملک آنکه نبوده‌ست و نباشد</p></div>
<div class="m2"><p>از مملکتش تا ابدالدهر جدایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این مملکت خسرو تایید سمائیست</p></div>
<div class="m2"><p>باطل نشود هرگز تایید سمائی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ایزد همه آفاق بدو داد و به حق داد</p></div>
<div class="m2"><p>ناحق نبود، آنچه بود کار خدایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پاکیزه دلست این ملک شرق و ملک را</p></div>
<div class="m2"><p>پاکیزه دلی باید و پاکیزه دهایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با هر که وفا کرد وفا را به سرآورد</p></div>
<div class="m2"><p>بس شهره بود در ملکان نیک وفایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر نامه کند شاه سوی قیصر رومی،</p></div>
<div class="m2"><p>ور پیک فرستد سوی فغفور ختایی،</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از طاعت او حلقه کند قیصر درگوش</p></div>
<div class="m2"><p>وز خدمت فغفور کند پشت دوتایی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هرگز به کجا روی نهاد این شه عادل</p></div>
<div class="m2"><p>با حاشیهٔ خویش و غلامان سرایی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>الا که به کام دل او کرد همه کار</p></div>
<div class="m2"><p>این گنبد پیروزه و گردون رحایی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون قصد به ری کرد و به قزوین و به ساوه</p></div>
<div class="m2"><p>شد بوی و بها از همه بویی و بهایی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون قصد کیا کرد به گرگان و به آمل</p></div>
<div class="m2"><p>بگذاشت کیا مملکت خویش و کیایی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کس کرد به کدیه، سپهی خواست ز گیلان</p></div>
<div class="m2"><p>هرگز به جهان‌میر که دیده‌ست و گدایی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کار مدد و کار کیا نابنوا شد</p></div>
<div class="m2"><p>زین نیز بتر باشدشان نابنوایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>امروز کیا بوسه دهد بر لب دریا</p></div>
<div class="m2"><p>کز دست شهنشاه بدو یافت رهایی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سالار سپاهان چو ملک شد به سپاهان</p></div>
<div class="m2"><p>برشد به هوا همچو یکی مرغ هوایی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر چه به هوا برشد چون مرغ همیدون</p></div>
<div class="m2"><p>ور چه به زمین درشد چون مردم مائی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فرزند به درگاه فرستاد و همی‌داد</p></div>
<div class="m2"><p>بر بندگی خویش بیکباره گوایی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زان روز مرائی شد و گشته ست سبکدل</p></div>
<div class="m2"><p>سالار، سبکدل نشود میرمرائی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای بار خدا و ملک بار خدایان</p></div>
<div class="m2"><p>شاه ملکانی و پناه ضعفایی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در دارفنا، اهل بقا خلق ندیده‌ست</p></div>
<div class="m2"><p>از اهل بقایی تو و در دار فنایی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون ایزد شاید ملک هفت سموات</p></div>
<div class="m2"><p>بر هفت زمین‌بر، ملک و شاه تو شایی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یک نیمه جهان را به جوانی بگشادی</p></div>
<div class="m2"><p>چون پیر شوی نیمهٔ دیگر بگشایی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زنگ همه مشرق به سیاست بزدودی</p></div>
<div class="m2"><p>زنگ همه مغرب به سیاست بزدایی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر شاه که از طاعت تو باز کشد سر</p></div>
<div class="m2"><p>فرق سر او زیر پی پیل بسایی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آنکس که دغایی کند او با ملک ما</p></div>
<div class="m2"><p>زو باز نگردد ملک ما به دغایی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا بوی دهد یاسمن و چینی و سنبل</p></div>
<div class="m2"><p>تا رنگ دهد وسمهٔ رومی و الایی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جاوید بزی بارخدایا به سلامت</p></div>
<div class="m2"><p>با دولت پیوسته و با عمر بقایی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یک دست تو با زلف و دگر دست تو با جام</p></div>
<div class="m2"><p>یک گوش به چنگی و دگر گوش به نایی</p></div></div>