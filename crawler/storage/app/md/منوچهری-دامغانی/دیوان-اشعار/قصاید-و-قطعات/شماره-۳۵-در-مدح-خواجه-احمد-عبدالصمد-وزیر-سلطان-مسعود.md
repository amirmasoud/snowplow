---
title: >-
    شمارهٔ ۳۵ - در مدح خواجه احمد عبدالصمد وزیر سلطان مسعود
---
# شمارهٔ ۳۵ - در مدح خواجه احمد عبدالصمد وزیر سلطان مسعود

<div class="b" id="bn1"><div class="m1"><p>آمدت نوروز و آمد جشن نوروزی فراز</p></div>
<div class="m2"><p>کامگارا! کار گیتی تازه از سر گیر باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لالهٔ خودروی شد چون روی بترویان بدیع</p></div>
<div class="m2"><p>سنبل اندر پیش لاله چون سر زلف دراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاخ گل شطرنج سیمین و عقیقین گشته است</p></div>
<div class="m2"><p>وقت شبگیران به نطع سبزه بر شطرنج باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلبنان در بوستان چون خسروان آراسته</p></div>
<div class="m2"><p>مرغکان چون شاعران در پیش این یازان فراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لالهٔ رازی شکفته پیش برگ یاسمن</p></div>
<div class="m2"><p>چون دهان بسدین در گوش سیمین گفته راز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوستان چون مسجد و شاخ بنفشه در رکوع</p></div>
<div class="m2"><p>فاخته چون مؤذن و آواز او بانگ نماز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وان بنفشه چون عدوی خواجهٔ گیتی نگون</p></div>
<div class="m2"><p>سر به زانو برنهاده رخ به نیل اندوده باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواجه احمد آن رئیس عادل پیروزگر</p></div>
<div class="m2"><p>آن فریدون فر کیخسرو دل رستم براز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر زمان ز افراط عدل او چنان گردد کزو</p></div>
<div class="m2"><p>زعفران گر کاری، آزد بر دو دندان گراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست حرص او به مال و خواسته از بهر جود</p></div>
<div class="m2"><p>حرص چون چونین بود محمود باشد حرص و آز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گاه صرافست و گه بزاز و هرگز کس ندید</p></div>
<div class="m2"><p>رایگان زر صیرفی و رایگان دیبا بزاز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر چنو زر صیرفی بودی و بزازی یکی</p></div>
<div class="m2"><p>دیبه و دینار نه مقراض دیدی و نه گاز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وان قلم اندر بنانش گه معز و گه مذل</p></div>
<div class="m2"><p>دشمنان زو بامذلت، دوستان با اعتزاز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برکشد تار طراز عنبرین از کام خویش</p></div>
<div class="m2"><p>چون برآرد عنکبوت از کام خود تار طراز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قیمت یکتا طرازش از طراز افزون بود</p></div>
<div class="m2"><p>در جهان هرگز شنیدستی طرازی زین طراز؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قامت کوتاه دارد، رفتن شیر دژم</p></div>
<div class="m2"><p>گونهٔ بیمار دارد، قوت کوه طراز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در عیان عنبر فشاند، در نهان لل خورد</p></div>
<div class="m2"><p>عنبرست او را بضاعت، للست او را جهاز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر مدیحی کو به جز تو بر کنیت و برنام اوست</p></div>
<div class="m2"><p>خود نه پیوندش به یکدیگر فراز آید نه ساز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هست با خط تو خط چینیان چون خط برآب</p></div>
<div class="m2"><p>هست با شمشیر تو اقلام شیران خرگواز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا همی دولت بماند، بر سر دولت بمان</p></div>
<div class="m2"><p>تا همی ملکت بپاید بر سر ملکت بناز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گنج نه، گوهر فشان، صهبا کش و دستان شنو</p></div>
<div class="m2"><p>بار ده، قصه ستان توقیع زن، تدبیرساز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روی بین و زلف ژول و خال خار و خط ببوی</p></div>
<div class="m2"><p>کف گشای و دل فروز و جان ربای و سرفراز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جز به گرد گل مگرد و جز به راه مل مپوی</p></div>
<div class="m2"><p>جز به نایی دم مزن، و نرد جز با می مباز</p></div></div>