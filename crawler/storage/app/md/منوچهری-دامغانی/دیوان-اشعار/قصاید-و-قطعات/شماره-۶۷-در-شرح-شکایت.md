---
title: >-
    شمارهٔ ۶۷ - در شرح شکایت
---
# شمارهٔ ۶۷ - در شرح شکایت

<div class="b" id="bn1"><div class="m1"><p>گاه توبه کردن آمد از مدایح و ز هجی</p></div>
<div class="m2"><p>کز هجی بینم زیان و از مدایح سود نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خسیسانرا هجی گویی، بلی باشد مدیح</p></div>
<div class="m2"><p>گر بخیلانرا مدیح آری، بلی باشد هجی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزگاری پیشمان آمد، بدین صنعت همی</p></div>
<div class="m2"><p>هم خزینه، هم قبیله، هم ولایت، هم لوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از میان خانهٔ کعبه فرو آویختند</p></div>
<div class="m2"><p>شعر نیکو را به زرین سلسله پیش عزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امرؤ القیس و لبید و اخطل و اعشی قیس</p></div>
<div class="m2"><p>برطللها نوحه کردندی و بر رسم بلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما همه بر نظم و شعر و قافیه نوحه کنیم</p></div>
<div class="m2"><p>نه بر اطلال و دیار و نه وحوش و نه ظبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بو نواس و بو حداد و بوملیک، ابن البشیر</p></div>
<div class="m2"><p>بو دواد و بن درید و ابن احمر، یافتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه گفته‌ست «آذنتنا» آنکه گفت «الذاهبین»</p></div>
<div class="m2"><p>آنکه گفت «السیف اصدق» آنکه گفت« ابلی الهوی»</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوالعلاء و بوالعباس و بوسلیک و بوالمثل</p></div>
<div class="m2"><p>آنکه از ولوالج آمد آنکه آمد از هری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از حکیمان خراسان، کو شهید و رودکی</p></div>
<div class="m2"><p>بوشکور بلخی و بوالفتح بستی هکذی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گو بیاید و ببینید این شریف ایام را</p></div>
<div class="m2"><p>تا کند هرگز شما را شاعری کردن کری؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روزگاری کان حکیمان و سخنگویان بدند</p></div>
<div class="m2"><p>بود هر یک را به شعر نغز گفتن اشتهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اندرین ایام ما بازار هزلست و فسوس</p></div>
<div class="m2"><p>کار بوبکر ربابی دارد و طنز جحی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کرا شعری بری، یا مدحتی پیش آوری</p></div>
<div class="m2"><p>گوید این یکسر دروغست ابتدا تا انتهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر مدیح و آفرین شاعران بودی دروغ</p></div>
<div class="m2"><p>شعر حسان بن ثابت کی شنیدی مصطفی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر لب و دندان آن شاعر که نامش نابغه</p></div>
<div class="m2"><p>کی دعا کردی رسول هاشمی خیرالوری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاعری عباس کرد و طلحه کرد و حمزه کرد</p></div>
<div class="m2"><p>جعفر و سعد وسعید و سید ام القری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور عطا دادن به شعر شاعران بودی فسوس</p></div>
<div class="m2"><p>احمدبن مرسل ندادی کعب را هدیه ردی</p></div></div>