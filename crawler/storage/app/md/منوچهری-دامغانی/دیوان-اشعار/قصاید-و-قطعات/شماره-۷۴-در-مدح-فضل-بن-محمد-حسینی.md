---
title: >-
    شمارهٔ ۷۴ - در مدح فضل‌بن محمد حسینی
---
# شمارهٔ ۷۴ - در مدح فضل‌بن محمد حسینی

<div class="b" id="bn1"><div class="m1"><p>یکی سخنت بگویم گر از رهی شنوی</p></div>
<div class="m2"><p>یکی رهت بنمایم اگر بدان بروی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبوی بگزین، تا گردی از مکاره دور</p></div>
<div class="m2"><p>برو بدان ره تا جاودانه شاد بوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایا کریم زمانه! علیک عین‌الله</p></div>
<div class="m2"><p>تویی که چشمهٔ خورشید را به نور ضوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی که فاتح مغموم این سپهر بوی</p></div>
<div class="m2"><p>تویی که کاشف مکروه این زمانه شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر ز هیبت تو آتشی برافروزند</p></div>
<div class="m2"><p>برآسمان بر، استارگان شوند شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نیکویی نگری، گر همی به کس نگری</p></div>
<div class="m2"><p>به مردمی گروی گر همی به کس گروی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عذاب دوزخ، آنجا بود کجا تو نیی</p></div>
<div class="m2"><p>ثواب جنت آنجا بود، کجا تو بوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برند آن تو هر کس، تو آن کس نبری</p></div>
<div class="m2"><p>دوند زی تو همه کس، تو زی کسی ندوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر قوام زمانه برآفتاب بود</p></div>
<div class="m2"><p>تو آن زمانه قوامی که آفتاب توی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیاید از تو بخیلی چو از رسول دروغ</p></div>
<div class="m2"><p>دروغ بر تو نگنجد، جو بر خدای دوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخاوت تو و رای بلند و طالع و طبع:</p></div>
<div class="m2"><p>نه منقلب، نه مخالف، نه منکسف، نه غوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وفا و همت و آزادگی و دولت و دین:</p></div>
<div class="m2"><p>نکوی و عالی و محمود و مستوی و قوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بو شعیب و خلیل و چو قیس و عمرو و کمیت</p></div>
<div class="m2"><p>به ذوق و وزن عروض و به نظم و نثر و روی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو ابن رومی شاعر، چو ابن‌مقله دبیر</p></div>
<div class="m2"><p>چو ابن‌معتز نحوی، چو اصمعی لغوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بلا و نعمت و اقبال و مردمی و ثنای</p></div>
<div class="m2"><p>بری و آری و توزی و کاری و دروی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به مردمی تو اندر زمانه مردم نیست</p></div>
<div class="m2"><p>که رای تو به علوست و باب تو علوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز همت و هنر تو شگفت ماندستم</p></div>
<div class="m2"><p>که ایمنی تو بر او و بر آسمان نشوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به مشتریت گمانی برم به همت و طبع</p></div>
<div class="m2"><p>که همچو هور لطیفی و همچو نور قوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به گاه خلعت دادن، به گاه صلهٔ شعر</p></div>
<div class="m2"><p>نه سیم تو ملکی و نه زر تو هروی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مدیح تو متنبی به سر نیارد برد</p></div>
<div class="m2"><p>نه بوتمام و نه اعشی قیس و نه طهوی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بزرگوارا!، نام‌آورا!، خداوندا!</p></div>
<div class="m2"><p>حدیث خواهم کردن به تو یکی نبوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حدیث رقعهٔ توزیع برتو عرضه کنم</p></div>
<div class="m2"><p>چنانکه عرضه کند دین به مانوی منوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هزار سال همیدون بزی به پیروزی</p></div>
<div class="m2"><p>به مردمی و به آزادگی و نیکخوی</p></div></div>