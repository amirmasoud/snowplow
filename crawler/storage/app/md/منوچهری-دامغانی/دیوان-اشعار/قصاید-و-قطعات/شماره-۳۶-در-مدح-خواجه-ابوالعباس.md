---
title: >-
    شمارهٔ ۳۶ - در مدح خواجه ابوالعباس
---
# شمارهٔ ۳۶ - در مدح خواجه ابوالعباس

<div class="b" id="bn1"><div class="m1"><p>بیار ساقی زرین نبید و سیمین کاس</p></div>
<div class="m2"><p>به باده حرمت و قدر بهار را بشناس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبید خور که به نوروز هر که می نخورد</p></div>
<div class="m2"><p>نه از گروه کرامست و نز عداد اناس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگاه کن که به نوروز چون شده‌ست جهان</p></div>
<div class="m2"><p>چو کارنامهٔ مانی در آبگون قرطاس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرو کشید گل سرخ روی‌بند از روی</p></div>
<div class="m2"><p>برآورید گل مشکبوی سر ز تراس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی نثار کند ابر شامگاهی در</p></div>
<div class="m2"><p>همی عبیر کند باد بامدادی آس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درست گویی نخاس گشت باد صبا</p></div>
<div class="m2"><p>درخت گل به مثل چون کنیزک نخاس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خجسته را به جز از خردما ندارد گوش</p></div>
<div class="m2"><p>بنفشه را به جز از کرکما ندارد پاس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزاردستان این مدحت منوچهری</p></div>
<div class="m2"><p>کند روایت در مدح خواجه ابو العباس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بزرگ بار خدایی که ایزد متعال</p></div>
<div class="m2"><p>یگانه کرد به توفیقش از جمیع الناس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه به کردن خیرست مر ورا همت</p></div>
<div class="m2"><p>همه به دادن مالست مر ورا وسواس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هزار بار ز عنبر شهیترست به خلق</p></div>
<div class="m2"><p>هزار بار ز آهن قویترست به باس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو عدل او هست آنجایگه نباشد جور</p></div>
<div class="m2"><p>چو امن او هست آنجایگاه نیست هراس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدای، عز و جل، از تنش بگرداناد</p></div>
<div class="m2"><p>مکاره دو جهان و وساوس خناس</p></div></div>