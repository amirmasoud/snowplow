---
title: >-
    شمارهٔ ۷۰ - در وصف بهار و مدح ابو حرب بختیار محمد
---
# شمارهٔ ۷۰ - در وصف بهار و مدح ابو حرب بختیار محمد

<div class="b" id="bn1"><div class="m1"><p>نوروز، روزگار مجدد کند همی</p></div>
<div class="m2"><p>وز باغ خویش باغ ارم رد کند همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرگس میان باغ تو گویی درمز نیست</p></div>
<div class="m2"><p>اوراق عشرهای مجلد کند همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در لاله‌زار، لالهٔ نعمان سرخ روی</p></div>
<div class="m2"><p>خالی ز مشک و غالیه بر خد کند همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان نسترن چو ناف بلورین دلبری</p></div>
<div class="m2"><p>کوناف را میانه پر از ند کند همی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وان برگهای بید تو گویی کسی به قصد</p></div>
<div class="m2"><p>پیکانهای پهن زبرجد کند همی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضرابوار شاخ گل زرد هر شبی</p></div>
<div class="m2"><p>دینارهای گرد مجدد کند همی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بهر آنکه زلف معقد نکو بود</p></div>
<div class="m2"><p>سنبل به باغ زلف معقد کند همی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وز بهر آنکه روی بود سرخ خوبتر</p></div>
<div class="m2"><p>گلنار روی خویش مورد کند همی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خور باز مجمری بفروزد برآسمان</p></div>
<div class="m2"><p>گویی که زر به تیغ مهند کند همی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابر گلاب‌ریز همی بر گلابدان</p></div>
<div class="m2"><p>برروی گل گلاب مصعد کند همی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ابر بهار باز کند مطرد سیاه</p></div>
<div class="m2"><p>هر گه که روی خویش به راود کند همی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی عود، باد، عود مثلث کند همی</p></div>
<div class="m2"><p>بی‌تاب آب درع مزرد کند همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باغ طری ستبرق رومی کند همی</p></div>
<div class="m2"><p>بربر همی قلاده ز فرقد کند همی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر سر عصابهٔ زر رومی کند همی</p></div>
<div class="m2"><p>دربر لباده‌ای ز زبرجد کند همی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سوسن سرین ز بیرم کحلی کند همی</p></div>
<div class="m2"><p>نسرین دهان ز در منضد کند همی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لاله دل از فتیلهٔ عنبر کند همی</p></div>
<div class="m2"><p>خیری رخ از صحیفهٔ عسجد کند همی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باد بزین صناعت مانی کند همی</p></div>
<div class="m2"><p>مرغ حزین روایت معبد کند همی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بلبل گلو گشاده سحرگاه بر درخت</p></div>
<div class="m2"><p>گویی ثنای میر مؤید کند همی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بوحرب بختیار محمد، که رای او</p></div>
<div class="m2"><p>ارکانهای ملک مؤکد کند همی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>طوبی بر آن قلم که به عنوان نامه‌بر</p></div>
<div class="m2"><p>بوحرب بختیار محمد کند همی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر هیچ میر عمر مؤبد کند به فضل</p></div>
<div class="m2"><p>این میر عمر خویش مؤبد کند همی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ور هیچ خلق سعد کند طالع کسی</p></div>
<div class="m2"><p>او طالع کریمان اسعد کند همی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بی‌ابر، فعل ابر بهاری کند همی</p></div>
<div class="m2"><p>بی‌تیغ، کار تیغ مجرد کند همی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رای موافق و نیت و اعتقاد او</p></div>
<div class="m2"><p>عالم بسان خلد مخلد کند همی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کردارهٔ سلیمترین با عدوی خویش</p></div>
<div class="m2"><p>آنست کاین سلیم مسهد کند همی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اقبال کار مرد به رای مسدد است</p></div>
<div class="m2"><p>او رای کارهای مسدد کند همی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برش قلاده‌ایست که هر خرد و هر بزرگ</p></div>
<div class="m2"><p>گردن بدان قلاده مقلد کند همی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر هر کسی لطف کند و لطف بیشتر</p></div>
<div class="m2"><p>بر احمد بن قوص بن احمد کند همی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چونانش همتیست رفیع و فراشته</p></div>
<div class="m2"><p>کز فرق هر دو فرقد، مرقد کند همی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با چاکران خویش و جز از چاکران خویش</p></div>
<div class="m2"><p>احسان بی‌نهایت و بی‌حد کند همی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این عادتش طبیعی وجودش جبلی است</p></div>
<div class="m2"><p>هرعادتی نه مرد مسعد کند همی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کان اختیار کار نیاید که بنده کرد</p></div>
<div class="m2"><p>این اختیار میر محمد کند همی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا باد مشکبیز به اردیبهشت ماه</p></div>
<div class="m2"><p>عالم چو عارض بت امرد کند همی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر پای باد دولت میر بزرگوار</p></div>
<div class="m2"><p>کوپای حادثات مقید کند همی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زو قوت و سیادت و سودد مباد دور</p></div>
<div class="m2"><p>کوقوت و سیادت و سودد کند همی</p></div></div>