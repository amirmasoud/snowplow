---
title: >-
    شمارهٔ ۷۲ - در مدح ابوالحسن عمرانی
---
# شمارهٔ ۷۲ - در مدح ابوالحسن عمرانی

<div class="b" id="bn1"><div class="m1"><p>صنما! گرد سرم چند همی‌گردانی</p></div>
<div class="m2"><p>زشتی از روی نکو زشت بود گر دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا بکن آنچه شب و روز همی وعده دهی</p></div>
<div class="m2"><p>یا مکن وعده هر آن چیز که آن نتوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حد و غایت نافرمانی در مگذر</p></div>
<div class="m2"><p>که پدیدارست اندازهٔ نافرمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من بردی و از خویشتنم دور کنی</p></div>
<div class="m2"><p>برنیاید صنما کار بدین آسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهربانی نکنی بر من و مهرم طلبی</p></div>
<div class="m2"><p>ندهی داد و همی‌داد ز من بستانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌وفایی کنی و نادان سازی تن خویش</p></div>
<div class="m2"><p>نیستی‌ای بت یکباره بدین نادانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبوی راضی گر زانکه امیرت خوانم</p></div>
<div class="m2"><p>من بدان راضی باشم که غلامم خوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تو ما را نه کنار و نه پیام و نه سلام</p></div>
<div class="m2"><p>مکن ای دوست که کیفر بری و درمانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی: اندر دل پنهانت همی‌دارم دوست</p></div>
<div class="m2"><p>به بود دشمنی از دوستی پنهانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکن ای دوست که بیداد نشانی نگذاشت</p></div>
<div class="m2"><p>عدل باز آمد با بوالحسن عمرانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواجه و سید سادات رئیس الرؤسا</p></div>
<div class="m2"><p>همچو خورشید به بخشندگی و رخشانی</p></div></div>