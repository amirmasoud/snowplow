---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>چو از زلف شب باز شد تاب‌ها</p></div>
<div class="m2"><p>فرو مرد قندیل محراب‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپیده‌دم، از بیم سرمای سخت</p></div>
<div class="m2"><p>بپوشید بر کوه سنجاب‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به میخوارگان ساقی آواز داد</p></div>
<div class="m2"><p>فکنده به زلف اندرون تاب‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بانگ نخستین از آن خواب خوش</p></div>
<div class="m2"><p>بجستیم چون گو ز طبطاب‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عصیر جوانه هنوز از قدح</p></div>
<div class="m2"><p>همی‌زد به تعجیل پرتاب‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آواز ما خفته همسایگان</p></div>
<div class="m2"><p>بی‌آرام گشتند در خواب‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برافتاد بر طرف دیوار و بام</p></div>
<div class="m2"><p>ز بگمازها نور مهتاب‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منجم به بام آمد از نور می</p></div>
<div class="m2"><p>گرفت ارتفاع سطرلاب‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابر زیر و بم شعر اعشی قیس</p></div>
<div class="m2"><p>همی‌زد زننده به مضراب‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>و کاس شربت علی لذة</p></div>
<div class="m2"><p>و اخری تداویت منها بْها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لکی یعلم الناس انی امرو</p></div>
<div class="m2"><p>اخذت المعیشة من بابها</p></div></div>