---
title: >-
    شمارهٔ ۶۰ - در مدح سلطان مسعود غزنوی
---
# شمارهٔ ۶۰ - در مدح سلطان مسعود غزنوی

<div class="b" id="bn1"><div class="m1"><p>ای لعبت حصاری، شغلی دگر نداری</p></div>
<div class="m2"><p>مجلس چرا نسازی، باده چرا نیاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چونانکه من به شادی روزی هم گذارم</p></div>
<div class="m2"><p>خواهم که تو به شادی روزی همی‌گذاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دوستدار مایی، ای ترک خوبچهره</p></div>
<div class="m2"><p>زین بیش کرد باید مارات خواستاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنمای دوستداری، بفزای خواستاری</p></div>
<div class="m2"><p>زیرا که خواستاری باشد ز دوستداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خوارکار ترکی، من بردبار عاشق</p></div>
<div class="m2"><p>خوش نیست خوارکاری، خوبست بردباری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر با تو بردباری چندین نکردمی من</p></div>
<div class="m2"><p>در خدمتم نکردی چندین تو خوارکاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر گرد خوارکاری گردی تو نیز با ما</p></div>
<div class="m2"><p>آری تو خویشتن را نزدیک ما به خواری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من دل به تو سپردم، تا شغل من بسیجی</p></div>
<div class="m2"><p>زان دل به تو سپردم تا حق من گزاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر زانکه جرم کردم، کاین دل به تو سپردم</p></div>
<div class="m2"><p>خواهم که دل به رافت تو باز من سپاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل باز ده به خوشی ورنه ز درگه شه</p></div>
<div class="m2"><p>فردات خیلتاشی ترک آورم تتاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از درگه شهنشاه، مسعود با سعادت</p></div>
<div class="m2"><p>زیبا به پادشاهی، دانا به شهریاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاهی بزرگواری، کو را به هیچ کاری</p></div>
<div class="m2"><p>از کس نخواست باید، جز از خدای یاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او را گزید لشکر، او را گزید رعیت</p></div>
<div class="m2"><p>او را گزید دولت، او را گزید باری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از ننگ آنکه شاهان، باشند بر ستوران</p></div>
<div class="m2"><p>بر پشت ژنده پیلان، این شه کند سواری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر زانکه خسروان را مهدی بود بر استر</p></div>
<div class="m2"><p>خنیاگران او را پیلست با عماری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اکلیلهای پیلانش از گوهرست و لؤلؤ</p></div>
<div class="m2"><p>صندوق پیلهایش از صندل قماری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای شهریار عالم یک چند صید کردی</p></div>
<div class="m2"><p>یک چند گاه باید اکنون که می گساری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جام رحیق خواهی، شعر مدیح خواهی</p></div>
<div class="m2"><p>مال حلال جویی، شاخ کمال کاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من بنده را ز رحمت کردی بزرگ، شاها</p></div>
<div class="m2"><p>پاینده باد بختت، پاینده بختیاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درخواستی تو شعرم، اینت بزرگ شاهی</p></div>
<div class="m2"><p>اینت کریم طبعی، اینت بزرگواری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اضعاف حرفهایی کز شعر من شنیدی</p></div>
<div class="m2"><p>نیکیت باد و نعمت، شادیت و شادخواری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شعری که تو شنیدی، آنست بحر نیکو</p></div>
<div class="m2"><p>آنست وزن شیرین، آنست لفظ جاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بد گفتن اندرآنکس، کومادح تو باشد</p></div>
<div class="m2"><p>باشد ز زشتنامی، باشد ز بدعواری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای میر! مصطفی را گفتند کافران بد</p></div>
<div class="m2"><p>با آنهمه نبوت، وان فر کردگاری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چندان دروغ و بهتان، گفتند آن جهودان</p></div>
<div class="m2"><p>بر عیسی‌بن مریم، بر مریم و حواری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من کیستم که برمن نتوان دروغ گفتن</p></div>
<div class="m2"><p>نه قرص آفتابم، نه ماه ده چهاری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای شاعر سبکدل با من چه اوفتادت</p></div>
<div class="m2"><p>پنداشتم که زینت بیشست هوشیاری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو آفرین خسرو گویی دروغ باشد</p></div>
<div class="m2"><p>ویحک دلیر مردی کاین لفظ گفت یاری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با من همی چخی تو و آگه نه ای که خیره</p></div>
<div class="m2"><p>دنبال ببر خایی، چنگال شیر خاری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون روی من ببینی، با من کنی تلطف</p></div>
<div class="m2"><p>مهمان بری به خانه، نقل و رحیقم آری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>و آنجا که من نباشم، گویی مثالب من</p></div>
<div class="m2"><p>نیکست کت نیاید زین کار شرمساری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یا باش دشمن من، یا دوست باش ویحک</p></div>
<div class="m2"><p>نه دوستی نه دشمن، اینت سیاهکاری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آنکس که شاعرست او، او شاعران بداند</p></div>
<div class="m2"><p>خود باز باز داند از مرغک شکاری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تزویرگر نیم من، تزویرگر تو باشی</p></div>
<div class="m2"><p>زیرا که چون منی را تزویرگر شماری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این جایگاه نتوان تزویر شعر کردن</p></div>
<div class="m2"><p>افسوس کرد نتوان بر شیر مرغزاری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هستند جز تو اینجا استاد شاعرانی</p></div>
<div class="m2"><p>با لفظهای مائی، با طبعهای ناری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ایشان مرا تجارب کردند بی‌محابا</p></div>
<div class="m2"><p>دیدند سحر شعرم دیدند کامگاری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو نیز تجربت کن تا دستبرد بینی</p></div>
<div class="m2"><p>تا بردوم به شعرت چون باد صحاری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از بهر آنکه شعرم شه دید و خوشدل آمد</p></div>
<div class="m2"><p>برخاست از تو غلغل، برخاست از تو زاری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من شعر بیش گویم، کان شاه را خوش آید</p></div>
<div class="m2"><p>الفاظهای نیکو، ابیاتهای عاری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر تو به هر مدیحی، چندین تپید خواهی</p></div>
<div class="m2"><p>نهمار ناصبوری، نهمار بیقراری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا من در این دیارم، مدح کسی نگفتم</p></div>
<div class="m2"><p>جز آفرین و مدحت شه را به حقگزاری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جز درگه شهنشه بر درگهی نبودم</p></div>
<div class="m2"><p>نه بر در حجازی، نه بر در بخاری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همچون تویی که خدمت کهتر کنی و مهتر</p></div>
<div class="m2"><p>از بهر دوشیانی وز بهر یک دو آری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دانی که من مقیمم بر درگه شهنشه</p></div>
<div class="m2"><p>تا بازگشت سلطان از لاله‌زار ساری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>این دشتها بریدم، وین کوهها پیاده</p></div>
<div class="m2"><p>دو پای پر جراحت، دو دیده گشته تاری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>امید آنکه خواند، روزی ملک دو بیتم</p></div>
<div class="m2"><p>بختم شود مساعد، روزم شود بهاری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اکنون که شاه شاهان بر بنده کرد رحمت</p></div>
<div class="m2"><p>کوشی که رحمت شه از بنده بازداری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خشم آیدت که خسرو با من کند نکویی</p></div>
<div class="m2"><p>ای ویحک آب دریا از من دریغ داری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ای کاشکی حسودم، چون تو هزار بودی</p></div>
<div class="m2"><p>اکنون که دیده خسرو از من امیدواری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>حاسد چو بیش باشد بهتر رود سعادت</p></div>
<div class="m2"><p>چون باد بیش باشد، بهتر رود سماری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شاها به رغم حاسد، خواهم که من رهی را</p></div>
<div class="m2"><p>چون شاعران دیگر بر خدمتی گماری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بر من ز فرت ارجو آن عز و ناز باشد</p></div>
<div class="m2"><p>کز فر میر ماضی، بوده‌ست بر غضاری</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دایم بزی امیرا! با عز و با جلالت</p></div>
<div class="m2"><p>فعل تو بختیاری، ملک تو اختیاری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زیر تو تخت زرین بر سرت چتر دیبا</p></div>
<div class="m2"><p>زین سو صف غلامان، زان سو صف جواری</p></div></div>