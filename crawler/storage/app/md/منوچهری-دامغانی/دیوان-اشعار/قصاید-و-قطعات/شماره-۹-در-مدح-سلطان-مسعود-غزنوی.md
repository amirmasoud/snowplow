---
title: >-
    شمارهٔ ۹ - در مدح سلطان مسعود غزنوی
---
# شمارهٔ ۹ - در مدح سلطان مسعود غزنوی

<div class="b" id="bn1"><div class="m1"><p>ای ترک ترا با دل احرار چه کارست</p></div>
<div class="m2"><p>نه این دل ما غارت ترکان تتارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ما بستانی دل و ما را ندهی دل</p></div>
<div class="m2"><p>با ما چه سبب هست ترا، یا چه شمارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را به از این دار و دل ما به از این جوی</p></div>
<div class="m2"><p>من هیچ ندانم که مرا با تو چه کارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگاه که من جهد کنم دل به کف آرم</p></div>
<div class="m2"><p>بازش تو بدزدی ز من این کارنه کارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من پار بسی رنج و عنای تو کشیدم</p></div>
<div class="m2"><p>امسال به هش باش که امسال نه پارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه دل دهدم کز تو کنم روی به یکسوی</p></div>
<div class="m2"><p>نه با تو ازین بیش مرا رنج و مرارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر روز دگر خوی و دگر عادت و کبرست</p></div>
<div class="m2"><p>این خوی بد و عادت تو چند هزارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوی تو همی‌گردد و خویی که نگردد</p></div>
<div class="m2"><p>خوی ملک پیلتن شیر شکارست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مسعود ملک آنکه به جنب هنر او</p></div>
<div class="m2"><p>اندر ملکان هر چه هنر بود عوارست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن ملکستانی که هر آن ملک که بستاند</p></div>
<div class="m2"><p>کو تیغ بدو تیز کند ملکسپارست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در لشکر اسکندر از اسب نبودی</p></div>
<div class="m2"><p>چندانکه در این لشکر از پیل قطارست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ده پانزده من بیش نبد گرز فریدون</p></div>
<div class="m2"><p>هفتاد منی گرز شه شیر شکارست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از چوب بدی تخت سلیمان پیمبر</p></div>
<div class="m2"><p>وین تخت شه مشرق از زر عیارست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گویند که آن تخت ورا باد ببردی</p></div>
<div class="m2"><p>وین نزد من ای دوست نه فخرست و نه عارست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زیرا که هر آن چیز که باشد برباید</p></div>
<div class="m2"><p>باشد سبک و هر چه سبک باشد خوارست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ار روی ملوکانش هر روز نشاطست</p></div>
<div class="m2"><p>وز کیسهٔ شاهانش هر روز نثارست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر چند که خوبست، درو خوبترین چیز</p></div>
<div class="m2"><p>دیدار شه پیلتن شیرشکارست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آمد ملکا عید و می لعل همی‌گیر</p></div>
<div class="m2"><p>کاین می سبب رستن بنیان ضرارست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می بر تو حلالست که در دار قراری</p></div>
<div class="m2"><p>وان را بزه باشد که نه در دار قرارست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا خاک فروتر بود و نار زبرتر</p></div>
<div class="m2"><p>تا پیش هوا نار و هوا از پی نارست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گوشت به سوی نوش جهانگیر بزرگست</p></div>
<div class="m2"><p>چشمت به سوی آن صنم باده گسارست</p></div></div>