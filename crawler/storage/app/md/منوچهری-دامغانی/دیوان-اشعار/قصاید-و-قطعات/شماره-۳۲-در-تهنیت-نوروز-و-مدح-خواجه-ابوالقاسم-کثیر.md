---
title: >-
    شمارهٔ ۳۲ - در تهنیت نوروز و مدح خواجه ابوالقاسم کثیر
---
# شمارهٔ ۳۲ - در تهنیت نوروز و مدح خواجه ابوالقاسم کثیر

<div class="b" id="bn1"><div class="m1"><p>نوروز فرخ آمدو نغز آمد و هژیر</p></div>
<div class="m2"><p>با طالع مبارک و با کوکب منیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابر سیاه چون حبشی دایه‌ای شده‌ست</p></div>
<div class="m2"><p>باران چو شیر و لاله‌ستان کودکی بشیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر شیرخواره لاله ستانست، پس چرا</p></div>
<div class="m2"><p>چون شیرخواره، بلبل کو برزند صفیر!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صلصل به لحن زلزل وقت سپیده‌دم</p></div>
<div class="m2"><p>اشعار بونواس همی‌خواند و جریر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر بید، عندلیب زند، باغ شهریار</p></div>
<div class="m2"><p>برسرو، زندواف زند، تخت اردشیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق شده‌ست نرگس تازه به کودکی</p></div>
<div class="m2"><p>تا هم به کودکی قد او شد چو قد پیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با سرمه‌دان زرین ماند خجسته راست</p></div>
<div class="m2"><p>کرده به جای سرمه، بدان سرمه‌دان عبیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلنار، همچو درزی استاد برکشید</p></div>
<div class="m2"><p>قوارهٔ حریر، ز بیجاده‌گون حریر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی که شنبلید همه شب زریر کوفت</p></div>
<div class="m2"><p>تا بر نشست گرد به رویش بر، از زریر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برروی لاله، قیر به شنگرف برچکید</p></div>
<div class="m2"><p>گویی که مادرش همه شنگرف داد وقیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر شاخ نار اشکفهٔ سرخ شاخ نار</p></div>
<div class="m2"><p>چون از عقیق نرگسدانی بود صغیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نرگس چنانکه بر ورق کاسهٔ رباب</p></div>
<div class="m2"><p>خنیاگری فکنده بود حلقه‌ای ز زیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برگ بنفشه، چون بن ناخن شده کبود</p></div>
<div class="m2"><p>در دست شیرخواره به سرمای زمهریر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وان نسترن، چو مشکفروشی، معاینه</p></div>
<div class="m2"><p>در کاسهٔ بلور کند عنبرین خمیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اکنون میان ابر و میان سمنستان</p></div>
<div class="m2"><p>کافور بوی باد بهاری بود سفیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرغان دعا کنند به گل بر، سپیده‌دم</p></div>
<div class="m2"><p>برجان و زندگانی بوالقاسم کثیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شیخ العمید صاحب سید که ایمنست</p></div>
<div class="m2"><p>اندر پناه ایزد و اندر پناه میر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زایل نگردد از سر او تا جهان بود</p></div>
<div class="m2"><p>این سایهٔ شهنشه و این سایهٔ قدیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا دستگیر خلق بود خواجه، لامحال</p></div>
<div class="m2"><p>او را بود خدا و خداوند دستگیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خواجهٔ بزرگوار، بزرگست نزد ما</p></div>
<div class="m2"><p>وز ما بزرگتر، به بر خسرو خطیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرقان به نزد مردم عامه بود بزرگ</p></div>
<div class="m2"><p>لیکن بزرگتر به بر مردم بصیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زیرا که میرداند در فضل او تمام</p></div>
<div class="m2"><p>ما را به فضل او نرسد خاطر و ضمیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بسیار کس بود که بخواند ز بر نبی</p></div>
<div class="m2"><p>تفسیر او نداند جز مردم خبیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این عز و این کرامت و این فضل و این هنر</p></div>
<div class="m2"><p>زان اصل ثابتست و از آن گوهر اثیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کس را خدای بی‌هنری مرتبت نداد</p></div>
<div class="m2"><p>بیهوده هیچ سیل نیاید سوی غدیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باشد همو بزرگ و چنو روز او بزرگ</p></div>
<div class="m2"><p>باشد شقی حقیر و چنو روز او حقیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای بیقیاس و دولت تو چون تو بیقیاس</p></div>
<div class="m2"><p>ای بی‌نظیر و همت تو چون تو بی‌نظیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در خورد همت تو خداوند جاه داد</p></div>
<div class="m2"><p>جاه بزرگوار و گرانمایه و هجیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مقدار مرد و مرتبت مرد و جاه مرد</p></div>
<div class="m2"><p>باشد چنانکه در خور او باشد و جدیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ورز غنی بباید اندر خور غنی</p></div>
<div class="m2"><p>ورز فقیر باید اندر خور فقیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پیراهن قصیر بود زشت بر طویل</p></div>
<div class="m2"><p>پیراهن طویل، بود زشت بر قصیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر تو یسیر کرد خداوند کار تو</p></div>
<div class="m2"><p>ایزد کناد کار همه بندگان یسیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دایم بود هوای تن تو اسیر عقل</p></div>
<div class="m2"><p>اندی که نیست عقل هوای ترا اسیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دولت به سوی شاه رود، یا به سوی تو</p></div>
<div class="m2"><p>باران، به رودخانه رود، یا به آبگیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از نفس تو نیاید، فعل خسیس دون</p></div>
<div class="m2"><p>آواز سگ نیاید، از موضع زئیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>باشد به هر مراد به پیش تو بخت نیک</p></div>
<div class="m2"><p>از بخت نیک به، نبود مرد را خفیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دشمنت را همیشه نذیرست بخت بد</p></div>
<div class="m2"><p>از بخت بد بتر، نبود مرد را نذیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فعل تن تو نیکو، خوی تن تو نیک</p></div>
<div class="m2"><p>از خوی نیک باشد، فعل نکو خبیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از کار خیر، عزم تو هرگز نگشت‌باز</p></div>
<div class="m2"><p>هرگز ز راه باز نگشته ست هیچ تیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از حشمت تو ملک ملک را گزیر نیست</p></div>
<div class="m2"><p>آری درخت را بود از آب ناگزیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر حکم تو سریر تو محکم نداری</p></div>
<div class="m2"><p>زیر تو از سرور تو بر پردی سریر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جود از دو کف بخل زدایت کند نفر</p></div>
<div class="m2"><p>بخل از دو دست جود فزایت کند نفیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا شیر در میان بیابان کند خروش</p></div>
<div class="m2"><p>تا مرغ در میان درختان زند صفیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>روز تو باد فرخ، چون دلت با مراد</p></div>
<div class="m2"><p>دست تو باد با قدح و لبت با عصیر</p></div></div>