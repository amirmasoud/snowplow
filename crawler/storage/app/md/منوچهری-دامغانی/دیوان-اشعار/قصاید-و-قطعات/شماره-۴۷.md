---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>ای بت زنجیر جعد، ای آفتاب نیکوان</p></div>
<div class="m2"><p>طلعت خورشید داری، قامت فردوسیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نافرید ایزد زخوبان جهان چون تو کسی</p></div>
<div class="m2"><p>دلربا و دلفریب و دلنواز و دلستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرت خوانم ماه، ماهی، ورت خوانم سرو،سرو</p></div>
<div class="m2"><p>گرت خوانم حور، حوری، ورت خوانم جان، چو جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشک جعد و مشک خط و مشک ناف و مشکبوی</p></div>
<div class="m2"><p>خوش سماع و خوش سرود و خوش کنار و خوش زبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روت از گل درج دارد، درجت از عنبر طراز</p></div>
<div class="m2"><p>مشکت از مه نافه دارد، ماهت از مشک آسمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم بت زنجیر جعدی، هم بت زنجیر زلف</p></div>
<div class="m2"><p>هم بت لاله جبینی، هم بت لاله رخان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای روان و جان من دایم ز تو با خرمی</p></div>
<div class="m2"><p>ای سرا و باغ من دایم ز تو چون بوستان</p></div></div>