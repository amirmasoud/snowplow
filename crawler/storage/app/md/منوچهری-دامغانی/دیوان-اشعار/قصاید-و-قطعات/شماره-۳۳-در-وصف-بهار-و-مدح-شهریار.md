---
title: >-
    شمارهٔ ۳۳ - در وصف بهار و مدح شهریار
---
# شمارهٔ ۳۳ - در وصف بهار و مدح شهریار

<div class="b" id="bn1"><div class="m1"><p>نوبهار آمد و آورد گل تازه فراز</p></div>
<div class="m2"><p>می خوشبوی فزار آور و بربط بنواز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بلنداختر نام‌آور، تا چند به کاخ</p></div>
<div class="m2"><p>سوی باغ آی که آمد گه نوروز فراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوستان عود همی‌سوزد، تیمار بسوز</p></div>
<div class="m2"><p>فاخته نای همی‌سازد، طنبور بساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به قدح بلبله را سر به سجود آور زود</p></div>
<div class="m2"><p>که همی بلبل بر سرو کند بانگ نماز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سماعی که بدیعست، کنون گوش بنه</p></div>
<div class="m2"><p>به نبیدی که لطیفست، کنون دست بیاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر همی‌خواهی بنشست، ملکوار نشین</p></div>
<div class="m2"><p>ور همی تاختن آری به سوی خوبان تاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدوان از بر خویش و بپران از کف خویش</p></div>
<div class="m2"><p>بر آهوبچه، یوز و بر تیهوبچه، باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زرستان: مشک فشان، جام ستان، بوسه بگیر</p></div>
<div class="m2"><p>باده خور، لاله سپر، صید شکر، چوگان باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخل کش، داد ده و شیرکش و زهره شکاف</p></div>
<div class="m2"><p>تیغ کش، باره فکن، نیزه زن و تیرانداز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طلب و گیر و نمای و شمر و ساز و گسل</p></div>
<div class="m2"><p>طرب و ملک و نشاط و هنر و جود و نیاز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بستان کشور جود و بفشان زر و درم</p></div>
<div class="m2"><p>بشکن لشکر بخل و بفکن پیکر آز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آفرین زین هنری مرکب فرخ پی تو</p></div>
<div class="m2"><p>که به یک شب ز بلاساغون آید به طراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شخ نوردیکه چو آتش بود اندر حمله</p></div>
<div class="m2"><p>همچنان برق مجال و به روش باد مجاز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پایش از پیش دو دستش بنهد سیصد گام</p></div>
<div class="m2"><p>دستش از پیش دو چشمش بنهد سیصد باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بانگ او کوه بلرزاند، چون شنهٔ شیر</p></div>
<div class="m2"><p>سم او سنگ بدراند، چون نیش گراز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون ریاضتش کند رایض چون کبک دری</p></div>
<div class="m2"><p>بخرامد به کشی در ره و برگردد باز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه به دستش در خم و نه به پایش در عطف</p></div>
<div class="m2"><p>نه به پشتش در، پیچ و نه به پهلو در، ماز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بهتر از حوت به آب اندر، وز رنگ به کوه</p></div>
<div class="m2"><p>تیزتر ز آب به شیب اندر وز آتش به فراز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگذرد او به یکی ساعت از پول صراط</p></div>
<div class="m2"><p>بجهد باز به یک جستن از کوه طراز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ره بر و شخ شکن و شاد دل و تیز عنان</p></div>
<div class="m2"><p>خوش رو و سخت سم و پاک تن و جنگ آغاز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گوش و پهلو و میان و کتف و جبهه و ساق</p></div>
<div class="m2"><p>تیز و فربی و نزار و قوی و پهن و دراز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برق جه، باد گذر، یوز دو و کوه قرار</p></div>
<div class="m2"><p>شیر دل، پیل قدم، گورتک، آهو پرواز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بجهد، گر به جهانی، ز سر کوه بلند</p></div>
<div class="m2"><p>بدود، گر بدوانی ز بر تار طراز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که کن و بارکش و کارکن و راهنورد</p></div>
<div class="m2"><p>صفدر و تیزرو و تازه رخ و شیرآواز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به چنین اسب نشین و به چنین اسب گذر</p></div>
<div class="m2"><p>به چنین اسب گذار و به چنین اسب گراز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رخ دولت بفروز، آتش فتنه بنشان</p></div>
<div class="m2"><p>دل حکمت بزدای، آلت ملکت به طراز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر همه خلق ببند و به همه کس بگشای</p></div>
<div class="m2"><p>درهای حدثان و خمهای بگماز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نجهد از بر تیغت، نه غضنفر، نه پلنگ</p></div>
<div class="m2"><p>نرهد از کف رادت، نه بضاعت، نه جهاز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ماه را راس و ذنب ره ندهد در هر برج</p></div>
<div class="m2"><p>تا ز سعد تو ندارند مر این هر دو جواز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ذاکر فضل تو و مرتهن بر تواند</p></div>
<div class="m2"><p>چه طرازی به طراز و چه حجازی به حجاز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نصرت از کوههٔ زینت نه فرودست و نه بر</p></div>
<div class="m2"><p>دولت از گوشهٔ تاجت نه فرازست و نه باز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همچنین دیر زی و شاد زی و خرم زی</p></div>
<div class="m2"><p>همچنین داد ده و نیزه زن و بخل گداز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دست زی می بر و بر نه به سر نیکان تاج</p></div>
<div class="m2"><p>جام بر کف نه و بر نه به دل اعدا گاز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کش و بند و بر و آر و کن کار و خور و پوش</p></div>
<div class="m2"><p>کین و مهر و غم و لهو و بد و نیک و می و راز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ده و گیر و چن و باز و گز و بوس و روو کن</p></div>
<div class="m2"><p>زر و جام و گل و گوی و لب و روی و ره ناز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دل خویش و کف خویش و رخ خویش و سر خویش</p></div>
<div class="m2"><p>بزدای و بگشای و بفروز و بفراز</p></div></div>