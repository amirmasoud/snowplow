---
title: >-
    شمارهٔ ۱۶ - در وصف نوروز و مدح خواجه ابوالحسن بن حسن
---
# شمارهٔ ۱۶ - در وصف نوروز و مدح خواجه ابوالحسن بن حسن

<div class="b" id="bn1"><div class="m1"><p>روزی بس خرمست، می‌گیر از بامداد</p></div>
<div class="m2"><p>داد زمانه بده کایزد داد تو داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواسته داری و ساز، بیغمیت هست باز</p></div>
<div class="m2"><p>ایمنی و عز و ناز، فرخی و دین وداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیز چه خواهی دگر، خوش بزی و خوش بخور</p></div>
<div class="m2"><p>انده فردا مبر، گیتی خوابست و باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفته و فرمودنی، مانده و فرسودنی</p></div>
<div class="m2"><p>بود همه بودنی، کلک فرو ایستاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌خور کت بادنوش، بر سمن و پیلگوش</p></div>
<div class="m2"><p>روز رش و رام و جوش، روز خور و ماه و باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد نوروز ماه می خور و می ده پگاه</p></div>
<div class="m2"><p>هر روز تا شامگاه، هر شب تا بامداد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بارد در خوشاب، از آستین سحاب</p></div>
<div class="m2"><p>وز دم حوت آفتاب، روی به بالا نهاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برجه تا برجهیم، جام به کف برنهیم</p></div>
<div class="m2"><p>تن به می‌اندر دهیم، کاری صعب اوفتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرغ دل‌انگیز گشت، باد سمنبیز گشت</p></div>
<div class="m2"><p>بلبل شبخیز گشت، کبک گلو برگشاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلبل باغی به باغ، دوش نوایی بزد</p></div>
<div class="m2"><p>خوبتر از باربد نغزتر از بامشاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت سحرگه چکاو، خوش بزند در تکاو</p></div>
<div class="m2"><p>ساعتکی گنج گاو، ساعتکی گنج باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رعد تبیره زنست، برق کمند افکنست</p></div>
<div class="m2"><p>وقت طرب کردنست، می خور کت نوش باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قوس قزح، قوسوار، عالم فردوسوار</p></div>
<div class="m2"><p>کبک دری کوسوار، کرده گلو پر زباد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باغ پر از حجله شد راغ پر از کله شد</p></div>
<div class="m2"><p>دشت پر از دجله شد، کوه پر از مشک ساد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زان می عنابگون، در قدح آبگون</p></div>
<div class="m2"><p>ساقی، مهتابگون ترکی، حورا نژاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای بدل ذویزن، بوالحسن بن الحسن</p></div>
<div class="m2"><p>فاعل فعل حسن، صاحب دوکف راد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در همه کاری صبور، وز همه عیبی نفور</p></div>
<div class="m2"><p>کالبد تو ز نور، کالبد ما ز لاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فضل و کرم کرد تست، جود و سخا ورد تست</p></div>
<div class="m2"><p>دولت شاگرد تست گوهر و عقل اوستاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ویژه تویی در گهر، سخته تویی در هنر</p></div>
<div class="m2"><p>نکته تویی طرفه‌تر از نکت سندباد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای عوض آفتاب، روز و شبان به آب وتاب</p></div>
<div class="m2"><p>تو به مثل چون عقاب، حاسد ملعونت خاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفته امت مدحتی، خوبتر از لعبتی</p></div>
<div class="m2"><p>سخت نکو حکمتی، چون حکم بن معاذ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جایزه خواهم یکی، کم بدهی اندکی</p></div>
<div class="m2"><p>ور ندهی بیشکی، ز ایزد خواهم عیاذ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سیم تو زی من رسید، جامه نیامد پدید</p></div>
<div class="m2"><p>جام بباید کشید، جامه ببایدت داد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هست در آن بس کشی جامه ز تن برکشی</p></div>
<div class="m2"><p>برفکنی برکشی بنده‌ات را برچکاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بنده بنازد بدان، سر بفرازد بدان</p></div>
<div class="m2"><p>کس نگذارد بدان چون بچه بایست شاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا طرب و مطربست، مشرق و تا مغربست</p></div>
<div class="m2"><p>تا یمن و یثرب است، آمل و استار باد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بنشین خورشیدوار، می خور جمشیدوار</p></div>
<div class="m2"><p>فرخ و امیدوار چون پسر کیقباد</p></div></div>