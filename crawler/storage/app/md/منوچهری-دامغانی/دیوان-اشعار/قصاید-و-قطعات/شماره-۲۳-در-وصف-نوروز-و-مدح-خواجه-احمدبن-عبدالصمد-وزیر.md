---
title: >-
    شمارهٔ ۲۳ - در وصف نوروز و مدح خواجه احمدبن عبدالصمد وزیر
---
# شمارهٔ ۲۳ - در وصف نوروز و مدح خواجه احمدبن عبدالصمد وزیر

<div class="b" id="bn1"><div class="m1"><p>نوروز روز خرمی بیعدد بود</p></div>
<div class="m2"><p>روز طواف ساقی خورشید خد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجلس به باغ باید بردن، که باغ را</p></div>
<div class="m2"><p>مفرش کنون ز گوهر و مسند زند بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن برگهای شاسپرم بین و شاخ او</p></div>
<div class="m2"><p>چون صدهزار همزه که بر طرف مد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرگس بسان حلقهٔ زنجیر زر نگر</p></div>
<div class="m2"><p>کاندر میان حلقهٔ زرین وتد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر میان لاله، دلی هست عنبرین</p></div>
<div class="m2"><p>دل عنبرین بود، چو عقیقین جسد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن خاک هست والد و گل باشدش ولد</p></div>
<div class="m2"><p>بس رشد والدی که لطیفش ولد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابر گهرفشان را هر روز بیست بار</p></div>
<div class="m2"><p>خندیدن و گریستن و جزر و مد بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورشید چون نبرده حبیبی که باحبیب</p></div>
<div class="m2"><p>گاهیش وصل و صلح و گهی جنگ و صد بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم خجسته را مژه زرد و میان سیاه</p></div>
<div class="m2"><p>پرده زبرجدین و عقیقین رمد بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سنبل بسان زلفی با پیچ و با عقد</p></div>
<div class="m2"><p>زلف آن نکو بود که به پیچ و عقد بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بادام چون شیانی بارد به روز باد</p></div>
<div class="m2"><p>چون دست راد احمد عبدالصمد بود</p></div></div>