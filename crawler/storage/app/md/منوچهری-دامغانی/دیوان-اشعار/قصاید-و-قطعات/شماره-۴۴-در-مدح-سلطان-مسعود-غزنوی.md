---
title: >-
    شمارهٔ ۴۴ - در مدح سلطان مسعود غزنوی
---
# شمارهٔ ۴۴ - در مدح سلطان مسعود غزنوی

<div class="b" id="bn1"><div class="m1"><p>آمد نوروز ماه با گل سوری به هم</p></div>
<div class="m2"><p>بادهٔ سوری بگیر، بر گل سوری بچم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف بنفشه ببوی، لعل خجسته ببوس</p></div>
<div class="m2"><p>دست چغانه بگیر، پیش چمانه به خم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پسر نردباز داو گران بر به نرد</p></div>
<div class="m2"><p>وز دو کف سادگان ساتگنی کش به دم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای صنم ماهروی! خیز به باغ اندر آی</p></div>
<div class="m2"><p>زانکه شد از رنگ و بوی باغ بسان صنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاخ برانگیخت در، خاک برانگیخت نقش</p></div>
<div class="m2"><p>باد فرو بیخت مشک، ابر فرو ریخت نم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقرعه زن گشت رعد مقرعهٔ او درخش</p></div>
<div class="m2"><p>غاشیه کش گشت باد، غاشیهٔ او دیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قمری در شد به حال، طوطی در شد به نطق</p></div>
<div class="m2"><p>بلبل در شد به لحن، فاخته در شد به دم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جلوات آمده‌ست بر سر گل عندلیب</p></div>
<div class="m2"><p>در حرکات آمده‌ست شاخک شاهسپرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باد علمدار شد، ابر علم شد سیاه</p></div>
<div class="m2"><p>برق چنانچون ز زر یک دو طراز علم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راغ به باغ اندرون، چون علم اندر علم</p></div>
<div class="m2"><p>باغ به راغ اندرون، چون ارم اندر ارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر دم طاووس ماه، بر سر هدهد کلاه</p></div>
<div class="m2"><p>بر رخ دراج گل، بر لب طوطی بقم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گردن هر قمریی معدن جیمی ز مشک</p></div>
<div class="m2"><p>دیدهٔ هر کبککی مسکن میمی ز دم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رنگ رخ لاله را ازند و عودست خال</p></div>
<div class="m2"><p>شمع گل زرد را از می و مشکست شم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ماهی در آبگیر دارد جزعین زره</p></div>
<div class="m2"><p>آهو در مرغزار دارد سیمین شکم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باد زره گر شده‌ست، آب مسلسل زره</p></div>
<div class="m2"><p>ابر شده خیمه دوز ماغ مسلسل خیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صلصل خواند همی شعر لبید و زهیر</p></div>
<div class="m2"><p>نارو راند همی مدح جریر و قثم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر دم هر طاووسی صد قمر و سی قمر</p></div>
<div class="m2"><p>بر پر هر کککی نه رقم و ده رقم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرغان بر گل کنند جمله به نیکی دعا</p></div>
<div class="m2"><p>بر تن و بر جان میر بارخدای عجم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بار خدایی که او جز به رضای خدا</p></div>
<div class="m2"><p>بر همه روی زمین می‌ننهد یک قدم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاه جهان بوسعید ابن یمین دول</p></div>
<div class="m2"><p>حافظ خلق خدا ناصر دین امم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از بر اهل زمین، وز بر تخت پدر</p></div>
<div class="m2"><p>هست چو شمس الضحی هست چو بدر الظلم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روی ندارد گران از سپه و جز سپه</p></div>
<div class="m2"><p>مال ندارد دریغ از حشم و جز حشم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دولت او غالبست، بر عدو و جز عدو</p></div>
<div class="m2"><p>طاعت او واجبست بر خدم و جز خدم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عاقبت کار او در دو جهان خیر کرد</p></div>
<div class="m2"><p>عاقبت کار او خیر بود لاجرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نیست به بد رهنمون، نیست به بد مضطرب</p></div>
<div class="m2"><p>نیست به بد بردبار، نیست به بد متهم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شرم خدا آفرین بر دل او غالبست</p></div>
<div class="m2"><p>شرم نکو خصلتیست در ملک محتشم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بد نسگالد به خلق، بد نبود هرگزش</p></div>
<div class="m2"><p>وانکه بدی کرد هست عاقبتش بر ندم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دیوست آنکس که هست عاصی در امر او</p></div>
<div class="m2"><p>دیو در امر خدای عاصی باشد، نعم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ایزد هفت آسمان کرده‌ست اندر قران</p></div>
<div class="m2"><p>لعنت اینند جای بر تن دیو دژم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خسرو ما پیش دیو جم سلیمان شده‌ست</p></div>
<div class="m2"><p>وان سر شمشیر او مهر سلیمان جم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بالله نزدیک من حاجت سوگند نیست</p></div>
<div class="m2"><p>کز همه دیوان ملک، دود برآرد به هم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یا بکشدشان به پیل یا بکشدشان به تیر</p></div>
<div class="m2"><p>یا بگذارد به تیغ، یا بگدازد به غم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تیغ دو دستی زند بر عدوان خدای</p></div>
<div class="m2"><p>همچو پیمبر زده‌ست بر در بیت‌الحرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نز پی ملکت زند شاه جهان تیغ کین</p></div>
<div class="m2"><p>نز پی تخت و حشم، نز پی گنج و درم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بلکه ز بهر خدای وز پی خلق خدای</p></div>
<div class="m2"><p>وز پی ربح سپاه، وز پی سود خدم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دانی کاین فتنه بود هم به گه بیور اسب</p></div>
<div class="m2"><p>هم به گه بخت‌نصر هم به گه بوالحکم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هم گه بهرام گور هم گه نوشیروان</p></div>
<div class="m2"><p>هم به گه اردشیر هم به گه رستهم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آخر چیره نبود جز که خداوند حق</p></div>
<div class="m2"><p>آخر بیگانه را دست نبد بر عجم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آخر دیری نماند استم استمگران</p></div>
<div class="m2"><p>زانکه جهان‌آفرین دوست ندارد ستم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ایزد ما این جهان نز پی جور آفرید</p></div>
<div class="m2"><p>نز پی ظلم و فساد، نز پی کین و نقم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>داد ببین تا کجاست، فضل ببین تا کراست</p></div>
<div class="m2"><p>کیست عظیم الفعال، کیست کریم الشیم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اوست خداوند ملک، اوست خداوند خلق</p></div>
<div class="m2"><p>اوست محلی به حمد اوست مصفا ز ذم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>داد بر خسروست، فضل بر شهریار</p></div>
<div class="m2"><p>جود بر شاه شرق، بخشش مال و نعم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا نکند کس شمار جنبش چرخ فلک</p></div>
<div class="m2"><p>تا نکند کس پدید منبع جذر اصم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شاد روان باد شاه شاد دل و شادکام</p></div>
<div class="m2"><p>گنجش هر روز بیش، رنجش هر روز کم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر سر او تاج او نور فزوده به ملک</p></div>
<div class="m2"><p>در کف او تیغ او خصم کشیده به دم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دست سوی جام می، پای سوی تخت زر</p></div>
<div class="m2"><p>چشم سوی روی خوب، گوش سوی زیر و بم</p></div></div>