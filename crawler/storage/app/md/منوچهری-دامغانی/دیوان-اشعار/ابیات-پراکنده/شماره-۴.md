---
title: >-
    شمارهٔ  ۴
---
# شمارهٔ  ۴

<div class="b" id="bn1"><div class="m1"><p>آهو با شیر کی تواند کوشید</p></div>
<div class="m2"><p>جوجگک با باز کی تواند پرید</p></div></div>