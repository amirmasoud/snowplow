---
title: >-
    شمارهٔ  ۹
---
# شمارهٔ  ۹

<div class="b" id="bn1"><div class="m1"><p>چو رستم گشت در کوشش، چو حاتم گشت در بخشش</p></div>
<div class="m2"><p>چو لقمان گشت در حکمت، چو سلمان گشت در عرفان</p></div></div>