---
title: >-
    قطعه دوبیتی
---
# قطعه دوبیتی

<div class="b" id="bn1"><div class="m1"><p>هست ایام عید و فصل بهار</p></div>
<div class="m2"><p>جشن جمشید و گردش گلزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نگار بدیع وقت صبوح</p></div>
<div class="m2"><p>زود برخیز و راح روح بیار</p></div></div>