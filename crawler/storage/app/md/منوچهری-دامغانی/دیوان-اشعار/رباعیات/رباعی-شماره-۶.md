---
title: >-
    رباعی شمارهٔ ۶
---
# رباعی شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>مسعود جهاندار چو مسعود ملک</p></div>
<div class="m2"><p>بنشست به حق به جای محمود ملک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ملک جز این نبود مقصود ملک</p></div>
<div class="m2"><p>کز ملک به تربیت رسد جود ملک</p></div></div>