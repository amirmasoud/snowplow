---
title: >-
    شمارهٔ ۸ - در شهادت عابس بن شبیب شاکری
---
# شمارهٔ ۸ - در شهادت عابس بن شبیب شاکری

<div class="b" id="bn1"><div class="m1"><p>ای چو زنها کرده تکمیل جمال</p></div>
<div class="m2"><p>کن چو مردان عزم تحصیل کمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند از این مال و منالت افتخار</p></div>
<div class="m2"><p>کاین منال و مال را نی اعتبار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس بمال ارمرد کارآگه بدی</p></div>
<div class="m2"><p>بی سخن قارون کلیم الله بدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به اسبت نازی این نازش در اوست</p></div>
<div class="m2"><p>که اصیل و چابک و نغز و نکوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق ز اسب و استر ارآدم شدند</p></div>
<div class="m2"><p>اهل اصطبل افسر عالم شدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ترا نازش بجامه اطلس است</p></div>
<div class="m2"><p>همچو تو این جامه با صد ناکس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرد اطلس پوش اگر ز اطیاب بود</p></div>
<div class="m2"><p>کرم ابریشم یک از اقطاب بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگذر از اینها که رطب و یابس است</p></div>
<div class="m2"><p>فخر زیبا بر جناب عابس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چونکه شه را در وغا بی یار دید</p></div>
<div class="m2"><p>زندگانی برتن خود عار دید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با غلام خود که شوذب داشت نام</p></div>
<div class="m2"><p>گفت رایت چیست درکار امام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شوذب او را داد پاسخ کای دلیر</p></div>
<div class="m2"><p>هرچه زودش جان سپارم هست دیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت طوبی لک چنین خوش دیدمت</p></div>
<div class="m2"><p>زآن سبب بر مشورت بگزیدمت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس به نزد شه شد و بوسید خاک</p></div>
<div class="m2"><p>گفت ای گردون زعشقت سینه چاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست کس پیشم زتو محبوب تر</p></div>
<div class="m2"><p>دادمت گر بودم از جان خوب تر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دارم اینک عزم رزم این سپاه</p></div>
<div class="m2"><p>نزد پیغمبر تو باش از من گواه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وانکه اسب انگیخت سوی آن گروه</p></div>
<div class="m2"><p>همچو سیلی کو نشیب آید ز کوه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت چون دیدش ربیع بن تمیم</p></div>
<div class="m2"><p>کای سپه حقت لنا نارالجحیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>الحذر که شیر شیران است این</p></div>
<div class="m2"><p>گاه کین مرگ دلیرانست این</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پور پرشور شبیب شاکریست</p></div>
<div class="m2"><p>بر دم تیغش اجل را چاکریست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کس بتنها سوی او ننهد قدم</p></div>
<div class="m2"><p>که رود ز اول قدم سوی عدم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لشکر از بیم آنچنان پیچان شدند</p></div>
<div class="m2"><p>که ندیده رزم از او بیجان شدند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لیک عابس تاخت هر سو جنگ جو</p></div>
<div class="m2"><p>لفظ چون قندش مکرر مرد کو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون عمر یک تن هماوردش نیافت</p></div>
<div class="m2"><p>گفت باید جمله بر حربش شتافت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لشکر از جا کرد جنبش یکسره</p></div>
<div class="m2"><p>ماند او چون نقطه اندر دایره</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تیغ جز نزدیک چون ناید بکار</p></div>
<div class="m2"><p>بیم جیش از دور کردش سنگسار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در شکستن سنگ هر پولاد مشت</p></div>
<div class="m2"><p>گاه سینه گاه پهلو گاه پشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شد چو زان بیشرم نامردم نژند</p></div>
<div class="m2"><p>جوشن از بر کند و خود ازسرفکند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت من کز سرگذشتم سر ز هوش</p></div>
<div class="m2"><p>خود بار سر بود سربار دوش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون نمودم ترک جان تن گو مباش</p></div>
<div class="m2"><p>تن چو بیجان گشت جوشن گو مباش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر تن استم بارش سنگ وکلوخ</p></div>
<div class="m2"><p>همچو گل از دست یاری شنگ و شوخ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرد کش تاب کلوخ و سنگ نیست</p></div>
<div class="m2"><p>جز زنی رعنا و شوخ و شنگ نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>الغرض میکشت ده ده بیست بیست</p></div>
<div class="m2"><p>تا بخاک افکند افزون از دویست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لیک از هر جانبش سنگی گران</p></div>
<div class="m2"><p>پوست را با گوشت کند از استخوان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خاک ره با خون او بسرشته گشت</p></div>
<div class="m2"><p>گوشتش با خاک ره آغشته گشت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بسکه خونش ریخته ازسنگ شد</p></div>
<div class="m2"><p>سنگ دشت کینه مرجان رنگ شد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون ز ضعف از زین نگون شد پیکرش</p></div>
<div class="m2"><p>جیش ببریدند از پیکر سرش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پس ز فخر کشتن آن نامور</p></div>
<div class="m2"><p>شد خصومت جیش را نزد عمر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>این همی گفت اوفتاد ازسنگ من</p></div>
<div class="m2"><p>وان دگر گفتا نرست ازچنگ من</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گفت ابن سعد از تدبیر شوم</p></div>
<div class="m2"><p>کو نشد مقتول الا از هجوم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جمله را باید بناوردش ستود</p></div>
<div class="m2"><p>زآنکه یک یک کس هماوردش نبود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یاد عابس کز دل جیحون گذشت</p></div>
<div class="m2"><p>موج اشکش از سر گردون گذشت</p></div></div>