---
title: >-
    شمارهٔ ۱۷ - در منقبت شاه اولیا علی مرتضی و شهادت حضرت علی اصغر
---
# شمارهٔ ۱۷ - در منقبت شاه اولیا علی مرتضی و شهادت حضرت علی اصغر

<div class="b" id="bn1"><div class="m1"><p>ای که فرو رفته ببحر تمنی</p></div>
<div class="m2"><p>گاه بصورت چمی وگاه بمعنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشکن و بفکن هرآنچه اسفل و اعلی</p></div>
<div class="m2"><p>خواهی اگر رستگی به نشئه اخری</p></div></div>
<div class="b2" id="bn3"><p>جوی بجان بستگی بصادر اول</p></div>
<div class="b" id="bn4"><div class="m1"><p>احمد و حیدر که یک وجود و دو اسمند</p></div>
<div class="m2"><p>کشور ایجاد را قویم طلسمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه ز صلب و رحم عیان بدو قسمند</p></div>
<div class="m2"><p>لیک یکی روح رفته در بدو جسمند</p></div></div>
<div class="b2" id="bn6"><p>دو ننماید مگر بدیده احول</p></div>
<div class="b" id="bn7"><div class="m1"><p>شیر خدا آفتاب برج میامن</p></div>
<div class="m2"><p>باب حکم پرده دار واجب و ممکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مخزن اسرار هر چه ساری و ساکن</p></div>
<div class="m2"><p>عرصه لاهوت راست ماه مهیمن</p></div></div>
<div class="b2" id="bn9"><p>ساحت ناسوت راست شاه مجلل</p></div>
<div class="b" id="bn10"><div class="m1"><p>عقل بر ذاتش ازکبر به تصبی</p></div>
<div class="m2"><p>عرش بر قدرش از عظم بتابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نوح بنزد مقام او متنبی</p></div>
<div class="m2"><p>جان نبی را تنش بهینه مربی</p></div></div>
<div class="b2" id="bn12"><p>چهر خدا را رخش مهینه سجنجل</p></div>
<div class="b" id="bn13"><div class="m1"><p>فرش در لامکان فراشته اورنگ</p></div>
<div class="m2"><p>باسش قاروره قضا زده برسنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ملک ورا جبرئیل مرغ شباهنگ</p></div>
<div class="m2"><p>پای تصور بکوی شوکت او لنگ</p></div></div>
<div class="b2" id="bn15"><p>دست تفکر بذیل حشمت او شل</p></div>
<div class="b" id="bn16"><div class="m1"><p>بود و نبود اسمی از تکون آدم</p></div>
<div class="m2"><p>آدم تنها نه بلکه خلقت عالم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هستی او سکه زد بنقد پر وکم</p></div>
<div class="m2"><p>چرخ بر کاخ او بنائی مبهم</p></div></div>
<div class="b2" id="bn18"><p>مهر برچهر او وجودی مهمل</p></div>
<div class="b" id="bn19"><div class="m1"><p>ای که ز گردون چو شد مقام بخاکت</p></div>
<div class="m2"><p>خلق نشاندند جنب ابن صهاکت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کیست که بیند رسل گریبان چاکت</p></div>
<div class="m2"><p>ایزد ننموده جز به پیکر پاکت</p></div></div>
<div class="b2" id="bn21"><p>مختصری تا بدین نهایه مطول</p></div>
<div class="b" id="bn22"><div class="m1"><p>عقبی بیروی تو بذلت دنیا</p></div>
<div class="m2"><p>دنیا بارای تو بعزت عقبی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حکم تو صورت جدا کند زهیولی</p></div>
<div class="m2"><p>با تو زمین نجف زگردون اعلی</p></div></div>
<div class="b2" id="bn24"><p>بی تو سپهر برین ز غبرا اسفل</p></div>
<div class="b" id="bn25"><div class="m1"><p>هم ازل ازمهر تو باخذ مطالع</p></div>
<div class="m2"><p>هم ابد از قهر تو بکسب مقاطع</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از تو قلم زد بلوح نقش وقایع</p></div>
<div class="m2"><p>امر شریعت بدون سعی تو ضایع</p></div></div>
<div class="b2" id="bn27"><p>کار نبوت جدا زتیغ تو مختل</p></div>
<div class="b" id="bn28"><div class="m1"><p>کشور توحید شد زقلب تو محدود</p></div>
<div class="m2"><p>باره دین گشت ز اهتمام تو مشدود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بزم توصد پرده به ز جنت موعود</p></div>
<div class="m2"><p>قوت ایزد ز بازوان تو مشهود</p></div></div>
<div class="b2" id="bn30"><p>لطف الهی زعارض تو ممثل</p></div>
<div class="b" id="bn31"><div class="m1"><p>ای حرم کعبه ات ز حلقه بگوشان</p></div>
<div class="m2"><p>وی دل دانای تو زبان خموشان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>با تو که گفت ازحسین چشم بپوشان</p></div>
<div class="m2"><p>خاصه در آندم که اهل بیت خروشان</p></div></div>
<div class="b2" id="bn33"><p>نزدش با اصغر آمدند معجل</p></div>
<div class="b" id="bn34"><div class="m1"><p>گفتند این طفل کو چو بحر بجوشد</p></div>
<div class="m2"><p>نیست چو ماکز عطش بصبر بکوشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اشک بپاشد چنانکه خاک بپوشد</p></div>
<div class="m2"><p>رخ بخراشد چنانکه جان بخروشد</p></div></div>
<div class="b2" id="bn36"><p>جز بکفی آب عقده اش نشود حل</p></div>
<div class="b" id="bn37"><div class="m1"><p>هی بفغان خود زگاهواره پراند</p></div>
<div class="m2"><p>مادر او هم زبان طفل نداند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه بودش شیر تا بلب برساند</p></div>
<div class="m2"><p>نه بودش آب تا برخ بفشاند</p></div></div>
<div class="b2" id="bn39"><p>مانده بتسکین قلب اوست معطل</p></div>
<div class="b" id="bn40"><div class="m1"><p>گاهی ناخن زند بسینه مادر</p></div>
<div class="m2"><p>گاهی بیجان شود بدامن خواهر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>باری ازما گذشته چاره اصغر</p></div>
<div class="m2"><p>با بنشانش شرار آه چو آذر</p></div></div>
<div class="b2" id="bn42"><p>یا ببرش همرهت بجانب مقتل</p></div>
<div class="b" id="bn43"><div class="m1"><p>شه ز حرم خانه اش ربود و روانشد</p></div>
<div class="m2"><p>پیر خرد همعنان بخت جوان شد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زین پدر وزن پسر بلرزه جهان شد</p></div>
<div class="m2"><p>آمد و آورد هر طرف نگران شد</p></div></div>
<div class="b2" id="bn45"><p>تا بکه سازد حقوق خویش مدلل</p></div>
<div class="b" id="bn46"><div class="m1"><p>گفت که ایقوم روح پیکرم اینست</p></div>
<div class="m2"><p>ثانی حیدر علی اصغرم اینست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آن همه اصغر بدند اکبرم اینست</p></div>
<div class="m2"><p>حجه کبرای روز محشرم اینست</p></div></div>
<div class="b2" id="bn48"><p>رحمی کش حال برفناست محول</p></div>
<div class="b" id="bn49"><div class="m1"><p>او که بدین کودکی گناه ندارد</p></div>
<div class="m2"><p>یا که سر رزم این سپاه ندارد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بلکه بس افسرده است آه ندارد</p></div>
<div class="m2"><p>جای دهید آنکه را پناه ندارد</p></div></div>
<div class="b2" id="bn51"><p>پیش کز ایزد بریدکیفر اکمل</p></div>
<div class="b" id="bn52"><div class="m1"><p>ناکه از آنقوم از سعادت محروم</p></div>
<div class="m2"><p>حرمله اش تیرکینه راند بحلقوم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>حلق وراخست و جست برشه مظلوم</p></div>
<div class="m2"><p>وز شه مظلوم آن سه شعبه مسموم</p></div></div>
<div class="b2" id="bn54"><p>رد شد و سرزد زقلب احمد مرسل</p></div>
<div class="b" id="bn55"><div class="m1"><p>طفلی کز تشنگی بغم شده مدغم</p></div>
<div class="m2"><p>جست و برآورد دست و خست رخ از غم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گردن و سرگاه راست کرد وگهی خم</p></div>
<div class="m2"><p>شه زگلویش کشید تیر و هماندم</p></div></div>
<div class="b2" id="bn57"><p>ملک جهان بر جنان نمود مبدل</p></div>
<div class="b" id="bn58"><div class="m1"><p>شاها جیحون کهینه چامه نگارم</p></div>
<div class="m2"><p>کز فر تو مهر گشته حاجب بارم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ده به امم اجر هرچه مدح تو آرم</p></div>
<div class="m2"><p>من بجنان و جحیم کار ندارم</p></div></div>
<div class="b2" id="bn60"><p>باتوام از نور و نار رسته مخیل</p></div>