---
title: >-
    شمارهٔ ۱۸ - در تشکیل مجلس عزا و رثای بر جناب شهادت مآب حضرت سیدالشهدا
---
# شمارهٔ ۱۸ - در تشکیل مجلس عزا و رثای بر جناب شهادت مآب حضرت سیدالشهدا

<div class="b" id="bn1"><div class="m1"><p>یارب زکیست برپا این بزم دردناکی</p></div>
<div class="m2"><p>کز قدسیان رود هوش زین خاکیان باکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آلودگان برغم هر یک بعین پاکی</p></div>
<div class="m2"><p>گوئی حلال دانند هم گریه هم تباکی</p></div></div>
<div class="b2" id="bn3"><p>مانا حرام دانند هم بذله هم تبسم</p></div>
<div class="b" id="bn4"><div class="m1"><p>هم از سیاه پوشی مرکعبه راست معشوق</p></div>
<div class="m2"><p>هم از سپید کاری مرخلد راست موثوق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برآن شده است اکلیل دراین زده است منجوق</p></div>
<div class="m2"><p>سینا و نور حقش از برق آه مخلوق</p></div></div>
<div class="b2" id="bn6"><p>ظلمات و آب خضرش از اشک چشم مردم</p></div>
<div class="b" id="bn7"><div class="m1"><p>یکجا بتی چو خورشید پر ازستاره اش رخ</p></div>
<div class="m2"><p>یکسو بچین ز محنت روئی چو ماه خلخ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم را نهاده ترجیح بر روزگار فرخ</p></div>
<div class="m2"><p>لب خشک و دیدگان تر هر شوخ نغز پاسخ</p></div></div>
<div class="b2" id="bn9"><p>دم سرد و اندرون گرم هر شیخ خوش تکلم</p></div>
<div class="b" id="bn10"><div class="m1"><p>هرگز ندیده ام من بزمی چنین بعالم</p></div>
<div class="m2"><p>کش انبساط عشرت در انعقاد ماتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مدهوش پیر و برنا در جوش ترک و دیلم</p></div>
<div class="m2"><p>گیسوی مهر چهران ازغم چو دم ارقم</p></div></div>
<div class="b2" id="bn12"><p>مژگان مه جبینان زانده چو نیش کژدم</p></div>
<div class="b" id="bn13"><div class="m1"><p>گوئی قتیل گشته است زین فرقه مهذب</p></div>
<div class="m2"><p>شاهیکه بی سریرش جانها بود معذب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زینسان کزو جهانیست گریان ز صبح تا شب</p></div>
<div class="m2"><p>تا برچه پایه افسرد زو کشت خاطراب</p></div></div>
<div class="b2" id="bn15"><p>تا بر چه مایه پژمرد زو غنچه دل ام</p></div>
<div class="b" id="bn16"><div class="m1"><p>تا از کدام خیلست این کشته مطهر</p></div>
<div class="m2"><p>کاندر مصیب اوست هر فرقه ای بر آذر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این بی تجملش تن آن بی عمامه اش سر</p></div>
<div class="m2"><p>هم مشرب قلندر آزادگان افسر</p></div></div>
<div class="b2" id="bn18"><p>هم مسند خشن پوش پروردگان قاقم</p></div>
<div class="b" id="bn19"><div class="m1"><p>نی نی یگانه بزمی است برتر زقبه ماه</p></div>
<div class="m2"><p>کز اوج سدره بگذشت آن را حضیض درگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دروی بسبط احمد شش سو بناله و آه</p></div>
<div class="m2"><p>آن تاج هفت اختر آن شبل سیمین شاه</p></div></div>
<div class="b2" id="bn21"><p>محبوب عقل اول یعنی فروغ پنجم</p></div>
<div class="b" id="bn22"><div class="m1"><p>شاهیکه چون جلالش زد نوبت انا الحق</p></div>
<div class="m2"><p>ذرات ما سوا را شد رتبتش مصدق</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عم عرش از او برفعت هم خلد از او برونق</p></div>
<div class="m2"><p>بر انبیا مرسل بر او صیا مطلق</p></div></div>
<div class="b2" id="bn24"><p>در ظاهرش تأخر در باطنش تقدم</p></div>
<div class="b" id="bn25"><div class="m1"><p>لیکن بدین شرافت چون زد بکربلا تخت</p></div>
<div class="m2"><p>جسم چو جان او گشت از تیر و نیزه صد لخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرکس به نصرتش خاست در باخت از جهان رخت</p></div>
<div class="m2"><p>این یک دلیل هرسست آن یک دخیل هرسخت</p></div></div>
<div class="b2" id="bn27"><p>این یک بوقعه پیدا آن یک به ناحیه گم</p></div>
<div class="b" id="bn28"><div class="m1"><p>یعقوب وارگشته اندر حزن شکیبا</p></div>
<div class="m2"><p>یوسف وش اوفتاده در چنگ گرگ اعدا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یحیی صفت نهاده سر را بطشت یغما</p></div>
<div class="m2"><p>اندام روح بخشش درخون بزیر و بالا</p></div></div>
<div class="b2" id="bn30"><p>مانندکشتی نوح کز موج درتلاطم</p></div>
<div class="b" id="bn31"><div class="m1"><p>هم پیکر بدیعش پامال نعل ابرش</p></div>
<div class="m2"><p>هم خیمه رفیعش محروق تف آتش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>درغارتش اعادی با هم پی کشاکش</p></div>
<div class="m2"><p>صبیان او پریشان نسوان او مشوش</p></div></div>
<div class="b2" id="bn33"><p>این را بجان توحش آن را بتن تالم</p></div>
<div class="b" id="bn34"><div class="m1"><p>برخی ز دخترانش چون مرغ نیم بسمل</p></div>
<div class="m2"><p>دستی ز غصه برسر پائی ز اشک درگل</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این خسته از معاند آن بسته از موکل</p></div>
<div class="m2"><p>پیدا عذار ایشان از حلقه سلاسل</p></div></div>
<div class="b2" id="bn36"><p>چون بر مجره تابان نور جمال انجم</p></div>
<div class="b" id="bn37"><div class="m1"><p>قومی زخواهرانش بابخت خود ستیزان</p></div>
<div class="m2"><p>درسایه کنیزان ازچشم بدگریزان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>این از نتیجه افتان آن از شکنجه خیزان</p></div>
<div class="m2"><p>در بارگاه دشمن از دیده اشک ریزان</p></div></div>
<div class="b2" id="bn39"><p>چون در میان شعله جوشنده بحر قلزم</p></div>
<div class="b" id="bn40"><div class="m1"><p>شاها مصایبت را دیدم چو غیر محدود</p></div>
<div class="m2"><p>از دیده و دهانم انگیخت درمنضود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ارجو که برگزینی از شاعرانم از جود</p></div>
<div class="m2"><p>آری چو هست جیحون خود چیست شعر مسعود</p></div></div>
<div class="b2" id="bn42"><p>جائیکه آب باشد باطل بود تیمم</p></div>