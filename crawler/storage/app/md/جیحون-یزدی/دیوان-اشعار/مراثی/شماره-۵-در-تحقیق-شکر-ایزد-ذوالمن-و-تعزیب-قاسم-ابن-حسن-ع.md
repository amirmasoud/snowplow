---
title: >-
    شمارهٔ ۵ - در تحقیق شکر ایزد ذوالمن و تعزیب قاسم ابن حسن (ع)
---
# شمارهٔ ۵ - در تحقیق شکر ایزد ذوالمن و تعزیب قاسم ابن حسن (ع)

<div class="b" id="bn1"><div class="m1"><p>ای که ایزد را کنی شکر و سپاس</p></div>
<div class="m2"><p>لفظ را بگذار و معنی را شناس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر هر چیزی زجنس خویش دان</p></div>
<div class="m2"><p>وزچنین شکر اندکی را بیش دان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکر زر وسیم اینست ای عمو</p></div>
<div class="m2"><p>کز تو گردد مضطری با آبرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکر اسب خوب این است ای فگار</p></div>
<div class="m2"><p>که شود وامانده بروی سوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکر این کت جامه الوان بود</p></div>
<div class="m2"><p>پوشش بیچاره عریان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکر این کت سفره پر رنگ و بوست</p></div>
<div class="m2"><p>خوردن همسایه مسکین کوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر خرمن این بود ای خوش صفات</p></div>
<div class="m2"><p>که ببخشی خوشه چینان را زکات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکر این کآمد زبان تو فصیح</p></div>
<div class="m2"><p>بستن لب دان زگفتار قبیح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکر این کت خانه خوش شد نصیب</p></div>
<div class="m2"><p>باشی اندرخط ایتام و غریب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکر اینکه هر دو پایت لنگ نیست</p></div>
<div class="m2"><p>جز قدم در راه دستی تنگ نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکر دستت که نشد شل ای دبیر</p></div>
<div class="m2"><p>خود زپا افتاده را دست گیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آری آری شکر لفظ و قول نیست</p></div>
<div class="m2"><p>دفع دیو از ظاهر لاحول نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ورنه کوفی هم نمود ای پاک ذات</p></div>
<div class="m2"><p>شکر حق در خوردن آب فرات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شکر کوفی این بود ای نور عین</p></div>
<div class="m2"><p>که نبندد آب بر روی حسین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون بشاه کربلا شد کار تنگ</p></div>
<div class="m2"><p>قاسم آمد تا ستاند اذن جنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هی بگریه بوسه زد بر دست شاه</p></div>
<div class="m2"><p>گشته جانش عاشق و پا بست شاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گاه پای شاه بوسیدی زغم</p></div>
<div class="m2"><p>دست خود پیچید در دامان عم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از صفا بس کرد گرد شه طواف</p></div>
<div class="m2"><p>یافت آن صید حرم اذن مصاف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آمد اندر رزم بی خفتان و خود</p></div>
<div class="m2"><p>جز ازار و پیرهن هیچش نبود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تاخت حیدر وار باطاق وطرم</p></div>
<div class="m2"><p>هر طرف آن ماه لم یبلغ حلم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت راوی نیک می آرم بیاد</p></div>
<div class="m2"><p>که چو قاسم روی بر میدان نهاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بندی از نعلین او بگسسته بود</p></div>
<div class="m2"><p>وزکمال کودکی نابسته بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بلکه از چپ بود آن هم نی زراست</p></div>
<div class="m2"><p>وزچپ واز راست اینسان رزم خواست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بانگ زد کای ابن سعد پر گنه</p></div>
<div class="m2"><p>اسب خود را آب دادی یا که نه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت آری تشنه کی مانم کمیت</p></div>
<div class="m2"><p>گفت پس چون تشنه خواهی اهل بیت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اسب تو سیراب و ما در العطش</p></div>
<div class="m2"><p>این یکی مدهوش و آن یک کرده غش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اسب تو سیراب و طفل شیر خوار</p></div>
<div class="m2"><p>در دلش از تنشه کامی خارخار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اسب تو سیراب و اولاد رسول</p></div>
<div class="m2"><p>از عطش بریان وگریان و ملول</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پس برون آورد تیغ آبدار</p></div>
<div class="m2"><p>زد همی بر خرمن جانها شرار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>او پیاده آن ستم کاران سوار</p></div>
<div class="m2"><p>او تنی تنها و ایشان صد هزار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>او بظاهر کوچک و آنها بزرگ</p></div>
<div class="m2"><p>او بباطن یوسف و آن قوم گرگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ناگهان باران تیرش درگرفت</p></div>
<div class="m2"><p>جسمش از پیکان چو عنقا پرگرفت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از خدنگش سینه بس سوراخ شد</p></div>
<div class="m2"><p>روزنش سوی الهی کاخ شد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پایش از رفتار و دست ازکار ماند</p></div>
<div class="m2"><p>اوفتاد وعم امجد را بخواند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تاخت شاه و نزد وی آنگه رسید</p></div>
<div class="m2"><p>که تنی میخواست او را سر برید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دست حق با تیغ بهرش شد علم</p></div>
<div class="m2"><p>دست شیطانیش گشت از بن قلم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن دغل نالید و بر رسم عرب</p></div>
<div class="m2"><p>از قبیله خویش شد یاری طلب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گفت الغوث ای شجاعان دلیر</p></div>
<div class="m2"><p>که فتاده رو به اندر چنگ شیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برسر نعش یتیم مجتبی</p></div>
<div class="m2"><p>جنگ در پیوست باشه از عدی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کوزه گیر و ده و دو گرم شد</p></div>
<div class="m2"><p>وز تکاپو جسم قاسم نرم شد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>استخوان پشت و پیش و پای و دست</p></div>
<div class="m2"><p>زیر سم اسبها درهم شکست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شاه چو نکرد آن جماعت را پریش</p></div>
<div class="m2"><p>دید قاسم خفته اندر خون خویش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بس بخاک از درد سوده پاشنه</p></div>
<div class="m2"><p>خاک را داده شکاف و پاش نه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گفت عمت راست سخت این داوری</p></div>
<div class="m2"><p>که تواش خوانی و ندهد یا روی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>باز جیحونا تلاطم میکنی</p></div>
<div class="m2"><p>چشم مردم را چو قلزم میکنی</p></div></div>