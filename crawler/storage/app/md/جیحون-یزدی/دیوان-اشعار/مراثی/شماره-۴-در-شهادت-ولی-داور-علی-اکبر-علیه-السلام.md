---
title: >-
    شمارهٔ ۴ - در شهادت ولی داور علی اکبر علیه السلام
---
# شمارهٔ ۴ - در شهادت ولی داور علی اکبر علیه السلام

<div class="b" id="bn1"><div class="m1"><p>چوشد در روز عاشورای پر شور</p></div>
<div class="m2"><p>جهان ازگردکین چون شام دیجور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علی اکبر آن پیرانه عشق</p></div>
<div class="m2"><p>کزو تکمیل شد سرمایه عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهین شهزاده کز حسن رویش</p></div>
<div class="m2"><p>بجان خورشید و مه خفاش کویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلعلش گوشه گیری آب حیوان</p></div>
<div class="m2"><p>زچهرش خوشه چینی باغ رضوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فروغ طور از رویش درخشی</p></div>
<div class="m2"><p>ید بیضا بدستش جزیه بخشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زقامت در قبا بالنده سروی</p></div>
<div class="m2"><p>چه سروی کانبیا (؟) مفتون تذروی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صباح عیدش از رخ غم نهادی</p></div>
<div class="m2"><p>شب قدرش زگیسو خانه زادی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دید از کید چرخ وکین دشمن</p></div>
<div class="m2"><p>پدر رامانده یکتا همچو ذوالمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زبی آبی شده از جسم تابش</p></div>
<div class="m2"><p>زتاب غم روان از چشم آبش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بقتلش نیز خیلی دیوزاده</p></div>
<div class="m2"><p>کمر بربسته و بازو گشاده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان آن غیرت الله مشتعل شد</p></div>
<div class="m2"><p>که برق از اشتعال خود خجل شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بعزم رزم تا نزد پدر رفت</p></div>
<div class="m2"><p>پدر را هوش از و از بدر رفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زمین بوسید وگفت ایجان امکان</p></div>
<div class="m2"><p>وجودت واجب ایوان امکان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قضا خالیگر خدام بزمت</p></div>
<div class="m2"><p>قدر سیلی خور ابطال رزمت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تن من بر روان زاندوه تنگ است</p></div>
<div class="m2"><p>دلمرا آرزوی اذن جنگ است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا فانی کن اندر خویش با لذات</p></div>
<div class="m2"><p>که التوحید اسقاط الاضافات</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ترا تاکی غریب وزار بینم</p></div>
<div class="m2"><p>بچشمت روز روشن تار بینم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو شاهین این چنین سرگرم کین دید</p></div>
<div class="m2"><p>بدور چشمش ازغم اشک غلطید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بناچار آنگهش اذن جدل داد</p></div>
<div class="m2"><p>وزین برد او بحق عزوجل داد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که ای دانای هر رازی کماهی</p></div>
<div class="m2"><p>بر این قوم از تو میخواهم گواهی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روان کردم کسی براین معسکر</p></div>
<div class="m2"><p>که بد اشبه زخلقت بر پیمبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو ما را شوق دیدار نبی بود</p></div>
<div class="m2"><p>زدیدارش دل فرسوده آسود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ولی اکبر بد انسان شور کین داشت</p></div>
<div class="m2"><p>که نه جا بر فلک نه بر زمین داشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فرو پوشیده خفتانی بقامت</p></div>
<div class="m2"><p>که بر پا شد ار آن قامت قیامت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حمایل کرد تیغی بریسارش</p></div>
<div class="m2"><p>که بد مریخ کمتر جان نثارش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بخواند اسب عقاب برنشستش</p></div>
<div class="m2"><p>عقاب چرخ شد سرعت پرستش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زحل میخواست تا گیرد رکابش</p></div>
<div class="m2"><p>ولی دل باخت از بیم عتابش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپهرش رفت کآید غاشیه کش</p></div>
<div class="m2"><p>ولی از صولتش افتاد در غش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدین شایستگی شد تا بهیجا</p></div>
<div class="m2"><p>و ازو هیجا بگردون جست ملجا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چوشد مردانه نزد آن عجایز</p></div>
<div class="m2"><p>ندا در داد برهل من مبارز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>توگفتی کاندر آن پیکارکس نیست</p></div>
<div class="m2"><p>وگر هست اندر از بیمش نفس نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برآن خورشید عارض مات گشتند</p></div>
<div class="m2"><p>پراکنده تر از ذرات گشتند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو دید آن شاهزاده نی هماورد</p></div>
<div class="m2"><p>برون آورد تیغ و جست ناورد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زبس افکند از آن اشرار کشته</p></div>
<div class="m2"><p>عیان شد هر طرف از کشته پشته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر چه زویلان را تاب و تب بود</p></div>
<div class="m2"><p>ولی افسوس کافزون تشنه لب بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عنان پیچید سوز تشنه کامیش</p></div>
<div class="m2"><p>بسوی خضر جان باب گرامیش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بگفت ای صد محیطت در هر انگشت</p></div>
<div class="m2"><p>علی اکبرت را تشنگی کشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرا سنگینی آهن برافروخت</p></div>
<div class="m2"><p>دلم از تف خورشید و عطش سوخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شهش گفت ای پدر قربان جانت</p></div>
<div class="m2"><p>بنه اندر دهان من زبانت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بخاتم نیز دادش قوت وقوت</p></div>
<div class="m2"><p>که الفت بد عقیقش را بیاقوت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دوباره عزم پرخاش عدو کرد</p></div>
<div class="m2"><p>رجز خوان از حقایق گفتگو کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بهر سو کز حسامش آتش انگیخت</p></div>
<div class="m2"><p>سر از تن بد که چون برگ خزان ریخت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گرفتندش سپه اندر میانه</p></div>
<div class="m2"><p>تنش شد تیر اعدا را نشانه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بناگه منقذبن مره دون</p></div>
<div class="m2"><p>عمودش کوفت برفرق همایون</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زبی تابی بیال اسب آویخت</p></div>
<div class="m2"><p>فلک بین اسب او در خصم بگریخت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بقلب دشمن بد قلب بردش</p></div>
<div class="m2"><p>بدست جم فکن دیوان سپردش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زدندش آنقدر با تیغ و ناوک</p></div>
<div class="m2"><p>که شد صد پاره آن اندام نازک</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو کار از حد وسیل ازسد برون شد</p></div>
<div class="m2"><p>پدر را خواند و از اسبش نگون شد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شهنشه اشک ریزان تاخت سویش</p></div>
<div class="m2"><p>باشک از روی و مو شد گرد شویش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بگفت ای از رخ قد خلد و طوبی</p></div>
<div class="m2"><p>پس از تو خاک غم برفرق دنیا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بقتلت دست شستند از خداوند</p></div>
<div class="m2"><p>خدا پیوند شان برد ز پیوند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تنی زاهل خبر گوید که یک زن</p></div>
<div class="m2"><p>دوید از خیمه گه بیرون بشیون</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ولی من را نشاید داد فتوی</p></div>
<div class="m2"><p>که زینب بود آن یا ام لیلی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شها جیحون که شد با تو درونش</p></div>
<div class="m2"><p>بهر حال از محن آور برونش</p></div></div>