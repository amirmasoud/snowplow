---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>دوش درخواب همی خنجر جانان دیدم</p></div>
<div class="m2"><p>تشنه خوابیدم و سرچشمه حیوان دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو هلالیست بخورشید جمالت ابرو</p></div>
<div class="m2"><p>که کمالش همه پیوسته بنقصان دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راستی ازذقنت عاقبتم پشت خمید</p></div>
<div class="m2"><p>وه که ازگوی تومن لطمه چوگان دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رست از خاتم لعل تو خط واگه باش</p></div>
<div class="m2"><p>کاندرین مور سر ملک سلیمان دیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که بیعقوب دهد مژه یوسف را باز</p></div>
<div class="m2"><p>که منش زنده در آن چاه زنخدان دیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زد گریبان مرا چاک و دل ازسینه ربود</p></div>
<div class="m2"><p>آن نکو سینه کزان چاک گریبان دیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چمن کرد چو خط رخش جیحون وصف</p></div>
<div class="m2"><p>عرق از شرم بروی گل و ریحان دیدم</p></div></div>