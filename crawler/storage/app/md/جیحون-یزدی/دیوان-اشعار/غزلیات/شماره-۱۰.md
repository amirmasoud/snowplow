---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>آنکه عشاق بود بنده رخسار بدیعش</p></div>
<div class="m2"><p>سعی ما با خط او نقش برآبست جمعیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس ندانم که نخواهد بود آن ترک مطاعش</p></div>
<div class="m2"><p>دل نباشد که نیابی برآنشوخ مطیعش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه را چهر تو منظور چه زحمت زخزانش</p></div>
<div class="m2"><p>هرکه را وصل تو مقدور چه حاجت بربیعش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب ازکوی خرابات چه خیزد که بشوخی</p></div>
<div class="m2"><p>ینجه بازاهد صد ساله زند طفل رضیعش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه از بند بگریخت نیابند پناهش</p></div>
<div class="m2"><p>هر که از چشم تو افتاد نجویند شفیعش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندهی گوش گر از مهر تو برناله جیحون</p></div>
<div class="m2"><p>باری اندیشه کن ازسطوت شهزاده رفیعش</p></div></div>