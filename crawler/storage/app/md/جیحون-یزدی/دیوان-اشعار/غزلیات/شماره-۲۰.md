---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>بر بوی آن دلارام طبعم گرفته خوئی</p></div>
<div class="m2"><p>کز خیل ماهرویان من قانعم ببوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شانه از دو زلفش چنگ هوس رهاکن</p></div>
<div class="m2"><p>کاینجاست عمر عشاق بسته بتار موئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زینسان که شکل قدت در چشم من نشسته</p></div>
<div class="m2"><p>هرگز چنین نخیزد سروی کنار جوئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد تن بکوی خمار شد خاک از قدح خوار</p></div>
<div class="m2"><p>تا قرعه سعادت سازد که را سبوئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آفتاب رویت ماه چهارده تافت</p></div>
<div class="m2"><p>بنگر زسست عقلی دارد چه سخت روئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز من که میتواند بوست بجان خریدن</p></div>
<div class="m2"><p>کاین لقمه از بزرگی گیرد بهر گلوئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر شبان تاریک جیحون دو چیز خواهد</p></div>
<div class="m2"><p>هم روی ماهتابی هم ماهتاب روئی</p></div></div>