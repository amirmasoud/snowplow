---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>کیست آن لعبت مغرور که رعناست زبس</p></div>
<div class="m2"><p>همه کس را سر او هست و درونی سرکس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من برآنم که ره عشق تو گیرم در پیش</p></div>
<div class="m2"><p>گر بدانم که دو صد غالیه دارد در پس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار اگر نیست وفادار چه فرق از اغیار</p></div>
<div class="m2"><p>باغ اگر نیست طربزا چه تفاوت زقفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند گوئی نفسی باش زمعشوق صبور</p></div>
<div class="m2"><p>کی بعشاق بود دور زمعشوق نفس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر رخ صافی تو رنگ بماند زنگاه</p></div>
<div class="m2"><p>برتن نازک تو خاز خلد از اطلس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا دلم عاشق رخسار توشد نشناسم</p></div>
<div class="m2"><p>بند از پند و نشاط ازغم و نسرین از خس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه را گر هوس از تست مرا عشق ازتست</p></div>
<div class="m2"><p>چکنم طفلی و نشنا خته عشق و هوس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلعجب بین که چو زد شمع توام شعله بجان</p></div>
<div class="m2"><p>رود از دیده جیحون همه دم رود ارس</p></div></div>