---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>چشمت به تیر غمزه دلم را نشانه کرد</p></div>
<div class="m2"><p>این لطف هم که کرد بمستی بهانه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد حدیث حور کند ای پسر می آر</p></div>
<div class="m2"><p>مردانگی نداشت خیالی زنانه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پیر میفروش کرامت عجب مدار</p></div>
<div class="m2"><p>عمری بصدق خدمت این آستانه کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دین و دل بگندم خالت دهم چه باک</p></div>
<div class="m2"><p>آدم بهشت بر سراین گونه دانه کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیشب حکایت از سر زلف نمود دل</p></div>
<div class="m2"><p>شب را دراز دید و هوای فسانه کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردا که پیکهای مرا حسن آن نگار</p></div>
<div class="m2"><p>دیوانه کرد و باز بسویم روانه کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقش رخ تو از دل جیحون نمیرود</p></div>
<div class="m2"><p>این طرفه آتشی است که درآب خانه کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آفاق را چو کلک ملکزاده مشک بیخت</p></div>
<div class="m2"><p>هرگه شکنج زلف ترا باد شانه کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم چراغ داد محمد رفیع راد</p></div>
<div class="m2"><p>کز نظم خویش حلقه به گوش زمانه کرد</p></div></div>