---
title: >-
    شمارهٔ ۱۲ - در تهنیت عید قربان
---
# شمارهٔ ۱۲ - در تهنیت عید قربان

<div class="b" id="bn1"><div class="m1"><p>عید قربان بود ای لعبت شوخ سپهی</p></div>
<div class="m2"><p>راست خیز از پی احرام و بنه کج کلهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز صفا هر وله آموز بدان سرو سهی</p></div>
<div class="m2"><p>رخ فرو سا بحجر تابری از وی سیهی</p></div></div>
<div class="b2" id="bn3"><p>همچو کز نقره شود بذل سپیدی بمحک</p></div>
<div class="b" id="bn4"><div class="m1"><p>ایکه برد ازکف ما صبر تو مفتاح فرج</p></div>
<div class="m2"><p>بتولای تو رستیم زتکلیف و هرج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاجیانرا بپرستیدن بیت از تو حجج</p></div>
<div class="m2"><p>کس مناسک نشناسد زتو امسال بحج</p></div></div>
<div class="b2" id="bn6"><p>محرم کعبه شوند از همه افواج ملک</p></div>
<div class="b" id="bn7"><div class="m1"><p>عمل حلقه ما با تو زحیرت تکبیر</p></div>
<div class="m2"><p>دست در حلقه زدن حک شده از لوح ضمیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حلقه وش بی سر و پائیم و سرا پا تقصیر</p></div>
<div class="m2"><p>حلق خلقی چو در آن حلقه مویست اسیر</p></div></div>
<div class="b2" id="bn9"><p>سزد ارگشته زدل حلقه بیت الله حک</p></div>
<div class="b" id="bn10"><div class="m1"><p>طفلی و با توکه فرمود بیا هر وله کن</p></div>
<div class="m2"><p>نیست حج واجبت از شیخ برو مسئله کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا زما دور شو و طی چنین مرحله کن</p></div>
<div class="m2"><p>از رخی آفت حج رحم برین قافله کن</p></div></div>
<div class="b2" id="bn12"><p>کاندر آنجا که توئی زهد شود مستهلک</p></div>
<div class="b" id="bn13"><div class="m1"><p>بر چه مذهب تو مگر معتقد ای نوش لبی</p></div>
<div class="m2"><p>کآفت اهل منی زآن حرکات عجبی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مست اندر عرفات ازبط بنت العنبی</p></div>
<div class="m2"><p>ازعجم دست کشیده پی قتل عربی</p></div></div>
<div class="b2" id="bn15"><p>مرحبا تا چه کند حسن تو الله معک</p></div>
<div class="b" id="bn16"><div class="m1"><p>عشق لبهای تو بازار تذکر شکند</p></div>
<div class="m2"><p>ناز حسنت دل ما را بتصور شکند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>توبه را غمزه ات از روی تهور شکند</p></div>
<div class="m2"><p>دست جمال فتد گردن اشتر شکند</p></div></div>
<div class="b2" id="bn18"><p>که زحمل چو توئی برد زدلها مدرک</p></div>
<div class="b" id="bn19"><div class="m1"><p>فرقه بر سر وصل تو بهم در جدلند</p></div>
<div class="m2"><p>جوقه از غم هجرت بخیم معتزلند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در بر محملت احرار برقص جملند</p></div>
<div class="m2"><p>قومی آنسان بتماشای رخت مشتغلند</p></div></div>
<div class="b2" id="bn21"><p>که نسازند طواف حرم حق بکتک</p></div>
<div class="b" id="bn22"><div class="m1"><p>بنه ای ترک پسر خرقه تدلیس بجا</p></div>
<div class="m2"><p>زاهدانرا بدل از شاهدی انداز فجا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خوف عشاق بدل کن زترقص برجا</p></div>
<div class="m2"><p>توکجا زهد کجا سبحه و دستار کجا</p></div></div>
<div class="b2" id="bn24"><p>درخور بوسی و بزم و طرب و جام کزک</p></div>
<div class="b" id="bn25"><div class="m1"><p>حمل جملی که بود هودجت ای مایه ناز</p></div>
<div class="m2"><p>نه عجب از طرب آید اگر اندر پرواز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ولی این پیشه بسوز و مئی اندیشه بساز (؟)</p></div>
<div class="m2"><p>تکیه گه چون کنی از خار مغیلان حجاز</p></div></div>
<div class="b2" id="bn27"><p>توکه رنجیدی اگر داشتی از گل تو شک</p></div>
<div class="b" id="bn28"><div class="m1"><p>ازتو زیبد که بدوش افکنی از زلف کمند</p></div>
<div class="m2"><p>خاصه اکنون که جهانده بجهان عید سمند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خیز و از چهره ستان باج زترکان خجند</p></div>
<div class="m2"><p>قبله خود بجز از درگه آصف مپسند</p></div></div>
<div class="b2" id="bn30"><p>تا زمین بوسیت از مهرکند ماه فلک</p></div>
<div class="b" id="bn31"><div class="m1"><p>آن وزیریکه بشه بست دل و از خود رست</p></div>
<div class="m2"><p>کمرش را ملک اندر سعد الملکی بست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ظلم برخاست زکیهان چو وی ازعدل نشست</p></div>
<div class="m2"><p>تا که بردست وزارت قلم آورد بدست</p></div></div>
<div class="b2" id="bn33"><p>پای مردم نتراود زحسام و بیلک</p></div>
<div class="b" id="bn34"><div class="m1"><p>کار آفاق رواج ازهمم کافی داد</p></div>
<div class="m2"><p>نظم جمهور بپردانی و کم لافی داد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>درد هر ناحیه را داروی بس شافی داد</p></div>
<div class="m2"><p>عزتش حق زبرازنده دل صافی داد</p></div></div>
<div class="b2" id="bn36"><p>کس زحق عزت نگرفته بزور و بکمک</p></div>
<div class="b" id="bn37"><div class="m1"><p>ایکه حکم تو چو تقدیر روان برافلاک</p></div>
<div class="m2"><p>گوهرت جرعه کش از موجه بحر لولاک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رنجه در پهنه قدرت قدم استدراک</p></div>
<div class="m2"><p>شرفه قصر جلال تو فراتر ز سماک</p></div></div>
<div class="b2" id="bn39"><p>پایه کاخ شکوه تو فروتر ز سمک</p></div>
<div class="b" id="bn40"><div class="m1"><p>هرچه کلک تو نگارد ز رشاقت افصح</p></div>
<div class="m2"><p>زده اقوال تو از خمکده وحی قدح</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گیتی اندر زمنت مبشر از قد افلح</p></div>
<div class="m2"><p>دوستان را که بود از درجات تو فرح</p></div></div>
<div class="b2" id="bn42"><p>دشمن ار دیدن آن را نتواند بدرک</p></div>
<div class="b" id="bn43"><div class="m1"><p>آصفا خاطر جیحون ز تالم فرسود</p></div>
<div class="m2"><p>گرچه اغلب ز عطایت بتنعم آسود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چه غم ار ملکت موروث مرا خصم ربود</p></div>
<div class="m2"><p>کز بتول آنکه خداوندی جیحونش بود</p></div></div>
<div class="b2" id="bn45"><p>کرد با حیله و تزویر رمع غضب فدک</p></div>
<div class="b" id="bn46"><div class="m1"><p>آدمی زاده گر از قرض برآوردی دم</p></div>
<div class="m2"><p>اینک آفاق بزیر دم من گشتی گم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ولی از شرم تو خاموشم و خون خور چون خم</p></div>
<div class="m2"><p>گر رسیدم بری و رسته شدم زین مردم</p></div></div>
<div class="b2" id="bn48"><p>زیر دم خروشان می نهم از هجو خسک</p></div>
<div class="b" id="bn49"><div class="m1"><p>تا فلک دور زند یکسره دوران تو باد</p></div>
<div class="m2"><p>تا بود کیوان چوبک زن ایوان تو باد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا چمد مهر چو گو درخم چوگان توباد</p></div>
<div class="m2"><p>جان احباب خصوصا من قربان توباد</p></div></div>
<div class="b2" id="bn51"><p>بدسگالت نشود از غم و اندوه منفک</p></div>