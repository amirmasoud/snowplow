---
title: >-
    شمارهٔ ۲۱ - وله
---
# شمارهٔ ۲۱ - وله

<div class="b" id="bn1"><div class="m1"><p>ای پسری کز جمال خلعت نازت بتن</p></div>
<div class="m2"><p>جان مرا تازه کن از آن شراب کهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که خلعت شه رسید زری بوجه حسن</p></div>
<div class="m2"><p>زآصفی طلعت است پر از فر ذوالمنن</p></div></div>
<div class="b2" id="bn3"><p>بمحضرش برفراز زقامتت نارون</p>
<p>بمقدمش برفروز زچهره ات مجمره</p></div>
<div class="b" id="bn4"><div class="m1"><p>گاه شرابست و بس ای قمر می پرست</p></div>
<div class="m2"><p>ما حصل عمر ما گردون برتاک بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو دانی احوال من که بایدم بود مست</p></div>
<div class="m2"><p>ساعتی از مستیم ندهم بر هر چه هست</p></div></div>
<div class="b2" id="bn6"><p>بویژه کاینک مرا بهانه آمد بدست</p>
<p>که آصف آراست تن بخلعتی فاخره</p></div>
<div class="b" id="bn7"><div class="m1"><p>از عجم اکنون نواست تا بعراق عرب</p></div>
<div class="m2"><p>از حسد شور ماست جان مخالف بلب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیک همایون شاه کوفت حصار کرب</p></div>
<div class="m2"><p>راه نشابور خواست ریزد یا للعجب</p></div></div>
<div class="b2" id="bn9"><p>ای حسن اخلاق ترک در این حسینی طرب</p>
<p>نغمه ناقوس ران از آن نکو حنجره</p></div>
<div class="b" id="bn10"><div class="m1"><p>بتابر افسانه ات تا کی سامع شوم</p></div>
<div class="m2"><p>یا که پی وصل تو چه قدر تابع شوم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تحمل ازحد گذشت حال منازع شوم</p></div>
<div class="m2"><p>چه مانده از آب رو که باز ضایع شوم</p></div></div>
<div class="b2" id="bn12"><p>مشنو کامروز هم ببوسه قانع شوم</p>
<p>چو کار بر رندی است به که شود یکسره</p></div>
<div class="b" id="bn13"><div class="m1"><p>دراین بهشتی بساط باتو غنودن خوش است</p></div>
<div class="m2"><p>زکوثر آسا نبیذ بعیش بودن خوش است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زطره و جامه ات گره گشودن خوش است</p></div>
<div class="m2"><p>بروز از پیکرت نور فزودن خوش است</p></div></div>
<div class="b2" id="bn15"><p>بیاد غلمان ترا بوسه نمودن خوش است</p>
<p>که بر حقیقت بود مجاز چون قنطره</p></div>
<div class="b" id="bn16"><div class="m1"><p>چو رویت از نقش بت گروه بتکّر کنند</p></div>
<div class="m2"><p>پیش بت اسلامیان سجده چو کافر کنند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرچه رخت را شبیه خلق بآذر کنند</p></div>
<div class="m2"><p>لیک ندانم چرا دامن از او ترکنند</p></div></div>
<div class="b2" id="bn18"><p>چه مایه مردم که پشت پیش تو چنبر کنند</p>
<p>چنین که برمه زده است مشک خطت چنبره</p></div>
<div class="b" id="bn19"><div class="m1"><p>ایزد روی ترا در خور بوس آفرید</p></div>
<div class="m2"><p>وگرنه روز نخست روی مرا نیز دید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حال بیا رام شو بدون گفت و شنید</p></div>
<div class="m2"><p>که قامتم چون هلال ز ابروانت خمید</p></div></div>
<div class="b2" id="bn21"><p>عشق لبت عاقبت بگرد من خط کشید</p>
<p>مگر که این نقطه راست خاصیت دایره</p></div>
<div class="b" id="bn22"><div class="m1"><p>باری ای برمهت زموی فتان عبیر</p></div>
<div class="m2"><p>وز بدن نازکت درون کتان حریر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زباده و ساده ات اکنون لابد گزیر</p></div>
<div class="m2"><p>ولی باسب اندر آی پذیره را در پذیر</p></div></div>
<div class="b2" id="bn24"><p>که بر تماشای جشن وز پی تشریف میر</p>
<p>هرسومه منظریست نشسته بر منظره</p></div>
<div class="b" id="bn25"><div class="m1"><p>وزیر انجم حشم مصدر انصاف و داد</p></div>
<div class="m2"><p>که رسم لغزش ربود بنای دانش نهاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هم باکابر مجیر هم باصاغر ملاذ</p></div>
<div class="m2"><p>سعدالملک ملک صاحب قدسی نژاد</p></div></div>
<div class="b2" id="bn27"><p>بدر سپهر وقار حسینقلی خان راد</p>
<p>که عقل با رای اوست چو پیش خور شب پره</p></div>
<div class="b" id="bn28"><div class="m1"><p>در نظر همتش بلندی چرخ پست</p></div>
<div class="m2"><p>خاست زکیهان فتن چو او بمسند نشست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باسخطش کوه را درکمر افتد شکست</p></div>
<div class="m2"><p>هرکه بوی بسته شد زقید افات رست</p></div></div>
<div class="b2" id="bn30"><p>فلک بکاخش بود شادروانی که هست</p>
<p>مجره اش ریسمان ماه نوش غرغره</p></div>
<div class="b" id="bn31"><div class="m1"><p>کسیکه با صد زبان بهر فنش گفتگوست</p></div>
<div class="m2"><p>چون که بر او رسد مغز نداند ز پوست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برسر خوانش بلند صوت کلوا و اشربواست</p></div>
<div class="m2"><p>لیک منادیش را شرم زلاتسرفواست</p></div></div>
<div class="b2" id="bn33"><p>قسوره غاب ملک تا که وجودی چو اوست</p>
<p>دشمن روباه وش فرت من قسوره</p></div>
<div class="b" id="bn34"><div class="m1"><p>زابر تا که کف و گاه عطای عمیم</p></div>
<div class="m2"><p>مباینتها بود بنزد ذوق سلیم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سحاب ریزد مطر وی بخشد زر و سیم</p></div>
<div class="m2"><p>آن بخروشی شگفت این بسکوتی عظیم</p></div></div>
<div class="b2" id="bn36"><p>حقیقت جود اوست زجوش طبع سلیم</p>
<p>باشد اگر اصل ابر تصاعد ابخره</p></div>
<div class="b" id="bn37"><div class="m1"><p>ای ز چو تو آصفی نازان کیهان خدا</p></div>
<div class="m2"><p>برادرت راست نیز چون تو فری جدا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زین دو تن متحد حاجت مردم روا</p></div>
<div class="m2"><p>چنانکه یابند خلق از حسنین اهتدا</p></div></div>
<div class="b2" id="bn39"><p>بر تو نماید سپهر بمشکلات اقتدا</p>
<p>با او سازد خرد بکارها مشوره</p></div>
<div class="b" id="bn40"><div class="m1"><p>زکلک تو گاه بزم بالد اکلیل و تخت</p></div>
<div class="m2"><p>زگرز او وقت رزم کوه شود لخت لخت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خامه تو طیس را بهم نوردیده رخت</p></div>
<div class="m2"><p>نیزه او جیش را نصرت بخشاد رخت</p></div></div>
<div class="b2" id="bn42"><p>زفاضل جاه تو است موجود اقبال و بخت</p>
<p>زحشو انعام اوست معدوم آز و شره</p></div>
<div class="b" id="bn43"><div class="m1"><p>زمهر و قهر تو زاد عوالم خیر و شر</p></div>
<div class="m2"><p>ربغض الطاف اوست مبانی نفع و ضر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شمسه ایوان تو مناص شمس و قمر</p></div>
<div class="m2"><p>قبه خرگاه او ملجا فتح و ظفر</p></div></div>
<div class="b2" id="bn45"><p>گیتی کوی ترا مقیم در رهگذر</p>
<p>گردون قصر ورا ساجد برکنگره</p></div>
<div class="b" id="bn46"><div class="m1"><p>دست زر افشان تو معطی برخاص و عام</p></div>
<div class="m2"><p>تیغ سرافشان او قوت اهل نظام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دامن حزم تو را سپهر در اعتصام</p></div>
<div class="m2"><p>توسن عزم ور استاره زیب لگام</p></div></div>
<div class="b2" id="bn48"><p>ز پخته افکار تو هر چه بجز وحی خام</p>
<p>با دل آگاه او پیر خرد مسخره</p></div>
<div class="b" id="bn49"><div class="m1"><p>نقود ابذال تو وقف رشید و رضیع</p></div>
<div class="m2"><p>لوای اجلال او عون شریف و وضیع</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دلی ترا همچو او بهر مقامی وسیع</p></div>
<div class="m2"><p>طبعی او را چو تو در همه حالی منیع</p></div></div>
<div class="b2" id="bn51"><p>او را باشد بذات مقتضیاتی بدیع</p>
<p>ترا بود در صفات خصایل نادره</p></div>
<div class="b" id="bn52"><div class="m1"><p>تا دهد آفاق را خلعت نور آفتاب</p></div>
<div class="m2"><p>تا بتمامی نجوم نیاید اندر حساب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تاکه فشاند مطر وقت بهاران سحاب</p></div>
<div class="m2"><p>تا کند از تیرضو رجم شیاطین شهاب</p></div></div>
<div class="b2" id="bn54"><p>خیام فر ترا عز موبد طناب</p>
<p>وثاق عزورا چرخ برین پنجره</p></div>