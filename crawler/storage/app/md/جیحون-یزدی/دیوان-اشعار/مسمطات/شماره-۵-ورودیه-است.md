---
title: >-
    شمارهٔ ۵ - ورودیه است
---
# شمارهٔ ۵ - ورودیه است

<div class="b" id="bn1"><div class="m1"><p>خیر مقدم بخرام ای بت سیمین صدرا</p></div>
<div class="m2"><p>زادک الله تعالی شرفا والقدرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ نما تا ببری ازسر خوبان غدرا</p></div>
<div class="m2"><p>رفته همچو هلال آمده چون بدرا</p></div></div>
<div class="b2" id="bn3"><p>آب ری خوب نشستت بمزاج و هاج</p></div>
<div class="b" id="bn4"><div class="m1"><p>برسم اسب تو سرها بتراکم نگرم</p></div>
<div class="m2"><p>در پیت دیده رندان ری و قم نگرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمت آلوده بخونریزی مردم نگرم</p></div>
<div class="m2"><p>دل خود را بخم طره تو گم نگرم</p></div></div>
<div class="b2" id="bn6"><p>بسکه هرگونه دل آورده ای اندر تاراج</p></div>
<div class="b" id="bn7"><div class="m1"><p>سخت عاشق کش و افروخته خد آمده ای</p></div>
<div class="m2"><p>از کدامین در جنت بچه حد آمده ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفته ای همچو غزالان و اسد آمده ای</p></div>
<div class="m2"><p>زآن منازل که تو با آن رخ و قد آمده ای</p></div></div>
<div class="b2" id="bn9"><p>سرو و نسرین برد امسال ملک جای خراج</p></div>
<div class="b" id="bn10"><div class="m1"><p>شاه را سست وفا یا تو فریبیدی سخت</p></div>
<div class="m2"><p>ورنه بودش بکف از طره تودامن بخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>علم قد بلندت ظفر انگیز درخت</p></div>
<div class="m2"><p>هوس تخت گرش بود تو را سینه چو تخت</p></div></div>
<div class="b2" id="bn12"><p>طلب تاج گرش بود تو را زلف چو تاج</p></div>
<div class="b" id="bn13"><div class="m1"><p>شد ورودت زچه رو بیخبر ای فتنه گل</p></div>
<div class="m2"><p>تا بسوزم زخورت مجمره گرد کاکل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مژده زلف ترا باز دهم برسنبل</p></div>
<div class="m2"><p>کوی توآب زنم از عرق چهره گل</p></div></div>
<div class="b2" id="bn15"><p>ره تو فرش نمایم ز پرند و دیباج</p></div>
<div class="b" id="bn16"><div class="m1"><p>حالیا رنجه از راه بکش جوشن سیم</p></div>
<div class="m2"><p>جستجو کم کن از اقوام که الملک عقیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>می ذخیره است مرا بهر تو از عهد قدیم</p></div>
<div class="m2"><p>بنشین فارغ و می خورکه بفتوای حکیم</p></div></div>
<div class="b2" id="bn18"><p>خستگی را بجز از می نبود هیچ علاج</p></div>
<div class="b" id="bn19"><div class="m1"><p>نی غلط گفتم ای ماه زشرمت اخرس</p></div>
<div class="m2"><p>تو بهمراه امیر آمدی این عیشت بس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آسمانراست بدین منزلت و قدر هوس</p></div>
<div class="m2"><p>خدمت میر چو خلد است و بخدا اندرکس</p></div></div>
<div class="b2" id="bn21"><p>نشود خسته دل از رنج تن و سوء مزاج</p></div>
<div class="b" id="bn22"><div class="m1"><p>چون رها سخت کمان تیر خود از شست کند</p></div>
<div class="m2"><p>گذر اندر جگر شیر نر مست کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیست را همت مردانه او هست کند</p></div>
<div class="m2"><p>گرز او اوج حصین حصن عدوپست کند</p></div></div>
<div class="b2" id="bn24"><p>گر فراتر ز بروج فلک استش ابراج</p></div>
<div class="b" id="bn25"><div class="m1"><p>ایکه بر خلق بهین دور جهان دوره تست</p></div>
<div class="m2"><p>دوست را پایه زتو سخت شد و دشمن سست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از تو گردید شکست همه آفاق درست</p></div>
<div class="m2"><p>از برتخت ملک آمده به زنخست</p></div></div>
<div class="b2" id="bn27"><p>همچو احمد که به آمد زنخست از معراج</p></div>