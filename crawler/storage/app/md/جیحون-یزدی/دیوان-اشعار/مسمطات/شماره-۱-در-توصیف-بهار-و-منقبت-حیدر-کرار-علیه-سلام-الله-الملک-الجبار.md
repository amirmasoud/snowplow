---
title: >-
    شمارهٔ ۱ - در توصیف بهار و منقبت حیدر کرار علیه سلام الله الملک الجبار
---
# شمارهٔ ۱ - در توصیف بهار و منقبت حیدر کرار علیه سلام الله الملک الجبار

<div class="b" id="bn1"><div class="m1"><p>باز جهان از بهار مژده رحمت شنفت</p></div>
<div class="m2"><p>بلبل رطب اللسان تهنیت از باغ گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشرت بنشسته خاست فتنه بیدار خفت</p></div>
<div class="m2"><p>پرده نشین غنچه را چو باد از هم شکفت</p></div></div>
<div class="b2" id="bn3"><p>گشت زشوخی طبع شاهد بازارها</p></div>
<div class="b" id="bn4"><div class="m1"><p>بازهمی بوی مشک زجویبار آیدم</p></div>
<div class="m2"><p>صحبت گل در میان زهر کنار آیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمزمه مرغ زار زمرغزار آیدم</p></div>
<div class="m2"><p>قهقه کبک مست زکوهسار آیدم</p></div></div>
<div class="b2" id="bn6"><p>گوئی بارد نشاط از در و دیوارها</p></div>
<div class="b" id="bn7"><div class="m1"><p>طوطی کرده ببر جامه زنگارگون</p></div>
<div class="m2"><p>ساخته منقار خویش رنگ بشنگرف و خون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاهی گوید سخن چو مردمی ذوفنون</p></div>
<div class="m2"><p>گاه سراید سرود چو مطربی پرفسون</p></div></div>
<div class="b2" id="bn9"><p>مرغ که دید این چنین شهره بگفتارها</p></div>
<div class="b" id="bn10"><div class="m1"><p>تذر و پر ریخته باز پر آورده است</p></div>
<div class="m2"><p>وز پر نورس هزار نقش بر آورده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرمه بچشم اندر از مشک تر آورده است</p></div>
<div class="m2"><p>پیرهنی در براز سیم و زر آورده است</p></div></div>
<div class="b2" id="bn12"><p>جلوه زطاوس برد بنغز رفتارها</p></div>
<div class="b" id="bn13"><div class="m1"><p>هین دم طاوس را پر مه و خورشید بین</p></div>
<div class="m2"><p>بالش بالنده تر زچتر جمشید بین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بتارکش از پرند افسر جاوید بین</p></div>
<div class="m2"><p>بافسرش پر چند بفرناهید بین</p></div></div>
<div class="b2" id="bn15"><p>لیک زناجنس پای درگل او خارها</p></div>
<div class="b" id="bn16"><div class="m1"><p>فاختگان چون بباغ داد به کوکو زنند</p></div>
<div class="m2"><p>گوئی بغدادیان کوس هلاکو زنند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لشکر دی را زکشت خیمه برونسو زنند</p></div>
<div class="m2"><p>سناجق سرو را جائی دلجو زنند</p></div></div>
<div class="b2" id="bn18"><p>کوبند اطراف وی زسبزه مسمارها</p></div>
<div class="b" id="bn19"><div class="m1"><p>هدهد شیاد را گرم تکاپو نگر</p></div>
<div class="m2"><p>جانب بلقیس گل رفته بجادو نگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بزم سلیمان سرو زو به هیاهو نگر</p></div>
<div class="m2"><p>پر از نقط نامه ایش بسته ببازو نگر</p></div></div>
<div class="b2" id="bn21"><p>شکسته طرف کله بسان عیارها</p></div>
<div class="b" id="bn22"><div class="m1"><p>بلبل شوریده راست هر گه شوری دگر</p></div>
<div class="m2"><p>وزگل سوری وراست هر دم سوری دگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ترنمش را به طبع بود سروری دگر</p></div>
<div class="m2"><p>مانا در زاریش نهفته زوری دگر</p></div></div>
<div class="b2" id="bn24"><p>آری دلکش تر است لحن گرفتارها</p></div>
<div class="b" id="bn25"><div class="m1"><p>نرگس بیمار باز قد بفرازد همی</p></div>
<div class="m2"><p>تکیه زنان برعصا بسبزه تازد همی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وز فر دینار چند برگل نازد همی</p></div>
<div class="m2"><p>همال رندان مست عربده سازد همی</p></div></div>
<div class="b2" id="bn27"><p>عربده ناید اگر هیچ ز بیمارها</p></div>
<div class="b" id="bn28"><div class="m1"><p>بنفشه آمد بباغ دو رفته از بهمنا</p></div>
<div class="m2"><p>چون یک عالم پری از پس اهریمنا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بجشن پیشی گرفت برسمن و سوسنا</p></div>
<div class="m2"><p>زحله های بنفش بریده پیراهنا</p></div></div>
<div class="b2" id="bn30"><p>ز پرنیانهای سبز دوخته شلوارها</p></div>
<div class="b" id="bn31"><div class="m1"><p>لاله نوخیز دوش بگنج سلطان زده است</p></div>
<div class="m2"><p>زلعل و یاقوت ناب مال فراوان زده است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بهار کرد آشکار آنچه به پنهان زده است</p></div>
<div class="m2"><p>شحنه اردیش داغ بر دل و برجان زده است</p></div></div>
<div class="b2" id="bn33"><p>تا که بدزدی کند نزدش اقرارها</p></div>
<div class="b" id="bn34"><div class="m1"><p>زگوهر افشان سحاب برناگیهان پیر</p></div>
<div class="m2"><p>پران از رعب سیل سنگ بچرخ اثیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آب بزیر گیاه آینه اندر حریر</p></div>
<div class="m2"><p>برق بابرسیاه عکس فکن در غدیر</p></div></div>
<div class="b2" id="bn36"><p>چو ذوالفقار علی در دل غدارها</p></div>
<div class="b" id="bn37"><div class="m1"><p>شهیکه نام نکوش حیدر کرار شد</p></div>
<div class="m2"><p>زآهن صارم عدوش زیبق فرار شد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بخلوت کردگار محرم اسرار شد</p></div>
<div class="m2"><p>صفاتش از ذات حق مظهر انوار شد</p></div></div>
<div class="b2" id="bn39"><p>وحدتش از کثرتست نقطه پرگارها</p></div>
<div class="b" id="bn40"><div class="m1"><p>سرسوی سامان اوست رفته وآینده را</p></div>
<div class="m2"><p>دست بدامان اوست جمع و پراکنده را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در کف امرش زمام فانی و پاینده را</p></div>
<div class="m2"><p>زنده کند مرده را شهی دهد بنده را</p></div></div>
<div class="b2" id="bn42"><p>گشاید آسان زهم عقده دشوارها</p></div>
<div class="b" id="bn43"><div class="m1"><p>بلوح آگاهیش مجاری خوب و زشت</p></div>
<div class="m2"><p>آدم و ابلیس را از قلمش سرنوشت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زسطوت و رافتش خلقت نار و بهشت</p></div>
<div class="m2"><p>نیست بجز نام او در حرم و درکنشت</p></div></div>
<div class="b2" id="bn45"><p>کنند تسبیح او سبحه و زنارها</p></div>
<div class="b" id="bn46"><div class="m1"><p>مظاهر روی اوست هیاکل ما خلق</p></div>
<div class="m2"><p>راجع بر سوی اوست طوایف ماسبق</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدو نمودند راه پیمبران فرق</p></div>
<div class="m2"><p>حق نبود غیر او او نبود غیر حق</p></div></div>
<div class="b2" id="bn48"><p>دیده احول کند زین سخن انکارها</p></div>
<div class="b" id="bn49"><div class="m1"><p>ایکه خدائی رداست راست ببالای تو</p></div>
<div class="m2"><p>ریزه خورند انبیا زنطع آلای تو</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>طفل نیابد زمام مگر بامضای تو</p></div>
<div class="m2"><p>کس نرود از جهان مگر به یاسای تو</p></div></div>
<div class="b2" id="bn51"><p>که میکند جز خدای ازین نمط کارها</p></div>
<div class="b" id="bn52"><div class="m1"><p>بیک فقیر از عطا هزارکشور دهی</p></div>
<div class="m2"><p>به هریک از کشورش هزار لشکردهی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به هریک از لشکرش هزار افسر دهی</p></div>
<div class="m2"><p>همره هر افسرش هزار بجان و دل سردهی</p></div></div>
<div class="b2" id="bn54"><p>که طبع تو عاشق است بجود (و) ایثارها</p></div>
<div class="b" id="bn55"><div class="m1"><p>هم از نوازنده مهر خلد مخلد توئی</p></div>
<div class="m2"><p>هم از گذارنده قهر نار موبد تویی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بنوبت رزم و بزم صاحب سودد تویی</p></div>
<div class="m2"><p>روی خدائی ولیک پشت محمد تویی</p></div></div>
<div class="b2" id="bn57"><p>وان دگرش یارها لایق در غارها</p></div>
<div class="b" id="bn58"><div class="m1"><p>شها توئی کز همم عون و پناه منی</p></div>
<div class="m2"><p>در دو جهان ازکرم فخر (؟)گناه منی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>رو بکه آرم که تو دلیل راه منی</p></div>
<div class="m2"><p>بهر طریق اوفتم نجات خواه منی</p></div></div>
<div class="b2" id="bn60"><p>جز از تو جیحون ندید دیار و دیارها</p></div>
<div class="b" id="bn61"><div class="m1"><p>دانم مدح تو را زفکر من برتریست</p></div>
<div class="m2"><p>گرفتم از خود مرا بشعر پیغمبریست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مدایحت را ظهور زمصحف داوریست</p></div>
<div class="m2"><p>ولی بیاران تو چولافم از داوریست</p></div></div>
<div class="b2" id="bn63"><p>هریک از من ثنات کرده طلب بارها</p></div>
<div class="b" id="bn64"><div class="m1"><p>خاصه امیر فرخنده رای(؟)</p></div>
<div class="m2"><p>که رخشند از صورتش معنی کیهان خدای</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سایه رایات وی مایه فر همای</p></div>
<div class="m2"><p>به تیغ لشکر شکن بکلک کشورگشای</p></div></div>
<div class="b2" id="bn66"><p>فتح و ظفر را از اوست رخسارها</p></div>
<div class="b" id="bn67"><div class="m1"><p>سپهر از جان دخیل بپایه تخت او</p></div>
<div class="m2"><p>سست شود روزگار با نیت سخت او</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>البرز آرد تحف بگرز یک لخت او</p></div>
<div class="m2"><p>طالع دشمن نرست ز پنجه بخت او</p></div></div>
<div class="b2" id="bn69"><p>که خفتگان غافلند زحال بیدارها</p></div>
<div class="b" id="bn70"><div class="m1"><p>برزم گندآوران چو او همارود جوست</p></div>
<div class="m2"><p>صد فوج ار بنگری هر یک پیچان ازوست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نه پشت گرم از صدیق نه سرد دل از عدوست</p></div>
<div class="m2"><p>در نظرش رمح خصم ناوک مژگان دوست</p></div></div>
<div class="b2" id="bn72"><p>بگوش او کوس رزم نغمه مزمارها</p></div>
<div class="b" id="bn73"><div class="m1"><p>ای بتو ذوالمنن ختم جمال و جلال</p></div>
<div class="m2"><p>کرده قضا و قدر حکم ترا امتثال</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>پیش تو کم از اناث حشمت و جاه رجال</p></div>
<div class="m2"><p>قدر تو نارد بیاد کیفر بر بدسگال</p></div></div>
<div class="b2" id="bn75"><p>شیر نخواهد نمود طعمه زمردارها</p></div>
<div class="b" id="bn76"><div class="m1"><p>گاه سخا در برت خطه و اقلیم چیست</p></div>
<div class="m2"><p>طریف و تالد کدام سریر و دیهیم چیست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز دنیوی درگذر جنت و تسنیم چیست</p></div>
<div class="m2"><p>طبع تو نشناخته است لعل چه و سیم چیست</p></div></div>
<div class="b2" id="bn78"><p>لعل بخرمن دهی سیم بخروارها</p></div>
<div class="b" id="bn79"><div class="m1"><p>دادگرا تا مرا رست لبان از لبن</p></div>
<div class="m2"><p>گیتی ازگفته ام ساخت پر از در دهن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>مگر در اوصاف تو که ناید از من سخن</p></div>
<div class="m2"><p>پیش تو مزجاه شد بضاعتم لیک من</p></div></div>
<div class="b2" id="bn81"><p>خوشم که بریوسفم یک از خریدارها</p></div>
<div class="b" id="bn82"><div class="m1"><p>کیست که در افتخار زدل برد جوش من</p></div>
<div class="m2"><p>که حلقه بندگیت شد حلل گوش من</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>خصم نگیرد گرو زپهنه هوش من</p></div>
<div class="m2"><p>مال هم آغوش اوکمال همدوش من</p></div></div>
<div class="b2" id="bn84"><p>پشک از آن جعل مشک زعطارها</p></div>
<div class="b" id="bn85"><div class="m1"><p>تا ببهاران بود دور گل بسدی</p></div>
<div class="m2"><p>تا زنسایم شود دل بچمن مهتدی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تا بسپهر دمن لاله کند فرقدی</p></div>
<div class="m2"><p>زابر کفت ملک را خرمی سرمدی</p></div></div>
<div class="b2" id="bn87"><p>زچهرت آمال را شگفته گلزارها</p></div>