---
title: >-
    شمارهٔ ۱۸ - وله
---
# شمارهٔ ۱۸ - وله

<div class="b" id="bn1"><div class="m1"><p>ای مه که قد تست فتن زا قیامتی</p></div>
<div class="m2"><p>نی نی زقامت تو قیامت علامتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر رخ نمانده باز چو باب سلامتی</p></div>
<div class="m2"><p>بازم مجو بسجن سکندر اقامتی</p></div></div>
<div class="b2" id="bn3"><p>هل خیمه را فرود و بکش رخش زیر زین</p></div>
<div class="b" id="bn4"><div class="m1"><p>جائی که بازسخره عصفور می شود</p></div>
<div class="m2"><p>نار از زبانه طعنه زن نور می شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرد بصیر مسخره کور می شود</p></div>
<div class="m2"><p>بهرام کشته از لگدگور می شود</p></div></div>
<div class="b2" id="bn6"><p>نادان کند تمکین و کودن شود مکین</p></div>
<div class="b" id="bn7"><div class="m1"><p>هان ای زحل مگوی که من نحس اکبرم</p></div>
<div class="m2"><p>نجمت بسم توسن تربیع بسپرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بالله که در نحوست من از تو بدترم</p></div>
<div class="m2"><p>هش دار کز مقارنه تیره اخترم</p></div></div>
<div class="b2" id="bn9"><p>ناگاه محترق شودت چرخ هفتمین</p></div>
<div class="b" id="bn10"><div class="m1"><p>هان ای خجسته برجیس ای کز توفقت</p></div>
<div class="m2"><p>در کوکبی نیامده تاب تطابقت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کم عشوه کن نه مشتریم برتعلقت</p></div>
<div class="m2"><p>نارم پی قران سعادت تملقت</p></div></div>
<div class="b2" id="bn12"><p>گر صد هزار قرن و بالم بود قرین</p></div>
<div class="b" id="bn13"><div class="m1"><p>هان ای ستاره سوخته مریخ دشنه زن</p></div>
<div class="m2"><p>هر چند تیغ بازی این جا سپر فکن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رنجه مکن سرین خود از شاخ کرگدن</p></div>
<div class="m2"><p>گشتم سته بترس که این فرقدین من</p></div></div>
<div class="b2" id="bn15"><p>هم موی مژه اش شده مریخ آفرین</p></div>
<div class="b" id="bn16"><div class="m1"><p>هان ای سر بریده زتن خیره آفتاب</p></div>
<div class="m2"><p>کز زن مزاجی تو دوچارم در انقلاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس کش عنان و اینقدر از کین مزن رکاب</p></div>
<div class="m2"><p>من برهمن نیم که ز کیدت باضطراب</p></div></div>
<div class="b2" id="bn18"><p>سایم ز روی عجز بخاک درت جبین</p></div>
<div class="b" id="bn19"><div class="m1"><p>ای زهره شادزی که بد از من تو نشنوی</p></div>
<div class="m2"><p>با مطربان مرا بچه زهره است کجروی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هان دور دورتست بزن راه پهلوی</p></div>
<div class="m2"><p>نشگفت مطربی کند از جای خسروی</p></div></div>
<div class="b2" id="bn21"><p>این عهد اگر زدخمه درآید سبکتگین</p></div>
<div class="b" id="bn22"><div class="m1"><p>هان ای عطارد ای قلمت قاید پلیس</p></div>
<div class="m2"><p>رخساره ات گرفته تر ازخصلت خسیس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اکنون که جوش دیگ تو شد وقف کاسه لیس</p></div>
<div class="m2"><p>دژخیم ترنشین و براتم به یخ نویس</p></div></div>
<div class="b2" id="bn24"><p>هر چم شود نصیب زمحصول مآء وطین</p></div>
<div class="b" id="bn25"><div class="m1"><p>هان ای قمر عبث به پی غدر میشوی</p></div>
<div class="m2"><p>گه در نعال و گاه سوی صدر میشوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زین ژاژ ترکتازت بی قدر میشوی</p></div>
<div class="m2"><p>تو خود گهی هلال و گهی بدر میشوی</p></div></div>
<div class="b2" id="bn27"><p>تا کی کنی بنقص کمالات من کمین</p></div>
<div class="b" id="bn28"><div class="m1"><p>من از عطش اگر مقر اندر سقر کنم</p></div>
<div class="m2"><p>مشنو که چشم عجز بآب خضر کنم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با حکم خویش حلقه بگوش قدر کنم</p></div>
<div class="m2"><p>آنم که خامه را چو پی هجو سرکنم</p></div></div>
<div class="b2" id="bn30"><p>صبح ازل گریزد زی شام واپسین</p></div>
<div class="b" id="bn31"><div class="m1"><p>گو کس پژوهشم ننماید که کیستم</p></div>
<div class="m2"><p>دراین بلد اسیر ندامت زچیستم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من طالب پژوهش افلاک نیستم</p></div>
<div class="m2"><p>زیرا که من بیزد غم افزا نزیستم</p></div></div>
<div class="b2" id="bn33"><p>جز در پی ثنای خداوند راستین</p></div>
<div class="b" id="bn34"><div class="m1"><p>محمود خان مهین ملک العرش راستان</p></div>
<div class="m2"><p>کآزرم شوکتش کشد ارواح باستان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دستان ملک داریش آنسوی داستان</p></div>
<div class="m2"><p>بر جای پاسبان فلکش رخ برآستان</p></div></div>
<div class="b2" id="bn36"><p>برجای دست بحر محیطش در آستین</p></div>
<div class="b" id="bn37"><div class="m1"><p>تا فکرتش گره زده با راز چرخ جیب</p></div>
<div class="m2"><p>گیتی پر از هنر شد و کیهان بری زعیب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>صدق و صفا فزود و بپرداخت شک و ریب</p></div>
<div class="m2"><p>هم خواند اقتضای قضا را بلوح غیب</p></div></div>
<div class="b2" id="bn39"><p>هم راند ازکمان گمان ناوک یقین</p></div>
<div class="b" id="bn40"><div class="m1"><p>ایصاحب صفات خوش از فر خجسته ذات</p></div>
<div class="m2"><p>از خاک درگه تو خجل چشمه حیات</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بارای تو عشا زده سر پنجه با غدات</p></div>
<div class="m2"><p>ازقهر تو فتد به بنین حیض چون بنات</p></div></div>
<div class="b2" id="bn42"><p>با لطف توبنات برد سبقت از بنین</p></div>
<div class="b" id="bn43"><div class="m1"><p>بر بوی آنکه روح نیا گردد از تو شاد</p></div>
<div class="m2"><p>هردم برسم خان خلیلی بعدل و داد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وه زین برادری که چنین دلکش اوفتاد</p></div>
<div class="m2"><p>طوبی له آن پدر که چو در خلد رو نهاد</p></div></div>
<div class="b2" id="bn45"><p>زوماند این دو تن خلف الصدق جانشین</p></div>
<div class="b" id="bn46"><div class="m1"><p>تو امر بر قدرکنی او نهی برقضا</p></div>
<div class="m2"><p>تو رنج را شفائی و او گنج را بلا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو برخصیم خوفی و او بر محب رجا</p></div>
<div class="m2"><p>توطیش را فنائی و او عیش را بقا</p></div></div>
<div class="b2" id="bn48"><p>تو رزم را یساری و او بزم را یمین</p></div>
<div class="b" id="bn49"><div class="m1"><p>تو مرجع افاخم و او ملجا رجال</p></div>
<div class="m2"><p>تو فیلسوف مشرب و او فلسفی مقال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تو مظهر درایت و او مظهر جلال</p></div>
<div class="m2"><p>تو آیت تبحر و او رایت کمال</p></div></div>
<div class="b2" id="bn51"><p>تو بهجت مصور و او حجت مبین</p></div>
<div class="b" id="bn52"><div class="m1"><p>تو کعبه معانی و او قبله صور</p></div>
<div class="m2"><p>تو قلب آفرینش و او قالب ظفر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو مدرک الحقایق و او کامل النظر</p></div>
<div class="m2"><p>تو فتح باب خیری و او سد راه شر</p></div></div>
<div class="b2" id="bn54"><p>تو از خرد سرشته و او از هنر عجین</p></div>
<div class="b" id="bn55"><div class="m1"><p>تو بحر علم موجی و او ابر فضل بار</p></div>
<div class="m2"><p>توکوه چرخ صدری واو عرش خاکسار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تو صرف احتشامی و او محض اقتدار</p></div>
<div class="m2"><p>پهلوی کفر از رقم رعب تو نزار</p></div></div>
<div class="b2" id="bn57"><p>بازوی شرع از قلم لطیف او سمین</p></div>
<div class="b" id="bn58"><div class="m1"><p>عکسی است پیش رای تو این مهر مستنیر</p></div>
<div class="m2"><p>خشتی است نزد کاخ وی این چرخ مستدیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تو جود را بشیری و او بخل را نذیر</p></div>
<div class="m2"><p>تو بخت را مظاهر و او فتح را مجیر</p></div></div>
<div class="b2" id="bn60"><p>تو تخت را محافظ و او تاج را معین</p></div>
<div class="b" id="bn61"><div class="m1"><p>ای دو منعمی که یک آمد نعیمتان</p></div>
<div class="m2"><p>یکروح در دو تن زخدای کریمتان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>رسوا چو یکدو قافیه من خصیمتان</p></div>
<div class="m2"><p>ارجو که از تموج جود عمیمتان</p></div></div>
<div class="b2" id="bn63"><p>خیزد زطبع حضرت جیحون در ثمین</p></div>
<div class="b" id="bn64"><div class="m1"><p>تا ماه و مهر را بدر از چرخ نی عبور</p></div>
<div class="m2"><p>تا چرخ زاید از روش مهر و مه دهور</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا در کسوف مه کند از مهر سلب نور</p></div>
<div class="m2"><p>هرساعتی ز دور تو بالاتر از شهور</p></div></div>
<div class="b2" id="bn66"><p>هرآنی از زمان وی آنسوتر از سنین</p></div>