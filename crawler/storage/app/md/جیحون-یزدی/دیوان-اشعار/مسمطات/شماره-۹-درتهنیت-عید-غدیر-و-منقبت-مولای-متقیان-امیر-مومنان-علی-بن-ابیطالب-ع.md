---
title: >-
    شمارهٔ ۹ - درتهنیت عید غدیر و منقبت مولای متقیان امیر مومنان علی بن ابیطالب (ع)
---
# شمارهٔ ۹ - درتهنیت عید غدیر و منقبت مولای متقیان امیر مومنان علی بن ابیطالب (ع)

<div class="b" id="bn1"><div class="m1"><p>ای بعذایرت بسی عاشق را دل است گم</p></div>
<div class="m2"><p>عذر بنه بزیر پا وزسر انبساط قم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وجدآور بهفت آب رقص افکن بچارام</p></div>
<div class="m2"><p>وزخم می بجام کن کاینک در غدیر خم</p></div></div>
<div class="b2" id="bn3"><p>گشت وصی مصطفی صدر نشین لو کشف</p></div>
<div class="b" id="bn4"><div class="m1"><p>درگه رجعت ازحرم فخر عجم شه عرب</p></div>
<div class="m2"><p>بهر وصایت علی آراست منبر از قتب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باوی برشد و ورا بستود از پس خطب</p></div>
<div class="m2"><p>گردید آستین فشان ناقه صالح از طرب</p></div></div>
<div class="b2" id="bn6"><p>زد بجهاز اشتران گام چو شحنه النجف</p></div>
<div class="b" id="bn7"><div class="m1"><p>دست بدست بانبی چون بزبر ز پست شد</p></div>
<div class="m2"><p>دست خدایرا خرد بندة پای بست شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هوش ز خم رفعتش می پخشیده مست شد</p></div>
<div class="m2"><p>سود چو پای برقتب عرش برین زدست شد</p></div></div>
<div class="b2" id="bn9"><p>کز چه ز پای او مرا دست نداد این شرف</p></div>
<div class="b" id="bn10"><div class="m1"><p>بین شهود و غیب ازو یافت یقین و رفت شک</p></div>
<div class="m2"><p>او قدم و حدوثرا هست چو حس مشترک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خصم ز لوح خامه اش خوانده مفاد قد هلک</p></div>
<div class="m2"><p>اختر شوکت ورا کثرت سرمدی فلک</p></div></div>
<div class="b2" id="bn12"><p>گوهر فطرت و را وحدت ایزدی صدف</p></div>
<div class="b" id="bn13"><div class="m1"><p>برسخط و محبتش جزیه دهند خیر و شر</p></div>
<div class="m2"><p>در ملکوت حشمتش دانده حشر پی حشر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فطرس از سلیل او شد بجناح مبتشر</p></div>
<div class="m2"><p>شیطان در مفاخرت بگذرد از ابوالبشر</p></div></div>
<div class="b2" id="bn15"><p>گر بطریق التجا دامنش آورد بکف</p></div>
<div class="b" id="bn16"><div class="m1"><p>ایکه چو در غلامیت حلقه کشم دو گوش را</p></div>
<div class="m2"><p>حلقه کعبه برکشد از حسدم خروش را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وقف توکردم از ازل دانش و عقل و هوش را</p></div>
<div class="m2"><p>در شعف اندر افکنم طایفه سروش را</p></div></div>
<div class="b2" id="bn18"><p>خاصه چو بر سلاله ات مدح تو خوانم از شعف</p></div>
<div class="b" id="bn19"><div class="m1"><p>میری کز نژاد شد بدر عرب خور عجم</p></div>
<div class="m2"><p>وز سخن و سخا بود موسی کف مسیح دم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فخر کند ز دوده اش مشعر و زمزم و حرم</p></div>
<div class="m2"><p>سبط امام هشتمین نواب آنکه از کرم</p></div></div>
<div class="b2" id="bn21"><p>مخزن عالمی بود در نظرش کم از خزف</p></div>
<div class="b" id="bn22"><div class="m1"><p>ای پدر از پس پدر داشته عز مولوی</p></div>
<div class="m2"><p>ناید یک ثنای تو دردو هزار مثنوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خامه تو حسام دین گاه فتوح معنوی</p></div>
<div class="m2"><p>کس بصفات نیک خود در همه عمر نشنوی</p></div></div>
<div class="b2" id="bn24"><p>گر نگری ورق ورق در اخبار ماسلف</p></div>