---
title: >-
    شمارهٔ ۱۶ - وله
---
# شمارهٔ ۱۶ - وله

<div class="b" id="bn1"><div class="m1"><p>ای به خم زلف تو حجله چینی صنم</p></div>
<div class="m2"><p>خال تو در زیر چشم نا فه و آهو بهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برهمنت آفتاب حلقه بگوشت حرم</p></div>
<div class="m2"><p>خنده پدید از لبت همچو وجود از عدم</p></div></div>
<div class="b2" id="bn3"><p>روی تو در موی تونور دو چارظلم</p></div>
<div class="b" id="bn4"><div class="m1"><p>ای بدو مرجان تو عقده در رمندرج</p></div>
<div class="m2"><p>نرم سرانگشت تو کلید گنج فرج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق تو عشاق را خوبتر از هر نهج</p></div>
<div class="m2"><p>قامت ما شد کمان ابرویت از چینت کج</p></div></div>
<div class="b2" id="bn6"><p>تیر تو بر ما نشست چشم توکرد از چه رم</p></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه بود نرم تر زاطلس چیست عذار</p></div>
<div class="m2"><p>لیک کند نرمیش بردل ما خارخار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشک تو ناهید پوش سرو تو خورشید بار</p></div>
<div class="m2"><p>با رخت از روشنی بود مه و مهر تار</p></div></div>
<div class="b2" id="bn9"><p>برلبت از نازکی بوسه نمودن ستم</p></div>
<div class="b" id="bn10"><div class="m1"><p>بازتر از خون کیست خنجر نازت بمشت</p></div>
<div class="m2"><p>کآرزوی آن مرا زخم نخورده بکشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در رهت اندوه و رنج راحت خرد و درشت</p></div>
<div class="m2"><p>بهر زمین بوس تست گر شده ام گوژپشت</p></div></div>
<div class="b2" id="bn12"><p>چونکه سپهر برین پیش ولی النعم</p></div>
<div class="b" id="bn13"><div class="m1"><p>سجده بخاک درش مایه نور جباه</p></div>
<div class="m2"><p>ناصیه آفتاب برسخن من گواه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نعل سم بادپاش حلقه کش گوش ماه</p></div>
<div class="m2"><p>رایت منصور او آیت فتح سپاه</p></div></div>
<div class="b2" id="bn15"><p>قبه خرگاه او جنه جند و حشم</p></div>
<div class="b" id="bn16"><div class="m1"><p>ایکه بدیهیم تو عرش برین داده بوس</p></div>
<div class="m2"><p>در بر منجوق تو گونه خورسندروس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فوج ترا اوج چرخ توشه کشی چابلوس</p></div>
<div class="m2"><p>مهابتت بشکند سطوت افغان بکوس</p></div></div>
<div class="b2" id="bn18"><p>رعایتت جان دمد در تن شیر علم</p></div>
<div class="b" id="bn19"><div class="m1"><p>جرعه کش مهرتست هر چه بگیتی بقا</p></div>
<div class="m2"><p>ریزه خور قهرتست آنچه بگیهان فنا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تابع امرت قدر مطیع نهیت قضا</p></div>
<div class="m2"><p>خوف موبد بود از تو گسستن رجا</p></div></div>
<div class="b2" id="bn21"><p>عز مخلد بود برتو شدن معتصم</p></div>
<div class="b" id="bn22"><div class="m1"><p>روزی کز توسنی خنگ رجال نبرد</p></div>
<div class="m2"><p>گرد زغبرا زنند برفلک گرد گرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیچد از آوای کوس دردل البرز درد</p></div>
<div class="m2"><p>گوشت بچشمه زره جوشد ز اندام مرد</p></div></div>
<div class="b2" id="bn24"><p>بس بدل آید زگرز بر سمن او درم</p></div>
<div class="b" id="bn25"><div class="m1"><p>ناگه گیری بر اسب چون تو زصرصر سبق</p></div>
<div class="m2"><p>بیلکت از پیلها باز نوردد ورق</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زان همه گردان کتی سد مجال نطق</p></div>
<div class="m2"><p>از دم تیغت که هست جوهر تأیید حق</p></div></div>
<div class="b2" id="bn27"><p>خصم شود منهزم ملک شود منتظم</p></div>
<div class="b" id="bn28"><div class="m1"><p>میرا این نغز شعر که غرق معنی بود</p></div>
<div class="m2"><p>زحسن مضمون بکر یکسره حبلی بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باکره و حامله این خوش دعوی بود</p></div>
<div class="m2"><p>نی نی اشعار من مادر عیسی بود</p></div></div>
<div class="b2" id="bn30"><p>که در بکارت پراست ز روح قدسش شکم</p></div>
<div class="b" id="bn31"><div class="m1"><p>تا که بگرید غمام بخت تو در خنده باد</p></div>
<div class="m2"><p>تا که زند خنده برق خصم توگرینده باد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز رفته ات خوبتر زمان آینده باد</p></div>
<div class="m2"><p>نخل مراد تو سبز بیخ غمت کنده باد</p></div></div>
<div class="b2" id="bn33"><p>معاندت مبتذل معاونت محتشم</p></div>