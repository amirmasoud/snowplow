---
title: >-
    شمارهٔ ۳ - درتهنیت عید غدیر و منقبت حضرت امیر (ع)
---
# شمارهٔ ۳ - درتهنیت عید غدیر و منقبت حضرت امیر (ع)

<div class="b" id="bn1"><div class="m1"><p>ترکا بجوش خم غدیر از نیاز بین</p></div>
<div class="m2"><p>وز این نیاز مست شوو جان بناز بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستی به خم زساقی کوثر دراز بین</p></div>
<div class="m2"><p>پیمان کن و بگردش پیمانه راز بین</p></div></div>
<div class="b2" id="bn3"><p>کزاین خم است مستی ذرات ممکنات</p></div>
<div class="b" id="bn4"><div class="m1"><p>زین خم نخست باده بجام اراده شد</p></div>
<div class="m2"><p>وز وی فلک ستاده زمین اوفتاده شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنگاه مست از و ملک از طبع ساده شد</p></div>
<div class="m2"><p>پس درکف رسل قدحی زو نهاده شد</p></div></div>
<div class="b2" id="bn6"><p>تا دوست را حیات بود خصم را ممات</p></div>
<div class="b" id="bn7"><div class="m1"><p>خم غدیر پر زالهی شراب بین</p></div>
<div class="m2"><p>وزاین شراب جان مخالف کباب بین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نی اندر این خم آیت نیل از صواب بین</p></div>
<div class="m2"><p>برقبطیش زخون و بسبطی زآب بین</p></div></div>
<div class="b2" id="bn9"><p>کاین صاف خم طغات نکو داند از هدات</p></div>
<div class="b" id="bn10"><div class="m1"><p>بود این چنین صباح که جبریل با درود</p></div>
<div class="m2"><p>زایزد بسوی احمد مختار شد فرود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت ای زقهر و لطف تو نازان زیان و سود</p></div>
<div class="m2"><p>بستای مرعلی را انسان که حق ستود</p></div></div>
<div class="b2" id="bn12"><p>ورنه رسالت تو بنائیست بی ثبات</p></div>
<div class="b" id="bn13"><div class="m1"><p>یعنی که حج گذاشتنت سیرخانه بود</p></div>
<div class="m2"><p>درغسلت از کدورت ظاهر کرانه بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صومت بجای خوردن نان شبانه بود</p></div>
<div class="m2"><p>مقصود ما علی بود آنها بهانه بود</p></div></div>
<div class="b2" id="bn15"><p>کزوی روان به پیکر حج است تا صلات</p></div>
<div class="b" id="bn16"><div class="m1"><p>او صورت شرایع و او معنی ملل</p></div>
<div class="m2"><p>او کعبه حقیقت و او رکن هر عمل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از او ابد پدید تواند شد از ازل</p></div>
<div class="m2"><p>او گفت با کلیم که انظر الی الجبل</p></div></div>
<div class="b2" id="bn18"><p>او خضر را نواخت بسر چشمه حیات</p></div>
<div class="b" id="bn19"><div class="m1"><p>احمد چو این ترانه زجبریل گوش کرد</p></div>
<div class="m2"><p>درجمع خیل رفته و آینده گوش کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>می درسکون به پای خم از وجد نوش کرد</p></div>
<div class="m2"><p>آنگاه رای نشر پیام سروش کرد</p></div></div>
<div class="b2" id="bn21"><p>بر خاست منبری زقتب یا که از حصات</p></div>
<div class="b" id="bn22"><div class="m1"><p>پس دست حق گرفت بدست و فراز برد</p></div>
<div class="m2"><p>آنسان که اوج عرش بپا یش نماز برد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لوح از قلم بسوی بنانش نیاز برد</p></div>
<div class="m2"><p>چرخ از قدر بساعد او شاهباز برد</p></div></div>
<div class="b2" id="bn24"><p>شد منبر از نبی و ولی پر صفات و ذات</p></div>
<div class="b" id="bn25"><div class="m1"><p>گفتا نبی بخلق که دست خداست این</p></div>
<div class="m2"><p>دست خدا بود که بمنبر بپاست این</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حلال مشکلات بارض و سماست این</p></div>
<div class="m2"><p>سر حلقه رسل غرض از اولیاست این</p></div></div>
<div class="b2" id="bn27"><p>در کین او هلاکت و در مهر او نجات</p></div>
<div class="b" id="bn28"><div class="m1"><p>دستی است این که بیعت او بیعت خداست</p></div>
<div class="m2"><p>بازویش آخته علم قدرت خداست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در خنصرش تختم از حشمت خداست</p></div>
<div class="m2"><p>سبابه اش کلید در رحمت خداست</p></div></div>
<div class="b2" id="bn30"><p>او ذات و ماسواست از او جلوه صفات</p></div>
<div class="b" id="bn31"><div class="m1"><p>باری حق از علی بنبی چون جهاند پیک</p></div>
<div class="m2"><p>وافشرد پی که بلغ ما انزل الیک</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>افتاده از نفاق بشلوار خصم کیک</p></div>
<div class="m2"><p>میخواست تا زغم بعدم رخ نهد ولیک</p></div></div>
<div class="b2" id="bn33"><p>بخ لک ای علی گفت ازشاه گشته مات</p></div>
<div class="b" id="bn34"><div class="m1"><p>آری چو از سقایت فرمان ذوالمنن</p></div>
<div class="m2"><p>بنشست جوش خم غدیر ازمی کهن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فاروق و آشناش چو شیخان خم شکن</p></div>
<div class="m2"><p>زین خم به خام محن گشتشان سکن</p></div></div>
<div class="b2" id="bn36"><p>شد زاشکشان مدام وز آلامشان سقات</p></div>
<div class="b" id="bn37"><div class="m1"><p>ایماه مهربان و بت دل ستان من</p></div>
<div class="m2"><p>از قد و چهر سرو من و بوستان من</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زین عبد تازه کن بکهن باده جان من</p></div>
<div class="m2"><p>بل زیب ده زرطل و قدح گرد خوان من</p></div></div>
<div class="b2" id="bn39"><p>کز کردگار مغتفرند این زمان عصات</p></div>
<div class="b" id="bn40"><div class="m1"><p>ای گلبنی که حور بود باغبان تو</p></div>
<div class="m2"><p>غلمان غلام رانده از دودمان تو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>می خور مترس نار نیابد نشان تو</p></div>
<div class="m2"><p>کاندر ولای شاه ولایت بجان تو</p></div></div>
<div class="b2" id="bn42"><p>صد بار بهتر از حسنات است سیئات</p></div>
<div class="b" id="bn43"><div class="m1"><p>شاهیکه کشف سر خدائی بمیل اوست</p></div>
<div class="m2"><p>هر چیز هست و بود و بود درطفیل اوست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>میکال ریزه چین گرانبار کیل اوست</p></div>
<div class="m2"><p>جبریل خاک روب سبک سیرخیل اوست</p></div></div>
<div class="b2" id="bn45"><p>با گفت او هر آنچه بجز وحی ترهات</p></div>
<div class="b" id="bn46"><div class="m1"><p>گر بگذری بخلد جز او دلنواز نیست</p></div>
<div class="m2"><p>ور در شوی بنار جز او جانگداز نیست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر بنگری بعرش جز او چاره ساز نیست</p></div>
<div class="m2"><p>ور در زمین چمی بجز اویکه تازنیست</p></div></div>
<div class="b2" id="bn48"><p>کز وی پراست عالم ایجاد را جهات</p></div>
<div class="b" id="bn49"><div class="m1"><p>ای داوریکه مصحف توحید روی تست</p></div>
<div class="m2"><p>ایمان و کفر در بدر از جستجوی تست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زنار و سبحه خسته دل ازگفتگوی تست</p></div>
<div class="m2"><p>هرجا که بنگرم بهمه سوی کوی تست</p></div></div>
<div class="b2" id="bn51"><p>از دیر تا حرم زحرم تا بسومنات</p></div>
<div class="b" id="bn52"><div class="m1"><p>تو پیش از آفرینش غیر از تو پیش نی</p></div>
<div class="m2"><p>از تست بر زو پست و زاغیار و خویش نی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بی حکم تو تخالف در گرگ و میش نی</p></div>
<div class="m2"><p>هستی بجز حقیقت ذات تو بیش نی</p></div></div>
<div class="b2" id="bn54"><p>گو شیخ شهر خواندم از جمله غلات</p></div>
<div class="b" id="bn55"><div class="m1"><p>من غیر مدحت تو در آفاق ننگرم</p></div>
<div class="m2"><p>بی مهر تو بخورگه اشراق ننگرم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جز جفت ابروان تو را طاق ننگرم</p></div>
<div class="m2"><p>طاقیست آن که جفتش از آفاق ننگرم</p></div></div>
<div class="b2" id="bn57"><p>موی توام عشا و عذار توام غدات</p></div>
<div class="b" id="bn58"><div class="m1"><p>شاها اگر ز روح تن از عقل جان کنم</p></div>
<div class="m2"><p>آنگه چو خضر زندگی جاودان کنم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کی یک ثنای تو بهزاران قران کنم</p></div>
<div class="m2"><p>لیکن ازآن خوشم که چو نامت بیان کنم</p></div></div>
<div class="b2" id="bn60"><p>گردد زچاکر تو بسوی من التفات</p></div>
<div class="b" id="bn61"><div class="m1"><p>سلطان حمید ناصر دولت که شخص او</p></div>
<div class="m2"><p>از مهر و ماه باج ستاند به رای و رو</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>میریکه چرخ در خم چوگان اوست گو</p></div>
<div class="m2"><p>گو خصم او شود چو حصاری زسنگ و رو</p></div></div>
<div class="b2" id="bn63"><p>کش ازکمیت کنده پراند سوی کمات</p></div>
<div class="b" id="bn64"><div class="m1"><p>تیغش برزم آینه دار اجل بود</p></div>
<div class="m2"><p>تفش ببزم عقده گشای امل بود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نزدش ملک چو در برایزد هبل بود</p></div>
<div class="m2"><p>در مردمی یگانه و ضرب المثل بود</p></div></div>
<div class="b2" id="bn66"><p>جودش الوف باز ندانسته ازمآت</p></div>
<div class="b" id="bn67"><div class="m1"><p>ای آنکه افتخار زمان و زمین توئی</p></div>
<div class="m2"><p>در هر هنر مخاطب صد آفرین توئی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>اندر شرف بخاتم دولت نگین توئی</p></div>
<div class="m2"><p>از محمدت مجیر بنات و بنین توئی</p></div></div>
<div class="b2" id="bn69"><p>کز عدل هم بنین بتو شادند و هم بنات</p></div>
<div class="b" id="bn70"><div class="m1"><p>روی سپه نگار سمن بو عذار تو</p></div>
<div class="m2"><p>پشت سمند کاخ لئالی نگار تو</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>کیهان پر از در ازکف گوهر نثار تو</p></div>
<div class="m2"><p>مانا درآستین یمین و یسار تو</p></div></div>
<div class="b2" id="bn72"><p>بر جای دست گشته نهان دجله و فرات</p></div>
<div class="b" id="bn73"><div class="m1"><p>میرا بنظم کس زمن افزون نمی شود</p></div>
<div class="m2"><p>کافزون ازین صتاعت و مضمون نمی شود</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هرکس زیزد خیزد جیحون نمی شود</p></div>
<div class="m2"><p>باران تمام لؤلؤ مکنون نمی شود</p></div></div>
<div class="b2" id="bn75"><p>کی در چمن جماد برد سبقت از نبات</p></div>
<div class="b" id="bn76"><div class="m1"><p>اینها که بنگری همه ریشند وسبلتند</p></div>
<div class="m2"><p>اندر حضور درخور صد گونه غیبتند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>افسردگان معنی و سرگرم صورتند</p></div>
<div class="m2"><p>نه زاهل دولتند و نه زابناء ملتند</p></div></div>
<div class="b2" id="bn78"><p>زود اکشان عظام شود زآسمان رفات</p></div>
<div class="b" id="bn79"><div class="m1"><p>من در دو کونم ازکرم دوست زندگیست</p></div>
<div class="m2"><p>آنجا بخواجگیست گر اینجا به بندگیست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>مهر علی مرا بعلا درکشندگی است</p></div>
<div class="m2"><p>چون ذوالفقار ناطقه ام را برندگیست</p></div></div>
<div class="b2" id="bn81"><p>هرچند شایگان بقوافیست از لغات</p></div>
<div class="b" id="bn82"><div class="m1"><p>تا خاک را درنگ بود باد را شتاب</p></div>
<div class="m2"><p>تا نار انجماد برد آب التهاب</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>تا بالد ارض برفلک از عید بوتراب</p></div>
<div class="m2"><p>یار ترا شکوه خطر بر حد نصاب</p></div></div>
<div class="b2" id="bn84"><p>خصمت ز افتقار پژوهنده زکات</p></div>