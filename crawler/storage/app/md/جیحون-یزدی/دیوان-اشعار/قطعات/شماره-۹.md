---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ای مهین والاگهر والی که ما از همتت</p></div>
<div class="m2"><p>فارغ از اندیشه و آسوده از دریوزه‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دقتی کردم چو یاقوتی مفرخ گوش دار</p></div>
<div class="m2"><p>تا بدانی در چه فن زین گنبد پیروزه‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان از دو محمد دین ایزد شد قوی</p></div>
<div class="m2"><p>کز وجود این دو اندر نعمت هرروزه‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن یکی پیغمبر یزدان و این والی یزد</p></div>
<div class="m2"><p>وز پی تقلیدشان ما فرقه دلسوزه‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان عرب‌ها را شرف وز این عجم‌ها را شعف</p></div>
<div class="m2"><p>زین و آن ما پر نموده زآب رحمت کوزه‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیکن این شیر عجم برعکس آن میر عرب</p></div>
<div class="m2"><p>بدعتی انگیخت کز وی ریگ غم در موزه‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زمان آن محمد روزه گر در روز بود</p></div>
<div class="m2"><p>در زمان این محمد ما به شب هم روزه‌ایم</p></div></div>