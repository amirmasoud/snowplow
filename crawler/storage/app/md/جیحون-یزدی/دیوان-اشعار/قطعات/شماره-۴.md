---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>دلم بر چهره‌ات تا مهر بسته است</p></div>
<div class="m2"><p>زهر شیرین لبی الفت گسسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا سودای پستان تو کافی است</p></div>
<div class="m2"><p>که صفرایم بلیموئی شکسته است</p></div></div>