---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>دِهْ سرخ‌مِیی کَز تُتُقِ اَنوارش</p></div>
<div class="m2"><p>شرمنده شوند ثابت و سیّارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن آینه‌وَش چنان شود زو صافی</p></div>
<div class="m2"><p>کز سینهٔ مرد بنگری اسرارش</p></div></div>