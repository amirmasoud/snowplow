---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>آنشوخ که زد شرر بجانها خشمش</p></div>
<div class="m2"><p>زلفش دل خلق برد و داند پشمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمی او راست تنگ و ...گشاد</p></div>
<div class="m2"><p>ایکاش ...بود اثری ازچشمش</p></div></div>