---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>عطارد پسر شکر ز مرجانم ده</p></div>
<div class="m2"><p>عنابی از آن لبان خندانم ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دانه بهل به زنخدانم بخش</p></div>
<div class="m2"><p>بردار سپستان و دو پستان و دو پستانم ده</p></div></div>