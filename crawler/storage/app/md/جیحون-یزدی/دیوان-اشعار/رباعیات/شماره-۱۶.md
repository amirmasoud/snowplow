---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>شهزاده محمد ای شه شیر مصاف</p></div>
<div class="m2"><p>کز صارم تو گاو زمین دزدد ناف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بامردیت این عجب بود کافتاده است</p></div>
<div class="m2"><p>در وعده تو چون زن خصم تو خلاف</p></div></div>