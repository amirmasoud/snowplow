---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>لیمو زلبت چو شکر ناب شود</p></div>
<div class="m2"><p>عنقا زدم تیر تو در تاب شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکذره زمهر رخ چون کوکب تو</p></div>
<div class="m2"><p>گر بر دل البرز خورد آب شود</p></div></div>