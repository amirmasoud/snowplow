---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>حاجی که زساز عیش سوزی نخرد</p></div>
<div class="m2"><p>تا مزرعه زری بروزی نخرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد ده دارد بیزد اندرهمه عمر</p></div>
<div class="m2"><p>کش هیچکسش گزی بگوزی نخرد</p></div></div>