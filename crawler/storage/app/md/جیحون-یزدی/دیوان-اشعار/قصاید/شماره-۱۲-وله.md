---
title: >-
    شمارهٔ ۱۲ - وله
---
# شمارهٔ ۱۲ - وله

<div class="b" id="bn1"><div class="m1"><p>جشن میلاد خداوند سفیران خداست</p></div>
<div class="m2"><p>کشف حق وجد فرق شور قدر سور قضاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زد بزیر فلک و روی زمین تخت شهی</p></div>
<div class="m2"><p>که فزون تر بشکوه گهر از ارض و سماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهی از مکه درخشید که مانند جدی</p></div>
<div class="m2"><p>خال ابروی وی از بهر امم قبله نماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو کلیمی که زند نعره رب ارنی</p></div>
<div class="m2"><p>که حق اینک متجلی شده اندر بطحاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عارضی تافت که با آن ید بیضا موسی</p></div>
<div class="m2"><p>نشناسد بر او دست چپ خویش ز راست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گیتی انباشته از عیش چو باغ مینو است</p></div>
<div class="m2"><p>خاک آموده زاختر چو سپهر میناست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می بسا غرنه و هر سو صنمی عربده جوست</p></div>
<div class="m2"><p>مشک پیدانه و هر لحظه هوا نافه گشاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خرامنده تذروی که سیه طره تست</p></div>
<div class="m2"><p>پر زاغی که دل انگیزتر از فرهماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بطی از خون حمام آر چو طاووس بکاخ</p></div>
<div class="m2"><p>که دگر بوم محن خفته بمرز عنقاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل ارم بزم حرم کفر بغم دین بیغم</p></div>
<div class="m2"><p>می بلب جان بطرب خصم به تب فقر فناست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خضر خط لعبت من ایکه بود چهره تو</p></div>
<div class="m2"><p>خرمنی لاله که پرورده از آب بقاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پر کن آن جام چو مرآت سکندر که دگر</p></div>
<div class="m2"><p>خاک گیتی همه چون آب خضر عمر فزاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترک زمرد خط من ایکه زمر جان لب تو</p></div>
<div class="m2"><p>رنگ یاقوت زخجلت همه چون کاهرباست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رطل الماس نهاد ازمی چون لعل آور</p></div>
<div class="m2"><p>که کنون بحر مشیت زصفا گوهر زاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از صدف گشت برون در یتیمی که زقدر</p></div>
<div class="m2"><p>قاب قوسین یکی قطره اش از صد دریاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای بت صاف ذقن ایکه ترا جای بدن</p></div>
<div class="m2"><p>گنجی از نقره مصقول بزربفت قباست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>موی بگشای که آفاق همه غالیه بوست</p></div>
<div class="m2"><p>روی بنمای که از مظهر کل کشف غطاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>می بکش نقل بچش رود بزن عود بسوز</p></div>
<div class="m2"><p>کآسمان راد و زمین شاد و جهان کامرواست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خسروی رفت بر او رنگ نبوت که خلیل</p></div>
<div class="m2"><p>حلقه زن بر در کاشانه او همچو گداست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>محرم خلوت معبود محمد که زجود</p></div>
<div class="m2"><p>خوان کونین در ایوان جلالش یغماست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لب جان بخش وی آنگه که در آید بسخن</p></div>
<div class="m2"><p>روح عیسی بصد امیدش ازو چشم شفاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نزد قدسش که ملک ریزد از او اشک ز رشک</p></div>
<div class="m2"><p>صوم و تسبیح رسل طاعتی از روی ریاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صالح ارناقه از سنگ بمعجز آورد</p></div>
<div class="m2"><p>هر شتربان زکهین چاکر او از صلحاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>موسی ارجست لقای خضر از بحر علوم</p></div>
<div class="m2"><p>هر شبانی زکمین خادم او خضر لقاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عرش تا فرش پر از وی بود و از همه سوی</p></div>
<div class="m2"><p>هم بجایست نشان جستن از وهم بیجاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عجب این نیست که زد خیمه زمعراج بعرش</p></div>
<div class="m2"><p>عجب اینست که چون بارگهش درغبر است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یک فروغ از رخ او بیش بذرات نتافت</p></div>
<div class="m2"><p>گر چه اندر حرم و دیر زمهرش غوغاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>معنی عالم و آدم بجز او نیست ولیک</p></div>
<div class="m2"><p>اختلاف صور از احولی دیده ماست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای مهین جلوه حق وی که زفر تو کلیم</p></div>
<div class="m2"><p>لن ترانی شنو افتاده بطور سیناست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>لعلت از دل بحدیثی ببرد ظلمت شرک</p></div>
<div class="m2"><p>همچو حل کرده یاقوت که تریاک رباست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عزمت ار سرمه کش چشم محالات بود</p></div>
<div class="m2"><p>از دم روح قدس پنجه مریم بحناست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کشتی حلم تو تا لنگر تسلیم فکند</p></div>
<div class="m2"><p>نوح از لاتذرش غرفه طوفان حیاست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پیش حکم تو که با حکم خدا زاده بهم</p></div>
<div class="m2"><p>قدرت لوح و قلم تالی فرعون و عصاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نزد رای تو که شد مشعله افروز قدم</p></div>
<div class="m2"><p>حشمت کون و مکان ثانی خورشید و سهاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بندگانت همه مردانه و پاکند ولیک</p></div>
<div class="m2"><p>پاک و مردانه تر از جمله شه دوره ماست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ناصرالدین شه غازی مه افلاک شکوه</p></div>
<div class="m2"><p>که جهان با دل پهناور او تنگ فضاست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن ظفرمند عدو بند که بر پشت سمند</p></div>
<div class="m2"><p>همچو کوهیست که زین برزده بر باد صباست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نکند بیم زتیغ و نهراسد از تیر</p></div>
<div class="m2"><p>تیر و تیغش بمثل یاسمن و مهرگیاست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بس دلیر است تصور نکند معنی ترس</p></div>
<div class="m2"><p>بل گمانش که چو او هر که بود مردوغاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آری آنکس که خود از گنج و گهر مستغنی است</p></div>
<div class="m2"><p>به یقینش که چو او در همه کس استغناست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خود پولاد بفرقش چو کلاه تتری است</p></div>
<div class="m2"><p>درع آهن به تنش تالی چینی دیباست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بس بود عشق برزمش همه شب تا بسحر</p></div>
<div class="m2"><p>دیده درخواب که تیغ آخته بر اژدرهاست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از گمان گر بمثل رانده بجابلسا تیر</p></div>
<div class="m2"><p>هدف وی شده هر شیر که درجا بلقاست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای هنر دوست خدیوی که برغم دشمن</p></div>
<div class="m2"><p>کمترین خا صیت خوی تو عفو است و عطاست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر برد مژده کس از چونتو خلف سوی بهشت</p></div>
<div class="m2"><p>تاج بر ز آدم و خلخال ستان بر حواست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آن چه گنجی است که از یمن زمانت نفزود</p></div>
<div class="m2"><p>وآن چه رنجیست که از سطوت باس تونکاست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ای خورتخت و مه تاج تو دانی کامروز</p></div>
<div class="m2"><p>زینت تخت سخن حضرت تا ج الشعر است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر انصاف بود با سخن دلکش من</p></div>
<div class="m2"><p>نظم مسعود هدر گفته وطواط هباست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>لیک فیض توام این زمزمه آموخت بلی</p></div>
<div class="m2"><p>بلبلانرا زگل آرایش در برگ و نواست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا که در مایه نه مانند خریف است ربیع</p></div>
<div class="m2"><p>تا که در پایه نه با سلطنت صیف شتاست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>باد هر فصل زفر خنده زما نت خرم</p></div>
<div class="m2"><p>که جهان کهن از دولت بختت برناست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خواستم گفت که گردون سزدت حاجب بار</p></div>
<div class="m2"><p>عفل فرمود که این دون بود و آن والاست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هان فلک چیست که پا بر سر کوی تو نهد</p></div>
<div class="m2"><p>که بکوی تو دو صد همچو فلک بی سر و پاست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زاشتیاق کف بذل تو همی در معدن</p></div>
<div class="m2"><p>سیم و زر را چونبات از دل و جان نشو ونماست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدسگال تو چو شامیست که آلوده نخفت</p></div>
<div class="m2"><p>نیکخواه تو چو صبحی است که آسوده نخاست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>که دعا کرده به جان تو که از حسن قبول</p></div>
<div class="m2"><p>هر کجا نام تو آید به میان بر تو دعاست</p></div></div>