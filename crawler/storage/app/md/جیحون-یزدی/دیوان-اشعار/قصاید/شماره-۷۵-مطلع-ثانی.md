---
title: >-
    شمارهٔ ۷۵ - مطلع ثانی
---
# شمارهٔ ۷۵ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>کیستم من آنکه بوسد آسمان غبرای من</p></div>
<div class="m2"><p>به ز امروز است از الطاف حق فردای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرد اسیاف ملوک اندر غلاف از بیم زنگ</p></div>
<div class="m2"><p>از غلاف آید برون چون صارم برای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رای من سازد شب دیجور را روشن چو روز</p></div>
<div class="m2"><p>بر شب دیجور اگر عکسی فتد از رای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر زمین بیدار بختی آسمان چون من ندید</p></div>
<div class="m2"><p>هم مگر در خواب بیند بعد از این همتای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هفت بحر موج زن را گر فرا سنجی بوهم</p></div>
<div class="m2"><p>قطره باشد ز بحر طبع گوهر زای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دید چون ایوان من کیوان بحسرت گفت کاش</p></div>
<div class="m2"><p>بود خشتی زین بنانه گنبد خضرای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون شنید ایوان من گفت ای زحل بیجا ملاف</p></div>
<div class="m2"><p>کاین دنائت دور بود از فطرت بنای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که را زاعیان کیهان بنگری بوده است خود</p></div>
<div class="m2"><p>چاکر اجداد من یابنئده آبای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میر اصطبلم ببندد از فلک پیمای مهر</p></div>
<div class="m2"><p>گوی زرین بردم خنگ جهان پیمای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شه نژادا کامگارا خود چه دانی کز سپهر</p></div>
<div class="m2"><p>تا چه حد افسردگی دارد دل شیدای من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خسروی بودم که شیرینم زمشکو تاخت رخ</p></div>
<div class="m2"><p>وامقی بودم که بر بود آسمان عذرای من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نگیری دست من پس دست شو از هست من</p></div>
<div class="m2"><p>پایمردی کن اگر داری سر ابقای من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا بود گردنده گردون تا چمد رخشند مهر</p></div>
<div class="m2"><p>عرش گوید ای تراب درگهت ملجای من</p></div></div>