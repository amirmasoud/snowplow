---
title: >-
    شمارهٔ ۵۸ - وله
---
# شمارهٔ ۵۸ - وله

<div class="b" id="bn1"><div class="m1"><p>خور بسر ما گفت امروز کنم درک حمل</p></div>
<div class="m2"><p>گفت رو درک حمل کن نکنم ترک عمل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت هرساله بنوروز زدم خیمه بگل</p></div>
<div class="m2"><p>گفت در خیمه نه اکنون عوض گل منقل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت رو گرم ببر لشکر سرما کامروز</p></div>
<div class="m2"><p>حمل از نامیه جیش از تو برد این که وتل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت از یخ بود آنسان سپهم جوش پوش</p></div>
<div class="m2"><p>که زچنگ اسدش هم نرسد هیچ خلل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راستی ایزد داناست کز آثار پدید</p></div>
<div class="m2"><p>حالها کار بهار است زسرما مختل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیکتر آنکه بپاس روش عهد قدیم</p></div>
<div class="m2"><p>مجلسی سازیم امروز بصد گونه حلل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نی و بربط و طنبور و دف و تار و رباب</p></div>
<div class="m2"><p>از می و مطرب و رامشگر و تشبیب و غزل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هفت سین هم چو ز سرما ندهد دست امسال</p></div>
<div class="m2"><p>به که آید همه برسین یکی ساده بدل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بغل و جیب چو آکنده نباشد زسمن</p></div>
<div class="m2"><p>ما بگیریم سمن سا بدنی را ببغل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سنبل ار نیست چه غم چنگ زنیمش در زلف</p></div>
<div class="m2"><p>که بود سنبل از او درشکن رشک خجل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا تو با ساده و باده سپری یکدوسه روز</p></div>
<div class="m2"><p>فرودین تافته از سرما بازوی حیل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هان چسان چاره سرما نکند فروردین</p></div>
<div class="m2"><p>که بود بنده ای از بارگه صدر اجل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آفتاب فلک هیمنه صدراعظم</p></div>
<div class="m2"><p>که بخجلت ز حضیض در او اوج زحل</p></div></div>