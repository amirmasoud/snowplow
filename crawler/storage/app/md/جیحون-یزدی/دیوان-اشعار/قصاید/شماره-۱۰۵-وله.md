---
title: >-
    شمارهٔ ۱۰۵ - وله
---
# شمارهٔ ۱۰۵ - وله

<div class="b" id="bn1"><div class="m1"><p>بتی که رشک لب لعل او برد عیسی</p></div>
<div class="m2"><p>زخط دمیدنش افتاده کار با موسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زجور خط شده تاری تر از دل فرعون</p></div>
<div class="m2"><p>رخی که بود درخشان تر از کف موسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخش بزیر خط اندر چنانکه پنداری</p></div>
<div class="m2"><p>درون کسوت مجنون نهان شود لیلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو قلب شیر بود ریش چون زصورت رست</p></div>
<div class="m2"><p>زآدمی برمند اهل درد ازین معنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولی نگار سزد نوخط وسهی قامت</p></div>
<div class="m2"><p>که پاکباز و حقیقت شناس نیست صبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه مایه خون که من از دست کودکی خوردم</p></div>
<div class="m2"><p>که می نداشت ممیز نفاق را زوفی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من از شراب سرودم سخن وی از جلاب</p></div>
<div class="m2"><p>من از ثریا کردم حدیث و او زثری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو تربیت شد رفت و برحریفان خفت</p></div>
<div class="m2"><p>چنانکه عمر ابد در فراش مرگ فجی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون ز بالغ و نابالغ بتان دل من</p></div>
<div class="m2"><p>چنان رمیده که سبحان ربی الاعلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولی تغزل باز از پوشان اولی است</p></div>
<div class="m2"><p>بمدح آصف جم مرتبت حسینقلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وزیر عادل باذل بزرگ کوچک دل</p></div>
<div class="m2"><p>که بر زمانه فشاند آستین استغنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زکلک اوست همان خاصیت به پیکر ملک</p></div>
<div class="m2"><p>که از دعای مسیحا بقالب موتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ستاره سوخته خصم از شکوهش آن بیند</p></div>
<div class="m2"><p>که از طلوع ملمع سهیل تخم زنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای آن وزیر ابوذرجمهر چهر که ماند</p></div>
<div class="m2"><p>بدور عدل تو برطاق شهرت کسری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هنوز این قدم اولین دولت تست</p></div>
<div class="m2"><p>کجاست تا که برآید بغایت القصوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بمان که تا بزند شه بعون خامه تو</p></div>
<div class="m2"><p>بمرز کاشغر اندر همای سایه لوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>امید گاها ده سال رفت کز یکبار</p></div>
<div class="m2"><p>فزون بیزد نیامد محمد بن علی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چرا که یزدان داند که یزدیان از بخل</p></div>
<div class="m2"><p>برای دنیا هر دم دهند صد عقبی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از این گذشته که بخل اقتضای این ملکست</p></div>
<div class="m2"><p>خدات حفظ کند زین طبیعت مسری</p></div></div>