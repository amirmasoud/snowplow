---
title: >-
    شمارهٔ ۴۲ - در تهنیت عید رمضان
---
# شمارهٔ ۴۲ - در تهنیت عید رمضان

<div class="b" id="bn1"><div class="m1"><p>مه شوال چو برکوه فرابست کمر</p></div>
<div class="m2"><p>روزه بگریخت چنان کش نتوان یافت اثر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخت بود این رمضان سست ندانستم من</p></div>
<div class="m2"><p>ورنه کی بستمش اینگونه برتخت کمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدی آن عربده واعظ و عجب زاهد</p></div>
<div class="m2"><p>کزمه نو به یکی چشم زدن گشت هدر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقریانرا که گلو بود چو قمری پر باد</p></div>
<div class="m2"><p>نک هلال آمد و افشرد به حنجر خنجر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الغرض چون مه شوال برافراشت علم</p></div>
<div class="m2"><p>دید از روزه جهانرا همگی زیر و زبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه بیک جام شراب و نه بیک چنگ رباب</p></div>
<div class="m2"><p>نه بیک مجلس شهد و نه بیک کاخ شکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسته جان شیخ و نوان تفته روان شوخ جوان</p></div>
<div class="m2"><p>خونجگر خرد و کلان سوخته دل ماده و نر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مطربان آمده خاموش چو پر بسته هزار</p></div>
<div class="m2"><p>شاهدان گشته سیه پوش چو بگرفته قمر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیک بد دل شد و بر حالت گیتی افسرد</p></div>
<div class="m2"><p>که مگر عمر بسر برد و بپا شد محشر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ورنه کو مغبچگانی که زمن ماند بپار</p></div>
<div class="m2"><p>همه در مهر بهشت و همه در کینه سقر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو نشد آن قصر برافراخته همچون فردوس</p></div>
<div class="m2"><p>چون شد آن باده افروخته همچون کوثر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس سفیری طلبید و بفلک داد پیام</p></div>
<div class="m2"><p>کای درون پر زمعانی و برون پر زصور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چیست این فتنه که بینم زتو در ملک پدید</p></div>
<div class="m2"><p>که خود از سایه فرزند گریزد مادر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مگر از کوکبه ام نیست ترا آگاهی</p></div>
<div class="m2"><p>که برتیغ من انداخته خورشید سپر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فوجها دارم کوشنده مثال ضرغام</p></div>
<div class="m2"><p>توپها دارم غرنده مثال اژدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فلک از بیم بلرزید و چنین داد جواب</p></div>
<div class="m2"><p>کای بخدام تو رضوان جنان فرمانبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ملکتی را که تواش پار نهادی بامن</p></div>
<div class="m2"><p>داشتم هر دمش از خلد برین نیکوتر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم نشاندم بچمن برلب هر چشمه درخت</p></div>
<div class="m2"><p>هم فشاندم بدمن بر سر هر لاله مطر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لیک ماه رمضان دید چو ملکی اینسان</p></div>
<div class="m2"><p>رغبت آورد و طمع کرد و برون تاخت حشر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس بخواند ازو زرایش رجب و شعبانرا</p></div>
<div class="m2"><p>گفت جاسوس وش آرید براین خطه گذر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر که بینید فریبید زمن او را دل</p></div>
<div class="m2"><p>چه بعقل و چه بنقل و چه بزور و چه بزر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن دوتن نیز پس از هم برسیدند بملک</p></div>
<div class="m2"><p>به برون طالب خیر و بدرون صاحب شر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرکجا از که و مه جشنی و جوشی دیدند</p></div>
<div class="m2"><p>خیرخواهانه نمودند در آن خیل مقر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اولا گفتند ای قوم حذر از شوال</p></div>
<div class="m2"><p>که ورا نیست زدنیاوز دین هیچ خبر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گه درین فکر که کی ساده رسد از خلخ</p></div>
<div class="m2"><p>گه درین ذکر که کی باده رسد از خلر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لیک از آنجای که بدبار خدا یار شما</p></div>
<div class="m2"><p>رمضان زد بشهی طبل و رسد با لشکر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فیض عقبی بودش غالیه سا بر ایمن</p></div>
<div class="m2"><p>عیش دنیا بودش نافه گشا برایسر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خلق نادیده و نشناخته گفتند بهم</p></div>
<div class="m2"><p>آفرین زین ملک راد رعیت پرور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رمضان نیز شبانگاه درآمد در ملک</p></div>
<div class="m2"><p>محتسب خواند و عسس راند بهر راهگذر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفت هرکس که برد ساده ببریدش پی</p></div>
<div class="m2"><p>گفت هرکس که خورد باده بکوبیدش سر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نان اگر خواست کسی گفت که بر خوان خلیل</p></div>
<div class="m2"><p>آب اگر جست تنی گفت که در جام خضر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر چه گفتم رمضانا بهراس از شوال</p></div>
<div class="m2"><p>جان مکن جور مکن گنج منه رنج مبر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نشنید از من وزد آتشی آنسان در ملک</p></div>
<div class="m2"><p>که ز دودش بچکید اشک زچشم اختر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفت شوال مخور غم که بجان و سرمیر</p></div>
<div class="m2"><p>به مه روزه همین گاه نمایم کیفر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پس زجا جست و میان بست و براند اسب وکشید</p></div>
<div class="m2"><p>تیغ از ماه نو و زد بدل روزه شرر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>راست گفتی که امیر است و به پیکار عدو</p></div>
<div class="m2"><p>تاخته یکتنه شمشیر زن و جنگ آور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بت شکن دادگر عهد بر اهیم خلیل</p></div>
<div class="m2"><p>که بایوان همه بحر است و بمیدان آذر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در وغا برکند از تن زدلیری جوشن</p></div>
<div class="m2"><p>گاه کین بفکند از سر زشجاعت مغفر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سطوت آموخته از برق پرندش آتش</p></div>
<div class="m2"><p>سرعت اندوخته از سیر سمندش صرصر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ماه بی عون لوایش نفروزد به افق</p></div>
<div class="m2"><p>مهر بی یاری تیغش ندمد از خاور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ننهد وحش بجز درکنف خیلش گام</p></div>
<div class="m2"><p>نزند طیر بجز برطرف میلش پر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جیشها داده هزیمت که ز انجم افزون</p></div>
<div class="m2"><p>حصنها کرده مسخر که ز گردون برتر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پای پیک آبله زد دست دبیر آفت یافت</p></div>
<div class="m2"><p>بسکه برد آن و نوشت این خبر از فتح دگر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آنچه من دیدم از او صد یکش از بر شمرم</p></div>
<div class="m2"><p>در هزاران کس ده تن ننماید باور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همه بگذار چو شد یزد پر از شورش عام</p></div>
<div class="m2"><p>خاصه وقتی که تهی بود زلشکر کشور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>لب ارذال کز آرامی گیتی بدخشک</p></div>
<div class="m2"><p>فتنه کردند که سازند مگر کامی تر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سوی هر خانه دویدند بصمصام وسنان</p></div>
<div class="m2"><p>در هر دکه گشادند بکوپال و تبر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این دوان تا که زنی را کشد از دامن شوی</p></div>
<div class="m2"><p>و آن روان تا پسری را ستد از چنگ پدر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>این بفریاد که بس شاه جهانرا او رنگ</p></div>
<div class="m2"><p>آن به بیداد که بس میر زمانرا افسر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از غبار رهشان چشم کواکب شد کور</p></div>
<div class="m2"><p>از غو نعره اشان گوش ملایک شد کر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>آنچه اشراف بلد داد زدندی کایقوم</p></div>
<div class="m2"><p>تخم در شوره مکارید نبخشید ثمر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خواهشی چند نمودند که تحسین به یزید</p></div>
<div class="m2"><p>مطلبی چند سرودند که رحمت به عمر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>باری از این شغب و شور چو لختی بگذشت</p></div>
<div class="m2"><p>دل آگاه امیر آمد از آن مستحضر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>آنچنان شد غضب آلوده که مژگان نگار</p></div>
<div class="m2"><p>آنچنان گشت بر آشفته که زلف دلبر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گفت پیدا نشد این بد مگر از نیکی من</p></div>
<div class="m2"><p>شاخ نیکی منشانید که بد آرد بر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>پس برآمد بسمند و بکف آورد کمند</p></div>
<div class="m2"><p>خود ننهاده بسر خفتان ننموده ببر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چرخ بگرفت عنانش که بگو با مریخ</p></div>
<div class="m2"><p>فتح بوسید رکابش که بفرما به ظفر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هم قدر گفت بمان منت خود نه بقضا</p></div>
<div class="m2"><p>هم قضا گفت مرو خدمت خود ده بقدر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>او نپذیرفت زکس برخی و فرمود بخصم</p></div>
<div class="m2"><p>آنچه را صولت حیدر به یهود خیبر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نور چهرش چو درخشید بر آن تیره دلان</p></div>
<div class="m2"><p>آنهمه آتش افروخته شد خاکستر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>آن یک از خوابگه موش همی جست مناص</p></div>
<div class="m2"><p>و آن یک از کلبه خرگوش همی خواست مفر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>داورا بنده دیهیم تو تاخ الشعر است</p></div>
<div class="m2"><p>که چنو بنده کم آورده بکیهان داور</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ولی ازکید خضر باشدم آن قدر ملال</p></div>
<div class="m2"><p>که اگر بار دهی رخت کشم سوی سفر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>من در این مرز چنانم که بمعدن یاقوت</p></div>
<div class="m2"><p>من در این بوم چنانم که بدریا گوهر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شعر دلکش چه فزاید چو لئامت بفحول</p></div>
<div class="m2"><p>دختر بکرچه زاید چو عنن در شوهر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>مهر توتسته بقلاده مرا همچون شیر</p></div>
<div class="m2"><p>ورنه در بیشه افلاک فکندم اخگر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تادمد آینه مهر و چمد ساغر ماه</p></div>
<div class="m2"><p>عمر خضرت بود و طنطنه اسکندر</p></div></div>