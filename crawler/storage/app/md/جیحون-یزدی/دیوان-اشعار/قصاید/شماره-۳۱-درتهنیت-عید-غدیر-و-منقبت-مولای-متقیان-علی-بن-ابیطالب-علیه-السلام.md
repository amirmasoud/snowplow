---
title: >-
    شمارهٔ ۳۱ - درتهنیت عید غدیر و منقبت مولای متقیان علی بن ابیطالب علیه السلام
---
# شمارهٔ ۳۱ - درتهنیت عید غدیر و منقبت مولای متقیان علی بن ابیطالب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>بت من که از لطافت بودش ز روح عنصر</p></div>
<div class="m2"><p>نه چنان لطیف کآید بخیال یا تصور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر قامتش بطوبی شکن آورد کروبی</p></div>
<div class="m2"><p>بر طلعتش ز خوبی بمزاج گل تکسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بودش بدوش کاکل چو بسر و ناز سنبل</p></div>
<div class="m2"><p>رخکش چو خرمنی گل لبکش چو حقه در</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه عجب دل ار زعالم بجفای اوست خرم</p></div>
<div class="m2"><p>چوویی برای من کم چو منی برای وی پر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدمی که در تحرک سوی او شود تبرک</p></div>
<div class="m2"><p>سزد ارهمی بتارک کند آن قدم تفاخر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه گر چه در تجرع نتوان از او تمتع</p></div>
<div class="m2"><p>بدو طره اش تواضع بدو چهره اش تکبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برغیر با تلطف زده باده تالف</p></div>
<div class="m2"><p>همه برمنش تکلف همه با منش تغیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دم خلد در وصالش دل نافه برده خالش</p></div>
<div class="m2"><p>جگر گل از جمالش بگداخت در تحسر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صنما غدیرخم شد گه می زدن زخم شد</p></div>
<div class="m2"><p>دل خصم از اشتلم شد بتزاید تکدر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هله آنکه را حسد بد بنشست برخرخود</p></div>
<div class="m2"><p>چو نبی بامر حق شد ز بر جهاز اشتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه چنان ولی ذوالمن ز رسول شد معین</p></div>
<div class="m2"><p>که بود برای یک تن ره طفره وتعذر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شه دین علی عالی دل حق ولی والی</p></div>
<div class="m2"><p>که از و علی التوالی بفلک شودتذکر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مه برج آفرینش جلوات شمس بینش</p></div>
<div class="m2"><p>ثمر درخت دانش در لجه تبحر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شرر دل ضیاغم بشر ملک قوایم</p></div>
<div class="m2"><p>که باژدهای صارم برد ازیلان تنمر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه بکشورش تناهی نه بلشکرش ملاهی</p></div>
<div class="m2"><p>زده کوس لا الهی به ارائک تجبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ملکا امیر نحلا اسد الاسود فحلا</p></div>
<div class="m2"><p>زتوجه تو رحلا بعوالم تکنر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زکلیم رب ارنی که بطور گفت و دانی</p></div>
<div class="m2"><p>پس نفی لن ترانی توئی آن ولیکن انظر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازل و ابد غلامت مه و مهر کاس و جامت</p></div>
<div class="m2"><p>نتوان زد از مقامت دم خبرت از تفکر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جبروت خرگه تو ملکوت جرگه تو</p></div>
<div class="m2"><p>بغبار درگه تو بود آیت تبصر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حرم خدای سبحان کند استلامش ازجان</p></div>
<div class="m2"><p>کل کوی تو بدوران بپذیرد ارتحجر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رخ تست قبله کن همه سوئیش تمکن</p></div>
<div class="m2"><p>نه سزد بوی تیامن نه بود در او تیاسر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زنقود و صفت ارجو که شوم بعز خواجو</p></div>
<div class="m2"><p>برشه اگر نکور و بدر آید از تعیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شرف البقا وجودش تحف التقا سجودش</p></div>
<div class="m2"><p>لب عالمی زجودش به تحمد و تشکر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فرحت به المحاضر علم الجهار والسر</p></div>
<div class="m2"><p>هومن افاخم البر هومن اکارم الحر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نفر ازد از قضا قد نفروزد از قدرخد</p></div>
<div class="m2"><p>چو نشیند او بمسند بفضایل تدبر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زهی ای ستوده کیشت فر از آفتاب بیشت</p></div>
<div class="m2"><p>طپد آسمان به پیشت چو تذرو نزد سنقر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سخطت جنود اخگر سخنت عقود اختر</p></div>
<div class="m2"><p>علم تو جالب الشر قلم تو کاشف الضر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سمن مراد چهرت چمن خدم سپهرت</p></div>
<div class="m2"><p>نسمات صبح مهرت بارم کند تمسخر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خهی آن نجیب توسن که شوی برآن در اوژن</p></div>
<div class="m2"><p>بود از سم چو آهن شخ و دره کوب و ره بر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز درنگ رخ چو تابد بوغا چنان شتابد</p></div>
<div class="m2"><p>که بعقل هم نیابد صفتی از او تبادر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه باوج فایض نه سکونش از حضایض</p></div>
<div class="m2"><p>ظفرش غلام رایض فلکش امیر آخور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هله تا که اهل سنت بمباحت امامت</p></div>
<div class="m2"><p>نزنند گوی دولت بر شیعی از تشاجر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بتو التجا جهان را زتو ارتقا زمان را</p></div>
<div class="m2"><p>به سعادت اختران را پی جاه تو تظاهر</p></div></div>