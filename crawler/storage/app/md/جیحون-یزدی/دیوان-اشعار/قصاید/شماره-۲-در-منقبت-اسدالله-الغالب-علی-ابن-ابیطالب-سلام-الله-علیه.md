---
title: >-
    شمارهٔ ۲ - در منقبت اسدالله الغالب علی ابن ابیطالب سلام الله علیه
---
# شمارهٔ ۲ - در منقبت اسدالله الغالب علی ابن ابیطالب سلام الله علیه

<div class="b" id="bn1"><div class="m1"><p>گفتم بعقل دوش که ای آیت هدی</p></div>
<div class="m2"><p>گشت اول از مشیّت که کفر و دین بنا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی نور دین و ظلمت کفر انتشار داشت</p></div>
<div class="m2"><p>گر حکمت حکیم نمیکردی اقتضا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از قدرت که بر سر بازار اختلاف</p></div>
<div class="m2"><p>این شوخ پارسی شد آن شیخ پارسا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرمود اراده که بر ابلیس وسوسه</p></div>
<div class="m2"><p>تا او ز سجده کردن آدم کند ابا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در رشته ازل همه بودند اگر گهر</p></div>
<div class="m2"><p>ماهیّت گهر که کند قلب جز خدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خلقت نجوم بود یکسر آفتاب</p></div>
<div class="m2"><p>خود کی یکی سهیل شود دیگری سها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حق تفاوتی بیکایک نهفته است</p></div>
<div class="m2"><p>پس از خزف چه میطبد فعل کهربا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از یک عنا صر است نباتات اگر پدید</p></div>
<div class="m2"><p>پس از چه نیست خارچو نسرین فرح فزا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باده باین رطوبت و گرمی است از چه رو</p></div>
<div class="m2"><p>افیون باین یبوست و سردیست از چرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر حسب ذات اگر همه را طایر آفرید</p></div>
<div class="m2"><p>از کیست این تباین عصفور با هما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تحبیب هر دلی اگر از اهتمام اوست</p></div>
<div class="m2"><p>بوجهل را که داد خصومت بمصطفی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست صنایع که بزهدان انجماد</p></div>
<div class="m2"><p>یاقوت ولعلرا بدو رنگی فشرد پا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زمرد چه حیله پخت که افعی نمود کور</p></div>
<div class="m2"><p>اثمد چه جلوه ساخت که گردید توتیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما را با تضاد حرام و حلال چه</p></div>
<div class="m2"><p>بی میل رازق ار نرسد رزق ما سوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر ما خوریم خمر چه روزی دهیست او</p></div>
<div class="m2"><p>ور او نصیب کرده چه بد میکنیم ما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این سلب علم خالق از اشیا نمودنست</p></div>
<div class="m2"><p>گوئی گرم محک زند از عیش و ابتلا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یعنی نداند آنکه صبوریم یا جهول</p></div>
<div class="m2"><p>یعنی نفهمد آنکه لجینیم یا طلا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>القصّه عقل چون سخنانم شنید گفت</p></div>
<div class="m2"><p>در حل این عقود بمولا کن التجا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاه نجف علی ولی کز توجهش</p></div>
<div class="m2"><p>مسجود اتقیا شود اشباح اشقیا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بس منجلی است زآینه اش نور ایزدی</p></div>
<div class="m2"><p>هر موی او رموز انا الحق کند ادا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آب رخش مکشف اسرار ذوالمنن</p></div>
<div class="m2"><p>خاک رهش مکون ارواح اولیا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ثعبان سطوتش فکند عکس اگر بچوب</p></div>
<div class="m2"><p>لرزد کلیم را بدن از هیئت عصا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در عالمی که خرگه نام جلال اوست</p></div>
<div class="m2"><p>ابعاد آن برون رود از ننگ انتها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باید کزین حدوث خرد خواندش قدم</p></div>
<div class="m2"><p>اثبات شیی اگر نکند نفی ما عدا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای زیب بخش وحی که از با بسمله</p></div>
<div class="m2"><p>یزدان سور بنام تو کرده است ابتدا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مخلوق تو است هر چه بگیتی بود قدر</p></div>
<div class="m2"><p>مرزوق تو است آنچه بگیهان بود قضا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گردی زآستان تبرای تو الم</p></div>
<div class="m2"><p>دردی ببوستان تولاّی تو شفا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>قیدی کز امتثال تو مستوجب نجات</p></div>
<div class="m2"><p>فقری که باو داد تو مستلزم غنا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رخشد چو مهچه علم اقتدار تو</p></div>
<div class="m2"><p>بر رد شمس کی کند ادراک اکتفا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>درآن زمین که قصد جهان دگر کنی</p></div>
<div class="m2"><p>از هر ستاره شود ایجاد صد سما</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شاها مرا بکاخ فصاحت معاصرین</p></div>
<div class="m2"><p>هر یک نماز برده که روحی لک الفدا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کوته بود بقامت طبع بلند من</p></div>
<div class="m2"><p>دوزند اگر ز اطلس چرخ برین قبا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با این علو پایه کمند بلاغتم</p></div>
<div class="m2"><p>باشد بر اوج قلعه مدح تو نارسا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ارجو که وقت جمع مدیحت رود بچرخ</p></div>
<div class="m2"><p>این حشو زائدی که ترا گفته ام ثنا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا آنکه در سرایر تقدیر کردگار</p></div>
<div class="m2"><p>بیجا بود تفکر این چون و آن چرا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر کس که همچو فکرت جیحون ستایدت</p></div>
<div class="m2"><p>یابد ز شمع رای تو توفیق اهتدا</p></div></div>