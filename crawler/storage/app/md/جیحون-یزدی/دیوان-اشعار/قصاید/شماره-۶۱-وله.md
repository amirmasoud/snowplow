---
title: >-
    شمارهٔ ۶۱ - وله
---
# شمارهٔ ۶۱ - وله

<div class="b" id="bn1"><div class="m1"><p>ماه رمضان تافت از این بر شده طارم</p></div>
<div class="m2"><p>شاهد بتالم شد و زاهد به تنعم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم شیخ سرافراخت بگردون زتعیش</p></div>
<div class="m2"><p>هم شوخ جبین سود بغبرا زتالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین جمله بترگرمی ایام ولیالیست</p></div>
<div class="m2"><p>کافتاده بهر جسم چوآتش که بهیزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلزم شده از تف هوا خشک چو هامون</p></div>
<div class="m2"><p>هامون زده از تابش خورموج چو قلزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق از اثر روزه این فصل چنان مات</p></div>
<div class="m2"><p>کز اصل ندانند تشکر زتظلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناید زدو صد رنج یتیمی بتباکی</p></div>
<div class="m2"><p>ناید بدو صد گنج لئیمی به تبسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم نروند ار زپی خوردن روزه</p></div>
<div class="m2"><p>امسال رود روزه پی خوردن مردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای ترک من ای زهره وش باخته زهره</p></div>
<div class="m2"><p>کز روزه بود ماه تو چون محترق انجم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فکری زپی چاره صوم است مرا پیش</p></div>
<div class="m2"><p>گر زآنکه برکس نکنی هیچ تکلم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این ماه بشب می نتوان خورد ولیکن</p></div>
<div class="m2"><p>در صبح توان خورد بلاترس و توهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زیرا که خلایق همه را صبح برد خواب</p></div>
<div class="m2"><p>دارند به بیداری شبها چو تصمم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر شام بهر ناحیه خیلی است هویدا</p></div>
<div class="m2"><p>در صبح بهر زاویه صد خیل شود گم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه شحنه در آید برواقت که بتاخیر</p></div>
<div class="m2"><p>نه شیخ درآید به وثاقت که مهاقم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با خاطر آسوده بزن جام صبوحی</p></div>
<div class="m2"><p>کآفاق مبراست ز تشویش تهاجم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا چاشت نیابی تنی از خلق بکشور</p></div>
<div class="m2"><p>گر توسنت اندر طلب کس فکند سم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنگه بکبابت شکنم صولت ناهار</p></div>
<div class="m2"><p>وز سر برمت جوش بدان جوش سرخم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس تخت گذاریم و بخوابیم بر آن مست</p></div>
<div class="m2"><p>ما و تو بدانسان که بگردون مه و کژدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زین بیش نباشد که ببینند گرم قوم</p></div>
<div class="m2"><p>بر ضعف روانم همه آرند ترحم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خود نیست خبرشان که چو بخت خوش سرتیپ</p></div>
<div class="m2"><p>عیشم بوفور است و نشاطم بتراکم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نعلی است مه از ابرش او واشده از پی</p></div>
<div class="m2"><p>گوئیست خور از اشقرا و نازده بردم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>افلاک دهد بوسه ورا برطرف ذیل</p></div>
<div class="m2"><p>ابحار برد سجده ورا بر شرف کم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای فخر اقالیم که بر درگهت از بیم</p></div>
<div class="m2"><p>بردوش کشد چرخ زتو بار تحکم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اقبال تو و دور فلک راست توافق</p></div>
<div class="m2"><p>اجلال تو و ملک جهانراست تلازم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فرت چو نیاورد فرو سر بدو گیتی</p></div>
<div class="m2"><p>از شوکت خود ساخت بنا عالم سوم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا عقل ده و چرخ نه و خلد بود هشت</p></div>
<div class="m2"><p>شش دانک وثاق تو بر از طارم هفتم</p></div></div>