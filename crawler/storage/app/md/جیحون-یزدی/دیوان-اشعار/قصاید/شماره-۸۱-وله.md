---
title: >-
    شمارهٔ ۸۱ - وله
---
# شمارهٔ ۸۱ - وله

<div class="b" id="bn1"><div class="m1"><p>رسم نوروز شد امسال مگر دیگرگون</p></div>
<div class="m2"><p>کز زمین جای سمن مهر و مه آید بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من فزون دیدم نوروز ولیکن امسال</p></div>
<div class="m2"><p>زآنچه من دیدم دربوی و برنگست فزون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو پنداری خورده است زیک پستان شیر</p></div>
<div class="m2"><p>بهر ضحاک خزان با علم افریدون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سمن انگاری برده است زیک خامه نگار</p></div>
<div class="m2"><p>بهر تزیین چمن با ورق انگلیون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساده بسته است بخود طنطنه اسکندر</p></div>
<div class="m2"><p>باده باده است گرو از خرد افلاطون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ریزد این سیل زکهسار همی یاکه سحر</p></div>
<div class="m2"><p>رگی از عمان بگشاده فلک برهامون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیزد این نسترن از باغ همی یا که بزرق</p></div>
<div class="m2"><p>برخی از اختر دزدیده زمین ازگردون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقش جسته است زمین باز زچهر لیلی</p></div>
<div class="m2"><p>آب خورده است فلک باز زچشم مجنون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قمری پاری و بلبل بچه امسالی</p></div>
<div class="m2"><p>سازد کردند نواها بدگرگون قانون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلد گوئی که بگیتی شد و عقبی بگذشت</p></div>
<div class="m2"><p>کاین همه روح و طرب گشت بگیتی مقرون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لعبتان حور سیر مغبچگان مانا از برف</p></div>
<div class="m2"><p>چهره تصویر جنان گیسو زنجیر جنان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رخشان خونی کانگیخته غلمان بر</p></div>
<div class="m2"><p>تنشان برفی کآمیخته گویا با خون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طفلها بینی با طره چون بر غراب</p></div>
<div class="m2"><p>لیک ازجامه الوان همه چون بوقلمون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیرها یابی با شیبت چو نکف کلیم</p></div>
<div class="m2"><p>لیک در خرقه زربفت همه چون قارون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترک من نیز بر آراسته اند ام چو سرو</p></div>
<div class="m2"><p>از قبائی که برش رخت شقایق مرهون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تاج از مشک بسرکفش زگلبرگ بپای</p></div>
<div class="m2"><p>جامه از زربه برون کرته ززیبق بدرون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لیکن از بسکه لطیف است تن و جامه او</p></div>
<div class="m2"><p>خود چه پنهان ز تو پیداست درونش زبرون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شانه پیمود برآن سنبل پر حلیت و فن</p></div>
<div class="m2"><p>سرمه اندوده بران نرگس پر مکر و فسون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هفت سین چیده و می خورده و زیور بسته</p></div>
<div class="m2"><p>وزشعف گاه زند بربط وگاهی ارغون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا محول شودش حال سوی احسن حال</p></div>
<div class="m2"><p>خواندن مدحت سرتیپ بخود کرده شگون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>داور مصطفوی نام که از برق حسام</p></div>
<div class="m2"><p>خصم را بولهبی کله کند چون کانون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نامش اربر پر پروانه فرو خواند کس</p></div>
<div class="m2"><p>جا کند برسرشمع و بود از شعله مصون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اوج گردون نبود همچو حضیض در او</p></div>
<div class="m2"><p>زآنکه این رتبه والا نشود یافت ز دون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بس عجب نی که زهشیاری و بیداردلی</p></div>
<div class="m2"><p>مستی افتد زمی و خواب رود از افیون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رایش ار در بر خورشید شود روی بروی</p></div>
<div class="m2"><p>روشنت گردد کاین مغتنم است آن مغبون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بخت او راست بدان پایه ببالائی میل</p></div>
<div class="m2"><p>ببزمش نچکد درد می از جام نگون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این که میگفت که با آن همه رنج ایران را</p></div>
<div class="m2"><p>چون باندک زمن ازگنج نمودی مشحون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حزم او بهتر از اول بتواند افراخت</p></div>
<div class="m2"><p>نهم ایوان شود اربر سرکیوان وارون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای امیریکه زدرک شرف خدمت تو</p></div>
<div class="m2"><p>دهر از کوکب اقبال خود آمد ممنون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آسمان راست بمعماری کوی توهوس</p></div>
<div class="m2"><p>اینک از مهر مه آورده دوخشتش بنمون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گشته بربارگه و تخت و زمان تو بجان</p></div>
<div class="m2"><p>چرخ شیدا و زمین عاشق وکیهان مفتون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از غبار قدم و نعل سم ابرش تو</p></div>
<div class="m2"><p>نصرت و فتح بیاراسته آذان و عیون</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کلک تقدیر کند مطلع دیوان وجود</p></div>
<div class="m2"><p>آنچه تدبیر ترا نقش پذیرد بکمون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هرکه جز رای تو جست ارهمه خورشید بود</p></div>
<div class="m2"><p>زنده در گل شود اندر پی عبرت مدفون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای سراج الامراکت زفر سجده تخت</p></div>
<div class="m2"><p>یافته منصب تاج الشعرائی جیحون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در عیان گر به ثنای تو تکاهل رخ داد</p></div>
<div class="m2"><p>در نهان کام تغافل نزدم تا بکنون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بنده مدیون زثنا ذات تو مدیون زعطا</p></div>
<div class="m2"><p>لیکن الحمد کزین سوی ادا گشت دیون</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا بهر سال یکی گاخ برآرد کلبن</p></div>
<div class="m2"><p>که زیاقوت و زمرد بودش شقف وستون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به عهود و بقرون جان و دل تو پیروز</p></div>
<div class="m2"><p>کز تو پیروز دل و جهان عهود است و قرون</p></div></div>