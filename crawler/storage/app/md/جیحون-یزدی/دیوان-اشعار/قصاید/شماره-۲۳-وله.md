---
title: >-
    شمارهٔ ۲۳ - وله
---
# شمارهٔ ۲۳ - وله

<div class="b" id="bn1"><div class="m1"><p>جشن میلاد شه دنیا و ما فیها بود</p></div>
<div class="m2"><p>چرخ جان افشان زمین شادان جهان شیدا بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز پنداری کلیمی رب ارنی گوی شد</p></div>
<div class="m2"><p>کز تجلی طور ایران سینه سینا بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه پشت آسمان برسجده کاخش دوتاست</p></div>
<div class="m2"><p>لیک برروی زمین از جاه و فریکتا بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هان چو حق یکتای بی همتاست با برهان عقل</p></div>
<div class="m2"><p>لاجرم این ظل حق یکتای بی همتا بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفرینش را عیان شد مظهری کز فره اش</p></div>
<div class="m2"><p>آفرین ها برروان آدم وحوا بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی همانا بوالبشر را رجعتی افتاده باز</p></div>
<div class="m2"><p>زآنکه تاج تارکش از علم الاسما بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخت رام و دهر آرام و می بهجت بجام</p></div>
<div class="m2"><p>خارها گل زهر هامل پستها بالا بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسروی شد ناصرالدین فرقه اسلام را</p></div>
<div class="m2"><p>کز حقیقت هر مجازی باز بزم آرا بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می نبی ساقی نبی میخانه ری میخواره شاه</p></div>
<div class="m2"><p>بانک قولوا لا الهش غلغل مینا بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناصری کوکب بتا زین موکب میلاد جشن</p></div>
<div class="m2"><p>گاه رامش وقت نازش نوبت صهبا بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر طرف رقاصکی برجر اثقالش وقوف</p></div>
<div class="m2"><p>کوه بر مو بسته اش گه زیر وگه بالا بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رحل بر کف چشم برصف رقص بر قانون دف</p></div>
<div class="m2"><p>وز سقایت کشته خود را پی احیا بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>توپ شهر آشوب ثهلان کوب کشور روب بین</p></div>
<div class="m2"><p>کو فراز چرخه چون برچرخ اژدرها بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دود او ابریست کش بانگ و شرر رعد است و برق</p></div>
<div class="m2"><p>بلکه از روئین تگرگش ابر طوفان زا بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای بت پیمان گسل پیمانه ده کز پایکوب</p></div>
<div class="m2"><p>بانگ سرمستان ز دستان آسمان پیما بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>انبساط کوس جیش شه نگر کاندر سلام</p></div>
<div class="m2"><p>قلب از وحی رنج ازو طی پیر ازو برنا بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خسرو صاحبقران شه ناصرالدین کز شرف</p></div>
<div class="m2"><p>گوی چوگان نفاذش گنبد خضرا بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صارم آفاق گیرش در ملمع گون غلاف</p></div>
<div class="m2"><p>صبح را ماند که پنهان در شب یلدا بود</p></div></div>