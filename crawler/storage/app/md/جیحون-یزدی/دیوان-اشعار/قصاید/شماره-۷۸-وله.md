---
title: >-
    شمارهٔ ۷۸ - وله
---
# شمارهٔ ۷۸ - وله

<div class="b" id="bn1"><div class="m1"><p>کیست آن خسرو شیرین سخن و شور افکن</p></div>
<div class="m2"><p>که بمو غارت مرد است و برو فتنه زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل صد شام سر زلف ورا در دنبال</p></div>
<div class="m2"><p>خون صد صبح بناگوش ورا برگردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طره و رخ بودش برق زن اندر برقع</p></div>
<div class="m2"><p>سینه و تن بودش جلوه گر از پیراهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای همه سلسله ام برخی آن طره و رخ</p></div>
<div class="m2"><p>وی همه طا یفه ام بنده آن سینه و تن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمش از مژه چو مریخ که گیرد خنجر</p></div>
<div class="m2"><p>چهره از طره چو خورشیدکه پوشد جوشن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه چمد سرو چو افراخته قدش در باغ</p></div>
<div class="m2"><p>نه دمد لاله چو افروخته خدش بدمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشتری دلشده عارض آن ماه جبین</p></div>
<div class="m2"><p>ماه نوشیفته ابروی آن زهره ذقن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا که دیدم بسیه طره اش آن روی سپید</p></div>
<div class="m2"><p>این مثل گشت مجرب که شب است آبستن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاکه روئید خط سبزش ازآن گونه سرخ</p></div>
<div class="m2"><p>این سخن گشت مسلم که برد سبزه حزن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوش هی خورد می وگفت بمن کو مستی</p></div>
<div class="m2"><p>پس چرا هیچ گهی مست نمیگردم من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم آری بر چشمان تو ای تازه جوان</p></div>
<div class="m2"><p>مستی از وی ببرد گرچه بود صاف وکهن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تنگ سازد دل بگشاده ام اندرگه وصل</p></div>
<div class="m2"><p>چون کنم جهد که بوسم مگرش تنگ دهن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل من با دل او جنگد و بس بوالعجب است</p></div>
<div class="m2"><p>شیشه را که کند سینه سپر با آهن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داند از رندی گویا که منش شیفته ام</p></div>
<div class="m2"><p>که بمی خوردن و خلوت نشود پیرو من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با همه کودکی آن گونه بعقل است بزرگ</p></div>
<div class="m2"><p>که کس از دبه او برد نیارد روغن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دی ورا دیدم در باغ که خوش خفته بناز</p></div>
<div class="m2"><p>وزسمن ساخته دور بر بستر خرمن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بارها گه بسمن گه برخش سودم دست</p></div>
<div class="m2"><p>رخ او بود بسی نرم تر از برگ سمن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کارها داند در شاهدی و دلداری</p></div>
<div class="m2"><p>که ندانسته بت خلخ و شوخ ارمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شب چو می نوشد و بر زینت مجلس کوشد</p></div>
<div class="m2"><p>یکدام از سحر کند خلق دوصد گونه سخن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گه نهد زلف بدستم که شنیدی هرگز</p></div>
<div class="m2"><p>با چنین بوی کسی مشک بیارد زختن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گه بلب آورد انگشت که دیدی هرگز</p></div>
<div class="m2"><p>با چنین رنگ عقیقی رسد از کان یمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دلبرم سخت بود زیرک و زیرک باید</p></div>
<div class="m2"><p>دلبر چاکر درگاه خداوند زمن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آصف جم حشم پاک گهر سعدالملک</p></div>
<div class="m2"><p>کز قبولش ز پری باج ستد اهریمن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن هنر خوی درستی طلب نیکی خواه</p></div>
<div class="m2"><p>که بتدبیرر باید ز رخ بحر شکن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پدر چرخ بآوردن چون او عنین</p></div>
<div class="m2"><p>مادر دهر بتولید چو او استرون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه عجب آنکه سخنهای شگرفش شنود</p></div>
<div class="m2"><p>بر گریبان ز شعف چاک زند تا دامن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر سخن برلحد مرده صد ساله کند</p></div>
<div class="m2"><p>زندگی یابد و برتن درد از وجد کفن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>طایر فکرت او را چه غم از کید نجوم</p></div>
<div class="m2"><p>کاین نه مرغیست که دردام فتد ازارزن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همت عالی او را چه طمع بر دنیا</p></div>
<div class="m2"><p>کاین نه مردیست که از ره رود از عشوه زن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>او در اصلاب گرامی پدران بد ورنه</p></div>
<div class="m2"><p>کی مثل شد علم کاوه و تیغ قارن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دور عدل وی اگر زنده شوند آن مردم</p></div>
<div class="m2"><p>بدهانش نرسد دست دراز از بهمن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رازها داند زین کارگه کون و فساد</p></div>
<div class="m2"><p>رمزها بیند در آینه سر و علن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آنچه در حوصله اوست ودیعت زخدای</p></div>
<div class="m2"><p>گر بجبریل کنی قصه برآرد شیون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای مهین میر که در محفل تو پیر خرد</p></div>
<div class="m2"><p>همچو طفلی است که ناشسته لبان را زلبن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هرکجا چامه سرائی شود از در دریا</p></div>
<div class="m2"><p>هرکجا بدره فشانی شود از زر معدن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مشک بیزنده خطت بر ورق کافوری</p></div>
<div class="m2"><p>شب تاریست کزو چشم کواکب روشن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>برغریبی که زند دایر حسن تو خط</p></div>
<div class="m2"><p>پای چون نقطه بدامن کشد از یاد وطن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نظم عمان بتو بخشید شه و گفت بوی</p></div>
<div class="m2"><p>نگر این بحر و عبث آب مسا در هاون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کشتی حزم تو لنگر چو بعمان افکند</p></div>
<div class="m2"><p>دیگر از باد مخالف نپذیرفت فتن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو بعمان شدی و چرخ نوشتش کای بحر</p></div>
<div class="m2"><p>یم ببین خوش بنشین تند مرو جوش مزن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر گهر پروری این گونه بپرور که بود</p></div>
<div class="m2"><p>جای او برسر و دیگر گهران برگرزن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر ای عمان مسکن بتو کرده است نهنگ</p></div>
<div class="m2"><p>این نهنگیست که دارد دل عمان مسکن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>باری اندازه نگهدار وز پهنات ملاف</p></div>
<div class="m2"><p>خود مبادا که بگیرد دهنت را بلجن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>لیک چون گشت ورا ساحل عمان مخیم</p></div>
<div class="m2"><p>گفت باید زگهر ذیل جهان پر مخزن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یکدو غواص باقبال شه انگیخت به یم</p></div>
<div class="m2"><p>بحر دل موج گسل سنگ جگر سیم بدن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>صدفی چند برآورد زدریا چو سپهر</p></div>
<div class="m2"><p>وندر او سلک لئالی عوض عقد پرن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گوئیا گوش صدف بد برهش برآواز</p></div>
<div class="m2"><p>کز گهر کرد پر از ایسر او تا ایمن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بلکه نیل آنچه گهر داشت زریش فرعون</p></div>
<div class="m2"><p>اندر آویخشش از مهر بدم توسن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پس گهرهای ثمین را همه بر یاد ملک</p></div>
<div class="m2"><p>ریخت بر مقدم هر دوست به رغم دشمن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آصفا رانده اورنگ تو تاج الشعراست</p></div>
<div class="m2"><p>که نجوئیش دل سوخته مهما امکن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یاد جیحون بتوشد برآب از عمان</p></div>
<div class="m2"><p>دل قلزم گهرت را نبد اینگونه سنن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یار نو دیدی و یارکهنت رفت ز یاد</p></div>
<div class="m2"><p>مکن این کز تو نبد دیدن واینسان دیدن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تازد اید محن از دل رخ وگیسوی نگار</p></div>
<div class="m2"><p>لطف شاهت زره و حفظ خداوند مجن</p></div></div>