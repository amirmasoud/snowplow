---
title: >-
    شمارهٔ ۲۷ - وله
---
# شمارهٔ ۲۷ - وله

<div class="b" id="bn1"><div class="m1"><p>نگار من چو بخیزد بنارون ماند</p></div>
<div class="m2"><p>ولی اگر بنشیند به نسترن ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگاه تکیه زدن چون بنفشه است ولیک</p></div>
<div class="m2"><p>اگر بخفت بیک تل یاسمن ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدین سرین که مر او راست سخت و شیرینکار</p></div>
<div class="m2"><p>چو بر جهد که بر قصد بکوهکن ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرشته شد مگر از جوهر غزال ختن</p></div>
<div class="m2"><p>که چشم او برم آهوی ختن ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه گردش است بچشمان آن نهان جوان</p></div>
<div class="m2"><p>که در خواص برطلی می کهن ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر زبا غ جنان نامده بسیر جهان</p></div>
<div class="m2"><p>چرا بغلمان از چهرو از ذقن ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر که مردم فردوس را تکلم نیست</p></div>
<div class="m2"><p>که آن نگار بغلمان بی دهن ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر که چهره وجعدش همال روز و شبست</p></div>
<div class="m2"><p>ز من شنو که بیزدان و اهرمن ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درون پیرهن آن پیکر منور او</p></div>
<div class="m2"><p>بآتشی که درافتد به پیرهن ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلم که رانده از دام زلف سرکش اوست</p></div>
<div class="m2"><p>بدان غریب جدا مانده از وطن ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خطش بجانب لب گر چه راهبر باشد</p></div>
<div class="m2"><p>ولی فسون لب او براهزن ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شبی لبش زترنم گهر فشاند بکاخ</p></div>
<div class="m2"><p>صدف درآمد وگفتا که این بمن ماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخشم گفتمش آهسته ران که آن لب لعل</p></div>
<div class="m2"><p>بدر نثار کف میر موتمن ماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وحید عصر محمد علی رئیس نظام</p></div>
<div class="m2"><p>که هر چه مرد بود پیش او بزن ماند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز بس تراکم نعما بود بماحضرش</p></div>
<div class="m2"><p>گمان بری که به آلای ذوالمنن ماند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنین که بدعت اشرار را بپردازد</p></div>
<div class="m2"><p>درست شد که بمحمود بت شکن ماند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدی نخواهد و بد ننگرد بدی نکند</p></div>
<div class="m2"><p>بدستگاه شریعت بحسن ظن ماند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همیشه تابد مد گاه فرودین گل سرخ</p></div>
<div class="m2"><p>ز بخت سبز به آراسته چمن ماند</p></div></div>