---
title: >-
    شمارهٔ ۶۴ - وله
---
# شمارهٔ ۶۴ - وله

<div class="b" id="bn1"><div class="m1"><p>نامه نوشتم بدلربای خود از قم</p></div>
<div class="m2"><p>کای بت شیرین سخن سلام علیکم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قم از ری بطمطراق زدم تخت</p></div>
<div class="m2"><p>لیک بیزد این دو روزه میرسم از قم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم چشمم زیمن خدمت خسرو</p></div>
<div class="m2"><p>ننگرد از کبر بر معارف مردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای سریرشه از سخن بادیبان</p></div>
<div class="m2"><p>فرق من آمد سزای تاج تسلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاد زی ای مه که چون زره رسم آید</p></div>
<div class="m2"><p>طیش و تالم بدل بعیش و تنعم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک نیاوردمت تحف زلطایف</p></div>
<div class="m2"><p>کآنچه بیارم تو را براوست تقدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاقم بهر تو آورم بچه زهره</p></div>
<div class="m2"><p>کاطلس اندام تو است غیرت قاقم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرسله در دهم تو را بچه یارا</p></div>
<div class="m2"><p>کآرد لعلت گهر بگاه تکلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منت خمار هم نمیکشم امسال</p></div>
<div class="m2"><p>ریزم انگور خود ز بهر تو درخم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز خم آرم میی که در مه ساغر</p></div>
<div class="m2"><p>گوئی شمس است یا عصاره انجم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نقصی اگر فی المثل بکارم باقیست</p></div>
<div class="m2"><p>می برم اینجا بنزد خواجه تظلم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سبط پیمبر حسین نام حسن خلق</p></div>
<div class="m2"><p>اول مملوک اخت قبله هفتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش کف راد او گدائی معدن</p></div>
<div class="m2"><p>با دل زخار او فقیری قلزم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم گهرش طیب است از اب براب</p></div>
<div class="m2"><p>هم صدفش طاهر است از ام برام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در فرو اجلال گوید ارلمن الملک</p></div>
<div class="m2"><p>چرخ سراید که قدیکون لانتم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جدش رخ تافته زخرمن هستی</p></div>
<div class="m2"><p>نگذشت آدم اگر زخوشه گندم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در یکی از تنک تر عوالم فضلش</p></div>
<div class="m2"><p>نه فلک پهنه ور چو حلقه شود گم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تخت مهی تازده است بختش در ملک</p></div>
<div class="m2"><p>نیست بتخت شهان مکانت هیزم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یک میل از ارض جاه او نکند طی</p></div>
<div class="m2"><p>خنگ فلک گر بساحتش فکند سم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>میرا برمن نگر که بلبل طبعم</p></div>
<div class="m2"><p>آمده بر شاخ مدح تو بترنم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفته جیحون گزین نه غیر که در شرع</p></div>
<div class="m2"><p>باشد تا آب باطلست تیمم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا که بهر مه در این دوازده منظر</p></div>
<div class="m2"><p>یکبار آید قمر بجانب کژدم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دشمن از زمردین عمامه تو کور</p></div>
<div class="m2"><p>گر همه تن افعی است از دم تا دم</p></div></div>