---
title: >-
    شمارهٔ ۱۷ - وله
---
# شمارهٔ ۱۷ - وله

<div class="b" id="bn1"><div class="m1"><p>رخشد از چهره همی جلوه شمس و قمرت</p></div>
<div class="m2"><p>مگر از مهر بود ما در و از مه پدرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پدر و مادرت از ماه و زمهر است مگر</p></div>
<div class="m2"><p>که برخساره بود جلوه شمس و قمرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بدین طره و رخسار بهر جا گذری</p></div>
<div class="m2"><p>سنبل و لاله همی بردمد از رهگذرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل صد خیل بموی تو و مو بر بن گوش</p></div>
<div class="m2"><p>وین عجب تر که نی از ناله ایشان خبرت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بیک دل جگرم خون شده ازغصه تو</p></div>
<div class="m2"><p>چون کنی با دل صد خیل بنازم جگرت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو بمن دشمن و من زان رخ نیکو که تراست</p></div>
<div class="m2"><p>دوست تر دارم هر روز ز روز دگرت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه دم بیشتر از پیشترم دل ببری</p></div>
<div class="m2"><p>چون نخواهم همه دم بیشتر از پیشترت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سروی اما ننهی پای بهر گلشن و کوی</p></div>
<div class="m2"><p>مهی اما نتوان دید بهر بام و درت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرنه مه زچه در کف نفتد دامن تو</p></div>
<div class="m2"><p>ورنه سرو چرا بر نخوریم از ثمرت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بهر پرده بدین مو که که تو داری مگریز</p></div>
<div class="m2"><p>که شود بوی به از نافه او پرده درت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر بمغرب تو بدین موی گریزی ازمن</p></div>
<div class="m2"><p>که بمشرق من از او بوی برم بر اثرت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با وصالت بزمستان نفروزم آتش</p></div>
<div class="m2"><p>که بگرمی نگرم ثانی سوزان شررت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در حضورت ببهاران نکنم یاد چمن</p></div>
<div class="m2"><p>که بنرمی شمرم تالی نسرین ترت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خواهمت بوسه زنم گه بقدم گاه بفرق</p></div>
<div class="m2"><p>کز خدا ختم نکوئی است بسیمینه برت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پای تا سرهمگی درخور بوسی وکنار</p></div>
<div class="m2"><p>نه شگفت ارنکنم فرق زپا تا بسرت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چند گوئی که مجو از لب شیرینم کام</p></div>
<div class="m2"><p>ورنه گویم سخنی تلخ و بد و جان شکرت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو کجا و سخن تلخ که شیرین گردد</p></div>
<div class="m2"><p>چون برآید زمیان لب همچون شکرت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یاد داری که بمستی شبکی پرسیدی</p></div>
<div class="m2"><p>که من و ماه کدامیم به اندر نظرت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفتم ازمه تو بهی لیک اگر بپسندد</p></div>
<div class="m2"><p>از پی خدمت خود داور والا گهرت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>راد شهزاده آراسته سیف الدوله</p></div>
<div class="m2"><p>که شود چرخ غلامت بگزیند اگرت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وارث تخت شهی آنکه سپهرش گوید</p></div>
<div class="m2"><p>کای مه و منطقه قربان کلاه وکمرت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دید تیغش چو ببر دهر بدو داد پیام</p></div>
<div class="m2"><p>که بزی خوش که بود تابع فرمان ظفرت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یافت کلکش چو بکف چون بدو برد نماز</p></div>
<div class="m2"><p>که بمان شاد که شد سخره قدرت قدرت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که زمن مژده بسوی عضدالدوله برد</p></div>
<div class="m2"><p>کز نیاکان تو افزود شکوه پسرت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این پسر را که تو داری نه عجب گر رضوان</p></div>
<div class="m2"><p>آید از خلد پی تهنیت از بوالبشرت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای محمد سیر و نام کز اخلاق نکو</p></div>
<div class="m2"><p>گشته ضرب المثل اندر همه عالم سیرت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>توئی آن دوحه گلزار فتوت که بود</p></div>
<div class="m2"><p>مردمی شاخ و شرف برگ و فتوحات برت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ور بدنیاست وجود تو بعقبی مانند</p></div>
<div class="m2"><p>واندر او لطف و غضب جای جنان و سقرت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جانب کوه وری چند بکین تازی اسب</p></div>
<div class="m2"><p>که دمد نام خدا چرخ بدفع خطرت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سینه اسب تو پرشد مگر ازکشتی نوح</p></div>
<div class="m2"><p>که جهانیش بطوفان و نباشد حذرت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گرتو با این دل و این زهره سوی بیشه چمی</p></div>
<div class="m2"><p>روبهم گر ننهد پنجه همی شیر نرت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سخت تر از تو دلیری نشنیدم به نبرد</p></div>
<div class="m2"><p>خلق کرده است مگر بار خدای از حجرت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که برازنده تر از تست بهیجا که بود</p></div>
<div class="m2"><p>توسن از چرخ و سپر ازمه و مغفر زخورت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>توئی آن سرو سهی قامت فرخنده لقا</p></div>
<div class="m2"><p>که بود کاخ فلک ناصردین کاشمرت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چشم شه بر رخ تو گوش تو برگفته شاه</p></div>
<div class="m2"><p>که ایازیست بنزد شه محمود فرت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>داورا چاکر دیهیم تو تاج الشعر است</p></div>
<div class="m2"><p>که خجل ماند ه زالطاف برون ازشمرت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>لطف تو پیش ملک پایه من بس بفزود</p></div>
<div class="m2"><p>که بسی پایه فزاید ملک دادگرت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شه کجا بنده کجا بحر کجا قطره کجا</p></div>
<div class="m2"><p>لطفها میکنی ای تاج سرم خاک درت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا بود ارض وسما باش تو چون بحر وسحاب</p></div>
<div class="m2"><p>گفت دلکش گهرت بخش فراوان مطرت</p></div></div>