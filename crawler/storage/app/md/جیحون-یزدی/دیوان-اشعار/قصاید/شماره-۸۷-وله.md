---
title: >-
    شمارهٔ ۸۷ - وله
---
# شمارهٔ ۸۷ - وله

<div class="b" id="bn1"><div class="m1"><p>ای آنکه بقد تالی سرو چمنی تو</p></div>
<div class="m2"><p>نی سرو چه باشد که سراپا سمنی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان در بدنم نیست دمی کز تو شوم دور</p></div>
<div class="m2"><p>ای سیم بدن ترک مگر جان منی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهد دوجهانت اگر از جان عجبی نیست</p></div>
<div class="m2"><p>جان دو جهان رفته بیک پیرهنی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی با تو کنم رای تماشای بساتین</p></div>
<div class="m2"><p>کز چهره گلستان و بقد نارونی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصف دهنت رالب من زهره ندارد</p></div>
<div class="m2"><p>زآنرو که مرا لقمه بیش از دهنی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردیکه ترا دید زن از خانه برون کرد</p></div>
<div class="m2"><p>ای ترک پسر فتنه بر مرد و زنی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنرا که سری هست بپای تو سپارد</p></div>
<div class="m2"><p>ای دزد دل خلق عجب موتمنی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ارژنه شیری بر بایندگی دل</p></div>
<div class="m2"><p>با آنکه رمنده چو غزال ختنی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سخت دلی پنجه نهد پیش تو پولاد</p></div>
<div class="m2"><p>با آنکه بسی نرم تر از یاسمنی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیزی چو زجاکنده شود با تو یکی کوه</p></div>
<div class="m2"><p>هان بارخ شیرین و فن کوهکنی تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نیست سراپای ترا طبع سقنقور</p></div>
<div class="m2"><p>پس چون بیکی جلوه علاج عننی تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرچندکه خواهی بجهان رخش تطاول</p></div>
<div class="m2"><p>کاندر صف خوبان جهان تهمتنی تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هوش از سر پیران بیکی غمزه ببردی</p></div>
<div class="m2"><p>گر چه بلب آلوده هنوز از لبنی تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیتو نشود انجمی ساز ز رندان</p></div>
<div class="m2"><p>گرچه به مژه غارت هر انجمنی تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سالی نگهی جانب عشاق توانکرد</p></div>
<div class="m2"><p>گیرم که برخساره سهیل یمنی تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زینگونه که بر قد تو دلهاست هوا خواه</p></div>
<div class="m2"><p>گوئی علم فتح امیر زمنی تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سرتیپ عرب زیب عجم مظهر از جم</p></div>
<div class="m2"><p>کش دور فلک گفت دویم ذوالیزنی تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برکژی تیغش زقضا آمده مرقوم</p></div>
<div class="m2"><p>کز بهر دل راست پسندان مجنی تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر راستی نیزه اش ازچرخ نبشته است</p></div>
<div class="m2"><p>کز بهر کج اندیش مزاجان محنی تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای مصطفوی نام که از مرتضوی جام</p></div>
<div class="m2"><p>سرمست شرافت زحسین و حسنی تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زین یک گه میدان و از آن یک گه ایوان</p></div>
<div class="m2"><p>دارنده بخت نو و رای کهنی تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با آنکه گهر در کف تو باز نماند</p></div>
<div class="m2"><p>کز جود ندانسته گهر را ثمنی تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زین هر دو یگانه در شهوار بود صدق</p></div>
<div class="m2"><p>گر گویمت از گنج گهر مختزنی تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در حلقه اقران خود از فطرت شایان</p></div>
<div class="m2"><p>یکمرده و صافی چو اویس قرنی تو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از بر به اصناف وزاحسان باشراف</p></div>
<div class="m2"><p>بر روی زمین صاحب فخر (و) مننی تو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زآراستن خیر وز پیراستن شر</p></div>
<div class="m2"><p>در زیر فلک مصدر فرض و سننی تو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در بزم چو با دست گهر بیزکنی جای</p></div>
<div class="m2"><p>گوئی متموج شده بحر عدنی تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر رزم چو با تیغ شرر خیز کنی رای</p></div>
<div class="m2"><p>گوئی متحرک شده جیش گشنی تو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از بسکه بکوی تو نعم ریخته برهم</p></div>
<div class="m2"><p>برکنده بسی را سوی خود از وطنی تو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا عالم پیداست ترا بخت جوان باد</p></div>
<div class="m2"><p>کاسباب تن آسائی و دفع حزنی تو</p></div></div>