---
title: >-
    شمارهٔ ۳۶ - مطلع ثانی
---
# شمارهٔ ۳۶ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>کای بهر مرحله اجرام مشار و تو مشیر</p></div>
<div class="m2"><p>وی بهرغائله افلاک مجارو تو مجیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیست کیهان که نماید برجاه تو بزرگ</p></div>
<div class="m2"><p>چیست گردون که نباشد برقدر تو حقیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کو ه از دشت نگشته برخنگت ممتاز</p></div>
<div class="m2"><p>رزم با بزم ندارد بر خیلت توفیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اغنیا را زفلک گرد سرای ازتو حصار</p></div>
<div class="m2"><p>فقرا را زستبرق بوثاق ازتو حصیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلت از پیش و پس رفته و آینده علیم</p></div>
<div class="m2"><p>رایت از کیف و کم ثابت و سیاره خبیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز بفرخنده خطاب تو ندارد مانند</p></div>
<div class="m2"><p>کشد از جنت اگر خامه رضوان تصویر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر سوزنده عتاب تو ندارد تمثال</p></div>
<div class="m2"><p>کند از دوزخ اگر منطق مالک تفسیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدیاری که دهد فرتو یکروز عبور</p></div>
<div class="m2"><p>ابدالدهر بجان آید از او بوی عبیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو سنت صورسرافیل گذارد زصهیل</p></div>
<div class="m2"><p>قلمت نفخه جبریل نماید زصریر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دخل هر روزه فزائی نه برآورده سپاه</p></div>
<div class="m2"><p>خرج صد ساله ستانی نفرستاده سفیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکه درخواب بدوران تو نیران نگرد</p></div>
<div class="m2"><p>گرددش بردم تیغت زمعبر تعبیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فتح را ناوک رمح تو عدیلست و بدیل</p></div>
<div class="m2"><p>چرخ را سایه چتر تو نصیر است و بصیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کام بخشای امیرا منم آن چامه نگار</p></div>
<div class="m2"><p>که بخوان سخنم زائده چینی است جریر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فاریاب ار چه نباشد هله محروسه یزد</p></div>
<div class="m2"><p>که تو را فر طغانشاه و مرا نظم ظهیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سالها رفت که از سیرمه وگردش مهر</p></div>
<div class="m2"><p>شعر شعری صفتم را نبد اکرام شعیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گاه چون باز پریدم پی یکپارچه گوش</p></div>
<div class="m2"><p>گاه چون یوز دویدم پی یکمشت پنیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جای راحت همه دیدم ستم از پیل ملک</p></div>
<div class="m2"><p>جای نعمت همه خوردم لگد از اسب وزیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لیک تا سایه بزم توام افتاده بسر</p></div>
<div class="m2"><p>نزد قصرم بود افراشته افلاک قصیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مه نتابد چو کنیزان من اندر نخشب</p></div>
<div class="m2"><p>سرو ناید چو غلامان من اندر کشمیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا هلال از فر شوال و شکوه رمضان</p></div>
<div class="m2"><p>گاه از لطف بشیر است وگه از عنف نذیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ماه نوا زقدا عدای تو اندر حسرت</p></div>
<div class="m2"><p>بدر از چهره احباب تو اندر تشویر</p></div></div>