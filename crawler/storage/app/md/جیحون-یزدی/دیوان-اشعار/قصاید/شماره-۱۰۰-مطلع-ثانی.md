---
title: >-
    شمارهٔ ۱۰۰ - مطلع ثانی
---
# شمارهٔ ۱۰۰ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>کای باقبال جهانگیر و ممالک پیمای</p></div>
<div class="m2"><p>وی مصور ز رخت معنی تایید خدای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تخت از صولت کام تو بود کیوان پوی</p></div>
<div class="m2"><p>تاج از دولت فرق تو بود فرقدسای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد گوی دم اسبت مه افلاک مسیر</p></div>
<div class="m2"><p>گرد نعل سم خنگت گل خورشید اندای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد باسیر سمند تو یکی بیهده گرد</p></div>
<div class="m2"><p>عقل با ذوق سلیم تویکی هرزه درای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرشب از سهم کیی کاخ شکوهت مریخ</p></div>
<div class="m2"><p>پرد از خواب چنان کش شکند طاق سرای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هردم از رعب کله گوشه قدرت کیوان</p></div>
<div class="m2"><p>بجهد از جای چنان کش گسلد بند قبای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلک بشکست قضا دفتر پیچید قدر</p></div>
<div class="m2"><p>هربلد را که شد اخلاق تو فرمان فرمای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر خاور را در محضر تو سر برسنگ</p></div>
<div class="m2"><p>جند اختر را درکشور تو پا درلای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کیست فردوس که نبود بر بزمت مدهوش</p></div>
<div class="m2"><p>چیست دوزخ که نماند گه رزمت دروای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابر اگر با تو رود اوج بگو باد مسنج</p></div>
<div class="m2"><p>بحر اگر با تو زند موج بگو آب مسای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا موالید سه و عقل ده و ارکان چار</p></div>
<div class="m2"><p>بر درت هفت فلک یک جهتی پشت دوتای</p></div></div>