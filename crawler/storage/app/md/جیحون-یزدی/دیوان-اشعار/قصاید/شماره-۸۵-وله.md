---
title: >-
    شمارهٔ ۸۵ - وله
---
# شمارهٔ ۸۵ - وله

<div class="b" id="bn1"><div class="m1"><p>سپهسالار اعظم زد چو اندر مرز ری اردو</p></div>
<div class="m2"><p>قضا را تاب شد از تن قدر را رنگ رفت از رو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک حیران که این داور زند آیا کدامین در</p></div>
<div class="m2"><p>دول پژمان که این لشکر چمد آیا کدامین سو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بت من ای مه ارمن برجسم تو جانها تن</p></div>
<div class="m2"><p>غزال آسا و شیر اوژن سنان اندام و جوشن مو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عنان پیچید باز از نو زشهر اسپهد خسرو</p></div>
<div class="m2"><p>تو نیز اقبال آساشو روان اندر رکاب او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر جا بایدش خفتان که ورزد رزم در میدان</p></div>
<div class="m2"><p>تواش بر جسم به از جان زره پوشی کن از گیسو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و گر هنگام پیکارش بشمشیر اوفتد کارش</p></div>
<div class="m2"><p>تو برکف گهر بارش بنه تیغ از خم ابرو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولیک ای لعبت سرکش مخیزاز کاخ بنشین خوش</p></div>
<div class="m2"><p>کزو ار دوست مینووش نشاید فتنه در مینو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو با این طلعت شایان چو در اردو کنی جولان</p></div>
<div class="m2"><p>ربایندت زمو چوگان چنان کت از زنخدان گو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی از چشم تو گویا که هی هی اندر این صحرا</p></div>
<div class="m2"><p>زبخت شاه شد بر ما بپای خویش صید آهو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی برقد تومفتون که بخ بخ اندر این هامون</p></div>
<div class="m2"><p>شد از اقبال صدر اکنون معسکر باغی از ناژو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مهین اسپهد اعظم هز بر از دوده آدم</p></div>
<div class="m2"><p>مسالک دان ملایک دم حقایق دان دقایق جو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برایت اندر آن شیرش که ازگردان کند سیرش</p></div>
<div class="m2"><p>کشیده رعب شمشیرش بگرد مملکت بارو</p></div></div>