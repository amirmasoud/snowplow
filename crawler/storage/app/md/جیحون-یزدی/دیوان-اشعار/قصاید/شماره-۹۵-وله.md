---
title: >-
    شمارهٔ ۹۵ - وله
---
# شمارهٔ ۹۵ - وله

<div class="b" id="bn1"><div class="m1"><p>زان موسوی دو اژدرگیسوی آسیه</p></div>
<div class="m2"><p>روزم چو قیرگون دل فرعون شد سیه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاصی شدم درست چو فرعون برخدای</p></div>
<div class="m2"><p>تا دیدم آن شکسته سر زلف آسیه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لذت برد زپیکر نیکوش پیرهن</p></div>
<div class="m2"><p>منت کشد زچنبرگیسوش غالیه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر وقوف بوس و کنارش بروز وصل</p></div>
<div class="m2"><p>هر شب کنم نظاره در اشکال الفیه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشک اربچین بود رخ او از فسونگری</p></div>
<div class="m2"><p>چین را بمشک طره نموده است تعبیه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر طوبی قدش همه غلمان اگر غلام</p></div>
<div class="m2"><p>با کوثر لبش همه گرحور جاریه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیشش کمال مه چو لباسی است مسترق</p></div>
<div class="m2"><p>نزدش جمال گل چو قبائیست عاریه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشگفت اگر زخجلت شمشاد قامتش</p></div>
<div class="m2"><p>دیگر نپرورد سمن و سرو نامیه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این بس زحسن او که بود نام فرخش</p></div>
<div class="m2"><p>در مدح شاهزاده آزاده قافیه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عبدالحسین شبل امیر آخور ملک</p></div>
<div class="m2"><p>کافلاک از او بدوش کشیده است غاشیه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیلی است با کمند چو آید بکارزار</p></div>
<div class="m2"><p>شیریست برسمند چو تازد بناحیه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون برفراز چنگ لوا گیرد او بجنگ</p></div>
<div class="m2"><p>هر گوشه از جیوش نگون گردد الویه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یاللعجب که با رخ چون خلد در نبرد</p></div>
<div class="m2"><p>بر خصم باز میکند ابواب هاویه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دشمن هراسد از دم شمشیر او چنان</p></div>
<div class="m2"><p>کز ذوالفقار صفدر صفین معاویه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در وقعه خون چکد زپرند وی آنقدر</p></div>
<div class="m2"><p>کز حاجیان بمکه در اعیاد اضحیه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای مفخر عجم که زهندی بلارکت</p></div>
<div class="m2"><p>اتراک چین گریزد و اعراب بادیه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ارکان بطوف کعبه کوی تو زاشتیاق</p></div>
<div class="m2"><p>هر روز را شمرده بخود یوم ترویه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در هیچ فن کتابی از ابناء فضل نیست</p></div>
<div class="m2"><p>کانرا نخوانده شخص تو از متن و حاشیه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا اهل منطق از پی اثبات رای خویش</p></div>
<div class="m2"><p>در جمع میکنند کفایت بتثنیه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از حال تا زمان مضارع شریف تر</p></div>
<div class="m2"><p>و زماضیت ستوده تر اوقاف حالیه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قوس محب تو زصعودش بود وتر</p></div>
<div class="m2"><p>خصم تو منفعل ز دوایر به زاویه</p></div></div>