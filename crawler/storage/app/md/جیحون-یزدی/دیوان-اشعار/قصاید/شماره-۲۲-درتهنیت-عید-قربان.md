---
title: >-
    شمارهٔ ۲۲ - درتهنیت عید قربان
---
# شمارهٔ ۲۲ - درتهنیت عید قربان

<div class="b" id="bn1"><div class="m1"><p>جشن اضحی شد و برطوف حرم کوشش حاج</p></div>
<div class="m2"><p>ما و دیدارخلیلی که حرم هم محتاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما خداجو زحرم حاج حرم جو زخدا</p></div>
<div class="m2"><p>بنگر ای خواجه بود صرفه بمایا با حاج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خلیل دل رندان بحرم بنهد تخت</p></div>
<div class="m2"><p>نه عجب گر حرم ازگرد رهش جوید تاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذوق گویا نگرد سوی خلیلی که برد</p></div>
<div class="m2"><p>حسن خال و ذقنش از حجر و زمزم باج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دلارام جلیل ای سمن اندام خلیل</p></div>
<div class="m2"><p>که دهد چهر تو را آذر نمرود خراج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عید اضحی بود و می بصفا باید خورد</p></div>
<div class="m2"><p>که غم هجر حجر را بجز این نیست علاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لیکن ایشوخ من ارحج ننهم سیمم نیست</p></div>
<div class="m2"><p>تو چرا حج ننهی کت همه سیمی است رواج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشتی از آنهمه سیم تو من ار داشتمی</p></div>
<div class="m2"><p>بگذر از حج که فراتر شدمی از معراج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی غلط گفتم آنخوی فتن جو که توراست</p></div>
<div class="m2"><p>سیم ندهد بخوشی تا نبرد زر بلجاج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از تو یک غمزه و صد خیل عرب درغارت</p></div>
<div class="m2"><p>وز تو یک عشوه و صد ملک عجم در تاراج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرتو با این خط و این قد سوی بطحا گذری</p></div>
<div class="m2"><p>خار هامون همه گیرد سبق از سوسن وکاج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>توئی آن شمع روان سوز حرم خانه دل</p></div>
<div class="m2"><p>که قمرگرد جهان وصل تو جوید بسراج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>موی تو مشک ختن چشم تو آهوی ختا</p></div>
<div class="m2"><p>لعل تو کان گهر سینه تو صفحه عاج</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر ما در ره تو راه تو بر درگه شه</p></div>
<div class="m2"><p>درگه شه کنف میر ملایک افواج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بت شکن داور محمود براهیم خلیل</p></div>
<div class="m2"><p>که بود معدلتش مسلک رای و منهاج</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه در پنجه او پیل بدانسان لرزد</p></div>
<div class="m2"><p>که بلرزد زفر پنجه شاهین دراج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنچه با کله ضرغام بیک مشت کند</p></div>
<div class="m2"><p>نکند سختی صد صخره صما بزجاج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تیغ آفاق ستانش چو برآید زغلاف</p></div>
<div class="m2"><p>فتح را روشنی صبح دهد از شب داج</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تیر اوگاه وغاگر همه بر سنگ خورد</p></div>
<div class="m2"><p>از دگر سوی کند دیده شیران آماج</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خطی اردر گذر سیل کشد سطوت او</p></div>
<div class="m2"><p>نگذرد آن خط اگر بگذرد از چرخ امواج</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای مهین تر خلف آدم و حوا که برزم</p></div>
<div class="m2"><p>بیم رویت کند از پشت گوان قطع نتاج</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بخت چالاک تو در بازی با خصم بکین</p></div>
<div class="m2"><p>بد قماریست که از وی نبرد صد لیلاج</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیلک ار بر شکم کوه زنی روز مصاف</p></div>
<div class="m2"><p>بجهد از پشت چو سوزن که جهد از دیباج</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عیب خصم تو مستر نشود گر بشود</p></div>
<div class="m2"><p>مریمش رشته طرا زنده و عیسی نساج</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر منجم که بعهد تو در انجم نگرد</p></div>
<div class="m2"><p>جز تو را نیکی طالع نکند استخراج</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با کمان تو که منصور بود درناورد</p></div>
<div class="m2"><p>دل گردان طپد آنگونه که بیضه حلاج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>میر قلزم گهرا حضرت جیحون است این</p></div>
<div class="m2"><p>کز فر ا فسر تو بر شعرا آمد تاج</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرقم از خاک قدوم تو متوج شد لیک</p></div>
<div class="m2"><p>تاج تنها چه کند چون نبود مایحتاج</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جام کش کام بران نام ببر سیم ببار</p></div>
<div class="m2"><p>تا شود بحر قصاید بمدیحت مواج</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تاکه درشش جهه از تافتن هفت اختر</p></div>
<div class="m2"><p>چار مادر به سه مولود بیا کنده دواج</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>محنتت باد فراموش و مسرت همدوش</p></div>
<div class="m2"><p>شاهدت باد در آغوش و سلامت به مزاج</p></div></div>