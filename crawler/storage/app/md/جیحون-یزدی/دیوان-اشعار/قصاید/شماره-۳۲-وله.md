---
title: >-
    شمارهٔ ۳۲ - وله
---
# شمارهٔ ۳۲ - وله

<div class="b" id="bn1"><div class="m1"><p>مرا ترکیست مشکین موی و نسرین بوی و سیمین بر</p></div>
<div class="m2"><p>سهالب مشتری غبغب هلال ابروی و مه پیکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گردد رام و گیرد جام و بخشد کام و تابد رخ</p></div>
<div class="m2"><p>بود گلبیز و حالت خیز و سحر انگیز و غارتگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهانش تنگ و قلبش سنگ و صلحش جنگ و مهرش کین</p></div>
<div class="m2"><p>بقد تیر و بموقیر و برخ شیر و بلب شکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بر ایوان چه در میدان چه بامستان چه در بستان</p></div>
<div class="m2"><p>نشیند ترش وگوید تلخ و آرد شور و سازد شر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آید رقص ودزدد ساق وگردد دور نشناسم</p></div>
<div class="m2"><p>ترنج ازشست و شست ازدست و دست از پا و پا ازسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همانا طلعتش این خلعت پیروزی وکیشی</p></div>
<div class="m2"><p>گرفت از حال و اقبال و جمال شاه گردون فر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیاث المک و المله جم اختر ناصرالدوله</p></div>
<div class="m2"><p>کزو نازد نگین و تخت و طوق و یاره و افسر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زتمکین و صفا و سطوت و عزمش سبق برده</p></div>
<div class="m2"><p>هم از خاک و هم از آب و هم از آتش هم از صرصر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سمند و صارم و سهم و سنانش را گه هیجا</p></div>
<div class="m2"><p>سما بیدا هنر شیدا ظفر پیدا خطر مضمر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایا شاهی که شد کف و بنان و سکه و نامت</p></div>
<div class="m2"><p>پناه سیف و عون کلک و فخر سیم و ذخرزر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پر است ازعزم و حزم و رایت جیش تو کیهانرا</p></div>
<div class="m2"><p>ز پست و برز وفوق و تحت و شرق و غرب و بحر و بر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فتد گاه تک خنگ قلل کوب تلل برت</p></div>
<div class="m2"><p>پلنگ از پای و شیر از پی نهنگ از پوی و مرغ از پر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک از صد گونه اوصاف تو ننویسد کس ارگردد</p></div>
<div class="m2"><p>مداد ابحار و کلک اشجار و هفتم آسمان دفتر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدزدد بال و ناف و مشک و ناخن از صهیل او</p></div>
<div class="m2"><p>عقاب چرخ وگاو ارض و پیل مست و شیرنر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شمارد پا و دست و سم و ساق و ساعدش یکسان</p></div>
<div class="m2"><p>پل و شط و حصار و خندق و کهسار و خشک و تر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نداند گرم و سرد و رعد و برق و آب و برف و نم</p></div>
<div class="m2"><p>چه در تیر و چه در قوس و چه در آبان چه در آذر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>الا تا فرقها دارند نزد فکرت دانا</p></div>
<div class="m2"><p>صور از ذات و حادث از قدیم اعراض از جوهر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در و بام و سر و پای و رگ و چشم و دل خصمت</p></div>
<div class="m2"><p>به کند و کوب و بند و چوب و تیر و ناخج و نشتر</p></div></div>