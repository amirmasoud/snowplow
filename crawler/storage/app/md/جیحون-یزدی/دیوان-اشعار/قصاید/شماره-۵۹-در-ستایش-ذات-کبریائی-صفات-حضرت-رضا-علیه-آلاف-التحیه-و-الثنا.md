---
title: >-
    شمارهٔ ۵۹ - در ستایش ذات کبریائی صفات حضرت رضا علیه آلاف التحیه و الثنا ء
---
# شمارهٔ ۵۹ - در ستایش ذات کبریائی صفات حضرت رضا علیه آلاف التحیه و الثنا ء

<div class="b" id="bn1"><div class="m1"><p>بودم زسرو قدان صنمی سمین و سالم</p></div>
<div class="m2"><p>ارمش بروی بنده حرمش بکوی خادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه دهد ره وسایل نه ستد زکس رسایل</p></div>
<div class="m2"><p>رخکش چو رای عادل خطکش چو روی ظالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه گر پی مفاخر نرود زکبر وافر</p></div>
<div class="m2"><p>نه بجرگه اصاغر نه بخرگه اعاظم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو چهره اش حقایق بدو طره اش دقایق</p></div>
<div class="m2"><p>حرکات او موافق کلمات او ملایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عدم از لبش موید فتن از رخش قوی ید</p></div>
<div class="m2"><p>خد او شفای سرمد قد او بلای دایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه غم ار بسی قبایل کشد او بدین شمایل</p></div>
<div class="m2"><p>ملکی نیایدش دل که نویسدش مظالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زده حلقه ها بکاکل که به دوربین تسلسل</p></div>
<div class="m2"><p>بوفای او تفضل بجفای او مکارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر او بغارت جان شد از آن سپاه مژگان</p></div>
<div class="m2"><p>غمش ازکدام سلطان رمش از کدام حاکم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که ام از سرور دلجو که ام از غرور بدگو</p></div>
<div class="m2"><p>بمیانه من و او بود ای بسا عوالم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لبش آن لطیف موضع که نه بوسه راست موقع</p></div>
<div class="m2"><p>برخش عرق مصدع به تنش سمن مزاحم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه دولت مساعد چو بود ببزم قاعد</p></div>
<div class="m2"><p>همه غارت معاضد چو شود بکاخ قائم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رخ و زلف او در آیت زضلا لت و هدایت</p></div>
<div class="m2"><p>بتالفش عنایت بتکلفش مراحم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه دم برصبایح می ناب خورده واضح</p></div>
<div class="m2"><p>نه تفقدش بناصح نه تالمش زلائم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شبکیش گفتم ای مه پی خون رزمپوره</p></div>
<div class="m2"><p>که زسکرت آید آنگه که شوی زکرده نادم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بجواب گفت جیحون ز عنب از آن خورم خون</p></div>
<div class="m2"><p>که برای نسل هارون ز چه کشت پورکاظم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شه دین امام ضامن مه مرکز میامن</p></div>
<div class="m2"><p>که دوکون را اماکن ز سرایراست عالم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شهی از خودی مجردمهی از خدا مقید</p></div>
<div class="m2"><p>بردوحه محمد گل بوستان هاشم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پسر یگانه زهرا پدر هزار حوا</p></div>
<div class="m2"><p>خور آسمان اعراب و در یم اعاجم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شب قدر محو مویش دل دیر وکعبه سویش</p></div>
<div class="m2"><p>زصفا حصات کویش زده طعنه بر نواجم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زبرون پرده چون هی بدرون پرده اش پی</p></div>
<div class="m2"><p>شده نقش پرده از وی همه جان شکر ضیاغم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه چو جد او میسر همه خلق اگر پیمبر</p></div>
<div class="m2"><p>نه چو جده اش مصور همه گون اگر فواطم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زتقاعدش بغبرا باسف سپهر خضرا</p></div>
<div class="m2"><p>زتقربش بفردا بشعف روان آثم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زهی ای وجود اجمل رخت ایزدی سجنجل</p></div>
<div class="m2"><p>بحسب ظهور اول بنسب سلیل خاتم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زتو کامران قوابل زتو حل شده مشاکل</p></div>
<div class="m2"><p>زتو جنبش هیاکل زتو دانش جماجم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو بعاطفت زنی دم چو بکین شوی مصمم</p></div>
<div class="m2"><p>شود از بهایم آدم شود آدم از بهایم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اثرات کف موسی بجنیبت از تجلی</p></div>
<div class="m2"><p>ثمرات گفت عیسی بزمینت از نسایم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو زدی زطبع قادر بنقاط دین دوایر</p></div>
<div class="m2"><p>کبرت بک الصغایر ظهرت بک العلایم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زلل از تو در جدائی زمصاحف خدائی</p></div>
<div class="m2"><p>نه زکوشش کسائی نه زاجتهاد عاصم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بسوی تو ره سپاران زصنوف تاجداران</p></div>
<div class="m2"><p>بدر تو خاکساران زاهالی عمایم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زقدوم تو بدانسان شده نامور خراسان</p></div>
<div class="m2"><p>که در او زمهر تابان بود افسر افاخم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بودش زبسکه تمکین بنفاذ دولت و دین</p></div>
<div class="m2"><p>چه عبوری از بساتین چه اموری از عضایم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه جود بالتوالی باریکه معالی</p></div>
<div class="m2"><p>همه عدل لایزالی بوساده محاکم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بعباد از او فواید برقاب از او قلاید</p></div>
<div class="m2"><p>علمش بجیش قاید قلمش بملک ناظم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو گروه غم نصیبان برنطق او ادیبان</p></div>
<div class="m2"><p>چو بلارک خطیبان بر تیغ او صوارم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بهنر چنان مواظب که زخویش گشته غایب</p></div>
<div class="m2"><p>زمعانیش مشارب زمعارفش مطاعم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خهی آن سپهر پیما که فراز آن کند جا</p></div>
<div class="m2"><p>نه چو او سترک اعضا نه چو او قوی قوائم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>برد ار جبال و صحرا بشکوه و فر عنقا</p></div>
<div class="m2"><p>خورد ارحدید مهما بجلادت نعایم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هله تا کلام نحوی همه معرب است و مبنی</p></div>
<div class="m2"><p>هله تا که از تعدی بدراست فعل لازم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فلکت دخیل عاجل ملکت زخیل آجل</p></div>
<div class="m2"><p>زتو عزت مشاغل بتو رونق مناظم</p></div></div>