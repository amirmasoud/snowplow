---
title: >-
    شمارهٔ ۴ - در منقبت اسدالله الغالب علی بن ابیطالب (ع)
---
# شمارهٔ ۴ - در منقبت اسدالله الغالب علی بن ابیطالب (ع)

<div class="b" id="bn1"><div class="m1"><p>ای بررخ رنگینت زآن طره مشکینا</p></div>
<div class="m2"><p>در دامن روح القدس یک گله شیاطینا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افزون رخت ازخورشید کمتر لبت از ذره</p></div>
<div class="m2"><p>وآن ذره ات آبستن از خوشه پروینا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشنو که دلم یابد بی وصف لبت آرام</p></div>
<div class="m2"><p>فرهاد کی آرامد بی قصه شیرینا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاشاکه رهد عاشق از مژه خون ریزت</p></div>
<div class="m2"><p>کی صرفه برد گنجشگ از پنجه شاهینا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کس ننماید باز از نافه سخن آغاز</p></div>
<div class="m2"><p>بگشا گرهی از ناز زان سنبل پرچینا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنشین که زنم جامی بریاد خطت آری</p></div>
<div class="m2"><p>می بیش دهد مستی برطرف ریاحینا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر پنجه ات از نرمی آرد بمن آن گرمی</p></div>
<div class="m2"><p>کز وصل سقنقوراست در مردم عنینا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر از غم لیلی گشت مجنون هم اندر دشت</p></div>
<div class="m2"><p>عشق لب تو انباشت شهری ز مجانینا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افسونگریم نبود لیک از غم زلفینت</p></div>
<div class="m2"><p>پیچم همه شب با مار تا صبح به بالینا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای داغ دل لاله در گلشن رعنائی</p></div>
<div class="m2"><p>وی شعله جواله اندر زبر زینا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرگز نکند نسرین خون در دل کس چندین</p></div>
<div class="m2"><p>گیرم که توئی از حسن نوباوه نسرینا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ایمه که خورد سوگند بر شکر لعلت قند</p></div>
<div class="m2"><p>وی روی تو را پیوند با روح بساتینا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا چند بسیر باغ پیچی تو عنان از راغ</p></div>
<div class="m2"><p>ترسم ننماید فرق گل را زتو گلچینا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو زاده از حوری تو لمعه از نوری</p></div>
<div class="m2"><p>بگذار که با غ آید نزدت پی تزیینا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود سبزه چه حد دارد کز کام تو یابد کام</p></div>
<div class="m2"><p>جائیکه بود نرگس با چشم تو مسکینا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر باغ همی خواهی تا بهره بری از گل</p></div>
<div class="m2"><p>در آینه بین گلها زان چهره رنگینا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نی آینه را منگرکو آلت خود بینی است</p></div>
<div class="m2"><p>بیزار بود حیدر از مردم خود بینا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاه ملکوتی صدر خورشید جنود بدر</p></div>
<div class="m2"><p>آن کاسر اهل غدر آن صفدر صفینا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نامد حرم ار زاول شایسته میلادش</p></div>
<div class="m2"><p>قومی نشد از سجیل رخت افکن سجینا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در نعمت او موسی زد مائده از سلوی</p></div>
<div class="m2"><p>در ملکت او یونس پرورده یقطینا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در مهد درید اژدر بی شبهه زدم تا دم</p></div>
<div class="m2"><p>گوباش خوارج را اهمال بتحسینا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن دست که شست دیونا گشته تولد بست</p></div>
<div class="m2"><p>نشگفت شد ار در مهد درنده تنینا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر آذر شمشیرش نگداخت روان شرک</p></div>
<div class="m2"><p>بد کعبه کنون همسنگ با آذر برزینا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر طینتش از جان نیست عالم بدوجو چون داد</p></div>
<div class="m2"><p>نگذشت زیک گندم آدم که بد از طینا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای کز بر اشباه نازند هدات راه</p></div>
<div class="m2"><p>همچون کلمات الله از سوره یاسینا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تدبیر حبیب تو هم پله با تقدیر</p></div>
<div class="m2"><p>تمجید عدوی تو هم رتبه توهینا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر جای تو کش راکع جان فلک تاسع</p></div>
<div class="m2"><p>هر کس که بحق ننشست شد باذل تسعینا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ذاتت نتوان سنجید کاین گوهر قدوسی</p></div>
<div class="m2"><p>تن در ندهد هرگز در حیز تخمینا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در کعبه اگر رکنی است از فخر قدوم تست</p></div>
<div class="m2"><p>ورنه چه ثمر گفتن ارکان با ساتینا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یکذره زمهر تو سنجند اگر در حشر</p></div>
<div class="m2"><p>صد بار دهد میزان اشکست بشاهینا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اندر ره تو سالک هرگز نشود هالک</p></div>
<div class="m2"><p>غسلش دهد ارمالک اندر خم غسلینا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>منظور کلیم الله درکوی تو ماندن بود</p></div>
<div class="m2"><p>با آنکه نهادش نام میقات ثلاثینا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در امت او چون کس نشناخت خدا از گاو</p></div>
<div class="m2"><p>در وصف تو نتوانست اعلان مضامینا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>الحمد که در ظلت مرا امت احمد راست</p></div>
<div class="m2"><p>هوشی که شود تکمیل هر ساعت ازو دینا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سر حلقه این امت شاهی است که از همت</p></div>
<div class="m2"><p>بخشد کف او نعمت بر خیل سلاطینا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شه ناصر دین راد فرخنده بدید و زاد</p></div>
<div class="m2"><p>کش کاخ فلک بنیاد مسجود خواقینا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در دهر ز اعلامش تا بنده علاماتا</p></div>
<div class="m2"><p>در ملک زیاسایش پاینده قوانینا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر نکته که کس تا حال تبیان نتوانستش</p></div>
<div class="m2"><p>او نیک برون آمد از عهد تبیینا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بزمش که باهل ارض شد خدمت بر وی فرض</p></div>
<div class="m2"><p>ویسی است که افلاکش از جان شده رامینا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بخشی است زلطف حق بخشی که بود او را</p></div>
<div class="m2"><p>این نیست عروسی کش گیرند بکابینا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ایشاه ملایک جان وی خسرو چرخ ارکان</p></div>
<div class="m2"><p>کز دور تو شد دوران مشحون زمیامینا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو کافل ارزا قی اندر ملکان طاقی</p></div>
<div class="m2"><p>جود کف تو از سبع نشناخته سبعینا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دانی تو سرایر را گوئی تو ضمایر را</p></div>
<div class="m2"><p>مانا که شود از غیب بر نطق تو تلقینا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>روزی که چو برق از میغ بکشی زنیام آن تیغ</p></div>
<div class="m2"><p>بهرام فروخشکد چون هیکل چوبینا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آنرا که بطبع اندر رعب تو کند تبرید</p></div>
<div class="m2"><p>دوزخ شودش عاجز زاندیشه تسخینا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>رمح تو نیندیشد از بارقه ابطال</p></div>
<div class="m2"><p>ثعبان نکند پروا از سعی خراطینا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شاها نگر از رحمت کاین بنده ات از زحمت</p></div>
<div class="m2"><p>آراست حجال نظم زین بکر خواتینا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا مهر چمد بر عکس از دور فلک چونان</p></div>
<div class="m2"><p>موری که رود وارون از سیر طواحینا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سازند دعایت ورد هم ثابت وهم سیار</p></div>
<div class="m2"><p>جبریل امین هم نیز گوینده آمینا</p></div></div>