---
title: >-
    شمارهٔ ۵۰ - وله
---
# شمارهٔ ۵۰ - وله

<div class="b" id="bn1"><div class="m1"><p>شیفته برروی سر کاکل چون عنبرش</p></div>
<div class="m2"><p>تا دگر آن فتنه جوی چیست بزیر سرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شانه نراند بمو آب نریزد برو</p></div>
<div class="m2"><p>کاین دو زیان آورد به آتش و عنبرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیک نداند هنوز زخردی و سادگی</p></div>
<div class="m2"><p>که شانه و آب شد بموی و رو چاکرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب چو آتش شود شانه مشوش شود</p></div>
<div class="m2"><p>نوازد ار این دو را بعنبر و آذرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز مژه و چشم او که دیدم از چشم خود</p></div>
<div class="m2"><p>من نشنیدم غزال پنجه ز شیر نرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب و رخش در صفت شکر و آتش و لیک</p></div>
<div class="m2"><p>آب چکد ز آتشش زهر دهد شکرش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاب نماند دگر در تن و جان مرمرا</p></div>
<div class="m2"><p>چو تابد از پیرهن سینه چون مرمرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرگ نبیند بعمر پیر نگردد بدهر</p></div>
<div class="m2"><p>هر که چنین لعبتش و آنکه چنین دلبرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی نه در کوی او پای من آمد بسنگ</p></div>
<div class="m2"><p>گرگذرد جبرئیل در شکند شهپرش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس ار بگوید بماه کاین پسر از پشت تست</p></div>
<div class="m2"><p>بسکه بود پاک روی می نشود باورش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کس ارسراید بمهرکاین گهر ازکان تست</p></div>
<div class="m2"><p>بسکه بود خیره سر می نشود منکرش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که شبی را گرفت قامت او در بغل</p></div>
<div class="m2"><p>تا بقیامت وزد بوی گل از بسترش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وآنکه از آن چشم مست زد قدح و شد زدست</p></div>
<div class="m2"><p>باز نیارد بهوش طنطنه محشرش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>او که بدام دو زلف دلم زکف برد و رفت</p></div>
<div class="m2"><p>من بکدامین حیل کشم بدام اندرش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه گیردم می زدست تا به درآرم زپاش</p></div>
<div class="m2"><p>نه خواهدم باخت نرد تا بکنم ششدرش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بجرگ رندان شهر باده خورد رطل رطل</p></div>
<div class="m2"><p>چون برمن میرسد آب کشد ساغرش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کنون که از زهد خشک می نخورد نزد من</p></div>
<div class="m2"><p>دست ندارم از او تا ننمایم ترش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواه بزر یا بزور خواه بشر یا بشور</p></div>
<div class="m2"><p>میگذرم بر درش میکشم اندر برش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تخت نهد گر بماه بخشمش آرد براه</p></div>
<div class="m2"><p>مگر که باشد پناه از ملک بندرش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مهدی هادی صفت آنکه زنیکونیت</p></div>
<div class="m2"><p>فزون بود از سپهر کوکبه اخترش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غنا نخواهد فقیر چون گذرد جانبش</p></div>
<div class="m2"><p>وطن نجوید غریب چون نگرد منظرش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خدمت درماندگان نعمت بی منتهاش</p></div>
<div class="m2"><p>صحبت آموزگار دولت جان پرورش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هدیه بخردان برد بذات خود نیم شب</p></div>
<div class="m2"><p>کو ببزرگی بود قاعده دیگرش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بعهد او نی عجب اگر نبارد سحاب</p></div>
<div class="m2"><p>بسکه خجل باشد از دست عطا گسترش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نی ز عهود قدیم بیش ببارد که ابر</p></div>
<div class="m2"><p>از حسد کف اوست همیشه چشمی ترش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فر گشاده دلش بدجله و نیل نیست</p></div>
<div class="m2"><p>مگر که باشد محیط تعبیه در گوهرش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صدور هر کار خیر چه در حرم چه بدیر</p></div>
<div class="m2"><p>ژرف چو بینی بود مسند او مصدرش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای کف دربار تو برشده ابری که هست</p></div>
<div class="m2"><p>سوختن آز برق صیت سخاتندرش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون بتو اقبال ساخت قدر جلالت شناخت</p></div>
<div class="m2"><p>تیغ نهان در نیام نیست عیان جوهرش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دادگرا چرخ پیر عروس گشتی بتو</p></div>
<div class="m2"><p>نشگفت از آنکه خواست جوان بود شوهرش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنکه بدانش بود کاشف غیب و شهود</p></div>
<div class="m2"><p>پیش تو نشناخته است ایمنش از ایسرش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هیچ قضائی بملک نیارمد با مراد</p></div>
<div class="m2"><p>تا که نگردد زتو تقویتی یاورش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بهر مهام عباد جای نگیرد تو را</p></div>
<div class="m2"><p>کس ار زآهن بود عناصر پیکرش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>میرا از بسکه هست گفته جیحون پر آب</p></div>
<div class="m2"><p>بپای خود هر طرف روان بود دفترش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مهرتو کندش ز یزد ور نه به بنگاه خویش</p></div>
<div class="m2"><p>شاهد فرخار بود بلکه می خلرش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا که بود زلف دوست کمند عاشق رباش</p></div>
<div class="m2"><p>تا که بود چشم یار غمزه غارتگرش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پشت عرب تا عجم به پیش کاخ تو خم</p></div>
<div class="m2"><p>که جز بکاخ تو نیست ملک و ملک زیورش</p></div></div>