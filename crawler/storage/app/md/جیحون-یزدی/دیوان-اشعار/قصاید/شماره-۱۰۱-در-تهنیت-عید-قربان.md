---
title: >-
    شمارهٔ ۱۰۱ - در تهنیت عید قربان
---
# شمارهٔ ۱۰۱ - در تهنیت عید قربان

<div class="b" id="bn1"><div class="m1"><p>خلیل ارکرد قربانی بعید از امر یزدانی</p></div>
<div class="m2"><p>مرا باشد خلیلی کش هزاران عید قربانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلیل ارکعبه را بر در همی ازشوق سودی سر</p></div>
<div class="m2"><p>مرا باشد خلیلی کش نماید کعبه دربانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلیل ارجانب شیطان ببطحا گشت سنگ افشان</p></div>
<div class="m2"><p>خلیل من بزلف آراسته آئین شیطانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گز از ابن السبیل انباشتی کوی خلیل الله</p></div>
<div class="m2"><p>بود کوی خلیل من مطاف عرش رحمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلیل از بت نزد گردم خلیل اربت شکست از هم</p></div>
<div class="m2"><p>بود روی خلیل من زبت چون نقشه مانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر سوی خلیل الله نبود اقبال نمرودی</p></div>
<div class="m2"><p>کند نمرود برخوان خلیل من مگس رانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الا فرخ خلیل من کت اندر چهر مینوون</p></div>
<div class="m2"><p>همی سازد گلستان آذری آذر گلستانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همایون عید اضحی گشت و ما را جز سرکویت</p></div>
<div class="m2"><p>حرم بیت الحرامست و صفا زندان ظلمانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا بیت الله است آنجا که باشد چون تو راماوی</p></div>
<div class="m2"><p>که در تو وصف ذاتی هست و در وی وصف عنوانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرایت کعبه رخسارت صفا چاه ذفن زمزم</p></div>
<div class="m2"><p>دوگیسو حلقه دل ناسک خرد در حلقه جنبانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمین گرنایدت باور یکی سوی حرم بگذر</p></div>
<div class="m2"><p>که تا دارند خلقی کعبه را بر زاهد ارزانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو در کوی منی با این صفا گر برفروزی رخ</p></div>
<div class="m2"><p>حرم را حاج دو بینند روحانی و جسمانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو در بطحا بدین خوبی فرازی گر زقد طوبی</p></div>
<div class="m2"><p>برد خارمغیلان اعتدال از سرو بستانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو در مشعر بدین مشرب گشائی گر بخنده لب</p></div>
<div class="m2"><p>شود ریگ بیابان غیرت لعل بدخشانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو گر در خانه یزدان نمائی عارض تابان</p></div>
<div class="m2"><p>صود جویان صنم گویان بگردند از مسلمانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>الآنمرود طینت مه که چهرت زد بآذر ره</p></div>
<div class="m2"><p>خلیل آسا مرا زین عید کن برعیش مهمانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بکش عجل سمین تن برای جبرئیل جان</p></div>
<div class="m2"><p>که با تسویل نفسانی نگنجد راز یزدانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ترا آسایش تن تا که از آرایش جان به</p></div>
<div class="m2"><p>نه بهر از کعبه خواهی بردنه ز اسرار و یرانی(؟)</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مدان دین خواندن قرآن که خواند از ما فزون عثمان</p></div>
<div class="m2"><p>زسلمان فرق بسیار است تا استاد سلمانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بلی برنقش انسان دل منه رو نفس انسان شو</p></div>
<div class="m2"><p>که خاتم را اثر نی جز در انگشت سلیمانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شتر با عمر اندک در بهر سالی گذارد حج</p></div>
<div class="m2"><p>ولی از حاج نبود چون ندارد روح انسانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درون تا سرد باشد ازسقم یا زازد یاد غم</p></div>
<div class="m2"><p>برون گر می نیاید از فروغ مهر نورانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برو جان گرم کن زایمان که تا برهد تن از نیران</p></div>
<div class="m2"><p>که دل گر سرد باشد بر بدن باراست بارانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نگفت ایزد نیابی هیچ جز بر ذکر من گویا</p></div>
<div class="m2"><p>در اشیا هرچه بینی پست و بالا قاصی و دانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ترا تفریق آن و این برآن دارد مثل کزکین</p></div>
<div class="m2"><p>بری انجیل دراسلام و قرآن نزد نصرانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهان را بین همه زیبا چه از خار و چه از دیبا</p></div>
<div class="m2"><p>مگو کاین شوخ کنعانست یا آن شیخ صنعانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه دل بربند در انسان نه رخ برتاب ازحیوان</p></div>
<div class="m2"><p>که کس نز عاقبت آگاه و نز توفیق ربانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مگر قطمیر نگرائید از سجین بعلیین</p></div>
<div class="m2"><p>مگر بلعم نبد نورانی وگردید نیرانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بسان میرکو فطرت که وحدت بیند ازکثرت</p></div>
<div class="m2"><p>نه فانی خواهد از باقی نه باقی جوید از فانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>براهیم خلیل الله فلک خنگ و ملک اسپه</p></div>
<div class="m2"><p>که رست از بخردی صدره ز اجرامی وارکانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یگانه داور اکمل زهفت اختر برخ اجمل</p></div>
<div class="m2"><p>که هست از صادر اول فروزان جلوه ثانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>امیری کز هنرمندی در آفاقش خداوندی</p></div>
<div class="m2"><p>جهان از وی به خرسندی چو ممسک از زرکانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فتن با عدل او مهمل ظلم بارای او مختل</p></div>
<div class="m2"><p>دل صافش نماید حل مشاکل را بآسانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حوادث در علو رایت اجلالش آن بیند</p></div>
<div class="m2"><p>که دید اندر درفش کاویان ضحاک علوانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه باک ابطال جیشش را گراز گردون ببارد خون</p></div>
<div class="m2"><p>چه پروا اهل ساحل را اگر دریاست طوفانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدشمن رعب او آنسان نماید کم سرو سامان</p></div>
<div class="m2"><p>که عمرو لیث را آهنگ اسمعیل سامانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>الا ای چشم آذربایجان کزیمن اقبالت</p></div>
<div class="m2"><p>برد از خاک تبریز آبرو کحل سپاهانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بیزدی خیلت ار داور ایالت داد غم مسپر</p></div>
<div class="m2"><p>کز اول داشت موسی بر شعیبی گله چوپانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رسد وقتی که جیش عیش بخش طیش فرسایت</p></div>
<div class="m2"><p>زنند از خاک برافلاک کوس از تنگ میدانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بمان فیروز و فرخنده که تا از تخت پاینده</p></div>
<div class="m2"><p>کند گردون گردنده بکویت کاسه گردانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بمان با ایزدی فره همی در دولتی فربه</p></div>
<div class="m2"><p>که تایابد وجوب امر تو ذرات امکانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ترا زالفاظ گوناگون صفات جانفزا بیرون</p></div>
<div class="m2"><p>که ادراک معانی نی بیانی هست وجدانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنان از پاکی گوهر بعدل آراستی کشور</p></div>
<div class="m2"><p>که از دوران تو نیرو گرفت اشغال دیوانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو کروبی نژاد آن بزم را کز مقدم آرائی</p></div>
<div class="m2"><p>ملک گر از فلک آید بود غول بیابانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نه تنها فردی از اقران چو در چنگیزخان قاآن</p></div>
<div class="m2"><p>که چون ماهی در انجم در رجال ظل سلطانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زاعیان ظل سلطان زید قدره برگزیدت زان</p></div>
<div class="m2"><p>که دیدت بهر خود به از برادرهای اعیانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نه تو درکار اوکاهل نه او درخیر تو ذاهل</p></div>
<div class="m2"><p>دو تن یک پیشه و یک دل بتدبیر جهانبانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>امیرا بنده ات جیحون که طبعش از محیط افزون</p></div>
<div class="m2"><p>نگر کاندر مدیحت ریخت گوهرهای عمانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بس از استبرقی خلعت مرا آراستی طلعت</p></div>
<div class="m2"><p>همم بزم بهشت آئین همم دلدار غلمانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مباد آسمان آنگه که من جز اندر این خرگه</p></div>
<div class="m2"><p>بممدوحی دگر بوسم زمین در تهنیت رانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>الا تا صبح عید ازکه نماید رخ برد انده</p></div>
<div class="m2"><p>ترا برکعبه ماند بیت از ستوار بنیانی</p></div></div>