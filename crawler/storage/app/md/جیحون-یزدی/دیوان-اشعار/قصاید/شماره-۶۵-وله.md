---
title: >-
    شمارهٔ ۶۵ - وله
---
# شمارهٔ ۶۵ - وله

<div class="b" id="bn1"><div class="m1"><p>جهان بگشتم و دیدم بسی مقیم و مقام</p></div>
<div class="m2"><p>بزرگی الحق ختم است بر جناب امام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار ملک دهد شفقتش بیک سایل</p></div>
<div class="m2"><p>هزار شهر ستد سطوتش بیک پیغام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زامتلا بود اجسام عالمی زآلاش</p></div>
<div class="m2"><p>اگر چه هست تداخل محال در اجسام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس ای امام که نشناسد از نوالت کس</p></div>
<div class="m2"><p>که سنگ خاره کدامست و لعل پاره کدام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس ای امام که اینک صنوف انسانرا</p></div>
<div class="m2"><p>مواهب تو بجای عرق چکد زمشام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کدام زنده توان یافت در همه دنیا</p></div>
<div class="m2"><p>که نی طعام تواش جای روح در اندام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کدام مرده توان جست در همه عقبی</p></div>
<div class="m2"><p>که بوی خیر تواش خوش نکرده است مشام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو تو سپهر کرم را برادری ز ازل</p></div>
<div class="m2"><p>سزد چو میرمحمدحسین قطب کرام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه در ملاطفت تو به وی مجال حسود</p></div>
<div class="m2"><p>نه در مخالصت او بتو ره نمام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیان متقن تو مقتدای سحر حلال</p></div>
<div class="m2"><p>حریم حرمت او قبله گاه بیت حرام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خورد زسفره تو قوت نطفه در اصلاب</p></div>
<div class="m2"><p>کند بهمت اورشد بچه در ارحام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو رابمسند تحقیق قدسی است بیان</p></div>
<div class="m2"><p>ورا بمحفل تدریس عرشی است کلام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهر کنایه تو التذاذ نالش چنگ</p></div>
<div class="m2"><p>بهر اشاره او انحطاط گردش جام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گه تفقد تو خام مملکت پخته</p></div>
<div class="m2"><p>بر درایت او رای پخته کاران خام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو را بمصطبه جبریل ارضی است وجود</p></div>
<div class="m2"><p>ورا بمحکمه وحی سماوی است احکام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فلک ز درگه تو بی ثنا نکرده یاد</p></div>
<div class="m2"><p>ملک زحضرت او بی وضو نبرده نام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>محیط موجه ای اما یکی بجیحون بین</p></div>
<div class="m2"><p>که هست ماهی کلکش نهنگ بحر آشام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیامده چو منی پیش ازین ز پشت پدر</p></div>
<div class="m2"><p>نیاورد چو منی بعد ازین مشیمه مام</p></div></div>