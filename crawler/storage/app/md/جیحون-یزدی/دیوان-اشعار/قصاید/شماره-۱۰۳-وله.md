---
title: >-
    شمارهٔ ۱۰۳ - وله
---
# شمارهٔ ۱۰۳ - وله

<div class="b" id="bn1"><div class="m1"><p>ای کزدو چهر غیرت یک بوستان گلی</p></div>
<div class="m2"><p>از گل گذشته گاه طرب به زبلبلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نادر بکف فتد چو توئی کز جمال و صوت</p></div>
<div class="m2"><p>هم بانوای بلبل و هم بارخ گلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز شکار با دوش باز جره</p></div>
<div class="m2"><p>وقت خمار با اثر ساغر ملی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانی بسرو و ماه ولی سرو ومه نه ای</p></div>
<div class="m2"><p>کز قد و رخ بسرو و مه اندر تطاولی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماهی و لیک ماه شکر پاش پاسخی</p></div>
<div class="m2"><p>سروی ولیک سرو سمن بوی کاکلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازجور خویشتن بمن اندر تعمدی</p></div>
<div class="m2"><p>وزمیل من بخویشتن اندر تجاهلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دل چه گفته ای که همی در تصوری</p></div>
<div class="m2"><p>با جان چه کرده ای که همی در تخیلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پنهان زمن مگر تو بدل در شدایدی</p></div>
<div class="m2"><p>مخفی زمن مگر تو بجای تدللی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برهر چه روی میکنم اندر برابری</p></div>
<div class="m2"><p>در هر چه رای میزنم اندر تعقلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درچشم من ستاده چو عکس صنوبری</p></div>
<div class="m2"><p>در مغز من نشسته چو بوی قرنفلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جانا مگر بچشم و سرما مواظبی</p></div>
<div class="m2"><p>ترکا مگر بجان و تن ما قرا ولی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هنگام هجر غارت دل چون تطیری</p></div>
<div class="m2"><p>ایام وصل راحت جان چون تفالی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بریاد سوسن از چه همی در ترانه ای</p></div>
<div class="m2"><p>بربوی سنبل از چه همی در تغزلی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنمای خط که خود توبه از کشت سوسنی</p></div>
<div class="m2"><p>بگشای مو که خود تو به از باغ سنبلی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در میگساری از برمن دورشو که من</p></div>
<div class="m2"><p>پندارمت زلطف برای تنقلی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گاه شکار در برمن بازآ که من</p></div>
<div class="m2"><p>می بینمت زخوی قوی پنجه طغرلی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پرکبرتر بایوان ازشاه خلخی</p></div>
<div class="m2"><p>مغرورتر بمیدان ازگرد زابلی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تاجت زمشک او فر و تختت زسیم ناب</p></div>
<div class="m2"><p>سخت ای پسر تو صاحب جاه و تمولی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یاد آیدت که گفتم رای به ری خطاست</p></div>
<div class="m2"><p>رو جای کن بجی گر از اهل توکلی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نشنیدی و رسیدی و دیدی که در عجم</p></div>
<div class="m2"><p>کس نیست چون امیرعرب مصطفی قلی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای برتر و مهین تر سردارهای شاه</p></div>
<div class="m2"><p>کز فره چرخ چاکر و انجم یساولی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اندر دل حبیب تمامی سکونتی</p></div>
<div class="m2"><p>در خاطره حسود بکلی تزلزلی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از اختران زبخردی اندر تقدمی</p></div>
<div class="m2"><p>با آسمان زمرتبت اندر تقابلی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بنهاده در مسیل حوادث هزار سد</p></div>
<div class="m2"><p>بربسته بر شطوط نوائب دو صد پلی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا قرص مهر نطع فلک را دهد فروغ</p></div>
<div class="m2"><p>بیند فلک بخوان ظفر در تناولی</p></div></div>