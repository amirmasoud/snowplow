---
title: >-
    شمارهٔ ۱ - وله
---
# شمارهٔ ۱ - وله

<div class="b" id="bn1"><div class="m1"><p>طلعت میراست این در خلعت شاه جلیل</p></div>
<div class="m2"><p>یا ممکن گشته درگلشن براهیم خلیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این امیر ماست در تشریف شاهی جلوه گر</p></div>
<div class="m2"><p>یا بچرخ اطلس اندر جای کرده جبریل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وه از این خلعت که هست از یمن دو دست ملک</p></div>
<div class="m2"><p>ایمنش بحر محیط و ایسرش دریای نیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمس نور شمسه اش را از ازل آمد رهین</p></div>
<div class="m2"><p>چرخ عطف دامنش را تا ابد باشد دخیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هشته در هر تار و پودش صولت چنگال شیر</p></div>
<div class="m2"><p>خفته در هرآستینش سطوت خرطوم پیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم موافق زو ببالا هم منافق زو بشیب</p></div>
<div class="m2"><p>هم موالف زو عزیز و هم مخالف زو ذلیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوش گشتم تهنیت جوکش شوم تبریک کو</p></div>
<div class="m2"><p>چرخ ناگه ساختم زین شعر ترا زخود وکیل</p></div></div>
<div class="b2" id="bn8"><p>راست بین تن پوش خسرو و میرکج دینهم را</p>
<p>گر ندیدی در لباس کعبه ابراهیم را</p></div>
<div class="b" id="bn9"><div class="m1"><p>خوب رخ طبال فوجاهین دهل را چوب زن</p></div>
<div class="m2"><p>بر دهل زین جشن خوش هم چو بزن هم خوب زن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوست غالب از طرب بین خصم مغلوب ازکرب</p></div>
<div class="m2"><p>پس بشادی کوس را زین غالب و مغلوب زن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر چه در هرفوج سربازان یوسف منظر است</p></div>
<div class="m2"><p>منتخب ساز وصفی چون زاده یغوب زن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خورد ما را کرم غم تا شاه را نوشد کرم</p></div>
<div class="m2"><p>حالی اندر چشمه می غوطه چون ایوب زن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرز گردون کوب میناگرد گیتی روب غم</p></div>
<div class="m2"><p>گرد گیتی روب را با گرز گردون کوب زن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حالیا کآشوب در شهر ازخوشی زین جشن خاست</p></div>
<div class="m2"><p>زین چکامه نیز نیز تو آهنگ شهر آشوب زن</p></div></div>
<div class="b2" id="bn15"><p>راست بین تن پوش خسرو میرکج دیهیم را</p>
<p>گر ندیدی در لباس کعبه ابراهیم را</p></div>
<div class="b" id="bn16"><div class="m1"><p>باز ساقی را بکف هم نور بین هم ناربین</p></div>
<div class="m2"><p>وز خط و زلفش برخ هم مور بین هم مار بین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در سپهر جام چون خورشید می آید بموج</p></div>
<div class="m2"><p>از حبابش هر طرف ثابت نگر سیار بین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در میان باده خوران برکنار مطربان</p></div>
<div class="m2"><p>ارغنون بین رود بین طنبور بین مزمار بین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون بچشم پرفسون ساقی قدح آرد برون</p></div>
<div class="m2"><p>فتنه را رفته بخواب و بخت را بیدار بین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>توپ تندر بانگ اژدر دم چو آید در نفیر</p></div>
<div class="m2"><p>مرزدوده سقف گردونرا ز دودش تار بین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون مهینه میر برکاخ سلام آرد جلوس</p></div>
<div class="m2"><p>ریخته زین نظم نزدش لؤلؤ شهوار بین</p></div></div>
<div class="b2" id="bn22"><p>راست بین تن پوش خسرو و میرکج دیهیم را</p>
<p>گر ندیدی در لباس کعبه ابراهیم را</p></div>
<div class="b" id="bn23"><div class="m1"><p>داوری کش ماسوی منت بمولائی کشد</p></div>
<div class="m2"><p>چرخ شیب قصر او خجلت زبالائی کشد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسکه محکم بسته سد عدل را نشگفت اگر</p></div>
<div class="m2"><p>ازسکندر انتقام خون دارائی کشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چرخ پیر از عشق او مشهورعالم شد بلی</p></div>
<div class="m2"><p>عشق پیران چون بجنبد سربر سوالی کشد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرچه کار ملکتش انجم بهمراهی کنند</p></div>
<div class="m2"><p>هر چه بار شوکتش گردون بتنهائی کشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از قدومش ازض را آن مایه حاصل شدکه چرخ</p></div>
<div class="m2"><p>خاکش اندر چشم اختر بهر بینائی کشد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر سحر خورشید از این ترجیح جیحونی بمهر</p></div>
<div class="m2"><p>در بساط وی سماط از در دریائی کشد</p></div></div>
<div class="b2" id="bn29"><p>راست بین تن پوش خسرو و میرکج دیهیم را</p>
<p>گر ندیدی در لباس کعبه ابراهیم را</p></div>
<div class="b" id="bn30"><div class="m1"><p>ای ترا هردم زشه تشریف و اکرامی دگر</p></div>
<div class="m2"><p>چرخ در ایثار اقدامت با قدامی دگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو بچشم مملکت دیگر جمی دربطش و بخش</p></div>
<div class="m2"><p>وین سپهر و مهرت از جان تختی و جامی دگر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تاکه جست از خضر تو خاتم دولت شرف</p></div>
<div class="m2"><p>می نماند آفاق راز افلاک ابهامی دگر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>قلب قدوسی نژادت گاه قبض و بسط ملک</p></div>
<div class="m2"><p>هر زمان یابد زروح القدس الهامی دگر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تالوا و مسندت شد فخر عرش و ذحرفرش</p></div>
<div class="m2"><p>یافته اجرام و ارکان سیر و آرامی دگر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر کند اجرام و ارکان برخلافت امتزاج</p></div>
<div class="m2"><p>قدرتت بخشد بکون ارکان و اجرامی دگر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا که عالم بر نظامست آسمان هر بامداد</p></div>
<div class="m2"><p>گویدت زینسان درود از طبع نظامی دگر</p></div></div>
<div class="b2" id="bn37"><p>راست بین تن پوش خسرو و میرکج دیهیم را</p>
<p>گر ندیدی در لباس کعبه ابراهیم را</p></div>
<div class="b" id="bn38"><div class="m1"><p>داورا ای کز تو راحت گشت یکسر رنج من</p></div>
<div class="m2"><p>وه از این همت که از روی رنج من شد گنج من</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون نسازم زیور زانو بتان سیم ساق</p></div>
<div class="m2"><p>کز نوالت شد فرو درسیم تا آرنج من</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون بر اسب پیلتن بیدق جهانم سوی لعب</p></div>
<div class="m2"><p>گر وزیر شه بود مات آید از شطرنج من</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گشتم آخر زاهتمام چون تو خوارزمی لسان</p></div>
<div class="m2"><p>بنده نجم الدین کبری یزدهم ارگنج من</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چون کنم شکر تو کاندر قحط سال مردمی</p></div>
<div class="m2"><p>شد بدل برنشئه می سکر بزر البنج من</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تادمد یاقوت شمس از زمردین کان افق</p></div>
<div class="m2"><p>رخشدت دری چنین زافکار گوهرسنج من</p></div></div>
<div class="b2" id="bn44"><p>راست بین تن پوش خسرو میر کج دینهیم را</p>
<p>گر ندیدی در لباس کعبه ابراهیم را</p></div>