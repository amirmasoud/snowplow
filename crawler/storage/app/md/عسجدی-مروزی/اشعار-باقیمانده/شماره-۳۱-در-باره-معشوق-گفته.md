---
title: >-
    شمارهٔ  ۳۱ - در باره معشوق گفته
---
# شمارهٔ  ۳۱ - در باره معشوق گفته

<div class="b" id="bn1"><div class="m1"><p>نه هم قیمت لعل باشد بلور</p></div>
<div class="m2"><p>نه همرنگ گلنار باشد پژند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپیچد دلم چون ز پیچه بتم</p></div>
<div class="m2"><p>گشاید بر غم دلم پیچه بند</p></div></div>