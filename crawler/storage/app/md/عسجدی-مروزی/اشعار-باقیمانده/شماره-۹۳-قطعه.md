---
title: >-
    شمارهٔ  ۹۳ - قطعه
---
# شمارهٔ  ۹۳ - قطعه

<div class="b" id="bn1"><div class="m1"><p>گفتم همی چه گوئی ای هیز گلخنی</p></div>
<div class="m2"><p>گفتا که چه شنیدی ای پیر مسجدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم یکی که مسجدیم چون نه غرمنم</p></div>
<div class="m2"><p>گفتا تو نیز هم نه چنین پیر زاهدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم پلید بینی، لنگی بزرگ پای</p></div>
<div class="m2"><p>محکم ستبر ساقی زین گرد ساعدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون هیز طیره شد ز میان ربوخه گفت</p></div>
<div class="m2"><p>بر ریش خربطان ریم ای خواجه عسجدی</p></div></div>