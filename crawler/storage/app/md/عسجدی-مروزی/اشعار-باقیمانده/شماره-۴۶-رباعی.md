---
title: >-
    شمارهٔ  ۴۶ - رباعی
---
# شمارهٔ  ۴۶ - رباعی

<div class="b" id="bn1"><div class="m1"><p>چون شاه بگیرد بکف اندر شمشیر</p></div>
<div class="m2"><p>از بیم بیفکند ز کفها شم شیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب که بمردی و تهور مثلش</p></div>
<div class="m2"><p>در معرکه با تیغ گزارد شم شیر</p></div></div>