---
title: >-
    شمارهٔ  ۸۸ - از قطعه ای
---
# شمارهٔ  ۸۸ - از قطعه ای

<div class="b" id="bn1"><div class="m1"><p>کلکش چو مرغکی است دو دیده پر آب مشک</p></div>
<div class="m2"><p>وز بهر خیر و شر دو زبان است و تن یکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای طبع کارساز چه کردم ترا چه بود</p></div>
<div class="m2"><p>با من همی نسازی و دایم همی ژکی</p></div></div>