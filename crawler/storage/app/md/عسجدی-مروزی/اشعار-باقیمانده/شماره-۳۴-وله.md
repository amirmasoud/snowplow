---
title: >-
    شمارهٔ  ۳۴ - وله
---
# شمارهٔ  ۳۴ - وله

<div class="b" id="bn1"><div class="m1"><p>هر که بر درگه ملوک بود</p></div>
<div class="m2"><p>از چنین کار با خدوک بود</p></div></div>