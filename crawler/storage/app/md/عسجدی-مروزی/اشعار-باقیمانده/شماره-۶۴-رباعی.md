---
title: >-
    شمارهٔ  ۶۴ - رباعی
---
# شمارهٔ  ۶۴ - رباعی

<div class="b" id="bn1"><div class="m1"><p>آن جسم پیاله بین بجان آبستن</p></div>
<div class="m2"><p>همچون سمنی بارغوان آبستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی نی غلطم پیاله از غایت لطف</p></div>
<div class="m2"><p>آبیست به آتش روان آبستن</p></div></div>