---
title: >-
    شمارهٔ  ۳۶ - در توصیف بهار و مدح سید ابونصر
---
# شمارهٔ  ۳۶ - در توصیف بهار و مدح سید ابونصر

<div class="b" id="bn1"><div class="m1"><p>بنوبهار جوان شد جهان پیر ز سر</p></div>
<div class="m2"><p>ز روی سبزه بر آورد شاخ نرگس سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خزان جهان را عهد ار چه کرده بود کهن</p></div>
<div class="m2"><p>بهار عهد جهان باز تازه کرد ز سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوا نشاند ببرگ شکوفه در، یاقوت</p></div>
<div class="m2"><p>صبا فشاند بشاخ بنفشه بر، عنبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بوی باد برآورد سبزه مشگ نبات</p></div>
<div class="m2"><p>ز سیل ابر برآورد لاله در ثمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بهر آنک ز پیروزه فرخیست نشان</p></div>
<div class="m2"><p>ز بهر آنک ز بیجاده روشنیست اثر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبات سبزه ز پیروزه برفکند ردا</p></div>
<div class="m2"><p>ستاک لاله ز بیجاده برنهاد افسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر ز سندس حله ندید باغ بزیر</p></div>
<div class="m2"><p>اگر ز عبقر کله ندید شاخ زبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببین که باغ کنون حله بافد از سندس</p></div>
<div class="m2"><p>ببین که شاخ کنون کله بندد از عبقر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بسکه بر تن لاله کند زمانه نگار</p></div>
<div class="m2"><p>ز بسکه بر سر نرگس کند زمانه صور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیاض لاله ز نور و سواد لاله ز دود</p></div>
<div class="m2"><p>کنار نرگس سیم و میان نرگس زر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دهان گل ز سرشگ دو چشم ابر ملا</p></div>
<div class="m2"><p>ز عقدهای عقیق و ز دانهای درر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شقایق و سمن از مهر کرده روی بروی</p></div>
<div class="m2"><p>بنفشه و گل، از ناز برده سر در سر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز خنده گاه ز هم باز برده لاله سرخ</p></div>
<div class="m2"><p>بگریه چشم ز هم باز کرده نرگس تر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز بسکه تاک گلان، در دارد و مرجان</p></div>
<div class="m2"><p>ز بسکه شاخ شجر زر دارد و زیور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه معقد درند تاکهای گلان</p></div>
<div class="m2"><p>همه مرصع زراند شاخهای شجر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز سبزه گشته تن دشت مشتری کردار</p></div>
<div class="m2"><p>ز لاله گشته سر کوه مشتری پیکر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فراز شاخ نکوتر بود گل نرگس</p></div>
<div class="m2"><p>میان نجم ثریا نکوترست قمر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو راهبان بتعبد همه بتان بهار</p></div>
<div class="m2"><p>بپیش در، محراب و بدست بر، مجمر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا ز ناله تن دل، شده بگونه نیل</p></div>
<div class="m2"><p>مرا ز آذر دل، اشگ گشته چون آذر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چرا همیشه بآذر درست آذرگون</p></div>
<div class="m2"><p>چرا همیشه به نیل اندرست نیلوفر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پر از بخور، دم گل شده ز تف بخار</p></div>
<div class="m2"><p>پر از خطر، لب لاله شده ز قطر مطر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خطر ز ابر گرفت اینجهان و ابر همی</p></div>
<div class="m2"><p>ز کف بار خدا، دهخدا گرفت خطر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نجیب (سید ابونصر) آن خجسته نژاد</p></div>
<div class="m2"><p>که ابر جود شکارست و ببر شیر شکر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سخنوری که یمین دول شده بسخن</p></div>
<div class="m2"><p>هنروری که امین ملل شده به هنر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دلش بوقت سخا، اخترست زر شعاع</p></div>
<div class="m2"><p>کفش بگاه لقا، آذرست تیغ شرر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر چه گردون عالی تر از زمین بقیاس</p></div>
<div class="m2"><p>اگر چه دریا کافی تر از شمر بقدر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو وهمش آید گردون بود بقدر زمین</p></div>
<div class="m2"><p>چو کفش آید، دریا بود بنرخ شمر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز هر چه کافزون خوانی، بنعمت او افزون</p></div>
<div class="m2"><p>ز هر چه برتر دانی، بهمت او برتر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر کند بگیاه ارج مهمتریش نگاه</p></div>
<div class="m2"><p>اگر کند به حجر، فر بهتریش نظر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو زعفران شود از ارج او، هرآنچه گیاه</p></div>
<div class="m2"><p>چو بهرمان شود از فر او، هرآنچه حجر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر ادیب نپوید مگر بپای ادب</p></div>
<div class="m2"><p>سوی بصیر نبیند مگر بچشم بصر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تبارک اله از آن تیره سار خامه او</p></div>
<div class="m2"><p>که نام او قلم قدرتست در دفتر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پرنده تیری کو را، دو سر بود پیکان</p></div>
<div class="m2"><p>رونده رمحی کاو را، دو شاخ باشد سر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زبان بریده تواند، همیشه گفت سخن</p></div>
<div class="m2"><p>میان کفیده تواند، همیشه بست کمر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنان صریرش وقت فنا مخالف را</p></div>
<div class="m2"><p>چنانک وقت فنا، قوم عاد را صرصر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز فر بار خدا، دهخداست قدرت او</p></div>
<div class="m2"><p>که قدرت ازلی دادش ایزد اکبر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ممحدی که نظام زمانه گشته بجد</p></div>
<div class="m2"><p>موقری که قوام ستاره گشته به فر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو روز میدان باشد مبارز میدان</p></div>
<div class="m2"><p>چو وقت محضر باشد، مناظر محضر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر آن کجا که بود جفن ملت، او شمشیر</p></div>
<div class="m2"><p>هر آن کجا که بود سطر دولت، او مسطر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدان بشر را فخرست بر همه اجناس</p></div>
<div class="m2"><p>که او میان بشر هست، اختیار بشر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدانش خواهم گفتن، که ای زمین فتوح</p></div>
<div class="m2"><p>بدانش خواهم گفتن، که ای درخت هنر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که هست همچو زمینی، که چرخ دارد بار</p></div>
<div class="m2"><p>که هست همچو درختی، که ماه دارد بر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدانک در خور جای پدر پسر باشد</p></div>
<div class="m2"><p>بدادش ایزد دانا یکی پسر در خور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گلی که هیچ نیاید بدی ز باد خزان</p></div>
<div class="m2"><p>مهی که هیچ نبیند کسوف ز آفت خور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بسی نپاید تا چون پدر به تیغ و قلم</p></div>
<div class="m2"><p>سوار گردد و گردد مدیح را محور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بخواند آنک بخوانند، هرکس از هر باب</p></div>
<div class="m2"><p>بداند آنک ندانند، هرکس از هر در</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همیشه تا گذر هفت، بر دوازده برج</p></div>
<div class="m2"><p>همیشه تا مدد مردم، از چهار گهر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پسر همیشه بپا یاد، با پدر بمراد</p></div>
<div class="m2"><p>پدر همیشه بماناد، با خجسته پسر</p></div></div>