---
title: >-
    شمارهٔ  ۵۴ - وله
---
# شمارهٔ  ۵۴ - وله

<div class="b" id="bn1"><div class="m1"><p>تا یکی خم بشکند ریزه شود سیصد سبو</p></div>
<div class="m2"><p>تا مرد پیری بپیش او مرد سیصد کلوک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که موک مردمان جوید بشو گو خط دو کش</p></div>
<div class="m2"><p>کی نخست او را زند باشد موک؟</p></div></div>