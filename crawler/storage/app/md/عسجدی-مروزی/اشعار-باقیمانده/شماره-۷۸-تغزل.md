---
title: >-
    شمارهٔ  ۷۸ - تغزل
---
# شمارهٔ  ۷۸ - تغزل

<div class="b" id="bn1"><div class="m1"><p>ساقی به آبگینه بغداد در فکند</p></div>
<div class="m2"><p>یاقوت رنگ باده خوشخوار مشک‌بو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی که پیش عاشق، معشوق مهربانش</p></div>
<div class="m2"><p>بگریست، اوفتاد به رخسارش اشک او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دل برآورید دم سرد و آه گرم</p></div>
<div class="m2"><p>بفسُرد آب دیده و بگداخت رنگِ رو</p></div></div>