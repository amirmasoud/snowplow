---
title: >-
    شمارهٔ  ۲۶ - در دور تو
---
# شمارهٔ  ۲۶ - در دور تو

<div class="b" id="bn1"><div class="m1"><p>در دور تو عقل کل کنشتی گردد</p></div>
<div class="m2"><p>حسن ابدی شهره بزشتی گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکستر کشتگانت در دوزخ عشق</p></div>
<div class="m2"><p>پیرایه حوران بهشتی گردد</p></div></div>