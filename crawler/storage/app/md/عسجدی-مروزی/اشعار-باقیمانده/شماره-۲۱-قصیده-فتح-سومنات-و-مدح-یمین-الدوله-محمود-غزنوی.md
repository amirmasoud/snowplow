---
title: >-
    شمارهٔ  ۲۱ - قصیده فتح سومنات و مدح یمین الدوله محمود غزنوی
---
# شمارهٔ  ۲۱ - قصیده فتح سومنات و مدح یمین الدوله محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>جان مرا غمت هدف حادثات کرد</p></div>
<div class="m2"><p>تا عشق سوی من نظر التفات کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال مرا و زلف پریشان خویش را</p></div>
<div class="m2"><p>در راه عاشقی رقم مشکلات کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا شاه خسروان سفر سومنات کرد</p></div>
<div class="m2"><p>کردار خویش را علم معجزات کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آثار روشن ملکان گذشته را</p></div>
<div class="m2"><p>نزدیک بخردان همه از مشکلات کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزدود ز اهل کفر جهان را بر اهل دین</p></div>
<div class="m2"><p>شکر و دعای خویشتن از واجبات کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محمود شهریار کریم آنکه ملک را</p></div>
<div class="m2"><p>بنیاد بر محامد و بر مکرمات کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شطرنج ملک باخت ملک با هزار شاه</p></div>
<div class="m2"><p>هر شاه را بلعب دگر شاهمات کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاها تو از سکندر بیشی، بدانجهت</p></div>
<div class="m2"><p>کو هر سفر که کرد بدیگر جهات کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عین الرضای ایزد جوئی تو در سفر</p></div>
<div class="m2"><p>باز او سفر بجستن عین الحیات کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو کارها به نیزه و تیر و کمان کنی</p></div>
<div class="m2"><p>او کارها به حیله و کلک و دوات کرد</p></div></div>