---
title: >-
    شمارهٔ  ۲۷ - وعده دوست
---
# شمارهٔ  ۲۷ - وعده دوست

<div class="b" id="bn1"><div class="m1"><p>دل دوش هزار چاره سازی میکرد</p></div>
<div class="m2"><p>با وعده دوست عشقبازی میکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بر کف پای تو تواند مالید</p></div>
<div class="m2"><p>دل را همه شب دیده نمازی میکرد</p></div></div>