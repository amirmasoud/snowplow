---
title: >-
    شمارهٔ  ۱ - از مدایح
---
# شمارهٔ  ۱ - از مدایح

<div class="b" id="bn1"><div class="m1"><p>بامید قبولت بکر فکرم</p></div>
<div class="m2"><p>چو بهر یوسف مصری زلیخا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بانواع نفایس خویشتن را</p></div>
<div class="m2"><p>بسان نوعروسی کرده آسا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی کز خدمتت دوری کند هیچ</p></div>
<div class="m2"><p>برو دشمن شود گردون گردا</p></div></div>