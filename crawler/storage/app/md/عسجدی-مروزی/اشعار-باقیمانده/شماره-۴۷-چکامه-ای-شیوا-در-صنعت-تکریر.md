---
title: >-
    شمارهٔ  ۴۷ - چکامه ای شیوا در صنعت تکریر
---
# شمارهٔ  ۴۷ - چکامه ای شیوا در صنعت تکریر

<div class="b" id="bn1"><div class="m1"><p>باران قطره قطره همی بارم ابروار</p></div>
<div class="m2"><p>هر روز خیره خیره ازین چشم سیل بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آن قطره قطره، قطره باران شده خجل</p></div>
<div class="m2"><p>ز آن خیره خیره، خیره دل من ز هجر یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاری که ذره ذره نماید همی نظر</p></div>
<div class="m2"><p>هجرانش باره باره بمن برنهاد بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آن ذره ذره، ذره بدل آیدم چو کوه</p></div>
<div class="m2"><p>ز آن باره باره، باره بچشم آیدم غبار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدنش نوبه نوبه، چو نو ماه گاه گاه</p></div>
<div class="m2"><p>رفتنش گوشه گوشه، کران کرده زین دیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین نوبه نوبه، نوبه خوابم شده تباه</p></div>
<div class="m2"><p>ز ان گوشه گوشه، گوشه جان و دلم فکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل گشته رخنه رخنه بزاری بتیغ هجر</p></div>
<div class="m2"><p>ز آن مشک توده توده بر آن گرد لاله زار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آن رخنه رخنه، رخنه شده عقل و دین مرا</p></div>
<div class="m2"><p>ز آن توده توده، توده بدل بر غم نگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دندانش دانه دانه درّ است جانفزای</p></div>
<div class="m2"><p>لبهاش پاره پاره عقیق است آبدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آن دانه دانه، دانه در یتیم زرد</p></div>
<div class="m2"><p>ز آن پاره پاره، پاره یاقوت سرخ خوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حوری که تیره تیره بپوشد رخان روز</p></div>
<div class="m2"><p>چونانکه طره طره شود طره بر عذار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز آن تیره تیره، تیره شود نور آفتاب</p></div>
<div class="m2"><p>ز آن طره طره، طره شود طره تتار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طره‌ش چو حلقه حلقه قطار از پی قطار</p></div>
<div class="m2"><p>حلقه‌ش چو چشمه چشمه نور هدی قطار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز آن حلقه حلقه، حلقه زنجیر شرمگین</p></div>
<div class="m2"><p>ز آن چشمه چشمه، چشمه خورشید درد خوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زلفینش نافه نافه گشاید نثار مشک</p></div>
<div class="m2"><p>رخسارش لاله لاله نماید فروغ نار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز آن نافه نافه، نافه خوشبوی با دریغ</p></div>
<div class="m2"><p>ز آن لاله لاله، لاله خودروی شرمسار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سیم است بیضه بیضه بر آن سیم سنگدل</p></div>
<div class="m2"><p>ریحان دسته دسته بر آن طرف گل نگار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز آن بیضه بیضه، بیضه کافور جفت خاک</p></div>
<div class="m2"><p>ز آن دسته دسته، دسته سنبل ببوی خار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تیمار عقده عقده، اندر دلم زده‌ست</p></div>
<div class="m2"><p>و ز خواجه تحفه تحفه نشاط دل و قرار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز آن عقده عقده، عقده ابروی تو مدام</p></div>
<div class="m2"><p>ز آن تحفه تحفه، تحفه چنین مدح نامدار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دی خواجه تازه تازه بر الفاظ شعر من</p></div>
<div class="m2"><p>ز آن گونه گونه نیز بمن کرد بر نثار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز آن تازه تازه، تازه بهر شهر ازو شکر</p></div>
<div class="m2"><p>ز آن گونه گونه، گونه من چون گل بهار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همتش پایه پایه عزیز و شود بلند</p></div>
<div class="m2"><p>گسترده سایه سایه، از هر سویی هزار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز آن پایه پایه، پایه گه خدمت ملوک</p></div>
<div class="m2"><p>ز آن سایه سایه، سایه گه سجده کبار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دینار کیسه کیسه دهد اهل فضل را</p></div>
<div class="m2"><p>چونانکه سله سله برد طاقت ستار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز آن کیسه کیسه، کیسه صراف عیب گیر</p></div>
<div class="m2"><p>ز آن سله سله، سله بزاز مستعار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از عطر حبه حبه دهد هر کسی عطا</p></div>
<div class="m2"><p>از جود ریزه ریزه کم و بیش پر عیار</p></div></div>
<div class="c"><p>...</p></div>
<div class="b" id="bn28"><div class="m1"><p>او باز حقه حقه دهد عطر خلق را</p></div>
<div class="m2"><p>چونانکه تخته تخته دهد عود را کبار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز آن حقه حقه، حقه سیماب زار از اوست</p></div>
<div class="m2"><p>ز آن تخته تخته، تخته ارزیز زبر و زار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از چرخ بهره بهره طرب باد خواجه را</p></div>
<div class="m2"><p>وز خلق شهره شهره ثناهاش یادگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز آن بهره بهره، بهره رسیده بمانعم</p></div>
<div class="m2"><p>زآن شهره شهره، شهره ایام شهریار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از چرخ برخه برخه سعادت بجانش باد</p></div>
<div class="m2"><p>از عرش جمله جمله ز احسان کردگار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز آن برخه برخه، برخه ابر جان او ز سعد</p></div>
<div class="m2"><p>ز آن جمله جمله، جمله مر او را ز بخت یار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا هست سوره سوره کتاب خدای ما</p></div>
<div class="m2"><p>وز علم نکته نکته بهر سوره آشکار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز آن سوره سوره، سوره مهترش باد حرز</p></div>
<div class="m2"><p>ز آن نکته نکته، نکته بهترش غمگسار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا هست خامه خامه بهر بادیه زریک</p></div>
<div class="m2"><p>وز باد غیبه غیبه بر او نقش بی شمار</p></div></div>