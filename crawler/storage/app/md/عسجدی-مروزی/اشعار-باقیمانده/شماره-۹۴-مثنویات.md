---
title: >-
    شمارهٔ  ۹۴ - مثنویات
---
# شمارهٔ  ۹۴ - مثنویات

<div class="b" id="bn1"><div class="m1"><p>چو آمد گه زادن زن فراز</p></div>
<div class="m2"><p>بکشکینه گرمش آمد نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من و زن در آن خانه تنها و بس</p></div>
<div class="m2"><p>مرا گفت کی شوی، فریادرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر شوربائی بچنگ آوری</p></div>
<div class="m2"><p>من مرده را باز رنگ آوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نااهل را قدر گردد بلند</p></div>
<div class="m2"><p>نباشد چو آزاده هوشمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه چنارست برگش بزرگ</p></div>
<div class="m2"><p>نباشد در آن نفع برگ تورک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدایا تو این جمله را دست گیر</p></div>
<div class="m2"><p>ورستاد جودت ز ما وامگیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزخمی کزوغ ورا خرد کرد</p></div>
<div class="m2"><p>همین حرب سازند مردان مرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرد را کرد گردن و سر و پشت</p></div>
<div class="m2"><p>کوفته سر به سر به کاج و به مشت</p></div></div>