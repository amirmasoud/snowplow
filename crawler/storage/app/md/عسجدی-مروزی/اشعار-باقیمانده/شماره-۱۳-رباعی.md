---
title: >-
    شمارهٔ  ۱۳ - رباعی
---
# شمارهٔ  ۱۳ - رباعی

<div class="b" id="bn1"><div class="m1"><p>ای گشته خجل آبحیات از دهنت</p></div>
<div class="m2"><p>سرو از قد و ماه از رخ و سیم از ذقنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاحب نظری کجاست تا درنگرد</p></div>
<div class="m2"><p>صد یوسف مصر در ته پیرهنت</p></div></div>