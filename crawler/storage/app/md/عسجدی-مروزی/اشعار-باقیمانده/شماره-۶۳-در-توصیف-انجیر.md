---
title: >-
    شمارهٔ  ۶۳ - در توصیف انجیر
---
# شمارهٔ  ۶۳ - در توصیف انجیر

<div class="b" id="bn1"><div class="m1"><p>انجیر کش از شاخ بستدی تو</p></div>
<div class="m2"><p>وصفش تو بیک بیت بشنو از من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون برگ گل زرد خرد کرده</p></div>
<div class="m2"><p>سربسته و کرده میان پر ارزن</p></div></div>