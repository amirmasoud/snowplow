---
title: >-
    بخش ۵ - در نصیحت و تحریض در سلوک و ریاضت و تهذیب اخلاق سیۀ بحسنه و مخالفت نفس و هوی و متابعت پیر کامل رهنما و بیان روش اولیا و طریق وصول به مقامات عرفا
---
# بخش ۵ - در نصیحت و تحریض در سلوک و ریاضت و تهذیب اخلاق سیۀ بحسنه و مخالفت نفس و هوی و متابعت پیر کامل رهنما و بیان روش اولیا و طریق وصول به مقامات عرفا

<div class="b" id="bn1"><div class="m1"><p>پاک شو از نخوت و حرص و حسد</p></div>
<div class="m2"><p>تا که باشی بندۀ پاک احد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تکبر وز رعونت دور باش</p></div>
<div class="m2"><p>تا که گردی زاولیای خواجه تاش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که از اخلاق بد وارسته شد</p></div>
<div class="m2"><p>او به اوصاف نکو پیوسته شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لطف و احسان و کرم را پیشه کن</p></div>
<div class="m2"><p>نیکویی کن وز بدی اندیشه کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رو فتوت را شعار خویش ساز</p></div>
<div class="m2"><p>عفت و حکمت دثار خویش ساز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سخاوت کوش و در بذل و عطا</p></div>
<div class="m2"><p>تا بیابی تو مقام اصفیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اول از نفس و هوی بیزار شو</p></div>
<div class="m2"><p>پس ب ه کوی عشق جانان خوار شو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خودپرستی را رها کن حق پرست</p></div>
<div class="m2"><p>بت پرست د هر که او از خود نرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی ز دست نفس خود یابد خلاص</p></div>
<div class="m2"><p>تا نگردد بندۀ خاص الخواص</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خودی بگذر خدا را بنده باش</p></div>
<div class="m2"><p>پیش ره بینان چو خاک افکنده باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیستی بپذیر و هستی را بهل</p></div>
<div class="m2"><p>خاک ره شو زیر پای اهل دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کار ناید طمطرق و گفت و گوی</p></div>
<div class="m2"><p>گر سلوک ره کن ی پیری بجوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر روی این راه را بی راهبر</p></div>
<div class="m2"><p>کی تو در منزل رسی ای بی خبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اندرین ره گر به تنها می روی</p></div>
<div class="m2"><p>ریشخند و سخرۀ شیطان شوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر سفر خواهی بجو اول رفیق</p></div>
<div class="m2"><p>پس برو ایمن ز رهزن در طریق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صحبت غالب طلب گر طالبی</p></div>
<div class="m2"><p>تا به مطلوبی رساند غالبی</p></div></div>