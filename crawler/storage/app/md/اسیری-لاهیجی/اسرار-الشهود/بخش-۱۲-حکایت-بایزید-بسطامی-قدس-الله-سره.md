---
title: >-
    بخش ۱۲ - حکایت بایزید بسطامی قدس اللّه سره
---
# بخش ۱۲ - حکایت بایزید بسطامی قدس اللّه سره

<div class="b" id="bn1"><div class="m1"><p>والی اقلیم عرفان بایزید</p></div>
<div class="m2"><p>آنکه دایم بود عشقش بر مزید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت حق فرمود الهامی بدل</p></div>
<div class="m2"><p>آمد آوازی و اعلامی بدل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که خزینه ما ز هر جنسی پر است</p></div>
<div class="m2"><p>اندرین گنجینه هر نقدی دراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طاعت مقبول خود اینجا بسی است</p></div>
<div class="m2"><p>خدمت لایق بسی با هر کسی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علم و اسرار و معارف بی حد است</p></div>
<div class="m2"><p>زهد و تقوی بی حساب و بیعداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این اشارات و ارادات و فنون</p></div>
<div class="m2"><p>خود ز بسیاریست از احصاء فزون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر مرا خواهی بیا چیزی بیار</p></div>
<div class="m2"><p>کان نباشد نزد من ای مرد کار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم آن چیزی که نبود مر ترا</p></div>
<div class="m2"><p>خود چه باشد گو الهی مر مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت آن عجز است و خواری و نیاز</p></div>
<div class="m2"><p>نیستی و درد و سوز جانگداز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فقر و مسکینی ز خود آوارگی</p></div>
<div class="m2"><p>دلشکسته بودن و بیچارگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عارفان را اینچنین آمد خطاب</p></div>
<div class="m2"><p>خویشتن بین کی بود ز اهل صواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرد رعنا دان که از حق غافلست</p></div>
<div class="m2"><p>در طریق اهل عرفان جاهلست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رهروانی کاندرین ره رفته اند</p></div>
<div class="m2"><p>اینچنین هشیار و آگه رفته اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هستی خود از میان برداشتند</p></div>
<div class="m2"><p>خویش را معدوم محض انگاشتند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دایۀ خود را بجستند این فریق</p></div>
<div class="m2"><p>بیخود از خود رفته اند اندر طریق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرده اند ایشان به راه ذوالمنن</p></div>
<div class="m2"><p>ذل و خواری را شعار خویشتن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در خور عارف نباشد ما و من</p></div>
<div class="m2"><p>اندرین ره بی منی باید شدن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون تو من گویی بود بی شک دو من</p></div>
<div class="m2"><p>من کجا گنجد به راه ذوالمنن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بهر جنت زاهدان را جست و جوست</p></div>
<div class="m2"><p>در دل عاشق نگنجد غیر دوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عاشقانت را به جان باشد مرید</p></div>
<div class="m2"><p>هر کسی کو لذت عشقت چشید</p></div></div>