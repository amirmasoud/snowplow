---
title: >-
    بخش ۱۹ - حکایت زاهدی که به وقت بایزید در بسطام وحید التقوی بود
---
# بخش ۱۹ - حکایت زاهدی که به وقت بایزید در بسطام وحید التقوی بود

<div class="b" id="bn1"><div class="m1"><p>زاهدی در وقت سلطان بایزید</p></div>
<div class="m2"><p>بود در بسطام در تقوی وحید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود بس صاحب قبول و با تبع</p></div>
<div class="m2"><p>در میان شهر شهره در ورع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صایم الدهر و به شب قایم چو شمع</p></div>
<div class="m2"><p>دایم از خوفش روان در دیده دمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز او از صحبت سلطان دین</p></div>
<div class="m2"><p>بایزید آن شاه ارباب یقین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود نبد خالی ز اخلاصی که داشت</p></div>
<div class="m2"><p>بدملازم صبح و شام و عصر و چاشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شنیدی گفته های بایزید</p></div>
<div class="m2"><p>زان سخن ذوقش شدی دایم مزید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیخ روزی در مقام اولیا</p></div>
<div class="m2"><p>رمزها می گفت با اهل صفا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت زاهد شیخ دین را کای امام</p></div>
<div class="m2"><p>مدت سی سال اکنون شد تمام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که به روز آخر همیشه صایمم</p></div>
<div class="m2"><p>شب همه شب در عبادت قایمم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرده ام پیوسته ترک خواب و خور</p></div>
<div class="m2"><p>زانچه می گویی نمی یابم اثر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می کنم تصدیق این احوال و فن</p></div>
<div class="m2"><p>دوست می دارم همیشه این سخن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خود نمی دانم حجاب ما ز چیست</p></div>
<div class="m2"><p>واقفم کن چون ز تو پوشیده نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بایزیدش گفت صد سال دگر</p></div>
<div class="m2"><p>روز تا شب با همه شب تا سحر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در نماز و روزه باشی دایماً</p></div>
<div class="m2"><p>هم نخواهد بود بویی زین ترا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت زاهد شیخ را کآخر چرا</p></div>
<div class="m2"><p>سد راهم چیست گو بهر خدا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شیخ گفتش زانکه محجوبی به خود</p></div>
<div class="m2"><p>هستی تو هست در راه تو سد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت زاهد چیست دردم را دوا</p></div>
<div class="m2"><p>تو طبیبی کن علاج جان ما</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شیخ گفت او را که تو هرگز قبول</p></div>
<div class="m2"><p>می نخواهی کرد و خواهی شد ملول</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت شیخا من نیم مرد فضول</p></div>
<div class="m2"><p>هر چه فرمایی به جان دارم قبول</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شیخ گفت او را همین ساعت برو</p></div>
<div class="m2"><p>ریش و موی سر تراش و پاک شو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جامه و دستار بر کن ای سلیم</p></div>
<div class="m2"><p>بر میان بند یک ازاری از گلیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>توبرۀ پر جوز در گردن فکن</p></div>
<div class="m2"><p>رو به بازار آنگهی بی ما و من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شاخ هستی را بکن از بیخ و بن</p></div>
<div class="m2"><p>کودکان هر محلت گرد کن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گو که یک سیلی هر آن کو زد مرا</p></div>
<div class="m2"><p>می دهم یک جوزش از بهر خدا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در تمام شهر گرد و گو چنین</p></div>
<div class="m2"><p>از سر صدق و ز اخلاص و یقین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر کجا که می شناس ن د مر ترا</p></div>
<div class="m2"><p>همچنین می کن که اینستت دوا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زانکه این هستی حجاب محکم است</p></div>
<div class="m2"><p>این سد از سد سکندر کی کم است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت زاهد کی توانم کرد این</p></div>
<div class="m2"><p>گو دوای دیگر ای دانای دین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شیخ گفت او را که اول گفتمت</p></div>
<div class="m2"><p>تو نخواهی کرد کاین کاریست سخت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>غیر از این خود نیست دردت را دوا</p></div>
<div class="m2"><p>هست این درمان دردت زاهدا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در ره مولی حجاب زین بتر</p></div>
<div class="m2"><p>نیست رهرو را اگر داری خبر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سالها گر چه ریاضت ها کشید</p></div>
<div class="m2"><p>چون نرست از خود وصال حق ندید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جان او چون واصل جانان نشد</p></div>
<div class="m2"><p>دردمندان را از او درمان نشد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از چنین سالک نیاید رهبری</p></div>
<div class="m2"><p>چون نشد او از حجاب خود بری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون به وصل دوست او را ره نشد</p></div>
<div class="m2"><p>از ره و منزل ز حق آگه نشد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سالکان را رهنمایی چون کند</p></div>
<div class="m2"><p>در طریقت پیشوایی چون کند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون به وصل دوست او را نیست بار</p></div>
<div class="m2"><p>رو سر خود گیر و دست از وی بدار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ور نه سرگردان شوی سر دم کنی</p></div>
<div class="m2"><p>در خودی بی شک خدا را گم کنی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در ره حق سالکا بیخود درآی</p></div>
<div class="m2"><p>همچو آن زاهد مرو راه خدای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر چه عمری در ریاضت می گذاشت</p></div>
<div class="m2"><p>چونکه نگذشت از خودی سودی نداشت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون شوی دور از خودی بر ما رسی</p></div>
<div class="m2"><p>تا تو با خویشی بود وصلش محال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای دل از مردان حق غافل مشو</p></div>
<div class="m2"><p>جان به عشق این جماعت کن گرو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با تو گفتم مجملی ز احوال شان</p></div>
<div class="m2"><p>تا بدانی زین نشانها حالشان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر خدا جویی بجو این قوم را</p></div>
<div class="m2"><p>زانکه ایشانند خاصان خدا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سد راه خویش دان هستی خود</p></div>
<div class="m2"><p>نیست شو زین هستی و پستی خود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر همی خواهی که بینی روی یار</p></div>
<div class="m2"><p>خویش را از پردۀ هستی برآر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هر که او در ره گرفتار خودست</p></div>
<div class="m2"><p>دایماً محجوب از یار خودست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پردۀ خود از میان بردار زود</p></div>
<div class="m2"><p>در پس پرده ببین دیدار زود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نیستی از خویش عین وصل اوست</p></div>
<div class="m2"><p>بگذر از هستی دلت گر وصل جوست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا تو با خویشی بود وصلش محال</p></div>
<div class="m2"><p>بیخود از خودشو که تا یابی وصال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خودپرستی کار محجوبان بود</p></div>
<div class="m2"><p>نیستی این درد را درمان بود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هر که خود از خویش خالی کرده است</p></div>
<div class="m2"><p>گوی دولت از میان او برده است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پاک کن زنگ دویی از خویشتن</p></div>
<div class="m2"><p>تا ز خود بینی جمال ذوالمنن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پاک کن آیینه دل را ز زنگ</p></div>
<div class="m2"><p>تا ببینی هر چه خواهی بیدرنگ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ساز جاروبی ز عشق ای مرد کار</p></div>
<div class="m2"><p>خانۀ دل را بروب از هر غبار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از غبار خویش خود را پاک کن</p></div>
<div class="m2"><p>پس به خود دیدار یار ادراک کن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سد خود را از ره خود دور کن</p></div>
<div class="m2"><p>وز وصالش جان ودل پر نور کن</p></div></div>