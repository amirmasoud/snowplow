---
title: >-
    بخش ۳۹ - اشارت به حدیث قدسی: لو لم تذنبوا الذهبت بکم و خلقت خلقاً یذنبوا و یستغفرون فاغفر لهم، تا احکام اسمائی ظهور یابد
---
# بخش ۳۹ - اشارت به حدیث قدسی: لو لم تذنبوا الذهبت بکم و خلقت خلقاً یذنبوا و یستغفرون فاغفر لهم، تا احکام اسمائی ظهور یابد

<div class="b" id="bn1"><div class="m1"><p>در حدی ث قدس حق فرموده است</p></div>
<div class="m2"><p>سر پنهان را عیان بنموده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت لو لم تذنبوا یعنی شما</p></div>
<div class="m2"><p>گر نمی کردید این جرم و خطا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من شما را بر دمی سوی عدم</p></div>
<div class="m2"><p>آفریدم خلق دیگر از کرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا که ایشان می بکردندی گناه</p></div>
<div class="m2"><p>پس به استغفار گشتی عذر خواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا من ایشان را بیامرزیدمی</p></div>
<div class="m2"><p>از رئوفی آ ن گنه بخشیدمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا که غفاری من ظاهر شدی</p></div>
<div class="m2"><p>زان گناهان غافری پیدا بدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی شود ظاهر تجلی رحیم</p></div>
<div class="m2"><p>گر گنه از ما نمی آمد مقیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مقتضای اسم تواب و غفور</p></div>
<div class="m2"><p>هست جرم از ما چه می افتی تو دور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنچه می دانم نمی گویم تمام</p></div>
<div class="m2"><p>زانکه می ترسم که لغزد پای عام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بحر عرفان گر چه می آید به جوش</p></div>
<div class="m2"><p>حاکم شرعم همی گوید خموش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر بگیرد قاهر است و منتقم</p></div>
<div class="m2"><p>می کند از ما سرانجام مهم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور همی بخشد رئوف است و رحیم</p></div>
<div class="m2"><p>می شود پیدا تجلی کریم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر به جنت می برد غفار دان</p></div>
<div class="m2"><p>ور به دوزخ می برد قهار خوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هست عاصی را به رحمت افتقار</p></div>
<div class="m2"><p>م ج رمان را لازم آمد انکسار</p></div></div>