---
title: >-
    بخش ۶۲ - در بیان آنکه عمل بی اخلاص وسیلۀ قرب نمی‌گردد بلکه موجب زیادتی بعد شود
---
# بخش ۶۲ - در بیان آنکه عمل بی اخلاص وسیلۀ قرب نمی‌گردد بلکه موجب زیادتی بعد شود

<div class="b" id="bn1"><div class="m1"><p>ای بسا اعمال کان باشد وبال</p></div>
<div class="m2"><p>گفت پیغمبر ازین رو رب مال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر عمل کان موجب کبر و ریاست</p></div>
<div class="m2"><p>در حقیقت آن عمل جرم و خطاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست بی اخلاص اعمالت قبول</p></div>
<div class="m2"><p>چون ریا شرک ست از قول رسول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نداری نور تأیید خدا</p></div>
<div class="m2"><p>کی توان کرد نیک از بد جدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صورتش بینی ز معنی غافلی</p></div>
<div class="m2"><p>زانکه در علم طریقت جاهلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر باید صاحب علم و عیان</p></div>
<div class="m2"><p>کو به نور کشف بیند هر نهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا که واقف سازدت از نیک و بد</p></div>
<div class="m2"><p>قول او باشد ز اعمالت سند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود ک ه ز ارشادش شوی ایمن ز دیو</p></div>
<div class="m2"><p>وارهد جانت مگر از مکر و ریو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در مقام قرب حق سازی وطن</p></div>
<div class="m2"><p>ایمن آبی در سفر از راهزن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با هوای نفس خود یکدم م ساز</p></div>
<div class="m2"><p>لاتکلنی بشنو از دریای راز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر به دست دشمنان گردی اسیر</p></div>
<div class="m2"><p>به که باشد نفس بد بر تو امیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با تو افعی گر درون جامه است</p></div>
<div class="m2"><p>بهتر از نفسی که او خودکامه است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با پلنگ و شیر گر آیی به جنگ</p></div>
<div class="m2"><p>بهتر از صلح است با نفس دو رنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیش عقرب به ز نفس بد فعال</p></div>
<div class="m2"><p>در پناه پیر سازش پایمال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پند ناصح گوش کن از جان و دل</p></div>
<div class="m2"><p>نفس را با آرزوی خود مهل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که شد مستغرق دریای راز</p></div>
<div class="m2"><p>مشکل ار آید دگر با خویش باز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرغ جان زین دام و دانه کن خلاص</p></div>
<div class="m2"><p>همچو روح اللّه در بزم خاص</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با گدایان کم نشین شاهی طلب</p></div>
<div class="m2"><p>غافلی بگذار و آگاهی طلب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این ده ویرانه با جغدان گذار</p></div>
<div class="m2"><p>کن به قاف قرب چون عنقا گذار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاهباز دست سلطانی ، چرا</p></div>
<div class="m2"><p>در جهان باشی چو بومان بینوا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو همای دولتی ای ممتحن</p></div>
<div class="m2"><p>چند جویی جیفه چون زاغ و زغن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از ملک چون هست قدر تو فزون</p></div>
<div class="m2"><p>پس چرا در دست شیطانی زبون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بلبل گلزار عالم چون تو نیست</p></div>
<div class="m2"><p>بینوا چون صعوه بودن بهر چیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون شوی بیدار زین خواب گران</p></div>
<div class="m2"><p>ز آ ه و واویلا چه سود ت آن زمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هان و هان این دم که هستت فرصتی</p></div>
<div class="m2"><p>جهدها کن تا بیابی دولتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر چه آن اینجا نیاوردی بدست</p></div>
<div class="m2"><p>تا نپنداری دلا آنجات هست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کشتگاه آخرت دنیاست هان</p></div>
<div class="m2"><p>هر چه کاری ، بدروی آخر همان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این دو روزه عمر را فرصت شمار</p></div>
<div class="m2"><p>هان مشو از کار غافل زینهار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چونکه فرصت هست بنشان بیدرنگ</p></div>
<div class="m2"><p>آن نهال میوه های رنگ رنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خار بن را از زمین دل بکن</p></div>
<div class="m2"><p>در عوض بنشان ریاحین و سمن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هان درخت خار منشان در زمین</p></div>
<div class="m2"><p>تا نگردی تو پشیمان و حزین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر چه می کاری جو و گندم بکار</p></div>
<div class="m2"><p>تلخ دانه چون نمی آید به کار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برکن از بیخ و ز بن خار و تلو</p></div>
<div class="m2"><p>تاکه خار و خس نگیرد در گلو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>این دمست آن وقت تخم انداختن</p></div>
<div class="m2"><p>کارهای روز حاجت خواستن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زانکه دنیا مزرع عقبی بود</p></div>
<div class="m2"><p>کشت کن تا بهره ات آنجا بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر چه کشتی جنس آن خواهی درود</p></div>
<div class="m2"><p>نیک و بد آنجا عیان خواهدنمود</p></div></div>