---
title: >-
    بخش ۶۳ - در بیان آنکه انسان هر چه از نیک و بد می‌کند صورت همه در عالم معنی به وی بازگشت خواهد نمود. و تحریض بر آنکه نیکی دربارۀ خلایق موجب رضای خالق است.
---
# بخش ۶۳ - در بیان آنکه انسان هر چه از نیک و بد می‌کند صورت همه در عالم معنی به وی بازگشت خواهد نمود. و تحریض بر آنکه نیکی دربارۀ خلایق موجب رضای خالق است.

<div class="b" id="bn1"><div class="m1"><p>از خدا تبلی السر ائر می شنو</p></div>
<div class="m2"><p>روز و شب جز در پی نیکی مرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که نیکی می کند با خاص و عام</p></div>
<div class="m2"><p>اوست از قول نبی خیر الانام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بد مباش و بد مگوی و بد مکن</p></div>
<div class="m2"><p>چون چنین کردی تویی مقصود کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانکه بد کردار باشد در جهان</p></div>
<div class="m2"><p>اوست شر الناس این نیکو بدان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرصت نیکی غنیمت می شمر</p></div>
<div class="m2"><p>با همه نیکی کن از بد درگذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که نیکی کرد هرگز بد ندید</p></div>
<div class="m2"><p>و ر بدی کس را ن گ ویی کی شنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت الخلق عیال اللّه نبی</p></div>
<div class="m2"><p>با عیالش کی کند بد جز غنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهترین خلق نزد حق پسند</p></div>
<div class="m2"><p>آن بود کو با هم ه نیکی کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار مردان چیست احسان و کرم</p></div>
<div class="m2"><p>بردباری دان و بخشیدن درم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پند گفتن بار غم برداشتن</p></div>
<div class="m2"><p>تلخ و ترشی را شکر انگاشتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راحت خلقان طلب در رنج خویش</p></div>
<div class="m2"><p>تا توانی شد ز خلقان پیش و بیش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر همی خواهی قبول خاص و عام</p></div>
<div class="m2"><p>پیشۀ خود کن تواضع والسلام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خویشتن را از همه کمتر شمار</p></div>
<div class="m2"><p>سر ز جیب فقر و درویشی برآر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نخوت و کبر و ریا را دور دار</p></div>
<div class="m2"><p>جان به عجز و مسکنت مسرور دار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باش همچون خاک ره خوار و ذلیل</p></div>
<div class="m2"><p>زیر پای هر ضعیف و هر جلیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پاس دلها دار و آزادی بکن</p></div>
<div class="m2"><p>شاخ بیرحمی بکن از بیخ و بن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیشه کن عجز و نیاز و افتقار</p></div>
<div class="m2"><p>در طریق فقر جان را کن نثار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صبر کن در محنت و رنج و بلا</p></div>
<div class="m2"><p>جان فدای عشق جانان کن هلا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از تنعم بگذر و عیش و نشاط</p></div>
<div class="m2"><p>هان مکن با اهل غفلت اختلاط</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یاد حق کن مونس جان و روان</p></div>
<div class="m2"><p>دل مبرا ساز از فکر جهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کار خود یکبارگی با حق گذار</p></div>
<div class="m2"><p>هستی خود در میان کلی میار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زاد این ره چیست قول با عمل</p></div>
<div class="m2"><p>گفت بیکردار را نبود محل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر تو خواهی رفت راه ذوالمنن</p></div>
<div class="m2"><p>دست در فتراک رهبینان بزن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن سلاطین اقالیم یقین</p></div>
<div class="m2"><p>حاکمان کشور دنیا و دین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن جماعت کز خودی وارسته اند</p></div>
<div class="m2"><p>در مقام بیخودی پیوسته اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فارغ از خود گشته و باقی به دوست</p></div>
<div class="m2"><p>جملگی مغز آمده فارغ ز پوست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مقصد و مقصود ایجاد جهان</p></div>
<div class="m2"><p>محرمان بزم وصل دوستان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مقتدا و رهنمای انس و جان</p></div>
<div class="m2"><p>آمده لولاک اندر شأن آن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر قبول خاطر ایشان شوی</p></div>
<div class="m2"><p>شد مسلم بر تو ملک معنوی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر نظر بر حال زارت افکنند</p></div>
<div class="m2"><p>از بلا و محنتت ایمن کنند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در تو گر از عین رحمت بنگرند</p></div>
<div class="m2"><p>زودت از اسفل به اعلی افکنند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بردبارند و رحیم و مشفقند</p></div>
<div class="m2"><p>در اخوت حقشناس مطلقند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یک دعایی گر کند شیخ شفیق</p></div>
<div class="m2"><p>بهتر از صد ساله طاعت ای رفیق</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دشمنی شان هست عین دوستی</p></div>
<div class="m2"><p>مغز گردو در گذار از پوستی</p></div></div>