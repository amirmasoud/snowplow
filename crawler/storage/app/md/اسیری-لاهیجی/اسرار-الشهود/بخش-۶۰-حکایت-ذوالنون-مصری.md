---
title: >-
    بخش ۶۰ - حکایت ذوالنون مصری
---
# بخش ۶۰ - حکایت ذوالنون مصری

<div class="b" id="bn1"><div class="m1"><p>شیخ ذوالنون مقتدای خاص و عام</p></div>
<div class="m2"><p>آن انیس حضرت رب الانام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه او از جان و دل آگاه بود</p></div>
<div class="m2"><p>رهنمای رهروان راه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت یک روزی در ایام سفر</p></div>
<div class="m2"><p>می شدم در بادیه بی پا و سر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه و صحرا جملگی پر برف بود</p></div>
<div class="m2"><p>هیچ جا روی زمین خالی نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدم آنجا در میان ابر و میغ</p></div>
<div class="m2"><p>ارزنی می ریخت گبری بی دریغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش ای گبر بر گو قول راست</p></div>
<div class="m2"><p>دانۀ بی دام پاشیدن چراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت مرغان در سواد ابترند</p></div>
<div class="m2"><p>می نیابند دانه و بس مضطرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر آن می پاشم این ارزن که تا</p></div>
<div class="m2"><p>سیر گردند مرغکان بینوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حق مگر زین رو به من رحمت کند</p></div>
<div class="m2"><p>در قیامت این بود ما را سند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتمش از دین چرا بیگانه ای</p></div>
<div class="m2"><p>کی قبول افتد که پاشی دانه ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت باری گر قبول دوست نیست</p></div>
<div class="m2"><p>داند و بیند که آخر بهر کیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس بود این بینوا را خود همین</p></div>
<div class="m2"><p>کو همی گوید که بهر کیست این</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت ذوالنون وقت من خوش شد از آن</p></div>
<div class="m2"><p>در طواف کعبه هر سو ی ی دوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیدم آنجا در طواف آن گبر را</p></div>
<div class="m2"><p>عاشق و زار و نزار و بینوا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت دیدی عاقبت ای شیخ دین</p></div>
<div class="m2"><p>کانچه می کشتم بر آمد اینچنین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حق به روی من در ایمان گشاد</p></div>
<div class="m2"><p>پس مرا در خانۀ خود بار داد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر چه بهر او کنی باشد قبول</p></div>
<div class="m2"><p>انما الاعمال بشنو از رسول</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از در لطفش کسی نومید ن ی ست</p></div>
<div class="m2"><p>بر همه لاتقنطوا چون حجتی است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت ذوالنون وقت من خوش شد از آن</p></div>
<div class="m2"><p>گفتم ای دانندۀ فاش و نهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گبر چل ساله به مشت ارزنی</p></div>
<div class="m2"><p>از کمال مرحمت مؤمن کنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از همه بیگانگی پردازیش</p></div>
<div class="m2"><p>آشنای رحمت خود سازیش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر کرا حق خواند بی علت بخواند</p></div>
<div class="m2"><p>وانکه را هم راند بی علت براند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رو مکن با خود قیاس کار او</p></div>
<div class="m2"><p>فعل او بی علت است علت مجو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس تو ای ذوالنون برو فارغ نشین</p></div>
<div class="m2"><p>فعل او معلول با علت مبین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لاابالی دان جناب کبریا</p></div>
<div class="m2"><p>نیست علت لایق فعل خدا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از خیال و عقل و فهم این برترست</p></div>
<div class="m2"><p>اندرین ره بوعلی کور و کرست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باد استغنا وزیدن چون گرفت</p></div>
<div class="m2"><p>نیست سوزش کز پی افسون گرفت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>واگذار این کار خود را با خدا</p></div>
<div class="m2"><p>پیشۀ خود ساز تسلیم و رضا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر تو خواهی جان بری از دست غم</p></div>
<div class="m2"><p>غرقه کن خود را به دریای عدم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حظ نفس خود مجو در راه دوست</p></div>
<div class="m2"><p>مرد خودبین کی شود آگاه دوست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر که راه عشق جانان می رود</p></div>
<div class="m2"><p>ترک جان چون گفت آسان می رود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در طریقت هست چون هستی گناه</p></div>
<div class="m2"><p>نیست شو گر وصل خواهی وصل شاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زاد این ره نیست جز محو و فنا</p></div>
<div class="m2"><p>اندرین ره توشه گر داری درآ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نفی خود کن آنگهی اثبات حق</p></div>
<div class="m2"><p>بعد لااله بین آیات حق</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خویش را ایثار راه یار کن</p></div>
<div class="m2"><p>جان خود از وصل برخوردار کن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو برون شو تا که یار آید درون</p></div>
<div class="m2"><p>وصل بیچون را مجو در چند و چون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از حجاب خویش خود را وارهان</p></div>
<div class="m2"><p>جانفشان شو جانفشان شو جانفشان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برفشان از هر دو عالم آستین</p></div>
<div class="m2"><p>در مقام وحدت آ ایمن نشین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر روی راه خدا بیخود برو</p></div>
<div class="m2"><p>دوست خواهی از خودی بیگانه شو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اصل طاعتهاست محو و نیستی</p></div>
<div class="m2"><p>تا تو هستی کی شناسی نیستی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وارهان خود را ز قید خویشتن</p></div>
<div class="m2"><p>مست وصلش کرد بر کونین تن</p></div></div>