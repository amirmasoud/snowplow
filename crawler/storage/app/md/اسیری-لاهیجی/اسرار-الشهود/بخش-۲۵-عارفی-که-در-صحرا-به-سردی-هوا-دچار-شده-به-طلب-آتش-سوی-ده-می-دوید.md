---
title: >-
    بخش ۲۵ - عارفی که در صحرا به سردی هوا دچار شده به طلب آتش سوی ده می‌دوید
---
# بخش ۲۵ - عارفی که در صحرا به سردی هوا دچار شده به طلب آتش سوی ده می‌دوید

<div class="b" id="bn1"><div class="m1"><p>عارفی می رفت یک روزی به راه</p></div>
<div class="m2"><p>بود صحرا و نبود آنجا پناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابر پیدا گشت و باریدن گرفت</p></div>
<div class="m2"><p>جامه اش تر گشت و چاهیدن گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می دوید از دست باران آن چنان</p></div>
<div class="m2"><p>که تو گویی کرد دشمن قصد جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون در آن صحرا از آن سرما گذشت</p></div>
<div class="m2"><p>یک ده ویران بدید آن سوی دشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او ز هول جان به سوی ده شتافت</p></div>
<div class="m2"><p>تا تواند او ز سرما چاره یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون رسید آنجا به گرد ده دوید</p></div>
<div class="m2"><p>عاقبت یک خانۀ معمور دید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر در خانه رسید آواز داد</p></div>
<div class="m2"><p>صاحب خانه جوابش باز داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در زمان آمد بر او کردش سلام</p></div>
<div class="m2"><p>عارفش گفتا علیکم والسلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس تواضع کرد او با میهمان</p></div>
<div class="m2"><p>اندرون خانه بردش در زمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز پرسید از کجاها می رسی</p></div>
<div class="m2"><p>کرد از احوال او پرسش بسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت سرما خورده ام آتش بیار</p></div>
<div class="m2"><p>نیست پروای سخن معذور دار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوییا در خانه اش آتش نبود</p></div>
<div class="m2"><p>رفت تا بستاند از همسایه زود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بستد آتش را سوی خانه شتافت</p></div>
<div class="m2"><p>خرقه دید آنجا و مهمان را نیافت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در تعجب ماند از آن حال غریب</p></div>
<div class="m2"><p>پیش او آمد خیالات عجیب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آتشی افروخت تا بیند که چیست</p></div>
<div class="m2"><p>آن مگر جن بود یا نه خود پری است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لحظه ای شد خرقه جنبیدن گرفت</p></div>
<div class="m2"><p>میهمان در خرقه لرزیدن گرفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آمد و در پیش آتش خوش نشست</p></div>
<div class="m2"><p>صاحب خانه ز حیرت لب ببست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر زمان نوعی خیالش آمدی</p></div>
<div class="m2"><p>دم به دم زان حال حیران‌تر شدی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عاقبت پرسید او از میهمان</p></div>
<div class="m2"><p>هر کجا بودی مدار از من نهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زانکه حیرانم درین کار عجب</p></div>
<div class="m2"><p>واقفم گردان ز اسرار عجب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت مهمانش که ما را سرد بود</p></div>
<div class="m2"><p>از غم سرما دلم پر درد بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چونکه تو دیر آمدی گفتم روم</p></div>
<div class="m2"><p>تا که گرم از آتش دوزخ شوم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بهر آتش زود در دوزخ شدم</p></div>
<div class="m2"><p>هر طرف جویندۀ آتش بدم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هفت دوزخ گشتم و آتش نبود</p></div>
<div class="m2"><p>من نه آتش دیدم و نی نیز دود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در عجب ماندم که آن آتش کجاست</p></div>
<div class="m2"><p>دوزخ سوزان ز آتش چون جداست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عاقبت با مالک دوزخ عیان</p></div>
<div class="m2"><p>گفتم از آتش بده ما را نشان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سوی آتش بهر حق شو رهبرم</p></div>
<div class="m2"><p>تا مگر از دست سرما جان برم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت مالک نیست اینجا آتشی</p></div>
<div class="m2"><p>تو مگر دیوانه ای یا سرخوشی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفتمش دیوانه و سرخوش نیم</p></div>
<div class="m2"><p>گو خبر ز آتش که جویای ویم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر نشان دوزخ اینجا آمدم</p></div>
<div class="m2"><p>من ندیدم آتش و حیران شدم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>انبیا دادند از دوزخ نشان</p></div>
<div class="m2"><p>زآتش سوزان به خلقان جهان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن نشان انبیا از کذب نیست</p></div>
<div class="m2"><p>مشکلم حل کن بگو احوال چیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفت آری آن نشانها راست است</p></div>
<div class="m2"><p>تو یقین میدان که شک برخاسته است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نیست اینجا آتشی بشنو ز من</p></div>
<div class="m2"><p>هر کسی آرد خود آ ن با خویشتن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن ی ک ی از آتش شهوت بسوخت</p></div>
<div class="m2"><p>وان یکی از کینه آتش برفروخت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آتش هر یک بود نوعی دگر</p></div>
<div class="m2"><p>فهم کن او را که تا یابی خبر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آتش دوزخ بود کز خشم تست</p></div>
<div class="m2"><p>با تو گفتم من سخنهای درست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هفت دوزخ چیست اخلاق بدت</p></div>
<div class="m2"><p>هشت جنت هست اعمال خودت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زینهار ای جان من صد زینهار</p></div>
<div class="m2"><p>نیک کن پیوسته دست از بد بدار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زانکه هر چه اینجا کنی از نیک و بد</p></div>
<div class="m2"><p>مونست خواهدشدن اندر لحد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آن مشقتهای جمله انبیا</p></div>
<div class="m2"><p>وان ریاضتهای جمله اولیا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کی عبث باشد بگو ای بیخبر</p></div>
<div class="m2"><p>دیده گر داری در آن حکمت نگر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آنچه گفتم هست از عین الیقین</p></div>
<div class="m2"><p>نی به استدلال و تقلید است این</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>راست دان و راست گوی و راست بین</p></div>
<div class="m2"><p>راستی کن کج مرو در راه دین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>حشر تو بر صورت اعمال تست</p></div>
<div class="m2"><p>هر چه دیدی نیک و بد احوال تست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هر چه می بینی هم از خود دیده ای</p></div>
<div class="m2"><p>گر جزای نیک و گر بد دیده ای</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مرغ معنی صورت همت شناس</p></div>
<div class="m2"><p>همت آمد کار دینت را اساس</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فکر دنیایی است م رغ خانگی</p></div>
<div class="m2"><p>فکر شهوانی خروس است بی شکی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هست بلبل عشق و رندی و سماع</p></div>
<div class="m2"><p>شد هما فکر قناعت و انقطاع</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>باز آمد دعوت قابل به راه</p></div>
<div class="m2"><p>چرغ و شاهین است قرب پادشاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فکر سرداری بود دال عقاب</p></div>
<div class="m2"><p>هدهد ارسال رسل بهر خطاب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خودنمایی بود طاوس ای پسر</p></div>
<div class="m2"><p>کرکس و زاغ است دنیا سربسر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>قاز چبود فکرهای شست و شو</p></div>
<div class="m2"><p>فاخته طاعات و ذکر دل بگو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بط چه باشد حرص دنیای دنی</p></div>
<div class="m2"><p>جوجه باشد حال دنیای غنی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در قناعت گشت آن موسیچه فاش</p></div>
<div class="m2"><p>هست تیهو حیلتی اندر معاش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خود کبوتر چیست ای دانای کل</p></div>
<div class="m2"><p>ذکر دل گهگاه ارسال رسل</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هست قمری صورت اطوار دل</p></div>
<div class="m2"><p>گوش کن از عارفان اسرار دل</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کوف آمد ذکر صهو و انزوا</p></div>
<div class="m2"><p>ساز تعلیم علوم انبیا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بوم استبعاد شد از اولیا</p></div>
<div class="m2"><p>صورت تقلید دان خفاش را</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بعد از آن توحید بوتیماردان</p></div>
<div class="m2"><p>مرغ لک لک را حصول مال خوان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خود شتر مرغ است تدبیر خطا</p></div>
<div class="m2"><p>مرغ آبی چیست پاکی نفس را</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>صرف همت در فنا عنقا شناس</p></div>
<div class="m2"><p>با فنا سیمرغ را میکن قیاس</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>رب ارباب است عنقای بقا</p></div>
<div class="m2"><p>منطق الطیر است این اسرارها</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>عارف اسرار مرغان گر شوی</p></div>
<div class="m2"><p>مرغ معنی را به جان چاکر شوی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>من چه گویم شرخ عالم های دل</p></div>
<div class="m2"><p>با کسی کو را فروشد پا بگل</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>محرم اسرار دل اهل دلست</p></div>
<div class="m2"><p>هر که نبود اهل دل ناقابلست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دل چه باشد مخزن گنج یقین</p></div>
<div class="m2"><p>اهل دل دان عارف اسرار دین</p></div></div>