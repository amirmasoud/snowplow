---
title: >-
    بخش ۶۵ - حکایت ابو تراب نخشبی
---
# بخش ۶۵ - حکایت ابو تراب نخشبی

<div class="b" id="bn1"><div class="m1"><p>عارف حق بوتراب نخشبی</p></div>
<div class="m2"><p>آنکه بود او عارف علم نبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت اکنون هست بیشک هشت سال</p></div>
<div class="m2"><p>تا که از فضل خدای ذوالجلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود ندادم هیچ چیزی من به کس</p></div>
<div class="m2"><p>نی ز کس چیزی گرفتم یک نفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن یکی گفتش چگونه باشد این</p></div>
<div class="m2"><p>شرح این را بازگو ای شیخ دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فهم این معنی به غایت مشکلست</p></div>
<div class="m2"><p>حل این از هر کسی کی حاصلست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داد پاسخ شیخ عالم بوتراب</p></div>
<div class="m2"><p>سائلان را از ره صدق و صواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم جانم چونکه می باشد به دوست</p></div>
<div class="m2"><p>هر چه می بینم به عالم جمله اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقش غیر از لوح جانم شسته شد</p></div>
<div class="m2"><p>دل به دلبر مونس و پیوست ه شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من ندیدم غیر جانان در جهان</p></div>
<div class="m2"><p>در حقیقت اوست پیدا و نهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه گفتم بود با وی گفت و گو</p></div>
<div class="m2"><p>وانچه بشنیدم ش نید م من از او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر چه بگرفتم گرفتم هم از او</p></div>
<div class="m2"><p>وانچه دادم هم ندادم جز بدو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اینچنین دیدند بینایان راه</p></div>
<div class="m2"><p>حبذا چشمی که بیند روی شاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دیده ای کو نور رخسارش ندید</p></div>
<div class="m2"><p>او ندارد بهره زین گفت و شنی د</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیده را روشن کن از نور صفا</p></div>
<div class="m2"><p>وانگهی بنگر جمال آن لقا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چشم بینایی بباید مر ترا</p></div>
<div class="m2"><p>تا کنی باور سخنهای مرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کور مادرزاد کی باور کند</p></div>
<div class="m2"><p>گر همی گویی به الوان صد سند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دیدۀ بینا دل دانا بیار</p></div>
<div class="m2"><p>تا ز روی حق نگردی شرمسار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آفتابت ماند پنهان زیر میغ</p></div>
<div class="m2"><p>پرتو روشن ندیدی صد دریغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر ترا دیده بدی در راه دوست</p></div>
<div class="m2"><p>هر چه می بینی عیان دیدی که اوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر تو خود را دور میدانی بیا</p></div>
<div class="m2"><p>بشنو آخر نحن اقرب از خدا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از خدا انی قریب گوش کن</p></div>
<div class="m2"><p>حق محیط جمله آمد بی سخن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دیده باید تا ببیند آفتاب</p></div>
<div class="m2"><p>دیدۀ بینا نه زان چشم خراب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر بدانی نسبت ما را به دوست</p></div>
<div class="m2"><p>آن زمان بینی که هستی جمله اوست</p></div></div>