---
title: >-
    بخش ۳۶ - جنید بغدادی
---
# بخش ۳۶ - جنید بغدادی

<div class="b" id="bn1"><div class="m1"><p>پیر بغدادی جنید آن رهنما</p></div>
<div class="m2"><p>چونکه شد اندر طریقت پیشوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کسی کردند آغ ا ز حسد</p></div>
<div class="m2"><p>گفتن او را پیشوایی کی رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد کمال ار هست پوشاند حسد</p></div>
<div class="m2"><p>بحر قلزم را بجوشاند حسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانع جمله کمال آم د حسد</p></div>
<div class="m2"><p>خلق عالم را وبال آمد حسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت پیغمبر حسد ایمان برد</p></div>
<div class="m2"><p>همچو آن آتش که هیزم را خورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از حسد بگذر درآ در راه دین</p></div>
<div class="m2"><p>گر همی خواهی شوی آگاه دین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خلیفه عاقب ت آ ن حاسدان</p></div>
<div class="m2"><p>عرض کردند حال آ ن شیخ زمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کو ه می گوید ح کایات ع ج ب</p></div>
<div class="m2"><p>می کنند از وی روایات عجب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین سخن در فتنه می افتند خلق</p></div>
<div class="m2"><p>مبتلای بدع تی گردند خلق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت با ایشان خلیفه در جواب</p></div>
<div class="m2"><p>منع او بی حجتی نبود صواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زانکه بی حج ت چو کردم منع او</p></div>
<div class="m2"><p>در میان خلق افتد گفت و گو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فتنه دیگر ا ز آن پیدا شود</p></div>
<div class="m2"><p>کار او زین بیشتر بالا شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می بباید آزمایش کرد زود</p></div>
<div class="m2"><p>تا به حجت منع او بتوان نمود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن خلیفه داشت یک زیبا کنیز</p></div>
<div class="m2"><p>بود در پیش خلیفه بس عزیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در جمال ودر ملاحت دلپذیر</p></div>
<div class="m2"><p>در همه عالم به خوبی بی نظیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بد خلیف ه عاشق روی نکوش</p></div>
<div class="m2"><p>دایماً آشفت ۀ آن رنگ و بوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت تا پوشد لباس فاخرش</p></div>
<div class="m2"><p>زود آرایند هر گون زیورش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرد رویش بسته درهای ثمین</p></div>
<div class="m2"><p>دست و پایش پر ز خلخال گزین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یک کنیز دیگرش همراه کرد</p></div>
<div class="m2"><p>تا از آن دریا برانگیزند گرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در ف ل ان جا گفت رو ای خوبرو</p></div>
<div class="m2"><p>روبرو شو با ج نید آخر بگو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کامدم پیش تو ای شیخ انام</p></div>
<div class="m2"><p>از سر صدق و ز اخلاص تمام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زانکه دل بگرفتم از کار جهان</p></div>
<div class="m2"><p>نیست ما را طاق ت بار گران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هست ما را مال بیحد و شمار</p></div>
<div class="m2"><p>وین دلم با کس نمی گیرد قرار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیش تو از بهر این کار آمدم</p></div>
<div class="m2"><p>تا بگویم پیشت احوال ندم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا بیندشی صلاح کار ما</p></div>
<div class="m2"><p>زانکه هستی تو امام و رهنما</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر بخواهی تو م را ای پیشوا</p></div>
<div class="m2"><p>مال خود سازم همه پیشت فدا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رو به طاعت آورم در صحبتت</p></div>
<div class="m2"><p>چون کنیزان باشم اندر خدمتت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اندرین معنی نما سعی بلیغ</p></div>
<div class="m2"><p>روی خود بنما چو خور در زیر میغ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رو گشاده خویش بر وی عرضه کن</p></div>
<div class="m2"><p>تا مگر بفریبد او را این سخن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آمد آن مهرو روان پیش جنید</p></div>
<div class="m2"><p>تا مگر ساز د ز رعنا پیش صید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنچه تعلیمش نمود اندر نهفت</p></div>
<div class="m2"><p>او دو صد چندان همه با شیخ گفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یک نظر بر روی آ ن زیبا نگار</p></div>
<div class="m2"><p>اوفتاد آن شیخ را بی اختیار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سر به پیش افکند شیخ اوستاد</p></div>
<div class="m2"><p>گشت خاموش و جواب زن نداد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لحظه ای شد سر برآورد آن زمان</p></div>
<div class="m2"><p>کرد آهی دردناک از سوز جان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آه را چون در رخ آن زن دمید</p></div>
<div class="m2"><p>در خسوف افتاد و جان از وی پرید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر نیامد زو نفس در حال مرد</p></div>
<div class="m2"><p>بر سر یک امتحان جان را سپرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>امتحان او لیا هر کو کند</p></div>
<div class="m2"><p>خویش را بر تیغ فولادی زند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>این جماعت را که بی ما و منند</p></div>
<div class="m2"><p>امتحان کم کن که بی جانت کنند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خادمه شد با دل اندوهگین</p></div>
<div class="m2"><p>با خلیفه گفت حا ل ش را چنین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شد خلیفه بیقرار از درد و غم</p></div>
<div class="m2"><p>آتشی افتاد در وی از ندم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گفت هر نادان که با اهل دلان</p></div>
<div class="m2"><p>آن کند که می نباید کرد آن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>این ببیند که نباید دیدنش</p></div>
<div class="m2"><p>زین گلستان این بود گل چیدنش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پس خلیفه گفت مرد اینچنین</p></div>
<div class="m2"><p>پیش خود نتوان طلب کردن یقین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در زمان برخاست شد پیش جنید</p></div>
<div class="m2"><p>گفت کای لطف خدا را گشته صید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون دلت می داد کآخر آن چنان</p></div>
<div class="m2"><p>زار سوزی ماه رویی همچو جان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گفت شیخش کای امیر المؤمنین</p></div>
<div class="m2"><p>رحم تو بر مؤمنان آمد چنین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خواستی چل ساله طاع ا ت مرا</p></div>
<div class="m2"><p>این سلوک و این ریاضات مرا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این همه بیخوابی و جان کندنم</p></div>
<div class="m2"><p>در طلب پیوسته خونها خوردنم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا دهی بر باد جوجو خرمنم</p></div>
<div class="m2"><p>من کیم تا در میان گویم منم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فعل حق دان هر چه کردند اولیا</p></div>
<div class="m2"><p>زانکه در حق گشته اند ایشان فنا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در میا با اولیا اندر نبرد</p></div>
<div class="m2"><p>چون چنین کردی چنین خواهند کرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>صدق پیش آور که تا بینی عیان</p></div>
<div class="m2"><p>آنچه دادند اولیا از وی نشان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>امتحان شیخ دین گر می کنی</p></div>
<div class="m2"><p>دست حیرت بسکه بر سر می زند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در حقیقت امتحان اهل حق</p></div>
<div class="m2"><p>امتحان حق بود بی هیچ دق</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گر نداری صدق و اخلاص و یقین</p></div>
<div class="m2"><p>در ره مردان مر و جایی نشین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر به پیشت فعل ایشان بد نمود</p></div>
<div class="m2"><p>آن ز جهل تست ای مرد عنود</p></div></div>