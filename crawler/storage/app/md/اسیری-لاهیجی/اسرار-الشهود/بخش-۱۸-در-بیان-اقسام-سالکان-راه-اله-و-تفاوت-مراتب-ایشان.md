---
title: >-
    بخش ۱۸ - در بیان اقسام سالکان راه اله و تفاوت مراتب ایشان
---
# بخش ۱۸ - در بیان اقسام سالکان راه اله و تفاوت مراتب ایشان

<div class="b" id="bn1"><div class="m1"><p>چار قسم اند سالکان راه دین</p></div>
<div class="m2"><p>حال هر یک را زمن بشنو یقین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اولین مجذوب سالک آمدست</p></div>
<div class="m2"><p>کاول از جذبه به حق واصل شدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حق فرستادش به سوی خلق زود</p></div>
<div class="m2"><p>تا که خلقان جهان را ره نمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با همه قربی که دارد با خدا</p></div>
<div class="m2"><p>از ریاضت نیست یک ساعت جدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانکه هر کو مقتدای راه شد</p></div>
<div class="m2"><p>از بد و نیک مقام آگا ه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نباشد در عمل ثابت قدم</p></div>
<div class="m2"><p>چون رهاند خلق را از دست غم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقتدا چون در ریاضت قایمست</p></div>
<div class="m2"><p>تابعش را میل طاعت دایمست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زانکه باشد تابع اعمال پیر</p></div>
<div class="m2"><p>هر مرید صادق از صدق ضمیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیگر آنکه شأن حق بیغایت است</p></div>
<div class="m2"><p>هر زمانش نوع دیگر آیت است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چونکه معروف است بیحد لاجرم</p></div>
<div class="m2"><p>معرفت بیغایت آید نیز هم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمرها گر او ریاضت می کند ‌</p></div>
<div class="m2"><p>روز و شب را صرف طاعت می کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دم بدم بیند جمال دیگر او</p></div>
<div class="m2"><p>لاجرم دایم بود در جست و جو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر دو صد سال ا ندرین ره می رود</p></div>
<div class="m2"><p>هر دم از هر نوع حیران می شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در نماز از بس که بر پا ایستاد</p></div>
<div class="m2"><p>عاقبت در پاش آماس اوفتاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حال پیغمبر نگر با آن کمال</p></div>
<div class="m2"><p>فاستقم بودش خطاب از ذوالجلال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سورۀ طه بدان نازل شدست</p></div>
<div class="m2"><p>غیرت خلق جهان این آمدست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رهنمایی لایق آن کاملست</p></div>
<div class="m2"><p>کو ز خود فانی، به جانان واصلست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست اکمل در طریقت زو کسی</p></div>
<div class="m2"><p>جز خدا او را نباشد مونسی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سالها باید فلک بر سر رود</p></div>
<div class="m2"><p>تاکه پیری اینچنین پیدا شود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>و آن دوم را سالک مجذوب خوان</p></div>
<div class="m2"><p>کو سلوکی کرد و از هستی برست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در ریاضت در عبادت سالها</p></div>
<div class="m2"><p>کرد سعی و گشت قابل جذبه را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون دل او قاب ل انوار شد</p></div>
<div class="m2"><p>جان پاکش قابل اسرار شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شاهباز جذبه او را در ربود</p></div>
<div class="m2"><p>جان او شد محرم بزم شهود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در مقام وصل جانان راه یافت</p></div>
<div class="m2"><p>از خدا جان و دل آگاه یافت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اینچنین کامل بجو گر رهروی</p></div>
<div class="m2"><p>تا ز وصل دوست با بهره شوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس سیم مجذوب مطلق می شمر</p></div>
<div class="m2"><p>کو ز تاب نور حق شد بیخبر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دایماً حیران دیدار خداست</p></div>
<div class="m2"><p>از خیال عقل و دانشها جداست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از خودی بگذشت و واصل شد به دوست</p></div>
<div class="m2"><p>مست سرمد از می دیدار اوست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>او ز مستی گشت از خود بیخبر</p></div>
<div class="m2"><p>دیگران را چون شود او راهبر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>محتسب انکار ایشان گر کند</p></div>
<div class="m2"><p>غیرت حق در دمش بی سر کند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کشته اند این قوم بر خوان خدا</p></div>
<div class="m2"><p>کی بود انکار این مستان روا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رو به صدق دل بجو ز ایشان نظر</p></div>
<div class="m2"><p>منکر تابع مشو ای بیخبر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چارمینش سالک بی جذبه است</p></div>
<div class="m2"><p>کو سلوکی کرد و از هستی برست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>او به عقل خویش این ره می رود</p></div>
<div class="m2"><p>چون ندارد عشق کی واصل شود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون نشد حالش به کوی عشق پست</p></div>
<div class="m2"><p>از می هستی است او پیوسته مست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یا ندارد پیر تا پاکش کند</p></div>
<div class="m2"><p>یا نهان دارد از او احوال خود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در ارادت گر شدی او مستقیم</p></div>
<div class="m2"><p>با غم هجران کجا بودی ندیم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سد راه سالکان حق پرست</p></div>
<div class="m2"><p>در میان پرده خلق بد شدست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سالک بی جذبه چون واصل نشد</p></div>
<div class="m2"><p>در طریقت لاجرم کامل نشد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون نشد واصل نباشد رهنما</p></div>
<div class="m2"><p>زو مجو چیزی که هست او بینوا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>باش مهمان کر یم ان ای پسر</p></div>
<div class="m2"><p>با لئیمان کم نشین جان پدر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر چه جویی از محل خود بجوی</p></div>
<div class="m2"><p>با زمستان از گل و ریحان مگوی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اینچنین کس را اگر تابع شوی</p></div>
<div class="m2"><p>ره نیابی عاقبت گردی غوی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زین چهار آن هر دو کاول گفته شد</p></div>
<div class="m2"><p>مرشد راهند و این در سفته شد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زین دو کآخر شرح ایشان داده ام</p></div>
<div class="m2"><p>رهبری هرگز نیا ید بیش و کم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وین یکی از خودپرستی بینواست</p></div>
<div class="m2"><p>وان دگر از نور حق در خود فناست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>این یکی را مستی خود پرده است</p></div>
<div class="m2"><p>وان یکی خود را در او گم کرده است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این یکی از خود ره حق بسته است</p></div>
<div class="m2"><p>وان دگر از خود زحق وارسته است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زانکه او مجذوب مطلق ابترست</p></div>
<div class="m2"><p>صورت او زهر و معنی شکرست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>رهبر راه طریقت او بود</p></div>
<div class="m2"><p>کو به احکام شریعت می رود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سالک بی جذبه خود آگاه نیست</p></div>
<div class="m2"><p>واقف این منزل و این راه نیست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از ره و منزل چو واقف نیست او</p></div>
<div class="m2"><p>رهنمایی چون کند آخر بگو</p></div></div>