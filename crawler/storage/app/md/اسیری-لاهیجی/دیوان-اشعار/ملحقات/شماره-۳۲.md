---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>تا عشق ترا به جان و دل بگزیدیم</p></div>
<div class="m2"><p>مهر دو جهان ز جان و دل ببریدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد دو جهان بهای درد و غم تو</p></div>
<div class="m2"><p>دادیم و غم ترا به جان بخریدیم</p></div></div>