---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>ما از غم تو روی به صحرا کردیم</p></div>
<div class="m2"><p>واندر طلبت پشت به دنیا کردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رندانه ز هر دو کون آزاد شدیم</p></div>
<div class="m2"><p>وامق صفتی روی به عذرا کردیم</p></div></div>