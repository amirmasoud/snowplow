---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>ما دو عالم را همه لا دیده ایم</p></div>
<div class="m2"><p>لا چه باشد جمله الا دیده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در لباس جمله ذرات جهان</p></div>
<div class="m2"><p>مهر روی او هویدا دیده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور خورشید جمال او عیان</p></div>
<div class="m2"><p>از همه پنهان و پیدا دیده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که ره یابد ببزم وصل دوست</p></div>
<div class="m2"><p>هردمش عیدی مهنا دیده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن امانت کو نگنجد در جهان</p></div>
<div class="m2"><p>در دل عشاق مأوا دیده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانکه مستغنی ز جایست و جهات</p></div>
<div class="m2"><p>در دل هر ذره اش جا دیده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پس هر ذره از عین الیقین</p></div>
<div class="m2"><p>ای اسیری ما خدا را دیده ایم</p></div></div>