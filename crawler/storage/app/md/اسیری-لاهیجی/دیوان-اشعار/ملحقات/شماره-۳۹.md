---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>اسیری از سگان نوربخش است</p></div>
<div class="m2"><p>از آنش دردو عالم نوربخش است</p></div></div>