---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>دوش دیدم یار مست و جمله اغیار مست</p></div>
<div class="m2"><p>جام مست و باده مست و خانه خمار مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان ما مست و حریفان مست و ساقی مست مست</p></div>
<div class="m2"><p>بزم مست و شاهد و مطرب همه یکبارمست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل مست و عشق مست و عاشق و معشوق مست</p></div>
<div class="m2"><p>زهد مست و توبه مست و زاهد هشیار مست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کعبه و میخانه مست و مسجد و محراب مست</p></div>
<div class="m2"><p>سنگ و چوب و گل همه مست و در و دیوار مست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گبر و ترسا و کلیسا مست و عیسی بود مست</p></div>
<div class="m2"><p>دیر و ناقوس و صلیب و راهب و زنار مست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بت پرستان مست بودند و بت بتخانه مست</p></div>
<div class="m2"><p>کافر و انکار مست و مؤمن و اقرار مست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رند دردآشام مست و شیخ و مولا بودمست</p></div>
<div class="m2"><p>خرقه پوش شهرمست و جبه و دستار مست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علم و فتوی مست و مفتی مست و عالم بودمست</p></div>
<div class="m2"><p>شبلی و منصور مست و ریسمان و دارمست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محتسب مست و عسس هم مست و شحنه بود مست</p></div>
<div class="m2"><p>جمله اصناف مست و کوچه و بازار مست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صوفی ما مست و خلوت مست و ذوق و حال مست</p></div>
<div class="m2"><p>هم مرید و پیر مست و طالب دیدار مست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهل و عرفان مست و عارف مست و جاهل بود مست</p></div>
<div class="m2"><p>سالک اطوار مست و صاحب اسرار مست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاک وبادو آب و آتش جملگی بودند مست</p></div>
<div class="m2"><p>انجم و افلاک مست و کوکب سیار مست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عقل کل مست و ملایک مست و جسم و روح مست</p></div>
<div class="m2"><p>جبرئیل و وحی مست و احمدمختار مست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم اسیری مست بود و جمله ذرات مست</p></div>
<div class="m2"><p>کفر وایمان مست و دین و مذهب و دیندار مست</p></div></div>