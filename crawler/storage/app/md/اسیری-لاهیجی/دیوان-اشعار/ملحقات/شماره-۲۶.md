---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>ای عاشقان ای عاشقان مائیم عشق دلبران</p></div>
<div class="m2"><p>چون عشق سودی بی زیان کاری نباشد در جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاری نهانی شد عیان در صورت جان و جهان</p></div>
<div class="m2"><p>از غیر او نام و نشان کو در همه کون و مکان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق آمد و در دل نشست راه برون شد راببست</p></div>
<div class="m2"><p>گفتم بگوی ای خودپرست چون یابی از دستم امان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که ای سلطان من غایب مشو از جان من</p></div>
<div class="m2"><p>اینست خود درمان من میباش دایم میهمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام شراب آتشین داد و بگفتا نوش این</p></div>
<div class="m2"><p>مستی مکن ای بی یقین هشیار باش و کاردان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نوش کردم آتشی افتاد در جانم خوشی</p></div>
<div class="m2"><p>در پیش آن عاشق کشی دیدم فنای جاودان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون زان فنا دیدم بقا دیگر ندیدم جز خدا</p></div>
<div class="m2"><p>ما از کجا غیر از کجا مایی در اویی شد نهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایمان عیان از روی او کفر آشکار از موی او</p></div>
<div class="m2"><p>جمله بجست و جوی او در کعبه و دیر مغان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می بین اسیری روبرو حسنش ز هر روی نکو</p></div>
<div class="m2"><p>زیرا که نبود غیر او اندر نهان و در عیان</p></div></div>