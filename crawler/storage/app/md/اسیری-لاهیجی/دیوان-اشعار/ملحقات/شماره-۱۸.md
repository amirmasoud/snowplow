---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>من که خورشید جمال تو عیان می بینم</p></div>
<div class="m2"><p>عکس روی تو ز مرآت جهان می بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم آن رند که دایم ز خرابات جهان</p></div>
<div class="m2"><p>شاهد حسن ترا جلوه کنان می بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر ذاتت که ز نورش دو جهان پیدا شد</p></div>
<div class="m2"><p>در پس پرده هر ذره نهان می بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا که گشتم بره عشق تو بی نام و نشان</p></div>
<div class="m2"><p>در همه کون و مکان از تو نشان می بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تجلی جمال تو دلم پرنور است</p></div>
<div class="m2"><p>پرتو روی تو از دیده جان می بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راست ناید بصفت حسن تو گر شرح دهم</p></div>
<div class="m2"><p>جلوه روی تو بیرون ز بیان می بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده اهل نظر از رخ خوبان جهان</p></div>
<div class="m2"><p>چون اسیری بجمالت نگران می بینم</p></div></div>