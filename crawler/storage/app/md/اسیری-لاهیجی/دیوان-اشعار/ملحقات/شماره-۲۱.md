---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ساقی شراب عشق ده تا از خرد یکسو شوم</p></div>
<div class="m2"><p>مست و خرابم کن چنان کز ما برآیم هو شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شاهد مه روی ما در ده می جام فنا</p></div>
<div class="m2"><p>تا از خمار ما و من یابم امان و او شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقت است تا چون عاشقان دست از خودی کوته کنم</p></div>
<div class="m2"><p>پا در ره عشقش نهم با دوست همزانو شوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گلخن طبع و هوا همچون ملک دوری کنم</p></div>
<div class="m2"><p>در گلشن ذات و صفت مانند گل خوش بو شوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر میان ما و تو مایی ما آمد حجاب</p></div>
<div class="m2"><p>ایکاش برخیزد منی تا با تو روبررو شوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوی خوش عشاق تو جانبازی است و نیستی</p></div>
<div class="m2"><p>هستی چو محو عشق شد با عاشقان همخو شوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مایی اسیری غرق شد در موج دریای قدم</p></div>
<div class="m2"><p>بحرم بمعنی این زمان در صورت ارچه جو شوم</p></div></div>