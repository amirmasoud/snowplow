---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>آئینه خدا بخدا مرتضی علیست</p></div>
<div class="m2"><p>گنج بقا و نور لقا مرتضی علیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکل گشا بقول سلونی ولو کشف</p></div>
<div class="m2"><p>معجزنما بروز وغا مرتضی علیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صاحب لوای منزلت قربت و وصال</p></div>
<div class="m2"><p>مسندنشین ملک دنا مرتضی علیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید آسمان ولایت ولی حق</p></div>
<div class="m2"><p>خیرالوری و نور هدی مرتضی علیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واقف ز سر لم یزلی از ره عیان</p></div>
<div class="m2"><p>عارف بعلم کشف و صفا مرتضی علیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریای در و معرفت و گوهر یقین</p></div>
<div class="m2"><p>کوه وقار وجود و سخا مرتضی علیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن نشاة که ختم ولایت بود بر او</p></div>
<div class="m2"><p>چون گویمت قبول نما مرتضی علیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن سر دایری که بهر دور ظاهر است</p></div>
<div class="m2"><p>چشم بصیرتت بگشا مرتضی علیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن کاملی که گفت انااللوح و القلم</p></div>
<div class="m2"><p>هم عرش و هم زمین و سما مرتضی علیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن کو کلام ناطق و سری است کس مدان</p></div>
<div class="m2"><p>برتر شده ز چون و چرا مرتضی علیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر شاه اولیاطلبی و امام دین</p></div>
<div class="m2"><p>بشنو اسیریا بخدا مرتضی علیست</p></div></div>