---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>کافر عشقم مسلمان نیستم</p></div>
<div class="m2"><p>بت پرستم اهل ایمان نیستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رندم و آزاده از خلد و جحیم</p></div>
<div class="m2"><p>در هوای حور و غلمان نیستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرد و یکتاام براه عشق یار</p></div>
<div class="m2"><p>همچو زاهد از دورنگان نیستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حق پرستم نه چو زاهد خودپرست</p></div>
<div class="m2"><p>مؤمنم از بت پرستان نیستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صوفی صافی ز اوصاف بشر</p></div>
<div class="m2"><p>مرد سالوسی چو شیخان نیستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سریر ملک عرفان و یقین</p></div>
<div class="m2"><p>پادشاهم از گدایان نیستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستم و لایعقل از جام وصال</p></div>
<div class="m2"><p>در خمار درد هجران نیستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نور وحدت بر دل من چون بتافت</p></div>
<div class="m2"><p>نور حقم جنس خلقان نیستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باده عشقش مرا هشیار ساخت</p></div>
<div class="m2"><p>عشق ورزی را زمستان نیستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوز عشق آمد دوای درد ما</p></div>
<div class="m2"><p>عاشقم جویای درمان نیستم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون اسیری از کمال نیستی</p></div>
<div class="m2"><p>گوید از عامم ز خاصان نیستم</p></div></div>