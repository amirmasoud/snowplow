---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>هرگز نبود حسن ترا مبداء و غایت</p></div>
<div class="m2"><p>نه عشق مرا هست نهایت نه بدایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی هادی عشقت بخدا سالک عاشق</p></div>
<div class="m2"><p>هرگز نتواند که رود راه هدایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد که همه در پی حوری و بهشت است</p></div>
<div class="m2"><p>دیدار تو میجست اگر داشت درایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق نبرد جان بسلامت ز ره عشق</p></div>
<div class="m2"><p>از جانب معشوق اگر نیست حمایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس دور فتادست ز معشوق و ره عشق</p></div>
<div class="m2"><p>هرکو نکند جانب عشاق رعایت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ره سوی وصال تو برد این دل مهجور</p></div>
<div class="m2"><p>هرگه که درآید بمیان دست ولایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر حال دل زار اسیری ز توای جان</p></div>
<div class="m2"><p>از عین ترحم نظری هست کفایت</p></div></div>