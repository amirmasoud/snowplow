---
title: >-
    شمارهٔ ۴۷۳
---
# شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>چون عشق سراسیمه درآمد بمیانه</p></div>
<div class="m2"><p>برد از غم دنیا و ز دینم بکرانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبر چو نقاب از رخ چون ماه برانداخت</p></div>
<div class="m2"><p>دیدم بیقین دانش عقلست فسانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درهر چه نظر میکنم از روی حقیقت</p></div>
<div class="m2"><p>از پرتو حسن تو در و هست نشانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد عرصه آفاق پراز کوکبه عشق</p></div>
<div class="m2"><p>تا شاهد رخسار تو آمد بمیانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم همه پرفتنه و غوغاست که آن یار</p></div>
<div class="m2"><p>از خانه بصحرای جهان گشت روانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کسوت اغیار چو بنمود رخ آن یار</p></div>
<div class="m2"><p>ای قصه در آفاق جهان گشت ترانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنمود بخود یار جمال رخ خود را</p></div>
<div class="m2"><p>ما و تو اسیری بمیان بود بهانه</p></div></div>