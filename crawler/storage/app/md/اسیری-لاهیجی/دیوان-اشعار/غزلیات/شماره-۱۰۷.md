---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>خیل غمت بجور و جفا ملک جان گرفت</p></div>
<div class="m2"><p>دل تن نهاد و دامن شادی روان گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل ره عدم چو گرفتی از آن میان</p></div>
<div class="m2"><p>جانم ز هستی تو کناری نهان گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آید همیشه اشک بدریوزه پیش تو</p></div>
<div class="m2"><p>اوسایل است و راه نشاید برآن گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل مرکب قرار بمیدان عشق تاخت</p></div>
<div class="m2"><p>چون شهسوار درد تو آمد عنان گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا لذت شراب غمت یافت کام جان</p></div>
<div class="m2"><p>سرخوش شد و بمصطبه پای دنان گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون حسن تو بحور و بغلمان ظهور کرد</p></div>
<div class="m2"><p>زاهد چنان هوای جنان از چنان گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف تو داشت جان اسیری ببند خویش</p></div>
<div class="m2"><p>رویت بصد لطافتش آخر ضمان گرفت</p></div></div>