---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای عشق تو آتش زده در خرمن جان‌ها</p></div>
<div class="m2"><p>وز سوز غمت سوخته دل‌ها و روان‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون شد دل عشاق ز دست الم عشق</p></div>
<div class="m2"><p>شرح غم عشق تو برونست ز بیان‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شوق جمال تو دل چرخ پرآتش</p></div>
<div class="m2"><p>بی‌صبر و قرار از غم تو دور و زمان‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محرم به حریم تو نه نام و نه نشانست</p></div>
<div class="m2"><p>بی‌نام و نشان در حرمت نام و نشان‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرگشته و حیران به بیابان تحیر</p></div>
<div class="m2"><p>در ذات و صفات تو یقین‌ها و گمان‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بحر هویت که دو عالم به کنارست</p></div>
<div class="m2"><p>خلقان همگی بی‌خبر از قعر و میان‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پرتو حسنت شده تابان همه عالم</p></div>
<div class="m2"><p>وز روی تو روشن همه کون و مکان‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی حسن رخت شرح دهد نطق کماهی</p></div>
<div class="m2"><p>در وصف جمال تو چو لال است زبان‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عارف نبود هرکه نبیند چو اسیری</p></div>
<div class="m2"><p>آیینه روی تو عیان‌ها و نهان‌ها</p></div></div>