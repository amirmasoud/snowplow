---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>شاه و گدا یکسان بود بر درگه سلطان عشق</p></div>
<div class="m2"><p>صدپایه از طاق فلک بالاترست ایوان عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برقطع سر از عاشقان گر حکم راند شاه عشق</p></div>
<div class="m2"><p>قطعا نمی پیچند سر عشاق از فرمان عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش طبیب عاشقان بیمار درد عشق را</p></div>
<div class="m2"><p>جز جان فشانی از غمش هرگز نشد درمان عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غواص دریای فناآورد بیرون عاقبت</p></div>
<div class="m2"><p>درهای اسرار یقین زین بحر بی پایان عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق براه عشق او در کوی خواری و فنا</p></div>
<div class="m2"><p>تا بی سروسامان نگشت پیدا نشد سامان عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن ترک چشم و غمزه اش از عاشقان بی نوا</p></div>
<div class="m2"><p>رخت دل و جان ناگهان بردند در تالان عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بربود گوی عاشقی آخر اسیری از میان</p></div>
<div class="m2"><p>تا اسب همت از کران در راند در میدان عشق</p></div></div>