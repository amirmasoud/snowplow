---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>هرکه او پیوسته با یاد خداست</p></div>
<div class="m2"><p>لطف حق در شان وی بی منتهاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرشد از نور الهی کاینات</p></div>
<div class="m2"><p>جان مستان غرقه بحر صفاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حق تجلی میکند برکوه طور</p></div>
<div class="m2"><p>موسیا برخیز میقات لقاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو پنهان می شوی پیداست یار</p></div>
<div class="m2"><p>جز تو او را پرده دیگر کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر زمان از پرده نوعی رخ نمود</p></div>
<div class="m2"><p>دم بدم اورا دگرگون عشوه هاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصل معشوق است عاشق را خیال</p></div>
<div class="m2"><p>فکر جنت زاهدان را کو جداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ره بجانان جذبه آمد یا سلوک</p></div>
<div class="m2"><p>ای اسیری هر دوره گو راه ماست</p></div></div>