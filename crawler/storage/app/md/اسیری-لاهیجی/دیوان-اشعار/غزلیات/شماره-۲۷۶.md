---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>عاشق دیوانه دل کز غم همه سوزست و ساز</p></div>
<div class="m2"><p>نقد جان در بوته عشق تو دارد در گداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جر نیاز و سوز نبود رهبر سالک بدوست</p></div>
<div class="m2"><p>نیست بی رهبر کسی را ره بوصل بی نیاز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در قمار عشق جانان هم به داو اولین</p></div>
<div class="m2"><p>دین و دنیا جان و دل در باخت رند پاکباز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست بی دلبر دلم را هیچ آرام و قرار</p></div>
<div class="m2"><p>جان مشتاق لقا را صبر کو بی دلنواز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده از ما وام کن زاهد ببین رخسار دوست</p></div>
<div class="m2"><p>دیده محمود باید بهر دیدار ایاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مؤمنان در مسجد و کفار در بتخانه ها</p></div>
<div class="m2"><p>رو به محراب دو ابروی تو دارند در نماز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طایر جان اسیری در پی عنقای وصل</p></div>
<div class="m2"><p>در هوای لامکان طیران کند چون شاهباز</p></div></div>