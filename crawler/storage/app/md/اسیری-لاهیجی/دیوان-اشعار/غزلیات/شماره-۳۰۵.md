---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>ز جام عشق سرمستم زهی ذوق</p></div>
<div class="m2"><p>ز مستی از خودی رستم زهی ذوق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همین قدر بلندم خود تمام است</p></div>
<div class="m2"><p>که در کویت چنین پستم زهی ذوق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه حاجت ساقیا با ساغر و می</p></div>
<div class="m2"><p>ز حسنت بیخود و مستم زهی ذوق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو از مستی خود کلی شدم نیست</p></div>
<div class="m2"><p>بهستی دگر هستم زهی ذوق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو از بند خودی خود را بریدم</p></div>
<div class="m2"><p>بوصل دوست پیوستم زهی ذوق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مجنون گشت جان از شوق رویت</p></div>
<div class="m2"><p>بقید زلف تو بستم زهی ذوق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شدم بی پا و سر از باده عشق</p></div>
<div class="m2"><p>ز دست عقل خوش جستم زهی ذوق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همیشه با می و شاهد حریفم</p></div>
<div class="m2"><p>چو عهد توبه بشکستم زهی دوق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خورشید جمالت پرده برداشت</p></div>
<div class="m2"><p>اسیری رفت از دستم زهی ذوق</p></div></div>