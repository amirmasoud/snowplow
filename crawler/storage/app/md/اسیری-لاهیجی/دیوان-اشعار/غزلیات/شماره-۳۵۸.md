---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>دادیم دل بدست غم عشق آن صنم</p></div>
<div class="m2"><p>گو دل نگاه دار و مکن بیش ازین ستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذر زفکر عالم و با یاد دوست باش</p></div>
<div class="m2"><p>چون یارمونست بود از بیش و کم چه غم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با درد و سوز عشق بساز ای دل حزین</p></div>
<div class="m2"><p>گر زانکه میکنی بجهان کار بی ندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه در غم فراقم و گه در غم وصال</p></div>
<div class="m2"><p>کو جذبه که باز رهم من ازین دو هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چشم ساقی و لب میگون جانفزاش</p></div>
<div class="m2"><p>دارم بروی دوست فراغت زجام جم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل بدرد خو کن و درمان ز کس مجو</p></div>
<div class="m2"><p>شاید شوی بدولت غم شاد و محترم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر زانکه وصل یار اسیری طلب کنی</p></div>
<div class="m2"><p>باید که برسر خود و عالم نهی قدم</p></div></div>