---
title: >-
    شمارهٔ ۴۵۰
---
# شمارهٔ ۴۵۰

<div class="b" id="bn1"><div class="m1"><p>فی مقامی این کیف، این این</p></div>
<div class="m2"><p>این علم، این حال، این عین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این فرق، این جمع، این بعد</p></div>
<div class="m2"><p>این قرب، این وصل، این بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذاتنا عن وصف اطلاق و قید</p></div>
<div class="m2"><p>مطلق ماعندنا زین وشین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیس آثار لغیری فی الوجود</p></div>
<div class="m2"><p>فی شهودی کل کشف عین زین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کل عین قلت هذا غیرنا</p></div>
<div class="m2"><p>فهو عینی لاتقل للعین غین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قداضائت شمسنا کل الظلام</p></div>
<div class="m2"><p>مایری الخفاش ضعفا نور عین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ان یکن اخفاء سرستة</p></div>
<div class="m2"><p>یا اسیری قل وکشفی فرض عین</p></div></div>