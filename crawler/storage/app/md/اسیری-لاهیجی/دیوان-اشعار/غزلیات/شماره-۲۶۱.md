---
title: >-
    شمارهٔ ۲۶۱
---
# شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>ای ز خورشید جمالت هر دوعالم غرق نور</p></div>
<div class="m2"><p>وی ز سبحات جلالت هرکجا شرست و شور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه عکس روی تو بیند ز مرآت جهان</p></div>
<div class="m2"><p>در حقیقت از همه عالم بود او را حضور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وای بر جانی که باشد از فراقت غمزده</p></div>
<div class="m2"><p>خوش دلی کور است از وصل تو شادی و سرور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>والهان حسن رخسار تو فارغ گشته اند</p></div>
<div class="m2"><p>از نعیم و عزت دنیا و از فردوس و حور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در فراقت مبتلاشد هرکه نگذشت از خودی</p></div>
<div class="m2"><p>ره بوصل تو کسی یابد که کرد از خود عبور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست تنها بیقرار از شوق تو جان و دلم</p></div>
<div class="m2"><p>هست ذرات جهان از مهر رویت ناصبور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جمال نوربخش او اسیری گشته است</p></div>
<div class="m2"><p>آنچنان حیران که از هر دو جهان دارد نفور</p></div></div>