---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>ما می پرست یار و جهان می پرست ما</p></div>
<div class="m2"><p>مامست عشق و کون ومکان بوده مست ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنب وجودم از می توحید حق پرست</p></div>
<div class="m2"><p>زاهد مکن بسنگ ملامت شکست ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ملک عشق منصب ما بین چو شد بلند</p></div>
<div class="m2"><p>ماپست یارو جمله جهان گشته پست ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر ماهیی که بود درین بحر بی کران</p></div>
<div class="m2"><p>موج کرم فکند تمامی بشست ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذرات کون آینه مهر روی اوست</p></div>
<div class="m2"><p>این روی دیده بود ز بت بت پرست ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر وحدت خدا همه ذرات شاهدند</p></div>
<div class="m2"><p>گر منکری شنو تو جواب الست ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما پشت پا زدیم اسیری بهر دو کون</p></div>
<div class="m2"><p>تا اوفتاد دامن عشقش بدست ما</p></div></div>