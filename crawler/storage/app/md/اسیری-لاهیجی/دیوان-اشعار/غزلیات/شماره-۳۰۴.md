---
title: >-
    شمارهٔ ۳۰۴
---
# شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>وقت کوچ آمد نفیرالطریقست الطریق</p></div>
<div class="m2"><p>ره خطرناکست یاران وا ممانید از رفیق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طریق عشق جانان عاشق جانباز باش</p></div>
<div class="m2"><p>گر همی خواهی که حاصل گرددت ذوق طریق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ سالک ره نیابد در مقام وصل دوست</p></div>
<div class="m2"><p>فی طریق العشق لولم یهده شیخ شفیق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز نمود هست مطلق نیست نفس کاینات</p></div>
<div class="m2"><p>کل مافی الکون موج و هوکالبحرالعمیق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اشتیاق جام وصلت در خمارم ساقیا</p></div>
<div class="m2"><p>لطف فرما از لب لعلت شرابی چون عقیق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق دیوانه را با عقل و هشیاری چه کار</p></div>
<div class="m2"><p>چون ز جام عشق دایم میکشد جانش رحیق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از غم دنیا و دین پیوسته باشد در کنار</p></div>
<div class="m2"><p>هرکه در بحر فنا همچو اسیری شد غریق</p></div></div>