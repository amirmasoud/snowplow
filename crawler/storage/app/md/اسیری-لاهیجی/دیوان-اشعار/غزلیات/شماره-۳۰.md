---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>ای پرتو جمال تو نورالیقین ما</p></div>
<div class="m2"><p>گیسوی عنبرین تو حبل المتین ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسون غمزه تو دلم را ز ره ببرد</p></div>
<div class="m2"><p>چشمت برهزنی شده سحر مبین ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ابرو و غمزه چشم تو چون ترک فتنه جو</p></div>
<div class="m2"><p>تیر و کمان گرفته بود در کمین ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانا ز زلف سرکش خونریز بازجو</p></div>
<div class="m2"><p>کز بهر چیست بسته میانرا به کین ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست قضا ز آتش شوقت کشیده بود</p></div>
<div class="m2"><p>روز الست داغ چنین برجبین ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با عاشقان ز مذهب و زهد و ورع مگو</p></div>
<div class="m2"><p>رندی و عشق ورزی و مستیست دین ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خو کن بدرد عشق و غم او اسیریا</p></div>
<div class="m2"><p>شادی مجو و عیش ز جان حزین ما</p></div></div>