---
title: >-
    شمارهٔ ۴۴۶
---
# شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>از حد گذشت نوبت هجران جان ستان</p></div>
<div class="m2"><p>وقت است کز وصال تو گردیم شادمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا با خودی ز وصل نخواهی شنید بو</p></div>
<div class="m2"><p>واصل گهی شوی که نیابی ز خود نشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصل تو نیست لایق زهاد خودپرست</p></div>
<div class="m2"><p>این دولتی است در خور عشاق جان فشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره می نمود جانب هستی خرد ولی</p></div>
<div class="m2"><p>عشقم بسوی فقر و فنا برد موکشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون در طریق عشق حجابست کبر و ناز</p></div>
<div class="m2"><p>دارم همیشه روی نیازی برآستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنگ خیال و وهم زمرآت دل زدای</p></div>
<div class="m2"><p>تا روی جانفزاش نماید درو عیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جام عشق جان اسیری چو مست شد</p></div>
<div class="m2"><p>فارغ ز هست و نیست ز سود آمد و زیان</p></div></div>