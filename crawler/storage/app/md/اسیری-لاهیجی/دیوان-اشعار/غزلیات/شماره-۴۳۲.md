---
title: >-
    شمارهٔ ۴۳۲
---
# شمارهٔ ۴۳۲

<div class="b" id="bn1"><div class="m1"><p>ای ز آفتاب روی تو روشن جهان جان</p></div>
<div class="m2"><p>وز پرتو جمال تو تابان شده جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر نقاب شاهد رویت نهان هنوز</p></div>
<div class="m2"><p>وصف جمال او بجهان گشاه داستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگشای دیده زاهد و بنگر که ظاهرست</p></div>
<div class="m2"><p>عکس جمال دوست زمرآت جسم و جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بویی ز عاشقی نشنیدست عاشقی</p></div>
<div class="m2"><p>کاندر طریق عشق نبودست جان فشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازنام وصل یافت نشانی کسی که او</p></div>
<div class="m2"><p>از خود ببزم وصل بکل گشت بی نشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این طرفه بین که یار در آغوش و من چنین</p></div>
<div class="m2"><p>در جست و جوی او بجهان گشته ام دوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لذت ببین و عیش که عمری ز جور یار</p></div>
<div class="m2"><p>هرگز ز دست غصه نبودم دمی امان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیرون ز شرح جلوه روی تو دیده ام</p></div>
<div class="m2"><p>زان رو ز وصف حسن تو کوتاه شد زبان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنگر علو مرتبه از محض موهبت</p></div>
<div class="m2"><p>دارد مکان همیشه اسیری بلامکان</p></div></div>