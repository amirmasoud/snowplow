---
title: >-
    شمارهٔ ۳۲۱
---
# شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>مائیم نقاب شاهد شنگ</p></div>
<div class="m2"><p>او شنگ و نقاب همچو وی ینگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عهدیست میان ما و دلبر</p></div>
<div class="m2"><p>کز ما نشود جدا به نیرنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کس که جمال روی او دید</p></div>
<div class="m2"><p>شیداست چو ما و واله و دنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر لحظه به تیغ غمزه چشمش</p></div>
<div class="m2"><p>بی جرم کند بخونم آهنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق در آشتی همیزد</p></div>
<div class="m2"><p>معشوق نداشت جز سرجنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیرت سر عاشقان بی باک</p></div>
<div class="m2"><p>کردست بدار عشق آونگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دولت عشق شد اسیری</p></div>
<div class="m2"><p>آزاده ز قید نام و ز ننگ</p></div></div>