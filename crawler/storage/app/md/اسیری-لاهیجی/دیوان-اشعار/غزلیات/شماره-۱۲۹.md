---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>مهر روی تو که تابان ز همه ذراتست</p></div>
<div class="m2"><p>حسن او را همه کون و مکان مرآتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیستی جمله بهستی تو پیدا شده است</p></div>
<div class="m2"><p>غیر ازین هرکه بگوید سخن طاماتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش عارف که ز اوراق جهان حسن تو دید</p></div>
<div class="m2"><p>مصحف روی ترا جمله جهان آیاتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده آن دیده که بینا بود از نور یقین</p></div>
<div class="m2"><p>که جمال تو هویدا ز همه ذراتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه عالم همگی مظهر اسما شده است</p></div>
<div class="m2"><p>گشته ظاهر همه اسما ز صفات و ذاتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که گهی عشق و گهی عاشق و گه معشوقی</p></div>
<div class="m2"><p>وه شئونات الهی چه عجب حالاتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعد آزادگی از قید اسیری دانست</p></div>
<div class="m2"><p>که ظهورات ترا هر دو جهان آلاتست</p></div></div>