---
title: >-
    شمارهٔ ۳۴۵
---
# شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>ماز تاب حسن او شیدا و حیران گشته ایم</p></div>
<div class="m2"><p>همچو زلف بیقرار او پریشان گشته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آتش سودای جانان تا دل و جانم بسوخت</p></div>
<div class="m2"><p>هم بیمن درد عشقش عین درمان گشته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جمال روی ساقی مست و لایعقل شدیم</p></div>
<div class="m2"><p>وز شراب لعل اومدهوش و حیران گشته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز لقای دوست جانم را نباشد وایه</p></div>
<div class="m2"><p>بی دل و دین ما ز بهر وایه جان گشته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر حجاب جان سالک کفر زلف یار بود</p></div>
<div class="m2"><p>ما بکفر زلف او واقف ز ایمان گشته ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه بدیر و گه بکعبه گه بمسجد گه کنشت</p></div>
<div class="m2"><p>در همه اطوار ما جویای جانان گشته ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرتو خورشید رویش ظلمت ما محو کرد</p></div>
<div class="m2"><p>تازتاب نور او چون ماه تابان گشته ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جای ما در سایه پیر خرابات آمدست</p></div>
<div class="m2"><p>شاهد و می را چو ما پیوسته جویان گشته ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای اسیری تا جمال روی جانان دیده ایم</p></div>
<div class="m2"><p>فارغ از قید بهشت و حور و غلمان گشته ایم</p></div></div>