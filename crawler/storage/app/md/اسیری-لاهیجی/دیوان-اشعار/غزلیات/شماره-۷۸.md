---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>معشوق باز و رندم و قلاش و می پرست</p></div>
<div class="m2"><p>دارم بعشق دوست فراغت زهر چه هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوند کن بوصل، دل پاره پاره را</p></div>
<div class="m2"><p>ماخود شکسته ایم چه حاجت دگر شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عاشقی بهر دو جهان گشت سربلند</p></div>
<div class="m2"><p>عاشق که شد بعشق تو چون خاک راه پست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمری ز بهر دیدن او دست و پا زدم</p></div>
<div class="m2"><p>چون رو نمود حیف که کلی شدم ز دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دور شد نقاب جلال از جمال یار</p></div>
<div class="m2"><p>گردد عیان که عابد بت بود حق پرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دم که در خمار عدم بود جام و می</p></div>
<div class="m2"><p>جانم ز باده لب لعل تو بود مست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید روی دوست چو بر جان و دل بتافت</p></div>
<div class="m2"><p>گر خود پرست بود اسیری ز خود برست</p></div></div>