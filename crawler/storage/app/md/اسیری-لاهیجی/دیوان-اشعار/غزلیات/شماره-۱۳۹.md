---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>تا بعشق تو جان گرفتارست</p></div>
<div class="m2"><p>دل از این درد و غم جگرخوارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر خود هرکه بی غم عشقت</p></div>
<div class="m2"><p>میگذارد بهر زه بیکارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن انکار عشق ما زاهد</p></div>
<div class="m2"><p>عاشقان را بعشق اقرارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد کوی تو دایما عاشق</p></div>
<div class="m2"><p>پا بجا در سفر چو پرگارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود محالست از تو ببریدن</p></div>
<div class="m2"><p>با تو پیوستن ازچه دشوارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه از درد عشق بیم آرد</p></div>
<div class="m2"><p>نیست عاشق که مرد بیمارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل بسودای وصل جانان باخت</p></div>
<div class="m2"><p>جان ودل را، واز دوئی وارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی فرود آوریم سر به بهشت</p></div>
<div class="m2"><p>غرض عاشقان چو دیدار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون اسیری بقید عشق رخش</p></div>
<div class="m2"><p>جمله خلق جهان گرفتارست</p></div></div>