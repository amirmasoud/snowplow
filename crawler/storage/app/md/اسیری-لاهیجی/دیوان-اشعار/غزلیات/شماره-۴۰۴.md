---
title: >-
    شمارهٔ ۴۰۴
---
# شمارهٔ ۴۰۴

<div class="b" id="bn1"><div class="m1"><p>ازآن می تا بنوشیدم دو سه جام</p></div>
<div class="m2"><p>شدم در زهد و هشیاری نکونام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بده ساقی شراب آتشینم</p></div>
<div class="m2"><p>که سازد پخته از یک جرعه صد خام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طلب کن همت از پیر خرابات</p></div>
<div class="m2"><p>براه میکده گر می نهی گام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجو جام مصفا و می صاف</p></div>
<div class="m2"><p>ببین عکس رخ ساقی درآن جام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا می خواره باش و مست و قلاش</p></div>
<div class="m2"><p>مگر یابی ازین وصل دلارام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جام لعل ساقی تا شدم مست</p></div>
<div class="m2"><p>ز دستم رفت عقل و صبر و آرام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو روی شاهدم صافی و بیغش</p></div>
<div class="m2"><p>چو چشم مست ساقی درد آشام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نباشد کار من جز می پرستی</p></div>
<div class="m2"><p>گر از لعل لبش یابم دمی کام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حریف شاهد و جام شرابم</p></div>
<div class="m2"><p>هنر بین زین اسیری گو در ایام</p></div></div>