---
title: >-
    شمارهٔ ۴۴۹
---
# شمارهٔ ۴۴۹

<div class="b" id="bn1"><div class="m1"><p>همچو خاک ره بعشقش خوار می باید شدن</p></div>
<div class="m2"><p>بلبل آسا در پی گل، زار می باید شدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر همی خواهی که بوی فقریابی در طریق</p></div>
<div class="m2"><p>با همه کس چون گل بی خار می باید شدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تو پا در دایره عشقش نهی چون عاشقان</p></div>
<div class="m2"><p>بردرش سرگشته چون پرگار می باید شدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکجا باشد اگر وادی بود گر کوه طور</p></div>
<div class="m2"><p>همچو موسی از پی دیدار می باید شدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر وصال یار خواهی در رهش مردانه وار</p></div>
<div class="m2"><p>از خیال کفر و دین بیزار می باید شدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی من از من یارمیگوید(انا)الحق پس مرا</p></div>
<div class="m2"><p>همچو منصور از چه رو بردار می باید شدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با حریفان گر همی خواهی کنی هم کاسگی</p></div>
<div class="m2"><p>لاابالی بردر خمار می باید شدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه خواهد کو شود مست از شراب نیستی</p></div>
<div class="m2"><p>از خمار هستیش هشیار می باید شدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با همه مستی اسیری با ادب رو در طریق</p></div>
<div class="m2"><p>راه عشق دوست برهنجار می باید شدن</p></div></div>