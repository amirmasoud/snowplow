---
title: >-
    شمارهٔ ۲۳۷
---
# شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>ساقیا می ده که هشیارم کند</p></div>
<div class="m2"><p>مستیش زین خواب بیدارم کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان میی کارد خمارش نیستی</p></div>
<div class="m2"><p>فارغ از هستی و پندارم کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در صفای او نماید روی یار</p></div>
<div class="m2"><p>صاف و پاک از زنگ اغیارم کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان شرابی ده که شادی آورد</p></div>
<div class="m2"><p>وز غم کونین بیزارم کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد پیاپی جام ما در دایره</p></div>
<div class="m2"><p>تا که سرگردان چو پرگارم کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست و بیخود سازدم منصور وار</p></div>
<div class="m2"><p>چون همی خواهد که بردارم کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جرعه زان می اسیری گو بنوش</p></div>
<div class="m2"><p>تا چو مولانا و عطارم کند</p></div></div>