---
title: >-
    شمارهٔ ۳۶۵
---
# شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>حان الرحیل مابر دلدار میرویم</p></div>
<div class="m2"><p>لاخوف گو بعشق چو همراه می شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نظر بدنیی و عقبی نیفکنیم</p></div>
<div class="m2"><p>ترک همه گرفته پی یار می رویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما طالبیم و در پی مطلوب روز و شب</p></div>
<div class="m2"><p>دایم چو سایه از پی آن نور می دویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشتاق حسن روی نگاریم آنچنان</p></div>
<div class="m2"><p>کز شوق یکنفس به شب و روز نغنویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطرب بنای، دم زدم عشق می دمد</p></div>
<div class="m2"><p>تا سر عشق از نفس نای بشنویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی ز عشق و عاشق کهنه کنی حدیث</p></div>
<div class="m2"><p>عشقم بنو ببین که کنون عاشق نویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معشوق و عاشق ار چه بمعنی یکی بود</p></div>
<div class="m2"><p>بنگر اسیریا که به معنی یکی دویم</p></div></div>