---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>جان ما را میل دل جستن نشد</p></div>
<div class="m2"><p>درد و غم در پیش وی گفتن نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وعده کرد امشب که آیم پیش تو</p></div>
<div class="m2"><p>ز انتظارش امشبم خفتن نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل دوراندیش چندانی که کرد</p></div>
<div class="m2"><p>جوی خون از دیده ها بستن نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر امید آنکه یارم می کشد</p></div>
<div class="m2"><p>در بدر گردیدم و کشتن نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواست دل کز دام هجران بگسلد</p></div>
<div class="m2"><p>عمر آخر گشت و زان جستن نشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانم ار دارد فراغ از هر دو کون</p></div>
<div class="m2"><p>لیکن از عشق بتان رستن نشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای اسیری شکر کن کز عشق او</p></div>
<div class="m2"><p>هرگزت یک لحظه برگشتن نشد</p></div></div>