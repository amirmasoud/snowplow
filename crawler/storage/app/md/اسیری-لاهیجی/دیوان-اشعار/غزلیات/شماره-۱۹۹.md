---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>یارم اگر جمال نماید چه می شود</p></div>
<div class="m2"><p>از رخ نقاب زلف گشاید چه می شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبر اگر بکلبه احزان بیدلان</p></div>
<div class="m2"><p>روزی بروی مهر درآید چه می شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بزم وصل گر بدهد بار عاشقان</p></div>
<div class="m2"><p>تا روزگار هجر سرآید چه می شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیمار عشق را اگر آن بیوفا طبیب</p></div>
<div class="m2"><p>یک لحظه پرسشی بنماید چه می شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمش بغمزه چون دل زهاد می ربود</p></div>
<div class="m2"><p>گر جان عاشقان برباید چه می شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر یار رو نهان کند از ما زمان زمان</p></div>
<div class="m2"><p>تا جان ما بشوق فزاید چه می شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان یار اسیریا که بحسن است بی نظیر</p></div>
<div class="m2"><p>صد جور اگر بجان تو آید چه میشود</p></div></div>