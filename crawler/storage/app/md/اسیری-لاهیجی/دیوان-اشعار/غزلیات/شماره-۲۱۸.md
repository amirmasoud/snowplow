---
title: >-
    شمارهٔ ۲۱۸
---
# شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>در هوای عشق بازم دل پرواز کرد</p></div>
<div class="m2"><p>بار دیگر عاشقی جانم ز سر آغاز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر در او بس که بنشستم درآخر آن صنم</p></div>
<div class="m2"><p>رحم کرد و آن در بسته برویم باز کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون درون رفتم بخلوتخانه بزم شهود</p></div>
<div class="m2"><p>وه چه دلداری که با من دلبر طناز کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ز نقش غیرخالی دید او لوح دلم</p></div>
<div class="m2"><p>در سرای انس با خود جان ما دمساز کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه از خلقان چو عنقا در جهان عزلت گزید</p></div>
<div class="m2"><p>مرغ جانش در هوای لامکان پرواز کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه بیند از همه عالم جمال روی او</p></div>
<div class="m2"><p>در نهان و آشکارا با خودش همراز کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جمال نوربخشش چون اسیری شد فنا</p></div>
<div class="m2"><p>از بقای بی زوالش بانوا و ساز کرد</p></div></div>