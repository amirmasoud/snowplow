---
title: >-
    شمارهٔ ۴۱۷
---
# شمارهٔ ۴۱۷

<div class="b" id="bn1"><div class="m1"><p>حسن جان افزای روی او عیان</p></div>
<div class="m2"><p>دیده ام از روی پیدا و نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرتو خورشید روی او بود</p></div>
<div class="m2"><p>در حقیقت جمله ذرات جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن او بر نقش عالم جلوه کرد</p></div>
<div class="m2"><p>شد جهان زان روز با نام و نشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی او پیداست، کو چشم یقین؟</p></div>
<div class="m2"><p>تا جمال دوست بیند بی گمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر زمان از روی مه رویی دگر</p></div>
<div class="m2"><p>حسن جان افروز او گردد عیان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه گویم در بیان آن جمال</p></div>
<div class="m2"><p>قطره باشد ز بحر بی کران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مظهر آیات اسرار خداست</p></div>
<div class="m2"><p>هرچه ظاهر گشت در کون و مکان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست در عالم بجز دیدار دوست</p></div>
<div class="m2"><p>مرهم درد درون عاشقان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد اسیری نیست در هستی و گفت</p></div>
<div class="m2"><p>لیس فی الدارین غیری هر زمان</p></div></div>