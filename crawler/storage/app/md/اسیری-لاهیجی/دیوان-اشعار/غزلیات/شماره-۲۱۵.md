---
title: >-
    شمارهٔ ۲۱۵
---
# شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>من عاشق آن جان و جهانم همه دانند</p></div>
<div class="m2"><p>از جان ببریدن نتوانم همه دانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان می نتوان برد از آن غمزه و ابرو</p></div>
<div class="m2"><p>من کشته آن تیروکمانم همه دانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف سیه و چشم بلا جوی تو دیدم</p></div>
<div class="m2"><p>آشفته و بیمار از آنم همه دانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دولت عشق رخ آن سرو خرامان</p></div>
<div class="m2"><p>سرحلقه رندان جهانم همه دانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا گشت اسیری بغم عشق گرفتار</p></div>
<div class="m2"><p>آزاده ازین کون و مکانم همه دانند</p></div></div>