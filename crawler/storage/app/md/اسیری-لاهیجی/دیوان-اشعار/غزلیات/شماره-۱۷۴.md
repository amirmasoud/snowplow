---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>زان پیشتر که دور جهان را مدار بود</p></div>
<div class="m2"><p>از شوق روی دوست دلم بیقرار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نگشت جان من از وصل ناامید</p></div>
<div class="m2"><p>چون لطف دوست در حق من بیشمار بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منصور وار گفت اناالحق بپای دار</p></div>
<div class="m2"><p>هرکو بدار عشق چو او پایدار بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستی بی خمار بجز جان ما که دید؟</p></div>
<div class="m2"><p>کو مست سرمدی ز می بی خمار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان دم که شاه حسن تو زد خیمه در جهان</p></div>
<div class="m2"><p>عالم ز شور عشق پر از گیرو دار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناموس و فخر ما بجهان چیست؟عشق یار</p></div>
<div class="m2"><p>عشاق را ز زهد ریا ننگ و عار بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی ما و من برفت اسیری براه دوست</p></div>
<div class="m2"><p>چون ما و من حجاب ره وصل یار بود</p></div></div>