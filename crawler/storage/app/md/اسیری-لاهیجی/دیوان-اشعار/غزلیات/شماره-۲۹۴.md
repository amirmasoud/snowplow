---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>شرح درد عشق دارد طول و عرض</p></div>
<div class="m2"><p>فرصت ار یابم کنم با دوست عرض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق اگر گوید بترک سربگو</p></div>
<div class="m2"><p>عاشقان را طاعت عشقست فرض</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طاقت و تاب غم عشقت نداشت</p></div>
<div class="m2"><p>غیر عاشق نه سماوات و نه ارض</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب حیوان گر چه می بخشد حیات</p></div>
<div class="m2"><p>میکند آن را ز لعل دوست قرض</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای اسیری گر محال آمد وصال</p></div>
<div class="m2"><p>خوش محالی میکنم هر لحظه فرض</p></div></div>