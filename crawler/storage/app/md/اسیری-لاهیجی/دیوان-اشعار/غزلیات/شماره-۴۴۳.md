---
title: >-
    شمارهٔ ۴۴۳
---
# شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>آتش عشق درزدی در دل و جان عاشقان</p></div>
<div class="m2"><p>از غم عشق یک نفس نیست امان عاشقان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست بدور ما دلی بی غم عشق یکنفس</p></div>
<div class="m2"><p>پرز جفای عشق شد دور و زمان عاشقان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حق وفا که رونما هر نفسی و گر نه خود</p></div>
<div class="m2"><p>بی تو بچرخ میرود آه و فغان عاشقان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود گمان که عاقبت سربنهم بپای تو</p></div>
<div class="m2"><p>شکر که گشت پیش تو راست گمان عاشقان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک مرا بعشق تو باد فنا بباد داد</p></div>
<div class="m2"><p>در ره عشق محو شد نام و نشان عاشقان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ دلم بلامکان می پرد از هوای تو</p></div>
<div class="m2"><p>از سه و چار و نه برون هست مکان عاشقان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهبر ما بوصل تو هادی عشق بوده است</p></div>
<div class="m2"><p>از غم عشق میرسد شادی جان عاشقان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق من و جمال تو چونکه رسید برکمال</p></div>
<div class="m2"><p>وه که عجب فسانه گشت میان عاشقان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت بگو اسیریا جان تو کیست در جهان</p></div>
<div class="m2"><p>غیر تو نیست گفتمش جان و جهان عاشقان</p></div></div>