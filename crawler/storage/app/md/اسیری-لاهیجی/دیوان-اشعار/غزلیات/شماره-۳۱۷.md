---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>ای در شعاع روی تو عقل خبیردنگ</p></div>
<div class="m2"><p>در راه عشقت آمده پای خرد بسنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست شراب عشقم و از عقل بیخبر</p></div>
<div class="m2"><p>واعظ بما مگو سخن زهد و ریو و رنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناموس اگر برفت بعشق تو گو برو</p></div>
<div class="m2"><p>ما را بعشق دوست چه پروای نام و ننگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صاحب دلان، دلا از من بیدل برد بزور</p></div>
<div class="m2"><p>هر دم بعشوه دگر آن یار شوخ شنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانم نبود بی می و معشوق یک نفس</p></div>
<div class="m2"><p>زان دم که ما بدامن رندان زدیم چنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوید رباب نغمه شوق تو زیر و بم</p></div>
<div class="m2"><p>آید صدای عشق تو زآواز عود و چنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا او فتاد پرده عزت ز روی یار</p></div>
<div class="m2"><p>در تاب حسن اوست اسیری دلو و دنگ</p></div></div>