---
title: >-
    شمارهٔ ۳۹۵
---
# شمارهٔ ۳۹۵

<div class="b" id="bn1"><div class="m1"><p>چون یار برقص آید من مطربی آغازم</p></div>
<div class="m2"><p>ور من بسماع ایم یارست نوا سازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مهر رخش گردد ذرات جهان رقصان</p></div>
<div class="m2"><p>در جلوه چو می آید آن دلبر طنازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حسن رخ جانان جان گشت چنان حیران</p></div>
<div class="m2"><p>کز تاب جمال او با خویش نپردازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شوق جمال تو بربوی وصال تو</p></div>
<div class="m2"><p>در انجمن و خلوت با یاد تو همرازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسرار غم عشقت میداشت دلم پنهان</p></div>
<div class="m2"><p>دردا که فتاد اکنون از پرده برون رازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سودای جهان یکسر کردیم برون از سر</p></div>
<div class="m2"><p>تا سوز غم عشقت شد مونس و دمسازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر زانکه گداگشتم بی برگ و نوا گشتم</p></div>
<div class="m2"><p>برجمله شهنشاهان از عشق تو می نازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر عشق جهان سوزت با ما نفسی سازد</p></div>
<div class="m2"><p>یک لحظه ز عقل و دین بنیاد براندازم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر جان اسیری را دادی بوصالت ره</p></div>
<div class="m2"><p>شکرانه آن خود را پیش تو فدا سازم</p></div></div>