---
title: >-
    شمارهٔ ۳۰۰
---
# شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>دارم ز درد عشق تو بر دل هزار داغ</p></div>
<div class="m2"><p>مجروح عشق را ز دو عالم بود فراغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عارف چو راه یافت بخلوتگه شهود</p></div>
<div class="m2"><p>شد پیش او جهان همه گلزار و باغ وراغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با آن قد چو طوبی و رخسار چون بهشت</p></div>
<div class="m2"><p>دارم فراغت از گل و سرو و ز کشت و باغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستم مدام و نیست ز هشیاریم خبر</p></div>
<div class="m2"><p>بازم بعشوه چشم تو میگیردم ایاغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در راه جست و جوی بدم تا که یافتم</p></div>
<div class="m2"><p>از ترک مست خویش ز هر ذره سراغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشنو نوای بلبل جان از گل وصال</p></div>
<div class="m2"><p>از گلشن دلت چو برون شد کلاغ و زاغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با زاهدان مگوی اسیری ز سر عشق</p></div>
<div class="m2"><p>شکر مناسب است بطوطی نه با کلاغ</p></div></div>