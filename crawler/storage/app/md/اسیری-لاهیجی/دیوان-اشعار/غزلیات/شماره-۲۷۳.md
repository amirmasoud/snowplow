---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>ساقی قدحی بکام ما ریز</p></div>
<div class="m2"><p>فرصت چو غنیمت است برخیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدمستی عاشقان جان باز</p></div>
<div class="m2"><p>صدباره به از صلاح و پرهیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میکده آی و با حریفان</p></div>
<div class="m2"><p>می نوش و بشاهدان درآمیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان کن گرو شراب و شاهد</p></div>
<div class="m2"><p>از زهد ریا دلا بپرهیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی که تو جان بری سلامت</p></div>
<div class="m2"><p>در دامن زلف او درآویز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بستان دل و جان و در عوض ده</p></div>
<div class="m2"><p>یک بوسه از آن لب شکرریز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر در قدمش بنه اسیری</p></div>
<div class="m2"><p>تسلیم شو و بعشق مستیز</p></div></div>