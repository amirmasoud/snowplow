---
title: >-
    شمارهٔ ۳۳۰
---
# شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>تا کرد شاه عشق بملک دلم نزول</p></div>
<div class="m2"><p>برخاستست از سر جان عقل بوالفضول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید عمرم ار بفراقت زوال یافت</p></div>
<div class="m2"><p>لیکن ز جان خیال وصال تو لایزول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن یار عین ماست نه از روی اتحاد</p></div>
<div class="m2"><p>این خانه پر ازوست ولیکن نه از حلول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی بهره نیست ذره از مهر روی دوست</p></div>
<div class="m2"><p>نور ترا بظلمت عالم بود شمول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی با خودی ببزم وصالت توان رسید</p></div>
<div class="m2"><p>فانی ز خویش شو که بحق یافتی وصول</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانش همه بمذهب من هست معرفت</p></div>
<div class="m2"><p>در دین ما جز این نه فروعست و نه اصول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از قیل و قال هیچکس آگه نشد ز حال</p></div>
<div class="m2"><p>مفتی ز قول راست مرنج و مشو ملول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهد رسد بجان تو بوئی ز عشق یار</p></div>
<div class="m2"><p>گفتار عاشقان اگرت اوفتد قبول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کس واقف ار ز حال اسیری نشد چه شد</p></div>
<div class="m2"><p>بهتر ز شهرت دگرانست این خمول</p></div></div>