---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>از شاهد و می گر خبری هست بگوئید</p></div>
<div class="m2"><p>چون باده پرستی هنری هست بگوئید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کوی خرابات فنا سالک ره را</p></div>
<div class="m2"><p>جز عشق اگر راهبری هست بگوئید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معشوق مرا کز غم او بیدل و دینم</p></div>
<div class="m2"><p>با عاشق بیدل نظری هست بگوئید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیر از رخ جانان که شد او مطلع انوار</p></div>
<div class="m2"><p>در دور اگر ماه و خوری هست بگوئید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز رفتن این مرتبه قید باطلاق</p></div>
<div class="m2"><p>درسیر و سلوک ار سفری هست بگوئید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون غمزه فتان تو ای ماه پری رو</p></div>
<div class="m2"><p>در دور قمر فتنه گری هست بگوئید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بهر خمار اشکن اگر صاف اگر درد</p></div>
<div class="m2"><p>در میکده گر ماحضری هست بگوئید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز زاهد رعنا که بود مانع عشاق</p></div>
<div class="m2"><p>در عشق اگر دردسری هست بگوئید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون پیر مغان عارف اسرار کماهی</p></div>
<div class="m2"><p>گر زانک بعالم دگری هست بگوئید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز شادی وصل و غم هجران زخ یار</p></div>
<div class="m2"><p>بالله که بهشت و سقری هست بگوئید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در کوی خرابات بقلاشی و رندی</p></div>
<div class="m2"><p>گر خود ز اسیری بتری هست بگوئید</p></div></div>