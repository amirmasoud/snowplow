---
title: >-
    شمارهٔ ۴۶۳
---
# شمارهٔ ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>جمال یار برانداخت پرده از ناگاه</p></div>
<div class="m2"><p>عیان نمود بنقش جهان رخ چون ماه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو لااله ز رویش نقابها برداشت</p></div>
<div class="m2"><p>نمود از همه عالم جمال الاالله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مباش منکر اگر زانکه گفتمت همه اوست</p></div>
<div class="m2"><p>که کشف و عقل بدین دعوی اند هر دو گواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روان ز هر ورق آیات حسن او خوانیم</p></div>
<div class="m2"><p>بمصحف رخ خوبان چو میکنیم نگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه عاشق و قلاش و مست و اوباشم</p></div>
<div class="m2"><p>بروی دوست که دارم همیشه روی براه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا ز قهر مترسان و ناامید مکن</p></div>
<div class="m2"><p>به لطف دوست چو عشاق کرده اند پناه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیریا، سرطاعات نیستی آمد</p></div>
<div class="m2"><p>چرا که نیست چو هستی بدین عشق گواه</p></div></div>