---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>تا بکفر زلف تو جان مرا اقرار شد</p></div>
<div class="m2"><p>دل ز ایمان برگرفت و در پی زنار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شراب عشق جانان جان ما چون گشت مست</p></div>
<div class="m2"><p>از خیال زهد و هشیاری دلم بیزار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شراب جام عشقم از ازل مست و خراب</p></div>
<div class="m2"><p>من ازاین مستی نخواهم تا ابد هشیار شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می نگنجد در جهان جانم ز شادی و نشاط</p></div>
<div class="m2"><p>تا غم عشق تو ما را مونس و غمخوار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دلم خو با جفای عشق جانان کرده است</p></div>
<div class="m2"><p>در وفای عشق او جان و دلم ایثار شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاز لذات دو عالم نگذری مردانه وار</p></div>
<div class="m2"><p>کی توان کی، از لقای دوست برخوردار شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد اسیری فارغ و آزاده از دنیا و دین</p></div>
<div class="m2"><p>تا ببزم عشق جانان جان ما را بار شد</p></div></div>