---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>ریزد مدام ساقی جانها شراب ناب</p></div>
<div class="m2"><p>در کام جان که مست خرابست در خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوید حریف ماشو و پیوسته باده نوش</p></div>
<div class="m2"><p>نظاره کن جمال دلفروز بی حجاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوشیم جامهای پیاپی ز دست دوست</p></div>
<div class="m2"><p>زان باده که نیست خمارش بهیچ باب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کوی زهد جان مرا چونکه راه نیست</p></div>
<div class="m2"><p>مائیم و بزم عشق و می و مطرب و رباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد اگر نعیم ابد میکنی طلب</p></div>
<div class="m2"><p>دوری مجو ز شاهد و پیمانه شراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان مست شد چنانکه نیامد دگر بهوش</p></div>
<div class="m2"><p>زان بحرهای می که بنوشید بیحساب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دو جهان کشید اسیری بجرعه</p></div>
<div class="m2"><p>کو مست تشنه بود و دو عالم شراب ناب</p></div></div>