---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>اسیر عشق تو از هردوکون آزاد است</p></div>
<div class="m2"><p>کسی که با غم تو مونس است دلشاد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه منع عاشق دیوانه میکنی زاهد</p></div>
<div class="m2"><p>بعشق دوست ندانم ترا چه واداداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن ملامت عاشق بعشق ورزیدن</p></div>
<div class="m2"><p>که کار عشق نه کسبی است بل خداداد است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بغمزه چشم تو بنیاد غارت جان کرد</p></div>
<div class="m2"><p>ز ترک مست نپرسی که این چه بنیاد است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درون خلوت جانم هزار گلزار است</p></div>
<div class="m2"><p>خیال روی تو آنجا چو رخت بنهادست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان ز تاب تجلی شدست غرقه نور</p></div>
<div class="m2"><p>مگر که یار ز رخسار پرده بگشادست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیک کرشمه ز عشاق جان و دل بربود</p></div>
<div class="m2"><p>بدلبری چه توان گفت کو چه استاد است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنوش باده عشق و بریش زهد بخند</p></div>
<div class="m2"><p>چه اعتبار بکار جهان که برباد است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همیشه کارتو سوزست و ناله و زاری</p></div>
<div class="m2"><p>اسیریا بخدا گو ترا چه افتادست</p></div></div>