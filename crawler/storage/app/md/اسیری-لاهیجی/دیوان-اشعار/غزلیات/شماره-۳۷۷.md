---
title: >-
    شمارهٔ ۳۷۷
---
# شمارهٔ ۳۷۷

<div class="b" id="bn1"><div class="m1"><p>بدلداری و دلجوئی درآمد عاقبت یارم</p></div>
<div class="m2"><p>بحمدالله ز دیدارش بسامان شد همه کارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازآن روزی که روی تو درآمد در نظر مارا</p></div>
<div class="m2"><p>بهشت و حور عین هرگز بدیده در نمی آرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا می لعل دلدارست و شاهد، ماه رخسارش</p></div>
<div class="m2"><p>که دارد در جهان باری چنین عیشی که من دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی بینی مگر زاهد چرا در ظلمت جهلی</p></div>
<div class="m2"><p>که عالم غرق نورآمد ز مهر روی دلدارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی محرم نمی بینم که گویم رازهای دل</p></div>
<div class="m2"><p>ز وصف حسن رخسارش ازآن گفتن نمی یارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه خرمن ها که جانم را شود محصول از وصلش</p></div>
<div class="m2"><p>چو دایم تخم عشق او بملک دل همی کارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیری از جفای او مکن زاری که ناگاهی</p></div>
<div class="m2"><p>کند آن بی‌وفا رحمی به آه و ناله زارم</p></div></div>