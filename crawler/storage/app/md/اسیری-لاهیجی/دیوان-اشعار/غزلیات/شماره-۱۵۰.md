---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>از شوق رخت در همه جا غلغله هست</p></div>
<div class="m2"><p>از زلف توبرجان جهان سلسله هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جور غم عشق تو در ملک دل و جان</p></div>
<div class="m2"><p>بس فتنه و آشوب و عجب زلزله هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر شاهد جانها بخرابات نیامد</p></div>
<div class="m2"><p>در مجلس مستان ز چه رو غلغله هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا مهر جمال تو بتابید بذرات</p></div>
<div class="m2"><p>پیوسته در آفاق جهان ولوله هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مفتی چه نشستی که از این کوچه تقلید</p></div>
<div class="m2"><p>تا منزل تحقیق بسی مرحله هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی پیر مرو راه طریقت که درین راه</p></div>
<div class="m2"><p>در هر قدمی واقعه هایله هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنها چه روی راه خطرناک اسیری</p></div>
<div class="m2"><p>هر دم چو ازین راه روان قافله هست</p></div></div>