---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>چه نسبت است به روی تو ماه تابان را</p></div>
<div class="m2"><p>به پیش مهر توان گفت ذره کیوان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظر بدیده ما گر کنی عیان بینی</p></div>
<div class="m2"><p>ز روی صورت و معنی جمال جانان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار خون بیکی غمزه گر بخواهد ریخت</p></div>
<div class="m2"><p>دهد لبش به شکرخنده جمله تاوان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جان کافر و مؤمن خیال زلف و رخش</p></div>
<div class="m2"><p>رقم نگر چو کشیدست کفر و ایمان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمال خویش چو می‌خواست تا نظاره کند</p></div>
<div class="m2"><p>ببین چه آینه ساخت نقش انسان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبیب بهر مداوای عشق کوشش کرد</p></div>
<div class="m2"><p>ندید درخور این درد هیچ درمان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیریا به جهان باز فتنه برخیزد</p></div>
<div class="m2"><p>اگر شراب ازین سان دهند مستان را</p></div></div>