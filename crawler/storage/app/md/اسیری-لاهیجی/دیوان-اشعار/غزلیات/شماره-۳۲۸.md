---
title: >-
    شمارهٔ ۳۲۸
---
# شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>زهی بزم و ساقی و حسن و جمال</p></div>
<div class="m2"><p>زهی مستی و ذوق و جام وصال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهی مطرب و چنگ و آواز نی</p></div>
<div class="m2"><p>زهی شورش و وجد ارباب حال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه رندان می نوش پرهیزکار</p></div>
<div class="m2"><p>چه عشاق جانباز نیکو خصال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرابات عشق است و رندان مست</p></div>
<div class="m2"><p>می و مطرب و شاهد بی مثال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه نور تجلی و سکر و فنا</p></div>
<div class="m2"><p>چه صحو و بقائی بحق لایزال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب سیر و طیری ز بالای عرش</p></div>
<div class="m2"><p>چه حالست حالات اهل کمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه رفتار و قامت چه زیبا نگار</p></div>
<div class="m2"><p>چه چشم است و ابرو چه غنج دلال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه رندان عاشق چه معشوق و می</p></div>
<div class="m2"><p>عجب عشق و حالی که ناید بقال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسیری ز عشقست جاهت بلند</p></div>
<div class="m2"><p>مبادا بجاهت زپستی زوال</p></div></div>