---
title: >-
    شمارهٔ ۴۰۰
---
# شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>ما حاوی سرکن فکانیم</p></div>
<div class="m2"><p>ما نسخه جامع جهانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسرار حروف دفتر کون</p></div>
<div class="m2"><p>از خط رخ نگار خوانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیرون ز احاطه جهانیم</p></div>
<div class="m2"><p>برتر ز زمین و از مکانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما مرکز دایره زمینیم</p></div>
<div class="m2"><p>ما محور دور آسمانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این جمله جهان مثال جسم است</p></div>
<div class="m2"><p>ما جان جهان جان جانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مائیم محیط هر دو عالم</p></div>
<div class="m2"><p>بنگر که چه بحر بیکرانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ملک فنا اگر گدائیم</p></div>
<div class="m2"><p>سلطان جهان جاودانیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن گنج نهان بما عیان شد</p></div>
<div class="m2"><p>ما خود بطلسم داستانیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از هر دو جهان کنار داریم</p></div>
<div class="m2"><p>با آنکه همیشه در میانیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون مظهر ظاهریم و باطن</p></div>
<div class="m2"><p>هم عین عیان و هم نهانیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ما مطلب نشان اسیری</p></div>
<div class="m2"><p>از نام و نشان چوبی نشانیم</p></div></div>