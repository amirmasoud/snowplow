---
title: >-
    شمارهٔ ۴۸۸
---
# شمارهٔ ۴۸۸

<div class="b" id="bn1"><div class="m1"><p>عاشقم برخط و خال و رخ مه سیمائی</p></div>
<div class="m2"><p>که بیک عشوه فریبد دل صد دانائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست در گردن جان سلسله زلف کسی</p></div>
<div class="m2"><p>که اسیرست بدامش دل هر شیدائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون توان دید جمال رخش امروز بنقد</p></div>
<div class="m2"><p>از پی نسیه چرا منتظر فردائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکند بار دگر دیده بطوبی نظری</p></div>
<div class="m2"><p>گر بجنت بخرامی بچنین بالائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچنان واله حسنش شده ام از دل و جان</p></div>
<div class="m2"><p>که ندارم بجهان هیچ بخود پروائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر گدائی که بکوی تو درآید روزی</p></div>
<div class="m2"><p>گشت در مملکت هر دو جهان دارائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مائی ما که اسیری به جهان قطره نمود</p></div>
<div class="m2"><p>غرقه در بحر شد و هست کنون دریائی</p></div></div>