---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>درد تو دوای دل و هم مرهم جانست</p></div>
<div class="m2"><p>دشنام تو بهتر ز دعای دگرانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطف است و وفا جور و جفایت بحقیقت</p></div>
<div class="m2"><p>جنگ تو بعاشق همه از صلح نشانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدم که رضایت همه در ناله و زاری است</p></div>
<div class="m2"><p>این ناله و افغان دلم از پی آنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک شمه ز حسن تو هرآنکس که به بیند</p></div>
<div class="m2"><p>دیوانه شد و در پی او خلق دوانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جلوه گری حسن رخت دیدم و گفتم</p></div>
<div class="m2"><p>این حسن نه حسن است که در حدبیانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیدا و نهان آینه روی تو دیدیم</p></div>
<div class="m2"><p>گر عالم جانست و گر ملک جهانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآتش عشق تو دل و جان اسیری</p></div>
<div class="m2"><p>بریان و کبابست چه گویم که چه سانست</p></div></div>