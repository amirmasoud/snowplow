---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>تا جان مرا شد بغم عشق تولا</p></div>
<div class="m2"><p>کردم ز قرار و خرد و صبر تبرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با چاشنی عشق برابر نتوان کرد</p></div>
<div class="m2"><p>نه آرزوی دنیی و نه لذت عقبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هر دو جهان از هوس دیدن رویت</p></div>
<div class="m2"><p>عاشق نکند جز بسر کوی تومأوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذرات دو عالم همه مست می عشقند</p></div>
<div class="m2"><p>در بزم شهود تو نه ادناست نه اعلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کعبه و بتخانه طلبکار تو بودم</p></div>
<div class="m2"><p>هرجا که شدم وصل تو بودست تمنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی آینه ادراک جمالت نتوان کرد</p></div>
<div class="m2"><p>آئینه رخسار تو شد دیده بینا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دل که نبیند ز جهان عکس جمالت</p></div>
<div class="m2"><p>در عالم تحقیق بود جاهل و اعمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مست می دیدار خدا را خبری نیست</p></div>
<div class="m2"><p>از آتش سوزان و نه از روضه و طوبی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ذره سرگشته دل و جان اسیری</p></div>
<div class="m2"><p>در پرتو خورشید جمالت شده شیدا</p></div></div>