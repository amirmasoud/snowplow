---
title: >-
    شمارهٔ ۴۰۶
---
# شمارهٔ ۴۰۶

<div class="b" id="bn1"><div class="m1"><p>ای عاشقان، ای عاشقان، من جسمهارا جان کنم</p></div>
<div class="m2"><p>ای عارفان، ای عارفان، جانها بحق جانان کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سالکان، ای سالکان، اندر پناه من شوید</p></div>
<div class="m2"><p>تا جغد را سربرکنم، شهباز در جولان کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای صادقان، ای صادقان، صدق آورید از جان و دل</p></div>
<div class="m2"><p>من درد را درمان کنم، من قطره را عمان کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بیدلان، ای بیدلان، محنت شد و شادی رسید</p></div>
<div class="m2"><p>من با شما آن شاه را هم عهد و هم پیمان کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای عالمان، ای عالمان، زین علمها جاهل شوید</p></div>
<div class="m2"><p>تا من بتعلیم خدا مشکل همه آسان کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای رهروان، ای رهروان، با ما روید این راه را</p></div>
<div class="m2"><p>تا من شما را از کرم ره بین و هم ره دان کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای کافران، ای کافران، نومید بودن شرط نیست</p></div>
<div class="m2"><p>من دیر را مسجد کنم، من کفر را ایمان کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای عابدان، ای عابدان، در خوف باشید و رجا</p></div>
<div class="m2"><p>هم عاصیان عابد کنم، طاعات را عصیان کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای منعمان، ای منعمان، دل در جهان بستن چرا</p></div>
<div class="m2"><p>من منعمان مفلس کنم، من سود را خسران کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای بیکسان، ای بیکسان، هستم چو غمخوار شما</p></div>
<div class="m2"><p>هم با شما مونس شوم، هم کار غمخواران کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای غافلان، ای غافلان، خوش از اسیری غافلید</p></div>
<div class="m2"><p>من کوه را صحرا کنم، صحرا همه بستان کنم</p></div></div>