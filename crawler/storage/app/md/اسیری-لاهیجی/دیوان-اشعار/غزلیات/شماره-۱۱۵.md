---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>با خیال زلف تو در خلوت تارم خوشست</p></div>
<div class="m2"><p>از صفای روی تو باروح انوارم خوشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلوت تاریک وصمت وجوع و بیداری شب</p></div>
<div class="m2"><p>با همه در بزم وصلت گر بود بارم خوشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی گل رخسار تو پیوسته روز و شب زغم</p></div>
<div class="m2"><p>همچو بلبل با فغان و ناله زارم خوشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکسی را در جهان باشد بچیزی میل دل</p></div>
<div class="m2"><p>جان ما را دایمابا درد دلدارم خوشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل فکارست و جگر خونست و دیده خون فشان</p></div>
<div class="m2"><p>این همه در آرزوی روی آن یارم خوشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با وجود تابش مهر جمال نوربخش</p></div>
<div class="m2"><p>گر بقید ظلمت زلفش گرفتارم خوشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون اسیری کفر و ایمان عکس زلف و روی اوست</p></div>
<div class="m2"><p>زان سبب با کعبه و با دیر و زنارم خوشست</p></div></div>