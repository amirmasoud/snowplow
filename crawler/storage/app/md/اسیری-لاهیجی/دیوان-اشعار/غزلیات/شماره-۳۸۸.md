---
title: >-
    شمارهٔ ۳۸۸
---
# شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>دردا که ز درد تو بدرمان نرسیدیم</p></div>
<div class="m2"><p>زین غم دل و جان رفت بجانان نرسیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار دویدیم بسر در پی مطلوب</p></div>
<div class="m2"><p>سر در طلبش رفت و بسامان نرسیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کوچه عشق تو همه عمر برفتیم</p></div>
<div class="m2"><p>آمد بسر این عمر و به پیشان نرسیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بادیه عشق تو سرگشته حیران</p></div>
<div class="m2"><p>چندانکه دویدیم بپایان نرسیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معروف بعرفان شده ام لیک چه حاصل</p></div>
<div class="m2"><p>در معرفت کنه تو ای جان نرسیدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتند فنا شو که رسیدی بحقیقت</p></div>
<div class="m2"><p>چون مائی ما رفت عجب زان نرسیدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بهر دل ریش اسیری همه عالم</p></div>
<div class="m2"><p>مرهم شد و زان نیز بدرمان نرسیدیم</p></div></div>