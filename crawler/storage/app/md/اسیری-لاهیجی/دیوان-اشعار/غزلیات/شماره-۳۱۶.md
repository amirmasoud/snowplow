---
title: >-
    شمارهٔ ۳۱۶
---
# شمارهٔ ۳۱۶

<div class="b" id="bn1"><div class="m1"><p>تا جامه هستی ز غم عشق نشد پاک</p></div>
<div class="m2"><p>در جستن معشوق نه عاشق چالاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا روح مجرد نشد از قید علایق</p></div>
<div class="m2"><p>کی همچو مسیحا بتوان رفت برافلاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی نور تجلی جمال تو توان دید</p></div>
<div class="m2"><p>تا لوح دل از نقش دو عالم نشود پاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من مست می عشق توام هرچه که هستم</p></div>
<div class="m2"><p>گر زاهد زراقم و گر عاشق بی باک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلدار در آید بنهد رخت اقامت</p></div>
<div class="m2"><p>چون خانه دل پاک شود از خس و خاشاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد بره عشق چو عاشق دل و جان، باز</p></div>
<div class="m2"><p>کی راست شود کار ز تسبیح و ز مسواک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شو پست و بجو منصب عالی چو اسیری</p></div>
<div class="m2"><p>تو مرکز دور فلکی چونکه شدی خاک</p></div></div>