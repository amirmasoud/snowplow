---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>روی تو به نقشهای محکم</p></div>
<div class="m2"><p>بنمود جمال خود بعالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر بکمال حسن خود را</p></div>
<div class="m2"><p>آورد عیان به نقش آدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرلحظه بجلوه نماید</p></div>
<div class="m2"><p>حسن رخ تو بچشم محرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن تو گرفت جمله آفاق</p></div>
<div class="m2"><p>شد ملک جهان ترا مسلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید رخت چو گشت تابان</p></div>
<div class="m2"><p>شد محو فنا جهان بیکدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمت بکرشمه می رباید</p></div>
<div class="m2"><p>جان و دل عاشقان دمادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آینه روی خود عیان دید</p></div>
<div class="m2"><p>آورد بخویش عشق محکم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بازد همه روزه عشق با خود</p></div>
<div class="m2"><p>خود بود بخویش یار و همدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آوازه ز عاشق و ز معشوق</p></div>
<div class="m2"><p>انداخت بکاینات هر دم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس نیست درین میان بجز یار</p></div>
<div class="m2"><p>محبوب و محبت و محب هم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان دم که جمال دوست دیدیم</p></div>
<div class="m2"><p>شادیم اسیریا و بی غم</p></div></div>