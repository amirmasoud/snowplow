---
title: >-
    شمارهٔ ۴۶۵
---
# شمارهٔ ۴۶۵

<div class="b" id="bn1"><div class="m1"><p>ای جمال روی تو خورشید تابان آمده</p></div>
<div class="m2"><p>وی دو زلف مشکبویت عنبرافشان آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شعاع روی تو دل واله و حیران شده</p></div>
<div class="m2"><p>جان بسودای سر زلفت پریشان آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خم هر موی تو پیداست زنار و صلیب</p></div>
<div class="m2"><p>زلف و رویت جان ما را کفر و ایمان آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آتش شوقت دلم پیوسته در سوز و گداز</p></div>
<div class="m2"><p>داغ بیحد از غم عشق تو برجان آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق ورزی بین که هر دم یارباما می کند</p></div>
<div class="m2"><p>گه شده پیدا جمالش گاه پنهان آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نقاب جمله ذرات جهان دیدم عیان</p></div>
<div class="m2"><p>مهر حسن روی او چون ماه تابان آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فکر زلف یار جانرا دایما همدم شده</p></div>
<div class="m2"><p>مونس دل ذکر حسن روی جانان آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان مشتاق لقارا سوز شوقش مرهم است</p></div>
<div class="m2"><p>عاشقانرا درد عشقش عین درمان آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمله ذرات جهان درپرتو مهر رخش</p></div>
<div class="m2"><p>چون اسیری دایما شیدا و حیران آمده</p></div></div>