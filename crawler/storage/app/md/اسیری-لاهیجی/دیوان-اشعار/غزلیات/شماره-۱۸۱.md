---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>عاشق بکوی عشق چو خود را فدی کند</p></div>
<div class="m2"><p>معشوقش از خودی خود او را خودی کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لحظه هست و نیست شود نفس کاینات</p></div>
<div class="m2"><p>فیض خدا چو هرنفس آمد شدی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روشن شود ز پرتو رخسار او جهان</p></div>
<div class="m2"><p>حسن رخش چو جلوه گری ابتدی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافل مشو ز یار و تعالوا شنو خطاب</p></div>
<div class="m2"><p>هر سوت چو(ن) منادی غیب این ندی کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیکی ندید در دو جهان از خدا و خلق</p></div>
<div class="m2"><p>هرکو به ره روان ره حق بدی کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یابی سعادت ابدی از وصال دوست</p></div>
<div class="m2"><p>گر زانکه دولت ازلی آمدی کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد مقتدا بمملکت عشق اسیریا</p></div>
<div class="m2"><p>هرکو بعاشقان خدااقتدی کند</p></div></div>