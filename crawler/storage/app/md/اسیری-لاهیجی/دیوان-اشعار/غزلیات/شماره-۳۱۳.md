---
title: >-
    شمارهٔ ۳۱۳
---
# شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>چونکه شهانند گدایان عشق</p></div>
<div class="m2"><p>باش دلا چاکر سلطان عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دفتر ناموس بآتش بسوز</p></div>
<div class="m2"><p>تا که رهی یابی بدیوان عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ره بسر خوان وصالش بری</p></div>
<div class="m2"><p>گر تو شوی یکدمه مهمان عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زود شوی واقف اسرار غیب</p></div>
<div class="m2"><p>گر بزنی دست بدامان عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موت ارادی چو شود اختیار</p></div>
<div class="m2"><p>زود شوی زنده بجانان عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مملکت عشق مسلم مراست</p></div>
<div class="m2"><p>زانکه منم خسرو و خاقان عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا که مسخر شودم دیو نفس</p></div>
<div class="m2"><p>گشت دلم تخت سلیمان عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل، تو دور از من و بیگانه باش</p></div>
<div class="m2"><p>زانکه منم محرم خاصان عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهی اسیری بوصالش رسی</p></div>
<div class="m2"><p>زود به پیمای بیابان عشق</p></div></div>