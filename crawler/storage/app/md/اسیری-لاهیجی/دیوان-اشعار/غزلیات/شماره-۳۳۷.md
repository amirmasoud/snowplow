---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>ما سال‌ها به کوی ملامت دویده‌ایم</p></div>
<div class="m2"><p>راه سلامت همچو ملامت ندیده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرص و امل چو رهزن راه طریقتند</p></div>
<div class="m2"><p>ما رخت دل به ملک قناعت کشیده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مفتی اگر ز راه خرد می‌رسد به جان</p></div>
<div class="m2"><p>ما از طریق عشق به جانان رسیده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جام وصل مست مدامند جان و دل</p></div>
<div class="m2"><p>تا ما به کوی میکده خلوت گزیده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عقل و زهد خشک بشستیم دست دل</p></div>
<div class="m2"><p>تا جرعه ز باده عشقش چشیده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آشفته گشته‌ایم و پریشان و بی‌قرار</p></div>
<div class="m2"><p>تا از نسیم زلف تو بویی شنیده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرآت روی اوست اسیری همه جهان</p></div>
<div class="m2"><p>ما عکس حسنش از همه ذرات دیده‌ایم</p></div></div>