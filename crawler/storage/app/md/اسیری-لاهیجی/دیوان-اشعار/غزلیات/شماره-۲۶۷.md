---
title: >-
    شمارهٔ ۲۶۷
---
# شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>فوحدته فی کثرة الکون ظاهر</p></div>
<div class="m2"><p>وآیاته عن شبهة الشبه طاهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان ظن ذوجهل بانک غایب</p></div>
<div class="m2"><p>فحققت ها انتم مع الکل حاضر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظهورک فی کل المظاهر ماخفی</p></div>
<div class="m2"><p>ولکن حجاب الکون وجهک ساتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان لم یرالمحجوب وجه حبیبنا</p></div>
<div class="m2"><p>الی حسنه عین المحبین ناظر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظهرنا بنور ساطع من جماله</p></div>
<div class="m2"><p>وها وجهه شمس و نحن ذرایر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وجود جمیع الکاینات مسبح</p></div>
<div class="m2"><p>علی ای حال شاکر لک ذاکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ونار هواکم احرقت بقلوبنا</p></div>
<div class="m2"><p>وارواحنا فی نور غرک حایر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فکشف جمال الوجه افنی مظاهرا</p></div>
<div class="m2"><p>لان تجلی الذات للکل قاهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فقد نورت عین الاسیری بنوره</p></div>
<div class="m2"><p>لذلک لایخفی علیه السرایر</p></div></div>