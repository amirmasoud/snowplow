---
title: >-
    شمارهٔ ۴۰۱
---
# شمارهٔ ۴۰۱

<div class="b" id="bn1"><div class="m1"><p>من که پیوسته بروی تو چنین حیرانم</p></div>
<div class="m2"><p>صفت حسن تو چون گویم و کی بتوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرت عشق مرا بیخبر از من دارد</p></div>
<div class="m2"><p>من بدین حال کجا حال دگر میدانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقان گر بره عشق نهادند قدم</p></div>
<div class="m2"><p>من درین ره بهوای تو بسرگردانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهدا پیش تو گر عیب نماید رندی</p></div>
<div class="m2"><p>من بصدق دل و جان خاک ره رندانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست مارا نفسی بی رخ تو صبرو قرار</p></div>
<div class="m2"><p>من بدین شوق چه موقوف قیامت مانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرستانم ز لب لعل تو داد دل خود</p></div>
<div class="m2"><p>چون خضر زنده جاوید بماند جانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>(چه شوی رنجه اسیری بمداوای دلم)</p></div>
<div class="m2"><p>(غیر دردش چو نسازد بجهان درمانم)</p></div></div>