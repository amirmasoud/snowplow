---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>مائیم بعشق تو میان بسته بزنار</p></div>
<div class="m2"><p>ترسا صفت از غیر تو کلی شده بیزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آزاده ز قید غم دنیا و ز دینم</p></div>
<div class="m2"><p>در دام کمند سر زلف تو گرفتار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جام می عشق چنان مست و خرابم</p></div>
<div class="m2"><p>کز بیخبری می نشناسم سرود دستار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفت از سرمن فکر و خیال خرد و صبر</p></div>
<div class="m2"><p>تا گشت دلم از می عشق تو خبردار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جویای می و شاهدم ای پیر خرابات</p></div>
<div class="m2"><p>بنما بمن از راه کرم خانه خمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رندیم و خراباتی و قلاش و نظرباز</p></div>
<div class="m2"><p>درباخته در کوی فنا هستی و پندار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هستی جهان نیست بجز وهم وخیالی</p></div>
<div class="m2"><p>عارف شو و از جمله جهان روی بحق آر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن یار بنقش همه اغیار برآمد</p></div>
<div class="m2"><p>دیار درین دار مجوئید بجز یار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صدبار بهر لحظه اگر رو بنماید</p></div>
<div class="m2"><p>برحسن دگر جلوه کند یار بهر بار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اسرار حقیقت بجهان هرکه کند فاش</p></div>
<div class="m2"><p>منصور صفت زود برآید بسردار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از باده اطلاق اسیری چو بنوشید</p></div>
<div class="m2"><p>آزاده ز قید دو جهان گشت بیکبار</p></div></div>