---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>از قید غم جهان شد آزاد</p></div>
<div class="m2"><p>هر کو دل و جان بعشق او داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برجان خراب عشق بازان</p></div>
<div class="m2"><p>تا چند کند جفا و بیداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد نوبت وصل و هجر بگذشت</p></div>
<div class="m2"><p>وارستم ازین غم و شدم شاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی بهره بود ز عاشقانت</p></div>
<div class="m2"><p>زاهد که کند ز عشق واداد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز ز کمند زلف جانان</p></div>
<div class="m2"><p>جان و دل ما نگشت آزاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد عین شراب آخر کار</p></div>
<div class="m2"><p>آن دل که ز جرعه زدی داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معشوقه پرست شد اسیری</p></div>
<div class="m2"><p>از زهد دگر کجا کند یاد</p></div></div>