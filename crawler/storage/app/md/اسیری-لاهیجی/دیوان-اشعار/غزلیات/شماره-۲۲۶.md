---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>ز شوقت جمله عالم بیقرارند</p></div>
<div class="m2"><p>همه مشتاق دیدار نگارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه مست می شوق آنچنانند</p></div>
<div class="m2"><p>که بی تو نه قرار و صبر دارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملایک از می دیدار مستند</p></div>
<div class="m2"><p>زمین و آسمان حیران یارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه مست ازمی وصلند بیخود</p></div>
<div class="m2"><p>ولی با خود ز هجران در خمارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان در نور روی یار محواند</p></div>
<div class="m2"><p>که خود را او و او را خود شمارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حریم خانه دل جز خیالت</p></div>
<div class="m2"><p>چه محرومان که محرم می گذارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیری عاشقان مست دیدار</p></div>
<div class="m2"><p>ز لذات دو عالم یاد نارند</p></div></div>