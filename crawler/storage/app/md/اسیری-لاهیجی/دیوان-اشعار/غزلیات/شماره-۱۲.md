---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>گر وصال دوست میخواهی دلا</p></div>
<div class="m2"><p>از مقام کفر و ایمان برتر آ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو بیگانه نخواهی شد ز خود</p></div>
<div class="m2"><p>با خدای خود نگردی آشنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاکباز و پاک رو گر نیستی</p></div>
<div class="m2"><p>کی تو برخوردار باشی از لقا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با می و معشوق باشد هم نفس</p></div>
<div class="m2"><p>هرکه شد خاک ره رندان چو ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقی کو واصل معشوق شد</p></div>
<div class="m2"><p>فاش میگوید و من اهوی انا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش درآ در وادی ایمن دمی</p></div>
<div class="m2"><p>می شنو انی اناالله موسیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وادی ایمن چه باشد طور عشق</p></div>
<div class="m2"><p>که درو نور تجلیست از خدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو عیسی چون برآیی بر فلک؟</p></div>
<div class="m2"><p>گر نخواهی شد مجرد از هوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بقای جاودان خواهی بحق</p></div>
<div class="m2"><p>بایدت اول ز خود گشتن فنا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز فنا اندر فنا در راه عشق</p></div>
<div class="m2"><p>من ندیدم درد عاشق را دوا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منکر حال اسیری کی شود</p></div>
<div class="m2"><p>هر که دارد از خدا ایمان عطا</p></div></div>