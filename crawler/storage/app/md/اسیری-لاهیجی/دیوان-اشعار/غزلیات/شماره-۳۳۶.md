---
title: >-
    شمارهٔ ۳۳۶
---
# شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>ای کنایت ز سر کوی تو جنات نعیم</p></div>
<div class="m2"><p>شد اشارت بفراق رخ تو نار جحیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشک فردوس برین است و نعیم ابدی</p></div>
<div class="m2"><p>دوزخ ما که در آنجاست بما یار ندیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست از مهر رخت در دل هر ذره نشان</p></div>
<div class="m2"><p>یافت جان همه از نکهت زلف تو نسیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل پاک توان دید جمال تو عیان</p></div>
<div class="m2"><p>زانکه آوینه روی تو بود قلب سلیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فارغ از دعوی برهان حدوث و قدم است</p></div>
<div class="m2"><p>جان عارف که بود محرم اسرار قدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست و بیخود ز می و وصل تو ذرات دو کون</p></div>
<div class="m2"><p>همه از فیض تو با بهره زهی فیض عمیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه تجلی جمالی کند و گاه جلال</p></div>
<div class="m2"><p>چون کنم کین دل دیوانه نگردد بدو نیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر جفا می کند آن یار و اگر مهر و وفا</p></div>
<div class="m2"><p>چاره ماچه بود غیر رضا و تسلیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای اسیری ز خدا غیر خدا هیچ مجو</p></div>
<div class="m2"><p>بهر حق عابد حق شو نه ز امید و ز بیم</p></div></div>