---
title: >-
    شمارهٔ ۳۳۱
---
# شمارهٔ ۳۳۱

<div class="b" id="bn1"><div class="m1"><p>چون خیال وصل جانان هست سودای محال</p></div>
<div class="m2"><p>جان شیدایم ز وصلش گشت قانع با خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ز هستی هست باقی یکسر مو وصل نیست</p></div>
<div class="m2"><p>نیست از هستی خودشو گر همی خواهی وصال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان جان و جانان پرده جز پندار نیست</p></div>
<div class="m2"><p>چونکه پندارت نماند هر دو دارند اتصال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر شود صافی ز زنگ غیر مرآت دلت</p></div>
<div class="m2"><p>می توان دیدن عیان از دیده جان آن جمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال اهل دل طلب مفتی، که تا عارف شوی</p></div>
<div class="m2"><p>راز عرفان کی شود حاصل ز راه قیل و قال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامن صاحب کمالی گیر و میرو در رهش</p></div>
<div class="m2"><p>گر همی خواهی که یابی حال ارباب کمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بکنه حسن او کس را اسیری ره نبود</p></div>
<div class="m2"><p>در بیان وصف او زان رو زبانها گشت لال</p></div></div>