---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>چو نار هجر او جان میگدازد</p></div>
<div class="m2"><p>زلال وصل کو تا دل نوازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخط عنبرین و خال مشکین</p></div>
<div class="m2"><p>لباس دلبری را می طرازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قبای دلبری و خوبی امروز</p></div>
<div class="m2"><p>بقد همچو سروش می برازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رباید گوی محبوبی ز خوبان</p></div>
<div class="m2"><p>چو رخش حسن در میان بتازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گنج وصل او کی راه یابد</p></div>
<div class="m2"><p>طلسم هستی هر کو در نبازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی کو جان خود را باخت در عشق</p></div>
<div class="m2"><p>میان عاشقان او سر فرازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیری را بغیر از درد عشقش</p></div>
<div class="m2"><p>بجان او دگر درمان نسازد</p></div></div>