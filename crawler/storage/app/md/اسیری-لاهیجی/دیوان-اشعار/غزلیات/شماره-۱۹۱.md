---
title: >-
    شمارهٔ ۱۹۱
---
# شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>هر لحظه بروئی دگر آن روی نماید</p></div>
<div class="m2"><p>هر دم بمن از باب دگر یار درآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا بدل پاک نظر کن رخ او بین</p></div>
<div class="m2"><p>آئینه صافی چو همه روی نماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اعمی نتواند که به بیند مه رویت</p></div>
<div class="m2"><p>جز دیده بینا بجمال تو نشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تاب جمال تو شود محو دو عالم</p></div>
<div class="m2"><p>چون روی تو از پرده پندار برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز غمزه جادوی تو جان و دل و(د)ینم</p></div>
<div class="m2"><p>ای شوخ جفا پیشه نگویی که رباید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندانکه بجانم غم عشق تو فزون است</p></div>
<div class="m2"><p>شادی دل عاشق دیوانه فزاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق چه کند گر نکند جامه بصد چاک</p></div>
<div class="m2"><p>مطرب چو سرود غم عشق تو سراید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راضی به قضا باش و ز غم فارغ واز او</p></div>
<div class="m2"><p>زیرا غم و شادی جهان هیچ نپاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دام بلا جان اسیری شود آزاد</p></div>
<div class="m2"><p>زان زلف معنبر چو گره باز گشاید</p></div></div>