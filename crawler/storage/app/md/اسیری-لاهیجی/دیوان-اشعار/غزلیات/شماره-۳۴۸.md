---
title: >-
    شمارهٔ ۳۴۸
---
# شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>شهباز لامکانم من در مکان نگنجم</p></div>
<div class="m2"><p>عنقای بی نشانم اندر نشان نگنجم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بحر بیکرانم مقصود کن فکانم</p></div>
<div class="m2"><p>من جان جان جانم اندر جهان نگنجم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از خودی فنایم من گوهر بقایم</p></div>
<div class="m2"><p>من در بی بهایم در بحر و کان نگنجم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی جای و آشیانم بی نام و بی نشانم</p></div>
<div class="m2"><p>برتر ز جسم و جانم در جسم و جان نگنجم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من عین عشق و ذوقم سرمست جام شوقم</p></div>
<div class="m2"><p>از چرخ و عرش فوقم در آسمان نگنجم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اعجوبه جهانم بالاتر از بیانم</p></div>
<div class="m2"><p>من گنج بس نهانم اندر عیان نگنجم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فارغ ز کفر و دینم با دوست همنشینم</p></div>
<div class="m2"><p>من نور هر یقینم اندر گمان نگنجم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من مست ذوق و حالم حیران آن جمالم</p></div>
<div class="m2"><p>مستغرق وصالم در خود ازآن نگنجم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم عشق با صفایم هم عقل با وفایم</p></div>
<div class="m2"><p>هم ارض و هم سمایم اندر بیان نگنجم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مست می جنونم در عشق ذوفنونم</p></div>
<div class="m2"><p>از عقلها برونم در داستان نگنجم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باری اسیریا گو چونست کان پری رو</p></div>
<div class="m2"><p>دارد کنار با تو من در میان نگنجم</p></div></div>