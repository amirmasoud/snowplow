---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>اگر از چهره ذاتش برافتد برده اسما</p></div>
<div class="m2"><p>ز تاب پرتو حسنش فنا گردد همه اشیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جام باده عشقش همه ذرات سرمستند</p></div>
<div class="m2"><p>گرفت آفاق ازین معنی سراسر فتنه و غوغا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان یک باده با هر کس دهد ساقی ز دیگر جام</p></div>
<div class="m2"><p>کند وامق ز عذرا مست و مجنون از رخ لیلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تجلی میکند هر دم بعالم شاهد حسنش</p></div>
<div class="m2"><p>اگر دیدار میخواهی بیاور دیده بینا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو از رخ پرده بردارد جمال خود بیاراید</p></div>
<div class="m2"><p>همه اعضای من گردد سراسر دیده سرتاپا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که تا هر جا بهر عاشق نماید شیوه دیگر</p></div>
<div class="m2"><p>گهی مسجد کند منزل گهی میخانه را مأوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حسن تازه هر ساعت نماید یار دیداری</p></div>
<div class="m2"><p>از آن رو دایما بودست اسیری واله و شیدا</p></div></div>