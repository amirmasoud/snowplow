---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>با روی تو از جنت اعلی نتوان گفت</p></div>
<div class="m2"><p>با قد تو از قامت طوبی نتوان گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با کفر خم زلف تو ترسا صفتان را</p></div>
<div class="m2"><p>جز قصه زنار و چلیپانتوان گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مجلس رندان خراباتی بی باک</p></div>
<div class="m2"><p>جز ذکر می و شاهد رعنا نتوان گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرآت جمال رخ جانان بحقیقت</p></div>
<div class="m2"><p>جز جان و دل پاک مصفا نتوان گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ما و منی پاک شو و وصل طلب کن</p></div>
<div class="m2"><p>در مجلس وصلش سخن از ما نتوان گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پنهان ز همه راز دل ای جان و جهانم</p></div>
<div class="m2"><p>گوئیم بتو گر چه هویدا نتوان گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سردهنش هیچ نگفتیم بزاهد</p></div>
<div class="m2"><p>با جاهل کج فهم معما نتوان گفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خلق مکن فاش دلا سرحقیقت</p></div>
<div class="m2"><p>با غیر خدا سر خدا را نتوان گفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با آن لب جانبخش اسیری که تو دانی</p></div>
<div class="m2"><p>افسانه افسون مسیحا نتوان گفت</p></div></div>