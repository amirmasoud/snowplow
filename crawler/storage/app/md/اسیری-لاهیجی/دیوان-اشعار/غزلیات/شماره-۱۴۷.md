---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>گشت تابان مهر ذاتش از صفات</p></div>
<div class="m2"><p>وز صفاتش گشت روشن کاینات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ندیدی پرتو روی حبیب</p></div>
<div class="m2"><p>از چه کردی سجده کافر پیش لات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده باطن گشا ظاهر ببین</p></div>
<div class="m2"><p>مظهر ذات و صفاتش ممکنات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی جهت بر دل تجلی کرد یار</p></div>
<div class="m2"><p>چون گذشتم از مکان و از جهات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لذتی کردم ز موت اختیار</p></div>
<div class="m2"><p>چون بدیدم خوش حیاتی در ممات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون توانم کرد وصف روی او</p></div>
<div class="m2"><p>کی درآید کنه حسنش در صفات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون اسیری دید نور ذات او</p></div>
<div class="m2"><p>یافت از قید خودی کلی نجات</p></div></div>