---
title: >-
    شمارهٔ ۲۶۶
---
# شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>هان ای صبا ز لطف بشیراز کن گذر</p></div>
<div class="m2"><p>زین جان بی نوا برجانان پیام بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان مبتلای محنت غربت ز اشتیاق</p></div>
<div class="m2"><p>دارد دلی پرآتش و پیوسته دیده تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوید دعا بصدق دل و از سر نیاز</p></div>
<div class="m2"><p>دارد امید فاتحه هر شام و هر سحر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای سالکان راه بهمت مدد دهید</p></div>
<div class="m2"><p>باشد رسیم باز بدیدار یکدگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هستم امیدوار بلطفش که عاقبت</p></div>
<div class="m2"><p>بینم جمال روضه آن سیدالبشر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بار غم فراق عزیزان و رنج راه</p></div>
<div class="m2"><p>برجان کشم ببوی وصالش درین سفر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذر اسیریا بره عشق او ز بیم</p></div>
<div class="m2"><p>هر جا روی عنایت حق است راهبر</p></div></div>