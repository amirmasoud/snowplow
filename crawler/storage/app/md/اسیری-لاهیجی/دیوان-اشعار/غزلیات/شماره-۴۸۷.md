---
title: >-
    شمارهٔ ۴۸۷
---
# شمارهٔ ۴۸۷

<div class="b" id="bn1"><div class="m1"><p>ز تاب آتش شوقت شدم مجنون و شیدایی</p></div>
<div class="m2"><p>چه باشد گر بمشتاقان جمال خویش بنمایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیکدم شور و غوغای قیامت در جهان گیرد</p></div>
<div class="m2"><p>چنین سرمست از خلوت بصحرا گر برون آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل دیوانه از رویت نمیدانم چه می بیند</p></div>
<div class="m2"><p>که چون زلف پریشانت ز عشقش گشت سودایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو هر جا حسن رخسارت دگرگون جلوه بنمودست</p></div>
<div class="m2"><p>دل دیوانه عاشق ازآن گشتست هر جایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل و جان اسیران را ز دام غم کنی آزاد</p></div>
<div class="m2"><p>بشوخی گرنقاب زلف از رخسار بگشایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان آئینه عشقست اگر صورت و گر معنی</p></div>
<div class="m2"><p>دو عالم مست این جامند چه پنهان و چه پیدایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز نام زهد و هشیاری اسیری ننگ میدارد</p></div>
<div class="m2"><p>چرا کو شهره شهرست در مستی و رسوایی</p></div></div>