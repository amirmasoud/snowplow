---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>یارم چو ز رخ نقاب بگشود</p></div>
<div class="m2"><p>اسرار دو کون فاش بنمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار است عیان بصورت کون</p></div>
<div class="m2"><p>این نقش جهان نمود بی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد نقش دوئی خیال احول</p></div>
<div class="m2"><p>چون غیر یکی نبود موجود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ما نظری بوام بستان</p></div>
<div class="m2"><p>وانگاه نگر بروی مقصود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر ذره که در جهان هستی است</p></div>
<div class="m2"><p>آئینه مهر روی او بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مائی و توئی به اوست پیدا</p></div>
<div class="m2"><p>پیوسته بهم شد آتش و دود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذر ز خود و بجو خدا را</p></div>
<div class="m2"><p>زنهار مجو زیان بی سود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارد خبری ز سوز عشقش</p></div>
<div class="m2"><p>این ناله چنگ و بربط و عود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بزم وصال او اسیری</p></div>
<div class="m2"><p>از خویش فنا شد و بیاسود</p></div></div>