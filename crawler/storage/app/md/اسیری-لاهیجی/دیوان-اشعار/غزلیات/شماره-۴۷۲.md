---
title: >-
    شمارهٔ ۴۷۲
---
# شمارهٔ ۴۷۲

<div class="b" id="bn1"><div class="m1"><p>ساقی باقی مرا جامی بده</p></div>
<div class="m2"><p>از لب لعل خودم کامی بده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم شوخت را ز بهر صید جان</p></div>
<div class="m2"><p>از دو زلف عنبرین دامی بده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از رخ چون روز و زلف همچو شب</p></div>
<div class="m2"><p>جان ما را صبحی و شامی بده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژده وصلش اگر داری صبا</p></div>
<div class="m2"><p>پیش این مهجور پیغامی بده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان که شد از درد هجرت مضطرب</p></div>
<div class="m2"><p>برامید وصل آرامی بده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من دعای جان تو گویم مدام</p></div>
<div class="m2"><p>تو عوض یکبار دشنامی بده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی سروسامان شدم در عشق تو</p></div>
<div class="m2"><p>کار عاشق را سرانجامی بده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق مارا نیست آغازی پدید</p></div>
<div class="m2"><p>چون نبود آغاز انجامی بده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد اسیری مست چشم سرخوشت</p></div>
<div class="m2"><p>از می لعلت ورا جامی بده</p></div></div>