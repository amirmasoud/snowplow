---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>برابر طور عشق ای دل ببین نور تجلی را</p></div>
<div class="m2"><p>که تا بیخود شوی از خود بدانی طور و موسی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا دعوت مکن واعظ بحوران از قصور خود</p></div>
<div class="m2"><p>که ما دیدار میخواهیم نه دنیا(و) نه عقبی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا گردیده مجنون نباشد کی توانی دید</p></div>
<div class="m2"><p>شعاع پرتو حسن جهان افروز لیلی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نداری دیده معنی، ندیدی زان، مه رویش</p></div>
<div class="m2"><p>ز حسن صورت یوسف نباشد بهره اعمی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو گشتم عاشق صادق بمعشوق خراباتی</p></div>
<div class="m2"><p>گرفتم جام می بر کف، فکندم زهد و تقوی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنزد جان مشتاقان نباشد هیچ مقداری</p></div>
<div class="m2"><p>به پیش قدورخسارش دلا فردوس و طوبی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو عالم صورت و معنی جمال نوربخش او</p></div>
<div class="m2"><p>ز صورت بگذر ار خواهی اسیری حسن معنی را</p></div></div>