---
title: >-
    شمارهٔ ۳۳۵
---
# شمارهٔ ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>همه صورت همه معنی همه حسنی و جمال</p></div>
<div class="m2"><p>همه حشمت همه رفعت همگی جاه و جلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه لطفی و ملاحت همه احسان و کرم</p></div>
<div class="m2"><p>همه اخلاق جمیل و همه اطوار کمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه قربت همه وحدت همگی کشف و شهود</p></div>
<div class="m2"><p>همه دانش همه بینش همه وجد و همه حال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همگی سکر و فنائی همه تمکین و بقا</p></div>
<div class="m2"><p>همه انوار تجلی همگی ذوق وصال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه عشقی همه عاشق همه معشوق و ناز</p></div>
<div class="m2"><p>همگی عشوه و جلوه همه غنجی و دلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه کانی همه گوهر همه دری و صدف</p></div>
<div class="m2"><p>همه دریای محیط و همگی آب زلال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه قیدی و اسیری همه اطلاق و عنا</p></div>
<div class="m2"><p>همه اسرار حقیقت نه خیالات محال</p></div></div>