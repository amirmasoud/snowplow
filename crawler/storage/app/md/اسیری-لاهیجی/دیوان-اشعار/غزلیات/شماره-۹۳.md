---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>ای جمله جهان شیفته حسن و جمالت</p></div>
<div class="m2"><p>جان و دل عشاق اسیر خط و خالت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر تراش دل و دین تاختن آرد</p></div>
<div class="m2"><p>در مملکت جان همه دم خیل خیالت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را ز خمار غم هجران خبری نیست</p></div>
<div class="m2"><p>چون مست مدامم ز می جام وصالت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگرفت جمال تو بدعوی همه آفاق</p></div>
<div class="m2"><p>در حسن و ملاحت بجهان نیست مثالت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کشتن عشاق نمایی ید بیضا</p></div>
<div class="m2"><p>خون همه گویی که بفتوی است حلالت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دادند بهر کس ز ازل قسمت و بخشی</p></div>
<div class="m2"><p>زان روز دلم را غم عشق است حوالت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون شاهد حسن تو ز رخ پرده برانداخت</p></div>
<div class="m2"><p>شد محو فنا جان اسیری ز جمالت</p></div></div>