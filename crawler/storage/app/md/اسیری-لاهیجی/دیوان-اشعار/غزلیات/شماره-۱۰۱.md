---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>دل ز بند غم دمی آزاد نیست</p></div>
<div class="m2"><p>بی غم عشق تو جانم شاد نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاقت تاب غم عشق از کجاست</p></div>
<div class="m2"><p>جان و دل راگر ز توامداد نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچنانم واله حسن رخت</p></div>
<div class="m2"><p>کز جهان و جان دلم را یاد نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راز عشق از عاشقان باید شنید</p></div>
<div class="m2"><p>زانکه سر عشق بازهاد نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز جورت میخورد خون جگر</p></div>
<div class="m2"><p>جان ما را زهره فریاد نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دادما ندهد ز جور عشق یار</p></div>
<div class="m2"><p>همچو من کس عاشق بیداد نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاد راهم درد و سوز و زاریست</p></div>
<div class="m2"><p>عاشقان را غیر ازین خود زاد نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهد از منعت کند از عاشقی</p></div>
<div class="m2"><p>گو طریق عشق را واداد نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه شمشاد ای اسیری دلرباست</p></div>
<div class="m2"><p>همچو سرو قامتش آزاد نیست</p></div></div>