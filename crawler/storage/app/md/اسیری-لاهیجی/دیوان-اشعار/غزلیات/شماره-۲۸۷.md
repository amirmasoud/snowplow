---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>دلدار برگرفت نقاب از جمال خویش</p></div>
<div class="m2"><p>با عاشقان نمود رخ بی مثال خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون حسن خود بدید در آئینه شد چنین</p></div>
<div class="m2"><p>آشفته کرشمه و غنج و دلال خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای آفتاب حسن چو ما ذره توایم</p></div>
<div class="m2"><p>یارب مساز کم ز سرما ظلال خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نور مهر روی تو عالم منور است</p></div>
<div class="m2"><p>اظهار کرده به دو عالم کمال خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشاق را به آتش هجران بسوختی</p></div>
<div class="m2"><p>برسوخته بریز زلال وصال خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگشا نقاب زلف بروز آرشام را</p></div>
<div class="m2"><p>دیوانه ساز خلق جهان از جمال خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود را به پیش یار اسیری نثار کن</p></div>
<div class="m2"><p>در بزم وصل او چو نداری مجال خویش</p></div></div>