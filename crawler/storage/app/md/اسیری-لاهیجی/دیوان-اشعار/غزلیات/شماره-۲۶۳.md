---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>ای دل براه عشق ز کونین در گذر</p></div>
<div class="m2"><p>گر زانک عاشقی ز سر جان و سرگذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی ببزمگاه وصالت دهند راه</p></div>
<div class="m2"><p>از جان و دل ز جمله جهان پیشتر گذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردانه گر براه طریقت قدم نهی</p></div>
<div class="m2"><p>ز امید باغ جنت و بیم سقرگذر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با عقل کی بمنزل وصلش توان رسید</p></div>
<div class="m2"><p>شو مست جام عشق و ز ره بیخبر گذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داری هوس که شاهد جان رو نمایدت</p></div>
<div class="m2"><p>بیخود بکوی میکده کن یک سحر گذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانم نیاورد بنظر مهر و ماه را</p></div>
<div class="m2"><p>تا روی جان فزای ترا دید در گذر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باید که ره بسر اسیری بری چو ما</p></div>
<div class="m2"><p>اول قدم برو ز سرجاه و زر گذر</p></div></div>