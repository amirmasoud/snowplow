---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>رند و قلاشیم و مست و می پرست</p></div>
<div class="m2"><p>از شراب عشق تو رفته ز دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معتکف در کعبه و مسجد بدم</p></div>
<div class="m2"><p>در خراباتم کنون افتاده مست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بمعشوقی کنی اظهار ناز</p></div>
<div class="m2"><p>از نیاز عاشقان عالم پرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمله عالم غرق بحر وحدتند</p></div>
<div class="m2"><p>از زمین و آسمان بالا و پست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود عالم جز بهستی تو نیست</p></div>
<div class="m2"><p>دایما از فیض عامت نیست هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش بند ما مثال خویش خواست</p></div>
<div class="m2"><p>صورت آدم کشید و نقش بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شراب وصل تو مست مدام</p></div>
<div class="m2"><p>چون اسیری گبر و ترسا بت پرست</p></div></div>