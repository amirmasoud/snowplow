---
title: >-
    شمارهٔ ۲۸۲
---
# شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>محرم بزم وصالت کی شود هر بوالهوس</p></div>
<div class="m2"><p>درخور این شیوه جان پاکبازانست و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز خیال زلف و رویت جان مارا روز و شب</p></div>
<div class="m2"><p>مونس و همدم نه بینم در همه آفاق کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شعاع پرتو مهر جمال روی دوست</p></div>
<div class="m2"><p>جمله ذرات هست و نیست دیدم هر نفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقان احرام طوف کوی جانان بسته اند</p></div>
<div class="m2"><p>پرصدا شد جمله آفاق زآواز جرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هوای وصل جانان برتر از کون و مکان</p></div>
<div class="m2"><p>مرغ جانم هر زمان طیران نماید زین قفس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذوق عرفان نیست زاهد را وگر گوید که هست</p></div>
<div class="m2"><p>گو که بسم الله بیا اینست میدان و فرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جذبه عشقش اسیری را بجائی ره نمود</p></div>
<div class="m2"><p>کاندر آن عالم ندیدم زیرو بالا پیش و پس</p></div></div>