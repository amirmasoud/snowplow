---
title: >-
    شمارهٔ ۳۸۲
---
# شمارهٔ ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>بسویت می کشد دل هر زمانم</p></div>
<div class="m2"><p>ز جان من چه خواهد دل ندانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان شادم که از دست غم تو</p></div>
<div class="m2"><p>نباشد در جهان یکدم امانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم بردی و رفتی و نگفتی</p></div>
<div class="m2"><p>که من بی جان و دل کی زنده مانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جور عشق او صبر و خرد را</p></div>
<div class="m2"><p>مدارای دل طمع دیگر ز جانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دیدم پرتو خورشید رویش</p></div>
<div class="m2"><p>بسان ذره شیدای جهانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برو ناصح مده پندم ز عشقش</p></div>
<div class="m2"><p>نگر من عاشق و رسوا چه سانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیری وصف حسن نوربخشش</p></div>
<div class="m2"><p>چگویم چونکه ناید در بیانم</p></div></div>