---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>دوش یارم پرده از رخسار خود بگشاده بود</p></div>
<div class="m2"><p>گویی از حسنش قیامت در جهان افتاده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ملاحت مثل او هرگز ندیدم در جهان</p></div>
<div class="m2"><p>آن پری رو گوئیا در حسن حوری زاده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وه چه عیشی داشتم کز چشم مست و روی او</p></div>
<div class="m2"><p>شاهد و شمع و شراب و مطرب آماده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجلس همچون بهشت و یارحوری در کنار</p></div>
<div class="m2"><p>در میان این مطرب از جام لعلش باده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون حمایل ساعد سیمین او در گردنم</p></div>
<div class="m2"><p>بر رخ زردم رخ خورشید وش بنهاده بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست ما بگرفته یار و در برخود میکشید</p></div>
<div class="m2"><p>دولت عالم چگویم دوش دستم داده بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جمال نوربخش او اسیری والهی</p></div>
<div class="m2"><p>بیخود از خود گشته وز قید جهان آزاده بود</p></div></div>