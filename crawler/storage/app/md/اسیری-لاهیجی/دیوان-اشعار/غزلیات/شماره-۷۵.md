---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>(در مقام لی مع الله سیر نیست</p></div>
<div class="m2"><p>وحدت محض است اینجا غیر نیست)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>(در قلندر خانه توحید عشق</p></div>
<div class="m2"><p>کعبه و مسجد کنشت و دیر نیست)</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>(در مقام ذات بیرون از صفات</p></div>
<div class="m2"><p>هیچ ره رو را مجال سیر نیست)</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات فنا از نیک و بد</p></div>
<div class="m2"><p>کو نشان آنجا چو شر و خیر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جای سیمرغست و قاف قربتست</p></div>
<div class="m2"><p>محرم این آشیان هر طیر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای اسیری در مقام محو غیر</p></div>
<div class="m2"><p>کشف و سکر و صحوو سیر و طیرنیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واقف شرب شراب شیخ جام</p></div>
<div class="m2"><p>کی شوی گر مشرب بوالخیر نیست</p></div></div>