---
title: >-
    شمارهٔ ۴۲۵
---
# شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>بس غریب و طرفه افتادست حال عاشقان</p></div>
<div class="m2"><p>جسم ایشان در زمین و جانشان برآسمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مکان ابدان ایشان پای بند آمد ولی</p></div>
<div class="m2"><p>دایما ارواحشان طیران کند در لامکان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظاهر ایشان بود مشغول خلق از مرحمت</p></div>
<div class="m2"><p>لیک در باطن ز حق نبوند غافل یکزمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در شعاع مهر ذاتش فانی مطلق شدند</p></div>
<div class="m2"><p>پس بحق باقی شده دیدند حیات جاودان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازمقام بی نشانی صد نشان آورده اند</p></div>
<div class="m2"><p>در فنای عشق تا گشتند بی نام و نشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونکه ایشان بوده اند ایجاد عالم را سبب</p></div>
<div class="m2"><p>برطفیل ذاتشان آمد همه کون و مکان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست با هریک ز حق فیض و عطا بی منتها</p></div>
<div class="m2"><p>ای اسیری حال ایشان نیست درحد و بیان</p></div></div>