---
title: >-
    شمارهٔ ۴۲۲
---
# شمارهٔ ۴۲۲

<div class="b" id="bn1"><div class="m1"><p>مرآت روی دوست نظرکن جهان ببین</p></div>
<div class="m2"><p>درآینه جهان نگر او را عیان ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید حسن اوز همه ذره رونمود</p></div>
<div class="m2"><p>تابان رخش زمشرق کون و مکان ببین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن یار بی نشان که نهان بود از همه</p></div>
<div class="m2"><p>آمد عیان بصورت نام و نشان ببین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوری آن کسان که شدند منکر لقا</p></div>
<div class="m2"><p>دیدار دوست ازهمه فاش و عیان ببین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با عقل کم نشین که بود جای ترس و بیم</p></div>
<div class="m2"><p>همراه عشق شو همه امن و امان ببین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر عاشقی وصال طلب کن نه حور عین</p></div>
<div class="m2"><p>بگذر ز فکر جنت و روی جنان ببین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذر اسیریا بره عشق از دلیل</p></div>
<div class="m2"><p>معشوق را برون ز یقین و گمان ببین</p></div></div>