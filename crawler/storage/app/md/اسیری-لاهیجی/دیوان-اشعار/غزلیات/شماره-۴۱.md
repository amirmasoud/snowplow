---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>ساقی بده می که بود مستیش فنا</p></div>
<div class="m2"><p>تا وارهاندم ز خمار منی و ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان باده که چون که بنوشیم جرعه</p></div>
<div class="m2"><p>فارغ کند ز غصه دنیا و دین مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جام عشق جمله جهان مست و بیخودند</p></div>
<div class="m2"><p>رقاص و کف زنان همه گویند تن تلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر شد زیار عرصه کونین و همچنان</p></div>
<div class="m2"><p>بی دیده در طلب که کجا جویمش کجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذرات غرق بحر محیطند و از عطش</p></div>
<div class="m2"><p>جویای قطره شده هر یک چو بی نوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دایم حریف شاهد و می باش و پاکباز</p></div>
<div class="m2"><p>گر زانکه میروی ره رندان بی ریا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جامی بدست مست و خرامان رسید یار</p></div>
<div class="m2"><p>گفتا اسیریا بکجا میروی بیا</p></div></div>