---
title: >-
    شمارهٔ ۳۹۲
---
# شمارهٔ ۳۹۲

<div class="b" id="bn1"><div class="m1"><p>روشن ز ماه روی تو شد منزل دلم</p></div>
<div class="m2"><p>وز زلف سرکشت همه سوداست حاصلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشتم غریق بحر غم و در تعجبم</p></div>
<div class="m2"><p>گر لطف بیکران نکشیدی بساحلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فکر دقیق من بمیان تو ره نبرد</p></div>
<div class="m2"><p>وز سر غیب آن دهن تنگ غافلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حقا که ورد من بشب و روز ذکرتست</p></div>
<div class="m2"><p>زیرا که فارغ از همه افکار باطلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیرم ز مصحف رخت آیات محکمات</p></div>
<div class="m2"><p>گر ساعد چو سیم تو گردد حمایلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوری ز مهرروی تو مارا هلال ساخت</p></div>
<div class="m2"><p>ای وای برمن ار تو نیائی مقابلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آزاده شد اسیری ز قید بهشت و حور</p></div>
<div class="m2"><p>تا گشته است کوی تو مأوا و منزلم</p></div></div>