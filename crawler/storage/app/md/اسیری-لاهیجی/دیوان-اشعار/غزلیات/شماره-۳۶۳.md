---
title: >-
    شمارهٔ ۳۶۳
---
# شمارهٔ ۳۶۳

<div class="b" id="bn1"><div class="m1"><p>از عشق تو مامعتکف کوی نیازیم</p></div>
<div class="m2"><p>وز آتش سودای تو در سوز و گدازیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گوشه محراب خم ابروی جانان</p></div>
<div class="m2"><p>پیوسته با خلاص به اوراد و نمازیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محروم نیم از حرم وصل تو یکدم</p></div>
<div class="m2"><p>از روز ازل با تو چو ما محرم رازیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر لحظه دگر عشوه کند شاهد حسنت</p></div>
<div class="m2"><p>ماواله و شیدای چنان عشوه و نازیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما جان و جهان و سر و زر از پی معشوق</p></div>
<div class="m2"><p>در کوی غم عشق همه پاک ببازیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آتش هجران تو چون عود درین راه</p></div>
<div class="m2"><p>بربوی وصال تو بسوزیم و بسازیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از روی حقیقت همه یارست اسیری</p></div>
<div class="m2"><p>ما عارف اشیا نه بتقلید و مجازیم</p></div></div>