---
title: >-
    شمارهٔ ۴۵۶
---
# شمارهٔ ۴۵۶

<div class="b" id="bn1"><div class="m1"><p>ای هر دوکون لمعه از نور ذات تو</p></div>
<div class="m2"><p>وی کاینات بوده نمود صفات تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عکس جمال روی تو پیداست از جهان</p></div>
<div class="m2"><p>مرآت حسن روی تو شد کاینات تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارند روبروی تو در هر مقام و حال</p></div>
<div class="m2"><p>مؤمن ز کعبه کافر ازین سومنات تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر ذره گر چه مظهر خورشید ذات شد</p></div>
<div class="m2"><p>دارد ظهور خاص بهر جای ذات تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون روی تو جمال نمود از منات و لات</p></div>
<div class="m2"><p>شد بت پرست عابدلات و منات تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مخمور و بیخودیم بده ساقی از کرم</p></div>
<div class="m2"><p>یک جرعه از شراب لب چون نبات تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تاب مهر نور جمال حبیب شد</p></div>
<div class="m2"><p>از قید هست و نیست اسیری نجات تو</p></div></div>