---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>بردار ای صبا ز جمالش نقاب را</p></div>
<div class="m2"><p>گو بنگرید آن رخ چون آفتاب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دیده تاب دیدن حسن رخش نداشت</p></div>
<div class="m2"><p>برروی خود فکند ازین رو نقاب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی بیار باده بمخمور عشق ده</p></div>
<div class="m2"><p>بشکن خمار عاشق مست و خراب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای پیر میکده در میخانه باز کن</p></div>
<div class="m2"><p>مست مدام ساز همه شیخ و شاب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر بحر هستی تو جهان غیر موج نیست</p></div>
<div class="m2"><p>عارف بغیر بحر چگوید حباب را؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمری بعشق روی تو نغنود دیده هیچ</p></div>
<div class="m2"><p>با چشم عاشق تو چه کارست خواب را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزد اسیری دوزخ محض و عذاب دان</p></div>
<div class="m2"><p>بی روی دوست جنت عدن و ثواب را</p></div></div>