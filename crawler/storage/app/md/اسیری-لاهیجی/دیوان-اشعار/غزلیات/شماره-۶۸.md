---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>باکوی تو از روضه رضوان نتوان گفت</p></div>
<div class="m2"><p>با روی تو از حور و زغلمان نتوان گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عاشق دیوانه مجوئید سلامت</p></div>
<div class="m2"><p>با بیخبران از سر و سامان نتوان گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با زاهد بی ذوق مگو سر اناالحق</p></div>
<div class="m2"><p>اسرار سلاطین چو بعامان نتوان گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید صفت ز آینه جمله ذرات</p></div>
<div class="m2"><p>چون گشت عیان روی تو، پنهان نتوان گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد دل عاشق نشود به بمداوا</p></div>
<div class="m2"><p>با درد و غم عشق ز درمان نتوان گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن تو ندانم به چه تشبیه توان کرد</p></div>
<div class="m2"><p>خورشید جمالت مه تابان نتوان گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق ز غم عشق چو شد بی دل و بی دین</p></div>
<div class="m2"><p>باوی دگر از کفر و ز ایمان نتوان گفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلدار که پیمان شکن و جور و جفا خوست</p></div>
<div class="m2"><p>باوی سخن از مهر و ز پیمان نتوان گفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جانا چه کند چاره این درد اسیری</p></div>
<div class="m2"><p>چون حال و دل خویش بجانان نتوان گفت</p></div></div>