---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>صورت یاربخواب امد و در گوشم گفت</p></div>
<div class="m2"><p>که بخفتن نتوان در معانی را سفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیز اندر پی مطلوب درآ از سر درد</p></div>
<div class="m2"><p>در طلب روز مخور شب همه شب نیز مخفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش اغیار برون کن ز درون دل خود</p></div>
<div class="m2"><p>تا که با یار همیشه بودت گفت و شنفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقبت دیده جان باز کند بررخ دوست</p></div>
<div class="m2"><p>هر دلی کو بغم و درد مدام آمد جفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان و دل ساز فدا گر هوس دیدارست</p></div>
<div class="m2"><p>زانکه اینجا بکسی رو ننمایند بمفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توئی تست حجاب تو اگر رفت توئی</p></div>
<div class="m2"><p>یار بینی که عیانست ز پیدا و نهفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رودر آئینه دل گفت نماییم دگر</p></div>
<div class="m2"><p>زین سخن جان اسیری چو گل تازه شکفت</p></div></div>