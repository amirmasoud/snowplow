---
title: >-
    شمارهٔ ۲۷۲
---
# شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>آمد برون ز خانه بصد ناز و عشوه یار</p></div>
<div class="m2"><p>حسن جمال خود بجهان کرد آشکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معشوق چون بجلوه گری گشت داستان</p></div>
<div class="m2"><p>هر عاشقی بنقش دگر کرد بیقرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون پرده خیال ز چشم تو دور شد</p></div>
<div class="m2"><p>گردد عیان که هست جهان نقش آن نگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سودای کفر و دین ز خیالم نمی رود</p></div>
<div class="m2"><p>تا دیده ام دمیده برویش خط غیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تابنده شد ز پرده هر ذره آفتاب</p></div>
<div class="m2"><p>چون ظلمت منی و توئی رفت برکنار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیر و سلوک حق کسی شد درین طریق</p></div>
<div class="m2"><p>کوبا خداست دایم و از خود کند فرار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسرار کشف در خور هر گوش و فهم نیست</p></div>
<div class="m2"><p>دم درکش ای اسیری و سر را نگاه دار</p></div></div>