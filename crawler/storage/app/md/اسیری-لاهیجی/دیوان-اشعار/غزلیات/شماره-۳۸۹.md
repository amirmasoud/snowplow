---
title: >-
    شمارهٔ ۳۸۹
---
# شمارهٔ ۳۸۹

<div class="b" id="bn1"><div class="m1"><p>ز شوق روی جانان آنچنانم</p></div>
<div class="m2"><p>که سر از پاو پا از سر ندانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان مستغرقم در بحر وحدت</p></div>
<div class="m2"><p>که از کثرت بکلی برکرانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو از خود فانی و باقی باویم</p></div>
<div class="m2"><p>ازآن هم جسم و هم جان جهانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موالید و عناصر غیر ما نیست</p></div>
<div class="m2"><p>حقیقت هم زمین و آسمانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمود ما بود بود دو عالم</p></div>
<div class="m2"><p>گهی پیدا و دیگر دم نهانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهی در صورت قهرم گهی لطف</p></div>
<div class="m2"><p>گهی کافر گهی مؤمن ازآنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی آزاده از هر قید و گاهی</p></div>
<div class="m2"><p>اسیری گه چنین و گه چنانم</p></div></div>