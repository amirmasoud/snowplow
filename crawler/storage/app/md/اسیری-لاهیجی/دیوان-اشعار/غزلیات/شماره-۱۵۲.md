---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>دل را بجمال رخ تو مهر تولاست</p></div>
<div class="m2"><p>جان را همه دم دولت وصل تو تمناست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک پایه ز معراج کمال دل عارف</p></div>
<div class="m2"><p>می دان بیقین کرسی نه چرخ معلاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پیش کسی کو بجهان ز اهل شهودست</p></div>
<div class="m2"><p>لاخود همه پندار بود جمله چو الاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید صفت عکس جمال تو عیان دید</p></div>
<div class="m2"><p>هر کس که چو آئینه دلش پاک و مصفاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دیده که بیند بجهان نور لقایت</p></div>
<div class="m2"><p>لذات نعیم ابد اینجاش مهیاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از عقل مجو حالت ارباب مقامات</p></div>
<div class="m2"><p>زیرا ز خیال خرد این حال معراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز یار نبیند بجهان همچو اسیری</p></div>
<div class="m2"><p>هرکو دلش از دیدن اغیار مبراست</p></div></div>