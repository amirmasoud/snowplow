---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>یارب ز چه روروی تو در پرده نهانست</p></div>
<div class="m2"><p>در پرده نهانست و پس پرده عیانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پرتو رخسار تو روشن شده دیدیم</p></div>
<div class="m2"><p>گر کعبه و گر مسجد و گر دیر مغانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این طرفه غریبست که خورشید جمالت</p></div>
<div class="m2"><p>در صورت ذرات عیانست و نهانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با آنکه توئی بود و جهان هست نمودی</p></div>
<div class="m2"><p>باکس نتوان گفت چنین است و چنانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر زانکه بخوبان نظری میکنم اما</p></div>
<div class="m2"><p>از روی همه دیده برویت نگرانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مسجد ومیخانه ز شوق تو چه گویم</p></div>
<div class="m2"><p>رندان همه در شورش و زاهد بفغانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از باده عشق است چنان مست اسیری</p></div>
<div class="m2"><p>کز بیخبری عاشق و معشوق ندانست</p></div></div>