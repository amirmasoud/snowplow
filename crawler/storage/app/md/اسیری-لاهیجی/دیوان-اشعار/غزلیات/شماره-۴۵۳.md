---
title: >-
    شمارهٔ ۴۵۳
---
# شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>قد تجلی العشق فی کل المجالی فانظروا</p></div>
<div class="m2"><p>از پس هر ذره تابان گشت مهر روی او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فی مرایا کل عین قد راینا عینه</p></div>
<div class="m2"><p>فافتحوا عینا کم حتی تروا ماتبتغوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار پیشت حاضر و تو از خودی غایب ازو</p></div>
<div class="m2"><p>با خودآ آخر چه گم کردی که میجویی بگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من شراب العشق لم یشرب کشربی شارب</p></div>
<div class="m2"><p>کاسنا بحر فمالم تشربوا لم تعلموا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقیا جام و سبو پرساز بهر دیگران</p></div>
<div class="m2"><p>بحر نوشانرا چه پروا با صراحی و سبو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حار قلبی صار روحی والها فی حسنه</p></div>
<div class="m2"><p>ما نظرنا غیره لما ارنا وجهه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دم به دم بیند اسیری حسن او نوعی دگر</p></div>
<div class="m2"><p>نوش بادش هر نفس جام تجلی نو به نو</p></div></div>