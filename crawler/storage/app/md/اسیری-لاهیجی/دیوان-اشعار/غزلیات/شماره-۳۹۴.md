---
title: >-
    شمارهٔ ۳۹۴
---
# شمارهٔ ۳۹۴

<div class="b" id="bn1"><div class="m1"><p>تا در طریق عشق تو من جان فشان شدم</p></div>
<div class="m2"><p>بی جان شدم ولیک جهان در جهان شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان دم که باختم دل و جان در قمار عشق</p></div>
<div class="m2"><p>از هر چه عقل فرض کند، بیش از آن شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهباز همتم چو پر و بال برگشاد</p></div>
<div class="m2"><p>بالاتر از زمین و زمان و مکان شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا با نشان شوم مگر از یار بی نشان</p></div>
<div class="m2"><p>نام و نشان گذاشتم و بی نشان شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون در فنا ز هستی خود نیست آمدم</p></div>
<div class="m2"><p>در عالم بقا بخدا جاودان شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا گشته ام گدای گدایان کوی تو</p></div>
<div class="m2"><p>در ملک فقر پادشه شه نشان شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آزاده چون که گشت اسیری ز قید خویش</p></div>
<div class="m2"><p>مطلق بکوی عشق و جنون داستان شدم</p></div></div>