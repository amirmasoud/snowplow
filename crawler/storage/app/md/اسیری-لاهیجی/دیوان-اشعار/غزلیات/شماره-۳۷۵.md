---
title: >-
    شمارهٔ ۳۷۵
---
# شمارهٔ ۳۷۵

<div class="b" id="bn1"><div class="m1"><p>بیا ای مرهم درد درونم</p></div>
<div class="m2"><p>ببین تا در غم عشق تو چونم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم عشق تو برد از من دل و دین</p></div>
<div class="m2"><p>بدست عشق تو زینسان زبونم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو هر دم حسن رویت می فزاید</p></div>
<div class="m2"><p>از آن هر لحظه در عشقت زبونم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شوق حسن روی جانفزایت</p></div>
<div class="m2"><p>روانست از دو دیده جوی خونم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز مهر آن جمال عالو افروز</p></div>
<div class="m2"><p>بسان ذره بی صبر و سکونم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غریب و بی کس و بی یار و همدم</p></div>
<div class="m2"><p>ز بی رحمی این گردون دونم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برون پرده در پندار مانده</p></div>
<div class="m2"><p>نکردی واقف از سر درونم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درون گنبد نه چرخ گردون</p></div>
<div class="m2"><p>مجو ما را کزین جمله برونم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنم حل همه مشکل اسیری</p></div>
<div class="m2"><p>چو در اقلیم عشقش ذوفنونم</p></div></div>