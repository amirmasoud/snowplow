---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>آن هادی ره روان کجا شد</p></div>
<div class="m2"><p>وان سرور عارفان کجا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن مطلب طالبان صادق</p></div>
<div class="m2"><p>وان مونس عاشقان کجا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن حجت حق و رهبر خلق</p></div>
<div class="m2"><p>وان ملجاء سالکان کجا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن مرکز دور چرخ و انجم</p></div>
<div class="m2"><p>وان مقصد کن فکان کجا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن غوث جهان و قطب آفاق</p></div>
<div class="m2"><p>وان نادره جهان کجا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>والی ولایت یقین کو</p></div>
<div class="m2"><p>سلطان محققان کجا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عنقا صفتش نشان نیابم</p></div>
<div class="m2"><p>آن طایر بی نشان کجا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او جان جهان بد و جهان تن</p></div>
<div class="m2"><p>تن هست بگو که جان کجا شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>او بود مدار سر دایر</p></div>
<div class="m2"><p>از دایره زمان کجا شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر دوجهان برش عیان بود</p></div>
<div class="m2"><p>آن عارف غیب دان کجا شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دامن ز غبار ما اسیری</p></div>
<div class="m2"><p>افشاند چو جان روان کجاشد</p></div></div>