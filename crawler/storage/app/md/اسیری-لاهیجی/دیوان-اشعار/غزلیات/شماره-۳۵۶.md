---
title: >-
    شمارهٔ ۳۵۶
---
# شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>زنگ دویی ز آینه دل زدوده‌ایم</p></div>
<div class="m2"><p>تا حسن جان‌فزای تو با تو نموده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو کلیم تا که به طور دل آمدیم</p></div>
<div class="m2"><p>انی اناالله از همه عالم شنوده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گلشن وصال چو شاهان به عیش و ناز</p></div>
<div class="m2"><p>دایم حریف شاهد و پیمانه بوده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگذار ای رقیب مرا با جفای دوست</p></div>
<div class="m2"><p>ما یار خود به جور و وفا آزموده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که عشق ما به تو هردم فزون چراست</p></div>
<div class="m2"><p>گفتا از آنکه ما به ملاحت فزوده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم به دیر و کعبه چرا روی کرده‌اند</p></div>
<div class="m2"><p>گفتا از آنکه ما همه جا رو نموده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که پیر میکده این در چه بسته؟</p></div>
<div class="m2"><p>گفتا اسیریا که درآ در گشوده‌ایم</p></div></div>