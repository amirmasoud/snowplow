---
title: >-
    شمارهٔ ۴۶۰
---
# شمارهٔ ۴۶۰

<div class="b" id="bn1"><div class="m1"><p>جان ما را نیست در عالم بجز این آرزو</p></div>
<div class="m2"><p>کو نشیند یکدمی با دلبر خود روبرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بملک وصل او جان و دلم آرام یافت</p></div>
<div class="m2"><p>سالها از دست هجرانش دویدم سوبسو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن روی او عیان دیدم ز مرآت جهان</p></div>
<div class="m2"><p>دیده بینا نه بیند در دو عالم غیر او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده نابست پیش مست صهبای شهود</p></div>
<div class="m2"><p>ساقی و میخانه و می خواره و جام و سبو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دل صافی توان دیدن جمال روی دوست</p></div>
<div class="m2"><p>زاهدا آن دل نداری در پی اش هرزه مپو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنگ غم ز آئینه دل میزداید چار چیز</p></div>
<div class="m2"><p>آب و دیگر باده و گشت چمن، روی نکو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیرت جان اسیری در نماز عشق بین</p></div>
<div class="m2"><p>جز به محراب دو ابروی تو نارد سرفرو</p></div></div>