---
title: >-
    شمارهٔ ۲۵۸
---
# شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>عاشق چشمان مست آن نگار</p></div>
<div class="m2"><p>تا ابد هرگز نباشد بی خمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دلی کو پر ز درد عشق نیست</p></div>
<div class="m2"><p>پیش عشاق جهان ناید بکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نباشد دلربائی مثل او</p></div>
<div class="m2"><p>تخم عشق او بدل دایم بکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اهل دل جمله خریدار ویند</p></div>
<div class="m2"><p>من که باشم تا که آیم در شمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست همچون زلف عنبر بوی او</p></div>
<div class="m2"><p>در خطا و درختن مشک تتار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شدم مست از شراب لعل او</p></div>
<div class="m2"><p>دم بدم ساقی مرا زان لب می آر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای اسیری چون گرفتارش شدی</p></div>
<div class="m2"><p>دایما خون دل از دیده ببار</p></div></div>