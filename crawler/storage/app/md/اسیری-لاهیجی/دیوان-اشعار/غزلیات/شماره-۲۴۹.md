---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>در آرزوی روی تو گشتیم بیقرار</p></div>
<div class="m2"><p>بردار پرده از رخ و مقصود ما برآر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی مجو بهانه که فرصت غنیمت است</p></div>
<div class="m2"><p>بگشا سربسو و بتعجیل می بیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیوانه ایم و عاشق و مست مدام عشق</p></div>
<div class="m2"><p>با عقل و پارسائی و تقوی مرا چه کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با عاشقان حکایت معشوق و باده گو</p></div>
<div class="m2"><p>با زاهدان ز جنت و حوران گلعذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دل ز فکر غیر مبرا نمی شود</p></div>
<div class="m2"><p>در بزم وصل یار ترا کی دهند بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بینی جمال عشق و زمعشوق برخوری</p></div>
<div class="m2"><p>در راه عاشقان خدا گر شوی نثار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکو قدم براه طریقت نهد بحق</p></div>
<div class="m2"><p>از دامن اسیری ما دست گو مدار</p></div></div>