---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>بگرفت صیت حسن تو از قاف تا بقاف</p></div>
<div class="m2"><p>تا او فتاد پرتو رویت بنون و کاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آئینه جمال تو دیدیم هرچه بود</p></div>
<div class="m2"><p>عارف کسی بود که بدین دارد اعتراف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یارست هرچه هست درین دار غیر نیست</p></div>
<div class="m2"><p>برحق چرا تو نسبت باطل کنی گزاف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشاق او بمذهب عشقند متفق</p></div>
<div class="m2"><p>در دین عشق کفر بود گر کنی خلاف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر فقر و نیستی است تولای عاشقان</p></div>
<div class="m2"><p>زاهد ز کبر و هستی خود این همه ملاف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عنقای وحدتم همه جا قاف قرب ماست</p></div>
<div class="m2"><p>مجموع کاینات مرا گشته کوه قاف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آئینه دل تو چو صافست اسیریا</p></div>
<div class="m2"><p>دارد همیشه میل ازین روبروی صاف</p></div></div>