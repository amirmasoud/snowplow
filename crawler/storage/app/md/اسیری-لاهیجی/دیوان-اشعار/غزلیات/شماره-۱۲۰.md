---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>شفای این دل بیمار جز لقای تو نیست</p></div>
<div class="m2"><p>طبیب جان خرابم کسی ورای تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسیکه پرسش خسته دلان کند دایم</p></div>
<div class="m2"><p>بدور حسن تو جانا بجز جفای تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غریب و بی کسم و در جهان مرا باری</p></div>
<div class="m2"><p>بحق صحبت جانان که کس بجای تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا ز وصل تو نومید می تواند شد</p></div>
<div class="m2"><p>کسی که مقصد جانش بجز وفای تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیری در غم دوری بسوخت ای دلبر</p></div>
<div class="m2"><p>بیاکه مرهم دردش بجز لقای تو نیست</p></div></div>