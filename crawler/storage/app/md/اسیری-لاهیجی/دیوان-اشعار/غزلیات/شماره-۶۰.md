---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>عشق توچاره ساز دل بیقرار ماست</p></div>
<div class="m2"><p>درد و غم تو مرهم جان فگارماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فقر و فناست شیوه رندان جان فشان</p></div>
<div class="m2"><p>در راه عشق هستی و ناموس عارماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معشوقه باز و رندم و قلاش و پاکباز</p></div>
<div class="m2"><p>جانا بدین عشق تو اینها شعار ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا گشته ایم خاک کف پای عاشقان</p></div>
<div class="m2"><p>در کاینات عشق همه گیر و دار ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میخانه تا سپرد بما پیر می فروش</p></div>
<div class="m2"><p>ذرات جرعه نوش می خوشگوار ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شسته ایم دست دل از کار کاینات</p></div>
<div class="m2"><p>در هر دو کون هر چه تو بینی بکارماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جور و از جفای رقیبان اسیریا</p></div>
<div class="m2"><p>غم نیست چون به مهر و وفا یار یار ماست</p></div></div>