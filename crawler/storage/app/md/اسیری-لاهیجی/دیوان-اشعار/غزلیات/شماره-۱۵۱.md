---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>جمله عالم رو بما دارند و مارا روبدوست</p></div>
<div class="m2"><p>وربمعنی روشناسی جمله رو خودروی اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست جانت را مشامی ورنه آفاق جهان</p></div>
<div class="m2"><p>از نسیم طره عنبر فشانش مشکبوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرده رویش بعالم نیست جز وهم و خیال</p></div>
<div class="m2"><p>چون نماند این خیالت هر چه بینی جمله اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهد حسنش ندارد در حقیقت خود حجاب</p></div>
<div class="m2"><p>روی او پنهان چو بینی در نقاب ما و توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن یکی از کعبه دیدش دیگر از دیر و کنشت</p></div>
<div class="m2"><p>هر کسی رارخ بجائی می نماید حسن دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست این می هر کسی از جام دیگر گشته اند</p></div>
<div class="m2"><p>جام زاهد حور و جام عاشقان روی نکوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سربسر آفاق عالم گشت غرق این شراب</p></div>
<div class="m2"><p>با اسیری گو چه جای قصه جام و سبوست</p></div></div>