---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>من نخواهم شادی و عیش و طرب</p></div>
<div class="m2"><p>درد و سوز عشق خواهم روز و شب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غم و محنت گریزانند خلق</p></div>
<div class="m2"><p>ما بجان جویای دردش ای عجب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصل مطلوب آرزوی طالب است</p></div>
<div class="m2"><p>من فنای خویش خواهم زین طالب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوز دل آمد نشان عاشقی</p></div>
<div class="m2"><p>ساز راه عشق رنجست و تعب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش جان سوز عشقش هر زمان</p></div>
<div class="m2"><p>از دل عاشق برآرد صد لهب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دایما خواهم که باشد جان و دل</p></div>
<div class="m2"><p>ز آتش سودای او در تاب تب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آتش عشق تو جان عاشقان</p></div>
<div class="m2"><p>کی بسوزد، می فروزد چون ذهب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتنه جویی جمله برچون عشق نیست</p></div>
<div class="m2"><p>در میان ترک و تاجیک و عرب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتاب عشق از ذرات کون</p></div>
<div class="m2"><p>گشت طالع عاشقان یاللعجب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بقای عشق گشتم من فنا</p></div>
<div class="m2"><p>عین عشقم این زمان از فضل رب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من اسیر دام عشقم لاجرم</p></div>
<div class="m2"><p>شد اسیری در جهان ما را لقب</p></div></div>