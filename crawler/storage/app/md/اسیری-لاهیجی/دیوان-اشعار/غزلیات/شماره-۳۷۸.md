---
title: >-
    شمارهٔ ۳۷۸
---
# شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>حج من سوی تو آمد طوف کویت کعبه‌ام</p></div>
<div class="m2"><p>دیدن دیدار تو باشد صفا و مروه‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامه احرام من باشد تجرد از هوا</p></div>
<div class="m2"><p>لازم درگاه تو بودن همیشه وقفه‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاجیان را گرچه باشد هدیه بد نه پیش تو</p></div>
<div class="m2"><p>در هوایت کشتن نفس و هوا شد هدیه‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رو به سوی هرکسی دارند در هر ملتی</p></div>
<div class="m2"><p>کعبه ما کوی یار و روی او شد قبله‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد نماز و ورد من ذکر جمال روی او</p></div>
<div class="m2"><p>دل ز یاد غیر خالی کردن آمد روزه‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدن دیدار دلبر هست عید من</p></div>
<div class="m2"><p>جان و دل ایثار کردن پیش جانان فطره‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای اسیری جمله عشاق جانبازان بدند</p></div>
<div class="m2"><p>جان به عشق دلبر افشان گو که من زین زمره‌ام</p></div></div>