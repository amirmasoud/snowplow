---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>نیست ما را هیچ فکری جز لقای روی دوست</p></div>
<div class="m2"><p>آفرین بررای درویشی که در فکر نکوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف و رویت نیست تنها آرزوی ما و بس</p></div>
<div class="m2"><p>مشتری و ماه را شبرو شدن زین آرزوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقی را باید از باد صبا آموختن</p></div>
<div class="m2"><p>میرود بی پا و بی سر دایما در جستجوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستی عشاق باشد زان دو چشم پر خمار</p></div>
<div class="m2"><p>سرخوشی بیدلان نه از باده جام و سبوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش قاضی محبان مدعی عشق را</p></div>
<div class="m2"><p>شاهد عادل بغیر از چشم گریان، رنگ روست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو(ی)اگر پیش سر عشاق در میدان غم</p></div>
<div class="m2"><p>میکند دعوی که من سرگشته ام بیهوده گوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر سرو قامتش در باغ دل جایی مده</p></div>
<div class="m2"><p>همت عالی اسیری چون ترا آئین و خوست</p></div></div>