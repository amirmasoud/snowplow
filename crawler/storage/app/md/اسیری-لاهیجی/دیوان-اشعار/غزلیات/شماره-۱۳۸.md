---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>ای ز خورشید جمالت گشته روشن کاینات</p></div>
<div class="m2"><p>وی ز مهر ماه رویت چرخ و انجم بی ثبات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبعه سیاره سرگردان ز شوق روی تو</p></div>
<div class="m2"><p>برامید بوی وصلت آرمیده ثابتات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر فیضت گر نمی بارید برملک عدم</p></div>
<div class="m2"><p>کی دمیدی سبزه و گل در زمین ممکنات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب ذات تابان شد ز ذرات جهان</p></div>
<div class="m2"><p>این کسی داند که بیند ذات را عین صفات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه روی تو ز مرآت جهان بیند عیان</p></div>
<div class="m2"><p>گشت یکسان در شهودش کعبه و لات و منات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون مسیحای لب تو دم دمد در مرده ها</p></div>
<div class="m2"><p>در نفس هریک برون آرد سر از جیب حیات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون اسیری پرتو خورشید روی نوربخش</p></div>
<div class="m2"><p>هر که بیند یابد از قید من و مائی نجات</p></div></div>