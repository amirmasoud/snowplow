---
title: >-
    شمارهٔ ۳۵۹
---
# شمارهٔ ۳۵۹

<div class="b" id="bn1"><div class="m1"><p>باز از سودای زلفش بی سرو سامان شدم</p></div>
<div class="m2"><p>در شعاع حسن او شیدائی و حیران شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که خورشید جمال نوربخش او بتافت</p></div>
<div class="m2"><p>ظلمت ما نور گشت و همچو مه تابان شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بدیدم از همه ذرات مهر روی دوست</p></div>
<div class="m2"><p>عارف اهل یقین و صاحب عرفان شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه بودم قطره چون در بحر کل گشتم فنا</p></div>
<div class="m2"><p>در بقا بعدالفنا دریای بی پایان شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دین و دنیا چون فدای عشق جانان ساختم</p></div>
<div class="m2"><p>در طریق عاشقی سرحلقه رندان شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قمار عشق جانان جان و دل درباختم</p></div>
<div class="m2"><p>محرم بزم وصالش بی دل و بی جان شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عارفان گر شهره شهرند اسیری از عمل</p></div>
<div class="m2"><p>من بمحض موهبت اعجوبه دوران شدم</p></div></div>