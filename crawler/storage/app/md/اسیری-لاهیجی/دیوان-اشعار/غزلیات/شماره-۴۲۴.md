---
title: >-
    شمارهٔ ۴۲۴
---
# شمارهٔ ۴۲۴

<div class="b" id="bn1"><div class="m1"><p>جلوه معشوق دیدم در لقای عاشقان</p></div>
<div class="m2"><p>واله و شیدا ازآنم در هوای عاشقان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تجلی رخ معشوق و تاب حسن او</p></div>
<div class="m2"><p>گر فنا شد جان ما بادا بقای عاشقان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرفراز کاینات آمد بملک هر دو کون</p></div>
<div class="m2"><p>هر سری کو خاک ره شد زیر پای عاشقان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بکوی عاشقان گشتم نثار ره، چه شد</p></div>
<div class="m2"><p>صد هزاران جان و دل بادا فدای عاشقان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه با نام و نشان از وصل معشوق آمدست</p></div>
<div class="m2"><p>دردو عالم کس نمی بینم ورای عاشقان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو مگردان از جفا و جور عشق و مهر کن</p></div>
<div class="m2"><p>در میان جان و دل مهرو وفای عاشقان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پادشاه تخت ملک عشق آمد لایزال</p></div>
<div class="m2"><p>دل که باشد در جهان از جان گدای عاشقان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شراب وصل جانان گشت مست و بیخبر</p></div>
<div class="m2"><p>جان که شیدای جهان شد در ولای عاشقان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای اسیری تا بکی داری نهان اسرار عشق</p></div>
<div class="m2"><p>چون جهان پرگشت از صیت و صدای عاشقان</p></div></div>