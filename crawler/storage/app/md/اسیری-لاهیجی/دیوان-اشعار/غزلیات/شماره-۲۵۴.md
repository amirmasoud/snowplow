---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>بی شادی وصال تو دل را کجا قرار</p></div>
<div class="m2"><p>ناید غم فراق تو ای دوست در شمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد جان دلشدگان ای صبا برس</p></div>
<div class="m2"><p>از روی دوست برفکن این زلف تابدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صحت پذیر نیست دل خسته، ای طبیب</p></div>
<div class="m2"><p>در هر نفس اگر نکنی سوی من گذار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخشید جان نو بتن مرده در نفس</p></div>
<div class="m2"><p>هر شربتی که داد لب لعل آبدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مست مدام عشق توام ساقیا بده</p></div>
<div class="m2"><p>پیوسته جام باده از آن چشم پر خمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمم چو باز شد دو جهان پرزیار بود</p></div>
<div class="m2"><p>نام و نشان غیر ندیدم درین دیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید روی دوست ز ذرات چون بتافت</p></div>
<div class="m2"><p>هر ذره چو ماه شد از مهر روی یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون در میان جان و دلم بوده مقیم</p></div>
<div class="m2"><p>ای سرو ناز از چه زما میکنی کنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر شاهد و شراب اسیری طلب کنی</p></div>
<div class="m2"><p>سر ز آستان پیر خرابات برمدار</p></div></div>