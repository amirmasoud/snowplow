---
title: >-
    شمارهٔ ۳۷۰
---
# شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>ما در هوای وصل تو شیدای عالمیم</p></div>
<div class="m2"><p>پیوسته با خیال تو شادان و خرمیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما در خیال زلف پریشان بیقرار</p></div>
<div class="m2"><p>آشفته خاطریم و بسودای محکمیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مونس کجا شویم به زهاد چونکه ما</p></div>
<div class="m2"><p>در میکده بشاهد و می یار و همدمیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محروم از جمال تو یک دم نبود جان</p></div>
<div class="m2"><p>چون در حریم انس بوصل تو محرمیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون حسن عارض تو نیاید بشرح و وصف</p></div>
<div class="m2"><p>ما در بیان حسن و جمال تو ابکمیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانرا ز درد فرقت جانان خبر کجاست</p></div>
<div class="m2"><p>از وصل او همیشه چو دلشاد و بی غمیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا حسن تو نقاب اسیری ز رخ فکند</p></div>
<div class="m2"><p>در پرتو جمال تو حیران عالمیم</p></div></div>