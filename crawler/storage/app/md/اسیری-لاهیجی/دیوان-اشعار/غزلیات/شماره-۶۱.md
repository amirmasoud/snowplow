---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>هرکس که از جفای غم عشق خایفست</p></div>
<div class="m2"><p>با عاشقان بدین و مذهب مخالفست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در موقف محبت و عشق است لایزال</p></div>
<div class="m2"><p>هر دل که او ز رمز فاحببت واقفست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانی طواف کعبه وصلت که میکند؟</p></div>
<div class="m2"><p>آن دل که در مدینه عشق تو طایفست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعدالفنا بملک بقا هرکه راه یافت</p></div>
<div class="m2"><p>معروف وار بر همه اسرار عارفست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن دل که خواند آیت حسنش زهر ورق</p></div>
<div class="m2"><p>فرقان عشق و مصحف سرمعارفست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق که از امید و ز بیم جهان رهید</p></div>
<div class="m2"><p>امیدوار بر تو و از خویش خایفست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تجرید نیستی چو اسیری براه دوست</p></div>
<div class="m2"><p>گر خود مقاصد دو جهانت موافقست</p></div></div>