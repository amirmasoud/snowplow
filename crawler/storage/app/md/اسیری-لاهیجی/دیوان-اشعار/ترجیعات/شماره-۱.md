---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>جنبش بحر عشق پیداشد</p></div>
<div class="m2"><p>موج زد نقش ما هویدا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشت دریا عیان بصورت ما</p></div>
<div class="m2"><p>مائی ما نمود دریا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دو عالم بنقش ما بنمود</p></div>
<div class="m2"><p>اصل جمله حقیقت ما شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلزم عشق زد نفس در دم</p></div>
<div class="m2"><p>جمله کاینات پیدا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نماید کمال خود پیدا</p></div>
<div class="m2"><p>عشق از خانه سوی صحرا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق برخود لباس هر دو جهان</p></div>
<div class="m2"><p>چون بیاراست آشکارا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن خود در لباس زیبا دید</p></div>
<div class="m2"><p>عاشق خویش گشت و شیدا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نام خود کرد عاشق و معشوق</p></div>
<div class="m2"><p>گاه مجنون و گاه لیلا شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غیر او نیست در جهان موجود</p></div>
<div class="m2"><p>بیند آنکو بعشق بینا شد</p></div></div>
<div class="b2" id="bn10"><p>که جهان موجهای این دریاست</p>
<p>موج دریا و یکیست غیر کجاست</p></div>
<div class="b" id="bn11"><div class="m1"><p>غیرت عشق اینچنین فرمود</p></div>
<div class="m2"><p>که نباشد بغیر او موجود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا نه بیند جمال او غیری</p></div>
<div class="m2"><p>خویشتن را بنقش جمله نمود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر زمان کسوت دگر پوشید</p></div>
<div class="m2"><p>لحظه لحظه بحسن دیگر بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه او بود طالب و مطلوب</p></div>
<div class="m2"><p>غیر او نیست شاهد و مشهود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این همه نقش های گوناگون</p></div>
<div class="m2"><p>در حقیقت بجز نمود نبود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مهر رویش ز پرده ذرات</p></div>
<div class="m2"><p>چونکه بنمود جان ما آسود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جمله عالم نمود در نظرم</p></div>
<div class="m2"><p>نقش موجی بروی بحر وجود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر دو عالم ظهور یک عشق است</p></div>
<div class="m2"><p>گر نظر میکنی بعین شهود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دل چو دریافت ذوق حالت عشق</p></div>
<div class="m2"><p>پرده از روی راز خویش گشود</p></div></div>
<div class="b2" id="bn20"><p>که جهان موجهای این دریاست</p>
<p>موج و دریا یکیست غیر کجاست</p></div>
<div class="b" id="bn21"><div class="m1"><p>من یقین و گمان نمی دانم</p></div>
<div class="m2"><p>علم و معنی بیان نمی دانم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من مقامات و حال و کشف و شهود</p></div>
<div class="m2"><p>بی نشان و نشان نمی دانم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من تجلی و نور و ذوق و سماع</p></div>
<div class="m2"><p>صحو و محو و عیان نمی دانم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عقل و نفس و ملائک و ارکان</p></div>
<div class="m2"><p>لامکان و مکان نمی دانم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر دو عالم بدیده در نارم</p></div>
<div class="m2"><p>این جهان آن جهان نمی دانم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>غیر یک نقطه اندرین ادوار</p></div>
<div class="m2"><p>هیچ دور و زمان نمی دانم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>غیر آن یک حقیقت مطلق</p></div>
<div class="m2"><p>آشکار و نهان نمی دانم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غیر یک نور منبسط بجهان</p></div>
<div class="m2"><p>من زمین آسمان نمی دانم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وصف آن واحد کثیرنما</p></div>
<div class="m2"><p>همچو این یک بیان نمی دانم</p></div></div>
<div class="b2" id="bn30"><p>که جهان موجهای این دریاست</p>
<p>موج و دریا یکیست غیر کجاست</p></div>
<div class="b" id="bn31"><div class="m1"><p>شاهد عشق حسن خود پیدا</p></div>
<div class="m2"><p>کرد اول بصورت اسما</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پس برون کرد سر ز جیب جهان</p></div>
<div class="m2"><p>گشت پیدا بکسوت اشیا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر زمانی جمال او ظاهر</p></div>
<div class="m2"><p>می نماید بنقش ما و شما</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عشق هر دم ظهور دیگر داشت</p></div>
<div class="m2"><p>زان کند نقش مختلف پیدا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر دم از کوی سربرون آرد</p></div>
<div class="m2"><p>روی دیگر نماید او هر جا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر زمان جلوه دگر دارد</p></div>
<div class="m2"><p>حسن رویش بدیده بینا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عشق با حسن خویش می بازد</p></div>
<div class="m2"><p>متهم کرده وامق و عذرا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مهر حسنش ز روی هر ذره</p></div>
<div class="m2"><p>می توان دید هم بدیده ما</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>میخروشد محیط عشق دگر</p></div>
<div class="m2"><p>میرساند بگوش جمله صدا</p></div></div>
<div class="b2" id="bn40"><p>که جهان موجهای این دریاست</p>
<p>موج و دریا یکیست غیر کجاست</p></div>
<div class="b" id="bn41"><div class="m1"><p>ما خراباتیان می نوشیم</p></div>
<div class="m2"><p>خرقه زهد را کجا پوشیم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از پی شاهد و شراب مدام</p></div>
<div class="m2"><p>در ره جست و جو بجان کوشیم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر دو عالم روان بیک جرعه</p></div>
<div class="m2"><p>گر ز ما می خرند بفروشیم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز آتش شوق شاهد و باده</p></div>
<div class="m2"><p>همچو خنب شراب در جوشیم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در خرابات عشق مست و خراب</p></div>
<div class="m2"><p>بی خبر از خودیم و مدهوشیم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ساقیا از شراب لعل لبت</p></div>
<div class="m2"><p>مست و لایعقلیم و بیهوشیم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>وقت آن شد که سوی بحر رویم</p></div>
<div class="m2"><p>هفت دریا بیک نفس نوشیم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>غوطه در بحر بی کرانه زنیم</p></div>
<div class="m2"><p>عین دریا شویم و بخروشیم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پس ببانگ بلند می گوئیم</p></div>
<div class="m2"><p>از کس این راز را نمی پوشیم</p></div></div>
<div class="b2" id="bn50"><p>که جهان موجهای این دریاست</p>
<p>موج و دریا یکیست غیر کجاست</p></div>
<div class="b" id="bn51"><div class="m1"><p>عالمی پر ز شور می بینم</p></div>
<div class="m2"><p>دلبری بس غیور می بینم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از سر کوی عشق عالم سوز</p></div>
<div class="m2"><p>عقل را دور دور می بینم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز آفتاب جمال او عالم</p></div>
<div class="m2"><p>دایما غرق نور می بینم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از دم جانفزای لعل لبش</p></div>
<div class="m2"><p>هر نفس نفخ صور می بینم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در تماشای جلوه رویش</p></div>
<div class="m2"><p>جان و دل در حضور می بینم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شاهد حسن او بصد جلوه</p></div>
<div class="m2"><p>دم بدم در ظهور می بینم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>در تجلی حسن او هر دم</p></div>
<div class="m2"><p>عالمی بی شعور می بینم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هرکه دارد گمان که غیری هست</p></div>
<div class="m2"><p>در یقینش قصور می بینم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر کسی کو نشان عشق بخواند</p></div>
<div class="m2"><p>گفت بین السطور می بینم</p></div></div>
<div class="b2" id="bn60"><p>که جهان موج های این دریاست</p>
<p>موج و دریا یکیست غیر کجاست</p></div>
<div class="b" id="bn61"><div class="m1"><p>مرحبا ترک مست یغمائی</p></div>
<div class="m2"><p>دل ز ما می بری برعنائی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>در جهان نیست کس بتو مانند</p></div>
<div class="m2"><p>بی نظیری بحسن و زیبائی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تا جمال تو بینم از همه رو</p></div>
<div class="m2"><p>در جهان گشته ام تماشائی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مظهر حسن با کمال تو بود</p></div>
<div class="m2"><p>هرچه دیدم نهان و پیدائی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون بهر جا جمال تو بنمود</p></div>
<div class="m2"><p>عاشقانرا دلی است هر جائی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تا بتابید مهر رخسارت</p></div>
<div class="m2"><p>ذره سان گشته ایم شیدائی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>محو مطلق شود همه عالم</p></div>
<div class="m2"><p>گر نقاب از جمال بگشائی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شاهد عشق می نماید رو</p></div>
<div class="m2"><p>از پس پرده من و مائی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>باز بینی بنور عشق عیان</p></div>
<div class="m2"><p>چون ترا شد بعشق بینائی</p></div></div>
<div class="b2" id="bn70"><p>که جهان موج های این دریاست</p>
<p>موج و دریا یکیست غیر کجاست</p></div>
<div class="b" id="bn71"><div class="m1"><p>در خرابات ما گذر نکند</p></div>
<div class="m2"><p>هر که از خویشتن سفر نکند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>آه کان دلبر خراباتی</p></div>
<div class="m2"><p>هیچ برحال ما نظر نکند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ناله عاشقان شیدائی</p></div>
<div class="m2"><p>در دل سنگ او اثر نکند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چنگ در زلف او تواند زد</p></div>
<div class="m2"><p>هرکه ازکافری حذر نکند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>یار با تو جمال ننماید</p></div>
<div class="m2"><p>تا ترا از تو بی خبر نکند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هرکه محجوب کفر و دین باشد</p></div>
<div class="m2"><p>دست با دوست در کمر نکند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>این خرابات عشق دریاییست</p></div>
<div class="m2"><p>مائی ما در او گذر نکند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>عالم حیرتست و می دانم</p></div>
<div class="m2"><p>عقل ازین جای سر بدر نکند</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ما چه دانیم نقش عالم چیست</p></div>
<div class="m2"><p>عشق ما را خبر اگر نکند</p></div></div>
<div class="b2" id="bn80"><p>که جهان موج های این دریاست</p>
<p>موج و دریا یکیست غیر کجاست</p></div>
<div class="b" id="bn81"><div class="m1"><p>ما حریفان بزم رندانیم</p></div>
<div class="m2"><p>مست جام وصال جانانیم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>جرعه جام ماست بحر محیط</p></div>
<div class="m2"><p>ما چه دریا دل و چه رندانیم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ما برندی و عشق ورزیدن</p></div>
<div class="m2"><p>در همه کاینات دستانیم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>نیست ما را خبر ز هشیاری</p></div>
<div class="m2"><p>چون ز جام الست مستانیم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ما ز اوراق دفتر عالم</p></div>
<div class="m2"><p>رقم حسن دوست میخوانیم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>نیست حاجت مرا بظن و قیاس</p></div>
<div class="m2"><p>ما ز اهل شهود و ایقانیم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ما بدیدار دوست پیوسته</p></div>
<div class="m2"><p>واله و دنگ و مست و حیرانیم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بدی عاشقان مگو زاهد</p></div>
<div class="m2"><p>همه را ما چو نیک می دانیم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>کشف شد بر دلم چو این حالت</p></div>
<div class="m2"><p>غیر ازین برزبان نمی رانیم</p></div></div>
<div class="b2" id="bn90"><p>که جهان موج های این دریاست</p>
<p>موج و دریا یکیست غیر کجاست</p></div>
<div class="b" id="bn91"><div class="m1"><p>جان ما در هوای دلدارست</p></div>
<div class="m2"><p>دل گرفتار عشق آن یارست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>از شراب دو چشم مخمورش</p></div>
<div class="m2"><p>جان گهی مست و گاه خمارست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>دل ببازار عشق هر ساعت</p></div>
<div class="m2"><p>وصل او را بجان خریدارست</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>جان ما را ببزمگاه شهود</p></div>
<div class="m2"><p>دیده دایم بروی دلدارست</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>در خرابات عشق با شاهد</p></div>
<div class="m2"><p>عاشقانرا چه عیش و بازارست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>می نماید جمال دوست عیان</p></div>
<div class="m2"><p>دیده بگشا که وقت دیدارست</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>حسن او بیند از دو کون عیان</p></div>
<div class="m2"><p>دل که از نقش غیر بیزارست</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>عشق را جلوه هاست بی غایت</p></div>
<div class="m2"><p>هر دو عالم ازو نمودارست</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چون زبانم بعشق گویا شد</p></div>
<div class="m2"><p>با تو گوید کزین خبردارست</p></div></div>
<div class="b2" id="bn100"><p>که جهان موجهای این دریاست</p>
<p>موج و دریا یکیست غیر کجاست</p></div>
<div class="b" id="bn101"><div class="m1"><p>شاهد حسن او نمود عیان</p></div>
<div class="m2"><p>خویشتن در لباس کون و مکان</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>در پس پرده همه ذرات</p></div>
<div class="m2"><p>آفتاب جمال اوست نهان</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>دم بدم در لباس مستوری</p></div>
<div class="m2"><p>جلوه ها میکند رخ جانان</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>حسن او هر زمان بروی دگر</p></div>
<div class="m2"><p>آشکارا شود بدیده جان</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>جام گیتی نماست عارض دوست</p></div>
<div class="m2"><p>که نماید ازو عکوس جهان</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>حسن رخسار او عیان دیدم</p></div>
<div class="m2"><p>در مزایای جمله اعیان</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>هر چه بینی نشان آن یارست</p></div>
<div class="m2"><p>غیر او را کجاست نام و نشان</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>یار هر دم جمال خود پیدا</p></div>
<div class="m2"><p>می نماید بصورت اکوان</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>گشت روشن چو آفتاب منیر</p></div>
<div class="m2"><p>براسیری ز عین علم و عیان</p></div></div>
<div class="b2" id="bn110"><p>که جهان موج های این دریاست</p>
<p>موج و دریا یکیست غیر کجاست</p></div>
<div class="b" id="bn111"><div class="m1"><p>الا ای دلبر شوخ جفاکار</p></div>
<div class="m2"><p>مرا با من بلطف خویش مگذار</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>که ما و من حجاب راه ما شد</p></div>
<div class="m2"><p>حجاب ما بفضل از پیش بردار</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چو برخیزد خیال ما ز پیشم</p></div>
<div class="m2"><p>مگر بینم دمی بی پرده دیدار</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>جهانرا مظهر حسن تو بینم</p></div>
<div class="m2"><p>بهر جا رو نموده بهر اظهار</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>که تا نبود نشان و نام عالم</p></div>
<div class="m2"><p>ز روی خود برافکن پرده ای یار</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>دمی معشوق خود شو عاشق خود</p></div>
<div class="m2"><p>ترا دایم چو با خود بود بازار</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چنان مست مدام چشم یارم</p></div>
<div class="m2"><p>که تا بودم نبودم هیچ هشیار</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>شراب وحدتش ما را چنان ساخت</p></div>
<div class="m2"><p>که کثرت را نه بینم غیر پندار</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بگو با خاص و عام این نکته روشن</p></div>
<div class="m2"><p>اسیری چون شدی واقف ز اسرار</p></div></div>
<div class="b2" id="bn120"><p>که عالم چون تن و جان جهان اوست</p>
<p>نهان در پرده کون و مکان اوست</p></div>
<div class="b" id="bn121"><div class="m1"><p>منم در عاشقی رسوای عالم</p></div>
<div class="m2"><p>ز عشق تو شده شیدای عالم</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>برویت تا سواد زلف دیدم</p></div>
<div class="m2"><p>فتاد اندر سرم سودای عالم</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>جهان از شوق رویت بیقرارست</p></div>
<div class="m2"><p>که در خوبی توئی زیبای عالم</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>خرد تا مست شد از باده عشق</p></div>
<div class="m2"><p>بمجنونیست سر غوغای عالم</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>ببحر وحدتش غرقم ندارم</p></div>
<div class="m2"><p>نه پروای خود و پروای عالم</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>چو گشتم شادمان از وصل دلبر</p></div>
<div class="m2"><p>فراغت دارم از غم های عالم</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>جهان روشن ز مهر روی یارست</p></div>
<div class="m2"><p>که شد نور رخش دارای عالم</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>جهان خالی ز اغیارست دایم</p></div>
<div class="m2"><p>که از یارست پر مأوای عالم</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>مترس از کس اسیری فاش میگو</p></div>
<div class="m2"><p>ترا چون هست استغنای عالم</p></div></div>
<div class="b2" id="bn130"><p>که عالم چون تن و جان جهان اوست</p>
<p>نهان در پرده کون و مکان است</p></div>
<div class="b" id="bn131"><div class="m1"><p>چو پیدا شد جمال روی انور</p></div>
<div class="m2"><p>برآمد از جهان الله اکبر</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>نسیم زلف عنبر بوی او ساخت</p></div>
<div class="m2"><p>دماغ جمله عالم معطر</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>قد چون سرو او از عزو از ناز</p></div>
<div class="m2"><p>لباس جان و تن راکرد دربر</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>جهان از حسن او برداشت حظی</p></div>
<div class="m2"><p>رسید آخر بآدم حظ اوفر</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بهر دم جلوه دیگر نماید</p></div>
<div class="m2"><p>نشد هرگز یکی جلوه مکرر</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>زهی حسن جهان آرا که خود را</p></div>
<div class="m2"><p>دمادم می نماید نوع دیگر</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>یکی معنی است گر صد گر هزارست</p></div>
<div class="m2"><p>بصورتهای گوناگون مصور</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>چو روی نوربخشش گشت ظاهر</p></div>
<div class="m2"><p>ز نورش جمله عالم شد منور</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>چو زیر پرده عالم اسیری</p></div>
<div class="m2"><p>بدیدی روی او زین پرده بگذر</p></div></div>
<div class="b2" id="bn140"><p>که عالم چون تن و جان جهان اوست</p>
<p>نهان در پرده کون و مکان اوست</p></div>
<div class="b" id="bn141"><div class="m1"><p>جهان مرآت حسن دلبرماست</p></div>
<div class="m2"><p>رخش ز آئینه هر ذره پیداست</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>بود قایم بهستی نیست دایم</p></div>
<div class="m2"><p>که قیوم جهان بودن خداراست</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>رخش آئینه گیتی نما شد</p></div>
<div class="m2"><p>که اندر وی همه عالم هویداست</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>مرا از خط و خالش گشت روشن</p></div>
<div class="m2"><p>که روی خوب او عالم بیاراست</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>نگنجد ما و من در بزم وصلش</p></div>
<div class="m2"><p>که بزم وصل جانان بی من و ماست</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>بزیر پرده زلف سیاهش</p></div>
<div class="m2"><p>رخ پر نور او یا رب چه زیباست</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>اگر خواهی که گردد برتو روشن</p></div>
<div class="m2"><p>بدست آور دلی کو سرشناساست</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>منور کن بنور معرفت دل</p></div>
<div class="m2"><p>که پیش عارف این آمد ره راست</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>درو بنگر که بینی چون اسیری</p></div>
<div class="m2"><p>که هر ذره بدین معنی چه گویاست</p></div></div>
<div class="b2" id="bn150"><p>که عالم چون تن و جان جهان اوست</p>
<p>نهان در پرده کون و مکان اوست</p></div>
<div class="b" id="bn151"><div class="m1"><p>همه عالم بچشم من سیاهست</p></div>
<div class="m2"><p>که زلفش پرده روی چو ماهست</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>کسی کایات حسنش را نخواند</p></div>
<div class="m2"><p>ز اوراق جهان، او دل سیاهست</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>هرآنکو منکر دیدار یارست</p></div>
<div class="m2"><p>همه طاعات او عین گناهست</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>کسی را نقد عرفان گشت حاصل</p></div>
<div class="m2"><p>که او فارغ ز فکر مال و جاهست</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>بمعشوق ار چه ره بسیار باشد</p></div>
<div class="m2"><p>طریق عاشقی الحق چه راهست</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>بوصل او کجا ره می توان برد</p></div>
<div class="m2"><p>بما تا ذره مائی ما هست</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>به پیش آنکه دارد روشناسی</p></div>
<div class="m2"><p>جهان آئینه دار روی شاهست</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>اسیری آفتاب نوربخش است</p></div>
<div class="m2"><p>که ذرات دو عالم را پناهست</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>مرا از هاتف غیبی دمادم</p></div>
<div class="m2"><p>رسد این نکته چندین سال و ماهست</p></div></div>
<div class="b2" id="bn160"><p>که عالم چون تن و جان جهان اوست</p>
<p>نهان در پرده کون و مکان اوست</p></div>
<div class="b" id="bn161"><div class="m1"><p>ز شور جلوه های بی نهایت</p></div>
<div class="m2"><p>پر از آشوب و غوغا شد ولایت</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>همه عالم پر از روح و صفا شد</p></div>
<div class="m2"><p>ز انوار جمال جانفزایت</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>توئی معشوق و عالم جمله عاشق</p></div>
<div class="m2"><p>چنین بودست قسمت از بدایت</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>طلب کردم همه عمر و ندیدم</p></div>
<div class="m2"><p>بعالم هیچ مطلوبی ورایت</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>چو حسنت را نهایت نیست پیدا</p></div>
<div class="m2"><p>نباشد شوق ما را نیز غایت</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>اگر یک لحظه دیدارم نمائی</p></div>
<div class="m2"><p>هزاران جان و دل سازم فدایت</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>ندانم از چه روی خویش پوشی</p></div>
<div class="m2"><p>چو عالم هست مشتاق لقایت</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>بیا بنمابعالم روی خوبت</p></div>
<div class="m2"><p>جهان روشن کن از نور هدایت</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>همه ذرات گوید چون اسیری</p></div>
<div class="m2"><p>چو پیدا شد رخ گیتی نمایت</p></div></div>
<div class="b2" id="bn170"><p>که عالم چون تن و جان جهان اوست</p>
<p>نهان در پرده کون و مکان اوست</p></div>
<div class="b" id="bn171"><div class="m1"><p>دلا گر طالبی یکدم میارام</p></div>
<div class="m2"><p>که تا شاید بدست آری دلارام</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>تن اندر محنت و اندوه درده</p></div>
<div class="m2"><p>مگر که توسن نفست شود رام</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>کنون عمریست کاندر راه عشقش</p></div>
<div class="m2"><p>بناکامی مرا بگذشت ایام</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>درین اندیشه بودم گاه و بیگاه</p></div>
<div class="m2"><p>که از غیبم ندا آمد که ای خام</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>برو در خود تفکر کن زمانی</p></div>
<div class="m2"><p>ترا از تو شود حاصل همه کام</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>اگرچه حسن رویش را بعالم</p></div>
<div class="m2"><p>ظهوری بود و خواهد بود مادام</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>ولی ظاهر بانسان شد حقیقت</p></div>
<div class="m2"><p>که جز انسان نیابی مظهر تام</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>اسیری چون جمال نوربخشش</p></div>
<div class="m2"><p>که ماه و مهر نور از وی کند وام</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>عیان از پرده هر ذره دیدی</p></div>
<div class="m2"><p>باطراف جهان بفرست پیغام</p></div></div>
<div class="b2" id="bn180"><p>که عالم چون تن و جان جهان اوست</p>
<p>نهان در پرده کون و مکان اوست</p></div>