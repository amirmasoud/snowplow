---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>ما مست مدام کوی آن خماریم</p></div>
<div class="m2"><p>عالم همگی شراب و ما می خواریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لحظه جهان بجرعه نوشیدیم</p></div>
<div class="m2"><p>مست ابدیم و از ازل خماریم</p></div></div>