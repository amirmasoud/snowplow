---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>بینا بحقایق معانی مائیم</p></div>
<div class="m2"><p>دانا بمعارف بیانی مائیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکو بیقین بعلم و عین است بحق</p></div>
<div class="m2"><p>از جمله جهان اگر بدانی مائیم</p></div></div>