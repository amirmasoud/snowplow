---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>ما چون ز ازل مست و خراب آمده ایم</p></div>
<div class="m2"><p>با ساغر و جام و باشراب آمده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تقوی مطلب زما چواز حکم ازل</p></div>
<div class="m2"><p>با شاهد و باده و رباب آمده ایم</p></div></div>