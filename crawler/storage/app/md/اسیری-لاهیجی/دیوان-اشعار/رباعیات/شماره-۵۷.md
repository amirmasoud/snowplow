---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>ما جمله جهان مصحف ذاتت دانیم</p></div>
<div class="m2"><p>از هر ورقی آیت و صفت خوانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آنکه مدرسیم در مکتب عشق</p></div>
<div class="m2"><p>در معرفت کنه تو ما نادانیم</p></div></div>