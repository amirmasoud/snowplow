---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>شهباز فضای لامکانی مائیم</p></div>
<div class="m2"><p>سیمرغ هوای بی نشانی مائیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ره بخزانه حقیقت طلبی</p></div>
<div class="m2"><p>مفتاح در گنج معانی مائیم</p></div></div>