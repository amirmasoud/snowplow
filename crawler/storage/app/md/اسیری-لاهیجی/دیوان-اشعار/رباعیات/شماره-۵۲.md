---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>در پرتوحسن دلبری حیرانم</p></div>
<div class="m2"><p>کز مهر رخش چو ذره سرگردانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جور و جفای یار بی مهر و وفا</p></div>
<div class="m2"><p>درمانده بدام درد بی درمانم</p></div></div>