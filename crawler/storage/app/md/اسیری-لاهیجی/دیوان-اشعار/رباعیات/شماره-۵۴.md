---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>گفتی چو غمت رسد ترا غمخوارم</p></div>
<div class="m2"><p>شادت کنم و بغم دگر نگذارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که ز پا فتادم از درد فراق</p></div>
<div class="m2"><p>بردست وصال جان ما بردارم</p></div></div>