---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>درد تو دوای این دل رنجورست</p></div>
<div class="m2"><p>وصل تو شفای جان هر مهجورست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کو ز جهان جمال روی توندید</p></div>
<div class="m2"><p>نزدیک محققان ز عرفان دورست</p></div></div>