---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>عالم ز جمال تو منور بینم</p></div>
<div class="m2"><p>برصورت آدمت مصور بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکس که بدولت لقای تو رسید</p></div>
<div class="m2"><p>شادی جهان ورا میسر بینم</p></div></div>