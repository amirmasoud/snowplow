---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>جز عشق تو نیست در جهان حاصل من</p></div>
<div class="m2"><p>جز درد و غمت مباد اندر دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ملک طرب خانه طلب می کردم</p></div>
<div class="m2"><p>جز برسر کوی غم نشد منزل من</p></div></div>