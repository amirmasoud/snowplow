---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>مائیم مجرد از اضافات و نسب</p></div>
<div class="m2"><p>آزاده ز قید وصف مربوب وز رب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مرتبه ذات معرا ز صفات</p></div>
<div class="m2"><p>هرگز نبود نام ز مطلوب و طلب</p></div></div>