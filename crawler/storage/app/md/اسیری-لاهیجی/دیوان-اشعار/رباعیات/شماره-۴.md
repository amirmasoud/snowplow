---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ای ملک فراق جای دیرینه ما</p></div>
<div class="m2"><p>مأوای غم تو خانه سینه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظاهر شده عکس جمله اسما و صفات</p></div>
<div class="m2"><p>در مظهر تام قلب بی کینه ما</p></div></div>