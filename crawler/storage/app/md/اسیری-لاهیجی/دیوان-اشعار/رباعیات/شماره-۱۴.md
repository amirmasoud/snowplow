---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>هر لحظه جمال دوست دیدن چه خوش است</p></div>
<div class="m2"><p>زان لب سخن بوسه شنیدن چه خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بربوی وصال او دل و جان مرا</p></div>
<div class="m2"><p>دامن ز همه جهان کشیدن چه خوش است</p></div></div>