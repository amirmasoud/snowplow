---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>روی تو ز مرآت جهان می بینم</p></div>
<div class="m2"><p>حسنت ز همه جهان عیان می بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غیرت اگر پرده برو افکندی</p></div>
<div class="m2"><p>در پرده کون ترا نهان می بینم</p></div></div>