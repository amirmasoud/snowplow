---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>داری سر جور و بیوفائی با ما</p></div>
<div class="m2"><p>پیوسته ازآن کنی جدائی باما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب بود این که از سر مهر و وفا</p></div>
<div class="m2"><p>رخسار چو خورشید نمائی با ما</p></div></div>