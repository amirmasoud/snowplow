---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>ما بنده عشق و از خرد آزادیم</p></div>
<div class="m2"><p>با درد و غم عشق تو بس دلشادیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون حاصل عمر ما بجز عشق تو نیست</p></div>
<div class="m2"><p>گوئی مگر از برای عشقت زادیم</p></div></div>