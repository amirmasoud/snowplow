---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>دریای دلم نه قعر دارد نه کران</p></div>
<div class="m2"><p>چون ذره به پیش او همه کون و مکان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مظهر علم حق بود ای نادان</p></div>
<div class="m2"><p>پیدا و نهان ازآن درو گشته عیان</p></div></div>