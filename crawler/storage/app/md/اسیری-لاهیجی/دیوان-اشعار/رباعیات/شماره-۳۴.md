---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>ای حسن تو از روی بتان کرده ظهور</p></div>
<div class="m2"><p>روی تو بپرده جهان شد مستور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهاد ز درد هجر تو رنجورند</p></div>
<div class="m2"><p>عشاق ز باده وصالت مخمور</p></div></div>