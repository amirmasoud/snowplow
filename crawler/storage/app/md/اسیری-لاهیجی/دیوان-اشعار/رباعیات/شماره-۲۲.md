---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>آنجا که منم نه ذات باشد نه صفات</p></div>
<div class="m2"><p>نه انجم و افلاک و عناصر نه جهات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه کشف و تجلی و نه حال و نه مقام</p></div>
<div class="m2"><p>نه جنت و دوزخ و نه نور و ظلمات</p></div></div>