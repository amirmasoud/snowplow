---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>ای مونس جان من بگاه و بیگاه</p></div>
<div class="m2"><p>بی یاد تو کار دل تباهست تباه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشتاق جمال تو چنانم که دمی</p></div>
<div class="m2"><p>گر غایبم از خیال تو واویلاه</p></div></div>