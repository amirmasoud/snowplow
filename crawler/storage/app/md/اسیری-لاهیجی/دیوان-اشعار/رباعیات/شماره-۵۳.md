---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>عمری بامید وصل دلبر بودم</p></div>
<div class="m2"><p>در آتش هجر او چو اخگر بودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاره بحریم حرمتش یافت دلم</p></div>
<div class="m2"><p>دایم ز طلب چو حلقه بردر بودم</p></div></div>