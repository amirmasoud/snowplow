---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>رندیم و حریف شاهد و پیمانه</p></div>
<div class="m2"><p>مخمور دو چشم جادوی مستانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روز ازل نصیب ما عشق تو بود</p></div>
<div class="m2"><p>زان روی شدم بعاشقی افسانه</p></div></div>