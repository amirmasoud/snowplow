---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>ما را بجهان نیست بجز یک هوسی</p></div>
<div class="m2"><p>کایی و به پیش ما نشینی نفسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعل لب خود بپرسشی بگشائی</p></div>
<div class="m2"><p>گوئی که بگو تو از کجائی چه کسی؟</p></div></div>