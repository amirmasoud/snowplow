---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>تا در رخ خوب تو نگاهی کردم</p></div>
<div class="m2"><p>هر روز ز اشتیاقش آهی کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا گشت دلم به بند عشقت پابند</p></div>
<div class="m2"><p>از دست غم تو روبراهی کردم</p></div></div>