---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>جانا ز جهان جمال تو می بینم</p></div>
<div class="m2"><p>از باغ جهان گل وصالت چینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دایم ز جهان مرا چو مشهود توئی</p></div>
<div class="m2"><p>در مرتبه شهود بی تلوینم</p></div></div>