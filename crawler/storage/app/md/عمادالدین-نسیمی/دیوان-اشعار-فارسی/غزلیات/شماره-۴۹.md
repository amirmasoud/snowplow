---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>لوح محفوظ است پیشانی و قرآن روی دوست</p></div>
<div class="m2"><p>«کل شی ء هالک » لاریب اندر شأن اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمه حیوان کز او شد زنده جاوید خضر</p></div>
<div class="m2"><p>در بهشت روی او دیدم روان آن چارجوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی تواند یافت از ماهیت معنی خبر</p></div>
<div class="m2"><p>آن که در باغ جهان حیران و مست رنگ و بوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند باشی بسته ظن و بعید از معرفت؟</p></div>
<div class="m2"><p>طالب مغزی شو آخر چند گردی گرد پوست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهد غیبی به معنی هست حاضر با همه</p></div>
<div class="m2"><p>غافل کوته نظر چندین چرا در جست و جوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای به گرد معصیت آلوده دامن عمرها</p></div>
<div class="m2"><p>آب رحمت آمد آگه شو که وقت شست و شوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همنشینت خضر و در ظلمات جهلی گم شده</p></div>
<div class="m2"><p>از عطش مردی و آب سلسبیلت در سبوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای ز غفلت در حجاب سر وجه الله اگر</p></div>
<div class="m2"><p>طالب دیدار حقی وجه حق روی نکوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کشد عشقت نسیمی را و احیا می کند</p></div>
<div class="m2"><p>عشق هستی سوز را با هر که هست این طبع و خوست</p></div></div>