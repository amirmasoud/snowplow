---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>تا منور شد ز خورشید رخ او دیده‌ام</p></div>
<div class="m2"><p>در همه اشیا ظهور صورت او دیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مذاق جان من بوی دم عیسی نرفت</p></div>
<div class="m2"><p>تا چو موسی نطق آن شیرین‌دهان بشنیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کافرم گر، دیده‌ام بی‌عشق او چندان که من</p></div>
<div class="m2"><p>گرد اقلیم وجود خویشتن گردیده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی کنم چون زاهد خام آرزوی خانقاه</p></div>
<div class="m2"><p>من که در میخانه چون می سال‌ها جوشیده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای به خوبی فرد و واحد در دو عالم جز رخت</p></div>
<div class="m2"><p>قبله‌ای گر هست من زان قبله برگردیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارد از دنیی و عقبی هرکسی بگزیده‌ای</p></div>
<div class="m2"><p>از همه دنیی و عقبی من تو را بگزیده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به شئ الله از لب داده‌ای جامی مرا</p></div>
<div class="m2"><p>صد فریدون را ز چشمت جام جم بخشیده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه عمری بودم از سودای زلفت بی‌قرار</p></div>
<div class="m2"><p>تا شدم بیمار چشم مستت آرامیده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوش در می، ساقی لعلت نمی‌دانم چه ریخت</p></div>
<div class="m2"><p>کز خمارش تا به روز امشب به سر غلتیده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا ز وصلت بشنوم روزی درایی چون جرس</p></div>
<div class="m2"><p>بر درت شب‌ها به زاری تا سحر نالیده‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر زمان می‌پوشم از تو خلعت دردی ز نو</p></div>
<div class="m2"><p>از تو چون پوشانم آن‌ها کز تو من پوشیده‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برقع از رخسار گلگون تا برافکندی چو سرو</p></div>
<div class="m2"><p>بر گل خودروی خندان در چمن خندیده‌ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای دلم رنجور سودای تو، هر جانی که او</p></div>
<div class="m2"><p>از چنین سودا نه رنجور است، از او رنجیده‌ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>(ای به قدر و رفعت افزون صد ره از کون و مکان</p></div>
<div class="m2"><p>یک به یک پیموده‌ام من مو به مو سنجیده‌ام)</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت چشمت: ای نسیمی از که مستی؟ گفتمش</p></div>
<div class="m2"><p>جام سودای تو در بزم ازل نوشیده‌ام</p></div></div>