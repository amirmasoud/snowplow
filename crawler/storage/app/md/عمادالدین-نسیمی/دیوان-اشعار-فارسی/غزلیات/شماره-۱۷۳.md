---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>در ضمیرم روز و شب نقش تو می‌بندد خیال</p></div>
<div class="m2"><p>جز تو نقشی در خیالم صورتی باشد محال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست با عشقت مرا پیوند جانی تا ابد</p></div>
<div class="m2"><p>جاودان زان با توام هرجا که هستم در وصال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان من با مهر رویت الفتی دارد چنان</p></div>
<div class="m2"><p>کز وجود خویش و از کون و مکان دارد ملال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم از خورشید رویت آتشی در جان و دل</p></div>
<div class="m2"><p>وز خیال نقش ابروی تو هستم چون هلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قامت سرو گل اندام تو در باغ بهشت</p></div>
<div class="m2"><p>بشکند بازار طوبی را به حد اعتدال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آرزومند جمال کعبه وصل ترا</p></div>
<div class="m2"><p>آتش شوق تو در جان خوشتر از آب زلال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واقف سر «سواد الوجه فی الدارین » هست</p></div>
<div class="m2"><p>هر که این معنی بجست از ابجد آن زلف و خال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش رخسارت گل از شرم آب گردد در زمان</p></div>
<div class="m2"><p>گر کنی عزم گلستان با چنین حسن و جمال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتابی شد نسیمی در هوای او بلی</p></div>
<div class="m2"><p>ذره را خورشید سازد همت صاحب کمال</p></div></div>