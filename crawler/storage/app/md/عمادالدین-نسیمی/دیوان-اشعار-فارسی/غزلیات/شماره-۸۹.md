---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>دلم ز مهر تو آن دم چو صبح دم می زد</p></div>
<div class="m2"><p>که آفتاب رخت در قدم علم می زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جام عشق تو بودم خراب و مست آنروز</p></div>
<div class="m2"><p>که نقش بند قضا، رسم جام جم می زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بوی زلف تو آشفته آن زمان بودم</p></div>
<div class="m2"><p>که منشی کن از آن کاف و نون به هم می زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود خانه چشمم هنوز بر بنیاد</p></div>
<div class="m2"><p>که عشق روی تو بر جان در حرم می زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی که دیده من خلوت خیال تو بود</p></div>
<div class="m2"><p>فلک هنوز سراپرده بر عدم می زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جست و جوی وصال تو من کجا بودم</p></div>
<div class="m2"><p>که در جهان قدم جان من قدم می زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنوز چهره شادی ز عقل پنهان بود</p></div>
<div class="m2"><p>که عشق بر رخ جانم نشان غم می زد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنوز خامه فطرت به امر «کن » جاری</p></div>
<div class="m2"><p>نگشته بود که بر من غمت رقم می زد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلیم و طور هنوز از عدم خبر می داد</p></div>
<div class="m2"><p>که جان من «ارنی » با تو دم به دم می زد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا شود ز خطا پاک نامه عملم</p></div>
<div class="m2"><p>اگر نه منشی عفوت بر آن قلم می زد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چگونه قلب نسیمی چو زر شدی رایج</p></div>
<div class="m2"><p>اگر نه فضل تواش سکه بر درم می زد</p></div></div>