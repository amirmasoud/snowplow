---
title: >-
    شمارهٔ ۱۸۲
---
# شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>من شاهباز زاده شاه شریعتم</p></div>
<div class="m2"><p>از بهر صید طایر قدس حقیقتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاووس باغ انسم و سیمرغ باغ قدس</p></div>
<div class="m2"><p>عنقای برج عزت و شاه شریعتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاهوتیم که هم بر ناسوت گشته‌ام</p></div>
<div class="m2"><p>تا جزء و کل شناسم و هم بعد و قربتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آری ز ملک تا ملکوتم گذر بود</p></div>
<div class="m2"><p>جبروت منزلم شده لاهوت خلوتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانسته ام که مبداء و میعاد من کجاست</p></div>
<div class="m2"><p>اینجا اگر چه پاسی در قید صورتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آدم نبود و عالم و نه جن و نه ملک</p></div>
<div class="m2"><p>کو کرد ظاهرم چو یم از نور فطرتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا آمدم ز عالم علوی در این مقام</p></div>
<div class="m2"><p>دایم از این فراق گرفتار محنتم</p></div></div>