---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>باز آمد آن خورشید جان در رخ نقاب انداخته</p></div>
<div class="m2"><p>وز عنبر تر برقعی بر آفتاب انداخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرین لب جان پرورش بشکسته بازار شکر</p></div>
<div class="m2"><p>سودای چشمش مستی ای اندر شراب انداخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای سنبلت روز مرا از چهره چون شب ساخته</p></div>
<div class="m2"><p>وی غمزه ات بخت مرا در دیده خواب انداخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دیده صورتگران حیران بماند در رخت</p></div>
<div class="m2"><p>هست از خیالت نقش ها در خاک و آب انداخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای موسی یوسف لقا در خیمه میقات ما</p></div>
<div class="m2"><p>زلف تو از هر جانبی پنجه طناب انداخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای رشته جان مرا زلف جهانسوز رخت</p></div>
<div class="m2"><p>از طره عنبرشکن در پیچ و تاب انداخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از عشق رویت در جهان، ای آفتاب عاشقان</p></div>
<div class="m2"><p>سر تا قدم گنجم ولی خود در خراب انداخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این آتش قدسی مرا هرگز نخواهد کم شدن</p></div>
<div class="m2"><p>سوزی که هست آن از توام در جان کباب انداخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این شربت قند لبت در آرزوی وصل خود</p></div>
<div class="m2"><p>چندین هزاران تشنه را سر در سراب انداخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما را به زهد ای مدعی! دعوت مکن بیهوده چون</p></div>
<div class="m2"><p>هست آنکه عاشق می شود چشم از ثواب انداخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای از بیاض عارضت زلف سیه دل روز و شب</p></div>
<div class="m2"><p>جان من آشفته را در اضطراب انداخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای برده زلف کافرت آرام و عقل مرد و زن</p></div>
<div class="m2"><p>وی چشم جادویت فغان در شیخ و شاب انداخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای بر درت کاف کنف انوار کوکب ریخته</p></div>
<div class="m2"><p>وی پیش مرجانت صدف در خوشاب انداخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا بوی زلف و عارضت شد با نسیمی هم‌نفس</p></div>
<div class="m2"><p>بر آتشت آهو و گل مشک و گلاب انداخته</p></div></div>