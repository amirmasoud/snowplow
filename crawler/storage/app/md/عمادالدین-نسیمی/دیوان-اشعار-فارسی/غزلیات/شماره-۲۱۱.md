---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>تو رسم دلبری داری و دانی دلبری کردن</p></div>
<div class="m2"><p>ولیکن من ز تو مشکل توانم دل بری کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن تشبیه رویش را به گلبرگ طری زانرو</p></div>
<div class="m2"><p>که نتوان نسبت آن گل را به گلبرگ طری کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر داری سر عشقش ز فکر جان و تن بگذر</p></div>
<div class="m2"><p>که کار طبع خامان است فکر سرسری کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هاروت ار چه آموزند مردم ساحری لیکن</p></div>
<div class="m2"><p>کجا چون مردم چشمت تواند ساحری کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی کو شد هوادارت تواند دم زد از جایی</p></div>
<div class="m2"><p>سری کافتاد در پایت تواند سروری کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگو صورتگر چین را مکن اندیشه رویش</p></div>
<div class="m2"><p>که نقاش ازل داند چنین صورتگری کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیال شمع رخسارش کسی کو در نظر دارد</p></div>
<div class="m2"><p>نظر باشد حرام او را به ماه و مشتری کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سری کز خاک درگاهت چو گردون سربلند آمد</p></div>
<div class="m2"><p>نخواهد گردن افرازی به تاج سنجری کردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به وصف چشم جادوی تو اشعار نسیمی را</p></div>
<div class="m2"><p>سراسر می‌توان نسبت به سحر سامری کردن</p></div></div>