---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>ای دهانت پسته خندان من</p></div>
<div class="m2"><p>خاک پایت چشمه حیوان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف و رخسار تو، ای خورشید حسن!</p></div>
<div class="m2"><p>لیلة القدر و مه تابان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان شیرینم فدای لعل تو</p></div>
<div class="m2"><p>کو بسی شیرین تر است از جان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بهشت جاودانم تا که هست</p></div>
<div class="m2"><p>روضه کویت سرابستان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داروی درمان من درد تو بس</p></div>
<div class="m2"><p>ای دوای درد بی درمان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آتش عشق تو، هردم می رود</p></div>
<div class="m2"><p>بر فلک دود دل سوزان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز بختم بی رخت تاریک شد</p></div>
<div class="m2"><p>ای چراغ دیده گریان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترسم انجامد به طوفان در غمت</p></div>
<div class="m2"><p>رستخیز اشک چون مرجان من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سنبلت در هر زمان داغی نهد</p></div>
<div class="m2"><p>بر دل مجروح سرگردان من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل بر آتش چون کباب افتاده است</p></div>
<div class="m2"><p>تا غم عشق تو شد مهمان من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کفر زلفت با نسیمی درگرفت</p></div>
<div class="m2"><p>ای رخت دین من و ایمان من</p></div></div>