---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>صبح از افق بنمود رخ، در گردش آور جام را</p></div>
<div class="m2"><p>وز سر خیال غم ببر، این رند دُردآشام را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای صوفی خلوت‌نشین بستان ز رندان کاسه‌ای</p></div>
<div class="m2"><p>تا کی پزی در دیگ سر، ماخولیای خام را؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایام را ضایع مکن، امروز را فرصت شمار</p></div>
<div class="m2"><p>بیدادی دوران ببین، دادی بده ایام را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای چرخ زرگر! خاک من زرساز تا جامی شود</p></div>
<div class="m2"><p>باشد که بستاند لبم زان لعل شیرین‌کام را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد روزه‌دار و متقی، امروز نامم در جهان</p></div>
<div class="m2"><p>فردا به محشر چون برم یارب ز ننگ این نام را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی زنی لاف از عمل، بتخانه در زیر بغل</p></div>
<div class="m2"><p>ای ساجد و عابد شده دائم، ولی اصنام را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای شمع اگر باد صبا یابی شبی در مجلسش</p></div>
<div class="m2"><p>از عاشق بیدل بگو با دلبر این پیغام را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کای از شب زلفت سیه‌روز پریشان بخت من</p></div>
<div class="m2"><p>کی روز گردانم شبی با صبح رویت شام را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای غرّه فردا مکن دعوت به حورم زان که من</p></div>
<div class="m2"><p>امروز حاصل کرده‌ام محبوب سیم‌اندام را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای زلف و خال رهزنت صیاد مرغ جان و دل</p></div>
<div class="m2"><p>وه وه که خوب آورده‌ای این دانه و آن دام را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌آن قد همچون الف، لامی شد از غم قامتم</p></div>
<div class="m2"><p>پیچیده کی بینم شبی با آن الف این لام را؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاک نسیمی در ازل شد با شراب آمیخته</p></div>
<div class="m2"><p>ای ساقی مهوش بیار آن آب آتش‌فام را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می با جوانان خوردنم، خاطر تمنا می‌کند</p></div>
<div class="m2"><p>تا کودکان در پی فتند این پیر دُردآشام را</p></div></div>