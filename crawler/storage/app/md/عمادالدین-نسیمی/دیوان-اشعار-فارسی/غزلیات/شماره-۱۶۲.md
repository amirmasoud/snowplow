---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>ز من که طایر قافم نشان عنقا پرس</p></div>
<div class="m2"><p>ز من که ماهی عشقم رسوم دریا پرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>(ز من که خادم خمار و ساکن دیرم</p></div>
<div class="m2"><p>رموز باده اسرار و جام صهبا پرس)</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صفای باطن رندان مست دردآشام</p></div>
<div class="m2"><p>به نور طلعت جام از می مصفا پرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیث توبه و زهد از کجا و من ز کجا</p></div>
<div class="m2"><p>بیان این خبر از زاهدان رعنا پرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا که چشم تو باشد همیشه در خاطر</p></div>
<div class="m2"><p>ز ناتوانی و مستی و عشق و سودا پرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرچه از غم یوسف ضریر شد یعقوب</p></div>
<div class="m2"><p>بیا و لذت عشق از دل زلیخا پرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقیم صومعه داند رسوم سالوسی</p></div>
<div class="m2"><p>ز من که عابد خورشیدم از مسیحا پرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ره ریا و تکلف ز شیخ و واعظ جوی</p></div>
<div class="m2"><p>طریق شیوه اهل حقیقت از ما پرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیار باده و بنشین و دم غنیمت دان</p></div>
<div class="m2"><p>نسیمی مست و خراب است حال دنیا پرس</p></div></div>