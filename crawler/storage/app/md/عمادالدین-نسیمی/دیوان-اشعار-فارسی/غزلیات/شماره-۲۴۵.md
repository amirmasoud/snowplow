---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>ای جان عاشق از لب جانان ندا شنو</p></div>
<div class="m2"><p>آواز «ارجعی » به جهان بقا شنو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سالکان عالم غیبی ز هر طرف</p></div>
<div class="m2"><p>چندین هزار مژده وصل و لقا شنو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم صدای صوت «اناالحق » فرو گرفت</p></div>
<div class="m2"><p>ای سامع! این سخن تو به سمع رضا شنو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آنکه اهل میکده را منکری بیا</p></div>
<div class="m2"><p>از صوفیان صومعه بوی ریا شنو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صوفی کجا و ذوق می صاف از کجا</p></div>
<div class="m2"><p>این نکته را ز درد کش آشنا شنو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سوز عود و نغمه چنگ و نوای نی</p></div>
<div class="m2"><p>شرح درون خسته پردرد ما شنو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر صبحدم شمامه آن زلف عنبرین</p></div>
<div class="m2"><p>ز انفاس روح پرور باد صبا شنو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای سروناز بر سر و چشمم ز روی لطف</p></div>
<div class="m2"><p>بنشین دمی و قصه این ماجرا شنو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی ز روی لطف که: «ادعونی استجب »</p></div>
<div class="m2"><p>بنگر به سوی ما و هزاران دعا شنو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بعد از وفات بر سر خاک و عظام من</p></div>
<div class="m2"><p>بگذر دمی و غلغله مرحبا شنو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکدم عنان زلف پریشان به دست باد</p></div>
<div class="m2"><p>بگذار و حال نافه مشک خطا شنو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روزی خطاب کن ز کرم کای گدای من!</p></div>
<div class="m2"><p>کوس جلال و طنطنه کبریا شنو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شرح غم نسیمی آشفته مو به مو</p></div>
<div class="m2"><p>ای باد صبح زان سر زلف دو تا شنو</p></div></div>