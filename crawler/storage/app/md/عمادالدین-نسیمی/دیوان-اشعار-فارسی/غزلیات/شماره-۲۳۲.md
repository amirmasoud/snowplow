---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>روی خداست ای صنم روی تو، رای من ببین</p></div>
<div class="m2"><p>وز رخ همچو مصحفت فال برای من ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار به عشوه خون من خورد و حلال کردمش</p></div>
<div class="m2"><p>جور و جفای او نگر، مهر و وفای من ببین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نافه مشک چین اگر با تو دم از خطا زند</p></div>
<div class="m2"><p>روی سیاه را بگو زلف دو تای من ببین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>(پیش تو بر زمین چو زد مردم دیده اشک را</p></div>
<div class="m2"><p>گفت به اشک پهلوان، مشک سقای من ببین)</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>(کشت مرا و زنده کرد از لب جانفزای خود</p></div>
<div class="m2"><p>لطف نگار من چها کرد برای من ببین)</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لعل لب تو بوسه ای داد به خونبهای من</p></div>
<div class="m2"><p>طالع و بخت من نگر، قدر و بهای من ببین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنبل زلفت آرزو کرده ام ای خجسته فال!</p></div>
<div class="m2"><p>نقش و خیال مختلف، فکر خطای من ببین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>(دامن وصل تو به کف بخت نداد و عمر شد</p></div>
<div class="m2"><p>آتش جان گداز من باد و هوای من ببین)</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وهم پرست را بگو، بگذر از این خیال و ظن</p></div>
<div class="m2"><p>در رخ یار من نگر، روی خدای من ببین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی سر و پای عشق شو همچو فلک نسیمیا</p></div>
<div class="m2"><p>سر «الست ربکم » در سر و پای من ببین</p></div></div>