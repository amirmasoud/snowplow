---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>عقل را سودای گیسوی تو مجنون می‌کند</p></div>
<div class="m2"><p>فکر آن زنجیر پر سودا عجب چون می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست ابروی تو آن حرفی که نامش را اله</p></div>
<div class="m2"><p>در کلام کبریا قبل از «قلم» «نون» می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صورت روی تو بر هر دل که می‌آید فرو</p></div>
<div class="m2"><p>نقش هر اندیشه را زان خانه بیرون می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که می‌خواند به لؤلؤ نظم دندان ترا</p></div>
<div class="m2"><p>بی‌ادب، کم‌حرمتی با دُرّ مکنون می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ازل با عشق رویت جان ما بود آشنا</p></div>
<div class="m2"><p>عشقبازی جان من با تو نه اکنون می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق ما زان لایزال آمد که عیش مست عشق</p></div>
<div class="m2"><p>نیست آن عشقی که مست خمر و افیون می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم بهبودی چه داری زان طبیب ای دل که او</p></div>
<div class="m2"><p>چاره بیماری سودا به معجون می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه را نامش به درویشی برآمد بر درت</p></div>
<div class="m2"><p>کی نظر در ملک جم یا گنج قارون می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز آتش مهرت وجودم گرچه می‌کاهد چو شمع</p></div>
<div class="m2"><p>جانم آن سوزی که دارد در دل افزون می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خرقه خلوت‌نشینان چون سیاه و ازرق است</p></div>
<div class="m2"><p>ای خوشا آن کو به می رخساره گلگون می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر نسیمی سایه زلف تو تا افتاده است</p></div>
<div class="m2"><p>سلطنت در تحت آن ظل همایون می‌کند</p></div></div>