---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>آرزومندی و درد هجر یار از حد گذشت</p></div>
<div class="m2"><p>در غمش صبر دل امیدوار از حد گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه دلشادم به امید شب وصلت، ولی</p></div>
<div class="m2"><p>محنت هجران و جور روزگار از حد گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه بر راه خیالش دیده می دارم نگاه</p></div>
<div class="m2"><p>انتظار وصل روی آن نگار از حد گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی بنمای ای گل خندان که بی وصل رخت</p></div>
<div class="m2"><p>بر دل مجروح بلبل زخم خار از حد گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرط عاشق نیست از بیداد نالیدن، ولی</p></div>
<div class="m2"><p>جور آن آشفته زلف بی قرار از حد گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر امید جام نوشین شراب لعل دوست</p></div>
<div class="m2"><p>خوردن خون دل و درد خمار از حد گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آب مژگانم حذر کن کز غم رویت مرا</p></div>
<div class="m2"><p>گریه جان سوز و چشم اشکبار از حد گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کمند زلفت ای مه، از کمانداران چرخ</p></div>
<div class="m2"><p>تیرباران بر من لاغر شکار از حد گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بار هجرانت نسیمی بارها بر جان کشید</p></div>
<div class="m2"><p>دل ضعیف است ای نگار این بار، بار از حد گذشت</p></div></div>