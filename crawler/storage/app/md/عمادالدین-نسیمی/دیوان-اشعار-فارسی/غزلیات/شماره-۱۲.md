---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>ای رخت از روی حسن آیینه گیتی نما</p></div>
<div class="m2"><p>وی قدت چون طوبی از خوبی به صد نشو و نما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که جان بازد ز شوق روی خوبت جان من</p></div>
<div class="m2"><p>عاشقان خویش را ای ماه گاهی رو نما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون رخت مقصود خلق است، کعبه عشاق هم</p></div>
<div class="m2"><p>هست از این هر عالمی عاشق تو را تنها نه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان جانها چون سر زلف تو آمد لاجرم</p></div>
<div class="m2"><p>گر تو ننمایی رخت هردم رود صد جان ز ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقیا بر یاد چشم مست یار دلربا</p></div>
<div class="m2"><p>خیز و در عین قدح ریز آن می راحت فزا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیز و در بحر محیط می فکن کشتی جام</p></div>
<div class="m2"><p>تا چو ما گردی در این دریا به حکمت آشنا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرمت می دار کز بیت الحرام آورده اند</p></div>
<div class="m2"><p>تا شوی از شرب او واقف ز اسرار قضا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تلخی‌اش را حق شمر چون گفته‌اند «الحق مر»</p></div>
<div class="m2"><p>رنگ و بویش را مدان باطل که آن نبود روا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زاهدا! نوشیدن می از سر اخلاص و صدق</p></div>
<div class="m2"><p>بهتر از ورزیدن زهد است با شید و ریا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در هوای دلبران عمر نسیمی صرف شد</p></div>
<div class="m2"><p>وز همه در عمر خود هرگز نمی‌بیند وفا</p></div></div>