---
title: >-
    شمارهٔ ۵۶ - مهاجرت از باکو
---
# شمارهٔ ۵۶ - مهاجرت از باکو

<div class="b" id="bn1"><div class="m1"><p>خاک باد آن سر که در وی سر سودای تو نیست</p></div>
<div class="m2"><p>دور باد از شادی، آن کو یار غم‌های تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو در بالا کمال راستی دارد ولی</p></div>
<div class="m2"><p>در کمال حسن و زیبایی چو بالای تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه خورشید اقتباس از شمع رویت می کند</p></div>
<div class="m2"><p>روشن و تابان چو نور صبح سیمای تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لا نظیری در جهان حسن و لطف و دلبری</p></div>
<div class="m2"><p>سر برآر از جیب یکتایی که همتای تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی درآرندش به چشم اهل نظر چون توتیا</p></div>
<div class="m2"><p>آن که او چون خاک راه افتاده در پای تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن که در بند سر و جان است و فکر دین و دل</p></div>
<div class="m2"><p>خودپرست و پست همت مرد سودای تو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست از اهل بصیرت آن که او را چشم جان</p></div>
<div class="m2"><p>تا ابد روشن به روی عالم آرای تو نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کعبه ارباب تحقیق است رویت زان جهت</p></div>
<div class="m2"><p>قبله تحقیق ما جز روی زیبای تو نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی به ذیل عروة الوثقی تمسک باشدش</p></div>
<div class="m2"><p>هر که را حبل المتین زلف سمن سای تو نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کجی ماند به ابروی تو ماه نو ولی</p></div>
<div class="m2"><p>راستی را مثل ابروی چو طغرای تو نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای نسیمی چون خدا گفت: «ان ارضی واسعه»</p></div>
<div class="m2"><p>خطه «باکو» به جا بگذار کاین جای تو نیست</p></div></div>