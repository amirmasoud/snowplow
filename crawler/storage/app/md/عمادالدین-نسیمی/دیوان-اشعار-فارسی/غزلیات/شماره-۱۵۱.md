---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>دوش باز آمد به برج آن طالع ماهم دگر</p></div>
<div class="m2"><p>دولتم شد یار و بخت سعد همراهم دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدتی عقلم ز راه عشق گمره گشته بود</p></div>
<div class="m2"><p>جذبه لطفش کشید، آورد با راهم دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خیالم فکر زهد و توبه و طامات بود</p></div>
<div class="m2"><p>عشق آن بت رخ نمود از پرده ناگاهم دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داشتم چون غنچه مستور آتش دل در درون</p></div>
<div class="m2"><p>کرد رسوایش چنین آن دود و این آهم دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آب چشمم پای در گل بود آن سرو بلند</p></div>
<div class="m2"><p>باز چون بید است بر سر دست کوتاهم دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهر آن خورشید تابان بر دلم چون ماه نو</p></div>
<div class="m2"><p>هردم افزون گشت و من چون شمع می کاهم دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان دهم من، هرشبی چون شمع، باد صبحدم</p></div>
<div class="m2"><p>زنده می سازد به بویش هر سحرگاهم دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یار سنبل مو که جوجو خرمن عمرم بسوخت</p></div>
<div class="m2"><p>می دهد بر باد سودا باز چون کاهم دگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من ز چشم مست ساقی در خمارم روز و شب</p></div>
<div class="m2"><p>مستی این می مرا بس، می نمی خواهم دگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نسیمی من نخواهم توبه کرد از روی خوب</p></div>
<div class="m2"><p>این نصیحت کم کن ای زاهد، به اکراهم دگر</p></div></div>