---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>عارفان روی تو را نور یقین می‌خوانند</p></div>
<div class="m2"><p>عروه موی تو را حبل متین می‌خوانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه بر لوح قضا منشی تقدیر نوشت</p></div>
<div class="m2"><p>عاشقانت ز رخ و زلف و جبین می‌خوانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صفت چشم تو است آیت «مازاغ» از آن</p></div>
<div class="m2"><p>گوشه‌گیران دو ابروی تو این می‌خوانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظم دندان تو را کآب حیاتش نام است</p></div>
<div class="m2"><p>خرده‌بینان تواَش دُرّ ثمین می‌خوانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنت عدن سر کوی تو را مشتاقان</p></div>
<div class="m2"><p>صحن باغ ارم و خُلد برین می‌خوانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیدلانی که مدام از سر سودا مستند</p></div>
<div class="m2"><p>مردم چشم تو را گوشه‌نشین می‌خوانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر، آن زمره که گویند به روی تو خطاست</p></div>
<div class="m2"><p>نقش‌های غلط و لعبت چین می‌خوانند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل و دین می‌برد از خلق رخت زان جهتش</p></div>
<div class="m2"><p>آفت خلق و بلای دل و دین می‌خوانند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنت و حور و لقا گرچه به وجه دگر است</p></div>
<div class="m2"><p>اهل دل نور سماوات و زمین می‌خوانند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آب حیوان که لب لعل تو است، آن به یقین</p></div>
<div class="m2"><p>در بهشت ابدش ماء معین می‌خوانند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نسیمی ز تو آنان که رسیدند به حق</p></div>
<div class="m2"><p>جاودان مصحف روی تو چنین می‌خوانند</p></div></div>