---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>ای کعبه جمال توام قبله صلات</p></div>
<div class="m2"><p>حسن رخ تو داده به خورشید و مه زکات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذرات کاینات به مهر تو قایمند</p></div>
<div class="m2"><p>چون عالم صفات که قایم بود به ذات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ادراک حسن روی تو خفاش کی کند</p></div>
<div class="m2"><p>ای آفتاب روی تو مستجمع صفات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روشن شد اینک روی تو در لات دیده اند</p></div>
<div class="m2"><p>آنان که در کنشت پرستیده اند لات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نازکی رخ چو مهت بر بساط حسن</p></div>
<div class="m2"><p>لیلاج عقل را به دو منصوبه کرده مات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کاینات غیر تو کس را وجود نیست</p></div>
<div class="m2"><p>ای یافته وجود به ذات تو کاینات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرک است در طریق حقیقت دویی، ولی</p></div>
<div class="m2"><p>ما خضر تشنه ایم و تویی چشمه حیات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دم درکش از بیان لب لعلش ای خرد</p></div>
<div class="m2"><p>کافزون ز وسع کوزه بود دجله و فرات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلفش به راستی شب قدر است و راست است</p></div>
<div class="m2"><p>گر خوانمش به وجه دگر لیلة البرات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن کو زفضل حق چو نسیمی به حق رسید</p></div>
<div class="m2"><p>شمع هدایت آمد و پروانه نجات</p></div></div>