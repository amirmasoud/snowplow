---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>ای جمالت نسخه اسماء حسنی آمده</p></div>
<div class="m2"><p>وی خم ابروت ما اوحی و اوحی آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمده در شأن چشمت رمز ما زاغ البصر</p></div>
<div class="m2"><p>روی و مویت را بیان یس و طاها آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسته از لعل لبت یاقوت را خون در جگر</p></div>
<div class="m2"><p>در دندان تو از لؤلؤی لا لا آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی «آنست نارا» راز و رمز آن «شجر»</p></div>
<div class="m2"><p>دیده ام آن عارض و آن قد و بالا آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمزه ات با عاشقان در شیوه سفک دماست</p></div>
<div class="m2"><p>گرچه رویت سوره انا فتحنا آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ظهور آفتاب رویت ای بدر منیر</p></div>
<div class="m2"><p>هردو عالم ذره سان در عشق دروا آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سجده روی تو می آرد نسیمی دایما</p></div>
<div class="m2"><p>ای جمالت مظهر ایزد تعالی آمده</p></div></div>