---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>خلاق دو عالم بجز از فضل خدا نیست</p></div>
<div class="m2"><p>او ذات و صفاتش بجز از سی و دو تا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن سی و دو تا اصل کمال است به تحقیق</p></div>
<div class="m2"><p>خود نیست که در جانش از این سی و دو تا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ظاهر و باطن به مجازی و حقیقت</p></div>
<div class="m2"><p>داننده و بیننده بجز فضل علا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تقسیم سماوات و زمین کرده به شش روز</p></div>
<div class="m2"><p>قایم شده بر عرش و بر این هیچ خطا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن جان که او پرورش از فضل خدا یافت</p></div>
<div class="m2"><p>فی الجمله حق اوست، در این چون و چرا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس هست تجلیگه حق آدم خاکی</p></div>
<div class="m2"><p>در صورت او دید، کسی را که ریا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون صورت او سی و دو آیات خدا بود</p></div>
<div class="m2"><p>ای بی بصر! از آیت حق هیچ جدا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر ذات مصفا به کلام متکلم</p></div>
<div class="m2"><p>هرکس که بدانست بر او موت و فنا نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر فضل خدا تکیه نسیمی صمدی کرد</p></div>
<div class="m2"><p>خونش دگر از طعنه شیطان دغا نیست</p></div></div>