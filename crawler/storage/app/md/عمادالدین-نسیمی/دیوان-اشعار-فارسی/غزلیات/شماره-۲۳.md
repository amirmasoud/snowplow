---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>هیچ می‌دانی که عالم از کجاست؟</p></div>
<div class="m2"><p>یا ظهور نقش آدم از کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا حروف اسم اعظم در عدد</p></div>
<div class="m2"><p>چند باشد یا خود اعظم از کجاست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گنج دانش را طلسم محکم است</p></div>
<div class="m2"><p>این طلسم گنج محکم از کجاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن دمی کز وی مسیحا مرده را</p></div>
<div class="m2"><p>زنده گردانید آن دم از کجاست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاتم ملک سلیمانی ز چیست؟</p></div>
<div class="m2"><p>حکم تسخیر است خاتم از کجاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چیست اصل فکرهای مختلف</p></div>
<div class="m2"><p>وین خیالات دمادم از کجاست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن یکی اندوهگین دانی ز چیست؟</p></div>
<div class="m2"><p>وین یکی پیوسته خرم از کجاست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای نسیمی ز آنچه میدانی بگوی</p></div>
<div class="m2"><p>کاین یکی بیش آن یکی کم از کجاست؟</p></div></div>