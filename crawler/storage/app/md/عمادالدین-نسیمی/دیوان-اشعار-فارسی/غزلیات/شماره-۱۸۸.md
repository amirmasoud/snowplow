---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>ترک شراب کی کنم من که چو رند فاسقم؟</p></div>
<div class="m2"><p>عار ز زهد اگر کنم فخر من است که عاشقم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خط استوا مرا دل چو به شرح سینه بود</p></div>
<div class="m2"><p>کشف شدم ز سر او سر «سماء طارق » م</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظاهر و باطن جهان هست چون ذات یک وجود</p></div>
<div class="m2"><p>ظاهر زیم چو روز من نه که چو لیل غاسقم(؟)</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد دراز قیدها فضل خدای من خلاص</p></div>
<div class="m2"><p>زان که دو هفت از خدا طایف بیست عایقم (؟)</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>«فالق حب النوی » فضل خدای ماست بس</p></div>
<div class="m2"><p>راست شنو ز من که من بنده فضل فالقم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود کلید رزق چون حسن خط نگار من</p></div>
<div class="m2"><p>رزق ز روی او دهد فضل خدای رازقم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سی و دو نطق مطلق است ذات و صفات یک وجود</p></div>
<div class="m2"><p>کوست به حق انبیا بی شک و شبهه خالقم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه به آخر آمدم بهر بیان سر حشر</p></div>
<div class="m2"><p>نور محمدیم چون بر همه خلق سابقم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راز مسیحی چون ز من فاش شود «عمل » منم</p></div>
<div class="m2"><p>هست چو مصحف حیات عین کلام ناطقم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعر مگو که معجز است سربه‌سر این کلام من</p></div>
<div class="m2"><p>زان که چو شاعر دگر از دگران نه سارقم</p></div></div>