---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>با چنین رفتن، به منزل کی رسی؟</p></div>
<div class="m2"><p>با چنین خصلت، به حاصل کی رسی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس گرانجانی و بس اشتر تنی</p></div>
<div class="m2"><p>در سبکروحان یکدل کی رسی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با چنین رفتن چگونه دم زنی؟</p></div>
<div class="m2"><p>با چنین وصلت به واصل کی رسی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون که اندر سر گشادی نیستی</p></div>
<div class="m2"><p>در گشاد سر مشکل کی رسی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو آبی اندر این گل مانده ای</p></div>
<div class="m2"><p>پس بیا از آب و از گل کی رسی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذر از خورشید و از مه چون خلیل</p></div>
<div class="m2"><p>ورنه در خورشید کامل کی رسی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ضعیفی رو به فضل حق گریز</p></div>
<div class="m2"><p>زان که بی مفضل به مفضل کی رسی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی عنایت های آن دریای لطف</p></div>
<div class="m2"><p>در چنین موجی به ساحل کی رسی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی براق عشق و سعی جبرئیل</p></div>
<div class="m2"><p>چون محمد در منازل کی رسی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی پناهان را پناه خود کنی</p></div>
<div class="m2"><p>در پناه شاه مقبل کی رسی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیش بسم الله بسمل شو تمام</p></div>
<div class="m2"><p>ورنه چون مردی به بسمل کی رسی؟</p></div></div>