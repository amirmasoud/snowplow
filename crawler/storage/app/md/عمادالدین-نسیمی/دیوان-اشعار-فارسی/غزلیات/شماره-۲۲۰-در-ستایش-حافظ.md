---
title: >-
    شمارهٔ ۲۲۰ - در ستایش حافظ
---
# شمارهٔ ۲۲۰ - در ستایش حافظ

<div class="b" id="bn1"><div class="m1"><p>بیا ای گنج بی پایان، چو خود ما را توانگر کن</p></div>
<div class="m2"><p>مس بی قیمت ما را به اکسیر نظر زر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو بحر گوهر و کانی، تو عین آب حیوانی</p></div>
<div class="m2"><p>وجود خاکی ما را حیاتی بخش و گوهر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب لعل تو چون دارد به جانبخشی ید بیضا</p></div>
<div class="m2"><p>چو عیسی دعوت احیا به لعل روحپرور کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عالم، صبحدم، بویی ز گیسویت روان گردان</p></div>
<div class="m2"><p>مشام قدسیان مشکین، جهان را پر ز عنبر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقاب از آفتاب رخ، برانداز ای قمر طلعت</p></div>
<div class="m2"><p>سرای دیده اشیا، به نور خود منور کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سودای خط و خالت، دلی کو رو بگرداند</p></div>
<div class="m2"><p>رخش در مجمع خوبان سیه چون روی دفتر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سودای سر زلفت، سرم سودا گرفت آن کو</p></div>
<div class="m2"><p>ندارد در سر این سودا، برو گو خاک بر سر کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نار عشق اگر خواهی که عالم را بسوزانی</p></div>
<div class="m2"><p>درآ در وادی ایمن ز رخسار آتشی برکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نطقت در حدیث آور، وز آن جان بخش در عالم</p></div>
<div class="m2"><p>دم روح القدس در دم، جهان را پر ز شکر کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر آنکو عاشق رویت نگشت ای صورت رحمان</p></div>
<div class="m2"><p>بنی آدم مخوان او را و نامش سنگ مرمر کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل از تسبیح صوفی شد ملول، ای مطرب مجلس</p></div>
<div class="m2"><p>ز قند آن لب شیرین سخن گوی و مکرر کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ملک را می نهد خطش چو طفلان لوح در دامن</p></div>
<div class="m2"><p>الا ای حافظ قرآن! تو این هفت آیت از بر کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به سالوسی چو زراقان سیه تا کی کنی جامه</p></div>
<div class="m2"><p>قلم بر دلق ازرق کش، به می رخساره احمر کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو هست از روی شمس الدین نشانی شمس خاور را</p></div>
<div class="m2"><p>بیا در روی شمس الدین سجود شمس خاور کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به جست و جوی دیدارش، چو خورشید و مه ای عاشق</p></div>
<div class="m2"><p>به هر کویی قدم در نه، به هر منظر سری در کن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلا با وصلش ار خواهی که ذات متحد گردی</p></div>
<div class="m2"><p>وجود هر دو عالم را نثار روی دلبر کن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خوبی در میان تا مه بسی فرق است رویش را</p></div>
<div class="m2"><p>اگر باور نمی داری بیا باهم برابر کن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو پاکان از در فضلش خدابین می شوند ای دل</p></div>
<div class="m2"><p>بیا و سرمه چشم از غبار خاک این در کن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نسیمی شد به حق واصل الهی عاشقانت را</p></div>
<div class="m2"><p>به حق حرمت فضلت که این دولت میسر کن</p></div></div>