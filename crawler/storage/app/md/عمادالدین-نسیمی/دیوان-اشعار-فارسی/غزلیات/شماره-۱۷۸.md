---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>صورت رحمان من آن روی نکو دانسته ام</p></div>
<div class="m2"><p>چشمه حیوان ز آب روی او دانسته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه با من باد صبح آن بوی جان پرور نگفت</p></div>
<div class="m2"><p>از کجا یا از که دارد من به بو دانسته ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاکروب کوی عشقم در حقیقت چون صبا</p></div>
<div class="m2"><p>تا ز فراش طریقت رفت و رو دانسته ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دفتر طامات گو بر من مخوان زاهد که من</p></div>
<div class="m2"><p>گرچه رندم حاصل این گفت و گو دانسته ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شستم از جان دست و گشتم طالب وصلت به دل</p></div>
<div class="m2"><p>سالک عشقم طریق جست و جو دانسته ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصه واعظ مگویید ای عزیزان پیش من</p></div>
<div class="m2"><p>زان که من افسون آن افسانه گو دانسته ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ندانم زرق و سالوسی، مکن عیبم که من</p></div>
<div class="m2"><p>رسم شاهد بازی و جام و سبو دانسته ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان ز گفتارم بیابی گر بگویم شمه ای</p></div>
<div class="m2"><p>آنچه از اخلاق آن پاکیزه خو دانسته ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل به زلف و غبغبش دادم که طفل عشق را</p></div>
<div class="m2"><p>ناگزیر است از چنین چوگان و گو، دانسته ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای که می گویی که خواهی شد ز عشق او هلاک</p></div>
<div class="m2"><p>نیستم نادان، من این معنی نکو دانسته ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نسیمی شسته ام از خرقه و سجاده دست</p></div>
<div class="m2"><p>الله الله بین چه نیکو شست و شو دانسته ام</p></div></div>