---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>زهی جمال تو مستجمع جمیع صفات</p></div>
<div class="m2"><p>رخ تو آینه رونمای عالم ذات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حق سبعه رویت که سوره کبراست</p></div>
<div class="m2"><p>که عید اکبرم این است و بهترین صلوات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمال حسن رخت قابل نهایت نیست</p></div>
<div class="m2"><p>چرا که لایتناهی بود جمیع صفات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سجود قبله روی تو می کند دل من</p></div>
<div class="m2"><p>صلوة دایمم این است و قبله گاه صلات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز لام و بی لبت یافتم حیات ابد</p></div>
<div class="m2"><p>که آب خضر همین شربت است و عین فرات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلی که کشته رویت نشد بدان حی نیست</p></div>
<div class="m2"><p>چگونه زنده توان بود بی وجود حیات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو شاه عرصه حسنی و هر که دید رخت</p></div>
<div class="m2"><p>به یک پیاده حسن رخ تو شد شهمات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهی ز حسن رخت عید ماه نو کرده</p></div>
<div class="m2"><p>سواد زلف تو روشن شبی سیاه برات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به مصر جامع رویت گزاردم جمعه</p></div>
<div class="m2"><p>زهی حلاوت ایمان و طعم قند ونبات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیال روی تو را عابدی که قبله نساخت</p></div>
<div class="m2"><p>ز عابدان مشمارش که می پرستد لات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسی که جان چو نسیمی فدای روی تو کرد</p></div>
<div class="m2"><p>سواد نامه اعمال او بود حسنات</p></div></div>