---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>در کوی خرابات مناجات توان کرد</p></div>
<div class="m2"><p>در طور لقا عیش خرابات توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بازی شطرنج خط و خال تو این است</p></div>
<div class="m2"><p>لیلاج جهان را به رخت مات توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای زاهد مغرور به طاعت مکن افغان</p></div>
<div class="m2"><p>شیخی به چنین کشف و کرامات توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر مرکب تحقیق توانی به کف آری</p></div>
<div class="m2"><p>سیاره صفت سیر سماوات توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی سخن از خرقه و سجاده و پرهیز</p></div>
<div class="m2"><p>ارشاد بدین کهنه خرافات توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی بر سر بازار خرابات مغان خرج</p></div>
<div class="m2"><p>سیم دغل از توبه و طامات توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی تو به خوبی نه بدان مرتبه دیدم</p></div>
<div class="m2"><p>کاندیشه حسنش به خیالات توان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دیده تحقیق بود درک تجلی</p></div>
<div class="m2"><p>از چهره هر ذره ذرات توان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دادند نشان رخت آن زمره که گفتند</p></div>
<div class="m2"><p>سجده ز برای صنم و لات توان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون پیش نسیمی صفت و ذات یکی شد</p></div>
<div class="m2"><p>کی فرق میان صفت و ذات توان کرد</p></div></div>