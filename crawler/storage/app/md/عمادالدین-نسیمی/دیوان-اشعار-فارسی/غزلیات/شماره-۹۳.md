---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>اگر گویم که مهر و مه، ز رخسارت حیا باشد</p></div>
<div class="m2"><p>وگر گویم که انسانی، مرا شرم از خدا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک را نیست آن صورت که نسبت کرده ام با او</p></div>
<div class="m2"><p>کمال حسن و زیبایی بدینسان هم تو را باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چین و جعد گیسویت مرنج ار دم زند نافه</p></div>
<div class="m2"><p>چه آید از سیه رویی که در اصلش خطا باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصالت نیست آن گنجی که بر بیگانه بگشایند</p></div>
<div class="m2"><p>که آن را حاصل است این در که با بحر آشنا باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان پرسیدم از دلبر، دل گم گشته را گفتا</p></div>
<div class="m2"><p>بجز در بند گیسویم دل عاشق کجا باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن خاکی چو گل گردد نیابی ذره ای در وی</p></div>
<div class="m2"><p>که بی سودای آن جعد و سر زلف دو تا باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا با ما بشوی ای جان به آب دیده دست از دل</p></div>
<div class="m2"><p>که دل تا زلف او بیند کجا در بند ما باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نباشد عهد خوبان را وفا، گویند و می گویم</p></div>
<div class="m2"><p>که خوب آن را توان گفتن که عهدش بی وفا باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حریف ما شو ای صوفی که ذکر حلقه رندان</p></div>
<div class="m2"><p>به است از طاعت و زهدی که با روی و ریا باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیا ای ماه سیمین بر به خونم پنجه رنگین کن</p></div>
<div class="m2"><p>کز اقبالت گر این حاجت روا گردد روا باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نسیمی با تو شد یکرو قفا زد هر دو عالم را</p></div>
<div class="m2"><p>کسی کو رو به حق دارد دو کونش در قفا باشد</p></div></div>