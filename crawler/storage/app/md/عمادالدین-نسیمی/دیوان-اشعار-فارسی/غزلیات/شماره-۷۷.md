---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>ماه نو چون دیدم ابروی توام آمد به یاد</p></div>
<div class="m2"><p>چون نظر کردم به گل، روی توام آمد به یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طره مشکین شبی دیدم مسلسل بر قمر</p></div>
<div class="m2"><p>سنبل زلفین هندوی توام آمد به یاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معجزات انبیا می‌خواند ارباب معین</p></div>
<div class="m2"><p>سحر چشم مست جادوی توام آمد به یاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شب قدر آیتی تفسیر می‌کرد آفتاب</p></div>
<div class="m2"><p>قصه سودای گیسوی توام آمد به یاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصف باغ خلد می‌کردند با هم زاهدان</p></div>
<div class="m2"><p>جنت‌آباد سر کوی توام آمد به یاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقیان روضه می‌کردند ذکر سلسبیل</p></div>
<div class="m2"><p>ذوق جام لعل دلجوی توام آمد به یاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون رقیبانت به خونم نیز می‌کردند تیغ</p></div>
<div class="m2"><p>ساعد سیمین بازوی توام آمد به یاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عابدان از قبله می‌گفتند هر یک نکته‌ای</p></div>
<div class="m2"><p>گوشه محراب ابروی توام آمد به یاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>(حاصلی از بهر دستاویز روز آخرت</p></div>
<div class="m2"><p>فکر می‌کردم شبی موی توام آمد به یاد)</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌زد اشعار نسیمی دم ز انفاس مسیح</p></div>
<div class="m2"><p>از دم جان‌بخش خوشبوی توام آمد به یاد</p></div></div>