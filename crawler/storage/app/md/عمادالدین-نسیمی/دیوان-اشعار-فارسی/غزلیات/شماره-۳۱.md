---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>گرچه چشم ترک مستت فتنه و ابرو بلاست</p></div>
<div class="m2"><p>این چنین دلبر بلا و فتنه دیگر کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش اشیا سر به سر روشن شد از رویت مگر</p></div>
<div class="m2"><p>جام جمشید رخت آیینه گیتی نماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تو هستی دایم اندر خانقاه و میکده</p></div>
<div class="m2"><p>رند و صوفی را چرا پیوسته باهم ماجراست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سالکان را در طریق کعبه وصل رخت</p></div>
<div class="m2"><p>منزل اول فنای خویش و نفی ماسواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گونه رویت آفتاب ذات پاک است از چه رو</p></div>
<div class="m2"><p>از رخت صحن سرای هر دو عالم پر ضیاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر صراط الله از آن بر خط رویت می روم</p></div>
<div class="m2"><p>کاهل معنی را صراط الله خط استواست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چار مژگان و دو ابرو و دو خط و موی سر</p></div>
<div class="m2"><p>هشت باب جنت و هم جنت و فردوس ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرو را تا نسبتی کردم به بالای تو نیست</p></div>
<div class="m2"><p>راستی را زین فرح پیوسته در نشو و نماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل ز من گفتم که دزدید؟ ابرویت گفتا که چشم</p></div>
<div class="m2"><p>این چنین پرفتنه کج با کسی گفته است راست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به سر سی و دو خط رخت ره برده ام</p></div>
<div class="m2"><p>شش جهت چندانکه می بینم همه روی خداست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نسیمی رستگار است از فنا و از عدم</p></div>
<div class="m2"><p>هر وجودی را که از سی و دو نطق حق بقاست</p></div></div>