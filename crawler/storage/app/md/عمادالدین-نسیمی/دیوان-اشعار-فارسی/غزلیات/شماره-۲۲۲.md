---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>قصد زلف یار داری در سر ای دل هی مکن</p></div>
<div class="m2"><p>مرد این سودا نه ای با دلبر ای دل هی مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دولت بوسیدن پایش تمنا می کنی</p></div>
<div class="m2"><p>زین هوس تا سر نبازی بگذر ای دل هی مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل می گوید: غم ناموس خور، بگذر ز عشق</p></div>
<div class="m2"><p>عاشقی را نیست اینها در خور ای دل هی مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه برد آزار و جور از حد رقیب سنگدل</p></div>
<div class="m2"><p>چون توان کردن جدایی زین در ای دل هی مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش شمع روی او پروانه شو، ز آتش مترس</p></div>
<div class="m2"><p>جان بخواهد سوختن، فکر پر ای دل هی مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته ای کز عشقبازی توبه خواهم کرد، کرد</p></div>
<div class="m2"><p>بیش از این تدبیر کار منکر ای دل هی مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کنی سودا که روزی دربر آری قامتش</p></div>
<div class="m2"><p>سرو سیمین بر، نیاید در بر ای دل هی مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درد باطن سوز جانم عرضه پیش هر طبیب</p></div>
<div class="m2"><p>چون نخواهد شد به درمان کمتر ای دل هی مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وصل مهرویان سیم اندام نسرین بر طلب</p></div>
<div class="m2"><p>سعی بی سود است کردن بی زر ای دل هی مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جام می نوش از کف ساقی که در دور لبش</p></div>
<div class="m2"><p>توبه کفر است از شراب و ساغر، ای دل هی مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نسیمی از لب لعلش طلب کن سلسبیل</p></div>
<div class="m2"><p>تکیه بر فردا و آب کوثر ای دل هی مکن</p></div></div>