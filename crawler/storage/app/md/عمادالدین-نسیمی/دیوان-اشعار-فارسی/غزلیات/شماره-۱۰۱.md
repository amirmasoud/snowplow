---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>ای که رخت به روشنی غیرت آفتاب شد</p></div>
<div class="m2"><p>خفته ز شرم مردمی چشم خوشت به خواب شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نافه به بوی زلف تو، آمد و گشت خاک ره</p></div>
<div class="m2"><p>گل ز هوای عارضت رفت و در آتش آب شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو چو دید قامتت، رفت به خویشتن فرو</p></div>
<div class="m2"><p>مه ز رخ تواش حیا آمد و در نقاب شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم تو دوش در دلم بست خیال سرخوشی</p></div>
<div class="m2"><p>چون قدح لب توام دیده پر از شراب شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت که هستم، آفتاب، آینه دار روی تو</p></div>
<div class="m2"><p>دود به سر برآمدش زلف تو زان بتاب شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله لطف دلبران روی تو جمع کرد از آن</p></div>
<div class="m2"><p>مصحف حسن را رخت فاتحة الکتاب شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطرب عشق نکته ای گفت مگر به گوش کس</p></div>
<div class="m2"><p>کاین جگر حزین ما ز آتش او کباب شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخت سعادت ازل هست ملازم درش</p></div>
<div class="m2"><p>آن که به روی دولتش وصل تو فتح باب شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفع حجاب کی کند از رخ بخت جاودان</p></div>
<div class="m2"><p>آن که ز روی هستی اش یک سر مو حجاب شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گنج وصال، آرزو هرکس اگرچه می کند</p></div>
<div class="m2"><p>در سر و کار این طلب عاشق دل خراب شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل به دعا وصال او خواسته بود هاتفی</p></div>
<div class="m2"><p>گفت نسیمی این دعا مژده که مستجاب شد</p></div></div>