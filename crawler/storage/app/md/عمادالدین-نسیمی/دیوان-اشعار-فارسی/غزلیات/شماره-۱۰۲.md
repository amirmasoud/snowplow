---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>مهر رخسار تو داغ عشق بر دل می‌کشد</p></div>
<div class="m2"><p>سنبل زلف تو، مه را در سلاسل می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منزل جان است گیسویت وز آنجا هر نفس</p></div>
<div class="m2"><p>جذبه‌ای می‌آید و جان را به منزل می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کعبه دل روی محبوب است اینک راه دور</p></div>
<div class="m2"><p>گر کسی را دل به سوی کعبه گل می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش رویت سجده آن کو حق نمی‌داند ز جهل</p></div>
<div class="m2"><p>از سجود حق چو شیطان سر به باطل می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ازل عشقت نصیب اهل غفلت چون نبود</p></div>
<div class="m2"><p>دولت جاوید از آن دامن ز غافل می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای کشان ما را به راه و رسم عقل از کوی عشق</p></div>
<div class="m2"><p>دل عنان اختیار از دست عاقل می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نار غیرت مدعی را رشته کوتاه عمر</p></div>
<div class="m2"><p>می‌کشد از تن ولیکن سخت کاهل می‌کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من نمی‌خواهم خلاص از بحر عشقت یک نفس</p></div>
<div class="m2"><p>گرچه جان غرقه را خاطر به ساحل می‌کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>معجز چشمت که عارف خواندش سحر حلال</p></div>
<div class="m2"><p>جان عاشق را به جذبه سوی بابل می‌کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جذبه زلف تو عمری کشکشانم می‌کشید</p></div>
<div class="m2"><p>این زمانم نقش آن شکل و شمایل می‌کشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نسیمی کشته چشم سیاهت هرکه شد</p></div>
<div class="m2"><p>شُکر حق می‌گوید و منت ز قاتل می‌کشد</p></div></div>