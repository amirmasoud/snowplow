---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>نظر برداشت از من تا حبیبم</p></div>
<div class="m2"><p>به جای وصل هجران شد نصیبم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا درد دل از درمان چو بگذشت</p></div>
<div class="m2"><p>قدم برداشت از بالین طبیبم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز یاران عزیز و خویش و پیوند</p></div>
<div class="m2"><p>فتاده دور و مسکین و غریبم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوم منصور چون عیسی اگر یار</p></div>
<div class="m2"><p>کند در عشق بردار و صلیبم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پاکی و ادب در عشق جانان</p></div>
<div class="m2"><p>که تا بنماید آن صورت عجیبم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا بشنو علی اسرار معنی</p></div>
<div class="m2"><p>ز عشق یار وز وصل حبیبم</p></div></div>