---
title: >-
    شمارهٔ ۲۰۱
---
# شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>با روی او مگو که ز گلزار فارغیم</p></div>
<div class="m2"><p>کز هستی دو کون به یکبار فارغیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شیخ شهر! دور ز انکار ما برو</p></div>
<div class="m2"><p>اقرار کن به ما که ز انکار فارغیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با نور و ظلمت رخ و زلفش الی الابد</p></div>
<div class="m2"><p>از شمع آفتاب و شب تار فارغیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اغیار نیست در ره وحدت اگر بود</p></div>
<div class="m2"><p>بالله به جان یار ز اغیار فارغیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را ز ماه روی تو هر ماه حاصل است</p></div>
<div class="m2"><p>از هفته های هفت و شش و چار فارغیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع رخت که مطلع انوار کبریاست</p></div>
<div class="m2"><p>تا دیده شد ز مطلع انوار فارغیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مست از شراب صافی میخانه مسیح</p></div>
<div class="m2"><p>تا گشته ایم از می خمار فارغیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر دو کون چون ز رخت آشکار شد</p></div>
<div class="m2"><p>از نکته های مخفی اسرار فارغیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منصور گشت کام نسیمی به فضل حق</p></div>
<div class="m2"><p>از ما بدار دست که از دار فارغیم</p></div></div>