---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>گر سعادت نظری بر من زار اندازد</p></div>
<div class="m2"><p>بر سرم سایه سرو قد یار اندازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور از آن یار و دیارم نظر سعد کجاست</p></div>
<div class="m2"><p>تا مرا باز بدان یار و دیار اندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که شد مست غرور از می پندار امروز</p></div>
<div class="m2"><p>منتظر باش که فرداش خمار اندازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سببی ساز خدایا که طبیبم نظری</p></div>
<div class="m2"><p>بر دل خسته بی صبر و قرار اندازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که باشم که شوم کشته به تیغش مگر او</p></div>
<div class="m2"><p>از کرم سایه بر این صید نزار اندازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش ابروی کماندار تو میرم که مدام</p></div>
<div class="m2"><p>تیر مژگان همه بر عاشق زار اندازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر برد بوی سر زلف ترا باد به چین</p></div>
<div class="m2"><p>خون دل در جگر مشک تتار اندازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کند چشم تو بر گوشه نشینان نظری</p></div>
<div class="m2"><p>مستی و عربده در صومعه دار اندازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شد از دولت وصل تو نسیمی منصور</p></div>
<div class="m2"><p>وقت آن است که سر در سر دار اندازد</p></div></div>