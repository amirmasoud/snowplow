---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>بهار آمد بهار آمد بهارِ سبزپوش آمد</p></div>
<div class="m2"><p>رها کن فکر خام ای دل که می در خُم به جوش آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب ساقی و جام مل، میان باغ و فصل گل</p></div>
<div class="m2"><p>غنیمت دان که از غیبم سحرگاه این به گوش آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که: صوفی گر می صافی نمی‌نوشد مکن عیبش</p></div>
<div class="m2"><p>حیات تازه را محرم فقیه دُردنوش آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلا دریوزه همت ز باب می‌فروشان کن</p></div>
<div class="m2"><p>که بوی نفحه عیسی ز پیر می‌فروش آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می گلگون خورای زاهد که از قدس الوهیت</p></div>
<div class="m2"><p>گل آورد آتش موسی و بلبل در خروش آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا بی‌عشق مه‌رویان بقای سر نمی‌باید</p></div>
<div class="m2"><p>که سر بی‌عشق در گردن کشیدن بار دوش آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن آه ای دل پرغم، بپوش اسرار دل محکم</p></div>
<div class="m2"><p>که نامحرم خطابین است و می‌باید خموش آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آب دیده دوش از غم، مپرس ای دل که چون بودم</p></div>
<div class="m2"><p>که از غم سر به سر طوفان مرا تنها نه دوش آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بانگ چنگ و عود و نی بنوش ای رند عارف می</p></div>
<div class="m2"><p>که طاب العیش و طوبی لک ز فضل حق سروش آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به صوفی می ده ای ساقی که در دارالشفای ما</p></div>
<div class="m2"><p>علاج علت خامی شراب پخته جوش آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نسیمی تا لب جانان و جام می بود دیگر</p></div>
<div class="m2"><p>به زهد خشک بی‌حاصل نخواهد سرفروش آمد</p></div></div>