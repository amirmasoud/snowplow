---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای شب زلفت که روزش کس نمی بیند به خواب</p></div>
<div class="m2"><p>در تب است از تابش خورشید رویت آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم از نور تجلی کرد نورانی رخت</p></div>
<div class="m2"><p>گرچه زلفت چون شب قدر است و رویت ماهتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که پیش خط و خالت چون ملک در سجده نیست</p></div>
<div class="m2"><p>باشد ابلیسی که هست از نار حرمان در عذاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با دم جان پرورت انفاس عیسی بسته نطق</p></div>
<div class="m2"><p>پیش زلف تابدارت گشته مریم رشته تاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قبله تحقیق من روی تو و وصلت حیات</p></div>
<div class="m2"><p>جنت جاوید من کوی تو و لعلت شراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طره طرار زلفت سوره رحمان و عرش</p></div>
<div class="m2"><p>غمزه غماز عینت معنی ام الکتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه را غیر از تو باشد آرزویی در جهان</p></div>
<div class="m2"><p>تشنه ای باشد که جوید آب حیوان در سراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی کند میل نعیم و نعمت دنیای دون</p></div>
<div class="m2"><p>کز لبت نوشیده باشد شربت ناز و عتاب؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون وجود غیر ممنوع است و شرکت منتفی است</p></div>
<div class="m2"><p>با جمال خویش باشد حسن رویت را خطاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در رخت نور تجلی دیده اکنون چون ندید</p></div>
<div class="m2"><p>از لبت جز «لن ترانی » کی بود او را جواب؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می کند شرح «الم نشرح » نسیمی از خطت</p></div>
<div class="m2"><p>ای رخت «انا فتحنا» از تو شد این فتح باب</p></div></div>