---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>دلی دارم که در وی غم نگنجد</p></div>
<div class="m2"><p>چه جای غم؟ که شادی هم نگنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان ما و یار همدم ما</p></div>
<div class="m2"><p>اگر همدم نباشد دم نگنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلی کو فارغ است از سور و ماتم</p></div>
<div class="m2"><p>در او هم سور و هم ماتم نگنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز انگشتی که عالم خاتم اوست</p></div>
<div class="m2"><p>دگر چیزی در این خاتم نگنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان درکش نسیمی خود ز گفتار</p></div>
<div class="m2"><p>مگو چیزی که در عالم نگنجد</p></div></div>