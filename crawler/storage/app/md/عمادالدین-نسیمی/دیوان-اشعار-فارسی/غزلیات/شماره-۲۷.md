---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>امشب از روی تو مجلس را ضیایی دیگر است</p></div>
<div class="m2"><p>دیده‌ها را نور و دل‌ها را صفایی دیگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرمم از روی تو می‌آید بشر گفتن تو را</p></div>
<div class="m2"><p>جز خدا کفر است اگر گویم خدایی دیگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نهادیم از سر دریوزه در کویت قدم</p></div>
<div class="m2"><p>هر زمان از فضل حق ما را عطایی دیگر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه هست آب و هوای روضه رضوان لطیف</p></div>
<div class="m2"><p>جنت‌آباد سر کوی تو جایی دیگر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکسی در سر هوایی دارد از مهرت ولی</p></div>
<div class="m2"><p>در سر ما ز آتش عشقت هوایی دیگر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست این دل قابل تیمار و درمان ای طبیب</p></div>
<div class="m2"><p>درد بیمار محبت را دوایی دیگر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر در سلطان، گدا هستند بسیاری ولی</p></div>
<div class="m2"><p>بر در آن حضرت این مفلس گدایی دیگر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خانه مردم ز بس کز آب چشمم شد خراب</p></div>
<div class="m2"><p>هر زمان با آب چشمم ماجرایی دیگر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم مستش گفت: هستم من بلای جان خلق</p></div>
<div class="m2"><p>گفتم ابرو، غمزه‌اش گفت: آن بلایی دیگر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه دارند از گل رویش نوایی هرکسی</p></div>
<div class="m2"><p>بلبل جان نسیمی را نوایی دیگر است</p></div></div>