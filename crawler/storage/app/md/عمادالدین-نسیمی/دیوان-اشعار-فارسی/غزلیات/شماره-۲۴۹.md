---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>ماییم دل ز عالم بر زلف یار بسته</p></div>
<div class="m2"><p>از دست پرنگارش دل در نگار بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودای چشم مستش در جان و دل نشسته</p></div>
<div class="m2"><p>در خاطر از خیالش فکر خمار بسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشد ز حسن و زلفش پای صبا گشاده</p></div>
<div class="m2"><p>از مشک رو سیه شد راه تتار بسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای پرده ای ز سنبل بر یاسمن کشیده</p></div>
<div class="m2"><p>وی برقعی ز ریحان بر لاله زار بسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای صورت خدایی، ظاهر در آب و خاکی</p></div>
<div class="m2"><p>وی پیکر الهی بر باد و نار بسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای زلف بی قرارت بشکسته چون دل من</p></div>
<div class="m2"><p>عهدی که با دل و جان آن بی قرار بسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت صلوة و سجده، دارم حضور دل چون</p></div>
<div class="m2"><p>نقش تو در دلم هست ای گلعذار بسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خال عنبرینت بر «بی » نهاده نقطه</p></div>
<div class="m2"><p>وز مشک سوده خطی بر گل غبار بسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای زلف جان شکارت در حلقه های سودا</p></div>
<div class="m2"><p>جان و دل اسیران چندین هزار بسته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از گفتن انا الحق سر تا ابد نپیچد</p></div>
<div class="m2"><p>آن سر که باشد ای جان در فوق دار بسته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زلف تو با نسیمی ای نور دیده تا کی</p></div>
<div class="m2"><p>باشد به کین میان را چون روزگار بسته</p></div></div>