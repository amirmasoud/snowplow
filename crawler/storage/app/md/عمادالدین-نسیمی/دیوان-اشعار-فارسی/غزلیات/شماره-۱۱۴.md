---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>عشاق، هوای رخ زیبای تو دارند</p></div>
<div class="m2"><p>زانروی چو منصور همه بر سر دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رحم آر به جان و دل این قوم که در عشق</p></div>
<div class="m2"><p>مجروح و دل آزرده و بیمار و نزارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ است جهان در نظر همت ایشان</p></div>
<div class="m2"><p>غیر از تو کسی در دو جهان هیچ ندارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با یاد تو شب تا به سحر با دل پرسوز</p></div>
<div class="m2"><p>فریاد ز جان هر نفس از عشق برآرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردند شمار همه کس در ره عشقت</p></div>
<div class="m2"><p>از هیچ کسان نیز مرا هم نشمارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی به نسیمی که به جان طالب مایی</p></div>
<div class="m2"><p>ای دولت آندم که مرا با تو گذارند</p></div></div>