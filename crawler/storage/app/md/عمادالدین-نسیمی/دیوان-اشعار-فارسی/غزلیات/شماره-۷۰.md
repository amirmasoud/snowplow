---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>می روم با درد و حسرت از دیارت، خیر باد</p></div>
<div class="m2"><p>دل به خدمت می گذارم یادگارت خیر باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا باشم همی گویم دعای دولتت</p></div>
<div class="m2"><p>از خدا صد آفرین بر روزگارت خیر باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می روم با آب چشم و آتش دل بی خبر</p></div>
<div class="m2"><p>از جفای ترک چشم پرخمارت خیر باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دهد عمرم امان، رویت ببینم عاقبت</p></div>
<div class="m2"><p>ور بمیرم در غریبی ز انتظارت خیر باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نسیم چین زلفت بگذرد بر خاک من</p></div>
<div class="m2"><p>زنده برخیزم به بوی مشکبارت خیر باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز من یاد آوری بنویس آخر رقعه ای</p></div>
<div class="m2"><p>کای نسیمی بر کلام آبدارت خیر باد</p></div></div>