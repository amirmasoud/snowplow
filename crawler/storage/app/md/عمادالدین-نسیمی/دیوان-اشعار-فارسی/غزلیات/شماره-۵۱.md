---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>مشرک بی دیده کی احوال ما داند که چیست</p></div>
<div class="m2"><p>مرد حق بین معنی سر خدا داند که چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گمرهی کز خط وجه دوست، روی حق ندید</p></div>
<div class="m2"><p>شرح بیست و هشت و سی و دو کجا داند که چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو ما سبع المثانی از کتاب روی یار</p></div>
<div class="m2"><p>هر که خواند معنی این آیه ها داند که چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که از شق القمر پی بر صراط الله نبرد</p></div>
<div class="m2"><p>سوی خط کی ره برد یا استوا داند که چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه ما از فی و ضاد و لام حق دانسته ایم</p></div>
<div class="m2"><p>در تصوف صوفی صاحب صفا داند که چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چین زلف عنبرینت حلقه دام بلاست</p></div>
<div class="m2"><p>بسته زنجیر، قدر این بلا داند که چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در میان جان ما و زلف عنبر بوی یار</p></div>
<div class="m2"><p>نیست اسراری که آن باد صبا داند که چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلسبیل و کوثر لعلش هر آن کو نوش کرد</p></div>
<div class="m2"><p>چون نسیمی لذت جام بقا داند که چیست</p></div></div>