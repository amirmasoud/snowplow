---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>این چه چشم است این چه ابرو این چه زلف است این چه خال؟</p></div>
<div class="m2"><p>در مقام خویش هر یک دلبری صاحب کمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق بالای دلجوی تو شد سرو چمن</p></div>
<div class="m2"><p>انبت الله ای نگار، این است حد اعتدال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>واله و حیران شود صورتگر چینی اگر</p></div>
<div class="m2"><p>صورت پاکیزه چون روی تو آرد در خیال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر جمالت مست و حیرانم، ندانم چون کنم</p></div>
<div class="m2"><p>شرح آن شکل و شمایل، وصف آن حسن و جمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ متاب از چشمه چشمم چو می دانی که خوب</p></div>
<div class="m2"><p>می نماید عکس ماه بدر در آب زلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم دوران جز به دور زلف و رخسارت ندید</p></div>
<div class="m2"><p>گشته طالع در شب قدر آفتاب بی زوال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خیال زلف و خالت عشق می بازم بلی</p></div>
<div class="m2"><p>اینت کار عاشق سودایی آشفته حال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در غم روی تو هردم ز آتش دل چون قلم</p></div>
<div class="m2"><p>دود آه و ناله ام در سینه می پیچد چو نال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کنم بر یاد ابرویت نظر بر ماه نو</p></div>
<div class="m2"><p>گرچه دور است از کمال حسن ابرویت هلال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نسیمی وصل آن گلچهره گر داری هوس</p></div>
<div class="m2"><p>در تن ای عاشق چو بلبل تا نفس داری بنال</p></div></div>