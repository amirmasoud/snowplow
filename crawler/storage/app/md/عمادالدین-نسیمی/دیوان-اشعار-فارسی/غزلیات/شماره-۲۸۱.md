---
title: >-
    شمارهٔ ۲۸۱
---
# شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>بیا ای احسن صورت! بیا ای اکمل معنی</p></div>
<div class="m2"><p>به میدان الوهیت که داری جای این دعوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصالت جنت عدن است در دل اهل جنت را</p></div>
<div class="m2"><p>جز این صورت نمی بندد که باشد جنت اعلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مراد از دنیی و عقبی تویی ما را و کی باشد</p></div>
<div class="m2"><p>بجز وصل تو عاشق را مراد از دنیی و عقبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمالت در همه اشیاء تجلی کرده است اما</p></div>
<div class="m2"><p>چو مجنون عاشقی بیند خدا را در رخ لیلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال صورت رویت به چین گر بگذرد روزی</p></div>
<div class="m2"><p>شود بر کافران بسته در بتخانه مانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ناز و نعمت دنیی مناز ای صاحب کشور</p></div>
<div class="m2"><p>که نادانی بود نازش به ناز و نعمت دنیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگو با منکر رویش حدیث آن لب، ای عاشق</p></div>
<div class="m2"><p>که در دجال نابینا نگیرد نفخه عیسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بند زلف او زاهد از آنرو دل نمی بندد</p></div>
<div class="m2"><p>که بر ساحر سیه مار است و عقرب: معجز موسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم عشق پریرویان مگو با ساکن خلوت</p></div>
<div class="m2"><p>حدیث آفتاب و مه مگو با دیده اعمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فقیه از آیت خطش به نور حق نشد بینا</p></div>
<div class="m2"><p>زمرد می کشد لعلش مگر در دیده افعی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گدای کوی آن شاهم که درویش در او را</p></div>
<div class="m2"><p>طفیل همتش باشد سریر و افسر کسری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز عرش روی خود بگشا نقاب، ای صورت رحمان</p></div>
<div class="m2"><p>که تا از لوح محفوظت بخوانند آیت کبری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>(چو عاشق بر محک زاهد کی آید سرخ رو چون زر</p></div>
<div class="m2"><p>که رنگ عاشقان خون است و رنگ زاهدان هندی)</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نسیمی را تو معبودی و دین و قبله و ایمان</p></div>
<div class="m2"><p>تو خواهی حق پرستش خوان و خواهی عابد عزی</p></div></div>