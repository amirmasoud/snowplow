---
title: >-
    شمارهٔ ۲۶۶
---
# شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>ای باغ جنت از گل روی تو آیتی</p></div>
<div class="m2"><p>وصف کمال حسن تو ما لا نهایتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب حیات از لب لعل تو جرعه ای</p></div>
<div class="m2"><p>پیش لب تو قصه شیرین حکایتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هر نظر ز نقش خیال تو صورتی</p></div>
<div class="m2"><p>در هر دلی ز مهر جمالت سرایتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر درد و هر غم از تو دوایی و شربتی</p></div>
<div class="m2"><p>هر جور و هر جفا ز تو فضل و عنایتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکو نکرد در طلبت نقد عمر صرف</p></div>
<div class="m2"><p>بی حاصل ابلهی است و ندارد کفایتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>(پروانه حریم وصال تو عاشقی است</p></div>
<div class="m2"><p>کز نور شمع روی تو دارد هدایتی)</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با آنکه جور و ظلم تو با من ز حد گذشت</p></div>
<div class="m2"><p>صد شکر می کنم که ندارم شکایتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون حسن با ملاحت اگر دارد اتفاق</p></div>
<div class="m2"><p>زیبا بود دو پادشه اندر ولایتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دارد نسیمی از همه عالم تو را و بس</p></div>
<div class="m2"><p>ای اولی که هیچ نداری نهایتی!</p></div></div>