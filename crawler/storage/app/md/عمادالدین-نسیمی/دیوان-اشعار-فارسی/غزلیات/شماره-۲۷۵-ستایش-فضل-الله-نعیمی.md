---
title: >-
    شمارهٔ ۲۷۵ - ستایش فضل‌الله نعیمی
---
# شمارهٔ ۲۷۵ - ستایش فضل‌الله نعیمی

<div class="b" id="bn1"><div class="m1"><p>دم حق دمید در ما، دم فضل لایزالی</p></div>
<div class="m2"><p>چه مبارک است این دم ز جناب فضل عالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو جناب ذوالجلالت همه بر کمال دیدم</p></div>
<div class="m2"><p>گنه است اگر نگویم که تو ذات ذوالجلالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صنما ز طرف برقع رخ همچو ماه بنما</p></div>
<div class="m2"><p>که سرای «کن فکان » شد ز وجود غیر خالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه خیال نقش بندم که نه صورت تو باشد</p></div>
<div class="m2"><p>که شد از رخ تو روشن که تو نقش هر خیالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جمال و حسن و خوبی نکنم ستایش تو</p></div>
<div class="m2"><p>که تو همچنان که هستی همه حسنی و جمالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسدت که گوی خوبی ببری ز جمله خوبان</p></div>
<div class="m2"><p>که تو آن مه ملیحی که به حسن بی مثالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عدم و زوال و نقصان به تو راه از آن ندارد</p></div>
<div class="m2"><p>که تو آن خجسته مهری که منزه از زوالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز فراق و درد دوری نکنم حدیث از آنرو</p></div>
<div class="m2"><p>که چو روح و نطق با من شب و روز در وصالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کمال اگر تواند صفتت فزونتر آید</p></div>
<div class="m2"><p>بنمای تا بگویم که فزونتر از کمالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بشری به صورت تو نشنیدم الله الله</p></div>
<div class="m2"><p>چه جمیل حسن و خلقی، چه لطیف زلف و خالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنما به خلق عالم رخ و نفی ما سوا کن</p></div>
<div class="m2"><p>که به صاد و عین بهره دهد آن به جیم و دالی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به تو چون غنی نباشم که به وصف درنیایی</p></div>
<div class="m2"><p>که چه بی کرانه ملکی و چه بی شماره مالی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شب قدر اگرچه بهتر ز هزار ماه باشد</p></div>
<div class="m2"><p>تو به قدر و رفعت افزون ز هزار ماه و سالی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز شراب فضل ما را قدحی ده، ای نسیمی!</p></div>
<div class="m2"><p>که تو جام آفتابی و تو روح لایزالی</p></div></div>