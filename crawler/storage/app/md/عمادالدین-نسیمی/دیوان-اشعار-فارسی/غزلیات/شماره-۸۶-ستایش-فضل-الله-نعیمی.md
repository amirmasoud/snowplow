---
title: >-
    شمارهٔ ۸۶ - ستایش فضل‌الله نعیمی
---
# شمارهٔ ۸۶ - ستایش فضل‌الله نعیمی

<div class="b" id="bn1"><div class="m1"><p>قاصدی کو تا به جان پیغام دلدار آورد</p></div>
<div class="m2"><p>یا هوایی کز نسیم طره یار آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کس از دنیا و عقبی باشد آزادی چو ما</p></div>
<div class="m2"><p>دردمندی را که عشق یار در کار آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر اناالحق‌های ما را بشنود منصور مست</p></div>
<div class="m2"><p>هم به خون ما دهد فتوی و هم دار آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر برد بویی به چین از طره زلفت نسیم</p></div>
<div class="m2"><p>مشک را در ناف آهویان به زنهار آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خطا آید سیه رو گر برد باد صبا</p></div>
<div class="m2"><p>بوی گیسویش به چین و مشک تاتار آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به جان بتوان خریدن وصل آن محبوب را</p></div>
<div class="m2"><p>نیم جانی هرکه را باشد به بازار آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف و رخسات عیان شد منکر رویت کجاست</p></div>
<div class="m2"><p>تا به ایمان سر زلف تو اقرار آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نور و ظلمت را یکی بیند ز روی اتحاد</p></div>
<div class="m2"><p>عارفی کو در خیال آن زلف و رخسار آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با لب و چشم نگارم وقت آن آمد که رند</p></div>
<div class="m2"><p>اهل تقوی را به دوش از کوی خمار آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون قدش سروی نخواهد رست چون رویش گلی</p></div>
<div class="m2"><p>تا ابد چندان که روید سرو و گل بار آورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای نسیمی هر که را رهبر شود فضل اله</p></div>
<div class="m2"><p>از وجود خویش و غیرش جمله بیزار آورد</p></div></div>