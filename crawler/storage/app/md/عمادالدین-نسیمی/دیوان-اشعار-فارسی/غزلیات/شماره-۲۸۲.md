---
title: >-
    شمارهٔ ۲۸۲
---
# شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>زلف را بر هر دو رخ جا می کنی</p></div>
<div class="m2"><p>غارت جان، قصد دلها می کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می دهی ساغر ز چشم پرخمار</p></div>
<div class="m2"><p>سالکان را مست و شیدا می کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان از زلف و رخسار این قمر!</p></div>
<div class="m2"><p>هر زمان صد فتنه پیدا می کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرده ای آیینه ما را و در او</p></div>
<div class="m2"><p>صورت خود را تماشا می کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده برمی داری از روی چو ماه</p></div>
<div class="m2"><p>گنج حق را آشکارا می کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوشه گیران مرقع پوش را</p></div>
<div class="m2"><p>بت پرست عشق و رسوا می کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دانه می سازی ز خال عنبرین</p></div>
<div class="m2"><p>دام دل زلف سمن سا می کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کنی با عاشقان ناز و عتاب</p></div>
<div class="m2"><p>مدعی را آفرین ها می کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدلی را هردم ای لیلی چو من</p></div>
<div class="m2"><p>عاشق و مجنون و شیدا می کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشکل هردو جهان حل می شود</p></div>
<div class="m2"><p>چون ز گیسو یک گره وا می کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کس ندیده است این قیامت ها که تو</p></div>
<div class="m2"><p>در جهان ای سدره بالا! می کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اهل معنی را به دور زلف و خال</p></div>
<div class="m2"><p>همچو نقطه بی سر و پا می کنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طور سینای تجلی توییم</p></div>
<div class="m2"><p>ای که ما را طور سینا می کنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای نسیمی از دم روح القدس</p></div>
<div class="m2"><p>مردگان را حشر و احیا می کنی</p></div></div>