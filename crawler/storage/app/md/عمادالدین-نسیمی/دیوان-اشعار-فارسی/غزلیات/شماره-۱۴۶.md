---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>شرح غم دل ما با یار ما که گوید؟</p></div>
<div class="m2"><p>گر محرمی نباشد جان غصه با که گوید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان با خیال لعلش گوید غم دل، آری</p></div>
<div class="m2"><p>با غنچه حال بلبل غیر از صبا که گوید؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غلتان اگر نه هردم اشکم رود به کویش</p></div>
<div class="m2"><p>سرو روان ما را از ما دعا که گوید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد ز روی نیکو گوید نظر بپوشان</p></div>
<div class="m2"><p>در دین حق پرستان این را روا که گوید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر منکری ز خامی گوید مباش عاشق</p></div>
<div class="m2"><p>مشنو حدیث او را بگذار تا که گوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان با هوای مهرش آمد به لب ندانم</p></div>
<div class="m2"><p>با آفتاب هبلی(؟) حال هبا که گوید؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن را که نیست ای جان، روی تو قبله دل</p></div>
<div class="m2"><p>چون اهل وحدت او را رو با خدا که گوید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصل تو گرچه بیش است از حد ما ولیکن</p></div>
<div class="m2"><p>در عالم هویت شاه و گدا که گوید؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلف و رخت نگارا، صد شرح داد اما</p></div>
<div class="m2"><p>تفسیر این، کماهی، ای دلربا که گوید؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن کو به نور مهرش روشن نکرد دیده</p></div>
<div class="m2"><p>او را چو صبح صادق صاحب صفا که گوید؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون دیده نسیمی روی تو دیده باشد</p></div>
<div class="m2"><p>با سالکان عشقت شرح قفا که گوید؟</p></div></div>