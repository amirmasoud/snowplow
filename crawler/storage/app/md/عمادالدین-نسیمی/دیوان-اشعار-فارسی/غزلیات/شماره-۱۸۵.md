---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>در خمارم ساقیا! جام جمی می‌بایدم</p></div>
<div class="m2"><p>محرم همدم ندارم، همدمی می‌بایدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم از زلف پریشانش حکایت‌ها بسی</p></div>
<div class="m2"><p>خلوت بی‌مدعی با محرمی می‌بایدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خشک شد لب ز آتش دل در جگر آبم نماند</p></div>
<div class="m2"><p>ای مه از دریای فضلت شبنمی می‌بایدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شادی ما در دو عالم جز غم روی تو نیست</p></div>
<div class="m2"><p>زان به نو، هر ساعت از عشقت، غمی می‌بایدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دل مجروح خود را یک زمان تسکین دهم</p></div>
<div class="m2"><p>از سنان غمزهٔ او مرهمی می‌بایدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کنم قربان پایت هردم ای جان جهان</p></div>
<div class="m2"><p>هر نفس جانی و هردم عالمی می‌بایدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در طریق کعبه شوق تو جان مرد از عطش</p></div>
<div class="m2"><p>ای حیات تشنه! آب زمزمی می‌بایدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نباشم در بیابان محبت بی‌طریق</p></div>
<div class="m2"><p>همچو ابراهیم عاشق ادهمی می‌بایدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سینه از درد فراقت چون دل نی شرحه شد</p></div>
<div class="m2"><p>از دم عیسی دمی اکنون دمی می‌بایدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حاصل دنیی و عقبی در حقیقت یک دم است</p></div>
<div class="m2"><p>تا شناسد قدر این دم، آدمی می‌بایدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفحهٔ روح‌القُدُس دارد نسیمی در نفس</p></div>
<div class="m2"><p>ای که می‌گویی مسیح مریمی می‌بایدم</p></div></div>