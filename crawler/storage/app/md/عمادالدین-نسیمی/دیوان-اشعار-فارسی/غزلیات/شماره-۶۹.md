---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>سی و دو خط رخت گنج ترا افتتاح</p></div>
<div class="m2"><p>ظلمت زلف تو شب، نور جمالت صباح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و جهان می دهم وصل ترا می خرم</p></div>
<div class="m2"><p>بین که چه بیع و شری دید ضمیرم صلاح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راحت روحانیان از دم روح تو شد</p></div>
<div class="m2"><p>یافت بقا آنکه یافت از در وصلت رواح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راح و رحیق غمت کرد جهان را غریق</p></div>
<div class="m2"><p>بی خبران را نصیب نیست ازین روح و راح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده باقی به ما، ساقی از آن خم بده</p></div>
<div class="m2"><p>کز نم هر قطره اش پرشده جمله قداح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غازی میدان عشق پردل و یکدل بود</p></div>
<div class="m2"><p>کز دل و جان بر میان بسته به مردی سلاح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پردلی و یکدلی در ره عشق آورد</p></div>
<div class="m2"><p>زانکه نیابد وصال از سر لعب و مزاح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طالب حق کی شدی واصل ذات قدیم</p></div>
<div class="m2"><p>گر نبدی در جهان حسن و جمالت ملاح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چونکه نسیمی رهید از سر پندار خویش</p></div>
<div class="m2"><p>گشت بری، لاجرم، شد ز فنا استراح</p></div></div>