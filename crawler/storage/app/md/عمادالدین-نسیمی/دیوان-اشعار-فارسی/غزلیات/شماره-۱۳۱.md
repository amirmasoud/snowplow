---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>قبله عشاق عارف صورت رحمان بود</p></div>
<div class="m2"><p>جان و دل در عشق جانان باختن خوب آن بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش روی خوبرویان سجده می آرم، فقیه!</p></div>
<div class="m2"><p>قبله ای کی به ز روی و صورت خوبان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد اندر عشق او در باز جان و دل چو من</p></div>
<div class="m2"><p>زان که هر کو عشق او زینسان بود انسان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکته سر خدا در صورت خوبان خفی است</p></div>
<div class="m2"><p>محرم این نکته جان عاشق حیران بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی به جان واماند از جانان، بگو ناصح، کسی</p></div>
<div class="m2"><p>عاشق مقتول خود را چون دیت جانان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنج و اندوه فراق عاشق غمدیده را</p></div>
<div class="m2"><p>صورت سبع مثانی وجه او درمان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناصحا فکر من اندر عشق او جان دادن است</p></div>
<div class="m2"><p>کی مرا فکر غم زولانه و زندان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق می بازد نسیمی تا اثر باشد از او</p></div>
<div class="m2"><p>عشق بازی با جمال دوست جاویدان بود</p></div></div>