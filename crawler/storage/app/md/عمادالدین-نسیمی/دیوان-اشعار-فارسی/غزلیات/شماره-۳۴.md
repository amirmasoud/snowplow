---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>مرغ عرشیم و قاف خانه ماست</p></div>
<div class="m2"><p>کن فکان فرش آشیانه ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جعد مشکین و زلف وجه الله</p></div>
<div class="m2"><p>دام دل عین و خال دانه ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای فسوسی دم از فکوک مزن</p></div>
<div class="m2"><p>ذات حق فارغ از فسانه ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان حرام است با تو می خوردن</p></div>
<div class="m2"><p>کاین شراب از شرابخانه ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی نشان ره به ذات حق نبرد</p></div>
<div class="m2"><p>کان نشان سی و دو نشانه ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر طلبکار ذات یزدانی</p></div>
<div class="m2"><p>وجه بی عذر و بی بهانه ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش کفر سوز و شرک گذار</p></div>
<div class="m2"><p>نار توحید یکزبانه ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچه اشیا وجود از او دارد</p></div>
<div class="m2"><p>گوهر بحر بی کرانه ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نام صوفی مبر که آن دلبر</p></div>
<div class="m2"><p>فارغ از فش و ریش و شانه ماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن تنانای ما الف لام است</p></div>
<div class="m2"><p>مست عشقیم و این ترانه ماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نسیمی همه جهان امروز</p></div>
<div class="m2"><p>سرخوش از باده شبانه ماست</p></div></div>