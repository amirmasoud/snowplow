---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>ما حاصل از حیات رخ یار کرده‌ایم</p></div>
<div class="m2"><p>عهدی به یار بسته و اقرار کرده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منصور شد ز دولت عشق تو کار ما</p></div>
<div class="m2"><p>بردار سر که عزم سر دار کرده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما ملتفت به زهد ریایی نمی‌شویم</p></div>
<div class="m2"><p>زان، رو به کنج خانه خمار کرده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صوفی به زهد ظاهر اگر فخر می‌کند</p></div>
<div class="m2"><p>آن فخر ننگ ماست کز او عار کرده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را عصا و خرقه و سجاده گو مباش</p></div>
<div class="m2"><p>ما ترک بت‌پرستی و زنار کرده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون حسن یار تا ابد است از خلل بری</p></div>
<div class="m2"><p>عهدی که با محبت دلدار کرده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هردم به بوی وصل جمالش هزار عیش</p></div>
<div class="m2"><p>با محرمان صاحب اسرار کرده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگذر ز زهد و زرق که ما این معاملات</p></div>
<div class="m2"><p>در خانقاه و مدرسه بسیار کرده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکس طلب کنند مرادی نسیمیا</p></div>
<div class="m2"><p>ما اختیار از همه، دیدار کرده‌ایم</p></div></div>