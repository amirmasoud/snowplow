---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>مرا خون هست از چشمم، می و ساغر نمی‌باید</p></div>
<div class="m2"><p>چنین مخمور و مستی را مِیِ دیگر نمی‌باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو می در خُم همی‌جوشم، بدین سر پرده می‌پوشم</p></div>
<div class="m2"><p>ظهور کنت کنزا را جز این مظهر نمی‌باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا ای ساقی باقی که مستان جمالت را</p></div>
<div class="m2"><p>به غیر از شمع رخسارت چراغی درنمی‌باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جز نقل لبش با ما مگو ای مطرب مجلس</p></div>
<div class="m2"><p>که اهل ذوق را نقلی جز این شکّر نمی‌باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر با زلف او داری سر سودا، ز سر بگذر</p></div>
<div class="m2"><p>که با سودای زلف او هوای سر نمی‌باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شمع از آتش عشقش برافروز ای دل عارف</p></div>
<div class="m2"><p>که تنها در غم عشقش رخ چون زر نمی‌باید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجو جز گوهر وصلش ز بحر کاف و نون ای دل</p></div>
<div class="m2"><p>که غواصان معنی را جز این گوهر نمی‌باید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز الفقر خط و خالش سواد الوجه اگر داری</p></div>
<div class="m2"><p>فقیر پایه قدرت از این برتر نمی‌باید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خاک آستان او مرا بالین و بستر شد</p></div>
<div class="m2"><p>جز این بالین نمی‌خواهم، جز این بستر نمی‌باید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا آن چهره زیبا بس است ای سنبل رعنا</p></div>
<div class="m2"><p>قرین گل جز این ریحان جان‌پرور نمی‌باید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نسیمی حرف نام خود سترد از دفتر عفت</p></div>
<div class="m2"><p>که نام هرکه عاشق شد در این دفتر نمی‌باید</p></div></div>