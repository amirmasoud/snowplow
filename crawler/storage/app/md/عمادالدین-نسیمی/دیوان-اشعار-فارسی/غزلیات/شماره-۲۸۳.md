---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>جام شراب و ساقی زیبا و بانگ نی</p></div>
<div class="m2"><p>یحیی العظام و هی رمیم بفضل حی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زهد باریا چو نشد کار ما تمام</p></div>
<div class="m2"><p>ماییم بعد از این و خرابات و جام می</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطرب بیا و نغمه مستانه ساز کن</p></div>
<div class="m2"><p>چون کار عشق نیست بجز های و هوی و هی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید عشق پرده ز رخسار برگرفت</p></div>
<div class="m2"><p>ظل ظلیل عشق ز تشویق گشت طی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانم فدای عشق که از نور او نماند</p></div>
<div class="m2"><p>در چشم عاشقان بجز از دوست هیچ شی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شد اسیر چاه زنخدان او دلم</p></div>
<div class="m2"><p>عار آیدش ز شاهی کاووس و جاه کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیزار شد ز خرقه و بگذشت ز خانقاه</p></div>
<div class="m2"><p>جان نسیمی تا به خرابات برد پی</p></div></div>