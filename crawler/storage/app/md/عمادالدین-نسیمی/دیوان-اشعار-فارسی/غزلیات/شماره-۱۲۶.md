---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>سر چه باشد که نثار قدم یار کنند</p></div>
<div class="m2"><p>یا دل و دین به چه ارزد که در این کار کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قبله جان نبود جز رخ جانان زان رو</p></div>
<div class="m2"><p>عاشقان قبله خود ابروی دلدار کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی تواند شدن از سر انا الحق واقف</p></div>
<div class="m2"><p>هر که او را غم آن است که بردار کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرطش آن است که بر دار ببیند خود را</p></div>
<div class="m2"><p>هر که از فضل تواش واقف اسرار کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن گروهی که در انکار منند از عشقت،</p></div>
<div class="m2"><p>گر ببینند رخت را همه اقرار کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهل تحصیل ندارند ز معنی خبری</p></div>
<div class="m2"><p>سبق عشق تو در مدرسه تکرار کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دردمندان تو هر لحظه دلی می طلبند</p></div>
<div class="m2"><p>تا به درد و غم عشق تو گرفتار کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خبر از جنت روی تو ندارند آنان</p></div>
<div class="m2"><p>کآرزوی چمن و رغبت گلزار کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش روی تو بود سجده ارباب یقین</p></div>
<div class="m2"><p>گرچه کوته نظران روی به دیوار کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر شوند از می اسرار تو واقف زهاد</p></div>
<div class="m2"><p>سالها خادمی خانه خمار کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ساکنان سر کویت چو نسیمی شب و روز</p></div>
<div class="m2"><p>به طواف حرم کعبه شدن عار کنند</p></div></div>