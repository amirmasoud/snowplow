---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>من به توفیق خدا، ره به خدا یافته ام</p></div>
<div class="m2"><p>عارف حق شده و ملک بقا یافته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شفاخانه روح القدس از دست مسیح</p></div>
<div class="m2"><p>خورده ام شربت شافی و شفا یافته ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر از کعبه به بتخانه روم عیب مکن</p></div>
<div class="m2"><p>که خدا را به حقیقت همه جا یافته ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاطر از محنت اغیار و دل از رنج خلاص</p></div>
<div class="m2"><p>رستگار آمده از درد و، دوا یافته ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوق عیشی که بدان دست سلاطین نرسد</p></div>
<div class="m2"><p>از وصالت من درویش گدا یافته ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز تو کام دگر از هر دو جهانم چون نیست</p></div>
<div class="m2"><p>چه کنم هردو جهان را چو تو را یافته ام؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرح اوراق کتبخانه اسرار ازل</p></div>
<div class="m2"><p>از خط و زلف و رخ و خال تو وا یافته ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناله و سوز دل از آتش عشق است مرا</p></div>
<div class="m2"><p>مکن اندیشه که از باد هوا یافته ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیستم منتظر جنت و فردوس و لقا</p></div>
<div class="m2"><p>از رخت جنت و فردوس و لقا یافته ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در طواف حرم کوی تو ای کعبه حسن</p></div>
<div class="m2"><p>هردم از مشعر موی تو صفا یافته ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای نسیمی ز خیال رخ آن ماه بپرس</p></div>
<div class="m2"><p>کز خیال رخ آن ماه چه ها یافته ام</p></div></div>