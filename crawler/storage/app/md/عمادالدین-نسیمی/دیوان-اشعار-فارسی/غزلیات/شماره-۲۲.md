---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>مرا در آتش غم، عشقت آن زمان انداخت</p></div>
<div class="m2"><p>که عشق روی تو آشوب در جهان انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تیر غمزه چو چشمت مرا بزد گفتم</p></div>
<div class="m2"><p>که مشتری نظری بر من از کمان انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو زلف اگرچه بر آتش مرا رخت بنشاند</p></div>
<div class="m2"><p>لبت مرا چو سخن در همه زبان انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سحر ز دامن زلفت هوا غبار گرفت</p></div>
<div class="m2"><p>نسیم صبح در آفاق بوی جان انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صدف به شکر دهانت گشاد لب زانرو</p></div>
<div class="m2"><p>سحاب دانه لؤلؤش در دهان انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی که نسبت روی تو را به مه می کرد</p></div>
<div class="m2"><p>خجل شد از تو نظر چون بر آسمان انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آستان قبول تو سرور آن کس شد</p></div>
<div class="m2"><p>که همچو پرده سر خود بر آستان انداخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین که حسن رخت لایزال و لم یزل است</p></div>
<div class="m2"><p>نظر ز روی تو چون یک نفس توان انداخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جزو لایتجزا حکیم قایل نیست</p></div>
<div class="m2"><p>مگر دهان تو او را در این گمان انداخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گرد لعل تو می گشت عقل چون پرگار</p></div>
<div class="m2"><p>حدیث نقطه موهوم در میان انداخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگرچه کشتی تن بشکند چه باک او را</p></div>
<div class="m2"><p>که باد شرطه فضل تو بر کران انداخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بپرس حال نسیمی ز چشم و زلف و ببین</p></div>
<div class="m2"><p>که خسته را به دو سودا چه ناتوان انداخت</p></div></div>