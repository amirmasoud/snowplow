---
title: >-
    شمارهٔ ۱۵۷ - استقبال از حافظ
---
# شمارهٔ ۱۵۷ - استقبال از حافظ

<div class="b" id="bn1"><div class="m1"><p>تکیه کن بر فضل حق، ای دل ز هجران غم مخور</p></div>
<div class="m2"><p>وصل یار آید، شوی زان خرم، ای جان غم مخور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه جانسوز است درد هجر جانان، غم مخور</p></div>
<div class="m2"><p>کز وصال او، رسی روزی به درمان، غم مخور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی گل خندان نماند دایم اطراف چمن</p></div>
<div class="m2"><p>غنچه باز آید، شود عالم گلستان، غم مخور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه از درد فراق ای دل ز پا افتاده ای</p></div>
<div class="m2"><p>از کرم دستت بگیرد فضل یزدان، غم مخور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه خوردی هردم از جام فلک صدگونه زهر</p></div>
<div class="m2"><p>هم به تریاکی رسی زین چرخ گردان غم مخور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر پریشان روزگاری بی سر زلف نگار</p></div>
<div class="m2"><p>بسته ای دل را در آن زلف پریشان، غم مخور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی لب خندان او شبها شدی گر اشکبار</p></div>
<div class="m2"><p>بازیابی روز وصل، ای چشم گریان غم مخور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک دو روزی دور اگر گردید برعکس مراد</p></div>
<div class="m2"><p>همچنین دایم نخواهد گشت دوران، غم مخور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه مشکل می نماید بر دل عاشق فراق</p></div>
<div class="m2"><p>چون کند وصلش عنایت، گردد آسان، غم مخور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در ازل چون بسته ای با عشق او عهد الست</p></div>
<div class="m2"><p>تا ابد عشقش بدان عهد است و پیمان، غم مخور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سلسبیل و کوثر و جنات عدن و حور عین</p></div>
<div class="m2"><p>وصل یار است، آن چو حاصل کرده ای، زان غم مخور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست از تیر ملامت عاشقان را ترس و باک</p></div>
<div class="m2"><p>گر تو ز ایشانی یقین، از تیرباران غم مخور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون تو را با وصل جانان اتصالی سرمدی است</p></div>
<div class="m2"><p>گر به صورت غایب است از دیده جانان، غم مخور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرچه دنیا را نبی زندان مؤمن گفته است</p></div>
<div class="m2"><p>چون مخلد نیست این زندان، ز زندان غم مخور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون به فضل حق تعالی عارف اسما شدی</p></div>
<div class="m2"><p>اسم اعظم را بخوان، از دیو و شیطان غم مخور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وقت آن آمد که بگشاید نسیم از روی لطف</p></div>
<div class="m2"><p>نافه ای زان جعد زلف عنبرافشان، غم مخور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرچه رنجوری ز رنج دیو باشد خلق را</p></div>
<div class="m2"><p>حرز جان عاشقان چون هست قرآن، غم مخور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جور گردون گرچه بسیار است و قهرش بی شمار</p></div>
<div class="m2"><p>رحمت رحمان چو بی حد است و پایان، غم مخور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر جهان از فتنه یأجوج پرطوفان شود</p></div>
<div class="m2"><p>چون تویی با نوح در کشتی، ز طوفان غم مخور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون «سوداالوجه فی الدارین » حاصل کرده ای</p></div>
<div class="m2"><p>گنج قارون داری و ملک سلیمان، غم مخور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از «سقا هم » چون شراب معرفت نوشیده ای</p></div>
<div class="m2"><p>هستی آن خضری که نوشد آب حیوان، غم مخور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هم رسی روزی به مقصود دل از شاهی که او</p></div>
<div class="m2"><p>می دهد کام دل درویش و سلطان، غم مخور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>(چون ندارد پیش حق چندان بقایی ملک و مال</p></div>
<div class="m2"><p>گر نشد جمع آن تو را، خوش باش و چندان غم مخور)</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>«کنت کنزا مخفیا» ادراک هربی دیده نیست</p></div>
<div class="m2"><p>چون تو داری گوهر آن گنج پنهان غم مخور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون ز غواصان دریای الوهیت شدی</p></div>
<div class="m2"><p>در دل دریا شو و از آب عمان غم مخور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صورت و نقش جهان کان است و معنی گوهرش</p></div>
<div class="m2"><p>چون تویی گوهرشناس، ای گوهر کان غم مخور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون در دکان حرص و آز و شهوت بسته ای</p></div>
<div class="m2"><p>زین تجارت نیستت یک حبه خسران غم مخور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روی و موی آن نگار ایمان و کفر عاشق است</p></div>
<div class="m2"><p>گر بدین آورده ای ای عاشق ایمان، غم مخور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جان عاشق را چو مسکن روضه دارالبقاست</p></div>
<div class="m2"><p>گر شود روزی سرای جسم ویران، غم مخور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گوی چوگان سر زلفش کن ای دل جان و سر</p></div>
<div class="m2"><p>میل آن خورشید اگر داری ز چوگان غم مخور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر هوای کعبه داری در ره، ای عاشق، چو ما</p></div>
<div class="m2"><p>زاد راهت خون دل کن وز مغیلان غم مخور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای نسیمی با تو چون دارد نظر فضل اله</p></div>
<div class="m2"><p>بند و زندانش همه لطف است و احسان، غم مخور</p></div></div>