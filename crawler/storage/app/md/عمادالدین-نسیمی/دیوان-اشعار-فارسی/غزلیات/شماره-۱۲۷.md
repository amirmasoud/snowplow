---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>دردمندان تو اندیشه درمان نکنند</p></div>
<div class="m2"><p>مستمندان غمت فکر سر و جان نکنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمره ای را که بود خاک درت آب حیات</p></div>
<div class="m2"><p>چون سکندر طلب چشمه حیوان نکنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش چشم تو بمیرم که غرامت باشد</p></div>
<div class="m2"><p>جان اگر صرف چنین گوشه نشینان نکنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سفر کعبه کویت چو کنند اهل صفا</p></div>
<div class="m2"><p>حذر از بادیه و خار مغیلان نکنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوی جمعیت از آن حلقه نیاید که در او</p></div>
<div class="m2"><p>ذکر آن سلسله زلف پریشان نکنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش روی تو کنم سجده که ارباب یقین</p></div>
<div class="m2"><p>قبله جز روی تو، ای قبله ایمان نکنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مفلسان حرم کوی تو از حشمت و جاه</p></div>
<div class="m2"><p>چون نسیمی هوس ملک سلیمان نکنند</p></div></div>