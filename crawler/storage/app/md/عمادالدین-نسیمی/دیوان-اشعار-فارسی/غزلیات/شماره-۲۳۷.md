---
title: >-
    شمارهٔ ۲۳۷
---
# شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>ندا آمد به جان از چرخ پروین</p></div>
<div class="m2"><p>که بالا رو چو دزد بسته منشین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی اندر سفر چندین نماند</p></div>
<div class="m2"><p>جدا از شهر و از یاران پیشین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندای «ارجعی » آخر شنیدی</p></div>
<div class="m2"><p>از آن سلطان و شاهنشاه یاسین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در این ویرانه جغدانند ساکن</p></div>
<div class="m2"><p>چه مسکن ساختی ای باز مسکین!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه آساید به هر پهلو که خسبد</p></div>
<div class="m2"><p>کسی کز خار دارد او نهالین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه پیوندی کند صراف و قلاب</p></div>
<div class="m2"><p>چه نسبت زاغ را با باز و شاهین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه آرایی به گچ ویرانه ای را</p></div>
<div class="m2"><p>که بالا نقش دارد زیر سجین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا جان را نیارایی به حکمت</p></div>
<div class="m2"><p>که ارزد هر دمت صد چین و ماچین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه زان حکمت که مایه گفت و گوی است</p></div>
<div class="m2"><p>از آن حکمت که جان گردد خدابین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو گوهر شو که خواهند یا نخواهند</p></div>
<div class="m2"><p>نشانندت همه بر تاج زرین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رها کن پسروی چون نون کج مج</p></div>
<div class="m2"><p>الف می باش، فرد و راست بنشین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کلوخ انداز کن در عشق مردان</p></div>
<div class="m2"><p>تو هم مردی ولی مرد کلوچین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عروسی کلوخی با کلوخی</p></div>
<div class="m2"><p>کلوخ آرد نثار و سنگ کابین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به گورستان برو در خشت بنگر</p></div>
<div class="m2"><p>که نشناسی تو سرهاشان ز پایین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خداوندا رسان جان را به جانان</p></div>
<div class="m2"><p>از آن راهی که رفتند آل یاسین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دعای ما تو ایشان را درآموز</p></div>
<div class="m2"><p>چنان کز ما دعا وز توست آمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عنایت آنچنان فرما که باشد</p></div>
<div class="m2"><p>ز ما احسان اندک وز تو تحسین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نسیمی را به فضل خود نگه دار</p></div>
<div class="m2"><p>ز مکر دیو و از راه شیاطین</p></div></div>