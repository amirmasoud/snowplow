---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>ز سودای سر زلفش سرم دنگ است و سودایی</p></div>
<div class="m2"><p>بیا ای دنگه سر صوفی! ببین تا در چه سودایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو دنگ باده و بنگی نه از عشق خدا دنگی</p></div>
<div class="m2"><p>از آن پیوسته دلتنگی به غفلت عمر فرسایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو را سودای سیم و زر، مرا آن سرو سیمین بر</p></div>
<div class="m2"><p>اسیر و مبتلا کرده ربوده عقل و دانایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان از فتنه حسنش پرآشوب است و پرغوغا</p></div>
<div class="m2"><p>چرا زین فتنه ای غافل چرا در جنگ و غوغایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حجاب خویشتن بینی ز ره بردار و بیخود شو</p></div>
<div class="m2"><p>که نتوان حسن حق دیدن به خود بینی و خودرایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمال حق در این عالم، ببین امروز و حق بین شو</p></div>
<div class="m2"><p>که فردا کور خواهی بود اگر موقوف فردایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی را دیده احول ز کج بینی دو می بیند</p></div>
<div class="m2"><p>ببینی گر نه ای احول به تنهایی که تنهایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خط و خال و زلف او شد اشیا جمله پیموده</p></div>
<div class="m2"><p>تو تا کی ز آتش شهوت ز شش سو بادپیمایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو مجنون شو ار خواهی که بینی روی لیلی را</p></div>
<div class="m2"><p>که لیلی را نمی بیند بجز مجنون شیدایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به چشمش دل چرا دادی نگر در من، نگر در من</p></div>
<div class="m2"><p>که عاشق چون نگه دارد دل از ترکان یغمایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیا ای صورت رحمان! که آمد روز آن دولت</p></div>
<div class="m2"><p>که مشتاقان رویت را نقاب از چهره بگشایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شب اسراست آن گیسو و قوسین اسم آن ابرو</p></div>
<div class="m2"><p>بیا حق را در این اسرا ببین گر مرد اسرایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شدم در قلزم سودا چو گیسوی تو غرق اما</p></div>
<div class="m2"><p>در این دریا تو هرکس را کجا چون در بدست آیی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلم پرخون شد از سودا، بیا قیفال دل بگشا</p></div>
<div class="m2"><p>که شوقت آتش محض است و ذات عشق صفرایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صفات ذات مطلق را تویی آیینه صورت</p></div>
<div class="m2"><p>به معنی گرچه از وجه دگر اسمای حسنایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آنرو قبله رویت هدی للعالمین آمد</p></div>
<div class="m2"><p>که حق را مظهر کلی و گنج سر اسمایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو آن یوسف لقا ماهی که در مصر الوهیت</p></div>
<div class="m2"><p>عزیز حقی و حق را هم اسم و هم مسمایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ملک شد عاشق رویت از آنرو می کند سجده</p></div>
<div class="m2"><p>چه حسن است این تعالی الله بدین خوبی و زیبایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو آن خورشید تابانی که در دنیی و در عقبی</p></div>
<div class="m2"><p>به رخسار آفت جانها به زلف آرام دلهایی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به حسن و صورت و معنی تویی آن واحد مطلق</p></div>
<div class="m2"><p>که چون ذات الوهیت بخوبی فرد و یکتایی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ندید از اول فطرت جهان تا آخر خلقت</p></div>
<div class="m2"><p>چو رویت صورتی زانرو که بی مانند و همتایی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز اشیا چون جدا دانم تو را ای عین اشیا؟ چون</p></div>
<div class="m2"><p>محیطی بر همه اشیا و عین جمله اشیایی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وجود هرچه می بینم تویی در ظاهر و باطن</p></div>
<div class="m2"><p>چه عالی گوهری یارب! چه بی اندازه دریایی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تویی آن عالم وحدت که هستی منشاء کثرت</p></div>
<div class="m2"><p>از آن در جا نمی گنجی که هم در جا و بی جایی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نهان چون گویم ای دلبر تو را از دیده، چون اعمی</p></div>
<div class="m2"><p>که در هر ذره می بینم که چون خورشید پیدایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیا ای بی نظیر من که خوبان دو عالم را</p></div>
<div class="m2"><p>به حسن خود غنی سازی چو روی خود بیارایی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سرای هر دو عالم را لقا بنمای و جنت کن</p></div>
<div class="m2"><p>که رضوان حریر اندام و حور سدره بالایی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نسیمی نفحه عیسی در اشیا می دمد هردم</p></div>
<div class="m2"><p>بیا ای زنده گر مشتاق انفاس مسیحایی</p></div></div>