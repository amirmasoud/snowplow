---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>نه چرخ دیده نه سیاره از بدایت حسن</p></div>
<div class="m2"><p>ملک صفت بشری چون تو در نهایت حسن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه صورتی! چه جمالی! علیک عین الله</p></div>
<div class="m2"><p>کمال حسن همین است و حد غایت حسن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آفتاب جمالت زوال بادا دور</p></div>
<div class="m2"><p>کز او به اوج رسید آفتاب رایت حسن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خسروان ملاحت تویی بعون الله</p></div>
<div class="m2"><p>شهنشهی که بر او ختم شد ولایت حسن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا ز دانش و عقل این قدر کفایت بس</p></div>
<div class="m2"><p>که تابع سخن عشقم و کفایت حسن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخ چو ماه تو است آن که هست در شأنش</p></div>
<div class="m2"><p>نزول سوره لطف و در او روایت حسن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجا برم من بیدل ز جور خوبان داد</p></div>
<div class="m2"><p>که خوبرو همه جا هست در حمایت حسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرد به توبه مرا ره نمود و عشق به حسن</p></div>
<div class="m2"><p>زهی ضلالت عقل و زهی هدایت حسن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملولم از دم واعظ، کجاست اهل دلی؟</p></div>
<div class="m2"><p>که همچو گل ورقی خواند از روایت حسن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز عشق مست شوی چون نسیمی ای زاهد!</p></div>
<div class="m2"><p>اگر به سمع رضا بشنوی حکایت حسن</p></div></div>