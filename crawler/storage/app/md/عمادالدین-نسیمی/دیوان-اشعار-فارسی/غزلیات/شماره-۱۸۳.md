---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>من گنج لامکانم اندر مکان نگنجم</p></div>
<div class="m2"><p>برتر ز جسم و جانم در جسم و جان نگنجم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل و خیال انسان ره سوی من نیارد</p></div>
<div class="m2"><p>در وهم از آن نیایم در فهم از آن نگنجم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بحر بی کرانم حد و جهت ندارم</p></div>
<div class="m2"><p>من سیل بس شگرفم در ناودان نگنجم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من نقش کایناتم من عالم صفاتم</p></div>
<div class="m2"><p>من آفتاب ذاتم در آسمان نگنجم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من صبح روز دینم من مشرق یقینم</p></div>
<div class="m2"><p>در من گمان نباشد من در گمان نگنجم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من جنت و نعیمم، من رحمت و رحیمم</p></div>
<div class="m2"><p>من گوهر قدیمم در بحر و کان نگنجم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من جان جان جانم برتر ز انس و جانم</p></div>
<div class="m2"><p>من شاه بی نشانم من در نشان نگنجم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من رکن ضاد فضلم من دست زاد فضلم</p></div>
<div class="m2"><p>من روز داد فضلم من در زمان نگنجم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من مصحف کریمم، در لام فضل میمم</p></div>
<div class="m2"><p>من آیت عظیمم در هیچ شان نگنجم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من سر کاف و نونم، من بی چرا و چونم</p></div>
<div class="m2"><p>خاموش و لاتحرک من در بیان نگنجم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من سفره خلیلم من نعمت جلیلم</p></div>
<div class="m2"><p>من کاسه سپهرم در هفت خوان نگنجم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من منطق فصیحم من همدم مسیحم</p></div>
<div class="m2"><p>من ترجمان جیمم در ترجمان نگنجم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من قرص آفتابم حرف است آسیابم</p></div>
<div class="m2"><p>من لقمه بزرگم من در دهان نگنجم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من جانم ای نسیمی یعنی دم نعیمی</p></div>
<div class="m2"><p>درکش زبان ز وصفم من در لسان نگنجم</p></div></div>