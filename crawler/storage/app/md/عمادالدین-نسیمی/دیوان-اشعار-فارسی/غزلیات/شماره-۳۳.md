---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>سلطان غمت را دل پردرد مقام است</p></div>
<div class="m2"><p>آن دل چه نشان دارد و آن مرد کدام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو چون هست دلم بنده جاوید</p></div>
<div class="m2"><p>کار دلم از دولت وصل تو تمام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز پختن سودای سر زلف تو در سر</p></div>
<div class="m2"><p>دیگر هوس عاشق دلسوخته خام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آن که کنی عرضه سجاده و تسبیح</p></div>
<div class="m2"><p>مرغ دل ما فارغ از این دانه و دام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون توبه ز مستی کند آن رند که شد مست</p></div>
<div class="m2"><p>زان باده که روح القدسش جرعه جام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای کرده رخت روز، شب تیره ما را</p></div>
<div class="m2"><p>صبحی که نه با روی تو باشد همه شام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای طالب ناموس رها کن طلب نام</p></div>
<div class="m2"><p>در عشق بزرگی و کرامت نه به نام است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا محرم اسرار خیال تو دلم شد</p></div>
<div class="m2"><p>کار نظر از اشک چو لؤلؤ به نظام است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر طالب جنت که مرادش نه تو باشی</p></div>
<div class="m2"><p>وصل تو حرام آمد و حقا که حرام است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر طور لقا جان کلیمت «ارنی » گوی</p></div>
<div class="m2"><p>دیدار تو می خواهد و مشتاق کلام است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محراب نسیمی رخ و ابروی تو باشد</p></div>
<div class="m2"><p>تا روی تواش قبله و چشم تو امام است</p></div></div>