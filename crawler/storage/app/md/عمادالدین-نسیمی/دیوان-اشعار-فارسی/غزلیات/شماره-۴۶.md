---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>مطلع نور تجلی آفتاب روی اوست</p></div>
<div class="m2"><p>لیلة القدری که می گویند هست آن موی اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قاب قوسینی که در معراج دید آن شب رسول</p></div>
<div class="m2"><p>گر به چشم دل ببینی هیئت ابروی اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عروة الوثقی که خواند عارفش حبل المتین</p></div>
<div class="m2"><p>سوره واللیل زلفش و آیت گیسوی اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلد و فردوس و نعیم و روضه دارالسلام</p></div>
<div class="m2"><p>چون به معنی بنگری وصف بهشت کوی اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج مخفی را طلسم و اسم اعظم را کلید</p></div>
<div class="m2"><p>طره عنبر، نسیم سنبل هندوی اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معجزات انبیا و سر علم من لدن</p></div>
<div class="m2"><p>حرفی از دیوان سحر غمزه جادوی اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حقیقت رو به سوی کعبه می دانی کراست؟</p></div>
<div class="m2"><p>هر که را روی دل از دنیی و عقبی سوی اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچنانم غرقه در فکرش که در بحر محیط</p></div>
<div class="m2"><p>نقش هر صورت که می بینم خیال روی اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>(جانم از پابوس وصلش گرچه دور افتاده است</p></div>
<div class="m2"><p>صید آن زلف پریشان است که همزانوی اوست)</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی شود حاصل وصال یار بی جور رقیب؟</p></div>
<div class="m2"><p>تا گل صدبرگ باشد خار هم پهلوی اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای نسیمی نحل اندر شأن آن لب کس ندید</p></div>
<div class="m2"><p>کاین چنین پاکیزه شهد ناب در کندوی اوست</p></div></div>