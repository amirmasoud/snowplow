---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>ای سر زلف شکن بر شکن و چین بر چین</p></div>
<div class="m2"><p>بستد از ملکت روم وز حبش لشکر چین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چین به چین نافه چین کرده به چین گل برچین</p></div>
<div class="m2"><p>نرگسش خفته و از یاسمینش گل برچین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز گل دسته ببسته صنمم سنبل تر</p></div>
<div class="m2"><p>ز هرش رشته فرو هشته دوصد نافه (چین)</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حلقه در حلقه گره در گره و بند به بند</p></div>
<div class="m2"><p>پیچ در پیچ و زره بر زره و چین برچین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش تا شکری چینم و شفتالوی چند</p></div>
<div class="m2"><p>ابرویش گفت بچین غمزه او گفت مچین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک من گفت به خون تو خطی آوردم</p></div>
<div class="m2"><p>وای بزم حالمزه بوسوز اگر الوسه حسین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من در این چین و مچین گشته اسیرم چه کنم</p></div>
<div class="m2"><p>رخ و زلف بت من در همه چین است و مچین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ختا و ختن خسرو خوبان جهان</p></div>
<div class="m2"><p>همچو ترکی نبود در همه چین و ماچین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای نسیمی! چو تمنای وصالش کردی</p></div>
<div class="m2"><p>خار باغش شو و از باغ لطافت گل چین</p></div></div>