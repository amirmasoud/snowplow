---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>ای روز و شب خیال رخت همنشین ما</p></div>
<div class="m2"><p>جاوید باد عشق جمالت قرین ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دم که بود نقش وجودم عدم هنوز</p></div>
<div class="m2"><p>مهر تو بود مونس جان حزین ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما سجده پیش قبله روی تو می کنیم</p></div>
<div class="m2"><p>تا هست و بود قبله، همین است دین ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را هوای جنت و خلد برین کجاست</p></div>
<div class="m2"><p>روی تو هست جنت و خلد برین ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی که دور چرخ دهد خاک ما به باد</p></div>
<div class="m2"><p>نگذارد آستان تو، خاک جبین ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خاتم جهان ملاحت به لطف و حسن</p></div>
<div class="m2"><p>شد مهر مهر روی تو نقش نگین ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا در هوای مهر تو چون ذره گم شدیم</p></div>
<div class="m2"><p>گو برمخیز دشمن از این پس به کین ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هردم به چشم اهل وفا نازنین تر است</p></div>
<div class="m2"><p>هرچند ناز می کند آن نازنین ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست آرزوی جان نسیمی وصال تو</p></div>
<div class="m2"><p>ای آرزوی جان، نفس واپسین ما</p></div></div>