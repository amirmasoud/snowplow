---
title: >-
    شمارهٔ ۲۴۳
---
# شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>دویی شرک است از آن بگذر، موحد باش و یکتا شو</p></div>
<div class="m2"><p>وجود ما سوی الله را به لا بگذار و الا شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر توحید اگر داری چو یکرنگان سودایی</p></div>
<div class="m2"><p>درآ در حلقه زلفش ز یکرنگان سودا شو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسیح از نفخه آدم مصور گشت و دم دم شد</p></div>
<div class="m2"><p>تو گر می خواهی آن دم را، بیا و همدم ما شو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخ و زلف و خط و خالش کلام ایزدی می دان</p></div>
<div class="m2"><p>اگر تفسیر می خواهی امین سر اسما شو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشو چون عیسی مریم به چرخ چارمین قانع</p></div>
<div class="m2"><p>دل از حد و جهت برکن، مکان بگذار و بالا شو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو هست آیینه مؤمن به قول مصطفی مؤمن</p></div>
<div class="m2"><p>بیا در صورت خوبان ببین حق را و دانا شو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر چون موسی عمران تمنای لقا داری</p></div>
<div class="m2"><p>جلا ده دیده دل را، به حق دانا و بینا شو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به چوگان سر زلفش فلک را پا و سر بشکن</p></div>
<div class="m2"><p>به دور نقطه خالش چو خالش بی سر و پا شو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بینی مصحف رویش سخن ز انا فتحنا گو</p></div>
<div class="m2"><p>چو یابی عقد گیسویش به الرحمن و طه شو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عین و لام و میم ما رموز کن فکان دریاب</p></div>
<div class="m2"><p>به فا و ضاد و لام او در اشیا عین اشیا شو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز امر کاف و نون کن نه امروز آمدی بیرون</p></div>
<div class="m2"><p>نداری اول و آخر، برو خالی ز فردا شو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو گنج گوهر جانی مشو در آب و گل پنهان</p></div>
<div class="m2"><p>در اشیا چون گرفتی جا، رها کن جا و بیجا شو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نباشد معدن لؤلؤ کنار خشک بحر ای دل!</p></div>
<div class="m2"><p>اگر دردانه می خواهی فرو در قعر دریا شو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نسیمی شد به حق واصل به فضل دولت یزدان</p></div>
<div class="m2"><p>تو نیز این بخت اگر خواهی فدای روی زیبا شو</p></div></div>