---
title: >-
    شمارهٔ ۴ - مسدس ترکیب
---
# شمارهٔ ۴ - مسدس ترکیب

<div class="b" id="bn1"><div class="m1"><p>ای شاه تخت «من عرف »</p></div>
<div class="m2"><p>شد نقد طاعاتم تلف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بقاء بحر لطف</p></div>
<div class="m2"><p>دستم تو گیری از شرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا دامنت آرم به کف</p></div>
<div class="m2"><p>یا حضرت شاه نجف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بند عصیانم اسیر</p></div>
<div class="m2"><p>دارم گناهان کبیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بی مثال و بی نظیر!</p></div>
<div class="m2"><p>الحق تویی شاه و امیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افتاده ها را دست گیر</p></div>
<div class="m2"><p>یا حضرت شاه نجف!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاها! ولی و داوری</p></div>
<div class="m2"><p>مردان عالم را سری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم رهنما و رهبری</p></div>
<div class="m2"><p>ساقی حوض کوثری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم یاور و هم سروری</p></div>
<div class="m2"><p>یا حضرت شاه نجف!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلطان و شاه کاملی</p></div>
<div class="m2"><p>دارم به مهرت خوشدلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد صبح ایمانم جلی</p></div>
<div class="m2"><p>گویم ز راه مقبلی:</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا شاه مردان! یا علی!</p></div>
<div class="m2"><p>یا حضرت شاه نجف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مقصود ذات آدمی</p></div>
<div class="m2"><p>در محیط اعظمی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم اعظمی هم اعلمی</p></div>
<div class="m2"><p>موسی ید و عیسی دمی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سلطان هر دو عالمی</p></div>
<div class="m2"><p>یا حضرت شاه نجف!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دشمن ز تیغت سینه چاک</p></div>
<div class="m2"><p>از تیغ غم بادا هلاک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایمان و مولای تو پاک</p></div>
<div class="m2"><p>دیگر ندارد هیچ باک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن را که برداری ز خاک</p></div>
<div class="m2"><p>یا حضرت شاه نجف!</p></div></div>