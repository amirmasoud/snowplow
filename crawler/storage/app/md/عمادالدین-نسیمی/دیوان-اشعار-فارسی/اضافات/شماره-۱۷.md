---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>ای «سقاهم ربهم » نام لبت</p></div>
<div class="m2"><p>آب حیوان جرعه جام لبت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روح قدسی دردی آشام لبت</p></div>
<div class="m2"><p>خون عاشق ریختن کام لبت</p></div></div>