---
title: >-
    شمارهٔ ۲ - مستزاد
---
# شمارهٔ ۲ - مستزاد

<div class="b" id="bn1"><div class="m1"><p>ای حسن تو دردانه چو ک . . . بنند اعلا</p></div>
<div class="m2"><p>چون نقطه فردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آن نقطه ذات است ز «با» گشته هویدا</p></div>
<div class="m2"><p>در دیده مردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شری است ترا در همه گنجینه دل ها</p></div>
<div class="m2"><p>ای مخزن اسرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوری است ترا در همه سر نسبت غوغا</p></div>
<div class="m2"><p>هر سوی که گردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنها که زنند از می عشق تو دم و دم</p></div>
<div class="m2"><p>از نفخه عیسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاصل شده آن دم همه را چون من شیدا</p></div>
<div class="m2"><p>از عشق تو دردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید مدام از غم تو گردد و تابد</p></div>
<div class="m2"><p>ای نور دو عالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مردمک دیده به هر منزل و هر جا</p></div>
<div class="m2"><p>بی فایده گردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این نه فلک خرقه کبود از غم عشقت</p></div>
<div class="m2"><p>ای شمع دل افروز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خون بکشیدند همه دامان قباها</p></div>
<div class="m2"><p>بر خورشید زردی (؟)</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زین نه پدر و چهار ام و سه پسر را</p></div>
<div class="m2"><p>ای حامله دم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پرورده چو جان ز آتش و آب و گل ما را</p></div>
<div class="m2"><p>در غنچه چو وردی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیچاره نسیمی چو از این چاره فنا شد</p></div>
<div class="m2"><p>ناچار فراقت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باید شدنش از غم بیهوده دنیا</p></div>
<div class="m2"><p>وز هر دل سردی</p></div></div>