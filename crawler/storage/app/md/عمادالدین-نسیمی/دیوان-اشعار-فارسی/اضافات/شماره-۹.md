---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>آدم به سهو کرد خطایی و سجده کرد</p></div>
<div class="m2"><p>زو درگذشت حق که خداوند عالم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آدمی ز غفلت اگر واردی رود</p></div>
<div class="m2"><p>تو نیز درگذر که ز فرزند آدم است</p></div></div>