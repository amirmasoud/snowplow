---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>چه مرغ است آن که دارد بیست و یک حرف</p></div>
<div class="m2"><p>نه پر دارد نه پا دارد نه، خود، سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه عالم فرو بگرفت سرش</p></div>
<div class="m2"><p>تعالی شانه والله اکبر</p></div></div>