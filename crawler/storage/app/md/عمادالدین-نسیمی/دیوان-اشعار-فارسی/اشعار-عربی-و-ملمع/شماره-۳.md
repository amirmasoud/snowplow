---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>رَأَیتُ وَجهَک مَن تَحتَ شِعرَک لَیلا</p></div>
<div class="m2"><p>بِنورِ وَجهَک یا ذَالجَلال والِاکرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اِذا فَنیتُ فَقُل لی بِفَضلَک فَضلا</p></div>
<div class="m2"><p>وَجَدتُ وَجهُ بَقایی بِفیٰ و ضاد و لام</p></div></div>