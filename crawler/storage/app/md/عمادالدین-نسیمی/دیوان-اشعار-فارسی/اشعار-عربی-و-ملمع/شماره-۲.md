---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>لک الفضل لک الاحسان، لک الجود لک النعمة</p></div>
<div class="m2"><p>لک الحمد لک الغفران، لک الشکر لک المنة</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لک العدل لک السلطان، لک الحکم لک البرهان</p></div>
<div class="m2"><p>لک القدیس یا سبحان، لک العطف لک النعمة</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لک الخلق لک الامر لک الامن لک الاعطا</p></div>
<div class="m2"><p>لک الملک لک الشأن لک العزة لک قدرة</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لک التنزیه یا ظاهر! لک التعظیم یا طاهر!</p></div>
<div class="m2"><p>لک التقدیر یا قادر! لک الحول لک القوة</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لک العفو لک الحمد لک العرش لک الکرسی</p></div>
<div class="m2"><p>لک الصدق لک الوعد لک القدر لک القدرة</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لک الانعام یا منعم! لک التوفیق یا محسن!</p></div>
<div class="m2"><p>لک التفضیل یا مفضل! لک الجاه لک الحشمة</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لک التوحید یا واحد! لک التمجید یا ماجد!</p></div>
<div class="m2"><p>لک التحمید یا حامد! لک العلم لک الحکمة</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الهی کهیعص اغفرلی و ارحم</p></div>
<div class="m2"><p>بفضل (باء) بسم الله یا ذوالفضل والرحمة</p></div></div>