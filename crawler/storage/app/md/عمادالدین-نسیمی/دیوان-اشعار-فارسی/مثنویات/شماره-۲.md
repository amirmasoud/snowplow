---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای بی خبر از حقیقت خویش!</p></div>
<div class="m2"><p>از غصه بیش و کم دلت ریش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوت زده راه و کرده عریان</p></div>
<div class="m2"><p>از کسوت عقل و دین و ایمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چنبر دیو کرده گردن</p></div>
<div class="m2"><p>مستغرق ما و غره من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مغرور حیات پنج روزه</p></div>
<div class="m2"><p>چون گوشه نشین به زهد و روزه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر طول امل نهاده بنیاد</p></div>
<div class="m2"><p>سرمایه عمر داده بر باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دیو قرین و از خدا دور</p></div>
<div class="m2"><p>ابلیس صفت به زهد مغرور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز محنت و حسرت و خسارت</p></div>
<div class="m2"><p>حاصل نشود از این تجارت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلداده به اهرمن چرایی؟</p></div>
<div class="m2"><p>خالی ز محبت خدایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر پیش فکنده ای چو حیوان</p></div>
<div class="m2"><p>رو کرده چو غول در بیابان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کارت شب و روز خورد و خواب است</p></div>
<div class="m2"><p>فکرت همه کار ناصواب است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشغول مشو به لذت جسم</p></div>
<div class="m2"><p>تا گم نشوی ز صورت اسم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای دور ز خانه طریقت!</p></div>
<div class="m2"><p>بیگانه ز عالم حقیقت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با بحر وجود آشنا شو</p></div>
<div class="m2"><p>جویای صفات و ذات ما شو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما آب حیات و عین ذاتیم</p></div>
<div class="m2"><p>ما نسخه عالم صفاتیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ماییم رضای کبریایی</p></div>
<div class="m2"><p>ماییم کتابت خدایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ما سی و د حرف لایزالیم</p></div>
<div class="m2"><p>ما مظهر ذات بی زوالیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قایم به وجود ماست اشیا</p></div>
<div class="m2"><p>بی هستی ما کجاست اشیا؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگشا نظر و جمال ما بین</p></div>
<div class="m2"><p>حسن رخ و خط و خال ما بین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ماییم دم مسیح مریم</p></div>
<div class="m2"><p>ماییم حروف اسم اعظم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ماییم طلسم گنج پنهان</p></div>
<div class="m2"><p>ماییم به حق حقیقت جان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ماییم کلیم و طور سینا</p></div>
<div class="m2"><p>ماییم لقا و عین بینا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ماییم کتاب و لوح و خامه</p></div>
<div class="m2"><p>ماییم بیان عرش نامه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ماییم شراب و جام ساقی</p></div>
<div class="m2"><p>ماییم اساس ملک باقی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در پرده دل چو غنچه پنهان</p></div>
<div class="m2"><p>چون گل ز نسیم خویش خندان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون سرو به گل ز عشق دلبر</p></div>
<div class="m2"><p>پا رفته فرو و دست بر سر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ما نوح و سفینه نجاتیم</p></div>
<div class="m2"><p>ما آب حیات کایناتیم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن دلبر گلعذار ماییم</p></div>
<div class="m2"><p>دل داده به یار و یار ماییم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ارخای عنان نفس کرده</p></div>
<div class="m2"><p>مست از می جهل و می نخورده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ایام حیات کرده ضایع</p></div>
<div class="m2"><p>نایافته ره به صنع صانع</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در قلزم جهل گشته ای غرق</p></div>
<div class="m2"><p>«بر» را از «هر» نکرده ای فرق</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گم کرده ره صواب و دانش</p></div>
<div class="m2"><p>در فکر جهان و این و آنش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چند از پی حرص و آز پویی</p></div>
<div class="m2"><p>مهمل شنوی، گزاف گویی؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در کار جهان چو بسته ای دل</p></div>
<div class="m2"><p>زین ره نرسد کسی به منزل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا کی غم خورد و خواب و شهوت</p></div>
<div class="m2"><p>ای عشوه نفس کرده محوت!</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون واو شش است اوحد ذات</p></div>
<div class="m2"><p>چون شش جهت از میانه برخاست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>«اوحد» برود «احد» بماند</p></div>
<div class="m2"><p>پیداست کز آن چه حد بماند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خوی ددی از مزاج بگذار</p></div>
<div class="m2"><p>انسان شو و سر ز پیش بردار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بی کبر و نفاق و بی ریا شو</p></div>
<div class="m2"><p>آیینه ذات کبریا شو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ماییم چو مصحف الهی</p></div>
<div class="m2"><p>ماییم سفید و هم سیاهی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون لاله کشیده داغ بر دل</p></div>
<div class="m2"><p>چون سنبل زلف در سلاسل</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از فضل خدای گشته فاضل</p></div>
<div class="m2"><p>مقصود نسیمی کرده حاصل</p></div></div>