---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>آن روضه مقدس و آن کعبه صفا</p></div>
<div class="m2"><p>آن مرقد مطهر و آن قبله دعا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن قبه منور با رفعت و شرف</p></div>
<div class="m2"><p>کو، هست عرش منزلت و آسمان بنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانی که چیست؟ قبله حاجات جمله خلق</p></div>
<div class="m2"><p>یعنی مقام مشهد سلطان اولیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر کمال و خازن اسرار «لوکشف »</p></div>
<div class="m2"><p>گنج علوم و گوهر دریای «لافتی »</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قایم مقام ختم رسل، صدر کائنات</p></div>
<div class="m2"><p>مسندنشین بارگه ملک کبریا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرخیل اصفیا و امام هدی بحق</p></div>
<div class="m2"><p>سلطان هردو کون علی موسی الرضا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن آفتاب برج امامت که می رسد</p></div>
<div class="m2"><p>خورشید را ز قبه پرنور او ضیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن پادشاه ملک ولایت که همتش</p></div>
<div class="m2"><p>بر خوان خویش خلق جهان را زده صلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن در قیمتی که به دریای فکر و عقل</p></div>
<div class="m2"><p>کس ره بدو نمی برد الا به آشنا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاهی که خلق را سبب حب و بغض او</p></div>
<div class="m2"><p>از دوزخ است خوف و به جنت بود رجا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای افصحی که علم ترا در بیان حق</p></div>
<div class="m2"><p>چون دانش رسول خدا نیست انتها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تسبیح ذاکران تو یعنی ملائکه</p></div>
<div class="m2"><p>سبحان من تقدس بالعز والعلا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اوراد ساکنان سماوات، روز و شب</p></div>
<div class="m2"><p>در مدح جد و باب تو یاسین و هل اتی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در وصف روی و موی تو خوانند قدسیان</p></div>
<div class="m2"><p>هر صبح و شام سوره واللیل والضحی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کی وصف (و) مدحت تو بود حد هرکسی</p></div>
<div class="m2"><p>چون کردگار گفته ترا مدحت و ثنا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون وارث محمد و موسی تویی، بحق</p></div>
<div class="m2"><p>آمد برون ترا ز شجر مصحف و عصا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سنگی که هست از کف پایت بر او نشان</p></div>
<div class="m2"><p>چون مروه خلق سجده کنند از سر صفا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از بهر روشنی بصر، خاک درگهت</p></div>
<div class="m2"><p>در دیده می کشند خلایق چو توتیا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فراش بارگاه جلال تو: جبرئیل</p></div>
<div class="m2"><p>مداح خاندان شما حضرت خدا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باب بنی فضائل تو: مرتضی علی</p></div>
<div class="m2"><p>جد بزرگوار تو: سلطان انبیا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در گوش خادمان و مقیمان درگهت</p></div>
<div class="m2"><p>هردم رسد ز حضرت حق «فادخلوا» ندا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز آواز حافظان خوش الحان حضرتت</p></div>
<div class="m2"><p>هر بامداد در ملکوت اوفتد صدا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر بلبلان روضه تو رشک می برند</p></div>
<div class="m2"><p>بستان سرای جنت و مرغان خوش نوا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در زیر سایه علم مصطفی رود</p></div>
<div class="m2"><p>آن کس که او به ملک ولایت زند لوا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حق موالیان تو در روز رستخیز</p></div>
<div class="m2"><p>خلد برین و شربت کوثر بود جزا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن کس که کرده در ره دین شما خلاف</p></div>
<div class="m2"><p>او را همیشه در درک اسفل است جا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو حجت خدایی و ما بندگان همه</p></div>
<div class="m2"><p>بنهاده ایم سر چو قلم بر خط رضا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یا شاه اولیا نظری کن که عاجزیم</p></div>
<div class="m2"><p>افتاده در کمند غم و محنت و بلا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دارالشفاء خسته دلان آستان توست</p></div>
<div class="m2"><p>با صد نیاز آمده ایم از پی شفا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چشم عنایتی به سوی حال ما فکن</p></div>
<div class="m2"><p>تا از عنایت تو رهیم از غم و عنا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هرکس ز حادثات به جایی برد پناه</p></div>
<div class="m2"><p>آورده ایم ما به جناب تو التجا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هستیم ز سائلان درت یا ابوالحسن</p></div>
<div class="m2"><p>داریم امید از کرمت رحمت و عطا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>درویشم و تو پادشه بنده پروری</p></div>
<div class="m2"><p>سلطان تویی و ما همه بیچاره و گدا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دردی که بر دل من بیچاره خاطر است</p></div>
<div class="m2"><p>آن را هم از خزانه لطفت رسد دوا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جز زاری و دعا چه بدان حضرت آوریم</p></div>
<div class="m2"><p>ای قادر کریم و خداوند رهنما</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یارب به حق ذات تو و بی نیازی ات</p></div>
<div class="m2"><p>از خلق، ای تو باقی و عالم همه فنا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یارب به انبیا و رسولان حضرتت</p></div>
<div class="m2"><p>یارب به عز و منزلت و فخر مصطفی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یارب بحق سید کونین و آل او</p></div>
<div class="m2"><p>یارب به علم و حلم و کمالات مرتضی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یارب به پاکی و شرف فاطمه که هست</p></div>
<div class="m2"><p>جبار بر فضیلت و بر عصمتش گوا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یارب به عزت حسن و حرمت حسین</p></div>
<div class="m2"><p>مسموم کین دشمن و مظلوم کربلا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یارب به سوز سینه زین العباد، کو</p></div>
<div class="m2"><p>هرگز به عمر خویش نیاسود از بکا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یارب به فضل و دانش باقر که چون نبی</p></div>
<div class="m2"><p>هرگز نبود مهر دلش از خدا جدا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یارب به صدق جعفر صادق که در جهان</p></div>
<div class="m2"><p>اسلام را به برکت علمش بود بقا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یارب به حق موسی کاظم که در دو کون</p></div>
<div class="m2"><p>بر خلق کائنات امیر است و مقتدا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یارب بدان شهید خراسان که درگهش</p></div>
<div class="m2"><p>چون کعبه است قبله حاجات خلق را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یارب به حق موسی رضا آن شه جوان</p></div>
<div class="m2"><p>والی ولی و آل حق و والی ولا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یارب به حرمت تقی متقی که بود</p></div>
<div class="m2"><p>سرو ریاض خلد و گل باغ انما</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یارب به ذات پاک علی نقی که هست</p></div>
<div class="m2"><p>قایم مقام آن شه معصوم مجتبی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یارب به طاعت حسن عسکری که کس</p></div>
<div class="m2"><p>چون او امور حق به شرایط نکرد ادا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یارب به حق مهدی هادی که جمله خلق</p></div>
<div class="m2"><p>دارند تا به حشر بدان شاه اقتدا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یارب به ذات جمله امامان که ذاتشان</p></div>
<div class="m2"><p>پاک آفریده ای ز همه زلت و خطا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کاین بنده روی کرده بدان کعبه نجات</p></div>
<div class="m2"><p>آورده است زاری و حاجات با دعا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جرمم همه ببخش و مرادات بنده ات</p></div>
<div class="m2"><p>از فضل خود بدآور و حاجات کن روا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>امیدوار بنده نسیمی به فضل توست</p></div>
<div class="m2"><p>یا قاضی الحوایج! یا سامع الدعا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رحمت بدین فقیر ز فضلت چو حاصل است</p></div>
<div class="m2"><p>داریم تا به حشر بدان شاه اقتدا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>(یارب به حق ذکر نسیمی و سوز او</p></div>
<div class="m2"><p>من بنده را ببخش گنه لطف کن عطا)</p></div></div>