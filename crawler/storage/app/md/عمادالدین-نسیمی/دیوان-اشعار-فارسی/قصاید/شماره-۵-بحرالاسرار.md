---
title: >-
    شمارهٔ ۵ - بحرالاسرار
---
# شمارهٔ ۵ - بحرالاسرار

<div class="b" id="bn1"><div class="m1"><p>مشعل خورشید کز نورش جهان را زیور است</p></div>
<div class="m2"><p>چند باشی گرم در چهرش که طشت آذر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغها دارد فلک بر سینه از مهر رخش</p></div>
<div class="m2"><p>پینه های داغ باشد آن که گویی اختر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا زداید زنگ از آیینه چرخ کبود</p></div>
<div class="m2"><p>ماه نو هر ماه همچون صیقل روشنگر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهربانی می نماید آفتاب اما چه سود</p></div>
<div class="m2"><p>نوعروسی را که روی خوب زیر چادر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مسیحا گر بود بر آفتابت تکیه گاه</p></div>
<div class="m2"><p>روز آخر خشت بالین است و خاکت بستر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو قارون طالب گنجی ولی آگه نه ای</p></div>
<div class="m2"><p>زان که در هر جا که گنجی هست مارش بر سر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشتی آفاق را از مال مالامال دان</p></div>
<div class="m2"><p>کی سلامت می رود کاین بحر پرشور و شر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر امید آب حیوان از چه باید کند جان؟</p></div>
<div class="m2"><p>عاقبت لب تشنه خواهد مرد اگر اسکندر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملک دنیا سر به سر چون خانه زنبور دان</p></div>
<div class="m2"><p>گاه در وی شهد صافی گاه زهر و نشتر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پا به حرمت نه به روی خاک اگر داری خبر</p></div>
<div class="m2"><p>کاین غبار تیره فرق خسروان کشور است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنگره ایوان شه می گوید از دارا نشان</p></div>
<div class="m2"><p>خشت چرخ پیرزن خاک قباد و قیصر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر گلی کز خاک می روید نشان گلرخی است</p></div>
<div class="m2"><p>سبزه برطرف چمن خط بتان دلبر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرچه عالم بود در فرمان سنجر آن زمان</p></div>
<div class="m2"><p>سنجدی ناید برون آنجا که خاک سنجر است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تن یکی مشت غبار اندر ره باد فناست</p></div>
<div class="m2"><p>عمر کوه برف، لیکن آفتابش بر سر است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آدمی را معرفت باید نه جامه از حریر</p></div>
<div class="m2"><p>در صدف بنگر که او را سینه پرگوهر است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرد را جرأت به کار آید نه خنجر در میان</p></div>
<div class="m2"><p>ورنه هر پر ماکیانی را به پهلو خنجر است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر نه ای ابله مرو با حرص زر در زیر خاک</p></div>
<div class="m2"><p>هر که حرص مال دارد موش دشت محشر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چاکر دو نان مشو از بهر یک لب نان که تو</p></div>
<div class="m2"><p>غافلی و رزق تو بر تو ز تو عاشق تر است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مهر زر از سینه بیرون کن که صندوق لحد</p></div>
<div class="m2"><p>جای ذکر و طاعت است آنجا نه مأوای زر است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فکر مالت برده خواب از دیده شبهای دراز</p></div>
<div class="m2"><p>یاد مردن کن که مالت وارثان را درخور است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مال تو مار است و عقرب چند ورزی مهر او؟</p></div>
<div class="m2"><p>هیچ کس دیدی که در دنیا محب اژدر است؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مطلع دیگر شنو کز استماعش گوش خلق</p></div>
<div class="m2"><p>عبرتی گیرد، هر آن گوشی که در وی گوهر است</p></div></div>