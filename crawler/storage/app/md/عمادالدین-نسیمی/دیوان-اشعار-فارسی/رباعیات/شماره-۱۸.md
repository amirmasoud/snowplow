---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>الله ز حال بندگان آگاه است</p></div>
<div class="m2"><p>پیغمبر مات رهنمای راه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیزارم از آن دین که در او منسوخ است</p></div>
<div class="m2"><p>دین دین محمد رسول الله است</p></div></div>