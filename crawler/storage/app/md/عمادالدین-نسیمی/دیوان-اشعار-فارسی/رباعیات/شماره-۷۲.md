---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>یاقوت لبا! لعل بدخشانی کو؟</p></div>
<div class="m2"><p>وان راحت روح و راح ریحانی کو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند حرام در مسلمانی شد</p></div>
<div class="m2"><p>تو می خور و غم مخور، مسلمانی کو؟</p></div></div>