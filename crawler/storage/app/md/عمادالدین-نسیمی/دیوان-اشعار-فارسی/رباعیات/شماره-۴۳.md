---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>آن سی و دو خط وجه آدم چو ندید</p></div>
<div class="m2"><p>شیطان، به خلاف حق سر از سجده کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کس که به حق بدید امام ملکوت</p></div>
<div class="m2"><p>از قوت حرف و صوت او گفت و شنید</p></div></div>