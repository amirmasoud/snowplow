---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>تا می نشنوی ز خویشتن واحد و فرد</p></div>
<div class="m2"><p>تحصیل مراد خویش نتوانی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آزاد شو از جهان و پاک از همه گرد</p></div>
<div class="m2"><p>خود را زن ره مساز اگر هستی مرد</p></div></div>