---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>خورشید ازل بتافت از روزن تن</p></div>
<div class="m2"><p>تا چهره خود ببیند اندر روزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوید که چو روزن از میان برخیزد</p></div>
<div class="m2"><p>«من باشم و من باشم و من باشم و من »</p></div></div>