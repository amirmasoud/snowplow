---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>رفتم به کنشت گبر و ترسا و یهود</p></div>
<div class="m2"><p>زیرا که عبادتگه رهبان تو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سنگ و کلوخ و در و دیوار کنشت</p></div>
<div class="m2"><p>جز زمزمه ذکر تو گوشم نشنود</p></div></div>