---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>آنم که جهان چو حلقه در مشت من است</p></div>
<div class="m2"><p>وین قوت حق ز پشتی پشت من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کونین و مکان و هر چه در عالم هست</p></div>
<div class="m2"><p>در قبضه قدرت یک انگشت من است</p></div></div>