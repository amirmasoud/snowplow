---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>من نور تو در جبه ازرق دیدم</p></div>
<div class="m2"><p>وز نور تو چون نور تو مطلق دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون روی ز غیر حق بگردانیدم</p></div>
<div class="m2"><p>سر تا به قدم وجود خود حق دیدم</p></div></div>