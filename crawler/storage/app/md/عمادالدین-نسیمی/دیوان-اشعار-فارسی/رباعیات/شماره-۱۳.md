---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>طوف سر کوی یار طاعات من است</p></div>
<div class="m2"><p>اوصاف جمال او مناجات من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در من نگرد کسی که او را طلبد</p></div>
<div class="m2"><p>کایینه ذات و صفتش ذات من است</p></div></div>