---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>چون هستی ما ز کاف و نون پیدا شد</p></div>
<div class="m2"><p>ماهیت کاف و نون عین ما شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او را چو مظاهر صفات اشیا بود</p></div>
<div class="m2"><p>اشیا همه او و او همه اشیا شد</p></div></div>