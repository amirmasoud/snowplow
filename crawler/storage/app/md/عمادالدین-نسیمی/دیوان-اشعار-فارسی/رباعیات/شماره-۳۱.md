---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>حرفی است حقیقتی که ذاتش خوانند</p></div>
<div class="m2"><p>ترکیب و کلام و هم صفاتش خوانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنان که چو خضر یافتند آب حیات</p></div>
<div class="m2"><p>آن ذات و صفات را حیاتش خوانند</p></div></div>