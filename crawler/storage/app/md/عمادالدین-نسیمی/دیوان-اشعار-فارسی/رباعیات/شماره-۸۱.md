---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>واقف به خدا ز بی وقوفی نشوی</p></div>
<div class="m2"><p>عارف به حق از طریق صوفی نشوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسرار خدا بر تو نگردد روشن</p></div>
<div class="m2"><p>تا در ره جستجو حروفی نشوی</p></div></div>