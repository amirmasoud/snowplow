---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>ای آن که تو تن را به سفالی ننهی</p></div>
<div class="m2"><p>لب بر لب آن آب زلالی ننهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر وحی که آید به تو، از من آید</p></div>
<div class="m2"><p>زنهار، به دل وهم و خیالی ننهی</p></div></div>