---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>در قاعده شریعت آگاه نبی است</p></div>
<div class="m2"><p>در مرتبه حقیقت الله نبی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تخت وجود آسمان توحید</p></div>
<div class="m2"><p>هم شاه ولی آمد و هم ماه نبی است</p></div></div>