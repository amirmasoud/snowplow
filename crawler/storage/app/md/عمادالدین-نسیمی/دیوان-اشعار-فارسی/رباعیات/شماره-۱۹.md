---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>هر وصف که در شان کلام الله است</p></div>
<div class="m2"><p>خاصیت او تمام بسم الله است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نقطه که آن در بی بسم الله است</p></div>
<div class="m2"><p>آن خال رخ علی ولی الله است</p></div></div>