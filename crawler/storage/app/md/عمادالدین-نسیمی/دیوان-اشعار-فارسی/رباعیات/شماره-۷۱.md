---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>ای آیت نور، پرتو غبغب تو</p></div>
<div class="m2"><p>تفسیر دخان دو گیسوی چون شب تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشیاست مرکب شده از سی و دو حرف</p></div>
<div class="m2"><p>وان سی و دو حرف نیست الا لب تو</p></div></div>