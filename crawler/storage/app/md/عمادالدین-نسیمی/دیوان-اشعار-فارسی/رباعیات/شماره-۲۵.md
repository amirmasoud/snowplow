---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>آن علم که از ذات خدا منفک نیست</p></div>
<div class="m2"><p>در هستی او هیچ نبی را شک نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطی است خدا به وجه آدم بنوشت</p></div>
<div class="m2"><p>وان سی و دو خط لوح حرفی حک نیست</p></div></div>