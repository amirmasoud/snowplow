---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>آن نقطه که مرکز جهان است تویی</p></div>
<div class="m2"><p>وان قطره که اصل کن فکان است تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن حرف که از اسم بیان است تویی</p></div>
<div class="m2"><p>وان اسم که از ذات نشان است تویی</p></div></div>