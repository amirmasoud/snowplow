---
title: >-
    بخش ۴۳ - غزل
---
# بخش ۴۳ - غزل

<div class="b" id="bn1"><div class="m1"><p>همان سنگین دل نامهربانم</p></div>
<div class="m2"><p>که در شوخی به عالم داستانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل من مهر او جوید که خواهم</p></div>
<div class="m2"><p>لبم احوال او گوید که دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر خواهم که جان به خشم توان زود</p></div>
<div class="m2"><p>و گر خواهم که دل دزدم توانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا با من چه کار؟ ار دل فریبم</p></div>
<div class="m2"><p>ترا از من چه سود؟ ار مهربانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل و جان گر بمن بخشند شاید</p></div>
<div class="m2"><p>که دل را چون تن و تن را چو جانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا بد مهر می‌خوانی و اینم</p></div>
<div class="m2"><p>مرا دلسوز می‌دانی و آنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر جان می‌نهی در آستینم</p></div>
<div class="m2"><p>و گر سر میزنی بر آستانم</p></div></div>