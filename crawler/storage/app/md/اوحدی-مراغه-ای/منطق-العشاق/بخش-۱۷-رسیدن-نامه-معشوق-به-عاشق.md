---
title: >-
    بخش ۱۷ - رسیدن نامهٔ معشوق به عاشق
---
# بخش ۱۷ - رسیدن نامهٔ معشوق به عاشق

<div class="b" id="bn1"><div class="m1"><p>چو بشنید این حدیث از هوش رفته</p></div>
<div class="m2"><p>بیفتاد این سخن در گوش رفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلش با آن گران پاسخ دژم بود</p></div>
<div class="m2"><p>هنوز اندر وفا ثابت قدم بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی دانست کان خواری به دل نیست</p></div>
<div class="m2"><p>ز معشوقان دل آزاری به دل نیست</p></div></div>