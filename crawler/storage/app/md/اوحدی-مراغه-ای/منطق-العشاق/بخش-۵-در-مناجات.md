---
title: >-
    بخش ۵ - در مناجات
---
# بخش ۵ - در مناجات

<div class="b" id="bn1"><div class="m1"><p>ازین گفتن، خدایا، شرم دارم</p></div>
<div class="m2"><p>و زان حضرت به غایت شرمسارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فیض خود دلم پر نور گردان</p></div>
<div class="m2"><p>زبانم را ز باطل دور گردان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ضمیرم را ز معنی بهره ور کن</p></div>
<div class="m2"><p>خیال فاسد از طبعم بدر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا توفیق نیکو بندگی ده</p></div>
<div class="m2"><p>دلم را زنده دار و زندگی ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خود رایی تبه شد کار ما را</p></div>
<div class="m2"><p>خداوندا، به خود مگذار ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گناه هر که در عالم بیامرز</p></div>
<div class="m2"><p>و زان پس اوحدی را هم بیامرز</p></div></div>