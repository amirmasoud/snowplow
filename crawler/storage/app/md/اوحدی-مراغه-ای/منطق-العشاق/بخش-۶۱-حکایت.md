---
title: >-
    بخش ۶۱ - حکایت
---
# بخش ۶۱ - حکایت

<div class="b" id="bn1"><div class="m1"><p>به خر گفتند: کیمخت از چه بستی؟</p></div>
<div class="m2"><p>بگفت: از زخم سیخ و چوب دستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو من در خاک خاموشی نشستم</p></div>
<div class="m2"><p>زدندم چوب، تا کیمخت بستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشان دانش اندر قیل و قالست</p></div>
<div class="m2"><p>هران کس را که نطقی نیست لالست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به قدر راستی گیرد سخن سنگ</p></div>
<div class="m2"><p>سخن کز راستی بگذشت، شد لنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن گر بد بود بنیاد جنگست</p></div>
<div class="m2"><p>چو نیک آید نشان هوش و هنگست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن نوباوهٔ بستان روحست</p></div>
<div class="m2"><p>سخن مفتاح ابواب فتوحست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن کشاف اسرار نهانیست</p></div>
<div class="m2"><p>مکن منع این سخن را، کاسما نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن کز روی دانش باشد و هوش</p></div>
<div class="m2"><p>کنند او را چو مروارید در گوش</p></div></div>