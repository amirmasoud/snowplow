---
title: >-
    بخش ۳۶ - غزل
---
# بخش ۳۶ - غزل

<div class="b" id="bn1"><div class="m1"><p>دل از ما بر گرفتی، یاد می‌دار</p></div>
<div class="m2"><p>جفا از سر گرفتی، یاد می‌دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دست من ندادی زلف و بامن</p></div>
<div class="m2"><p>به مویی در گرفتی، یاد می‌دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دستم تنگ دیدی، چون دهانت</p></div>
<div class="m2"><p>کسی دیگر گرفتی، یاد می‌دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا درویش دیدی، رفتی از غم</p></div>
<div class="m2"><p>رخم در زر گرفتی، یاد می‌دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل من ریش کردی، دیگری را</p></div>
<div class="m2"><p>چو جان در بر گرفتی، یاد می دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا چون حلقه بر در دیدی، اکنون</p></div>
<div class="m2"><p>به ترک در گرفتی، یاد می‌دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفتی دست یکسر دوستان را</p></div>
<div class="m2"><p>مرا کمتر گرفتی، یاد می‌دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دیدی در سر من سوز مهرت</p></div>
<div class="m2"><p>ز کین خنجر گرفتی، یاد می‌دار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو سر گردان بدیدی اوحدی را</p></div>
<div class="m2"><p>زبانش بر گرفتی، یاد می‌دار</p></div></div>