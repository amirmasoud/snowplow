---
title: >-
    بخش ۱۹ - حکایت
---
# بخش ۱۹ - حکایت

<div class="b" id="bn1"><div class="m1"><p>خبر دادند مجنون را که: لیلی</p></div>
<div class="m2"><p>ندارد با تو پیوندی و میلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدیشان گفت: اگر معشوق جافیست</p></div>
<div class="m2"><p>وفای عاشق بیچاره کافیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو نیز، ار طالب آن یار نغزی</p></div>
<div class="m2"><p>قدم را راست می‌نه، تا نلغزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر زخمی ز یاری سرمیپچان</p></div>
<div class="m2"><p>عنان از دوستداری برمپیچان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طریق عشق سستی بر نتابد</p></div>
<div class="m2"><p>محبت جز درستی بر نتابد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اول آزمایش باشد آنجا</p></div>
<div class="m2"><p>چو بگریزی، گشایش باشد آنجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر خواهی که او غم خوارت افتد</p></div>
<div class="m2"><p>تحمل کن، کزین بسیارت افتد</p></div></div>