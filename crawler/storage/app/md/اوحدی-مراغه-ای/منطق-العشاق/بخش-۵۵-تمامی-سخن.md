---
title: >-
    بخش ۵۵ - تمامی سخن
---
# بخش ۵۵ - تمامی سخن

<div class="b" id="bn1"><div class="m1"><p>پری، با آنکه واقف می‌شد از دوست</p></div>
<div class="m2"><p>در آن معنی که حق با جانب اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر ره تازه زهری بر شکر زد</p></div>
<div class="m2"><p>حروف مهر و کین بر یک دگر زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوشت این نامه و فرمود تا زود</p></div>
<div class="m2"><p>بدو بردند، نظرم نامه این بود</p></div></div>