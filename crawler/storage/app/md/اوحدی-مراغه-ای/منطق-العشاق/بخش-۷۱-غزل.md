---
title: >-
    بخش ۷۱ - غزل
---
# بخش ۷۱ - غزل

<div class="b" id="bn1"><div class="m1"><p>که روز غم بسر خواهد شد آخر</p></div>
<div class="m2"><p>سخن نوعی دگر خواهد شد آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهال آرزو در سینه و دل</p></div>
<div class="m2"><p>به شادی بارور خواهد شد آخر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو زر بود از جفا روی تو اول</p></div>
<div class="m2"><p>ولی کارت چو زر خواهد شد آخر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تایید سعادت اختر مهر</p></div>
<div class="m2"><p>ز برج غم بدر خواهد شد آخر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخواهم داد کام دوستان را</p></div>
<div class="m2"><p>حکایت مختصر خواهد شد آخر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهان عاشق از لوزینهٔ وصل</p></div>
<div class="m2"><p>پر از شهد و شکر خواهد شد آخر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز مهر اوحدی بر روی آن ماه</p></div>
<div class="m2"><p>جهانی را خبر خواهد شد آخر</p></div></div>