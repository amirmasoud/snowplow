---
title: >-
    بخش ۳۹ - خلاصهٔ سخن
---
# بخش ۳۹ - خلاصهٔ سخن

<div class="b" id="bn1"><div class="m1"><p>چرا بر زورمندی تند گردی؟</p></div>
<div class="m2"><p>که گر تندی نماید کند گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو سنگ از آب هر سیلی چه رنجی؟</p></div>
<div class="m2"><p>اگر مجنونی از لیلی چه رنجی؟</p></div></div>