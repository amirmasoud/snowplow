---
title: >-
    بخش ۵۴ - حکایت
---
# بخش ۵۴ - حکایت

<div class="b" id="bn1"><div class="m1"><p>به گل گفتند: بلبل بس حقیرست</p></div>
<div class="m2"><p>ترا با او چرا این دارو گیرست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفتا: بلبلی کز من زند لاف</p></div>
<div class="m2"><p>بر من به ز ده سیمرغ در قاف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل صافی ترا از لشکری به</p></div>
<div class="m2"><p>درون بی‌نفاق از کشوری به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر، کز راستی آید، بلندست</p></div>
<div class="m2"><p>برون از راستی خود ناپسندست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چالاکی نظر جوی از بلندان</p></div>
<div class="m2"><p>ولی پرهیز کن از چشم بندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پاکی دیده‌ای کو باز باشد</p></div>
<div class="m2"><p>به صید دل کمند انداز باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازو چون سر کشی، از پا نیفتی</p></div>
<div class="m2"><p>میفگن بر زمینش، تا نیفتی</p></div></div>