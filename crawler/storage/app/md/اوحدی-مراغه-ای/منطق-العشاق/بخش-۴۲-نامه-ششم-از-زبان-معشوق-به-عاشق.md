---
title: >-
    بخش ۴۲ - نامه ششم از زبان معشوق به عاشق
---
# بخش ۴۲ - نامه ششم از زبان معشوق به عاشق

<div class="b" id="bn1"><div class="m1"><p>اگر صد چون تومیرد غم ندارم</p></div>
<div class="m2"><p>که سر گردان و عاشق کم ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم سنگست، نرمش چون توان کرد؟</p></div>
<div class="m2"><p>به آه سرد گرمش چون توان کرد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شوخی شیر گیرد چشم مستم</p></div>
<div class="m2"><p>به آهو نافه بخشد زلف پستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو از تنگ دهانم قند ریزد</p></div>
<div class="m2"><p>ز تنگ شکر مصری چه خیزد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر صد بوسه لعلم پیشکش کرد</p></div>
<div class="m2"><p>ز مال خویشتن بخشید،خوش کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا بر من که داد این پادشاهی؟</p></div>
<div class="m2"><p>که از لعلم حساب خرج خواهی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو من در ملک خوبی پادشاهم</p></div>
<div class="m2"><p>ز لب شکر بدان بخشم که خواهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا با روی و زلف من چه کارست؟</p></div>
<div class="m2"><p>که این چون گنج شد یا آن چو مارست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برای آن همی دادی غرورم</p></div>
<div class="m2"><p>که بر بندی به هر نزدیک و دورم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا از بهر این می‌خواستی تو؟</p></div>
<div class="m2"><p>خریدار شگرفی راستی تو!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هر جرمی میور در گناهم</p></div>
<div class="m2"><p>که گر شهری بسوزم پادشاهم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نسازد پادشاهان را غلامی</p></div>
<div class="m2"><p>تو می‌سوز اندرین سودا، که خامی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برون آور، ترا گر حجتی هست</p></div>
<div class="m2"><p>که نتوان با تو دل در دیگری بست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من آن آهووش صحرا نوردم</p></div>
<div class="m2"><p>که خود را بستهٔ دامی نکردم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلم هر لحظه جایی انس گیرد</p></div>
<div class="m2"><p>به یک جا چون نشیند تا بمیرد؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گهی گل چینم و گه خار گیرم</p></div>
<div class="m2"><p>هر آن کس را که خواهم یار گیرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی را بر لب خود میر سازم</p></div>
<div class="m2"><p>یکی را آهنین زنجیر سازم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دل مردم بسوزم تا توانم</p></div>
<div class="m2"><p>ولی هرگز پشیمانی ندانم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز روبه بازی زلفم حذر کن</p></div>
<div class="m2"><p>سر خود گیر و با او سربسر کن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سرم سودای او ورزد که خواهد</p></div>
<div class="m2"><p>دلم از بهر آن لرزد که خواهد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همی گویی: ترا چون موی شد تن</p></div>
<div class="m2"><p>تو خود بس ناتوان گشتی، ولی من</p></div></div>