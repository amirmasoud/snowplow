---
title: >-
    بخش ۱۱ - خلاصهٔ سخن
---
# بخش ۱۱ - خلاصهٔ سخن

<div class="b" id="bn1"><div class="m1"><p>کسی کو آزمود، آنگاه پیوست</p></div>
<div class="m2"><p>نباید بعد از آن خاییدنش دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو پیوندی و آنگاه آزمایی</p></div>
<div class="m2"><p>ز حیرت دست خود بسیار خایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل عاشق سکونت پیشه باید</p></div>
<div class="m2"><p>عزیمت را نخست اندیشه باید</p></div></div>