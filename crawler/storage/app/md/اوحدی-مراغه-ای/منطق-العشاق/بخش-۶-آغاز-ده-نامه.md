---
title: >-
    بخش ۶ - آغاز ده نامه
---
# بخش ۶ - آغاز ده نامه

<div class="b" id="bn1"><div class="m1"><p>شنیدم کز هوسناکان جوانی</p></div>
<div class="m2"><p>به ناگه فتنه شد بر دلستانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخش زرد و تنش باریک میشد</p></div>
<div class="m2"><p>جهان بر چشم او تاریک میشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبی بیدار بود، از عشق نالان</p></div>
<div class="m2"><p>پریشان گشته چون آشفته حالان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلش را آتش سودا برآشفت</p></div>
<div class="m2"><p>چو آتش تیزتر شد باد را گفت:</p></div></div>