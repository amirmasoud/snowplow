---
title: >-
    بخش ۴۵ - شنیدن عاشق سخن معشوق را
---
# بخش ۴۵ - شنیدن عاشق سخن معشوق را

<div class="b" id="bn1"><div class="m1"><p>به زودی قاصدی این نامه چون باد</p></div>
<div class="m2"><p>بیاورد و بدان آشفته دل داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو عاشق دید کار خویش مشکل</p></div>
<div class="m2"><p>به زاری با دل خود گفت: کای دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشو در بند او کز مهر دورست</p></div>
<div class="m2"><p>نمی‌خواهد ترا، آخر نه زورست</p></div></div>