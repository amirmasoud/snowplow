---
title: >-
    بخش ۱ - سر آغاز
---
# بخش ۱ - سر آغاز

<div class="b" id="bn1"><div class="m1"><p>به نام آنکه ما را نام بخشید</p></div>
<div class="m2"><p>زبان را در فصاحت کام بخشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نور خود بر افروزندهٔ دل</p></div>
<div class="m2"><p>به نار بیدلی سوزندهٔ دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر هر نامه‌ای از نام او خوش</p></div>
<div class="m2"><p>جهان جان ز عکس جام او هوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درود از ما، سلام از حضرت او</p></div>
<div class="m2"><p>دمادم بر رسول و عترت او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابوالقاسم، که شد عالم طفیلش</p></div>
<div class="m2"><p>فلک دهلیز چاوشان خیلش</p></div></div>