---
title: >-
    بخش ۳۲ - خلاصهٔ سخن
---
# بخش ۳۲ - خلاصهٔ سخن

<div class="b" id="bn1"><div class="m1"><p>از آن دلدار هر جایی چه خیزد؟</p></div>
<div class="m2"><p>که او هر ساعت از جایی گریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو صورت هست معنی نیز باید</p></div>
<div class="m2"><p>برون از حسن خیلی چیز باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه هر گوهر که بینی شب چراغست</p></div>
<div class="m2"><p>نباشد گل به هر وادی که راغست</p></div></div>