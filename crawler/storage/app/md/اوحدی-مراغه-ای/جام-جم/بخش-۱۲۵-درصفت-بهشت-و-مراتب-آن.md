---
title: >-
    بخش ۱۲۵ - درصفت بهشت و مراتب آن
---
# بخش ۱۲۵ - درصفت بهشت و مراتب آن

<div class="b" id="bn1"><div class="m1"><p>چون بمیری ازین جواهر خمس</p></div>
<div class="m2"><p>عقل و نفست نپاید اندر رمس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در این نه مقوله بسته شود</p></div>
<div class="m2"><p>دل ازین چار قید رسته شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برهی از سه بعد و از شش حد</p></div>
<div class="m2"><p>اوحدی‌وش رخ آوری به احد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این تخیل نماند و احساس</p></div>
<div class="m2"><p>وین تکاپوی منهیان حواس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدهٔ روح بی‌سبل گردد</p></div>
<div class="m2"><p>مشکل نفس جمله حل گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه خواهی میسرت باشد</p></div>
<div class="m2"><p>وآنچه جویی برابرت باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جهانی رسی سراسر جان</p></div>
<div class="m2"><p>وندرو کاردار عقل و روان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لبشان بی‌زبان سخن پیوند</p></div>
<div class="m2"><p>چهره بی‌عشوه شاهد و دلبند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه یکرنگ و هیچ رنگی نه</p></div>
<div class="m2"><p>همه صلح و هراس جنگی نه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جامها پر ز شهد و شیر وشراب</p></div>
<div class="m2"><p>باغها پردرخت و میوه و آب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باغ مینو گشاده در درهم</p></div>
<div class="m2"><p>شاخ مینا کشیده سر در هم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شربت آینده نزد رنجوران</p></div>
<div class="m2"><p>میوه ریزنده بر سر دوران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرچه جان کشته پیش دل رسته</p></div>
<div class="m2"><p>چشم جان دیده هرچه دل جسته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دور نزدیک و سخت نرم شده</p></div>
<div class="m2"><p>زشت زیبا و سرد گرم شده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه از مردن و هلاک ایمن</p></div>
<div class="m2"><p>دل و جانها ز ترس و باک ایمن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه ز اندوه رخ بریزد رنگ</p></div>
<div class="m2"><p>نه ز انبوه خانه گردد تنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فارغ از رنج ناملایم و ضد</p></div>
<div class="m2"><p>ایمن از ازدحام دشمن وند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر سر دوشها تراز بقا</p></div>
<div class="m2"><p>در کف هوشها جواز لقا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر بساط بقا چو دلبندان</p></div>
<div class="m2"><p>وز نشاط لقا چو گل خندان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باغهایی به دست خود کشته</p></div>
<div class="m2"><p>بر زمینی ز عنبر آغشته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گه شراب بقا چشانندش</p></div>
<div class="m2"><p>گه به باع لقا کشانندش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گه کند در جمال حور نظر</p></div>
<div class="m2"><p>گه ز کوثر کنندش آبشخور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملکش در نوازش آرد و ناز</p></div>
<div class="m2"><p>میکند در جهان جان پرواز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حلم او انگبین ناب شود</p></div>
<div class="m2"><p>علم گه شیر و گه شراب شود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حله پوشد، که سترپوشی کرد</p></div>
<div class="m2"><p>باده نوشد، که خشم نوشی کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پیشش آرند میوه‌های بهشت</p></div>
<div class="m2"><p>از درخت عمل که اینجا کشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تیر انصاف در کمان آرند</p></div>
<div class="m2"><p>جان به شکرانه در میان آرند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رنج‌بینان به راحتی برسند</p></div>
<div class="m2"><p>ره‌نشینان به ساحتی برسند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون شوی دور ازین سرای هوس</p></div>
<div class="m2"><p>با تو همراه علم باشد بس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عملت میبرد علم در پیش</p></div>
<div class="m2"><p>علم خود را جدا مدار از خویش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر طلب میکنی بهشت بقا</p></div>
<div class="m2"><p>نزنی جز در بهشت لقا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در بهشت خدا علف نبود</p></div>
<div class="m2"><p>هرچه خواهد شدن تلف نبود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وآنچه از خوردنیست نام او را</p></div>
<div class="m2"><p>گرچه باشد، مشو غلام او را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بادهٔ او رحیق مختومست</p></div>
<div class="m2"><p>ختمش از مشک او نه از مومست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شیر علمست و باده معرفتش</p></div>
<div class="m2"><p>شهد شیرین تعقل صفتش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در زمین شیر و انگبین گویی</p></div>
<div class="m2"><p>چون روی بر فلک همین گویی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو کزین گونه غره‌باشی و غرق</p></div>
<div class="m2"><p>ز آسمان تا زمین برتوچه فرق؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رو به دیدار روح دل خوش کن</p></div>
<div class="m2"><p>گندم و میوه را برآتش کن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در بهشتی که سفرهٔ نانست</p></div>
<div class="m2"><p>پی‌منه، کان بهشت دونانست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گرتو از بهر باغ در کاری</p></div>
<div class="m2"><p>در ده این باغ‌ها بسی داری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بی عمل در بهشت رفت آدم</p></div>
<div class="m2"><p>آدمی بی‌عمل درآید هم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>باغ دیدار جوی و آب لقا</p></div>
<div class="m2"><p>باغ انگور و میوه را چه بقا؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>میزبان را چو با تو میل بود</p></div>
<div class="m2"><p>خوردن میوه خود طفیل بود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جای خود در بهشت باقی کن</p></div>
<div class="m2"><p>رخ در آن بزمگاه ساقی کن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دست جز بر در قبول مکش</p></div>
<div class="m2"><p>داس در گندم فضول مکش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آدمت را که خواب جهل بود</p></div>
<div class="m2"><p>امر« لاتقرابا» ش سهل نمود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر بدان نکته دست رد نزدی</p></div>
<div class="m2"><p>در ره «اهبطو» ش حد نزدی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چه دهی دل بدین شمامهٔ شوم؟</p></div>
<div class="m2"><p>دست کش سوی میوه معلوم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کار حوا به جز هوا نبود</p></div>
<div class="m2"><p>ز آدم این بیخودی روا نبود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آن بهشتی که اندرو علفست</p></div>
<div class="m2"><p>لایق مدخلان ناخلفست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اندر آن عالم این ستمها نیست</p></div>
<div class="m2"><p>وین بد و نیک و بیش و کمها نیست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>فارغست از تزاحم و تنگی</p></div>
<div class="m2"><p>نیست رنگی بغیر یکرنگی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عالم وحدتست عالم نور</p></div>
<div class="m2"><p>عالم کثرت این سرای غرور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جای شخص مجرد روحی</p></div>
<div class="m2"><p>نبود جز بهشت سبوحی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>برتفاوت بود مراتب خلد</p></div>
<div class="m2"><p>دور از اندازه نیست راتب خلد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هشت جنت ز بهر این آمد</p></div>
<div class="m2"><p>از حکیمان بما چنین آمد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هر یکی را ز ما بهشتی هست</p></div>
<div class="m2"><p>قصر و ایوان و آب و کشتی هست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تو ببین نیک تا چه کاشته‌ای؟</p></div>
<div class="m2"><p>چه به روز پسین گذاشته‌ای؟</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نکنی رخ به خانه‌های بهشت</p></div>
<div class="m2"><p>گرنه از زر بود بنا را خشت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زر فرستی برای خشت زنان</p></div>
<div class="m2"><p>چند ازین زر؟ زهی سرشت زنان!</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نه به اخلاص میکنی کاری</p></div>
<div class="m2"><p>زان درختت نمیدهد باری</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تو که در بند قلیه و نانی</p></div>
<div class="m2"><p>کی رسی در بهشت رحمانی؟</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خوردن اینجا روا نمیدارند</p></div>
<div class="m2"><p>در بهشت آش و سفره چون آرند؟</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>در بهشت ار خوری جو و گندم</p></div>
<div class="m2"><p>همچو آدم کنی ره خود گم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ریستن گیردت ز خوردن زشت</p></div>
<div class="m2"><p>به درت باید آمدن ز بهشت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>عاقلان مردن از اجل گیرند</p></div>
<div class="m2"><p>عاشقان پیش ازین اجل میرند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بی‌گناهی بپوی مردانه</p></div>
<div class="m2"><p>که گنه‌کار ترسد از خانه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مرگ نیکان حیات جان باشد</p></div>
<div class="m2"><p>مرگ بر بدکنش زیان باشد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گر بترسد ز مرگ بدکاره</p></div>
<div class="m2"><p>نتوان کرد عیب بیچاره</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>دل او میدهد گواهی راست</p></div>
<div class="m2"><p>که اجل داد او بخواهد خواست</p></div></div>