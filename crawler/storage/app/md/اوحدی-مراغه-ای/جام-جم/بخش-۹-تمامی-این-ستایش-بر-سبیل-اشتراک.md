---
title: >-
    بخش ۹ - تمامی این ستایش بر سبیل اشتراک
---
# بخش ۹ - تمامی این ستایش بر سبیل اشتراک

<div class="b" id="bn1"><div class="m1"><p>خسروی طاهر و وزیری پاک</p></div>
<div class="m2"><p>هر دو در دین مبارز و چالاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن فلک را کشیده اندر سلک</p></div>
<div class="m2"><p>وین جهان را نظام داده به کلک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن چو ماهست بر سپهر جلال</p></div>
<div class="m2"><p>وین چو مهرست در جهان کمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب دین از فروغ این شده روز</p></div>
<div class="m2"><p>دل کفر از شعاع آن پر سوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه این گفت او خلاف نکرد</p></div>
<div class="m2"><p>و آنچه این، او جز اعتراف نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن آن دل شده، دل این جان</p></div>
<div class="m2"><p>جان آن سال و مه بر جانان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهره در بزم آن کژ آهنگی</p></div>
<div class="m2"><p>ماه با عزم این کهن لنگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قول آن را به راستی پیوند</p></div>
<div class="m2"><p>عزم این مر مخالفان را بند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل ز تضعیف این به برگ و نوا</p></div>
<div class="m2"><p>حکم تالیف آن روان و روا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن به شاهی فلک گزید اورنگ</p></div>
<div class="m2"><p>وین بمیری ز ماه دارد ننگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن به بغداد عشق غارت کرد</p></div>
<div class="m2"><p>وین به تبریز دین عمارت کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تیغ این منهی رموز ظفر</p></div>
<div class="m2"><p>کلک او محرز کنوز قدر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر این با خدای و خلق درست</p></div>
<div class="m2"><p>سیر آن در رضای خالق چیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر زمان فکر آن به طرزی نو</p></div>
<div class="m2"><p>هر دمی بخت این و ارزی نو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دو جهانند هر تنی به هنر</p></div>
<div class="m2"><p>بل دو جانند در تنی مضمر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سخت نیکند، چشم بدشان دور</p></div>
<div class="m2"><p>باهم این پادشاه و این دستور</p></div></div>