---
title: >-
    بخش ۱۵ - در تخلص به اسم خواجه غیاث‌الدین
---
# بخش ۱۵ - در تخلص به اسم خواجه غیاث‌الدین

<div class="b" id="bn1"><div class="m1"><p>ای دل، از حکم زیجهای کهن</p></div>
<div class="m2"><p>طالع وقت را نگاهی کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نمودار راست، بی تخمین</p></div>
<div class="m2"><p>راز این طفل نورسیده ببین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که قوی حال یا زبون طرفست؟</p></div>
<div class="m2"><p>کوکبش در هبوط یا شرفست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جهان بر چه حال خواهد بود؟</p></div>
<div class="m2"><p>از چه چیزش وبال خواهد بود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به در آور ز سیر این اجرام</p></div>
<div class="m2"><p>سیر هیلاج و کدخدا و سهام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوکب او ز کوکب دستور</p></div>
<div class="m2"><p>بنگر نیک تا نباشد دور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بدانیم و دل برو بندیم</p></div>
<div class="m2"><p>به سخنهای عشق پیوندیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به چه میمانی؟ ای حدیقهٔ نور</p></div>
<div class="m2"><p>بس شگرفی، که چشم بد ز تو دور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نبات حسن برومندی</p></div>
<div class="m2"><p>همچو روی حسان همی خندی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناشکفته گلی نهشتی تو</p></div>
<div class="m2"><p>از شگفتی مگر بهشتی تو؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای فتوح دل سحر خیزم</p></div>
<div class="m2"><p>قرة العین خاطر تیزم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرع و اصل تو بار نامهٔ دین</p></div>
<div class="m2"><p>باب و فصلت تراز خامهٔ دین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از بهار تو تا تازه دل جانها</p></div>
<div class="m2"><p>وز نهار تو روشن ایمانها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز تو طبعم به دست شب خیزی</p></div>
<div class="m2"><p>کرده بر فرق عقل گلریزی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به زمین از سپهر پیغامی</p></div>
<div class="m2"><p>زین مباهات « جام جم» نامی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روشنی یافت عالم از نورت</p></div>
<div class="m2"><p>چون نبشتم به نام دستورت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خواجه یادم نکرد و چیزی هست</p></div>
<div class="m2"><p>که به مصر سخن عزیزی هست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حیف باشد چنین سخن سنجی</p></div>
<div class="m2"><p>بی‌نصیب آنگه از چنان گنجی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لطفش از هر کسی خبر یابست</p></div>
<div class="m2"><p>مگر از بخت من که در خوابست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از درختی بدان طربناکی</p></div>
<div class="m2"><p>چه کم از سایه‌ای بدین خاکی؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من فگندم سفینه را در یم</p></div>
<div class="m2"><p>گر بر او رسد ندارم غم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای مباهات من بایامت</p></div>
<div class="m2"><p>افتخار حدیثم از نامت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در جهان کس تویی، بگویم فاش</p></div>
<div class="m2"><p>منم آن هیچ کس، کس من باش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زان دل ابرساز دریا کن</p></div>
<div class="m2"><p>التفاتی به جانب ما کن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مایه داری و میتوان امروز</p></div>
<div class="m2"><p>غم پیران خور، ای جوان، امروز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نتوان کم چنین بیندازی</p></div>
<div class="m2"><p>که نه تبریزیم، نه شیرازی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گوشه دارم نه چون کمان چون تیر</p></div>
<div class="m2"><p>گوش دارم، که مستمندم و پیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هست بر موجب قبالهٔ من</p></div>
<div class="m2"><p>دو سه درویش درحبالهٔ من</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن تعلق چو پای بندم کرد</p></div>
<div class="m2"><p>حلق در حلقهٔ کمندم کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من از آن توام چو هستی اهل</p></div>
<div class="m2"><p>غم ایشان بخور، غم من سهل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از کرمشان چو خادمان بنواز</p></div>
<div class="m2"><p>یا مرا نیز خادم خودساز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لطف کن، در کشاکشم مگذار</p></div>
<div class="m2"><p>که چو خادم همی کشندم زار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خاک آن خادمان بی‌خایه</p></div>
<div class="m2"><p>به ازین خادمان بی‌مایه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فکرت من نهاد دیوانی</p></div>
<div class="m2"><p>که نخوردم ز حاصلش نانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یا رها کن چنین غریوانم</p></div>
<div class="m2"><p>یا به بیع اندر آر دیوانم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا تو باشی مصاحب دیوان</p></div>
<div class="m2"><p>که نشاید دو صاحب دیوان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تاکنون گر چه چرخ سفله نهاد</p></div>
<div class="m2"><p>هیچم آن دست بوس دست نداد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به خیالی ز دور ساخته‌ام</p></div>
<div class="m2"><p>هوسی غایبانه باخته‌ام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از دعایت نبوده‌ام خالی</p></div>
<div class="m2"><p>بگذرانم گواه آن حالی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پای رفتن نبود در دستم</p></div>
<div class="m2"><p>ورنه من بر گزاف ننشستم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بعد ازین چون قلم به سر کوشم</p></div>
<div class="m2"><p>جامهٔ کاغذین فروپوشم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>علم جامه جمله قصهٔ داد</p></div>
<div class="m2"><p>و اندرو کرده غصهٔ خود یاد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مگرم کاغذی شود روزی</p></div>
<div class="m2"><p>بر سر آن غیاث دین سوزی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>احدی کو دهد به هر کس کام</p></div>
<div class="m2"><p>اوحدی را به دست داد این جام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جامش از راه چون درست آمد</p></div>
<div class="m2"><p>گر چه دیر آمدست چست آمد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>او چو در پردهٔ طلسم کمال</p></div>
<div class="m2"><p>پیشت آورد کارنامهٔ حال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ره بگنجش ده، ار نرفت این بار</p></div>
<div class="m2"><p>بر سر گنج خویشتن چون مار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نفسی هم به کار من پرداز</p></div>
<div class="m2"><p>که چو کیخسروم نبینی باز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جام بستان، که میگریزم من</p></div>
<div class="m2"><p>زانکه سرمستم و بریزم من</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جاودانیست، من بگویم راست</p></div>
<div class="m2"><p>سخن، آنگه چنین سخن که مراست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دخترانند خوب و بالغ و بکر</p></div>
<div class="m2"><p>که به نه ماه زاده‌اند از فکر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نگشاید جزین سخن دل تنگ</p></div>
<div class="m2"><p>که بماند چو نقش بر دل سنگ</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نیست امروز، خواجه میداند</p></div>
<div class="m2"><p>هیچکس کین چنین سخن راند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>روزگارم بساز و کار ببین</p></div>
<div class="m2"><p>شیرگیرم کن و شکار ببین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جرعه‌ای زان کرم به کامم ریز</p></div>
<div class="m2"><p>بادهٔ جود خود به جامم ریز</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در دلیری، اگر چه گشتم گرم</p></div>
<div class="m2"><p>ورقم پر عرق شدست از شرم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گر چه شوخیست این و پیشانی</p></div>
<div class="m2"><p>تو بنه عذر این پریشانی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مگر این سروران که در پیشند</p></div>
<div class="m2"><p>چون ز فضل و هنر ز من بیشند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دور دارند ازین حروف انگشت</p></div>
<div class="m2"><p>نزنندم درفش خود بر مشت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در مصافات من سخن سنجم</p></div>
<div class="m2"><p>به مصافم مبر، که می‌رنجم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>با غم عشق خلوتی دارم</p></div>
<div class="m2"><p>وز بد و نیک سلوتی دارم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زان حضور آمد این نماز درست</p></div>
<div class="m2"><p>گو: مگرد این شکسته باز درست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از تو خالی مدار گنجم را</p></div>
<div class="m2"><p>تا ببویی مگر ترنجم را</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جام جمشید میبری زنهار!</p></div>
<div class="m2"><p>عدل جمشید کن به لیل و نهار</p></div></div>