---
title: >-
    بخش ۸۵ - در تلقین ذکر
---
# بخش ۸۵ - در تلقین ذکر

<div class="b" id="bn1"><div class="m1"><p>ذکر بیفکر علم بی‌عملست</p></div>
<div class="m2"><p>دل بیعشق چشم پر سبلست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلقهٔ ذکر حلقهٔ دل تست</p></div>
<div class="m2"><p>گلهٔ ما ز حلق پر گل تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذکر در دل چو جای کردو نشست</p></div>
<div class="m2"><p>بانگ خواهی بلند و خواهی پست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه نامش همیبری شنواست</p></div>
<div class="m2"><p>گر نداری فغان و نعره رواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وآنکه سر حروف می‌داند</p></div>
<div class="m2"><p>بی‌زبان و حروف میخواند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نتوانش سپاس، فکر آنست</p></div>
<div class="m2"><p>حاضرش میشناس، ذکر آنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لال گردی و گنگ ارین دانی</p></div>
<div class="m2"><p>ور ندانی، کرا همی خوانی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه او را نه آشنایی تو</p></div>
<div class="m2"><p>به کدامش زبان ستایی تو؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل نادان ز کار سست آید</p></div>
<div class="m2"><p>دم ز دانش زنی درست آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچ دانی که رویت اندر کیست؟</p></div>
<div class="m2"><p>چو ندانی خروش بیهده چیست؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل غایب به بانگ محتاجست</p></div>
<div class="m2"><p>که چو حاضر شود به معراجست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو دلت با زبان نشد هم عهد</p></div>
<div class="m2"><p>زشت باشد به ذکر کردن جهد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یار باید دل و زبان باهم</p></div>
<div class="m2"><p>تا توان زد ز نام پاکش دم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل چو پر نقش و رنگ باشد و بوی</p></div>
<div class="m2"><p>به زبان هر چه بایدت میگوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در دلت دار و گیر تاراجست</p></div>
<div class="m2"><p>زان به تلقین پیر محتاجست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیر داند که کیست لایق ذکر؟</p></div>
<div class="m2"><p>هر کسش چون ادا کند بی‌فکر؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه را گر به ذکر بنشانی</p></div>
<div class="m2"><p>نرهی هرگز از پیشمانی</p></div></div>