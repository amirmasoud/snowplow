---
title: >-
    بخش ۵۵ - در مذمت بخل و بخیلان
---
# بخش ۵۵ - در مذمت بخل و بخیلان

<div class="b" id="bn1"><div class="m1"><p>خوان اینان که خون دل پالود</p></div>
<div class="m2"><p>ندهد لقمه جز که زهر آلود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهر بر روی و زهر در کاسه</p></div>
<div class="m2"><p>چون نگیرد خوردنده را تا سه؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لقمه مستان ز دست لقمه شمار</p></div>
<div class="m2"><p>کز چنان لقمه داشت لقمان عار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاسهٔ پر پیاز دوغینه</p></div>
<div class="m2"><p>به ز صد منعم دروغینه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دستش ار شربت دگر دهدت</p></div>
<div class="m2"><p>دوغ او داغ بر جگر نهدت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوردن رزق خویش و منت خلق</p></div>
<div class="m2"><p>زهر خور، نان چه مینهی در حلق؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه بخشد ازین خسیسان دیگ</p></div>
<div class="m2"><p>روغنی بر کشیده دان از ریگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به باغ تو آفتی نرسد</p></div>
<div class="m2"><p>به کسی از تو رافتی نرسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون نظارگی بپالودی</p></div>
<div class="m2"><p>لبش از میوه‌ای نیالودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با چنین لطف چشم بد ز تو دور</p></div>
<div class="m2"><p>که بهشت آرزوت باشد و حور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر درختی بدین برومندی</p></div>
<div class="m2"><p>در باغ کرم چه می‌بندی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رو غریبانه سایه‌ای بر ساز</p></div>
<div class="m2"><p>یا بیفشان و حلقها ترساز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو سه سیب ار بما فرو دوسد</p></div>
<div class="m2"><p>به از آن کانچنان همی پوسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میوه چون هست، مایه‌ای برسان</p></div>
<div class="m2"><p>هم به همسایه سایه‌ای برسان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عنبت سرخ گشت و عنابی</p></div>
<div class="m2"><p>رخ چرا چون بنفشه میتابی؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خوشه‌ای چونکه در نکردی باز</p></div>
<div class="m2"><p>هم ز بالای در فرو انداز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون مجال کرامتی باشد</p></div>
<div class="m2"><p>بستن در غرامتی باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا بهارست میوه‌ای میده</p></div>
<div class="m2"><p>هم زکوتی به بیوه‌ای میده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جودکی خواند این صفت را دین؟</p></div>
<div class="m2"><p>بخل را نیز عار باشد ازین</p></div></div>