---
title: >-
    بخش ۳۴ - در نصیحت ملوک به عدل
---
# بخش ۳۴ - در نصیحت ملوک به عدل

<div class="b" id="bn1"><div class="m1"><p>ایکه بر تخت مملکت شاهی</p></div>
<div class="m2"><p>عدل کن، گر ز ایزد آگاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عدل چون گشت با خلافت یار</p></div>
<div class="m2"><p>نهلند از خلاف و ظلم آثار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عدل باید خلیفه را، پس حکم</p></div>
<div class="m2"><p>عدل نبود کجا کند کس حکم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عدل بی‌علم بیخ و بر نکند</p></div>
<div class="m2"><p>حکم بی‌عدل و علم اثر نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تخت را استواری از عدلست</p></div>
<div class="m2"><p>پادشه را سواری از عدلست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دود دلها به دادگر نرسد</p></div>
<div class="m2"><p>عادلان را به جان خطر نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پایداری به عدل و داد بود</p></div>
<div class="m2"><p>ظلم و شاهی چراغ و باد بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طاق کسری به داد ماند درست</p></div>
<div class="m2"><p>خانه سازی، به داد کوش نخست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عدل و عمر دراز هم زادند</p></div>
<div class="m2"><p>عاقلانم چنین خبر دادند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاه کو عدل و داد پیشه کند</p></div>
<div class="m2"><p>پادشاهیش بیخ و ریشه کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سایهٔ کردگار باشد شاه</p></div>
<div class="m2"><p>شاه عادل، نه شاه عادل کاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سایه آنرا بود که دارد تن</p></div>
<div class="m2"><p>تو بر آن نور رنگ سایه مزن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نور کلی ز سایه دور بود</p></div>
<div class="m2"><p>سایهٔ نور نیز نور بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خلق ازین سایه در پناه آیند</p></div>
<div class="m2"><p>مردم از فر او به راه آیند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاه خفته است فتنهٔ بیدار</p></div>
<div class="m2"><p>چشم دولت ز شاه خفته مدار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاه چون مستعد جنگ بود</p></div>
<div class="m2"><p>دشمنان را مجال تنگ بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جنگ دشمن به ساز باشد و مرد</p></div>
<div class="m2"><p>این دو پیشی به دست باید کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عدل باید طلایهٔ سپهت</p></div>
<div class="m2"><p>تا کند فتح را دلیل رهت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لشکر از عدل بر نشان وز داد</p></div>
<div class="m2"><p>تا کنندت به فتح و نصرت شاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بتو دادند ملک دست به دست</p></div>
<div class="m2"><p>مده این ملک را به غافل و مست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دشمنانت به هم چو رای زنند</p></div>
<div class="m2"><p>بر فتوح تو دست و پای زنند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر یکی را به گوشه‌ای انداز</p></div>
<div class="m2"><p>آنکه دفعش نمیتوان، بنواز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر قوی پنجه دست کین مگشای</p></div>
<div class="m2"><p>بر ضعیف و زبون کمین مگشای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کان یکی گر سگست گرگ شود</p></div>
<div class="m2"><p>وین به قصد تو سر بزرگ شود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فاش کن حیلت بد اندیشان</p></div>
<div class="m2"><p>تا نگویند غافلی زیشان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاه باید که دارد از سر هوش</p></div>
<div class="m2"><p>بر جهان چشم و بر رعیت گوش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شاه را گر به عدل دست رسست</p></div>
<div class="m2"><p>قاصد او یکی پیاده بسست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مال ده، گر چهار کس باشد</p></div>
<div class="m2"><p>یک سر تازیانه بس باشد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هیچ در وقت تندی و تیزی</p></div>
<div class="m2"><p>میل و رغبت مکن به خونریزی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خون ناحق مکن، چو یابی دست</p></div>
<div class="m2"><p>کز مکافات آن نشاید رست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر ز قرآن به دل رسیدت فیض</p></div>
<div class="m2"><p>یاد کن سر «کاظمین‌الغیظ»</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اختر و آسمان کمر بستند</p></div>
<div class="m2"><p>به چهار آخشیج پیوستند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا چنین صورتی هویدا شد</p></div>
<div class="m2"><p>وندران سر صنع پیدا شد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نسخهٔ حرز کردگارست این</p></div>
<div class="m2"><p>بس طلسمی بزرگوارست این</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر که بی‌موجبش خراب کند</p></div>
<div class="m2"><p>خویش را عرضهٔ عذاب کند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون نباشد ز شرع حکمی جزم</p></div>
<div class="m2"><p>ظلم باشد به کشتن کس عزم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ظلمت از ظلم دان و نور از عدل</p></div>
<div class="m2"><p>این بدان و مباش دور از عدل</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>روح خود را به عالم ارواح</p></div>
<div class="m2"><p>انس ده، تا رسی به روح و به راح</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون ملک با تو آشنایی یافت</p></div>
<div class="m2"><p>دلت از غیب روشنایی یافت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اینکه چون سایه سو بسو گردی</p></div>
<div class="m2"><p>سایه برخیزد و تو او گردی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قول و فعل و ضمیر چون شد راست</p></div>
<div class="m2"><p>اختلافی نماند اندر خواست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر چه خواهی تو ایزد آن خواهد</p></div>
<div class="m2"><p>وین مراد دلت به جان خواهد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آب خواهی تو، ابر آب کشد</p></div>
<div class="m2"><p>ایمنی، فتنه سر به خواب کشد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>با تو بیعت کنند جن و ملک</p></div>
<div class="m2"><p>سر به حکمت دهند چرخ و فلک</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نامت اسمی شود زداینده</p></div>
<div class="m2"><p>تن طلسمی جهان گشاینده</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سخنت را قضا قبول کند</p></div>
<div class="m2"><p>پیش تختت قدر نزول کند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دیدنت حشمت و جلال دهد</p></div>
<div class="m2"><p>التفات تو ملک و مال دهد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>آنکه دل در تو بست جان یابد</p></div>
<div class="m2"><p>وآنکه سودت برد زیان یابد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هر که قصد تو کرد خسته شود</p></div>
<div class="m2"><p>دشمنت خود به خود شکسته شود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فر کیخسروی ازینجا خاست</p></div>
<div class="m2"><p>که جهانرا به علم و عدل آراست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>روز خلوت گلیم پوشیدی</p></div>
<div class="m2"><p>به نماز و بهروزه کوشیدی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دست بستی، کمر بیفگندی</p></div>
<div class="m2"><p>تاج شاهی ز سر بیفگندی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>روی بر ریگ و دل چو دیگ به جوش</p></div>
<div class="m2"><p>دل سخن گستر و زبان خاموش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا بدیدی دلش به دیدهٔ راز</p></div>
<div class="m2"><p>دیدنیهای این نشیب و فراز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سر جام جهان نما اینست</p></div>
<div class="m2"><p>اثر قربت خدا اینست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>روشنانی که این خرد دارند</p></div>
<div class="m2"><p>جام جسم و ضمیر خود دارند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هر کرا این کمان و تیر بود</p></div>
<div class="m2"><p>روح صید و فرشته گیر بود</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خطبه اینست و سکه آن باشد</p></div>
<div class="m2"><p>که دو گیتی در آن میان باشد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عادلی، سایهٔ خدا باشی</p></div>
<div class="m2"><p>ورنه از سایه هم جدا باشی</p></div></div>