---
title: >-
    بخش ۲۶ - در وجود نوع انسان
---
# بخش ۲۶ - در وجود نوع انسان

<div class="b" id="bn1"><div class="m1"><p>امتزاج این دو روح را با هم</p></div>
<div class="m2"><p>چونکه در اعتدال شد محکم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس دانا بدان تعلق ساخت</p></div>
<div class="m2"><p>سایهٔ نور چون بدان انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوع انسان از آن میان برخاست</p></div>
<div class="m2"><p>شد به قامت ز استقامت راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن او شد به عقل و جان قایم</p></div>
<div class="m2"><p>تن تباهی ندید و جان دایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صاحب علم و صنعت و سخنست</p></div>
<div class="m2"><p>زانکه او را سه روح و یک بدنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و آنچه اصل وجود انسانست</p></div>
<div class="m2"><p>زبدهٔ این نبات و حیوانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آدمی زین دو چون خورش سازد</p></div>
<div class="m2"><p>مایهٔ نشو و پرورش سازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن غذا در بدن چو یابد نظم</p></div>
<div class="m2"><p>خون شود در تن از حرارت هضم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون برآید برین سخن چندی</p></div>
<div class="m2"><p>یابد آن خون ز روح پیوندی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شودش رنگ از اعتدال مزاج</p></div>
<div class="m2"><p>به سپیدی چو زیبق و چو زجاج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در چنین حال زرع خوانندش</p></div>
<div class="m2"><p>اصل این چند فرع دانندش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در زوایای پشت رست شود</p></div>
<div class="m2"><p>نسبتش با بدن درست شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اینچنین خوب گوهری ناسفت</p></div>
<div class="m2"><p>چون کند خفت خلوتی با جفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در نهد روی از آن حدایق غلب</p></div>
<div class="m2"><p>به دهان رحم ز مجری صلب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باز با آب زن در آمیزد</p></div>
<div class="m2"><p>زود اندر مشیمه شان ریزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هفت کوکب به کار او کوشند</p></div>
<div class="m2"><p>خلعت تربیت برو پوشند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به رحم شهر بند سازندش</p></div>
<div class="m2"><p>تا چو خون نژند سازندش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چرخ پیوندش استوار کند</p></div>
<div class="m2"><p>تا در آن جایگه قرار کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ماه اول زحل کند کارش</p></div>
<div class="m2"><p>اندران وقت کو بود یارش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گردد این خون در آن مشیمهٔ تنگ</p></div>
<div class="m2"><p>متغیر به شکل و صورت و رنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در هنر زمره‌ای که گام نهند</p></div>
<div class="m2"><p>بر چنین آب نطفه نام نهند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این زمان گر زحل قوی باشد</p></div>
<div class="m2"><p>طفل پردان و معنوی باشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر یکایک ستارگان زین هفت</p></div>
<div class="m2"><p>هر یکی زین قیاس حکمی رفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مشتری باشدش به ماه دوم</p></div>
<div class="m2"><p>مدد و یاور و پناه دوم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سرخ جامه شود بسان جگر</p></div>
<div class="m2"><p>باز گردد به رنگهای دگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>افتدش در مسام بادی گرم</p></div>
<div class="m2"><p>زان پدید آید اختلاجی نرم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حکمایی، که رسم وحد دانند</p></div>
<div class="m2"><p>اندرین حالتش ولد خوانند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر سوم ماهش آفتی نرسد</p></div>
<div class="m2"><p>یا گزند و مخافتی نرسد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یارمندی رسد ز بهرامش</p></div>
<div class="m2"><p>متصرف شود در اندامش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عضوهای رئیسه را در تن</p></div>
<div class="m2"><p>با دگر عضوها کند روشن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ولدی را که حالت این باشد</p></div>
<div class="m2"><p>نزد دانا لقب جنین باشد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ماه چارم به قوت خود مهر</p></div>
<div class="m2"><p>شودش نقش بند پیکر و چهر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تن او نغز و پرتوان گردد</p></div>
<div class="m2"><p>روحش اندر بدان روان گردد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در شکم خویش را بجنباند</p></div>
<div class="m2"><p>مرد داننده کودکش خواند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ماه پنجم بهزهره پردازد</p></div>
<div class="m2"><p>از سرش موی رستن آغازد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>منفصل گرددش رسوم از هم</p></div>
<div class="m2"><p>صورت چشم و گوش و بینی و فم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون به ماه ششم رساند کار</p></div>
<div class="m2"><p>شود از انجمش عطارد یار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در دهانش زبان گشاده شود</p></div>
<div class="m2"><p>داد ترکیب هاش داده شود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هفتم او را قمر نگاه کند</p></div>
<div class="m2"><p>رویش از روشنی چو ماه کند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اندرین ماه بی‌خلاف و گزند</p></div>
<div class="m2"><p>گر بزاید بماند این فرزند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هشتمین ماه باز ازین ایوان</p></div>
<div class="m2"><p>نوبت آید به کوکب کیوان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر ز مادر بزاید این هنگام</p></div>
<div class="m2"><p>هم شود کار زندگیش تمام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در نهم مشتریش باشد پشت</p></div>
<div class="m2"><p>اندران راه سهمناک درشت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سعدش این بند را کلید شود</p></div>
<div class="m2"><p>قوتی در ولد پدید شود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا بتدریج سرنگون کندش</p></div>
<div class="m2"><p>وز شکنجی چنان برون کندش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مدتی بوده اندران تنگی</p></div>
<div class="m2"><p>او سبک، لیک ازو شکم سنگی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>طفل در تنگ و مادر آهسته</p></div>
<div class="m2"><p>هر دو از بار یکدگر خسته</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دست بر روی، ارنج بر زانو</p></div>
<div class="m2"><p>رنجه از خفت و خیز کدبانو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>قوت آن خون و هیچ قوت نه</p></div>
<div class="m2"><p>خبر از بنیت و نبوت نه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چون برون آید از چنان بندی</p></div>
<div class="m2"><p>در دگر محنت اوفتد چندی</p></div></div>