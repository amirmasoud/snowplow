---
title: >-
    بخش ۳۵ - حکایت
---
# بخش ۳۵ - حکایت

<div class="b" id="bn1"><div class="m1"><p>رفت کسری ز خط شهر به دشت</p></div>
<div class="m2"><p>با سواران ز هر طرف میگشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلشنی دید تازه و خندان</p></div>
<div class="m2"><p>تر و نازک چو خط دلبندان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پر ز نارنج و نار باغی خوش</p></div>
<div class="m2"><p>زیر هر برگ آن چراغی خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت: کاب از کدام جویستش؟</p></div>
<div class="m2"><p>که بدین گونه رنگ و بویستش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باغبانش ز دور ناظر بود</p></div>
<div class="m2"><p>داد پاسخ که نیک حاضر بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت: عدل تو داد آب او را</p></div>
<div class="m2"><p>زان نبیند کسی خراب او را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پادشاهی به زور باشد و مرد</p></div>
<div class="m2"><p>مرد را مال دوست داند کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مال کس بی‌عمارتی ننهاد</p></div>
<div class="m2"><p>وین عمارت به عدل باشد و داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از عمارت نظر مدار دریغ</p></div>
<div class="m2"><p>بی‌رعیت چو آب باش و چو میغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک معمول و گنج مالامال</p></div>
<div class="m2"><p>بر کشد تخت را به گردون بال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاه بی‌شهر چون ستاند باج</p></div>
<div class="m2"><p>شهر بی‌ده زبون شود ز خراج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طلب عدل کن ز شاه و وزیر</p></div>
<div class="m2"><p>گو مدان نحو و حکمت و تفسیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نحوشان عمر و وزید را شاید</p></div>
<div class="m2"><p>عدلشان عالمی بیاراید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاه مهر و وزیر ماه بود</p></div>
<div class="m2"><p>زین دو آفاق در پناه بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شب چو رفت آفتاب در پرده</p></div>
<div class="m2"><p>مه نیابت کند دو صد مرده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ملک را شب وزیر نام اندوز</p></div>
<div class="m2"><p>حارس و پاسبان بود تا روز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نصب این هر دو کردگار کند</p></div>
<div class="m2"><p>نه ز رو مرد بیشمار کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نشود طالع اختر شاهی</p></div>
<div class="m2"><p>بی‌وجود مدبر داهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خنجر خسروست و کلک وزیر</p></div>
<div class="m2"><p>سپر ملک روز گیراگیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاه باشد به روز عدل چو باغ</p></div>
<div class="m2"><p>مرشب فتنه را وزیر چراغ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وزرا ملک را امینانند</p></div>
<div class="m2"><p>کار فرمای دولت اینانند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وزرایی، که مرکز جاهند</p></div>
<div class="m2"><p>آسمان قبول را ماهند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر نسازند کار درویشان</p></div>
<div class="m2"><p>وزر باشد وزارت ایشان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خلق صد شهر گشته سر گردان</p></div>
<div class="m2"><p>در پی خواجه در بدر گردان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پی ایشان هزار دل در تست</p></div>
<div class="m2"><p>کام این بیدلان بباید جست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>روی چندین هزار دل در تست</p></div>
<div class="m2"><p>کام این بیدلان بباید جست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کار ایشان به دست خویش بساز</p></div>
<div class="m2"><p>مرهم سینه‌های ریش بساز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خیر تاخیر بر نمی‌تابد</p></div>
<div class="m2"><p>خنک آنکس که خیر دریابد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چشم گیتی تویی، مرو در خواب</p></div>
<div class="m2"><p>فرصت از دست میرود، دریاب</p></div></div>