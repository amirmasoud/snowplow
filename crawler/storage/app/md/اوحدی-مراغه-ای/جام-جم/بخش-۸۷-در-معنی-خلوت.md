---
title: >-
    بخش ۸۷ - در معنی خلوت
---
# بخش ۸۷ - در معنی خلوت

<div class="b" id="bn1"><div class="m1"><p>پر دلی باید از عوایق دور</p></div>
<div class="m2"><p>تا درین خلوتش دهند حضور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر دلی کو ز جان نیندیشد</p></div>
<div class="m2"><p>سخن آب و نان نیندیشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشته تسلیم ره نماینده</p></div>
<div class="m2"><p>تا چه گردد ز وقت زاینده؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تحفهٔ جان نهاده بر کف دست</p></div>
<div class="m2"><p>روی دل کرده در سرای الست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر به دریای «لا» فرو برده</p></div>
<div class="m2"><p>تن به مرگ آشنا فرو برده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چو در وی کند سعادت رو</p></div>
<div class="m2"><p>تخته بیرون برد به ساحل «هو»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاطری تیز و فکرتی ثاقب</p></div>
<div class="m2"><p>واردات جمال را راقب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بر وی حواس بر بسته</p></div>
<div class="m2"><p>به نظرهای خاص پیوسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترک این عدت و عدد کرده</p></div>
<div class="m2"><p>هر چه غیر از خداست رد کرده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رستمی پشت کرده بر دستان</p></div>
<div class="m2"><p>روی در تیغ کرده چون مستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یاد او میکنی، به زاری کن</p></div>
<div class="m2"><p>سر او را خزینه داری کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به زبان نفی کن، به دل اثبات</p></div>
<div class="m2"><p>تا دلت پر شود ز عزت ذات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه به چپ در دهی ندا از راست؟</p></div>
<div class="m2"><p>که جزو هر چه هست جمله هباست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از زبان در دلت گشاید راه</p></div>
<div class="m2"><p>معجز لا اله الاالله</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گله در چول و غله اندر چال</p></div>
<div class="m2"><p>نتوان داشت چله از سر حال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از چهل خصلت ذمیمه ببر</p></div>
<div class="m2"><p>تا تو در چله فرد باشی و حر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چیست آن کبر و نخوت و هستی</p></div>
<div class="m2"><p>غضب و کید و غفلت و مستی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بطر و ریب و حرص و بخل و حیل</p></div>
<div class="m2"><p>بغض و بدعهدی و دروغ و دغل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شهوت و غمز و کندی و تیزی</p></div>
<div class="m2"><p>فسق و بهتان و فتنه‌انگیزی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>طیش و کفران و مردم‌آزاری</p></div>
<div class="m2"><p>هزل و غذر و نفاق و خونخواری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حسد و آز جبن و زرق و ریا</p></div>
<div class="m2"><p>کسل و ظلم و جور و حقد و جفا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنچه گفتم به خویشتن مپسند</p></div>
<div class="m2"><p>عکس اینها ببین و کارش بند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پس به خلوت نشین و زاری کن</p></div>
<div class="m2"><p>در فرو بند و چله داری کن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر که زین پر شد و از آن خالی</p></div>
<div class="m2"><p>در ممالک ولی شد و والی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دل او دفتر فرشته شود</p></div>
<div class="m2"><p>به حروف دگر نبشته شود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خلوت اینست و چله این باشد</p></div>
<div class="m2"><p>صفت عارفان چنین باشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دل، که خالی نگشت بازاریست</p></div>
<div class="m2"><p>خیز و خالیش کن که این کاریست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنکه فرمود کار به عین صباح</p></div>
<div class="m2"><p>گر به اخلاص نیست، نیست مباح</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مهل اندر دل خود از وسواس</p></div>
<div class="m2"><p>اثری از غرور «الخناس»</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر این «قل اعوذ» برخوانی</p></div>
<div class="m2"><p>«قل هوالله» باشدت ثانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون قوی دل شدی ز عالم غیبب</p></div>
<div class="m2"><p>هر چه خواهی بیابی اندر جیب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرغ همت ز گنج خانهٔ حال</p></div>
<div class="m2"><p>بر وجود بگستراند بال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به مرید ار خبر دهند از غیب</p></div>
<div class="m2"><p>در چنین حالتی نباشد عیب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا به شیخش یقین درست شود</p></div>
<div class="m2"><p>به ریاضت امین و رست شود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بشناسد جزای رنج که برد</p></div>
<div class="m2"><p>به چنان دستگاه و گنج که برد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نظر شیخ بر دلش تابد</p></div>
<div class="m2"><p>راز دلها برمز دریابد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شودش ذهن از آن زبان بستن</p></div>
<div class="m2"><p>به حدیثی چو گوهر آبستن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دل او گنج هر بیان آید</p></div>
<div class="m2"><p>وز دلش بر سر زبان آید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به چنین نیستی چو گردد هست</p></div>
<div class="m2"><p>دلش از جام فقر گردد مست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نسیه و نقد خود بر اندازد</p></div>
<div class="m2"><p>صدق دستور حال خود سازد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو ز دلها شود به صدق آگاه</p></div>
<div class="m2"><p>در دل او شود ز دلها راه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر چه را بر دلش گذر باشد</p></div>
<div class="m2"><p>شیخ را چون از آن خبر باشد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مهربان و شفیق او گردد</p></div>
<div class="m2"><p>به دل و جان رفیق او گردد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز سماع و حدیث و خفت وز خورد</p></div>
<div class="m2"><p>آن پسندد برو که بتوان کرد</p></div></div>