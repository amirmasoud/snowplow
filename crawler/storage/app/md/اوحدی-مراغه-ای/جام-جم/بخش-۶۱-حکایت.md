---
title: >-
    بخش ۶۱ - حکایت
---
# بخش ۶۱ - حکایت

<div class="b" id="bn1"><div class="m1"><p>بود در روم پیش ازین سر و کار</p></div>
<div class="m2"><p>صاحبی نان ده و فتوت یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لنگری باز کرده چون کشتی</p></div>
<div class="m2"><p>پر ز سنگ و ز آلت کشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در لنگر نهاده باز فراخ</p></div>
<div class="m2"><p>کرده ریش دراز را به دو شاخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلق رومش نماز بردندی</p></div>
<div class="m2"><p>بچهٔ خود بدو سپردندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نان صاحب ز کار رندان بود</p></div>
<div class="m2"><p>گوشه بیکارشان چو زندان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حوریان گرد او گروه شده</p></div>
<div class="m2"><p>رند و عامی در آه و اوه شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمع گشتند ازین صفت خیلی</p></div>
<div class="m2"><p>هر یکی را به دیگری میلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناگهان رومی غلام باره</p></div>
<div class="m2"><p>صورتی نحس و جامه‌ای پاره</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به یکی زان میانه عشق آورد</p></div>
<div class="m2"><p>علم مصر در دمشق آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در نهانی انار و سیبش داد</p></div>
<div class="m2"><p>تا به تلبیس خود فریبش داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برد روزی به گوشهٔ باغش</p></div>
<div class="m2"><p>می‌نهاد از عمود خود داغش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خر زهٔ خویش در وعا می‌کرد</p></div>
<div class="m2"><p>هر دمی بر اخی دعا می‌کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باغبان این بدید و گفت: ای خر</p></div>
<div class="m2"><p>پدرش را دعا کن و مادر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رند گفتا: ز هر دو بیزارم</p></div>
<div class="m2"><p>که من این دولت از اخی دارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حکم او تا به دست مادر بود</p></div>
<div class="m2"><p>طفل در خانه، قفل بر در بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون پدر پیش صاحب آوردش</p></div>
<div class="m2"><p>به نیابت چنین بپروردش</p></div></div>