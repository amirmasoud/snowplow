---
title: >-
    بخش ۷۱ - در راستی
---
# بخش ۷۱ - در راستی

<div class="b" id="bn1"><div class="m1"><p>راستی کن، که راستان رستند</p></div>
<div class="m2"><p>در جهان راستان قوی دستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راستگاران بلندنام شوند</p></div>
<div class="m2"><p>کج‌روان نیم پخته خام شوند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسف از راستی رسید به تخت</p></div>
<div class="m2"><p>راستی کن، که راست گردد بخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بدی دامنش گرفت چه باک؟</p></div>
<div class="m2"><p>چکند دست بد به دامن پاک؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راست گوینده راست بیند خواب</p></div>
<div class="m2"><p>خواب یوسف که کج نشد، دریاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون درو بود راست کرداری</p></div>
<div class="m2"><p>خواب او گشت قفل بیداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون به نیکی درید پیرهنی</p></div>
<div class="m2"><p>شد مسخر چو مصرش انجمنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیرهن کین بود مقاماتش</p></div>
<div class="m2"><p>دیده روشن کند کراماتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گو بدر بر تن نکو رفتار</p></div>
<div class="m2"><p>پوستین گرگ و پیرهن کفتار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دامنی را که در کشی ز هوا</p></div>
<div class="m2"><p>این اثرها کند، رواست، روا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به گزاف آنچنان عزیز نشد</p></div>
<div class="m2"><p>که گرفتار خفت و خیز نشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون خیانت نکرد با دل جفت</p></div>
<div class="m2"><p>راست آمد هر آن حدیث که گفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پاک دل را زیان به تن نرسد</p></div>
<div class="m2"><p>ور رسد جز به پیرهن نرسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از دو چاه و دو گرگ دیده شکنج</p></div>
<div class="m2"><p>چه عجب گر رسد به جاه و به گنج؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرگ اول چو بیگناه آمد</p></div>
<div class="m2"><p>نام او در کتاب شاه آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرگ آخر چو در فضیحت ماند</p></div>
<div class="m2"><p>ایزد او را به نام خویش بخواند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر غلامی عزیز گردد و شاه</p></div>
<div class="m2"><p>نه عجب، چون بری بود ز گناه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور شود شاه خواجهٔ جانی</p></div>
<div class="m2"><p>عجب اینست و نیست ارزانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قول و فعل تو تا نگردد راست</p></div>
<div class="m2"><p>هر چه خواهی نمود جمله هباست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کور و کر گرنه‌ای ز چاه مترس</p></div>
<div class="m2"><p>راست باش و زمیر و شاه مترس</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>استوار و شجاع باش و دلیر</p></div>
<div class="m2"><p>در نفاذ امور شرع چو شیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بندهٔ شرع باش و راتب او</p></div>
<div class="m2"><p>مگذر از شرع و از مراتب او</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عقل را شرع در کنشت کند</p></div>
<div class="m2"><p>جبن را شرع خوب و زشت کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صدق چون راست شد روانت را</p></div>
<div class="m2"><p>بی‌رعونت کند گمانت را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آخرین یار اولیا صدقست</p></div>
<div class="m2"><p>اولین کار انبیا صدقست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر که زین صدق دم تواند زد</p></div>
<div class="m2"><p>در ولایت قدم تواند زد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا نگردد درون و بیرون راست</p></div>
<div class="m2"><p>بوی صدق از تو برنخواهد خاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صدقت از نار خود سقیم کند</p></div>
<div class="m2"><p>صبر در صدق مستقیم کند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صادقان را رجال گفت خدای</p></div>
<div class="m2"><p>خنک آنکو به صدق دارد رای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صدق آیینه ایست حال ترا</p></div>
<div class="m2"><p>روی نفس تو و کمال ترا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا تو باشی، ز راستی مگذر</p></div>
<div class="m2"><p>مکش از خط راستگاران سر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صدق میزان کرده‌ها باشد</p></div>
<div class="m2"><p>و آنچه در زیر پرده‌ها باشد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر چو بوبکر صدق کرداری</p></div>
<div class="m2"><p>جز خدا و رسول نگذاری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>راستی ورز و رستگاری بین</p></div>
<div class="m2"><p>یار شو خلق را و یاری بین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>صادقی، هر چه جز خداست بباز</p></div>
<div class="m2"><p>از بد و نیک با خدا پرداز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ترسکاری، به راست رفتن کوش</p></div>
<div class="m2"><p>ور نداری، تو خود نداری هوش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر حکیمی دروغ سار مباش</p></div>
<div class="m2"><p>با کژو با دروغ یار مباش</p></div></div>