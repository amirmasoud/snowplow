---
title: >-
    بخش ۱۲۸ - خطاب به خواجه غیاث‌الدین محمد
---
# بخش ۱۲۸ - خطاب به خواجه غیاث‌الدین محمد

<div class="b" id="bn1"><div class="m1"><p>ای شب و روز عالم از تو بساز</p></div>
<div class="m2"><p>شب و روزی به کار ما پرداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب نگاهی درین معانی کن</p></div>
<div class="m2"><p>روز لطفی چنانکه دانی کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حبذا از چنان دل افروزی !</p></div>
<div class="m2"><p>اتفاق چنین شب و روزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صاحبا، در شب سعادت خواب</p></div>
<div class="m2"><p>مکن و روز نیک را دریاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که وجودت به جود فربه باد</p></div>
<div class="m2"><p>روزت از روز و شب ز شب به باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تحفه کین مفلس فقیر آورد</p></div>
<div class="m2"><p>در پذیر، ارچه بس حقیر آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو که بر فرق آسمان تاجی</p></div>
<div class="m2"><p>به متاع زمین چه محتاجی ؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر علومست در نوشتهٔ تست</p></div>
<div class="m2"><p>ور سلوکست سر گذشتهٔ تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه بدان آورندت اینها پیش</p></div>
<div class="m2"><p>که شود دانشت به اینها بیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن از خواندنت به کام رسد</p></div>
<div class="m2"><p>چون به نام تو شد به نام رسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کاملی را که بنگری از دور</p></div>
<div class="m2"><p>گرچه خامل بود، شود مشهور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صوت صیت تو در جهانگیری</p></div>
<div class="m2"><p>بر صدای فلک کند میری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قید اقبال در سر قلمت</p></div>
<div class="m2"><p>مرکز فتح سایهٔ علمت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مستی خواجگان همنامت</p></div>
<div class="m2"><p>در دو گیتی ز جرعهٔ جامت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر تو خوردی ازین جهانداری</p></div>
<div class="m2"><p>که بزرگی ز آسمان داری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدعا خواستست شاه ترا</p></div>
<div class="m2"><p>زان پرستد همی سپاه ترا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با تو همراه کرده‌اند از غیب</p></div>
<div class="m2"><p>سروری، چون کف کلیم از جیب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای همه ناز و نوشها بتو خوش</p></div>
<div class="m2"><p>ناز ما نیز وقتها میکش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طرفه باشد چو موی بر دیبا</p></div>
<div class="m2"><p>ناز کردن ز روی نازیبا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من درین سالها که بی توشه</p></div>
<div class="m2"><p>کرده بودم زاین و آن گوشه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ارغنون غمت نواخته‌ام</p></div>
<div class="m2"><p>بدعای تو سر فراخته‌ام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خانه پرور ز سایه گوید و نور</p></div>
<div class="m2"><p>عاشقانرا چه غیبت و چه حضور؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مردم این جهان و مرد تویی</p></div>
<div class="m2"><p>نوش داروی اهل درد تویی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن مبین کم سریست یا پاییست؟</p></div>
<div class="m2"><p>بشنو کین سخن هم از جاییست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر قبول اوفتد رهینم و شاد</p></div>
<div class="m2"><p>و گرش رد کنی، بقای تو باد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه که هر مهره‌ای گهر باشد</p></div>
<div class="m2"><p>کار درویش ما حضر باشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چشم کردی بروی هرکس باز</p></div>
<div class="m2"><p>نظری هم بدین غریب انداز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من چگویم : چه کن؟ تو میدانی</p></div>
<div class="m2"><p>مددم کن بهر چه بتوانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نظری کن به حال من زین به</p></div>
<div class="m2"><p>زانکه من هم رعیتم در ده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ده نشینی چه دیگ جوشاند ؟</p></div>
<div class="m2"><p>جامهٔ مدح در که پوشاند ؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این چنین فضل و خلق باید و خوی</p></div>
<div class="m2"><p>تا توان باخت در معانی گوی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از تو گیرد سخن فروغ چو شمع</p></div>
<div class="m2"><p>که بر تست کل معنی جمع</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مصر جامع تویی معانی را</p></div>
<div class="m2"><p>پادشاهی و پهلوانی را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هرکجا این چنین کمالی هست</p></div>
<div class="m2"><p>نطق را اندرو مجالی هست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا کنونم نبوده ممدوحی</p></div>
<div class="m2"><p>آب توفان آز را نوحی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون رسید این سفینه بر جودی</p></div>
<div class="m2"><p>عرضه افتد به لحن داودی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در زبور سخن مناجاتم</p></div>
<div class="m2"><p>مشتمل بر فنون حاجاتم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بنوازم به قدر و اندازه</p></div>
<div class="m2"><p>تا برون آورم تر و تازه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از نورد سخن نسیجی چند</p></div>
<div class="m2"><p>وز رصدگاه فضل زیجی چند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گرچه از سیرت هنر پوشی</p></div>
<div class="m2"><p>تن فرو داده‌ام به خاموشی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دگر اندر خروشم آوردند</p></div>
<div class="m2"><p>همچو دریا به جوشم آوردند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سخن اوحدی، که میدانی</p></div>
<div class="m2"><p>اندرین روزگار ارزانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کم به دیوان برند مانندش</p></div>
<div class="m2"><p>ور مدون شود، بخوانندش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر مگس انگبین چه داند کرد؟</p></div>
<div class="m2"><p>جز مگس انگبین تواند خورد؟</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مگسی انگبین چو ماه کند</p></div>
<div class="m2"><p>مگسی دیگرش تباه کند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>این سخنهای بکر پرورده</p></div>
<div class="m2"><p>مهل امروز در پس پرده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شعر نوری ز عرش زاینده است</p></div>
<div class="m2"><p>زان چو عرش استوار و پاینده است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فیض باید به آسمان قایم</p></div>
<div class="m2"><p>تا بماند چو آسمان دایم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گرچه فوجی به شعر مشهورند</p></div>
<div class="m2"><p>پیش عقل از حساب ما دورند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اندرین جام کن به لطف نگاه</p></div>
<div class="m2"><p>تا ببینی چو بیژنم در چاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ای که کیخسرو زمانی تو</p></div>
<div class="m2"><p>کی روا باشد ار ندانی تو؟</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بیژن شیر خفته در زندان</p></div>
<div class="m2"><p>کنده گرگین بی‌هنر دندان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>داری این جام و این گلستان را</p></div>
<div class="m2"><p>بدر افگن سفال مستان را</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون چراغیست این صحیفهٔ نور</p></div>
<div class="m2"><p>شده نزدیک ازو منور و دور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کش برافروختم به روغن روح</p></div>
<div class="m2"><p>آخر شب به بزمهای صبوح</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هر کرا باشد این چنین گنجی</p></div>
<div class="m2"><p>برده باشد به حاصلش رنجی</p></div></div>