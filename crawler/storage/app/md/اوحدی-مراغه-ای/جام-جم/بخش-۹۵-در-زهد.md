---
title: >-
    بخش ۹۵ - در زهد
---
# بخش ۹۵ - در زهد

<div class="b" id="bn1"><div class="m1"><p>زهدت آن باشد، ای سعادت جوی</p></div>
<div class="m2"><p>کز متاع جهان بتابی روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی در فضل بی‌نیاز کنی</p></div>
<div class="m2"><p>پشت بر فضلهٔ مجاز کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر فرازی ز فقر صرف درفش</p></div>
<div class="m2"><p>زان توجه کلاه سازی و کفش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود، گر ز زهد گیری رنگ</p></div>
<div class="m2"><p>حاجت اربعین و خلوت تنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که او زهد را حصار کند</p></div>
<div class="m2"><p>تیر شیطان برو چه کار کند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهد چون قلعه‌ایست پاس ترا</p></div>
<div class="m2"><p>قفس آهنین حواس ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قلعه را در مساز بی‌بارو</p></div>
<div class="m2"><p>احتما باید، آنگهی دارو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلوت از بهر آن پسند آید</p></div>
<div class="m2"><p>که حواس تنت به بند آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شد از زهد گردنت باریک</p></div>
<div class="m2"><p>نیست محتاج خلوت تاریک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خویشتن را ازین و آن باز آر</p></div>
<div class="m2"><p>پس همی گیر چله در بازار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حاضر وقت باش و غایب غیر</p></div>
<div class="m2"><p>تا توانی به استقامت سیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون نهادی کلاه خرسندی</p></div>
<div class="m2"><p>بر در بندگی کمر بندی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر دلی کو به زهد چست آید</p></div>
<div class="m2"><p>به عبادت رسد، درست آید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زهد فرضست و زهد فضل، بدان</p></div>
<div class="m2"><p>ترک دنیا بدین دو زهد توان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهد فرض از حرام برگشتن</p></div>
<div class="m2"><p>زهد فضل از حلال بگذشتن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چونکه امروز خود حلالی نیست</p></div>
<div class="m2"><p>دومین زهد جز خیالی نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زاهدی، جز حلال کم نخوری</p></div>
<div class="m2"><p>به بود کان حلال هم نخوری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر کرا زهد پرده‌دار شود</p></div>
<div class="m2"><p>محرم وحی کردگار شود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دست عثمان، که تیر شد قلمش</p></div>
<div class="m2"><p>زهد کرد از جهانیان علمش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زاهدی ترک مال و جاه بود</p></div>
<div class="m2"><p>ترگ چون پر شود کلاه بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر همی خواهی این کلاه بلند</p></div>
<div class="m2"><p>کمر بندگی و طاعت بند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر که او راست دید و زرق نکرد</p></div>
<div class="m2"><p>این کله را ز تاج فرق نکرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تاج را لازمست دری خاص</p></div>
<div class="m2"><p>در این تاج نیست جز اخلاص</p></div></div>