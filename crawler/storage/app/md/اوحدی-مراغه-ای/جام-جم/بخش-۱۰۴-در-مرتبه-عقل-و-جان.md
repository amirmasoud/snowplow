---
title: >-
    بخش ۱۰۴ - در مرتبهٔ عقل و جان
---
# بخش ۱۰۴ - در مرتبهٔ عقل و جان

<div class="b" id="bn1"><div class="m1"><p>پیش ازین آدمی و این آدم</p></div>
<div class="m2"><p>دیو بود و فرشته در عالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون رسید آدمی ز عالم جود</p></div>
<div class="m2"><p>عزتش را فرشته کرد سجود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با روانش ملک چو خویشی داشت</p></div>
<div class="m2"><p>پیش دیدش که رخ به پیشی داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه جمع فرشته و ملکند</p></div>
<div class="m2"><p>از قواهای انجم و فلکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کنند از محل خویش نزول</p></div>
<div class="m2"><p>شکلهای دگر کنند قبول</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اصل جنی ز نار بود و هوا</p></div>
<div class="m2"><p>بر فلک زان نرفت و نیست روا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک آدم بدید و سجده نبرد</p></div>
<div class="m2"><p>دیدگانش به خاک خواهد مرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک او دیده بود و آتش خود</p></div>
<div class="m2"><p>نور او را یکی ندید از صد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر او زان قفای لعنت خورد</p></div>
<div class="m2"><p>که قفا را ز روی فرق نکرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو به نفس شریف و عقل زکی</p></div>
<div class="m2"><p>از شمار فرشته و ملکی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غضبت آتشست و شهوت باد</p></div>
<div class="m2"><p>وین دو دیو چنین ترا همزاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقلت از عالم اله آمد</p></div>
<div class="m2"><p>نفست از بارگاه شاه آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو ملک با تو اینچنین همراه</p></div>
<div class="m2"><p>سوی ایشان نمیکنی تو نگاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست تن را مهار در بینی</p></div>
<div class="m2"><p>جز خرد در دماغ، اگر بینی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عقل بر ناخوشی کشید و خوشی</p></div>
<div class="m2"><p>تا جدا گشت رومی از حبشی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نامهایی کز آسمان آید</p></div>
<div class="m2"><p>همه بر نام عقل و جان آید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جز خرد مرد آن جواب نبود</p></div>
<div class="m2"><p>غیر ازو لایق خطاب نبود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تن درنده است و روح دارنده</p></div>
<div class="m2"><p>عقل مر هر دو را نگارنده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جامهٔ کون را علم عقلست</p></div>
<div class="m2"><p>روح لوح آمد و قلم عقلست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تن و جان را به دست عقل سپار</p></div>
<div class="m2"><p>پای بیگانه در میانه میار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>علم نیرو دهد کمالت را</p></div>
<div class="m2"><p>عقل اجابت کند سؤالت را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون ترا زین جهان گزیری نیست</p></div>
<div class="m2"><p>بهتر از عقل دستگیری نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای به تایید عقل بیننده</p></div>
<div class="m2"><p>آفرین کن بر آفریننده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که تواند ز آب گندیده</p></div>
<div class="m2"><p>آفریدن رخ و لب و دیده؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قالبت را که هست پردهٔ روح</p></div>
<div class="m2"><p>آلت روح دان و کردهٔ روح</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کردهٔ اوست، نازنین ز آنست</p></div>
<div class="m2"><p>از چنان نیست، از چنین ز آنست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روح و چندین فرشته در کارند</p></div>
<div class="m2"><p>تو به خوابی و جمله بیدارند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا تو بازار خویش تیز کنی</p></div>
<div class="m2"><p>آمد و رفت و خفت و خیز کنی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زان عمل ساعتی نیاسایند</p></div>
<div class="m2"><p>تو بفرسایی و نفرسایند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر کجا عقل و جان تواند بود</p></div>
<div class="m2"><p>تن کجا در میان تواند بود؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در عروقی بدین صفت باریک</p></div>
<div class="m2"><p>مخرجی تنگ و مدخلی تاریک</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کیست جز جان که کار داند کرد</p></div>
<div class="m2"><p>راز خویش آشکار داند کرد؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پی جان رو، که کار کن جانست</p></div>
<div class="m2"><p>تن بیچاره بنده فرمانست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون سپاه تو بار بربندد</p></div>
<div class="m2"><p>عقل راه شمار بربندد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر مجرد شود فرشتهٔ تو</p></div>
<div class="m2"><p>نرسد آفتی به کشتهٔ تو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عقل شمعست و علم بیداری</p></div>
<div class="m2"><p>نفس خواب و هوس شب تاری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عقل را هم چو دل نداند کس</p></div>
<div class="m2"><p>روح را دل نکو شناسد و بس</p></div></div>