---
title: >-
    بخش ۴ - در نعت رسول
---
# بخش ۴ - در نعت رسول

<div class="b" id="bn1"><div class="m1"><p>عاشقی، خیز و حلقه بر در زن</p></div>
<div class="m2"><p>دست در دامن پیمبر زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حب این خواجه پایمرد تو بس</p></div>
<div class="m2"><p>نظر او دوای درد تو بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اوست معنی و این دگرها نام</p></div>
<div class="m2"><p>پخته او بود و این دگرها خام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه از اصطفا بر افلا کند</p></div>
<div class="m2"><p>در ره مصطفی کم از خاکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از در او توان رسید به کام</p></div>
<div class="m2"><p>دیگران را بهل برین در و بام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اوست در کاینات مردم و مرد</p></div>
<div class="m2"><p>او خداوند دین و صاحب درد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سفر آدم سفیرنامهٔ اوست</p></div>
<div class="m2"><p>درج ادریس درج خامهٔ اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیعه در بیعتش میان بسته</p></div>
<div class="m2"><p>زانکه ناقوس را زبان بسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سر او ز نیک نامی تاج</p></div>
<div class="m2"><p>همه شب‌های او شب معراج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش او خود مکن حکایت شب</p></div>
<div class="m2"><p>او چراغ، آنگهی شکایت بس؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوهر چار عقد و نه درج اوست</p></div>
<div class="m2"><p>اختر پنج رکن و نه برج اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شقهٔ عرض عطف دامانش</p></div>
<div class="m2"><p>ملک از زمرهٔ غلامانش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن که مه بشکند به نیم انگشت</p></div>
<div class="m2"><p>آفتابش چه باشد اندر مشت؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وانکه در دست اوست ماه فلک</p></div>
<div class="m2"><p>پایش آسان رود به راه فلک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شب معراج کوس مهر زده</p></div>
<div class="m2"><p>خیمه بر تارک سپهر زده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گذر از تیر و از زحل کرده</p></div>
<div class="m2"><p>مشکل هفت چرخ حل کرده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر سر جملها بدانسته</p></div>
<div class="m2"><p>شرح و تقصیل آن توانسته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در دمی شد نود هزار سخن</p></div>
<div class="m2"><p>کشف برجان او ز عالم کن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به دمی رفته، باز گردیده</p></div>
<div class="m2"><p>روی او را به چشم سر دیده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>میم احمد چو از میان برخاست</p></div>
<div class="m2"><p>به یقین خود احد بماند راست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>راه دان اوست، جبرییلش ساز</p></div>
<div class="m2"><p>هر چه او آورد، دلیلش ساز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای فلک موکب ستاره حشر</p></div>
<div class="m2"><p>وی ز بشرت گشاده روی بشر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هاشمی نسبت قریشی اصل</p></div>
<div class="m2"><p>ابطحی طینت تهامی فصل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>علم نصرتت ز عالم نور</p></div>
<div class="m2"><p>یزک لشکرت صبا و دبور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چرخ نه پایه پای منبر تو</p></div>
<div class="m2"><p>به سر عرش جای منبر تو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>معجزت سنگ را زبان بخشد</p></div>
<div class="m2"><p>بوی خلقت به مرده جان بخشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روز محشر، که بار عام بود</p></div>
<div class="m2"><p>از تو یک امتی تمام بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگرفته به نور شرع یقین</p></div>
<div class="m2"><p>چار یار تو چار حد زمین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز ایزد و ما درود چون باران</p></div>
<div class="m2"><p>به روان تو باد و بر یاران</p></div></div>