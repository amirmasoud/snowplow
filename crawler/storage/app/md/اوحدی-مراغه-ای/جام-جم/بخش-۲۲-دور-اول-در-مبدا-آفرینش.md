---
title: >-
    بخش ۲۲ - دور اول در مبدا آفرینش
---
# بخش ۲۲ - دور اول در مبدا آفرینش

<div class="b" id="bn1"><div class="m1"><p>روز شد، ای حکیم،از آن منزل</p></div>
<div class="m2"><p>خبری ده که چون گذشت این دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود ازین آمدن مراد چه بود؟</p></div>
<div class="m2"><p>سر این هجر و این بعاد چه بود؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر آغاز کار دریابیم</p></div>
<div class="m2"><p>وز وجود جهان خبر یابیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه دانستنیست این به عیان</p></div>
<div class="m2"><p>گر ندانسته‌ای درست بدان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاولین قسمت از طریق قیاس</p></div>
<div class="m2"><p>در وجود و عدم دهند اساس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وین وجود ار فنا پذیر بود</p></div>
<div class="m2"><p>ممکنست ار چه بر اثیر بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور فنا را بدو نباشد راه</p></div>
<div class="m2"><p>واجبست و بدین مخواه گواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ذات واجب قدیم و فرد بود</p></div>
<div class="m2"><p>بی‌چه و چون و خواب و خورد بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باشد او از جهات نیز بدر</p></div>
<div class="m2"><p>تو از آن ذات بی‌جهت مگذر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه در امتناع و امکانست</p></div>
<div class="m2"><p>ذات واجب مغایر آنست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون شد از امتناع و امکان حر</p></div>
<div class="m2"><p>شد ز جودش وجود عالم پر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کرد هستیش اقتضای ظهور</p></div>
<div class="m2"><p>زانکه نورست و فاش گردد نور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ذات او بر وجود شاهی کرد</p></div>
<div class="m2"><p>رحمتش رخ به نیک خواهی کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صنع را مظهری ضرورت شد</p></div>
<div class="m2"><p>طالب جسم و جان و صورت شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اول جمله اوست، عز وجل</p></div>
<div class="m2"><p>گر چه آخر ندارد و اول</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عزتش چون ز خود به خود پرداخت</p></div>
<div class="m2"><p>نظری بر کمال خویش انداخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان نظر گشت عقل کل موجود</p></div>
<div class="m2"><p>عقل کورا بدید کرد سجود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نفس کل شد پدید از آن دیدن</p></div>
<div class="m2"><p>شد پسندیده زان پسندیدن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نفس چون در سوم نورد افتاد</p></div>
<div class="m2"><p>سومین جوهر دو فرد افتاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زان سه رتبت سه بعد پیدا شد</p></div>
<div class="m2"><p>پیکر آسمان هویدا شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جوهر نفس چون به خود نگریست</p></div>
<div class="m2"><p>تا بداند که حق که واو کیست؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عقل و نفس و فلک پدید آمد</p></div>
<div class="m2"><p>چرخ در گفت و در شنید آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هم چنین تا که نه فلک شد راست</p></div>
<div class="m2"><p>حکمتش چون بدین فزونی خواست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شد عیان زین دو چار کاشانه</p></div>
<div class="m2"><p>هفت شاه و دوازده خانه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه در مهد این همایون رخش</p></div>
<div class="m2"><p>روشن آیین و روشنایی بخش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نرم خوبان تیز تا زنده</p></div>
<div class="m2"><p>هر یکی پرده‌ای نوازنده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چرخ چون دور کرد و شد شیدا</p></div>
<div class="m2"><p>شد زمین روشن و زمان پیدا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در زمان گشت چار فصل پدید</p></div>
<div class="m2"><p>بر زمین نیز هفت خط بکشید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هفت اقلیم از آن بپیوستند</p></div>
<div class="m2"><p>هر یکی بر ستاره‌ای بستند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون از آن جنبش شبانروزی</p></div>
<div class="m2"><p>یافت انجم برات پیروزی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شد نماینده زین ورق درحال</p></div>
<div class="m2"><p>مشرق و مغرب و جنوب و شمال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چرخ از اول که چیره شد در دور</p></div>
<div class="m2"><p>چار عنصر پدید شد بر فور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کاتش و باد و آب و خاک تواند</p></div>
<div class="m2"><p>هم حیات تو، هم هلاک تواند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وین عناصر چو دست بر هم داد</p></div>
<div class="m2"><p>زان سه مولود نامدار بزاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن سه مولود چیست؟ نیک بدان</p></div>
<div class="m2"><p>معدن و پس نبات و پس حیوان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گشت معدن به خاک پوشیده</p></div>
<div class="m2"><p>وز زمین شد نبات جوشیده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حیوان بر زمین و آب و هوا</p></div>
<div class="m2"><p>شد به جنبش روان و حکم روا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>این سه موقوف بر چهار ارکان</p></div>
<div class="m2"><p>و آن برین هفت گنبد گردان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چرخ محتاح نفس و نفس به عقل</p></div>
<div class="m2"><p>تا به وحدت رسید نقل به نقل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر چه هر یک چنین مدار کند</p></div>
<div class="m2"><p>چون به وحدت رسید، فرار کند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آنکه با عقل بود روحش جفت</p></div>
<div class="m2"><p>جنبش نفس را طبیعت گفت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>طبع چون در مزاج پیوندد</p></div>
<div class="m2"><p>از تراکیب نقشها بندد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چونکه از طبع و از مزاج برون</p></div>
<div class="m2"><p>نیست این نقشهای گوناگون</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اختلاف زمان برون آورد</p></div>
<div class="m2"><p>نه مزاج از چهار عنصر فرد</p></div></div>