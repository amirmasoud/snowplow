---
title: >-
    بخش ۳۸ - در منع تبختر و طیش
---
# بخش ۳۸ - در منع تبختر و طیش

<div class="b" id="bn1"><div class="m1"><p>نرم باش، ای پسر، به رفتن، نرم</p></div>
<div class="m2"><p>تا نگردد دلت به رفتن گرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این صفتهای لاابالی چیست؟</p></div>
<div class="m2"><p>تو چه دانی که چند خواهی زیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته‌ای: از جهان چو میگذریم</p></div>
<div class="m2"><p>خود بیا تا غم جهان نخوریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نمانی نه در شمار شوی</p></div>
<div class="m2"><p>ور بمانی نه کم وقار شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه ضرورت به ترک تازیدن؟</p></div>
<div class="m2"><p>پیش شمشیر مرگ بازیدن؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوش بر قول ناخلف کردن؟</p></div>
<div class="m2"><p>مال و اوقات خود تلف کردن؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوش تا خویش را نیارایی</p></div>
<div class="m2"><p>که نمانی اگر بکار آیی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در تو چون روزگار چشم کند</p></div>
<div class="m2"><p>چون تواند دلت که خشم کند؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاید ار حال خود بگردانی</p></div>
<div class="m2"><p>تا مگر چشم بد بگردانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باد سر خاکسار خواهدبود</p></div>
<div class="m2"><p>باده خور خاک خوار خواهد بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفس اگر شوخ شد، خلافش کن</p></div>
<div class="m2"><p>تیغ جهلست در غلافش کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه شب عیش و باده خوردن تست</p></div>
<div class="m2"><p>که آبروی جهان به گردن تست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دوستی زین عمل به باد شود</p></div>
<div class="m2"><p>دشمن خود مهل، که شاد شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر سبک‌سر نشاید ایمن بود</p></div>
<div class="m2"><p>که سبک‌سر به سر در آید زود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کم شنیدم که مرد آهسته</p></div>
<div class="m2"><p>گردد از خوی خویشتن خسته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست در شهرسست فرهنگی</p></div>
<div class="m2"><p>هیچ عیبی بتر ز بی‌سنگی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در هنر بس پدر که داد دهد</p></div>
<div class="m2"><p>پسری شپ شپش به باد دهد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای که رویت به قربت شاهست</p></div>
<div class="m2"><p>چه روی کابگینه در راهست؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>میروی، نرم تر بنه گامت</p></div>
<div class="m2"><p>تا مبادا که بشکنی جامت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حیف! عیشی چنین به دست آورد</p></div>
<div class="m2"><p>پس به طیشی درو شکست آورد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر بترسی ز پادشاه خموش</p></div>
<div class="m2"><p>در مراعات سر شاهی کوش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاه خاموش با تو در سازد</p></div>
<div class="m2"><p>سر شاهی سرت بیندازد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر نه دین قاید امارت تست</p></div>
<div class="m2"><p>بس خرابی که در عمارت تست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خود نمایی به اسب و جامه مکن</p></div>
<div class="m2"><p>گوش بر اهل سوق و عامه مکن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>راست گردان ز بهر نام بلند</p></div>
<div class="m2"><p>سیرتی خاص گیر عام پسند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چند جویی برین و آن پیشی؟</p></div>
<div class="m2"><p>نه کز ابنای جنس خود بیشی؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو نبودی پدیدت آوردند</p></div>
<div class="m2"><p>پس به گفت و شنیدت آوردند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باز فانی شوی به آخر کار</p></div>
<div class="m2"><p>به سگان باز دار این مردار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در میان دو نیست هستی تو</p></div>
<div class="m2"><p>غایت غفلتست مستی تو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه نهی در میان این دو فنا</p></div>
<div class="m2"><p>بر خود و دوش خویش رنج و عنا؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر که بالاترست منزل او</p></div>
<div class="m2"><p>به تواضع رغوب‌تر دل او</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه را روی در تو و تو به خواب</p></div>
<div class="m2"><p>چه دهی پیش کردگار جواب؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>قرب سلطان مبارک آنکس راست</p></div>
<div class="m2"><p>که کند کار مستمندی راست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خوش بباید بر آن امیر گریست</p></div>
<div class="m2"><p>که به تدبیر روستایی زیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روستایی کند کفایت و صرف</p></div>
<div class="m2"><p>تو مگر سازی از خراجش طرف</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وانگهی خویش را امین دانی</p></div>
<div class="m2"><p>آه اگر مردمی چنین دانی!</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مکن از بهر این تفرج و فرج</p></div>
<div class="m2"><p>رزق ده ساله را به زودی خرج</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بیوه زن دوک رشته در مهتاب</p></div>
<div class="m2"><p>کرده بر خود حرام راحت و خواب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خایهٔ مرغ گرد کرده به صبر</p></div>
<div class="m2"><p>تا بیاید امیر و از سر جبر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خایه‌ها را به خایگینه کند</p></div>
<div class="m2"><p>مرغ و کرباس را هزینه کند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وانگهی بر نشیند و تازد</p></div>
<div class="m2"><p>فلکش سر چرا نیندازد؟</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به جفا دل مهل، که چست شود</p></div>
<div class="m2"><p>کانچه بشکست کی درست شود؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چه نهی بر نهال خود تیشه؟</p></div>
<div class="m2"><p>در بریدن بباید اندیشه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>غضبی، کز طریق دانش خاست</p></div>
<div class="m2"><p>عقل و دین عذر آن تواند خواست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آن غضب ناپسند باشد و زشت</p></div>
<div class="m2"><p>که چو کردی مجال عذر نهشت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در جهان هر چه حکمت و ریوست</p></div>
<div class="m2"><p>همه تریاک زهر این دیوست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خرد و جانت ار تمام شوند</p></div>
<div class="m2"><p>غضب و شهوتت غلام شوند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بس رسول و نبی شدند هلاک</p></div>
<div class="m2"><p>تا جهان زان دو دیو گردد پاک</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>این دو را گر تو زیر گام کنی</p></div>
<div class="m2"><p>خویشتن را بلند نام کنی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مکن از جام جهل خود را مست</p></div>
<div class="m2"><p>که بیکباره میروی از دست</p></div></div>