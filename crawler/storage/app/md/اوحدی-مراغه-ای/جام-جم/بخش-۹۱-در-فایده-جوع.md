---
title: >-
    بخش ۹۱ - در فایدهٔ جوع
---
# بخش ۹۱ - در فایدهٔ جوع

<div class="b" id="bn1"><div class="m1"><p>قوت دل ز عقل و جان باشد</p></div>
<div class="m2"><p>قوت تن ز آب و نان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانه خالی بود، حضور دهد</p></div>
<div class="m2"><p>تن خالی فروغ و نور دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علم جویی، به ترک سیری کن</p></div>
<div class="m2"><p>جان طلب میکنی، دلیری کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر خاری بخور، مشوه خیره</p></div>
<div class="m2"><p>تا نگردد دلت چو تن تیره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صیقل نفس چیست؟ کم خوردن</p></div>
<div class="m2"><p>آفت عقل؟ نفس پروردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلق را بر نماز داشته‌اند</p></div>
<div class="m2"><p>صفت روزه راز داشته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهتر از جوع بر دلیلی نیست</p></div>
<div class="m2"><p>به جزین آتش خلیلی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتشی کو بهار و لاله دهد</p></div>
<div class="m2"><p>ترک این سفره و نواله دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بدان ملک آرزوست رجوع</p></div>
<div class="m2"><p>نرسی جز به پای مردی جوع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رای روشن شود ز کم خوردن</p></div>
<div class="m2"><p>بهر خوردن چراست غم خوردن؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عود و چنک و چغان که پر سازند</p></div>
<div class="m2"><p>از درون تهی خوش آوازند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پر شکم شد، خر و رباب یکیست</p></div>
<div class="m2"><p>تیره گردید، خاک و آب یکیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عیب« صوت‌الحمیر» میدانی</p></div>
<div class="m2"><p>بر سر سفره خر چه میرانی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شکمت پر شود، بخار کند</p></div>
<div class="m2"><p>بر دماغ و دیو اندر آید از در تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نحل را چون لطیف بود خورش</p></div>
<div class="m2"><p>گشت نخلی که شهد بود برش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خون حیوان مخور که گنده شوی</p></div>
<div class="m2"><p>آب حیوان بخور، که زنده شوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آب حیوان مدان به جز دانش</p></div>
<div class="m2"><p>چون بیابی، به نوش از جانش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زین خورشها تهی شکم بهتر</p></div>
<div class="m2"><p>ور حلالست نیز کم بهتر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که چو بادت در شکنبه زند</p></div>
<div class="m2"><p>آتشت در کلاه و پنبه زند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در نباتی چو کثرت عددی</p></div>
<div class="m2"><p>نیست، کم شد درو فضول ردی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باز حیوان که اصل ترکیبش</p></div>
<div class="m2"><p>بیشتر بود، گشت کم طیبش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گند سرگین ز گند غایط کم</p></div>
<div class="m2"><p>کین یک از رستنیست و آن از دم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به جزین چون نماند برهانی</p></div>
<div class="m2"><p>خاک خوردن به از چنین نانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون به پاکیست فرق این که و مه</p></div>
<div class="m2"><p>معدنی از نبات و حیوان به</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آز را تا تو هم شکی یابی</p></div>
<div class="m2"><p>کام یابی، ولیک کم یابی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چند و چند آخر از گران خیزی؟</p></div>
<div class="m2"><p>جهد کن تا در آن میان خیزی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو نه از بهر خوردن آمده‌ای</p></div>
<div class="m2"><p>کز پی کار کردن آمده‌ای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بندهٔ مرده دل چکار آید؟</p></div>
<div class="m2"><p>زنده شو، تا سگت شکار آید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>راه دینا ز بهر رفتن تست</p></div>
<div class="m2"><p>نه ز بهر فراغ و خفتن تست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر چه مستت کند شراب تو اوست</p></div>
<div class="m2"><p>و آنچه بی خویش کرد خواب تو اوست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نان اگر پرخوری، کند مستی</p></div>
<div class="m2"><p>کم خور، ای خواجه، کز بلا رستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دل چرا میل آن طعام کند؟</p></div>
<div class="m2"><p>که حلال ترا حرام کند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گندم و گوشت خون شود در تن</p></div>
<div class="m2"><p>خون منی گردد و منی روغن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آتش شهوت اندر آن افتد</p></div>
<div class="m2"><p>فتنه‌ای درمیان ران افتد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شوخ از آن روغنست در تن تو</p></div>
<div class="m2"><p>خون صابونیان به گردن تو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نفس پرچرک و خرقه صابونی؟</p></div>
<div class="m2"><p>این هم از حیلتست و مابونی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>روزه‌دار و به دیگران بخوران</p></div>
<div class="m2"><p>نه بخور روز و شب، شکم بدران</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو ز آسیب روزهٔ ماهی</p></div>
<div class="m2"><p>برکشی هر دم از جگر آهی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عارفان ماه خویش سال کنند</p></div>
<div class="m2"><p>روزه گیرند و شب وصال کنند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ننمایند روی وصل به خام</p></div>
<div class="m2"><p>پختگان را وصال نیست حرام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آنکه از پیش کردگار خورد</p></div>
<div class="m2"><p>با تو چون هر شبی دوبار خورد؟</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو که هم شام و هم سحر بخوری</p></div>
<div class="m2"><p>ره به آن روزها چگونه بری؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با چنان خوردن و چنان آروق</p></div>
<div class="m2"><p>چون بری رخت روح بر عیوق؟</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بسکه شب نای لب بجنبانی</p></div>
<div class="m2"><p>روز مانند نای انبانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عارفان را ز روزه در شب قدر</p></div>
<div class="m2"><p>شود از فیض نور چهره چو بدر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تو به روزی هلال عید شوی</p></div>
<div class="m2"><p>ور به ماهی رسد قدید شوی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو شکم بوده‌ای، از آنی سست</p></div>
<div class="m2"><p>جان و دل باش، تا که باشی چست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هر که روزش به فربهی باشد</p></div>
<div class="m2"><p>چون شکم شد تهی، تهی باشد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تن چو از خون ثقیل سنگ آید</p></div>
<div class="m2"><p>دل ز بار بدن به تنگ آید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در تن این باد ناخوش و گنده</p></div>
<div class="m2"><p>چون گذارد چراغ را زنده؟</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر دمت بوی بر دماغ زند</p></div>
<div class="m2"><p>همچو بادی که بر چراغ زند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شکم پر ز هیج را چکنی؟</p></div>
<div class="m2"><p>رودهٔ پیچ پیچ را چکنی؟</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جگر و دل درست کن بیقین</p></div>
<div class="m2"><p>جگر شیر مردی و دل دین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تو ز کم خوردن و ز بیخوابی</p></div>
<div class="m2"><p>یابی، ار زانکه دولتی یابی</p></div></div>