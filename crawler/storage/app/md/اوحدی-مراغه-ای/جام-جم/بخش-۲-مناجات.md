---
title: >-
    بخش ۲ - مناجات
---
# بخش ۲ - مناجات

<div class="b" id="bn1"><div class="m1"><p>ای خرد را تو کار سازنده</p></div>
<div class="m2"><p>جان و تن را تو دل نوازنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صفات تو محو شد صفتم</p></div>
<div class="m2"><p>گم شد اندر ره تو معرفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روشنایی ببخش از آن نورم</p></div>
<div class="m2"><p>از در خویشتن مکن دورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشحهٔ نور در دماغم ریز</p></div>
<div class="m2"><p>زیت این شیشه در چراغم ریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ببینم چو در نظر باشی</p></div>
<div class="m2"><p>راه یابم چو راه بر باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنمایی،چرا ندانم دید؟</p></div>
<div class="m2"><p>ننمایی، کجا توانم دید؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه شد مدتی که در راهم</p></div>
<div class="m2"><p>همچنان در هبوط این چاهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پس پرده میکنم بازی</p></div>
<div class="m2"><p>تا مگر پرده را براندازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر درت بی‌ادب زدم انگشت</p></div>
<div class="m2"><p>حلقه‌ای ساختم ز چنبر پشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا ز در حلقه را در آویزم</p></div>
<div class="m2"><p>میزنم آه و اشک میریزم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بتو میپویم، ای پناهم تو</p></div>
<div class="m2"><p>مگر آری دگر به راهم تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرم از راه شد، به راه آرش</p></div>
<div class="m2"><p>دست من گیر و در پناه آرش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین خیالات بر کنارم کش</p></div>
<div class="m2"><p>پردهٔ عفو پیش کارم کش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با منی درد سر چه میخواهم؟</p></div>
<div class="m2"><p>چو تو دارم دگر چه میخواهم؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کرمت چون ز من بریده نشد</p></div>
<div class="m2"><p>چه ببینم دگر؟ که دیده نشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بی‌خود ار زانکه باختم ندبی</p></div>
<div class="m2"><p>تو به چوب خودم بکن ادبی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با چنین داغ بندگی، که مراست</p></div>
<div class="m2"><p>به سر خود چه گردم از چپ و راست؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از تو گشت استخوان من پر مغز</p></div>
<div class="m2"><p>اگر چه کاری نیامد از من نغز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باد نخوت برون کن از خاکم</p></div>
<div class="m2"><p>متصل کن به عنصر پاکم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روشنم کن چو روز شبخیزان</p></div>
<div class="m2"><p>به شبم زین وجود بگریزان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون بر اندیشم از تو اندر حال</p></div>
<div class="m2"><p>مرغ اندیشه را بریزد بال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو بجویی مرا؟ خیالست این</p></div>
<div class="m2"><p>باز پرسی ز من؟ محالست این</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا حدوث مرا قدم چه کند؟</p></div>
<div class="m2"><p>وان وجود اندرین عدم چه کند؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دیر شد کز دکان گریخته‌ام</p></div>
<div class="m2"><p>و آب رویی، که بود، ریخته‌ام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خجلم من ز بینوایی خویش</p></div>
<div class="m2"><p>شرمسار از گریز پایی خویش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وه! که از کار خود چه تنگدلم!</p></div>
<div class="m2"><p>می‌نمیرم ز غم، چه سنگدلم!</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سود دیدم، سفر به آن کردم</p></div>
<div class="m2"><p>بختم آشفته شد، زیان کردم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دلم از کار تن به جان آمد</p></div>
<div class="m2"><p>هم ز من بر من این زیان آمد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جگرم خون شد از پریشانی</p></div>
<div class="m2"><p>آه! ازین جان سخت پیشانی!</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گشته چندین ورق سیاه از من</p></div>
<div class="m2"><p>من کجا میروم؟ که آه از من!</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تنگدستی چو من چه کار کند؟</p></div>
<div class="m2"><p>تا ازو خود کسی شمار کند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بی‌چراغ تو من به چاه افتم</p></div>
<div class="m2"><p>دست من گیر، تا به راه افتم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جز عطای تو پایمردم نیست</p></div>
<div class="m2"><p>غیر ازین اشک و روی زردم نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از تو عذر گناه می‌خواهم</p></div>
<div class="m2"><p>چون تو گفتی: بخواه، میخواهم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دست حاجت کشیده، سر در پیش</p></div>
<div class="m2"><p>آمدم بر درت من درویش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مگرم رحمت تو گیرد دست</p></div>
<div class="m2"><p>ورنه اسباب ناامیدی هست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چکند عذر پیچ بر پیچم؟</p></div>
<div class="m2"><p>که ز کردار خویش بر هیچم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نتوانستم آنچه فرمودی</p></div>
<div class="m2"><p>بتوانم، به من چو بنمودی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر ببخشی تو، جای آن دارم</p></div>
<div class="m2"><p>ور بسوزی، سزای آن دارم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>غم ما خور، که از غمت شادیم</p></div>
<div class="m2"><p>مهل از دستمان، که افتادیم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر چراغی به راه ما داری</p></div>
<div class="m2"><p>به در آییم ازین شب تاری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ما چه داریم کان ندادهٔ تست؟</p></div>
<div class="m2"><p>چه نهد کس که نانهادهٔ تست؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به عنایت علاج کن رنجم</p></div>
<div class="m2"><p>دستگاهی فرست از آن گنجم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دست و دامن گشاده مییم</p></div>
<div class="m2"><p>مدوان، چون پیاده مییم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون گریزم؟ که پای راهم نیست</p></div>
<div class="m2"><p>چون نشینم؟ که دستگاهم نیست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر چه دانم که نیک بد کردم</p></div>
<div class="m2"><p>چه توان کرد؟ چونکه خود کردم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>قلمی بر سر گناهم کش</p></div>
<div class="m2"><p>راه گم کرده‌ام، براهم کش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گر تو توفیق بندگیم دهی</p></div>
<div class="m2"><p>جاودان خط زندگیم دهی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دل من خوش کن از شمایل خود</p></div>
<div class="m2"><p>گردنم پر کن از حمایل خود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کام من پیش تست، پیشم خوان</p></div>
<div class="m2"><p>خاکپای سگان خویشم خوان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>با وفا عقد کن روانم را</p></div>
<div class="m2"><p>همدم صدق ساز جانم را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دیر شد، ساغر میم درده</p></div>
<div class="m2"><p>که من امشب نمیروم در ده</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>میدوم در پی تو سرگشته</p></div>
<div class="m2"><p>تا به پایان برم سر رشته</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>من ازین دو رهی به آزارم</p></div>
<div class="m2"><p>تو فرستاده‌ای، تو باز آرم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چون نهشتند در سرم مغزی</p></div>
<div class="m2"><p>نغز دانی تو کمتر از نغزی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>عشق و دیوانگی و سرمستی</p></div>
<div class="m2"><p>کرد بازم بدین تهی دستی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از برای تو در تو دارم دست</p></div>
<div class="m2"><p>چون تو باشی، هر آنچه باید هست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کردگارا، به حرمت نیکان</p></div>
<div class="m2"><p>که در آرم به سلک نزدیکان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ریشهٔ آز بر کش از جانم</p></div>
<div class="m2"><p>به نیاز و طمع مرنجانم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از شراب حضور سیرم کن</p></div>
<div class="m2"><p>در نفاذ سخن دلیرم کن</p></div></div>