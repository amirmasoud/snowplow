---
title: >-
    بخش ۱۱ - در صفت سرای معمور
---
# بخش ۱۱ - در صفت سرای معمور

<div class="b" id="bn1"><div class="m1"><p>ای همایون سرای فرخنده</p></div>
<div class="m2"><p>که شد از رونقت طرب زنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاق کسری ز دفترت کسریست</p></div>
<div class="m2"><p>هشت جنت ز گلشنت قصریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاکت از مشک و سنگت از مرمر</p></div>
<div class="m2"><p>بادت از خلد و آبت از کوثر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه پیموده سنگ و بر سخته</p></div>
<div class="m2"><p>بهر فرش تو تخته بر تخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با زر شمسهٔ تو در یاری</p></div>
<div class="m2"><p>لاجورد سپهر زنگاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاشی و آجرت به هر خرده</p></div>
<div class="m2"><p>مال قارون به دم فرو برده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گچ بام تو نه سپهر به دور</p></div>
<div class="m2"><p>از ره کهکشان کشیده به ثور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرده با شاخ گلبنت ز فلک</p></div>
<div class="m2"><p>شاخ طوبی خطاب « طوبی لک»</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقشبندان کن به کنده گری</p></div>
<div class="m2"><p>بر درت کرده عمر خود سپری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در تک این رواق بالنده</p></div>
<div class="m2"><p>پشت ماهی به گاو نالنده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماه ازین طارم زمین مرکز</p></div>
<div class="m2"><p>در دم آفتابت آجر پز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صحن معمورت آستان سپهر</p></div>
<div class="m2"><p>سقف مرفوعت آشیانهٔ مهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون ز سرخاب روی شاهد شنگ</p></div>
<div class="m2"><p>داده سرخاب را جمال تو رنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کار سنگ از تو چون نگار شده</p></div>
<div class="m2"><p>جام با سنگ سازگار شده</p></div></div>