---
title: >-
    بخش ۵۸ - حکایت
---
# بخش ۵۸ - حکایت

<div class="b" id="bn1"><div class="m1"><p>من شنیدم که صاحب دیذی</p></div>
<div class="m2"><p>داشت ناپاکزاده تلمیذی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سالها دیده در سرای سپنج</p></div>
<div class="m2"><p>پر هنر بر سرش مصیبت و رنج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا خرد جمع کرد و دانا شد</p></div>
<div class="m2"><p>هم سخن گوی و هم توانا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه بسیار مال و جاه بیافت</p></div>
<div class="m2"><p>قرب سلطان و عز شاه بیافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون وفا در سرشت و زاد نداشت</p></div>
<div class="m2"><p>حق استاد خود بیاد نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راستان رنج خود تلف کردند</p></div>
<div class="m2"><p>زانکه در کار ناخلف کردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاک تن در وفا تمام آید</p></div>
<div class="m2"><p>بدگهر نا پسند و خام آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که در سیرت وفا شد گرد</p></div>
<div class="m2"><p>ز وفا راه در فتوت برد</p></div></div>