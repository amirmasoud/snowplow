---
title: >-
    بخش ۴۶ - حکایت
---
# بخش ۴۶ - حکایت

<div class="b" id="bn1"><div class="m1"><p>پسری با پدر به زاری گفت</p></div>
<div class="m2"><p>که: مرا یار شو به همسر و جفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت: بابا، زنا کن و زن نه</p></div>
<div class="m2"><p>پند گیر از خلایق، از من نه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در زنا گر بگیردت عسسی</p></div>
<div class="m2"><p>بهلد، کو گرفت چون تو بسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زن بخواهی، ترا رها نکند</p></div>
<div class="m2"><p>ور تو بگذاریش چها نکند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از من و مادرت نگیری پند</p></div>
<div class="m2"><p>چند دیدی و نیز دیدم چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن رها کن که نان و هیمه نماند</p></div>
<div class="m2"><p>ریش بابا بین که: نیمه نماند</p></div></div>