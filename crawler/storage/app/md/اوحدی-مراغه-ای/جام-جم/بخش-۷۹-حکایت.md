---
title: >-
    بخش ۷۹ - حکایت
---
# بخش ۷۹ - حکایت

<div class="b" id="bn1"><div class="m1"><p>آن شیندی که شاه کیخسرو</p></div>
<div class="m2"><p>چون ز معنی بیافت ملکی نو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار این تخت چون ز دست بداد؟</p></div>
<div class="m2"><p>نیستی جست و هر چه هست بداد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پی شاه هر کسی بشتافت</p></div>
<div class="m2"><p>پر بگشتند و کس نشانه نیافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پادشاهی بدان توانایی</p></div>
<div class="m2"><p>با چنان علم و عقل و دانایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست بازی که هم به کاری رفت</p></div>
<div class="m2"><p>که ز تختی چنان بغاری رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کسی بر گهر نیابد دست</p></div>
<div class="m2"><p>نتواند کبود مهره شکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن کسانی که در هنر کوشند</p></div>
<div class="m2"><p>خویش را از نظر چنان پوشند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راه معنی باسب و زین نروند</p></div>
<div class="m2"><p>جز به دل در طریق دین نروند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به هر رشته‌ای در آویزی</p></div>
<div class="m2"><p>کی ازین چاه بر زبر خیزی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند در بند فربهی باشی؟</p></div>
<div class="m2"><p>پر مشو کز هنر تهی باشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این گروه مغفل ساهی</p></div>
<div class="m2"><p>نتوانند با تو همراهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست آزاده‌ای به چنگ آور</p></div>
<div class="m2"><p>روی در روی نام و ننگ آور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کو برون آورد ز غرقابت</p></div>
<div class="m2"><p>برگشاید دو دیده از خوابت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون ازین خانه میروی به درست</p></div>
<div class="m2"><p>به طلب راه را رفیقی چست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا بگوید، چو بازپرسی راست</p></div>
<div class="m2"><p>کندرین راه منزل تو کجاست؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این رباطیست پر ز حجره و رخت</p></div>
<div class="m2"><p>از پس و پیش چند منزل سخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اولش مهد و آخرش تابوت</p></div>
<div class="m2"><p>در میان جستجوی خرقه و قوت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون بزایی، اگر ندانی مرد</p></div>
<div class="m2"><p>کی ازین عرصه گو توانی برد؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خواه اطلس بپوش و خواهی دلق</p></div>
<div class="m2"><p>با خدا باش در میانهٔ خلق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیحضوری مباش و بی‌شوقی</p></div>
<div class="m2"><p>تا بیابی ز جام ما ذوقی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر کرا نفس شد پراگنده</p></div>
<div class="m2"><p>روح قدسیش کی شود زنده؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگذر از ریش و سبلت و بینی</p></div>
<div class="m2"><p>که تو این نیستی که می‌بینی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرد هر در مگرد چون گولان</p></div>
<div class="m2"><p>درج شو در حساب مقبولان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر چه کارت به جای خود نبود</p></div>
<div class="m2"><p>هیچ فارغ مشو، که بد نبود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سرت آغاز اگر کند جستن</p></div>
<div class="m2"><p>نتوان نیز پای را بستن</p></div></div>