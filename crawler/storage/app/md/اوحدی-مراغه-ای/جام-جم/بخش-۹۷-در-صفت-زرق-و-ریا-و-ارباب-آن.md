---
title: >-
    بخش ۹۷ - در صفت زرق و ریا و ارباب آن
---
# بخش ۹۷ - در صفت زرق و ریا و ارباب آن

<div class="b" id="bn1"><div class="m1"><p>سخنی کز سر معامله نیست</p></div>
<div class="m2"><p>عقل را اندرو مجامله نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌رعونت قدم نخواهی زد</p></div>
<div class="m2"><p>بی‌ریا هیچ دم نخواهی زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن نماز دراز کردن تو</p></div>
<div class="m2"><p>وز حرام احتراز کردن تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز بر سفره نان نخوردن سیر</p></div>
<div class="m2"><p>پیش بیگانه شب نخفتن دیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاهی از چل تنان خبر گفتن</p></div>
<div class="m2"><p>گاه از ابدال قصه برگفتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چیست؟ این چیست؟ گر نه زرق و ریاست</p></div>
<div class="m2"><p>راست روراست، گر ز بهر خداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ دانی که کیستند ابدال؟</p></div>
<div class="m2"><p>گر ندانی چرا نمیری لال؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرد غیب از کجا تواند دید؟</p></div>
<div class="m2"><p>آنکه عیب و هجا تواند دید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ز ابدال بوده باشی تو</p></div>
<div class="m2"><p>زانکه ابدال می‌تراشی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیو تست آنکه دیده‌ای از دور</p></div>
<div class="m2"><p>چه کنی دیو خویش را مشهور؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو که کاچی ز رشته نشناسی</p></div>
<div class="m2"><p>دیو نیز از فرشته نشناسی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر بگویی که: چیست در دستم؟</p></div>
<div class="m2"><p>بر نپیچم سر از تو تا هستم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر چنین آتشی چه دود کنی؟</p></div>
<div class="m2"><p>بگریز از میان، که سود کنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر سر راه پادشاه و امیر</p></div>
<div class="m2"><p>مینهی دام و دانه از تزویر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بنشینی خود و دو باز آری</p></div>
<div class="m2"><p>علما را ز خود بیازاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر زمین طعنه: کین گرفتاریست</p></div>
<div class="m2"><p>بر فلک بذله: کان نگونساریست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اختر و چرخ چیست؟ مجبوری</p></div>
<div class="m2"><p>غنصر و طبع چیست؟ مزدوری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه به دانش دل تو گردد نرم</p></div>
<div class="m2"><p>نه سرت را ز خلق و خالق شرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چیست این ترهات بیهوده؟</p></div>
<div class="m2"><p>نقره‌ای بر سر مس اندوده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تاجر از سود و از زیان گوید</p></div>
<div class="m2"><p>کاتب از خط و از بنان گوید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وزرا رای نیک و قربت شاه</p></div>
<div class="m2"><p>امرا شوکت و سلاح و سپاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیر سالوس را بپرسیدم</p></div>
<div class="m2"><p>گفت: من بارها خدا دیدم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آتشم درفتاد از آن نادان</p></div>
<div class="m2"><p>گفتم: ای دل، تو نیک‌تر وادان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اینکه پیغمبرست باری دید</p></div>
<div class="m2"><p>وانکه موسیست نور و ناری دید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شیخکی روز و شب چو خر به چرا</p></div>
<div class="m2"><p>از دو مرسل زیادتست چرا؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر که حال به خویش در بندد</p></div>
<div class="m2"><p>که ندارد، به خویشتن خندد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به تکبر مریز بر کس زهر</p></div>
<div class="m2"><p>گر امام دهی شوی، یا شهر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا به چند از مقام رابعه لاف؟</p></div>
<div class="m2"><p>ای کم ارزن، زنخ مزن به گزاف</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>او زنی بود و گوی مردان برد</p></div>
<div class="m2"><p>هر کسی آن عمل که کرد آن برد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو درم بر سر درم بسته</p></div>
<div class="m2"><p>ما به رخ راه بیش و کم بسته</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو ندانسته سال و مه به خروش</p></div>
<div class="m2"><p>ما بدانسته روز و شب خاموش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اینکه داری تو ما گذاشته‌ایم</p></div>
<div class="m2"><p>زآنچه داری تو شرم داشته‌ایم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ما به گم کردن نشان قدم</p></div>
<div class="m2"><p>تو به نقاشی رواق و حرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر چه چون ما تو پیر میگردی</p></div>
<div class="m2"><p>همچنان گرد میر میگردی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پیش والی ولی چکار کند؟</p></div>
<div class="m2"><p>باشه چون پشه را شکار کند؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اعتماد تو بر چماق امیر</p></div>
<div class="m2"><p>بیش بینم که بر خدای کبیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شیخ کو از امیر گیرد پشت</p></div>
<div class="m2"><p>از خمیرش سبک بر آور مشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تیغ درویش تیغ یزدانیست</p></div>
<div class="m2"><p>تیغ سلطان به شحنه ارزانیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نفس گولست، سر به راهش کن</p></div>
<div class="m2"><p>کل فضولست، بی‌کلاهش کن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دره، کز دست بیگناه افتد</p></div>
<div class="m2"><p>سر قیصر چنان به چاه افتد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا عصای تو اژدها نشود</p></div>
<div class="m2"><p>به دعای تو کس رها نشود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آنکه عون خدای رایت اوست</p></div>
<div class="m2"><p>علم شاه در حمایت اوست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آه ازین ابلهان دیوپرست!</p></div>
<div class="m2"><p>همه از جام دیو ساری مست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر چه داری تو راز خویش نهفت</p></div>
<div class="m2"><p>من درین شهرم و بخواهم گفت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اینکه خود را خموش میدارم</p></div>
<div class="m2"><p>گوشهٔ‌عرصه گوش میدارم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر کسی دیگر این غلط بگذاشت</p></div>
<div class="m2"><p>من بگویم، نگه ندانم داشت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا تو ریش و سری چو ما باشی</p></div>
<div class="m2"><p>جان و دل گرد، تا خدا باشی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گرگ در دشت و شیر در بیشه</p></div>
<div class="m2"><p>همه هم حرفتند و هم پیشه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نه تو دینار داری و من دانگ</p></div>
<div class="m2"><p>به رخ من چرا برآری بانگ؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دو الف یک جهت به بی‌نقطی</p></div>
<div class="m2"><p>این سقط چو نشد؟ آن سری سقطی؟</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو به ریش و به جبه معتبری</p></div>
<div class="m2"><p>اگر آن ریش و اهلی چه بری؟</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گفت بگذار، گردمی باید</p></div>
<div class="m2"><p>در غم عشق مردمی باید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زان چنین در بلا و در بندی</p></div>
<div class="m2"><p>که به تقدیر حق نه خرسندی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بنده‌ای، خیز و رخ به طاعت کن</p></div>
<div class="m2"><p>زآنچه او میدهد قناعت کن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چیست این زرق و شید و حیله و مکر؟</p></div>
<div class="m2"><p>تا دو نان برکنی ز خالد و بکر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زان بر میر و خواجه جای کنی</p></div>
<div class="m2"><p>که توکل نه بر خدای کنی</p></div></div>