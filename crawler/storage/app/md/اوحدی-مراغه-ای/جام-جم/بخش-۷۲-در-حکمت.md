---
title: >-
    بخش ۷۲ - در حکمت
---
# بخش ۷۲ - در حکمت

<div class="b" id="bn1"><div class="m1"><p>حکمت از فکر راست‌بین باشد</p></div>
<div class="m2"><p>در مراعات سر دین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظر اندر صفات حق کردن</p></div>
<div class="m2"><p>به دل اثبات ذات حق کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخنی کان به دل فرو ناید</p></div>
<div class="m2"><p>دان که از حکمتی نکو ناید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نخوانی حکیم دو نان را</p></div>
<div class="m2"><p>گر چه دانند علم یونان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن فعل حکیم و حالش را</p></div>
<div class="m2"><p>بین و آنگه شنومقالش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر زبان حکیم خاموشست</p></div>
<div class="m2"><p>فعل او بین که سربسر هوشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه ازین رو رسول با مردم</p></div>
<div class="m2"><p>گفت: «منی خذوا مناسککم»</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی آن حکمتی ندارد نور</p></div>
<div class="m2"><p>کز کتاب و ز سنت افتد دور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کرا این متاع در بارست</p></div>
<div class="m2"><p>نطق او در زبان و کردارست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیدنش حکمتست و فعل امام</p></div>
<div class="m2"><p>صحبتش رحمت خواص و عوام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت گفتن حکیم را پیداست</p></div>
<div class="m2"><p>کانچه گوید به قدر گوید و راست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به هوا و مجاز دم نزند</p></div>
<div class="m2"><p>در پی آرزو قدم نزند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدهد بر خرد هوا را دست</p></div>
<div class="m2"><p>خرد او کند هوا را پست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حفظ ناموس را کمر بندد</p></div>
<div class="m2"><p>راه سالوس و زرق بربندد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنچه داند نه هشتنی باشد</p></div>
<div class="m2"><p>آنچه گوید نبشتنی باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سیرت رفتگان طریق او را</p></div>
<div class="m2"><p>صفت صادقان رفیق او را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با امل انس کمترش باشد</p></div>
<div class="m2"><p>اجل اندر برابرش باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نشود وقت او به بازی صرف</p></div>
<div class="m2"><p>ننهد بی‌یقین قلم بر حرف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>غم عمر گذشته گیرد پیش</p></div>
<div class="m2"><p>دل ز بهر درم ندارد ریش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شفقت بر جوان و پیر کند</p></div>
<div class="m2"><p>رحم بر منعم و فقیر کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زو دل هیچ کس نیازارد</p></div>
<div class="m2"><p>چون بیازرد، زود باز آرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کوشد اندر تمام دانستن</p></div>
<div class="m2"><p>ننگش آید ز خام دانستن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پر به خواب و خورش هوس نکند</p></div>
<div class="m2"><p>بی‌تواضع نظر به کس نکند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صورت اهل حکمت این باشد</p></div>
<div class="m2"><p>حکما را صفت چنین باشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرنه آنی که در گمان افتی</p></div>
<div class="m2"><p>هرخسی را حکیم چون گفتی؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حکمت آموز و نور حاصل کن</p></div>
<div class="m2"><p>دل خود را به نور واصل کن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر به حکمت رسی سوار شوی</p></div>
<div class="m2"><p>حکما را سپاسدار شوی</p></div></div>