---
title: >-
    بخش ۶ - در ستایش خرد
---
# بخش ۶ - در ستایش خرد

<div class="b" id="bn1"><div class="m1"><p>ای نخستینه فیض عالم جود</p></div>
<div class="m2"><p>اولین نسخهٔ سواد وجود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روح در مکتبت نو آموزی</p></div>
<div class="m2"><p>ابد از مد مدتت روزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان ترا زمین سایه</p></div>
<div class="m2"><p>آفتاب سپهر نه پایه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لنگر کشتی نفوس تویی</p></div>
<div class="m2"><p>مسعد اختر نحوی تویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که دور از تو دور ازو نیکی</p></div>
<div class="m2"><p>وانکه نزد تو، یافت نزدیکی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست راه از تو تا به علت تو</p></div>
<div class="m2"><p>بجز از بیش او و قلت تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر ایجاد علت اولی</p></div>
<div class="m2"><p>نیست بالاتر از تو معلولی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظرت کرده تربیت جان را</p></div>
<div class="m2"><p>یار او کرده نور ایمان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش رخ بسته‌ای، ز قاف به قاف</p></div>
<div class="m2"><p>تتق از زر نگار گوهر باف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوش نه چرخ بر اشارت تست</p></div>
<div class="m2"><p>کاخ هفت اختر از عمارت تست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یزک لشکر وجود تویی</p></div>
<div class="m2"><p>قاید کاروان جود تویی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دین ز حفظ تو پایدار بود</p></div>
<div class="m2"><p>دل ز بوی تو با قرار بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لشکر روح را امیر تویی</p></div>
<div class="m2"><p>همه طفلند خلق و پیر تویی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای ز چرخ و سروش بالاتر</p></div>
<div class="m2"><p>از تو گوهر نزاد والاتر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مددی ده، که دیو رنجم داد</p></div>
<div class="m2"><p>جان من شو، که تن شکنجم داد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کارگاه من از تو بر کارست</p></div>
<div class="m2"><p>تو نباشی، مرا چه مقدارست؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سایهٔ خود مدار دور از من</p></div>
<div class="m2"><p>مبر، ای محض نور، نور از من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به فلک راه ده روانم را</p></div>
<div class="m2"><p>فلکی کن به علم جانم را</p></div></div>