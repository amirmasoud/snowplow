---
title: >-
    بخش ۱۱۸ - در سبب مرگ طبیعی
---
# بخش ۱۱۸ - در سبب مرگ طبیعی

<div class="b" id="bn1"><div class="m1"><p>پیش ازین کردمت ز حال آگاه</p></div>
<div class="m2"><p>که: سه روحند جسم را همراه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار هر یک پدید و مدت کار</p></div>
<div class="m2"><p>وین سخن باز می‌کنم تکرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چهل سال روح روینده</p></div>
<div class="m2"><p>میکند کار در تن بنده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن او باشد اندر افزونی</p></div>
<div class="m2"><p>متفاوت به چندی و چونی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون گذشتی از آن، نبالد تن</p></div>
<div class="m2"><p>هر دم از زحمتی بنالد تن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیکن آثار روح حیوانی</p></div>
<div class="m2"><p>که تو ادراک و جنبشش خوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچنان برقرار خود باشند</p></div>
<div class="m2"><p>بر سر شغل و کار خود باشند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه پیری به قدر کند شوند</p></div>
<div class="m2"><p>گر چه رامند، لیک تند شوند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بدنها رطوبتیست لطیف</p></div>
<div class="m2"><p>منفصل گشته از فضول کثیف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که حیات ترا عزیزی اوست</p></div>
<div class="m2"><p>نشانهٔ قوت غریزی اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن رطوبت چو برقرار بود</p></div>
<div class="m2"><p>زان مزاج تو رطب و حار بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تن به تدبیر نفس انسانی</p></div>
<div class="m2"><p>زنده باشد، چنانکه میدانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون شود در تن آن نظارت کم</p></div>
<div class="m2"><p>بدنت را شود حرارت کم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اندک اندک همی شود زو خرج</p></div>
<div class="m2"><p>تا بپالاید از مشام و ز فرج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کندت قید سردی و خشکی</p></div>
<div class="m2"><p>طرح کافور بر خط مشکی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنچه تحلیل یابد از بدلش</p></div>
<div class="m2"><p>دهدت دست، کم بود خللش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور بدل کم شود شکسته شود</p></div>
<div class="m2"><p>تا حیات از بدن گسسته شود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کند اندر تنت هلاک نزول</p></div>
<div class="m2"><p>نفس نطقیت را کند معزول</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سبب اینست مرگ و مردان را</p></div>
<div class="m2"><p>ضغف و فرتوتی و فسردن را</p></div></div>