---
title: >-
    بخش ۵۰ - در تربیت اولاد
---
# بخش ۵۰ - در تربیت اولاد

<div class="b" id="bn1"><div class="m1"><p>شرم دار، ای پدر، ز فرزندان</p></div>
<div class="m2"><p>ناپسندیده هیچ مپسند آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با پسر قول زشت و فحش مگوی</p></div>
<div class="m2"><p>تا نگردد لئیم و فاحشه گوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بدارش به گفتها آزرم</p></div>
<div class="m2"><p>تا بدارد ز کرده‌های تو شرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بچهٔ خویش را به ناز مدار</p></div>
<div class="m2"><p>نظرش هم ز کار باز مدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به خواری برآید و سختی</p></div>
<div class="m2"><p>نکشد محنت و زبون بختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کارش آموز، تا شود بنده</p></div>
<div class="m2"><p>جور کن، تا شود سر افگنده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدهش دل، که پهلوان گردد</p></div>
<div class="m2"><p>تو شوی پیر و او جوان گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کمانش خری، چو تیر شود</p></div>
<div class="m2"><p>ور کمر یافت، خود اسیر شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ننشیند، سفر کند ز برت</p></div>
<div class="m2"><p>بگدازد ز هجر خود جگرت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر دم آید به روی او خطری</p></div>
<div class="m2"><p>هر زمان آورند ازو خبری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مادر از اشتیاق او میرد</p></div>
<div class="m2"><p>پدر اندر فراق او میرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون هوس کرد پنجه و کشتیش</p></div>
<div class="m2"><p>گر اجازت دهی همی کشتیش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا به جنگیش برند و سر بدهد</p></div>
<div class="m2"><p>یا شود دزد مال و زر بنهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر چه فرزند کشتهٔ تو بود</p></div>
<div class="m2"><p>این بلا دست‌رشتهٔ تو بود</p></div></div>