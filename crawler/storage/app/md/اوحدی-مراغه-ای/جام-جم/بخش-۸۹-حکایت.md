---
title: >-
    بخش ۸۹ - حکایت
---
# بخش ۸۹ - حکایت

<div class="b" id="bn1"><div class="m1"><p>مرشدی را ملامتی افتاد</p></div>
<div class="m2"><p>در مریدان قیامتی افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خصومت میان فرو بستند</p></div>
<div class="m2"><p>وز پی خصم او برون جستند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان مریدان یکی که داناتر</p></div>
<div class="m2"><p>به فنون هنر تواناتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تحمل ز بس تمام که بود</p></div>
<div class="m2"><p>بنجنبید از آن مقام که بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاضری چون دلش شکیبا دید</p></div>
<div class="m2"><p>از وی آن حال را نه زیبا دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت: حقی که در شمار آید</p></div>
<div class="m2"><p>این چنین روز را به کار آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنمریدش جواب داد که: باش</p></div>
<div class="m2"><p>دل خویش و درون ما مخراش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیخ را از من این نباشد چشم</p></div>
<div class="m2"><p>بر من از خامشی نگیرد خشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنج او چون هبا توان کردن</p></div>
<div class="m2"><p>خرقه دیگر قبا توان کردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز چون تخم فتنه پاشد شیخ</p></div>
<div class="m2"><p>با مریدان چه کرده باشد شیخ؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا کسی راسخ و امین نبود</p></div>
<div class="m2"><p>لایق صحبتی چنین نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر تو خواهی که کار دین سازی</p></div>
<div class="m2"><p>بار دنیی ز خود بیندازی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نقش لوح خودی چو بتراشی</p></div>
<div class="m2"><p>قلمش رخ نهد به جماشی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر کند بر تو بی‌ادب انکار</p></div>
<div class="m2"><p>تو بکوش و ادب نگه میدار</p></div></div>