---
title: >-
    بخش ۱۲۴ - در بیان علومی که همراه نفس شوند
---
# بخش ۱۲۴ - در بیان علومی که همراه نفس شوند

<div class="b" id="bn1"><div class="m1"><p>در قیامت کجا رود با نفس؟</p></div>
<div class="m2"><p>علم هر بوالفضول و هر با خفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علم نفسست و عقل و علم‌اله</p></div>
<div class="m2"><p>کز جهان با تو میشود همراه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وین سه علم ار کنی به عقل نظر</p></div>
<div class="m2"><p>از کلام و حدیث نیست به در</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علم کان جز حدیث و قرآنست</p></div>
<div class="m2"><p>سر بسر ساز و آلت نانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان ازین علم نقش گیرد و بس</p></div>
<div class="m2"><p>چه کند علم ترهات و هوس؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاصل این سه علم ارچه بسیست</p></div>
<div class="m2"><p>زود دریابد، ار به خانه کسیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان بسیطست و این سه علم بسیط</p></div>
<div class="m2"><p>تو فرو رفته در وجیز و وسیط</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زینت عقل چیست؟ دانش و داد</p></div>
<div class="m2"><p>شرف نفس؟ خلق خوب نهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین سه هم با تو نقل باید کرد</p></div>
<div class="m2"><p>نفس را نیز عقل باید کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>و آن دو را در میان چو واسطه نیست</p></div>
<div class="m2"><p>به حقیقت دو نیستند، یکیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نداری سر صداع و نبرد</p></div>
<div class="m2"><p>گرد این ثالث ثلاثه مگرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نفس و عقلند کدخدای فلک</p></div>
<div class="m2"><p>زین دو شاید شد آشنای فلک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این دو فرمانده، ار ندانندت</p></div>
<div class="m2"><p>به فلک بر شوی، برانندت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین سه علم آنکه هست بیگانه</p></div>
<div class="m2"><p>ندهندش بر آسمان خانه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر این جا شناختی رستی</p></div>
<div class="m2"><p>ورنه، جان میکن اندرین پستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پی این زاد رو، که زاد اینست</p></div>
<div class="m2"><p>روح را توشهٔ معاد اینست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر که او آشنا نشد با نجم</p></div>
<div class="m2"><p>همچو شیطان کند شهابش رجم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دیو چون استراق سمع کند</p></div>
<div class="m2"><p>آتشش احتراق جمع کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا چو آن آتش اندرو افتد</p></div>
<div class="m2"><p>سر معلق‌زنان فرو افتد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رفتن دیو تا هوا باشد</p></div>
<div class="m2"><p>جای او برفلک کجا باشد؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فلکی چون نبود همراهش</p></div>
<div class="m2"><p>برنیامد کلاه ازین چاهش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو به بادی چو یخ فروبندی</p></div>
<div class="m2"><p>به تفی آخ واخ فرو بندی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون توانی گذشت ازین دو نهنک؟</p></div>
<div class="m2"><p>مگر آنشب که خورده باشی بنک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اعتدال ار ز زر بیاموزی</p></div>
<div class="m2"><p>در اثیر اوفتی، برافروزی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قلب را سوختن یقین باشد</p></div>
<div class="m2"><p>وین اثیر از برای این باشد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نقد آنکس که خالص آمد تفت</p></div>
<div class="m2"><p>از خلاص اثیر بیرون رفت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>راه گردون پر آتش اندازیست</p></div>
<div class="m2"><p>پس تو پنداشتی که بربازیست؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گرنه پیش این زبانه‌ها بودی</p></div>
<div class="m2"><p>آسمان آشیانه‌ها بودی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون سمندر نگشته آتش‌خوار</p></div>
<div class="m2"><p>چون روی بر سپهر آتش بار؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای چو روباه، نزد شیر مرو</p></div>
<div class="m2"><p>پیش او باش حق دلیر مرو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گذرت بر اثیر خواهد بود</p></div>
<div class="m2"><p>راه بر زمهریر خواهد بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سرد و گرم این دم ار نورزی تو</p></div>
<div class="m2"><p>زین بسوزی وزان بلرزی تو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>طاقت هیچ سرد و گرمت نیست</p></div>
<div class="m2"><p>به فلک میروی و شرمت نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا تنت همچو جان نگردد پاک</p></div>
<div class="m2"><p>نتوانی گذشت بر افلاک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون شود جمع نور با سایه</p></div>
<div class="m2"><p>چه سپهر و چه نردبان پایه؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آنکه از آب و خاک مایه نداشت</p></div>
<div class="m2"><p>برفلک شد، که هیچ سایه نداشت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سایه زایل شود چو نور آمد</p></div>
<div class="m2"><p>غیب بگریخت چون حضور آمد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر کرا عقل و روح دایه بود</p></div>
<div class="m2"><p>تن او را کدام سایه بود؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نور بر سایه چون زیادت شد</p></div>
<div class="m2"><p>غیب در کسوت شهادت شد</p></div></div>