---
title: >-
    بخش ۱ - سر آغاز
---
# بخش ۱ - سر آغاز

<div class="b" id="bn1"><div class="m1"><p>قل هوالله لامره قد قال</p></div>
<div class="m2"><p>من له‌الحمد دائما متوال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>احد غیر واجب باحد</p></div>
<div class="m2"><p>صمد لم یلد و لم یولد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه هست اسم اعظمش مطلق</p></div>
<div class="m2"><p>حی و قیوم نزد زمرهٔ حق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه بی‌نام او نگشت تمام</p></div>
<div class="m2"><p>نامهٔ ذوالجلال و الاکرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه فوقیتش مکانی نیست</p></div>
<div class="m2"><p>وآنکه کیفیتش نشانی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه بیرون ز جوهر و عرضست</p></div>
<div class="m2"><p>وآنکه فارغ ز صحت و مرضست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه تا بود یار و جفت نداشت</p></div>
<div class="m2"><p>وآنکه تا هست خورد و جفت نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه زاب سفید و خاک سیاه</p></div>
<div class="m2"><p>صنع او آفتاب سازد و ماه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه مغزست و این دگرها پوست</p></div>
<div class="m2"><p>وآنکه چون نیک بنگری همه اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنکه او خارج از عبارت ماست</p></div>
<div class="m2"><p>ذات او فارغ از اشارت ماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست انگشت را به حرفش راه</p></div>
<div class="m2"><p>مگر از لا اله الالله</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خرد ادراک ذات او نکند</p></div>
<div class="m2"><p>فکر ضبط صفات او نکند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دور و نزدیک و آشکار و نهان</p></div>
<div class="m2"><p>کردگار جهانیان و جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه کروبیان عالم غیب</p></div>
<div class="m2"><p>سر فرو برده زین دقیقه به جیب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر چه کرد و کند به هر دو سرا</p></div>
<div class="m2"><p>کس ندارد مجال چون و چرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از حدیث چه و چگونه و چند</p></div>
<div class="m2"><p>هستیش کرده بر زبانها بند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای منزه کمالت از کم و کاست</p></div>
<div class="m2"><p>هر چه دور از هدایت تو نه راست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>راز پنهان آفرینش تو</p></div>
<div class="m2"><p>نتوان دید جز ببینش تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در نهان نهان نهفته رخت</p></div>
<div class="m2"><p>در عیان همچو گل شکفته رخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خالق هر چه بود و هست تویی</p></div>
<div class="m2"><p>آنکه بگشود وانکه بست تویی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بنبستی دری که نگشودی</p></div>
<div class="m2"><p>هستی امروز و باشی و بودی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از عدم در وجود میری</p></div>
<div class="m2"><p>پیش خود در سجود میری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ندهی،نعمت تو بیشی هست</p></div>
<div class="m2"><p>بدهی، عادت توپیشی هست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ما چه پوشیم؟ اگر نپاشی تو</p></div>
<div class="m2"><p>چه خوریم؟ ار مدد نباشی تو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نتوانیم گفت و نیست شکی</p></div>
<div class="m2"><p>شکر نعمت ز صد هزار یکی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کس خبردار کنه ذات تو نیست</p></div>
<div class="m2"><p>فکر کس واقف صفات تو نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عرش کم در بزرگواری تو</p></div>
<div class="m2"><p>فرش در موکب عماری تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای تو بیچون، چگونه دانندت؟</p></div>
<div class="m2"><p>چیستی؟بر چه اسم خوانندت؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عقل ذات تو را چه نام نهد؟</p></div>
<div class="m2"><p>فکرت اینجا چگونه گام نهد؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیستت جای، در چه جایی تو؟</p></div>
<div class="m2"><p>همه زان تو خود، کرایی تو؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قدرتت در عدد نمی‌گنجد</p></div>
<div class="m2"><p>قدر در رسم و حد نمیگنجد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رخت از نور خود درآورده</p></div>
<div class="m2"><p>پیش دلها هزار و یک پرده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دل ز بوی تو بوی جان شنود</p></div>
<div class="m2"><p>جان چه گوی؟ ترا همان شنود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رحمتت دایمست و پاینده</p></div>
<div class="m2"><p>لایزال از تو خیر زاینده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چونکه ذات تو بیکران باشد</p></div>
<div class="m2"><p>کس چه گوید ترا که آن باشد؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه به ذات تو اسم در گنجد</p></div>
<div class="m2"><p>نه به گنجت طلسم در گنجد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بسمو تو چون نپیوندیم</p></div>
<div class="m2"><p>سمت و اسم بر تو چون بندیم؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون نبیند کسی تمام ترا</p></div>
<div class="m2"><p>چون بداند که چیست نام ترا؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اسم را نار در زند نورت</p></div>
<div class="m2"><p>چه طلسمی؟ که چشم بد دورت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ذات و اسم تو هر دو ناپیداست</p></div>
<div class="m2"><p>عقل در جستن تو هم شیداست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اوحدی، این سخن نه بر سازست</p></div>
<div class="m2"><p>او پدیدار و دیده‌ها بازست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سخن عشق کم خریدارست</p></div>
<div class="m2"><p>ورنه معشوق بس پدیدارست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نیست، گر نیک بنگری حالی</p></div>
<div class="m2"><p>در جهان ذره‌ای ازو خالی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در تو و دیدن تو خیری نیست</p></div>
<div class="m2"><p>ورنه در کاینات غیری نیست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بشناسش که او چه باشد و چیست؟</p></div>
<div class="m2"><p>تا بدانی که رویت اندر کیست؟</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دوست نادیده دست بر چه نهی؟</p></div>
<div class="m2"><p>رقم بود و هست بر چه نهی؟</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اندرین ره تو پردهٔ کاری</p></div>
<div class="m2"><p>هم تو باشی، که پرده برداری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گر چه هست این حکایت اندر پوست</p></div>
<div class="m2"><p>ما نخواهیم جز حکایت دوست</p></div></div>