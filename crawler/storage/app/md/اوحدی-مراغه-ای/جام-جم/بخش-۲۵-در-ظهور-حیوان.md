---
title: >-
    بخش ۲۵ - در ظهور حیوان
---
# بخش ۲۵ - در ظهور حیوان

<div class="b" id="bn1"><div class="m1"><p>باز چون در مزاج این ارکان</p></div>
<div class="m2"><p>متضاعف شد اعتدال و توان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قوت و حس و جنبش به مراد</p></div>
<div class="m2"><p>مدد روح رستنیها داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جسم چون زین دو روح یاری یافت</p></div>
<div class="m2"><p>بر حیات و روش سواری یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حرکت کرد بر زمین چپ و راست</p></div>
<div class="m2"><p>رستنی خورد و خواب و راحت خواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین میان ماده گشت و نر پیدا</p></div>
<div class="m2"><p>وز پی ماده گشت نر شیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماده و نر به هم چو جفت شدند</p></div>
<div class="m2"><p>در تمنای خیز و خفت شدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ز تولیدشان جهان پر گشت</p></div>
<div class="m2"><p>کوه و صحرا و غار و وادی و دشت</p></div></div>