---
title: >-
    بخش ۲۳ - در ترتیب ظهور موالید ثلاثه اول در صفت معدن
---
# بخش ۲۳ - در ترتیب ظهور موالید ثلاثه اول در صفت معدن

<div class="b" id="bn1"><div class="m1"><p>جرم خورشید گرد پیکر خاک</p></div>
<div class="m2"><p>مدتی چون بگشت با افلاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب و خاکش ز عکس تافته شد</p></div>
<div class="m2"><p>تبش اندر دو گانه یافته شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>متصاعد شد از میان دو بخار</p></div>
<div class="m2"><p>که دو روحند و در هوا طیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روح خاکی کثیف بود و نژند</p></div>
<div class="m2"><p>روح آبی لطیف و نیز بلند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روح آبی چو در مشیمهٔ کان</p></div>
<div class="m2"><p>محتبس گشت ز اقتضای زمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روش آفتاب تابش داد</p></div>
<div class="m2"><p>حرکت کرد و اضطرابش داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر هوا رفت و آب شد، بچکید</p></div>
<div class="m2"><p>بر زمین گرم گشت و پس بتپید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان صعود و هبوط پیوسته</p></div>
<div class="m2"><p>گشت اجزاش روشن و بسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمره‌ای روح مطلقش گفتند</p></div>
<div class="m2"><p>فرقه‌ای دهن و زیبقش گفتند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روح خاکی چو پس دخانی بود</p></div>
<div class="m2"><p>وندرو اندکی گرانی بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به یکی معدن احتباسش کرد</p></div>
<div class="m2"><p>جنبش خویش در حراسش کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تپشی دایم اندرو پیوست</p></div>
<div class="m2"><p>راه بیرون شدش نبود، ببست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون بسی روزگارش این شد ورد</p></div>
<div class="m2"><p>در گوکان فتاد و شد گوگرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قدما نفس نام کردندش</p></div>
<div class="m2"><p>حکما احترام کردندش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ذکر این نفس و روح راز نهفت</p></div>
<div class="m2"><p>شد به جسمی غبار معدن جفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روح و نفس و بدن مهیا شد</p></div>
<div class="m2"><p>کارگاهی ز خاک پیدا شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نوبتی دیگر از حرارت کان</p></div>
<div class="m2"><p>گرم گشت این سه جزو را ارکان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شد ز حر مقام و ضیق محل</p></div>
<div class="m2"><p>عقد آن در رطوبت این حل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وین سه را در زمان پیوستن</p></div>
<div class="m2"><p>گاه پیمان و دوستی بستن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وزن و قدر ار به اعتدال بود</p></div>
<div class="m2"><p>تن مصفا و جان زلال بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>و گر آن آب چون حجر گردد</p></div>
<div class="m2"><p>به مرور زمانه زر گردد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ور بود وزن زیبق افزون‌تر</p></div>
<div class="m2"><p>نقره‌ای باشد و نگردد زر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور ز مساوات و وزن این دو بخار</p></div>
<div class="m2"><p>تیره باشد ز اختلاط غبار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نام جسمی چنین حدید بود</p></div>
<div class="m2"><p>وین پس از مدتی مدید بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور ظلمت عدیم نور شدند</p></div>
<div class="m2"><p>وز مساوات و وزن دور شدند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زان تمازج به مذهب هر مس</p></div>
<div class="m2"><p>جسد قلع و سرب خیزد و مس</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وآنچه ملح و شبوب و زاجاتند</p></div>
<div class="m2"><p>هم ز تاثیر این مزاجاتند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هم چنین از دریچهای دگر</p></div>
<div class="m2"><p>حال و حکم نتیجهای دگر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا شد این خاک پر گهر گنجی</p></div>
<div class="m2"><p>خلق نامبرده بر یکی رنجی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اصل و بنیاد این جواهر خاک</p></div>
<div class="m2"><p>این دو روحند، با تو گفتم پاک</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وین جمیع ار نفیس و گردونند</p></div>
<div class="m2"><p>زادهٔ اختران گردونند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زین میان زر بود نتیجهٔ مهر</p></div>
<div class="m2"><p>نقره فرزند ماه زیبا چهر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مس و آهن ز زهره و بهرام</p></div>
<div class="m2"><p>بهره‌مندند و نور یاب مدام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>قلع از مشتری و جیوه ز تیر</p></div>
<div class="m2"><p>زحل اندر سرب کند تاثیر</p></div></div>