---
title: >-
    بخش ۵۶ - در بیرونقی شعر و کساد آن
---
# بخش ۵۶ - در بیرونقی شعر و کساد آن

<div class="b" id="bn1"><div class="m1"><p>شاعری چیست؟ بر در دونان</p></div>
<div class="m2"><p>خانهٔ کرد و حکمت یونان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ثناشان دریغ باشد رنج</p></div>
<div class="m2"><p>طبع را دادن عذاب و شکنج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خفته ممدوح مست با خاتون</p></div>
<div class="m2"><p>تو به مدحش ز دیده ریزان خون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب کنی روز و روز در کارش</p></div>
<div class="m2"><p>در نویسی به درج طومارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راویی چست را کنی همدست</p></div>
<div class="m2"><p>سرش از جام وعده سازی مست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا روی پیش او سلام کنی</p></div>
<div class="m2"><p>شعر خوانی، سخن تمام کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او خطابت کند که: خوش گفتی</p></div>
<div class="m2"><p>در معنی به مدح ما سفتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقد را باز گرد و کاری کن</p></div>
<div class="m2"><p>بار دیگر بما گذاری کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زو چو آن بشنوی برون ایی</p></div>
<div class="m2"><p>خود ندانی ز غم که چون آیی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز شعریش بر ترنگانی</p></div>
<div class="m2"><p>به تقاضا قلم بلنگانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون بیایی به وعده باز برش</p></div>
<div class="m2"><p>بسته یابی بسان سنگ درش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل دربان بلا به نرم کنی</p></div>
<div class="m2"><p>بر خود او را به آقچه گرم کنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا ترا پیش او چو راه کند</p></div>
<div class="m2"><p>او به دربان ترش نگاه کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کای: خر قلتبان، قرار این بود؟</p></div>
<div class="m2"><p>آنچه گفتم هزار بار این بود؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بار دادی، چه روز این بارست؟</p></div>
<div class="m2"><p>من بکارم، چه وقت این کارست؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس نپرسیده: کای پدر چونی؟</p></div>
<div class="m2"><p>چیست حالت؟ ز درد سر چونی؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بنویسد برات بر جایی</p></div>
<div class="m2"><p>کز سه خروار ادا کند تایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خود ز این خواجگان مدخل کیست؟</p></div>
<div class="m2"><p>که فزون باشدش عطا از بیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیست را چون غریم ده ببرد</p></div>
<div class="m2"><p>پنج راوی ز نیمه ره ببرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو بمانی و برده ماهی رنج</p></div>
<div class="m2"><p>بیستت ده شده، دهت شده پنج</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سر بواب را بنتوان بست</p></div>
<div class="m2"><p>ز جراحت چو میر گردد مست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مده، ای فاضل، آب رخ بر باد</p></div>
<div class="m2"><p>که خدا این جهان بر آب نهاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز آسمان رسته شد سخن را بیخ</p></div>
<div class="m2"><p>به زمینش فرو مبر، چون میخ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به خرمند خرده دانش ده</p></div>
<div class="m2"><p>ز دل آمد برون، به جانش ده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زین نهاد انوری چه کرد قیاس؟</p></div>
<div class="m2"><p>رتبت شاعران پس از کناس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرورانی که پیش ازین ایام</p></div>
<div class="m2"><p>سعی کردند در بلندی نام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر چه در فضل بودشان پیشی</p></div>
<div class="m2"><p>شعرا را به همت از بیشی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گنجها در کنار میکردند</p></div>
<div class="m2"><p>تا ستایش گزان می‌کردند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>منکه خلوت نشین این گنجم</p></div>
<div class="m2"><p>در جهانی چنین کجا گنجم؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا بکی زین گروه ننگ خورم؟</p></div>
<div class="m2"><p>نان اینان بهل، که سنگ خورم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون ز حرصم حکایتی بنماند</p></div>
<div class="m2"><p>ز سپهرم شکایتی بنماند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در رخ او چو پسته خندانم</p></div>
<div class="m2"><p>گر چه از پست میدهد نانم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زین میان کاش دوستی بودی!</p></div>
<div class="m2"><p>که برو نیمه پوستی بودی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در جهان دوستی به دست نشد</p></div>
<div class="m2"><p>که ازو در دلم شکست نشد</p></div></div>