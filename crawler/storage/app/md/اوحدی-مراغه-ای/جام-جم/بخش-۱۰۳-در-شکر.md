---
title: >-
    بخش ۱۰۳ - در شکر
---
# بخش ۱۰۳ - در شکر

<div class="b" id="bn1"><div class="m1"><p>شکر کن، تا شکر مذاق شوی</p></div>
<div class="m2"><p>نام کفران مبر، که عاق شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غایت شکر چیست؟ دانستن</p></div>
<div class="m2"><p>حق یک شکر نا توانستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکر ما گر رسد به هفت اورنگ</p></div>
<div class="m2"><p>پیش انعام او نیارد سنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نعمتش را سپاسداری کن</p></div>
<div class="m2"><p>زو زیادت بخواه و زاری کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به شکر و ثبات میل بود</p></div>
<div class="m2"><p>کامهای دگر طفیل بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانکه در شکر اگر نکوشی تو</p></div>
<div class="m2"><p>کم شراب مزید نوشی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم به تن شکر استطاعت کن</p></div>
<div class="m2"><p>هم بدل شکر این بضاعت کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکر دل رحمت و خلوص و رضاست</p></div>
<div class="m2"><p>دیدن عجز از آنکه شکر خداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکر تن خدمت و تحمل و صبر</p></div>
<div class="m2"><p>کار کردن به اختیار و به جبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دل و تن چو شکر گردد راست</p></div>
<div class="m2"><p>به زبان عذر آن بباید خواست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر ز دانش در قبول زنی</p></div>
<div class="m2"><p>دست در دامن رسول زنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیگر آن را لوای شکری هست</p></div>
<div class="m2"><p>خواجه دارد لوای حمد به دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه شد چشم او به منعم باز</p></div>
<div class="m2"><p>جان او برکشد به حمد آواز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>و آنکه از نعمتش گذر نکند</p></div>
<div class="m2"><p>جز به شکرش زبان بدر نکند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خویشتن را متابع او ساز</p></div>
<div class="m2"><p>کو ترا بشنواند این آواز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر شود خاطرت خطاب شنو</p></div>
<div class="m2"><p>بشنود هر زمان خطابی نو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این خطابت نیاید اندر گوش</p></div>
<div class="m2"><p>تا نبخشی به مصطفی دل و هوش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لهجهٔ او اگر بیابی باز</p></div>
<div class="m2"><p>راه یابی به کار خانهٔ راز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در شناساست این سخن را روی</p></div>
<div class="m2"><p>نشناسی، هر آنچه خواهی گوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سر به مهرست سر این پاکان</p></div>
<div class="m2"><p>از برای ضمیر دراکان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دیو را نیست تاختن بر گول</p></div>
<div class="m2"><p>که ازو دور نیست چنبر غول</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پای دانندگان به بند آرد</p></div>
<div class="m2"><p>سر بیدار در کمند آرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از دم و دام این نهنگ خلاص</p></div>
<div class="m2"><p>جز به توفیق نیست، یا اخلاص</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کوش تا بی‌حضور دل نروی</p></div>
<div class="m2"><p>تا ز کردار خود خجل نروی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اندرین پرده بار دل دارد</p></div>
<div class="m2"><p>پی دل رو، که کار دل دارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عقل دل را به علم بنگارد</p></div>
<div class="m2"><p>علم جان را بر آسمان آرد</p></div></div>