---
title: >-
    بخش ۶۵ - حکایت
---
# بخش ۶۵ - حکایت

<div class="b" id="bn1"><div class="m1"><p>شیخکی بر فسانه بود وگزاف</p></div>
<div class="m2"><p>چشم بر هم نهاده میزد لاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حدیثی دلیل خواستمش</p></div>
<div class="m2"><p>حرمت و آب رخ بکاستمش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مریدان او مریدی خر</p></div>
<div class="m2"><p>به غضب گفت: ازین سخن بگذر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او دلیلست ازو دلیل مخواه</p></div>
<div class="m2"><p>شرح گردون ز جبرییل مخواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه گوید به گوش دل بشنو</p></div>
<div class="m2"><p>ور جدل میکنی به مدرسه رو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نظر کردم آن غضب کوشی</p></div>
<div class="m2"><p>تن نهادم به عجز و خاموشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نه تسلیم کردمی در حال</p></div>
<div class="m2"><p>مرغ ریش مرا نهشتی بال</p></div></div>