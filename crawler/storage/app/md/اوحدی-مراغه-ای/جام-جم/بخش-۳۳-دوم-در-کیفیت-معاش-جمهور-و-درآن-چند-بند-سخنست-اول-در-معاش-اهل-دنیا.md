---
title: >-
    بخش ۳۳ - دوم در کیفیت معاش جمهور و درآن چند بند سخنست اول در معاش اهل دنیا
---
# بخش ۳۳ - دوم در کیفیت معاش جمهور و درآن چند بند سخنست اول در معاش اهل دنیا

<div class="b" id="bn1"><div class="m1"><p>نوبهارست و روز عیش امروز</p></div>
<div class="m2"><p>بهل این اضطراب و طیش امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت یاریست، دوستان دستی</p></div>
<div class="m2"><p>جای رحمست بر چنان مستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه جای غمست، غم نخوریم</p></div>
<div class="m2"><p>دست بر هم زنیم و در گذریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش دستان، که پیش ازین بودند</p></div>
<div class="m2"><p>یکدم از درد سر نیسودند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بتو هشتند منزلی آباد</p></div>
<div class="m2"><p>تا ازیشان کنی به نیکی یاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانچه هست ار بهش ندانی کرد</p></div>
<div class="m2"><p>جهد کن تا بهش توانی خورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیرت آن گذشتگان بشنو</p></div>
<div class="m2"><p>چون شنیدی بنه اساسی نو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش زمینیست، در عمارت کوش</p></div>
<div class="m2"><p>حاصل رنج خود بپاش و بپوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این عمارت به عدل شاید کرد</p></div>
<div class="m2"><p>بیشتر رخ به عدل باید کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر کسی را به قدر ملکی هست</p></div>
<div class="m2"><p>که بدان ملک حکم دارد و دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاه در کشور و ملک در شهر</p></div>
<div class="m2"><p>هر یکی دارد از حکومت بهر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نه از معدلت خطاب کنند</p></div>
<div class="m2"><p>دان که آن ملک را خراب کنند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پادشاهی تو هم به مسکن خویش</p></div>
<div class="m2"><p>بلکه در هستی خود و تن خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اندرین ملک پادشاهی خود</p></div>
<div class="m2"><p>ثبت کن نام بیگناهی خود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی‌حسابی مکن، بهانه مجوی</p></div>
<div class="m2"><p>که حسابت کنند موی به موی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه عدلش نمیرود در خواب</p></div>
<div class="m2"><p>ملک او را مکن به ظلم خراب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که درین خانه بی‌وقار شوی</p></div>
<div class="m2"><p>اندر آن خانه شرمسار شوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این سخن راز اوحدی بر رس</p></div>
<div class="m2"><p>که به جز اوحدی نداند کس</p></div></div>