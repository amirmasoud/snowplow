---
title: >-
    بخش ۹۰ - در ترک و تجرید
---
# بخش ۹۰ - در ترک و تجرید

<div class="b" id="bn1"><div class="m1"><p>بی‌درم باش، ارت سرد نیست</p></div>
<div class="m2"><p>کاولین گام عاشقان اینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این ده و باغ و بچه وزن تو</p></div>
<div class="m2"><p>غول راهند و غل گردن تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غل و غولی چنین گذاشته به</p></div>
<div class="m2"><p>داشت چون بد بود، نداشته به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل که وحدت سرای این راهست</p></div>
<div class="m2"><p>پاک دارش،که خلوت شاهست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی دل جز در آن یگانه مکن</p></div>
<div class="m2"><p>مرغ دینی، هوای دانه مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در و دیوار در شمار تواند</p></div>
<div class="m2"><p>انجم و آسمان بکار تواند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با تو گویا زبان هر ذره</p></div>
<div class="m2"><p>که: به دنیا چنین مشو غره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملک دین را تو راست میکن کار</p></div>
<div class="m2"><p>ملک دنیا به کاردان بگذار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند ازین نیستی و این هستی؟</p></div>
<div class="m2"><p>ازل اندر ابد زن و رستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشقی، هم به تاب تیشهٔ خود</p></div>
<div class="m2"><p>آتشی در فگن به بیشهٔ خود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خرد را فسار و سوزن اندر جیب</p></div>
<div class="m2"><p>چون روی در سراچهٔ لاریب؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا ترا از تو شیشه در بارست</p></div>
<div class="m2"><p>از تو تا دوست راه بسیارست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آشنایی طلب، ز دنیا فرد</p></div>
<div class="m2"><p>که درین بحر غوطه داند خورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا تو داری خبر ز هستی خود</p></div>
<div class="m2"><p>میل داری به بت‌پرستی خود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیده بازت نشد به عالم نور</p></div>
<div class="m2"><p>زان به ظلمت فروشدستی دور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دیده بازت نشد به عالم غیب</p></div>
<div class="m2"><p>زان به ظلمت فرو نشستی و عیب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ره که باید به پای جان رفتن</p></div>
<div class="m2"><p>با خر و بار چون توان رفتن؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو دل خود چو ده خراب کنی</p></div>
<div class="m2"><p>که در سنگ و خاک آب کنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خانه را در مکن، که در بندست</p></div>
<div class="m2"><p>وندرو زر منه، که زر گندست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نام زر چیست؟ جیفهٔ مردار</p></div>
<div class="m2"><p>کی خورد جیفه جز سگ و کفتار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بخت اگر نیست خواجه زر چکند؟</p></div>
<div class="m2"><p>رخت اگر نیست خانه در چکند؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرد از آراستن تباه شود</p></div>
<div class="m2"><p>سینه از خواستن سیاه شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عارف کردگار زر چکند؟</p></div>
<div class="m2"><p>ولی‌الله بار و خر چکند؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من ده خویش پربها کردم</p></div>
<div class="m2"><p>به فضولان ده رها کردم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در جهان داد بندگیش نداد</p></div>
<div class="m2"><p>که ز بند جهان نگشت آزاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو ز لاهوتی، ای الهی دل</p></div>
<div class="m2"><p>ملک ناسوت را بناس بهل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا کی این سنقر و ایاز رهی؟</p></div>
<div class="m2"><p>برهان خویش را، که باز رهی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرغ او آشیانه کی سازد؟</p></div>
<div class="m2"><p>مور او کی به دانه پردازد؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>غیر در غار ما نمی‌گنجد</p></div>
<div class="m2"><p>عشوه در بار ما نمی‌گنجد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>غار ما منزل پلنگانست</p></div>
<div class="m2"><p>نه مقام خسان و ننگانست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنکه اندر جهان ندارد گنج</p></div>
<div class="m2"><p>چون توان آگنیدنش در کنج؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تشنگان اندرین حیاض رسند</p></div>
<div class="m2"><p>به ریاضت درین ریاض رسند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عزلت و جوع بود و صمت و سهر</p></div>
<div class="m2"><p>سالکان را به راستی رهبر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>این چهارند در طریق کمال</p></div>
<div class="m2"><p>حالت فقر و حیلت ابدال</p></div></div>