---
title: >-
    بخش ۱۲۷ - در معارج ارواح و ابدان و عذاب ایشان
---
# بخش ۱۲۷ - در معارج ارواح و ابدان و عذاب ایشان

<div class="b" id="bn1"><div class="m1"><p>ورندارد ز دین و دانش بهر</p></div>
<div class="m2"><p>از تنش جان جدا کنند به قهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جهان جای او حجیم بود</p></div>
<div class="m2"><p>آبش از جرعهٔ حمیم بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنگ ماند برو جهان فراخ</p></div>
<div class="m2"><p>رخ فرا میکند به هر سوراخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد او دودهای ظلمانی</p></div>
<div class="m2"><p>از مزاجات جهل و نادانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او در آن دودهای آتش ریز</p></div>
<div class="m2"><p>میرود چشم بسته، افتان خیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عور ماند، که پرده در بودست</p></div>
<div class="m2"><p>خوار ماند، که عشوه‌گر بودست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه رود با روان غمناکان</p></div>
<div class="m2"><p>گه درآید به گور ناپاکان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هوا بر شود، بسوزندش</p></div>
<div class="m2"><p>بر زمین بگذرد، بدوزندش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کور و در دست او عصایی نه</p></div>
<div class="m2"><p>عور و بر دوش او کسایی نه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن او قوت مار و طعمهٔ مور</p></div>
<div class="m2"><p>او همی بین و میگذر از دور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه ز پس راه یابد و نه ز پیش</p></div>
<div class="m2"><p>نه به بیگانه در رسد، نه به خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رخ به راه آورد، قفاش زنند</p></div>
<div class="m2"><p>باز گردد، به صد جفاش زنند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه گریزندگیش را پایی</p></div>
<div class="m2"><p>نه ستیزندگیش را رایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان او در تموز و یخ‌بندان</p></div>
<div class="m2"><p>زنده، لیکن فتاده در زندان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل او بی‌ضیا و نور و فروغ</p></div>
<div class="m2"><p>گوش او بر گزاف و فحش و دروغ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ظلمت ظلم بر وی اندوده</p></div>
<div class="m2"><p>چرک بر چرک و دوده بر دوده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تهمت و جهل و حسرت و خواری</p></div>
<div class="m2"><p>فرقت و گمرهی و بی‌یاری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرده پهنای خاک تنگ برو</p></div>
<div class="m2"><p>چرخ باریده شوک و سنگ برو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جانش از نور علم عاری و عور</p></div>
<div class="m2"><p>تن ز ظلمت بمانده در گل گور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زان و حل قوت گذشتن نه</p></div>
<div class="m2"><p>به عمل راه باز گشتن نه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرد بر گرد او ز مظلمه‌ها</p></div>
<div class="m2"><p>برقهای جهنده از دمه‌ها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صحبتش با بدان و نیکی نه</p></div>
<div class="m2"><p>سر او پر خمار و سیکی نه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کارش از دست رفته، سر در پیش</p></div>
<div class="m2"><p>دیده احوال خویش و رفته ز خویش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون در آید سرش ز غفلت نوم</p></div>
<div class="m2"><p>بشناسد که : «لیس ظلم الیوم»</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دوزخ نقد مفسدان اینست</p></div>
<div class="m2"><p>نسیه خور صد هزار چندینست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این چنین مرگ مرگ عام بود</p></div>
<div class="m2"><p>وینچنین مرده ناتمام بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روح ازین گنبدش بدر نشود</p></div>
<div class="m2"><p>بلکه زین چاه بر زبر نشود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روی تحقیق ازو نهان گردد</p></div>
<div class="m2"><p>آرزومند این جهان گردد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر به یک چند در لباس خیال</p></div>
<div class="m2"><p>اندر آید به خواب اهل و عیال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بنماید به عجز صورت خویش</p></div>
<div class="m2"><p>عرضه دارد همی ضرورت خویش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا بدانند جنس رازش را</p></div>
<div class="m2"><p>معنی حاجت و نیازش را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دو سه نانش به زور بفرستند</p></div>
<div class="m2"><p>یا چراغی به گور بفرستند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بعد ازو گر یکی ز صد بدهند</p></div>
<div class="m2"><p>صدقات آن بود که خود بدهند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هرچه بیش از کفاف داری تو</p></div>
<div class="m2"><p>ندهی، بر گزاف داری تو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پیش از آن کت اجل کند در خواب</p></div>
<div class="m2"><p>خویشتن را به زندگی دریاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا نباید بلابه و زاری</p></div>
<div class="m2"><p>مال خود خواستن بدین خواری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حق ایزد نداده‌ای به خوشی</p></div>
<div class="m2"><p>تا مکافات آن چنین بکشی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از تو کرد او به صد زبان خواهش</p></div>
<div class="m2"><p>تو ندادی به گوش خود راهش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اهل حاجت که داری از چپ و راست</p></div>
<div class="m2"><p>لب ایشان بدان زبان گویاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حق و ادرار خویش میطلبند</p></div>
<div class="m2"><p>نه ز انصاف بیش میطلبند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شکر انعام او به دانش کن</p></div>
<div class="m2"><p>نظری هم به بندگانش کن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آنچه بینی که دون و بدکارند</p></div>
<div class="m2"><p>بر ایزد نه روزیی دارند؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر چنینش خوری، رسی به صواب</p></div>
<div class="m2"><p>ور نه بعد از تو خود خورند اصحاب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بتو پیش از تو گر زری دادند</p></div>
<div class="m2"><p>دان که از بهر دیگری دادند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر تو دادیش یافتی جنت</p></div>
<div class="m2"><p>ور نه او خود ربود بی‌منت</p></div></div>