---
title: >-
    بخش ۱۰۷ - در عشق
---
# بخش ۱۰۷ - در عشق

<div class="b" id="bn1"><div class="m1"><p>عشق و دل را یک اختیار بود</p></div>
<div class="m2"><p>عقل و جان را دویی حصار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آستان عقل پیشتر نرود</p></div>
<div class="m2"><p>عشق خود ز آشیان بدر نرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بال دل چیست؟ عشق دیوانه</p></div>
<div class="m2"><p>بند جان کیست؟ عقل فرزانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق دیوانه را چو برخوانند</p></div>
<div class="m2"><p>عقل فرزانه را بدر مانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که عاشق نشد تمام نگشت</p></div>
<div class="m2"><p>وانکه در عشق پخت خام نگشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همره عشق شو، که یار اینست</p></div>
<div class="m2"><p>در پی عشق رو، که کار اینست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل ورزی، ز کار سرد شوی</p></div>
<div class="m2"><p>عشق ورز، ای پسر، که مرد شوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میل صورت به شهوتست و هوس</p></div>
<div class="m2"><p>میل معنی به عشق باشد و بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل شمعست اندرین خانه</p></div>
<div class="m2"><p>مرد در پای عشق پروانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق خواند ترا به عالم محو</p></div>
<div class="m2"><p>عقل گوید ز فقه و منطق و نحو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سینه را عشق چاک داند کرد</p></div>
<div class="m2"><p>نفس را عشق پاک داند کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تبش نور کبریا عشقست</p></div>
<div class="m2"><p>آتش خرمن ریا عشقست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشق برقیست کام سوزنده</p></div>
<div class="m2"><p>وز تمامی تمام سوزنده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عشق را روی در هلاک بود</p></div>
<div class="m2"><p>هر کرا عشق نیست خاک بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا ز هستیت شمه‌ای برجاست</p></div>
<div class="m2"><p>نتوان راه عشق رفتن راست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بندهٔ رنج باش و راحت بین</p></div>
<div class="m2"><p>دفتر عشق خوان، فصاحت بین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرد عاشق ز عشق گویا شد</p></div>
<div class="m2"><p>گل ببین کو ز گل چه بویا شد؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جدل و بحث لاولن دگرست</p></div>
<div class="m2"><p>ناطق عشق را سخن دگرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هوس از صورتی گذر نکند</p></div>
<div class="m2"><p>عشق در هر دو شان نظر نکند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عشق را از هوس نمیدانی</p></div>
<div class="m2"><p>لاجرم «بشر» و «هند» میخوانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عقل جویان بود سکونت را</p></div>
<div class="m2"><p>عشق برهم زند رعونت را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رخ او کس به خود نداند دید</p></div>
<div class="m2"><p>عشق بیخود رخش تواند دید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آسمانها به عشق میگردند</p></div>
<div class="m2"><p>اختران نیز در همین دردند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عشق جام تو و شراب تو بس</p></div>
<div class="m2"><p>عاشقی محنت و عذاب تو بس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر ازین بوته خالص آید مرد</p></div>
<div class="m2"><p>نرسد دوزخش دو اسبه به گرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرمی از عشق جوی، اگر مردی</p></div>
<div class="m2"><p>هر که عاشق نشد، زهی سردی!</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عشق روی و ز نخ نمیگویم</p></div>
<div class="m2"><p>با تو از برف و یخ نیمگویم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عشق آن شاهدان بالایی</p></div>
<div class="m2"><p>که کندشان سپهر لالایی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دلبری جوی و پای بندش باش</p></div>
<div class="m2"><p>آتشی بر کن و سپندش باش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خیز و جامی ز دست مادر کش</p></div>
<div class="m2"><p>تا ببینی جمال وقتی خوش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر چه کوتاه دیدهٔ بامم</p></div>
<div class="m2"><p>دور کن سنگ طعنه از جامم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>راه باریک و وقت بیگاهست</p></div>
<div class="m2"><p>رو بگردان، که چاه در راهست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جام ما را مده به بد مستان</p></div>
<div class="m2"><p>ور دهد نیز دست بد، مستان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عشقداری و پای جنبش هست</p></div>
<div class="m2"><p>منشین، دست یارگیر به دست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرد در راه عشق مرد نشد</p></div>
<div class="m2"><p>تا لگد کوب گرم و سرد نشد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سخن عاشقان به حال بود</p></div>
<div class="m2"><p>نه به آواز و قیل و قال بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر چه در خط و در بیان آید</p></div>
<div class="m2"><p>دست بیگانه در میان آید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو مگو: چون ز دل به دل راهست؟</p></div>
<div class="m2"><p>کانکه دل دارد از دل آگاهست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دل چو نعل اندر آتش اندازد</p></div>
<div class="m2"><p>عرش را در کشاکش اندازد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همت دل کمند عاشق بس</p></div>
<div class="m2"><p>یاد معشوق بند عاشق بس</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دیگر، ای مرغ دل، به پرواز آی</p></div>
<div class="m2"><p>در چه اندیشه رفته‌ای، باز آی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سخنی کش به راز باید گفت</p></div>
<div class="m2"><p>چون بهر جای باز شاید گفت؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چیست گفتن چو اشک داری و آه</p></div>
<div class="m2"><p>قاضی عشق را بس این دو گواه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>من و ما تا بچند دشمن و دوست؟</p></div>
<div class="m2"><p>بس ازین بیخودی خود همه اوست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چند گویی که: شیشه بشکستی</p></div>
<div class="m2"><p>کی بود کار جام بی‌مستی؟</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جد و جهدی بکار می‌باید</p></div>
<div class="m2"><p>هر کرا وصل یار می‌باید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همه محرومی از نجستن تست</p></div>
<div class="m2"><p>بی‌بری از گزاف رستن تست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عاشق بی‌طلب چه کرد کند؟</p></div>
<div class="m2"><p>مرد باید، که کار مرد کند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>درد ما را به مرغ و ماش چکار؟</p></div>
<div class="m2"><p>عاشقان را به نان و آش چکار؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نظر دل چو بر جمال بود</p></div>
<div class="m2"><p>عشق خوانند و عشق حال بود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا نخوانی مقالتی در عشق</p></div>
<div class="m2"><p>نکنی وجد و حالتی در عشق</p></div></div>