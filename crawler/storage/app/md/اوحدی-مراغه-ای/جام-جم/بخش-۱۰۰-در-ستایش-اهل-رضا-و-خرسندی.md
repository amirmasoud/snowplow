---
title: >-
    بخش ۱۰۰ - در ستایش اهل رضا و خرسندی
---
# بخش ۱۰۰ - در ستایش اهل رضا و خرسندی

<div class="b" id="bn1"><div class="m1"><p>حبذا! مفلسان آواره</p></div>
<div class="m2"><p>جامه و جان پاره در پاره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم بیشی ز دل به در کرده</p></div>
<div class="m2"><p>به کمی سوی خود نظر کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دلی زنده و تنی مرده</p></div>
<div class="m2"><p>رخت در کوچهٔ ابد برده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با چنان دیدهٔ تر و لب خشک</p></div>
<div class="m2"><p>نفسی خوش زدن چو نافهٔ مشک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلشان هم شکسته، هم خندان</p></div>
<div class="m2"><p>وز زبان لب گرفته در دندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه پنهان کند حکایت دوست</p></div>
<div class="m2"><p>لب او وانگهی شکایت دوست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راز او را ز خود چه میپوشند؟</p></div>
<div class="m2"><p>چون به مشهور کردنش کوشند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دل آتش نهاده چون لاله</p></div>
<div class="m2"><p>غنچه‌وش لب به بسته از ناله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل پر از درد و روی در وادی</p></div>
<div class="m2"><p>بسته بر دوش زاد بی‌زادی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهر نوشان بی‌ترش رویی</p></div>
<div class="m2"><p>تلخ عیشان بی‌تبه گویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر بلایی رسد ز عالم خشم</p></div>
<div class="m2"><p>بر بلای دگر نهند دو چشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل خوشند ار چه در گذار استند</p></div>
<div class="m2"><p>تا مبادا که در دیار استند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نفس چون شد مفارق از پیوند</p></div>
<div class="m2"><p>بر تن او چه راحت و چه گزند؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در خرابی چو گنج پوشیده</p></div>
<div class="m2"><p>جام صد درد و رنج نوشیده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیش زهرهٔ خروش کراست؟</p></div>
<div class="m2"><p>یاره این فغان و جوش کراست؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه گردن نهاده‌اند به حکم</p></div>
<div class="m2"><p>لب ز گفتار بسته، صم بکم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر که آهنگ این بیان کرده</p></div>
<div class="m2"><p>هیبتش قفل بر زبان کرده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عارفان را بداغ کل لسان</p></div>
<div class="m2"><p>کرده مشغول ازین فسون و فسان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حکمتش راه طعنهٔ چه و چون</p></div>
<div class="m2"><p>بسته بر فهم کند و دانش دون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لب خاصان به مهر خاموشی</p></div>
<div class="m2"><p>تو به گفتار هرزه میکوشی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر چه باشد در آن حضورت بار</p></div>
<div class="m2"><p>هم طریق ادب نگه میدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخن اینجا به راز شاید گفت</p></div>
<div class="m2"><p>کان ببینی که باز شاید گفت</p></div></div>