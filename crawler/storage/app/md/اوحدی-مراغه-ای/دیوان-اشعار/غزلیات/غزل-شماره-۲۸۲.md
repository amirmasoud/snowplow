---
title: >-
    غزل شمارهٔ ۲۸۲
---
# غزل شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>از در ما چو در آمد، اثر ما بنماند</p></div>
<div class="m2"><p>این دل و دین و تن و جان و سر و پا بنماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم آن فتنهٔ پیدا به دلم پوشیده</p></div>
<div class="m2"><p>نظری کرد، که پوشیده و پیدا بنماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن عشق، که عقلم به معما می‌خواند</p></div>
<div class="m2"><p>بر دلم کشف چنان شد که معما بنماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیلت ما همه حالت شد و حیلتها سوخت</p></div>
<div class="m2"><p>حالت ما همه معنی شد و اسما بنماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دو می‌دید دلم در کف یغما بودم</p></div>
<div class="m2"><p>چون برستم ز دویی زحمت یغما بنماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل من دردی آن درد به دریا نوشید</p></div>
<div class="m2"><p>به طریقی که نم در همه دریا بنماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای تمنای دل من ز دو گیتی نظرت</p></div>
<div class="m2"><p>نظری کن، که دگر هیچ تمنا بنماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه از هر جهتم سری و سودایی بود</p></div>
<div class="m2"><p>جهت سر تو بگرفتم و سودا بنماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوش با درد تو گقتم که: محابا کن، گفت:</p></div>
<div class="m2"><p>اوحدی، تن به قضاده، که محابا بنماند</p></div></div>