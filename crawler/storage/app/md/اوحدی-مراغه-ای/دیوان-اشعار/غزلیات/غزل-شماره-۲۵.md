---
title: >-
    غزل شمارهٔ ۲۵
---
# غزل شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>به خرابات برید از در این خانه مرا</p></div>
<div class="m2"><p>که دگر یاد شراب آمد و پیمانه مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل دیوانه به زنجیر نبستن عجبست</p></div>
<div class="m2"><p>که به زنجیر ببندد دل دیوانه مرا؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می بیارید و تنم را بنشانید چو شمع</p></div>
<div class="m2"><p>پیش آن شمع و بسوزید چو ویرانه مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو گنجیست درین عالم ویران رخ او</p></div>
<div class="m2"><p>یاد آن گنج دوانید به ویرانه مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر میان از سر زلفش کمری می‌بستم</p></div>
<div class="m2"><p>گر بدو دست رسیدی چو سرشانه مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که خواهد که به دامم کشد آسان آسان</p></div>
<div class="m2"><p>گو: مپندار به جز خال لبش دانه مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرم از شوق و دل از عشق چنین شیفته شد</p></div>
<div class="m2"><p>تا که شد اوحدی شیفته هم خانه مرا</p></div></div>