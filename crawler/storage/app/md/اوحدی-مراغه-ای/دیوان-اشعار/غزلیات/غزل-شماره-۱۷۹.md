---
title: >-
    غزل شمارهٔ ۱۷۹
---
# غزل شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>شاهد من در جهان نظیر ندارد</p></div>
<div class="m2"><p>بوی سر زلف او عبیر ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو بدین قد خوش خرام نروید</p></div>
<div class="m2"><p>ماه چنان طلعت منیر ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابروی همچون کمان بسیست ولیکن</p></div>
<div class="m2"><p>هیچ کس آن قامت چو تیر ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهر، که در حسن پادشاه نجومست</p></div>
<div class="m2"><p>هیات آن روی مستنیر ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طفل چنین در کنار دایهٔ دنیا</p></div>
<div class="m2"><p>مادر دور سپهر پیر ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عنبر سارا بهل، که نافهٔ چینی</p></div>
<div class="m2"><p>نکهت آن زلف همچو قیر ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی اندر فراق عارض خوبش</p></div>
<div class="m2"><p>چاره به جز ناله و نفیر ندارد</p></div></div>