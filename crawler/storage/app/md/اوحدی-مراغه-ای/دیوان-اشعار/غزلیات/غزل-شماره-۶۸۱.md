---
title: >-
    غزل شمارهٔ ۶۸۱
---
# غزل شمارهٔ ۶۸۱

<div class="b" id="bn1"><div class="m1"><p>دل به تو دادیم و شکستی، برو</p></div>
<div class="m2"><p>سینهٔ ما را چو بخستی ، برو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد دل از پیش تو می‌خواستم</p></div>
<div class="m2"><p>چون بت بیداد پرستی، برو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز ز سر عربده داری و جنگ</p></div>
<div class="m2"><p>هیچ نگویم که: تو مستی، برو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیستی از همچو منی در جهان</p></div>
<div class="m2"><p>سهل بود، چون که تو هستی، برو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آمده بودم که نشینی دمی</p></div>
<div class="m2"><p>چون ز تکبر ننشستی، برو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گم شده بودم که: بجویی مرا</p></div>
<div class="m2"><p>چونکه نجستی و بخستی، برو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی شیفته در دام تست</p></div>
<div class="m2"><p>گر تو ازین دام بجستی، برو</p></div></div>