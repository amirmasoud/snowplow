---
title: >-
    غزل شمارهٔ ۵۴۲
---
# غزل شمارهٔ ۵۴۲

<div class="b" id="bn1"><div class="m1"><p>به تازه باد جدایی گلی ببرد ز باغم</p></div>
<div class="m2"><p>که همچو بلبل مسکین از آن به درد و به داغم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر حدیث مشوش کنم بدیع نباشد</p></div>
<div class="m2"><p>که از فراق عزیزان مشوشست دماغم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا مبر به تفرج، مکن حدیث تماشا</p></div>
<div class="m2"><p>که بر جمال رخ او، نه مرد گلشن و راغم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چراغ خویش به آتش گرفتمی همه وقتی</p></div>
<div class="m2"><p>چه آتشست جدایی؟ کزان بمرد چراغم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آنزمان که ببستند باغ وصل ترا در</p></div>
<div class="m2"><p>نه میل بود به صحرا، نه دل کشید به باغم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همیشه با دل فارغ نشستمی من و اکنون</p></div>
<div class="m2"><p>خیال روی تو فرصت نمی‌دهد به فراغم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو اوحدی گرو از بلبلان اگر چه ببردم</p></div>
<div class="m2"><p>ز هجرت، ای گل رنگین، زبان گرفته چو زاغم</p></div></div>