---
title: >-
    غزل شمارهٔ ۲۰۸
---
# غزل شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>چو دل شد زان او هرگز نمیرد</p></div>
<div class="m2"><p>چو خورد از خوان او هرگز نمیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سر می‌گردم از عشقش، چو دانم</p></div>
<div class="m2"><p>که سرگردان او هرگز نمیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن عاشق بمیرد در جدایی</p></div>
<div class="m2"><p>ولیکن جان او هرگز نمیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دردش گر دلم زین پیش می‌مرد</p></div>
<div class="m2"><p>پس از درمان او هرگز نمیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنم را پر شود پیمانهٔ عمر</p></div>
<div class="m2"><p>ولی پیمان او هرگز نمیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زندان عزیزی در شد این دل</p></div>
<div class="m2"><p>که در زندان او هرگز نمیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روان اوحدی را هست حکمی</p></div>
<div class="m2"><p>که بی‌فرمان او هرگز نمیرد</p></div></div>