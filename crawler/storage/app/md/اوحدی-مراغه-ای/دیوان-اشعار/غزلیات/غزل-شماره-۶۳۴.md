---
title: >-
    غزل شمارهٔ ۶۳۴
---
# غزل شمارهٔ ۶۳۴

<div class="b" id="bn1"><div class="m1"><p>چون مرا غمناک بیند شاد گردد یار من</p></div>
<div class="m2"><p>زان سبب شادی نمی‌گردد به گرد کار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشک چشمم سر دل یک یک به رخ‌ها بر نبشت</p></div>
<div class="m2"><p>گوییا با اشک بیرون می‌رود اسرار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت ازین شهرم به صحرا برد می‌باید که شب</p></div>
<div class="m2"><p>مردم اندر زحمتند از نالهٔ بسیار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نه آب چشم سیل‌انگیز من مانع شود</p></div>
<div class="m2"><p>هر شبی شهری بسوزد آه آتشبار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو یاقوتست اشکم، تا خیال لعل او</p></div>
<div class="m2"><p>آشنایی می‌کند با دیدهٔ بیدار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ز تیمارش چنان گشتم که نتوان گفت و او</p></div>
<div class="m2"><p>خود نمی‌پرسد که: حالت چیست؟ ای بیمار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اوحدی هجران او کوتاه کردی دست زود</p></div>
<div class="m2"><p>گر به گوش او رسیدی ناله‌های زار من</p></div></div>