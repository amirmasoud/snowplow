---
title: >-
    غزل شمارهٔ ۷۸۲
---
# غزل شمارهٔ ۷۸۲

<div class="b" id="bn1"><div class="m1"><p>زهی! زلف و رخت قدری و عیدی</p></div>
<div class="m2"><p>قمر حسن ترا کمتر معیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه خوبان عالم را بدیدم</p></div>
<div class="m2"><p>بر آن طوبی ندارد کس مزیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مراد چرخ ازرق جامه آنست</p></div>
<div class="m2"><p>که باشد آستانت را مریدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآن درگه بمیرم، بس عجب نیست</p></div>
<div class="m2"><p>به کوی شاهدی گور شهیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گنجی می‌خرم وصل ترا، گر</p></div>
<div class="m2"><p>ز کنجی بر نیاید من یزیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی در گردنت گویی بدیدم</p></div>
<div class="m2"><p>دو دست خویش چون حبل الوریدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مستوری ز مستان رخ مگردان</p></div>
<div class="m2"><p>که بعد از وعده نپسندم وعیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر آحادی چه داند سر عشقت؟</p></div>
<div class="m2"><p>که همچون اوحدی باید وحیدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر غافل نشد جان تو از عشق</p></div>
<div class="m2"><p>ز دل پرداز او بر خوان نشیدی</p></div></div>