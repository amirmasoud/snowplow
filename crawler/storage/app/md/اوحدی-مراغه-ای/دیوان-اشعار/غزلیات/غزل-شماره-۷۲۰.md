---
title: >-
    غزل شمارهٔ ۷۲۰
---
# غزل شمارهٔ ۷۲۰

<div class="b" id="bn1"><div class="m1"><p>می‌نالم ازین کار به سامان نرسیده</p></div>
<div class="m2"><p>وین درد جگر سوز به درمان نرسیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا، سخنست این همه سوراخ ببینید</p></div>
<div class="m2"><p>بر سینهٔ این کشتهٔ پیکان نرسیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افسوس! که موری نشکستیم درین خاک</p></div>
<div class="m2"><p>وین قصه به نزدیک سلیمان نرسیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ترک پری‌چهره، چه بیداد و جفا ماند؟</p></div>
<div class="m2"><p>کز کافر چشمت به مسلمان نرسیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خوان تو برخاسته یغمای طفیلی</p></div>
<div class="m2"><p>زان گونه که یک لقمه به مهمان نرسیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شک نیست که این چشم چو دریا نگذارد</p></div>
<div class="m2"><p>در شهر یکی خانهٔ توفان نرسیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زود اوحدی اندر سخن خود برساند</p></div>
<div class="m2"><p>آوازهٔ این جور به سلطان نرسیده</p></div></div>