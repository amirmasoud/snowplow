---
title: >-
    غزل شمارهٔ ۱۵۱
---
# غزل شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>ترک من ترک من خسته‌دل زار گرفت</p></div>
<div class="m2"><p>شد دگرگونه دگری یار گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این که در کار بلای دل ما می‌کوشید</p></div>
<div class="m2"><p>اثر قول حسودست که برکار گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل من آینهٔ صورت او بود و ز غم</p></div>
<div class="m2"><p>آه می‌کردم و آن آینه زنگار گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه عجب خرقهٔ پرهیزم اگر پاره شود</p></div>
<div class="m2"><p>بدرد دامن هر گل که درین خار گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ز خاک در او میل سفر می‌نکنم</p></div>
<div class="m2"><p>نبود بر من مسکین، که گرفتار گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوی این درد، که امسال به همسایه رسید</p></div>
<div class="m2"><p>ز آتشی بود که در خرمن من پار گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای صبا، از چمن وصل نسیمی برسان</p></div>
<div class="m2"><p>که ازین خانهٔ تنگم دل بیمار گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با دل فارغ او زاری من سود نداشت</p></div>
<div class="m2"><p>گر چه سوز سخنم در در و دیوار گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوحدی خوار گرفت از غم و من می‌گفتم:</p></div>
<div class="m2"><p>خوار گردد که سخن‌های چنین خوار گرفت</p></div></div>