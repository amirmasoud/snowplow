---
title: >-
    غزل شمارهٔ ۱۹۸
---
# غزل شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>دلم جز تو آهنگ یاری نکرد</p></div>
<div class="m2"><p>به غیر از تو میل کناری نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به طرف چمن در خزانی نرفت</p></div>
<div class="m2"><p>تماشای گل در بهاری نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به راه تو بر هیچ خاکی ندید</p></div>
<div class="m2"><p>که از اشک بر وی نثاری نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی را که با رویت افتاد مهر</p></div>
<div class="m2"><p>چو مه را بدید، اعتباری نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آنها که دل مدخلی می‌کنند</p></div>
<div class="m2"><p>بجز دوستیت اختیاری نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لبت پیش ما هیچ شغلی ندید</p></div>
<div class="m2"><p>که از محنتش پود و تاری نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبی در فراقت نکردیم روز</p></div>
<div class="m2"><p>که با ما جهان کار زاری نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمودی که: رویم چه کرد از جفا؟</p></div>
<div class="m2"><p>وفایی که جستیم باری نکرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرامنده قدی چنان دلنواز</p></div>
<div class="m2"><p>چه معنی که بر ما گذاری نکرد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگوید کسی شکر ایام عمر</p></div>
<div class="m2"><p>کزان لعل شیرین شکاری نکرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز نوشیدنی‌ها می وصل تست</p></div>
<div class="m2"><p>که نوشندگان را خماری نکرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خیال تو پیش من آمد شبی</p></div>
<div class="m2"><p>ولی نیم ساعت قراری نکرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل اوحدی تکیه بر عمر داشت</p></div>
<div class="m2"><p>خود او نیز بگذشت و کاری نکرد</p></div></div>