---
title: >-
    غزل شمارهٔ ۲۰۱
---
# غزل شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>ای سنگدل، به حق وفا کز وفا مگرد</p></div>
<div class="m2"><p>عهدی بکن به وصل و از آن عهد وا مگرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما برگزیده‌ایم ترا از جهان، تو نیز</p></div>
<div class="m2"><p>پیوند ما گزین وز پیوند ما مگرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میغ خون دل شب هجر آشنای ما</p></div>
<div class="m2"><p>می‌بین و با مخالف ما آشنا مگرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آنکه یک دم از دل ما نیستی جدا</p></div>
<div class="m2"><p>هر دم به شیوه‌ای دگر از ما جدا مگرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی: برو، که مهرهٔ مهرت بریختم</p></div>
<div class="m2"><p>خونم بریز و گرد چنین مهرها مگرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی دست در میان تو کردم، رخ تو گفت:</p></div>
<div class="m2"><p>بالای ما بلاست، به گرد بلا مگرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را غرض روا شدن از وصل روی تست</p></div>
<div class="m2"><p>گو: کام اوحدی ز دو گیتی روا مگرد</p></div></div>