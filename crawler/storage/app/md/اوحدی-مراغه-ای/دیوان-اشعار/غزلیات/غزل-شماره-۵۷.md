---
title: >-
    غزل شمارهٔ ۵۷
---
# غزل شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>پس از مشقت دوشین که داشت گوش امشب؟</p></div>
<div class="m2"><p>که من به کام رسم زان لب چو نوش امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشیده‌ایم بسی‌بار چرخ، وقت آمد</p></div>
<div class="m2"><p>که چرخ غاشیهٔ ما کشد به دوش امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیار، ساقی، از آن جام راوقی، تا من</p></div>
<div class="m2"><p>در افگنم به رواق فلک خروش امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیال خوب مبند، ای دل امشبی و مخسب</p></div>
<div class="m2"><p>تو نیز جهد کن، ای دیده و بکوش امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خانقاه دلم سیر شد، برای خدای</p></div>
<div class="m2"><p>مرا مبر ز سرکوی می‌فروش امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شراب حاضر و معشوق مست و من عاشق</p></div>
<div class="m2"><p>ز من مدار توقع به عقل و هوش امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ترک نام کن، ای اوحدی وخرمن ننگ</p></div>
<div class="m2"><p>بیار باده و بنشین و باده نوش امشب</p></div></div>