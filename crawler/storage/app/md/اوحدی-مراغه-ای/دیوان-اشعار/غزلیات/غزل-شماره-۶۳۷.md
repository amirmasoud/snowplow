---
title: >-
    غزل شمارهٔ ۶۳۷
---
# غزل شمارهٔ ۶۳۷

<div class="b" id="bn1"><div class="m1"><p>هر شب ز عشق روی تو این چشم لعبت باز من</p></div>
<div class="m2"><p>در خون نشیند، تا کند چون روز روشن راز من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دیده گر در پیش دل سیلی نرفتی هر نفس</p></div>
<div class="m2"><p>آتش به جانم در زدی این آه برق‌انداز من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من شرح دل پرداز خود برخی فرستم پیش تو</p></div>
<div class="m2"><p>لیکن تو کمتر میکنی گوشی به دل پرداز من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بالم به سنگ سر کشی بشکستی ای سیمین بدن</p></div>
<div class="m2"><p>ورنه کجا خالی شدی کوی تو از پرواز من؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برخاستی تا: خون من در پای خود ریزی دگر</p></div>
<div class="m2"><p>ای آرزوی دل، دمی بنشین و بنشان آز من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پروانه‌وارم سوختی، ای شمع وز رخسار تو</p></div>
<div class="m2"><p>نه پرتوی بر حال دل، نه بوسه‌ای در گاز من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بس که نالد اوحدی در حسرت دیدار تو</p></div>
<div class="m2"><p>پر شد جهان ز آوازه عشق بلند آواز من</p></div></div>