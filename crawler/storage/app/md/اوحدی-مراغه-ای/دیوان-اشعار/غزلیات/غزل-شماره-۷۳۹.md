---
title: >-
    غزل شمارهٔ ۷۳۹
---
# غزل شمارهٔ ۷۳۹

<div class="b" id="bn1"><div class="m1"><p>یارب! تو دوش با که به شادی نشسته‌ای؟</p></div>
<div class="m2"><p>کامروز بی‌غم از در ما باز جسته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روی عشوه بند قبا را گشاده باز</p></div>
<div class="m2"><p>وز راه شیوه طرف کله بر شکسته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیم از میان ببرده و در کیسه ریخته</p></div>
<div class="m2"><p>زر در کمر کشیده و بر کوه بسته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز گو: شکفته مشو هیچ گل، که تو</p></div>
<div class="m2"><p>صد گلبن شکفته و صد لاله دسته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل نقل نیست باده بنه، کز دهان و لب</p></div>
<div class="m2"><p>یک خانه شهد و شکر و عناب و پسته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برخیز و شمع را بنشان، یا بهل چنان</p></div>
<div class="m2"><p>تا شمع بیندت که چنین خوش نشسته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر دیگری ز حسرت او غصه میخورد</p></div>
<div class="m2"><p>ای اوحدی، تو باری ازین غصه راسته‌ای</p></div></div>