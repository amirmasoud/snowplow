---
title: >-
    غزل شمارهٔ ۴۰۷
---
# غزل شمارهٔ ۴۰۷

<div class="b" id="bn1"><div class="m1"><p>منم غریب دیار تو، ای غریب‌نواز</p></div>
<div class="m2"><p>دمی به حال غریب دیار خود پرداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر کمند که خواهی بگیر و بازم بند</p></div>
<div class="m2"><p>به شرط آنکه ز کارم نظر نگیری باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرم چو خاک زمین خوار می‌کنی سهلست</p></div>
<div class="m2"><p>چو خاک می‌کن و بر خاک سایه می‌انداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درون سینه دلم چون کبوتران بتپد</p></div>
<div class="m2"><p>چه آتشست که در جان من نهادی باز؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوای قد بلند تو می‌کند دل من</p></div>
<div class="m2"><p>تو دست کوته من بین و آرزوی دراز!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آستین خیالت همی دهم بوسه</p></div>
<div class="m2"><p>بر آستان وصالت مرا چو نیست جواز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار دیده به روی تو ناظرند و تو خود</p></div>
<div class="m2"><p>نظر به روی کسی بر نمی‌کنی از ناز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر بسوزدت، ای دل، ز درد ناله مکن</p></div>
<div class="m2"><p>دم از محبت او می‌زنی، بسوز و بساز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حدیث درد من، ای مدعی، نه امروزست</p></div>
<div class="m2"><p>که اوحدی ز ازل بود رند و شاهد باز</p></div></div>