---
title: >-
    غزل شمارهٔ ۲۱۹
---
# غزل شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>هر سحرم ز هجر تو ناله بر آسمان رسد</p></div>
<div class="m2"><p>گر تو جفا چنین کنی، از تو دلم به جان رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مایهٔ روزگار خود در هوس تو باختم</p></div>
<div class="m2"><p>سود تو می‌بری، بهل کز تو مرا زیان رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیر کمان ابروان بر سپرم مزن، که من</p></div>
<div class="m2"><p>در جگرش نهان کنم، تیر کزان کمان رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز تو لاله‌رخ دلم ناله کند، روا بود</p></div>
<div class="m2"><p>دل چو شود ز غصه پر، هم به سر زبان رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخت دل شکسته را پیش تو می‌هلم، ولی</p></div>
<div class="m2"><p>بدرقه گر تویی، سبک دزد به کاروان رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ستمی منال، اگر عاشقی آن جمال را</p></div>
<div class="m2"><p>بار چنین بسی بری، تا فرحی چنان رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر کار عاشقان نیست به جز هلاک و خود</p></div>
<div class="m2"><p>بر دل ریش اوحدی، گر تو تویی، همان رسد</p></div></div>