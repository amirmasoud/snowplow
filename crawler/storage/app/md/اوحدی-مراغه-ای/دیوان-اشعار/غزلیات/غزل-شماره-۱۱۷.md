---
title: >-
    غزل شمارهٔ ۱۱۷
---
# غزل شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>پیداست حال مردم رند، آن چنان که هست</p></div>
<div class="m2"><p>خرم دلی که فاش کند هر نهان که هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌خواره گنج دارد و مردم بر آن که: نه</p></div>
<div class="m2"><p>زاهد نداشت چیزی و ما را گمان که هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مؤمن ز دین برآمد و صوفی ز اعتقاد</p></div>
<div class="m2"><p>ترسا محمدی شد و عاشق همان که هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سود جهان به مردم عاقل بده، که من</p></div>
<div class="m2"><p>از بهر عاشقی بکشم هر زیان که هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلقی نشان دوست طلب می‌کنند و باز</p></div>
<div class="m2"><p>از دوست غافلند به چندین نشان که هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای محتسب، تو دانی و شرع و اساس آن</p></div>
<div class="m2"><p>قانون عشق را بگذار آن چنان که هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای آنکه یاد من نرود بر زبان تو</p></div>
<div class="m2"><p>از بهر یاد تست مرا این زبان که هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نامرد را مراد بهشتست ازان جهان</p></div>
<div class="m2"><p>ما را مراد روی تو از هر جهان که هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر گفته‌اند: نیست مرا با تو دوستی</p></div>
<div class="m2"><p>مشنو ز بهر من سخن دشمنان، که هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیچاره آنکه خاک کف پای دوست نیست</p></div>
<div class="m2"><p>ای من غلام خاک کف پای آن که هست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آشفته را گواه نباشد به عاشقی</p></div>
<div class="m2"><p>زنگ رخش ز دور ببین و بدان که هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر زانکه اوحدی سگ تست، از درش مران</p></div>
<div class="m2"><p>او را بهر لقب که تو دانی بخوان که هست</p></div></div>