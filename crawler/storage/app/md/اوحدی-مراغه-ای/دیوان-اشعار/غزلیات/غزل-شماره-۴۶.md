---
title: >-
    غزل شمارهٔ ۴۶
---
# غزل شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>رخ خوب خویشتن را به چه پوشی از نظرها؟</p></div>
<div class="m2"><p>که به حسرت تو رفتن بدو دیده خاک درها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برت آمدیم یک دم، ز برای دست بوسی</p></div>
<div class="m2"><p>چو ملول گشتی از ما، ببریم درد سرها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو به ناز خفته هرشب، ز منت خبر نباشد</p></div>
<div class="m2"><p>که زخون دیده گریم ز غمت به رهگذرها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب آمدم که: بعضی ز تو غافلند، مردم</p></div>
<div class="m2"><p>مگر از ره بصارت خللیست در بصرها؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نتوانم از خجالت که: بر تو آورم جان</p></div>
<div class="m2"><p>که شنیدم: التفاتی نکنی به مختصرها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز لبت نبات خیزد، چو به خنده برگشایی</p></div>
<div class="m2"><p>بهل این شکر فروشی، که بسوختی جگرها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آن کمان ابرو دل اوحدی چه سنجد؟</p></div>
<div class="m2"><p>که به زخم تیر مژگان بشکافتی سپرها</p></div></div>