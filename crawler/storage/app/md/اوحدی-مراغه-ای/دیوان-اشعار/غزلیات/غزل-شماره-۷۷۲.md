---
title: >-
    غزل شمارهٔ ۷۷۲
---
# غزل شمارهٔ ۷۷۲

<div class="b" id="bn1"><div class="m1"><p>جان را ستیزهٔ تو ندارد نهایتی</p></div>
<div class="m2"><p>خوبان جفا کنند ولی تا به غایتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگین دلی، و گرنه چنین درد سینه سوز</p></div>
<div class="m2"><p>در سینهٔ تو نیز بکردی سرایتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم شکایت از تو، ولی منع میکند</p></div>
<div class="m2"><p>حسن وفا که: باز نمایم شکایتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی زمین چو قصهٔ فرهاد کوهکن</p></div>
<div class="m2"><p>پر شد حکایت من و شیرین حکایتی!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود چیست کشتن چو منی؟ کاهلی ز تست</p></div>
<div class="m2"><p>تا هر زمان مرا بنسوزی ولایتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گفت و گوی دشمن بسیار باک نیست</p></div>
<div class="m2"><p>گر باشدم ز لطف تو اندک حمایتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان زلف کافرانه مرنج، اوحدی، دگر</p></div>
<div class="m2"><p>کز کافری بدیع نباشد جنایتی</p></div></div>