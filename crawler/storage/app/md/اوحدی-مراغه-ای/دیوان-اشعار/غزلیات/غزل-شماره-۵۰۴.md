---
title: >-
    غزل شمارهٔ ۵۰۴
---
# غزل شمارهٔ ۵۰۴

<div class="b" id="bn1"><div class="m1"><p>آن تخم، که در باغ وفا کاشته بودم</p></div>
<div class="m2"><p>شد خار دلم، گر چه گل انگاشته بودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون جگرم خورد و بلای دل من شد</p></div>
<div class="m2"><p>یاری که به خون جگرش داشته بودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پنداشتم آن یار به جز مهر نورزد</p></div>
<div class="m2"><p>او خود به جز آنست که پنداشته بودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گستاخ منش کرده‌ام، اکنون چه توان کرد؟</p></div>
<div class="m2"><p>من بدروم آن تخم که خود کاشته بودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاهی که هوس بر گذرم کند ز سودا</p></div>
<div class="m2"><p>شاید که درافتم، که نینباشته بودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر حرفی از آن دیدم و خطیست به خونم</p></div>
<div class="m2"><p>بر لوح دل آن نقش که بنگاشته بودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیلاب فراق آمد و نگذاشت که باشد</p></div>
<div class="m2"><p>از اوحدی آن مایه که بگذاشته بودم</p></div></div>