---
title: >-
    غزل شمارهٔ ۱۶۴
---
# غزل شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>ای ماه سر نهاده از مهر بر زمینت</p></div>
<div class="m2"><p>صد مشتری درخشان از زهرهٔ جبینت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار تو دل فروزی، شغل تو دیده دوزی</p></div>
<div class="m2"><p>دین تو بنده سوزی، ای من غلام دینت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چنبری چو ماری، هر شقه‌ای تتاری</p></div>
<div class="m2"><p>هر حلقه زنگباری، از طره بر جبینت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم نیست گر شد آبم، یا هجر داد تابم</p></div>
<div class="m2"><p>از بوسه گر بیابم، دستی بر آستینت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سحرست و بی‌وفایی، این حسن و دلربایی</p></div>
<div class="m2"><p>ختم آن گر نمایی، بر خاتم جبینت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان دست پاک طاهر، نور نگار ظاهر</p></div>
<div class="m2"><p>ای زینت جواهر، زان ساعد سمینت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود را زمن چه پوشد؟ جام صفا چو نوشد؟</p></div>
<div class="m2"><p>در یاس من چه کوشد؟ روی چو یاسمینت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشوب عقل و جانی، آرایش جهانی</p></div>
<div class="m2"><p>چون ماه آسمانی، ای آسمان زمینت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه ز خوب چهری، چون اختر سپهری</p></div>
<div class="m2"><p>با دیگران به مهری، با اوحدیست کینت</p></div></div>