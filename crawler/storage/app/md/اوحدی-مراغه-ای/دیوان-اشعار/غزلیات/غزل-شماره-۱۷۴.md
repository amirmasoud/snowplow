---
title: >-
    غزل شمارهٔ ۱۷۴
---
# غزل شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>یاد تو ما را چو در خیال بگردد</p></div>
<div class="m2"><p>عقل پریشان شود، ز حال بگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تو پسر مادر سپهر نزاید</p></div>
<div class="m2"><p>گرد جهان گر هزار سال بگردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه نبیند ستاره‌ای چو جبینت</p></div>
<div class="m2"><p>گر چه بسی بر سپهر زال بگردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط سیه می‌دمد ز رویت و زنهار!</p></div>
<div class="m2"><p>تا نگذاری که: گرد خال بگردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل ندارد، که ترک روی تو گوید</p></div>
<div class="m2"><p>چشم نباشد، کزان جمال بگردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هوس بوسهٔ توایم ولی نیست</p></div>
<div class="m2"><p>زهره که کس گرد این سؤال بگردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن بزن، ای اوحدی، سخن چه فروشی؟</p></div>
<div class="m2"><p>خوی بد نیکوان به مال بگردد</p></div></div>