---
title: >-
    غزل شمارهٔ ۱۷۸
---
# غزل شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>دلی، که میل به دیدار دوستان دارد</p></div>
<div class="m2"><p>فراغتی ز گل و باغ و بوستان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کدام لاله به روی تو ماند؟ ای دلبند</p></div>
<div class="m2"><p>کدام سرو چنین قد دلستان دارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرت به جان بخرم بوسه‌ای، زیان نکنم</p></div>
<div class="m2"><p>که بوسه عاشق بدبخت را زیان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که چون تو پری چهره در کنار کشد</p></div>
<div class="m2"><p>اگر چه پیر بود، دولتی جوان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به قصد کشتن من بست و باز نگشاید</p></div>
<div class="m2"><p>کمر، که قد بلند تو در میان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خاکپای تو آنرا که هست دست رسی</p></div>
<div class="m2"><p>چه غم ز سرزنش هر که در جهان دارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو کردی جای خیال تو اوحدی در دل</p></div>
<div class="m2"><p>به وصل خود برسانش، که جای آن دارد</p></div></div>