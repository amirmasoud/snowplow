---
title: >-
    غزل شمارهٔ ۷۱۰
---
# غزل شمارهٔ ۷۱۰

<div class="b" id="bn1"><div class="m1"><p>خیز و کار رفتنت را ساز ده</p></div>
<div class="m2"><p>همرهان خویش را آواز ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ گل را در زمین پوشیده‌دار</p></div>
<div class="m2"><p>مرغ دل را در فلک پرواز ده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر گمان داری ز معنی‌دان بپرس</p></div>
<div class="m2"><p>ور کمان داری به تیر انداز ده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شوی واقف ز راز آن طرف</p></div>
<div class="m2"><p>مژده‌ای در گوش اهل راز ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور بخواهی نیز کردن یاد ما</p></div>
<div class="m2"><p>هم به یاد آن بت طناز ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس نپردازد سخن چون اوحدی</p></div>
<div class="m2"><p>گوش با قول سخن پرداز ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق را آغاز و انجامی نبود</p></div>
<div class="m2"><p>ساقیا، این جامم از آغاز ده</p></div></div>