---
title: >-
    غزل شمارهٔ ۱۹۰
---
# غزل شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>به دشمنان نتوان رفت و این شکایت کرد</p></div>
<div class="m2"><p>که: دوست بر دل ما جور تا چه غایت کرد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لبش، که بر دل ما راه زد، جنایت نیست</p></div>
<div class="m2"><p>دلم که آه زد از دست او جنایت کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا، که درد ترا من به جان خریدارم</p></div>
<div class="m2"><p>اگر به سینه رسید، ار به جان سرایت کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبت که آیت لطفست، قهر بر دل من</p></div>
<div class="m2"><p>روا بود، چو به حکم حدیث و آیت کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمینه پرتوی از صورت تو بتواند</p></div>
<div class="m2"><p>هزار زهره و خورشید را حمایت کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی ندید رخت را، که وصف داند گفت</p></div>
<div class="m2"><p>قمر نشان تو از دیگری روایت کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر ز بام رخت را مجاوران فلک</p></div>
<div class="m2"><p>به آفتاب نمودند و او حکایت کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر به شحنه بگویند، شهر بگذارد</p></div>
<div class="m2"><p>ستم، که نرگس مست تو در ولایت کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به عشق سرزنش و منع دل کفایت نیست</p></div>
<div class="m2"><p>از آن که در همه عمر خود این کفایت کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشان روی تو از هر که باز پرسیدم</p></div>
<div class="m2"><p>میان عالمیانم نشان و رایت کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بریخت خون من از چشم و مردم از چپ و راست</p></div>
<div class="m2"><p>درین حدیث که: با اوحدی عنایت کرد</p></div></div>