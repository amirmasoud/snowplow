---
title: >-
    غزل شمارهٔ ۳۱۸
---
# غزل شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>مردم شهرم به می‌خوردن ملامت می‌کنند</p></div>
<div class="m2"><p>ساقیا، می ده، بهل، کایشان قیامت می‌کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی در محراب و دل پیش تو دارند، ای پسر</p></div>
<div class="m2"><p>پیشوایانی که مردم را امامت می‌کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر مقامی را بگردیدند سیاحان، کنون</p></div>
<div class="m2"><p>بر سر کوی تو آهنگ اقامت می‌کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر در مسجد گذاری کن، که پیش قامتت</p></div>
<div class="m2"><p>در نماز آیند آنهایی که قامت می‌کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صوفیان کز حلقهٔ زلفت بجستند، این زمان</p></div>
<div class="m2"><p>داده‌اند انصاف و ترتیب غرامت می‌کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باغبانان خدمت سرو و گل اندر بوستان</p></div>
<div class="m2"><p>سال و مه بر یاد آن رخسار و قامت می‌کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم بزیر لب به دشنامی جوابی می‌فرست</p></div>
<div class="m2"><p>عاشقانی را که زیر لب سلامت می‌کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردم چشمت به نشترهای مژگان چو تیر</p></div>
<div class="m2"><p>سینهٔ ما را چرا چندین حجامت می‌کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوحدی را از جهان چشم سلامت بود، لیک</p></div>
<div class="m2"><p>خال و زلفت خاک در چشم سلامت می‌کنند</p></div></div>