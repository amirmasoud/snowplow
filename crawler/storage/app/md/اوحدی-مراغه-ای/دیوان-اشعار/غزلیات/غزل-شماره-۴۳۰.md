---
title: >-
    غزل شمارهٔ ۴۳۰
---
# غزل شمارهٔ ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>چو نام او همی گویی به نام خود قلم در کش</p></div>
<div class="m2"><p>ورش دانسته‌ای، زنهار! خامش باش و دم درکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازآن بی‌چون و چند ار تو نشانی یافتی این جا</p></div>
<div class="m2"><p>ز کوی چند و چون بگذر، زبان از بیش و کم در کش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراغی گر همی خواهی، چراغی از وفا بر کن</p></div>
<div class="m2"><p>به باغ آن پری نه روی و داغ آن صنم در کش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو با زنار عشق او صبوحی کرد روح تو</p></div>
<div class="m2"><p>دلت را خاجها بر رخ ز نیل درد و غم در کش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دست عشق شهر آشوب اگر دادی همیخواهی</p></div>
<div class="m2"><p>سر آشفتهٔ خود را به پای آن علم درکش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو در وصل می‌جویی در صحبت ببند اول</p></div>
<div class="m2"><p>پس آنگه کشتی حاجت به دریای کرم درکش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا وقتی که او خواند، به راهی رو که او داند</p></div>
<div class="m2"><p>چو رفتی دامن اخفا به آثار قدم درکش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن و این چه می‌لافی؟ طلب کن شربت شافی</p></div>
<div class="m2"><p>ز کفر و دین می‌صافی، بیامیز و بهم در کش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بوی جام یکرنگی، چو شد دور از تو دلتنگی</p></div>
<div class="m2"><p>ازل را با ابد ضم کن، حدث را با قدم درکش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز تلخ یار شیرین لب نشاید رخ ترش کردن</p></div>
<div class="m2"><p>گرت جام شفا بخشند و کرکاس الم، در کش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر گوش تو می‌خواهد نوای خسروانیها</p></div>
<div class="m2"><p>به بزم اوحدی آی و شراب از جام جم در کش</p></div></div>