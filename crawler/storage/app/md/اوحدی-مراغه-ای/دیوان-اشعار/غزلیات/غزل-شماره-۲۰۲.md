---
title: >-
    غزل شمارهٔ ۲۰۲
---
# غزل شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>عشق بی‌علت ترنج دوستی بار آورد</p></div>
<div class="m2"><p>گر به علت عشق ورزی رنج و تیمار آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست پیش پاکبازان کام دل جستن؟ غرض</p></div>
<div class="m2"><p>وین غرض در دوستی نقصان بسیار آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان مهربانان مهر دار و گو مباش</p></div>
<div class="m2"><p>همت ارباب دل خود سنگ در کار آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جذب مغناطیس بین کاهن به خود چون می‌کشد؟</p></div>
<div class="m2"><p>کم ز سنگی نیستی کاهن به رفتار آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دل اندر کافری بندد جوانی پاکباز</p></div>
<div class="m2"><p>در نهان او مسلمانی پدیدار آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار گردن کش ز دامت گر چه سر بیرون برد</p></div>
<div class="m2"><p>این کمند آخر همش روزی گرفتار آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ز خوبان دوستی خواهی، به پاکی میل کن</p></div>
<div class="m2"><p>میل خوبان جنبش اندر نقش دیوار آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از برای عاشقست این ناز و غنج و چشم و روی</p></div>
<div class="m2"><p>خواجه بهر مشتری جوهر به بازار آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوحدی، گر کژ روی انکار دشمن لازمست</p></div>
<div class="m2"><p>دوستی چون راست ورزی دشمن اقرار آورد</p></div></div>