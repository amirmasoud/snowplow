---
title: >-
    غزل شمارهٔ ۳۹۰
---
# غزل شمارهٔ ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>من که خمارم، به مسجدها مده راهم دگر</p></div>
<div class="m2"><p>کاین زمان می‌‎خوردم و در حال می‌خواهم دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محنت من جمله از عشقست و رنج از آگهی</p></div>
<div class="m2"><p>باده‌ای در ده، که عقلم هست و آگاهم دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رحم بر گمراه و سرگردان نگفتی: واجبست؟</p></div>
<div class="m2"><p>رحمتی بر من، که سرگردان و گمراهم دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدتی در بسته بودم دیده از دیدار خواب</p></div>
<div class="m2"><p>صورت او در خیال آمد ز ناگاهم دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی گندم‌گون او با من نمی‌دانم چه کرد؟</p></div>
<div class="m2"><p>این همی دانم که: همچون کاه می‌کاهم دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با زنخدانش مرا میلیست، می‌دانم که: زود</p></div>
<div class="m2"><p>خواهد افگندن به بازی اندر آن چاهم دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم ببخشیدی دلش بر نالهٔ شبهای من</p></div>
<div class="m2"><p>گر به گوش او رسیدی ناله و آهم دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من که بر عشقم بریدستند ناف از کودکی</p></div>
<div class="m2"><p>چون توان از عشق ببریدن با کراهم دگر؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوحدی امسال اگر آهنگ رفتن می‌کند</p></div>
<div class="m2"><p>گو: سفر می‌کن، که من حیران آن ماهم دگر</p></div></div>