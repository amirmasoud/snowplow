---
title: >-
    غزل شمارهٔ ۷۴۲
---
# غزل شمارهٔ ۷۴۲

<div class="b" id="bn1"><div class="m1"><p>ای که تیر بی‌وفایی در کمان پیوسته‌ای</p></div>
<div class="m2"><p>بار دیگر چیست کندر دیگران پیوسته‌ای؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به شمشیر فراقم پی کنی صد پی روان</p></div>
<div class="m2"><p>در تو پیوندم، که صد رگ با روان پیوسته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بهایی گوهر، اندر سلک پیمان و وفا</p></div>
<div class="m2"><p>با چنان خرمهرها بس رایگان پیوسته‌ای!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میخوری خون دل من، تا ز دل دوری کنم</p></div>
<div class="m2"><p>از دلم چون دور گردی؟ چون به جان پیوسته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقت خاموشی چو فکر اندر دلم پیچیده‌ای</p></div>
<div class="m2"><p>روز گویایی چو ذکرم در زبان پیوسته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه هر دم بشکنی عهدی و برداری دلی</p></div>
<div class="m2"><p>همچنین می‌کن، که با ما هم چنان پیوسته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوش دار، ای بت که از زلف گریبانگیر خود</p></div>
<div class="m2"><p>فتنها در دامن آخر زمان پیوسته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بجوشد خونم اندر پوست چندان طرفه نیست</p></div>
<div class="m2"><p>که آتش مهرم به مغز استخوان پیوسته‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دشمن من خاک بر سر کرد، تا در کوی خویش</p></div>
<div class="m2"><p>اوحدی را سر به خاک آستان پیوسته‌ای</p></div></div>