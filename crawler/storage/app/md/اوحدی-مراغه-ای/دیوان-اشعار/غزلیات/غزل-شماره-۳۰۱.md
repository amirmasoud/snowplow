---
title: >-
    غزل شمارهٔ ۳۰۱
---
# غزل شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>دل به کسی سپرده‌ام کو همه قصد جان کند</p></div>
<div class="m2"><p>کام کسی روا نکرد، اشک بسی روان کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که بدید کار ما وین رخ زرد زار ما</p></div>
<div class="m2"><p>گفت که: در دیار ما جور چنین فلان کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حجت بندگی بدو، دارم از اعتراف خود</p></div>
<div class="m2"><p>بی‌خبرست مدعی، هر چه جزین بیان کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت:وفا کنم، دلا، هر چه بگوید آن پری</p></div>
<div class="m2"><p>بر همه گوش کن ولی این مشنو که آن کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف دراز دست را بند نهاد چند پی</p></div>
<div class="m2"><p>ور بخودش فرو هلد بار دگر چنان کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من سخن جفای او با همه گفته‌ام، ولی</p></div>
<div class="m2"><p>پند نگیرد اوحدی، تا دل و دین در آن کند</p></div></div>