---
title: >-
    غزل شمارهٔ ۵۱۷
---
# غزل شمارهٔ ۵۱۷

<div class="b" id="bn1"><div class="m1"><p>منازل سفرت پیش دیده می‌آرم</p></div>
<div class="m2"><p>اگر چه هیچ به منزل نمی‌رسد بارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیاه مهر بروید ز خاک منزل تو</p></div>
<div class="m2"><p>که من ز دیده برو آب مهر می‌بارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن به روز وداعت نهان شدم ز نظر</p></div>
<div class="m2"><p>کز آب چشم روان فاش میشد اسرارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجال آمدن و پای راه رفتن نیست</p></div>
<div class="m2"><p>که رخت خویش بر آن خاک آستان دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به روز گویمت: امشب به خواب خواهم دید</p></div>
<div class="m2"><p>چو شب شود همه شب تا به روز بیدارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرم به روز قرارست یا به شب بی‌تو</p></div>
<div class="m2"><p>ز روز وصل و شب صحبت تو بیزارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جای آنم، اگر بر دلم ببخشایند</p></div>
<div class="m2"><p>که دل بدادم و از درد بیدلی زارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا به خوان وز درد فراق هیچ مپرس</p></div>
<div class="m2"><p>که آب دیده نیابت کند ز گفتارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببر ز من طمع طوع و بندگی، که هنوز</p></div>
<div class="m2"><p>بدان کمند که افگنده‌ای گرفتارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بتاب دوزخ هجران تمام خواهم سوخت</p></div>
<div class="m2"><p>اگر سبک نبدی در بهشت دیدارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تویی ز مردم چشمم عزیزتر، گر چه</p></div>
<div class="m2"><p>من از برای تو در چشم مردمان خوارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل از رکاب تو خالی نمی‌شود باری</p></div>
<div class="m2"><p>اگر چه نیست بر آن در چو اوحدی بارم</p></div></div>