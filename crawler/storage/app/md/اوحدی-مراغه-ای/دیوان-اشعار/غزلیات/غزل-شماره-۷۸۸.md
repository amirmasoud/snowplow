---
title: >-
    غزل شمارهٔ ۷۸۸
---
# غزل شمارهٔ ۷۸۸

<div class="b" id="bn1"><div class="m1"><p>ز تورانیان تنگ چشمی سواری</p></div>
<div class="m2"><p>در ایران به زلف سیه کرد کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که کافر نکردست بر دین پرستی</p></div>
<div class="m2"><p>که دشمن نکردست با دوستداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهانش خموشی، لبش باده نوشی</p></div>
<div class="m2"><p>سرش پر خروشی، میان پود و تاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چهره چراغی، به رخساره باغی</p></div>
<div class="m2"><p>به سیرت بهشتی، به صورت بهاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستم را به سختی دلش پایمردی</p></div>
<div class="m2"><p>طرب را به نرمی تنش دستیاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بالا چو سروی، برفتن تذروی</p></div>
<div class="m2"><p>به پیکار شاهی، به پیکر نگاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیاووش رویی، فرنگیس مویی</p></div>
<div class="m2"><p>فریبرز شکلی، فریدون شعاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه جمشید، لیکن هرش بنده میری</p></div>
<div class="m2"><p>نه ضحاک، لیکن هرش زلف ماری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر شعر گویی در آن غمزه زیبد</p></div>
<div class="m2"><p>و گر هوش بندی در آن زلف باری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کزین بیژنی را بدوزد به تیری</p></div>
<div class="m2"><p>وزان رستمی را ببند به تاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شماری گر از بیدلانش بگیری</p></div>
<div class="m2"><p>نگیری دل اوحدی در شماری</p></div></div>