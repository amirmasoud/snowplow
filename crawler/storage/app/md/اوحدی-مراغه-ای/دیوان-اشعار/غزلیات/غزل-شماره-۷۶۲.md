---
title: >-
    غزل شمارهٔ ۷۶۲
---
# غزل شمارهٔ ۷۶۲

<div class="b" id="bn1"><div class="m1"><p>ای برون از بلندی و پستی</p></div>
<div class="m2"><p>جز تو کس را نمی‌رسد هستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل در وادی محبت تو</p></div>
<div class="m2"><p>ره غلط می‌کند ز سرمستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سر جمله‌ها شود نامت</p></div>
<div class="m2"><p>خویشتن را به جمله بر بستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حلقه‌ای نیست خالی از ذکرت</p></div>
<div class="m2"><p>گر چه در هیچ حلقه ننشستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بودن ما جدا نبود از تو</p></div>
<div class="m2"><p>با تو بودیم تا تو بودستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر چار سوی رغبت خویش</p></div>
<div class="m2"><p>نخریدی دلی، که نشکستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی، گر وصال او خواهی</p></div>
<div class="m2"><p>ببر از خویشتن، که پیوستی</p></div></div>