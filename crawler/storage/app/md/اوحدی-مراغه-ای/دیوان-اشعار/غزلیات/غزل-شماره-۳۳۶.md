---
title: >-
    غزل شمارهٔ ۳۳۶
---
# غزل شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>من از آن که شوم کو نه ازان تو بود؟</p></div>
<div class="m2"><p>یا چه گویم که نه در لوح و بیان تو بود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن لب، که تو داری، نتوانم گفتن</p></div>
<div class="m2"><p>ور بگویم سخنی هم ز زبان تو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمانم به جهانی دگر اندازی، لیک</p></div>
<div class="m2"><p>نروم جز به جهانی که جهان تو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن و دل گر به فدای تو کند چندان نیست</p></div>
<div class="m2"><p>خاصه آن کش دل و تن زنده به جان تو بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگذاری که ببوسد لبم آن پای و رکاب</p></div>
<div class="m2"><p>ای خوش آن بوسه که بر دست و عنان تو بود!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نشانی بنماند ز تن من بر خاک</p></div>
<div class="m2"><p>دل تنگم به همان مهر و نشان تو بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان خود را سپر تیر بلا خواهم ساخت</p></div>
<div class="m2"><p>اگر آن تیر، که آید ز کمان تو بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بپوسد تن من گوش و روانی که مراست</p></div>
<div class="m2"><p>بر ورود خبر و حکم روان تو بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چه آرند به بازار دو کون، از نیکی</p></div>
<div class="m2"><p>همه، چون نیک ببنینی، ز دکان تو بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده در کل مکان گر چه ترا می‌بیند</p></div>
<div class="m2"><p>من نخواهم که به جز دیده مکان تو بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌کنم ذکر تو پیوسته به قلب و به لسان</p></div>
<div class="m2"><p>خنک آن قلب که مذکور لسان تو بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست غم سر دل اوحدی ار گردد فاش</p></div>
<div class="m2"><p>چو دلش حافظ اسرار نهان تو بود</p></div></div>