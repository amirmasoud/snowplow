---
title: >-
    غزل شمارهٔ ۵۵
---
# غزل شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>زان دوست که غمگینم، غم‌‎خوار کنش، یارب</p></div>
<div class="m2"><p>دشمن که نمی‌خواهد، هم‌خوار کنش، یارب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر دل سخت او کین پر شد و مِهر اندک</p></div>
<div class="m2"><p>آن مهر که اندک شد، بسیار کنش، یارب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر گشته و غم‌خوارم، آن کین غم ازو دارم</p></div>
<div class="m2"><p>همچون من سرگشته، بی‌یار کنش، یارب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردست رقیبان را خار گل روی خود</p></div>
<div class="m2"><p>نازک شکفید آن گل، بی‌خار کنش، یارب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر زلف چو زُنّارش می‌رنجد ازین خرقه</p></div>
<div class="m2"><p>این خرقه که من دارم، زنّار کُنَش یارب!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این سینه که شد سوزان از مِهر جگردوزان</p></div>
<div class="m2"><p>چون مهر بر افروزان، یا نار کنش، یارب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن کاو نکند باور بیماری و درد من</p></div>
<div class="m2"><p>یک چند به درد او، بیمار کنش، یارب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشمش همه را خواند وز روی مرا راند</p></div>
<div class="m2"><p>مستست و نمی‌داند، هشیار کنش، یارب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دم به دل سختم، تاراج کند رختم</p></div>
<div class="m2"><p>در خواب شد این بختم، بیدار کنش، یارب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌کار شد آه من، اندر دل ماه من</p></div>
<div class="m2"><p>منگر به گناه من، پر کار کنش، یارب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل برد و ز درد دل می‌گریم و می‌گویم:</p></div>
<div class="m2"><p>کان کس که ببُرد این دل، دلدار کنش، یارب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن کش نشد آگاهی از غارت رخت من</p></div>
<div class="m2"><p>یک هفته اسیر این طرّار کنش، یارب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر زانکه بیازارد، سهل‌‎ست، مرا آن بت</p></div>
<div class="m2"><p>از اوحدی آن آزار، بیزار کنش، یارب</p></div></div>