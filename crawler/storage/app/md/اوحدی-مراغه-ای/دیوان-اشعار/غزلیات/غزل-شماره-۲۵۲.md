---
title: >-
    غزل شمارهٔ ۲۵۲
---
# غزل شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>حسن بدکان نشست، عشق پدیدار شد</p></div>
<div class="m2"><p>حسن فروشنده گشت، عشق خریدار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلوت دل چون ز دوست پر شد و پر کرد پوست</p></div>
<div class="m2"><p>واقعه انبوه گشت داعیه بسیار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد و شد در گرفت از چپ و از راست دوست</p></div>
<div class="m2"><p>دل به تماشای او بر در و دیوار شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده ز رخ دور کرد، شهر پر از نور کرد</p></div>
<div class="m2"><p>دیدن او سهل گشت، دادن جان خوار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دو جهان ذره‌ای بی‌هوس او نماند</p></div>
<div class="m2"><p>از همه ذرات کون او چو خریدار شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن که شایسته بود بر زد و بر تخت رفت</p></div>
<div class="m2"><p>عشق که دیوانه بود سر زد و بردار شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر تن من بار بست حسن چو نیرو گرفت</p></div>
<div class="m2"><p>بر دل من زور کرد عشق چو در کار شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صورت لیلی‌رخی صبح چو در دادمی</p></div>
<div class="m2"><p>فتنه در آمد ز خواب، عربده بیدار شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل در غارت گرفت، ترک عمارت گرفت</p></div>
<div class="m2"><p>تا چه خرابی کند؟ عشق چو معمار شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه به جز یاد او قیمت و قدری نیافت</p></div>
<div class="m2"><p>هر چه به جز عشق او پست و نگونسار شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از دل من عشق جست نقش دویی چون بشست</p></div>
<div class="m2"><p>شب همه معراج گشت، رخ همه دیدار شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من چو ز من گم شدم، غرق ترحم شدم</p></div>
<div class="m2"><p>دوست مرا دوست داشت، یار مرا یار شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر چه جزین چند بار فتنهٔ او دیده‌ام</p></div>
<div class="m2"><p>بندهٔ این بار من، کین همه انبار شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اوحدی از دست عشق تا قدحی نوش کرد</p></div>
<div class="m2"><p>رخ به خرابات کرد، رخت به خمار شد</p></div></div>