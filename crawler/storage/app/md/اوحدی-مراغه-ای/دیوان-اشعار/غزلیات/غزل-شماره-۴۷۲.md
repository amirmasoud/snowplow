---
title: >-
    غزل شمارهٔ ۴۷۲
---
# غزل شمارهٔ ۴۷۲

<div class="b" id="bn1"><div class="m1"><p>من چو همین حرف الف دیده‌ام</p></div>
<div class="m2"><p>حرف دگر زان نپسندیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه نه از پیش الف شد روان</p></div>
<div class="m2"><p>همچو الف بر همه خندیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ ندارد الف عاشقان</p></div>
<div class="m2"><p>هیچ ندارم، که نترسیده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ز الف شد همه حرفی پدید</p></div>
<div class="m2"><p>من همه دیدم، چو الف دیده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بهم آمد الفی، راست شد</p></div>
<div class="m2"><p>هر نقطی کز همگان چیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش الف بسکه فتادم چو با</p></div>
<div class="m2"><p>ها شدم، از بسکه بغلتیده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ها چو شود راست چه باشد؟ الف</p></div>
<div class="m2"><p>گفته شد آن حرف که پوشیده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوسه زدم پای الف را ولی</p></div>
<div class="m2"><p>دست خودم بود که بوسیده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من الف وصلم و جز نام وصل</p></div>
<div class="m2"><p>هر چه بگفتند بنشنیده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پر بنوشتند ولی یاد من</p></div>
<div class="m2"><p>هیچ نکردند و نرنجیده‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان خط و زان نقطه نشان کس نداد</p></div>
<div class="m2"><p>جز الف، از هر که بپرسیده‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پای و سرم در حرکت گم که شد</p></div>
<div class="m2"><p>هم به سکونیست که ورزیده‌ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون الف از عشق بگشتم به سر</p></div>
<div class="m2"><p>وز سر این عشق نگردیده‌ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر نه غلام الفم، همچو لام</p></div>
<div class="m2"><p>در الف از بهر چه پیچیده‌ام؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون الف صدر نشین اوحدیست</p></div>
<div class="m2"><p>بی‌سخن او به چه ارزیده‌ام؟</p></div></div>