---
title: >-
    غزل شمارهٔ ۷۶۰
---
# غزل شمارهٔ ۷۶۰

<div class="b" id="bn1"><div class="m1"><p>دانه‌ای بر روی دام انداختی</p></div>
<div class="m2"><p>مرغ آدم را ز بام انداختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شود سجاده و تسبیح رد</p></div>
<div class="m2"><p>جرعه‌ای در کاس و جام انداختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کرا خون خواستی کردن حلال</p></div>
<div class="m2"><p>خرقهٔ او بر حرام انداختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سزای سوختن دیدی مرا</p></div>
<div class="m2"><p>در چنین سودای خام انداختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیدلان را چون ندیدی مرد وصل</p></div>
<div class="m2"><p>در کف پیک و پیام انداختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک سخن ناگفته، ما را چون سخن</p></div>
<div class="m2"><p>در زبان خاص و عام انداختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیگران را بار دادی چون کلیم</p></div>
<div class="m2"><p>اوحدی را در کلام انداختی</p></div></div>