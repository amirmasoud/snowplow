---
title: >-
    غزل شمارهٔ ۳۶۵
---
# غزل شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>هر که مشغول تو گشت از دگران باز آید</p></div>
<div class="m2"><p>وانکه در پای تو افتاد سرافراز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کبوتر که ز دام سر زلفت بجهد</p></div>
<div class="m2"><p>به سر دانهٔ خال تو سبک باز آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقت جان دادن اگر بر رخت افتد نظرم</p></div>
<div class="m2"><p>چشم من تا به لب گور نظر باز آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور سگ کوی تو در گور من آواز دهد</p></div>
<div class="m2"><p>استخوانم ز نشاط تو به آواز آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مفلسی را که خیال تو در افتد به دماغ</p></div>
<div class="m2"><p>گر صدش غم بود اندر طرب و ناز آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه با واقعهٔ عشق تو پرداخت چو من</p></div>
<div class="m2"><p>چه عجب! اگر به سخن واقعه پرداز آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود گرفتم ز غم خویش بسوزی تو مرا</p></div>
<div class="m2"><p>چون من امروز که داری که سخن ساز آید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قصهٔ اوحدی از راه سپاهان بشنو</p></div>
<div class="m2"><p>همچو آوازهٔ سعدی که ز شیراز آید</p></div></div>