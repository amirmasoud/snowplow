---
title: >-
    غزل شمارهٔ ۲۹۱
---
# غزل شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>یوسف ما را به چاه انداختند</p></div>
<div class="m2"><p>گرگ او را در گناه انداختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آنگه از بهر برون آوردنش</p></div>
<div class="m2"><p>کاروانی را به راه انداختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از فراق روی او یعقوب را</p></div>
<div class="m2"><p>سالها در آه آه انداختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خریداران بدیدندش ز جهل</p></div>
<div class="m2"><p>در بها سیم سیاه انداختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد به مصر و از زلیخا دیدنش</p></div>
<div class="m2"><p>باز در زندان شاه انداختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواب زندان را چو معنی باز یافت</p></div>
<div class="m2"><p>تختش اندر بارگاه انداختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد پس از خواری عزیز و در برش</p></div>
<div class="m2"><p>خلعت« ثم اجتباه» انداختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نبیند هر کسی آن ماه را</p></div>
<div class="m2"><p>برقعی بر روی ماه انداختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون گواه انگشت بر حرفش نهاد</p></div>
<div class="m2"><p>زخم بر دست گواه انداختند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حال سلطانیش چون مشهور شد</p></div>
<div class="m2"><p>جست و جویی در سپاه انداختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دشمنش را از هوای سرزنش</p></div>
<div class="m2"><p>صاع در آب و گیاه انداختند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قرعهٔ خط بشارت بردنش</p></div>
<div class="m2"><p>بر بشیر نیک خواه انداختند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باز با قوم خودش کردند جمع</p></div>
<div class="m2"><p>جمله را در عزو جاه انداختند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این حکایت سر گذشت روح تست</p></div>
<div class="m2"><p>کش درین زندان و چاه انداختند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اوحدی چون باز دید این سرو گفت</p></div>
<div class="m2"><p>سر او را با اله انداختند</p></div></div>