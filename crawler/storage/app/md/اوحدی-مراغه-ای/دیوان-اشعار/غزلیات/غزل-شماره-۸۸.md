---
title: >-
    غزل شمارهٔ ۸۸
---
# غزل شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>نهان از نهان کیست؟ دلدار ماست</p></div>
<div class="m2"><p>برون از جهان چیست؟ بازار ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دستم ز باغ جهان گل مده</p></div>
<div class="m2"><p>که بی‌روی آن نازنین خار ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر مقبلی هست،در بند اوست</p></div>
<div class="m2"><p>وگر مشکلی هست، در کار ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر ما به جز نام آن رخ مگوی</p></div>
<div class="m2"><p>که او قبلهٔ چشم بیدار ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندیدی رخش را، ز ما هم مپرس</p></div>
<div class="m2"><p>بدیدی، چه حاجت به گفتار ماست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو پندار باشی ز دلدار دور</p></div>
<div class="m2"><p>که دوری هم از پیش پندار ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن مصر اگر شرمساری بریم</p></div>
<div class="m2"><p>ازین صاع باشد، که دربار ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز نار غم آن پری شعله‌ای</p></div>
<div class="m2"><p>باین خرقه در زن، که زنار ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میان من و او حجاب اوحدیست</p></div>
<div class="m2"><p>چو او رفع شد، روز دیدار ماست</p></div></div>