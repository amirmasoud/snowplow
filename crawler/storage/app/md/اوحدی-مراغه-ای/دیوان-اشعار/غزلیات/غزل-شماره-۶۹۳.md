---
title: >-
    غزل شمارهٔ ۶۹۳
---
# غزل شمارهٔ ۶۹۳

<div class="b" id="bn1"><div class="m1"><p>روز عید آن ترک را دیدم پگاه آراسته</p></div>
<div class="m2"><p>گشته از رویش سراسر عید گاه آراسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاق ابرو را ز شوخی چون هلالی داده خم</p></div>
<div class="m2"><p>روی نیکو را به زیبایی چو ماه آراسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم جمال ماه رویش آب خوبان ریخته</p></div>
<div class="m2"><p>هم هلال نعل اسبش خاک راه آراسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیدلان را مال و سر بر دست و دلبر بی‌نیاز</p></div>
<div class="m2"><p>بندگان از پیش و پس حیران و شاه آراسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او چو شمعی در میان و عاشقان پیرامنش</p></div>
<div class="m2"><p>حلقه‌ای از ناله و فریاد و آه آراسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرگس چشم و گل و رخسار و سرو قد او</p></div>
<div class="m2"><p>در شنکج حلقهٔ زلف سیاه آراسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف چوگان وار خود همچون رسنها داده تاب</p></div>
<div class="m2"><p>وانگهی گوی زنخدان را به چاه آراسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لاف عشقت میزنند آشفته حالان جهان</p></div>
<div class="m2"><p>اوحدی میرست و در عشقت سپاه آراسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیدن خوبان اگر جرم و گناهست، ای صنم</p></div>
<div class="m2"><p>نالها دارم بدین جرم و گناه آراسته</p></div></div>