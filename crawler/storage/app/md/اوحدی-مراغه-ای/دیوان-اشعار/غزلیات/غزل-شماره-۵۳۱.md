---
title: >-
    غزل شمارهٔ ۵۳۱
---
# غزل شمارهٔ ۵۳۱

<div class="b" id="bn1"><div class="m1"><p>گر مرغ این هوایی، بال و پرت بسوزم</p></div>
<div class="m2"><p>ور حال دل نمایی، دل در برت بسوزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من شمع گشتم و تو پروانه، تا به زاری</p></div>
<div class="m2"><p>در پای من بمیری، من در برت بسوزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ز آتشت بسوزم دیگر بشارت آرم</p></div>
<div class="m2"><p>تا بنگرم که هستی، زان بهترت بسوزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاکسترت کنم من روزی در آتش خود</p></div>
<div class="m2"><p>وز دستم ار بنالی خاکسترت بسوزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون عودت ار بسازم، ایمن مشو، که من گر</p></div>
<div class="m2"><p>در پرده‌ات بسازم، در دیگرت بسوزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا غرق عشق گردی در بحر بی‌نشانی</p></div>
<div class="m2"><p>هم بادبان ببرم، هم لنگرت بسوزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقتی که نام خود را مؤمن کنی ز طاعت</p></div>
<div class="m2"><p>مؤمن کنی، ولیکن چون کافرت بسوزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان رنگ و بوی چندین چون گل مخند، کین جا</p></div>
<div class="m2"><p>گر زانکه عود خامی بر مجمرت بسوزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی: خلاص یابد، هر زر که خالص آید</p></div>
<div class="m2"><p>من در خلاص غیرت سیم و زرت بسوزم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هان! تا چو اوحدی تو بر هر دری نگردی</p></div>
<div class="m2"><p>ورنه چو خاک کوچه بر هر درت بسوزم</p></div></div>