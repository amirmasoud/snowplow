---
title: >-
    غزل شمارهٔ ۵۳۲
---
# غزل شمارهٔ ۵۳۲

<div class="b" id="bn1"><div class="m1"><p>روزی بر آن شمع چو پروانه بسوزم</p></div>
<div class="m2"><p>در خویش زنم آتش و مردانه بسوزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون با من بیگانه غمش را سر خویشست</p></div>
<div class="m2"><p>با خویش در آمیزم و بیگانه بسوزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیوانه شوم، سر به خرابات برآرم</p></div>
<div class="m2"><p>بر خویش دل عاقل و دیوانه بسوزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر آتش اندوه برین آب بماند</p></div>
<div class="m2"><p>هم رخت براندازم و هم خانه بسوزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وصل دلم را نه به پیمانه دهد می</p></div>
<div class="m2"><p>در می‌فگنم آتش و پیمانه بسوزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاران همه در گلشن وصلند به شادی</p></div>
<div class="m2"><p>من چند درین گلخن ویرانه بسوزم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بر گذرم دام نهد، اوحدی، این بار</p></div>
<div class="m2"><p>هم دام بدرانم و همه دانه بسوزم</p></div></div>