---
title: >-
    غزل شمارهٔ ۶۷۳
---
# غزل شمارهٔ ۶۷۳

<div class="b" id="bn1"><div class="m1"><p>به جان من، به جان من، به جان تو، به جان تو</p></div>
<div class="m2"><p>که نام من نفرمایی فراموش از زبان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سود من، نپندارم، ترا هرگز زیان دارد</p></div>
<div class="m2"><p>که سود تست سود من، زیان من زیان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو و من در میان ما کجا گنجد؟ که اینساعت</p></div>
<div class="m2"><p>تو گردیدی و گردیدم، تو آن من، من آن تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلط کردم، نه آن گنجی که در آغوش من گنجی</p></div>
<div class="m2"><p>مرا این بس که در گنجم به کنجی در جهان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر از خاک زمینم بر ندارد آسمان هرگز</p></div>
<div class="m2"><p>اگر ساکن خودم خواند زمین و آسمان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لبت می‌پرسد از جانم که: کامت چیست؟ تا دانم</p></div>
<div class="m2"><p>چه باشد کام مشتاقی؟ دهانی بر دهان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گمان بردی که برگشتم به جور از آستانت من؟</p></div>
<div class="m2"><p>بلی در حق مسکینان خود این باشد گمان تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل از ما خواستی، جانا، دریغی نیست دل، لیکن</p></div>
<div class="m2"><p>چو روی از ما نمی‌پوشی، کسی باید ضمان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آن حشمت که می‌بینم نخواهد هیچ کم گشتن</p></div>
<div class="m2"><p>فقیری گر بیاساید زمانی در زمان تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو با آن حس و زیبایی نگردی هم نشین من</p></div>
<div class="m2"><p>که از خواری و گمراهی نمی‌یابم نشان تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رخت را شد به جان و دل خریدار اوحدی، لیکن</p></div>
<div class="m2"><p>بدین سرمایه چون گردد کسی گرد دکان تو؟</p></div></div>