---
title: >-
    غزل شمارهٔ ۱۴۵
---
# غزل شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>آن ستمگر، که وفای منش از یاد برفت</p></div>
<div class="m2"><p>آتش اندر من مسکین زد و چون باد برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او به بغداد روان گشت و مرا در پی او</p></div>
<div class="m2"><p>آب چشمست که چون دجلهٔ بغداد برفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه می‌گفت که: از بند شما آزادم</p></div>
<div class="m2"><p>هم‌چنان بندهٔ آنیم، که آزاد برفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او چو برخاست غم خود به نیابت بنشاند</p></div>
<div class="m2"><p>تا نگویی که: سپهر از بر بیداد برفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از من خسته به شیرین که رساند خبری؟</p></div>
<div class="m2"><p>کز فراق تو چها بر سر فرهاد برفت!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش ازین در دل من هر هوسی بگذشتی</p></div>
<div class="m2"><p>دل بدو دادم و دانم همه از یاد برفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی، از غم او ناله نمی‌باید کرد</p></div>
<div class="m2"><p>سهل کاریست غم ما، اگر او شاد برفت</p></div></div>