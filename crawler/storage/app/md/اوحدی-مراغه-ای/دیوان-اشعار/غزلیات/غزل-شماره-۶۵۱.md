---
title: >-
    غزل شمارهٔ ۶۵۱
---
# غزل شمارهٔ ۶۵۱

<div class="b" id="bn1"><div class="m1"><p>حلقهٔ زرین بر آن گوش گهربندش ببین</p></div>
<div class="m2"><p>خال مشکین بر لب شیرین چون قندش ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسته بر هم گردن شهری، دل دیوانه را</p></div>
<div class="m2"><p>در میان حلقهای زلف چون بندش ببین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم معنی برگشای و چشمهٔ آب حیات</p></div>
<div class="m2"><p>مضمر اندر گوشهٔ لعل شکرخندش ببین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک همچون دجلهٔ من در غمش دیدی بسی</p></div>
<div class="m2"><p>بر دل من محنت چون کوه الوندش ببین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده‌ای کان عهد یاران قدیمی چون شکست؟</p></div>
<div class="m2"><p>این زمان با دوستان تازه پیوندش ببین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقان از آرزوی روی او جان می‌دهند</p></div>
<div class="m2"><p>آرزوی عاشقان آرزومندش ببین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی پندم همی گوید که: ترک عشق کن</p></div>
<div class="m2"><p>دیدن رویی چنان و دادن پندش ببین!</p></div></div>