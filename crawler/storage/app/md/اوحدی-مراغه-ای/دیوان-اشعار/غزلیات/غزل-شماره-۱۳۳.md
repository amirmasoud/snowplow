---
title: >-
    غزل شمارهٔ ۱۳۳
---
# غزل شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>جنبیدن این پرده دل افروز گواهیست</p></div>
<div class="m2"><p>کندر پس این پرده پر از عربده ماهیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر صورت این پرده بزرگان شده حیران</p></div>
<div class="m2"><p>وین خرده ندانسته که: در پرده چه شاهیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این پرده به تلبیس کجا دور توان کرد؟</p></div>
<div class="m2"><p>هر موی برین پرده جهانی و سپاهیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آنکه درین پرده شما راست مجالی</p></div>
<div class="m2"><p>زان پرده به در هیچ میابید، که چاهیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این پرده نشین چیست؟ که ما را غرض امروز</p></div>
<div class="m2"><p>بر صورت بی‌صورت این پرده نگاهیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای کوه بلا بر دل عشاق نهاده</p></div>
<div class="m2"><p>آن پرده برانداز، که صد پرده به کاهیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطرب، تو بدین پرده که ما را بزدی راه</p></div>
<div class="m2"><p>بنواز دگر باره، که خوش پرده و راهیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آواز کسی راه در این پرده ندارد</p></div>
<div class="m2"><p>هرگز،مگرآن نغمه که پشتی و پناهیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنهار!که تا دست طمع باز نگیری</p></div>
<div class="m2"><p>از دامن این پرده، که پشتی و پناهیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای اوحدی، از در طلب خط نجاتی</p></div>
<div class="m2"><p>روی از خط این پرده مپیچان، که گناهیست</p></div></div>