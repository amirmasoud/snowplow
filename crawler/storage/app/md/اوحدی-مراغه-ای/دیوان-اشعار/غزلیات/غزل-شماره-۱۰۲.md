---
title: >-
    غزل شمارهٔ ۱۰۲
---
# غزل شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>انجمن شهر ملای گلست</p></div>
<div class="m2"><p>باده بیاور، که صلای گلست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نالهٔ مرغان سحرخوان به صبح</p></div>
<div class="m2"><p>از سر عشقت، نه برای گلست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر رخ خوبان جهان خط کشید</p></div>
<div class="m2"><p>سبزه، که خاک کف پای گلست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغ، که او خاک معنبر کند</p></div>
<div class="m2"><p>سنبل او خواجه سرای گلست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیرهن یوسف مصری، که شهر</p></div>
<div class="m2"><p>پرصفت اوست، قبای گلست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر به در دوست نهادند خلق</p></div>
<div class="m2"><p>در همه سرها چو هوای گلست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی، اینها همه گفتی، ولی</p></div>
<div class="m2"><p>با رخ آن ماه چه جای گلست؟</p></div></div>