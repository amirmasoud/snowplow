---
title: >-
    غزل شمارهٔ ۳
---
# غزل شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>گر تو طالب عشقی، غم دمادمست اینجا</p></div>
<div class="m2"><p>ور نشانه می‌پرسی، رشته سر گمست اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون درین مقام آیی گوش کن که: در راهت</p></div>
<div class="m2"><p>ز آب چشم مظلومان چاه زمزمست اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چیست جرم ما؟ گویی کز حریف ناهمتا</p></div>
<div class="m2"><p>هر کجا که بنشینی گو که کژدمست اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جو فروش مفتی را از نماز و از روزه</p></div>
<div class="m2"><p>رنگ چهرهٔ کاهی بهر گندمست اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر حریف مایی تو، ما و کنج می‌خانه</p></div>
<div class="m2"><p>ور زعشق می‌پرسی، عشق در خمست اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونکه بنده فرمانی، پیش حاکم مطلق</p></div>
<div class="m2"><p>سربنه، که هر ساعت صدتحکمست اینجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو دیو بگریزی،چون زمردت پرسم</p></div>
<div class="m2"><p>گر تو مردمی،بالله،خود چه مردمست اینجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم بسوزدت روزی، گرچه نیک خامی تو</p></div>
<div class="m2"><p>کین تنور چون پر شد سنگ هیزمست اینجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوحدی، ترا از چه نان نمی‌فروشد کس؟</p></div>
<div class="m2"><p>گرنه نام بوبکری با تو در قمست اینجا</p></div></div>