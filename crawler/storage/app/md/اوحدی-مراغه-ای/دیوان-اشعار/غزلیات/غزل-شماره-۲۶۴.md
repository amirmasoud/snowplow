---
title: >-
    غزل شمارهٔ ۲۶۴
---
# غزل شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>کسی که چشمهٔ چشمش چنین ز گریه بجوشد</p></div>
<div class="m2"><p>چگونه راز دل خود ز چشم خلق بپوشد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلی که این همه آتش درو زنند بنالد</p></div>
<div class="m2"><p>تنی که این همه گرمی درو کنند بخوشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حدیث ماه رخش آن چنان که هست نگویم</p></div>
<div class="m2"><p>مرا مجال نماند، ز مشتری، که بجوشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کوشش از متصور شود وصال رخ تو</p></div>
<div class="m2"><p>به دوستی، که پشیمان شود کسی که نکوشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستمگرا، ز غمت گر دلم خروش برآرد</p></div>
<div class="m2"><p>عجب مدار، که سنگ از چنین غمی بخروشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا به دل ز که جویم؟ گرت به ترک بگویم</p></div>
<div class="m2"><p>بدان درم چه ستاند؟ کسی که جان بفروشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا نصیحت بسیار می‌کنند، ولیکن</p></div>
<div class="m2"><p>چه سود پند رفیقان؟ چو اوحدی ننیوشد</p></div></div>