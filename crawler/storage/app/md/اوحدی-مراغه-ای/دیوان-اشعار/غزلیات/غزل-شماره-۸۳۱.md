---
title: >-
    غزل شمارهٔ ۸۳۱
---
# غزل شمارهٔ ۸۳۱

<div class="b" id="bn1"><div class="m1"><p>نزدیک یار اگر نه چنین خوار و خردمی</p></div>
<div class="m2"><p>در هجرش این مذلت و خواری نبردمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌او ز جان ملول شدم، کو خیال او؟</p></div>
<div class="m2"><p>تا جان خود به دست خیالش سپردمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از باد صبح‌گاه درین تنگنای هجر</p></div>
<div class="m2"><p>گر بوی او به من نرسیدی به مردمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو آن توان و توش؟ کزین خاکدان غم</p></div>
<div class="m2"><p>خود را به آستان در دوست بردمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صافی کجا شدی دلم از دردی جهان؟</p></div>
<div class="m2"><p>گر من نه در حمایت این صاف و دردمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر شمار دیدن او نام من کجاست؟</p></div>
<div class="m2"><p>تا بعضی از جنایت او برشمردمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نقش روی خود ننهفتی ز چشم من</p></div>
<div class="m2"><p>من نام اوحدی ز ورق بر ستردمی</p></div></div>