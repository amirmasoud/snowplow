---
title: >-
    غزل شمارهٔ ۱۸۹
---
# غزل شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>خاک آن بادیم کو بر آستانت بگذرد</p></div>
<div class="m2"><p>یا شبی بر چین زلف دلستانت بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد ازین چون گرم شد بازار خورشید رخت</p></div>
<div class="m2"><p>مشتری مشنو که: از پیش دکانت بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابروانی چون کمان داری و خلقی منتظر</p></div>
<div class="m2"><p>تا کرا دوزی؟ به تیری کز کمانت بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نام من فرهاد کردند از پریشانی، ولی</p></div>
<div class="m2"><p>در زمان شیرین شود گر بر زبانت بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش تیر غم نشان کردی دلم را وانگهی</p></div>
<div class="m2"><p>من در آن تشویش کان تیر از نشانت بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ضمیر نازک اندیشت ز باریکی سخن</p></div>
<div class="m2"><p>پیکر مویی شود تا بر دهانت بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست در عشق اوحدی را جز به زاری دسترس</p></div>
<div class="m2"><p>وین نه پیکانیست کز برگستوانت بگذرد</p></div></div>