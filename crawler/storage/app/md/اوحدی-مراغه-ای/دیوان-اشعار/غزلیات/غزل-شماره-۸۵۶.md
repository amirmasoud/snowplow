---
title: >-
    غزل شمارهٔ ۸۵۶
---
# غزل شمارهٔ ۸۵۶

<div class="b" id="bn1"><div class="m1"><p>ز دست کس نکشیدم جفا و مسکینی</p></div>
<div class="m2"><p>مگر ز دست تو کافر، که دشمن دینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو دیدهٔ همه کس دیدن تو میخواهد</p></div>
<div class="m2"><p>کسی چه عیب تو گوید؟ که: خویشتن بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر پیاده روی، سرو گلشن جانی</p></div>
<div class="m2"><p>وگر سوار شوی، شمع خانهٔ زینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب شراب که باشد رخ تو شاهد و شمعی</p></div>
<div class="m2"><p>بجز لب تو نیاید بکار شیرینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندانمت که به دست که اوفتادی باز؟</p></div>
<div class="m2"><p>عجب که دست نبوسند کش تو شاهینی!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به درد مند غم او رمن که میگوید؟</p></div>
<div class="m2"><p>مکن حکایت درمان چو درد او چنین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میان به جستن یار، اوحدی،چنان دربند</p></div>
<div class="m2"><p>که تا به دست نیاید ز پای ننشینی</p></div></div>