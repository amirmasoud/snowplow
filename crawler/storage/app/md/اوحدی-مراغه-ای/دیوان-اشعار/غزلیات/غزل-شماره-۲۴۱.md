---
title: >-
    غزل شمارهٔ ۲۴۱
---
# غزل شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>سر عشق از خرد برون باشد</p></div>
<div class="m2"><p>عشق را پیشرو جنون باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند گویی که: عشق بدبختیست؟</p></div>
<div class="m2"><p>پس تو پنداشتی که چون باشد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تو بر خوان عشق خواهی بود</p></div>
<div class="m2"><p>خورشت خاک و باده خون باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقت چشم آرزومندان</p></div>
<div class="m2"><p>اثر حرقت درون باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نصیحت قرار کی گیرد؟</p></div>
<div class="m2"><p>دل ، که از عشق بی‌سکون باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی به شاخ غمش رسد دستی؟</p></div>
<div class="m2"><p>که نه در زیر این ستون باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی، گر تو صد زبان داری</p></div>
<div class="m2"><p>عاشق بی‌درم زبون باشد</p></div></div>