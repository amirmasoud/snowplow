---
title: >-
    غزل شمارهٔ ۲۰۹
---
# غزل شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>تیر از کمان به من اندازد</p></div>
<div class="m2"><p>عشق از کمین چو برون تازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درکس نیوفتد این آتش</p></div>
<div class="m2"><p>کو را چو موم بنگدازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شاه ما سپه انگیزد</p></div>
<div class="m2"><p>چون ماه ما علم افرازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دست بنده چه کار آید؟</p></div>
<div class="m2"><p>جز سرکه در قدمش بازد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن کس که غیر او داند</p></div>
<div class="m2"><p>هرگز بغیر نپردازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پرده راه ندارد کس</p></div>
<div class="m2"><p>و آنگاه پرده که او سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنوازدم چو بخواهد زد</p></div>
<div class="m2"><p>پس بهتر آنکه بننوازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس فتنها که برانگیزد</p></div>
<div class="m2"><p>آن رخ چو پرده براندازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با اوحدی غم او هر دم</p></div>
<div class="m2"><p>از گونهٔ دگر آغازد</p></div></div>