---
title: >-
    غزل شمارهٔ ۴۲۳
---
# غزل شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>عشرت بهار کن، که شود روزگار خوش</p></div>
<div class="m2"><p>می‌در بهار خور، که بود بی غبار و غش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی: به روز شش همه گیتی تمام شد</p></div>
<div class="m2"><p>می‌به، که او تمام نشد جز به ماه شش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خیز و زین قیاس دو شش ساله‌ای ببین</p></div>
<div class="m2"><p>کز حسن او کند دل ماه دو هفته غش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست ار به وصل موی میانی رسد به روز</p></div>
<div class="m2"><p>اندر میانش آر و شب اندر کنار کش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان پیش کت کشد لحد گور در کنار</p></div>
<div class="m2"><p>خالی نباید از تن خوبان کنار و کش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینجا که نقل بوسه بود زان دهان و لب</p></div>
<div class="m2"><p>دندان کس به میوه نیالاید و نمش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون دستگاه و مکنت آن هست می‌بنوش</p></div>
<div class="m2"><p>با مطربان فاخر و با شاهدان کش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کز روی همچو ماه و جبینی چو مشتری</p></div>
<div class="m2"><p>جام آفتاب رخ شود و باده زهره وش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور نیست دسترس، سر دستار پاره کن</p></div>
<div class="m2"><p>دستار رند میکده را گو: مدار فش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ریزنده کرد جنبش باد مسیح دم</p></div>
<div class="m2"><p>برگ گل از درخت چو موسی به چوب هش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت سحر ز شاخ چمن گل چو بشکفد</p></div>
<div class="m2"><p>گویی به سحر ماه بر آمد ز چاه کش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مانند آنکه بر رخ زیبا عرق چکد</p></div>
<div class="m2"><p>بر روی سرخ لاله ز شبنم فتاده رش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آشفته‌ایم و دلشده، یا مطرب «السماع»</p></div>
<div class="m2"><p>آتش‌دلیم و غمزده، یا ساقی، «العطش»</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می‌صیقلیست در کف رندان که میبرد</p></div>
<div class="m2"><p>از سینه‌ها کدورت و از دیده‌ها غمش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صوفی، بیا و در می صافی نگاه کن</p></div>
<div class="m2"><p>ور جام اوحدی نخوری، قطره‌ای بچش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر طور بزم ما دل و جانها ببین بلاش</p></div>
<div class="m2"><p>وز برق نور باده بهم بر فتاده بش</p></div></div>