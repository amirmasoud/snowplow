---
title: >-
    غزل شمارهٔ ۶۱۱
---
# غزل شمارهٔ ۶۱۱

<div class="b" id="bn1"><div class="m1"><p>قصهٔ یار سبک روح نگفتم به گرانان</p></div>
<div class="m2"><p>که چنین حال نشاید که بگویند به آنان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که جان خواسته‌ای از من بیدل، بفرستم</p></div>
<div class="m2"><p>جان چه چیزست؟ که زودش نفرستند به جانان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان به تن باز رود کشتهٔ شمشیر غمت را</p></div>
<div class="m2"><p>در لحد نام تو گر بشنود از مرثیه خوانان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر خوان خیال تو ز بس خون که بخوردیم</p></div>
<div class="m2"><p>پیر گشتیم و ز ما صرفه ببردند جوانان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به شیرین سخنی آب نمی‌یابم و کرده</p></div>
<div class="m2"><p>بارها غارت حلوای لبت چرب زبانان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال من پیش رقیبان تو دانی به چه ماند؟</p></div>
<div class="m2"><p>قصهٔ گرگ دهن بسته و انبوه شبانان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه از مدعیان واقعهٔ خود بنهفتم</p></div>
<div class="m2"><p>هیچ پوشیده نشد بر نظر واقعه دانان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بخندد لب من عیب مکن هیچ، که حالی</p></div>
<div class="m2"><p>مدتی هست که دل تنگم ازین تنگ دهانان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر رخ چون سپرش تیر نظر گر نفگندی</p></div>
<div class="m2"><p>اوحدی، زخم چرا خوردی ازین سخت کمانان؟</p></div></div>