---
title: >-
    غزل شمارهٔ ۲۶۸
---
# غزل شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>به من از دولت وصل تو مقرر می‌شد</p></div>
<div class="m2"><p>کارم از لعل گهربار تو چون زر می‌شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش گفتم: بتوان دید به خوابت، لیکن</p></div>
<div class="m2"><p>با فراق تو کرا خواب میسر می‌شد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بارها شمع بکشتم که نشینم تاریک</p></div>
<div class="m2"><p>خانه دیگر ز خیال تو منور می‌شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل دل را ز تمنای تو سعیی می‌کرد</p></div>
<div class="m2"><p>عشق می‌آمد و او نیز مسخر می‌شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه بسیار بگفتیم نیامد در گوش</p></div>
<div class="m2"><p>خوشتر از نام تو، با آنکه مکرر می‌شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرح هجران تو گفتم: بنویسم، لیکن</p></div>
<div class="m2"><p>ننوشتم ، که همه عمر در آن سر می‌شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی را غزل امروز روانست، که شب</p></div>
<div class="m2"><p>صفت خط تو می‌کرد و سخن تر می‌شد</p></div></div>