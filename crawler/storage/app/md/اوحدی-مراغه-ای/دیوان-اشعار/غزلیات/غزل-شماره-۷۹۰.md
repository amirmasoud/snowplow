---
title: >-
    غزل شمارهٔ ۷۹۰
---
# غزل شمارهٔ ۷۹۰

<div class="b" id="bn1"><div class="m1"><p>من به هر جوری نخواهم کرد زاری</p></div>
<div class="m2"><p>زانکه دولت باشد از خوی تو خواری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفته‌ای: خونت بریزم،سهل باشد</p></div>
<div class="m2"><p>بعد ازین گر بر سرم شمشیر باری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گو: بیاموز، ابر نیسانی، ز چشمم</p></div>
<div class="m2"><p>اشک باریدن در آن شبهای تاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر ندارم سر ز خاک آستانت</p></div>
<div class="m2"><p>من خود این خیر از خدا خواهم به زاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو خواهم گفت هر جوری که کردی</p></div>
<div class="m2"><p>گر نخواهی عذرم، آخر شرم داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اوحدی مقبل شود در هر دو عالم</p></div>
<div class="m2"><p>ار قبولش می‌کنی روزی به یاری</p></div></div>