---
title: >-
    غزل شمارهٔ ۲۳۲
---
# غزل شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>مستیم و مستی ما از جام عشق باشد</p></div>
<div class="m2"><p>وین نام اگر بر آریم، از نام عشق باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوابی دگر ببینیم هر شب هلاک خود را</p></div>
<div class="m2"><p>وین شیوه دلنوازی پیغام عشق باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌درد عشق منشین، کندر چنین بیابان</p></div>
<div class="m2"><p>آن کس رود به منزل کش کام عشق باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درمان دل نخواهم، تا درد مهر هستم</p></div>
<div class="m2"><p>صبح خرد نجویم، تا شام عشق باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشکفت اگر ز عشقش لاغر شویم و خسته</p></div>
<div class="m2"><p>کین شیوه لاغریها در یام عشق باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیش از اجل نبیند روی خلاص و رستن</p></div>
<div class="m2"><p>در گردنی، که بندی از دام عشق باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزی که کشته گردم بر آستانهٔ او</p></div>
<div class="m2"><p>تاریخ بهترینم ایام عشق باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشنو که: باز داند سر نیازمندان</p></div>
<div class="m2"><p>الا کسی که پایش در دام عشق باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از چشم اوحدی من خفتن طمع ندارم</p></div>
<div class="m2"><p>تا پاسبان زاری بر بام عشق باشد</p></div></div>