---
title: >-
    غزل شمارهٔ ۹۷
---
# غزل شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>این نوبت آب دیده ز هنجار دیگرست</p></div>
<div class="m2"><p>کار دلم نه بر نهج کار دیگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هیچ یار بر دلم این بار غم نبود</p></div>
<div class="m2"><p>یاران، مدد، که این ستم از یار دیگرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دردمند عشق، به درمان مدار گوش</p></div>
<div class="m2"><p>کامشب طبیب ما بر بیمار دیگرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خانه اوست چون نبود، ماه، گو: متاب</p></div>
<div class="m2"><p>وانگه به روزنی که ز دیوار دیگرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر عشق می‌زنم دگر و هر چه باد باد!</p></div>
<div class="m2"><p>ای دل، به هوش باش، که این بار دیگرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز بهر عشق هر که کمر بست بر میان</p></div>
<div class="m2"><p>نزدیک من کمر نه، که زنار دیگرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای اوحدی، مجوی تو از عشق نام و ننگ</p></div>
<div class="m2"><p>بگذر، که آن متاع به بازار دیگرست</p></div></div>