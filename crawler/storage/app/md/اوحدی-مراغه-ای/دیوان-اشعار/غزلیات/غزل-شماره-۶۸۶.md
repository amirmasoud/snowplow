---
title: >-
    غزل شمارهٔ ۶۸۶
---
# غزل شمارهٔ ۶۸۶

<div class="b" id="bn1"><div class="m1"><p>عمر که بی‌او گذشت، ذوق ندیدیم ازو</p></div>
<div class="m2"><p>دل بر شادی نخورد، تا ببریدیم ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست تمنای ما شاخ امیدی نشاند</p></div>
<div class="m2"><p>لیک به هنگام کار میوه نچیدیم ازو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند جفا گفت و زو دل نگرفتیم باز</p></div>
<div class="m2"><p>چند ستم کرد و رو در نکشیدیم ازو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه ستمگار بود خاطر ازو برنگشت</p></div>
<div class="m2"><p>ور چه جفا پیشه داشت ما نرمیدیم ازو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی چندین طلب دل چو ز باغ رخش</p></div>
<div class="m2"><p>سیب گزیدن نیافت، دست گزیدیم ازو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زو دل ما بعد ازین عشوه نخواهد خرید</p></div>
<div class="m2"><p>کاتش ما برفروخت هر چه خریدیم ازو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر زتو پرسند: کیست عاشق دیوانه؟ گو</p></div>
<div class="m2"><p>ما، که نشان وفا می‌طلبیدیم ازو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز شنیدیم: کو آتش ما می‌کشد</p></div>
<div class="m2"><p>رو، که به جز باد نیست هر چه شنیدیم ازو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سر خوان لبش، پیش حسودان ما</p></div>
<div class="m2"><p>آن همه حلوا چه سود؟ چون نچشیدیم ازو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون به در دل رسی،رنگ رخ اوحدی</p></div>
<div class="m2"><p>خود بتو گوید که: ما در چه رسیدیم ازو؟</p></div></div>