---
title: >-
    غزل شمارهٔ ۵۴۵
---
# غزل شمارهٔ ۵۴۵

<div class="b" id="bn1"><div class="m1"><p>وه! که امروز چه آشفته و بی‌خویشتنم</p></div>
<div class="m2"><p>دشمنم باد بدین شیوه که امروز منم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد چو مویی تنم از غصهٔ نادیدن تو</p></div>
<div class="m2"><p>رحمتی کن، که ز هجر تو چو موییست تنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اثری نیست درین پیرهن از هستی من</p></div>
<div class="m2"><p>وین تو باور نکنی، تا نکنی پیرهنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهنت دیدم و تنگ شکرم یاد آمد</p></div>
<div class="m2"><p>سخنی گفتی و از یاد برفت آن سخنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دهان تو چو خواهم که حدیثی گویم</p></div>
<div class="m2"><p>یاوه گردد سخن از نازکی اندر دهنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بمیرم من و آیی به نمازم بیرون</p></div>
<div class="m2"><p>تا لب گور به ده جای بسوزد کفنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش عشق تو از سینهٔ من ننشیند</p></div>
<div class="m2"><p>مگر آن روز که در خاک نشانی بدنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلق گویند: برو توبه کن از شیوهٔ عشق</p></div>
<div class="m2"><p>می‌کنم توبه ولی بار دگر می‌شکنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر زند بر جگرم چشم تو هر دم تیری</p></div>
<div class="m2"><p>اوحدی نیستم، ار پیش رخت دم بزنم</p></div></div>