---
title: >-
    غزل شمارهٔ ۵۸۴
---
# غزل شمارهٔ ۵۸۴

<div class="b" id="bn1"><div class="m1"><p>آن پرده برانداز، که ما نور پرستیم</p></div>
<div class="m2"><p>مستور چرایی؟ چو نه مستورپرستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیری اگر آن روی به دوری بپرستید</p></div>
<div class="m2"><p>ما صبر نداریم که از دور پرستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلق از هوس حور طلب گار بهشتند</p></div>
<div class="m2"><p>وانگاه بهشتی تو، که ما حور پرستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را غرض از دیدن خوبان صفت تست</p></div>
<div class="m2"><p>گر بهر تجلی بود، ار طور پرستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روشن به چراغی شده هر خانه که بینی</p></div>
<div class="m2"><p>ما نور تو بینیم و همان نور پرستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان خرمگسان دور، که ما نوش لبت را</p></div>
<div class="m2"><p>زنار فرو بسته چو زنبور پرستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوته نظران روی به گلزار نهادند</p></div>
<div class="m2"><p>ماییم که آن نرگس مخمور پرستیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با هجر تو ممکن نشد اندیشهٔ شادی</p></div>
<div class="m2"><p>کین ماتم از آن نیست که ما سور پرستیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اصحاب ضلال از بت و از خشت چه بینند؟</p></div>
<div class="m2"><p>در صدر نشین، تا بت مشهور پرستیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر کفر بود کشتن نفسی، به حقیقت</p></div>
<div class="m2"><p>ما نفس کشان کافر کافور پرستیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>امروز که گشت اوحدی از هجر تو رنجور</p></div>
<div class="m2"><p>بیرون نتوان رفت، که رنجور پرستیم</p></div></div>