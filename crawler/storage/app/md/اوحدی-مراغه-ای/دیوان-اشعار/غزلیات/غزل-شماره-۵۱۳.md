---
title: >-
    غزل شمارهٔ ۵۱۳
---
# غزل شمارهٔ ۵۱۳

<div class="b" id="bn1"><div class="m1"><p>تا میسر گشت در گرمابه وصل آن نگارم</p></div>
<div class="m2"><p>در دل و چشم آتش و آب دوصد گرمابه دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سرش تا گل بدیدم پای صبر خویشتن را</p></div>
<div class="m2"><p>در گلی دیدم، کزان گل راه بیرون شد ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سنگ چون بر پای او زد بوسه رفت از دست هوشم</p></div>
<div class="m2"><p>شانه چون در زلف او زد دست برد از دل قرارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست من چون شانه در زلفش نخواهد رفت، لیکن</p></div>
<div class="m2"><p>گر چو سنگ از پای او سرباز گیرم سنگسارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون من می‌ریخت همچون آب حوض آن ماه و دیگر</p></div>
<div class="m2"><p>گرد پای حوض می‌کشت این دل مجروح زارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر تن چون گل همی پوشیده مشکین زلف، یعنی</p></div>
<div class="m2"><p>خرمنی گل در میان تودهٔ مشک تتارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناخنش در خون خود می‌دیدم و در ناخن خود</p></div>
<div class="m2"><p>آن قدر قوت نمی‌دیدم که پشت خود بخارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سر من آب می‌کردند و می‌گفتم: رها کن</p></div>
<div class="m2"><p>تا به آب دیدهٔ‌خود پیش او غسلی بر آرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عکس طاس و نور تشتش تا به چشم من درآمد</p></div>
<div class="m2"><p>شد ز خون دل چو طاسی چشم و چون تشتی کنارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌جمال او دو طاس خون شد ستم چشم و هر دم</p></div>
<div class="m2"><p>چون بگریم زین دو طاس خون کم از تشتی نبارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این دو طاس خون ز چشم خلق پنهان می‌کنم من</p></div>
<div class="m2"><p>تا بدانی کز غمش جز طاس بازی نیست کارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عزم حمامش کدامین روز خواهد بود دیگر؟</p></div>
<div class="m2"><p>این بمن گویید، تا من نیز روزی می‌شمارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر کند نقاش بر گرمابه نقش صورت او</p></div>
<div class="m2"><p>سالها چون نقش از آن گرمابه سر بیرون نیارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من فقاع از عشق آن رخ بعد ازین خواهم گشودن</p></div>
<div class="m2"><p>چون فقاعم عیب نتوان کرد اگر جوشی برآرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اوحدی، تا دل به حمام در آوردست ازین پی</p></div>
<div class="m2"><p>بار دیگر چون برآیم دل به حمامی سپارم</p></div></div>