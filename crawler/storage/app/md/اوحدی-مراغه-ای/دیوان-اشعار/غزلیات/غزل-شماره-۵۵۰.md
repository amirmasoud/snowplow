---
title: >-
    غزل شمارهٔ ۵۵۰
---
# غزل شمارهٔ ۵۵۰

<div class="b" id="bn1"><div class="m1"><p>پر از دل مپرس، ای پری، من چه دانم؟</p></div>
<div class="m2"><p>ز مردم تو دل می‌بری، من چه دانم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه گویی: بدان تا کجا شد دل تو؟</p></div>
<div class="m2"><p>ز من چون تو داناتری، من چه دانم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا چند پرسی که: لاغر چرایی؟</p></div>
<div class="m2"><p>تو این بنده می‌پروری، من چه دانم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز من صبر جستی و عقل و سکونت</p></div>
<div class="m2"><p>پریشانم، این داوری من چه دانم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمودی که: چون فاش گردید رازت؟</p></div>
<div class="m2"><p>تو این پرده‌ها میدری، من چه دانم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مپرس اینکه: دیوانه چون شد دل تو؟</p></div>
<div class="m2"><p>به دست تو بود، ای پری، من چه دانم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگو: کاوحدی چون خریدار من شد؟</p></div>
<div class="m2"><p>تویی ماه و او مشتری، من چه دانم؟</p></div></div>