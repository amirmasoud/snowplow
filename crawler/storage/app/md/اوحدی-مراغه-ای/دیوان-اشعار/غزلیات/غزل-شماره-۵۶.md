---
title: >-
    غزل شمارهٔ ۵۶
---
# غزل شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>بت خورشید رخ من به گذارست امشب</p></div>
<div class="m2"><p>شب روان را رخ او مشعله دارست امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک مشکست و زمین عنبر و دیوار عبیر</p></div>
<div class="m2"><p>باد گل بوی و هوا غالیه بارست امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدهٔ آن که نمی‌خفت و سعادت می‌جست</p></div>
<div class="m2"><p>گو: نگه کن، که سعادت بگذارست امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن بهشتی، که ترا وعده به فردا دادند</p></div>
<div class="m2"><p>همه در حلقهٔ آن زلف چو مارست امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل این باغچه بی‌خار نباشد فردا</p></div>
<div class="m2"><p>گل بچینید، که بی‌زحمت خارست امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عید را قدر نباشد بر شبهای چنین</p></div>
<div class="m2"><p>روز نوروز خود اندر چه شمارست امشب؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا قبولت نکند یار نیابی اقبال</p></div>
<div class="m2"><p>مقبل آنست که در صحبت یارست امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماهرویی که ز ما پرده همی کرد و حجاب</p></div>
<div class="m2"><p>پرده از روی بر انداخت که: بارست امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوست حاضر شده ناخوانده و دشمن غایب</p></div>
<div class="m2"><p>اوحدی، پرورش روح چه کارست امشب؟</p></div></div>