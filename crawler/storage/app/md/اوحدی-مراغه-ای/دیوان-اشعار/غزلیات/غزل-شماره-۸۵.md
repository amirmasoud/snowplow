---
title: >-
    غزل شمارهٔ ۸۵
---
# غزل شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>یارب، این مهمان چون ماه از کجاست؟</p></div>
<div class="m2"><p>وین سپاه کیست و آن شاه از کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عکس خورشیدی چنان بالا بلند</p></div>
<div class="m2"><p>بر چنین دیوار کوتاه از کجاست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز مرغ جان به شاخ دل رسید</p></div>
<div class="m2"><p>غلغل «انی انا الله» از کجاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل درین وادی ز تاریکی بسوخت</p></div>
<div class="m2"><p>سوی آن آتش بگو راه از کجاست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه خونریزیست این فریاد چیست؟</p></div>
<div class="m2"><p>ورنه بیدادست این آه از کجاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندرین خرگاه می‌گویند: هست</p></div>
<div class="m2"><p>خوبرویی، راه خرگاه از کجاست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی را پادشاهی بنده خواند</p></div>
<div class="m2"><p>مفلسی را دیگر این جاه از کجاست؟</p></div></div>