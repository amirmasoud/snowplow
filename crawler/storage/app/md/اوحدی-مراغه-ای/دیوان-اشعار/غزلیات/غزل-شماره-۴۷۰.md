---
title: >-
    غزل شمارهٔ ۴۷۰
---
# غزل شمارهٔ ۴۷۰

<div class="b" id="bn1"><div class="m1"><p>من درین شهر پای بند توام</p></div>
<div class="m2"><p>عاشق قامت بلند توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردهٔ آن دهان چون پسته</p></div>
<div class="m2"><p>کشتهٔ آن لب چو قند توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌دوانی و می‌کشی زارم</p></div>
<div class="m2"><p>چون بدیدی که در کمند توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای هلاک دلم پسندیده</p></div>
<div class="m2"><p>دولتی باشد از پسند توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گذری می‌کن، ار طبیب منی</p></div>
<div class="m2"><p>آتشی می‌نه، ار سپند توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو: رفیقان سفر کنند که من</p></div>
<div class="m2"><p>نتوانم، که پای بند توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اوحدی باز پرس حال، که من</p></div>
<div class="m2"><p>تا چه غایت نیازمند توام؟</p></div></div>