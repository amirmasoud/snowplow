---
title: >-
    غزل شمارهٔ ۱۱۴
---
# غزل شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>مرا سر بلندی ز سودای اوست</p></div>
<div class="m2"><p>سری دوست دارم که در پای اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مزاج دلم گرم از آن می‌شود</p></div>
<div class="m2"><p>که بر مهر روی دلارای اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا زیبد ار لاف شاهی زنم</p></div>
<div class="m2"><p>که در سینه گنج تمنای اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیابی در اجزای من دره‌ای</p></div>
<div class="m2"><p>که آن ذره خالی ز سودای اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرم جای شور و تنم جای شوق</p></div>
<div class="m2"><p>لبم جای ذکر و دلم جای اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که نزدیک لیلی خبر می‌برد؟</p></div>
<div class="m2"><p>که: مجنون آشفته شیدای اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل اوحدی کی برآید ز بند؟</p></div>
<div class="m2"><p>که در بند زلف سمن سای اوست</p></div></div>