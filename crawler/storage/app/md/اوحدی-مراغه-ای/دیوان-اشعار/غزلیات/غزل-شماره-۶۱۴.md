---
title: >-
    غزل شمارهٔ ۶۱۴
---
# غزل شمارهٔ ۶۱۴

<div class="b" id="bn1"><div class="m1"><p>تا به کی این بستن و بگسیختن؟</p></div>
<div class="m2"><p>سیر نگشتی تو ز خون ریختن؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست چنین مست شدن وانگهی</p></div>
<div class="m2"><p>با من بیچاره بر آویختن؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر لب بدخواه زدن آب وصل</p></div>
<div class="m2"><p>وز تن من گرد بر انگیختن؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیم تنا، خوش عملی نیست این</p></div>
<div class="m2"><p>دل ز کسان بردن و بگریختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پردهٔ صد دل به دریدن به جور</p></div>
<div class="m2"><p>پردهٔ رخسار در آویختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک توام، ای پسر، آخر چراست؟</p></div>
<div class="m2"><p>بر سر ما خاک جفا بیختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست ندارد ز تو باز اوحدی</p></div>
<div class="m2"><p>گر چه نداری سر آمیختن</p></div></div>