---
title: >-
    غزل شمارهٔ ۲۱۴
---
# غزل شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>چو میل او کنم، از من به عشوه بگریزد</p></div>
<div class="m2"><p>دگر چو روی به پیچم به من در آویزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر برابرش آیم به خشم برگردد</p></div>
<div class="m2"><p>وگر برش بنشینم به طیره برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رغم من برود هر زمان، که در نظرم</p></div>
<div class="m2"><p>کسی بجوید و با مهر او در آمیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی که بر سر کویش گذر کنم چون باد</p></div>
<div class="m2"><p>رقیب او ز جفا خاک بر سرم بیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و گر به چشم نیازش نگه کنم روزی</p></div>
<div class="m2"><p>به خشم درشود و فتنه‌ای برانگیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آتشم من و جز دیده کس نمی‌بینم</p></div>
<div class="m2"><p>که بی‌مضایقه آبی بر آتشم ریزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه کار ماست چنین دوستی، ولی چه کنم؟</p></div>
<div class="m2"><p>که اوحدی ز چنین کارها نپرهیزد</p></div></div>