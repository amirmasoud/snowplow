---
title: >-
    غزل شمارهٔ ۳۹۴
---
# غزل شمارهٔ ۳۹۴

<div class="b" id="bn1"><div class="m1"><p>کاکل کافرانه بین، زیور گوش او نگر</p></div>
<div class="m2"><p>و آن مغلی مغول‌ها بر سر و دوش او نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ قمر کجا بری؟ روی چو ماه او ببین</p></div>
<div class="m2"><p>تنگ شکر چه می‌کنی؟ لعل خموش او نگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیوه‌کنان چو بگذرد بر سر اسب گوی‌زن</p></div>
<div class="m2"><p>تندی مرکبش ببین، گرمی و جوش او نگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عجبی ز حیرتم، در رخ چون نگار او؟</p></div>
<div class="m2"><p>حیرت من چه می‌کنی؟ بردن هوش او نگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر به رخش نگه کنم، بهر نگاه کردنی</p></div>
<div class="m2"><p>زهر مریز بر دلم، چشمهٔ نوش او نگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست شبانه بامداد، آمد و کرد قتل ما</p></div>
<div class="m2"><p>فتنهٔ روز ما ببین، مستی دوش او نگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که به وقت تاختن غارت او ندیده‌ای</p></div>
<div class="m2"><p>حجرهٔ اوحدی ببین، خانه فروش او نگر</p></div></div>