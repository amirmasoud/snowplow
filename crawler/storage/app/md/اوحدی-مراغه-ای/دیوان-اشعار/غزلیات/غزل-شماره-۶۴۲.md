---
title: >-
    غزل شمارهٔ ۶۴۲
---
# غزل شمارهٔ ۶۴۲

<div class="b" id="bn1"><div class="m1"><p>عشق را فرسوده‌ای باید چو من</p></div>
<div class="m2"><p>در مشقت بوده‌ای باید چو من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لایق سودای آن جان و جهان</p></div>
<div class="m2"><p>از جهان آسوده‌ای باید چو من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا غم او را به کار آید مگر</p></div>
<div class="m2"><p>کار غم فرسوده‌ای باید چو من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از برای خوردن حلوای غم</p></div>
<div class="m2"><p>خون دل پالوده‌ای باید چو من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انتظار دیدن آن ماه را</p></div>
<div class="m2"><p>سالها نغنوده‌ای باید چو من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ز وصل او به درمانی رسد</p></div>
<div class="m2"><p>درد دل پیموده‌ای باید چو من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی، راه غم آن دوست را</p></div>
<div class="m2"><p>خاک و خون آلوده‌ای باید چو من</p></div></div>