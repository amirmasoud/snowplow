---
title: >-
    غزل شمارهٔ ۳۹۱
---
# غزل شمارهٔ ۳۹۱

<div class="b" id="bn1"><div class="m1"><p>دلبر من بر گذشت همچو بهاری دگر</p></div>
<div class="m2"><p>بر رخش از هفت ونه، نقش و نگاری دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش: ای جان، بیا، دست به یاری بده</p></div>
<div class="m2"><p>گفت: نیارم، که هست به ز تو یاری دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش: آخر مکن بیش کنار از برم</p></div>
<div class="m2"><p>گفت: دلم می‌کند میل کناری دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش: از هجر تو گشت نهارم چو لیل</p></div>
<div class="m2"><p>گفت: ازین پیش بود لیل و نهاری دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش: از وصل تو آن من خسته کو؟</p></div>
<div class="m2"><p>گفت که: فردا کنم بر تو گذاری دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش: امروز کن، گر گذری میکنی</p></div>
<div class="m2"><p>گفت که: فردا کنم بر تو گذاری دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش: از کار تو نیک فرو مانده‌ام</p></div>
<div class="m2"><p>گفت: برو بعد ازین در پی کاری دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتمش: ای بی‌وفا، عهد همین بود و مهر؟</p></div>
<div class="m2"><p>گفت که: می‌آورند چند قطاری دگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتمش: آن دل که من پیش تو دارم، بده</p></div>
<div class="m2"><p>گفت: به از من ببین مظلمه‌داری دگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتمش: ار دیگری عاشق زارم کند؟</p></div>
<div class="m2"><p>گفت: به دست آورم عاشق زاری دگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتمش: ار اوحدی نیست شود در غمت</p></div>
<div class="m2"><p>گفت: به از اوحدی هست هزاری دگر</p></div></div>