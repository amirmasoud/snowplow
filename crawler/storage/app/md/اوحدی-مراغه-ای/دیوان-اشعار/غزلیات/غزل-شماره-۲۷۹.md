---
title: >-
    غزل شمارهٔ ۲۷۹
---
# غزل شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>رخ تو به جز جور و خواری نداند</p></div>
<div class="m2"><p>دل من به جز بردباری نداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بی یارمندی بنالند مردم</p></div>
<div class="m2"><p>من از یارمندی، که یاری نداند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز روزم چه پرسی؟ که چشم ترمن</p></div>
<div class="m2"><p>بجز رنگ شبهای تاری نداند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من از داغ هجر تو هردم به نوعی</p></div>
<div class="m2"><p>بگریم، که ابر بهاری نداند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان نقش رویت گرفتست چشمم</p></div>
<div class="m2"><p>که نقش منش پیش داری نداند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پیش دلم شادمانی چه جویی؟</p></div>
<div class="m2"><p>که غم دید و جز سوگواری نداند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم دانشی کز جهان کرد حاصل</p></div>
<div class="m2"><p>گران نیز یادش نیاری نداند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عشق تو زارند خلقی ولیکن</p></div>
<div class="m2"><p>کس این شیوه فریاد و زاری نداند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روانم ز جور لبت چون نسوزد؟</p></div>
<div class="m2"><p>که با اوحدی سازگاری نداند</p></div></div>