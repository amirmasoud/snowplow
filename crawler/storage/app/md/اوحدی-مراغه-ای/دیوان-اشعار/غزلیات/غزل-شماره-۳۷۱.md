---
title: >-
    غزل شمارهٔ ۳۷۱
---
# غزل شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>دلی که در سر زلف شما همی آید</p></div>
<div class="m2"><p>به پای خویش به دام بلا همی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آستان تو موقوفم، ای سعادت آن</p></div>
<div class="m2"><p>کز آستان تو اندر سرا همی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشانه جز دل ما نیست تیر چشم ترا</p></div>
<div class="m2"><p>اگر صواب رود ور خطا همی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بر تو به پا آمدم مرنج، که زود</p></div>
<div class="m2"><p>به سر برون رود آن کو به پا همی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دست حیلت و افسون سپر نشاید ساخت</p></div>
<div class="m2"><p>بر آن رمیده که تیر قضا همی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم شکایت بیگانگان چگونه کند؟</p></div>
<div class="m2"><p>چو بر من این همه از آشنا همی آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم آتشیست که در جان اوحدی زده‌ای</p></div>
<div class="m2"><p>و گرنه این همه دود از کجا همی آید؟</p></div></div>