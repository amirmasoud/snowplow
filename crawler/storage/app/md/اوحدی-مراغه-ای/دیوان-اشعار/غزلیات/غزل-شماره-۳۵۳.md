---
title: >-
    غزل شمارهٔ ۳۵۳
---
# غزل شمارهٔ ۳۵۳

<div class="b" id="bn1"><div class="m1"><p>بی‌تو دل و جان من زیر و زبر می‌شود</p></div>
<div class="m2"><p>دم به دمم درد دل بیش و بتر می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر به سر شد مرا در غم هجران تو</p></div>
<div class="m2"><p>تا تو نگویی: مرا بی‌تو به سر می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از رخ چون شمع خود روشنییی پیش تو</p></div>
<div class="m2"><p>کین شب تاریک ما دیر سحر می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند بپوشیدم این راز دل و خلق را</p></div>
<div class="m2"><p>از سخن عاشقان زود خبر می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه تو خواهی بگوی، کین همه دشنام تلخ</p></div>
<div class="m2"><p>چون به لبت می‌رسد شهد و شکر می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نه دل اوحدی سوخته‌ای، هر دمش</p></div>
<div class="m2"><p>سینه چه جان می‌کند، دیده چه تر می‌شود؟</p></div></div>