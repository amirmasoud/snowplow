---
title: >-
    غزل شمارهٔ ۱۸
---
# غزل شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>اگر یک سو کنی زان رخ سر زلف چو سنبل را</p></div>
<div class="m2"><p>ز روی لاله رنگ خود خجالت‌ها دهی گل را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا پیش لب لعل تو سربازیست در خاطر</p></div>
<div class="m2"><p>اگر چه پیش روی تو سربازیست کاکل را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ و زلف تو بس باشد ز بهر حجت و برهان</p></div>
<div class="m2"><p>اگر دعوی کند وقتی کسی دور تسلسل را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تجمل روی خوبان را بیاراید ولیکن تو</p></div>
<div class="m2"><p>رخی داری که از خوبی بیاراید تجمل را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نباید گوش مالیدن مرا در عشق و نالیدن</p></div>
<div class="m2"><p>اگر گل زین صفت باشد غرامت نیست بلبل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قرنفل در دهان داری، که هنگام سخن گفتن</p></div>
<div class="m2"><p>به صحرا می‌برد ز آن لب صبابوی قرنفل را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآید نالهٔ «دل دل» ز هر سو چون برانگیزی</p></div>
<div class="m2"><p>به روز کشتن و غارت غبار نعل دلدل را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی‌گفتی: به فضل خود ببخشایم بسی بر تو؟</p></div>
<div class="m2"><p>کنون وقت آمد انعام و احسان و تفضل را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز عشقش توبه بشکستم بگیر ای اوحدی، دستم</p></div>
<div class="m2"><p>و گر باور نمی‌داری بیار آن ساغر مل را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جمالش کرد حیرانم، چه ماهست آن؟ نمی‌دانم</p></div>
<div class="m2"><p>که چشم از کشف ماهیت نمی‌بندد تامل را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهل، تا می‌کند خواری، که با او هم کند یاری</p></div>
<div class="m2"><p>چو جانم میل او دارد نهادم دل تحمل را</p></div></div>