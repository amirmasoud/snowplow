---
title: >-
    غزل شمارهٔ ۳۶۷
---
# غزل شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>دل سرمست من آن نیست که باهوش آید</p></div>
<div class="m2"><p>مگر آن لحظه کش آواز تو در گوش آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخت این آتش سوزنده که در سینه نهاد</p></div>
<div class="m2"><p>عجب از دیگ هوس نیست که در جوش آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز آن کایم و در پای غلامان افتم</p></div>
<div class="m2"><p>چه غلامی ز من بی‌تن و بی‌توش آید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شربت قند رها کن، که از آن ساعد و دست</p></div>
<div class="m2"><p>اگرم زهر دهی بر دل من نوش آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگرم داعیهٔ لطف تو بگشاید چشم</p></div>
<div class="m2"><p>ورنه از من چه سکون و ادب و هوش آید؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن پنهان تو بر خاطر من سهل کند</p></div>
<div class="m2"><p>هر چه از جور رقیبان جفا کوش آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر نیازست و دعا دست جهانی زن و مرد</p></div>
<div class="m2"><p>تا کرا گوهر آن گنج در آغوش آید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیم آنست که: از فکرت و اندیشهٔ تو</p></div>
<div class="m2"><p>همه تحصیل که کردیم فراموش آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بید با قامت رعنای چنان شرط آنست</p></div>
<div class="m2"><p>که به سر پیش تو، ای سرو قباپوش، آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عجب از طالع خود دارم و دوران فلک</p></div>
<div class="m2"><p>کان چنان صید به دام من مدهوش آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اوحدی وقت سخن گر چه گهر بارد و در</p></div>
<div class="m2"><p>پیش لعل لب گویای تو خاموش آید</p></div></div>