---
title: >-
    غزل شمارهٔ ۱۳۰
---
# غزل شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>گر سری در سر کار تو شود چندان نیست</p></div>
<div class="m2"><p>با تو سختی به سری کار خردمندان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردن ما ز بسی دام برون جست و کنون</p></div>
<div class="m2"><p>سر نهادیم به بند تو، که این بند آن نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل، از میل به چاه زنخ او داری</p></div>
<div class="m2"><p>به گنه کوش، که زیباتر ازین زندان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمس را دیدم و مثل قمرش نور نداشت</p></div>
<div class="m2"><p>پسته را دیدم و همچون شکرش خندان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگ جانی، که به سیمین تن او دل ندهد</p></div>
<div class="m2"><p>بیش ازینش تو مخوان دل، که کم از سندان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جهان نوش لبی را نشناسم امروز</p></div>
<div class="m2"><p>که غلام دهن او ز بن دندان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتسب را اگر آن چهره در آید به نظر</p></div>
<div class="m2"><p>عذرها خواهد و گوید: گنه از رندان نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اوحدی شاد شو از دیدن این روی و مخور</p></div>
<div class="m2"><p>غم بی‌فایده چندین، که جهان چندان نیست</p></div></div>