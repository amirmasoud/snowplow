---
title: >-
    غزل شمارهٔ ۷۰۸
---
# غزل شمارهٔ ۷۰۸

<div class="b" id="bn1"><div class="m1"><p>چیست آن شهریار در پرده؟</p></div>
<div class="m2"><p>شور در شهر و یار در پرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر زمان بار می‌دهد، لیکن</p></div>
<div class="m2"><p>نیست امکان بار درپرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرده از روی برگرفت آن ماه</p></div>
<div class="m2"><p>همچنان روی کار در پرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه گلها ازو شکفته و باز</p></div>
<div class="m2"><p>گل او غنچه‌وار در پرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده داری بدوستان دادست</p></div>
<div class="m2"><p>و آنگه آن پرده دار در پرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چیست این نقش گونه‌گون؟ ار نیست</p></div>
<div class="m2"><p>نقشبندی سوار در پرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پس پرده جمله حیرانند</p></div>
<div class="m2"><p>کس ندارد گذار در پرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه را رخ به خون دیده نگار</p></div>
<div class="m2"><p>نیست کس با نگار در پرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نخواهی که: گم شوی از خود</p></div>
<div class="m2"><p>نروی زینهار! در پرده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رانی، این پرده را چو راست کنند</p></div>
<div class="m2"><p>نالهٔ زار زار در پرده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از برون گر هزار بینی، نیست</p></div>
<div class="m2"><p>جز یکی زان هزار در پرده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم تویی پردهٔ بصیرت تو</p></div>
<div class="m2"><p>خویشتن را مدار در پرده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پردهٔ خویش را بسوز و ببین</p></div>
<div class="m2"><p>دوست را آشکار در پرده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرو، ار پرده در میان بینی</p></div>
<div class="m2"><p>پرده بین را چکار در پرده؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو که چون شیر پرده پشمینی</p></div>
<div class="m2"><p>چون بگیری شکار در پرده؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رفع این پرده یک نفس کارست</p></div>
<div class="m2"><p>مبر این روزگار در پرده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر آن رخ جمال بنماید</p></div>
<div class="m2"><p>نهلد پود و تار در پرده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پرده زان دیده‌ای، که هست ترا</p></div>
<div class="m2"><p>دیدهٔ اعتبار در پرده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نظر جهل چون تواند دید</p></div>
<div class="m2"><p>یار در غار و غار در پرده؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر که او اختیار خود بگذاشت</p></div>
<div class="m2"><p>رفت بی‌اختیار در پرده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر درین پرده میروی، ایدل</p></div>
<div class="m2"><p>اوحدی را میار در پرده</p></div></div>