---
title: >-
    غزل شمارهٔ ۳۳۵
---
# غزل شمارهٔ ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>دوش بی‌روی تو باغ عیش را آبی نبود</p></div>
<div class="m2"><p>مرغ و ماهی خواب کردند و مرا خوابی نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کتاب طالع شوریده می‌کردم نظر</p></div>
<div class="m2"><p>بهتر از خاک درت روی مرا آبی نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با خیال پرتو رخسار چون خورشید تو</p></div>
<div class="m2"><p>چشم دل را حاجب شمعی و مهتابی نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم من توفان همی بارید در پای غمت</p></div>
<div class="m2"><p>گر چه از گرمی دلم را در جگر آبی نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نماز از دل بهر جانب که می‌کردم نگاه</p></div>
<div class="m2"><p>عقل را جز طاق ابروی تو محرابی نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز لب خوشیده و چشم تر اندر هجر تو</p></div>
<div class="m2"><p>از تر و خشک جهانم برگ و اسبابی نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی را دامن اندر دوستی شد غرق خون</p></div>
<div class="m2"><p>زانکه بحر دوستی را هیچ پایابی نبود</p></div></div>