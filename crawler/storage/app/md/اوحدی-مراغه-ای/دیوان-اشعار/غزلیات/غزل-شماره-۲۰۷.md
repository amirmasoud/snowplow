---
title: >-
    غزل شمارهٔ ۲۰۷
---
# غزل شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>صفات قلندر نشان برنگیرد</p></div>
<div class="m2"><p>صفات تجرد بیان برنگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عدم خانهٔ نیستی راست گنجی</p></div>
<div class="m2"><p>که وصلش وجود جهان برنگیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشاد از دل تنگ درویش یابد</p></div>
<div class="m2"><p>خدنگی که هیچش کمان برنگیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من آن خاکسارم، که گر برگذاری</p></div>
<div class="m2"><p>بیفتم، کسم رایگان برنگیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بالای من بر کشیدند دلقی</p></div>
<div class="m2"><p>که پهنای هفت آسمان برنگیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل دین طلب تنگ تن برنتابد</p></div>
<div class="m2"><p>تن راهرو بار جان برنگیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن یاد دنیا، که اندیشهٔ ما</p></div>
<div class="m2"><p>هماییست کین استخوان برنگیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ما گوهری داد دست عنایت</p></div>
<div class="m2"><p>که اندازهٔ بحر و کان برنگیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو سرمایه بسیار گردان، که دل را</p></div>
<div class="m2"><p>چو سرمایه پر شد زیان برنگیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبان درکش،ای اوحدی، زین حکایت</p></div>
<div class="m2"><p>که ناگه سرت با زبان برنگیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازان یار بیگانگی دارد آن کس</p></div>
<div class="m2"><p>که پندار خویش از میان برنگیرد</p></div></div>