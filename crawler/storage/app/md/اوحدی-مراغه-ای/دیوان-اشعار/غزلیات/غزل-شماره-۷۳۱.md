---
title: >-
    غزل شمارهٔ ۷۳۱
---
# غزل شمارهٔ ۷۳۱

<div class="b" id="bn1"><div class="m1"><p>بسیار دشمنست مرا و تو دوست نه</p></div>
<div class="m2"><p>با دوستان خویشتن اینها نکوست؟ نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من سال و ماه در سخن و گفت و گوی تو</p></div>
<div class="m2"><p>وانگه تو با کسی که درین گفت و گوست نه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با من هزار تندی و تیزی نموده‌ای</p></div>
<div class="m2"><p>گفتم به هیچ کس که: فلان تندخوست؟ نه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای عاشقان موی تو افزون ز موی سر</p></div>
<div class="m2"><p>زیشان چو من ز مویه کسی همچو موست؟ نه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلقی به بوی زلف تو از خویش رفته‌اند</p></div>
<div class="m2"><p>کس را وقوف هست که آن خود چه بوست؟ نه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویند: ترک او کن و یاری دگر بگیر</p></div>
<div class="m2"><p>اندر جهان حسن کسی مثل اوست؟ نه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای قیمتی چو جان بر ما خاک کوی تو</p></div>
<div class="m2"><p>ما را بر تو قیمت آن خاک کوست؟ نه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهری به آرزوی تو از جان برآمدند</p></div>
<div class="m2"><p>کس را برآمدی ز تو جز آرزوست؟ نه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با اوحدی طریق جدایی گرفته‌ای</p></div>
<div class="m2"><p>ای پاردوست بوده و امسال دوست نه</p></div></div>