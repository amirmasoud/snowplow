---
title: >-
    غزل شمارهٔ ۳۲۴
---
# غزل شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>همیشه تا تن من برقرار خواهد بود</p></div>
<div class="m2"><p>به کوی عشق دلم را گذار خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرم به خاک بپوسید و آتش غم دوست</p></div>
<div class="m2"><p>در استخوان تن من به کار خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بتا، بدور غم خویش کشته گیر مرا</p></div>
<div class="m2"><p>جنایت تو اگر زین شمار خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بهر کشتن من چرخ تیز می‌بینم</p></div>
<div class="m2"><p>که باستیزهٔ چشم تو یار خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلای عشق تو خوش کرده‌ایم با دل خود</p></div>
<div class="m2"><p>به بوی آنکه خزان را بهار خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم ز هجر تو اندر حساب داشت غمی</p></div>
<div class="m2"><p>گمان که برد که چندین هزار خواهد بود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا، که تا نبود پیشت اوحدی را باز</p></div>
<div class="m2"><p>همیشه دیدهٔ او اشکبار خواهد بود</p></div></div>