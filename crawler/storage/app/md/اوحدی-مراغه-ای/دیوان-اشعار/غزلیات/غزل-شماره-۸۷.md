---
title: >-
    غزل شمارهٔ ۸۷
---
# غزل شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>نوبهارست و چمن خرم و گلزار اینجاست</p></div>
<div class="m2"><p>ارم دیده و آرام دل زار اینجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر خار چمن روی بمالیم چو گل</p></div>
<div class="m2"><p>گر بدانیم که باز آن گل بی‌خار اینجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن از آنجا نشکیبد، دلم اینجا چون نیست</p></div>
<div class="m2"><p>دلم آنجا ننشیند، که مرا یار اینجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب ار تا به ابد روی رهایی بیند</p></div>
<div class="m2"><p>این دل خسته که محبوس و گرفتار اینجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکرم زان لب و سیب از رخ و نار از سینه</p></div>
<div class="m2"><p>نفرستاد، چو دانست که: بیمار اینجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرم نیز بگوید که: دل خویش ببر</p></div>
<div class="m2"><p>روی آوردن او نیست، که دلدار اینجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی آن نیست که: این جا بنشیند بی‌کار</p></div>
<div class="m2"><p>دل آشفتهٔ ما را، که سر و کار اینجاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از وجود من اگر اندک و بسیاری ماند</p></div>
<div class="m2"><p>اندک اینست که می‌بینی و بسیار اینجاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر من این جا تو اگر عرضه کنی هشت بهشت</p></div>
<div class="m2"><p>ندهم دل به بهشت تو، که دیدار اینجاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می به دست من سر گشته اگر خواهی داد</p></div>
<div class="m2"><p>هم ازین میکده درخواه، که دستار اینجاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر چه در جملهٔ خوبان طلبیدی از حسن</p></div>
<div class="m2"><p>به رخ دوست نظر کن، که به یک بار اینجاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش شکر دهنش بار شکر نگشایند</p></div>
<div class="m2"><p>چو ببینند که: آن قند به خروار اینجاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بجز او کس نشناسم که بجوید دل ما</p></div>
<div class="m2"><p>بفرست، اوحدی، آن دل، که خریدار اینجاست</p></div></div>