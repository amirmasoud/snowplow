---
title: >-
    غزل شمارهٔ ۱۲۷
---
# غزل شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>چه دستها، که ز دست غم تو بر سر نیست؟</p></div>
<div class="m2"><p>چه دیدها؟ که ز نادیدنت به خون تر نیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کدام پشت، که در عهد زلف چون رسنت</p></div>
<div class="m2"><p>ز بس کشیدن بار بلا چو چنبر نیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حکایتی که مرا از غم تو نقش دلست</p></div>
<div class="m2"><p>اگر قیاس کنی در هزار دفتر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار جامهٔ پرهیز دوختیم و هنوز</p></div>
<div class="m2"><p>نظر ز روی تو بر دوختن میسر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شام تا به سحر، غیر از آن که سجده کنم</p></div>
<div class="m2"><p>بر آستان تو هیچم نماز دیگر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر تو روی بپیچی و گر ببندی در</p></div>
<div class="m2"><p>به هیچ روی مرا بازگشت ازین در نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز چهره پرده برافکن، که با رخ تو مرا</p></div>
<div class="m2"><p>به شب چراغ و به روز آفتاب در خور نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر که بود بگفتم حدیث خویش تمام</p></div>
<div class="m2"><p>هنوز هیچ کسی را تمام باور نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز دست زلف تو دل باز می‌توان آورد</p></div>
<div class="m2"><p>ولی چه فایده؟ چون اوحدی دلاور نیست</p></div></div>