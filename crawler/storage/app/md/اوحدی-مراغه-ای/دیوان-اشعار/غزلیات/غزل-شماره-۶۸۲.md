---
title: >-
    غزل شمارهٔ ۶۸۲
---
# غزل شمارهٔ ۶۸۲

<div class="b" id="bn1"><div class="m1"><p>حسن مصرست و رخ چون قمرت میر درو</p></div>
<div class="m2"><p>عشق زندان و حصارش که شدم پیر درو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خم ابروت کمانیست، که دایم باشد</p></div>
<div class="m2"><p>هم کمان مهره و هم ناوک و هم تیر درو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقهٔ زلف تو دامیست گره گیر، که هست</p></div>
<div class="m2"><p>حلق و پای دل من بسته به زنجیر درو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنتست آن رخ خوب و ز دهان و لب تو</p></div>
<div class="m2"><p>می‌رود جوی شراب و عسل و شیر درو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود که جوید ز کمند سر زلف تو خلاص؟</p></div>
<div class="m2"><p>که به اخلاص رود گردن نحجیر درو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسم این کار پریشان، که نمی‌بینم جز</p></div>
<div class="m2"><p>جگر ریش و دل سوخته توفیر درو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر من از عشق تو آشفته شوم نیست عجب</p></div>
<div class="m2"><p>کاوحدی شیفته شد با همه تدبیر درو</p></div></div>