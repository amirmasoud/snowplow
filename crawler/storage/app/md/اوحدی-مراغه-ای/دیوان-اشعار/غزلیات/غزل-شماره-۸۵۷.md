---
title: >-
    غزل شمارهٔ ۸۵۷
---
# غزل شمارهٔ ۸۵۷

<div class="b" id="bn1"><div class="m1"><p>از مردم این مرحله دلساز نبینی</p></div>
<div class="m2"><p>در طارم این قبه هم آواز نبینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی زن و فرزند و برادر؟ که ازین قوم</p></div>
<div class="m2"><p>جز خانه برو خانه برانداز نبینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان عالم و از لذت آن چاشنیی جوی</p></div>
<div class="m2"><p>سهلست گر آن نعمت و آن ناز نبینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فردا اگر از کلی احوال بپرسند</p></div>
<div class="m2"><p>آن روز کسی را تو سرافراز نبینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رازیست درین جنبش و آرام،ولیکن</p></div>
<div class="m2"><p>ترسم که تو خود نیک درین راز نبینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاری بکن، ای خواجه، که این صورت زیبا</p></div>
<div class="m2"><p>پیوسته برین صورت و این ساز نبینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای اوحدی ، این عمر به افسوس مکن خرج</p></div>
<div class="m2"><p>کین عمر چو بگذشت دگر باز نبینی</p></div></div>