---
title: >-
    غزل شمارهٔ ۵۴۷
---
# غزل شمارهٔ ۵۴۷

<div class="b" id="bn1"><div class="m1"><p>درهجر تو درمان دل خسته ندانم</p></div>
<div class="m2"><p>زان پیش که روزی به غمت می‌گذرانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که: به وصلم برسی زود، مخور غم</p></div>
<div class="m2"><p>آری، برسم، گر ز غمت زنده بمانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر من ز دلست این همه، کو قوت پایی؟</p></div>
<div class="m2"><p>تا دل بتو بگذارم و خود را برهانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانا، چو به نقد از بر من دل بربودی</p></div>
<div class="m2"><p>همنقد بده بوسه، که من وعده ندانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدی که: چو دادم دل خود را بتو آسان</p></div>
<div class="m2"><p>بگذشتی و بگذاشتی از پی نگرانم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان از کف اندوه تو آسان نتوان برد</p></div>
<div class="m2"><p>اینست که از روی تو دوری نتوانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دی با من آسوده دلی دیدی و دینی</p></div>
<div class="m2"><p>امروز نگه کن که: نه اینست و نه آنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای مسکن من خاک درت، بر من مسکین</p></div>
<div class="m2"><p>بیداد مکن پر، که جوانی و جوانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پای دلم اوحدی ار دست بدارد</p></div>
<div class="m2"><p>خود را به سر کوی تو روزی برسانم</p></div></div>