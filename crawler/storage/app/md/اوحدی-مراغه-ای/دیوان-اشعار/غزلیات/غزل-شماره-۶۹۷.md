---
title: >-
    غزل شمارهٔ ۶۹۷
---
# غزل شمارهٔ ۶۹۷

<div class="b" id="bn1"><div class="m1"><p>کجایی؟ ای ز رخت آب ارغوان رفته</p></div>
<div class="m2"><p>مرا به عشق تو آوازه در جهان رفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خون دیده ترا کرده‌ام به دست، ولی</p></div>
<div class="m2"><p>ز دست من سر زلف تو رایگان رفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همیشه قد تو با سرکشی قرین بوده</p></div>
<div class="m2"><p>مدام زلف تو با فتنه هم عنان رفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل از شکایت آن جورها که روی تو کرد</p></div>
<div class="m2"><p>هزار بار بنزدیکت باغبان رفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دست زلف سیاه تو تا توان خواری</p></div>
<div class="m2"><p>بدین شکستهٔ مسکین ناتوان رفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آب دیده بگریم ز هجرت آن روزی</p></div>
<div class="m2"><p>که مرده باشم و خاک در استخوان رفته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چگونه راز دل اوحدی توان پوشید؟</p></div>
<div class="m2"><p>حدیثش از دهن و تیرش از کمان رفته</p></div></div>