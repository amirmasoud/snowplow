---
title: >-
    غزل شمارهٔ ۷۶
---
# غزل شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>بی تو نکردیم به جایی نشست</p></div>
<div class="m2"><p>با تو نشستیم به هر جا که هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت خوب از چه به گیتی بسیست</p></div>
<div class="m2"><p>چشم مرا مثل تو صورت نبست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاف نخستین «بلی» می‌زنم</p></div>
<div class="m2"><p>روز نخستین که تو گویی:«الست»</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف سیه را به ازان می‌شکن</p></div>
<div class="m2"><p>ورنه بسی دل که بخواهد شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موی برست از کف امید ما</p></div>
<div class="m2"><p>وز کف موی تو نخواهیم رست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که کند گوش به گفتار تو</p></div>
<div class="m2"><p>بس که به گفتار بخواهد نشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که ز من صبر طلب میکنی</p></div>
<div class="m2"><p>خود چو منی را چه بر آید ز دست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پند، که بی‌بادهٔ صافی دهی</p></div>
<div class="m2"><p>کی شنود عاشق دردی پرست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوحدی از عشق تو دیوانه شد</p></div>
<div class="m2"><p>گر دگری می‌شود از عشق مست</p></div></div>