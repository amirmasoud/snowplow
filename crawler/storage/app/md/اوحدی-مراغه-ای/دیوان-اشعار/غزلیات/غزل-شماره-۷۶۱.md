---
title: >-
    غزل شمارهٔ ۷۶۱
---
# غزل شمارهٔ ۷۶۱

<div class="b" id="bn1"><div class="m1"><p>اگر چه از بر من بارها چو تیر بجستی</p></div>
<div class="m2"><p>هم آخرم بکشیدیّ و چون کمان بشکستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درآمدم که نشینم، برون شدی به شکایت</p></div>
<div class="m2"><p>برون شدم که بیایم، درم به روی ببستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا به داغ بکشتی، ولی ز باغ رخ خود</p></div>
<div class="m2"><p>گلم به دست ندادی، دلم به خار بخستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هلاک همچو منی در غم تو حیف نباشد؟</p></div>
<div class="m2"><p>من ار ز پای درآیم چه باک؟ چون تو به دستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مبین در آینه آن زلف و چهره را، که اگر تو</p></div>
<div class="m2"><p>چنان جمال ببینی کسی دگر نپرستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو با کمال بزرگیّ و احتشام ندانم</p></div>
<div class="m2"><p>که در درون دل تنگ من چگونه نشستی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا ز مستی و عشقست نام زلف تو بردن</p></div>
<div class="m2"><p>که قصه‌های پریشان ز عشق خیزد و مستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نماز شام ندیدی؟ که پیش روی چو ماهت</p></div>
<div class="m2"><p>چگونه مهر عدم شد ز شرم با همه هستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مبر ستیزه، چو من کام دل ز لعل تو جویم</p></div>
<div class="m2"><p>چه حاجتست خصومت؟ بیار بوسه و رستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو خود نیایی و من پیشت آمدن نتوانم</p></div>
<div class="m2"><p>مگر به دست رسولم حکایتی بفرستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر هزار دلست از غمت یکی نرهانم</p></div>
<div class="m2"><p>که باد و غمزهٔ چون تیر و باد و زلف چو شستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مترس در غمش ای اوحدی، ز خواری و محنت</p></div>
<div class="m2"><p>که اوفتاده نترسد ز خاکساری و پستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر آن دو نرگس جادو به جان خلاص دهندت</p></div>
<div class="m2"><p>زهی عنایت و دولت! برو! که نیک برستی</p></div></div>