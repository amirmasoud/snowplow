---
title: >-
    غزل شمارهٔ ۵۸۳
---
# غزل شمارهٔ ۵۸۳

<div class="b" id="bn1"><div class="m1"><p>بندهٔ عشقیم و سالهاست که هستیم</p></div>
<div class="m2"><p>ورزش عشق تو کار ماست، که مستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس بدویدیم در به در ز پی تو</p></div>
<div class="m2"><p>چون که نشان تو یافتیم نشستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز دل ما بزیر پای غم تو</p></div>
<div class="m2"><p>بام لگدکوب شد که خانهٔ پستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار نداریم جز خیال تو، گر چه</p></div>
<div class="m2"><p>مدعیان را خیال بود که: جستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دل ما هر کس آمدی و نشستی</p></div>
<div class="m2"><p>دل به تو پرداختیم وز همه رستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طوق تو بر گردنیم و داغ تو بر دل</p></div>
<div class="m2"><p>بند تو بر پای و باد توبه به دستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهر، که در کام عشق بود، چشیدیم</p></div>
<div class="m2"><p>شیشه، که در بار عقل بود، شکستیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه به دست تو همچو مرغ گرفتار</p></div>
<div class="m2"><p>گاه به دام تو همچو ماهی شستیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر «نعم» در دهان ز روز نخستین</p></div>
<div class="m2"><p>راز «بلی» در زبان ز روز الستیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ز کمرمان بیفگنند چو فرهاد</p></div>
<div class="m2"><p>باز نخواهد شد آن کمر که ببستیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اوحدی، اینجا بتان پرند ولیکن</p></div>
<div class="m2"><p>کفر بود، گر به جز یکی بپرستیم</p></div></div>