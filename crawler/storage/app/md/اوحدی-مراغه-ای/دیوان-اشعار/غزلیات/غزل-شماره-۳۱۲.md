---
title: >-
    غزل شمارهٔ ۳۱۲
---
# غزل شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>گر کسی در عشق آهی می‌کند</p></div>
<div class="m2"><p>تا نپنداری گناهی می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیدلی گر می‌کند جایی نظر</p></div>
<div class="m2"><p>صنع یزدان را نگاهی می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با دم صاحبدلان خواری مکن</p></div>
<div class="m2"><p>کان نفس کار سپاهی می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه سنگی می‌نهد در راه ما</p></div>
<div class="m2"><p>از برای خویش چاهی می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بنالد خسته‌ای معذور دار</p></div>
<div class="m2"><p>زحمتی دارد، که آهی می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق را آن کو سپه سازد به عقل</p></div>
<div class="m2"><p>دفع کوهی را به کاهی می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر کند رندی نظر بازی، رواست</p></div>
<div class="m2"><p>محتسب هم گاه‌گاهی می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک دم از خاطر فراموشم نشد</p></div>
<div class="m2"><p>آنکه یادم هر به ماهی می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند نالیدم و آن بت خود نگفت:</p></div>
<div class="m2"><p>کین تضرع دادخواهی می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اوحدی را گر چه از غم بیمهاست</p></div>
<div class="m2"><p>هم به امیدش پناهی می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اشتر حاجی نمی‌داند که چیست؟</p></div>
<div class="m2"><p>بار بر پشتست و راهی می‌کند</p></div></div>