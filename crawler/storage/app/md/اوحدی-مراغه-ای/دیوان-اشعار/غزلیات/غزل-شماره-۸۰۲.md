---
title: >-
    غزل شمارهٔ ۸۰۲
---
# غزل شمارهٔ ۸۰۲

<div class="b" id="bn1"><div class="m1"><p>جهد بکن تا که به جایی رسی</p></div>
<div class="m2"><p>درد بکش، تا به دوایی رسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر آن کوچه بسی برگهاست</p></div>
<div class="m2"><p>خیز و برو، تا به نوایی رسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیرهنی چاک نکردی به عشق</p></div>
<div class="m2"><p>کی ز بر او به قبایی رسی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نشوی فارغ و یکتا، کجا</p></div>
<div class="m2"><p>از سر آن زلف بتایی رسی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که به بوسی تو زمینش ز دور</p></div>
<div class="m2"><p>تا که به بوسیدن پایی رسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو درآیی ز پی کاروان</p></div>
<div class="m2"><p>زود به آواز درایی رسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از صف دل دور مشو، زانکه تو</p></div>
<div class="m2"><p>هم ز دل خود به صفایی رسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که به مخلوق چنین غره‌ای</p></div>
<div class="m2"><p>خود چه کنی؟ گربه خدایی رسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواجه ترا چون ز غلامان شمرد</p></div>
<div class="m2"><p>گر نگریزی به بهایی رسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یوسف خود را بتوانی ربود</p></div>
<div class="m2"><p>گر به چنین گرگ‌ربایی رسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اوحدیا، سایه ز ما برمگیر</p></div>
<div class="m2"><p>گر به چنین ظل همایی رسی</p></div></div>