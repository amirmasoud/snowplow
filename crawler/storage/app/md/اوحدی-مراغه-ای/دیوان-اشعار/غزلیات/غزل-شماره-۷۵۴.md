---
title: >-
    غزل شمارهٔ ۷۵۴
---
# غزل شمارهٔ ۷۵۴

<div class="b" id="bn1"><div class="m1"><p>ای ماه و مشتری ز جمالت قرینه‌ای</p></div>
<div class="m2"><p>وز گیسوی تو هر شکنی عنبرینه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر می‌زنی به تیغ، نداریم سر دریغ</p></div>
<div class="m2"><p>سر چون توان کشید ز مهری به کینه‌ای؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ دلم به داغ غمت تن فرو دهد</p></div>
<div class="m2"><p>گر باشدش ز دانهٔ خال تو چینه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر لحظه آن دو ساعد سیمین نهان کنند</p></div>
<div class="m2"><p>در جان من به دست محبت دفینه‌ای؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل در خمار هجر تو می‌میرد، ای نگار</p></div>
<div class="m2"><p>بفرست ازان شراب تعطف قنینه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساکن نمی‌شود غم عشقت ز جان ما</p></div>
<div class="m2"><p>یارب، فرو فرست به دلها سکینه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاصد نبرد نامه، که از آب چشم خلق</p></div>
<div class="m2"><p>پیش تو آمدن نتوان بی‌سفینه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیغام ما چگونه رسد نزد آن حرم؟</p></div>
<div class="m2"><p>چندان رسولش آمده از هر مدینه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشمت ز فتنه بین که: به پیش من آورد</p></div>
<div class="m2"><p>در معرضی که زلف تو باشد پسینه‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اشکم چو سیم دیدی و زر خواستی ز من</p></div>
<div class="m2"><p>پنداشتی که باشد از آنم خزینه‌ای؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر در بهای بوسه لبت زر طلب کند</p></div>
<div class="m2"><p>مشکل کشد کمان تو چون من کمینه‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روزی نشد که غمزهٔ مست تو سنگدل</p></div>
<div class="m2"><p>بر راه اوحدی نشکست آبگینه‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صافی کجا شود دل او زین عتابها؟</p></div>
<div class="m2"><p>تا با تو سینه‌ای نرساند به سینه‌ای</p></div></div>