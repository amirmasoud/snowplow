---
title: >-
    غزل شمارهٔ ۳۶
---
# غزل شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>ای چراغ چشم توفان بار ما</p></div>
<div class="m2"><p>بیش ازین غافل مباش از کار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر زمانی در به روی ما مبند</p></div>
<div class="m2"><p>گر چه کوته دیده‌ای دیوار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکر آن که خواب می‌گیرد به شب</p></div>
<div class="m2"><p>رحمتی بر دیدهٔ بیدار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که با هر کس چو گل بشکفته‌ای</p></div>
<div class="m2"><p>بیش ازین نتوان نهادن خار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاشکی آن رخ نبودی در نقاب</p></div>
<div class="m2"><p>تا نکردی مدعی انکار ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با چنان ساعد که بر بازوی اوست</p></div>
<div class="m2"><p>کس نپیچد پنجهٔ عیار ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلق عالم گر شوند اغیار و خصم</p></div>
<div class="m2"><p>نیست غم، گر یار باشد یار ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اوحدی، می‌بوس خاک آستان</p></div>
<div class="m2"><p>کندر آن حضرت نباشد بار ما</p></div></div>