---
title: >-
    غزل شمارهٔ ۵۸۸
---
# غزل شمارهٔ ۵۸۸

<div class="b" id="bn1"><div class="m1"><p>مادر غم هجران تو، گر زانکه بمیریم</p></div>
<div class="m2"><p>بر وصل تو یکروز ببینی که: امیریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سرو، که اسباب جوانی همه داری</p></div>
<div class="m2"><p>با ما به جفا پنجه مینداز، که پیریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تلخ شود کام دل ما چه تفاوت؟</p></div>
<div class="m2"><p>کز یاد لب لعل تو در شکر و شیریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بهر فرستان این قصه بر تو</p></div>
<div class="m2"><p>پیوسته دوان در طلب پیک و دبیریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شهر طبیبیست که داند همه رنجی</p></div>
<div class="m2"><p>او نیز ندانست که: مجروح چه تیریم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با روی تو این سختی پیوند که ما راست</p></div>
<div class="m2"><p>بعد از تو روا باشد، اگر دوست نگیریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گو: قافله بیرون رو و همراه سفر کن</p></div>
<div class="m2"><p>ما را سفر و عزم نباشد، که اسیریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر تلخ، که خواهی تو، بگو، تا بنیوشیم</p></div>
<div class="m2"><p>هر زهر، که داری تو، بده، تا بپذیریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این نامه پراگنده از آنست که بی‌تو</p></div>
<div class="m2"><p>چون اوحدی امروز پراگنده ضمیریم</p></div></div>