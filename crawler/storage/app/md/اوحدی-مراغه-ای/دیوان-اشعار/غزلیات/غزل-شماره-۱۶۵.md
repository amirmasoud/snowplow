---
title: >-
    غزل شمارهٔ ۱۶۵
---
# غزل شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>ای طیرهٔ شب طرهٔ خورشید پناهت</p></div>
<div class="m2"><p>آرایش عالم رخ رنگین چو ماهت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاب دل ناهید ز باد خم زلفت</p></div>
<div class="m2"><p>آ ب رخ خورشید ز خاک سر راهت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیباچهٔ خوبی ورق روی منیرت</p></div>
<div class="m2"><p>عنوان شگرفی رقم خط سیاهت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر رشتهٔ پروین زده صد سوزن طعنه</p></div>
<div class="m2"><p>تابیدن عکس گهر از بند کلاهت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خاک فزون گشته سیاه تو، ولیکن</p></div>
<div class="m2"><p>آتش زده سودای تو بر قلب سپاهت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فردا به قیامت گر ازین گونه برآیی</p></div>
<div class="m2"><p>ایزد، نه همانا، که بپرسد ز گناهت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزدیک شود با فلک از روی بلندی</p></div>
<div class="m2"><p>روزی که کند اوحدی از دور نگاهت</p></div></div>