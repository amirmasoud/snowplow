---
title: >-
    غزل شمارهٔ ۱۹۲
---
# غزل شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>باد بویی از دو زلفت وام کرد</p></div>
<div class="m2"><p>سوی چین آورد و مشکش نام کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزهٔ آهووش گو افگنت</p></div>
<div class="m2"><p>تیر غم در دیدهٔ بهرام کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانهٔ خالی، که بر رخسار تست</p></div>
<div class="m2"><p>پای ما را بستهٔ این دام کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قامت من چون الف بود از نشاط</p></div>
<div class="m2"><p>آن الف را دام زلفت لام کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نازنینا، صبح ما را همچو شام</p></div>
<div class="m2"><p>فتنهٔ‌آن لعل خون آشام کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توسن دل، گرچه تندی می‌نمود</p></div>
<div class="m2"><p>عاقبت چشم تو او را رام کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش روی تو ما را سخت سوخت</p></div>
<div class="m2"><p>گر چه کار اوحدی را خام کرد</p></div></div>