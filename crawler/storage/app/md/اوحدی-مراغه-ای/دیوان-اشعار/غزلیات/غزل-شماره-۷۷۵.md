---
title: >-
    غزل شمارهٔ ۷۷۵
---
# غزل شمارهٔ ۷۷۵

<div class="b" id="bn1"><div class="m1"><p>نقشی ز صورت خود هر جا پدید کردی</p></div>
<div class="m2"><p>پس عشق دیدن آن در ما پدید کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هر کسی نداند سر پرستش تو</p></div>
<div class="m2"><p>وامق بیافریدی، عذرا پدید کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید را بدادی نوری ز طلعت خود</p></div>
<div class="m2"><p>وز بهر خدمت او جوزا پدید کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا قطره را نباشد از گم شدن هراسی</p></div>
<div class="m2"><p>بر راه باز گشتن دریا پدید کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌خواستی که از ما بر ما بهانه گیری</p></div>
<div class="m2"><p>ورنه چرا ز آدم حوا پدید کردی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوری که شمع گردون از عکس اوست روشن</p></div>
<div class="m2"><p>در نقطهٔ دل ما چون ناپدید کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا دولت وصالت بی‌وعده‌ای نباشد</p></div>
<div class="m2"><p>امروز عاشقان را فردا پدید کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان ساغر نهانی بر باده‌ای که دانی</p></div>
<div class="m2"><p>چون گرم گشت منزل غوغا پدید کردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از جستن نهانت چون اوحدی زبون شد</p></div>
<div class="m2"><p>در عین بی‌نشانی خود را پدید کردی</p></div></div>