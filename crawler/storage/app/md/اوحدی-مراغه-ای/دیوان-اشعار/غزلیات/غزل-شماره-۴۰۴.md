---
title: >-
    غزل شمارهٔ ۴۰۴
---
# غزل شمارهٔ ۴۰۴

<div class="b" id="bn1"><div class="m1"><p>پاکبازان را چه خارا و چه خز؟</p></div>
<div class="m2"><p>گر به رنگی قانعی در خرقه خز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامه گه ازرق کنی، گاهی سیاه</p></div>
<div class="m2"><p>جامه خود دانی، تو مردم را مرز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخرت زندان تن خواهد شدن</p></div>
<div class="m2"><p>این که بر خود می‌تنی چون کرم قز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو ایزد را بدین خواهی شناخت</p></div>
<div class="m2"><p>نیک دور افتاده‌ای، سودا مپز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نخواهی فهم کردن، زان چه سود؟</p></div>
<div class="m2"><p>گر منت مشروح گویم، یا لغز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محتسب گو: در پی رندان مرو</p></div>
<div class="m2"><p>کین جماعت را نباشد سنگ و گز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیب مستان کم کن و در مجلس آی</p></div>
<div class="m2"><p>گر ننوشی باده‌ای، سیبی بگز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باده خوردن در بهار ار ظلم بود</p></div>
<div class="m2"><p>در زمستان خود نمی‌جوشید رز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوش داری گفتهای اوحدی</p></div>
<div class="m2"><p>تا که لؤلؤ را بدانی از خرز</p></div></div>