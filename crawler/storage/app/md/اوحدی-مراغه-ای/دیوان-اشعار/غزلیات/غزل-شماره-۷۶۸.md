---
title: >-
    غزل شمارهٔ ۷۶۸
---
# غزل شمارهٔ ۷۶۸

<div class="b" id="bn1"><div class="m1"><p>چون فتنه شدم بر رخت، ای حور بهشتی</p></div>
<div class="m2"><p>رفتی و مرا در غم خود زار بهشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دست تو من پای فشارم به چه قوت؟</p></div>
<div class="m2"><p>با روی تو من صبر نمایم به چه پشتی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خاک سر کوی تو یک روز بگریم</p></div>
<div class="m2"><p>زان گونه که بیرون نتوان رفت به کشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانم که: حسابی نبود روز قیامت</p></div>
<div class="m2"><p>او را که بدین حال تو امروز بکشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش که توان برد خود این غصه؟ که پیشت</p></div>
<div class="m2"><p>صد قصه نبشتم که جوابی ننبشتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خوی تو بس گل که به خونابه سرشتم</p></div>
<div class="m2"><p>تا خود تو بدین خوی و نهاد از چه سرشتی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خاطر خود جز تو خیالی نگذارد</p></div>
<div class="m2"><p>آنرا که تو یکروز به خاطر بگذشتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دل، که همی جویی ازین دام رهایی</p></div>
<div class="m2"><p>آن روز که گفتیم چرا باز نگشتی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون اوحدی از قامت او درد همی چین</p></div>
<div class="m2"><p>کین میوهٔ آن شاخ بلندست که کشتی</p></div></div>