---
title: >-
    غزل شمارهٔ ۳۴۸
---
# غزل شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>گفتم: که: بی‌وصال تو ما را به سر شود</p></div>
<div class="m2"><p>گر صبر صبر ماست عجب دارم ار شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر تو بر صحیفهٔ جان نقش کرده‌ایم</p></div>
<div class="m2"><p>مشکل خیال روی تو از دل بدر شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی که: مختصر بکنیم این سخن، ولی</p></div>
<div class="m2"><p>گر بر لبم نهی لب خود، مختصر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیر از دو بوسه هر چه به بیمار خود دهی</p></div>
<div class="m2"><p>گر آب زندگیست، که بیمارتر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ما بلا کشیم ز بالات، عیب نیست</p></div>
<div class="m2"><p>کار دلست و راست به خون جگر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از فرق آسمان برباید کلاه مهر</p></div>
<div class="m2"><p>دستی که در میان تو روزی کمر شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزی به آستانهٔ وصلی برون خرام</p></div>
<div class="m2"><p>تا اوحدی به جان و دلت خاک در شود</p></div></div>