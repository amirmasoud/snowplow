---
title: >-
    غزل شمارهٔ ۴۹۸
---
# غزل شمارهٔ ۴۹۸

<div class="b" id="bn1"><div class="m1"><p>بیا، بیا که ز مهرت به جان همی گردم</p></div>
<div class="m2"><p>به بوی وصل تو گرد جهان همی گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خفته‌ای، خبرت کی بود؟ که من هر شب</p></div>
<div class="m2"><p>به گرد کوی تو چون پاسبان همی گردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملامت من بیدل مکن درین غرقاب</p></div>
<div class="m2"><p>تو بر کناری و من و در میان همی گردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیشگاه قبول تو راه نیست، مگر</p></div>
<div class="m2"><p>رها کنی، که برین آستان همی گردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار بار شدم در غم تو پیر، ولی</p></div>
<div class="m2"><p>دگر به بوی وصالت جوان همی گردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدم به پرسش من رنجه کن، که هر ساعت</p></div>
<div class="m2"><p>بسان چشم خوشت ناتوان همی گردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لبت بشارت کامی به اوحدی دادست</p></div>
<div class="m2"><p>درین دیار به امید آن همی گردم</p></div></div>