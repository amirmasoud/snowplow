---
title: >-
    غزل شمارهٔ ۷۲۲
---
# غزل شمارهٔ ۷۲۲

<div class="b" id="bn1"><div class="m1"><p>ماییم و خراباتی پر بادهٔ جوشیده</p></div>
<div class="m2"><p>جز رند خراباتی آن باده ننوشیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رندان سر افرازش دستار گرو کرده</p></div>
<div class="m2"><p>خوبان طرب سازش رخسار نپوشیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رندان وی از سستی بر چرخ سبق برده</p></div>
<div class="m2"><p>خوبان وی از مستی در عربده کوشیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌فتنه مقیمانش فعلی نپسندیده</p></div>
<div class="m2"><p>بی‌باده حریفانش قولی ننوشیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان باده چو تر گردی، از صومعه برگردی</p></div>
<div class="m2"><p>وانگاه به سر گردی، ای زاهد خوشیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دل که توانسته این حال طلب کرده</p></div>
<div class="m2"><p>چون حال بدانسته دیگر نخروشیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا اوحدی افتاده اندر پی این باده</p></div>
<div class="m2"><p>پستان سعادت را بگرفته و دوشیده</p></div></div>