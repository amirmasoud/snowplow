---
title: >-
    غزل شمارهٔ ۸۵۵
---
# غزل شمارهٔ ۸۵۵

<div class="b" id="bn1"><div class="m1"><p>رخ و زلفت، ای پریرخ، سمنست و مشک چینی</p></div>
<div class="m2"><p>به دهان و لب بگویم که : نبات و انگبینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو اگر در آب روزی نظری کنی بر آن رخ</p></div>
<div class="m2"><p>هوست کجا گذارد که : کسی دگر ببینی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به زبان خود نگارا، خبرم بپرس روزی</p></div>
<div class="m2"><p>که دلت زبون مبادا! ز رقیب چون ز بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو ز چهره بر گشایی تو نقاب، عقل گوید:</p></div>
<div class="m2"><p>قلمست و نرگس و گل نه دهان و چشم و بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دلم خیال رویت نرود به هیچ وجهی</p></div>
<div class="m2"><p>که دلم نگین مهرست و تو مهر آن نگینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شد، اوحدی، دل تو به خیال او پریشان</p></div>
<div class="m2"><p>متحیرم که بی او بچه عذر می‌نشینی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برو و ز باغ رویش دو سه گل به چین نهفته</p></div>
<div class="m2"><p>که چو باغبان ببیند نهلد که گل بچینی</p></div></div>