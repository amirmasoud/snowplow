---
title: >-
    غزل شمارهٔ ۷۷۳
---
# غزل شمارهٔ ۷۷۳

<div class="b" id="bn1"><div class="m1"><p>سوگند من شکستی، عهدم به باد دادی</p></div>
<div class="m2"><p>با این ستیزه رویی روز و شبم به یادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی: چو کارت افتد من دستگیر باشم</p></div>
<div class="m2"><p>خود با حکایت من دیگر نیوفتادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چستی نمودم، ای جان، در کار عشق اول</p></div>
<div class="m2"><p>سودی نداشت با تو چیستی و اوستادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دیده و دل من گشتند فتنهٔ تو</p></div>
<div class="m2"><p>آب اندران فگندی آتش از آن نهادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم سرو لاله‌رویی، هم ماه مشک مویی</p></div>
<div class="m2"><p>هم ترک تند خویی، هم شاه حورزادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی تو شمع گیتی چون مهر نیم روزان</p></div>
<div class="m2"><p>بوی تو راحت جان چون باد بامدادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شادی کنی چو بینی ما را بغم نشسته</p></div>
<div class="m2"><p>ای اوحدی غلامت،خوش میروی بشادی!</p></div></div>