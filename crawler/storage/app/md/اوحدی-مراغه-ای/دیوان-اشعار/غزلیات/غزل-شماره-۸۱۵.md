---
title: >-
    غزل شمارهٔ ۸۱۵
---
# غزل شمارهٔ ۸۱۵

<div class="b" id="bn1"><div class="m1"><p>گفتم که: بگذرانم روزی به نام و ننگی؟</p></div>
<div class="m2"><p>خود با کمند عشقم وزنی نبود و سنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت از دهان تنگش بار و خرم به غارت</p></div>
<div class="m2"><p>دردا! که بر نیامد خروار من به تنگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ می‌نمود از اول و اکنون همی نماید</p></div>
<div class="m2"><p>از بهر کشتن ما هر ساعتی بینگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>احوال خود بگویم با زلفش آشکارا</p></div>
<div class="m2"><p>اکنون که جز سیاهی ما را نماند رنگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی نهان بماند در زیر پنبه آتش؟</p></div>
<div class="m2"><p>هم بر زنیم ناگه این شیشه را به سنگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا دامن قیامت بیرون نرفتی از کف</p></div>
<div class="m2"><p>ما را به دامن او گر می‌رسید چنگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبرو قرار ازان دل، زنهار! تا نجویی</p></div>
<div class="m2"><p>کش در برابر آید زین گونه شوخ شنگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رویی بدان لطافت، چون پرده باز گیرد</p></div>
<div class="m2"><p>بیننده را نماند سامان هوش و هنگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس تیرغم که در دل ما را رسید، لیکن</p></div>
<div class="m2"><p>در سالها نیامد بر سینه زین خدنگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گردن به غم نهادم کز درد دوری او</p></div>
<div class="m2"><p>شادی نمی‌نماید نزدیک من درنگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از بهر اوست با من یک شهر دشمن، ار نه</p></div>
<div class="m2"><p>با اوحدی کسی را خشمی نبود و جنگی</p></div></div>