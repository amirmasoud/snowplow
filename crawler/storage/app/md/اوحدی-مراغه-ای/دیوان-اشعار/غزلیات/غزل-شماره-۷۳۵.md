---
title: >-
    غزل شمارهٔ ۷۳۵
---
# غزل شمارهٔ ۷۳۵

<div class="b" id="bn1"><div class="m1"><p>ببر، ای باد صبح‌دم، بده ای پیک نیک‌پی</p></div>
<div class="m2"><p>سخن عاشقان بدو، خبر بیدلان بوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز من آن شوخ دیده را چو ببینی بگوی تو:</p></div>
<div class="m2"><p>عجب از حال بیدلان، که چنین غافلی تو، هی!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دف آن خسته را مزن، که دمی بی‌حضور تو</p></div>
<div class="m2"><p>نتواند ز چنگ غم، که ننالد بسان نی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنمودم هزار پی بتو احوال خویشتن</p></div>
<div class="m2"><p>ننوشتی جواب آن که نمودم هزار پی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز برم تا برفته‌ای تو، زمانی نمی‌شود</p></div>
<div class="m2"><p>نه گشاینده بند غم، نه گوارنده جام می</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن بوسه گفته‌ای، بنگویی که: چند و چون؟</p></div>
<div class="m2"><p>خبر وصل داده‌ای، ننموده‌ای که: کو و کی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن، ای مدعی، مرا ز درش دور بعد ازین</p></div>
<div class="m2"><p>که من آن خاک کوچه را نفروشم به تاج کی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز پی آنکه بنگرم رخ لیلی ز گوشه‌ای</p></div>
<div class="m2"><p>من مجنون خسته را، که برد به کنار حی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر، ای اوحدی، تو هم دل خود را تو دوستی؟</p></div>
<div class="m2"><p>به رخ و زلف او دهی، برهی زین بهار و دی</p></div></div>