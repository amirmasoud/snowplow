---
title: >-
    غزل شمارهٔ ۸۸۲
---
# غزل شمارهٔ ۸۸۲

<div class="b" id="bn1"><div class="m1"><p>زهی! حسن ترا گل خاک کویی</p></div>
<div class="m2"><p>نسیم عنبر از زلف تو بویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخت بر سوسن و گل طعنه‌ها زد</p></div>
<div class="m2"><p>که بود این ده زبانی، آن دو رویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیامد در خم چوگان خوبی</p></div>
<div class="m2"><p>به از سیب زنخدان تو گویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر زلفت ز بهر غارت دل</p></div>
<div class="m2"><p>پریشانست هر تاری به سویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدی جویای بالای تو گر سرو</p></div>
<div class="m2"><p>توانستی که بگذشتی ز جویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز زلفت حلقه‌ای جستم، ندادی</p></div>
<div class="m2"><p>چه سختی می‌کنی با من به مویی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل سخت تو چون دید اوحدی گفت:</p></div>
<div class="m2"><p>بدین سنگم بباید زد سبویی</p></div></div>