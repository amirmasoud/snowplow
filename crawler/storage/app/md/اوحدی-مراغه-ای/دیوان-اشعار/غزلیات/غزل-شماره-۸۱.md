---
title: >-
    غزل شمارهٔ ۸۱
---
# غزل شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>پیراهن ار ز یاسمن و گل کند رواست</p></div>
<div class="m2"><p>آن سرو لاله چهره، که در غنچهٔ قباست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلقی، چو طرف، بر کمرش بسته‌اند دل</p></div>
<div class="m2"><p>وین دولت از میانه ببینیم تا کراست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرد از هوای خویش دلم گرم ذره‌وار</p></div>
<div class="m2"><p>آن آفتاب روی، که بر بام این سراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر خاک پای او چه غم؟ ار صد هزار پی</p></div>
<div class="m2"><p>آب رخم بریخت، که خون منش بهاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمش چه ساحریست؟ که شرطی ز دشمنی</p></div>
<div class="m2"><p>با من رها نکرد و همان دوستی بجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با من، دلا، گر سخن آن دهان مگوی</p></div>
<div class="m2"><p>من بر شنیده‌ام سخن او، دهان کجاست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جان اوحدی اگر او ناوکی نخست</p></div>
<div class="m2"><p>چندین فغان و ناله و فریادش از چه خاست؟</p></div></div>