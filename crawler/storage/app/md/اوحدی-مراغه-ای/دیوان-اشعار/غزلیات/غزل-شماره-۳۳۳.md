---
title: >-
    غزل شمارهٔ ۳۳۳
---
# غزل شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>این چنین نقشی اگر در چین بود</p></div>
<div class="m2"><p>قبلهٔ خوبان آن ملک این بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چنین رخسار و دندان و جبین</p></div>
<div class="m2"><p>مشتری، یا زهره، یا پروین بود؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دهی دشنام ازان لبها دعاست</p></div>
<div class="m2"><p>هر چه حلوایی دهد شیرین بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دلت سیر آید از من طرفه نیست</p></div>
<div class="m2"><p>عهد خوبان را بقا چندین بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوش بر گفتار ما کمتر کنی</p></div>
<div class="m2"><p>فی‌المثل گر سورهٔ یاسین بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آشنایان همچو فرزین بگذری</p></div>
<div class="m2"><p>با غریبان اسب لطفت زین بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون به بخت اوحدی آید سخن</p></div>
<div class="m2"><p>جمله صلحت خشم و مهرت کین بود</p></div></div>