---
title: >-
    غزل شمارهٔ ۶۶۰
---
# غزل شمارهٔ ۶۶۰

<div class="b" id="bn1"><div class="m1"><p>گر سوی من چنین نگرد چشم مست تو</p></div>
<div class="m2"><p>سر در جهان نهم به غریبی ز دست تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد بهار و خاطر هر کس کشد به باغ</p></div>
<div class="m2"><p>میلی کی او کند که بود پای بست تو؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قاضی ترا به دیده ملامت همی کند</p></div>
<div class="m2"><p>بر محتسب، ز دست محبان مست تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر بگذرد به چرخ بلندم به گردنی</p></div>
<div class="m2"><p>گر دست من رسد به سر زلف پست تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد بار پیش دشمن اگر بشکنی مرا</p></div>
<div class="m2"><p>سهلست پیش من، چو نبینم شکست تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردا! که هستیم ز فراق تو نیست شد</p></div>
<div class="m2"><p>کامی ندیده از دهن نیست هست تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک ساعت اوحدی به دو چشمت نگاه کرد</p></div>
<div class="m2"><p>پنجاه تیر بر دلش آمد ز شست تو</p></div></div>