---
title: >-
    غزل شمارهٔ ۶۵۸
---
# غزل شمارهٔ ۶۵۸

<div class="b" id="bn1"><div class="m1"><p>بنگر بدان دو ابروی همچون کمان او</p></div>
<div class="m2"><p>وآن غمزهٔ چو تیر و رخ مهربان او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انگشت می‌گزد به تحیر کمان چرخ</p></div>
<div class="m2"><p>ز انگشت رنگ داده و انگشتوان او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر جان من طلب کند، از وی دریغ نیست</p></div>
<div class="m2"><p>بشنو، که این دروغ نگفتم به جان او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو: بوسه‌ای به جان بفروش، ار زیان کند</p></div>
<div class="m2"><p>دل نیز می‌دهم، که نخواهم زیان او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با دشمنان دوست کنم دوستی مدام</p></div>
<div class="m2"><p>زیرا که غیرت آیدم از دوستان او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از وی بپرس حال من، ای باد صبح دم</p></div>
<div class="m2"><p>باشد که نام من برود بر زبان او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن کو به حسن فتنهٔ آخر زمان بود</p></div>
<div class="m2"><p>ناچار فتنها بود اندر زمان او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن موی او به پای رسد، گر فرو کشی</p></div>
<div class="m2"><p>لیکن به لاغری نرسد در میان او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی طبیب خفتهٔ ما را خبر نبود:</p></div>
<div class="m2"><p>کامشب نخفت تا به سحر ناتوان او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روزی که جان اوحدی از تن جدا شود</p></div>
<div class="m2"><p>از دوستی جدا نشود استخوان او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ذوقهای شعر روانش بسی که خلق</p></div>
<div class="m2"><p>گویند: کافرین خدا بر روان او</p></div></div>