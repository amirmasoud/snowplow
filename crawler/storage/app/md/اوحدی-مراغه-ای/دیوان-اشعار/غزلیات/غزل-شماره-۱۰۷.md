---
title: >-
    غزل شمارهٔ ۱۰۷
---
# غزل شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>درد دلم را طبیب چاره ندانست</p></div>
<div class="m2"><p>مرهم این ریش پاره پاره ندانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راز دلم را به صبر، گفت: بپوشان</p></div>
<div class="m2"><p>حال دل غرقه از کناره ندانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طالع من خود چه شور بود؟ که هرگز</p></div>
<div class="m2"><p>هیچ منجم در آن ستاره ندانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار به یک بار میل سوی جفا کرد</p></div>
<div class="m2"><p>حق وفای هزار باره ندانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برد گمانی که: ما به عشق اسیریم</p></div>
<div class="m2"><p>این که چه نامیم یا چه کاره؟ ندانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خال بنا گوش اوز گوشه نشینان</p></div>
<div class="m2"><p>برد چنان دل، که گوشواره ندانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قافلهٔ عقل را به ساعد سیمین</p></div>
<div class="m2"><p>راه ز جایی بزد که باره ندانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوش به خونی گریستم، که ز موجش</p></div>
<div class="m2"><p>عقل به اندیشها گذاره ندانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سختی ازان دید، اوحدی، که به اول</p></div>
<div class="m2"><p>قاعدهٔ آن دل چو خاره ندانست</p></div></div>