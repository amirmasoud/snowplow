---
title: >-
    غزل شمارهٔ ۲۹۲
---
# غزل شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>دوشم از کوی مغان دست به دست آوردند</p></div>
<div class="m2"><p>از خرابات سوی صومعه مست آوردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ می‌خواره ندارد طمع حور و بهشت</p></div>
<div class="m2"><p>این بشارت به من باده پرست آوردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقیانش، ز می عشق چو گردیدم مست</p></div>
<div class="m2"><p>به می دیگرم از نیست به هست آوردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف و خال و خط خوبان همه رنجست، آنها</p></div>
<div class="m2"><p>از کجا این همه تشویش به دست آوردند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این شگرفان که نگنجند در آفاق از حسن</p></div>
<div class="m2"><p>در چنین سینهٔ تنگ از چه نشست آوردند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قلب سالوس و ریا را نشکستند درست</p></div>
<div class="m2"><p>مگر این قوم که در زلف شکست آوردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی را چو ازین دایره دیدند برون</p></div>
<div class="m2"><p>زود در حلقهٔ آن زلف چو شست آوردند</p></div></div>