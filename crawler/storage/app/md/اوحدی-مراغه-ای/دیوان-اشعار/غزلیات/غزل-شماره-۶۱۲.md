---
title: >-
    غزل شمارهٔ ۶۱۲
---
# غزل شمارهٔ ۶۱۲

<div class="b" id="bn1"><div class="m1"><p>به ترک وصل آن تنگ شکر کردن، توان؟ نتوان</p></div>
<div class="m2"><p>چو او باشد بغیر از او نظر کردن، توان؟ نتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سودای کنار او حذر می‌کردم از اول</p></div>
<div class="m2"><p>کنون چون در میان رفتم حذر کردن، توان؟ نتوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرم در دام و تن در قید و دل دربند مهر او</p></div>
<div class="m2"><p>مسلمانان، درین حالت سفر کردن توان؟ نتوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غریبی، مفلسی گر با کسی دلبستگی دارد</p></div>
<div class="m2"><p>بدین تهمت ز شهر او را بدر کردن، توان؟نتوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جرم آنکه این دل میل خوبان می‌کند، وقتی</p></div>
<div class="m2"><p>دل بیچاره را خون در جگر کردن، توان ؟ نتوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز قوس ابروان چشمش چو تیر از غمزه اندازد</p></div>
<div class="m2"><p>بغیر از دیده تیرش را سپر کردن، توان؟ نتوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به زاری پیکر عشق از رخ او نور می‌گیرد</p></div>
<div class="m2"><p>چنان رخ را قیاسی با قمر کردن، توان؟ نتوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا گوید: حدیث من مگو، دیگر چه می‌گویی؟</p></div>
<div class="m2"><p>حدیث پادشاهان را دگر کردن، توان؟ نتوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازان لب اوحدی گر بوسه‌ای بستد شبی پنهان</p></div>
<div class="m2"><p>چه گویی؟ عالمی را زان خبر کردن، توان؟ نتوان</p></div></div>