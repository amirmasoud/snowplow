---
title: >-
    غزل شمارهٔ ۱۶۸
---
# غزل شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>بیا، که دیدن رویت مبارکست صباح</p></div>
<div class="m2"><p>بیا، که زنده به بوی تو می‌شوند ارواح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تویی، که وصل تو هر درد را بود درمان</p></div>
<div class="m2"><p>تویی، که نام تو هر بند را بود مفتاح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروغ روی تو بر جان چنان تجلی کرد</p></div>
<div class="m2"><p>که بر سواد شب تیره پرتو مصباح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به راستی که: نظیرت کجا به دست آرد؟</p></div>
<div class="m2"><p>هزار سال گر آفاق طی کند سیاح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من از شریعت عشق تو دارم این فتوی</p></div>
<div class="m2"><p>که: می پرستی و رندی و عاشقیست مباح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صلاح ما همه در گوشهٔ خراباتست</p></div>
<div class="m2"><p>چرا ملامت ما می‌کنند اهل صلاح؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سزد که: خار خورند از رخ تو گل رویان</p></div>
<div class="m2"><p>که بلبلیست ترا همچو اوحدی مداح</p></div></div>