---
title: >-
    غزل شمارهٔ ۳۲۸
---
# غزل شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>آن روز کو که روی غم اندر زوال بود؟</p></div>
<div class="m2"><p>با او مرا به بوسه جواب و سؤال بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آن رخ چو ماه و جبین چو مشتری</p></div>
<div class="m2"><p>هر ساعتم ز روی وفا اتصال بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از روز وصل در شب هجر او فتاده‌ام</p></div>
<div class="m2"><p>آه! آن زمان کجا شد و باز این چه حال بود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر من چه شب گذشت ز هجران یار دوش؟</p></div>
<div class="m2"><p>نه‌نه، شبش چگونه توان گفت؟سال بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که: بی رخش بتوان بود مدتی</p></div>
<div class="m2"><p>خود بی‌رخش بدیدم و بودن محال بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن بی‌وفا نگر که: جدا گشت و خود نگفت</p></div>
<div class="m2"><p>روزی دلی ربودهٔ این زلف و خال بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای اوحدی، بریدن ازان زلف همچو جیم</p></div>
<div class="m2"><p>دیدی که بر بلای دل خسته دال بود؟</p></div></div>