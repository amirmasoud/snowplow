---
title: >-
    غزل شمارهٔ ۸۱۲
---
# غزل شمارهٔ ۸۱۲

<div class="b" id="bn1"><div class="m1"><p>بر ما ستم و خواری، ای طرفه پسر تا کی؟</p></div>
<div class="m2"><p>وندر پی وصلت ما پوینده بسر تا کی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ما ستمی کرده، خون دل ما خورده</p></div>
<div class="m2"><p>ما بر ستمت پرده میپوش و مدر، تا کی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امشب تو به زیبایی خود خانه بیارایی</p></div>
<div class="m2"><p>فردا که برون آیی رفتی و دگر تا کی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عنبر به دلاویزی بر دامن مه ریزی</p></div>
<div class="m2"><p>این بوالعجب انگیزی در دور قمر تا کی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بنده لبت را من، عاشق طلبت را من</p></div>
<div class="m2"><p>شیرین رطبت را من میبین و مخور تا کی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون هست شبستانت، پر غلغل مستانت</p></div>
<div class="m2"><p>من بندهٔ دستانت، چون خاک بدر تا کی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیوسته به صد زاری، چون اوحدی از خواری</p></div>
<div class="m2"><p>شبهای چنین تاری، با آه سحر تا کی؟</p></div></div>