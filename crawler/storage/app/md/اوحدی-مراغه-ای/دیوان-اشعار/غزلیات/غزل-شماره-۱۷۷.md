---
title: >-
    غزل شمارهٔ ۱۷۷
---
# غزل شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>هر دم از خانه رخ بدر دارد</p></div>
<div class="m2"><p>در پی عاشقی نظر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر زمان مست بر سر کویی</p></div>
<div class="m2"><p>با کسی دست در کمر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار آن کس شود، که مینوشد</p></div>
<div class="m2"><p>دست آن کس کشد، که زر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوست گیرد نهان و فاش کند</p></div>
<div class="m2"><p>مخلصان را درین خطر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که قلاش‌تر ز مردم شهر</p></div>
<div class="m2"><p>پیش او راه بیشتر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات ما شود عاشق</p></div>
<div class="m2"><p>هر که سودای درد سر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار ترسای ما، مترس از کس</p></div>
<div class="m2"><p>عاشقی خود همین هنر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مزن، ای اوحدی، به جز در دوست</p></div>
<div class="m2"><p>کان دگر خانها دو در دارد</p></div></div>