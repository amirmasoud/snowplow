---
title: >-
    غزل شمارهٔ ۲۹۵
---
# غزل شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>آن نه من باشم که چون میرم به تابوتم برند</p></div>
<div class="m2"><p>یا به دوش و سر خراب و مست و مبهوتم برند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مثل زر خالص برون آیم ز آتش، گردرو</p></div>
<div class="m2"><p>از برای آزمایش همچو یاقوتم برند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشتری قوسی نهادست از برای بزم من</p></div>
<div class="m2"><p>تا بسان آفتاب از دلو در حوتم برند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جوان بختی که هستم وقت پیوستن به حق</p></div>
<div class="m2"><p>ننگ دارم گر ز راه چرخ فرتوتم برند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر فلک بینی صعود روح پاکم، زهره وار</p></div>
<div class="m2"><p>فی‌المثل صد نوبت ار در چاه هاروتم برند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون اله خویش را تقدیس کردم سال‌ها</p></div>
<div class="m2"><p>پس مرا می‌زیبد ار بر قدس لاهوتم برند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیستم ز آنها در آن گیتی که بر کاخ بهشت</p></div>
<div class="m2"><p>چون طفیلی از برای خرقه و قوتم برند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کجا من خوان معنی گسترم، کروبیان</p></div>
<div class="m2"><p>طرفه نبود گر به میکائیل سرغوتم برند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایهاالناس، اوحدی وار الوداعی می‌زنم</p></div>
<div class="m2"><p>زانکه وقت آمد کزین زندان ناسوتم برند</p></div></div>