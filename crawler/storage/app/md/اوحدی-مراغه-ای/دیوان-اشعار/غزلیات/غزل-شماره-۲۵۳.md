---
title: >-
    غزل شمارهٔ ۲۵۳
---
# غزل شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>گل ز روی او شرمسار شد</p></div>
<div class="m2"><p>دل چو موی او بی‌قرار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه بر زمینش نهاده رخ</p></div>
<div class="m2"><p>چون بر اسب خوبی سوار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وانکه دید روی نگار من</p></div>
<div class="m2"><p>ز اشک دیده رویش نگار شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر به خاک پایش در افکنم</p></div>
<div class="m2"><p>چون که دست عقلم ز کار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می که نوشیدم، آتشی بر زد</p></div>
<div class="m2"><p>غم که پوشیدم، آشکار شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همرهان من، گو: سفر کنید</p></div>
<div class="m2"><p>کاوحدی به دامی شکار شد</p></div></div>