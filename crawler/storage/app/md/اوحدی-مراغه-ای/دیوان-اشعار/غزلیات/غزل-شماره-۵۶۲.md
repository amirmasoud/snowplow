---
title: >-
    غزل شمارهٔ ۵۶۲
---
# غزل شمارهٔ ۵۶۲

<div class="b" id="bn1"><div class="m1"><p>نظر چو بر لب و دندان یار خویش کنم</p></div>
<div class="m2"><p>طویلهٔ گهر اندر کنار خویش کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا ز خاک درش شرمسار باید بود</p></div>
<div class="m2"><p>اگر نظر به تن خاکسار خویش کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حساب من چه کند یار؟ آن چنان بهتر</p></div>
<div class="m2"><p>که او شمار خود و من شمار خویش کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقیب اگر چه بر آن در ملازمست ولی</p></div>
<div class="m2"><p>سگ استخوان خورد و من شکار خویش کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نیست جای ملامت، بهل، که مدعیان</p></div>
<div class="m2"><p>فغان کنند و من آهسته کار خویش کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرم نهی چو کله تیغ تیز بر تارک</p></div>
<div class="m2"><p>گمان مبر تو که: من ترک یار خویش کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا ز دوست خویش اعتماد آنم نیست</p></div>
<div class="m2"><p>که پنجه با سر و دست نگار خویش کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو اوحدی سخن از لعل آن صنم راند</p></div>
<div class="m2"><p>هزار دامن گوهر نثار خویش کنم</p></div></div>