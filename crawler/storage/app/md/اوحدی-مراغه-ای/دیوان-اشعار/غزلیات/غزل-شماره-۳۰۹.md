---
title: >-
    غزل شمارهٔ ۳۰۹
---
# غزل شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>صبری کنیم تا ستم او چه می‌کند؟</p></div>
<div class="m2"><p>با این دل شکسته غم او چه می‌کند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس علاج درد دلی می‌کنند و ما</p></div>
<div class="m2"><p>دم در کشیده تا الم او چه می‌کند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دست ما چو نیست عنان ارادتی</p></div>
<div class="m2"><p>بگذاشتیم تا کرم او چه می‌کند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بخت من، به دست من انداز دامنش</p></div>
<div class="m2"><p>وین سر ببین که: در قدم او چه می‌کند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیسی دمست یار، مرا پیش او بکش</p></div>
<div class="m2"><p>وآنگه نگاه کن که: دم او چه می‌کند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک ره به پیش دیدهٔ من نام او ببر</p></div>
<div class="m2"><p>وز گریه بین که اشک و نم او چه میکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حیرتم ز مدعی نادرست مهر</p></div>
<div class="m2"><p>تا مهر عشق بر درم او چه می‌کند؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورشید را چو نیست در آن آستانه بار</p></div>
<div class="m2"><p>گویی نسیم در حرم او چه می‌کند؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این دوستان نگر که: نگفتند:اوحدی</p></div>
<div class="m2"><p>با هجر بیش و وصل کم او چه می‌کند؟</p></div></div>