---
title: >-
    غزل شمارهٔ ۶۴۶
---
# غزل شمارهٔ ۶۴۶

<div class="b" id="bn1"><div class="m1"><p>ای اوفتاده در غم عشقت ز پای من</p></div>
<div class="m2"><p>گر دست اوفتاده نگیری تو، وای من!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نای دلم مگیر به چنگ جفا چنین</p></div>
<div class="m2"><p>کز چنگ محنت تو ننالم چو نای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پشتم چو چنبر از غم و نیکوست ماجری</p></div>
<div class="m2"><p>دل بسته‌ام در آن رسن مشک‌سای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردن بسی بگشت، تن و دل به جای بود</p></div>
<div class="m2"><p>روی ترا بدیدم و رفتم ز جای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دشمن لب تو بوسد و در آرزوی آن</p></div>
<div class="m2"><p>کز دور بوسه می‌دهمت، خاک پای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سگ بر در سرای تو گستاخ و من غریب</p></div>
<div class="m2"><p>ای بندهٔ سگان در آن سرای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد ترا به خلق چو گویم چو اوحدی؟</p></div>
<div class="m2"><p>آن به که اعتماد کنم بر خدای من</p></div></div>