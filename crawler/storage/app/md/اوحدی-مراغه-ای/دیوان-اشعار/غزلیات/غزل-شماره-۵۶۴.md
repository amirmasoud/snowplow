---
title: >-
    غزل شمارهٔ ۵۶۴
---
# غزل شمارهٔ ۵۶۴

<div class="b" id="bn1"><div class="m1"><p>تیر تدبیر تو در کیش ندارم، چه کنم؟</p></div>
<div class="m2"><p>سپر جور تو با خویش ندارم، چه کنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق گویند که: ترک‌ش کن و عهدش بشکن</p></div>
<div class="m2"><p>ای عزیزان، چو من این کیش ندارم چه کنم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزنی ناوک و دل شکر نگوید چه کند؟</p></div>
<div class="m2"><p>بزنی خنجر و سر پیش ندارم چه کنم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبعم اندیشهٔ سودای تو کرده‌است و خطاست</p></div>
<div class="m2"><p>چارهٔ طبع بداندیش ندارم چه کنم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طاقت ناوک چشم تو مرا نیست ولیک</p></div>
<div class="m2"><p>چون زدی درد، جگرریش ندارم چه کنم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان فدا کردم و گفتی که: نه اندر خور ماست</p></div>
<div class="m2"><p>در جهان چون من از این بیش ندارم چه کنم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که را دولت وصل تو بود محتشم است</p></div>
<div class="m2"><p>این سعادت من درویش ندارم چه کنم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دی غمت گفت که: بیگانه مشو با خویشان</p></div>
<div class="m2"><p>من بیگانه سر خویش ندارم چه کنم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشت قربان غمت اوحدی و می‌گوید:</p></div>
<div class="m2"><p>تیر تدبیر تو در کیش ندارم چه کنم؟</p></div></div>