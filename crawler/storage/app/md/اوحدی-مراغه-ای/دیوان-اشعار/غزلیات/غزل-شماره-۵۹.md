---
title: >-
    غزل شمارهٔ ۵۹
---
# غزل شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>مکن از برم جدایی، مرو از کنارم امشب</p></div>
<div class="m2"><p>که نمی‌شکیبد از تو دل بی‌قرارم امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز طرب نماند باقی، که مرا تو هم وثاقی</p></div>
<div class="m2"><p>چو لب تو گشت ساقی نکند خمارم امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه زنی صلای رفتن؟چو نماند پای رفتن</p></div>
<div class="m2"><p>چه کنی هوای رفتن؟ که نمیگذارم امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رخم چو بر گشادی در وعدها که دادی</p></div>
<div class="m2"><p>نه شگفت اگر به شادی نفسی برآرم امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شدم وصال روزی، به توقعم چه سوزی؟</p></div>
<div class="m2"><p>چه شود که بر فروزی دل سوکوارم امشب؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل بخت شد شکفته، که شوم چو بخت خفته</p></div>
<div class="m2"><p>که تو داده‌ای نهفته بر خویش بارم امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر از هزار دستم، بکشند خوار و پستم</p></div>
<div class="m2"><p>چو یکی همی پرستم، چه غم از هزارم امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر آرزو نجویم، پی آرزو نپویم</p></div>
<div class="m2"><p>همه از تو شکر گویم، که تویی شکارم امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل اوحدی تو داری، چو نمی‌دهی بیاری</p></div>
<div class="m2"><p>نکنم به ترک زاری، که ز عشق زارم امشب</p></div></div>