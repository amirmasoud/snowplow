---
title: >-
    غزل شمارهٔ ۳۸۸
---
# غزل شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>یک شبم دادی به عمری پیش خود بار، ای پسر</p></div>
<div class="m2"><p>بعد از آن یادم نکردی، یاد می‌دار، ای پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیک بد حالم ز دست هجر حال آشوب تو</p></div>
<div class="m2"><p>لطف کن،ما را به حال خویش مگذار، ای پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتهٔ چشم توام، غافل مباش از حال من</p></div>
<div class="m2"><p>گوشمالم بر مده، گوشی به من دار، ای پسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نالهٔ من در غم هجر تو شد زیر، ای نگار</p></div>
<div class="m2"><p>رحمتی کن، کز غم هجر توام زار، ای پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون گل وصلی نخواهی هرگزم دادن به دست</p></div>
<div class="m2"><p>خارم از پای دل حیران برون آر، ای پسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته‌ای: در کار عشق من بباید باخت جان</p></div>
<div class="m2"><p>خود ندارم در دو گیتی غیر ازین کار، ای پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش: بوسی بده، گفتا که: پر بشمار زر</p></div>
<div class="m2"><p>زر ندارم، چون شمارم؟ بوسه بشمار، ای پسر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیگران را چون به وصل خویشتن کردی عزیز</p></div>
<div class="m2"><p>اوحدی را همچو خاک ره مکن خوار، ای پسر</p></div></div>