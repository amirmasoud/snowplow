---
title: >-
    غزل شمارهٔ ۵۶۷
---
# غزل شمارهٔ ۵۶۷

<div class="b" id="bn1"><div class="m1"><p>ای نرگست به شوخی صدبار خورده خونم</p></div>
<div class="m2"><p>بر من ترحمی کن،بنگر: که بی‌تو چونم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافل شدی ز حالم، با آنکه دور بینی</p></div>
<div class="m2"><p>عاجز شدم ز دستت، با آنکه ذوفنونم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تریاک زهر خوبان سیمست و من ندارم</p></div>
<div class="m2"><p>درمان درد عاشق صبرست و من زبونم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کس گرفت با خویش از ظاهرم قیاسی</p></div>
<div class="m2"><p>بگذار تا ندانند احوال اندرونم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر خون خود بریزم صدبار در غم تو</p></div>
<div class="m2"><p>دانم که: بار دیگر رخصت دهی به خونم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل خواستی تو از من، تشریف ده زمانی</p></div>
<div class="m2"><p>گر جان دریغ بینی، از عاشقان دونم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بس فسون که کردم افسانه شد دل من</p></div>
<div class="m2"><p>خود در تو نیست گیرا افسانه و فسونم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میم دهان خود را از من نهان چه کردی؟</p></div>
<div class="m2"><p>باری، نگاه می‌کن در قامت چو نونم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر اوحدی سکونی دارد، صبور باشد</p></div>
<div class="m2"><p>من چون کنم صبوری آخر؟ که بی‌سکونم</p></div></div>