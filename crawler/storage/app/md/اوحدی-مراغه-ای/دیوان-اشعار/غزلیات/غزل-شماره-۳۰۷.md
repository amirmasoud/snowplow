---
title: >-
    غزل شمارهٔ ۳۰۷
---
# غزل شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>عاشق کسی بود که چو عشقش ندی کند</p></div>
<div class="m2"><p>اول قدم ز روی وفا جان فدی کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبر، که دستگیری عاشق کند ز لطف</p></div>
<div class="m2"><p>گر جان کنند در سر کارش کری کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهری که دشمنی دهد از بهر رنج، تو</p></div>
<div class="m2"><p>بستان به یاد دوست بخور، تا شفی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بستم دکان مشغله را در به روی خلق</p></div>
<div class="m2"><p>تا عشق او در آید و بیع و شری کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آستان نمی‌گذرم تا جفای او</p></div>
<div class="m2"><p>خاکم وظیفه سازد و خونم جری کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر کشتگان تیغ غم او کفن مپوش</p></div>
<div class="m2"><p>کان به شهید عشق که از خون ردی کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجنون که شب رود بر لیلی، شگفت نیست</p></div>
<div class="m2"><p>روز از تحملی ز سگان حمی کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد هواست، چار حد آن خراب کن</p></div>
<div class="m2"><p>هر خانه را که جز هوس او بنی کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای اوحدی، ز هر چه کنی کار عشق به</p></div>
<div class="m2"><p>آیا کسی که عشق ندارد چه می‌کند؟</p></div></div>