---
title: >-
    غزل شمارهٔ ۹۶
---
# غزل شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>بنگرید این فتنه را کز نو پدیدار آمده‌ست</p></div>
<div class="m2"><p>خلق شهری از دل و جانش خریدار آمده‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغ رویش را ز چاه غبغبست امسال آب</p></div>
<div class="m2"><p>زان سبب سیب زنخدانش به از پار آمده‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقد هر خوبی که در گنج ملاحت جمع بود</p></div>
<div class="m2"><p>یک به یک در حلقهٔ آن زلف چون مار آمده‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بارها جان عزیز خویش را در پای او</p></div>
<div class="m2"><p>پیشکش کردیم و اندر پیش او خوار آمده‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوسه‌ای زان لعل بربودیم و آسان گشت کار</p></div>
<div class="m2"><p>گر چه بر طبع حسودان نیک دشوار آمده‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به کار ما نظر کرد او چه باشد؟ سالها</p></div>
<div class="m2"><p>خون دل خوردیم تا امروز در کار آمده‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ آن زلف سر بر دوش کرد از دوش باز</p></div>
<div class="m2"><p>اوحدی را کز کلاه خسروی عار آمده‌ست</p></div></div>