---
title: >-
    غزل شمارهٔ ۲۲۹
---
# غزل شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>بهار و بوستان ما سر کوی تو بس باشد</p></div>
<div class="m2"><p>چراغ مجلس ما پرتو روی تو بس باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برای نزهت ار وقتی بیارایند جنت را</p></div>
<div class="m2"><p>مرا از هر که در جنت نظر سوی تو بس باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خون خوردن میموزان دل ما را به خوان غم</p></div>
<div class="m2"><p>که ما را خود جگر خوردن ز پهلوی تو بس باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر خواهی که: جفت غم کنی خلق جهانی را</p></div>
<div class="m2"><p>اشارت گونه‌ای از طاق ابروی تو بس باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرت سودای آن دارد که: که ملک چین به دست آری</p></div>
<div class="m2"><p>سوادی از سر آن زلف هندوی تو بس باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شوق کعبه گو: حاجی، بیابان گیر و زحمت کش</p></div>
<div class="m2"><p>طواف عاشقان گرد سر کوی تو بس باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خون اوحدی دست نگارین را چه رنجانی؟</p></div>
<div class="m2"><p>که او را شیوه‌ای از چشم جادوی تو بس باشد</p></div></div>