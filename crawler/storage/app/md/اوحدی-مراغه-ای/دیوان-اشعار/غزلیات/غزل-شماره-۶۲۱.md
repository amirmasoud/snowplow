---
title: >-
    غزل شمارهٔ ۶۲۱
---
# غزل شمارهٔ ۶۲۱

<div class="b" id="bn1"><div class="m1"><p>تا ندانی ز جسم و جان مردن</p></div>
<div class="m2"><p>پیش آن رخ کجا توان مردن؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقی چیست؟ زنده بودن فاش</p></div>
<div class="m2"><p>وآنگه از عشق او نهان مردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از برون جهان نشاید مرد</p></div>
<div class="m2"><p>در جهان باید از جهان مردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ دانی حیات باقی چیست؟</p></div>
<div class="m2"><p>پیش آن خاک آستان مردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهل یاریست، یار، در غم او</p></div>
<div class="m2"><p>سهل کاریست هر زمان مردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوسه‌ای زان دهن بخواهم خواست</p></div>
<div class="m2"><p>که نشاید به رایگان مردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی، دل به دیگری مسپار</p></div>
<div class="m2"><p>تا نباید چو دیگران مردن</p></div></div>