---
title: >-
    غزل شمارهٔ ۶۴۱
---
# غزل شمارهٔ ۶۴۱

<div class="b" id="bn1"><div class="m1"><p>نگارا، چرا شدی نهان از نهان من؟</p></div>
<div class="m2"><p>چه کردم که گشته‌ای جهان از جهان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کینم مخای لب، چو آنم که پیش ازین</p></div>
<div class="m2"><p>همی بر نداشتی دهان از دهان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو من پر شدم ز تو، ز من پر شد این جهان</p></div>
<div class="m2"><p>به نوعی که تنگ شد مکان از مکان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان در تو گم شدم که: گر جویدم کسی</p></div>
<div class="m2"><p>نیابد به عمرها نشان از نشان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو سرمایهٔ دکان مرا در سر تو شد</p></div>
<div class="m2"><p>چرا دور می‌کنی دکان از دکان من؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به گوشت همی رسد که: من می‌کنم زیان</p></div>
<div class="m2"><p>ولی در تو کی رسد زیان از زیان من؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا در دل آتشیست نهفته ز هجر تو</p></div>
<div class="m2"><p>که بر می‌کند کنون زبان از زبان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو شد در دلم پدید خبرها، که می‌شنید</p></div>
<div class="m2"><p>خبرها بسی بود عیان از عیان من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی فتنها که گشت پدید از جمال تو</p></div>
<div class="m2"><p>بسی فیضها که شد روان از روان من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا در زمین مجوی، مرا از زمان مپرس</p></div>
<div class="m2"><p>که غیرت برد همی زمان از زمان من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخوانند سالها،درین وجد و حالها</p></div>
<div class="m2"><p>سخن کاوحدی کند بیان از بیان من</p></div></div>