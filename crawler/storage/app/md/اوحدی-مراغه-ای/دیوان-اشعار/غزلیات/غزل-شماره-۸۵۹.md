---
title: >-
    غزل شمارهٔ ۸۵۹
---
# غزل شمارهٔ ۸۵۹

<div class="b" id="bn1"><div class="m1"><p>آمد بهار، خیمه بزن بر کنار جوی</p></div>
<div class="m2"><p>بر دوست کن کنار وز دشمن کنار جوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌چار فصل عیش فزاید، به می‌گرای</p></div>
<div class="m2"><p>گل پنج روز بیش نپاید، به باغ پوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بستان پر از بدایع صنعست، لیک هیچ</p></div>
<div class="m2"><p>رنگیش نیست بیرخ یار بدیع جوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سنگ و روست آنکه نشد گرم دل به عشق</p></div>
<div class="m2"><p>در عهد آن نگار مکن یاد سنگ و روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی که بی‌تکلف چشمش نظر کنی</p></div>
<div class="m2"><p>از نقش صورت دگران لوح دل بشوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای باد، بوی زلف چو چوگان او بیار</p></div>
<div class="m2"><p>تا سر به مژده در کف پایت نهم چو گوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دم به شیوهٔ دگرم صید میکنند</p></div>
<div class="m2"><p>گاهی به قند آن لب و گاهی به بند موی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با قد آن صنم ز چمن، سرو گو: مبال</p></div>
<div class="m2"><p>با روی آن پری، ز زمین لاله گو: مروی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای اوحدی، تو خاک سر کوی دوست باش</p></div>
<div class="m2"><p>باشد که دوست را گذر افتد به خاک کوی</p></div></div>