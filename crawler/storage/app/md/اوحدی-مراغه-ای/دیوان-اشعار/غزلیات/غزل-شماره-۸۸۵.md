---
title: >-
    غزل شمارهٔ ۸۸۵
---
# غزل شمارهٔ ۸۸۵

<div class="b" id="bn1"><div class="m1"><p>مشتاق آن نگارم آیا کجاست گویی؟</p></div>
<div class="m2"><p>با ما نمی‌نشیند بی ما چراست گویی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما در هوای رویش چون ذره گشته پیدا</p></div>
<div class="m2"><p>وین قصه خود بر او باد هواست گویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد بار کشت ما را نادیده هیچ جرمی</p></div>
<div class="m2"><p>در دین خوبرویان کشتن رواست گویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزدیک او شد آن دل کز غم شکسته بودی</p></div>
<div class="m2"><p>این غم هنوز دارم آن دل کجاست گویی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زلف کژرو او گر بشنوی نسیمی</p></div>
<div class="m2"><p>تا زنده‌ای حکایت زان سر و راست گویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دیگران بیاری آسان بر آورد سر</p></div>
<div class="m2"><p>این ناز و سر گرانی از بخت ماست گویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون دلم بریزد و آنگاه خشم گیرد</p></div>
<div class="m2"><p>آنرا سبب ندانم این خون بهاست گویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتا که: جان شیرین پیش من آر و زین غم</p></div>
<div class="m2"><p>تن خسته شد ولیکن دل را رضاست گویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از اوحدی دل و دین بردند و عقل و دانش</p></div>
<div class="m2"><p>رخت گزیده گم شد، دزد آشناست گویی</p></div></div>