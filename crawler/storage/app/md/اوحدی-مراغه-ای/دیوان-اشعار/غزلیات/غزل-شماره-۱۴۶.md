---
title: >-
    غزل شمارهٔ ۱۴۶
---
# غزل شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>چه شد آن سرو سهی؟ کز لب این بام برفت</p></div>
<div class="m2"><p>که به یک دیدن او از دلم آرام برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه سخن کرد به چشم و چه شکر گفت ز لب؟</p></div>
<div class="m2"><p>که رواج شکر و قیمت بادام برفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دلش بر بنهادیم و به جان پرسیدیم</p></div>
<div class="m2"><p>تا نگویی تو که: بی پرسش و اکرام برفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام در دست گرفتیم به یاد دهنش</p></div>
<div class="m2"><p>می به شرم لب او چون عرق از جام برفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نتوانم شدن از سایهٔ دیوارش دور</p></div>
<div class="m2"><p>که توانم ز تن و قوتم از کام برفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای صبا، از دهن او خبری بارسان</p></div>
<div class="m2"><p>که به امید تو ما را همه ایام برفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوست در ولولهٔ آن که: چو قاصد برسد</p></div>
<div class="m2"><p>دشمن اندر طلب آن که: چه پیغام برفت؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل ما را به چه پرسی که: چرا شد بر او؟</p></div>
<div class="m2"><p>حاجتش بود، به آوازهٔ انعام برفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کرا بر سر ازین درد بلایی نرسید</p></div>
<div class="m2"><p>نتوان گفت که: او نیک سرانجام برفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن که از خنجر او کشته نشد، مردارست</p></div>
<div class="m2"><p>دل که بر آتش او پخته نشد، خام برفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما خود آن دانه ندیدیم که این مور برد</p></div>
<div class="m2"><p>بلکه مرغی نشنیدیم کزین دام برفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرچه سر گشته بسی دارد و عاشق بسیار</p></div>
<div class="m2"><p>ازمیان همه در عشق مرا نام برفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اوحدی گر ز بر او برود معذورست</p></div>
<div class="m2"><p>کز لبش کام نمی‌دید و به ناکام برفت</p></div></div>