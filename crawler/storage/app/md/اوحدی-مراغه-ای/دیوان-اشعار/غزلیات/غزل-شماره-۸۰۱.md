---
title: >-
    غزل شمارهٔ ۸۰۱
---
# غزل شمارهٔ ۸۰۱

<div class="b" id="bn1"><div class="m1"><p>باز آمدی، که خونم بر خاک در بریزی</p></div>
<div class="m2"><p>توفان موج خیزم زین چشم تر بریزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر ساعتی به شکلی، هر لحظه‌ای بینگی</p></div>
<div class="m2"><p>دوداز دلم برآری، خون از جگر بریزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تشنه‌ای به خونم، حاکم تویی،ولیکن</p></div>
<div class="m2"><p>در پای خویش ریزش،روزی اگر بریزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانند آفتابی، کز بس شعاع خوبی</p></div>
<div class="m2"><p>چون دیده بر تو دوزم، نور از نظر بریزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شهر اگر نماند شکر، چه غم؟ که روزی</p></div>
<div class="m2"><p>لعل تو گر بخندد، شهری شکر بریزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بالله که برنگیرم سر ز آستانهٔ تو</p></div>
<div class="m2"><p>گر خنجرم چو باران بر فرق سر بریزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد نوبت اوحدی را خون ریختی و گر تو</p></div>
<div class="m2"><p>آنی که می‌شناسم، بار دگر بریزی</p></div></div>