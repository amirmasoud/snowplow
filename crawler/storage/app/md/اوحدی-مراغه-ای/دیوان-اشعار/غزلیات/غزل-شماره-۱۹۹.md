---
title: >-
    غزل شمارهٔ ۱۹۹
---
# غزل شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>به یک نظر دل شهری شکاردانی کرد</p></div>
<div class="m2"><p>همیشه جور کنی و آشکاردانی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز طره غالیه بر یاسمین توانی برد</p></div>
<div class="m2"><p>به شیوه معجزه با خنده یار دانی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو باد اگرچه گذر می‌کنی بهر سویی</p></div>
<div class="m2"><p>به سوی ما نه همانا گذار دانی کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر مراد دل خود طلب کنیم از تو</p></div>
<div class="m2"><p>مراد دشمن ما اختیار دانی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو این ستیزه و ناز و عتاب و شوخی را</p></div>
<div class="m2"><p>اگر به ترک بگویی چه کار دانی کرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه پرسمت ز وفا، گویی: آن نمی‌دانم</p></div>
<div class="m2"><p>ولی چو بوسه بخواهم کناردانی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ستم که بر دل من کرده‌ای، عجب دارم</p></div>
<div class="m2"><p>که گر به یاد تو آرم شمار دانی کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه طفلی و خود را نهی به نادانی</p></div>
<div class="m2"><p>هنوز چارهٔ چون من هزار دانی کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگار چهره بپوشی ز اوحدی، لیکن</p></div>
<div class="m2"><p>به خون دیده رخش را نگار دانی کرد</p></div></div>