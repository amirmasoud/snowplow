---
title: >-
    غزل شمارهٔ ۵۳۸
---
# غزل شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>یارب، تو حاضری که ز دستش چه میکشم؟</p></div>
<div class="m2"><p>وز عشوه‌های نرگس مستش چه میکشم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد نوبت آزمودم و جز بند دل نبود</p></div>
<div class="m2"><p>دیگر کمند زلف چو شستش چه میکشم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون آهوان به حکم خطا حلق خویشتن</p></div>
<div class="m2"><p>در حلقه‌های سنبل پستش چه میکشم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم: به دامنش بکشم گرد از آسمان</p></div>
<div class="m2"><p>چون گرد بر ضمیر نشستش چه می‌کشم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چندین هزار جو و جفا زان دهن، که نیست</p></div>
<div class="m2"><p>از بهر یک دو بوسه که هستش، چه میکشم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خونم ز دل گشود و برویم ببست در</p></div>
<div class="m2"><p>بنگر که: از گشاد وز بستش چه میکشم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایدل، ندیده‌ای، برو از اوحدی بپرس</p></div>
<div class="m2"><p>تا از دو لعل کینه پرستش چه میکشم؟</p></div></div>