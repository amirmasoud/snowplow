---
title: >-
    غزل شمارهٔ ۳۴۱
---
# غزل شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>روز وداع گریه نه در حد دیده بود</p></div>
<div class="m2"><p>توفان اشک تا به گریبان رسیده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزدیک بود کز غم من ناله برکشد</p></div>
<div class="m2"><p>از دور هر که نالهٔ زارم شنیده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدی که: چون به خون دلم تیغ برکشید؟</p></div>
<div class="m2"><p>آن کس که جان به خون دلش پروریده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن سست عهد سرکش بدمهر سنگدل</p></div>
<div class="m2"><p>ما را به هیچ داد، که ارزان خریده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مرغ وحشی از قفس تن رمیده شد</p></div>
<div class="m2"><p>آن دل، که در پناه رخش آرمیده بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان دردمند شد تن مسکین، که مدتی</p></div>
<div class="m2"><p>دل درد آن دو نرگس بیمار چیده بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز وداع دل بشد از دست و حیف نیست</p></div>
<div class="m2"><p>کان روز اوحدی طمع از جان بریده بود</p></div></div>