---
title: >-
    غزل شمارهٔ ۴۴۲
---
# غزل شمارهٔ ۴۴۲

<div class="b" id="bn1"><div class="m1"><p>با یار بی‌وفا نتوان گفت حال خویش</p></div>
<div class="m2"><p>آن به که دم فرو کشم از قیل و قال خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من شرح حال خویش ندانم که چیست خود؟</p></div>
<div class="m2"><p>زیرا که یک دمم نگذارد به حال خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنرا که هست طالع ازین کار، گو: بکوش</p></div>
<div class="m2"><p>ما را نبود بخت و گرفتیم فال خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل، نگفتمت که: مخواه از لبش مراد؟</p></div>
<div class="m2"><p>دیدی که: چون شکسته شدی از سؤال خویش؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بی‌وفا، ز عشق منت گر خبر شود</p></div>
<div class="m2"><p>دانم که شرمسار شوی از فعال خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندان مرو، که من به تامل ز راه فکر</p></div>
<div class="m2"><p>نقش تو استوار کنم در خیال خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جد ترا، اگر ز جمالت خبر شود</p></div>
<div class="m2"><p>ای بس درودها که فرستد به آل خویش!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را به خویش خوان و بر خویش بارده</p></div>
<div class="m2"><p>باشد که بعد ازین برهیم از ضلال خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای اوحدی، مقیم سر کوی یار باش</p></div>
<div class="m2"><p>گر در سرای دوست نیابی مجال خویش</p></div></div>