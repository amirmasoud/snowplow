---
title: >-
    غزل شمارهٔ ۵۸۹
---
# غزل شمارهٔ ۵۸۹

<div class="b" id="bn1"><div class="m1"><p>حال این پیکر از آن بتگر دانا پرسیم</p></div>
<div class="m2"><p>یا خود از پیش حکیمان توانا پرسیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه طلسمست درین گنج و چه رسمست او را</p></div>
<div class="m2"><p>یا چه اسمست؟ کسی نیست کزو وا پرسیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه بسیار درین خانه، ولیکن ما را</p></div>
<div class="m2"><p>راه آن نیست که گوییم سخن، یا پرسیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که ما را بشناسد به خدا راه برد</p></div>
<div class="m2"><p>کو شناسنده؟ که از وی سخن ما پرسیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان مسیحست و صلیبش تن و این معنی را</p></div>
<div class="m2"><p>زود دانیم اگر پیش مسیحا پرسیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر فرزند درین خانه نشد پیدا، لیک</p></div>
<div class="m2"><p>چون به آن خانه در آییم ز بابا پرسیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روح را پیشتر از آدم و حوا اصلیست</p></div>
<div class="m2"><p>ما نه طفلیم، که از آدم و حوا پرسیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد هزار اسم فزونست و مسماش یکی</p></div>
<div class="m2"><p>اسم جوییم کنون؟ یا ز مسما پرسیم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حال امروز بپرسیم ز داننده به نقد</p></div>
<div class="m2"><p>حال فردا بگذاریم، که فردا پرسیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قطره‌ای بیش نباشد دو جهان از دریاش</p></div>
<div class="m2"><p>صفت قطره همان به که ز دریا پرسیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اوحدی، رو، تو سخن گوی که مقصود سخن</p></div>
<div class="m2"><p>یک حدیثست و هم از مردم یکتا پرسیم</p></div></div>