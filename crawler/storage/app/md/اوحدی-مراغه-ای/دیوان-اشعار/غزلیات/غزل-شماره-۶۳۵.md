---
title: >-
    غزل شمارهٔ ۶۳۵
---
# غزل شمارهٔ ۶۳۵

<div class="b" id="bn1"><div class="m1"><p>سر بارندگی دارد دو چشم تند بار من</p></div>
<div class="m2"><p>که فتح‌الباب هجرانست و تحویل نگار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا چون ماه در عقرب خوش آمد روی و زلف او</p></div>
<div class="m2"><p>از آن نیکی نمیبینم، که بد بود اختیار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من آن چرخم، که از جانست مهرم در میان دل</p></div>
<div class="m2"><p>من آن صبحم، که از اشکست پروین در کنار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا روی چو تقویمست و به روی جدولی خونین</p></div>
<div class="m2"><p>که حکم آن نشد، منسوخ چون تقویم پار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرم را اتصالی هست کلی با خیال او</p></div>
<div class="m2"><p>از آن سر در نمی‌آرد به دوش بردبار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خبر ده ز اجتماع او تنم را، تا برون آید</p></div>
<div class="m2"><p>به استقبال روی او دل و صبر و قرار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیاپی مایلست این دل به قرب نقطهٔ خالش</p></div>
<div class="m2"><p>دریغ ار خارج از مرکز نیفتادی مدار من!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سرحد وصالش گر زوجهی راه میابم</p></div>
<div class="m2"><p>شرف هم خانه میگردد دگر با روزگار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو ماه از عقدهٔ زلفش مگر دارد خسوف آن رخ؟</p></div>
<div class="m2"><p>که از آغاز تاثیرش زمستان شد بهار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دانستی کز آن تست بیت‌المال دل یکسر</p></div>
<div class="m2"><p>به سهم‌الغیب آن غمزه بگو: تا کیست یار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طریق اجتماعی نیست دل را با فرح بی‌تو</p></div>
<div class="m2"><p>ازان چون عقلهٔ زلف تو منکوسست کار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز اشکم نقطه میراند غمت بر تختهای رخ</p></div>
<div class="m2"><p>که در هنگامها گوید نهان و آشکار من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فلکها را رصد کردم من، ای ماه و نپندارم</p></div>
<div class="m2"><p>کزیشان چون تو خورشیدی بتابد بر دیار من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو اصطرلاب این دل را بگردان در شعاع رخ</p></div>
<div class="m2"><p>ببین تا ارتفاع مهر چندست از شمار من؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آن خاک اوحدی را گر نهی بر جبهه اکلیلی</p></div>
<div class="m2"><p>به شعری میبرد شعر چو در شاهوار من</p></div></div>