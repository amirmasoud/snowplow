---
title: >-
    غزل شمارهٔ ۳۳۴
---
# غزل شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>روز هجران آن نگار این بود</p></div>
<div class="m2"><p>منتهای وصال یار این بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی او لالهٔ بهارم بود</p></div>
<div class="m2"><p>عمر آن لالهٔ بهار این بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست از اندیشه در کنارم خون</p></div>
<div class="m2"><p>بحر اندیشه را کنار این بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرده بودم ز وصل جامی نوش</p></div>
<div class="m2"><p>می‌آن جام را خمار این بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان سفر کرد و بر قرار خودی</p></div>
<div class="m2"><p>ای دل بی‌وفا، قرار این بود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بار غم بر دلم همی بینی</p></div>
<div class="m2"><p>آخر، ای چشم اشکبار، این بود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم، ای چرخ، زینهاری تو</p></div>
<div class="m2"><p>آن همه عهد و زینهار این بود؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اختیاری دگر نشاید کرد</p></div>
<div class="m2"><p>چرخ را چون که اختیار این بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خار و گل با همند، می‌دیدم</p></div>
<div class="m2"><p>گل ز دستم برفت و خار این بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرگ ازین دیدها نهان آید</p></div>
<div class="m2"><p>پیش من مرگ آشکار این بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل ما از فراق می‌ترسید</p></div>
<div class="m2"><p>چون بدیدیم، ختم کار این بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اوحدی، بر تو گر جفایی رفت</p></div>
<div class="m2"><p>چه کنی؟ حکم کردگار این بود</p></div></div>