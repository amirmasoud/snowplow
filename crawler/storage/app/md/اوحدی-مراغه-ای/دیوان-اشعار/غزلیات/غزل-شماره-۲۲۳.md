---
title: >-
    غزل شمارهٔ ۲۲۳
---
# غزل شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>آنکه دلم برد و جور کرد و جدا شد</p></div>
<div class="m2"><p>صید ندیدم ز بند او، که رها شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دگران سرکشی نمود و تکبر</p></div>
<div class="m2"><p>سرکش و بیدادگر به طالع ما شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنج که بردیم باد برد و تلف گشت</p></div>
<div class="m2"><p>سعی که کردیم هرزه بود و هبا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوبت آن وصل را که وعده همی داد</p></div>
<div class="m2"><p>هیچ به فرصت نگه کرد و قضا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز برم برد و زهره نیست که گویم:</p></div>
<div class="m2"><p>آن دل سرگشته را که برد و کجا شد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر کندم قصد جان دریغ ندارم</p></div>
<div class="m2"><p>کام من آمد چو کام دوست روا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با همه جوری دلم نداد که گویم:</p></div>
<div class="m2"><p>اوحدی از هجر او شکسته چرا شد؟</p></div></div>