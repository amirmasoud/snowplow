---
title: >-
    غزل شمارهٔ ۵۷۰
---
# غزل شمارهٔ ۵۷۰

<div class="b" id="bn1"><div class="m1"><p>ز چشم خلق هوس می‌کند که گوشه گزینم</p></div>
<div class="m2"><p>ولی تعلق خاطر نمی‌هلد که نشینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوار گشتم و گفتم: ز دست او ببرم جان</p></div>
<div class="m2"><p>کمند عشق بیفگند و درکشید ز زینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گناه من همه در دوستی همین که: بر آتش</p></div>
<div class="m2"><p>گرم چو عود بسوزد، گناه دوست نبینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز من حکایت مهر و حدیث عشق چه پرسی؟</p></div>
<div class="m2"><p>که رفت عمر درین محنت و هنوز برینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمین ز چشم کماندار او، رواست که سازد</p></div>
<div class="m2"><p>مرا که نیست کمان چنان، چه مرد کمینم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کدام خواب گرانت ربوده بود؟ نگارا</p></div>
<div class="m2"><p>که هیچ گوش نکردی به ناله‌های حزینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدم به پرسش من، دیر شد، که رنجه نکردی</p></div>
<div class="m2"><p>کنون که رنج بتر شد، بپرس بهتر ازینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا به شربت و دارو نیاز و میل نباشد</p></div>
<div class="m2"><p>دوای درد من این مایه بس که: درد تو چینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بوستان مبر، ای اوحدی، مرا ز بر او</p></div>
<div class="m2"><p>که با شمایل او فارغ از بهشت برینم</p></div></div>