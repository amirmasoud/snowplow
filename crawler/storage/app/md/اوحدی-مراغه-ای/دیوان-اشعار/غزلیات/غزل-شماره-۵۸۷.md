---
title: >-
    غزل شمارهٔ ۵۸۷
---
# غزل شمارهٔ ۵۸۷

<div class="b" id="bn1"><div class="m1"><p>دیریست تا ز دست غمت جان نمی‌بریم</p></div>
<div class="m2"><p>وقتست کز وصال تو جانی بپروریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه‌نه، چه جای وصل؟ که ما را ز روزگار</p></div>
<div class="m2"><p>این مایه بس که: یاد تو در خاطر آوریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن چتر سلطنت، که تو در سر کشیده‌ای</p></div>
<div class="m2"><p>در سایهٔ تو هم نگذارد که بنگریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیدیست هر به ماهی اگر ابروی ترا</p></div>
<div class="m2"><p>همچون هلال عید ببینیم و بگذریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی به بزم و مجلس ما در نیامدی</p></div>
<div class="m2"><p>تا بنگری که: بی‌تو چه خونابه میخوریم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>احول ما، کجاست، دبیری که بشنود</p></div>
<div class="m2"><p>تا نامه می‌نویسد و ما جامه می‌دریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ما کسی به هیچ مسلمان خبر نکرد:</p></div>
<div class="m2"><p>کامروز مدتیست که در بند کافریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناز ترا کجاست خریدار به ز ما؟</p></div>
<div class="m2"><p>کان را بهر بها که تو گویی همی‌خریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر روز رنج ما ز فراقت بتر شود</p></div>
<div class="m2"><p>ایدون گمان بری تو که هر روز بهتریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوشی بما نداشته‌ای هیچ بار و ما</p></div>
<div class="m2"><p>در گوش کرده حلقه و چون حلقه بر دریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما را، اگر چه صد سخن تلخ گفته‌ای</p></div>
<div class="m2"><p>با یاد گفتهای تو در شهد و شکریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صد شب گریستیم ز هجرت چو اوحدی</p></div>
<div class="m2"><p>باشد که: با وصال تو روزی به سر بریم</p></div></div>