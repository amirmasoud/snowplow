---
title: >-
    غزل شمارهٔ ۸۷۵
---
# غزل شمارهٔ ۸۷۵

<div class="b" id="bn1"><div class="m1"><p>دمشق عشق شد این شهر و مصر زیبایی</p></div>
<div class="m2"><p>ز حسن طلعت این دلبران یغمایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تنگ شکر مصری برون نیاورند</p></div>
<div class="m2"><p>به لطف شکر تنگ تو در شکر خایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمر که بسته‌ای، ای ماه، بر میان شب و روز</p></div>
<div class="m2"><p>مگر به کشتن ما بسته‌ای که نگشایی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر به مصر غلامی عزیز شد چه عجب؟</p></div>
<div class="m2"><p>به هر کجا که تو رفتی عزیز می‌آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو روی باز کنی نیستی کم از یوسف</p></div>
<div class="m2"><p>چو غنج و ناز کنی بهتر از زلیخایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برو تو شهر بگو: تا دگر نیارایند</p></div>
<div class="m2"><p>کزان جمال تو خود شهرها بیارایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سرای توبیت‌المقدسست امروز</p></div>
<div class="m2"><p>رخ تو قبلهٔ شوریدگان شیدایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جنگ رفتن سلطان دگر چه محتاجست؟</p></div>
<div class="m2"><p>که چون تو شاه سوارش به صلح می‌آیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز چین زلف تو چون اوحدی حدیثی گفت</p></div>
<div class="m2"><p>برو مگیر، که آشفته بود و سودایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو هندوانت اگر سر به بندگی ننهد</p></div>
<div class="m2"><p>به دست خود چو فرنگش بزن به رسوایی</p></div></div>