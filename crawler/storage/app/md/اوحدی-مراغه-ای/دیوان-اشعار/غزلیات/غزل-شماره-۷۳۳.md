---
title: >-
    غزل شمارهٔ ۷۳۳
---
# غزل شمارهٔ ۷۳۳

<div class="b" id="bn1"><div class="m1"><p>ای شهر شگرفان را غیر از تو امیری نه</p></div>
<div class="m2"><p>بی‌یاد تو در عالم ذهنی و ضمیری نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهری به مراد تو گردیده مرید، آنگه</p></div>
<div class="m2"><p>این جمله مریدان را جز عشق تو پیری نه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نامه نبشتن را دربسته میان، لیکن</p></div>
<div class="m2"><p>خود لایق این معنی در شهر دبیری نه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلقی به خیال تو، مشتاق جمال تو</p></div>
<div class="m2"><p>وز صورت حال تو داننده خبیری نه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز روی تو در عالم من خوب نمی‌دانم</p></div>
<div class="m2"><p>ای از همه خوبانت مثلی و نظیری نه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا غمزهٔ شوخت را دیدم، ز دلم دایم</p></div>
<div class="m2"><p>خون می‌چکد و در وی پیکانی و تیری نه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشت اوحدی از مهرت خشنود به درویشی</p></div>
<div class="m2"><p>وانگاه به غیر از تو رویش به امیری نه</p></div></div>