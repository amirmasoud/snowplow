---
title: >-
    غزل شمارهٔ ۳۰۵
---
# غزل شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>یار آن کسی بود که به کارت نگه کند</p></div>
<div class="m2"><p>باری نگه کنی، دوسه بارت نگه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه دعا به نالهٔ زیرت چو گوش کرد</p></div>
<div class="m2"><p>روز بلا به گریهٔ زارت نگه کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی اگر ترا به میان در کشد غمی</p></div>
<div class="m2"><p>دستی بگیرد و ز کنارت نگه کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار کسی بکش، که ز پای ار بیوفتی</p></div>
<div class="m2"><p>باری به اوفتادن بارت نگه کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مست شد ز بادهٔ اندوه او سرت</p></div>
<div class="m2"><p>جامی دو کم دهد، به خمارت نگه کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از مهر دوستی چه کنی فخر؟ کو ز کبر</p></div>
<div class="m2"><p>هر ساعتی به دیدهٔ عارت نگه کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اغیارات ار نگه نکند هیچ باک نیست</p></div>
<div class="m2"><p>چون اوحدی بکوش، که یارت نگه کند</p></div></div>