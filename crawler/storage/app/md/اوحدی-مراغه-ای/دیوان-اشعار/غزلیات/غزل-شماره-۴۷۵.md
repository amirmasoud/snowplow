---
title: >-
    غزل شمارهٔ ۴۷۵
---
# غزل شمارهٔ ۴۷۵

<div class="b" id="bn1"><div class="m1"><p>تا دل اندر پیچ آن زلف به تاب انداختم</p></div>
<div class="m2"><p>جان خود در آتش و تن در عذاب انداختم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود زمانی نیست پیش دیدهٔ من راه خواب</p></div>
<div class="m2"><p>بس که این توفان خون در راه خواب انداختم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نپنداری که دیدم تا برفتی روی ماه</p></div>
<div class="m2"><p>یا به مهر دل نظر بر آفتاب انداختم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شتاب عمر می‌ترسد دل من، خویش را</p></div>
<div class="m2"><p>زان بجست و جوی وصل اندر شتاب انداختم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود خود در عشق تو هم سینه ریش و دل کباب</p></div>
<div class="m2"><p>دیگر از هجرت نمکها بر کباب انداختم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکر کردم تا در آتش دیدم این دل را چنین</p></div>
<div class="m2"><p>زانکه می‌پنداشتم کین دل به آب انداختم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نه مرد آن دهانم، با لب شیرین تو</p></div>
<div class="m2"><p>اوحدی را در سؤال و در جواب انداختم</p></div></div>