---
title: >-
    غزل شمارهٔ ۶۵۴
---
# غزل شمارهٔ ۶۵۴

<div class="b" id="bn1"><div class="m1"><p>از بند زلفش پای ما مشکل گشاید بعد ازین</p></div>
<div class="m2"><p>چشمی که بیند غیر او ما را نشاید بعد ازین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را چو با دیدار او پیوند و پیمان تازه شد</p></div>
<div class="m2"><p>در چشم ما جز روی او بازی نماید بعد ازین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود را چو دادیم آگهی از ذوق حلوای لبش</p></div>
<div class="m2"><p>لذت نیابد کام ما، گر شهد خاید بعد ازین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دستگاه چرخ اگر اندوه و محنت کم شود</p></div>
<div class="m2"><p>از پیش ما گو: خرج کن چندان که باید بعد ازین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس فتنه زایید آسمان، در دور چشم مست او</p></div>
<div class="m2"><p>از روزگار بی‌وفا تا خود چه زاید بعد ازین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با زلف آن دلدار چون باد صبا گستاخ شد</p></div>
<div class="m2"><p>یا عنبر افشاند هوا، یا مشک ساید بعد ازین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای یار نیکوکار، تو تدبیر کار خویش کن</p></div>
<div class="m2"><p>کز ما به جز سودای او کاری نیاید بعد ازین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا این زمان گر نطق ما تقصیر کرد اندر سخن</p></div>
<div class="m2"><p>بر یاد آن شیرین دهان شیرین سراید بعد ازین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گو: آزمایش را ببر گردی ز خاک اوحدی</p></div>
<div class="m2"><p>گر در جهان آشفته‌ای عشق آزماید بعد ازین</p></div></div>