---
title: >-
    غزل شمارهٔ ۵۵۹
---
# غزل شمارهٔ ۵۵۹

<div class="b" id="bn1"><div class="m1"><p>جای آن دارد که: من بر دیدها جایت کنم</p></div>
<div class="m2"><p>رایگان باشی اگر، جان در کف پایت کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پسته حیران آید و شکر به تنگ آید ز شرم</p></div>
<div class="m2"><p>چون حدیث پستهٔ تنگ شکر خایت کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه شد فرسوده عقل من ز دست زلف تو</p></div>
<div class="m2"><p>آفرین بر دست زلف عقل فرسایت کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر دل و بر دیدهٔ من گر کنی حکم، ای پسر</p></div>
<div class="m2"><p>دیده را مزدور و دل را کارفرمایت کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خویش را دیوانه سازم، تا بدین صحبت مگر</p></div>
<div class="m2"><p>خلق را در حلقهٔ زلف سمن سایت کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رای رای تست، هر حکمی که می‌خواهی بکن</p></div>
<div class="m2"><p>چون مرا روی تو باید، خدمت رایت کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی گر دل به دست چشم مستت داد، من</p></div>
<div class="m2"><p>جان فدای حسن روی عالم آرایت کنم</p></div></div>