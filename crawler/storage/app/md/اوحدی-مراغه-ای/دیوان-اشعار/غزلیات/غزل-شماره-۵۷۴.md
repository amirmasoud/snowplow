---
title: >-
    غزل شمارهٔ ۵۷۴
---
# غزل شمارهٔ ۵۷۴

<div class="b" id="bn1"><div class="m1"><p>به پیشگاه قبول ار چه کم دهد راهم</p></div>
<div class="m2"><p>هنوز دولت آن آستانه می‌خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرم کند ز جفا همچو ریسمان باریک</p></div>
<div class="m2"><p>از آنچه هست سر سوزنی نمی‌کاهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم ز مهر رخش نیم ذره کم نکند</p></div>
<div class="m2"><p>اگر ز طیره کند همچو سایه در چاهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر به آب وصالش طمع کند غیری</p></div>
<div class="m2"><p>من آن طمع نپسندم، که خاک درگاهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آه سینهٔ من دشمنان ببخشیدند</p></div>
<div class="m2"><p>به گوش دوست، همانا، نمی‌رسد آهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر او به کار من خسته التفات کند</p></div>
<div class="m2"><p>چه التفات نماید به دولت و جاهم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به طوع حلقهٔ مهرش کشیده‌ام در گوش</p></div>
<div class="m2"><p>حسود بین که: جدا می‌کند به اکراهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر تو عزم سفر داری، اوحدی، امروز</p></div>
<div class="m2"><p>مرا بهل، که گرفتار مهر آن ماهم</p></div></div>