---
title: >-
    غزل شمارهٔ ۱۵۰
---
# غزل شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>چندان نظر تمام، که دل نقش او گرفت</p></div>
<div class="m2"><p>از وی نظر بدوز چو دل را فرو گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون رو، ای خیال پراکنده، از دلم</p></div>
<div class="m2"><p>از دیگری مگوی، که این خانه او گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای پیرخرقه،یک نفس این دلق سینه‌پوش</p></div>
<div class="m2"><p>بر کن ز من، که آتش غم در کو گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانا، تو بر شکست دل ما مگیر عیب</p></div>
<div class="m2"><p>چون سنگ می‌زنی، نبود بر سبو گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویی که ناقه ختنی را گره گشود</p></div>
<div class="m2"><p>باد صبا، که از سر زلف تو برگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سگ باشد ار به صحبت سلطان رضا دهد</p></div>
<div class="m2"><p>آشفته‌ای که با سگ آن کوی خو گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل را ز اشتیاق تو،ای سرو ماهرخ</p></div>
<div class="m2"><p>خون رگ برگ فروشد و غم تو به تو گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر زخم بد، که هست، برین سینه می‌زنی</p></div>
<div class="m2"><p>عشق تو، راستی، دل ما را نکو گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک شربت آب وصل فرو کن به حلق دل</p></div>
<div class="m2"><p>کو را دگر نوالهٔ غم در گلو گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در صد هزار بند بماند چو موی تو</p></div>
<div class="m2"><p>آن خسته را که دست خیال تو مو گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوشی به اوحدی کن و چشمی برو گمار</p></div>
<div class="m2"><p>کافاق را به نقش تو در گفت و گو گرفت</p></div></div>