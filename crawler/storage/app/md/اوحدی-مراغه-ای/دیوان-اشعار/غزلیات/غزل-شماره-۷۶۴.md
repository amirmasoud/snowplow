---
title: >-
    غزل شمارهٔ ۷۶۴
---
# غزل شمارهٔ ۷۶۴

<div class="b" id="bn1"><div class="m1"><p>کدامین نقشبند این نقش بستی؟</p></div>
<div class="m2"><p>همه یک دست و هر نقشی به دستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نور جان شدست این نقش ممتاز</p></div>
<div class="m2"><p>و گرنه کی چنین در دل نشستی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر این جان در بت سنگین بدیدی</p></div>
<div class="m2"><p>عجب دارم خلیل ار بت شکستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ورین معنی بتی را جمع بودی</p></div>
<div class="m2"><p>کدامین مؤمن از بت باز رستی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا، تا هر دم از دستی بر آییم</p></div>
<div class="m2"><p>مگر نقاش این آید به دستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که گر پا بستهٔ این نقش گردیم</p></div>
<div class="m2"><p>چه فرق از مؤمنی تا بت‌پرستی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهاد اندر لب شیرین این قوم</p></div>
<div class="m2"><p>میی روشن، که هر جامی و مستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پریشان کرد گرد روی ایشان</p></div>
<div class="m2"><p>سر زلفی که هر تاری و شستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مسلمان، اوحدی، آن روز بودی</p></div>
<div class="m2"><p>که از دام چنین بتها برستی</p></div></div>