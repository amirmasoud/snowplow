---
title: >-
    غزل شمارهٔ ۵۷۳
---
# غزل شمارهٔ ۵۷۳

<div class="b" id="bn1"><div class="m1"><p>مشتاق یارم و به در یار می‌روم</p></div>
<div class="m2"><p>دلدارم اوست، در پی دلدار می‌روم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بینم آفتاب رخ او ز روزنی</p></div>
<div class="m2"><p>مانند سایه بر در و دیوار می‌روم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او در میان دایرهٔ خانه نقطه‌وار</p></div>
<div class="m2"><p>من گرد خط کوچه چو پرگار می‌روم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدبار چون خلیل مرا سوختند وباز</p></div>
<div class="m2"><p>همچون کلیم در پی دیدار می‌روم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوشم نشان دوست به بازار داده‌اند</p></div>
<div class="m2"><p>عیبم مکن که بر سر بازار می‌روم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با یادش ار برهنه به خارم برآورند</p></div>
<div class="m2"><p>گویی که: بر حریر، نه بر خار می‌روم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با صوفیان صومعه احوال من بگوی</p></div>
<div class="m2"><p>کز خانقاه بر در خمار می‌روم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گردنم حمایل تسبیح برگشای</p></div>
<div class="m2"><p>امشب که من به بستن زنار می‌روم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی: دلیل چیست که خود شربتی نساخت؟</p></div>
<div class="m2"><p>از پیش این طبیب، که بیمار می‌روم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیچاره شد ز چارهٔ کار من اوحدی</p></div>
<div class="m2"><p>زانش وداع کردم و ناچار می‌روم</p></div></div>