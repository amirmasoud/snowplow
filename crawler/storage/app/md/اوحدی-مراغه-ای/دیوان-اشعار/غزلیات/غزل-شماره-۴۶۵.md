---
title: >-
    غزل شمارهٔ ۴۶۵
---
# غزل شمارهٔ ۴۶۵

<div class="b" id="bn1"><div class="m1"><p>مستم از بادهٔ مهر تو، مرا مست مهل</p></div>
<div class="m2"><p>رفتم از دست، دمی دست من از دست مهل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ز شوق می لعل توچو خون شد مپسند</p></div>
<div class="m2"><p>پشتم از بار غم هجر تو بشکست، مهل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز می‌بینم همدست رقیبان شده‌ای</p></div>
<div class="m2"><p>گر از آن دست نه‌ای کارم ازین دست مهل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نداری گل وصلم، به کفم خار جفا</p></div>
<div class="m2"><p>گر غم هجر تو اندر جگرم خست، مهل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر خدنگی زند آن غمزهٔ جادو مگذار</p></div>
<div class="m2"><p>ور خطایی کند آن نرگس سرمست مهل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل به نزد تو فرستادم و گفتی: بس نیست</p></div>
<div class="m2"><p>اوحدی را ز جفا همچو زمین پست مهل</p></div></div>