---
title: >-
    غزل شمارهٔ ۳۴۲
---
# غزل شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>عشق همان به که به زاری بود</p></div>
<div class="m2"><p>عزت عشق از در خواری بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست بگیرد دل درویش را</p></div>
<div class="m2"><p>دوست که در مهد و عماری بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم نکند صید چنان آهویی</p></div>
<div class="m2"><p>گر سگ ما شیر شکاری بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گل و باغش نبود چاره‌ای</p></div>
<div class="m2"><p>دیده که چون ابر بهاری بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار مرا می‌کشد از عشق خود</p></div>
<div class="m2"><p>کشتن عشاق چه یاری بود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز که بی‌وصل بر آید ز کوه</p></div>
<div class="m2"><p>در نظر من شب تاری بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم بکند چارهٔ او اوحدی</p></div>
<div class="m2"><p>چون شب رندی و سواری بود</p></div></div>