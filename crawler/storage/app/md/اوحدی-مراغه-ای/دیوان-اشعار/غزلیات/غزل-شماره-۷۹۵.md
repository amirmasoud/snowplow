---
title: >-
    غزل شمارهٔ ۷۹۵
---
# غزل شمارهٔ ۷۹۵

<div class="b" id="bn1"><div class="m1"><p>او شوی چو خود را تو از میانه بر گیری</p></div>
<div class="m2"><p>در بها بیفزایی، تا بهانه بر گیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگ و شانه‌ای باید تا ز پا و سر گویی</p></div>
<div class="m2"><p>پا و سر چو گم گردد سنگ و شانه برگیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر مقیم درگاهی خاک شو، که در ساعت</p></div>
<div class="m2"><p>گردنت زند گر سر ز آستانه برگیری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دام شرک را دانه جز تو کس نمی‌بینم</p></div>
<div class="m2"><p>گر ز دام در خوفی دم ز دانه برگیری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سلوک این منهج گر به صدق می‌کوشی</p></div>
<div class="m2"><p>تا ز راه دربندی دل ز خانه برگیری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چوما نه بی‌برگی ساغری بیاشامی</p></div>
<div class="m2"><p>هم چمان برون آیی، هم چمانه برگیری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی، خطا باشد قول جز درین پرده</p></div>
<div class="m2"><p>گر صواب می‌جویی این ترانه برگیری</p></div></div>