---
title: >-
    غزل شمارهٔ ۵۵۶
---
# غزل شمارهٔ ۵۵۶

<div class="b" id="bn1"><div class="m1"><p>گر شبی چارهٔ این درد جدایی بکنم</p></div>
<div class="m2"><p>از شب طرهٔ او روز نمایی بکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور به دست آورم از شام دو زلفش گرهی</p></div>
<div class="m2"><p>تا سحر بر رخ او غالیه سایی بکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرزنش می‌کندم عقل که: در عشق مپیچ</p></div>
<div class="m2"><p>بروم چارهٔ این عقل ریایی بکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از برای سخن عقل خطایی باشد</p></div>
<div class="m2"><p>که به ترک رخ آن ترک ختایی بکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مسخر شود آن روی چو خورشید مرا</p></div>
<div class="m2"><p>پادشاهی چه؟ که دعوی خدایی بکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه باشد، ز دل و دانش و دین، گر خواهد</p></div>
<div class="m2"><p>بدهم و آنچه مرا نیست گدایی بکنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جدایی شدم آشفتهٔ و اندر همه شهر</p></div>
<div class="m2"><p>مددی نیست که تدبیر جدایی بکنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبر گویند: بکن، صبر به دل شاید کرد</p></div>
<div class="m2"><p>چون مرا نیست دلی، صبر کجایی بکنم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوحدی وار اگر آن زلف دو تا بگذارد</p></div>
<div class="m2"><p>زود یکتا شوم و ترک دوتایی بکنم</p></div></div>