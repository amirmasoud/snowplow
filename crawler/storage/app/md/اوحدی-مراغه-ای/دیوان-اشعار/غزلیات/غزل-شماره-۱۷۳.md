---
title: >-
    غزل شمارهٔ ۱۷۳
---
# غزل شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>چون بگذری دلم به تپیدن در اوفتد</p></div>
<div class="m2"><p>دستم ز غم به جامه دریدن در اوفتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر پرتوی ز روی تو افتد بر آسمان</p></div>
<div class="m2"><p>ماهش چو مشتری به خریدن در اوفتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور قامتت به باغ درآید، ز شرم او</p></div>
<div class="m2"><p>حالی به قد سرو خمیدن در اوفتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرواز مرغ جان نبود جز به کوی تو</p></div>
<div class="m2"><p>روزی که اتفاق پریدن در اوفتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان کمترین نثار تو باشد ز دست ما</p></div>
<div class="m2"><p>آن ساعتی که فرصت دیدن در اوفتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانم که: بر حکایت من رحمت آوری</p></div>
<div class="m2"><p>وقتی گرت مجال شنیدن در اوفتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلوت نشین خیال تو گر در دل آورد</p></div>
<div class="m2"><p>چون اوحدی به کوچه دویدن در اوفتد</p></div></div>