---
title: >-
    غزل شمارهٔ ۱۲۶
---
# غزل شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>ای مدعی، دلت گر ازین باده مست نیست</p></div>
<div class="m2"><p>در عیب ما مرو، که ترا حق به دست نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگشای دست و جان و دلت را بیه اد دوست</p></div>
<div class="m2"><p>ایثار کن روان، که درین راه پست نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با محتسب بگوی که: از قاضیان شهر</p></div>
<div class="m2"><p>رو، عذر ما بخواه، که او نیز مست نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا صوفیان به بادهٔ صافی رسیده‌اند</p></div>
<div class="m2"><p>در خانقاه جز دو سه دردی پرست نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من عاشقم، مرا به ملامت خجل مکن</p></div>
<div class="m2"><p>کز عشق، تا اجل نرسد، بازرست نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مهر او چو ذره هوا گیر شو بلند</p></div>
<div class="m2"><p>کین ره به پای سایه نشینان پست نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کس که نیست گشت به هستی رسید زود</p></div>
<div class="m2"><p>وآنکس که او گمان برد آنجا که هست نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک ذره نیست در دل مجروح اوحدی</p></div>
<div class="m2"><p>کز ضرب تیر عشق برو صد شکست نیست</p></div></div>