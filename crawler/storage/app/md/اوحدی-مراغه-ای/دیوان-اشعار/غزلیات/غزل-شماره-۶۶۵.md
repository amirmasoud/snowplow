---
title: >-
    غزل شمارهٔ ۶۶۵
---
# غزل شمارهٔ ۶۶۵

<div class="b" id="bn1"><div class="m1"><p>تو سروی ، بر نشاید چیدن از تو</p></div>
<div class="m2"><p>تو ماهی، مهر نتوان دیدن از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آشفته دل را تا کی آخر</p></div>
<div class="m2"><p>میان خاک و خون غلتیدن از تو؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گردان رخصت خونم به عالم</p></div>
<div class="m2"><p>که رخصت نیست برگردیدن از تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرم صد آستین بر رخ فشانی</p></div>
<div class="m2"><p>نخواهم دامن اندر چیدن از تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا چون هیچ ترسی از خدا نیست</p></div>
<div class="m2"><p>همی باید مرا ترسیدن از تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گناهم نیست اندر عشق و گر هست</p></div>
<div class="m2"><p>گناه از بنده و بخشیدن از تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر صد رنج باشد اوحدی را</p></div>
<div class="m2"><p>شفا یابد به یک پرسیدن از تو</p></div></div>