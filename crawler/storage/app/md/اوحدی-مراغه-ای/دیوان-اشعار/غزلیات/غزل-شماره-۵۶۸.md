---
title: >-
    غزل شمارهٔ ۵۶۸
---
# غزل شمارهٔ ۵۶۸

<div class="b" id="bn1"><div class="m1"><p>درد تو برآورد ز دنیا و ز دینم</p></div>
<div class="m2"><p>با مایهٔ عشق تو آن نه باد و نه اینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم همه آفاق به دیدار تو بینند</p></div>
<div class="m2"><p>تا پردهٔ ز رخ برنکنی هیچ نبینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تحصیل تو مقدور و من آسوده روا نیست</p></div>
<div class="m2"><p>از خرمن اقبال چرا خوشه نچینم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندیشهٔ مستوری و دین داشتنم بود</p></div>
<div class="m2"><p>سودای تو نگذاشت که مستور نشینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گنج وصالت به سعادت برسد زود</p></div>
<div class="m2"><p>گر خاتم لعل تو شود ملک نگینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ماه تشبه به رخت کرد ز خوبی</p></div>
<div class="m2"><p>با ماه به پیکارم و با مهر به کینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نور تو در خلق نبینم ز دو گیتی</p></div>
<div class="m2"><p>هم گوش فروبندم و هم گوشه نشینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پایی به کرم بررخ من نیز همی نه</p></div>
<div class="m2"><p>کندر سر کویت نه کم از خاک زمینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون اوحدی از وصل به شاهی برسم زود</p></div>
<div class="m2"><p>گر خاتم لعل توشود ملک یمینم</p></div></div>