---
title: >-
    غزل شمارهٔ ۶۲۴
---
# غزل شمارهٔ ۶۲۴

<div class="b" id="bn1"><div class="m1"><p>از تو مرا تا به کی بی‌سر و سامان شدن؟</p></div>
<div class="m2"><p>در طلب وصل تو زار و پریشان شدن؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نفسم خون دل ریزی و گویی: مگوی</p></div>
<div class="m2"><p>واقعه‌ای مشکلست: دیدن و نادان شدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ز تو درمان دل جستم و دشمن شدی</p></div>
<div class="m2"><p>مصلحت من نبود در پی درمان شدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف تو در بند آن هست که: شادم کند</p></div>
<div class="m2"><p>گر نزند روی تو رای پشیمان شدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی ترا عادتست، زلف ترا قاعده</p></div>
<div class="m2"><p>دل بربودن ز من هر دم و پنهان شدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه تو خواهی بکن، زانکه نه کار منست</p></div>
<div class="m2"><p>با چو تو مسکین کشی دست و گریبان شدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلق به دیر و به زود راه به پایان برند</p></div>
<div class="m2"><p>رای ترا هیچ نیست راه به پایان شدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر دل ویران من طعنه زدن تا به چند؟</p></div>
<div class="m2"><p>بین که: چه گنجی دروست با همه ویران شدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار تو پیمان شکن نیست به جز سرکشی</p></div>
<div class="m2"><p>کار دل اوحدی بر سر پیمان شدن</p></div></div>