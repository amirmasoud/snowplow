---
title: >-
    غزل شمارهٔ ۷۵۳
---
# غزل شمارهٔ ۷۵۳

<div class="b" id="bn1"><div class="m1"><p>در کعبه گر ز دوست نبودی نشانه‌ای</p></div>
<div class="m2"><p>حاجی چه التفات نمودی به خانه‌ای؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغان آن هوا به زمین چون کنند میل؟</p></div>
<div class="m2"><p>تا در میان دام نبینند دانه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بویی ز وصل اگر به مشامش نمی‌رسید</p></div>
<div class="m2"><p>رغبت به هیچ موی نمی‌کرد شانه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این کوشش و کشش همه بی‌کار چون بود؟</p></div>
<div class="m2"><p>عاقل چگونه دل بنهد بر فسانه‌ای؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا عشق آتشی نزند در درون دل</p></div>
<div class="m2"><p>از راه سینه کی بدر افتد زبانه‌ای؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محتاج پیک و نامه نباشد مرید را</p></div>
<div class="m2"><p>کانجا کفایتست سر تازیانه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیز، ای رفیق خفته، که صوت نشیدخوان</p></div>
<div class="m2"><p>آتش فگند در شتران از ترانه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ثابت نباشد آن قدم اندر طریق عشق</p></div>
<div class="m2"><p>کو می‌کند ز خار مغیلان کرانه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر راستست، هر چه طلب می‌کنم تویی</p></div>
<div class="m2"><p>وین راه دور نیست بغیر از بهانه‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با اوحدی یکی شو و مشنو که: در وجود</p></div>
<div class="m2"><p>هرگز در آن یگانه رسد جز یگانه‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما را اگر محال نباشد به پیشگاه</p></div>
<div class="m2"><p>این فخر بس که: بوسه دهیم آستانه‌ای</p></div></div>