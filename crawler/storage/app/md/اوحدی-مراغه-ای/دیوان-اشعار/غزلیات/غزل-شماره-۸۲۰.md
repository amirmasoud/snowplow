---
title: >-
    غزل شمارهٔ ۸۲۰
---
# غزل شمارهٔ ۸۲۰

<div class="b" id="bn1"><div class="m1"><p>سرم بی‌دولتست، ار نه ز پایت کی شدی خالی؟</p></div>
<div class="m2"><p>که حور نرگسین چشمی و ماه عنبرین خالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشا چشمی که روز و شب تواند دید روی تو</p></div>
<div class="m2"><p>که میمون طالع و بخت و همایون طلعت و فالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نجستم هیچ ازین دنیا بغیر از دیدن رویت</p></div>
<div class="m2"><p>بهیچم بر نمیگیری ز درویشی و بی‌مالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخواهد بود تا هستم دل من بی‌ولای تو</p></div>
<div class="m2"><p>اگر خنجر کشد سلطان و گر ناوک زند والی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا بر گریهای من مپندارم که دل سوزد</p></div>
<div class="m2"><p>که همچون گل همی خندی و همچون سرو می‌بالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدین حسن ار شبی تنها به دست زاهدی افتی</p></div>
<div class="m2"><p>بزورت بوسه بستاند، اگر خود رستم زالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون من زلف ترا گفتم که: وقتی مالشی میده</p></div>
<div class="m2"><p>نهادی زلف را بر گوش و گوش من همی مالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پریشانی مکن با ما چو زلف خویشتن چندین</p></div>
<div class="m2"><p>که من خود بیتو میسوزم ز مسکینی و بد حالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخواهد بود تحصیلی مرا بی‌روز وصل تو</p></div>
<div class="m2"><p>اگر پیشت فروخوانم تمامت علم غزالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بب دیده می‌گریم ز دستان تو هر ساعت</p></div>
<div class="m2"><p>که آتش میزینی در جان و می‌گویی: چه مینالی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان پر شرح تست و نام اوحدی، لیکن</p></div>
<div class="m2"><p>عجب دارم که نام او رود در مجلس عالی!</p></div></div>