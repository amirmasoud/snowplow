---
title: >-
    غزل شمارهٔ ۱۳۹
---
# غزل شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>نگر: مگرد گر آن سر و سیم بر بگذشت؟</p></div>
<div class="m2"><p>که: آب دیدهٔ نظارگان ز سر بگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز من چو زان رخ همچون قمر نشان پرسید</p></div>
<div class="m2"><p>رسید بر فلکم آه و از قمر بگذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بخت بین که: نخفتم شبی جزین ساعت</p></div>
<div class="m2"><p>که خفته بودم و دولت ز پیش در بگذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدام پرده بماند درست و پوشیده؟</p></div>
<div class="m2"><p>بدین طریق که آن ترک پرده در بگذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر به پند پدر گوش برنکرد کسی</p></div>
<div class="m2"><p>که از مقابل او روی آن پسر بگذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسافری، که به شهر آمد و بدید او را</p></div>
<div class="m2"><p>ندیده‌ایم کز آن آستان در بگذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دید آن سر زلف دراز در کمرش</p></div>
<div class="m2"><p>سرشک دیدهٔ خونریزم از کمر بگذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز من بپرس گزند جراحت دل ریش</p></div>
<div class="m2"><p>که چند نوبتم این ناوک از جگر بگذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو اوحدی نشدش دل به هیچ نوع درست</p></div>
<div class="m2"><p>هر آن شکسته که این تیرش از سپر بگذشت</p></div></div>