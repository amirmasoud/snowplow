---
title: >-
    غزل شمارهٔ ۲۲۴
---
# غزل شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>تا رسم جگرخواری پیش تو روا باشد</p></div>
<div class="m2"><p>عشق ترا مشکل کاری به نوا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمشماد همی لرزد چون بید ز بالایت</p></div>
<div class="m2"><p>بالای چنین، رعنا در شهر بلا باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین‌سان که گریبانم بگرفت غم عشقت</p></div>
<div class="m2"><p>این خرقه که می‌بینی یک روز قبا باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من میکنم آن طاعت کز بنده سزد، لیکن</p></div>
<div class="m2"><p>شرطست که نگریزی، گر روز جزا باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلقی ز پیت پویان، مهر تو به جان جویان</p></div>
<div class="m2"><p>زین جمله دعا گویان تا بخت کرا باشد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب غم عشق تو بگذشت ز سر ما را</p></div>
<div class="m2"><p>وآنگه تو ز بی‌رحمی بگذاشته تا: باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لعلت نکند سعیی در چارهٔ کار من</p></div>
<div class="m2"><p>بیچاره کسی کو را کاری به شما باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم را که بها نبود در شهر کسان هرگز</p></div>
<div class="m2"><p>آن روز که من جویم شهریش بها باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر خوب شود کاری، از طلعت خود گیری</p></div>
<div class="m2"><p>ور زشت شود، تاوان بر طالع ما باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتی که: روا گردد از من همه حاجت‌ها</p></div>
<div class="m2"><p>مرد اوحدی از عشقت، آخر چه روا باشد؟</p></div></div>