---
title: >-
    غزل شمارهٔ ۶۰
---
# غزل شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>مهر گسل گشت یار، عهد شکن شد حبیب</p></div>
<div class="m2"><p>اصل خطر شد دوا، رای خطا زد صلیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوارم و بی‌وصل دوست خوار بود آدمی</p></div>
<div class="m2"><p>زارم و بی‌روی گل زار بود عندلیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیر کشید، ای نگار، سوختنم ز انتظار</p></div>
<div class="m2"><p>یا نظری بی‌ستیز، یا گذری بی‌رقیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما ز تو مهر و وفا خواسته‌ایم، ای صنم</p></div>
<div class="m2"><p>نی چو کسان دگر عاشق رنگیم و طیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست ز خامان عجب عشق زنخدان و لب</p></div>
<div class="m2"><p>طبع چه جوید؟رطب، طفل چه جوید زبیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابروی محراب‌وش گر سوی مسجد بری</p></div>
<div class="m2"><p>نعره برآرد امام، در غلط افتد خطیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بکشم خویش را در طلب وصل تو</p></div>
<div class="m2"><p>سود ندارد، که نیست کار برون از نصیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چاره به جز صبر نیست، کان رخ چون آفتاب</p></div>
<div class="m2"><p>دل برباید، مگر دیده بدوزد لبیب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل‌منه، ای اوحدی، زانکه به شهر کسان</p></div>
<div class="m2"><p>جور کشد بی‌سخن عاشق و آنگه غریب</p></div></div>