---
title: >-
    غزل شمارهٔ ۱۳۴
---
# غزل شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>عشرت خلوت و دیدار عزیزان شاهیست</p></div>
<div class="m2"><p>وین نداند، مگر آن دل که درو آگاهیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شناسد که: چه بر یوسف مسکین آمد</p></div>
<div class="m2"><p>از غم روی زلیخا؟ که چو یوسف چاهیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست کوته مکن از باده و باقی مگذار</p></div>
<div class="m2"><p>چیزی از عشق، که در روز بقا کوتاهیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم از هر دو جهان روی تو می‌خواهد و این</p></div>
<div class="m2"><p>چون ببینی تو، هم از غایت نیکو خواهیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو آهو بره را سر به کمند آوردیم</p></div>
<div class="m2"><p>پیش ما شیر فلک را هوس روباهیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطرب، امشب همه آوازهٔ خرگاهی زن</p></div>
<div class="m2"><p>اندرین خیمه، که معشوقهٔ ما خرگاهیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتنهٔ روی خود، ای ماه و دل سوختگان</p></div>
<div class="m2"><p>ز اوحدی پرس، که در شست تو همچون ماهیست</p></div></div>