---
title: >-
    غزل شمارهٔ ۷۸
---
# غزل شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>آمد نسیم گل به دمیدن ز چپ و راست</p></div>
<div class="m2"><p>ساقی، می شبانه بیاور، که روز ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در باغ شد شکفته به هر جانبی گلی</p></div>
<div class="m2"><p>فریاد عندلیب ز هر جانبی بخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا پیش شاخ گل ننشینی، قدح به دست</p></div>
<div class="m2"><p>آشوب بلبلان بندانی که: از کجاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دم بنفشه‌وار فرو می‌روم به خود</p></div>
<div class="m2"><p>از فکر جام لاله که: خالی ز می چراست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهد، بسوز عود، که خواهیم عیش کرد</p></div>
<div class="m2"><p>مطرب، بساز عود، که خواهیم عذر خواست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز عشق هر هوس که پزی زین سپس، هدر</p></div>
<div class="m2"><p>جز عیش هر عمل که کنی بعد ازین، هباست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من عمر خود به عمر گل اندر فزودمی</p></div>
<div class="m2"><p>گر راه بودمی به سر این فزود و کاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون گل کلاه‌داری خود ترک می‌کند</p></div>
<div class="m2"><p>بر ما عجب نباشد اگر پیرهن قباست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای نو رسیده سبزه، که آبت ز سر گذشت</p></div>
<div class="m2"><p>گر سرگذشت خویش ز ما بشنوی رواست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا ما قفای گل بنبینیم چون هلیم</p></div>
<div class="m2"><p>دست از می؟ ارچه سرزنش خلق در قفاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز یاد بید و سرو مکن پیش اوحدی</p></div>
<div class="m2"><p>کو نشنود به وقت گل الا حدیث راست</p></div></div>