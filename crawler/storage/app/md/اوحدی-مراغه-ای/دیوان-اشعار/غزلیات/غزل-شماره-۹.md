---
title: >-
    غزل شمارهٔ ۹
---
# غزل شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>مبارک روز بود امروز، یارا</p></div>
<div class="m2"><p>که دیدار تو روزی گشت ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آن دوزخ دلم، یارب، که دیدم</p></div>
<div class="m2"><p>به چشم خود بهشت آشکارا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه مهرست این، که داغ دولتست این</p></div>
<div class="m2"><p>که بر دل بر ز دست این بی‌نوا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز یک نا گه چه گنج دولتست این؟</p></div>
<div class="m2"><p>که در دست اوفتاد این بی‌نوا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین حالت که من روی تو دیدم</p></div>
<div class="m2"><p>عنایت‌هاست با حالم خدا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم آه آتشینم کارگر بود</p></div>
<div class="m2"><p>که شد نرم آن دل چون سنگ خارا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا تشریف یک پرسیدنت به</p></div>
<div class="m2"><p>ز تخت کیقباد و تاج دارا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بکش زود اوحدی را، پس جدا شو</p></div>
<div class="m2"><p>که بی‌رویت نمی‌خواهد بقا را</p></div></div>