---
title: >-
    غزل شمارهٔ ۴۸
---
# غزل شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>ای سفر کرده، دلم بی‌تو بفرسود،بیا</p></div>
<div class="m2"><p>غمت از خاک درت بیشترم سود، بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سود من جمله ز هجر تو زیان خواهد شد</p></div>
<div class="m2"><p>گر زیانست درین آمدن از سود، بیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مایهٔ راحت و آسایش دل بودی تو</p></div>
<div class="m2"><p>تا برفتی تو دلم هیچ نیاسود بیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز اشتیاق تو در افتاد به جانم آتش</p></div>
<div class="m2"><p>وز فراق تو در آمد به سرم دود، بیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ریختم در طلبت هر چه دلم داشت، مرو</p></div>
<div class="m2"><p>باختم در هوست هر چه مرا بود، بیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز بهر دل دشمن نکنی چارهٔ من</p></div>
<div class="m2"><p>دشمنم بر دل بیچاره ببخشود، بیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زود برگشتی و دیر آمده بودی به کفم</p></div>
<div class="m2"><p>دیر گشت آمدنت، دیر مکش، زود بیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کم شود مهر ز دوری دگران را لیکن</p></div>
<div class="m2"><p>کم نشد مهر من از دوری و افزود، بیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بپالودن خون دل من داری میل</p></div>
<div class="m2"><p>اوحدی خون دل از دیده بپالود، بیا</p></div></div>