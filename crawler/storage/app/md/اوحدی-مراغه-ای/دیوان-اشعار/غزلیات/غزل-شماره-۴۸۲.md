---
title: >-
    غزل شمارهٔ ۴۸۲
---
# غزل شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>چو بر سفینهٔ دل نقش صورت تو نبشتم</p></div>
<div class="m2"><p>حکایت دگران سر به سر زیاد بهشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه نام مرا دور کرده‌ای تو ز دفتر</p></div>
<div class="m2"><p>به نام روی تو صد دفتر نیاز نبشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شاخ وصل تو دستم نداد میوهٔ‌شیرین</p></div>
<div class="m2"><p>مگر که دانهٔ این میوه تلخ بود، که کشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه موی شکافی همی کنم ز معانی</p></div>
<div class="m2"><p>به اعتماد تو یکسر پلاس بود،که رشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خاک پای تو کز دامن تو دست ندارم</p></div>
<div class="m2"><p>و گر ز قالب پوسیده کوزه سازی و خشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر تو روی نخواهی نمود روز قیامت</p></div>
<div class="m2"><p>به دوزخم بر ازین ره، که من نه مرد بهشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرشک دیده چنان ریخت اوحدی ز فراقت</p></div>
<div class="m2"><p>کز آب دیدهٔ او خاک ره به خون بسرشتم</p></div></div>