---
title: >-
    غزل شمارهٔ ۶۷۷
---
# غزل شمارهٔ ۶۷۷

<div class="b" id="bn1"><div class="m1"><p>ای مدد تیره شب از موی تو</p></div>
<div class="m2"><p>روز مرا روشنی از روی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر آنم که: شوم یک سحر</p></div>
<div class="m2"><p>خاک نسیمی که دهد بوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک شوم، تا مگر آرد مرا</p></div>
<div class="m2"><p>باد محبت به سر کوی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز به گوش تو رساند مگر</p></div>
<div class="m2"><p>قصهٔ ما حاجب ابروی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برمکن از من به جفا دل، که من</p></div>
<div class="m2"><p>برنکنم خیمه ز پهلوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قیمت وصل تو که داند که: چیست؟</p></div>
<div class="m2"><p>هر دو جهان می‌نه و یک موی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف تو در حلق دل اوحدیست</p></div>
<div class="m2"><p>چون نکشد خاطر او سوی تو؟</p></div></div>