---
title: >-
    غزل شمارهٔ ۱۰۵
---
# غزل شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>روی تو، که قبلهٔ جهانست</p></div>
<div class="m2"><p>از دیدهٔ من چرا نهانست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی به جز از درت ندارم</p></div>
<div class="m2"><p>گر درنگری، بجای آنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل زده‌ای تو آتش عشق</p></div>
<div class="m2"><p>وین آه، که می‌زنم، دخانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل یاد تو در ضمیر دارد</p></div>
<div class="m2"><p>آن نیست که بر سر زبانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این سر، که به عاشقی سبک شد</p></div>
<div class="m2"><p>بی‌روی تو بر تنم گرانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصل تو بدین ودل خریدم</p></div>
<div class="m2"><p>گر سود کنیم و گر زیانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک بوسه اگر به جان فروشی</p></div>
<div class="m2"><p>منت می‌نه، که رایگانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با من تن لاغر و دل تنگ</p></div>
<div class="m2"><p>از عشق تو کمترین نشانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مار را ز غم تو اوحدی وار</p></div>
<div class="m2"><p>جان بر کف و خرقه در میانست</p></div></div>