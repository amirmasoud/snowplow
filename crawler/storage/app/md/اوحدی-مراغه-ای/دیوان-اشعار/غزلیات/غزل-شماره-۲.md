---
title: >-
    غزل شمارهٔ ۲
---
# غزل شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>سلام علیک، ای نسیم صبا</p></div>
<div class="m2"><p>به لطف از کجا می‌رسی؟ مرحبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشانی ز بلقیس، اگر کرده‌ای</p></div>
<div class="m2"><p>چو مرغ سلیمان گذر بر سبا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسیمی بیاور ز پیراهنش</p></div>
<div class="m2"><p>که شد پیرهن بر وجودم قبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر یابم از بوی زلفش خبر</p></div>
<div class="m2"><p>نیابد وجودم گزند از وبا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نزدیک آن دلربا گفتنیست</p></div>
<div class="m2"><p>که ما را کدر کرد سیل از ربا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دردش ببین این سرشک چو لعل</p></div>
<div class="m2"><p>روانم برین روی چون کهربا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همین حاصل است اوحدی را ز عشق</p></div>
<div class="m2"><p>که خونم هدر کرد و مالم هبا</p></div></div>