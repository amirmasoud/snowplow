---
title: >-
    غزل شمارهٔ ۱۲۹
---
# غزل شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>هم خانه‌ایم، روی گرفتن حلال نیست</p></div>
<div class="m2"><p>ناگفته پرسشی، که سخن را مجال نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی: بسنده کن به خیالی ز وصل ما</p></div>
<div class="m2"><p>ما را بغیر ازین سخنی در خیال نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ماه صورت تو ببیند، به صدق دل</p></div>
<div class="m2"><p>خود معترف شود که: درو این کمال نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پرده‌ای و بر همه کس پرده می‌دری</p></div>
<div class="m2"><p>با هر کسی و با تو کسی را وصال نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشکل در آن که: وصل تو ممکن نمیشود</p></div>
<div class="m2"><p>ورنه به ممکنات رسیدن محال نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لالند عارفان تو از شرح چند و چون</p></div>
<div class="m2"><p>از معرفت خبر نشد آنرا که لال نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرسیده‌ای که: آنچه طلب میکنی کجاست؟</p></div>
<div class="m2"><p>از من خبر مپرس، که جای سال نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای اوحدی، چو این دگران سر دوستی</p></div>
<div class="m2"><p>با دیگری مگوی، که ما را به فال نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر مدعی سماع حدیثت نمی‌کند</p></div>
<div class="m2"><p>دل مرده را سماع نباشد، که حال نیست</p></div></div>