---
title: >-
    غزل شمارهٔ ۵۱۰
---
# غزل شمارهٔ ۵۱۰

<div class="b" id="bn1"><div class="m1"><p>عمریست تا ز دست غمت جامه می‌درم</p></div>
<div class="m2"><p>دستم بگیر، تا مگر از عمر برخورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یادم نمی‌کنی تو به عمر و نمی‌رود</p></div>
<div class="m2"><p>یاد تو از خیال و خیال تو از سرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفت از فراق روی تو عمرم به سر، ولی</p></div>
<div class="m2"><p>پایم نمی‌رود که ز پیش تو بگذرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌بایدم خزینهٔ قارون و عمر نوح</p></div>
<div class="m2"><p>تا دولت وصال تو گردد میسرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون عمر گل دو هفته وفای تو بیش نیست</p></div>
<div class="m2"><p>ای گل، تو این دو هفته مبر سایه از سرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر عزیز و جان گرامی تویی مرا</p></div>
<div class="m2"><p>ای عمر و جان، تو دور چرا باشی از برم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گیتی بسان عمر مرا گو: فرو نورد</p></div>
<div class="m2"><p>گر در بسیط خاک بغیر تو بنگرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمری دگر بباید و شلتاق عالمی</p></div>
<div class="m2"><p>تا گنج غارتی چو تو باز آید از درم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیرین‌تری ز عمر و من اندر فراق تو</p></div>
<div class="m2"><p>فرهادوار محنت و تلخی همی برم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای عمر عاریت، مکن از پیش من کنار</p></div>
<div class="m2"><p>تا در کنار خویش چو جانت بپرورم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر اوحدی به سیم سخن عمر می‌خرد</p></div>
<div class="m2"><p>من عمر می‌فروشم و وصل تو می‌خرم</p></div></div>