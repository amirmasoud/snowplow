---
title: >-
    غزل شمارهٔ ۲۹
---
# غزل شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>آن سیه چهره که خلقی نگرانند او را</p></div>
<div class="m2"><p>خوبرویان جهان بنده به جانند او را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبرانی که به خوبی بنشانند امروز</p></div>
<div class="m2"><p>جای آنست که بر دیده نشانند او را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامنش پاک ز عارست و دلش پاک ز عیب</p></div>
<div class="m2"><p>پاکبازان جهان بنده از آنند او را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر در افتد به کفم دامن وصلش روزی</p></div>
<div class="m2"><p>از کف من به جهانی نجهانند او را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست بی‌مصلحتی از بر او دوری من</p></div>
<div class="m2"><p>برمیدم ز برش، تا نرمانند او را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قیمت قامت او را من بیدل دانم</p></div>
<div class="m2"><p>ورنه این یک دوسه افسرده چه دانند او را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که گشت اوحدی از بهر تو بدنام جهان</p></div>
<div class="m2"><p>بندهٔ تست، نام که خوانند او را</p></div></div>