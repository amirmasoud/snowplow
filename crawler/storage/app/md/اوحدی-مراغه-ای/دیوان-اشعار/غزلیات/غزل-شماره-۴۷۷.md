---
title: >-
    غزل شمارهٔ ۴۷۷
---
# غزل شمارهٔ ۴۷۷

<div class="b" id="bn1"><div class="m1"><p>ای زاهد مستور، ز من دور، که مستم</p></div>
<div class="m2"><p>با توبهٔ خود باش، که من توبه شکستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنار ببندی تو و پس خرقه بپوشی</p></div>
<div class="m2"><p>من خرقهٔ پوشیده به زنار ببستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همتای بت من به جهان هیچ بتی نیست</p></div>
<div class="m2"><p>هر بت که بدین نقش بود من بپرستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فردای قیامت که سر از خاک برآرم</p></div>
<div class="m2"><p>جز خاک در او نبود جای نشستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست من و دامان شما، هر چه ببینید</p></div>
<div class="m2"><p>جز حلقهٔ آن در، بستانید ز دستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر گرد من ار دانه و دامیست عجب نیست</p></div>
<div class="m2"><p>روزی دو، که مرغ قفس و ماهی شستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سر هوس اوست، به هر گوشه که باشم</p></div>
<div class="m2"><p>در دل طرب اوست، به هر گونه که هستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بارم نتوان برد، که مسکین و غریبم</p></div>
<div class="m2"><p>خوارم نتوان کرد که افتاده و پستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باشد سخنم حلقه به گوش همه دل‌ها</p></div>
<div class="m2"><p>چون حلقه به گوش سخن روز الستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پنهان شدم از خلق وز خلق خلق او</p></div>
<div class="m2"><p>خلقم چو بدیدند و بجستند بجستم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوش اوحدی از زهد سخن گفت و من از عشق</p></div>
<div class="m2"><p>القصه، من از غصهٔ او نیز برستم</p></div></div>