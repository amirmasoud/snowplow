---
title: >-
    غزل شمارهٔ ۲۸۴
---
# غزل شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>خانه خالی شد و در کوی دل اغیار نماند</p></div>
<div class="m2"><p>همه غم رفت و ه بغیر از غم آن یار نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه در پای دلم خار جفا بود، دگر</p></div>
<div class="m2"><p>گل به دست آمد و در پای دلم خار نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن گروهی که به آزار دلم کوشیدند</p></div>
<div class="m2"><p>چون برفتند دگر هیچ دلازار نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دشمن از غصهٔ من علت بیماری داشت</p></div>
<div class="m2"><p>دوستان مژده، که آن ناخوش بیمار نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم من بر سر خاک درش از شوق امشب</p></div>
<div class="m2"><p>سیل خونین صفتی ریخت، که دیوار نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناله میکردم و گفت: اوحدی، این روزی دو</p></div>
<div class="m2"><p>قصه بسیار نگوییم، که بسیار نماند</p></div></div>