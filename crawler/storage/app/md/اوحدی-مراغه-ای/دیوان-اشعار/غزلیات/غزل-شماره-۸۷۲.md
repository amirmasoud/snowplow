---
title: >-
    غزل شمارهٔ ۸۷۲
---
# غزل شمارهٔ ۸۷۲

<div class="b" id="bn1"><div class="m1"><p>ای از گل سوری دهنت غنچه نمایی</p></div>
<div class="m2"><p>وی بر سمن از سنبل تر غالیه سایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میدان که: سر ما و نشان قدم تست</p></div>
<div class="m2"><p>در کوی تو هر جا که سری بینی و پایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش این دل من خانهٔ عشق تو همی کند</p></div>
<div class="m2"><p>و امروز دگر باره بنا کرد سرایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌واسطه روزی هوس دیدن ما کن</p></div>
<div class="m2"><p>کندر دل ما جز هوست نیست هوایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک روز به زلف تو در آویزم و رفتم</p></div>
<div class="m2"><p>شک نیست که باشد سر این رشته به جایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی منکر ما را هوس پرده دری بود</p></div>
<div class="m2"><p>پنداشت که بتوان زدن این پرده به تایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن کس که درین واقعه عذرم نپذیرد</p></div>
<div class="m2"><p>بر سینه نخوردست مگر تیر بلایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من گردن تسلیم به شمشیر سپردم</p></div>
<div class="m2"><p>از دوست کجا روی بپیچم به قفایی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان تخم وفا بهره چه معنی که ندیدیم</p></div>
<div class="m2"><p>نیکی و بدی را چو پدیدست جزایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برگشتنت، ای اوحدی، از یار خطا بود</p></div>
<div class="m2"><p>دل بر نتوان داشت ز ترکی به خطایی</p></div></div>