---
title: >-
    غزل شمارهٔ ۲۶۰
---
# غزل شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>جهان از باد نوروزی جوان شد</p></div>
<div class="m2"><p>زمین در سایهٔ سنبل نهان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قیامت می‌کند بلبل سحرگاه</p></div>
<div class="m2"><p>مگر گل فتنهٔ آخر زمان شد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رنگ سبزه و شکل ریاحین</p></div>
<div class="m2"><p>زمین گویی به صورت آسمان شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا در طرهٔ شمشاد پیچید</p></div>
<div class="m2"><p>بنفشه خاک پای ارغوان شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهار آمد، بیا و توبه بشکن</p></div>
<div class="m2"><p>که در وقتی دگر صوفی توان شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز رنگ و بوی گل اطراف بستان</p></div>
<div class="m2"><p>تو پنداری بهشت جاودان شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولیکن اوحدی را برگ گل نیست</p></div>
<div class="m2"><p>که او آشفتهٔ روی فلان شد</p></div></div>