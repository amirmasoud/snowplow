---
title: >-
    غزل شمارهٔ ۲۹۰
---
# غزل شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>باز شادروان گل بر روی خار انداختند</p></div>
<div class="m2"><p>زلف سنبل بر بنا گوش بهار انداختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دختران گل به وقت صبح‌دم در پای سرو</p></div>
<div class="m2"><p>از سر شادی طبقهای نثار انداختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهدان سوسن از بهر تماشا در چمن</p></div>
<div class="m2"><p>لاله را با سنبل اندر کارزار انداختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلبل شیرین سخن شکر فشانی پیشه کرد</p></div>
<div class="m2"><p>تا بساط فستقی بر جویبار انداختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرم تازان صبا از گرد عنبر وقت صبح</p></div>
<div class="m2"><p>موکب سلطان گل را در غبار انداختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچگان را گر چه بر گل پرده پوشی عادتست</p></div>
<div class="m2"><p>عاقبت هم بخیه‌ای بر روی کار انداختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ز مستی در شکوفه است و گل اندر خفت و خیز</p></div>
<div class="m2"><p>نرگس بیچاره را چون در خمار انداختند؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وقت صبح آهنگران باد ز آب پیچ پیچ</p></div>
<div class="m2"><p>بی‌گنه زنجیر بر پای چنار انداختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دماغ بید گویی هم خلافی دیده‌اند</p></div>
<div class="m2"><p>کز میان بوستانش بر کنار انداختند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبزه‌ها را گرچه بر بالای گل دستی بود</p></div>
<div class="m2"><p>هم ز گیسوها کمندش بر حصار انداختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چمن را نیست در سر خاطر سوری دگر</p></div>
<div class="m2"><p>از چه بر دست عروسانش نگار انداختند؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صبح دم بزم چمن گرمست، زیرا کندرو</p></div>
<div class="m2"><p>نالهٔ موسیچه و قمری و سار انداختند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راویان نظم ز اشعار بدیع اوحدی</p></div>
<div class="m2"><p>بار دیگر فتنه‌ای در روزگار انداختند</p></div></div>