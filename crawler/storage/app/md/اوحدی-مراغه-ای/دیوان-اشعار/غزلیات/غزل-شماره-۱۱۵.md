---
title: >-
    غزل شمارهٔ ۱۱۵
---
# غزل شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>دل بسته شد به دام دو زلف چو دال دوست</p></div>
<div class="m2"><p>بر بوی دانها که بدیدم ز خال دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را چه قدر و قیمت و جان چیست؟ کین دو رفت</p></div>
<div class="m2"><p>وندر خجالتیم هنوز از جمال دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانش چگونه تحفه فرستم؟ کزوست جان</p></div>
<div class="m2"><p>کس دوست را چگونه فریبد به مال دوست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مالم به دست نیست، که درپای او کنم</p></div>
<div class="m2"><p>زان زیر دست دشمنم و پایمال دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی‌نی، ز دست تنگی و بیچارگی چه شک؟</p></div>
<div class="m2"><p>نقصان ما چه رنگ دهد با کمال دوست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را مجال بود بروبر، به دوستی</p></div>
<div class="m2"><p>دشمن رها نکرد که باشد مجال دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیگانه را ز راز دل ما چه آگهی؟</p></div>
<div class="m2"><p>با آشنای دوست توان گفت حال دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان سو گذر به جانب من کس نمی‌کند</p></div>
<div class="m2"><p>تا باز پرسمش خبری از مقال دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دانم که: از شکست دل من خجل شود</p></div>
<div class="m2"><p>کو میل خویش عرضه کند بر ملال دوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بختم بخفت و بخت مرا چشم آن نبود</p></div>
<div class="m2"><p>کندر شود به خواب و ببیند خیال دوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن دوست را به هستی ما التفات نیست</p></div>
<div class="m2"><p>تا هست و نیست صرف شود بر سؤال دوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امیدوارم از شب هجران که: عاقبت</p></div>
<div class="m2"><p>شادم کند به دولت صبح وصال دوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اندر دمی دو عید، که گویند، اشارتیست</p></div>
<div class="m2"><p>بر دیدن دو ابروی همچون هلال دوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن ماهرخ به سال مرا وعده می‌دهد</p></div>
<div class="m2"><p>ای من غلام و چاکر آن ماه و سال دوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای اوحدی، مکن طلب او به پای فکر</p></div>
<div class="m2"><p>کندر تصور تو نگنجد جلال دوست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وقتی اگر هوای سر کوی او کنی</p></div>
<div class="m2"><p>گر مرغ زیرکی نپری جز به بال دوست</p></div></div>