---
title: >-
    غزل شمارهٔ ۳۰۸
---
# غزل شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>ترک ستم پرست من ترک جفا نمی‌کند</p></div>
<div class="m2"><p>عهد به سر نمی‌برد، وعده وفا نمی‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هندوی ترک آن صنم کرد بسی خطا ولیک</p></div>
<div class="m2"><p>ناوک چشم مست او هیچ خطا نمی‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به وصال او رسم، هم بربایم از لبش</p></div>
<div class="m2"><p>یک دو سه بوسه ناگهان، گر چه رها نمیکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوس به جان بها کنیم، ار بفروخت خود نکو</p></div>
<div class="m2"><p>ور نفروخت میبریم آنچه بها نمی‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چارهٔ من خدا کند در غم روی او مگر</p></div>
<div class="m2"><p>خود نکند به جای کس هر چه خدا نمیکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در غم او بسوختند اهل جهان،حسود من</p></div>
<div class="m2"><p>خام نشسته پیش او شکر چرا نمیکند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست بدار، اوحدی، یار دگر به دست کن</p></div>
<div class="m2"><p>کو غم ما نمی‌خورد، چارهٔ ما نمی‌کند</p></div></div>