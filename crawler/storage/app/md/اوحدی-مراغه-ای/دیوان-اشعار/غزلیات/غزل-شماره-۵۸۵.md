---
title: >-
    غزل شمارهٔ ۵۸۵
---
# غزل شمارهٔ ۵۸۵

<div class="b" id="bn1"><div class="m1"><p>امروز عید ماست، که قربان او شدیم</p></div>
<div class="m2"><p>اکنون شویم شاه، که دربان او شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان غریب نیست که باشد غریب‌دار</p></div>
<div class="m2"><p>این سرو ماه چهره، که مهمان او شدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بادصبح، بگذر و از ما سلام کن</p></div>
<div class="m2"><p>بر روضه‌ای، که عاشق رضوان او شدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرخنده یوسفیست، که زندان اوست دل</p></div>
<div class="m2"><p>زیبا محمدیست، که سلمان او شدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این خواجه از کجاست؟ که «طوعا و رغبة»</p></div>
<div class="m2"><p>بی‌کره و جبر بندهٔ فرمان او شدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ما گدای آن رخ و درویش آن دریم</p></div>
<div class="m2"><p>ننشست خسروی، که ز سلطان او شدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم: ز درد عشق تو شد اوحدی هلاک</p></div>
<div class="m2"><p>گفتا: چه غم ز درد؟ که درمان او شدیم</p></div></div>