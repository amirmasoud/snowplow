---
title: >-
    غزل شمارهٔ ۲۴۸
---
# غزل شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>چون من سر تو دارم سامانم از که باشد؟</p></div>
<div class="m2"><p>دردم تو می‌فرستی، درمانم از که باشد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی: برو ز پیشم، خود می‌روم، ولیکن</p></div>
<div class="m2"><p>زین غصه گر بمیرم تاوانم از که باشد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون در فراق خویشم زار و ضعیف کردی</p></div>
<div class="m2"><p>گر بار غم کشیدن نتوانم، از که باشد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردم همی فرستی هر ساعت از برخود</p></div>
<div class="m2"><p>باز آر به درد دوری درمانم، از که باشد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بوسه‌ای به زاری هرگز نمی‌دهی تو</p></div>
<div class="m2"><p>گر بعد ازین به زورت بستانم، از که باشد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوشم به طنز گفتی: کز کیست این فغانت؟</p></div>
<div class="m2"><p>زخم تو می‌خورم من، افغانم از که باشد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوری که می‌پسندی بر اوحدی نهانی</p></div>
<div class="m2"><p>گر در میان مردم برخوانم، از که باشد؟</p></div></div>