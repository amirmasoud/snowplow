---
title: >-
    غزل شمارهٔ ۵۲۷
---
# غزل شمارهٔ ۵۲۷

<div class="b" id="bn1"><div class="m1"><p>به غم خویش چنان شیفته کردی بازم</p></div>
<div class="m2"><p>کز خیال تو به خود نیز نمی‌پردازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که از نالهٔ شبگیر من آگاه شود</p></div>
<div class="m2"><p>هیچ شک نیست که چون روز بداند رازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته بودی: خبری ده، که ز هجرم چونی؟</p></div>
<div class="m2"><p>آن چنانم که ببینی و ندانی بازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عهد کردی که: نسوزی به غم خویش مرا</p></div>
<div class="m2"><p>هیچ غم نیست، تو می‌سوز، که من میسازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد ازین با رخ خوب تو نظر خواهم باخت</p></div>
<div class="m2"><p>گو: همه شهر بدانند که: شاهد بازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن چنان بر دل من ناز تو خوش می‌آید</p></div>
<div class="m2"><p>که حلالت نکنم گر نکشی از نازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر از دام خودم نیز خلاصی بخشی</p></div>
<div class="m2"><p>هم به خاک سر کوی تو بود پروازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اوحدی گر نه چو پروانه بسوزد روزی</p></div>
<div class="m2"><p>پیش روی تو چو شمعش به شبی بگدازم</p></div></div>