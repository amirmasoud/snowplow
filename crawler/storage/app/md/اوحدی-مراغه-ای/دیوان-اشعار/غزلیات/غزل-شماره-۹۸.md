---
title: >-
    غزل شمارهٔ ۹۸
---
# غزل شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>ترک گندم گون من هر دم به جنگی دیگرست</p></div>
<div class="m2"><p>روی او را هر زمان حسنی و رنگی دیگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنگهای شکر مصری بسی دیدیم، لیک</p></div>
<div class="m2"><p>شکر شیرین دهان او ز تنگی دیگرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از میان دلبران شنگ و گل رویان شوخ</p></div>
<div class="m2"><p>یار ما را می‌رسد، شوخی و شنگی دیگرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیدلان خسته را زان زلفهای چون رسن</p></div>
<div class="m2"><p>هر زمان در گردن دل پالهنگی دیگرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌وفا خواند مرا خود پیش ازین در عشق او</p></div>
<div class="m2"><p>نام من بد گشته بود، این نیز ننگی دیگرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بگویم: صلح کن، گوید: بگیرم در کنار</p></div>
<div class="m2"><p>راستی صلحی چنین بنیاد جنگی دیگرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای نصیحت‌گو،دمی چنگ از گریبانم بدار</p></div>
<div class="m2"><p>کین زمانم دامن خاطر به چنگی دیگرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کمان ابروی آن تیر بالا هر نفس</p></div>
<div class="m2"><p>اوحدی را در دل مسکین خدنگی دیگرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش ازین سنگی ز راه خویش اگر بر می‌گرفت</p></div>
<div class="m2"><p>این زمان نتوان، که دستش زیر سنگی دیگرست</p></div></div>