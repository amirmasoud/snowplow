---
title: >-
    غزل شمارهٔ ۴۵۱
---
# غزل شمارهٔ ۴۵۱

<div class="b" id="bn1"><div class="m1"><p>ما به ابد می‌بریم عشق ترا از ازل</p></div>
<div class="m2"><p>در همه عالم که دید عشق چنین بی‌خلل؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر من شور تو هیچ نیاید برون</p></div>
<div class="m2"><p>گر چه سر آید زمان ور چه در آید اجل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ کسم، گر بدل بر تو گزینم به دل</p></div>
<div class="m2"><p>هیچ کسی خود بدل بر تو گزیند بدل؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع لبت را بدید، مهر گرفت از عقیق</p></div>
<div class="m2"><p>موم دهانت بدید مهر گرفت از عسل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راهرو عقل را زلف تو دارالامان</p></div>
<div class="m2"><p>کار کن روح را لطف تو بیت‌العمل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوده ز جور تو ما در همه وقتی زبون</p></div>
<div class="m2"><p>گشته به مهر تو ما در همه گیتی مثل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه شبستان تو مورچهٔ وتخت جم</p></div>
<div class="m2"><p>وصل تو و جان ما یوسف و سیم دغل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلف تو تن را نوشت سورهٔ نون بر ورق</p></div>
<div class="m2"><p>قد تو دل را نهاد لوح الف در بغل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم مرا از لبت نیست گزیری که، هست</p></div>
<div class="m2"><p>لعل لبت را شکر، چشم سرم را سبل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فوت نشد نکته‌ای از کشش و از جسش</p></div>
<div class="m2"><p>با لب و زلف ترا مرتبهٔ عقد و حل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اوحدی از دیر باز فتنهٔ تست، ای غزال</p></div>
<div class="m2"><p>تا نشود ناامید زود نیوش این غزل</p></div></div>