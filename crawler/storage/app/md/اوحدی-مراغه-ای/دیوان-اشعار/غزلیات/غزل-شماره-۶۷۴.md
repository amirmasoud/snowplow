---
title: >-
    غزل شمارهٔ ۶۷۴
---
# غزل شمارهٔ ۶۷۴

<div class="b" id="bn1"><div class="m1"><p>به چشم سر هدف سازم دل خود را به جان تو</p></div>
<div class="m2"><p>اگر بر نام من تیری بیندازد کمان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل من بوسه‌ای زان لب تمنی می‌کند، لیکن</p></div>
<div class="m2"><p>نمی‌گویم سخن بی‌زر، که می‌دانم زبان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دست خود نخواهی کردن اندر گردنم روزی</p></div>
<div class="m2"><p>شبی بگذار تا باشد دو دستم در میان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا گفتی: میان در بند اگر خواهی کنار من</p></div>
<div class="m2"><p>میان بستم، که دربندم به دست خود میان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو از حکم حدیث تو نمی‌دانم گذشتن من</p></div>
<div class="m2"><p>شگفتم زان حدیث آید که بگذشت از زبان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه باشد گر به نام من فرو خواند لبت حرفی؟</p></div>
<div class="m2"><p>ز چندان آیت خوبی که منزل شد بشان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر جانب ز شوقت چون سگ گم گشته می‌گردم</p></div>
<div class="m2"><p>به بوی آنکه در یابم غبار کاروان تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خنک یاری که هستی تو به خلوت هم نشین او!</p></div>
<div class="m2"><p>که من باری نمی‌یابم نشانی از نشان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دستان اوحدی را کرد چشمت پیر می‌بینم</p></div>
<div class="m2"><p>سرش را من، که خواهد رفت در پای جوان تو</p></div></div>