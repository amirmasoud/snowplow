---
title: >-
    غزل شمارهٔ ۲۷۸
---
# غزل شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>سر نگردانم ازو، گر به سرم گرداند</p></div>
<div class="m2"><p>بنهم گردن، اگر خاک درم گرداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه چنان بستهٔ مهرم که بپیچانم رخ</p></div>
<div class="m2"><p>وقت شمشیر زدن گر سپرم گرداند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی بنمود و چو مشتاق شدم، بار دگر</p></div>
<div class="m2"><p>باز پوشید، که مشتاق ترم گرداند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه آنست که: یاد لب شیرینش باز</p></div>
<div class="m2"><p>همچو فرهاد به کوه و کمرم گرداند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای نسیم سحر، از خود به فغانم، برسان</p></div>
<div class="m2"><p>خبر او، که ز خود بی‌خبرم گرداند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش ازینم خبر از پا و سر خود می‌بود</p></div>
<div class="m2"><p>وقت آنست که بی‌پا و سرم گرداند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی در غمش ار ناله چنین خواهد کرد</p></div>
<div class="m2"><p>زود باشد که به گیتی سمرم گرداند</p></div></div>