---
title: >-
    غزل شمارهٔ ۴۴۶
---
# غزل شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>گفتم: به چابکی ببرم جان ز دست عشق</p></div>
<div class="m2"><p>خود هیچ یاد و هوش نیاورد مست عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد گونه مرهم ار بنهی سودمند نیست</p></div>
<div class="m2"><p>آنرا که زخم بر جگر آمد ز شست عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتیم: دل ز عشق بپرداختیم و خود</p></div>
<div class="m2"><p>هر روز بیش می‌شود این جا نشست عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چند سر کشیدم ازین عشق سالها</p></div>
<div class="m2"><p>هم زیر پای کرده مرا زور دست عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایزد مگر به لطف خلاصی دهد، که راه</p></div>
<div class="m2"><p>بیرون نمی‌بریم ز دیوار بست عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای نیک‌خواه عافیت اندیش خیر گوی،</p></div>
<div class="m2"><p>زین پس مکن نصیحت محنت پرست عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرسیده‌ای که: باده خورد اوحدی؟ بلی</p></div>
<div class="m2"><p>خوردست باده، لیک ز جام الست عشق</p></div></div>