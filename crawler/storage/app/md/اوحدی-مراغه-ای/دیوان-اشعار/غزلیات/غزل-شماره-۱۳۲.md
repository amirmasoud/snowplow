---
title: >-
    غزل شمارهٔ ۱۳۲
---
# غزل شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>با من از شادی وصل تو اثر چیزی نیست</p></div>
<div class="m2"><p>دل ریشست و تن زار و دگر چیزی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل من بردی و گویی که: ندانم که کجاست؟</p></div>
<div class="m2"><p>از سر زلف سیاه تو به در چیزی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینه را ساخته بودم سپر تیر غمت</p></div>
<div class="m2"><p>دل نهادم به جراحت، که سپر چیزی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو چشمت که: مرابی‌تو به شبهای دراز</p></div>
<div class="m2"><p>تا دم صبح به جز آه سحر چیزی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته‌ای: درد ترا نیست نشانی پیدا</p></div>
<div class="m2"><p>اشک چون سیم ببین، روی چو زر چیزی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آبرویی نبود پیش تو من بعد مرا</p></div>
<div class="m2"><p>که برین چهره به جز خون جگر چیزی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیگران را همه اسبابی و مالی باشد</p></div>
<div class="m2"><p>اوحدی را بجزین دیدهٔ تر چیزی نیست</p></div></div>