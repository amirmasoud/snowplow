---
title: >-
    غزل شمارهٔ ۲۰۵
---
# غزل شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>سوز تو شبی بسازم آورد</p></div>
<div class="m2"><p>وندر سخنی درازم آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان دم که تو روی باز کردی</p></div>
<div class="m2"><p>از هر چه به جز تو بازم آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تیغ زنند رخ نپیچیم</p></div>
<div class="m2"><p>زین قبله که در نمازم آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اقبال به کعبهٔ وصالت</p></div>
<div class="m2"><p>بی‌درد سر مجازم آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون توبهٔ منزل امانی</p></div>
<div class="m2"><p>با بدرقه و جوازم آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لطف تو به مکهٔ حقیقت</p></div>
<div class="m2"><p>از بادیهٔ حجازم آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن بخت که دل به خواب می‌جست</p></div>
<div class="m2"><p>بیدار ز در فرازم آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این قاعدهٔ نیازمندی</p></div>
<div class="m2"><p>در عهد تو بی‌نیازم آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون دید که: شمع جمع عشقم</p></div>
<div class="m2"><p>اندوه تو در گدازم آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گستاخی اوحدی برتو</p></div>
<div class="m2"><p>در غارت و ترکتازم آورد</p></div></div>