---
title: >-
    غزل شمارهٔ ۱۶۲
---
# غزل شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>هر کسی را می‌نوازد لطف و خاطر جستنت</p></div>
<div class="m2"><p>چون به نزد ما رسی، با خاطر آید جستنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امشبم داغی نهادی از جفا بر دل، کزو</p></div>
<div class="m2"><p>سالها نتوان، اگر روزی بباید شستنت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ترا میخواهم از دنیا، بهر منزل که هست</p></div>
<div class="m2"><p>ای که منزل در دلم داری ومن در جستنت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو بستانی دگر هرگز نرستی از زمین</p></div>
<div class="m2"><p>راستی را گر بدیدی اعتدال رستنت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو من عهد از میان جان شیرین کرده‌ام</p></div>
<div class="m2"><p>وه! کرا دل میدهد عهد چنان بشکستنت؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نخواهی تا چو من مسکین و بی‌مسکن شوی</p></div>
<div class="m2"><p>خاطر مسکین مسکینان نباید خستنت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی، چون دانهٔ خالش دلت را صید کرد</p></div>
<div class="m2"><p>بعد ازین از دام او ممکن نباشد جستنت</p></div></div>