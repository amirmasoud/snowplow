---
title: >-
    غزل شمارهٔ ۳۴
---
# غزل شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>دراز شد سفر یار دور گشتهٔ ما</p></div>
<div class="m2"><p>فغان ازین دلی بی‌او نفور گشته ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آن رسید که توفان بر آیدم بدو چشم</p></div>
<div class="m2"><p>ز سوز سینه همچون تنور کشته ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخواند راوی مستان به صوت داودی</p></div>
<div class="m2"><p>ز شوق او سخن چون زبور گشته ما؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بودی آنکه چو حوری در آمدی هر دم</p></div>
<div class="m2"><p>به خانهٔ چو سرای سرور گشتهٔ ما؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه بودی ار خبر او همی رسانیدند</p></div>
<div class="m2"><p>به گوش خاطر از خود نفور گشته ما؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز حافظان وفا نیست مشفقی که کند</p></div>
<div class="m2"><p>ملامت دل از کار دور گشتهٔ ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث ما تو بگوی، اوحدی که مشغولست</p></div>
<div class="m2"><p>به یاد دوست دل با حضور گشتهٔ ما</p></div></div>