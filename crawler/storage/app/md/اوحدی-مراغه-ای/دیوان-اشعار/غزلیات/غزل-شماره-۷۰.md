---
title: >-
    غزل شمارهٔ ۷۰
---
# غزل شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>چون گشت با تو ما را پیوند دل زیادت</p></div>
<div class="m2"><p>گر هجر ما، گزینی، دوری ز حسن عادت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبهاست تا دلم را تب دارد از غم تو</p></div>
<div class="m2"><p>آه! از تو، گر نیایی روزی بدین عیادت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبعت به طالع ما شد تند و تیز، ارنه</p></div>
<div class="m2"><p>زین بیشتر نبودی بدمهر و بی‌ارادت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشقی که نیست برتو، حربیست بی‌غنیمت</p></div>
<div class="m2"><p>عیشی که نیست با تو، دینیست بی‌شهادت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند نیست با ما مهر تو در ترقی</p></div>
<div class="m2"><p>هر لحظه با تو ما را شوقیست در زیادت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاگرد صورت تست آیینه در لطیفی</p></div>
<div class="m2"><p>کین می‌کند تجلی و آن میکند اعادت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندان که جور خواهی بر جان من همی کن</p></div>
<div class="m2"><p>کز بندگان نیاید کاری به جز عبادت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باشد که: اوحدی را از غیب دست گیرد</p></div>
<div class="m2"><p>آن کس که واقفست او بر غیب و بر شهادت</p></div></div>