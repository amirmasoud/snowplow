---
title: >-
    غزل شمارهٔ ۷۵۵
---
# غزل شمارهٔ ۷۵۵

<div class="b" id="bn1"><div class="m1"><p>با این چنین بلایی، بعد از چنان عذابی</p></div>
<div class="m2"><p>راضی شدم که: بینم روی ترا به خوابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد نامه مشق کردم در شرح مهربانی</p></div>
<div class="m2"><p>نادیده از تو هرگز یک نامه را جوابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر گه که بر در تو من آب روی جویم</p></div>
<div class="m2"><p>خون مرا بریزی بر خاک در چو آبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر غم تو رازم رمزی دو بود و اکنون</p></div>
<div class="m2"><p>هر حرف از آن شکایت فصلی شدست و بابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز سر صورت تو چیزی دگر ندارم</p></div>
<div class="m2"><p>مقصود هر حدیثی، مضمون هر کتابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندان نمک لبت را در پسته بسته آخر</p></div>
<div class="m2"><p>کی بی‌نمک بماند بر آتشت کبابی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در غیرتیم لیکن مقدور نیست کس را</p></div>
<div class="m2"><p>با چشم چون تو شوخی آغاز احتسابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک تن کجا تواند؟پوشید از نظرها</p></div>
<div class="m2"><p>روی ترا، که این جا شهریست و آفتابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در غصه اوحدی را موقوف چند داری؟</p></div>
<div class="m2"><p>یا کشتن خطایی، یا گفتن صوابی</p></div></div>