---
title: >-
    غزل شمارهٔ ۱۴۹
---
# غزل شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>به وقت گل پی معشوق و باده باید رفت</p></div>
<div class="m2"><p>سوار عیش تراند، پیاده باید رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چمن بسان بهشتی گشاده روی طرب</p></div>
<div class="m2"><p>در آن بهشت به روی گشاده باید رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهشت خوش نبود بی‌جمال نازک یار</p></div>
<div class="m2"><p>یکی دو ره پی آن حورزاده باید رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سیب ساده بود شاخها به موسم گل</p></div>
<div class="m2"><p>به بوی آن رخ چون سیب ساده باید رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سر برون نهی از شهر و روی در صحرا</p></div>
<div class="m2"><p>بزرگ‌زادگی از سهر نهاده باید رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن زمان که به عزم طرب شوی بر پای</p></div>
<div class="m2"><p>نشاط باده به سر در فتاده باید رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای کاسه گرفتن سبو چو زد زانو</p></div>
<div class="m2"><p>پیاله وار بر ایستاده باید رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز باده پر قدحی چند نوش کرده دگر</p></div>
<div class="m2"><p>به دست بر قدحی پر ز باده باید رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازین جهان چو همی باید، اوحدی، رفتن</p></div>
<div class="m2"><p>به کام داد دل خویش داده باید رفت</p></div></div>