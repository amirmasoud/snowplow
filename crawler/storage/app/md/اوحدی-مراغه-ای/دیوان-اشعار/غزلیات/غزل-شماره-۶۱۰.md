---
title: >-
    غزل شمارهٔ ۶۱۰
---
# غزل شمارهٔ ۶۱۰

<div class="b" id="bn1"><div class="m1"><p>کأس می در دست و کوس عشق بر بامستمان</p></div>
<div class="m2"><p>چون بود انکار با می خواره و با مستمان؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زود جام زهد خود بر سنگ شیدایی زند</p></div>
<div class="m2"><p>گر بنوشد صوفی آن صافی که در جا مستمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه میخواهد که: ما را سر بگرداند ز عشق</p></div>
<div class="m2"><p>تیغ بر کش، گو: چه جای سنگ و دشنامستمان!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که میگویی: سر خود گیر و دست از من بدار</p></div>
<div class="m2"><p>تا برون آید سر و دستی که در دامستمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه بنویسیم صد دفتر نخواهد شد تمام</p></div>
<div class="m2"><p>شرح آن تلخی، که از هجر تو در کامستمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک چشم من کنون خونیست و آن خون نیز هم</p></div>
<div class="m2"><p>چون ببینی یا ز دل، یا از جگر وامستمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ترا دیدیم دل را آرزویی جز تو نیست</p></div>
<div class="m2"><p>تا نپنداری که میل خواب و آرامستمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به منزل باش،گو، کز تو چه خواریها کشیم؟</p></div>
<div class="m2"><p>کانچه دیدیم از تو سودا اولین گامستمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر جهان پر نقش باشد در دل ما جز یکی</p></div>
<div class="m2"><p>نیست ممکن، خاصه کاکنون اوحدی نامستمان</p></div></div>