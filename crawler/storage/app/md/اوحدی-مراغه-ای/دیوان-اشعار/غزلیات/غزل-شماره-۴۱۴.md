---
title: >-
    غزل شمارهٔ ۴۱۴
---
# غزل شمارهٔ ۴۱۴

<div class="b" id="bn1"><div class="m1"><p>هر چه گویم من، ای دبیر، امروز</p></div>
<div class="m2"><p>نه به هوشم، ز من مگیر امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قلم نیستی به من در کش</p></div>
<div class="m2"><p>که گرفتارم و اسیر امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالها در کمین نشستم تا</p></div>
<div class="m2"><p>در کمانم کشد چو تیر امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رو بشارت زنان، که گشت یکی</p></div>
<div class="m2"><p>با غلام خود آن امیر امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده بر من مدر، که نتوان دوخت</p></div>
<div class="m2"><p>نظر از یار بی‌نظیر امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میل یار قدیم دارد دل</p></div>
<div class="m2"><p>تن ازین غصه، گو: بمیر امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی، جز حدیث دوست مگوی</p></div>
<div class="m2"><p>که جزو نیست در ضمیر امروز</p></div></div>