---
title: >-
    غزل شمارهٔ ۸۱۷
---
# غزل شمارهٔ ۸۱۷

<div class="b" id="bn1"><div class="m1"><p>نه بیگانه‌ای، ای بت خانگی</p></div>
<div class="m2"><p>مکن با من خسته بیگانگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو گر پایمردی نکردی به لطف</p></div>
<div class="m2"><p>چه سود این دلیری و مردانگی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پری‌زاده‌ای چون تو پیش نظر</p></div>
<div class="m2"><p>نباشد ز من طرفه دیوانگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چراغیست روی تو، ای ماهرخ</p></div>
<div class="m2"><p>که شمعش نیرزد به پروانگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگیری بسی دل زلف چو دام</p></div>
<div class="m2"><p>گر آن خال مشکین کند دانگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مهر سر زلفت، ای سنگدل</p></div>
<div class="m2"><p>هوس می‌کند سنگ را شانگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تمکین مکوش، اوحدی، در غمش</p></div>
<div class="m2"><p>که عاشق نکوشد به فرزانگی</p></div></div>