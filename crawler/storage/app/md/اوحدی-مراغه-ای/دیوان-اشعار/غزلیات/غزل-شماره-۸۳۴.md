---
title: >-
    غزل شمارهٔ ۸۳۴
---
# غزل شمارهٔ ۸۳۴

<div class="b" id="bn1"><div class="m1"><p>ای هر سر مویت را رویی به پریشانی</p></div>
<div class="m2"><p>صد روی خراشیده موی تو به پیشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سینه نهان کردم سودای تو مه، لیکن</p></div>
<div class="m2"><p>بس درد که برخیزد زین آتش پنهانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن دیگ نبایستی پختن به نیستانها</p></div>
<div class="m2"><p>اکنون که برفت آتش، با دست پشیمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انکار مکن ما را گر بی‌سر و پا بینی</p></div>
<div class="m2"><p>کین کار هم از اول سر داشت به ویرانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای یار پری پیکر، دیوانه شدیم از تو</p></div>
<div class="m2"><p>باز آی، که صد نوبت کردیم پری خوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک روز نمی‌آیی، تا در غم خود بینی</p></div>
<div class="m2"><p>صد خانهٔ چون دوزخ، صد دیدهٔ چون خانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوری که تو، ای کافر، کردی و پسندیدی</p></div>
<div class="m2"><p>گر بر تو کنم گویی: ای وای مسلمانی!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زینسان که سراسیمه گشت اوحدی از مهرت</p></div>
<div class="m2"><p>او باز کجا دارد دست از تو به آسانی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در وصف تو دیوانی از شعر چو پر کرد او</p></div>
<div class="m2"><p>پر بر تو کند دعوی از شرعی و دیوانی</p></div></div>