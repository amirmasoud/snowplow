---
title: >-
    غزل شمارهٔ ۴
---
# غزل شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>قراری چون ندارد جانم اینجا</p></div>
<div class="m2"><p>دل خود را چه می‌رنجانم اینجا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر عاشق کله‌داری نداند</p></div>
<div class="m2"><p>بنه کفشی، که من مهمانم اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا گفتی: کز آنجا آگهی چیست؟</p></div>
<div class="m2"><p>چه می‌پرسی، که من حیرانم اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه او پنهان شد از چشمم، که من نیز</p></div>
<div class="m2"><p>ز چشم مدعی پنهانم اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر بتوان حدیثی گوی از آن روی</p></div>
<div class="m2"><p>که من بی‌روی او نتوانم اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگارینی که سرگرداند از من</p></div>
<div class="m2"><p>نگردانی، که سرگردانم اینجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا با دوست پیمانی قدیمست</p></div>
<div class="m2"><p>بدان پیوند و آن پیمانم اینجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز زلفش برد ما غم هست بویی</p></div>
<div class="m2"><p>چنین زنده به بوی آنم اینجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به درد اوحدی دلشاد گشتم</p></div>
<div class="m2"><p>که آن لب می‌کند درمانم اینجا</p></div></div>