---
title: >-
    غزل شمارهٔ ۵۳۷
---
# غزل شمارهٔ ۵۳۷

<div class="b" id="bn1"><div class="m1"><p>عیب من نیست که: در عشق تو تیمار کشم</p></div>
<div class="m2"><p>بار بر گردن من چون تو نهی بار کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر خاک درت گر بودم راه شبی</p></div>
<div class="m2"><p>سرمه‌وارش همه در دیدهٔ بیدار کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم آن نیست که من بعد به کاری آید</p></div>
<div class="m2"><p>مگرش من به تمنای تو در کار کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دهان تو، که از وی شکر اندر تنگست</p></div>
<div class="m2"><p>اگرم دست دهد قند به خروار کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که گل چیند از خار نباید نالید</p></div>
<div class="m2"><p>من که دل بر تو نهم جور به ناچار کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با سر زلف تو خود دست درازی نه رواست</p></div>
<div class="m2"><p>به ازآن نیست که پای بمقدار کشم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی، قصهٔ بیگانه بر یار برند</p></div>
<div class="m2"><p>من به پیش که بر جور که از یار کشم؟</p></div></div>