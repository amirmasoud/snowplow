---
title: >-
    غزل شمارهٔ ۸۴۸
---
# غزل شمارهٔ ۸۴۸

<div class="b" id="bn1"><div class="m1"><p>جفا بر کسی بیش ازین چون کنی؟</p></div>
<div class="m2"><p>که هر دم به نوعی دلش خون کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو روزی ز دست غم خود مرا</p></div>
<div class="m2"><p>به صحرا دوانی و مجنون کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگویم به کس حال بیداد تو</p></div>
<div class="m2"><p>که ترسم بگویند و افزون کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی‌دارم از دامنت دست باز</p></div>
<div class="m2"><p>گرم دامن دیده جیحون کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برآنی که بر من کنی رحمتی</p></div>
<div class="m2"><p>چه سودم دهد؟ گر نه اکنون کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبود این گمان اوحدی را بتو</p></div>
<div class="m2"><p>که با او دل خود دگرگون کنی</p></div></div>