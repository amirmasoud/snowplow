---
title: >-
    غزل شمارهٔ ۲۶۵
---
# غزل شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>تا دل مجروح من عاشق زار تو شد</p></div>
<div class="m2"><p>هیچ ندیدیم و عمر در سر کار تو شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعل تو روزی مرا وعدهٔ وصلی بداد</p></div>
<div class="m2"><p>فکرم ازان روز باز روز شمار تو شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنده بود عاشقی، کز هوس روی تو</p></div>
<div class="m2"><p>بر سر کوی تو مرد، خاک دیار تو شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبح چو حسن تو کرد روی به باغ آفتاب</p></div>
<div class="m2"><p>مشغله از ره براند، مشعله‌دار تو شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سر خاک درت دوش غباری بخاست</p></div>
<div class="m2"><p>باد بهشت آن بدید، خاک غبار تو شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طعنه زند سرمه را، چشم چو خاک تو دید</p></div>
<div class="m2"><p>شکر کند زخم را، دل که شکار تو شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمرهٔ عشاق را در شب دیدار قرب</p></div>
<div class="m2"><p>هر دل و جانی که بود، جمله نثار توشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاکرم از دل، که او گشت شکارت، بلی</p></div>
<div class="m2"><p>شکر کند زخم را، دل که شکار تو شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از همه گنجی سعید وز همه رنجی بعید</p></div>
<div class="m2"><p>گر تو ندانی که کیست؟ اوست که یار تو شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زندهٔ جاوید ماند، سکهٔ اقبال یافت</p></div>
<div class="m2"><p>سر که فدای تو گشت، زر که نثار تو شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر ز خط اوحدی بر نگرفت آفتاب</p></div>
<div class="m2"><p>تا قلم فکر او وصف نگار تو شد</p></div></div>