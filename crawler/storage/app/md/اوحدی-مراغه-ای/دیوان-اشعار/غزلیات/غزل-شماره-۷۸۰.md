---
title: >-
    غزل شمارهٔ ۷۸۰
---
# غزل شمارهٔ ۷۸۰

<div class="b" id="bn1"><div class="m1"><p>بر خسته‌ای ملامت چندین چه می‌پسندی؟</p></div>
<div class="m2"><p>کورا نظر بپوشد شوخی به چشم‌بندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خواجهٔ فسرده، خوبی دلت نبرده</p></div>
<div class="m2"><p>گر درد ما بنوشی، بر درد ما نخندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون پسته لب ببستم از ذکر شکر او</p></div>
<div class="m2"><p>زان شب که نقل کردیم آن پسته‌های قندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دست کوته ما مهر زر ار نبیند</p></div>
<div class="m2"><p>کی سر نهد به مهری؟ سروی بدان بلندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیگر به هیچ آبی در بار و بر نیاید</p></div>
<div class="m2"><p>شاخ سکون و صبرم، کز بیخ و بن بکندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکس حکایت خود اندر نبشت، لیکن</p></div>
<div class="m2"><p>چون اوحدی که داند سر نیازمندی؟</p></div></div>