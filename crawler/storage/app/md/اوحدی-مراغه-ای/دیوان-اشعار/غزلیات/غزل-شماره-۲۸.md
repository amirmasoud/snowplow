---
title: >-
    غزل شمارهٔ ۲۸
---
# غزل شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>زخمی، که بر دل آید ، مرهم نباشد او را</p></div>
<div class="m2"><p>خامی که دل ندارد این غم نباشد او را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که: دل بدوده، من جان همی فرستم</p></div>
<div class="m2"><p>زیرا که با چنان رخ دل کم نباشد او را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیسی مریم از تو گر باز گردد این دم</p></div>
<div class="m2"><p>این مرده زنده کردن دردم نباشد او را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند: ازو طلب دار آیین مهربانی</p></div>
<div class="m2"><p>نه نه، طلب ندارم، دانم نباشد او را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پیش هیچ خوبی هرگز وفا نجستم</p></div>
<div class="m2"><p>زیرا وفا و خوبی باهم نباشد او را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چشم من خجل شد ابر بهار صد پی</p></div>
<div class="m2"><p>او گر چه بربگرید، این نم نباشد او را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این گریه کاوحدی کرد از درد دوری او</p></div>
<div class="m2"><p>گر بعد ازین بمیرد ماتم نباشد او را</p></div></div>