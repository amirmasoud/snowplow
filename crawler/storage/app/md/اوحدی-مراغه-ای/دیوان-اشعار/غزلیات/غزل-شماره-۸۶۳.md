---
title: >-
    غزل شمارهٔ ۸۶۳
---
# غزل شمارهٔ ۸۶۳

<div class="b" id="bn1"><div class="m1"><p>یک سخن زان لعل خاموشم بگوی</p></div>
<div class="m2"><p>نکته‌ای شیرین‌تر از نوشم بگوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دهانم نه لب و سری که هست</p></div>
<div class="m2"><p>از زبان خویش در گوشم بگوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امشبم چون دوش بودن آرزوست</p></div>
<div class="m2"><p>از چه می‌داری شب دوشم؟ بگوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوش من در گفتن شیرین تست</p></div>
<div class="m2"><p>تا نباید رفتن از هوشم بگوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیشبم پوشیده گفتی: سر بیار</p></div>
<div class="m2"><p>این سخن بی‌یار سر پوشم بگوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دل مجنون من پیغام وصل</p></div>
<div class="m2"><p>پیش از آن کز هجر برجوشم بگوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی، با چشم مستش حال من</p></div>
<div class="m2"><p>گر نکردستی فراموشم بگوی</p></div></div>