---
title: >-
    غزل شمارهٔ ۲۹۶
---
# غزل شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>چون ز بغداد و لب دجله دلم یاد کند</p></div>
<div class="m2"><p>دامنم را چو لب دجلهٔ بغداد کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ کس نیست که از یار سفر کردهٔ من</p></div>
<div class="m2"><p>برساند خبری خیر و دلم شاد کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز از یاد من خسته فراموش نشد</p></div>
<div class="m2"><p>آنکه هرگز نتواند که مرا یاد کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هجر داغیست که گر بر جگر کوه نهند</p></div>
<div class="m2"><p>سنگ بر سینه زنان آید و فریاد کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانهٔ عمر مرا عشق ز بنیاد بکند</p></div>
<div class="m2"><p>عشق باشد که چنین کار به بنیاد کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه خون دل من ریخت ز بیداد و برفت</p></div>
<div class="m2"><p>کاج باز آید و خون ریزد و بیداد کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه غم از شاه و چه اندیشه ز خسرو باشد؟</p></div>
<div class="m2"><p>گر به شیرین رسد آن ناله که فرهاد کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد بر گلبن این باغ گلی را نگذاشت</p></div>
<div class="m2"><p>کز نسیمش دلم از بند غم آزاد کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوحدی چون که از آن خرمن گل دورافتاد</p></div>
<div class="m2"><p>خرمن عمر، ضروریست، که بر باد کند</p></div></div>