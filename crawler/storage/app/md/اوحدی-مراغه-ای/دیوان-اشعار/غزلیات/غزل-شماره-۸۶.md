---
title: >-
    غزل شمارهٔ ۸۶
---
# غزل شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>ای نسیم صبح دم، یارم کجاست؟</p></div>
<div class="m2"><p>غم ز حد بگذشت، غم‌خوارم کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت کارست، ای نسیم، از کار او</p></div>
<div class="m2"><p>گر خبر داری، بگو دارم، کجاست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواب در چشمم نمی‌آید به شب</p></div>
<div class="m2"><p>آن چراغ چشم بیدارم کجاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر در او از برای دیدنی</p></div>
<div class="m2"><p>بارها رفتم، ولی بارم کجاست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوست گفت: آشفته گرد و زار باش</p></div>
<div class="m2"><p>دوستان آشفته و زارم، کجاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیستم آسوده از کارش دمی</p></div>
<div class="m2"><p>یارب، آن، آسوده از کارم کجاست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به گوش او رسانم حال خویش</p></div>
<div class="m2"><p>نالهای اوحدی‌وارم کجاست؟</p></div></div>