---
title: >-
    غزل شمارهٔ ۴۹۲
---
# غزل شمارهٔ ۴۹۲

<div class="b" id="bn1"><div class="m1"><p>معراج ما به روح و روان بود صبح دم</p></div>
<div class="m2"><p>دیدار ما به دیدهٔ جان بود صبح دم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دلفروز پرده برانداخت همچو روز</p></div>
<div class="m2"><p>از چشم غیر اگرچه نهان بود صبح دم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون فکرتم ز انفس و آفاق در گذشت</p></div>
<div class="m2"><p>پرواز من برون ز جهان بود صبح‌دم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با جبرئیل عقل روانم، که شاد باد،</p></div>
<div class="m2"><p>از رفرف دماغ روان بود صبح دم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جایی رسید فکرم و بگذشت، کندرو</p></div>
<div class="m2"><p>روح‌القدس کشیده عنان بود صبح دم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طاوس جانم از هوس منتهای وصل</p></div>
<div class="m2"><p>بر شاخ سدره جلوه کنان بود صبح دم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریافتم ز قرب مکانی و منزلی</p></div>
<div class="m2"><p>کان جانه منزل و نه مکان بود صبح دم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندیشها که وهم هراسنده کرده بود</p></div>
<div class="m2"><p>با شوق گفتنم نه چنان بود صبح‌دم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و آن سودها که نفس هوس پیشه جمع داشت</p></div>
<div class="m2"><p>در کوی عشق جمله زیان بود صبح دم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>او خود ثنای خود به خودی گفت: کاوحدی</p></div>
<div class="m2"><p>از وصف حال کند زبان بود صبح‌دم</p></div></div>