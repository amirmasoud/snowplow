---
title: >-
    غزل شمارهٔ ۴۴۵
---
# غزل شمارهٔ ۴۴۵

<div class="b" id="bn1"><div class="m1"><p>مردی به هوش بودم و خاطر بجای خویش</p></div>
<div class="m2"><p>ناگاه در کمند تو رفتم به پای خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدبار گفته‌ام دل خود را بدین هوس:</p></div>
<div class="m2"><p>کای دل به قتل خویشتنی رهنمای خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقتی علاج مردم بیمار کردمی</p></div>
<div class="m2"><p>اکنون چنان شدم که ندانم دوای خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باشد بجای خویش اگرم سرزنش کنی</p></div>
<div class="m2"><p>تا پیش ازین چرا ننشستم بجای خویش؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش تو نیست روی سخن گفتنم، مگر</p></div>
<div class="m2"><p>بر دست قاصدی بفرستم دعای خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو: بوسه‌ای بده، لبت ار می‌کشد مرا</p></div>
<div class="m2"><p>باری گرفته باشم ازو خون بهای خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای اوحدی، چو همت او بر هلاک تست</p></div>
<div class="m2"><p>شرط آن بود که سعی کنی در فنای خویش</p></div></div>