---
title: >-
    غزل شمارهٔ ۲۱۲
---
# غزل شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>دیگر مرا به ضربت شمشیر غم بزد</p></div>
<div class="m2"><p>فریاد ازین سوار، که صید حرم بزد!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزلت گزیده بودم و کاری گرفته پیش</p></div>
<div class="m2"><p>یارم ز در درآمد و کارم به هم بزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دم در کشیده بود دل من ز دیر باز</p></div>
<div class="m2"><p>آتش در اوفتاد به جانم، چو دم بزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درویش را ز نوشت شاهی خبر نشد</p></div>
<div class="m2"><p>تا روزگاز نوبت این محتشم بزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دیده بر طلایهٔ حسنش نظر فگند</p></div>
<div class="m2"><p>عشقش به دل در آمد و حالی علم بزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هی نیزهٔ ستیزه که مریخ راست کرد</p></div>
<div class="m2"><p>شمشیر خوی او همه را چون قلم بزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد بار چین طرهٔ پستش ز بوی مشک</p></div>
<div class="m2"><p>بر دست باد قافلهٔ صبح دم بزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آیینهٔ دو عارض او از شعاع نور</p></div>
<div class="m2"><p>بسیار سنگ طعنه که بر جام جم بزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم که: بر دلم نکند جور و هم بکرد</p></div>
<div class="m2"><p>گفتا: بر اوحدی نزنم زخم و هم بزد</p></div></div>