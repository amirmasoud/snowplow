---
title: >-
    غزل شمارهٔ ۱۹۷
---
# غزل شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>جز لبم شرح میان او نکرد</p></div>
<div class="m2"><p>جز دلم وصف دهان او نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی اقبالی ندید آن سر، که زود</p></div>
<div class="m2"><p>جای خود بر آستان او نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راز دل زان فاش می‌گردد، که دوست</p></div>
<div class="m2"><p>چارهٔ درد نهان او نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که قتل ما بدید آگاه شد:</p></div>
<div class="m2"><p>کان به جز تیر و کمان او نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه سر در پای عشق او نباخت</p></div>
<div class="m2"><p>دست وصل اندر میان او نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاطر آشفتهٔ ما کی کند؟</p></div>
<div class="m2"><p>عشرتی کندر زمان او نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر که نالد اوحدی زین پس، که دوست</p></div>
<div class="m2"><p>گوش بر آه و فغان او نکرد</p></div></div>