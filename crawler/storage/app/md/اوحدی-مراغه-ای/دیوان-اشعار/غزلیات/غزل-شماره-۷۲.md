---
title: >-
    غزل شمارهٔ ۷۲
---
# غزل شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>گرچه صد بارم برانند از برت</p></div>
<div class="m2"><p>بر نمی‌دارم سر از خاک درت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ابد منظور جانی، زانکه دل</p></div>
<div class="m2"><p>در ازل کرد این نظر بر منظرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد از سر تو ز آن رو غافلست</p></div>
<div class="m2"><p>کو نمی‌بیند به محراب اندرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر صباحی تازه گردد جان ما</p></div>
<div class="m2"><p>از نسیم طرهٔ جان پرورت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو جان وصل تو ما را در خورست</p></div>
<div class="m2"><p>گر چه جان ما نباشد در خورت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه بود اندر سر کار تو شد</p></div>
<div class="m2"><p>خود به چیزی در نمی‌آید سرت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیر گیران پلنگ انداز را</p></div>
<div class="m2"><p>کرد عاجز پنجهٔ زور آورت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر نگیرد سر ز خط امر تو</p></div>
<div class="m2"><p>هر که شد چون اوحدی فرمان برت</p></div></div>