---
title: >-
    غزل شمارهٔ ۱۸۵
---
# غزل شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>موی فشانم دگر عشق به درها ببرد</p></div>
<div class="m2"><p>در همه عالم ز من ناله خبرها ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی چو گلبرگ تو اشک مرا سیم کرد</p></div>
<div class="m2"><p>مطرب ما این نوا برزد و زرها ببرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ز سفرهای خود سود بسی داشتم</p></div>
<div class="m2"><p>عشق تو در باختن سود سفرها ببرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داشتم از شاخ عمر وعدهٔ برخوردنی</p></div>
<div class="m2"><p>باد فراقت به باغ بر زد و برها ببرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز نیاید به هوش عاشق رویت، که او</p></div>
<div class="m2"><p>توش ز تنها ربود، هوش ز سرما ببرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلف تو دل برد و هست در پی جان، ای عجب!</p></div>
<div class="m2"><p>بار کجا می‌هلد، دوست، که خرها ببرد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داشت دلی اوحدی، نقد و دگر چیزها</p></div>
<div class="m2"><p>این دو بر آتش بسوخت عشق و دگرها ببرد</p></div></div>