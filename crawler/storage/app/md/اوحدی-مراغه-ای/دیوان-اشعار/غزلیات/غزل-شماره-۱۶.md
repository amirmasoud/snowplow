---
title: >-
    غزل شمارهٔ ۱۶
---
# غزل شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>مطرب، چو بر سماع تو کردیم گوش را</p></div>
<div class="m2"><p>راهی بزن، که ره بزند عقل و هوش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابریشمی بساز و ازین حلقه پنبه کن</p></div>
<div class="m2"><p>نقل حضور صوفی پشمینه پوش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جامی بیار، ساقی، از آن بادهای خام</p></div>
<div class="m2"><p>وز عکس او بسوز من نیم جوش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر لوح دل نقوش پریشان کشیده‌ایم</p></div>
<div class="m2"><p>جامی بده، که محو کنیم این نقوش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را به می بشوی، چنان کز صفای ما</p></div>
<div class="m2"><p>غیرت بود مشایخ طاعت فروش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر ما ملامت دگران از کدورتست</p></div>
<div class="m2"><p>صافی ملامتی نکند در نوش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با مدعی بگوی که: ما را مگوی وعظ</p></div>
<div class="m2"><p>کاگنده‌ایم سمع نصیحت نیوش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای باد صبح، نیک خراشیده خاطریم</p></div>
<div class="m2"><p>لطفی بکن، به دوست رسان این خروش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرمی کند به خلوت ما آن پری گذر</p></div>
<div class="m2"><p>بگذار تا گذار نباشد سروش را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد نوش ما چو زهر ز هجران او، ولی</p></div>
<div class="m2"><p>زهر آن چنان خوریم به یادش که نوش را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای اوحدی، بگوی سخن، تا بداندت</p></div>
<div class="m2"><p>دشمن، که بی‌بصر نشناسد خموش را</p></div></div>