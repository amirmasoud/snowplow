---
title: >-
    غزل شمارهٔ ۶۰۷
---
# غزل شمارهٔ ۶۰۷

<div class="b" id="bn1"><div class="m1"><p>این دلبران که می‌کشدم چشم مستشان</p></div>
<div class="m2"><p>کس را خبر نشد که، چه دیدم ز دستشان؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ما در بلا و غم و غصه بر گشاد</p></div>
<div class="m2"><p>آن کس که نقش زلف و لب و چهره بستشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خون کنند چون بنماییم حال دل</p></div>
<div class="m2"><p>گویند نیستمان خبر از حال و هستشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر شکست خاطر ما سعی می‌نمود</p></div>
<div class="m2"><p>یاری که چین زلف سیه می‌شکستشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دانهای خال نهادند گرد لب</p></div>
<div class="m2"><p>دیگر ز دام زلف شکاری نرستشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنها که تن به مهر سپارند و دل به عشق</p></div>
<div class="m2"><p>زینها مگر به مرگ بود باز رستشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پنجاه گونه بر دل ریشم جراحتست</p></div>
<div class="m2"><p>زان تیرها که بر جگر آمد ز شستشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر مهر و دوستی ننهند این گروه دل</p></div>
<div class="m2"><p>گویی چه دشمنیست که در دل نشستشان؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر پایشان نهم ز وفا بوسه بعد ازین</p></div>
<div class="m2"><p>زیرا که روی گفتم و خاطر بخستشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اینان بدین بلندی قد و جلال قدر</p></div>
<div class="m2"><p>کی باشد التفات بدین خاک پستشان؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما را ازین بتان مکن، ای اوحدی، جدا</p></div>
<div class="m2"><p>کایمان نیاورد به کسی بت پرستشان</p></div></div>