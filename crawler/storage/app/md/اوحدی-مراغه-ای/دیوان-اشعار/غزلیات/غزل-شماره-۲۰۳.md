---
title: >-
    غزل شمارهٔ ۲۰۳
---
# غزل شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>پیری که پریرم ز مناجات بر آورد</p></div>
<div class="m2"><p>دی مست و خرابم به خرابات برآورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک جرعه به ذات خود ازان بادهٔ صافی</p></div>
<div class="m2"><p>در داد که گرد از من و از ذات بر آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بتکده‌ای برد مرا مست و بدیدم</p></div>
<div class="m2"><p>رویی، که خروش از جگر لات بر آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید جبینی، که فروع رخش از دور</p></div>
<div class="m2"><p>چون شعله زد، آشوب ز ذرات بر آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون در شدم، آن قامت رعنا به قیامی</p></div>
<div class="m2"><p>دل را ز مقام و ز مقامات برآورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون جان رخ او دید، پس دست گزیدن</p></div>
<div class="m2"><p>انگشت شهادت به تحیات برآورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با اوحدی از راه کرامت سخنی گفت</p></div>
<div class="m2"><p>وز بحر دلش موج کرامات برآورد</p></div></div>