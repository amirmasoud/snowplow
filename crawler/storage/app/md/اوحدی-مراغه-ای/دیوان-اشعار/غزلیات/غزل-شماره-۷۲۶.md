---
title: >-
    غزل شمارهٔ ۷۲۶
---
# غزل شمارهٔ ۷۲۶

<div class="b" id="bn1"><div class="m1"><p>ای روشن از رخ تو زمین و زمان همه</p></div>
<div class="m2"><p>تاریک بی‌تو چشم همین و همان همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خود ترا به چشم یقین دیده عاشقان</p></div>
<div class="m2"><p>و افتاده از یقین خود اندر گمان همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مشتری به نقد، چو دلال، حسن تو</p></div>
<div class="m2"><p>زر برده و متاع تو اندر دکان همه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عالم از رخ تو نشانی شده پدید</p></div>
<div class="m2"><p>و افتاده عالمی ز پی آن نشان همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم تو عرضه کرده ز هر سو هزار ترک</p></div>
<div class="m2"><p>با ما نهاده تیر جفا در کمان همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدم که با تو ناله و فریاد سود نیست</p></div>
<div class="m2"><p>دادم به باد عشق تو سود و زیان همه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون غنچه در هوای تو یک بارگی دلیم</p></div>
<div class="m2"><p>چون بید نیستیم ز عشقت زبان همه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرد آشکار صورت خوبت هزار حسن</p></div>
<div class="m2"><p>و آن حسنها ز دیدهٔ صورت نهان همه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم ترا به کشتن ما تیغ بر کمر</p></div>
<div class="m2"><p>ما را به جستن تو کمر بر میان همه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر کارکرد قهر تو، دادیم سر ز دست</p></div>
<div class="m2"><p>ور یار گشت لطف تو، بریدم جان همه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از بس که پر شدم ز صفات کمال تو</p></div>
<div class="m2"><p>نزدیک شد که پر شود از من جهان همه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در عرض دیدن تو دل تنگ اوحدی</p></div>
<div class="m2"><p>خطی به خون نبشته و ما در ضمان همه</p></div></div>