---
title: >-
    غزل شمارهٔ ۶۵۳
---
# غزل شمارهٔ ۶۵۳

<div class="b" id="bn1"><div class="m1"><p>آن تیر غمزه را دل خلقی نشانه بین</p></div>
<div class="m2"><p>انگشت رنگ داده و انگشتوانه بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی سیاه چرده و زلف سیاه کار</p></div>
<div class="m2"><p>چشم سیاه تنگ خوش جاودانه بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در باغ عارضش ز برای شکار دل</p></div>
<div class="m2"><p>زلف چو دام بنگر و خال چو دانه بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با آن غرور و غفلت و خردی و بیخودی</p></div>
<div class="m2"><p>یک بوسه زو طلب کن و پنجه بهانه بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد میان لاغر آن خان نیکوان</p></div>
<div class="m2"><p>پیچیده دایم آن کمر تنگ خانه بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دست زلف هندوی او جور می‌برم</p></div>
<div class="m2"><p>بخت مرا نگه کن و حال زمانه بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرد اوحدی ز داغ غمم او هزار بار</p></div>
<div class="m2"><p>با آن دو دل حکایت مرد یگانه بین</p></div></div>