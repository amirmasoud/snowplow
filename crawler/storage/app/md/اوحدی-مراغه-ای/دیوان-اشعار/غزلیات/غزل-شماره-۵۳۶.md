---
title: >-
    غزل شمارهٔ ۵۳۶
---
# غزل شمارهٔ ۵۳۶

<div class="b" id="bn1"><div class="m1"><p>سخن بگوی چو من در سخن نمی‌باشم</p></div>
<div class="m2"><p>که در حضور تو با خویشتن نمی‌باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بوی پیرهنت بشنوم ز خود بروم</p></div>
<div class="m2"><p>چنان که گویی در پیرهن نمی‌باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به وقت دیدنت ار در دعا کنم تقصیر</p></div>
<div class="m2"><p>ز من مگیر، که آن لحظه من نمی‌باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا اگر چه بسی عیب هست، شکر کنم</p></div>
<div class="m2"><p>که در وفا چو تو پیمان‌شکن نمی‌باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم به شکل دهان تو زان سبب تنگست</p></div>
<div class="m2"><p>که هیچ بی‌سخن آن دهن نمی‌باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از برای تو گشتم مقیم، تا دانی</p></div>
<div class="m2"><p>که بر گزاف درین انجمن نمی‌باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به روز مردنم ار با جنازه خواهی بود</p></div>
<div class="m2"><p>در انتظار حنوط و کفن نمی‌باشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برای مصلحت ار گفتم: از تو سیر شدم</p></div>
<div class="m2"><p>از آن مرنج، که بر یک سخن نمی‌باشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر تو قصد تن و جان اوحدی داری</p></div>
<div class="m2"><p>بیا، که زنده بدین جان و تن نمی‌باشم</p></div></div>