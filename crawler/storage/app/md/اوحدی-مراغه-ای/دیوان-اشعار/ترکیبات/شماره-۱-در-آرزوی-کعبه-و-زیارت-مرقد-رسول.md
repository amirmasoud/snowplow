---
title: >-
    شمارهٔ ۱ - در آرزوی کعبه و زیارت مرقد رسول
---
# شمارهٔ ۱ - در آرزوی کعبه و زیارت مرقد رسول

<div class="b" id="bn1"><div class="m1"><p>هوس کعبه و آن منزل و آنجاست مرا</p></div>
<div class="m2"><p>آرزوی حرم مکه و بطحاست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل آهنگ حجازست و زهی یاری بخت</p></div>
<div class="m2"><p>گر یک آهنگ درین پرده شود راست مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرم از دایرهٔ صبر برون خواهد شد</p></div>
<div class="m2"><p>شاید ار بگسلم این بند که در پاست مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خیال حجر اسود و بوسیدن او</p></div>
<div class="m2"><p>آب زمزم همه در عین سویداست مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل من روشن از آنست که از روزن فکر</p></div>
<div class="m2"><p>ریگ آن بادیه در دیدهٔ بیناست مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر آتش سوزنده نشینم هردم</p></div>
<div class="m2"><p>از هوای دل آشفته که برخاست مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم از حلقهٔ آن خانه مبادا محروم</p></div>
<div class="m2"><p>کز جهان نیست جزین مرتبه درخواست مرا</p></div></div>
<div class="b2" id="bn8"><p>از هوی و هوس خویش جدا باش، ای دل</p>
<p>خاک آن خانه و آن خانه خدا باش، ای دل</p></div>
<div class="b" id="bn9"><div class="m1"><p>عمر بگذشت، ز تقصیر حذر باید کرد</p></div>
<div class="m2"><p>به در کعبهٔ اسلام گذر باید کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناگزیرست در آن بادیه از خشک لبی</p></div>
<div class="m2"><p>تکیه بر گریهٔ این دیدهٔ‌تر باید کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرد ریگی که از آن زیر قدمها ریزد</p></div>
<div class="m2"><p>سرمه‌وارش همه در دیدهٔ سر باید کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آب و نان و شتر و راحله تشویش دلست</p></div>
<div class="m2"><p>خورد آن مرحله از خون جگر باید کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روی چون در سفر کعبه کنند اهل سلوک</p></div>
<div class="m2"><p>از خود و هستی خود جمله سفر باید کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر تراشیدن و احرام گرفتن سهلست</p></div>
<div class="m2"><p>از سر این نخوت بیهوده بدر باید کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شرح احرام و وقوف و صفت رمی و طواف</p></div>
<div class="m2"><p>با دل خویش به تقریر دگر باید کرد</p></div></div>
<div class="b2" id="bn16"><p>هر دلی را که ز تحقیق سخن بویی هست</p>
<p>بشناسد که سخن را بجزین رویی هست</p></div>
<div class="b" id="bn17"><div class="m1"><p>یارب، امسال بدان رکن و مقامم برسان</p></div>
<div class="m2"><p>کام من دیدن کعبه است و به کامم برسان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دولت وصل تو هرچند که خاصست، دمی</p></div>
<div class="m2"><p>عام گردان و بدان دولت عامم برسان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جز به کام مدد و عون تو نتوان آمد</p></div>
<div class="m2"><p>راه عشق تو، بدان قوت و کامم برسان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صبرم از پای درآمد، تو مرا دست بگیر</p></div>
<div class="m2"><p>به سر تربت این صدر همامم برسان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون هلال ار بپسندی که بمانم ناقص</p></div>
<div class="m2"><p>به جمال رخ آن بدر تمامم برسان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هندوی آن درم، ار خواجه جوازی بدهد</p></div>
<div class="m2"><p>صبح بیرون برو روزست به شمامم برسان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر بدان روضه گذارت بود، ای باد صبا</p></div>
<div class="m2"><p>عرضه کن عجز و زمین بوس و سلامم برسان</p></div></div>
<div class="b2" id="bn24"><p>بوی آن خاک دمی گر برهاند ز عذاب</p>
<p>به نسیم خوش آن روضه در آییم ز خواب</p></div>
<div class="b" id="bn25"><div class="m1"><p>ای رخت قبلهٔ احرار بگردانیده</p></div>
<div class="m2"><p>شرک را گرد جهان خوار بگردانیده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سکهٔ شرع ترا قوت این دین درست</p></div>
<div class="m2"><p>بهر اقلیم چو دینار بگردانیده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کافران جمله ز شوق سر زلف تو کمر</p></div>
<div class="m2"><p>در میان بسته و زنار بگردانیده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روز هجرت به لعاب دهنش خصم ترا</p></div>
<div class="m2"><p>عنکبوتی ز در غار بگردانیده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سر عشقت دل عشاق به دست آورده</p></div>
<div class="m2"><p>دست قهرت سر اغیار بگردانیده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شوق دیدار تو دولاب فلک را هر شب</p></div>
<div class="m2"><p>ز آب این دیدهٔ بیدار بگردانیده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تحفه را هر سحری باد صبا از سر لطف</p></div>
<div class="m2"><p>بوی زلف تو به گلزار بگردانیده</p></div></div>
<div class="b2" id="bn32"><p>«انااملح» که حدیث تو در افواه انداخت</p>
<p>قصهٔ یوسف مصری همه در چاه انداخت</p></div>
<div class="b" id="bn33"><div class="m1"><p>بوی مشک از سر زلف تو به چین آوردند</p></div>
<div class="m2"><p>بت پرستان ختا روی به دین آوردند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن عروسست کمالت که سر انگشتان</p></div>
<div class="m2"><p>در قمر وصمت نقصان مبین آوردند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لشکر طرهٔ هندوی تو بر اهل ختا</p></div>
<div class="m2"><p>ای بسا صبح که از شام کمین آوردند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا حدیث تو نمود اهل معانی را روی</p></div>
<div class="m2"><p>رخنه در قیمت درهای ثمین آوردند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دلشان سخت و سیه چون حجراسود بود</p></div>
<div class="m2"><p>مردم مکه، که در مهر تو کین آوردند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خفتهٔ عشق تو هر روز فزون خواهد شد</p></div>
<div class="m2"><p>خود چنینست، نگویم که: چنین آوردند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برق دل گرم شد از غیرت و بگریست چو ابر</p></div>
<div class="m2"><p>اندر آن شب که یراق تو به زین آوردند</p></div></div>
<div class="b2" id="bn40"><p>سر معراج ترا هم تو توانی گفتن</p>
<p>در دمی بود و از آن دم تو توانی گفتن</p></div>
<div class="b" id="bn41"><div class="m1"><p>آن شب از هر چه به زیر فلک ماه بماند</p></div>
<div class="m2"><p>جز تو چیزی نشنیدیم که آگاه بماند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جبرییل ارچه در آن شب ز رفیقان تو بود</p></div>
<div class="m2"><p>حاصل آنست که در نیمهٔ آن راه بماند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون براق تو بدید آتش برق عظمت</p></div>
<div class="m2"><p>گشت حیران و در آن آخر بی‌کاه بماند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>داشت هر رقعه وجود تو ز کثرت رختی</p></div>
<div class="m2"><p>رخت آن رقعه چو پرداخته شد شاه بماند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آتشی در شجر اخضر هستی افتاد</p></div>
<div class="m2"><p>چون شجر سوخته شده «انی اناالله» بماند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>صبح با آن نفس سرد چو دیر آگه شد</p></div>
<div class="m2"><p>از شب وصل تو با گریه و با آه بماند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دیدنی‌ها همه دیدی و بگفتی به همه</p></div>
<div class="m2"><p>هر که باور نکند قول تو در چاه بماند</p></div></div>
<div class="b2" id="bn48"><p>آنچه در دین تو از امن و امان پیدا شد</p>
<p>نشنیدیم که در هیچ زمان پیدا شد</p></div>
<div class="b" id="bn49"><div class="m1"><p>سر ز برد یمن، ای برق یمان، بیرون آر</p></div>
<div class="m2"><p>دل کوته نظران را ز گمان بیرون آر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>علم صدق به ایوان فلکها برکش</p></div>
<div class="m2"><p>لشکر شرع به صحرای جهان بیرون آر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خار دریای دل ما ز فراق رخ تست</p></div>
<div class="m2"><p>دسته‌ای گل ز در روضهٔ جان بیرون آر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هر نشانی که تو داری همه دیدیم، کنون</p></div>
<div class="m2"><p>ز پس پرده رخ فتنه‌نشان بیرون آر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بی‌سخن‌های تو قلب دل ما زر نشود</p></div>
<div class="m2"><p>کیمیای سخن از درج دهان بیرون آر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بدعت از هر طرفی سر به میان برد، دگر</p></div>
<div class="m2"><p>تیغ اعجاز نبوت ز میان بیرون آر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ما ز کردار بد خویش ز جان در خطریم</p></div>
<div class="m2"><p>این خطر بنگر و آن خط امان بیرون آر</p></div></div>