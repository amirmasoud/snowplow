---
title: >-
    رباعی شمارهٔ ۱۶۳
---
# رباعی شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>چون دوست نماند دل و جانیم همه</p></div>
<div class="m2"><p>چون تن برود روح و روانیم همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر هیچ ندانیم برآییم به هیچ</p></div>
<div class="m2"><p>عین همه‌ایم، اگر بداینم همه</p></div></div>