---
title: >-
    رباعی شمارهٔ ۱۵۷
---
# رباعی شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>ای راه خلل ز چار قسمت بسته</p></div>
<div class="m2"><p>داننده ز روح نقش جسمت بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صندوق طلسم را همی مانی تو</p></div>
<div class="m2"><p>صد گنج گشاده در طلسمت بسته</p></div></div>