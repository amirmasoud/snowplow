---
title: >-
    رباعی شمارهٔ ۸۸
---
# رباعی شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>شد در پی اوباش چو ننگیش نبود</p></div>
<div class="m2"><p>در خوی و سرشت ساز و سنگیش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایشان چو شدند سیر و ترکش کردند</p></div>
<div class="m2"><p>آمد بر من ولیک رنگیش نبود</p></div></div>