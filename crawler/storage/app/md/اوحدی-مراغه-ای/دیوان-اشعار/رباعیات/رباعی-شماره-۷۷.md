---
title: >-
    رباعی شمارهٔ ۷۷
---
# رباعی شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>لعلت که پر از گوهر ناسفت آمد</p></div>
<div class="m2"><p>چون طاق دو ابروی تو بی‌جفت آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من عشق ترا نهفته بودم در دل</p></div>
<div class="m2"><p>چون کار به جان رسید در گفت آمد</p></div></div>