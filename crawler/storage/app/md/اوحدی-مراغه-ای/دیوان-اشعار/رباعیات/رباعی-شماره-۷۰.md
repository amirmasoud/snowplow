---
title: >-
    رباعی شمارهٔ ۷۰
---
# رباعی شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>خال زنخت تیر گناه اندازد</p></div>
<div class="m2"><p>رخت دل عاشقان به راه اندازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غیرت خالی، که بر آن نرگس تست</p></div>
<div class="m2"><p>بیمست که خویش را به چاه اندازد</p></div></div>