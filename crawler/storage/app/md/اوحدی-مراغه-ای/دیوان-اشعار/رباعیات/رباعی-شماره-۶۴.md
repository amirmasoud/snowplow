---
title: >-
    رباعی شمارهٔ ۶۴
---
# رباعی شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>خالت که به شیوه کار ده گیسو کرد</p></div>
<div class="m2"><p>عیش از دل غمدیده من یکسو کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زیر لبت سیاه کارانه نشست</p></div>
<div class="m2"><p>تا آن لب ساده دل ترا سوسو کرد</p></div></div>