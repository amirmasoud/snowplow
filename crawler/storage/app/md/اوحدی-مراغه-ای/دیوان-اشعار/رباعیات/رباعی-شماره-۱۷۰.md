---
title: >-
    رباعی شمارهٔ ۱۷۰
---
# رباعی شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>اقبال سعادت به ازینت بودی</p></div>
<div class="m2"><p>گر لذت علم و درد دینت بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردون بستی به گوش داریت کمر</p></div>
<div class="m2"><p>گر گوش به هر گوشه نشینت بودی</p></div></div>