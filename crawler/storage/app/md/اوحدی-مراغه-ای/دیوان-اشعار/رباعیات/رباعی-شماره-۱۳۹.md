---
title: >-
    رباعی شمارهٔ ۱۳۹
---
# رباعی شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>روی تو ز حسن لاف‌ها زد به جهان</p></div>
<div class="m2"><p>لعل تو ز لطف طعن‌ها زد در جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف تو چو افتادگیی عادت کرد</p></div>
<div class="m2"><p>بنگر که چگونه بر سر آمد ز میان؟</p></div></div>