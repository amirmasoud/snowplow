---
title: >-
    رباعی شمارهٔ ۹۹
---
# رباعی شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>دشمن گرو وصل ز من برد آخر</p></div>
<div class="m2"><p>او گشت بزرگ و من شدم خرد آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آورد به جان لب ترا از بوسه</p></div>
<div class="m2"><p>دندان به رخت تیز فرو برد آخر</p></div></div>