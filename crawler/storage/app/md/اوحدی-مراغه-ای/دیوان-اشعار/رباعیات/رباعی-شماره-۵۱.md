---
title: >-
    رباعی شمارهٔ ۵۱
---
# رباعی شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>صد را، رخت از هیچ الم زرد مباد!</p></div>
<div class="m2"><p>بر روی تو از هیچ غمی گرد مباد!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردیست بزرگ مرگ فرزند عزیز</p></div>
<div class="m2"><p>بر جان عزیزت دگر این درد مباد!</p></div></div>