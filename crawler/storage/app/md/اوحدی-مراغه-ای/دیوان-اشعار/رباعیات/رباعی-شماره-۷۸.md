---
title: >-
    رباعی شمارهٔ ۷۸
---
# رباعی شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>از نوش جهان نصیب من نیش آمد</p></div>
<div class="m2"><p>تیر اجلم بر جگر ریش آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوته سفری گزیده بودم، لیکن</p></div>
<div class="m2"><p>ز آنجا سفری دراز در پیش آمد</p></div></div>