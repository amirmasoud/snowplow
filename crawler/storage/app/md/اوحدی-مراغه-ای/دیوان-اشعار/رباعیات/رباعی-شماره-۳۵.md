---
title: >-
    رباعی شمارهٔ ۳۵
---
# رباعی شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>دل بندهٔ بوی عنبر آمیز گلست</p></div>
<div class="m2"><p>جان چاکر عارض دلاویز گلست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل که هزار خار کن بندهٔ اوست</p></div>
<div class="m2"><p>او نیز غلام خار سرتیز گلست</p></div></div>