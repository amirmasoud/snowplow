---
title: >-
    رباعی شمارهٔ ۱۵۹
---
# رباعی شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>ای چرخ ز مهر زیر میغت برده</p></div>
<div class="m2"><p>گیتی به ستم اجل، به تیغت برده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرورده به صد ناز جهانت اول</p></div>
<div class="m2"><p>و آخر ز جهان به صد دریغت برده</p></div></div>