---
title: >-
    رباعی شمارهٔ ۱۵۸
---
# رباعی شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>ای تن، دل خود به روی چون ماهش ده</p></div>
<div class="m2"><p>جانی داری، به لعل دلخواهش ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون جگرم برون شود، می‌خواهی</p></div>
<div class="m2"><p>ای دیده، تو مردمی کن و راهش ده</p></div></div>