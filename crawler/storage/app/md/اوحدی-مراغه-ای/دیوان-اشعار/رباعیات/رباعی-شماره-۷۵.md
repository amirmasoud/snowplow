---
title: >-
    رباعی شمارهٔ ۷۵
---
# رباعی شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>لب نیست که از مراغه پر خنده نشد</p></div>
<div class="m2"><p>آب قرقش دید و به جان بنده نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مردهٔ گور او عجب می‌دارم</p></div>
<div class="m2"><p>کز شهر برون رفت، چرا زنده نشد؟</p></div></div>