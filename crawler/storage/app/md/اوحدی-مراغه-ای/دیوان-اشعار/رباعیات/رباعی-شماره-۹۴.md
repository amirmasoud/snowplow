---
title: >-
    رباعی شمارهٔ ۹۴
---
# رباعی شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>دستارچه را دست تو در می‌باید</p></div>
<div class="m2"><p>از چشم من و لب تو تر می‌باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نتوان که چو دستارچه دستت بوسم</p></div>
<div class="m2"><p>زیراکه به دستارچه زر می‌باید</p></div></div>