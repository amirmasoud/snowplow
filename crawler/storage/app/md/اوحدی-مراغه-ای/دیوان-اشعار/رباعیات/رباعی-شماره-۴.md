---
title: >-
    رباعی شمارهٔ ۴
---
# رباعی شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ای میل دل من به جهان سوی لبت</p></div>
<div class="m2"><p>تنگ آمده دل ز تنگی خوی لبت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خال تو آخر دل ما چند خورد؟</p></div>
<div class="m2"><p>خون دل خویشتن ز پهلوی لبت</p></div></div>