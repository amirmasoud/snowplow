---
title: >-
    رباعی شمارهٔ ۳۸
---
# رباعی شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>زلفت چو شب و چهره چو روزی نیکوست</p></div>
<div class="m2"><p>من روز و شبت ز بهر آن دارم دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کو ز رخت روز و ز زلفت شب ساخت</p></div>
<div class="m2"><p>پیوسته نگهدار شب و روز تو اوست</p></div></div>