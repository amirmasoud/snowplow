---
title: >-
    رباعی شمارهٔ ۱۱۶
---
# رباعی شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>ای بدر فلک گرفته از رای تو رنگ</p></div>
<div class="m2"><p>لالای ترا ز بدر و از لل ننگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار تو عطای بدره باشد شب بزم</p></div>
<div class="m2"><p>شغل تو غزای بدر باشد گه جنگ</p></div></div>