---
title: >-
    رباعی شمارهٔ ۱۶۵
---
# رباعی شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>آب ار چه به هر گوشه کند جنبش و رای</p></div>
<div class="m2"><p>بر صحن سرایت به سر آمد، نه به پای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان که به گرد خویش بر میگردد</p></div>
<div class="m2"><p>از بزم تو خوب تر نمی‌بیند جای</p></div></div>