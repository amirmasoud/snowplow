---
title: >-
    رباعی شمارهٔ ۱۶۱
---
# رباعی شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>داریم ز قدت گلها راست همه</p></div>
<div class="m2"><p>دل ماندگیی چند که برجاست همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نیز که امروز ز ما کردی یاد</p></div>
<div class="m2"><p>تاثیر دعای سحر ماست همه</p></div></div>