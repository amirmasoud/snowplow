---
title: >-
    رباعی شمارهٔ ۱۸۲
---
# رباعی شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>در صورت آدم ار فرشتست تویی</p></div>
<div class="m2"><p>ور آدمی از روح سرشتست تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر می‌نبشتست درین دور کسی</p></div>
<div class="m2"><p>آن وحی خط و آنکه نبشتست تویی</p></div></div>