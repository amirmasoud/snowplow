---
title: >-
    رباعی شمارهٔ ۴۷
---
# رباعی شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>شد درد بر پای فلک فرسایت</p></div>
<div class="m2"><p>تا عرضه کند سختی خود بر رایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد طمع آنکه بگیری دستش</p></div>
<div class="m2"><p>ورنه چه سگست او که بگیرد پایت؟</p></div></div>