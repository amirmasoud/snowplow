---
title: >-
    رباعی شمارهٔ ۶
---
# رباعی شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>گر راست روی محرم جان سازندت</p></div>
<div class="m2"><p>ور کژ بروی ز دل بیندازندت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حلقهٔ عاشقان چو ابریشم چنگ</p></div>
<div class="m2"><p>تا راست نگردی تو بننوازندت</p></div></div>