---
title: >-
    رباعی شمارهٔ ۲۱
---
# رباعی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>خالی که به شیوه پای بست لب تست</p></div>
<div class="m2"><p>همچون دلم آشفته و مست لب تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار دلش خون مکن و روزی چند</p></div>
<div class="m2"><p>نیکو دارش، که زیر دست لب تست</p></div></div>