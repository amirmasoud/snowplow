---
title: >-
    رباعی شمارهٔ ۱۱۴
---
# رباعی شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>خالی داری بر لب چون قند، از مشک</p></div>
<div class="m2"><p>خطی داری بر رخ دلبند، از مشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ساعد خود نگار بستی یا خود</p></div>
<div class="m2"><p>بر ماهی سیمین زرهی چند از مشک؟</p></div></div>