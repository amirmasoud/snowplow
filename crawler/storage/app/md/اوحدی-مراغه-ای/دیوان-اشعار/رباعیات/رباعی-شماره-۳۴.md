---
title: >-
    رباعی شمارهٔ ۳۴
---
# رباعی شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>بر گوشهٔ چشم تو، که شوخ و شنگست</p></div>
<div class="m2"><p>آن خال تو دانی به کدامین رنگست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موریست که بر کنار بادام نشست</p></div>
<div class="m2"><p>پیداست که در لب تو شکر تنگست</p></div></div>