---
title: >-
    رباعی شمارهٔ ۱۳۷
---
# رباعی شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>تا کی ز میان؟ کناره سویی گیریم</p></div>
<div class="m2"><p>برخیز که راه جست و جویی گیریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سایهٔ زهد سرد بودن تا چند؟</p></div>
<div class="m2"><p>وقتست که آفتاب رویی گیریم</p></div></div>