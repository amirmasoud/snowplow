---
title: >-
    رباعی شمارهٔ ۱۵۱
---
# رباعی شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>ما را به سرای وصل خویش آری تو</p></div>
<div class="m2"><p>بر ما ز لب لعل شکر باری تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس پرده ز روی خویش برداری تو</p></div>
<div class="m2"><p>عاشق نشویم، پس چه پنداری تو؟</p></div></div>