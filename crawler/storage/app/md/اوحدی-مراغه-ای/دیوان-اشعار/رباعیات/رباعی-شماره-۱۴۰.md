---
title: >-
    رباعی شمارهٔ ۱۴۰
---
# رباعی شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>ای قاعدهٔ تو مشک در مو بستن</p></div>
<div class="m2"><p>پای دل ما به بند گیسو بستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر خواست و چو زر ندیدن گرهی</p></div>
<div class="m2"><p>در هم شدن و گره در ابرو بستن</p></div></div>