---
title: >-
    رباعی شمارهٔ ۹۲
---
# رباعی شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>جان از سر زلف دلپذیریت نرهد</p></div>
<div class="m2"><p>عقل ار خطر خط خطیرت نرهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گر به مثل زهرهٔ شیران دارد</p></div>
<div class="m2"><p>از نرگس مست شیر گیرت نرهد</p></div></div>