---
title: >-
    رباعی شمارهٔ ۳۳
---
# رباعی شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>ما را تو چنین ز دل بر آری نیکست</p></div>
<div class="m2"><p>وانگه بدو زلف خود سپاری نیکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلفت که به فتنه سر بر آورد چنان</p></div>
<div class="m2"><p>او را تو چنین فرو گذاری نیکست</p></div></div>