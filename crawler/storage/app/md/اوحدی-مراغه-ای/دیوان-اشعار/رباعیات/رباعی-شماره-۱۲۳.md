---
title: >-
    رباعی شمارهٔ ۱۲۳
---
# رباعی شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>امروز که گشت باغ رنگین از گل</p></div>
<div class="m2"><p>شد خاک چمن چو نافهٔ چین از گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشکفت به صحرا گل مشکین، نه شگفت</p></div>
<div class="m2"><p>گر ناله کند بلبل مسکین از گل</p></div></div>