---
title: >-
    رباعی شمارهٔ ۸۹
---
# رباعی شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>افسوس! که در عمر درازیم نبود</p></div>
<div class="m2"><p>خطی ز زمانهٔ مجازیم نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشاند مرا فلک به بازی در خاک</p></div>
<div class="m2"><p>هر چند که وقت خاک بازیم نبود</p></div></div>