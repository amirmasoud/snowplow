---
title: >-
    رباعی شمارهٔ ۴۰
---
# رباعی شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>ای آنکه ترا قوت هر بیشی هست</p></div>
<div class="m2"><p>بنگر به دلم، که اندکش ریشی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درویشم و دست حاجتی داشته پیش</p></div>
<div class="m2"><p>گر زانکه ترا فراغ درویشی هست</p></div></div>