---
title: >-
    رباعی شمارهٔ ۱۴۸
---
# رباعی شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>ای شیخ، گران جان چو تنندی منشین</p></div>
<div class="m2"><p>زین آب روان بگیر پندی، منشین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مست شدی از می صافی به قرق</p></div>
<div class="m2"><p>بر جان حریفان چو سهندی منشین</p></div></div>