---
title: >-
    رباعی شمارهٔ ۶۱
---
# رباعی شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>گل گفت: مهل، که باد بویم ببرد</p></div>
<div class="m2"><p>چون خاک به هر برزن و کویم ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با وصل من آن آب چو آتش مینوش</p></div>
<div class="m2"><p>زان پیش که آتش آبرویم ببرد</p></div></div>