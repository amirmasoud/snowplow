---
title: >-
    رباعی شمارهٔ ۹۰
---
# رباعی شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>از دست تو راضیم به آزردن خود</p></div>
<div class="m2"><p>در عشق تو قانعم به خون خوردن خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی که: ببینم آن دو دست به نگار</p></div>
<div class="m2"><p>مانند دو عنبرینه در گردن خود</p></div></div>