---
title: >-
    رباعی شمارهٔ ۵۴
---
# رباعی شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>خورشید که خاک ازو چو زر می‌گردد</p></div>
<div class="m2"><p>از شوق رخ تو دربه‌در می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک جرعه می صاف تو در صافی ریخت</p></div>
<div class="m2"><p>شد مست و درین میان به سر می‌گردد</p></div></div>