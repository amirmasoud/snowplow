---
title: >-
    رباعی شمارهٔ ۱۵۲
---
# رباعی شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>ای گشتهٔ تن من چو خیالی بی‌تو</p></div>
<div class="m2"><p>هجر تو مرا کرده به حالی بی‌تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ماه دو هفته، رفتی و هست مرا</p></div>
<div class="m2"><p>روزی چو شبی، شبی چو سالی بی‌تو</p></div></div>