---
title: >-
    رباعی شمارهٔ ۱۱۹
---
# رباعی شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>دیگر ز شراب شوق مستی، ای دل</p></div>
<div class="m2"><p>و آن توبه که داشتی شکستی، ای دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بادهٔ نیستی خراب افتادی</p></div>
<div class="m2"><p>تا باد چنین باد که هستی، ای دل</p></div></div>