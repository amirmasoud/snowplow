---
title: >-
    رباعی شمارهٔ ۵۶
---
# رباعی شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>شطرنج تو ما را به شط رنج سپرد</p></div>
<div class="m2"><p>لجلاج لجاج با تو نتواند برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسبی که تو از رقعه ربودی و فشرد</p></div>
<div class="m2"><p>از دست تو بیرون نکنندش بدو کرد</p></div></div>