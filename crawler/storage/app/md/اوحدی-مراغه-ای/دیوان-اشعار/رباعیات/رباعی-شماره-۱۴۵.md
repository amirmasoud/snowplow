---
title: >-
    رباعی شمارهٔ ۱۴۵
---
# رباعی شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>هر دم لحد تنگ بگرید بر من</p></div>
<div class="m2"><p>وین خاک به صد رنگ بگرید بر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سنگ نویسید به زاری حالم</p></div>
<div class="m2"><p>تا بشنود و سنگ بگرید بر من</p></div></div>