---
title: >-
    رباعی شمارهٔ ۱۳۳
---
# رباعی شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>هر شب ز غمت به خون بگرید چشمم</p></div>
<div class="m2"><p>ز اندازه و حد فزون بگرید چشمم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چشم منی همیشه ثابت، لیکن</p></div>
<div class="m2"><p>ترسم بروی تو، چون بگرید چشمم</p></div></div>