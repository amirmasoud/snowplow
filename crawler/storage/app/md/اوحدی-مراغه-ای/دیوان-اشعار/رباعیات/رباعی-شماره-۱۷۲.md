---
title: >-
    رباعی شمارهٔ ۱۷۲
---
# رباعی شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>آن زلف،که دارد از تو برخورداری</p></div>
<div class="m2"><p>مانندهٔ میغست که بر خورداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی برخورم از قامت چون سرو تو من</p></div>
<div class="m2"><p>کز هر طرفی هزار برخورداری</p></div></div>