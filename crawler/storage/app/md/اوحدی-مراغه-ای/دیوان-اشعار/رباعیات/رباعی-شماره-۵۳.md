---
title: >-
    رباعی شمارهٔ ۵۳
---
# رباعی شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>گل را، که صبا، مرغ‌صفت بال گشاد</p></div>
<div class="m2"><p>گفتی که: منجم ورق فال گشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون گربهٔ بید خوانش آراسته دید</p></div>
<div class="m2"><p>سر بر زد و بوی برد و چنگال گشاد</p></div></div>