---
title: >-
    رباعی شمارهٔ ۱۱۵
---
# رباعی شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>اطراف چمن ز مشک بوییست به برگ</p></div>
<div class="m2"><p>گلزار زمانه را نکوییست به برگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل را ز دو رویه کار با برگ و نواست</p></div>
<div class="m2"><p>آری همه کاری ز دو روییست به برگ</p></div></div>