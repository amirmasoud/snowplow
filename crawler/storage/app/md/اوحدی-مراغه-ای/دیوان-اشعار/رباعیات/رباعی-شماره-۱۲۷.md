---
title: >-
    رباعی شمارهٔ ۱۲۷
---
# رباعی شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>نی بی‌تو مرا قرار باشد یک دم</p></div>
<div class="m2"><p>نی سوی منت گذار باشد یک دم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گه که بخواندمت به کاری باشی</p></div>
<div class="m2"><p>پیداست که خود چه کار باشد یک دم؟</p></div></div>