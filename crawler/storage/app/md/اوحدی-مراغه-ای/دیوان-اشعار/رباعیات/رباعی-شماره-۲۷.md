---
title: >-
    رباعی شمارهٔ ۲۷
---
# رباعی شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>ابر آن نکند که این جلب زن کردست</p></div>
<div class="m2"><p>ببر آن نکند که این جلب زن کردست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنیاد مسلمانی ازو گشت خراب</p></div>
<div class="m2"><p>گبر آن نکند که این جلب زن کردست</p></div></div>