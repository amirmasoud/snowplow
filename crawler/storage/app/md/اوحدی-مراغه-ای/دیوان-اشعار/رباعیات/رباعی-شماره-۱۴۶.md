---
title: >-
    رباعی شمارهٔ ۱۴۶
---
# رباعی شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>ای مهر تو از جهان پذیرفتهٔ من</p></div>
<div class="m2"><p>مشتاق تو این دیدهٔ ناخفتهٔ من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند جهان ز گفتهٔ من پر شد</p></div>
<div class="m2"><p>اکنون به کمال میرسد گفتهٔ من</p></div></div>