---
title: >-
    رباعی شمارهٔ ۱۰۳
---
# رباعی شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>ای کرده مهندسانت از ساز سپهر</p></div>
<div class="m2"><p>از برج و ستاره گشته انباز سپهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکل تو فگنده از فلک تشت قمر</p></div>
<div class="m2"><p>نقش تو نهاده بر طبق راز سپهر</p></div></div>