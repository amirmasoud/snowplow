---
title: >-
    قصیدهٔ شمارهٔ ۳۸ - وله سترالله عیوبه
---
# قصیدهٔ شمارهٔ ۳۸ - وله سترالله عیوبه

<div class="b" id="bn1"><div class="m1"><p>هرگز به جان فرا نرسی بی‌فروتنی</p></div>
<div class="m2"><p>خواهی که او شوی تو، جدا گرد از منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار ! قصد کندن بیخ کسان مکن</p></div>
<div class="m2"><p>زیرا که بیخ خویشتنست آنکه می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیکی کن، ای پسر تو، که نیکی به روزگار</p></div>
<div class="m2"><p>سوی تو بازگردد، اگر در چه افگنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل در جهان مبند، که بی‌جرعه‌های زهر</p></div>
<div class="m2"><p>کس شربتی نمی‌خورد، از دست او، هنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امروز کار کن که جوانی و زورمند</p></div>
<div class="m2"><p>فردا کجا توان؟ که شوی پیر و منحنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی من و جمال من و ملک و مال من؟</p></div>
<div class="m2"><p>چندین هزار من که شد از قطره‌ای منی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر برفراشتی که : به زور تهمتنم</p></div>
<div class="m2"><p>ای زیردست آز، چه سود از تهمتنی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز با دل شکسته ترا کار زار نیست</p></div>
<div class="m2"><p>خود را نگاه دار، که بر قلب می‌زنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کردی کلاه کژ، که : کمر بسته‌ام به سیم</p></div>
<div class="m2"><p>ای سنگدل، چه سیم؟ که دربند آهنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر نیک بنگری،همه زندان روح تست</p></div>
<div class="m2"><p>چون کرم پیله، بر تن خود هرچه می‌تنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر مرهم تو بر دل مردم بمنتست</p></div>
<div class="m2"><p>بردار مرهمت، که نمک می‌پراگنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مشکل بزاید از تو بسی خیر، از آنکه تو</p></div>
<div class="m2"><p>چون مادر زمانه ز نیکی سترونی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از پند گفتن تو چه فرقست تا به نیش؟</p></div>
<div class="m2"><p>از بهر آنکه تیز ترا ز فرق سوزنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا برزنی به کیسهٔ بازاریان یکی</p></div>
<div class="m2"><p>روز دراز بر سر بازار و برزنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از بهر لقمه‌ای، که نهندت به کام در</p></div>
<div class="m2"><p>دیدم که : زخم‌دارتر از قعر هارونی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دانی حساب گندم خود جوبه جو ولی</p></div>
<div class="m2"><p>«الحمد» را درست ندانی، ز کودنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نادان به جز حکایت دنیا نمی‌کند</p></div>
<div class="m2"><p>ناچار خود حکایت دنیا کند دنی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای اوحدی، کسی بجزو نیست در جهان</p></div>
<div class="m2"><p>درویش باش، تا غم کارت خورد غنی</p></div></div>