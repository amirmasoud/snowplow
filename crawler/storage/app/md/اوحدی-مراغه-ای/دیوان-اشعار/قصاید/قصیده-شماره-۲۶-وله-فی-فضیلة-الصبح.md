---
title: >-
    قصیدهٔ شمارهٔ ۲۶ - وله فی فضیلة الصبح
---
# قصیدهٔ شمارهٔ ۲۶ - وله فی فضیلة الصبح

<div class="b" id="bn1"><div class="m1"><p>چشم صاحب دولتان بیدار باشد صبحدم</p></div>
<div class="m2"><p>عاشقان را نالهای زار باشد صبحدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن جماعت را که در سینه ز شوق آتش بود</p></div>
<div class="m2"><p>کارگاه سوز دل بر کار باشد صبحدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبحدم باید شدن در کوی او، کز شاخ وصل</p></div>
<div class="m2"><p>هر گلی کت بشکفد بی‌خار باشد صبحدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوی او بی‌زحمت ناجنس باشد صبح‌گاه</p></div>
<div class="m2"><p>راه او بی‌زحمت اغیار باشد صبحدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده بردار سعادت وقت صبح از روی و این</p></div>
<div class="m2"><p>آن تواند دید کو بیدار باشد صبحدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرده دل در خواب نوشینست و دولت در گذار</p></div>
<div class="m2"><p>شادمان آندل که دولتیار باشد صبحدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طالبان پرتو خورشید روی دوست را</p></div>
<div class="m2"><p>چشم بر در، روی بر دیوار باشد صبحدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنده‌داران شب امید را بر در گهش</p></div>
<div class="m2"><p>دیدها دریای گوهربار باشد صبحدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز اگر با عمرو و با زیدست رازی خلق را</p></div>
<div class="m2"><p>راز دل با خالق جبار باشد صبحدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زنده‌داران شب امید را بر درگهش</p></div>
<div class="m2"><p>دیدها دریای گوهر بار باشد صبحدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از در رحمت به دست آویزی «هل من سائل»؟</p></div>
<div class="m2"><p>سایلان را کوی حضرت بار باشد صبحدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر تو می‌خواهی که بگشاید در احسان او</p></div>
<div class="m2"><p>بر در او رفتنت ناچار باشد صبحدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرچه کمیابی کسی در صبحدم ناخفته، لیک</p></div>
<div class="m2"><p>حاضری زانخفتگان بیدار باشد صبحدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تیر آه دردمندان در کمینگاه دعا</p></div>
<div class="m2"><p>از کمان سینه‌ها طیار باشد صبحدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر شبت میگویم این و عقل میگوید: بلی</p></div>
<div class="m2"><p>پند گیرد خواجه، گر هشیار باشد صبحدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه در خوردن بود روز دراز او به سر</p></div>
<div class="m2"><p>خفته بگذارش، که بس بیمار باشد صبحدم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در شب شهوت گر از گل بستر و بالین کنی</p></div>
<div class="m2"><p>آنچنان بالین و بستر مار باشد صبحدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دست با هر کس که دادی در میان همچون کمر</p></div>
<div class="m2"><p>باز باید کرد، کان زنار باشد صبحدم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چرخ با صد دیده می‌بیند ترا جایی چنین</p></div>
<div class="m2"><p>آدمی را خود ز خفتن عار باشد صبحدم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اوحدی، گر زان شب بیچارگی خوفیت هست</p></div>
<div class="m2"><p>چارهٔ کار تو استغفار باشد صبحدم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قصهٔ بیدار شو، با خفته‌ای مردانه گو</p></div>
<div class="m2"><p>کین سخن با کاهلان دشوار باشد صبحدم</p></div></div>