---
title: >-
    قصیدهٔ شمارهٔ ۱ - له فی‌المناجات
---
# قصیدهٔ شمارهٔ ۱ - له فی‌المناجات

<div class="b" id="bn1"><div class="m1"><p>راه گم کردم، چه باشد گر به راه آری مرا؟</p></div>
<div class="m2"><p>رحمتی بر من کنی وندر پناه آری مرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌نهد هر ساعتی بر خاطرم باری چو کوه</p></div>
<div class="m2"><p>خوف آن ساعت که با روی چو کاه آری مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه باریکست و شب تاریک، پیش خود مگر</p></div>
<div class="m2"><p>با فروغ نور آن روی چوماه آری مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحمتی داری، که بر ذرات عالم تافتست</p></div>
<div class="m2"><p>با چنان رحمت عجب گر در گناه آری مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد جهان در چشم من چون چاه تاریک از فزع</p></div>
<div class="m2"><p>چشم آن دارم که بر بالای چاه آری مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دفتر کردارم آن ساعت که گویی: بازکن</p></div>
<div class="m2"><p>از خجالت پیش خود در آه‌آه آری مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که چون جوزا نمی‌بندم کمر در بندگی</p></div>
<div class="m2"><p>کی چو خورشید منور در کلاه آری مرا؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسب خیرم لاغرست و خنجر کردار کند</p></div>
<div class="m2"><p>آن نمی‌ارزم که در قلب سپاه آری مرا؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاف یکتایی زدم چندان، که زیر بار عجب</p></div>
<div class="m2"><p>بیم آنستم که با پشت دو تاه آری مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر زمان از شرم تقصیری که کردم در عمل</p></div>
<div class="m2"><p>همچو کشتی ز آب چشم اندر شناه آری مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاطرم تیره است و تدبیرم کژ و کارم تباه</p></div>
<div class="m2"><p>با چنین سرمایه کی در پیشگاه آری مرا؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر حدیث من به قدر جرم من خواهی نوشت</p></div>
<div class="m2"><p>همچو روی نامه با روی سیاه آری مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بندگی گرزین نمط باشد که کردم، اوحدی</p></div>
<div class="m2"><p>آه از آن ساعت که پیش تخت شاه آری مرا!</p></div></div>