---
title: >-
    قصیدهٔ شمارهٔ ۳۲ - وله روحه الله روحه
---
# قصیدهٔ شمارهٔ ۳۲ - وله روحه الله روحه

<div class="b" id="bn1"><div class="m1"><p>ای رنج ناکشیده، که میراث می‌خوری</p></div>
<div class="m2"><p>بنگر که: کیستی تو و مال که می‌بری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او جمع کرد و چون به نمی‌خورد ازو بماند</p></div>
<div class="m2"><p>دریاب کز تو باز نماند چو بگذری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم به دستگاه توانگر نمی‌شود</p></div>
<div class="m2"><p>درویش را چو دست بگیری توانگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از قوت و خرقه هرچه زیادت بود ترا</p></div>
<div class="m2"><p>با ایزدش معامله کن، گر مبصری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زر غول مرد باشد و زن غل گردنش</p></div>
<div class="m2"><p>در غل غول باشی، تا با زن و زری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوهر کشیست، ای پسر، این دهر بچه‌خوار</p></div>
<div class="m2"><p>برگیر ازو تو مهر و مگیرش به مادری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرزند بنده‌ایست، خدا را، غمش مخور</p></div>
<div class="m2"><p>کان نیستی که به ز خدا بنده پروری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر مقبلیست گنج سعادت از آن اوست</p></div>
<div class="m2"><p>ور مدبرست، رنج زیادت چه می‌بری؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای خواجه، ملک را که به دست تو داده‌اند</p></div>
<div class="m2"><p>قانون بد منه، که به کلی تو می‌خوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌عدل ملک دیر نماند، نگاه دار</p></div>
<div class="m2"><p>مال رعیت از ستم و جور لشکری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرد هوی مگرد، که گردد وبال تو</p></div>
<div class="m2"><p>گر خود به بال جعفر طیار می‌پری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دریای فتنه این هوس و آرزوی تست</p></div>
<div class="m2"><p>در موج او مرو، چو ندانی شناوری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این شست و شوی جبه و دستار تا به کی؟</p></div>
<div class="m2"><p>دست از جهان بشوی، که آنست گازری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرگز نباشدت به بد دیگران نظر</p></div>
<div class="m2"><p>در فعل خویشتن تو اگر نیک بنگری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پر سرمکش، که عاقبت از بهر کشتنت</p></div>
<div class="m2"><p>ناگه رسن دراز کند چرخ چنبری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جای خرد به مرتبه بالای چرخهاست</p></div>
<div class="m2"><p>رو با خرد نشین، که تو از چرخ برتری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بوجهل را ز کعبه به دوزخ کشید جهل</p></div>
<div class="m2"><p>پیش خرد نتیجهٔ جهلست کافری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ظلمت خلاف نور بود، زان کشید ابر</p></div>
<div class="m2"><p>شمشیر برق در رخ خورشید خاوری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صد جامهٔ سیاه بپوشی، چو خلق نیست</p></div>
<div class="m2"><p>گرد تو کس نگردد، اگر گاو عنبری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خوابت نگیرد، ار نبود همسر تو زن</p></div>
<div class="m2"><p>زان غسل واجبیست، که با زن برابری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاید که از تو دیو گریزان شود، مگوی :</p></div>
<div class="m2"><p>کز چشم ما برای چه پنهان شود پری؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گیرم که بعد ازین نکنی روی در گناه</p></div>
<div class="m2"><p>عذر گناه کرده، بگو: تا چه آوری؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از کار کرد خویش پشیمان شوی یقین</p></div>
<div class="m2"><p>روزی که کردگار کند با تو داوری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفتار اوحدی نبود بی‌حقیقتی</p></div>
<div class="m2"><p>قولش قبول کن، که به اقبال رهبری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر طالبی، فروغ بگیری ز آفتاب</p></div>
<div class="m2"><p>ور غالبی، دریغ نداری ز مشتری</p></div></div>