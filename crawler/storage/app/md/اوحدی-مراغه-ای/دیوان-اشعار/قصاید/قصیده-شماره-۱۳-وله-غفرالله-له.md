---
title: >-
    قصیدهٔ شمارهٔ ۱۳ - وله غفرالله‌له
---
# قصیدهٔ شمارهٔ ۱۳ - وله غفرالله‌له

<div class="b" id="bn1"><div class="m1"><p>چمن ز باد خزان زرد و زار خواهد ماند</p></div>
<div class="m2"><p>درخت گل همه بیبرگ و بار خواهد ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین دو هفته نثاری نبینی اندر باغ</p></div>
<div class="m2"><p>که آب و سبزه به زیر نثار خواهد ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه طبع طفل چمن مستقیم خواهد شد</p></div>
<div class="m2"><p>نه دست شاهد گل در نگار خواهد ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازین قیاس تو در آدمی نگر، کو نیز</p></div>
<div class="m2"><p>نه دیر و زود درین گیر و دار خواهد ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هر چه نام وجودی برو کنند اطلاق</p></div>
<div class="m2"><p>مکن قبول که جز کردگار خواهد ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پسر به درد پدر دردمند خواهد شد</p></div>
<div class="m2"><p>پدر به داغ پسر سوکوار خواهد ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین صفت ز برای چه بایدت پرورد؟</p></div>
<div class="m2"><p>تن عزیز، که در خاک خوار خواهد ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بکوش نیک وز کردار بد کناری گیر</p></div>
<div class="m2"><p>که کردهای خودت در کنار خواهد ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن حکایت آن زر شمار دنیا دوست</p></div>
<div class="m2"><p>که در فضیحت روز شمار خواهد ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر چه نیک بر آرد به شوخ چشمی نام</p></div>
<div class="m2"><p>چو نامه باز کند شرمسار خواهد ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه نوبهار و خزان بر سر هم آید! لیک</p></div>
<div class="m2"><p>نه آن خزان و نه این نوبهار خواهد ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو جز تواضع و جز طاعت اختیار مکن</p></div>
<div class="m2"><p>به دستت ار دوسه روز اختیار خواهد ماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به رونق گل این باع دل منه، زنهار!</p></div>
<div class="m2"><p>که گل سفر کند از باغ و خار خواهد ماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به بارنامهٔ دنیا مشو فریفته، کان</p></div>
<div class="m2"><p>نه دولتیست که بس پایدار خواهد ماند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو زور داری، افتادگان مسکین را</p></div>
<div class="m2"><p>بگیر دست، که دستت ز کار خواهد ماند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو اوحدی طلب نام کن درین گیتی</p></div>
<div class="m2"><p>که نام نیک ز ما یادگار خواهد ماند</p></div></div>