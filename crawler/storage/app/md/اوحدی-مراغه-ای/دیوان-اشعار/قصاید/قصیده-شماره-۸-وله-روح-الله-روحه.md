---
title: >-
    قصیدهٔ شمارهٔ ۸ - وله روح‌الله روحه
---
# قصیدهٔ شمارهٔ ۸ - وله روح‌الله روحه

<div class="b" id="bn1"><div class="m1"><p>بر آستان در او کسی که راهش هست</p></div>
<div class="m2"><p>قبول و منزلت آفتاب و ماهش هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به راستی سر ازین دامگاه دامن‌گیر</p></div>
<div class="m2"><p>کسی برد که ز توفیق او پناهش هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرت ز گوشهٔ دل خواهش محبت اوست</p></div>
<div class="m2"><p>یقین بدان که از آنگونه نیز خواهش هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه باک از آن که پراکنده حالتیم و روان؟</p></div>
<div class="m2"><p>اگر چنانکه به احوال ما نگاهش هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو با خدای خود ار می‌کنی معاملتی</p></div>
<div class="m2"><p>دلیر کن، که کریمست و دستگاهش هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گمان مرد ز گیتی اگر دوام و بقاست</p></div>
<div class="m2"><p>یقین بدان تو که: اندیشهٔ پناهش هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گاه عجز ضروریست عرض قصه، تو نیز</p></div>
<div class="m2"><p>به عجز قصهٔ خود عرض کن، که گاهش هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرچه لذت شیرین دهد، به ملک مناز</p></div>
<div class="m2"><p>که رخت خسروپرویز تاج و گاهش هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خواجه را اجل از ملک پنبه خواهد کرد</p></div>
<div class="m2"><p>چه اعتبار به پشمی که در کلاهش هست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به نان و آب تفاخر مکن، که حیوان نیز</p></div>
<div class="m2"><p>به هر طرف که نگه میکند گیاهش هست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر ز تیغ تو نفسی سپر نیندازد</p></div>
<div class="m2"><p>حذر کن از نفس او، که تیر آهش هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رونده، گو: قدم اینجا به احتیاط بنه</p></div>
<div class="m2"><p>که زیر هر قدمی چندگونه چاهش هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر گناه کند نیک مرد خیراندیش</p></div>
<div class="m2"><p>مترس، گو، زعقوبت، که عذرخواهش هست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مقدسا و خدایا، به حق راهروی</p></div>
<div class="m2"><p>که از هدایت خاص تو انتباهش هست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که روز بازپسین در گذار و رحمت کن</p></div>
<div class="m2"><p>بر آنکه جاه ندارد، برآنکه جاهش هست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به بوی لطف تو می‌آید اوحدی برتو</p></div>
<div class="m2"><p>اگرچه سخت مخوفست و پرگناهش هست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرش به تیر بدوزی ورش به تیغ‌زنی</p></div>
<div class="m2"><p>ره گریز ندارد، که داغ شاهش هست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز کردهای خودش گرچه خوفهاست، ولی</p></div>
<div class="m2"><p>امید رحمت و آمرزش الهش هست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در آنزمان که تو بر نامهٔ سیه بخشی</p></div>
<div class="m2"><p>برو ببخش، که بس نامهٔ سیاهش هست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز خرمن عمل نیکش ارچه نیست جوی</p></div>
<div class="m2"><p>ز شرم بی‌عملی گونهٔ چو کاهش هست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر آتش دل وت گر گواه می‌خواهی</p></div>
<div class="m2"><p>ز گرمی نفس خویشتن گواهش هست</p></div></div>