---
title: >-
    قصیدهٔ شمارهٔ ۱۷ - فی‌الموعظة و تخلصه فی‌النعت و المناقب
---
# قصیدهٔ شمارهٔ ۱۷ - فی‌الموعظة و تخلصه فی‌النعت و المناقب

<div class="b" id="bn1"><div class="m1"><p>دوش از نسیم گل دم عنبر به من رسید</p></div>
<div class="m2"><p>وز نافه بوی زلف پیمبر به من رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل چون ز سر محرم اسرار انس شد</p></div>
<div class="m2"><p>آن سر سر بمهر مستر به من رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وهمم ز ریگ یثرب قربت چو برگذشت</p></div>
<div class="m2"><p>از ناف روضه نافهٔ اذفر به من رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوری، که در تصرف کس مدخلی نداشت</p></div>
<div class="m2"><p>در صورت روان مصور به من رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل را به لب رسید ز غم جان و عاقبت</p></div>
<div class="m2"><p>جان در میان نهادم و دلبر به من رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از من جدا شد و چو من از من جدا شدم</p></div>
<div class="m2"><p>از دیگران جدا شد و دیگر به من رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برقدم آن قبا، که قدر راست کرده بود</p></div>
<div class="m2"><p>قادر نظر بکرد و مقدر به من رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دست ساقیی، که از آن دست کس ندید</p></div>
<div class="m2"><p>جامی از آن طهور مطهر به من رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نامم رواست، گر چو خضر، جاودان بود</p></div>
<div class="m2"><p>زیرا که آرزوی سکندر به من رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با من به جنگ بود جهانی و من به لطف</p></div>
<div class="m2"><p>از داوری گذشتم و داور به من رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون بی‌سبب خلیفه نسب بودم، از قدیم</p></div>
<div class="m2"><p>تخت سخن گرفتم و افسر به من رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در قلب‌گاه نطق چو کردم دلاوری</p></div>
<div class="m2"><p>میر سپاه گشتم و لشکر به من رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر کس نصیبه‌ای ز تر و خشک روزگار</p></div>
<div class="m2"><p>برداشتند و این سخن تر به من رسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در صدر نطق حاجب دیوان منم، که من</p></div>
<div class="m2"><p>قانون درست کردم و دفتر به من رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دست خرد چو نقل سخن را نصیب کرد</p></div>
<div class="m2"><p>خصمم گرفت پسته و شکر به من رسید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غواص بحر فکر منم ورنه از کجا</p></div>
<div class="m2"><p>چندین هزار دانهٔ گوهر به من رسید؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با این پیادگی، که تو بینی، کم از زنم</p></div>
<div class="m2"><p>گر اسب هیچ مرد دلاور به من رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این نیست جز نتیجهٔ زاری وزانکه من</p></div>
<div class="m2"><p>زوری نیازمودم و بی‌زر به من رسید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از اوحدی شنو که: به چل سال پیش ازو</p></div>
<div class="m2"><p>این بخشش از محمد و حیدر به من رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صد خرمن تمام ببخشیدم از کرم</p></div>
<div class="m2"><p>وز هیچ کس جوی، خجلم، گر به من رسید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از علت ضلال دلم تن درست شد</p></div>
<div class="m2"><p>بی‌آنکه هیچ بوی مزور به من رسید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لوزینهٔ حدیثم از آن نغز طعم شد</p></div>
<div class="m2"><p>کز جوز نطق مغز مقشر به من رسید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سری که داد ناطقه با اوحدی قرار</p></div>
<div class="m2"><p>از کارگاه نطق مقرر به من رسید</p></div></div>