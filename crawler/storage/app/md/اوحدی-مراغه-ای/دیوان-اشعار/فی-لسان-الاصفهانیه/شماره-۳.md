---
title: >-
    شمارهٔ  ۳
---
# شمارهٔ  ۳

<div class="b" id="bn1"><div class="m1"><p>هردم ای دل بکمند غم یاری در کو</p></div>
<div class="m2"><p>دینه ویبو و بسوادی نگاری در کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر کر تو دل م کرد زنه ساده دلیر</p></div>
<div class="m2"><p>این ونینه و ترش کن که بکاری در کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پند گیر و دلم انبار لفتن زتن ار</p></div>
<div class="m2"><p>مرد عاقل بنبوتا که بکاری در کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مای شوخ ندارن ارد بخاره و خود</p></div>
<div class="m2"><p>محنتی بو که پیاده بسواری در کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نپسندد سر زلفش که کرو دل‌ها صید</p></div>
<div class="m2"><p>گرنه هر لحظه بان دام هزاری در کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ارجه رس کرشه لشین دل و کوشم در هو</p></div>
<div class="m2"><p>مکر ابروش و امن کوشه و ذاری در کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوحدی رنج بکر برت اگرت کامی کو</p></div>
<div class="m2"><p>عاشقی سعی بکر بو که شکاری در کو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دلت میل بان دیم کرو کردش کرت</p></div>
<div class="m2"><p>بو که ایرو و غلط بربکناری در کو</p></div></div>