---
title: >-
    شمارهٔ ۱۷۷
---
# شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>تا در خم آن گیسو چین و شکن افتاده</p></div>
<div class="m2"><p>بس بند و گره ز آن چین در کار من افتاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مسلک آزادی ما را نبود هادی</p></div>
<div class="m2"><p>جز آنکه در این وادی خونین کفن افتاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادم که در این عالم از حرص بنی آدم</p></div>
<div class="m2"><p>مسکین و غنی با هم اندر محن افتاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین شعله که پیدا نیست آنکس که نسوزد کیست</p></div>
<div class="m2"><p>این شور قیامت چیست در مرد و زن افتاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عالم مسکینی جان داده بشیرینی</p></div>
<div class="m2"><p>هر کشته که می بینی چون کوهکن افتاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از وادی عشق ای دل جان برده کسی مشکل؟</p></div>
<div class="m2"><p>زیرا که به هر منزل سرها ز تن افتاده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با ذوق سخنرانی گر نامه ما خوانی</p></div>
<div class="m2"><p>در جای سخن دانی در از دهن افتاده</p></div></div>