---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>همین بس است ز آزادگی نشانه ما</p></div>
<div class="m2"><p>که زیر بار فلک هم نرفته شانه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دست حادثه پامال شد به صد خواری</p></div>
<div class="m2"><p>هر آن سری که نشد خاک آستانه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان این همه مرغان بسته پر مائیم</p></div>
<div class="m2"><p>که داده جور تو بر باد آشیانه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار عقده چین را یک انقلاب گشود</p></div>
<div class="m2"><p>ولی به چین دو زلفت شکست شانه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر میان دو همسایه کشمکش نشود</p></div>
<div class="m2"><p>رود بنام گرو، بی قباله خانه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کنج دل ز غم دوست گنج‌ها داریم</p></div>
<div class="m2"><p>تهی مباد از این گنج‌ها خزانه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در این وکیل و وزیر ای خدا اثر نکند</p></div>
<div class="m2"><p>فغان صبحدم و ناله شبانه ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برای محو تو ای کشور خراب بس است</p></div>
<div class="m2"><p>همین نفاق که افتاده در میانه ما</p></div></div>