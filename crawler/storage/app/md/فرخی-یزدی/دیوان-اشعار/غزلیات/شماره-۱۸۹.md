---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>دل ز غم یک پرده خون شد پرده‌پوشی تا به کی؟</p></div>
<div class="m2"><p>جان ز تن با ناله بیرون شد خموشی تا به کی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خم از خونابه‌های دل دهان کف کرده است</p></div>
<div class="m2"><p>با همه افسردگی این گرم‌جوشی تا به کی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد بی‌درمان ز کوشش کی مداوا می‌کند</p></div>
<div class="m2"><p>ای طبیب چاره‌جو بیهوده‌کوشی تا به کی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیرو اشراف دادِ نوع‌خواهی می‌زند</p></div>
<div class="m2"><p>با سرشت دیو دعوی سروشی تا به کی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مفتخور را با زر ملت فروشی می‌خرید</p></div>
<div class="m2"><p>ای گروه مفتخر ملت‌فروشی تا به کی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنگ بی‌رنگی طلب کن ساده‌جویی تا به کی؟</p></div>
<div class="m2"><p>مست صهبای صفا شو باده‌نوشی تا به کی؟</p></div></div>