---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>آن غنچه که نشکفت ز حسرت دل ما بود</p></div>
<div class="m2"><p>وان عقده که نگشود ز غم مشکل ما بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجنون که به دیوانه گری شهره شهر است</p></div>
<div class="m2"><p>در دشت جنون همسفر عاقل ما بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دامن دل رنگ نبود از اثر خون</p></div>
<div class="m2"><p>معلوم نمی شد دل ما قاتل ما بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرسبز نگردید هر آن دانه که کشتیم</p></div>
<div class="m2"><p>پا بسته آفت زدگی حاصل ما بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردانه مه بود و جگر گوشه خورشید</p></div>
<div class="m2"><p>این شمع شب افروز که در محفل ما بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این سر که به دست غم هجر تو سپردیم</p></div>
<div class="m2"><p>در پای غمت هدیه ناقابل ما بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از راه صنم پی به صمد بردم و دیدم</p></div>
<div class="m2"><p>مستوره آئینه حق باطل ما بود</p></div></div>