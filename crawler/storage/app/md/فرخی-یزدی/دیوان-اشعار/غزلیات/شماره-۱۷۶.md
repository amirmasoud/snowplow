---
title: >-
    شمارهٔ ۱۷۶
---
# شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>ای توده دست قدرت از آستین برون کن</p></div>
<div class="m2"><p>وین کاخ جور و کین را تا پایه سرنگون کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از اشک و آه ای دل کی می بری تو حاصل</p></div>
<div class="m2"><p>از انقلاب کامل خود را غریق خون کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با صد زبان حقگو لب بند از هیاهو</p></div>
<div class="m2"><p>در پنجه غم او خود را چو من زبون کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون کوهکن به تمکین بسپار جان شیرین</p></div>
<div class="m2"><p>وز خون خویش رنگین دامان بیستون کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با فکر بکر عاقل آسان نگشت مشکل</p></div>
<div class="m2"><p>دیوانه وار منزل در وادی جنون کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در راه عشق یاری باری چو پا گذاری</p></div>
<div class="m2"><p>آن همتی که داری بر خویش رهنمون کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در انتظار آن گل فریاد کن چو بلبل</p></div>
<div class="m2"><p>آشفته زلف سنبل از اشک لاله‌گون کن</p></div></div>