---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>ما مست و خراب از می صهبای الستیم</p></div>
<div class="m2"><p>خمخانه تهی کرده و افتاده و مستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با طره دلبند تو کردیم چو پیوند</p></div>
<div class="m2"><p>پیوند ز هر محرم و بیگانه گسستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سبحه صد دانه ارباب ریا به</p></div>
<div class="m2"><p>صد مرتبه این رشته زنار که بستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرقی که میان من و شیخ است همین است</p></div>
<div class="m2"><p>کو دل شکند دایم و ما توبه شکستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دامن وصل از سر زلفت بکف آید</p></div>
<div class="m2"><p>چون شانه مشاطه سراپا همه دستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ناصح مشفق تو برو در غم خود باش</p></div>
<div class="m2"><p>ما گر بد و گر خوب همانیم که هستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون شاهد عیب و هنر ما عمل ماست</p></div>
<div class="m2"><p>گو خصم زند طعنه که ما دوست پرستیم</p></div></div>