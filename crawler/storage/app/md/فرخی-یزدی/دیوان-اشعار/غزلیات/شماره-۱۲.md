---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>غارت غارتگران شد مال بیت المال ما</p></div>
<div class="m2"><p>با چنین غارتگرانی وای بر احوال ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اذن غارت را به این غارتگران داده است سخت</p></div>
<div class="m2"><p>سستی و خون سردی و نادانی و اهمال ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد ما بهر استبداد و آزادی به جنگ</p></div>
<div class="m2"><p>تا چه سازد بخت او تا چون کند اقبال ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال ما یکچند دیگر گر بدینسان بگذرد</p></div>
<div class="m2"><p>بدتر از ماضی شود ایام استقبال ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیخ و شاب و شاه و شحنه و شبرو شدند</p></div>
<div class="m2"><p>متفق بر محو آزادی و استقلال ما</p></div></div>