---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>نارفیقان چون به یکرنگان دو رنگی می‌کنند</p></div>
<div class="m2"><p>از چه تفسیر دو رنگی را زرنگی می‌کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مقام صلح این قوم ار سپر انداختند</p></div>
<div class="m2"><p>تیغ بازی با سلحشوران جنگی می‌کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیو را خوانند همسنگ پری هنگام مهر</p></div>
<div class="m2"><p>روم را درگاه کین همرنگ زنگی می‌کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرض و طول ارض را از بهر خود خواهند و بس</p></div>
<div class="m2"><p>با همه روزی فراخی چشم تنگی می‌کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیر مردی را اگر بینند این روبه‌وَشان</p></div>
<div class="m2"><p>خرد با سرپنجه‌ای خوی پلنگی می‌کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نام آزادی برای خویش سازند انحصار</p></div>
<div class="m2"><p>بازی این رل را حریفان با قشنگی می‌کنند</p></div></div>