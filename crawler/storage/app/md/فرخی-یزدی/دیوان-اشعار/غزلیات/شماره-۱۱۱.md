---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>آنکه از آرا خریدن مسند عالی بگیرد</p></div>
<div class="m2"><p>مملکت را می فروشد تا که دلالی بگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ولایت را بغارت می دهد تا با جسارت</p></div>
<div class="m2"><p>تحفه از حاکم ستاند، رشوه از والی بگیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خیانت کور سازد آنکه چشم مملکت را</p></div>
<div class="m2"><p>چشم آن دارد ز ملت مزد کحالی بگیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی کرسی وکالت آنکه زد حرف از کسالت</p></div>
<div class="m2"><p>اجرت خمیازه خواهد، حق بیحالی بگیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تهی مغزی نماید کیسه بیگانه را پر</p></div>
<div class="m2"><p>تا به کف بهر گدائی، کاسه خالی بگیرد</p></div></div>