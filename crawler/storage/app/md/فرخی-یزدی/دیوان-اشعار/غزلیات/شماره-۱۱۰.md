---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>توده را با جنگ صنفی آشنا باید نمود</p></div>
<div class="m2"><p>کشمکش را بر سر فقر و غنا باید نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صف حزب فقیران اغنیا کردند جای</p></div>
<div class="m2"><p>این دو صف را کاملا از هم جدا باید نمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این بنای کهنه پوسیده ویران گشته است</p></div>
<div class="m2"><p>جای آن با طرح نو از نو بنا باید نمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا مگر عدل و تساوی در بشر مجری شود</p></div>
<div class="m2"><p>انقلابی سخت در دنیا به پا باید نمود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسکنت را محو باید کرد بین شیخ و شاب</p></div>
<div class="m2"><p>معدلت را شامل شاه و گدا باید نمود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از حصیر شیخ آید دم به دم بوی ریا</p></div>
<div class="m2"><p>چاره آن باریا و بوریا باید نمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرخی بی‌ترک جان گفتن در این ره پا منه</p></div>
<div class="m2"><p>زان که در اول قدم جان را فدا باید نمود</p></div></div>