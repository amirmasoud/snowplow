---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>خرم آن روزی که ما را جای در میخانه بود</p></div>
<div class="m2"><p>تا دل شب بوسه گاه ما لب پیمانه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقده های اهل دل را مو به مو می کرد باز</p></div>
<div class="m2"><p>در کف مشاطه باد صبا گر شانه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با من و مرغ بهشتی کی شود هم آشیان</p></div>
<div class="m2"><p>آن نظر تنگی که چشمش سوی آب و دانه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوخت از یک شعله آخر شمع را پا تا به سر</p></div>
<div class="m2"><p>برق آن آتش که در بال و پر پروانه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرق شهر و دشت از نقص جنون کی می گذاشت</p></div>
<div class="m2"><p>راستی مجنون اگر مانند من دیوانه بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خانه آباد ما را کرد در یک دم خراب</p></div>
<div class="m2"><p>جور و بیدادی که در این کشور ویرانه بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کرا از جنس این مردم گرفتم یار خویش</p></div>
<div class="m2"><p>دیدم از ناآشنایی محرم بیگانه بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزگار او را نسازد پست همچون فرخی</p></div>
<div class="m2"><p>هر که با طبع بلند و همت مردانه بود</p></div></div>