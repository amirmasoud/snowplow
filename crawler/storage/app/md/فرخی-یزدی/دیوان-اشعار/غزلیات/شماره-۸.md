---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>به هنگام سیه‌روزی علم کن قد مردی را</p></div>
<div class="m2"><p>ز خون سرخ‌فام خود بشوی این رنگ زردی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نصیب مردم دانا به جز خون جگر نبود</p></div>
<div class="m2"><p>در آن کشور که خلقش کرده عادت هرزه‌گردی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز لیدرهای جمعیت ندیدم غیر خودخواهی</p></div>
<div class="m2"><p>از آن با جبر کردم اختیار اقدام فردی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون تازم چنان بر این مبارزهای نالایق</p></div>
<div class="m2"><p>که تا بیرون کنند از سر هوای هم‌نبردی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی کز سوز دل شد برق آهم آسمان‌پیما</p></div>
<div class="m2"><p>چو بخت خود سیه کردم، سپهر لاجوردی را</p></div></div>