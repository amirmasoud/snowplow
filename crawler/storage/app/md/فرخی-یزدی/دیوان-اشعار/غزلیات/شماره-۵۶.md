---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>از ره داد ز بیدادگران باید کشت</p></div>
<div class="m2"><p>اهل بیدادگر این است و گران باید کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده ملک دریدند چو از پرده دری</p></div>
<div class="m2"><p>فاش و بی پرده از این پرده دران باید کشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه خوش پوشد و خوش نوشد و بیکار بود</p></div>
<div class="m2"><p>چون خورد حاصل رنج دگران باید کشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزمودیم وز ابناء بشر جز شر نیست</p></div>
<div class="m2"><p>خیرخواهانه از این جانوران باید کشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسکنت را از دم داس درو باید کرد</p></div>
<div class="m2"><p>فقر را با چکش کارگران باید کشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی خبر تا که بود از دل دهقان مالک</p></div>
<div class="m2"><p>خبر این است کز آن بی‌خبران باید کشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه گفتیم و نوشتیم چو آدم نشدند</p></div>
<div class="m2"><p>زین سپس اول از این گاو و خران باید کشت</p></div></div>