---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>پاسبان خفته این دار گر بیدار بود</p></div>
<div class="m2"><p>کی برای کیفر غارتگران بی‌دار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده دل تا نشد چاک از غمت پیدا نگشت</p></div>
<div class="m2"><p>کز پس یک پرده پنهان صدهزار اسرار بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناتوانی بین که درمان دل بیمار خویش</p></div>
<div class="m2"><p>جُستم از چشمی که آن هم از قَضا بیمار بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در شب غم آنکه دامان مرا از کف نداد</p></div>
<div class="m2"><p>با گواهی دادن دل دیده خونبار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست گوش حق‌نیوشی در خراب‌آباد ما</p></div>
<div class="m2"><p>ورنه از دست تو ما را شکوه بسیار بود</p></div></div>