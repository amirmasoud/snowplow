---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>ای دل اندر عاشقی با طالع مسعود باش</p></div>
<div class="m2"><p>چون بچنگ آری ایازی عاقبت محمود باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش این مردم تعین چون به موجودیت است</p></div>
<div class="m2"><p>گر رسد دستت، بهر قیمت بود، موجود باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نوازی دوستان را جنت شداد شو</p></div>
<div class="m2"><p>تا گدازی دشمنان را آتش نمرود باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش یکرنگان دورنگی چون نمی آید پسند</p></div>
<div class="m2"><p>یا چو یزدان پاک یا چون اهرمن مردود باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا درآئی در شمار کشتگان راه عشق</p></div>
<div class="m2"><p>با هزاران داغ دل چون لاله خون آلود باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش مردان خدا هرگز دم از هستی مزن</p></div>
<div class="m2"><p>نیستی را پیشه کن ناچیز شو نابود باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهرو ثابت قدم، هستی اگر چون فرخی</p></div>
<div class="m2"><p>در طلب با عزم ثابت طالب مقصود باش</p></div></div>