---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>«خیزید ز بیدادگران داد بگیرید</p></div>
<div class="m2"><p>وز دادستانان جهان یاد بگیرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دادستانی ره و رسم ار نشناسید</p></div>
<div class="m2"><p>در مدرسه این درس ز استاد بگیرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تیشه و از کوه گران یاد بیارید</p></div>
<div class="m2"><p>سرمشق در این کار ز فرهاد بگیرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فاسد شده خون در بدن عارف و عامی</p></div>
<div class="m2"><p>دستور حکیمانه ز فصاد بگیرید»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چند چو صیدید گرفتار دد و دام</p></div>
<div class="m2"><p>از دام برون آمده صیاد بگیرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضحاک عدو را به چکش مغز توان کوفت</p></div>
<div class="m2"><p>سرمشق گر از کاوه و حداد بگیرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آزادی ما تا نشود یکسره پامال</p></div>
<div class="m2"><p>در دست ز کین دشنه پولاد بگیرید</p></div></div>