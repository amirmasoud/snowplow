---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>چون ز شهر آن شاهد شیرین‌شمایل می‌رود</p></div>
<div class="m2"><p>در قفایش، کاروان در کاروان، دل می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو کز دنبال او وادی به وادی چشم رفت</p></div>
<div class="m2"><p>پیش‌پیشش اشک هم منزل به منزل می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل اگر دیوانه نبود الفتش با زلف چیست</p></div>
<div class="m2"><p>کی به پای خویش عاقل در سلاسل می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون به باطن در جهان نبود وجودی غیر حق</p></div>
<div class="m2"><p>حق بود آن هم که در ظاهر به باطل می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یارب این مقتول عشق از چیست کز راه وفا</p></div>
<div class="m2"><p>سر به کف بگرفته استقبال قاتل می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوی لیلی بس خطرناک است ز آنجا تا به حشر</p></div>
<div class="m2"><p>همچو مجنون بازگردد هرچه عاقل می‌رود</p></div></div>