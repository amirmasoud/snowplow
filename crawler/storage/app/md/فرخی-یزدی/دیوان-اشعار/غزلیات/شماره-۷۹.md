---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>در کهن ایران ویران انقلابی تازه باید</p></div>
<div class="m2"><p>سخت از این سست‌مردم قتل بی‌اندازه باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا مگر از زردرویی رخ بتابیم ای رفیقان</p></div>
<div class="m2"><p>چهره ما را ز خون سرخ دشمن غازه باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نام ما، در پیش دنیا پست از بی‌همتی شد</p></div>
<div class="m2"><p>غیرتی چون پور کیخسرو بلندآوازه باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌کند تهدید ما را این بنای ارتجاعی</p></div>
<div class="m2"><p>منهدم این کاخ را از صدر تا دروازه باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرخی از زندگانی تنگدل شد در جوانی</p></div>
<div class="m2"><p>دفتر عمرش به دست مرگ بی‌شیرازه باید</p></div></div>