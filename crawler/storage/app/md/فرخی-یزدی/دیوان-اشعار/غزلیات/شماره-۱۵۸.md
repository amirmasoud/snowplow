---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>ز بس از روزگار بخت و سخت و سست دلتنگم</p></div>
<div class="m2"><p>بسختی متصل با روزگار و بخت در جنگم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو رنگی چون پسند آید بچشم مردم دنیا</p></div>
<div class="m2"><p>بغیر از خون دل خوردن چه سازم من که یکرنگم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشم با این تهی دستی بلندی جویم از پستی</p></div>
<div class="m2"><p>نه در سر شور دیهیم و نه در دل مهر اورنگم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگو با عارف و عامی سپردم جان بناکامی</p></div>
<div class="m2"><p>گذشتم از نکو نامی کنون آماده ننگم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم آن مرغ دلخسته شکسته بال و پر بسته</p></div>
<div class="m2"><p>که دست آسمان دایم ز اختر می‌زند سنگم</p></div></div>