---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>زندگانی گر مرا عمری هراسان کرد و رفت</p></div>
<div class="m2"><p>مشکل ما را به مردن خوب آسان کرد و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جغد غم هم در دل ناشاد ما ساکن نشد</p></div>
<div class="m2"><p>آمد و این بوم را یکباره ویران کرد و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش مردم آشکارا چون مرا دیوانه ساخت</p></div>
<div class="m2"><p>روی خود را آن پری از دیده پنهان کرد و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانکرد از کار دل چون عقده باد مشکبوی</p></div>
<div class="m2"><p>گردشی در چین آن زلف پریشان کرد و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش از این‌ها در مسلمانی خدایی داشتم</p></div>
<div class="m2"><p>بت‌پرستم آن نگار نامسلمان کرد و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با رمیدن‌های وحشی آمد آن رعنا غزال</p></div>
<div class="m2"><p>فرخی را با غزل‌سازی غزل‌خوان کرد و رفت</p></div></div>