---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>شرط خوبی نیست تنها جان من گفتار خوب</p></div>
<div class="m2"><p>خوبی گفتار داری بایدت رفتار خوب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو را تعمیر این ویران عمارت لازم است</p></div>
<div class="m2"><p>باید از بهر مصالح آوری معمار خوب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بت‌پرست خوب به از خودپرست بدرفیق</p></div>
<div class="m2"><p>یار بد بدتر بود صد بار از اغیار خوب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوب دانی کیست پیش خوب و بد در روزگار</p></div>
<div class="m2"><p>آنکه می‌ماند ز کار خوب او آثار خوب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشته تسبیح سالوسی بد آمد در نظر</p></div>
<div class="m2"><p>زین سپس دست من و زلف تو و زنار خوب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نام آزادی ز بدکیشان نمی‌آمد به ننگ</p></div>
<div class="m2"><p>کشور ویران ما را بود اگر احرار خوب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار طوفان خوب گفتن نیست هر بیکاره را</p></div>
<div class="m2"><p>کار می‌خواهد ز اهل کار آن هم کار خوب</p></div></div>