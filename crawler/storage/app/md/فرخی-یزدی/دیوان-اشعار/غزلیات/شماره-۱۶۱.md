---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>در میکده گر رند قدح نوش نبودیم</p></div>
<div class="m2"><p>همچو خم می اینهمه در جوش نبودیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک صبح نشد شام که در میکده عشق</p></div>
<div class="m2"><p>از نشئه می بیخود و مدهوش نبودیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جور خزانیم زبان بسته وگرنه</p></div>
<div class="m2"><p>هنگام بهار این همه خاموش نبودیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک ذره اگر مهر و وفا داشتی ای مه</p></div>
<div class="m2"><p>از یاد تو اینگونه فراموش نبودیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تهمتنی شهره نگشتیم در آفاق</p></div>
<div class="m2"><p>گر کینه کش خون سیاووش نبودیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شمع سحر مردن ما بود مسلم</p></div>
<div class="m2"><p>گر زنده از آن صبح بناگوش نبودیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما پاکدلان را غم عشقت چو محک زد</p></div>
<div class="m2"><p>دانست چو سیم سره مغشوش نبودیم</p></div></div>