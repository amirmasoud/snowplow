---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه در طریق عدالت قدم زنیم</p></div>
<div class="m2"><p>با این مرام در همه عالم، علم زنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این شکل زندگی نبود قابل دوام</p></div>
<div class="m2"><p>خوب است این طریقه بد را به هم زنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قانون عادلانه‌تر از این کنیم وضع</p></div>
<div class="m2"><p>آنگاه بر تمام قوانین قلم زنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست صفا دهیم به معمار عدل و داد</p></div>
<div class="m2"><p>پا بر سر عوالم جور و ستم زنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون جنگ خلق بر سر دینار و درهم است</p></div>
<div class="m2"><p>باید به جای سکه چکش بر درم زنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دنیا چو شد بهشت برین زین تبدلات</p></div>
<div class="m2"><p>ما از نشاط طعنه به باغ ارم زنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را چو فرخی همه خوانند تندرو</p></div>
<div class="m2"><p>روزی گر از حقایق ناگفته دم زنیم</p></div></div>