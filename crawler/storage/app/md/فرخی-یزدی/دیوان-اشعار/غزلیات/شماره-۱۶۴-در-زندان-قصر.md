---
title: >-
    شمارهٔ ۱۶۴ - در زندان قصر
---
# شمارهٔ ۱۶۴ - در زندان قصر

<div class="b" id="bn1"><div class="m1"><p>ترسم ای مرگ نیایی تو و من پیر شوم</p></div>
<div class="m2"><p>وین قدر زنده بمانم که ز جان سیر شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمانا ز ره مهر مرا زود بکش</p></div>
<div class="m2"><p>که اگر دیر کشی پیر و زمینگیر شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوهرم هست و برش دارم و ماندم به غلاف</p></div>
<div class="m2"><p>چون نخواهم کج و خونریز چو شمشیر شوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میر میراث خوران هم نشوم تا گویم</p></div>
<div class="m2"><p>مردم از جور بمیرند که من میر شوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم آن کشتی طوفانی دریای وجود</p></div>
<div class="m2"><p>که ز امواج سیاست ز بر و زیر شوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوشه گیری اگرم از اثر اندازد به</p></div>
<div class="m2"><p>که من از راه خطا صاحب تأثیر شوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش دشمن سپر افکندن من هست محال</p></div>
<div class="m2"><p>در ره دوست گر آماجگه تیر شوم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم مخور ای دل دیوانه که از فیض جنون</p></div>
<div class="m2"><p>چون تو من هم پس از این لایق زنجیر شوم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهره شهرم و شهریه نگیرم چون شیخ</p></div>
<div class="m2"><p>که بر شحنه و شه کوچک و تحقیر شوم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کار در دوره ما جرم بود یا تقصیر</p></div>
<div class="m2"><p>فرخی بهر چه من عامل تقصیر شوم</p></div></div>