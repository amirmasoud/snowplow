---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>زد فصل گل چو خیمه به هامون جنون ما</p></div>
<div class="m2"><p>از داغ تازه سوخت دل لاله‌گون ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دم به خون دیده نشستیم تا کمر</p></div>
<div class="m2"><p>کان سنگدل ببست کمر را به خون ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما جز برای خیر بشر دم نمی‌زنیم</p></div>
<div class="m2"><p>این است یک نمونه ز راز درون ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بزم ما سخن ز خداوند و بنده نیست</p></div>
<div class="m2"><p>دون پیش ماست عالی و عالی‌ست دون ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را به سوی وادی دیوانگی کشید</p></div>
<div class="m2"><p>این عشق خیره‌سر که بود رهنمون ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقی ز بس که ریخت به ساغر شراب تلخ</p></div>
<div class="m2"><p>لبریز کرد کاسه صبر و سکون ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا روز مرگ از سر ما دست برنداشت</p></div>
<div class="m2"><p>بخت سیاه سوخته واژگون ما</p></div></div>