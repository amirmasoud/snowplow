---
title: >-
    شمارهٔ ۱۳۳ - درباره کمک به قحطی‌زدگان روسیهٔ بعد از انقلاب
---
# شمارهٔ ۱۳۳ - درباره کمک به قحطی‌زدگان روسیهٔ بعد از انقلاب

<div class="b" id="bn1"><div class="m1"><p>نمود همچو ابوالهول رو به ملت روس</p></div>
<div class="m2"><p>بلای قحط و غلا با قیافه منحوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتاد هیکل سنگین دیوپیکر قحط</p></div>
<div class="m2"><p>به روی قلب دهاقین روس چون کابوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر که دیو سپید است این بلای سیاه</p></div>
<div class="m2"><p>که کرده روسیه را مبتلا چو کیکاوس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی به ساحل ولگا ببین که ناله زار</p></div>
<div class="m2"><p>فشار گرسنگی را چه سان کند محسوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سان جوجه ز فقدان دانه بی‌جان بین</p></div>
<div class="m2"><p>تذرو کبک خرامی که بود چون طاوس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجا رواست شود، زرد رنگ چون خیری</p></div>
<div class="m2"><p>عذار سرخ نکویان همچو تاج خروس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی ز کثرت سختی ز عمر خود بیزاد</p></div>
<div class="m2"><p>یکی ز شدت قحطی ز زندگی مأیوس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آرزوی یکی دانه شام تا به سحر</p></div>
<div class="m2"><p>بود به سنبله چشم گرسنگان مأنوس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون که ملت روس است با مجاعه دوچار</p></div>
<div class="m2"><p>گه تهمتنی است ای سلاله سیروس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دستگیری قومی نما سرافرازی</p></div>
<div class="m2"><p>که می‌کنند اجل را به جان و دل پابوس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جوی ز گندم این سرزمین تواند داد</p></div>
<div class="m2"><p>ز چنگ مرگ رها جان صد هزار نفوس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نوشت خامه خونین «فرخی» این بیت</p></div>
<div class="m2"><p>به روی صفحه طوفان به صدهزار افسوس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جنوب بحر خزر شد ز اشک چشمه چشم</p></div>
<div class="m2"><p>برای ساحل رود نوا چو اقیانوس</p></div></div>