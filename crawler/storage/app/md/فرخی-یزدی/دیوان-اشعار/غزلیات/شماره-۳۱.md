---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>در چمن تا قد سرو تو برافراخته است</p></div>
<div class="m2"><p>روز و شب نوحه‌گری کار من و فاخته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برد با کهنه حریفی است که در بازی عشق</p></div>
<div class="m2"><p>هرچه را داشته چون من همه را باخته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگمان غلط آن ترک کمانکش چون تیر</p></div>
<div class="m2"><p>روزگاریست مرا از نظر انداخته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان من ز آه دل سوخته پرهیز نمای</p></div>
<div class="m2"><p>که بدین سوختگی کار مرا ساخته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستی چشم تو با ابروی کج عربده داشت</p></div>
<div class="m2"><p>یا پی کشتن من تیغ ستم آخته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنگ بر طره پرچین تو زد آنکه چو باد</p></div>
<div class="m2"><p>تا ختن از پی این مشک ختا تاخته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرخی دلخوش از آن است که این مردم را</p></div>
<div class="m2"><p>یک به یک دیده و سنجیده و بشناخته است</p></div></div>