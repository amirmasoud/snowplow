---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>ای دودهٔ طهمورث، دل یکدله باید کرد</p></div>
<div class="m2"><p>یک سلسله دیوان را در سلسله باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا این سر سودایی، از شور نیفتاده</p></div>
<div class="m2"><p>در راه طلب پا را، پُر آبله باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدبختیِ ما تنها از خارجه چون نَبْوَد</p></div>
<div class="m2"><p>هر شِکوِه که ما داریم از داخله باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با جامهٔ مُستَحفِظ در قافله دزدانند</p></div>
<div class="m2"><p>این راهزنان را طرد، از قافله باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهریمنِ استبداد، آزادیِ ما را کشت</p></div>
<div class="m2"><p>نه صبر و سکون جایز، نه حوصله باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مابینِ بشر شد سد، چون مسئلهٔ سرحد</p></div>
<div class="m2"><p>زین بعد ممالک را، بی‌فاصله باید کرد</p></div></div>