---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>شب چو در بستم و مست از مِیِ نابش کردم</p></div>
<div class="m2"><p>ماه اگر حلقه به در کوفت جوابش کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدی آن تُرکِ خَتا دشمن جان بود مرا</p></div>
<div class="m2"><p>گرچه عمری به خطا دوست خطابش کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منزلِ مردمِ بیگانه چو شد خانهٔ چشم</p></div>
<div class="m2"><p>آنقدر گریه نمودم که خرابش کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرحِ داغِ دلِ پروانه چو گفتم با شمع</p></div>
<div class="m2"><p>آتشی در دلش افکندم و آبش کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرقِ خون بود و نمی‌مرد ز حسرت فرهاد</p></div>
<div class="m2"><p>خواندم افسانهٔ شیرین و به خوابش کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل که خونابهٔ غم بود و جگرگوشهٔ درد</p></div>
<div class="m2"><p>بر سر آتشِ جورِ تو کبابش کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زندگی کردن من مردن تدریجی بود</p></div>
<div class="m2"><p>آنچه جان کَنْد تنم، عمر حسابش کردم</p></div></div>