---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>به کوی ناامیدی شمع‌آسا محفلی دارم</p></div>
<div class="m2"><p>ز اشک و آه خود در آب و آتش منزلی دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلا و محنت و رنج و پریشانی و درد و غم</p></div>
<div class="m2"><p>هزاران خرمن از کشت محبت حاصلی دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد از دارالشفای مرگ، درمان درد مهجوری</p></div>
<div class="m2"><p>برای درد خود زین پس علاج عاجلی دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو گل شد ز آب چشمم خاک کویت، از درم راندی</p></div>
<div class="m2"><p>نگفتی من در آنجا حق یک آب و گلی دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر عدلیه حکم تخلیت اول کند اجرا</p></div>
<div class="m2"><p>من بی‌خانمان آخر خدای عادلی دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو از بیداد گل می‌نالی و من از گل‌اندامی</p></div>
<div class="m2"><p>تو ای بلبل اگر داری دلی من هم دلی دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گره شد گریه از غم در گلوی فرخی آنسان</p></div>
<div class="m2"><p>که نتواند به آسانی بگوید مشکلی دارم</p></div></div>