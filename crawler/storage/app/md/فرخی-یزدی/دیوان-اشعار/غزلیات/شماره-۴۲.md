---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>عشقبازی را چه خوش فرهاد مسکین کرد و رفت</p></div>
<div class="m2"><p>جان شیرین را فدای جان شیرین کرد و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یادگاری در جهان از تیشه بهر خود گذاشت</p></div>
<div class="m2"><p>بیستون را گر ز خون خویش رنگین کرد و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیشب آن نامهربان مه آمد و از اشک شوق</p></div>
<div class="m2"><p>آسمان دامنم را پر ز پروین کرد و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش از این‌ها ای مسلمان داشتم دین و دلی</p></div>
<div class="m2"><p>آن بت کافر چنینم بی‌دل و دین کرد و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا شود آگه ز حال زار دل، باد صبا</p></div>
<div class="m2"><p>موبه‌مو گردش در آن گیسوی پرچین کرد و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وای بر آن مردم‌آزاری که در ده روز عمر</p></div>
<div class="m2"><p>آمد و خود را میان خلق ننگین کرد و رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این غزل را تا غزال مشک موی من شنید</p></div>
<div class="m2"><p>آمد و بر فرخی صد گونه تحسین کرد و رفت</p></div></div>