---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>این غرقه به خاک و خون دلی بود</p></div>
<div class="m2"><p>یا طایر نیم بسملی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست تو قطره قطره خون شد</p></div>
<div class="m2"><p>یک چند اگر مرا دلی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجنون که کناره جست زین خلق</p></div>
<div class="m2"><p>دیوانه نمای عاقلی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل داشت هوای دام صیاد</p></div>
<div class="m2"><p>پیداست که صید غافلی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز آنکه بکشت جان زد آتش</p></div>
<div class="m2"><p>از عشق مرا چه حاصلی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان داد شهید عشق و تا حشر</p></div>
<div class="m2"><p>شرمنده تیغ قاتلی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندیشه وصل هر چه کردم</p></div>
<div class="m2"><p>الحق که خیال باطلی بود</p></div></div>