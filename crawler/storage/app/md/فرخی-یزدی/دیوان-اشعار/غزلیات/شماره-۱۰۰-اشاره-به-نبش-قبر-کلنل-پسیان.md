---
title: >-
    شمارهٔ ۱۰۰ - اشاره به نبش قبر کلنل پسیان
---
# شمارهٔ ۱۰۰ - اشاره به نبش قبر کلنل پسیان

<div class="b" id="bn1"><div class="m1"><p>با من ای دوست ترا گر سر پرخاش نبود</p></div>
<div class="m2"><p>یار دشمن شدنت در همه جا فاش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پافشاری پی حق خود اگر ملت داشت</p></div>
<div class="m2"><p>مال او غارت یک دسته عیاش نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پول تصویبی مجلس نبد از ماه به ماه</p></div>
<div class="m2"><p>گرد آن کهنه حریف این همه کلاش نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی دولت قانونی اگر این باشد</p></div>
<div class="m2"><p>نامی از دولت و قانون به جهان کاش نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما طرفداری خورشید حقیقت کردیم</p></div>
<div class="m2"><p>آن زمانی که هما سخره خفاش نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با چنین زندگی آری به خدا می‌مردم</p></div>
<div class="m2"><p>اگر این جانی بی‌عاطفه نَبّاش نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر به نقادی کابینه نمی‌راند سخن</p></div>
<div class="m2"><p>خامه فرخی این قدر گهرپاش نبود</p></div></div>