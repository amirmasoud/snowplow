---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>از دست تو کس همچو من بی‌سروپا نیست</p></div>
<div class="m2"><p>گر هست چو من این همه انگشت‌نما نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود عقده خود را ز دل از گریه گشودم</p></div>
<div class="m2"><p>دیدم که کسی بهر کسی عقده گشا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از صفحه زنگاری افلاک شود محو</p></div>
<div class="m2"><p>هر نام که در دفتر ارباب وفا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زندان نفس یا قفس دل بودش نام</p></div>
<div class="m2"><p>هر سینه که آماجگه تیر بلا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دایره فقر قدم نه که در آن خط</p></div>
<div class="m2"><p>یک نقطه ترا فاصله با شاه و گدا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از راه صنم پی به صمد بردم و دیدم</p></div>
<div class="m2"><p>راهی به خدا نیست که آن ره به خدا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با منفعت صنفی خود فرخی امروز</p></div>
<div class="m2"><p>خود در صدد کشمکش فقر و غنا نیست</p></div></div>