---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>نیمه شب زلف را در سایه مه تاب دادی</p></div>
<div class="m2"><p>وز رخ چون آفتابت زینت مهتاب دادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم می آلوده را پیوستگی دادی به ابرو</p></div>
<div class="m2"><p>جای ترک مست را در گوشه محراب دادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابرویت را پر عرق کردی دگر از آتش می</p></div>
<div class="m2"><p>یا برای قتل ما شمشیر خود را آب دادی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون پرستاران نشاندی کنج لب خال سیه را</p></div>
<div class="m2"><p>هندوی پر تاب و تب را شیره عناب دادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده ام را تا قیامت روز و شب بیدار دارد</p></div>
<div class="m2"><p>وعده وصلی که از شوخی توام در خواب دادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا زدی ای لعبت چین شانه زلف عنبرین را</p></div>
<div class="m2"><p>در کف باد صبا صد نافه مشک ناب دادی</p></div></div>