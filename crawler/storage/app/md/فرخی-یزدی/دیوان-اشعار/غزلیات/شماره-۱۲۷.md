---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>حلقه زلفی که غیر تاب ندارد</p></div>
<div class="m2"><p>تا چه کند با دلی که تاب ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشمکش چین و اضطراب بشر چیست</p></div>
<div class="m2"><p>گیتی اگر حال انقلاب ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجلس ما را هر آنکه دید به دل گفت</p></div>
<div class="m2"><p>ملت جم، حسن انتخاب ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه خدایا به فکر خانه خود نیست</p></div>
<div class="m2"><p>یا خبر از خانه خراب ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواجه پی جمع مال و توده بدبخت</p></div>
<div class="m2"><p>هیچ بجز فکر نان و آب ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زور به پشت حساب مشت زد و گفت</p></div>
<div class="m2"><p>حرف حسابی دگر جواب ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرخی از زندگی خوش است به نانی</p></div>
<div class="m2"><p>گر نرسد آن هم، اضطراب ندارد</p></div></div>