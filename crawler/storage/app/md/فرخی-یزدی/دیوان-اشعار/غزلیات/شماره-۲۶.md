---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>با فکر تو موافق ناموس انقلاب</p></div>
<div class="m2"><p>باید زدن به دیر کهن کوس انقلاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دست من رسد ز سر شوق می روم</p></div>
<div class="m2"><p>تا خوابگاه مرگ به پابوس انقلاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بهر حفظ ملک گزرسس بیاورم</p></div>
<div class="m2"><p>در اهتزاز پرچم سیروس انقلاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون هزار زاغ بریزم به بوم خویش</p></div>
<div class="m2"><p>آید به جلوه باز چو طاوس انقلاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از انقلاب ناقص ما بود کاملا</p></div>
<div class="m2"><p>دیدیم اگر نتیجه معکوس انقلاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سالوس انقلابی ما اهل زرق بود</p></div>
<div class="m2"><p>یاران حذر کنید ز سالوس انقلاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طوفان خون پدید کند کلک فرخی</p></div>
<div class="m2"><p>آن سر بریده تا شده مأنوس انقلاب</p></div></div>