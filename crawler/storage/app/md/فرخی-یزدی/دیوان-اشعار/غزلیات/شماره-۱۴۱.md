---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>گرچه ما از دستبرد دشمنان افتاده‌ایم</p></div>
<div class="m2"><p>ما ز بهر جنگ از سر تا به پا آماده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طریق بندگی، روزی که بنهادیم پای</p></div>
<div class="m2"><p>بر خلاف نوع خواهی یک قدم ننهاده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افترایی گر به ما بستند ارباب ریا</p></div>
<div class="m2"><p>پیش وجدان راستی با جبهه بگشاده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلب ما تسخیر شد از مهر جمعی خودپرست</p></div>
<div class="m2"><p>آه از این بت‌ها که ما در قلب خود جا داده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیشه ما راستی، وین نادرستان حسود</p></div>
<div class="m2"><p>در پی تنقید ما کاندر سیاست ساده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این اسیری تا به کی، ای ملت بی‌دست و پای</p></div>
<div class="m2"><p>گر برای حفظ آزادی ز مادر زاده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرخی چندیست ما هم در پی صید عوام</p></div>
<div class="m2"><p>روز تا شب در خیال سبحه و سجاده‌ایم</p></div></div>