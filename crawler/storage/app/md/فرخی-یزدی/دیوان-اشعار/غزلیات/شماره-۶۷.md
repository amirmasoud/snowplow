---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>این ستمکاران که می‌خواهند سلطانی کنند</p></div>
<div class="m2"><p>عالمی را کشته تا یک دم هوسرانی کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه باقی مانده از دربار چنگیز و نِرُن</p></div>
<div class="m2"><p>بار بار آورده و سر بار ایرانی کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جشن و ماتم پیش ما باشد یکی چون بره را</p></div>
<div class="m2"><p>روزگار جشن و ماتم هردو قربانی کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز شادی نیست در شهری که از هر گوشه‌اش</p></div>
<div class="m2"><p>بی‌نوایان بهر نان هرشب نواخوانی کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به کی با پول این یک مشت خلق گرسنه</p></div>
<div class="m2"><p>صبح عید و عصر جشن و شب چراغانی کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با چنین نعمت که می‌بینند این مردم رواست</p></div>
<div class="m2"><p>شکرها تقدیم دربار بریتانی کنند</p></div></div>