---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>چون باد تا در آن خم گیسو درآمدیم</p></div>
<div class="m2"><p>با خون دل چو نافه آهو درآمدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با پای خسته در ره بی انتهای عشق</p></div>
<div class="m2"><p>رفتیم آنقدر که بزانو درآمدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامان پاک ما اگر آلوده شد ز می</p></div>
<div class="m2"><p>از آب توبه شکر که نیکو درآمدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی تو در برابر ما بود جلوه گر</p></div>
<div class="m2"><p>هر جا که رو نهاده و هر سو درآمدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را مکن ز ریشه که با خواری تمام</p></div>
<div class="m2"><p>در گلشن تو چون گل خودرو درآمدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کوی عشق غلغله ها بس بلند بود</p></div>
<div class="m2"><p>ما هم در آن میان به هیاهو درآمدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محراب و کعبه حاجت ما چون روا نکرد</p></div>
<div class="m2"><p>در قبله‌گاه آن خم ابرو درآمدیم</p></div></div>