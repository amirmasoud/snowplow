---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>گر ز روی معدلت آغشته در خون می‌شویم</p></div>
<div class="m2"><p>هرچه بادا باد ما تسلیم قانون می‌شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقلی چون در محیط ما بود دیوانگی</p></div>
<div class="m2"><p>زین سبب چندی خردمندانه مجنون می‌شویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لطمه ضحاک استبداد ما را خسته کرد</p></div>
<div class="m2"><p>با درفش کاویان روزی فریدون می‌شویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا به دشمن غالب از اقبال سعد آییم ما</p></div>
<div class="m2"><p>یا که مغلوب عدو از بخت وارون می‌شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا چه قارون در حضیض خاک بگزینیم جای</p></div>
<div class="m2"><p>یا چو عیسی مستقر بر اوج گردون می‌شویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طعم آزادی ز بس شیرین بود در کام جان</p></div>
<div class="m2"><p>بهر آن از خون خود فرهاد گلگون می‌شویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روح را مسموم سازد این هوای مرگبار</p></div>
<div class="m2"><p>زندگانی گر بود زین خطه بیرون می‌شویم</p></div></div>