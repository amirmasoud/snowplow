---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>کاخ جور تو گر از سیم بنائی دارد</p></div>
<div class="m2"><p>کلبه بی در ما نیز صفائی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو نی با دل سوراخ کند ناله ز سوز</p></div>
<div class="m2"><p>بینوائی که چو من شور و نوائی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غم عشق تو مردیم و ننالیم که مرد</p></div>
<div class="m2"><p>نکند ناله ز دردی که دوائی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پا نهد بر سر خوبان جهان شانه صفت</p></div>
<div class="m2"><p>هر که دست و هنر عقده گشائی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش ظلم در این خاک نگردد خاموش</p></div>
<div class="m2"><p>مهد زرتشت عجب آب و هوائی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به کام تو فلک دور زند غره مشو</p></div>
<div class="m2"><p>که جهان از پی هر سور عزائی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس چرا از ستم و جور چنین گشته خراب</p></div>
<div class="m2"><p>آخر این خانه اگر خانه خدائی دارد</p></div></div>