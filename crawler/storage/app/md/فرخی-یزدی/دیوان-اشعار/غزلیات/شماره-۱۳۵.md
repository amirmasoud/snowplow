---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>گر در طلب اهل دلی همدم ما باش</p></div>
<div class="m2"><p>سلطانی اگر می طلبی یار گدا باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در صدد خواجگی کون و مکانی</p></div>
<div class="m2"><p>با صدق و صفا بنده مردان خدا باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهی چو بر آن طره آشفته زنی چنگ</p></div>
<div class="m2"><p>چون شانه سراپا همه جا عقده گشا باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر مغبچه میکده ای شوخ ختا شو</p></div>
<div class="m2"><p>ور معتکف مدرسه ای شیخ ریا باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بدر درخشان شوی از سیر تکامل</p></div>
<div class="m2"><p>همچون مه نو لاغر و انگشت نما باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بادیه عشق اگر پای گذاری</p></div>
<div class="m2"><p>اول قدم آماده صد گونه بلا باش</p></div></div>