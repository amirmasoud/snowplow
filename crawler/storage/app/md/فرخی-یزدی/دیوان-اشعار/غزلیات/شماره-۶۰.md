---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>آنکه آتش برفروزد آه دل افروز ماست</p></div>
<div class="m2"><p>وآنکه عالم را بسوزد ناله جان سوز ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر ما پا مزن منعم که چندی بعد از این</p></div>
<div class="m2"><p>طایر اقبال و دولت مرغ دست آموز ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست جز انگشتری این گنبد فیروز رنگ</p></div>
<div class="m2"><p>گردشش آنهم به دست طالع فیروز ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نام مسکین و غنی روزی که محو و کهنه گشت</p></div>
<div class="m2"><p>با تساوی عموم آن روز نو، نوروز ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوک مژگان تو را با فرخی گفتم که چیست</p></div>
<div class="m2"><p>گفت این برگشته پیکان ناوک دلدوز ماست</p></div></div>