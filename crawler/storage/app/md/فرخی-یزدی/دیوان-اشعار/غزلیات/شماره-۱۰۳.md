---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>آن دسته که سرگشته سودای جنونند</p></div>
<div class="m2"><p>پا تا به سر از دایره عقل برونند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی که بود رهرو آزادی گیتی</p></div>
<div class="m2"><p>آنانکه در این بادیه آغشته بخونند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در محفل ما صحبتی از شاه و گدا نیست</p></div>
<div class="m2"><p>دانی همگی عالی و عالی همه دونند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با پنجه برآرند زبان از دهن شیر</p></div>
<div class="m2"><p>آنانکه ز سر پنجه تو زبونند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جویای وکالت ز موکل نبود کم</p></div>
<div class="m2"><p>این دوره جگر سوختگان بس که فزونند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جلوه طاوسی این خلق بترسید</p></div>
<div class="m2"><p>کز راه دورنگی همه چون بوقلمونند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون زاغ کشاندند سوی خانه‌خرابی</p></div>
<div class="m2"><p>این خانه‌خرابان که به ما راهنمونند</p></div></div>