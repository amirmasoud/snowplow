---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>ز خودآرایی تن جامه جان چاک می‌خواهم</p></div>
<div class="m2"><p>ز خون‌افشانی دل دیده را نمناک می‌خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از خونسردی نوباوگان کاوه پرخون شد</p></div>
<div class="m2"><p>شقاوت‌پیشه‌ای خونریز چون ضحاک می‌خواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو از بالا نشستن آبرومندی نشد حاصل</p></div>
<div class="m2"><p>نشیمن با گدای هم‌نشین خاک می‌خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در این بازی که طرح نو نماید رفع ناپاکی</p></div>
<div class="m2"><p>حریف کهنه کار پاکباز پاک می‌خواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رود از بس پی صید غزالان این دل وحشی</p></div>
<div class="m2"><p>به گیسوی تو او را بسته فتراک می‌خواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قفس از شش جهت شد تنگ در این خاکدان بر دل</p></div>
<div class="m2"><p>پری شایسته پرواز نه افلاک می‌خواهم</p></div></div>