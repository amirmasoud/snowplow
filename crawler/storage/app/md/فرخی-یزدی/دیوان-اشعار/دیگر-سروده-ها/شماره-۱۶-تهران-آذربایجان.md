---
title: >-
    شمارهٔ ۱۶ - تهران - آذربایجان
---
# شمارهٔ ۱۶ - تهران - آذربایجان

<div class="b" id="bn1"><div class="m1"><p>بود اگر تهران دمی در یاد آذربایجان</p></div>
<div class="m2"><p>بر فلک می رفت کی فریاد آذربایجان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک خود خواه خطر خیز ری بی آبروی</p></div>
<div class="m2"><p>داد بر باد فنا بنیاد آذربایجان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یسکر از بی اعتنائیهای تهران شد خراب</p></div>
<div class="m2"><p>خطه مینووش آباد آذربایجان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از فشار خارج و داخل زمانی شاد نیست</p></div>
<div class="m2"><p>خاطر غم دیده ناشاد آذربایجان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکری و سلدوز و سلماس و خوی و ساوجبلاغ</p></div>
<div class="m2"><p>سر بسر پامال شد ز اکراد آذربایجان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ارومی بانگ هل من ناصروینصر بلند</p></div>
<div class="m2"><p>کو معینی تا کند امداد آذربایجان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خصم خیره بخت تیره والی از اهمال سست</p></div>
<div class="m2"><p>سخت اندر زحمتند افراد آذربایجان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست رسم داد کز بیداد شخصی خودپرست</p></div>
<div class="m2"><p>کر شود گوش فلک از داد آذربایجان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی روا باشد به بند بندگی گردد اسیر</p></div>
<div class="m2"><p>ملت با غیرت آزاد آذربایجان</p></div></div>