---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>چون مرتجعین آلت نیرنگ شدند</p></div>
<div class="m2"><p>آزادی و ارتجاع در جنگ شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>القصه بنام حفظ اسلام ز کفر</p></div>
<div class="m2"><p>یک دسته ز روی سادگی رنگ شدند</p></div></div>