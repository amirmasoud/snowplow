---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>آن خم که بود مدام در جوش، منم</p></div>
<div class="m2"><p>آن مرغ که شد به شام خاموش، منم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حلقه رندان خراباتی خویش</p></div>
<div class="m2"><p>آن پاک نشین خانه بر دوش، منم</p></div></div>