---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>ثروت سبب وحی سماوی نشود</p></div>
<div class="m2"><p>با فقر و غنا قطع دعاوی نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نشود بین بشر ختم نزاع</p></div>
<div class="m2"><p>تا قیمت اوقات مساوی نشود</p></div></div>