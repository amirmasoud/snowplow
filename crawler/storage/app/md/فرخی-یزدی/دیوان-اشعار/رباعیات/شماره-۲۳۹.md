---
title: >-
    شمارهٔ ۲۳۹
---
# شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>تا چند به جور و ظلم تصمیم کنید</p></div>
<div class="m2"><p>در کیسه خویشتن زر و سیم کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر منفعتی که حاصل مملکت است</p></div>
<div class="m2"><p>خوبست که عادلانه تقسیم کنید</p></div></div>