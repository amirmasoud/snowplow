---
title: >-
    شمارهٔ ۳۳۰
---
# شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>ای دل شکن آتش به دل تنگ مزن</p></div>
<div class="m2"><p>بر شیشه ارباب وفا سنگ مزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دوست بپشت گرمی دشمن خویش</p></div>
<div class="m2"><p>بیهوده بروی دوستان چنگ مزن</p></div></div>