---
title: >-
    شمارهٔ ۲۲۷
---
# شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>گر ما و تو را دفع اعادی باید</p></div>
<div class="m2"><p>وز دشمن خود قطع ایادی باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خصم قوی به حالت صلح و صفا</p></div>
<div class="m2"><p>آماده جنگ اقتصادی باید</p></div></div>