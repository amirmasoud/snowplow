---
title: >-
    شمارهٔ ۲۶۵
---
# شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>تنها نه منم غمین برای دل خویش</p></div>
<div class="m2"><p>کس نیست که نیست مبتلای دل خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که تو شادکام می پنداری</p></div>
<div class="m2"><p>او داند و درد بی دوای دل خویش</p></div></div>