---
title: >-
    شمارهٔ ۳۶۷
---
# شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>آنانکه کنند با دو صد طنازی</p></div>
<div class="m2"><p>دایم به مقدرات ایران بازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش کنند وقت خود را مصرف</p></div>
<div class="m2"><p>یک لحظه به فابریک آدم سازی</p></div></div>