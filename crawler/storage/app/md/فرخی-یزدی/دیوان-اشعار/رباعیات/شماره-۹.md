---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>پیش همه منفعت اگر مطلوب است</p></div>
<div class="m2"><p>در نفع چرا این بد و آن یک خوب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودی که زیان ندارد از بهر عموم</p></div>
<div class="m2"><p>سودیست که جوینده آن محبوب است</p></div></div>