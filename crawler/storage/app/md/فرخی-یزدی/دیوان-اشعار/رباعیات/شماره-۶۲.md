---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>این زمزمه های شوم را قائل کیست</p></div>
<div class="m2"><p>واین نغمه ناپسند را حاصل چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گفتن حرف حق اثر هست اما</p></div>
<div class="m2"><p>گوینده چو با اراده باطل نیست</p></div></div>