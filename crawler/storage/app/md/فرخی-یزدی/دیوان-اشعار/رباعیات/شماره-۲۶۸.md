---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>از یک طرفی مجلس ما شیک و قشنگ</p></div>
<div class="m2"><p>از یک طرفی عرصه به ملیون تنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قانون و حکومت نظامی و فشار</p></div>
<div class="m2"><p>این است حکومت شتر گاو پلنگ</p></div></div>