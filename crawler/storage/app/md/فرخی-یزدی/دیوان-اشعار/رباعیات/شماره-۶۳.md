---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>ای کاهن خودپرست، معبود تو کیست</p></div>
<div class="m2"><p>وی خائن شوم پست، مقصود تو کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با ناز ایاز جلوه منما کاین مرد</p></div>
<div class="m2"><p>هر چند که احمد است محمود تو نیست</p></div></div>