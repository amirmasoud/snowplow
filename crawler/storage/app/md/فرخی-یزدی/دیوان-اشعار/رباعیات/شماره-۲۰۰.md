---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>ایکاش که جز رنگ صفا رنگ نبود</p></div>
<div class="m2"><p>مسکین ز غنی این همه دلتنگ نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بین بشر صلح و صفا داشت دوام</p></div>
<div class="m2"><p>سرمایه اگر مسبب جنگ نبود</p></div></div>