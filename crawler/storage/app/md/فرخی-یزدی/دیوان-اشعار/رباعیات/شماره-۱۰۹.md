---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>این چرخ برین که سرفرازی دارد</p></div>
<div class="m2"><p>بر جنس بشر دست درازی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با پرده دلفریب پر نقش و نگار</p></div>
<div class="m2"><p>یک لحظه دو صد هزار بازی دارد</p></div></div>