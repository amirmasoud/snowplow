---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>ای توده بی صدا خموشی نکنید</p></div>
<div class="m2"><p>بر پرد دریده پرده پوشی نکنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مرتجعین پول بگیرید ولیک</p></div>
<div class="m2"><p>در موقع رأی خودفروشی نکنید</p></div></div>