---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>آنانکه پریر با عدو یار شدند</p></div>
<div class="m2"><p>دیروز به اغیار مددکار شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آماده چو کردند سیه روزی ما</p></div>
<div class="m2"><p>امروز به روز ما گرفتار شدند</p></div></div>