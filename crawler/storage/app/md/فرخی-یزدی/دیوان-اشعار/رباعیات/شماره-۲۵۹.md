---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>با کجروی خلق جعلق خوش باش</p></div>
<div class="m2"><p>با کشمکش گنبد ازرق خوش باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی با سیه و سفید اگر خوش بودی</p></div>
<div class="m2"><p>امروز به کابینه ابلق خوش باش</p></div></div>