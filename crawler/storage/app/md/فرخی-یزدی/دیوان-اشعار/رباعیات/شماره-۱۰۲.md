---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>دردا که جهان به ما دل شاد نداد</p></div>
<div class="m2"><p>جز درس غم و محن به ما یاد نداد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای داد که آسمان ز بیدادگری</p></div>
<div class="m2"><p>با اینهمه داد ما به ما داد نداد</p></div></div>