---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>یا دوست دشمنند یا دشمن دوست</p></div>
<div class="m2"><p>از دست رها مکن چو من دامن دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرهیز نما ز دوستانی که ز جهل</p></div>
<div class="m2"><p>گر خوار شوی چو خار در گلشن دوست</p></div></div>