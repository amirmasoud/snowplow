---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>ما طعنه زن مقام مردی نشویم</p></div>
<div class="m2"><p>چون باد اسیر هرزه گردی نشویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اما نبود گناه در پیش عموم</p></div>
<div class="m2"><p>گر معتقد قدرت فردی نشویم</p></div></div>