---
title: >-
    شمارهٔ ۲۱۵
---
# شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>دولت چو بفکر خویش تشکیل شود</p></div>
<div class="m2"><p>ناچار نفوذ غیر تقلیل شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با فکر خودی اگر نگردد تشکیل</p></div>
<div class="m2"><p>بر آن نظر خارجه تحمیل شود</p></div></div>