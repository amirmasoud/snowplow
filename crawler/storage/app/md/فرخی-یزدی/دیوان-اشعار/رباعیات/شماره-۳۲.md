---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>قانون که اصول واجب التعظیم است</p></div>
<div class="m2"><p>ما را به اطاعتش سر تسلیم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوید که بنای زندگانی بشر</p></div>
<div class="m2"><p>بر روی قواعد امید و بیم است</p></div></div>