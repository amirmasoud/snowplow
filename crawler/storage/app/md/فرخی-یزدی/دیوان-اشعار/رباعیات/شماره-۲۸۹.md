---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>تا چند ز آه سینه دل چاک شوم</p></div>
<div class="m2"><p>تا کی ز سرشک دیده غمناک شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این آتش و آه و آب چشمم باقیست</p></div>
<div class="m2"><p>تا از اثر باد اجل خاک شوم</p></div></div>