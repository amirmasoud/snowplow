---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>یکچند به مرگ سخت جانی کردیم</p></div>
<div class="m2"><p>رخساره به سیلی ارغوانی کردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری گذراندیم به مردن مردن</p></div>
<div class="m2"><p>مردم به گمان که زندگانی کردیم</p></div></div>