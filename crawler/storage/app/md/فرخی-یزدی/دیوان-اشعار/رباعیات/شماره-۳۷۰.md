---
title: >-
    شمارهٔ ۳۷۰
---
# شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>زد چنگ زمانه چنگ بی‌تکلیفی</p></div>
<div class="m2"><p>شد باز شروع جنگ بی‌تکلیفی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای آه که آتیه این ملک خراب</p></div>
<div class="m2"><p>بگرفت دوباره زنگ بی‌تکلیفی</p></div></div>