---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>بس ناله جغد غم در این بوم آید</p></div>
<div class="m2"><p>نشگفت اگر فر هما شوم آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک لحظه اگر کسی کند باز دو گوش</p></div>
<div class="m2"><p>از چار طرف صدای مظلوم آید</p></div></div>