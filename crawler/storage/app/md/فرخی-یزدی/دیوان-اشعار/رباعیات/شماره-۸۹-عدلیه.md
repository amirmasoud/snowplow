---
title: >-
    شمارهٔ ۸۹ - عدلیه
---
# شمارهٔ ۸۹ - عدلیه

<div class="b" id="bn1"><div class="m1"><p>در کشور ما که جنگ اصنافی نیست</p></div>
<div class="m2"><p>حاکم به جز از اصول اشرافی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این است که بر خطای یک تن ناچار</p></div>
<div class="m2"><p>صد مدرک و درج ده سند کافی نیست</p></div></div>