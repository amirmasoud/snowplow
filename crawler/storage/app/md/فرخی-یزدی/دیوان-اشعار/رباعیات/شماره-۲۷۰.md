---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>کابینه اگر بود ز بحران تعطیل</p></div>
<div class="m2"><p>دیروز به مجلس آمد و شد تشکیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اما به رئیس الوزرا یک دو نفر</p></div>
<div class="m2"><p>آخر ز فشار وکلا شد تحمیل</p></div></div>