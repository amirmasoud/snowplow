---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>آن میر که جا در اطلس و قاقم کرد</p></div>
<div class="m2"><p>در جامعه خوش نامی خود را گم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی که بود به چشم مردم محبوب</p></div>
<div class="m2"><p>هرکس که نگاهداری از مردم کرد</p></div></div>