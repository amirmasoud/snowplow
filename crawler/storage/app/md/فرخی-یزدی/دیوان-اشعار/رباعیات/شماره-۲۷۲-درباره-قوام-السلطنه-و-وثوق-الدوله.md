---
title: >-
    شمارهٔ ۲۷۲ - درباره قوام‌السلطنه و وثوق‌الدوله
---
# شمارهٔ ۲۷۲ - درباره قوام‌السلطنه و وثوق‌الدوله

<div class="b" id="bn1"><div class="m1"><p>بدبختی ایران ز دو تن یافت دوام</p></div>
<div class="m2"><p>این نکته مسلم خواص است و عوام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دولت انگلیس را بود وثوق</p></div>
<div class="m2"><p>این سلطنت هنود را هست قوام</p></div></div>