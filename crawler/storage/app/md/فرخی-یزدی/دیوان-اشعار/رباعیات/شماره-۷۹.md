---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>در غمکده ای که شادیش جز غم نیست</p></div>
<div class="m2"><p>تنها نه همین خاطر ما خرم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر هر که نظر کنی گرفتار غم است</p></div>
<div class="m2"><p>گویا دل شاد در همه عالم نیست</p></div></div>