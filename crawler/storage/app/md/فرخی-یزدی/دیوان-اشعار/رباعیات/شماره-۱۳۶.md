---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>شادم که دل خراب ترمیم نشد</p></div>
<div class="m2"><p>در پیش امید و بیم تسلیم نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک صبح رهین نور امید نگشت</p></div>
<div class="m2"><p>یک شام غمین ظلمت و بیم نشد</p></div></div>