---
title: >-
    شمارهٔ ۳۷۷
---
# شمارهٔ ۳۷۷

<div class="b" id="bn1"><div class="m1"><p>ای جعبه مرا گوهر مقصود توئی</p></div>
<div class="m2"><p>اسباب زیان و مایه سود توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر منتظرالوکاله را ای صندوق</p></div>
<div class="m2"><p>تا رأی میان تست معبود توئی</p></div></div>