---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>گر سائس ملک با کیاست باشد</p></div>
<div class="m2"><p>دارای درایت و فراست باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مابین دو همسایه بباید ناچار</p></div>
<div class="m2"><p>مایل بتوازن سیاست باشد</p></div></div>