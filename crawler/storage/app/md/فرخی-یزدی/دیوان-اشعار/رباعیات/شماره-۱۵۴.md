---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>چون عیش و غم زمانه قسمت کردند</p></div>
<div class="m2"><p>ما را غم بیکرانه قسمت کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیخ و شه و شحنه عیش و نوش همه را</p></div>
<div class="m2"><p>بردند و برادرانه قسمت کردند</p></div></div>