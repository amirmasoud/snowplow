---
title: >-
    شمارهٔ ۳۱۴
---
# شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>برخیز که تا باده گلرنگ زنیم</p></div>
<div class="m2"><p>بنشین که بشور چنگ بر چنگ زنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دلشکنی کار ریاکاران است</p></div>
<div class="m2"><p>بر شیشه سالوس و ریا سنگ زنیم</p></div></div>