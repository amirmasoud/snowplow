---
title: >-
    شمارهٔ ۳۴۳
---
# شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>در مرز عجم ذلت ایرانی بین</p></div>
<div class="m2"><p>در ملک عرب محو مسلمانی بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دایم سر سروران اسلامی را</p></div>
<div class="m2"><p>پامال تجاوز بریتانی بین</p></div></div>