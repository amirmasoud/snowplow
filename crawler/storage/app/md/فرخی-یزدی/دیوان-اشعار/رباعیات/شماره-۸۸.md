---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>چون نامه ما برای کلاشی نیست</p></div>
<div class="m2"><p>چون خامه ما مرتشی از راشی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس پیشه ما هرزه درآئی نبود</p></div>
<div class="m2"><p>پس حرفه ما تهمت و فحاشی نیست</p></div></div>