---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>آن کس که مقام مستشاری دارد</p></div>
<div class="m2"><p>در مالیه اختصاص کاری دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راپورت ورا اگر بدقت خوانی</p></div>
<div class="m2"><p>بیش از همه چیز امیدواری دارد</p></div></div>