---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>آنانکه تو را دو سال یکبار خرند</p></div>
<div class="m2"><p>هر چند گران شوی بناچار خرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ارزان مفروش خویش را ای توده</p></div>
<div class="m2"><p>چون مردم کم فروش بسیار خرند</p></div></div>