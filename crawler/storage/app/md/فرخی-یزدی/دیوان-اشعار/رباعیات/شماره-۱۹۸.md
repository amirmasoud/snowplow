---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>گر شیخ ریا رند قدح نوش نبود</p></div>
<div class="m2"><p>گر شحنه شهر مست و مدهوش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک شمه ز بی مهری او می گفتم</p></div>
<div class="m2"><p>گر مهر مرا بر لب خاموش نبود</p></div></div>