---
title: >-
    شمارهٔ ۲۸۰
---
# شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>از روز ازل عاشقی آموخت دلم</p></div>
<div class="m2"><p>از عشق چو شمع شعله افروخت دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا خاک مرا دهد بباد آتش عشق</p></div>
<div class="m2"><p>از دیده نریخت آب تا سوخت دلم</p></div></div>