---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>در کشور ما که دزد را واهمه نیست</p></div>
<div class="m2"><p>جز گرگ شبان برای مشتی رمه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که مضار هست بهر همه است</p></div>
<div class="m2"><p>وانجا که منافع است مال همه نیست</p></div></div>