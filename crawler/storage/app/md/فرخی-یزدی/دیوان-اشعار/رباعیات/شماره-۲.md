---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>تن یافت برهنگی ز بی رختی ما</p></div>
<div class="m2"><p>دل تن بقضا داد ز جان سختی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دید غم و محنت ما را شب عید</p></div>
<div class="m2"><p>بگرفت عزای روز بدبختی ما</p></div></div>