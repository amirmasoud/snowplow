---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>آسوده در این دیر کهن نیست کسی</p></div>
<div class="m2"><p>بی درد و غم و رنج محن نیست کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاران شرکای موقع منفعتند</p></div>
<div class="m2"><p>هنگام ضرر شریک من نیست کسی</p></div></div>