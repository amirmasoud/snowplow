---
title: >-
    شمارهٔ ۳۲۱
---
# شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>ما قاعده متانت از کف ندهیم</p></div>
<div class="m2"><p>ما گوش به گفتار مزخرف ندهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با پند صحیح رفقا گاه مثال</p></div>
<div class="m2"><p>ما پاسخ هر ناقص و اجوف ندهیم</p></div></div>