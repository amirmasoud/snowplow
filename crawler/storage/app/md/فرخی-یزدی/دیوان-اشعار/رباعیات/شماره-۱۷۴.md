---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>هر کس می بی حقیقتی نوش کند</p></div>
<div class="m2"><p>هر قول که می دهد فراموش کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک رشته حقیقت آشکارا گفتم</p></div>
<div class="m2"><p>گر دولت ما به حرف حق گوش کند</p></div></div>