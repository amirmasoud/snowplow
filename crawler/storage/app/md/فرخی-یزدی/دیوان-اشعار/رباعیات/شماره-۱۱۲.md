---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>این خانه دگر چونی نوائی دارد</p></div>
<div class="m2"><p>وز راز درون بسر هوائی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکسان نبود وضع سیاست دایم</p></div>
<div class="m2"><p>هر روز سیاست اقتضائی دارد</p></div></div>