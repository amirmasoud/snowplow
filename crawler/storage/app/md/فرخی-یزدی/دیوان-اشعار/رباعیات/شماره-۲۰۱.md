---
title: >-
    شمارهٔ ۲۰۱
---
# شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>هر خانه که شادیش بجز غم نبود</p></div>
<div class="m2"><p>ویرانی آن خرابه پر کم نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش در و دیوار ندارد حاصل</p></div>
<div class="m2"><p>از بهر عمارتی که محکم نبود</p></div></div>