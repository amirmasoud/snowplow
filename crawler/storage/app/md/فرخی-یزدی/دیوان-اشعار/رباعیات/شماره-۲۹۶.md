---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>ما خاک به سر ز بی‌حسابی شده‌ایم</p></div>
<div class="m2"><p>ما دربه‌در از خانه‌خرابی شده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای صاحب مال و مالک کاخ جلال</p></div>
<div class="m2"><p>با ما منشین که انقلابی شده‌ایم</p></div></div>