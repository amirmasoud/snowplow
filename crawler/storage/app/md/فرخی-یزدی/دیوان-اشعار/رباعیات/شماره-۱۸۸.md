---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>آن را که نفوذ و اقتدارات بود</p></div>
<div class="m2"><p>در دست تمام اختیارات بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چیست ندانست که بدبختی ما</p></div>
<div class="m2"><p>یکسر ز خرابی ادارات بود</p></div></div>