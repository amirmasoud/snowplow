---
title: >-
    شمارهٔ ۳۶۶
---
# شمارهٔ ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>دیدی بخلاف عزم و تصمیم شدی</p></div>
<div class="m2"><p>از حمله ارتجاع در بیم شدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با اینهمه اظهار شهامت آخر</p></div>
<div class="m2"><p>در پیش قوای خصم تسلیم شدی</p></div></div>