---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>هر چند که پشت خم تخت من است</p></div>
<div class="m2"><p>در روی زمین برهنگی رخت من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با اینهمه جور چرخ و بی مهری ماه</p></div>
<div class="m2"><p>خورشید فلک ستاره بخت من است</p></div></div>