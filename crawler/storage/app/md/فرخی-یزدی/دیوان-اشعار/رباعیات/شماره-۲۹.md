---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>در کشور ما که مهد اندوه و غم است</p></div>
<div class="m2"><p>در آن دل و جان شاد بسیار کم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از همقدمان خود عقب خواهد ماند</p></div>
<div class="m2"><p>هر کس که درین زمانه ثابت قدم است</p></div></div>