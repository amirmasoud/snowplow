---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>ای توده گرفتار جهالت شده‌ای</p></div>
<div class="m2"><p>گم‌گشته وادی ضلالت شده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکس که کنی وکیل گر جنس تو نیست</p></div>
<div class="m2"><p>بیچون و چرا بدان که آلت شده‌ای</p></div></div>