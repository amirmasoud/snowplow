---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>آن رند دغل بازگه با مکر و حیل</p></div>
<div class="m2"><p>با لفظ قرارداد، می کرد جدل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدی که چسان عاقبت اندر مجلس</p></div>
<div class="m2"><p>بگرفت قرارداد، ناطق به بغل</p></div></div>