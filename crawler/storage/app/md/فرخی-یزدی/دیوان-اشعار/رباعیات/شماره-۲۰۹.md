---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>دیشب که به پای دل مرا سلسله بود</p></div>
<div class="m2"><p>از دست سر زلف تو ما را گله بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون موی تو عاقبت پریشانم کرد</p></div>
<div class="m2"><p>موئی که میان من و دل فاصله بود</p></div></div>