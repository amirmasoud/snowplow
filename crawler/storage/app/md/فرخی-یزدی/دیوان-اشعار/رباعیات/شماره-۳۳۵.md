---
title: >-
    شمارهٔ ۳۳۵
---
# شمارهٔ ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>ای توده عمل باهمم عالیه کن</p></div>
<div class="m2"><p>بگذشته گذشت صحبت از حالیه کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر علت ورشکستگی می خواهی</p></div>
<div class="m2"><p>چشمی بقرار بانک با مالیه کن</p></div></div>