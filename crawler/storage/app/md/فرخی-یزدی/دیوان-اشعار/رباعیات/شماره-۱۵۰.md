---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>آن سلسله ای که از امیران هستند</p></div>
<div class="m2"><p>معمار در این سرای ویران هستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چیست که با ثروت هنگفت مدام</p></div>
<div class="m2"><p>اندر صدد غارت ایران هستند</p></div></div>