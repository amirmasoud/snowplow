---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>جان بنده رنج و زحمت کارگر است</p></div>
<div class="m2"><p>دل غرقه به خون ز محنت کارگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دیده انصاف چو نیکو نگری</p></div>
<div class="m2"><p>آفاق رهین منت کارگر است</p></div></div>