---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>با پاک دلان پاک نهادی باید</p></div>
<div class="m2"><p>از مختلسین قطع ایادی باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا آنکه ز ورشکستگی باید مرد</p></div>
<div class="m2"><p>یا چاره فقر اقتصادی باید</p></div></div>