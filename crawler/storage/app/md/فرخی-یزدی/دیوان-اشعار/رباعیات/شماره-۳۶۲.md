---
title: >-
    شمارهٔ ۳۶۲
---
# شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>با زور و وبال تا وزارت کردی</p></div>
<div class="m2"><p>بس مال که از مالیه غارت کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد خانه خراب کردی ای خانه خراب</p></div>
<div class="m2"><p>تا کاخ بلند خود عمارت کردی</p></div></div>