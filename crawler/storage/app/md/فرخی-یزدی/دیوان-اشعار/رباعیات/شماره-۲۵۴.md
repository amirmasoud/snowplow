---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>گل نیست دلم که رنگ و بو خواهد و بس</p></div>
<div class="m2"><p>در باغ چو من نام نکو خواهد و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خاک نشینی نکند ناله و آه</p></div>
<div class="m2"><p>از دولت اشک آبرو خواهد و بس</p></div></div>