---
title: >-
    شمارهٔ ۳۵۲
---
# شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>در اول وهله پا فشردیم همه</p></div>
<div class="m2"><p>گوی سبق از زمانه بردیم همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تفرقه بگسیخته شد چون صف ما</p></div>
<div class="m2"><p>از مرتجعین شکست خوردیم همه</p></div></div>