---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>با خلق خدا شریک غم باید شد</p></div>
<div class="m2"><p>سربار بدوش دوست کم باید شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی ببری گوی معارف خواهی</p></div>
<div class="m2"><p>درگاه عمل پیشقدم باید شد</p></div></div>