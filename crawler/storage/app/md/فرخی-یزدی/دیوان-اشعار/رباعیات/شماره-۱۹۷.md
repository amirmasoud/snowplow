---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>ایکاش بشهر شحنه را زور نبود</p></div>
<div class="m2"><p>ملت ز فشار ظلم مقهور نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک شمه ز قانون شکنی می گفتم</p></div>
<div class="m2"><p>گر نامه ما اسیر سانسور نبود</p></div></div>