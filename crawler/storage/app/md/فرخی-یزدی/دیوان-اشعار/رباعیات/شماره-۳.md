---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>از بسکه زند نوای غم چنگی ما</p></div>
<div class="m2"><p>اندوه کند عزم همآهنگی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادی و گشایش جهان کافی نیست</p></div>
<div class="m2"><p>در موقع غم برای دل تنگی ما</p></div></div>