---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>ما دایره کثرت و قلت هستیم</p></div>
<div class="m2"><p>ما آینه عزت و ذلت هستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو در طلب حکومت مقتدری</p></div>
<div class="m2"><p>ما طالب اقتدار ملت هستیم</p></div></div>