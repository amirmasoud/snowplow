---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>از رنگ افق من آتشی می‌بینم</p></div>
<div class="m2"><p>در خلق جهان کشمکشی می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اما پس از این کشمکش امروزی</p></div>
<div class="m2"><p>از بهر بشر روز خوشی می‌بینم</p></div></div>