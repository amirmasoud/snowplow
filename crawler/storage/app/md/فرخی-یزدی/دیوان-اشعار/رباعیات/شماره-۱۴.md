---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>هر روز در این خرابه جنگی دگر است</p></div>
<div class="m2"><p>در ساغر شهد ما شرنگی دگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اوضاع سیاست عمومی گویا</p></div>
<div class="m2"><p>چون بوقلمون باز به رنگی دگر است</p></div></div>