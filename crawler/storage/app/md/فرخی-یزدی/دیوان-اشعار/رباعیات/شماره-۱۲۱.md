---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>درد و غم خوبان جوان پیرم کرد</p></div>
<div class="m2"><p>بد عهدی آسمان زمینگیرم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ماندم و من با همه بدبختیها</p></div>
<div class="m2"><p>ای مرگ بیا که زندگی سیرم کرد</p></div></div>