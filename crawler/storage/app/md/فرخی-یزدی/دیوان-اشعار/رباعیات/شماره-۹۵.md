---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>عهدی که در این خانه نوا بود، گذشت</p></div>
<div class="m2"><p>همسایه به ما حکمروا بود گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین خانه خدا بترس ای خانه خراب</p></div>
<div class="m2"><p>کان دوره که خانه بی خدا بود گذشت</p></div></div>