---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>در این ره سخت گر شود پای تو سست</p></div>
<div class="m2"><p>از دست شکستگان شوی رنجه درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چیز که خواستی مهیا کردند</p></div>
<div class="m2"><p>گر مرد هنروری کنون نوبت تست</p></div></div>