---
title: >-
    شمارهٔ ۳۵۰
---
# شمارهٔ ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>آن دسته که در نزد تو پیشند همه</p></div>
<div class="m2"><p>با حرف رفیق نوش و نیشند همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آید چو میان پای عمل می دانند</p></div>
<div class="m2"><p>یکسر پی جلب نفع خویشند همه</p></div></div>