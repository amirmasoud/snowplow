---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>هر چند که انقلاب را قاعده نیست</p></div>
<div class="m2"><p>در آتش و خون برای کس مائده نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اما دول قوی چو در جنگ شوند</p></div>
<div class="m2"><p>بهر ملل ضعیف بیفایده نیست</p></div></div>