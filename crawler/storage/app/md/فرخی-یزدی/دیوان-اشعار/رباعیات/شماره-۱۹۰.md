---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>طوفان که طرفدار صفا خواهد بود</p></div>
<div class="m2"><p>معدوم کن جور و جفا خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جنگ کند برای حیثیت خویش</p></div>
<div class="m2"><p>نسبت بعقیده باوفا خواهد بود</p></div></div>