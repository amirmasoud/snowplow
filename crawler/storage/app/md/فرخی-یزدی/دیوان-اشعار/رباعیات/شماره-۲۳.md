---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>در ملک وجود خودنمائی غلط است</p></div>
<div class="m2"><p>در بندگی اظهار خدائی غلط است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانگی آموز که با مسلک راست</p></div>
<div class="m2"><p>با خلق زمانه آشنائی غلط است</p></div></div>