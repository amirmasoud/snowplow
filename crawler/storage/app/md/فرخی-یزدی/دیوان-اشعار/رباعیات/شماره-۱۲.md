---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>روزی که شرار بغض و کین شعله ور است</p></div>
<div class="m2"><p>وز آتش فتنه خشک و تر در خطر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسوس من این است که در آن هنگام</p></div>
<div class="m2"><p>بیچاره تر آن بود که بیچاره تر است</p></div></div>