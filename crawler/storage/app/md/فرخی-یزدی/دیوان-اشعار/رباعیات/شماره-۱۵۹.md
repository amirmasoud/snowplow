---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>آنانکه ز خون دو دست رنگین کردند</p></div>
<div class="m2"><p>آزادی حق خویش تأمین کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارند در انظار ملل حق حیات</p></div>
<div class="m2"><p>آن قوم که انقلاب خونین کردند</p></div></div>