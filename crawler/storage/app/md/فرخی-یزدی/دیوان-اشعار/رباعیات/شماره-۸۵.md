---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>در مملکتی که نام آزادی نیست</p></div>
<div class="m2"><p>ویرانی آن قابل آبادی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر دل چون آهن آزادی کش</p></div>
<div class="m2"><p>درمان بجز از دشنه پولادی نیست</p></div></div>