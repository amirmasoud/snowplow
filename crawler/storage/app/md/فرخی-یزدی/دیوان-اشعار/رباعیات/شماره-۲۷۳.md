---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>چندی ز هوس باده پرستی کردم</p></div>
<div class="m2"><p>می خوردم و از غرور مستی کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون پای امیدواریم خورد بسنگ</p></div>
<div class="m2"><p>دیدم که عبث دراز دستی کردم</p></div></div>