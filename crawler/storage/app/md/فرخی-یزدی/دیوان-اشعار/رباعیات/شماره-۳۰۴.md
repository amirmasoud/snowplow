---
title: >-
    شمارهٔ ۳۰۴
---
# شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>روزی که به کار زندگی دست زدیم</p></div>
<div class="m2"><p>در عالم نیستی دم از هست زدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او رنگ فلک نبود چون در خور ما</p></div>
<div class="m2"><p>پا بر سر این نشیمن پست زدیم</p></div></div>