---
title: >-
    شمارهٔ ۳۰۰
---
# شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>آن روز که ما و دل ز مادر زادیم</p></div>
<div class="m2"><p>دایم ز فشار درد و غم ناشادیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در لجه این جهان پر حلقه و دام</p></div>
<div class="m2"><p>آزاد ولی چو ماهی آزادیم</p></div></div>