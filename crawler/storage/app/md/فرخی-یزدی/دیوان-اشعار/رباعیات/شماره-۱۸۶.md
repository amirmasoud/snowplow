---
title: >-
    شمارهٔ ۱۸۶
---
# شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>خوش باش که ارباب یقین شک نکنند</p></div>
<div class="m2"><p>از لوح ضمیر نام حق حک نکنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اثبات گناهان خطاکاران را</p></div>
<div class="m2"><p>در محکمه بی منطق و مدرک نکنند</p></div></div>