---
title: >-
    شمارهٔ ۲۶۰
---
# شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>ای دوست به فکر جنگجوئی کم باش</p></div>
<div class="m2"><p>در صلح عمومی علم عالم باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با هر که زنی لاف محبت یکروز</p></div>
<div class="m2"><p>مردانه و ثابت قدم و محکم باش</p></div></div>