---
title: >-
    شمارهٔ ۲۲۴
---
# شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>آن اهل خطا که با خطاکار نمود</p></div>
<div class="m2"><p>با کار خطا شبهه در افکار نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر رغم مدافعین بیگانه پرست</p></div>
<div class="m2"><p>آخر به خطای خویش اقرار نمود</p></div></div>