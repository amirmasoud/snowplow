---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>درد هر چو ما کسی بدین ذلت نیست</p></div>
<div class="m2"><p>وین ذلت لایزال بیعلت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست از طرف ملت بی علم قصور</p></div>
<div class="m2"><p>تقصیر همین ز جانب دولت نیست</p></div></div>