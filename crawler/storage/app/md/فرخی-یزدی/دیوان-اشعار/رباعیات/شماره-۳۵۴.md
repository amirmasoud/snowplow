---
title: >-
    شمارهٔ ۳۵۴
---
# شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>افسوس که از رأی خراب من و تو</p></div>
<div class="m2"><p>یکمرتبه شد پاک حساب من و تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آراء لواسان چو بخوبی خوانند</p></div>
<div class="m2"><p>حاکی است ز سؤ انتخاب من و تو</p></div></div>