---
title: >-
    شمارهٔ ۷۱ - صندوق آرا
---
# شمارهٔ ۷۱ - صندوق آرا

<div class="b" id="bn1"><div class="m1"><p>دردا که دوای دل به جز حسرت نیست</p></div>
<div class="m2"><p>حسرت بحساب قلت و کثرت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم که شود مجلس پنجم هم بد</p></div>
<div class="m2"><p>بدتر ز فساد دوره فترت نیست</p></div></div>