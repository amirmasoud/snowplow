---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>اول بخطا پیشه مماشات کنید</p></div>
<div class="m2"><p>قانع چو نشد خطایش اثبات کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اثبات چو شد خطا بحکم قانون</p></div>
<div class="m2"><p>بر کیفر آن خطا مجازات کنید</p></div></div>