---
title: >-
    شمارهٔ ۳۰۱
---
# شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>یک عمر چو جغد نوحه خوانی کردیم</p></div>
<div class="m2"><p>نفرین به اساس زندگانی کردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان کندن تدریجی خود را آخر</p></div>
<div class="m2"><p>تبدیل به مرگ ناگهانی کردیم</p></div></div>