---
title: >-
    شمارهٔ ۳۰۸
---
# شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>یک عمر به بند آز پا بسته شدیم</p></div>
<div class="m2"><p>بر اهل هوس قائد و سردسته شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینک پی مرگ ناگهانیم دوان</p></div>
<div class="m2"><p>از بسکه ز دست زندگی خسته شدیم</p></div></div>