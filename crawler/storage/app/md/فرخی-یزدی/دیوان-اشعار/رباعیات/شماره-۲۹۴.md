---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>تا بر سر حرص و آز خود پا زده‌ایم</p></div>
<div class="m2"><p>لبخند به دستگاه دنیا زده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با کشتی طوفانی بشکسته خویش</p></div>
<div class="m2"><p>شادیم از آنکه دل به دریا زده‌ایم</p></div></div>