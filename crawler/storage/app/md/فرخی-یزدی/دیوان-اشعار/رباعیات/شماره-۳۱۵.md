---
title: >-
    شمارهٔ ۳۱۵
---
# شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>گر طالع خفته را سحرخیز کنیم</p></div>
<div class="m2"><p>از آب رزان آتش دل تیز کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک چله نشسته گوشه میکده ای</p></div>
<div class="m2"><p>وز هر چه بغیر باده پرهیز کنیم</p></div></div>