---
title: >-
    شمارهٔ ۲۲۰
---
# شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>تا جرأت و پشتکار توأم نشود</p></div>
<div class="m2"><p>شیرازه کارها منظم نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم نشد این بنای ویران آباد</p></div>
<div class="m2"><p>بی شبهه از این خرابتر هم نشود</p></div></div>