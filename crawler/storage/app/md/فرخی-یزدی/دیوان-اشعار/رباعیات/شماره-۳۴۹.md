---
title: >-
    شمارهٔ ۳۴۹
---
# شمارهٔ ۳۴۹

<div class="b" id="bn1"><div class="m1"><p>یکدسته که کاندید جدیدند همه</p></div>
<div class="m2"><p>سال و مه و هفته ها دویدند همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که ز رأی خوانده گردیده دو ثلث</p></div>
<div class="m2"><p>ناچار سه ربع نا امیدند همه</p></div></div>