---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>طوفان که ز توقیف برون می‌آید</p></div>
<div class="m2"><p>جان در تن ارباب جنون می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین سرخ کلیشه کن حذر ای خائن</p></div>
<div class="m2"><p>اینجاست که فاش بوی خون می‌آید</p></div></div>