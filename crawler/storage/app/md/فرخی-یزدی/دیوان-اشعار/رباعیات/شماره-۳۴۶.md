---
title: >-
    شمارهٔ ۳۴۶
---
# شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>احزاب جهان راه نجاتند همه</p></div>
<div class="m2"><p>در جامعه باعث حیاتند همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کشور ما چو جنگ صنفی نبود</p></div>
<div class="m2"><p>این است که بی عزم و ثباتند همه</p></div></div>