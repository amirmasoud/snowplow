---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>آن سان که ستاره در سما افزون است</p></div>
<div class="m2"><p>در روی زمین حادثه گوناگون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>القصه از این حوادث رنگارنگ</p></div>
<div class="m2"><p>بر هر که نظر بیفکنی دل خون است</p></div></div>