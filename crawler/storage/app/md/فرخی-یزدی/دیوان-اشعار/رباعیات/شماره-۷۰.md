---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>ای داد که راه نفسی پیدا نیست</p></div>
<div class="m2"><p>راه نفسی بهر کسی پیدا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهریست پر از ناله و فریاد و فغان</p></div>
<div class="m2"><p>فریاد که فریادرسی پیدا نیست</p></div></div>