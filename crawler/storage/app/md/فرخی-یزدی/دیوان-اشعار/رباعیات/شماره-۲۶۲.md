---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>در بیشه دهر، شیر با دندان باش</p></div>
<div class="m2"><p>هم پیشه پنجه هنرمندان باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شام کند خار چمن خون به دلت</p></div>
<div class="m2"><p>چون غنچه صبحدم دمی خندان باش</p></div></div>