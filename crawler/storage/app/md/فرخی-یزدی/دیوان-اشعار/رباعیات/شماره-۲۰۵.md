---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>اسرار سراچه کهن تازه نبود</p></div>
<div class="m2"><p>غوغای حیات غیر آوازه نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این جامه زندگی که خیاط ازل</p></div>
<div class="m2"><p>از بهر من و تو دوخت، اندازه نبود</p></div></div>