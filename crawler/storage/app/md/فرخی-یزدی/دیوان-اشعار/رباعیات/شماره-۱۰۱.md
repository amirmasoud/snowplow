---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>ای دوست برای دوست جان باید داد</p></div>
<div class="m2"><p>در راه محبت امتحان باید داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنها نبود شرط محبت گفتن</p></div>
<div class="m2"><p>یک مرتبه هم عمل نشان باید داد</p></div></div>