---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>عمری به ره جنون نشستیم و گذشت</p></div>
<div class="m2"><p>وز ملک خرد برون نشستیم و گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>القصه کنار این چمن با خواری</p></div>
<div class="m2"><p>چون لاله میان خون نشستیم و گذشت</p></div></div>