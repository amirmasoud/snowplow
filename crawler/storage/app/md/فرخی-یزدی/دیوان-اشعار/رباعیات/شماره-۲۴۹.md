---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>از بهر مجازات و مکافات وزیر</p></div>
<div class="m2"><p>قانع نشوم به نفی و اثبات وزیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این است که از پارلمان باید خواست</p></div>
<div class="m2"><p>بگذشتن قانون مجازات وزیر</p></div></div>