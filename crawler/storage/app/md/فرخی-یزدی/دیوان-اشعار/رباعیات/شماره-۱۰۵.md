---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>دشمن پی دشمنی کمر می‌بندد</p></div>
<div class="m2"><p>بیگانه ره نفع و ضرر می‌بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دعوی دوستی کند دولت روس</p></div>
<div class="m2"><p>کی دوست به روی دوست در می‌بندد</p></div></div>