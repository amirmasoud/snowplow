---
title: >-
    شمارهٔ ۳۷۱
---
# شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>در اول عشق باده نوشی اولی</p></div>
<div class="m2"><p>در آخر عمر می فروشی اولی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دوره فترت است همچون خم می</p></div>
<div class="m2"><p>با خوردن خون دل خموشی اولی</p></div></div>