---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>استاد ازل که درس بیداد نداد</p></div>
<div class="m2"><p>جز مسئله داد مرا یاد نداد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما داد ز بیدادگران بستانیم</p></div>
<div class="m2"><p>گر محکمه داد به ما داد نداد</p></div></div>