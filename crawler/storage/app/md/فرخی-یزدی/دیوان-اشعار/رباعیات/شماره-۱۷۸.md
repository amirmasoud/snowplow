---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>عاقل که جز اقدام لزومی نکند</p></div>
<div class="m2"><p>غمناک دل غریب و بومی نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داند که حکومتی نگردد ثابت</p></div>
<div class="m2"><p>تا تکیه بر افکار عمومی نکند</p></div></div>