---
title: >-
    آغیز یِئمیشی
---
# آغیز یِئمیشی

<div class="b" id="bn1"><div class="m1"><p>بیر گون آغیز قالار بوش</p></div>
<div class="m2"><p>بیر گون دُولی داد اُولار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گون وارکی هئچ زاد اولماز</p></div>
<div class="m2"><p>گون وارکی هر زاد اولار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بختین دورا باخارسان </p></div>
<div class="m2"><p>یادیار قوهوم قارداشدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمما بختین یاتاندا </p></div>
<div class="m2"><p>قوهوم قارداش یاد اولار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چالیش آدین گلنده </p></div>
<div class="m2"><p>رحمت اوخونسون سنه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دونیادا سندن قالان</p></div>
<div class="m2"><p>آخیردا بیر آد اولار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گُوردون ایشین اَگیلدی</p></div>
<div class="m2"><p>دورما ،اَکیل،گؤزدَن ایت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوستون گؤره ر داریخار</p></div>
<div class="m2"><p>دوشمن گؤره ر شاد اولار</p></div></div>