---
title: >-
    بخش ۹ - لشکر کشیدن برزو به سوی ایران  قسمت دوم
---
# بخش ۹ - لشکر کشیدن برزو به سوی ایران  قسمت دوم

<div class="b" id="bn1"><div class="m1"><p>چو رستم مر آن هر دو تن را بدید</p></div>
<div class="m2"><p>زغم روی او گشت چون شنبلید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گستهم گفت ای دلارای مرد</p></div>
<div class="m2"><p>نگه کن که گردونت گردان چه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم از بهر نام و هم از بهر کین</p></div>
<div class="m2"><p>ز ترکان بپرداز روی زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس من نگه دار و هشیار باش</p></div>
<div class="m2"><p>دلیر و دلارای و بیدار باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفت این و شمشیر کین برکشید</p></div>
<div class="m2"><p>بدان بارگاه سپهبد دوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بالین آن هر دو بسته چو یوز</p></div>
<div class="m2"><p>خروشان و جوشان شه نیمروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برفت و ز لشکر نیامدش باک</p></div>
<div class="m2"><p>جهان پهلوان رستم خشمناک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزد تیغ بر گردن پاسدار</p></div>
<div class="m2"><p>سر آمد برو گردش روزگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو آمد بر طوس گفتش که خیز</p></div>
<div class="m2"><p>که آمد کنون جایگاه گریز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فریبرز بابند برداشتش</p></div>
<div class="m2"><p>سپهبد به گردن بر افراشتش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همان طوس بر گردن گستهم</p></div>
<div class="m2"><p>نشاند و بیامد چو شیر دژم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن پیش کین دیو آگه شود</p></div>
<div class="m2"><p>ز چاره مرا دست کوته شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مر آن هر دو تن را برون آورید</p></div>
<div class="m2"><p>از آن پاسبانان کس او را ندید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ببردند مر هر دوان در زمان</p></div>
<div class="m2"><p>به نزدیک خسرو چو باد دمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه راه بر دشت بی ره برید</p></div>
<div class="m2"><p>چنان چون طلایه به ره بر ندید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خسرو (به) بی راه و راه</p></div>
<div class="m2"><p>ندیدش کس او را ز هر دو سپاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو آمد به نزدیک خسرو فراز</p></div>
<div class="m2"><p>زمین را ببوسید و بردش نماز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مر آن هر دو تن را به خسرو سپرد</p></div>
<div class="m2"><p>بدو گفت کای نامور شاه گرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر آن سان که پیمان بکردم نخست</p></div>
<div class="m2"><p>سپردم به شه هر دوان را درست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به خسرو بگفت آنکه افراسیاب</p></div>
<div class="m2"><p>همی گفت و کرده دو دیده پر آب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشستند بر خوان و می خواستند</p></div>
<div class="m2"><p>همه کینه را دل بیاراستند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو شب دامن تیره را در کشید</p></div>
<div class="m2"><p>سیاهی برفت و سپیدی دمید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز هر دو سپه خاست آواز کوس</p></div>
<div class="m2"><p>هوا گشت مانند چشم خروس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سر از خواب برکرد افراسیاب</p></div>
<div class="m2"><p>دو چشمش چو خون شد ز کین و ز تاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی بارگه دید پر گفت وگوی</p></div>
<div class="m2"><p>وزان نامداران شده رنگ و بوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو افراسیاب این سپه را بدید</p></div>
<div class="m2"><p>ز پیران ویسه سخن بد رسید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدو گفت پیران ویسه همه</p></div>
<div class="m2"><p>که گرگ اندر آمد میان رمه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مر آن بستگان را گشادند دست</p></div>
<div class="m2"><p>ببرد و کسی را زلشکر نخست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نکردند کس را به چیزی زیان</p></div>
<div class="m2"><p>همانا که خرسند بود اندر آن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سپاس از خداوند پیروزگر</p></div>
<div class="m2"><p>کزیشان نشد شاه خسته جگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو افراسیاب آن ز پیران شنید</p></div>
<div class="m2"><p>بکردار دریا ز کین بردمید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>طلایه بپرسید تا تیره شب</p></div>
<div class="m2"><p>که بوده ست کآورد شور و شغب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به دژخیم فرمود تا در زمان</p></div>
<div class="m2"><p>سرش را زتن دور کردند در آن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وز آن پس بفرمود تا بی درنگ</p></div>
<div class="m2"><p>بیایند گردان به میدان جنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تبیره زنان در دمیدند نای</p></div>
<div class="m2"><p>زمانه تو گفتی در آمد ز جای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وزین روی کیخسرو و مرد وپیل</p></div>
<div class="m2"><p>جهان کرد مانند دریای نیل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زمین پر زجوش و هوا پر خروش</p></div>
<div class="m2"><p>همی کر شد از بانگ اسبان دو گوش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>درفشیدن تیغ ازآن تیره گرد</p></div>
<div class="m2"><p>چو آتش پس پرده لاژورد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کسی را نبد زان میانه گذار</p></div>
<div class="m2"><p>ز بس تیرو شمشیر و گرد و سوار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خروش تبیره ز هر دو سپاه</p></div>
<div class="m2"><p>برآمد همی تا به خورشید و ماه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بفرمود خسرو که صف برکشید</p></div>
<div class="m2"><p>همه سر به سر تن به کشتن دهید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز ترکان هر آن کس که او کین کشد</p></div>
<div class="m2"><p>سر بخت خود را به پروین کشد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همه نامداران ایران سپاه</p></div>
<div class="m2"><p>نبودند جز یکدل و کینه خواه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بفرمود تا پور گودرز گیو</p></div>
<div class="m2"><p>ابا نامداران و گردان نیو</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سوی میمنه لشکر آراستند</p></div>
<div class="m2"><p>به خون ریختن تیغ پیراستند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همه لشکرش دست تشنه به خون</p></div>
<div class="m2"><p>همه نامداران به جنگ اندرون</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو افراسیاب آن سپه را بدید</p></div>
<div class="m2"><p>که خسرو از آن گونه لشکر کشید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به چشمش چنان آمد آن دشت جنگ</p></div>
<div class="m2"><p>که آمد مر او را زمانه به تنگ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به پیران سالار فرمود پس</p></div>
<div class="m2"><p>که ما را درنگ اندرین کار بس</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بفرمای تا ساز جنگ آورند</p></div>
<div class="m2"><p>جهان بر بداندیش تنگ آورند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز جنگ آوران لشکری برگزین</p></div>
<div class="m2"><p>وز ایشان بپرداز روی زمین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو شیران تند و پلنگ ژیان</p></div>
<div class="m2"><p>که یابند ایران ز ایشان زیان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سوی میمنه بارمان بر کشید</p></div>
<div class="m2"><p>خود و نامداران والا خرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز جنگ آوران ده هزار دگر</p></div>
<div class="m2"><p>سواران جنگ آور نامور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سپهدار هومان، سوار دلیر</p></div>
<div class="m2"><p>که روبه ستاند ز چنگال شیر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سوی میسره ساز جنگ آورد</p></div>
<div class="m2"><p>بدان دشت تا کی درنگ آورد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به قلب اندرون جای خود را بساز</p></div>
<div class="m2"><p>وز آنجا به نزدیک خسرو بتاز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بدان بی هنر خسرو خیره سر</p></div>
<div class="m2"><p>بگویش که چندین زکین پدر،</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چه داری به ابرو درون بند و چین</p></div>
<div class="m2"><p>چه پوشی به پیلان و مردان زمین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اگر چه سیاوخش بودت پدر</p></div>
<div class="m2"><p>به کین پدر بسته داری کمر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تو را شرم ناید کزین کیمیا</p></div>
<div class="m2"><p>سپه گستری پیش چشم نیا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دو دیده به آب جفا شسته ای</p></div>
<div class="m2"><p>به خون خوردن ما کمر بسته ای</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مگر شاه نشنید آن داستان</p></div>
<div class="m2"><p>که جمشید زد درگه باستان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو بر آرزو خیره جنگ آوری</p></div>
<div class="m2"><p>جهان بر دل خویش تنگ آوری</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چه کردند ایران و توران زمین</p></div>
<div class="m2"><p>چه داری ز هر دو سپه درد و کین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سیاوخش تا زنده بود از نخست</p></div>
<div class="m2"><p>مر او را به جز تخم شادی نرست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>که ما را چو فرزند و داماد بود</p></div>
<div class="m2"><p>بر او روز و شب جان ما شاد بود</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو از راه دانش بپیچید سر</p></div>
<div class="m2"><p>نه سر ماند با او نه تاج و کمر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بیا تا بگردیم یک با دگر</p></div>
<div class="m2"><p>ببینیم تا کیست پیروز گر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>اگر دست یابی تو بر من به کین</p></div>
<div class="m2"><p>برآساید از جنگ روی زمین</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به خنجر سرم را ز تن دور کن</p></div>
<div class="m2"><p>ز خونم ددان را همی سور کن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>شود سر به سر شهر توران تو را</p></div>
<div class="m2"><p>چو در خاک آری ز زین مر مرا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>وگر من شوم بر تو بر چیره دست</p></div>
<div class="m2"><p>همان گرد کینه ز میدان نشست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>سرت را در آرم به خم کمند</p></div>
<div class="m2"><p>کنم دست و پایت به آهن به بند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز دریای گنگت به راه افکنم</p></div>
<div class="m2"><p>ز پشت نوندت به چاه افکندم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>پی و بیخ رستم ز بن برکنم</p></div>
<div class="m2"><p>به ایران همی آتش اندر زنم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو بشنید پیران ز افراسیاب</p></div>
<div class="m2"><p>خروشان بیامد چو دریای آب</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به لشکرگه شاه ایران رسید</p></div>
<div class="m2"><p>خروشی چو شیر ژیان برکشید</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>که ای نامداران ایران زمین</p></div>
<div class="m2"><p>ز من سوی خسرو برید آفرین</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>پیامی ز من نزد خسرو برید</p></div>
<div class="m2"><p>بگویید با او و پاسخ دهید</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو بشنید گودرز کشوادگان</p></div>
<div class="m2"><p>روان شد بر شاه آزادگان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به خسرو چنین گفت کای شهریار</p></div>
<div class="m2"><p>سخن بشنو از من یکی گوش دار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>مرا گفت پیران ویسه نژاد</p></div>
<div class="m2"><p>دلی پر زکینه سری پر ز باد</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ز افراسیاب آوریده پیام</p></div>
<div class="m2"><p>به نزدیک شاهنشه نیک نام</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>یکی مرد باید کنون چاپلوس</p></div>
<div class="m2"><p>که پیران مر او را ندارد فسوس</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بدین کار شایسته گرگین بود</p></div>
<div class="m2"><p>که گفتار او جمله نفرین بود</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>سخن را بیندیشد از پیش و پس</p></div>
<div class="m2"><p>همه باد پیماید اندر قفس</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>که پیران نگوید سخن جز دروغ</p></div>
<div class="m2"><p>دروغش بر او نگیرد فروغ</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به گرگین بفرمود پس شهریار</p></div>
<div class="m2"><p>که رو نزد آن ترک ناهوشیار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>فریبنده مردی ست پیران پیر</p></div>
<div class="m2"><p>دروغش نباید همی دلپذیر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چنان چون بود در خور او جواب</p></div>
<div class="m2"><p>بگو تا برد نزد افراسیاب</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>از ایدر به نزدیک پیران خرام</p></div>
<div class="m2"><p>ببین تا چه دارد بر ما پیام</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>شنو پاسخش یک به یک باز ده</p></div>
<div class="m2"><p>چنان کن که پیران بگوید که زه</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چو بشنید گرگین زمین بوسه داد</p></div>
<div class="m2"><p>برانگیخت شب رنگ مانند باد</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بیامد به کردار باد دمان</p></div>
<div class="m2"><p>به نزدیک پیران گشاده زبان</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>یکی گرگ پیکر درفش از برش</p></div>
<div class="m2"><p>به خورشید رخشان رسیده سرش</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>درفشش ببردند با او به هم</p></div>
<div class="m2"><p>چو پیران ورا دید شد پر زغم</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>به دل گفت با این دلاور، دروغ</p></div>
<div class="m2"><p>نگیرد چو نادان ز دانش فروغ</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>اگر تلخ گویم همان بشنوم</p></div>
<div class="m2"><p>همان بر که کارم همان بدروم</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چو شد نزد او پور میلاد راد</p></div>
<div class="m2"><p>ز اسب اندر آمد درودش بداد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چو پیران ورا دید آمد فرود</p></div>
<div class="m2"><p>همی داد بر شاه ایران درود</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بپرسید از شاه و بنشست شاد</p></div>
<div class="m2"><p>بر آن خاک بر ترک ویسه نژاد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>به گرگین چنین گفت کای نامور</p></div>
<div class="m2"><p>سخن بشنو از من همی سر به سر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>پیام شهنشاه افراسیاب</p></div>
<div class="m2"><p>به گرگین فرو خواند بر سان آب</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چو بشنید گرگین برآورد خشم</p></div>
<div class="m2"><p>ز کینه چو خون کرد مر هر دو چشم</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>به پیران چنین گفت کای نامدار</p></div>
<div class="m2"><p>ستوده به دانش بر شهریار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>به دیان که این گفت، خسرو نخست</p></div>
<div class="m2"><p>برین سان که گفتی سراسر درست</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چو از فر دیان همه باز گفت</p></div>
<div class="m2"><p>ز گفتار او ماند پیران شگفت</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>کنون یک به یک پاسخت باز داد</p></div>
<div class="m2"><p>بدان تا بگویی به آن دیو زاد</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>مرا گفت کیخسرو نامجوی</p></div>
<div class="m2"><p>که نزدیک آن پهلوان شو بگوی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>تو را شرم ناید ز ریش سپید</p></div>
<div class="m2"><p>زدیان همانا بریدی امید</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>نیاید ز تو جز دروغ و فسوس</p></div>
<div class="m2"><p>بدان گه که بندید بر پیل کوس</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>ز اول تو کشتی همه تخم کین</p></div>
<div class="m2"><p>ز تو گشت آشفته روی زمین</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>سیاوخش شد کشته از بهر تو</p></div>
<div class="m2"><p>کجا نوش پنداشت این زهر تو</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>به گفتار گرمت روان را بداد</p></div>
<div class="m2"><p>ندانست کت هست گفتار باد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چو کشتی همه تخمت آمد به بر</p></div>
<div class="m2"><p>به گردون برآورد این شاخ سر</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>فریب تو دیگر نخواهیم خورد</p></div>
<div class="m2"><p>برآریم از جان بدخواه گرد</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>دگر آنکه گفتی که افراسیاب</p></div>
<div class="m2"><p>همی راند از دیدگان جوی آب</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>ز بهر سیاوخش گریان شده ست</p></div>
<div class="m2"><p>وز آن کردن بد پشیمان شده ست</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>ز کردار بد گر بپیچد رواست</p></div>
<div class="m2"><p>که جان وی اندر دم اژدهاست</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>کسی را که دیان براند ز در</p></div>
<div class="m2"><p>کس او را به گیتی نگیرد به بر</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>کجا خسروش خصم و دشمن خدای</p></div>
<div class="m2"><p>کجا ماند او روز میدان به پای</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>کجا شاه ما راست خویش و نیا</p></div>
<div class="m2"><p>به آورد جوید ازو کیمیا</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>وگر مهربان گشت بر شاه نو</p></div>
<div class="m2"><p>درفشان چو خورشید بر گاه نو</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>نبرد کسی چون کند خواستار</p></div>
<div class="m2"><p>که باشد مر او را به دل خواستار</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>به میدان چرا خواند او را به جنگ</p></div>
<div class="m2"><p>چنان کم خرد ترک پور پشنگ</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>بزرگان ایران کجا رفته اند</p></div>
<div class="m2"><p>نه با شاه ایشان بر آشفته اند</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>چو گیو و چو گودرز، رهام و زال</p></div>
<div class="m2"><p>فریبرز کاوس با فر و یال</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>چو طوس و تهمتن فرامرز راد</p></div>
<div class="m2"><p>جهان پهلوان اشکش پاک زاد</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>سپهدار چون قارن رزم زن</p></div>
<div class="m2"><p>که مردان نمایند پیشش چو زن</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>چرا داد باید به من خواسته</p></div>
<div class="m2"><p>چو او جنگ را باید آراسته</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>به میدان چو از دشمن او کین کشد</p></div>
<div class="m2"><p>چرا اسب من زین زرین کشد</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>پسندد ز ما ایزد دادگر</p></div>
<div class="m2"><p>که خسرو به جنگ تو بندد کمر</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>تو را گر نبردت کند آرزوی</p></div>
<div class="m2"><p>بیا تا من و تو به هم کینه جوی</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>به دیان دادار (و) چرخ بلند</p></div>
<div class="m2"><p>به رخشنده خورشید و تیغ و کمند</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>که گر پیشم آیی به هنگام جنگ</p></div>
<div class="m2"><p>نمانم تو را بیش بر زین درنگ</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>که مرغی زند سر به آب اندرون</p></div>
<div class="m2"><p>برانم ز تو بر زمین جوی خون</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>به گرگین چنین گفت کای کم خرد</p></div>
<div class="m2"><p>به خسرو چنین گفت کی در خورد</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>بگفت این و از خاک بر پای جست</p></div>
<div class="m2"><p>بر آن باره پیل پیکر نشست</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>بیامد خروشان چو دریای آب</p></div>
<div class="m2"><p>همه باز گفتش به افراسیاب</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو بشنید افراسیاب دلیر</p></div>
<div class="m2"><p>بغرید بر سان ارغنده شیر</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>به پیران چنین گفت کامروز جنگ</p></div>
<div class="m2"><p>بجوییم با برزوی تیز چنگ</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>یکی سوی میدان شود جنگ جوی</p></div>
<div class="m2"><p>ببینیم تا چون بود جنگ اوی</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>بدان تا چگونه کند کارزار</p></div>
<div class="m2"><p>چه بازی نماید برو روزگار</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>که با فر و برز است و با شاخ و یال</p></div>
<div class="m2"><p>مگر کشته آید بدو پور زال</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>چو رستم شود کشته بر دست اوی</p></div>
<div class="m2"><p>به ماهی گراینده شد شست اوی</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>برآریم از ایران و خسرو دمار</p></div>
<div class="m2"><p>برآساید این لشکر از کارزار</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>پی و بیخ ایرانیان برکنیم</p></div>
<div class="m2"><p>همه بوم و بر آتش اندر زنیم</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>چو پیران ز افراسیاب این شنید</p></div>
<div class="m2"><p>سوی برزو نامور بنگرید</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>بدو گفت کای پهلوان شاد باش</p></div>
<div class="m2"><p>همه ساله ز اندوه آزاد باش</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>که امروز خورشید ما روی توست</p></div>
<div class="m2"><p>دو چشم سواران همه سوی توست</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>شه چین و ما چین و توران زمین</p></div>
<div class="m2"><p>ز بازوی تو جوید امروز کین</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>یک امروز اگر رای جنگ آیدت</p></div>
<div class="m2"><p>همی تخت ایران به چنگ آیدت</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>به دیان که تا من کمر بسته ام</p></div>
<div class="m2"><p>ز خون بسی نامور خسته ام</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>چو کاموس جنگی چو خاقان چین</p></div>
<div class="m2"><p>سواران و گردان توران زمین</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>به کینه برین بارگاه آمدند</p></div>
<div class="m2"><p>سزاوار تخت و کلاه آمدند</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>ندیدند از افراسیاب دلیر</p></div>
<div class="m2"><p>که دیدی تو ای نامبردار شیر</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>مگر بخت فرخنده یار تو شد</p></div>
<div class="m2"><p>چو افراسیابی شکار تو شد</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>چو پیران چنین گفت برزوی شیر</p></div>
<div class="m2"><p>بغرید بر سان شیر دلیر</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>فرود آمد از اسب مانند باد</p></div>
<div class="m2"><p>رکاب شه نامور بوسه داد</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>بدو گفت افراسیاب دلیر</p></div>
<div class="m2"><p>یک امروز بگشای چنگال شیر</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>بر آن سان که باشند مردان مرد</p></div>
<div class="m2"><p>برآور به خورشید رخشنده گرد</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>که امروز جنگ پلنگ آوری</p></div>
<div class="m2"><p>همان نام ایران به ننگ آوری</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>یکی دیو بینی چو نر اژدها</p></div>
<div class="m2"><p>چو شیری که از بند گردد رها</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>درآید به میدان و جنگ آورد</p></div>
<div class="m2"><p>همه رای و رسم پلنگ آورد</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>یکی اسب زیرش چو کوهی روان</p></div>
<div class="m2"><p>که از دیدنش خیره گردد روان</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>ورا رخش خوانند و او رستم است</p></div>
<div class="m2"><p>کزو شهر توران پر از ماتم است</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>بدو گفت برزوی کای شهریار</p></div>
<div class="m2"><p>کجا باشد این رستم نامدار؟</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>چه پوشد به جنگ و درفشش کجاست؟</p></div>
<div class="m2"><p>سوی دست چپ باشد ار دست راست؟</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>به بالا و دیدار و کردار کیست؟</p></div>
<div class="m2"><p>چه گیرد به میدان ورا کار چیست؟</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>چو بشنید پیران چنین گفت پس</p></div>
<div class="m2"><p>که چون او نباشد دگر هیچ کس</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>درختی به بار است با فر (و) شاخ</p></div>
<div class="m2"><p>قوی گردن و یال و سینه فراخ</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>ورا جوشن ازچرم شیران بود</p></div>
<div class="m2"><p>چو خورشید تابنده رخشان بود</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>پلنگینه پوش است اندر نبرد</p></div>
<div class="m2"><p>به گردون رساند در آورد گرد</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>هژبری به زیر جهان پهلوان</p></div>
<div class="m2"><p>کزو شاد مانند پیر وجوان</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>به سان هیون گردن و دست و پای</p></div>
<div class="m2"><p>به پیکر چو کوه جهنده ز جای</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>کمندی به فتراک بر شصت خم</p></div>
<div class="m2"><p>سپهبد رباید چو دریا به دم</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>یکی گرزه گاو پیکر به دست</p></div>
<div class="m2"><p>چو غرنده شیر است و چون پیل مست</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>به خشکی پلنگ و به دریا نهنگ</p></div>
<div class="m2"><p>نیارند با زخم او تاب جنگ</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>جهانجوی برزوی چون پیل مست</p></div>
<div class="m2"><p>برآشفت و یازید چون شیر دست</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>بفرمود تا در زمان بی درنگ</p></div>
<div class="m2"><p>نهادند بر باره زین خدنگ</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>به بر گستوانش بیاراستند</p></div>
<div class="m2"><p>یکی جوشن پهلوان خواستند</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>بپوشید جوشن سوار دلیر</p></div>
<div class="m2"><p>کمر بست بر کینه چون نره شیر</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>یکی ترگ چینی به سر بر نهاد</p></div>
<div class="m2"><p>کمان را به زه کرد و ترکش گشاد</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>کمندی به فتراک گلگون ببست</p></div>
<div class="m2"><p>یکی گرزه گاو پیکر به دست</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>سپر بر کتف نیزه بر پشت اسب</p></div>
<div class="m2"><p>خروشنده مانند آذرگشسب</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>به باره بر آمد ز هامون چو گرد</p></div>
<div class="m2"><p>همی تاخت تا جایگاه نبرد</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>به گفتار آن گه زبان برگشاد</p></div>
<div class="m2"><p>بدان نامداران فرخ نژاد</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>که ای نامور شاه آزاده خوی</p></div>
<div class="m2"><p>چرا جنگ ترکان کنی آرزوی</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>سرت را چه تابی ز راه خرد</p></div>
<div class="m2"><p>تو آن کن که از شهریاران سزد</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>ز شاهان که کرده ست این کیمیا</p></div>
<div class="m2"><p>به گیتی که جسته ست جنگ نیا</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>چو برگردد از راه دانش سرت</p></div>
<div class="m2"><p>به پیکان بدوزم سپر بر سرت</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>بفرمای تا نامداران جنگ</p></div>
<div class="m2"><p>بیایند پیشم بسازند جنگ</p></div></div>