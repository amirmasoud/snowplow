---
title: >-
    بخش ۱۸ - گرفتار شدن پهلوانان ایران به مکر سوسن مطرب
---
# بخش ۱۸ - گرفتار شدن پهلوانان ایران به مکر سوسن مطرب

<div class="b" id="bn1"><div class="m1"><p>وزین روی گردان ایران تمام</p></div>
<div class="m2"><p>رسیدند نزدیک ایوان سام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خوردن نهادند سر روز و شب</p></div>
<div class="m2"><p>نیاسود از خنده شان هیچ لب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبد کارشان جز می و خفت و خورد</p></div>
<div class="m2"><p>کس اندیشه مکر سوسن نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر پهلوانان ز می گشت شاد</p></div>
<div class="m2"><p>به شادی جهاندار کردند یاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی کس ندانست شب را ز روز</p></div>
<div class="m2"><p>همه نامداران گیتی فروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی گفت هر کس که چون من دگر</p></div>
<div class="m2"><p>به میدان کینه نبندد کمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی گفت من شیر گیرم به دست</p></div>
<div class="m2"><p>شود پست از گرز من پیل مست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی گفت هر کس ز مردی خویش</p></div>
<div class="m2"><p>به بیگانه مردم چه با مرد خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین داوری طوس بر پای خاست</p></div>
<div class="m2"><p>چنین گفت چون من به ایران نخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز تخم فریدون و نوذر نژاد</p></div>
<div class="m2"><p>ندارد چو من دیگری چرخ یاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نباشد چو من گرد با فر و یال</p></div>
<div class="m2"><p>نه گودرز کشواد و نه پور زال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو بشنید گودرز گفتا خموش</p></div>
<div class="m2"><p>نگویند چنین مردم تیز هوش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه بیشی کنی پیش آزادگان</p></div>
<div class="m2"><p>به ویژه بزرگان کشوادگان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر چند از من تو را شرم نیست</p></div>
<div class="m2"><p>کسی را به نزد تو آزرم نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز برزوت هم شرم ناید کنون</p></div>
<div class="m2"><p>که در خاکت آورد از زین نگون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون می فزونی کنی پیش اوی</p></div>
<div class="m2"><p>بدیده به میدان کمابیش اوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز گودرز چون طوس بشنید این</p></div>
<div class="m2"><p>چو شیر درنده درامد ز کین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بزد دست و خنجر کشید از نیام</p></div>
<div class="m2"><p>بزد دست رهام فرخنده نام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به نیرو جدا کرد خنجر ازوی</p></div>
<div class="m2"><p>بدو گفت کای مرد پرخاشجوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر نیستی شرم رستم ز پیش</p></div>
<div class="m2"><p>بدیدی همه مردی ورای خویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز گردان تو را پیشه جز جنگ نیست</p></div>
<div class="m2"><p>چو شیرانت خود پنجه جنگ نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو بشنید طوس این برآورد خشم</p></div>
<div class="m2"><p>ز کینه پر از آب کرده دو چشم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به اسب اندر آمد سرافراز مرد</p></div>
<div class="m2"><p>ز گردنده گردون برآورد گرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو او سوی ایران سر اندر کشید</p></div>
<div class="m2"><p>چو رستم بیامد مر او را ندید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نگه کرد از هر سوی چپ و راست</p></div>
<div class="m2"><p>چنین گفت طوس سپهبد کجاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه افتاد کآشفته اند این همه</p></div>
<div class="m2"><p>چه گرگ اندر آمد میان رمه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به بیهوده این داوری از چه خاست</p></div>
<div class="m2"><p>دل نامداران پر انده چراست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدو گفت برزوی کای پهلوان</p></div>
<div class="m2"><p>به کام تو بادا سپهر روان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به بی دانشی طوس را یار نیست</p></div>
<div class="m2"><p>به جز جنگ جستن ورا کار نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ندارد به گیتی کسی را به مرد</p></div>
<div class="m2"><p>ز گودرز و رهام جوید نبرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همه از فریدون سخن گفت و بس</p></div>
<div class="m2"><p>جز از خود نداند دگر هیچ کس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برآورد بازو و خنجر کشید</p></div>
<div class="m2"><p>همی خواست از تن سرش را برید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز دستش برون کرد رهام گرد</p></div>
<div class="m2"><p>همه پنجه و دست او کرد خرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برون شد ز گودرزیان پر ز خشم</p></div>
<div class="m2"><p>ز کینه چو دو طاس خون کرده چشم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زکان و دنان شد ز خانه برون</p></div>
<div class="m2"><p>همانا که شد پیش خسرو کنون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فرامرز را گفت کای بی خرد</p></div>
<div class="m2"><p>از آزادگان این کی اندر خورد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همان است طوس سپهبد که گفت</p></div>
<div class="m2"><p>نماند نژاد و هنر در نهفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جهاندار گودرز کشوادگان</p></div>
<div class="m2"><p>چو داند همی خوی آزادگان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همان تیزی و تند خویی طوس</p></div>
<div class="m2"><p>نبایست بستن برین گونه کوس</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنین گفت با برزوی نامور</p></div>
<div class="m2"><p>ندانی تو آیین گیتی مگر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که بد نامی آید به فرجام ازین</p></div>
<div class="m2"><p>چنین گفت دانای ایران زمین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که بر میزبان میهمان پادشاست</p></div>
<div class="m2"><p>تو آن کن که از نامداران سزاست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنین گفت رستم به گودرز پس</p></div>
<div class="m2"><p>که چون تو به دانش ندانیم کس</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جهان پهلوان طوس بی دانش است</p></div>
<div class="m2"><p>نه همچون تو از رای با رامش است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ولیکن ز تخم کیان است طوس</p></div>
<div class="m2"><p>نبایست کردن مر او را فسوس</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز بهر من اکنون و دستان سام</p></div>
<div class="m2"><p>به جان و سر شاه فرخنده نام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نتابی ز فرمان من هیچ سر</p></div>
<div class="m2"><p>بر آن سان که داری نژاد و گهر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شوی از پی طوس نوذر دوان</p></div>
<div class="m2"><p>به دست آری او را به ره بر روان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که هر کس کز ایدر شود پیش اوی</p></div>
<div class="m2"><p>نیاید به گفتار او کینه جوی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نباشدت ننگی که شه زاده است</p></div>
<div class="m2"><p>ز تخم بزرگان آزاده است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو بشنید گودرز آمد دوان</p></div>
<div class="m2"><p>بر ان سان که فرموده بد پهلوان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زمانی بر آمد جهان جوی گیو</p></div>
<div class="m2"><p>چنین گفت کای نامبردار نیو</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو دانی سپهدار گودرز را</p></div>
<div class="m2"><p>همان نامور طوس با ارز را</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر چند گودرز فرزانه است</p></div>
<div class="m2"><p>ازو طوس پر کین و دیوانه است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شوم از پی نامداران دوان</p></div>
<div class="m2"><p>به چربی بجویم دل هر دوان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بدو گفت رستم که فرمان تو راست</p></div>
<div class="m2"><p>بر آن رای رو کت همی رای خواست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو خورشید گشت از بر چرخ راست</p></div>
<div class="m2"><p>جهان جوی گستهم بر پای خاست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به رستم چنین گفت کای پهلوان</p></div>
<div class="m2"><p>دل کارزار و خرد را روان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تو دانی که از نوذر شهریار</p></div>
<div class="m2"><p>ندارم به جز طوس را یادگار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو گودرز و چون گیو دو جنگ جوی</p></div>
<div class="m2"><p>ندانم چه آید مر او را به روی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مرا دل ازین هر دو بر بیم گشت</p></div>
<div class="m2"><p>ز درد برادر به دو نیم گشت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بدو گفت رستم برو شادمان</p></div>
<div class="m2"><p>میانجی همی باش بر هر دوان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو بیرون شد از کاخ رستم به کین</p></div>
<div class="m2"><p>به اسب اندر آمد ز روی زمین</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همی راند باره به کردار باد</p></div>
<div class="m2"><p>مگر بنگرد طوس و گودرز شاد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو گستهم از پیش رستم برفت</p></div>
<div class="m2"><p>دل بیژن از درد ایشان بتفت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همی گفت آیا چه شاید بدن</p></div>
<div class="m2"><p>نشاید برین کار دم بر زدن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو گل هر زمانی همی بشکفید</p></div>
<div class="m2"><p>سرشکش ز دیده به رخ برچکید</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نگه کرد رستم بدو ناگهان</p></div>
<div class="m2"><p>بدو گفت کای پهلوان جهان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چرا نا شکیبی تو بر جای خویش</p></div>
<div class="m2"><p>چه اندیشه آمدت اکنون به پیش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بدو گفت بیژن که ای پهلوان</p></div>
<div class="m2"><p>ز اندیشه گشتم شکسته روان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ندانم که از طوس و گودرز و گیو</p></div>
<div class="m2"><p>چه آید برین دشت و گستهم نیو</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>اگر پهلوان رای بیند که من</p></div>
<div class="m2"><p>شوم از پی نامدار انجمن</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به بیژن چنین گفت رستم که خیز</p></div>
<div class="m2"><p>برانگیز از جای شبرنگ تیز</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نباید که پرخاش جویی و جنگ</p></div>
<div class="m2"><p>همه نام نیک تو گردد به ننگ</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چو بشنید بیژن ز رستم چنین</p></div>
<div class="m2"><p>ز هامون بر آمد به بالای زین</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>پی اسب گردان ایران گرفت</p></div>
<div class="m2"><p>به دل مانده از کار ایشان شگفت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بر آمد برین بر زمانی دراز</p></div>
<div class="m2"><p>نیامد همی طوس و گودرز باز</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>دل رستم اندیشه ای کرد بد</p></div>
<div class="m2"><p>چنان کز ره نامداران سزد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چنین گفت ز اندوه بگذشت کار</p></div>
<div class="m2"><p>همانا سر آمد ورا روزگار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>زمانی ز هر گونه اندیشه کرد</p></div>
<div class="m2"><p>دل خویش از اندیشه چون بیشه کرد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بپیچید بر خویشتن پهلوان</p></div>
<div class="m2"><p>شد از کار ایشان خلیده روان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چنین گفت از آن پس فرامرز را</p></div>
<div class="m2"><p>سرافراز نامی با ارز را</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>که از کار گردان شدم دل غمی</p></div>
<div class="m2"><p>ز اندیشه در جانم آمد کمی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>هر آن گه که باشد مرا روز جنگ</p></div>
<div class="m2"><p>همی جستن آرد مرا پای و چنگ</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ندانم چه آید ازین کین به من</p></div>
<div class="m2"><p>چه خیزد از آشوب این انجمن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ببند از پی راه رفتن میان</p></div>
<div class="m2"><p>مگر باز بینم ایرانیان</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به جوشن بپوشان نخستین برت</p></div>
<div class="m2"><p>یکی ترگ رومی بنه بر سرت</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>برافراز بازو به گرز گران</p></div>
<div class="m2"><p>چو آشفته شیران مازندران</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>برانگیز باره به کردار باد</p></div>
<div class="m2"><p>نباید به ره بر همی لب گشاد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بگو با سرافراز گودرز و گیو</p></div>
<div class="m2"><p>که ای نامداران وگردان نیو</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>همی چشم دارم که گردند باز</p></div>
<div class="m2"><p>همه نامدارن گردن فراز</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>فرامرز بشنید این از پدر</p></div>
<div class="m2"><p>به گردنده گردون برآورد سر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>نشست از بر باره راهور</p></div>
<div class="m2"><p>خروشان به کردار شیر شکار</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چو آمد فرامرز از در برون</p></div>
<div class="m2"><p>به برزو چنین گفت رستم کنون</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بسازیم بر بزم چون کنیم</p></div>
<div class="m2"><p>برین خستگی بر چه افسون کنیم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بدو گفت برزوی کای نامور</p></div>
<div class="m2"><p>نباید به غم خود دل تاجور</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>نباید چنین دل درین کار بست</p></div>
<div class="m2"><p>به اندیشه از مرگ هرگز که رست</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چنین بود تا بود گردان سپهر</p></div>
<div class="m2"><p>گهی زهر و کین و گهی نوش و مهر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>درین داوری بود برزوی شیر</p></div>
<div class="m2"><p>که زال سرافراز گرد دلیر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ز خواب اندر آمد همی بنگرید</p></div>
<div class="m2"><p>در ایوان رستم یلان را ندید</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>به جز پهلوان رستم نامدار</p></div>
<div class="m2"><p>ابا برزوی گرد شیر شکار</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>(به رستم چنین گفت زال سوار</p></div>
<div class="m2"><p>کجا رفت گودرز و طوس آن دو یار)</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>به مستی به نخجیر گوران شدند</p></div>
<div class="m2"><p>وگر پیش شاه دلیران شدند</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بدو گفت رستم که ای پهلوان</p></div>
<div class="m2"><p>سر نامداران و پشت گوان</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چه گویم ز کردار ایرانیان</p></div>
<div class="m2"><p>به پرخاش بسته همیشه میان</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>که طوس سپهبد بدین انجمن</p></div>
<div class="m2"><p>سخن گفت از مردی خویشتن</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بر آشفت و گودرز را سرد گفت</p></div>
<div class="m2"><p>سر انجمن ناجوان مرد گفت</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>ز کینه به شمشیر یازید دست</p></div>
<div class="m2"><p>در آمد از آن پس ز جای نشست</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>همه داستان پیش دستان بگفت</p></div>
<div class="m2"><p>همی آب دیده به مژگان برفت</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بپرسید زال از فرامرز شیر</p></div>
<div class="m2"><p>بر آشفت با پهلوان دلیر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>به رستم چنین گفت کای بی خرد</p></div>
<div class="m2"><p>ز تو این چنین رای کی در خورد</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>فرامرز را گر بد آید به روی</p></div>
<div class="m2"><p>چه گویی به گردان پرخاشجوی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>ندانی مگر طوس و گودرز و گیو</p></div>
<div class="m2"><p>همان بیژن گیو و گستهم نیو</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>ز درد تو دلشان نباشد تهی</p></div>
<div class="m2"><p>وگر تاج زرشان به سر بر نهی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>به کینه همی تیز و دیوانه اند</p></div>
<div class="m2"><p>نژاد تو را نیز بیگانه اند</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>برو بر یکی پیش دستی کند</p></div>
<div class="m2"><p>بهانه پس آن گاه مستی کند</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>بگفت این و از جایگه بردمید</p></div>
<div class="m2"><p>خروشی چو شیر ژیان بر کشید</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بیارید گفتا کنون جوشنم</p></div>
<div class="m2"><p>که بیم است تا دل ز تن بر کنم</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بپوشید جوشن چو شیر ژیان</p></div>
<div class="m2"><p>ببست از پی راه رفتن میان</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>نشست از بر باره تیز گام</p></div>
<div class="m2"><p>تو گفتی مگر زنده شد باز سام</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>بپوشید اسبش به بر گستوان</p></div>
<div class="m2"><p>چنان چون بود ساز و رزم کیان</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>به دست اندرون گرز سام سوار</p></div>
<div class="m2"><p>به آهن درون غرقه شیر شکار</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>کمان کیانی به بازو درون</p></div>
<div class="m2"><p>بر آن باره بر چون که بیستون</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>به برزو چنین گفت زال دلیر</p></div>
<div class="m2"><p>که ای نامور ببر و درنده شیر</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>بدان گه که من چون تو بودم، به جنگ</p></div>
<div class="m2"><p>چه روباه پیشم چه شرزه پلنگ</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>چنین چنبری گشت یال یلی</p></div>
<div class="m2"><p>نتابم همی خنجر کابلی</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>اگر نام دستان نوشتی بر آب</p></div>
<div class="m2"><p>نماندی به آب اندرون هیچ تاب</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>یکی ترگ چینی به سر بر نهاد</p></div>
<div class="m2"><p>همی رفت تازان به کردار باد</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>چو بر باره پیل پیکر نشست</p></div>
<div class="m2"><p>کمندی به فتراک باره ببست</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>خروشی بر آورد چون نره شیر</p></div>
<div class="m2"><p>به کینه همی رفت گرد دلیر</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>تو گفتی ندارد به تن در روان</p></div>
<div class="m2"><p>ز اندیشه نامور پهلوان</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>همی گفت آیا برین دشت کین</p></div>
<div class="m2"><p>چه اید ز گردان ایران زمین</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>جهان پهلوان زال سام سوار</p></div>
<div class="m2"><p>همی رفت بر سان شیر شکار</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>ز هفت صد همانا فزون بود سال</p></div>
<div class="m2"><p>ز نیرو به گردون برآورده یال</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>نه بود و نه هست و نه باشد دگر</p></div>
<div class="m2"><p>چو زال و چو رستم دگر نامور</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>به ره بر نبودش ز تیزی زمان</p></div>
<div class="m2"><p>همی رفت بر سان باد دمان</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>اگر چند بد پیر و برگشته روز</p></div>
<div class="m2"><p>همی تافت چون مهر گیتی فروز</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>چو برزو نگه کرد در روی اوی</p></div>
<div class="m2"><p>ندانست کس را به گیتی چنوی</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>به رستم چنین گفت کای پهلوان</p></div>
<div class="m2"><p>سر نامداران و پشت گوان</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>به دیان که توران همه دیده ام</p></div>
<div class="m2"><p>همه کشور ترک گردیده ام</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>ندیدم سواری بدین فر ویال</p></div>
<div class="m2"><p>که سام نریمانش خوانده ست زال</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>تو گفتی که شیری ست بر پشت پیل</p></div>
<div class="m2"><p>و یا کوه البرز در آب نیل</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>به روز جوانی همانا که ابر</p></div>
<div class="m2"><p>به خاک سیه در فکندی هژبر</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>زمانه نیارد همانا چنین</p></div>
<div class="m2"><p>دگر نامداری به ایران و چین</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>چو بودم بر آورد پیکار جوی</p></div>
<div class="m2"><p>مرا آرزو بود دیدار اوی</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>چو دیدم جهان پهلوان را چنین</p></div>
<div class="m2"><p>بیفزود مهرم ز پیکار و کین</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>بگرید بر آن کس همی روزگار</p></div>
<div class="m2"><p>که جوید ز دستان همی کارزار</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>چو زال سپهبد برانگیخت اسب</p></div>
<div class="m2"><p>همی رفت بر سان آذر گشسب</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>چو برزوی (و) رستم به خوردن نشست</p></div>
<div class="m2"><p>پرستار شد کودک می پرست</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>نوای مغانی و آوای رود</p></div>
<div class="m2"><p>به پروین رسانیده بانگ سرود</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>کنون بازگردم به آغاز کار</p></div>
<div class="m2"><p>بگویم که چون بود طوس سوار</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>نگه کن چه اورد او را به پیش</p></div>
<div class="m2"><p>همی گشت گردون و کردار خویش</p></div></div>