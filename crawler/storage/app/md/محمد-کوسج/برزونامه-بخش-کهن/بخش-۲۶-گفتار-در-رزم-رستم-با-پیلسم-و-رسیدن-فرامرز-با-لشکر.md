---
title: >-
    بخش ۲۶ - گفتار در رزم رستم با پیلسم و رسیدن فرامرز با لشکر
---
# بخش ۲۶ - گفتار در رزم رستم با پیلسم و رسیدن فرامرز با لشکر

<div class="b" id="bn1"><div class="m1"><p>وزین روی رستم چو شیر ژیان</p></div>
<div class="m2"><p>بیامد بر پیلسم در زمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تندی برو تیر باران گرفت</p></div>
<div class="m2"><p>کمند و کمان سواران گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو پیکار او دید ترک دلیر</p></div>
<div class="m2"><p>بدو گفت کای نامور نره شیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا رزم گردان بدی بزمگاه</p></div>
<div class="m2"><p>نترسم چو بینم چو تو صد سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه کاموس جنگی نه خاقان چین</p></div>
<div class="m2"><p>نه از شاه و گردان ایران زمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مپندار گر چرخ گردان شوی</p></div>
<div class="m2"><p>به مردی چو سام نریمان شوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برین دشت گردی ز چنگم رها</p></div>
<div class="m2"><p>نگیری دگر نزد خسرو بها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگه کرد رستم به بالای اوی</p></div>
<div class="m2"><p>برآن تیز گفتار پرخاش جوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز چندان بزرگان و گردن کشان</p></div>
<div class="m2"><p>به ایران و توران نبد ز آن نشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمانی به بازو و گرزی به دست</p></div>
<div class="m2"><p>یکی باره در زیر چون پیل مست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کمندی به فتراک او شست خم</p></div>
<div class="m2"><p>نبودش ز رستم به دل هیچ غم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپهدار ایران ز دیدار او</p></div>
<div class="m2"><p>پر اندیشه شد دل از آن کار او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو گفت رستم که نام تو چیست</p></div>
<div class="m2"><p>به توران به تو بر که خواهد گریست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به نیرنگ و دستان این دیو زاد</p></div>
<div class="m2"><p>سر نامور دادخواهی به باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کفن دوز بود آنکه جوشنت دوخت</p></div>
<div class="m2"><p>بر این آتش کین روانت بسوخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون گور تو کام شیران شده ست</p></div>
<div class="m2"><p>همان خاک تو دشت ایران شده ست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بینی ز من ساز و پیکار جنگ</p></div>
<div class="m2"><p>نترسی به دریا ز غران نهنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسی چون تو دیدم در آوردگاه</p></div>
<div class="m2"><p>که از تیغشان خون چکیدی به ماه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو پیکار من بودشان آرزوی</p></div>
<div class="m2"><p>زمانه بر ایشان شدی کینه جوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو بشنید ازو پیلسم این چنین</p></div>
<div class="m2"><p>بر ابرو در افکند از خشم چین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین داد پاسخ ورا پیلسم</p></div>
<div class="m2"><p>براندیش ازین کینه بر بیش و کم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه انگشت بر دست یکسان بود</p></div>
<div class="m2"><p>که چرخ فریبنده گردان بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنانت فرستم به نزدیک زال</p></div>
<div class="m2"><p>که دیگر ننازی به کوپال و یال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به خم کمندت در آرم ز زین</p></div>
<div class="m2"><p>ز خونت کنم سرخ روی زمین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>(فرستم از آن پس به دریای چین</p></div>
<div class="m2"><p>به نزد سپهدار توران زمین )</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به سر نه کلاه و برهنه به تن</p></div>
<div class="m2"><p>نمایم بر آن نامدار انجمن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که این است آن کو به توران زمین</p></div>
<div class="m2"><p>همیشه کمر بسته از بهر کین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو را پیش گردان از ایران ز پس</p></div>
<div class="m2"><p>نماید به انگشت هر کس به کس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگفت این و گرز گران بر کشید</p></div>
<div class="m2"><p>دو رخساره کرده زکین شنبلید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بزد بر سر نامور پهلوان</p></div>
<div class="m2"><p>بر آن بد که از تن برآمدش جان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نجنبید بر زین سرافراز مرد</p></div>
<div class="m2"><p>از آن زخم بر وی نبد هیچ درد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برافراشت بازو به گرز گران</p></div>
<div class="m2"><p>برآورد چو پتک آهنگران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بزد بر سر ترگ ترک دلیر</p></div>
<div class="m2"><p>جهان پهلوان رستم نره شیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نجنبید بر زین گو نامجوی</p></div>
<div class="m2"><p>نیاورد از زخم چین در بروی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو رستم بزد گرز و اندر گذشت</p></div>
<div class="m2"><p>گمانش چنان بد که بر پهن دشت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شد از خون او سرخ روی زمین</p></div>
<div class="m2"><p>به دل در نماندش همی مهر و کین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دل هر دوان شد ز اندیشه خون</p></div>
<div class="m2"><p>که نامد یکی ز آن دوان کس نگون</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به گرز گران گردن افراشتند</p></div>
<div class="m2"><p>همی نعره از چرخ بگذاشتند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو دریا ز کینه بر آشوفتند</p></div>
<div class="m2"><p>به یکدیگر از بر همی کوفتند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یکی جنگ کردند بر سان شیر</p></div>
<div class="m2"><p>نیامد یکی زآن دو از جنگ سیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همی باز گشتند از هم به درد</p></div>
<div class="m2"><p>شده خیره از گنبد لاجورد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دل هردو در بر طپیدن گرفت</p></div>
<div class="m2"><p>خوی و خون ز هردو دویدن گر فت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو از روز یک بهره اندر گذشت</p></div>
<div class="m2"><p>یکی گرد پیدا شد از روی دشت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سپهدار دستان چو آن را بدید</p></div>
<div class="m2"><p>که لشکر ازآن گرد آمد پدید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بر انگیخت باره چو باد دمان</p></div>
<div class="m2"><p>به گردن بر آورد گرز گران</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فرامرز را دید کآمد دوان</p></div>
<div class="m2"><p>خروشان و جوشان چو شیر ژیان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز ایران و زسیستان لشکری</p></div>
<div class="m2"><p>بیاورد هرجا که بد مهتری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فراز آوریده سپه ده هزار</p></div>
<div class="m2"><p>همه جنگ جوی از در کار زار</p></div></div>