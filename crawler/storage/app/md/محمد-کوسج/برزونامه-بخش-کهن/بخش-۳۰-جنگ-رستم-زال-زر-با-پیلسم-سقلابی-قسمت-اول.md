---
title: >-
    بخش ۳۰ - جنگ رستم زال زر با پیلسم سقلابی  قسمت اول
---
# بخش ۳۰ - جنگ رستم زال زر با پیلسم سقلابی  قسمت اول

<div class="b" id="bn1"><div class="m1"><p>وز آن پس به اسب اندر آمد چو باد</p></div>
<div class="m2"><p>ز یزدان نیکی دهش کرد یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمانی به بازو و گرزی به دست</p></div>
<div class="m2"><p>همی تاخت هر سوی چون پیل مست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمندی به فتراک بر شصت خم</p></div>
<div class="m2"><p>دلی پر ز کینه سری پر ز غم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سراسیمه آمد به نزدیک شاه</p></div>
<div class="m2"><p>چو دریای جوشان به دل کینه خواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز آنجا بیامد به ایران سپاه</p></div>
<div class="m2"><p>چو تندر خروشید ز ابر سیاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ایرانیان گفت رستم کجاست</p></div>
<div class="m2"><p>که خواهم به میدان ازو کینه خواست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به میدان بگردیم یک با دگر</p></div>
<div class="m2"><p>به کینه ببندیم هر دو کمر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببینیم تا بر که گردد زمان</p></div>
<div class="m2"><p>همانا سرآید یکی را زمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی گفت و می گشت بر پیش صف</p></div>
<div class="m2"><p>ز کینه همی بر لب آورد کف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دستان مر او را بدان سان بدید</p></div>
<div class="m2"><p>سرشکش ز دیده به رخ بر چکید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیامد به نزدیک رستم چو باد</p></div>
<div class="m2"><p>بدو گفت کای پهلو پاک زاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم آوردت آمد برآرای جنگ</p></div>
<div class="m2"><p>که خواهد همی رستم تیزچنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا سال نزدیک هفتصد رسید</p></div>
<div class="m2"><p>که چشمم چنین نامداری ندید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندانم که فرجام این کار چیست</p></div>
<div class="m2"><p>همی بخت رخشنده خود یار کیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بترسم نباید که چرخ روان</p></div>
<div class="m2"><p>نگردد به کام دل پهلوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز آن پس ز دیده ببارید آب</p></div>
<div class="m2"><p>همی کرد نفرین بر افراسیاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو رستم ز دستان شنید این سخن</p></div>
<div class="m2"><p>دگرگونه اندیشه افکند بن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدو گفت کاین ناله زار چیست</p></div>
<div class="m2"><p>تو را با جهاندار پیکار چیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نبشته نگردد به سر بر دگر</p></div>
<div class="m2"><p>به از تو نداند کس ای نامور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز دیان مگر روی بر تافتی</p></div>
<div class="m2"><p>که از کینه با دیو بشتافتی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بمیرد هر آن کو ز مادر بزاد</p></div>
<div class="m2"><p>نماند به گیتی کسی راد و شاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به نیک و بد چرخ خرسند باش</p></div>
<div class="m2"><p>همیشه مرا از در پند باش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به برزو چنین گفت پس پهلوان</p></div>
<div class="m2"><p>که ای نامور گرد روشن روان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به هر کار باید که در پیش شاه</p></div>
<div class="m2"><p>میان بسته باشی چو من با سپاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نتابی سر از شهریار جهان</p></div>
<div class="m2"><p>به فرمان او بسته داری میان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرا سال افزون شد از چارصد</p></div>
<div class="m2"><p>ندیدم به گیتی یکی روز بد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کنون گر زمانه فراز آمده ست</p></div>
<div class="m2"><p>به تو نوبت جنگ باز آمده ست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر کشته گردم به آوردگاه</p></div>
<div class="m2"><p>نباید که پیچی سر از حکم شاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>میان را ببند از پی کین من</p></div>
<div class="m2"><p>خود و نامداران این انجمن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بفرمود تا رخش را زین کنند</p></div>
<div class="m2"><p>سواران بروها پر از چین کنند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بپوشید تن را به ببر بیان</p></div>
<div class="m2"><p>برآورد بر زه دو زاغ کمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کمندی ببسته به فتراک زین</p></div>
<div class="m2"><p>به زین اندر آمد ز روی زمین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به گردن برآورد گرز گران</p></div>
<div class="m2"><p>دورویه نظاره برو بر سران</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همی راند تا پیش آوردگاه</p></div>
<div class="m2"><p>به نزدیک آن نامور کینه خواه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>درفشش ببردند با او به هم</p></div>
<div class="m2"><p>نبودش به دل اندرون هیچ غم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نگه کرد در وی همان پیلسم</p></div>
<div class="m2"><p>ز دیده ببارید بر روی نم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به رستم چنین گفت کای پهلوان</p></div>
<div class="m2"><p>سرافراز گردان روشن روان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به هنگام کین چو برخاستی</p></div>
<div class="m2"><p>ز پیکار بر دل چه آراستی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که من چون برآوردم از خواب سر</p></div>
<div class="m2"><p>چنین کردم اندیشه ای نامور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که یالت بدوزم به پیکان تیر</p></div>
<div class="m2"><p>کنم روز رخشنده بر زال قیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به گرز گران گردنت بشکنم</p></div>
<div class="m2"><p>به زاولستان آتش اندر زنم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو بشنید رستم بر آشفت سخت</p></div>
<div class="m2"><p>چنین گفت کای مرد شوریده بخت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نیاید ز خرگور پیکار شیر</p></div>
<div class="m2"><p>بخندد بر این گفته مرددلیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چرا غره گشتی به بازوی خویش</p></div>
<div class="m2"><p>بر این برز و بالای و نیروی خویش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>برآوردگه مرد چون تو هزار</p></div>
<div class="m2"><p>گر آیند پیشم نبرده سوار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به دیان که چندان نمانم بر این</p></div>
<div class="m2"><p>که در تک نهد رخش پی بر زمین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به کردار افسانه از جنگ من</p></div>
<div class="m2"><p>همانا شنیدی به هر انجمن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چه کردم به مازندران روز کین</p></div>
<div class="m2"><p>که در بند بد شهریار زمین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو آید کسی را زمانه به سر</p></div>
<div class="m2"><p>به پیکار با من ببندد کمر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شنیدی که کاموس جنگی چه دید</p></div>
<div class="m2"><p>ز خم کمندم چو پیشم کشید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو را آن زمان کشت افراسیاب</p></div>
<div class="m2"><p>که کشتی فکندی بر این روی آب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به افسونگری دیده بی شرم کرد</p></div>
<div class="m2"><p>به گفتار شیرین دو لب نرم کرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فریبنده گشتی به گفتار او</p></div>
<div class="m2"><p>چه دانی تو نیرنگ و کردار او</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بگرید به تو دوده و کشورت</p></div>
<div class="m2"><p>نشیند به ماتم همی مادرت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>فریبنده پیران دهد تاج زر</p></div>
<div class="m2"><p>کسی را که با من ببندد کمر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو ایشان به دریای بیم اندرند</p></div>
<div class="m2"><p>به چاره بکوشند تا بگذرند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو غرقه به هر شاخ یازند دست</p></div>
<div class="m2"><p>که بر موج دریا نشاید نشست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تو را همچو الکوس و دیگر سران</p></div>
<div class="m2"><p>بمانند در زیر گرز گران</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو بینی به میدان تو کردار من</p></div>
<div class="m2"><p>همی راست دانی تو گفتار من</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو بشنید ازو پیلسم این سخن</p></div>
<div class="m2"><p>به پاسخ نگر تا چه افکند بن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به رستم چنین گفت کای پهلوان</p></div>
<div class="m2"><p>دل کارزار و خرد را روان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بساید سپهرت گر از آهنی</p></div>
<div class="m2"><p>ز گشت زمانه همی بشکنی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بگفت این و زان پس به کردار باد</p></div>
<div class="m2"><p>دو زاغ کمان را به زه برنهاد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به تندی بر او تیر باران گرفت</p></div>
<div class="m2"><p>تو گفتی جهان باد و باران گرفت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو رستم چنان دید از پیلسم</p></div>
<div class="m2"><p>کمان کیانی برآورد هم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دو ترکش ز پیکان بپرداختند</p></div>
<div class="m2"><p>دل از کینه چون آب بگداختند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سپرها به دست اندرون بیشه شد</p></div>
<div class="m2"><p>دل نامداران پر اندیشه شد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دل پهلوانان شد از غم به درد</p></div>
<div class="m2"><p>به مردی برآورد از مهر گرد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به رستم چنین گفت پس پیلسم</p></div>
<div class="m2"><p>که گردون ز تیر تو بودی به خم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تو گفتی که پیکان من روز جنگ</p></div>
<div class="m2"><p>ز بیمش بسوزد به دریا نهنگ</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>همه خام بوده ست گفتار تو</p></div>
<div class="m2"><p>چو دیدم برآورد کردار تو</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چه یازی به چاره به هر سوی جنگ</p></div>
<div class="m2"><p>چه داری به یاد از نبرد پلنگ</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بیاور که بینند تورانیان</p></div>
<div class="m2"><p>همان نامداران ایرانیان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تو آنی که گفتی چو من نیست کس</p></div>
<div class="m2"><p>به مردی کنم باد را در قفس</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>پسنده ست گفتار و کردار خود</p></div>
<div class="m2"><p>چه داری ز نیرنگ و گفتار خود</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>برآشفت رستم به سان پلنگ</p></div>
<div class="m2"><p>ز کینه بیازید چون شیر چنگ</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز فتراک بگشاد پیچان کمند</p></div>
<div class="m2"><p>برآمد خروشش به ابر بلند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>دل پهلوان شد ازو پر ز خشم</p></div>
<div class="m2"><p>بدو در نگه کرد رستم به چشم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو ترک آن چنان دید بر سان باد</p></div>
<div class="m2"><p>کمندش ز فتراک زین برگشاد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بینداخت آن تاب داده کمند</p></div>
<div class="m2"><p>بدان تا سر رستم آمده به بند</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ز یکدیگران روی برگاشتند</p></div>
<div class="m2"><p>به پروین همی نعره برداشتند</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>همی زور کرد این بر آن،آن بر این</p></div>
<div class="m2"><p>نجنبید یک مرد بر پشت زین</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چو زال آن چنان دید آمد فرود</p></div>
<div class="m2"><p>همی داد نیکی دهش را درود</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به پیش جهاندار بر خاک سر</p></div>
<div class="m2"><p>نهاد و ببارید خون جگر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نیایش کنان پیش دیان پاک</p></div>
<div class="m2"><p>بمالید رخ را بر آن تیره خاک</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چنین گفت کای کردگار جهان</p></div>
<div class="m2"><p>شناسنده آشکار و نهان</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>تو دانی که رستم به پیش کیان</p></div>
<div class="m2"><p>همی بسته دارد همیشه میان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>مر او را بر این ترک پرخاشخر</p></div>
<div class="m2"><p>بر این دشت گردانش پیروزگر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>وزین سو به میدان دو گرد دلیر</p></div>
<div class="m2"><p>همی زو کردند بر سان شیر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز بس تاب و نیروی هر دو سوار</p></div>
<div class="m2"><p>کمند کیانی نبد پایدار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>گسسته شد آن تاب داده کمند</p></div>
<div class="m2"><p>نیامد یکی را از آن دو گزند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>دل هر دوان گشت از رزم سیر</p></div>
<div class="m2"><p>به میدان کینه درون هر دو شیر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>فرو مانده بد هر دو گردان به جای</p></div>
<div class="m2"><p>ندانست ایشان یکی سر ز پای</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>پر از خون دو دیده، پر از خاک سر</p></div>
<div class="m2"><p>ز کینه گسسته دوال کمر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ز یکدیگران بازگشتند به درد</p></div>
<div class="m2"><p>دل هر دو پرخون و رخ گشته زرد</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ز جان سیری آمد تن هر دوان</p></div>
<div class="m2"><p>همان سال خورده همان نوجوان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ازین پس چنین گفت رستم بدوی</p></div>
<div class="m2"><p>که ای نامور شیر پرخاش جوی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>به میدان ببندیم هر دو کمر</p></div>
<div class="m2"><p>به کشتی بکوشیم یک با دگر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>و گر ما نشینیم تا دیگران</p></div>
<div class="m2"><p>نمایند مردی به گرز گران</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چه فرمایی اکنون چه جنگ آوریم</p></div>
<div class="m2"><p>که تا نام مردی به چنگ آوریم</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چو بشنید ازو این سخن پیلسم</p></div>
<div class="m2"><p>دلش گشت از آن کار او پر ز غم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بر آن بر همی رفت بایست اوی</p></div>
<div class="m2"><p>به میدان کینه درافکند گوی(؟)</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چنین گفت با رستم نامور</p></div>
<div class="m2"><p>به کشتی ببندیم هر دو کمر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بگفت این و از باره به زیر</p></div>
<div class="m2"><p>چو ارغنده ببر و چو درنده شیر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>به یک سو کشیدند ز آوردگاه</p></div>
<div class="m2"><p>دورویه نظاره بر ایشان سپاه</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>جهان پهلوان رستم پاک زاد</p></div>
<div class="m2"><p>جهان آفریننده را کرد یاد</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>به کشتی گرفتن ببستش میان</p></div>
<div class="m2"><p>سرافراز ایران و پشت کیان</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>همی کرد از داور پاک یاد</p></div>
<div class="m2"><p>ز شاه سرافراز گردون نهاد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>که شاه و سپهبد مرا یاد باد</p></div>
<div class="m2"><p>دل دشمنانش پر از داد باد</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>به دل بر نبودش ز بدخواه باک</p></div>
<div class="m2"><p>همی گشت زال اندر آن تیره خاک</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>جهان پهلوان رستم نره شیر</p></div>
<div class="m2"><p>که هرگز نگشتی ز پیکار سیر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>میان کیانی به کینه ببست</p></div>
<div class="m2"><p>بر آن خاک تیره بزد هر دو دست</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>به بند کمر برزده پالهنگ</p></div>
<div class="m2"><p>به کشتی گرفتن نهاده دو چنگ</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بپیچیده از کینه هر دو به هم</p></div>
<div class="m2"><p>هم آن نامور مرد و هم پیلسم</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>دورویه نظاره بر آن هر دو تن</p></div>
<div class="m2"><p>بدان تا که پوشد ز خفتان کفن</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>سپهر از روش باز مانده ز بیم</p></div>
<div class="m2"><p>دل پیلسم گشته از غم دو نیم</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>جهان جوی افراسیاب دلیر</p></div>
<div class="m2"><p>بیامد به آوردگه همچو شیر</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>درفش سیاه اژدها پیکرش</p></div>
<div class="m2"><p>برافراخته از فراز سرش</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بدان تا ببیند کز آن هر دوان</p></div>
<div class="m2"><p>زمانه که را بر سر آرد زمان</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>تبیره خروشان ز هر دو گروه</p></div>
<div class="m2"><p>دل نامداران ز غم شد ستوه</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>چو رستم جهان را بر آن گونه دید</p></div>
<div class="m2"><p>خروشی چو شیر ژیان برکشید</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بدو گفت کای ترک شوریده بخت</p></div>
<div class="m2"><p>که بر تو بگرید همی تاج و تخت</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>به دل در نداری همی تاب جنگ</p></div>
<div class="m2"><p>چه یازی به چاره به هر سوی چنگ</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>به میدان به هر سوی تازی ز بیم</p></div>
<div class="m2"><p>تو گویی دلت گشت از غم دو نیم</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>به گرز گران و به تیر و کمان</p></div>
<div class="m2"><p>به زاولستانت کنم میهمان</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>نبینی دگر مرز سقلاب و روم</p></div>
<div class="m2"><p>بزرگان و گردان آن مرز و بوم</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>سپهدار ترکان ز چنگال شیر</p></div>
<div class="m2"><p>همی جست از آواز مرد دلیر</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>گرازان و تازان برآوردگاه</p></div>
<div class="m2"><p>جهان پهلوان رستم رزم خواه</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>سپهدار رستم بر آن کار زار</p></div>
<div class="m2"><p>به گردون برآورده سر نامدار</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>چو دریای جوشان برآورده جوش</p></div>
<div class="m2"><p>به گردنده گردون رسانده خروش</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>به یکدیگران بر بپیچیده سخت</p></div>
<div class="m2"><p>به کردار پیچان دو شاخ درخت</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>دو بازوی هر دو به گرد کمر</p></div>
<div class="m2"><p>چو پیچان دو خرطوم بر یکدگر</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>گرفته کمرگاه گردان به چنگ</p></div>
<div class="m2"><p>چو شیران آشفته تیزچنگ</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>ز خون و ز خوی خاک آوردگاه</p></div>
<div class="m2"><p>شد آغشته تا پشت ماهی و ماه</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>ز نیرو چو دو طاس خون کرده چشم</p></div>
<div class="m2"><p>دل هر دو در تن پر از کین و خشم</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>گسسته شد از زور گردان کمر</p></div>
<div class="m2"><p>ز مردی نیفتاد یک نامور</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>دل هر دو در بر طپیدن گرفت</p></div>
<div class="m2"><p>خوی و خون ز هر دو دویدن گرفت</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>فروماند بازوی گندآوران</p></div>
<div class="m2"><p>تو گفتی ندارند در تن روان</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>نشستند از دور هر دو خموش</p></div>
<div class="m2"><p>به آواز شیپور بنهاده گوش</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>زمانی به آسودگی دم زدند</p></div>
<div class="m2"><p>ز دیده به رخسار بر نم زدند</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو آسوده گشتند بار دگر</p></div>
<div class="m2"><p>به کشتی گرفتن نهادند سر</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>سپهدار برزو بیامد دوان</p></div>
<div class="m2"><p>به رستم چنین گفت کای پهلوان</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>گران کن رکیب و سبک کن عنان</p></div>
<div class="m2"><p>برو شادمان نزد ایرانیان</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>برآسای تا من ببندم میان</p></div>
<div class="m2"><p>به کشتی گرفتن چو شیر ژیان</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>به برزو چنین گفت پس نامدار</p></div>
<div class="m2"><p>به هر کار یزدان مرا هست یار</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>بگفت این و آن گه چو شیر ژیان</p></div>
<div class="m2"><p>بیامد به میدان کینه دمان</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>چنین گفت با نامور پیلسم</p></div>
<div class="m2"><p>بیا که تا بگردیم دیگر به هم</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>وزین روی پیران بیامد دوان</p></div>
<div class="m2"><p>به آوردگه بر چو پیل دمان</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>بدو گفت افراسیاب دلیر</p></div>
<div class="m2"><p>ستاده ست در پیش صف همچو شیر</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>همی گوید ای نامور پهلوان</p></div>
<div class="m2"><p>چو باز آیی از دشت روشن روان</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>همه مرز ایران و توران تو راست</p></div>
<div class="m2"><p>زمانه سراسر به فرمان تو راست</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>چو بشنید زو پیلسم گشت شاد</p></div>
<div class="m2"><p>نیایشگری را زبان بر گشاد</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>ز شادی ببستش کمر بر میان</p></div>
<div class="m2"><p>در آمد به میدان کینه دمان</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>بر رستم آمد چو آشفته شیر</p></div>
<div class="m2"><p>بدو گفت کای پهلوان دلیر</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>بیا تا ببینم کاین کوژپشت</p></div>
<div class="m2"><p>همی با که گردد کینه درشت</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>بدو گفتم رستم که دل شاد دار</p></div>
<div class="m2"><p>همه رنج بگذشته را باد دار</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>بگفت این و آمد به نزدش فراز</p></div>
<div class="m2"><p>جهان پهلوان رستم سر فراز</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>چو با اژدهای دمان شیر نر</p></div>
<div class="m2"><p>به کشتی بر آویخت با نامور</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>همی زور کرد این بر آن،آن بر این</p></div>
<div class="m2"><p>نیامد ز مردی یکی بر زمین</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>که را بخت بد گشت همداستان</p></div>
<div class="m2"><p>نباشد کسش نیز هم داستان (؟)</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>به گیتی نگیرد کس او را به چیز</p></div>
<div class="m2"><p>به نزد گرامی شودخوار نیز</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>زمانه چو آمد به تنگی فراز</p></div>
<div class="m2"><p>نگردد به مردی و نیرنگ باز</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>سپهدار ترکان چو برگشت بخت</p></div>
<div class="m2"><p>بلرزید مانند شاخ درخت</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>تو گفتی که گردون دو دستش ببست</p></div>
<div class="m2"><p>دل شاه ترکان ز کینه بخست</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>بیازید رستم دو پایش به کین</p></div>
<div class="m2"><p>به گردن بر آورد و زد بر زمین</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>نشست از بر سینه پیلسم</p></div>
<div class="m2"><p>بر آمد خروشیدن گاودم</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>ببستش به خم کمند اندرون</p></div>
<div class="m2"><p>ببارید بد گوهر از دیده خون</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>بنالید (و) از درد دل ناله کرد</p></div>
<div class="m2"><p>ز دیده همی رخ پر از ژاله کرد</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>به رخش اندر آمد سپهبد دوان</p></div>
<div class="m2"><p>همی تاخت بر دشت روشن روان</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>چو آمد به نزدیک دستان سام</p></div>
<div class="m2"><p>سپهدار برزوی فرخنده نام</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>بیامد به نزدیک رستم فراز</p></div>
<div class="m2"><p>زمین را ببوسید و بردش نماز</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>ز دست جهان پهلوان بستدش</p></div>
<div class="m2"><p>ز کینه همی بر زمین بر زدش</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>که پهلو و پشتش به هم در شکست</p></div>
<div class="m2"><p>سر کینه ور گشت با خاک پست</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>وز آنجا بیاورد او را به راه</p></div>
<div class="m2"><p>بدان تا ببیند دورویه سپاه</p></div></div>