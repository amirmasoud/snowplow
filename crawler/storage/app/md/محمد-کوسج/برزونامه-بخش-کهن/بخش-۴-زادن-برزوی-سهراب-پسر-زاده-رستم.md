---
title: >-
    بخش ۴ - زادن برزوی سهراب، پسر زاده رستم
---
# بخش ۴ - زادن برزوی سهراب، پسر زاده رستم

<div class="b" id="bn1"><div class="m1"><p>ز شنگان چو سهراب آمد به در</p></div>
<div class="m2"><p>شده بود شهرو ازو بارور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نه ماه بگذشت از آن روزگار</p></div>
<div class="m2"><p>درخت قضا رفته آورد بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فرمان دیان جدا شد از وی</p></div>
<div class="m2"><p>دل افروز پوری چو خورشید روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برش چون بر شیر و چهرش چو خون</p></div>
<div class="m2"><p>سطبرش دو بازو چو ران هیون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل افروز مادر بد آن شمع روز</p></div>
<div class="m2"><p>سپهر یلان،گرد گیتی فروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهانجوی را نام برزوی کرد</p></div>
<div class="m2"><p>به دیدار او دل به نیروی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان سان همی پروریدش به ناز</p></div>
<div class="m2"><p>که نامد به چیزیش روزی نیاز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدش گشت با سرو نازنده راست</p></div>
<div class="m2"><p>چنان بود فرمان دیان که خواست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بگذشت از عمر او بیست سال</p></div>
<div class="m2"><p>پهن کرد سینه قوی کرد یال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نهان کرد مادر ازو راز خویش</p></div>
<div class="m2"><p>همی داشت او را هم آواز خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز کردار خود هیچ با او نگفت</p></div>
<div class="m2"><p>همی داشت آن راز را در نهفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دل گفت اگر من بگویم بدوی</p></div>
<div class="m2"><p>که تو پور سرخابی ای ماهروی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بپوید ز شنگان به ایران شود</p></div>
<div class="m2"><p>به پرخاش آن نره شیران شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم او چون پدر رزم رای آیدش</p></div>
<div class="m2"><p>یکی تنگ تابوت جای آیدش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نباید که همچون پدر زاروار</p></div>
<div class="m2"><p>شود کشته در دشت (و) در کارزار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به تدبیر، تقدیر برگشت خواست</p></div>
<div class="m2"><p>چنان خواست دیان که مادر نخواست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو پورش ابا یال و نیروی بود</p></div>
<div class="m2"><p>تو گفتی که از آهن و روی بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به برزیگری داشت مادر ورا</p></div>
<div class="m2"><p>که بودش بسی ملک اندر خورا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهان جوی از تخمه راستان</p></div>
<div class="m2"><p>به برزیگری گشت هم داستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی مرد عام کشاورز بود</p></div>
<div class="m2"><p>اگر چه خداوند صد مرز بود</p></div></div>