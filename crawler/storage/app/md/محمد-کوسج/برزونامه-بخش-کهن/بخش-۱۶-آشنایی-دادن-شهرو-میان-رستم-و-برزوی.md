---
title: >-
    بخش ۱۶ - آشنایی دادن شهرو میان رستم و برزوی
---
# بخش ۱۶ - آشنایی دادن شهرو میان رستم و برزوی

<div class="b" id="bn1"><div class="m1"><p>نگه کرد شهرو چو آن را بدید</p></div>
<div class="m2"><p>خروشی چو شیر ژیان بر کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیامد دوان تا به آوردگاه</p></div>
<div class="m2"><p>چنین گفت با رستم کینه خواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که ای نامور پهلوان جهان</p></div>
<div class="m2"><p>سر افرازتر کس میان مهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو را شرم ناید ز دیان پاک</p></div>
<div class="m2"><p>که چونین جوانی برین تیره خاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زاری برآری روان از تنش</p></div>
<div class="m2"><p>ز خون سرخ گردد همه جوشنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز نسل نریمان و فرزند تو</p></div>
<div class="m2"><p>نبیره جهاندار و پیوند تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو را او نبیره ست و هستی نیا</p></div>
<div class="m2"><p>برو دل چه داری پر از کیمیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان جوی، فرزند سهراب گرد</p></div>
<div class="m2"><p>بدین زور بازو و این دست برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخواهیش کشتن برین گونه خوار</p></div>
<div class="m2"><p>نترسی ز دیان پروردگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که گاهی نبیره کشی گاه پور</p></div>
<div class="m2"><p>بهانه تو را کین ایران و تور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو را خود به دیده درون شرم نیست</p></div>
<div class="m2"><p>جهان را به نزدیکت آزرم نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی گفت و میراند خون جگر</p></div>
<div class="m2"><p>همه خاک آورد کرده به سر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو گفت رستم که ای شهره زن</p></div>
<div class="m2"><p>مرا اندرین داستانی بزن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه گویی مگر خواب گویی همی</p></div>
<div class="m2"><p>بدین دشت چاره چه جویی همی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نباشد نژاد نریمان نهان</p></div>
<div class="m2"><p>میان کهان و میان مهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز سهراب گرد است این را نژاد؟</p></div>
<div class="m2"><p>بباید همی راز بر من گشاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو دارد ز زال و نریمان نشان</p></div>
<div class="m2"><p>چرا رزم جوید چو گردن کشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه سر به سر پیش من بازگوی</p></div>
<div class="m2"><p>به ژرفی نگه کن بهانه مجوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مجوی اندرین ره به جز راستی</p></div>
<div class="m2"><p>نباید که آری به تن کاستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ورا گفت شهرو که ای پهلوان</p></div>
<div class="m2"><p>زبانم نگردد همی در دهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مگر خنجر از دست بیرون کنی</p></div>
<div class="m2"><p>زمانی برین خسته افسون کنی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بترسم چو رستم بجنبد ز جای</p></div>
<div class="m2"><p>بگرداند این تیغ زن را ز پای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهان جوی برزوی را بسته دست</p></div>
<div class="m2"><p>بیامد دمان پیش رستم نشست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفت کای پهلوان جهان</p></div>
<div class="m2"><p>فروزنده چون خور میان مهان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدان گه که سهراب شد پهلوان</p></div>
<div class="m2"><p>سر افراز و نامی میان گوان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فسیله بر آن کوه ما داشتی</p></div>
<div class="m2"><p>شب و روز آنجای بگذاشتی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدان گه که سر کرد بر رزم و کین</p></div>
<div class="m2"><p>همی کرد آهنگ ایران زمین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیامد به نزد فسیاه دمان</p></div>
<div class="m2"><p>ابا وی سپاهی چو شیر ژیان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدان چشمه آمد زمانی فرود</p></div>
<div class="m2"><p>همی داد نیکی دهش را درود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پدر بد مرا نامداری دلیر</p></div>
<div class="m2"><p>همه ساله بودی به نخجیر شیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به فرمان دادار پروردگار</p></div>
<div class="m2"><p>پدر بود آن روز اندر شکار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به دز بر به جز من دگر کس نبود</p></div>
<div class="m2"><p>کجا داد دیان ازین گونه بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به ناگاه ایمن ز کردار بد</p></div>
<div class="m2"><p>برون آمدم من چو آشفته دد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برهنه سر و پای و بر سر سبوی</p></div>
<div class="m2"><p>به نزدیک چشمه شدم پوی پوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جهان جوی از خیمه چون بنگرید</p></div>
<div class="m2"><p>برهنه سر و پای و رویم بدید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دلش گشت مهر مرا خواستار</p></div>
<div class="m2"><p>یکی را بفرمود کو را بیار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرا برد نزدیک او زنده رزم</p></div>
<div class="m2"><p>بدان تا بماند زمانی ز رزم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به افسونگری دیده بی شرم کرد</p></div>
<div class="m2"><p>به شیرین زبانی مرا نرم کرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر آن سان که آیین مردان بود</p></div>
<div class="m2"><p>همان نیز فرمان دیان بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به چاره در آورد پایم به دام</p></div>
<div class="m2"><p>برون کرد شمشیر کین از نیام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به مردانگی کام دل برگرفت</p></div>
<div class="m2"><p>به چاره مرا تنگ در بر گرفت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو از راه من گشت آگاه شیر</p></div>
<div class="m2"><p>سرافراز و نامی و گرد دلیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گرفتم از آن نامور شیر بر</p></div>
<div class="m2"><p>ز اندیشه افکند در پیش سر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرا نامور گرد آواز داد</p></div>
<div class="m2"><p>مرا گفت گردون تو را ساز داد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که گشتی ز سهراب یل بارور</p></div>
<div class="m2"><p>ز تخم جهان پهلوان زال زر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>برون کرد از انگشت انگشتری</p></div>
<div class="m2"><p>نگینی فروزنده چون مشتری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چنین گفت با من که این گوش دار</p></div>
<div class="m2"><p>بدانچت بگویم همی هوش دار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نگه دار این چون پسر آیدت</p></div>
<div class="m2"><p>همی رنج گیتی به سر آیدت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به هنگام آن کو شود کینه ور</p></div>
<div class="m2"><p>ببندد به پیکار جستن کمر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بگویش که دارد مر این را نگاه</p></div>
<div class="m2"><p>که باشد فروزنده چون مهر و ماه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وگر دختر آید به سان پری</p></div>
<div class="m2"><p>در انگشت او باید انگشتری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بگفت این و آن گاه اندر زمان</p></div>
<div class="m2"><p>به اسب اندر آمد چو باد بزان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بیامد به ایران به دیدار تو</p></div>
<div class="m2"><p>دگرگونه بد رای و گفتار تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جهان جوی برزو شد از من جدا</p></div>
<div class="m2"><p>به مانند سهراب نر اژدها</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به شنگان خود او بود دمساز من</p></div>
<div class="m2"><p>نگفتم بدو هیچ ازین راز من</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به برزیگری گشت همداستان</p></div>
<div class="m2"><p>بخواندم برو نامه باستان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدان تا نجوید همی ساز جنگ</p></div>
<div class="m2"><p>سرش را همی داشتم زیر سنگ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نباید که همچون پدر روزگار</p></div>
<div class="m2"><p>شود کشته در دشت پیکار زار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز ناگه یکی روز افراسیاب</p></div>
<div class="m2"><p>بدو باز خورد همچو دریای آب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نبیره ست روز و رستم نیا</p></div>
<div class="m2"><p>به چاره بجستم همی کیمیا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بدو گفت بنمای انگشتری</p></div>
<div class="m2"><p>چه داری نهانش به سان پری</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بدو داد انگشتری شهره زن</p></div>
<div class="m2"><p>برهنه رخان پیش آن انجمن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نگه کرد رستم بدان بنگرید</p></div>
<div class="m2"><p>نگین جفت آن مهره خویش دید</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بخندید چون گل رخ تاج بخش</p></div>
<div class="m2"><p>ز هامون بر آمد بر افراز رخش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به برزوی شیراوزن آواز داد</p></div>
<div class="m2"><p>که گردون گردان تو را ساز داد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز هامون بر افراز باره نشین</p></div>
<div class="m2"><p>به نزدیک گردان ایران زمین</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو بشنید برزو رستم سخن</p></div>
<div class="m2"><p>بدو گفت کای سرور انجمن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>از آن پیش تا نزد ایرانیان</p></div>
<div class="m2"><p>شوی شاد کن جان تورانیان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به من بخش رویین و آن لشکرش</p></div>
<div class="m2"><p>بدان تا شود شاد زی کشورش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بگوید به توران مر این کیمیا</p></div>
<div class="m2"><p>که برزو نبیره ست و رستم نیا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دگر آنکه گر او نبودی مگر</p></div>
<div class="m2"><p>شدی زهر گرگین به ما کارگر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو بشنید رستم ز برزو چنین</p></div>
<div class="m2"><p>بدو گفت کای نامور شیر کین</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نیازارد او را کسی زین سپاه</p></div>
<div class="m2"><p>بگو تا شود شاد و ایمن به راه</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>وز آنجای بر سان باد دمان</p></div>
<div class="m2"><p>برفتند برزوی و رستم دوان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>رسیدند نزدیک ایرانیان</p></div>
<div class="m2"><p>دو پیل سر افراز و شیر ژیان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو رستم به نزدیک گردان رسید</p></div>
<div class="m2"><p>ز شادی به دل نعره ای بر کشید</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بدیشان چنین گفت کاین نامور</p></div>
<div class="m2"><p>که بد بسته با ما به کینه کمر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>دل ما ازو پر غم و تاب گشت</p></div>
<div class="m2"><p>به فرجام فرزند سهراب گشت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو رستم چنین گفت ایرانیان</p></div>
<div class="m2"><p>به شادی گشادند یکسر میان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>زواره به مژده بتازید اسب</p></div>
<div class="m2"><p>به نزدیک دستان چو آذرگشسب</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>همه سیستان یکسر آذین ببست</p></div>
<div class="m2"><p>به هر جای مردم به شادی نشست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ز دروازه آمد برون پور سام</p></div>
<div class="m2"><p>خود و پهلوانان فرخنده نام</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>(بیامد چو برزو مر او را بدید</p></div>
<div class="m2"><p>پیاده شد و پیش دستان دوید)</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>(به بر درگرفتش ورا زال زر</p></div>
<div class="m2"><p>همی شاد شد زو دل کینه ور)</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بپرسید ایرانیان را همه</p></div>
<div class="m2"><p>که او چون شبان بود و ایشان رمه)</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>نهادند سر سوی ایوان شتاب</p></div>
<div class="m2"><p>خود و نامداران با جاه و آب)</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به خوردن نهادند سر ها همه</p></div>
<div class="m2"><p>چه مهتر چه کهتر شبان و رمه</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چو برگشت رویین از آن رزمگاه</p></div>
<div class="m2"><p>همی رفت خسته به بی راه و راه</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بیامد به نزدیک پیران چو باد</p></div>
<div class="m2"><p>همی کرد از آن لشکر گشن یاد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>همه شهر دیدش پر از تاب و توش</p></div>
<div class="m2"><p>ز مستان به گردون رسیده خروش</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بپرسید رویین که این بزم چیست</p></div>
<div class="m2"><p>به ایوان ما در فزونی ز کیست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>یکی گفت کافراسیاب دلیر</p></div>
<div class="m2"><p>بیامد بدین شهر پیران چو شیر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بزرگان توران دو بهره سوار</p></div>
<div class="m2"><p>فزونند با او ز بهر شکار</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>سپاه سپهبد همه گشته شاد</p></div>
<div class="m2"><p>چو بشنید رویین به کردار باد</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بیامد شتابان بر شهریار</p></div>
<div class="m2"><p>ز اندیشه خسته دل نامدار</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به پیران خبر برد سالار بار</p></div>
<div class="m2"><p>که آمد سپهبد ز دشت شکار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>خروشی بر آمد ز شهر ختن</p></div>
<div class="m2"><p>وزان نامداران لشکر شکن</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چو رویین به نزدیک خسرو رسید</p></div>
<div class="m2"><p>سرشکش ز دیده به رخ بر چکید</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>زمین را ببوسید رویین گرد</p></div>
<div class="m2"><p>به مژگان همی خاک خانه سترد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چو افراسیاب دلیرش بدید</p></div>
<div class="m2"><p>بدو گفت کای نامور چه رسید</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چه افتاد مر پهلوان زاده را</p></div>
<div class="m2"><p>نباید به غم جان آزاده را</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>همانا که خستی ز دشت شکار</p></div>
<div class="m2"><p>وگر گشتی از میهمان سوگوار</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چو بشنید رویین زبان برگشاد</p></div>
<div class="m2"><p>که جاوید بادا سرافراز شاد</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>وزان پس همه کار برزو بدوی</p></div>
<div class="m2"><p>بگفتش که چون بود پیکار اوی</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ز شهرو و بهرام گوهر فروش</p></div>
<div class="m2"><p>سپاه و سپهبد بدو داده هوش</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ز نیرنگ و افسون و مرغ و شرنگ</p></div>
<div class="m2"><p>برآورد گشتن به سان پلنگ</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>به فرجام فرزند سهراب گشت</p></div>
<div class="m2"><p>وزان پس مرا دیده پر آب گشت</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>وزو شادمان(شد) دل تاج بخش</p></div>
<div class="m2"><p>سوی سیستان راند چون باد رخش</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چو بشنید افراسیاب این سخن</p></div>
<div class="m2"><p>بدو تازه شد باز درد کهن</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بزد دست و جامه به تن بر درید</p></div>
<div class="m2"><p>خروشی چو شیر ژیان بر کشید</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>همی کند موی و همی ریخت آب</p></div>
<div class="m2"><p>ز دیده سپهدار افراسیاب</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>همی گفت و خود جای گفتار بود</p></div>
<div class="m2"><p>که با او زمانه به پیکار بود</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چه گویید و تدبیر این کار چیست</p></div>
<div class="m2"><p>بدین رزم برزو مرا یار کیست</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>همانا که از ما بتابید بخت</p></div>
<div class="m2"><p>ز توران بخواهد شدن تاج و تخت</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>نخواهد گسستن همی تخم سام</p></div>
<div class="m2"><p>به گردون بر آمد ازین تخمه نام</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چه گویم یکی رفت، آید دگر</p></div>
<div class="m2"><p>ببندد درین کینه جستن کمر</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>ز دستان بد آمد به تورانیان</p></div>
<div class="m2"><p>برین نامداران و گند آوران</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>تو گویی بر آن تخمه گردان سپهر</p></div>
<div class="m2"><p>به خوبی نموده ست جاوید چهر</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>به کینه جهان پهلوان زین سپس</p></div>
<div class="m2"><p>نماند به توران زمین هیچ کس</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>نیاسایدش تیغ اندر نیام</p></div>
<div class="m2"><p>چو با او بپیوست برزو و سام</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ازو بود پیوسته جانم به بیم</p></div>
<div class="m2"><p>ز بیم نهیبش دل من دو نیم</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>کنون یاری امد مر او را به جنگ</p></div>
<div class="m2"><p>چو آشفته شیران و شرزه پلنگ</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>به ایران و توران چو برزوی کیست</p></div>
<div class="m2"><p>برین کشور ما بباید گریست</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>ندانم چه آرد به ما بر سپهر</p></div>
<div class="m2"><p>که ببرید از ما به یکبار مهر</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>چو برزو نبیره چو رستم نیا</p></div>
<div class="m2"><p>نماند ازین تخمه گردی به پا</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ز رستم همی بود توران به جوش</p></div>
<div class="m2"><p>دل نامداران نوان با خروش</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>چو تنها بدی در صف کارزار</p></div>
<div class="m2"><p>چه یک مرد پیشش چه پنجه هزار</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>کنون چون دو گردند برزوی و اوی</p></div>
<div class="m2"><p>ز خون بزرگان برانند جوی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>از آن پس نبندند تورانیان</p></div>
<div class="m2"><p>به کینه کمر پیش ایرانیان</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>کس این داستان در زمانه نخواند</p></div>
<div class="m2"><p>به توران همی خاک باید فشاند</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>بزرگان توران پر از خون جگر</p></div>
<div class="m2"><p>ز آب دو دیده همه روی تر</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>بدو گفت لشکر که ای شهریار</p></div>
<div class="m2"><p>نباشد چو تو در جهان نامدار</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>نبیره فریدون و پور پشنگ</p></div>
<div class="m2"><p>به دریا گریزان ز بیمت نهنگ</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>به کینه چو شیرو، به نیرو چو پیل</p></div>
<div class="m2"><p>به دل ابر بهمن، به کف رود نیل</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>چو بر پشت شبرنگ گردی سوار</p></div>
<div class="m2"><p>چه یک تن به پیشت چه سیصد هزار</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>کنون این همه بیم و زاری چراست</p></div>
<div class="m2"><p>چنین پهلوی یال و بازو که راست</p></div></div>