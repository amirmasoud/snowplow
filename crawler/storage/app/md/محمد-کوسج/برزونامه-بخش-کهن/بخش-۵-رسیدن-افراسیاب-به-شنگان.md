---
title: >-
    بخش ۵ - رسیدن افراسیاب به شنگان
---
# بخش ۵ - رسیدن افراسیاب به شنگان

<div class="b" id="bn1"><div class="m1"><p>از آن پس که برگشت از آن رزمگاه</p></div>
<div class="m2"><p>که رستم برو کرد گیتی سیاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که از بهر بیژن به توران زمین</p></div>
<div class="m2"><p>چه آمد به روی سپهدار چین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان راه بی ره سر اندر کشید</p></div>
<div class="m2"><p>گریزان ز رستم به شنگان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود و نامداران پرخاشخر</p></div>
<div class="m2"><p>پر از درد جان و پر از کین جگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسیدند نزدیکی آن حصار</p></div>
<div class="m2"><p>که بد پهلوان اندرو شادخوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پیران و گرسیوز و شاه چین</p></div>
<div class="m2"><p>رسیدند نزدیک شنگان زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان چشمه آمد زمانی فرود</p></div>
<div class="m2"><p>همی داد هر کس روان را درود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شه چین ز ناگه یکی بنگرید</p></div>
<div class="m2"><p>کشاورز مردی تناور بدید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ستاده بر آن دشت همچون هیون</p></div>
<div class="m2"><p>به تن همچو کوه و به چهره چو خون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گشاده برو ساعد و یال و برز</p></div>
<div class="m2"><p>درختیش در دست مانند گرز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قوی گردن و سینه و بر فراخ</p></div>
<div class="m2"><p>به تن چون درخت و به بازو چو شاخ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>(چو افراسیابش بدان سان بدید</p></div>
<div class="m2"><p>به پیران ویسه یکی بنگرید)</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدان نامداران چنین گفت پس</p></div>
<div class="m2"><p>که زین سان دلاور ندیده ست کس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا سال بگذشت بر چارصد</p></div>
<div class="m2"><p>ازین سان ندیدم نه مردم نه دد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه سام نریمان نه گرشاسپ گرد</p></div>
<div class="m2"><p>نه چشم یلان نیز چونین شمرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ستاده ست از آن گونه بر پهن دشت</p></div>
<div class="m2"><p>ازین سان سپاهی برو برگذشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیامدش در دل ز ما هیچ باک</p></div>
<div class="m2"><p>چه ماییم پیشش چه یک مشت خاک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفت این و بادی ز دل برکشید</p></div>
<div class="m2"><p>به کردار دریا دلش بردمید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به رویین چنین گفت باره بران</p></div>
<div class="m2"><p>بیاور مرا او را به نزدم دوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدان تا بدانم که از تخم کیست</p></div>
<div class="m2"><p>چه گوید برین دشت از بهر چیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو بشنید رویین پیران چو شیر</p></div>
<div class="m2"><p>بیامد به نزدیک مرد دلیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو گفت کای مرد دهقان پژوه</p></div>
<div class="m2"><p>چه باشی بدین دشت با این گروه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شه چین و ماچین همی خواندت</p></div>
<div class="m2"><p>بدان تا از این رنج برهاندت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جهاندار افراسیاب دلیر</p></div>
<div class="m2"><p>که روبه ستاند ز چنگال شیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو بشنید برزوی آواز اوی</p></div>
<div class="m2"><p>چو گلبرگ بفروخت از راز اوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به رویین چنین گفت کای بی خرد</p></div>
<div class="m2"><p>نیاید تو را خنده از گفت خود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهاندار دادار دادآور است</p></div>
<div class="m2"><p>که روزی ده بندگان یکسر است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه گویی کنون کیست پور پشنگ</p></div>
<div class="m2"><p>چرا آمد ایدر بدین راه تنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیایم به گفتار تو پیش اوی</p></div>
<div class="m2"><p>که دانم ز هر بد کمابیش اوی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو بشنید رویین بدو گفت بس</p></div>
<div class="m2"><p>نگوید سخن را بدین گونه کس</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نبیره فریدون دلارای کین</p></div>
<div class="m2"><p>سر سروران شاه توران زمین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز دیان مگر روی بر تافتی</p></div>
<div class="m2"><p>و یا بر ره دیو بشتافتی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز فرمان شاهان نتابند سر</p></div>
<div class="m2"><p>یکی داشت با حکم پیروزگر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز دانا شنیدم به هر روزگار</p></div>
<div class="m2"><p>که فرمان شاهان مدارید خوار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو رویین چنین گفت برزوی برز</p></div>
<div class="m2"><p>بدو گفت کای مرد بی آب و ارز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر آن شاه کو دادگستر بود</p></div>
<div class="m2"><p>به هر دو جهان شاه و مهتر بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه این بی خرد کز خرد دور شد</p></div>
<div class="m2"><p>روانش بر دیو مزدور شد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چه دانش بود با چنین تاجور</p></div>
<div class="m2"><p>که باشد همه سال بیدادگر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سیاوش چو از مرز ایران برفت</p></div>
<div class="m2"><p>پناه روان درگه او گرفت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پذیرفت او را به زنهار خویش</p></div>
<div class="m2"><p>که روزی نیاردش آزار پیش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به گفتار گرسیوز شوم روی</p></div>
<div class="m2"><p>گران کرد بیهوده دل را بدوی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به دژخیم فرمود تا بی گناه</p></div>
<div class="m2"><p>سرش را ببرند چون کینه خواه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کنون تا جدا شد سر او ز تن</p></div>
<div class="m2"><p>به توران نیابی تو با مرد زن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر آن خون کزین کینه شد ریخته</p></div>
<div class="m2"><p>بدان گیتی او باشد آویخته</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مرا یار بخت است و شاهم خدای</p></div>
<div class="m2"><p>ندانم جز او شاه در دو سرای</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو رویین به تندی از او این شنید</p></div>
<div class="m2"><p>بزد دست و تیغ از میان برکشید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدان تا زند بر سر و یال اوی</p></div>
<div class="m2"><p>ز بالاش خون اندر آرد به روی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سبک برزوی شیر دل، تیز چنگ</p></div>
<div class="m2"><p>بیازید بازو به سان نهنگ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بدان تا رباید مر او را ز زین</p></div>
<div class="m2"><p>به خواری در آرد به روی زمین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بترسید رویین و از بیم جان</p></div>
<div class="m2"><p>بپیچید ازو روی و شد تا زنان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کشاورز دنبال اسبش گرفت</p></div>
<div class="m2"><p>به تندی زمانی همی داشت تفت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز نیروی فرخنده بخت جوان</p></div>
<div class="m2"><p>تکاور به روی اندر آمد دمان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دم اسب در دست آن نامدار</p></div>
<div class="m2"><p>بماند و بیفتاد از وی سوار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جهاندار از دور می دید آن</p></div>
<div class="m2"><p>به پیران چنین گفت کای پهلوان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نه از مردم است این زآهرمن است</p></div>
<div class="m2"><p>من ایدون گمانم که تخم من است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ازین جنگی گرد شاید چنان</p></div>
<div class="m2"><p>که در دیده رستم آرد سنان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدین کفت و بازو و این زور و یال</p></div>
<div class="m2"><p>به گیتی ندانم کس این را همال</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گمانم که روز نبرد این دلیر</p></div>
<div class="m2"><p>تن اژدها را در آرد به زیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تو گویی که از دانش آگاه نیست</p></div>
<div class="m2"><p>به چشمش همان شاه و چاکر یکی ست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بدین تندی و تیزی خویش کام</p></div>
<div class="m2"><p>سر ژنده پیل اندر آرد به دام</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مگر آفریننده بخشودمان</p></div>
<div class="m2"><p>که آسان همی راه بنمودمان</p></div></div>