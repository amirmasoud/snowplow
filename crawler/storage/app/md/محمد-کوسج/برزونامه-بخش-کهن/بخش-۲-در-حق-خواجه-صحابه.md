---
title: >-
    بخش ۲ - در حق خواجه صحابه
---
# بخش ۲ - در حق خواجه صحابه

<div class="b" id="bn1"><div class="m1"><p>محمد رسولش به هر دو سرای</p></div>
<div class="m2"><p>که بگزیدش از خلق عالم خدای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محمد که ایزد ورا برگزید</p></div>
<div class="m2"><p>ابا او بسی کرد گفت و شنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی تا بود این جهان را بقا</p></div>
<div class="m2"><p>درود و ثنا باد بر مصطفی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابر آل و اصحاب و یاران او</p></div>
<div class="m2"><p>در احکام دین جان سپاران او</p></div></div>