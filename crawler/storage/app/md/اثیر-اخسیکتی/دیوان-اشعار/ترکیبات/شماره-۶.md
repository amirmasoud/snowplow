---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ای دل بی‌رحم تو را، مایهٔ شادی غم ما</p></div>
<div class="m2"><p>این چه بلا بود قضا، من ز کجا نوز کجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که ز من جور و جفا، شرم نداری ز خدا</p></div>
<div class="m2"><p>اینت بلائی که توئی، یارب زنهار تورا</p></div></div>
<div class="b2" id="bn3"><p>یا رب زنهار ز تو سخت بلائی که توئی</p></div>
<div class="b" id="bn4"><div class="m1"><p>نگار جادو سخنی، سوار لشکرشکنی</p></div>
<div class="m2"><p>آفت هر جان و تنی، فتنه دور ز منی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون غمزه بر غمزه زنی، گشته بهم برفکنی</p></div>
<div class="m2"><p>اینت بلائی که توئی، یا رب زنهار ز تو</p></div></div>
<div class="b2" id="bn6"><p>یارب زنهار ز تو سخت بلائی که توئی</p></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه توئی سرو سهی، بچهره خورشید و مهی</p></div>
<div class="m2"><p>چوپای در مهد نهی، ز دور نادیده رهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل بربائی ز رهی، برزنی و عشوه دهی</p></div>
<div class="m2"><p>اینت بلائی که توئی، یارب زنهار ز تو</p></div></div>
<div class="b2" id="bn9"><p>یارب زنهار ز تو سخت بلائی که توئی</p></div>
<div class="b" id="bn10"><div class="m1"><p>برکنی از عشوه سرم، خون کنی از غم جگرم</p></div>
<div class="m2"><p>شبی چو باران بگرم، ور نخرامی زدرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غصه ز تو چند خورم، محنت تو چند برم</p></div>
<div class="m2"><p>اینت بلائی که توئی، یارب زنهار ز تو</p></div></div>
<div class="b2" id="bn12"><p>یارب زنهار ز تو سخت بلائی که توئی</p></div>
<div class="b" id="bn13"><div class="m1"><p>شیفته زار توام، عاشق رخسار توام</p></div>
<div class="m2"><p>گشته و بیمار توام، بدل گرفتار توام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بجان. خریدار توام، بیا. که در کار توام</p></div>
<div class="m2"><p>اینت بلائی که توئی، یارب زنهار ز تو</p></div></div>
<div class="b2" id="bn15"><p>یارب زنهار ز تو سخت بلائی که توئی</p></div>
<div class="b" id="bn16"><div class="m1"><p>باشد شرمیت یقین، از من رنجور حزین</p></div>
<div class="m2"><p>زغمزه بگشای کمین، مگر از این غمزه کین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اثیر خود را به از این، ز دوستداران بگزین</p></div>
<div class="m2"><p>اینت بلائی که توئی، یارب زنهار زتو</p></div></div>
<div class="b2" id="bn18"><p>یارب زنهار ز تو سخت بلائی که توئی</p></div>