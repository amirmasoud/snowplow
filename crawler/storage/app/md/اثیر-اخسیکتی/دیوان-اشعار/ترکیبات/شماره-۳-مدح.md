---
title: >-
    شمارهٔ ۳ - مدح
---
# شمارهٔ ۳ - مدح

<div class="b" id="bn1"><div class="m1"><p>ای پایه شرف ز فلک بر گذاشته</p></div>
<div class="m2"><p>مدح تو را زمانه بدل برنگاشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز شاه شرق براین چتر آبگون</p></div>
<div class="m2"><p>در ظل رایت تو علم برفراشته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مثال تو یک خلف پدر و مادر وجود</p></div>
<div class="m2"><p>در صد هزار دُر نه بزاده نه کاشته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سایه ی جناب تو فضل فلک زده</p></div>
<div class="m2"><p>عیسی چنانک باید خرم گذاشته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در انتظار نوبت میمون تو هنر</p></div>
<div class="m2"><p>صد دیده در تقلب عالم گماشته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز، دیگ سینه ی عدو و کاسه دماغ</p></div>
<div class="m2"><p>شمشیر صبح فام تو را، وجه چاشته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پر گوهر آستین ضمیرم بمدح تو</p></div>
<div class="m2"><p>لیکن قبای قافیه دامن نداشته</p></div></div>
<div class="b2" id="bn8"><p>لفظ الهی از ره اطلاق مشکل است</p>
<p>اینجا دگر نه معنی لاهوت حاصل است</p></div>
<div class="b" id="bn9"><div class="m1"><p>یازان شده است دست معالی بسوی تو</p></div>
<div class="m2"><p>تازان شده است پای بزرگی بکوی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی تو بسته کرده در غم بر اهل فضل</p></div>
<div class="m2"><p>ای اهل فضل را همه شادی بروی تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در عدت امید نشسته است تخت ملک</p></div>
<div class="m2"><p>با صد هزار چشم که بیند بسوی تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باد کرم نمی وزد الا ز طبع تو</p></div>
<div class="m2"><p>آب سخن نمی رود الا بجوی تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مل جرعه ئی چشید و خجل شد بلطف تو</p></div>
<div class="m2"><p>گل شمه ئی گشید و خجل شد ز بوی تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آمد سحاب تا بسخا جلوه ئی کند</p></div>
<div class="m2"><p>از شرم آب شد چو، نگه کرد سوی تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنجا که زخم تیغ کند جوی خون روان</p></div>
<div class="m2"><p>ناید دوست ز آب دغا جز سبوی تو</p></div></div>
<div class="b2" id="bn16"><p>در مدح تو به عجز مقرّ شد ضمیر من</p>
<p>با آنکه عاجز است جهان از نظیر من</p></div>
<div class="b" id="bn17"><div class="m1"><p>با آن همه که چهره ی دعوی سیاه کرد</p></div>
<div class="m2"><p>خورشید را خجالت رای منیر من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من در کمند عجز اسیرم بمدح تو</p></div>
<div class="m2"><p>بوده مبارزان معانی اسیر من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در ملک نظم و نثر نشان هاست بیشمار</p></div>
<div class="m2"><p>بر دیده ی زمانه ز پای سریر من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کاری است ذکر تو، بدست زبان من</p></div>
<div class="m2"><p>راهی است مدح تو، نه بپای ضمیر من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو آفتاب فضلی و اندر خطر بود</p></div>
<div class="m2"><p>باقوت تو اختر شعر خطیر من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر تارک اثیر نهم پای فخر اگر</p></div>
<div class="m2"><p>گوئی ز روی بنده نوازی، کاثیر من؟</p></div></div>
<div class="b2" id="bn23"><p>عمرت جود ور چرخ ز آثام دور باد</p>
<p>از تاج و تخت تو بد ایام دور باد</p></div>
<div class="b" id="bn24"><div class="m1"><p>ای شاه شاهزاده سپهرت غلام باد</p></div>
<div class="m2"><p>کام جهان ز توست جهانت بکام باد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن دست مال بخش که جانها نثار اوست</p></div>
<div class="m2"><p>همواره در بهار طرب سوی جام باد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جام از سرشک دیده ی انگور در کفت</p></div>
<div class="m2"><p>وز گریه چشم حاسد تو لعل فام باد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پیراهن خلاف تو را بر تن عدو</p></div>
<div class="m2"><p>همواره زه چو خنجر و دامن چو دام باد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر عقد مملکت نکند واسطه تو را</p></div>
<div class="m2"><p>دهر این چنین که هست گسسته نظام باد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شاها، جهان ابلق اگر چند توسن است</p></div>
<div class="m2"><p>چون دید زین دولت تو خوش لگام باد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>میمون همای مدح تو را همچو من هزار</p></div>
<div class="m2"><p>در زیر پرّ تربیت و اهتمام باد</p></div></div>