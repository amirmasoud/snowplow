---
title: >-
    شمارهٔ ۲ - مدح سلطان مظفرالدین قزل ارسلان
---
# شمارهٔ ۲ - مدح سلطان مظفرالدین قزل ارسلان

<div class="b" id="bn1"><div class="m1"><p>ای بنده ی لب تو لب آبدار می</p></div>
<div class="m2"><p>گلگونه کرد عکس رخت برعذار می</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تخت هوس نهاده رخت بربساط گل</p></div>
<div class="m2"><p>رخت خرد فکنده لبت در جوار می</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون صبح جامه چاک زده غنچه حباب</p></div>
<div class="m2"><p>پیش نسیم زلف تو بر جویبار می</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برخیزد از مقرنس سقف فلک نشان</p></div>
<div class="m2"><p>صد نرگسه ز شعله ی انجم شرارمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هم شکن شماری زنگاری فلک</p></div>
<div class="m2"><p>چون از قسیمه موج برآرد بحارمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم سیاه کردان بر ذوالخمار غم</p></div>
<div class="m2"><p>دست طرب چو لعل کند ذوالفقارمی</p></div></div>
<div class="b2" id="bn7"><p>عکس می است شعله ی مجلس فروز عید</p>
<p>روز طرب بباده برافروز روز عید</p></div>
<div class="b" id="bn8"><div class="m1"><p>دست زمان نقاب گشاد از جمال عید</p></div>
<div class="m2"><p>دلاله عروس طرب شد دلال عید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم سیه سپید زمانه بدید و گفت:</p></div>
<div class="m2"><p>با او گسسته عین کمال از جمال عید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خالی است عید بر لب ایام تا بحشر</p></div>
<div class="m2"><p>خط زوال دست بریده ز خال عید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعد فلک چو آینه چشم است جمله تن</p></div>
<div class="m2"><p>بر بوی عکس از رخ مسعود فال عید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر ارغنون بلبله ی ارغوان نمای</p></div>
<div class="m2"><p>حال طرب خوش است که خوش بادحال عید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نو بهار عشرت خسرو دهد مثال</p></div>
<div class="m2"><p>بستان روزگار بگیرد نهال عید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عین کمال عید رخ اوست دور باد</p></div>
<div class="m2"><p>عین کمال فتنه، ز عین کمال عید</p></div></div>
<div class="b2" id="bn15"><p>چرخ ظفر مظفر دین عالم کرم</p>
<p>در شان خستگان عنا مرهم کرم</p></div>
<div class="b" id="bn16"><div class="m1"><p>دریا گه سخا زغلامان دست اوست</p></div>
<div class="m2"><p>در روی مهر طبع کرم پای بست اوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دُردی کشی است ازشکر، شکراو، از آنک</p></div>
<div class="m2"><p>در مجلس نوال شده مست مست اوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دیری است تا که مسند شاهی نهاده چشم</p></div>
<div class="m2"><p>بر پای انتظار به بوی نشست اوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جای بلند پایه وجود فراخ او</p></div>
<div class="m2"><p>هرچند نسبت همه خلق است هست اوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>میدان دهر اگرچه فراخ است تنک اوست</p></div>
<div class="m2"><p>ایوان چرخ اگرچه بلند است پست اوست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تیری همی نه بینم در جعبه سخن</p></div>
<div class="m2"><p>کان در مصاف گاه نه بامرد شست اوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همچون کمان پنبه زنان بشکلی است خصم</p></div>
<div class="m2"><p>ور خودز آهن است نه مردان شست اوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صدرش چوپای مرد فتوح است روزگار</p></div>
<div class="m2"><p>گر زان ستانه لاف زند حق بدست اوست</p></div></div>
<div class="b2" id="bn24"><p>ماهی که آفتاب سزد دورباش او</p>
<p>بهرام تند طبع سزد خیل تاش او</p></div>
<div class="b" id="bn25"><div class="m1"><p>کان، دایم از سخاش بخروار زر کشد</p></div>
<div class="m2"><p>جان، دایم از بیانش بدامن گهر کشد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سیمرغ مشرقی است به پرواز رایتش</p></div>
<div class="m2"><p>زان طول و عرض گیتی درزیر پر کشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هم کفته فلک شکند هم عمود صبح</p></div>
<div class="m2"><p>گر حلم او زمانه به معیار بر کشد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر در گه کمالش شبدیز آسمان</p></div>
<div class="m2"><p>هرمه دو بار کردن در طوق زر کشد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پیش کمال او که جهانی است پایدار</p></div>
<div class="m2"><p>فانی جهان، هر آنچه کشد مختصر کشد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اقبال گرنه بوسه دهد آستان او</p></div>
<div class="m2"><p>دست تصرف اجلش دربدر کشد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عزمش بپشت باد برافکنده راحله</p></div>
<div class="m2"><p>یعنی که بار اسب سبکبار خر کشد</p></div></div>