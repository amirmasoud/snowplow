---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>صلحی رود میان تموز و خزان چنانک</p></div>
<div class="m2"><p>تا حشر دور چرخ نگرداندش تباه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جمله خسروی است قزل ارسلان که عقل</p></div>
<div class="m2"><p>از وی بدیگری نبرد راه اشتباه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارت کزین دو فرقد و یک آفتاب باد</p></div>
<div class="m2"><p>تا نفخ صور رسته ملک جهان سه ماه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این بدر خسروانه بماناد تا ابد</p></div>
<div class="m2"><p>هم بر سرور جاهت و هم بر سریر جاه</p></div></div>