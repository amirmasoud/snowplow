---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>خسروا، بر بساط همت توست</p></div>
<div class="m2"><p>قدم طبع آسمان سایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون وطن بر ستانه ی تو کنم</p></div>
<div class="m2"><p>سر چرخ برین سزد جایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکر یزدان که عقد مدحت توست</p></div>
<div class="m2"><p>گوهر نظم عالم آرایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پهن بگشای چشم و نغز ببین</p></div>
<div class="m2"><p>تا کیم، من، چکار را شایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو در سایه ی هما آمد</p></div>
<div class="m2"><p>طوطی خاطر شکر خایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلکم، بی گزاف میرانم</p></div>
<div class="m2"><p>عالمم، نی بهر زه می لایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو بهاری و من چمن، چه شود</p></div>
<div class="m2"><p>گر مقرر کنی سر و پایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چونکه شایسته خزانه توست</p></div>
<div class="m2"><p>هر گهر کا ز ضمیر می زایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بتو، از جنس اصطناع صداع</p></div>
<div class="m2"><p>آنچه بنمودنی است بنمایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با سخای تو، دی همی گفتم</p></div>
<div class="m2"><p>که چه تقدیر میکند رایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه دفع خزان بارد را</p></div>
<div class="m2"><p>در حریم لباچه ئی آیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت سهل است من به نیمچه ئی</p></div>
<div class="m2"><p>رتبت و جاه تو بیفزایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتمش در مصاف دشمن و دوست</p></div>
<div class="m2"><p>چون علم نیمه ئی بیارایم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیمه را، نیمگان فرود آیند</p></div>
<div class="m2"><p>من تمامم تمام فرمایم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باغبان بهشت مدح تو باد</p></div>
<div class="m2"><p>وهم چالاک سحر پیمایم</p></div></div>