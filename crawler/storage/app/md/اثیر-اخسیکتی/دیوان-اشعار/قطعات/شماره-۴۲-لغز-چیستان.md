---
title: >-
    شمارهٔ ۴۲ - لغز- چیستان
---
# شمارهٔ ۴۲ - لغز- چیستان

<div class="b" id="bn1"><div class="m1"><p>چیست آن معشوقه ای کاو نه زخاص است و نه عام</p></div>
<div class="m2"><p>با حریفان سر بسر یکسان بود در ابتسام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه باشد چشم او در جامه های شعر زرد</p></div>
<div class="m2"><p>گاه باشد فرش او بر فرشهای سیم خام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه در تیمار یاران گاه در تیمار خود</p></div>
<div class="m2"><p>خوش همی خندد مقیم و زار می گرید مدام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پناه وصل او یک رنگ باشد روز و شب</p></div>
<div class="m2"><p>با جمال روی او یکسان نماید صبح و شام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا دیدار او باشد خجل باشد ضیا</p></div>
<div class="m2"><p>هر کجا رخسار او باشد نهان گردد ظلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست او را سوختن در مذهب صوفی هلال</p></div>
<div class="m2"><p>نیست او را کشتن اندر ملت تازی حرام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در فنون انتفاع و در صنوف فایده</p></div>
<div class="m2"><p>ابتر او چون صحیح و ناقص او چون تمام</p></div></div>