---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>دل گواهی میدهد این کعبه اقبال را</p></div>
<div class="m2"><p>کرد معمار فلک دایم بمعموری ضمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش این دیوان اگر تقدیر دستوری دهد</p></div>
<div class="m2"><p>سجده آرد طاق کسری نه که طاق آسمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظل او غمخوار گان را چون ادم بزم طرب</p></div>
<div class="m2"><p>صحن او ترسندگان را چون حرم حصن امان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پاسبانی بر سرش بر پاس هر بامی فضا</p></div>
<div class="m2"><p>پیشگاری بر درش در پیش هر کاری زمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز رقیبان هنر در وی نبوده دیده کس</p></div>
<div class="m2"><p>جز امینان خرد بر وی نبوده قهرمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوته از بالای اوج منظرش دست یقین</p></div>
<div class="m2"><p>قاصر از پهنای بسط مطرحش پای گمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پی جانداری سلطان عالی هیکلش</p></div>
<div class="m2"><p>شحنه جوشن وران چرخ بردارد کمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دور باش عکس اولاحول دیو آمد که هست</p></div>
<div class="m2"><p>هفت نقش منفعل از چار قطر او زمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مطرب طبع است خاکپای او در خاصیت</p></div>
<div class="m2"><p>راست چون خاکی که باشد مدفن زر جهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسته محنت حریم او پسند ملتجا</p></div>
<div class="m2"><p>هاتف دولت صدای او گزیند ترجمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دولت فربه ز فر اوست راعی عجاف</p></div>
<div class="m2"><p>سایه لاغر زیاد او مراعی آسمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مالک او گر نبودی مسند قاضی القضات</p></div>
<div class="m2"><p>در جناب او سعادت کی نشستی یک زمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا رب اقبالی ده او را بر حضور اولیا</p></div>
<div class="m2"><p>چون اجل امید بند و چون سخن جاویدمان</p></div></div>