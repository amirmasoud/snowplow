---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ای شمع آسمانی پروانه جمالت</p></div>
<div class="m2"><p>هر دیده ئی و مهری از خاتم مثالت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمشید کیست، مرغی در آشیان ملکت</p></div>
<div class="m2"><p>خورشید چیست، ماهی برخیمه کمالت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شب برسم تحفه، از مجلس کواکب</p></div>
<div class="m2"><p>مه در طبق در آرد، نو باوه جلالت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با رنگ لب حسامت، سر تا قدم زبان شد</p></div>
<div class="m2"><p>اسرار گفت خواهد، با جان بدسگالت</p></div></div>