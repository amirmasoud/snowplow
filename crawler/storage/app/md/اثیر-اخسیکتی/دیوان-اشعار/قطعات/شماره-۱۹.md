---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>من گرد سر کوی تو از بهرتو گردم</p></div>
<div class="m2"><p>بلبل ز پی گل بکنار چمن آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشکست دلم در شکن زلف مبادا</p></div>
<div class="m2"><p>کز چشم بدی برشکن او شکن آید</p></div></div>