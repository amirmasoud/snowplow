---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>چون بدیدم بدیدهٔ تحقیق</p></div>
<div class="m2"><p>که جهان منزل عناست کنون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راد مردان نیک محضر را</p></div>
<div class="m2"><p>روی در برقع فناست کنون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان چون حریف نامنصف</p></div>
<div class="m2"><p>به ره عشوه و دغاست کنون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل‌فکار است همچو دانه برآنک</p></div>
<div class="m2"><p>زیر این سبز آسیاست کنون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبع بیمار من ز نشتر آز</p></div>
<div class="m2"><p>شکر، یزدان درست خاست کنون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز عقاقیر خانهٔ توبه</p></div>
<div class="m2"><p>نوشداروی صدق خواست کنون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وز زبان جهان خدیو خدای</p></div>
<div class="m2"><p>مادح حضرت خداست کنون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لهجه‌ای خوش‌نواتر از زخمه</p></div>
<div class="m2"><p>بلبل باغ مصطفاست کنون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عزت خاره و قصب بر من</p></div>
<div class="m2"><p>چون فزون شد خرد نکاست کنون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر آزاده و تن آزاد</p></div>
<div class="m2"><p>هیچ کز پشم و پنبه راست کنون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مدتی خدمت ثنا گردم</p></div>
<div class="m2"><p>نوبت خدمت دعاست کنون</p></div></div>