---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>بخدائی که روی بند عدم</p></div>
<div class="m2"><p>امرش از چهره جهان بگشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد لطفش بباغ رحمت در</p></div>
<div class="m2"><p>بید امید را زبان بگشاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقد های جواهر و اعراض</p></div>
<div class="m2"><p>از دل کان کن فکان بگشاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیبتش عقل را زبان بربست</p></div>
<div class="m2"><p>رحمتش عجز را دهان بگشاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساخت میتین و تیغ صبح و بدان</p></div>
<div class="m2"><p>چشمه مهر از آسمان بگشاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمر کوه را مرصع کرد</p></div>
<div class="m2"><p>چون جواهر زبندگان بگشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تربیت کرد نفس ناطقه را</p></div>
<div class="m2"><p>تا بدو کشور بیان بگشاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوی لطفش چو رنگ بط آمیخت</p></div>
<div class="m2"><p>نبض خون از دل روان بگشاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پی انس و جان بدست اجل</p></div>
<div class="m2"><p>بند ترکیب انس و جان بگشاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که مرا فرقت شما هر دم</p></div>
<div class="m2"><p>عقدی از جزع درفشان بگشاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نعره ها میزنم که سوزش آن</p></div>
<div class="m2"><p>چرخ را خون ز دیدگان بگشاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناله ها میکنم که جوزا را</p></div>
<div class="m2"><p>کمر سیم از میان بگشاد</p></div></div>