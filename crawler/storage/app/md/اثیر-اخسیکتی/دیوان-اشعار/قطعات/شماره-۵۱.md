---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>ای ز بزم تو با لطایف خلق</p></div>
<div class="m2"><p>پیشه ی گوش و دل شکر رفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پی گوش و گردن مدحت</p></div>
<div class="m2"><p>عقل شاگرد من بدر سفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده بر در بماند فرمان چیست</p></div>
<div class="m2"><p>سخنی باز میتوان گفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه چنان لنگری است کز سبکیش</p></div>
<div class="m2"><p>همچو دریا بباید آشفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بخدمت رسد بجای آرد</p></div>
<div class="m2"><p>شرط کم گفتن و سبک خفتن</p></div></div>