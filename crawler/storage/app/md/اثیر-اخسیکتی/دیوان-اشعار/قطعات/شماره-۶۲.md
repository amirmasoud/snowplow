---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>بازوی ملک نیامیخت چو تو شمشیری</p></div>
<div class="m2"><p>خامه و هم نیانگیخت چو تو تمثالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سرا پرده ی جاه تو هوا دهلیزی</p></div>
<div class="m2"><p>وز ترازوی وقار تو زمین مثقالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخمه ی گرز، و،صلیل سرتیغت زعراق</p></div>
<div class="m2"><p>بخراسان وری افتاد ظفر را حالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاده بخت جوان تو جهان کهن است</p></div>
<div class="m2"><p>طفل دیدی که تولد کند ازوی زالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفدرا، چرخ بهر مرتبه درپای آرد</p></div>
<div class="m2"><p>در هوای سخن، ار باز گشایم بالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که لفظی بهم آرد نشود همسرمن</p></div>
<div class="m2"><p>کل کجا، دیلم گردد بکلاه شالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر جهودی شود از حاشیه متنی حشو!</p></div>
<div class="m2"><p>من مسیحم نکشم غاشیه دجالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا که بر ماه نگارم رقم مدحت شاه</p></div>
<div class="m2"><p>همتم داشت زبان بند چوماهی سالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی دل من که بود غالب ساغر ز مئی</p></div>
<div class="m2"><p>بی رخ گل که بود بلبل عاشق لالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قلم پرتو خورشید نگارد لیلی</p></div>
<div class="m2"><p>سایه شهپر سیمرغ بر آرد زالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من نه آنم که شوم بهردوزرپاره قلب</p></div>
<div class="m2"><p>حلقه در گوش لئیمان چودف قوالی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاخ طوبی چو سر پنجه دعوی بفراخت</p></div>
<div class="m2"><p>بازوی بال نیارد که بر آرد بالی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آیت معنی تیغ تو روایت کندا</p></div>
<div class="m2"><p>هر زبان قلمی کاو بنگارد قالی</p></div></div>