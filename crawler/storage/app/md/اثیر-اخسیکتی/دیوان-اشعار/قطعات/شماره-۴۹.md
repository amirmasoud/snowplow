---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>شاها سموم فاقه نهالی من بسوخت</p></div>
<div class="m2"><p>آبیم ده ز لطف که پروردهٔ توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عالمم خراب کند غصه کی بود</p></div>
<div class="m2"><p>چون ز آب و خاک نطق برآوردهٔ توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دست چرخ ناکس چون من بسی است لیک</p></div>
<div class="m2"><p>زان است غبن من که به کس کردهٔ توام</p></div></div>