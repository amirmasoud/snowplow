---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>دو کس بزاویه ئی در نشسته مخموریم</p></div>
<div class="m2"><p>به یاد باده ی دوشینه هردومست وخراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مصاف عشرت ما بشکند زمانه اگر</p></div>
<div class="m2"><p>تو نشکنی بتفضل خمارما بشراب</p></div></div>