---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>از تو ننالم به هیچکس که به دشمن</p></div>
<div class="m2"><p>شرط نباشد بدوستان گله کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک دلی اندرجهان کجاست که با او</p></div>
<div class="m2"><p>یک گله بتوان ز یار ده گله کردن</p></div></div>