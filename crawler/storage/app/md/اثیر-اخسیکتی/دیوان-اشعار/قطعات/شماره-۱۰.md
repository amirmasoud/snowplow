---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>در جهان هفت خصال است پسند حکما</p></div>
<div class="m2"><p>که از آن هفت فراترعددی با من نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بمطعوم در آئی بسوی گوشت گرای</p></div>
<div class="m2"><p>که بدن را به ازاو هیچ غذا ممکن نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وانکه را، هست نپوشد سلب فاخر پاک</p></div>
<div class="m2"><p>من بگویم که خردنیست اگرموهن نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مال تو گر بسر آید منه انبار، بده</p></div>
<div class="m2"><p>ضامن رزق عوض باز دهد خائن نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده ی ناب همی نوش و به ادمان کم کوش</p></div>
<div class="m2"><p>زانکه از خاصیتش هیچ دراو مامن نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور حریفی طلبی زیرک و آزاده طلب</p></div>
<div class="m2"><p>در مزاج تواگر بخل و حسد مزمن نیست</p></div></div>