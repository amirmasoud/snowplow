---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>زهی دست بیضات چون تیغ خورشید</p></div>
<div class="m2"><p>جهان را شده نیک از او حالت بد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قلم وار دادی زبان خرد را</p></div>
<div class="m2"><p>چو دفتر نشان کردم این بر دل خود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه همچون دوات سیه کاسه مانم</p></div>
<div class="m2"><p>سپیدی چرا میکنم همچو کاغذ</p></div></div>