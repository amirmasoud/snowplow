---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>جمشید رکابا توئی آن شاه که امرت</p></div>
<div class="m2"><p>از سنگ سیه ناقه صالح بدر آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر گلبن فردوس خورد آب خلافت</p></div>
<div class="m2"><p>برجای گل تازه، شتر خار بر آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او، بار بیک رقص شتر بگسلد از هم</p></div>
<div class="m2"><p>هر کار که بدخواه تو در یکدگر آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماهارکشی دور فتد در بنه چرخ</p></div>
<div class="m2"><p>تا چون شترش کی بقطار تودر آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر هر که رود کین شتر در دل دوران</p></div>
<div class="m2"><p>از تیغ تو ناکاه کمیتش بسر آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچون شتر جمز رود ابر سبکپای</p></div>
<div class="m2"><p>تا باز ز فتح تو بعالم خبر آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ملک جهان جوی که در خانه همت</p></div>
<div class="m2"><p>هرکس شتر خویش ببالای در آرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در شغل خلافت چه برد خصم شتردل</p></div>
<div class="m2"><p>احسنت پلاسی و مهاری بسر آرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه گه فلک از نفس شتر حذف کند پا</p></div>
<div class="m2"><p>تا محمل اعدای تو در پشت سر آرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این دیده ی نار است بداندیش تودارد</p></div>
<div class="m2"><p>کز قد شتر کردن کژ در نظر آرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر شهپر جبرئیل چمد چون شتر حاج</p></div>
<div class="m2"><p>هر کاو سوی درگاه توراه سفر آرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منزلگه عیسی سزد و مرحله خضر</p></div>
<div class="m2"><p>آنجا که شتربان توروزی مقر آرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با بخشش تو هر که کند یاد دو عالم</p></div>
<div class="m2"><p>از بهر شتر غالیه گون آنجور آرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور فی المثل از جرعه بزم توشودمست</p></div>
<div class="m2"><p>جمّال شتر جانب خانه بجر آرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در مرتبه تا کعب کمال تو نباشد</p></div>
<div class="m2"><p>کردون شتر خو که زکوهان قمر آرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر کاو بغذا مغزشتر خورده نباشد</p></div>
<div class="m2"><p>آلت ز پی شیشه ز دودن بتر آرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کشتی زشتر وصف طبیعی است ولیکن</p></div>
<div class="m2"><p>با شیر ژیان دست کجا در کمر آرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پای شتر آمد کف بدخواه تو دررزم</p></div>
<div class="m2"><p>ز آنجا که بر او نیزه گذارد سپر آرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باز این دم مشکین ز حیات است اثیرا</p></div>
<div class="m2"><p>کس بر شتری اینهمه خون جگر آرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حساد فرومایه بسی داری و اشتر</p></div>
<div class="m2"><p>بیماری مرک از مگس مختصر آرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زان قوم کران خوی که با بار قمطره</p></div>
<div class="m2"><p>نادان بود آنکس که شتر در شمر آرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از عقل که باشد خرفی کاو شتر نر</p></div>
<div class="m2"><p>بی فایده در کارگه شیشه گر آرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صد خنده زند خرکه گه علت قولنج</p></div>
<div class="m2"><p>دانا به بر لفج شتر گل شکر آرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای آنکه بیک دم زدن از بکر تفکر</p></div>
<div class="m2"><p>صد مرغ شترمرغ بیانت بپر آرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زان طبع که پیرایه ده کل وجود است</p></div>
<div class="m2"><p>شاید که نصیب شتری اینقدر آرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یک نکته هم از باب شتر لایق حال است</p></div>
<div class="m2"><p>تا بنده بر این نکته حکایت بسر آرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ایشاه درین فصل شتر موی بیفکند</p></div>
<div class="m2"><p>ترسم شتر من به غلط موی بر آرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاها در عید است و مدام از پی قربان</p></div>
<div class="m2"><p>در شرط بود کاین شتر و آن نفر آرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شایسته نحر است عدو چون شتر کور</p></div>
<div class="m2"><p>چندانش امان ده که ز گل پای بر آرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من کعب غزال آرم و خلعت برم و زر</p></div>
<div class="m2"><p>تا جان کند آن بیش که روشن کدر آرد</p></div></div>