---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>خطاست پیش خرد در همه فنون هنر</p></div>
<div class="m2"><p>عطارد ار قلمی راند جز بفتوی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرت بود هوس دلبران پرده لطف</p></div>
<div class="m2"><p>بیا که جلوه کنانند وقت اتقی من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسان طفل نوآموز پیر عقل آنگه</p></div>
<div class="m2"><p>نوشته درس حقایق همه ز املی من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنو بهار حقایق میان روضه فضل</p></div>
<div class="m2"><p>شکوفه دار معانی است شاخ طوبی من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قیاس شعری من چون قیاس برهانی است</p></div>
<div class="m2"><p>برآنک محض صواب است عین دعوی من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بصیرتم چو گشاده است چشم عقل از آنک</p></div>
<div class="m2"><p>نقاب زرق بگیرد ز روی تقوی من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسر مه سخنم کس نمیرسد زان است</p></div>
<div class="m2"><p>ز من زمانه تغافل طریق دعوی من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا به نظم ستودند چون ز رقت حال</p></div>
<div class="m2"><p>حکایتی است سخنهای من ز شکوی من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هنوز سطوت الفاظ من ندانستند</p></div>
<div class="m2"><p>دریغ آنکه ندیدند روی دعوی من</p></div></div>