---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>هر که را دردل از خرد خبراست</p></div>
<div class="m2"><p>صنعت ذات او همه هنر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنری باش و هرچه خواهی باش</p></div>
<div class="m2"><p>نه بزرگی بمادر و پدر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نافه مشک را به بین بمثل</p></div>
<div class="m2"><p>کز لباس بدیع معتبر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم بی خرد ز روی قیاس</p></div>
<div class="m2"><p>بر آن کس که صاحب بصر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه از جنس مردم است به شخص</p></div>
<div class="m2"><p>به حقیقت ز جنس گاو و خر است</p></div></div>