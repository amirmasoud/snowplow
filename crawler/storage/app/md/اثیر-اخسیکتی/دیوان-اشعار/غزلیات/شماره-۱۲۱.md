---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>چون با غم تو قرار گیرم</p></div>
<div class="m2"><p>از هر دو جهان کنار گیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با بخت ز جیب سر بر آرم</p></div>
<div class="m2"><p>چون دامن آن نکار گیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وز مرتبه دست خود ببوسم</p></div>
<div class="m2"><p>کان طره مشکبار گیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی محنتش ار، دمی بر آرم</p></div>
<div class="m2"><p>حقا، که نه در شمار گیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی زهره آنکه سنگ تشنیع</p></div>
<div class="m2"><p>در شیشه روزگار گیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی صبر، که بر کران نشینم</p></div>
<div class="m2"><p>نی کام، که در کنار گیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمم همه خونشد و ندارم</p></div>
<div class="m2"><p>آن چشم، که اعتبار گیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن است صلاح من، که حالی</p></div>
<div class="m2"><p>دنبال صلاح کار گیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیروز شوم، اگر در این شغل</p></div>
<div class="m2"><p>مردی چو اثیر، یار گیرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نی نی فکند اثیر کردی</p></div>
<div class="m2"><p>خاک در شهر یار گیرم</p></div></div>