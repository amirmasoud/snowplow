---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>گوهر دیده کرده ام، پیشکش جمال تو</p></div>
<div class="m2"><p>اطلس رخ کشیده ام، در قدم خیال تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و خرد در آستین، بر طمعی همی درم</p></div>
<div class="m2"><p>بو، که عنایتی کند، در حق من وصال تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در تو کجا رسد کسی، تا برسد بپای تو</p></div>
<div class="m2"><p>مرغ تو کی شود دلی، گر نپرد ببال تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست اثیر مرد تو، خاصه کنون که برفلک</p></div>
<div class="m2"><p>ماه تمام در خط است، از خط چون هلال تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موت و حیات عاشقان، معنی جزع و لعل تست</p></div>
<div class="m2"><p>دانه و دام زیر گان، صورت زلف و خال تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من بتو مایل و تو خود، هر نفسی ملول تر</p></div>
<div class="m2"><p>وه، که خجل نمی شود، میل من از ملال تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن من ز اشک خون، چون شفق است لاله گون</p></div>
<div class="m2"><p>کافسر آفتاب شد سنبل شب مثال تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانه و دل ز زیرکی پست نشست چون بدید</p></div>
<div class="m2"><p>از همه زیر گان کسی تا شده در جوال تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حادثه تو عام شد، خاصه که خاص میکند</p></div>
<div class="m2"><p>حضرت خسرو جهان، مملکت جمال تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق اثیر جد شمر وصل لبت محال دان</p></div>
<div class="m2"><p>وه. که بهم چه خوش بود جد من و محال تو</p></div></div>