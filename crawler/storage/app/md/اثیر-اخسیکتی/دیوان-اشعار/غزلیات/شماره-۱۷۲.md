---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>رخ تو فتنه جهان بودی</p></div>
<div class="m2"><p>گر نه، از دیده نهان بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و دین رفت در سر غم تو</p></div>
<div class="m2"><p>کاش باری امید جان بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رخت یادگار خواستمی</p></div>
<div class="m2"><p>گرنه اشکم چو ارغوان بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل، به خستی بجان ز دست غمت</p></div>
<div class="m2"><p>گرنه رویم چو زعفران بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میزبانی است تازه وعده تو</p></div>
<div class="m2"><p>گرنه در لقمه استخوان بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشوه ئی میدهی که آن توام</p></div>
<div class="m2"><p>کی چنین بودی، ار چنان بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوردمی، بر زخاک کوی تو دوش</p></div>
<div class="m2"><p>گر نه فریاد پاسبان بودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در رکابم فلک پیاده شدی</p></div>
<div class="m2"><p>چون قبول تو هم عنان بودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از مقیمان آستان غمت</p></div>
<div class="m2"><p>فخر گردی گر آسمان بودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان لطافت که در دل است تو را</p></div>
<div class="m2"><p>کاشکی هیچ در زبان بودی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرما گر به هیچ ارزیدی</p></div>
<div class="m2"><p>خاک آن فرخ آستان بودی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دوش گفتی اثیر از آن من است</p></div>
<div class="m2"><p>نه چنین بودی ار چنان بودی</p></div></div>