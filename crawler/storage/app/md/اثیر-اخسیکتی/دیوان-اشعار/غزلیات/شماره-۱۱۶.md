---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>هرغم که دهد عشق تو من خار ندارم</p></div>
<div class="m2"><p>بی تو علم الله که جز این کار ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور از شب زلفین تو مرگ دل من باد</p></div>
<div class="m2"><p>آنروز، کش از درد تو، تیمار ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عشق تو خوارم، نه که خود عزم من آنست</p></div>
<div class="m2"><p>من خواری عشق تو، چنین خوار ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دیده چه شک باشد، اگر خون نفشانم</p></div>
<div class="m2"><p>وز ناله چه عذر آرم اگر، زار ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوئی که زر خشک همی با مر داری</p></div>
<div class="m2"><p>برگشتم از این یارب زنهار ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هان روی چو زر خواهی هان سنگ و ترازو</p></div>
<div class="m2"><p>در کیسه نه زین باری بسیار ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بل تا چو کمر دست در آرم به میانت</p></div>
<div class="m2"><p>من نیر مسلمانم و زنار ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی که اثیرا قدر این کار نداری</p></div>
<div class="m2"><p>گر راست همی خواهی نهمار ندارم</p></div></div>