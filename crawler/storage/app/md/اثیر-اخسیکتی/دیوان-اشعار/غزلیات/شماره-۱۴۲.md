---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>ای در دل و جان سواری تو</p></div>
<div class="m2"><p>شیران جهان شکان شکاری تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پای در آرد عافیت را</p></div>
<div class="m2"><p>بیداد بدستیاری تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وی دشمن جان من جهانی</p></div>
<div class="m2"><p>جرمم همه دوستداری تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عهدی است میان ما و لیکن</p></div>
<div class="m2"><p>موقوف بر استواری تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاری است بزرگ عشق خاصه</p></div>
<div class="m2"><p>در نوبت خرد کاری تو</p></div></div>