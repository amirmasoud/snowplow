---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>نیم شبان دلبرک نیم مست</p></div>
<div class="m2"><p>بهر صبوحی زبرم چست جست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف کما بیشتر از جام خورد</p></div>
<div class="m2"><p>صدره بسا بیشتر از زلف دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بانگ برآورد بشادی که کو</p></div>
<div class="m2"><p>آنکه طلسم در غم او شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بستد از او جام ببالین من</p></div>
<div class="m2"><p>تنگ به برآمد و پیشم نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دو یکی کرد دل و دوستی</p></div>
<div class="m2"><p>جامه آسایش و جای نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت بشارت، که باقبال صبح</p></div>
<div class="m2"><p>عالم از آرایش ظلمت برست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبحدمان ای بت خورشید چهر</p></div>
<div class="m2"><p>می خوری و خواب کنی، خیر هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قصد مکن تا مژه بر هم زنی</p></div>
<div class="m2"><p>چونکه شوم چون مژه ات می پرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کس چه گمان برد که ریش اثیر</p></div>
<div class="m2"><p>مرهم از آن دست پذیرد که، خست</p></div></div>