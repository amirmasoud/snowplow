---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>خوی تو باجور روزگار بسازد</p></div>
<div class="m2"><p>حسن تو با لطف کردگار بسازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وعده وصلت بکوش هوش فروخوان</p></div>
<div class="m2"><p>تا برود کار انتظار بسازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر پی بوی گلی ز باغ رخ تو</p></div>
<div class="m2"><p>با الم صد هزار خار بسازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی تو دیدم ز خوی خویش خبرده</p></div>
<div class="m2"><p>تا دل کار اوفتاده کار بسازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوخت مرا طبع روزگار مباد آنک</p></div>
<div class="m2"><p>خوی تو با طبع روزگار بسازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو اثیر آنکه درفتاد بدامت</p></div>
<div class="m2"><p>تا به ابد برگ اضطرار بسازد</p></div></div>