---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>روزی که جفاهای تو بر یاد من آید</p></div>
<div class="m2"><p>دل نوش کند غصه و از خویشتن آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون صفّ بلا، راست کنی از سر تسلیم</p></div>
<div class="m2"><p>مرد آن بود آری، که نه کمتر ززن آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تیغ بیالاید عشقت بمن این فخر</p></div>
<div class="m2"><p>حاشا که ز صد پیرهنم یک کفن آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چند زبان گرد بدارم که لب توست</p></div>
<div class="m2"><p>چیزی که ز ایام بدندان من آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من گرد سر کوی تو از بهرتو گردم</p></div>
<div class="m2"><p>بلبل ز پی گل بکنار چمن آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشکست دلم زان شکن زلف مبادا</p></div>
<div class="m2"><p>کز چشم بدان برشکن او شکن آید</p></div></div>