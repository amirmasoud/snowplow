---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>یک امروزت سر من چاکرت هست</p></div>
<div class="m2"><p>چگوئی این لطافت در سرت هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا گر داده ئی صدبار دانم</p></div>
<div class="m2"><p>کزان باری هزار دیگرت هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدین معنی تو باکس برنیائی</p></div>
<div class="m2"><p>که از من نازنین تر دربرت هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گدائی هم بباید چشم بدرا</p></div>
<div class="m2"><p>چو از شاهان هزاران چاکرت هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه میگویم تو و جان اینت سودا</p></div>
<div class="m2"><p>کسی باور ندارد باورت هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه زین جانهای زهر آلود صد جان</p></div>
<div class="m2"><p>در آن مرجان شکر پیکرت هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اثیر اینک بدست کافرش ده</p></div>
<div class="m2"><p>اگر دُردی دگر در ساغرت هست</p></div></div>