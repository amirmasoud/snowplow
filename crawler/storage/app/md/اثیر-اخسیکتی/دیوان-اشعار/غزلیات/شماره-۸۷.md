---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>امید وصل غم روز هجر چون شب شمع</p></div>
<div class="m2"><p>ز سوز سینه لبم خشک و دیده تر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید وصل ز دل پرس و درد هجر زجان</p></div>
<div class="m2"><p>که هر کسی ز غم خویشتن خبر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بذوق جان سخن تلخ تو خوش است زقند</p></div>
<div class="m2"><p>از آنکه بر لب شیرین او گذر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه بس خوش و شیرین بود شکر لیکن</p></div>
<div class="m2"><p>حلاوت لب تو لذت دگر دارد</p></div></div>