---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>آنرا که چنان سلسله ها بافته باشد</p></div>
<div class="m2"><p>هر سلسله زندان دلی تافته باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا که بجوئید زجانهای عزیزان</p></div>
<div class="m2"><p>در هر شکن آرامگهی یافته باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد بار بگفتم مکن ای دل مرو آنجا</p></div>
<div class="m2"><p>کان ره نه به پای چو توئی بافته باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر گنبد طرار منه چشم که ناکاه</p></div>
<div class="m2"><p>تا در نگری جیب تو بشکافته باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد نامه سر بسته بخوانی و ندانی</p></div>
<div class="m2"><p>کانجا سخن از کاغذ سر تافته باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در راه سر گمشده ئی کم چو اثیر است</p></div>
<div class="m2"><p>آنکس که بتو هم بتو بشتافته باشد</p></div></div>