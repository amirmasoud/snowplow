---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>بی غم عشق تو دل بکار نیاید</p></div>
<div class="m2"><p>جان نبود آب و گل بکار نیاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل برافشاند کیسه لیک ز تقصیر</p></div>
<div class="m2"><p>در رخ تو جز خجل بکار نیاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع دلت خوانم ای عزیزتر، از جان</p></div>
<div class="m2"><p>نام تو شمع چگل بکار نیاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوی تو هر عشق نامه ئی که نوشتم</p></div>
<div class="m2"><p>نقش جز از خون دل بکار نیاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناوک دلدوز برکشی که فلان را</p></div>
<div class="m2"><p>گل شکر معتدل بکار نیاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چو توئی بشکند مصاف ضعیفی</p></div>
<div class="m2"><p>هیچ ایاز و قزل بکار نیاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیغم تو یک نفس که شاد بر آرم</p></div>
<div class="m2"><p>گر کنی از من بحل بکارنیاید</p></div></div>