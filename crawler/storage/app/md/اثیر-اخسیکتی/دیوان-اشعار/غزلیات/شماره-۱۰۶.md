---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>از همه عالم خریدار توام</p></div>
<div class="m2"><p>باورم کن عاشق زار توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای بر کار دل من می نهی</p></div>
<div class="m2"><p>گرچه میدانی که بر کار توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند گوئی دامنم خواهی گرفت</p></div>
<div class="m2"><p>پس بگیرم عاشق زار توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش در هنگامه زلفت شکافت</p></div>
<div class="m2"><p>جیب دعوی چشم طرار توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طیلسان خواجکی بر هم درید</p></div>
<div class="m2"><p>بر میان عشق زنار توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ندارم کیسه بیع و شری</p></div>
<div class="m2"><p>خاکروب گرد بازار توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بخاک افکنده آزرمی بدار</p></div>
<div class="m2"><p>نیست باری، ترک آزار توام</p></div></div>