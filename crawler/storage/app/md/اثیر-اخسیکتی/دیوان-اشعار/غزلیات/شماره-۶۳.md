---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>باز مرا عشق کهن تازه شد</p></div>
<div class="m2"><p>باز ز من شهر پر آوازه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ز برم رخت سفر بار کرد</p></div>
<div class="m2"><p>جان ز پیش تا در دروازه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنج دل سوخته از حد گذشت</p></div>
<div class="m2"><p>درد دل ریش زاندازه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق اثیر ار چه کهن گشته بود</p></div>
<div class="m2"><p>مژده شما را که ز سر تازه شد</p></div></div>