---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>زهی فرمانده مطلق جهان را</p></div>
<div class="m2"><p>مشرف کرده نامت هر زبان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک بر تخت شاهی نانشانده</p></div>
<div class="m2"><p>چو تو یک خسروخسرو نشان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اثرهای بزرگت شاد کرده</p></div>
<div class="m2"><p>روان طغرل الب ارسلان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو سایه بست برفتراک چترت</p></div>
<div class="m2"><p>سپهر پیر اقبال جوان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدست پایدار دولت تو</p></div>
<div class="m2"><p>گرفته دامن آخر زمان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با یام تو امید بزرگ است</p></div>
<div class="m2"><p>مبارک دوده سلجوقیان را</p></div></div>