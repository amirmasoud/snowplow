---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>هرکه عشقت خرید جان بفروخت</p></div>
<div class="m2"><p>و آن خریدن بدو جهان بفروخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هوای تو دل قفس بشکست</p></div>
<div class="m2"><p>ز قفس بگذر آشیان بفروخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که نام تو خواست برد نخست</p></div>
<div class="m2"><p>بر مراد و ادب زبان بفروخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانکه یک روز شد معامل تو</p></div>
<div class="m2"><p>تا بس دیر خانمان بفروخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو دیده خیال یافت نشان</p></div>
<div class="m2"><p>دل خود را بر آن نشان بفروخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان اگر بر تو صرف شد سهل است</p></div>
<div class="m2"><p>هرکه جانان خرید جان بفروخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتو در بست دل بهیچ مرا</p></div>
<div class="m2"><p>سبکی را چنین گران بفروخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آری ارزان خریده بود متاع</p></div>
<div class="m2"><p>چون در افتاد رایگان بفروخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وانگهی گفت چون اثیری را</p></div>
<div class="m2"><p>کس بدین مایه سوزیان بفروخت</p></div></div>