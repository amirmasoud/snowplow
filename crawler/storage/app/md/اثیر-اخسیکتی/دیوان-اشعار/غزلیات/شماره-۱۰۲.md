---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>چند خورم خون خود ازدست دل</p></div>
<div class="m2"><p>شستم از دوست بهفت آب و گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین شبش او داند و شمع ختن</p></div>
<div class="m2"><p>زین قبل او داند و ماه چگل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیدلی ار زانکه بدین چاشنی است</p></div>
<div class="m2"><p>باد دل از من بدو عالم به حل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز اگر می برود گو برو</p></div>
<div class="m2"><p>نیست غم او همه بر من سجل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فارغم از دل من و طبعی چو آب</p></div>
<div class="m2"><p>ساخته با مدح شه صف گسل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرهمه سنگ است چو مومش کند</p></div>
<div class="m2"><p>آتش سودای بتی سنگدل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو خسرو فش خسرو نسب</p></div>
<div class="m2"><p>مظفرالدولت والدین قِزل</p></div></div>