---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>چون مرا دولت وصل تو، شبی روزی نیست</p></div>
<div class="m2"><p>زانکه در مذهب من، عیدی و نوروزی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم آن لعل صفت محنت من برشکند</p></div>
<div class="m2"><p>چرخ پیروزه ندا کرد که پیروزی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند گوئی، که بدآموزی صاحب غرض است</p></div>
<div class="m2"><p>عادت بوالعجب توست، بدآموزی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیلکی باز کن از غمزه، بدآموزان را</p></div>
<div class="m2"><p>زانکه در عادت تو سنت دلدوزی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم دلسوختگان، دامن تو کی گیرد</p></div>
<div class="m2"><p>در جهان تو. چو غمخواری و دلسوزی نیست</p></div></div>