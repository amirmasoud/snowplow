---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>روی تو، ممالک جهان ارزد</p></div>
<div class="m2"><p>وصل تو، حیات جاودان ارزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان لعل که صد هزار دل دارد</p></div>
<div class="m2"><p>یک بوسه بصد هزار جان ارزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نام چو منی همی بری خه خه</p></div>
<div class="m2"><p>این نام بدین لب و دهان ارزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان، بندگی تو را کمر در بست</p></div>
<div class="m2"><p>انصاف بده که رایگان ارزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم افکندم بدان گل عارض</p></div>
<div class="m2"><p>تا خود به هزار بوستان ارزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل گفت اثیر اگر بصر داری</p></div>
<div class="m2"><p>نیکو بنگر که بیش از آن ارزد</p></div></div>