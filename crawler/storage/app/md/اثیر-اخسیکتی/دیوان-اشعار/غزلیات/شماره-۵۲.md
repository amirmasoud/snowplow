---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>از تو هر آنچه برمن درویش میرود</p></div>
<div class="m2"><p>راضی شدم چو برهمه، زین بیش میرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منشین بجور در پس افلاک چون مهت</p></div>
<div class="m2"><p>بر سر گرفته غاشیه در پیش میرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای تشنه جمال تو چشمم، بیاد آر</p></div>
<div class="m2"><p>کابت همه بجوی بداندیش میرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تشنکیش در عجبم خاصه کاین زمان</p></div>
<div class="m2"><p>بر رخ دو جویم از جگر ریش میرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دولت غم تو به محنت غنی شدم</p></div>
<div class="m2"><p>نیک است اینکه با من درویش میرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی کام خوش کنم بوصال تو چون تورا</p></div>
<div class="m2"><p>همراه نیم نوش دو صد نیش میرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیش تو چیست جور و کنون بر موافقت</p></div>
<div class="m2"><p>دور فلک چو تیر بر آن کیش میرود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کوی تو زمانه مرا گفت گوش دار</p></div>
<div class="m2"><p>پایت بقصد خون سر خویش میرود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل گفت: رو که دست نیالاید او بما</p></div>
<div class="m2"><p>مهتاب او بگشتن بَرخیش میرود</p></div></div>