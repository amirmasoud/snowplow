---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>نام تو، بهر زبان در افتاد</p></div>
<div class="m2"><p>شوری به همه جهان در افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حیرت عارض تو خورشید</p></div>
<div class="m2"><p>از طارم آسمان در افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هنگام نظاره تو حورا</p></div>
<div class="m2"><p>از کنگره ی جنان در افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راز تو نهان چگونه دارم</p></div>
<div class="m2"><p>کاین قصه بهر زبان در افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق تو خریده شد بجانی</p></div>
<div class="m2"><p>یارب که چه رایگان در افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انصاف بده چنان همائی</p></div>
<div class="m2"><p>سگ را بیک استخوان در افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را چو اثیر خویش خواندی</p></div>
<div class="m2"><p>سیلاب به خانمان در افتاد</p></div></div>