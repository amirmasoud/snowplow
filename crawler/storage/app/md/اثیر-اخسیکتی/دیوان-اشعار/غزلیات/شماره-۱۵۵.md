---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>گره مشک، بر سمن چه زنی</p></div>
<div class="m2"><p>لشگر زنگ، برختن چه زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ز لعل تو، بوسه ئی طلبم</p></div>
<div class="m2"><p>بر شکر لولوء عدن چه زنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد گریبان دریده است از تو</p></div>
<div class="m2"><p>چاک، برطرف پیرهن چه زنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو گوئی، که جان نفس نزنم</p></div>
<div class="m2"><p>من چه گویم، که بوسه تن چه زنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر لب اوست خط اجره ی تو</p></div>
<div class="m2"><p>دست بر زلف پرشکن چه زنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقی ای اثیر و یارت اوست</p></div>
<div class="m2"><p>همه دانند لاولن چه زنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پسرا، هست روز آن که تو روی در وفا کنی</p></div>
<div class="m2"><p>ز من ار پند بشنوی، ره وحشت رها کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه چنان پای در گلم، که ز تو مهر بگسلم</p></div>
<div class="m2"><p>چو خبر داری از دلم، بوفا گر صفا کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بکند چشم آشنا، همه شب در سرشک خون</p></div>
<div class="m2"><p>اگرش با خیال خود، نفسی آشنا کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دو جهان نهد سر بدین، سرای بو که تا مگر</p></div>
<div class="m2"><p>قدمی بر سمک نهی، گذری بر سما کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز رخ تو آفتاب و مه، بحدق برند جمله ره</p></div>
<div class="m2"><p>تو در این موکب وسپه، نکنی تا یکجا کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طمع بوسه است و بس، زلب تو اثیر را</p></div>
<div class="m2"><p>بسر تو، گر که اینقدر طمع او ادا کنی</p></div></div>