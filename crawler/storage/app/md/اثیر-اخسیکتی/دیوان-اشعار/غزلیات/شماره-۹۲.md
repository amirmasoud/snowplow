---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>دلا فتراک آن جان و جهان گیر</p></div>
<div class="m2"><p>وگرنه ترک من گو دست جان گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا در مملکت جائی است صافی</p></div>
<div class="m2"><p>بر او سودی نمیگیرم زیان گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآوردم به ننک از عشق نامی</p></div>
<div class="m2"><p>بهر نامم که خواهی در زبان گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا گوئی جهانی خصم داری</p></div>
<div class="m2"><p>بشو در خون خود جان جهان گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبکپائی نه از فتوی عشق است</p></div>
<div class="m2"><p>تو خود بر من اجل را سرگران گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشم صبری است یعنی در کمینم</p></div>
<div class="m2"><p>بقوت دست و بازوی کمان گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین لشکر تو با اوکی برآئی</p></div>
<div class="m2"><p>برو درگاه سلطان ارسلان گیر</p></div></div>