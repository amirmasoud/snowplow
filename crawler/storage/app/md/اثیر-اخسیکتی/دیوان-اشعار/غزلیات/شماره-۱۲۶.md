---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>خیز تا دست طرب یکدم، بجام می زنیم</p></div>
<div class="m2"><p>دوستگانی بر رخ ماه مبارک پی زنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای در میدان عشق لعبتان عُز نهیم</p></div>
<div class="m2"><p>دست بر فتراک مهر لعبتان ری زنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما کم از پروانه ایم آخر، مگر می آتش است</p></div>
<div class="m2"><p>هرچه بادا باد، بل تا خویشتن بر وی زنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لشگری میسازد او باش خرابات آنگهی</p></div>
<div class="m2"><p>قصد تاج خان کنیم ورای ملک کی زنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قوت دلهای نازک، در گل ما تعبیه است</p></div>
<div class="m2"><p>حسبة لله سنگی بر سر این کی زنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بر این آهنگ زیرش مستی ما بگسلد</p></div>
<div class="m2"><p>هستی یکسر زخمه ئی بربم و بر لاشی زنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او قدح ها در کشد، زین باده و لب نسترد</p></div>
<div class="m2"><p>ما ببوئی جرعه ی صد سال هوئی هی زنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دمی دیگر، دم عالم فرو خواهد شدن</p></div>
<div class="m2"><p>ما ز صحبت گر دمی داریم با هم کی زنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر دمی دیوانه با ما، دم زند همچون اثیر</p></div>
<div class="m2"><p>آهی از دل بر کشیم و آتش اندر نی زنیم</p></div></div>