---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>در فراقت طاقت من گشت طاق</p></div>
<div class="m2"><p>مستغاث از جور و بیداد ازفراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جفت اندوه و فغانم روز و شب</p></div>
<div class="m2"><p>تا بماند ستم، من از وصل تو طاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز خیال تو ندارم هم نشین</p></div>
<div class="m2"><p>جز غمان تو ندارم هم وثاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلق عالم شرح نتوانند داد</p></div>
<div class="m2"><p>آنچه من در سینه دارم از فراق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه ممکن بُد بکردم من ولیک</p></div>
<div class="m2"><p>دولت وصلم نیفتاد اتفاق</p></div></div>