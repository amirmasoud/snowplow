---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>یا رب آن ماه تمام، آن من است</p></div>
<div class="m2"><p>که بقد سرو خرامان من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با سر زلف پریشان، همه روز</p></div>
<div class="m2"><p>در پی کار پریشان من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر جاوید طمع میدارم</p></div>
<div class="m2"><p>که لبش چشمه ی حیوان من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن لب، آنلب، که شکر بنده اوست</p></div>
<div class="m2"><p>کس چه داند، چه بدندان من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمزه او، همه کفر است و لیک</p></div>
<div class="m2"><p>کفر او، بهتر از ایمان من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سک کوی ویم نیست دریغ</p></div>
<div class="m2"><p>سخنش، کز همه در جان من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که دیوانگیم از سر اوست</p></div>
<div class="m2"><p>زلف او سلسله جنبان من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جرم زنجیر وی است اینکه زغم</p></div>
<div class="m2"><p>پیرهن بر تن، زندان من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اینکه در پای صد اندوهم گشت</p></div>
<div class="m2"><p>هم دلی بی سر و سامان من است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این همه هست و همی گوید اثیر</p></div>
<div class="m2"><p>«یا رب، آن ماه تمام آن من است»</p></div></div>