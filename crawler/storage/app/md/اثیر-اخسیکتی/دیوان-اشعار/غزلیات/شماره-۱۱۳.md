---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>از دل گره غم تو بگشادم</p></div>
<div class="m2"><p>سودای تو از دماغ بنهادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برمن دگری گزیده ئی شاید</p></div>
<div class="m2"><p>او را بتو و تو را باو دادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمری است که خاک تو همی بوسم</p></div>
<div class="m2"><p>معلومم شد کنون که بربادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک چند زجور تو برآسایم</p></div>
<div class="m2"><p>گر دولت عافیت دهد دادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بل تا، زره سپهر باز افتد</p></div>
<div class="m2"><p>روزی دو سه کاروان فریادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من بنده بخت فرخ خویشم</p></div>
<div class="m2"><p>کز دست غم تو کرد آزادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمری بگذاشتم که یکساعت</p></div>
<div class="m2"><p>در عشق تو کس ندید دل شادم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>والله که کنون چنین همی دانم</p></div>
<div class="m2"><p>کاین دم ز مشیمه جهان زادم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگماشت خدای رامردی را</p></div>
<div class="m2"><p>تا محنت تو ببرد از یادم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حالی باری ز ظالمان جستم</p></div>
<div class="m2"><p>هر چند بکافری در افتادم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای طبع اثیر برهمی میزن</p></div>
<div class="m2"><p>کز دل گره غم تو بگشادم</p></div></div>