---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>آن زلف مشوش بین در عنبر و بان منگر</p></div>
<div class="m2"><p>و آن قامت دلگش بین در سروروان منگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بالعل لبش خطی درنام بدخشان کش</p></div>
<div class="m2"><p>آسایش جانداری ز آسایش جان منگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوید که جهان و جان تاخیر مکن گو، هان</p></div>
<div class="m2"><p>کان جان و جهان آمد در جان جهان منگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با شخص فنادشمن بر راه سران منشین</p></div>
<div class="m2"><p>با دیده ی نامحرم در روی بتان منگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او معنی دل دارد تو صورت و تن بینی</p></div>
<div class="m2"><p>با چشم چنین هی هی در یار چنان منگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش از تو اثیر از ری بیزار شود لیکن</p></div>
<div class="m2"><p>در سوز دلش می بین در قول زبان منگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر صاحب تمکینی در حضرت عشق تو</p></div>
<div class="m2"><p>چون همت خسرو کی در کون و مکان منگر</p></div></div>