---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>دوش در عیش و عشرتی بودم</p></div>
<div class="m2"><p>کز طرب تا بروز نغنودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا ربود و شراب و شمعی و من</p></div>
<div class="m2"><p>زحمت اندر میانه، من بودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وصالش غمی فرو گفتم</p></div>
<div class="m2"><p>وز جمالش دمی، بر آسودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه کام نشاط خوش کردم</p></div>
<div class="m2"><p>گاه جام طرب به پیمودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گره هجر و بند گیسوی یار</p></div>
<div class="m2"><p>هر دو با هم بلطف بگشودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست با چرخ در کمر گردم</p></div>
<div class="m2"><p>پای بر ماه و مشتری سودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواجه گیها، زمانه در سر داشت</p></div>
<div class="m2"><p>لیک من، بندگیش فرمودم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چار بوسم زیار را تب بود</p></div>
<div class="m2"><p>پنج دیگر ز راه بربودم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ده به بخشید بعد از آنم لیک</p></div>
<div class="m2"><p>بستدم بر لبش، به بخشودم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با چنین عیش ظلم باشد اگر</p></div>
<div class="m2"><p>گویم از بخت خود نه خشنودم</p></div></div>