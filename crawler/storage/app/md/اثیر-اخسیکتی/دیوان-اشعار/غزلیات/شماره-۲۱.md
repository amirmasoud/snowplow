---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>با دل بد عهد تو گاو گل سر دامن است</p></div>
<div class="m2"><p>گنبد نیلوفری در یک پیراهن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک نفس آخر بدار گر نه چو چرخ بلند</p></div>
<div class="m2"><p>گرد جهان گشتنت بهر ستم گردن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معدن گوهر شده است بی تو دو چشمم ولیک</p></div>
<div class="m2"><p>آن لب یاقوت رنگ گوئی از این معدن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ دلان را بسی صید کنی تا بشکل</p></div>
<div class="m2"><p>زلف توچون پای دام خال توچون ارزن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طره میگون تو تیره کند آب من</p></div>
<div class="m2"><p>نزد من احوال او همچو رخت روشن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد دل او هم سوار دررخم او گرچه دید</p></div>
<div class="m2"><p>کان شب خورشید نعل همچو فلک توسن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تو در آویختم همچو قبای تو زانک</p></div>
<div class="m2"><p>خون من وجیب تو هر دو در آن کردن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشتری وصل تو گر بفروشی منم</p></div>
<div class="m2"><p>نقدم اشک و حسب، جنسم جان و تن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طنز کنی کای اثیر هم سخن آورده ئی</p></div>
<div class="m2"><p>بیش تو شرمی مدار کاین سخنت با من است</p></div></div>