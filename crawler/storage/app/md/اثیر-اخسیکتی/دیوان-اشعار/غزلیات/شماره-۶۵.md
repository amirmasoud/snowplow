---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>تن بی غم تو جان نمی‌خواهد</p></div>
<div class="m2"><p>جان بی رخ تو جهان نمی‌خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو، کور دلی که بر دل از عشقت</p></div>
<div class="m2"><p>مانند سگ استخوان نمی‌خواهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم بکن اینقدر، که جان از تو</p></div>
<div class="m2"><p>جز یک دو نفس امان نمی‌خواهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با دیده چگونه‌ای که جز نقشی</p></div>
<div class="m2"><p>از کیسه تو زیان نمی‌خواهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم بکنم چنین، چه می‌خواهی</p></div>
<div class="m2"><p>هم چین چو دلت چنان نمی‌خواهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان می‌دهدت به یک نظر چندین</p></div>
<div class="m2"><p>بندیش که رایگان نمی‌خواهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سهل است اثیر چون مراعاتی</p></div>
<div class="m2"><p>از تو به سر زبان نمی‌خواهد</p></div></div>