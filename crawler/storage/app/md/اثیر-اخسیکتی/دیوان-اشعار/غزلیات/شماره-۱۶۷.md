---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>زلف چون بر عذار میفکنی</p></div>
<div class="m2"><p>لیل را در نهار میفکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون لبت مست لطف میگردد</p></div>
<div class="m2"><p>باده را در خمار میفکنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانی آویخته است برفتراک</p></div>
<div class="m2"><p>تا نظر بر شکار میفکنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کجا مهر در میان آمد</p></div>
<div class="m2"><p>خویشتن بر کنار میفکنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرما با تو کی دود که بجور</p></div>
<div class="m2"><p>اسب بر روزگار میفکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو سوزن، اگرچه سرتیزی</p></div>
<div class="m2"><p>بخیه بر روی کار میفکنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صف ناموس تو شکست آرد</p></div>
<div class="m2"><p>زانکه در چنگ یار میفکنی</p></div></div>