---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>یا رب آن روح ممثل چه خوش است</p></div>
<div class="m2"><p>بر گلش زلف مسلسل چه خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان دو صف رسته لولوی عدن</p></div>
<div class="m2"><p>بدو یاقوت مکلل چه خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غدر رنگینش با خرقه نکوست</p></div>
<div class="m2"><p>ناز شیرینش با دل چه خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا رب آن عهد دروغش که بدو</p></div>
<div class="m2"><p>نتوان بود موصل چه خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وان مراعات مزور که بدان</p></div>
<div class="m2"><p>نتوان کرد معول چه خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با اثیر است همین گوید و بس</p></div>
<div class="m2"><p>یارب این روح ممثل چه خوش است</p></div></div>