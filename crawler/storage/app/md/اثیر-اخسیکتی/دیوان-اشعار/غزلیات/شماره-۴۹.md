---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>گر نقاب از دورخ براندازد</p></div>
<div class="m2"><p>عالم از عافیت بپردازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرصه روزگار تنک آید</p></div>
<div class="m2"><p>باره ی حسن اگر برون تازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفلک بر، زنور عارض او</p></div>
<div class="m2"><p>ماه با آفتاب بگدازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل بر گوشه بساط عدم</p></div>
<div class="m2"><p>همه نقد وجود دربازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر زمین بر، زرشک قامت او</p></div>
<div class="m2"><p>سرو همچون هلال بکرازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر غمش سر بجان فرود آرد</p></div>
<div class="m2"><p>دل ز شادی کله براندازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصلش اخسیکتی امیدمدار</p></div>
<div class="m2"><p>که وفا با جمال کم سازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه با روزگار ناز کند</p></div>
<div class="m2"><p>چون توئی را چگونه بنوازد</p></div></div>