---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>هر آنکس را که دلداری چو آن سرو سهی باشد</p></div>
<div class="m2"><p>نه پندارم که جانش را ز تیمار آگهی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهین منتش هستند در هر گوشه ئی صد دل</p></div>
<div class="m2"><p>وگر نزدیک تر خواهی یکی ز ایشان رهی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش افتاده است با بیماری عشقم چو چشم او</p></div>
<div class="m2"><p>مباد آندم کزین بیماریم روز بهی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سزد گرماه نوسازد رکاب از آسمان مرکب</p></div>
<div class="m2"><p>هر آن دلرا که با سوداش کامی همرهی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن کوته ندانم کرد، در هنگامه مهرش</p></div>
<div class="m2"><p>کسی کز وی سخن گوید، چه جای کوتهی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دماغی پر سمر دارم، از آن کهتر نوازیها</p></div>
<div class="m2"><p>بگویم با نو، چون مجلس زنا اهلان تهی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبی در خدمتش بر آسمان، خواهم زدن خیمه</p></div>
<div class="m2"><p>چو جام پرده در جفت سماع خر گهی باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اثیرا چون فلک گردت باسم بندگی تمکین</p></div>
<div class="m2"><p>اگر تمکین کنی دور فلک را، ابلهی باشد</p></div></div>