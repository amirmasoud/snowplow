---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>در عشق تو یک کار مرا ساز و نسق نیست</p></div>
<div class="m2"><p>خون میخورم از غصه و سامان نطق نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد بفذلک ز غمت دفتر عمرم</p></div>
<div class="m2"><p>گر ما بقئی هست به جز یک دو ورق نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمم مه رخسار تو را باز مبیناد</p></div>
<div class="m2"><p>گردر شب هجران تو چشمم چوشفق نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مملکت درد، نشان می ندهد کس</p></div>
<div class="m2"><p>یک کار که ازعشق تو بی ساز و نسق نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق از تو، چو نیلوفر تا حلق درآبند</p></div>
<div class="m2"><p>وز شرم تو را بر گل رخسار عرق نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما نیز رضای تو گزیدیم، چو کس را</p></div>
<div class="m2"><p>بر هرچه هوای تو کند، زهره دق نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی دید اثیر از تو وفا، خاصه که امروز</p></div>
<div class="m2"><p>در قالب عالم ز وفا هیچ رمق نیست</p></div></div>