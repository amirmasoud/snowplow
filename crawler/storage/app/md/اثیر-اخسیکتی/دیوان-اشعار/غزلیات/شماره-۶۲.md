---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>دل ز دست غمت بجان نجهد</p></div>
<div class="m2"><p>عقل جز خسته بر کران نجهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک پاشی چو تو ندیدم من</p></div>
<div class="m2"><p>کز کفت باد رایگان نجهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لبت هرکه گوهری طلبد</p></div>
<div class="m2"><p>به هزاران هزار کان نجهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتواند گریخت از تو دلی</p></div>
<div class="m2"><p>تا به حیلت در آن جهان نجهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وعده ئی گرده ئی به کشتن من</p></div>
<div class="m2"><p>کوش تا باد در میان نجهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلف تو دست بر اثیر نهد</p></div>
<div class="m2"><p>آه کز دست او به جان نجهد</p></div></div>