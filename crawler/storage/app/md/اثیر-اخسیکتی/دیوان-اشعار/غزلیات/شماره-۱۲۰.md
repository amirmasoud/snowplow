---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>بی شب زلف تو سیه روزم</p></div>
<div class="m2"><p>خسته روزگار کین توزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محرم بزم خوبی تو منم</p></div>
<div class="m2"><p>که بیک آه می بر افروزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تو را حسن نیک میسازد</p></div>
<div class="m2"><p>چشم بددور، خوش همی سوزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرهمی نه، که بخت دلریشم</p></div>
<div class="m2"><p>چینه ئی ده، که بس نوآموزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرمی را بنقد شب خوش باش</p></div>
<div class="m2"><p>تا چه از راز نسیه روزم</p></div></div>