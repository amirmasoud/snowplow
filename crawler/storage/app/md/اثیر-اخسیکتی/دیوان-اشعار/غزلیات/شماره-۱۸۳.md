---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>کره مشک بر سمن چه زنی</p></div>
<div class="m2"><p>لشکر زنگ بر ختن چه زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ز لعل تو بوسه ئی طلبم</p></div>
<div class="m2"><p>بر شکر لولو عدن چه زنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد کریبان دریده است از تو</p></div>
<div class="m2"><p>چاک بر طرف پیرهن چه زنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو گوئی که جان نفس نزنم</p></div>
<div class="m2"><p>من چه گویم که بوسه تن چه زنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر لب اوست خط اجره ی تو</p></div>
<div class="m2"><p>دست بر زلف پرشکن چه زنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقی ای اثیر و یارت اوست</p></div>
<div class="m2"><p>همه دانند لا ولن چه زنی</p></div></div>