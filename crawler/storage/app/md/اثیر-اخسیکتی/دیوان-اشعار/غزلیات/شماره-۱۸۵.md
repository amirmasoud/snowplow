---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>ای بنده ی لب تو، لب آبدار می</p></div>
<div class="m2"><p>گلگونه کرده عکس رخت بر عذار می</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تخت هوس نهاده رخت بر بساط گل</p></div>
<div class="m2"><p>رخت خرد فکنده لبت، در جوارمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون صبح جامه چاک زده، غنچه حباب</p></div>
<div class="m2"><p>پیش نسیم زلف تو، بر جویبار می</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هم شکن شماری، ز نگاری فلک</p></div>
<div class="m2"><p>چون از فتنه موج برآرد بحار می</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم سیاه گردان بر ذوالخمار غم</p></div>
<div class="m2"><p>دست طرب چو لعل کند ذوالفقار می</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگرفت ملک شادی و برداشت رسم غم</p></div>
<div class="m2"><p>اینست کمترین اثر گیر و دار می</p></div></div>