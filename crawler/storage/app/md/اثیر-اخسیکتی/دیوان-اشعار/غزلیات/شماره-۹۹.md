---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>در خون دل نشاندم روی ازفراق رویش</p></div>
<div class="m2"><p>قدی چو سرو گردم موی از فراق رویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوئی است ز آب خضرش، در چشمه لب من</p></div>
<div class="m2"><p>بر شیب رخ براندم، جوی از فراق رویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا آب صبح صادق، رویش بگشت برشب</p></div>
<div class="m2"><p>بر خاک تیره دادم، خوی از فراق رویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در لاله می، نه بینم رنگ از وصال رنگش</p></div>
<div class="m2"><p>وز گل نمی پذیرم بوی از فراق رویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی گوی غبغب او چوگان قد اثیر است</p></div>
<div class="m2"><p>چوگان هربلا را، گوی ازفراق رویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عشق یکزمان زدل من نفورباش</p></div>
<div class="m2"><p>ای دل چو عاشقی به بلاها صبور باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق آتشی است کاب دودیده شراراوست</p></div>
<div class="m2"><p>دادمت پند و گفتمت زین کاردور باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیوانه وار بسته زنجیر زلف باش</p></div>
<div class="m2"><p>پروانه وار سوخته نارو نور باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاهی بسان آتش سوزان زبانه زن</p></div>
<div class="m2"><p>گاهی میان آتش سوزان بخور باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای عاشقی که موی شکافی بکار عشق</p></div>
<div class="m2"><p>ز آهن شکاف غمزه خوبان، حذور باش</p></div></div>