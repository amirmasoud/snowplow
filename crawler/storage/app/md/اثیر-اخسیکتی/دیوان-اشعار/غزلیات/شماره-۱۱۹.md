---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>کو محرمی که قصه تو در میان نهم</p></div>
<div class="m2"><p>گوش سخن بگیرم و در یک کران نهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد، به نیوش وصل بیک رمز سر بمُهر</p></div>
<div class="m2"><p>از دست دل برآرم و در دست جان نهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک ره، اجازت کرمم ده ز بندگی</p></div>
<div class="m2"><p>تا محنتی ز صحبت او بر کسان نهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش کن بوعده ئی، دل من، گو خلاف باش</p></div>
<div class="m2"><p>تا چشم انتظار، به عمری در آن نهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست خوش توام بزبان خوشم بدار</p></div>
<div class="m2"><p>تا من بلطف، نام تو اندر زبان نهم</p></div></div>