---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ز میان ببرد ناگه، دل من بتی شکر لب</p></div>
<div class="m2"><p>بدو رخ برادر مه، بدو زلف نایب شب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو کمند عنبرینش، ز خم و گره مسلسل</p></div>
<div class="m2"><p>دو عقیق شکرینش، ز دو گوهر مرکب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدم نظر شکسته، رخش از فروغ بی‌حد</p></div>
<div class="m2"><p>گذر سخن ببسته، دهنش ز تنگی لب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوهزار جان تشنه، نگرد در او و او را</p></div>
<div class="m2"><p>پر از آب زندگانی، شده روی چاه غبغب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شده کیسه‌دار دل‌ها، دلش از طویله دُر</p></div>
<div class="m2"><p>زده کاروان جانها، مهش از میان عقرب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنشستم و زمانی، به رخش نگاه کردم</p></div>
<div class="m2"><p>دل از این نشسته در خون تن از آن فتاده در تب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو سؤال بوسه کردم، به کرشمه گفت با من</p></div>
<div class="m2"><p>تو نه مرد این حدیثی «فاذا فرغت فانصب»</p></div></div>