---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>یاد میدار که از مات نمی آید یاد</p></div>
<div class="m2"><p>ای امید من و عهد تو سراسر همه باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکنی یک طرف از قصه ی من هرگز گوش</p></div>
<div class="m2"><p>نه زِیم یک نفس از غصه تو هرگز شاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاوری نیست، که با خصم تو بردارم تیغ</p></div>
<div class="m2"><p>داوری نیست، که از هجر تو بستانم داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو نگفتی که وصالم برساند بخودت</p></div>
<div class="m2"><p>راستی نیک رسانید که چشمت مرساد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی ار، فاش کنی عشق پری، جان نبری</p></div>
<div class="m2"><p>نبرم، خود نبرم حسن تو جاوید زیاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر غرض خون من است از سر اینک سروطشت</p></div>
<div class="m2"><p>ور نه این طشت سه سال است که ازبام افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من بر این تهمت اگر کشته شوم باکی نیست</p></div>
<div class="m2"><p>همه سرسبزی کمتر سک دربان تو باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عافیت خواستی از من خیرالله جزاک</p></div>
<div class="m2"><p>او همان شب بعدم رفت که خیر تو بزاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گله ی وصل تو با هجر تو میگفتم دوش</p></div>
<div class="m2"><p>که ستد عمرو از او هیچ بجز غم نگشاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق ما مظلمه ی کس بقیامت نبرد</p></div>
<div class="m2"><p>گر ز تو عمر ستد در عوضش عشق بداد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در میان روی بمن کرد خیالت که اثیر</p></div>
<div class="m2"><p>زین سخن بگذر و این واقعه بگذارز، یاد</p></div></div>