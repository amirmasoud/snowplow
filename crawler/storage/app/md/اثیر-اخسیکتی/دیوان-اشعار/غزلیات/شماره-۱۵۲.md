---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>ای مرا چون جان گرامی جام جانپرور بخواه</p></div>
<div class="m2"><p>چون رخ و اشک من و خود، باده احمر بخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعل جان آشوب بگشا، بهر جان دارو بیار</p></div>
<div class="m2"><p>زلف جان آویز بشکن جام جان پرور بخواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو زلفت سر گرانم، ساعتی دیگر بپای</p></div>
<div class="m2"><p>همچو چشمت نیم مستم، ساغر دیگر بخواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده احمر تو را، از دست غم بیرون کند</p></div>
<div class="m2"><p>چاکر او باش و کین، از گنبد اخضر بخواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز بارست ارسلان سلطان می را، زود باش</p></div>
<div class="m2"><p>از حباب و جام، هم اورنگ و هم افسر بخواه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون زبرپوش فلک، پوشید باغ و خانه زیب</p></div>
<div class="m2"><p>درد سرمشمر، کله دیوی سبک با سر بخواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگهت از گل عاریت کن لذت از شکر بگیر</p></div>
<div class="m2"><p>زینت از فردوس بستان، صفوت از کوثر بخواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرخ را گو، چتر خورشید و دف کردان بده</p></div>
<div class="m2"><p>ماه را گو، بربط ناهید خیناگر بخواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجلسی برساز و آنگه بر غزل‌های اثیر</p></div>
<div class="m2"><p>بادهٔ چون آفتاب از ترک مه‌پیکر بخواه</p></div></div>