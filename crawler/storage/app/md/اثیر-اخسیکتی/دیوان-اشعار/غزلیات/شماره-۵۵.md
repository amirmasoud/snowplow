---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>دهان تنگ آن دلبر نشان طبع من دارد</p></div>
<div class="m2"><p>که در یک نقطه و همی جهانی در وطن دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهرها در شکم دارد لب یاقوت فام او</p></div>
<div class="m2"><p>وزاو سربسته هر نکته شکرها در شکن دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان خندد که پنداری صبا بر لؤلؤ شبنم</p></div>
<div class="m2"><p>دهان لاله رعنا فرا روی چمن دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهان چون چشمه خضر است هر کزوی کنف جوید</p></div>
<div class="m2"><p>سر حسرت گرفته چون سکندر در کفن دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون روح القدس معصوم است و زماروی می پوشد</p></div>
<div class="m2"><p>چو حور العین هم جانست و یاقوتی بتن دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دمی کزوی صفت گوید چو احمد مهرلاجوید</p></div>
<div class="m2"><p>لبی کزوی عصا جوید چو موسی داغ لن دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن های فراخ او که در عالم نمی گنجد</p></div>
<div class="m2"><p>شگفت آید بدان تنگی که او جای سخن دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مهمان خانه عصمت نمکدان ملائک را</p></div>
<div class="m2"><p>کسی داند که گوش جان بدان شیرین دهن دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کجا رمزی در اندازد قتیلی چون حسین آرد</p></div>
<div class="m2"><p>کجا زهری برافشاند شهیدی چون حسن دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خرد شارب همی خواند نشانی را که پنداری</p></div>
<div class="m2"><p>سواد لاله بر عنوان درج یاسمن دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشاط آهوان غمزه ی او خود عجب نبود</p></div>
<div class="m2"><p>که گرد سبزه جان سبز از مشک ختن دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان را مژده میآرد بشعر آبدار من</p></div>
<div class="m2"><p>بدین شادی دهانش چرخ پر در عدن دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی گوید بحمدالله اثیر امروز در کیهان</p></div>
<div class="m2"><p>طراوت نظم او دارد که بوی عشق من دارد</p></div></div>