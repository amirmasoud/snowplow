---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>هنگامه ی خورشید ز رخسار تو بشکست</p></div>
<div class="m2"><p>بازارچه سرو ز رفتار تو بشکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر تعبیه لطف که در تازه گلی بود</p></div>
<div class="m2"><p>قدرش زرخ زر تو برتار تو بشکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر رونق و ناموس که هر لعل و گهرداشت</p></div>
<div class="m2"><p>با قاعدد لعل گهربار تو بشکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برچید دکان عقل و بپرداخت وطن صبر</p></div>
<div class="m2"><p>ساز حیل هردو چودرکار تو بشکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف تو ز ما بس که تراش دل و دین کرد</p></div>
<div class="m2"><p>تا تیشه او تیز ز پیکار تو بشکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس پرده مصحف که چلیپای تو بدرید</p></div>
<div class="m2"><p>بس حرمت سجاده که زنار تو بشکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کیسه دل طره طرار تو بشکافت</p></div>
<div class="m2"><p>صد لشکر جان غمزه خونخوار تو بشکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کیسه دل طره طرار تو بشکافت</p></div>
<div class="m2"><p>صد لشکر جان غمره خونخوار تو بشکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بازار بتان تا بکنون داشت رواجی</p></div>
<div class="m2"><p>واکنون که اثیر است خریدار تو شکست</p></div></div>