---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>جانم فدای توست، که جانان من توئی</p></div>
<div class="m2"><p>شمع وثاق و تازه گلستان من توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستند شاهدان شکر لب بعهد تو</p></div>
<div class="m2"><p>لیکن از آنمیانه بدندان من توئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان بر سر غم تو نهم، وزمن این سخن</p></div>
<div class="m2"><p>بی حرمتی است جان، چه بود، جان من توئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عشق تو بخدمت سلطان برآمدم</p></div>
<div class="m2"><p>ای مه، سعادت تو که سلطان من توئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکس که گفت اثیر، بزنگان چه میکنی</p></div>
<div class="m2"><p>زین نکته غافل است، که زنگان من توئی</p></div></div>