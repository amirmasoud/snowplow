---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>والله که به بیباکی، ناموس جهان بردی</p></div>
<div class="m2"><p>حقا که به چالاکی، آرام روان بردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آورد بر این زلفت، چون کان می کردون</p></div>
<div class="m2"><p>رو، رو که بدان چوگان گوی از همگان بردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان بود که میگفتم بند سر زلفینش</p></div>
<div class="m2"><p>رغم من مسکین را، هم دست بدان بردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا خود سر زلفینت، بگشوده همی بینم</p></div>
<div class="m2"><p>هین ای دل زندانی بگریز که جان بردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دشنام دهان از من چون بر گذری گویم</p></div>
<div class="m2"><p>یارب من و آن، کاخر نامم بزبان بردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کم بار دهی بازم بر درگه بار خود</p></div>
<div class="m2"><p>این رسم چنین دانم، زان تنگ دهان بردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی فرهت ندهم، صد نقش گر آوردی</p></div>
<div class="m2"><p>و آخر به سبکدستی، چیزی ز میان بردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هر سخنی پیچم، در تو چو یقین دیدم</p></div>
<div class="m2"><p>روی از تو نه پیچانم بر من چو گمان بردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی که اثیر از ما، در صبر گریز، آری</p></div>
<div class="m2"><p>حال رمه دانستم، چون نام شبان بردی</p></div></div>