---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>گه بود ماه که با روی تو از کوه بر آید</p></div>
<div class="m2"><p>چه زند سرو که با قد تو بالا بنماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا بوی تو آمد ز صبا گرد نخیزد</p></div>
<div class="m2"><p>هر کجا روی تو آمد ز سحر صبح نیاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمت آورد بدر صبر. خرد گفت که حقا</p></div>
<div class="m2"><p>اگر او اوست که من دانم ز و جور نشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی ار بر سر این مهر بپائی بخوری بر</p></div>
<div class="m2"><p>باش اینجا، سخنی هست اگر عمر بپاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبر زندان فراق تو شکستن نتواند</p></div>
<div class="m2"><p>ور بدندان همه آن است که زنجیر نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی کس نبود وصل تو، یا بخت من این است</p></div>
<div class="m2"><p>که شب حامله جز هجر همی هیچ نزاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بار این حادثه من خسته، بمنزل برسانم</p></div>
<div class="m2"><p>گر در آن سر که جفاهای تو باشد مگر آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفته بودی بخورم خون دلت مصلحت این است</p></div>
<div class="m2"><p>گوشمالیش بدین جور که او کرد بباید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاید ای دوست همین آید از آن خو که تو داری</p></div>
<div class="m2"><p>ور جز این آید از آن خو که تو را هست نشاید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرده گیر آن همه لیکن، پس از این خوی بدتو</p></div>
<div class="m2"><p>کاین برآید بفر دوست رساند چه سر آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشوه میداد وصال تو که روزی بتوانم</p></div>
<div class="m2"><p>عقل میگفت اثیرا مشنو هرزه سُراید</p></div></div>