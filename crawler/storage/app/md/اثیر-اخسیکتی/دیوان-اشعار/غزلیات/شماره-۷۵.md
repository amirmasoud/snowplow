---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>یارستم پیشه باز، دست جفا می برد</p></div>
<div class="m2"><p>و ز همه یاران سخن، دست بمامی برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ جفا، راست کرد طره اوتاجهان</p></div>
<div class="m2"><p>وای دماغی کزو بوی وفا می برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت که سر کم ندید از درما عاشقان</p></div>
<div class="m2"><p>نیک بدان کاین سخن سربکجا می برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نایب زلفین اوست، شحنه مژگان او</p></div>
<div class="m2"><p>هرکه در این روزگار نام جفا می برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راه فرو بسته ام، بر گذر راه از آنک</p></div>
<div class="m2"><p>قصه بیداد او سوی سما می برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند تظلم کنی ای دل رعنا که هست</p></div>
<div class="m2"><p>هرچه اثیرت کنون درد و عنا می برد</p></div></div>