---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>هیچ، دردی به تو ای مایه درمان مرساد</p></div>
<div class="m2"><p>هیچ، گردی به تو ای چشمه حیوان مرساد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن است اینکه تو ماهی و سمند تو سپهر</p></div>
<div class="m2"><p>به چنین ماه سپهر، آفت دوران مرساد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده ی آن دهنم، از بن دندان که بدو</p></div>
<div class="m2"><p>هیچکس را بجز از من، سر دندان مرساد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا رب آن لب، چو، به عشاق سبکدل برسند</p></div>
<div class="m2"><p>غبن باشد به رقیبان گران جان مرساد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه در عشق، بجان من سر گشته رسید</p></div>
<div class="m2"><p>اثر آن ز دهان بر لب و دندان مرساد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف حسنی و من سوخته ی یعقوبم</p></div>
<div class="m2"><p>من و غم خانه، تورا نکبت زندان مرساد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه بود گرخم زلف تو بپای تو، رسد</p></div>
<div class="m2"><p>بر دل من غم هجر تو بپایان مرساد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه در عالم جان والی بیدادگری</p></div>
<div class="m2"><p>قصهٔ درد اثیر از تو به سلطان مرساد</p></div></div>