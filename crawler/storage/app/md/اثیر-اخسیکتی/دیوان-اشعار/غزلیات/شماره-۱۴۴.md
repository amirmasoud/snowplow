---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>سیم اگر پیش سمن لافی زد از سیمای او</p></div>
<div class="m2"><p>سر و باری گیست تا گوید که من، بالای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر آنست مه، کز آسمان یک شب فتد</p></div>
<div class="m2"><p>با سری در محنت سودای او، در پای او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گیسوی ده پای او، هر تا کزو بار افکنی</p></div>
<div class="m2"><p>هست مأوای دلی در عاشقی یکتای او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خویشتن قربان کنم، کزرای بیند چون بمن</p></div>
<div class="m2"><p>زنده بودن شرط نبود بر خلاف رای او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بدان سر تا قدم دل شو، که با آن طول و عرض</p></div>
<div class="m2"><p>در سویدا می نگنجد محمل سودای او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان بده بر روی او گر، عاشقی پروانه وار</p></div>
<div class="m2"><p>کمتر از شمعی بدان روی جهان آرای او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نرگس مینا قدم کن، گر تماشا بایدت</p></div>
<div class="m2"><p>در سرا بستان شمساد سمن فرسای او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نه بینی کز طرب چون پاکبازی میکند</p></div>
<div class="m2"><p>سنبل خوش سایه بر گلنار نور افزای او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردم دیده است و دانم دیده هرمردمی</p></div>
<div class="m2"><p>برپری میگردد از عکس رخ زیبای او</p></div></div>