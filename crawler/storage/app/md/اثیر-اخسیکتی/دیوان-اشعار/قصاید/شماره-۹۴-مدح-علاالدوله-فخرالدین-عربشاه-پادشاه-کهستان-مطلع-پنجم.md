---
title: >-
    شمارهٔ ۹۴ - مدح علاءالدوله فخرالدین عربشاه پادشاه کهستان - مطلع پنجم
---
# شمارهٔ ۹۴ - مدح علاءالدوله فخرالدین عربشاه پادشاه کهستان - مطلع پنجم

<div class="b" id="bn1"><div class="m1"><p>غنچه دو اسبه رسید با سپه ضیمران</p></div>
<div class="m2"><p>پهلوی گلگونشان کوفته از ضیم ران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه هامون گرفت جوشن ازرق شعار</p></div>
<div class="m2"><p>نیزه ی اغصان نمود بیرق اخضر عیان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخش صبا میدوید، کرم سوی سبزه گاه</p></div>
<div class="m2"><p>پیشگشی ساختش، آب ز، بر گستوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوخته دل لاله را، چهره ی مصقول بین</p></div>
<div class="m2"><p>روی زنان آمد از بیم اجل زان جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وان گل خندان نگر غره بیکروزه عمر</p></div>
<div class="m2"><p>مرگ شبی خونش را، تیغ زده بر فسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خجلت نرگس به بین، دیده زده بر زمین</p></div>
<div class="m2"><p>بر طمع سود زر عمر گرامی زیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باد بروی چمن، غنچه قریر الحدق</p></div>
<div class="m2"><p>زانکه بمدح شه است سوسن، رطب اللسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فخر جهان فخر دین شاه علاءالدول</p></div>
<div class="m2"><p>صاحب نادر قرین سید صاحب قران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میر پیمبر نسب، کُرد غضنفر حسب</p></div>
<div class="m2"><p>صدر ازل پیشکار بد ابد قهرمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مصری تازی سرش طوق ده جام جم</p></div>
<div class="m2"><p>چرغ هلال افکنش حلقه گوش طغان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیسه کلاغ زمان، قصر کُه راغ جای</p></div>
<div class="m2"><p>و زخم این دایره، در کف قدرش کمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لقمه خود چرب کرد از فلک کاسه پشت</p></div>
<div class="m2"><p>ورنه شدی خشک شیر دانه اطفال کان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محرم هم خلوت است، خنجر او با فلک</p></div>
<div class="m2"><p>زین قبلش آفتاب، مهر نهد بر دهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ماهچه ی زلف شام، ساخت ز نعل براق</p></div>
<div class="m2"><p>چون ز شرف نخچ کرد فرق سر فرقدان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آینه در روی داشت، همت او بدر را</p></div>
<div class="m2"><p>تا بشعاع کمال، عکس پذیرفت جان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حشمت او هم از اوست، زانکه به پروازگاه</p></div>
<div class="m2"><p>شهپر خود روی مرغ، به، ز پر پرنیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون خلف هوش او، بکر نزاید خرد</p></div>
<div class="m2"><p>بی صدف گوش او، در نفشاند بیان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از مدد خلق و حلم، ساخت زمان و زمین</p></div>
<div class="m2"><p>لنگر بحر فلک کنگر قصر جنان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آدم بود از ادیم کز افق نسبتش</p></div>
<div class="m2"><p>دید روان بر یمین، موکب نجم الیمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در حرم امن او، آب نپوشد زره</p></div>
<div class="m2"><p>با مدد عدل او، شعله نسازد سنان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای گل بستان آنک، خنده زدی بر دغا</p></div>
<div class="m2"><p>از سر نیلوفریش اشکفه ی ارغوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا نشود صدر دین، پی سپر هر پلید</p></div>
<div class="m2"><p>خانه خدایش نشاند بر طرف آستان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرچه بود سخت کوش، پای قضا در رکاب</p></div>
<div class="m2"><p>حمله ی گیتی تو را باز نتابد عنان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر نه لکام قرار، بر سر عالم کنی</p></div>
<div class="m2"><p>فتنه بدرد فسار چون رمه بی شبان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر سر سرّ ازل پرده درد کلک تو</p></div>
<div class="m2"><p>گاه برمز، خرد گاه بغمز عیان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خامه برون زد سپر عزم تو سوزن نهاد</p></div>
<div class="m2"><p>تا که بپوشد از او ملک لباس امان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رنجه نباید شدن گرچه در او رنج هاست</p></div>
<div class="m2"><p>کز پی احکام خویش تاب خورد ریسمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سخت رکابی نمود تیغ تو ورنه شدی</p></div>
<div class="m2"><p>کوی زمین ریز ریز در خم نه صولجان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از تو چو نیلوفر است گنبد نیلوفری</p></div>
<div class="m2"><p>روی سیه، گوژپشت، قد دو تا سرگران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا بهم آرند سر غیب دل پاک تو</p></div>
<div class="m2"><p>عقل فضولی نهاد، ز میان بر کران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گل چو شود شغبه گوش در سخن عندلیب</p></div>
<div class="m2"><p>سوسن از آن نکته کسیت تا که بود ترجمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>طفل دبستان عقل حاسد و خوش طبع توست</p></div>
<div class="m2"><p>گو برو از روی آب خط معما بخوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بال همای خرد حیف بود چتر آنک</p></div>
<div class="m2"><p>ده یک پر مگس بس بودش سایبان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نشتر طوفان گشای غمزه پیگان توست</p></div>
<div class="m2"><p>پشته گردونش بام چون شخن ناودان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تیغ تو صفرا کند تا که بر آرد بقذف</p></div>
<div class="m2"><p>مایه ی سودای خاک معده ی چرخ کیان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>قدر تو را در ربود پیر ازل طفل وار</p></div>
<div class="m2"><p>در بر و دوش زمان و ز سر و چشم مکان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زانکه محالی بود در دمن نه خراب</p></div>
<div class="m2"><p>غمکده های غراب گشته همای آشیان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>قافیه همتای گنج نیست گراز راه لفظ</p></div>
<div class="m2"><p>بر سر هر دو نشست یک لقب شایگان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شاها در مدح تو گشت سخن های من</p></div>
<div class="m2"><p>درد دم دام و دد، انس دل انس و جان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صیت تو در زین کشید نظم چو آب مرا</p></div>
<div class="m2"><p>کرد جنیبت روش تو سن باد بزان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عقل بصد کنج دُر دارم اندر مزاد</p></div>
<div class="m2"><p>بابت این حضرتم گر بخری رایگان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>طبع مرا وام هاست بر فلک از آب و نان</p></div>
<div class="m2"><p>سست ادائی کند، گر تو نباشی ضمان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون همه نقد دلم سکه بنام تو یافت</p></div>
<div class="m2"><p>چند گدازد تنم در شرر امتحان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کوشش حرص مرا پوشش خود طعمه ساز</p></div>
<div class="m2"><p>زانکه ببازیچه طفل، زود شود شادمان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نیست مسلم مرا، بی کلهت سروری</p></div>
<div class="m2"><p>مرغ گلی کی شود بی دم عیسی پران</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر سر بازار کون سنگ ز اوباش بود</p></div>
<div class="m2"><p>یافت به تشریف مهر خواجگی مهربان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مهر کلاه زر است، صدره سدره سپهر</p></div>
<div class="m2"><p>صبح بدین کام زن شام بدان کامران</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر گذرانم ز عرش مدح تو امروز اگر</p></div>
<div class="m2"><p>با فلک و آفتاب گرم از اینجا روان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ای خم گیسوی تو ناف غزال ازل</p></div>
<div class="m2"><p>باد جهان بر عدوت کام هژبر ژیان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پی سپر جاه تو، هفت زمین عذار</p></div>
<div class="m2"><p>عاشق درگاه تو هشت جنان را جنان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یک خلفش مملکت چون شده صورت پذیر</p></div>
<div class="m2"><p>نطفه شمشیر او در رحم کن فکان</p></div></div>