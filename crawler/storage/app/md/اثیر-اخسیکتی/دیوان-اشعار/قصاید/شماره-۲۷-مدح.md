---
title: >-
    شمارهٔ ۲۷ - مدح
---
# شمارهٔ ۲۷ - مدح

<div class="b" id="bn1"><div class="m1"><p>اقتدارش رایت خورشید بر گردون زده است</p></div>
<div class="m2"><p>بارگاهش خیمه جمشید بر هامون زده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک درگاهش چو عقد گلستان از باد صبح</p></div>
<div class="m2"><p>آتش اندر آبروی لولوی مکنون زده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طرف حکم اوست هر دُر شب افروزی که صنع</p></div>
<div class="m2"><p>تا قیامت بر ستام ابلق گردون زده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زّر احسانش که موزون نیست در معیار وهم</p></div>
<div class="m2"><p>در سرا ضرب ضمیر من زر موزون زده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی کامش هوا بر کارگاه اعتدال</p></div>
<div class="m2"><p>مهره ی بر روی این دیبای سقلاطون زده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که معجون خلاف اوسرشته است آسمان</p></div>
<div class="m2"><p>زهره داروی فنا- حالی بر آن معجون زده است</p></div></div>