---
title: >-
    شمارهٔ ۶۶ - مدح سلطان مظفرالدین قزل ارسلان
---
# شمارهٔ ۶۶ - مدح سلطان مظفرالدین قزل ارسلان

<div class="b" id="bn1"><div class="m1"><p>کجاست راوی اخبار و ناقل آثار</p></div>
<div class="m2"><p>بیا و قصه پیشینکان تمام بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آستان شهان آی و یک بیک برخوان</p></div>
<div class="m2"><p>نشان و نام کیان جوی و در بدربشمار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگو، رکاب که بوده است چرخ انجم دان</p></div>
<div class="m2"><p>بگو سخای که بوده است، ابر گوهر بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که آزمود کمان بر شهاب صاعقه ریز</p></div>
<div class="m2"><p>که رام کرد، بنان بر نهنگ دریا بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مثال تیغ که بود آسمان کوکب سوز</p></div>
<div class="m2"><p>خیال رمح که بود اژدهای کوه ادبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهنشهان به یساری که، خورده اند یمین</p></div>
<div class="m2"><p>سخنوران به یمین که، برده اند یسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمند دهر که را گشت دهر خوش گردون</p></div>
<div class="m2"><p>لگام امر را که را گشت چرخ طاعت دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بروز معرکه اشک که گشت همچو شفق</p></div>
<div class="m2"><p>رخ حسام و کف بیلک که یافت بکار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که بر گرفت به عکس جمال مهر شعاع</p></div>
<div class="m2"><p>ز روی آینه ماه، و صمت ز نگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپهر کوس که را خواند رعد قاف شکاف</p></div>
<div class="m2"><p>زمانه تیر که را گفت برق خاره گذار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر کمال که آمد برون ز چنبر عقل</p></div>
<div class="m2"><p>ره عطای که آمد فزون بکام شمار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکستگان کمند که داد وقت ظفر</p></div>
<div class="m2"><p>ز یک حدیث بزنهار جان بجان زنهار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنوک نیزه که می داد چرخ را بستک</p></div>
<div class="m2"><p>به نعل باره که میکرد کوه را، شد یار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بوقت دوران از ظلمت نجاشی شب</p></div>
<div class="m2"><p>که بر حواشی خورشید میفشاند غبار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بجز سهیل فلک جمله ماه ملک افروز</p></div>
<div class="m2"><p>سماک صاعقه رمح آفتاب تیغ گذار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نبردهای ملک باختر مظفر دین</p></div>
<div class="m2"><p>که زیر گردش خاور ملک ندارد یار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زمین خدیو قزل ارسلان که تربیتش</p></div>
<div class="m2"><p>گذار یافت دو منزل ز گنبد دوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز تیغ تیز سبک پاره کرد مغز عدو</p></div>
<div class="m2"><p>چنانکه کرد گر انبار کردن احرار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بنوک نیزه تنین مثال افعی دم</p></div>
<div class="m2"><p>شمرده مهره ی پشت عدو هزاران بار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دونده باره ی او چیست، کوه صرصرتک</p></div>
<div class="m2"><p>گزنده نیزه او چیست، مار مهره شمار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گهر ز قبه او فوج فوج موج انگیز</p></div>
<div class="m2"><p>چو خیل حور نسیمش گرفته بر سرمار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز گرد معرکه چون نوخطان بماندمشک</p></div>
<div class="m2"><p>سرشته غالیه و برکشیده کرد عذار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپهر صبح قیامی چو راه کاه گشان</p></div>
<div class="m2"><p>کواکبش همه ثابت ولیکن او سیار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرفته شکل زبان تا بدو بیان کرده</p></div>
<div class="m2"><p>هر آنچه یافته شد در رکاب رزم اسرار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدان زبان دل اعدا شکافته لیکن</p></div>
<div class="m2"><p>بود بهین زبانها زبان دل بسیار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهان پناها، شاها، مظفرا، ملکا،</p></div>
<div class="m2"><p>به عزم، باد شتابی، به حزم کوه وقار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بکینه دل بندی، بوعده دشمن بند</p></div>
<div class="m2"><p>به حمله شیر شکاری، بنام شیر شکار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مخالفان تو را بخت خواب دشمن تو</p></div>
<div class="m2"><p>فرو گرفته چو خرگوش خفته را بیدار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدان مقام که خرطوم پشه را در جو</p></div>
<div class="m2"><p>ز تنگای مکان بود دم زدن دشوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ظفر برید تو را با سپهر گفت اینک</p></div>
<div class="m2"><p>خلاصه سفر هفت و اعتکاف چهار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همین حصار که ریزید از ..........</p></div>
<div class="m2"><p>چو مرکزی که تند بر محیط او پر کار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از آن قبل که فرادست اوست طاق نسیم</p></div>
<div class="m2"><p>منزه است نطاق فسیل او ز غبار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز ارتفاع معالیش و هم سر گردان</p></div>
<div class="m2"><p>ز سنگ لاخ حوالیش باد پای افکار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بسان خاتمی آنکوه هست و بازوی او</p></div>
<div class="m2"><p>چو حلقه ای که در آرد نکینه را بکنار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نکار او چو به بینی چنان فرو مانی</p></div>
<div class="m2"><p>که در فتد ز کفت خامه مزاج نکار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ولی گشادن این حصن و صد هزار چنان</p></div>
<div class="m2"><p>مدان بفضل خدا بر خدایگان دشوار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگرچه قلعه روئین دژ است فارغ باش</p></div>
<div class="m2"><p>بدو که خسرو روئین تن است باز گذار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فلک به قلعه قدرای خود چرا نازد</p></div>
<div class="m2"><p>که ماه با تو بود کوتوال قلعه گذار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدان حصار گروهی پناه کرده همه</p></div>
<div class="m2"><p>ز ترس قالب بی قلب چون مترس حصار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز قصه های شراب خلاف خنجر شاه</p></div>
<div class="m2"><p>در آمده بسر آن گروه همچو خمار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بطعنه گفته زبان سنان مینا بر</p></div>
<div class="m2"><p>چو خوش بود گل اگر بر گذر نیفتد خار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز دستیاری تیغ تو سام دستان را</p></div>
<div class="m2"><p>بمانده پای ز جنبش برفته دست ز کار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>حسام سبز قبا در کف عدو گوئی</p></div>
<div class="m2"><p>گرفته بود ز خذلان عهد بد زنکار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نمی برید ز یک درع عیبه را پیوند</p></div>
<div class="m2"><p>نمی رساند بیک موی شخص را آزار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو انتقام الهی بدید آگه گشت</p></div>
<div class="m2"><p>که هست کافر نعمت ز جمله کفار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز دست تیغ تو زنهار خوار شد پس از آنک</p></div>
<div class="m2"><p>به نقض عهد تو زنهار خواه بد،ستار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نهنگ بود عدو کفچلیز گشت ز بیم</p></div>
<div class="m2"><p>چو زین نهادی بر جودی محیط آثار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عزیز کرده لطف تو بود روز نخست</p></div>
<div class="m2"><p>چو قدر عز تو نشناخت چرخ گردش خوار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز نقض عهد چنین خوار گشت خوار شود</p></div>
<div class="m2"><p>هر آنکه عهد عهد ملوک گیرد خوار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>عدو چو نقش در خیمه گشت روز بتر</p></div>
<div class="m2"><p>چو نقش روز بهی بر در تو یافت قرار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر آنکه چهره ی فردای خود بدید ازوی</p></div>
<div class="m2"><p>بسی بتر بود امسال عمر او از پار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بسا که قلزم قهرت خزان خونین را</p></div>
<div class="m2"><p>بدست موج شتر خیز باز داده مهار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بسان آینه زنگ خورده دوران</p></div>
<div class="m2"><p>ز خون خصم بر اندوه هر دوروی آهار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز جوی شریان سیراب بیلک تشنه</p></div>
<div class="m2"><p>ز دیک سینه غذایاب تعلق ناهار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز دست پیشکی روز و شب بجای کمر</p></div>
<div class="m2"><p>میان حریف شده بادو زنکی زنار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شعاع چست پرنده شجاع کرد سیاه</p></div>
<div class="m2"><p>بهم برآمده خورشید روشن شب تار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>غریو کوس بدان حد که نور بخشد چشم</p></div>
<div class="m2"><p>گرفته روح بعزم رحیل پای افزار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>امید را وجل افکنده سنگ در موزه</p></div>
<div class="m2"><p>وقاد را اجل آکنده کیک در شلوار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>در این مقام برآمد ملک ز مطلع قلب</p></div>
<div class="m2"><p>چو مه ز انجم رخشان گزیده اند نگار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز نعل خشم فلک زد بدست و ساعد چاک</p></div>
<div class="m2"><p>هلال وار همی داد صد هزار سوار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بهم گزارش آواز بر کشیده کوه</p></div>
<div class="m2"><p>زباد گرز همی گشت با زمین هموار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بناچخی که همی راند خصم را میدوخت</p></div>
<div class="m2"><p>زه کمان و سر انگشت چست بر سوفار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بخون حاسد او خاک مست گشت چنان</p></div>
<div class="m2"><p>که هم چنین نشود نیز تا ابد هوشیار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>قضا، رکابا، اندازه مخالف تو</p></div>
<div class="m2"><p>که گرد چرخ برانداز کرد زین پیکار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ترازوئی است حسام تو تا ببیند لیک</p></div>
<div class="m2"><p>عیار سفته خود بر یکی در آن معیار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>قضا، کتابه تاریخ او همی بندد</p></div>
<div class="m2"><p>هم از سیاهی شب بر بیاض چشم نهار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مدیر دایره هفت خانه ی خامه توست</p></div>
<div class="m2"><p>تو از پی مداری باز بر ضمیر مدار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>هزار شهر گشادی به تیغ کشور گیر</p></div>
<div class="m2"><p>مراغه نیز ز خیل گرفتگان انکار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خود این پدر چه بود کز نعال مرکب او</p></div>
<div class="m2"><p>چو خاک پست شود طارم بلند مدار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>جهان شکار فراوان ملوک دیدم لیک</p></div>
<div class="m2"><p>کس از ملوک ندیدم چو تو ملوک شکار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نکینه ی که سلاطین شهر بر افسر</p></div>
<div class="m2"><p>کشیده بود چو خر مهره خصم در افسار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>سپید بازی در آشیان پیره زنان</p></div>
<div class="m2"><p>بباد داده بر او مخلب و دُم و منقار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>باصل عالی و مخذول مانده از اعوان</p></div>
<div class="m2"><p>نژاد خوار ملخ گیر گشته از ادبار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>احد گزین چو پیمبر و لیک روز اُحد</p></div>
<div class="m2"><p>وحید مانده ز خیل مهاجر و انصار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گشاد نامه ی امیدوار بازو را</p></div>
<div class="m2"><p>نورد واقعه کوتاه کرد چون طومار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بیاد سعی جمیل تو چون سفینه ز رنگ</p></div>
<div class="m2"><p>در او فتاده بوحشاب قلزم ذخار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>هر آن امید که دارد بروز بسته خویش</p></div>
<div class="m2"><p>توئی بشرع تفضل و را پذیر فتار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>تو راست طبع ز دوران پیر و بخت جوان</p></div>
<div class="m2"><p>دل دلیر و کف راد و لشکر جرار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو مرد ملک طرازی و افسر آرائی است</p></div>
<div class="m2"><p>کسی که کار سپارد بخوله و آکار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>هر آنکه عقل جهانی بدو بداد خدای</p></div>
<div class="m2"><p>جهان بماند اگر بر جهان شود سالار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سزای پوشش هر عفو کسوتی است جدا</p></div>
<div class="m2"><p>سزای فرق کلاه و سزای پای آزار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>اگرچه مرکب عیسی بزرگوار خری است</p></div>
<div class="m2"><p>ز زلف یار ولی کی توان نهاد افسار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز چنگ و ساعد خود شرم باد شاهین را</p></div>
<div class="m2"><p>گهی که ماغ سیه بر پرد بدریا بار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به میهمانی جم وقت پیش خوان کباب</p></div>
<div class="m2"><p>چو بارنامه رسد صفوه را بر آن بیزار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>دو فرقد، اند، شها، بر سپهر ملک که باد</p></div>
<div class="m2"><p>سپهر ملک از این فرقدین برخوردار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو آفتاب و قمر شاه روز و والی شب</p></div>
<div class="m2"><p>ز اختران نطاق شما هزار هزار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ندیده گرد خلافت بساط عز شما</p></div>
<div class="m2"><p>زکام دور درآمد شد خزان و بهار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بدین قصیده غرا بخواست عذر اثیر</p></div>
<div class="m2"><p>جهان بر غم جهانی معاند مکار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>خران معرکه در نوک کلک من بعیان</p></div>
<div class="m2"><p>بدیده اند خیالات نشتر بیطار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>جوال دور صفت تن فراخ و سر کوچک</p></div>
<div class="m2"><p>زمن زمان چو زنوک جوال دور حمار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بقلب اشتر چون بول اشتران مقلوب</p></div>
<div class="m2"><p>باصل استر چون فرج استران بیکار</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>غبار قافله نادیده در مسالک صدق</p></div>
<div class="m2"><p>ولی به سلسله لاف چون جرس بیدار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>حرام زاده چو استر و لیک از سر جاه</p></div>
<div class="m2"><p>ستام و طوق فکنده بر استر رهوار</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ببار عام صدا داده بر در رایت</p></div>
<div class="m2"><p>ولیک بر در خانه نداده کس را بار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>میان تهی چو دهل لیک در مصاف سخن</p></div>
<div class="m2"><p>از او به طنطنه و بانگ بد دلان آوار</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>کشیش وار بر او رنگ بسته فضله ی نقل</p></div>
<div class="m2"><p>بعقد دفتر و جامه بموی دیر و اوار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>کشیش و مفتی از ایشان چو عیسی و احمد</p></div>
<div class="m2"><p>علی الحقیقه بدنیا و آخرت بیزار</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چو عرض گاه از آنست کاخ مفخر من</p></div>
<div class="m2"><p>خیال باطل ایشان مناره اعطار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>مرا خیال بود نظم و نثر و ایشان را</p></div>
<div class="m2"><p>به شصت سال درون آتشی جهد ز چنار</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>عجب تر آنکه بدو نگروید عیسوئی</p></div>
<div class="m2"><p>که تیز خر نشناسد ز بانگ موسیقار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بدانکه آنکه نباشد چو نقش روحانی</p></div>
<div class="m2"><p>وگر چه چابک و رعنا فتد نقوش جدار</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>غرض چمیدن و حمل است گرنه بتراشد</p></div>
<div class="m2"><p>ز کاژ و توژ بیک روزه ده شتر نجار</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ضرر کند گذر سمع از شنودن او</p></div>
<div class="m2"><p>چو روده را اسهال و مثانه را ادرار</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>من آب پاکم و آن نظم ریزه مردار است</p></div>
<div class="m2"><p>جدا بآب توانکرد مرده از کشتار</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>خورد ز دیگ سگی نیم بخت نو خورده</p></div>
<div class="m2"><p>کسی که دست شریعت ندارد از من دار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ز من بعدت یکماهه فرصتی طلبد</p></div>
<div class="m2"><p>که بود شعر دو ممدوح در کشید تبار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>ز ارسلان چو بودره به اختسان نزدیک</p></div>
<div class="m2"><p>ز روی فضل نمیگویم از ره گفتار</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>نمونه کفشی در پای این کهن گشته</p></div>
<div class="m2"><p>بقالبی دگر آرند تا شود بر کار</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>عروس زشت لقا را به شو دهند دو جا</p></div>
<div class="m2"><p>به رنج ناخوشی اش آزمون کنند دو بار</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بعرض سال سیاه دریده بستانند</p></div>
<div class="m2"><p>ز شاه اطلس و دیبا، چه جبه و دستار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ز لال حیوان قسم نشستگان و مرا</p></div>
<div class="m2"><p>نصیب کرد جهان تاختن سکندر وار</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بحق تربیت صدر و آستانه شاه</p></div>
<div class="m2"><p>که کوفت نوبتشان بر در رضا مسمار</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>که یاد روز فراق رکاب شاه مرا</p></div>
<div class="m2"><p>برابری فکند عالمی پر از دینار</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>من از خرابی احوال خود ندارم ننگ</p></div>
<div class="m2"><p>و لیک عار شمارم شماتت اغیار</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>کف بحار بیک قبضه می ننبارد</p></div>
<div class="m2"><p>کنار ابر بهاری به لولوی شهسوار</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بدین قصیده مرا گر غنی کنی چه شود</p></div>
<div class="m2"><p>نه من فزون ز سحابم نه شاه کرم ز بحار</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>از این سخن بدعا باز گردم و گویم</p></div>
<div class="m2"><p>سه بیت دُر ثمین در سیاقت تکرار</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>همیشه تا که کبار زبان دهند بدانک</p></div>
<div class="m2"><p>کند به تربیت ابر آفتاب اقرار</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>حقوق تربیت قبضه و حسام تو را</p></div>
<div class="m2"><p>زبان ملک قلم باد اگر کند انکار</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>گهی بجام بسوگند دختر انگور</p></div>
<div class="m2"><p>گهی بدست تو زلفین لعبت فرخار</p></div></div>