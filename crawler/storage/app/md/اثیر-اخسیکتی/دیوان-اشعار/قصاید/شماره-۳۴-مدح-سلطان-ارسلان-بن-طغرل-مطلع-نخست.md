---
title: >-
    شمارهٔ ۳۴ - مدح سلطان ارسلان بن طغرل - مطلع نخست
---
# شمارهٔ ۳۴ - مدح سلطان ارسلان بن طغرل - مطلع نخست

<div class="b" id="bn1"><div class="m1"><p>ای عید ملک و ملت عیدت خجسته باد</p></div>
<div class="m2"><p>عالم بسعی تیغ تو از فتنه رسته باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چابک رکاب عمر تو تا منزل ابد</p></div>
<div class="m2"><p>بر تیز کام ابلق مدت نشسته باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهباز همت تو چو طعمه طلب کند</p></div>
<div class="m2"><p>از کُردگاه شیر سپهریش مسته باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عشق مجلس تو که طاقت عهودها</p></div>
<div class="m2"><p>ریحان سبزه زار فلک دسته دسته باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مطلب سخن نه بمدحت زند نوا</p></div>
<div class="m2"><p>در زخمه نخستین، رودش، گسسته باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بزمت ارنه حلقه‌به‌گوشی بود چو دف</p></div>
<div class="m2"><p>این چنگ گوژپشت به هم درشکسته باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سر که چون کمان زتو برتافت روی لطف</p></div>
<div class="m2"><p>مغزش بنوک ناوک قهر تو خسته باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر دیده ی که دشمن باغ جمال توست</p></div>
<div class="m2"><p>راه نظر چو دیده ی نرگس به بسته باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کم بودهای عقل. بجاسوسی دلت</p></div>
<div class="m2"><p>در جیب و آستین عدم باز خسته باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا باغ ملک را ز تو نو باوه ها رسد</p></div>
<div class="m2"><p>شاخ قضا ز بیخ رضای تو رسته باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شام ار ز حشمت تو برخ درکشد سپر</p></div>
<div class="m2"><p>شمشیر آفتاب از او باز جسته باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وان ارغنون که چرخ باو رقص میکند</p></div>
<div class="m2"><p>بر دست او بتار مهین، راه بسته باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در عالم حقیقت رخسار توست عید</p></div>
<div class="m2"><p>این عید برسخا و سخن، فر خجسته باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در هر دلی که خصمی تو سر کند چو جوز</p></div>
<div class="m2"><p>رسوا شده ز روزن دیده چو پسته باد</p></div></div>