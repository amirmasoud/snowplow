---
title: >-
    شمارهٔ ۷۹ - مدح کمال الدین زنجانی معروف به تعجیلی، وزیر سلطان رکن الدین طغرل
---
# شمارهٔ ۷۹ - مدح کمال الدین زنجانی معروف به تعجیلی، وزیر سلطان رکن الدین طغرل

<div class="b" id="bn1"><div class="m1"><p>ایا چو ذات خرد جوهرت عدیم مثال</p></div>
<div class="m2"><p>نه نیک رفت که گفتم وجود نیست محال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفتمی که به مانی تو کز ضرورت لفظ</p></div>
<div class="m2"><p>عنان نطق نه پیچاندی بسوی و بال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوقت نسخت ماهیت تو عقل از عجز</p></div>
<div class="m2"><p>درید دفتر و هم و شکست کلک خیال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندید گرد کمال تو گر چه از تعجیل</p></div>
<div class="m2"><p>هزار نعل بیفکند آسمان چو هلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمال تو ننهد پای در تصور عقل</p></div>
<div class="m2"><p>وگر زمانه به پیمایدش دو صد مکیال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر بشرطی، کاندر مقام استغنا</p></div>
<div class="m2"><p>جهان ناقص فارغ شود ز استکمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آن کمال که نسبت درست کرد بتو</p></div>
<div class="m2"><p>دگر بخواب نبیند نشان روی زوال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مطال مدت تو، عقل را بدوزد چشم</p></div>
<div class="m2"><p>مطار همت تو، و هم را بسوزد بال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کند جناب تو را قبله عزیمت خویش</p></div>
<div class="m2"><p>بهر طرف که نهد روی مسرع اقبال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیک نواله شود آز ممتلی معده</p></div>
<div class="m2"><p>اگر نوید حضورش دهی بخوان نوال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو زلف سر نکشد با تو دهر اگرچه بتی است</p></div>
<div class="m2"><p>سفید کاخ چو عارض سیاه دست چو خال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترازوئی است وقار تو را که کفه آن</p></div>
<div class="m2"><p>بدانک سنگ کند نسبت زمین و جبال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سبک سری دو، چه سنجند در چنان میزان</p></div>
<div class="m2"><p>که کوه سنگ نیارد در او بیک مثقال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در آن نمی نگرم من که همت تو، تو را</p></div>
<div class="m2"><p>وزیر مشرق و مغرب کند باستقلال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حکایتی است ز طبع تو، اینکه وصف کنند</p></div>
<div class="m2"><p>زمین گلشن و آب زلال و باد شمال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بآب تربیت تو نمو پذیرفته است</p></div>
<div class="m2"><p>بهر مکان که نشانده است، دست فضل نهال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز امر و نهی تو عالم رصد گهی بنهاد</p></div>
<div class="m2"><p>که بسته ماند و گشاده ره حرام و حلال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قضا نبیره صیت تو چون همی بنواخت</p></div>
<div class="m2"><p>ز پشت شیر فلک، بسکه بر کشید دوال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شکوه کلک تو در راه بود گر نه، هنر</p></div>
<div class="m2"><p>بر آب بستی، رخت صحایف آمال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگاه کرد بدست تو، گفت عقل این است</p></div>
<div class="m2"><p>قبای صورت، پوشیده معنی افضال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>طمع که پیر خرابات طبع بود از تو</p></div>
<div class="m2"><p>بمال مست شد از رطل های مالامال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز تندی ره و نفس تو بر گریوه ی نور</p></div>
<div class="m2"><p>همی برآید پای صبا بسنگ کلال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به بست راه سخن در ثنای تو بر من</p></div>
<div class="m2"><p>که چشم راوی تنگ است و نظم پر، آخال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وگر خموش نشینم گر، سنگان سخن</p></div>
<div class="m2"><p>بدست کدیه بگیرند دامنم در حال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من از وظیفه معنی چه احتباس کنم</p></div>
<div class="m2"><p>که هست در پس هر پرده ئی هزار خیال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کرم چو در دم گرداب حادثات افتاد</p></div>
<div class="m2"><p>بمستغاث درآمد که ای کمال، تعال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زمانه گر چه ندانست کان توئی لیکن</p></div>
<div class="m2"><p>نشان خانه تفصیل داشت ز آن اجمال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رضاش گفت بتعریض کای عفاک الله</p></div>
<div class="m2"><p>عناد پیشه توان کرد در همه احوال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کنون که در گذرد آب این ضعیف از سر</p></div>
<div class="m2"><p>چه فایده ز جواب و چه منفعت ز سئوال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>طریق حضرت در اجل نمیدانی</p></div>
<div class="m2"><p>کزو نیاز غریق است در خزاین مال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مربی فضلای جهان. کمال الدین</p></div>
<div class="m2"><p>که هم کمال جهان است و هم جهان کمال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>علی سپهر معالی که بر بسیط زمین</p></div>
<div class="m2"><p>همی فتد ز رکاب وی آفتاب جلال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خدایگانا حسبی ز لفظ راوی شعر</p></div>
<div class="m2"><p>در آن لباس که لایق بود بقدر مقال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدولت تو که پاینده باد، گفته شده است</p></div>
<div class="m2"><p>بخوانم، ار نبود در میان خوف و ملال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو عقل در گذرد ز اعتبار استعداد</p></div>
<div class="m2"><p>محال بیند ناطق شمردن اطفال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کجا بلیغ شود خطبه شمایل تو</p></div>
<div class="m2"><p>ز کودکی که نداند همی یمین ز شمال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بیان اصل ز اقلیدس معانی خواه</p></div>
<div class="m2"><p>که او بمرتبه ی تخته است از اشکال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>متاع خویشتن ار چند عرضه میگردم</p></div>
<div class="m2"><p>باسم اوست همان رسم اجرة دلال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همیشه تا که پدید است نزد اهل بصر</p></div>
<div class="m2"><p>شکر ز حنظل و لولو ز سنگ و زر ز سفال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نثار کام و کف ناصح و عدوت کناد</p></div>
<div class="m2"><p>همین ششانه به ترتیب، ایزد متعال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنان شده که بر اثبات انعدام نیاز</p></div>
<div class="m2"><p>ز جود دست تو آرد جهان باستدلال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هزار موسم نوروز و جشن پروردین</p></div>
<div class="m2"><p>ز مدت تو ضمان کرده گردش مه و سال</p></div></div>