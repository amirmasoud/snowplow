---
title: >-
    شمارهٔ ۱۰۲ - مدح فخرالدین عربشاه پادشاه کهستان
---
# شمارهٔ ۱۰۲ - مدح فخرالدین عربشاه پادشاه کهستان

<div class="b" id="bn1"><div class="m1"><p>ای روی تو عید عالم جان</p></div>
<div class="m2"><p>خلقی ز تو روزه دار حرمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون ریختن اختیار کرده</p></div>
<div class="m2"><p>بر کیش غم تو، عید قربان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا گشته قلندران راهت</p></div>
<div class="m2"><p>فرمان سپهر را بفرمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از زلف تو عقل، بر عقابین</p></div>
<div class="m2"><p>وز چهره ی تو، بصر بزندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ملک تو عدل، کند چنگل</p></div>
<div class="m2"><p>در دور تو فتنه تیز دندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی طره ی عارضت خرد را</p></div>
<div class="m2"><p>کافر شده عالمی، در ایمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دوش فکنده لام خلقت</p></div>
<div class="m2"><p>از روی تو کفر نو مسلمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عشق رکاب خوبی تو</p></div>
<div class="m2"><p>مه لاغری بلای نقصان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوسیست بصد هزار عالم</p></div>
<div class="m2"><p>ارزان، نه که رایگان ارزان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کوی از همه نیکوان عالم</p></div>
<div class="m2"><p>بربوده تو همچو کوی چوگان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک گل ز عذار تو بسخره</p></div>
<div class="m2"><p>بر گلشن هشت خلد خندان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کوی مستمعی که شد غم من</p></div>
<div class="m2"><p>چون خوبی تو هزار چندان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گه گاه بعقد زلف جان را</p></div>
<div class="m2"><p>بگشای ز تخته بند ارکان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما را نگشاد نیم غمزه</p></div>
<div class="m2"><p>از دستخوش وجود بستان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آسایش خلق را به عیدی</p></div>
<div class="m2"><p>برخیز و رکاب را به جنبان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به نشین بوثاق و عالمی را</p></div>
<div class="m2"><p>بر آتش انتظار بنشان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا مهر پیاده گردد از چرخ</p></div>
<div class="m2"><p>شبدیز بعیدگاه پروان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عید خود و خلق کن خجسته</p></div>
<div class="m2"><p>بر طلعت خسرو قهستان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاهی که بر ارغنون مدحش</p></div>
<div class="m2"><p>در رقص خوش است وقت دوران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در کیسه کون کرده اسبش</p></div>
<div class="m2"><p>سرمایه کیمیای امکان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در چشم جهان غبار خیلش</p></div>
<div class="m2"><p>همزانوی توتیای احسان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اسرار سپهر هفت پوشش</p></div>
<div class="m2"><p>بر عرصه گه سخاش عریان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>میدان مدیح اوست بالله</p></div>
<div class="m2"><p>هرجا که سخن نمود، جولان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>با طبع سخیش عرصه کون</p></div>
<div class="m2"><p>چون چشم بخیل تنگ میدان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای نام فضایل تو بوده</p></div>
<div class="m2"><p>برنامه ی کاینات عنوان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جنت ز کف تو دست بر گوت</p></div>
<div class="m2"><p>دوزخ ز تف تو داغ بر ران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اشخاص تو در ولات توفیق</p></div>
<div class="m2"><p>سرهنگ تو در انات خذلان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رایت همه کون را بیک قرص</p></div>
<div class="m2"><p>کرده است چو آفتاب مهمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خفته است ز موج خیر فامت</p></div>
<div class="m2"><p>زان بر سر گژروی است سرطان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باغ طربت چو شاخ طوبی</p></div>
<div class="m2"><p>آزاد ز برگ ریز اخزان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نزدیک وفاق تو ولی را</p></div>
<div class="m2"><p>تا حشر اساس عمر عمران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وز سیل خلاف تو عدو را</p></div>
<div class="m2"><p>تا گور بنای لهو ویران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر شاخ لطافت تو یک سیب</p></div>
<div class="m2"><p>در جیب نهاده صد سپاهان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وز چرخ کفایت تو یک مهر</p></div>
<div class="m2"><p>در ذیل گرفته صد خراسان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جز کین تو نیست شهره ی عام</p></div>
<div class="m2"><p>زی مصطبه ی هوای شیطان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جز مهر تو نیست حاجب خاص</p></div>
<div class="m2"><p>در بارگه رضای یزدان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آنرا که ستانه ی تو بالین</p></div>
<div class="m2"><p>یک درد به از هزار درمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>وآنجا که عنایت تو مسند</p></div>
<div class="m2"><p>یک مورچه به، زصد سلیمان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در باغ ولات بهر پر چین</p></div>
<div class="m2"><p>می، خار کشد به پشت رضوان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خنده زده بر فلک چو خورشید</p></div>
<div class="m2"><p>قهر تو که نیست مرد میدان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بگریسته بر زمانه چون میغ</p></div>
<div class="m2"><p>کین تو که نیست خرد خفتان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از افسر عصمت تو عاطل</p></div>
<div class="m2"><p>یک شاه نه در سراچه ی جان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وز داغ مروت تو آزاد</p></div>
<div class="m2"><p>یک طفل، نه در مشیمه کان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای فیض کف تو نوش دارو</p></div>
<div class="m2"><p>بیماری فاقه راست، هجران</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بر در گهت از پی تقرب</p></div>
<div class="m2"><p>ثور فلک است گاو قربان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گشته است حمل ز عشق خوانت</p></div>
<div class="m2"><p>بر شعله ی آفتاب بریان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ماشاءالله بماند فکرم</p></div>
<div class="m2"><p>در معرض این حدیث حیران</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عیدی دگر است جز رخ شاه</p></div>
<div class="m2"><p>در آینه یقین و امکان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کوته گردم که بیش از این نیست</p></div>
<div class="m2"><p>میدان مجال و وهم انسان</p></div></div>