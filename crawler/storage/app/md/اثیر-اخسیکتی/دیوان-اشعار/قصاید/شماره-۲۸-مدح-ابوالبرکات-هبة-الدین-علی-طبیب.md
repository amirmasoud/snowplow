---
title: >-
    شمارهٔ ۲۸ - مدح ابوالبرکات هبة الدین علی طبیب
---
# شمارهٔ ۲۸ - مدح ابوالبرکات هبة الدین علی طبیب

<div class="b" id="bn1"><div class="m1"><p>مرا دلی است زصد گه نهاده بر ره حاج</p></div>
<div class="m2"><p>بباجشان شده لکن طمع نداشته باج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر شکسته ز مقلان غنچه بویا</p></div>
<div class="m2"><p>سپر فکنده ز پیکان غنچه غناج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پرده دار صبا داده جان که باز افکن</p></div>
<div class="m2"><p>جلال هودج آن ناقه ضعیف مزاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر بیک نظر این گشته، باز یابد روح</p></div>
<div class="m2"><p>مگر بیک نفس این ناتوان رسد بعلاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اشک خونین او بر نشان پای حبیب</p></div>
<div class="m2"><p>بآفتاب مجرد نهفته روی فجاج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهانه رنگی کز چشم های چرغ کند</p></div>
<div class="m2"><p>کنار من به عقیق آب قلزم مواج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای عرق سلامت محیط دامن من</p></div>
<div class="m2"><p>کشان بزورق زنگار کون سر امواج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طمع بشمع فلک باز بسته تا گشته</p></div>
<div class="m2"><p>بحال سوخته پروانه در فروغ سراج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زو صف عاج بناگوش شاهدان همه سال</p></div>
<div class="m2"><p>شده صحیفه دیوان او سطیحه ساج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخوانده آیت لن تفلحوا اذن ابدا</p></div>
<div class="m2"><p>ز خط دل گسلان بر کنار تخته عاج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نصیحتش نکنم زان کجا برسته او</p></div>
<div class="m2"><p>کس از متاع نصیحت نبرد بوی رواج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز عشق سنبل مفتول نیکوان همه روز</p></div>
<div class="m2"><p>چو گل شکفته از آن بر بنفشه شب داج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وگر کزیر نباشد ز ناصحی آیم</p></div>
<div class="m2"><p>بصدر دفتر القاب افتخار الحاج</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهان خدیو کریمان خجسته بو البرکات</p></div>
<div class="m2"><p>کجا ز برکت و یمنش نطاق بندد پاج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اجل ز درگه او طاق طارم گردان</p></div>
<div class="m2"><p>خجل ز طلعت او، روی کوکب و هاج</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صفای خاطر او منهی مسالک غیب</p></div>
<div class="m2"><p>چنانکه منهی دیوان من صفای ز جاج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو چاکری است فلک در رکاب او تازان</p></div>
<div class="m2"><p>چو سائلی است جهان در جناب او محتاج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کمینه کینه او در دل حسود چنان</p></div>
<div class="m2"><p>که زقه سر شمشیر با خم او داج</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر ز صحن تواضع ببام قدر رود</p></div>
<div class="m2"><p>نه نُه فلک که نودهم نیایدش معراج</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سرای عالم یک سده از معالی اوست</p></div>
<div class="m2"><p>مسطح است فلک در میانه ابراج</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هنر به حضرت او تحفه کی توان بردن</p></div>
<div class="m2"><p>که علم بیدق و فرزین برد بر لجلاج</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گه، فراست او منهی قضا ملحم</p></div>
<div class="m2"><p>گه، کیاست او ابلق زمان هم، لاج</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زهی، سپار ده دوران به نهمت تو عنان</p></div>
<div class="m2"><p>خهی، گذارده کیوان بهمت تو خراج</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زرشک نقش تو در هفت شقه پرده سبز</p></div>
<div class="m2"><p>بکار و مایه فزونند صد هزار ازواج</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سرای ملک چنان شد بکدخدائی تو</p></div>
<div class="m2"><p>که شام و چاشت بدربان همی رود سکباج</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگاهد از عدد دشمنت جهان ارچه</p></div>
<div class="m2"><p>زیادتی دهد انعام را بوجه نتاج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مزین است بنامت صحایف و اقلام</p></div>
<div class="m2"><p>موشح است بذکرت دفاتر و اوراج</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بهم نشانی تو یافت عز سمع و بصر</p></div>
<div class="m2"><p>مزاج نطفه ز دل در بدایت امشاج</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عراق صدرا، امسال سم مرکب تو</p></div>
<div class="m2"><p>از این سواد به بطحا و مکه راند افواج</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بموسمی که عروق زمین ز جوشش خور</p></div>
<div class="m2"><p>همی به پوست برافکند و نژدهای مزاج</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هوای مطبخه میکرد در مسام سحاب</p></div>
<div class="m2"><p>هر آن عرق که همی زاد قطره ی لجاج</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زمین سوخته دل در سراب مار شکنج</p></div>
<div class="m2"><p>چو مهر خرده زر حقه برسیه دیباج</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خدای عز و جل در رکاب فرخ تو</p></div>
<div class="m2"><p>لطیفه های کرامات کرده بود ادراج</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که با قبول تو فردوس شد زمین سراب</p></div>
<div class="m2"><p>که با نزول تو سلسال گشت آب اجاج</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هزار باغ خورنق شگفت در منزل</p></div>
<div class="m2"><p>هزار چشمه حیوان گشوده بر منهاج</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بساط رُفت چو فراش باد مجمره سوز</p></div>
<div class="m2"><p>بسیط ماند چو بستر جبال ابر دواج</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز عکس بوقلمون زمین خلعت پوش</p></div>
<div class="m2"><p>هوای فاخته کون شد چو شهپر دراج</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برفتی و بسزا فرض و نفل حج بگذارد</p></div>
<div class="m2"><p>چنانکه پاک و مبرا بد، از فسون و لجاج</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مساعی تو امان برگرفت از زوار</p></div>
<div class="m2"><p>مآثر تو مناسک فزود بر حجاج</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کنون اوان جدا بودن آمد از تادیب</p></div>
<div class="m2"><p>کنون زمان بر آسودن آمد از ادلاج</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به بختیاری در مرکز شرف به نشین</p></div>
<div class="m2"><p>دل و دو دیده بپای فتن چو عود بساج</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خلاص بارکشان نه ز غصه ایغاف</p></div>
<div class="m2"><p>نجات راهنوردان نه از کف مهراج</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بناز در کنف عز سرمدی چندان</p></div>
<div class="m2"><p>که دورچرخ رساند سماک را به دجاج</p></div></div>