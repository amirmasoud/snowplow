---
title: >-
    شمارهٔ ۹۵ - در نعت رسول اکرم صلی الله علیه وآله وسلم و ثنای مولای متقیان علی ابن ابی طالب علیه السلام
---
# شمارهٔ ۹۵ - در نعت رسول اکرم صلی الله علیه وآله وسلم و ثنای مولای متقیان علی ابن ابی طالب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>ای عقل خنجر تو و ناوردگاه جان</p></div>
<div class="m2"><p>بیرون جهان سمند کمال از پل جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنّین رکی است دهر، مده تاب در کمند</p></div>
<div class="m2"><p>تر دامنی است چرخ، منه تیر در کمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلفی شکن که روی نماید در او یقین</p></div>
<div class="m2"><p>راهی مرو، که باژ ستاند در او کمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان در کف الست کمر بسته ئی چو چرخ</p></div>
<div class="m2"><p>تا پنبه وار باز نشینی بدو کدان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گردن بتان نکنی دست همچو عقد</p></div>
<div class="m2"><p>آوارگی نبرده چو گوهر ز خانمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دولت آستان تو، بر شرفه ی فلک</p></div>
<div class="m2"><p>دام زمین چه میکنی و دانه ی زمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چار سوی عنصر هنگامه ئی است کرم</p></div>
<div class="m2"><p>پرهیز کن ز جیب شکافان بی نشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلوت مخواه تا نزند مرگ بارگاه</p></div>
<div class="m2"><p>اختر مجوی تا نکنی منزل آسمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جاهی طمع مدار بیک آه عادتی</p></div>
<div class="m2"><p>پیلی مکن شکار بیک تار ریسمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر یک سرشک دیده اعمی مبند بحر</p></div>
<div class="m2"><p>وز یک قراضه کف سفله مساز کان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا کی ز تاب کوره بسوزی ببوی گل</p></div>
<div class="m2"><p>تا کی ز آب روی برائی برای نان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دوران محرقه است چه فضل و چه انتساب</p></div>
<div class="m2"><p>طوفان آفت است چه بام و چه نردبان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با حجره نیاز مبر رخت آز از آنک</p></div>
<div class="m2"><p>در یک بدست جای تو گنج گران ممان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر پر دلی ز پوست برون آی دانه وار</p></div>
<div class="m2"><p>تا آخور کمیت طرازی ز کهگشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر اهل ملک سایه میفکن همای شکل</p></div>
<div class="m2"><p>تا با سگان شریک نباشی در استخوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شبدیز، در مصاف طبیعت همی فکن</p></div>
<div class="m2"><p>شهباز، در هوای هویت همی پران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر بر کران روی ز چلیپای لا اله</p></div>
<div class="m2"><p>زنار بر گشایدت الالله از میان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>داری است شکل لا زده بر چار سوی دین</p></div>
<div class="m2"><p>تا هر دو کون خشک شود بر دو شاخ آن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر خلعتی که عشق به مقراض لا بُرد</p></div>
<div class="m2"><p>چست آید آن تمام ببالای عقل و جان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر دو جهان به تابش تو چشم روشن اند</p></div>
<div class="m2"><p>از تن چو شمع پیش کشی کن سوی روان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خواهی کزین خلاب برآئی گلاب وار</p></div>
<div class="m2"><p>یک ره متاب سر از تاب امتحان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صحبت ببر ز نفس بهم جنسی خرد</p></div>
<div class="m2"><p>از سگ خلاص جوی بهم مهری شبان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عنقای نیک عهد توئی قاف قرب را</p></div>
<div class="m2"><p>با هر کلاغ پیسه چه گیری یک آشیان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اندر بر قبول خز از چاک آستین</p></div>
<div class="m2"><p>چون بر در رسول شدی خاک آستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن خاص بار خلوت و سالار خاص و عام</p></div>
<div class="m2"><p>مقصود چرخ و انجم و منعوت انس و جان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جاهش بکاروان ابد داده بدرقه</p></div>
<div class="m2"><p>نورش بدیدگان ازل بوده دیده بان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گنجی چو او نیامده در کنج آب و خاک</p></div>
<div class="m2"><p>لعلی چو او نخاسته از کان کن فکان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن در مبیت فقر وی از مطبخ امیت</p></div>
<div class="m2"><p>وحدت کشیده سفره و عزلت نهاده خوان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وز بهر سر بریدن دهر هوا پرست</p></div>
<div class="m2"><p>ز هر آب داده غیرت او دهره ی هوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عاجز عزیم آب و گل از آب اسپری</p></div>
<div class="m2"><p>تا معجز شفاعت او نا شده ضمان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دست از قلم کشید بنان مبارکش</p></div>
<div class="m2"><p>وانگه میان ماه قلم کرده از بنان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هرگز نداده دیده همت بعلو و سفل</p></div>
<div class="m2"><p>تا خود چه رنگ دارد هم کون و هم مکان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خاک درش بمصر علا برده جبرئیل</p></div>
<div class="m2"><p>زو چرخ پیر گشته زلیخا صفت جوان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هم کاسکی نکرده جهان را که کم بود</p></div>
<div class="m2"><p>بر خوان عنکبوتان جمشید میهمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شق کرده دست فکرت او شقه ی خبر</p></div>
<div class="m2"><p>پس دیده آشکار بر آن چهره ی عیان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آدم مسافر عدم و بانگ نام او</p></div>
<div class="m2"><p>بر کاینات قافله سالار و ساربان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>و آن شب که روی داد بخلوت سرای انس</p></div>
<div class="m2"><p>بر بام چرخ باز نهادند نردبان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ابلق جهاند بر کمر کوهسار علو</p></div>
<div class="m2"><p>جبریل در رکاب و سرافیل در عنان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در غار کرده پاشنه بندی برای راه</p></div>
<div class="m2"><p>از پهلوی ادیم به از کام افعوان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سهمش شکسته در کف ناهید ارغنون</p></div>
<div class="m2"><p>جاهش فکنده بر کتف ماه طیلسان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دوشیزگان خلد از آن عشق در جنون</p></div>
<div class="m2"><p>تا کی زنند موکب میمونش در جنان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از سفره مکان سفری کرده ناشتا</p></div>
<div class="m2"><p>در لامکانش تازه تر افتاده میزبان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جام سلام نوش کنان آن ز سرّ لطف</p></div>
<div class="m2"><p>بی زحمت رقیب لب و ساقی و دهان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یک نصفی خمار شکن خورده بار عشق</p></div>
<div class="m2"><p>چون از شبانه بود دگر روز سر گران</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>صاحب ولایتی که پذیرفت زر دین</p></div>
<div class="m2"><p>از سکه عطیت او نقش جاودان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نا کرده پیش دلبر اسلام دست هین</p></div>
<div class="m2"><p>کاو عبره کرد ملک دل و جان بپای. هان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>صدرش خزینه خانه صدر رسل شده</p></div>
<div class="m2"><p>سلطان صدق گفته زهی نیک قهرمان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مشاطه داده مژده ایمان به مصطفی</p></div>
<div class="m2"><p>ایمان صفت برهنه عروسی برایگان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در خطبه خلافه ز کلک سخنورش</p></div>
<div class="m2"><p>کشته زبان دره فاروق ترجمان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آن هندسی ضمیر که از لوح جاه او</p></div>
<div class="m2"><p>کسری است ملک کسری صغری است خان خان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کرده مجاهدان خرد را مجاهدی</p></div>
<div class="m2"><p>بر نامده ز بادیه وحی کاروان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رانده بر آفتاب دو اسبه سپاه خشم</p></div>
<div class="m2"><p>لیکن چو آفتاب یک اسبه جهان ستان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دست نظر به خشم چو بر قرص نور زد</p></div>
<div class="m2"><p>گر پر نسوختی بشکستیش یک کران</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دید از مدینه صف نهاوند را تهی</p></div>
<div class="m2"><p>یا ساریه الجبل زده حالی بپر دلان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نقش ولای او چو رقم گشت بر دلت</p></div>
<div class="m2"><p>از دفتر فضیلت حیدر خطی بخوان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جان در بهای مهر سگ کوی او بده</p></div>
<div class="m2"><p>تا عقل گویدت که زهی بیع بی زبان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مشاطه کلام قدم را به هفت دست</p></div>
<div class="m2"><p>از پیش جلوه داده و پس کرده جانفشان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بی هیچ تهمتی به شبستان مصحفش</p></div>
<div class="m2"><p>چون کلک سر بریده بشمیر سیل ران</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>لعلی ز حقه دل و جان وقت بازگشت</p></div>
<div class="m2"><p>پیش کلام مجد کشیده به نورهان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مرغان آب و دانه ز تسبیح مرتضی</p></div>
<div class="m2"><p>در سایبان شهپر عصمت شده نهان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>با زخم ذوالفقار در آغوش کرده است</p></div>
<div class="m2"><p>اندر بنان او عمل خامه توان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اخسیکتی ز دامن حیدر مدار دست</p></div>
<div class="m2"><p>جانی است دست و پای تو در پای اوفشان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>دیوان مدح اوست حمایت سرای من</p></div>
<div class="m2"><p>بستان مهر اوست تماشا گه امان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>رمحش پیاده خواه بیک حمله روستم</p></div>
<div class="m2"><p>تیغش نواله خوار بیک چاشت هفتخوان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در دست او شکوفه باغ ظفر شده</p></div>
<div class="m2"><p>نیلوفری که رنگ پذیرد ز ارغوان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>مستسقی حسام وراتفته شد جگر</p></div>
<div class="m2"><p>تا شربت آرد از رک شرک آب ناردان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ختم است بر ثنای علی مقطع سخن</p></div>
<div class="m2"><p>کز بعد ارغنون نرسد پشه را فغان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ای علم لایزال تو. همخانه ی وجود</p></div>
<div class="m2"><p>احسانت کرده بام و در طبع پاسبان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>در صفت انتقام تو موسی است رزم زن</p></div>
<div class="m2"><p>و اندرو بای قهر تو عیسی است ناتوان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ارجو که بر ستانه حفظ تو کم رسد</p></div>
<div class="m2"><p>دستان چرخ کهنه در این تازه داستان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ای عقل نازنین چو توئی مقتدای نفس</p></div>
<div class="m2"><p>تا کی سرای طغرل و تا کی در طغان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خلقان حرص و آز بکش از سر اثیر</p></div>
<div class="m2"><p>وز ننک مدح گفتن خلقانش وارهان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مرغ سحر گهی است صفیر سلام او</p></div>
<div class="m2"><p>او را به آشیانه شروانیان رسان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تا در خوی خجالب جیحون کنند خاک</p></div>
<div class="m2"><p>خاقانی ثناگر و خاقان شعر خوان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>باری فراخ سال سخن بیند آنکه گفت:</p></div>
<div class="m2"><p>«قحط وفاست در بنه ی آخرالزمان»</p></div></div>