---
title: >-
    شمارهٔ ۱۰ - مدح خواجه فخرالدین
---
# شمارهٔ ۱۰ - مدح خواجه فخرالدین

<div class="b" id="bn1"><div class="m1"><p>در تتق ابر شد، باز رخ آفتاب</p></div>
<div class="m2"><p>همچو بناگوش یار در خم زلف بتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر سیه پیرهن ابر سپید پریش</p></div>
<div class="m2"><p>هندوی کافور موی ترک معنبر نقاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانقه صوفیان بر گه ز بس اقحوان</p></div>
<div class="m2"><p>یک رده احمر لباس یک صفه ازرق ثیاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساغر یاقوت رنگ، لاله چو بر خاک زد</p></div>
<div class="m2"><p>نرگس مخمور چشم زود در آمد ز خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سر هر آبگیر، صفحه سیمین نمود</p></div>
<div class="m2"><p>شاخ به تذهیب کرد یک ورق زرناب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در طرب آبادباغ، گشت ز غوغای دی</p></div>
<div class="m2"><p>منظر شمشاد پست، طارم گلبن خراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبزه کم عمر را گشت محاسن سفید</p></div>
<div class="m2"><p>هم ز رحیل صبا هم ز نزول ضیاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرگس فیروزه تخت تاجی بر سر نهاد</p></div>
<div class="m2"><p>قبه ز زر طلا نیزه ز سیم مذاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرده ز پر غراب جامه سیه شاخ را</p></div>
<div class="m2"><p>محنت فصل هرم حسرت عهد شباب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون زچکا، ارغنون گشت شنیدن محال</p></div>
<div class="m2"><p>باده چون ارغوان هست کشیدن صواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمزمه گوی از برش بلبل چون مطربان</p></div>
<div class="m2"><p>رقص کنان بر سرش همچو شکرفان حباب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جان شیاطین غم، سوخته گردد چو او</p></div>
<div class="m2"><p>از افق جام گرد، تاختنی چون شهاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر در لطفش زده، روح بدر یوزه چشم</p></div>
<div class="m2"><p>روح که طالب نصب، راح که صاحب نصاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون لب جام از صفاش مطلع خورشید شد</p></div>
<div class="m2"><p>نصفی مه زار و زرد، در دهن و در رضاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون مه ناکاسته، مجلسی آراسته</p></div>
<div class="m2"><p>بر رخ صدر اجل، خواجه جام شراب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از کف ترک چو ماه باده ده باده خواه</p></div>
<div class="m2"><p>چشمه لب بی گناه گوشه خور بی سحاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آتش رخساره ی کز پی دیدار او</p></div>
<div class="m2"><p>چشم فلک شد سپید، جان ملک شد کباب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>منتظر وصل او دیده ی خوارزمشاه</p></div>
<div class="m2"><p>مفتخر از اصل او، دوده افراسیاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جان بستاند ز دل جزع وی اندر جفا</p></div>
<div class="m2"><p>دل برباید ز جان، لعل وی اندر عتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون سر کلک وزیر، طره ی او بر عذار</p></div>
<div class="m2"><p>پشت حواصل نگار کرده به پرغراب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سرور نیکو سیر خواجه والا گهر</p></div>
<div class="m2"><p>مهتر عالی ثمر صاحب فرخ جناب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گوهر درج لطف اختر برج شرف</p></div>
<div class="m2"><p>بازوی اقبال تیغ خامه دولت کتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فخر نظام ملل فرو بهای دول</p></div>
<div class="m2"><p>آن زکفش بی خلل ملک سخا، زاضطراب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ابر کفش چون بدید خشک نهال امید</p></div>
<div class="m2"><p>بر سر بام جهان زد علم فتح باب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از همه ابنای دهر همت او جمع کرد</p></div>
<div class="m2"><p>هم شرف انتساب هم گهر اکتساب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صیقل رایش چو برد، دست بروشنگری</p></div>
<div class="m2"><p>دست قضا برکشید خنجر ملک از قراب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نور وفاقش دهد عارص مه را فروغ</p></div>
<div class="m2"><p>رنک خلافش کند طره شب را خضاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مسرع عزمش چو کرد مرکب تعجیل کرم</p></div>
<div class="m2"><p>شق نکند گرد او باد بپای شتاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دشمن خود را بر او، گرچه تشبه کند</p></div>
<div class="m2"><p>نیک شناسد خرد بحر محیط از سراب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای در، میدان ملک حزم تو آبی زده</p></div>
<div class="m2"><p>کاسب قضا را بر او، مانده خراندر خلاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عرصه جاه تو را طی نکند نور و ظل</p></div>
<div class="m2"><p>مسرع عزم تو را پی نبرد با دو آب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>طینت خاک است و آب ذات شریف تو لیک</p></div>
<div class="m2"><p>خاک نسیم، الحراک باد اثیر التهاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کین تو در کار دین گر نزند دارعدل</p></div>
<div class="m2"><p>در نفس از شب روی، توبه کند ماهتاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کام خطا کی نهد ذهن تو در هیچ کوی</p></div>
<div class="m2"><p>راه غلط گم رود فکر تو در هیچ باب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سر نکشد چرخ چون جاه عمر هیبت</p></div>
<div class="m2"><p>ذره تادیب برد بر کتف احتساب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو گل مل طینتی وز پی قمع عدوت</p></div>
<div class="m2"><p>گل نبود بی دروغ، مل نبود بی خراب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هم گه دیوان توئی، مرد دوات و قلم</p></div>
<div class="m2"><p>هم گه میدان توئی، گرد طعان ضراب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سایر کلک تو را عقل نداند میسر</p></div>
<div class="m2"><p>سایل تیغ تو را، چرخ نداند جواب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مدح تو جمع آورد عاجل و آجل به هم</p></div>
<div class="m2"><p>عاجل دنیا عطا آجل عقبی ثواب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سلک عبارت گسست، جوهر اوصاف تو</p></div>
<div class="m2"><p>قطره که داند شمارد، ذره که گیرد حساب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عرصه مدح تو کی پای فلک کرد طی</p></div>
<div class="m2"><p>چون به فلک در زند، دست تصرف تراب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چند تواند شنید عقل بسمع قبول</p></div>
<div class="m2"><p>مدحت گردون علو، سیرت خورشید تاب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ای خرد هرزه کار لاشه دعوی بدار</p></div>
<div class="m2"><p>ابرش افلاک نیست اهل عنان در رکاب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای ز دل پاک تو عقل سری پر نهیب</p></div>
<div class="m2"><p>وی ز کف راد تو کنج دلی پر نهاب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>راه ز اندیشه بیش مرحله عجز پبش</p></div>
<div class="m2"><p>سست بپا کرده هین پا و سرش انقلاب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تیر عقاب افسرت غرق شود تا به پر</p></div>
<div class="m2"><p>گرچه نشان باشدش چشمه بال عقاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نیست مرا در جهان از ستم آسمان</p></div>
<div class="m2"><p>جز به حریمت امان جز به جنابت مآب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گشت امیدم که رست از بد و نیک آن توست</p></div>
<div class="m2"><p>ابر عطائی ببار مهر سخائی بتاب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا چو عروسان باغ چهره گشایند باز</p></div>
<div class="m2"><p>ابر بهاری زند بر رخ هر یک گلاب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در چمن باغ عمر باد لب و طبع تو</p></div>
<div class="m2"><p>کوری حساد را باده کش و لهو یاب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هرکه نباشد چو چنگ با تو بیک پرده در</p></div>
<div class="m2"><p>خورده بسی گوشمال از تو بسان رباب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کرده مقالات من با شرف مدح تو</p></div>
<div class="m2"><p>در دل ناصح سرور بر تن فاضح عذاب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شعر سراید بسی هر کسی اندر بسی</p></div>
<div class="m2"><p>لیک ز بهر آبه سود به زهریر گلاب</p></div></div>