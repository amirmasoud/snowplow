---
title: >-
    شمارهٔ ۳۲ - مدح یکی از صدور
---
# شمارهٔ ۳۲ - مدح یکی از صدور

<div class="b" id="bn1"><div class="m1"><p>ای کلک تو بر لوح عطارد زده ابجد</p></div>
<div class="m2"><p>عنوان نسب نامه آدم باب وجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم کاهل هامونی با حلم تو مسرع</p></div>
<div class="m2"><p>هم شبرو گردونی با عزم تو معقد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر مفرش صدر تو پی عزت جاوید</p></div>
<div class="m2"><p>در سایه قدر تو سر دولت سرمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز رای تو در تیه معانی نبرد راه</p></div>
<div class="m2"><p>جز حزم تو بر راه حوادث نکشد سد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در موکب اقبال علمدار جلالت</p></div>
<div class="m2"><p>بر چتر سپهری زده یک گوشه مطرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مسند همت بنشین زانکه ضیاها است</p></div>
<div class="m2"><p>از خاک کف پای تو تا دیده فرقد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دشمن چه شنیده است و چه دیده است زتوباس</p></div>
<div class="m2"><p>تا بر غر تزویر زند بانک مؤید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا شست قضا در کشد این تیر جگر دوز</p></div>
<div class="m2"><p>تا دست قدر برکشد این تیغ مغمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم خوابه کین تو هم از بارقه خشم</p></div>
<div class="m2"><p>بر خرده الماس کند عرصه مرقد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در مجلس تادیب تو چون سوسن و نرگس</p></div>
<div class="m2"><p>از بیم زبان لال وز غم دیده مشهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زرین قلم چرخ شود نکته بینش</p></div>
<div class="m2"><p>زان لفظ گهر بار بر این لوح زبر جد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه پایه افلاک مرصع ز پی توست</p></div>
<div class="m2"><p>بر منبر چوبین چه نهی بیهده مسند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر، دیده کان طلعت زیبای تو بیند</p></div>
<div class="m2"><p>پیش رخ خورشید به بندد تتق رد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر سلسله خط تو بگذشت خرد گفت</p></div>
<div class="m2"><p>صد پای معانی است بهر حلقه مقید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>احسنت زهی ذات تو در مبدأ ترکیب</p></div>
<div class="m2"><p>از شرکت طبع آمده چون عقل مجرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خاک در میمون تو، اکسیر سعادت</p></div>
<div class="m2"><p>وز وی شده عز ابدی عز مخلد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو کعبه فضلی و من از دور تو محروم</p></div>
<div class="m2"><p>لبیک زنان روی نهاده سوی مقصد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن باز سپیدم که بیک صولت پرواز</p></div>
<div class="m2"><p>بر شیر سیه تنک کنم عرصه مصید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شب طره مشگین نفشاند به تبرک</p></div>
<div class="m2"><p>گر مدخنه طبع تو تنک آمده بد قد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا پای بشویند عروسان نکاتم</p></div>
<div class="m2"><p>در شیشه ی مه گرده گلابیست مصعد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یک رمز مرا کاتب علوی بنویسد</p></div>
<div class="m2"><p>چون کار بشرح اند، در این هفت مجلد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیش تو میان بستم چون رمح ز دینی</p></div>
<div class="m2"><p>گوهر ز زبان رسته چون تیغ مهند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در چشم عدو خارم و بر خد ولی خال</p></div>
<div class="m2"><p>پالایش این چشمم و آرایش آن خد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خاری که ز زخمش شود آن دیده معذب</p></div>
<div class="m2"><p>خالی که ز لطفش شود این چهره مورد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر رغم جهانی چه شود، گر چو منی را</p></div>
<div class="m2"><p>اسباب مرتب کنی احوال ممهد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نیکو نبود گر پس از ایمان مدیحت</p></div>
<div class="m2"><p>طبعم به ثنای دگری گردد مرتد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زان پس که خضر وار سپردم ره دریا</p></div>
<div class="m2"><p>سجاده سبز آرم بر صرح ممرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا درع سیه عیبه مه را گند از نور</p></div>
<div class="m2"><p>زرادی خورشید بزر آب مزّرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از سم براق تو هلالی که بیفتد</p></div>
<div class="m2"><p>بادا شده زو گردن خورشید مقلد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هم نام تو بر دیده اقبال منقش</p></div>
<div class="m2"><p>هم عهد تو، با مدت ایام مؤکد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر دوش من از بخشش تو دیبه معلم</p></div>
<div class="m2"><p>در کوش تو از مدحت من در معقد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عرض تو چو علم تو ز آفات منزه</p></div>
<div class="m2"><p>رسم تو چو اسم تو در آفاق محمد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دین ساخت عمادی ز تو ایوان شرف را</p></div>
<div class="m2"><p>بادا، بتو این ایوان تا حشر معمد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در تهنیت روزه چگویم که جهان را</p></div>
<div class="m2"><p>هر روز بدیدار تو عیدی است مجدد</p></div></div>