---
title: >-
    شمارهٔ ۵۱ - مدح
---
# شمارهٔ ۵۱ - مدح

<div class="b" id="bn1"><div class="m1"><p>ای عهد تو چون عهد قضا سرمد</p></div>
<div class="m2"><p>وی عمر تو چون عمر ابد ممتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرمد از چرخ توئی ز آنروی</p></div>
<div class="m2"><p>ارزانی به ملکت سرمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رای تو چون قضای خدا الحق</p></div>
<div class="m2"><p>بیداد راضدی است در این مرصد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک از تو چون بدرزید ازچرخ</p></div>
<div class="m2"><p>آسوده را قدی است در این مرقد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سفره ی سخای تو قرص خور</p></div>
<div class="m2"><p>قرصی است کز محیط کند مبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجموع مفردات وجودی تو</p></div>
<div class="m2"><p>ای عالمی ز فضل و کرم موجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سابق است بر تو جهان شاید</p></div>
<div class="m2"><p>پیش از مرکبات بود مفرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور لاحقی بعهد چه عیب آرد</p></div>
<div class="m2"><p>بعد از مرکبات بود ابجد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دریای بند حزم تو گردون را</p></div>
<div class="m2"><p>بیچاره ی شمر چو زمین معقد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اخبار زود باور عقل آید</p></div>
<div class="m2"><p>گر با کفایت تو شود مسند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی نام تو مدان که عطارد را</p></div>
<div class="m2"><p>مقدار کلک رقم کند یامد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با مشتری سمند تو انباز است</p></div>
<div class="m2"><p>کاین نعل در میان نهد و آن خد</p></div></div>