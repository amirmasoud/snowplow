---
title: >-
    شمارهٔ ۹۷ - مدح اقضی القضات خواجه رکن الدین حافظ همدانی
---
# شمارهٔ ۹۷ - مدح اقضی القضات خواجه رکن الدین حافظ همدانی

<div class="b" id="bn1"><div class="m1"><p>ای عشق تو داده بر جهان فرمان</p></div>
<div class="m2"><p>درد تو گوارنده تر از درمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانه ی خرمن غمت گردون</p></div>
<div class="m2"><p>پروانه ی شمع عارضت دوران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سایه زلف و نور رخسارت</p></div>
<div class="m2"><p>شد عالم نور و سایه آبادان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان را هوس نظاره ی رویت</p></div>
<div class="m2"><p>بر غرفه ی چشم تا زد از زندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز بهر سپند عارضت گل را</p></div>
<div class="m2"><p>در کوره ی مالک افکند رضوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زی مجلس تو چو تحفه ئی آرم</p></div>
<div class="m2"><p>دل میگوید که بر طبق نه جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر طلعت تو چو عیدی آغازم</p></div>
<div class="m2"><p>جان میگوید که دیده کن قربان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر خوان هلاک باشد افطارش</p></div>
<div class="m2"><p>هر، کز تو گرفته روزه حرمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوی ذقنت مرا چنین کرده است</p></div>
<div class="m2"><p>دل، داغ و خمیده چون سر چوگان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قدّم چو هلال در فراق توست</p></div>
<div class="m2"><p>بر ماه صیام چون نهم بهتان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر چهره ی من نوشته کلک غم</p></div>
<div class="m2"><p>خطی بوجوه زعفران آسان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لوزینه خیال لعل نوشینت</p></div>
<div class="m2"><p>بروی شده چشم من گلاب افشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ور کرد دلم مثال خط تو</p></div>
<div class="m2"><p>چون تره ی خورد برگ بر بریان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من تن زده و خیال منهی را</p></div>
<div class="m2"><p>جاسوس نظر بهر طرف پویان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کان برگ و نوا بدید گفت الحق</p></div>
<div class="m2"><p>نزدیک تو باید آمدن مهمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دندان امید بر کنم از تو</p></div>
<div class="m2"><p>فردا چو لب افق شود خندان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شب دامن و خوان صاحب فاضل</p></div>
<div class="m2"><p>فهرست کمال گوهر انسان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رکن الدین، رکن کعبه ملت</p></div>
<div class="m2"><p>حسنیه بهار گلشن احسان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دیباچه ی تالیف سعادت را</p></div>
<div class="m2"><p>همزانوی جسم اسم پاکش دان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر مونس روح خوانمش، تقوی</p></div>
<div class="m2"><p>ور شمع ضمیر خوانمش، ایمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رایش مهر است و آسمان ذره</p></div>
<div class="m2"><p>دستش ابر است و مکرمت باران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در مسکین او کمال را مسکن</p></div>
<div class="m2"><p>بر ساحت او امید را جولان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مقبول نگشت نامه روزی</p></div>
<div class="m2"><p>تا نام کفت نداشت بر عنوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در یوزه گزید بر در جودش</p></div>
<div class="m2"><p>گنجینه کان و کیسه ارکان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وقفند بر آستانه قهرش</p></div>
<div class="m2"><p>طاق بهرام و طارم کیوان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای رخش ظفر تاخته از گردون</p></div>
<div class="m2"><p>وی کوی کمال برده از اقران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خورشید چو خیل تاش رای توست</p></div>
<div class="m2"><p>بر مردم دیده میدهد فرمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هرکس که شود حواری عیسی</p></div>
<div class="m2"><p>گردونش چو تره ها نهد بر خوان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دستوری داد هر دو عالم را</p></div>
<div class="m2"><p>جاه تو که فارغ آمد از اعوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تنها رو گشت خسرو انجم</p></div>
<div class="m2"><p>چون فایده ئی ندید از اخوان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دیوان عمل بتو شرف یابد</p></div>
<div class="m2"><p>نه تو بوجود عامل و دیوان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رای توبه اختران دهد پرتو</p></div>
<div class="m2"><p>قهر تو بر آسمان نهد پالان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دهر از سخط تو پشت پائی خورد</p></div>
<div class="m2"><p>بنهاد ز دست حیلت و دستان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کلک تو بهار گلشن دولت</p></div>
<div class="m2"><p>خط تو زهاب چشمه ی حیوان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا صبح هدایت تو هر خاطر</p></div>
<div class="m2"><p>کاذب چو زبان ذنب السرحان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ورد دم صور قهر تو نبود</p></div>
<div class="m2"><p>جز آیت کل من علیها فان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ای جای گرفته در دل عصمت</p></div>
<div class="m2"><p>وی پای نهاده بر سر اقران</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در صلب سپهر منعقد نطفه</p></div>
<div class="m2"><p>از لقمه حکمت صد لقمان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پیران عقول تخته برگیرند</p></div>
<div class="m2"><p>گر ذهن تو نو کند دبیرستان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در حیطه آسمان توئی مرکز</p></div>
<div class="m2"><p>در دیده اختران توئی انسان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در دیده همت امل بخشت</p></div>
<div class="m2"><p>نا یافته کاینات هیچ امکان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای آنکه در اعتقاد با مهرت</p></div>
<div class="m2"><p>گشته است روانم احدالصنوان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زان پس که هوای خاک درگاهت</p></div>
<div class="m2"><p>بستاند مرا ز حضرت سلطان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نام تو نگاشت نظم من بر دل</p></div>
<div class="m2"><p>داغ تو نهاد، شعر من بر ران</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اینجا بتو پای بسته ام، ورنه</p></div>
<div class="m2"><p>من کیستم و اقامت زنگان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ایام بهر چه میکند با من</p></div>
<div class="m2"><p>برخواهم داشت رهنی از سامان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مفقود چهار ساله عمرم را</p></div>
<div class="m2"><p>آخر به تفقدی بده تا وان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نا راستی سر و تنم می بین</p></div>
<div class="m2"><p>کفارت آن گذشته ها میدان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا طبع بود مکیف اعضا</p></div>
<div class="m2"><p>تا نفس بود مدبر ابدان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بادات، نهایت امل حاصل</p></div>
<div class="m2"><p>بادات، ولایت بدن عمران</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از مجلس تو بشکر بر گشته</p></div>
<div class="m2"><p>ماه رمضان چون رجب و شعبان</p></div></div>