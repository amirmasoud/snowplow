---
title: >-
    شمارهٔ ۴۴ - مدح اتابک علاءالدین محمد خداوند مراغه
---
# شمارهٔ ۴۴ - مدح اتابک علاءالدین محمد خداوند مراغه

<div class="b" id="bn1"><div class="m1"><p>هرکه بر منهاج عزمی رای مقصد می‌کند</p></div>
<div class="m2"><p>عزم درگاه علاءالدین محمد می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه در هیجا به مار مقرعه با خصم ملک</p></div>
<div class="m2"><p>کار رمح خطی و تیغ ممهد می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نام میمونش که بر چهر قمر منقوش باد</p></div>
<div class="m2"><p>ملک را فرمان‌پذیر شرع احمد می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در نسب قیصرنژاد آمد سکندروار از آن</p></div>
<div class="m2"><p>بر ره یأجوج فتنه خنجرش سد می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از مکارم بال‌های وعده بیرون می‌پرد</p></div>
<div class="m2"><p>در ممالک رخنه‌های فتنه منسّد می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حریم دست او کلک خط‌آور سال و ماه</p></div>
<div class="m2"><p>عشقبازی‌ها که با شمشیر امرد می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لطف طبعش در بیان انموذج جان می‌نهد</p></div>
<div class="m2"><p>حذراتش در ظفر خاصیت حد می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهمت بی‌مثلی او هر نفس در کوی و هم</p></div>
<div class="m2"><p>عقل مؤمن را در او صد بار مرتد می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک با اعصار کام تو به بازار رواج</p></div>
<div class="m2"><p>ای بسا، طین را که بر ناموس عسجد می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باغبان فتح چون مشاطگان از خون خصم</p></div>
<div class="m2"><p>چهره نیلوفر تیغش مورد می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماه اگر حمل سلاحش را نمی‌بندد نطاق</p></div>
<div class="m2"><p>خور چرا داغ سیه فامش مزرد می‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با نسیم خلق او در باغ صد صاحب قبول</p></div>
<div class="m2"><p>صحبدم گلشن ره آورد صبا، رد می‌کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ابر، در گرداب خوش از غصه قهرش نشست</p></div>
<div class="m2"><p>کآتش سرکش بر او، بیداد بی‌حد می‌کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در بیان آن چیرگی دارد، که چون کلک حکیم</p></div>
<div class="m2"><p>صورت معقول محسوس مشاهد می‌کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از نصاب لفظ تو هر شب فلک یابد زکات</p></div>
<div class="m2"><p>زان به مروارید ترصیع زبرجد می‌کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جوهر قابل چو از اقبال او تشریف یافت</p></div>
<div class="m2"><p>جلوه هردم در زبر پوش مجدد می‌کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون به تیغ او رسد، بکر ظفر، بلقیس‌وار</p></div>
<div class="m2"><p>کشف ساق از ساحل صرح ممرد می‌کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر در او روح رستم می‌پزد سودا، از آن</p></div>
<div class="m2"><p>تا سپر داری سرهنگان مفرد می‌کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز آب تیغ او حشر کرده است باد سست‌کوش</p></div>
<div class="m2"><p>در دغاز آن شیر رایت را مؤبد می‌کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای ز فر و قدر جایی، کآسمانت پایگاه</p></div>
<div class="m2"><p>با هزاران شرمساری، فرق فرقد می‌کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عدل تو، چون سر و پیرای طبیعت سال و ماه</p></div>
<div class="m2"><p>خفته کان را می طراز تا سهی‌قد می‌کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زو به عهد چون تویی ابر مؤبد لاف جود</p></div>
<div class="m2"><p>برق شمشیر تعصب، زان مجرد می‌کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باد هم، در عزم سدّ پای بند خصم توست</p></div>
<div class="m2"><p>کز حباب آب صد زنجیر مورّد می‌کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در بهاران خلق و خلقت عرض لشکر می‌دهد</p></div>
<div class="m2"><p>راد سرو، آنجا به قامت کار مطرد می‌کند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا بمالد در قدمگاه تو اعنی آسمان</p></div>
<div class="m2"><p>ماه نو قد، خم به خم سر تا قدم خد می‌کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گنبد پر دیده را، عدلت به میل صبحدم</p></div>
<div class="m2"><p>توتیای خواب در جفن مشدد می‌کند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عهد میمون تو، عقلا، دور دور است از فنا</p></div>
<div class="m2"><p>زان که عدلت با بقا عهدی مؤکد می‌کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاد باش ای آنکه اقبالت نطاق ماه را</p></div>
<div class="m2"><p>همدم تارک میان ماه اعبد می‌کند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جفن انصاف تو تیغ فتنه بیداد را</p></div>
<div class="m2"><p>چون حدق را، جفن خواب‌آلود معهد می‌کند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باد عیسی در دمم، بین، آب حیوان در قلم</p></div>
<div class="m2"><p>این همی‌بخشد حیات و آن مخلد می‌کند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کللک صورت ساز من انباز نفس ناطقه است</p></div>
<div class="m2"><p>آنچنان کز یک سخن پنجه مجلد می‌کند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خصم افعی سار داند کاین گهر در سلک نظم</p></div>
<div class="m2"><p>گرچه یاقوت است تأثیر زمرد می‌کند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ذکر باقی را، حکیمان عمر سرمد خوانده‌اند</p></div>
<div class="m2"><p>وین سخن عمری است که ذکر تو سرمد می‌کند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا به استثنای الاله رود از لا اله</p></div>
<div class="m2"><p>هر زمان کاو، افتتاح لفظ اشهد می‌کند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دایم آن خواهم، که هر شب زنگی اعلای تو</p></div>
<div class="m2"><p>تیغ تو لختی، فراز خواب مشهد می‌کند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گو همه خورشید جای مرقد عز تو باد</p></div>
<div class="m2"><p>تا گل صاحب‌جمال از غنچه مرقد می‌کند</p></div></div>