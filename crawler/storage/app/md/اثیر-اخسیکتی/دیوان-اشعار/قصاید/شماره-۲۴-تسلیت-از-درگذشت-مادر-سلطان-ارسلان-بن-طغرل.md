---
title: >-
    شمارهٔ ۲۴ - تسلیت از درگذشت مادر سلطان ارسلان بن طغرل
---
# شمارهٔ ۲۴ - تسلیت از درگذشت مادر سلطان ارسلان بن طغرل

<div class="b" id="bn1"><div class="m1"><p>در گلشن ایام نسیمی ز وفا نیست</p></div>
<div class="m2"><p>در دیده افلاک نشانی ز حیا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خوانچه مینای فلک خود همه قرص است</p></div>
<div class="m2"><p>و آن هم زپی گرسنه چشمان چوما نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پنهای فلک حبر ندارد که به تحقیق</p></div>
<div class="m2"><p>بر مائده او به جز این ترش ابا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر لحظه جوانی بگشد عالم اگر چند</p></div>
<div class="m2"><p>جز بر سر پیران اثر گرد دعا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنجا که امل دام نهد باز و مگس هست</p></div>
<div class="m2"><p>لیکن چو اجل تیغ گشد میرو گدا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در راه فنا خلق چو دندانه شانه است</p></div>
<div class="m2"><p>کائینه آن، روی چو این سطح قفا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آسایش و سیمرغ دو نام است که معنیش</p></div>
<div class="m2"><p>یا هست، در ادراک نمی آید و یا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک است میان خانه افلاک و لیکن</p></div>
<div class="m2"><p>چندان که نه بندد ره سیلاب بلا نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر گیسه عالم چه زنی دست که در وی</p></div>
<div class="m2"><p>از عقد بها یک گهر بیش بها نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمتر بود از یک نفس امید فراغت</p></div>
<div class="m2"><p>گر هست تو را حاصل، و الله که مرانیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>الحق گهری سخت ثمین است امان لیک</p></div>
<div class="m2"><p>افسوس که بر صفحه شمشیر بقا نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روی دل از این شاهد بد مهر بگردان</p></div>
<div class="m2"><p>کانجا که جمال است علی القطع وفا نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم مالک دریاست، زمین هم ملک کان</p></div>
<div class="m2"><p>برجای عظیم است ولی نیک ادا نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مجروح هوائی ز هوان دست بیفشان</p></div>
<div class="m2"><p>زیراک هوان نیست هر آنجا که هوا نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون سبزه زره پوش بباغ آی که دردی</p></div>
<div class="m2"><p>زوبین زندت غنچه و گر چند کیا نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زین عالم خونخواره دلی خونشده چون لعل</p></div>
<div class="m2"><p>دانم که مرا هست ندانم که کرا نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر چرخ چه دعوی کنم از بخت چگویم</p></div>
<div class="m2"><p>چون محنت عمرم ز تفاسیر کوا نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دیماه فنا تاختن آورد جهان را</p></div>
<div class="m2"><p>خورشید امان جز کنف ظل خدا نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از خطه اقبال شه مشرق و مغرب</p></div>
<div class="m2"><p>گر نیم قدم پیش نهی خطه خطا نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون صورت پیگانش که سیراب ظفر باد</p></div>
<div class="m2"><p>در باغچه معرکه یک زهر گیا نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جز شاخ زمستانی و جز مرغ خزانی</p></div>
<div class="m2"><p>در دولت او گیست که با برگ و نوا نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای شاه. ضمیر تو که در پرده صبر است</p></div>
<div class="m2"><p>مهری است که چون ماه نو انگشت نمانیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر رأی تو پیداست که این حادثه صعب</p></div>
<div class="m2"><p>دردی است که در سر شده قانونش دوا نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کس، نوش نکرده است زخمخانه دوران</p></div>
<div class="m2"><p>یک جام صفا کاخر او دُرد جفا نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پائی و سری نیست بزیر فلک دون</p></div>
<div class="m2"><p>کز دست فلک همچو فلک بی سر وپا نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در باغ جهان گلبن امید ز تخمی است</p></div>
<div class="m2"><p>کاو را به چنین آب و هوا نشو و نما نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در خار سر سوزن درزی نگذشت است</p></div>
<div class="m2"><p>یک جامه که چون پیرهن غنچه قبا نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با این همه هم مرهم تسلیم که از وی</p></div>
<div class="m2"><p>چون در گذری صورت بهبود شفا نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این خاتمت کل عزاهای ملوک است</p></div>
<div class="m2"><p>کاندر پی وی فاتحه ی هیچ عزا نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خلد ابدی باد جزای تو در این رنج</p></div>
<div class="m2"><p>کان را که گذشت است به جزخلدجزانیست</p></div></div>