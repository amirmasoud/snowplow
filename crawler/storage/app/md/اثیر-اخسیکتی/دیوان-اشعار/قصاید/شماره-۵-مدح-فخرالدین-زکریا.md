---
title: >-
    شمارهٔ ۵ - مدح فخرالدین زکریا
---
# شمارهٔ ۵ - مدح فخرالدین زکریا

<div class="b" id="bn1"><div class="m1"><p>زهی جناب تو والا مکان نعمت والا</p></div>
<div class="m2"><p>ز روی همت عالی فلک نشیب و تو بالا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملوک را همه روزه بدرگه تو تنزه</p></div>
<div class="m2"><p>فتوح را همه ساله به حضرت تو توّلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگوش کوس، غریو بیان فتح شنوده</p></div>
<div class="m2"><p>سعادت تو ز خامش زبان رایت اعلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز رهروان معانی تو راست سبق ترقی</p></div>
<div class="m2"><p>ز خسروان زمانه تو راست قدر معلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه نتایج و ارکان تو را مزید معالی</p></div>
<div class="m2"><p>که هفت والی چرخ از در تو اند، مولّا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از سپهر بپرسی که کیست پشت سلاطین</p></div>
<div class="m2"><p>زبان بمدح سراید بحرف واضح و والا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر ملوک جهان، پهلوان تهمتن ثانی</p></div>
<div class="m2"><p>تفاخر همه اسلاف فخرالدین ذکریا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که با شجاعت داود، ساخت ملک سلیمان</p></div>
<div class="m2"><p>که با وفای براهیم، یافت عصمت یحیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهی که زبده ی مهر وی است راحت عقبی</p></div>
<div class="m2"><p>شهی که زنده بنام وی است ساحت دنیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز روزنامه او روز کار، یافته روزی</p></div>
<div class="m2"><p>بر آستانه ی او، مکرمات یافته مأوا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر آب و سبزه شمشیر او، و قود ظفر را</p></div>
<div class="m2"><p>وجوه مطعم و مشرب، امیر منزل و مرعا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زسنک سبزه بر آرد، بالتفات و عنایت</p></div>
<div class="m2"><p>ز شیر شیر بدوشد، باحتمال و مدارا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مقر قائمه حلم اوست، مرکز قوموا</p></div>
<div class="m2"><p>هیون ساریه ذهن اوست مرکب اسرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقیم شد چو دم و طبع او بکار در آمد</p></div>
<div class="m2"><p>صدف ز لولو مکنون بقر، ز عنبر سارا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگر چنانکه توانی شنود چاوش عبرت</p></div>
<div class="m2"><p>گشاده بر قدم آورد، نی ز فتنه ولوصا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دم وی است، خرد را به نکته مایه عدت</p></div>
<div class="m2"><p>در وی است، هنر را ز فتنه مامن و ملجا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به رأی جنبش و آرام اوست، تا بقیامت</p></div>
<div class="m2"><p>ثبات مرکز اغبر، مدار گنبد خضرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهی خراب جهان را، بعدل کرده عمارت</p></div>
<div class="m2"><p>امیدهای کهن را، بفضل کرده مطرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لباس ملک، تو را زیبد، ارچه در نظر من</p></div>
<div class="m2"><p>جهان فروز تری، همچو آفتاب معرّا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو شوق در دل عاشق بطبع جای پذیرد</p></div>
<div class="m2"><p>صدای کوس تو، در طاق این رواق پر آوا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سیه سپید توشان دید، همچو جفت جواهر</p></div>
<div class="m2"><p>رخ دوام به بیند نه، طاق ابروی طغرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در آنکه دست تو دریاست شبهتی نشناسم</p></div>
<div class="m2"><p>کزو سیاهی توقیع، عنبری است ز دریا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فلک چو ابروی خُضبه خضاب وسمه گرفته</p></div>
<div class="m2"><p>در تو در خم ابرو عزیز دیده بنیا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>توئی مفلسف تدبیر عقل و حکمت خاکی</p></div>
<div class="m2"><p>توئی مهندس ترتیب چرخ و انجم و قمرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدان اجازت عدلت که در بدایت عالم</p></div>
<div class="m2"><p>چهار مادر گیتی گرفت حمل نُه آبا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زفاف خانه گردون خراب باد که روزی</p></div>
<div class="m2"><p>همی ز عقل و زمانه نبات زاید از ابنا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنان رفیع جنابی که با بلندی قامت</p></div>
<div class="m2"><p>سر فلک نکند جز مکانت تو تمنا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو تو مجرد جودی زبان عقل که باشد</p></div>
<div class="m2"><p>که در مقابل رایت کند حدیث مجازا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بمدح توست سخنور زبان لاله اخرس</p></div>
<div class="m2"><p>بنام توست نیوشنده کوش صخره صما</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو نرم روی خدنک از کمان صلب پرانی</p></div>
<div class="m2"><p>خواص نرمی و چربی دهد صلابت خارا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زمانه با تو چه سودا پزد، که دست شجاعت</p></div>
<div class="m2"><p>بریخت خون حوادث، زسهم این سر صفرا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگرچه رای تو بودی بیاض عارض مشرق</p></div>
<div class="m2"><p>بخاصیت ندمیدی ز شب دواله سودا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تبیره ساز حوادث، بر او زند سپر کین</p></div>
<div class="m2"><p>هر آنکه کرد ز قهرت، دمی نکون و تبرا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دماغ چرخ ز خصمت، بجز بخار نبیند</p></div>
<div class="m2"><p>که در دهان زمانه نواله ایست مهنا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بحفل طالع تو داد ملکتست تمامت</p></div>
<div class="m2"><p>بسعی دانش تو کار دانش است مهیا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خجسته کلک تو صوری است بر دهان ممالک</p></div>
<div class="m2"><p>زده بقصد امامت دم عنایت و احیا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنان به نزل نعم با نعم قرار گرفتی</p></div>
<div class="m2"><p>که جز بلفظ شهادت نرفت، بر دهنت، لا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اجل چو صورت پروانه شد بر آتش تیغت</p></div>
<div class="m2"><p>که عشق بار ندادش بخود فراغت و پروا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر آنکه زنده کند سنت خلاف تو یکدم</p></div>
<div class="m2"><p>حدیث خلق رها کن بخلق قابلی او را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عظیم خلق تو گوئی که ارغنون بزرگی است</p></div>
<div class="m2"><p>که از مسام بد اندیش جان کشد بمواسا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ممان که با تو سر از جور برکند فلک الثور</p></div>
<div class="m2"><p>که زهره تو، به ثور است آفتاب به جوزا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برای بزم تو چون برگشند برق یمانی</p></div>
<div class="m2"><p>که شد بریشم نورش بانعکاس مثنا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو گرد خلق تو کرددز حلم و جود و تواضع</p></div>
<div class="m2"><p>مثلثی بکف آرد سپهر مجمره سیما</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همی سمور تو گیرند سامیان مراتب</p></div>
<div class="m2"><p>از آن سما، به جنابت نه بست مجلس اسما</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ملقب اند باسماء تو ملوک زمانه</p></div>
<div class="m2"><p>توئی بقدر ز القاب آن گروه مسما</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بنای ملک تو آنگه کند قبول تباهی</p></div>
<div class="m2"><p>کجا قبول کند سطح آب خط معما</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز اصطناع تو ممکن بود بباغ زمانه</p></div>
<div class="m2"><p>که تخم بقله حمقا شود درختک دانا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بهار در رک او آن عمل کند که نماید</p></div>
<div class="m2"><p>بجای عقد شکوفه ز شاخ عقد ثریا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هزار ناله بر آید بر او ز باغ خورنق</p></div>
<div class="m2"><p>هزار شود در افتد از او به جنت مأوا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ستارگان زبر و زیر شاخ چرخ مثالش</p></div>
<div class="m2"><p>گرفته صورت اکلیل در برابر رؤیا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سپهر گفته خرد را بدین نشان که تو دادی</p></div>
<div class="m2"><p>اثیر پرهنر است این درخت بوالعجب اسما</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اگر زهی ز درختی چنین دریغ نداری</p></div>
<div class="m2"><p>بر بقای مخلد کند ز برگ هویدا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همیشه تا خرد و نفس و چرخ و طبع زمانه</p></div>
<div class="m2"><p>بآفرینش یزدان مقوّمند و محلّا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نه عقل راه نماید نه نفس کار گذارد</p></div>
<div class="m2"><p>مکر ببدرقه رحمت خدای تعالا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو باش عالم دل را ز عیم قاعده گستر</p></div>
<div class="m2"><p>تو باش ملکت جان را امیر مرتبه افزا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ستاره زفت و تو معطی مزاج عمر تو زیرک</p></div>
<div class="m2"><p>زمانه سست و تو محکم، سپهر پیر و تو برنا</p></div></div>