---
title: >-
    شمارهٔ ۸ - مدح خواجه امام صفی الدین اصفهانی
---
# شمارهٔ ۸ - مدح خواجه امام صفی الدین اصفهانی

<div class="b" id="bn1"><div class="m1"><p>زهی تو روح بخوبی و دیگران همه قالب</p></div>
<div class="m2"><p>بساط حسن تو بوسد چو بر گشاد بقا، لب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رد ای نور سیه کرد ماه سبز عمامه</p></div>
<div class="m2"><p>چو پیش عارض خورشید درکشی تتق شب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار دیده بره بر نهاده اند به مجمر</p></div>
<div class="m2"><p>ز صحن گلشن مینا مقدسان مقرب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که تا به تحفه کی آردنسیم باد سحر گه</p></div>
<div class="m2"><p>بجان خرید بخوری از آن دو زلف مطیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر بماه فلک مایه ی دهد رخت از شرم</p></div>
<div class="m2"><p>مه مقنع سر بر نیارد از چه نخشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار جان عزیز است و بوسه ی ز تو احسنت</p></div>
<div class="m2"><p>من الذی هو یطلب من الذی هو یرغب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشان سبزه پدیدار کرد چشمه نوشت</p></div>
<div class="m2"><p>که عقل راه نداند همی بجانب مشرب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا عزیمت رفتن درست کی شود از ری</p></div>
<div class="m2"><p>که هیچگونه نیاید برون مه تو،ز عقرب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز غمزه ی تو بر جاودان خطه بابل</p></div>
<div class="m2"><p>فسانه گشت فسون های جانگداز مجرب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تناسب است به زلف تو قامت شعرا را</p></div>
<div class="m2"><p>که بار منت مخدومشان همی کند احدب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طراز کشور دانش نگین خاتم رادی</p></div>
<div class="m2"><p>صفی دولت و دین اکرم العراق مهذب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسی که سایه اعدای او به قتل خداوند</p></div>
<div class="m2"><p>چو آفتاب سنان میکشد بدیده ی اهدب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهر تند رکابت اگر رکاب ببوسد</p></div>
<div class="m2"><p>بتازیانه دوران کند قضاش مؤدب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رکاب دار قدر داغ در نهاد بآتش</p></div>
<div class="m2"><p>که بوالفتوح کند نقش ران ادهم و اشهب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو راه گنه کمالش سپرد پای تفکر</p></div>
<div class="m2"><p>بسنگ عجز در آمد اثر ندید ز مطلب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زهی برای تو تاریخ مشکلات مفضل</p></div>
<div class="m2"><p>زهی بجود تو تألیف مکرمات مبوب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نهاد غاشیه بر دوش آسمان سبک پی</p></div>
<div class="m2"><p>گهی که پای در آری چو آفتاب بمرکب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز عشق کسب شرف دست معطی تو چنان کرد</p></div>
<div class="m2"><p>که یک قدم ننهد پای حرص در ره مکسب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پی کتابت آن خامه شهاب وش تو</p></div>
<div class="m2"><p>دبیر گردون درکش گرفته تخته مکتب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هوای مدح تو هر ساعه در ضمیر سخنور</p></div>
<div class="m2"><p>قذان کنند چو سودای حک و ناخن اجرب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ملقب است ز ذات بزرگوار تو القاب</p></div>
<div class="m2"><p>که گفت اینکه ز القاب نام توست ملقب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو خواست کرد کریمی و سروری و بزرگی</p></div>
<div class="m2"><p>اگر نگشتی دست و دل تو ملجاء و مهرب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو تو کریم نه بیند، دگر زمانه سفله</p></div>
<div class="m2"><p>چو تو یگانه نیارد، دگر جهان مرکب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز پاس عدل تو، شیران شرزه وقت غنودن</p></div>
<div class="m2"><p>گشاده چشم بخواب اندرون روند چو، ارنب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر آن تذرو که در مرغزار عدل تو پرّد</p></div>
<div class="m2"><p>گرفت نسر فلک را گه شکار به مخلب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بزرگوارا هر چت خطاب کرد بیانم</p></div>
<div class="m2"><p>یقین شناخت کزان پایه، برتر است مخاطب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدفع عارضه تو شگفت نیست که عیسی</p></div>
<div class="m2"><p>اگر فرو جهد از سقف این رواق مقبقب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شفا، دو اسبه همی تازد از حدیقه تقدیر</p></div>
<div class="m2"><p>قریب در رسد اینست در گمان من اغلب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو ماه چرخ جلالی، تو را چه مفسدت از میغ</p></div>
<div class="m2"><p>تو شیر بیشه ملکی، تو را چه منقصت از تب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>طلا ریاضت خایسک دید و زحمت سندان</p></div>
<div class="m2"><p>از آن صحایف مصحف از او کنند مذهب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چگونه بوسه زند بر عذار و فرق عروسان</p></div>
<div class="m2"><p>گل ار نگردد در کوره ی گلاب مذوّب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بتاج شاهان زان بر نهاد تخت جلالت</p></div>
<div class="m2"><p>که لعل در تف خورشید گشته بود معذب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رسید موسم خورشید بر تو باد خجسته</p></div>
<div class="m2"><p>بگوی، تا همه اسباب آن کنند مرتب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز دسته های ریاحین و باده های مروج</p></div>
<div class="m2"><p>ز مطربان خوش آواز و مادحان مهذب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بباده طبع تو رغبت نموده و فضلا را</p></div>
<div class="m2"><p>گهی ثنای تو مطلب، گهی دعای تو مرغب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو شمع جان حسودان بلب رسیده ز عزت</p></div>
<div class="m2"><p>تو بر نهاده بلب، صبح وار جام لبالب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بهر چه رای کنی انقیاد کرده تو را چرخ</p></div>
<div class="m2"><p>به هرچه روی کنی کارساز بوده تو را، ربّ</p></div></div>