---
title: >-
    شمارهٔ ۳۸ - مدح نجم الدین لاجین والی همدان
---
# شمارهٔ ۳۸ - مدح نجم الدین لاجین والی همدان

<div class="b" id="bn1"><div class="m1"><p>در سر مردان غم عشق تو معجر می‌کشد</p></div>
<div class="m2"><p>زاهدان را در خرابات قلندر می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هشت راه از کعبه وصل تو تا زر می‌رود</p></div>
<div class="m2"><p>چار حد از خامه عشق تو تا سر می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیک بر سنجم تو را چون زر کنی احوال آنک</p></div>
<div class="m2"><p>نام عشقت بر زبان می‌آرد و زر می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خشک بندی بر نقاب افکنده تا غیرتت</p></div>
<div class="m2"><p>میل حرمان در هزاران دیده تر می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دام زلفت بند بر پای دل و دین می‌نهد</p></div>
<div class="m2"><p>دست حسنت حلقه در گوش مه و خور می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب و گل چون بگسلد زنجیر عشقت تا قضا</p></div>
<div class="m2"><p>جان و دل را رشته در گردن بدین درمی‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه دست‌آویز او طرف کمند زلف توست</p></div>
<div class="m2"><p>دولتش بر بام این پیروزه منظر می‌کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زود عمر عالمی بگسست و خشمت هر زمان</p></div>
<div class="m2"><p>زیر بیدادی بده آهنگ برتر می‌کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاشهٔ صبرم که نعل افکندهٔ راه عناست</p></div>
<div class="m2"><p>نزل تیمارت به منزلگاه محشر می‌کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پشت و پهلویی ندارد لیک بار عالمی</p></div>
<div class="m2"><p>همچو کلک نجم‌الدین با جسم لاغر می‌کشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن امل بخشی که جودش کار حاتم می‌کند</p></div>
<div class="m2"><p>وآن اجل خشمی که قهرش تیغ حیدر می‌کشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از سر همت خطیب جاه حاکم نسبتش</p></div>
<div class="m2"><p>طیلسان ماه، در اطرف منبر می‌کشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیل عزمش رخت گل بر پشت صرصر می‌نهد</p></div>
<div class="m2"><p>میل رایش کحل اندر چشم اختر می‌کشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کلک مانی طبعش آن استاد چابک صورت است</p></div>
<div class="m2"><p>کآذر اندر دستگاه صنع آذر می‌کشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حلقه گوش دواتش چون حسام شاه شرق</p></div>
<div class="m2"><p>حلقه‌ها در گوش اهل هفت کشور می‌کشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آب روی حکم کوثر کام او از روی صبر</p></div>
<div class="m2"><p>روز و شب ماه ار در بینی آذر می‌کشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جّره باز ذهن او از آشیان قدسیان</p></div>
<div class="m2"><p>هر زمانی جبرئیلی را به شهپر می‌کشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر همه صاحب عیاران می‌بچربد در کمال</p></div>
<div class="m2"><p>ناقد ذاتش بهر معیارکش سر می‌کشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رشته‌ها گر سوی چنبر می‌کشد سر پس چرا</p></div>
<div class="m2"><p>رشته او داج خصمش سر به چنبر می‌کشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عقدهٔ ابروی قهرش ماه را گیسوکشان</p></div>
<div class="m2"><p>در سیاست گاه صحن ظل اغبر می‌کشد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از غبار آستانش هر نفس چشم خرد</p></div>
<div class="m2"><p>زّله دیگر به زیر آستین بر می‌کشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاد باش ای محسنی کز منزل احسان تو</p></div>
<div class="m2"><p>از پی سرمایه هر دم نزل دیگر می‌کشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل چو با تو عقد بند بکر فکرت را شبی</p></div>
<div class="m2"><p>تا سحرگاه ابد کابین دختر می‌کشد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دایه ابرت در این گهوارهٔ ازرق حلل</p></div>
<div class="m2"><p>نیم شیران امل را تنگ در بر می‌کشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دست بیرون کرد رایت ماه را با اوج او</p></div>
<div class="m2"><p>بر مهی طغرای منشور مزّور می‌کشد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نعل شبدیز تو چون شب سرمه‌سای آمد از آنک</p></div>
<div class="m2"><p>توتیا در دیدهٔ این پیر اعور می‌کشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در کمند پیسهٔ روز و شب از بنگاه تو</p></div>
<div class="m2"><p>بخت ناز کبریا بر بام محور می‌کشد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عقلت اندر کاردان چون از ممالک دید گفت</p></div>
<div class="m2"><p>رخش رستم بین که پشم‌آکند بر خر می‌کشد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صاحبا پرورد کان خاطرم را آسمان</p></div>
<div class="m2"><p>در صف مدح تو صدر بنده‌پرور می‌کشد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همچو زوار تو گوش هوش ارباب هنر</p></div>
<div class="m2"><p>از در فکرم به دامن درو گوهر می‌کشد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بارهٔ فضلم و لیکن عالم ابلق مرا</p></div>
<div class="m2"><p>در قطار صحبت یک عالم استر می‌کشد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شاهد طبعم ز بیم چشم مشتی با حفاظ</p></div>
<div class="m2"><p>چهره‌ها در پرده خط معنبر می‌کشد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>الغیاث ای نوح عصمت هین که طوفان بلا</p></div>
<div class="m2"><p>زورق عمرم به گرداب فنا درمی‌کشد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا شب غواص شکل از قعر این بحر نگون</p></div>
<div class="m2"><p>صد هزاران لؤلؤ خوشاب بر سر می‌کشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>رشک انجم باد هر گوهر که از دریای طبع</p></div>
<div class="m2"><p>خاطرم در سلک اوصاف تو سر درمی‌کشد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بازو و برزت قوی بادا که چنگال اجل</p></div>
<div class="m2"><p>فقر را در پای آن دست توانگر می‌کشد</p></div></div>