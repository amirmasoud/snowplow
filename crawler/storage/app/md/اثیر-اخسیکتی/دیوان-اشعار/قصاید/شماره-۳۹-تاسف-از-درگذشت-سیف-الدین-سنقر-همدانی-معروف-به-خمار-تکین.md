---
title: >-
    شمارهٔ ۳۹ - تاسف از درگذشت سیف الدین سنقر همدانی معروف به خمار تکین
---
# شمارهٔ ۳۹ - تاسف از درگذشت سیف الدین سنقر همدانی معروف به خمار تکین

<div class="b" id="bn1"><div class="m1"><p>نمی توان بسر سرّ روزگار رسید</p></div>
<div class="m2"><p>که خانه بسته در است و نظر شکسته کلید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپید گشت چو چشم شکوفه چشم امل</p></div>
<div class="m2"><p>که در بهار فراغت گلی شکفته ندید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر این چهار چمن خنده ی چو غنچه که زد</p></div>
<div class="m2"><p>کجا بسوزن خاری جهان دلش نخلید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بزم کیتی منشین و گرنه ساغر وار</p></div>
<div class="m2"><p>بخون سپار دل و دیده را بجای نبید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکرده مهره گردن چو ناچخ از آهن</p></div>
<div class="m2"><p>به پیش سیلی ایام کی توان بجهید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدام مرگ برآویخت صد هزا ران مرغ</p></div>
<div class="m2"><p>که حرصش از سر منقار نیم دانه نچید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکال صورت عالم زهر که در ذهنی است</p></div>
<div class="m2"><p>بدیده ی خرد این حال را بباید دید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا شد آنکه خدنکش دل ستاره بدوخت</p></div>
<div class="m2"><p>کجا شد آنکه حسامش سر ستم ببرید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کجا شد آنکه بنای فساد آب ببرد</p></div>
<div class="m2"><p>ز میغ تیغ وی از بس سرشک خون بچکید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا شد آنکه صف خصم را به تنهائی</p></div>
<div class="m2"><p>هزار بار بیک حمله سر بسر بدرید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کجا شد آنکه کمینه وثاق قود کشش</p></div>
<div class="m2"><p>عنان ز ابلق گردون بکین همی بکشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پناه لشکر منصور سیف الدین سنقر</p></div>
<div class="m2"><p>که باز عدل جز از آشیان او نپرید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به بست چاشنی از اضطراب ملک عراق</p></div>
<div class="m2"><p>که کام تلخی، تلخی زهر مرگ چشید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خبر نداشت که جان میفروشد آنساعت</p></div>
<div class="m2"><p>که امن خلق ببازار رزم در نخرید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به جنگ و آشتی روز کار تن در ده</p></div>
<div class="m2"><p>که جای نیک و بد است و سرای پاک و پلید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل ستیزه عصمت بمزد خود برساد</p></div>
<div class="m2"><p>گذشت چون بجوار خدای پاک رسید</p></div></div>