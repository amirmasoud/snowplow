---
title: >-
    شمارهٔ ۵۶ - مدح خواجه جلال الدین ابوالفضل بن قوام الدین درگزینی وزیر سلطان ارسلان بن طغرل
---
# شمارهٔ ۵۶ - مدح خواجه جلال الدین ابوالفضل بن قوام الدین درگزینی وزیر سلطان ارسلان بن طغرل

<div class="b" id="bn1"><div class="m1"><p>ایا خدای بخلقت نیافریده نظیر</p></div>
<div class="m2"><p>ستانه تو جهان را ز حادثات مجیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلال دولت و دینی نظام ملک و ملل</p></div>
<div class="m2"><p>پناه تیغ و کلاه و مدار چرخ و سریر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان فضل ابوالفضل کز فضایل او</p></div>
<div class="m2"><p>نیافت و هم فضولی گذر به عشر عشیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آنکه در شکند با تو کین بکاسه سر</p></div>
<div class="m2"><p>بیک پیاله بیاندازدش دهان سعیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دولت تو خجسته لقا و خوش منظر</p></div>
<div class="m2"><p>نیامدست جوانی ز صلب عالم پیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه در نشیمن دانش نشست چون تو همای</p></div>
<div class="m2"><p>نه از مشیمه اقبال زاد، چون تو وزیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در این کمان نکون، خوش همی رود الحق</p></div>
<div class="m2"><p>جهان بآهن حکم تو بسته، در زنجیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حامئی چو تو بازوی روزگار قوی</p></div>
<div class="m2"><p>ز گوهری چو تو گنجور آب و خاک، فقیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جناب توست جهان را ز جور خود ملجا</p></div>
<div class="m2"><p>وجود توست فلک را ز دور خود توفیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پیش دست تو با فضل پیش دستی سبق</p></div>
<div class="m2"><p>نشسته جبهت خورشید در خوی تشویر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کف کفایت تو بست، دست ظلم فلک</p></div>
<div class="m2"><p>تف مهابت تو بُرد آبروی اثیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در این دوازده خانه بساط سبز برون</p></div>
<div class="m2"><p>بدیده فکرت تو نقش مهره ی تقدیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنون بشکر چورمان همه دهان و لب است</p></div>
<div class="m2"><p>که گشت ناسخ تاخیرش آیت تیسیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فلک چو فومه انگور خون گریست بسی</p></div>
<div class="m2"><p>در انتظار جنابت بپایمال ز حیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو ساغر تو بباده است خوش همی سازد</p></div>
<div class="m2"><p>گران رکابی بم با سبک عنانی زیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صبا ز جلوه خلقت که نافه شرف است</p></div>
<div class="m2"><p>نمیرسد بکره بند زلف جعد غدیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه مایه ها که ز طبع تو می، بیند وزد</p></div>
<div class="m2"><p>هوای فصل بهار و سخای ابر مطیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بجان همی بخرد چرخ گرد اسبت را</p></div>
<div class="m2"><p>که چشم درد مهش را ز سرمه نیست گزیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دهان کنبد مه آتشین شود چو تنور</p></div>
<div class="m2"><p>چو خاطر تو برآرد زبانه ی تدبیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فنا سپه نکشد بر حصار ملت و ملک</p></div>
<div class="m2"><p>که خندقی است زعزم تو برگذار قعیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرآنکه در شکند با تو کین بکاسه سر</p></div>
<div class="m2"><p>به یک پیاله بینباردش دهان سعیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سماع صیت تو مرغی است لیک، عالم شاخ</p></div>
<div class="m2"><p>شراب خلق تو دامی است، لیک مردم گیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سبک سران حسد، گر زبون عزم تواند</p></div>
<div class="m2"><p>عجب مدان که، بود خس بدست باد اسیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به تیغ کین تو، همچون پیاز مثله شوند</p></div>
<div class="m2"><p>اگرچه ده ده در یک بطانه اند، چو سیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فلک دو وقت به خصمان تو خطاب کند</p></div>
<div class="m2"><p>بود سیاق خطابش دو لفظ عکس پذیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بوقت کودکی، ای شیرتان حرام چو خون</p></div>
<div class="m2"><p>بگاه خواجگی، ای خونتان حلال چو شیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عدوت، تا علف تیغ انتقام شود</p></div>
<div class="m2"><p>رسد، به منزل شیخوخت از ره تا خیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وگرنه چونکه بپرداختی و ثاق رحم</p></div>
<div class="m2"><p>شرر مثال بدی، زود آی و حالی میر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کنون فلک سر تعذیب احمقان دارد</p></div>
<div class="m2"><p>زبان کلک تو اکنون، دلیل بعث پذیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زهی غریب کرم را، بحضرت تو وطن</p></div>
<div class="m2"><p>خهی برید سخن را، بمدحت تو میسر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مهندسان خرد را، ثنای توست بنا</p></div>
<div class="m2"><p>مدبران فلک را، ذکای توست مشیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر بخدمت این بار گه، نیامده ام</p></div>
<div class="m2"><p>بجان تو، که مفرمای حمل بر تقصیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شعاع نیک بسیط است و چشم شبپره تنک</p></div>
<div class="m2"><p>ستانه سخت بلند است و پای مور قصیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در این مقام که پویندگان پالانی</p></div>
<div class="m2"><p>برنگبار سرشت او فتند از کشمیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز قلعه حیوانی چو یونس اندر بحر</p></div>
<div class="m2"><p>ز بندهای طبیعی چو یوسف اندر بیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مجوی گوهر معنی که در چنین منزل</p></div>
<div class="m2"><p>مسیح داغی خیر است و خر غلام شعیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>برون ز خشم و شره نیست هیچ باعث طبع</p></div>
<div class="m2"><p>سواد را به زبیر و گلاب را به زهیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فراغت است طبیعی مغنّی گل را</p></div>
<div class="m2"><p>که جغد راند با ساده زخمه های صفیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرا در آینه فکر، صورت آن بسته است</p></div>
<div class="m2"><p>کسی که گفت نکور رو چنانکه خواهی گیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همیشه تا که، عقولند دفتر الهام</p></div>
<div class="m2"><p>همیشه تا که، نقوشند خامه تصویر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز حجم دفتر، تو دست ملک باد قوی</p></div>
<div class="m2"><p>بروی خامه تو، چشم عقل باد قریر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ثنای شاه جهان و مدیح صدر بزرگ</p></div>
<div class="m2"><p>به یک شکم متولد شده ز فکر اثیر</p></div></div>