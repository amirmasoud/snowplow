---
title: >-
    شمارهٔ ۹۹ - مدح خواجه رکن الدین حسن
---
# شمارهٔ ۹۹ - مدح خواجه رکن الدین حسن

<div class="b" id="bn1"><div class="m1"><p>عرض داد از چابکی خورشید شمعی پیرهن</p></div>
<div class="m2"><p>در جلال آسمان بر مهد اطفال چمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابر دریا باری از الماس هندی چاک زد</p></div>
<div class="m2"><p>بی گناهی تا بدامان جیب پاکان عدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه بر اطراف چمن غلطد به پهلو آفتاب</p></div>
<div class="m2"><p>گه در آغوش نسیم آید بشوخی، یاسمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عود سوز، لاله ها را مشک تبت در کنار</p></div>
<div class="m2"><p>عود ساز، بلبلان را راه ارغن در دهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه را ره گرد همچون ساغر اندر وقت نوش</p></div>
<div class="m2"><p>زلف مشکین بنفشه روی می فام سمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشه پروین ببرد از حقه سیماب گون</p></div>
<div class="m2"><p>ماه مشاطه ز بهر نظم عقد نسترن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برده انگشتان چراغ افروخت دست ارغوان</p></div>
<div class="m2"><p>تا که شمع ساق نرگس سرنگون دارد لکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبحدم چون قرص کافوری ز فوار آب نور</p></div>
<div class="m2"><p>در سپیداب ضیا گیرد رخ مشک ختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرغ را، الفاظ مهر آمیز چون شاه حرم</p></div>
<div class="m2"><p>صبح را ز انفاس عشق انگیز، چون پیر قرن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر ادیم لامکان دوزد کواکب را قمر</p></div>
<div class="m2"><p>پس سهیل او گذارد کام در راه یمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون غراب اندر پگه خیزی علم بیرون زنیم</p></div>
<div class="m2"><p>سوی طاووسان بستانی، هزار آوا، و، من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او. چو سحبان در اداء حمد رب العالمین</p></div>
<div class="m2"><p>من چو حسان در بیان مدح رکن الدین حسن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن ز مهرش باغبان شرع گفته مرحبا</p></div>
<div class="m2"><p>ای نهال سدره بیخ تو، تو بر طوبی فتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای بدست راد، ایوان سخا را پادشاه</p></div>
<div class="m2"><p>وی به تیغ نطق، ایوان جدل را تهمتن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در دل فرعونیانش دست موسی را عصا</p></div>
<div class="m2"><p>در لب زوار سورش نطق عیسی را وطن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون طلی آهن دلان کفر خوش گردن شوند</p></div>
<div class="m2"><p>گر برآرد رأی او از کیسه اکسیر سخن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قرصه سیمین سر قتل حسودش را زند</p></div>
<div class="m2"><p>تیغ های آتشین بر آبگون سنگ مسن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عندلیب نعمتش هرگه که دستان بر کشد</p></div>
<div class="m2"><p>لاله زار آسمان چون گل بدرد پیرهن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مصری یوسف نگارش را چو دامن چاک زد</p></div>
<div class="m2"><p>صد هزاران پیر کنعان است در بیت الحزن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرزه صبح جلالش بفکند تخت بنات</p></div>
<div class="m2"><p>دشنه خورشید رایش بگسلد عقد پرن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حقه لعلش اگر دری نهد بر من یزید</p></div>
<div class="m2"><p>مفلس آید کیسه ی دریا، ز یک ثمن ثمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بحر فتوی، کان تقوی، ظل حق خورشید شرع</p></div>
<div class="m2"><p>شیخ ملت، پیرامت، زین دین، تاج سنن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن ولی، با خط جودش، چون رقم سیمین نما</p></div>
<div class="m2"><p>و آن عدو، در خط قهرش چون قلم زرین بدن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون کمین وابسته ی شب، شیر شمشیر دلیل</p></div>
<div class="m2"><p>چوی کمانکش بوده بدعت مرد ناورد وثن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بار گاهش، اولین محمل ز بنگاه وجود</p></div>
<div class="m2"><p>آستانش، آخرین منزل ز بیداد و محن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خار زرع شرع، یعنی مبتدع بر کند پاک</p></div>
<div class="m2"><p>اینهمه طوبی نشانان بنده این خار کن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هرکه فتان خواندش بر حق بود زیرا که او</p></div>
<div class="m2"><p>بر جمال مذهب نعمان جهان را مفتتن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جذبه لطف است بر دو دست مشکوه از فلک</p></div>
<div class="m2"><p>خلوت انس است، در نه پای و مندیش از منن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مبتدع جولان کنان، هل من مبارز برزبان</p></div>
<div class="m2"><p>پس تو را شمشیر در بازار غفلت مر تهن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چرم خر طبعان عیسی نام بیرون کش ز سر</p></div>
<div class="m2"><p>چند پوشی صدره ی طاووس بر قد زغن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر بضاعت دار شرعی، سود بشناس از زیان</p></div>
<div class="m2"><p>ور عروس آرای فرعی، حله وادان از کفن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر سئوالی داری اینک مفتی نعمان بیان</p></div>
<div class="m2"><p>ور، سواری خواهی اینک حیدر رستم فکن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حمله روباروی باید کرد چون شیر عرین</p></div>
<div class="m2"><p>روبه آسا چند از این در هر پسی دستان و فن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خلوت اعجاز و آنگه، سحر کاری پرده در</p></div>
<div class="m2"><p>درگه فردوس آنگه، عنکبوتی پرده تن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از حنیفی مذهبان از ماست بر ما آنچه هست</p></div>
<div class="m2"><p>زین و آن تا کی شکایت الغیاث از خویشتن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای عدو را اشک لعل از بیم تو چون ناردان</p></div>
<div class="m2"><p>وی ولی را فرق سبز از جاه تو چون نارون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چرخ ازرق پوش در وجد ثنایت پایکوب</p></div>
<div class="m2"><p>ماه بزم افروز، بر عشق جمالت دست زن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بی سران را، افسر انعام تو اقلیم بخش</p></div>
<div class="m2"><p>سرکشان را، سیلی تأدیب تو گردن شکن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کام را پی کن، بدین طوطی لب شکرفشان</p></div>
<div class="m2"><p>تا حسود از رشک بکدازد چو شکر در لبن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ابرو بحرو، مصرو هند، از کلک و خنجر، با شما</p></div>
<div class="m2"><p>پس سموم امتحان و باغ کوفه ممتحن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نیست آخر، تیع نعمان بسملی چون تیغ هند</p></div>
<div class="m2"><p>تا جهانی حک شود، بر دوک جوق پیره زن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نکته ها سرباز گفت اخسیکتی با خصم زانک</p></div>
<div class="m2"><p>هندوان تاس بشناسند رمز بر همن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا عروس آب، چون برقع بدرد در بهار</p></div>
<div class="m2"><p>بادمشک افشان کند، زلفش پر از تاب و شکن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آب جا پرور مبادا، پیش لطفت خوشگوار</p></div>
<div class="m2"><p>باد عالم را مبادا پیش صیتت کام زن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>روزگار از طبع در حکم تو بسته کوش و هوش</p></div>
<div class="m2"><p>آسمان، بر عهد رکن الدین نهاده جان و تن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدر قدر او، علم بر کنبد اخضر زده</p></div>
<div class="m2"><p>عالمی در گرد آن موکب، چو انجم انجمن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نامه مشاطه دوشیزگان خاطرم</p></div>
<div class="m2"><p>تا یکی زایشان اگر عرضی دهد باشد حسن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هر تهی چشمی چو چنبر را چنین سحر آرزوست</p></div>
<div class="m2"><p>لیک بر بام جهان کمتر توان رفت از رسن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هرکسی گوید من و تو لیک اندر شرط عشق</p></div>
<div class="m2"><p>فرقکی هست از چه بالوعه تا چاه ذقن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>من نگویم من، که آید بر بناگوش خرد</p></div>
<div class="m2"><p>بی خلاف از دست یک من، ناگهانی سنگی دومن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>لیکن آخر کافری نبود من و طبعی چنین</p></div>
<div class="m2"><p>وانگهی هر هرزه لائی ژاژ خائی گو و من؟</p></div></div>