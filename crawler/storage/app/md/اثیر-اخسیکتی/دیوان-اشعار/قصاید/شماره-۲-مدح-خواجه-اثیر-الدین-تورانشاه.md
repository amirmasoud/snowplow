---
title: >-
    شمارهٔ ۲ - مدح خواجه اثیر الدین تورانشاه
---
# شمارهٔ ۲ - مدح خواجه اثیر الدین تورانشاه

<div class="b" id="bn1"><div class="m1"><p>میان در بست اقبال آگهی را</p></div>
<div class="m2"><p>قدوم موکب توران شهی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان صدری که پیش آستانش</p></div>
<div class="m2"><p>فلک خم داد بالای سهی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با یامش که جاویدان بما ناد</p></div>
<div class="m2"><p>هنر دریافت ایام بهی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفرمانش که دایر باد دائم</p></div>
<div class="m2"><p>قمر در باخت دوران مهی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز فرّ او بر این گرد آخُر خشک</p></div>
<div class="m2"><p>سعادت مستعد شد خر بهی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهی در ظل او بنهاد گردون</p></div>
<div class="m2"><p>صعود رتبت مهر و مهی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شمشیرش چنانشد شیر گردون</p></div>
<div class="m2"><p>که جوشن ساخت عجز روبهی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زعشق صیت او سنک فسرده</p></div>
<div class="m2"><p>برآرد پنبه از گوش آگهی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وزارت جو که بر نطع جلالت</p></div>
<div class="m2"><p>دورخ طرح افکند شاهنشهی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز هر تهمت بر آسوده است رایش</p></div>
<div class="m2"><p>بلی تهمت نماند منتهی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کمالش را زنقص آن ایمنی هست</p></div>
<div class="m2"><p>که از آتش عیار ده دهی را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز مغروری که خصم جاه او بود</p></div>
<div class="m2"><p>دماغش قابل آمد ابلهی را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فلک را کرد بر تأدیب او چست</p></div>
<div class="m2"><p>فلک جوی است خود کمتر رهی را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زبان تیغ داند کرد تفسیر</p></div>
<div class="m2"><p>سقط بانک خروس بیگهی را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درخش رای او چون چشمه طاق</p></div>
<div class="m2"><p>نهد حصبه نکو روی چهی را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کفش، کار است مجلس خانه جود</p></div>
<div class="m2"><p>ز در بیرون کند منت نهی را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کند در هیضه اسراف صد بار</p></div>
<div class="m2"><p>بیک انعام آز مشتهی را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ببازار کرم صد کیسه پر</p></div>
<div class="m2"><p>بها کرده است یک دست تهی را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر خواهد کلاه ملک بخشد</p></div>
<div class="m2"><p>کمر در بستگان در گهی را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خداوندا. در این ایوان که گوئی</p></div>
<div class="m2"><p>بهشت است آفریده خود رهی را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بفرخ فال می خور تا مغنی</p></div>
<div class="m2"><p>دهد، بالا سماع خر گهی را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بمی بر لب زند ممزوج ساغر</p></div>
<div class="m2"><p>بنوشاب دم آبان مهی را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قدح ز اشک عنب خالی فرستد</p></div>
<div class="m2"><p>که یادت باد رخسار بهی را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زاول منزل دل تا در لهو</p></div>
<div class="m2"><p>مدان چون من حریفی همرهی را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سخن های در ازم هست لیکن</p></div>
<div class="m2"><p>صداع آماده بهتر کوتهی را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی تا فرهی را نام باشد</p></div>
<div class="m2"><p>معین باد نامت فرهی را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز سر سبزی چنان بادی که از وی</p></div>
<div class="m2"><p>خزان مینا کند برک کهی را</p></div></div>