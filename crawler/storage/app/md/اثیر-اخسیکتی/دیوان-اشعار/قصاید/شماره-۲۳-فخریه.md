---
title: >-
    شمارهٔ ۲۳ - فخریه
---
# شمارهٔ ۲۳ - فخریه

<div class="b" id="bn1"><div class="m1"><p>گره گشای سخن خامه توان من است</p></div>
<div class="m2"><p>خزانه دار روان خاطر روان من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشید زین من این دیزه هلال رکاب</p></div>
<div class="m2"><p>از آنک شهپر روح القدس عنان من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنار و آستی کان چو بحر پر درشد</p></div>
<div class="m2"><p>که در ولایت معنی گدای کان من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ارسلا نشه ملک قناعتم زین روی</p></div>
<div class="m2"><p>جهان قیصر و خان، صد یک جهان من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرور سیم نیالایدم چو ماهی شیم</p></div>
<div class="m2"><p>که چشمه سار ازل غسل کاه جان من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمان من نکشد دست و بازوی شروان</p></div>
<div class="m2"><p>که تیر چرخ یک اندازی از کمان من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه من قرین وجودم سفه بود گفتن</p></div>
<div class="m2"><p>هنوزدرعدم است آنکه هم قران من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمان، زمان زمین گستر خرد بخش است</p></div>
<div class="m2"><p>محال باشد گفتن زمان، زمان من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر زبان هنر می سراید این معنی</p></div>
<div class="m2"><p>بحکم عقل سجل میکنم که آن من است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زآخور فلکی تو سنی برون ناید</p></div>
<div class="m2"><p>که طوق نعلش بی حلقه دهان من است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سزد که منبر دعوی هزار پایه کنم</p></div>
<div class="m2"><p>که ترجمان رموز ازل بیان من است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکار نکته ز شاهین وحی بربایم</p></div>
<div class="m2"><p>چو آستان شه عزلت آشیان من است</p></div></div>