---
title: >-
    شمارهٔ ۳۳ - مدح خواجه صدرالدین قاضی مراغهٔ وزیر سلطان طغرل
---
# شمارهٔ ۳۳ - مدح خواجه صدرالدین قاضی مراغهٔ وزیر سلطان طغرل

<div class="b" id="bn1"><div class="m1"><p>کار دو گیتی به کام صدر اجل باد</p></div>
<div class="m2"><p>جایگه دشمنانش صدر، اجل باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کعبه آمال حرز دولت و دین آنک</p></div>
<div class="m2"><p>با شرف او زحل وضیع محل باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوبت عمر ابد بنام بلندش</p></div>
<div class="m2"><p>کوفته در صدر بارگاه ازل باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایل بی برگ با عنایت جورش</p></div>
<div class="m2"><p>چون گل صد برگ در حمایت ظل باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سینه شیران ز بهر رتبت و رایش</p></div>
<div class="m2"><p>کردن طاعت نهاده پیش کفل باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر سوی رایش نگه کند به تکبر</p></div>
<div class="m2"><p>دیده خورشید مبتلای سبل باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیره دلی را که نقص او بزبان برد</p></div>
<div class="m2"><p>حرف بکار اندرش زبان وَرَل باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاسد جاهش ببوستان بقا، در</p></div>
<div class="m2"><p>چون بگلستان در اوفتاده جعل باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخش قضا بامضای عزم عجولش</p></div>
<div class="m2"><p>چون شترلوک در میان و حل باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای ز شرف بر سپهر کرده تقدم</p></div>
<div class="m2"><p>پای محل تو بر جبین زحل باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موسم اضحی شتاب کرد بخدمت</p></div>
<div class="m2"><p>اسب نجاحش بزیر ران امل باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وز پی قربانت شرع اگر نه پسندد</p></div>
<div class="m2"><p>چرخ ندا کرده خون جدی و حمل باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وآنکه کم آید بحضرت تو چو خادم</p></div>
<div class="m2"><p>گوشتی و نان و هیزمش بخلل باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین سه غمش باز خر که ضامن عمرت</p></div>
<div class="m2"><p>تا به قیامت خدای عز و جل باد</p></div></div>