---
title: >-
    شمارهٔ ۶۳ - توصیف رباب، چنگ، کمانچه، دف، بربط - مدح سید عزالدین خسروشاه فرزند علاء الدوله فخرالدین عربشاه رئیس همدان
---
# شمارهٔ ۶۳ - توصیف رباب، چنگ، کمانچه، دف، بربط - مدح سید عزالدین خسروشاه فرزند علاء الدوله فخرالدین عربشاه رئیس همدان

<div class="b" id="bn1"><div class="m1"><p>بزمی است ز لطف خلد پیکر</p></div>
<div class="m2"><p>حورانش بکف در آب کوثر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبی که خوی خجالت او</p></div>
<div class="m2"><p>سر بر زند از جبین آذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی ز سواد شب فکنده</p></div>
<div class="m2"><p>صد سلسله بربوده منور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لعلش، بر بوده آب لاله</p></div>
<div class="m2"><p>جز عش، بنشانده باد عنبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در فرقت مشک طره او</p></div>
<div class="m2"><p>پیراهن، خرقه کرده مجمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با ساعد بسته چنگ خورده</p></div>
<div class="m2"><p>بر بیست و چهار عرق نشتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی داعی مهر سلطنت نای</p></div>
<div class="m2"><p>بفروخته سر، برای افسر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جسته ز کمانچه تیر نغمت</p></div>
<div class="m2"><p>در قبضه گه کمان محور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز زخم جگر خراش زخمه</p></div>
<div class="m2"><p>به نشسته رباب دست برسر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بربط ز پی پیاله بازی</p></div>
<div class="m2"><p>در پنجه گرفته هشت ساغر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دف در کف زهره گان مجلس</p></div>
<div class="m2"><p>کوکب جلجل، سپهر چنبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از قبه مجمر فلک شکل</p></div>
<div class="m2"><p>ظاهر شده صد هزار اختر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تنوره ز حقه لب و. دم</p></div>
<div class="m2"><p>گلگونه دهان بروی اخگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پروانه بکرد کعبه شمع</p></div>
<div class="m2"><p>گه طوف کنان گهی مجاور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جمع آمده بر سپهر عشرت</p></div>
<div class="m2"><p>از ساقی و باده صد، مه و خور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز جوش جیوش، نوبتی را</p></div>
<div class="m2"><p>روشن شده نفخ صور و محشر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بانک دم کرّنای کرده</p></div>
<div class="m2"><p>این کور هزار دیده راکر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وز جرعه ساقیان نموده</p></div>
<div class="m2"><p>این دیده پی بریده اشقر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر طاق سپهر اگر بتخمین</p></div>
<div class="m2"><p>برجی است بصورت دو پیکر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تعلیق هزار صورت نغز</p></div>
<div class="m2"><p>زین طاق سپهر شکل بنگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زین کلک، شکسته خامه مانی</p></div>
<div class="m2"><p>زان، دست گزیده طبع آذر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چالاکی جمله گفته با تو</p></div>
<div class="m2"><p>ما را نه جماد خوان نه جانور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل برده ز خلق لطف هریک</p></div>
<div class="m2"><p>بی جان که شنید و دید، دلبر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر جان یابند، جمله نشگفت</p></div>
<div class="m2"><p>در دولت شهریار صفدر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دیباچه نسخه سعادت</p></div>
<div class="m2"><p>فهرست نتایج پیمبر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عزالدین، کز کلاه داری</p></div>
<div class="m2"><p>بر فرق فلک فکند معجر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خسرو شه، کز نهیب تیغش</p></div>
<div class="m2"><p>شد روبه ماده ضیغم نر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای کرده سخات دامن آز</p></div>
<div class="m2"><p>چون جیب صدف ملاذ گوهر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وقتی که ره هوا بگیرد</p></div>
<div class="m2"><p>جز تیغ کشیده پنجه و در</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>و آن روز که نطفه نرینه</p></div>
<div class="m2"><p>در صلب شود ز بیم، دختر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سنگ از تف رمح شمع تمثال</p></div>
<div class="m2"><p>حل گردد، چون در آب شکر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لعبت بازان چرخ بندند</p></div>
<div class="m2"><p>در پیش زگرد تیره چادر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خاک از پی ترکتاز دیده</p></div>
<div class="m2"><p>پای آرد در رکاب صرصر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زان مرغ چهار بال در سیر</p></div>
<div class="m2"><p>نسر فلکی بیفکند پر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر طارم سرمه رنگ غلطد</p></div>
<div class="m2"><p>در آب سیاه دیده ی خور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از صدمت گرز گاو صورت</p></div>
<div class="m2"><p>ارواح نهند رخت بر خر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خوانسالار اجل کند راست</p></div>
<div class="m2"><p>بر خوانچه تیغ کاسه سر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دراعه دهر را فرستد</p></div>
<div class="m2"><p>ناوک سوی کلبه رفو گر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو، رمح شهاب شکل در کف</p></div>
<div class="m2"><p>شبدیز فلک، بزیر ران در</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یک مرده چو مهر حمله آری</p></div>
<div class="m2"><p>پهنای زمین چو ذره بشکر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مشاطه خنجر تو بندد</p></div>
<div class="m2"><p>بر گردن و گوش ملک، زیور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا زنده، سلاله جلالت</p></div>
<div class="m2"><p>چون نصرت و فتح با تو، همبر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آن بازوی زورمند کزوی</p></div>
<div class="m2"><p>سر پنجه ملک یافت یاور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سوگند، به صانعی کزویست</p></div>
<div class="m2"><p>بر کشتی دور نقطه، لنگر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون کلک ازل براند حکمش</p></div>
<div class="m2"><p>جز سطح عدم نبود دفتر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر تورنه، این نکاح بوده است</p></div>
<div class="m2"><p>تزویج عرض نجست جوهر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زین جاست که کدخدای صورت</p></div>
<div class="m2"><p>بر مایه اصل گشت شوهر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خورشید نثار را همی ساخت</p></div>
<div class="m2"><p>زان کیسه سنگ کرد پرزر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چرخ از پی این نشست بر اوج</p></div>
<div class="m2"><p>مملو شده آستین بگوهر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ای مملکت درست، بالین</p></div>
<div class="m2"><p>از خاک در تو کرده بستر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چون تو خلفی نزاده هرگز</p></div>
<div class="m2"><p>از سه پدر و چها مادر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>با خاک درت مشام ارواح</p></div>
<div class="m2"><p>سر در نارد به مشک اذفر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در قید تو فتنه کیست، محبوس</p></div>
<div class="m2"><p>در وصف تو عقل چیست، مضطر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بی دست تو تیغ و کلک بیکار</p></div>
<div class="m2"><p>بی مدح تو فکر و نطق، ابتر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هرچند که در جهان اثیر است</p></div>
<div class="m2"><p>امروز به نظم، سحر گستر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بفکنده سپر که می نبیند</p></div>
<div class="m2"><p>در جعبه فکر، تیر دیگر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ای پایگه جلالت تو</p></div>
<div class="m2"><p>از قمه هفت چرخ برتر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>زین بیشتری و لیک دستار</p></div>
<div class="m2"><p>زان بیش نمی شود میسر</p></div></div>