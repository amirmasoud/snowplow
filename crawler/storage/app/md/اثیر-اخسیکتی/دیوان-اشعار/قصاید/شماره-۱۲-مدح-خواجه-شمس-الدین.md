---
title: >-
    شمارهٔ ۱۲ - مدح خواجه شمس الدین
---
# شمارهٔ ۱۲ - مدح خواجه شمس الدین

<div class="b" id="bn1"><div class="m1"><p>گر مایه گیرد از رخت ای دلبر آفتاب</p></div>
<div class="m2"><p>عاشق شود زمانه بصد دل، بر آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر بامداد گیرد بر بوی روی تو</p></div>
<div class="m2"><p>نه کلّه فلک را در زیور آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در رشک جیب تو بدردّ صبح پیرهن</p></div>
<div class="m2"><p>از وی چو بامداد بر آرد سر آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بوسه ی ز لعل تو بر خویشتن کند</p></div>
<div class="m2"><p>دارد هزار کیسه کان پر زر آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زیر جل کشیده جمال تو چرخ را</p></div>
<div class="m2"><p>تا رخت بار نامه نهد بر خور آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا زلف مشکبار تو بر ماه تکیه زد</p></div>
<div class="m2"><p>از غم شکسته دل شد چون مجمر آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خطبة الوداع جمال و بهای خویش</p></div>
<div class="m2"><p>هر روز از آن کبود کند منبر آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن تو نوبتی چو برون زد براه چرخ</p></div>
<div class="m2"><p>پرچم کند سنان خط محور آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از رشگ آفتاب رخت هر شبی چو شمع</p></div>
<div class="m2"><p>با کام خشک باشد و چشم تر آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مانده است جمله دیده از این منظر بلند</p></div>
<div class="m2"><p>هر روز در نظاره ی آن منظر آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در خلوتی که ماه تو زنجیر بگسلد</p></div>
<div class="m2"><p>مانند حلقه روی نهد بر در آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی رؤیت جمال تو سر بر نیاورد</p></div>
<div class="m2"><p>در خوابگاه مغرب از بستر آفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شب بر رخ تو باده خورم تا زعکس او</p></div>
<div class="m2"><p>طالع شود چو می ز لب ساغر آفتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از مه نقاب طره شبرنگ باز کن</p></div>
<div class="m2"><p>تا بر نیاید از تتق خاور آفتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای ماهرو اگرچه دراین حق بدست توست</p></div>
<div class="m2"><p>چندین مکش زبان وقعیت در آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چاکر شو آفتاب فلک را از آنکه هست</p></div>
<div class="m2"><p>در پیش آفتاب زمین چاکر آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دریای فضل و گوهر افضال شمس دین</p></div>
<div class="m2"><p>کزکان رای اوست کمین گوهر آفتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گردون مکرمات فرامرز کز شرف</p></div>
<div class="m2"><p>با قدر گردنش نبود سرور آفتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کر، باس او بگنبد نیلوفری رسد</p></div>
<div class="m2"><p>چادر کند کبود چو نیلوفر آفتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بی عزم او نتافت بر این بحر نیلکون</p></div>
<div class="m2"><p>هر صبحدم ز هیچ طرف معبر آفتاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای، خیره زان بیان سخن پرور آسمان</p></div>
<div class="m2"><p>وای تیره زان بنان سخا گستر آفتاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بی بازوی ضمیر تو گاه مصاف صبح</p></div>
<div class="m2"><p>در روی شب همی نزند خنجر آفتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در بند یک اشارت دنبال چشم توست</p></div>
<div class="m2"><p>کاید بسر دوان بسرت یکسر آفتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بی سایه عنایت خورشید رأی تو</p></div>
<div class="m2"><p>در سایه ذره وار شود مضمر آفتاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این ظلم کز تو بر سر زر آمد و درم</p></div>
<div class="m2"><p>بر سر کند ز دست تو خاکستر آفتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آرزوی مجلس تو بر زمین نهاد</p></div>
<div class="m2"><p>زانو به پیش زهره خیناگر آفتاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زین پس براین رواق سپر شکل درطلوع</p></div>
<div class="m2"><p>گیرد بجای تیغ بکف مزمر آفتاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در مجلس تو گرچه زبی مایکی خویش</p></div>
<div class="m2"><p>دانم که خدمتی نکند در خور آفتاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای زهره میاندیش که از خاکپای تو</p></div>
<div class="m2"><p>معجر فروکشد به رخ از هر آفتاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای برگرفته زان کف بیضاء مال بخش</p></div>
<div class="m2"><p>در بخشش و عطا مدد کیفر آفتاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با لعبتی که عارضش از پرده سیاه</p></div>
<div class="m2"><p>آرد بسجده از فلک اخضر آفتاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زین شعر آفتابی کز کان خاطرم</p></div>
<div class="m2"><p>لعلی است کش نشانده در او افسر آفتاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>امروز من رهی به جناب تو آمدم</p></div>
<div class="m2"><p>زیرا که بر سپهر بود خوشتر آفتاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دارد ضمیر من بسخن پروری کمال</p></div>
<div class="m2"><p>هرگز نشان که داده سخن پرور آفتاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون عبهر آمده است مرا طبع دیده ور</p></div>
<div class="m2"><p>کزوی شود بوقت سخن مظهر آفتاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عبهر ز آفتاب شگفته شود و لیک</p></div>
<div class="m2"><p>در طبع من شگفته شد از، عبهر آفتاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هرچند سایه وار سیه گشت حال من</p></div>
<div class="m2"><p>هم نیست از دویدن مستظهر آفتاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>روشن شود به نزد عطای تو زآنکه هست</p></div>
<div class="m2"><p>مدحت فروش ذره و مدحت خر آفتاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا رایت از کمین گه مشرق بر آورد</p></div>
<div class="m2"><p>در ساعتی بغرب کشد لشکر آفتاب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بادا، چنانکه رایت رای تو تا بدید</p></div>
<div class="m2"><p>شمشیر صبح بر نکشد دیگر آفتاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون عود گشته طالع اعدات محترق</p></div>
<div class="m2"><p>در مجمر قرآن چو کند آذر آفتاب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گردون چنبریش بصد رشته بسته پای</p></div>
<div class="m2"><p>گر بر در تو سرکشد از چنبر آفتاب</p></div></div>