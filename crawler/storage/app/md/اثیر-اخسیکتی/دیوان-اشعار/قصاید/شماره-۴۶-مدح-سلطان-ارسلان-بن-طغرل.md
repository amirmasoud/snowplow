---
title: >-
    شمارهٔ ۴۶ - مدح سلطان ارسلان بن طغرل
---
# شمارهٔ ۴۶ - مدح سلطان ارسلان بن طغرل

<div class="b" id="bn1"><div class="m1"><p>تا قافله شیر ز ماهی به حمل شد</p></div>
<div class="m2"><p>در باغ صبا صانع چالاک عمل شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بلبل خوش نغمه که ناهید طیور است</p></div>
<div class="m2"><p>نالیدن او تار اغانی به زحل شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خاک بر انگیخت گل زرد زر سرخ</p></div>
<div class="m2"><p>با مرتبت رونق او خاک خجل شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ضرّاب زر از لاله درستی ملکی بود</p></div>
<div class="m2"><p>تا خور که درستی فلکی بود دغل شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا زاده دریا چو صدف قبه بر آورد</p></div>
<div class="m2"><p>دامان گل از لعل پر از لولوی طل شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاخ متمایل شبه دست اشل داشت</p></div>
<div class="m2"><p>انگشت زنان برگ بر آن دست اشل شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از یشم زده ابر که خفتان فک بود</p></div>
<div class="m2"><p>این یشم بیفتاد که اکلیل قلل شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بستان چو عروسان ز زر و سیم جلی گشت</p></div>
<div class="m2"><p>هان چون سر و تنشان ز خضر سیر خلل شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا قرص فروزنده که تنوّر روان است</p></div>
<div class="m2"><p>مهمانی عالم را در وجه حمل شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دست پلنگینه شب از نور غزاله</p></div>
<div class="m2"><p>هر جا که غزالی است سراینده غزل شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان بخش جوان بخت که در مجلس ومیدان</p></div>
<div class="m2"><p>روشن کف او شهره ی روزی و اجل شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم زور غضفر که به مردی و دلیری</p></div>
<div class="m2"><p>یکباره چو هم نام در آفاق مثل شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با فضله خوان و قدحش جدول و بستان</p></div>
<div class="m2"><p>این صحن بهشت آمد و آن جوی عسل شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با زلزله گرز کرانش گه ناورد</p></div>
<div class="m2"><p>تجویف دل کوه پراز لرزو و جل شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنجا که سپر ترکش میدان بلا گشت</p></div>
<div class="m2"><p>روزی که زره چنبر حلقوم بطل شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از تیغ سران برق هوا کرد و بصر سوخت</p></div>
<div class="m2"><p>و از خون یلان خاک در آغشت و وحل شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر عارض مه کرد در آغوش کلف جفت</p></div>
<div class="m2"><p>در چشم سنان چون مدد باد سبل شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از مردمک دیده تهی یافت نشیمن</p></div>
<div class="m2"><p>هر شعله خنجر که ببالین مقل شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پولاد بلارک لقب از قبضه گردان</p></div>
<div class="m2"><p>یکبار دگر در دل خارای جبل شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وان باره که بر گوشه او کوه سکون بود</p></div>
<div class="m2"><p>چون و هم سبک تک همه تن باد عجل شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با پرچم دیلم کله رمح شهنشاه</p></div>
<div class="m2"><p>روح از بر اعدا چو دماغ از سر کل شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وان چیره زبان هندوی ابخاز گشایش</p></div>
<div class="m2"><p>چون طبع مناظر به همه جنگ و جدل شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اندهگده خصم ز سیلاب حسامش</p></div>
<div class="m2"><p>گر خود همه طاق فلکی بود ظلل شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای ابلق خوش گام زمان وقف رکابت</p></div>
<div class="m2"><p>یکران مه از داغ تو آباد کفل شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی یاد تو هر حرف که در کام بجنبید</p></div>
<div class="m2"><p>حقا که کزاینده تر از نوک عسل شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا محو شود خصم تو از دفتر ابجد</p></div>
<div class="m2"><p>چون بهر نهان خانه امراض و علل شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شاها، خبرت باد که حال من مسکین</p></div>
<div class="m2"><p>یکبار دگر، همچو دماغم به خلل شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر واقعه این است سراینده لب من</p></div>
<div class="m2"><p>انگار که چون چشم حیا میر اجل شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا نقش کتابت که نگار جمل آمد</p></div>
<div class="m2"><p>از نقش سه حرف است که تصحیف جمل شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عمرت ابدی باد، که عزت ازلی گشت</p></div>
<div class="m2"><p>وان، کاو ابدی گشت هم از حکم ازل شد</p></div></div>