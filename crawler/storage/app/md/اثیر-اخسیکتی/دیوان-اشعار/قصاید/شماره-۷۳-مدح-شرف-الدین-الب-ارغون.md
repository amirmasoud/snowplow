---
title: >-
    شمارهٔ ۷۳ - مدح شرف الدین الب ارغون
---
# شمارهٔ ۷۳ - مدح شرف الدین الب ارغون

<div class="b" id="bn1"><div class="m1"><p>کرد از جهان رحیل جهانی همه شرف</p></div>
<div class="m2"><p>ای مملکت علی الله و ای فلک لاسلف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون اسب رقعه دو سپهر پیاده رو</p></div>
<div class="m2"><p>فرزین ملک را بر بود از میان صف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اختر فشان ز دیده سحابی بمن رسید</p></div>
<div class="m2"><p>گفتا بگویمت لمن الملک قد کشف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفت آنکه، از خزانه او آز شد غنی</p></div>
<div class="m2"><p>رفت آنکه، از ستانه او جود زد صلف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر در نهاد چرخ گمان شکل تیردار</p></div>
<div class="m2"><p>آنرا که بود حضرتش آمال را هدف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بس که آه، دامن گیسوی شب گرفت</p></div>
<div class="m2"><p>بر روی ماه سوخته شد پرده کلف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون چهره در نقاب کشید او عجب مدار</p></div>
<div class="m2"><p>گر فتنه همچو زلف بشورد بهر طرف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او بود، دست ملک چو از کار بازماند</p></div>
<div class="m2"><p>زین پس کجا امید بقبض و به بسط کف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی نی هنوز نیست کرم سخره فنا</p></div>
<div class="m2"><p>نی نی هنوز نیست امل طعمه تلف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بحر هنر بچرخ رساند همه عتاب</p></div>
<div class="m2"><p>قصر سخابه مهر برآرد همی شرف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن دوحه کمال که از بیخ بر گسست</p></div>
<div class="m2"><p>کام جهان خوش است بدین میوه شرف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خورشید مکرمت شرف الدین که بخلاف</p></div>
<div class="m2"><p>کم زاید از مشیمه دوران چنو خلف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صدر سپهر مسند و دُر جلال عقد</p></div>
<div class="m2"><p>شاخ ارم حدیقه و شاه حرم کنف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر زخمه تحکمش این چرخ گوژپشت</p></div>
<div class="m2"><p>در کوش انقیاد کشد حلقه همچو دف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای دردها، ز جرعه، کین تو عاریت</p></div>
<div class="m2"><p>وی، صفوها ز جام رضای تو معترف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگزیده خدمت تو زمانه بصد و لوع</p></div>
<div class="m2"><p>بگرفته دامن تو سعادت بصد شرف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر پایگه که منصب صدر سعید بود</p></div>
<div class="m2"><p>اکنون تو راست زانکه توئی دُر آن صدف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>میراث شرع جز به محمد کجا رسد</p></div>
<div class="m2"><p>آن دوده را که مثل خلیلی بود شرف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زان پیش بین تر است دل پادشاه وقت</p></div>
<div class="m2"><p>قلزم ز قطره فرق کند لولو از خذف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گوهر چو روشن است نگوید حدیث سنگ</p></div>
<div class="m2"><p>عنبر چو حاضر است نگردد بگرد کف</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اقبال چون تکلف این اقتراح کرد</p></div>
<div class="m2"><p>بر نیت عزیز منه بعد از آن کلف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر چرخ تکیه کم کن اگرچه غلام توست</p></div>
<div class="m2"><p>می بین که روزگار چه عاق است و ناخلف</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جز نام نیک کسب مکن زانکه مال و عمر</p></div>
<div class="m2"><p>هستند روزگار تهی مایه را علف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بنگر به چیست زنده ثنای گذشتگان</p></div>
<div class="m2"><p>کوتاه شد فقد عرف الشر من عرف</p></div></div>