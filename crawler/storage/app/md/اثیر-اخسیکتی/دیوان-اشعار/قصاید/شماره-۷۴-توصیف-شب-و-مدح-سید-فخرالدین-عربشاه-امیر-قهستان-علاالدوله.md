---
title: >-
    شمارهٔ ۷۴ - توصیف شب و مدح سید فخرالدین عربشاه امیر قهستان «علاءالدوله»
---
# شمارهٔ ۷۴ - توصیف شب و مدح سید فخرالدین عربشاه امیر قهستان «علاءالدوله»

<div class="b" id="bn1"><div class="m1"><p>دوش که این شهسوار کره ابلق</p></div>
<div class="m2"><p>از قرپوس غروب گشت معلق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شام سیه گر، بزیر دست فرو داد</p></div>
<div class="m2"><p>مهره ی اصفر ز طرف رقعه ازرق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سر زین کوهه ی افول در افکند</p></div>
<div class="m2"><p>سبز قبای سپهر ترک مغرق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سقف جهان پر ز برگ نر گسه دیدم</p></div>
<div class="m2"><p>چون طبق سبز پر، ذرایر زنبق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نصفی ی سیمین ماه داشت پر از دُر</p></div>
<div class="m2"><p>ساقی زرین کلاه سیمین منطق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهر، که مجلس فروز بزم جهان است</p></div>
<div class="m2"><p>کرد از آن بیم، عزم کال محقق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشت پدید از نقاب گیسوی ظلمت</p></div>
<div class="m2"><p>گردن این رخش تیز کام مطوق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو نشان حق از میانه باطل</p></div>
<div class="m2"><p>یا چو خیال صواب در دل احمق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با فلکم زین قبل مناظره افتاد</p></div>
<div class="m2"><p>گرچه مقالات هر دو بود مصدق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتمش این ملحم سپید که بسته ست؟</p></div>
<div class="m2"><p>بر سر رمح سماک رامح بیرق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت: مخالف عقیم دورفکنده است</p></div>
<div class="m2"><p>بر لب دریای نیل هاله زورق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه قهستان علاء دولت و عالی</p></div>
<div class="m2"><p>مفتخر دوده فخر دین و کنف حق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خسرو عادل عربشه آنکه عجم را</p></div>
<div class="m2"><p>گشت مصفا ز تیغش آب مروق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه زمین روب میوه دار نوالش</p></div>
<div class="m2"><p>با طبق آفتاب گشت مطابق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ابر که مفتاح فتح باب جهان است</p></div>
<div class="m2"><p>بی کف او کم گشاد یک در مغلق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرد بغلطاق خار پشت نسیمی</p></div>
<div class="m2"><p>از گل اخلاق او حریر و ستبرق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باس قوی ساعدش چو دست برآورد</p></div>
<div class="m2"><p>بست سر انگشت روزگار بفندق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بارمعانی دو مغزه بست چو بادام</p></div>
<div class="m2"><p>هر که بمدحش دهان گشاد چوفستق</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دوش خرد گفت: پادشاه بحق اوست</p></div>
<div class="m2"><p>گفتمش: اینها چه، سر بتافت که الحق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای ز حسام تو تاج ملک مرصع</p></div>
<div class="m2"><p>و ز سر کلک تو کار شرع برونق</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شد ز حساب فش سواد هویدار</p></div>
<div class="m2"><p>گردن این رخش تیز کام مطوق</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خاک درت کعبه سرای مسدس</p></div>
<div class="m2"><p>نور کفت شمسه ی روان مطبق</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رکن و ثیق است تیغ شاه جهان را</p></div>
<div class="m2"><p>رکن دگر خامه ی تو، بل هو اوثق</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جود تو بر گاو بست محمل حاتم</p></div>
<div class="m2"><p>نطق تو بر خر نهاد رخت فرز دق</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرچه تو سازی جهان در آن نزند طعن</p></div>
<div class="m2"><p>هرچه تو گوئی فلک بر او ننهد دق</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای شده تشبیب فتح و نص سعادت</p></div>
<div class="m2"><p>از ورق آسمان بذکر تو ملحق</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خامه فکرت بود بمدح تو جاری</p></div>
<div class="m2"><p>نامه دولت بود بذکر تو ملصق</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وقت نظر دیده بان قلعه حزمت</p></div>
<div class="m2"><p>ماهی خاکی به بیند از بن خندق</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از چو تو شاخی ریاض مرتضوی را</p></div>
<div class="m2"><p>ابر به جیب است و آفتاب مطوق</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هین که بدین عید جمله در رقم آورد</p></div>
<div class="m2"><p>تا بقلم نسخه سدیر و خورنق</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در جل ساغر کش آن کمیت طرب را</p></div>
<div class="m2"><p>چونکه مه روزه زین نهاد بر ابلق</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>موسم باده است و کار باده در این وقت</p></div>
<div class="m2"><p>از همگان لایق آمد و ز تو، الق</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>می بقدح خور که حاسدان تو و من</p></div>
<div class="m2"><p>جمله بکاسه همی خورند و به ملعق</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ساغر خورشید آب در دهن آرد</p></div>
<div class="m2"><p>چون تو بکف برنهی شراب مروق</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بار بدی را بخوان که زیر نزارش</p></div>
<div class="m2"><p>زار بنالد چو عاشقان مشوق</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>غنه او در غنا چو حکم تو جاری</p></div>
<div class="m2"><p>زخمه ی او پر نوا چو امر تو مطلق</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در فلج افتاده باسماع تر او</p></div>
<div class="m2"><p>زهره خوش نغمه را دو دست زمرفق</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ساقی گلرخ بدست باده گلرنگ</p></div>
<div class="m2"><p>ماه مدور نهاد مشک محرّق</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>طرف لبش خالی از هلال مقیر</p></div>
<div class="m2"><p>گرد گلش دودی از عبیر مسحق</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هم گه، میدان چو تیغ و نیزه معارض</p></div>
<div class="m2"><p>هم گه، مجلس چو جام و باده معانق</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کرده عروسان بکر گلشن فکرم</p></div>
<div class="m2"><p>شقه الفاظ را به جلوه گری شق</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>موکب شعر مرا ز فخر مدیحت</p></div>
<div class="m2"><p>مقرعه زن گشته صد رشیدی و عمعق</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا ندهد طوطی مشبک قالب</p></div>
<div class="m2"><p>غنه بلبل بهر زه لائی لقلق</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>لجه اقبال باد جام تو را ریق</p></div>
<div class="m2"><p>چهره ی خورشید باد کلک تو را رق</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کون که موضوع دست کاری قدس است</p></div>
<div class="m2"><p>بوده ز یک مصدر جلال تو مشتق</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آرزوئی می برم ز خلعت و آنرا</p></div>
<div class="m2"><p>یک نظر شاه کرده گیر محقق</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کام زنی باد پی سبکسر و قبچاق</p></div>
<div class="m2"><p>درعه ئی از اطلس و کلاه مغرق</p></div></div>