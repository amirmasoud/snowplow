---
title: >-
    شمارهٔ ۱۱۵ - مدح خواجه اثیرالدین نورانشاه
---
# شمارهٔ ۱۱۵ - مدح خواجه اثیرالدین نورانشاه

<div class="b" id="bn1"><div class="m1"><p>ای برویت چشم روشن اختر نیک اختری</p></div>
<div class="m2"><p>آفتاب مهترانی آسمان مهتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه فرزند جهان ناقصت خواند خطاست</p></div>
<div class="m2"><p>چون تو، در وصف کمال خود جهان دیگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس تو با ما، در این جای وز رشک جاه تو</p></div>
<div class="m2"><p>چون رسن برخود همی پیچد سپهر چنبری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالمی اقطاع قدرت شد چگونه عالمی</p></div>
<div class="m2"><p>آنکه برتر زان ولایت نیست اسم برتری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یافت از رایت زهابی چشمه خورشید ازآن</p></div>
<div class="m2"><p>نرگس انجم به شست از گنبد نیلوفری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطرب عشرت سرای چرخ دف بر دف نهد</p></div>
<div class="m2"><p>گر نه تمکین یابد از بزم تو در خیناگری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتنه پنهان چون پری از تیغ کلک آسای توست</p></div>
<div class="m2"><p>کز طریق خاصیت بگریزد از آهن پری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه چون زنجیر سرپیچید از درگاه تو</p></div>
<div class="m2"><p>دولتش گوید سر و سندان چو حلقه بردری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستبوسند اختران مشاطه کلک تو را</p></div>
<div class="m2"><p>چون رخ دفتر بیاراید بخط عنبری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با تو ور پیوسته بودی خواجه تاش جبرئیل</p></div>
<div class="m2"><p>گرنه بگسستی از این پس رشته برپیغمبری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرکب لطفت بر این کام ار بماند تا بدیر</p></div>
<div class="m2"><p>خیمه ی عصمت زحد آب و گل بیرون بری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صبح اقبالت همی در جلوه امروز ایستد</p></div>
<div class="m2"><p>صبر کن تا چهره بگشاید عروس خاوری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پهلوی تیغت چنان فربه شود کزیک سخاش</p></div>
<div class="m2"><p>کیسه کان روی استغنا نهد در لاغری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سایه چتر تو را بر دوش گیرد آفتاب</p></div>
<div class="m2"><p>نامه بخت تو را در دیده گیرد مشتری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تیغ کاهی تو آراید جهان کهنه را</p></div>
<div class="m2"><p>رغم این مشت خرخاص از پی دانش خری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در رکاب مدحت تو رتبتی یابد سخن</p></div>
<div class="m2"><p>کز وزارت گرم تر راند عنان شاعری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بخت را گر تو پیوندی است استحقاق توست</p></div>
<div class="m2"><p>طوق گوهر هم ز خود بربندد آب گوهری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خدمتش را از بن دندان کمر بندد جهان</p></div>
<div class="m2"><p>هرکه دولت را مرصع کرد تاج سروری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مشتری در صدر چون مانند خورشیدت بدید</p></div>
<div class="m2"><p>آن شکوه مرتبت را شد بصد جان مشتری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ظاهراً نشناخت از حیرت تو را پرسید کیست</p></div>
<div class="m2"><p>عقل گفت، آن کز تو ظاهرتر بود در ظاهری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خواجه ی محسن اثیرالدین که بر احسان او</p></div>
<div class="m2"><p>حق تعالی ختم کرد آئین سائل پروری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>طاق اطلس را که عالم جست در زیر قباست</p></div>
<div class="m2"><p>پروزی دان بر بساط جاهش از پهناوری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صاحبا گر وقفه ئی یابم ز چرخ تیز تک</p></div>
<div class="m2"><p>وقف این درگه کنم نظم دری طبع جری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرکب فکرم براندازد ستام جبرئیل</p></div>
<div class="m2"><p>سیلی شعرم بدراند قفای سامری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سرّ القا ما، عصای کلک من روشن کند</p></div>
<div class="m2"><p>معجزش چون باز مالد کعبتین ساحری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر بجنبانی سری در من سر عالم شوم</p></div>
<div class="m2"><p>زانکه سر جنبان تو کاری نباشد سرسری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خواجه کیهائی فروشد بر جهان بیرون ز حد</p></div>
<div class="m2"><p>هرکه را صورت خریداری کند در چاکری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چشم روشن کن بدین گوهر که از همتای او</p></div>
<div class="m2"><p>قاصر افتاده است و قاصر دست عقل جوهری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قرة العین است دوران که پیش مهد او</p></div>
<div class="m2"><p>آسمان هم دایگانی می کند هم مادری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با سپند چشم زخمش مجمری کردی فلک</p></div>
<div class="m2"><p>گر خورد بهرام را تمکین بدی در اخکری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مخبر هرکس پس از خلاق او اخلاق اوست</p></div>
<div class="m2"><p>ای نکو منظر بحمدالله که نیکو مخبری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ابر نصرت بار تورانشه که از رایش فکند</p></div>
<div class="m2"><p>سایه بر ایران و توران رایت اسکندری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای ز حد آفرینش خیمه قدرت برون</p></div>
<div class="m2"><p>وی ز گفت آفرین خوان دامن مدحت بری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سایه بر فرق وجود افکن که چرخ اعظمی</p></div>
<div class="m2"><p>روز بر دانش همایون کن که سعد اکبری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دامن همت چنان در سطح هفتم چرخ کش</p></div>
<div class="m2"><p>کز غبار هر نحوست روی کیوان بستری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز آفتاب همت و ابر بنان روی امل</p></div>
<div class="m2"><p>بشکفان چون لاله ی سیراب و گلبرگ طری</p></div></div>