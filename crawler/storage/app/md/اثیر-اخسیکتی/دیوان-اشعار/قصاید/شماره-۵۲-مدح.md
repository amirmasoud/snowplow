---
title: >-
    شمارهٔ ۵۲ - مدح
---
# شمارهٔ ۵۲ - مدح

<div class="b" id="bn1"><div class="m1"><p>ای شاه شاهزاده سپهرت غلام باد</p></div>
<div class="m2"><p>کام جهان ز توست جهانت بکام باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دست مال بخش که جانها نثار اوست</p></div>
<div class="m2"><p>همواره در بهار طرب سوی جام باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام از سرشک دیده ی انگور در کفت</p></div>
<div class="m2"><p>وز گریه چشم حاسد تو نیل فام باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیراهن خلاف تو را بر تن عدو</p></div>
<div class="m2"><p>همواره زه چوخنجر ودامن چودام باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عقد مملکت نکند واسطه تورا</p></div>
<div class="m2"><p>دهر این چنین که هست گسسته نطام باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاها جهان ابلق اگر چند توسن است</p></div>
<div class="m2"><p>چون دید زین دولت تو خوش لگام باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میمون همای مدح تو را همچون من هزار</p></div>
<div class="m2"><p>در زیر تیر تربیتت اهتمام باد</p></div></div>
<div class="b2" id="bn8"><p>عمرت چو دور ز ایام دور باد</p>
<p>از تاج و تخت تو بد ایام دور باد</p></div>