---
title: >-
    شمارهٔ ۶۲ - مدح فخرالدین علاءالدوله عربشاه خداوند قهستان
---
# شمارهٔ ۶۲ - مدح فخرالدین علاءالدوله عربشاه خداوند قهستان

<div class="b" id="bn1"><div class="m1"><p>ای جزع تو، هم نیام و هم خنجر</p></div>
<div class="m2"><p>وی لعل تو، هم شراب و هم ساغر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نقش تو، نغز خامه ی مانی</p></div>
<div class="m2"><p>وز روی تو، تیره کلبه آذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوی کرده زطیره عذارت مه</p></div>
<div class="m2"><p>تر گشته ز خجلت لبت شکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با زلف تو، کفر گشته در بالش</p></div>
<div class="m2"><p>وز چشم تو دین فتاده در بستر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب را خم طره تو دامنگیر</p></div>
<div class="m2"><p>صبح از پی روی تو گریبان در</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیجاده تو ز غم ما را</p></div>
<div class="m2"><p>چون عارض ماه کرده است اصفر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ماه مگریز زانکه بیجاده</p></div>
<div class="m2"><p>رسمی است که کاه را کشد در بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوسی بفروش و دین و دل بستان</p></div>
<div class="m2"><p>تا حق مکاس جان نهم برسر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با سایه قهر زلف شبرنگت</p></div>
<div class="m2"><p>سر در کنف غرور دارد خور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان تحفه بمجلس تو می آرم</p></div>
<div class="m2"><p>چون شمع زبان خشک و چشم تر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در ملک گل رخ تو سلطان را</p></div>
<div class="m2"><p>نازش نرسد بتاج چون عبهر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاصه که قبول یافت لعل تو</p></div>
<div class="m2"><p>از گوهر تاج آل پیغمبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دریای سپهر موج فخرالدین</p></div>
<div class="m2"><p>دارای عجم، عرب شه صفدر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آویخته در جلال او گردون</p></div>
<div class="m2"><p>چون دست عرض زدامن جوهر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نقشی است گواه پاکی زهرا</p></div>
<div class="m2"><p>سری است دلیل عصمت حیدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیدا شده در وجود او عالم</p></div>
<div class="m2"><p>چون در غلبات مهر جرم زر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شد غرقه فلک چو از تف تیغش</p></div>
<div class="m2"><p>یک موج بزد محیط براخضر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در موج خلاف او چه کشتی هاست</p></div>
<div class="m2"><p>همخوابه ی بادبان شده لنگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای پهلوی دین، به تیغ تو فربه</p></div>
<div class="m2"><p>وی کیسه کان، ز دست تو لاغر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از خاک تو تاج میکند گردون</p></div>
<div class="m2"><p>با قدر تو باج میدهد اختر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عزم تو، چو آفتاب تنها رو</p></div>
<div class="m2"><p>نا جسته ز هیچ همرمی یاور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در یوزه قهر، کی کند هرگز</p></div>
<div class="m2"><p>از روبه ماده، چنگ شیر نر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صد غوطه دهد محیط عالم را</p></div>
<div class="m2"><p>کف تو که قلزمی است بی معبر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای معتکفان آستان تو</p></div>
<div class="m2"><p>آزاد ز دام کنبد اخضر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جز بنده که در ترانه ی مدحت</p></div>
<div class="m2"><p>دارد صفت رباب را مشگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کرده بنوا، بترک مجلس را</p></div>
<div class="m2"><p>واو، بر رک جان همیخورد نشتر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون گل بدرید پرده رازش</p></div>
<div class="m2"><p>شب بازی این بنفشه گون چادر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای نفس شرف پذیر هان و هان</p></div>
<div class="m2"><p>خود را ز شمار هر خسی مشمر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زان یک دو سه صلب دیده چون سندان</p></div>
<div class="m2"><p>کون سوخته همچو بوته زر گر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بی تیغ زبان نمانده چون ماهی</p></div>
<div class="m2"><p>پس در صف مار مانده جوشن در</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برخاسته با کمان تاریکی</p></div>
<div class="m2"><p>جلادی نور را چو خاکستر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون مار زخاک طعمه کن، بنشین</p></div>
<div class="m2"><p>لشگر چه کشی چو مور بهر خور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آلوده مشو که سرفراز آمد</p></div>
<div class="m2"><p>از غایت پاکدامنی مرمر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بندیش ز خاکساری همت</p></div>
<div class="m2"><p>دنبال خسان مدار چون صرصر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در تعزیت گل کرم بنشین</p></div>
<div class="m2"><p>دراعه کبود همچو نیلوفر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از عقل مبین هوان، که هرگز کس</p></div>
<div class="m2"><p>نگرفته مسیح را بجرم خر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عیب است بطبع چون صدف شعرت</p></div>
<div class="m2"><p>آبستن و، وانگهش لقب دختر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ناگشته دغل درون گیتی روی</p></div>
<div class="m2"><p>رایج نشوی بنزد هر مهتر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در مهد رعایت تو طفلی هست</p></div>
<div class="m2"><p>زاده چو مسیح ناطق از مادر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اینت چو دوات کی شود روشن</p></div>
<div class="m2"><p>صد تیره دلی بکرده چون دفتر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جز خامه بخون من خطی داری</p></div>
<div class="m2"><p>یک بارکی از خط ادب مگذر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>محرومی فضل من چو روز آمد</p></div>
<div class="m2"><p>گر منکر هر دوئی؟ زهی منکر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یا، بر سر دولتم کلاهی نه</p></div>
<div class="m2"><p>یا پیرهن مقام در بر در</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هرچند که بارگاه شاه اکنون</p></div>
<div class="m2"><p>دارد ز تو بندگان معزز تر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دربان سرای اوست صد خاقان</p></div>
<div class="m2"><p>فراش بساط اوست، صد قیصر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر چاووش او شدی نیازاری</p></div>
<div class="m2"><p>از خنجر مرگ حنجر سنجر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شبدیز مجره طوق با قهرش</p></div>
<div class="m2"><p>بر طاق نهد حدیث کر و فر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مریخ زبر برون کند جوشن</p></div>
<div class="m2"><p>خورشید ز سر فرو نهد مغفر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ای چرخ بساط او چو بنوردی</p></div>
<div class="m2"><p>زین خسته قهر خود سخن گستر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گو. ای شده بی ثنای تو جان را</p></div>
<div class="m2"><p>فکرت ز نتاج خلقت گوهر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در دامن من نهاده خلق تو</p></div>
<div class="m2"><p>در جیب صبا شمامه اذفر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در سایه جرم تو، زمین ساکن</p></div>
<div class="m2"><p>در پرتو جاه تو، فلک مضطر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در بزم تو، ریش گردن زهره</p></div>
<div class="m2"><p>اوزان گه لطفت توست خیناگر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کام قدح تو سر بخاریده</p></div>
<div class="m2"><p>در مالش گوش چشمه انور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ای طفل وجود را دلت دایه</p></div>
<div class="m2"><p>وی بکر مدیح را کفت شوهر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از صولت بحر لفظ او لولوء</p></div>
<div class="m2"><p>بی زحمت گاو خط او عنبر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بر کردن او خراج نه گردون</p></div>
<div class="m2"><p>در پیکر او روان دو پیکر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هر باد نفس گرفته عالم را</p></div>
<div class="m2"><p>چون ابر ز آب نظم در گوهر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>این عبداللهیش بیوفتاده</p></div>
<div class="m2"><p>رهبان صفتان دهر را باور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هر نقش فروش پای او دارد</p></div>
<div class="m2"><p>در بیع گه سران معنی خر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>در رزم کجا شود هر اشتر دل</p></div>
<div class="m2"><p>با چشم دریده مالک اشتر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بوده است ز مهر حلقه در گوشم</p></div>
<div class="m2"><p>هرچند که حلقه بوده ام بردر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در منزل شکر خواهم آسودن</p></div>
<div class="m2"><p>آنروز که رخت بر نهم زایدر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>این شرزه فرو گشایم، از زنجیر</p></div>
<div class="m2"><p>این مهره برون جهانم، از ششدر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دانسته که در پیش جهودانند</p></div>
<div class="m2"><p>جان در بدن حواریان مضطر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گشته ز غذای لقمه عرشی</p></div>
<div class="m2"><p>هم کاسه قرص مهر بر محور</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بردار چکار، آن خطیبی را</p></div>
<div class="m2"><p>کا ز چرخ نهاده باشدش منبر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زین فلک اثیر زین شعله</p></div>
<div class="m2"><p>ز محمده اثیر شد مطهر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>جز باده که نقطه عقول است</p></div>
<div class="m2"><p>شاها، تو منوش نکته دیگر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>عمری است که سخره میکند روحش</p></div>
<div class="m2"><p>از خاک در تو بر، بم و کوثر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>با باد عنان همی زند مدحت</p></div>
<div class="m2"><p>از رایض طبع او ببحر و بر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>جان میدهد از مقام او نامت</p></div>
<div class="m2"><p>در نقش طراز جامه ششتر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گر مدحت تو بیان کند، گوئی</p></div>
<div class="m2"><p>عودی است فکنده، دردم آذر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>یاقوت که میهمان آتش شد</p></div>
<div class="m2"><p>خاصیت خود بیان کند، یک سر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چرخ اربخورد به بد، رگی خونم</p></div>
<div class="m2"><p>هم بار خورم بمکرمی درخور</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>لابد به مطالبت برون آید</p></div>
<div class="m2"><p>با منظر من ز سر این مخبر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کای طوطی، در آن قفص چه خوردی قوت</p></div>
<div class="m2"><p>وی طوطی از آن چمن من چه داری بر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ای در صف ترکتاز قهر تو</p></div>
<div class="m2"><p>تقدیر قرا غلامی از لشگر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ای مایه قلزم گهر شبیر</p></div>
<div class="m2"><p>ای مایه دوحه ی ثمر شبر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>باد از سر ذوالفقار عدل تو</p></div>
<div class="m2"><p>حلق سر ذوالقفار ظلم احمر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>خورشید سمند زیر تو دلدل</p></div>
<div class="m2"><p>کردون بلند پیش تو قنبر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>گویم چه پلنگ من برنگی بر</p></div>
<div class="m2"><p>بربست طویله چون خرمزمر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بیمار سفر گزیدم از عیسی</p></div>
<div class="m2"><p>لب خشک رحیل کردم از کوثر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ای عذر جرایم فلک راتب</p></div>
<div class="m2"><p>عذریم، در این مقام یادآور</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ور جمله بد است خجلتم مسپار</p></div>
<div class="m2"><p>این راه بپای مکرمت بسپر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>دولت ز ثنای من رسانندت</p></div>
<div class="m2"><p>در عمر خضر بملک اسکندر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>حقی که مراست از جناب تو</p></div>
<div class="m2"><p>ویران نکند اساس آن، محشر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>رفتم که خلف نیابیم هرگز</p></div>
<div class="m2"><p>از پشت فلک سخنور دیگر</p></div></div>