---
title: >-
    شمارهٔ ۴۸ - مدح اتابک علاءالدین محمد
---
# شمارهٔ ۴۸ - مدح اتابک علاءالدین محمد

<div class="b" id="bn1"><div class="m1"><p>پای دار، ای کوی گردون زخم چوگان در رسید</p></div>
<div class="m2"><p>هم نبردان را خبر کن، مرد میدان در رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق را گو، دیده مفرش دار، چون دلبر نشست</p></div>
<div class="m2"><p>جسم را گو، دست درکش گیر، چون جان در رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منبر اسلام را، چون گل مرصع شد کمر</p></div>
<div class="m2"><p>زانکه بحری با جهانی دُرّ و مرجان در رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب رو معنی برست از، پیک ماه شب چراغ</p></div>
<div class="m2"><p>چون شعاع شمع خورشید درخشان در رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خط رمز خدائی، نقطه موهوم بود</p></div>
<div class="m2"><p>فضل های ذوالجلالی بین، که برهان در رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش اگر چون شمع گریان بود عقل دل شده</p></div>
<div class="m2"><p>بامدادان دلبرش، چون صبح خندان در رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خشک سال فاقه را گو، پیش کن دست سئوال</p></div>
<div class="m2"><p>کز ربیع جود، نعمت های الوان در رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشکن ای صراف، آنگه کفه میزان خویش</p></div>
<div class="m2"><p>زانکه نقاد بصیر از آل او زان در رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجلس عالی علاء الدین محمد کز شرف</p></div>
<div class="m2"><p>رخش اقبالش بدین میدان و ایوان در رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صحن این میدان، ز بهر پای بوس منبرش</p></div>
<div class="m2"><p>یکقدم بگذارد در ساعت بکیوان در رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر مثال نامه طی کردند فرش کافری</p></div>
<div class="m2"><p>چونکه توفیقش ز لشکرگاه ایمان در رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی عیار رأی تو دان این دُرست آفتاب</p></div>
<div class="m2"><p>قلب گردد چون بدارالضرب میزان در رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ار، ز ابری شکل سفره رشته در گردن ببرد</p></div>
<div class="m2"><p>چون رخش ............ ایام را خوان در رسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دامن مشرق بسی کوشید تا هنگام صبح</p></div>
<div class="m2"><p>در کله داری باین کوی گرییان در رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاد باش ای محسنی کز منزل احسان تو</p></div>
<div class="m2"><p>شاعران را صد هزاران نزل و احسان در رسید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر در قدرت، فلک میگفت، صدرا راه هست</p></div>
<div class="m2"><p>کاین مرقع پوش سیاح لت انبان در رسید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر دراندازی ردا چون مصطفی شرط است زانک</p></div>
<div class="m2"><p>جانفشانی مر مدیحت را چو حسّان در رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در عراق آن جرّه باز نطق را بگشای بال</p></div>
<div class="m2"><p>کز گریز وحشت آباد خراسان در رسید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این زمان با وی همی گوید زبان عقل بین</p></div>
<div class="m2"><p>کان ثنا گوی سخنور از سخندان در رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر تماشای در فردوس اعلی بایدت</p></div>
<div class="m2"><p>پای از این دوزخ برون نه زانکه رضوان در رسید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا که شعر آسان نماید از رهِ گشت عطا</p></div>
<div class="m2"><p>هم معانی گشت جمع و هم به اوزان در رسید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاعران را جمع گردان در جناب خویش از آنک</p></div>
<div class="m2"><p>نوبت مشتی گران طبع پریشان در رسید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دست اعلی بر علی برکش همایون تخت را</p></div>
<div class="m2"><p>هفت پایه دیگر از گردون گردان در رسید</p></div></div>