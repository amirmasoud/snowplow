---
title: >-
    شمارهٔ ۱۱۷ - مدح خواجه امام صفی الدین اصفهانی
---
# شمارهٔ ۱۱۷ - مدح خواجه امام صفی الدین اصفهانی

<div class="b" id="bn1"><div class="m1"><p>ای صورت تو آیت زیبائی و خوشی</p></div>
<div class="m2"><p>نقشی کجا چو قامت آن دلربا کشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر فرق خاک تیره در دست آب پاک</p></div>
<div class="m2"><p>ز آنروی آبدار گل لعل آتشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلفی چنانک، شام سر آسیمه بر شفق</p></div>
<div class="m2"><p>خطی چنانک، مشک ختن زان برد کشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورت نیافت عقل و تو عقل مصوری</p></div>
<div class="m2"><p>کس نقش جان ندیده تو جان منقشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید بامداد، نخندد بدان تری</p></div>
<div class="m2"><p>گلبرگ چاشتگاه، نباشد بدان خوشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دور از تصرف لب و دندان حاسدان</p></div>
<div class="m2"><p>شیرین تر است لعل تو چندانکه میچشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید نیکوان زمینی و سایه وار</p></div>
<div class="m2"><p>پایت ببوسد ارسرزلف فرو کشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا یافتی قبول رکاب صفی دین</p></div>
<div class="m2"><p>در موکب تو ماه روان شد به چاوشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنجا که طشت خانه قدرت کشند بار</p></div>
<div class="m2"><p>می در دهد و طای فلک تن به مفرشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>و اندم که یغلغ غرمات تو پر گشاد</p></div>
<div class="m2"><p>در جعبه شهر بند شود تیر آرشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صفو، لطافت تو مبراست از کدر</p></div>
<div class="m2"><p>صبح، سعادت تو معراست از عشی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کلکت بیک وجب قد کوته گه نفاذ</p></div>
<div class="m2"><p>بر بست راه حمله رمح چهل رشی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از غیرت تو گر متکیف شود هوا</p></div>
<div class="m2"><p>عصفور را بباز در آید بباغشی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آمد گدای دست تو خورشید مرتعش</p></div>
<div class="m2"><p>آری ز باب گدیه طریقی است مرعشی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی خیل تاشی گل خلقت نسیم را</p></div>
<div class="m2"><p>با هر دماغ در نگرفت آشناوشی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در عرضگاه تو ز غلامان پرده کی</p></div>
<div class="m2"><p>مردود گشت ماه به عیب متمشی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بهرام در رباطت طبعت بسی نماند</p></div>
<div class="m2"><p>تا سر برآورد بگریبان راوشی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نارنک زاد غنچه خوش طبع ترک چشم</p></div>
<div class="m2"><p>در خیل صورت تو زده لاف یلدشی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جانداری ذکاء تو را شاه روشنان</p></div>
<div class="m2"><p>تلفیق کرده تیغ زنی در سپر کشی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای با عموم عدل تو از خیل مارشکل</p></div>
<div class="m2"><p>راه گریز جسته خیال مبر قشی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در سر گرفته با نقط کلک اصفرت</p></div>
<div class="m2"><p>گلگون آسمان هوس خال ابرشی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا خصم باد سار تو بنمود روی شوم</p></div>
<div class="m2"><p>در خاک جسته چشم قمر عیب اعمشی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با خامه تو گفته خرد از سیاه حرف</p></div>
<div class="m2"><p>گرچه ز نور حامله ی مار ارقشی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا اشتلم نکرد بنام تو مرغ صبح</p></div>
<div class="m2"><p>تیغ سحر جهان نگشاید بشب کشی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تحریش روزکار مشعبد همه هباست</p></div>
<div class="m2"><p>چون تو یگانه‌ای به هنر نامحرشی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اقوالی از معایب شعر است اگرچه داد</p></div>
<div class="m2"><p>این ننگ خانه قافیه را رنگ موحشی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بینا دلی که پیش نهد شمع آفتاب</p></div>
<div class="m2"><p>چشم ستاره را بنکوهد به اخفشی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خلوتگه نشاط تو روشن لمن یشاء</p></div>
<div class="m2"><p>تو کامران به ساغر اقبال منتشی</p></div></div>