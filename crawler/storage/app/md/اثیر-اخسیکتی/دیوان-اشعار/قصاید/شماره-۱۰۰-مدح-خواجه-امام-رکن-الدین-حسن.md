---
title: >-
    شمارهٔ ۱۰۰ - مدح خواجه امام رکن الدین حسن
---
# شمارهٔ ۱۰۰ - مدح خواجه امام رکن الدین حسن

<div class="b" id="bn1"><div class="m1"><p>دوش چون راند عرصه گردون</p></div>
<div class="m2"><p>این سبک پای کره ی گلگون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی قلم گشت صنع چابک دست</p></div>
<div class="m2"><p>نقش بند بساط بوقلمون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کله دلبری شکن دادند</p></div>
<div class="m2"><p>شوخ چشمان کله گردون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نور در ظلمت او فتاد چنانک</p></div>
<div class="m2"><p>دست موسی به لحیه ی فرعون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داس در خوشه کرده گوشه ی چرخ</p></div>
<div class="m2"><p>کندم انجم او فتاده برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر رکاب هلال بوسه زنان</p></div>
<div class="m2"><p>لعبتان قباچه های جفون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه حلقه چو یاره ی لیلی</p></div>
<div class="m2"><p>چرخ نیلی چو ساعد مجنون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم ای نقطه میم دایره روی</p></div>
<div class="m2"><p>چه کرشمه است ای به ابروی نون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو قاب مقوس کشتی</p></div>
<div class="m2"><p>بر عذار مسطح جیحون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا چو نقاب منحنی قامت</p></div>
<div class="m2"><p>زده بر گنج خانه قارون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خامش نکته گوی گشته هلال</p></div>
<div class="m2"><p>به بیانی چو لولو مکنون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه بدین لام های رنگارنگ</p></div>
<div class="m2"><p>نه بدین وصف های گوناگون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که منم پیک بی قرین فلک</p></div>
<div class="m2"><p>زیر پی کرده صد هزار فزون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پرچم شاه و طوق ابرش من</p></div>
<div class="m2"><p>کرده عقد ازل بهم مقرون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>البشاره که زیر چتر صباح</p></div>
<div class="m2"><p>میرسد شهریار عید کنون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ناسخ جشن های کیخسرو</p></div>
<div class="m2"><p>ناسخ رسم های افریدون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عربی زاده ای که مولد او</p></div>
<div class="m2"><p>باد بر صاحب عجم میمون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کعبه مکرمات رکن الدین</p></div>
<div class="m2"><p>آن جنابش زرکن و کعبه فزون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خامه قدس را دلش دفتر</p></div>
<div class="m2"><p>نامه انس را دمش مضمون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رنگ حقدش کسی نیامیزد</p></div>
<div class="m2"><p>تا درونش چو گل بجوشد خون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نقش او دید در گذار قدم</p></div>
<div class="m2"><p>قلم کن به صفحه ی فیکون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نسختی بر کنار ذهنش کرد</p></div>
<div class="m2"><p>بیرق برق و چتر ابر نکون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کفه بی کفایت عدویش</p></div>
<div class="m2"><p>زان بود پی سپرده عر جون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا کنون سنگلاخ عمر گذشت</p></div>
<div class="m2"><p>بعد از این چیست جز عدم هامون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مردم دیده چاک پیرهن است</p></div>
<div class="m2"><p>زانکه بر طلعتش بود مفتون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وی سخن را بیان تو تاریخ</p></div>
<div class="m2"><p>وی سخارا بنان تو قانون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دست برزد ز تیغ تو دریا</p></div>
<div class="m2"><p>چار ربع زمین کند مسکون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کار قومی چراست چون زنجیر</p></div>
<div class="m2"><p>کز خلاف تو نیست محض جنون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ذوفنون اند در عداوت تو</p></div>
<div class="m2"><p>مثل است اینکه الجنون فنون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هیأتی نیست کین تو که از او</p></div>
<div class="m2"><p>چار در بند بگذرد مامون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای جهانی که بر نداری جز</p></div>
<div class="m2"><p>وی سپهری که بر نداری دون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چرخ در شربت معیشت من</p></div>
<div class="m2"><p>زهر دارد همی کند معجون</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>روزگارم بشوخ چشمی گشت</p></div>
<div class="m2"><p>چین ابرو بدو نمای که چون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خوابگاهم دم نهنگ چراست</p></div>
<div class="m2"><p>کشتی مکرمات تو مشحون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من قصب در نبسته عید گشاد</p></div>
<div class="m2"><p>رزمه ی گارگاه سقلاطون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شعر موزون من همان بهتر</p></div>
<div class="m2"><p>که رود با عطای تو موزون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>با برات سخای تو خندید</p></div>
<div class="m2"><p>بر بروت زمانه ی وارون</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اهل مدحم توئی و جز مانی</p></div>
<div class="m2"><p>خود که ماند به نقش انکلیون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نفس عیسوی همی خواهد</p></div>
<div class="m2"><p>هیأت طیر این گل مسنون</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا ز شب تیره کی فرو شوید</p></div>
<div class="m2"><p>دست مشرق بقرصه صابون</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پوش عید تو باد جامه جاه</p></div>
<div class="m2"><p>نوش در کام حاسدت افیون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دشمنان بیقرار و مضطر بند</p></div>
<div class="m2"><p>تا مرا در جناب توست سکون</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دست تهدید می برد بر ریش</p></div>
<div class="m2"><p>گر اجازت بود بگویم. کون</p></div></div>