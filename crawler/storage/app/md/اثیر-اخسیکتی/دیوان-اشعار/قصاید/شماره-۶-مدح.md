---
title: >-
    شمارهٔ ۶ - مدح
---
# شمارهٔ ۶ - مدح

<div class="b" id="bn1"><div class="m1"><p>مرزبان خطه ی اول فلک معزول باد</p></div>
<div class="m2"><p>حاش لله گر برین در گه ندارد انتما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منشی دیوان ثانی چاکر طغرای توست</p></div>
<div class="m2"><p>بر فلک زان خامه و خطش روان است و روا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطرب عشرت گه ثالت نشیند توبه کار</p></div>
<div class="m2"><p>گر نه تمکین یابد از سمع تو در ضرب ادا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسرو ملک چهارم با جهانی دار و بُرد</p></div>
<div class="m2"><p>دارد از تیغ تو تاج عزت و تخت علا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز پی حمل سلاحت گرد پنجم رزمگاه</p></div>
<div class="m2"><p>می پزد در کاسه سر عشق با ورد و دعا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاکم ایوان سادس گر سیاقت بشنود</p></div>
<div class="m2"><p>در بر اندازد ردای کحلی از صدر قضا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پناهت هندو، ماحی که هفتم بام راست</p></div>
<div class="m2"><p>مرزبانی میکند در خطه ی نشو و نما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای سعادات نطاق روشنای ثابته</p></div>
<div class="m2"><p>بوده بی عون مبارک طالعت عین شفا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طارم اطلس ز من بایست معقد بر درت</p></div>
<div class="m2"><p>منتظر تا یابد از جان داروی تیغت شفا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نفس کل در ششهزار و اند سال از بهر تو</p></div>
<div class="m2"><p>نقش های فانی انگیزد ز نیرنگ بقا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خسروا، من بنده را با سمع اعلا قصه ایست</p></div>
<div class="m2"><p>ورچه غیرت رخصه می ندهد که دارم برملا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر دم این دیک خماهن روی پرتفت اثیر</p></div>
<div class="m2"><p>نیم لختی دیکرم پیش آورد زانده، ابا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قبله از قلاد دل سازم چو هستم چاشت خوان</p></div>
<div class="m2"><p>شربت از خون جگر سازم چو باشم ناشتا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باده ی من راوقی بر راه دارد چون محن</p></div>
<div class="m2"><p>لقمه من تریقی در پیش دارد چون عنا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از طپانچه آسمانی چهرو بر وی ساخته</p></div>
<div class="m2"><p>اشک باریده شهاب ثاقب از جرم سما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تیره درگاهی است دل از آن نیارامد که شد</p></div>
<div class="m2"><p>گونه ی از درد زرد و روی او چون گهر با</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در گوای روی من بنگر برین دعوی که رفت</p></div>
<div class="m2"><p>تا نشان صدق بینی ناطق از روی گوا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سینه پر خون چودریائی است ماهی شکل دل</p></div>
<div class="m2"><p>اضطراب دل ز تأثیر حرارت آشنا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>راست خواهی، من بزندان دل تنگ خودم</p></div>
<div class="m2"><p>هم چو یونس در دل ماهی به بند ابتلا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر نبودی شاه، دیوار دل من رخنه دار</p></div>
<div class="m2"><p>کی سوی صحرای همت منفذی بودی مرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون طبیب عقل حال نبض من معلوم کرد</p></div>
<div class="m2"><p>گفت انالله این نوعی است از دارالعنا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شرم بادت از گل صد برگ خود تا کرده ی</p></div>
<div class="m2"><p>بینوا پوشیده در غنچه زنکان رها</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شهریارا مجلس انس تو بستانی است خوش</p></div>
<div class="m2"><p>دست و رخسارت سحاب جود و خورشید سحا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بنده گر زین بزم غایب میشود معذور دار</p></div>
<div class="m2"><p>بلبل از بستان بایام خزان گردد جدا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر زمستان باز میگردم زمستان برمن است</p></div>
<div class="m2"><p>عذر دانم خواست دردستان اثناء ثنا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا سر غربال تذویر زمان هر شب فتد</p></div>
<div class="m2"><p>گندم انجم در این پیروزه پیکر آسیا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قبه ی افلاک را بادا، ز ایوانت شکوه</p></div>
<div class="m2"><p>قرصه ی خورشید را بادا، ز رخسارت ضیا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از پی پاداش باد افراه جمهورامم</p></div>
<div class="m2"><p>داد، درگاه تو را گردون لقب دارالجزا</p></div></div>