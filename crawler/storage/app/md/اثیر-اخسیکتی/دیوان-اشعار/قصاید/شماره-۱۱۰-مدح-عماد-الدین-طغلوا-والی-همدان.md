---
title: >-
    شمارهٔ ۱۱۰ - مدح عماد الدین طغلوا. والی همدان
---
# شمارهٔ ۱۱۰ - مدح عماد الدین طغلوا. والی همدان

<div class="b" id="bn1"><div class="m1"><p>ای سپهری که چو خورشید، جواد آمده‌ای</p></div>
<div class="m2"><p>در دل و دیده سویدای سواد آمده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نفس تازه کند عقل به مدح تو بیاض</p></div>
<div class="m2"><p>تا تو در حیز این کهنه سواد آمده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شغل مدح تو بدان باز گذاریم که تو</p></div>
<div class="m2"><p>برتر از مرتبه کلک و مداد آمده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوهر آتش طبعی نه به ترکیب بشر</p></div>
<div class="m2"><p>زین عنا تودهٔ دون طبع رماد آمده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شرف بر شرف طارم ایوان بگذشت</p></div>
<div class="m2"><p>سقف ایوان سخن، تا تو عماد آمده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دل منهی اسرار ازل خاسته‌ای</p></div>
<div class="m2"><p>با کف ضامن ارزاق عباد آمده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیسه پرداخته شد جوهری‌فطرت را</p></div>
<div class="m2"><p>تا تو ای گوهر از هر به مراد آمده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صدف بحر ازل را چو تو یک گوهر نیست</p></div>
<div class="m2"><p>آه کاندر کف غواص کساد آمده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکتهٔ جان و خرد را تو فواید شده‌ای</p></div>
<div class="m2"><p>سینه طبع فلک را تو فواد آمده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ده زبان خواسته‌ای روز سخن سوسن‌وار</p></div>
<div class="m2"><p>که چو نرکس همه‌شب جفت سهاد آمده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای سخای تو مرا گفته سحابی که چو من</p></div>
<div class="m2"><p>در گهرباری با طبع جواد آمده‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سوی آن کل معانی رو، اگر چون دگران</p></div>
<div class="m2"><p>جزو کردار به اقدام معاد آمده‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میزبان کرمت گفت به ترجیب درای</p></div>
<div class="m2"><p>که به مهمانکدهٔ کام و مراد آمده‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جام بگسار که در مجلس سلطان شده‌ای</p></div>
<div class="m2"><p>کام بگذار که در سبع شداد آمده‌ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صاحبا، معجزهٔ نطق بدینسان که توراست</p></div>
<div class="m2"><p>ار پی جنبش انواع جماد آمده‌ای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ماه جاهی وز گردون شرف تاخته‌ای</p></div>
<div class="m2"><p>در پاکی وز دریای سواد آمده‌ای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کون ذات تو ز تأثیر فساد ایمن باد</p></div>
<div class="m2"><p>کز پی مصلحت کون و فساد آمده‌ای</p></div></div>