---
title: >-
    شمارهٔ ۷۲ - مدح سلطان قزل ارسلان
---
# شمارهٔ ۷۲ - مدح سلطان قزل ارسلان

<div class="b" id="bn1"><div class="m1"><p>زهی عنصر جوهر آفرینش</p></div>
<div class="m2"><p>توئی روح در پیکر آفرینش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تو دور پذرفت چرخ بزرگی</p></div>
<div class="m2"><p>به توسعد شد اختر آفرینش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمال مه منظر خوبرویان</p></div>
<div class="m2"><p>بدین نیلگون منظر آفرینش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه آن کرم ران است قدرت که هرگز</p></div>
<div class="m2"><p>رسد در غبارش خر آفرینش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیک خرج انعام تو بر نیاید</p></div>
<div class="m2"><p>بن کیسه ی لاغر آفرینش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیارند زو، بی نگین تو حلقه</p></div>
<div class="m2"><p>قضا و قدر بر در آفرینش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنانت چو عکس افکند نام یابد</p></div>
<div class="m2"><p>کواکب نشان محور آفرینش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جناب تو خلد است اگر خلد باقی</p></div>
<div class="m2"><p>بود زین سوی محشر آفرینش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو دست تو را هیچ دانی چه خواند</p></div>
<div class="m2"><p>فلک قلزم و اخضر آفرینش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو در نطق کوشی گهروار بیند</p></div>
<div class="m2"><p>خرد صفحه خنجر آفرینش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوی مجلس و ساغرت می نویسد</p></div>
<div class="m2"><p>جهان جنت و کوثر آفرینش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخستین ثنای تو خواندند بر وی</p></div>
<div class="m2"><p>چو نه پایه شد منبر آفرینش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به اول دعای تو گفتند در وی</p></div>
<div class="m2"><p>چو انبوه شد محضر آفرینش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان زد فروغ آفتاب جلالت</p></div>
<div class="m2"><p>که شد سوخته جوهر آفرینش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس این صحن و این سقف خود نیست چیزی</p></div>
<div class="m2"><p>بلی دود و خاکستر آفرینش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو تو نازینی نه پرورد هرگز</p></div>
<div class="m2"><p>فلک در کنار و بر آفرینش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>توئی با مدیح تو و الله و اعلم</p></div>
<div class="m2"><p>نخستین خط از دفتر آفرینش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نخستین که از مشرق مسند تو</p></div>
<div class="m2"><p>کله گوشه بر زد خور آفرینش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خرد گفت الله اکبر نزیبد</p></div>
<div class="m2"><p>سری را جز این افسر آفرینش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فلک چشم حیرت بمالید و گفتا</p></div>
<div class="m2"><p>کدام است است این نوبر آفرینش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدنباله چشم بنموده ماهش</p></div>
<div class="m2"><p>که اینک سر و سرور آفرینش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قزل ارسلان کشور آرای مغرب</p></div>
<div class="m2"><p>که شاه است بر کشور آفرینش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قضا گفت زیباست، پایندا بادا</p></div>
<div class="m2"><p>چنین سایه‌ای بر سر آفرینش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جهان داورا، شهریارا، خلافت</p></div>
<div class="m2"><p>خلافی است با داور آفرینش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سر از چنبر تو، که تا بد که زورت</p></div>
<div class="m2"><p>همی بگسلد چنبر آفرینش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عروسی است ملکت که با زیور او</p></div>
<div class="m2"><p>نیاورد سنگی زر آفرینش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنان پاک دُردانه‌ای را چه حاجت</p></div>
<div class="m2"><p>به خلخالی از زیور آفرینش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنان دان که بیرون شد آن بوم بی بر</p></div>
<div class="m2"><p>ز اقطار بوم و بر آفرینش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو خشک و تر آفرینش گرفتی</p></div>
<div class="m2"><p>ز بد گیر تا بهتر آفرینش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه باشد که یک خشک صحرا نباشد</p></div>
<div class="m2"><p>ز مجموع خشک و تر آفرینش</p></div></div>