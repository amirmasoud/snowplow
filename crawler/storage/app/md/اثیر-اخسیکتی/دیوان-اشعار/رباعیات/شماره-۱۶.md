---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>در نامه ی تو، قلم چو گردن بفراشت</p></div>
<div class="m2"><p>گفتم بنویسمت، سرشکم نگذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال دل مشتاق نه آن صورت داشت</p></div>
<div class="m2"><p>کان را بسر قلم، توانست نگاشت</p></div></div>