---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>پیراهن دل، محنت و اندوه بس است</p></div>
<div class="m2"><p>غم های بسی گرانتر، از کوه بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل تاب یک اندوه تو دارد لیکن</p></div>
<div class="m2"><p>اندوه در آن است، که اندوه بس است</p></div></div>