---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>گررشک برد فرشته برپاکی ما</p></div>
<div class="m2"><p>گردیو کند، ننگ ز بی باکی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایمان بسلامت، بدر مرگ بریم</p></div>
<div class="m2"><p>احسنت، زهی، چستی و چالاکی ما</p></div></div>