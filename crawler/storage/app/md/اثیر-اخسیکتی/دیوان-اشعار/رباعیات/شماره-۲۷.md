---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>دست تو، کزو گنج گهر می زاید</p></div>
<div class="m2"><p>بحری است، که یک دم از سخاناساید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز باد، سماع اگر بجنبد شاید</p></div>
<div class="m2"><p>کز جنبش باد، بحر در موج آید</p></div></div>