---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>امشب منم و وصال آن سرو بلند</p></div>
<div class="m2"><p>می را، ز لبش چاشنی‌ای داده به قند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شب، اگرت هزار کار است مرو</p></div>
<div class="m2"><p>وی صبح، گرت هزار شادی است مخند</p></div></div>