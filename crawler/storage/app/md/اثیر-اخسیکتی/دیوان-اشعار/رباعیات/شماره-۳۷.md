---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>امروز میی در کف و یاری در پیش</p></div>
<div class="m2"><p>دستی بزن، از حدیث فردا مندیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وآن روز، که چشم تر کنی، ای درویش</p></div>
<div class="m2"><p>در رحمت او نگر، نه در کرده خویش</p></div></div>