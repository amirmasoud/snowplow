---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>در بند تو بیوفاست دل چه توان کرد</p></div>
<div class="m2"><p>بر روی تو مبتلاست دل، چه توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اقبال دو کون عرض کردیم بر او</p></div>
<div class="m2"><p>جز محنت دل نخواست دل، چه توان کرد</p></div></div>