---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>چندانکه مجال وهم انسان باشد</p></div>
<div class="m2"><p>بر بنده، سخن گذاری آسان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با اینهمه، چون ببارگاه تو رسد</p></div>
<div class="m2"><p>ران ملخ و خوان سلیمان باشد</p></div></div>