---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>گر با من دُرد خواره داری سرمی</p></div>
<div class="m2"><p>قارون شوی امشب چو من از گوهرمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل یک تنه و لشکر غم بیعدد است</p></div>
<div class="m2"><p>لابد مددی بیابد از ساغر می</p></div></div>