---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>در رهگذر باد، چراغی که توراست</p></div>
<div class="m2"><p>ترسم که بمیرد، از فراغی که توراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی جگر سوخته عالم بگرفت</p></div>
<div class="m2"><p>گر نشنیدی زهی، دماغی که توراست</p></div></div>