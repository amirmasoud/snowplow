---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>حاشا که ز دل، مهر تو آسان برود</p></div>
<div class="m2"><p>وان عشق گران خریده، ارزان برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای از بر من نرفته، مهر تو مرا</p></div>
<div class="m2"><p>با شیر فرو شده ست، با جان برود</p></div></div>