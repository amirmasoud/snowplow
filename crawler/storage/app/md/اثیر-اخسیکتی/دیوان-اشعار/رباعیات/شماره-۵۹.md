---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>گه طعمه‌ مور، اژدهایی سازی</p></div>
<div class="m2"><p>گه از پر پشه‌ای، همایی سازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هم شکنی کاسه صد کسرا را</p></div>
<div class="m2"><p>تا دسته کوزه گدایی سازی</p></div></div>