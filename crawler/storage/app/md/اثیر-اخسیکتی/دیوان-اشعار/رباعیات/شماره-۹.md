---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>بر عرض تو، گر عارضه ئی چیرشدست</p></div>
<div class="m2"><p>صحت برسد کنون، که بس دیرشدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو جان جهانی و جهان زنده بتو</p></div>
<div class="m2"><p>آخر نه جهان، ز جان خود سیر شدست</p></div></div>