---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ایزد، دلکی مهر فزایت بدهاد</p></div>
<div class="m2"><p>به زین نظری باین گدایت بدهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوبی و خوشی و دلفریبی و جمال</p></div>
<div class="m2"><p>داری همه، جزوفا، خدایت بدهاد</p></div></div>