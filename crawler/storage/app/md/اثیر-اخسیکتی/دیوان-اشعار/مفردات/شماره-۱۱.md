---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>از زلف تو صد هزار منزل</p></div>
<div class="m2"><p>تا روی تو وسمه خطرناک</p></div></div>