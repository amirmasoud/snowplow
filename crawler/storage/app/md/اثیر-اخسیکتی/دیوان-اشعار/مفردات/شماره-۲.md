---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>در انتظار عراقی لطایف خوش تو</p></div>
<div class="m2"><p>بسا لطایف رازی که داد غصه مرا</p></div></div>