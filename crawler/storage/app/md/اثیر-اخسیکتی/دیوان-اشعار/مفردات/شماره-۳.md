---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>از کف ترکی چو ماه باده خور و باده خواه</p></div>
<div class="m2"><p>چشمهٔ لب بی گیاه گوشهٔ خور بی حجاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان بستاند ز دل جزع وی اندر جفا</p></div>
<div class="m2"><p>دل برباید ز جان لعل وی اندر عتاب</p></div></div>