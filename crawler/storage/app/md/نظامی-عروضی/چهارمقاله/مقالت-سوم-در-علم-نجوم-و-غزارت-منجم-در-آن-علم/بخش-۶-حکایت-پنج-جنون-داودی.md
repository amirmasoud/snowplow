---
title: >-
    بخش ۶ - حکایت پنج - جنون داودی
---
# بخش ۶ - حکایت پنج - جنون داودی

<div class="n" id="bn1"><p>محمود داودی پسر ابوالقاسم داودی عظیم معتوه بود بلکه مجنون و از علم نجوم بیشتر حظی نداشت و از اعمال نجوم مولودگری دانستی و در مقومیش اشکال بود که هست یا نه و خدمت امیرداد ابوبکر بن مسعود کردی به پنج دیه، اما احکام او بیشتر قریب صواب بودی.</p></div>
<div class="n" id="bn2"><p>و در دیوانگی تا به درجه‌ای بود که خداوند من ملک الجبال امیرداد را جفتی سگ غوری فرستاده بود سخت بزرگ و مهیب. او به اختیار خویش با آن هر دو سگ جنگ کرد و از ایشان به سلامت بجست.</p></div>
<div class="n" id="bn3"><p>و بعد از آن به سالها در هری به بازار عطاران بر دکان مقری حداد طبیب با جماعتی از اهل فضل نشسته بودیم و از هر جنس سخن همی رفت. مگر بر لفظ یکی از آن افاضل برفت که: «بزرگ مردا که ابوعلی سینا بوده است.»</p></div>
<div class="n" id="bn4"><p>او را دیدم که در خشم شد و رگهای گردن از جای برخاست و ستبر شد و همه امارات غضب بر وی پدید آمد و گفت:</p></div>
<div class="n" id="bn5"><p>«ای فلان! بوعلی سینا که بوده است؟ من هزار چندان بوعلى ام که هرگز بوعلى با گربه جنگ نکرد، من در پیش امیرداد با دو سگ غوری جنگ کردم.»</p></div>
<div class="n" id="bn6"><p>مرا آن روز معلوم گشت که او دیوانه است. اما با این دیوانگی دیدم که در سنهٔ خمس و خمسمایة که سلطان سنجر به دشت خوزان فرود آمد و روی به ماوراء النهر داشت به حرب محمد خان، امیرداد سلطان را در پنجده میزبانی کرد عظیم شگرف. روز سوم به کنار رود آمد و در کشتی نشست و نشاط شکار ماهی کرد و در کشتی داودی را پیش خواند تا از آن جنس سخن دیوانگانه همی گفت و او همی خندید و امیرداد را صریح دشنام دادی. یکباری سلطان داودی را گفت:</p></div>
<div class="n" id="bn7"><p>«حکم کن که این ماهی که این بار بگیرم به چند من بود؟»</p></div>
<div class="n" id="bn8"><p>گفت:</p></div>
<div class="n" id="bn9"><p>«شست برکش.»</p></div>
<div class="n" id="bn10"><p>سلطان شست برکشید. او ارتفاع بگرفت و ساعتی بایستاد و گفت:</p></div>
<div class="n" id="bn11"><p>«اکنون در انداز.»</p></div>
<div class="n" id="bn12"><p>سلطان شست درانداخت. گفت:</p></div>
<div class="n" id="bn13"><p>«حکم می‌کنم که این که بر کشی پنج من بود.»</p></div>
<div class="n" id="bn14"><p>امیرداد گفت:</p></div>
<div class="n" id="bn15"><p>«ای ناجوانمرد! در این رود ماهی پنج منی از کجا باشد؟»</p></div>
<div class="n" id="bn16"><p>داودی گفت:</p></div>
<div class="n" id="bn17"><p>«خاموش باش! تو چه دانی؟»</p></div>
<div class="n" id="bn18"><p>میرداد خاموش شد. ترسید که اگر استقصا کند دشنام دهد. چون ساعتی بود شست گران شد و امارات آن که صیدی در افتاده است ظاهر شد. سلطان شست برکشید. ماهی سخت بزرگ در افتاده بود چنان که برکشیدند شش من بود. همه در تعجب بماندند و سلطان شگفتیها نمود و الحق جای شگفتی بود. گفت:</p></div>
<div class="n" id="bn19"><p>«داودی چه خواهی؟»</p></div>
<div class="n" id="bn20"><p>خدمت کرد و گفت:</p></div>
<div class="n" id="bn21"><p>«ای پادشاه روی زمین! جوشنی خواهم و سپری و نیزه‌ای تا با باوردی جنگ کنم.»</p></div>
<div class="n" id="bn22"><p>و این باوردی سرهنگی بود ملازم در سرای امیرداد و داودی را با وی تعصب بود. به سبب لقب که او را شجاع الملک همی نوشتند و داودی را شجاع الحکماء و داودی مضایقت همی کرد که او را چرا شجاع می نویسند و آن را امیرداد بدانسته بود و پیوسته داودی را با او در انداختی و آن مرد مسلمان در دست او درمانده بود.</p></div>
<div class="n" id="bn23"><p>فی الجمله در دیوانگی محمود داودی هیچ اشکالی نبود و این فصل بدان آوردم تا پادشاه را معلوم باشد که در احکام نجومی جنون و عته از شرائط آن باب است.</p></div>