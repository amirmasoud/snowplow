---
title: >-
    شمارهٔ ۷ - این بقیه ترکیب بند در مرثیه اشجع الشعرا یولقلی بیک واقع است که روز ماتم ولد دلبندم خبر موت او رسید
---
# شمارهٔ ۷ - این بقیه ترکیب بند در مرثیه اشجع الشعرا یولقلی بیک واقع است که روز ماتم ولد دلبندم خبر موت او رسید

<div class="b" id="bn1"><div class="m1"><p>این درد بین که از پی هم ناگهان رسید</p></div>
<div class="m2"><p>عضوی شکست از تن و زخمی بر آن رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جای رفت زورق بی بادبان صبر</p></div>
<div class="m2"><p>موجی نرفته موج دگر از کران رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>واحسرتا که از قدراندازی فلک</p></div>
<div class="m2"><p>بر دل دو زخم کاریم از یک کمان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد به مغز مردمکم سهم اولین</p></div>
<div class="m2"><p>بگذشت سهم دیگر و بر استخوان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل را نماند روی تلافی ز روزگار</p></div>
<div class="m2"><p>جوری ندیده ام که به دادم توان رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نتوان به عمر نوح و خضر بر کران نهاد</p></div>
<div class="m2"><p>باری که از مصیبت چرخم به جان رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ممنون شدم ز عمر که پیرانه سر مرا</p></div>
<div class="m2"><p>طفلی پی سرور ز بخت جوان رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد خاطرم شکفته که کاری شگفت شد</p></div>
<div class="m2"><p>نخل مرا شکوفه به فصل خزان رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماه نوی ز مغرب طالع طلوع کرد</p></div>
<div class="m2"><p>زیب قبیله و شرف خاندان رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بودم ازین طرب مترنم که ناگهان</p></div>
<div class="m2"><p>از خاصگان خانه به گوشم فغان رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم خروش چیست؟ که خادم دوید و گفت</p></div>
<div class="m2"><p>مرگ فلان و نامه موت فلان رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فریاد ازین دورنگی گیتی که خلق را</p></div>
<div class="m2"><p>حرمانش با مراد عنان بر عنان رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک سور کردم و به دو ماتم شدم اسیر</p></div>
<div class="m2"><p>شادیم فرو آمد و غم توأمان رسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گشتم ملول و تلخ مزاج از بنات خویش</p></div>
<div class="m2"><p>نوشم به حلق و زهر به کام و دهان رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن قاصدی که بر سر کوی از در سرا</p></div>
<div class="m2"><p>بهر بشارت خلفم شادمان رسید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در صحن برزن از پی اعلام تعزیت</p></div>
<div class="m2"><p>روز عزای خلف و صدیقم همان رسید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صد غصه در برابر یک ذوق دیده ام</p></div>
<div class="m2"><p>نتوان درین جهان به خوشی رایگان رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر مرد پایه پایه شود بی خطا بلند</p></div>
<div class="m2"><p>بتوان به نردبان به سر آسمان رسید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن را که جذب حق پی تکمیل برکشید</p></div>
<div class="m2"><p>علمش ورای رفعت وهم و گمان رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وآن را که بی عنایتی حق فرو گذاشت</p></div>
<div class="m2"><p>از فرق فرقدان به ته خاکدان رسید</p></div></div>
<div class="b2" id="bn21"><p>من باری از زمانه مرادی نیافتم</p>
<p>با صد هزار عقده گشادی نیافتم</p></div>
<div class="b" id="bn22"><div class="m1"><p>غم داشت باغبان که گل و یاسمن چه شد؟</p></div>
<div class="m2"><p>گل جامه می درید که مرغ چمن چه شد؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خاطر ز فوت نافه آهو رمیده بود</p></div>
<div class="m2"><p>آمد فغان که طرفه غزال ختن چه شد؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دل بود از مصیبت گجرات مویه گر</p></div>
<div class="m2"><p>غافل که از جفای قضا بردکن چه شود؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دوران یولقلی انیسی به سر رسید</p></div>
<div class="m2"><p>آن رستم مصاف و مسیح سخن چه شد؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دستان سرای خسرو و شیرین خموش شد</p></div>
<div class="m2"><p>ظاهر نشد که عاقبت کوه کن چه شد؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون نظم او ستاره افلاک در همند</p></div>
<div class="m2"><p>آن ناظم جواهر نعش و پرن چه شد؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از جعد فکر چهره معنی مشوش است</p></div>
<div class="m2"><p>عقده گشای یوسف مشگین رسن چه شد؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پوشیده گشت پایه مقدور هر کسی</p></div>
<div class="m2"><p>انجم شناس طالع هر انجمن چه شد؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ایرج سپه ز هند به خوارزم می کشد</p></div>
<div class="m2"><p>آن ترک تیز حمله شمشیرزن چه شد؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>داراب از دکن بحبش تاخت می برد</p></div>
<div class="m2"><p>آن پیش تاز رخش به دریافکن چه شد؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جان در وفا سپرد که سالار مملکت</p></div>
<div class="m2"><p>گوید: دریغ ترک وفادار من چه شد؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بی صاحب سخن، سخن افتاد دربدر</p></div>
<div class="m2"><p>درها یتیم شد همه بحر عدن چه شد؟</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بوی بشیر مصر به کنعان نمی رسد</p></div>
<div class="m2"><p>مفتاح شادی در بیت الحزن چه شد؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این نونهال ها ثمر خام می دهند</p></div>
<div class="m2"><p>دل تشنه ایم میوه نخل کهن چه شد؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>این بلبلان ثمر خام می دهند</p></div>
<div class="m2"><p>دل تشنه ایم میوه نخل کهن چه شد؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>این بلبلان بخرده دینار می پرند</p></div>
<div class="m2"><p>مرغی که می فشاند شکر از دهن چه شد؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>معنی به لفظ روشنشان کرم پیله است</p></div>
<div class="m2"><p>آن شبچراغ در دل شب نور تن چه شد؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یک کس به رنگ مهر سلیمان نگین نیافت</p></div>
<div class="m2"><p>درحیرتم که کان عقیق یمن چه شد؟</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دفتر سیه ز شعر وی و دیده روشنست</p></div>
<div class="m2"><p>آن خامه و دوات چو شمع و لگن چه شد؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نطقی که بود واسطه عقل و روح کو؟</p></div>
<div class="m2"><p>نظمی که بود رابطه جان و تن چه شد؟</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>طور هزار موسی تورات خوان کجاست؟</p></div>
<div class="m2"><p>مصر هزار یوسف گل پیرهن چه شد؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آن گوهری که خورد زمینش ز مهر کو؟</p></div>
<div class="m2"><p>آن خاتمی که برد فرو اهرمن چه شد؟</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فریادرس مجو که درین دشت کربلا</p></div>
<div class="m2"><p>پرسش نشد که خون حسین و حسن چه شد؟</p></div></div>
<div class="b2" id="bn45"><p>وااندها انیس دل دوستان نماند</p>
<p>عیشی که داشت سر گل و بوستان نماند</p></div>
<div class="b" id="bn46"><div class="m1"><p>بی جبرئیل رفته به معراج شاعری</p></div>
<div class="m2"><p>بر قوم خویش یافته فضل پیمبری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از لطف طبع راز ملک گفته با ملک</p></div>
<div class="m2"><p>از حسن نظم عقد پری بسته با پری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>افتاده از دو مصرع منقوش او به تاب</p></div>
<div class="m2"><p>بر صفحه جمال بتان زلف عنبری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کرده بسیج راه چنان از ریاض خاک</p></div>
<div class="m2"><p>چون باد صبح جان شده از روح پروری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خندان گرفته تحفه جنت ز دست حور</p></div>
<div class="m2"><p>صورت به جان فشانی و معنی به دلبری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>رضوان به ره ستاده ز انفاس طبع او</p></div>
<div class="m2"><p>پیچیده در مشام نسیم معنبری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>او در هوای نعره «طوبی لهم » به تاب</p></div>
<div class="m2"><p>از شوق قامتش دل طوبی صنوبری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فردوس سوی او نگران با هزار چشم</p></div>
<div class="m2"><p>کز خواب ناز باز کند چشم عبهری</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گر پرده از عروس ضمیرش برافکنند</p></div>
<div class="m2"><p>غلمان غلامیش کند و حور چاکری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ور حور از عذوبت لفظش خبر دهد</p></div>
<div class="m2"><p>کوثر کند نثار لبش طبع کوثری</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ایام خط به دفتر فضل و هنر کشید</p></div>
<div class="m2"><p>ای نامه رخ سیه کن و ای خامه خون گری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زآشوب رستخیز گر اینجا مثل زنم</p></div>
<div class="m2"><p>حرفیست سرزبانی و شوریست سرسری</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>می خواست پی به گوهر مقصود خود برد</p></div>
<div class="m2"><p>در بحر شعر رفت فرو از شناوری</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گر بی خبر ز گفته خود شد عجب مدان</p></div>
<div class="m2"><p>معنیش بادگی کند و لفظ ساغری</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از علم و فضل بود گران بر زمین فکند</p></div>
<div class="m2"><p>طبعش ز فربهی و جهانش ز لاغری</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از تیغ بت شکن شده از نظم بت نگار</p></div>
<div class="m2"><p>کرده به دست و خامه خلیلی و آزری</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>برعکس از تهور و تدبیر کرده است</p></div>
<div class="m2"><p>در صف سخن شکافی و در بزم صفدری</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر دفتر کواکب و افلاک طی کنند</p></div>
<div class="m2"><p>هر حرف او کند فلکی، نقطه اختری</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>از نظم او که شهرت محمود داده است</p></div>
<div class="m2"><p>گم گشته نام فرخی و ذکر عنصری</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پرسی اگر به حشر چه آرم ندا رسد</p></div>
<div class="m2"><p>شعر انیسی آور و وحی پیمبری</p></div></div>
<div class="b2" id="bn66"><p>دیگر کسی نماند «نظیری » برابرت</p>
<p>عکس تو بود مرثیه گو شد ثناگرت</p></div>
<div class="b" id="bn67"><div class="m1"><p>ای از صفای دل همه نور و صفا شده</p></div>
<div class="m2"><p>برتر ز آفرینش ارض و سما شده</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گلبانگ حمد برده به کروبیان عرش</p></div>
<div class="m2"><p>از کبریای حق همه تن کبریا شده</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>در خدمت ملک به ملک گشته همنشین</p></div>
<div class="m2"><p>از سایه خدا سوی نور خدا شده</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>اول چو شیر صرف به اعضا درآمده</p></div>
<div class="m2"><p>آخر چو زبده از همه اخوان جدا شده</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>اندیشه تا به سرحد تحقیق برده پی</p></div>
<div class="m2"><p>از نور عقل بر اثر انبیا شده</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بر آسمان ذهن و ذکا کرده اختری</p></div>
<div class="m2"><p>هر گل که در حدیقه طبع تو واشده</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>جز تو که در فصاحت لفظت نظیر نیست</p></div>
<div class="m2"><p>کم ممکنی به طرز قدیم آشنا شده</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>از اعتقاد ثابت و نعت فصیح خویش</p></div>
<div class="m2"><p>حسان ثابت حرم مصطفی شده</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هرگه پی بیان در خلوت گشوده ای</p></div>
<div class="m2"><p>شاهان ستاده بر سر کویت گدا شده</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>خندان نموده گلشن احسان به آب نظم</p></div>
<div class="m2"><p>اول سحاب بوده و آخر صبا شده</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ایزد جزای خیر تو را بر جنان نوشت</p></div>
<div class="m2"><p>کو نیز بوده از فقرا ز اغنیا شده</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>خوش رو که عاریت به عناصر سپرده ای</p></div>
<div class="m2"><p>جوهر به جای مانده عوارض هبا شده</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>در حبس تن ز شدت کربی که دیده ای</p></div>
<div class="m2"><p>گردیده آبت آتش و آتش هوا شده</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>از نقش رسته ای و به معنی رسیده ای</p></div>
<div class="m2"><p>حاصل تو را بقای ابد از فنا شده</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چون پیرهن که از در کنعان درآورند</p></div>
<div class="m2"><p>رضوان خلد را کفنت توتیا شده</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چون لوح علم کل که همه حسن ها ازوست</p></div>
<div class="m2"><p>خاک از طراوت تو به نشو و نما شده</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>تا تو به خاک خفته ای ای چشم مردمی</p></div>
<div class="m2"><p>مردم گیا به خاک دکن کیمیا شده</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تو بسته لب به مرگ کرم از ثنا و من</p></div>
<div class="m2"><p>در ماتم تو مرثیه گوی ثنا شده</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>از بهر آن به مرثیه تو ثنای من</p></div>
<div class="m2"><p>خالص شده که بی طمع و بی ریا شده</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>از قبله سخن نکنم روی بر قفا</p></div>
<div class="m2"><p>کو کرده اقتدا به تو و مقتدا شده</p></div></div>
<div class="b2" id="bn87"><p>تو رفته ای و کار مرا بر سر آمده</p>
<p>فکری کنم که کار سخن ابتر آمده</p></div>
<div class="b" id="bn88"><div class="m1"><p>بس خوب یافتی و سخن کم گذاشتی</p></div>
<div class="m2"><p>شوری ز لعل خویش به عالم گذاشتی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>مشرف شدی به لوح قضا وز ضیای طبع</p></div>
<div class="m2"><p>خورشید را به عیسی مریم گذاشتی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>سیراب گردد از نم کلک تو عالمی</p></div>
<div class="m2"><p>بحری درون قطره شبنم گذاشتی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>طرز تو کهنه تا به قیامت نمی شود</p></div>
<div class="m2"><p>آیین درست و قاعده محکم گذاشتی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>گر غم گذاشتی بدل اما ز درج نطق</p></div>
<div class="m2"><p>نیکو مفرحی جهت غم گذاشتی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>حق باد ساقیت که ز راح خیال خویش</p></div>
<div class="m2"><p>عیش مدام و جام دمادم گذاشتی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>جاوید باد نام تو کز گوهر نتاج</p></div>
<div class="m2"><p>بس فخر در قبیله آدم گذاشتی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>جمشید عصر بودی از ابداع طبع خویش</p></div>
<div class="m2"><p>چندین سواد شهر معظم گذاشتی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>در شش جهت چو کوس سخن می زدی چرا</p></div>
<div class="m2"><p>ملکی که بود بر تو مسلم گذاشتی؟</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چون بی تو کار عیش فراهم نمی شود</p></div>
<div class="m2"><p>اوراق نظم بهر چه درهم گذاشتی؟</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>آن رایتی که ملک فریدون بهاش بود</p></div>
<div class="m2"><p>بردی و داغ بر جگر جم گذاشتی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>آن گوهری که ملک سلیمان چو او نبود</p></div>
<div class="m2"><p>پذرفتی و دریغ به خاتم گذاشتی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>از کار خویش بر سر خاقان روزگار</p></div>
<div class="m2"><p>بهر علامت افسر معلم گذاشتی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>نوروز و عید را به الم ساختی اسیر</p></div>
<div class="m2"><p>بر سال نام ماه محرم گذاشتی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ما را که از عزای تو آرد برون؟ که تو</p></div>
<div class="m2"><p>افلاک را به کسوت ماتم گذاشتی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>از سبزه زار چرخ نچیدی گلی به کام</p></div>
<div class="m2"><p>همچون بنفشه پشت فلک خم گذاشتی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>کوس دهن دریده نهادی به گوشه ای</p></div>
<div class="m2"><p>کلک زبان بریده ابکم گذاشتی</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>تنها نسوختی جگر پاره طبیب</p></div>
<div class="m2"><p>صد داغ دل به دارو و مرهم گذاشتی</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>از برکت تو فیض به آفاق می رسد</p></div>
<div class="m2"><p>صد حیف خاک بر سر زمزم گذاشتی</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>شاید که چون مسیح لبت زندگی دهد</p></div>
<div class="m2"><p>رخ بر رکاب خسرو اعظم گذاشتی</p></div></div>
<div class="b2" id="bn108"><p>تا خاک باد جای تو خلد نعیم باد</p>
<p>سند و دکن ز ایرج و عبدالرحیم باد</p></div>