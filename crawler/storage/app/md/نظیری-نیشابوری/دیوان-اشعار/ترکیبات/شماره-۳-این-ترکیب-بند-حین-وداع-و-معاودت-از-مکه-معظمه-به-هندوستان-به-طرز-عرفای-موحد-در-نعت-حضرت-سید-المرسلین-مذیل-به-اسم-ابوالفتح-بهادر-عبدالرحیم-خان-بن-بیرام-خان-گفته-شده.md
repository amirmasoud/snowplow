---
title: >-
    شمارهٔ ۳ - این ترکیب بند حین وداع و معاودت از مکه معظمه به هندوستان به طرز عرفای موحد در نعت حضرت سید المرسلین مذیل به اسم ابوالفتح بهادر عبدالرحیم خان بن بیرام خان گفته شده
---
# شمارهٔ ۳ - این ترکیب بند حین وداع و معاودت از مکه معظمه به هندوستان به طرز عرفای موحد در نعت حضرت سید المرسلین مذیل به اسم ابوالفتح بهادر عبدالرحیم خان بن بیرام خان گفته شده

<div class="b" id="bn1"><div class="m1"><p>شب گلابی بر رخم خوابم ز چشم تر زدند</p></div>
<div class="m2"><p>از سجود درگه عشقم گلی بر سر زدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول شب بانگ نوشانوشم از ذرات خاست</p></div>
<div class="m2"><p>که ندای الصلوة آمد همه ساغر زدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قبله کردم قصد در چشمم در تارسا نمود</p></div>
<div class="m2"><p>کعبه بستم نقش بر رویم بت آزر زدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نم میزاب و تار سبحه حاجت خواستم</p></div>
<div class="m2"><p>قرعه بر شط شراب و بر خم کافر زدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردن سرخ صراطی حلق قربانی نمود</p></div>
<div class="m2"><p>کز شراب شعله بارش بر گلو خنجر زدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرهم از آب و گل دیر مغان می ساختند</p></div>
<div class="m2"><p>هرکه از خار مغیلانش به پا نشتر زدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر شدم مجنون ز حرفم داستان ها ساختند</p></div>
<div class="m2"><p>ور شدم منصور دارم بر سر منبر زدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خرابات محبت یافت هرکس هرچه یافت</p></div>
<div class="m2"><p>کعبه را هم حلقه ای پی گم کنان بر در زدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بولهب از کعبه، ابراهیم از بتخانه خاست</p></div>
<div class="m2"><p>واژگون نعلیست هرجا گونه دیگر زدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرع شارع بهر عامست ارنه اهل عشق را</p></div>
<div class="m2"><p>جذبه چون گردید غالب دوش بر رهبر زدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غیرعاشق نیست کس را ره به معراج وصال</p></div>
<div class="m2"><p>جبرئیلش را گره در راه بر شهپر زدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آب خضر و جام اسکندر به پشت پا زدیم</p></div>
<div class="m2"><p>خیمه ما بر کنار چشمه کوثر زدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر کجا رفتم بدوش روزگارم بار بود</p></div>
<div class="m2"><p>کعبه را محمل کجا بر ناقه لاغر زدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر کشم از مکه سر، ترسانم از کردار خویش</p></div>
<div class="m2"><p>طایرانش سنگ عبرت پیل را بر سر زدند</p></div></div>
<div class="b2" id="bn15"><p>کعبه است اینجا ملک حیران کار افتاده است</p>
<p>آسمان را در گل این خانه بار افتاده است</p></div>
<div class="b" id="bn16"><div class="m1"><p>دیده ام را از جمال کعبه بینا کرده اند</p></div>
<div class="m2"><p>توشه راه خراباتم مهیا کرده اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خوش تماشاییست گبری سجده می آرد به دیر</p></div>
<div class="m2"><p>دامن عرش و نقاب کعبه بالا کرده اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برهمن گویا همی سوزد که هر سو در منا</p></div>
<div class="m2"><p>آتشی از خون بسمل بر سر پا کرده اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آتشین پایی ز وادی می رسد کاندر حرم</p></div>
<div class="m2"><p>ریگ ها را سایه پرورد مصلا کرده اند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از گل و آبش فرح می بارد این آن خانه است</p></div>
<div class="m2"><p>کش خضر سقا و ابراهیم بنا کرده اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشئه می سازند رنگین نغمه می سازند خوش</p></div>
<div class="m2"><p>آتش قندیل و آب سبحه یک جا کرده اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بوسه بر سنگ سیاه او به گستاخی مزن</p></div>
<div class="m2"><p>مردمان دیده را زین سرمه بینا کرده اند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یوسفان را بر سر چاهش سبو بشکسته اند</p></div>
<div class="m2"><p>حوریان را در ره وادیش سودا کرده اند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هم ازین جنسست گر بت را سجود آورده اند</p></div>
<div class="m2"><p>هم به این نقش است گر وصف چلیپا آورده اند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کیش و مذهب را زبان دان گر شوی معنی یکی است</p></div>
<div class="m2"><p>مصحف و انجیل را از هم مجزا کرده اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قتل اسماعیل رمزی بود این افشاگران</p></div>
<div class="m2"><p>لوح صحرا را به خون کشته انشا کرده اند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زاهد و فاسق به وسعت گنجد آنجایی که اوست</p></div>
<div class="m2"><p>این فقیهان راه حق را تنگ بر ما کرده اند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کعبه را مستانه لبیک آرم از میقات عشق</p></div>
<div class="m2"><p>کز الستم هم به این لبیک گویا کرده اند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عشق می بگرفته پا و سر کبابم سوخته</p></div>
<div class="m2"><p>آتش این هیمه را بسیار گیرا کرده اند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عشقم از کوبی برون آورده از بس تنگیش</p></div>
<div class="m2"><p>کعبه را گه قبله گاهی دیر ترسا کرده اند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مستیم تا پیشگاهی برده از بس وسعتش</p></div>
<div class="m2"><p>خاک عقبا بر سر مشغول دنیا کرده اند</p></div></div>
<div class="b2" id="bn32"><p>بر سر هر چشمه خالی صد سبو می کرده ام</p>
<p>خضر گم کردست راهی را که من طی کرده ام</p></div>
<div class="b" id="bn33"><div class="m1"><p>این قدر دانم که با نظاره چشمم آشناست</p></div>
<div class="m2"><p>آن که حیران رخ اویم نمی دانم کجاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پای تا سر محو در نظاره گشتم همچو شمع</p></div>
<div class="m2"><p>درنظر افزود چندانی که از جسمم بکاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سیل دیدار آمد و خاشاک هستی پاک برد</p></div>
<div class="m2"><p>این که اکنون غوطه در وی می خورم بحر فناست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خواب ازان آشفته تر دیدم که تعبیرش کنی</p></div>
<div class="m2"><p>برنمی آرد قیامت سر ازین شوری که خاست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جمله اجزای وجودم را منور ساخت عشق</p></div>
<div class="m2"><p>سایه پیش آفتاب و مس به نزد کیمیاست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دارم از اقبال عشق اندیشه آزادگی</p></div>
<div class="m2"><p>گر هوایی در سر سروست از باد صباست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر سر مرغان وادی گل فشانی می کنم</p></div>
<div class="m2"><p>کز سرشگم در کف پا خار در نشو و نماست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در قیامت خون بهای دیده گریان من</p></div>
<div class="m2"><p>دستگاه روز بازار شهیدان مناست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ای صبا خیز و کف خاکی دگر زان کو بیار</p></div>
<div class="m2"><p>نور شد در دیده آن گردی که گفتی توتیاست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>قطع گفتن کن که خاموشی درین صف واعظست</p></div>
<div class="m2"><p>ترک دانش کن که نادانی درین ره مقتداست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا به صدر آشنایی حیرت اندر حیرتست</p></div>
<div class="m2"><p>دیده ای وا کن که بینایی درین ره پیشواست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از سر اخلاص پا بردار مقصد در دلست</p></div>
<div class="m2"><p>از حضور دل زبان بگشا اجابت در دعاست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>طرف و سعی حاجیان اظهار شوقی بیش نیست</p></div>
<div class="m2"><p>آن که من می جویمش نی در حرم نی در صفاست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از جهان چندش که جستم هیچ بانگی برنخاست</p></div>
<div class="m2"><p>خم که در میخانه پر گردید از می بی صداست</p></div></div>
<div class="b2" id="bn47"><p>خیر بادی کعبه را گفتم که سنگ راه بود</p>
<p>پی به دل بردم که راهش سوی آن درگاه بود</p></div>
<div class="b" id="bn48"><div class="m1"><p>گوشه ای خفتم که راهم را سر و پایان نبود</p></div>
<div class="m2"><p>لنگر افکندم که کشتی در خور طوفان نبود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرغ بینش را شکستم پر که طیران کند داشت</p></div>
<div class="m2"><p>رخت دانش را بریدم پی کزین میدان نبود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سر به سر بازار حکمت کور دیدم خلق را</p></div>
<div class="m2"><p>توتیای حق شناسی در همه دکان نبود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شیشه بر صد که شکستم باده موسی نداشت</p></div>
<div class="m2"><p>غوطه در صد چشمه خوردم چشمه حیوان نبود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اهل معنی را ز صحبت سبزه ای از گل نرست</p></div>
<div class="m2"><p>قوم وادی را ز عرفان تره ای بر خوان نبود</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دیده یعقوب بر دیوار و در وا شد دریغ</p></div>
<div class="m2"><p>غیر بوی پیرهن در کلبه احزان نبود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دل به حسرت بر در از نظاره مجلس گداخت</p></div>
<div class="m2"><p>جان به درگه سوخت کش زین بیشتر فرمان نبود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا نگه کردم عنان برتافت کز یک جلوه اش</p></div>
<div class="m2"><p>پاره پاره دل چو طور موسی عمران نبود</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زخم زد اما به جولانی ز خاکم بر نداشت</p></div>
<div class="m2"><p>کاین چنین گو در خور آن دست و آن چوگان نبود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خون ما در گردن بی باک عشق پرده در</p></div>
<div class="m2"><p>حسن تا در ره بود این فتنه در دوران نبود</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در بن هر خار صد لیلی است از دیدار او</p></div>
<div class="m2"><p>وادیی دیدی که مجنونی درو حیران نبود</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>این حجاب از بود ما باشد وگرنه پیش ازین</p></div>
<div class="m2"><p>برقع صورت به پیش چهره جانان نبود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>حسن پرتو بر جهان افکند ورنه پیش ازین</p></div>
<div class="m2"><p>ذره دل آشفته و پروانه سرگردان نبود</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پرده از عالم برافتد گر برآید آشکار</p></div>
<div class="m2"><p>ما عدم بودیم آن روزی که او پنهان نبود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>برنتابد فر حق جز کبریای احمدی</p></div>
<div class="m2"><p>غیر یک دل در دو عالم قابل جولان نبود</p></div></div>
<div class="b2" id="bn63"><p>احمد مرسل که باطن مشرق انوار داشت</p>
<p>دوست را آیینه بر اندازه دیدار داشت</p></div>
<div class="b" id="bn64"><div class="m1"><p>تا زمین شد مولد و مأوای خیرالمرسلین</p></div>
<div class="m2"><p>صد شرف در منزلت بر آسمان دارد زمین</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>این جهان در علم او شاخ گیا در بوستان</p></div>
<div class="m2"><p>وین فلک با فضل او بال مگس در انگبین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>طور صد موسی برانگیزد ز خاک آستان</p></div>
<div class="m2"><p>شمع صد عیسی برافروزد به باد آستین</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آب درجو داشت آن فصلی که عالم بود خاک</p></div>
<div class="m2"><p>دست در گل داشت آن روزی که آدم بود طین</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شکل اول را چو کلک آفرینش نقش بست</p></div>
<div class="m2"><p>زو جواز آفرین می خواست صورت آفرین</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>صنع را مشاطه کل علم را آیینه دار</p></div>
<div class="m2"><p>در بر و پهلوی آدم دیده حوا را جنین</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ذیل قدرش چهره آرا بود از اول خاک را</p></div>
<div class="m2"><p>گر نبودی سجده او موی رستی از جبین</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گر نگرداند به آیین شریعت چرخ را</p></div>
<div class="m2"><p>پنبه گردد باز تار و پود ایام و سنین</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نزد عقل من ز تصدیق نبوت برتر است</p></div>
<div class="m2"><p>رسم او ما راست مذهب کار او ما راست دین</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>منزلت بنگر که اقرار به او ایمان ماست</p></div>
<div class="m2"><p>خصم اگر گوید کلام اوست قرآن مبین</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گل نگار از جلوه اش فرش رخ خلد برین</p></div>
<div class="m2"><p>عطرروب از روضه اش جاروب زلف حور عین</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>صورت شق القمر بر چرخ می دانی چه بود؟</p></div>
<div class="m2"><p>خاتمی می کرد در انگشت بشکستش نگین</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گر نیفتد سایه اش بر خاک چندان دور نیست</p></div>
<div class="m2"><p>بی مکان را هم مکان شد بی نشان را هم نشین</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چون سبق کز طفل ماند ماند ازو لوح و قلم</p></div>
<div class="m2"><p>چون قفس کز مرغ ماند ماند ازو عرش برین</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گر به یک دم طی کند هفت آسمان نبود عجب</p></div>
<div class="m2"><p>جبرئیلش در رکابست و براقش زیر زین</p></div></div>
<div class="b2" id="bn79"><p>دیده اش از سرمه «مازاغ » روشن کرده اند</p>
<p>منزلش در «لانبی بعدی » معین کرده اند</p></div>
<div class="b" id="bn80"><div class="m1"><p>مطرب مستم ز خلوتگاه سلطان آمده</p></div>
<div class="m2"><p>سرخوش از احسان شده با خود به الحان آمده</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>شعله گردم ولی از خار وادی خاسته</p></div>
<div class="m2"><p>سوخته ابرم ولی بر کعبه گریان آمده</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>وادی از دنبال من گه بر کتف بگریسته</p></div>
<div class="m2"><p>کعبه استقبال من زمزم به دامان آمده</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ما به جانان بسته ایم احرام از میقات عشق</p></div>
<div class="m2"><p>کعبه ما قبله گبر و مسلمان آمده</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>نی هوای کعبه دارم بعد ازین نی فکر دیر</p></div>
<div class="m2"><p>مقصد من بارگاه خان خانان آمده</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>آن که در ظلمات تنهایی خیال مدح او</p></div>
<div class="m2"><p>تشنه طبعان سخن را آب حیوان آمده</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>هر که را طومار عهد نیک بختی واکنند</p></div>
<div class="m2"><p>ساعت مولود او بینند عنوان آمده</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>طبع من سنگیست از تحمید او گویا شده</p></div>
<div class="m2"><p>نظم من خاکیست از مدحش درو جان آمده</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گنج از هر نکته ام پیدا توان کردن که او</p></div>
<div class="m2"><p>پای تا سر در مدیح خویش پنهان آمده</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>سنگ از من لعل گردد خاک از من زر شود</p></div>
<div class="m2"><p>آب و رنگ آفتابم جانب کان آمده</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>شکرست این نوع لفظ از شکرستان خاسته</p></div>
<div class="m2"><p>بلبلست این جنس خاطر از گلستان آمده</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>در نمکزار حقوقش خاطرم افتاده بود</p></div>
<div class="m2"><p>خود نمک گردید و سوی نمکدان آمده</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بر کف اسکندر امروز آب حیوان دیده اند</p></div>
<div class="m2"><p>مژده الیاس را خضر از بیابان آمده</p></div></div>
<div class="b2" id="bn93"><p>بلبل مستم پیام نوبهار آورده ام</p>
<p>تازه تر صوتی به باغ از صوت پار آورده ام</p></div>
<div class="b" id="bn94"><div class="m1"><p>هرکجا دامن ز چشم خونفشان افشانده ام</p></div>
<div class="m2"><p>موج خونین بر سر هفت آسمان افشانده ام</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>پا و دست از مسند و پهلوی جم دزدیده ام</p></div>
<div class="m2"><p>دوش و سر از تاج و تشریف کیان افشانده ام</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>هم به آن سر بار شوقت را به دوش آورده ام</p></div>
<div class="m2"><p>همه به آن پا دست در راهت ز جان افشانده ام</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>شرم دارم گر ز نزدیکان تو نامم برند</p></div>
<div class="m2"><p>با چنین دوری که جان بر آستان افشانده ام</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>لذت درد محبت کی فراموشم شود؟</p></div>
<div class="m2"><p>این نمک را من به مغز استخوان افشانده ام</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>قسمتم جز بر همان مهمانسرای خود مباد</p></div>
<div class="m2"><p>از کباب دل پرست آن جا که خوان افشانده ام</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>دور را مهمان گستاخم به بازی بارها</p></div>
<div class="m2"><p>نقل بر ساقی و می بر میزبان افشانده ام</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>تا چه گل ها بشکفد کز سرگذشت زلف تو</p></div>
<div class="m2"><p>شب عبیری بر دماغ میهمان افشانده ام</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>در بیان آرزومندی سری شوریده است</p></div>
<div class="m2"><p>هر نقط کز خامه مشکین فشان افشانده ام</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بلبل باغم که شاخ و برگ بر گل چیده ام</p></div>
<div class="m2"><p>شبنم صبحم که بر خورشید جان افشانده ام</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>مست شوقم خرده بر زشت و نکوی من مگیر</p></div>
<div class="m2"><p>از نسیم تست گر گل گر خزان افشانده ام</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>گرچه با تو در سفر کوته عنانی کرده ام</p></div>
<div class="m2"><p>جان و دل از پی بر آن دست و عنان افشانده ام</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>کار با ضعف دل افتادست و اشگ عاجزی</p></div>
<div class="m2"><p>جعبه خالی کرده بر دشمن کمان افشانده ام</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>همتی در کار دل کن کز مزار عافیت</p></div>
<div class="m2"><p>خاک سرگردانیش بر خان و مان افشانده ام</p></div></div>
<div class="b2" id="bn108"><p>هرکه رخ تابد ازین دولتسرا گمراه باد</p>
<p>کعبه هم لبیک گوی خاک این درگاه باد</p></div>