---
title: >-
    شمارهٔ ۵۱۲
---
# شمارهٔ ۵۱۲

<div class="b" id="bn1"><div class="m1"><p>کیست این از روی رعنایی به جولان آمده</p></div>
<div class="m2"><p>کرده بر هرکس نظر بر خویش نازان آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صفا چون صبحدم در تازه رویی چون بهار</p></div>
<div class="m2"><p>صد گلستان سنبل و گل در گریبان آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمبدم می گردد از نظاره عالم محوتر</p></div>
<div class="m2"><p>چشم قربانیست بر دیدار حیران آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوستان را می خراشد دل، خروش گریه ام</p></div>
<div class="m2"><p>من خودم در اشگ گرم خویش پنهان آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق در نظاره حورند از اوقات خویش</p></div>
<div class="m2"><p>روزگار ما ز سر تا پا پریشان آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو ابر از گوش ها رعدم ز سر بیرون شده</p></div>
<div class="m2"><p>همچو کوه از چشم ها سیلم به دامان آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی جور از راه بخشش یار می گرداندم</p></div>
<div class="m2"><p>کین دیرینم به یادش بعد نسیان آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوششم بی مزد و منت صنعتم بی نرخ و قدر</p></div>
<div class="m2"><p>کار خویشم از زبان بر خویش تاوان آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکر لله شد «نظیری » یار در غربت دچار</p></div>
<div class="m2"><p>زین سفر نازم که سودست آنچه نقصان آمده</p></div></div>