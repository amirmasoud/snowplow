---
title: >-
    شمارهٔ ۴۸۸
---
# شمارهٔ ۴۸۸

<div class="b" id="bn1"><div class="m1"><p>دوش کردیم دل و دیده به دیدار گرو</p></div>
<div class="m2"><p>سر نهادیم به پوشیدن اسرار گرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاک بازانه کشیدیم سر از داو حریف</p></div>
<div class="m2"><p>سیم و زر باخته و جبه و دستار گرو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علمی شقه عمامه از آن زلف نداشت</p></div>
<div class="m2"><p>دلق و عمامه نهادیم به یک تار گرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون برآریم سر از دایره مشکینش؟</p></div>
<div class="m2"><p>چرخ کردست درین دایره پرگار گرو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آبروی من اگر برد جمالش چه عجب</p></div>
<div class="m2"><p>برده از نار مغان آن رخ گلنار گرو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بندم از صومعه زنار که در دیر مغان</p></div>
<div class="m2"><p>مصحف و خرقه نگیرند به زنار گرو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغ محبوس گر آن سرو بهشتی بیند</p></div>
<div class="m2"><p>به پر و بال کند چنگل و منقار گرو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر رود سر، من ازین شورش و سودا نروم</p></div>
<div class="m2"><p>کرده ام رخت درین گوشه بازار گرو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می شود هر نفس از عشق «نظیری » رنگی</p></div>
<div class="m2"><p>دلق درویش که کردست به عیار گرو</p></div></div>