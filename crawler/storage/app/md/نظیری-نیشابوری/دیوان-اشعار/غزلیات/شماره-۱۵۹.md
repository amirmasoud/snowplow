---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>گر تشنه بر سر خم میرم عجب نباشد</p></div>
<div class="m2"><p>رحمی نمی نمایند تا جان به لب نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با صد امید خواندند کز انتظار سوزند</p></div>
<div class="m2"><p>چون در نمی گشایند کاش این طلب نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صهبای راز دادند سرمست شوق کردند</p></div>
<div class="m2"><p>گویند لب گشودن شرط ادب نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من یک سبب ندارم وز کبر بر در بخت</p></div>
<div class="m2"><p>یک مدعا نسازند تا صد سبب نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ذلتی ببینند آمرزشی نمایند</p></div>
<div class="m2"><p>پایی اگر بلغزد جای طرب نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز دل توانگر لذت نیابد از عشق</p></div>
<div class="m2"><p>غم نیست عاشقان را گر قوت شب نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از عقدهای دوران دل بد مکن «نظیری »</p></div>
<div class="m2"><p>آن را که واگذارند جز از غضب نباشد</p></div></div>