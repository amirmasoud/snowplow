---
title: >-
    شمارهٔ ۳۰۶
---
# شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>ناله اصحاب مسجد نیست بی فریادرس</p></div>
<div class="m2"><p>تا مؤذن می شود بیدار می خوابد عسس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساجدان را تن ز نقصان وظایف کاسته</p></div>
<div class="m2"><p>بر زمین چسبیده اند از ضعف روزی چون مگس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرسنه چشمان بر انجم چشم حسرت دوخته</p></div>
<div class="m2"><p>صبح از قرصی که دارد برنمی آرد نفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر خروش سینه لرزان همچو بر سیلاب موج</p></div>
<div class="m2"><p>بر سرشک دیده غلطان همچو بر گرداب خس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامنت زاری کنان خواهند گیرند این گروه</p></div>
<div class="m2"><p>لیک نتوانند بردارند دست از پیش و پس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر امید آب و دانه تا به کی داری اسیر</p></div>
<div class="m2"><p>یا بکش این عاجزان را یا برون آر از قفس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو به تخت مصر پیراهن فشانی بر صبا</p></div>
<div class="m2"><p>قحطیان را روح می پرد به آواز جرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرده این سور واین شیون به هم نزدیک نیست</p></div>
<div class="m2"><p>مفلسان از درد می گویند و منعم از هوس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چاره‌ای خواهد «نظیری » بهر این بیچارگان</p></div>
<div class="m2"><p>دارد از احسان میرزا شادمان این ملتمس</p></div></div>