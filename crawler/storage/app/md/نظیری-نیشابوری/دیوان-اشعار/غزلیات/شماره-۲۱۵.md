---
title: >-
    شمارهٔ ۲۱۵
---
# شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>کسی به ملک حدوث از قدم نمی‌افتد</p></div>
<div class="m2"><p>که بر گذرگه شادی و غم نمی‌افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به روشنایی دل رو که رفتگان رستند</p></div>
<div class="m2"><p>گذار زنده‌دلان بر عدم نمی‌افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من این مرقع الوان بیفکنم روزی</p></div>
<div class="m2"><p>که طرح رندی و تقوا به هم نمی‌افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبان دعوت و تسخیر به که بربندم</p></div>
<div class="m2"><p>که در چراغ کس آتش به دم نمی‌افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسافری که ز نابود بود خود بیند</p></div>
<div class="m2"><p>به فکر منفعت بیش و کم نمی‌افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلیل عشق نزیبد کسی که در هر گام</p></div>
<div class="m2"><p>سرش چو شمع به پیش قدم نمی‌افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان ز شوق تو گردیده کعبه سرگردان</p></div>
<div class="m2"><p>که راه کعبه روان بر حرم نمی‌افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان پرستش روی تو جذب دل‌ها کرد</p></div>
<div class="m2"><p>که عشق برهمنان بر صنم نمی‌افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ذکر من خط نسیان کشیده‌ای اما</p></div>
<div class="m2"><p>به فکر غیر ز دستت قلم نمی‌افتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز سهو خاطر یاران چنان سقیم شدم</p></div>
<div class="m2"><p>که سایه قلمم بر رقم نمی‌افتد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نویسی ار به «نظیری» دعا و گر دشنام</p></div>
<div class="m2"><p>ز شوق نامه به فکر رقم نمی‌افتد</p></div></div>