---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>هر که را معنی نمی خیزد ز دل گفتار نیست</p></div>
<div class="m2"><p>نیست یک عارف که هم ساقی و هم خمار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار خار کوی یاری هست هر کس را دلیست</p></div>
<div class="m2"><p>نشکفد هر گل که در پای دلش این خار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سحر چشم بت به کارست و دعای برهمن</p></div>
<div class="m2"><p>گبر هر تاری که بندد بر میان زنار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توبه هشیار، می گویند می گردد قبول</p></div>
<div class="m2"><p>تا ننوشم می مرا یارای استغفار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستی و شاهدپرستی، هرزه خندی و نشاط</p></div>
<div class="m2"><p>کار کار می گسارانست و دیگر کار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش پای گرم و سرد روزگار افتاده ام</p></div>
<div class="m2"><p>سایه در ویرانه ام از پستی دیوار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندکی ای ناله امشب بی اثر می یابمت</p></div>
<div class="m2"><p>آن که هر شب می شنید امشب مگر بیدار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردم از شرمندگی تا چند با هر ناکسی</p></div>
<div class="m2"><p>مردمت از دور بنمایند و گویم یار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجلس آخر شد «نظیری » حال خود با او بگو</p></div>
<div class="m2"><p>هر نفس بزمی و هر دم صحبتی در کار نیست</p></div></div>