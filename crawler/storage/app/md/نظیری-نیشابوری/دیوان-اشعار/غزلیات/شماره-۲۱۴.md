---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>دل نمی‌دانم کجا، زین آستانم می‌کشد</p></div>
<div class="m2"><p>مرگ می‌بینم که با هجرام عنانم می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سر مو بر تنم دارد خروشی از وداع</p></div>
<div class="m2"><p>هجر پیوند تو از رگ‌های جانم می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داشتم در سینه پیکان خدنگ کاریی</p></div>
<div class="m2"><p>دست غیرت این زمان از استخوانم می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌کند آسودگی سیری به گرد خاطرم</p></div>
<div class="m2"><p>گریه هم پایی ز چشم خون‌فشانم می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصه وارستگی امروز پیش دل گذشت</p></div>
<div class="m2"><p>طرفه حرف ناامیدی از زبانم می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر بازار جانبازان کمان آویختم</p></div>
<div class="m2"><p>دست غیرت بشکنم هرکس کمانم می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌کشم سر از کمند او «نظیری» بعد ازین</p></div>
<div class="m2"><p>گر به صد زنجیر آن نامهربانم می‌کشد</p></div></div>