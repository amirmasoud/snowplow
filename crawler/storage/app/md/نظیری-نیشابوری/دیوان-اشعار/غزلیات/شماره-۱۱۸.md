---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>بیا که مردم و بر راه چشم جان بازست</p></div>
<div class="m2"><p>به گفتگوی تو زخم مرا دهان بازست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خون ما اگرت میل هست مانع نیست</p></div>
<div class="m2"><p>می مغانه سبیل و در مغان بازست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو یوسفی تو که از مصر حسن چون تو کسی</p></div>
<div class="m2"><p>برون نیامده تا راه کارون بازست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آرزوی نثار قدوم تو همه شب</p></div>
<div class="m2"><p>گهر فروش دو چشم مرا دکان بازست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی رود چو گرسنه ولی چه سود از این</p></div>
<div class="m2"><p>که خوان وصل پر و دست میهمان بازست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بلبل قفسم من ازین چه ذوق مرا</p></div>
<div class="m2"><p>که گل شکفته و درهای بوستان بازست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صمد به جای صنم بر زبانم آمده است</p></div>
<div class="m2"><p>بتم فتاده و زنارم از میان بازست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دعا کنید به وقت شهادتم او را</p></div>
<div class="m2"><p>که آن دمی است که درهای آسمان بازست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن شتاب «نظیری » به کار جان بازی</p></div>
<div class="m2"><p>که چشم کارشناسان کاردان بازست</p></div></div>