---
title: >-
    شمارهٔ ۳۴۸
---
# شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>بر غمزده ای خنده زدم گفت حزین باش</p></div>
<div class="m2"><p>گر با تو هم اندیشه ما هست چنین باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم: شده دل منکر دین گفت: غمی نیست</p></div>
<div class="m2"><p>گو عاشق ما باش و صنم خانه نشین باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کافیست اگر عشق بود عرض شهادت</p></div>
<div class="m2"><p>تصدیق کن و بی خبر از مذهب و دین باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دور فلک شکر کن و سیر کواکب</p></div>
<div class="m2"><p>بخت تو که خوبست، بد روی زمین باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در فکر هما بودن صیاد همایونست</p></div>
<div class="m2"><p>در دام تو هرچند نیفتد به کمین باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس راه به جولانگه سیمرغ نبرده</p></div>
<div class="m2"><p>شاید که مثالی بنمایند به چین باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افلاک و زمین بار امانت نکشیدند</p></div>
<div class="m2"><p>آن حوصله پیدا کن و آنگاه امین باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا هست نزاعی به دلت دشمن خویشی</p></div>
<div class="m2"><p>گر دوست نیی با همه با خویش به کین باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تلخ سخن های تو ما پند گرفتیم</p></div>
<div class="m2"><p>گو خاتم یاقوت تو الماس نگین باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا خط سیه کار تو در فکر شبیخونست</p></div>
<div class="m2"><p>گو آه مرا توسن شبرنگ به زین باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آزرده نگردیدی از ابرام «نظیری »</p></div>
<div class="m2"><p>هرچند که بهتر شده ای بهتر از این باش</p></div></div>