---
title: >-
    شمارهٔ ۴۲۳
---
# شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>آتشین گفتار خاکی پیکرم</p></div>
<div class="m2"><p>قطعه باغ خلیل آزرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دم احیای عیسی معجزم</p></div>
<div class="m2"><p>در ید بیضای موسی دفترم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جای گل بلبل برآرد شاخ گل</p></div>
<div class="m2"><p>گر فشانی بر چمن خاکسترم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم معنی به نورم روشن است</p></div>
<div class="m2"><p>در حقیقت آفتاب دیگرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غوطه ها در بحر معنی صنع کرد</p></div>
<div class="m2"><p>تا بزاد از نه صدف یک گوهرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سخن هرکس هیولایی نمود</p></div>
<div class="m2"><p>من هیولای سخن را جوهرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس به معیارم نمی آرد سخن</p></div>
<div class="m2"><p>هین محک صاحب عیار و هین زرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصل معنی دیر اگر دستم دهد</p></div>
<div class="m2"><p>پرده افلاک را برهم درم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جوهرم، جسمم، نمی دانم چیم؟</p></div>
<div class="m2"><p>هرچه هستم غرق مهر حیدرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اختران چون سرمه در چشمم کنند</p></div>
<div class="m2"><p>آسمان گوید غبار آن درم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برتر از حال «نظیری » نکته ها</p></div>
<div class="m2"><p>گویم و از خود نیاید باورم</p></div></div>