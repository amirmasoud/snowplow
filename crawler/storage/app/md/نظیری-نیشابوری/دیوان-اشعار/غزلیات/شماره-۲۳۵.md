---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>ناوک غم جان شکافد سینه گر جوشن شود</p></div>
<div class="m2"><p>عشق مغناطیس گردد دل اگر آهن شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه پرحسرتی دارم که از اندوه او</p></div>
<div class="m2"><p>تا به نزدیک لب آرم خنده را، شیون شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیش شد سرگشتگی چندان که پایم پیش شد</p></div>
<div class="m2"><p>سر به تاریکی نهادم تا رهی روشن شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک توجه از تو در کارست و یک عالم مراد</p></div>
<div class="m2"><p>غم ندارم گر اجابت با دعا دشمن شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب ترنم های من بیدار دارد خلق را</p></div>
<div class="m2"><p>هرکه را سوزد چراغی ناله ام روغن شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که بی تو جامه تن در بر جان تنگ شد</p></div>
<div class="m2"><p>گر گریبان را بدوزم چاک از دامن شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصل اگر خواهی «نظیری » شوق را سرمایه ساز</p></div>
<div class="m2"><p>نور عشق است این چراغ وادی ایمن شود</p></div></div>