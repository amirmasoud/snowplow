---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>کس چو من نیست که پیش نظر از دل برود</p></div>
<div class="m2"><p>غایب از دیده نگردد ز مقابل برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دولتی بود که مردیم به هنگام وداع</p></div>
<div class="m2"><p>آنقدر زنده نماندیم که محمل برود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه بیگانگیی پیش نداری که کسی</p></div>
<div class="m2"><p>به دلیل ره و طی کردن منزل برود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبر داریم که این تهمت عشق از سر غیر</p></div>
<div class="m2"><p>همچو خون بحل از گردن قاتل برود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصه ما به عزیزان وطن خواهد گفت</p></div>
<div class="m2"><p>هرکه را تخته ازین ورطه به ساحل برود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیکویی دوستی آرد به دل دشمن و دوست</p></div>
<div class="m2"><p>همه جا سر زند این ریشه چو در گل برود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرد عاشق ندهد دل به تماشای جهان</p></div>
<div class="m2"><p>آن دهد کیسه به طرار که غافل برود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر چشمان تو گردم که زبس خونخواری</p></div>
<div class="m2"><p>قطره ای خون نگذارد که ز بسمل برود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من و آزار «نظیری » به کسی عارم باد</p></div>
<div class="m2"><p>به زبان آید از آنم گله کز دل برود</p></div></div>