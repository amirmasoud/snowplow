---
title: >-
    شمارهٔ ۴۶۰
---
# شمارهٔ ۴۶۰

<div class="b" id="bn1"><div class="m1"><p>دهشت از صیدم مکن بی زخم کاری نیستم</p></div>
<div class="m2"><p>خود شکار کس شوم شیر شکاری نیستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مغز افروزد شمیم و کشت سوزد شبنمم</p></div>
<div class="m2"><p>آه محنت دیده ام باد بهاری نیستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود به خون خویش می جوشم چو صهبا در سبو</p></div>
<div class="m2"><p>زین حریفان از کسی ممنون یاری نیستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به که از من کم رسد زحمت به صدر اعتبار</p></div>
<div class="m2"><p>پر به تنگ از گوشه بی اعتباری نیستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آگهی بخش است حالم، پند ده بیناییم</p></div>
<div class="m2"><p>در سر مغرور کم از هوشیاری نیستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فصل ها از سرگذشت ناامیدی خوانده ام</p></div>
<div class="m2"><p>گوش بر افسانه امیدواری نیستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچه می گوید زبانم کرده انشا کاتبم</p></div>
<div class="m2"><p>جز رقم از خامه بی اختیاری نیستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انتظار وعده دارم در ادای وام دوست</p></div>
<div class="m2"><p>بد ادا وقت طلب در جانسپاری نیستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوی شرمم پندگیران را «نظیری » بر جبین</p></div>
<div class="m2"><p>گرچه دارم منفعت بی شرمساری نیستم</p></div></div>