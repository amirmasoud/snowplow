---
title: >-
    شمارهٔ ۴۸۱
---
# شمارهٔ ۴۸۱

<div class="b" id="bn1"><div class="m1"><p>چند فارغ از نشاط درد و درمان زیستن</p></div>
<div class="m2"><p>همچو خون مرده زیر پوست پنهان زیستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق و این ناآشنایی؟ عشق و این بی نسبتی؟</p></div>
<div class="m2"><p>تشنه دیدار وانگه در بیابان زیستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوبی از اندازه بیرون می بری انصاف نیست</p></div>
<div class="m2"><p>دشمن جان بودن و شیرین تر از جان زیستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده پراشک و زبان پر شکر مشکل حالتیست</p></div>
<div class="m2"><p>با چنین نازک دلی ها سخت پیمان زیستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیش میخواران مفلس را چراغ خلوتم</p></div>
<div class="m2"><p>بایدم از خانه همسایه پنهان زیستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا سحر با ساز و صحبت تا به شب در گشت و سیر</p></div>
<div class="m2"><p>همچو گل طرفی نبستم از پریشان زیستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشت خاشاک «نظیری » شعله ای کرد و نشست</p></div>
<div class="m2"><p>باد شمع انجمن را تا به پایان زیستن</p></div></div>