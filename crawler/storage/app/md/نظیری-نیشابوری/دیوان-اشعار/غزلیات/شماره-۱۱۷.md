---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>گر کند گیتی وفایی با وفاداران خوش است</p></div>
<div class="m2"><p>زندگانی با عزیزان عیش با یاران خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محنت شب گیر با شوق حرم دشوار نیست</p></div>
<div class="m2"><p>گر بیادت بگذرد شب های بیداران خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرگس شوخ تو مست از ناله شب گیر ماست</p></div>
<div class="m2"><p>می فروشان را سر از غوغای می خواران خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مال و عصمت را زلیخا بد درین سودا نباخت</p></div>
<div class="m2"><p>ماه کنعان بردن از خیل خریداران خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرجه ای نگذاشت گردون تا از آن بیرون روم</p></div>
<div class="m2"><p>پیر کودک دل چه با قید گرفتاران خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذوق مرغان می پراند مرغ نوآموز را</p></div>
<div class="m2"><p>نو به کوی دوست رفتن با هواداران خوش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیرتم نیکو ز استیلای عشق آزاد ساخت</p></div>
<div class="m2"><p>چون مرض طغیان نماید خواب بیماران خوش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساقی هم رنگ باید ساغر گل رنگ را</p></div>
<div class="m2"><p>می پرستان را نظر بر لاله رخساران خوش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غرق طوفان شد «نظیری » هر که دل بر مال بست</p></div>
<div class="m2"><p>رخت بیرون ده که کشتی سبک باران خوش است</p></div></div>