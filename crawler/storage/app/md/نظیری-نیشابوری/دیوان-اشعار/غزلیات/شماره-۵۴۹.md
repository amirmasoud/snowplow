---
title: >-
    شمارهٔ ۵۴۹
---
# شمارهٔ ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>یک ره کم این حجره خاکی نگرفتی</p></div>
<div class="m2"><p>ترک صنم و دیر مغاکی نگرفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیو و ملک از عربده ناکی تو جستند</p></div>
<div class="m2"><p>در شکوه عنان دل شاکی نگرفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کوچه وحدت که شهودی به دوام است</p></div>
<div class="m2"><p>حظ نظر از وسوسه ناکی نگرفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر ببری ز اول اگر پاک ببازی</p></div>
<div class="m2"><p>درباختی ار مهره به پاکی نگرفتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مهد دلت میل به شوخی و هوا داشت</p></div>
<div class="m2"><p>راحت چو می از نشئه تا کی نگرفتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون گل به نسیمی دل شوریده شود چاک</p></div>
<div class="m2"><p>هرچند که چون غنچه به چاکی نگرفتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر چرخ نسودی چو قمر گوشه نعلین</p></div>
<div class="m2"><p>زنار مسیحا به شراکی نگرفتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردیم که در چشم تو گیریم بهایی</p></div>
<div class="m2"><p>چون زندگی اما به هلاکی نگرفتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حال تو نشد رتبه معراج «نظیری »</p></div>
<div class="m2"><p>گر بطن سمک را به سماکی نگرفتی</p></div></div>