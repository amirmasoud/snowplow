---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>هوا بدیهه رسانست و باغ موزون است</p></div>
<div class="m2"><p>به هر ترنم مرغی هزار مضمون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبان بلبل شوخ از سخن نمی افتد</p></div>
<div class="m2"><p>اگر چه خورده گل همچو در مکنون است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهوش زی که تو گر از برون نمی بینی</p></div>
<div class="m2"><p>درون پرده ببینند هرچه بیرون است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر به لذت لطف نهان رسی دانی</p></div>
<div class="m2"><p>که اندک تو ز بیشت چگونه افزون است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به شور وادی و فریاد سیل خروش داریم</p></div>
<div class="m2"><p>کز اهل سلسله ماست هر که مجنون است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز روی دوست هویدا بود سعادت دوست</p></div>
<div class="m2"><p>نوشته اند به عنوان که خاتمت چون است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشان ذوق حقیقت به نازکان ندهند</p></div>
<div class="m2"><p>چه شد که فاخته خوش گوی و سرو موزون است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر کنار بیابان عشق دریابی</p></div>
<div class="m2"><p>ز خون کشته ببینی هزار جیحون است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هیچ کاسه چشم گدای پر نشود</p></div>
<div class="m2"><p>مگر ز کاسه آزادگان که وارون است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو نام توبه گرفتم، قدح به یاد آمد</p></div>
<div class="m2"><p>بنوش باده «نظیری » که فال میمون است</p></div></div>