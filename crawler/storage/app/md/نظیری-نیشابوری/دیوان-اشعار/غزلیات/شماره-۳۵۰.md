---
title: >-
    شمارهٔ ۳۵۰
---
# شمارهٔ ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>رمید طایر جانم ز آشیانه خویش</p></div>
<div class="m2"><p>که در هوای تو خوش یافت آب و دانه خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از قفای نظر کو به کوی می گردد</p></div>
<div class="m2"><p>نظر ز شوق تو گم کرده راه خانه خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز باغ رفت گل و بلبلان خموش شدند</p></div>
<div class="m2"><p>من اسیر همان عاشق فسانه خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که واقف ذوقی شود نمی بینم</p></div>
<div class="m2"><p>بغیر خویش که می رقصم از ترانه خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به شب که دردی دردی به کام دل ریزند</p></div>
<div class="m2"><p>کنم به روز طرب از می شبانه خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مروت و کرم از دیگری نمی بینم</p></div>
<div class="m2"><p>نشسته ام به گدایی بر آستانه خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بس که دور زمان را ز خسروان ننگ است</p></div>
<div class="m2"><p>زمانه نازد اگر گویمش زمانه خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گنج خانه محمود مدح نفروشم</p></div>
<div class="m2"><p>به شاهنامه خرم بیت عاشقانه خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو را که نقد جهان باید از طلب منشین</p></div>
<div class="m2"><p>مرا خوش است دل از داغ جاودانه خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر ز برهمنان سرکشی، نیازارند</p></div>
<div class="m2"><p>تو را که هست بت خویش در خزانه خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلی به شرط «نظیری » نهاده بر سر راه</p></div>
<div class="m2"><p>به هر که تیر زند می‌دهد نشانه خویش</p></div></div>