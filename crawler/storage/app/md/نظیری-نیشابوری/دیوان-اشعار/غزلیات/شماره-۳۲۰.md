---
title: >-
    شمارهٔ ۳۲۰
---
# شمارهٔ ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>با حکمت ایستاده‌ام اینم پناه بس</p></div>
<div class="m2"><p>با عفوت این گنه که ندارم گناه بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسنت که خط نوشت به خونم درنگ چیست</p></div>
<div class="m2"><p>یک مؤمن و دو کافر هندو گواه بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچند از دلم غم دیرینه پرسش است</p></div>
<div class="m2"><p>مکتوب تو فراق تو را عذرخواه بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تعویذ چشم زخم وصال تو هجر تست</p></div>
<div class="m2"><p>نقصان ماه حرز تمامی ماه بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گو کوکب براق سواران در ابر باش</p></div>
<div class="m2"><p>در تیره شب دلیل رهم برق آه بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بادم که نور دیده یعقوب می برم</p></div>
<div class="m2"><p>از مصر بوی پیرهنم زاد راه بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد خاندان ز آه ضعیفی تبه شود</p></div>
<div class="m2"><p>روز سپید را دم شاه سیاه بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیوانگان ز ماه نو آشفته می شوند</p></div>
<div class="m2"><p>شور مراست جلوه پر کلاه بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حیف آیدم که آن خم ابرو ترش شود</p></div>
<div class="m2"><p>بهر نظارگی تو ضبط نگاه بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امید هست سود و زیان سربسر شود</p></div>
<div class="m2"><p>سرمایه ام خجالت تقصرگاه بس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آوردن شفیع «نظیری » خیانت است</p></div>
<div class="m2"><p>امید بنده بر کرم پادشاه بس</p></div></div>