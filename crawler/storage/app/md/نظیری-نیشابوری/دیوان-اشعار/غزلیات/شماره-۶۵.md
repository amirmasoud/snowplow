---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>میم در جام و ماهم تا سحر بر روزنست امشب</p></div>
<div class="m2"><p>دو دستم تا به وقت صبح طوق گردنست امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو چشمم حجله آیین بسته اند از گریه شادی</p></div>
<div class="m2"><p>درو بام از چراغان سرشکم روشنست امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شماری تا سحر، دستم به زلف درهمی دارد</p></div>
<div class="m2"><p>گریبانم گریبانست و دامن دامنست امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه شب بر لب و رخسار و گیسو می زنم بوسه</p></div>
<div class="m2"><p>گل و نسرین و سنبل را صبا در خرمنست امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مغنی می گساری می کند ساقی نوا سازی</p></div>
<div class="m2"><p>ازین شادی که در بزم حسودان شیونست امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دل طرح وصال جاودانی نقش می بندم</p></div>
<div class="m2"><p>گرم خود دوست می آید به خلوت دشمنست امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به اقبال محبت شاهد و می در نظر دارم</p></div>
<div class="m2"><p>نه من با بخت خویشم نی «نظیری » با منست امشب</p></div></div>