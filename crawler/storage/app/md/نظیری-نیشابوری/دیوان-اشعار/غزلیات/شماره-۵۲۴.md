---
title: >-
    شمارهٔ ۵۲۴
---
# شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>دیریست ز گوشه نقابی</p></div>
<div class="m2"><p>داریم امید فتح بابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زلف کجش به عقد انگشت</p></div>
<div class="m2"><p>ذوقست گرفتن حسابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وآنگه به نشان بوسه کردن</p></div>
<div class="m2"><p>از صفحه رویش انتخابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هردم به گلاب پاش مژگان</p></div>
<div class="m2"><p>بر جیب فشاندش گلابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دیده تر بر آن بناگوش</p></div>
<div class="m2"><p>غلطان دیدن در خوشابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ارزد به نشان صد سلیمان</p></div>
<div class="m2"><p>تا بی کندن ز لعل نابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انگشت نمای شهرم از وی</p></div>
<div class="m2"><p>هر دم به نوازش خطابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مطعون دارد میان خلقم</p></div>
<div class="m2"><p>هر روز به شهرت عتابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل هر گله کز تغافلش داشت</p></div>
<div class="m2"><p>سر شد به ندادن جوابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رازی که به صد دهان نگنجد</p></div>
<div class="m2"><p>گردید بیان به اضطرابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با دوست گر اتحاد خواهی</p></div>
<div class="m2"><p>از خویش «نظیری » اجتنابی</p></div></div>