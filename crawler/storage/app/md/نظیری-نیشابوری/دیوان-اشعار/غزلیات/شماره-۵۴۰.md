---
title: >-
    شمارهٔ ۵۴۰
---
# شمارهٔ ۵۴۰

<div class="b" id="bn1"><div class="m1"><p>به خودبینی به جز بتگر نباشی</p></div>
<div class="m2"><p>ز خود بگریز تا کافر نباشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ظلمت افکند طوفان جهلت</p></div>
<div class="m2"><p>چو کشتی گر گران لنگر نباشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خیل طایران قدس گردی</p></div>
<div class="m2"><p>اگر در قید بال و پر نباشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه انسان زاده ای فضلی طلب کن</p></div>
<div class="m2"><p>که از همچون خودی کمتر نباشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان سیراب ساز امروز جان را</p></div>
<div class="m2"><p>که فردا تشنه کوثر نباشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر خواهی گذارندت درین بزم</p></div>
<div class="m2"><p>دمی بی دود چون مجمر نباشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه کس صید فربه خواهد و عشق</p></div>
<div class="m2"><p>کند ردت اگر لاغر نباشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر پای ریاحین سر نداری</p></div>
<div class="m2"><p>چو نرگس صاحب افسر نباشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو ساغر پیشه گر بخشش نگیری</p></div>
<div class="m2"><p>میان همگنان سرور نباشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگردی زین خط پرگار بی سر</p></div>
<div class="m2"><p>اگر چون نقطه ز اول سر نباشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>«نظیری » دل به سیمین غبغبی بند</p></div>
<div class="m2"><p>که در قید مه و اختر نباشی</p></div></div>