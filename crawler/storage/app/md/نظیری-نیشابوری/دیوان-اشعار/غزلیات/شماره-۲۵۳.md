---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>عنان دل ز خودرایی به فریادم نگه دارد</p></div>
<div class="m2"><p>بنالم کاندر آن دل ناله مظلوم ره دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل دیوانه ام را گنج در ویرانه افتادست</p></div>
<div class="m2"><p>گدایی عشق بازی با جمال پادشه دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو گوید کفر مجذوبی به استغفار حاجت نیست</p></div>
<div class="m2"><p>کسی کز عشق گمره شد چه پروای گنه دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا گر هست کبری در دماغ از کبریای اوست</p></div>
<div class="m2"><p>حباب از جوش دریا باد نخوت در کله دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تجلی جمالی هست در هرجا که ذوقی هست</p></div>
<div class="m2"><p>بیابان شور اگر می آورد یوسف به چه دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فقیری را که شب ها تکیه گاه از خشت آن در شد</p></div>
<div class="m2"><p>چنان خوابد که گویی تکیه بر خورشید و مه دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حکایت های عهد دوستی را کرده ام از بر</p></div>
<div class="m2"><p>چو هندویی که بهر سوختن هیزم نگه دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان بهتر که نگشایی سر راز دل ما را</p></div>
<div class="m2"><p>که حرف هجر خونین نامه ما را سیه دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خاک پای گلبن می نویسد شکوه از غربت</p></div>
<div class="m2"><p>اگر بر شاخ طوبا بلبلی آرامگه دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شبیخون غم از پا درنمی آرد «نظیری » را</p></div>
<div class="m2"><p>ز اشک و آه شب سلطان ما خیل و سپه دارد</p></div></div>