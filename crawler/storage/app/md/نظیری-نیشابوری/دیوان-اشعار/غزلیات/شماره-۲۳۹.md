---
title: >-
    شمارهٔ ۲۳۹
---
# شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>چون ابر بهاری به سرم سایه فکن شد</p></div>
<div class="m2"><p>بر هر بر بومم که نظر کرد چمن شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شمع که شد رهبر پروانه به آتش</p></div>
<div class="m2"><p>دل سوزی او باعث جان بازی من شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می خواست شود قایل نظمم به بلاغت</p></div>
<div class="m2"><p>صدپایه به شیب آمد و بر اوج سخن شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی جام همه می کش و بی باده همه مست</p></div>
<div class="m2"><p>از نظم نو آیین مغان رسم کهن شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شک نیست که از نیم نظر کار برآید</p></div>
<div class="m2"><p>آن را که دلیل آصف اعجاز سخن شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همسایگیش را اثر ابر بهارست</p></div>
<div class="m2"><p>هم خانه گلستان شد و هم خار سمن شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از یار و دیار ار نکنم یاد عجب نیست</p></div>
<div class="m2"><p>از رشک من امسال غریبی به وطن شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر خاک درش جای شهیدان ندهد کس</p></div>
<div class="m2"><p>لطفی است که کافور تن و عطر کفن شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهمان بهشتی مخور اندوه «نظیری »</p></div>
<div class="m2"><p>نزهتگه حوران جنان بیت حزن شد</p></div></div>