---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>ما بید بوستانیم، ما را ثمر نباشد</p></div>
<div class="m2"><p>مردود دوستانیم از ما بتر نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لب برون نیاید آواز عشقبازان</p></div>
<div class="m2"><p>پرواز مرغ بسمل جز زیر پر نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاراج دیدگانند آوارگان معشوق</p></div>
<div class="m2"><p>راهی نمی برد عشق کانجا خطر نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد در اگر گشایند بر جلوه گاه دیدار</p></div>
<div class="m2"><p>آن را که چشم بستند راهش به در نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اول نشان مردی اخفای کار خوبست</p></div>
<div class="m2"><p>بهتر ازین که گفتم دیگر هنر نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فیروزی ضعیفان در عجز و انکسارست</p></div>
<div class="m2"><p>تا نشکند صف ما ما را طفر نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تیغ کی هراسم، دیدار مزد قتل است</p></div>
<div class="m2"><p>خونی که عشق ریزد هرگز هدر نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دل به جای خویش است دارد عنان دیده</p></div>
<div class="m2"><p>عاشق که شد پریشان صاحب نظر نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در گوشه نقابت سیر گل است و نسرین</p></div>
<div class="m2"><p>زین خوبتر نظر را هرگز سفر نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرجا رود مسافر حرف تو ارمغانست</p></div>
<div class="m2"><p>یک خانه نیست کز تو پر از شکر نباشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قاصد که می فرستی، رطل گرانش درده</p></div>
<div class="m2"><p>کز ما خبر نیابد، تا بی خبر نباشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از شاخ لهو برگی حاصل نشد «نظیری »</p></div>
<div class="m2"><p>لب تشنه باد کشتی کز گریه تر نباشد</p></div></div>