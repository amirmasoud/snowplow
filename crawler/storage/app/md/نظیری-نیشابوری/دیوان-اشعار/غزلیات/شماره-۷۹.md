---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>این نخل که از چشمه جان رسته که کشتست؟</p></div>
<div class="m2"><p>وین خط که دهد یاد ز معجز که نوشتست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما فتنه ز مشاطه حسنیم نه از عشق</p></div>
<div class="m2"><p>زنار مغان رشته خط و زلف که رشتست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز از اثر دهشت ما وحشت ما نیست</p></div>
<div class="m2"><p>با پرده بگو پرده ز رخسار که هشتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین لخت دل و پاره جان چاشنیی گیر</p></div>
<div class="m2"><p>بر گریه تلخ و نمک خنده برشتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوق غمم از طینت خاکی نرود هیچ</p></div>
<div class="m2"><p>هرچند به شادی گل آدم نسرشتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو روی تو نظاره کن و خوی تو بنگر</p></div>
<div class="m2"><p>گوید کس اگر آدمیی را که فرشتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرتاسر صحرای رخت لاله و نسرین</p></div>
<div class="m2"><p>صد رنگ دگر گل، که ندانند که کشتست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل جامه ز بر، بس سبک از نازکی انداخت</p></div>
<div class="m2"><p>عریانی گلزار ز کوتاهی رشتست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در حیرتم از ترک و فنای تو «نظیری »</p></div>
<div class="m2"><p>کس غیر اجل فرش امل درننوشتست</p></div></div>