---
title: >-
    شمارهٔ ۵۳۶
---
# شمارهٔ ۵۳۶

<div class="b" id="bn1"><div class="m1"><p>گر حسن جمال تو طلبکار نبودی</p></div>
<div class="m2"><p>در خانقه و بتکده دیار نبودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نرگس مست تو نکردی جدل آغاز</p></div>
<div class="m2"><p>تا گردش او بودی هشیار نبودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی پرده توانستی اگر روی نمودن</p></div>
<div class="m2"><p>در کعبه حجاب در و دیوار نبودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازت ننهادی به دل این بار امانت</p></div>
<div class="m2"><p>گر حسن تو از عشق گرانبار نبودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میزان تو در دست غرورست وگرنه</p></div>
<div class="m2"><p>حسن تو به این قیمت و مقدار نبودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر غیرت تو پرده پندار بهشتی</p></div>
<div class="m2"><p>یک دل شده محروم ز دیدار نبودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افسوس که خوی تو چو روی تو نکو نیست</p></div>
<div class="m2"><p>ورنه همه بودی گل و یک خار نبودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای کاش نیامیختی این رنگ محبت</p></div>
<div class="m2"><p>تا این همه نیرنگ تو در کار نبودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسان ز عتاب تو توانست رهیدن</p></div>
<div class="m2"><p>گر جان به کمند تو گرفتار نبودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن تاب و توان رفت که بر یاد رخ تو</p></div>
<div class="m2"><p>دشوار شدی کارم و دشوار نبودی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می رست ازین حلقه زنار «نظیری »</p></div>
<div class="m2"><p>گر معنی تسلیم به زنار نبودی</p></div></div>