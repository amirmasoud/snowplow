---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>بی تو دوشم در درازی از شب یلدا گذشت</p></div>
<div class="m2"><p>آفتاب امروز چون برق از سرای ما گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیش خاری نیست کز خون شکاری سرخ نیست</p></div>
<div class="m2"><p>آفتی بود این شکارافکن کزین صحرا گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوکت حسنش کسی را رخصت آهی نداد</p></div>
<div class="m2"><p>گرچه هر سو دادخواهی بود، او تنها گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلوه اش ننمود از بس محو رفتارش شدم</p></div>
<div class="m2"><p>ناله ام نشنید از بس گرم استغنا گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواستی آشفتگی دستار بردن از سرش</p></div>
<div class="m2"><p>بس که سرمست و به خود مغرور و بی پروا گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با پریشانان چه گویم، صولت هجرش چه کرد</p></div>
<div class="m2"><p>باد یأسی آمد و بر دفتر دل‌ها گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز امشب با سگ کویش «نظیری » همرهست</p></div>
<div class="m2"><p>شوکتی دیدم که پنداری جم و دارا گذشت</p></div></div>