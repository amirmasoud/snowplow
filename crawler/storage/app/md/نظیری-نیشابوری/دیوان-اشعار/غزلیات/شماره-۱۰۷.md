---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>زهی نسخه آفرینش جمالت</p></div>
<div class="m2"><p>نکت یاب مجموعه گل خیالت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به فطرت فزونی نخواهد زبولت</p></div>
<div class="m2"><p>ز ظلمت برونی نباشد زوالت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همیشه حق از قول و رای تو روشن</p></div>
<div class="m2"><p>نپوشیده موج حوادث زلالت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حد خرد هست پرواز هر تن</p></div>
<div class="m2"><p>تو روحی خرد پرد از پر و بالت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه وجدها صوفیان را ز قولت</p></div>
<div class="m2"><p>همه حال ها قدسیان را ز حالت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اعجاز قولت که ایمان نیارد</p></div>
<div class="m2"><p>حلالت بود خون منکر، حلالت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به باطن تو را دید آدم مقدم</p></div>
<div class="m2"><p>ز صدر جنان شد به صف نعالت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پیرامنت سایه ظاهر نگردد</p></div>
<div class="m2"><p>که خور گشته طالع فراز نهالت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به حسن تو نقاش نقشی نیارد</p></div>
<div class="m2"><p>که صنعت گری ختم شد بر کمالت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>توان گفت لیس کمثله به شأنت</p></div>
<div class="m2"><p>که در غیب نبود مثال مثالت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>«نظیری » چنان ساز صافی سخن را</p></div>
<div class="m2"><p>که روح نبی خوش شود از مقالت</p></div></div>