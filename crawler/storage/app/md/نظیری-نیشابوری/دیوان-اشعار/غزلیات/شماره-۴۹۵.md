---
title: >-
    شمارهٔ ۴۹۵
---
# شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>از نصیحت برفروزد روی تو</p></div>
<div class="m2"><p>از شکر گردد ترش ابروی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند گرم خشم و بی باکی شدن</p></div>
<div class="m2"><p>روی تو در آتشست از خوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با می ما مشگ تو آمیختند</p></div>
<div class="m2"><p>رنگ ما نگرفتی و ما بوی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا که پا از خانه بیرون می نهم</p></div>
<div class="m2"><p>در بیابان می رمد آهوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گریم و خاک رهت شویم به اشک</p></div>
<div class="m2"><p>جای خود گم کرده ام در کوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه گهم از رشحه ای سیراب کن</p></div>
<div class="m2"><p>آب خوبی نیست کم در جوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تحفه ای زان حقه مرهم فرست</p></div>
<div class="m2"><p>تا دلم بگشاید از پهلوی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر دفع مرگ حرز جان کنم</p></div>
<div class="m2"><p>گر خدنگی یابم از بازوی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوستان را پشت بر صحبت مکن</p></div>
<div class="m2"><p>روی دل دارد «نظیری » سوی تو</p></div></div>