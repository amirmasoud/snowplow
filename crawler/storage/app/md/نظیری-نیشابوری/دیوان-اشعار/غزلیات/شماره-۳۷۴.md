---
title: >-
    شمارهٔ ۳۷۴
---
# شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>اگر تو نشنوی از ناله‌های زار چه حظ</p></div>
<div class="m2"><p>وگر تو ننگری از چشم اشکبار چه حظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درآ به مشرب روحانیان و واصل شو</p></div>
<div class="m2"><p>معاشران تو مستان تو هوشیار چه حظ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشم ما در و دیوار بوستان مستند</p></div>
<div class="m2"><p>تو را که باده نمی‌نوشی، از بهار چه حظ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمک به سینه مجروح چاشنی بخشد</p></div>
<div class="m2"><p>اگر غمی ندهندت ز غمگسار چه حظ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کلید قفل همه گنج‌ها به ما دادند</p></div>
<div class="m2"><p>به دست ما چو ندادند اختیار چه حظ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرم به پهلوی ساقی به بزم بنشانند</p></div>
<div class="m2"><p>مرا که بی‌خود و مستم ز اعتبار چه حظ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز عمر آن چه گرامی‌تر است در سفرست</p></div>
<div class="m2"><p>مرا که دل به غریبی است از دیار چه حظ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به لاف هم تک برق و براق می‌تازم</p></div>
<div class="m2"><p>برون نمی‌رودم مرکب از غبار چه حظ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزار ذوق «نظیری» به درد نومیدی‌ست</p></div>
<div class="m2"><p>فریب وعده نباشد ز انتظار چه حظ</p></div></div>