---
title: >-
    شمارهٔ ۲۰۷
---
# شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>این کعبه را بنا نه به باطل نهاده‌اند</p></div>
<div class="m2"><p>بس معنی و جمال درین گِل نهاده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درمانده گشته است به این کار و بال عقل</p></div>
<div class="m2"><p>هرسو هزار عقده مشکل نهاده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین گل چه دیده‌اند مگر حاملان عرش</p></div>
<div class="m2"><p>کز رنج ره به بادیه محمل نهاده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلزم به شور رفته و عمان نشسته تلخ</p></div>
<div class="m2"><p>زین آب زندگی که به ساحل نهاده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه این چه دوستی است که سرهای یکدگر</p></div>
<div class="m2"><p>خویشان بریده در ره قاتل نهاده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوارم مکن که ریختن آب روی را</p></div>
<div class="m2"><p>با خون صد شهید مقابل نهاده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر هرکه هوشیار بود اعتراض کن</p></div>
<div class="m2"><p>مستان قدم به بزم تو غافل نهاده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنمای رخ تمام که شاهانه چیده‌اند</p></div>
<div class="m2"><p>شاهان که خان به دعوت سایل نهاده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گردن بنه به تیغ «نظیری» که عاشقی</p></div>
<div class="m2"><p>بر سر کلاه مردم عاقل نهاده‌اند</p></div></div>