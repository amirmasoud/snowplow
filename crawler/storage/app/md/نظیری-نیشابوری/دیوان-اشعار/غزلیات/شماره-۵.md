---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>تو اگر ز کعبه راندی، و گر از بهشت ما را</p></div>
<div class="m2"><p>غم بنده پرور تو به دری نهشت ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو حدیث راست گویان به همه مذاق تلخیم</p></div>
<div class="m2"><p>به سفینه عزیزان نتوانیم نوشت ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل و برگ خانه ما همه بلبلان مستند</p></div>
<div class="m2"><p>که به عاشقی برآمد همه کار و کشت ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که نشست نیم ساعت بر ما زلال طبعان؟</p></div>
<div class="m2"><p>که ز پرده برنیامد همه خوب و زشت ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عتاب تلخ ساقی دل ما غبار دارد</p></div>
<div class="m2"><p>به حلاوت حریفان نتوان سرشت ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه روزه دست حسرت چو مگس ز دور لیسیم</p></div>
<div class="m2"><p>که سر آستین مهمان به شکر نهشت ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه صنم به جای بینی نه گلی به آب و رونق</p></div>
<div class="m2"><p>ز خطابه هم برآمد همه خاک و خشت ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تواضع جم و کی سر ما فرو نیاید</p></div>
<div class="m2"><p>که حدیث عشق و سودا شده سرنوشت ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به صداع غم «نظیری » ز خمار باده رستم</p></div>
<div class="m2"><p>نکند دماغ خوش بو گل صد بهشت ما را</p></div></div>