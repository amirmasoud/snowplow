---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>گردش چشم بتان مستی من حالی کرد</p></div>
<div class="m2"><p>دور وارون نتواند قدحم خالی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قبض درکار ندیدم چو شدم مست مدام</p></div>
<div class="m2"><p>حل هر عقده که می کرد به خوشحالی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پای جبریل به کرسی خیالم نرسد</p></div>
<div class="m2"><p>عشق بس پایه معراج مرا عالی کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شور این بادیه از بادیه گردیست مدام</p></div>
<div class="m2"><p>رخت مجنون به عدم برد و مرا والی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه بر خوان طمع دست نیازید رسید</p></div>
<div class="m2"><p>مگس آلوده شد از شه گران بالی کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجم در مجلس اصحاب به کارست که جنگ</p></div>
<div class="m2"><p>جای از خسته درونی و حزین نالی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم از خنده نوشین حریفان بگرفت</p></div>
<div class="m2"><p>گوشه یی کو که دل از گریه توان خالی کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قصه عشق به وصف تو طویل است طویل</p></div>
<div class="m2"><p>درک تفسیر جمالت خرد اجمالی کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یوسف از خواری اخوان به کسادی افتاد</p></div>
<div class="m2"><p>که فروشنده به پیش آمد و دلالی کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود نزدیک که کام از لب شیرین گیرد</p></div>
<div class="m2"><p>دست می یافت ظفر بخت بداقبالی کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کرد بازیچه معشوق «نظیری » خود را</p></div>
<div class="m2"><p>آن چه خردان نکنند او به کهن سالی کرد</p></div></div>