---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>نه فوت صحبت این دوستان غمی دارد</p></div>
<div class="m2"><p>نه مرگ مردم این عهد ماتمی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان این همه احباب پرده پوشی نیست</p></div>
<div class="m2"><p>دریده پرده ترست آن که محرمی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خوش بیانی هم صحبتان ز جای مرو</p></div>
<div class="m2"><p>که پر ز نیش بود هرکه مرهمی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هرزه دفتر امید هرکجا مگشا</p></div>
<div class="m2"><p>که مبتلای هوا کار درهمی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار لطمه ز هر خار بایدش خوردن</p></div>
<div class="m2"><p>نکوسرشتی اگر طبع خرمی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز طعن گرسنه چشمان دلیر ننماید</p></div>
<div class="m2"><p>هلال عید که ابروی پرخمی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کاوش مژه رگ های جانش بشکافد</p></div>
<div class="m2"><p>تنک دلی که چو من چشم پر نمی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خویش و اهل گذر کن که ملک بی خویشی</p></div>
<div class="m2"><p>برون ز عالم این خلق عالمی دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جاه و حشمت دنیا چرا قفا نکند</p></div>
<div class="m2"><p>کسی که همچو «نظیری » مسلمی دارد</p></div></div>