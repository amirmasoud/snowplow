---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>اخترشناس در روش بخت من گم است</p></div>
<div class="m2"><p>مشکل فتاده کار نه در دست انجم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوران صلای تفرقه داد و شراب ما</p></div>
<div class="m2"><p>یک پاره در صراحی و یک پاره در خم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی چو فیض اوست همه صرف او کنیم</p></div>
<div class="m2"><p>این جرعه ای که در ته جام تکلم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیرین نکرده خنده شادی مذاق کس</p></div>
<div class="m2"><p>گل نیز تلخ گشته زهر تبسم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشد به ناامیدی خویشم محبتی</p></div>
<div class="m2"><p>کو آشنای گوشه چشم ترحم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسودمی اگر به خودم کس گذاشتی</p></div>
<div class="m2"><p>از جور او کشنده ترم رحم مرده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناخن همیشه در جگر خاره می زنم</p></div>
<div class="m2"><p>دیریست رخش سعی مرا سنگ در سم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی سر ز کار بسته برآرم که چرخ را</p></div>
<div class="m2"><p>دوران نماند و رشته امید من گم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتار بی نتیجه «نظیری » نمی خرند</p></div>
<div class="m2"><p>عودی که سوزد و ندهد بوی هیزم است</p></div></div>