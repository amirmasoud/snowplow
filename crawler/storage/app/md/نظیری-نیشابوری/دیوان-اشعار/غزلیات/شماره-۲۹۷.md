---
title: >-
    شمارهٔ ۲۹۷
---
# شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>سر برآور بر کله داران قباها تنگ ساز</p></div>
<div class="m2"><p>روی بنما عاقل و دیوانه را یک رنگ ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه و درویش از دل و جان آرزومند تواند</p></div>
<div class="m2"><p>گر نسازی با پلاس فقر با اورنگ ساز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواست ایزد از دل سخت تو بنماید مثل</p></div>
<div class="m2"><p>با خلیل خویش گفتا کعبه را از سنگ ساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما به کلی بر تو ملک دل مسلم داشتیم</p></div>
<div class="m2"><p>حسن را بر تخت بنشان غمزه را سرهنگ ساز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو گستاخی است گفتن ترک بدخویی نمای</p></div>
<div class="m2"><p>با دل خود گفته ایم آیینه را بی زنگ ساز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موج حرمان بین و در کشتی آزادی نشین</p></div>
<div class="m2"><p>قهر دوران بین و عریانی سلاح جنگ ساز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار اگر جوری کند بر جبهه طالع نگار</p></div>
<div class="m2"><p>بخت اگر رحمی کند فهرست نام و ننگ ساز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صوفی و مطرب ز بانگت در خلاف افتاده اند</p></div>
<div class="m2"><p>یا صداع کس مده یا ناله را آهنگ ساز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما به ناخن تار و پود جسم از هم کنده ایم</p></div>
<div class="m2"><p>خواه تار سبحه گردان خواه زلف چنگ ساز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست با آسودگی چندان «نظیری » لذتی</p></div>
<div class="m2"><p>با لب پرخنده و با چشم پرنیرنگ ساز</p></div></div>