---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>یک باره در وفا برآور</p></div>
<div class="m2"><p>وین قهر قدیم را سر آور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا محرم کعبه صفا کن</p></div>
<div class="m2"><p>یا بر سر کوی بتگر آور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نقش بدیم خامه سر کن</p></div>
<div class="m2"><p>ور سطر کجیم مسطر آور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیراهن گل هزار رنگست</p></div>
<div class="m2"><p>رنگیش هم از وفا برآور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طوفان هزار موجه داری</p></div>
<div class="m2"><p>کشتی هزار لنگر آور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بد مستیم باده کم ده</p></div>
<div class="m2"><p>ور مخموریم ساغر آور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور از شر و شور ما به تنگی</p></div>
<div class="m2"><p>مجلس برچین و بستر آور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای هادی کعبه «نظیری »</p></div>
<div class="m2"><p>مؤمن بردیش کافر آور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امروز به رنگ دیگرش بر</p></div>
<div class="m2"><p>فرداش به نظر دیگر آور</p></div></div>