---
title: >-
    شمارهٔ ۴۵۲
---
# شمارهٔ ۴۵۲

<div class="b" id="bn1"><div class="m1"><p>رخ نما تا رونما جان آوریم</p></div>
<div class="m2"><p>عرضه کن ایمان که ایمان آوریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش کفر زلف و روی بت شکن</p></div>
<div class="m2"><p>حسن عهد و صدق پیمان آوریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی درمان برده ایم از راه درد</p></div>
<div class="m2"><p>هم ز راه درد درمان آوریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان جانان در میان جان بود</p></div>
<div class="m2"><p>از دم جان بوی جانان آوریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهد مضمون ما عنوان ماست</p></div>
<div class="m2"><p>شرح احوال پریشان آوریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قول ما آیینه اسرار ماست</p></div>
<div class="m2"><p>حجتی بر ذوق عرفان آوریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبلان هند ناخوش نغمه اند</p></div>
<div class="m2"><p>عندلیبی از خراسان آوریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست نیشابور کان خوش نمک</p></div>
<div class="m2"><p>این ملاحت زان نمکدان آوریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یار اگر سامان کار ما کند</p></div>
<div class="m2"><p>کار بی سامان به سامان آوریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاطری را کز پریشانی گریخت</p></div>
<div class="m2"><p>از پشیمانی پشیمان آوریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حسن بی اندازه و مقدار را</p></div>
<div class="m2"><p>عشق بی آغاز و پایان آوریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دفتر وحی «نظیری » در بغل</p></div>
<div class="m2"><p>رشگ بستان و گلستان آوریم</p></div></div>