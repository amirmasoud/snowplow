---
title: >-
    شمارهٔ ۴۹۶
---
# شمارهٔ ۴۹۶

<div class="b" id="bn1"><div class="m1"><p>حسن از خط شود قوی بازو</p></div>
<div class="m2"><p>یار نوخط خوش است و …ـو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نظر خط حجاب بردارد</p></div>
<div class="m2"><p>گرچه از خط نقاب سازد رو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرشدت به جوان که این مثل است</p></div>
<div class="m2"><p>تیر بهتر که پیر در پهلو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه خواهد کند به کعبه نماز</p></div>
<div class="m2"><p>من و محراب آن خم ابرو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موسی و طور، ما و کوچه یار</p></div>
<div class="m2"><p>هرکسی در رهی کند تک و پو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردن از زلف عرش پر زنار</p></div>
<div class="m2"><p>چهره از خال مصر پر جادو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشهد غمزه زایرش گفتار</p></div>
<div class="m2"><p>کعبه چهره حاجبش هندو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قد برافراخته چو شعله نار</p></div>
<div class="m2"><p>مغ آتش پرست هر سو مو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در همه شهر کافرستانی</p></div>
<div class="m2"><p>کس ندیدست چون سر آن کو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک و مال و خرد «نظیری » را</p></div>
<div class="m2"><p>همه یک سو و عشق او یک سو</p></div></div>