---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>ای از کرم نریخته خون سبیل را</p></div>
<div class="m2"><p>وز لطف عید کرده عزای خلیل را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ملک مصر یوسف کنعان به یاد تو</p></div>
<div class="m2"><p>دریای نیل ساخته چشم کحیل را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویی به غیر واسطه در گوش خاکیی</p></div>
<div class="m2"><p>رازی کز آن خبر نبود جبرئیل را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داده به کنج فقر نشان جنت النعیم</p></div>
<div class="m2"><p>کرده سبیل مشت گدا سلسبیل را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پل بسته حرز مهر تو بر معبر کلیم</p></div>
<div class="m2"><p>ای کرده باد قهر تو خون رود نیل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر فرد گشته حاکم این ملک غیر تو</p></div>
<div class="m2"><p>ناکرده گرم جا، زده کوس رحیل را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درویش و پادشه به وجود تا قایلند</p></div>
<div class="m2"><p>خرسند کرده یی تو عزیز و ذلیل را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بفزوده بر رسوم مقدر به حسن سعی</p></div>
<div class="m2"><p>وز معصیت نکاسته رزق کفیل را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچیم گر تو بازستانی متاع خویش</p></div>
<div class="m2"><p>دارد دو عالم از تو کثیر و قلیل را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قائل به عجز کشت ثنای تو هر که گفت</p></div>
<div class="m2"><p>در هستی تو ره نبود قال و قیل را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در تو به اجتهاد نظر کی توان رسید</p></div>
<div class="m2"><p>صد شبهه در ره است قیاس و دلیل را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>توحید حق بیان «نظیری » بلند ساخت</p></div>
<div class="m2"><p>برتر نهید پایه عرش جلیل را</p></div></div>