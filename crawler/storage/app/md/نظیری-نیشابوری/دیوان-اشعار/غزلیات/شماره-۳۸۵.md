---
title: >-
    شمارهٔ ۳۸۵
---
# شمارهٔ ۳۸۵

<div class="b" id="bn1"><div class="m1"><p>آنی به اثر داری و شأنی به تصرف</p></div>
<div class="m2"><p>دل ها نشود شیفته کس به تکلف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکر تو به وحدت برد از گفت مجازم</p></div>
<div class="m2"><p>هرچند که طبعم بگریزد ز تصوف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر قامت ما کسوت تقصیر بریدند</p></div>
<div class="m2"><p>تا زیب خداوند شود عفو و تلطف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب باز کشیدیم که مهر تو درآید</p></div>
<div class="m2"><p>پستان کرم شیر درآرد به توقف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غبن زمانی که به قید تو نبودم</p></div>
<div class="m2"><p>در خود به غضب بینم و در توبه تأسف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون گرسنه سفله به خوان تو رسیدم</p></div>
<div class="m2"><p>از لقمه بسوزم لب و کام و نکنم پف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستوری تو بیش کند شوق «نظیری »</p></div>
<div class="m2"><p>جز عصمت یوسف ندرد پرده یوسف</p></div></div>