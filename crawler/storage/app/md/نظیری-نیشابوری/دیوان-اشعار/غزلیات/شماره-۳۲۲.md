---
title: >-
    شمارهٔ ۳۲۲
---
# شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>تو کودکی به بزرگان زبان درازی بس</p></div>
<div class="m2"><p>به صید شیردلان قصد شاهبازی بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برای قبله اسلام کعبه ساخته اند</p></div>
<div class="m2"><p>نیاز خاک سر کوی خانه سازی بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شهر گرد به یک تاختن برآوردی</p></div>
<div class="m2"><p>ز رخش جور فرود آی تاکتازی بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خوب رو به هر آلایشی قبول دلی</p></div>
<div class="m2"><p>مساز جامه نمازی، رخ نمازی بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به روی معجزه خال مجاهدی که تو راست</p></div>
<div class="m2"><p>برای کشتن اهل فرنگ غازی بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان برد دل محمود چشم هندویت</p></div>
<div class="m2"><p>که با ایاز بگوید دگر ایازی بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قد چو چنگ نگویم که در کنارم گیر</p></div>
<div class="m2"><p>به نغمه ای که ز دورم همی نوازی بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیاز، شیوه ما عاجزان محتاج است</p></div>
<div class="m2"><p>تو را که حسن و جمالست بی نیازی بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقاب طلعت خورشید چند خواهی بود</p></div>
<div class="m2"><p>شبا! تو زلف نگارم نیی درازی بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو صبح بر مه و انجم خلاف می گرم</p></div>
<div class="m2"><p>همین که خصم شود پست سرفرازی بس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز کج قمار «نظیری » به راستی نبری</p></div>
<div class="m2"><p>به کم زنان دغاباز پاکبازی بس</p></div></div>