---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>دهل و نای دریدیم به آوازه صبح</p></div>
<div class="m2"><p>بانگ فتحی نشنیدیم ز دروازه صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیر گشتیم ز فیض سحر آگاه، دریغ</p></div>
<div class="m2"><p>جامه‌ای پاره نکردیم به اندازه صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کم خراشست دم مرغ سحر برخیزید</p></div>
<div class="m2"><p>جگری تازه کنیم از نمک تازه صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می و معشوق به اندازه ما می باید</p></div>
<div class="m2"><p>رطل خورشید کند چاره خمیازه صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مغفر جام بیارابرش می در زین کش</p></div>
<div class="m2"><p>تا به یک حمله کنم غارت جمازه صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپه شب به شبیخون دعا بشکستم</p></div>
<div class="m2"><p>علم روز زنم بر در دروازه صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفته اوراق شب و روز به هم برچینیم</p></div>
<div class="m2"><p>بخیه ثابت و سیاره شیرازه صبح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست در گردن عذرای جهان اندازم</p></div>
<div class="m2"><p>حله روز به هم برزنم و غازه صبح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرو و شمشاد به وجدند «نظیری » وقتست</p></div>
<div class="m2"><p>به سر شاخ سراییم سر آوازه صبح</p></div></div>