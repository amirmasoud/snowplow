---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>دیدمش در دل نهفتم آه بی‌تأثیر را</p></div>
<div class="m2"><p>در کمان از بس که دزدیدم شکستم تیر را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای رفتن نیست زین بزمم که در بیرون در</p></div>
<div class="m2"><p>بخت دارد در کمین هجر گریبان گیر را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشدل از غیرم که در بزم وصال او نیافت</p></div>
<div class="m2"><p>ذوق درد اضطراب و لذت تغییر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از کمند دادی گر آزارم خجل از من مباش</p></div>
<div class="m2"><p>کرده ام خاطرنشان خویش صد تقصیر را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشته دل پامال حسرت عشوه در کارش مکن</p></div>
<div class="m2"><p>قلب زراندود ما ضایع کند اکسیر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نگاهی شد «نظیری » صید و من در انفعال</p></div>
<div class="m2"><p>زان که آن وحشی نمی ارزد بهای تیر را</p></div></div>