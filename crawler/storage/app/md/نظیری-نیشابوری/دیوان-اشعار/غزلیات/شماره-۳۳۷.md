---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>خرامان آمد از می در سر آتش</p></div>
<div class="m2"><p>چو او آمد درآمد از در آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنفشه کرده خندان بر بناگوش</p></div>
<div class="m2"><p>چو بر طرف کله نیلوفر آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رنگ آمیزی آن زلف و رخسار</p></div>
<div class="m2"><p>سمندر کرده از خاکستر آتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبش افروخته از خنده مجمر</p></div>
<div class="m2"><p>ز عشقش سوخته عود تر آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هر سو هندوی آتش پرستی</p></div>
<div class="m2"><p>به گرد عارضش رقصان بر آتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برو پروانه جان افشان و از رشک</p></div>
<div class="m2"><p>فشاند شمع هر دم بر سر آتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر آن بت خلیل الله بسوزد</p></div>
<div class="m2"><p>برد بهر خوش آمد آزر آتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر انکار آورد، آن لب عجب نیست</p></div>
<div class="m2"><p>که روح الله زند در ما در آتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر دوزخ به آن لب برفروزند</p></div>
<div class="m2"><p>گل و ریحان شود بر کافر آتش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به جنت سوز عشقش گر نباشد</p></div>
<div class="m2"><p>شود بر مؤمن آب کوثر آتش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>«نظیری » کام دل از سوختن جوی</p></div>
<div class="m2"><p>شود پروانه را بال و پر آتش</p></div></div>