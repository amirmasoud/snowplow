---
title: >-
    شمارهٔ ۵۰۴
---
# شمارهٔ ۵۰۴

<div class="b" id="bn1"><div class="m1"><p>از گلستان گل به بازار آمده</p></div>
<div class="m2"><p>عید مرغان گرفتار آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نمی نالم به قانون بر حقم</p></div>
<div class="m2"><p>زخمه بیگانه بر تار آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیخته انده جهان را تا چو من</p></div>
<div class="m2"><p>مرد عشقی بر سر کار آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک دم از بتخانه غافل گشته ام</p></div>
<div class="m2"><p>صد گره در کار زنار آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قفس در باغ خونین دل ترم</p></div>
<div class="m2"><p>رشته ام ب خار دیوار آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انده انده زایدم کایینه را</p></div>
<div class="m2"><p>مایه زنگار زنگار آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستی ما را چه داند از کجاست</p></div>
<div class="m2"><p>آن که از میخانه هشیار آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست از مقصود کوته کرده ام</p></div>
<div class="m2"><p>بر سر انگشتم ز گل خار آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از «نظیری » شکرستان شد جهان</p></div>
<div class="m2"><p>در قفس طوطی به گفتار آمده</p></div></div>