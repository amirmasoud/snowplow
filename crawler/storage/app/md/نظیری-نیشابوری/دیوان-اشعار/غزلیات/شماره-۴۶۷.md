---
title: >-
    شمارهٔ ۴۶۷
---
# شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>درمان ضعف دل به لب نوشخند کن</p></div>
<div class="m2"><p>حرفی بگوی و مشک و گلابی به قند کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب پاک از ترشح آب حرام کرد</p></div>
<div class="m2"><p>طرف ردا به گردن صوفی کمند کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی عبوس عارف شهرم دماغ سوخت</p></div>
<div class="m2"><p>خادم بیار مجمر و فکر سپند کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهرم به رگ ز حاسد بدگوی می دود</p></div>
<div class="m2"><p>نیشم ز دل برآر و علاج گزند کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با ما بد است خصم که خود از چه خوب نیست</p></div>
<div class="m2"><p>گو اشتلم به طینت ناارجمند کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن کس که دین ندارد و گوید که عارفم</p></div>
<div class="m2"><p>تکفیر او به ملت هفتاد و اند کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کی چو موج آب به هر سو شتافتن</p></div>
<div class="m2"><p>در عین بحر پای چو گرداب بند کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقدت همه ز روی ریا قلب مانده است</p></div>
<div class="m2"><p>صراف خویش شو سخن چون و چند کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دشمن اگر به سفره تو میهمان شود</p></div>
<div class="m2"><p>سربخش و نام خویش به همت بلند کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آرایش برون چه کنی پشم گوسپند</p></div>
<div class="m2"><p>گرگی که در درونست تو را گوسپندکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>افغان که سوختی و به مرهم نمی خری</p></div>
<div class="m2"><p>آن را که داغ می نهی اول پسند کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عالی نموده عشق «نظیری » مقام تو</p></div>
<div class="m2"><p>معنی بلند آور و دعوی بلند کن</p></div></div>