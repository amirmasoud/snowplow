---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>گل خلعت نو داد دگر شاخ کهن را</p></div>
<div class="m2"><p>بر سلطنت حسن سجل ساخت چمن را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاخ گل خوش بو به ره باد سحرگاه</p></div>
<div class="m2"><p>بگشود سر نافه غزالان ختن را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد لاله به خمیازه به یاد می لعلت</p></div>
<div class="m2"><p>از باده لبالب چو قدح دید دهن را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افراخت صراحی سر و گردن به توجه</p></div>
<div class="m2"><p>تا خوش به کف مست دهده جام ذقن را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر تا قدم نی به تماشا نگران شد</p></div>
<div class="m2"><p>تا خوب دهد چنگ به مطرب برو تن را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حوران بهاری به نثار می و مطرب</p></div>
<div class="m2"><p>در بوسه گرفتند سراپای چمن را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عهد می و نغمه ز بس دید درستی</p></div>
<div class="m2"><p>سنبل ز خم جعد برون کرد شکن را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلبرگ بناگوش رخت بود مناسب</p></div>
<div class="m2"><p>گل دست شد و بست بر او زلف رسن را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر گوش خورد نعره احسنت «نظیری »</p></div>
<div class="m2"><p>پرسی اگر از مرده صدساله سخن را</p></div></div>