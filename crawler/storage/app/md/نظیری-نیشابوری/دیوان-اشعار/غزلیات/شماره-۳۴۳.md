---
title: >-
    شمارهٔ ۳۴۳
---
# شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>یا در درون قبه این آسمان مباش</p></div>
<div class="m2"><p>یا از حوادثی که رسد دل گران مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس را خط دوام فراقت نداده اند</p></div>
<div class="m2"><p>بار جهان اگر نکشی درجهان مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا میهمان میکده ای نقل و جام هست</p></div>
<div class="m2"><p>این تلخ و شور کم نشود بدگمان مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی مایگان بوالهوست قدربشکنند</p></div>
<div class="m2"><p>با دل توانگران بنشین رایگان مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دخل بقا به خرج فنا سر به سر نمای</p></div>
<div class="m2"><p>اگر در مقام سود نیی در زیان مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سایل که دل نشین ست گره بر جبین مزن</p></div>
<div class="m2"><p>مهمان که انگبین ست ترش میزبان مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیمرغ قاف شو که خردمند یابدت</p></div>
<div class="m2"><p>نادان فریب نغمه و پست آشیان مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم سبیل تست سبیل جهان مگرد</p></div>
<div class="m2"><p>جنت طفیل تست طفیل جنان مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آزار تو ز تست «نظیری » ز خود گریز</p></div>
<div class="m2"><p>خصمی تو به تست ز خود در امان مباش</p></div></div>