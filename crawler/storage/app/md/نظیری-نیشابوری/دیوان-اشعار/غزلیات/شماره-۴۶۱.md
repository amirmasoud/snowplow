---
title: >-
    شمارهٔ ۴۶۱
---
# شمارهٔ ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>دست در طره آشفته یاری نزدیم</p></div>
<div class="m2"><p>یادگاری گرهی بر سر تاری نزدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرممان باد که مشهور جهانیم به عشق</p></div>
<div class="m2"><p>نشدیم آتش و برقی به دیاری نزدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره دوست چو خاشاک دوا ریخته اند</p></div>
<div class="m2"><p>بر سر آبله ای نشتر خاری نزدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد صد سالک چالاک برین راه گذر</p></div>
<div class="m2"><p>دست در حلقه فتراک سواری نزدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه را زشتی و زیبایی ما در نظر است</p></div>
<div class="m2"><p>بخیه ای بر طرف پرده کاری نزدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه دادند و گرفتند در آن کوی نکوست</p></div>
<div class="m2"><p>بر ترازو و محک وزن و عیاری نزدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلوت انس «نظیری » نبود روزی ما</p></div>
<div class="m2"><p>حلقه‌ای بر در دل در شب تاری نزدیم</p></div></div>