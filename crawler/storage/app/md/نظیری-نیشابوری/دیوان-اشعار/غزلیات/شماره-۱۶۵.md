---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>هوس چو دیر کشد شعله در نهاد افتد</p></div>
<div class="m2"><p>به حد عشق رسد میل چون زیاد افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشاط صحبت فرهاد رشک خسرو داشت</p></div>
<div class="m2"><p>خوشست عشق اگر کار بر مراد افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شهر و باده فرسودم و کسم نخرید</p></div>
<div class="m2"><p>بلاست جنس گرانمایه در کساد افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو قیمتی نهدم روزگار بفروشید</p></div>
<div class="m2"><p>نه یوسفم که خریدار بر مراد افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا به دست تهی گوشه نقاب سپرد</p></div>
<div class="m2"><p>کم است آدم مفلس به اعتماد افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدنگ غمزه گره بر کمان ابرو چند</p></div>
<div class="m2"><p>گشاد ده که همه کارها گشاد افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عنان دل ز ملامت بتاب و دستم گیر</p></div>
<div class="m2"><p>که هر که را تو بگویی ز پا فتاد، افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ضمیر روشن تو لوح محو و اثبات است</p></div>
<div class="m2"><p>که تا ز یاد برآید که تا بیاد افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو ذره خلق جهان در هوات می گردند</p></div>
<div class="m2"><p>بشر ندیده کسی کافتاب زاد افتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تنم ز سیلی پند زمانه کاسته شد</p></div>
<div class="m2"><p>چو طفل شوخ که در قید اوستاد افتد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حذر ز آه «نظیری » که خانمان سوز است</p></div>
<div class="m2"><p>مباد این خس سوزان به دست باد افتد</p></div></div>