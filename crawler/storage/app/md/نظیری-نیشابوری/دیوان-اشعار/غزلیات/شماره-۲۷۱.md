---
title: >-
    شمارهٔ ۲۷۱
---
# شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>طلوع باده ز شام و سحر دریغ مدار</p></div>
<div class="m2"><p>ز خاک جرعه خود چون قمر دریغ مدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر به گنج سر بیل باغبان آید</p></div>
<div class="m2"><p>بگو که آب زر از جام زر دریغ مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیات تلخ بده عیش خوشگوار بگیر</p></div>
<div class="m2"><p>چو عشق تیغ کشد جان و سر دریغ مدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شکر آن که حدیثی چو انگبین داری</p></div>
<div class="m2"><p>ز سایلان ترش رو شکر دریغ مدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را به بینش کوتاه خویش نتوان دید</p></div>
<div class="m2"><p>مگر تو را به تو بینم نظر دریغ مدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درون جانی و در پرده ای ز مردم چشم</p></div>
<div class="m2"><p>جمال اگر ننمایی خبر دریغ مدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همیشه چشم به احسان آشنا دارد</p></div>
<div class="m2"><p>ز خاک کشته غربت گذر دریغ مدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جراحت دل شوریده خشک می گردد</p></div>
<div class="m2"><p>از آن دو زلف سیه مشک تر دریغ مدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیان شوق «نظیری » دراز انشایی است</p></div>
<div class="m2"><p>بیاض چهره ز خون جگر دریغ مدار</p></div></div>