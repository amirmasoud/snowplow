---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>فراق دوستان بسیار پیش آمد دل ما را</p></div>
<div class="m2"><p>غم بیرون گرفت از ما هوای منزل ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل افشان بود با تو هر بن خار و سر سنگی</p></div>
<div class="m2"><p>تو چون رفتی از اینجا آفتی زد حاصل ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عفاک الله به قید عشقم از هستی برآوردی</p></div>
<div class="m2"><p>به یک مشکل نمودی حل هزاران مشکل ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر مقبول اگر مردود حرف ما اثر دارد</p></div>
<div class="m2"><p>توان تعویذ بازو کرد سحر باطل ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرشت ما خواص مهر و طبع دوستی دارد</p></div>
<div class="m2"><p>برهمن بت نمی سازد مگر مشت کل ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه افسانه گیسو و رخسار تو می گویم</p></div>
<div class="m2"><p>شب ما نور می بخشد چراغ محفل ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشارت در گذر دادیم و شاهد در نظر داریم</p></div>
<div class="m2"><p>به دیدار تو چشم افتاد بخت مقبل ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین صحرا «نظیری » نیست لاغرتر ز ما صیدی</p></div>
<div class="m2"><p>که بر فتراک می بندد شکار بسمل ما را</p></div></div>