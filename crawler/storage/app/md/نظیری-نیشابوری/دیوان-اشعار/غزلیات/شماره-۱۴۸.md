---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>چگونه نام تو آریم بر زبان گستاخ</p></div>
<div class="m2"><p>که یاد تو نتوان کرد در نهان گستاخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر به گلبن تو بلبلی پناه آورد</p></div>
<div class="m2"><p>کسی نمی زندش گل بر آشیان گستاخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر ارجمند که در راه تو شهید شود</p></div>
<div class="m2"><p>هما نمی کندش قصد استخوان گستاخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر سؤالی از آب لب کنیم خیره ببخش</p></div>
<div class="m2"><p>به میزبان کریم است، میهمان گستاخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کعبه سجده عارف نمی کنند قبول</p></div>
<div class="m2"><p>اگر به دیر نهد پا بر آستان گستاخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محرمات حرمگاه های معبودند</p></div>
<div class="m2"><p>به مقتضای طبیعت مده عنان گستاخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب که جان به سلامت برند مغروران</p></div>
<div class="m2"><p>ستارگان قدرانداز و آسمان گستاخ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چگونه حرمت درویش پارسا ماند</p></div>
<div class="m2"><p>سؤال زشت و، غنی سخت دل، زبان گستاخ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مباد صاعقه بی نیازیی بجهد</p></div>
<div class="m2"><p>چنین مجوی «نظیری » ازو نشان گستاخ</p></div></div>