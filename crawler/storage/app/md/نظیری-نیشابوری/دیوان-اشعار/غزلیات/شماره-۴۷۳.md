---
title: >-
    شمارهٔ ۴۷۳
---
# شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>به گریه در دل تو گر اثر توان کردن</p></div>
<div class="m2"><p>تو را ز ذوق محبت خبر توان کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر به مهر من آب و گلت سرشته شود</p></div>
<div class="m2"><p>دل و زبان تو شیر و شکر توان کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قبول سلطنت هر دو کون چندان نیست</p></div>
<div class="m2"><p>که با محبت تو سر به سر توان کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پند مردم ازین راه برنمی گردم</p></div>
<div class="m2"><p>به جستجوی تو سر در خطر توان کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیان شوق به تقریر در نمی گنجد</p></div>
<div class="m2"><p>نمی شود که سخن مختصر توان کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نامه گر صفت اشتیاق بنویسم</p></div>
<div class="m2"><p>ز کاغذ و قلمم بال و پر توان کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دیده تا به دلم رفت گریه طوفان کرد</p></div>
<div class="m2"><p>گذر عجب گر ازین رهگذر توان کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علاج نیست که خصم از درون جان برخاست</p></div>
<div class="m2"><p>ز کید دشمن بیرون حذر توان کردن</p></div></div>