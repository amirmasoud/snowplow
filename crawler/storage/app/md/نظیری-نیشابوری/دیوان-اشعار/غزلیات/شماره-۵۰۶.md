---
title: >-
    شمارهٔ ۵۰۶
---
# شمارهٔ ۵۰۶

<div class="b" id="bn1"><div class="m1"><p>در بند تو زنجیر گرفتار شکسته</p></div>
<div class="m2"><p>زندان شده صد رخنه و دیوار شکسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین بیش شکرخنده حلاوت نفروشد</p></div>
<div class="m2"><p>طوطیم ز شکر سر منقار شکسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بس که عنان پیچد ازان چهره نگاهم</p></div>
<div class="m2"><p>خار مژه در دیده خونبار شکسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد قافله ناز گشوده به دلم بار</p></div>
<div class="m2"><p>سودای تو را رونق بازار شکسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیرون کنم از تن به سر ناخن غیرت</p></div>
<div class="m2"><p>این خار که در سینه افگار شکسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی جامه کنم پاره و نی سینه کنم چاک</p></div>
<div class="m2"><p>دیریست دل و دستم ازین کار شکسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خسته ز بیچارگی چاره گرانم</p></div>
<div class="m2"><p>اندوه طبیبان دل بیمار شکسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیمانه پیمان تو از بند عزیزان</p></div>
<div class="m2"><p>اندک شده پیوسته و بسیار شکسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیمان تو جای عجبی نیست «نظیری »</p></div>
<div class="m2"><p>خوش باش که عهد از طرف یار شکسته</p></div></div>