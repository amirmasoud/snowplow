---
title: >-
    شمارهٔ ۴۵۱
---
# شمارهٔ ۴۵۱

<div class="b" id="bn1"><div class="m1"><p>ضبط حرفی می‌کنم کز وی زبان می‌سوزدم</p></div>
<div class="m2"><p>شکوه‌ای در دل گره دارم که جان می‌سوزدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاس تن از دور می‌دارد شب هجر تو جان</p></div>
<div class="m2"><p>بس که از داغ جدایی استخوان می‌سوزدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جای شیون دود آهم از دهان سر می‌زند</p></div>
<div class="m2"><p>بس که از سوز درون بر لب فغان می‌سوزدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواستم شمعی که از وی خانه‌ام روشن شود</p></div>
<div class="m2"><p>وه چه دانستم که رخت و خانمان می‌سوزدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهربانان زودتر بخشید خونم را به او</p></div>
<div class="m2"><p>بی‌گناهم کشته و از بیم آن می‌سوزدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرده‌ام در بی‌خودی آهی که از وی دور باد</p></div>
<div class="m2"><p>کرد لب تب خال و از دل تا زبان می‌سوزدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از که می‌نالد «نظیری»؟ باز مرغ دام کیست؟</p></div>
<div class="m2"><p>عیب‌گویی‌های آن آتش‌بیان می‌سوزدم</p></div></div>