---
title: >-
    شمارهٔ ۲۵۸
---
# شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>حسن چندی سر به دل شوخی و خودرایی دهد</p></div>
<div class="m2"><p>شه چو گیرد مملکت اول به یغمایی دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده عاشق نیابد ذوق از دیدار دوست</p></div>
<div class="m2"><p>گر نه اول ترک دیدن های هرجایی دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لذت دشنامش از من پرس کاب تلخ و شور</p></div>
<div class="m2"><p>ذوق کوثر در مذاق مرد صحرایی دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردد از جان دادنم معلوم شوق روی دوست</p></div>
<div class="m2"><p>زان نمی میرم که ترسم مرگ رسوایی دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بیابان ها نمی گنجم اگر طغیان شوق</p></div>
<div class="m2"><p>بند بگشاید چو سیلم سر به شیدایی دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریه ما تلخ و طبع میزبان رنجش پذیر</p></div>
<div class="m2"><p>صوت مطرب بر دلش بگذار گیرایی دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکوه کمتر کن «نظیری » گر کسی یاری نکرد</p></div>
<div class="m2"><p>رخت ما سوزد چه نقصان تماشایی دهد</p></div></div>