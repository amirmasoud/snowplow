---
title: >-
    شمارهٔ ۵۱۵
---
# شمارهٔ ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>کجایی ای گل و مل را به رنگ و بو کرده</p></div>
<div class="m2"><p>جنان جمال تو نادیده آرزو کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلی به رنگ تو گلچین نچیده از چمنی</p></div>
<div class="m2"><p>هزار مرتبه گلزار رفت و رو کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار جنس می آورده در میان خمار</p></div>
<div class="m2"><p>به نشئه تو نبودست در سبو کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپاس خلق تو بر جان عاشقان فرضست</p></div>
<div class="m2"><p>پری وشان جهان را فرشته خو کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب از حلاوت حرفت نمی توانم بست</p></div>
<div class="m2"><p>که همچو نیشکرم شهد در گلو کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چگونه مردمک شب پرست ما بیند</p></div>
<div class="m2"><p>تو را که ذره و خورشید جستجو کرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو را به قول و غزل رام خویش نتوان کرد</p></div>
<div class="m2"><p>عبث خیال توام گرم گفتگو کرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو گل به جیب دگر کن که عشق چاره شناس</p></div>
<div class="m2"><p>نصیب سینه من مرهم و رفو کرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«نظیری » از ته دل خارخار غیر بکن</p></div>
<div class="m2"><p>که عشق آب نوی دیده را به جو کرده</p></div></div>