---
title: >-
    شمارهٔ ۳۱۲
---
# شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>به امید توام خرسند ازین پس</p></div>
<div class="m2"><p>نخواهم گشت حاجتمند ازین پس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بهتان گناهم سوخت دشمن</p></div>
<div class="m2"><p>به عصیانم نمی سوزند ازین پس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر در دل ملالی یابم از تو</p></div>
<div class="m2"><p>نخواهم تن به ناخن کند ازین پس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم از خانمان برکنده عشقت</p></div>
<div class="m2"><p>ندارم مهر بر فرزند ازین پس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بند نیستی دیدم دهانت</p></div>
<div class="m2"><p>به هستی نیستم دربند ازین پس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر از آغوش شمشادت گرفتم</p></div>
<div class="m2"><p>ز صرصر نشکنم پیوند ازین پس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنون خوش وقت باید بود با هم</p></div>
<div class="m2"><p>که داند زندگی تا چند ازین پس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تعلیم خردمندان نبودم</p></div>
<div class="m2"><p>بسم نابخردان را پند ازین پس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکر در مصر ارزان شد «نظیری »</p></div>
<div class="m2"><p>به کنعان می فرستم قند ازین پس</p></div></div>