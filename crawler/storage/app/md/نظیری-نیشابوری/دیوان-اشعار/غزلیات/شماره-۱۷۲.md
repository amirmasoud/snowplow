---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>دوشینه سرودی دل افگار برآورد</p></div>
<div class="m2"><p>کاهو ز حرم مرغ ز گلزار برآورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امسال دگر اشک صلاح و دم زهدم</p></div>
<div class="m2"><p>رنگ می پار و گل پیرار برآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من توبه نیاورده ام از کعبه که کافر</p></div>
<div class="m2"><p>بت از گرو خانه خمار برآورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنها نه مرا راه زد از بوالعجبی عشق</p></div>
<div class="m2"><p>بس شیخ که از خرقه و زنار برآورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کو سر خار ره ما بر کف پا خورد</p></div>
<div class="m2"><p>صد رنگ گل از گوشه دستار برآورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بد کرد به ما هر که در خلوت ما زد</p></div>
<div class="m2"><p>ما را ز سراپرده دیدار برآورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون کبک خرامنده به هر ره که گذشتی</p></div>
<div class="m2"><p>جولان تو طاووس ز رفتار برآورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس حلقه که زد بر در افلاک «نظیری »</p></div>
<div class="m2"><p>کاین صبح طرب را ز شب تار برآورد</p></div></div>