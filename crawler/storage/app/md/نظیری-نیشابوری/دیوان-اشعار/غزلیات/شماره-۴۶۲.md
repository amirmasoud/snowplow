---
title: >-
    شمارهٔ ۴۶۲
---
# شمارهٔ ۴۶۲

<div class="b" id="bn1"><div class="m1"><p>کنم بی باده بدمستی که سودایی دگر دارم</p></div>
<div class="m2"><p>به ساقی تلخ می گویم که دل جایی دگر دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظر گردد حجاب آنجا که من دیدار می بینم</p></div>
<div class="m2"><p>نهان از چشم ظاهربین تماشایی دگر دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به روی فهم ریزم مزد عقل کارفرما را</p></div>
<div class="m2"><p>که غیر از کار او بر سر تقاضایی دگر دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانم با که در حرفم همین مقدار می دانم</p></div>
<div class="m2"><p>که با خود هر نفس آشوب و غوغایی دگر دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حدیث طور از من پرس از محمل چه می پرسی</p></div>
<div class="m2"><p>که من پی بر پی مجنون صحرایی دگر دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مژگان ابر سیرابم بشارت کوه و صحرا را</p></div>
<div class="m2"><p>که در هر قطره آب دیده دریایی دگر دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه داند فهم کوته بال جولانگاه شوقم را</p></div>
<div class="m2"><p>که او راه دگر رفتست و من جایی دگر دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرد را نیست در سودای من یک ذره گنجایی</p></div>
<div class="m2"><p>که او رایی دگر کردست و من رایی دگر دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«نظیری » برتر از مطلب برآوردست همت را</p></div>
<div class="m2"><p>که برتر از تمنا من تمنایی دگر دارم</p></div></div>