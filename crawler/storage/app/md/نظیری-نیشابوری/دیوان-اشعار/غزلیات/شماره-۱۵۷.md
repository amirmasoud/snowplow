---
title: >-
    شمارهٔ ۱۵۷
---
# شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>در به روی عیش تا بستیم دیگر وانشد</p></div>
<div class="m2"><p>صد کلید آورد بخت و قفل این در وانشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گریبانی که غم آویخت کمتر شد درست</p></div>
<div class="m2"><p>خوش دلی کم دوخت جیبی را که یک سر وانشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا غم از ویرانه ما راه آمد شد گشود</p></div>
<div class="m2"><p>دیده شمع امید ما ز صرصر وانشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچنان مکتوب ناکامی به هم پیچیده ماند</p></div>
<div class="m2"><p>نامه سربسته ما هیچ جا سر وانشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعی کردم تا مگر از عشق پردازم دلی</p></div>
<div class="m2"><p>قطره خونابه ای از روی اخگر وانشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اضطراب از بهر جان بردن بسی پروانه کرد</p></div>
<div class="m2"><p>پیچ و تاب شعله اش از بال و از پر وانشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن که شب خواب «نظیری » را به افسون بست بست</p></div>
<div class="m2"><p>هیچ کار بسته او زان فسونگر وانشد</p></div></div>