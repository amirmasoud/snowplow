---
title: >-
    شمارهٔ ۲۱۸
---
# شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>شادی عشق تو هنگامه غم برهم زد</p></div>
<div class="m2"><p>شور حسنت نمکی بر جگر آدم زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب ز دیدار تو گردید به مهر آبستن</p></div>
<div class="m2"><p>جامه بر سنگ ز سور رخ تو ماتم زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهد لب های تو دکان طبیبان در بست</p></div>
<div class="m2"><p>دست در دامن تیغ نگهت مریم زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کعبه آمد حجرالاسود خالت بوسید</p></div>
<div class="m2"><p>غوطه در موجه چاه ذقنت زمزم زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا قضا خال بهشتی جمال تو بدید</p></div>
<div class="m2"><p>شست آن خال که بر ناصیه آدم زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سخندانی تو طفل ندیدست کسی</p></div>
<div class="m2"><p>گره اعجاز لبت بر نفس مریم زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق دوشاب دل آن روز که سودا می پخت</p></div>
<div class="m2"><p>مایه مهر برین شیره جان ها کم زد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوش می خواست قدم بر من افتاده نهد</p></div>
<div class="m2"><p>کند خاک من و بر دیده نامحرم زد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دولت از فیض دم صبح «نظیری » دریافت</p></div>
<div class="m2"><p>در به غواص ندادند که بی جا دم زد</p></div></div>