---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>حضور وقت نمی‌یابم و حلاوت فرض</p></div>
<div class="m2"><p>دلم به قهر تو رهن است و جان ز لطف تو قرض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هم برآمده از شوخی تو اوقاتم</p></div>
<div class="m2"><p>نه سنتم ز تو سنت بود نه فرضم فرض</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک حجاب دعایم نمی شود اما</p></div>
<div class="m2"><p>به غمزه حاجت ابرو نمی نماید فرض</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن که از دل شوریده بر زبان آید</p></div>
<div class="m2"><p>به رسم تحفه ملک بر سما برد از ارض</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به شکر نعمت تو برنمی توانم خاست</p></div>
<div class="m2"><p>که تا به گردنم از بار منتت در قرض</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مثال ما گل خندان و سرو آزادست</p></div>
<div class="m2"><p>درین حدیقه به طولست عیش ما نه به عرض</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فضل اوست «نظیری » چو مزد کار آخر</p></div>
<div class="m2"><p>معلم ملکوتت به علم کردم فرض</p></div></div>