---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>ز بس بود دل خود کام ناسپاس مرا</p></div>
<div class="m2"><p>ز روی هم رسد اندوه بی قیاس مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلا مقام مرا پیش ازین نمی دانست</p></div>
<div class="m2"><p>غم تو کرد درین شهر رو شناس مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه روز بود که تشریف عشق پوشیدم</p></div>
<div class="m2"><p>که خوشدلی نشناسد درین لباس مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز رشک دوش چنان درهمم که نتوان برد</p></div>
<div class="m2"><p>به بزم وصل تو امشب به التماس مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخی که داشت ملک میلش از توجه غیر</p></div>
<div class="m2"><p>چنان نمود به چشمم که شد هراس مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن ز آه «نظیری » فراغتی داری</p></div>
<div class="m2"><p>کزین فسرده دلان کرده ای قیاس مرا</p></div></div>