---
title: >-
    شمارهٔ ۵۶۰
---
# شمارهٔ ۵۶۰

<div class="b" id="bn1"><div class="m1"><p>نه گلم کفاف رنگی نه گلم پسند بویی</p></div>
<div class="m2"><p>سر و برگ خود ندارم ز خیال روی و مویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تصور جمالش ز هزار فکر رستم</p></div>
<div class="m2"><p>دل جمع من پریشان نشود به هیچ سویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شده با بدان مصاحب چه فسون کنم ندانم</p></div>
<div class="m2"><p>که به طبع دیو مردم نشود فرشته خویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم دل چگونه پوشم که لباس صبر و طاقت</p></div>
<div class="m2"><p>ز هم آنچنان دریدم که نمی شود رفویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفحات زلف جانان چو بلا مسلسل آمد</p></div>
<div class="m2"><p>که نفس ز هم گسست و نگسست گفتگویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سگ خانه زاد عشقم به جفا نمی گریزم</p></div>
<div class="m2"><p>نه شکاری غریبم که جهم به های و هویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکند قبولم آتش پس مرگ اگر بسوزند</p></div>
<div class="m2"><p>اگرم ز لوث عصیان ندهند شست و شویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شده عرصه از زبونان صف پردلی نبینم</p></div>
<div class="m2"><p>که ز صولجان مردی بدرآوریم گویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسنات دین پرستان همه سیئات باشد</p></div>
<div class="m2"><p>مگرآن که روز محشر نکنند جستجویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن ار ز دوست باشد بردم برون ز دنیا</p></div>
<div class="m2"><p>دل پر هزار حسرت به امید بازگویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه غمت ز دیده من که به خون دل نگار است</p></div>
<div class="m2"><p>تو که بر کنار جویی نشکسته ای سبویی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز غلوی اضطرابم ز حجاب برنیاید</p></div>
<div class="m2"><p>ز صد آرزو «نظیری » رسم ار به آرزویی</p></div></div>