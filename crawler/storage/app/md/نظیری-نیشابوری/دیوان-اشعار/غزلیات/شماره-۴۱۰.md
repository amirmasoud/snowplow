---
title: >-
    شمارهٔ ۴۱۰
---
# شمارهٔ ۴۱۰

<div class="b" id="bn1"><div class="m1"><p>ما به برهان و خبر پیرو ترسا نشویم</p></div>
<div class="m2"><p>تا رخ بت نپرستیم شکیبا نشویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تماشای تو چون آینه گم گردیدیم</p></div>
<div class="m2"><p>که ز پیدایی دیدار تو پیدا نشویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر بر لب چو سر کیسه ممسک زده ایم</p></div>
<div class="m2"><p>تا سر شیشه می وانشود، وانشویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرمه در دیده دل تا نکشد لطف حکیم</p></div>
<div class="m2"><p>گر سراپای شود دیده که بینا نشویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگذر بودن حسن گل و خوبی بهار</p></div>
<div class="m2"><p>گوشمالی است که مشغول تماشا نشویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابتلاهای عزیزان همه زآنست که ما</p></div>
<div class="m2"><p>غره مهلت ده روزه دنیا نشویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقش امید به صد دوزخ و دریا، شستیم</p></div>
<div class="m2"><p>تا دگر مصدر هر عرض تمنا نشویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرود خامه تکلیف خرد، از سر ما</p></div>
<div class="m2"><p>تا چو سودای جنون، بی سر و بی پا نشویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قیمت خاک در آن کوی، به افلاک رسید</p></div>
<div class="m2"><p>ما ندانیم چه نرخیم، که بالا نشویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگذارید که در تنگ شکر، گم گردیم</p></div>
<div class="m2"><p>کان پشیزیم که بیعانه سودا نشویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در محبت دل و دین باختن اول قدم است</p></div>
<div class="m2"><p>ما «نظیری» ز تو خرسند به این‌ها نشویم</p></div></div>