---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>فارغ تر از دل تو ندیدم دلی دگر</p></div>
<div class="m2"><p>ایزد تو را سرشته ز آب و گلی دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مرغ سدره را بکشی مایلی که باز</p></div>
<div class="m2"><p>در خاک و خون طپیده شود بسملی دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر مشکلی که عاجزی ما بیان کند</p></div>
<div class="m2"><p>آسان کنی که پیش نهی مشکلی دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آب و گل غرض شجر قامت تو بود</p></div>
<div class="m2"><p>عالم نداد بهتر ازین حاصلی دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نور محفل تو جهان درگرفته است</p></div>
<div class="m2"><p>نفروخته چراغ تو از محفلی دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاطر به منتهای جمالت نمی رسد</p></div>
<div class="m2"><p>دارم به هر مشاهده ات منزلی دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ماهتاب روی که غیر از جمال دوست</p></div>
<div class="m2"><p>دریای عشق را نبود ساحلی دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مستان اساس میکده زیبا نهاده اند</p></div>
<div class="m2"><p>رسمی اگر ز نو ننهد عاقلی دگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساقی قدح به کف تو «نظیری » نظر بغیر</p></div>
<div class="m2"><p>دوران ندیده است چو تو غافلی دگر</p></div></div>