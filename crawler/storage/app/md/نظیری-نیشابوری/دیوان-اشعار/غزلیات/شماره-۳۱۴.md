---
title: >-
    شمارهٔ ۳۱۴
---
# شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>ما به دل شادیم از باغ و بهار ما مپرس</p></div>
<div class="m2"><p>در جهان عشق زادیم از دیار ما مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش در یک بزم با او تا سحر می خورده ایم</p></div>
<div class="m2"><p>نرگس مخمور او بین وز خمار ما مپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شکایت بود از فرقت به خلوت گفته شد</p></div>
<div class="m2"><p>از تلافی های بخت حق گذار ما مپرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت ما آیینه رخساره معشوق ماست</p></div>
<div class="m2"><p>حسن روی او نگر از روزگار ما مپرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم گریان آوریم و جان پر حسرت بریم</p></div>
<div class="m2"><p>گو کس از آغاز و از انجام کار ما مپرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خلاص امتحان صدبار آتش دیده ایم</p></div>
<div class="m2"><p>نقد دارالضرب عشقیم از عیار ما مپرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما ضعیفان قصد منزلگاه عنقا کرده ایم</p></div>
<div class="m2"><p>از هزار ما یکی ماند شمار ما مپرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فضل او چون ما بسی را بی سبب بخشیده است</p></div>
<div class="m2"><p>باعث آمرزش آمرزگار ما مپرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصه ما را «نظیری » نیست هرگز انتها</p></div>
<div class="m2"><p>بحر بی پایان عشقیم از کنار ما مپرس</p></div></div>