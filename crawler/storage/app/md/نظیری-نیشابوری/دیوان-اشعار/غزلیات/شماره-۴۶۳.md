---
title: >-
    شمارهٔ ۴۶۳
---
# شمارهٔ ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>عشق تو شیرازه اجزای من</p></div>
<div class="m2"><p>شوق تو فهرست سراپای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسمله گوشه ابروی تست</p></div>
<div class="m2"><p>فاتحه شرح تمنای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رابطه بند به بندم ز تست</p></div>
<div class="m2"><p>ثبت به داغت شده اعضای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کعبه کوی تو بود مرجعم</p></div>
<div class="m2"><p>بر سر معراج بود پای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردمک چشم جهانی ز تست</p></div>
<div class="m2"><p>روشنی دیده بینای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شکرستان تو اجری خورست</p></div>
<div class="m2"><p>طوطی گویای شکرخای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چمن حسن تو بیرون مباد</p></div>
<div class="m2"><p>زمزمه بلبل گویای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این قدر ارزم که به هیچم خری</p></div>
<div class="m2"><p>بیم زیان نیست ز سودای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این شرفم بس که شوی مشتری</p></div>
<div class="m2"><p>هیچ مده قیمت کالای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس ز رفیقان ره افتاده ام</p></div>
<div class="m2"><p>گر نکنی رحم به من وای من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جای «نظیری » دگر اینجا کجاست؟</p></div>
<div class="m2"><p>من شده نو آمده بر جای من</p></div></div>