---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>بر قفا چشمت نمی افتد چو این در واشود</p></div>
<div class="m2"><p>آن زمان درگاه بشناسی که صدرت جا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن که او در کلبه احزان پسر گم کرد یافت</p></div>
<div class="m2"><p>تو که چیزی گم نکردی از کجا پیدا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوست دارد از غریبان ناله بیچارگی</p></div>
<div class="m2"><p>عشق می خواهد که کشتی غرق در دریا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه می خواهد که منشور خراباتش دهند</p></div>
<div class="m2"><p>باید اول خانمان برهم زن و رسوا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زو همه خوبی زما زشتی همانا لایق است</p></div>
<div class="m2"><p>پرده ما بسته ماند پرده او واشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد بهار عمر و ناپخته است انگورم هنوز</p></div>
<div class="m2"><p>نیست معلومم که آخر سرکه یا صهبا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمره آن کو برآرم، پایم ار آید به کار</p></div>
<div class="m2"><p>حلقه آن دربگیرم دستم ار گیرا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کم «نظیری » راست بر جایی نظر افگنده ام</p></div>
<div class="m2"><p>وای اگر روز جزا چشم و دلم گویا شود</p></div></div>