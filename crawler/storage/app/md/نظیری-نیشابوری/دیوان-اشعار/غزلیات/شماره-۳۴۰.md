---
title: >-
    شمارهٔ ۳۴۰
---
# شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>شرمسارم از دل بی صبر و بی آرام خویش</p></div>
<div class="m2"><p>خود به یار از بی قراری می برم پیغام خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جهان درد و غم فرمان روا بنشسته ام</p></div>
<div class="m2"><p>در کمال اوج طالع بر فراز بام خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود ز خود ساغر ستانم خود به خود ساقی شوم</p></div>
<div class="m2"><p>از کف نوشین لبی هرگز نگیرم جام خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عود مطرب تر، دم نی سرد، حیران مانده ام</p></div>
<div class="m2"><p>بر کدامین آتش اندازم کباب خام خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج در ویرانه دارم، با پری در خلوتم</p></div>
<div class="m2"><p>سایه ای هست از جنون تا من نگردم رام خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد «نظیری » عاقبت فرخنده از لطف ازل</p></div>
<div class="m2"><p>فال نیک صبح همره داشت مزد شام خویش</p></div></div>