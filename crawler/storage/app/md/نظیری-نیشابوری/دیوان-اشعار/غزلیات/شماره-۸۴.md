---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>فخر والانسبتان از بند اوست</p></div>
<div class="m2"><p>آنچه هرگز نگسلد پیوند اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردن شمشاد را زلفش بخست</p></div>
<div class="m2"><p>سرو از آزادگان بند اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه شکل نیستی دارد دهانش</p></div>
<div class="m2"><p>هستی جان ها ز شکرخند اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقض زلفش دایه بر عهدش شکست</p></div>
<div class="m2"><p>گر شکستی هست در سوگند اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طره اش را هست پیوندی به صلح</p></div>
<div class="m2"><p>چین ابرو گرچه خویشاوند اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر شبم بی خواست می آید به خواب</p></div>
<div class="m2"><p>بسترم مشگ و عبیرآگند اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشربش صفرای بیماران شکست</p></div>
<div class="m2"><p>بوسه می خوش از ترنج و قند اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زود آمیزش به مردم می کند</p></div>
<div class="m2"><p>مشک را بو اندکی مانند اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن گل بر باد حرمان زود رفت</p></div>
<div class="m2"><p>هر که تمکینی ندارد بند اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کینه کش از دوستان مهرجوی</p></div>
<div class="m2"><p>طبع مغرور ز خود خرسند اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بار تکلیفش ز دوش انداختم</p></div>
<div class="m2"><p>گو بکش هرکس که حاجتمند اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ظلم اخوان بر «نظیری » می کنند</p></div>
<div class="m2"><p>معنی او بهتر از فرزند اوست</p></div></div>