---
title: >-
    شمارهٔ ۴۰۰
---
# شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>گر کشف حجب خواهی بستان می ناب اول</p></div>
<div class="m2"><p>ور علم ازل جویی بگذر ز کتاب اول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق مکش دفتر کاسرار لدنی را</p></div>
<div class="m2"><p>گویند به وحی آخر آرند به خواب اول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهی به یکی آری دل را ز پریشانی</p></div>
<div class="m2"><p>در معبد بت رویی چندیش بتاب اول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا صاف ملایک را بر خاک تو پیمایند</p></div>
<div class="m2"><p>در مدرسه بر سرکش دردی شراب اول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حلقه نمی گنجی تا پخته نمی گردی</p></div>
<div class="m2"><p>شرط است که میخواران سازند کباب اول</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاید به شب ظلمت رب ارنی گوییم</p></div>
<div class="m2"><p>ما را به لب ساغر رفتست خطاب اول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا هست دمی باقی محروم مکن ساقی</p></div>
<div class="m2"><p>می ها به خم افکندیم با تو به حجاب اول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را به صد افسانه در خواب چو می کردی</p></div>
<div class="m2"><p>از بهر چه می کردی بیدار ز خواب اول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پیری و محرومی خوردیم می و خفتیم</p></div>
<div class="m2"><p>گر دور ز سرگیری زین پیر خراب اول</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سهلست اگر کاری برعکس صواب افتد</p></div>
<div class="m2"><p>چون وضع جهان کردند از روی شتاب اول</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیش از همه می بارد بر کشت «نظیری » را</p></div>
<div class="m2"><p>کو تخم نمی کارد بر فکر سحاب اول</p></div></div>