---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>افسانه شیرین مرا گوش نکردند</p></div>
<div class="m2"><p>صد تلخ چشیدم شکری نوش نکردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک خرده گرفتند پس از نکته بسیار</p></div>
<div class="m2"><p>گشتیم فراموش و فراموش نکردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما روزه ازین مائده بر خشک گشادیم</p></div>
<div class="m2"><p>در کاسه ما جرعه سر جوش نکردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معلوم شد از مستی ما حوصله ما</p></div>
<div class="m2"><p>دادند به حکمت می و بیهوش نکردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باید به عصا رفت چو موسی که درین راه</p></div>
<div class="m2"><p>یک چاه نکندند که خس پوش نکردند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حلقه شدم زان خط رخسار و قرینم</p></div>
<div class="m2"><p>با کوکب آن صبح بناگوش نکردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانم به ره پردگیان سحری سوخت</p></div>
<div class="m2"><p>سویم نگهی از ته شب پوش نکردند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خونابه به بو آمده بر جیب و کنارم</p></div>
<div class="m2"><p>زان سنبل خوش بوم در آغوش نکردند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امروز نه رحمست که لب تشنه گذارند</p></div>
<div class="m2"><p>آن را که لبی تر ز می دوش نکردند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فریاد ازین شوق که در جان «نظیری » است</p></div>
<div class="m2"><p>تا مردنش از زمزمه خاموش نکردند</p></div></div>