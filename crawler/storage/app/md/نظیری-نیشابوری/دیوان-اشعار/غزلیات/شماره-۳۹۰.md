---
title: >-
    شمارهٔ ۳۹۰
---
# شمارهٔ ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>لب ساقی روان‌ها، دل چشمه حقایق</p></div>
<div class="m2"><p>لفظ آفتاب روشن، معنیش صبح صادق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سخت گیری تو، مرتد شود مسلمان</p></div>
<div class="m2"><p>وز راست گویی تو مؤمن شود منافق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چاه ذقن به خوبی معراج ماه کنعان</p></div>
<div class="m2"><p>گیسو کلام ملهم، رخسار حق ناطق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی جذبه دلیلی از خود نمی توان رست</p></div>
<div class="m2"><p>کاریست با صعوبت عقلی است ناموافق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عونا تجدک روحی یا مظهرالعجائب</p></div>
<div class="m2"><p>اکشف هموم قلبی یا کاشف الدقایق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی نور تو هیولا صورت نمی پذیرد</p></div>
<div class="m2"><p>لولاک فی وجود مایخلق الخلایق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اصحاب پیش چشمت دنیا و دین نهادند</p></div>
<div class="m2"><p>گوید قبول و ردت زین هر دو چیست لایق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پیر و شیخ و مرشد کاری نمی گشاید</p></div>
<div class="m2"><p>دریابم از عنایت برهانم از علایق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخر ترحمی کن بر زاری «نظیری »</p></div>
<div class="m2"><p>مهرت شفای دل ها لطفت طبیب حاذق</p></div></div>