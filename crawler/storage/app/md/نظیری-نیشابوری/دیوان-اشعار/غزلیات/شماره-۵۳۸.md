---
title: >-
    شمارهٔ ۵۳۸
---
# شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>به توتیای رهم خون ز چشم تر چیدی</p></div>
<div class="m2"><p>جراحت از دل هجران خلیده برچیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیث خوش نمک ارمغان صحبت کیست؟</p></div>
<div class="m2"><p>که درد از دل و آزارم از جگر چیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از فراق تو مردم تو را چه حاصل شد</p></div>
<div class="m2"><p>به غیر از اینکه گل شهرت از سفر چیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همیشه جلوه طراز رقیب بی روشی</p></div>
<div class="m2"><p>کدام میوه ازان نخل بارور چیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز لغوگوییی هم صحبتان دلت نگرفت</p></div>
<div class="m2"><p>اگرچه بال مگس دایم از شکر چیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبا ز گلشنم آزرده حال می گذرد</p></div>
<div class="m2"><p>چو شاخ گل به رگم داغ نیشتر چیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دست غارت تو آن درخت عریانم</p></div>
<div class="m2"><p>که از مقام خودش کندی و ثمر چیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز هیچ سو رخ آسودگی نمی بینم</p></div>
<div class="m2"><p>ز بس که مشغله بر روی یکدگر چیدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ما دو گونه نیلوفری فتاده چو تو</p></div>
<div class="m2"><p>به رخ بنفشه شام و گل سحر چیدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشد که طعمه زاغی دهی همایم را</p></div>
<div class="m2"><p>گرم چه صد بر دولت ز بال و پر چیدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز گریه سحری یافتی «نظیری » فیض</p></div>
<div class="m2"><p>گل مراد به اقبال چشم تر چیدی</p></div></div>