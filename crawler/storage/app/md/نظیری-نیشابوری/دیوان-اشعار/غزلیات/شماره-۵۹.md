---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>ادب گرفته عنان خمار و مستی ما</p></div>
<div class="m2"><p>برابر است بلندی ما و پستی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خود ز دوست نیابیم تا ز می مستیم</p></div>
<div class="m2"><p>تمام دوست پرستی است می پرستی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار ساغر دیدار شد تهی و هنوز</p></div>
<div class="m2"><p>فرود حوصله ماست شوق و مستی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خمار شام ندارد صبوح ما هرگز</p></div>
<div class="m2"><p>به یک طلوع بود نشئه الستی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مثال صورت موهوم بی نشان بودیم</p></div>
<div class="m2"><p>به منظر تو کشیدند نقش هستی ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز حقه کهرت کام برنمی آید</p></div>
<div class="m2"><p>ز تنگی دهن تست تنگ دستی ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز گونه‌های «نظیری» تپانچه پوست بریخت</p></div>
<div class="m2"><p>عذار دف نخورد ضربت دو دستی ما</p></div></div>