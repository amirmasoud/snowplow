---
title: >-
    شمارهٔ ۲۲۴
---
# شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>درد و غمت که همچو هما استخوان خورند</p></div>
<div class="m2"><p>بر من مبارکند گرم مغز جان خورند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر نامه ام مخند که آشفته خاطران</p></div>
<div class="m2"><p>مو کز قلم کشند نی اندر بنان خورند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست آئییم به صلح اگر نکهتی بری</p></div>
<div class="m2"><p>زان می که در محبت هم دوستان خورند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیشکر آن چنان نخورد کس ز دست دوست</p></div>
<div class="m2"><p>کازادگان ز دست مبارز سنان خورند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانی و صد کرشمه مژگان چه می کنم</p></div>
<div class="m2"><p>این تیرها تمام اگر بر نشان خورند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم هزار تشنه جگر در کمین تست</p></div>
<div class="m2"><p>ترسم که خام میوه این بوستان خورند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آزادگان به جای رسیدند و ما همان</p></div>
<div class="m2"><p>زان رهروان که گرد پی کاروان خورند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرجا گلی است بهر «نظیری » طربگهی است</p></div>
<div class="m2"><p>کی بلبلان مست غم آشیان خورند</p></div></div>