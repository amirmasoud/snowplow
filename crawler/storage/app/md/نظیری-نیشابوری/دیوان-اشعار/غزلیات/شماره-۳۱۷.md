---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>ای که نامه می‌نویسی سوی من فرمان نویس</p></div>
<div class="m2"><p>خدمتی کز دست می‌آید مرا بر جان نویس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوستان تا نامه واکردن پریشان می‌شوند</p></div>
<div class="m2"><p>لطف فرما هرکسی را نام بر عنوان نویس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند عرض آبرومندی به نام کشوری</p></div>
<div class="m2"><p>در سر نامه نمی گنجیم بر پایان نویس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد جور خویش و پیمان درست ما بگرد</p></div>
<div class="m2"><p>هرکجا در جور نقصی هست بر پیمان نویس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر در آیینه نمی خواهی که بینی مثل خویش</p></div>
<div class="m2"><p>آیتی از رشک و عنبر بر مه تابان نویس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرمی سودای ما تا هست این بازار هست</p></div>
<div class="m2"><p>چشم من افشانگرست و روی تو ریحان نویس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلک روح افزای را در پرسش در رنجه ساز</p></div>
<div class="m2"><p>یعنی از بهر «نظیری» نسخه درمان نویس</p></div></div>