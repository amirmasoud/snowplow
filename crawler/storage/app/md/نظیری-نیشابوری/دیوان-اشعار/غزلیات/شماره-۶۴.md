---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>عشق دهد با دل شوریده تاب</p></div>
<div class="m2"><p>پرورش ذره کند آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کم نشود سوز دل از سیل اشک</p></div>
<div class="m2"><p>آتش سودا ننشیند به آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه که عاشق کشد از خامی است</p></div>
<div class="m2"><p>دود کند دل چو نباشد کباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با سخن تلخ تبسم خوش است</p></div>
<div class="m2"><p>نشئه دهه شهد چون گردد شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیر رود جان که تویی در دلم</p></div>
<div class="m2"><p>شعله کند بر سر شمع اضطراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شب هجران نبود روشنی</p></div>
<div class="m2"><p>گرچه بود تا به سحر ماهتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده «نظیری » نشناسد رخش</p></div>
<div class="m2"><p>بس که گدازد نگهم از حجاب</p></div></div>