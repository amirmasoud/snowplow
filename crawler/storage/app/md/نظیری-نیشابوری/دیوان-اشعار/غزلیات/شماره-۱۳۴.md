---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>دل که جمعست غم بی سر و سامانی نیست</p></div>
<div class="m2"><p>فکر جمعیت اگر نیست پریشانی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیضه در جنگل شهباز نهد طایر من</p></div>
<div class="m2"><p>در فضایی که منم بال و پرافشانی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کنم یاد ز بتخانه مرا عیب مکن</p></div>
<div class="m2"><p>هر که را حب وطن نیست مسلمانی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاابالی شو و دریاب فراخی نشاط</p></div>
<div class="m2"><p>چند در تنگی مشرب که فراوانی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست لذت ز نظربای بزمی که در او</p></div>
<div class="m2"><p>خنده زیر لب و گریه پنهانی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک ادبار به تاج سر جم دوخته اند</p></div>
<div class="m2"><p>هیچ سر نیست کش این نیل به پیشانی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر در خلوت ما فر هما می بخشند</p></div>
<div class="m2"><p>هدهدی را که به سر تاج سلیمانی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طبل درویشی ما بر در جاوید زدند</p></div>
<div class="m2"><p>بر لب بام بجز نوبت سلطانی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صحبت آینه طبعان به دمی تیره شود</p></div>
<div class="m2"><p>در چنین بزمگهی جای گرانجانی نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو به معموره مصری و من مجنون را</p></div>
<div class="m2"><p>نبرد شوق بدان کوچه که ویرانی نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از فسون دانی چشمان سیاهی که توراست</p></div>
<div class="m2"><p>صد «نظیری » به نگه داشتن ارزانی نیست</p></div></div>