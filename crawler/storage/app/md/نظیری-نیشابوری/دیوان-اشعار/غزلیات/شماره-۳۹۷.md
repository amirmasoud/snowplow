---
title: >-
    شمارهٔ ۳۹۷
---
# شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>نقش دیبا چنان کشید فرنگ</p></div>
<div class="m2"><p>که ز من برد دانش و فرهنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفر از عشق و عشق از ایمان</p></div>
<div class="m2"><p>چیست این فتنه ها و این نیرنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمزمم سوخته است گو هندو</p></div>
<div class="m2"><p>مشت خاکسترم فشان بر گنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وه که بر ما نوشته باده فروش</p></div>
<div class="m2"><p>باده را سنگ و جام را پا سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند کورانه دست اندازیم</p></div>
<div class="m2"><p>دامن کس نیاید اندر چنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زو همه نقش ها و او بی نقش</p></div>
<div class="m2"><p>زو همه رنگ ها و او بی رنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گله در دوستی نمی گنجد</p></div>
<div class="m2"><p>بس که شد راه دوستداری تنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به قضا تن دهم که در دریا</p></div>
<div class="m2"><p>شادی گوهرست و خوف نهنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو مکن ضرب زخمه را خارج</p></div>
<div class="m2"><p>گر «نظیری » غلط کند آهنگ</p></div></div>