---
title: >-
    شمارهٔ ۵۲۱
---
# شمارهٔ ۵۲۱

<div class="b" id="bn1"><div class="m1"><p>ازین مجلس نمی خیزد دمی کاحیا کند گوشی</p></div>
<div class="m2"><p>پیاله چشم بگشاید صراحی واکند گوشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گران شد حسن شمع از بذله سنجان مطربی خواهم</p></div>
<div class="m2"><p>که از تحریر گلبانگ غزل گویا کند گوشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر می در خروش آید دم مطرب به جوش آید</p></div>
<div class="m2"><p>سر معنی طلب با صد زیان سودا کند گوشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طنین بال مور از لهجه داود یاد آرد</p></div>
<div class="m2"><p>درین پرده مگس بر نغمه عنقا کند گوشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا صد شرح غم بر روی هم افتاد و او خود را</p></div>
<div class="m2"><p>به تمکین نکته ای پرسد، به استغنا کند گوشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی ذوق از مقامات هزار ما تواند کرد</p></div>
<div class="m2"><p>که از هر عضو همچون شاخ گل پیدا کند گوشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ضمیر پرگهر دارم قرین ابر نیسانی</p></div>
<div class="m2"><p>سخن را مستمع خواهم که چون دریا کند گوشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلیخایی بباید تا حدیث عشق و سودا را</p></div>
<div class="m2"><p>ز سر معجر براندازد به رغبت واکند گوشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بیت الحزن یعقوب بندد گر بشیر آید</p></div>
<div class="m2"><p>پریشان گرددش خاطر که بر هرجا کند گوشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین مجلس که کس نام و سلام کس نمی گیرد</p></div>
<div class="m2"><p>نیازی عرضه خواهم کرد اگر پروا کند گوشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>«نظیری » کیست؟ مسکینی به اهل فقر هم صحبت</p></div>
<div class="m2"><p>نه پیش جم برد عرضی، نه بر دارا کند گوشی</p></div></div>