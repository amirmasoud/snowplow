---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>پرده برداشته ام از غم پنهانی چند</p></div>
<div class="m2"><p>به زیان می رود امروز گریبانی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان ضعیفان که وفا داشت درین شهر اسیر</p></div>
<div class="m2"><p>قفسی چند بجا مانده و زندانی چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر و سامان سخن کردن این جمعم نیست</p></div>
<div class="m2"><p>پهلوی من بنشانید پریشانی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس خرابیم ز یکدیگرمان نشناسند</p></div>
<div class="m2"><p>مانده ایم از ده غارت زده ویرانی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشته از بس به هم افتاده کفن نتوان کرد</p></div>
<div class="m2"><p>فکر خورشید قیامت کن و عریانی چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ دل را ستم حادثه مجروح نکرد</p></div>
<div class="m2"><p>که نه لعل تو بر آن ریخت نمکدانی چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ کس را سرپایی نزد ایام که ما</p></div>
<div class="m2"><p>پشت دستی نگزیدیم به دندانی چند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر عشرت طلبی لخت دل آریم برون</p></div>
<div class="m2"><p>چیده ایم از گل این بادیه دامانی چند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم بر فیض «نظیری » همه خوبان دارند</p></div>
<div class="m2"><p>کاسه در پیش گدا داشته سلطانی چند</p></div></div>