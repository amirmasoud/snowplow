---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>گوش گل می‌درد از مژده هنگام صبوح</p></div>
<div class="m2"><p>زنده دارد نفس باد صبا نام صبوح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو مرغ فلکی رام گلستان شده‌ای</p></div>
<div class="m2"><p>خواب مرغ سحری رفته و آرام صبوح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو گل از مرغ سحرگاه گرفتارتری</p></div>
<div class="m2"><p>دم نزد صبح حریفان که نشد دام صبوح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چنان بزم که مستان سحر می نوشند</p></div>
<div class="m2"><p>صاف خورشید بود درد ته جام صبوح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست و پا گر نزند دل نفسم می گیرد</p></div>
<div class="m2"><p>در گو روز فتادم ز لب بام صبوح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم مطلوب سر از دامن دلبر نگرفت</p></div>
<div class="m2"><p>لابه نیم شبی کردم و ابرام صبوح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حق دیدار «نظیری » نرسانی به تمام</p></div>
<div class="m2"><p>در شب وصل ادا گر نکنی وام صبوح</p></div></div>