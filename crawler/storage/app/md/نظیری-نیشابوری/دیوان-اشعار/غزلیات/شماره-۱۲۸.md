---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>داند اخلاص مرا وز حال من آگاه نیست</p></div>
<div class="m2"><p>در دلش ره دارم و بر آستانم راه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت با ما سرکش است و مدعا با ما به جنگ</p></div>
<div class="m2"><p>کهربای شوق ما را جذبه یک کاه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فصل ها شد در تباهی، برنیامد عمر ما</p></div>
<div class="m2"><p>کشتی ما را سفر از سیر سال و ماه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شست دل صد ره گشودم بر هدف کاری نکرد</p></div>
<div class="m2"><p>گوییا پیکان و پر با این خدنگ آه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاطر دوران ز کین دوستان در عهد تو</p></div>
<div class="m2"><p>آن چنان پر شد که دل ها را به دل ها راه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرض حال جمله ره دارد به خلوتگاه رب</p></div>
<div class="m2"><p>جز دعای من که او مقبول این درگاه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش از ین در جان سپاری ها از آن لب قسمتم</p></div>
<div class="m2"><p>حرف تلخی بود اکنون گاه هست و گاه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جستجوی وصل با این زندگی بی طاقتی است</p></div>
<div class="m2"><p>ذوقی از پرواز با این رشته کوتاه نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر «نظیری » شکوه از بی مهریت دارد مرنج</p></div>
<div class="m2"><p>عیب صاحب را که پوشد بنده، دولتخواه نیست</p></div></div>