---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>خونم ار بر خاک ریزی نقض پیمان کی شود</p></div>
<div class="m2"><p>خاکم ار زین در برون ریزی پریشان کی شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرمی اهل محبت از دم گرم منست</p></div>
<div class="m2"><p>ناله ام تا نشنود بلبل غزلخوان کی شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوربختی را چه سازم چاره نتوان ساختن</p></div>
<div class="m2"><p>چون نمک بر ریش آید در نمکدان کی شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بازوی ما دلخراشان را کمندی لازم است</p></div>
<div class="m2"><p>هرچه دست ما رسد در وی گریبان کی شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پشت پا زن بر هوس آنگه هوای عشق کن</p></div>
<div class="m2"><p>تا بت خو نشکند کافر، مسلمان کی شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ کس بر روی بستر کسب جمعیت نکرد</p></div>
<div class="m2"><p>بر سر کو تا نخوابد دل به سامان کی شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داروی غم گریه مستانه می شد پیش ازین</p></div>
<div class="m2"><p>آن هم از کیفیت افتادست درمان کی شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنده نتوان کرد ما آزادگان را جز به مهر</p></div>
<div class="m2"><p>جنس ما بسیار کم یابست ارزان کی شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تنگدستی چون تو کی یابد «نظیری » قرب دوست</p></div>
<div class="m2"><p>آن چنان نوباوه یی هرگز فراوان کی شود</p></div></div>