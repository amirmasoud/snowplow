---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>اشک در دیده نیارم که حجابم نبرد</p></div>
<div class="m2"><p>حایل گریه کنم شرم که آبم نبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طپش و تابش من گرم سئوالش سازد</p></div>
<div class="m2"><p>صد ادا هست که کس پی به جوابش نبرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشته ام پی سپر حادثه چون گنج یتیم</p></div>
<div class="m2"><p>جز خضر راه به دیوار خرابم نبرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوار از عجز و تنزل شده ام می خواهم</p></div>
<div class="m2"><p>که به صلحش نروم تا به عتابم نبرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که عطر گل و مل راه مشامش بگرفت</p></div>
<div class="m2"><p>بوی از سوختگی های کبابم نبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرخوش از گردش چشم و لب میگون کندم</p></div>
<div class="m2"><p>زود مستم، به سوی بزم شرابم نبرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قطعه سبز خطش دیده ام و چشمه نوش</p></div>
<div class="m2"><p>هوس از راه به هر نقش سرابم نبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکنم یاد لب باده فروشش به نماز</p></div>
<div class="m2"><p>که ز مسجد به خرابات خرابم نبرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نپرد مرغ که واله نکند امیدم</p></div>
<div class="m2"><p>نوزد باد که از پای شتابم نبرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرشب از نرگس فتان به کمین نظرم</p></div>
<div class="m2"><p>صد فسون ساز نشاندست که خوابم نبرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست از باده به جز باد «نظیری» در دست</p></div>
<div class="m2"><p>نگذرد آب رز از کام که آبم نبرد</p></div></div>