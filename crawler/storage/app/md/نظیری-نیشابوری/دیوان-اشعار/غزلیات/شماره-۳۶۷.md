---
title: >-
    شمارهٔ ۳۶۷
---
# شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>روی دل با دوست باید داشت در مرگ و نشاط</p></div>
<div class="m2"><p>راست رفتی در محبت، راست رفتی در صراط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوستی با دشمنان دوست دشمن دوستی است</p></div>
<div class="m2"><p>تا نباشد دل موافق درنگیرد اختلاط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اعتدال از سرو باغ آموز نی از خار و گل</p></div>
<div class="m2"><p>نه سراپا بستگی نی پای تا سر انبساط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چیست این گردون طلسمی بوالعجب تعویذ دهر</p></div>
<div class="m2"><p>سر نمی آرد کسی بیرونش از خط و نقاط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمان دیریست دلگیرست از بازی خویش</p></div>
<div class="m2"><p>لیک داو آخر نمی گردد که برچیند بساط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست در کل جهان جزوی که آن در کار نیست</p></div>
<div class="m2"><p>نقطه ای کم می شود می ریزد از هم ارتباط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظم عالم را حکیمی هست آخر، روشنست</p></div>
<div class="m2"><p>حکمتش از استواری ز استواری احتیاط</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود عجب دارم که در کنه جمال خود رسد</p></div>
<div class="m2"><p>کی توان یک ذات را گفتن محیطست و محاط</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیز فرض خود ادا فرما «نظیری » تا رویم</p></div>
<div class="m2"><p>خواب در مسجد حرامست و اقامت در رباط</p></div></div>