---
title: >-
    شمارهٔ ۴۳۶
---
# شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>غساله شوی ته کاسه و ایاغ شدم</p></div>
<div class="m2"><p>بتر ز پنبه رنگین روی داغ شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه خضر بود درین تیره ره نه چشمه خضر</p></div>
<div class="m2"><p>ز شرم هرزه دوی سرو در سراغ شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فغان و شیون مرغان چنان ملولم کرد</p></div>
<div class="m2"><p>که جیب و دامن خالی برون ز باغ شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگویم این که سیه بختیم نمی انداخت</p></div>
<div class="m2"><p>چو بال زاغ بدم، همچو چشم زاغ شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به روی سبزه و گل بود سیر و پروازم</p></div>
<div class="m2"><p>نصیب خواند که پروانه چراغ شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ته پیاله به من داد لیک از مستی</p></div>
<div class="m2"><p>فتیله بر دل خامان نهاد و داغ شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دشت و مزرعه گشتن، هواپرستی بود</p></div>
<div class="m2"><p>به کنج خلوت و عزلت ز باغ و راغ شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسیم نیم شبم بر مشام بویی زد</p></div>
<div class="m2"><p>سحر شکفته و خوش طبع و تر دماغ شدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مدار کار «نظیری » به خلق و دم درکش</p></div>
<div class="m2"><p>که فارغ از همه در گوشه فراغ شدم</p></div></div>