---
title: >-
    شمارهٔ ۴۳۳
---
# شمارهٔ ۴۳۳

<div class="b" id="bn1"><div class="m1"><p>به تمنای غلط بر همه کس میر شدیم</p></div>
<div class="m2"><p>بدر از خانه نرفتیم و جهانگیر شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر عمر به سودای نوی افتادیم</p></div>
<div class="m2"><p>هوس و حرص جوان گشت اگر پیر شدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مه کله گوشه پی خدمت ما می شکند</p></div>
<div class="m2"><p>که سرافراز به اندازه تقصیر شدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک تلخ و جگر شور ز ما پرس که جیست</p></div>
<div class="m2"><p>طفل بودیم که باز از شکر و شیر شدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غافل از شیوه رندی به سلوک افتادیم</p></div>
<div class="m2"><p>تازه ناکرده دماغ از پی نخجیر شدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوست بر مان نگران از سر شفقت بگذشت</p></div>
<div class="m2"><p>خاک بودیم ز فیض نظر اکسیر شدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کجا راه دهد اسب بر آن تاز که ما</p></div>
<div class="m2"><p>بارها مات درین عرصه به تدبیر شدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شادی هفته به آزادی ما می گردد</p></div>
<div class="m2"><p>همچو آدینه چه سر حلقه زنجیر شدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چار فصل چمن عمر ندیدیم افسوس</p></div>
<div class="m2"><p>نارسیده به جوانی ز تعب پیر شدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رشک بر پیری ما چرخ و عطارد دارد</p></div>
<div class="m2"><p>پشت خم همچو کمان راست تر از تیر شدیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوشتر از عمر زلیخا به طرب برگشتیم</p></div>
<div class="m2"><p>عذر تقصیر عمل در پی تو قیر شدیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان دو محراب نشین هندو زنارپرست</p></div>
<div class="m2"><p>پیش کفار به دریوزه تکبیر شدیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فکر آبادی ایمان «نظیری » کردیم</p></div>
<div class="m2"><p>سوی دل‌های خراب از پی تعمیر شدیم</p></div></div>