---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>آن که بر ما رقم کین زده از کینه ما</p></div>
<div class="m2"><p>نقش خود دیده در آیننه ز آیینه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عید و نوروز بود مکتب ما را هر روز</p></div>
<div class="m2"><p>به محبت گذرد شنبه و آدینه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محضر سلطنت عشق اگر بخوانند</p></div>
<div class="m2"><p>خاتم و سکه برآرند ز گنجینه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورده دل زخمی از آن غمزه که نتوانی دوخت</p></div>
<div class="m2"><p>تو که صد بار فزون دوخته‌ای سینه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان نگاهی که به دنبال چشمت نرسد</p></div>
<div class="m2"><p>خون فرو می چکد از خرقه پشمینه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آزمودیم، به زور می امسال نبود</p></div>
<div class="m2"><p>قدحی داشت خم از باده پارینه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طرفه شوری سحر از سینه «نظیری » برخاست</p></div>
<div class="m2"><p>ساخت کار همه را گریه دوشینه ما</p></div></div>