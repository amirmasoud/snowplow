---
title: >-
    شمارهٔ ۴۹۳
---
# شمارهٔ ۴۹۳

<div class="b" id="bn1"><div class="m1"><p>از صبح روزگار گشاد جبین مجو</p></div>
<div class="m2"><p>روی شکفته از دل اندوهگین مجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم ثبات مهر ندیدم بر آسمان</p></div>
<div class="m2"><p>جنسی که بر فلک نبود از زمین مجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قاصد پیام یار ز ما آورد به ما</p></div>
<div class="m2"><p>اینجا نشان مقدم روح الامین مجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنجا که زلف و چهره نمودند جادویی</p></div>
<div class="m2"><p>گر مریم است معجزش از آستین مجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تمثال خوبی دو جهانت نموده اند</p></div>
<div class="m2"><p>نقشی که در تو نیست ز روم و ز چین مجو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در زلف و رخ نظاره کن و خال لب نگر</p></div>
<div class="m2"><p>راه گمان مپوی و مقام یقین مجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشاق او ز نور و ز ظلمت گذشته اند</p></div>
<div class="m2"><p>در کشوری که عشق بود کفر و دین مجو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تلخ از لبش چو نحل عسل جوش می زند</p></div>
<div class="m2"><p>گر نیش بایدت نخوری انگبین مجو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با نیک و بد بساز «نظیری » ز روزگار</p></div>
<div class="m2"><p>گر باغبان گیا دهدت یاسمین مجو</p></div></div>