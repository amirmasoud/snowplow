---
title: >-
    شمارهٔ ۵۵۷
---
# شمارهٔ ۵۵۷

<div class="b" id="bn1"><div class="m1"><p>سحر که سنبلم از جیب پیرهن بکشی</p></div>
<div class="m2"><p>گمان برم که رگ جانم از بدن بکشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شود کنار و برم بی تو باغ عریانی</p></div>
<div class="m2"><p>که غارتش کنی و سنبل و سمن بکشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبم به هم زده یارب تو انتقام مرا</p></div>
<div class="m2"><p>ز صبح ظالم قطاع تیغ زن بکشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکسته شد صنم عیشم از خلیل صباح</p></div>
<div class="m2"><p>تو داد من صمد از این صنم شکن بکشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نخل عمر نیابد خزان پیری راه</p></div>
<div class="m2"><p>به نوبهار اگر باده کهن بکشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سایه گل تو آن گیاه بی کارم</p></div>
<div class="m2"><p>که سر اگر بکشم، بیخم از چمن بکشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خطاب غمزه خمار تو به من زآنست</p></div>
<div class="m2"><p>که مست حرف خودم سازی و سخن بکشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز چشم هندوی تو این مزاج می آید</p></div>
<div class="m2"><p>که صندلم به جبین پیش برهمن بکشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عنان طبع تو در دست ناز و بدخویی است</p></div>
<div class="m2"><p>عجب نباشد اگر سر ز خویشتن بکشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اسیر کرده تو از خوشی نیارد یاد</p></div>
<div class="m2"><p>چو طفل خاطرش از خویش و از وطن بکشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مقید لب شیرین مکن «نظیری » دل</p></div>
<div class="m2"><p>که خسرو ار شوی اندوه کوهکن بکشی</p></div></div>