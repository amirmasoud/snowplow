---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>بکش، بسوز، که نام امان نخواهم برد</p></div>
<div class="m2"><p>دعا به درد سر آسمان نخواهم برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن ملاحظه از کشتنم که روز جزا</p></div>
<div class="m2"><p>ز رشک نام تو را بر زبان نخواهم برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دل طپیدن آغاز عشق دانستم</p></div>
<div class="m2"><p>کزین معامله غیر از زیان نخواهم برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز اضطراب دلم روز وصل معلومست</p></div>
<div class="m2"><p>که از بلای شب هجر جان نخواهم برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس است چند کنی ای فراق بی رحمی</p></div>
<div class="m2"><p>دگر به خویش تحمل گمان نخواهم برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر ز دامن یوسف کنند بالینم</p></div>
<div class="m2"><p>سری که وقف تو شد ز آستان نخواهم برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به این ملال که من می روم بسوی چمن</p></div>
<div class="m2"><p>چه جای غنچه که برگ خزان نخواهم برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>«نظیری » این چه بلندی و تیز پروازیست</p></div>
<div class="m2"><p>ز شوق ره به سوی آشیان نخواهم برد</p></div></div>