---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>به هجر و وصل دلم الفت و نزاع ندارد</p></div>
<div class="m2"><p>نشاط آمدن و کلفت وداع ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شهر ما نفروشند جز رضا و محبت</p></div>
<div class="m2"><p>کسی دکان نگشاید که این متاع ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آن فراز که من می کنم عروج مقامی است</p></div>
<div class="m2"><p>که هیچ پایه بر آن پایه ارتفاع ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان حقارتم از چشم اعتبار فگنده ست</p></div>
<div class="m2"><p>که دهر بر من و حال من اطلاع ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رطل خون جگر می خورم ز بخت به شکرم</p></div>
<div class="m2"><p>که سر ز جام تنک مشربم صداع ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تیرگی شب انتظار شمع امیدم</p></div>
<div class="m2"><p>برابر پر پروانه‌ای شعاع ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عبث به وعده لطفش دلت خوشست «نظیری »</p></div>
<div class="m2"><p>کدام لطف؟ که با بخت تو نزاع ندارد</p></div></div>