---
title: >-
    شمارهٔ ۳۲۷
---
# شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>از خوی کریم تو گنه گشت فراموش</p></div>
<div class="m2"><p>شرمنده نماندیم زهی عفو خطاپوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل راه تو پویید و نهد بر سر و جان پای</p></div>
<div class="m2"><p>جان دست تو بوسید و زند بر دل و دین دوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز بر تو نخوانم که ندرد ورقم بخت</p></div>
<div class="m2"><p>جز از تو نپرسم که نتابد فلکم گوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویا سخن عشق تو شد قوت خردها</p></div>
<div class="m2"><p>کاندم که کنم وصف تو در دام فتد هوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من خود شوم از هر سخن خویش پشیمان</p></div>
<div class="m2"><p>وین قوم به من هیچ نگویند که خاموش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پختیم رنگ و ریشه و لذت نگرفتیم</p></div>
<div class="m2"><p>زین خام حریفان که ندارند به هم جوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردد دو جهان هیچ چو با هم بنشینند</p></div>
<div class="m2"><p>سلطان قلندروش و ابدال نمدپوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از رفتن دوران هنردوست یتیمم</p></div>
<div class="m2"><p>نتوان پدر از سر شده را گفت که مخروش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچند به عشرت گذرد فرصت پیری</p></div>
<div class="m2"><p>ایام جوانی نتوان کرد فراموش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>افسرده تر از صبح خمار شب دوشم</p></div>
<div class="m2"><p>امروز که بر دوش برندم ز می دوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنشین به خود ار خوش شودت وقت «نظیری »</p></div>
<div class="m2"><p>یوسف که خری مفت به قلب دو سه مفروش</p></div></div>