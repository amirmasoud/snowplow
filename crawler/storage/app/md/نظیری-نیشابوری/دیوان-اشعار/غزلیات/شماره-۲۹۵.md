---
title: >-
    شمارهٔ ۲۹۵
---
# شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>خمش ز لابه که طبعش مشوشست هنوز</p></div>
<div class="m2"><p>شکر بخور مکن شعله سرکشست هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تحملی که مزاجش به اعتدال آید</p></div>
<div class="m2"><p>میان عفو و غضب در کشاکشست هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آشنایی طفل من اعتمادی نیست</p></div>
<div class="m2"><p>فرشته خوست ولی آسمان و شست هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی به میکده اش برقع از جمال افتاد</p></div>
<div class="m2"><p>قرابه آب فشان جام در غشست هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر جراحت حرمان عشق بسیارست</p></div>
<div class="m2"><p>که این شسکته خدنگی ز ترکشست هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یک دو زخم که خوردی ز حسن امن مباش</p></div>
<div class="m2"><p>که در کمین گه ابرو کمان کشست هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نجات نیست «نظیری » ز دهر بوقلمون</p></div>
<div class="m2"><p>اگرچه ریخت گل، ایوان منقشست هنوز</p></div></div>