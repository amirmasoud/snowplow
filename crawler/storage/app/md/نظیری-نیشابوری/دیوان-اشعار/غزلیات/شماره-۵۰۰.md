---
title: >-
    شمارهٔ ۵۰۰
---
# شمارهٔ ۵۰۰

<div class="b" id="bn1"><div class="m1"><p>تا شوی هم انس آگاهی اطلاق خواب ده</p></div>
<div class="m2"><p>ترک بالین حریر و بستر سنجاب ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش هر پندار پیش آید، به می از دل بشوی</p></div>
<div class="m2"><p>سر به صورت خانه نام و نسب سیلاب ده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی ار نوشی بگوید، زهد و تقوا کن نثار</p></div>
<div class="m2"><p>مطرب ار دستی برآرد، جبه و جلباب ده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پادشه رند است و عارف، جامه رندی بپوش</p></div>
<div class="m2"><p>جبه سالوس و تسبیح ریا پرتاب ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردمندان را علاجی زان نظر دریوزه کن</p></div>
<div class="m2"><p>مستحقان را زکاتی زان دو لعل ناب ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون چنین کردی روش بر کوه صحرا کن گذر</p></div>
<div class="m2"><p>خار و خارا را خواص قاقم و سنجاب ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توسن طبع و هوا سر داده میرانی کجا</p></div>
<div class="m2"><p>دور از مقصد شدی آخر عنانی تاب ده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سیه چشمان هندی آب در چشمت نماند</p></div>
<div class="m2"><p>آب ریزان می شود در یزد چشمی آب ده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ره خطرناک است و منزل دور و رهزن در کمین</p></div>
<div class="m2"><p>روز بی گه شد «نظیری »، ترک این اسباب ده</p></div></div>