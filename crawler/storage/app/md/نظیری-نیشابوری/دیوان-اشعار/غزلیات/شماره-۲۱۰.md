---
title: >-
    شمارهٔ ۲۱۰
---
# شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>نمی توان به گزند از من انتقام کشید</p></div>
<div class="m2"><p>که دایه زهر به طفلی مرا به کام کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمانه یک نفسم بر مراد خود نگذاشت</p></div>
<div class="m2"><p>به هر که داد مراد از من انتقام کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار نقش خوشم داد چرخ و تا دیدم</p></div>
<div class="m2"><p>قلم گرفت و خط سهو بر تمام کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا فریب نبرد از ره ارنه این جادو</p></div>
<div class="m2"><p>عنان خاص گرفت و کمند عام کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آه و ناله حریفم ز جام و نغمه مگو</p></div>
<div class="m2"><p>که کارم از می و مطرب به این مقام کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شراب دور خزان بی تفاوتی نگرفت</p></div>
<div class="m2"><p>که گر حلال رسید و اگر حرام کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه جای من، که به جام شراب و طره حور</p></div>
<div class="m2"><p>فرشته را ز فلک می توان به دام کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان نزار فتادم به عشق نیم نظر</p></div>
<div class="m2"><p>که سایه از سر کویم به زیر بام کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بساط عافیت ای عقل و هوش برچینید</p></div>
<div class="m2"><p>دگر «نظیری » بی ظرف یک دو جام کشید</p></div></div>