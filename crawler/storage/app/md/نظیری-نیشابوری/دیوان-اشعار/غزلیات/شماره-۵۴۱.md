---
title: >-
    شمارهٔ ۵۴۱
---
# شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>سوی هرکس به عنایت نظر انداخته‌ای</p></div>
<div class="m2"><p>تا قیامت ز خودش بی‌خبر انداخته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طمعم نیست کزین کو به سلامت بروم</p></div>
<div class="m2"><p>که به هرسو که نهم پای، سر انداخته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل در حلقه نگنجد ز بس اندر خم زلف</p></div>
<div class="m2"><p>دل سودازده بر یکدگر انداخته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فهم در دایره تنگ دهان تو گم است</p></div>
<div class="m2"><p>گرچه از حلقه خالش به در انداخته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز شیرین سخنان تو ازان شوریدست</p></div>
<div class="m2"><p>که به گفتن نمکی در شکر انداخته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ما کیست که سرگشته رویت باشد</p></div>
<div class="m2"><p>خانمان‌ها به شکرخنده برانداخته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه در کلبه درویش اقامت نکند</p></div>
<div class="m2"><p>دولت ماست که بر ما گذر انداخته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده صد دجله به من داده سرو می‌سوزم</p></div>
<div class="m2"><p>آتشم بین که چه در خشک و تر انداخته‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بین چه‌ها گشته‌ام ای شمع جهان گرد سرت</p></div>
<div class="m2"><p>که چو پروانه‌ام آتش به پر انداخته‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم این راه رسیدست به پایان دیدم</p></div>
<div class="m2"><p>که از اول قدمم دورتر انداخته‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من که تقدیر، جنببت کش تدبیرم بود</p></div>
<div class="m2"><p>بسته‌ای دستم و رختم ز خر انداخته‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باید از اول شب کرد «نظیری» شب‌گیر</p></div>
<div class="m2"><p>بار در مرحله پرخطر انداخته‌ای</p></div></div>