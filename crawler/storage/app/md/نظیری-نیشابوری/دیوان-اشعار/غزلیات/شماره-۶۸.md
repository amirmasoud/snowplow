---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>عشق تو بند علایق ز ره ما برداشت</p></div>
<div class="m2"><p>هرکه مجنون تو شد سلسله از پا برداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنس ارزنده و ارباب بصارت مشتاق</p></div>
<div class="m2"><p>نتوان دست ز بیعانه سودا برداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون توان گشت کنون ساکن خلوتگه باغ</p></div>
<div class="m2"><p>مجلس آراست گل و مرغ تقاضا برداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست در گردن معشوق حمایل دارم</p></div>
<div class="m2"><p>نتوان کف پی هر عرض تمنا برداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عارفان گوشه چشمی به دو عالم ندهند</p></div>
<div class="m2"><p>هر کجا باد نقاب از رخ زیبا برداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محضر بندگی از مرتبه من بیش است</p></div>
<div class="m2"><p>این نشان را دل مفلس ز کجا تا برداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده می بایدم آویخت که هرکس نگریست</p></div>
<div class="m2"><p>شرح سودای تو را نسخه ز سیما برداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که نازک دلم از عشق حدیثی تا رفت</p></div>
<div class="m2"><p>اشکم از پرده برون آمد و غوغا برداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طفل در گریه «نظیری » چو تو کافرخو نیست</p></div>
<div class="m2"><p>پدرت تا ز کدامین در ترسا برداشت</p></div></div>