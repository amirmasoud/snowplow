---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>حکم جفا صحیح و امید وفا غلط</p></div>
<div class="m2"><p>تعبیر تو درست ولی خواب ما غلط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ته کاسه سگ تو به ما کس نمی‌دهد</p></div>
<div class="m2"><p>لاف گدا ز مکرمت پادشا غلط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک فال خوب راست نشد بر زبان ما</p></div>
<div class="m2"><p>شومی جغد ثابت و یمن هما غلط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در التماس ما سخن دوستان دروغ</p></div>
<div class="m2"><p>در احتیاج ما مدد آشنا غلط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر از آن جمال فروغی دلیل ساز</p></div>
<div class="m2"><p>دل کرده ره در آن سر زلف دوتا غلط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچند ما به غل و غش آییم در نظر</p></div>
<div class="m2"><p>اما به خاصیت نکند کیمیا غلط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنجا که حل و عقد به رد و قبول تست</p></div>
<div class="m2"><p>حکم ستاره باطل و علم قضا غلط</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا سهو کار ما ز تو اصلاح می‌شود</p></div>
<div class="m2"><p>خواهیم دیگری نکند غیر ما غلط</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همت ز می فروش «نظیری » طلب که هست</p></div>
<div class="m2"><p>اخبار خضر و چشمه آب بقا غلط</p></div></div>