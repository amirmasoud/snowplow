---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>صفا از عقده دل‌هاست آن زلف معقد را</p></div>
<div class="m2"><p>بحمدلله که ربطی هست با مطلق مقید را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دادی؟ روح را با جسم الفت گر نگردیدی</p></div>
<div class="m2"><p>محمد کاروان سالار ارواح مجرد را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آن حسن و شمایل طرح عشق افکنده شد ورنه</p></div>
<div class="m2"><p>نمی‌دادند نقش هستی این لوح زبرجد را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مکتب‌خانه کن مصحف از بر داشت آن روزی</p></div>
<div class="m2"><p>که عقل کل نمی‌کرد از الف با فرق ابجد را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حدیث دل‌فروزش بس که شد مجموعه حکمت</p></div>
<div class="m2"><p>حکیمان جزو می‌سازند اوراق مجلد را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وجود مرکز پرگار عالم کی شدی ثابت</p></div>
<div class="m2"><p>احد خود قاب قوسین ار نبودی میم احمد را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مسکن بستر از پهلوی گرمش سرد ناگشته</p></div>
<div class="m2"><p>کند طی بر براق معرفت اقصای مقصد را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرامی میهمانی در ره امشب میزبان دارد</p></div>
<div class="m2"><p>ملایک بر فلک صف بست و عرض آراست مسند را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«نظیری» نشئه ذوقی ز جام هوشمندان کش</p></div>
<div class="m2"><p>می و مطرب پریشان می‌کند مستان سرمد را</p></div></div>