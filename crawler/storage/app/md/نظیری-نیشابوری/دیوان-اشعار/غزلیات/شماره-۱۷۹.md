---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>هنوز راه نگاهم به بام و در ندهند</p></div>
<div class="m2"><p>کبوتری که بیاموختند سر ندهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراب نرگس سنگین دلان سرمستم</p></div>
<div class="m2"><p>که بر طریق نظر مهر را گذر ندهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز غم به گونه زرین شدم چه چاره کنم</p></div>
<div class="m2"><p>قبول صحبت صاحبدلان به زر ندهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازین گشاده جبینان ثبات عیش مجو</p></div>
<div class="m2"><p>که گل دهند به خروار و یک ثمر ندهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زهر یأس بساز و مجو حلاوت کام</p></div>
<div class="m2"><p>دوا چو داروی تلخت کند شکر ندهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خوان نعمت دوران رضا به قسمت ده</p></div>
<div class="m2"><p>که طعمه یی ز غمت خوش گوارتر ندهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز درد سوز که بر بستر آب عنابت</p></div>
<div class="m2"><p>بغیر تب زدگی و تف جگر ندهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه یاد جور رفیقان کنم، نصیبم بود</p></div>
<div class="m2"><p>که تشنه بر لب جو میرم و خبر ندهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مثال ما لب دریا و حال مستسقی ست</p></div>
<div class="m2"><p>دهند شوق ولی رخصت نظر ندهند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سزد که مقنعه بر سر کنند آن مردان</p></div>
<div class="m2"><p>که تاج عشق بخواهند و ترک سر ندهند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ظفر تو راست «نظیری » که محو ذوق شدی</p></div>
<div class="m2"><p>به هرکه غوطه به دریا نزد، گهر ندهند</p></div></div>