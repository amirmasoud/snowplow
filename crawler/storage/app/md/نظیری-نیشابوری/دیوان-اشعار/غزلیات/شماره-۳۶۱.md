---
title: >-
    شمارهٔ ۳۶۱
---
# شمارهٔ ۳۶۱

<div class="b" id="bn1"><div class="m1"><p>همیشه خنده شادی به آن لبان مخصوص</p></div>
<div class="m2"><p>فریب حسن به اقبال جاودان مخصوص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تو قبله امیدهای روحانی</p></div>
<div class="m2"><p>سر نیاز به آن خاک آستان مخصوص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکایت تو چو فکرم ز مغز بیگانه</p></div>
<div class="m2"><p>محبت تو چو مغزم به استخوان مخصوص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمی فتاده که با طایران وحشی دل</p></div>
<div class="m2"><p>نمی شویم به هم در یک آشیان مخصوص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدیم هر دری از شاهدان هرجایی</p></div>
<div class="m2"><p>نه می به میکده نه گل به گلستان مخصوص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز طول روز قیامت عجب هراسانم</p></div>
<div class="m2"><p>که روز هجر تو باشد به این نشان مخصوص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حاجتم نرسد گرچه شد به خدمت تو</p></div>
<div class="m2"><p>به آشناییی آه من آسمان مخصوص</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز تو رگم به رگ و مو به موی در سخن است</p></div>
<div class="m2"><p>حکایت تو همین نیست با زبان مخصوص</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نامه تو معطر بغل «نظیری » را</p></div>
<div class="m2"><p>چو گل فروش که باشد به باغبان مخصوص</p></div></div>