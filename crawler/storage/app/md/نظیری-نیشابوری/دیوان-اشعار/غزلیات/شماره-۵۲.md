---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>به بریدن نرود ذوق تو ز اندیشه ما</p></div>
<div class="m2"><p>سال ها پنجه به هم داده رگ و ریشه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اصل ما آب ز سرچشمه تحقیق خورد</p></div>
<div class="m2"><p>گل تسلیم و رضا آورد اندیشه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می منصور که در جوش ز خامی ها بود</p></div>
<div class="m2"><p>بعد دوری به قوام آمده در شیشه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خس و خار نبینیم به جز جلوه دوست</p></div>
<div class="m2"><p>شجر وادی ایمن بود از بیشه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق آورده خلیل الله از آزر چه عجب</p></div>
<div class="m2"><p>یا صمدگوی شود گر صنم از تیشه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوه کن از هنر عشق ندارد نامی</p></div>
<div class="m2"><p>نام ما راست که عشق است همین پیشه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل برگ چمن عشق «نظیری » ماییم</p></div>
<div class="m2"><p>نرود تا ابد از خاک رگ و ریشه ما</p></div></div>