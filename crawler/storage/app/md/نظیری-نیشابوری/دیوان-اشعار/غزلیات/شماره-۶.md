---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>برای خشت خم خوبیم گو آن پیر ترسا را</p></div>
<div class="m2"><p>کزین بازیچه طفلان خرد مشت گل ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان را نیست آن معنی که باید فکر آن کردن</p></div>
<div class="m2"><p>الف با خوان هر مکتب شکافد این معما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خود از بهر حسرت داد راهم ورنه معلومست</p></div>
<div class="m2"><p>ز دریا چند در آغوش گنجد موج دریا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همین بس شاهد بی‌اختیاری‌های مشتاقان</p></div>
<div class="m2"><p>که عذر از جانب یوسف بود جرم زلیخا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خموشی نزد عشق آرم که بر درگه سلطانان</p></div>
<div class="m2"><p>کمان بر زه نمی‌آرند بازوی توانا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همین مقدار می‌خواهیم از رخ پرده برداری</p></div>
<div class="m2"><p>که بشناسیم قدر بینش نادان و دانا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«نظیری» خاطری از داغ دل آزرده‌تر دارد</p></div>
<div class="m2"><p>قدم هشیار نِهْ اینجا که در خون می‌نهی پا را</p></div></div>