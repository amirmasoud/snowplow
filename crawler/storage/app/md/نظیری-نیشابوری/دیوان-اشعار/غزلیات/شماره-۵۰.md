---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>ازین ویرانه تر می خواستم ویرانه خود را</p></div>
<div class="m2"><p>ازین ویرانه بیرون می برم دیوانه خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حریفان نشئه مهر و محبت را نمی دانند</p></div>
<div class="m2"><p>به دست دشمن خود می دهم پیمانه خود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه مورش خاید از سختی نه مرغش چیند از تلخی</p></div>
<div class="m2"><p>نمی بینم ز جنس هیچ خرمن دانه خود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شوق آن که طبل رحلتی ناگاه بنوازند</p></div>
<div class="m2"><p>همیشه رخت بر درگاه دارم خانه خود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عزیزان دیده از خاکسترم سازند نورانی</p></div>
<div class="m2"><p>تو شمع بزم خلوت می کنی پروانه خود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آیات زبور و نغمه داوود نفروشم</p></div>
<div class="m2"><p>بیان دردناک و نعره مستانه خود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«نظیری » قصه فرهاد و خسرو داستانی شد</p></div>
<div class="m2"><p>کنون من هم کتابی می‌کنم افسانه خود را</p></div></div>