---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>دوران می حسرت همه در ساغر ما کرد</p></div>
<div class="m2"><p>بر هرچه نهادیم دل از دیده جدا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگشود قضا شست که آهی نکشیدیم</p></div>
<div class="m2"><p>بر دوست ترم خورد خدنگی که خطا کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بازوی هنردارم و اقبال ندارم</p></div>
<div class="m2"><p>می کوشم و کاری نتوانم به سزا کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریاد برآریم از آن یار مشعبد</p></div>
<div class="m2"><p>کو از ازل این شعبده چرخ روا کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود طلعت خود دید اگر پرده برانداخت</p></div>
<div class="m2"><p>خود فتنه خود گشت اگر فتنه به پا کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با آن که لبش داد منادی محبت</p></div>
<div class="m2"><p>نی بر سر مهر آمد و نی عهد وفا کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناوک فکنی بر سر هر راه نشانید</p></div>
<div class="m2"><p>در عشق کمندم به گلو بست و رها کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دشمن به ارم افکند و دوست بر آتش</p></div>
<div class="m2"><p>با این همه حد نیست که گوییم جفا کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چندین سخن عشق که گفتند و شنیدند</p></div>
<div class="m2"><p>کس حق محبت نتوانست ادا کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برند به جای پر و بالش سر منقار</p></div>
<div class="m2"><p>مرغی که بلند از سر این شاخ نوا کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خرسند به تسلیم و رضا گشت «نظیری »</p></div>
<div class="m2"><p>مسکین نتوانست خصومت به قضا کرد</p></div></div>