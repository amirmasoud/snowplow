---
title: >-
    شمارهٔ ۵۲۷
---
# شمارهٔ ۵۲۷

<div class="b" id="bn1"><div class="m1"><p>نیست با مشاطه گلبن طرازم حاجتی</p></div>
<div class="m2"><p>عشق اگر خواهد بروید بر سفالی جنتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچه ام گل در گلو دارد بهارم تازه روست</p></div>
<div class="m2"><p>خنده ای کافیست با غم راز صبح رحمتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشتری گوره کن و دلال گو در پا فکن</p></div>
<div class="m2"><p>جنس اگر خوب است خواهد کرد پیدا قیمتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار ما را این چنین ناپخته کی خواهد گذاشت</p></div>
<div class="m2"><p>عشق اگر مرد است و با او هست بوی غیرتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر کوی ترقی خودنمایی ها کنم</p></div>
<div class="m2"><p>گر قدم برتر نهد از پایه خود همتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نغمه سنجیده می گویند این را ناله نیست</p></div>
<div class="m2"><p>نی نشان درد دارد نی خراش رقتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرکنم از تحفه مصرش «نظیری » آستین</p></div>
<div class="m2"><p>گر بیازد بر نقام باد دست رغبتی</p></div></div>