---
title: >-
    شمارهٔ ۴۰۴
---
# شمارهٔ ۴۰۴

<div class="b" id="bn1"><div class="m1"><p>زان شب که یار کرد نگاهی به سوی دل</p></div>
<div class="m2"><p>دیگر به سوی خویش ندیدیم روی دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاحبدلی بود که نصیبی به ما دهد</p></div>
<div class="m2"><p>گویی به خاک ما نرسیدست بوی دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن را که رخ ز آیینه دوست تافتند</p></div>
<div class="m2"><p>پهلوی دل نشسته نبیند عدوی دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر من نکرد مرحمتی پیر می فروش</p></div>
<div class="m2"><p>تا بر سر خمش نشکستم سبوی دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر حق گرفت خون دل و دیده دامنم</p></div>
<div class="m2"><p>از عیش های دیده بریدم گلوی دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دستم به چاک سینه از آن باز کرده اند</p></div>
<div class="m2"><p>تا من به آب دیده کنم شست و شوی دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اغوای دیو و ذلت آدم به آب رفت</p></div>
<div class="m2"><p>سر داده اند سیل محبت به جوی دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچند گویم از غم دل بیشتر شود</p></div>
<div class="m2"><p>خالی نمی شود دلم از گفتگوی دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم شوم ملازم دل بینمت مگر</p></div>
<div class="m2"><p>هرچند برشدم نرسیدم به کوی دل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان دم که دل به دست رضایت سپرده ام</p></div>
<div class="m2"><p>از وی نکرده ام پس از آن جستجوی دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یاری که دستگیری یاری کند کجاست؟</p></div>
<div class="m2"><p>از هر غمم غم تو کند جستجوی دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنشین که راحتست «نظیری » وجود عشق</p></div>
<div class="m2"><p>یک آرزو کنند هزار آرزوی دل</p></div></div>