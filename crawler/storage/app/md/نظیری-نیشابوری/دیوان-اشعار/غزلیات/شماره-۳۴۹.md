---
title: >-
    شمارهٔ ۳۴۹
---
# شمارهٔ ۳۴۹

<div class="b" id="bn1"><div class="m1"><p>بی تو نه با غم خوش و نی خانه خوش</p></div>
<div class="m2"><p>با نگار خانگی ویرانه خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ آزادم نخواهد آمدن</p></div>
<div class="m2"><p>خویش را دارم به دام و دانه خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من خود از فرزند دل برکنده ام</p></div>
<div class="m2"><p>کودکان دارند با دیوانه خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده را از گریه نیسان می کنم</p></div>
<div class="m2"><p>شاهدان را هست با دردانه خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرد کوچک دل نداند چون کند</p></div>
<div class="m2"><p>خواب شیرین آید و افسانه خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبر باید تا جگرخایی کنم</p></div>
<div class="m2"><p>درکشیدم زهر این پیمانه خوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دعوی چابک سواری می کنم</p></div>
<div class="m2"><p>گرچه رو برتافتم مردانه خوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می دهم شکرانه بگریختن</p></div>
<div class="m2"><p>هم مصافم هست و هم شکرانه خوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سهل نبود بر صف آتش زدن</p></div>
<div class="m2"><p>می نماید گرچه از پروانه خوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرد باطن بین چرا کاری کند</p></div>
<div class="m2"><p>کاشنا ناخوش شود بیگانه خوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در خرابات «نظیری » عیب نیست</p></div>
<div class="m2"><p>هست دیوانه خوش و فرزانه خوش</p></div></div>