---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>چه شور بود؟ که عشقت به من کرامت کرد</p></div>
<div class="m2"><p>که نارسیده قیامت دلم قیامت کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیث من که ز مجموعه وفای تو خواند؟</p></div>
<div class="m2"><p>که نه به خون دل و دیده اش علامت کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کعبه دل من عاشقان نماز آرند</p></div>
<div class="m2"><p>که قبله شد صنم و برهمن امامت کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر که نماز کنم صدهزار سجده شکر</p></div>
<div class="m2"><p>که در دیار تو دل نیت اقامت کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قضای کفر ادا می کنم که بر من عشق</p></div>
<div class="m2"><p>نماز و طاعت صدساله را غرامت کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نثار دیده تصدق دهم که بخت جوان</p></div>
<div class="m2"><p>به کوی زهد و ریا نوبر ندامت کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مزاج عشق «نظیری » حریص و سودایی است</p></div>
<div class="m2"><p>درین معامله نتوان تو را ملامت کرد</p></div></div>