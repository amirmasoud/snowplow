---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>هرکه نوشید می شوق تو نسیانش نیست</p></div>
<div class="m2"><p>وان که محو تو شد اندیشه حرمانش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به حسن تو مقید شد و جاوید بماند</p></div>
<div class="m2"><p>که ز فکر تو برون آمدنش آسانش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به کی فکر توان کرد و سخن تازه نوشت</p></div>
<div class="m2"><p>قصه شوق حدیثی است که پایانش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ کس نامه سربسته ما فهم نکرد</p></div>
<div class="m2"><p>نه همین خاتمه اش نیست که عنوانش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبب از عقل مپرسید که غمنامه ما</p></div>
<div class="m2"><p>درس عشق است که از علم دبستانش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دلم ره به دلت عشق نمودست و خوشم</p></div>
<div class="m2"><p>که به آن خانه دری هست که دربانش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راه دیگر به سوی کعبه اعرابی هست</p></div>
<div class="m2"><p>که غم از سرزنش خار مغیلانش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاطر غیب نمای تو مگر جام جم است</p></div>
<div class="m2"><p>که رخ حال من از آینه پنهانش نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایه نامه تو بال هما می داند</p></div>
<div class="m2"><p>هدهد ما که سر تاج سلیمانش نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرد تاجر که ز غربت به وطن می آید</p></div>
<div class="m2"><p>تحفه یی خوب تر از نامه اخوانش نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون قلم گریه شادی کنم از نامه دوست</p></div>
<div class="m2"><p>که بجز از دل خندان مژه گریانش نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس که از دقت فهم تو «نظیری » بگداخت</p></div>
<div class="m2"><p>نکته‌ای نیست که آمیخته با جانش نیست</p></div></div>