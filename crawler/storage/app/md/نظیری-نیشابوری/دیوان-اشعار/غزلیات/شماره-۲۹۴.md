---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>سخن گویید با من کمتر امروز</p></div>
<div class="m2"><p>که دارم دل به جای دیگر امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان سودا مزاجم را گرفته</p></div>
<div class="m2"><p>که تلخم می نماید شکر امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان اشکم به خشک و تر رسیده</p></div>
<div class="m2"><p>که چوبم می نسوزد آذر امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بس طوفان درو بامم گرفته</p></div>
<div class="m2"><p>فراز بام می یابم در امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سمند عشق را زین برگرفتم</p></div>
<div class="m2"><p>خرد را می نهم جل بر خر امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کفر این صنم گر دین نبازم</p></div>
<div class="m2"><p>نویسندم ملایک کافر امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو یک می باختم عمری دوشش را</p></div>
<div class="m2"><p>فکندم مهره را در ششدر امروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین عشرت که من جان می سپارم</p></div>
<div class="m2"><p>نمی گرید به مرگم مادر امروز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ظاهر دیده گر صورت پرستست</p></div>
<div class="m2"><p>منم جان را به معنی رهبر امروز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر دوران خرد نظمم «نظیری »</p></div>
<div class="m2"><p>کشد حسنش قلم در کشور امروز</p></div></div>