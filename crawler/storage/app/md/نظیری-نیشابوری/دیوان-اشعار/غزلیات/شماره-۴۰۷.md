---
title: >-
    شمارهٔ ۴۰۷
---
# شمارهٔ ۴۰۷

<div class="b" id="bn1"><div class="m1"><p>منادیست در آن کو که خون زنده سبیل</p></div>
<div class="m2"><p>به عشق نیست زیان قاتل است اجر قتیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاه بر ره مردان غیب دوخته ایم</p></div>
<div class="m2"><p>هنوز دیده به گردی نکرده ایم کحیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسوم فقر و توکل درازدستی نیست</p></div>
<div class="m2"><p>نشسته ایم که خرما در اوفتد ز نخیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به اضطراب، پدید آمدیم و نیست شدیم</p></div>
<div class="m2"><p>که در نهاد کرم بود، غایت تعجیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمال و جاه موافق به هم نساخته اند</p></div>
<div class="m2"><p>قبای سرو قصیر است و قد سرو طویل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شقاوت ازلی را، علاج نتوان کرد</p></div>
<div class="m2"><p>به مهد، جبهه بدخو سیه کنند از نیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بر و بحر زمین، فرصت اقامت نیست</p></div>
<div class="m2"><p>به چار حد جهان می زنند طبل رحیل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دمی سه چار، شبستان عمر روشن دار</p></div>
<div class="m2"><p>که روغنت به چراغ است و نور در قندیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشی باغ و گلستان طلب، نه مزرع و ده</p></div>
<div class="m2"><p>وظیفه گر نشود وجه می خداست کفیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قدح کش و به چمن صنع حق تماشا کن</p></div>
<div class="m2"><p>بسست سرو به تکبیر و مرغ در تهلیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به جان مپیچ «نظیری » اگر جنان خواهی</p></div>
<div class="m2"><p>که بوی باغ و چمن نشنود دماغ بخیل</p></div></div>