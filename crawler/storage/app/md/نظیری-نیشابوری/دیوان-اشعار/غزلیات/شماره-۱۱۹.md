---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>ابری به نظر آمد و برقی ز میان جست</p></div>
<div class="m2"><p>صد فتنه به هر مرحله از خواب گران جست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انگیخت از آن ظلمت و پرتو تن و جانی</p></div>
<div class="m2"><p>وز پرده برون آمد و در خانه جان جست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسوده ز آفات به هم ساخته بودیم</p></div>
<div class="m2"><p>ناگاه خطایی شد و تیری ز کمان جست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشنید کس از کس سخن مهر و محبت</p></div>
<div class="m2"><p>شوقی به ضمیر آمد و حرفی ز زبان جست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مدعیان غلغله افتاد ازین رشک</p></div>
<div class="m2"><p>منصف به میان آمد و منکر به کران جست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ربطست به او سر به سر اجزای جهان را</p></div>
<div class="m2"><p>زین سلسله حاصل که به جایی نتوان جست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین بیش حکایت نتوان کرد «نظیری »</p></div>
<div class="m2"><p>افروخت ورق در کف و آتش ز بنان جست</p></div></div>