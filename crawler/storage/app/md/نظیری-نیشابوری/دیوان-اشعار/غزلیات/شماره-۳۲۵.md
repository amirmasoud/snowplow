---
title: >-
    شمارهٔ ۳۲۵
---
# شمارهٔ ۳۲۵

<div class="b" id="bn1"><div class="m1"><p>یارب آن سرو که پرورده ای از اشک منش</p></div>
<div class="m2"><p>آفت صرصر بیگانه ببر از چمنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاتم لعل سلیمانی او باز آورد</p></div>
<div class="m2"><p>پیش از آن دم که برد آب و صفا اهرمنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق شوریدگیم می طلبد می ترسم</p></div>
<div class="m2"><p>که پریشان کند این خواب پریشان ز منش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهر برهم خورد ار باد به زلفش گذرد</p></div>
<div class="m2"><p>که کمینگاه صد آشوب بود هر شکنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسن زلف چو در چاه ذقن آویزد</p></div>
<div class="m2"><p>ابله آنست که در چه نرود از رسنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پارسایی که به سوداش دل از دست دهد</p></div>
<div class="m2"><p>گر فرشتست که باشد خطر از خویشتنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهر از افسانه و افسون لبش پر شده است</p></div>
<div class="m2"><p>گرچه دانم نبرد ره به دهانش سخنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سحر پرده اغیار بدرم تا چند</p></div>
<div class="m2"><p>همچو گل شب به هوا پاره کند پیرهنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق بی آتش و بی دود همه سوختن است</p></div>
<div class="m2"><p>عاشق آن نیست که خود داغ نهد بر بدنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تندرستیم و ز رنجوری خود در تابیم</p></div>
<div class="m2"><p>هرکه را رو دهد این عارضه بستر فکنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به امیدی که غزل های «نظیری » خوانی</p></div>
<div class="m2"><p>بالد از شوق تو چون غنچه زبان در دهنش</p></div></div>