---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>نظر به ظاهر و صیاد در خفا خفتست</p></div>
<div class="m2"><p>اجل رسیده چه داند بلا کجا خفتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا ز فتنه آن چشم نیم باز رهیم</p></div>
<div class="m2"><p>که فتنه خاسته از خواب و پای ما خفتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی به قلب شبم ترکتاز می آرد</p></div>
<div class="m2"><p>که بر فراش قصب پای در حنا خفتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمیم مهر ز باغ وفا نمی آید</p></div>
<div class="m2"><p>به هر چمن که تو نشکفته یی صبا خفتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبیب عشق ببرد طمع ز بیماری</p></div>
<div class="m2"><p>که شب به راحت ازین درد بی دوا خفتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی از معانقه روز وصل یابد ذوق</p></div>
<div class="m2"><p>که چند شب ز هم آغوش خود جدا خفتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگیر کام دل ای کعبتین مردم چشم</p></div>
<div class="m2"><p>که بردت آمده و نقش بر قفا خفتست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب امید به از صبح عید می گذرد</p></div>
<div class="m2"><p>که آشنا به تمنای آشنا خفتست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فسانه صرف «نظیری » مکن که خواب کند</p></div>
<div class="m2"><p>شکسته‌ای که به صد درد مبتلا خفتست</p></div></div>