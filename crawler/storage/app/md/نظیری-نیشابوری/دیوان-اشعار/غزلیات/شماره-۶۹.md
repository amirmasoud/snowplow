---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>صافی شوم از کون که در درد صفا نیست</p></div>
<div class="m2"><p>بر عرش زنم جوش که در خمکده جا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رویم همه چون سایه، که در خدمت خورشید</p></div>
<div class="m2"><p>صد گونه سجودست که در خدمت ما نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لطف نظر سوختگان تابش برقست</p></div>
<div class="m2"><p>اینجا پر پروانه طلب بال هما نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندان که در آن جعبه خدنگ است نصیبست</p></div>
<div class="m2"><p>در همت ما جستن و در شست خطا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخرام به گلشن که پی سیر و صبوحی</p></div>
<div class="m2"><p>پیغام گلی نیست که با باد صبا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توفیق نکوکاری ما و تو عطاییست</p></div>
<div class="m2"><p>اخلاص به دینار و مروت به بها نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صدگونه دوا بر سر هر شاخ گیاهیست</p></div>
<div class="m2"><p>اما چو تو را درد ندادند دوا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کفر و ضلالت بود ار دین و هدایت</p></div>
<div class="m2"><p>خوش باش که کار ازلی جز به عطا نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با حکم قضا ساز که در دیر «نظیری »</p></div>
<div class="m2"><p>مقبول فغان نیست نمازی که قضا نیست</p></div></div>