---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>دلم را نور رحمت از وداع جان فرو گیرد</p></div>
<div class="m2"><p>شهادت خانه ام را پرتو ایمان فرو گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل پرحسرتی دارم که هر سو چشم بگشایم</p></div>
<div class="m2"><p>سرشک حسرتم از دیده تا دامان فرو گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس ساید به هم در کیش طاقت ناوک آهم</p></div>
<div class="m2"><p>خراش سینه ام را سونش پیکان فرو گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خرسندی مدان گر بی تو بر بستر نهم پهلو</p></div>
<div class="m2"><p>سرم را اضطراب از زانوی حرمان فرو گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن ساعت که آهم گرد راه از چهره افشاند</p></div>
<div class="m2"><p>جراحت های اهل درد را درمان فرو گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به حسرت می سپارم جان، ببند از گریه چشمم را</p></div>
<div class="m2"><p>که گر اشکی بیفتد دهر را طوفان فرو گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر آید بجز یاد تو در خاطر «نظیری » را</p></div>
<div class="m2"><p>ز دل تا بگذراند صد رهش نسیان فرو گیرد</p></div></div>