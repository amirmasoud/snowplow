---
title: >-
    شمارهٔ ۳۸۴
---
# شمارهٔ ۳۸۴

<div class="b" id="bn1"><div class="m1"><p>نالم ز چرخ اگر نه بر افغان خورم دریغ</p></div>
<div class="m2"><p>گریم به دهر اگرنه به طوفان خورم دریغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر گل شکر نشاند و خون جگر دهد</p></div>
<div class="m2"><p>بر سفره سپهر به مهمان خورم دریغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبحم که بر صبوح خودم خوانده روزگار</p></div>
<div class="m2"><p>خندم به طنز و بر لب خندان خورم دریغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهمان مسرفم که به ممسک رسیده ام</p></div>
<div class="m2"><p>بر مرگ میزبان به سر خوان خورم دریغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با جاهلان معجبم افتاده اختلاط</p></div>
<div class="m2"><p>تحسین کنم به ظاهر و پنهان خورم دریغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کارم به دوستی ریایی فتاده است</p></div>
<div class="m2"><p>در مرگ دوستان به گریبان خورم دریغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیماری ضعیف خرد را علاج نیست</p></div>
<div class="m2"><p>با حکمت مسیح به درمان خورم دریغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دشوار کم شود اگر افسوس کم خورم</p></div>
<div class="m2"><p>مشکل از آن فتاده که آسان خورم دریغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بازآی تا به پای تو ریزم نثار خویش</p></div>
<div class="m2"><p>من آن نیم که بهر تو بر جان خورم دریغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شورابه ای که بر لبم از دیدگان چکد</p></div>
<div class="m2"><p>دونم اگر به چشمه حیوان خورم دریغ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آه و ناله عمر «نظیری » به سر رسید</p></div>
<div class="m2"><p>سیر آمدم ز بس که پریشان خورم دریغ</p></div></div>