---
title: >-
    شمارهٔ ۵۴۸
---
# شمارهٔ ۵۴۸

<div class="b" id="bn1"><div class="m1"><p>شد آخر روز بر ناییی و میل دل همان باقی</p></div>
<div class="m2"><p>بلا گردید ضعف پیری و طغیان مشتاقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بی باکی و رندی منفعل بودم چه دانستم</p></div>
<div class="m2"><p>که در پیری کشد کارم به سالوسی و زراقی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فقیهان شاهد عادل نویسندم صلاح آنست</p></div>
<div class="m2"><p>که در باقی کنم من بعد حرف شاهد و ساقی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به الطاف خداوندی امید واثقی دارم</p></div>
<div class="m2"><p>فراموشم نمی گردد بشارت های مشتاقی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوازش کن کزان لب های شیرین تلخ نافع تر</p></div>
<div class="m2"><p>حیات خضر یابم گر دهی زهرم به تریاقی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمال بوستان دیدم نه بخت و کار من ماند</p></div>
<div class="m2"><p>بنفشه در سیه کاری و گل در ساده اوراقی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«نظیری » واله صوت و سخن چندین مکن خاطر</p></div>
<div class="m2"><p>که ماند قصه را در جای نازک داستان باقی</p></div></div>