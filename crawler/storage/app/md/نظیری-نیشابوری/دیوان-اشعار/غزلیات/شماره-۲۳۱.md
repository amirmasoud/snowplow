---
title: >-
    شمارهٔ ۲۳۱
---
# شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>خوشا کز بس هجوم گریه ام در دامن آویزد</p></div>
<div class="m2"><p>سر دست نگارینم نگار از گردن آویزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان در دست آویزم به دل گرمی و دمسازی</p></div>
<div class="m2"><p>که در هنگام جان بازی به دشمن دشمن آویزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسازد بوی یوسف دیده یعقوب را روشن</p></div>
<div class="m2"><p>اگر عشق زلیخایش نه در پیراهن آویزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقیم کوی تو بی روی تو با بلبلی ماند</p></div>
<div class="m2"><p>که صیادش به ماه دی قفس در گلشن آویزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفتم در پر پروانه سوزم درنمی گیرد</p></div>
<div class="m2"><p>حذر کن زان که ناگه آتشم در روغن آویزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلی دارم به دست طعن ناصح چون کهن دلقی</p></div>
<div class="m2"><p>که در هر بخیه لختی خرقه اش از سوزن آویزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چراغ ما چه زیب و فر دهد محفل سرایی را</p></div>
<div class="m2"><p>که قندیل مه و مهرش فلک از روزن آویزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببینی گر جلایی از مه و پروین مشو ایمن</p></div>
<div class="m2"><p>به شکل خوشه گه صیاد دام از خرمن آویزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پی درد «نظیری » این همه گفت و شنو دارم</p></div>
<div class="m2"><p>گلی می‌چینم از گلشن که خاری در من آویزد</p></div></div>