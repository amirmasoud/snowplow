---
title: >-
    شمارهٔ ۵۳۵
---
# شمارهٔ ۵۳۵

<div class="b" id="bn1"><div class="m1"><p>دریغا در چنین فصلی حریفم یار بایستی</p></div>
<div class="m2"><p>میان بلبلانم جای در گلزار بایستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشد ز ایوان و قصر افراختن جمعیتم حاصل</p></div>
<div class="m2"><p>ره آمد شد غم سوی من دیوار بایستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سعی دیده شب‌زنده‌دارم کار نگشاید</p></div>
<div class="m2"><p>به جای دیده من بخت من بیدار بایستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین وقتی که بر ساقی و ساغر دسترس دارم</p></div>
<div class="m2"><p>کنار لاله‌زار و دامن کهسار بایستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بهر آنکه در پای سهی و نارون ریزم</p></div>
<div class="m2"><p>مرا چون غنچه گل شست پر دینار بایستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرم دستار از مخموری می برنمی‌تابد</p></div>
<div class="m2"><p>شرابم در سر و دستار در خمار بایستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هرکس می‌نشینم نشتری در آستین دارد</p></div>
<div class="m2"><p>پی آسودنم یک بار بی‌آزار بایستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل بلبل به این نالیدن آسایش نمی‌یابد</p></div>
<div class="m2"><p>نوای عشق را منقار موسیقار بایستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه‌کس لاف در خلوت «نظیری» می‌تواند زد</p></div>
<div class="m2"><p>تو را این خودفروشی بر سر بازار بایستی</p></div></div>