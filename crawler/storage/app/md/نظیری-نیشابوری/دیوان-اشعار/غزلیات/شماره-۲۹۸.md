---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>ذوق وجدان و نظر خالص شد و خامم هنوز</p></div>
<div class="m2"><p>صاف شد می ها ولی من دردی آشامم هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوش و لب بر مژده دیدار و قاصد در سفر</p></div>
<div class="m2"><p>خانه پر شادی و در راهست پیغامم هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برنمی آید هلال عیدم از ابر امید</p></div>
<div class="m2"><p>عمر رفت و همچو طفلان بر سر بامم هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز مولودم فلک محضر به فرزندی نوشت</p></div>
<div class="m2"><p>بس که خوارم از پدر نشنیده کس نامم هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیر هفتاد و دو ملت کرده ام در طور عشق</p></div>
<div class="m2"><p>کس نمی داند چه خواهد بود انجامم هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکر ابلیس و فریب دانه ام آمد بیاد</p></div>
<div class="m2"><p>بارها گشتم ز قید آزاد و در دامم هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از درون دوزخ ز بی تابی برون اندازدم</p></div>
<div class="m2"><p>صدره از خامی به آتش رفتم و خامم هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه از صحت ز بدمستی برونم کرده اند</p></div>
<div class="m2"><p>جرعه ای از رحم می ریزند در جامم هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکر اگر ردم «نظیری » تلخ بر طبعش نیم</p></div>
<div class="m2"><p>می کند گاهی لبی شیرین به دشنامم هنوز</p></div></div>