---
title: >-
    شمارهٔ ۳۵۹
---
# شمارهٔ ۳۵۹

<div class="b" id="bn1"><div class="m1"><p>ساقیا برخیز با مستان برقص</p></div>
<div class="m2"><p>عشق ساغر می کند گردان برقص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرقه‌ها را گل‌فشان کن از شراب</p></div>
<div class="m2"><p>جام بر کف چون گل خندان برقص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کفر و ایمان از برون پرده‌اند</p></div>
<div class="m2"><p>تو درون پرده با خاصان برقص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بانگ یاسبوح یا صوفی برآر</p></div>
<div class="m2"><p>بر ره ناقوس با رهبان برقص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جای در خلوت به بی‌ذوقی مگیر</p></div>
<div class="m2"><p>بر سَرِ خُم چون مِیِ جوشان برقص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راه ازین شورش به مقصد می‌رسد</p></div>
<div class="m2"><p>همچو کشتی بر سر طوفان برقص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برفشان هستی، که جانان جان ماست</p></div>
<div class="m2"><p>صوفیا با ساز و با دستان برقص</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر سرشکم در تماشا دیده‌ای‌ست</p></div>
<div class="m2"><p>لخت دل گو بر سر مژگان برقص</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوشمندان دار برپا می‌کنند</p></div>
<div class="m2"><p>مست گو منصور در زندان برقص</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست ازین کشتن «نظیری» زندگی</p></div>
<div class="m2"><p>روی بر شمشیر در میدان برقص</p></div></div>