---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>تا به کی بر خرقه بندم جسم غم فرسوده را</p></div>
<div class="m2"><p>سر به طوفان می دهم این مشت خاک سوده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در درون همچون عنب شد خوشه اشکم گره</p></div>
<div class="m2"><p>بس فرو خوردم به دل خون های ناپالوده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوش ها کر گشت و یارب یاربم کاری نکرد</p></div>
<div class="m2"><p>نیست گویا روزنی این سقف قیراندوده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خضر صد منزل به پیشم آمد و نشناختم</p></div>
<div class="m2"><p>بازمی باید ز سر گیرم ره پیموده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وه که یک قاصد که باشد محرم این راز نیست</p></div>
<div class="m2"><p>چند بر کاغذ نویسم حال و شویم دوده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شراب سودمندم بخت بد پرهیز داد</p></div>
<div class="m2"><p>می که می خوردم نمی خوردم غم بیهوده را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل ز بهر اشک لؤلؤیی رنگ کاهیم</p></div>
<div class="m2"><p>در بلورین حقه دارد کهربای سوده را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کنایت گاه مستی منع آن لب چون کنم</p></div>
<div class="m2"><p>می چکد بی خود حلاوت قند آب آلوده را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با «نظیری » چون نشینی گوش بر حرفش مکن</p></div>
<div class="m2"><p>در پریشانی میفکن خاطر آسوده را</p></div></div>