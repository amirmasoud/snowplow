---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>درد دل را می کنم با صبر پیوندی دگر</p></div>
<div class="m2"><p>بر طبیب خود تغافل می زنم چندی دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اعتمادی نیست بر عهدیی که نقصانی ندید</p></div>
<div class="m2"><p>هست در پیمان گسستن مهر پیوندی دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه می دانم قسم خوردن به جانت خوب نیست</p></div>
<div class="m2"><p>هم به جان تو که یادم نیست سوگندی دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای تا سر دیده ام از شوق رخسارت که هست</p></div>
<div class="m2"><p>هر سرشکم بی تو چشم آرزومندی دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیر کنعان با که گیرد انس در بیت الحزن</p></div>
<div class="m2"><p>بوی یوسف را نمی یابد ز فرزندی دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون به شرم بخششم کشتی حلالت ساختم</p></div>
<div class="m2"><p>کاین مروت نیست با طبع خداوندی دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاب می آری که از کف می نهی آیینه را</p></div>
<div class="m2"><p>از جمال تو ندیدم جز تو خرسندی دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکوه و شکر «نظیری » عکس کین و مهر تست</p></div>
<div class="m2"><p>آینه منما که طوطی نشکند قندی دگر</p></div></div>