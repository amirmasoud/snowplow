---
title: >-
    شمارهٔ ۴۳۹
---
# شمارهٔ ۴۳۹

<div class="b" id="bn1"><div class="m1"><p>شب نه تشویش صبا نی شور بلبل داشتم</p></div>
<div class="m2"><p>خلوتی تا صبحدم با سنبل و گل داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیش ها سیل بهاری بود، تا آمد گذشت</p></div>
<div class="m2"><p>صحبتی با دوستداران بر سر پل داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد آن مستان که برچیدند از اینجا نقل و جام</p></div>
<div class="m2"><p>بهره کیفیتی از جزو تا کل داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرتو اکسیر چشمانم به گنج افتاده بود</p></div>
<div class="m2"><p>هرچه می بردند در بردن تغافل داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کارم از یک زخمه آخر شد که ظاهر کرد عشق</p></div>
<div class="m2"><p>هرچه در جوهر ترقی و تنزل داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق و مستی زودتر زینم به مقصد می رساند</p></div>
<div class="m2"><p>دیر از آن رفتم که در رفتن تأمل داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در همه کاری مسافر را سبکباری خوش است</p></div>
<div class="m2"><p>بسکه ماندم توشه در بار توکل داشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌شنیدم از «نظیری» عشق وی کردم هوس</p></div>
<div class="m2"><p>کی چنین جانسوز دردی در تخیل داشتم</p></div></div>