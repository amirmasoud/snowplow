---
title: >-
    شمارهٔ ۳۸۶
---
# شمارهٔ ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>فتنه با زلف تو گرفته طرف</p></div>
<div class="m2"><p>دل ما را نمی دهد از کف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیم کش سر دهی خدنگ نگاه</p></div>
<div class="m2"><p>بگذرانی ز صد هزار صدف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست برد نگاه چالاکت</p></div>
<div class="m2"><p>مرد برباید از میانه صف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تو سلطان خزانه داو کند</p></div>
<div class="m2"><p>رفته بازی و مهره گشته تلف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاق بر مادر و پدر گردد</p></div>
<div class="m2"><p>از نکو پروریدن تو خلف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر بساطی که بندگان تواند</p></div>
<div class="m2"><p>خواجه را بر غلام نیست شرف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکجا نغمه و ترانه تست</p></div>
<div class="m2"><p>از کف مطربان نیفتد دف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جعد اگر ز آفتاب برداری</p></div>
<div class="m2"><p>ننماید به روی ماه کلف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن چه بی روی تو «نظیری » دید</p></div>
<div class="m2"><p>بی سلیمان ندیده بود آصف</p></div></div>