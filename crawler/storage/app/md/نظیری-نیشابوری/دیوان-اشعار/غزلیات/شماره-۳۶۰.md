---
title: >-
    شمارهٔ ۳۶۰
---
# شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>هرکه چون یوسف شود از محنت زندان خلاص</p></div>
<div class="m2"><p>قحطیان را می کند از قحط در کنعان خلاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زود از دنبال هر کام و تمنا می روند</p></div>
<div class="m2"><p>این تهی ظرفان نمی گردند از حرمان خلاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پادشاهان را دل ما رام کردن دولت است</p></div>
<div class="m2"><p>ما به دام آییم دشوار و شویم آسان خلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما نظربازیم و عاشق پیشه گو مفتی بدان</p></div>
<div class="m2"><p>نیست زاهد از ریا و عاشق از بهتان خلاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد خلوت نشین را دل به صد جا می رود</p></div>
<div class="m2"><p>کس نیابد از فریب آن صف مژگان خلاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش «نظیری » دامن وقتی به جنگ آورده‌ای</p></div>
<div class="m2"><p>دیر بازآید گر از دستت کند دامان خلاص</p></div></div>