---
title: >-
    شمارهٔ ۳۳۵
---
# شمارهٔ ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>گر جهان کشته بیداد شود برگذرش</p></div>
<div class="m2"><p>از تکبر به سوی خلق نیفتد نظرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قفا رو نکند بهر تسلای دلی</p></div>
<div class="m2"><p>گرچه یک شهر به فریاد رود بر اثرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه را فتنه او برد به یک بار ببرد</p></div>
<div class="m2"><p>هیچ کس باز نیابد که بگوید خبرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سر و مال اسیرانش امان می خواهند</p></div>
<div class="m2"><p>کس نگوید که مروت نبود این قدرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که از جنگ و پشیمانی او می ترسند</p></div>
<div class="m2"><p>مفت گویان نفروشند نصیحت به زرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لؤلوی دیده مردم به خزف نشمارد</p></div>
<div class="m2"><p>لیک خواهد که بریزند به دامن گهرش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه را شهرت سودای زلیخا باشد</p></div>
<div class="m2"><p>ماه گل پیرهن آرند همه از سفرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مژده کام به ما داد دهانش ز اول</p></div>
<div class="m2"><p>کس چه داند که همه هیچ بود چون کمرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخت ما را که مه چارده در ابر بود</p></div>
<div class="m2"><p>نکند جز به غلط ناله خروس سحرش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن هما کز نظر همت ما برمی خاست</p></div>
<div class="m2"><p>بود با نام تو آموخته کندیم پرش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وان تذروی که دم از فرط محبت می زد</p></div>
<div class="m2"><p>بود از سرو تو آویخته دادیم سرش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرچه نیکوست نو و کهنه «نظیری » نیکوست</p></div>
<div class="m2"><p>خشک سازیم رطب چون نفروشیم ترش</p></div></div>