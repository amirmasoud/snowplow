---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>پایمالم فتنه‌ای را هر که در شور آورد</p></div>
<div class="m2"><p>بر سر راهم بلا از هر طرف زور آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تخم غم در آب و خاک من نکو بر می دهد</p></div>
<div class="m2"><p>خرمنی حاصل کنم، گر دانه یی مور آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که شام زندگانی شمع بالینم نشد</p></div>
<div class="m2"><p>کی پس از مرگم چراغی بر سر گور آورد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق و تشریف هم آغوشی، محالست این که کس</p></div>
<div class="m2"><p>خلعت سلطان برای مفلس عور آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی همین هنگامه رسوایی من شد بلند</p></div>
<div class="m2"><p>عشق دایم بر سر بازار منصور آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن گل برقی به بستان زد که اکنون شاخ گل</p></div>
<div class="m2"><p>بلبل و پروانه را مجروح و رنجور آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجلس عشق از فروغ من «نظیری » روشن است</p></div>
<div class="m2"><p>موسی از بهر چراغم آتش از طور آورد</p></div></div>