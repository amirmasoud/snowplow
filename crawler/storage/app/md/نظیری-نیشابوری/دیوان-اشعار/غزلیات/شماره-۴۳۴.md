---
title: >-
    شمارهٔ ۴۳۴
---
# شمارهٔ ۴۳۴

<div class="b" id="bn1"><div class="m1"><p>نه مقامی که در آن زاد سفر تازه کنیم</p></div>
<div class="m2"><p>نه غباری که از آن سرمه نظر تازه کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی این بادیه هرگز نوزیدست نسیم</p></div>
<div class="m2"><p>سینه بر برق گشاییم و جگر تازه کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه از شعله چو پروانه پر انداخته ایم</p></div>
<div class="m2"><p>وز طپیدن نتوانیم که پر تازه کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تشنه دارند به بحر و دم آبی ندهند</p></div>
<div class="m2"><p>خود لب خشک به خوناب مگر تازه کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی بود یار سفر کرده ما بازآید</p></div>
<div class="m2"><p>جان مشتاق از آن سینه و بر تازه کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلق را فتنه این شهر فراموش شده</p></div>
<div class="m2"><p>زخم پنهان بنماییم و خبر تازه کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت آن شد که می از ساغر خورشید زنیم</p></div>
<div class="m2"><p>لبی از خنده شادی چو سحر تازه کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمس دین اختر اعظم به سعادت خواهیم</p></div>
<div class="m2"><p>نوبت سلطنت شمس و قمر تازه کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنده باشیم و ملوکانه حکومت رانیم</p></div>
<div class="m2"><p>روش دیگر و آیین دگر تازه کنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به تضرع کله فقر ز سر برداریم</p></div>
<div class="m2"><p>پادشاهانه همه تاج و کمر تازه کنیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نقش امید «نظیری » به جهان نتوان یافت</p></div>
<div class="m2"><p>به که این تخته بشوییم و ز سر تازه کنیم</p></div></div>