---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>جام گیر اختر افتاده بر افلاک انداز</p></div>
<div class="m2"><p>روح شو عاریت خاک سوی خاک انداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دعوی عقل جز از عشق مشخص نشود</p></div>
<div class="m2"><p>بحث کج را به در داور بی باک انداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با چنین دیده آلوده تو را نتوان دید</p></div>
<div class="m2"><p>دیده از خود ده و بر خود نظر پاک انداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش موهوم مرا از دل من پاک بروب</p></div>
<div class="m2"><p>بر در از خلوت خود ریزه خاشاک انداز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه جا دام ز گیسوی تو انداخته اند</p></div>
<div class="m2"><p>تو درین دشت عنان سر ده و فتراک انداز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه را بدرقه آن لشکر مژگان باشد</p></div>
<div class="m2"><p>گو همه بار به وادی خطرناک انداز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده آن که نظر جز به جمال تو کند</p></div>
<div class="m2"><p>ناوک انداز بر آن دیده و چالاک انداز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن شوخ از در و دیوار نماید ناچار</p></div>
<div class="m2"><p>باغبان گو سر پر عربده تاک انداز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن که در پیرهن پاره یوسف بیند</p></div>
<div class="m2"><p>گو نگاهی به سوی این جگر چاک انداز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوست گانی به حریفان سحر خیز دهند</p></div>
<div class="m2"><p>چاره علت مخمور به تریاک انداز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همت از ساغر لبریز «نظیری » خیزد</p></div>
<div class="m2"><p>می خور و نقب به گنجینه امساک انداز</p></div></div>