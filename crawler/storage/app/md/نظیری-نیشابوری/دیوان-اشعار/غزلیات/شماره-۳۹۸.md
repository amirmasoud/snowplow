---
title: >-
    شمارهٔ ۳۹۸
---
# شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>تا عشق چه ها کند به بلبل</p></div>
<div class="m2"><p>بسیار دریده پرده گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمشیر مقربان دریده</p></div>
<div class="m2"><p>دیوانه عشق بی تأمل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برتر بود آستانه عشق</p></div>
<div class="m2"><p>از هرچه خرد کند تعقل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانان خواهی گذر ز کونین</p></div>
<div class="m2"><p>دنیا سیل است و آخرت پل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آتش قهرت ار نشانند</p></div>
<div class="m2"><p>دل خسته مدار در توکل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چون رخ دلبران برآری</p></div>
<div class="m2"><p>از خرمن شعله شاخ سنبل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر مور نهاده اند باری</p></div>
<div class="m2"><p>کافلاک نمی کند تحمل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رحمی که ز دست می رود کار</p></div>
<div class="m2"><p>به غرقه جفا بود تغافل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوری چو تو یوسفی برآید</p></div>
<div class="m2"><p>از جنس توالد و تناسل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در عشق گریز تا بیابی</p></div>
<div class="m2"><p>ملکی که نکرده کس تخیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزم تو و آنگهی «نظیری »</p></div>
<div class="m2"><p>از چرخ نمی کند تنزل</p></div></div>