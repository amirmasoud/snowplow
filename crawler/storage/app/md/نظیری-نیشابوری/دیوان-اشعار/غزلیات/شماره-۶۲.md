---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>چون غنچه دل مبند و چو بو بر هوا متاب</p></div>
<div class="m2"><p>بر گل سوار باش و عنان از صبا متاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمیزش از صلاح دو یک دل به هم رسد</p></div>
<div class="m2"><p>جایی که تار میل نباشد دو تا متاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز خضر به تشنه زلال بقا نداد</p></div>
<div class="m2"><p>مس به امیدواری این کیمیا متاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوقی اگر نجات ز خودبینی ات دهد</p></div>
<div class="m2"><p>بگریز و رخ ز آینه هم بر قفا متاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سفره هیچ نیست سئوال از برون چرا</p></div>
<div class="m2"><p>قفل گشوده بر در گنج عطا متاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شغل توام ز گوشه خاطر نمی رود</p></div>
<div class="m2"><p>گوشم چو طفل از پی هر مدعا متاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر صفحه نقش ها همه زیبا کشیده اند</p></div>
<div class="m2"><p>برقع به دست کوته چون و چرا متاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آهی زدیم و پیرهن پاره سوختیم</p></div>
<div class="m2"><p>گو همنشین فتیله پی داغ ما متاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم از امیدواری دیدار روشن است</p></div>
<div class="m2"><p>گو روشنی مهر و مهم بر سرا متاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معشوق ساقی است مزن بر پیاله دست</p></div>
<div class="m2"><p>یوسف نموده رخ بصر از توتیا متاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>افسون لب به کار «نظیری » کفایت است</p></div>
<div class="m2"><p>نعلش در آتش از پی مهر و وفا متاب</p></div></div>