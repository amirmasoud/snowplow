---
title: >-
    شمارهٔ ۳۳۶
---
# شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>آن که غایب از نظر گردید درمی‌یابمش</p></div>
<div class="m2"><p>هرگه از خود بی‌خبر گردم خبر می‌یابمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوه سرو و فریب نرگسم دل می‌برد</p></div>
<div class="m2"><p>سوی بُستان می‌روم کآنجا اثر می‌یابمش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوییا شرط وفاداری به سر خواهد رساند</p></div>
<div class="m2"><p>بیشتر حاضر به هنگام خطر می‌یابمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون توانم غافل از مژگان خونریزش شدن</p></div>
<div class="m2"><p>من که دایم بر سر رگ نیشتر می‌یابمش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ نتوانم سر از فرمان او برتافتن</p></div>
<div class="m2"><p>کز رگ گردن به خود نزدیک‌تر می‌یابمش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیبت شام فراق او نرفتست از دلم</p></div>
<div class="m2"><p>روز فیروز که آید از سحر می‌یابمش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جوانی معتکف گشتم به پیری کوچه‌گرد</p></div>
<div class="m2"><p>آنچه در خلوت ندیدم در گذر می‌یابمش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوییا طول امل‌های «نظیری» کم شده</p></div>
<div class="m2"><p>اندکی در چشم مردم مختصر می‌یابمش</p></div></div>