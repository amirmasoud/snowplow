---
title: >-
    شمارهٔ ۳۷۲
---
# شمارهٔ ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>درود پاک تو بر ریش باصفا واعظ</p></div>
<div class="m2"><p>که ره ز قول تو دور است تا خدا واعظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو از عذاب خدا ما ز مغفرت گوییم</p></div>
<div class="m2"><p>نگاه کن تو کجایی و ما کجا واعظ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس ز دوری و بیگانگی زنی هر دم</p></div>
<div class="m2"><p>مگر دل تو به حق نیست آشنا واعظ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد از وعید تو پرگوش ما چه می گویی</p></div>
<div class="m2"><p>اگر به حشر بریم از تو ماجرا واعظ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جهل شوم به وحدت نیاوری اقرار</p></div>
<div class="m2"><p>تو را چه زهره تکذیب اولیا واعظ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فراز عرش نشان خدای می گویی</p></div>
<div class="m2"><p>کشد خدای به چشم تو توتیا واعظ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلام حق به غلط تا به کی کنی تفسیر</p></div>
<div class="m2"><p>تو هیچ شرم نداری ز مصطفا واعظ؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا حدیث «نظیری » تو را فروغ دهد</p></div>
<div class="m2"><p>نداده آیت قرآن تو را ضیا واعظ</p></div></div>