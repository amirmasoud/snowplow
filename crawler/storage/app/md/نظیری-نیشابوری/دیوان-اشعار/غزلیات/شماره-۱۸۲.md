---
title: >-
    شمارهٔ ۱۸۲
---
# شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>دلم از ناله خوش گردید امید اثر باشد</p></div>
<div class="m2"><p>بسی آسود شستم این خدنگم کارگر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر دزدیده دیدن ها نباشد بهر پاس دل</p></div>
<div class="m2"><p>محبت از تغافل های بیجا در خطر باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم تا خو به آسایش نگیرد روز خرسندی</p></div>
<div class="m2"><p>به خاطر شیوه یی آید که آن جانسوزتر باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هجران روز ما دارد غبار عالمی بر دل</p></div>
<div class="m2"><p>نباشد در شب ما روشنی گر صد سحر باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگویم جرم ادراکست، شرم غمزه را نازم</p></div>
<div class="m2"><p>که صدره کشته ام دید و ز حالم بی خبر باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن دورم، که بس دشوار باشد بال افشاندن</p></div>
<div class="m2"><p>اسیری را که گردی زین حرم بر بال و پر باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«نظیری » شاد هم باشی که خدمتکار دیرینی</p></div>
<div class="m2"><p>کدامت قدر و قیمت پیش او؟ خاکت به سر باشد</p></div></div>