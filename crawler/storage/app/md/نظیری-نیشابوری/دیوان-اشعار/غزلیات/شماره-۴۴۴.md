---
title: >-
    شمارهٔ ۴۴۴
---
# شمارهٔ ۴۴۴

<div class="b" id="bn1"><div class="m1"><p>تا از قضای دشت به گلشن فتاده ام</p></div>
<div class="m2"><p>از چشم طایران نوازن فتاده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نقش کارگاه جهانم نمود نیست</p></div>
<div class="m2"><p>کز ضعف همچو رشته ز سوزن فتاده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه سینه می خراشم و گه چهره می کنم</p></div>
<div class="m2"><p>شوریده تر ز باد به خرمن فتاده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی در حساب گوهرم آید نه در نظر</p></div>
<div class="m2"><p>از کیسه کریم به برزن فتاده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشتاق التفاتم و محتاج رحمتم</p></div>
<div class="m2"><p>چون طفل شیرخوار به دامن فتاده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سعیم اسیر دوست درین ترکتاز کرد</p></div>
<div class="m2"><p>طالع نگر که قسمت دشمن فتاده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین بوم و مرغزار نیم گر ملونم</p></div>
<div class="m2"><p>طاووس سدره ام ز نشیمن فتاده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز شهم که تا کشد از مرحمت مرا</p></div>
<div class="m2"><p>در دست این عجوز برهمن فتاده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طبل رحیل قافله سالار می زند</p></div>
<div class="m2"><p>من در طلسم بی در و روزن فتاده ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون گل به رنگ و بوی هوا خرقه در گرو</p></div>
<div class="m2"><p>دستار داغدار به گردن فتاده ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ریحان دمد به عشق «نظیری » ز آتشم</p></div>
<div class="m2"><p>در گلشن خلیل ز گلخن فتاده ام</p></div></div>