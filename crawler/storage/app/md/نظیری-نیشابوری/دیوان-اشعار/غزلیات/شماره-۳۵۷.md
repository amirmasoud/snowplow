---
title: >-
    شمارهٔ ۳۵۷
---
# شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>دم دم شاهدست و می می خاص</p></div>
<div class="m2"><p>لب ز لب بوسه چین و جان رقاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می بیغش برآمده ز سبو</p></div>
<div class="m2"><p>چون زر خالص از درون خلاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوییا در مزاج نافع او</p></div>
<div class="m2"><p>همه اشیا نهاده اند خواص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهر اندر محیط خم دیده</p></div>
<div class="m2"><p>می به شیشه چو دیده غواص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که با سلسبیل می ماند</p></div>
<div class="m2"><p>مستش ایمن بود به روز قصاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطربش چون سرود بردارد</p></div>
<div class="m2"><p>ماتمی را کند ز غصه خلاص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی سیم ساعدش باید</p></div>
<div class="m2"><p>ساغرش خواه سیم و خواه رصاص</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>واعظ ار رد ما کند خوانیم</p></div>
<div class="m2"><p>قول «القاص لایحب القاص »</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکسی از رهی رسد به خدای</p></div>
<div class="m2"><p>تو ز طاعت «نظیری » از اخلاص</p></div></div>