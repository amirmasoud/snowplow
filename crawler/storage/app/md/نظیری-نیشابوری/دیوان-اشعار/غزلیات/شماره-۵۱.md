---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>هر روز جویم آب رخ روز رفته را</p></div>
<div class="m2"><p>گویم به فخر ننگ ز مردم نهفته را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب بستم از سخن که درین مجمع نفاق</p></div>
<div class="m2"><p>به یافتم ز گفته حدیث نگفته را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز شب امید به دوران من ندید</p></div>
<div class="m2"><p>جام می دوساله و ماه دو هفته را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خفاش بخت من چو نبیند چه فایده</p></div>
<div class="m2"><p>گر سرمه زآفتاب کشد چشم خفته را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خون همیشه نشتر مژگان شکسته ام</p></div>
<div class="m2"><p>ناسفته کرده ام همه درهای سفته را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فراش کوی دوست شو ای ناله یک سحر</p></div>
<div class="m2"><p>در چشم بخت کن خس و خاشاک رفته را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهرست آب دیده «نظیری » نه اشک تلخ</p></div>
<div class="m2"><p>در دیده آب می کنم الماس تفته را</p></div></div>