---
title: >-
    شمارهٔ ۴۱۳
---
# شمارهٔ ۴۱۳

<div class="b" id="bn1"><div class="m1"><p>نمی گردید کوته رشته معنی، رها کردم</p></div>
<div class="m2"><p>حکایت بود بی پایان به خاموش ادا کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به لذت بود گر لخت جگر گر پاره دل بود</p></div>
<div class="m2"><p>نمک رفت از سخن تا به تکلف آشنا کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین دکان کاسد صد هنر بی یک سبب هیج است</p></div>
<div class="m2"><p>به مس محتاجم اکنون، گرچه مس را کیمیا کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدنگ جعبه توفیق امشب در کمانم بود</p></div>
<div class="m2"><p>غزالم در نظر بسیار خوب آمد خطا کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهادت را عوض، فردوس جانان داد در محشر</p></div>
<div class="m2"><p>دیت خود بود خونم را غلط کردم بها کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبم خوش بود و چشمم داشت الحق گریه گرمی</p></div>
<div class="m2"><p>شکایت بود بر لب، یاد او کردم دعا کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گره نیکو نمی زیبید آن ابروی زیبا را</p></div>
<div class="m2"><p>اگر افسون او، گر سحر بابل بود واکردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هر کاری که همت می گماری نصرت از حق جو</p></div>
<div class="m2"><p>که بر گنجشک دام افکندم و صید هما کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز کوی یار چون تو، درهم و آشفته می‌آمد</p></div>
<div class="m2"><p>«نظیری » کسب صد گلزار امروز از صبا کردم</p></div></div>