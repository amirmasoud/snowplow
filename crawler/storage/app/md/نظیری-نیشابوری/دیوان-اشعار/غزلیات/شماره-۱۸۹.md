---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>پیران که دقع قبض طباشیر برده‌اند</p></div>
<div class="m2"><p>آب رخ جوان به دم پیر برده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون من هر آن کسان که نفس کرده‌اند سرد</p></div>
<div class="m2"><p>نور سحر به نالهٔ شبگیر برده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرگشته‌اند اگرچه به تحصیل تجربه</p></div>
<div class="m2"><p>پی تا فراز طارم تدبیر برده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سالخوردگان نبود خوش فضول از آنک</p></div>
<div class="m2"><p>صحبت به ضیف خانه تقدیر برده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیران ز روز تیره سیه‌کار می‌شوند</p></div>
<div class="m2"><p>با آن که مو سفید سر از شیر برده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌باکی و غرور جوانی نماند حیف</p></div>
<div class="m2"><p>پیران همه خجالت و تقصیر برده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شادی به شیب کز می و افیون بود چه حظ؟</p></div>
<div class="m2"><p>این قوم ره به عیش به تزویر برده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کج شود به ما دل نازک به آن سزد</p></div>
<div class="m2"><p>بار گران به قامت چون تیر برده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با موی همچو سبحه کافور نگروند</p></div>
<div class="m2"><p>آنان که دل به زلف چو زنجیر برده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یوسف فریب گرگ ممثل کجا خورد؟</p></div>
<div class="m2"><p>روبَهْ به صید کردن نخجیر برده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وحشی چو تو، شکار «نظیری» کجا شود</p></div>
<div class="m2"><p>شهباز را به دام مگس گیر برده‌اند</p></div></div>