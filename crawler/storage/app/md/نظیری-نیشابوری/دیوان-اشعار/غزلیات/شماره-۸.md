---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ز شهر دوست می‌آیم پیام عشق بر لب‌ها</p></div>
<div class="m2"><p>به تلقینی کنم آزاد طفلان را ز مکتب‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگو منصور از زندان اناالحق گو برون آید</p></div>
<div class="m2"><p>که دین عشق ظاهر گشت و باطل ساخت مذهب‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو من هرکسی طبیبی دارد از زحمت چه غم دارد</p></div>
<div class="m2"><p>که آهی گر کشم بر کوه و صحرا افکنم تب‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سحرگه خسته و رنجور از خلوت برون آیم</p></div>
<div class="m2"><p>چو پروانه که از صحبت برآید آخر شب‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دست او جراحت‌های زهرآلوده بنمایم</p></div>
<div class="m2"><p>به زخم ناصحان سوزن زنند از نیش عقرب‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل شب داشت دردی از کدورت‌های حرمانم</p></div>
<div class="m2"><p>به سوی آسمان دیدم فرو بارید کوکب‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به محض التفاتی زنده دارد آفرینش را</p></div>
<div class="m2"><p>اگر نازی کند از هم فرو ریزند قالب‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بیدادی که بر دل شد نکردم ضبط خود ز اول</p></div>
<div class="m2"><p>کنون کاتش همی‌بارد پشیمانم ز یارب‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«نظیری» پر گشا تا دیده دل در گشایندت</p></div>
<div class="m2"><p>که از تنگی عالم تنگ می‌گردند مشرب‌ها</p></div></div>