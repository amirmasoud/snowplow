---
title: >-
    شمارهٔ  ۲ - این قطعه در تاریخ فوت جلال الدین اکبر پادشاه گفته شده
---
# شمارهٔ  ۲ - این قطعه در تاریخ فوت جلال الدین اکبر پادشاه گفته شده

<div class="b" id="bn1"><div class="m1"><p>جهاندار فرمان روا شاه اکبر</p></div>
<div class="m2"><p>که ملک از پدر تا به آدم گرفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو عالم جگر تشنه سیراب گشته</p></div>
<div class="m2"><p>نگینی کز انگشت از نم گرفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ایثار زر بس عرق کرده دستش</p></div>
<div class="m2"><p>ورق های افلاک بر هم گرفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو رفته به مسند به معراج رفته</p></div>
<div class="m2"><p>گرفته جهان را چو خاتم گرفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قضایای کیوان به ایوان گشوده</p></div>
<div class="m2"><p>عطایای رفرف به سلم گرفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان کرده کار جهان راست عدلش</p></div>
<div class="m2"><p>که دینش مسیحای مریم گرفته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسیط فلک دلگشاتر نموده</p></div>
<div class="m2"><p>بساط جهان را فراهم گرفته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرو رفته ز اوج شرف آفتابش</p></div>
<div class="m2"><p>چو یوسف ته چاه مظلم گرفته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس از مرگ او از کسوف مکرر</p></div>
<div class="m2"><p>یقین شد که خورشید ماتم گرفته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان برده مرگش ز ایام شادی</p></div>
<div class="m2"><p>که نوروز رنگ محرم گرفته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز ویرانی ملکش افسوس خوردم</p></div>
<div class="m2"><p>کسی گفت ملک جهان کم گرفته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو سال وفاتش به تاریخ دیدم</p></div>
<div class="m2"><p>«به اقبال ایزد دو عالم گرفته »</p></div></div>