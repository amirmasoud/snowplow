---
title: >-
    شمارهٔ ۱۰ - ایضا در مدح خان خانان بن بیرام خان واقعست
---
# شمارهٔ ۱۰ - ایضا در مدح خان خانان بن بیرام خان واقعست

<div class="b" id="bn1"><div class="m1"><p>چو شمع سوز دلم عشق بر زبان انداخت</p></div>
<div class="m2"><p>دگر نخواستی آتش مرا به جان انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرد ببوی معانی بخست چندانم</p></div>
<div class="m2"><p>که بیخت خاکم و بیرون ز آستان انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دم از فراق عزیزان نمی توانم زد</p></div>
<div class="m2"><p>که از بلندترین پایه ام زمان انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب دراز نخوابم که شور احبابم</p></div>
<div class="m2"><p>نمک به مردمک چشم خون فشان انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به شاخ سدره ز مرغان خوش نوا بودم</p></div>
<div class="m2"><p>جفای حادثه بر خاکم آشیان انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز سینه کنم پیش اگرچه شست قضا</p></div>
<div class="m2"><p>خطا نکرد خدنگی که بر نشان انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه گویم از خم چوگان او خلاصی نیست</p></div>
<div class="m2"><p>که هرکسم به کران دید در میان انداخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین مخاطره کس دست کس نمی گیرد</p></div>
<div class="m2"><p>ز بحر بیهده ام موج بر کران انداخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به فقر ساخته بودم فریب عشق مرا</p></div>
<div class="m2"><p>به دست صد هوس مختلف عنان انداخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به جام و مطربه گفتم وظیفه کافی نیست</p></div>
<div class="m2"><p>ببایدم شد و ادرار بر مغان انداخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جمال خدمت صاحب که شسته ام ز غرض</p></div>
<div class="m2"><p>نظر به طمع نمی بایدم بر آن انداخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به غیر هم نبرم التجا که گویندم</p></div>
<div class="m2"><p>رسوم او بفلان بود بر فلان انداخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به هر طریق دلم نقش بست دید خطا</p></div>
<div class="m2"><p>بباید این ورق از اصل داستان انداخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به جام جم ندهم آبرو که همت طبع</p></div>
<div class="m2"><p>مرا سفینه به دریای بیکران انداخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ثنا به زر نفروشم که لذت ورعم</p></div>
<div class="m2"><p>ز عیش مدحت عبدالرحیم خان انداخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز ذکر دوست به گرمای حشر سیرابم</p></div>
<div class="m2"><p>که در میانه کوثر مرا نشان انداخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همین بس است سعادت که یار پرسش من</p></div>
<div class="m2"><p>به خوش بیانی کلک گهرفشان انداخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به این شرف که به تشریف خاصش ارزیدم</p></div>
<div class="m2"><p>ز وجد خرقه چو پروانه مرغ خان انداخت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به خط و خلعت او چون مفاخرت نکنم؟</p></div>
<div class="m2"><p>مرا به تربیت آوازه در جهان انداخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بساط کهنه اگر روزگار برچیند</p></div>
<div class="m2"><p>اساس تازه بسی طرح می توان انداخت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باو مدیح فرستادنم بدان ماند</p></div>
<div class="m2"><p>که نخل میوه به دامان باغبان انداخت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به مجلسش چو رود مدح من چنان گویند</p></div>
<div class="m2"><p>که دزد قیمت کالا به کاروان انداخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سخن زیاده نباید سرود باید گفت</p></div>
<div class="m2"><p>شکر به یاد لبت طوطی از دهان انداخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به این قدر که «نظیری » سپاس نعمت گفت</p></div>
<div class="m2"><p>پری مغز شکافش بر استخوان انداخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بس این دعات که اعدات بی کمان افتند</p></div>
<div class="m2"><p>چنان که دولت تو تیر بی کمان انداخت</p></div></div>