---
title: >-
    شمارهٔ ۱۲ - این قصیده نیز بعد از معاودت مکه به احمدآباد گجرات در مدح شاهزاده همایون نژاد شاهزاده مراد گفته شده
---
# شمارهٔ ۱۲ - این قصیده نیز بعد از معاودت مکه به احمدآباد گجرات در مدح شاهزاده همایون نژاد شاهزاده مراد گفته شده

<div class="b" id="bn1"><div class="m1"><p>پس از ادای طواف حج و رسوم عباد</p></div>
<div class="m2"><p>به سیر عرصه گجراتم اتفاق افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قبول جذبه آن آب و خاکم از کشتی</p></div>
<div class="m2"><p>چو دست قابله از مهد برکنار نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان به شوق خرامان شدم در آن کشور</p></div>
<div class="m2"><p>که سوی حجله زیبا عروس، نوداماد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر رهی که چمیدم وزید باد امید</p></div>
<div class="m2"><p>به هر دری که رسیدم، رسید بانگ گشاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپهر خواست که بختم به رفعتی برسد</p></div>
<div class="m2"><p>ز دور چشمم بر قصر شهریار افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هجوم بار ملک بود در شدم به میان</p></div>
<div class="m2"><p>تلاش عام در آن بارگاه بارم داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درآمدم به حریمی تمام حور و قصور</p></div>
<div class="m2"><p>ز بوس و خنده گلستان ز جرعه نوش آباد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طرب نهاده درو بر سر طرب اسباب</p></div>
<div class="m2"><p>فرح نهاده درو بر پی فرح بنیاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طرب که رخت کشید اندرو برون نکشید</p></div>
<div class="m2"><p>فرح که پای نهاد اندرو برون ننهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دگر نگشت جگرگوشه دربدر کان را</p></div>
<div class="m2"><p>که خانه زان شرف آفتاب کرد آباد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمانه بهر همین روز داشت عیشی را</p></div>
<div class="m2"><p>که هر دو روز امانت به دیگری می داد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نثار عمر گرامی به رو نما آرند</p></div>
<div class="m2"><p>شب امید جهان روز شادمانی داد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برات بخشش من خواست بر مراد دهد</p></div>
<div class="m2"><p>به پیش منشی شب شام او نهاد مداد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شبی چو چشم عزیزان به روی هم روشن</p></div>
<div class="m2"><p>غم زمانه نفهمیده چون دل آزاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شبی دو دیده عشاق زو بارایش</p></div>
<div class="m2"><p>ز خاک کنده معشوق برگرفته سواد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو داد بخشی آن شب به یاد می آید</p></div>
<div class="m2"><p>ز روشنایی دل نور می زند فریاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در آن بساط چو بر خود مرا شعور نبود</p></div>
<div class="m2"><p>ز دور دیده دانا دلی به من افتاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به مهر گفت که ای زیب بخش مجمع انس</p></div>
<div class="m2"><p>بیا بیا که وقت آمدی مبارک باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نشاط مجلس و آیین جشن فروردیست</p></div>
<div class="m2"><p>تو نیز جلوه آیین نظم خواهی داد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همین بگفت و دوید و هنوز پیدا بود</p></div>
<div class="m2"><p>که شد غریو کزین قطره کرده دریا یاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنان به پایه دولت شدم شتاب زده</p></div>
<div class="m2"><p>که چند بار سرم در مقام پا افتاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بس که تیزبان بارگاه در رفتم</p></div>
<div class="m2"><p>ادب ز پایه خود پای بر فراز نهاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز دلفریبی آیین و فر سلطانی</p></div>
<div class="m2"><p>به گاه تهنیتم رسم سجده رفت از یاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو خوب رسم ادب را بجا نیاوردم</p></div>
<div class="m2"><p>ندا رسید که ای روستای مادرزاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بساط عرش و تکبر؟ تو را چه پیش آمد؟</p></div>
<div class="m2"><p>حریم کعبه و غفلت؟ تو را چه حال افتاد؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به دست شمع چو پروانه عطا دادند</p></div>
<div class="m2"><p>درین بساط شبی بر سر قدم استاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز عندلیب شود شاخ گل غزلخوان تر</p></div>
<div class="m2"><p>اگر وزد به گلستان ازین شبستان باد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جواب دادم و گفتم به جرم معذورم</p></div>
<div class="m2"><p>که تا منم به چنین دولتی نگشتم شاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به محفلی که دو قندیل ماه و خورشیدست</p></div>
<div class="m2"><p>چراغ بخت ضعیفی چه نور خواهد داد؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر این نشاط و تماشا اگر نظر فکند</p></div>
<div class="m2"><p>نسیم شانه کند گم به طره شمشاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهان چو روی شه آراستست و چشم مرا</p></div>
<div class="m2"><p>نگاه بخت ضعیف است در حجاب رماد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شه خلاصه خدم یوسف ستاره حشم</p></div>
<div class="m2"><p>همای سدره اقبال شاهزاده مراد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زمین چو صفحه تقویم پر ز خانه شود</p></div>
<div class="m2"><p>اگر عدالت او سایه افکند به بلاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>قبای ملک بر اندازه دید بر قد او</p></div>
<div class="m2"><p>نهاد فتنه کلاه از سر و کمر بگشاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سخن به میکده از اعتدال او می رفت</p></div>
<div class="m2"><p>شد از طبیعت مستان برون خیال فساد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز بس که امن شد اندر زمان او عالم</p></div>
<div class="m2"><p>به دام خوشه برآورد دانه صیاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تمام خلق در ایام او غنی گشتند</p></div>
<div class="m2"><p>حسد به مهر بدل گشت در دل حساد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنان سلامت عهدش در اجل دربست</p></div>
<div class="m2"><p>که حور خلد فشاند ز زلف گرد کساد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دل ولایت خسرو به وصل او مایل</p></div>
<div class="m2"><p>چنان که خاطر شیرین به صحبت فرهاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو در شمایل او بنگرند فخر کنند</p></div>
<div class="m2"><p>مکارم پدر و حسن سیرت اجداد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو ماه بدر خرامان میانه انجم</p></div>
<div class="m2"><p>میان حلقه روشن دلان مهرنژاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تمام سال به ملکش نشاط و مهمانی</p></div>
<div class="m2"><p>چو در عجم مه نوروز و در عرب اعیاد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کشیده سر به فلک همچو سرو آزاده</p></div>
<div class="m2"><p>سران ملک سر افکنده پیش او منقاد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به رمز دلبری و رسم خسروی داده</p></div>
<div class="m2"><p>تسلی دل جمع از الوف تا آحاد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به یک نگاه که از دور بر صف افکنده</p></div>
<div class="m2"><p>نموده پایه هرکس به قدر استعداد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به پای قبه او پشت گرمی اقطاب</p></div>
<div class="m2"><p>به روی سده او سرفرازی اوتاد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عساکرش همه از اولیا و از ابدال</p></div>
<div class="m2"><p>مجاورش همه از سابقان و از افراد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همه به حرب و جدل لیک بر سبیل صلاح</p></div>
<div class="m2"><p>همه به سیر و سفر لیک بر طریق رشاد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همیشه رخش وغا زیر ران به تیغ زدن</p></div>
<div class="m2"><p>همیشه تیغ غزا بر میان به غزو و جهاد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ازو به بدرقه افراد خواسته همت</p></div>
<div class="m2"><p>ازو به فاتحه اقطاب جسته استمداد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به بزم مملکتش عامل گرسنه قلم</p></div>
<div class="m2"><p>به گرد سفره نگردد چو گربه زهاد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به صحن بارگهش شحنه و رییس و عسس</p></div>
<div class="m2"><p>ستاده سجده به کف در خضوع و در اوراد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز صبح تا به دم شام بر سر عالم</p></div>
<div class="m2"><p>چو آفتاب زرافشان شده به دست جواد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ستاده بدره ببر خازنانش در تقسیم</p></div>
<div class="m2"><p>نشسته نسخه به کف مادحانش در انشاد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رسانده روزی مقدور بیش از تقدیر</p></div>
<div class="m2"><p>سپرده قسمت موجود پیش از ایجاد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دوباره سبعه الوان کشیده در هر روز</p></div>
<div class="m2"><p>چون نزل سبع مثانی ز خوان سبع شداد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به شکل راتبه خواران دونان مه و خورشید</p></div>
<div class="m2"><p>صباح و شام گرفته ز خوان او معتاد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>طبق ز نقل کواکب گرفته زهره به دست</p></div>
<div class="m2"><p>به شب نشین حریمش چو خوانچه ای افتاد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عروس کعبه که ام القرای آفاق است</p></div>
<div class="m2"><p>وظیفه خواره او با قبیله و احفاد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز ارض تا به سما چشم بر عنایت او</p></div>
<div class="m2"><p>عیال جود وی آبای علوی و اولاد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به آب ساخته آتش ز عون مطبخ او</p></div>
<div class="m2"><p>که برگرفته خلاف از میانه اضداد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز بس ملایمت خلق او عجب نبود</p></div>
<div class="m2"><p>که پر ز مغز شود از نوال او اجساد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به جای خون همه لعل و درر برون آید</p></div>
<div class="m2"><p>به نیشتر رگ دست ار گشایدش فصاد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به تحت یک نظرش فصل صد خریف و ربیع</p></div>
<div class="m2"><p>به ضمن یک سخنش جفر جامع و اعداد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>زهی نتیجه لطف خدای عز وجل</p></div>
<div class="m2"><p>معاند تو کند با خدای خویش عناد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>عدو ز حلم تو بر خود چو بید می لرزد</p></div>
<div class="m2"><p>در آستین صبوریست خنجر فولاد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تو همچو سرو به آزادگی مثل شده ای</p></div>
<div class="m2"><p>اگر کج است فلک بد به راستی مرساد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تویی که بوده و نابوده جهان از تست</p></div>
<div class="m2"><p>سخن درست بگفتیم هرچه باداباد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زهی به رشد تو چشم جهانیان روشن</p></div>
<div class="m2"><p>خلیفه دو جهان را تویی مرید و مراد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز شفقت جد و بابت بر اهل هر ملت</p></div>
<div class="m2"><p>ام زمانه به یک رنگ کفر و ایمان زاد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>طبیعت همه ابنای دهر ملحد شد</p></div>
<div class="m2"><p>ولی ز فطنت تو بر طرف فتاد الحاد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>وگرچه فضله ای از فاضلان جاهل عصر</p></div>
<div class="m2"><p>به طمع جاه و غنا کرد مذهبی ایجاد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>پس از حصول مرادات حال آن فاسد</p></div>
<div class="m2"><p>مثل چو باغ ارم گشت و حسرت شداد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نخست روز که شد خلق آسمان و زمین</p></div>
<div class="m2"><p>کتاب محمل مبدا شدند تا به معاد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به قابلیت هر جزو و کل نظر کردند</p></div>
<div class="m2"><p>شتافتند به خدمت که بودت استعداد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>اراده این که به صدر تو تربیت یابند</p></div>
<div class="m2"><p>چو یافت رتبه تو لب گزید از استبعاد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>نگاه کرد به بالا و سر فرود آورد</p></div>
<div class="m2"><p>زمین فتاد به درگاه و آسمان استاد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز روی صدق کنون در مقام بندگی اند</p></div>
<div class="m2"><p>به هر که لطف تو بینند می کنند امداد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>امید هست که احوال من شود روشن</p></div>
<div class="m2"><p>کنون که بر سر من فر شهریار افتاد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>خرد به کعبه چو رشدم همی نمود نمود</p></div>
<div class="m2"><p>به عزم کعبه درگاه صاحبم ارشاد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چنان به جاذبه شوق خلیفه می بردم</p></div>
<div class="m2"><p>که تر نمی شودم پای از شط بغداد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چنان به مرجع اخلاص خود شتابانم</p></div>
<div class="m2"><p>که قرض خواه تنک مایه جانب میعاد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>رفیق کس نشوم با توجهم همراه</p></div>
<div class="m2"><p>بسیج ره نکنم با توکلم همزاد</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>اگر خزینه عالم به من فرو ریزند</p></div>
<div class="m2"><p>درم خریده لطفم نمی شود آزاد</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>دری که مرجع اخلاص خویش ساخته ام</p></div>
<div class="m2"><p>اگر خراب شود دل بر آن نهم بنیاد</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>صفای دل به ولی نعمتم خداداست</p></div>
<div class="m2"><p>اگر طریقه ملت یکی است گر هفتاد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>سخن که نگهت اخلاص از آن نمی آید</p></div>
<div class="m2"><p>اگرچه نقش «نظیری »ست بر صحیفه مباد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>اگر رفیق رهم زاد همتی سازی</p></div>
<div class="m2"><p>به راحتم برسانی که آفتت مرساد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>همیشه تا ببهارست خنده زن لب گل</p></div>
<div class="m2"><p>مدام تا به خزانست تیغ زن دم باد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به بحر خاطر تو خنده موج زن بادا</p></div>
<div class="m2"><p>به جوی جان عدو آب خنجر جلاد</p></div></div>