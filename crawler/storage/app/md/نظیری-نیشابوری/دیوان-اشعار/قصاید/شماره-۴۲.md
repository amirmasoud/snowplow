---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>نبی که معجز ماه دو پیکر آورده</p></div>
<div class="m2"><p>مثال نور خود و نور حیدر آورده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فراز منبر یوم الغدیر این رمزست</p></div>
<div class="m2"><p>که سر ز جیب محمد علی برآورده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حدیث «لحمک لحمی » بیان این معنیست</p></div>
<div class="m2"><p>که بر لسان مبارک پیمبر آورده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدای از آدمشان تا به آل عبد مناف</p></div>
<div class="m2"><p>به صلب پاک و به بطن مطهر آورده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم از سرایت این نور آل زهرا را</p></div>
<div class="m2"><p>نبی به زیر عبا با علی درآورده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قوی بذریت خویش دیده ظهر علی</p></div>
<div class="m2"><p>پیمبرش شب هجرت به بستر آورده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به آب زمزم و خاک صفا سرشته گلش</p></div>
<div class="m2"><p>حقش به حکمت و عفت مخمر آورده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهاده وقت ولادت به خاک کعبه جبین</p></div>
<div class="m2"><p>نیاز و بندگی از بطن مادر آورده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدیجه نور نبی دیده در جبین علی</p></div>
<div class="m2"><p>به شادمانی داماد دختر آورده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بعرس فاطمه و مرتضی نثار ملک</p></div>
<div class="m2"><p>درخت های جنان حله ها برآورده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درخت طوبی اسناد جنت و انهار</p></div>
<div class="m2"><p>برون به نام محبان حیدر آورده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سزد که خاک کشد آفتاب اندر چشم</p></div>
<div class="m2"><p>ز معدنی که چو سبطین گوهر آورده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درون قبه بیضاست جای ذوالقرنین</p></div>
<div class="m2"><p>ز عرش آل عبا رخت برتر آورده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>علی به جای سر است از جسد پیمبر را</p></div>
<div class="m2"><p>که صحتش همه رای منور آورده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برو به سبقت اسلام کس مقدم نیست</p></div>
<div class="m2"><p>به بعثت نبی ایمان برابر آورده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هزار شاهد عادل به مجمع اسلام</p></div>
<div class="m2"><p>به دعوی «انا صدیق اکبر» آورده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نبی به کودکی اسلام کرده تعلیمش</p></div>
<div class="m2"><p>نه همچو غیر به ایمانش کافر آورده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به دوستکانی این باده از لبان حبیب</p></div>
<div class="m2"><p>کرامتیست که ساقی کوثر آورده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز قول ثابت «لولا علی » برجم نسا</p></div>
<div class="m2"><p>به فضل خویش مثالی مقرر آورده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ندای «بخ بخ لک یا علی مولایی »</p></div>
<div class="m2"><p>گواه نص ولایت به محضر آورده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خلاف مشورت او که کرده ذوالنورین</p></div>
<div class="m2"><p>خروش توبه به بالای منبر آورده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>محل شدت نیران فتنه اشرار</p></div>
<div class="m2"><p>علی ز مهلکه اش بارها برآورده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیان صفدر کرار عسکر فرار</p></div>
<div class="m2"><p>به شرح واقعه حرب خیبر آورده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نبی به وقت مؤاخات عزت و اصحاب</p></div>
<div class="m2"><p>به لفظ و صدق علی را برابر آورده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو بر ولایت او هر نبی شده مبعوث</p></div>
<div class="m2"><p>زمان ازو که درین کار بهتر آورده؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وصی کسیست که تجهیز مصطفی کرده</p></div>
<div class="m2"><p>نه آن که میل به محراب و منبر آورده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>میان خوف و رجا داده حق نشان نجات</p></div>
<div class="m2"><p>نبی که مندر و والی رهبر آورده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گشایش از در دیگر مجو به حکم خدا</p></div>
<div class="m2"><p>که غیر باب علی را به گل برآورده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کسی ز آتش دوزخ بری نخواهد ماند</p></div>
<div class="m2"><p>مگر کسی که تولا به حیدر آورده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خلاف نیست روا در خلیفه ای که نبیش</p></div>
<div class="m2"><p>«خلیفتی و خلیلی » مکرر آورده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>امام اوست که در تن حیات موتا را</p></div>
<div class="m2"><p>چو عیسی از نفس روح پرور آورده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز بس محبت سکان آسمان ایزد</p></div>
<div class="m2"><p>به شکل او ملکی را مصور آورده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مقام مجد گرفته به عرش علیین</p></div>
<div class="m2"><p>لوای حمد به صحرای محشر آورده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چگونه نور کسی را به گل توان اندود</p></div>
<div class="m2"><p>که آفتاب فرو رفته را برآورده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چگونه قول کسی را توان به خاک انداخت</p></div>
<div class="m2"><p>که سنگ ریزه گرفتست و گوهر آورده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همای همت زوج بتول آن مرغیست</p></div>
<div class="m2"><p>که دولت دو جهان زیر شهپر آورده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بیان نسبت خود کرده با خلیل علی</p></div>
<div class="m2"><p>برون ز کعبه صنم های آزر آورده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز کوی نخوت و پندار احتسابت او</p></div>
<div class="m2"><p>کلیم مست و براهیم بتگر آورده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خدا دوازده تن را ز عترت اطهار</p></div>
<div class="m2"><p>امام خلق جهان تا به محشر آورده</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کسی که پی به امام زمان خود نبرد</p></div>
<div class="m2"><p>رسول صادقش از خیل کافر آورده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تویی امام که اقرار بر امامت تو</p></div>
<div class="m2"><p>صهیب و جابر و سلمان و بوذر آورده</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نه والیی که به حقیت ولایت خویش</p></div>
<div class="m2"><p>سجل به مهر رئیس و توانگر آورده</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خدا محبت آل تو کرده فرض و تو را</p></div>
<div class="m2"><p>به آیت «الوالارحام » سرور آورده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زبیر و طلحه که از بیعتت برون شده اند</p></div>
<div class="m2"><p>اجل به نزد خداشان مکدر آورده</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>قضای چرخ بر آن جاهلی زند خنده</p></div>
<div class="m2"><p>که در مشاوره حرف مزور آورده</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همان که خسته اشرار کرده عثمان را</p></div>
<div class="m2"><p>عزا نهاده به پاداش و لشکر آورده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سگان دست برو گرد کنند نوحه سزاست</p></div>
<div class="m2"><p>مخدرات رسول از حرم برآورده</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سپه که خوانده به جنگ جمل زبیر عوام</p></div>
<div class="m2"><p>کف خسی به سر راه صرصر آورده</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>معاندان که سبب گشته حرف صفین را</p></div>
<div class="m2"><p>صف غزال به جنگ غضنفر آورده</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>معاندان تو را نیست مغفرت که رسول</p></div>
<div class="m2"><p>به تو محاربه با خود برابر آورده</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مگوی خال که خال نبی به کتف نبی است</p></div>
<div class="m2"><p>نه قرحه ای که سزاوار نشتر آورده</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نزاع و صلح تو میزان باطل و حقست</p></div>
<div class="m2"><p>که بهر دوزخ و فردوس داور آورده</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دلیر تیغ نرانم به موی استردن</p></div>
<div class="m2"><p>که یار شیفته مو خال بر سر آورده</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کدام روز؟ که گفتست ابن ملجم را؟</p></div>
<div class="m2"><p>به مهر فاحشه ای خون حیدر آورده</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خوشا علی که چنان در نماز محو شده</p></div>
<div class="m2"><p>که سجده با الم زخم منکر آورده</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>عجایب این که به شکل خود و لباس نبی</p></div>
<div class="m2"><p>به کفن و دفن خود اعرابیی درآورده</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نموده مرده و زنده دو تن به یک صورت</p></div>
<div class="m2"><p>ز یک حقیقت مخفی دو پیکر آورده</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>میان احمد و حیدر تمیز نتوان کرد</p></div>
<div class="m2"><p>درین مقام بیانی سخنور آورده</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همان که گشته پسر را تکاور از سهر مهر</p></div>
<div class="m2"><p>برای نعش پدر هم تکاور آورده</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سخن به پرده علی گفته در شب معراج</p></div>
<div class="m2"><p>صباح تهنیه پیش پیمبر آورده</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نبی ز زله شب نیم سیب کرده عیان</p></div>
<div class="m2"><p>ز جیب نیمه دیگر علی برآورده</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز فکر بوالعجبی های قادر بیچون</p></div>
<div class="m2"><p>به حیرتم که عجایب دو مظهر آورده</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هراس نیست ز فوت و فنا «نظیری » را</p></div>
<div class="m2"><p>که پی به چشمه خضر و سکندر آورده</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کدورت از چه جهت رو دهد؟ محبی را</p></div>
<div class="m2"><p>که از ولای علی دل منور آورده</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چه کم کند ز جلال کسی زیان جهان؟</p></div>
<div class="m2"><p>که خواجگی ز غلامی قنبر آورده</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به نظم آخرت از دست داده دنیا را</p></div>
<div class="m2"><p>فکنده رخت به دریا و گوهر آورده</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کسی ز طاعت و خدمت زیان نمی بیند</p></div>
<div class="m2"><p>که هر که تحفه رطب برد شکر آورده</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>قبول سمع تو کافیست یا علی ولی</p></div>
<div class="m2"><p>زمانه گوش تمیز از ازل کر آورده</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ز آستان تو دورم اگر به بیداری</p></div>
<div class="m2"><p>مرا به واقعه نور تو در بر آورده</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ازان شبی که به این خواب گشته ام مسرور</p></div>
<div class="m2"><p>خرد به هر نظرم پایه برتر آورده</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به مدحت تو بس این عز که همگنان گویند</p></div>
<div class="m2"><p>برات جایزه بر حوض کوثر آورده</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>صلت که می طلبد بنده ثناگویت</p></div>
<div class="m2"><p>مناقب تو نگویم که در خور آورده</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هنر بس است همین کز برای ختم سخن</p></div>
<div class="m2"><p>درود پاک بر آل مطهر آورده</p></div></div>