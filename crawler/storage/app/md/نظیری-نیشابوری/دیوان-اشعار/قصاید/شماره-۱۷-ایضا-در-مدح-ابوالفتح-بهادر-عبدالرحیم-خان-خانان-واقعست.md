---
title: >-
    شمارهٔ ۱۷ - ایضا در مدح ابوالفتح بهادر عبدالرحیم خان خانان واقعست
---
# شمارهٔ ۱۷ - ایضا در مدح ابوالفتح بهادر عبدالرحیم خان خانان واقعست

<div class="b" id="bn1"><div class="m1"><p>چنان ز دقتم اندیشه تنگ میدان شد</p></div>
<div class="m2"><p>که لفظ و معنیم از طبع روی گردان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نردبان سخن پای فکرتم درماند</p></div>
<div class="m2"><p>که برشوم به سوی پایه ای که نتوان شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سمند عرصه اندیشه ام گمان نبری</p></div>
<div class="m2"><p>کزین سبب که حرون گشت سست جولان شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن ز پایه خود برتری همی طلبید</p></div>
<div class="m2"><p>چو آستانه طبعم بدید حیران شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهان چشمه بینباشتم ز بی میلی</p></div>
<div class="m2"><p>دمی که دست زد خضر آب حیوان شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود چکیده مغز خرد جواهر من</p></div>
<div class="m2"><p>نه قطره ای که حرج در گلوی نیسان شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طلوع اول کیفیت کمال من است</p></div>
<div class="m2"><p>سخن که نشئه ترکیب چار ارکان شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بیم دقت و اصلاح مفسدان سخن</p></div>
<div class="m2"><p>هزار گو هر معنی نصیب نسیان شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گفت وگوی کسان خاطرم چنان بگرفت</p></div>
<div class="m2"><p>که لفظ بر قد معنی طلسم و زندان شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گلست خاطر معنی پذیر من گویی</p></div>
<div class="m2"><p>که تا وزید نسیمی برو پریشان شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به گرم و سرد تموز و خزان نمی سازم</p></div>
<div class="m2"><p>که سینه تا به لب از معنیم گلستان شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زبان بخایم و نظمی نیاورم به زبان</p></div>
<div class="m2"><p>که پیش نظم کسان بایدم پشیمان شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کند نزول ز معراج خاطرم سخنی</p></div>
<div class="m2"><p>که ناسخ همه گفتارها چو قرآن شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هزار شکر که هرگز نبوده بر خوانم</p></div>
<div class="m2"><p>نواله ای که خجل بایدم ز مهمان شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به وقت دعوت افطار مریم نطقم</p></div>
<div class="m2"><p>هزار معجز عیسی طفیلی خوان شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دهد بلندی فطرت به کار دشواری</p></div>
<div class="m2"><p>که سهل داند هر مشکلی که آسان شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به طعنه چند ز یاران مهربان شنوم</p></div>
<div class="m2"><p>که شاخ سبز نگردید و باغ ویران شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در آن بهار ز باغم چه طرف بربستم</p></div>
<div class="m2"><p>که باردار درختان و میوه الوان شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شراب من که به جام و سبو نمی گنجد</p></div>
<div class="m2"><p>بریزم و نفروشم که سخت ارزان شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در خزینه خاطر به غیر نگشایم</p></div>
<div class="m2"><p>که وقت همت صاحب ذخیره کان شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کند صدف بغل گوش تا گریبان پر</p></div>
<div class="m2"><p>که در نثار لب از نام خان خانان شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زهی سحاب بنانی که دفتر فرمان</p></div>
<div class="m2"><p>ز رشحه قلمت چشمه سار حیوان شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو را جهان به نشان وکالت ارزانی</p></div>
<div class="m2"><p>ستاره ایست نگینت که قطب دوران شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز شوق نام تو از صفحه بگذرد تحریر</p></div>
<div class="m2"><p>ز خاتم تو نشان تا قفای فرمان شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ته مثال به نام تو تا مزین گشت</p></div>
<div class="m2"><p>ز فخر نام تو عنوان نامه پایان شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو آسمانی و هر رتبه فرع رتبه تست</p></div>
<div class="m2"><p>به خود بناز که کیوان هم از تو کیوان شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه شد که خامه به دستت گرفت جای سنان؟</p></div>
<div class="m2"><p>عصا به دست شبان چوب بود ثعبان شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صلاح کار نکو شد سپهر ذات تو را</p></div>
<div class="m2"><p>که از ملمع کوتاه ملک عریان شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که زیب مملکت خویش در تو می پوشد</p></div>
<div class="m2"><p>سری که دایره عالمش گریبان شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تواضعیست تو را ملک خواستن ورنه</p></div>
<div class="m2"><p>به حکمت این فلک پایدار گردان شد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز سهم خنجر گوهر نثار هندی تست</p></div>
<div class="m2"><p>که غرق خون جگر کوه در بدخشان شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز بیم نعل شرربار رخش سرکش تست</p></div>
<div class="m2"><p>که شعله در جگر سنگ خاره پنهان شد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کجا که مرکب عزم تو رو به فتح آورد</p></div>
<div class="m2"><p>هزار سد سکندر غبار میدان شد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تبارک الله ازان بادپای عالم گرد</p></div>
<div class="m2"><p>که زین او به مثل مسند سلیمان شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فلک ز حمله او چون زمین به پشت افتاد</p></div>
<div class="m2"><p>زمین ز شیهه او چون سپهر گردان شد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گهی که تیز شد از باد حمله آتش او</p></div>
<div class="m2"><p>برون ز چار جدار چهار ارکان شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز چابکی سوی مقصد بدان شتاب رسید</p></div>
<div class="m2"><p>که منزلی پس از آن سایه اش نمایان شد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خیال شب به سخای تو ماجرایی داشت</p></div>
<div class="m2"><p>که کار من ز تو خواهد چگونه سامان شد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز شرح جود تو گفتم رقم کنم سخنی</p></div>
<div class="m2"><p>اناملم به سر صفحه گوهر افشان شد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>قیاس حوصله با رشحه گفتگو کردم</p></div>
<div class="m2"><p>درون خانه موری هزار طوفان شد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سحاب بخششت اندر ضمیر من بگذشت</p></div>
<div class="m2"><p>لبالب از در سیراب بحر عمان شد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کنون به کاوش لطف تو حاجتست مرا</p></div>
<div class="m2"><p>وگرنه خاطر من بحر و باطنم کان شد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به داده کرم از من نظر دریغ مدار</p></div>
<div class="m2"><p>گر احتیاج نماند آرزو فراوان شد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خجل ز بخشش و الطاف گشته ام اما</p></div>
<div class="m2"><p>چه سازم از تو تسلی به هیچ نتوان شد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همیشه فیض رسان با همچو ابر بهار</p></div>
<div class="m2"><p>که بر گل تو «نظیری » هزاردستان شد</p></div></div>