---
title: >-
    شمارهٔ ۲۳ - این قصیده درمدح ابوالمظفر جلال الدین اکبرپادشاه درحین سورفرزند همایون اونورالدین محمدشاه سلیم گفته شده
---
# شمارهٔ ۲۳ - این قصیده درمدح ابوالمظفر جلال الدین اکبرپادشاه درحین سورفرزند همایون اونورالدین محمدشاه سلیم گفته شده

<div class="b" id="bn1"><div class="m1"><p>زمانه را پی تزیین سور شاه امسال</p></div>
<div class="m2"><p>بهار پیش ز نوروز کرد استقبال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان ز شادی آیین خویشتن دارد</p></div>
<div class="m2"><p>چو آفتاب دهانی ز خنده مالامال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشیده ماه جلالی به طالع فیروز</p></div>
<div class="m2"><p>فراز چتر سپهری سرادقات جلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به فر و شان همایون نشست بر مسند</p></div>
<div class="m2"><p>به قدرت ملک العرش ایزد متعال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز فوج فوج ظریفان و لون لون لباس</p></div>
<div class="m2"><p>جهان به جلوه درآمد به احسن الاشکال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سریر گشت مزین به صد حلی و حلل</p></div>
<div class="m2"><p>ملک نشست مرتب به صد قبول و جمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز عکس مشعل و شمع حریم مجلس او</p></div>
<div class="m2"><p>سپهر گشت پر از شکل بدر و نقش هلال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه با غلوی تمنائیش هراس جدل</p></div>
<div class="m2"><p>نه در هجوم تماشائیش غبار ملال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی که باده درو خورد مست شد دایم</p></div>
<div class="m2"><p>کسی که عیش درو کرد شاد شد به نوال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عبید مجمره گردان او شمیم بهشت</p></div>
<div class="m2"><p>عقیق مروحه جنبان او نسیم شمال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درو ز غیرت بر باد داده جم مسند</p></div>
<div class="m2"><p>درو ز حسرت در خاک کرده قارون مال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همین که دیده به نظاره اش مقید شد</p></div>
<div class="m2"><p>ازو به سعی بدر بردن دلست محال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود به مرتبه ای دلپذیر زینت او</p></div>
<div class="m2"><p>کزو چو دور روی سایه ناید از دنبال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خیال هست جهان را که بر پرد از شوق</p></div>
<div class="m2"><p>که کرده اند به آیین و زینتش پر و بال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خجسته مجلس سوری که رفته حورالعین</p></div>
<div class="m2"><p>ز صحن او به سر زلف خویش گرد ملال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به تحفه از بر مشاطگان او هر دم</p></div>
<div class="m2"><p>برند غالیه حوران برای زیب جمال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کند نثار شب سورش آنقدر شادی</p></div>
<div class="m2"><p>که شادمانی نوروز را کند پامال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شبش چنان به لطافت که دیده اعمی</p></div>
<div class="m2"><p>ز جیب شام ببیند جمال صبح وصال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شبی به نور و صفا آنچنان که بر رخ او</p></div>
<div class="m2"><p>به جای مردمک دیده بود نقطه خال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شبی چنین که شنیدی و سور شاه درو</p></div>
<div class="m2"><p>شده طلایه عشرت چو روز اول سال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دو در یک دل هم درج شاهزاده سلیم</p></div>
<div class="m2"><p>دو صبح بود به خورشید او نموده جمال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در اتفاق قدم بر قدم چو فتح و ظفر</p></div>
<div class="m2"><p>ز اتحاد عنان بر عنان چو جاه و جلال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به فعل گشته مقارن چو با بیان معنی</p></div>
<div class="m2"><p>به نطق گشته موافق چو با جواب سئوال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر در آینه با هم جمال بنمایند</p></div>
<div class="m2"><p>ز اتحاد سزد گر یکی شود تمثال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به عهد دوستی این برادران شاید</p></div>
<div class="m2"><p>که دیگر از رحم آیند توامان اطفال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنان موافق هم کز یکی چو یاد کنی</p></div>
<div class="m2"><p>نمی شود که نیاید یکی دگر به خیال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به قدر جمله بزرگند اگر چه هست به عمر</p></div>
<div class="m2"><p>تفاوتی چو شب قدر و غره شوال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وجود این سه گهر با وجود حضرت شاه</p></div>
<div class="m2"><p>چو چار عنصر تن دهر را مقوی حال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مقارع فلکی را سلوک شه قانون</p></div>
<div class="m2"><p>مدارج ملکی را رسوم شه منوال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مغنیان حریمش به زخمه ناخن</p></div>
<div class="m2"><p>ز صحن سینه گشایند چشمه های زلال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو دست چنگی او بر دود به رشته چنگ</p></div>
<div class="m2"><p>به رقص جان و دل اندر زمین کشند اذیال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پیاله ای ز شراب مروقش بر کف</p></div>
<div class="m2"><p>که سوی نکهت او روح کرده استقبال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به جامش از دهن بط اگر نریزی زود</p></div>
<div class="m2"><p>گلو ز مشک شود بسته اش چو ناف غزال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فروغ ساغر او گر به آفتاب افتد</p></div>
<div class="m2"><p>ز ارتفاع دگر رو نیاورد به زوال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وگر هوای خمش به دماغ پشه خورد</p></div>
<div class="m2"><p>به رنگ شهپر طاووس گرددش پر و بال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پی ار برند به کحل الجواهر خم او</p></div>
<div class="m2"><p>رسد به قیمت یاقوت هر شکسته سفال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قمر به نسبت جامش پر و تهی گردد</p></div>
<div class="m2"><p>که گاه بدر نماید به چشم و گاه هلال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>میی که آدم از انگور او اگر خوردی</p></div>
<div class="m2"><p>تمام دیو نهادان شدی فرشته خصال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>میی چنین و چو خمخانه انجمن در جوش</p></div>
<div class="m2"><p>ز بانگ نغمه قوال و خنده هزال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شه از سریر خرامان شده به سوی سریر</p></div>
<div class="m2"><p>مثال ابر بهاری روانه بر اطلال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جلال دین و دول شاه اکبر غازی</p></div>
<div class="m2"><p>خلیفه به سزا داور خجسته خصال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خضر دلی که به سرچشمه ولایت او</p></div>
<div class="m2"><p>قضا به آب بقا شسته دفتر آجال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر نه ضامن ارزاق لطف او گردد</p></div>
<div class="m2"><p>به گرد جسم نگردند در رحم اشکال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هرآنگهی که سوی آسمان نیاز برد</p></div>
<div class="m2"><p>ز آب چشمه خورشید دیده مالامال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز یمن دعوت او آسمان شود مفتوح</p></div>
<div class="m2"><p>ز روی فرخ او آفتاب گیرد فال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو سر به سجده نهد ز آسمان ندا آید</p></div>
<div class="m2"><p>بر آفتاب پرستان وبال نیست وبال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز عون دعوت و افسون او عجب نبود</p></div>
<div class="m2"><p>که آفتاب شود ایمن از کسوف و زوال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>رسوم دولت او را قوام تا حدی</p></div>
<div class="m2"><p>که بر مذاهب و ادیان کشد خط ابطال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>حدیث و وحی چنان از بلاغتش منسوخ</p></div>
<div class="m2"><p>که بر کلام عرب کس نمی نویسد قال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به دین او که به آفاق صلح کل دارد</p></div>
<div class="m2"><p>به جز ستم که حرامست هرچه هست حلال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زهی به رتبت کسری و قدر اسکندر</p></div>
<div class="m2"><p>به فر دولت تو خلق رسته اوز اهوال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به بذل و رفق تو آسوده تن صغیر و کبیر</p></div>
<div class="m2"><p>تو کدخدای جهانی جهن تراست عیال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سری که دعوی گردنکشی کند با تو</p></div>
<div class="m2"><p>برآورد ز گریبان او اجل چنگال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر خلاف رضای تو شاخ میوه دهد</p></div>
<div class="m2"><p>به خویشتن زند آتش ز زننگ خویش نهال</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اگر درازی عمر عدو رقم سازی</p></div>
<div class="m2"><p>ز روی صفحه نماید الف به صورت دال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نعوذ بالله ازان فیل دیو هیکل تو</p></div>
<div class="m2"><p>که آسمانش سراسیمه گردد از دنبال</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به فرق کوه و به خرطوم کوه و بینی کوه</p></div>
<div class="m2"><p>بهر دو ناب از آن که روان دو رود زلال</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گهی که حمله کند از صلابت بدنش</p></div>
<div class="m2"><p>سرش عمود به کف بر میان زند از بال</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گران رکاب بماندن، سبک عنان بشدن</p></div>
<div class="m2"><p>به وزن کوه ولی در شتاب یک مثقال</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به دشت و کوه چرد بیشه ها کند میدان</p></div>
<div class="m2"><p>به شهر و کوی چمد خانه ها کند اطلال</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به شارعی که نمایان شود ز هیبت او</p></div>
<div class="m2"><p>بیفکنند ز ارحام مادران اطفال</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو بردوند به کوه و کمر ز حمله او</p></div>
<div class="m2"><p>ز بیم جان فتد اقطاب بر سر ابدال</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نهند سلسله کاینات بر پایش</p></div>
<div class="m2"><p>چنان کشد که عروسان سرو قد خلخال</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چکان ز شورش مغزش به کاکلش مستی</p></div>
<div class="m2"><p>چو رشحه ای که به سنبل چکد ز فرق نهال</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گه قرار و سکون چون عدالت مهدی</p></div>
<div class="m2"><p>گه خروش و تردد چو فتنه دجال</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به بیشه شعله فتد در رود به گاه نبرد</p></div>
<div class="m2"><p>ز کشته پشته شود بر دود چو روز قتال</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چنان که بخت جوان ملک به حکم ملک</p></div>
<div class="m2"><p>کجک به تارک او چون به فرق دولت دال</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به غیر اسب تو کان بردود به حمله او</p></div>
<div class="m2"><p>به گاه شورش او می شود جهان پامال</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تبارک الله ازان توسن پری زادت</p></div>
<div class="m2"><p>که مانیش نتواند کشید شبه و مثال</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز پا فتاده به دولت رسد ز طلعت او</p></div>
<div class="m2"><p>ز ماه غره او ماه عید گیرد فال</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به مشکلات منجم رسد که درگه دور</p></div>
<div class="m2"><p>مدار و مرکز و ادوار را کشد اشکال</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زمین چو قطره سیماب از سمش لرزان</p></div>
<div class="m2"><p>به گاه جستن اندر قوایمش زلزال</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سخن بربط نیاید به گاه تعریفش</p></div>
<div class="m2"><p>که از جلادت او می شود گسسته مقال</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>زنند چنگ اگر ناقصان به فتراکش</p></div>
<div class="m2"><p>صبی رسد به بلوغ و فنی رسد به کمال</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز حادثات زمانت امان تواند داد</p></div>
<div class="m2"><p>درو نمی رسد از چابکی فنا و زوال</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>کنی قیاس زمان زودتر رود ز قیاس</p></div>
<div class="m2"><p>کنی خیال مکان پیش تر رسد ز خیال</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>عنان سوی ازلش از ابد بگردانی</p></div>
<div class="m2"><p>زمان ماضی آید به جای استقبال</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>سپهروار سر دیو را به عرصه خاک</p></div>
<div class="m2"><p>درافکند خم فتراکش از شهاب دوال</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو تیر غیب زند ز آسمان گذار کند</p></div>
<div class="m2"><p>مگر خدنگ تو از نعل او نموده نصال</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به نزد قوس کیانی تو محل نبرد</p></div>
<div class="m2"><p>کمان گروهه نماید کمان رستم زال</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ز حدت آهن شمشیر برق پیکر تو</p></div>
<div class="m2"><p>ز کان برآمد و مشهور شد به تیغ جبال</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ز رعد تازی تو نفخ صور تابد روی</p></div>
<div class="m2"><p>ز برق هندی تو تیغ کوه دزدد یال</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>قیامتست قیامت گهی که از سر کین</p></div>
<div class="m2"><p>به رزم معرکه آرا شوی به قصد جدال</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ره برون شدن از قید تن نیابد روح</p></div>
<div class="m2"><p>ز بس که معرکه گردد ز کشته مالامال</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>اجل ز بدگهران خاک را فرو بیزد</p></div>
<div class="m2"><p>تن زمین شود از زخم تیره چون غربال</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ز بس که تفرقه افتد به پیکر اعدا</p></div>
<div class="m2"><p>ز ضربت دم شمشیر و خنجر قتال</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>جدا سر از تن و تن از قدم رود هر سو</p></div>
<div class="m2"><p>که مرگ را نشود بهر قبض روح مجال</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>من آن زمان به رکاب تو از نهایت شوق</p></div>
<div class="m2"><p>ادا کنم دو سه بیتی ز بینوایی حال</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>فلک ز آه کنم همچو صفحه تقویم</p></div>
<div class="m2"><p>زمین از اشک کنم همچو تخته رمال</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به مدحت تو کنم بکریان فریدون فر</p></div>
<div class="m2"><p>ز دولت تو کنم عنیان همایون فال</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>برم فروغ صد الهام را به کذب مباح</p></div>
<div class="m2"><p>کنم حرام صد اعجاز را به سحر حلال</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ره سخن ز تمیز تو آن چنان بندم</p></div>
<div class="m2"><p>که در برابر قولم کسی نگوید قال</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>فغان ز مدعی باد سنج بیهده دو</p></div>
<div class="m2"><p>به دشت و در چو سراسیمه نافه بچه ضال</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>گهی که دیده در آژنگ ابرویش بیند</p></div>
<div class="m2"><p>سنان و تیغ کند بر ضمیر استقلال</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ز قول تیره او راه فهم من تاریک</p></div>
<div class="m2"><p>ز نظم نازک من طبع صلب او صلصال</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به مبحثی که رسد نکته ای نسازد قطع</p></div>
<div class="m2"><p>اگر چه دعوی قاطع کند به فضل و کمال</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>گرسنه قهرتر و تیزخوی تر گردد</p></div>
<div class="m2"><p>درو چو آتش چندان که بیش ریزی مال</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>دعا و مدح برو چون بر آب هیزم خس</p></div>
<div class="m2"><p>هجا و فحش بر او چون بر آتش آب زلال</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>به حسن لطف تو سخن از بغل برون آرم</p></div>
<div class="m2"><p>نهان کنم چو رسد از جفاش در دنبال</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>کلام حق اگر از جیب خود برون آرم</p></div>
<div class="m2"><p>درآن کشد ز نفاق درون خط ابطال</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>سخن ز دیدن او در جهد به خاطر من</p></div>
<div class="m2"><p>چو گربه های ز باد پلنگ در آغال</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چو در برابر نطقم دکان فروچیند</p></div>
<div class="m2"><p>خزف نهد به ترازو گهر برد به جوال</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>از آن ذخیره کز اکسیر خاطرم ببرد</p></div>
<div class="m2"><p>خمیرمایه صد من زرست یک مثقال</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چو در ریاض معانی روش کند فکرم</p></div>
<div class="m2"><p>جهد ز شاخچه طبع من هزار نهال</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ولی ز هیبت این دی تمیز سرد شنو</p></div>
<div class="m2"><p>سخن شود به گلو چون به نای کلکم نال</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>جماعتی ز سفیهان تیره طبع دنی</p></div>
<div class="m2"><p>مدام در پیش افتاده اند همچو وبال</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>همه چو فکر خطا رخ نموده سوی سفر</p></div>
<div class="m2"><p>یکی نبرده به جا راه چون خیال محال</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>ز بی تمیزی این ناقدان کم مایه</p></div>
<div class="m2"><p>گهر به قدر خزف گشته زر به نرخ سفال</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>شدست مطلع اشعارم از ملالت طبع</p></div>
<div class="m2"><p>ستاره سوخته چون ماه منخسف احوال</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چو کهربا شده در طبع من جواهر نظم</p></div>
<div class="m2"><p>که پیش کس نتوان کرد ازین مقوله مقال</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>سزد که اختر نظم ما به یک ساعت</p></div>
<div class="m2"><p>توجه تو برون آرد از هبوط و وبال</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>برم به مدح تو دیوان شعر بر گردون</p></div>
<div class="m2"><p>اگر مراد «نظیری » دهی به استعجال</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>همیشه تا چو گل از تندباد حادثه هست</p></div>
<div class="m2"><p>نهال روزی اهل هنر پریشان حال</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>وجود این سه پسر با حیات شه بادا</p></div>
<div class="m2"><p>شکفته تر ز گل و تازه تر ز روی نهال</p></div></div>