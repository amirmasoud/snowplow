---
title: >-
    شمارهٔ ۲۱ - ایضا این قصیده در مدح ابوالمظفر جلال الدین هنگام فتح قلعه اسیر گفته شده
---
# شمارهٔ ۲۱ - ایضا این قصیده در مدح ابوالمظفر جلال الدین هنگام فتح قلعه اسیر گفته شده

<div class="b" id="bn1"><div class="m1"><p>چو رو به برج شرف کرد آفتاب منیر</p></div>
<div class="m2"><p>دمید فاتحه فتح بر حصار اسیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه میر جلالی به فر فروردین</p></div>
<div class="m2"><p>در امن گاه ممالک شد انبساط پذیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز لهو بی خردان عز سلطنت می رفت</p></div>
<div class="m2"><p>نگاهداشت ملک حرمت کلاه و سریر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چها ز رحم نمودند بندگانش آزاد</p></div>
<div class="m2"><p>ملوک زاده ز زندان و گنج از زنجیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پشته ها زر و تل ها درم برآوردند</p></div>
<div class="m2"><p>به کوه زد نظر شاه گوییا اکسیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیان فتح اسیر از قیاس بیرونست</p></div>
<div class="m2"><p>نخست قصه مالی گرت کنم تقریر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نردبان عقول و هواس را بستند</p></div>
<div class="m2"><p>که برشوند به دیوار او پی تسخیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس گرانی اندیشه پایه ها بشکست</p></div>
<div class="m2"><p>قضای رفته بیفتاد بر سر تدبیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نظر به سلسله ممکنات افکندند</p></div>
<div class="m2"><p>جدار قلعه مهین بود و پای مور قصیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمی رسید کمندی هم از سر شب و روز</p></div>
<div class="m2"><p>پی صعود گرفتند طره شبگیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدان جدار دویدند چون هوس به دماغ</p></div>
<div class="m2"><p>در آن حصار خزیدند همچو سر به ضمیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز پخته کوشی هشیار و خام نوشی مست</p></div>
<div class="m2"><p>ز خلق کشته روان بود خون به رنگ عصیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کشید قلعه مالی گر از نهیب فغان</p></div>
<div class="m2"><p>که چیست جنگ و عداوت به این ضعیف صغیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر به دعوی تخت و خزینه آمده اید</p></div>
<div class="m2"><p>صغیر را نگرفتست کس به جرم کبیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز من گرفته به ناحق نعیم دنیا را</p></div>
<div class="m2"><p>کنون عقوبتش افکنده در بلای سعیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز توپ و ضرب زن آتشکده ست مریخش</p></div>
<div class="m2"><p>نشسته بر سر آتشکده چو راهب پیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دلیر بود به خون ریز خصم و می گفتم</p></div>
<div class="m2"><p>که آخرش به عقوبت شوند دامن گیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تنور برج به باروت و نفت تافته بود</p></div>
<div class="m2"><p>خبر نداشت که خاکش به خون کنند خمیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نخست روز که نامش اسیر می کردند</p></div>
<div class="m2"><p>به دل گذشت که این نام می کند تأثیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز شرح حال هم آشفته اند سکانش</p></div>
<div class="m2"><p>که مشرفند درو بر صحیفه تقدیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خدنگ تفرقه از هر طرف به صید آمد</p></div>
<div class="m2"><p>سپه که پره زد و در حصار شد نخبیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو این نوید شنیدند پردلان دادند</p></div>
<div class="m2"><p>عنان به جودت عقل و جلادت تدبیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به یک اشاره عدو را به حضرت آوردند</p></div>
<div class="m2"><p>چنانکه موی نجنبید بر مشار و مشیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به بارگاه خلافت سر سجودآورد</p></div>
<div class="m2"><p>به رخ غبار خطا بر جبین خوی تقصیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به عجز گفت که اینک من و نگین و کلاه</p></div>
<div class="m2"><p>به رمز گفت که این قلعه و قلیل و کثیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در آن مقام که آید به جزر و مد دریا</p></div>
<div class="m2"><p>هنر چه مایه تواند نمود موج غدیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به قابلیت او شاه دید و خندان گفت</p></div>
<div class="m2"><p>که ای مشام بزرگی و سلطنت را سیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو تنگ حوصله و ملک را نواله بزرگ</p></div>
<div class="m2"><p>نه در خور سر منقار تست این انجیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به کوزه بوم و بر ملک سبز نتوان داشت</p></div>
<div class="m2"><p>گذار گلشن و صحرا به بحر و ابر مطیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بشو به آب شفاعت دل اسیران را</p></div>
<div class="m2"><p>ز لوث ذلتشان ده حصار را تطهیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زمین حکم ببوسید و بی درنگ نوشت</p></div>
<div class="m2"><p>به آن گروه که اینست امر و نیست گزیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو این پیام سوی حارسان قلعه رسید</p></div>
<div class="m2"><p>ز فهم کج همه رفتند در غریو و نفیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه به کار خروشان چو مرغ بی هنگام</p></div>
<div class="m2"><p>ولیک مات چو شطرنجیان بی تدبیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو بخت بد کند از خانه دور صاحب را</p></div>
<div class="m2"><p>ز پی به نوحه کشد سگ فغان و مرغ صفیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پس از همه غرض و خواست یافتند امان</p></div>
<div class="m2"><p>به ملک و مال و رعیت به نام و ننگ امیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگرچه رحمت شه پیر را جوان می کرد</p></div>
<div class="m2"><p>جوان ز هول بیابان همی رسیدی پیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نمود کشوری از عالم مثال نشان</p></div>
<div class="m2"><p>ز جان اثر نه و بازار و خانه پر تصویر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بزیر باریکی مانده بود تنگ نفس</p></div>
<div class="m2"><p>ز حمل مال یکی گشته بود شادی میر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز بس که گشت گران اجرت کشیدن مال</p></div>
<div class="m2"><p>گدای شهر غنی گشت و مالدار فقیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همه خراب ز کردار خویش و شه به فسون</p></div>
<div class="m2"><p>که چون کند دل ویران این همه تعمیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همه ز اختر خود در وبال و داور خلق</p></div>
<div class="m2"><p>چو آفتاب بر اوج شرف نهاده سریر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خلیفه به سزا شاه اکبر غازی</p></div>
<div class="m2"><p>بصیر غیب نظر مالک فرشته دبیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر آن مثال که طغراش نام او نبود</p></div>
<div class="m2"><p>درست نیست بسان نماز بی تکبیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کنون به پشت کند مرغ بر هوا پرواز</p></div>
<div class="m2"><p>که پادشاه سلیمان و آصف است وزیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زهی به سلطنت و عدل بی عدیل و مثال</p></div>
<div class="m2"><p>خهی به مکرمت و رحم بی شبیه و نظیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>محبت تو در اجزای آفرینش دهر</p></div>
<div class="m2"><p>چو روغنست نهان گشته در طبیعت شیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چگونه مهر تو بیرون رود ز آب و گلی</p></div>
<div class="m2"><p>که کرده است چهل سال رحمتش تخمیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تو داد معدلت و رحم داده ای به کمال</p></div>
<div class="m2"><p>به کار دولت تو کس نمی کند تقصیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز ذوق بوالعجبی های نقش قدرت تو</p></div>
<div class="m2"><p>دمی نمی نهد از دست خامه را تقدیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>قضا تو را ز پی کارنامه می دارد</p></div>
<div class="m2"><p>که همچو نقش تو دیگر نمی شود تصویر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>لباس مفلسی از فربهی نعمت تو</p></div>
<div class="m2"><p>چنان پرست که سوزن نمی رود به حریر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ابا نمودن طبع تو از خیال فساد</p></div>
<div class="m2"><p>برون ز قالب شیطان کشید نفس شریر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جهان به حرص و هوا هر بنا که طرح انداخت</p></div>
<div class="m2"><p>کند خراب که تو خوب می کنی تعمیر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>قضا نطق نزند هر کجا که فرمان را</p></div>
<div class="m2"><p>به عزل ونصب تو باشد تصرف و تغییر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو اصل راحت و آسایشی که عالم را</p></div>
<div class="m2"><p>ز هرچه هست گزیرست و از تو نیست گزیر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خیال بد نکند کس که در ره دل و گوش</p></div>
<div class="m2"><p>نشانده حزم تو در هر قدم هزار خبیر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>جهان ستان ملکا، شه نشان خداوندا</p></div>
<div class="m2"><p>که عالمست ز امن تو بر فراش حریر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز هرچه در همه ملکست از تو می خواهم</p></div>
<div class="m2"><p>دهی و کلبه امنی و یک دو پار حصیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ازین گذشته سر جرأتی دگر دارم</p></div>
<div class="m2"><p>که وقت فرصت خاصان فتاده در تأخیر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>من و رفیقی ز ابنای من ز ملک عراق</p></div>
<div class="m2"><p>به گرم و سرد تموز و خزان شدیم مسیر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دو مرغ بودیم آورده سوی هند پناه</p></div>
<div class="m2"><p>ز کید مشتری و دام ماه و آفت تیر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>قضای بد سوی کشمیرش از هوا انداخت</p></div>
<div class="m2"><p>مگر کشید در آن بوم بی مقام صفیر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>اسیر بند تو گردید و خلق می گویند</p></div>
<div class="m2"><p>به عندلیب چمن درخورست نی زنجیر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به نیکی و به بدی از ازل قلم رفتست</p></div>
<div class="m2"><p>ببخش جرم غنی را به التماس فقیر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گرسنه است به دریوزه شفاعت من</p></div>
<div class="m2"><p>خطای نظم من و جرم او شهابپذیر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز عرض حال «نظیری » نگاه عفو مپوش</p></div>
<div class="m2"><p>که همچو لطف تواش نیست در زمانه نظیر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چه دست ریس سزای تو روزگار آرد</p></div>
<div class="m2"><p>که رشته ای به سر دوک می تند بر خیر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همیشه تا به مدارا و رفق نیکویان</p></div>
<div class="m2"><p>دل رمیده دشمن کنند صید و اسیر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>جمال دولت تو دلفریب صیادی</p></div>
<div class="m2"><p>که هر که چشم بدوزد برو به خیر اخیر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سرش به حلقه امید بند گردانند</p></div>
<div class="m2"><p>عطا و لطف تو آن آهوان آهوگیر</p></div></div>