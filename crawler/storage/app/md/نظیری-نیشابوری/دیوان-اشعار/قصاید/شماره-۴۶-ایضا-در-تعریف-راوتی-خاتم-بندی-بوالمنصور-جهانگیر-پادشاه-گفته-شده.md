---
title: >-
    شمارهٔ ۴۶ - ایضا در تعریف راوتی خاتم بندی بوالمنصور جهانگیر پادشاه گفته شده
---
# شمارهٔ ۴۶ - ایضا در تعریف راوتی خاتم بندی بوالمنصور جهانگیر پادشاه گفته شده

<div class="b" id="bn1"><div class="m1"><p>بهشت شاه نشین بین که دل نشین بینی</p></div>
<div class="m2"><p>بدایع رقم صورت آفرین بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار مانی بر چوب بسته یابی دست</p></div>
<div class="m2"><p>که رشگ صنعت چینی و نقش چین بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درو نجوم مصور به نقش پیرایی</p></div>
<div class="m2"><p>نظیر کارگه چرخ هفتمین بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زند چو سقف فلک بر هواش گوهر موج</p></div>
<div class="m2"><p>ز بس که در صدفش گوهر ثمین بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدست عالمی از عاج و آبنوس بنا</p></div>
<div class="m2"><p>که چون به صنعتش از چشم خرده بین بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ستاره دوخته بر سقف آسمان یابی</p></div>
<div class="m2"><p>بنفشه ریخته بر سطح یاسمین بینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مثل به انجم و چرخش زدن به آن ماند</p></div>
<div class="m2"><p>که موج کوثر چون موج بارگین بینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صدف چسان ز درون اینقدر فکنده برون</p></div>
<div class="m2"><p>اگر نه مایه دریا درو دفین بینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه از صنایع خلقت اگر نگارینش</p></div>
<div class="m2"><p>ز حرف و نقطه سیمین و عنبرین بینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دقت نکتش طوطیان گویا را</p></div>
<div class="m2"><p>فرو شده سر منقار دانه چین بینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر که خط نگارین و روی معشوق است</p></div>
<div class="m2"><p>که خوش نما پر مورش بر انگبین بینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگو که پرده ز تصویر او براندازند</p></div>
<div class="m2"><p>که عاشقی ایاز و سبکتکین بینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هزار خسرو شیرین نشسته از هر سو</p></div>
<div class="m2"><p>اسیر سلسله زلف عنبرین بینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر به چشم بصارت درو نگاه کنی</p></div>
<div class="m2"><p>دلی پر از سر مژگان حور عین بینی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملک به سجده گهش پا به ترس می بنهد</p></div>
<div class="m2"><p>ز بس که بر سر هم ریخته جبین بینی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر او گر از دم روح الامین نشیند گرد</p></div>
<div class="m2"><p>به دست مریم جاروب از آستین بینی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور اطلس فلکش از بساط برچینند</p></div>
<div class="m2"><p>ز شهپر ملکش فرش بر زمین بینی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز لوحه های کلیمش اگر نساخته اند</p></div>
<div class="m2"><p>مبطنش ز چه چون مصحف مبین بینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وگر تضمن معنی نکرده تصویرش</p></div>
<div class="m2"><p>چرا مبطنش از نام چون نگین بینی؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز جنت آمده است این وثاق تو گویی</p></div>
<div class="m2"><p>وگر نه خانه دنیا ز آب و طین بینی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه بر نواحی او رهگذار غم یابی</p></div>
<div class="m2"><p>نه بر حواشی او خاطر حزین بینی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هر کجا گذری عیش در خفا یابی</p></div>
<div class="m2"><p>به هر طرف گری ذوق در کمین بینی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگر سکینه موسی است نو پدید شده</p></div>
<div class="m2"><p>که با سعادت و فیروزیش قرین بینی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به خوابگه زنیش پاسبان شه یابی</p></div>
<div class="m2"><p>به رزمگه بریش کار ساز دین بینی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مگو ز عرش سلیمان مرکبش کو را</p></div>
<div class="m2"><p>گهی فراز هواگاه بر زمین بینی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فراز فیل صلب پوش اساس شاه نگر</p></div>
<div class="m2"><p>که عرش سیمین بر کوه آهنین بینی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز عرش و فیل شه هندگونه مسند جم</p></div>
<div class="m2"><p>که زیر قایمه اش دیو در کمین بینی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به این نشاطگه عاج و آبنوس نگر</p></div>
<div class="m2"><p>که دست مایه احسنت و آفرین بینی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بود سپهر دگر کش لیالی و ایام</p></div>
<div class="m2"><p>چو رومی و حبشی داغ بر جبین بینی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فروغ شمسه او شعله در زند به افق</p></div>
<div class="m2"><p>گر از سپهر دل شام او غمین بینی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کسی تواند ابداع این چنین کردن</p></div>
<div class="m2"><p>که بر خزانه بحر و برش امین بینی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مگر نمونه ای از فکرت شهنشاهست</p></div>
<div class="m2"><p>که هرچه بینی پا تا سرش گزین بینی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بلی ولایت عالم به زیب و فر گردد</p></div>
<div class="m2"><p>دمی مقیدش از فکر خرده بین بینی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همه اساس به دیدار شاه زیبنده است</p></div>
<div class="m2"><p>چو نازنین نگری جمله نازنین بینی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جمال حور و می خلد در نظر باشد</p></div>
<div class="m2"><p>چو او به بزم درآید نه آن نه این بینی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه از بشاشت او فکر درد و غم دانی</p></div>
<div class="m2"><p>نه از نزاهت او رنگ کبر و کین بینی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جمال شاه جهانگیر و زیب این محفل</p></div>
<div class="m2"><p>صفای رضوان در جنت برین بینی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شهی که از پی نقل اساسه منزل</p></div>
<div class="m2"><p>هیون نه فلکش داغ بر سرین بینی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سخی دلی که بروز صلای مجلس او</p></div>
<div class="m2"><p>سحاب را عرق از شرم بر جبین بینی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مگو که عرش برین عکس بر زمان انداخت</p></div>
<div class="m2"><p>که سایه ملک العرش بر زمین بینی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز عدل او همه اموات ذی حیات شدند</p></div>
<div class="m2"><p>به خاک بین که بر او روح را امین بینی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مدبری که اولوالامری و خلافت را</p></div>
<div class="m2"><p>به شأن دولت او آیت مبین بینی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کسی که مجلس کیخسروی بیاراید</p></div>
<div class="m2"><p>صدش چو رستم زال آستان نشین بینی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو رای خلوت صاحب قرانیش افتد</p></div>
<div class="m2"><p>ندیم حاجب از تغرل و تکین بینی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جهان ستانی شاها به نام تو ختم است</p></div>
<div class="m2"><p>چنان که خاتم زر کامل از نگین بینی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به یاد بزم تو سودایی تنک فکریست</p></div>
<div class="m2"><p>فلک که مغز سرش خشک از طنین بینی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نظیریم که به کفر و به دین میسر نیست</p></div>
<div class="m2"><p>که ساحری چو من اعجازآفرین بینی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر به مدحت و فرقت رضا شدم بپذیر</p></div>
<div class="m2"><p>که گر ببینیم از درد پا انین بینی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرا به خلعت صورت سزد که بنوازی</p></div>
<div class="m2"><p>چنین که در رحم دولتم جنین بینی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همیشه تا که سپهر منقش ایوان را</p></div>
<div class="m2"><p>به دیده بانی این ممکن حصین بینی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>درو به عیش بمان در لیالی و ایام</p></div>
<div class="m2"><p>که در سنینش مه و مهر در سنین بینی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو را نشیمن عشرت به رفعتی بادا</p></div>
<div class="m2"><p>که پایه نهم چرخش اولین بینی</p></div></div>