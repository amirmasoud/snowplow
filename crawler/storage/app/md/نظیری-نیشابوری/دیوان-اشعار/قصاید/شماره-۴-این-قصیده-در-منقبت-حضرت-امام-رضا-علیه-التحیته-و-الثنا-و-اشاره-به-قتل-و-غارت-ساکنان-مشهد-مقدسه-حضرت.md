---
title: >-
    شمارهٔ ۴ - این قصیده در منقبت حضرت امام رضا علیه التحیته و الثنا و اشاره به قتل و غارت ساکنان مشهد مقدسه حضرت
---
# شمارهٔ ۴ - این قصیده در منقبت حضرت امام رضا علیه التحیته و الثنا و اشاره به قتل و غارت ساکنان مشهد مقدسه حضرت

<div class="b" id="bn1"><div class="m1"><p>چنان رسیدن دی سرد ساخت دنیی را</p></div>
<div class="m2"><p>که کرد در دل مجنون فسرده لیلی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسیم صبح بدان گونه گشت رنگ ستان</p></div>
<div class="m2"><p>که برد از کف دست نگار حنی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فسردگی هوا تا به غایتی برسید</p></div>
<div class="m2"><p>که بست در دل عاشق ره تمنی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به آن رسید ز تأثیر تندباد خزان</p></div>
<div class="m2"><p>که بار و برگ بریزد درخت طوبی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بیع رضوان مالک اگر شود راضی</p></div>
<div class="m2"><p>خود به گلخن دوزخ بهشت ماوی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فغان که گشت در احیای خلق افسرده</p></div>
<div class="m2"><p>دمی که مایه اعجاز بود عیسی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجوز برد که بر حرف باستان دل داشت</p></div>
<div class="m2"><p>کنون به نطق درآورده است املی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه چشم از دمه دیدست حال شخص تباه</p></div>
<div class="m2"><p>نه گوش درک زنخ بند کرده شکوی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شرح سردی امروز کرده اند حذر</p></div>
<div class="m2"><p>لباس لفظ که پوشیده اند معنی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بیم سرما طفل از رحم نمی زاید</p></div>
<div class="m2"><p>اگرچه وعده زادن گذشته حبلی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشاطه دست عروس از نگار بگشاید</p></div>
<div class="m2"><p>که می برد نفس صبح رنگ حنی را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خماردار نیارد به حلق مینا دست</p></div>
<div class="m2"><p>فراق دیده نبوسد عذار سلمی را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر نه ز آفت سرما درخت ایمن بود</p></div>
<div class="m2"><p>چراست شعله بیضا به دست موسی را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز چله دی و بهمن که بر کمان دارند</p></div>
<div class="m2"><p>عطارد افکند از دست کلک انسی را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز درد مارگزیده خبر نمی یابد</p></div>
<div class="m2"><p>که نیش عقرب بر دست زهر افعی را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز جیب صفحه تصویر چین برآوردم</p></div>
<div class="m2"><p>شکست رنگ به رخساره نقش مانی را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به شهد داده برودت طبیعت کافور</p></div>
<div class="m2"><p>به مشگ داده رطوبت مزاج کسنی را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به دل محبت معشوق ره نمی یابد</p></div>
<div class="m2"><p>که سد راه شود برف عهد قربی را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گذشته برف ز بس از سربنای جهان</p></div>
<div class="m2"><p>نموده منزل قارون رواق کسری را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز برف پنجه خور مانده آنچنان از کار</p></div>
<div class="m2"><p>که در میانه کافور دست موتی را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز برف ساخته استاد زمهریر سفید</p></div>
<div class="m2"><p>چو سقف خانه نداف چرخ اعلی را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز ضعف پرتو خورشید در میان سحاب</p></div>
<div class="m2"><p>بسان نور بصر گشته چشم اعمی را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>میان دوزخ تابان در استخوان مریض</p></div>
<div class="m2"><p>فسردگی هوا لرزه ساخت حمی را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رسید کوتهی روز تا بدان غایت</p></div>
<div class="m2"><p>که جای در دم صبح است شام اضحی را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خدنگ رستم اگر در کمان کشیده شود</p></div>
<div class="m2"><p>هوای سرد ببندد محل مجری را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کنون طیور نسازند در چمن مسکن</p></div>
<div class="m2"><p>زنار همچو سمندر کنند سکنی را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو رفت گل ز چمن عندلیب گوشه گرفت</p></div>
<div class="m2"><p>به زاغ و بوم فکندند امر شوری را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سخن به نصف خلافت رسید و مل نشنید</p></div>
<div class="m2"><p>طلب ز مطرب و شاهد نمود فتوی را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سرود مطرب گفتا به راستی کز ما</p></div>
<div class="m2"><p>کسی به سر نرساند طریق اولی را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صلاح عقل چنانست در چنین فصلی</p></div>
<div class="m2"><p>که از شراب بشویی لباس تقوی را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز باده بر در میخانه ها وضو سازی</p></div>
<div class="m2"><p>به پای ساغر می افکنی مصلی را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از آن شراب کنی در قدح که مرد کند</p></div>
<div class="m2"><p>اگر ز دور خورد بر مشام خنثی را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از آن شراب کنی در قدح که یاد صبا</p></div>
<div class="m2"><p>ز فیض نگهت او روح داد عیسی را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از آن شراب کزو گر خیال جرعه کشد</p></div>
<div class="m2"><p>چو نطفه روح دهد صورت هیولی را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از آن شراب که گر قطره ای به خاک چکید</p></div>
<div class="m2"><p>کند درست عظام رمیم موتی را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هزار کوه غم از یکدگر فرو ریزد</p></div>
<div class="m2"><p>در آن مقام که ظاهر کند تجلی را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه زان شراب که انگور او شهید کند</p></div>
<div class="m2"><p>شه سریر امامت علی موسی را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>امام ثامن ضامن که روز بازپسین</p></div>
<div class="m2"><p>به گوش خوف رساند ندای بشری را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>امام ثامن ضامن که در شریعت حق</p></div>
<div class="m2"><p>ز هفت مفتی صادق گرفته فتوی را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شهید خاک خراسان که گرد مرکب او</p></div>
<div class="m2"><p>به جای نور بصر گشته چشم اعمی را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دیت ستان لب لعل زهر خورده اوست</p></div>
<div class="m2"><p>زمردی که کند کور چشم افعی را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر ز ناف زمین حق بنای کعبه نهاد</p></div>
<div class="m2"><p>زمین مشهد او کرد صدر دنیی را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به هر قدم که نهی در حریم روضه او</p></div>
<div class="m2"><p>نهاده اند اقامت ریاض عقبی را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به ذوق تر ز کلیم اند نقش های گلیم</p></div>
<div class="m2"><p>نموده جبهه هر خشت صد تجلی را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فروغ قبه خورشید شکل مرقد او</p></div>
<div class="m2"><p>نموده بدر فروزنده چشم اعمی را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شعاع نور قنادیل او به هم شکند</p></div>
<div class="m2"><p>طلیعه های کواکب سپهر اعلی را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو حافظان حریمش کشیده اند سرود</p></div>
<div class="m2"><p>نموده اند ترقی عقولی اولی را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز حس جوهر و آواز و فکر تحریرش</p></div>
<div class="m2"><p>نموده اند صور جوهر هیولی را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو عندلیب به گلدسته های مسجد او</p></div>
<div class="m2"><p>مؤذنان مترنم ستانده املی را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز ذکر اشهد ان لا اله الا الله</p></div>
<div class="m2"><p>به گوش خوف رسانیده بانگ بشری را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز شوق طوف مزارش که عید شادی هاست</p></div>
<div class="m2"><p>عروس نیمه شب از دست شسته حنی را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به ماه میل سر گنبدش رجوع کنند</p></div>
<div class="m2"><p>جماعتی که پرستیده اند شعری را</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز زهد عاکف و سیار صحن روضه او</p></div>
<div class="m2"><p>به ناز و منت گیرند من و سلوی را</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به ارتقای مقام نفوس خدامش</p></div>
<div class="m2"><p>نوید ذبح عظیم است کبش قربی را</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>فراخی نعمش با صفا بدل کرده</p></div>
<div class="m2"><p>نزاع کاسه مجنون و سنگ لیلی را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وسیله شرف از عیدگاه او حجاج</p></div>
<div class="m2"><p>به قدس و کعبه فرستند فطر و اضحی را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دو گوهه طرق و مشهد مقدس او</p></div>
<div class="m2"><p>زنند طعنه به تقدیس قدس و رضوی را</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آن دو کوه گران حلم کوه سنگی زاد</p></div>
<div class="m2"><p>که یک نتیجه کبری بود دو صغری را</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زهی امام که مفتاح باب مرحمتست</p></div>
<div class="m2"><p>محبت تو رضای ملک تعالی را</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چنانکه برگ بریزد ز تندباد خزان</p></div>
<div class="m2"><p>محبت تو بریزد گناه کبری را</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تو گر به روز قیامت شفیع خلق شوی</p></div>
<div class="m2"><p>عذاب نیست پرستندگان عزی را</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>پی نتیجه اعمال حاکم محشر</p></div>
<div class="m2"><p>اگر به خلد نویسد برات اجری را</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به نزد مالک دوزخ روان کند رضوان</p></div>
<div class="m2"><p>محبتت نکند گر نشانه مجری را</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو عکس آینه از بطن نطفه بنماید</p></div>
<div class="m2"><p>اگر ز رای تو زیور دهند حبلی را</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به هر گیاه که ابر سخات آب دهد</p></div>
<div class="m2"><p>دهد به قاعده بار درخت طوبی را</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هر آنکه خاک درت را به تاج و تخت دهد</p></div>
<div class="m2"><p>کند معاوضه با تره من و سلوی را</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ز بس عنایت عامت نموده مستغنی</p></div>
<div class="m2"><p>ز حاجتی که به هم بوده اهل دنیا را</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به نزد فهم چنان مدعا شود ظاهر</p></div>
<div class="m2"><p>که احتیاج نیفتد به لفظ معنی را</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به بوسه دیر؟ درت نقش شد کسی بیند</p></div>
<div class="m2"><p>نیاز عرضه کند اشتیاق مانی را</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>غریب از حرکات سپهر مضطرب است</p></div>
<div class="m2"><p>بر آستان تو یابد مگر تسلی را</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تمام گشت به نی پاسخ کلیم از طور</p></div>
<div class="m2"><p>که راند در ارنی بر زبان خود نی را</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز گفتن ارنی زان تو نی نمی شنوی</p></div>
<div class="m2"><p>که از سخاوت بانون نیاوری یی را</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ز لفظ آری با کوه اگر خطاب کنی</p></div>
<div class="m2"><p>جواب نیست جز آری جواب آری را</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شها کسی که به دل جای داده خصم تو را</p></div>
<div class="m2"><p>به کعبه پهلوی مصحف نهادی عزی را</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>جماعتی نه موالی ز خصم آل نبی</p></div>
<div class="m2"><p>همه گرفته به میراث دین خنثی را</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>زدند بیخ و بن مؤمنان به تیغ خلاف</p></div>
<div class="m2"><p>نگاه هیچ نکردند صدق دعوی را</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز خلق سید و اشراف جوی خون راندند</p></div>
<div class="m2"><p>به روضه تو گشادند دست دعوی را</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بقیئه ای که نگشتند کشته از افلاس</p></div>
<div class="m2"><p>فروختند به غربت دیار و مأوی را</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>شها فغان ز زمانی که اهل دانش او</p></div>
<div class="m2"><p>ز فهم شعر ندانند غیر آری را</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>حدیث عیسی و افسانه را یکی دانند</p></div>
<div class="m2"><p>ز نیشکر نشناسند شاخ کسنی را</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>درین زمانه بدان گونه شعر خوار شدست</p></div>
<div class="m2"><p>که ننگ می شود از نام خویش شعری را</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ولی ز پاکی نظمم به یمن مدحت تست</p></div>
<div class="m2"><p>شرف به نظم روان جریر و اعشی را</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>غلام پیر «نظیری » درم خریده تست</p></div>
<div class="m2"><p>توجهی که ببیند جمال مولی را</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>نماز مرده کند بر مدیح مرده دلان</p></div>
<div class="m2"><p>به مدحت تو کند زنده روح اعشی را</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چنان ثنای تو گوید که ذوق جایزه اش</p></div>
<div class="m2"><p>زبان دهد به ته خاک معن و یحیی را</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>کنون که لب به شکایت گشوده ام خواهم</p></div>
<div class="m2"><p>که نوشم از کف تو شربت تسلی را</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چنان ثنای تو گویم که ذوق خواندن آن</p></div>
<div class="m2"><p>دهد زبان بیان نقش های مانی را</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چو خامه را به بنان رخصت جوار دهم</p></div>
<div class="m2"><p>کند به معجزه کار عصای موسی را</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو صفحه را به سر کلک آشنا سازم</p></div>
<div class="m2"><p>کنم بسان دبیر بهار انشی را</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>وگر غبار رهت رابه نوک خامه کنم</p></div>
<div class="m2"><p>کنم چو دیده پر از نور میم املی را</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>وگر امیدی «نظیری » بر تو عرضه کنم</p></div>
<div class="m2"><p>پر از مراد کنی دامن تمنی را</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>مراد دل به تو گفتم دگر تو می دانی</p></div>
<div class="m2"><p>زبان من به دعا ختم کرد دعوی را</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>همیشه تا ز شب و روز امتیازی هست</p></div>
<div class="m2"><p>درین جهان شب یلدا و روز اضحی را</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>صباح عید محبت نیاورد شب غم</p></div>
<div class="m2"><p>شب عدوت نبیند صباح دنیی را</p></div></div>