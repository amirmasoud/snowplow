---
title: >-
    شمارهٔ ۴۴ - یک قصیده
---
# شمارهٔ ۴۴ - یک قصیده

<div class="b" id="bn1"><div class="m1"><p>زنند باغ و بهارم صلای ویرانی</p></div>
<div class="m2"><p>گلم ز شاخ فرو ریزد از پریشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه رنگ و بوی به جا مانده نی بر و برگم</p></div>
<div class="m2"><p>چو نخل بادیه افتاده ام به عریانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سموم وادی غم دیده پای تا فرقم</p></div>
<div class="m2"><p>ز هم بریزم اگر ناگهم بجنبانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جدا ازان شکن طره ام سرانگشتت</p></div>
<div class="m2"><p>گزند خورده دندان صد پشیمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خجل ز مردن خویشم گمان نبود الحق</p></div>
<div class="m2"><p>که بی رخ تو چنین جان دهم به آسانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه شکست دلم همچو کار زندان بان</p></div>
<div class="m2"><p>تمام شکوه حالم چو شغل زندانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمان خسته دلی بی تهورست ار نه</p></div>
<div class="m2"><p>به تیر آه کند لخت سینه پیکانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر دلی به کف آری زیان نخواهی کرد</p></div>
<div class="m2"><p>به این قدر که عنان زین روش بگردانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکست ما چه مصافست ما ضعیفانیم</p></div>
<div class="m2"><p>که مور در صف ما می کند سلیمانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر مطالعه چهره ام کنی نظری</p></div>
<div class="m2"><p>قصورهای ضمیرم همه فروخوانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز کعبه آیم و بت های آزری در جیب</p></div>
<div class="m2"><p>هزار بتکده را در خورم به رهبانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گریزگاه جنونست ورنه هر روزم</p></div>
<div class="m2"><p>هزار کفر برون آرد از مسلمانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نگاهدار حجابست ورنه از نگهی</p></div>
<div class="m2"><p>وصال تا ابد اندازدم به حیرانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امید نیست درین قحط مردمی که کسی</p></div>
<div class="m2"><p>ز گرگ یوسف ما را خرد به ارزانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هوای دوست پر و بال می دهد ورنه</p></div>
<div class="m2"><p>به رستخیز نمی جنبم از گران جانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز حلق کشته ام آلوده تر به خون چشمیست</p></div>
<div class="m2"><p>به درگه تو فرستاده اند قربانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>منش ز بادیه آورده ام به شهر که گفت؟</p></div>
<div class="m2"><p>که عشق همره مجنون نشد بیابانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز منکران محبت به خود پناهم ده</p></div>
<div class="m2"><p>که نوح زورق ازین ورطه کرد طوفانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز گلبنی قفسم را اگر بیاویزی</p></div>
<div class="m2"><p>کنند بر سر من بلبلان گل افشانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز هم شناخته ام چاک جیب و دامان را</p></div>
<div class="m2"><p>گذشت آن که گریبان کند گریبانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز شوق آن که زمین بوس خدمت تو کنم</p></div>
<div class="m2"><p>ز فرق تا قدمم همچو سایه پیشانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حذر کن ای غم ایام رهزنی تا کی؟</p></div>
<div class="m2"><p>به خدمت که روانم مگر نمی دانی؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روم که حلقه فتراک دلبری بوسم</p></div>
<div class="m2"><p>که دل چو گوی رباید به زلف چوگانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کجایی ای غم هجر تو مونس جانم</p></div>
<div class="m2"><p>خیال وصل تو از شاهدان پنهانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عزیمت در عبدالرحیم خان دارم</p></div>
<div class="m2"><p>فلک نگردد اگر زین رهم بگردانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هوای ابر کفش نیست در سرت که کنی</p></div>
<div class="m2"><p>کلاه پادشهی را کلاه بارانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قبای شاهی ازان دیر می کند در بر</p></div>
<div class="m2"><p>که چست یافته تشریف خان خانانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به همت تو فنا در فناست ره نبرد</p></div>
<div class="m2"><p>که در بقای تو افزود هرچه شد فانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ملال بحر و بر از خاطرم گهی برود</p></div>
<div class="m2"><p>که جام و شیشه بچینی و گل برافشانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز تلخ و شور چه زمزمم بشویی لب</p></div>
<div class="m2"><p>به چاه غبغب ساقی و راح ریحانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به دوستکامی رنگین لطیفه ای گویی</p></div>
<div class="m2"><p>به دلربایی شیرین بدیهه ایی خوانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زلال لطف زنی آنقدر به جوش دلم</p></div>
<div class="m2"><p>که برنهم سر افسانه های طولانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کنم نشاط به پیمانه میان دوری</p></div>
<div class="m2"><p>خورم شراب به اندازه پشیمانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>میی دهی که به طوفان شیشه غرق شود</p></div>
<div class="m2"><p>گر از حباب نسازد کلاه بارانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>میی که بر سر تاج قباد رشگ کند</p></div>
<div class="m2"><p>به دانه عنبش گوهر بدخشانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به دیده لای خمش توتیای پرورده</p></div>
<div class="m2"><p>گل سبوی وی از سرمه صفاهانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>میی که دیو ازو قطره ای اگر بخورد</p></div>
<div class="m2"><p>پری ز شیشه برون آرد از پری خانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>میی که یابد اگر جم ز جنس او جامی</p></div>
<div class="m2"><p>به رونما دهدش خاتم سلمانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نه آن میی که دل عاجران کباب کند</p></div>
<div class="m2"><p>میی که مرغ بهشتش کنند بریانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عفی الله از سخن هزل و لابه های مزاح</p></div>
<div class="m2"><p>هزار توبه ازین هرزهای لامانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هزار حوری و قدسی مدام می نوشند</p></div>
<div class="m2"><p>به پرده های ضمیرم شراب روحانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کجا شوم من از ام الخبائث آبستن</p></div>
<div class="m2"><p>که حاملم همه از بکرهای وحدانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رخم ز قبله ایمان به مرگ تافته باد</p></div>
<div class="m2"><p>اگر ز کعبه به حد می کنم هجارانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به جام جم نکند میل هر که نوشیده</p></div>
<div class="m2"><p>شراب معرفت بزم خان خانانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نگاه کن که به سویش چگونه می بینند</p></div>
<div class="m2"><p>به صد امید شهنشاهی و جهانبانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به قصد دشمن او تیغ کوه را هر سال</p></div>
<div class="m2"><p>زمین دوباره کند صیقلی و سوهانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هنوز کوه نجنبیده چرخ ساخته کار</p></div>
<div class="m2"><p>به تیغ برق نهادش ز فرط برانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ازین سبب شده اکنون بر جبان و شجاع</p></div>
<div class="m2"><p>سپهر شهره به جلدی زمین به کسلانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هم از حراست عدل ویست اگر برخی</p></div>
<div class="m2"><p>مصون ز فتنه دهرند انسی و جانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فلک که ثبت مه و مهر در بغل دارد</p></div>
<div class="m2"><p>به مهر او نرسانیده خط ترخانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>طفیلیان سر خوانش میزبان گردند</p></div>
<div class="m2"><p>ز خلق و مکرمتش در سرای مهمانی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زهی ز صبح کریمان گشاده ابروتر</p></div>
<div class="m2"><p>ندیده چهره خشم تو چین پیشانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گرفته ساغر دولت به دست می بینم</p></div>
<div class="m2"><p>ز آسمان نهم برگذشته میزانی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به صد تلاش ابد می کشد ز دنبالت</p></div>
<div class="m2"><p>نمی شود که جمال از ازل بگردانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مگر که گه گه یک پایه زیرتر آیی</p></div>
<div class="m2"><p>که عرش زیر سریرت نهاده پیشانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به دست مهر و عتابت زمانه داده زمام</p></div>
<div class="m2"><p>که کرده گاه عصایی و گاه ثعبانی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز صبحدم فلک کینه جو ستاده به پا</p></div>
<div class="m2"><p>به این خیال که تو در کجاش بنشانی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به عرش نالد ز اندیشه کمندت شخص</p></div>
<div class="m2"><p>معلقش به زمین آورد ز بی جانی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز شوق پایه تو قطره در رود به رحم</p></div>
<div class="m2"><p>رسد ز طبع جمادی به نفس انسانی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز بس که سیر و تردد کند بدل سازد</p></div>
<div class="m2"><p>به فطرت ملکوتی مزاج حیوانی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چه سعی ها که نماید ولی به تو نرسد</p></div>
<div class="m2"><p>که چون مقام تو بیند ز خود شود فانی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تو خود نظیر خودی لا اله الا الله</p></div>
<div class="m2"><p>همان یکی است که خود اولست و خود ثانی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کس از مکارم اخلاق نامه ای ننوشت</p></div>
<div class="m2"><p>که نام نیک تو بروی نکرد عنوانی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو نیک بینم ازان سده حلقه در گوشست</p></div>
<div class="m2"><p>کجا که بخت برآرد سر از پریشانی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کسی از تو هیچ نخواهد تو در دل مردم</p></div>
<div class="m2"><p>به دست خویش نهال امید بنشانی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هزار دفتر حاجت ز بر توانی خواند</p></div>
<div class="m2"><p>به حرف معذرتی چون رسی فرومانی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بروز جودت اگر قطرگی کند دریا</p></div>
<div class="m2"><p>گه کفایت تو قطره کرده عمانی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز کلبه ای که نفاق تو خاست می آید</p></div>
<div class="m2"><p>چو ماه منخسفش روزهای ظلمانی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به کشوری که وفاقت رسید می گذرد</p></div>
<div class="m2"><p>شبش چو آینه بی لباس نورانی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو باده از پی صافی سینه ها جویی</p></div>
<div class="m2"><p>ز شوق نشئه در آغوش خم بیالانی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به پاسبانی درگاه تو چه زود رسید</p></div>
<div class="m2"><p>پریر بود که کیوان گذاشت کیوانی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>سپهر کار رباینده است امید است</p></div>
<div class="m2"><p>که از نتیجه خدمت رسد به دربانی!</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نسیم پیرهنی می کنم هوس گرچه</p></div>
<div class="m2"><p>عذار یوسف ما به بود به عریانی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>درین قصیده به گستاخی ارچه عرفی گفت</p></div>
<div class="m2"><p>به داغ رشگ پس از مرگ سوخت خاقانی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>کنون به گور چنان او ز رشگ من سوزد</p></div>
<div class="m2"><p>که در تنور تو آن گوسفند بریانی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دگر که گفت مبادا ز راوی شعرم</p></div>
<div class="m2"><p>درین قصیده به روز کمال بنشانی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>تو را که فضل به حدی بود که در بزمت</p></div>
<div class="m2"><p>طیور وقت ترنم کنند سحبانی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>کمال جهل و بلاهت بود که طعنه زند</p></div>
<div class="m2"><p>به نقص مایه کج فهمی و غلط خوانی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>دگر نبود ز شرط ادب درآوردن</p></div>
<div class="m2"><p>به سلک مدح تو مدح حکیم گیلانی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو نقش زشت به دیوار عذر می گوید</p></div>
<div class="m2"><p>ازین تعرض من با وجود بی جانی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>کجاست گیوه گیوی و تاج افریدون</p></div>
<div class="m2"><p>کجاست کاسه شیبول و راح ریحانی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>گر او به فضل فلاطونست برکشیده تست</p></div>
<div class="m2"><p>بود به قرب کیان اعتبار یونانی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>اگرچه سایه به رفعت زمین فرو گیرد</p></div>
<div class="m2"><p>ولی نهد به پی آفتاب پیشانی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>وگرچه ابر درافشان شود کسی نکند</p></div>
<div class="m2"><p>کلاه پادشهی را کلاه بارانی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>گرفتم آن که ز فضل و هنر مجسم بود</p></div>
<div class="m2"><p>کجا به رتبت روحانیست جسمانی؟</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>اگرچه کشور چین پر ز نقش مانی بود</p></div>
<div class="m2"><p>خراب گشت نه صورت به جاست نی مانی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به طرز وی دو سه بیت دگر ادا سازم</p></div>
<div class="m2"><p>که بهر دعوی او قاطعست برهانی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>زهی به رای روان بخش شمع لاهوتی</p></div>
<div class="m2"><p>به علم در دل هر قطره کرد عمانی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به چشم عقل هیولای جوهر اول</p></div>
<div class="m2"><p>به ذوق روح تمنای نشئه ثانی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز صبح اول عالم به نور فطرت تو</p></div>
<div class="m2"><p>دبیر لوح قضا می کند قلمرانی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>به باغ کون همه روز و شب روان باشد</p></div>
<div class="m2"><p>زلال فیض تو از چار جوی ارکانی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>گهر ز صلب فلک زان به بطن خاک آید</p></div>
<div class="m2"><p>که نیز زاده اویند بحری و کانی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>کراست زهره که گوید فلان ببخش و مبخش</p></div>
<div class="m2"><p>ز تست هرچه به هر شخص داری ارزانی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>جهان برات به مهر مسلمی می داد</p></div>
<div class="m2"><p>بگفتم این چه کم انصافیست و نادانی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>تو طبل همچو گدا در ته گلیم زنی</p></div>
<div class="m2"><p>وگرنه بر لب بامست کوس سلطانی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>تو را قاف به قاف فلک گرفته کرم</p></div>
<div class="m2"><p>همین به خوان مه و خورشید می کند نانی</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>به این نوال و کرم با وجود می یابد</p></div>
<div class="m2"><p>یکی ازان دو به هر چند روز نقصانی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>گهی تبه شود این از هبوط تیره دلی</p></div>
<div class="m2"><p>گیه سیه شود آن ز احتراق رخشانی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چو آن فرو بری این را برآوری از جیب</p></div>
<div class="m2"><p>چو این برآوری آن را چراغ می رانی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>تو را هم این لب نان زله عنایت اوست</p></div>
<div class="m2"><p>نمی شود که به گل آفتاب پوشانی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>تو راست در دم هر صبح بذل یک مهری</p></div>
<div class="m2"><p>وراست در دم هر بذل مهر صدگانی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چو دید صدق حدیثم به لفظ گیلی گفت</p></div>
<div class="m2"><p>به آفتاب سخن دارد این خراسانی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ز صدق من به فراز درخت بنشستم</p></div>
<div class="m2"><p>ز کذب او به سر شاخ گاو پالانی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>جهان ستان ملکا! شه نشان خداوندا</p></div>
<div class="m2"><p>به مدحت تو خجل طوطی از زبان دانی</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>سرود هجر «نظیری » شنو که دلسوزست</p></div>
<div class="m2"><p>حکایت قفس از قمری گلستانی</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بجو چراغ و کمر باز کن که قصه من</p></div>
<div class="m2"><p>دراز و تیره ترست از شب زمستانی</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>تویی که گشتم و بر تو نیافتم بدلی</p></div>
<div class="m2"><p>منم که رفتم و بر من نداشتی ثانی</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>هزار رنگ گهر ریختم کسی نشناخت</p></div>
<div class="m2"><p>که جنس من یمنی بود یا بدخشانی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>تو در برابر چشمم به صورتی هر دم</p></div>
<div class="m2"><p>من و سرود غم و گریه های پنهانی</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ز بس گره شده در دل اگر سخن طلبی</p></div>
<div class="m2"><p>بریزم از مژه یاقوت های رمانی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چو فتح نامه تو جمله شادیم ز چه رو</p></div>
<div class="m2"><p>مرا چو مرتبه دوستان نمی خوانی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>به ناتوانی بستر ز بال مرغ کنم</p></div>
<div class="m2"><p>اگر ز گوشه بام خودت نپرانی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>امید بار درت بسته عقل می گوید</p></div>
<div class="m2"><p>که خوش چه کام دل خود به هرزه می رانی</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>عتاب و ناز به بیزاریت فروخته اند</p></div>
<div class="m2"><p>به رغبت ار بخرندت به خویش تاوانی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>اگر به کعبه ز دیرت کرشمه ای نبرد</p></div>
<div class="m2"><p>زیان کنی بت و زنار در مسلمانی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>وگر به میکده در کار عشوه ای نکند</p></div>
<div class="m2"><p>خمار بخشدت این نشئه های انسانی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>سفر معطل وقتست صبر کن چندان</p></div>
<div class="m2"><p>که جذبه ای رسد از جذبه های رحمانی</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>ببین که در برش از مهر می کشد خورشید</p></div>
<div class="m2"><p>ازان که ذره نکردست بال افشانی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>کنون به نزد تو این ماجرا فرستادم</p></div>
<div class="m2"><p>که از توهم عقلم به لطف برهانی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چگونه اند وفا و کرم امیدم هست</p></div>
<div class="m2"><p>به یک دو حرف سر خامه ای بجنبانی</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>بهای وقت در آن کوی چیست؟ می خواهم</p></div>
<div class="m2"><p>نمونه ای بخری قیمتی بفهمانی</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>به لطف بخششم از رنج روزگار بخر</p></div>
<div class="m2"><p>که آنچه جود تو باشد به آنم ارزانی</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>سخن چو می رود از حد برون چرا نرود؟</p></div>
<div class="m2"><p>به صرف کار دعاگویی و ثناخوانی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>کلید عیش به دست تو باد تا باشد</p></div>
<div class="m2"><p>سحر به غنچه گشایی صبا به رضوانی</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>ظفر به نام تو دایم هزاردستان باد</p></div>
<div class="m2"><p>به یاد خصم تو پروانه شبستانی</p></div></div>