---
title: >-
    شمارهٔ ۴۰ - این قصیده در راه مکه معظمه بعد از غارت سارقان مذیل به مدح نواب محمد عزیز اعظم خان منظوم شده
---
# شمارهٔ ۴۰ - این قصیده در راه مکه معظمه بعد از غارت سارقان مذیل به مدح نواب محمد عزیز اعظم خان منظوم شده

<div class="b" id="bn1"><div class="m1"><p>کس به مشهد پروانه ام نماید راه</p></div>
<div class="m2"><p>که خون بمسل من نیست زیب این درگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دست و خنجر او صد هزار اسماعیل</p></div>
<div class="m2"><p>به خون خویش نویسند: عبده و فداه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر قدم که روم پیش دورتر افتم</p></div>
<div class="m2"><p>که وحشی است غزل و کمند من کوتاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنوز ناشد گامی به سوی او نزدیک</p></div>
<div class="m2"><p>هزار مرحله در خون دل شدم به شناه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گرد وادی این ره نمی توان گشتن</p></div>
<div class="m2"><p>ز وهم عقده او شیر می شود روباه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی ز قافله پس مانده از دلیل ضعیف</p></div>
<div class="m2"><p>یکی به بادیه گم گشته از قیاس تباه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز کشته گشته بیابان پر و نشسته درو</p></div>
<div class="m2"><p>به شکل ماتمیان کعبه در لباس سیاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صفا ز محنت این تیه غصه می آرم</p></div>
<div class="m2"><p>چو آهن از دم مصقل چو نقره از دل کاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به حسن غارت خود ناز بیشتر دارم</p></div>
<div class="m2"><p>ز چشم شوخ که ره می زند به حسن نگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امیدوار به عجز خودم که آسان تر</p></div>
<div class="m2"><p>ز پا فتاده این ره رسد به منزلگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مبر امید که گامی به پای توفیقست</p></div>
<div class="m2"><p>اگر به مرحله یک فرسخست اگر پنجاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رسیدگان حقیقت نشسته در سیراند</p></div>
<div class="m2"><p>درآن مقام که سالک همی رسد گه گاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بس که تارک مغرور خاک ره شده است</p></div>
<div class="m2"><p>زبان عذر بروید زمین به شکل گیاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنه کلاه و کمر گر سر سفر داری</p></div>
<div class="m2"><p>که راه سخت مخوفست و روز بس کوتاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هزار بار خرد گفت هان به حرمت رو</p></div>
<div class="m2"><p>که نیست جای قدم در ره از نشان جباه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو خیره توسن جهل و غرور می تازی</p></div>
<div class="m2"><p>به حضرتی که به سر می رود سپهر دوتاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>متاع بتکده داری به سوی کعبه مبر</p></div>
<div class="m2"><p>سر معامله داری ز حج قبول مخواه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من از فریب طمع در جواب می گفتم</p></div>
<div class="m2"><p>که سعی و طوف حرم را زیان ندارد جاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نگشتم از روش خویش تا برآوردم</p></div>
<div class="m2"><p>به جای نعره لبیک بانگ واویلاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من از کمینگه دولت مراد می جستم</p></div>
<div class="m2"><p>نشانه تفکرم کرد بخت دولتخواه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز حسن تربیت فطرتم چه سود که هست</p></div>
<div class="m2"><p>به هر ترقی او آفتی مرا در راه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حیات نقد عزیزی دهد به باد فنا</p></div>
<div class="m2"><p>به هر قدم که کشد یوسفی نفس از چاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فلک غنیمت عمرم برون برد همه شب</p></div>
<div class="m2"><p>بدان کمند که در روزنم بتابد ماه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قضا به وقت خوشم کرد آن ستم که نکرد</p></div>
<div class="m2"><p>خزان به سایه بید و صبا به برگ گیاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کجاست جذبه حبل المتین توفیقی؟</p></div>
<div class="m2"><p>که تار طاقتم از بار چرخ شد یکتاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به سیر مروه و طوف مدینه مشتاقم</p></div>
<div class="m2"><p>کجاست جاذبه ای؟ یا رسول واشوقاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دلم ز هند و سموم نگر گداخته شد</p></div>
<div class="m2"><p>در آرزوی نشابورم و شمال هراه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گرم به مخلب طایر توان برون آورد</p></div>
<div class="m2"><p>ز چنگ هند جگرخواره یا نبی الله</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بشو چو دیده اعرابیم به شیر سفید</p></div>
<div class="m2"><p>که همچو مردمک هندویم به خاک سیاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تویی که در شب اسری گذشتی از کونین</p></div>
<div class="m2"><p>چنان که بگذرد از مردمان دیده نگاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>براق برق عنان توبی چرا طی کرد</p></div>
<div class="m2"><p>رهی که داشت مه سنبله میاه و گیاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به دلو ماه عطارد گلاب می افشاند</p></div>
<div class="m2"><p>حمل به نغمه ناهید می نمودش راه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اسد به خنجر مریخ شد عنان گیرش</p></div>
<div class="m2"><p>که آفتاب به پایش فکنده بود کلاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز بیم حدت مریخ مشتری آورد</p></div>
<div class="m2"><p>به منزل زحلش آن غلام بی اکراه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گذشت از ملکوت و ملک تکاور تو</p></div>
<div class="m2"><p>که گشت دست دو کون از رکاب او کوتاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عنان به پایه کرسیش بستی و رفتی</p></div>
<div class="m2"><p>چنان که این قدمت زان قدم نشد آگاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نشان پای تو بر رهگذار دیده بیافت</p></div>
<div class="m2"><p>که تاج فرق خودش ساخت عرش والاگاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حدیث دل ز دل و وصل جان ز جان برخاست</p></div>
<div class="m2"><p>که بود رؤیت و گفتار بی عیون و شفاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گشاده شد درجنت به روی بی برگان</p></div>
<div class="m2"><p>به نزد حق چو گشودی لب شفاعتخواه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سر تفاخر کرسی ز دوش عرش گذشت</p></div>
<div class="m2"><p>کف قدوم تو سودند بر جبین و جباه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به هم ز شوق سرودند کاین همان شخص است</p></div>
<div class="m2"><p>که نور او به جهان داده این جلالت و جاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ملک به سجده گل سر فرو نمی آورد</p></div>
<div class="m2"><p>سپهر صورت فضلش نگاشت بر درگاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدن ز رشحه رحمت به هم نمی آمیخت</p></div>
<div class="m2"><p>جهان نوید وفاقش فکنده در افواه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به صدق دعوت حق او شهادت آورده</p></div>
<div class="m2"><p>ز بعد اشهد ان لا الاه الا الله</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز بس بلند بود همتش گه بخشش</p></div>
<div class="m2"><p>گنه برند برش عاصیان به عذر گناه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سبک عتاب شفیعا! گران خطا بخشا!</p></div>
<div class="m2"><p>که هست مهر تو طاعت فزای و عصیان کاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو را که قدرت قرب این بود نجاتی ده</p></div>
<div class="m2"><p>مرا که ظلمت شب می فزایم از دم آه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به قدر خود که مرا همت بلندی ده</p></div>
<div class="m2"><p>که سر فرود نیارم به صدر و منصب شاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز هند سفله به جز سفلگی نمی زاید</p></div>
<div class="m2"><p>کراهتست همه حاصل طبیعت و آه!</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بضاعت ره دین داده ام به غارت کفر</p></div>
<div class="m2"><p>ز من مربی دین را که می کند آگاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ستون شرع محمد عزیز اعظم خان</p></div>
<div class="m2"><p>پناه دین نبی پاس دار قول الاه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نمود بدرقه همتش مدد ورنه</p></div>
<div class="m2"><p>هزار گونه خطر بود شرع را در راه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در آن دیار که خورشید فصل او تابد</p></div>
<div class="m2"><p>به ذره درزند آتش فروغ سایه جاه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سپهر و هرچه خدا آفرید سایه تست</p></div>
<div class="m2"><p>شبیه نیست تو را لا الاه الا الله</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به خوف گاه نبوت حمایت تو رسید</p></div>
<div class="m2"><p>وگرنه یوسفش افتاده بود اندر چاه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز طبع و رای تو خورشید آسمان دارد</p></div>
<div class="m2"><p>همان حجاب که اشباه دارد از اشباه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به شیر بیشه گردون دلیر حمله کند</p></div>
<div class="m2"><p>گر آهویی خورد از وادی تو آب و گیاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>متاع دهر نمی ارزد التفات تو را</p></div>
<div class="m2"><p>به سلطنت فکنی سایه از سر اکراه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر به هم نکشد همت تو چین جبین</p></div>
<div class="m2"><p>ز نه فلک به سرش برنهد زمانه کلاه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>وگر تو نقد شب و روز را به خرج دهی</p></div>
<div class="m2"><p>زمانه کیسه کند خالی از سفید و سیاه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به رای از ملکان ملک آن چنان گیری</p></div>
<div class="m2"><p>که باد از نفس کهربا رباید کاه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ندیده هیبت تو در نبرد روی غنیم</p></div>
<div class="m2"><p>ندیده دولت تو در مصاف پشت سپاه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو آتش که به خود در تن چنار افتد</p></div>
<div class="m2"><p>کند عدوی تو را قطع نسل قوت باه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به عهد دولتت آن را که دل غمین گردد</p></div>
<div class="m2"><p>چو ریسمان سیاست به حلق پیچد آه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز همت تو چه گویم امل دراز شود</p></div>
<div class="m2"><p>به سایه تو چو آیم سخن شود کوتاه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بهار طبع امیرا که در حدیقه ملک</p></div>
<div class="m2"><p>نگشته سرو تو از بار «من خلق » دوتاه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سپهر منزلتا بر درت «نظیری » را</p></div>
<div class="m2"><p>نماز شام مصیبت دمید صح پگاه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مخند گر شب ماتم نشاط عید کند</p></div>
<div class="m2"><p>که دیده است به سلخ حیات غره ماه</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به درگهت ز بد و نیک داد ازان دارد</p></div>
<div class="m2"><p>که دیده مسند پاداش و بند پادافراه</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سخن که خسته افتاده می کند بپذیر</p></div>
<div class="m2"><p>که لخت دل به رخ بسترست و گل درگاه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>رخیست غرقه به خونم نثار درگه تو</p></div>
<div class="m2"><p>سزای سکه برآورده ام زری از کاه</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>امید هست که بر درگه کریم کنند</p></div>
<div class="m2"><p>قبول هدیه ناچیز و تحفه مزجاه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به گوشه نظر التفات محتاجم</p></div>
<div class="m2"><p>به زارییی که توان کشتنم به نیم نگاه</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز بی بضاعتی خود چنان هراسانم</p></div>
<div class="m2"><p>که بهر توشه ره بازگردم از درگاه</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به سیل مرحمت از خاک ذلتم بردار</p></div>
<div class="m2"><p>که همچو ماهی عطشان فتاده ام در راه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>همیشه تا نشود کشته شمع مهر از باد</p></div>
<div class="m2"><p>مدام تا نشود تیره روی روز از آه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>شکوه روی تو آرایش کلاه تو باد</p></div>
<div class="m2"><p>چو نور ماه که بر آسمان زند خرگاه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چراغ عمر تو پرنور تا صباح نشور</p></div>
<div class="m2"><p>که روشنست به نور تو شرع را بنگاه</p></div></div>