---
title: >-
    شمارهٔ ۱ - یک قصیده
---
# شمارهٔ ۱ - یک قصیده

<div class="b" id="bn1"><div class="m1"><p>چندی به غلط بتکده کردیم حرم را</p></div>
<div class="m2"><p>وقتست که از کعبه برآریم صنم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیخ هوس وصل ببریم که زشتست</p></div>
<div class="m2"><p>خار و خس بیگانه گلستان ارم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما و در آسودگی مرگ کزین بیش</p></div>
<div class="m2"><p>زحمت نتوان داد شفا را و الم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمریست که همسایه بختیم درین کوی</p></div>
<div class="m2"><p>یک بار ندیدیم در خانه هم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دست به پیچاک سر زلف نیرزد</p></div>
<div class="m2"><p>انگشت جم ارزنده بود خاتم جم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسیار دویدیم و به جایی نرسیدیم</p></div>
<div class="m2"><p>در خانه نشاندیم دگر بخت دژم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنهایی این بادیه را شور غریب است</p></div>
<div class="m2"><p>مجنون نه سیه خانه شناسد نه حشم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشق ز پریشانی خاطر چه نویسد</p></div>
<div class="m2"><p>هر روز شماریست سر زلف بخم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا هست غمی شفقت ایام به من هست</p></div>
<div class="m2"><p>آن نیست که از کم ندهد قسمت کم را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دستی ز شکربخشی دوران نبرم پس</p></div>
<div class="m2"><p>کز هر بن ناخن نکشم خار ستم را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کار سر کلکم نکند نشتر فصاد</p></div>
<div class="m2"><p>خونابه کش زخم درونست الم را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا توبه ام از مذهب و از کیش ندادند</p></div>
<div class="m2"><p>در باز نکردند خرابات و حرم را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برهم زده ام مسکن و معبد به سراغت</p></div>
<div class="m2"><p>کافر به چه حالست که گم کرده صنم را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غواص که دیدست به بیچارگی من</p></div>
<div class="m2"><p>از دست گهر داده و درباخته دم را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عشق من و حسن تو قدیمند ولیکن</p></div>
<div class="m2"><p>در خدمت تو نام و نشان نیست قدم را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در بیع وفای تو من از بس که حریصم</p></div>
<div class="m2"><p>ناکشته به بیعانه دهم وجه سلم را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مدی دو سه مخصوص دل ما نکشیدی</p></div>
<div class="m2"><p>مخدوم چنین یاد نمودست خدم را؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ما نام خود از حاشیه شستیم کزین بیش</p></div>
<div class="m2"><p>مهمان طفیلی نتوان بود قلم را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در مدح سپهدار گریزیم که نامش</p></div>
<div class="m2"><p>در وزن فزاید چه سخن را چه درم را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>داد و دهش از خاصیت اسم رحیم است</p></div>
<div class="m2"><p>گر جیب عیارست و گر دست کرم را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بی یاری این نام به منزل نرسد کس</p></div>
<div class="m2"><p>ای ساحل توفان زدگان نام تویم را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرجا کف راد تو سر بدره گشاید</p></div>
<div class="m2"><p>بر کیسه بماند گره ارباب همم را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در عرض خداوندی تو خواجگی خصم</p></div>
<div class="m2"><p>دیریست که کافر نکند سجده صنم را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در رهگذر قافیه لا نفتادست</p></div>
<div class="m2"><p>تا بدرقه کردست سخای تو نعم را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جان داروی مهر تو به ملکی که نباشد</p></div>
<div class="m2"><p>صحت به در مرگ نویسند سقم را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اختر خبر از غیب دهد حالتش اینست</p></div>
<div class="m2"><p>کز سجده تو راست کند پشت بخم را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گردون دم از اعجاز زند حکمتش اینست</p></div>
<div class="m2"><p>کز دیدن تو بیش کند دولت کم را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اسباب جهانبانی تو ساخته گردون</p></div>
<div class="m2"><p>از نفع و ضرر داده به تیغت تف و نم را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زنجیر غلامی تو پرداخته گیتی</p></div>
<div class="m2"><p>از عشق و وفا برد به کار آتش و دم را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر دهر ترش گر شودت گوشه ابرو</p></div>
<div class="m2"><p>حدت بستاند ز بقم رنگ بقم را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دیدند چو حجاب قضا ملک تو گفتند</p></div>
<div class="m2"><p>رفتیم که دروازه ببندیم عدم را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر روی ضعیفان تهی دست گشاده</p></div>
<div class="m2"><p>مفتاح سر کلک تو ابواب کرم را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا مهر تو و کین تو در دل نسرشتند</p></div>
<div class="m2"><p>جاری ننمودند به لب مدحت و ذم را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>انجم سپها گر ز درت گوشه گرفتم</p></div>
<div class="m2"><p>برداشتم از سلک خدم کار اهم را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در پاس تو یک رو چو دم تیغ نگارم</p></div>
<div class="m2"><p>زان بیش که دم شعله کند صبح دو دم را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عریان ز هوس ها شدم آن روز که دادم</p></div>
<div class="m2"><p>تشریف ز گرد در تو بیت حرم را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قصدم همه این بود که در خدمت معبود</p></div>
<div class="m2"><p>بر گوشه نهم صحبت مخدوم و خدم را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باز از اثر لطف و عطا انس در تو</p></div>
<div class="m2"><p>از چشم و دلم برد برون وحشت و رم را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دل گفت که ته جرعه عیش آب حیاتست</p></div>
<div class="m2"><p>یک چند چشیدم به گمان شربت سم را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دیدم که همان شکر آلوده به زهر است</p></div>
<div class="m2"><p>لاحول کنان بوسه زدم خوان کرم را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>راضی شده ام بی تو به اکسیر قناعت</p></div>
<div class="m2"><p>نشناخته ام قیمت آن خاک قدم را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کس مملکت فقر و قناعت نگرفتست</p></div>
<div class="m2"><p>بر گوشه نهادست گدا طبل و علم را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مجنون شوم و کام تو از دهر بگیرم</p></div>
<div class="m2"><p>کاین دیو به دیوانه دهد خاتم جم را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سرمست به سودای تو برخاست «نظیری »</p></div>
<div class="m2"><p>صبحی که گشودند خرابات قدم را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو نقل و می و مطرب ازو بازنگیری</p></div>
<div class="m2"><p>او باز ندارد ز زبان شکر نعم را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا طبع درآمد شدن هر خوش و ناخوش</p></div>
<div class="m2"><p>غمخوار پناهی طلبد شادی و غم را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بادا در تو مأمن و ملجای حوادث</p></div>
<div class="m2"><p>سادات عرب را و سلاطین عجم را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اقطاب جهان را ز تو تحقیق ارادت</p></div>
<div class="m2"><p>چون از جهت کعبه مقیمان حرم را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بر واقعه بابری و قصه چنگیز</p></div>
<div class="m2"><p>فتح از تو نویسند همایون دوم را</p></div></div>