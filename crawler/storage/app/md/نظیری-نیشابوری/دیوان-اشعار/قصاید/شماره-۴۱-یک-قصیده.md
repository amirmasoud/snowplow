---
title: >-
    شمارهٔ ۴۱ - یک قصیده
---
# شمارهٔ ۴۱ - یک قصیده

<div class="b" id="bn1"><div class="m1"><p>ای جلالت خلوت از اغیار تنها ساخته</p></div>
<div class="m2"><p>حکمت تو از کرم دی کار فردا ساخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده از روی صفات ذات خود برداشته</p></div>
<div class="m2"><p>آنچه پنهان بود در علم آشکارا ساخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل کل بگشوده بر خورشید ذاتت روزنی</p></div>
<div class="m2"><p>وز فروغش دیده هر ذره بینا ساخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شحنه عشقت نشسته بر سر بازار کون</p></div>
<div class="m2"><p>درسگاه عقل را دکان سودا ساخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بدایت عشق را از شور حسن انگیخته</p></div>
<div class="m2"><p>وز ریاح انس انسان روح پیدا ساخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق صورت کن سروده در طربگاه شهود</p></div>
<div class="m2"><p>روح راه رقص بر آهنگ آوا ساخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رقص خود از عشق دیده صوت عشق از شور حسن</p></div>
<div class="m2"><p>پس خزیده از سماع و قطع غوغا ساخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندرین ره آن که منزل دیده و واصل شده</p></div>
<div class="m2"><p>تا نهایت رفته و مقطع ز مبدا ساخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل را بین کاندرین صحرای پرخوف و خطر</p></div>
<div class="m2"><p>صید گنجشگی نکرده رام عنقا ساخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بحر ژرف و، ریسمان کوتاه و، غواص اعجمی</p></div>
<div class="m2"><p>وین قضا در قعر یم لؤلؤ لالا ساخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راه بر شمشیر و شاهد در حصار آتشین</p></div>
<div class="m2"><p>عشق عاشق را به رفتن بی محابا ساخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کس نبیند بدر رخسار تو الا در حجاب</p></div>
<div class="m2"><p>گرچه دل افزون دریده پرده اخفا ساخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در ازل یک در بیضا کرده پیدا از وجود</p></div>
<div class="m2"><p>در بیضا آب کرده بحر خضرا ساخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دم زده بحر و پدید آورده ابر قطره بار</p></div>
<div class="m2"><p>بسته قطره بحر پر لولوی لالا ساخته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کرده چندین صورت از یک جوهر اصلی پدید</p></div>
<div class="m2"><p>یک حقیقت این همه شکل آشکارا ساخته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آفریده کل عالم را ز جزو نقطه ای</p></div>
<div class="m2"><p>وی عجب کل جای در هر جزو اجزا ساخته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بی درون و بی برون بگرفت بیرون و درون</p></div>
<div class="m2"><p>ماورای شیب و بالا شیب و بالا ساخته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نظم امکان و وجوب آورده افلاطون عشق</p></div>
<div class="m2"><p>عقل نشکافد که این معنی معما ساخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن که شیرین گوست بر کج مج زبانان گو مخند</p></div>
<div class="m2"><p>کاندرین پرده مگس آهنگ عنقا ساخته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>داستان واجب و ممکن بجز یک حرف نیست</p></div>
<div class="m2"><p>بر زبان آورده چون الکن مثنا ساخته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کرده خود وصف کمال ذات خود در هر زبان</p></div>
<div class="m2"><p>ذات خود را در حجاب خویش گویا ساخته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گاه عذرا بوده و در کار وامق کرده ناز</p></div>
<div class="m2"><p>گاه وامق گشته و با داغ عذرا ساخته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از کنار پیر کنعان یوسف صدیق را</p></div>
<div class="m2"><p>خوار و سرگردان سودای زلیخا ساخته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر که را آورده شور جذبه تو در سماع</p></div>
<div class="m2"><p>جبه و جلباب هستی پاره در پا ساخته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون هوا تو در خفایا چون سراب اندر نمود</p></div>
<div class="m2"><p>از نمود ما وجود خود هویدا ساخته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ماییی ما در هوای خود نموده چون سراب</p></div>
<div class="m2"><p>بی هوای خویش ما را برده بی ما ساخته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همچو آب زندگی پوشیده از ما روی ما</p></div>
<div class="m2"><p>چون سراب از آب تو با رنگ و سیما ساخته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از که جز ذات تو این ایثار می آید که او</p></div>
<div class="m2"><p>خویش را پوشیده ما را آشکارا ساخته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شکر این شفقت که گوید کز دو عالم ذات تو</p></div>
<div class="m2"><p>گرچه بر سر آمده جا در سویدا ساخته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>طبع احیا داده اشیا را به اکسیر وجود</p></div>
<div class="m2"><p>حکمت ذرات عالم را مسیحا ساخته</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گشته از دریای فضلت ابر رحمت قطره بار</p></div>
<div class="m2"><p>باطن هر ذره را از علم دریا ساخته</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در نهاد ما عبودیت سرشته از الست</p></div>
<div class="m2"><p>نقش آب و خاک ما طوعا اطعنا ساخته</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زاصل خلقت داده ما را علم بر هستی خویش</p></div>
<div class="m2"><p>از ازل ما را به ذات خود شناسا ساخته</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حکمتت از بهر احداث موالید ثلاث</p></div>
<div class="m2"><p>کرده سفلی امهات و علوی آبا ساخته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر یکی زان راست طبعان را دوات و خامه ای</p></div>
<div class="m2"><p>داده از طبع و بنان مشغول انشا ساخته</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خواسته بر جنس و نوع دفتر مجمل فصول</p></div>
<div class="m2"><p>فصل و ابواب کتاب کل مجزا ساخته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قصه بر خود خوانده و از خود نوشته تا ابد</p></div>
<div class="m2"><p>پاک از تکرار و سهو انشا و املا ساخته</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بعد ازان انشا مرکب کرده با هم مفردات</p></div>
<div class="m2"><p>شخص انسان انتخاب کل اسما ساخته</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مشت خاکی را به تأثیر صفات خویشتن</p></div>
<div class="m2"><p>اسم اعظم کرده و عین مسما ساخته</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کنز اسرار الوهیت نموده هیکلی</p></div>
<div class="m2"><p>نامش آدم کرده حرزش اصطفینا ساخته</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تاج فخر علم الاسما نهاده بر سرش</p></div>
<div class="m2"><p>بر سریر اسجدوا از عزتش جا ساخته</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بهر انس او که وحشت داشت از حور و ملک</p></div>
<div class="m2"><p>از صراع پهلویش ترکیب حوا ساخته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خطبه حوا و آدم بهر آیین نکاح</p></div>
<div class="m2"><p>از ثنای خویش و نعت احمد انشا ساخته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در شهادت گفته نام مصطفا با نام خویش</p></div>
<div class="m2"><p>علت غایی آدم آشکارا ساخته</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عشقبازی بین که بهر یک شجر دهقان صنع</p></div>
<div class="m2"><p>هشت خضرا آفریده هفت غبرا ساخته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گفته از حب حبیب خویش حرفی با عدم</p></div>
<div class="m2"><p>عالم آسوده را پر شور و غوغا ساخته</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ای جمالت آب ما را نار حمرا ساخته</p></div>
<div class="m2"><p>خاک ما را برده و باد مسیحا ساخته</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در تن ما می پرد جان در هوای وصل تو</p></div>
<div class="m2"><p>بوی گل مرغ قفس را ناشکیبا ساخته</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از پی اظهار قدرت طایران قدس را</p></div>
<div class="m2"><p>حکمتت آورده و محبوس دنیا ساخته</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>صنعت بازیچه ای چندست و ما را همچو طفل</p></div>
<div class="m2"><p>بهر دفع گریه مشغول تماشا ساخته</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جادوی دنیا موالید مشعبد کرده جمع</p></div>
<div class="m2"><p>رشته صد تو پدید از تاریکتا ساخته</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رشته ها را همچو تار سحر کرده پر گره</p></div>
<div class="m2"><p>هر دم از افسون یکی بسته یکی واساخته</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عزت تو «عندنا باق » نوشته بر نگین</p></div>
<div class="m2"><p>«انفدو ما عندکم » نقش رخ ما ساخته</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هشت ثابت در مقام خویشتن دایم هوا</p></div>
<div class="m2"><p>جنبش چرخش صبا کردست نکبا ساخته</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هرچه از بحر و بر هستی برون آورده سر</p></div>
<div class="m2"><p>خرج وجه «کل شیئی هالک الا» ساخته</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کبریات از مغز چشم سالکان راه دین</p></div>
<div class="m2"><p>در بیابان بهر زاغان خوان یغما ساخته</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کلک استغنات هرجا خط بحرب الله زده</p></div>
<div class="m2"><p>آیه «نصر من الله » کفر طغرا ساخته</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هر کجا جباریت رخش جلال انگیخته</p></div>
<div class="m2"><p>چهره اسلام گم در گرد هیجا ساخته</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عاشقانت سینه بر شمشیر کافر می زنند</p></div>
<div class="m2"><p>کز شهادت عشق تو اموات احیا ساخته</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>منکرانت تیغ بر فرق مجاهد می کشند</p></div>
<div class="m2"><p>کز خطاشان لات و هبلی دست بالا ساخته</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>امر و نهی تست آن کاقرار و انکاری به او</p></div>
<div class="m2"><p>مؤمن و عابد مطیع گبر خود را ساخته</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زآهن یک کان به حکمت کبریای ذات تو</p></div>
<div class="m2"><p>گاه سیف الله گه شمشیر ترسا ساخته</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هشته از روی توجه رشته حبل المتین</p></div>
<div class="m2"><p>کفر برقع بافته ایمان مصلا ساخته</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گر نخندم در میان گریه کافر نعمتیست</p></div>
<div class="m2"><p>داده غم وز یاد خود تریاق غم ها ساخته</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ور نسازم آرزو کوتاه نافرمانی است</p></div>
<div class="m2"><p>عشق را معمار عقل کارفرما ساخته</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>شادی و اندوه تو بر جان کس کوتاه نیست</p></div>
<div class="m2"><p>هرکسی را جامه عشقت به بالا ساخته</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>راح روح آمیخته با دردی جسم و حاس</p></div>
<div class="m2"><p>آفریده عشق کان می را مصفا ساخته</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دمبدم انفاس رحمانی دمیده بر جهان</p></div>
<div class="m2"><p>بلبل و گل را ازان گویا و بویا ساخته</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>وقت فکر از حسن اوصاف تو صید انداز عقل</p></div>
<div class="m2"><p>پر ز آهوی معانی دشت و صحرا ساخته</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ماه نخشب کرده طالع از چه روشندلان</p></div>
<div class="m2"><p>یوسفان را بر سر آن چاه سقا ساخته</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>در خطا بگرفته دست ما به شفقت بارها</p></div>
<div class="m2"><p>گرچه قادر بر خطا ما را به عمدا ساخته</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>داده وهم و حس و عقل وعشق چند اضداد را</p></div>
<div class="m2"><p>زاب و خاک و باد و آتش جلد و اعضا ساخته</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>کرده چون زیق قوا را در گل حکمت نهان</p></div>
<div class="m2"><p>پس به تکسیر هوا یک یک مجزا ساخته</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>از غش سفلی برآورده عیار پاک را</p></div>
<div class="m2"><p>برده سوی علوی و اکسیرآسا ساخته</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>شسته زاب کوثر و تسنیم پاکش بارها</p></div>
<div class="m2"><p>داخل اکسیر کل یعنی هیولا ساخته</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>عین جوهر کرده یک بار دگر اعراض را</p></div>
<div class="m2"><p>نور جزوی متصل با نور اعلا ساخته</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>نور اول عقل کل لوح مبینش کرده نام</p></div>
<div class="m2"><p>اصل اشیا ذات مولای مزکا ساخته</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>خواجه کونین و مقصود دو عالم مصطفی</p></div>
<div class="m2"><p>آن که حقش محرم عرش معلا ساخته</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>پای بر افلاک از همت نهاده رفرفش</p></div>
<div class="m2"><p>بر سر ره هیبتش جبریل را جا ساخته</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>زنده از «اوحی الی عبده » دل شب داشته</p></div>
<div class="m2"><p>از «ابیت عند ربی » نزل احیا ساخته</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تاج تکریم «لعمرک » حق نهاده بر سرش</p></div>
<div class="m2"><p>خلعت تعظیم «لولاک »ش به بالا ساخته</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>رؤیت «خیرالهدی حق الیقین »ش کرده دل</p></div>
<div class="m2"><p>بر «صراط المستقیم »ش عقل دانا ساخته</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>در شهادت کرده حق نامش ردیف نام خویش</p></div>
<div class="m2"><p>پس به ذکر آن جدا مؤمن ز ترسا ساخته</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>فکر نعتش سوخته ما را به داغ مفلسی</p></div>
<div class="m2"><p>عقل ما را خشک مغز از دود سودا ساخته</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>گر به حق بستایمش شرکست الحاد و حلول</p></div>
<div class="m2"><p>ور به وصف خلق از همتاش یکتا ساخته</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>پس همان گویم که در فرقان و انجیل و زبور</p></div>
<div class="m2"><p>بر زبان انبیا جبریل گویا ساخته</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ای وجود از نور تو ذرات پیدا ساخته</p></div>
<div class="m2"><p>عقل کل را پرتو ذات تو بینا ساخته</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>نور تو فایض شده بر عقل طالع گشته روح</p></div>
<div class="m2"><p>نور تو وارد شده بر نقش دنیا ساخته</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>کوکب نور تو اشیا را شده اصل الاصول</p></div>
<div class="m2"><p>خلق نامش حب خضرا در بیضا ساخته</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>فیض این نورت به تسلیم ملایک کرده خاص</p></div>
<div class="m2"><p>فیض این نورت به جن و انس مولا ساخته</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>سید اولاد آدم جبرئیلت خوانده نام</p></div>
<div class="m2"><p>«رحمة للعالمین »ت حق تعالا ساخته</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>موج حکمت در دل از «عین الیقین »ت کرده جوش</p></div>
<div class="m2"><p>چشمه ز آلایش نموده پاک دریا ساخته</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>از کتاب «من لدن » علمت ادب آموخته</p></div>
<div class="m2"><p>در دبیرستان «اوادنا»ت دانا ساخته</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>در حقیقت نکته ای از کلک فضلت بیش نیست</p></div>
<div class="m2"><p>هرچه را انسان مجلد یا مجزا ساخته</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>گفته در قانون طب حرفی به ترسای طبیب</p></div>
<div class="m2"><p>ز اهل ایمان گشته و زنار را واساخته</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>زیور لفظ احادیث متین معیار او</p></div>
<div class="m2"><p>حلقه در گوش ادیبان اطبا ساخته</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>از توجه کرده بیماران باطن را علاج</p></div>
<div class="m2"><p>درد نادانی و دل سختی مداوا ساخته</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>خواب هرکس در جهان بر شکل بیداری اوست</p></div>
<div class="m2"><p>دیده حق بین تو رؤیت ز رؤیا ساخته</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>حسن ظاهر نزد باطن در دو دنیا کرده عرض</p></div>
<div class="m2"><p>خواب و مرگ آیینه صغرا و کبرا ساخته</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>خوانده بر تو صورت امروز را از لوح دی</p></div>
<div class="m2"><p>فضل حق کایینه امروز و فردا ساخته</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>در ره عرفان به رأفت عقل و حس وهم را</p></div>
<div class="m2"><p>خار خشگ شبهه بیرون از کف پا ساخته</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>دیده سدر المنتهای معرفت را برگ بر</p></div>
<div class="m2"><p>وادی تحقیق را طی تا به اقصا ساخته</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>یافته از سابقان بزم عزت برتری</p></div>
<div class="m2"><p>مقعد صدق ملیک مقتدر جا ساخته</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>کرده بر کل مقامات صفات خود عروج</p></div>
<div class="m2"><p>بر در خلوتسرای ذات مأوا ساخته</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>در شهود آراسته باطن به آثار جمال</p></div>
<div class="m2"><p>از غبار آیینه خاطر مصفا ساخته</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>حسن خلق از قرب خالق گشته سر تا پا تمام</p></div>
<div class="m2"><p>هرچه مظهر دیده از ظاهر هویدا ساخته</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>واله خلق جمیلت عاصی و ترسا شده</p></div>
<div class="m2"><p>حسن رخسارت حکیم و شیخ شیدا ساخته</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>حلم تو در راه دین بار احد برداشته</p></div>
<div class="m2"><p>وز تنومندی ننالیده به ایذا ساخته</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بر ستم گر بازویت نگشاده تیر انتقام</p></div>
<div class="m2"><p>گوهر ثابت به جور سنگ خارا ساخته</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>قوم عاصی سنگ بر لب های معصومت زده</p></div>
<div class="m2"><p>تو لب خونین به عذر قوم گویا ساخته</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>مجرمان را کرده از عفو و ترحم توبه کار</p></div>
<div class="m2"><p>کافران را مؤمن از رفق و مدارا ساخته</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>از فتوت کرده جرم دشمن غدار عفو</p></div>
<div class="m2"><p>گرچه عذر باطنی را دشمن امضا ساخته</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>تاخته اعرابی از حرص و حماقت بر سرت</p></div>
<div class="m2"><p>تو ز احسانت شتر پر بار خرما ساخته</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>هر خرابی کز مدینه تا به بحرین آمده</p></div>
<div class="m2"><p>بهر تألیف دلی بی خواهش اعطا ساخته</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>هر کجا در عهده همت گرفته مشکلی</p></div>
<div class="m2"><p>بوده گر کار دو عالم بی تقاضا ساخته</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>نا نراند بر زبان الا به نفی غیر حق</p></div>
<div class="m2"><p>از سخاوت آن هم استثنا بالا ساخته</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>حیدر صفدر که در رزمش قفا دشمن ندید</p></div>
<div class="m2"><p>در صف از تو جسته استظهار و ملجا ساخته</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>از ثباتت بارها در رزم جمعیت شده</p></div>
<div class="m2"><p>گرچه حرب خصمت از اصحاب تنها ساخته</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>آدم ار دیدی حیای تو کجا عاصی شدی</p></div>
<div class="m2"><p>شرم تو شرمنده شیطان را ز اغوا ساخته</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>این حیا در ظل ایمان پرده دار عزتست</p></div>
<div class="m2"><p>هر که را از پرده بیرون دیده رسوا ساخته</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>آن گه از ایمان حیا را در حمایت آمده</p></div>
<div class="m2"><p>با خدایش کارهای خاص پیدا ساخته</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>کامل ایمان تر نباشد از تو در عالم کسی</p></div>
<div class="m2"><p>کز حیا خود را ز چشم خویش اخفا ساخته</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>دیده ای بر مؤمن و کافر به چشم مرحمت</p></div>
<div class="m2"><p>رحمت عالم الهی در دلت جا ساخته</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>در شب معراج برگشته ز ره هفتاد بار</p></div>
<div class="m2"><p>کار امت را به نزد حق تعالا ساخته</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>از کمال مهر و شفقت در محل احتضار</p></div>
<div class="m2"><p>امت امت گفته جان تسلیم مولا ساخته</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ای تماشاگاه حق مرآت اشیا ساخته</p></div>
<div class="m2"><p>در تواضع قدر حال خویش پیدا ساخته</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>حق حبیب الله از عزت تو را خواندست و تو</p></div>
<div class="m2"><p>از تواضع نام خود «عبدا شکورا» ساخته</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>روز رزم از جا نجنبیده ولی هنگام بزم</p></div>
<div class="m2"><p>کودکی دستت اگر بگرفته برپا ساخته</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>بر معاند ظن لاف «لا نبی بعدی » زده</p></div>
<div class="m2"><p>«ما انا الا بشر» نزل احبا ساخته</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>هیچگه ناطق نگشته از هوای نفس خویش</p></div>
<div class="m2"><p>بارها بهر صلاح دین به اعدا ساخته</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>حق به زیر سایبان عصمتت پرورده است</p></div>
<div class="m2"><p>در دلت آداب زهد و عفت ابقا ساخته</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>در دل شب ها سرشگ دیده معصوم تو</p></div>
<div class="m2"><p>از زلل پاک انبیا را وز خطایا ساخته</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>عدل از تعدیل اخلاقست وین نسبت تو را</p></div>
<div class="m2"><p>بر خط وسطای حکمت حکم اجرا ساخته</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>دین تو از عدل میزان حق و باطل شده</p></div>
<div class="m2"><p>شرع تو قانون خود کیش نصارا ساخته</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>جز از آثار عدالت نیست این کز اعتقاد</p></div>
<div class="m2"><p>متفق در کار دین شرع تو دل ها ساخته</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>از عدالت کرده در عیش صدیقان اهتمام</p></div>
<div class="m2"><p>حظ خود ایثار بر اصحاب و ابنا ساخته</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>نعمت کونین را پیشت ملایک کرده عرض</p></div>
<div class="m2"><p>با همه زهدت بلابدی ز دنیا سوخته</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>جوع و سیری را به نوبت کرده زهدت اختیار</p></div>
<div class="m2"><p>گرچه حق پر از ذهب صحرای بطحا ساخته</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>کار عالم را کفایت کرده از یک ماجرا</p></div>
<div class="m2"><p>ورد خود در هر دعا «رزقا کفافا» ساخته</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>کرده در نان جوی امساک بهر قوت خویش</p></div>
<div class="m2"><p>گنج ها را صرف در ایثار و اعطا ساخته</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>با لزوم زهد آورده به جا حق جهاد</p></div>
<div class="m2"><p>بی درم کار غزایا و سرایا ساخته</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>لؤلؤ منثور پاشیده ز رویت در وضو</p></div>
<div class="m2"><p>نور بیضا هیئت تابان ز سیما ساخته</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>اتصال «لی مع الله » کرده حاصل در نماز</p></div>
<div class="m2"><p>«ما سوی الله » را ز استغراق افنا ساخته</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>از حضور قلب مستغرق به نور حق شده</p></div>
<div class="m2"><p>وز خضوع جسم سر دل هویدا ساخته</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>از میان برداشته لطف حق استار حجاب</p></div>
<div class="m2"><p>نعمت دنیا و عقبا را مهیا ساخته</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>شکر عشقت داده بر معراج «عندالله » عروج</p></div>
<div class="m2"><p>ز استقامت جای در صحو از سکارا ساخته</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>ظاهرا بنموده اعضا در رکوع و در سجود</p></div>
<div class="m2"><p>باطنا غایب مصلی از مصلا ساخته</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>در نماز از بهر خرسندی امت کرده سهو</p></div>
<div class="m2"><p>لیک سهوت غایب از ادنا به اعلا ساخته</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>جلوه حق دیده در آیات قرآنی دلت</p></div>
<div class="m2"><p>از تجلی قرائت روشن اعضا ساخته</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>دیده از ترتیل و تدبیر زبان و دل اثر</p></div>
<div class="m2"><p>مو به مویت را خشوع قلب قرا ساخته</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>آیه «نون والقلم » را دیده از انوار خویش</p></div>
<div class="m2"><p>سر باطن را به لفظ ظاهر املا ساخته</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>گرچه رو در کعبه گل گشته ساجد ظاهرا</p></div>
<div class="m2"><p>عرش دل را قبله سرا و ضرا ساخته</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>از حریم حرمتش جان را نموده محترم</p></div>
<div class="m2"><p>وز صفای صفوتش دل را مصفا ساخته</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>در طریق عمره از لبیک خونین کرده غسل</p></div>
<div class="m2"><p>گاه احرام از دو عالم دل معرا ساخته</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>کرده قربانگاه اوصاف مبینت از منا</p></div>
<div class="m2"><p>سیرگاه اوج علوی از معلا ساخته</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>پر و بال راه بیچون کرده جوع و صوم را</p></div>
<div class="m2"><p>جسم را با روح هم پرواز بالا ساخته</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>صمت و سمر و جوع را بنهاده بر تقوی بنا</p></div>
<div class="m2"><p>عشق و شوق قرب را از صوم مبنا ساخته</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>صوم بیداری فزوده از گرانی کرده کم</p></div>
<div class="m2"><p>صوم برده جسم سفلی روح والا ساخته</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>داده عزت مستحقان را به ارسال زکات</p></div>
<div class="m2"><p>وز ذمایم مال داران را معرا ساخته</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>خمس و انواع عبادات و زکات فضل خویش</p></div>
<div class="m2"><p>بر جهان احکام و امر و نهی آنها ساخته</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>از رسالت بر رسولان کرده اعطای درود</p></div>
<div class="m2"><p>وز ولایت بر موالی نشر آلا ساخته</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>کعبه ای از چار اصل این فضایل کرده راست</p></div>
<div class="m2"><p>سقف آن از چار رکن شرع برپا ساخته</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>کرده اول رکن را از صدق صدیقی بنا</p></div>
<div class="m2"><p>پس ز رکن عدل فاروقش توانا ساخته</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>حلم «ذوالنورین » داده رکن سیم را نظام</p></div>
<div class="m2"><p>وز شجاعت مرتضی بنیادش اعلا ساخته</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>راه آن از پرتو این چار کوکب کرده حام</p></div>
<div class="m2"><p>باب آن را از دوستی آل زهرا ساخته</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>بخ بخ ای دین محمد کز کمال رفعتت</p></div>
<div class="m2"><p>استوار اجمال ابرار و اجلا ساخته</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>خه خه ای اعزاز اهل البیت خیرالمرسلین</p></div>
<div class="m2"><p>کز خطا در حس ایزدتان مبرا ساخته</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>«یا شفیع المذنبین » در ظل احسانم درآر</p></div>
<div class="m2"><p>شرمسارم پیش حق زل و خطایا ساخته</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>طبع عطشان «نظیری » را صلت بر نعت تست</p></div>
<div class="m2"><p>خود زبان کوتاه از عرض تمنا ساخته</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>اقتدار توبه و اشگ سحرگاهیش ده</p></div>
<div class="m2"><p>تا کند در جنب «هم مستغفرین » جا ساخته</p></div></div>