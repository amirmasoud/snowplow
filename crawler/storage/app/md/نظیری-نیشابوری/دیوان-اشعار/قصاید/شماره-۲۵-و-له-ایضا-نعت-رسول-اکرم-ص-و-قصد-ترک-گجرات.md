---
title: >-
    شمارهٔ ۲۵ - و له ایضا نعت رسول اکرم (ص) و قصد ترک گجرات
---
# شمارهٔ ۲۵ - و له ایضا نعت رسول اکرم (ص) و قصد ترک گجرات

<div class="b" id="bn1"><div class="m1"><p>هر شب بذیل صحبت جانان تن آورم</p></div>
<div class="m2"><p>وز دامنش نثار به دامن درآورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون روم ز ارض جسد در سمای روح</p></div>
<div class="m2"><p>وحی مبین و کشف مبرهن درآورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انشا کنم به منطق سیمرغ راز غیب</p></div>
<div class="m2"><p>شورش به طایران نوازن درآورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیلی لباس و سینه فروزان چو کرم شب</p></div>
<div class="m2"><p>در صحن لفظ معنی روشن درآورم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش زبان شمع بخاید چون من به حرف</p></div>
<div class="m2"><p>کلک زبان بریده الکن درآورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از مد عنبرین که کشم صبح اولین</p></div>
<div class="m2"><p>اشهب به روی عنبر لادن درآورم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر برزند چو صبح ز دیوار بوستان</p></div>
<div class="m2"><p>کلک و ورق گرفته به گلشن درآورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگه کنم نظاره مرآت گل به نظم</p></div>
<div class="m2"><p>صد معنی از خموشی سوسن درآورم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون حسن ارغوان نگرم از سرشگ گرم</p></div>
<div class="m2"><p>خون در رگ فسرده روین درآورم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر گوهری که از شب آبستنم بزاد</p></div>
<div class="m2"><p>روزش به عقد بخت سترون درآورم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر معنیی بربط سخن توسنی کند</p></div>
<div class="m2"><p>سختش لگام بر سر توسن درآورم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر صحن چارباغ جهان افکنم سماط</p></div>
<div class="m2"><p>از هشت خلد خوان ملون درآورم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گردد بحل کار، فرومانده بس که من</p></div>
<div class="m2"><p>لعل گران به تیشه کان کن درآورم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طبعم شکفته از طرف کس نمی شود</p></div>
<div class="m2"><p>تا کی دقیقه های مبین درآورم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر روز گوی برده به دعوی ز آفتاب</p></div>
<div class="m2"><p>بر زین نشسته شور به برزن درآورم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زین زال فتنه جوی کشم کینه قدیم</p></div>
<div class="m2"><p>غارت به خان و مانش چو بهمن درآورم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از دیک سینه جوش برآرم به سوز دل</p></div>
<div class="m2"><p>گردون به رقص همچو نهنبن درآورم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دوران هنوز خون سیاوش نکرده پاک</p></div>
<div class="m2"><p>سهراب را به حرب تهمتن درآورم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صد نوحه جیب و سینه درد در درون و من</p></div>
<div class="m2"><p>از خنده پرده بر رخ شیون درآورم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صد تفته سوزنم به جگر هست و آب نیست</p></div>
<div class="m2"><p>چندان که نم به چشمه سوزن درآورم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از بس گشاد تیر دهد شست روزگار</p></div>
<div class="m2"><p>نگذاردم که دست به جوشن درآورم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صد زخم دل گزا خورم از دست ناکسان</p></div>
<div class="m2"><p>تا لقمه ای به کام چو هاون درآورم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گوهر به مشت می دهم و نیست ناخنم</p></div>
<div class="m2"><p>چندان که یک خراش به معدن درآورم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دستم نمی رسد که برآرم ز آستین</p></div>
<div class="m2"><p>پایم نمی دهد که به دامن درآورم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باید هزار دور کند آسمان که من</p></div>
<div class="m2"><p>یک بار آفتاب به روزن درآورم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>امید عیش نیست درین چاه غم مگر</p></div>
<div class="m2"><p>بیژن شوم که مرغ مثمن درآورم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با خصم سخت روترم از تیغ و دوست او</p></div>
<div class="m2"><p>آیینه ام که در دل آهن درآورم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گردون اگر بیازش دستی کرم کند</p></div>
<div class="m2"><p>گل مهره زمین به فلاخن درآورم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>افشای راز بت نه صلاح است ورنه من</p></div>
<div class="m2"><p>صد پارسا به کیش برهمن درآورم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن گفتگو به خاک مذلت فتاده باد</p></div>
<div class="m2"><p>کز بهر مصلحت حیل و فن درآورم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شک نارسانده دست به چوگان اعتقاد</p></div>
<div class="m2"><p>گوی یقین به حال گه ظن درآورم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مگر زمانه پرده یوسف دریده است</p></div>
<div class="m2"><p>کی دل به دست عشوه این زن درآورم؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پاک از جهان روم که مسیح مجردم</p></div>
<div class="m2"><p>قارون نیم که گنج به مدفن درآورم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر توسن زمانه کسی ره به سر نبرد</p></div>
<div class="m2"><p>من زیرکم که پای به کودن درآورم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ترسم چنان ز حاصل دوران که دانه را</p></div>
<div class="m2"><p>چون مورچه شکسته به مکمن درآورم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>قسمت رسیدنیست ز احسان هر که هست</p></div>
<div class="m2"><p>دست از چه پیش رزق معین درآورم؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گل در بغل نسیم چمن می کند مرا</p></div>
<div class="m2"><p>من آن نیم که دست به چیدن درآورم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر نخل شعله هر شرری بلبلی شود</p></div>
<div class="m2"><p>گر خار خار سینه بگلخن درآورم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر بخیه بر مرقع درویش عقده ایست</p></div>
<div class="m2"><p>غفلت شود که رشته به سوزن درآورم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر حلقه کمند و خم دام فتنه ام</p></div>
<div class="m2"><p>مرغی نیم که چشم به ارزن درآورم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در وادیی که بیم خطر بیشتر بود</p></div>
<div class="m2"><p>شب نالم و به قافله رهزن درآورم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن نغمه سنج بلبل باغم که در خزان</p></div>
<div class="m2"><p>مرغان رفته را به نشیمن درآورم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>عشقت چو دست فتنه به یغما برآورد</p></div>
<div class="m2"><p>از ره بلای رفته به مأمن درآورم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از بس که پیش عشوه تو بی بهاست جان</p></div>
<div class="m2"><p>شرم آیدم که کیل به خرمن درآورم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ابر بهار حسن توام کز سرشک و آه</p></div>
<div class="m2"><p>شب های رنگ و بوی به گلشن درآورم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مغز جگر گدازم و در دیده ها کشم</p></div>
<div class="m2"><p>تا در چراغ حسن تو روغن درآورم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در همتم چو سرو به آزادگی مثل</p></div>
<div class="m2"><p>قمری نیم که طوق به گردن درآورم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عشاق همتی که ز در صبر رفته را</p></div>
<div class="m2"><p>گر درنیاورد دگری من درآورم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خورشید جرعه نوش شراب خم منست</p></div>
<div class="m2"><p>حاشا که لب به دردی هر دن درآورم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شهدی چو دوستی به قدح خوشگوار نیست</p></div>
<div class="m2"><p>کی لب به زهر کینه دشمن درآورم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نی نی که لاف می زنم و هزل می کنم</p></div>
<div class="m2"><p>کی من به امر و نهی کسی تن درآورم؟</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آن روستاییم که به غازان شه ار رسم</p></div>
<div class="m2"><p>اول زبان به گفتن «کم سن » درآورم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در ظاهرم تکبر و در باطنم هوا</p></div>
<div class="m2"><p>سنجاب در ته خزادکن درآورم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از بس بدم کفایت جرمم نمی کند</p></div>
<div class="m2"><p>تاراج اگر به رحمت ذوالمن درآورم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آن سالخورده مطرب دیرم که صبح و شام</p></div>
<div class="m2"><p>انجیل را به نغمه ارغن درآورم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>طبعم رحم فسرده شد از شهوت زنان</p></div>
<div class="m2"><p>لب بندم و عقیم بزادن درآورم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دیوان شعر کهنه بشویم ز فکر تو</p></div>
<div class="m2"><p>از نعت خواجه نظم مدون درآورم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شهری ز بیم احمد مرسل کنم بنا</p></div>
<div class="m2"><p>آفاق را به حصن محصن درآورم</p></div></div>