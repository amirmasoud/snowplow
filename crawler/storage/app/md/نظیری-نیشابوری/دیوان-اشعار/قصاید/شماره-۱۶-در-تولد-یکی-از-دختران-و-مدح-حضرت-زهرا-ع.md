---
title: >-
    شمارهٔ ۱۶ - در تولد یکی از دختران و مدح حضرت زهرا (ع)
---
# شمارهٔ ۱۶ - در تولد یکی از دختران و مدح حضرت زهرا (ع)

<div class="b" id="bn1"><div class="m1"><p>گذشت کوکبه ام از فلک که زهره برآمد</p></div>
<div class="m2"><p>زیاده گشت صفا خانه روبم از سفر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آستان ثنایم نثار یمن قدم شد</p></div>
<div class="m2"><p>سعادت و شرف مشتری که بر اثر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سزد که سلسله زرین کند چو زهره و پرچین؟</p></div>
<div class="m2"><p>بلی به طالع رودابه عقد زال زر آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمیم نفخه روح القدس شنید مشامم</p></div>
<div class="m2"><p>رسید ثانی مریم ز عیسیم خبر آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوید مایده عیسوی که مریم ما را</p></div>
<div class="m2"><p>بمهرگان ثمر نوبهار ماحضر آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آشیان خطرناک رست مرغ امیدم</p></div>
<div class="m2"><p>ز بیضه بچه برآوردم و ببال و پر آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس این نشانه شب خیزیم که طایر تسلیم</p></div>
<div class="m2"><p>ز بطن طوق بگردن چو قمری سحر آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر صحیح طراوت نبینیم عجبی نیست</p></div>
<div class="m2"><p>نماند رنگ برویم که پاره جگر آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گوشه جگرم بود آب روی تمنا</p></div>
<div class="m2"><p>شکست گونه لعلم که تیشه بر کمر آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسوی ملک فنایم روان ز کشور هستی</p></div>
<div class="m2"><p>بحد سردی و خشگی مزاج گرم و تر آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز کم رطوبتیم شاخ نخل خشگ ثمر داد</p></div>
<div class="m2"><p>چو استخوان رطب کاشتم رطب ثمر آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مدار از رحم روزگار چشم فراغت</p></div>
<div class="m2"><p>که همچو کیسه جراح پر ز نیشتر آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محیط ظلمت و نورم هزار بار نمودند</p></div>
<div class="m2"><p>ز بطن بدر آیم که دور من بسر آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تمام شد خط پرگار من بنقطه آخر</p></div>
<div class="m2"><p>ز دایره بدر آیم که دور من بسر آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نتایج ملکوتست از نتیجه طبعم</p></div>
<div class="m2"><p>مشخص است که ام النسا ز بوالبشر آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رسید حد تناسل بده هزار قریبم</p></div>
<div class="m2"><p>که هر یک از دگری در قبول پیشتر آمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کنم تلاش فزونی که بهر دانش فرزند</p></div>
<div class="m2"><p>گواه دانش جد و فضیلت پدر آمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنون بحد بلاغت رسد پیمبر وحیم</p></div>
<div class="m2"><p>که بعد سن بلوغم یک اربعین بسر آمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قدوم دختر بر فضل من چو زهره زهرا</p></div>
<div class="m2"><p>ز بعد بعثت احمد دلیل معتبر آمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو نخل قوت؟ خانه بود؟ شهدم</p></div>
<div class="m2"><p>گلم بشهد سرشتند نطفه گلشکر آمد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بخلوت حرمم زان گریخت نقطه صلبی</p></div>
<div class="m2"><p>که صدر دفتر دیوان من پر از گهر آمد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر از خزف بر من خوارتر نمود مرانش</p></div>
<div class="m2"><p>بنزد مادر خود پربهاتر از گهر آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نشاط زمزمه ماکیان و بیضه خویش است</p></div>
<div class="m2"><p>چنان که بانگ خروس از سفیده سحر آمد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حجاب دختر اول ز خانه ساخت نفورم</p></div>
<div class="m2"><p>یکی نبود بسم نور دیده دگر آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بروی این دو سعید اخترم سرود و سرور است</p></div>
<div class="m2"><p>طرب کنید که مهمان زهره و قمر آمد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خواص کحل سپاهان غبار مقدمشان داشت</p></div>
<div class="m2"><p>که دست و روی سیه کرده قوت بصر آمد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز مکر و شعبده عشق هان و هان بخبر باش</p></div>
<div class="m2"><p>که آن پری متمثل بصورت بشر آمد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز باب؟ از اصابت نظرم بود</p></div>
<div class="m2"><p>جمال من متفرق به چند باب درآمد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بهر هنر صفتم از جمال خوی نمودند</p></div>
<div class="m2"><p>یک آفتاب منیر از هزار غرفه برآمد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کنون ز عین من این مردمان عین؟</p></div>
<div class="m2"><p>که عین انسان انسان عین در نظر آمد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هزار شکر کزین نو رسیده دخت شریفم</p></div>
<div class="m2"><p>ز مغز رحمت سودای مادرش بدر آمد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز پای تا بسرم از خیال حق همه جان بود</p></div>
<div class="m2"><p>مثال او بضمیرم فتاد جانور آمد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به این قصیده برجسته شد تدارک عیبم</p></div>
<div class="m2"><p>که دختر و پسرم توأمان بیکدگر آمد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بنات نعش نهفتم به حسن نظم چو پروین</p></div>
<div class="m2"><p>به نرد خویش بنازم که بازیم قدرآمد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نبود اگرچه گوارا رسیدنش بمذاقم</p></div>
<div class="m2"><p>به بخت خویش موافق چو شیر با شکر آمد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دل از فراخی جا جمع گشت تاجورنرا</p></div>
<div class="m2"><p>ز خیل حلقه بگوشان سری به حلقه درآمد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو زهره گر بهبودطست کوکبش نزنم طعن</p></div>
<div class="m2"><p>برادران عقب را چراغ رهگذر آمد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فروغ دوستی آلش از جمال هویداست</p></div>
<div class="m2"><p>خصال جد و پدر از شمایلش سیر آمد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بمهر فاطمه روشن دل آن نشد به چه معنی</p></div>
<div class="m2"><p>به حسن ماده تقویم احسن الصور آمد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گل حدیقه صلب رسول زهره زهرا</p></div>
<div class="m2"><p>که از صباح عروسان شکفته روی تر آمد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>لطیفه «انا املح » که از جمال صبیحش</p></div>
<div class="m2"><p>کلاله خم گیسو چو هاله قمر آمد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کشد جماعت نسوان امت از ته دوزخ</p></div>
<div class="m2"><p>بآن دو زلف چو «حبل المتین » که با کمر آمد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنان رسید به معراج حد حسن جمالش</p></div>
<div class="m2"><p>که قاب قوسین از ابروانش درنظر آمد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز آب و گل شجری بردمید باغ نبی را</p></div>
<div class="m2"><p>که اصل جمله سادات فرع آن شجر آمد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هزار شاخچه شد سبز زان درخت برومند</p></div>
<div class="m2"><p>که هر کدام برفعت سپهر سایه ور آمد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زدند تیشه کاوش به کان فطرت آدم</p></div>
<div class="m2"><p>نتاج ازو گهر و نسل دیگران شجر آمد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>علی سحاب و صدف فاطمه محیط نبی شد</p></div>
<div class="m2"><p>که چون حسین و حسن شان دو قیمتی گهر آمد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو می کند رخ خاتون خلد زیور و افسر</p></div>
<div class="m2"><p>علیش با دو گهر گوشوار تاج سر آمد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سرادق ملکوتش به این جهان بکشیدند</p></div>
<div class="m2"><p>که دستگاه مهین بود و عرصه مختصر آمد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گذاشتند بخلد برین اثاثیه بیتش</p></div>
<div class="m2"><p>که زود جانب منزل تواند از سفر آمد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>لوای مجد به عرش مجید و قبه خضرا</p></div>
<div class="m2"><p>جهان به کلبه و پشمینه ای برو بسر آمد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شتاب داشت که پیمان بحق درست رساند</p></div>
<div class="m2"><p>گذاشت رخت به منزل که در سفر خطر آمد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عوض به مسکنت و فقر کرده جاه جهان را</p></div>
<div class="m2"><p>که فقر لازم شخصست و جاه بر گذر آمد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یقین بدان که ولای علی و مذهب آلش</p></div>
<div class="m2"><p>طریقه ایست که او از صراط راست تر آمد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>درست گشت که آل بتول آل رسولست</p></div>
<div class="m2"><p>گهی که زیر عبا با علی و آل برآمد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>حدیث اول نوری که بی خلاف درستست</p></div>
<div class="m2"><p>روایتی است که از راویان معتبر آمد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دگر که خلقت حیدر ز نور پاک رسولت</p></div>
<div class="m2"><p>بقول حق نبی از نقاب در خبر آمد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو این دو قول مطابق کنی بری ز تعصب</p></div>
<div class="m2"><p>بشارتست علی و مصطفی چو ماه و خور آمد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز شخص فاطمه کان پرده شد میانه هر دو</p></div>
<div class="m2"><p>مثال هر دو چو نور دو چشم و یک نظر آمد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کسی که تفرقه آل مرتضی و نبی کرد</p></div>
<div class="m2"><p>ز مشرکان دوبینش نگر که کج نظر آمد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>که در سیادت آل رسول شبهه بیان کرد</p></div>
<div class="m2"><p>که از فضول زبان در زبانه مستقر آمد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اگر بصورت جد گفت بایدش بسزا کشت</p></div>
<div class="m2"><p>وگر بهرزه سرائید دوزخش مقر آمد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بس است از پی الزام دفع شبهه «نظیری »</p></div>
<div class="m2"><p>بیان این دو سه مصرع که لب و مختصر آمد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همیشه تا به سپهر کمال و فضل هویداست</p></div>
<div class="m2"><p>که آفتاب محمد شد و علی قمر آمد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>موالیان موحد بر اوج رفعت و دولت</p></div>
<div class="m2"><p>زنند طبل که بر مشرکان دین خطر آمد</p></div></div>