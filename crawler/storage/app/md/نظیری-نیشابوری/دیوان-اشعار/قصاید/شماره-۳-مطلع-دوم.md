---
title: >-
    شمارهٔ ۳ - مطلع دوم
---
# شمارهٔ ۳ - مطلع دوم

<div class="b" id="bn1"><div class="m1"><p>ای خاک درت صندل سرگشته سران را</p></div>
<div class="m2"><p>بادا مژه جاروب رهت تاجوران را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشاطه سیمای رخ خلق ز زینت</p></div>
<div class="m2"><p>از آب و گلت غالیه رخسار جهان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر درگه تو فتنه چین و رخ خوبان</p></div>
<div class="m2"><p>بر صحن تو عاشق سر و افسر ملکان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویا شده ازشادی دیوار حریمت</p></div>
<div class="m2"><p>هر نقش که یاد آمده نقاش گمان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شکل صنم بر تو رقم کرده مصور</p></div>
<div class="m2"><p>بگشاد زمین بوس تواش مهر دهان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور صورت رضوان به سوی خلد کشیده</p></div>
<div class="m2"><p>برتافته از ذوق ریاض تو عنان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر طفل نبسته صور از خامه بزاید</p></div>
<div class="m2"><p>خواهد ز صریر درت آموخت زبان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد مرتبه خورشید فلک بهر اقامت</p></div>
<div class="m2"><p>در سایه ات فتاده و بگشاده میان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر روزن تو جادوییی دیده ندارد</p></div>
<div class="m2"><p>بهر چه دهد درک نظر طبع دخان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور جام تو آیینه جمشید نباشد</p></div>
<div class="m2"><p>چون عرض کند خوبی رخسار جهان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>معراجی و ره در کنف عدل تو حق را</p></div>
<div class="m2"><p>فردوسی و جا در حرم خاص تو جان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با سایه خود ساخته همسایه خدایت</p></div>
<div class="m2"><p>زان بر همه انداخته ای ظل امان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آفاق ز آیین تو در عیش و سرورند</p></div>
<div class="m2"><p>کاراستی از فر جهانگیر جهان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن شاه جوان بخت کز اندیشه ثاقب</p></div>
<div class="m2"><p>انوار زمین ساخت آثار زمان را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در حسن گل و آب ز بس کرده تصرف</p></div>
<div class="m2"><p>آراسته چون طبع نگین روی مکان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از غایت آرامش عهدش عجبی نیست</p></div>
<div class="m2"><p>ز آسودگی ار راست شود خانه کمان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تقدیر کشیدست برین طارم خضرا</p></div>
<div class="m2"><p>بر کهگل نامش رقمی کاهکشان را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای ملک خدایی که در ابداع صنایع</p></div>
<div class="m2"><p>در خاک نهد خاطر معمار تو جان را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نیرنگ خیال تو جهان غالیه گون کرد</p></div>
<div class="m2"><p>زان سان که عروسان جوان غالیه دان را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر خانه که شد در کنف عدل تو آباد</p></div>
<div class="m2"><p>ره نیست درآن خانه نزول حدثان را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کوی تو نظرگاه خدا دید «نظیری »</p></div>
<div class="m2"><p>ره در حرم حق نبود هون و هوان را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در منفعت خلق جهان کوش که بخشد</p></div>
<div class="m2"><p>حق عمر دراز آدمی نفع رسان را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بنشین و به عشرت گذران تا ابدالدهر</p></div>
<div class="m2"><p>این دولت و این مکنت و این ملک و مکان را</p></div></div>