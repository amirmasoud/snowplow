---
title: >-
    شمارهٔ ۳۶ - مطلع سیوم در صفت شعر
---
# شمارهٔ ۳۶ - مطلع سیوم در صفت شعر

<div class="b" id="bn1"><div class="m1"><p>شعر مسیح دلست معنی او جان او</p></div>
<div class="m2"><p>چاشنی عاشقی شربت دکان او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوهری از شعر نیست راست نماینده تر</p></div>
<div class="m2"><p>آینه فهم هاست نکته پنهان او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه به جولان فهم پی به سخن برده اند</p></div>
<div class="m2"><p>گرد سخن گشته اند قافیه سنجان او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه سخن نقطه ایست از سر پرگار طبع</p></div>
<div class="m2"><p>هست به وسعت برون از خط دوران او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل وحی اندکی اوج فراتر گرفت</p></div>
<div class="m2"><p>ورنه ز یک پرده اند این من و آن او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به خیال دگر از سخن افتاده اند</p></div>
<div class="m2"><p>بحر غلط کرده اند قافیه سنجان او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نسخه شعر مرا گر به عطارد برند</p></div>
<div class="m2"><p>مقطع شعرم شود مطلع دیوان او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکته مستانه ام گر بسراید ادیب</p></div>
<div class="m2"><p>لوح و قلم بشکند طفل دبستان او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باد که در بوستان عطرفروشی کند</p></div>
<div class="m2"><p>بر ورقم سوده است گوشه دامان او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکس ازاین بارگاه خواهش خود می گرفت</p></div>
<div class="m2"><p>ذائقه من شناخت چاشنی خوان او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اهل سخن ناخنی بر دل هم می زدند</p></div>
<div class="m2"><p>زخم مرا خوش نمود گرد نمکدان او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وسوسه خاطرم آب سخن تیره داشت</p></div>
<div class="m2"><p>شکر کنون روشنست چشمه حیوان او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرچه به پیمانه ام زهر کند روزگار</p></div>
<div class="m2"><p>شهدفروشی کنم بر در دکان او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقل چهل سال چنگ در جگر خاره زد</p></div>
<div class="m2"><p>نقب کنون خورده است بر گهر کان او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با صدفم ابر جود فیض هنر دیده است</p></div>
<div class="m2"><p>باز نمی ایستد ژاله نیسان او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور چه به خون ریزیم دار زند آسمان</p></div>
<div class="m2"><p>مدح سرایی کنم در ته زندان او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چرخ که زخمم زند نیست ز نقصان من</p></div>
<div class="m2"><p>گوی سخن برده ام از خم چوگان او</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دهر که خصمم شود کی ز قصور منست</p></div>
<div class="m2"><p>عرض هنر کرده ام بر سر میدان او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سعدی و سعدش که اند من که سخن آورم</p></div>
<div class="m2"><p>غیرت خاقانیست حسرت خاقان او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاهد طبع مرا نعت برازنده است</p></div>
<div class="m2"><p>پیرهن مصطفاست بر قد حسان او</p></div></div>