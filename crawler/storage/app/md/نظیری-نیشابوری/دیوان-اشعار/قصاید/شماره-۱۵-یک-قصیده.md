---
title: >-
    شمارهٔ ۱۵ - یک قصیده
---
# شمارهٔ ۱۵ - یک قصیده

<div class="b" id="bn1"><div class="m1"><p>هرچه در معرض فنا باشد</p></div>
<div class="m2"><p>دل درو بستن از خطا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مال دنیا تمام عاریت است</p></div>
<div class="m2"><p>عاریت را بقا کجا باشد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیک این عاریت که می گویم</p></div>
<div class="m2"><p>دست افزار کارها باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه در خیر کار فرمایی</p></div>
<div class="m2"><p>نفع آن کار مر تو را باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور به غفلت معطلش داری</p></div>
<div class="m2"><p>رود و غفلت از قفا باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مال دنیا چو سیل درگذرست</p></div>
<div class="m2"><p>سیل در خانه کی روا باشد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر به صحرا و کشت زارش ده</p></div>
<div class="m2"><p>تا تو را حاصل و نما باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچه ده در عوض بود یک را</p></div>
<div class="m2"><p>حسرت از رفتنش چرا باشد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخل و همت چو بوی نیک، بد است</p></div>
<div class="m2"><p>شهرتش در کف صبا باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خوشی بو ز ناف ها آرد</p></div>
<div class="m2"><p>گرچه در چین و در خطا باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به بدی بو برد ز مزبله ها</p></div>
<div class="m2"><p>گرچه در خانه و سرا باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آدمی را به قدر همت و بخل</p></div>
<div class="m2"><p>همه جا قیمت و بها باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین کریمان اگرچه سایل را</p></div>
<div class="m2"><p>خاک در دیده توتیا باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرد را همت بلندخوش است</p></div>
<div class="m2"><p>گر شهنشه اگر گدا باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هفت دریا به جنب خاطر من</p></div>
<div class="m2"><p>ژاله در کام اژدها باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سایلان کفم که هرچه دهد</p></div>
<div class="m2"><p>کمش افزون ز مدعا باشد</p></div></div>