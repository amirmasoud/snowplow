---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>هردم که نه با توام برآید مرضیست</p></div>
<div class="m2"><p>مقصود تویی حقیقت من غرضیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس در عجبم که مانده ام بی تو چسان</p></div>
<div class="m2"><p>قایم چو به جوهریست هر جا عرضیست</p></div></div>