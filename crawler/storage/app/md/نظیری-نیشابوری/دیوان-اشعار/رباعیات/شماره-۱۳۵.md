---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>یک دم دل شاد را به صد دم ندهی</p></div>
<div class="m2"><p>واندم به دم عیسی مریم ندهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغان ضمیر از صفا رام شوند</p></div>
<div class="m2"><p>جمع آی که از تفرقه شان رم ندهی</p></div></div>