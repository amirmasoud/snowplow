---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>طی املم به جهد و پرواز نشد</p></div>
<div class="m2"><p>کارم به طواف و سعی دلساز نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند که بر کعبه تنیدم زنار</p></div>
<div class="m2"><p>این رشته کفرم از میان باز نشد</p></div></div>