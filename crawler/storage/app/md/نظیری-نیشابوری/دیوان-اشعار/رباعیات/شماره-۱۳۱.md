---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>گر در شب تیره تابیم نور شوی</p></div>
<div class="m2"><p>ور در دم مردن آییم سور شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دیده من اگر ببینی رخ خویش</p></div>
<div class="m2"><p>ترسم که به حسن خویش مغرور شوی</p></div></div>