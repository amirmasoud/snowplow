---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>دایم به رهت در دو چشمم بازست</p></div>
<div class="m2"><p>صد تحفه ام از نیاز پااندازست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش آی و به مهر دانه عشوه بریز</p></div>
<div class="m2"><p>پر تیز مرو که روح در پروازست</p></div></div>