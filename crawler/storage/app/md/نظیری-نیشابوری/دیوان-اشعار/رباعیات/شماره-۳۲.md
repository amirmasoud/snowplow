---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>ذرات دو کون را ز هم بیشی نیست</p></div>
<div class="m2"><p>کس نیست که با دگر کسش خویشی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رتبه مساوات بود عالم را</p></div>
<div class="m2"><p>در دایره هیچ نقطه را پیشی نیست</p></div></div>