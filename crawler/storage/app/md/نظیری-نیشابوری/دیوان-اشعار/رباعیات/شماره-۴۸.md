---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>نی حوصله ام که جرعه مستم دارد</p></div>
<div class="m2"><p>نی حرص که فاقه زیر دستم دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من همت مطلقم که مستی و خمار</p></div>
<div class="m2"><p>می نتواند بلند و پستم دارد</p></div></div>