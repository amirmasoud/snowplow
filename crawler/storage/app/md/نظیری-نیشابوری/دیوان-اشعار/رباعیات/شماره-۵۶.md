---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>آن عیش که جمله عیش ها راست کلید</p></div>
<div class="m2"><p>نابوده و نارسیده صد بار رمید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برقیست که تا نمود دیدار گذشت</p></div>
<div class="m2"><p>مرغیست که تا نشست بر بام پرید</p></div></div>