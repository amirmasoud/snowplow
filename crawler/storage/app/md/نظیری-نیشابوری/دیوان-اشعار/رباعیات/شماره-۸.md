---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>افتاده کلاه از سر و ناموسم نیست</p></div>
<div class="m2"><p>در بتکده دستانم و ناقوسم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عزت و ناز رفت و گر خلوت و ساز</p></div>
<div class="m2"><p>بر هرچه بجز تو رفت افسوسم نیست</p></div></div>