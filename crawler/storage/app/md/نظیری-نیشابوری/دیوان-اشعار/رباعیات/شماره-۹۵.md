---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>اندر ره و بی ره همه جا گردیدیم</p></div>
<div class="m2"><p>با مؤمن و کافر آشنا گردیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتخانه و کعبه ام به مقصد نرساند</p></div>
<div class="m2"><p>ره پای و سری نداشت و اگر دیدیم</p></div></div>