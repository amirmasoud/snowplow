---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>در هجر تو مرگ همنشینم بادا</p></div>
<div class="m2"><p>منظور دو دیده آستینم بادا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بی تو به کام دل برآرم نفسی</p></div>
<div class="m2"><p>یارب نفس بازپسینم بادا</p></div></div>