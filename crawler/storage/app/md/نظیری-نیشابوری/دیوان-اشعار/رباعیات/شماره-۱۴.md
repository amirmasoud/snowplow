---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>برخیز که عشق از گرانان سیرست</p></div>
<div class="m2"><p>درتاز که پروانه درین صف شیرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با عشق بقای جاودانی بخشد</p></div>
<div class="m2"><p>گر چشمه خضر اگر دم شمشیرست</p></div></div>