---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>با موکب شه دل خرابی چه کند؟</p></div>
<div class="m2"><p>با قافله تشنه سرابی چه کند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من مفلس و مست حسن سر تا قدمت</p></div>
<div class="m2"><p>با میکده ای دل کبابی چه کند؟</p></div></div>