---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>هر دم عمل خیر تبه خواهم کرد</p></div>
<div class="m2"><p>دایم به خطا نامه سیه خواهم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از معصیتم فزون شود مغفرتش</p></div>
<div class="m2"><p>هرچند عطا کند گنه خواهم کرد</p></div></div>