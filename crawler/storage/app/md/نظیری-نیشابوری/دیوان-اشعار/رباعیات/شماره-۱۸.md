---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>درد سر و گردن تو حرز تن تست</p></div>
<div class="m2"><p>اندوه تو صیقل دل روشن تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ددسرس کشیده ای نیست عجب</p></div>
<div class="m2"><p>ناموش زمانه بر سر و گردن تست</p></div></div>