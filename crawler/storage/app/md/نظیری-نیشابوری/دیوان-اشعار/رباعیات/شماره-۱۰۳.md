---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>گر دوری ازان طرف گلستان دارم</p></div>
<div class="m2"><p>از هر چمنش هزاردستان دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زافسانه مستی گل و بلبل او</p></div>
<div class="m2"><p>هر گوشه که محفلیست مستان دارم</p></div></div>