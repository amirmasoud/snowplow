---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>بدگوییی خود کنم مگر یار شوی</p></div>
<div class="m2"><p>بستانمت از تو تا وفادار شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک رنگان را ظریف طبعان نخرند</p></div>
<div class="m2"><p>رنگی دگر آیم که خریدار شوی</p></div></div>