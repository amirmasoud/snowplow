---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>از دوست منادیست اندر رگ و پوست</p></div>
<div class="m2"><p>کان می بردت به جانب کعبه دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لبیک تو پاسخ ندا کردن نیست</p></div>
<div class="m2"><p>پنهان طلبی اگر نه از جانب اوست</p></div></div>