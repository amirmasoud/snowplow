---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>رو جانب حق از همه سو باید بود</p></div>
<div class="m2"><p>در کوشش نفی مو به مو باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر ظهور تو نهان گشته تو را</p></div>
<div class="m2"><p>در ستر خود و ظهور او باید بود</p></div></div>