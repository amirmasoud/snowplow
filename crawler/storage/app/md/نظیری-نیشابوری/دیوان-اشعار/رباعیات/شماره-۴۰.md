---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>هر کز سر کجروی به درگاه آید</p></div>
<div class="m2"><p>هرچند به گاه آمده بیگاه آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از معجزه بیوه که در راه آید</p></div>
<div class="m2"><p>دستار هزار کرد کوتاه برآید</p></div></div>