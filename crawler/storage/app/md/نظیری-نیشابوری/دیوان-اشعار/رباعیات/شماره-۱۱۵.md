---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>فرمان تو آمد و ز جا برجستم</p></div>
<div class="m2"><p>می خواندم و اسباب سفر می بستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان دم که گرفت این بشارت دستم</p></div>
<div class="m2"><p>برخاستم و دگر ز پا ننشستم</p></div></div>