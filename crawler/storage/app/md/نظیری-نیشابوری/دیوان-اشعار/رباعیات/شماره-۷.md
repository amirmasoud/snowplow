---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>چون شمع تنت ز آتش محفل گرمست</p></div>
<div class="m2"><p>بر دیده نشین که خانه دل گرمست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قربان شومت این تب بیماری نیست</p></div>
<div class="m2"><p>گویا تنت از هوای منزل گرمست</p></div></div>