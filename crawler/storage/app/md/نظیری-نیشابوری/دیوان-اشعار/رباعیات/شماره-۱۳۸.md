---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>هر سو به تن از زخم مهی کاسته ای</p></div>
<div class="m2"><p>یا صفحه گل از رقم آراسته ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شعله آتشین که از خوردن چوب</p></div>
<div class="m2"><p>سرکش تر و خانه سوزتر خاسته ای</p></div></div>