---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>جستم ز بلا بلا پناهم دادند</p></div>
<div class="m2"><p>در قلب جفا گریزگاهم دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بستند ره نجاتم از هر طرفی</p></div>
<div class="m2"><p>وآنگه به سر کوی تو راهم دادند</p></div></div>