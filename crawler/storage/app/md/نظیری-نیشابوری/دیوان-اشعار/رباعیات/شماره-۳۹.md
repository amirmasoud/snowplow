---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>ارباب زمانه آفت دل باشند</p></div>
<div class="m2"><p>چون موج سراب نقش باطل باشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این کهنه سفینه های از کار شده</p></div>
<div class="m2"><p>خوبست جنازه های ساحل باشند</p></div></div>