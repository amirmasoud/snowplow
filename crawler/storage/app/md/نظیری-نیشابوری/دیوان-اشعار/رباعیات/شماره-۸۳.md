---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>جان را به پذیره آی از جا بردار</p></div>
<div class="m2"><p>زان پیش که شوقم بکشد پا بردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و دل و دیده ارمغان آورده</p></div>
<div class="m2"><p>آیینه ای از بهر تماشا بردار</p></div></div>