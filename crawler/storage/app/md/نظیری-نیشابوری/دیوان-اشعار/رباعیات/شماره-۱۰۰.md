---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>آتش خورم و زحمت جانان نشوم</p></div>
<div class="m2"><p>پروانه شمعم مگس خوان نشوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خنده ساقی گزک مستانم</p></div>
<div class="m2"><p>جز بر جگر کباب مهمان نشوم</p></div></div>