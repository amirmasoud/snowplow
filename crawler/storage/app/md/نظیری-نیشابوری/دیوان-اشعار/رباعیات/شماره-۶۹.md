---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>هرکس ز خودی و خودستایی گردید</p></div>
<div class="m2"><p>از کبر گذشت و کبریایی گردید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر مرد که سر به حلقه عجز سپرد</p></div>
<div class="m2"><p>سرحلقه مردان خدایی گردید</p></div></div>