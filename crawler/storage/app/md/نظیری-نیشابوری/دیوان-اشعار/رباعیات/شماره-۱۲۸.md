---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>ذوق آمده رخت خوش دلی بر در نه</p></div>
<div class="m2"><p>بنما در ناب و داغ بر کوثر نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان خنده شیرین و لب گلناری</p></div>
<div class="m2"><p>لخت جگری بخای و بر اخگر نه</p></div></div>