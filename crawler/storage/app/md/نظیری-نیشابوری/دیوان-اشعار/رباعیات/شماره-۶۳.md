---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>قابل تویی از تاب و توانت دادند</p></div>
<div class="m2"><p>ناقص تو اگر ضعف و هوانت دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردند نظر به قدر استعدادت</p></div>
<div class="m2"><p>هرچیز که خواستی همانت دادند</p></div></div>