---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>صد نعل درست واژگون آوردم</p></div>
<div class="m2"><p>صد موسی و خضر رهنمون آوردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا سرچشمه ای نشان می دادند</p></div>
<div class="m2"><p>بردم عطش و سبوی خون آوردم</p></div></div>