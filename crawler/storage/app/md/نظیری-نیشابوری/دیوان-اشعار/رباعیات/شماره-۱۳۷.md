---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>چندان که به حکمت گروی دورتری</p></div>
<div class="m2"><p>تا می شمری نجوم بی نورتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کور که تو راه ازو می پرسی</p></div>
<div class="m2"><p>او می داند که تو ازو کورتری</p></div></div>