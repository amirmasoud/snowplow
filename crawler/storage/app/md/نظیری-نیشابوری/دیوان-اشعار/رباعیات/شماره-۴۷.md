---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>طوفان شد و سبزه ام به آبی نرسد</p></div>
<div class="m2"><p>کشتم به ترشح سحابی نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوزد عطشم اگرچه از سایه ابر</p></div>
<div class="m2"><p>هرگز قدمم به آفتابی نرسد</p></div></div>