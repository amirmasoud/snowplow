---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>پر می بینم که بینوا می آیم</p></div>
<div class="m2"><p>بیگانه ز خویش و آشنا می آیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش چو رفتنم ندانم به کجاست</p></div>
<div class="m2"><p>می دانستم که از کجا می آیم؟</p></div></div>