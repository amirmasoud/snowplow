---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>کی شنبه من به شادمانی برخاست</p></div>
<div class="m2"><p>یا جمعه من بزم نشاطی آراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوبست که ایام برافتد ورنه</p></div>
<div class="m2"><p>تا شنبه و جمعه هست این غم برخاست</p></div></div>