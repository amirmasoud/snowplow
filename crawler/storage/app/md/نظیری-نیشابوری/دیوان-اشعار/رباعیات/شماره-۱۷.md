---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>پیرایه حسن تندی خو بشکست</p></div>
<div class="m2"><p>تیر مژه در کمان ابرو بشکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر قصد دلم خدنگ بی باکی را</p></div>
<div class="m2"><p>چندان بکشید پر که بازو بشکست</p></div></div>