---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>ای بلبل بی قرار غوغا بردار</p></div>
<div class="m2"><p>بخروش و گره از دل شیدا بردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوبی بهار شد به افسوس بنال</p></div>
<div class="m2"><p>گل می رود از قفاش آوا بردار</p></div></div>