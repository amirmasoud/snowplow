---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>بنمای رخ و شکفته رویم گردان</p></div>
<div class="m2"><p>بگشای لب و فرشته خویم گردان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان خط که چراگاه غزال نظرست</p></div>
<div class="m2"><p>بردار نقاب و مشگ بویم گردان</p></div></div>