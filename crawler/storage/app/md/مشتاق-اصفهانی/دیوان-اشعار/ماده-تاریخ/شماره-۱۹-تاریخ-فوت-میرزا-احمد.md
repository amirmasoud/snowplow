---
title: >-
    شمارهٔ ۱۹ - تاریخ فوت میرزا احمد
---
# شمارهٔ ۱۹ - تاریخ فوت میرزا احمد

<div class="b" id="bn1"><div class="m1"><p>دریغ و درد که از محفل جهان چون شمع</p></div>
<div class="m2"><p>بچشم اشک‌فشان رفت میرزااحمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شوق صحبت روحانیان ز عالم تن</p></div>
<div class="m2"><p>بسوی عالم جان رفت میرزااحمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازاین چمن شد و مرغان بناله می‌گویند</p></div>
<div class="m2"><p>فغان که رفت و جوان رفت میرزااحمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو زد بهم پروبال و بآشیانه قدس</p></div>
<div class="m2"><p>ز دامگاه جهان رفت میرزااحمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوشت ازپی تاریخ رحلتش مشتاق</p></div>
<div class="m2"><p>که از جهان بجنان رفت میرزااحمد</p></div></div>