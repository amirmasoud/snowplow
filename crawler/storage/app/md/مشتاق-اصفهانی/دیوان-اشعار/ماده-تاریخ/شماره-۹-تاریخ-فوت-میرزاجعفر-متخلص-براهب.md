---
title: >-
    شمارهٔ ۹ - تاریخ فوت میرزاجعفر متخلص براهب
---
# شمارهٔ ۹ - تاریخ فوت میرزاجعفر متخلص براهب

<div class="b" id="bn1"><div class="m1"><p>راهب که ز داغ رحلت او</p></div>
<div class="m2"><p>خونابه ز چشم دوستان رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرواز گرفت مرغ روحش</p></div>
<div class="m2"><p>زین باغ بروضه جنان رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفت و زغمش فغان احباب</p></div>
<div class="m2"><p>هر دم ز زمین بر آسمان رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچند زبان آتشین داشت</p></div>
<div class="m2"><p>زین بزم بگویمت چسان رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردید ز سرد مهری دهر</p></div>
<div class="m2"><p>خاموش چو شمع از میان رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر دیدی چگونه زین باغ</p></div>
<div class="m2"><p>آن طایر طوبی آشیان رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردید ز نغمهای دلکش</p></div>
<div class="m2"><p>خاموش و زجرگ بلبلان رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشتاق برای سال فوتش</p></div>
<div class="m2"><p>تاریخ طلب بهر کران رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتش ناگاه ره‌نوردی</p></div>
<div class="m2"><p>راهب صد حیف کز جهان رفت</p></div></div>