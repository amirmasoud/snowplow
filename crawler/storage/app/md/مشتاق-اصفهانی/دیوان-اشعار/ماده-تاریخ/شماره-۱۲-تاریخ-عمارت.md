---
title: >-
    شمارهٔ ۱۲ - تاریخ عمارت
---
# شمارهٔ ۱۲ - تاریخ عمارت

<div class="b" id="bn1"><div class="m1"><p>حضرت میرزا ابوطالب</p></div>
<div class="m2"><p>ابروی سراچه ایجاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه صدخانه دل از لطفش</p></div>
<div class="m2"><p>گشت چون کعبه خلیل‌آباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه معمار قدرت آب و گلشن</p></div>
<div class="m2"><p>از نکوئی سرشته چون اجداد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه دارد نشان شمسه از او</p></div>
<div class="m2"><p>زینت ایوان خانه ایجاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ریخت رنگ عمارتی که زند</p></div>
<div class="m2"><p>از صفا طعنه بر بهشت‌آباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صباح و مسادرش از خلد</p></div>
<div class="m2"><p>بندد و واکند ز بست و گشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در تماشایش آید ار رضوان</p></div>
<div class="m2"><p>رودش گلشن بهشت از یاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرمه از گردش ار کشد یابد</p></div>
<div class="m2"><p>روشنی چشم کور مادرزاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باشد آئینه از صفا هر خشت</p></div>
<div class="m2"><p>که در او کار کرده است استاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسکه در وی چو خامه مانی</p></div>
<div class="m2"><p>کلک نقاش داد صنعت داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست سرسبز نخل تصویرش</p></div>
<div class="m2"><p>از طراوت همیشه چون شمشاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وصف گل‌های نقش دیوارش</p></div>
<div class="m2"><p>چون زبان قلم کند بنیاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچو منقار بلبلان گردد</p></div>
<div class="m2"><p>خامه بر کف لبالب از فریاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر نسیمی رود ز باغچه‌اش</p></div>
<div class="m2"><p>بوی گل را دهد ز عطر بباد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدل آن ذوق کاین نسیم بود</p></div>
<div class="m2"><p>دیده گشتی کجا ز باد مراد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من که و وصف او که تعریفش</p></div>
<div class="m2"><p>هست بیرون ز حد استعداد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وصف او نغمه‌ایست در بدنم</p></div>
<div class="m2"><p>چون زبان در دهان شمع زباد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این عمارت چو شد تمام که برد</p></div>
<div class="m2"><p>قصر شیرین ز خاطر فرهاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت مشتاق بهر تاریخش</p></div>
<div class="m2"><p>این عمارت همیشه باد آباد</p></div></div>