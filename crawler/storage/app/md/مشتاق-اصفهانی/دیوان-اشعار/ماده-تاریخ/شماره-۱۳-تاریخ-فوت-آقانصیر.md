---
title: >-
    شمارهٔ ۱۳ - تاریخ فوت آقانصیر
---
# شمارهٔ ۱۳ - تاریخ فوت آقانصیر

<div class="b" id="bn1"><div class="m1"><p>افغان ز دور چرخ که آقانصیر را</p></div>
<div class="m2"><p>آخر مقام خانه تاریک گور شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک راست تا بهشت آن نکو صفات</p></div>
<div class="m2"><p>زآنجا برهنمائی رب غفور شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داخل ازین چمن چو بعشرت سرای خلد</p></div>
<div class="m2"><p>از اشتیاق صحبت غلمان و حور شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشتاق گفت ازپی تاریخ رحلتش</p></div>
<div class="m2"><p>آقانصیر داخل دارالسرور شد</p></div></div>