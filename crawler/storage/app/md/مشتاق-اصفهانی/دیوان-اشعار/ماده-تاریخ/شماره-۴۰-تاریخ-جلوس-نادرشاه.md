---
title: >-
    شمارهٔ ۴۰ - تاریخ جلوس نادرشاه
---
# شمارهٔ ۴۰ - تاریخ جلوس نادرشاه

<div class="b" id="bn1"><div class="m1"><p>هزار شکر که آمد بهار و رفت خزان</p></div>
<div class="m2"><p>ز فیض مقدم گل شد جهان پیر جوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پرده رخ بصفایی نمود ابر بهار</p></div>
<div class="m2"><p>که هرگز آینه ناید برون زآینه دان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمید لاله و گل صدهزار رنگ ز خاک</p></div>
<div class="m2"><p>شد آشکار زمین در دل آنچه داشت نهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمین بجوش طراوت که همچو گل بر شاخ</p></div>
<div class="m2"><p>شکفته شد بسر تیر غنچه پیکان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز فیض ابر بهاری دمید سبزه ز خاک</p></div>
<div class="m2"><p>بدان صفت که خط از سبزه عذاربتان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر قدم ز دل خاک سبزه‌ای زد جوش</p></div>
<div class="m2"><p>حیات بخش‌تر آبش ز چشمه حیوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چمن ز جوش صفا شد بآن صفت کامد</p></div>
<div class="m2"><p>بدیده قطره شبنم چو گوهر غلطان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بسکه موج طراوت ازین چمن برخواست</p></div>
<div class="m2"><p>فتاد آب فلک را بجوی کاه کشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صفاپذیر چنان شد زمین ز صیقل ابر</p></div>
<div class="m2"><p>که گشت آئینه از حیرتش چوآب روان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چمن ز لاله و گل شد بآن صفت لبریز</p></div>
<div class="m2"><p>که همچو غنچه فراهم نیامدش دامان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین که جلوه موج هواست هستی بخش</p></div>
<div class="m2"><p>درین بهار چو جام سبک زرطل گران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رواج کار بجائی رسیده مستانرا</p></div>
<div class="m2"><p>که شیشه‌گر شکند محتسب دهد تاوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدان صفت که کند اقتضای باده‌کشی</p></div>
<div class="m2"><p>درین بهار ز کیفیت هواداران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عجب نباشد اگر جام می‌شود در بزم</p></div>
<div class="m2"><p>برنگ ساغر خورشید خودبخود گردان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درین حدیقه که با صد زبان نمی‌آید</p></div>
<div class="m2"><p>ز کس شماره آمد شد بهار و خزان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیامدست بهاری بدین خوشی هرگز</p></div>
<div class="m2"><p>کزو شکفته گل انتفاش پیر و جوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر غلط نکنم این سروش عیش و نشاط</p></div>
<div class="m2"><p>بود ز فیض جلوس شهنشه دوران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جناب شاه ملایک سپاه نادرشاه</p></div>
<div class="m2"><p>خدیو جم عظمت خسرو سکندرشان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شهی که پایه قدر رفع دربانش</p></div>
<div class="m2"><p>قدم گذاشته برتر از این بلند ایوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شهی که کشتی نه چرخ را بهم شکند</p></div>
<div class="m2"><p>چوآب تیغ جهان گیر او کند طوفان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شهی که ناخن مشکل‌گشای همت او</p></div>
<div class="m2"><p>هزار عقده دشوار را کند آسان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شهی که گاه گهر پاشی حساب کرم</p></div>
<div class="m2"><p>بر آب از کف جودش رود چه بحر و چه کان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شهی که هیبت او بانگ اگر زنده بر کوه</p></div>
<div class="m2"><p>بلرزه آید و گردن بسان آب روان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به تخت سلطنت این خسرو بلند اقبال</p></div>
<div class="m2"><p>که پیش رفعت جاهش خجل بود کیوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جلوس کرد بروز خجسته نوروز</p></div>
<div class="m2"><p>ز یاری فلک و نصرت خدای جهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دگر برای چه زین باغ هر کف خاکی</p></div>
<div class="m2"><p>ز خرمی نشود رشک روضه رضوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خلاصه از مدد بخت تکیه زد مشتاق</p></div>
<div class="m2"><p>چو بر سریر شهنشاهی آن رفیع مکان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نوشت خامه دو مصرع که سال تاریخش</p></div>
<div class="m2"><p>ز هر یکیش شود بی‌کم و زیاد عیان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رساند مژده شاهی بگوش اهل جهان</p></div>
<div class="m2"><p>جلوس نادر آفاق شاه جم دربان</p></div></div>