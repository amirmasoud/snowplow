---
title: >-
    شمارهٔ ۵۰ - تاریخ فوت خادم
---
# شمارهٔ ۵۰ - تاریخ فوت خادم

<div class="b" id="bn1"><div class="m1"><p>خادم که از ترانه دلکش درین چمن</p></div>
<div class="m2"><p>بودش چو غنچه گوش بر آواز هر گلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون رفت از حدیقه و از رفتنش فتاد</p></div>
<div class="m2"><p>در جرگ بلبلان نواسنج غلغلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشتاق خسته دل پی تاریخ رحلتش</p></div>
<div class="m2"><p>گفتا ز بوستان سخن رفت بلبلی</p></div></div>