---
title: >-
    شمارهٔ ۳۱ - تاریخ فوت مریم
---
# شمارهٔ ۳۱ - تاریخ فوت مریم

<div class="b" id="bn1"><div class="m1"><p>آه از چرخ جفاپیشه که دست</p></div>
<div class="m2"><p>نکشد یکنفس از جور و ستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه از گردش ایام گذاشت</p></div>
<div class="m2"><p>پا درین مرحله چون نقش قدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساختش ره سیر از دشت وجود</p></div>
<div class="m2"><p>راست چون جاده بصحرای عدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه را در چمن دهر چو سرو</p></div>
<div class="m2"><p>ساخت سر سبز ز بخت خرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو اوراق خوان آخر کار</p></div>
<div class="m2"><p>ریختش دفتر هستی از هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند بر لوح سخن از شیمش</p></div>
<div class="m2"><p>راه پیموده کنم طی چو قلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت از جور فلک در ته خاک</p></div>
<div class="m2"><p>بانوی حجله عصمت مریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باشد اولی که کنم مختصرش</p></div>
<div class="m2"><p>این سخن کاورد اندوه و الم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قد آن گلبن نو خیز که بود</p></div>
<div class="m2"><p>غیرت سرو گلستان ارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرنگون ساختش از صرصر کین</p></div>
<div class="m2"><p>فلک حادثه زا همچو علم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غرض از جور فلک چون گردید</p></div>
<div class="m2"><p>کام نادیده برون از عالم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت تاریخ وفاتش مشتاق</p></div>
<div class="m2"><p>رفت ناکام ز دنیا مریم</p></div></div>