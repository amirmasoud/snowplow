---
title: >-
    شمارهٔ ۴ - تاریخ رحلت میر عبدالغفار
---
# شمارهٔ ۴ - تاریخ رحلت میر عبدالغفار

<div class="b" id="bn1"><div class="m1"><p>حیف از میر فلک مرتبه عبدالغفار</p></div>
<div class="m2"><p>ابروی گهر نسل شه او دانا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ز عقد در سادات گرامی گهرش</p></div>
<div class="m2"><p>ناگه افتاد بخاک ره و شد ناپیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جذبه شوق ملاقات رسول مدنی</p></div>
<div class="m2"><p>شد کمندافکن او زین چمن تنگ فضا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفت ازین باغ بفردوس برین و افزود</p></div>
<div class="m2"><p>پایه‌اش از شرف خدمت جدا علی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذره‌اش اختر تابان شد از آمیزش مهر</p></div>
<div class="m2"><p>قطره‌اش گوهر رخشان ز وصال دریا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصه کوته چو شد از محفل دهر و مشتاق</p></div>
<div class="m2"><p>که بود انجمن افروز سخن شمع‌آسا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشت سرگرم دعا و زپی تاریخش</p></div>
<div class="m2"><p>گفت جایش بجنان باد ز الطاف خدا</p></div></div>