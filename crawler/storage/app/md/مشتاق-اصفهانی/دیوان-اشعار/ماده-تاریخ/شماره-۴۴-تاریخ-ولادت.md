---
title: >-
    شمارهٔ ۴۴ - تاریخ ولادت
---
# شمارهٔ ۴۴ - تاریخ ولادت

<div class="b" id="bn1"><div class="m1"><p>هزار شکر که از نوشکفته گشت گلی</p></div>
<div class="m2"><p>زابر لطف الهی ز بوستان حسن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عطا نموده باو کودکی خدا که چو شمع</p></div>
<div class="m2"><p>زپرتو رخش افروخت دودمان حسن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآفت این ثمر نارسیده ایمن باد</p></div>
<div class="m2"><p>که هست قوت دل و قوت روان حسن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جناب ایزدی از لطف نام او احمد</p></div>
<div class="m2"><p>ز تخت عرش رسانیده بر زبان حسن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو این نهال برآمد ز باغ رعنائی</p></div>
<div class="m2"><p>که جلوه‌اش بود آرام‌بخش جان حسن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوشت خامه مشتاق بهر تاریخش</p></div>
<div class="m2"><p>دمید گلبنی از طرف گلستان حسن</p></div></div>