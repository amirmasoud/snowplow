---
title: >-
    شمارهٔ ۴۹ - تاریخ طالار سعادت‌آباد شاه طهماسب صفوی
---
# شمارهٔ ۴۹ - تاریخ طالار سعادت‌آباد شاه طهماسب صفوی

<div class="b" id="bn1"><div class="m1"><p>شکر کز لطف الهی کرد تسخیر جهان</p></div>
<div class="m2"><p>همچو مهر از تیغ عالم‌گیر شاه دین‌پناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع بزم افروز دولت ماه اوج سلطنت</p></div>
<div class="m2"><p>آفتاب برج حشمت سایه لطف‌اله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وارث ملک سلیمان صاحب تخت کیان</p></div>
<div class="m2"><p>ثانی صاحب قرآن شاه جهان طهماسب شاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نافذالامری که گردون از ازل دارد بگوش</p></div>
<div class="m2"><p>حلقه بهر بندگیش از گوشوار مهر و ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوکت آئینی که گر با پله تمکین او</p></div>
<div class="m2"><p>کوه را سنجی سبکتر آید از یگبرک کاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معدلت کیشی که در ایام عدل او سزد</p></div>
<div class="m2"><p>کار شبنم زآتش سوزان اگر بیند گیاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساخت عالی منظری کز ارتفاعش دور نیست</p></div>
<div class="m2"><p>گرفتد گاه تماشای از سر گردون کلاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر فراز بام او هرکس که آید بنگرد</p></div>
<div class="m2"><p>چون مه کنعان زیستی مهر را در قعر چاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برفراز بامش ار خواهد رساند خویش را</p></div>
<div class="m2"><p>توسن اندیشه از رفتار ماند نیمه راه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه در گامی ز سرعت این سمند تیزرو</p></div>
<div class="m2"><p>از دو عالم بگذرد زانسان که از عینک نگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این بلند ایوان کیوان پایه چون اتمام یافت</p></div>
<div class="m2"><p>زاهتمام شاه گردون شوکت و انجم سپاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کلک مشتاق ازپی تاریخش این مصرع نوشت</p></div>
<div class="m2"><p>دیده بد دور زاین ایوان گردون دستگاه</p></div></div>