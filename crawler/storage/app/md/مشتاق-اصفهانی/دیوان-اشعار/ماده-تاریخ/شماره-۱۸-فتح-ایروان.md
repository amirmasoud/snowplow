---
title: >-
    شمارهٔ ۱۸ - فتح ایروان
---
# شمارهٔ ۱۸ - فتح ایروان

<div class="b" id="bn1"><div class="m1"><p>بحمدالله که از نیروی بخت و قوت طالع</p></div>
<div class="m2"><p>در آخر شامل احوال ما لطف‌الله آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عون حق کلید نصرت و مفتاح فیروزی</p></div>
<div class="m2"><p>بدست خسرو صاحب قرآن طهماسب شاه آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خراسان و عراق و شام در زیر نگین او</p></div>
<div class="m2"><p>زتیغ غازیان و لشکر و خیل سپاه آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لوای نصرت از پیش و سپاه بیکران ازپی</p></div>
<div class="m2"><p>بآذربایجان با این شکوه و فرو جاه آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدفع رومی از تبریز سوی ایروان لشگر</p></div>
<div class="m2"><p>روان کرد و خود از پی کینه جو و کینه خواه آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تیغ غازیان مشتاق چو گشتند سرتاسر</p></div>
<div class="m2"><p>فنا از ایروان رومی و از دنبال شاه آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پی تاریخ پیر عقل گفتا شد بردن رومی</p></div>
<div class="m2"><p>ز شهر ایروان و شاه گردون جایگاه آمد</p></div></div>