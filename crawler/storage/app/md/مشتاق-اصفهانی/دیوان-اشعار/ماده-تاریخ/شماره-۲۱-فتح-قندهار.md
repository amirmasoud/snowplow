---
title: >-
    شمارهٔ ۲۱ - فتح قندهار
---
# شمارهٔ ۲۱ - فتح قندهار

<div class="b" id="bn1"><div class="m1"><p>چو تیغ نادر دوران شهنشه ایران</p></div>
<div class="m2"><p>بدهر غلغله از فتح قندهار افکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برای جستن تاریخ این همایون فتح</p></div>
<div class="m2"><p>ببزم اهل سخن رهروی گذار افکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشید سر بگریبان و مطلعی مشتاق</p></div>
<div class="m2"><p>ز پشت پرده فکرت بروی کار افکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که آشکار ز هر مصرعش شود آن سال</p></div>
<div class="m2"><p>که فتح قلعه عدو را بحال زار افکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپهبدی که بجان سگان شرار افکند</p></div>
<div class="m2"><p>بهند زلزله از فتح قندهار افکند</p></div></div>