---
title: >-
    شمارهٔ ۱۰ - تاریخ نارنجستان اصفهان
---
# شمارهٔ ۱۰ - تاریخ نارنجستان اصفهان

<div class="b" id="bn1"><div class="m1"><p>آصف روشن‌دل صاحب ضمیر</p></div>
<div class="m2"><p>سید نیکو سرشت پاک زاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسندآرای وزارت آنکه هست</p></div>
<div class="m2"><p>شاه ایران را محل اعتماد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکته‌سنجانی که هر یک سفته‌اند</p></div>
<div class="m2"><p>گوهری در وصف این روشن نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درخور است ار مدح او هر دم کنند</p></div>
<div class="m2"><p>بیش از بیش و زیاد از زیاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داده این نارنج خانه کز صفا</p></div>
<div class="m2"><p>آبروی باغ جنت را بباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون سعی میرزا تعمیر شد</p></div>
<div class="m2"><p>هم به لطف حضرت رب‌العباد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خامه مشتاق کز مفتاح نطق</p></div>
<div class="m2"><p>بس در معنی بر اهل دل گشاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پی تاریخ سالش زد رقم</p></div>
<div class="m2"><p>دائم این نارنج خانه سبز یاد</p></div></div>