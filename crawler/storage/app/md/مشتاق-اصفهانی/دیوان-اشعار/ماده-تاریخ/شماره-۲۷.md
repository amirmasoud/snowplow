---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>سیدمحمد آنکه فلک از وفات او</p></div>
<div class="m2"><p>پنهان دریغ میخورد و آشکار حیف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین گلستان نچیده گلی شد برون دریغ</p></div>
<div class="m2"><p>زین بوستان نخورده بری بست بار حیف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخلش ز پا فتاد ز جور سپهر آه</p></div>
<div class="m2"><p>سروش نگون شد از ستم روزگار حیف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافل ز رشته در سادات بر زمین</p></div>
<div class="m2"><p>افتاده گم شد آن گهر ابدار حیف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>القصه از جفای فلک چون بباد رفت</p></div>
<div class="m2"><p>آن گل که بود چندیش از ساخسار حیف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشتاق گفت از پی تاریخ رحلتش</p></div>
<div class="m2"><p>زان کوکب سپهر سیادت هزار حیف</p></div></div>