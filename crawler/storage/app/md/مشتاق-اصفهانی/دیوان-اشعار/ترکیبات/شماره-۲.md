---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>باز سرگرم‌تر از رقص شرر می‌آئی</p></div>
<div class="m2"><p>شعله‌سان بر زده دامن به کمر می‌آئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرق آلوده‌تر از لاله تر می‌آئی</p></div>
<div class="m2"><p>رخ برافروخته دیگر به نظر می‌آئی</p></div></div>
<div class="b2" id="bn3"><p>از شکار دل گرم که دگر می‌آئی</p></div>
<div class="b" id="bn4"><div class="m1"><p>گل بدین رنگ ز گلزار نیاید بیرون</p></div>
<div class="m2"><p>مهر از طالع انوار نیاید بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماه از جیب شب تار نیاید بیرون</p></div>
<div class="m2"><p>از صدف گوهر شهوار نیاید بیرون</p></div></div>
<div class="b2" id="bn6"><p>به صفائی که تو از خانه به در می‌آئی</p></div>
<div class="b" id="bn7"><div class="m1"><p>محو رخسار تو ای ماه جبین نیست که نیست</p></div>
<div class="m2"><p>بنما لعل‌تر از زیر نگین نیست که نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در رهت باخته جان و دل دین نیست که نیست</p></div>
<div class="m2"><p>کشته ناز تو بر روی زمین نیست که نیست</p></div></div>
<div class="b2" id="bn9"><p>که چو خورشید تو با تیغ و سپر می‌آئی</p></div>
<div class="b" id="bn10"><div class="m1"><p>بسکه از بزم تو ای شوخ بلا حیرانم</p></div>
<div class="m2"><p>خبرم نیست که چون محو و چرا حیرانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو آئینه ندانم بکجا حیرانم</p></div>
<div class="m2"><p>این لطافت که ترا داده خدا حیرانم</p></div></div>
<div class="b2" id="bn12"><p>که چسان اهل نظر را بنظر می‌آئی</p></div>
<div class="b" id="bn13"><div class="m1"><p>چند بینم دل شوریده گرفتار ترا</p></div>
<div class="m2"><p>جان غم سوخته میخواست چه بسیار ترا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کیست کاورد بدین شوخی رفتار ترا</p></div>
<div class="m2"><p>می‌چکد آب حیات از گل رخسار ترا</p></div></div>
<div class="b2" id="bn15"><p>چشم بد دور که خوش تازه و تر می‌آئی</p></div>
<div class="b" id="bn16"><div class="m1"><p>زهره کیست بسوی تو تواند دیدن</p></div>
<div class="m2"><p>یکنظر روی نکوی تو تواند دیدن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر کرا در سرکوی تو تواند دیدن</p></div>
<div class="m2"><p>کیست گستاخ که سوی تو تواند دیدن</p></div></div>
<div class="b2" id="bn18"><p>که عرقناک ز آئینه بدر می‌آئی</p></div>
<div class="b" id="bn19"><div class="m1"><p>صبر با جان کسی لطف کلامت نگذاشت</p></div>
<div class="m2"><p>تاب در هیچ دلی زلف چو شامت نگذاشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هیچ مرغی بچمن جذبه دامت نگذاشت</p></div>
<div class="m2"><p>اثر ازدین و دل و هوش خرامت نگذاشت</p></div></div>
<div class="b2" id="bn21"><p>دگر از خانه بامید چه در می‌آئی</p></div>
<div class="b" id="bn22"><div class="m1"><p>خواستم پرتوی از روی چو ماهت گیرم</p></div>
<div class="m2"><p>پیشم آئی و سر زلف سیاهت گیرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کی توانم که در آغوش گمانت گیرم</p></div>
<div class="m2"><p>من به یک چشم کدامین سر راهت گیرم</p></div></div>
<div class="b2" id="bn24"><p>که تو در جلوه بصد راهگذر می‌آئی</p></div>
<div class="b" id="bn25"><div class="m1"><p>میکند محرمی آئینه و ناشاد ترا</p></div>
<div class="m2"><p>چشم بد دور حیا روز فزون باد ترا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرچه تر دامنیم غیر نشان داد ترا</p></div>
<div class="m2"><p>می‌رود دامن پاک صدف از یاد ترا</p></div></div>
<div class="b2" id="bn27"><p>که در آغوش من ای صاف گهر می‌آئی</p></div>
<div class="b" id="bn28"><div class="m1"><p>مکن ای گلشن خوبی ز تو با حسن و جمال</p></div>
<div class="m2"><p>منه بر روی من خسته در باغ و وصال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مردم از حسرت آغوش تو ای شوخ غزال</p></div>
<div class="m2"><p>وحشت از صحبت عشقم مکن ای تازه نهال</p></div></div>
<div class="b2" id="bn30"><p>کر ز پیوند نکوتر بثمر می‌آئی</p></div>
<div class="b" id="bn31"><div class="m1"><p>گاه جز خنده بر و برگ دگر نیست ترا</p></div>
<div class="m2"><p>از حیا گاه در آئینه نظر نیست ترا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>غیر نیرنگ و فسون کارد گر نیست ترا</p></div>
<div class="m2"><p>چه عجب عاشق یک رنگ اگر نیست ترا</p></div></div>
<div class="b2" id="bn33"><p>که تو هر دم بنظر رنگ دگر می‌آئی</p></div>
<div class="b" id="bn34"><div class="m1"><p>نوبهار است و چمن رشک چراغان گردید</p></div>
<div class="m2"><p>مشعل لاله دگر بار فروزان گردید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بلبل گلشن تصویر غزلخوان گردید</p></div>
<div class="m2"><p>ثمر از سرو و گل از بید نمایان گردید</p></div></div>
<div class="b2" id="bn36"><p>کی تو ای سرو گل اندام ببر می‌آئی</p></div>
<div class="b" id="bn37"><div class="m1"><p>از دل سوخته‌ام بوی کبابی مانده است</p></div>
<div class="m2"><p>در ته ساغر جان جرعه شرابی مانده است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>با تو در زیر لبم نیم جوانی مانده است</p></div>
<div class="m2"><p>از حیاتم نفسی پا برکابی مانده است</p></div></div>
<div class="b2" id="bn39"><p>می‌رود وقت ببالینم اگر می‌آئی</p></div>
<div class="b" id="bn40"><div class="m1"><p>غسل از آب رخ فیض چمن می‌باید</p></div>
<div class="m2"><p>بیش از بوی گل تازه کفن می‌باید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اولین گام به فردوس وطن می‌باید</p></div>
<div class="m2"><p>جان من در عوض جان کهن می‌باید</p></div></div>
<div class="b2" id="bn42"><p>هرکه را در دم آخر تو بسر می‌آئی</p></div>
<div class="b" id="bn43"><div class="m1"><p>در سر کوی تو ای ماه بخوبی شایع</p></div>
<div class="m2"><p>عمر بگذشت و نشد مهر جمالت طالع</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از کجا باز شود جان بفدایت مانع</p></div>
<div class="m2"><p>گشت خورشید جهانتاب ز مغرب طالع</p></div></div>
<div class="b2" id="bn45"><p>کی تو ای سنگدل از خانه بدر می‌آئی</p></div>
<div class="b" id="bn46"><div class="m1"><p>ای ز رخسار تو گلهای چمن در آزرم</p></div>
<div class="m2"><p>مهر در کار به قربان تو رفتن سرگرم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نشد از گرمی صهبا دل سنگین تونرم</p></div>
<div class="m2"><p>برنیاید مه رویت بلی از پرده شرم</p></div></div>
<div class="b2" id="bn48"><p>کی دگر از ته این ابر بدر می‌آئی</p></div>
<div class="b" id="bn49"><div class="m1"><p>در گلزار اگر بر رخ ما بند شود</p></div>
<div class="m2"><p>بلبل شیفته ما بچه خورسند شود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دل چرا بیهده در دام تو پابند شود</p></div>
<div class="m2"><p>بچه امید کسی از تو برومند شود</p></div></div>
<div class="b2" id="bn51"><p>نه بزاری نه بزور و نه بزر می‌آئی</p></div>
<div class="b" id="bn52"><div class="m1"><p>ای غم عشق تو هر کشور دلها والی</p></div>
<div class="m2"><p>اختر حسن تو مشهور به فرخ فالی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>صبح دیدار تو سرمایه صدخوشحالی</p></div>
<div class="m2"><p>آنقدر باش که چون نی شود از خود خالی</p></div></div>
<div class="b2" id="bn54"><p>که به آغوش من ای تنگ شکر می‌آئی</p></div>