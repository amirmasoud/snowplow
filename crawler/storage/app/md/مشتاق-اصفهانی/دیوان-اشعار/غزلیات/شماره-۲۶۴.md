---
title: >-
    شمارهٔ ۲۶۴
---
# شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>به کف پیاله به گلشن روم چسان بی‌تو</p></div>
<div class="m2"><p>چه خون چه باده چه گلخن چه گلستان بی‌تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه گلبنی تو که هر سو برد سراسیمه</p></div>
<div class="m2"><p>دلم چو طایر گم کرده آشیان بی‌تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه گل ز وصل توام بشکفد مرا که گذشت</p></div>
<div class="m2"><p>بهار عمر به بی‌برگی خزان بی‌تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا که با تو مرا زیستن دمی خوشتر</p></div>
<div class="m2"><p>هزار مرتبه از عمر جاودان بی‌تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من از غم تو کشم خون تو با حریفان می</p></div>
<div class="m2"><p>چنین تو بی‌من و من مانده‌ام چنان بی‌تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین که جوشد ازو خون چو خاروخس چه عجب</p></div>
<div class="m2"><p>به سیلم ار دهد این چشم خونفشان بی‌تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیامدی به کنارم تو و از آتش شوق</p></div>
<div class="m2"><p>چو شمع سوختم و رفتم از میان بی‌تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز نقش پا نشناسد کسم که از خواری</p></div>
<div class="m2"><p>برابرم به زمین کرده آسمان بی‌تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه داند آنکه ندیدست ترکتازی سیل</p></div>
<div class="m2"><p>به من چه می‌کند اشک سبک‌عنان بی‌تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیا ز لطف و ببین بی‌قراری مشتاق</p></div>
<div class="m2"><p>که دیگرش نبود طاقت و توان بی‌تو</p></div></div>