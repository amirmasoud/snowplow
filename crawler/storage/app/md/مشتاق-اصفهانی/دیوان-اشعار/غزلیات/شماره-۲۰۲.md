---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>گرفتم آمد آن سرو قباپوش</p></div>
<div class="m2"><p>کیم از سرکشی آید در آغوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عمری یاد کن یک ره کسی را</p></div>
<div class="m2"><p>که از یادش نه‌ای هرگز فراموش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریب زاهدان را کوچه‌ای هست</p></div>
<div class="m2"><p>که در هر گام او چاهی است خس‌پوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا خصمی به خصم آئین مردیست</p></div>
<div class="m2"><p>اگر دشمن خورد خونت بگو نوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خروشم از می وصلت عجب نیست</p></div>
<div class="m2"><p>کز آب آید سفال تشنه در جوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فغان فرماست شوق و غیرت عشق</p></div>
<div class="m2"><p>گذارد بر لب انگشتم که خاموش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جانم از شب هجران خوش آن‌دم</p></div>
<div class="m2"><p>که طالع گردد آن صبح بناگوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شدم نالان بر پیر خرابات</p></div>
<div class="m2"><p>برای شکوه از دور فلک دوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسان گوهر از گنجینه راز</p></div>
<div class="m2"><p>کشید این نکته‌ام آهسته در گوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که نتوان دم زدن ساقی حکیمست</p></div>
<div class="m2"><p>دهد گر زهر اگر تریاق می‌نوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تلاش روزی ننهاده مشتاق</p></div>
<div class="m2"><p>مکن ورنه بر و بیهوده می‌کوش</p></div></div>