---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>خون ازین غم سزد از دیده بسمل برود</p></div>
<div class="m2"><p>که بحسرت نگران باشد و قاتل برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماهم امشب سفری گشته خدایا مپسند</p></div>
<div class="m2"><p>که برآید مه و آن ماه بمحفل برود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتی ما که طلبکار شکست است چه سود</p></div>
<div class="m2"><p>که شرطه وزین ورطه بساحل برود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کر بخونم کشد از تیغ عماری‌کش یار</p></div>
<div class="m2"><p>نتوانم نروم از پی و محمل برود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماه من انجمن افروز بتان است که او</p></div>
<div class="m2"><p>چون ز محفل برود آرایش محفل برود</p></div></div>