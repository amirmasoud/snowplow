---
title: >-
    شمارهٔ ۲۳۶
---
# شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه بی‌تو دامن بر جسم و جان فشانم</p></div>
<div class="m2"><p>از خویش گرد هستی دامن فشان فشانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خاک آستانت خوش آنکه جان فشانم</p></div>
<div class="m2"><p>از شوق جان بخاک آن آستان فشانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش آنکه گرم جلوه سرو روان خود را</p></div>
<div class="m2"><p>بینم من و بپایش نقد روان فشانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بازآی از غمت چند خوناب دل ز دیده</p></div>
<div class="m2"><p>گاه آشکار ریزم گاهی نهان فشانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سازی تو گوهرافشان چون لعل لب من از شوق</p></div>
<div class="m2"><p>گوهر ز ابر چشم گوهرفشان فشانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گلشنی که باید از بیضه در قفس رفت</p></div>
<div class="m2"><p>کو فرصتی که بالی در آشیان فشانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بازآ ز دوریت چند ای گل ز اشگ خونین</p></div>
<div class="m2"><p>از دیده گاه لاله گاه ارغوان فشانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان قیمتی گهر دور مشتاق نقد کونین</p></div>
<div class="m2"><p>ریزندم ار به دامن دامن بر آن فشانم</p></div></div>