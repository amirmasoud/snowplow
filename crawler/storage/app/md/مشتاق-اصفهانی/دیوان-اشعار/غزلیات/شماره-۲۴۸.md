---
title: >-
    شمارهٔ ۲۴۸
---
# شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>کشد گر هر دمم صدبار افزون کن خدای من</p></div>
<div class="m2"><p>بمن جور و جفای او باو مهر و وفای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>براه عشق گوید پیرو من از قفای من</p></div>
<div class="m2"><p>که سرگردان‌تر است از من درین ره رهنمای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببزمت غیر و من در کنج غم یگره چه خواهد شد</p></div>
<div class="m2"><p>که من باشم بجای او و او باشد بجای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و دل کشته تیغ جفایت گوشه چشمی</p></div>
<div class="m2"><p>که آن باشد بهای خون او این خوبهای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چسان از قید عشق او رهم کاین رشته را باشد</p></div>
<div class="m2"><p>سری در دست صیاد و سری دیگر بپای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز کارم عقده هجران بیا بگشا که نتواند</p></div>
<div class="m2"><p>گشاید مشکل من جز تو کس مشکل‌گشای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بمن یار و منش نشناسم از حیرت چه حالست این</p></div>
<div class="m2"><p>که من بیگانه او باشم و او آشنای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز نخل آرزو اکنون نیم بی‌بهره کی بودی</p></div>
<div class="m2"><p>نوا زین گلبن بی‌برگ مرغ بینوای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیم شایسته تا خواهم وصالت از خدا ورنه</p></div>
<div class="m2"><p>ز کف ناجسته آید بر نشان تیر دعای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان به کز فغان مشتاق لب بندم درین وادی</p></div>
<div class="m2"><p>که هرگز نشنود محمل‌نشین بانگ‌درای من</p></div></div>