---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>مژده ای دل که شب فرقت یار آخر شد</p></div>
<div class="m2"><p>روز تاریک سرآمد شب تار آخر شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار از غیر برید الفت و با ما پیوست</p></div>
<div class="m2"><p>بود ربطی که میان گل و خار آخر شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دم سرد خزان آنچه به گلشن می‌رفت</p></div>
<div class="m2"><p>عاقبت از نفس گرم بهار آخر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صندل دردسرم شد می وصلش صد شکر</p></div>
<div class="m2"><p>محنت مستی و اندوه خمار آخر شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشت آیینه‌صفت خاک چمن عکس‌پذیر</p></div>
<div class="m2"><p>که ز صیقل‌گری ابر غبار آخر شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌رسد آنچه به ما از ستم هجر گذشت</p></div>
<div class="m2"><p>می‌کشد آنچه دل از فرقت یار آخر شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گل آن جور که بر بلبل مسکین می‌رفت</p></div>
<div class="m2"><p>عاقبت از اثر ناله زار آخر شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌کشد آن همه محنت که ز هجران مشتاق</p></div>
<div class="m2"><p>یارش از مهر چو آمد به کنار آخر شد</p></div></div>