---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>بنگاهی ز خودم بی‌خبر انداخته‌ای</p></div>
<div class="m2"><p>دل من خوش که بحالم نظر انداخته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرتو مهر توام نیست عجب کز خاکم</p></div>
<div class="m2"><p>شام برداشته‌ای و سحر انداخته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشته‌ای یار رقیب آه که بهر قتلم</p></div>
<div class="m2"><p>رنگ نوریخته طرح دگر انداخته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس جگرسوز بود داغ توزین آتش آه</p></div>
<div class="m2"><p>که من سوخته را در جگر انداخته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی ای شمع که گرد تو چو پروانه پرم</p></div>
<div class="m2"><p>آتشم بهر چه بر بال و پر انداخته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرم قتل منی و گشته فروتن برقیب</p></div>
<div class="m2"><p>تیغ برداشته‌ای و سپر انداخته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرگهر ساخته‌ای حقه یاقوت و زرشک</p></div>
<div class="m2"><p>عقدها در دل درج گهر انداخته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرده خونها بدل غیر ز غیرت نظری</p></div>
<div class="m2"><p>که بحال من خونین جگر انداخته‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>توئی آن خانه برانداز که هرجا چون برق</p></div>
<div class="m2"><p>کرده‌ای جلوه بسی خانه برانداخته‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی بمن ناو کی افکنده‌ای از جور که تو</p></div>
<div class="m2"><p>پی آن تیر نه تیر دگر انداخته‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایکه دانی هوس از عشق به افسوس که تو</p></div>
<div class="m2"><p>سنگ برداشته‌ای و گهر انداخته‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کرده مشتاق که در وادی عشقت نالان</p></div>
<div class="m2"><p>کز فغان شور درین بوم و بر انداخته‌ای</p></div></div>