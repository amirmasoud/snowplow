---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>عالم خراب از نگه می‌پرست تست</p></div>
<div class="m2"><p>زآن می فغان که در قدح چشم مست تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سررشته تپیدن دل نیست در کفم</p></div>
<div class="m2"><p>چون نبض اضطراب و سکونم به دست تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من با تو همچو شاخ درختم یکی به دار</p></div>
<div class="m2"><p>دست از شکستنم که شکستم شکست تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من در خمار حسرت یک بوسه و مدام</p></div>
<div class="m2"><p>لب جام باده را به لب می‌پرست تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشتاق از آن چه شکوه که در کوی او شدی</p></div>
<div class="m2"><p>با خاک اگر برابر از اقبال پست تست</p></div></div>