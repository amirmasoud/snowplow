---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>از صحن کعبه ساحت میخانه خوشتر است</p></div>
<div class="m2"><p>از دور سجه گردش پیمانه خوشتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب چه حکمت است که بیگانه خوی من</p></div>
<div class="m2"><p>با آشنا خوشست و به بیگانه خوشتر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سینه‌ام رود بکجا دل که جغد را</p></div>
<div class="m2"><p>از طرف باغ گوشه ویرانه خوشتر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما می‌کشان ز خوشه انگور دانه‌ای</p></div>
<div class="m2"><p>در کیش ما ز سبحه صددانه خوشتر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان بر جنون زدم که بکوی پریوشان</p></div>
<div class="m2"><p>عاشق خوشست و عاشق دیوانه خوشتر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گوشه دل از حرم و دیر فارغیم</p></div>
<div class="m2"><p>ماراست خانه‌ای که ز هر خانه خوشتر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مزرعی که قسمت برق است حاصلش</p></div>
<div class="m2"><p>در زیر خاک سوزد اگر دانه خوشتر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیوند جان ز خلق گسستم برای او</p></div>
<div class="m2"><p>کز هر که هست صحبت جانانه خوشتر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بعد از وفات یا مکش از خاک من که شمع</p></div>
<div class="m2"><p>قائم مقام تربت پروانه خوشتر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیر از حدیث عشق تو مشتاق نشنود</p></div>
<div class="m2"><p>کافسانه غمت ز هر افسانه خوشتر است</p></div></div>