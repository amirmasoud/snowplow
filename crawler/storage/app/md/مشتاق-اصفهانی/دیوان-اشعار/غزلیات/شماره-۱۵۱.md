---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>کسان مشغول کار خویش و من مشغول یار خود</p></div>
<div class="m2"><p>که خواهد آمدن تا زین میان آخر به کار خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخواهم پرتو از مهر و فروغ از مه که در عشقت</p></div>
<div class="m2"><p>خوشم با روزهای تیره و شبهای تار خود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باین شادم که نتوانم کنم گردآوری خود را</p></div>
<div class="m2"><p>بکویت گر پریشان ساختم مشت غبار خود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بی‌قدری ندارم عزتی ناخوانده مهمانم</p></div>
<div class="m2"><p>ببزم او مکرر آزمودم اعتبار خود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را با غیر دیدم کرد غیرت بیخودم ورنه</p></div>
<div class="m2"><p>به زخم کاریی میساختم زین تیغ کار خود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشم گر خاک جولانگاه او گشتم بامیدی</p></div>
<div class="m2"><p>که گردم گرد و افتم در قفای شهسوار خود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تیغش آن شهید بی‌کسم مشتاق در کویش</p></div>
<div class="m2"><p>که غیر از شمع دلسوزی ندیدم بر مزار خود</p></div></div>