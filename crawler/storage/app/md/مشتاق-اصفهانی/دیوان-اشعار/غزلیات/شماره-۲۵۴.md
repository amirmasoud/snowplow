---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>مکن از حرف دشمن بیوفا ترک وفاداران</p></div>
<div class="m2"><p>نه یار است آنکه از حرف غرض کو رنجد از یاران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارم با اسیران دگر نسبت بدان قدرم</p></div>
<div class="m2"><p>که در دامت منم سرحلقه خیل گرفتاران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدا را چاره‌ام کن کامد از دردت بلب جانم</p></div>
<div class="m2"><p>که بدنامی است از بهر طبیبان مرگ بیماران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسوزم چون بداغ شوق وصلت کافکند آتش</p></div>
<div class="m2"><p>بجان دانه لب تشنه شوق حسرت باران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیه شد همچو شب روزم ز خوبان این سزای آن</p></div>
<div class="m2"><p>که جوید پرتو مهر و وفا زین ماه رخساران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مزن لاف وفا پیمان ما مشکن مکن کاری</p></div>
<div class="m2"><p>که نه شرط وفا باشد نه آئین وفاداران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل از وصلش بکن مشتاق کان در گران قیمت</p></div>
<div class="m2"><p>به دست مفلسی کی افتد از جوش خریداران</p></div></div>