---
title: >-
    شمارهٔ ۳۰۳
---
# شمارهٔ ۳۰۳

<div class="b" id="bn1"><div class="m1"><p>رهبر باوست هر نقش از کارگاه هستی</p></div>
<div class="m2"><p>آغاز حق پرستیست انجام بت‌پرستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قانع بقطره‌ای چند از بهر بی‌نیازی</p></div>
<div class="m2"><p>همچون صدف نداریم پروای تنگدستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر مشت خاک از این دیر گاهی سبوست گه خم</p></div>
<div class="m2"><p>هست اختلاف صورت این نیستی و هستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد نامه‌ات نوشتم بهر جوابی و تو</p></div>
<div class="m2"><p>خطی نمی‌نویسی پیکی نمی‌فرستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاصل چه غیر افسوس زین عمر ما که بگذشت</p></div>
<div class="m2"><p>نیمی بخواب غفلت نیمی دگر بمستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای سست عهد با ما پیمان دوستداری</p></div>
<div class="m2"><p>بستن چه بود اول آخر چو می‌شکستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جایت کنون نباشد جز در کنار اغیار</p></div>
<div class="m2"><p>یاد آنزمان که بی‌ما جائی نمی‌نشستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پا بست باش مشتاق در آن چه ز نخدان</p></div>
<div class="m2"><p>از کف کمند زلفش بهر چه می‌گسستی</p></div></div>