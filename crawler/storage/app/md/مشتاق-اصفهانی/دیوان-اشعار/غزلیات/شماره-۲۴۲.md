---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>چون نخواهی نفسی کرد نگهداری من</p></div>
<div class="m2"><p>چیست سعی اینقدر از بهر گرفتاری من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده‌ام خوی بدردت چه کشم ناز طبیب</p></div>
<div class="m2"><p>صحتی گو نبرد از پی بیماری من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله‌ام از غم محرومی رهزن باشد</p></div>
<div class="m2"><p>ورنه کس نیست درین ره بسبکباری من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من رنجور ز دردت چو نخواهم جان برد</p></div>
<div class="m2"><p>گو کسی را نبود فکر پرستاری من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به از این باش به من گر بودت بنده هزار</p></div>
<div class="m2"><p>که تو یک بنده نداری بوفاداری من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناله زار من ای باد بگوشش مرسان</p></div>
<div class="m2"><p>ترسم آزرده شود خاطرش از زاری من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نسازم بدل زار چه سازم مشتاق</p></div>
<div class="m2"><p>دلبرم چون نکند ترک دل‌آزاری من</p></div></div>