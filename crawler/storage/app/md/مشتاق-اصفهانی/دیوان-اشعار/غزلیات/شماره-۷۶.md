---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>منم که داغ عزیزان هر دیارم سوخت</p></div>
<div class="m2"><p>فلک زآتش دوری هزار بارم سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دوریت منم آن ره‌طلب به کوی فنا</p></div>
<div class="m2"><p>که داغ حسرت شمع سر مزارم سوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو من در آتش آوارگی نسوزد کس</p></div>
<div class="m2"><p>به سنگ حسرت آسایش شرارم سوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا چه شکوه ز برق آن گیاه تشنه لبم</p></div>
<div class="m2"><p>که داغ حسرت باران نوبهارم سوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز گرمی تو به اغیار چون سپند ببین</p></div>
<div class="m2"><p>که سوخت آتش رشک و چه بی‌قرارم سوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو را نشست به دامن سزد که از تف رشک</p></div>
<div class="m2"><p>به یاد کوی تو آمیزش غبارم سوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خاک شعله زد آهم پس از وفات این است</p></div>
<div class="m2"><p>سپهر سفله چراغی که بر مزارم سوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین ریاض من آن بی‌نصیب گلچینم</p></div>
<div class="m2"><p>که دور دیدن گل‌ها به شاخسارم سوخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم به خاک تپان ماهی‌ای که دور از آب</p></div>
<div class="m2"><p>فلک در آتش هجران جویبارم سوخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیا بر آتشم از بوسه بزن آبی</p></div>
<div class="m2"><p>که داغ حسرت آن لعل آبدارم سوخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا چه شکوه ز آتش چو خاروخس مشتاق</p></div>
<div class="m2"><p>که برق جلوه آن آتشین عذارم سوخت</p></div></div>