---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>برجا دل و او مقابل ما</p></div>
<div class="m2"><p>گرد سر طاقت دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسرت بر اوست آنچه کشتیم</p></div>
<div class="m2"><p>گو برق بسوز حاصل ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز او که گشاید آنچه او بست</p></div>
<div class="m2"><p>آسان مشمار مشکل ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ننموده جمال خون ما ریخت</p></div>
<div class="m2"><p>آه از دل سنگ قاتل ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرگشته روان کوی عشقیم</p></div>
<div class="m2"><p>زنهار مپرس منزل ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شوق حرم ز خویش رفتیم</p></div>
<div class="m2"><p>رست از غم ناقه محمل ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بازآ که ز گریه بی‌تو نگذاشت</p></div>
<div class="m2"><p>مژگان تر آب در گل ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشتاق ز عشق منع ما بس</p></div>
<div class="m2"><p>تا چند شکنجه دل ما</p></div></div>