---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>از پی احیای من روح روان من بیا</p></div>
<div class="m2"><p>قالب بی‌جانم از هجر تو جان من بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زدی تیری و بر خاکم فکندی بر سرم</p></div>
<div class="m2"><p>از قفای تیر خود ابروکمان من بیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند باشم تلخ‌کام از حسرت گفتار تو</p></div>
<div class="m2"><p>لب پر از شهد سخن شیرین‌دهان من بیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوختم از آرزوی ترک تازت بر سرم</p></div>
<div class="m2"><p>گرم جولان همچو برق آتش‌عنان من بیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشور دل را مباد از من ستاند دیگری</p></div>
<div class="m2"><p>بهر تسخیرش شه کشورْسِتان من بیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفتی و سرکش شد از دل شعله آهم چو شمع</p></div>
<div class="m2"><p>آب وصلت تا شود آتش‌نشان من بیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل تپد چندم به خون از شوق یک ره بر سرم</p></div>
<div class="m2"><p>بهر تسکین دل در خون‌تپان من بیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چند این بی‌مهری آخر از ره رسم و وفا</p></div>
<div class="m2"><p>بر سرم یک ره بت نامهربان من بیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تو چون مشتاق تا کی باشد آغوشم تهی</p></div>
<div class="m2"><p>یک ره آخر در برم سرو روان من بیا</p></div></div>