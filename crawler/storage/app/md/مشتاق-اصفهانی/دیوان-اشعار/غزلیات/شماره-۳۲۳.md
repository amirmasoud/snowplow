---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>از دیر و کعبه در عشق گر نبودم نشانی</p></div>
<div class="m2"><p>کافیست بر جبینم گردی ز آستانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم فغانم آید جانسوزتر که چون نی</p></div>
<div class="m2"><p>دارم ز زخم تیرت پر رخنه استخوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آهی که از تو دزدد در سینه خسته جانی</p></div>
<div class="m2"><p>تیغیست در غلافی تیریست در کمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلشنی که نتوان بی‌ناله یکدم آسود</p></div>
<div class="m2"><p>کو فرصتی که بندم بر شاخی آشیانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر اختری سپهریست در حسن و اختر تو</p></div>
<div class="m2"><p>آن اختری که دارد سرکش‌تر آسمانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در وادی محبت آن رهروم که گاهی</p></div>
<div class="m2"><p>از دور هم نبیند گردی ز کاروانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جورت ار ننالم غافل مشو که ما را</p></div>
<div class="m2"><p>آه نهفته در دل تیریست در کمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنگ شکسته ماست کز عشق رنگ بستست</p></div>
<div class="m2"><p>ورنه بود بهاری دنبال هر خزانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کامل عیار عشقت دیوانه من اما</p></div>
<div class="m2"><p>می‌بایدت بسنگی هر لحظه امتحانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در راه انتظارت گیرم چگونه آرام</p></div>
<div class="m2"><p>نه صبری و شکیبی نه تابی و توانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شادم که رفت در عشق سرمایه‌ام بتاراج</p></div>
<div class="m2"><p>سودای عاشقان را سودیست هر زیانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرغان ز عشق باشند تا نغمه‌سنج مشتاق</p></div>
<div class="m2"><p>چون من کجا سراید مرغی بگلستانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کز ناله‌های الوان آن بلبلم که دارد</p></div>
<div class="m2"><p>با صد هزار دستان هر لحظه داستانی</p></div></div>