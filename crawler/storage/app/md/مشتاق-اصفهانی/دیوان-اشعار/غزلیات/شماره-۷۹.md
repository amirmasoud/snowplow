---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>اگر حرم بود ار دیرخانه خانه تست</p></div>
<div class="m2"><p>بهر دری که نهم سر بر آستانه تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه طایری تو که طوبی سزد که رشگ برد</p></div>
<div class="m2"><p>بر آن درخت که بر شاخش آشیانه تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا تو مردمک دیده مکش زینهار</p></div>
<div class="m2"><p>قدم ز خانه چشمم که خانه خانه تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه احتیاج بغواصیم درین قلزم</p></div>
<div class="m2"><p>مراکه دل صدف گوهر یگانه تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکش سر از کفم ای زلف یار سوزم چند</p></div>
<div class="m2"><p>بداغ حسرت سرپنجه‌ای که شانه تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهرفشان چو رگ ابر نوبهار چراست</p></div>
<div class="m2"><p>زبانم ارنه کلید در خزانه تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسلسبیل نشویم ز رخ غبار درت</p></div>
<div class="m2"><p>که آب روی من از خاک آستانه تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو خود چه نغمه‌سرائی که بر فلک مشتان</p></div>
<div class="m2"><p>برقص زهره ز گلبانگ عاشقانه تست</p></div></div>