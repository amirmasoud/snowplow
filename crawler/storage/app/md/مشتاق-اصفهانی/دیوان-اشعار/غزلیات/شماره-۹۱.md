---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>بر لب بغیر ناله که دمساز مانده است</p></div>
<div class="m2"><p>از دوریت بما چه دگر باز مانده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد خزان عمر و هوای چمن بجاست</p></div>
<div class="m2"><p>پر رفته است و حسرت پرواز مانده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون دل زید به پنجه مژگان او که صید</p></div>
<div class="m2"><p>سالم کجا بچنگن شهباز مانده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم ز دیر و کعبه بدل رو که این در است</p></div>
<div class="m2"><p>در عشق اگر دری بر خم باز مانده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردم بباغ ناله‌ای و تا ابد مرا</p></div>
<div class="m2"><p>از شوق غنچه گوش بر آواز مانده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز من که در دلم غمت افسرده پاکرا</p></div>
<div class="m2"><p>در خانه سیل‌خانه برانداز مانده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشتاق را ز عشق بود بوی گل کجا</p></div>
<div class="m2"><p>پنهان بزیر پرده غماز مانده است</p></div></div>