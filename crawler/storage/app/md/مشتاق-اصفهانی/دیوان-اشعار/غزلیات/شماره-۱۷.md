---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>ما حریف غم و پیمانه کشی پیشه ما</p></div>
<div class="m2"><p>دیده ما قدح ما دل ما شیشه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه از آن مه عوض مهر بجز کین طلبم</p></div>
<div class="m2"><p>که جفا پیشه او گشت و وفا پیشه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مادر این بادیه آن خار بن تشنه لبیم</p></div>
<div class="m2"><p>که رهین نمی از خاک نشد ریشه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکل عشق به فکرت نشود طی ورنه</p></div>
<div class="m2"><p>رخنه در سنگ کند ناخن اندیشه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه ضرور است به سنگی رسد از ما زخمی</p></div>
<div class="m2"><p>باش گو سست‌تر از ناخن ما تیشه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستی ما بود از خون دل آن روز مباد</p></div>
<div class="m2"><p>که تنک‌مایه از این باده شود شیشه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منع ما چد کنی این همه مشتاق که هست</p></div>
<div class="m2"><p>عشق بازی فن ما باده کشی پیشه ما</p></div></div>