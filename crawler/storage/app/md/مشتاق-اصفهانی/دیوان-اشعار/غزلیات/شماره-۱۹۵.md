---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>گرفتم ز آشیان پرواز از شوق لب بامش</p></div>
<div class="m2"><p>تو پنداری فریب دانه‌ام آورده در دامش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محالست اینکه ناکامی برآید ز آسمان کامش</p></div>
<div class="m2"><p>که از مینای خالی پر نگردد هیچکس جامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگیر آسان طریق عشق را کاین ره بود راهی</p></div>
<div class="m2"><p>که باید راهرو از سر گذشتن اولین گامش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدار از من دریغ بوسه ز آن لب چه خواهد شد</p></div>
<div class="m2"><p>که یکره تلخکامی زین شکر شیرین شود کامش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن خورشیدوش داغم که هرگز از فروغ او</p></div>
<div class="m2"><p>نگردد پخته نخل آرزوئی میوه خامش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه سوزان ببزم عشق از یک آتشیم اما</p></div>
<div class="m2"><p>درین محفل یکی شمع و یکی پروانه شد نامش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش‌آنشبها که که بر چون هاله زآن مه بود آغوشم</p></div>
<div class="m2"><p>کنون طالع نمی‌بینم شبی نیز از لب بامش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدیث تلخ حیف است از لب شیرین او ورنه</p></div>
<div class="m2"><p>مذاق جان ما را چاشنی بخش است دشنامش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز جورت اول عشقم مهیای هلاک خود</p></div>
<div class="m2"><p>که از آغاز هر کاری توان دانست انجامش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بخت تیره‌ام در کشوری مشتاق افتاده</p></div>
<div class="m2"><p>که صبحش راز ظلمت امتیازی نیست با شامش</p></div></div>