---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>تا کوی تو بی‌رهبری و راهبری چند</p></div>
<div class="m2"><p>رفتم ز پی ناله خونین جگری چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک دوزخ و نیک و بد ازو در حذر ای وای</p></div>
<div class="m2"><p>ریزد اگر از شعله آهم شرری چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خیل اسیران کهن نیستم اما</p></div>
<div class="m2"><p>روزی زده‌ام در قفسی بال و پری چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد وصل بتان قسمتم از ترک دل و دین</p></div>
<div class="m2"><p>دادم خزفی چند و خریدم گهری چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذار بنظاره گل از روی تو چینم</p></div>
<div class="m2"><p>رحم آر بمحرومی حسرت‌نگری چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن زلف پر از حلقه بر آن طرف بناگوش</p></div>
<div class="m2"><p>شامیست که در دل بود او را سحری چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشتاق من و راه‌نوردان ره عشق</p></div>
<div class="m2"><p>چون ریگ روانیم پریشان سفری چند</p></div></div>