---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>چو ماه چارده دارم نگاری چارده‌ساله</p></div>
<div class="m2"><p>که رخسارش بود ماه و خطش بر گرد مه هاله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین گلشن زیاد از ساغر حسن تو بود آن می</p></div>
<div class="m2"><p>که ساقی ریخت در جام گل و پیمانه لاله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نباشم چون خموش از جور او با این دل مسکین</p></div>
<div class="m2"><p>چه برمی‌خیزد از آه و چه برمی‌آید از ناله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش آن دم کز چمن مست آیی و از تاب می‌ریزد</p></div>
<div class="m2"><p>ز رخسارت عرق زان سان که از گلبرگ تر ژاله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندانی گر چه‌ها آمد ز تیغت بر دلم بنگر</p></div>
<div class="m2"><p>که خون می‌ریزد از مژگان تر پرگاله پرگاله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشاط طبع اگر جویی رو از میخانه جو کآنجا</p></div>
<div class="m2"><p>می یک‌ساله از دل می‌برد اندوه صدساله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به صحرا می‌رود آن آهوی مشکین چه خوش باشد</p></div>
<div class="m2"><p>زمانی کاید و مشکین غزالانش ز دنباله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عروس فکر بکرم فارغ از وصف است کاین دختر</p></div>
<div class="m2"><p>ز نیکویی ندارد حاجت تعریف دلاله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رباید بوسه‌ای گر ز آن لب شیرین زند دردم</p></div>
<div class="m2"><p>لب مشتاق از شیرینی آن شهد تبخاله</p></div></div>