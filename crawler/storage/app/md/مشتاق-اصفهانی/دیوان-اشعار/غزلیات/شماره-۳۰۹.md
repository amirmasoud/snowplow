---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>زهدم افسرده خوشا وقت قدح پیمائی</p></div>
<div class="m2"><p>که شود مست و زند دستی و کو بدپائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آگه از روز جزائی و کشتی زارم آه</p></div>
<div class="m2"><p>اگر امروز نمیداشت ز پی فردائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حالم آن ماهی لب تشنه ز هجرت داند</p></div>
<div class="m2"><p>که به خاک افکندش موجه‌ای از دریائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست با کی ز فنای تو جهان را که خورد</p></div>
<div class="m2"><p>چه غم از سوختن خاربنی صحرائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توبه چون با همه تلخی کنم از باده عشق</p></div>
<div class="m2"><p>که بکیفیت این می نبود صهبائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر نهان بر تو عیانست گرت بینش هست</p></div>
<div class="m2"><p>وگرت نیست چه پنهانی و چه پیدائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق بزمیست که هر لحظه در آن صد ساغر</p></div>
<div class="m2"><p>پر شود از می و خالی نشود مینائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شنوم در رهت از هر سر افتاده بخاک</p></div>
<div class="m2"><p>وای بر آنکه درین راه گذارد پائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل تهی از غم دلدار مبادم مشتاق</p></div>
<div class="m2"><p>که بود خوش غم جانگاه نشاط‌افزائی</p></div></div>