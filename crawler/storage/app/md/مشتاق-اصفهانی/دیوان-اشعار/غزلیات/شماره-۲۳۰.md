---
title: >-
    شمارهٔ ۲۳۰
---
# شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>چو فارغ در گرفتاری ز جور خار و خس باشم</p></div>
<div class="m2"><p>همان بهتر که در گلشن نباشم در قفس باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگیرد چون غبارم دامن منزل بود یکسان</p></div>
<div class="m2"><p>اگر از ره‌نوردان گاه پیش و گاه پس باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عشقم سرخوش و فارغ ز هر دشمن نیم مستی</p></div>
<div class="m2"><p>که در اندیشه روز از شحنه و شب از عسس باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخاتم گو مده صیاد مرغ بی‌پروبالم</p></div>
<div class="m2"><p>که باشم در حصار عافیت تا در قفس باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمیخواند به بزمم یار و نه میراندم از در</p></div>
<div class="m2"><p>نه مقبولم نه مردودم نمیدانم چه کس باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجولانگاه او کو قوت دستی که چون گردد</p></div>
<div class="m2"><p>عنان‌کش بر سرم او را عنان‌گیر فرس باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه اکنون از غمت در چار موج اشکم افتاده</p></div>
<div class="m2"><p>که عمری شد درین گرداب سرگردان چو خس باشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمیگیرد بکس مشتاق آن نامهربان الفت</p></div>
<div class="m2"><p>گرفتم اینکه من عاشق نباشم بوالهوس باشم</p></div></div>