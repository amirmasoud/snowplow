---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>ای صبا صبحدم چون‌رسی سوی او</p></div>
<div class="m2"><p>حال من عرضه ده با سگ کوی او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدتی شد که من از آن گلم بی‌خبر</p></div>
<div class="m2"><p>تا رسد غافلم از کجا بوی او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خطش شد مرا دوخته چاک دل</p></div>
<div class="m2"><p>بخیه زد آخر این چاک را موی او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آگهی کز مژه خون دل جوشدم</p></div>
<div class="m2"><p>گرزمی دیده لاله‌گون روی او</p></div></div>