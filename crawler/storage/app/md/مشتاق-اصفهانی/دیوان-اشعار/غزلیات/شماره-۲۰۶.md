---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>جمعند خوبان چون دسته گل</p></div>
<div class="m2"><p>وز ناله عاشق تنها چو بلبل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بردند از دل آن زلف و کاکل</p></div>
<div class="m2"><p>تاب و توان و صبر و تحمل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خرمن ما حسرت زد آتش</p></div>
<div class="m2"><p>و آن برق جولان گرم تغافل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شمع دارم در محفل او</p></div>
<div class="m2"><p>از پایه خویش هر دم تنزل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما کاهی و عشق کوهی مجوئید</p></div>
<div class="m2"><p>در زیر این بار از ما تحمل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در راه عشقم از توشه فارغ</p></div>
<div class="m2"><p>ساز ره ماست برگ توکل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میباید آنی کارایش حسن</p></div>
<div class="m2"><p>نه خط و خالیست نه زلف و کاکل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوریست پنهان در مغز مشتاق</p></div>
<div class="m2"><p>کز ناله در دهر افکنده غلغل</p></div></div>