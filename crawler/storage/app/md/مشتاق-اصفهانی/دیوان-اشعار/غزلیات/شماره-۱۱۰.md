---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>دوش در طرف چمن بلبلی افغان می‌کرد</p></div>
<div class="m2"><p>ناله در حلقه مرغان خوش‌الحان می‌کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود در وصل ندانم ز چه رو می‌نالید</p></div>
<div class="m2"><p>غالبا همچو من اندیشه هجران می‌کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتیم را که رهانید تو کل زین بحر</p></div>
<div class="m2"><p>غرق می‌شد اگر اندیشه طوفان می‌کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرنوشتش ز ازل بود که در چاه افتد</p></div>
<div class="m2"><p>ورنه یوسف حذر از حیله اخوان می‌کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تشنه زخم خدنگ توام ای کاش مرا</p></div>
<div class="m2"><p>در گلو قطره‌ای از چشمه پیکان می‌کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد می دُردکش میکده را می‌شد صاف</p></div>
<div class="m2"><p>هرچه می‌گفت گرش پیر مغان آن می‌کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جذبه کعبه بود خاصه مردان خدا</p></div>
<div class="m2"><p>ورنه هر سست قدم قطع بیابان می‌کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود اگر مست می شوق حرم شکوه چرا</p></div>
<div class="m2"><p>کعبه رو از ستم خار مغیلان می‌کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نرسد گل چو به مرغان چمن لشکر دی</p></div>
<div class="m2"><p>کاش می‌آمد و تاراج گلستان می‌کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیر خط لعل تو میجست مپندار که خضر</p></div>
<div class="m2"><p>در سیاهی طلب چشمه حیوان می‌کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قابل گنج محبت دل مشتاق نبود</p></div>
<div class="m2"><p>ورنه این خانه ز سیل مژه ویران می‌کرد</p></div></div>