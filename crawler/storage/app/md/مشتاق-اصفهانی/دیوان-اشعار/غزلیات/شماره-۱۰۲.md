---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>پایی به پای دشت‌نوردم نمی‌رسد</p></div>
<div class="m2"><p>گردی به گرد بادیه‌گردم نمی‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال مرا شنید و نپردازدم به حال</p></div>
<div class="m2"><p>دردم به او رسید و به دردم نمی‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داند مریض خویشم و آسوده خوانَدَم</p></div>
<div class="m2"><p>من در گمان اینکه به دردم نمی‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد خزان گلشن خویشم جدا ز تو</p></div>
<div class="m2"><p>آفت به باغی از دم سردم نمی‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون گریم از غم تو و داغم که هیچ رنگ</p></div>
<div class="m2"><p>از اشک سرخ بر رخ زردم نمی‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشتاق درد نامه عشاق خوانده‌ام</p></div>
<div class="m2"><p>افسانه‌ای به قصه دردم نمی‌رسد</p></div></div>