---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>دیدم من از پهلوی دل از بس جفا خون کردمش</p></div>
<div class="m2"><p>وآنگاه در عشق بتان از دیده بیرون کردمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون قطره‌ای نبود نصیب از چشمه وصلت مرا</p></div>
<div class="m2"><p>زین پس من و چشم تری کز گریه جیحون کردمش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز نشد استد مرا از چشمه دل جوش خون</p></div>
<div class="m2"><p>رفت اندکی تا کم شود از کاوش افزون کردمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عهد من یکدل مجو خرم به گیتی کز غمت</p></div>
<div class="m2"><p>هرجا دل شادی بود از ناله محزون کردمش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکره دماغم بیرخت تر از می عشرت نشد</p></div>
<div class="m2"><p>زین باده تا پیمانه‌ام پر گشت وارون کردمش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاشکم نمانده کشوری آباد در روی زمین</p></div>
<div class="m2"><p>هرجا که شهری یافتم زین سیل هامون کردمش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا قد بناز افروخته با هر خسی در باخته</p></div>
<div class="m2"><p>سروی که من چون فاخته از ناله موزن کردمش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگز ندیدم در قدح صهبای عشرت بی‌لبت</p></div>
<div class="m2"><p>پیمانه‌ام گر شد تهی از زهر پرخون کردمش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تنها نشد ز افسانه‌ام مشتاق سرگرم جنون</p></div>
<div class="m2"><p>با هر که گفتم نکته‌ای از عشق مجنون کردمش</p></div></div>