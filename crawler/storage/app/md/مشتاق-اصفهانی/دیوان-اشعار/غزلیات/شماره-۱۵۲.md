---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>قاصدی باز آمد و حرفی ز جائی می‌زند</p></div>
<div class="m2"><p>سوخت از شوقم که حرف آشنائی می‌زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون به صیدی غمزه‌ات تیر جفایی می‌زند</p></div>
<div class="m2"><p>ناوک رشگی بجان مبتلائی می‌زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چاره‌ام مر گست در بحر غمت از اضطراب</p></div>
<div class="m2"><p>نسپرد تا غرقه جان را دست و پائی می‌زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من خموشم در سر کویت ز بیم مدعی</p></div>
<div class="m2"><p>ورنه هر مرغی بگلزاری نوائی می‌زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشت ما را برق جانسوز غمت تنها نسوخت</p></div>
<div class="m2"><p>خویشتن را هر دم این ظالم بجائی می‌زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خویش را خواهد به یاد قاتل آرد روز حشر</p></div>
<div class="m2"><p>گر شهید عشق حرف خونبهائی می‌زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق جانسوز آتشی باشد که هر دم از تفش</p></div>
<div class="m2"><p>دود آهی سر ز جان مبتلائی می‌زند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نگردد در رهت مشتاق پامال ستم</p></div>
<div class="m2"><p>هرکه می‌آیی بر آن افتاده پائی می‌زند</p></div></div>