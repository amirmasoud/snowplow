---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>بر بلبل آنچه از ستم باغبان گذشت</p></div>
<div class="m2"><p>کی از جفای خار و ز جور خزان گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گامی نرفته خار جفا دامنم گرفت</p></div>
<div class="m2"><p>پنداشتم کز آن سر کو می‌توان گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پایم نه بسته کس ولی از بیم پاسبان</p></div>
<div class="m2"><p>نتوانم از حوالی آن آستان گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغم که ماند حسرت پیکان او بدل</p></div>
<div class="m2"><p>تیرش ازین چه غم که مر از استخوان گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صیاد بستن پروبالش چه لازمست</p></div>
<div class="m2"><p>مرغی که در هوای قفس ز آشیان گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذشت از زمانه بمشتاق ناتوان</p></div>
<div class="m2"><p>در پیری آنچه از ستم آن جوان گذشت</p></div></div>