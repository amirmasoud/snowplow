---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>نیست وقتی که مرا جان بر جانان نشود</p></div>
<div class="m2"><p>چون شود در بر جانان ز دل و جان نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه بزرگیست بدولت که همه عالم را</p></div>
<div class="m2"><p>آرد ار زیر نگین دیو سلیمان نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتیم مرگ بود چاره هجران ترسم</p></div>
<div class="m2"><p>جان سختی دهم و مشکلم آسان نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبودش تنگ دل عشق شکفتن ورنه</p></div>
<div class="m2"><p>غنچه‌ای نیست درین باغ که خندان نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت کامت ندهم تا ندهی جان ترسم</p></div>
<div class="m2"><p>آخر از شومی بخت این شود و آن نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیستی آب حیاتست بگوئید که خصر</p></div>
<div class="m2"><p>قطره زن در طلب چشمه حیوان نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شدی کوش که بهتر شوی ارنه ستمست</p></div>
<div class="m2"><p>قطره گوهر شود و گوهر غلطان نشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود بخود کفر محبت شود آخر ایمان</p></div>
<div class="m2"><p>کافر عشق تو گیرم که مسلمان شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس چراغی زپی سوختن ما مشتاق</p></div>
<div class="m2"><p>گو شب تیره پروانه چراغان نشود</p></div></div>