---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>ز خیلی کجا چون تو شاهی برآید</p></div>
<div class="m2"><p>ز بامی کجا چون تو ماهی برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود سرکش از کاکلی دود آهم</p></div>
<div class="m2"><p>که گاهی ز زیر کلاهی برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز این کافتد از عاجزی در کمندی</p></div>
<div class="m2"><p>ز صیدی چه در صیدگاهی برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز تو کامران غیر و ما و نگاهی</p></div>
<div class="m2"><p>که از کنج چشم تو گاهی برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمی کرد اوج اخترم چون زلیخا</p></div>
<div class="m2"><p>که ماهی چو یوسف ز چاهی برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوس پیشه‌ام خواند و سوزم چو بینم</p></div>
<div class="m2"><p>که از تهمتی بی‌گناهی برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مبر جور از اندازه بیرون خدا را</p></div>
<div class="m2"><p>مباد از دلی غافل آهی برآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو شامست کوی قمر طلعتان را</p></div>
<div class="m2"><p>که از طرف هر بام ماهی برآید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به از کشت ماشوره زاری که از وی</p></div>
<div class="m2"><p>گلی گر نروید گیاهی برآید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه خوش باشد ار روزی امیدواری</p></div>
<div class="m2"><p>امیدش ز امید گاهی برآید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر آن کشت ظلمست باران که از وی</p></div>
<div class="m2"><p>بامید برقی گیاهی برآید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تن لاغرم چون کشد بار عشقت</p></div>
<div class="m2"><p>کجا کار کوهی ز کاهی برآید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بده کام مشتاق یکره چه باشد</p></div>
<div class="m2"><p>امید گدائی ز شاهی برآید</p></div></div>