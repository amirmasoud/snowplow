---
title: >-
    شمارهٔ ۳۱۱
---
# شمارهٔ ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>هست چشمم ز غمت قلزم طوفان‌زایی</p></div>
<div class="m2"><p>که بود در دل هر قطره او دریایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم ما و دل پرخون که به میخانه عشق</p></div>
<div class="m2"><p>ساغری را نرسد قطره‌ای از مینایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل من پر ز هوایت سرم از سودایت</p></div>
<div class="m2"><p>هر دلی را هوسی هر سری و سودایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم آن دانه درین مزرعه کز طالع خشک</p></div>
<div class="m2"><p>قسمت من نبود قطره‌ای از دریایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سر کوی تو داند ز چه نتوانم رفت</p></div>
<div class="m2"><p>هرکه دستی بودش بر دل و در گل پایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منم آن فاخته کز ناله زارم پیداست</p></div>
<div class="m2"><p>که جدا مانده‌ام از سرو سهی بالایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه از شهر پرآشوب محبت مشتاق</p></div>
<div class="m2"><p>که درین شهر به هر کوچه بود غوغایی</p></div></div>