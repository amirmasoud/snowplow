---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>یکره ز وصل خویش مرا بهره‌مند کن</p></div>
<div class="m2"><p>اغیار را بر آتش غیرت سپند کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای میوه امید فرود آی خود ز شاخ</p></div>
<div class="m2"><p>یا آنکه دست کوته ما را بلند کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز اغیار درد خویش‌ستان و بمن سپار</p></div>
<div class="m2"><p>یا زین میان علاج من دردمند کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارای هزار خاک‌نشین شهسوار من</p></div>
<div class="m2"><p>گاهی نگاه در ته پای سمند کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی خوریم از غم زلف تو پیچ‌وتاب</p></div>
<div class="m2"><p>یکحلقه وقف گردن ما زین کمند کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهی نه تلخ‌کام گر از زهر حسرتم</p></div>
<div class="m2"><p>آن کنج لب که گفت پر از نوشخند کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشتاق را ز عشق نصیحت چه فایده</p></div>
<div class="m2"><p>ای پندگو برای خدا ترک پند کن</p></div></div>