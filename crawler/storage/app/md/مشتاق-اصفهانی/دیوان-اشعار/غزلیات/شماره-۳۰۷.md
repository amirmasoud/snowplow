---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>خوشا مستی و عشق نازنینی</p></div>
<div class="m2"><p>نه آئینی نه کیشی و نه دینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حذر کن ای زبردست از ضعیفان</p></div>
<div class="m2"><p>که دستی هست در هر آستینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ره ما تیره و هر گام صد چاه</p></div>
<div class="m2"><p>نه راه پس نه چشم پیش‌بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه آسوده است آن کز خلق گیتی</p></div>
<div class="m2"><p>نه مهری در دلش باشد نه کینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن از دیدنت منعم چه باشد</p></div>
<div class="m2"><p>زیان خرمنی از خوشه چینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمیگردد شراب انگور صدسال</p></div>
<div class="m2"><p>اگر در خم نماند اربعینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وفا تخمیست در آب و گل ما</p></div>
<div class="m2"><p>نروید این گیاه از هر زمینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیاساقی که مستی کیش عشق است</p></div>
<div class="m2"><p>برو زاهد چه دنیائی چه دینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شود تا نقش بروی اسم اعظم</p></div>
<div class="m2"><p>کجا شایسته باشد هر نگینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه راهی در درون پرده مشتاق</p></div>
<div class="m2"><p>نه کس را از برون علم‌الیقینی</p></div></div>