---
title: >-
    شمارهٔ ۲۷۹
---
# شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>از پرتو مهر، شد آخر آن ماه</p></div>
<div class="m2"><p>محفل فرزندم، الحمدالله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامان خرگاه، تا برزد آن ماه</p></div>
<div class="m2"><p>شد ماه پنهان، در زیر خرگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را بکویش خیزد چه از آه</p></div>
<div class="m2"><p>آن قصر عالی این رشته کوتاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیر و من آخر تا چند باشیم</p></div>
<div class="m2"><p>مقبول مجلس مردود درگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخلیست مقصود کورا و ما را</p></div>
<div class="m2"><p>شاخیست سرکش دستیست کوتاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون آفتابش گویم که باشد</p></div>
<div class="m2"><p>ماه من و مهر آن مهر و این ماه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او همدم غیر وز رشک ما را</p></div>
<div class="m2"><p>آهی و صد اشک اشکی و صد آه</p></div></div>