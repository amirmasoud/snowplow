---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>در طلبت ساز و برک راه ندارد</p></div>
<div class="m2"><p>هرکه بچشم و دل اشگ و آه ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیستم آن طالب فنا که چراغم</p></div>
<div class="m2"><p>غم زدم سرد صبح‌گاه ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش بسیه بختیم که روز و شب من</p></div>
<div class="m2"><p>منت پرتو ز مهر و ماه ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دهم اقلیم فقر را بدو عالم</p></div>
<div class="m2"><p>ملک چنین هیچ پادشاه ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاد بجرم محبتم که ثوابی</p></div>
<div class="m2"><p>روز جزا قدر این گناه ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راز غمت شد ز خویش فاش وگرنه</p></div>
<div class="m2"><p>در دل ما کس بجز تو راه ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ره بکه آرنداز درت دل و جانم</p></div>
<div class="m2"><p>غیر تو این پشت و آن پناه ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دولت مشتاق بس گدائی این در</p></div>
<div class="m2"><p>کوششی از بهر مال و جاه ندارد</p></div></div>