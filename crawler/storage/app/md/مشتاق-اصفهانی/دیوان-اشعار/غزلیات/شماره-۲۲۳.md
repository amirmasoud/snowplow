---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>بجان ز اندیشه غیرآمدم ز آن انجمن رفتم</p></div>
<div class="m2"><p>بیاران صحبت او باد ارزانی که من رفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکافات شکفتن نیست آسان عاقبت دیدی</p></div>
<div class="m2"><p>که چون گل زین گلستان رفتم و خونین کفن رفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسر در غربت آمد عمرم و نامد برم پیکی</p></div>
<div class="m2"><p>مگر یکبارگی از یاد یاران وطن رفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روم گفت از برت بیخود فتادم زین سخن بنگر</p></div>
<div class="m2"><p>که او نارفته من رفتم ولی از خویشتن رفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه حاصل سبز شد گر کویت از سیرابی اشکم</p></div>
<div class="m2"><p>که من با دامنی پرخار حسرت از چمن رفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشد ز آن لب نصیبم بوسه هرگز فغان کاخر</p></div>
<div class="m2"><p>ز حسرت تلخ‌کام از کویت ایشیرین‌دهن رفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم در سینه دارد ناله زاری که نشنیدم</p></div>
<div class="m2"><p>ز یعقوب ار چه صد ره بر در بیت‌الحزن رفتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه منت گر کمند طره او دستگیرم شد</p></div>
<div class="m2"><p>که بیرون تنه لب آخر از آن چاه ذقن رفتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نرفت از حسرت شیرین لبی کس از جهان هرگز</p></div>
<div class="m2"><p>به این جان کندن تلخی که من چون کوهکن رفتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه سودار بود صد رنگم سخن مشتاق کز گیتی</p></div>
<div class="m2"><p>در آخر غنچه‌سان مهر خموشی بر دهن رفتم</p></div></div>