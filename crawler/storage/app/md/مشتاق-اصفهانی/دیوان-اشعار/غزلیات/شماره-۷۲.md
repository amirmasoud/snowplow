---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>خونم ز لب بوسه فریب تو هوس ریخت</p></div>
<div class="m2"><p>آخر ز غمت خون مرا بین که چه کس ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با حسرت گلشن چه کند مرغ گرفتار</p></div>
<div class="m2"><p>صیاد چه شد گل به در و بام قفس ریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عشق تو داد آنکه مرا شب همه شب‌پند</p></div>
<div class="m2"><p>در آتش سوزان همه خار و همه خس ریخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن بیدل مستم که ز تمهید تو آخر</p></div>
<div class="m2"><p>شب خون مرا بر سر کوی تو عسس ریخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گرد یتیمی چو گهر زنده بگورم</p></div>
<div class="m2"><p>گردون ز غمت بر سر من خاک ز بس ریخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین باغ مجو خاطر آزاده که صیاد</p></div>
<div class="m2"><p>از غنچه بهر شاخ گلی رنگ قفس ریخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشتاق ز پا ماند به راه طلب آخر</p></div>
<div class="m2"><p>پر زد ز بس از شوق شکر بال مگس ریخت</p></div></div>