---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>کاش بیرون فتد از سینه دل زار مرا</p></div>
<div class="m2"><p>کشت نالیدن این مرغ گرفتار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌بقا شادی وصل تو و دانم که ز پی</p></div>
<div class="m2"><p>آرد این خنده کم گریه بسیار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه شد ار داد به‌صدرنگ گل آن گلبن ناز</p></div>
<div class="m2"><p>که ازو نیست بجز دامن پرخار مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم از رونق جنس هنر آفت زده‌ای</p></div>
<div class="m2"><p>که زد آتش بدکان گرمی بازار مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گومبر جانب گلشن قفسم را صیاد</p></div>
<div class="m2"><p>بس بود ناله‌ای از حسرت گلزار مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرود تیرگی از بخت بکوشش گو باد</p></div>
<div class="m2"><p>روز روشن دگر آنرا و شب تار مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه آخر بصد افسانه بخوابم میکرد</p></div>
<div class="m2"><p>ساخت از خواب عدم بهر چه بیدار مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گو طبیبم نکند چاره مریض عشقم</p></div>
<div class="m2"><p>که دل‌خسته بود خوش تن بیمار مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست گویائیم از خویش چو طوطی مشتاق</p></div>
<div class="m2"><p>این سخن‌هاست از آن آینه رخسار مرا</p></div></div>