---
title: >-
    شمارهٔ ۲۶۶
---
# شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>هر سحر غلتم در خون از نسیم کوی تو</p></div>
<div class="m2"><p>چون نغلتاند به خون ما را که دارد بوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شوم ایمن از او کز بهر قتل عاشقان</p></div>
<div class="m2"><p>غمزه را بر کف دو شمشیر است از ابروی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرنه از خون شهیدان می‌کشد بالا بگو</p></div>
<div class="m2"><p>می‌خورد آب از کجا سروقد دلجوی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسه نگرفته زان لب آتشم در جان گرفت</p></div>
<div class="m2"><p>سوختم لب‌تشنه آخر در کناری جوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غمت پیچید به خود دایم رگ جانم بتن</p></div>
<div class="m2"><p>پیچ و تاب آن رشته دارد بیشتر یا موی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برد از افسون نگاهی چشمت از ما عقل و هوش</p></div>
<div class="m2"><p>نیست کم ز اعجاز سحر نرگس جادوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من کیم تا با تو ای آتش‌طبیعت سر کنم</p></div>
<div class="m2"><p>شعله را در پیچ و تاب آرد ز تندی خوی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی از من رو به گردان چون کنم با اینکه هست</p></div>
<div class="m2"><p>روی دل سوی توام ای روی دل‌ها سوی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رازها روشن شود مشتاق از او گویا که هست</p></div>
<div class="m2"><p>ساغر گیتی‌نما آیینه زانوی تو</p></div></div>