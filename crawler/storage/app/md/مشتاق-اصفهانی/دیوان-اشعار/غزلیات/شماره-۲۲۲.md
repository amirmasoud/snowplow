---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>جهان روشن ز مهر عالم‌افروزی که من دارم</p></div>
<div class="m2"><p>دلی تاریک‌تر از شب بود روزی که من دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا و از زلال وصل بنشان آتشم تا کی</p></div>
<div class="m2"><p>جگرها سوزد از آه جگرسوزی که من دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آه و ناله‌ام صدجان و دل زخمی مشو ایمن</p></div>
<div class="m2"><p>ز تیغ جان‌شکاف و تیر دلدوزی که من دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شب تاریک‌تر روز من و از روز روشن‌تر</p></div>
<div class="m2"><p>شب اغیار از شمع شب‌افروزی که من دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز زلفش دل به صد افسون گرفتم لیک کی سازد</p></div>
<div class="m2"><p>فراموش آشیان مرغ نوآموزی که من دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نباشد فکر جان‌های غم‌اندوزت عبث تا کی</p></div>
<div class="m2"><p>غم‌اندوزی کدجان غم‌اندوزی که من دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برم مشتاق آمد یار همراه رقیب اما</p></div>
<div class="m2"><p>ببین فیروزی این بخت فیروزی که من دارم</p></div></div>