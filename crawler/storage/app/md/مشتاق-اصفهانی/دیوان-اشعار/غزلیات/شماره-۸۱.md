---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>گوشت کجا به ناله‌ام ای‌دل فریب هست</p></div>
<div class="m2"><p>آن گلبنی که صد چو منت عندلیب هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وادیی که شوق بود راهبر چه باک</p></div>
<div class="m2"><p>گر هر قدم هزار فراز و نشیب هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دعوای ناتوانیت ای خسته کی سزاست</p></div>
<div class="m2"><p>تا قوت کشیدن ناز طبیب هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوشش چه لازمست که از خوان وصل یار</p></div>
<div class="m2"><p>خواهد رسید قسمت ما گر نصیب هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نالم کی از جفای تو داغم از این که نیست</p></div>
<div class="m2"><p>بهر منت وفا و برای رقیب هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشتاق را ز ناله مکن منع بر درت</p></div>
<div class="m2"><p>هرجا گلیست زمزمه عندلیب هست</p></div></div>