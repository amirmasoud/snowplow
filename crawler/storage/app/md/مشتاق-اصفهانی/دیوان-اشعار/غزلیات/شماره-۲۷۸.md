---
title: >-
    شمارهٔ ۲۷۸
---
# شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>تا کی کند یار، از من کناره</p></div>
<div class="m2"><p>افغان ز گردون، آه از ستاره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او را که آبی، بر سردم نزع</p></div>
<div class="m2"><p>چون صبح یابد، عمر دوباره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داند دل ما، ز آندل چه دیده است</p></div>
<div class="m2"><p>آن شیشه کامد، بر سنگ خاره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین بحر خونخوار مردان گذشتند</p></div>
<div class="m2"><p>در فکر کشتی ما بر کناره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افغان ز غربت کز سنگ چون جست</p></div>
<div class="m2"><p>در دم سر آمد عمر شراره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیدردیم کشت از عشق خواهم</p></div>
<div class="m2"><p>جان شرحه شرحه دل پاره پاره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین محرمی آه با او ز حد رفت</p></div>
<div class="m2"><p>سرگوشی غیر چون گوشواره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهدیست گردون، کارام یکدم</p></div>
<div class="m2"><p>طفلان نگیرند، زین گاهواره</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاری ندارد، جان دادن ما</p></div>
<div class="m2"><p>زآن گوشه چشم، بس یک اشاره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا کی کشم ناز، زین چاره‌سازان</p></div>
<div class="m2"><p>خوش آنکه بگذشت، کارش ز چاره</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشتاق آریش، دربر اگر تو</p></div>
<div class="m2"><p>جز او زهر کس، گیری کناره</p></div></div>