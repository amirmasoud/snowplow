---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>ندانم می‌کنی گاهی به غربت یاد من یا نه</p></div>
<div class="m2"><p>به خاطر آیدت زین مانده بی‌کس در وطن یا نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا در مصر دولت بر سریر عزت آگاهی</p></div>
<div class="m2"><p>ز حال پیر کنعان هست در بیت‌الحزن یا نه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یاران نوت در طرح بزم عشرت افکندن</p></div>
<div class="m2"><p>به خاطر می‌رسد محرومی یار کهن یا نه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ‌یادت هیچ می‌آید به پاداش وفای تو</p></div>
<div class="m2"><p>ز بیداد بداندیشان کشیدم آنچه من یا نه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گوشت هیچ می‌گوید صبا کان خسته هجران</p></div>
<div class="m2"><p>کشید از دوریت سر در گریبان کفن یا نه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زندان غمت زان خون که می‌نوشم خبرداری</p></div>
<div class="m2"><p>به گاه باده‌پیمایی به گلگشت چمن یا نه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی آری به یاد این تلخ‌کام زهر حرمان را</p></div>
<div class="m2"><p>که مرد از حسرت یک بوسه زان کنج دهن یا نه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برم مشتاق دانم آید و اما نمی‌دانم</p></div>
<div class="m2"><p>که تا آید ز درد هجر خواهم زیستن یا نه</p></div></div>