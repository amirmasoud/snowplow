---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>بتی کز صحبتم گیرد ملالش</p></div>
<div class="m2"><p>چه سازم گر نسازم با خیالش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مخصوص منست و خاصه غیر</p></div>
<div class="m2"><p>شب هجرانش و روز وصالش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مزن ایدل بجان ناتوان طعن</p></div>
<div class="m2"><p>اگر از حبس تن نبود ملالش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآید از قفس بهر چه کین مرغ</p></div>
<div class="m2"><p>ندارد قوت پرواز بالش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهی نادان که او با چرخ باشد</p></div>
<div class="m2"><p>به دولت صلح و در نکبت ملالش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که این ساقی بود از باده مشتاق</p></div>
<div class="m2"><p>تهی جام زر و جام سفالش</p></div></div>