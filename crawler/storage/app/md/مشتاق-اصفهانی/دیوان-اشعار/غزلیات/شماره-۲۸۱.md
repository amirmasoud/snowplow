---
title: >-
    شمارهٔ ۲۸۱
---
# شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>مرا حسن و عشقست زاری و لابه</p></div>
<div class="m2"><p>خداوندگار و نبی و صحابه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشد ماه در پرده سر گربرآرد</p></div>
<div class="m2"><p>سر از پرده آن ماه زرین عصابه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تراود ز چاک دلم روز و شب خون</p></div>
<div class="m2"><p>کجا اینقدر داده زخمی تلابه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو و نغمه بلبل و طرف گلشن</p></div>
<div class="m2"><p>من و ناله جغد و کنج خرابه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عشق تو دلها پر از خون شد آری</p></div>
<div class="m2"><p>ز یک خم لبالب شود صد قرابه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز من برده دل ماه مشکین کمندی</p></div>
<div class="m2"><p>که هست اختر حسن اوذو ذبابه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل از سینه گرمم آن تاب دارد</p></div>
<div class="m2"><p>که ماهی ندارد ز تفتیده تابه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشند از شراب غمت می‌کشان را</p></div>
<div class="m2"><p>ز می توبه باید ز مستی انابه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز چاک دلم نقش مهرت نمایان</p></div>
<div class="m2"><p>بود چون ز محراب مسجد کتابه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز تو کام مشتاق خواهم از آن لب</p></div>
<div class="m2"><p>اجب دعوتی یا ولی‌الاجابه</p></div></div>