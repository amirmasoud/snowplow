---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>ای که از باد صبا بوی کسی می‌شنوی</p></div>
<div class="m2"><p>بوی جان از دم عیسی‌نفسی می‌شنوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافلی زآنچه دلم می‌کشد از سینه تنگ</p></div>
<div class="m2"><p>سخن مرغ اسیر و قفسی می‌شنوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردی از قافله کو ننماید بشتاب</p></div>
<div class="m2"><p>تا درین بادیه بانگ جرسی می‌شنوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار هر ناکسی و کس نپسندد صدبار</p></div>
<div class="m2"><p>گفتم اما تو کجا حرف کسی می‌شنوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با رقیبان مکن ای تازه گل الفت تا چند</p></div>
<div class="m2"><p>طعنه زآمیزش هر خاروخسی می‌شنوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم‌به‌دم نالم و افغان که ندانی به رهت</p></div>
<div class="m2"><p>تاله کیست که در هر نفسی می‌شنوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حال مشتاق چه دانی ز تمنای لبت</p></div>
<div class="m2"><p>سخن شهدی و حرف مگسی می‌شنوی</p></div></div>