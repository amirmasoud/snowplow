---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>دیدی آخر آنچه با من طالع ناساز کرد</p></div>
<div class="m2"><p>یار را از من مرا از یار غافل باز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میسرودم بر گلی یکچند همچون بلبلی</p></div>
<div class="m2"><p>ناگهان برگ سفر آن گل ز گلشن باز کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخت‌گیریهای هجرش بر فشار طایرم</p></div>
<div class="m2"><p>آشیان را تنگ‌تر از چنگل شهباز کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار بستم منهم از گلشن به چشم حسرتی</p></div>
<div class="m2"><p>گزنم خون جگر نتوانش از هم باز کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصه کوته آسمان و انجمش کز خشم و کین</p></div>
<div class="m2"><p>آن جفا بنیاد با من این ستم آغاز کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار را از من بریدند و مزا از خانمان</p></div>
<div class="m2"><p>گل ز گلشن رفت و بلبل از چمن پرواز کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیستم مشتاق سرگرم فغان مرغی کزو</p></div>
<div class="m2"><p>خویش را آخر کباب از شعله آواز کرد</p></div></div>