---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>از آن غم را دل ما خانه باشد</p></div>
<div class="m2"><p>که آن جغد است و این ویرانه باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمن شد آشنا ناآشنائی</p></div>
<div class="m2"><p>که از هر آشنا بیگانه باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیم مرغی که در دام تو او را</p></div>
<div class="m2"><p>خیال دام و فکر دانه باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا جویم می وصلت که این می</p></div>
<div class="m2"><p>نه در مینا نه در پیمانه باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس از مرگم ز حسرت سوزد آنشمع</p></div>
<div class="m2"><p>که خاکش تربت پروانه باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی باشد مرا از روز خوشتر</p></div>
<div class="m2"><p>که زلفت در کفم چون شانه باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن شد بسته زلف تو مشتاق</p></div>
<div class="m2"><p>که آن زنجیر و این دیوانه باشد</p></div></div>