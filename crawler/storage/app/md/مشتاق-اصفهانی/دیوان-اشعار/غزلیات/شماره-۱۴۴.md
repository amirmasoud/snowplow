---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه شمع خلوتم آن سروناز بود</p></div>
<div class="m2"><p>وز هر که بود غیر منش احتراز بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرگرم ناز او همه شب با من وز شوق</p></div>
<div class="m2"><p>تا صبح کار من همه عجز و نیاز بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین‌سان ز عشق خار نبودم که در برش</p></div>
<div class="m2"><p>عشاق را بر اهل هوس امتیاز بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از مهر بود خصم کش و بوالهوس گداز</p></div>
<div class="m2"><p>وز لطف دوست‌پرور و عاشق‌نواز بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فارغ ز منت می و ساغر به بزم شوق</p></div>
<div class="m2"><p>من از نیاز سرخوش و آن مست ناز بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دایم چراغ خلوت من بود همچو شمع</p></div>
<div class="m2"><p>وز رشگ غیر را همه سوز و گداز بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشتاق رو کنون و به بیچارگی بساز</p></div>
<div class="m2"><p>رفت آنزمان که یار ترا چاره‌ساز بود</p></div></div>