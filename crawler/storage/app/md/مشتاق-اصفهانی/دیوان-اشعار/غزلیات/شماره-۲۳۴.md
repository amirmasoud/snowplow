---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>دلم دارد هوای دام جان هم</p></div>
<div class="m2"><p>که بر من باغ تنگ است آشیان هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توام چون دوستی پروا ندارم</p></div>
<div class="m2"><p>که گردد دشمن انجم آسمان هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منال ایدل که در دل مهوشان را</p></div>
<div class="m2"><p>ندارد ناله تأثیری فغان هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخونم آن شکارافکن کشیده است</p></div>
<div class="m2"><p>که پیدا نیست تیر او کمان هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشو عاشق اگر خواهی دل و دین</p></div>
<div class="m2"><p>که خواهد از کفت رفت این و آنهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکوی نیستی آسوده ز آنم</p></div>
<div class="m2"><p>که پیدا نیست نام از من نشان هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محبت آتشی بر کرده مشتاق</p></div>
<div class="m2"><p>که دایم پیر ازو سوزد جوان هم</p></div></div>