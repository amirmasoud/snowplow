---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>نه هر مهی روش مهرگستری داند</p></div>
<div class="m2"><p>نه هر که خواجه شود بنده پروری داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار شیوه بکار است دلربایان را</p></div>
<div class="m2"><p>نه هر که برد دلی رسم دلبری داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بآن صنم ره و رسم وفا چه آموزم</p></div>
<div class="m2"><p>که مهر خود روش ذره‌پروری داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشست ناله و فریاد دادخواهانش</p></div>
<div class="m2"><p>وگرنه خسرو من دادگستری داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجام و آینه تا ننگرند آخر کار</p></div>
<div class="m2"><p>نه جم جمی نه سکندر سکندری داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم ربود و رخ از من نهفت و حیرانم</p></div>
<div class="m2"><p>که نیست او پری و شیوه‌پری داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرت دلیست بمعشوق و دلستانی ده</p></div>
<div class="m2"><p>که قدر گوهر یکدانه گوهری داند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خبر ز عشق ندارد کسی که رونق دهر</p></div>
<div class="m2"><p>ز گردش فلک و ماه مشتری داند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غنی ز سیم و زر از فیض مستیم ورنه</p></div>
<div class="m2"><p>گدای کوی مغان کیمیاگری داند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشد زاده خود را ز کین مدار طمع</p></div>
<div class="m2"><p>که آسمان پدری خاک مادری داند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مپرس از ستم یار جز ز من مشتاق</p></div>
<div class="m2"><p>که هم پری زده خوی به پری داند</p></div></div>