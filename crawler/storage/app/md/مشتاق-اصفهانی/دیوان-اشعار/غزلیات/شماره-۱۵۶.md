---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>خوبان سزد که پنجه بخونم فرو کنند</p></div>
<div class="m2"><p>هرچند میکنند نکویان نکو کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم چو در خمار چه حاصل پس از وفات</p></div>
<div class="m2"><p>خاک مرا از اینکه قدح یا سبو کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این رسم و راه حق‌طلبانست کاین گروه</p></div>
<div class="m2"><p>پوشند چشم و گمشده را جستجو کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منت چرا ز بخیه کشم بهر چاک دل</p></div>
<div class="m2"><p>کین چاک سینه نیست که او را رفو کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدن بسم ز دور گل آرزو بشاخ</p></div>
<div class="m2"><p>کین گل نه آن گلست که چینند و بو کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردل ز جوش حسرت الوان بحیرتم</p></div>
<div class="m2"><p>می صدهزار رنگ نه در یک سبو کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلها ز کوچه‌گردی زلفت دل مرا</p></div>
<div class="m2"><p>جویند اگر گذار بهر تار مو کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست از جهان بشوی که در کیش عشق نیست</p></div>
<div class="m2"><p>مقبول طاعتی که نه با این وضو کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نازم بآب خورد قناعت که در خور است</p></div>
<div class="m2"><p>این قطره را سراغ اگر جوبجو کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز آی چند آتش رشگم به جان زنند</p></div>
<div class="m2"><p>آن آبهای رفته که رجعت بجو کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عنقای قاف نیستیم گو کسم مپرس</p></div>
<div class="m2"><p>گم گشته نیستم که مرا جستجو کنند</p></div></div>