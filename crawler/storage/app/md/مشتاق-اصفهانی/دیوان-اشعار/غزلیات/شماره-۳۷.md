---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>عنان دل بکف کودکی بود ما را</p></div>
<div class="m2"><p>که میکشد ز ستم مرغ رشته بر پارا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مرا ز دل تست سنگدل یارا</p></div>
<div class="m2"><p>شکایتی که نباشد ز سنگ مینا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه میکند به فلک اضطراب ما که غمی</p></div>
<div class="m2"><p>ز دست و پا زدن غرقه نیست دریارا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیک نگاه غزالان شهر ما چه عجب</p></div>
<div class="m2"><p>اگر کشند بشهر آهوان صحرا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برد ز هر دو جهان دست اگر فتد مشتاق</p></div>
<div class="m2"><p>ترنج غبغب یوسف به کف زلیخا را</p></div></div>