---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>گر بعاشق سر عتابش نیست</p></div>
<div class="m2"><p>هرچه گویم چرا جوابش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نه کارم بهجر خویش گذاشت</p></div>
<div class="m2"><p>بهر قتلم چرا شتابش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوی عشقست چون گذرگه سیل</p></div>
<div class="m2"><p>که بجز خانه خرابش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون زنم دیده برهم آینه‌وار</p></div>
<div class="m2"><p>چشم حیران عشق خوابش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاری از عشق بس دلا کاتش</p></div>
<div class="m2"><p>رحم بر گریه کبابش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتئی را که داد تن بشکست</p></div>
<div class="m2"><p>بیمی از بحر و انقلابش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم آن رهروی که عشق افکند</p></div>
<div class="m2"><p>تشنه دروادئی که آبش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتمش چاره کن غم مشتاق</p></div>
<div class="m2"><p>که ز هجرت توان و تابش نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت سیماب‌وار جز مردن</p></div>
<div class="m2"><p>چاره بهر اضطرابش نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتمش غیر از این علاج دگر</p></div>
<div class="m2"><p>بهر سوز دل کبابش نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر لب انگشت زد مرا یعنی</p></div>
<div class="m2"><p>که خموش این سخن جوابش نیست</p></div></div>