---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>در محفلت ز خون دل از بس لبالبم</p></div>
<div class="m2"><p>فرقی ندارد از قدح باده قالبم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کرد توبه از لب ساغر جدا لبم</p></div>
<div class="m2"><p>ماند از نوای عشق زهی بینوا لبم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگشا لبم بشکوه که در گلستان عشق</p></div>
<div class="m2"><p>از دل چو غنچه لخت جگر چیده تا لبم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیگانه خوشتر است ز شادی غمین عشق</p></div>
<div class="m2"><p>هرگز بخنده گو نشود آشنا لبم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منت برای رزق زدونان چرا کشم</p></div>
<div class="m2"><p>گیرم که کاست جانم و افزود غالبم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشنامی از لب تو بسم کو جز این مخواه</p></div>
<div class="m2"><p>نفع از ثنا زبانم و سود از دعا لبم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشتاق بر لبم مزن انگشت زینهار</p></div>
<div class="m2"><p>کز خون دل چو ساغر صهبا لبالبم</p></div></div>