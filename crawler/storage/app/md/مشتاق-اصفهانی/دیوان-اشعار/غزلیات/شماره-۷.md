---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>ای باد بگو آن شه رعنا پسران را</p></div>
<div class="m2"><p>سر خیل بتان خسرو زرّین‌کمران را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناخن زن داغ دل ارباب محبت</p></div>
<div class="m2"><p>صیقل‌گر آئینه صاحب‌نظران را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر هم زن شیرازه جمعیت عشاق</p></div>
<div class="m2"><p>آشفته کن رشته شوریده سران را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کای رفته بغربت ز غمت شیشه صبرم</p></div>
<div class="m2"><p>نازک‌تر از آن گشته که دل نو سفران را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارم بره شوق من خاک‌نشین چند</p></div>
<div class="m2"><p>چون نقش قدم چشم به حسرت نگران را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ننویسی اگر نامه پیامی که تسلی</p></div>
<div class="m2"><p>بخشد من مهجور بفرقت گذران را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشتاق بس این ناله جانسوز که آن شوخ</p></div>
<div class="m2"><p>هرگز نفرستد خبری بی‌خبران را</p></div></div>