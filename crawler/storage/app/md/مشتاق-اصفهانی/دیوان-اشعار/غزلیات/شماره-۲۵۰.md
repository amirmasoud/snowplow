---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>دلم افسرده آه سرد من بین</p></div>
<div class="m2"><p>ز بی‌دردی بدردم دردمن بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رود چون در رهت بر باد خاکم</p></div>
<div class="m2"><p>پریشان در هوایت گرد من بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چمنها از تو سبز ای ابر رحمت</p></div>
<div class="m2"><p>بحرمان گیاه زرد من بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخواهی گر کشی از درد رشکم</p></div>
<div class="m2"><p>بدرد غیر منگر درد من بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین دشت از پی چابک‌سواران</p></div>
<div class="m2"><p>شتابان گرد صحرا گرد من بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم افسرده است اما بیادت</p></div>
<div class="m2"><p>فروزد آتش آه سرد من بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رساند او را بمن مشتاق آهم</p></div>
<div class="m2"><p>بیا و گنج باد آورد من بین</p></div></div>