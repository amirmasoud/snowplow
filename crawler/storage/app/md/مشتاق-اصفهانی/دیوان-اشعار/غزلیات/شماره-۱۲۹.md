---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>چه عجب که وقت مردن بمزار ما بیاید</p></div>
<div class="m2"><p>که نیامدست وقتی که بکار ما بیاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل هر کسی ز نازی شده صید دلنوازی</p></div>
<div class="m2"><p>چه شود که شاهبازی بشکار ما بیاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر اینچنین گدازد تن ما در انتظارش</p></div>
<div class="m2"><p>ز میان رویم تا او بکنار ما بیاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدیار یار باشم نگران که مدتی شد</p></div>
<div class="m2"><p>نه از آن دیار پیکی بدیار ما بیاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چکنیم دور از آن کو دل هرزه گرد خود را</p></div>
<div class="m2"><p>که دگر نه آن دلست این که بکار ما بیاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجز آنکه دیده ما ز غمش سفید گردد</p></div>
<div class="m2"><p>سحری کی از قفای شب تار ما بیاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپریم جان چو مشتاق اگر از جفا براهش</p></div>
<div class="m2"><p>چه عجب که هرچه گوئی زنگار ما بیاید</p></div></div>