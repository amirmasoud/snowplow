---
title: >-
    شمارهٔ ۱۵۷
---
# شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>آنان که می طلب زخم آرزو کنند</p></div>
<div class="m2"><p>خون جای باده غنچه صفت در سبو کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذر ز کام دل که نماند درین چمن</p></div>
<div class="m2"><p>گل اینقدر بشاخ که چینند و بو کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرجائیست یار از آن رهروان عشق</p></div>
<div class="m2"><p>گاهی بکعبه گاه به بتخانه رو کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این آرزوی دل همه از تست هرچه هست</p></div>
<div class="m2"><p>غیر از تو عاشقان چه دگر آرزو کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردن چو شیشه بر خط فرمان نهاده‌ام</p></div>
<div class="m2"><p>از خون و باده هرچه مرا در گلو کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانی چه نشاء در سر ما از خیال تست</p></div>
<div class="m2"><p>در ساغر تو باده اگر زین کدو کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشتاق را تلافی بیداد از بتان</p></div>
<div class="m2"><p>این بسکه گاه گوشه چشمی به او کنند</p></div></div>