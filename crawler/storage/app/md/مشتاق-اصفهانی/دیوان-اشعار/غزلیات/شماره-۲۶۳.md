---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>به توان گفتن کدامین عضو از اعضای تو</p></div>
<div class="m2"><p>خوب‌تر از یکدگر باشند سرتا پای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر نتابد رنگ شرک مدعی عشق غیور</p></div>
<div class="m2"><p>بر سر آن کوی جای ما بود یا جای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ بپایت سوده جا دارد کشد هر دم بخاک</p></div>
<div class="m2"><p>افسر خورشید را از رشک نقش پای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جای نازت با رقیبان است هر دم صد نیاز</p></div>
<div class="m2"><p>هست گویا خاصه از بهر من استغنای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان ننالم نیست با من دانمت چون التفات</p></div>
<div class="m2"><p>سوزدم با غیررشک لطف پا برجای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میرود زین خاکدان مشتاق و خواهد بوسه‌ای</p></div>
<div class="m2"><p>توشه راه عدم از لعل روح‌افزای تو</p></div></div>