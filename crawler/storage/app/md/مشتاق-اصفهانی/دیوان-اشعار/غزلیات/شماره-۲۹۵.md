---
title: >-
    شمارهٔ ۲۹۵
---
# شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>چه شد کآتش به جانم از غضب انداختی رفتی</p></div>
<div class="m2"><p>ز چشم افروختی رخ قد به ناز افراختی رفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که اندازد به خاکم گوهر تاج وفا باشم</p></div>
<div class="m2"><p>چه نقصان من ار قدر مرا نشناختی رفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه شد گر رخصت همراهیم دادی که بر خاکم</p></div>
<div class="m2"><p>به گام اولین چون نقش پا انداختی رفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو چون سیل بهاران خانه‌پردازی که از هر ره</p></div>
<div class="m2"><p>خرامان آمدی بس خانه ویران ساختی رفتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان می‌شد ز هستی بی‌تو در چشمم سیه شادم</p></div>
<div class="m2"><p>که از آیینه‌ام این زنگ را پرداختی رفتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه می‌دانی نیازم را که گر یک دم به دلجویی</p></div>
<div class="m2"><p>نشستی در برم قامت به ناز افراختی رفتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه گویم بر من از جورت چه‌ها ای شهسوار آمد</p></div>
<div class="m2"><p>زدی کشتی به تیغم توسن کین تاختی رفتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه داری تا کنی مشتاق دیگر رو به کوی او</p></div>
<div class="m2"><p>که نقد دین و دل در عشقبازی باختی رفتی</p></div></div>