---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>به عاشق مژدهٔ کامی صبا از وصل جانان بر</p></div>
<div class="m2"><p>نوید قطره‌ای بر تشنه‌ای از آب حیوان بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وصلت کرده‌ام خو از پی قتلم مکش خنجر</p></div>
<div class="m2"><p>اگر خواهی کشی در خاک و خونم نام هجران بر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبا خون شد اسیران قفس را دل ز مهجوری</p></div>
<div class="m2"><p>پیامی زین گرفتاران به مرغان گلستان بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو شمع بزم غیری هر شب و من سوزم از غیرت</p></div>
<div class="m2"><p>چه خواهد شد شبی هم با سیه‌روزی به پایان بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حسرت رفتم از کوی تو و اکنون که من رفتم</p></div>
<div class="m2"><p>بیا گو غیر ازین گلشن چو گلچین گل به دامان بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شکر اینکه از رخ رشک ماه و غیرت مهری</p></div>
<div class="m2"><p>شبی با ما به انجام آور و روزی به پایان بر</p></div></div>