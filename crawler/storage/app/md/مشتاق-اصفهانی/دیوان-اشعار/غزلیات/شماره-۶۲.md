---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>به کوی یار مرا بار در گل افتاده است</p></div>
<div class="m2"><p>فتاده بار من اما به منزل افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چگونه آورد او را به دام بی‌خود عشق</p></div>
<div class="m2"><p>که مرغ زیرک و صیاد غافل افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشم که کار مرا دوست بسته می‌خواهد</p></div>
<div class="m2"><p>وگرنه عقده من سخت مشکل افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خون خویش تپم تا ابد که مرگش نیست</p></div>
<div class="m2"><p>ز تیغ جور تو مرغی که بسمل افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تن فتاده به کویش اگر سر مشتاق</p></div>
<div class="m2"><p>به این خوش است که در پای قاتل افتاده است</p></div></div>