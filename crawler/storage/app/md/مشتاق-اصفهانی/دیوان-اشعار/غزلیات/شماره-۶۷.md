---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>دلم بی‌او صفا هرگز ندیده است</p></div>
<div class="m2"><p>لبم بی‌او نوا هرگز ندیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آن حاجت طلب در کوی عشقم</p></div>
<div class="m2"><p>که تأثیر از دعا هرگز ندیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشاید خواندم بلبل که آن گل</p></div>
<div class="m2"><p>نوا زین بی‌نوا هرگز ندیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا عشق تو در راهی فکنده است</p></div>
<div class="m2"><p>که رهرو رهنما هرگز ندیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا چشمیست کز عشق نکویان</p></div>
<div class="m2"><p>نگاه آشنا هرگز ندیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب ملکیست استغنا که چشمی</p></div>
<div class="m2"><p>درین کشور گدا هرگز ندیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه باغست اینکه مرغی برگ عیشی</p></div>
<div class="m2"><p>درین بستان‌سرا هرگز ندیده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پر از اغیار بزمت دایم اما</p></div>
<div class="m2"><p>درو مشتاق جا هرگز ندیده است</p></div></div>