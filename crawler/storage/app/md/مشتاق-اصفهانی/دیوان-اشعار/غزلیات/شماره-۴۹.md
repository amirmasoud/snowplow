---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>شب وصل است و آمد یار غیرش از قفا امشب</p></div>
<div class="m2"><p>غم هجران چو فردا خواهد آمد گو بیا امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چون رفتی نه ماهی بی‌رخت نه هفته مانم</p></div>
<div class="m2"><p>فراغم میکشد البته یا امروز یا امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هجرت سوختم دیروز و دیشب آه اگر باشم</p></div>
<div class="m2"><p>چون دیروز از تو دور امروز و چون دیشب جدا امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب هجر است و از اشگ جگر گون تا سحر بی‌او</p></div>
<div class="m2"><p>بخون من خفته آیا خفته بی‌من او کجا امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بزمم رفت دوش و آمد امشب مردم از خجلت</p></div>
<div class="m2"><p>که از دیشب ز هجرش مانده بودم زنده تا امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر دیشب چراغ محفل بیگانگان بودی</p></div>
<div class="m2"><p>که از چشمت نمی‌بینم نگاه آشنا امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهر شب امشبم مشتاق نالان‌تر ز هجرانش</p></div>
<div class="m2"><p>چه خواهم گر نه مرگ خویش خواهم از خدا امشب</p></div></div>