---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>به دل چگونه توان داغ عشق پنهان داشت</p></div>
<div class="m2"><p>به پنبه آتش‌سوزان نهفته نتوان داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشد نصیبم از آن بوسه چه حالست این</p></div>
<div class="m2"><p>که تشنه مردم و لعل تو آب حیوان داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خون عاشق از اظهار عشق تشنه شوی</p></div>
<div class="m2"><p>فغان که درد ترا باید از تو پنهان داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگین سلطنتست از دو کون کندن دل</p></div>
<div class="m2"><p>گمان مکن که چنین خاتمی سلیمان داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا که در تن ما بی‌رخ تو شکوه جان</p></div>
<div class="m2"><p>شکایتست که یوسف ز رنج زندان داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش آن مریض که در بستر رضا جان داد</p></div>
<div class="m2"><p>نه انتظار طبیب و نه فکر درمان داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رود زیاد به ساحل رسیده را حالی</p></div>
<div class="m2"><p>که از طلاطم دریا بروز طوفان داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون نه دیده من در طلسم حیرت از اوست</p></div>
<div class="m2"><p>مرا چو آینه رویت همیشه حیران داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دمید صبح شب عمر و دم نزد صبحم</p></div>
<div class="m2"><p>خوش آن زمان که شب انتظار پایان داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فکند ناله مشتاق در جهان آتش</p></div>
<div class="m2"><p>ز داغ‌ها که به جان از فراق جانان داشت</p></div></div>