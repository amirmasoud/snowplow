---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>غم دل کس به امید چه گوید دل‌ستانش را</p></div>
<div class="m2"><p>چرا بلبل خروشد نشنود چون گل فغانش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن ای گل جفا با بلبل خود این قدر ترسم</p></div>
<div class="m2"><p>رود از باغ و نتوانی تهی دید آشیانش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارم گر برش از بوالهوس فرقی عجب نبود</p></div>
<div class="m2"><p>که نشناسد ز گلچین هیچ گلبن باغبانش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کویت گر چنین آشفته می‌گردم مکن منعم</p></div>
<div class="m2"><p>دلی گم کرده‌ام اینجا و می‌جویم نشانش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو دستم بهر آن دادند در جولانگه نازش</p></div>
<div class="m2"><p>که از دستی رکابش گیرم از دستی عنانش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جفای دوست باشد لطف دیگر گو فلک هرگز</p></div>
<div class="m2"><p>نسازد مهربان با من دل نامهربانش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشد مشتاق تا کی محنت هجران خوش آن ساعت</p></div>
<div class="m2"><p>که بیند روی جانان و کند تسلیم جانش را</p></div></div>