---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>شدی تو گلبن ناز از کنار سوختگان</p></div>
<div class="m2"><p>خزان چو شمع سحر شد بهار سوختگان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مزن بهر خس و خار آتش ستم وز رشک</p></div>
<div class="m2"><p>ازین زیاده مکن خارخار سوختگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپند آتش عشقی نگشته کی دانی</p></div>
<div class="m2"><p>که چیست حال دل بیقرار سوختگان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سزای تو است صبا کاتشت بجان افتاد</p></div>
<div class="m2"><p>پی چه بردی از آن کو غبار سوختگان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه احتیاج بشمع است تربت ما را</p></div>
<div class="m2"><p>بس آه گرم چراغ مزار سوختگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز عشقبازی پروانه شد مرا روشن</p></div>
<div class="m2"><p>که غیر سوخت ندارد قمار سوختگان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مراچه شکوه ز بخت سیه که تاریکست</p></div>
<div class="m2"><p>ز آه سوختگان روزگار سوختگان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رهین ناله گرمم که چون سپند کسی</p></div>
<div class="m2"><p>جز او گره نگشاید ز کار سوختگان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب فراق ز سوز غمش ببین مشتاق</p></div>
<div class="m2"><p>چو شمع دیده شب‌زنده‌دار سوختگان</p></div></div>