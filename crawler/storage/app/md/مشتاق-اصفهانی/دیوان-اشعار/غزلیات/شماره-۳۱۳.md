---
title: >-
    شمارهٔ ۳۱۳
---
# شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>ز هرچه هست رخ ما از آن بگردانی</p></div>
<div class="m2"><p>که از دو کون بر آن آستان بگردانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مثال بلبل از آن شاخ گل که نتوانی</p></div>
<div class="m2"><p>بشاخ دیگر از آن آشیان بگردانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار آمد و دور نشاط ما ساقی</p></div>
<div class="m2"><p>ز ساغریست که در گلستان بگردانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه رسم شاهسواران بود که چون بینی</p></div>
<div class="m2"><p>فتاده بره از وی عنان بگردانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیک نگاه درین وادی ایصنم چه عجب</p></div>
<div class="m2"><p>ز کعبه گر ره صد کاروان بگردانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن جفا که نداری تو سنگدل دستی</p></div>
<div class="m2"><p>که تیره آه من از آسمان بگردانی</p></div></div>