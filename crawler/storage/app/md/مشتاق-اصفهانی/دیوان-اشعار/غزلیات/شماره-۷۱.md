---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه گشته تسلیم بر حکمت از بدایت</p></div>
<div class="m2"><p>لب بسته از بد و نیک نه شکر و نه شکایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دشمن قوی چیست بیمت ز ما ضعیفان</p></div>
<div class="m2"><p>ما را نه زور خصمی نه از کسی حمایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر درگه تو دادند پاداش خدمتم را</p></div>
<div class="m2"><p>هر خادمی که دارد مخدوم بی‌عنایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درمانده داردم سخت آن سنگدل که در وی</p></div>
<div class="m2"><p>نه گریه راست تأثیر نه ناله را سرایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بستیم رخت از آن کو کانجاست خواری افزون</p></div>
<div class="m2"><p>آن بنده که بیش است مستوجب رعایت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما کجرو از الستیم ای هادی طریقت</p></div>
<div class="m2"><p>آنرا بجو که باشد شایسته هدایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون در طلب نمیریم نادیده روی منزل</p></div>
<div class="m2"><p>هم پای جستجو لنگ هم راه بینهایت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بیم خوی تندت خون شد دلم چه سازم</p></div>
<div class="m2"><p>نه طاقت خموشی نه جرأت شکایت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد کشور دل ما ویران ز جور دشمن</p></div>
<div class="m2"><p>دستی برآر وقت است ای صاحب ولایت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برد از حدیث وصلت خواب عدم جهانرا</p></div>
<div class="m2"><p>فریاد ازین فسانه افغان از این حکایت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا کی کشی ازو سر کز فیض طبع قانع</p></div>
<div class="m2"><p>زان لب بود دوبوسی مشتاق را کفایت</p></div></div>