---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>پی چه گلبن بختم گل مراد دهد</p></div>
<div class="m2"><p>که تا دهد فلکش چیند و بباد دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌ست تلخ جدائی که میکشد ساقی</p></div>
<div class="m2"><p>از این شرابم اگر جرعه زیاد دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معاشران ز پی صحبت تو مدهوشند</p></div>
<div class="m2"><p>تر از حسرت ما ز آن میان که یاد دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازو مراد طلب عالمی و من خاموش</p></div>
<div class="m2"><p>که لطف دوست مرا باید آنچه داد دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنونکه سوخت فراقم کجاست دلسوزی</p></div>
<div class="m2"><p>که در هوای تو خاکسترم بباد دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوئی بکوی مغان کی سزد خوش آن ساقی</p></div>
<div class="m2"><p>که گر میم دهد از جام اتحاد دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود به بزم شهان ساغری بدور که یاد</p></div>
<div class="m2"><p>ز کاسه سر جمشید و کیقباد دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشم که درد توام در میان گرفته مباد</p></div>
<div class="m2"><p>فلک چو مهره از این ششدرم گشاد دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهد مراد دلش ایزد آنکه یاد تو را</p></div>
<div class="m2"><p>ز نامرادی مشتاق نامراد دهد</p></div></div>