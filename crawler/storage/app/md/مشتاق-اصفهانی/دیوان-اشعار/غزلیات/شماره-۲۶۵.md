---
title: >-
    شمارهٔ ۲۶۵
---
# شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>نی طاقت وصلت مرا نه صبر در هجران تو</p></div>
<div class="m2"><p>وصلت بلا هجرت بلا ای من بلاگردان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در یکدگر افتاده‌اند از بس شهیدان هر طرف</p></div>
<div class="m2"><p>جای تپیدن کی بود بر کشته در میدان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنها نه من گشتم خراب از جلوه مستانه‌ات</p></div>
<div class="m2"><p>چون سیل باشد هر قدم بس خانه‌ها ویران تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عید است و در خون می‌تپد از حسرت تیغت دلم</p></div>
<div class="m2"><p>برخیز و قربان کن مرا ای جان و دل قربان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردم گرفته دامنت امدادی ای باد صبا</p></div>
<div class="m2"><p>شاید به دامانی رسم دست من و دامان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب‌تشنه تا کی داریم بس قطره این دانه را</p></div>
<div class="m2"><p>ای ابر رحمت سوختم از حسرت باران تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاندوه بیرون گشته‌ام یارب درین میدان بود</p></div>
<div class="m2"><p>تیغ قضا خونریزتر یا خنجر مژگان تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر سر به تیغش افکنی حاشا ز حکمت سرکشد</p></div>
<div class="m2"><p>مشتاق دارد چون قلم سر بر خط فرمان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زینگونه چون صرصر بود گر ترکتاز جلوه‌ات</p></div>
<div class="m2"><p>دانم چو گرد آخر روم بر باد از جولان تو</p></div></div>