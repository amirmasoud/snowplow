---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>شه من ترا نشان نه که من گدات جویم</p></div>
<div class="m2"><p>همه حیرتم ندانم ز که و کجات جویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو به گنج‌های عالم تو به دست کس نیایی</p></div>
<div class="m2"><p>به کدام برگ یارب من بینوات جویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من دور از آستانت طلبم چه زین و آنت</p></div>
<div class="m2"><p>ندهد کسی نشانت مگر از خدات جویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه بدوستی نه دشمن نه به گلشنی نه گلخن</p></div>
<div class="m2"><p>زکه‌ات طلب کنم من ز کدام جات جویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسر شکار خود آ تو شکار پیشه تا کی</p></div>
<div class="m2"><p>بشکنج دام هجران من مبتلات جویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدعا نجات باید طلبیدن از هر آفت</p></div>
<div class="m2"><p>تو چه آفتی ندانم که به صد دعات جویم</p></div></div>