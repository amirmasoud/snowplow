---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>در چمن جوری که از باد خزان بر گل گذشت</p></div>
<div class="m2"><p>انتقام آن ستم باشد که بر بلبل گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده مرغان چمن را غیرتش آشفته حال</p></div>
<div class="m2"><p>بازپنداری صبا بر طره سنبل گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر آب از گریه‌ام نبود کنون طاق سپهر</p></div>
<div class="m2"><p>بارها سیل سرشگم از سر این پل گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چمن آن بلبل افسرده‌ام کز دل مرا</p></div>
<div class="m2"><p>برنیامد ناله زاری و فصل گل گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جفای خار مرغان قفس آسوده‌اند</p></div>
<div class="m2"><p>آه از آن محنت که در گلزار بر بلبل گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک عالم آید از مشتاق چون آید بهار</p></div>
<div class="m2"><p>از سر پیمانه نتواند به فصل گل گذشت</p></div></div>