---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>شد صبح که هرکس پی کاری گیرد</p></div>
<div class="m2"><p>یا آنکه بدوستی قراری گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشوقت کسی بود که از خانه برون</p></div>
<div class="m2"><p>آید سر راه انتظاری گیرد</p></div></div>