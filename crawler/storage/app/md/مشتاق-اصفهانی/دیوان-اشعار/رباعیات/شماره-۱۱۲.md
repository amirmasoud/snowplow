---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>جز فتنه نمیکند جهان طور نگر</p></div>
<div class="m2"><p>جز زهر نمیدهد فلک جور نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیتی نگر و فلک نگر گردش بین</p></div>
<div class="m2"><p>محفل نگر و جام نگر دور نگر</p></div></div>