---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>تنها نه زمین ز صید گاهت گلرنگ</p></div>
<div class="m2"><p>از خون دل منست چون عرصه جنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد مرغ دل از تیغ جفایت بر خاک</p></div>
<div class="m2"><p>افتاده بسان طایر خورده خدنگ</p></div></div>