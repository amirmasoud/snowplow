---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>دردشت محبت که گلشن محزونست</p></div>
<div class="m2"><p>ز آواز درائی جگرم پر خونست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بانگ جرس این اثر ندارد گویا</p></div>
<div class="m2"><p>این ناله زار ناله مجنونست</p></div></div>