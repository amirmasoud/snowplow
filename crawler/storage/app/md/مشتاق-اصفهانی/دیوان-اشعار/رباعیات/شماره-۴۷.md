---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>ای آنکه سرگشم از غمت گلگونست</p></div>
<div class="m2"><p>حالم ز فراق تو چه گویم چونست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کامم خالی ز شهد و لبریز ز زهر</p></div>
<div class="m2"><p>جامم تهی از باده و پر از خونست</p></div></div>