---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ای رشته بندگیت در گردن ما</p></div>
<div class="m2"><p>هم از تو بود رو بتو آوردن ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را بگنه مگیر از لطف که هست</p></div>
<div class="m2"><p>زامید عطای تو گنه کردن ما</p></div></div>