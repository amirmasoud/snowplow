---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>دانم بنشانه ناوک‌ها نخورد</p></div>
<div class="m2"><p>تا حق نشود اشاره فرما نخورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ماست که تیری بنشان اندازیم</p></div>
<div class="m2"><p>از ما نبود برو خورد یا نخورد</p></div></div>