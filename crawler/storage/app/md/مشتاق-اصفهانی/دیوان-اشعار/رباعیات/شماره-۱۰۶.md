---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>جام صهبا ز ساغر جم خوشتر</p></div>
<div class="m2"><p>آب انگور ز آب زمزم خوشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن گوشه که با پریوشی باده‌کشی</p></div>
<div class="m2"><p>صد مرتبه از عالم و آدم خوشتر</p></div></div>