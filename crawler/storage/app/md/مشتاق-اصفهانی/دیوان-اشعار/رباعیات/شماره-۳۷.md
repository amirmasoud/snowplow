---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>ای آنکه جفا عادت دیرینه تست</p></div>
<div class="m2"><p>خالی ز محبت دل پر کینه تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جر مهر تو نبود آنچه در سینه ماست</p></div>
<div class="m2"><p>جز کینه مانه آنچه در سینه تست</p></div></div>