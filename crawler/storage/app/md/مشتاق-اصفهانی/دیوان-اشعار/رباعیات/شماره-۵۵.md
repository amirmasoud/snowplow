---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>زآنچه از ستمت بجان بیتاب گذشت</p></div>
<div class="m2"><p>از چاره مرا کار بهر باب گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت آنکه رسیده بود سیلم بکمر</p></div>
<div class="m2"><p>اکنون چکنم که از سرم آب گذشت</p></div></div>