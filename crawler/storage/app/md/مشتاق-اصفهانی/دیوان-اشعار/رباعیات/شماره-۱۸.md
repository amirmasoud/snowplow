---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>در بر معشوق و در قدح می چه خوش است</p></div>
<div class="m2"><p>در گوشه بزم ناله نی چه خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرمست شدن بپای یار افتادن</p></div>
<div class="m2"><p>پس گریه های‌های هی‌هی چه خوشست</p></div></div>