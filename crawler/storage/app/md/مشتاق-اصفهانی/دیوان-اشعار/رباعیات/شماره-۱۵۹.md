---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>پیوسته تن خاک‌نشینی دارم</p></div>
<div class="m2"><p>در کوی غمت گل زمینی دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید اثر ز ناله‌ام هست که باز</p></div>
<div class="m2"><p>مینالم و ناله حزینی دارم</p></div></div>