---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>افغان که نه عمر جاودان خواهد ماند</p></div>
<div class="m2"><p>فریاد که نه جسم و نه جان خواهد ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کنج لحد از تن فرسوده ما</p></div>
<div class="m2"><p>فرداست که مشتی استخوان خواهد ماند</p></div></div>