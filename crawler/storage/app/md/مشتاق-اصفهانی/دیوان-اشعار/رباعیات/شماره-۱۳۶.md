---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>پیوسته دلم کباب میخواهد عشق</p></div>
<div class="m2"><p>دایم چشمم پرآب میخواهد عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ویران‌تر از آنچه گوئیم کرد و هنوز</p></div>
<div class="m2"><p>زین بیشترم خراب میخواهد عشق</p></div></div>