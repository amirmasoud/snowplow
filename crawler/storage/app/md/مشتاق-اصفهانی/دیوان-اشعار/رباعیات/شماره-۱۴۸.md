---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>ای موی تو در برون دل‌ها همه چنگ</p></div>
<div class="m2"><p>وی روی تو در فریب جان‌ها همه رنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظلیست ز موی تو چه هندو چه حبش</p></div>
<div class="m2"><p>عکسیست ز روی تو چه روم و چه فرنگ</p></div></div>