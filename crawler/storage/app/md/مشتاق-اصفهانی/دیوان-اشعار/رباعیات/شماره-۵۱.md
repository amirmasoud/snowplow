---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>آخر ثمر مهر تو کین بود ای دوست</p></div>
<div class="m2"><p>سودای تو خصم دل و دین بود ای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه قاعده محبت این بود ای دوست</p></div>
<div class="m2"><p>نه شیوه دوستی چنین بود ای دوست</p></div></div>