---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>در وحدت آن جان جهان هست سخن</p></div>
<div class="m2"><p>هرچند که دارد از شمار افزون تن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی گرت این دقیقه گردد روشن</p></div>
<div class="m2"><p>نظاره نور مهر کن در روزن</p></div></div>