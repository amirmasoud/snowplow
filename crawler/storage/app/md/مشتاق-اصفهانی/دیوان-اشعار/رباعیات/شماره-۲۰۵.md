---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>ای برده بحسن از مه و خورشید گرو</p></div>
<div class="m2"><p>وز داس جفاکشت وفا کرده درو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کلبه اغیار بکاشانه ما</p></div>
<div class="m2"><p>برخیز و بیا ولیک بنشین و مرو</p></div></div>