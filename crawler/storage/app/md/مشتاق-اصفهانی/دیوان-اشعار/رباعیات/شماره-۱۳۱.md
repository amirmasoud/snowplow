---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>ای کرده به خویش یک روش دایم فرض</p></div>
<div class="m2"><p>بشنو سخنی کامده در معرض عرض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باید چو برون شد آخر از صفحه ارض</p></div>
<div class="m2"><p>گردی چه بطول راه‌پیما چه بعرض</p></div></div>