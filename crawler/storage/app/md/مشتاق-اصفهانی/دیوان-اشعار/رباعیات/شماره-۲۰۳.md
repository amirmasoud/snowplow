---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>چون دل نهد از بیم هلاک من و تو</p></div>
<div class="m2"><p>بر الفت تن روان پاک من و تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرداست که قالب دگر ساخته‌اند</p></div>
<div class="m2"><p>از آتش و آب و باد و خاک من و تو</p></div></div>