---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>شاها شاها جهان پناها شاها</p></div>
<div class="m2"><p>سلطان سپهر دستگاها شاها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز و شب من سیه شد از غم رحمی</p></div>
<div class="m2"><p>تابان مهرامنیر ماها شاها</p></div></div>