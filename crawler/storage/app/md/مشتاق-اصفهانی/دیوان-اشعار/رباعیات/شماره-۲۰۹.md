---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>هرگز غم یار را باغیار مگو</p></div>
<div class="m2"><p>ور میگوئی بغیر دلدار مگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسرار بنا محرم اسرار مگو</p></div>
<div class="m2"><p>زنهار مگو هزار زنهار مگو</p></div></div>