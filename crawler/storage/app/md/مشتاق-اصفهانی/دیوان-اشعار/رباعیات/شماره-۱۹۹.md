---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>ایخسته دلم همیشه رنجور از تو</p></div>
<div class="m2"><p>روزم ز سیاهی شب دیجور از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌شعله خس و خار نمی‌سوزد و من</p></div>
<div class="m2"><p>از دوری تو در آتشم دور از تو</p></div></div>