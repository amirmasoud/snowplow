---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>سرگشته پدید زاهد گم گشته</p></div>
<div class="m2"><p>از بهر گزند جان مردم گشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیروز که رفته بود افعی بوده است</p></div>
<div class="m2"><p>امروز که بازگشته کژدم گشته</p></div></div>