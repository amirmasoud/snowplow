---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>گردون همه زهر در ایاغم کردی</p></div>
<div class="m2"><p>وز صرصر غم قصد چراغم کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردی بهزار حیله‌ام دور از یار</p></div>
<div class="m2"><p>داغم کردی و سخت داغم کردی</p></div></div>