---
title: >-
    شمارهٔ ۱۳۳
---
# شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>مخور نگشته از می ناب چه حظ</p></div>
<div class="m2"><p>نادیده شب سیه ز مهتاب چه حظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجران نکشیده را چه لذت ز وصال</p></div>
<div class="m2"><p>آنرا که عطش نباشد از آب چه حظ</p></div></div>