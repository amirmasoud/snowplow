---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>از نام تو کعبه را چسان نبود ننگ</p></div>
<div class="m2"><p>در کعبه تو و دل بکلیسای فرنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون و درون تو بود چندان رنگ</p></div>
<div class="m2"><p>یا رومی روم باش یا زنگی زنگ</p></div></div>