---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>امروز نه خدمت بتان دین منست</p></div>
<div class="m2"><p>این راه و روش مذهب دیرین منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من عاشق و عشق و رندی و باده‌کشی</p></div>
<div class="m2"><p>دین من و کیش من و آئین منست</p></div></div>