---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>آنم که بود بدامن صحرا باد</p></div>
<div class="m2"><p>تا ره سپرد بکوه پیچد فریاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرگشته چنان بکوه و دشتم بیتو</p></div>
<div class="m2"><p>کز لیلی مجنون وز شیرین فرهاد</p></div></div>