---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>آنرا که زر انباشته صد انبان است</p></div>
<div class="m2"><p>از رفتن ده درم کجا نقصانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کم مایه رود بر سر اندک ضرری</p></div>
<div class="m2"><p>در خانه مور شبنمی طوفانست</p></div></div>