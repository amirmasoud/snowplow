---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>اکنون که بود نشاط دل حاصل ما</p></div>
<div class="m2"><p>آراسته ز اسباب طرب محفل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داریم بکف ز خاک یاران ساغر</p></div>
<div class="m2"><p>پیمانه کند تا که ز مشت گل ما</p></div></div>