---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>دوران سپهر بیگنه کش گذرد</p></div>
<div class="m2"><p>ایام بگوینده و خامش گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عمر هزار سال در یکنفس است</p></div>
<div class="m2"><p>چون میگذرد کو همه ناخوش گذرد</p></div></div>