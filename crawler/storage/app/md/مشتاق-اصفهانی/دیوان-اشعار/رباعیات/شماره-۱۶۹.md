---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>پیدا چو گهر ز قطره آب شدیم</p></div>
<div class="m2"><p>وانگاه نهان چون در نایاب شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودیم بخواب در شبستان عدم</p></div>
<div class="m2"><p>بیدار شدیم و باز در خواب شدیم</p></div></div>