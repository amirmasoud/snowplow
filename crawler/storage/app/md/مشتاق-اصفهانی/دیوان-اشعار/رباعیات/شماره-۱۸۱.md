---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>رفتی تو و اشک لاله‌گون دل من</p></div>
<div class="m2"><p>شد سلسله جنبان جنون دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون گل که دمد ز خاک و خون دل من</p></div>
<div class="m2"><p>خرگاه برون زد از درون دل من</p></div></div>