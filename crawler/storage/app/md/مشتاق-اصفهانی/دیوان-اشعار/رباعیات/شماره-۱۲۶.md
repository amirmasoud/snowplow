---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>از عشق فتد بخرمن صبر آتش</p></div>
<div class="m2"><p>بارد عوض آب از این ابر آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برقیست محبت که ازو افتاده است</p></div>
<div class="m2"><p>در جان مسلمان و دل گبر آتش</p></div></div>