---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>مینای تهی حریف خامی بوده است</p></div>
<div class="m2"><p>پیمانه پر رند تمامی بوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامی که نهاده‌ایم ما لب بلبش</p></div>
<div class="m2"><p>می‌خاره لب بر لب جامی بودهاست</p></div></div>