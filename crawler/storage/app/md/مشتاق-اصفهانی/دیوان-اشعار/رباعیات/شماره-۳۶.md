---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>ای گلبن طفل غنچه پرورده تست</p></div>
<div class="m2"><p>ای گلشن چتر گل سراپرده تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گل دل عندلیب خون کرده تست</p></div>
<div class="m2"><p>ای باد بهار اینهمه آورده تست</p></div></div>