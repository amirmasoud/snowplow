---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>هر ناله که از غمت بلب می‌آرم</p></div>
<div class="m2"><p>زو آتش را بتاب و تب می‌آرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روز جزا بمجرمان صعب‌تر است</p></div>
<div class="m2"><p>روزی که من از غمت بشب می‌آرم</p></div></div>