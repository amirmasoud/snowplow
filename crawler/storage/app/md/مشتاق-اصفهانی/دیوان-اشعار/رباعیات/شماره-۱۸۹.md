---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>ای نور تو شمع بزم مردم گشته</p></div>
<div class="m2"><p>در نور تو ذرات جهان گمگشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این پرتو حسن تست کز پرده برون</p></div>
<div class="m2"><p>افتاده و مهر و ماه و انجم گشته</p></div></div>