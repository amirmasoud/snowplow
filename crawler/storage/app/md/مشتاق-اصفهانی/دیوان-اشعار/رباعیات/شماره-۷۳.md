---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>گردون ستیزه کار دیدی که چکرد</p></div>
<div class="m2"><p>ناسازی روزگار دیدی که چکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حرف رقیب عاقبت خونم ریخت</p></div>
<div class="m2"><p>دیدی که چکرد یار و دیدی که چکرد</p></div></div>