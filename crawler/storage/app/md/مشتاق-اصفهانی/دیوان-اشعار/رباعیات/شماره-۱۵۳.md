---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>پیمان تو کی درست می‌دانستم</p></div>
<div class="m2"><p>عهد تو همیشه سست می‌دانستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من سستی عهد تو مپندار کنون</p></div>
<div class="m2"><p>دانستم کز نخست می‌دانستم</p></div></div>