---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>پیمانه لب پیاله‌نوشی بوده است</p></div>
<div class="m2"><p>خم کالبد باده‌فروشی بوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدبار درین میکده هر مشت گلی</p></div>
<div class="m2"><p>ساغر بکفی سبو بدوشی بوده است</p></div></div>