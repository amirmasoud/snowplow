---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>دور از تو مرا رنج و گلابی که مپرس</p></div>
<div class="m2"><p>دردی که مگو غم و ملالی که مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که چه حال داری از دوری من</p></div>
<div class="m2"><p>دارم ز جدائی تو حالی که مپرس</p></div></div>