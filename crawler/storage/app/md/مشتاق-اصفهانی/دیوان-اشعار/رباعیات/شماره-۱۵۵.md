---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>یک چند منت دوست گمان میکردم</p></div>
<div class="m2"><p>وز شوق فدایت دل و جان میکردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تیغ جفا عاقبتم کشتی و کاش</p></div>
<div class="m2"><p>زآغاز ترا من امتحان میکردم</p></div></div>