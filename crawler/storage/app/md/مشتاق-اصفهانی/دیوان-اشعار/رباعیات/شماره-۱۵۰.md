---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>رندی به بغل صراحی و در کف جام</p></div>
<div class="m2"><p>با واعظ شهر گفت کای شیخ انام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون هر دو بود زآب عنبر و سرکه و می</p></div>
<div class="m2"><p>آن بهر چه شد حلال و این گشت حرام</p></div></div>