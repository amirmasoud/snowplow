---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>آنرا که نه دانش و نه عرفان باشد</p></div>
<div class="m2"><p>حاشا که ز معصیت گریزان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جرم و گنه بیخبران را چه گریز</p></div>
<div class="m2"><p>کج‌کج رفتن لازم مستان باشد</p></div></div>