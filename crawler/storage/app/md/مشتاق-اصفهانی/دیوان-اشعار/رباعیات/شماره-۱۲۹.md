---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه کنم خاطر غمدیده خویش</p></div>
<div class="m2"><p>شاد از قدم یار پسندیده خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند آه کشم اشک فشانم برهش</p></div>
<div class="m2"><p>گاه از دل خویش و گاه از دیده خویش</p></div></div>