---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>فریاد ز طبع جرم زاینده ما</p></div>
<div class="m2"><p>وز نفس به بد راه نماینده ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت آنچه ز عمر ما به بدکاری رفت</p></div>
<div class="m2"><p>آه ار گذرد چو رفته آینده ما</p></div></div>