---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>رفت از برم آن یگانه دیدی که چه کرد</p></div>
<div class="m2"><p>دلبر به سر بهانه دیدی که چه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افکند به صد مرحله‌ام دور از یار</p></div>
<div class="m2"><p>دور فلک و زمانه دیدی که چه کرد</p></div></div>