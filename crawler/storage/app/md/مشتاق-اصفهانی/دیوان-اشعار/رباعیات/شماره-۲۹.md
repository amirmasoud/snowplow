---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>هر ذره ز خاک تاجداری بوده است</p></div>
<div class="m2"><p>هر نقش قدم بزرگواری بوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گرد که بر باد سوار است امروز</p></div>
<div class="m2"><p>پیداست که وی شاهسواری بوده است</p></div></div>