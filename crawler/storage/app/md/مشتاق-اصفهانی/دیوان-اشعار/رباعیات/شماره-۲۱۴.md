---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>فاش از کفم ار کشیده دامن بودی</p></div>
<div class="m2"><p>چون روح مرا نهفته در تن بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در طلب تو وز تو غافل یعنی</p></div>
<div class="m2"><p>من با تو نبودم و تو با من بودی</p></div></div>