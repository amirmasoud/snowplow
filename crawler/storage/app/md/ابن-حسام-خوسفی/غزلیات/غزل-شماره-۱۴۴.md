---
title: >-
    غزل شمارهٔ ۱۴۴
---
# غزل شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>بی نیازی از نیاز ما چه استغناست این</p></div>
<div class="m2"><p>جور کم کن بر دلم کآخر نه از خاراست این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم ای سرو سهی بنشین که بنشیند بلا</p></div>
<div class="m2"><p>گفت بنشینم ولیکن نه بلا بالاست این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت رنگت سرخ دیدم این نه رنگ عاشقی است</p></div>
<div class="m2"><p>گفتمش فیض دموع چشم خون پالاست این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم آن مشک سیه بر دامن خورشید چیست؟</p></div>
<div class="m2"><p>گفت بر برگ گل تر عنبر سار است این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم از خون دلم گلگونه رنگین کرده ای</p></div>
<div class="m2"><p>گفت بر نسرین نشان لاله حمراست این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش چشمت برد از من دل و آرام و هوش</p></div>
<div class="m2"><p>گفت ترک مست را اندیشه یغماست این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت کام از لعل من می بایدت ابن حسام</p></div>
<div class="m2"><p>گفتم آری طعنه طوطی شکر خاست این</p></div></div>