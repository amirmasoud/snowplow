---
title: >-
    غزل شمارهٔ ۱۳۹
---
# غزل شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>چو زلف خود فرو مگذار کارم </p></div>
<div class="m2"><p>که چون زلفت پریشان روزگارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یاد لعل شیرینت چو فرهاد</p></div>
<div class="m2"><p>بتلخی روزگاری می گذارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز نقش رخت نیکو نیاید</p></div>
<div class="m2"><p>ز هر نقشی که نیکو می نگارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیارم بر زبان اورد نامت</p></div>
<div class="m2"><p>که گر یارم بشب خفتن نیارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درازی شب هجران ز من پرس</p></div>
<div class="m2"><p>که شب تا روز اختر می شمارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسوزد مشعل تابنده ماه</p></div>
<div class="m2"><p>گر از سوز درون آهی برآرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تاب هجر زلفت چون بنفشه</p></div>
<div class="m2"><p>سر از زانوی حسرت بر نیارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا ساقی ز چشم نیمه مستم</p></div>
<div class="m2"><p>قدح پر کن که در عین خمارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی یابم ز گلزار تو بویی</p></div>
<div class="m2"><p>ز غمزه می زنی بر دیده خارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رخ ابن حسام و خاک راهت</p></div>
<div class="m2"><p>که در راهت جز این رویی ندارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا استاد عشقت نکته دان کرد</p></div>
<div class="m2"><p>که رحمت باد بر آموزگارم</p></div></div>