---
title: >-
    غزل شمارهٔ ۱۰۱
---
# غزل شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>بیا که مجلس انس است و دلستان جان بخش</p></div>
<div class="m2"><p>همی کند ز گلزرا قدس ریحان بخش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا که هدهد هادی به لحن داوودی</p></div>
<div class="m2"><p>حدیث می کند از منطق سلیمان بخش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیا رسیده به سر چشمه ی زلال وصال</p></div>
<div class="m2"><p>به عاشقان جگر تشنه آب حیوان بخش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رهروان بیابان وادی ایمان</p></div>
<div class="m2"><p>ز طور قرب شب تیره نور غرفان بخش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز فیض لم یزلی آنچه عین بهبودست</p></div>
<div class="m2"><p>کرشمه ای کن و از لطف خود مرا آن بخش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ما به درد و دوای تو از تو خرسندیم</p></div>
<div class="m2"><p>تو خواه درد بیفزای و خواه درمان بخش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیاهکاری ابن حسام سرگردان</p></div>
<div class="m2"><p>به جعد تافته ی طرّه پریشان بخش</p></div></div>