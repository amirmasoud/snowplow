---
title: >-
    غزل شمارهٔ ۲۰
---
# غزل شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>حسنت که آفتاب تجلی از او گرفت</p></div>
<div class="m2"><p>یک جلوه کرد و مملکت دل فرو گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک تار از آن دو سنبل پرچین به چین رسید</p></div>
<div class="m2"><p>از زلف مشک بوی تو در مشک بو گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل اعتکاف کوی تو دارد بر او مگیر</p></div>
<div class="m2"><p>مرغ حریم کعبه نباشد برو گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این آهوی رمیده که اندر کمند تست</p></div>
<div class="m2"><p>او را مران که با سگ کوی تو خو گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم سخن ز کوی تو گویم به خنده گفت</p></div>
<div class="m2"><p>بگذار گفت و گو که جهان گفت و گو گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مه با عذار یار برابر همی نمود</p></div>
<div class="m2"><p>زلفش به شیوه شد طرف روی او گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر تا به پای هستی ابن حسام سوخت</p></div>
<div class="m2"><p>آه این چه آتش است که ناگه درو گرفت</p></div></div>