---
title: >-
    غزل شمارهٔ ۹۰
---
# غزل شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>آتش مهرتو در سینه نهان است هنوز</p></div>
<div class="m2"><p>خون دل از گذر دیده روان است هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگران رخ زیبای تو شد دیده و دل</p></div>
<div class="m2"><p>همچنانم دل ودیده نگران است هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمزه ات می دهدم عشوه که من آن توام</p></div>
<div class="m2"><p>چون بدیدم نظرش با دگران است هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ازل عکس جمالت به گلستان بردند</p></div>
<div class="m2"><p>بلبل از شوق رخت نمره زنان است هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان شمایل خبری باد به بستان آورد</p></div>
<div class="m2"><p>در چمن سرو سهی رقص کنان است هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از یقین دهنت هیچ نمی یارم گفت</p></div>
<div class="m2"><p>کانچه گویم همه در عین گمان است هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل که اندر شکن زلف تو بست ابن حسام</p></div>
<div class="m2"><p>مشکن آن را که دلش بسته ی آن است هنوز</p></div></div>