---
title: >-
    غزل شمارهٔ ۱۷۶
---
# غزل شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>آن زلف مشَّوش وش اندر خم و تاب اولی</p></div>
<div class="m2"><p>وآن نرگس خوش منظر مخمور و خراب اولی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مشک و گل و نسرین خوش منظر و خوش بویند</p></div>
<div class="m2"><p>بر عارض گلگونت بر مشک و نقاب اولی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیده من چهرت در سینه من مهرت</p></div>
<div class="m2"><p>هم دیده و هم سینه بی این دو کباب اولی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون فصل بهار آید گل در صف یار آید</p></div>
<div class="m2"><p>خوش گوی غزل خواند در چنگ و رباب اولی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وجه می صافی سجاده من بفروش</p></div>
<div class="m2"><p>کاین خرقه که من دارم در رهن و شراب اولی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این نامه بی ناموس در خم می اندارید</p></div>
<div class="m2"><p>کاین دفتر بی معنی غرق می ناب اولی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هان ابن حسام امشب خوش گفتی و دُر سفتی</p></div>
<div class="m2"><p>شعر تر حافظ را اینگونه جواب اولی</p></div></div>