---
title: >-
    غزل شمارهٔ ۱۰۴
---
# غزل شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>نگاری دلبری دارم چو زلف خود ز من سرکش</p></div>
<div class="m2"><p>به جان قربان شدم او را نمیگیرد دلم ترکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حد بگذشت مشتاقی به جام باده ی باقی</p></div>
<div class="m2"><p>لبالب کن قدح ساقی به یاد لعل او درکش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آن سرو سیم اندام اگر در بر کشی روزی</p></div>
<div class="m2"><p>نه قد سرو دلجو جو و نه ناز صنوبر کش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر آن آیینه روی از روی مهرت روی بنماید</p></div>
<div class="m2"><p>نه روی ملک دارا بین و نه حکم سکندر کش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو ای زاهد خودبین مه دایم عیب می بینی</p></div>
<div class="m2"><p>قلم در حرف رندان پریشان قلندر کش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزن چرخی و زین چنبر برون نه پای همت را</p></div>
<div class="m2"><p>کزین بیرون بنه پای و قلم در چرخ و چنبر کش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا ابن حسام اول چو در کوی نکونامی</p></div>
<div class="m2"><p>نشد ممکن گذر کردن به بدنامی علم در کش</p></div></div>