---
title: >-
    غزل شمارهٔ ۱۳۰
---
# غزل شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>دوش با زلفت به هم شوریده حالی داشتیم</p></div>
<div class="m2"><p>در سر از سودای ابرویت خیالی داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط ابروی کجت در چشم من پیوسته بود</p></div>
<div class="m2"><p>راست گویی در نظر شکل هلالی داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه با یاد دهانت عیش بر ما تنگ بود</p></div>
<div class="m2"><p>هر دم از شوق لبت شیرین مقالی داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند روزی پای بند کلبه ی آب و گلم</p></div>
<div class="m2"><p>من که همچون طایران سدره بالی داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرّما آن روز کان خورشید بر من تافتی</p></div>
<div class="m2"><p>حبّذا آن شب که با آن مه وصالی داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خوشا وقتی که ساقی وقت من خوش داشتی</p></div>
<div class="m2"><p>وز کف او چون می کوثر زلالی داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاد باد آن روزگار خوش که چون ابن حسام</p></div>
<div class="m2"><p>گاه گاهی بر سر کویت مجالی داشتم</p></div></div>