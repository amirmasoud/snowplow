---
title: >-
    غزل شمارهٔ ۵۹
---
# غزل شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>باحسن تو مه در صف دعوی ننشیند</p></div>
<div class="m2"><p>باصورت خوب تو به معنی ننشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردور هلالی به جز از چشم تومستی</p></div>
<div class="m2"><p>درگوشه ی محراب به تقوی ننشیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز لعل تو ترسا بچه کان آب حیاتست</p></div>
<div class="m2"><p>کس درصدد معجز عیسی ننشیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موسی صفت آنکس که به میقات نیاید</p></div>
<div class="m2"><p>برطور دلش نور تجلی ننشیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک ره آنم که برو گرد تعلق</p></div>
<div class="m2"><p>از رهگذر عشوه ی دنیی ننشیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرسبز کسی باد که از رنگ زمرّد</p></div>
<div class="m2"><p>پرهیزد و اندر دم افعی ننشیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ابن حسام از سرکوی تو خبر یافت</p></div>
<div class="m2"><p>شرط است که در روضه ی اعلی ننشیند</p></div></div>