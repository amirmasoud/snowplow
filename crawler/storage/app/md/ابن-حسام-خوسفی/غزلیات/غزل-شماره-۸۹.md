---
title: >-
    غزل شمارهٔ ۸۹
---
# غزل شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>می بیارید که ایام بهار است امروز</p></div>
<div class="m2"><p>نرگس از ساغر زر جرعه گسار است امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدهٔ خوش نظر باغ خمار آلود است</p></div>
<div class="m2"><p>قدح لاله پر از نوش گوار است امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم نسیم چمن از باغ بُخور انگیز است</p></div>
<div class="m2"><p>هم شمیم سحری مجمره دار است امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل خوشبوی نشان می‌دهد از طلعت دوست</p></div>
<div class="m2"><p>سرو دلجوی تو گویی قد یار است امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چمن از لاله و از سنبل تر پنداری</p></div>
<div class="m2"><p>راست مانند رخ و زلف نگار است امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی گذر کرد ندانیم به فردا که رسد</p></div>
<div class="m2"><p>حیف از این لحظه که در عین گذار است امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چنین فصل که گفتم سخن ابن حسام</p></div>
<div class="m2"><p>از لب مطرب خوش لهجه به کار است امروز</p></div></div>