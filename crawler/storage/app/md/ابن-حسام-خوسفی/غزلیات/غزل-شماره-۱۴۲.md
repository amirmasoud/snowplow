---
title: >-
    غزل شمارهٔ ۱۴۲
---
# غزل شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>گرچه بس منفعل از شرم گناه آمده‌ایم</p></div>
<div class="m2"><p>تکیه بر مرحمت لطف اله آمده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست در دامن ملاح عنایت زده‌ایم</p></div>
<div class="m2"><p>ما بدین بحر نه از بحر شناه آمده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رقم جرم و گناه از صفحات عملم</p></div>
<div class="m2"><p>محو فرمای که بس نامه سیاه آمده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهن از سوز درون خشک و رخ از دیده پر آب</p></div>
<div class="m2"><p>به انابت بدرت با دو گواه آمده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به اعزاز چو یوسف به عزیزی برسیم</p></div>
<div class="m2"><p>ما بدین مصر به تاریکی چاه آمده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جای آن هست که دریوزه کنیم ابن‌حسام</p></div>
<div class="m2"><p>بر سر راه چو بی‌توشهٔ راه آمده‌ایم</p></div></div>