---
title: >-
    غزل شمارهٔ ۱۵
---
# غزل شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>پوشد ز زشک یلمق تو اطلس آفتاب</p></div>
<div class="m2"><p>پیش رخ تو ذره بود واپس آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز زلف تو که سجد کند آفتاب را</p></div>
<div class="m2"><p>در دور حسن تو نپرستد کس آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر آفتاب تیره شود با کمال نور</p></div>
<div class="m2"><p>یک لمعه از لقای تو ما را بس آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم مگر به سر رسدم آفتاب وصل</p></div>
<div class="m2"><p>در طالعه نبود بدین سدرس آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مهر طلعت تو بر ابن حسام تافت</p></div>
<div class="m2"><p>شاید که تافت بر سمن و بر خس آفتاب</p></div></div>