---
title: >-
    غزل شمارهٔ ۱۶۰
---
# غزل شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>بلبل از شاخ گل زند هوهو</p></div>
<div class="m2"><p>نغمه کبک و بانگ تیهو هو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذر وقت گل به باغ بهار</p></div>
<div class="m2"><p>بشنو از مرغزار آهو هو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در رخ و زلف آن نگار نگر</p></div>
<div class="m2"><p>تا بگویند ترک و هندو هو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می نماید بعینه گویی</p></div>
<div class="m2"><p>زان میان دو چشم جادو هو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم و ابرو و غمزه دلجویند</p></div>
<div class="m2"><p>فَتحا شَیت لَن یَضلُّوا هو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنگر ابن حسام از چپ و راست</p></div>
<div class="m2"><p>بِشِنو ، دم به دم ز هر سو هو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما هم او و او همه ماییم</p></div>
<div class="m2"><p>هو و هوهو و هو و هوهو</p></div></div>