---
title: >-
    غزل شمارهٔ ۱۶۷
---
# غزل شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>شبی به پیش تو خواهم نشست روی بروی</p></div>
<div class="m2"><p>تطاول سر زلفت بگفت موی به موی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بوی زلف تو آشفته حال می گردم</p></div>
<div class="m2"><p>بسان باد صبا در ره تو کوی به کوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسوی صومعه گاهی ، گهی بسوی کنشت</p></div>
<div class="m2"><p>همی روم به طلب در پی تو سوی به سوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشان سرو تو از جویبار می جویم</p></div>
<div class="m2"><p>چو آب از این سببم سر نهاده جوی به جوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز گفت و گوی عواقب مگوی ابن حسام</p></div>
<div class="m2"><p>بیاد غبغب جانان سخن ز گوی بگوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شدن به جانب چین بهر مشک عین خطاست</p></div>
<div class="m2"><p>بجای مشک تو ان زلف موشک بوی به بوی</p></div></div>