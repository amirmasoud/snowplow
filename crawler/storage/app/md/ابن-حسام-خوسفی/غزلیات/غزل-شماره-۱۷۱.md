---
title: >-
    غزل شمارهٔ ۱۷۱
---
# غزل شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>خوشا آن دل که جانانش تو باشی</p></div>
<div class="m2"><p>خنک باغی که ریحانش تو باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رشک آید قد طوبی در آن باغ</p></div>
<div class="m2"><p>که سرو ناز پستانش تو باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علاج درد بی درمان نجوید</p></div>
<div class="m2"><p>دوا جوئی که درمانش تو باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خبر ها می دهد هدهد دگر بار</p></div>
<div class="m2"><p>سبا را تا سلیمانش تو باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن مجلس شکر ریزد به خروار</p></div>
<div class="m2"><p>که طوطی سخن دانش تو باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا ابن احسام این مرتبت بس</p></div>
<div class="m2"><p>که جانانش تو و جانش تو باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سزد گر بر همه خوبان کند ناز</p></div>
<div class="m2"><p>بتی کالحق غزلخوانش تو باشی</p></div></div>