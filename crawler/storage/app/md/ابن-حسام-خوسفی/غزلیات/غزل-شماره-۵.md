---
title: >-
    غزل شمارهٔ ۵
---
# غزل شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ای کعبهٔ جان خاک سر کوی تو ما را</p></div>
<div class="m2"><p>محراب دل اندر خم ابروی تو ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر بار که پای از سر کوی تو کشم باز</p></div>
<div class="m2"><p>پابست کند باز سر موی تو ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در راه تو خون دل عشاق سبیل است</p></div>
<div class="m2"><p>گو چشم تو خون ریز به یرغوی تو ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز نقش تو در دیده خیالی که در آید</p></div>
<div class="m2"><p>از سر ببرد نرگس جادوی تو ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مملکت حسن ز هر وجه که خوب است</p></div>
<div class="m2"><p>در چشم نیاید به جز از روی تو ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان روی که از سلسله اهل جنونیم</p></div>
<div class="m2"><p>زنجیر کند حلقه گیسوی تو ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افتاده چشم سیهت ابن حسام است</p></div>
<div class="m2"><p>زان روز که افتاده نظر سوی تو ما را</p></div></div>