---
title: >-
    غزل شمارهٔ ۱۵۳
---
# غزل شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>آن سرو ناز کو که ببوسیم پای او</p></div>
<div class="m2"><p>روشن کنیم دیده به خاک سرای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او سر زناز خویش نیارد بما فرود</p></div>
<div class="m2"><p>ما چون بنفشه سر بنهاده به پای او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او را به جای ما به غلط گر کسی بود</p></div>
<div class="m2"><p>ما را کسی نبود و نباشد به جای او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او گر جفا و جور کند بر دلم چه باک</p></div>
<div class="m2"><p>ما دل نهاده ای به جور و جفای او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او گر رضای خاطر ما را نگه نداشت</p></div>
<div class="m2"><p>ما بنده ایم خاطر ما و رضای او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او گر گدای درگه خود را ز در براند</p></div>
<div class="m2"><p>آیا کجا رود ز در او گدای او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او گلبنی است تازه ز گلشن سرای جان </p></div>
<div class="m2"><p>ابن حسام بلبل دستان سرای او</p></div></div>