---
title: >-
    غزل شمارهٔ ۲۴
---
# غزل شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>مرا درد تو دایم هم نشین است</p></div>
<div class="m2"><p>غمت پیوسته با جانم قرین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوس دارم که در پای تو میرم</p></div>
<div class="m2"><p>تمنای من از دولت همین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظر بر پسته تنگ تو دارم</p></div>
<div class="m2"><p>که چشم من به غایت خرده بین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عذار از دود آه من نگهدار</p></div>
<div class="m2"><p>که آه سوزناکم آتشین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیالت بر سواد دیده من</p></div>
<div class="m2"><p>انیس مردم دریا نشین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهفته گوشه چشمی به ما کن</p></div>
<div class="m2"><p>که هر گوشه رقیبی در کمین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر ابن حسام و خاک کویت</p></div>
<div class="m2"><p>که لطفش خوشتر از ماء معین است</p></div></div>