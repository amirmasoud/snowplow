---
title: >-
    غزل شمارهٔ ۹۳
---
# غزل شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>بیا به میکده بفروش خرقهٔ ناموس</p></div>
<div class="m2"><p>ریا و شمعه رها کن به زاهد سالوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حریف مصطبه و ساغرم به بانگ بلند</p></div>
<div class="m2"><p>به زیر پرده از این پس دگر نگویم کوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک به دست ستم بین که زیر پای بکوفت</p></div>
<div class="m2"><p>سر سریر فریدون و افسر کاووس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبیب شهر علاج دام نمی‌داند</p></div>
<div class="m2"><p>کزین معالجه دورست فهم جالینوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهال قد تو بر سرو می‌نماید ناز</p></div>
<div class="m2"><p>گل عذار تو بر لاله می‌کند افسوس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر زمین که غباری ز موکبت برسد</p></div>
<div class="m2"><p>دهم به وجه ارادت بر آن زمین صد بوس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال نظم تو ابن حسام تا چه کند</p></div>
<div class="m2"><p>که پای بند غروری تو نیز چون طاووس</p></div></div>