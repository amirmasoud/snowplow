---
title: >-
    غزل شمارهٔ ۱۰۷
---
# غزل شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>به وقت فصل بهار از چمن مکن اعراض</p></div>
<div class="m2"><p>جهان ز لاله و گل بین به رنگ و بوی ریاض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان حوضهٔ چشمم ز خون برست گیاه</p></div>
<div class="m2"><p>چنانکه لالهٔ سیراب بر کنار حیاض</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمامه ی سر زلفت که شام رعناییست</p></div>
<div class="m2"><p>چو باد صبح فرح بخش و داف الاَمراض</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هر خیال و تصور که غیر دوست بود</p></div>
<div class="m2"><p>ضمیر صافی ابن حسام شد مرتاض</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا ز کعبه و بتخانه کوی اوست غرض</p></div>
<div class="m2"><p>همین وسیله تمامم ز جملگی اعراض</p></div></div>