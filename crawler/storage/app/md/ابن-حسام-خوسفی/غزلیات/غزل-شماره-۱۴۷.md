---
title: >-
    غزل شمارهٔ ۱۴۷
---
# غزل شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>رخسار تو بی نقاب دیدن</p></div>
<div class="m2"><p>یک شب نتوان به خواب دیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رویی که حجاب آفتاب است</p></div>
<div class="m2"><p>کی شاید بی حجاب دیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیده ی ما خیال رویت</p></div>
<div class="m2"><p>چون مه بتوان در آب دیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در روی تو چشم خیره گردد</p></div>
<div class="m2"><p>نتوان رخ آفتاب دیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم تو خراب کرده دل را</p></div>
<div class="m2"><p>تا چند توان عتاب دیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر بتوان بعین رحمت</p></div>
<div class="m2"><p>یکبار بدین خراب دیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باریک دقیقه ای ست اینجا</p></div>
<div class="m2"><p>در موی تو پیچ و تاب دیدن</p></div></div>