---
title: >-
    غزل شمارهٔ ۳۴
---
# غزل شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>قد چو سرو تو بر جویبار دیده ماست</p></div>
<div class="m2"><p>غبار راه تو بر رهگذار دیده ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آرزوی لبت خون گرفت خانه چشم</p></div>
<div class="m2"><p>خیال لعل تو گلشن نگار دیده ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو نرگسی که دمد بر کنار چشمه آب</p></div>
<div class="m2"><p>سواد چشم تو در چشمه سار دیده ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلا کشید دل ودیده اختیار تو کرد</p></div>
<div class="m2"><p>بلای دل همه از اختیار دیده ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سرو و لاله چه خوانی مرا ز باغ رخت</p></div>
<div class="m2"><p>قد تو سرو و رخت لاله زار دیده ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکشت چشم تو ابن حسام را گفتم</p></div>
<div class="m2"><p>به غمزه گفت که این کار کار دیده ماست</p></div></div>