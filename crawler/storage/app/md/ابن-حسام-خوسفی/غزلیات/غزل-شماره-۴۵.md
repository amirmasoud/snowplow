---
title: >-
    غزل شمارهٔ ۴۵
---
# غزل شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>گر صد برگ را روی تو وارث</p></div>
<div class="m2"><p>شمیم مشک را موی تو وارث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هر نرگس که او جادو فریب است</p></div>
<div class="m2"><p>فریب چشم جادوی تو وارث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هر سنبل که بر نسرین کند ناز</p></div>
<div class="m2"><p>نسیم جعد گیسوی تو وارث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آن هندو که بر ابرو نشیند</p></div>
<div class="m2"><p>سواد زلف هندوی تو وارث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سروی کان به باغ راستان است</p></div>
<div class="m2"><p>قد چون سرو دلجوی تو وارث</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمان مشک را بر تخته سیم</p></div>
<div class="m2"><p>جبین و خط ابروی تو وارث</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خوش گویان همه ابن حسام است</p></div>
<div class="m2"><p>زبان و طبع خوشگوی تو وارث</p></div></div>