---
title: >-
    غزل شمارهٔ ۱۳۷
---
# غزل شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>بس که یاد آن لب و دندان چون دُر می‌کنم</p></div>
<div class="m2"><p>دامن از اشک چو مروارید تر پُر می‌کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سال‌ها سودای ابروی تو در سر داشتم</p></div>
<div class="m2"><p>بار دیگر آن خیال کج تصور می‌کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از وجودم تا عدم مویی نماند در میان</p></div>
<div class="m2"><p>در میانه چون به باریکی تفکُّر می‌کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد را مگذار بر زلفت وزیدن زانکه گر</p></div>
<div class="m2"><p>در سر زلف تو پیچد من تغیُّر می‌کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دهی فخرم به مقدار سگان کوی خویش</p></div>
<div class="m2"><p>من بدین مقدار بسیاری تفاخُر می‌کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شود پروانه شمع رخت ابن حسام</p></div>
<div class="m2"><p>روی سوی روشنایی چون سمندُر می‌کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جبرئیل از منتهای سدره آمین می‌کند</p></div>
<div class="m2"><p>چون دعای شاه عادل بایسنقر می‌کنم</p></div></div>