---
title: >-
    غزل شمارهٔ ۲۲
---
# غزل شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>سنبل تر دمیده بر گل دوست</p></div>
<div class="m2"><p>بوی گل می‌دمد ز سنبل دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد عنبر شمیم می‌گذرد</p></div>
<div class="m2"><p>یافت بویی مگر ز کاکل دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر تجمل که هست در خورشید</p></div>
<div class="m2"><p>ذره ای نیست با تجمل دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جفا از درش نخواهد رفت</p></div>
<div class="m2"><p>دوستان را بود تحمّل دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کسی راه توشه‌ای بردند</p></div>
<div class="m2"><p>ما برفتیم بر توکل دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصهٔ زلف او دراز مکش</p></div>
<div class="m2"><p>که درازست خود تطاول دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این رساله ز شعر ابن حسام</p></div>
<div class="m2"><p>یاد می‌دار از ترسل دوست</p></div></div>