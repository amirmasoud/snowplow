---
title: >-
    غزل شمارهٔ ۸۲
---
# غزل شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>گرملک بنگرد آن چشم خوش و لعل لذیذ</p></div>
<div class="m2"><p>دفع نظّارهٔ بد را بنویسد تعویذ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام جم دور جهان را فلک از یاد ببرد</p></div>
<div class="m2"><p>بده ای خسرو خوبان به من آن جام نبیذ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیت حسن که در شان رخت نازل شد</p></div>
<div class="m2"><p>بی‌مثال خط زلف تو نیابد تنفیذ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه برداشت ترا کی فکند ابن حسام</p></div>
<div class="m2"><p>نیست برداشتگان را ز کریمان تنبیذ</p></div></div>