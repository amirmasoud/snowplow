---
title: >-
    غزل شمارهٔ ۹۶
---
# غزل شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>دوش از فراغ روی تو با روی سندروس</p></div>
<div class="m2"><p>نالیده‌ام چو نای و فغان کرده‌ام چو کوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی به وعده دوش که کام از لبت دهم</p></div>
<div class="m2"><p>افسوس ازین سخن که لبت می‌کند فسوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن را که پای‌بوس میسّر نمی‌شود</p></div>
<div class="m2"><p>خود دست کی دهد که کند با تو دست بوس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیینه پیش دار و در ابروی خود نگر</p></div>
<div class="m2"><p>بر عاج بین کشیده کمانی ز آبنوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می ده که دیر و زود عظام رمیم من</p></div>
<div class="m2"><p>دستاس سالخورده گردون کند سبوس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرخ از چه شد کرانه این طشت نقره‌کوب</p></div>
<div class="m2"><p>خون سیاوش است در او یا سرشک طوس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابن حسام دل چه نهی در فریب دهر</p></div>
<div class="m2"><p>پرهیز کن ز عشوه دامادکش عروس</p></div></div>