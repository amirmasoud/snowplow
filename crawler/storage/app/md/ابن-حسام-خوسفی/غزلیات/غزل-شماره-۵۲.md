---
title: >-
    غزل شمارهٔ ۵۲
---
# غزل شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>مگر چو دردکشان جام بی ریا بخشند</p></div>
<div class="m2"><p>زکاس لَم یَزلی جرعه ای به ما بخشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قتیل عشق شو ای جان که مر ترا روزی</p></div>
<div class="m2"><p>زنوش داروی نوشین لبان شفا بخشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوای درد، طبیبان عشق می دانند</p></div>
<div class="m2"><p>ترا که درد نباشد کجا دوا بخشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که سعی نماید به کعبه ی مقصود</p></div>
<div class="m2"><p>عجب نباشد اگر مر ورا صفا بخشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایا دلا که اگر آزری طمع دارد</p></div>
<div class="m2"><p>که جرم او به جوانان پارسا بخشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امیدوار چنانم که جرم ابن حسام</p></div>
<div class="m2"><p>به گرد کوکبه ی شاه لافتی بخشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خطای این سر شوریده ی پریشان حال</p></div>
<div class="m2"><p>به جعد گیسوی مشکین مصطفی بخشند</p></div></div>