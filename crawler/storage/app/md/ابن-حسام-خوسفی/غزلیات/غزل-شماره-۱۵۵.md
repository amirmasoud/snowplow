---
title: >-
    غزل شمارهٔ ۱۵۵
---
# غزل شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>دل را حضور نیست دمی بی حضور او</p></div>
<div class="m2"><p>خرم دلی که شاد بود با سرور او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پویندگان وادی ایمن توقُّفی</p></div>
<div class="m2"><p>باشد که لمعه ای بدرخشید ز نور او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالک به اهتمام ارادت وصال یافت</p></div>
<div class="m2"><p>موسی و ذرّه ای ز تجلّی و طور او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درس زبور عشق به عشّاق می دهند</p></div>
<div class="m2"><p>داوود را ترنّم زیر زبور او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن را که در بهشت لقا وعده کرده اند</p></div>
<div class="m2"><p>او را چه التفات به حور و قصور او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اسرار دنیوی چه متاعیست پر غرور</p></div>
<div class="m2"><p>هان تا مگر نفریبد غرور او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابن حسام تا نشوی ملتغت به غیر</p></div>
<div class="m2"><p>پرهیز کن ز آتش قهر غیور او</p></div></div>