---
title: >-
    غزل شمارهٔ ۶۴
---
# غزل شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>آن‌ها که طرف روز به گیسو گرفته‌اند</p></div>
<div class="m2"><p>مه را به تاب طرّهٔ هندو گرفته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسونگرند گوشه‌نشینان چشم تو</p></div>
<div class="m2"><p>دل‌ها به سحر غمزهٔ جادو گرفته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یرغوچیان چشم سیاهت به یک نظر</p></div>
<div class="m2"><p>ملک دل خراب به یرغو گرفته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تواچیان ابروی شوخت هزار دل</p></div>
<div class="m2"><p>هردم بدان کمانچهٔ ابرو گرفته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردی به غمزه قصد دل ناحفاظ من</p></div>
<div class="m2"><p>ترکان مست بین که به لرغو گرفته‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوبان بام چرخ ز رشک جمال تو</p></div>
<div class="m2"><p>هر بامداد پرده فرارو گرفته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دُردی‌کشان جرعهٔ خمخانهٔ الست</p></div>
<div class="m2"><p>مستی ز جان بادهٔ یاهو گرفته‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مستان جام لَم یَزلی از شراب عشق</p></div>
<div class="m2"><p>بزم طرب به بانگ هیاهو گرفته‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابن حسام در طلبت سعی می‌کند</p></div>
<div class="m2"><p>آری مراد را به تکاپو گرفته‌اند</p></div></div>