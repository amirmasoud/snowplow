---
title: >-
    غزل شمارهٔ ۱۰۹
---
# غزل شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>دل به ره باز نیامد به فسون وعّاظ</p></div>
<div class="m2"><p>زان که چون خشم فسون خوان تواش نیست حفاظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزه هر لحظه به خونریز دلم تیز مکن</p></div>
<div class="m2"><p>قَد کَفانِی قَتَلتَنی زَمَراتِ الالحاظ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم خوش خواب تو شد راهزن بیداران</p></div>
<div class="m2"><p>همچنان خواب خوشش باد و مبادش ایقاظ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو نرمی کنی امروز و درشتی نکنی</p></div>
<div class="m2"><p>لایضُرُّونک فی الحَشر شَدادُ و غَلاظ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبه گر می طلبی بر سخن ابن حسام</p></div>
<div class="m2"><p>اِنَّها خالیةُ عَن شُبهاتِ الاَلفاظ</p></div></div>