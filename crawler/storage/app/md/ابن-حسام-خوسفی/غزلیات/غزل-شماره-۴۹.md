---
title: >-
    غزل شمارهٔ ۴۹
---
# غزل شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>بشکفت بر چمن گل عذرا عذار سرخ</p></div>
<div class="m2"><p>وز جرم لاله شد کمر کوهسار سرخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون خط تو شد طرف جویبار سبز</p></div>
<div class="m2"><p>وز لاله صحن باغ چو رخسار یار سرخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطرب بساز پرده ی عشاق درحجاز</p></div>
<div class="m2"><p>ساقی تو نیز عذر میار و می آر سرخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردون نگر که دامن این هفت خوان کند</p></div>
<div class="m2"><p>هرشب به خون دیده ی اسفندیار سرخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عطف هلالی فلکی بین که کرده اند</p></div>
<div class="m2"><p>از خون خسروان فلک اقتدار سرخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا خون نکرد در جگر غنچه ی روزگار</p></div>
<div class="m2"><p>گلگونه ی چمن نشد از روزگار سرخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر رهگذار دیده ز خون بسته ام دو جوی</p></div>
<div class="m2"><p>ای بس که کرده ام رخ ازین رهگذار سرخ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از حسرت لب تو کند دیده دم به دم</p></div>
<div class="m2"><p>رخسار زرد من چون گل نوبهار سرخ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کام از لبت که وعده ی ابن حسام بود</p></div>
<div class="m2"><p>چشمش نگر که چون شد از این انتظار سرخ</p></div></div>