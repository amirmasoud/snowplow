---
title: >-
    غزل شمارهٔ ۷۱
---
# غزل شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>هرشب از طوفان چشم آب از سرما بگذرد</p></div>
<div class="m2"><p>مردم چشمم ندانم چو ز دریا بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشک خون آلوده ی من با شفق همدم شود</p></div>
<div class="m2"><p>آه مینا گون من زین سقف مینا بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ندیدی زحمتی از خار مژگان پای دوست</p></div>
<div class="m2"><p>دیده مفرش کردمی در راه تا وا بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شب از آشوب قدش صدبلابالا شود</p></div>
<div class="m2"><p>برگذرگاهی اگر آن سرو بالا بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل ز خجلت خوی کند، نرگس سراندازد به پیش</p></div>
<div class="m2"><p>بر چمن گر قامت آن شوخ رعنا بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دماغ باد ناید بوی ریحان بهشت</p></div>
<div class="m2"><p>گر دمی بر طرف آن زلف سمن سا بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تلخی مردن نبیند آنکه وقت نزع روح</p></div>
<div class="m2"><p>بر زبانش نام آن لعل شکرخا بگذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بنفشه سر برآرم پای بوسش را زخاک</p></div>
<div class="m2"><p>سایه ی سَروش اگر بر تربت ما بگذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خانه ی صبر تو یغما گردد ای ابن حسام</p></div>
<div class="m2"><p>گر خیالی بر دلت زان ترک یغما بگذرد</p></div></div>