---
title: >-
    غزل شمارهٔ ۳۸
---
# غزل شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>ای روشنی دیده دعا می رسانمت</p></div>
<div class="m2"><p>صد بندگی به دست صبا می رسانمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>احرام کعبه سر کوی تو بسته ام</p></div>
<div class="m2"><p>وانگه تحیّتی به صفا می رسانمت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای سرو ناز بر چمن باغ دل بمان</p></div>
<div class="m2"><p>کز آب دیده نشو و نما می رسانمت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آرزوی لعل تو خون می شود دلم</p></div>
<div class="m2"><p>آخر نگفته ای که شفا می رسانمت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پنهان مدار ابن حسام از طبیب،درد</p></div>
<div class="m2"><p>بر وعده ای که گفت دوا می رسانمت</p></div></div>