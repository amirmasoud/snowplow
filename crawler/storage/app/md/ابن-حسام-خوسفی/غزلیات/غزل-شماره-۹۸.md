---
title: >-
    غزل شمارهٔ ۹۸
---
# غزل شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>صبا نشان غبار دیار یار بپرس</p></div>
<div class="m2"><p>دوای چشم ضرر دیده زان غبار بپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار بار گر از یار بر دلم بیش است</p></div>
<div class="m2"><p>به جان یار که او را هزار یار بپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زلف دوست که مجموع او پریشانیست</p></div>
<div class="m2"><p>حکایت من آشفته روزگار بپرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیث دیده ی بی خواب من ز درد فراق</p></div>
<div class="m2"><p>از آن دو نرگس خوش خواب پر خمار بپرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا که زار شدم ز آرزوی دیدن دوست</p></div>
<div class="m2"><p>تن نزار مرا بین و زار زار بپرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آن قرار که دادی مرا به پرسیدن</p></div>
<div class="m2"><p>تلطفی کن و روزی بر آن قرار بپرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علاج درد دل مستمند ابن حسام</p></div>
<div class="m2"><p>از آن مفرح یاقوت آبدار بپرس</p></div></div>