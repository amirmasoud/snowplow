---
title: >-
    غزل شمارهٔ ۱۴۶
---
# غزل شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>ای قامت بلند تو عمر دراز من</p></div>
<div class="m2"><p>محراب ابروی تو محل نماز من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستم چو شمع شب همه شب در گداز و سوز</p></div>
<div class="m2"><p>و آگه نیی ز گریه و سوز و گداز من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رازم به زیر پرده ز مردم نهفته بود</p></div>
<div class="m2"><p>بر رو فکند اشک من از پرده راز من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر قسمتم به کوی خرابات کرده اند</p></div>
<div class="m2"><p>با قسمت ازل چه کند احتراز من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من کار خود چنان که بباید نساختم</p></div>
<div class="m2"><p>باید که لطف دوست شود کارساز من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای سرو خوش خرام بنه سرکشی ز سر</p></div>
<div class="m2"><p>در ناز خود مبین و ببین در نیاز من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا در کشم به دامن همت که عاقبت</p></div>
<div class="m2"><p>سر بر کشد ز جیب حقیقت مجاز من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابن حسام را چو محل پیش بار نیست</p></div>
<div class="m2"><p>خیز ای نسیم و عرضه کن از من نیاز من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باشد به نیم جرعه کند کار من تمام</p></div>
<div class="m2"><p>ساقی میر مجلس مسکین نواز من</p></div></div>