---
title: >-
    غزل شمارهٔ ۱۴۱
---
# غزل شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>بی‌نیازی تو و ما بهر نیاز آمده‌ایم</p></div>
<div class="m2"><p>رفته بودیم ز کوی تو و باز آمده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی دل در طرف زاویهٔ تحقیق است</p></div>
<div class="m2"><p>ما بر این رو[ی] نه بر وجه مجاز آمده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش الطاف تو چون بنده‌نوازی می‌کرد</p></div>
<div class="m2"><p>گفت: باز آی که ما بنده‌نواز آمده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌جواز خط تو رفتن ما ممکن نیست</p></div>
<div class="m2"><p>رُقعه‌ای ده که به امید جواز آمده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا مگر سرو قدت سایه کشد بر سر ما</p></div>
<div class="m2"><p>سر قدم ساخته از راه دراز آمده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسته احرام ره کعبه اقبال شما</p></div>
<div class="m2"><p>به صفا سعی نموده به حجاز آمده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طاق ابروی تو پیوسته مرا در نظرست</p></div>
<div class="m2"><p>نظری کن که به محراب نماز آمده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگذاریم به داغ از تو [چو] شمع از سر سوز</p></div>
<div class="m2"><p>غالب آن است که با سوز و گذاز آمده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا که محمود شود عاقبتت ابن حسام</p></div>
<div class="m2"><p>جان فدا کرده به دیدار ایاز آمده‌ایم</p></div></div>