---
title: >-
    غزل شمارهٔ ۱۴۹
---
# غزل شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>ترا که گفت که بر برگ گل کلاله فکن</p></div>
<div class="m2"><p>بنفشه تاب ده و بر رخ چو لاله فکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به غمزه صید دل عاشقان کن و آنگه</p></div>
<div class="m2"><p>بهانه بر نظر نرگس غزاله فکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میار باده تلخم که عیش من تلخ است</p></div>
<div class="m2"><p>ز لعل خویش می ناب در پیاله فکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو درد نامه عشاق خویشتن خوانی</p></div>
<div class="m2"><p>کرشمه ای کن و چشمی برین رساله فکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقال ابن حسام آتشی دل آشوبست</p></div>
<div class="m2"><p>ز دیده آب سرشکی بر بن مقاله فکن</p></div></div>