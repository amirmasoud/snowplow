---
title: >-
    غزل شمارهٔ ۵۰
---
# غزل شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>لبت یاقوت گوهر پوش دارد</p></div>
<div class="m2"><p>به گاه بوسه طعم نوش دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرزلف سیاهت بر بناگوش</p></div>
<div class="m2"><p>زنزهت بوی مرزنگوش دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چوگوشت با رقیبان است کم زان</p></div>
<div class="m2"><p>که یک ره جانت ما گوش دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم با روی خوب تست دایم</p></div>
<div class="m2"><p>بگو بهرخدا نیکوش دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو جان تن را در آغوش آمدی دوش</p></div>
<div class="m2"><p>تنم جان بین که در آغوش دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زدوشت دوش برخورداربودم</p></div>
<div class="m2"><p>دلم امشب هوای دوش دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل ابن حسام از اتش شوق</p></div>
<div class="m2"><p>ز گرمی سینه را پرجوش دارد</p></div></div>