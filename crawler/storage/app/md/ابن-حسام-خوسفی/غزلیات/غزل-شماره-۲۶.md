---
title: >-
    غزل شمارهٔ ۲۶
---
# غزل شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>دلبری دارم که دل در بند زلف و خال اوست</p></div>
<div class="m2"><p>عاشقانش را شراب از جام مالامال اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتنه آن چشم فتانم که از هر گوشه ای</p></div>
<div class="m2"><p>فتنه ای پر شیوه از دنباله دنبال اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال وصف حسن او بالاترست از ممکنات</p></div>
<div class="m2"><p>هرچه گوید عقل کل جزوی ز وصف الحال اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طفل ابجد خوان مکتب خانه اسرار عشق</p></div>
<div class="m2"><p>نکته دان خرده بین رمز قیل و قال اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن تجلی کز جلالت کوه را از جا ببرد</p></div>
<div class="m2"><p>پرتوی از لمعه رخشنده اجلال اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوست گو بنمای رو تا جان بر افشتانم برو</p></div>
<div class="m2"><p>زانکه جان را وقت رحلت چشم استقبال اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی دولت زان بدان خورشید روی آورده ام</p></div>
<div class="m2"><p>کافتاب دولت اندر سایه اقبال اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کسی را چشم بر منظور و محبوبی دگر</p></div>
<div class="m2"><p>زان میان ما را نظر بر ایلیا و آل اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایه اندازد مگر بر من همایی کز شرف</p></div>
<div class="m2"><p>طایر فرخنده اقبال زیر بال اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این مگس بر خوان انعامش کجا یارد نشست</p></div>
<div class="m2"><p>کآسمان چون گرده ای بر سفره افضال اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوش تواند خواند فردا نامه را ابن حسام</p></div>
<div class="m2"><p>زان که نقش نام او بر نامه اعمال اوست</p></div></div>