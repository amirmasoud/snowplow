---
title: >-
    غزل شمارهٔ ۸۷
---
# غزل شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>دلم فدای تو باد ای نسیم عنبر بیز</p></div>
<div class="m2"><p>به دستگیری افتاده ای چو من برخیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین که چشم تو هر گوشه ای کمین دارد</p></div>
<div class="m2"><p>چگونه دل بنشیند به گوشه ی پرهیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبس که آتش رخساره ی تو می افروخت</p></div>
<div class="m2"><p>بسوخت خرمن تقوای من به آتش تیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زدیم در خم زلف گره گشای تو چنگ</p></div>
<div class="m2"><p>که مفلسیم و نداریم هیچ دست آویز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا که باده و گل را به هم برآمیزیم</p></div>
<div class="m2"><p>ز دست حادثه ی روزگار رنگ آمیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیار کاسه و از می پرآب رنگین کن</p></div>
<div class="m2"><p>زخاک پرشده بین کاسه ی سر پرویز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوار حادثه هر سو دو اسپه می تازد</p></div>
<div class="m2"><p>نه رخش ازو بتواند گریخت نه شبدیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فضای سینه ام آتش گرفت مردم چشم</p></div>
<div class="m2"><p>تو مردمی کن و آبی زدیده بر وی ریز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فضای سینه ام آتش گرفت مردم چشم</p></div>
<div class="m2"><p>تو مردمی کن و آبی ز دیده بر وی ریز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بتاخت لشکر غم قلب سینه ابن حسام</p></div>
<div class="m2"><p>پناه می طلبی خیز و در پیاله گریز</p></div></div>