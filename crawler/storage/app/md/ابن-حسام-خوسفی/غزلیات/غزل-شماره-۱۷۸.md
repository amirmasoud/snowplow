---
title: >-
    غزل شمارهٔ ۱۷۸
---
# غزل شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>کدام زهد و چه تقوی که خالی از خللی</p></div>
<div class="m2"><p>نکرده ام من مسکین به عمر خود عملی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب مدار ز لخشیدنم به حشر که من</p></div>
<div class="m2"><p>نرفته ام قدمی بر بساط بی زللی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نَشُسته ام به همه عمر بر خلاف هوی</p></div>
<div class="m2"><p>سواد خال خجالت ز چهره املی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گرد موکب چابک رکاب ره نرسد</p></div>
<div class="m2"><p>عنان به دست هوی داده ای چو من کِسِلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظر چگونه فتد بر تجلِّیات جلال</p></div>
<div class="m2"><p>چو در مباصره باشد بهر طرف سبلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عروس حسن عمل زینت آن زمان یابد</p></div>
<div class="m2"><p>که هم ز کسوت تقوی درو کشی حللی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عنایت ار نبود دستگیر ابن حسام</p></div>
<div class="m2"><p>چگونه راه رود پای بسته در وحلی</p></div></div>