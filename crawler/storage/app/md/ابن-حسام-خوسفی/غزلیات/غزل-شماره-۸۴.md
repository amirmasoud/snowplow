---
title: >-
    غزل شمارهٔ ۸۴
---
# غزل شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>خطت مشکست و خالت عتبر تر</p></div>
<div class="m2"><p>جهان را کرده ای پر مشک و عنبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ ولبت را مپوش از دردمندان</p></div>
<div class="m2"><p>زمعلولان که پوشد گل به شکر؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قد سرو و لبت در روضه ی جان</p></div>
<div class="m2"><p>نشان طوبی است و آب کوثر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخت آیینه ی صنع الهی است</p></div>
<div class="m2"><p>تعالی شانه الله الکبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز ابرویت که آن مشکین خیالیست</p></div>
<div class="m2"><p>هلالی بسته ای بر ماه انور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برت گفتم کشم یک شب در آغوش</p></div>
<div class="m2"><p>به خنده گفت ناید سرو در بر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا پیوسته هست از آرزویت</p></div>
<div class="m2"><p>خیالی کج چو ابروی تو در سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببر آب و لطافت سرو و گل را</p></div>
<div class="m2"><p>چو سرو ناز بر گلزار بگذر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درتو اهل دولت را مآب است</p></div>
<div class="m2"><p>سر ابن حسام و خاک آن در</p></div></div>