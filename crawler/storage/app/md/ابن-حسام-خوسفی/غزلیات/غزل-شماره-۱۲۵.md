---
title: >-
    غزل شمارهٔ ۱۲۵
---
# غزل شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>بیا بیا که دل بسته را گشاد دهیم</p></div>
<div class="m2"><p>بیار باده که غم های دل به باد دهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمی که مونس دیرینه بود در دل ما</p></div>
<div class="m2"><p>اگر ز یاد برفت آن غمش به یاد دهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به وقت نزع فریدون نگر به سام چه گفت</p></div>
<div class="m2"><p>که وقت شد که قبادی به کیقباد دهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روان خفته ی نوشیروان چه می گوید</p></div>
<div class="m2"><p>که بهتر است که انصاف و عدل و داد دهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان بسته ی طایی به رهروی خوش گفت</p></div>
<div class="m2"><p>که توشه ای بطلب تا که مات زاد دهیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سروش غیب ندا می کند به ابن حسام</p></div>
<div class="m2"><p>که ناامید چرایی که ما مراد دهیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همین کرشمه تمامم که دوش ساقی گفت</p></div>
<div class="m2"><p>وظیفه ای است مقرر ترا زیاد دهیم</p></div></div>