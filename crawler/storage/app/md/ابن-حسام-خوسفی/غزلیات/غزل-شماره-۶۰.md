---
title: >-
    غزل شمارهٔ ۶۰
---
# غزل شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>ان را که مهر روی تو در دل نیافتند</p></div>
<div class="m2"><p>او را به هیچ واسطه مقبل نیافتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حجاج ره به کعبه ی اهل صفا نبرد</p></div>
<div class="m2"><p>تا در حریم کوی تو منزل نیافتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عطرنسیم کوی توکان همدم صباست</p></div>
<div class="m2"><p>درچین طرّه های شمایل نیافتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکو نه خاک درگه جانان به جان خرید</p></div>
<div class="m2"><p>او را به هیچ باب معامل نیافتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنجینه ی رموز محبت که عشق اوست</p></div>
<div class="m2"><p>درسینه ای طلب که درو غل نیافتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهل روش که سالک اطوار عزتند</p></div>
<div class="m2"><p>جز عکس روی دوست مقابل نیافتند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست امید ابن حسام شکسته دل</p></div>
<div class="m2"><p>درگردن مراد حمایل نیافتند</p></div></div>