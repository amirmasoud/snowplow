---
title: >-
    غزل شمارهٔ ۹۲
---
# غزل شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>شیخ را صومعه در رهن شراب است امروز</p></div>
<div class="m2"><p>بر در میکده در چنگ ورباب است امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه در میکده دی منکر می نوشان شد</p></div>
<div class="m2"><p>در خرابات مغان مست و خراب است امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از می ای شیخ مرا توبه چه میفرمایی</p></div>
<div class="m2"><p>توبه موقوف که ایام شباب است امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرگس از غایت مستی سر ساغر دارد</p></div>
<div class="m2"><p>قدح لاله پر از باده ی ناب است امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چه خون کرده ام ای خون منت در گردن</p></div>
<div class="m2"><p>چشم خون ریز تو در عین عتاب است امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنشین تا نفسی با تو بهم بنشینیم</p></div>
<div class="m2"><p>اخ ای عمر چه هنگام شتاب است امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که دوش ابن حسام از غم عشقت بگریست</p></div>
<div class="m2"><p>مردم دیده ی او غرقه ی اب است امروز</p></div></div>