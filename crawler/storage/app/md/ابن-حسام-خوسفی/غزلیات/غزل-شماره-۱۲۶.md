---
title: >-
    غزل شمارهٔ ۱۲۶
---
# غزل شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>ما وصال تو به زاری و دعا می‌طلبیم</p></div>
<div class="m2"><p>دردمندیم و ز لعل تو دوا می‌طلبیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو حجّاج به احرام درت بسته میان</p></div>
<div class="m2"><p>کعبهٔ کوی تو از راه صفا می‌طلبیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکسی از پی مقصود خود اندر طلبی است</p></div>
<div class="m2"><p>حاصل آنست که ما از تو ترا می‌طلبیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده هرسو نگران و تو به خلوتگه دل</p></div>
<div class="m2"><p>تو کجایی و ترا ما به کجا می‌طلبیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نسیمی که ز زلف تو دمد موجودست</p></div>
<div class="m2"><p>آنچه از رایحهٔ باد صبا می‌طلبیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفحهٔ مشک خطا در شکن طرّهٔ تست</p></div>
<div class="m2"><p>ما ز چین سر زلفت به خطا می‌طلبیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غرض ابن حسام از رخ زیبای تو چیست</p></div>
<div class="m2"><p>غالب آنست که ما صنع خدا می‌طلبیم</p></div></div>