---
title: >-
    غزل شمارهٔ ۴۲
---
# غزل شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>خط تو دایره ماهتاب را بگرفت</p></div>
<div class="m2"><p>به باغ عارض تو سبزه آب را بگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستاره چشم سر طرّه قمر پوشید</p></div>
<div class="m2"><p>به زیر سایه شب آفتاب را بگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شب ، جمال تو، گفتم ببینم اندر خواب</p></div>
<div class="m2"><p>خیال روی تو در دیده خواب را بگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم ز فتنه ی چشم تو گر چه بود خراب</p></div>
<div class="m2"><p>غمت بیامد و ملک خراب را بگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رقص زهره به خنیاگری به چرخ آمد</p></div>
<div class="m2"><p>به چنگ دوش چو زهره رباب را بگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقیم کوی تو شد دل چه بخت یار دلیست</p></div>
<div class="m2"><p>که استانه ی دولت مآب را بگرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس از خیال پرستی و مستی ابن حسام</p></div>
<div class="m2"><p>که صبح شیب تو شام شباب را بگرفت</p></div></div>