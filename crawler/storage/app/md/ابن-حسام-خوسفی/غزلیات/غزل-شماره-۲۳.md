---
title: >-
    غزل شمارهٔ ۲۳
---
# غزل شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>دلم فریفتهٔ آن شمایل عربیست</p></div>
<div class="m2"><p>که شکل و شیوه او را هزار بوالعجبیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال لعل لبش در درون سینه من</p></div>
<div class="m2"><p>چو باده در دل پر خون شیشهٔ حلبیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکشت فتنهٔ چشمش مرا و می‌بینم</p></div>
<div class="m2"><p>که همچنان نظرش سوی من به بولعجبیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرید پیر مغانم که شیخ هر قومی</p></div>
<div class="m2"><p>میان قوم چو اندر میان فرقه نبیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مخار پای دل رهروان به خار جفا</p></div>
<div class="m2"><p>که این طریقهٔ بد راه و رسم بولهبیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرو ز راه ادب تا بلندبخت شوی</p></div>
<div class="m2"><p>که شوربختی مردم ز راه کم ادبیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجاست بانی قصر ارم که ترکیبش</p></div>
<div class="m2"><p>به قصر شاه کنون خشت شرفهٔ طنبیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز جام لم یزلی جرعه‌ای به من دادند</p></div>
<div class="m2"><p>مگوی مستی من ذوق بادهٔ عنبیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن ز گردش دوران شکایت ابن حسام</p></div>
<div class="m2"><p>که عادت فلک دون پرست منقلبیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برو به آب قناعت بشوی دست طمع</p></div>
<div class="m2"><p>که بیش حرمت مردم ز فرط کم طلبیست</p></div></div>