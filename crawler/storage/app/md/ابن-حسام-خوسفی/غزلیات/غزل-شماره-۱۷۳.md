---
title: >-
    غزل شمارهٔ ۱۷۳
---
# غزل شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>شب وداع و غم هجر و درد تنهایی</p></div>
<div class="m2"><p>دل شکسته و محزون کجا شکیبایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سواد دیده ی من روشنی ز روی تو یافت</p></div>
<div class="m2"><p>مرو مرو که ز چشمم برفت بینایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان که عمر گرامی به کس نمی ماند</p></div>
<div class="m2"><p>تو نیز عمر عزیزی از آن نمی پایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیث قد تو نسبت به سرو ناید راست</p></div>
<div class="m2"><p>و گر به سرو کنم نسبتش تو بالایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوای درد دل دردمند من لب توست</p></div>
<div class="m2"><p>مفَّرَح است علاج مزاج سودایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گره ز کار پریشان بسته بگشاید</p></div>
<div class="m2"><p>اگر گزه ز سر زلف بسته بگشایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بوی زلف تو دل در پی صبا می رفت</p></div>
<div class="m2"><p>بخنده گفت چه بر هرزه باد پیمایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگار کرده ام از خون خیال خانه چشم</p></div>
<div class="m2"><p>مگر به خانه رنگین دمی فرود آیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیان حسن تو ابن حسام با گل گفت</p></div>
<div class="m2"><p>که بلبلان به چمن واله اند و شیدایی</p></div></div>