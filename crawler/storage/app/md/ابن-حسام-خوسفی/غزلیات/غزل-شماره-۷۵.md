---
title: >-
    غزل شمارهٔ ۷۵
---
# غزل شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>یاد لبت کنم دهنم پرشکر شود</p></div>
<div class="m2"><p>نام رخت برم همه عالم قمر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با ابروی سیاه تو پیوسته ام خیال</p></div>
<div class="m2"><p>تاکی خیال کج زسر ما بدر شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیر خدنگ غمزه ات از دل کند گذر</p></div>
<div class="m2"><p>گر نه فضای سینه مرو را سپر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنشین که با تو عمر گرامی به سر بریم</p></div>
<div class="m2"><p>عمر آنچنان خوش است که باجان به سر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرح فراق یار نوشتن مجال نیست</p></div>
<div class="m2"><p>کز آب دیده صفحه ی طومار تر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما ره به اختیار به مقصد نمی بریم</p></div>
<div class="m2"><p>آری مگر عنایت او راهبر شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر نیک و بد که بر سر ما می رود قضاست</p></div>
<div class="m2"><p>هرگز گمان مبر که نبشته دگر شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابن حسام چشم به بهبود روزگار</p></div>
<div class="m2"><p>مگشای و زان بترس کزین هم بتر شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگذر ز سر که در ره عشاق اگر ترا</p></div>
<div class="m2"><p>کاری به سر شود هم از این رهگذر شود</p></div></div>