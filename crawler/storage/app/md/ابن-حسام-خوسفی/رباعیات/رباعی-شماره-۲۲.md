---
title: >-
    رباعی شمارهٔ ۲۲
---
# رباعی شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>ز آن درد که آن دیده روشن دارد</p></div>
<div class="m2"><p>چشمم ز برای او گهر می بارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن درد اگر غمزه او بگذارد</p></div>
<div class="m2"><p>چشمم به مژه ز چشم او بر دارد</p></div></div>