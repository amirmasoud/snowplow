---
title: >-
    رباعی شمارهٔ ۴۸
---
# رباعی شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>یا بوی تو از باد صبا می آید</p></div>
<div class="m2"><p>یا رایحه مشک خطا می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا چین سر زلف تو را بگشادند</p></div>
<div class="m2"><p>یا آهو چین نافه گشا می آید</p></div></div>