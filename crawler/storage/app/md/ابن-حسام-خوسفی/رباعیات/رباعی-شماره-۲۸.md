---
title: >-
    رباعی شمارهٔ ۲۸
---
# رباعی شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>خوی از سر زلف آن صنم می ریزد</p></div>
<div class="m2"><p>بر گل ز گلاب ناب نم می ریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حسرت عناب لبش دیده من</p></div>
<div class="m2"><p>خونابه ز چشم ، دم به دم می ریزد</p></div></div>