---
title: >-
    رباعی شمارهٔ ۹۰
---
# رباعی شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>ای صبح ندانم که چه در یافته ای</p></div>
<div class="m2"><p>مشکین قصب از بهر که بشکافته ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید عفاک الله از آنروز که گرم</p></div>
<div class="m2"><p>بر تشنه لبان کربلا تافته ای</p></div></div>