---
title: >-
    رباعی شمارهٔ ۸۰
---
# رباعی شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>جانیست مرا و نیست اندر خور تو</p></div>
<div class="m2"><p>غایت ز بر من است و اندر بر تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطف تو هزار در برویم بگشاد</p></div>
<div class="m2"><p>دیگر نروم به هیچ باب از در تو</p></div></div>