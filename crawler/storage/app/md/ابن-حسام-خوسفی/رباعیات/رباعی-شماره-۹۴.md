---
title: >-
    رباعی شمارهٔ ۹۴
---
# رباعی شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>ای جان اثری ز مظهر خاک بجوی</p></div>
<div class="m2"><p>ای گوهر پاک گوهر پاک بجوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سوک حسن مذاق ما پر زهر ست</p></div>
<div class="m2"><p>از نوش لبش مایه تریاک بجوی</p></div></div>