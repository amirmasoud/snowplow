---
title: >-
    رباعی شمارهٔ ۴۳
---
# رباعی شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>بر عارض چون گل تو سنبل بدمید</p></div>
<div class="m2"><p>از عنبر تو غالیه بر ماه کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچه سخنی ز پسته تنگ تو گفت</p></div>
<div class="m2"><p>باد سحر آمد و دهانش بدرید</p></div></div>