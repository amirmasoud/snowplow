---
title: >-
    رباعی شمارهٔ ۶۵
---
# رباعی شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>ای قطره ز دریا برسیدی به کمال</p></div>
<div class="m2"><p>از مشرب عذب یافتی آب زلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور تو ز بدر است و لطافت ز جمال</p></div>
<div class="m2"><p>المنّة لله تبارک و تعال</p></div></div>