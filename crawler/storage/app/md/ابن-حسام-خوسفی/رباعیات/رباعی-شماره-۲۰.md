---
title: >-
    رباعی شمارهٔ ۲۰
---
# رباعی شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>صافی جهان چو نیست می ساز به درد</p></div>
<div class="m2"><p>پشمینه بپوش چون همی باید مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مرگ چه دشوار تر آن کز ره عجز</p></div>
<div class="m2"><p>حاجت به در همچو خودی باید برد</p></div></div>