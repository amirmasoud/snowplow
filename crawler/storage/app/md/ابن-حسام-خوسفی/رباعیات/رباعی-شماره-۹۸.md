---
title: >-
    رباعی شمارهٔ ۹۸
---
# رباعی شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>در دست مرض گرچه زبون می آیی</p></div>
<div class="m2"><p>برپای روی و سرنگون می آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر سرت آسیا همی گردانند</p></div>
<div class="m2"><p>چون سفله ز آسیا برون می آیی</p></div></div>