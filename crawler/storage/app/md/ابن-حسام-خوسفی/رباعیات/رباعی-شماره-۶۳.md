---
title: >-
    رباعی شمارهٔ ۶۳
---
# رباعی شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>در مانده به چنگ شیر و دندان پلنگ</p></div>
<div class="m2"><p>در سینه اژدها و در کام نهنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر کوفته و مغر برآورد به سنگ</p></div>
<div class="m2"><p>به زان که بود سلیطه ای با تو به جنگ</p></div></div>