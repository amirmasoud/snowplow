---
title: >-
    رباعی شمارهٔ ۷۹
---
# رباعی شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>ای روضه برون آی رسول ثقلین</p></div>
<div class="m2"><p>ای ماه حجاز و آفتاب حرمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر زهر ببین خلعت زیبای حسن</p></div>
<div class="m2"><p>پر خون بنگر کسوت زیبای حسین</p></div></div>