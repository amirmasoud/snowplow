---
title: >-
    رباعی شمارهٔ ۱۸
---
# رباعی شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>لاله زدم باد صبا می خندد</p></div>
<div class="m2"><p>گویی لب و لعل دلربا می خندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دوست بگویم که چرا می خندد</p></div>
<div class="m2"><p>بر کوتاهی عمر ما می خندد</p></div></div>