---
title: >-
    رباعی شمارهٔ ۱۰
---
# رباعی شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>فریاد که آن یار پسندیده برفت</p></div>
<div class="m2"><p>ناکرده وداع ما و نادیده برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را به که آرام دهم مسکین من</p></div>
<div class="m2"><p>کآرام دل و روشنی دیده برفت</p></div></div>