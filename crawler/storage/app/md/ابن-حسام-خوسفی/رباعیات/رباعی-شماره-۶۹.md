---
title: >-
    رباعی شمارهٔ ۶۹
---
# رباعی شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>تیسیر ز فَسر سخنت یافت نظام</p></div>
<div class="m2"><p>کشّاف مکمّلی و حاوی کلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نکته که در وقایه دین کافی ست</p></div>
<div class="m2"><p>در سلک معانی بیان تو تمام</p></div></div>