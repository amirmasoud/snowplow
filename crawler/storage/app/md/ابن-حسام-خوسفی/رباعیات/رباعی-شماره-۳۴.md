---
title: >-
    رباعی شمارهٔ ۳۴
---
# رباعی شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>گر چه به قلم کام بسی دانی راند</p></div>
<div class="m2"><p>وز نوک قلم مشک توانی افشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نامه خویشتن بباید خواندن</p></div>
<div class="m2"><p>منویس کتابتی که نتوانی خواند</p></div></div>