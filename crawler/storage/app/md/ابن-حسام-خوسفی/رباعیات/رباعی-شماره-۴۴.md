---
title: >-
    رباعی شمارهٔ ۴۴
---
# رباعی شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>آن پیر که جنبشی ازو می آید</p></div>
<div class="m2"><p>معشوقه اگر جوان بود می شاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور پیر بود زن و جوان باشد مرد</p></div>
<div class="m2"><p>حیف است که پیر را جوان می گاید</p></div></div>