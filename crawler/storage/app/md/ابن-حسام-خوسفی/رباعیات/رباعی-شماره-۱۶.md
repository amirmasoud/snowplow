---
title: >-
    رباعی شمارهٔ ۱۶
---
# رباعی شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای هزل تمام هیچ و هازل همه هیچ</p></div>
<div class="m2"><p>زنهار چه زنهار که در هزل مپیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قولی که نه دین و شرع باشد مپسند</p></div>
<div class="m2"><p>راهی که به سوی حق نباشد مپسیچ</p></div></div>