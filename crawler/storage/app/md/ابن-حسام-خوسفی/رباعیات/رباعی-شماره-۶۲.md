---
title: >-
    رباعی شمارهٔ ۶۲
---
# رباعی شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>هر چند که یار پارسا باشد گرگ</p></div>
<div class="m2"><p>از بره همان به که جدا باشد گرگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون مهل از خانه زن ار خلق ولیست</p></div>
<div class="m2"><p>خر بسته به ار چه آشنا باشد گرگ</p></div></div>