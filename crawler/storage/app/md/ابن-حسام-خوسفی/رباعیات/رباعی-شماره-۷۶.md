---
title: >-
    رباعی شمارهٔ ۷۶
---
# رباعی شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>از خاک جهان به زرق رستن نتوان</p></div>
<div class="m2"><p>بر رهگذر سیل نشستن نتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مکر جهان حذر که از وی زادی</p></div>
<div class="m2"><p>با مادر خود نکا بستن نتوان</p></div></div>