---
title: >-
    رباعی شمارهٔ ۹۷
---
# رباعی شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>ای باد صبا غالیه سا می آیی</p></div>
<div class="m2"><p>چون مشک خطا نافه گشا می آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معلومم شد که از کجا می آیی</p></div>
<div class="m2"><p>از تربت آل مصطفی می آیی</p></div></div>