---
title: >-
    رباعی شمارهٔ ۷۵
---
# رباعی شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>تا من صفت عارض او می گویم</p></div>
<div class="m2"><p>الحق سخن از او چه نکو می گویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مایل دوست گشت و من مایل او</p></div>
<div class="m2"><p>زآن هر چه دلم گفت بگو می گویم</p></div></div>