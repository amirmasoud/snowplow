---
title: >-
    رباعی شمارهٔ ۱۳
---
# رباعی شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>بلبل ز پی گل غزلی تر می گفت</p></div>
<div class="m2"><p>باد سحر از نسیم عنبر می گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاله صفت کلاه دارا می کرد</p></div>
<div class="m2"><p>نرگس سخن از تاج سکندر می گفت</p></div></div>