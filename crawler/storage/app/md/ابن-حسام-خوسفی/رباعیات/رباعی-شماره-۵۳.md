---
title: >-
    رباعی شمارهٔ ۵۳
---
# رباعی شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>ای دست نشان قلمت لؤلؤ تر</p></div>
<div class="m2"><p>منشی فلک عبارتت را چاکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بام فلک ز ماه می سازند</p></div>
<div class="m2"><p>از بهر قلمدان تو ما شوره زر</p></div></div>