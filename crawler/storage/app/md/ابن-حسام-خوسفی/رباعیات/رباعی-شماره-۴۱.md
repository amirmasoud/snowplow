---
title: >-
    رباعی شمارهٔ ۴۱
---
# رباعی شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>خواهی که به کام دشمنانت نکنند</p></div>
<div class="m2"><p>انگشت نمای مردمانت نکنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرهیز کن از خواهر و از دختر و زن</p></div>
<div class="m2"><p>تا حیز و دیوث و قلتبانت نکنند</p></div></div>