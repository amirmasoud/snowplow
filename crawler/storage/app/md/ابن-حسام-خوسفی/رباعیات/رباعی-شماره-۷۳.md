---
title: >-
    رباعی شمارهٔ ۷۳
---
# رباعی شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>ماییم که بی هیچ غمی دم نزنیم</p></div>
<div class="m2"><p>یک دم به مراد خویش بی غم نزنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدبار شبی بود که صد خار فراق</p></div>
<div class="m2"><p>بر دیده زنیم و دیده بر هم نزنیم</p></div></div>