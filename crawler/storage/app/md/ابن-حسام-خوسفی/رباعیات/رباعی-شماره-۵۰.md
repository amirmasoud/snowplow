---
title: >-
    رباعی شمارهٔ ۵۰
---
# رباعی شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>گویند همایی به هوا می آید</p></div>
<div class="m2"><p>چون سایه الطاف خدا می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم غلطی که گر مرا می آید</p></div>
<div class="m2"><p>کوفست به ویرانی ما می آید</p></div></div>