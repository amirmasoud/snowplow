---
title: >-
    رباعی شمارهٔ ۱۴
---
# رباعی شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>در خانه خود به کمترین مایه قوت</p></div>
<div class="m2"><p>بیچاره به سر کنی به صد صبر و سکوت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سفره مردم نکشی دست به لوت</p></div>
<div class="m2"><p>تا پر نکنی شکم به سان الموت</p></div></div>