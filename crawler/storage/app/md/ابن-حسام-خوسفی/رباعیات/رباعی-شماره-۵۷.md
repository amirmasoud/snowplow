---
title: >-
    رباعی شمارهٔ ۵۷
---
# رباعی شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>بیداری شبهای من از اختر پرس</p></div>
<div class="m2"><p>حال دل پر خون ز لب ساغر پرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی زر ز لب دوست به کامی نرسند</p></div>
<div class="m2"><p>ای دوست بیا و از من بی زر پرس</p></div></div>