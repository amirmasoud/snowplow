---
title: >-
    رباعی شمارهٔ ۳۰
---
# رباعی شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>بیچاره حسود بی بصر خواهد شد</p></div>
<div class="m2"><p>چون کور شده است کورتر خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زیر و زبر چو گویمش کان فلکی</p></div>
<div class="m2"><p>مانند فلک زیر و زبر خواهد شد</p></div></div>