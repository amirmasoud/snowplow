---
title: >-
    شمارهٔ  ۹
---
# شمارهٔ  ۹

<div class="b" id="bn1"><div class="m1"><p>ازین سپس من و کنجی و دلبری چون حور</p></div>
<div class="m2"><p>دگر بس است مرا صحبت هپور و چپور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو باشی و من و من باشم و تو شیشه می</p></div>
<div class="m2"><p>کمانچه باشد و نی تار و تنبک و تنبور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به می مصالحه کردیم چشمه کوثر</p></div>
<div class="m2"><p>برو به کار خود ای واعظ تفنن جور!</p></div></div>