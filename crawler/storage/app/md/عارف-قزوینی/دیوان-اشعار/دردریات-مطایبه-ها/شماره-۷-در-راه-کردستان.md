---
title: >-
    شمارهٔ  ۷ - در راه کردستان
---
# شمارهٔ  ۷ - در راه کردستان

<div class="b" id="bn1"><div class="m1"><p>حشمت الملک آن که عنوانش</p></div>
<div class="m2"><p>پیش من اینکه خواندمی خانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز از صحبتش به تنگم و شب</p></div>
<div class="m2"><p>عاجز از قل قل قلیانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مزه حرف بی رویه زدن</p></div>
<div class="m2"><p>شیره کرده است زیر دندانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه خواهد کند سکوت ولیک</p></div>
<div class="m2"><p>چانه خارج بود ز فرمانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راه طهران الی به کردستان</p></div>
<div class="m2"><p>این چه خواهی ز یزد و کرمانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرقه در قلزم کثافت را</p></div>
<div class="m2"><p>کی کند پاک آب بارانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاش کالسکه راه آهن بود</p></div>
<div class="m2"><p>که بمردیم در بیابانش</p></div></div>