---
title: >-
    شمارهٔ  ۵ - دلاکیه عارف
---
# شمارهٔ  ۵ - دلاکیه عارف

<div class="b" id="bn1"><div class="m1"><p>رفت شخصی تا که بتراشد سرش</p></div>
<div class="m2"><p>در بر دلاک از خود خر ترش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لنگ بر زیر زنخ انداختش</p></div>
<div class="m2"><p>تیغ اندر سنگ روئین آختش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سرش پاشید آب از قمقمه</p></div>
<div class="m2"><p>او نشسته همچو سلطان جمجمه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس به . . . خویش مالید آینه</p></div>
<div class="m2"><p>گفت خوش بین باش به زین جای نه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیغ را مالید بر قیشی که بود</p></div>
<div class="m2"><p>پیش چشمش در رکوع و در سجود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیغ خود را کرد تیز آن دل دو نیم</p></div>
<div class="m2"><p>گفت بسم الله الرحمن الرحیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن سر بی صاحب بدبخت را</p></div>
<div class="m2"><p>یا سر چون سنگ خارا سخت را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرد زیر دست و مالیدن گرفت</p></div>
<div class="m2"><p>بعد از یک سو تراشیدن گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اولین بارش چنان ضربی به سر</p></div>
<div class="m2"><p>زد کز آن ضربت دلش را شد خبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت آخ استاد ببریدی سرم</p></div>
<div class="m2"><p>گفت: «راحت باش تا من سرورم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پنبه می چسبانمش تا خون ریش</p></div>
<div class="m2"><p>از سر خونین نریزد روی ریش »</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پنبه می چسباند یک لختی اگر</p></div>
<div class="m2"><p>بر سر لختش زدی ضرب دگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باز فریاد از دل پر خون کشید</p></div>
<div class="m2"><p>تا بجنبد چند جا را هم برید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هی بریدی آن و هی از جیب خویش</p></div>
<div class="m2"><p>پنبه می چسباند بر آن زخم ریش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پوست از آن سر همه تاراج کرد</p></div>
<div class="m2"><p>صفحه سر دکه حلاج کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا رسید آنجا که سرتاسر سرش</p></div>
<div class="m2"><p>قوزه زاری شد سربار آورش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت «سر این سر از بی صاحبی است</p></div>
<div class="m2"><p>ز آن تو پنداری کدو یا طالبی است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا تو دلاکی یقین دان مرده شوی</p></div>
<div class="m2"><p>جمله سرها را برد بی گفتگوی »</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تیغ دادن بر کف دلاک مست</p></div>
<div class="m2"><p>به که افتد شاهی احمد را به دست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن کند زخمی سرو این سر برد</p></div>
<div class="m2"><p>سر ز سرداران یک کشور برد</p></div></div>