---
title: >-
    شمارهٔ  ۲ - تیمورتاش نامه
---
# شمارهٔ  ۲ - تیمورتاش نامه

<div class="b" id="bn1"><div class="m1"><p>چنین گفت رندی به تیمورتاش:</p></div>
<div class="m2"><p>«خیانت اگر کردی ایمن مباش »</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار خوش و خرمی شد چو طی</p></div>
<div class="m2"><p>رسد شام تاریک یلدای دی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهان گر که نیرنگ بردی به کار</p></div>
<div class="m2"><p>درت پرده ات،پرده دار آشکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیندیش ز اندیشه بد، که بد</p></div>
<div class="m2"><p>بداندیش را سوی دوزخ برد . . .</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو، ای بنده گمره ناسپاس</p></div>
<div class="m2"><p>تو، ای ناجوان مرد حق ناشناس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگو راست ای دشمن راستی</p></div>
<div class="m2"><p>فزون زآنچه بودی چه میخواستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه شد با همه هوش و عقل سلیم</p></div>
<div class="m2"><p>به دیگ اوفتادی ز هول حلیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دریا، همه بار نتوان به چنگ:</p></div>
<div class="m2"><p>گهر برد، بی بیم کام نهنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز پایان تو، کاشکی خائنین</p></div>
<div class="m2"><p>بگیرند عبرت درین سرزمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قلم، روسیه مانده کار تست</p></div>
<div class="m2"><p>فرومانده در ننگ افکار تست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو، ای زشتخو، چون گلیم سیاه:</p></div>
<div class="m2"><p>شدی حاجب دادخواهان و شاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شب و روز، عمرت به مستی گذشت</p></div>
<div class="m2"><p>به مستی و شهوت پرستی گذشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به میدان بی عصمتی یکه تاز</p></div>
<div class="m2"><p>نبودی به فکر نشیب و فراز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو، گر دامن عفتی لکه دار</p></div>
<div class="m2"><p>نمودی ، تلافی کند روزگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز بدکاریت، آنچه بشنیده گوش:</p></div>
<div class="m2"><p>نگویم؟! که داند خداوند هوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون مانده در یاد من این مثل</p></div>
<div class="m2"><p>که بهتر ازینجا ندارد محل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز «انسان »، سگ ار دارد افغان و سوز</p></div>
<div class="m2"><p>خود این درد، سگ داند و پاره دوز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نبود از تو خوشبخت تر کس، اگر:</p></div>
<div class="m2"><p>نیفتادی آخر درین درد سر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو یک عمر بودی تو، مست غرور</p></div>
<div class="m2"><p>خماریش این است، چشم تو کور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه خوش بود هر روزه ات، خط سیر</p></div>
<div class="m2"><p>چطوری تو، آمشهدی، شب بخیر!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپس باید از خاک، بستر کنی</p></div>
<div class="m2"><p>ز رنج خوشی، خستگی درکنی!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بسی دستها از تو شد زیر دست</p></div>
<div class="m2"><p>چه خوش دست تقدیر، دست تو بست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سر بدسگالان چو افعی بکوب</p></div>
<div class="m2"><p>محال است از فکر بد، کار خوب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر آن کو، خیانت به این آب و خاک:</p></div>
<div class="m2"><p>کند، باید اینگونه گردد هلاک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو، با دشمن مملکت ساختی</p></div>
<div class="m2"><p>ولی نعمت خویش، نشناختی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کشانیده ای در خیانت به ننگ</p></div>
<div class="m2"><p>همه نام ننگین تیمور لنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز تخم حرام مغول یا تتار</p></div>
<div class="m2"><p>جز از اینکه دیدی، توقع مدار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هویت ز هر بی هویت مجوی</p></div>
<div class="m2"><p>بغیر از خطا، آنچه دیدی بگوی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو گنجشک از دیدن روی بوم</p></div>
<div class="m2"><p>مرا دل تپد زین سجل های شوم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پس از هفتصد سال کز روزگار:</p></div>
<div class="m2"><p>گذشت است، ماند از تو، این یادگار:</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که گویند بعد از تو، یاران تو</p></div>
<div class="m2"><p>سیهکارها، همقطاران تو:</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که تیمور لنگ ار یکی بود تاش:</p></div>
<div class="m2"><p>نبود آن یکی، غیر تیمورتاش . . .</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی نکته باقیست، از من به گوش:</p></div>
<div class="m2"><p>نگهدار همچون پیام سروش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به مادر، کسی جان فدا ساخته است</p></div>
<div class="m2"><p>که از خون او پرورش یافته است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به مام وطن «هر که » مأنوس نیست</p></div>
<div class="m2"><p>زنازاده در بند ناموس نیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر آنکس که خون خورد عمری چو من</p></div>
<div class="m2"><p>ازو باید آموخت، عشق وطن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وطن دوستان، دیده ام من بسی</p></div>
<div class="m2"><p>چه داند وطن چیست؟ هر ناکسی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>وطن، چاردیوار ملک است و باغ!</p></div>
<div class="m2"><p>وطن دوست جز این، ندارم سراغ!</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>براه وطن، آنکسی سوخته ست</p></div>
<div class="m2"><p>که در دیده، زین خاک اندوخته ست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز تن های ناپاک و خون نجس</p></div>
<div class="m2"><p>نباید وطن دوستی کرد حس</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به جان دوست دارد کس این آب و خاک</p></div>
<div class="m2"><p>که خونش بود چون می ناب، پاک</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>وطن دوست، ایرانی خالص است</p></div>
<div class="m2"><p>نه چون خالصی زاده ناقص است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وطن دوستانی درین مردمند</p></div>
<div class="m2"><p>که در کشور خویش، سردرگمند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دعاشان به درگاه پروردگار</p></div>
<div class="m2"><p>جز این نیست در باره این دیار:</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کند دشمن خانگی را هلاک</p></div>
<div class="m2"><p>کند شر بیگانه، زین آب و خاک</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خیانتگران آنچه هستند، کاش:</p></div>
<div class="m2"><p>به بینند کیفر، چو تیمورتاش</p></div></div>