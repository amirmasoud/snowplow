---
title: >-
    شمارهٔ  ۲ - بوسه و جان
---
# شمارهٔ  ۲ - بوسه و جان

<div class="b" id="bn1"><div class="m1"><p>دلم ز کف سر زلف تو را رها نکند</p></div>
<div class="m2"><p>دل از کمند تو وارستگی خدا نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه خون مرا بیگنه بریخت ولیک</p></div>
<div class="m2"><p>کسی مطالبه از یار خونبها نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آنکه از کف معشوق جام میگیرد</p></div>
<div class="m2"><p>نظر بجانب جام جهان نما نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسوخت سینه ندیدم اثر ز آه سحر</p></div>
<div class="m2"><p>ز من گذشت کسی بعد از این دعا نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بلبلان چمن از زبان من گوئید</p></div>
<div class="m2"><p>بخواب ناز گلم رفته کس صدا نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو بوسه ده که منت جان نثار خواهم کرد</p></div>
<div class="m2"><p>کسی معامله بهتر از این دو تا نکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفتمش که دلت جای عارفست بگفت</p></div>
<div class="m2"><p>کسی بدیر شهان فرش بوریا نکند</p></div></div>