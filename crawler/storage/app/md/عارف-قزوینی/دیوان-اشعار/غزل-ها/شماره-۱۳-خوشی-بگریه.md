---
title: >-
    شمارهٔ  ۱۳ - خوشی بگریه
---
# شمارهٔ  ۱۳ - خوشی بگریه

<div class="b" id="bn1"><div class="m1"><p>فتادم از نظر آن لحظه ای که دور شدم</p></div>
<div class="m2"><p>خوشم بگریه که از دست هجر کور شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی بمیکده و گاه در خراباتم</p></div>
<div class="m2"><p>هزار شکر که با اهل درد جور شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دعاش گفتم و دشنام هم نداد جواب</p></div>
<div class="m2"><p>کجاست مرگ که پیش رقیب بور شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نرد عشق تو عمری به ششدر افتادم</p></div>
<div class="m2"><p>در این قمار دگر لات و لوت و عور شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو چشم مست تو دنبال شور و شر می گشت</p></div>
<div class="m2"><p>شدم چو مست بهم چشمیش شرور شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهشت و حوری و کوثر بزاهد ارزانی</p></div>
<div class="m2"><p>بیار می که بری از بهشت و حور شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دست هجر ت و کنجی نشسته عارف و گفت</p></div>
<div class="m2"><p>چو نیست چاره ز بیچارگی صبور شدم</p></div></div>