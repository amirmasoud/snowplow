---
title: >-
    شمارهٔ  ۴۵ - فرقه بازی و جهالت!
---
# شمارهٔ  ۴۵ - فرقه بازی و جهالت!

<div class="b" id="bn1"><div class="m1"><p>ز بس بزلف تو دل بر سر دل افتاده</p></div>
<div class="m2"><p>چه کشمکش که میان من و دل افتاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فرقه بازی احزاب دل در آن سر زلف</p></div>
<div class="m2"><p>گذار شانه بر آن طره مشکل افتاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم بسوخت که بر صورت تو خال سیاه</p></div>
<div class="m2"><p>بسان ملت محکوم جاهل افتاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسوز از آتش رخ این حجاب و روی نما</p></div>
<div class="m2"><p>تو جان بخواه که جان غیر قابل افتاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بسکه خون ز غمت ریختم بدل از چشم</p></div>
<div class="m2"><p>دلم چو غرقه ز دریا بساحل افتاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجز جنون نبرد ره بسوی کعبه عشق</p></div>
<div class="m2"><p>که بار عقل در این راه بر گل افتاده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفته نور جهانتاب علم عالم و شیخ</p></div>
<div class="m2"><p>پی مباحثه بسی دلایل افتاده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپردمت برقیبان و با تو کارم نیست</p></div>
<div class="m2"><p>از آنکه کار بدست اراذل افتاده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو هرج و مرجی دربار عشق بین، عارف</p></div>
<div class="m2"><p>میان این همه دیوانه عاقل افتاده</p></div></div>