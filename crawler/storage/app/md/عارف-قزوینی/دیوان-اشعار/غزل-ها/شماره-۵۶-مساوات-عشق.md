---
title: >-
    شمارهٔ  ۵۶ - مساوات عشق
---
# شمارهٔ  ۵۶ - مساوات عشق

<div class="b" id="bn1"><div class="m1"><p>در عشق بدان فرق شهنشاه و گدا نیست</p></div>
<div class="m2"><p>کس نیست که در کوی بتان بیسر و پا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حسن تو انگشت نما هستی و لیکن</p></div>
<div class="m2"><p>در عشق تو جز من کسی انگشت نما نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسوای تو گشتیم من و دل بجهان نیست</p></div>
<div class="m2"><p>جائی که در آن قصه رسوائی ما نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستم بگذارید بگریم به غم دل</p></div>
<div class="m2"><p>جز اشک کسی در غم دل عقده گشا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این مهر که دارد بتو دل در همه کس نه</p></div>
<div class="m2"><p>وین جای که داری تو بدل در همه جا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با یار سخن دوش شد از عالم وحدت</p></div>
<div class="m2"><p>گفتم مشنو هر که تو را گفت خدا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بد گفت رقیب از پی و بشنیدم و گفتم</p></div>
<div class="m2"><p>با یار که دل بد مکن این نیز بما نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در فتنه یغما گری چشم تو ای شوخ</p></div>
<div class="m2"><p>آن چیست که غارتزده در کشور ما نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر پر شود ایران همه از حضرت اشرف</p></div>
<div class="m2"><p>یک بی شرفی مثل رئیس الوزرا نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صحبت بادب کن بر اهل ادب عارف</p></div>
<div class="m2"><p>اینجاست که جای سخن پرت و پلا نیست</p></div></div>