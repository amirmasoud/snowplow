---
title: >-
    شمارهٔ  ۳۴ - زاهد و باده
---
# شمارهٔ  ۳۴ - زاهد و باده

<div class="b" id="bn1"><div class="m1"><p>گذشت زاهد و لب تر ز دور باده نکرد</p></div>
<div class="m2"><p>ببین چه دور خوشی دید و استفاده نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعمد داد سر زلف خود بدست صبا</p></div>
<div class="m2"><p>چها که با من هستی بباد داده نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دچار فتنه شد آخر رقیب خورسندم</p></div>
<div class="m2"><p>چه فتنه ها که بپا این حرامزاده نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر به بستر راحت نمیتواند خفت</p></div>
<div class="m2"><p>کسیکه خصم خود از پشت زین پیاده نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بمجلس آمد یار از فراکسیون عجب آنک</p></div>
<div class="m2"><p>بهیچ کار بجز قتل من اراده نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قسم بساغر می در تمام عمر عارف</p></div>
<div class="m2"><p>بروی ساده رخان یک نگاه ساده نکرد</p></div></div>