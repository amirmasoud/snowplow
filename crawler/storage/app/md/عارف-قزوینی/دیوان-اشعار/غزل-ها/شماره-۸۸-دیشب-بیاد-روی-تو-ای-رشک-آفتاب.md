---
title: >-
    شمارهٔ  ۸۸ - دیشب بیاد روی تو ای رشک آفتاب
---
# شمارهٔ  ۸۸ - دیشب بیاد روی تو ای رشک آفتاب

<div class="b" id="bn1"><div class="m1"><p>دیشب بیاد روی تو ای رشک آفتاب</p></div>
<div class="m2"><p>شستم ز سیل اشگ من از دیده نقش خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا صبحدم که جیب افق چاک زد شفق</p></div>
<div class="m2"><p>صدرنگ ریخت دل بخیال رخت بر آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنشسته ام میانه سیلاب خون که گر</p></div>
<div class="m2"><p>بینی گمان بری که حبابی است روی آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد مست دل غمزه ات آنسان که مستیش</p></div>
<div class="m2"><p>افزون بود ز نشئه یک خم شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمت بزیر چشمی با یک اشاره کرد</p></div>
<div class="m2"><p>در هر کجا دلی است طرفدار انقلاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محروم شد ز روی تو زاهد، همیشه باد</p></div>
<div class="m2"><p>محروم ز آستین خطا دامن صواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خانه خرابه دل آنچه را که کرد</p></div>
<div class="m2"><p>چشمت، به کعبه آن نکند پیرو وهاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افتاد طره بر سر مژگانت آگه است</p></div>
<div class="m2"><p>گنجشک اوفتاده بسر پنجه عقاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از مهوشان شفق چو تو را انتخاب کرد</p></div>
<div class="m2"><p>تبریک گفت عارف از این حسن انتخاب</p></div></div>