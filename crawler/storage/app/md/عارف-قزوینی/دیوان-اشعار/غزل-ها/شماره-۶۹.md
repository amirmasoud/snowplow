---
title: >-
    شمارهٔ  ۶۹
---
# شمارهٔ  ۶۹

<div class="b" id="bn1"><div class="m1"><p>میخواستی دگر چه کند کرد یا نکرد</p></div>
<div class="m2"><p>مردم، قجر بمردم ایران چه ها نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کور دیده مردم خودبین بی خرد</p></div>
<div class="m2"><p>گر نیک بنگرید بجز بد بما نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با قید التزام خیانت به مملکت</p></div>
<div class="m2"><p>این پا بسر خطا و خیانت خطا نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیگانه را بخانه دو صد امتیاز داد</p></div>
<div class="m2"><p>در خانه باز در برخ آشنا نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهنشهی دوره کسرا نمود کسر</p></div>
<div class="m2"><p>تا صفر زان زیاد بغیر از گدا نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عارف چه شد که سید ضیاء آنچه را که دل</p></div>
<div class="m2"><p>میکرد آرزو، نتوانست یا نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی شه گرفت نی دو تن اشراف زد بدار</p></div>
<div class="m2"><p>گر گویمش که بدتر از این کرد یا نکرد</p></div></div>