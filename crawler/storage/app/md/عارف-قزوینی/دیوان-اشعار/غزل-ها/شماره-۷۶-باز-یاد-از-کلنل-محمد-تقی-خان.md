---
title: >-
    شمارهٔ  ۷۶ - باز یاد از کلنل محمد تقی خان
---
# شمارهٔ  ۷۶ - باز یاد از کلنل محمد تقی خان

<div class="b" id="bn1"><div class="m1"><p>برای اینکه مگر از تو دل نشان گیرد</p></div>
<div class="m2"><p>ز هر کنار گریبان این و آن گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه راه بسوی تو کاروان را نیست</p></div>
<div class="m2"><p>دل از هوس چو جرس راه کاروان گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجاست چون تو کز اشراف شهر تا برسد</p></div>
<div class="m2"><p>به شیخ و مرشد و جنگیر و روضه خوان گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وکیل و لیدر و سر دسته دزد در یکروز</p></div>
<div class="m2"><p>گرفته، داد ز دلهای ناتوان گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو اوفتاد بدست تو جان خصم امان</p></div>
<div class="m2"><p>چه شد که دادی امان، تا دوباره جان گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ارتجاع لگدکوب و پای مال تو شد</p></div>
<div class="m2"><p>بدان که پای بگیرد اگر جهان گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فکر کهنه خیال کهن دوامی نیست</p></div>
<div class="m2"><p>دوام ملک ز فکر نو و جوان گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ضیاء دیده روشندلان توئی و حسود</p></div>
<div class="m2"><p>چو موش کور ز خود کی توان عنان گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه غم ز هرزه درائی و لابه گوئی، از آن</p></div>
<div class="m2"><p>که سگ سکوت ز یک مشت استخوان گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمام ملک چرا گیرد آنکه می زیبد</p></div>
<div class="m2"><p>که میل سرمه و سرخاب و سرمه دان گیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه فاسق است در ایران ریاست وزرا</p></div>
<div class="m2"><p>که او به تجربه سرمشق از زنان گیرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به قرن بیست زن مرد کش، سپس نباش</p></div>
<div class="m2"><p>بروزن! آتش ننگت به دودمان گیرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قوام سلطنت این دور دور تست بکن!</p></div>
<div class="m2"><p>که انتقام از این دور آسمان گیرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس از شهادت کلنل گمان مبر عارف</p></div>
<div class="m2"><p>سکون گرفته و در یک مقر مکان گیرد</p></div></div>