---
title: >-
    شمارهٔ  ۱ - مس قلب در خور اکسیر
---
# شمارهٔ  ۱ - مس قلب در خور اکسیر

<div class="b" id="bn1"><div class="m1"><p>دل به تدبیر بر آن زلف چو زنجیر افتاد</p></div>
<div class="m2"><p>وای بر حالت دزدی که به شبگیر افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانه خال لب و دام سر زلف تو دید</p></div>
<div class="m2"><p>شد پشیمان که در این دام چرا دیر افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه و بیگاه ز بس آه کشیدم ز غمت</p></div>
<div class="m2"><p>سینه آتشکده شد آه ز تأثیر افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نگاهی دل ویرانه چنان کرده خراب</p></div>
<div class="m2"><p>که دگر کار دل از صورت تعمیر افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عارفا بندگی پیر مغانت خوش باد</p></div>
<div class="m2"><p>مس قلب تو چه شد در خور اکسیر افتاد</p></div></div>