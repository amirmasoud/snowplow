---
title: >-
    شمارهٔ  ۷۵
---
# شمارهٔ  ۷۵

<div class="b" id="bn1"><div class="m1"><p>ز طفلی آنچه بمن یاد داد استادم</p></div>
<div class="m2"><p>به غیر عشق برفت آنچه بود از یادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکند سیل غم عشق بیخ و بنیانم</p></div>
<div class="m2"><p>به باد رفت ز بیداد هجر بنیادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای پیروی از دل ملامتم نکنید</p></div>
<div class="m2"><p>برای این که ز مادر برای این زادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غمزه از من بی خانمان خانه بدوش</p></div>
<div class="m2"><p>گرفت هستی و من هرچه داشتم دادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آنچه رنگ تعلق بغیر بی رنگی</p></div>
<div class="m2"><p>گرفت یا که بخواهد گرفتن آزادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا بآنکه به هستی ز نیستی آورد</p></div>
<div class="m2"><p>قسم، به سایه دیوار نیستی شادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز پا درآمده در خون نشسته آن صیدم</p></div>
<div class="m2"><p>که رستم از غم و راحت نشست صیادم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفت جا بدلم کوه ناله مبهوتم</p></div>
<div class="m2"><p>چه شد که گوش تو نشنیده داد و فریادم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فغان و ناله و فریاد من جهانی را</p></div>
<div class="m2"><p>فرا گرفت نیامد کسی به امدادم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به نام همت مولا به نقش بی رنگی</p></div>
<div class="m2"><p>خوشم بعشق علی در خیال ارشادم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>علی بگوی اگر ناتوان شدی عارف</p></div>
<div class="m2"><p>علی نگفتم و در ناتوانی افتادم</p></div></div>