---
title: >-
    شمارهٔ  ۷۹ - دوخا محمد و عارف
---
# شمارهٔ  ۷۹ - دوخا محمد و عارف

<div class="b" id="bn1"><div class="m1"><p>ابرویش تا رقم قتل من امضاء می‌کرد</p></div>
<div class="m2"><p>مژه این حکم برون نامده اجرا می‌کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بچه حالی که دل سنگ به حالم می‌سوخت</p></div>
<div class="m2"><p>چشم خونریز وی این حال تماشا می‌کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدش از هر قدمی فتنه به پا می‌انگیخت</p></div>
<div class="m2"><p>لبش از هر سخنی مفسده برپا می‌کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه در واهمه این مردم از آن مردم چشم</p></div>
<div class="m2"><p>این همه همهمه یک بی‌سر و بی‌پا می‌کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از در دیده هرکس که گذر کرده، هنوز</p></div>
<div class="m2"><p>دور از دیده نگردیده به دل جا می‌کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دلی را که شدی خیل خیالش داخل</p></div>
<div class="m2"><p>محو چون داخله مملکت ما می‌کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من به هر شاخی از این باغ ز بیداد محیط</p></div>
<div class="m2"><p>آشیان بستم از آنجا پر من وا می‌کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار رسوایی دل بین که مرا در نظر</p></div>
<div class="m2"><p>کشوری، این همه رسوا شده رسوا می‌کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تلخ کامی من از زندگی این بس که دلم</p></div>
<div class="m2"><p>شهد آسودگی از مرگ تمنا می‌کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش از آنیکه زند سبزه سر از خاکش کاش</p></div>
<div class="m2"><p>دل (عارف) هوس سبزه و صحرا می‌کرد</p></div></div>