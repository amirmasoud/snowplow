---
title: >-
    شمارهٔ  ۶۸ - غزل پوشالی
---
# شمارهٔ  ۶۸ - غزل پوشالی

<div class="b" id="bn1"><div class="m1"><p>چه داد خواهی از این دادخواه پوشالی</p></div>
<div class="m2"><p>ز شاه کشور جم جایگاه پوشالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجای تاج کیانی و تخت جم مانده است</p></div>
<div class="m2"><p>حصیر پاره بجا و کلاه پوشالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بقدر یک سر موئی عدو نیندیشد</p></div>
<div class="m2"><p>از این سپهبد و از این سپاه پوشالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آه سینه پوشالی آتش افروزیم</p></div>
<div class="m2"><p>بکاخ و قصر و باین بارگاه پوشالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببین چه غافل و آرام خفته این ملت</p></div>
<div class="m2"><p>چو گوسفند در آرامگاه پوشالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پناه ملت مجلس بود چو گردد چاه</p></div>
<div class="m2"><p>پناهگاه بسوز این پناه پوشالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگو چگونه ز دنیا گذشته ای درویش</p></div>
<div class="m2"><p>که دل نمیکنی از خانقاه پوشالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهار آمد و عارف نمیشود سرسبز</p></div>
<div class="m2"><p>ز باغ و لاله و خرم گیاه پوشالی</p></div></div>