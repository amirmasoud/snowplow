---
title: >-
    شمارهٔ  ۲۰ - مرگ دوست
---
# شمارهٔ  ۲۰ - مرگ دوست

<div class="b" id="bn1"><div class="m1"><p>بمرگ دوست مرا میل زندگانی نیست؟</p></div>
<div class="m2"><p>ز عمر سیر شدم مرگ ناگهانی نیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بقای خویش نخواهم از آنکه میدانم</p></div>
<div class="m2"><p>که اعتماد بر این روزگار فانی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشم که هیچکس از من دگر نشان ندهد</p></div>
<div class="m2"><p>بکوی عشق نشان به ز بی نشانی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیاه روی نداری شود که گر بروم</p></div>
<div class="m2"><p>ببزم دوست بجز خجلت ارمغانی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خزم بخرقه پشمین خود که این گرمی</p></div>
<div class="m2"><p>بخرقه خز و در جامه یمانی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رهین منت چشمم نه چشمه حیوان</p></div>
<div class="m2"><p>بگو به خضر که این وضع زندگانی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سراغ وادی دیوانگان ز مجنون گیر</p></div>
<div class="m2"><p>جنون عشق بود این شتر چرانی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پرسش دل من آئی آنزمان که مرا</p></div>
<div class="m2"><p>برای گفتن درد درون زبانی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بزیر خرقه ز من مشتی استخوان مانده است</p></div>
<div class="m2"><p>بجان دوست که در زیر جامه جانی نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو شاهبازی و خواهی کنی سرافرازم</p></div>
<div class="m2"><p>منم خجل که در این باغم آشیانی نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وحید عصر خودی عارفا بدان امروز</p></div>
<div class="m2"><p>که از برای تو در زیر چرخ ثانی نیست</p></div></div>