---
title: >-
    شمارهٔ  ۵۳ - شکایت تلخ
---
# شمارهٔ  ۵۳ - شکایت تلخ

<div class="b" id="bn1"><div class="m1"><p>محیط گریه و اندوه و غصه و محنم</p></div>
<div class="m2"><p>کسیکه یک نفس آسودگی ندید منم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم که در وطن خویشتن غریبم وزین</p></div>
<div class="m2"><p>غریبتر که هم از من غریبتر وطنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر کجا که قدم مینهم بکشور خویش</p></div>
<div class="m2"><p>دچار دزد اداری اسیر راهزنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبیعت از پی آزار من کمر بسته</p></div>
<div class="m2"><p>کنم چه چاره چو دشمن قویست دم نزنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهال عمر مرا میوه غیر تلخی نیست</p></div>
<div class="m2"><p>بر آن سرم که من این بیخ را ز بن بکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شمع آب شدم بسکه سوختم فریاد</p></div>
<div class="m2"><p>که دیگران نه نشستند پای سوختنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو گشت محرم بیگانه خانه، به در گور</p></div>
<div class="m2"><p>کفن بیار که نامحرم است پیرهنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز قید تن شوم آزاد وان زمان زین بند</p></div>
<div class="m2"><p>برون شوم، نیم آزاد تا اسیر تنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به چشم من همه گلهای گلستان چون خار</p></div>
<div class="m2"><p>خلد، اگر به تماشای گل نظر فکنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در این دیار چه خاکی بسر توانم کرد</p></div>
<div class="m2"><p>بهر کجا که روم اوفتاده در لجنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگو بیار که اندر پی هلاکت من</p></div>
<div class="m2"><p>دگر مکوش که خود در هلاک خویشتنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبرد لذت شیرینی سخن عارف</p></div>
<div class="m2"><p>به گوش عبرت نشنید گر کسی سخنم</p></div></div>