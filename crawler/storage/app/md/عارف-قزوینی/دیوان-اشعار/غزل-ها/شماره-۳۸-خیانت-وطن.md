---
title: >-
    شمارهٔ  ۳۸ - خیانت وطن
---
# شمارهٔ  ۳۸ - خیانت وطن

<div class="b" id="bn1"><div class="m1"><p>دوباره فتنه چشم تو فتنه برپا کرد</p></div>
<div class="m2"><p>دلم ز شهر چو دیوانه رو به صحرا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدا خراب کند آن کسی که مملکتی</p></div>
<div class="m2"><p>برای منفعت خویش خوان یغما کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بخت یاری بیجا طلب مکن کاین شوم</p></div>
<div class="m2"><p>چو جغد میل بویرانه داشت غوغا کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفیق او همدانی است خوب میدانست</p></div>
<div class="m2"><p>که گفت «کرد غلط هرچه کرد عمدا کرد»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو در قلمرو خود دید صفحه ایران</p></div>
<div class="m2"><p>سیاه و درهم چون صفحه چلیپا کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهاد کشتن نفس است نی چپاول مال</p></div>
<div class="m2"><p>در این مجاهده عارف مرا چه رسوا کرد</p></div></div>