---
title: >-
    شمارهٔ  ۴۷ - تیغ زبان  پرده های ریا!
---
# شمارهٔ  ۴۷ - تیغ زبان  پرده های ریا!

<div class="b" id="bn1"><div class="m1"><p>محشر هر جا روم آنجا سر پا خواهم کرد</p></div>
<div class="m2"><p>بین چه آشوب من بیسر و پا خواهم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که از کرده پشیمان شده‌ام در هر کار</p></div>
<div class="m2"><p>نتوان گفت کزین بعد چه‌ها خواهم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بهر کار زدم دست ریا دیدم، روی</p></div>
<div class="m2"><p>بدر میکده بی روی ریا خواهم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدر ای پیر مغان پرده ارباب ریا</p></div>
<div class="m2"><p>ورنه در کار خرابات ریا خواهم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر طبیعت نشود پرده دراز مشتی دزد</p></div>
<div class="m2"><p>پرده شان پاره بامید خدا خواهم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از این خرقه سالوس بدر خواهم شد</p></div>
<div class="m2"><p>ترک عمامه و دستار و ردا خواهم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتیم مطرب الحمد که در کشور خویش</p></div>
<div class="m2"><p>آن وظیفه که مرا هست ادا خواهم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منع زاهد سبب خوردن می شد ورنه</p></div>
<div class="m2"><p>محتسب گوید اگر مستی، ابا خواهم کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه ز همسایه، که از سایه خود میترسم</p></div>
<div class="m2"><p>دوری از سایه این جنس دو پا خواهم کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم «ایران رود هر وقت تو آنوقت بیا»</p></div>
<div class="m2"><p>در سر وعده من ای مرگ وفا خواهم کرد</p></div></div>