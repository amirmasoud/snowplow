---
title: >-
    شمارهٔ  ۴۶ - جمهوری عشق  سلطنت حسن
---
# شمارهٔ  ۴۶ - جمهوری عشق  سلطنت حسن

<div class="b" id="bn1"><div class="m1"><p>عشق! مریزادت آن دو بازوی پر زور</p></div>
<div class="m2"><p>قادر و قاهر توئی و ما همه مقهور!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلطنت حسن را دوام و بقائی</p></div>
<div class="m2"><p>نیست مباش ای پسر مخالف جمهور!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی مپوشان که بیش از این نتوان دید</p></div>
<div class="m2"><p>جلوه کند آفتاب و روی تو مستور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شانه بزلفت مزن که خانه دلهاست</p></div>
<div class="m2"><p>چوب مکن بیجهت بلانه زنبور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای اجانب بریده گردد از ایران</p></div>
<div class="m2"><p>چشم بداندیش اگر ز روی تو شد دور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست خودی پای اجنبی ز میان برد</p></div>
<div class="m2"><p>مملکت اردشیر و کشور شاپور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخوت و کبر اینقدر چرا و چرائی</p></div>
<div class="m2"><p>از پی حسن دو روزه این همه مغرور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همدم بیگانگان مباش و بپرهیز</p></div>
<div class="m2"><p>عاقبت از جنس بد ز وصله ناجور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عارف اگر کهنه شد ترانه مزدک</p></div>
<div class="m2"><p>نغمه ای از نو علاوه کن تو به تنبور!</p></div></div>