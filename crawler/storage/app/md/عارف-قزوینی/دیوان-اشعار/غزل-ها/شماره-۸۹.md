---
title: >-
    شمارهٔ  ۸۹
---
# شمارهٔ  ۸۹

<div class="b" id="bn1"><div class="m1"><p>آتش الهی آنکه بیفتد میان دل</p></div>
<div class="m2"><p>نابود همچو دود شود دودمان دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانند خاندان گل از صرصر خزان</p></div>
<div class="m2"><p>هستی به باد داده شود خانمان دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>احوال دل مپرس که خون ریزد از قلم</p></div>
<div class="m2"><p>وقتی که می‌رسم سَرِ شَرحِ بیان دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با اینکه کرده خاک‌نشینم، سر درست</p></div>
<div class="m2"><p>مشکل برم به گور ز دست زبان دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسوا شدم ز دست دل آن سان که هر که را</p></div>
<div class="m2"><p>بینی حدیث من بود و داستان دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از من بریده الفت و با سگ گرفته خوی</p></div>
<div class="m2"><p>دل مهربان به سگ شده سگ پاسبان دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمم ندیده روی خوشی در تمام عمر</p></div>
<div class="m2"><p>بدبخت دیده‌ای که بود دیده‌بان دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افتاده در کمند خم طره‌ای به دام</p></div>
<div class="m2"><p>از آشیان عقاب بلندآشیان دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل را به طره تو سپردم ترا به حق</p></div>
<div class="m2"><p>هرجا که هست جان تو ای دوست جان دل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیگر به چشم خویش نیم مطمئن از آنک</p></div>
<div class="m2"><p>برداشت پرده از سَرِ سِرّ نهان دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد اشک محرم دل و از راه دوستی</p></div>
<div class="m2"><p>راه بهانه داد، کف دشمنان دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوبان یک از هزار ز تحصیل درس عشق</p></div>
<div class="m2"><p>بیرون نیامدند خوش از امتحان دل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر لامکان و خانه‌به‌دوشم ترا چه غم؟</p></div>
<div class="m2"><p>کاندر جوار جوانی و وندر مکان دل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یار ار نشان عارف بی‌نام از تو خواست</p></div>
<div class="m2"><p>برگو به آن نشان که گرفتی نشان دل</p></div></div>