---
title: >-
    شمارهٔ  ۴۸ - تمدن بی تربیت نسوان  سفر نیمه راه!
---
# شمارهٔ  ۴۸ - تمدن بی تربیت نسوان  سفر نیمه راه!

<div class="b" id="bn1"><div class="m1"><p>بفکن نقاب و بگذار در اشتباه ماند</p></div>
<div class="m2"><p>تو بر آن کسی که می‌گفت رخت به ماه ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدر این حجاب و آخر بدر آز ابر چون خور</p></div>
<div class="m2"><p>که تمدن ار نیائی تو به نیم راه ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو از این لباس خواری شوی عاری و برآری</p></div>
<div class="m2"><p>بدر همچه گل سر از تربتم ار گیاه ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل آنکه روت با واسطه حجاب خواهد</p></div>
<div class="m2"><p>تو مگوی دل که آن دل بجوال کاه ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پی صلح اگر تو بی پرده سخن میان گذاری</p></div>
<div class="m2"><p>نه حریف جنگ باقی نه صف سپاه ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو از آن زمان که پنهان رخ از ابر زلف کردی</p></div>
<div class="m2"><p>همه روزه تیره روزم بشب سیاه ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه ز شرم می نیارم برخت نگاه ترسم</p></div>
<div class="m2"><p>که برویت از لطافت اثر نگاه ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه شب پناه بر درگه حق برم که عمری</p></div>
<div class="m2"><p>ز دو چشم بد رخ خوب تو در پناه ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه ترس من از آنست خدا نکرده روزی</p></div>
<div class="m2"><p>سرما به پشت این معرکه بی کلاه ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز وزیر جنگ ما اسم و رسمی در میان نه</p></div>
<div class="m2"><p>سپهش نبینی عارف به سپاه آه ماند</p></div></div>