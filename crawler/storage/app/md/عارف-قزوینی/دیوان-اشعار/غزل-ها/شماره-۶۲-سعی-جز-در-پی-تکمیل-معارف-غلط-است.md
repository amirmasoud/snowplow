---
title: >-
    شمارهٔ  ۶۲ - سعی جز در پی تکمیل معارف غلط است
---
# شمارهٔ  ۶۲ - سعی جز در پی تکمیل معارف غلط است

<div class="b" id="bn1"><div class="m1"><p>دل که در سایه مژگان تو فارغ بال است</p></div>
<div class="m2"><p>گو ببین چشم بداندیش چه از دنبال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد از یک نگهی داد دل و بستد جان</p></div>
<div class="m2"><p>وه چه بد بدرقه چشمت چه خوش استقبال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد پسر سام بگیتی اگر آرد تنها</p></div>
<div class="m2"><p>تربیت آنکه ز سیمرغ بگیرد زال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سعی جز در پی تکمیل معارف غلط است</p></div>
<div class="m2"><p>ملت جاهل محکوم به اضمحلال است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستقل نیست دو کس در سر یک رأی ولی</p></div>
<div class="m2"><p>سر هر برزن و کو صحبت از استقلال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بداخلاقی و اشرافی فرمانفرماست</p></div>
<div class="m2"><p>تا ابد حالت ایران بهمین منوال است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس آخر این ملت محکوم بمرگ</p></div>
<div class="m2"><p>در شمار است بدافتاده و بداحوال است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عارف این خانه کند تربیت جغد کجا</p></div>
<div class="m2"><p>جای همچون شفقی مرغ همایون فال است</p></div></div>