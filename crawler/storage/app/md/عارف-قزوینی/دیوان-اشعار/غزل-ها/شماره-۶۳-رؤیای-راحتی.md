---
title: >-
    شمارهٔ  ۶۳ - رؤیای راحتی
---
# شمارهٔ  ۶۳ - رؤیای راحتی

<div class="b" id="bn1"><div class="m1"><p>در دور زندگی به جز از غم ندیده‌ام</p></div>
<div class="m2"><p>یک روز خوش ز عمر به عمرم ندیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم ببینم اینکه شب راحتی به خواب</p></div>
<div class="m2"><p>دیدم ز دست هجر تو دیدم ندیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتند دم ز عمر غنیمت توان شمرد</p></div>
<div class="m2"><p>من در شمار عمر خود آن دم ندیده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سال و ماه و هفته و ایام زندگی</p></div>
<div class="m2"><p>یک روز عید غیر محرم ندیده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از اولین سلاله آدم الی کنون</p></div>
<div class="m2"><p>زین خانواده یک نفر آدم ندیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندین هزار رشته مهر و وفا گسیخت</p></div>
<div class="m2"><p>یک رشته ناگسیخته محکم ندیده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دیده خیال و تصور که ممکن است</p></div>
<div class="m2"><p>گردد دو دل به هم یکی آن هم ندیده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز طره پریش تو و روزگار خویش</p></div>
<div class="m2"><p>ز اوضاع چرخ درهم و برهم ندیده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز جام می که عقده‌گشای غم است و بس</p></div>
<div class="m2"><p>کس در خرابه مملکت جم ندیده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عارف به غیر بارگه پیر می‌فروش</p></div>
<div class="m2"><p>گردن برای کرنش کس خم ندیده‌ام</p></div></div>