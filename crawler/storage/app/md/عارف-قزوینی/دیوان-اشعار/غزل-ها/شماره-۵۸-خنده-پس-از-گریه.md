---
title: >-
    شمارهٔ  ۵۸ - خنده پس از گریه
---
# شمارهٔ  ۵۸ - خنده پس از گریه

<div class="b" id="bn1"><div class="m1"><p>به سر کویت اگر رخت نبندم چه کنم</p></div>
<div class="m2"><p>واندر آن کوی اگر ره ندهندم چه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ز در بستن و واکردن میخانه به جان</p></div>
<div class="m2"><p>آمدم گر نکنم باز و نبندم چه کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم هجران و پریشانی و بدبختی من</p></div>
<div class="m2"><p>تو پسندیدی اگر من نپسندم چه کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانده در قید اسارت تن من وان خم زلف</p></div>
<div class="m2"><p>می‌کشد، می‌روم افتاده به بندم چه کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به اوضاع تو ای کشور بی‌صاحب جم</p></div>
<div class="m2"><p>نکنم گریه پس از گریه نخندم چه کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیت روی تو ز آتشکده زردشت است</p></div>
<div class="m2"><p>من بر آن آتش سوزان چو سپندم چه کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون من ریختی و وصل تو شد کام رقیب</p></div>
<div class="m2"><p>من به ناچار دل از مهر تو کندم چه کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرط عقل است سپس راه جنون گیرم و بس</p></div>
<div class="m2"><p>عارف آسوده من از ناصح و پندم چه کنم</p></div></div>