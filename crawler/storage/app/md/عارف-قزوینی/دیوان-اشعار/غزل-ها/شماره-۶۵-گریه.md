---
title: >-
    شمارهٔ  ۶۵ - گریه
---
# شمارهٔ  ۶۵ - گریه

<div class="b" id="bn1"><div class="m1"><p>مگر چسان نکنم گریه گریه کار من است</p></div>
<div class="m2"><p>کسیکه باعث اینکار گشته یار من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>متاع گریه ببازار عشق رایج و اشک</p></div>
<div class="m2"><p>برای آبرو و قدر و اعتبار من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شده است کور ز دست دل جنایتکار</p></div>
<div class="m2"><p>دو دیده من و دل هم جریحه دار من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو کوه غم پس زانو بزیر سایه اشک</p></div>
<div class="m2"><p>نشسته منظره اشک آبشار من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تیره روزی و بد روزگاریم یکعمر</p></div>
<div class="m2"><p>گذشت و بگذرد این روز روزگار من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان مردم ننگین من آنقدر ننگین</p></div>
<div class="m2"><p>شدم که، ننگ من اسباب افتخار من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تگرگ مرگ بگو سیل خون ببار و ببر</p></div>
<div class="m2"><p>تو رنگ ننگ که آن فصل خوش بهار من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مدام خون دل خویشتن خورم زین ره</p></div>
<div class="m2"><p>معیشت من و از این ممر مدار من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسر چه خاک بجز خاک تعزیت ریزم</p></div>
<div class="m2"><p>به کشوریکه مصیبت زمامدار من است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدان محرم ایرانی اول صفر است</p></div>
<div class="m2"><p>که قتل نادر ناکام نامدار من است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فشار مرگ که گویند بهر تن پس مرگ</p></div>
<div class="m2"><p>به من چه من چه کنم روح در فشار من است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تدارک سفر مرگ دید عارف و گفت</p></div>
<div class="m2"><p>در این سفر کلنل چشم انتظار من است</p></div></div>