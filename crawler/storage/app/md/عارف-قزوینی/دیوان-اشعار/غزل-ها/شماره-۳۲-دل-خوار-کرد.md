---
title: >-
    شمارهٔ  ۳۲ - دل خوار کرد
---
# شمارهٔ  ۳۲ - دل خوار کرد

<div class="b" id="bn1"><div class="m1"><p>دل خوار کرد در بر هر خار و خس مرا</p></div>
<div class="m2"><p>نگذاردم بحال خود این بوالهوس مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بسکه غم کشیده مرا سر بزیر پر</p></div>
<div class="m2"><p>خوشتر ز عالمی شده کنج قفس مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرسد طبیب درد دلم را چه گویمش</p></div>
<div class="m2"><p>چون نیست اهل درد همین درد بس مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با هرکسی ز مهر زدم دم چو خود نبود</p></div>
<div class="m2"><p>اهل وفا نگشت یکی دادرس مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستم رها کنید بگریم بحال خویش</p></div>
<div class="m2"><p>مست آنقدر نیم که بگیرد عسس مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نورسیده ام زره ای پیر میفروش</p></div>
<div class="m2"><p>از آن شراب کال یکی کامرس مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنگی بدل نمیزندم نغمه های عود</p></div>
<div class="m2"><p>ای تار و نی شوید دمی هم نفس مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم که بد معرفی عارف شدی و گفت</p></div>
<div class="m2"><p>«این نام نیک تا ابدالدهر بس مرا!»</p></div></div>