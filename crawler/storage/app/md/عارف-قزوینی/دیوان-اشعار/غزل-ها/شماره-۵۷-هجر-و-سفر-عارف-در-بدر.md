---
title: >-
    شمارهٔ  ۵۷ - هجر و سفر  عارف در بدر
---
# شمارهٔ  ۵۷ - هجر و سفر  عارف در بدر

<div class="b" id="bn1"><div class="m1"><p>عمرم گهی بهجر و گهی در سفر گذشت</p></div>
<div class="m2"><p>تاریخ زندگی همه در درد سر گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند اینکه عمر سفر کوته است و من</p></div>
<div class="m2"><p>دیدم که عمر من ز سفر زودتر گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بستی درم ز وصل و گشودی دری ز هجر</p></div>
<div class="m2"><p>آوخ ببین چه ها بمن دربدر گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هجر تو خون دل به حسابت حواله کرد</p></div>
<div class="m2"><p>در دوریت معیشتم از این ممر گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با کوه کوه بار فراق غمت بکوه</p></div>
<div class="m2"><p>رفتم، رسید سیل سرشک از کمر گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بازیچه نیست عشق و محبت مگر نبود</p></div>
<div class="m2"><p>در راه عشق یار پدر از پسر گذشت؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سود و زیان و نفع و ضرر دخل و خرج عشق</p></div>
<div class="m2"><p>کردم پس از هزار ضرر سربسر گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را چه خوب دست بسر کرد تا که چشم</p></div>
<div class="m2"><p>آمد ببیندش که چو برق از نظر گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کو، تا دگر پدید شود گویمش چه ها</p></div>
<div class="m2"><p>بر من ز دست ظلم تو بیدادگر گذشت!»</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاری مکن که خلق ز جورت بجان رسند</p></div>
<div class="m2"><p>ای جور پیشه، ورنه ز من یکنفر گذشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشکل بود که از خطر عشق بگذری</p></div>
<div class="m2"><p>عارف تو را که عمر ز چندین خطر گذشت</p></div></div>