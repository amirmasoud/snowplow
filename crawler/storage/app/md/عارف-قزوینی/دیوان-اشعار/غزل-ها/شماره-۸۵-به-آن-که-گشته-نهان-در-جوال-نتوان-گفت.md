---
title: >-
    شمارهٔ  ۸۵ - به آن که گشته نهان در جوال نتوان گفت
---
# شمارهٔ  ۸۵ - به آن که گشته نهان در جوال نتوان گفت

<div class="b" id="bn1"><div class="m1"><p>به یار شرح دل پرملال نتوان گفت</p></div>
<div class="m2"><p>نگفته بهتر، امر محال نتوان گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال یکشبه هجر تا بدامن حشر:</p></div>
<div class="m2"><p>اگر شود همه روزه وصال نتوان گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسان نقش خیال از تصورات خیال</p></div>
<div class="m2"><p>شدم تمام و بکس این خیال نتوان گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تراست پنبه غفلت بگوش و من الکن</p></div>
<div class="m2"><p>بگوش کر، سخن از قوش لال نتوان گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهان بپرده اسرار عشق یکسر موی</p></div>
<div class="m2"><p>به آنکه گشته نهان در جوال نتوان گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن ز علم مگو پیش جهل در بر جغد</p></div>
<div class="m2"><p>حدیث طایر «فرخنده فال » نتوان گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خموش باش که در پیشگاه حسن و جمال</p></div>
<div class="m2"><p>سخن ز مکنت و جاه و جلال نتوان گفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بملتی که ز تاریخ خویش بی خبر است</p></div>
<div class="m2"><p>بجز حکایت محو و زوال نتوان گفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پیش شیخ ز اسرار میفروش مگوی</p></div>
<div class="m2"><p>که حرف راست بشیطانخیال نتوان گفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مجال آنکه دهم شرح زندگانی خویش</p></div>
<div class="m2"><p>بدست خامه، بعمری مجال نتوان گفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس است شکوه و دلتنگی اینقدر عارف</p></div>
<div class="m2"><p>بد از محیط، علی الاتصال نتوان گفت</p></div></div>