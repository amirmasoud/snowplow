---
title: >-
    شمارهٔ  ۲۵ - دست به دامان!
---
# شمارهٔ  ۲۵ - دست به دامان!

<div class="b" id="bn1"><div class="m1"><p>گر رسد دست من به دامانش</p></div>
<div class="m2"><p>می‌زنم چاک تا گریبانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمرم اندر غمت بپایان شد</p></div>
<div class="m2"><p>شب هجر تو نیست پایانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد عشق آنقدر نصیبم کن</p></div>
<div class="m2"><p>که توانی رسی بدرمانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه با من بزندگی کرده است</p></div>
<div class="m2"><p>مرگ من میکند پشیمانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست و پا جمع کن که میگذرد</p></div>
<div class="m2"><p>بسر کشته شهیدانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر دل فاش کرد دیده از آن</p></div>
<div class="m2"><p>که دگر نیست حال کتمانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بنائی بکار عالم نیست</p></div>
<div class="m2"><p>بکن ای سیل اشک بنیانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که از کاسه سر جم خورد</p></div>
<div class="m2"><p>باده، سازد جهان نمایانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساغر می بگردش آر که چرخ</p></div>
<div class="m2"><p>نیست مستحکم عهد و پیمانش</p></div></div>