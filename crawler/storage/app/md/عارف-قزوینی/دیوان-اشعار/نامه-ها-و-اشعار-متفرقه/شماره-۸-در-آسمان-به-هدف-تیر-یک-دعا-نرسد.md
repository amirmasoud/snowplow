---
title: >-
    شمارهٔ  ۸ - در آسمان به هدف تیر یک دعا نرسد
---
# شمارهٔ  ۸ - در آسمان به هدف تیر یک دعا نرسد

<div class="b" id="bn1"><div class="m1"><p>شهید عشق تو کارش بدست و پا نرسد</p></div>
<div class="m2"><p>بداد آنکه تو راندی ز خود خدا نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسید عمر بپایان گذشت جان ز لبم</p></div>
<div class="m2"><p>رسید بر لب جانان ولی بجا نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوای سایه بالای آن کسم بسر است</p></div>
<div class="m2"><p>که بر سر کسی این سایه بی بلا نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذشت کار من از حرف باز از پی من</p></div>
<div class="m2"><p>بغیر یاوه سرائی ژاژخا نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر بحرف اثر بود زین میانه چرا</p></div>
<div class="m2"><p>در آسمان به هدف تیر یک دعا نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقیده عقده کلک مسلک و محن میهن</p></div>
<div class="m2"><p>به من ز عشق وطن غیر از ابتلا نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشهر نیستی آنسان غریب افتادم</p></div>
<div class="m2"><p>که سالها شد و یک یار آشنا نرسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو تا رسائی اقبال من ببین که رسا</p></div>
<div class="m2"><p>رسید بر همه در هر کجا بما نرسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه شد که دست تو عارف شد آنقدر کوتاه</p></div>
<div class="m2"><p>که هرچه کردی بر دامن رسا نرسد</p></div></div>