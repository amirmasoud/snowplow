---
title: >-
    شمارهٔ  ۱ - جواب عارف به دوستش آقای مر هزار
---
# شمارهٔ  ۱ - جواب عارف به دوستش آقای مر هزار

<div class="b" id="bn1"><div class="m1"><p>بهتر ز کوی یار، دل اندر نظر نداشت</p></div>
<div class="m2"><p>یا چون من فلک زده جای دگر نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین هزار یار گرفتم یک از هزار</p></div>
<div class="m2"><p>همچون «هزار» از دل زارم خبر نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم من آنچه راست، بگوشی اثر نکرد</p></div>
<div class="m2"><p>کشتم هر آنچه تخم حقیقت، ثمر نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز قطره خون، که دیده نثار ره تو کرد</p></div>
<div class="m2"><p>دل در بساط آه دگر در جگر نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک عمر ریختیم بدل خون و حاصلش</p></div>
<div class="m2"><p>این قطره گشت و هیچ ازین بیشتر نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنی غم از خرابی بنیان عمر من</p></div>
<div class="m2"><p>غفلت نکرد، بیشتر از این هنر نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا آن دقیقه ای که نکرد استخوانم آب</p></div>
<div class="m2"><p>از سر هوای عشق وطن دست برنداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز اول قدم، جدا شدم از همرهان، که کس</p></div>
<div class="m2"><p>در این طریق، یک قدم راست، برنداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایران پیر، همچو جوانان دور ما</p></div>
<div class="m2"><p>بی عفت و خطا روش و بدگهر نداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وجدان و حس، دو دشمن فولاد پنجه اند</p></div>
<div class="m2"><p>آنکس که داشت دشمن از اینان بتر نداشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوگند میخورم به حقیقت که در جهان</p></div>
<div class="m2"><p>روح بشر، ز روح حقیقت خبر نداشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از عشق سگ، ملامت عارف مکن که گفت</p></div>
<div class="m2"><p>جز این بهر چه دست زدم جز ضرر نداشت</p></div></div>