---
title: >-
    شمارهٔ  ۶
---
# شمارهٔ  ۶

<div class="b" id="bn1"><div class="m1"><p>نویسنده را بایدی چار چیز</p></div>
<div class="m2"><p>دل و دست و افکار و وجدان تمیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر اینکه ناپاک شد این چهار</p></div>
<div class="m2"><p>ز ناپاکی صاحبش شک مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خوش گفت سعدی خدای سخن</p></div>
<div class="m2"><p>بتحریف آن گفته بشنو زمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتد تیغ اگر دست زنگی مست</p></div>
<div class="m2"><p>از آن به قلم در کف خودپرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قلم چون گرفتی دوروئی مکن</p></div>
<div class="m2"><p>غرض ورزی و کینه جوئی مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکف خاک، چشم فتوت مپاش</p></div>
<div class="m2"><p>گرفتی قلم بی مروت مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن بی شمار است و مطلب هزار</p></div>
<div class="m2"><p>مگو حرف بی مغز نااستوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ره راستی و درستی گزین</p></div>
<div class="m2"><p>جز از راستی و درستی مبین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قلم گیر و همچون قلم راست باش</p></div>
<div class="m2"><p>نه هر چش خیال کجت خواست، باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجی گر، ز شمشیر جوئی بجاست</p></div>
<div class="m2"><p>قلم راست باید، چو کج شد خطاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قلم کز پی زحمت مردم است</p></div>
<div class="m2"><p>قلم نیست نیش دم کژدم است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مشو خار راه خیال کسی</p></div>
<div class="m2"><p>که اندیشه ز اندیشه باید بسی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رخ زشت چون خبث طینت مپوش</p></div>
<div class="m2"><p>بگمنامی و باز گوشی مکوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز نام تو ای ننگ باد حامیت</p></div>
<div class="m2"><p>چه دیدم که بینم ز گمنامیت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مقاله نویسی و بسط مقال</p></div>
<div class="m2"><p>چه لازم بزیر چپیه عقال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرت ننگ و رسوائی و عار نیست</p></div>
<div class="m2"><p>بگمنام بودنت اصرار چیست؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>درآ از پس پرده استتار</p></div>
<div class="m2"><p>نه مردی تو هم؟ سر بمردی برآر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زنان را هم از پرده نک رم بود</p></div>
<div class="m2"><p>چه مردی بود کز زنی کم بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برو بنده و پیچه و روی زشت</p></div>
<div class="m2"><p>نباید بدلخواه چیزی نوشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر بود وجدان نباید زبون</p></div>
<div class="m2"><p>به پیچش در چادر قیرگون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بخوی تو مزمن شد این ناخوشی</p></div>
<div class="m2"><p>که یکعمر کوشی بوجدان کشی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چرا عاریت جامه ات دربر است</p></div>
<div class="m2"><p>خر ار جل ز اطلس بپوشد خر است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز تغییر رنگ و به تبدیل نام</p></div>
<div class="m2"><p>تو را میشناسم من ای بدلجام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو را باب حرص است و مام تو آز</p></div>
<div class="m2"><p>تو زاینده آزی ای حقه باز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مزن بر رسیده دمل نیشتر</p></div>
<div class="m2"><p>مدر پرده خویش زین بیشتر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو نه اهل رزمی و نه اهل بزم</p></div>
<div class="m2"><p>تو دزدی و غارتگر نثر و نظم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مکرر بگوش من این داستان</p></div>
<div class="m2"><p>فرو رفته از گفته راستان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شنیدم چو طومار عمر «بهار»</p></div>
<div class="m2"><p>به پیچید اجل زد خزانش ببار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز شروان سوی طوس آمد فرود</p></div>
<div class="m2"><p>بخان تو مهمانکش آمد فرود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شدی میزبان سیه کاسه اش</p></div>
<div class="m2"><p>ببردی بتاراج سرمایه اش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو از تن برون شد روان جان او</p></div>
<div class="m2"><p>بدست تو افتاد دیوان او</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بعمری بدش هرچه اندوخته</p></div>
<div class="m2"><p>تو اندوختیش ای پدر سوخته</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر زنده از مرگ او نام تو است</p></div>
<div class="m2"><p>حقیقت نمی میرد ای نادرست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من این راز بنهفته بودم مگر</p></div>
<div class="m2"><p>که خود فاش گردد بدست دگر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه سازم تو ناجنس نگذاشتی</p></div>
<div class="m2"><p>میان من و خود ره آشتی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو آنی و جز این نمیشایدت</p></div>
<div class="m2"><p>وزین پس تخلص «خزان » بایدت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فرومایه با مایه دیگران</p></div>
<div class="m2"><p>بسرمایه داران چه ای سر گران</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو خود دانی ای شاعر مستطاب</p></div>
<div class="m2"><p>که در زندگانی نداری کتاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو دزد کتابست عنوان تو</p></div>
<div class="m2"><p>بماتحت طبع است دیوان تو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز ما تحت طبع آنچه آید برون</p></div>
<div class="m2"><p>ز افکار تست ای خراب اندرون</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فزون است پیش من اسرار تو</p></div>
<div class="m2"><p>ز کردار ناپاک و پندار تو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در این باب بنویسم ار یک کتاب</p></div>
<div class="m2"><p>نگردم به بابی از آن کامیاب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>طبیعت نیارد بصد قرن و نسل</p></div>
<div class="m2"><p>بهاری که هر آن شود چار فصل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به نیرنگ بازی تو عالیجناب</p></div>
<div class="m2"><p>نماند آنکه رنگی نریزی به آب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من ای چاپلوس آدم باز گوش</p></div>
<div class="m2"><p>نیم چون تو هرگز عقیده فروش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بعهد قوام و بدور وثوق</p></div>
<div class="m2"><p>ز رسوائیت خسته شد کوس و بوق</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو اسباب قتل حسین لله</p></div>
<div class="m2"><p>شدی ای دمکرات پست دله</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز عشق گل روی پول قجر</p></div>
<div class="m2"><p>بدست تو عشقی شد عمرش بسر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کنون مینویسی که شد انتحار</p></div>
<div class="m2"><p>در ایران ز گفتار من پایدار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر هست در گفت من این اثر</p></div>
<div class="m2"><p>تو دیگر چه میگوئی ای بی هنر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز اشعار آزادی من تمام</p></div>
<div class="m2"><p>گرفتید سرمشق خود هرکدام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چه شد اینک از شاعران وطن</p></div>
<div class="m2"><p>همه عیب جویند ز اشعار من</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بعالم عیان گشت پستی تو</p></div>
<div class="m2"><p>درستی من نادرستی تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مرا مادر پاک زائید پاک</p></div>
<div class="m2"><p>یقین دان بپاکی روم سوی خاک</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بعمر ار چه یکروز ناسوده ام</p></div>
<div class="m2"><p>ولی چون تو دامن نیالوده ام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>من از دوستان گر جدا مانده ام</p></div>
<div class="m2"><p>بیک رنگ ثابت بجا مانده ام</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو آنی و من این ز نزدیک و دور</p></div>
<div class="m2"><p>گواهند یک ملتی مست و کور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دگر اندرین مملکت کس نماند</p></div>
<div class="m2"><p>که باید قلم پشت تا گور راند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کسیرا که در شرق و غرب است نام</p></div>
<div class="m2"><p>سر هر زبانی باعزاز تام</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چه اندیشه از چون تو بی آبرو</p></div>
<div class="m2"><p>پریشیده فکر و پراکنده گو</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دهان پاک باید قلم پاکتر</p></div>
<div class="m2"><p>کز او نام تا گور آید بدر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وجودیکه نابود بد چون سراب</p></div>
<div class="m2"><p>چه گوید بدریای پرالتهاب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تو چون موش کوری و تا گور نور</p></div>
<div class="m2"><p>سزد گر گریزد ز خور موش کور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چه خورشید تابنده شد نور پاش</p></div>
<div class="m2"><p>چو خفاش اگر عاجزی کور باش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دگر کور کورانه این ره مپوی</p></div>
<div class="m2"><p>ز تا گور هندوستانی مگوی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از این راه دیگر برای تو پول</p></div>
<div class="m2"><p>محال است یک غاز گردد وصول</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بهندوستان پول هر قدر بود</p></div>
<div class="m2"><p>ربودند پیش از تو آن را رنود</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چنین است حرفت که تا گور کیست</p></div>
<div class="m2"><p>وگر هر که باشد همان اجنبی است</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شگفت است از چون توئی اینسخن</p></div>
<div class="m2"><p>و یا غیر از این رفته در ذهن من</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تو کز اجنبی بار بسته ای</p></div>
<div class="m2"><p>چه شد اینک از اجنبی خسته ای</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تو با خویش از اجنبی بدتری</p></div>
<div class="m2"><p>چه میگوئی از اجنبی پروری</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>پرستش اگر ز اجنبی این بود</p></div>
<div class="m2"><p>مرا این پرستشگه آئین بود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به تا گور از جان و دل بنده ام</p></div>
<div class="m2"><p>من امثال او را ستاینده ام</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گر از جنس انسان از اینسان نبود</p></div>
<div class="m2"><p>هم از اصل ایکاش انسان نبود</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بچشم خردمند پوشیده نیست</p></div>
<div class="m2"><p>که فکر کسی چون تو پوسیده نیست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>نبودند از این مردم ار در بشر</p></div>
<div class="m2"><p>چه چیزی از آدم بدی غیر شر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دگر از چه آگاهیت ای حکیم</p></div>
<div class="m2"><p>نبوده است از ایران و هند قدیم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>از این پس بتردستی از این و آن</p></div>
<div class="m2"><p>چو تاریخ دزدیدی آن را بخوان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>و یا آنکه از حضرت کسروی</p></div>
<div class="m2"><p>بدانسان که کش رفته گر کش روی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>توان آگهی یابی از عهد جم</p></div>
<div class="m2"><p>چه بد حال هندوستان و عجم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>وز آن عهد تا دور ساسانیان</p></div>
<div class="m2"><p>نبدشان بجز دوستی در میان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>از آن روز تا روزگار عرب</p></div>
<div class="m2"><p>که شد بر عجم روز چون تیره شب</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>همه خاندانهای والاتبار</p></div>
<div class="m2"><p>از ایران سوی هند بستند بار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>از این خانه بیرون بخواری شدند</p></div>
<div class="m2"><p>فراری بصد سو کواری شدند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ندادند تن در ره بندگی</p></div>
<div class="m2"><p>کشیدند سر از سرافکندگی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گریزان ز ننگ اسارت شدند</p></div>
<div class="m2"><p>فراری ز قید حقارت شدند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>برستند از این خاک خائن پرست</p></div>
<div class="m2"><p>ببستند رخت و بشستند دست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>سوی خاک هندوستان تاختند</p></div>
<div class="m2"><p>در آن خاکدان خانمان ساختند</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>شدی خاک زرخیز هندوستان</p></div>
<div class="m2"><p>ز جان زر خرید وطن دوستان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>هزار و سه صد سال در آن دیار</p></div>
<div class="m2"><p>ببودند با عزت و افتخار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>مرا نیست شکی در این اعتقاد</p></div>
<div class="m2"><p>یقینا هر ایرانی پاکزاد</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>خود این تخم امید کارد بدل</p></div>
<div class="m2"><p>که هندوستان کی شود مستقل</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>تو گر بچه باز گوش ار کری</p></div>
<div class="m2"><p>خوش این پند در گوش دل بسپری</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>نهالی که جز رنجشش نیست بار</p></div>
<div class="m2"><p>از این بیش بین دو ملت مکار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>اگر سر به پیچی تو ای بازگوش</p></div>
<div class="m2"><p>ز پندم پس این پند سعدی نیوش</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>که از کودکی دارم این گفته یاد</p></div>
<div class="m2"><p>چه خوش گفت ای روح گوینده شاد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>«مزن بی تأمل بگفتار دم</p></div>
<div class="m2"><p>نکو گوی اگر دیر گوئی چه غم »</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نوشتی یک از علت اشتهار</p></div>
<div class="m2"><p>بود مرگ در صفحه روزگار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>از این رو نمیمیرد عارف چرا؟</p></div>
<div class="m2"><p>که ملت بقبرش سرود ورا</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>بخوانند، از او قدردانی کنند</p></div>
<div class="m2"><p>چو من بهر او ریزه خوانی کنند</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>از این لطف مخصوص شرمنده ام</p></div>
<div class="m2"><p>وز این مهر تا زنده ام بنده ام</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ولی نیست اینقدر هم انتظار</p></div>
<div class="m2"><p>از این مردمان فراموشکار</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>مرا یک سؤالیست بی گفت وگو</p></div>
<div class="m2"><p>تو را گر جوابی است با من بگو</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بمن از چه روی اینهمه دشمنید</p></div>
<div class="m2"><p>برای چه راضی بمرگ منید</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>سزاوار بیمهری از چیستم</p></div>
<div class="m2"><p>من ایرانیم اجنبی نیستم</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بمن از چه ئید اینهمه سرگران</p></div>
<div class="m2"><p>چه گوئیدم ای بی پدرمادران</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>گر این است اسباب بی مهریم</p></div>
<div class="m2"><p>که گفتند من شاعر ملیم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>غلط کرد هرکس که این حرف گفت</p></div>
<div class="m2"><p>مگر هر که گفت هرچه باید شنفت</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>نه ملت مرا داند از خویشتن</p></div>
<div class="m2"><p>نه بر من وطن گوید اولاد من</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>کسی بی وطن تر ز من در جهان</p></div>
<div class="m2"><p>بهر جای جوید نگیرد نشان</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>همه مهر این مادر پیر گیج</p></div>
<div class="m2"><p>بود صرف در راه اولاد بیج</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>وطن آنچنان داد پاداش من</p></div>
<div class="m2"><p>که لبسوز شد کاسه آش من</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>شدم دشمن هر که بهر وطن</p></div>
<div class="m2"><p>شد آخر وطن دشمن جان من</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>وطن استخوان مرا آب کرد</p></div>
<div class="m2"><p>بهر روز یکسوی پرتاب کرد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>مرا خسته و خوار و رنجور کرد</p></div>
<div class="m2"><p>همین بس که ام زنده در گور کرد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بدی آنچه در حق من کرد خواست</p></div>
<div class="m2"><p>ز عشق وطن چیزی از من نکاست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>وطن حاصل عمر من باد داد</p></div>
<div class="m2"><p>وطن یادم «ای داد و بیداد داد»</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>شما دیگر ای زادگان وطن</p></div>
<div class="m2"><p>چه خواهید از قالب خشک من</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>مرا ننگ از شعر و از شاعری است</p></div>
<div class="m2"><p>خود این کار پستی ز سوداگری است</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>وحید خر آخوند گند گدا</p></div>
<div class="m2"><p>عجب آنکه شاعر نداند مرا</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>چنان بغض در دل بود از منش</p></div>
<div class="m2"><p>تو گفتی که من زنش</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>اگر طبع شاعر چو طبعش گداست</p></div>
<div class="m2"><p>نه من شاعرم شعر حق شماست</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>نبودم همه عمر موقع شناس</p></div>
<div class="m2"><p>خوش آمد نگفتم خدا را سپاس</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>از این راه چیزی نیندوختم</p></div>
<div class="m2"><p>چو دیگر کسان شعر نفروختم</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>بندرت اگر شعر من گفته ام</p></div>
<div class="m2"><p>برای دل خویشتن گفته ام</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>از این کار جز زحمت و درد و سر</p></div>
<div class="m2"><p>نشد حاصلم هیچ چیز دگر</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>بایرج چه خوش گفت دکتر حسن</p></div>
<div class="m2"><p>که احسن بر آن فکر بکر حسن</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>بود گفتن شعر خود حرف مفت</p></div>
<div class="m2"><p>نباید پس از حد برون مفت گفت</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>تو از مفت عارف همی سوختی</p></div>
<div class="m2"><p>هم از پوستش پوستین دوختی</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>بلی کسب شهرت ز من گر نبود</p></div>
<div class="m2"><p>بگمنامی از این جهان رفته بود</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>بمرگ من ار چشمتان بر در است</p></div>
<div class="m2"><p>شتر پوستش پوستین خر است</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>بلی مرگ حق است و حق پایدار</p></div>
<div class="m2"><p>پس از مرگ حق میشود آشکار</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>بوقت حساب سفید و سیاه</p></div>
<div class="m2"><p>رسد مرگ بی یک قلم اشتباه</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>در آخر چو بایست ازین درگذشت</p></div>
<div class="m2"><p>کنون نیز باید ازین درگذشت</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>مرا تاکنون خود نمائی نبود</p></div>
<div class="m2"><p>حقیقت شنو خودستائی چه سود</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>طبیعت هنر داد بر من چهار</p></div>
<div class="m2"><p>که آن چار در صفحه روزگار</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>نداده است و ندهد ازین پس دگر</p></div>
<div class="m2"><p>به تنهائی آن چاربر یکنفر</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بود قرنها مام ایران عقیم</p></div>
<div class="m2"><p>ز پروردن چون منی ای ندیم</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>ولیکن زهر کوره ده ده نفر</p></div>
<div class="m2"><p>گدا طبع شاعر درآید بدر</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>که هریک وحید سخن پرورند</p></div>
<div class="m2"><p>بهار ادب را گل صد پرند</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>مبین کاینچنین سربزیر پرم</p></div>
<div class="m2"><p>در این صحنه من یکه بازیگرم</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>بموسیقیم اولین اوستاد</p></div>
<div class="m2"><p>نشاید که منکر شد این از عناد</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>مرا دید اگر فاریابی بخواب</p></div>
<div class="m2"><p>برون نامد از قریه فاریاب</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>در آوازه بیرون ز اندازه ام</p></div>
<div class="m2"><p>به پیچید در چرخ آوازه ام</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>شدی زنده سعدی خدای سخن</p></div>
<div class="m2"><p>اگر شعر خود میشنیدی ز من</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>اگر بود داود، صوتش، گره</p></div>
<div class="m2"><p>بحلقش زدی همچو حلقه زره</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>سپر پیشم از عجز انداختی</p></div>
<div class="m2"><p>ز ره باز گشتی زره ساختی</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>به نزدیک من بود چون کودکی</p></div>
<div class="m2"><p>اگر بود در دور من رودکی</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>دهان بستی و چنگ خود سوختی</p></div>
<div class="m2"><p>به نزد من آهنگ آموختی</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>خداوند و خلاق آهنگ کیست</p></div>
<div class="m2"><p>جز از من کس ار گفت جز شرک نیست</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>ز شعر ار سخن گوئی اینت جواب</p></div>
<div class="m2"><p>من و گرز و میدان افراسیاب</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>ز چوگان اندیشه گوی سخن</p></div>
<div class="m2"><p>بمیدان فکندم بیا و بزن</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>بگفتار بگذشته ات اعتبار</p></div>
<div class="m2"><p>نباشد بیا تازه داری بیار</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>بود روشن افعال و اعمال من</p></div>
<div class="m2"><p>بچندین هنر این بود حال من</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>تو بودی در این مدت ار جای من</p></div>
<div class="m2"><p>طمع بانگ میزد که ای وای من</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>ولی من چه دارم باینحال؟ هیچ</p></div>
<div class="m2"><p>تو با هیچ همه چیز داری مپیچ</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>بود رختخوابم ز حاجی وکیل</p></div>
<div class="m2"><p>که خصمش زبون با دو عمرش طویل</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>اگر پهن فرشم بایوان بود</p></div>
<div class="m2"><p>سپاسم ز الطاف کیوان بود</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>سیه روی از روی اقبالی ام</p></div>
<div class="m2"><p>که دیگ وی از مطبخ خالی ام</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>پر از شکوه وارونه در زیر طاق</p></div>
<div class="m2"><p>فتاده است دلتنگ و قهر از اجاق</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>وگر میز و گر یکدو تا صندلی است</p></div>
<div class="m2"><p>ز دکتر بدیع است از بنده نیست</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>اثاثیه عارف بی اساس</p></div>
<div class="m2"><p>سه تا سگ دو دستی است کهنه لباس</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>من این زندگانی ناپایدار</p></div>
<div class="m2"><p>بزحمت از آن کردمی اختیار</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>که از هر بداندیش بد نشنوم</p></div>
<div class="m2"><p>ز بدگوی و بدخواه راحت شوم</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>ندانستمی یکدل صاف نیست</p></div>
<div class="m2"><p>درین مملکت یکجو انصاف نیست</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>نکردم من آن راکه بایست کرد</p></div>
<div class="m2"><p>نفهمیده بودم چه بایست کرد</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>نکردم بسان همه دوستان</p></div>
<div class="m2"><p>وطن زادگان و وطن دوستان</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>وطن را از اول بهانه کنم!</p></div>
<div class="m2"><p>فراهم زر و ملک و خانه کنم</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>مپندار این را هم از بد دلی</p></div>
<div class="m2"><p>که از بهر من یک اطاق گلی</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>در این کشور پهن یغما شده</p></div>
<div class="m2"><p>نمیشد که باشد مهیا شده</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>نه، این نیست، اینقدر کوته نظر</p></div>
<div class="m2"><p>نبودم بسازم باین مختصر</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>ز بوشهر وز پهلوی تا ارس</p></div>
<div class="m2"><p>وز آنسوی تا خانقین این هوس</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>بسر بود، ایران همه سربسر</p></div>
<div class="m2"><p>بود کشور من، چه خواهم دگر</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>تن و روح وخون من ایرانی است</p></div>
<div class="m2"><p>خود این کالبد را خود او بانی است</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>اگر جان بقربان نامش کنم</p></div>
<div class="m2"><p>تن و جان هم از او بود من کیم</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>منی در میان نیست تا بهر تن</p></div>
<div class="m2"><p>بگوید فلان چیز ایران ز من</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>من این بودم اینم، شما کیستید</p></div>
<div class="m2"><p>بداندیش و بدخواهم از چیستید</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>چو با خویش بدخواه و بیگانه اید</p></div>
<div class="m2"><p>سر خویش گیرید دیوانه اید</p></div></div>