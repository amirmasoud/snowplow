---
title: >-
    شمارهٔ  ۴ - رونوشت از خط زیبای عارف
---
# شمارهٔ  ۴ - رونوشت از خط زیبای عارف

<div class="b" id="bn1"><div class="m1"><p>ز ری سوی همدان رخت بست و بار گشاد</p></div>
<div class="m2"><p>یگانه راد تقی زاده بزرگ نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبان شدم ز زبان تمام تا گویم</p></div>
<div class="m2"><p>به پیشگاه تو پیغام جمع تا افراد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش آمدی ز خوش آمد گذشته همه کس</p></div>
<div class="m2"><p>بسان موکب گل مقدمت مبارکباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهار بهر پذیرائی تو گسترده است</p></div>
<div class="m2"><p>بساط سبزه ز اردیبهشت تا خرداد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گلبنان دبستان دانش همچون گل</p></div>
<div class="m2"><p>شکفته گشتی رویت شکفته روحت شاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان ملت ایران و آمریکا کرد</p></div>
<div class="m2"><p>خود این ورود تو روح یگانگی ایجاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قوی بماند آن دولت قوی بنیان</p></div>
<div class="m2"><p>جوان زید ز نو این ملت کهن بنیاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز پرتگاه سیه روز شام گمراهی</p></div>
<div class="m2"><p>ز نور صبح معارف بخواه استمداد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بروزگار نژادی که داد دانش داد</p></div>
<div class="m2"><p>چه شد که گشت ز بی دانشی نژند نژاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بوم شوم از آن مرز و بوم خیزد جهل</p></div>
<div class="m2"><p>همای دانش در کشور یکه تخم نهاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسرنگونی ضحاک جهل چیره شود</p></div>
<div class="m2"><p>همیشه علم از این پس چو کاوه حداد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خراب کشور ایران ز دست مدرسه گشت</p></div>
<div class="m2"><p>مگر دوباره ز دارالفنون شود آباد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برای صرف معارف محل بجز اوقاف</p></div>
<div class="m2"><p>نبود و نیست من این گفتم هر چه باداباد</p></div></div>