---
title: >-
    شمارهٔ ۱۶ - چه شورها
---
# شمارهٔ ۱۶ - چه شورها

<div class="n" id="bn1"><p>بنا به‌ گفتهٔ عارف قزوینی در دیوانش، چه شورها در استانبول و پس از «معلوم شدن خیالات ترک‌ها نسبت به آذربایجان» تصنیف شده است (اواخر سال ۱۳۳۶ ه.ق).</p></div>
<div class="b" id="bn2"><div class="m1"><p>چه شورها که من به پا، ز شاهناز می کنم</p></div>
<div class="m2"><p>در شکایت از جهان، به شاه باز می کنم</p></div></div>
<div class="b2" id="bn3"><p>جهان پر از غم دل از (جهان پر از غم دل از) زبان ساز می کنم (می کنم)</p></div>
<div class="b" id="bn4"><div class="m1"><p>ز من مپرس چونی، دلی چو کاسۀ خونی</p></div>
<div class="m2"><p>ز اشک پرس که افشا نمود راز درونی</p></div></div>
<div class="b2" id="bn5"><p>نمود راز درونی، نمود راز درونی، نمود راز درونی</p></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه جا ازین سفر، بدون دردسر</p></div>
<div class="m2"><p>اگر به در برم من، به شه خبر برم من</p></div></div>
<div class="b2" id="bn7"><p>چه پرده های نیرنگ ز شان، به بارگاه شه درم من (ز شان به بارگاه شه درم من)</p></div>
<div class="b" id="bn8"><div class="m1"><p>حکومت موقتی چه کرد به که نشنوی</p></div>
<div class="m2"><p>گشوده شد در سرای جم به روی اجنبی</p></div></div>
<div class="b2" id="bn9"><p>به باد رفت خاک و کاخ (به باد رفت خاک و کاخ) و بارگاه خسروی (کاخ خسروی)</p></div>
<div class="b" id="bn10"><div class="m1"><p>سکون ز بیستون شد، چو قصر کن فیکون شد</p></div>
<div class="m2"><p>صدای شیون شیرین، به چرخ بوقلمون شد</p></div></div>
<div class="b2" id="bn11"><p>(به چرخ بوقلمون شد، به چرخ بوقلمون شد، به چرخ بوقلمون شد)</p></div>
<div class="b" id="bn12"><div class="m1"><p>شه زنان، به سرزنان و موکنان</p></div>
<div class="m2"><p>به گریه گفت: کو سران ایران، دلاوران ایران؟</p></div></div>
<div class="b2" id="bn13"><p>چه شد که یک نفرمرد، نماند از بهادران ایران (نماند از بهادران ایران)</p></div>
<div class="b" id="bn14"><div class="m1"><p>کجاست کیقباد و جم، خجسته اردشیر کو؟</p></div>
<div class="m2"><p>شهان تاج بخش و خسروان باجگیر کو؟</p></div></div>
<div class="b2" id="bn15"><p>کجاست گیو پهلوان (کجاست گیو پهلوان)</p>
<p>و رستم دلیر کو (رستم دلیر کو)؟</p></div>
<div class="b" id="bn16"><div class="m1"><p>ز ترک این عجب نیست، چه اهل نام و نسب نیست</p></div>
<div class="m2"><p>قدم به خانۀ کیخسرو، این ز شرط ادب نیست (این ز شرط ادب نیست)</p></div></div>
<div class="b2" id="bn17"><p>(این ز شرط ادب نیست)</p></div>
<div class="b" id="bn18"><div class="m1"><p>ز آه و تف، اگر چه کف، زنی چون دف، بزن به سر، که این چه بازی است؟</p></div>
<div class="m2"><p>که دور ترک بازی است</p></div></div>
<div class="b2" id="bn19"><p>برای ترک سازی عجب زمینه سازی است(عجب زمینه سازی است)</p></div>
<div class="b" id="bn20"><div class="m1"><p>زبان ترک از برای از قفا کشدن است</p></div>
<div class="m2"><p>صلاح پای این زبان ز ممکلت بریدن است</p></div></div>
<div class="b2" id="bn21"><p>دو اسبه با زبان فارسی (دو اسبه با زبان فارسی)</p>
<p>از ارس پریدن است (خدا جهیدن است)</p></div>
<div class="b" id="bn22"><div class="m1"><p>نسیم صبحدم خیز، بگو به مردم تبریز</p></div>
<div class="m2"><p>که نیست خلوت زرتشت، جای صحبت چنگیز (جای صحبت چنگیز)</p></div></div>
<div class="b2" id="bn23"><p>زبانتان شد از میان، به گوشه ای نهان</p></div>
<div class="b" id="bn24"><div class="m1"><p>سیاه پوش و خاموش، ز ماتم سیاووش</p></div>
<div class="m2"><p>گر از نژاد اوئید، نکرد باید این دو را فراموش (نکرد باید این دو را فراموش)</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مگو سران فرقه، جمعی ارقه، مشتی حقه باز</p></div>
<div class="m2"><p>وکیل و شیخ و مفتی، مدرس است و اهل آز</p></div></div>
<div class="b2" id="bn26"><p>بدین سیاست آب رفته (بدین سیاست آب رفته)</p>
<p>کی شود به جوی باز (خدا به جوی باز)</p></div>
<div class="b" id="bn27"><div class="m1"><p>ز حربۀ تدین خراب مملکت از بن</p></div>
<div class="m2"><p>نشسته مجلس شوری به ختم مرگ تمدن (به ختم مرگ تمدن) (به ختم مرگ تمدن)</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه زین بتر ز بام و در به هر گذر</p></div>
<div class="m2"><p>گرفته سر به سر خریت، زمام اکثریت</p></div></div>
<div class="b2" id="bn29"><p>گر این بود مساوات</p>
<p>دوباره زنده باد بربریت (دوباره زنده باد بربریت)</p></div>
<div class="b" id="bn30"><div class="m1"><p>به غیر باده زادۀ حلال کسی نشان نداند</p></div>
<div class="m2"><p>از این حرام زادگان یکی خوش امتحان ندارد</p></div></div>
<div class="b2" id="bn31"><p>«رسول زاده» ری به ترک (رسول زاده، ری به ترک)</p>
<p>از چه رایگان نداد (رایگان نداد)</p></div>
<div class="b" id="bn32"><div class="m1"><p>گذاشت و بهره برداشت هر آنچه هیزم تر داشت</p></div>
<div class="m2"><p>به جز زیان ثمر از این «اجاق ترک» چه برداشت ؟</p></div></div>
<div class="b2" id="bn33"><p>با خود این چه ثمر داشت (با خود این چه ثمر داشت)</p></div>
<div class="b" id="bn34"><div class="m1"><p>به غیر اشک و دود، هر آنچه هست و بود</p></div>
<div class="m2"><p>یا نبود بی اثر ماند، ز سود ها ضرر ماند</p></div></div>
<div class="b2" id="bn35"><p>برای آنچه باقی است، ببین هزار ها خطر ماند (ببین هزار ها خطر ماند)</p></div>