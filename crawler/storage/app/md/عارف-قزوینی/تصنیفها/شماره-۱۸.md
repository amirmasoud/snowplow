---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>شانه بر زلف پریشان زده‌ای به به به</p></div>
<div class="m2"><p>دست بر منظرهٔ جان زده‌ای به به به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتاب از چه طرف سر زده‌ای امروز که سر</p></div>
<div class="m2"><p>به من بی‌سر و سامان زده‌ای به به به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صف دل‌ها همه بر هم زده‌ای ماشاءاله</p></div>
<div class="m2"><p>تا به هم آن صف مژگان زده‌ای به به به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبح از دست تو پیراهن طاقت زده چاک</p></div>
<div class="m2"><p>تا سر از چاک گریبان زده‌ای به به به</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من خراباتیم از چشم تو پیداست که دی</p></div>
<div class="m2"><p>باده در خلوت رندان زده‌ای به به به</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو بدین چشم گر عابد بفریبی چه عجب</p></div>
<div class="m2"><p>گول صد مرتبه شیطان زده‌ای به به به</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن یک‌لایی من بازوی تو سیلی عشق</p></div>
<div class="m2"><p>تو مگر رستم دستان زده‌ای به به به</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود پیدا ز تک و پوی رقیب این که تواَش</p></div>
<div class="m2"><p>همچو سگ سنگ به دندان زده‌ای به به به</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عارف این گونه سخن از دگران ممکن نیست</p></div>
<div class="m2"><p>دست بالاتر از امکان زده‌ای به به به</p></div></div>