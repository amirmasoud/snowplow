---
title: >-
    شمارهٔ ۳۲ - باد خزانی
---
# شمارهٔ ۳۲ - باد خزانی

<div class="n" id="bn1"><p>تاریخ دقیق خلق این تصنیف معلوم نیست اما به عنوان آخرین تصنیف در دیوان عارف آمده و او آن را طبق گفتهٔ خودش در اسفند ماه ۱۳۰۳ ه.ش در تبریز «به یاد دلاوران آذربایجانی» خوانده است.</p></div>
<div class="b" id="bn2"><div class="m1"><p>باد خزانی، زد ناگهانی، کرد آنچه دانی</p></div>
<div class="m2"><p>برهم زد ایّام نشاط و روزگار کامرانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظلم خزان کرد، با گلستان کرد، دانی چه سان کرد؟</p></div>
<div class="m2"><p>آنسان که من کردم،به دور زندگی با زندگانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو من فراری، بلبل به خواری، با سوگواری</p></div>
<div class="m2"><p>گل از نظرها محو شد، همچون خیالات جوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار گلزار، زار شد زار، شد پدیدار</p></div>
<div class="m2"><p>دیو دی، یا خود بلای آسمانی</p></div></div>
<div class="b2" id="bn6"><p>***</p></div>
<div class="b" id="bn7"><div class="m1"><p>از لشکر دی، شد عمر گل طی</p></div>
<div class="m2"><p>آمد دمادم، طیارۀ ابر، از آسمان هر سو پیاپی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود کرد مستور، چون فارسی نور، جا کرد با زور</p></div>
<div class="m2"><p>دی چون زبان ترک اندر مغز آذربایجانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آرام جان باش، شیرین بیان باش، سعدی زبان باش</p></div>
<div class="m2"><p>در خاک فردوسی طوسی، توسن ترک از چه رانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راه جان پوی، فارسی گوی، دست دل شوی</p></div>
<div class="m2"><p>زود از این الفاظ زشت بی معانی</p></div></div>
<div class="b2" id="bn11"><p>***</p></div>
<div class="b" id="bn12"><div class="m1"><p>کوه و در و بر، از برف یکسر، چون وضع کشور</p></div>
<div class="m2"><p>شد در فشار روح، آزادی کش...</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دور گلستان، جون در ساسان، رفت از زمستان</p></div>
<div class="m2"><p>شد جانشین دور ساسان، همچو دور ترکمانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای باد نوروز بشتاب امروز، با فتح و فیروز</p></div>
<div class="m2"><p>رو مژده آر از فرّ فروردین، بستان مژدگانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گو بهارا، خود بیارا، تا که ما را</p></div>
<div class="m2"><p>از کف ایام جان فرسا رهانی</p></div></div>
<div class="b2" id="bn16"><p>***</p></div>
<div class="b" id="bn17"><div class="m1"><p>ای ابر آذار، طرف چمنزار، بگری چو من زار</p></div>
<div class="m2"><p>چون آتش زردشت پر کن لاله در اطراف گلزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای یار اکنون، زن خیمه بیرون، همچون فریدون</p></div>
<div class="m2"><p>در پرچم گلبن ببین، نقش درفش کاویانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا عید جمشید، تا شیر و خورشید باقی است، امید</p></div>
<div class="m2"><p>دارم به ایران جان و آذربایجانا خود تو جانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جای انکار، اندرین کار، نیست ای یار</p></div>
<div class="m2"><p>آنکه فرق خادم و خائم ندانی</p></div></div>