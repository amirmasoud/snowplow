---
title: >-
    شمارهٔ ۱۱ - باد فرح‌بخش بهاری
---
# شمارهٔ ۱۱ - باد فرح‌بخش بهاری

<div class="b" id="bn1"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>
<div class="n" id="bn2"><p>عارف در دیوانش می‌نویسد که این تصنیف را پنج-شش ماه پس از «تصنیف شوستر» (که آن را زمستان سال ۱۳۲۹ ه.ق سروده) با «یک حالت یأس و ناامیدی» ساخته است. </p></div>
<div class="b" id="bn3"><div class="m1"><p>باد فرح بخش بهاری وزید</p></div>
<div class="m2"><p>پیرهن عصمت گل بردرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نالۀ جان سوز ز مرغ قفس</p></div>
<div class="m2"><p>تا به گلستان رسید (تا به گلستان رسید)</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قهقهۀ کبک دری</p></div>
<div class="m2"><p>بود چو از خودسری</p></div></div>
<div class="b2" id="bn6"><p>پنجۀ شاهین چرخ</p></div>
<div class="b" id="bn7"><div class="m1"><p>بی درنگ</p></div>
<div class="m2"><p>زد به چنگ</p></div></div>
<div class="b2" id="bn8"><p>رشتۀ عمرش برید</p></div>
<div class="b" id="bn9"><div class="m1"><p>تا به قفس اندرم</p></div>
<div class="m2"><p>ریخته یکسر برم</p></div></div>
<div class="b2" id="bn10"><p>بایدم از سر گذشت</p>
<p>شاید از این در پرید</p></div>
<div class="b" id="bn11"><div class="m1"><p>کشمکش و گیر و دار اگر گذارد</p></div>
<div class="m2"><p>کج روی روزگار اگر گذارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پای گل از باده تر کنم دماغی</p></div>
<div class="m2"><p>نیش جگر خوار خار اگر گذارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این دل بی اختیار اگر گذارد</p></div>
<div class="m2"><p>گوشه کنم اختیار اگر گذارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز آه دل آتش زنم به عمر بدخواه</p></div>
<div class="m2"><p>دیدۀ خونابه بار اگر گذارد</p></div></div>