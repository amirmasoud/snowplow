---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>بماندیم ما، مستقل شد ارمنستان</p></div>
<div class="m2"><p>(ارمنستان ارمنستان شد ارمنستان)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبردست شد، زیردست زیردستان</p></div>
<div class="m2"><p>(دستان زیردستان زیردستان)</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ملک جم شد خراب، گو به ساقی</p></div>
<div class="m2"><p>(گو به ساقی تو باش باقی تو باش باقی)</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبوحی بده زان شراب شب به مستان</p></div>
<div class="m2"><p>(بده به مستان، بده به مستان)</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس است ما را هوای بستان</p></div>
<div class="m2"><p>که گل دو روز است در گلستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بده می که دنیا دو روز بیشتر نیست</p></div>
<div class="m2"><p>مخور غم که ایران ز ما خرابتر نیست</p></div></div>
<div class="b2" id="bn7"><p>بد آن ملتی کز خرابیش خبر نیست</p>
<p>(جانم خراب نیست)</p></div>
<div class="b" id="bn8"><div class="m1"><p>آه که گر آه پر بگیرد</p></div>
<div class="m2"><p>دامن هر خشک و تر بگیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی خبران را خبر رسانید</p></div>
<div class="m2"><p>ز شان بر ما خبر بگیرد</p></div></div>
<div class="b2" id="bn10"><p>**********</p></div>
<div class="b" id="bn11"><div class="m1"><p>ز دارالفنون به جز جنون نداریم</p></div>
<div class="m2"><p>معارف نه، مالیه نی، قشون نداریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برفت حس ملت آن چنان که گوئی</p></div>
<div class="m2"><p>به تن جان به جان رگ به رگ خون نداریم</p></div></div>
<div class="b2" id="bn13"><p>به غیر عشق جنون نداریم</p></div>
<div class="b" id="bn14"><div class="m1"><p>چه خون توان خورد که خون نداریم</p></div>
<div class="m2"><p>نداریم اگر هیچ، هیچ غم نداریم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز اسباب بدبختی هیچ کم نداریم</p></div>
<div class="m2"><p>وجودی که باشد به از عدم نداریم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پند پدر گر پسر بگیرد</p></div>
<div class="m2"><p>دامن فضل و هنر بگیرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ما ز نیاکان نشان چه داریم؟</p></div>
<div class="m2"><p>تا که ز ما آن دگر بگیرد</p></div></div>