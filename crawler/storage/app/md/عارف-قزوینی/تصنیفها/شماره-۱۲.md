---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>بلبل شوریده فغان می کند</p></div>
<div class="m2"><p>شکوه ز آشوب جهان می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن گل گشته ز دستش رها</p></div>
<div class="m2"><p>ناله و فریاد و امان می کند</p></div></div>
<div class="b2" id="bn3"><p>***</p></div>
<div class="b" id="bn4"><div class="m1"><p>باد خزان پیرهن گل درید</p></div>
<div class="m2"><p>دامن گل شد ز نظر ناپدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو چو یعقوب از این غم خمید</p></div>
<div class="m2"><p>غصه قد سرو، کمان می کند</p></div></div>
<div class="b2" id="bn6"><p>***</p></div>
<div class="b" id="bn7"><div class="m1"><p>خارجه در مجلس ما جا گرفت</p></div>
<div class="m2"><p>نرگس شهلا ره ایما گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لاله ازین داغ به دل جا گرفت</p></div>
<div class="m2"><p>عاقبت این هیز زیان می کند</p></div></div>
<div class="b2" id="bn9"><p>***</p></div>
<div class="b" id="bn10"><div class="m1"><p>شد «پرتیوا» پی غارتگری</p></div>
<div class="m2"><p>ریخته دزدان عوض مشتری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه گل به جا ماند و نه باغی</p></div>
<div class="m2"><p>هر یک ز سو به سراغی</p></div></div>
<div class="b2" id="bn12"><p>***</p></div>
<div class="b" id="bn13"><div class="m1"><p>دزد ز هر سوی به غارتگری</p></div>
<div class="m2"><p>خیره‌سری بین که چه‌ها می‌کند</p></div></div>