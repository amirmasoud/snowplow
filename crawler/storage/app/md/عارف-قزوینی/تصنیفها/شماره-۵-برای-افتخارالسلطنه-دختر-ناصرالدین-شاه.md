---
title: >-
    شمارهٔ ۵ - برای افتخارالسلطنه - دختر ناصرالدین شاه
---
# شمارهٔ ۵ - برای افتخارالسلطنه - دختر ناصرالدین شاه

<div class="b" id="bn1"><div class="m1"><p>افتخار همه آفاقی و منظور منی</p></div>
<div class="m2"><p>شمع جمع همه عشاق به هر انجمنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سر زلف پریشان تو دلهای پریش</p></div>
<div class="m2"><p>همه خو کرده چو عارف به پریشان وطنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چه رو شیشۀ دل می شکنی؟</p></div>
<div class="m2"><p>تیشه بر ریشۀ جان از چه زنی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیم اندام ولی سنگ دلی</p></div>
<div class="m2"><p>سست پیمانی و پیمان شکنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر درد من به درمان رسد چه میشه؟</p></div>
<div class="m2"><p>شب هجر اگر به پایان رسد چه میشه؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر بار دل به منزل رسد چه گردد؟</p></div>
<div class="m2"><p>سر من اگر به سامان رسد چه میشه؟َ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر من اگر به سامان رسد چه میشه؟َ</p></div>
<div class="m2"><p>گر عارف «نظام السلطان» شود چه میشه؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز غمت خون می گریم بنگر چون می گریم</p></div>
<div class="m2"><p>ز مژه دل می ریزد ز جگر خون می آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افتخار دل و جان می آید</p></div>
<div class="m2"><p>یار بی پرده عیان می آید</p></div></div>
<div class="b2" id="bn10"><p>***</p></div>
<div class="b" id="bn11"><div class="m1"><p>تو اگر عشوه بر خسروپرویز کنی</p></div>
<div class="m2"><p>همچو فرهاد رود در عقب کوه کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>متفرق نشود مجمع دلهای پریش</p></div>
<div class="m2"><p>تو اگر شانه بر آن زلف پریشان نزنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز چه رو شیشۀ دل می شکنی؟</p></div>
<div class="m2"><p>تیشه بر ریشۀ جان از چه زنی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیم اندام ولی سنگ دلی</p></div>
<div class="m2"><p>سست پیمانی و پیمان شکنی (سست پیمانی و پیمان شکنی)</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به چشمت که دیده از صورتت نگیرم</p></div>
<div class="m2"><p>اگر می کشی و گر می زنی به تیرم</p></div></div>
<div class="b2" id="bn16"><p>تو سلطان حسن و من کمترین فقیرم</p></div>
<div class="b" id="bn17"><div class="m1"><p>گزندم اگر ز سلطان رسد چه میشه؟</p></div>
<div class="m2"><p>گزندم اگر ز سلطان رسد چه میشه؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز غمت خون می گریم</p></div>
<div class="m2"><p>بنگر چون می گریم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز مژه دل می ریزد</p></div>
<div class="m2"><p>ز جگر خون می آید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به سر کشتۀ جان می آید</p></div>
<div class="m2"><p>خون صد سلسله جان می ریزد</p></div></div>