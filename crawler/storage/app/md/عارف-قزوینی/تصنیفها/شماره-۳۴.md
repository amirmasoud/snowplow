---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>بهار نو رسید</p></div>
<div class="m2"><p>گل ار بستان دمید</p></div></div>
<div class="b2" id="bn2"><p>ای گلعذار! نه وقت خواب است</p></div>
<div class="b" id="bn3"><div class="m1"><p>ای رویت صبح عید</p></div>
<div class="m2"><p>در این عید سعید</p></div></div>
<div class="b2" id="bn4"><p>باده حلال، بوسه ثواب است</p></div>
<div class="b" id="bn5"><div class="m1"><p>خرابم کن ز می</p></div>
<div class="m2"><p>ز بانگ چنگ و نی</p></div></div>
<div class="b2" id="bn6"><p>که ملک جم یکسر خراب است</p></div>
<div class="b" id="bn7"><div class="m1"><p>برای انتخاب</p></div>
<div class="m2"><p>در این ملک خراب</p></div></div>
<div class="b2" id="bn8"><p>وطن فروش مشغول کار است</p></div>
<div class="b" id="bn9"><div class="m1"><p>وکیلان را بگو</p></div>
<div class="m2"><p>بس است این تکاپو</p></div></div>
<div class="b2" id="bn10"><p>دور از بهشت شیطان و مار است</p></div>
<div class="b" id="bn11"><div class="m1"><p>ما و عشق رخ دوست</p></div>
<div class="m2"><p>قبلۀ ما، ابروی اوست</p></div></div>
<div class="b2" id="bn12"><p>ما ندهیم دل خود، جز به یار</p>
<p>ما نکنیم اعتماد بر اغیار</p></div>
<div class="b" id="bn13"><div class="m1"><p>مطرب مجلس بکش این نغمه را</p></div>
<div class="m2"><p>از پرده بیرون</p></div></div>
<div class="b2" id="bn14"><p>ساقی مهوش بده جامی از آن</p>
<p>بادۀ گلگون</p></div>
<div class="b" id="bn15"><div class="m1"><p>ای وطن من</p></div>
<div class="m2"><p>تو لیلای منی</p></div></div>
<div class="b2" id="bn16"><p>من بر تو مجنون</p></div>
<div class="b" id="bn17"><div class="m1"><p>پاینده بادا درفش کاویان</p></div>
<div class="m2"><p>تیغ فریدون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای دل غافل</p></div>
<div class="m2"><p>بر احوال وطن</p></div></div>
<div class="b2" id="bn19"><p>خون گریه کن خون</p></div>
<div class="b" id="bn20"><div class="m1"><p>من با تو مایل</p></div>
<div class="m2"><p>بر احوال وطن</p></div></div>
<div class="b2" id="bn21"><p>خون گریه کن خون</p></div>