---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>رحم ای خدای دادگر کردی نکردی</p></div>
<div class="m2"><p>ابقا به فرزند بشر کردی نکردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ما در خشم و غضب بستی نبستی</p></div>
<div class="m2"><p>جز قهر اگر کار دگر کردی نکردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طاعون،وبا،قحطی، بگو دنیا بگیرد</p></div>
<div class="m2"><p>یک مشت جو گر بارور کردی نکردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش گرفت عالم ز گور بوالبشر بود</p></div>
<div class="m2"><p>صرف نظر گر زین پدر کردی نکردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیتی و هرچه اندر، ز خشک و تر بسوزان</p></div>
<div class="m2"><p>شفقت اگر با خشک و تر کردی نکردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک دفعه عالم بی خبر زیر و زبر کن</p></div>
<div class="m2"><p>جنبنده ای را گر خبر کردی نکردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این راه خیری بد نهادم پیش پایت</p></div>
<div class="m2"><p>با جبرئیل ار خیر و شر کردی نکردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این اشرف مخلوق زشت و بی شرف را</p></div>
<div class="m2"><p>با جنس سگ همسر اگر کردی نکردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملک کیانی را قجر چون دست خوش کرد</p></div>
<div class="m2"><p>کوتاه اگر دست قجر کردی نکردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایران هنرور را به ذلت اندر آرد</p></div>
<div class="m2"><p>عارف اگر کسب هنر کردی نکردی</p></div></div>