---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>خون چو سرچشمۀ آب حیات است</p></div>
<div class="m2"><p>پیش خون نقش هر رنگ مات است</p></div></div>
<div class="b2" id="bn2"><p>خون مدیر حیات و ممات است</p>
<p>خون فقط خضر راه نجات است</p></div>
<div class="b" id="bn3"><div class="m1"><p>رنگ خون رنگ میمون مینوست</p></div>
<div class="m2"><p>دشت بی لاله دیدن نه نیکوست</p></div></div>
<div class="b2" id="bn4"><p>گل به دربار خون تهینت گوست</p>
<p>قوۀ مجریه کاینات است</p></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو خون، گر شبیخون، آورد چون لاله گلگون</p></div>
<div class="m2"><p>سازد از خون، شهر و بیرون، دشت و هامون (دشت و هامون)</p></div></div>
<div class="b2" id="bn6"><p>گر از این دل خود سر خود خون نریزم</p>
<p>همه خون خودم از مژه بیرون نریزم</p></div>
<div class="b" id="bn7"><div class="m1"><p>گل اگر شبنم از خون بگیرد</p></div>
<div class="m2"><p>از سموم خزانی نمیرد</p></div></div>
<div class="b2" id="bn8"><p>تا ابد رنگ هستی بپذیرد</p>
<p>خون نگهدار ذات و صفات است</p></div>
<div class="b" id="bn9"><div class="m1"><p>شیر اگر خون نکرده حرامت</p></div>
<div class="m2"><p>ای پسر شیرپستان مامت</p></div></div>
<div class="b2" id="bn10"><p>زنده با نقش خون باد نامت</p>
<p>نقش این زندگی را ثبات است</p></div>
<div class="b" id="bn11"><div class="m1"><p>خون چو در یک ملتی نیست،کیست یا چیست؟</p></div>
<div class="m2"><p>نیست باد آن ملتی کز هستی غیر کند زیست،</p></div></div>
<div class="b2" id="bn12"><p>زانکه فانی است</p>
<p>چه خوش آنکه ز خون آسیا بگردد</p></div>
<div class="b" id="bn13"><div class="m1"><p>شهر خون،قریه خون، رهگذر خون</p></div>
<div class="m2"><p>کوه خون، دره خون، بحر و بر خون</p></div></div>
<div class="b2" id="bn14"><p>دشت و هامون ز خون سر به سر خون</p>
<p>رود خون، چشمه خون تا قنات است</p></div>
<div class="b" id="bn15"><div class="m1"><p>خون به خون ریختن باید انگیخت</p></div>
<div class="m2"><p>خون فاسد ز هر فاسدی ریخت</p></div></div>
<div class="b2" id="bn16"><p>کاین کهن پی بنا بی ثبات است</p></div>
<div class="b" id="bn17"><div class="m1"><p>ای هواخوان خونخواه،آه، صد آه</p></div>
<div class="m2"><p>تیره چون آه دل مظلوم باید،</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صبح بداندیش و بدخواه</p></div>
<div class="m2"><p>چون زمام به دست معاندین دون است</p></div></div>
<div class="b2" id="bn19"><p>ره چارۀ ما همگی به دست خون است</p></div>
<div class="b" id="bn20"><div class="m1"><p>تا شده ننگ نام نیاکان</p></div>
<div class="m2"><p>جز به خون نشستن این ننگ نتوان</p></div></div>
<div class="b2" id="bn21"><p>مشکل از هرجهت کار ایران</p>
<p>خون خود حلال این مشکلات است</p></div>
<div class="b" id="bn22"><div class="m1"><p>صد فلاطون ز ماهیت خون</p></div>
<div class="m2"><p>خورده خون، سر نیاورده بیرون</p></div></div>
<div class="b2" id="bn23"><p>داندش خداوند بی چون</p>
<p>کافرینندۀ حسن ذات است</p></div>
<div class="b" id="bn24"><div class="m1"><p>عارف ار بدنام گردد، چون تو نامش</p></div>
<div class="m2"><p>آنچه خون در زندگانی،حرامش</p></div></div>
<div class="b2" id="bn25"><p>دل غرقه به خون شد یار غار عارف</p>
<p>نه قرار دل وی و نی قرار عارف</p></div>