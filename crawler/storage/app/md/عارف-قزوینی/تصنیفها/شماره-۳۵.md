---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>بهار دلکش رسید و دل به جا نباشد</p></div>
<div class="m2"><p>از اینکه دلبر دمی به فکر ما نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در این بهار ای صنم بیا و آشتی کن</p></div>
<div class="m2"><p>که جنگ و کین با من حزین روا نباشد</p></div></div>
<div class="b2" id="bn3"><p>صبحدم بلبل، بر درخت گل، به خنده می‌گفت:</p>
<p>مه جبینان را، نازنینان را، وفا نباشد</p></div>
<div class="b" id="bn4"><div class="m1"><p>اگر تو با این دل حزین عهد بستی</p></div>
<div class="m2"><p>حبیب من با رقیب من، چرا نشستی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا عزیزم دل مرا از کینه خستی؟</p></div>
<div class="m2"><p>بیا برم شبی از وفا ای مه الستی</p></div></div>
<div class="b2" id="bn6"><p>تازه کن عهدی که با ما بستی</p></div>
<div class="b" id="bn7"><div class="m1"><p>به باغ رفتم دمی به گل نظاره کردم</p></div>
<div class="m2"><p>چو غنچه پیراهن از غم تو پاره کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روا نباشد اگر ز من کناره جوئی</p></div>
<div class="m2"><p>که من ز بهر تو از جهان کناره کردم</p></div></div>
<div class="b2" id="bn9"><p>ای پری پیکر، سرو سیمین بر، لعبت بهاری</p>
<p>مهوشی جانا، دلکشی اما، وفا نداری</p></div>
<div class="b" id="bn10"><div class="m1"><p>به باغ رفتم چو عارضت گلی ندیدم</p></div>
<div class="m2"><p>ز گلشنت از مراد دل گلی نچیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به خاک کوی تو لاجرم وطن گزیدم</p></div>
<div class="m2"><p>ببین در وطن از رفیقانت</p></div></div>
<div class="b2" id="bn12"><p>وز رقیبانت در وطن خواهی چه ها کشیدم</p></div>
<div class="b" id="bn13"><div class="m1"><p>ز جشن جمشید جم دلی نمانده خرم</p></div>
<div class="m2"><p>از آن که اهرمن را مکان بود به کشور جم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به پادشاه عجم بده ز باده جامی</p></div>
<div class="m2"><p>مگر که پادشه عجم ز دل برد غم</p></div></div>
<div class="b2" id="bn15"><p>خسرو ایران، باد جاویدان، به تخت شاهی</p>
<p>دشمنش بی جان، ملکش آبادان، چنانکه خواهی</p></div>
<div class="b" id="bn16"><div class="m1"><p>ز جنگ بین الملل مرا خبر نباشد</p></div>
<div class="m2"><p>ز بارش تیر آهنین حذر نباشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا به غیر از غمت غم دگر نباشد</p></div>
<div class="m2"><p>تو شاه منی، با ولای تو، با صفای تو</p></div></div>
<div class="b2" id="bn18"><p>از رقیبانم حذر نباشد</p></div>