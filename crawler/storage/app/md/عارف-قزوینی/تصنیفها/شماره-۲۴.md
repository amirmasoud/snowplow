---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>از خراسان در اعصار</p></div>
<div class="m2"><p>شد دو نار پدیدار</p></div></div>
<div class="b2" id="bn2"><p>هر دو با روح سرشار</p></div>
<div class="b" id="bn3"><div class="m1"><p>روح آن شاد، زنده این باد</p></div>
<div class="m2"><p>روح آن شاد، زنده این باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما از امرش</p></div>
<div class="m2"><p>تا که جانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست باقی</p></div>
<div class="m2"><p>سرنتابیم</p></div></div>
<div class="b2" id="bn6"><p>تا دمار از روزگار دشمنان دون، برآریم</p></div>