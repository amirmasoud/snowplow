---
title: >-
    بخش ۱۱۵ - بیان فراق رام
---
# بخش ۱۱۵ - بیان فراق رام

<div class="b" id="bn1"><div class="m1"><p>چو رام غافل از سیتا جدا ماند</p></div>
<div class="m2"><p>به پشت پای خود دولت ز در راند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطا کرد و خطا کرد و خطا کرد</p></div>
<div class="m2"><p>که بر معشوق خود عاشق جفا کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کفر عشق شد منسوب عاشق</p></div>
<div class="m2"><p>خجل از کرده خود چون منافق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو کاری کرد چون ناکرد کاران</p></div>
<div class="m2"><p>خزان آورد بر گل نو بهاران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پشیمان شد ز آزار آخر کار</p></div>
<div class="m2"><p>ز آزارش دو چندان گشت آزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دل بر کند بیخ شادمانی</p></div>
<div class="m2"><p>چو هجران گشت خصم زندگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به راحت خوش نخورده یکدم آب</p></div>
<div class="m2"><p>به کام دل نکرده یک مژه خواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هجر یار کرده عیش پدرود</p></div>
<div class="m2"><p>ز غم در دل شبی اندیشه بنمود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که ضایع گشت عمر نازنینم</p></div>
<div class="m2"><p>به جای مه بت دیگر گزینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دگر معشوقۀ خویش ب ایدم خواست</p></div>
<div class="m2"><p>که تنهایی مسلم مر خدا راست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر مه پاره سیتا رفت از دست</p></div>
<div class="m2"><p>مرا امکان خورشید دگر هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دگر ره کرد در دل فکر معقول</p></div>
<div class="m2"><p>بیندیشید از معقول منقول</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کزین اندیشه جانم را غرض چیست</p></div>
<div class="m2"><p>پری جان بود مر جان را عوض کیست؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو بر سیتا اگر گیرم دگر زن</p></div>
<div class="m2"><p>زمین و آسمان خندند بر من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر بی روی او بینم به گلزار</p></div>
<div class="m2"><p>مژه در دیده من بشکند خار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو از کوثر بدل خواهم شرابی</p></div>
<div class="m2"><p>نشانم ذره جای آفتابی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>براقی پی کنم در راهواری</p></div>
<div class="m2"><p>کنم زان پس به میدان خرسواری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه افزایم به دل آزار دیگر</p></div>
<div class="m2"><p>بباید کرد فکر کار دیگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنونم آنچه در راه صوابست</p></div>
<div class="m2"><p>همین اسمید جگ کار ثوابست</p></div></div>