---
title: >-
    بخش ۹۵ - بیدار کردن کنب کرن برادر خود را از خواب برای جنگ رام و بیدار شدن کنب کرن
---
# بخش ۹۵ - بیدار کردن کنب کرن برادر خود را از خواب برای جنگ رام و بیدار شدن کنب کرن

<div class="b" id="bn1"><div class="m1"><p>کمر بشکست راون را ز اندوه</p></div>
<div class="m2"><p>که چون آورد هنونت آنچنان کوه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دل گفتا که وقت کنب کرن است</p></div>
<div class="m2"><p>که این ساعت گران بر ما چو قرن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنم بیدار از خواب گرانش</p></div>
<div class="m2"><p>که دشمن خسپد از تیغ و سنانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گران خوابش، یقین خواب عدم نیست</p></div>
<div class="m2"><p>ولی زو در درازی هیچ کم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود پیوسته با خوابش سر و کار</p></div>
<div class="m2"><p>چو چشم عاشقان همواره بیدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان چشمش ز بیداری رمیده</p></div>
<div class="m2"><p>که جز در خواب بیداری ندید ه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به راون کنب کرن آ تشین تاب</p></div>
<div class="m2"><p>برادر بود همچون مرگ با خواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خفتن داشت عشق آن مرگ پیکر</p></div>
<div class="m2"><p>برادر را بود مهر برادر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بخت بد همیشه بود در خواب</p></div>
<div class="m2"><p>ز غفلت همچو مرده سر به سر خواب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسا کس نامزد شد بهر این کار</p></div>
<div class="m2"><p>که گردد کنب کرن از خواب بیدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که روزی مرگ آید خواب تا کی ؟</p></div>
<div class="m2"><p>جهانی غرق شد، این آب تا کی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگرچه خفته در خواب گران بود</p></div>
<div class="m2"><p>دمش با نعرهٔ صد پاسبان بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دمش را نفخ صور انگاشت صد بار</p></div>
<div class="m2"><p>ز خواب مرگ می شد مرده بیدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسی کو تا در قصرش رسیدی</p></div>
<div class="m2"><p>چو برگ باد از دم پریدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرستاده برون در بماندند</p></div>
<div class="m2"><p>پریشان خاطر و اب ت ر بماندند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فکندند از عقب دیوار خانه</p></div>
<div class="m2"><p>درون رفتند آخر زین بهانه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هزاران بوق و کوس طبل شاهی</p></div>
<div class="m2"><p>به گوشش کوفتندی خود کماهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آن غوغا که گشتی گوشها ریش</p></div>
<div class="m2"><p>چو از افسانه می شد خواب او بیش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گل آتش به بستر برفشاندند</p></div>
<div class="m2"><p>هزاران پیل بر سینه دواندند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نمک در دیده اش سودند چندان</p></div>
<div class="m2"><p>که چون دریا نمک را دیده شد کان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به بیداری نکرده دیده اش رو</p></div>
<div class="m2"><p>نه غلطیده خود از پهلو به پهلو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ازان خواب گران دلگیر راون</p></div>
<div class="m2"><p>به دیوان کرد این تقریر راون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جزین تدبیر دیگر نیست معلوم</p></div>
<div class="m2"><p>که این آهن به آن آتش شود موم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بباید برد چندین حور منظر</p></div>
<div class="m2"><p>معطر کسوت اندر مشک و عنبر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به خوبی بر همه با ماه خویشان</p></div>
<div class="m2"><p>به مغزش چون در آید بوی ایشان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ازان نکهت ز خواب خوش بر آید</p></div>
<div class="m2"><p>پی نظاره چشمان برگشاید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به شهر اندر زنی کو نازنین بود</p></div>
<div class="m2"><p>اگر مستور در محفل گزین بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بسان غنچه عطر اندود گشته</p></div>
<div class="m2"><p>روان در خلوت موعود گشته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز بس در جلوه هر سو پیکر ماه</p></div>
<div class="m2"><p>به موج نور تنگ آمد گذرگاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خرامیدند کبکان حصاری</p></div>
<div class="m2"><p>به رنگ و بو چو گلهای بهاری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به بوی گلرخان عطرپیرای</p></div>
<div class="m2"><p>از آن خواب گران برخاست از جای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مگر دور قمر آمد پدیدار</p></div>
<div class="m2"><p>کزان خواب گران شد فتنه بیدار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو آن دیوانه دیو از خواب برخاست</p></div>
<div class="m2"><p>ستون آسمان از قامت آراست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صبوحی بهر او کردند موجود</p></div>
<div class="m2"><p>طعام و باده بر آیین معهود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هزاران خم شراب ارغوانی</p></div>
<div class="m2"><p>سب وی خود فزون تر ز آنچه دانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>برای نقل او کرده مهیا</p></div>
<div class="m2"><p>طعامی توده توده کوه بالا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز دریا بیش خورده بادهٔ خون</p></div>
<div class="m2"><p>برو ن قل و کباب از کوه افزون</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هم ازخون و هم از می گشت بد مست</p></div>
<div class="m2"><p>به پیش تخت راون رفت بنشست</p></div></div>