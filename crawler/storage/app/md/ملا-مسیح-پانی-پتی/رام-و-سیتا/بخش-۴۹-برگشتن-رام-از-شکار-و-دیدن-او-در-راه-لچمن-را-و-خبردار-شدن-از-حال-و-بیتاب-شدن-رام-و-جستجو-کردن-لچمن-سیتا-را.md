---
title: >-
    بخش ۴۹ - برگشتن رام از شکار و دیدن او در راه لچمن را و خبردار شدن از حال و بیتاب شدن رام و جستجو کردن لچمن سیتا را
---
# بخش ۴۹ - برگشتن رام از شکار و دیدن او در راه لچمن را و خبردار شدن از حال و بیتاب شدن رام و جستجو کردن لچمن سیتا را

<div class="b" id="bn1"><div class="m1"><p>چو رام از کشتن آهوی زرین</p></div>
<div class="m2"><p>طلسم دیو را برداشت تمکین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دیوان ماند حیران رام عاقل</p></div>
<div class="m2"><p>شکار افکن روان شد سوی منزل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شگون بد بسی دید اندران راه</p></div>
<div class="m2"><p>نظر بر لچمن افتادش به ناگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نادانیِ لچمن ماند حیران</p></div>
<div class="m2"><p>که بر سیتا ترا کردم نگهبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین صحرا که پر دیو است تنها</p></div>
<div class="m2"><p>نمی دانم چه باشد حال سیتا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به گفت و گوی سیتا با برادر</p></div>
<div class="m2"><p>تمامی قصه لچمن گفت از سر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روان گشتند بس با هم شتابان</p></div>
<div class="m2"><p>گلستان را ز گل دیدند ویران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو از وی نی نشانی یافت نی نام</p></div>
<div class="m2"><p>چو صیت خویشتن آواره شد رام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلیمان را رود چون خاتم از دست</p></div>
<div class="m2"><p>اگر آواره گردد جای آن هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فسونگر چون رباید مهره از مار</p></div>
<div class="m2"><p>زند مار از دریغش سر به دیوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زغیرت خون دل از دیده زد جوش</p></div>
<div class="m2"><p>چو پیل مست رادان از بناگوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندارد پیل تاب تب کشیدن</p></div>
<div class="m2"><p>نه در عشق آدمیزاد آرمیدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بس غیرت به طوفان غضب غرق</p></div>
<div class="m2"><p>چو مهر آتش فشان از پای تا فرق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خیال زلف او کردی بهانه</p></div>
<div class="m2"><p>زدی غیرت به جانش تازیانه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زبان چون نام ز لف یار بردی</p></div>
<div class="m2"><p>چو نار نیم کشته تاب خوردی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به زنجیر جنون آن پیل پیکر</p></div>
<div class="m2"><p>ز مستی خاک ره می کرد بر سر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گلش را تا خزان بربود ازدست</p></div>
<div class="m2"><p>دلش گلدسته های داغ می بست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز داغ هجر دل خون شد به صد بار</p></div>
<div class="m2"><p>مبادا هیچ ک س را دل گرفتار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو سیمابی در آتش دیده ریزان</p></div>
<div class="m2"><p>ز هم بگسسته و در خون فروزان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز بد حالی چو چشمش گشت مضطر</p></div>
<div class="m2"><p>برادر گفت کای جان برادر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مشو یکبارگی غمگین سراپا</p></div>
<div class="m2"><p>روم باری ببینم کوه و صحرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مگر جای تماشا رفته باشد</p></div>
<div class="m2"><p>به گل چیدن به صحرا رفته باشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگفت از تشنه جانی، دل خرابست</p></div>
<div class="m2"><p>سرابست این تسل یها نه آب است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر بر آبِ جو باید خرامید</p></div>
<div class="m2"><p>که سرو اندر کنار جو توان دید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چرا جویی در آب آن حور دلخواه</p></div>
<div class="m2"><p>که ماهی یابی اندر آب، نی ماه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به کوه آن دلربا پنهان نماند</p></div>
<div class="m2"><p>تجلّی خدا پنهان نماند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دگر بارش تسل ی داد لچمن</p></div>
<div class="m2"><p>برآمد در سراغ آن پری زن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برای جست و جوی آن سمن بر</p></div>
<div class="m2"><p>دلاسا داده راهی شد برادر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نخست از کوه جست احوال او باز</p></div>
<div class="m2"><p>جوابش را صدا در داد آواز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که بنگر کان صنم در من نهانست</p></div>
<div class="m2"><p>نهان در سنگ من آن لعلِ کانست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به صحرا کان سهی قد گل همی چید</p></div>
<div class="m2"><p>به صد حسرت در آنجا هم خرامید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تماشا کرد بر دل لاله را داغ</p></div>
<div class="m2"><p>یقین دانست کان گل نیست در باغ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز کوه و دشت آمد بر لب جوی</p></div>
<div class="m2"><p>به نیلوفر ز آبش شد نشان جوی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به ناگه حال آب جو دگر دید</p></div>
<div class="m2"><p>ز اشک بی غمانش خشک تر دید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فتاده کار آبش در تباهی</p></div>
<div class="m2"><p>نهنگان گشته عین ریگ ماهی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مگر زان خشک شد آب روانی</p></div>
<div class="m2"><p>که با من نیست آب زندگانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قسم خورده نهنگش بر گواهی</p></div>
<div class="m2"><p>مقرر شد ب ر آبش بی گناهی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زبان شد جمله تن، ماهی سخن گفت</p></div>
<div class="m2"><p>نخوردم یونس؛ اندر انجمن گفت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>صدف از نیلوفر زد دست بر گوش</p></div>
<div class="m2"><p>نه اصلاً نیست در من گوهر گوش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به پیش رام آمد با ز نومید</p></div>
<div class="m2"><p>سخن را آب داد از سحر ناهید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کزین غم رست باری جان دردا</p></div>
<div class="m2"><p>نشد در آب غرق آن ماه سیما</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مرا در دل یقین دان کان گمانست</p></div>
<div class="m2"><p>فریبت را بری جای نهانست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بیا تا مت فق با هم شتابیم</p></div>
<div class="m2"><p>به کوه از جست و جو شاید بیابیم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دلش زیر هزاران کوه اندوه</p></div>
<div class="m2"><p>چو فرهاد او روان شد جانب کوه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>صدای ناله صد فرسنگ می زد</p></div>
<div class="m2"><p>چو پای خویش سر بر سنگ می زد</p></div></div>