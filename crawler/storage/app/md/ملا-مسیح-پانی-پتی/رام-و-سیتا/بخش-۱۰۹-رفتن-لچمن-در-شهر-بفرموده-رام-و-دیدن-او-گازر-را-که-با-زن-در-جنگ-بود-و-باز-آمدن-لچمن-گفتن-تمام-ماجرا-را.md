---
title: >-
    بخش ۱۰۹ - رفتن لچمن در شهر بفرمودهٔ رام و دیدن او گازر را که با زن در جنگ بود و باز آمدن لچمن گفتن تمام ماجرا را
---
# بخش ۱۰۹ - رفتن لچمن در شهر بفرمودهٔ رام و دیدن او گازر را که با زن در جنگ بود و باز آمدن لچمن گفتن تمام ماجرا را

<div class="b" id="bn1"><div class="m1"><p>قضا را گازری با عقل و فرهنگ</p></div>
<div class="m2"><p>در آن شب با زن خود بود در جنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفا دل گا زری پاکیزه دامن</p></div>
<div class="m2"><p>که شب را شسته کردی روز روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیه نامه به دستش گر گذشتی</p></div>
<div class="m2"><p>چنان شستی که کاغذ تر نگشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به علم شست و شو زانگونه آگاه</p></div>
<div class="m2"><p>که بز دودی کلف از عارض ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جامه داغ می بردی کماهی</p></div>
<div class="m2"><p>چو ایمان ازدل کافر سیاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز داغ طعنه شسته کسوت ننگ</p></div>
<div class="m2"><p>سر خذلان زده چون جامه بر سنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آلایش به هفتاد آب دریا</p></div>
<div class="m2"><p>سه باره رخت ناموسش م طرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زن آن مرد بوده چون پری پاک</p></div>
<div class="m2"><p>به عصمت دامنش از هر تری پاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شبی شد گفت و گو بین زن و شوی</p></div>
<div class="m2"><p>در آن آزرده دل گشت آن بلاجوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برون از خانه نزدیک پدر شد</p></div>
<div class="m2"><p>به غم آن شب برو آنجا بسر شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پدر بهر دوای جان پرورد</p></div>
<div class="m2"><p>سحر دستش گرفت و بازش آورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپارش کرد دختر را به داماد</p></div>
<div class="m2"><p>به شفقت چون پسر را پند می داد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که بی موجب مفرما کینه را کار</p></div>
<div class="m2"><p>دل بیچاره را زین پس میازار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ولی داماد بی غیرت بر آشفت</p></div>
<div class="m2"><p>جواب راستی با دخترش گفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زن آن بهتر که بنشیند کر و کور</p></div>
<div class="m2"><p>به کنج خانه همچون م رده در گور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زنی کز آستان بیرون نهد پای</p></div>
<div class="m2"><p>بباید دف ن کردن زنده بر جای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو از خانه برون رفتی شبانگاه</p></div>
<div class="m2"><p>سیه روی چو شب با عارض ماه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو بالین پدر کردی بهانه</p></div>
<div class="m2"><p>چه دانم خود کجا خفتی شبانه؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو بنهادی ز خانه پای بیرون</p></div>
<div class="m2"><p>ترا در خانه‌ام جا نیست اکنون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برو هر جا که می خواهی به عالم</p></div>
<div class="m2"><p>شوی کشته اگر دیگر زنی دم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برو تا مانَ دی ناموس نامم</p></div>
<div class="m2"><p>نیم بی غیرت و رسوا چو رامم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که آن بی غیرت و نادان دگر بار</p></div>
<div class="m2"><p>دوان از خانه بر دیو نگونسار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به خانه برد زن را بعد ششماه</p></div>
<div class="m2"><p>به وی بنشست باری حسب دلخواه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چرا بی غیرتی را کار فرمود</p></div>
<div class="m2"><p>نه آخر در جهان قحط زنان بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو لچمن گوش کرد آن حرف غیرت</p></div>
<div class="m2"><p>به گوش رام گفت آن را ز حیرت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شنود و رام بر جا منفعل ماند</p></div>
<div class="m2"><p>چه جای لچمن، از خود هم خجل ماند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شکسته بر دلش صد دشنهٔ تیز</p></div>
<div class="m2"><p>نه دل دادش ولی بر تیغ خونریز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به لچمن گفت و برخایید ا نگشت</p></div>
<div class="m2"><p>که نتوانستن از تیغ جفا کشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که عاشق گرچه با جانان ستیزد</p></div>
<div class="m2"><p>کجا آن زهره تا خونش بریزد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که معشوق ارچه باشد ر ند فاسق</p></div>
<div class="m2"><p>به خون او نجنبد دست عاشق</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ببر اندر بیابانش ازینجا</p></div>
<div class="m2"><p>رها کن در دد و دامش به صحرا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>غزال مشک را کن طعمهٔ شیر</p></div>
<div class="m2"><p>که از دیدار او گشتم به جان سیر</p></div></div>