---
title: >-
    بخش ۲۵ - رخصت شدن راجه جسرت از جنک و ملاقات کردن پرسرام در راه
---
# بخش ۲۵ - رخصت شدن راجه جسرت از جنک و ملاقات کردن پرسرام در راه

<div class="b" id="bn1"><div class="m1"><p>مهیا آمد از بهر سواری</p></div>
<div class="m2"><p>به پیل آسمان پیکر عماری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو خرگاهی زده در کوهساری</p></div>
<div class="m2"><p>چو فانوس فروزان از ح صاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شوق عاشق آن معشوق سرمست</p></div>
<div class="m2"><p>بسان شمع در فانوس بنشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگاه رام مست مخمل وی</p></div>
<div class="m2"><p>دل مخمور چون از شیشۀ می</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنک در وقت رخصت با دم سرد</p></div>
<div class="m2"><p>سه منزل با عزیزان همرهی کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جنک در ره وداع دوستان داد</p></div>
<div class="m2"><p>عزیزان را سفر در پیش افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو از ره نیمه ای آمد به پایان</p></div>
<div class="m2"><p>شگون بد زهر سو شد نمایان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ناگه خواست گرد هولناکی</p></div>
<div class="m2"><p>به دلها آتشی در دیده خاکی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر آمد پرسرام از کوه صحرا</p></div>
<div class="m2"><p>به قصد قتل شان گفت آشکارا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندانی من کیم من پرسرامم</p></div>
<div class="m2"><p>زمین در لرزه می آید ز نامم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منم فرزند جمداکن برهمن</p></div>
<div class="m2"><p>که بهر کینۀ خون پدر من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز طاعت گه کشیدم خنجر تیز</p></div>
<div class="m2"><p>به هفت اقلیم کردم عام خونریز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وزیدم زهرهٔ رزم آزمایان</p></div>
<div class="m2"><p>به آب تیغ شستم نام رایان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نچهتر گرد گیتی بیست و یکبار</p></div>
<div class="m2"><p>ببخشیدم برای اهل زنار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شنیدستم که اندر شهر ترهت</p></div>
<div class="m2"><p>کمان بر من کشیده رام جسرت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کمان بشن در فرمان من هست</p></div>
<div class="m2"><p>کزو تیری ظفر اندازم از شست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سخن با چتری زاده هر که گوید</p></div>
<div class="m2"><p>کمان من کشد با جنگ جوید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به چشم خویش مرگ خویش چون دید</p></div>
<div class="m2"><p>ز حسرت جسرت نامید نالید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تمامی لشکرش شد نیست از هست</p></div>
<div class="m2"><p>به خون شست از حیات خویشتن دست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر آمد رام در میدان که استاد</p></div>
<div class="m2"><p>پدر را از تسلی ساخته شاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پدر هر چند منع از جنگ فرمود</p></div>
<div class="m2"><p>دلیر آمد کمانش از دست بربود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به حمله چون کمان بشن بشکست</p></div>
<div class="m2"><p>به قصد او خدنگی ماند بر شست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز سهمش پرسرا م آمد به زنهار</p></div>
<div class="m2"><p>که از بهر خدا جانم نگهدار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جوابش داد آن شیر نیستان</p></div>
<div class="m2"><p>که جان من فدای نام یزدان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ترا از نام یزدان چون نبخشم</p></div>
<div class="m2"><p>چو گفتی نام او جان چون نبخشم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرا بخشیدن آسانست آسان</p></div>
<div class="m2"><p>ولی خالی نیفتد تیر مردان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بفرما تا ازین صحرای نخجیر</p></div>
<div class="m2"><p>عبادت خانه ات اندازم از تیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به صد الحاح دیگر پرسرامش</p></div>
<div class="m2"><p>نکرد از اشک خونهایی به جامش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگفتا جان بشن و شخصی رامی</p></div>
<div class="m2"><p>که گردد گرد نامت نیکنامی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مکن نومید طاعت این زبون را</p></div>
<div class="m2"><p>نشان تیر خود ساز این نگون را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به زنهارش ببخشید آن جوانمرد</p></div>
<div class="m2"><p>هر آنچه خواست دشمن او همان کرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پدر صد آفرین داد و سپاهش</p></div>
<div class="m2"><p>که عاجز کرده بخشیدی گناهش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دگر تا تخت گه شد گلشن آباد</p></div>
<div class="m2"><p>ز ره رفتن نیاسودند چون باد</p></div></div>