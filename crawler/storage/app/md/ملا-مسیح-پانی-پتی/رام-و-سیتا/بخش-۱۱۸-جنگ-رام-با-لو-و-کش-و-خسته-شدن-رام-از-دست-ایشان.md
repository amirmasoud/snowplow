---
title: >-
    بخش ۱۱۸ - جنگ رام با لو و کش و خسته شدن رام از دست ایشان
---
# بخش ۱۱۸ - جنگ رام با لو و کش و خسته شدن رام از دست ایشان

<div class="b" id="bn1"><div class="m1"><p>ز جا شیری فلک فرسای جنبید</p></div>
<div class="m2"><p>فلک حیران که کوه از جای جنبید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هژبر معرکه لنکا شکن رام</p></div>
<div class="m2"><p>که از بیمش شود خون باده در جام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکوه پادشاهی پیش رو کرد</p></div>
<div class="m2"><p>دو اسبه تاخت بر رت بهر ناورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سبقت پیشدستی خواست در کار</p></div>
<div class="m2"><p>به لوکش تیر باران کرد بسیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حریفان مستعد جنگ بودند</p></div>
<div class="m2"><p>ز میدان پیش تعظیمش نمودند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو تیر انداخت از خجلت کمان را</p></div>
<div class="m2"><p>روان برداشت شمشیر و سنان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به غیرت گرچه تند و تیز آشفت</p></div>
<div class="m2"><p>ولی تیغ و سنان کندی پذیرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر نیزه به گاه حمله شد خم</p></div>
<div class="m2"><p>لب تیغش ز پرّانی نزد دم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسامش شد چو برگ بید چوبین</p></div>
<div class="m2"><p>چو نیشکر شکسته نوک ژوپین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به رمحش طعنه گر شد موی بر تن</p></div>
<div class="m2"><p>به تیغش خنده می زد تیغ سوسن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مزاج خون به خون گرم پیوست</p></div>
<div class="m2"><p>دم شمشیر نوک نیزه اش بست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر شد مهر فرزندیش روشن</p></div>
<div class="m2"><p>که گاهی زخم فرصت بردی آهن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز دیگر سو پسر چون ناوک انداخت</p></div>
<div class="m2"><p>پدر را خسته کرد و باز نشناخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به زخم تیر لو شد رام افگار</p></div>
<div class="m2"><p>دل عاشق چو از مژگان دلدار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه لو بد رام را کو زخم کین زد</p></div>
<div class="m2"><p>که تیر عشوه سیتا از کمین زد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به میدان ماند شخص رام خسته</p></div>
<div class="m2"><p>گریزان گشت لشکر دل شکسته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پسر کشته پدر را بر نمی داشت</p></div>
<div class="m2"><p>برادر را برادر خسته بگذاشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سراسیمه ازو لشکر جدا شد</p></div>
<div class="m2"><p>بدن خاکست ازو چون سر جدا شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وزیده باد فتح آسمانی</p></div>
<div class="m2"><p>کش و لو باغ باغ از شادمانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ولی چون کوس کین آمد به فریاد</p></div>
<div class="m2"><p>به گوش زاهد آن افتاد آواز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به حجره در نهاده گوش بر گوش</p></div>
<div class="m2"><p>به حیرت می گزید انگشت افسوس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که اندر کوه باشد کارزاری</p></div>
<div class="m2"><p>وگرنه چیست بانگ کوس باری ؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه نسبت جنگ را همسایۀ من</p></div>
<div class="m2"><p>که خوش کردیم صلح کل به دشمن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه من با کس، نه کس با من بداندیش</p></div>
<div class="m2"><p>بجز خود را ندانم دشمن خویش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به من دشمن بغیر از نفس کس نیست</p></div>
<div class="m2"><p>به جنگ من خود او را دسترس نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هم او را تا به خود بد خواه دیدیم</p></div>
<div class="m2"><p>به شمشیر ریاضت سر بریدیم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مگر شد زنده نفس من دگر بار</p></div>
<div class="m2"><p>که آمد بهر کین با من به پیکار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ازین آواز حیران با دل تنگ</p></div>
<div class="m2"><p>که در کوهم که را با کیست این جنگ؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روان شد تا بر آتش ریزد آبی</p></div>
<div class="m2"><p>به صلح از جنگ بستاند ثوابی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پدر افتاده، فرزندان ستاده</p></div>
<div class="m2"><p>تماشا دیده در حیرت فتاده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نکرده طعنه ناشایستگی شان</p></div>
<div class="m2"><p>که می دانست نادانستگی شان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز روی آن حقیقت پرده برداشت</p></div>
<div class="m2"><p>ز گفتن گفتۀ ناگفته نگذاشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نخستین کرد بر هر یک دعاها</p></div>
<div class="m2"><p>پس آنگه گفت یک یک ماجراها</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گرفته دست هر یک شد روانه</p></div>
<div class="m2"><p>به میدان جانب رام یگانه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که عقل مصلحت دان چون گهر سفت</p></div>
<div class="m2"><p>به اول جنگ آخر آشتی گفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سوی رام آمد آن پیر نکوکار</p></div>
<div class="m2"><p>مسیحا رفت بر بالین بیمار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زده آبی به روی رام آزاد</p></div>
<div class="m2"><p>به هوش آمد چو مستان دیده بگشاد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به زخمش پیر دست مرحمت سود</p></div>
<div class="m2"><p>نموده خستگی ها روی بهبود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بگفتا کیستی کز مهربانی</p></div>
<div class="m2"><p>مرا بخشیدی از سر زندگانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که شکر این چنین نعمت ندانم</p></div>
<div class="m2"><p>به جان منت پذیرم تا توانم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به پاسخ زاهد فرخنده دیدار</p></div>
<div class="m2"><p>همه سرّ نهفته کرد اظهار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کش و لو را به پای رام افکند</p></div>
<div class="m2"><p>که ما را بخش جرم این دو فرزند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که نا دانسته شد در جنگ تقصیر</p></div>
<div class="m2"><p>به حیرت ماند رام از گفتۀ پیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شگفتی در شگفتی در فزودش</p></div>
<div class="m2"><p>که در وهم و گمان این هم نبودش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گرفت اندر کنار و ماند حیران</p></div>
<div class="m2"><p>به شفقت بوسه زد بر روی ایشان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دلش راز محبت کرد پیدا</p></div>
<div class="m2"><p>نهان پرسید حال ماه سیتا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو بشنید آن نوید شادمانی</p></div>
<div class="m2"><p>که در کوه است جای لعل کانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فزون شد آتش شوق جگرتاب</p></div>
<div class="m2"><p>شتابان گشت چون تشنه سو ی آب</p></div></div>