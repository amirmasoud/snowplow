---
title: >-
    بخش ۶۷ - ملاقات با سنپات کرگس و همراه دادن پسر خود را به سپارش رام
---
# بخش ۶۷ - ملاقات با سنپات کرگس و همراه دادن پسر خود را به سپارش رام

<div class="b" id="bn1"><div class="m1"><p>قضا را کرگسی بود اندران کوه</p></div>
<div class="m2"><p>قوی هیکل بسان ابر اندوه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان کرگسان سنپات نامش</p></div>
<div class="m2"><p>در افتادند میمونان به دامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شادی بهر صیدشان شده پیش</p></div>
<div class="m2"><p>که دیده چند روزی روزی خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل شان از نهیب چنگ سنپات</p></div>
<div class="m2"><p>ز خود رفته چو رنجورانِ هیهات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنون گفتا جتا یو خوش کسی بود</p></div>
<div class="m2"><p>به ظاهر گرچه او هم کرگسی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چگونه بهر کار رام جان داد</p></div>
<div class="m2"><p>مثالش چون به این کرگس توان داد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که این چون آن نه ننگ و نا م خواهد</p></div>
<div class="m2"><p>هلاک قاصدانِ رام خواهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خبر چون از جتایو یا فت سنپات</p></div>
<div class="m2"><p>به مرگ او تاسف خورد هیهات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفت ای وحش آخر از کجایی</p></div>
<div class="m2"><p>که آید از تو بوی آشنایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگو حال جتایو با من از سر</p></div>
<div class="m2"><p>که چون جان داد دور از من برادر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هنون آن سرگذشت غیرت آموز</p></div>
<div class="m2"><p>منقح گفت یک یک تا به آن روز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به زاری گفت گر گس کای وفا جو</p></div>
<div class="m2"><p>برادر بود با من خود جتایو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من و او از هوا روزی به پرواز</p></div>
<div class="m2"><p>به قصد آسمان کردیم پرواز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نوردیدیم تا ماهی هوا را</p></div>
<div class="m2"><p>خجل کردیم نسرینِ سما را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازین ترک ادب خورشید والا</p></div>
<div class="m2"><p>سرافشاند آتش غیرت ز بالا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جتایو ضعف کرد از گرمی مهر</p></div>
<div class="m2"><p>به حال او مرا آمد به دل مهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرفتم زیر پر بهر امانش</p></div>
<div class="m2"><p>ز بال خویش دادم سایبانش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سلامت ماند او از گرمی خور</p></div>
<div class="m2"><p>مرا از تابش او سوخت شهپر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شدم پروانه شمع آسمان را</p></div>
<div class="m2"><p>بسنجیدم به بال و پر زمان را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیفتادم درین جا بی پر و بال</p></div>
<div class="m2"><p>شد از عمرم هزار و چارصد سال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درین مدت ز حال آن برادر</p></div>
<div class="m2"><p>نشد معلوم این مهجور غمخَو ر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنون بش نیدم از تو این خبر را</p></div>
<div class="m2"><p>که از غم پاره می سازم جگر را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دگر گفتا مرا بیتابیی هست</p></div>
<div class="m2"><p>که صد فرسنگ بینم چون کف دست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی بینم کنون سیتا به لنکاست</p></div>
<div class="m2"><p>به باغ دیوکان آن روی دریاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو عذر بی پری گفتم به یاران</p></div>
<div class="m2"><p>نیارم همرهی با غمگساران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سپارس نام فرزندی مرا هست</p></div>
<div class="m2"><p>که در مرغانست چون عنقا زبردست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دهم همراه بهر کینه کوشی</p></div>
<div class="m2"><p>کند در کار یاران جانفروشی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپارس را چو زین معنی خبر شد</p></div>
<div class="m2"><p>همان دم بهر خدمت جلوه گر شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سپارس را سپارش کرد بسیار</p></div>
<div class="m2"><p>ز یاران رخصتی گشت آن وفادار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ازین مژده دلیرانِ قوی دست</p></div>
<div class="m2"><p>به شادی رقصها کردند چون مست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هنون گفتا چه شاید شاد گشتن</p></div>
<div class="m2"><p>ز دریا چون توان آسان گذشتن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جوابش داد انگد کای هواخواه</p></div>
<div class="m2"><p>چو امروز آخر آمد، گشت بیگاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همان بهتر کز اندیشه برآییم</p></div>
<div class="m2"><p>ز خورد و خواب آسایش نماییم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به غم شب سر فرو یازیم در جیب</p></div>
<div class="m2"><p>که تا فردا چه ظاهر گردد از غیب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه خوش گفتند پیران جگر سوز</p></div>
<div class="m2"><p>غم فردا نشاید خوردن امروز</p></div></div>