---
title: >-
    بخش ۹۷ - جنگ کردن کنب کرن با هنومان و خوردن چندین هزار میمونان را و جنگ کردن میمونان با کنب کرن
---
# بخش ۹۷ - جنگ کردن کنب کرن با هنومان و خوردن چندین هزار میمونان را و جنگ کردن میمونان با کنب کرن

<div class="b" id="bn1"><div class="m1"><p>چو عزم رزم گشته در دلش جزم</p></div>
<div class="m2"><p>به رزم آمد چو مستان شاد در بزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جوش خون، دلش رشک قرابه</p></div>
<div class="m2"><p>مسلح پا نهاده بر ارابه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حصاری بود ارابه به هزار اسب</p></div>
<div class="m2"><p>کشیدندی به صد محنت سوار اس ب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو پیل بر شکسته آهنین بند</p></div>
<div class="m2"><p>قیامت در سپاه رام افکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جنگ آن نهنگ تشنۀ خون</p></div>
<div class="m2"><p>برون نامد کس از گُردان میمون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برو زد شاه میمونان قوی چنگ</p></div>
<div class="m2"><p>به قصد اژدها آمد هوا جنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برو بارید دیو آسمان تن</p></div>
<div class="m2"><p>ز شمشیر برو طوفان آهن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر بود اژدها آن کوه بنیاد</p></div>
<div class="m2"><p>که دندانش همی خایید پولاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ناخنها و دندانهای چون تیر</p></div>
<div class="m2"><p>نکرده شاه میمون هیچ تقصیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آهن خوردنش دندان چو شد کند</p></div>
<div class="m2"><p>به شیری دل به جنگ سنگ شد تند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز دستش سنگها رفتی به فرسنگ</p></div>
<div class="m2"><p>خجل از سنگسازش، سنگ خرسنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به آخر دیو کرده پیش دستی</p></div>
<div class="m2"><p>ز بالا در فکنده سوی پستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به ژوپین شه به سر افتاده مجروح</p></div>
<div class="m2"><p>به بیهوشی بسان جسم بی روح</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز شادی دیو سر بر چرخ افراشت</p></div>
<div class="m2"><p>شکار شیر کرد و نعره برداشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو دشمن چیره شد بر شاه میمون</p></div>
<div class="m2"><p>به جوش آمد به دل، هنونت را خون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به کین بگرفت بر کف تخته سنگی</p></div>
<div class="m2"><p>به جنگ دشمن آمد بی درنگی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دوان از زخم سنگ و ضربت مشت</p></div>
<div class="m2"><p>هزار اس ب ارابه ، یک به یک کشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فتاده ه ر لوند چرخ پیمای</p></div>
<div class="m2"><p>چو رخش آسمان بی دست و بی پای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بلا ناید ز میمون در طویله</p></div>
<div class="m2"><p>چرا میمون بلا شد بر طویله</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیاده گشت عفریت غضبناک</p></div>
<div class="m2"><p>به خونریزی چو تیغ عشق بیباک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به یک ساعت کم از اندازه بیرون</p></div>
<div class="m2"><p>فرو خورد از سپاه خرس و میمون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به خاک افکند سرها از همه سوی</p></div>
<div class="m2"><p>به پای سیل خونها باخته گوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو شد بر قلب دشمن دیو چیره</p></div>
<div class="m2"><p>گذشت اندر دل عفریت تیره</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که کار دشمنان ، خود ساختم من</p></div>
<div class="m2"><p>سرخصمان به خاک انداختم من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>غریق زخم خون شد شاه میمون</p></div>
<div class="m2"><p>فتاده خوار در میدانم اکنون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ظفر شد جنگ بی تقریب تا کی</p></div>
<div class="m2"><p>بهار آمد ؛گذارم آتش دی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا شاید که ترک جنگ گیرم</p></div>
<div class="m2"><p>شکار فتح را در چنگ گیرم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برم برداشته جسمش ازینجا</p></div>
<div class="m2"><p>که گ ردد جان راون ، خوش به لنکا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو بردارم برم جسمش بدین سان</p></div>
<div class="m2"><p>نماند کس ز میمونان به میدان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یلانش خود به خود خواهند بگریخت</p></div>
<div class="m2"><p>پراکنده گهر چون رشته بگسیخت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سپه بی شاه بگریزد به فرسنگ</p></div>
<div class="m2"><p>تن بی سر چه کار آید گه جنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تنش را در بغل بگرفت دشمن</p></div>
<div class="m2"><p>روان شد سوی لنکا پیش راون</p></div></div>