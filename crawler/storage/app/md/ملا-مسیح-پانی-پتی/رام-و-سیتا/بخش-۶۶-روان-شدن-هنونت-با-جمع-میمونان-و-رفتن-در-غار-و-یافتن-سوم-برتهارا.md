---
title: >-
    بخش ۶۶ - روان شدن هنونت با جمع میمونان و رفتن در غار و یافتن سوم برتهارا
---
# بخش ۶۶ - روان شدن هنونت با جمع میمونان و رفتن در غار و یافتن سوم برتهارا

<div class="b" id="bn1"><div class="m1"><p>کهن تاریخدان عشق نامه</p></div>
<div class="m2"><p>چنین جنباند خونی نوک خامه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون بهر دلاسای دلارام</p></div>
<div class="m2"><p>گرفت انگشتری هنونت از رام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزاران کس ز میمونان چیده</p></div>
<div class="m2"><p>برای همرهی خود بر گزیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر جامون و انگد نیز چون باد</p></div>
<div class="m2"><p>روان گشتند با او بهر امداد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخستین چون پری را جست وجو کرد</p></div>
<div class="m2"><p>سوی در بند کوهی بنده رو کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسی جستند لیک از جستن کوه</p></div>
<div class="m2"><p>نشد پیدا ز مقصد غیر اندوه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پیش آمد بیابانِ دگر باز</p></div>
<div class="m2"><p>که پایانش ندیده وهم تکتاز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سراسر چون دل عاشق خرابی</p></div>
<div class="m2"><p>چو چشم بیغمان، نادیده آبی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجزخورشید و گردون دیده در خواب</p></div>
<div class="m2"><p>ندید آنجا نشان سبزه و آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز سایه دور همچون سایه از نور</p></div>
<div class="m2"><p>بجز وحشت در آنجا کس نه معمور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هوایش دشمن جان رستنی را</p></div>
<div class="m2"><p>زمین خورده قسم آبستنی را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه دشتی، دیولاخی بود جانکاه</p></div>
<div class="m2"><p>که سر غول بیابان را زدی راه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرس نه تشنه، جستندی شتابان</p></div>
<div class="m2"><p>بجز گردی ندیدند از بیابان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به صد سختی ز دشت محنت آن روز</p></div>
<div class="m2"><p>برون بردند جان در سه شباروز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی جستند کوه و دشت و صحرا</p></div>
<div class="m2"><p>بپیمودند راه شیب و بالا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسی گشتند تا در آخر کار</p></div>
<div class="m2"><p>به غارستان در افتادند چون مار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دران غاران قضا را بود غاری</p></div>
<div class="m2"><p>مهیب و سهمگین چون تیر ه ماری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سیه تر از درون زردگوشان</p></div>
<div class="m2"><p>چون زندان خانۀ بیدادکوشان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه قعر غار تنگ و تیره منزل</p></div>
<div class="m2"><p>چو چشم حاسد و دلهای مدخل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درو راهی چو رمز عشق باریک</p></div>
<div class="m2"><p>چو گور ظالمان پر تنگ و تاریک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شدند آن رستمان در چاه بیژن</p></div>
<div class="m2"><p>بسان نقب زن در نقب کان کن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گرفته دست یکدیگر دران راه</p></div>
<div class="m2"><p>چو میمونان به وقت آب در چاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در آن ظلمتکده رفتند پر دور</p></div>
<div class="m2"><p>ندیده چشم شان تا هفته ای نور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عیان شد زان سیاهی پس سفیدی</p></div>
<div class="m2"><p>چو صبحی بعد شام نا امیدی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در آن غار سیه دیدند ماهی</p></div>
<div class="m2"><p>مثال یوسفی در قعر چاهی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پری زادی چو در شب شبچراغی</p></div>
<div class="m2"><p>نشسته در میان شاه باغی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>درون باغ زرین قصر و بامش</p></div>
<div class="m2"><p>پری را سوم برنا بود نامش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سوی شان کرد رو آن حورزاده</p></div>
<div class="m2"><p>که اینجاتان گذر چون او فتاده؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو میمونان سخن زان بت شنفتند</p></div>
<div class="m2"><p>جواب آنچه بشنفتند گفتند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدو گفتند کاین جنت سرا چیست</p></div>
<div class="m2"><p>تو خود گو کیستی وین گلشن از کیست؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صنم گفتا که من یزدان پرستم</p></div>
<div class="m2"><p>به گوهر دختر من دیو هستم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پریزادم ندانم شیوه ریو</p></div>
<div class="m2"><p>که جز طاعت نبوده کارِ من دیو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هزاران سال طاعت کرده درخواست</p></div>
<div class="m2"><p>چنین جایی که نتوان کردنش راست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دعای او اجابت یافت آسان</p></div>
<div class="m2"><p>درین عالم بهشتش داد یزدان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو این گلگشت جا خلوت سرایست</p></div>
<div class="m2"><p>کسی آگاه نی کین جا چه جایست؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>صبا را نیز بارِ آ مدن نیست</p></div>
<div class="m2"><p>وگر آید ره بیرون شدن نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>درینجا نیست مهر و ماه را راه</p></div>
<div class="m2"><p>که دارد از رخم هم مهر و هم ماه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز شمع عارضم اینجاست روشن</p></div>
<div class="m2"><p>یقین است این سخن دیگر مبر ظن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرا زین حسن بر خورشید ناز است</p></div>
<div class="m2"><p>که گاه سجده بر خاک نیاز است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سخن گفت و در حیرت گشوده</p></div>
<div class="m2"><p>به یک حیرت دو صد حیرت فزوده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز باغ خویش شهد و میوه و آب</p></div>
<div class="m2"><p>نمود آن ماه وقف جمع احباب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو مهمانان ز خوردن سیر گشتند</p></div>
<div class="m2"><p>ز عجز روبهی چون شیر گشتند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بگفتا چشم بندید آن مه نو</p></div>
<div class="m2"><p>که بنمایم کنون راه بدر رو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو دیده بسته یکدم آرمیدند</p></div>
<div class="m2"><p>به دم خود را فراز کوه دیدند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو مشرف بود کوه شان به دریا</p></div>
<div class="m2"><p>نشد ممکن سفرها پیش ازینجا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به دل کردند، گرما باز گردیم</p></div>
<div class="m2"><p>نکرده کار یار خود، نه مردیم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که رام جان به لب، چشم است در راه</p></div>
<div class="m2"><p>چو با بی حاصلی رفتیم ناگاه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به نومیدی دهد جان رام در غم</p></div>
<div class="m2"><p>ز مرگ او بمیرد مادرش هم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>درین صورت یقین بد کرده باشیم</p></div>
<div class="m2"><p>هلاک رام ما خود کرده باشیم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به تدبیر حیات آن یگانه</p></div>
<div class="m2"><p>بباید کرد ترک آب و دانه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اجل چون وام جان از ما ستاند</p></div>
<div class="m2"><p>برین امید شاید زنده ماند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>قسم خوردند کان کو اهل نام است</p></div>
<div class="m2"><p>بجز غم، خوردنی بر وی حرام است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو انگد بود زایشان ناز پرورد</p></div>
<div class="m2"><p>نخستین ضعف دل در وی اثر کرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از آن ضعف دل انگد گشت بیهوش</p></div>
<div class="m2"><p>غم خود همگنان را شد فراموش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نشسته زار میمونان سردار</p></div>
<div class="m2"><p>پی تیمار بر بالین بیمار</p></div></div>