---
title: >-
    بخش ۶۲ - در صفت فصل بهار برسات و فراق رام
---
# بخش ۶۲ - در صفت فصل بهار برسات و فراق رام

<div class="b" id="bn1"><div class="m1"><p>هوای عشق آمد فصل برسات</p></div>
<div class="m2"><p>که فردوس برین را می کند مات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بس آب و هوایش جان نوایست</p></div>
<div class="m2"><p>به خوبی پادشاه هر هوایست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار عاشقان برسات هند است</p></div>
<div class="m2"><p>ز دلها خون فشان برسات هند است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز رنگ و بوی پرکاریست در وی</p></div>
<div class="m2"><p>سراپا ناله و زاریست در وی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوایش بیدلان را سینه کاود</p></div>
<div class="m2"><p>که از هر ذره عشق نو تراود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به وصفش مخزن گوهر گشایم</p></div>
<div class="m2"><p>چو چشم ابر در ریزی نمایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بنشسته پس از سالی به میعاد</p></div>
<div class="m2"><p>سلیمان فلک بر مسند باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن باد آتش او گشت پر تیز</p></div>
<div class="m2"><p>بخار آب دریا یافت انگیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به گریه ابر را نو شد اجازه</p></div>
<div class="m2"><p>که سازد سن ت عشّاق تازه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فلک عاشق زمین معشوق زارست</p></div>
<div class="m2"><p>که آن را گریه وین را خنده کار است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به خنده ابر همچون زنگی مست</p></div>
<div class="m2"><p>به بازی کرده تیغ برق در دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به تیغ برق ابر تیره رو تُند</p></div>
<div class="m2"><p>ز تیزی خنجر زرین خود کُند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مقلد پیشه گشته ابر آذار</p></div>
<div class="m2"><p>کند تقلید دشت شاه دز بار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به روی آسمان ابر غریوان</p></div>
<div class="m2"><p>سپاه انگیخته از تیره دیوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه دیو است او که نازل شد فرشته</p></div>
<div class="m2"><p>به رحمت باری از رحمت سرشته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به سقایی چه شخص بی بدیل است</p></div>
<div class="m2"><p>که آب زندگیش آب سبیل است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دگر شد شیر خواره عالم پیر</p></div>
<div class="m2"><p>که ابر از شیره جان می دهد شیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز رنگ آمیزی از ابر درافشان</p></div>
<div class="m2"><p>گشاده رنگ ریز چرخ دوکان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هوا چون نشره اطفال رنگین</p></div>
<div class="m2"><p>که هر یک ابر دارد رنگ چندین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زمانه غیرت نشو و نما را</p></div>
<div class="m2"><p>نموده گلشن رنگین هوا را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کشیده مانی از سیمرغ تمثال</p></div>
<div class="m2"><p>گشاده بر افق رنگین پر و بال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کفش رنگ زمانه بیخت گویی</p></div>
<div class="m2"><p>شفق با روز و شب آمیخت گویی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یقین ابر است بازاری چو قصاب</p></div>
<div class="m2"><p>که در یک کلبه دارد آتش و آب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز بس هر پیشگی هست آن هنرور</p></div>
<div class="m2"><p>گهی خنجر فروش و گه کمانگر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نقاب شعله دود آه عشق است</p></div>
<div class="m2"><p>در افشان کاویان شاه عشق است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو غم تیره بلای آتش افشان</p></div>
<div class="m2"><p>به دم چون اژدهای آتش افشان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فلک موسی و ابر سرمه سان طور</p></div>
<div class="m2"><p>تجلّی های برقش کرده پر نور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نهنگان سرزده زین موج ۀ نیل</p></div>
<div class="m2"><p>شکسته بند آهن حلقۀ پیل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به فرق نیل گر در خوشاب است</p></div>
<div class="m2"><p>یقین کان نیل نیسانی سحاب است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به گلشن بر ریاحین سایه گستر</p></div>
<div class="m2"><p>چو پیلان پیش پیش از فوج لشکر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خدایی سایبانها بر کشیده</p></div>
<div class="m2"><p>ز نو خور آسمانها آفریده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همانا گشت چرخ بی کناره</p></div>
<div class="m2"><p>ز صور آه عاشق پاره پاره</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فلک دیر است ابر تیره رهبان</p></div>
<div class="m2"><p>چو دین مصطفی خورشید پنهان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز چشم ابر اشک شادمانی</p></div>
<div class="m2"><p>چکان هر دم چو آب زندگانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هوا گو یی گشاد از دست اعجاز</p></div>
<div class="m2"><p>سر صندوق مروارید تر باز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز قطره دائره بر آب سی ار</p></div>
<div class="m2"><p>زهی نقطه که داند کار پرگار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز صلب ابر ریزان نطفۀ پاک</p></div>
<div class="m2"><p>رحم تازه بدو آبستنی خاک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز شاخ خشک بار آرد گل تر</p></div>
<div class="m2"><p>هم از موز و صدف کافور و گوهر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فلک نوبت زده عیش و طرب را</p></div>
<div class="m2"><p>زمین نو سبزه کرده پشت لب را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز سبزه شخص گیتی پرنیان پوش</p></div>
<div class="m2"><p>فکنده طیلسان خضر بر دوش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نشاط و انبساط از حد شد افزون</p></div>
<div class="m2"><p>زمین زنگ دل خود داده بیرون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز سبزه خاک را میناست دربار</p></div>
<div class="m2"><p>گرفته آسمان زو وام زنگار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به سر سبزی جهان چون بخت شاهان</p></div>
<div class="m2"><p>به شادابی زمین چون روی ماهان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز سبزه کوری غم نیست د شوار</p></div>
<div class="m2"><p>چو از رنگ زمرد دیده مار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شکسته موسی الواج ز برجد</p></div>
<div class="m2"><p>از آن سبزی فراوان داشت بیحد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خروش انگیز هر سوتازه سیلی</p></div>
<div class="m2"><p>چو دیوانه به صحرا کرده میلی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دل مرغان ز بند دام آزاد</p></div>
<div class="m2"><p>که دانه سبز شد در دام صیاد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تراویده صفا از پیکر خاک</p></div>
<div class="m2"><p>فلک می گشت بر گرد سر خاک</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز اقسام ریاحین بس گل هند</p></div>
<div class="m2"><p>ش کفت و کوکلا شد بلبل هند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز عشق قطره چاتک بادم سرد</p></div>
<div class="m2"><p>رقابت با صدف چون مور می کرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دل طاووس را از مستی جوش</p></div>
<div class="m2"><p>شده معزولی جنّت فراموش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به یک آیینه نازیدی سکندر</p></div>
<div class="m2"><p>هزار آیینه دارد این به هر پر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به پیش طوطی آن آیینه بنهاد</p></div>
<div class="m2"><p>که تعلیم شکر گفتن دهد یاد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جهان حسن را آیینه دار است</p></div>
<div class="m2"><p>از آن آیینه هایش پرنگار است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کمال حسن دارد اندک ی نقص</p></div>
<div class="m2"><p>که گرید در غمش در شادی رقص</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز طعن عیبجویان می هراسد</p></div>
<div class="m2"><p>که عیب خویش نیکو می شناسد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز یک عیبی که داری گو میندیش</p></div>
<div class="m2"><p>که باشد هر هنر را عیب زان بیش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در آن موسم ز غم رام جگر خون</p></div>
<div class="m2"><p>شکسته دل چو شاخ بید مجنون</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر چه ابر هر س و سیلها راند</p></div>
<div class="m2"><p>دلش را آتش غم سیخ بنشاند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز ابر در فشان آسمان وش</p></div>
<div class="m2"><p>به هرکس آب باران بردی آتش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بگو سرو از چه گردد آتش من</p></div>
<div class="m2"><p>که آب افشاند بر وی نفط روغن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مگر نامت از ان شد ابرِ آذار</p></div>
<div class="m2"><p>که بر عاشق نباری غیر آزار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>دلش غیرت نما سنگ و سبو را</p></div>
<div class="m2"><p>دمش چون صبح خنجر زن گلو را</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو شمع از آتش دل چهره افروخت</p></div>
<div class="m2"><p>چو مشک اندر دلش خون جگر سوخت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز بس بی شمع خود با سوز می زیست</p></div>
<div class="m2"><p>برو خاکستر پروانه بگریست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گرو برده به جانکاهی ز مهتاب</p></div>
<div class="m2"><p>نشسته چون گل اندر آتش و آب</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ز بس آزار جان شرمنده شد عشق</p></div>
<div class="m2"><p>وفایش دیده از جان بنده شد عشق</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به صد جان کندن آن مدت بسر برد</p></div>
<div class="m2"><p>به صد حسرت دلش خون جگر خورد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به صد غم آمد آن فصلش به پایان</p></div>
<div class="m2"><p>در آمد اول فصل زمستان</p></div></div>