---
title: >-
    بخش ۹۴ - آمدن هنونت به کوه شمالی و جنگ کردن او با دیوان و کشته شدن سیصد هزار دیوان از دست هنونت
---
# بخش ۹۴ - آمدن هنونت به کوه شمالی و جنگ کردن او با دیوان و کشته شدن سیصد هزار دیوان از دست هنونت

<div class="b" id="bn1"><div class="m1"><p>شنید این حرف هنونت نکونام</p></div>
<div class="m2"><p>ندیده انتظار رخصت رام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شباشب رفت سوی کوه موعود</p></div>
<div class="m2"><p>که آرد نوشداروی گیا زود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی راون به دیوان داد فرمان</p></div>
<div class="m2"><p>که در کوه آتش افروزند چندان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که نشناسد گیا ، میمون سرکش</p></div>
<div class="m2"><p>غلط خوانَ د کتاب شمع ز آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر بشناسد آن جاسوس چالاک</p></div>
<div class="m2"><p>شکار مدعا بندد به فتراک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو باز آید سر راهش ببن دند</p></div>
<div class="m2"><p>ز آگاهی به غفلت خوش بخند ند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر جا کو نفس ره مانده گیرد</p></div>
<div class="m2"><p>به هر نوعی که خاطرها پذیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شما یکبارگی بر وی بتازید</p></div>
<div class="m2"><p>به ضرب و زور کار او بسازید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز فرمانش سپاه دیو زادان</p></div>
<div class="m2"><p>شتابیدند هریک بد نهادان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوان پیش از هنون کردن د گلزار</p></div>
<div class="m2"><p>ز آتش لاله گون دامان کهسار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدید آن کوه را هنونت مشتاق</p></div>
<div class="m2"><p>چو آتشخانۀ دلهای عشاق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بس کاتش فروزان جابه جا دید</p></div>
<div class="m2"><p>ز امکان دور، تشخیص گیا دید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان آمد برای آن صف آرای</p></div>
<div class="m2"><p>که از بن کوه را برکند از جای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر انگشتی نهاد آن کوه بیباک</p></div>
<div class="m2"><p>به شاخ گاو بر چون مرکز خاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز کوه انگشت او از روی تمثیل</p></div>
<div class="m2"><p>چو شهرستان لوط و پرّ جبریل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو پیل مس ت در چنگال عنقا</p></div>
<div class="m2"><p>چو کشتی گران بر موج دریا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو کشتی گشته کوه از بس روانی</p></div>
<div class="m2"><p>نموده پور بادش بادبانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان برخشک راندی کشتی کوه</p></div>
<div class="m2"><p>که دریا گشت غرق موج اندوه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سبک برداشت کُه را آن نکونام</p></div>
<div class="m2"><p>دل دانا چو محنتهای ای ام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زبار کوه دستش را زیان نی</p></div>
<div class="m2"><p>چو غم بر خاطر عاشق گران نی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به حکم رام گویی توأمان بود</p></div>
<div class="m2"><p>که کوه اندر رکاب او دوان بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز حلمش کوه بی پا از تحمل</p></div>
<div class="m2"><p>که کوهی را روان برداشت چون گل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بسان تند باد ی کو برد دود</p></div>
<div class="m2"><p>به زودی کوه را از جای بربود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به دستش کوه شد بی پا و بی صبر</p></div>
<div class="m2"><p>به فرمان موکل سر نهد ابر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قیامت رفت بر دیوان ناساز</p></div>
<div class="m2"><p>که کوه اندر هوا آمد به پرواز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سپاه دیو زادان از کمین گاه</p></div>
<div class="m2"><p>چو بر وی حمله آوردند ناگاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به دستی کوه، دستی تختۀ سنگ</p></div>
<div class="m2"><p>دران ره کرد با دیوان بسی جنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز خونِ بد رگان بس چشمه بگشاد</p></div>
<div class="m2"><p>به دستش کوه همچون گوی فصاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زبس بارید دستش سنگ باران</p></div>
<div class="m2"><p>فزون تر گشت از ششصد هزاران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به صد دل حمله آورده به یک دست</p></div>
<div class="m2"><p>سپاه دیو زادان جمله بشکست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پس از فتح و ظفر با خاطر شاد</p></div>
<div class="m2"><p>دوان در وعده گاه آمد با ستاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به شبگیری شباشب از سحر پیش</p></div>
<div class="m2"><p>برفت و کوه را آورد ب ا خویش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو اندیشه به کارش کرد تمییز</p></div>
<div class="m2"><p>به حیرت ماند رام و لشکرش نیز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زبان آفرینش برگشادند</p></div>
<div class="m2"><p>فزون از گفتنم انصاف دادند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سبک هر داوری کان بود در کار</p></div>
<div class="m2"><p>به دست آمد ازان کوه گرانبار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دوان بر زخمهای خسته بستند</p></div>
<div class="m2"><p>چنان به شد که پنداری نخستند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به دم برخاست هر یک خسته از جای</p></div>
<div class="m2"><p>چو گردد مرده روز حشر بر پای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خداوندی که تاثیر گیا داد</p></div>
<div class="m2"><p>گیا داد و به لچمن هم شفا داد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برادر را به صحبت دید چون رام</p></div>
<div class="m2"><p>به شکر حق زبان جنباند در کام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جبین ساییده بهر سجده بر خاک</p></div>
<div class="m2"><p>فراوان کرد شکر ایزد پاک</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سپه کردند هر یک تهنیت باد</p></div>
<div class="m2"><p>غمستان گشت در دم عشرت آباد</p></div></div>