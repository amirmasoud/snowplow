---
title: >-
    بخش ۵۳ - روان شدن رام خبر گرفتن سیتا را و ملاقات شدن با کیندها دیو و یافتن خبر سیتا
---
# بخش ۵۳ - روان شدن رام خبر گرفتن سیتا را و ملاقات شدن با کیندها دیو و یافتن خبر سیتا

<div class="b" id="bn1"><div class="m1"><p>چو روزی چند در صحرا قدم زد</p></div>
<div class="m2"><p>به ناگه فتنۀ عالم علم زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنیده دیو پیدا شد بلاجوش</p></div>
<div class="m2"><p>غریوان تر ز ابر آسمان پوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جسم چون غم افزون تر ز مقدار</p></div>
<div class="m2"><p>نموده از هوا بر شکل جاندار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه جانداری قوی هیکل چو سیمرغ</p></div>
<div class="m2"><p>که صد سیمرغ کردی طعمه چون مرغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سوی آن دو بیدل کرد آهنگ</p></div>
<div class="m2"><p>فتاده سایۀ پایش دو فرسنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فکندی سایه اش دام بلا را</p></div>
<div class="m2"><p>فرو بردی پلنگ و اژدها را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دیدند آن بلای ناگهانی</p></div>
<div class="m2"><p>فرو هشتند دست از زندگانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پی تدبیر دفعش آن نکو مرد</p></div>
<div class="m2"><p>نخستین با برادر مشورت کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که ای لچمن مشو بر جنگ سرگرم</p></div>
<div class="m2"><p>برو اول سخن گو با عدو نرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز نرمی گرنه با صلح اوفتد کار</p></div>
<div class="m2"><p>همه اسباب سختی هاست طیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قدم در پیش ماند وگفت با دیو</p></div>
<div class="m2"><p>که دیوا در گذر زین شیوهٔ ریو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو خار ره به دامانم میاویز</p></div>
<div class="m2"><p>که کاری پیش دارم غیرت انگیز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفت ای ساده لوح بخت در خواب</p></div>
<div class="m2"><p>چه جای گفت و گو بز را به قصاب؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خلاص از من عجب کاریست مشکل</p></div>
<div class="m2"><p>مگو ای لقمه کز حلقم فروهل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر رحم آورد بر مرغ صیاد</p></div>
<div class="m2"><p>رود دام و فکندنهاش بر باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو لچمن دید کار از صلح بگذشت</p></div>
<div class="m2"><p>به کین خصم هم چون بخت برگشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به جنگش متف ق شد با برادر</p></div>
<div class="m2"><p>هدف کردنش از تیر جگر در</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسا کوشید با هم رام و لچمن</p></div>
<div class="m2"><p>نیامد کارگر تیری به دشمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دگر ره حمله کرد آن شرزه با شیر</p></div>
<div class="m2"><p>بریده هر دو ساق او به شمشیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به تیغ تیزش از پا در فکندند</p></div>
<div class="m2"><p>به خاک و خون به صحرا در فکندند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دم جان دادن آن عفریت خونخوار</p></div>
<div class="m2"><p>تواضع کرد کای رام نکو کار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزرگیهای تو بر من نهان نیست</p></div>
<div class="m2"><p>به اخلاق تو یک تن در جهان نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ترا دانسته رنجانیدم امروز</p></div>
<div class="m2"><p>چشیدم لذت زخم جگر سوز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا در ضمن این کار دگر بود</p></div>
<div class="m2"><p>رسیدم خود ز سعی تو به مقصود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نیاید گر ملالی زین سخن باز</p></div>
<div class="m2"><p>بگویم سر گذشت خود ز آغاز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ندانی دیوم ای فرخند ه بنیاد</p></div>
<div class="m2"><p>به اصل و نسل بودستم پریزاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جمالم بود رشک زهره و ماه</p></div>
<div class="m2"><p>و لیکن از برای خنده گه گاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به شکل بد به شبها زاهدان را</p></div>
<div class="m2"><p>بترساندم چو طفلان عابدان را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به ناگه زاهدی بر من دعا کرد</p></div>
<div class="m2"><p>اجابت شد دعا کو بی ریا کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ازان خوش منظریها در گذشتم</p></div>
<div class="m2"><p>برین صورت که دیدی مسخ گشتم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز اقبال تو ای شایستۀ تخت</p></div>
<div class="m2"><p>بر آمد از وبالم اختر بخت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بگویم با تو بهر شکر و احسان</p></div>
<div class="m2"><p>سخن کز وی شود دشوارت آسان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو عقلم نیست بر جا از کثافت</p></div>
<div class="m2"><p>بزن آتش که باز آید لطافت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پس از خل ع بدن ک ردم به سر باز</p></div>
<div class="m2"><p>خردمندانه گویم با تو این راز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به امر رام لچمن آتش افروخت</p></div>
<div class="m2"><p>وجود مردهٔ آن دیو را سوخت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پری گشت و ز آ تش بر پریده</p></div>
<div class="m2"><p>سخن گفت و دگر چشمش ندیده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که اکنون مصلحت با تو همین است</p></div>
<div class="m2"><p>خبر از عالم بالا چنین است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که زرین کوه رکه مونک است پر نور</p></div>
<div class="m2"><p>ز انواع نعم دامانش معمور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>درآن فرمانروا سگریو میمون</p></div>
<div class="m2"><p>ترا باید بدو پیوستن اکنون</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نه میمون نوعی از حیوان صحراست</p></div>
<div class="m2"><p>به هندی نام او انسان صحراست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به فهم و زیرکی دارد درستی</p></div>
<div class="m2"><p>برو ختم است چالاکی و چستی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر صد تیر بارد بر یکی شان</p></div>
<div class="m2"><p>به چست و چابکی رد سازد آسان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بسا میمون که بازیدند شطرنج</p></div>
<div class="m2"><p>به دانایی گذشته از شط رنج</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز چه نوشند بی دلو و رسن آب</p></div>
<div class="m2"><p>نکو تدبیری این جمع دریاب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به تدبیری که آن میمون نماید</p></div>
<div class="m2"><p>در بسته به رویت در گشاید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به گوشش چون درآمد نام میمون</p></div>
<div class="m2"><p>ز میمون فال خود را یافت میمون</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دلش را رهبری شد سوی مقصود</p></div>
<div class="m2"><p>شتابان گشت سوی کوه موعود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عجب نبود ز بخت تیره فرجام</p></div>
<div class="m2"><p>که اف تد کار مردم با دد و دام</p></div></div>