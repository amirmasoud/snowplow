---
title: >-
    بخش ۸۸ - تسلّی دادن هنونت سپاه بیدل را
---
# بخش ۸۸ - تسلّی دادن هنونت سپاه بیدل را

<div class="b" id="bn1"><div class="m1"><p>چو رام از تیر جادو گشت مجروح</p></div>
<div class="m2"><p>به خاک افتاد همچون جسم بی روح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپاه از بیم گشته دل به دو نیم</p></div>
<div class="m2"><p>که جان بر تن نخستین داد تقدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بالینش ستاده شاه میمون</p></div>
<div class="m2"><p>همی گفتی فشاندی از مژه خون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یاران گف ت هم جا نیست اندوه</p></div>
<div class="m2"><p>که باشد جشن مردان مرگ انبوه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولیکن پردلان آهن آشام</p></div>
<div class="m2"><p>شده یکسر پریشان چون دلارام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به حدی رفته از گردان تهور</p></div>
<div class="m2"><p>که خون می شد دل از سهم تصور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سهم تیر جادو با دل ریش</p></div>
<div class="m2"><p>پراکندند همچون روزی خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلاسا داده گفتی شاه میمون</p></div>
<div class="m2"><p>که شد از بیم تان بدخواه دلخون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل خود را دمی بر جای دارید</p></div>
<div class="m2"><p>لوای سعی را بر پای دارید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو مردان جان ببازید از پی نام</p></div>
<div class="m2"><p>مسوزانید دل از خستن رام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که آخر رام هم خواهد شفا یافت</p></div>
<div class="m2"><p>چه شد از بخت گر روزی جفا یافت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به روز بدکنون سویش نبین ید</p></div>
<div class="m2"><p>به چشم اولین، رویش ببینید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسی زین گونه گفت آن صاحب هوش</p></div>
<div class="m2"><p>نکرده لیک حرف او کسی گوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندیده باز پس از گفته اش کس</p></div>
<div class="m2"><p>هوا خواهش ببیکن بوده و بس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو نشیند از زبان او کسی پند</p></div>
<div class="m2"><p>بر ابروی کهن چین نو افکند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که هر جا هر که خواهد گو برو زود</p></div>
<div class="m2"><p>که ناخوشنودم از شخصی نه خشنود</p></div></div>