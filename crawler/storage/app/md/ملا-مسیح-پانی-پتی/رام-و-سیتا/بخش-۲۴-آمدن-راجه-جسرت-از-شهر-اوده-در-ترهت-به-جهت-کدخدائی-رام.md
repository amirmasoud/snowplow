---
title: >-
    بخش ۲۴ - آمدن راجه جسرت از شهر اوده در ترهت به جهت کدخدائی رام
---
# بخش ۲۴ - آمدن راجه جسرت از شهر اوده در ترهت به جهت کدخدائی رام

<div class="b" id="bn1"><div class="m1"><p>دل جسرت به غایت شادمان شد</p></div>
<div class="m2"><p>همان ساعت خوشش آمد روان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو داد این مژده بخت کیقبادی</p></div>
<div class="m2"><p>زده کوس سفر با طبل شادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دست نوبتی کوس سفر ساز</p></div>
<div class="m2"><p>به طبل شادمانی شد هم آواز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بس شادی برآورده پر و بال</p></div>
<div class="m2"><p>روان فیل و حشم هر یک ز دنبال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پشت پیل تخت بخت بنهاد</p></div>
<div class="m2"><p>چو زرین قلعه ای بر کوه فولاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جانش گشت راحت محنت راه</p></div>
<div class="m2"><p>به شهر ترهت آمد بع د یک ماه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جنک با رام و لچمن چند منزل</p></div>
<div class="m2"><p>به استقبال او رفتند خوشدل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فزود آیینه بندی رونق شهر</p></div>
<div class="m2"><p>غلط گفتم چه شهر آرایش دهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به شهر آیینه بندی از رخ رام</p></div>
<div class="m2"><p>به نو خورشید بندی یافته نام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرود آورد اندر جشن گاهی</p></div>
<div class="m2"><p>شده مهمان شاهی کج کلاهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جنک در پیش جسرت دست بسته</p></div>
<div class="m2"><p>دو زانو از پی خدمت نشسته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بس آیین مجلس ساز کرده</p></div>
<div class="m2"><p>زمین بر آسمان صد ناز کرده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به زیر سایه بانها گلعذاران</p></div>
<div class="m2"><p>چو بر گلزار ابر نو بهاران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پریزادان به رقص و نغمه سرگرم</p></div>
<div class="m2"><p>سراپا شوخی و سرتا قدم شرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جدا هر گوشه بزم میگساران</p></div>
<div class="m2"><p>به نقل و باده سر خوش جرعه خواران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جنک را گفت جسرت چیست تدبیر</p></div>
<div class="m2"><p>به کار خیر نتوان کرد تأخیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جنک مشاطه را کرده اشارت</p></div>
<div class="m2"><p>که رو اهل حرم را ده بشارت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که سیتا را بپوشانند زیور</p></div>
<div class="m2"><p>عروسانه بیارایند دختر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز حسنش گرچه بد مشاطه معزول</p></div>
<div class="m2"><p>برای رسم شد در کار مشغول</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو زد شانه به فرق آن پری روی</p></div>
<div class="m2"><p>ز آرایش فرو نگذاشت یک موی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو دست عشق زلفش از درازی</p></div>
<div class="m2"><p>به پا می کرد با خلخال بازی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز زلفش موی بافی گشت آیین</p></div>
<div class="m2"><p>که تا نفتد ز پای خویش پایین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو دیده موی بندش گفت معجر</p></div>
<div class="m2"><p>که دایم بسته بادا این ستمگر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو زیب کاکل مشکین او دید</p></div>
<div class="m2"><p>بنفشه در چمن زان طره ببرید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به در پر کرد فرق دلستان ر ا</p></div>
<div class="m2"><p>به شب بنموده راه کهکشان را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز مروارید گوشش زهره بی تاب</p></div>
<div class="m2"><p>که می افزود نور از آتش و آب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به پیشانی چو عقد گوهر آویخت</p></div>
<div class="m2"><p>گل از شبنم به پیشانی عرق ریخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زمین از سایۀ آن نازنین حور</p></div>
<div class="m2"><p>سرا پا گشته غرق زیور و نور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز سرمه مست تر شد چشم مستش</p></div>
<div class="m2"><p>ز پان شاداب لعل می پرستش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حدیث آن دهان یارای من نیست</p></div>
<div class="m2"><p>سخن کوته که جای دم زدن نیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به رو چون خور تُتقها بسته از نور</p></div>
<div class="m2"><p>جمالش بی نقاب از دیده مستور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز عفت ساخته گلگونه را ساز</p></div>
<div class="m2"><p>حیای او نقابِ مقنع انداز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بسا خون ریخت ناز خود نمایش</p></div>
<div class="m2"><p>به دستش خونبها رنگ حنایش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کف دستش حنا را رنگ بشکست</p></div>
<div class="m2"><p>لب لعلش مگر زد بوسه بر دست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لباس سرخ کرده پای تا فرق</p></div>
<div class="m2"><p>سراپایش ز زیور در گهر غرق</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جمالش چون نمود آرایش عشق</p></div>
<div class="m2"><p>بر آرایش فزود آرایش عشق</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به پایش گشت رنگ آرای جاوک</p></div>
<div class="m2"><p>شفق را زد به پشت پای جاوک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به سیمین ساق او زر بوسه می داد</p></div>
<div class="m2"><p>خوش آن سیمی که زر در پایش افتاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو چشم عاشقان شد گوهر آمای</p></div>
<div class="m2"><p>به بتخانه پرستشگر به یک پای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به خلوتگه برهمن آتش افروخت</p></div>
<div class="m2"><p>ز بعد بید عود هوم چون سوخت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گره زد دامن معشوق و عاشق</p></div>
<div class="m2"><p>نموده با درون ب یرون موافق</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بران هر دو دعای بید می خواند</p></div>
<div class="m2"><p>به گرد آتش طاعت بگرداند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز شادی مست جام بی غش عشق</p></div>
<div class="m2"><p>همی گشتند گرد آتش عشق</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به گرد شعله گشت آن چشمۀ نور</p></div>
<div class="m2"><p>که گردد گرد شمعش آتش طور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به شمع روی شان پروانه جان باخت</p></div>
<div class="m2"><p>کز آتش روی ایشان باز نشناخت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدن برگرد آتش کرده رقصان</p></div>
<div class="m2"><p>به گرد یکدگر گشتند از جان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به گرد خویش خواهم گشتن امروز</p></div>
<div class="m2"><p>که می گردم به گرد آن دل افروز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز هر جانب مبارکباد برخاست</p></div>
<div class="m2"><p>ز اهل نغمه هم فریاد برداشت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نثار هر دو مه گوهر فشاندند</p></div>
<div class="m2"><p>چو گوهر داده شد اختر فشاندند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>برای رونمایی تازه باغی</p></div>
<div class="m2"><p>فلک مه داد و حیرت شبچراغی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جنک را چون ز بخت روشن اختر</p></div>
<div class="m2"><p>فرو شد ب ار دختر خوانده از سر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دگر داد و سبک تر کرد گردن</p></div>
<div class="m2"><p>حقیقی دختر خود را به لچمن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دو دختر داشت دیگر از برادر</p></div>
<div class="m2"><p>که با سیتا همی دیدش برابر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یکی زانها به دامان برت بست</p></div>
<div class="m2"><p>دگر را با سترگن رشته پیوست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به یک شب کرد آن هر چار شادی</p></div>
<div class="m2"><p>به نخل بختش آمد بار شادی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>برای دختران چار داماد</p></div>
<div class="m2"><p>ز اندیشه فراوان گنجها داد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نیامد از دماغش بوی تنگی</p></div>
<div class="m2"><p>بجز در دادن رخصت درنگی</p></div></div>