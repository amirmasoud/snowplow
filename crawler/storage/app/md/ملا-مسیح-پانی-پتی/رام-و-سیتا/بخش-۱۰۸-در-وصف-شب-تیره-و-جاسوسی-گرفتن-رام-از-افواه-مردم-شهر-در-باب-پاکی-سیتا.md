---
title: >-
    بخش ۱۰۸ - در وصف شب تیره و جاسوسی گرفتن رام از افواه مردم شهر در باب پاکی سیتا
---
# بخش ۱۰۸ - در وصف شب تیره و جاسوسی گرفتن رام از افواه مردم شهر در باب پاکی سیتا

<div class="b" id="bn1"><div class="m1"><p>شبی تیره چو دود آه عشّاق</p></div>
<div class="m2"><p>به هجر اندوده بام نیلگون طاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبی چون زنگیان آدمی خوار</p></div>
<div class="m2"><p>سراپا زهر همچون سهمگین مار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبی تاریک چون اسمان کافر</p></div>
<div class="m2"><p>لباس راهبان افکنده در بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی چون عاصی محشر سیه روی</p></div>
<div class="m2"><p>شبی چون نامهٔ اعمال بدگوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب از ظلمت زده راه نظاره</p></div>
<div class="m2"><p>تبه گشته درو درج س تاره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی ابر سیه بسته به زنجیر</p></div>
<div class="m2"><p>چو زنگی غوطه زن در چشمهٔ قیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملال افزا چو رنگ رخت ماتم</p></div>
<div class="m2"><p>عدو سیما تر از پیشانی غم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس دیده درازی شب تار</p></div>
<div class="m2"><p>غنوده خواب چشم نقش دیوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخ آیینهٔ مه وقف صد زنگ</p></div>
<div class="m2"><p>به زلف شب خضاب ابر سیه رنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیه دل گشته شام از کینهٔ صبح</p></div>
<div class="m2"><p>نفس نامد برون از سینهٔ صبح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو طفل کور دل گردون سبق خواند</p></div>
<div class="m2"><p>ولی در سورة واللیل در ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اجابت شد دعای مرغ عیسی</p></div>
<div class="m2"><p>برون نامد به معجز دست موسی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قوی دیدند مرغان کفر شب را</p></div>
<div class="m2"><p>که از تکبیر بر بستند لب را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز جنبش باد را پا ماند در گل</p></div>
<div class="m2"><p>نفس راه دهان گم کرد در دل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هوا در چشم انجم کرد میلی</p></div>
<div class="m2"><p>قفس شد زاغ ش ب را قرص نیلی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به نوعی عیش خواب از دست رفته</p></div>
<div class="m2"><p>که دزد و پاسبان همخواب گشته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز جاسوسی لبها گوش مایوس</p></div>
<div class="m2"><p>چو شب گردان نظر در دیده محبوس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سیاهی کرد نور از دیده غار ت</p></div>
<div class="m2"><p>چرا شد کُحل 2 شب دزد بصارت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شب از جادو گشاد آن سرمهٔ ناز</p></div>
<div class="m2"><p>کزو کس را نبیند کس دگر باز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز ظلم ظلمت شب خور به جان بود</p></div>
<div class="m2"><p>مگر بخت سیاه بیدلان بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سحر گشته عقیم از زادن خور</p></div>
<div class="m2"><p>که از قطران شب زهدان شدش پر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جهان زیر نگین گشته سیاهی</p></div>
<div class="m2"><p>کشید از ابر بر سر چتر شاهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به حدی گشت گیتی ظلمت اندود</p></div>
<div class="m2"><p>که نتوان فرق کرد از آتش و دود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گشاده شب دهان اژدهایی</p></div>
<div class="m2"><p>فرو برده ز عالم روشنای ی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهان تاری چو بخت تیره روزان</p></div>
<div class="m2"><p>درونی شمع ن ی پروانه سوزان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ازان شد چون دل عاشق سیه فام</p></div>
<div class="m2"><p>که فاسد گشت شب را خون اندام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سیاهی و درازی هیچ در هیچ</p></div>
<div class="m2"><p>فرو نگذاشت از موی بتان پیچ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شکسته شب ز مستی سرمه دان را</p></div>
<div class="m2"><p>حسد برده درون حاسدان را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو هندو زن به ماتم سرگشاده</p></div>
<div class="m2"><p>نشاط عالمی بر باد داده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گشاده آسمان ابر بلا بیز</p></div>
<div class="m2"><p>ته برگستوان خنگ شب تیز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سیه رنگی شب تاریکی اندوز</p></div>
<div class="m2"><p>چو افعی پوست افکنده همان روز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گلیم خرس شد ظلمت جهان را</p></div>
<div class="m2"><p>که از بگذاشتن بگذاشت آن را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو تابه تیره مانده قرصهٔ ماه</p></div>
<div class="m2"><p>جهان زندانی آن در تک چاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز رفتن پای برجا مانده چون کوه</p></div>
<div class="m2"><p>و زو سر زد هزاران کوه اندوه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خجل بر آسمان سیاره بی نور</p></div>
<div class="m2"><p>چو بر نیلوفرستان فوج زنبور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز مصحف آیت ستر زنان خواند</p></div>
<div class="m2"><p>عروس صبح در پرده از آن ماند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به شب انگشت گر دوکان گشوده</p></div>
<div class="m2"><p>نهاد انگشت بر هم توده توده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فلک چون نافهٔ تاتار مشکین</p></div>
<div class="m2"><p>زمین چون دخمهٔ کفار بی دین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در آن تاریک شب رامِ نکو نام</p></div>
<div class="m2"><p>ز حرف غیر آشفت آن دلار ام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به خلوت ره نداد آن شمع جان را</p></div>
<div class="m2"><p>ز غم تاریک کرده خانمان را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هلال از نور آن شمع شب افروز</p></div>
<div class="m2"><p>شده خود شمع و خود پروانهٔ سوز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو در گرداب غیرت غرق در ماند</p></div>
<div class="m2"><p>برادر را به خلو ت پیش خود خواند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که رشکم سوخت از پا ت ا به ناخن</p></div>
<div class="m2"><p>برو در شهر همراه سترگن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شنو تا ذکر ما چون در جهان است</p></div>
<div class="m2"><p>چه عنوان نام و ننگم بر زبانست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هم اکنون شو روان و پای در راه</p></div>
<div class="m2"><p>به گوش هوش شو جاسوس افواه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نکونام است سیتا یا که بد نام</p></div>
<div class="m2"><p>سزای آفرین یا وقف دشنام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هم آواز است خلق و هاتف غیب</p></div>
<div class="m2"><p>هنرگوید هنر را ، عیب را عیب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همی گویند ز انسانی که بینند</p></div>
<div class="m2"><p>که خلق و آینه با هم فریبند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به پیشم نقل کن پس بی کم و کاست</p></div>
<div class="m2"><p>قسم دادم، نخواهی گفت جز راست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو پیدا بشنوم راز نهان را</p></div>
<div class="m2"><p>کنم فکری که باید کرد آن را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>روان شد لچمن از پیشش سوی شهر</p></div>
<div class="m2"><p>که از افواه بخشد گوش از بهر</p></div></div>