---
title: >-
    بخش ۹۱ - جنگ شدن لچمن به اندرجیت و کشته شدن اندرجیت از دست لچمن
---
# بخش ۹۱ - جنگ شدن لچمن به اندرجیت و کشته شدن اندرجیت از دست لچمن

<div class="b" id="bn1"><div class="m1"><p>به لچمن گفت با گردان لشکر</p></div>
<div class="m2"><p>که هان وقت است بشتاب ای برادر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دشمن تاز و فرصت بر غنیمت</p></div>
<div class="m2"><p>ظفر باشد به سرعت بر غنیمت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رخصت لچمن اندر پایش افتاد</p></div>
<div class="m2"><p>روان از همتش درخواست امداد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که من فرمان تو می خواهم و بس</p></div>
<div class="m2"><p>نباشد همرهم گو از سپه کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و لیکن رام را فرمان چنان بود</p></div>
<div class="m2"><p>به لشکر جمله پوید همر هش زود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سران یکسر دوان اندر رکابش</p></div>
<div class="m2"><p>ستاره ذره های آفتابش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روان شد با سپاه خویش لچمن</p></div>
<div class="m2"><p>بدان ره رهنمای او ببیکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنوز اندرجت جادو در آنجا</p></div>
<div class="m2"><p>نگشته سحر خود را کارفرما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که لچمن ب ا سپاه دوزخین تف</p></div>
<div class="m2"><p>به گرد آتش معبد زده صف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بیم آن سپاه آهنین چنگ</p></div>
<div class="m2"><p>نهان شد باز آتش در دل سنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر آتش هوم خود آنگاه بگذاشت</p></div>
<div class="m2"><p>دل از جان، جان ز جادو پاک برداشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز افسون و ز جادو کار بگذشت</p></div>
<div class="m2"><p>به لچمن جنگ و صف کرد اندران دشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به لچمن، اندرجت زانسان سگالید</p></div>
<div class="m2"><p>کزان گاو زمین از بار نالید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی غرید چون ابر بهاران</p></div>
<div class="m2"><p>بسان قطره می زد تیر باران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به کین باریدن آمد ابر بیداد</p></div>
<div class="m2"><p>ازو هر قطره باران کوه پولاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گهی از جا به ناخن کوه کندی</p></div>
<div class="m2"><p>به گردون برده بر فرقش فکندی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گهی از برق تیغ آن ابر سرکش</p></div>
<div class="m2"><p>گشادی بر سرش طوفان آتش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسی کوشید دیو سخت بازو</p></div>
<div class="m2"><p>ولی شد بار سنگش در ترازو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدینسان دست برد ه دیو پرفن</p></div>
<div class="m2"><p>همی دید و همی خندید لچمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همی پیچید بر آتش گیاهی</p></div>
<div class="m2"><p>غریق آب می زد دست و پایی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو شب نزدیک شد کارش بپرداخت</p></div>
<div class="m2"><p>نفس آماجگاه تیر خود ساخت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به شکل خارپشتی کردش از تیر</p></div>
<div class="m2"><p>چو شیر آمد پس آنگه سوی نخ جیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کشیده پرنیان آتش افکن</p></div>
<div class="m2"><p>سحابی قطره او صاعقه زن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر اندرجِت به تیغ تیز بشتافت</p></div>
<div class="m2"><p>به برق آسمان گون کوه بشکافت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سرش را چون همی برید از تن</p></div>
<div class="m2"><p>تو گویی سعد ذابح بود لچمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به مرگ خود مثل زد دیو ملعون</p></div>
<div class="m2"><p>سگ از بی روح کندن چون زید چون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون نعش دیو را جوش کفن شد</p></div>
<div class="m2"><p>ظفر زلف علم را شانه زن شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرشته بر فلک گفتند با هم</p></div>
<div class="m2"><p>سر دیو سفید افکند رستم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برو روحانیان گلها فشاندند</p></div>
<div class="m2"><p>چو رنگ و بویش اندر گل فتادند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو لچمن کار اندرجت چنان ساخت</p></div>
<div class="m2"><p>سرش را پیش پای رام انداخت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ببوسیده سر و دستش برادر</p></div>
<div class="m2"><p>سران هم آفرین کردن یکسر</p></div></div>