---
title: >-
    بخش ۱۱۹ - آمدن رام پیش سیتا و در بستن سیتا بر رام و زاری شروع کردن رام
---
# بخش ۱۱۹ - آمدن رام پیش سیتا و در بستن سیتا بر رام و زاری شروع کردن رام

<div class="b" id="bn1"><div class="m1"><p>صنم را آگهی داد او از آن پیش</p></div>
<div class="m2"><p>نموده حجره در بتخانۀ خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو زاهد در درون حجره بنشست</p></div>
<div class="m2"><p>از آزار درونی در برون بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کار خود چو رام این بستگی دید</p></div>
<div class="m2"><p>برون در س تاد و زار نالید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جای لب ببوسید آستانش</p></div>
<div class="m2"><p>که خاکش بود شکر در دهانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفته همچو زلفش حلقه بر در</p></div>
<div class="m2"><p>دلش چون حلقه می پیچید در بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو می دانست خود حق جانب اوست</p></div>
<div class="m2"><p>که از من دشمنی شد در حق دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جرم خویشتن خود کرد اقرار</p></div>
<div class="m2"><p>برای معذرت بگریست بسیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که ها ها بر من بیدل ببخشای</p></div>
<div class="m2"><p>گناه غیرتم را عف و فرمای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جان ما مکن جورت زیاده</p></div>
<div class="m2"><p>که هجرانت سزایم نیک داده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نکو دانی تو هم ای ماه پاره</p></div>
<div class="m2"><p>که مردان را ز غیرت نیست چاره</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترا آزرده کردم بس گنه بود</p></div>
<div class="m2"><p>ولیکن غیرتم این کار فرمود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صنم گفتا قسم خورده بر اینم</p></div>
<div class="m2"><p>که من تا زنده ام، رویت نبینم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خاک پای عشقم باد سوگند</p></div>
<div class="m2"><p>که جز عشق از دلم نگشاد کس بند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر من زنده مانم یا نمانم</p></div>
<div class="m2"><p>ازین قطع نظر کرده است جانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه رویت بینم و نی رو نمایم</p></div>
<div class="m2"><p>روم در آب و در آتش در آیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر صد ره کنی داودی آ هنگ</p></div>
<div class="m2"><p>نخواهد نرم گشتن این دل سنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مده درد سرم دیگر به گفتار</p></div>
<div class="m2"><p>شناسم نغمه ات از جنبش تار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مشو دشمن به کشتی کز مگس دوست</p></div>
<div class="m2"><p>نخواهم پوستین بیرون مکن پوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسان آتشی سر تا قدم نور</p></div>
<div class="m2"><p>ترا باید پرستیدن هم از دور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نمک بر زخم من سایی به دو دست</p></div>
<div class="m2"><p>ز من حق نمک خواهی نمک هست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گل شادی ز گلزارت نه چینم</p></div>
<div class="m2"><p>به نامت جان دهم رویت نبینم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بشویم دل ز نقشت گر توانم</p></div>
<div class="m2"><p>به جان تو کنون در بند آنم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دگر گردم کتان بی ماه رویت</p></div>
<div class="m2"><p>سرت گردم نگردم گرد کویت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خیالت را کنم پدرود چون خواب</p></div>
<div class="m2"><p>بشویم جویبار دیده زان آب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به صد زاری کنون خواهم ز یزدان</p></div>
<div class="m2"><p>دلم را چون دل او سنگ گردان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دلم آواره کرد این جان ناشاد</p></div>
<div class="m2"><p>کز آواره شدی آواره بر باد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شکیبایی خدا را یاوری کن</p></div>
<div class="m2"><p>چو غم خونم خورد غمخوارگی کن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دلی دارم چو عهد نو شکسته</p></div>
<div class="m2"><p>مرا آن دل پریشان کرد و خسته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو خورشیدی و روزم از تو شب شد</p></div>
<div class="m2"><p>تو جانی وز تو جان من به لب شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تویی هر چند آب زندگانی</p></div>
<div class="m2"><p>منم آتش مرا لیکن زبانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نبینم روی تو از جان مایی</p></div>
<div class="m2"><p>مرا با جانست ترک آشنایی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر خواهد ترا جان، جان من نیست</p></div>
<div class="m2"><p>از آنِ تست او هم زان من نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وگر خود را بینم بی تو بی تاب</p></div>
<div class="m2"><p>ببرم خویش را از خویش چون آب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من از نام وفا بیزار گشتم</p></div>
<div class="m2"><p>وفا دارم کزین سان خوار گشتم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به من گفتی وفا کمتر کند زن</p></div>
<div class="m2"><p>چه جای گفتن است ای کمتر ا ز زن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر راما تویی مرد وفا دار</p></div>
<div class="m2"><p>زنان را زین وفا ننگست صد بار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وفا از هر دو سو باید به هر حال</p></div>
<div class="m2"><p>که نتواند پریدن مرغ یکبال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر چه کاه دل را کهربایی</p></div>
<div class="m2"><p>ز بندت یافتم لیکن رهایی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>من آهندل خلاص از هر کمندم</p></div>
<div class="m2"><p>که ز استغنا به مغناطیس خندم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وگر ناید برون از دل کمندت</p></div>
<div class="m2"><p>بسوزانم در آتش چون سپندت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چه پنداری دلم را نیست کینه</p></div>
<div class="m2"><p>من آن دل را برو ن کردم ز سینه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر آرد نسیم از گلشنت بو</p></div>
<div class="m2"><p>به زندانش کنم در حلقۀ مو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کنون تا کی فریب ای یار چالاک</p></div>
<div class="m2"><p>گریبان دوختن دلها ز دل چاک</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به پای من سر خاری که زد نیش</p></div>
<div class="m2"><p>برآوردی و کردی سینه ام ریش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کسی کز روی او کردی عرق پاک</p></div>
<div class="m2"><p>چرا خون ریختی بی جرم بر خاک</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز من بگذر مرا با خویش بگذار</p></div>
<div class="m2"><p>مرا و خویش را دیگر میازار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مگو دیگر که با هم غمگساریم</p></div>
<div class="m2"><p>ز یک آتش چو لاله داغداریم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نمک را چاشنی آری همانست</p></div>
<div class="m2"><p>ولی چاک جگر غیر دهانست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به عشقم دیده راز دل کند فاش</p></div>
<div class="m2"><p>نبودی این چنین بد خواه ای کاش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز دیده بر دلم آفت رسیده</p></div>
<div class="m2"><p>ازین رو می نبینم روی دیده</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر عشق آیدم بهر مدارا</p></div>
<div class="m2"><p>کنم جنگی به او هم آشکارا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اگر چشمم بگرید بهر دیدار</p></div>
<div class="m2"><p>بگویم دور زین بازار می زار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نیارد تاب دردم سنگ خاره</p></div>
<div class="m2"><p>شود الماس زین غم پاره پاره</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو تو کافردلی باید درین کار</p></div>
<div class="m2"><p>که بردارد جفاهای ترا بار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نه آخر چون منی تو آدمیزاد</p></div>
<div class="m2"><p>مکن بر چون خودی زین گونه بیداد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به ویرانه پرستی خانۀ یار</p></div>
<div class="m2"><p>چو در خانه بود ساز ی گنه کار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>منم کز آتش آه سحرگاه</p></div>
<div class="m2"><p>به گردون بر بسوزم خرمن ماه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اگر شد کند شمش یر نگاهم</p></div>
<div class="m2"><p>حذر فرض است نیز از تیر آهم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به کشتم شعله بارد ابر آزار</p></div>
<div class="m2"><p>به بختم نیشکر زهر آورد بار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عنان دل نمانده خود به دستم</p></div>
<div class="m2"><p>هلاک این دل دشمن پرستم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بر آن کس خنده دارد مرده در گور</p></div>
<div class="m2"><p>که ساید سرمه بهر دیده کور</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز کوه آمد جواب لن ترانی ۲</p></div>
<div class="m2"><p>دوچشمش چشمه گشته خونفشانی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نهاده چشم خود بر چشم روزن</p></div>
<div class="m2"><p>نهان می دید حالش آن پری زن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نهان از گریۀ آن ابر آزار</p></div>
<div class="m2"><p>همی زد خنده در دل رشک گلزار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تبسم می نهفت از چین ابرو</p></div>
<div class="m2"><p>تغافل می نمود از هر خم مو</p></div></div>