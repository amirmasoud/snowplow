---
title: >-
    بخش ۴۱ - کشته شدن خرد و وتراسرا نیز و چهارده هزار دیو از دست رام
---
# بخش ۴۱ - کشته شدن خرد و وتراسرا نیز و چهارده هزار دیو از دست رام

<div class="b" id="bn1"><div class="m1"><p>غریوان بر ارابه خر به پیکار</p></div>
<div class="m2"><p>روان شد همچو توپ صاعقه بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمایان از ارابه بیرق او</p></div>
<div class="m2"><p>چه بیرق، بادبان زورق ا و</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو خر با لشکر از حد عدد بیش</p></div>
<div class="m2"><p>هراول گشت در فوج بد اندیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز یکسو بر سراپا فوج چون تاخت</p></div>
<div class="m2"><p>تو گویی چرخ را خواهد برانداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس برخاست گرد گیتی اندای</p></div>
<div class="m2"><p>زمین را شاخ گاو چرخ شد جای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپرده رام سیتا را به لچمن</p></div>
<div class="m2"><p>چو خور تنها زده بر قلب دشمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شست تیر دلدوز آن صف آرا</p></div>
<div class="m2"><p>قفس می کرد مرغان هوا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هر تیرش هزاران دیو نخ جیر</p></div>
<div class="m2"><p>بدینسان زد به دیوان چارده تیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خر دانست کز تیر آشکارا</p></div>
<div class="m2"><p>دو خر را کشت رام و ترسرا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به قصد رام آمد تند چون باد</p></div>
<div class="m2"><p>اجل خود صید را آرد به صیاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رسولی بود هر یک بیلک رام</p></div>
<div class="m2"><p>که می داد از زبان مرگ پیغام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز عکس کرگسان چرخ پرواز</p></div>
<div class="m2"><p>شده روی زمین چون سینۀ باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چوتیری کش کشاد آن شرزه از شست</p></div>
<div class="m2"><p>به فرق دیو برق خرد بشکست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس از برق ارابه شد نگون فرق</p></div>
<div class="m2"><p>شکست بادبان کشتی کند غرق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو خر را کشت جمشید عدو بند</p></div>
<div class="m2"><p>گریزان برد از و جان دیوکی چند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شده همراه خواهر پیش راون</p></div>
<div class="m2"><p>به لنکا داد خواه از رام و لچمن</p></div></div>