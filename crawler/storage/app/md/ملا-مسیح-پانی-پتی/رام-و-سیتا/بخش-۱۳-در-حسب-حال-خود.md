---
title: >-
    بخش ۱۳ - در حسب حال خود
---
# بخش ۱۳ - در حسب حال خود

<div class="b" id="bn1"><div class="m1"><p>اگرچه خاطرم دریای رازست</p></div>
<div class="m2"><p>که دریا را به غواصیم نازست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگویم خسروم یا خود نظامی</p></div>
<div class="m2"><p>که تابد از دلم معنی تمامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن را آن خدا هست این پیمبر</p></div>
<div class="m2"><p>شود ز انکارشان اندیشه کافر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غواصان دریای خدایی</p></div>
<div class="m2"><p>چه لافد خاک بیزی در گدایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود مر خاک شو را قسمت زر</p></div>
<div class="m2"><p>ولی خاکست پیش کیمیاگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدا از گل تواند کرد آدم</p></div>
<div class="m2"><p>چه شد گر شد کلال استاد عالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگردد کس نبی ز افسون و تلبیس</p></div>
<div class="m2"><p>مگر جبریل بتراشد ز ابلیس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به آن نیرو که دا رد گنجفه باز</p></div>
<div class="m2"><p>چو موسی کی برآرد دست اعجاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شهباز خدایی بر زند پر</p></div>
<div class="m2"><p>بود عنقا به چشمش صید لاغر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکار عنکبوتان جز مگس نیست</p></div>
<div class="m2"><p>چه چاره بیش زان چون دسترس نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نچیند ه نس جز در دانه دانه</p></div>
<div class="m2"><p>غنیمت داند ارزن مرغ خانه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و لیکن همتم شد کار فرمای</p></div>
<div class="m2"><p>که در میدان مردان می نهم پای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مدد از کردگار ، از بنده همت</p></div>
<div class="m2"><p>بحمدالله که از کس نیست منّت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین کز همتم فرمانروایی است</p></div>
<div class="m2"><p>گدایی را خیال پادشاهی است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هست و نیست، همت آشنا نیست</p></div>
<div class="m2"><p>که همت پادشاه است و گدا نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تهی دستی نشد نقصان همت</p></div>
<div class="m2"><p>که بس کافی بود زو شان همت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو همت از ازل زادم تهی دست</p></div>
<div class="m2"><p>اگر چیزی ندارم همتم هست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو خورشیدم تهی دست زرافشان</p></div>
<div class="m2"><p>که پر سازم به گوهر دامن کان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدین همت که هست این بینوا را</p></div>
<div class="m2"><p>مگر خود را ببخشد یا خدا را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سر خامه چو مشک آل وده کردم</p></div>
<div class="m2"><p>حقیقت را مجاز اندوده ک ردم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عروس مهر در زیور کشیدم</p></div>
<div class="m2"><p>پس از عنبر برو معجر کشیدم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>معنبر معجرش افکنده بر سر</p></div>
<div class="m2"><p>چو دود مشک تاج کان عنبر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جز اهل دل حدیثم کس نخواند</p></div>
<div class="m2"><p>اشارتهای گنگان، گنگ داند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دلم را با خموشی خوش سر و کار</p></div>
<div class="m2"><p>زبان نغمه سنجم مست گفتار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به ماتم گر چه نبود جای رقّاص</p></div>
<div class="m2"><p>ملال است از نشستن پای رقّاص</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بزن ای تشنه لب بر ساغرم دست</p></div>
<div class="m2"><p>اگر آبی ندارد شربتی هست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به خون، تیغ زبان را آب دادم</p></div>
<div class="m2"><p>به زخمی کشور دلها گشادم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شدم گه شمع گه پروانۀ عشق</p></div>
<div class="m2"><p>نمودم روشنی در خانۀ عشق</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به شمعستان دهم پروانه را بار</p></div>
<div class="m2"><p>خلیلم عندلیبِ شعله گلزار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز نخل نی برآرم شعلۀ نور</p></div>
<div class="m2"><p>ببین ای موسی عشق آتش طور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سخن را از معانی تاج دادم</p></div>
<div class="m2"><p>معانی را به جان معراج دادم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مسیحی ساختم تیغ زبان را</p></div>
<div class="m2"><p>حیات جاودان و مردگان را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جهانی زندگانی زین زبان یافت</p></div>
<div class="m2"><p>عجب تیغی کزو بس مرده جان یافت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه مشّاطه شدم بر دلبر نو</p></div>
<div class="m2"><p>شراب کهنه لیکن ساغر نو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز بحر شوق چون رودی گشودم</p></div>
<div class="m2"><p>چراغ افروز آهنگی سرودم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز دلسوزی خرد بس منع فرمود</p></div>
<div class="m2"><p>ولی پروانه مستی کار خود بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سرتاریخ هندی زان گشادم</p></div>
<div class="m2"><p>که اندر جادوی هندی نژادم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نصیب از بخت من بگریست صد بار</p></div>
<div class="m2"><p>که از کردار پردازم به گفتار</p></div></div>