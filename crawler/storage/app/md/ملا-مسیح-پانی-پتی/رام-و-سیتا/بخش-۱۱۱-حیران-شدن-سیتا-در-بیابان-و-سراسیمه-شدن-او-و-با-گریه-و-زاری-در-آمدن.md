---
title: >-
    بخش ۱۱۱ - حیران شدن سیتا در بیابان و سراسیمه شدن او و با گریه و زاری در آمدن
---
# بخش ۱۱۱ - حیران شدن سیتا در بیابان و سراسیمه شدن او و با گریه و زاری در آمدن

<div class="b" id="bn1"><div class="m1"><p>به تنهایی بت خونبار بگریست</p></div>
<div class="m2"><p>برو برگ درختان زار بگریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خود گفت از کجا وحشت فزوده است</p></div>
<div class="m2"><p>مگر خواب پریشانم نمود ه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به حیرت ماند تنها در بیابان</p></div>
<div class="m2"><p>سراسیمه شده هر سو شتابان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی جز کوزه کم دید آن غم اندود</p></div>
<div class="m2"><p>ز رفتنها چو اشک خود نیاسود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخنگویان به خویش آشفته می رفت</p></div>
<div class="m2"><p>بیابان را به گیسو رفته می رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که راما رم شدی چون از دلارام</p></div>
<div class="m2"><p>تو اسم بی مسمایی مگر رام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبانم تهینت گوید جفا را</p></div>
<div class="m2"><p>فغانم مرثیه گوید وفا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندیدم در وفایی ث انیش را</p></div>
<div class="m2"><p>محبت قشقه کش پیشانیش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکایت ها که می کرد آن دلارام</p></div>
<div class="m2"><p>مگرخود را یقین دانست بد رام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنون راما اگر گردی تو خورشید</p></div>
<div class="m2"><p>چو سایه مانی از خورشید نومید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نبینی عکس من ز آیینۀ آب</p></div>
<div class="m2"><p>بپرهیزد خیالم از تو در خواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که از غایب به دل کردی خطابی</p></div>
<div class="m2"><p>که از جرمش به خود کردی عتابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرفته سخت دامان وفا را</p></div>
<div class="m2"><p>که حاضر کن ضمانا خصم ما را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زبون گیری تو نیز ای عشق سرکش</p></div>
<div class="m2"><p>که با رام آب و با ما هستی آتش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کشیدی از دلش پیش ذقن آه</p></div>
<div class="m2"><p>که خوش بگریخت آن زندانی چاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگون آویخته زلف گره گیر</p></div>
<div class="m2"><p>که دزدم را رها کردی ز زنجیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شکایت را دمی ب ا چشم گریان</p></div>
<div class="m2"><p>به وحش و طیر دارد جان بریان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پریزادم نیارم ز آدمی یاد</p></div>
<div class="m2"><p>نهان بهتر پری از آدمیزاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز جور آدمی عزلت گزیده است</p></div>
<div class="m2"><p>پری را چشم کس زان رو ندیده است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به تنهایی کنم خو ، همچو خورشید</p></div>
<div class="m2"><p>نهان ما نم چو آب خضر جاوید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به صحرا خوش بسازم با دد و دام</p></div>
<div class="m2"><p>نگیرم ز آدمی زین پس دگر نام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نمانده مردمی در نوع انسان</p></div>
<div class="m2"><p>که بهتر ز آدمی غول بیابان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز انسان هیچ کس بدتر ندیدم</p></div>
<div class="m2"><p>که در نوع بشر جز شر ندیدم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زهی بدنفسی انسان پر فن</p></div>
<div class="m2"><p>به بی موجب به هر حیوانست دشمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر نامش بری اندر بیابان</p></div>
<div class="m2"><p>زنند آتش وطن را بی زبانان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به کام اژدها رفتن به ناکام</p></div>
<div class="m2"><p>به از پهلوی انسان کردن آرام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز چشم ناز کو معزول باشد</p></div>
<div class="m2"><p>که تا کی عشوه ام دلها خراشد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به معشوقی دگر با کس ننازم</p></div>
<div class="m2"><p>به حسن خویشتن خود عشقبازم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به صحرا از رخ آن رشک گلزار</p></div>
<div class="m2"><p>تجلّی زار گشته هر سر خار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نسیم ناز از هر جا وزیدی</p></div>
<div class="m2"><p>کرشمه رست او عشوه دمیدی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به بویش دشت شد دکان عطار</p></div>
<div class="m2"><p>نهالِ صندل و کافور اشجار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به هر وادی گذر آن سیمتن کرد</p></div>
<div class="m2"><p>نسیم نو بهار آنجا وطن کرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پریزادی شد از بخت پریشان</p></div>
<div class="m2"><p>به خویشاوندی غول بیابان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دل و جان کرد وقف نامرادی</p></div>
<div class="m2"><p>روان شد کوثر ثانی به وادی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز جنگ شیر ببر مردم آزار</p></div>
<div class="m2"><p>به صحرا آتش آهش نگهدار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به آتش خوش سرو کاری فتادش</p></div>
<div class="m2"><p>جز اشک شور کس آبی ندادش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز سوز دل به هر جا کرد فریاد</p></div>
<div class="m2"><p>ز آهش در بیابان آتش افتاد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دل عاشق شده از خسته جانی</p></div>
<div class="m2"><p>چو چشم دلبران از ناتوانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بری از فتنه چشم فتنه سازش</p></div>
<div class="m2"><p>کم آزاری گرفته شیوه نازش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کرشمه زان مژه مهجور مانده</p></div>
<div class="m2"><p>شکر خند از دهانش دور مانده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو آهو چشمش از سرمه رمیده</p></div>
<div class="m2"><p>گرفته خوابگاهش آب دیده</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دماغ آشفته ای زان زلف پرتاب</p></div>
<div class="m2"><p>دلش بیمارتر زان چشم پر آب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شکسته رنگ سرخی لبانش</p></div>
<div class="m2"><p>دل تنبول خون دور از دهانش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز خاموشی او حیران تکلم</p></div>
<div class="m2"><p>ز هجران لبش گریان تبس م</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز خونریزی کرشمه دست کوتاه</p></div>
<div class="m2"><p>فریب عشوه دلها را نزد آه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به غم از چشم دل اشک نشان داد</p></div>
<div class="m2"><p>پی نخجیر از خون یافت صی اد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به تنهایی دریده در بیابان</p></div>
<div class="m2"><p>مغیلان دامن عشقش گریبان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به سختی در فتاد آن نازک اندام</p></div>
<div class="m2"><p>کفیده نازنین پایش به هر گام</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو چشم خود شده بیما ر پریان</p></div>
<div class="m2"><p>چو موی خود سراسیمه پریشان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز نیش غم دلش چون پای افگار</p></div>
<div class="m2"><p>حنا بند کف پایش به خون خار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تراوش کرد داغ دل به پهلو</p></div>
<div class="m2"><p>گذشت آما س پایش تا به زانو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به خار افتاد کار و بارش انبای</p></div>
<div class="m2"><p>که چون شبنم به روی گل بدش جای</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سراپا آبله گشت از روانی</p></div>
<div class="m2"><p>کف پایش چو آب زندگانی</p></div></div>