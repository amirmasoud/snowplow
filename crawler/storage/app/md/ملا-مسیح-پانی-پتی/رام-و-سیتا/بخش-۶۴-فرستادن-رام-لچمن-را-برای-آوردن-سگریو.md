---
title: >-
    بخش ۶۴ - فرستادن رام لچمن را برای آوردن سگریو
---
# بخش ۶۴ - فرستادن رام لچمن را برای آوردن سگریو

<div class="b" id="bn1"><div class="m1"><p>چو شاهنشاه چین از غایت کین</p></div>
<div class="m2"><p>ز هر سو آخته شمشیر زری ن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عزم رزم شاه زنگی شب</p></div>
<div class="m2"><p>فشاند از زهر خندی آتش از لب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دران میدان مظّفر گشت و منصور</p></div>
<div class="m2"><p>به چرخ افراخت بختش بیرق نور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کین لچمن شده چون مهر در تاب</p></div>
<div class="m2"><p>حمائل کرده در بر تیغ زهر آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چشمه سوی کسکندها زده گام</p></div>
<div class="m2"><p>که صبح عمر میمونان کند شام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خود برده به فرمان همایون</p></div>
<div class="m2"><p>پیام رام بهر شاه میمون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که باید همچو گوی از سرشتابی</p></div>
<div class="m2"><p>وگرنه بر تن خود سر نیابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شنیده آتش کین از حد افزون</p></div>
<div class="m2"><p>چو زیبق مضطرب شد شاه میمون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکو نامد جز این تدبیر دیگر</p></div>
<div class="m2"><p>که بشتابد به جان یا سازد از سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوان آمد به صد جان معذرت خواه</p></div>
<div class="m2"><p>به مژگان روفت خاشاک و خس راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز جا جنبید میمون نکو رأی</p></div>
<div class="m2"><p>ز میمونان و خرسان لشکر آرای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه پیل افکن و غرنده چون ابر</p></div>
<div class="m2"><p>همه شیر افکن و درنده چون ببر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نهنگانی به نیرو اژدها جنگ</p></div>
<div class="m2"><p>پلنگانی به کین شیر هوا جنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زمین بوسید و پیش ر ام استاد</p></div>
<div class="m2"><p>پس از پوزشگری عرض سپه داد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که شاها عفو کن زین بنده تقصیر</p></div>
<div class="m2"><p>سپه را جمع می کردم به تدبیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آن در آمدنها دیر کردم</p></div>
<div class="m2"><p>که جمع فوج فوج شیر کردم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دگر خاطر نشانم بایدت کرد</p></div>
<div class="m2"><p>یقین بادت که روز رزم و ناورد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به گیتی هیچ کس را نیست یارا</p></div>
<div class="m2"><p>که بستیزد به راون دیو لنکا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جز این جمعی که آوردم به خدمت</p></div>
<div class="m2"><p>کرا این زهره وین نیرو و هم ت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه میمونند، خرسان خود کیانند</p></div>
<div class="m2"><p>که اینها هم ز تخم جنیانند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به اینها فتح لنکا منحصر بود</p></div>
<div class="m2"><p>سخن کوته حدیثم مختصر بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنون شادان گره دور از جبین کن</p></div>
<div class="m2"><p>ببین تدبیر کار و آفرین کن</p></div></div>