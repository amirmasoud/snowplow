---
title: >-
    بخش ۲۳ - رفتن رام در شهر ترهت همراه بسوامتر و کشیدن کمان در سینمبر و دادن راجه جنک دختر خود را سیتا به کدخدایی رام
---
# بخش ۲۳ - رفتن رام در شهر ترهت همراه بسوامتر و کشیدن کمان در سینمبر و دادن راجه جنک دختر خود را سیتا به کدخدایی رام

<div class="b" id="bn1"><div class="m1"><p>ز ره رفتن زمانی نارمیدند</p></div>
<div class="m2"><p>به روز جشن در ترهت رسیدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن مجمع پی بخت آزمایی</p></div>
<div class="m2"><p>همی کردند رایان خود نمایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی بر جِیش بی اندازه نازان</p></div>
<div class="m2"><p>یکی بر ملک و دولت مهره بازان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی نام و نسب را یاد می کرد</p></div>
<div class="m2"><p>یکی خود بر حسب، دل شاد می کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی بر عشق خود گشته فسون سنج</p></div>
<div class="m2"><p>یکی انگیخته منصوبۀ گنج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی مغرور همچون برق بر تیغ</p></div>
<div class="m2"><p>یکی را لاف گوهر بارش میغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد هر دم گل نظاره می چید</p></div>
<div class="m2"><p>نشسته عشق عرض حسن می دید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو اهل جشن روی رام دیدند</p></div>
<div class="m2"><p>به استقبال پیش از خود دویدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خود گفتند هر یک حیرت این است</p></div>
<div class="m2"><p>که خورشید فلک چون برزمین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکوهش کرد بر شاهان تقد م</p></div>
<div class="m2"><p>چو آید آب بر خیزد تیمم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جنک تعظیم زاهد کرده برخاست</p></div>
<div class="m2"><p>برای میهمان خلوتگه آراست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نهان پرسید ازو کین نوجوان کیست</p></div>
<div class="m2"><p>لباس فقر در بر از پی چیست؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز اقبالش چنان دانم که شاه است</p></div>
<div class="m2"><p>که پیشانیش بر دولت گواه است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به او خویش است مانا همره او</p></div>
<div class="m2"><p>که چون خورشید می تابد مه او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تبسم کرده زاهد با جنک گفت</p></div>
<div class="m2"><p>شکر خندید و در گفتن گهر سفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که این صاحبقران را رام نام است</p></div>
<div class="m2"><p>به تیغ برق تیغش هم نیام است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خون دشمنان این شیر سرمست</p></div>
<div class="m2"><p>چو خورشیدست صد خنجر به یکدست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زتیرش زهر ماری کرده بر زه</p></div>
<div class="m2"><p>ز شمشیرش به شیر شرزه لرزه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به چشمش پیل را مستی نمانده</p></div>
<div class="m2"><p>چه مستی بلکه خود هستی نمانده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دگ ر این لچمن گیتی ستان است</p></div>
<div class="m2"><p>به بازو با برادر هم عنان است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زیک روغن چراغی هر دو پر نور</p></div>
<div class="m2"><p>چو گوهر زاد هٔ نیسان و کافور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برای خاطر من راجه جسرت</p></div>
<div class="m2"><p>ز بهر قتل دیوان داد رخصت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز دفع شر دیوان این دو فرزند</p></div>
<div class="m2"><p>دل یک عالمی کردند خرسند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کنون ذوقست رام نوجوان را</p></div>
<div class="m2"><p>که در مجمع کشد پیشت کمان را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از آن رو رام را شوق کمانست</p></div>
<div class="m2"><p>که زیب چشم خوبان ز ابروانست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جنک بشنید و در تعظیم افزود</p></div>
<div class="m2"><p>اشارت کرد که آرند آن کمان زود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز خانه صد کسش بیرون نهادند</p></div>
<div class="m2"><p>چو برج قوس بر گردون نهادند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به پیش رام آوردند گردون</p></div>
<div class="m2"><p>سبک از قید قربان ساخت بیرون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زده دست آن کمان ابرو به فرمان</p></div>
<div class="m2"><p>کمان بر گوشۀ ابروش قربان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو آتش نرم کرده بند بندش</p></div>
<div class="m2"><p>ز بی قیدی به قید زه فکندش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو آن قوس قزح را ساخته زه</p></div>
<div class="m2"><p>بگفت از چاشنی قوس قزح؛ زه !</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از آن در چاشنی خمیازه آورد</p></div>
<div class="m2"><p>که بیدارش ز خواب عمرها کرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به نوعی دست کرد آن قبضه را چست</p></div>
<div class="m2"><p>کز آن سختی شده چون دست کس سست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نرفته در کمان خانه مگر رام</p></div>
<div class="m2"><p>که ماه نو شد از نو برج بهرام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه درشد در کمان رام ظفر کیش</p></div>
<div class="m2"><p>در آمد مشتری در خ انۀ خویش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شرف شد مشتری را زهره را اوج</p></div>
<div class="m2"><p>مه و خور زان قران سعد شد زوج</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کشیده قوس گردون بازوی او</p></div>
<div class="m2"><p>نگشته خم کمان ابروی او</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کشیده آن کمان ابرو گشاده</p></div>
<div class="m2"><p>ز سهمش لرزه در رایان فتاده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جهان لرزد چو مهر از قوس تابد</p></div>
<div class="m2"><p>چنین معنی به غیر از من که یابد؟</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ازآن ابرو کمان گفت ای کماندار</p></div>
<div class="m2"><p>کشیدی دو کمان خوش خوش به یکبار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کمان بشکست و تیرش برهدف خورد</p></div>
<div class="m2"><p>چو مردان گوی از میدان بدر برد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شکسته قبضه اش در ترکتازی</p></div>
<div class="m2"><p>کمان خس چو طفلان را به بازی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کمان اندر شکستن دادش آواز</p></div>
<div class="m2"><p>چو بشکستی ز دست خود مینداز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کمان بشکست ار چه سهمناک است</p></div>
<div class="m2"><p>چو طالع یار باشد از آن چه باک است؟</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کمان بشکست بهر عقد بستن</p></div>
<div class="m2"><p>زهی بستن که زود آرد شکستن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شکستن می دهد پیوند را ساز</p></div>
<div class="m2"><p>چو پیوندی که هرگز نشکند باز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که دیده ست از شکستن شاد و خرم</p></div>
<div class="m2"><p>خریدار کمان و صاجش هم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عجب بود آن شکستن زو عجب تر</p></div>
<div class="m2"><p>تماشاگر حزین تر از کمان گر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نه تنها رام بشکست آن کمان را</p></div>
<div class="m2"><p>کمرهای همه نظاره گان را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جهانی مد عی را دل شکسته</p></div>
<div class="m2"><p>به چشم حاسدان تیری نشسته</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به بخت عشق عاشق کار خود کرد</p></div>
<div class="m2"><p>هوس را گرمی هنگامه شد سرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>روان گشتند رایان از حسد بیش</p></div>
<div class="m2"><p>ز بیگانه خجل شرمنده از خویش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مثل زن این مثل زان وقت بسته است</p></div>
<div class="m2"><p>که بگریز از کمان گرچه شکست است</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو زاغان زان کمان را پس ندیدند</p></div>
<div class="m2"><p>که دیگر ز آبروی خود رمیدند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جنک در بر کش ید و کرد اکرام</p></div>
<div class="m2"><p>کشیده قشقه بر پیشانی رام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>حمائل از گهر زنّ ارش افکند</p></div>
<div class="m2"><p>به دامادی خویشش ساخت خرسند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به نامش نامزد چون گشت سیتا</p></div>
<div class="m2"><p>فرستاد این خبر مژده پدر را</p></div></div>