---
title: >-
    بخش ۵۵ - جای دادن راون سیتا را در باغ اسلوک بن
---
# بخش ۵۵ - جای دادن راون سیتا را در باغ اسلوک بن

<div class="b" id="bn1"><div class="m1"><p>موکل بر پری زن دیوکی چند</p></div>
<div class="m2"><p>ستم رأی و ستمکار و ستم بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستم زین بیشتر نبود سزاوار</p></div>
<div class="m2"><p>که طفل م رده مادر، دایه کفتار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همای هم قفس با جیفه خواران</p></div>
<div class="m2"><p>چو گنجی هم قفس با تیره ماران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وزان بدتر کزآنها آن جگر سوز</p></div>
<div class="m2"><p>شنیدی قصۀ راون شب و روز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که هست او صاحب اقبال و خوش نام</p></div>
<div class="m2"><p>هزاران بنده نیکو دارد از رام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرش با فر و زیبِ تاج شاهی است</p></div>
<div class="m2"><p>روان فرمانش از مه تابه ماهی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غرض آن بود زان تزویز و زان ریو</p></div>
<div class="m2"><p>که گردد حور را دل مایلِ دیو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزار افسوس کز دستان و تلبیس</p></div>
<div class="m2"><p>شرف بر آدمی می جست ابلیس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تکلف کرده می گفتند گهگاه</p></div>
<div class="m2"><p>که تا همخوابۀ راون شود ماه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پری زان گفتگوها پنبه در گوش</p></div>
<div class="m2"><p>به حیرت سر فرو، گریان و خاموش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان گوش دلش مشتاق سیماب</p></div>
<div class="m2"><p>که چشم کبک بر رخسار مهتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زمین کندی به ناخن بلکه جان نیز</p></div>
<div class="m2"><p>نهان دیدی به سوی آسمان نیز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز حیرت خویشتن را ساخته گم</p></div>
<div class="m2"><p>گهی در گریه و گه در تب سم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو طاقت طاق شد مه را به یکبار</p></div>
<div class="m2"><p>برآورد از غم دل نالۀ زار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به مردن دل نهاده، کام ناکام</p></div>
<div class="m2"><p>بران شد تا بیفتد از لب بام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو راون شد ز حال آن بت آگاه</p></div>
<div class="m2"><p>به حیرت ماند زان بیتابی ماه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز عشق آن پری، عفریت خونخوار</p></div>
<div class="m2"><p>چو ابری بود خون باران به گلزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیِ تسکین آن گلدستۀ ناز</p></div>
<div class="m2"><p>به گلشن کرد جای بودنش ساز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ندانست این قدر ز افراط مستی</p></div>
<div class="m2"><p>گلی کا نرا ز گلبن بر شکستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرش در باغ جنّ ت جاگزینی</p></div>
<div class="m2"><p>بجز پژمرده اش هرگز نبینی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درآن باغی که بود اسلوک بن نام</p></div>
<div class="m2"><p>سمن را داد راون جای آرام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز اسباب نشاط و کامرانی</p></div>
<div class="m2"><p>مهیا داشت بیش از آنچه دانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگر کز عشق عقلش رفته بر باد</p></div>
<div class="m2"><p>که مرغ بسملی را دانه می داد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون آن بستانسرا زندان او شد</p></div>
<div class="m2"><p>چمن از رنگ و بو حیران او شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بهشتی گشت تضمین در گلستان</p></div>
<div class="m2"><p>در آمد شبچراغی در شبستان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز روی بلبلان شرمنده شد گل</p></div>
<div class="m2"><p>بران گل گشت عاشق تر ز بلبل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چمن می گفت زان نخل گل آگند</p></div>
<div class="m2"><p>که سرو ما به طوبی گشت پیوند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نهالان چمن زان شمع گلشن</p></div>
<div class="m2"><p>گرو برده ز نخلِ وادی ایمن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بود از عکس آن ماه جهانتاب</p></div>
<div class="m2"><p>مثال چاه نخشب حوضۀ آب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>درآن جنّت سرا حورِ غم اندود</p></div>
<div class="m2"><p>چو لاله وقف داغ و غرق خون بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بهار اندود کرده بوستان را</p></div>
<div class="m2"><p>زده برهم ره و رسم خزان را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چمن زو تازه او پژمرده هر دم</p></div>
<div class="m2"><p>گشاده بر دل از گل صد درِ غم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنان کاندر قفس مرغ خوش الحا ن</p></div>
<div class="m2"><p>ازو خوشوقت خلق و او به زندان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنان جان جهان کز غم به جان بود</p></div>
<div class="m2"><p>خزان خود، بهار دیگران بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو نخ جی ری که او در دام افتاد</p></div>
<div class="m2"><p>خود اندر ماتم و زو عید صیاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گدازان همچو شمع محفل افروز</p></div>
<div class="m2"><p>جهان زو روشن و او جمله تن سوز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نگویم سوز جان آن پری وش</p></div>
<div class="m2"><p>که ترسم زو شود طوفان آتش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به جان کندن گران بیمار می زیست</p></div>
<div class="m2"><p>به تسکین خیال یار می زیست</p></div></div>