---
title: >-
    بخش ۵۸ - آمدن رام بالای قلعۀ کوه و ملاقات کردن با سگریو به میانجی هنومنت
---
# بخش ۵۸ - آمدن رام بالای قلعۀ کوه و ملاقات کردن با سگریو به میانجی هنومنت

<div class="b" id="bn1"><div class="m1"><p>چو در رکه مونک آمد رام دلخون</p></div>
<div class="m2"><p>خبر بردند بر سگریو میمون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برادر بال با او بود دشمن</p></div>
<div class="m2"><p>هلاکش را همی انگیختی فن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کنکش با وزیران گفت آن راز</p></div>
<div class="m2"><p>که بر من شد در فتنه ز نو باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شنیدم دو جوان پیل پیکر</p></div>
<div class="m2"><p>مسلح گشته شیران دلاور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سراغ ما همی پرسند هر جا</p></div>
<div class="m2"><p>به زیر کوه ما دارند مأوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمان سخت و سنانها تیز دارند</p></div>
<div class="m2"><p>مگر با من سرِ خونریز دارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بختم جان دشمن گشت مسکن</p></div>
<div class="m2"><p>که می خیزند ازو زین گونه دشمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همانا دوستدار بال هسنند</p></div>
<div class="m2"><p>کمین جویان به کین من نشستند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مناسب نیست نادانسته پیکار</p></div>
<div class="m2"><p>چه باید کرد تدبیر چنین کار؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر گیرم که سیاح و فقیرند</p></div>
<div class="m2"><p>به نفسانیت خود چون اسیرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرا خود باسلاح جنگ گردند</p></div>
<div class="m2"><p>یقین بر دشمنی آهنگ کردند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز اهل مشورت با او هنومان</p></div>
<div class="m2"><p>سخن سنجیده گفت و خواست فرمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که اول رفته حال شان بدانم</p></div>
<div class="m2"><p>ز لوحِ جبهه راز دل بخوانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به صدق شان دلم چون گردد آگاه</p></div>
<div class="m2"><p>بیارم همچو دولت بر در شاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همانجا ورنه از تدبیر دیگر</p></div>
<div class="m2"><p>به آسانی بلا در سازم از سر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به سوی رام پایین آمد از کوه</p></div>
<div class="m2"><p>ز کوه عیش سوی کوه اندوه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به لب کرد از زمین بوسش تیمم</p></div>
<div class="m2"><p>اجازت یافت زان پس درتکلم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس از نام و نسب تقریب پرسید</p></div>
<div class="m2"><p>ز هر حرفش هزاران نکته می چید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برو برخواند رام از روز اول</p></div>
<div class="m2"><p>حساب دفتر غم‌ها مفصل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سخن سر می زد از خون دیده عاشق</p></div>
<div class="m2"><p>ز روی راست همچون صبح صادق</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر چه یافت در صدقش یگانه</p></div>
<div class="m2"><p>یقین را کرد کاری عاقلانه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قسم را آتشی افروخت در دشت</p></div>
<div class="m2"><p>گرفته دستشان بر گرد می گشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که رام و لچمن و سگریو ز امروز</p></div>
<div class="m2"><p>به عهد دوستی باشند دلسوز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز یاران راز پنهانی نیوشند</p></div>
<div class="m2"><p>به کار یکدگر با جان بکوشند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دگر گفتا بیا برخیر اکنون</p></div>
<div class="m2"><p>به جان دریاب مهر شاه میمون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دل ازعهدش چوتسکین یافت ز اندوه</p></div>
<div class="m2"><p>بر آمد بر فراز قلۀ کوه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هنومن شد میانجی بهر پیوند</p></div>
<div class="m2"><p>جهانبان با جهانبان گشت خرسند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بنای یکدلی چون گشت محکم</p></div>
<div class="m2"><p>زد آن شوریده سر زان راز محرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که از هجران سیتا دل خرابم</p></div>
<div class="m2"><p>سیه روزم که گم شد آفتابم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز دستم دل شد واز دست دل جان</p></div>
<div class="m2"><p>زدم دستی به دامان عزیزان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>درونم ریش گشت و دیده خونبار</p></div>
<div class="m2"><p>مدد باید ز یاران در چنین کار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جوابش داد میمون با دلاسا</p></div>
<div class="m2"><p>که روزی در هوا دیدم تماشا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زنی را مو کشان دیوی ز جا برد</p></div>
<div class="m2"><p>ندانم لیکن آخر تا کجا برد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مزعفَر بود رنگ پرنیانش</p></div>
<div class="m2"><p>شنیدم نام تو نیز از زبانش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فکنده جامه ای بر مسکن من</p></div>
<div class="m2"><p>چنان دانم که سیتا باشد آن زن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر یکچند با صبرت بود کار</p></div>
<div class="m2"><p>توانم گشتن از حالش خبردار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فرستم لشکر خود را به هر جا</p></div>
<div class="m2"><p>رسانندت خبر زان ماه سیما</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به رام آن زرد جامه نیز بنمود</p></div>
<div class="m2"><p>ز چشمش خون به رویش زردی افزود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>قصب بی ماه دید و حله بی حور</p></div>
<div class="m2"><p>دلش بی صبر مانده دیده بی نور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز غم بی اختیارش بود تر چشم</p></div>
<div class="m2"><p>نهاد آن زعفرانی جامه بر چشم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو پنداری که کرد آن درد بر درد</p></div>
<div class="m2"><p>علاج درد چشم از جامۀ زرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز بس بی طاقتی از پا در افتاد</p></div>
<div class="m2"><p>صدای ناله اندر کوه در داد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دل سگریو نیز از سوز او سوخت</p></div>
<div class="m2"><p>چراغ زنده، شمع مرده افروخت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز دردش گشت میمون تازه زنبور</p></div>
<div class="m2"><p>فشاند الماس بر دیرینه ناسور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شعاع برق آهش برجگر تافت</p></div>
<div class="m2"><p>به داغ عاشقی همدرد خود یافت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بگفتا همچو تو دارم دل ریش</p></div>
<div class="m2"><p>تو از بیگانه می نالی، من از خویش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دلم بسته چو میمون در ببسته</p></div>
<div class="m2"><p>نه خود رسته نه کس بندش گسسته</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز دستت اهرمن بربود دلبر</p></div>
<div class="m2"><p>مرا شد اهرمن، بالِ برادر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر داد من از دشمن ستانی</p></div>
<div class="m2"><p>مرا بر آرزوی دل رسانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به هر تدبیر کان دانم ز هر جا</p></div>
<div class="m2"><p>در آغوشت نشانم حور سیتا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر راون به لنکا برده باشد</p></div>
<div class="m2"><p>و یا مه بر ثرّیا برده باشد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به هر تقدیر بر من هست آسان</p></div>
<div class="m2"><p>صنم را در کنار خویشتن دان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هم اکنون لیکن ای شیر قوی دست</p></div>
<div class="m2"><p>به کین بال می باید کمر بست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز تقریر سخن چون شد مقرر</p></div>
<div class="m2"><p>که قتل بال می خواهد برادر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سخن مشرو ح پرسید آن جهانجو</p></div>
<div class="m2"><p>ز حال سرگذشت بال با او</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>که تقریب خصومت در میان چیست</p></div>
<div class="m2"><p>بیآگاهم، ستم از جانبی کیست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اگر دانم که از خصمست تقصیر</p></div>
<div class="m2"><p>توان دل جمع کردن زو به یک تیر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>وگر نبود به جرم او گواهی</p></div>
<div class="m2"><p>نشاید ریخت خون بی گناهی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز صدق نیت او گشت آگاه</p></div>
<div class="m2"><p>پس از تحسین جوابش داد دلخواه</p></div></div>