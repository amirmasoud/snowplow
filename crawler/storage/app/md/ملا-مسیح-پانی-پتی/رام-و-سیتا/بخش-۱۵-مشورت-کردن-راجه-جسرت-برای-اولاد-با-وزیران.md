---
title: >-
    بخش ۱۵ - مشورت کردن راجه جسرت برای اولاد با وزیران
---
# بخش ۱۵ - مشورت کردن راجه جسرت برای اولاد با وزیران

<div class="b" id="bn1"><div class="m1"><p>شبی از دور بینی خلوت آراست</p></div>
<div class="m2"><p>ز هر یک اهل دانش مشورت خواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآمد از دل جسرَت دم سرد</p></div>
<div class="m2"><p>حساب عمر خود پیرانه سر کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که عمرم را شمار از صد فزون است</p></div>
<div class="m2"><p>درین اندیشه جانم غرق خون است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارم هیچ فرزندی ولیعهد</p></div>
<div class="m2"><p>که بعد از من کند ناموس را جهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه دولتی دارم کماهی</p></div>
<div class="m2"><p>معطّل می گذارم کار شاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شما هریک نکو خواهید دانا</p></div>
<div class="m2"><p>بگوئید آنچه باید کرد ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز جا جنبید ازان دانش پژوهان</p></div>
<div class="m2"><p>خرد سنجیده پیری مصلحت دان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمین بوسید، جسرت را دعا کرد</p></div>
<div class="m2"><p>به جان وام دعای او ادا کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زبان بگشاد و گفت: ای صاحب اقبال</p></div>
<div class="m2"><p>که گیرد ماه و مهر از روی ت و فال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازین پس غم مباش از بهر فرزند</p></div>
<div class="m2"><p>بدین تدبیر خاطر دار خرسند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا این نکته نقل از چار بید است</p></div>
<div class="m2"><p>علاج نسل تو جگ اسمید است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بعد جگ پی هوم آتشی سوز</p></div>
<div class="m2"><p>چراغ خاندان زان آتش افروز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به تحسینش همه کس لب گشادند</p></div>
<div class="m2"><p>قرار مصلحت بر جگ بدادند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز خلوت رای چون بر تخت بنشست</p></div>
<div class="m2"><p>گره بستش به جان و ز جان کمربست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وزیر ملک را نزدیک خود خواند</p></div>
<div class="m2"><p>ز لب بر فرق بختش گوهر افشاند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که ما را جگ اسمید است در پیش</p></div>
<div class="m2"><p>ترا کردم وکیل مطلق خویش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو همت شو درین کارم مددگار</p></div>
<div class="m2"><p>که همت کرد بر من واحب این کار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سپردش پس کلید گنج خانه</p></div>
<div class="m2"><p>اضافه داد همت بر خزانه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که از آب و سروج آن روی جا کن</p></div>
<div class="m2"><p>به نذر جگ شهری نو بنا کن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رها کردند پس اسپِ سیه گوش</p></div>
<div class="m2"><p>به دنبالش دلیران ظفرکوش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مزّین بر گلویش لوحی از زر</p></div>
<div class="m2"><p>چو ماه چارده در شب منور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر آن زر لوح نام رای مسطور</p></div>
<div class="m2"><p>جو بر خورشید تابان سکّۀ نور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نوشته بعد نامش کین جنیبت</p></div>
<div class="m2"><p>به رسم جگ سر داده ست جسرت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر آن رایی که بر جنگش بود رأی</p></div>
<div class="m2"><p>ببندد اسپ و او گردد صف آرای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر آن کس صلح شد معقول خاطر</p></div>
<div class="m2"><p>دهد باج و شود در جگ حاضر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نیامد کس ز حکم رای بیرون</p></div>
<div class="m2"><p>مطیع حکم او شد ربع مسکون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به سالی ربع مسکون را بپیمود</p></div>
<div class="m2"><p>نبستش هر که را یارا نه آن بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به هفت اقلیم گشت و باز گردید</p></div>
<div class="m2"><p>نشسته رای، راه او همی دید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خراج جسرت آوردند بر سر</p></div>
<div class="m2"><p>چه خاقان و چ ه فغفور و چه قیصر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ستاده بر سریرش تاجداران</p></div>
<div class="m2"><p>چو نرگس زار در فصل بهاران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در آن مدت که ماند اسبش به جولان</p></div>
<div class="m2"><p>بنای شهر نو آمد به پایان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هماندم رای با اس ب جهانگرد</p></div>
<div class="m2"><p>به دولت شهر نو را تختگه کرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چه شهر نو که خرم بوستانی</p></div>
<div class="m2"><p>نشاط خوشدلی را نوجهانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>منقّش کاخهای آن زر اندود</p></div>
<div class="m2"><p>در و دیوار مشک و عنبر اندود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نشسته جسرت اندر جگ مربع</p></div>
<div class="m2"><p>به تخت زر به سرتاج مرصع</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز هفت اقلیم هفتاد و دو ملّت</p></div>
<div class="m2"><p>رسیدند از صلای عام جسرت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ضیافت کرد گیتی را به احسان</p></div>
<div class="m2"><p>که گشت از ذره تا خورشید مهمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر آن نعمت که در خوان جهان بود</p></div>
<div class="m2"><p>نخوانده پیش هر کس داشت موجود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز کار نان دهی چون دل بپرداخت</p></div>
<div class="m2"><p>برای گنج بخشی انجمن ساخت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همی بارید یکسان تا که بودش</p></div>
<div class="m2"><p>بفرق مور و جم باران جودش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز بس زد بحر دستش موج گوهر</p></div>
<div class="m2"><p>بر آورد آرزوی هفت کشور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز جود عام او در ربع مسکون</p></div>
<div class="m2"><p>گدا هم کیسه شد با گنج قارون</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نمانده در جهان محتاج یک کس</p></div>
<div class="m2"><p>بجز کودک به شیر مادر و بس</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به رأی رای پس پیر برهمن</p></div>
<div class="m2"><p>به رسم هوم آتش کرد روشن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بر آتش مجتمع از کار دانان</p></div>
<div class="m2"><p>همه آتش پرست و بید خوانان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به کار جگ هر یک پا فشردند</p></div>
<div class="m2"><p>بخور هوم آتش را سپردند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به حکم چار بید آتش پرستان</p></div>
<div class="m2"><p>چو اس ب جگ را کردند قربان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در آتش سوختند آن اسب موجود</p></div>
<div class="m2"><p>که رسم جگ اسمید این چنین بود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز آتش جلوه گر شد شخصی از نور</p></div>
<div class="m2"><p>فروغ طلعتش چون آتش طور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تنش در شعله چون یاقوت شاداب</p></div>
<div class="m2"><p>سراپا آتشین چون کرم شب تاب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز آتش کرده پیراهن تن او</p></div>
<div class="m2"><p>چو خورشید و شفق پیراهن او</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به جسرت گفت خیز ای رای برگیر</p></div>
<div class="m2"><p>به جام زر برنج پخته در شیر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که این پسخوردهٔ روحانیان است</p></div>
<div class="m2"><p>به جسم آرزوی مرده، جانست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>برو دیگر ز نومیدی مکش آه</p></div>
<div class="m2"><p>که جگ تو شده مقبول درگاه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>طفیل جگ چو این آتش فروزی</p></div>
<div class="m2"><p>ترا خود چار فرزند است روزی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چنان طالع شوند این چار اختر</p></div>
<div class="m2"><p>که ماند نام تو تا روز محشر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو جسرت دید آن نور نهانی</p></div>
<div class="m2"><p>ز آتش یافت آب زندگانی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تواضع کرد و جام شیر بگرفت</p></div>
<div class="m2"><p>تبرّک چون مرید از پیر بگرفت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز بس شادی نمانده بر زمین پای</p></div>
<div class="m2"><p>سبک بر جست و کرد اندر حرم جای</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سه بانوی کلان را پیش خود خواند</p></div>
<div class="m2"><p>به صد جان در کنار خویش بنشاند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به دست خویش کرد آن شیر تقسیم</p></div>
<div class="m2"><p>یکی را نیم دو را نیمۀ نیم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همان شب هر سه مه را ماند امید</p></div>
<div class="m2"><p>به نه مه بارها چیدند از بید</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نخستین کوسلا شد مشرف رام</p></div>
<div class="m2"><p>دمید از کیکیی ماهی برت نام</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سویرا چون در گنجینه بگشاد</p></div>
<div class="m2"><p>سترگن را به لچمن توأمان داد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به طفلی کین چهار ارکان دولت</p></div>
<div class="m2"><p>توانا می شدند از ناز و نعمت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دل لچمن شدی از رام خرسند</p></div>
<div class="m2"><p>سترگن ۵ را به برت افتاد پیوند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به هر جا رام با اقبال می رفت</p></div>
<div class="m2"><p>چو سایه لچمن از دنبال می رفت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به بازی دو به دو هرچار نازان</p></div>
<div class="m2"><p>همی کردند بازی ناز بازان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>که آمد خال برت از مللک پنجاب</p></div>
<div class="m2"><p>به جسرت گفت کای رای ظفریاب</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به من بسپار خواهرزاد هٔ من</p></div>
<div class="m2"><p>که در پنجاب استادان پرفن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به جان تعلیم علم و دانش و دین</p></div>
<div class="m2"><p>کنند آن سان که گوید خلق تحسین</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>جوان گردد کند کسب سعادت</p></div>
<div class="m2"><p>به پیش رای آید بهر خدمت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سوی پنجاب از فرمودهٔ شاه</p></div>
<div class="m2"><p>برت رفت و سترگن نیز به همراه</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در آن کشور هلالش گشت مهتاب</p></div>
<div class="m2"><p>قوی سرپنجه گشت از آب پنجاب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز حال برت تا کی گویم اینجا</p></div>
<div class="m2"><p>من و گفتار عشق رام و سیتا</p></div></div>