---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>خدا ز سرو قد او مرا جدا نکند</p></div>
<div class="m2"><p>من و جدایی ازان سرو قد خدا نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بصوت ناله نهفتم صدای سیل سرشک</p></div>
<div class="m2"><p>که شرح راز دلم پیش کس ادا نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو استخوان نشانه چه مرده باشد</p></div>
<div class="m2"><p>که ناوک تو رسد جان خود جدا نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرفت آینه طبع را غبار الم</p></div>
<div class="m2"><p>چه گونه دل طلب جام غم زدا نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صدای نی همه در دست نیست باد کسی</p></div>
<div class="m2"><p>که کسب و جد ز ادراک این صدا نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی که دست ارادت بپیر عشق نداد</p></div>
<div class="m2"><p>بهیچ مرشدی آن به که اقتدا نکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی از تو اگر یار غافلست مرنج</p></div>
<div class="m2"><p>شهی چه باشد اگر رغبت گدا نکند</p></div></div>