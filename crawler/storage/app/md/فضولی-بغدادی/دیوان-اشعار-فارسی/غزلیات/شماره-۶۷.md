---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>پیش عاقل قصه درد من و مجنون یکیست</p></div>
<div class="m2"><p>اختلافی در سخن باشد ولی مضمون یکیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ دل را خواستم مرهم رساندی ناوکی</p></div>
<div class="m2"><p>غالبا پنداشتی داغ دل پرخون یکیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرط حسنست آن که طاق آن خم ابرو دو تاست</p></div>
<div class="m2"><p>رسم خطست این که خال آن لب میگون یکیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جای سرو قامتت در جان نمی گیرد الف</p></div>
<div class="m2"><p>حاش لله کی الف با آن قد موزون یکیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست در دل غیر تو زان رو عزیزی بر دلم</p></div>
<div class="m2"><p>قدر دارد در صدف هر گه در مکنون یکیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیستم بلبل که هر ساعت نهم دل بر گلی</p></div>
<div class="m2"><p>زین همه گل چهره مقصود من محزون یکیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر من هر که می بیند فضولی دود آه</p></div>
<div class="m2"><p>آن تصور می کند کین هم ز نه گردون یکیست</p></div></div>