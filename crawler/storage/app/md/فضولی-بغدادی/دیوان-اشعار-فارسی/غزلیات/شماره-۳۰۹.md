---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>در دل زار غمی ز آن لب می گون دارم</p></div>
<div class="m2"><p>چه کنم آه چه سازم دل پرخون دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر من ای شمع مزن خنده که سرمایه عشق</p></div>
<div class="m2"><p>گر سرشکست و فغان من ز تو افزون دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من اسیر غم دل ماندم و مجنون فرسود</p></div>
<div class="m2"><p>تاب من بر غم دل بیش ز مجنون دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوختم قطره آبی نزدم بر آتش</p></div>
<div class="m2"><p>گر چه از اشک وطن در دل جیحون دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال بودست مرا بد همه وقت ولی</p></div>
<div class="m2"><p>هرگز این حال نبودست که اکنون دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه ز درد تو کنم ناله گه از طعن رقیب</p></div>
<div class="m2"><p>وه که اندوه درون و غم بیرون دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه آن ماه جفا کرد فضولی بر من</p></div>
<div class="m2"><p>من ندارم گله از ماه ز گردون دارم</p></div></div>