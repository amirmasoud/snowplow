---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>چو از غم کنم چاک پیراهنم را</p></div>
<div class="m2"><p>ز مردم کند اشک پنهان تنم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه سان با قد خم کنم عزم کویش</p></div>
<div class="m2"><p>گرفتست خار مژه دامنم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمت دانه ها می فشاند ز چشمم</p></div>
<div class="m2"><p>بباد فنا می دهد خرمنم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیامد ز دست تو ای من غلامت</p></div>
<div class="m2"><p>که در طوق ساعد کشی گردنم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هر سو ره آرزو بست بر من</p></div>
<div class="m2"><p>سرشکم که بگرفت پیرامنم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مبین محتسب تند در ساغر می</p></div>
<div class="m2"><p>مکن تیره آیینه روشنم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز غم مرده ام ماتم خویش دارم</p></div>
<div class="m2"><p>فضولی ملامت مکن شیونم را</p></div></div>