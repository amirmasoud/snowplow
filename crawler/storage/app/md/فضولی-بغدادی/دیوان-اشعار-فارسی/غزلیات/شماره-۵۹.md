---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>کی توانم رست در کویت ز غوغای رقیب</p></div>
<div class="m2"><p>می کند فریاد سگ هر گه که می بیند رقیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصل تو می خواهم ولی مشکل که بندد صورتی</p></div>
<div class="m2"><p>نیست اهل درد را از خوان وصل او نصیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتشی از شوق گل در دل ندارد غالبا</p></div>
<div class="m2"><p>چون نمی سوزد قفس را نالهای عندلیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می نهد بر دست من دست از پی تشخیص لیک</p></div>
<div class="m2"><p>کی علاج درد من می آید از دست طبیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که دم از دولت قرب سلاطین می زنی</p></div>
<div class="m2"><p>نیست ممکن این که آزاری نبینی عنقریب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه دل از جان می کند او را نهان گه جان ز دل</p></div>
<div class="m2"><p>نیست آن گل چهره در هر جا که باشد بی رقیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست مقصود دل بیچاره غیر از وصل یار</p></div>
<div class="m2"><p>فاستجب ما قد دعا عبد ضعیف یا مجیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چند می پرسی فضولی بر که واله گشته</p></div>
<div class="m2"><p>والهم کی می رسد بر خاطرم نام حبیب</p></div></div>