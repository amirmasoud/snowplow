---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>عکس لبت نمود دلم کرد خون قدح</p></div>
<div class="m2"><p>دردا که کرد سوز درونم فزون قدح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر باد داد رخت سرای سلامتم</p></div>
<div class="m2"><p>یارب که چون حباب شود سرنگون قدح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش تو می کند رقم صفحه درون</p></div>
<div class="m2"><p>پا می نهد ز دائره خود برون قدح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردد مدام کف بلب آورده هر طرف</p></div>
<div class="m2"><p>بر عقل روشن است که دارد جنون قدح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انصاف بر صفای دل صافیش که کرد</p></div>
<div class="m2"><p>بهر لب تو ترک می لاله گون قدح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صیاد هوش و ره زن عقل است غالبا</p></div>
<div class="m2"><p>آموختست از لب لعلت فسون قدح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر از قدح مجوی فضولی مصاحبی</p></div>
<div class="m2"><p>زیرا که آگه است ز راز درون قدح</p></div></div>