---
title: >-
    شمارهٔ ۳۱۹
---
# شمارهٔ ۳۱۹

<div class="b" id="bn1"><div class="m1"><p>گر چشم برخسار تو صد بار گشادم</p></div>
<div class="m2"><p>هر بار دو صد سیل برخسار گشادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد کنان راز دلم پیش تو بگشاد</p></div>
<div class="m2"><p>هر سیل که از دیده خونبار گشادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه از تو که ناگفته باغیار گشادی</p></div>
<div class="m2"><p>هر راز که پیش تو من زار گشادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چشم گشادم بتو دیدم ز تو آزار</p></div>
<div class="m2"><p>فریاد که بر خود در آزار گشادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بختم بنگر در ره او کز پی راحت</p></div>
<div class="m2"><p>در رهگذر سیل فنا بار گشادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر سینه خراشیدم و خستم عجبی نیست</p></div>
<div class="m2"><p>راهی بدل از بهر غم یار گشادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کارم گرهی داشت از آن زلف فضولی</p></div>
<div class="m2"><p>آن زلف گرفتم گره از کار گشادم</p></div></div>