---
title: >-
    شمارهٔ ۲۱۵
---
# شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>ز عشقت ناله زاری که من دارم ندارد کس</p></div>
<div class="m2"><p>همین بس کار من کاری که من دارم ندارد کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه باشد گر نباشد دردی و داغی چو من کس را</p></div>
<div class="m2"><p>نگار لاله رخساری که من دارم ندارد کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترحم می کند بر حال من هر کس که می بیند</p></div>
<div class="m2"><p>چنین بی رحم دلداری که من دارم ندارد کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحال خود ندیدم هیچکس را در پریشانی</p></div>
<div class="m2"><p>چه حالست این مگر یاری که من دارم ندارد کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل شادی کزان گل غیر من دارد ندارم من</p></div>
<div class="m2"><p>بدل از جور او خاری که من دارم ندارد کس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ره و رسم اسیران بلا بسیار پرسیدم</p></div>
<div class="m2"><p>غم و اندوه بسیاری که من دارم ندارد کس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی هست وصف حسن او مضمون گفتارم</p></div>
<div class="m2"><p>از آن رو حسن گفتاری که من دارم ندارد کس</p></div></div>