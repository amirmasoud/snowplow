---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>نشاطم می‌کشد چون از تنم پیکان برون آید</p></div>
<div class="m2"><p>که شاید دامن پیکان گرفته جان برون آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخواهم ماند زنده چون نجاتم دادی از هجران</p></div>
<div class="m2"><p>بمیرد هر شرر کز آتش سوزان برون آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غباری کان مقیم درگهت تا شد نمی‌خواهد</p></div>
<div class="m2"><p>که گردد آدم وزان روضه رضوان برون آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر چشمی که آید همچو دود از اشک تر سازد</p></div>
<div class="m2"><p>سیه‌بختی که او از آتش هجران برون آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یاد غنچه خندان او مردم عجب نبود</p></div>
<div class="m2"><p>که از خار مزاحم غنچه خندان برون آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخواهد برد وقت مرگ اجل از سینه‌ام جز غم</p></div>
<div class="m2"><p>به خانه هرچه باشد چون بکاوند آن برون آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی هست در دل تیر او بسیار می‌ترسم</p></div>
<div class="m2"><p>که با سیلاب خون از دیده چون مژگان برون آید</p></div></div>