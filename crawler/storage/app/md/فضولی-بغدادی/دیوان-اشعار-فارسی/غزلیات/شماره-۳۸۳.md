---
title: >-
    شمارهٔ ۳۸۳
---
# شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>پی ماتم میان انجمن ای ماه جا کردی</p></div>
<div class="m2"><p>ز غیرت باز بر من شهر را ماتم سرا کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا گفتی مکن افغان که فردا خواهمت کشتن</p></div>
<div class="m2"><p>شب قتلست منعم از فغان کردن چرا کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه بود از خاک آن در دور کردی کشتگانت را</p></div>
<div class="m2"><p>شهیدان را چرا بیرون ز خاک کربلا کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زدی در رنگ ماتم گاه بر سر گاه بر سینه</p></div>
<div class="m2"><p>رساندی ظلم تا حدی که بر خود هم جفا کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدی عاشوریان را شمع محفل چون نمیرم من</p></div>
<div class="m2"><p>چه باشد بهتر از مردن تو چون میل عزا کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگریه آب دادی سبزه خاک شهیدان را</p></div>
<div class="m2"><p>اسیران بلا را کشته تیغ وفا کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی در ره او کشته تیغ جفا گشتی</p></div>
<div class="m2"><p>عفاک الله شهید کربلا را اقتدا کردی</p></div></div>