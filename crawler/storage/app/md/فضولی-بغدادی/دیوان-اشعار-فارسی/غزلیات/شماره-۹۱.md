---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>بر جان ما جفای نکویان ز حد گذشت</p></div>
<div class="m2"><p>اوقات ما میانه این قوم بد گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوز و گداز شمع ز رشک جمال تست</p></div>
<div class="m2"><p>رست از همه عذاب کسی کز حسد گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشمرد از سکان خودم هیچ دلبری</p></div>
<div class="m2"><p>بر من ز دلبران ستم بی عدد گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خون نشست مردم چشمم ز آرزو</p></div>
<div class="m2"><p>هر گه که در خیال من آن خال و خد گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشتم مقید غم عشق تو از ازل</p></div>
<div class="m2"><p>هرگز نمی توانم ازین تا ابد گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمرم گذشت لیک ندارم تأسفی</p></div>
<div class="m2"><p>شادم باین که در غم آن سرو قد گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذوق وصال او ز فضولی دریغ نیست</p></div>
<div class="m2"><p>اما بشرط آنکه تواند ز خود گذشت</p></div></div>