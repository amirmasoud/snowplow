---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>سر مکش از من که از من درد سر خواهی کشید</p></div>
<div class="m2"><p>هر دم از آه من آزار دگر خواهی کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چاره بیدرایم کن ور نه از افغان من</p></div>
<div class="m2"><p>محنت بیداری از من بیشتر خواهی کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برنخواهم داشتن ای شمع چشم از قامتت</p></div>
<div class="m2"><p>گر تو میل آتشم بر چشم تر خواهی کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انتظاری می کشم عمریست تا دانسته ام</p></div>
<div class="m2"><p>کآفتابی تیغ بر اهل نظر خواهی کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سینه را پیش از گریبان چاک خواهم زد اگر</p></div>
<div class="m2"><p>دامن از دست من خونین جگر خواهی کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش دلم زین رهگذر گر لطف می آیی برم</p></div>
<div class="m2"><p>آه اگر روزی قدم زین رهگذر خواهی کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محنت خوبان فضولی نیست در دنیا همین</p></div>
<div class="m2"><p>روز محشر هم عذابی زین بتر خواهی کشید</p></div></div>