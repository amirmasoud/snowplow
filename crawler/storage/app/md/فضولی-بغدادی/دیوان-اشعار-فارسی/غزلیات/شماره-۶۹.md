---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>سرو را همچو قدت شیوه رعنایی نیست</p></div>
<div class="m2"><p>این قدر هست که او مثل تو هرجایی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو و گل تا ز قد و روی تو دیدند شکست</p></div>
<div class="m2"><p>باغبان را سر و برگ چمن آرایی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همدمی چون غم او نیست دم تنهایی</p></div>
<div class="m2"><p>هر کرا هست غم او غم تنهایی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل از عاشقی از طعنه میندیش که ذوق</p></div>
<div class="m2"><p>نتوان یافت ز عشقی که برسوایی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که داری سر سودای تجارت بی نفع</p></div>
<div class="m2"><p>هیچ سرمایه به از جوهر دانایی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبر در عشق تو کاریست پسندیده ولی</p></div>
<div class="m2"><p>کرده ام تجربه کار من شیدایی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشو از دیده خونبار فضولی غایب</p></div>
<div class="m2"><p>که درو بی گل روی تو شکیبایی نیست</p></div></div>