---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>تا بوده ایم همدم غم بوده ایم ما</p></div>
<div class="m2"><p>غم را ملازم همه دم بوده ایم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم را ز من نبوده جدایی مرا ز غم</p></div>
<div class="m2"><p>هر جا که بوده ایم بهم بوده ایم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش از وجود با غم لعل تو عمرها</p></div>
<div class="m2"><p>همراز تنگنای عدم بوده ایم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بر کمان ابروی تو بسته ایم دل</p></div>
<div class="m2"><p>دایم نشان تیر ستم بوده ایم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز نگشته است کم از ما بلای تو</p></div>
<div class="m2"><p>یک لحظه بی بلای تو کم بوده ایم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر جا نهاده ایم قدم در ره نیاز</p></div>
<div class="m2"><p>افتاده تر ز خاک قدم بوده ایم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکدم نبوده ایم فضولی بکام دل</p></div>
<div class="m2"><p>پیوسته مبتلای الم بوده ایم ما</p></div></div>