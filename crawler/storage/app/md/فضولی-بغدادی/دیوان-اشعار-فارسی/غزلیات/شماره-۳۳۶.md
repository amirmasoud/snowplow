---
title: >-
    شمارهٔ ۳۳۶
---
# شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>دوش در مجلس نگاری بود همزانوی من</p></div>
<div class="m2"><p>عیشها می کرد دل تا صبح از پهلوی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار وحشی طبع و من معتاد الفت چون کنم</p></div>
<div class="m2"><p>آفت من خوی او شد محنت او خوی من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند می نازی فلک با ماه نو چندین مناز</p></div>
<div class="m2"><p>باش تا پیدا شود ماه هلال ابروی من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر رهش افتاده ام بگشاده چشم انتظار</p></div>
<div class="m2"><p>وه چه باشد گر کند گاهی نگاهی سوی من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون توانم بی بلا باشم چنین کز هر طرف</p></div>
<div class="m2"><p>صد بلا آویخته بی زلفت از هر موی من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوی من مگذر مبادا سر نهم بی اختیار</p></div>
<div class="m2"><p>بر رهت پای تو آزاری کشد از روی من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش ای شه فضولی هم غلام تست گفت</p></div>
<div class="m2"><p>کیست او کی آمد و جان یافت جا در کوی من</p></div></div>