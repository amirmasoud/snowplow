---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>ما نظر جز بر تبان سیمبر کم کرده ایم</p></div>
<div class="m2"><p>وز بتان سیمبر قطع نظر کم کرده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهدا از ما مجو بسیار آیین صلاح</p></div>
<div class="m2"><p>عشق بازانیم ما کار دگر کم کرده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده ایم اندیشه بسیار در هر کار لیک</p></div>
<div class="m2"><p>فکری از سودای خوبان خوبتر کم کرده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بلای عشق در راه وفای گلرخان</p></div>
<div class="m2"><p>گر چه بیش از پیش هم باشد حذر کم کرده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیم اشک و روی چون زر بر رهت افکنده ایم</p></div>
<div class="m2"><p>ما فقیرانیم جمع سیم و زر کم کرده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست ملک سلطنت را اعتباری پیش ما</p></div>
<div class="m2"><p>شاهبازانیم صید مختصر کم کرده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد فضولی شهره عالم حدیث عشق ما</p></div>
<div class="m2"><p>گر چه زین راز نهان کس را خبر کم کرده ایم</p></div></div>