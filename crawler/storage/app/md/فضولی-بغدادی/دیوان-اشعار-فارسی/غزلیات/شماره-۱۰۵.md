---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>بی لبت قطع نظر کرده ام از آب حیات</p></div>
<div class="m2"><p>دارد از شام غمت آب حیاتم ظلمات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت با درد وغمت صبر و ثباتم از دل</p></div>
<div class="m2"><p>غم و درد تو بدل شد بدل صبر و ثبات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیوه مهر و وفا از تو نمی باید خواست</p></div>
<div class="m2"><p>چون توان خواست صفاتی که نباشد در ذات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه چنان بسته بزنجیر بلایت شده ام</p></div>
<div class="m2"><p>که توانم گذرانید بدل فکر نجات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما فقیریم تو سلطان چه عجب گر ما را</p></div>
<div class="m2"><p>بترحم رسد از خرمن حسن تو زکات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچنان ساخته ضعفم که اگر بحث کنند</p></div>
<div class="m2"><p>نتوانم که کنم هستی خود را اثبات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت آنست فضولی که ز غم باز رهم</p></div>
<div class="m2"><p>چند در غم گذرد بی رخ یارم اوقات</p></div></div>