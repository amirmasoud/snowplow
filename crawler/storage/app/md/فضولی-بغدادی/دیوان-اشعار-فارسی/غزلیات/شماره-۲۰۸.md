---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>دلم از عشق تو رسوای جهانست امروز</p></div>
<div class="m2"><p>غم پنهان دلم بر تو عیانست امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزگار من اگر گشت سیه نیست عجب</p></div>
<div class="m2"><p>آفتاب رخت از دیده نهانست امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدتی خون دلم داشت اقامت در چشم</p></div>
<div class="m2"><p>پی آن سرو سفر کرده روانست امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده بودم خم گیسوی تو امشب در خواب</p></div>
<div class="m2"><p>این که دارم دل آشفته ازانست امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش دل گوشه چشمی ز تو در یافته بود</p></div>
<div class="m2"><p>بهمان گوشه چشمی نگرانست امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز می و مغبچه یارب چه طرب یافته است</p></div>
<div class="m2"><p>که دلم معتکف دیر مغانست امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوش کردند سکان منع فضولی ز درت</p></div>
<div class="m2"><p>سبب اینست که با آه و فغانست امروز</p></div></div>