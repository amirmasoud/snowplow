---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>زبان مرغ می داند مگر گل</p></div>
<div class="m2"><p>که دارد گوش بر فریاد بلبل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر جانی ندارد گل که دارد</p></div>
<div class="m2"><p>بآه بلبلان چندین تحمل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عاشق می فزاید قدر معشوق</p></div>
<div class="m2"><p>نه از بسیاری جاه و تجمل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگار من مکن بی التفاتی</p></div>
<div class="m2"><p>مزن بر عاشقان تیغ تغافل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر خواهی که بگشاید دل ما</p></div>
<div class="m2"><p>بر افشان زلف یا بگشای کاکل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشرط صبر بر غم می توان یافت</p></div>
<div class="m2"><p>گل مقصد ز گلزار توکل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توکل را فضولی کار فرما</p></div>
<div class="m2"><p>مکن کاری به تدبیر و تأمل</p></div></div>