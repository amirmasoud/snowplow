---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>نمی‌خواهم به او درد دل صد پاره بنویسم</p></div>
<div class="m2"><p>که می دانم نخواهد خواند گر صد باره بنویسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز راهت ریز بهر خشک کردن خاک بر خطی</p></div>
<div class="m2"><p>که من با خون دل بر صفحه رخساره بنویسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جنبش افکند مانند مژگان خامه را حیرت</p></div>
<div class="m2"><p>چو خواهم وصف رویش در دم نظاره بنویسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بغربت سوختم روزی نگفت آن ماه مشگین خط</p></div>
<div class="m2"><p>که آرم یاد و مکتوبی بآن بیچاره بنویسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سنگ خاره تا روز قیامت سر زند آتش</p></div>
<div class="m2"><p>ز سوز دل اگر حرفی بسنگ خاره بنویسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخواهد خواند کس افسانه فرهاد و مجنون را</p></div>
<div class="m2"><p>اگر شرح غم خود را من آواره بنویسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی کرد تیغ غم قلم هر استخوانم را</p></div>
<div class="m2"><p>که با هر یک حساب ظلم آن خونخواره بنویسم</p></div></div>