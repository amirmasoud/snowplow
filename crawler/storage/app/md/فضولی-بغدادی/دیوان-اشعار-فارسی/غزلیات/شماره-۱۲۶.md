---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>کسی در عاشقی از سوی پنهانم خبر دارد</p></div>
<div class="m2"><p>که چون من آتشی در سینه داغی بر جگر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزن دستی بدامان سرشک ای چشم ترک امشب</p></div>
<div class="m2"><p>چو دامانش غباری چهره ام زان خاک در دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگیر ای باد با خاک ره او رخنه چشمم</p></div>
<div class="m2"><p>که نزهتگاه دل بیم خلل زین رهگذر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا ساقی طریق بی خودی بنما که می بینم</p></div>
<div class="m2"><p>ره هشیاریم از مستی چشمش خطر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خونریزی بتیرت نسبتی دارند زانست این</p></div>
<div class="m2"><p>که با مژگان خونین مردم چشمم نظر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو سایه بر رهش افتاده ام کاری کن ای طالع</p></div>
<div class="m2"><p>که آید آفتاب من مرا از خاک بردارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی با خیال لعل میگون بتان هر دم</p></div>
<div class="m2"><p>چو می در جام صد گرداب خون در چشم تر دارد</p></div></div>