---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>چند منعم کنی از عشق جوانان ای شیخ</p></div>
<div class="m2"><p>نیستم طفل فریبم بود آسان ای شیخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حکم منع از مه رخسار جوانان نشدست</p></div>
<div class="m2"><p>مگر آگه نه ای از معنی قرآن ای شیخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر دل زار من آزار جوانان کم نیست</p></div>
<div class="m2"><p>تو هم از طعنه بسیار مرنجان ای شیخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه بخود می کشم ایام جوانی می ناب</p></div>
<div class="m2"><p>می دهد پند مرا گردش دوران ای شیخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ زیبا پسران قبله اهل نظر است</p></div>
<div class="m2"><p>هر که باور نکند نیست مسلمان ای شیخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیز تا کسب جوانی ز می ناب کنیم</p></div>
<div class="m2"><p>چند مانیم چنین پیر و پریشان ای شیخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای فضولی مطلب ترک هوای پسران</p></div>
<div class="m2"><p>نیست آسان که کسی بگذرد از جان ای شیخ</p></div></div>