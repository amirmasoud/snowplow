---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>ای مرضهای معاصی ز تو محتاج علاج</p></div>
<div class="m2"><p>تو شفیع و همه عالم به شفاعت محتاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ارجمند از جهت حسن قبولت اسلام</p></div>
<div class="m2"><p>سربلند از شرف پایه قدرت معراج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع قدر تو شب ظلمت حیرت را ماه</p></div>
<div class="m2"><p>خاک پای تو سر رفعت گردون را تاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار دنیا شده از دولت شرع تو تمام</p></div>
<div class="m2"><p>قدر دین یافته از سکه عدل تو رواج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل را حکم تو مستخدم اجرای امور</p></div>
<div class="m2"><p>ملک را امر تو مستلزم پیوند مزاج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرش را از شرف پای تو عالی مقدار</p></div>
<div class="m2"><p>شرع را از مه روی تو منور منهاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا نبی نیست ز لطف تو فضولی نومید</p></div>
<div class="m2"><p>طالب قطره آبیست ز بحر مواج</p></div></div>