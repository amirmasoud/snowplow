---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>دل که سوزان بود خندان از رخ آن ماه شد</p></div>
<div class="m2"><p>آنچنان کآتش گل از فیض خلیل الله شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه تنگم دل خون گشته را در حبس داشت</p></div>
<div class="m2"><p>زخم پیکانت ز بهر جستن او راه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در زنخدانت دلم از قید نام و ننگ رست</p></div>
<div class="m2"><p>مخلص یوسف ز یاران مخالف خواه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داد رخت شادمانی را بسیلاب سرشک</p></div>
<div class="m2"><p>تا دل محزونم از ذوق غمت آگاه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوی من ره یافت هر محنت که ره گم کرده بود</p></div>
<div class="m2"><p>تا شب تاریک من روشن ز برق آه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قد کشیدی دیده را تاب تماشایت نماند</p></div>
<div class="m2"><p>خلعت نور نظر بر قامتت کوتاه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کعبه ملکست و ملت درگه پیر مغان</p></div>
<div class="m2"><p>قدر دارد تا فضولی خاک این درگاه شد</p></div></div>