---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>ملولم از تو نمی پرسیم که حال تو چیست</p></div>
<div class="m2"><p>ملول بهر چه موجب ملال تو چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراب شد ز تو حالم چرا نمی پرسی</p></div>
<div class="m2"><p>چه حالتست ترا مانع سؤال تو چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا خیال تو و فکر تست در دل زار</p></div>
<div class="m2"><p>ترا چه فکر بدل می رسد خیال تو چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شراب عشق تو مدهوش کرده است مرا</p></div>
<div class="m2"><p>چه آگهم که فراق تو یا وصال تو چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اشک و آه من آزرده نمی دانی</p></div>
<div class="m2"><p>که زیب حسن تو پیرایه جمال تو چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر بسوختن سینه ام نه مایل</p></div>
<div class="m2"><p>دم نظر بدل از دیده انتقال تو چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی از سر جان در گذر براه فنا</p></div>
<div class="m2"><p>جز این کشته جانان شوی کمال تو چیست</p></div></div>