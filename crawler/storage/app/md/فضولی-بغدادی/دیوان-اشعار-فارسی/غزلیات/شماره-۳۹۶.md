---
title: >-
    شمارهٔ ۳۹۶
---
# شمارهٔ ۳۹۶

<div class="b" id="bn1"><div class="m1"><p>دلا آن به که چون با خوب رویان همنشین باشی</p></div>
<div class="m2"><p>نباشی غافل از ایام دوری دوربین باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا ای اشک هر دم پیش مردم می کنی رسوا</p></div>
<div class="m2"><p>نمی خواهم ترا مطلق که در روی زمین باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا ای چرخ می خواهی کز آن مه دور گردانی</p></div>
<div class="m2"><p>چه کین است این که با من بسته تا کی برین باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا در خون دل کردم نهان ای مردم دیده</p></div>
<div class="m2"><p>که چون بر من شبیخون آورد غم در کمین باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم را آتش اندوه خواهد سوخت می دانم</p></div>
<div class="m2"><p>مشو غافل چو ساکن در دل اندوهگین باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنم را پر کن از پیکان که چون آیی درون دل</p></div>
<div class="m2"><p>ز هر آفت که باشد در حصار آهنین باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی گر چه رسوایی مجو تدبیر کار از کس</p></div>
<div class="m2"><p>چه چاره چون ترا تقدیر می خواهد چنین باشی</p></div></div>