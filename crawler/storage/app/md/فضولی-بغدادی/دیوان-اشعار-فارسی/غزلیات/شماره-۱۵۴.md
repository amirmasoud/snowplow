---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>ای که گویی که دلت خون نشود چون نشود</p></div>
<div class="m2"><p>چه دلست آنکه ز بیداد بتان خون نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رونق حسن تو هر چند که افزون گردد</p></div>
<div class="m2"><p>عشقم آن نیست که از حسن تو افزون نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داری آن حسن که گر پیش تو آید لیلی</p></div>
<div class="m2"><p>نتواند که ترا بیند و مجنون نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رای عقلست که دل گرد خطت گم گردد</p></div>
<div class="m2"><p>حکم عشقست کزان دایره بیرون نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می نویسم خط خونابه بلوح رخ زرد</p></div>
<div class="m2"><p>آه اگر گلرخ من واقف مضمون نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه واقع شود از دور درو نیست ثبات</p></div>
<div class="m2"><p>عاقل آن به که بهر واقعه محزون نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار را هست بحال تو فضولی رحمی</p></div>
<div class="m2"><p>هست امید که این حال دگرگون نشود</p></div></div>