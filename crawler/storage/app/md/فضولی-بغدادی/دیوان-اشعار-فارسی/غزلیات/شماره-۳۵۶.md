---
title: >-
    شمارهٔ ۳۵۶
---
# شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>حباب نیست ز خون گرد دیده تر من</p></div>
<div class="m2"><p>هوای غیر تو بیرون شده است از سر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسوخت آتش حیرت مرا نمی دانم</p></div>
<div class="m2"><p>چه کرده ام ز چه رنجیده است دلبر من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بپای بوس توام ره بهیچ صورت نیست</p></div>
<div class="m2"><p>مگر کشند بخاک ره تو پیکر من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی برابری من کجا تواند کرد</p></div>
<div class="m2"><p>کنون که نیست کسی جز تو در برابر من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدم هلاک ز درد و غم تو رحمی کن</p></div>
<div class="m2"><p>بجان غمزده و چشم درد پرور من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدور خط تو مشکل توانم آسودن</p></div>
<div class="m2"><p>چنین که هر سر مو گشته خار بستر من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رقیب چند کنی منع او ز آزارم</p></div>
<div class="m2"><p>مگو که قطع شود روزی مقرر من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حریف بزم غمم خون دل بس است میم</p></div>
<div class="m2"><p>شراب وصل فضولی کجاست در خور من</p></div></div>