---
title: >-
    شمارهٔ ۲۷۵
---
# شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>بدیده سرمه از خاک راه یار می خواهم</p></div>
<div class="m2"><p>ولی آنرا نهان از دیده اغیار می خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه لطفیست این که بیداد خود از من کم نمی سازد</p></div>
<div class="m2"><p>چو می داند که من بیداد او بسیار می خواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگوید کس ز شرح محنت من پیش او حرفی</p></div>
<div class="m2"><p>درین محنت مدد از نالهای زار می خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی خواهم که بیند هیچ کس در خواب آن مه را</p></div>
<div class="m2"><p>همه شب چون ننالم خلق را بیدار می خواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اجل از بیم خجلت سوی من ناید چو می داند</p></div>
<div class="m2"><p>کزو من چاره درد دل بیمار می خواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بامیدی که خندد بر تمنای محال من</p></div>
<div class="m2"><p>بگریه کام دل زان لعل شکر بار می خواهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی رشته جان از غم زلفش گره دارد</p></div>
<div class="m2"><p>گشاد این گره زان طره طرار می خواهم</p></div></div>