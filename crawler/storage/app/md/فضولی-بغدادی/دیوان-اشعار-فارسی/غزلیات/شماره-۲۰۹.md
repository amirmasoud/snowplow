---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>دلا بمهر رخش دیده پر آب انداز</p></div>
<div class="m2"><p>ترست پرده چشمت بآفتاب انداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبا که گفت که حرفی ز بی قراری ما</p></div>
<div class="m2"><p>بگوی آن گل تر را در اضطراب انداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقاب کرد تن خاکیم ز چهره جان</p></div>
<div class="m2"><p>گرت هواست که افتد ز رخ نقاب انداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میانه من و تو هستی منست حجاب</p></div>
<div class="m2"><p>بیا و آتشی از رخ برین حجاب انداز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه می دهی از سر التفات دل برقیب</p></div>
<div class="m2"><p>سکیست جانب او سنگ اجتناب انداز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شدم خراب ز بی رحمی تو رحمی کن</p></div>
<div class="m2"><p>گهی ز لطف نظر بر من خراب انداز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه کار تست فضولی قبول قید ورع</p></div>
<div class="m2"><p>ترا که گفت که خود را درین عذاب انداز</p></div></div>