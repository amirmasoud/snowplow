---
title: >-
    شمارهٔ ۳۷۴
---
# شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>با منی اما چه حاصل سوی من مایل نه‌ای</p></div>
<div class="m2"><p>در دلی اما چه سود آگه ز حال دل نه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو بدان مایل که بر من هر زمان جوری کنی</p></div>
<div class="m2"><p>من بدین خوش دل که از من یک زمان غافل نه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوب می‌دانی طریق کشتن عشاق را</p></div>
<div class="m2"><p>گر چه طفلی از فن عاشق‌کشی جاهل نه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخل امیدم نمی‌یابد ز تو نشو و نما</p></div>
<div class="m2"><p>می‌شود معلوم کز نوری ز آب و گل نه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست حسن التفات گل‌رخان از کس دریغ</p></div>
<div class="m2"><p>ای که محرومی ازین دولت مگر قابل نه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که در ملک جهان یار اقامت می‌نهی</p></div>
<div class="m2"><p>غالبا آگه ز آفت‌های این منزل نه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر جنونم می‌زنی هردم فضولی طعنه‌ها</p></div>
<div class="m2"><p>گر چه می‌رنجم چه گویم با تو چون عاقل نه‌ای</p></div></div>