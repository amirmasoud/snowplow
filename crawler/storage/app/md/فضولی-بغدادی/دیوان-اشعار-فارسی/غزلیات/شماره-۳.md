---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای بسته دانش تو زبان سوآل ما</p></div>
<div class="m2"><p>ناکرده شرح پیش تو معلوم مآل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شام و سحر تصور آثار صنع تست</p></div>
<div class="m2"><p>نقش نگار خانه خواب و خیال ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درک حقیقت تو محالست بر خیال</p></div>
<div class="m2"><p>این آرزو کجا و خیال محال ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داریم حال بد ز مآل فعال بد</p></div>
<div class="m2"><p>ما را بحال ما نگذارد فعال ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی که از تو هر عملی را جزا رسد</p></div>
<div class="m2"><p>تعذیر ما بس است ز تو انفعال ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را مجال ده که ز ذکر تو دم زنیم</p></div>
<div class="m2"><p>بهر سخن دمی که نماید مجال ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اظهار عذر ماست فضولی ز معصیت</p></div>
<div class="m2"><p>بر روی زرد ما رقم اشک آل ما</p></div></div>