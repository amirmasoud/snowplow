---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>نظر بازی که حیران رخ آن سیمتن باشد</p></div>
<div class="m2"><p>نمی خواهم که بینم از حسد گر چشم من باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی از داغ می سوزم گهی از درد می نالم</p></div>
<div class="m2"><p>چه خوش باشد که در عشقت مرا نه جان نه تن باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرم را هست سودای خطت تا هست سر بر تن</p></div>
<div class="m2"><p>مرا عشق تو در جانست تا جان در بدن باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه فرق از صورت دیوار تا شیرین اگر شیرین</p></div>
<div class="m2"><p>چو صورت غافل از سوز درون کوهکن باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسبزه می دهد جان عاشق روی تو می سازد</p></div>
<div class="m2"><p>نمی خواهم که سایه با تو در سیر چمن باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بلبل را گره از کار نتواند که بگشاید</p></div>
<div class="m2"><p>چه سود ار غنچه را دندان ز شبنم در دهن باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندارد ذره در دل اثر افسانه زاهد</p></div>
<div class="m2"><p>فضولی درد دل باید که ذوقی در سخن باشد</p></div></div>