---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>بهار آمد صدایی برنمی‌آید ز بلبل‌ها</p></div>
<div class="m2"><p>مگر امسال رنگ دلربایی نیست در گل‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل آمد نیست میل سیر گلشن نازنینان را</p></div>
<div class="m2"><p>پریشان کرد گل‌های چمن را این تغافل‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو رغبت نیست در عاشق چه سود از آنکه محبوبان</p></div>
<div class="m2"><p>برافروزند عارض‌ها برافشانند کاکل‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین موسم چرا دل‌ها مقید نیست در گلشن</p></div>
<div class="m2"><p>مگر زنجیرهای زلف نگشادند سنبل‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو غنچه صد گره دارد دل از غم وین غم دیگر</p></div>
<div class="m2"><p>که دوران در گشاد هر گره دارد تعلل‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازآن بگرفت در بر آب را گلشن به صد عزت</p></div>
<div class="m2"><p>که پیدا کرد از اقبال او چندین تجمل‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی رهگذار عشقبازی صد خطر دارد</p></div>
<div class="m2"><p>شروع این طریق صعب را باید تأمل‌ها</p></div></div>