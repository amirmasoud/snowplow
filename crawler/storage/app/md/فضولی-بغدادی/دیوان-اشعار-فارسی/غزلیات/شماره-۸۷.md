---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>غیر ناکامی ز محبوبان مرا مطلوب نیست</p></div>
<div class="m2"><p>عاشقان را کام دل جستن ز خوبان خوب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ندیدم صد جفا از یار می خواهم وفا</p></div>
<div class="m2"><p>چیزی از محبوب می خواهم که در محبوب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد باید تا نیازارد ز خود معشوق را</p></div>
<div class="m2"><p>بهر یوسف در زلیخا رأفت یعقوب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد دلم صد پاره ناوردم شکایت بر زبان</p></div>
<div class="m2"><p>محنت و صبری که در من هست در ایوب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست جز لیلی بقای عشق مجنون را سبب</p></div>
<div class="m2"><p>ضایع است آنکس که بر گل چهره منسوب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد گچ رو ندارد رغبت عشق بتان</p></div>
<div class="m2"><p>راستی را این روش از هیچ کس مرغوب نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیستم یک دم فضولی بی تماشای بتان</p></div>
<div class="m2"><p>شاهد مقصد ز من در هیچ جا محجوب نیست</p></div></div>