---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>گر سر کویت شود مدفن پس از مردن مرا</p></div>
<div class="m2"><p>کی عذاب قبر پیش آید دران مدفن مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند باشم در جدل با خود ز غم ساقی بیار</p></div>
<div class="m2"><p>شیشه می تا رهاند ساعتی از من مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوست چون می خواهدم رسوا ندارم چاره ای</p></div>
<div class="m2"><p>می شوم رسوا چه باک از طعنه دشمن مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آتش دل چون نمی سوزد روان گویا که هست</p></div>
<div class="m2"><p>استخوانهای بدن فانوس وش زاهن مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کام من معنیست نی صورت ز یوسف طلعتان</p></div>
<div class="m2"><p>من نه یعقوبم چه ذوق از بوی پیراهن مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خنده دارند بی پروا ز آسیب خزان</p></div>
<div class="m2"><p>چون نیاید گریه بر گلهای این گلشن مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت جان از تن برون تن شد فضولی خاک ره</p></div>
<div class="m2"><p>عشق او شد آفت جان و بلای تن مرا</p></div></div>