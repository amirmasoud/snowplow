---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>چو پاره پاره دل از دیده ترم افتد</p></div>
<div class="m2"><p>هزار شعله آتش به بسترم افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیاورم بنظر آفتاب را ز شرف</p></div>
<div class="m2"><p>دمی که دیده بدان ماه پیکرم افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زند بدامن من آفتاب دست ز قدر</p></div>
<div class="m2"><p>گهی که سایه آن سرو بر سرم افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشم بکنج غم و بی کسی که باشم من</p></div>
<div class="m2"><p>که ره ببزم بتان سمنبرم افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدست اخترم ای کاش برق آتش آه</p></div>
<div class="m2"><p>رسد بچرخ شب غم در اخترم افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیاد لعل تو آتش فتاد در جگرم</p></div>
<div class="m2"><p>که آتشی بدل درد پرورم افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهیچ باب فضولی قرار نیست مرا</p></div>
<div class="m2"><p>مگر دمی که گذر سوی آن درم افتد</p></div></div>