---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>مرا هرگه که پندی می‌دهد با چشم تر ناصِح</p></div>
<div class="m2"><p>نهال محنتم را می‌دهد آب دگر ناصح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس است این سوز در من گر نصیحت نشنود زین بس</p></div>
<div class="m2"><p>به دم هردم نسازد آتشم را تیزتر ناصح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا گوید که مردم را به افغان درد سر کم ده</p></div>
<div class="m2"><p>نمی‌دانم چه حاصل می‌کند زین درد سر ناصح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نصیحت می‌کند کز خلق پنهان دار دردت را</p></div>
<div class="m2"><p>ندارد غالبا از درد پنهانم خبر ناصح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پند ناصح از خون جگر خوردن نگردم بس</p></div>
<div class="m2"><p>ز من از من بتر صد داغ دارد بر جگر ناصح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشود از پند سیل خونم از مژگان نمی‌دانم</p></div>
<div class="m2"><p>زبان بگشاد یا زد بر رک دل نیشتر ناصح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی راحتی گر بایدت از موج خون سدی</p></div>
<div class="m2"><p>ببند اطراف خود تا کم کند سویت گذر ناصح</p></div></div>