---
title: >-
    شمارهٔ ۳۵۵
---
# شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>چو شمع ز آتش دل اضطراب دارم من</p></div>
<div class="m2"><p>دل پر آتش چشم و پر آب دارم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره نظاره ز غیر تو بسته ام شب هجر</p></div>
<div class="m2"><p>مکن خیال که در دیده خواب دارم من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهید ساخت مرا جور بی حساب بتان</p></div>
<div class="m2"><p>چه غم ز پرسش روز حساب دارم من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک بدور مخالف مرا نترساند</p></div>
<div class="m2"><p>مشوشم چه غم از انقلاب دارم من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سایه در پی آن مه رقیب می فکند</p></div>
<div class="m2"><p>هزار داغ بدل ز آفتاب دارم من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه گونه چاک کنم سینه پیش بی دردان</p></div>
<div class="m2"><p>بتی ز چشم بدان در نقاب دارم من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی از الم بی کسی نخواهم رست</p></div>
<div class="m2"><p>چنین که از همه کس اجتناب دارم من</p></div></div>