---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>بر باد مده سلسله مشک فشان را</p></div>
<div class="m2"><p>مگشای ز پیوند تنم رشته جان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راز تو نهانست مرا در دل و ترسم</p></div>
<div class="m2"><p>چشم ترم اظهار کند راز نهان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخساره بهر کسی منما فتنه مینگیز</p></div>
<div class="m2"><p>از هم مگشا رابطه نظم جهان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه از دل شیدا که سراسیمه اویم</p></div>
<div class="m2"><p>در عاشقی ما چه گناهست بتان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای کاش دلم خون شود از عشق بر آیم</p></div>
<div class="m2"><p>تا چند کشم محنت هر غنچه دهان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بخت بخاک در آن گلرخم افکن</p></div>
<div class="m2"><p>مگذار که در خاک کشم حسرت آن را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر یاد خطش اشک روان ساز فضولی</p></div>
<div class="m2"><p>زان سبزه تر قطع مکن آب روان را</p></div></div>