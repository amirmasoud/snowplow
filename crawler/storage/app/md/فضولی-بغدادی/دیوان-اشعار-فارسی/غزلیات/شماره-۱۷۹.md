---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>گر بند بند ما چو نی از هم جدا کنند</p></div>
<div class="m2"><p>به زانکه در غمت ز فغان منع ما کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوبان نمی کنند وفایی بعاشقان</p></div>
<div class="m2"><p>خوبند بهر آنکه همیشه جفا کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بینند روی شاهد مقصود اهل دل</p></div>
<div class="m2"><p>گر توتیای دیده ازان خاک پا کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان نیست جز امانت تو نزد عاشقان</p></div>
<div class="m2"><p>وقتست جان من بتو یک یک فدا کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امروز دیده اند ترا زاهدان شهر</p></div>
<div class="m2"><p>فردا نماز خود همه باید فضا کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حیرتم که راهروان طریق عقل</p></div>
<div class="m2"><p>خود را اسیر دام تعلق چرا کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهل وفا نیند فضولی پری رخان</p></div>
<div class="m2"><p>هرگز طمع مدار که با تو وفا کنند</p></div></div>