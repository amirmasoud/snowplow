---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>بر گلویم تیغ ترک تند خوی من رسید</p></div>
<div class="m2"><p>تشنه لب بودم که آبی بر گلوی من رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نسیم وصل جانها را معطر شد دماغ</p></div>
<div class="m2"><p>غالبا کز ره غزال مشکبوی من رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ژاله وش بارید ازو سنگ ملامت بر سرم</p></div>
<div class="m2"><p>آفتی برگشت زار آرزوی من رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشته آنم که در جولان سمند ناز را</p></div>
<div class="m2"><p>سرکشند از سرکشی هرگه که سوی من رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم از افسانه فرهاد و مجنون شد تهی</p></div>
<div class="m2"><p>تا بگوش اهل عالم گفت و گوی من رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو من دیوانه دیگر بسرحد فنا</p></div>
<div class="m2"><p>گر رسید البته هم در جست و جوی من رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم از گریه فضولی پای در گل ماند گفت</p></div>
<div class="m2"><p>اینچنین بهتر که نتواند بکوی من رسید</p></div></div>