---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>طمع جور دلم زان بت بدخو دارد</p></div>
<div class="m2"><p>ز بتان آنچه دلم می طلبد او دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باید از حلقه زنجیر جنون سر نکشد</p></div>
<div class="m2"><p>هر که در سر هوس آن خم گیسو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش از حال دل نافه خبر داد صبا</p></div>
<div class="m2"><p>گرهی در دل ازان سنبل خوش بو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی جهت رغبت محراب ندارد زاهد</p></div>
<div class="m2"><p>میل نظاره آن گوشه ابرو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می تواند کرد قیاس ملک از حور و شان</p></div>
<div class="m2"><p>خوی بد دارد اگر عارض نیکو دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کند گر نه ز بهبود کند قطع نظر</p></div>
<div class="m2"><p>دل که بیماری ازان نرگس جادو دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن از عشق بتان منع فضولی ای شیخ</p></div>
<div class="m2"><p>همه کس میل جوانان پری رو دارد</p></div></div>