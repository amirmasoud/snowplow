---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>سایه ات را متصل ذوق وصالت حاصل است</p></div>
<div class="m2"><p>نیست دور از دولتی اما چه حاصل غافل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حل مشکل نیست مشکل پیش او اما چه سود</p></div>
<div class="m2"><p>مشکل خود پیش او اظهار کردن مشکل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پا کشید از چشمه چشمم ز بیم فتنه خواب</p></div>
<div class="m2"><p>کین گذرگه مردم خونریز را سرمنزل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نماند رازم از غیر تو پنهان دور نیست</p></div>
<div class="m2"><p>هست دل پیش تو و رازی که دارم در دل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساده افتادست لوح خوبی از نقش وفا</p></div>
<div class="m2"><p>کام دل جستن ز محبوبان خیال باطل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر بگردون گر کشد از روی رفعت دور نیست</p></div>
<div class="m2"><p>هر کرا چون سبزه در کوی بلا پا در گل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفت تکلیف را ره نیست در ملک جنون</p></div>
<div class="m2"><p>کی رود بیرون ازین کشور فضولی عاقل است</p></div></div>