---
title: >-
    شمارهٔ ۳۳۴
---
# شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>چشمی بگشا سوی من و زاری من بین</p></div>
<div class="m2"><p>در دام غم عشق گرفتاری من بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جور و جفا مردم و آهی نکشیدم</p></div>
<div class="m2"><p>آزار رقیبان و کم آزاری من بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کردم ز رخت منع دل و مردم دیده</p></div>
<div class="m2"><p>با سوخته چند ستمکاری من بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد جور کشیدم ز بتان ترک نکردم</p></div>
<div class="m2"><p>با اهل جفا رسم وفاداری من بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من مهر نمودم همه دم ماه و شان جور</p></div>
<div class="m2"><p>اغیاری این طائفه و یاری من بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس را نظری بر من افتاده نیفتد</p></div>
<div class="m2"><p>در رهگذر عشق بتان خواری من بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل را بسپردم بغم عشق فضولی</p></div>
<div class="m2"><p>با دشمن خود یاری و غمخواری من بین</p></div></div>