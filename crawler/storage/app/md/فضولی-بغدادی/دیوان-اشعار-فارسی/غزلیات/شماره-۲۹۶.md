---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>تا بوده ایم بی غم یازی نبوده ایم</p></div>
<div class="m2"><p>بی درد و داغ لاله عذاری نبوده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی اعتبار عاشقی و لذت جنون</p></div>
<div class="m2"><p>در هیچ کشوری و دیاری نبوده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بودست کار ما همه عمر عاشقی</p></div>
<div class="m2"><p>شکر خدا که بیهده کاری نبوده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز ز عمر ما نگذشته دمی که ما</p></div>
<div class="m2"><p>خونین جگر ز دست نگاری نبوده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر جا که بوده ایم ز آزار گلرخان</p></div>
<div class="m2"><p>بی گریه و ناله زاری نبوده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز کنج غم نبوده همیشه مقام ما</p></div>
<div class="m2"><p>هرگز بفکر باغ و بهاری نبوده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیوسته غرقه ایم فضولی بکام دل</p></div>
<div class="m2"><p>زین بحر هیچ گه بکناری نبوده ایم</p></div></div>