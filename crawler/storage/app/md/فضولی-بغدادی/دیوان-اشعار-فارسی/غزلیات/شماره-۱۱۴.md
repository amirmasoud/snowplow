---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>در عشق شهرتم سبب اشتهار تست</p></div>
<div class="m2"><p>بی اعتباریم جهت اعتبار تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ذکر من چرا مه من عار می کنی</p></div>
<div class="m2"><p>اظهار خاکساری من افتخار تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن تو گشته شهره عالم ز عشق من</p></div>
<div class="m2"><p>تا هست لوح هستیم آیینه دار تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر اشکباری مژه ام خنده می کنی</p></div>
<div class="m2"><p>گویا گلی تو وین مژه ابر بهار تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل را درون سینه اگر جا دهم رواست</p></div>
<div class="m2"><p>دردی که در دلست مرا یادگار تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمریست گم شدست دل مبتلا ز من</p></div>
<div class="m2"><p>گویا اسیر سلسله مشک بار تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانا مکن ز حال فضولی نظر دریغ</p></div>
<div class="m2"><p>کان هم ز عاشقان سیه روزگار تست</p></div></div>