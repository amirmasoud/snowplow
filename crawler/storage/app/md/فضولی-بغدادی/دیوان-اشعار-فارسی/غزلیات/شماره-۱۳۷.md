---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>عکس قد او آینه بربود خطا کرد</p></div>
<div class="m2"><p>خود را چو دل ما هدف تیر بلا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشک آینه دار قد خم گشته من شد</p></div>
<div class="m2"><p>دردا که قدم را غم عشق تو دو تا کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریاد ز ناسازی طالع که نکردیم</p></div>
<div class="m2"><p>جا در دل آن ماه که جا در دل ما کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار غم تو با دل تنگم شب هجران</p></div>
<div class="m2"><p>کاریست که با غنچه دم باد صبا کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو گرد ز دامن بفشاندی و من از غم</p></div>
<div class="m2"><p>مردم که چرا بخت مرا از تو جدا کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون ریخت جگر سوخت بدن خست دل آزرد</p></div>
<div class="m2"><p>با ما غم عشق تو چه گویم که چها کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برداشت دل از سجده ابروی بتان سر</p></div>
<div class="m2"><p>در حیرت آنم که چنین سهو چرا کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا قطره آبی نشد از جای نجنبید</p></div>
<div class="m2"><p>در هر دل پرسوز که پیکان تو جا کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پنجه غم ماند گریبان فضولی</p></div>
<div class="m2"><p>زان روز که دامان تو از دست رها کرد</p></div></div>