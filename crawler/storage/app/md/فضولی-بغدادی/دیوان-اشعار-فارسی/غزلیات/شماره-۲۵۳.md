---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>من که بی‌لاله‌رخی ساکن گلخن شده‌ام</p></div>
<div class="m2"><p>زآتش عشق چنین سوخته خرمن شده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌گل روی تو و گلشن کویت عمریست</p></div>
<div class="m2"><p>فارغ از میل گل و رغبت گلشن شده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌شوی یار کسان می‌کشی از غصه مرا</p></div>
<div class="m2"><p>جان من راضی ازین غصه به مردن شده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلبن پر گل و گلزار غم خوار مبین</p></div>
<div class="m2"><p>من عریان که به داغ تو مزین شده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌جهد آتشم از دل همه‌شب در کویت</p></div>
<div class="m2"><p>ز آتش دل شجر وادی ایمن شده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ کس نیست که در بند غم زلف تو نیست</p></div>
<div class="m2"><p>نه همین بسته زنجیر غمت من شده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوست را نیست فضولی غم ناکامی من</p></div>
<div class="m2"><p>آه ازین غم که به کام دل دشمن شده‌ام</p></div></div>