---
title: >-
    شمارهٔ ۴۰۲
---
# شمارهٔ ۴۰۲

<div class="b" id="bn1"><div class="m1"><p>سال و مهم بر زبان روز و شبم در دلی</p></div>
<div class="m2"><p>من ز تو غافل نیم گر تو ز من غافلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال خرابی دل از که کنم جست و جو</p></div>
<div class="m2"><p>چون تو ز روز ازل ساکن این منزلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو دل زار را نیست امید وفا</p></div>
<div class="m2"><p>طرفه نهالی ولی حیف که بی حاصلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست مرا دم بدم میل تو اما چه سود</p></div>
<div class="m2"><p>نیست ترا میل من جای دگر مایلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ز بلا بی خبر طعه ما ترک کن</p></div>
<div class="m2"><p>غرقه بحریم ما رو که تو بر ساحلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاصل عشق بتان نیست بغیر از جنون</p></div>
<div class="m2"><p>بسته اینها مشو ای دل اگر عاقلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست فضولی ترا میل نظر بازی</p></div>
<div class="m2"><p>علم تو زهداست و بس در فن ما جاهلی</p></div></div>