---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>به رندان از جهنم می‌دهد دایم خبر واعظ</p></div>
<div class="m2"><p>مگر مطلق ندیده در جهان جای دگر واعظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریبان چاک ازین غم می‌کند محراب در مسجد</p></div>
<div class="m2"><p>که آب روی منبر برد با دامان تر واعظ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تفسیر مخالف می‌دهد تغییر قرآن را</p></div>
<div class="m2"><p>تمنای تفوق می‌کند با این هنر واعظ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دم از کیفیت اِعراب مصحف می‌زند هردم</p></div>
<div class="m2"><p>بنای خانه دین می‌کند زیر و زبر واعظ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز کوی آن صنم سوی بهشت هشت در هردم</p></div>
<div class="m2"><p>چه می‌خواند مرا یارب که افتد در به در واعظ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنزل از مقام خود نمی‌کرد اینچنین دایم</p></div>
<div class="m2"><p>اگر در منع من می‌داشت قول معتبر واعظ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی نیست میل صحبت واعظ مرا زان رو</p></div>
<div class="m2"><p>که منع اهل دل کرد از بتان سیمبر واعظ</p></div></div>