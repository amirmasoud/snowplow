---
title: >-
    شمارهٔ ۳۶۵
---
# شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>شد درون سینه دل دیوانه از سودای او</p></div>
<div class="m2"><p>بستم از رگهای جان زنجیرها در پای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست از بی التفاتی گر نبیند سوی من</p></div>
<div class="m2"><p>آن که عین التفات اوست استغنای او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جای پیکانت درون سینه کردم چون کنم</p></div>
<div class="m2"><p>دل ز جا شد خواستم خالی نماند جای او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد روز و روزگارم را بیک دیدن سیه</p></div>
<div class="m2"><p>کی مرا این چشم بود از نرگس شهلای او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان بر آمد یار بهر پرسشم نگشاد لب</p></div>
<div class="m2"><p>بر نیامد کام من از لعل شکر خای او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر نخواهم داشت تا روز قیامت سر ز خواب</p></div>
<div class="m2"><p>گر شبی در خوابم آید قامت رعنای او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نباشم زار و سرگردان فضولی متصل</p></div>
<div class="m2"><p>رشته جان بسته ام بر زلف عنبرسای او</p></div></div>