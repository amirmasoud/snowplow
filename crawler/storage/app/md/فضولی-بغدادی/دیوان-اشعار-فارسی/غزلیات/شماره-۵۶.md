---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>نیست تا صبح بجز فکر تو کارم همه شب</p></div>
<div class="m2"><p>کارم اینست جز این کار ندارم همه شب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه روزم شده شب اختر آن شبها اشک</p></div>
<div class="m2"><p>آه ازین درد چسان اشک نبارم همه شب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لطف کن یک شب و در کلبه من گیر قرار</p></div>
<div class="m2"><p>تا بدانی که چرا نیست قرارم همه شب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ندانند که من بهر تو می نالم و بس</p></div>
<div class="m2"><p>بفغان از همه کس ناله برارم همه شب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ز من پیش تو گویند حکایت همه روز</p></div>
<div class="m2"><p>هیچ کس را بفراغت نگذارم همه شب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بامیدی که اگر بشنود آن ماه شبی</p></div>
<div class="m2"><p>ز فلک می گذرد ناله زارم همه شب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب تنهایی من نیست فضولی بی تو</p></div>
<div class="m2"><p>شمع بزمست خیال رخ یارم همه شب</p></div></div>