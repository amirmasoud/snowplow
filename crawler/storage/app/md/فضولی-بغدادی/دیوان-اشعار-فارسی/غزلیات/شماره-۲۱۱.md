---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>شمع بزم بهجتم مهر مه روی تو بس</p></div>
<div class="m2"><p>مطلع خورشید اقبالم سر کوی تو بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو نافه در سرم سودای مشک خشک نیست</p></div>
<div class="m2"><p>نافه عطر دماغم عقد گیسوی تو بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر طاعت هر کسی را هست رو در قبله</p></div>
<div class="m2"><p>قبله من گوشه محراب ابروی تو بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رقیبان هست مستغنی حریم درگهت</p></div>
<div class="m2"><p>مانع وصل تو بیم تندی خوی تو بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آرزوی بهره دیگر ندارم از حیات</p></div>
<div class="m2"><p>حاصل عمرم هوای قد دلجوی تو بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دهانت گر کند عالم تقاضای عدم</p></div>
<div class="m2"><p>عالمی را یک نظر از چشم جادوی تو بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می فزاید زهر دشنام تو ذوق اهل دل</p></div>
<div class="m2"><p>بهر دشنام تو اهل دل دعاگوی تو بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم گر بستست از عالم فضولی دور نیست</p></div>
<div class="m2"><p>در نظر او را خیال روی نیکوی تو بس</p></div></div>