---
title: >-
    شمارهٔ ۲۲۷
---
# شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>گرد گلت کشید ز عنبر حصار خط</p></div>
<div class="m2"><p>شد شاهد جمال ترا پرده دار خط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشست گرد رشک بر آیینه ماه را</p></div>
<div class="m2"><p>تا ماه من نمود بگرد عذار خط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزم بسان شمع سیه شد ز دود آه</p></div>
<div class="m2"><p>تا سر زد از خواشی رخسار یار خط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خطی نیافتیم بمضمون خط یار</p></div>
<div class="m2"><p>خواندیم از صحیفه دوران هزار خط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دل که سوخت اشک نشان ماند بر رخم</p></div>
<div class="m2"><p>چون مرده که ماند ازو یادگار خط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرده دلیم چون نخراشیم سینه را</p></div>
<div class="m2"><p>رسم مقررست بلوح مزار خط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی خط او چه سود فضولی ز زندگی</p></div>
<div class="m2"><p>درکش به حرف هستی خود زینهار خط</p></div></div>