---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>خورشید بسی خاک‌نشین شد به هوایت</p></div>
<div class="m2"><p>روزی نتوانست که بوسد کف پایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد که جانم به لب آمد ز تحیر</p></div>
<div class="m2"><p>حرفی نشنیدم ز لب روح‌فزایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون ریخته بهر ثواب از همه جز ما</p></div>
<div class="m2"><p>ما را گنه اینست که مردیم برایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان غافلی ای ماه که هر شب به تردد</p></div>
<div class="m2"><p>چون هاله رهی می‌فکنم گرد سرایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تیر تو بر من در صد ذوق گشادست</p></div>
<div class="m2"><p>هر زخم دهانیست مرا بهر دعایت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نقش تو بر لوح دل و دیده کشیدیم</p></div>
<div class="m2"><p>قطعا نکشیدیم سر از تیغ جفایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس نیست که اندیشه زلف تو ندارد</p></div>
<div class="m2"><p>تنها نه فضولیست گرفتار بلایت</p></div></div>