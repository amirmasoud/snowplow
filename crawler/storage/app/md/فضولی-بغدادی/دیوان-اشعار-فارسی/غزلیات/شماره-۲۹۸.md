---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>سرو نازم نشد آگه ز نیازم چه کنم</p></div>
<div class="m2"><p>بکه گویم غم دل آه چه سازم چه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کنم ناله چو بر زلف گره می بندی</p></div>
<div class="m2"><p>می کند کوتهی عمر درازم چه کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بودم از قید جنون رسته نمودی سر زلف</p></div>
<div class="m2"><p>بست تقدیر بدان سلسله بازم چه کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که شمع شب هجرم همه شب تا بسحر</p></div>
<div class="m2"><p>نکنم گریه نسوزم نگدازم چه کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بی کس بکه گویم غم خورشید و شان</p></div>
<div class="m2"><p>نیست جز سایه کسی محرم رازم چه کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من بخود مایل خوبان جفا پیشه نیم</p></div>
<div class="m2"><p>می ربایند بصد عشوه و نازم چه کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کنی منع فضولی که دگر عشق مباز</p></div>
<div class="m2"><p>عشقبازی نکنم عشق نبازم چه کنم</p></div></div>