---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>اگر رسوا شدم رسواییم را شد فغان باعث</p></div>
<div class="m2"><p>فغان را سوزش دل سوزش دل را بتان باعث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بغمزه خستیم دل چون نرنجم زان خم ابرو</p></div>
<div class="m2"><p>چه می دانم ندارد ز خم ناوک جز کمان باعث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه باشد گر بریزد خون چشم خون فشانم دل</p></div>
<div class="m2"><p>گرفتاری دل را گشت چشم خون فشان باعث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درون غنچه تا گلبرگ نبود چاک کی گردد</p></div>
<div class="m2"><p>سزد چاک دلم را گر بود داغ نهان باعث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خط سبزت ز آب دیده من رونقی دارد</p></div>
<div class="m2"><p>بود نشو و نمایی سبزه را آب روان باعث</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرم را سیل اشک از خاک راهت کاش بردارد</p></div>
<div class="m2"><p>که غوغای سکانت را شدست این استخوان باعث</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی در جهان از عشق ذوقی هست با هر کس</p></div>
<div class="m2"><p>ندارد جز مذاق عشق هستی جهان باعث</p></div></div>