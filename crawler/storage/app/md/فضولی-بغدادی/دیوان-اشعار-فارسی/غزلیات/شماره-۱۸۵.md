---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>گفتمش دل ز غمت زار و حزین می‌باید</p></div>
<div class="m2"><p>گفت آری سخن اینست چنین می‌باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش چشم تو در گوشه ابرو چه خوشست</p></div>
<div class="m2"><p>گفت پاکیزه‌نظر گوشه‌نشین می‌باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش بهر چه از من بربودی دل و دین</p></div>
<div class="m2"><p>گفت شیدای بتان بی دل و دین می‌باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم افتاده خود را به چه سان می‌خواهی</p></div>
<div class="m2"><p>گفت رسوا شده روی زمین می‌باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش نور خدا در مه رویت پیداست</p></div>
<div class="m2"><p>گفت پیداست ولی چشم یقین می‌باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم از چین سر زلف خودم تاری بخش</p></div>
<div class="m2"><p>گفت این دلشده را نافه چین می‌باید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش هست فضولی ز غلامان درت</p></div>
<div class="m2"><p>گفت کو شاهد او داغ جبین می‌باید</p></div></div>