---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>ای آنکه آفت دل و جان و تنی مرا</p></div>
<div class="m2"><p>من دوستم ترا تو چرا دشمنی مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان چه شود زانکه کنم میل زیستنی</p></div>
<div class="m2"><p>چون مهربان نه تو که جان منی مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسف قرار قیمت خویش از زمانه یافت</p></div>
<div class="m2"><p>ای در بی بها تو از او احسنی مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمعم من آتشی تو ز تو دوریم مباد</p></div>
<div class="m2"><p>زیرا حیات بخش دل روشنی مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مانند شمع سوخته حسرت توام</p></div>
<div class="m2"><p>با آنکه صبح وش سبب مردنی مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من لاله بهار غمم شبنمم تویی</p></div>
<div class="m2"><p>ای گوهر سرشک که در دامنی مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عشق جز تو نیست فضولی حسود من</p></div>
<div class="m2"><p>معلوم می شود که شریک فنی مرا</p></div></div>