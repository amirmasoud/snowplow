---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>دل درون سینه دردت را به جان می‌پرورد</p></div>
<div class="m2"><p>ذوق می‌بیند ازآن هردم ازآن می‌پرورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقبت معلوم شد بهر سکانت بوده است</p></div>
<div class="m2"><p>این که جسم ناتوانم استخوان می‌پرورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لعل اشک لاله‌گون پرورده چشم منست</p></div>
<div class="m2"><p>کی بدین رونق بود لعلی که کان می‌پرورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نریزد با خیال خط او چشمم سرشک</p></div>
<div class="m2"><p>سبزه دارد بآن آب روان می‌پرورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون قدت ناید اگر سازد بدین عالم روان</p></div>
<div class="m2"><p>هر نهالی را که رضوان در جنان می‌پرورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعمت دنیا به جاهل گر رسد نبود عجب</p></div>
<div class="m2"><p>هست عادت طفل را لطف زنان می‌پرورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده و دل را فضولی می‌دهد خون از جگر</p></div>
<div class="m2"><p>دشمنان خویش را بنگر چه سان می‌پرورد</p></div></div>