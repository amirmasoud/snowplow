---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>درد رسوایی نخواهد داشت درمان ای طبیب</p></div>
<div class="m2"><p>خویش را رسوا مکن ما را مرنجان ای طبیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست بهبود تو در ترک علاج درد من</p></div>
<div class="m2"><p>چون ندارد فکر بهبود من امکان ای طبیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون گشودن از رگ تن چیست گر داری مدد</p></div>
<div class="m2"><p>شوق لعلش را برون آر از رک جان ای طبیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ شربت نیست بهر دفع سودایم مفید</p></div>
<div class="m2"><p>غیر یاد وصل و ذکر لعل جانان ای طبیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کشی از غم مرا ظاهر بکن بهر خدا</p></div>
<div class="m2"><p>درد پنهان مرا پیش رقیبان ای طبیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من نمی خواهم که این درد دل پنهان من</p></div>
<div class="m2"><p>بر تو هم ظاهر شود از تو چه پنهان ای طبیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشته است از غصه مانند تو صد بی درد را</p></div>
<div class="m2"><p>چاره درد فضولی نیست آسان ای طبیب</p></div></div>