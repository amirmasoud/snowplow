---
title: >-
    شمارهٔ ۴۱۰
---
# شمارهٔ ۴۱۰

<div class="b" id="bn1"><div class="m1"><p>ای لعل تو آب زندگانی</p></div>
<div class="m2"><p>عشق تو حیات جاودانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که ترحمی نمایی</p></div>
<div class="m2"><p>چون حال دل مرا بدانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال دل خویش با تو صدره</p></div>
<div class="m2"><p>گفتم بزبان بی زبانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش تو عیان چو گشت حالم</p></div>
<div class="m2"><p>کردی بنیاد مهربانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رحمی بدل تو آمد اما</p></div>
<div class="m2"><p>نگذاشت غرور نوجوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون هیچ نتیجه ندارد</p></div>
<div class="m2"><p>در پیش تو عرض ناتوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن به که فضولی ار بمیرد</p></div>
<div class="m2"><p>ظاهر نکند غم نهانی</p></div></div>