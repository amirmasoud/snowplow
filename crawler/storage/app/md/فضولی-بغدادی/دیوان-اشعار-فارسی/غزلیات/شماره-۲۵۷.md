---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>بسی تاب از غم آن گیسوان پرشکن دیدم</p></div>
<div class="m2"><p>که تا سر رشته وصلت بدست خویشتن دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیدم هیچ کس را غافل از افسانه عشقت</p></div>
<div class="m2"><p>تو بودی بر زبان هر جا دو کس را در سخن دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جفا هر چند بر من بیشتر کردی نشد کمتر</p></div>
<div class="m2"><p>وفا کز من تو دیدی از جفایی کز تو من دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر شد باغبان دلبسته سرو خرامانت</p></div>
<div class="m2"><p>که او را سست در پروردن سرو چمن دیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جراحتهای تازه بر دلم بگشود صد روزن</p></div>
<div class="m2"><p>ز هر روزن درو دور از تو صد داغ کهن دیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمودی حال مشکین بر بیاض چهره زیبا</p></div>
<div class="m2"><p>سواد نقطه از مشک بر برگ سمن دیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی در هوای دلبران می بینمت گویا</p></div>
<div class="m2"><p>ندیدی آنچه من زان دلبر پیمان شکن دیدم</p></div></div>