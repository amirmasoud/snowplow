---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>ای همه دم بزم تو جای رقیب</p></div>
<div class="m2"><p>بهر تو ناچار لقای رقیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امر محالست مرا از تو کام</p></div>
<div class="m2"><p>کام تو چون هست رضای رقیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جور و جفای تو برای منست</p></div>
<div class="m2"><p>مهر و وفای تو برای رقیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر کوی تو گذر می کند</p></div>
<div class="m2"><p>چون ننهم روی بپای رقیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدن هر واقعه سهل است لیک</p></div>
<div class="m2"><p>سخت بلاییست بلای رقیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زندگی اهل دل از وصل تست</p></div>
<div class="m2"><p>وصل تو موقوف فنای رقیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبر و قرار تو فضولی کم است</p></div>
<div class="m2"><p>کمتر از آن مهر و وفای رقیب</p></div></div>