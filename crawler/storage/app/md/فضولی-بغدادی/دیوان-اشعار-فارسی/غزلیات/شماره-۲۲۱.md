---
title: >-
    شمارهٔ ۲۲۱
---
# شمارهٔ ۲۲۱

<div class="b" id="bn1"><div class="m1"><p>بکویش می روم بهر تماشای مه رویش</p></div>
<div class="m2"><p>تماشاییست از رسواییم امروز در کویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارم تاب تیر غمزهای آن کمان ابرو</p></div>
<div class="m2"><p>مگر سویم بتندی ننگرد تا بنگرم سویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه حاجت با رقیبان دگر در منع من او را</p></div>
<div class="m2"><p>رقیب او بس است از هر که باشد تندی خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلی دارم درون دل ز غیرت کس نمی خواهم</p></div>
<div class="m2"><p>نشنید پهلوم ترسم که ناگه بشنود بویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازان رسوا شدم کز غایت ضعف تنم در دل</p></div>
<div class="m2"><p>نگنجید و برون افتاد شوق رشته مویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدم خم باشد ز بار غم برون شو از تنم ای جان</p></div>
<div class="m2"><p>که نتوان بود اینجا با خیال قد دلجویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی چون نیابم در دل اهل محبت ره</p></div>
<div class="m2"><p>خیالی کرد ضعفم در خیال جعد گیسویش</p></div></div>