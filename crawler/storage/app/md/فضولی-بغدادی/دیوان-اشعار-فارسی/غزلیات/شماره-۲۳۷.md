---
title: >-
    شمارهٔ ۲۳۷
---
# شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>یارست فارغ از من و من بی قرار عشق</p></div>
<div class="m2"><p>کار منست ناله و اینست کار عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نو عاشقم سزد که دل چاک من بود</p></div>
<div class="m2"><p>اول گلی که می شکفد از بهار عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل بیا که وامق و مجنون گذشته اند</p></div>
<div class="m2"><p>برخاسته است خار و خس از رهگذار عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردا که هست دلبر من طفل و پیش او</p></div>
<div class="m2"><p>نی هست قدر عاشق و نه اعتبار عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلها اگر دمد ز سر خاک تربتم</p></div>
<div class="m2"><p>باشد هنوز در دل من خار خار عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دهر نیست روز خوش و روزگار خوش</p></div>
<div class="m2"><p>الا که روز عاشقی و روزگار عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسوا کنون نگشت فضولی ز عشق یار</p></div>
<div class="m2"><p>هرگز نبوده این که نبودست یار عشق</p></div></div>