---
title: >-
    شمارهٔ ۳۰۶
---
# شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>بیاد قد تو بر سینه هر الف که بریدم</p></div>
<div class="m2"><p>خطی ز کلک عدم بر وجود خویش کشیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسینه بی تو بسی داغهای تازه نهادم</p></div>
<div class="m2"><p>شکوفه عجب از نوبهار عشق بچیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وفا ز سرو قدی خواستم نگشت میسر</p></div>
<div class="m2"><p>زهی جفا که نشد بارور نهال امیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل نشاط منست از هوای وصل شگفته</p></div>
<div class="m2"><p>بسان غنچه ببوی تو جامه که بریدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>براه دوست غباری ندیدم از تن خاکی</p></div>
<div class="m2"><p>چه سرعتست که خود هم بگرد خود نرسیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشان نماند ز من در طلب ولیک چه حاصل</p></div>
<div class="m2"><p>که کس نداند نشانم از آنچه می طلبیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهر می شنود آه و ناله تو فضولی</p></div>
<div class="m2"><p>به آه و ناله تو در جهان کسی نشنیدم</p></div></div>