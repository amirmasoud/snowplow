---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>زهی جفای تو بر من دلیل رحمت خاص</p></div>
<div class="m2"><p>مرا وفای تو نقش صحیفه اخلاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدام مطرب بزم غم توام من مست</p></div>
<div class="m2"><p>سرود ناله من کرده چرخ را رقاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خراب باده عشقت ز ننگ عقل بری</p></div>
<div class="m2"><p>اسیر حلقه زلفت ز دام قید خلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جزای کشتن پروانه شمع را این بس</p></div>
<div class="m2"><p>که از نسیم دم صبح می رسد بقصاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلا و درد و غمت قدر داده اند مرا</p></div>
<div class="m2"><p>که نیست قیمت هر جنس جز بقدر خواص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم تو بود مشخص مرا دمی که هنوز</p></div>
<div class="m2"><p>نداشتند تعین هیاکل و اشخاص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث عشق فضولی به هیچ کس مگشا</p></div>
<div class="m2"><p>درون بحر نباید که دم زند غواص</p></div></div>