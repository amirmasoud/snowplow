---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>اگر بگذشت مجنون من بماندم یادگار او</p></div>
<div class="m2"><p>وگر شد کوهکن هم من کمر بستم به کار او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی خواهم که میرد در رهت اغیار می ترسم</p></div>
<div class="m2"><p>شود خاک و در آید باز در چشمم غبار او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بآب دیده ام افتاد عکس نوک مژگانش</p></div>
<div class="m2"><p>حذر ای مردم غافل ز تیغ آبدار او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندادم یک نفس از عمر خود کام از لبت جان را</p></div>
<div class="m2"><p>چه عمر است این که دارم چند باشم شرمسار او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آهم پر شرر شد چرخ کامم بر نمی آرد</p></div>
<div class="m2"><p>ز بیم آن که گردد ساده قصر زر نگار او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بهر جاه دنیا ترک دنیا می کند زاهد</p></div>
<div class="m2"><p>چرا گر ترک دنیا می فزاید اعتبار او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی کرد دوری اختیار از روضه کویت</p></div>
<div class="m2"><p>نرنجد تا سگت از ناله بی اختیار او</p></div></div>