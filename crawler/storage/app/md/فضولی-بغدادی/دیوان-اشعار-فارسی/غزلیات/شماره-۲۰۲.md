---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>می کشد زارم ببازی هر زمان طفلی دگر</p></div>
<div class="m2"><p>کرد دل بازیچه طفلان مرا پیرانه سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشک می ریزم چو از طفلان مرا سنگی رسد</p></div>
<div class="m2"><p>چون نهال بارور کز سنگ می ریزد ثمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نورسان را تا بفرزندی گزیدم در جهان</p></div>
<div class="m2"><p>رسم شد فرزند را مهری نباشد بر پدر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم من چون مردم بی مایه طفل اشک را</p></div>
<div class="m2"><p>متصل می پرورد اما بصد خون جگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه در دل می کند آن طفل گه در دیده جا</p></div>
<div class="m2"><p>نیست او را ذره از آب و از آتش حذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم از سیل سرشکم شد خراب اما چه سود</p></div>
<div class="m2"><p>دلبرم طفلست و او را نیست از عالم خبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاریند از حسن روزافزون جوانان وین سبب</p></div>
<div class="m2"><p>هست میل دل فضولی را به طفلان بیشتر</p></div></div>