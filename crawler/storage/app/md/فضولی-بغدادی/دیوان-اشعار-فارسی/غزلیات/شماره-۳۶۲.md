---
title: >-
    شمارهٔ ۳۶۲
---
# شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>دل که پنهان است شوق لعل محبوبان درو</p></div>
<div class="m2"><p>غنچه بشگفته است اوراق گل پنهان درو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خیال لعل جان بخش سواد دیده ام</p></div>
<div class="m2"><p>هست آن ظلمت که باشد چشمه حیوان درو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد بسودای سر زلف تو جسمم رشته ای</p></div>
<div class="m2"><p>صد گره افتاد از تاب غم دوران درو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر محنت راست گردابی پر از خاشاک و خس</p></div>
<div class="m2"><p>وادی عشقت که عشاقند سرگردان درو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناوکت بگذشت از جسمم چه جای راحت است</p></div>
<div class="m2"><p>با چنین جسمی که آرامی ندارد جان درو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در غم درج دهانت چون نباشم تنگ دل</p></div>
<div class="m2"><p>حقه گم کرده‌ام صد درد را درمان درو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست راحت بی‌غم جانان فضولی را دمی</p></div>
<div class="m2"><p>دم به دم آن به که افزاید غم جانان درو</p></div></div>