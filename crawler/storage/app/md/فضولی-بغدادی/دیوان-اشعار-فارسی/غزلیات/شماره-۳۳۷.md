---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>در غمم گر جان ز جسم ناتوان آید برون</p></div>
<div class="m2"><p>کی غم آن راحت جانم ز جان آید برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می مده ساقی مکن کاری که ناگه پیش خلق</p></div>
<div class="m2"><p>سر لعل او بمستی از زبان آید برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواستم کآرم خدنگش را برون از استخوان</p></div>
<div class="m2"><p>باز ترسیدم که مغز استخوان آید برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ندارد طاقت سوز درونم کاشکی</p></div>
<div class="m2"><p>خون شود وز راه چشم خون فشان آید برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر قد خم گشته ام رحمی بکن ز آهم بترس</p></div>
<div class="m2"><p>سرو من مگذار کین تیر از کمان آید برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی برون آمد شدم رسوای عالم اینچنین</p></div>
<div class="m2"><p>آه اگر امروز دیگر آنچنان آید برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نمودی رخ فضولی را مران از کوی خود</p></div>
<div class="m2"><p>بلبل گل دیده از گلشن چه سان آید برون</p></div></div>