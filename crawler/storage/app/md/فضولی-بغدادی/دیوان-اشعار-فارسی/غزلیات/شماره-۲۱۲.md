---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>غمت روز تنهاییم یار بس</p></div>
<div class="m2"><p>شبم هم نفس ناله زار بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا مایه خرمی روز غم</p></div>
<div class="m2"><p>دل خسته و جان افکار بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه کار آیدم قیدهای دگر</p></div>
<div class="m2"><p>دلم بسته زلف دلدار بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سریر سلامت چه جای منست</p></div>
<div class="m2"><p>مقامم سر کوی خمار بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارم بکار جهان هیچ کار</p></div>
<div class="m2"><p>مرا ترک کار جهان کار بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه حاجت بتیغ از پی کشتنم</p></div>
<div class="m2"><p>نگاهی ازان چشم خونخوار بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی ز لذات عالم مرا</p></div>
<div class="m2"><p>همان نشئه ذوق دیدار بس</p></div></div>