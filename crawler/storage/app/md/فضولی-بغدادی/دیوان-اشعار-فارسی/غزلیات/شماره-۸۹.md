---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>دل الفت تمام بآن خاک در گرفت</p></div>
<div class="m2"><p>خوش صحبتی میان دو افتاد در گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خونابه نیست بر مژه ام آتش دل است</p></div>
<div class="m2"><p>کز چاک سینه سر زد و در چشم تر گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون من بسیست باده کش بزم عشق لیک</p></div>
<div class="m2"><p>بودم تنک شراب مرا بیشتر گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شمع باز در سرم افتاد گرمی</p></div>
<div class="m2"><p>دل کرده بود ترک تعلق ز سر گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوق حریم روضه کوی تو داشت گل</p></div>
<div class="m2"><p>بگشاد دست و دامن باد سحر گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرهاد در زمانه من گشت کوهکن</p></div>
<div class="m2"><p>بگذاشت عاشقی پی کار دگر گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون خس فتاده بود فضولی بخاک ره</p></div>
<div class="m2"><p>او را نسیم لطف تو از خاک بر گرفت</p></div></div>