---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>گر نباشد قید آن گیسوی خم بر خم مرا</p></div>
<div class="m2"><p>کی بصد زنجیر بتوان داشت در عالم مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خیال آن پری خو کرده ام ناصح برو</p></div>
<div class="m2"><p>خوش نمی آید ملاقات بنی آدم مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه منم بی غم نه غم بی من دمی ایزد مگر</p></div>
<div class="m2"><p>آفرید از بهر من غم را و بهر غم مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی لب میگون آن گلرخ نمی یابم فرح</p></div>
<div class="m2"><p>گر شود جمشید ساقی می ز جام جم مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه دارم جسمی از سودای زلفت ناتوان</p></div>
<div class="m2"><p>من هلال اوج سودایم نه بینی کم مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو ستمکاری که از غم بر دلم داغی نهد</p></div>
<div class="m2"><p>دل گرفت ای همنشین از خاطر خرم مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناله دارد فضولی درد سر می آورد</p></div>
<div class="m2"><p>روز تنهایی نمی خواهم شود همدم مرا</p></div></div>