---
title: >-
    شمارهٔ ۳۱۸
---
# شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>آزارها ز یار جفا کار می کشم</p></div>
<div class="m2"><p>تا کار او جفاست من آزار می کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم می کشم ز یار و شکایت نمی کنم</p></div>
<div class="m2"><p>غم نیست چون غمیست که از یار می کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر من شدست این سبب طعنه دگر</p></div>
<div class="m2"><p>کز بهر یار طعنه اغیار می کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میلیست هر مژه که بآن جای توتیا</p></div>
<div class="m2"><p>گرد رهت بدیده خونبار می کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسیار کم چراست بمن التفات تو</p></div>
<div class="m2"><p>با آنکه من جفای تو بسیار می کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر یاد قامتت همه شب تا دم سحر</p></div>
<div class="m2"><p>آه دمادم از دل افگار می کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می می کشد رقیب فضولی ز جام وصل</p></div>
<div class="m2"><p>من در فراق حسرت دیدار می کشم</p></div></div>