---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>ماه من کز لعل لب کامی بهر ناکام داد</p></div>
<div class="m2"><p>خواستم من نیز کام خود مرا دشنام داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برد هوشم را بدشنامی لبش یا ساقی</p></div>
<div class="m2"><p>بهر مستی از می تلخی مرا یک جام داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مضطرب بودم که آیا چیست قدرم پیش او</p></div>
<div class="m2"><p>اضطرابم را بدشنامی لبش آرام داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم از ذوقی که در دشنام آن لب یافتم</p></div>
<div class="m2"><p>جان فدای نشأه کان باده گلفام داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چیست جرم من که مخصوصم بداغ هجر کرد</p></div>
<div class="m2"><p>آنکه بر خوان وصال خود صلای عام داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که هوشم برد گفتارش ندانستم که او</p></div>
<div class="m2"><p>وعده کشتن مرا یا مژده انعام داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با فضولی محنت ایام را کاری نماند</p></div>
<div class="m2"><p>بی خودی او را نجات از محنت ایام داد</p></div></div>