---
title: >-
    شمارهٔ ۴۰۷
---
# شمارهٔ ۴۰۷

<div class="b" id="bn1"><div class="m1"><p>چند ای دل نامه وصف بتان املا کنی</p></div>
<div class="m2"><p>ذکر خوبان پری رخسار مه سیما کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه زنی از غمزه مردم کش خون ریز دم</p></div>
<div class="m2"><p>گه زبان در مدح لعل در افشان گویا کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه ز شوق خال داغی بر دل پر خون نهی</p></div>
<div class="m2"><p>گاه فکر زلف را سرمایه سودا کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر زبان آری شکایت هر دم از جور بتان</p></div>
<div class="m2"><p>بی گناهی چند را هرجا رسی رسوا کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقت آن آمد کزین وضع پریشان بگذری</p></div>
<div class="m2"><p>باقی اوقات خود صرف ره تقوا کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر پری سوی تو آید چشم نگشایی برو</p></div>
<div class="m2"><p>چشم و دل را مطلع خورشید استغنا کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد فضولی شیوه رندی مکرر بعد ازین</p></div>
<div class="m2"><p>به که طور تازه طرز نوی پیدا کنی</p></div></div>