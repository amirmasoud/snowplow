---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>باز خونبارست مژگانم نمی‌دانم چرا</p></div>
<div class="m2"><p>اضطرابی هست در جانم نمی‌دانم چرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالمی بر حال من حیران و من بر حال خود</p></div>
<div class="m2"><p>مانده‌ام حیران که حیرانم نمی‌دانم چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزگاری شد که بدحال و پریشانم ولی</p></div>
<div class="m2"><p>بس که بدحال و پریشانم نمی‌دانم چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار می‌دانم که می‌داند دوای درد من</p></div>
<div class="m2"><p>لیک می‌گوید نمی‌دانم نمی‌دانم چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی وصالم می‌رهاند از مصیبت نی فراق</p></div>
<div class="m2"><p>در همه اوقات گریانم نمی‌دانم چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست کاری کآید از من هر طرف بی‌اختیار</p></div>
<div class="m2"><p>می‌دواند چرخ گردانم نمی‌دانم چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد خود را گرچه می‌دانم فضولی مهلک است</p></div>
<div class="m2"><p>فارغ از تدبیر درمانم نمی‌دانم چرا</p></div></div>