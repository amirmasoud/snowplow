---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>چه گونه فاش نگردد غم نهانی ما</p></div>
<div class="m2"><p>بشرح حال زبانیست بی زبانی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برون مباد زمانی ز جان ما غم یار</p></div>
<div class="m2"><p>که در بلا غم یارست یار جانی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سرشک بپای تو ریختیم و خوشیم</p></div>
<div class="m2"><p>که صرف راه تو شد نقد زندگانی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکست بار غمت قد ما چه سنگ دلی</p></div>
<div class="m2"><p>که هیچ رحم نکردی به ناتوانی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانه دشمن ما گشت در غمت گویا</p></div>
<div class="m2"><p>که رشک برد بر ایام شادمانی ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شدیم سالک راه وفات لیک چه سود</p></div>
<div class="m2"><p>که عمر تاب ندارد به همعنانی ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسیده ایم فضولی ز فیض عشق کام</p></div>
<div class="m2"><p>بس است درد و غم اسباب کامرانی ما</p></div></div>