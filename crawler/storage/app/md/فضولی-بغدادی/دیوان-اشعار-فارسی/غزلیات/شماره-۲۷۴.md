---
title: >-
    شمارهٔ ۲۷۴
---
# شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>آتشین رویی کز او چون شمع با چشم ترم</p></div>
<div class="m2"><p>زنده خواهم شد پس از مردن گر آید بر سرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوختم ناصح مده پندم مبادا کز دمت</p></div>
<div class="m2"><p>بر فروزد آتشی گر هست در خاکسترم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خار خاکستر شود ز آتش منم آن آتشی</p></div>
<div class="m2"><p>کز جفا خاکسترم گشتست خار بسترم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم چشمم ز تاب مهر رخسارت گداخت</p></div>
<div class="m2"><p>هست بیم صد بلا زین احتراق اخترم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی رخش بر سوز من گر شمع خندد دور نیست</p></div>
<div class="m2"><p>آتشی نادیده می سوزد تن غم پرورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعله است از چاکهای پهلوی من سر زده</p></div>
<div class="m2"><p>یا منم پروانه بگرفتست آتش بر تنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کفر می خوانند بی دردان فضولی عشق را</p></div>
<div class="m2"><p>گر درین اهل ریا اسلام باشد کافرم</p></div></div>