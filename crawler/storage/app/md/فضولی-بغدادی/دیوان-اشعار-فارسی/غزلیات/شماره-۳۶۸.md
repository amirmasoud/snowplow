---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>ای مست غافل از من و خونین جگر مشو</p></div>
<div class="m2"><p>من از تو بی خودم تو ز من بی خبر مشو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کارم بسوز و گریه فتادست در غمت</p></div>
<div class="m2"><p>غافل ز جان سوخته و چشم تر مشو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترسم که بی خبر شوی از حال عاشقان</p></div>
<div class="m2"><p>ای مست ناز می مخور و مستتر تر مشو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای سرو با نظاره روی تو زنده ام</p></div>
<div class="m2"><p>می میرم از فراق تو دور از نظر مشو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر از رکی نماند ز ضعفم بر استخوان</p></div>
<div class="m2"><p>با من مگو که تیر بلا را سپر مشو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل بس است بر من بی دل بلای عشق</p></div>
<div class="m2"><p>رو از برم تو نیز بلای دگر مشو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از راه عشق خیز فضولی که فتنه خاست</p></div>
<div class="m2"><p>زین بیشتر مقید این رهگذر مشو</p></div></div>