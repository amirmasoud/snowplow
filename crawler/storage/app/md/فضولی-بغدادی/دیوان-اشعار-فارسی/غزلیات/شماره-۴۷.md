---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>نهفتن در دل و جان درد و داغ آن پریوش را</p></div>
<div class="m2"><p>توانم گر توان پوشید با خاشاک آتش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سینه آه حسرت می کشم چون تیر از ترکش</p></div>
<div class="m2"><p>که کی سازد تهی بر سینه ام آن ترک ترکش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منقش گشت رخسارم بخون چون لاله زار آن به</p></div>
<div class="m2"><p>که مالم بر کف پای تو رخسار منقش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلا زهد ریایی هیچ کس را خوش نمی آید</p></div>
<div class="m2"><p>شعار خود مکن بهر خدا این وضع ناخوش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی دیدم که در زلف تو دل سرگشته می گردد</p></div>
<div class="m2"><p>نمی دانم چه تعبیرست این خواب مشوش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهی جورست و گه کم التفاتی کار آن بدخو</p></div>
<div class="m2"><p>بدور او بلا کم نیست عشاق بلاکش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی چند در بند جهات مختلف مانی</p></div>
<div class="m2"><p>ز غم بگذار تا بر هم زند ضعف تو هر شش را</p></div></div>