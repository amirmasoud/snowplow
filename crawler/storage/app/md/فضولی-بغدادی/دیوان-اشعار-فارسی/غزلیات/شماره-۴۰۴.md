---
title: >-
    شمارهٔ ۴۰۴
---
# شمارهٔ ۴۰۴

<div class="b" id="bn1"><div class="m1"><p>از شرم رخت منزل یوسف شده چاهی</p></div>
<div class="m2"><p>در روی زمین نیست برخسار تو ماهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من مایل آنم که کنی میل من اما</p></div>
<div class="m2"><p>مشکل که کند میل گدایی چو تو شاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چشم فتادم بتو هر گاه که گفتم</p></div>
<div class="m2"><p>دارم طمع گوشه چشمی ز تو گاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جان حزینم بنگاهی ز تو خرسند</p></div>
<div class="m2"><p>آزرده چرا می شوی از من بنگاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دست تو گر ریخته شد خون دل ما</p></div>
<div class="m2"><p>ما دل بتو دادیم ترا نیست گناهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی از سر کوی تو همان به که نتابیم</p></div>
<div class="m2"><p>غیر از سر کوی تو مرا نیست پناهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهی که شود چشم و دلت پاک فضولی</p></div>
<div class="m2"><p>بی سیل سرشکی مشو آتش آهی</p></div></div>