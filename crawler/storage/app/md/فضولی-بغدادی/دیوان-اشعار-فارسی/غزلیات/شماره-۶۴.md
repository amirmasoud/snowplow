---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>صیقل آیینه دلها نم چشم ترست</p></div>
<div class="m2"><p>هر کرا نمناک تر دیده دلش روشن ترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز نومیدی مراد از قطرهای اشک جو</p></div>
<div class="m2"><p>رهبر گم گشتگان در ظلمت شب اخترست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه کن آب چشمی ریز گر صاحب دلی</p></div>
<div class="m2"><p>کآدمی بی اشکی و آهی درخت بی برست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل که مملو از هوای دوست باشد چون حباب</p></div>
<div class="m2"><p>جلوه اش بالای دریای سپهر اخضرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهره زردی نما در عشق کین رنگ لطیف</p></div>
<div class="m2"><p>کارسازیهای بازار محبت را زرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع گر پرورد آتش را سزای خویش یافت</p></div>
<div class="m2"><p>نیست خالی از ندامت هر که دشمن پرورست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون حباب از اشک جابر آب دارد متصل</p></div>
<div class="m2"><p>تا فضولی را هوای سرو قدت در سرست</p></div></div>