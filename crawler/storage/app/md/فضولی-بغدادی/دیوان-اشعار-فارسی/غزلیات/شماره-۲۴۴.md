---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>ای دل از دیده فزون دیده ز دل سوی تو مایل</p></div>
<div class="m2"><p>دلم از دیده ترا می طلبد دیده ام از دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه عجب میل تو از سادگی مردم چشم</p></div>
<div class="m2"><p>تو بلایی چه شناسند ترا مردم غافل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش از سنگ بشستن نرود چند بگریم</p></div>
<div class="m2"><p>چو ستم یافت رقم در دلت از گریه چه حاصل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو خط نامه که خواهند بشویند ز اشکم</p></div>
<div class="m2"><p>نشده محو دل از دل نشود نقش تو زایل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالمی گریه کنان بر غم من در غم عشقت</p></div>
<div class="m2"><p>من بدان شاد که گشتم بغم عشق تو قایل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست در سلسله های خم زلفت دل سوزان</p></div>
<div class="m2"><p>هست آویخته قندیل فروزان بسلاسل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست در دهر کسی قابل تمکین اقامت</p></div>
<div class="m2"><p>جز فضولی که سر کوی ترا ساخته منزل</p></div></div>