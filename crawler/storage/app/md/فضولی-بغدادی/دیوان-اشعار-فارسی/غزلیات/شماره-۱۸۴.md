---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه غم سیم‌بری داشته باشد</p></div>
<div class="m2"><p>وز غم همه‌دم چشم تری داشته باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاحب‌نظر آنست که چون چشم گشاید</p></div>
<div class="m2"><p>با ماه‌لقایی نظری داشته باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثابت‌قدم آنست که غافل ننشیند</p></div>
<div class="m2"><p>در کوی محبت گذری داشته باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر بی‌خبری را چه کنم بنده آنم</p></div>
<div class="m2"><p>کز حال دل من خبری داشته باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با هیچ هنر نیست پسندیده من کس</p></div>
<div class="m2"><p>گر عشق بورزد هنری داشته باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سهلست فراغت سگ آنم که همیشه</p></div>
<div class="m2"><p>غمگین‌دل و خونین‌جگری داشته باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان نذر غم عشق تو کردست فضولی</p></div>
<div class="m2"><p>حاشا که خیال دگری داشته باشد</p></div></div>