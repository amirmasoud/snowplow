---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>با هر که غیرتست نگاهی نکرده ایم</p></div>
<div class="m2"><p>ما را چه می کشی چو گناهی نکرده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شعله برون نشود ز آتش درون</p></div>
<div class="m2"><p>هرگز ز درد عشق تو آهی نکرده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یارب چرا ز ماه و شان خالیست شهر</p></div>
<div class="m2"><p>ماهیست ما نظاره ماهی نکرده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز نقد شوق و دولت عشق تو در جهان</p></div>
<div class="m2"><p>هرگز نظر بمالی و جاهی نکرده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیغ زبان ماست که عالم گرفته است</p></div>
<div class="m2"><p>ما فتح کشوری بسپاهی نکرده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یارب چرا شدست سیه روزگار ما</p></div>
<div class="m2"><p>ظلمی بهیچ خانه سیاهی نکرده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را ز دهر نیست فضولی تمتعی</p></div>
<div class="m2"><p>زین باغ میل برگ گیاهی نکرده ایم</p></div></div>