---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>طعنه اغیار بهر یار می باید کشید</p></div>
<div class="m2"><p>یار باید طعنه اغیار می باید کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ یاری بی جفای طعنه اغیار نیست</p></div>
<div class="m2"><p>بهر یک گل محنت صد خار می باید کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیج مقصودی میسر نیست بی آزار دل</p></div>
<div class="m2"><p>بهر هر مقصود صد آزار می باید کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بدست آرد مسافر از منافع اندکی</p></div>
<div class="m2"><p>در سفرها محنت بسیار می باید کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدن آغیار با یارست نوعی از بلا</p></div>
<div class="m2"><p>عاشقان را این بلد ناچار می باید کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک کار عاشقی باید گرفتن بعد ازین</p></div>
<div class="m2"><p>چند سرگردانی این کار می باید کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر میسر هم شود عشاق را دیدار یار</p></div>
<div class="m2"><p>انتظار وعده دیدار می باید کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محنت عالم فضولی کرد ما را تنگدل</p></div>
<div class="m2"><p>پا ازین ویرانه خونخوار می باید کشید</p></div></div>