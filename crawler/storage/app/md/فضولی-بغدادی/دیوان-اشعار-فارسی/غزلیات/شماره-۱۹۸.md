---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>به درد و محنت بسیار ما را یار می‌داند</p></div>
<div class="m2"><p>ولی کم می‌کند اظهار آن بسیار می‌داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگو با من چه ربطیست این که با دلدار دارد دل</p></div>
<div class="m2"><p>که آن سریست دل می‌داند و دلدار می‌داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازو دیدم وفا تا گریه شد کارم بحمدالله</p></div>
<div class="m2"><p>فتاده با کسی کارم که قدر کار می‌داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مقدار محبت می‌نماید لطف با هرکس</p></div>
<div class="m2"><p>غلام طبع آن طفلم که این مقدار می‌داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدی گر از حسد اغیار گوید پیش یار از ما</p></div>
<div class="m2"><p>چه غم چون یار ما را بهتر از اغیار می‌داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز من پرسید محنت‌های سودای سر زلفش</p></div>
<div class="m2"><p>که اندوه شب تاریک را بیمار می‌داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی راز دل را من چه حاجت بر زبان آرم</p></div>
<div class="m2"><p>چو دلبر هرچه دارم در دل افکار می‌داند</p></div></div>