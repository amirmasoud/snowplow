---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>چنان بنهفته ضعف تن مرا لطف بدن او را</p></div>
<div class="m2"><p>که رفته عمرها نی او مرا دیده نه من او را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز درد عشق و داغ هجر می نالم خوش آن رندی</p></div>
<div class="m2"><p>که نی اندیشه جانست و نی پروای تن او را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمت در سینه دارم شمع را کی سوز من باشد</p></div>
<div class="m2"><p>ندارد جسم او جانی چه باک از سوختن او را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارد بر زبان جز راز عشقت شمع می دانم</p></div>
<div class="m2"><p>که آخر گشته بیرون می برند از انجمن او را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکوه بیستون نقشی که دیدی نیست جز شیرین</p></div>
<div class="m2"><p>زده بر سنگ از رشک جمالت کوهکن او را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بت است آن سنگدل این بس کمال معجز عشقم</p></div>
<div class="m2"><p>که می آرم باظهار تظلم در سخن او را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی سوخت بر تن داغ‌های تازه سر تا پا</p></div>
<div class="m2"><p>که نشناسند در کوی تو از داغ کهن او را</p></div></div>