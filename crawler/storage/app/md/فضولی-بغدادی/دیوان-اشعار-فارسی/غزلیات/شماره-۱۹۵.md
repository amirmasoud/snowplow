---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>بخاک بنده رحم ای دلربا از تو نمی آید</p></div>
<div class="m2"><p>تو سنگین دل بنی کار خدا از تو نمی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه سنگین دل کسی کز ناله و آهم نمی ترسی</p></div>
<div class="m2"><p>ز سنگ خاره می آید صدا از تو نمی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طریق مهربانی خوب می باشد ز محبوبان</p></div>
<div class="m2"><p>تو هم محبوبی این خوبی چرا از تو نمی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی آری ترحم بر من و سویم نمی آیی</p></div>
<div class="m2"><p>ترا دانسته ام این کارها از تو نمی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوی ما نامه می خواستم هر لحظه بفرستی</p></div>
<div class="m2"><p>چه حاصل آنچه می خواهیم ما از تو نمی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وفای خود مگر صرف رقیبان کرده ای گل</p></div>
<div class="m2"><p>که من جستم بسی بوی وفا از تو نمی آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی بگذر از قید ورع می نوش و رندی کن</p></div>
<div class="m2"><p>طریق زهد و آیین ربا از تو نمی آید</p></div></div>