---
title: >-
    شمارهٔ ۳۹۷
---
# شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>در کبودی فلک چون مه من نیست مهی</p></div>
<div class="m2"><p>بر سر هیچ مهی نیست هلال سیهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن از آه نشد ظلمت نومیدی ما</p></div>
<div class="m2"><p>وه که مردیم و نبردیم به وصل تو رهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو ربودی دل و دینم عوضی کن بوصال</p></div>
<div class="m2"><p>بگدایی چه روا ظلم کند چون تو شهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که داری گه و بی گاه نظرها برقیب</p></div>
<div class="m2"><p>می توان جانب ما هم نگهی کرد گهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هدف تیر تو گشتیم که از گوشه چشم</p></div>
<div class="m2"><p>گاه گاهی کنی از دور سوی ما نگهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جور کردی بمن ای ماه بترس از آهم</p></div>
<div class="m2"><p>من نه آنم ز من آزرده شوی بی گنهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرد در سعی فضولی و بجایی نرسید</p></div>
<div class="m2"><p>ز آن که دریای غم عشق ترا نیست تهی</p></div></div>