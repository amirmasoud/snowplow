---
title: >-
    شمارهٔ ۳۴۲
---
# شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>تو تیر افکنده ای چرخ مهر خود بماه من</p></div>
<div class="m2"><p>رقیبم گشته مشکل که کردی نیک خواه من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر بی داد من داری فلک بر گرد زین عادت</p></div>
<div class="m2"><p>نه بر من رحم بر خود کن بترس از برق آه من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه حالست این گه هر که سوی آن خورشید ره جستم</p></div>
<div class="m2"><p>مرا چون سایه دامن گیر شد بخت سیاه من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا در ظلمت غم میل طوف خاک آن در شد</p></div>
<div class="m2"><p>چراغی بر فروز ای آتش دل پیش راه من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکو رویان ز من برگشته اند آیا چه بد کردم</p></div>
<div class="m2"><p>چه باشد موجب رنجش چه شد یارب گناه من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک را گر بلایی هست بر من می شود نازل</p></div>
<div class="m2"><p>چه سلطانم که آسودست عالم در پناه من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی قید عقل از من مجو من بنده عشقم</p></div>
<div class="m2"><p>مطیعم تا چه فرماید چه گوید پادشاه من</p></div></div>