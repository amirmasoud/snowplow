---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>ملک را گر نظر بر قد آن سرو روان افتد</p></div>
<div class="m2"><p>سراسیمه چو مرغ تیر خورده ز آسمان افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مپوش از من رخ ای خورشید مپسند این که چون شمعم</p></div>
<div class="m2"><p>همه شب تا سحر آتش بمغز استخوان افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگو ای دل بشمع محفل از سوز درون حرفی</p></div>
<div class="m2"><p>مکن کاری که این راز نهان در هر زمان افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلا را نیست قابل جز صفای دل عجب نبود</p></div>
<div class="m2"><p>اگر زان شمع چون آیینه ام آتش بجان افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکندی زلف بر رخ اضطرابی کرد دل پیدا</p></div>
<div class="m2"><p>چو مرغی کش بناگه آتش اندر آشیان افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بمقصد راه کم جو از رکوع ای زاهد گم ره</p></div>
<div class="m2"><p>که بسیار آزمودم تیر کج کم بر نشان افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی صبح سان دم می زنی از مهر رخسارش</p></div>
<div class="m2"><p>نمی‌ترسی از آن دم کز تو آتش در جهان افتد</p></div></div>