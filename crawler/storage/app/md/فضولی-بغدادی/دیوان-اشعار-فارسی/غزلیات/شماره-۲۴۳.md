---
title: >-
    شمارهٔ ۲۴۳
---
# شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>ز حد گذشت بدور تو بی قراری دل</p></div>
<div class="m2"><p>مشو ز حال دل بی قرار من غافل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که معتقد عشق نیست نیست کسی</p></div>
<div class="m2"><p>مذاق نشأه عشق است قابل قابل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هر چه هست توانم برید میل ولی</p></div>
<div class="m2"><p>نمی شود که نباشم بمهوشی مایل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مراست مشکلی از عشق و چشم آن دارم</p></div>
<div class="m2"><p>که مشکلم بگشاید ز تو ولی مشکل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که کرد دعوی صبر و ثبات در عشقت</p></div>
<div class="m2"><p>که چون نقاب گرفتی ز رخ نگشت خجل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حریم کعبه که بر ماست طوف آن واجب</p></div>
<div class="m2"><p>ز شوق طوف درت مانده است پا در گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی از سر آن کو قدم منه بیرون</p></div>
<div class="m2"><p>که هیچ جا نبود زین شریف‌تر منزل</p></div></div>