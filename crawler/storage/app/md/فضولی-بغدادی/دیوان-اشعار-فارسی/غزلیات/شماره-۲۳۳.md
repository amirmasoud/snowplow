---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>قد کشیدی دیده ام تیر بلا را شد هدف</p></div>
<div class="m2"><p>جلوه کردی عنان اختیارم شد ز کف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می نهد سر هر سحر بر خاک راهت آفتاب</p></div>
<div class="m2"><p>جای آن دارد که سر بر چرخ ساید زین شرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان را دوش ذوق ماه نو در چرخ داشت</p></div>
<div class="m2"><p>گوشه ابرو نمودی ذوق آن شد بر طرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیرت لعل تو در کان لعل را در خون نشاند</p></div>
<div class="m2"><p>آب شد از شرم دندان تو لؤلؤ در صدف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سینه ام را سوخت دل وز ناله ام پیداست این</p></div>
<div class="m2"><p>بیشتر دارد فغان هرگه که آتش دید دف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر طرف صف بست مژگانم بقصد خیل خواب</p></div>
<div class="m2"><p>جلوه‌ها دارد سرشکم در میان هر دو صف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق ورز و جام می درکش فضولی متصل</p></div>
<div class="m2"><p>خیز و کاری کن مکن بیهوده عمر خود تلف</p></div></div>