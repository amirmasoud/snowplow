---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>عهد کردم که دیگر بیهده کاری نکنم</p></div>
<div class="m2"><p>سوی خوبان جفا پیشه گذاری نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردم از عشق بتان توبه چه خواهد بودن</p></div>
<div class="m2"><p>غایتش این که دگر ناله زاری نکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند بیداد رقیبان بداندیش کشم</p></div>
<div class="m2"><p>به از آن نیست که میلی بنگاری نکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بکی زحمت اغیار کشم می خواهم</p></div>
<div class="m2"><p>بعد ازین آرزوی صحبت یاری نکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکناری کشم از صحبت رندان خود را</p></div>
<div class="m2"><p>ز بتان آرزوی بوس و کناری نکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرهم داغ دل از فیض فراغت سازم</p></div>
<div class="m2"><p>هوس عاشقی لاله عذاری نکنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعد ازین مصلحت اینست که کنجی گیرم</p></div>
<div class="m2"><p>چو فضولی هوس باغ و بهاری نکنم</p></div></div>