---
title: >-
    شمارهٔ ۳۷۱
---
# شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>شد دلم صد پاره و چون لاله بر هر پاره‌ای</p></div>
<div class="m2"><p>سوختم داغی ز عشق آتشین‌رخساره‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد دلم خون تا شود فارغ ز سودای بتان</p></div>
<div class="m2"><p>وه که دارد باز هرسو قصد او خونخواره‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر درمان درد سر دادن طبیبان را چه سود</p></div>
<div class="m2"><p>چون مریض عشق جز مردن ندارد چاره‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز بی‌دردی بود غافل ز من آن هم خوش است</p></div>
<div class="m2"><p>تا به کام دل کنم در روی او نظاره‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر آزارم رقیب آن تندخو را تیز کرد</p></div>
<div class="m2"><p>آهنی افروخت آتش بهر من از خاره‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیرت حال من انجم را ز گشتن باز داشت</p></div>
<div class="m2"><p>تا نماند غیر اشکم کوکب سیاره‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی فضولی راست سر منزل سر کویت همین</p></div>
<div class="m2"><p>سر بدان جا می‌نهد هر جا که هست آواره‌ای</p></div></div>