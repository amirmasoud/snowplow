---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>سوخت دل صد قطره خون در چشم تر دارد هنوز</p></div>
<div class="m2"><p>مرد آتش شعله با صد شرر دارد هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمها بگشاده بر رویم ز خوناب جگر</p></div>
<div class="m2"><p>بر نگشته دل ز من با من نظر دارد هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغهای تازه از جسم ضعیفم کم نشد</p></div>
<div class="m2"><p>الله الله این درخت خشک بردارد هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برکشیدم آه لیکن درد دل تسکین نیافت</p></div>
<div class="m2"><p>تیر بیرون رفته پیکان در جگر دارد هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خالی از تصویر مجنون نیست لوح روزگار</p></div>
<div class="m2"><p>نیست چون من او درین عالم اثر دارد هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم ای بی درد در عشق تو بیخود گشته ام</p></div>
<div class="m2"><p>گفت عاشق نیست این از خود خبر دارد هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد دو تا قد فضولی از غم گردون ولی</p></div>
<div class="m2"><p>میل ابروی بتان سیمبر دارد هنوز</p></div></div>