---
title: >-
    شمارهٔ ۲۷۸
---
# شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>بسته شد بر رشته جان موی گیسوی توام</p></div>
<div class="m2"><p>می کشد هر سو که می افتد گره سوی توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که می گردد بگرد ماه رخسارت ز رشک</p></div>
<div class="m2"><p>کرد مانند سر مویی سر موی توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل از تأثیر مهرست این که همچون ماه نو</p></div>
<div class="m2"><p>می فزاید متصل سودای ابروی توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیر رشک است این که زد خورشید بر من روز وصل</p></div>
<div class="m2"><p>یا بسر افکند سایه قد دلجوی توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرد بیرون از سرم کویت هوای روضه را</p></div>
<div class="m2"><p>حسرتی نگذاشت در جانم سگ کوی توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو گلی من خار عمری دادمت خون از جگر</p></div>
<div class="m2"><p>چون شگفتی بی نصیب از دیدن روی توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوق بد خوییست هر ساعت فضولی در سرت</p></div>
<div class="m2"><p>خوی بد داری بسی آزرده خوی توام</p></div></div>