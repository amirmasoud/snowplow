---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>ماه من نخل قدت سرو خرامان منست</p></div>
<div class="m2"><p>سرو من ماه رخت شمع شبستان منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کند حال مرا هجر تو بد وصل تو خوش</p></div>
<div class="m2"><p>هجر تو درد من و وصل تو درمان منست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل اسیر قد و جان مست می لعل تو شد</p></div>
<div class="m2"><p>قامتت کام دل و لعل لبت جان منست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور بادا قد جانان من از چشم بدان</p></div>
<div class="m2"><p>سرو گلزار نکویی قد جانان منست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم ای شمع بتان جای دلم دام بلاست</p></div>
<div class="m2"><p>گفت کان دام بلا دور زنخدان منست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده از راز دل زار من افتاد و سبب</p></div>
<div class="m2"><p>چشم گریان من و چاک گریبان منست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برد آرام فضولی قد آن سرو روان</p></div>
<div class="m2"><p>گرچه آرام دل زار پریشان منست</p></div></div>