---
title: >-
    شمارهٔ ۳۳۹
---
# شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>می شود هر دم جنون ما ز ابرویت فزون</p></div>
<div class="m2"><p>هست ابروی تو ما را سر خط مشق جنون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل قدت را دید لعلت راست مایل مدتیست</p></div>
<div class="m2"><p>می کشم از دل ملامت می خورم از دیده خون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده می ریزد درون از چاکهای سینه ام</p></div>
<div class="m2"><p>دل ز راه دیده هر خونی که می آرد برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق از حال دل پر خون چه حاجت دم زند</p></div>
<div class="m2"><p>می توان دانست از رنگ سرشگ لاله گون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشته پیوند خود با تارهای زلف او</p></div>
<div class="m2"><p>کرده ام محکم ولی می ترسم از بخت زبون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پنبه ننهاد کس بر داغهای سینه ام</p></div>
<div class="m2"><p>کآتشی در سینه اش نگرفت از سوز درون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر دنیا منت دونان فضولی تا به کی</p></div>
<div class="m2"><p>دل به عالی‌همتی بردار از دنیای دون</p></div></div>