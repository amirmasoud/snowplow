---
title: >-
    شمارهٔ ۳۷۲
---
# شمارهٔ ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>شانه ای گل بخم طره طرار منه</p></div>
<div class="m2"><p>بستر راحت دلهاست درو خار منه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پایمالم مکن ای قامت خم مژگان را</p></div>
<div class="m2"><p>خار زیر قدمم از پی آزار منه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر آن زلف مکش بی ادب ای مشاطه</p></div>
<div class="m2"><p>دست بی باک چنین در دهن مار منه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیر صحرای بلا شیوه سر بازانست</p></div>
<div class="m2"><p>پای تقلید درین وادی خون خوار منه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرسان از بدی کار کدورت بر دل</p></div>
<div class="m2"><p>داغ صد دغدغه بر سینه افگار منه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای قضا بر خط رخسار بتان گاه رقم</p></div>
<div class="m2"><p>نقطه جز مردمک چشم من زار منه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می رسد کار بتدریج فضولی بکمال</p></div>
<div class="m2"><p>بهر تقوی قدح از دست بیک بار منه</p></div></div>