---
title: >-
    شمارهٔ ۳۸۷
---
# شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>گر خدنگ غمزه را زین سان دمادم می‌زنی</p></div>
<div class="m2"><p>کشته گردد عالمی تا چشم بر هم می‌زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست ممکن بیش ازین بیداد گر سنگین‌دلی</p></div>
<div class="m2"><p>بر وفاداران خود سنگ جفا کم می‌زنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانه در دام بهر صید مرغی می‌نهی</p></div>
<div class="m2"><p>یا به قصد دل گره بر زلف پرخم می‌زنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این که داری در غمش ای دل صدای گریه نیست</p></div>
<div class="m2"><p>خنده بر غفلت دل‌های بی‌غم می‌زنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که در سر ذوق جام وصل داری نیست دور</p></div>
<div class="m2"><p>گر ز مستی سنگ رد بر ساغر جم می‌زنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع شام فرقتم بگذار تا سوزم رفیق</p></div>
<div class="m2"><p>می‌کشم خود را اگر از منع من دم می‌زنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برگزیدی از همه عالم فضولی فقر را</p></div>
<div class="m2"><p>دولتی داری که استغنا به عالم می‌زنی</p></div></div>