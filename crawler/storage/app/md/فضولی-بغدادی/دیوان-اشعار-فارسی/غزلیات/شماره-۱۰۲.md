---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>عاشقی رونق ز اطوار من حیران گرفت</p></div>
<div class="m2"><p>عشق از فرهاد صورت یافت از من جان گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا در آرد نقش شیرین را بمهمانی درو</p></div>
<div class="m2"><p>خانه در بیستون فرهاد سرگردان گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر سر دعوی ندارد بهر خون کوهکن</p></div>
<div class="m2"><p>بیستون را صورت شیرین چرا دامان گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست لاله کوهکن انداخت سوی بیستون</p></div>
<div class="m2"><p>سینه پر خون که از داغ دل سوزان گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه مشکل بود بر فرهاد کار بیستون</p></div>
<div class="m2"><p>جان شیرین داد بر خود کار را آسان گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بخون شد غرق با تیر تو از سوز درون</p></div>
<div class="m2"><p>سوخت در تن آتشی از شعله در پیکان گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دید سرگردانی سیاح صحرای امید</p></div>
<div class="m2"><p>بهر آسایش فضولی دامن حرمان گرفت</p></div></div>