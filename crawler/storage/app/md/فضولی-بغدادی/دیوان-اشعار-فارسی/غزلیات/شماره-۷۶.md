---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>ای طربخانه دل خلوت سلطان غمت</p></div>
<div class="m2"><p>پرده دیده سراپرده خاک قدمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وعده داد مرا ماه من امشب ای صبح</p></div>
<div class="m2"><p>بخدا گر همه صدقست نگه داردمت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعلاجی دگرم حال مگردان که مرا</p></div>
<div class="m2"><p>ساز کارست بسی شربت ذوق المت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سزد ار دم زند از سلطنت روی زمین</p></div>
<div class="m2"><p>خاکساری که بود گرد حریم حرمت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفسی نیست که در رشته جانم گرهی</p></div>
<div class="m2"><p>نفتد از شکن سلسله خم بخمت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریه در دیده ز بیداد تو آبی نگذاشت</p></div>
<div class="m2"><p>عذر خواه کمی ماست کمال کرمت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کند منع تو از قتل فضولی اغیار</p></div>
<div class="m2"><p>این چه ظلم است بران کشته تیغ ستمت</p></div></div>