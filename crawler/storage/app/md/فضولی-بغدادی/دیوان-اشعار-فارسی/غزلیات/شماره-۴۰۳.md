---
title: >-
    شمارهٔ ۴۰۳
---
# شمارهٔ ۴۰۳

<div class="b" id="bn1"><div class="m1"><p>مراست هر طرف از سیل اشک دریایی</p></div>
<div class="m2"><p>کجا روم چه کنم ره نمی برم جایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی کنند بتان میل عشق بازان حیف</p></div>
<div class="m2"><p>که ضایع است هنر نیست کارفرمایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکایت غم عشق از کسی نمی شنوم</p></div>
<div class="m2"><p>مگر که نیست درین شهر ماه سیمایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا حریف جنون منند مردم شهر</p></div>
<div class="m2"><p>من و مصاحبت آهوان صحرایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه من همین سر سودای زلف او دارم</p></div>
<div class="m2"><p>سری کجاست که خالی بود ز سودایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین که کار تو عاشق کشیست هر ساعت</p></div>
<div class="m2"><p>نمی شود سر کوی تو بی تماشایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا فضولی بی دل قرار چون گیرد</p></div>
<div class="m2"><p>که یک دلست درو هر زمان تمنایی</p></div></div>