---
title: >-
    شمارهٔ ۳۴۳
---
# شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>شد چاک چاک سینه و از قطرهای خون</p></div>
<div class="m2"><p>افتاد پاره پاره دل از چاکها برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خونست قطره قطره که از دیده می چکد</p></div>
<div class="m2"><p>یا هست هر یکی شرری ز آتش درون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دادم بذکر لعل تو تسکین دود آه</p></div>
<div class="m2"><p>دفع گزند مار توان کرد بافسون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آواره تا بکی کندم عقل هرزه کرد</p></div>
<div class="m2"><p>خواهم مرا بسلسله خود کشد جنون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی صورتی قرار ندارد دمی دلم</p></div>
<div class="m2"><p>در کار عشق کم نتوان شد ز بیستون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر غیر من نمی رسد ای چرخ جور تو</p></div>
<div class="m2"><p>گویا میان خلق مرا دیده زبون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از من مکن سؤال فضولی که چیست حال</p></div>
<div class="m2"><p>حال دلم قیاس کن از اشک لاله گون</p></div></div>