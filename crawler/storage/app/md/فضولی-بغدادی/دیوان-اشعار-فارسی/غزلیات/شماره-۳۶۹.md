---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>از آن دو پاره بانگشت معجزت شد ماه</p></div>
<div class="m2"><p>که باشد از پی اثبات دعویت دو گواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکاف ماه ز انگشت تست یا در سیر</p></div>
<div class="m2"><p>میان دایره مه فکند رخش تو راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلام راست نزول از فلک تراست عروج</p></div>
<div class="m2"><p>عیار قد همین بس بمردم آگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی دمد ثمری بی گل شهادت تو</p></div>
<div class="m2"><p>نهال اشهد ان لا اله الا الله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمال قدر همین بس که وقت عرض کمال</p></div>
<div class="m2"><p>ز کسر ماه تمامت فزود رتبه جاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تویی کفیل چه باک از عذاب امت را</p></div>
<div class="m2"><p>تو شفیع چه اندیشه خلق را ز گناه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شها فضولی بی دل گدای درگه تست</p></div>
<div class="m2"><p>ز عین لطف تو دارد همیشه چشم نگاه</p></div></div>