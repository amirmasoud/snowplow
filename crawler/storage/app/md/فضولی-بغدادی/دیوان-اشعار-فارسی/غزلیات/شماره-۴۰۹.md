---
title: >-
    شمارهٔ ۴۰۹
---
# شمارهٔ ۴۰۹

<div class="b" id="bn1"><div class="m1"><p>نپرسد از من بی کس درین دیار کسی</p></div>
<div class="m2"><p>کسی نیم که ز من گیرد اعتبار کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشرط صبر بیوسف چو می رسد یعقوب</p></div>
<div class="m2"><p>چرا کند گله از دور روزگار کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو هست محنت هجران بقدر مدت عمر</p></div>
<div class="m2"><p>چرا بوصل نباشد امیدوار کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل مراد بر آرد اگر دهد آبی</p></div>
<div class="m2"><p>ز ابر صبر بگلزار انتظار کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شعاع مهر محبت کمندها دارد</p></div>
<div class="m2"><p>نمی رود سوی خوبان باختیار کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه غافلی که ترحم نمی کنی یک بار</p></div>
<div class="m2"><p>اگر برای تو میرد هزار بار کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سنگها که زدی بر سرم دهد یادم</p></div>
<div class="m2"><p>مرا چو لوح نهد بر سر مزار کسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرود ذوق فضولی ز کس نمی شنوم</p></div>
<div class="m2"><p>مگر نماند ز رندان باده خوار کسی</p></div></div>