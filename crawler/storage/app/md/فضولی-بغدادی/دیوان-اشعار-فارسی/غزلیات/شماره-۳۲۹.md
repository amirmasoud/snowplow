---
title: >-
    شمارهٔ ۳۲۹
---
# شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>چرا نگاه بدور رخت بماه کنم</p></div>
<div class="m2"><p>که چون نگاه کنی بر زمین نگاه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جدا ز شمع جمال تو تا بکی شبها</p></div>
<div class="m2"><p>چراغ خلوت خود را ز برق آه کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ناز بر سر من پا نمی نهی تو اگر</p></div>
<div class="m2"><p>هزار بار سر خویش خاک راه کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باشک و آه کنم عشق را بخود ثابت</p></div>
<div class="m2"><p>چه لایق است که دعوای بی گواه کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش آن شبی که دهم ساز بزمگاه سرور</p></div>
<div class="m2"><p>خیال روی ترا شمع بزمگاه کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر گناه قصاصم اگر تو خواهی کرد</p></div>
<div class="m2"><p>نه عاقلم عملی کز بجز گناه کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی از خط و زلف بتان گرفت دلم</p></div>
<div class="m2"><p>بسهو نامه خود تا بکی سیاه کنم</p></div></div>