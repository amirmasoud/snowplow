---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>بخت بد بی‌اختیار از کوی یارم می‌برد</p></div>
<div class="m2"><p>بی‌قرارم می‌کند بی‌اختیارم می‌برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نگریم در طریق عشق ترک اعتبار</p></div>
<div class="m2"><p>در میان پاکبازان اعتبارم می‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>التفاتی نیست بر حالم ز اهل این دیار</p></div>
<div class="m2"><p>وه که این بی‌التفاتی زین دیارم می‌برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چه بد کردم درین کشور که بهر کام دل</p></div>
<div class="m2"><p>آنکه با صد عزتم آورد خوارم می‌برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وه چه حالست این که دور دون بدین محنت‌سرا</p></div>
<div class="m2"><p>شادمان می‌آورد هربار زارم می‌برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر آن کوی تا یابم به کام دل قرار</p></div>
<div class="m2"><p>چرخ خاکم کرده بود اکنون غبارم می‌برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من فضولی نیستم سرگشته عالم به خود</p></div>
<div class="m2"><p>اینچنین بی‌خود به هرسو روزگارم می‌برد</p></div></div>