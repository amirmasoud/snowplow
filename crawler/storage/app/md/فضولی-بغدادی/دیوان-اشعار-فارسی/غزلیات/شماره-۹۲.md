---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>در دل لاله غمت آتش سودا انداخت</p></div>
<div class="m2"><p>شمع را آتش سودای تو از پا انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یافت از نکهت زلف تو خبر آهوی چین</p></div>
<div class="m2"><p>نافه مشک خود از شرم بصحرا انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ز دیدار تو مانع نشود چشم پرآب</p></div>
<div class="m2"><p>خواب را در نظرم کشت بدریا انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشک رخسار تو زد بتکده ها را بر هم</p></div>
<div class="m2"><p>آه من غلغله در گوش مسیحا انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگشادی بسخن صد گرهم چون سبحه</p></div>
<div class="m2"><p>عقد دندان تو بر رشته تقوا انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواست آزار خود از ناوک آهم گردون</p></div>
<div class="m2"><p>که غم عشق توام بر دل شیدا انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرورا از نظر انداخت فضولی چون ما</p></div>
<div class="m2"><p>یک نظر هر که بر آن قامت رعنا انداخت</p></div></div>