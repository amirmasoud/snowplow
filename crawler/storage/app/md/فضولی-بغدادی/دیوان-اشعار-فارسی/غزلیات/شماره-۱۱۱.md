---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>ما را بلای عشق تو عمریست آشناست</p></div>
<div class="m2"><p>از آشنا جدا شدن آشنا بلاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخاست از قد تو بهر گوشه فتنه ای</p></div>
<div class="m2"><p>سروی ز باغ حسن بدین فتنه برنخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان و دلم بسوخت جدایی جدا جدا</p></div>
<div class="m2"><p>این است حال آن که ز جانان خود جداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان پیش تست ما ببلای تو زنده ایم</p></div>
<div class="m2"><p>ای عمر مدتیست بلای تو جان ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس نیست کز بلای بتانم دهد نجات</p></div>
<div class="m2"><p>بس مشکل است دفع بلایی که از خداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستان جام عشق فتادند بی خبر</p></div>
<div class="m2"><p>کس آگهی نیافت که این نشاه از کجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون گشت عادت تو فضولی جفا کشی</p></div>
<div class="m2"><p>تکلیف ترک کردن عادت ترا جفاست</p></div></div>