---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ای زلف تو سرمایه رسوایی ما</p></div>
<div class="m2"><p>عشق تو بهار گل شیدایی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخسار تو شمعیست که می افروزد</p></div>
<div class="m2"><p>از پرتو او چراغ بینائی ما</p></div></div>