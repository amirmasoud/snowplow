---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>ای کرده بصد خون جگر جمع متاع</p></div>
<div class="m2"><p>آیا چه شود حال تو هنگام وداع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خلق نزاع از پی دنیا کم کن</p></div>
<div class="m2"><p>دنیا نه متاعیست که ارزد بنزاع</p></div></div>