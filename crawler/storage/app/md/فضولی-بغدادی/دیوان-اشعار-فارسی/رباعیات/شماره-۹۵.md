---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>دارد دل زارم آرزوی رخ او</p></div>
<div class="m2"><p>تا من بوصالش برسم طالع او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطف سخن لعل لبش هست نکو</p></div>
<div class="m2"><p>عشقش یکسو جمیع هستی یکسو</p></div></div>