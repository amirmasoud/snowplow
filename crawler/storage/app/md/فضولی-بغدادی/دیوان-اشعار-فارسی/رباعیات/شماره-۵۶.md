---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>جانانه طلب می کنی از جان بگذر</p></div>
<div class="m2"><p>وز صحبت جان ز وصل جانان بگذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا لذت جمعیت خاطر یابی</p></div>
<div class="m2"><p>از قید تردد پریشان بگذر</p></div></div>