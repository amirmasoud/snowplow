---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>سروی که شدم ربوده رفتارش</p></div>
<div class="m2"><p>آشفته خط و طره طرارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخلیست که سنبل است و ریحان برگش</p></div>
<div class="m2"><p>لعلیست که گل و غنچه خندان بارش</p></div></div>