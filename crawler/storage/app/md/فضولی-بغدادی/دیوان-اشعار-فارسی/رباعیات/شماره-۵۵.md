---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>شمشاد که گشتست بقد تو اسیر</p></div>
<div class="m2"><p>می رفت پیت چو سایه ای ماه منیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک چمنش گر نشدی دامن گیر</p></div>
<div class="m2"><p>در آب بپایش ننهادی زنجیر</p></div></div>