---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>عمریست که باز عشق یارست مرا</p></div>
<div class="m2"><p>دل در غم عشق بی قرارست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشتست گره گشای کارم غم عشق</p></div>
<div class="m2"><p>با غیر غم عشق چه کارست مرا</p></div></div>