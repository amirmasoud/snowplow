---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>انجام وجود اهل عالم عدم است</p></div>
<div class="m2"><p>پایان سرور و راحت و ذوق غم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار مکش در طلب راحت رنج</p></div>
<div class="m2"><p>کین جنس بسی عزیز و بسیار کم است</p></div></div>