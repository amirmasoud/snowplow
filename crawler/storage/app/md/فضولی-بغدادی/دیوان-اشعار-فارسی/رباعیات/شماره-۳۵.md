---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>ای دل بگذر ز تنگنای این کاخ</p></div>
<div class="m2"><p>آهنگ فنا کن که فضاییست فراخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگذار که در حدیقه تنگ چنین</p></div>
<div class="m2"><p>نخل املت هر طرف اندازد شاخ</p></div></div>