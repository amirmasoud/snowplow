---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>کام دل زار ما روا کن یارب</p></div>
<div class="m2"><p>توفیق سخن نصیب ما کن یارب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را به ازین سخن سرا کن یارب</p></div>
<div class="m2"><p>گویا بثنای مصطفا کن یارب</p></div></div>