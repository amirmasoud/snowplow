---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>ای قصر وجودم باساس اخلاص</p></div>
<div class="m2"><p>سلطان محبت ترا خلوت خاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفی روشت راه ضلال است و ظلام</p></div>
<div class="m2"><p>تقلید رهت طریق خیرست و خلاص</p></div></div>