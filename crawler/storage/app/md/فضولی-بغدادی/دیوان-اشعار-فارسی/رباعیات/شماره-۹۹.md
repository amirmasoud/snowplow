---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>ابنای زمان که در جهانند همه</p></div>
<div class="m2"><p>از جور زمانه در فغانند همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر یک به هوس عاشق کاری شده است</p></div>
<div class="m2"><p>بی عشق نیند عاشقانند همه</p></div></div>