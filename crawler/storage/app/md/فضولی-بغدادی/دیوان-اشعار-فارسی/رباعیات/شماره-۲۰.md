---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>مشتاق وصال تو کسی نیست که نیست</p></div>
<div class="m2"><p>حیران جمال تو کسی نیست که نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد حال ز حال تو کسی نیست که نیست</p></div>
<div class="m2"><p>خالی ز خیال تو کسی نیست که نیست</p></div></div>