---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>هر دم بدلم فرخ بتی می آرد</p></div>
<div class="m2"><p>کارم ز بتان رواج و رونق دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز عاشقی بتان نخواهم ورزید</p></div>
<div class="m2"><p>فکرم اینست گر خدا بگذارد</p></div></div>