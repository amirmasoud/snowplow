---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>پیوسته فلک با قران اختر</p></div>
<div class="m2"><p>می سوزد داغ بر دل اهل نظر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شام و سحر را بمدار آوردست</p></div>
<div class="m2"><p>شامی بمراد کس نکردست سحر</p></div></div>