---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>کار دو جهان ز عشق دارد رونق</p></div>
<div class="m2"><p>در عشق گرفته است این نظم نسق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی عشق نمی توان بمقصود رسید</p></div>
<div class="m2"><p>عشق است طریق مستقیم ره حق</p></div></div>