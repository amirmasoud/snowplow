---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>هر دل که غم عشق نهان است درو</p></div>
<div class="m2"><p>لذتهای همه جهان است درو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جسمی که درو نیست دل غمزده</p></div>
<div class="m2"><p>قبریست که مرده نهان است درو</p></div></div>