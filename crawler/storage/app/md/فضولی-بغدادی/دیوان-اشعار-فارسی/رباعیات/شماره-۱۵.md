---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>نگشاد بپرسش من آن دلبر لب</p></div>
<div class="m2"><p>کام دل من نداد آن شکر لب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقصود نشد میسر از دولت وصل</p></div>
<div class="m2"><p>وز شوق رسید دل بجان جان بر لب</p></div></div>