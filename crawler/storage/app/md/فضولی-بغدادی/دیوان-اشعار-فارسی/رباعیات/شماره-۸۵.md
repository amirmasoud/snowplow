---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>چون دید مرا مایل زلف و خط و خال</p></div>
<div class="m2"><p>افکند نظر سوی من آن طرفه غزال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمعیت حال داشتم چشم رسید</p></div>
<div class="m2"><p>دیوانه شدم مرا پریشان شد حال</p></div></div>