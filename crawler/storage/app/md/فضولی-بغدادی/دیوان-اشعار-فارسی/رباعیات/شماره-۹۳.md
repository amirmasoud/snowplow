---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>خوش آن که دمی با تو کنم سیر چمن</p></div>
<div class="m2"><p>من پر کنم از اشک و تو از گل دامن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را نبود رقیب در پیرامن</p></div>
<div class="m2"><p>من باشم و تو باشی و تو باشی و من</p></div></div>