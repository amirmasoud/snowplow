---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>فریاد ز دست فلک سفله نواز</p></div>
<div class="m2"><p>شه زاده بمنت و گدا زاده بناز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرگس ز برهنگی سرافکنده به پیش</p></div>
<div class="m2"><p>صد پیرهن حریر پوشیده پیاز</p></div></div>