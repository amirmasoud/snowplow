---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>در جان غم عشق تو نهانست مرا</p></div>
<div class="m2"><p>آرام دل و راحت جانست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جا کرده بسان خون درون رگ و پی</p></div>
<div class="m2"><p>این زندگی که هست از آنست مرا</p></div></div>