---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>تا سلسله عاشقی ما برپاست</p></div>
<div class="m2"><p>دام دل ما مقید بند بلاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکدم ز بلای عاشقی دور نه ایم</p></div>
<div class="m2"><p>گویا که بلای عاشقی عاشق ماست</p></div></div>