---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>آیین وفا ز ماه رویان مطلب</p></div>
<div class="m2"><p>آسودگی از عربده جویان مطلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسم بدی از بدان طمع دار ولی</p></div>
<div class="m2"><p>آثار نکویی ز نکویان مطلب</p></div></div>