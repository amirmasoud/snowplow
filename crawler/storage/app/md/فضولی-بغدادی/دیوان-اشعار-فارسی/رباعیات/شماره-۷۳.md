---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>داری همه شب دیده بیدار ای شمع</p></div>
<div class="m2"><p>وز سوز جگر چشم گهربار ای شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می سوزی و می گذاری و می گریی</p></div>
<div class="m2"><p>گویا که چو من جدایی از یار ای شمع</p></div></div>