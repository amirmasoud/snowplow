---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>ماییم که نیست هیچ کس همدم ما</p></div>
<div class="m2"><p>ما در غم کس نه ایم و کس در غم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی ما خبر از مردم عالم داریم</p></div>
<div class="m2"><p>نی مردم عالم خبر از عالم ما</p></div></div>