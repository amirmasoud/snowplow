---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>سادات که نور دیده و تاج سرند</p></div>
<div class="m2"><p>با فضل و نسب زبده نوع بشرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باید که ز راه راست بیرون نروند</p></div>
<div class="m2"><p>چون امت جد خویش را راهبرند</p></div></div>