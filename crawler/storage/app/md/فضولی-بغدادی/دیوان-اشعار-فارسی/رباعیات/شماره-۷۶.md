---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>دور از رخ او نمی کنم رغبت باغ</p></div>
<div class="m2"><p>دارم ز تماشای گل و لاله فراغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که خلد بسینه ام از گل خار</p></div>
<div class="m2"><p>آتش فکند بر جگرم لاله ز داغ</p></div></div>