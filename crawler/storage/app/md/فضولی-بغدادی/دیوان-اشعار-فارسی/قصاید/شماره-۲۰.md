---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>فلک ز دور مخالف مگر پشیمان شد</p></div>
<div class="m2"><p>که هرچه در دل ما بود عاقبت آن شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقیم زاویه محنت و بلا نشدیم</p></div>
<div class="m2"><p>مقام راحت ما خاک درگه خان شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای آفتاب فلک سایه خان فرخ رخ</p></div>
<div class="m2"><p>که از قضا همه دشوار بر تو آسان شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمی که حیمه برون زد سپاهت از بغداد</p></div>
<div class="m2"><p>دل تمامی اهل عراق لرزان شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آفتاب برون آمدی فکندی نور</p></div>
<div class="m2"><p>عدو ستاره صفت هر چه بود پنهان شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدم بحله نهادی چراغ دولت تو</p></div>
<div class="m2"><p>ز نور قبه صاحب زمان فروزان شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای عرض کشیدی در آن فضای شریف</p></div>
<div class="m2"><p>چنان سپاه که هر کس که دید حیران شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بید راز بدی کرد و هم تیغ تو دور</p></div>
<div class="m2"><p>توجه تو بلایی باهل عصیان شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علم زدی بر ماهیه و ز مقدم تو</p></div>
<div class="m2"><p>فضای رابعه رشک ریاض رضوان شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی که بود گریزان ز تو بهمت تو</p></div>
<div class="m2"><p>نه در حصار گرفتار بند و زندان شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر چه خواست پریشان دل ترا جمعی</p></div>
<div class="m2"><p>هزار شکر که آن جمع خود پریشان شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بباغ ملک عرب از بهار مقدم تو</p></div>
<div class="m2"><p>گلی شکفت دگر عالمی گلستان شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مخالفان تو تکیه بر آب می کردند</p></div>
<div class="m2"><p>که صد راه شود سد راه ایشان شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز صدق پاک تو آبی که بود اصل حیات</p></div>
<div class="m2"><p>بدفع دشمن جاه تو تیغ بران شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هوای سرکشی و کبر هر که در سر داشت</p></div>
<div class="m2"><p>گذشت از سر تقلید و بنده فرمان شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون مطیع تو آن اهل کفر را ماند</p></div>
<div class="m2"><p>که از صلابت اهل غزا مسلمان شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود تمرد بدخواه جاهت آن کفری</p></div>
<div class="m2"><p>که از ظهور محمد بدل به ایمان شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیک سفر که درو حرم دو قلعه گرفت</p></div>
<div class="m2"><p>شکوه و دولت و اقبال تو دو چندان شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مقررست که این فتحهای بی رحمت</p></div>
<div class="m2"><p>ز یمن صدق درستی عهد پیمان شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>معین است که دارد همیشه دست بفتح</p></div>
<div class="m2"><p>کسی که بنده درگاه شاه مردان شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>امیر تخت نجف پادشاه انس و ملک</p></div>
<div class="m2"><p>که چاکر در او هم ملک هم انسان شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>امام مفترض الطاعتی که طاعت اوست</p></div>
<div class="m2"><p>وسیله که ز حق مستحق غفران شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بشکر کوش فضولی که حب شاه نجف</p></div>
<div class="m2"><p>ترا حقیقت اسلام و اصل ایمان شد</p></div></div>