---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>مدار هفته دوران که نفع او ضررست</p></div>
<div class="m2"><p>نه گنج هفت درست اژدهای هفت سرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منه ز طول امل دل بسرعت شب و روز</p></div>
<div class="m2"><p>که مرغ عمر چنین تیز پر بدین دو پرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرت درو گهر آید بکف مکن طغیان</p></div>
<div class="m2"><p>که بحر را صد ازین قطره در کنار و برست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگو که جمع گهر تلخ کامیم ببرد</p></div>
<div class="m2"><p>که تلخ کامیم از گرد گردن گهرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکش ز قید موالید سر چو می دانی</p></div>
<div class="m2"><p>که سین سر چو شود نقطه دار شین شرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رقم مکن طمع سیم و زر بلوح ضمیر</p></div>
<div class="m2"><p>که سکه زر و سیم طمع خط خطرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو سیم جمع کنی فیض آن بخلق رسان</p></div>
<div class="m2"><p>چو سود زانکه شجر را شکوفه بی ثمرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کرم بگشا تا شوی بلند مقام</p></div>
<div class="m2"><p>که جای فتحه همیشه ز فتح بر زبرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز کس مکن طمع نفع تا نگردی پست</p></div>
<div class="m2"><p>که کسره زیر نشین صفوف خط خطرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غرض ز جمع زر و سیم چیست ممسکرا</p></div>
<div class="m2"><p>چو در حصول غرض شرط ترک سیم و زرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین نظرگه پر فیض کز طریق نظر</p></div>
<div class="m2"><p>بقدر بینش خود هر که هست بهره برست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نکو اگر نگری هیچ خلقتی بد نیست</p></div>
<div class="m2"><p>تفاوتی بدو نیکی که هست در نظرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنسخه هنر هیچکس مکش خط عیب</p></div>
<div class="m2"><p>بعیب کش خط اگر مدعای تو هنرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بکار کوش که در کارخانه عالم</p></div>
<div class="m2"><p>بقدر حاصل هر کار مزد کارگرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز هرزه کاری تو چرخ مهربان تو نیست</p></div>
<div class="m2"><p>و گر نه مهر پسر رسم فطری پدرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کسی که لاف هنر زد هنر نخواهد داشت</p></div>
<div class="m2"><p>که از صداست تهی هر نی که پر شکرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر اهل درکی ازان علم و عقل لاف مزن</p></div>
<div class="m2"><p>که زرق را سببست و معاشر قمرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ورای عالمی و عاقلیست دانش حق</p></div>
<div class="m2"><p>ز قول و فعل خدا علم و عقل کور و کرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کدام عقل کماهی به گنه کار رسید</p></div>
<div class="m2"><p>کدام عالم از انجام حال با خبرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مکش که فایده قید عقل درد دلست</p></div>
<div class="m2"><p>مکن که ما حصل بحث علم درد سرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کمال گر طلبی در مقام عشق طلب</p></div>
<div class="m2"><p>که فیض عشق ز علم و ز عشق بیشترست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مذاق عیش مجو مطلق از مقید عقل</p></div>
<div class="m2"><p>که مست خواب نه آگه ز نشئه سحرست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فریب عقل مخور آن مبین که در ره قید</p></div>
<div class="m2"><p>بچشم عقل اقالیم سبعه گنج زرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز نخل عشق طلب بر که مجملا بر او</p></div>
<div class="m2"><p>محقرست بر عقل هر چه معتبرست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز عشق مگذر اگر بر مجاز هم باشد</p></div>
<div class="m2"><p>که مرد را بحقیقت مجاز راهبرست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کتابه که بود محض فیض مضمونش</p></div>
<div class="m2"><p>خط عذار صنوبر قدان سیمبرست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نظر کسی که ندارد بر آفتاب وشی</p></div>
<div class="m2"><p>چو شب هزارش اگر دیده هست بی بصرست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گمان مبر که در آب و گلست نشانه حسن</p></div>
<div class="m2"><p>حقیقتی است که در روی خوب جلوه گرست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مشعبدیست درین پرده ور نه کس بخودی</p></div>
<div class="m2"><p>نه پرده دار نه پرده نشین نه پرده درست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز صورتست رهی گر توان بمعنی برد</p></div>
<div class="m2"><p>مظاهر گل معنی حدایق صورست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بود صفای فضای فرح فزای نجف</p></div>
<div class="m2"><p>دلیل آنکه درو پادشاه بحر و برست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>امام مفترض الطاعه حیدر صفدر</p></div>
<div class="m2"><p>که درک او ز حد دانش بشر بدرست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ملک بمحکمه او رجوع کرده قضا</p></div>
<div class="m2"><p>احاطه بشر او کجا حد بشرست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بجوهرش حجرالاسود آمده مظهر</p></div>
<div class="m2"><p>چه آب زنده گیست آن که ظلمتش حجرست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بران شجر که دهد جویبار قدرش آب</p></div>
<div class="m2"><p>هر آسمان یکی از برگ سبز آن شجرست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز شعله که زند سر ز آتش مهرش</p></div>
<div class="m2"><p>بر اوج عز و شرف هر ستاره یک شررست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گدای درگه اقبال او همایون فال</p></div>
<div class="m2"><p>همای اوج تمنای او فرشته فرست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ستاره شرف مهر راحت افزایش</p></div>
<div class="m2"><p>شب دراز امل را نشانه سحرست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>طلیعه علم فیض عالم آرایش</p></div>
<div class="m2"><p>سپاه علم و عمل را علامه ظفرست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همین ز جمجمه اقبال کرده حکمش را</p></div>
<div class="m2"><p>کشیده از خط فرمان او کدام سرست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رموز دانه خرما ازو مدار عجب</p></div>
<div class="m2"><p>که نخل معجز او را چنین هزار برست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ببوی آن گل تازه که داد سلمان را</p></div>
<div class="m2"><p>دماغ اهل یقین تا بهار حشر ترست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بیک نهیب دو شیر تر شد سنگ</p></div>
<div class="m2"><p>باوج پنجه زند خصم اگر چه شیر نرست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شکستن مه و برگشتن خور از مغرب</p></div>
<div class="m2"><p>میان خلق چو روشن بسان ماه خورست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نه در خورست که گویم بر آسمان وجود</p></div>
<div class="m2"><p>نبی ز کوکبه شمس آمد و ولی قمرست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>علی کسیست که در عزم قرب حق جبریل</p></div>
<div class="m2"><p>براه مانده ازو همچو خاک ره گذرست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چه سان برابر جبریل دارم و گویم</p></div>
<div class="m2"><p>علی میان خدا و نبی پیام برست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بشهر علم نبی چون علیست در چه عجب</p></div>
<div class="m2"><p>ز جبرئل گر او را ز حاجبان درست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زهی سپهر ولایت که در ولایتها</p></div>
<div class="m2"><p>ولایت تو چو خورشید و ماه مشتهرست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بشهر علم نبی از برای معموری</p></div>
<div class="m2"><p>ز معجزات تو هر یک ولایت دگرست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نشان داغ وفایت بسینه احباب</p></div>
<div class="m2"><p>بدفع تیر حوادث نمونه سپرست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سرسر افکن تیغ تو در تن بدرک</p></div>
<div class="m2"><p>بخون فاسد طغیان کفر نیشترست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز ذوالفقار تو و دلدل تو دشمن و دوست</p></div>
<div class="m2"><p>بر اسم پی روی و سرکشی بنفع ضرست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بدلدلت بود آن دال دال سوی نجات</p></div>
<div class="m2"><p>بذوالفقار تو آن ذال ذال الحذرست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز تیغ تو جگر دشمنان همه شده خون</p></div>
<div class="m2"><p>بعزم دشمنی تیغ تو کرا جگرست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اگر بملک جهان دل نداده چه عجب</p></div>
<div class="m2"><p>تو شاه بازی و این صید صید مختصرست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>فضای ملک عبودیت تو صحرا نیست</p></div>
<div class="m2"><p>که همچو طایر قدسش هزار جانورست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هوای بندگی درگه تو فایده ایست</p></div>
<div class="m2"><p>که صد خلیل بادرار ازو وظیفه خورست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کسی نیافت بقدر تو در صف عزت</p></div>
<div class="m2"><p>قضا که انجمن ارای محفل قدرست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گلی ندیده برنگ تو در بهار وجود</p></div>
<div class="m2"><p>صبا که چهره گشای عرایس زهرست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هزار شکر که در سلک خادمان توام</p></div>
<div class="m2"><p>همین سعادت من بس که قدرم این قدرست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شها بلطف نظر کن همین که با چه کسان</p></div>
<div class="m2"><p>مرا بقوت مدح تو دست در کمرست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گرفت کاتبی این راه حیرتی از پی</p></div>
<div class="m2"><p>قدم برسم تتبع نهاده بی سپرست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>میان این دو سخن هست گفته من حشو</p></div>
<div class="m2"><p>چرا کزان دو یکی رو یکی چو آسترست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>درین لباس حد نظم من معین شد</p></div>
<div class="m2"><p>که گر بود ز یکی زیر از یکی ز برست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>فضولیم من و کارم گنه اگر زین کار</p></div>
<div class="m2"><p>کشم بعذر زبان عذرم از گنه بترست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>امید هست که بدگو مرا معاف کند</p></div>
<div class="m2"><p>چو هرز مدح تو از هر ملامتم مقرست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همیشه تا بمکافات خیر و شهر بجهان</p></div>
<div class="m2"><p>امید و بیم بدو نیک جنت و سقرست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خوشم بدین که همیشه ز قرب تو حاصل</p></div>
<div class="m2"><p>طواف کعبه مرا بی مشقت سفرست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مرا جدا نکند حق ز جنت در تو</p></div>
<div class="m2"><p>که هر که هست ز جنت برون سقر مقرست</p></div></div>