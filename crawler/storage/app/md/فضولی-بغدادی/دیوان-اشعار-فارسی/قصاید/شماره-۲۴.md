---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>سحر که عامل دین را فزود رونق کار</p></div>
<div class="m2"><p>فکند بیم هوا لرزه در تن اشجار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگو جمیله مهرست در کنار زمین</p></div>
<div class="m2"><p>مگو سفیدی برفست بر سر کهسار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی کشیده همه شب مشقت سرما</p></div>
<div class="m2"><p>کنار منقل آتش گرفته روز قرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی نشسته همه شب میانه باران</p></div>
<div class="m2"><p>بر آفتاب فکندست صبحدم دستار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز فیض باد سحر در گذر بموسم دی</p></div>
<div class="m2"><p>که همچو نیت ظلم است در دل اسرار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باب جوهرسان دست در مه بهمن</p></div>
<div class="m2"><p>که بر مثابه زهرست در طبیعت مار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان ز تندی دی بست در هوا باران</p></div>
<div class="m2"><p>که قطره بی صدفی گشت لؤلؤ شهوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمانه داد رضا بارها که پنبه ابر</p></div>
<div class="m2"><p>بگیرد آتش و از برق گردد آتش بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکرد فایده سنجابی سحاب برعد</p></div>
<div class="m2"><p>هزار بار ز سرما کشیده ناله زار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان مالک و رضوان ز بهر لطف مقام</p></div>
<div class="m2"><p>شبی فتاد درین فصل دعوی بسیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بهر آنکه کند مدعای خود ثابت</p></div>
<div class="m2"><p>دلیل صدق سخن کرد هر یکی اظهار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی ز روضه گلی چند در میان آورد</p></div>
<div class="m2"><p>یکی ز آتش دوزخ نمود چند شرار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان گرفت درین بخت جانب مالک</p></div>
<div class="m2"><p>که آتش از گل و دوزخ ز روضه به صد بار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز حال مردم صحرانشین درین موسم</p></div>
<div class="m2"><p>به است حال مقیمان حجرهای مزار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنون درآی در آتش بسان ابراهیم</p></div>
<div class="m2"><p>گرت هواست که آتش ترا شود گلزار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درین هوا نظری سوی نار کن چو کلیم</p></div>
<div class="m2"><p>که شخص نور ترا در نظر نماید نار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ره مطالعه آفتاب بر ماهی</p></div>
<div class="m2"><p>ز بس که آب ز یخ بست بر یمین و یسار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز داغ آرزوی آفتاب و غصه غراب؟</p></div>
<div class="m2"><p>بر آتش است دل ماهیان قعر بحار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نکرده فرق نامیه درین موسم</p></div>
<div class="m2"><p>هوای بادیه را آب تیشه بخار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز بس که آب ز پای او فتاد و آتش سوخت</p></div>
<div class="m2"><p>ز بس که آب فسرد و هوا گرفت غبار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لباس لطف بمقراض اختلاف هوا</p></div>
<div class="m2"><p>بباد موسم گل دیده عناصر چار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگشت باغ ز سرما نمی توان رفتن</p></div>
<div class="m2"><p>مگر دمی که ز گل آتشی فتد در خار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خوشا کسی که درین فصل گوشه ای گیرد</p></div>
<div class="m2"><p>دهد بکنج قناعت فرار را بقرار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درون خانه در آید در ابتدای خزان</p></div>
<div class="m2"><p>رهی برون نبرد تا بابتدای بهار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صراحی و کتابی و سازی و صنمی</p></div>
<div class="m2"><p>جزین چهار که گفتم نباشد او را یار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گه از کتاب رساند بدیده نور سرور</p></div>
<div class="m2"><p>گه از مطالعه صفحه رخ دلدار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گهی دهد بدماغ از بخار باده بخور</p></div>
<div class="m2"><p>گهی کشد به مشام از بخور عود بخار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درو دریچه خلوت سرا فرو بندد</p></div>
<div class="m2"><p>بسان دیده احباب بر رخ اغیار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در انتظار شد از بهر اعتدال هوا</p></div>
<div class="m2"><p>بریده گشت و ز هم ریخت دو را پرکار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیابد از الم دهر آفتی ز انسان</p></div>
<div class="m2"><p>که ز اهل شرک محبان حیدر کرار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مه سپهر ولایت شه ولایت دین</p></div>
<div class="m2"><p>امام انس و ملک ملجاء صغار و کبار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مدام در همه افعال مدرک احوال</p></div>
<div class="m2"><p>همیشه در همه احوال واقف اسرار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کسی که واقف او از وجود فایده مند</p></div>
<div class="m2"><p>کسی که واقف او از حیات برخوردار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>میان بخدمت او مرگ بست بست کمر</p></div>
<div class="m2"><p>کمر که هست همینست ماعدا زنار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نبوده روز عزا از کمال فیض هنر</p></div>
<div class="m2"><p>جز او معین ز مهاجر معاون و انصار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز ذوالفقار چشانده بذوالخمار می</p></div>
<div class="m2"><p>که صبح روز جرایم همی شود هشیار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قطار ناقه و بار گهر اگر بخشد</p></div>
<div class="m2"><p>بسایلی که ز احسان ازو عجب مدار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که همتش دم تحریک می تواند داد</p></div>
<div class="m2"><p>بکمترین گدایان قطار همره بار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر بود بمثل آن قطار هفت اختر</p></div>
<div class="m2"><p>و گر بود ز صنادق چرخ بار قطار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>قطار هفته ایام را همیشه قضا</p></div>
<div class="m2"><p>بدست سلسله آل او سپرده مهار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مدار عالم کون فساد بی بنیاد</p></div>
<div class="m2"><p>نقیض روز قیامت شبست کوته بار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>درین شبند همه خلق مست خواب غرور</p></div>
<div class="m2"><p>همین علیست پی طاعت خدا بیدار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بهم نیامدن چشم او بخواست چنین</p></div>
<div class="m2"><p>چو عین اسم عیانست بر اولوالابصار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز بعض اهل زمانه چه باک ذاتش را</p></div>
<div class="m2"><p>ز خواب کرده به بیدار کی رسد آزار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>امین گنج وفا مقتدای راه نجات</p></div>
<div class="m2"><p>ملاذ و مرجع و امیدگاه استظهار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز فیض مرحمتش زنده صد هزار مسیح</p></div>
<div class="m2"><p>ز درگه کرمش صد خلیل راتبه خوار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بخاک درگه او کرده عرض توبه عذر</p></div>
<div class="m2"><p>زمان زمان بتضرع زمانه غدار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>قلم کشیده زبان لیک نی در اوصافش</p></div>
<div class="m2"><p>ز بهر آنکه نماید بعجز خود اقرار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>قدم قدم بره سالکان طوف رهش</p></div>
<div class="m2"><p>طبق طبق در انجم نموده چرخ نثار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ایا خجسته خصالی که منت کرمت</p></div>
<div class="m2"><p>نهاد طوق غلامی بگردن احرار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کسی که حب ترا نعمتی نداند نیست</p></div>
<div class="m2"><p>سزای نعمت الطاف ایزد جبار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به بی کسان طریق وفا تویی ملجأ</p></div>
<div class="m2"><p>به ره روان ره التجا تویی غمخوار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بلند منزلتا آن منم که شام و سحر</p></div>
<div class="m2"><p>زبان کشیده ثنای تو می کنم تکرار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز بهر آنکه بمن فیض تو رسیده و بس</p></div>
<div class="m2"><p>ز بهر آنکه ترا هست لطف عام شعار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گرفته بهره ز خوان عنایت عامت</p></div>
<div class="m2"><p>هزار بی سر و پا کمترین فضولی زار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>امید هست که تا هست در زمین فلک</p></div>
<div class="m2"><p>همیشه بر نهج اعتبار یکیش مدار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بهار جاه محبت بود بری ز خزان</p></div>
<div class="m2"><p>خزان عشرت اعدای تو بری ز بهار</p></div></div>