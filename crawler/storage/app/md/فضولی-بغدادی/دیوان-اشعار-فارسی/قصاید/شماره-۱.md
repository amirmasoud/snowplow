---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ماییم درد پرور دنیای بی وفا</p></div>
<div class="m2"><p>با درد کرده خوشده مستغنی از دوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نکرده درد دل اظهار ما طلب</p></div>
<div class="m2"><p>هر جا که دیده خط زده بر نسخه شفا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطلق وفا ندیده ز ابنای روزگار</p></div>
<div class="m2"><p>بر خود در آرزوی وفا کرده صد جفا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وحشت گزیده از همه عالم چه جای غیر</p></div>
<div class="m2"><p>با خویش هم نگشته درین وحشت آشنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماییم فرقه که همیشه مدار چرخ</p></div>
<div class="m2"><p>انداخته است تفرقه در میان ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیل دمادم از مژه هر سو گشوده لیک</p></div>
<div class="m2"><p>بسته زبان چو شمع در افشای ماجرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماییم آن گروه پریشان که چون حباب</p></div>
<div class="m2"><p>صورت نبسته جمعیت ما بهیج جا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گردباد حادثه بر باد داده باز</p></div>
<div class="m2"><p>هر جا که کرده بهر امل خانه بنا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جسته طریق مهر ز دور فلک ولی</p></div>
<div class="m2"><p>زان آینه ندیده بجز عکس مدعا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرکار سان دویده درین دایره بسی</p></div>
<div class="m2"><p>بهر علو منزلت از سر نموده پا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیکن در انتهای تردد نیافته</p></div>
<div class="m2"><p>غیر از همان مقام که بوده در ابتدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ماییم همچو قطره باران ز جرم خاک</p></div>
<div class="m2"><p>عمری بریده میل شده پیرو هوا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اول بسیر عالم علوی نهاده روی</p></div>
<div class="m2"><p>آخر بطبع سفلی خود کرده اقتدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ماییم همچو عکس بر آیینه وجود</p></div>
<div class="m2"><p>غافل ز خود بصورت انسان غلط نما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه آگه از فساد نه واقف ز حال کون</p></div>
<div class="m2"><p>نه در غم فنا نه در اندیشه بقا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کیفیت بقاست ولیکن کمال ذات</p></div>
<div class="m2"><p>کی ذات ما کند بچنین رتبه اقتضا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در ممکنات شرط فنا جز وجود نیست</p></div>
<div class="m2"><p>ما را وجود کو که بود قابل فنا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من آن نیم که می رسد از من بگوش خلق</p></div>
<div class="m2"><p>در هر نفس هزار صدای فرح فزا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اما چنان نیم که شناسم مذاق درد</p></div>
<div class="m2"><p>باشد مرا هم آرزوی ذوق آن صدا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از روی کفر صورت بی معنیم شده است</p></div>
<div class="m2"><p>چون بت همیشه زیور بتخانه ریا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خلقی بطعنه من و من بی خبر ز حال</p></div>
<div class="m2"><p>حیرت گرفته راه دلم راه ره ادا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تدبیر طعن خلق سرانجام کار خود</p></div>
<div class="m2"><p>با آن گذاشته که چنین ساخته مرا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر رخت اعتبار خود آتش زدم هنوز</p></div>
<div class="m2"><p>از دست قید چرخ نشد دامنم رها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون رشته . . . فتادست صد گره</p></div>
<div class="m2"><p>بر کارم از سپهر و ندارم گره گشا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لیکن امید هست که همچون فروغ صبح</p></div>
<div class="m2"><p>بگشاید این گره اثر مهر مرتضا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاهی که تا ازو نزند دم نمی شود</p></div>
<div class="m2"><p>آسان گشودن گره غنچه بر صبا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شاهی که بی ارادت او مشکل او کشد</p></div>
<div class="m2"><p>از چهره صباح فلک پرده سنا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاهی که گلبن کرم او به اهل فقر</p></div>
<div class="m2"><p>در هر نفس رسانده ز هر برگ صد نوا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بخشیده سنگ را نظرش قیمت گهر</p></div>
<div class="m2"><p>پوشیده فقر را کرمش کسوت غنا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شاهنشه سریر ولایت ولی حق</p></div>
<div class="m2"><p>سلطان دین امام مبین شاه اولیا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اصل تمیز شرع نبی از طریق کفر</p></div>
<div class="m2"><p>وجه تفوق نبی ما بر انبیا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از ذات پاک او صدف کعبه پر گهر</p></div>
<div class="m2"><p>وز فیض خاک او شرف ارض بر سما</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از نسخه کرامت عامش سیاهه ایست</p></div>
<div class="m2"><p>شرح شب مبارک معراج مصطفا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وز لاله زار حرمت آتش حدیقه</p></div>
<div class="m2"><p>خاک بخون سرشته صحرای کربلا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ریک نجف ز پرتو میل مزار او</p></div>
<div class="m2"><p>در چشم مردمست مکرم چو توتیا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر اهل دولتی اگر ز آسمان فیض</p></div>
<div class="m2"><p>خورشید مهر او فکند ذره ضیا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حایل بران نشانه بی دولتی بود</p></div>
<div class="m2"><p>آن حایل ار بود بمثل سایه هما</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حاجت گهیست کعبه درگاه او که نیست</p></div>
<div class="m2"><p>آنجا برای حاجت او حاجت دعا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای درگه تو کعبه حاجت روای خلق</p></div>
<div class="m2"><p>وی گشته حاجت همه از درگهت روا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هر حکمتی که بوده نهان در حجاب غیب</p></div>
<div class="m2"><p>رایت کشیده از رخ آن پرده خفا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زایی اگر برای وقوع قضیه ای</p></div>
<div class="m2"><p>بسته هزار سال گره در دل قضا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ممکن نبود این که تواند وقوع یافت</p></div>
<div class="m2"><p>تا رای انور تو ندارد بدان رضا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آدم کز آفرینش او مدعا نبود</p></div>
<div class="m2"><p>جز اتباع امر حق و طاعت خدا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در ابتدا حال امامی چو تو نداشت</p></div>
<div class="m2"><p>خالی نبود طاعتش از شبهه خطا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>حالا باقتدای تو عمریست در نجف</p></div>
<div class="m2"><p>طاعات فوت کرده خود می کند قضا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تیغ تو صیقلیست که داده هر آینه</p></div>
<div class="m2"><p>از زنگ شرک آینه شرع را جلا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از دین عبارتیست بحکم تو اتباع</p></div>
<div class="m2"><p>وز کفر شبهه ایست ز فرمان تو ابا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هر کس که بر مطالب دینی و عقبیش</p></div>
<div class="m2"><p>باشد ارادتی بحقیقت بود گدا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از بی نیازی که ترا هست در دو کون</p></div>
<div class="m2"><p>تحقیق شد که نیست بغیر از تو پادشا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در لشکری که چون تو چراغیست پیش رو</p></div>
<div class="m2"><p>نصرت چو سایه می رسد البته از قفا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در زیر هر لوا که بود چون تو نور پاک</p></div>
<div class="m2"><p>بر مهر و ماه می فکند سایه لوا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خوف از چه دارد آنکه بدست دلش دهد</p></div>
<div class="m2"><p>حبل المتین مهر تو سر رشته ز جا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>یا مرتضا ورای تو ما را ملاذ نیست</p></div>
<div class="m2"><p>درهر کجا که هست تویی ملجا ورا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مطلق نمی کنیم بغیر تو اعتماد</p></div>
<div class="m2"><p>هرگز نمی بریم بغیر از تو التجا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ورزیده ام مهر تو . . . ماست</p></div>
<div class="m2"><p>روزی که حق بحسن عمل می دهد جزا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>داریم تکیه بر عمل خود بصد امید</p></div>
<div class="m2"><p>چون سنگ آستان تو ماراست متکا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>رخسار ما بسده زرین درگهت</p></div>
<div class="m2"><p>کاهیست متصل متعلق بکهربا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر خاک درگه تو نهادیم روی زرد</p></div>
<div class="m2"><p>آن خاک را ز قدر گرفتیم در طلا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کردیم گر چه صرف جوانی بخدمتت</p></div>
<div class="m2"><p>خوش نیست گر کنیم بدین خدمت اکتفا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>پیرانه سر بدرگهت آن به که افکنیم</p></div>
<div class="m2"><p>قد خم استخوان شکسته چو بوریا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>با نیت دوام اقامت بر آوریم</p></div>
<div class="m2"><p>طاق دگر بدرگهت از قامت دو تا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>یا مرتضا فضولی بیچاره بی کس است</p></div>
<div class="m2"><p>قطع نظر نموده ز اقران و اقربا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>آن راست رو میانه جمعیست مختلف</p></div>
<div class="m2"><p>مایل بهیج فرد نه چون خط استوا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>وقتست لطف شامل احوال او کنی</p></div>
<div class="m2"><p>وان دردمند را برهانی ازین بلا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>او طوطیست در صفت تو شکر شکن</p></div>
<div class="m2"><p>او از کجا و صحبت زاغ و زغن کجا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>او بلبلست از چمن قدس باغ انس</p></div>
<div class="m2"><p>او از کجا و قید چنین تیره ترک را</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>فرعون چند رشته مکر از کمال سحر</p></div>
<div class="m2"><p>تا کی بچشم او بنمایند اژدها</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>وقتست ز آستین ید بیضا برون کنی</p></div>
<div class="m2"><p>باز افکنی بمعرکه ساحران عصا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>میل نمی کنند باعجاز موسوی</p></div>
<div class="m2"><p>گوساله می پرستند این قوم بی حیا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>وقتست دل بتفرقه کافران نهی</p></div>
<div class="m2"><p>تیغ دو سر کشیده کنی نیت قضا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بر عادتی که هست ترا بر طریق دین</p></div>
<div class="m2"><p>حق را بحسن سعی ز باطل کنی جدا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تا در ریاض حسن فصاحت بکام دل</p></div>
<div class="m2"><p>باشد زبان طوطی طبعم سخن سرا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>روزی مباد این که برای توقعی</p></div>
<div class="m2"><p>از من بغیر آل علی سر زند ثنا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در عمر خویش غیر ثنای علی و آل</p></div>
<div class="m2"><p>از هر چه کرده ایم بیان توبه ربنا</p></div></div>