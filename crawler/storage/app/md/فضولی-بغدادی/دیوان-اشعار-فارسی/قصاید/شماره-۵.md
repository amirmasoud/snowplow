---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>کشید شاهد گل را صبا ز چهره نقاب</p></div>
<div class="m2"><p>نقاب روی زمین گشت سبزه سیراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محذرات سراپرده بطون عصون</p></div>
<div class="m2"><p>زدند آتش نهضت بپردهای حجاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خار گل نکشد منت از پی تمکین</p></div>
<div class="m2"><p>کنونکه یافت قبول نظاره احباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس است خیمه گل را همین که از هر سو</p></div>
<div class="m2"><p>کمند مد شعاع بصر شدست طناب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوال حال بنفشه ز باغبان کردم</p></div>
<div class="m2"><p>بقد خم شده پیر شکسته داد جواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که ساکنان درون سراچه دل خاک</p></div>
<div class="m2"><p>فکنده اند بحذب برومیان قلاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگو که بی جهت افکنده است هر جانب</p></div>
<div class="m2"><p>شکافها بدل دشت دشنه سیلاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صلای روی زمین می دهد به اهل زمان</p></div>
<div class="m2"><p>زمان زمان اثر افتتاح این ابواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درید بر بدن سبزه سیل جامه برف</p></div>
<div class="m2"><p>ربود صوت عنادل ز چشم نرگس خواب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پی تولد اظهار گونه گونه گرفت</p></div>
<div class="m2"><p>عروق ناهیه رنگ مجازی اصلاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پی تعلم اطفال قمری و بلبل</p></div>
<div class="m2"><p>گشود دور ز اوراق گل هزار کتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگو که هست ز لطف بهار و جنبش باد</p></div>
<div class="m2"><p>فتاده سبزه تر بزمین بر آب حباب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشان سیلی سیلیست بر زمین که شدست</p></div>
<div class="m2"><p>کبود روی زمین پر ز آبله کف پاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لطافتیست هوا را که هر کجا گذرد</p></div>
<div class="m2"><p>رطوبتش گل نیلوفر آورد ز سراب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نمود قطره شبنم ببرگ نرگس تر</p></div>
<div class="m2"><p>چنانکه بر لب بیمار رشحه جلاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طراوتیست زمین را گرو چه خیزد گرد</p></div>
<div class="m2"><p>بذرهاش توان گفت قطرهای گلاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رطوبتی ز هوا شب مگر گرفت که روز</p></div>
<div class="m2"><p>بر آفتاب فکندست رخت خویش سحاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بمفلسان چمن تا دهد برسم ذکات</p></div>
<div class="m2"><p>پرست دامن گل از قراحته زر ناب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زری ز خیره گلبن بشرط استنما</p></div>
<div class="m2"><p>مگر که یافته است از مدار خول نصاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرفت سخن چمن رونق از نسیم بهار</p></div>
<div class="m2"><p>چنانکه مملکت از سرور بلند جناب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زهی خجسته خیالی که بر تعین او</p></div>
<div class="m2"><p>نیافتست تسلط تصرف القاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بام عرش که باران فیض راست غدیر</p></div>
<div class="m2"><p>مسلطست بگلزار علم او میزاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز چتر علم که بر عالمست سایه فکن</p></div>
<div class="m2"><p>مشیدست باوتاد حلم او اطناب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مه سپهر فضیلت فضیل دریا دل</p></div>
<div class="m2"><p>که روشنست باو دیده اولوالالباب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مهیست دیده تمامی در ابتدای طلوع</p></div>
<div class="m2"><p>گل شگفته در آغاز نو بهار شباب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زهی وجود تو آینه دار فیض ازل</p></div>
<div class="m2"><p>خلاف رای تو مستوجب عذاب و عقاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شده عقاب ستم دیر دور را خفاش</p></div>
<div class="m2"><p>بدور عدل تو ای آفتاب عالمتاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پناه عدل تو شد حامی عراق عرب</p></div>
<div class="m2"><p>که از تطاول دست ستم نگشت خراب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تویی که رایض اقبال کرده در هر کار</p></div>
<div class="m2"><p>عنان عزم ترا منعطف بصوب ثواب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تویی که لطف ازل قامت قبول ترا</p></div>
<div class="m2"><p>نکرده است مزین مگر بثوب ثواب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تصرف تو در احکام کارخانه دهر</p></div>
<div class="m2"><p>فزون تر از اثر عالمست در اعراب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نشانه قبه عرشست هر کجا که شود</p></div>
<div class="m2"><p>قد تو تیر کمانخانه خم محراب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر شهد صفت در مذاق لطف تو هست</p></div>
<div class="m2"><p>منافع متضمن برون ز حصر و حساب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حرارت غضبت نیز نیست بی نفعی</p></div>
<div class="m2"><p>سرور سینه بی سوز دل دهد چو شراب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شها فضولی زارم که گردش گردون</p></div>
<div class="m2"><p>فکنده است بسر رشته امیدم تاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هوای وصل توام پیر کرد بس که ز بود</p></div>
<div class="m2"><p>سمند عمر ز دستم عنان برسم شتاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>امید هست که حالا درین نشیمن غم</p></div>
<div class="m2"><p>کند لقای تو سلب کدورتم ایجاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>امید هست که تا هست نوبهار خزان</p></div>
<div class="m2"><p>درین سراچه حیرت مهیمن وهاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عموم فیض رساند ترا و در هر دم</p></div>
<div class="m2"><p>دری بروی رضایت گشاید از هر باب</p></div></div>