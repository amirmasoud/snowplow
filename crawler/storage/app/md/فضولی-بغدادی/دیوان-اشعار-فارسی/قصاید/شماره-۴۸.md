---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>منم افتاده چو پرکار بسرگردانی</p></div>
<div class="m2"><p>متصل از حرکات فلک چوگانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه در وادی ادبار ز بی اقبالی</p></div>
<div class="m2"><p>گاه در بادیه فقر ز بی سامانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه در کوی بلا با علم رسوایی</p></div>
<div class="m2"><p>گاه در گوشه محنت بغم تنهایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخت را با الم سابقه بد عهدیست</p></div>
<div class="m2"><p>چرخ را با دلم اندیشه نافرمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده در گریه چو ابرست مرا در غلطم</p></div>
<div class="m2"><p>ابر را نیست چو چشم تر من گریانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردم دیده من خون جگر خورده بسی</p></div>
<div class="m2"><p>که سرامد شده در عالم اشک افشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدتی بهر یقین در پی کسب عرفان</p></div>
<div class="m2"><p>عمر کردم تلف از غایت بی عرفانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون گشودم به یقین دیده عرفان دیدم</p></div>
<div class="m2"><p>کاین متاعیست که دارد همه جا ارزانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باقی عمر برینم که کنم بهر معاش</p></div>
<div class="m2"><p>. . . عمر تلف معرفت نادانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ذاتی قدس مرا مرتبه و منزل بود</p></div>
<div class="m2"><p>بیشتر از ملکی پیشتر از انسانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حالیا از اثر تیره دلان رتبه من</p></div>
<div class="m2"><p>بست بستر ز جمادی شده حیوانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طوطی طبع مرا گر چه بهنگام سخن</p></div>
<div class="m2"><p>بود در طرز ادا کیفیت روحانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دور از فهم خسیسان شده نزدیک دران</p></div>
<div class="m2"><p>که شود مضحکه چون لهجه هندوستانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آه اگر باشدم از مبدا تقدیر رقم</p></div>
<div class="m2"><p>خسرالدنیا والآخره در پیشانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترک دینی جهت راحت عقبی کردم</p></div>
<div class="m2"><p>نیست پیدا که میسر شود آن هم یانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مدد از علم و عمل می طلبیدم عمری</p></div>
<div class="m2"><p>گر شوم مستحق مرحمت ربانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>علمم افسوس که جز شیوه تذویر نشد</p></div>
<div class="m2"><p>عملم نیز سوا وسوسه شیطانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بامیدی عمل و علم نماند آن املم</p></div>
<div class="m2"><p>که کشم رخت بسر منزل بی عصیانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دارم امید که بی علم و عمل حب علی</p></div>
<div class="m2"><p>گیردم دست درین وادی سرگردانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن امام همه کز روی رضا طاعت او</p></div>
<div class="m2"><p>هم بر انسی متنحم شده هم برجانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رای او رافع رایات جهان آرایست</p></div>
<div class="m2"><p>شان او منزل آیات عظیم الشانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نفرت از طاعتش انکار بفرمان خداست</p></div>
<div class="m2"><p>انحراف از رهش اقرار به بی ایمانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حکمت از دوستیش کرده اساس خلقت</p></div>
<div class="m2"><p>که بدان بنیه دگر رو ننهد ویرانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اهل حکمت بهمین واسطه دعوی دارند</p></div>
<div class="m2"><p>که جهان قابل آن نیست که گردد فانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر نباشد جهت قوت ارکان وجود</p></div>
<div class="m2"><p>در کف رغبت او رشته عالم بانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هست ممکن که بیک حادثه از هم ایزد</p></div>
<div class="m2"><p>خلق را خوانده عموما ز پی مهمانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>میزان کرم او بسر خوان بهشت</p></div>
<div class="m2"><p>بنیه شمس جهتی طرح چهار ارکانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عزم این عالمیان را سوی آن عالم نیست</p></div>
<div class="m2"><p>جز صلای سر خوان کرم او بانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یافت از پرتو صیت صفتش در بغداد</p></div>
<div class="m2"><p>انطفا نایره شهره نوشروانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آری آنجا که برآید سخن از شیر خدا</p></div>
<div class="m2"><p>چه سگ است آنکه زند دم ز سگ سلمانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای شهنشاه قضا رای قدر قدر که هست</p></div>
<div class="m2"><p>درک را دانش تو دایره حیرانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در مجالی که کشد موکب اوصاف تو صف</p></div>
<div class="m2"><p>وهم را وسعت آن کو که کند میدانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برتر از بندگیت مرتبه ممکن نیست</p></div>
<div class="m2"><p>بخدا بندگیت هست به از سلطانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر بکیوان رسد از دور به تدریج خلل</p></div>
<div class="m2"><p>می رسد هندوی هندوی ترا کیوانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ور برد حکم قضا شیر فلک را از جا</p></div>
<div class="m2"><p>می توانی که بجایش سگ خود بنشانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کان کفا بحر دلا هست ز یمن مدحت</p></div>
<div class="m2"><p>سخنم به ز در بحری و لعل کانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خواهم از بخت که هم صرف نثار تو شود</p></div>
<div class="m2"><p>در و لعلی که بمن داشته ای ارزانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در عراق عرب امروز منم سلمان را</p></div>
<div class="m2"><p>به صفای سخن و حسن فصاحت ثانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر چه در لطف ادا رتبه سلمانم نیست</p></div>
<div class="m2"><p>قطره ای را نبود حوصله عمانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>لیک سلمان همه عمر تلف کرد حیات</p></div>
<div class="m2"><p>در ثنای نسب فرقه چنگیز خانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>من کمین مادح و منسوب به اهل البیتم</p></div>
<div class="m2"><p>کار من نیست بجز مدح و مناقب خوانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بهمین محرمی ارباب فراست دانند</p></div>
<div class="m2"><p>که کرا می رسد ار دم زند از سلمانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دارم امید که تا هست بگلزار سخن</p></div>
<div class="m2"><p>بلبل ناطقه را فرصت خوش الحانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فضل مداحی اولاد نبی را دایم</p></div>
<div class="m2"><p>دارد ایزد به فضولی حزین ارزانی</p></div></div>