---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>منم ببادیه نیستی نهاده قدم</p></div>
<div class="m2"><p>بحرف قید ز کلک فنا کشیده رقم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حکیم عقل ز درک تشخصم عاجز</p></div>
<div class="m2"><p>دبیر درک در اثبات هستیم ملزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهات ست ندارد حد احاطه من</p></div>
<div class="m2"><p>بجزؤ لا یتجز است جوهرم توأم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آرزوی سر زلف مهوشان عمریست</p></div>
<div class="m2"><p>من شکسته بجسم ضعیف و قامت خم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان شدت ایام گشته ام ناچیز</p></div>
<div class="m2"><p>بسان دال که در او شده است آن مدغم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیم مقید عالم حکیم بهر خدا</p></div>
<div class="m2"><p>بمن مگوی حدیث حدوث حرف قدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو حال عالم کون و فساد میپرسی</p></div>
<div class="m2"><p>ز من مپرس که من نیستم از آن عالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا ز نشئه عشقست عالمی که درو</p></div>
<div class="m2"><p>نه راحتیست ز لذت نه محنتی ز الم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه عشق عشق حقیقی که بر صحیفه کون</p></div>
<div class="m2"><p>طفیل او شده نقش مکونات رقم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نبی امی مکی محمد قرشی</p></div>
<div class="m2"><p>صلاح ملک عرب فتنه ملوک عجم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مه سپهر وفا آفتاب اوج سخا</p></div>
<div class="m2"><p>شه خجسته سر سرور حمیده شیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپاه دولت و دین را سوار خصم افکن</p></div>
<div class="m2"><p>سریر شرع مبین را شهنشه اعظم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تمیز داده حرام و حلال را بسلوک</p></div>
<div class="m2"><p>نموده راه نعیم و سقر بلا و نعم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سمنبری که چو بشکفته از ریاض حجاز</p></div>
<div class="m2"><p>سهی قدی که چو بر خاسته ز خاک حرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بعزم دفع معارض برون زده خیمه</p></div>
<div class="m2"><p>پی شکستن اصنام بر کشیده علم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هزار کافر را از صنم بر آورده</p></div>
<div class="m2"><p>بزیب حسن شکسته صف هزار صنم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو او بکعبه درون آمده برون شده بت</p></div>
<div class="m2"><p>بسعی او شده خالی حرم ز نا محرم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شنیده ام بره زهر کرده کرده سخن</p></div>
<div class="m2"><p>بمعجزش پی اظهار مکر اهل ستم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کمال فیض نگه کن که در تن مرده</p></div>
<div class="m2"><p>از و طبیعت آب حیات یافته سم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو کوس عدل زده در حجاز و در بغداد</p></div>
<div class="m2"><p>نموده طایر دولت ز طاق کسری سم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو در یمن زده سر چشمه از انگشتش</p></div>
<div class="m2"><p>بفارس یافته آتش که معارض نم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>میان موسی و او فرق ماه تا ماهیست</p></div>
<div class="m2"><p>کجا شکستن ماه و کجا بریدن یم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دمی که نشئه روزی گرفت هر قومی</p></div>
<div class="m2"><p>بقدر حوصله در بزم منشا و مقسم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نشاط دولت اسلام یافت امت را</p></div>
<div class="m2"><p>مذاق مستی می قوم عیسی مریم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همین بس است بتعظیم امت او مدح</p></div>
<div class="m2"><p>همین بس است به الزام قوم ذمی ذم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جز او نیافته ز ابنای روزگار کسی</p></div>
<div class="m2"><p>چو جبرئیل برادر چو مرتضی بن عم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر بلوح و قلم دست بهر خط ننهد</p></div>
<div class="m2"><p>ز بحر فضل چنان کاملی نگردد کم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه حاجت است آرزوی صنعت خط</p></div>
<div class="m2"><p>کسی که پای تواند نهد بلوح و قلم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زهی بحکم روان راح روح پرور را</p></div>
<div class="m2"><p>حرام کرده بجمشید و تلخ کرده بجم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قبول شرع تو و رد مذهب حکما</p></div>
<div class="m2"><p>عیان شده بهمه گشته چون زمانه حکم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بنای دعوی باطل نهاده رو بزوال</p></div>
<div class="m2"><p>اساس بنیه حق مانده آنچنین محکم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حدیقه ای ز ریاض رضای تست بهشت</p></div>
<div class="m2"><p>کنایه ز گلستان کوی تست ارم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دمی که کرده ای از لعل گوهرافشانی</p></div>
<div class="m2"><p>گشوده ای در گنج معانی مبهم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هزار قافله مرحمت به شهر وجود</p></div>
<div class="m2"><p>نهاده روی به حکم خدا ز ملک عدم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گهی که لب به تبسم گشوده ای و به خلق</p></div>
<div class="m2"><p>میان برگ گل تر نموده ای شبنم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هزار روضه روح و ریاض دل شده است</p></div>
<div class="m2"><p>ز فیض شبنم و گلبرگ نازکت خرم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو تاج اهل دلی ترک کرده دنیا</p></div>
<div class="m2"><p>تو خاتم رسلی سنگ بسته بشکم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که برگ ترک بر آرنده است در گل تاج</p></div>
<div class="m2"><p>نگین سنگ پسندیده است در خاتم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ملک بسجده آدم چه گونه سر ننهد</p></div>
<div class="m2"><p>ز خاک پای تو بود است طینت آدم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حیات چون ندهت مرده را دم عیسی</p></div>
<div class="m2"><p>ز فیض لعل لبت میزده است عیسی دم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سر از متابعت خضر چون کشد موسی</p></div>
<div class="m2"><p>براه پیرویت می نهاده خضر قدم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پی عروج تو بسته ببام عرش قضا</p></div>
<div class="m2"><p>ز چار عنصر و نه پایه فلک سلم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ستاره نیست که وقت عزیمت معراج</p></div>
<div class="m2"><p>سپاه جاه تو کرده سپهر را مخیم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فلک نداشت ستاره زمین گل و سبزه</p></div>
<div class="m2"><p>و لیک در شب معراج از نثار قدم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همین طبق طبق انداخت لعل و فیروزه</p></div>
<div class="m2"><p>همان خزانه خزانه گهر رساند بهم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حکایت کرم حاتم است غایت کفر</p></div>
<div class="m2"><p>بدور چون تو کریمی و در مجال کرم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز نیم شمه لطف تو میتواند بود</p></div>
<div class="m2"><p>هزار چون کرم خود هزار چون حاتم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شها فضولی ما گر چه هست محض خطا</p></div>
<div class="m2"><p>خطاست گر بدل آریم با وجود تو غم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>امید هست که از لطف تو پذیرد عفو</p></div>
<div class="m2"><p>معاصی همه خلق و فضولی ما هم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تویی که روز جزا چون شفیع خلق شوی</p></div>
<div class="m2"><p>جراحت همه را از تو میرسد مرهم</p></div></div>