---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>زبان خوشست که توحید حق کند به بیان</p></div>
<div class="m2"><p>اگر چنان نبود در دهان مباد زبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهی مکون کامل که هست در کونین</p></div>
<div class="m2"><p>رقم کشیده او نقش کاینا ما کان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمال صنع قدیمش خجسته دهقانی‌ست</p></div>
<div class="m2"><p>که در حدیقه تن کرده جاری آب روان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فضای قدرت بی‌علتش چو دریایی‌ست</p></div>
<div class="m2"><p>که چشم عقل در او زورقی‌ست سرگردان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار تحفه صلوات بر روان کسی</p></div>
<div class="m2"><p>که برگزیده آن حضرتست از انسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبی امی مکی محمد قرشی</p></div>
<div class="m2"><p>ملاذ نوع بشر مقتدای خلق جهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس است در صفت ذات او همین تعریف</p></div>
<div class="m2"><p>که هست به نعم او حضرت شه مردان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی والی والا علی عادل دل</p></div>
<div class="m2"><p>نظام دور فلک ناظم زمین و زمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شه سریر سلونی امام انس و ملک</p></div>
<div class="m2"><p>که وصف او چو صفات خداست بی‌برهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنون روایتی از معجزات او بشنو</p></div>
<div class="m2"><p>که تازه می‌شود از استماع او دل و جان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روایتست که چون آن امام کافی رای</p></div>
<div class="m2"><p>شد از مدینه برون کوفه را گرفت مکان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز هر دیار نهادند روی جانب او</p></div>
<div class="m2"><p>بدرد خویش ازو یافتند همه درمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میان مردم بصره دران زمان بودند</p></div>
<div class="m2"><p>محب و معتقد شاه اولیا دل و جان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دو نورسید کامل دو نطفه طاهر</p></div>
<div class="m2"><p>دو مخلص متشرع دو طالب ایمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به اصل هردو برادر دو گوهر از یک بحر</p></div>
<div class="m2"><p>به فصل هردو برآورده گل ز یک بستان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به عزم دولت پابوس آن سر آمد دهر</p></div>
<div class="m2"><p>شدند جانب کوفه ز شهر بصره روان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قضا رسید در اثنای ره ز ربح سفر</p></div>
<div class="m2"><p>تن برادر مه را نماند تاب و توان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدان رسید که جان از تن فسرده او</p></div>
<div class="m2"><p>برون رود چو خدنگی که بگذرد کمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گشود لب به برادر وصیتی فرمود</p></div>
<div class="m2"><p>که ای مراد دل و کام دیده نگران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دو غنچه بودیم از گلبن وفا زده سر</p></div>
<div class="m2"><p>امید بود که خواهیم شد گل خندان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو بهر تحفه درگاه شاه باقی باش</p></div>
<div class="m2"><p>که ناشکوفه بهار مرا رسیده خزان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دو لعل بودیم در رنگ خود به مرتبه</p></div>
<div class="m2"><p>سوی خزانه شاهی نهاده روی از کان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز سنگ حادثه بر من چنین شکست رسید</p></div>
<div class="m2"><p>تو بهر هدیه آن گنج مستدام بمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ولی وصیتم اینست بر تو ای همزاد</p></div>
<div class="m2"><p>که چون رسی تو به درگاه خواجه سلمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرا ز گوشه خاطر بسی فرو مگذار</p></div>
<div class="m2"><p>نیاز من بگذار و سلام من برسان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مشو ز لوح دل خویش نقش نام مرا</p></div>
<div class="m2"><p>حکایت من گم‌گشته پیش او برخوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگو که ای شه فرخنده‌رای فرخ‌رخ</p></div>
<div class="m2"><p>به خاک آروزی درگه تو برد فلان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به یاد خاک درت داد زندگی بر باد</p></div>
<div class="m2"><p>چنانکه بود به یاد تو زنده مرده بان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بمرد و آرزوی دیدن تو در جانش</p></div>
<div class="m2"><p>برفت و جان و دلش سوی وصل تو نگران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هنوز درد دل خود نکرده بود تمام</p></div>
<div class="m2"><p>که کرد مرغ روانش ز دام تن طیران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همای اوج وفا بود کرد پروازی</p></div>
<div class="m2"><p>ز دشت محنت غم سوی روضه رضوان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو شد برادر مهتر اسیر دام اجل</p></div>
<div class="m2"><p>دل برادر کهتر بسوخت در هجران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بسی ز گردش ایام بر فشاند سرشک</p></div>
<div class="m2"><p>بسی ز بی کسی هجر بر کشیده فغان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز هول غربت و رنج ره و مهابت مرگ</p></div>
<div class="m2"><p>جوان سوخته مانده بود بس خیران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که شرط دفن برادر چه سان بجای آرد</p></div>
<div class="m2"><p>چگونه گنج جهان را کند به خاک نهان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که ناگه از طرفی طرفه راکبی چون خضر</p></div>
<div class="m2"><p>رسید تیزتر از آب چشمه حیوان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خجسته ناقه‌سواری که پای ناقه او</p></div>
<div class="m2"><p>به قطع بادیه فیض داشت طی مکان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هزار صالح و ویس قرن نهاده جبین</p></div>
<div class="m2"><p>ز پای ناقه او هرکجا که مانده نشان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نقاب بسته برخ لیک از مهابت او</p></div>
<div class="m2"><p>در آسمان شده خورشید ذره‌سان لرزان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زبان گشوده به او از خوبی لفظ فصیح</p></div>
<div class="m2"><p>چه گفت گفت ای رنج دیده دوران</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مرو مرو ز خود از غایت غم و اندوه</p></div>
<div class="m2"><p>بیا بیا ز من این لوح پاک را بستان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دمی بدار به پیش دماغ مرده خود</p></div>
<div class="m2"><p>که از روایح آن مرده تو یابد جان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جوان به موجب فرموده لوح را بستد</p></div>
<div class="m2"><p>بداشت پیش دماغ جوان مرده روان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جوان مرده ازان لوح یافت فیض حیات</p></div>
<div class="m2"><p>ز جای جست بنوعی که کس ز خواب گران</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو در برادر مهتر برادر کهتر</p></div>
<div class="m2"><p>حیات دید بشد غرق بحر ذوق جنان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که رفت از سر او هوش و بی‌خبر افتاد</p></div>
<div class="m2"><p>به روی خاک به سان سرشک خود غلتان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>غریب واقعه دست داده در یکدم</p></div>
<div class="m2"><p>که مرد زنده و از مرگ یافت مرده امان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو هردو چشم گشودند بعد از آن احوال</p></div>
<div class="m2"><p>ز لوح و ناقه و ناقه نشین نبود نشان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جوان حقیقت احوال با برادر گفت</p></div>
<div class="m2"><p>ز مردن از اثر لوح و شخص فیض رسان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دمی به حیرت آن واقعه فرو رفتند</p></div>
<div class="m2"><p>که این نتیجه خوابست یا خیال گمان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زدند بار تحیرکنان قدم در ره</p></div>
<div class="m2"><p>به شهر کوفه رسیدند خرم و خندان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>قدم به مسجد کوفه نهاده با صد ذوق</p></div>
<div class="m2"><p>به روی شاه گشودند چشم اشک‌فشان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خوشا کسی که پی آرزوی بی‌غایت</p></div>
<div class="m2"><p>خوشا کسی که بس از اشتیاق بی‌پایان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به روی دوست گشاید به کام دل دیده</p></div>
<div class="m2"><p>کند مطالعه صفحه رخ جانان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>امام انس و ملایک علی بو طالب</p></div>
<div class="m2"><p>پس از نمودن رسم نوازش و احسان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خبر ز کیفیت سرگذشت ره پرسید</p></div>
<div class="m2"><p>غرض که راه نهان را کند به خلق عیان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>رموز مردن و آن لوح و شخص ناقه‌نشین</p></div>
<div class="m2"><p>حدیث یافتن درد و دیدن درمان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو از برادر کهتر همه به عرض رسید</p></div>
<div class="m2"><p>امیر جمله مردان علی عالی‌شان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>میان خلق بدان نوجوان چنین فرمود</p></div>
<div class="m2"><p>که ای نموده خدا مشکل ترا آسان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گرت فتد به همان چشم بار دگر</p></div>
<div class="m2"><p>شناختی بتوانی ز غایت عرفان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جواب داد که بالله تصور آن لوح</p></div>
<div class="m2"><p>مراست نقش پذیرفته بر صحیفه جان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بگو چسان نشناسم خجسته لوحی را</p></div>
<div class="m2"><p>که داده است مرا از غم زمانه امان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>روان ز جیب همان لوح را برون آورد</p></div>
<div class="m2"><p>امین تخت نجف سرو سایه سبحان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جوان چو دید همان طرفه لوح را بشناخت</p></div>
<div class="m2"><p>به خاک پای شه افتاد اضطراب‌کنان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>که یا امام زمان اعتقاد ماست درست</p></div>
<div class="m2"><p>تویی که هست صفات تو برتر از امکان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به جز تو کیست که هم حاضرست و هم غایب</p></div>
<div class="m2"><p>به جز تو کیست که هم سرورست هم سلطان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تویی که روح رسول الهی سر خدا</p></div>
<div class="m2"><p>تویی که اصل حدیثی و معنی قرآن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>معاون دم جان‌بخش عیسی مریم</p></div>
<div class="m2"><p>مقوی ید بیضای موسی عمران</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خداست مظهر علم تو و تو مظهر او</p></div>
<div class="m2"><p>ترا چگونه جدا از خدا کند نادان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>روایت است که بسیار کس به آن معجز</p></div>
<div class="m2"><p>ز جام صدق کشیدند شربت ایمان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>علی‌ست آن که جهان را همه مسلمان ساخت</p></div>
<div class="m2"><p>به ضرب تیغ و به تأثیر حجت برهان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>علی‌ست آن که دل دیده محبانش</p></div>
<div class="m2"><p>منزه است ز خبث و شرارت شیطان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دلا ز سر بگذر در ره وفای علی</p></div>
<div class="m2"><p>که مردنست درین ره حیات جاویدان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هزار شکر که از جان و دل فضولی زار</p></div>
<div class="m2"><p>همیشه هست علی را کمین مناقب خوان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نهاده روی به درگاه آل پیغمبر</p></div>
<div class="m2"><p>گرفته خوی به نفرین آل بومروان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>امید هست که تا هست گردش گردون</p></div>
<div class="m2"><p>امید هست که تا هست گنبد گردان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>همیشه از کرم مرتضی شود ممدود</p></div>
<div class="m2"><p>ظلال سلطنت و جاه پادشاه زمان</p></div></div>