---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>دلا تا کی چنین در قید آن زلف دو تا باشم</p></div>
<div class="m2"><p>اسیر دام محنت بسته بر دام بلا باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی بر یاد آن لبها سرشک لاله گون ریزم</p></div>
<div class="m2"><p>گه از بار غم آن ابروان خم دو تا باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مران از کوی خویشم ای پری هر دم برسوایی</p></div>
<div class="m2"><p>چو من دیوانه ام بگذار در دارالشفا باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارم تاب دوری اینقدر از بخت می خواهم</p></div>
<div class="m2"><p>نباشم بی تو یکدم با تو باشم هر کجا باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>براهت در طلب عاجز نیم کز ناتوانیها</p></div>
<div class="m2"><p>درین ره می توانم همره باد صبا باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوایت را نخواهم کرد بیرون چون حباب از سر</p></div>
<div class="m2"><p>مگر روزی که سر بر باد داده زین هوا باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من از رنج و عنای عشق دارم نشئه راحت</p></div>
<div class="m2"><p>ندارم راحتی هر گه که بی رنج و عنا باشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکش ای هجر در کنج غمم بگذار تا زینسان</p></div>
<div class="m2"><p>بسوز و گریه شبها شمع این ظلمت سرا باشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حرامم باد لذتهای درد عافیت بخشت</p></div>
<div class="m2"><p>اگر با لذت درد تو مشتاق دوا باشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین تا کی من از تو بی خود و تو بی خبر باشی</p></div>
<div class="m2"><p>تو محجوب از من و من آرزومند لقا باشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من و عشق بتان تا زنده ام حاشا که بگذارم</p></div>
<div class="m2"><p>طریق عاشقان و زاهدان خودنما باشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه رسم و راه زاهد رنگ و بوی عقل و دین جویم</p></div>
<div class="m2"><p>ز قول و فعل واعظ طالب صدق و صفا باشم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به سجاده بساط آرای اطواری حیل کردم</p></div>
<div class="m2"><p>به سبحه سلسله جنبان آیین ریا باشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تعین خوش ندارم تا نیابد کس نشان از من</p></div>
<div class="m2"><p>همان بهتر که گم در وادی فقر و فنا باشم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملول از اختلاط ناکسانم ای خوش آن روزی</p></div>
<div class="m2"><p>که نی با من کسی بی با کسی من آشنا باشم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نمی گنجد مرا در سر که از دون همتی چون مور</p></div>
<div class="m2"><p>برای دانه آزرده در هر زیر پا باشم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر آن می داردم همت که کام از حرص کم جویم</p></div>
<div class="m2"><p>نهاده بر سر هر گنج پا چون اژدها باشم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کند همت مرا از جمله آفاق مستغنی</p></div>
<div class="m2"><p>فقیر محتشم سیرت گدای پادشا باشم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نگردانم سگ کوی و گدای خوان کس خود را</p></div>
<div class="m2"><p>سگ کوی و گدای خوان شاه اولیا باشم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زبان بر بندم از ذکر ثنای غیر هر ساعت</p></div>
<div class="m2"><p>گشایم نطق مداح علی المرتضا باشم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شهنشاهی که ممکن نیست پایان ثنای او</p></div>
<div class="m2"><p>اگر یابم دوام عمر و مشغول ثنا باشم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بشرطی داده ایزد حسن گفتاریم که تا هستم</p></div>
<div class="m2"><p>به بستان مناقب بلبل دستان سرا باشم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گهی دامان و صف پنجه عنتر فکن گیرم</p></div>
<div class="m2"><p>گهی مفتاح باب معجز خیبرگشا باشم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گهی پرده کش گلهای باغ انما گردم</p></div>
<div class="m2"><p>گهی رشته کش درهای درج هل اتی باشم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گهی زنک شک از آیینه لاسیف بزدایم</p></div>
<div class="m2"><p>گهی آیینه دار نور فیض لافتی باشم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دم از اوصاف آن شه می زنم صد غنچه را در دم</p></div>
<div class="m2"><p>سزد گر چون صبا از نکهت آن دلگشا باشم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بخاک پای او پی برده ام کو خضر فرح پی</p></div>
<div class="m2"><p>که او را سوی آب زنده گانی رهنما باشم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>منم جا کرده در سلک سکان آشیان او</p></div>
<div class="m2"><p>مرا می زیبد از سر خلقه اهل وفا باشم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیم در مهر او از مالک و عمار و بوذر کم</p></div>
<div class="m2"><p>چو صدقم از همه بیش است کم از کس چرا باشم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شها شفقت شعارا چشم آن دارم که در راهت</p></div>
<div class="m2"><p>ز گمراهی نباشم اهل خوف اهل رجا باشم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرا کافیست از عالم سر کوی تو سر منزل</p></div>
<div class="m2"><p>نمی خواهم که یکدم از سر کویت جدا باشم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بهر نیک و بدی بخت از تو نومیدم نگردانم</p></div>
<div class="m2"><p>اگر نیکم اگر بد قابل عفو و عطا باشم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همین بس اعتقاد من که در معموره هستی</p></div>
<div class="m2"><p>همین حاکم ترا دانم همین تابع ترا باشم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بحیر کسر احکامت سر تسلیم پیش آرم</p></div>
<div class="m2"><p>بامر و نهی فرمانت سگ طوق رضا باشم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز لطفت گویم ار کیفیت ذوق و صفا یابم</p></div>
<div class="m2"><p>ز قهرت دانم ار شایسته جور و جفا باشم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه وقت انکسار عجز دل رنجانم از گردون</p></div>
<div class="m2"><p>نه در خیر و تدارک نیز ممنون قضا باشم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر سلطانی عالم دهندم کی پسند افتد</p></div>
<div class="m2"><p>پسند است اینکه درگاه ترا کمتر گدا باشم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنان نبود که با خدام درگاهت شوم همدم</p></div>
<div class="m2"><p>اگر با قدسیان در بارگاه کبریا باشم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شها این مقصد و این مدعا دارم که در عالم</p></div>
<div class="m2"><p>ز لطفت و اصل هر مقصد و هر مدعا باشم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز بحر فیض دریای نجف موجی رسد بر من</p></div>
<div class="m2"><p>کنم غسل طریقت پاک از رجس خطا باشم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گهی از سایران جلوه گاه مصطفی گردم</p></div>
<div class="m2"><p>گهی از زایران روضه خیرالنسا باشم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز راه صدق باشم قاصد طوف حسن یعنی</p></div>
<div class="m2"><p>بدان شه قاصد درگاه شاه کربلا باشم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز زین العابدین و باقر و صادق رسم جایی</p></div>
<div class="m2"><p>بارشاد ائمه قابل قرب خدا باشم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز خاک خطه بغداد یابم نکهت موسی</p></div>
<div class="m2"><p>ز اقلیم خراسان طالب نور رضا باشم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جواد از جود هادی از سخا بخشد مرا بهره</p></div>
<div class="m2"><p>ز لطف عسکری مستوجب جود و سخا باشم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دمی کز ملک معنی سوی صورت مهدی هادی</p></div>
<div class="m2"><p>بر افرازد لوای معدلت زیر لوا باشم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>الهی چون فضولی روزیم کرد آن که پیوسته</p></div>
<div class="m2"><p>ز الطاف علی و آل با برگ و نوا باشم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو من در ابتدا از شاه مردان برده ام فیضی</p></div>
<div class="m2"><p>چنان کن کین چنین از ابتدا تا انتها باشم</p></div></div>