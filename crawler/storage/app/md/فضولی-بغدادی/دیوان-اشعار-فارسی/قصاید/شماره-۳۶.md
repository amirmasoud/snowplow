---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>مرحبا ای قلم شمع شبستان خیال</p></div>
<div class="m2"><p>نامی نامیه پیغمبر معراج خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستگیر فقرای ادب آموز ملوک</p></div>
<div class="m2"><p>مرجع اهل دل و ملجاء ارباب کمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن معشوقه صورت ز تو عنبر گیوست</p></div>
<div class="m2"><p>چهره شاهد زیبا ز تو مشکین خط و خال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قد بر افراخته ساخته زیور حسن</p></div>
<div class="m2"><p>طره بر تافته یافته زیب جمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ز ریحان تو گلزار صحایف مملو</p></div>
<div class="m2"><p>وی ز یاقوت تو دامان نسخ مالامال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضاعف الله لک القدر لنا فی الایام</p></div>
<div class="m2"><p>فتح الله بک الباب لنا فی الامال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار هرکس که شدی مژده دولت دادی</p></div>
<div class="m2"><p>ای صریر نی تو رونق بزم اقبال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه سبک روح کسی کز پی انجام امور</p></div>
<div class="m2"><p>می دوی پای ز سر کرده بسرعت مه و سال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لازم طبع تو فیضیست ولی فیض عمیم</p></div>
<div class="m2"><p>روش کار تو سحریست ولی سحر حلال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس بود شاهد اقبال تو این حسن قبول</p></div>
<div class="m2"><p>که شدی محرم سرو چمن جاه و جلال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یوسف مصر وفا خضر ره اهل صفا</p></div>
<div class="m2"><p>آصف پاک گهر سرور پاکیزه خصال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وقت آنست که در مجلس آن عالی قدر</p></div>
<div class="m2"><p>کنی از راه کرم یاد من شیفته حال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راز پنهان من نامه سیه بر ورقی</p></div>
<div class="m2"><p>بنویسی و برو عرض کنی بی اهمال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیده بودی که چه سان سلوکم زین پیش</p></div>
<div class="m2"><p>چه روش داشت فلک با من و چون بود احوال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زلف محبوب بکف داشتم و جام طرب</p></div>
<div class="m2"><p>نشئه جام طرب داشتم و ذوق وصال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بنگر بر من و بر صورت حالم حالا</p></div>
<div class="m2"><p>که رسیدست بر آیینه من گرد زوال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از که پرسم ره این بادیه که گردانم</p></div>
<div class="m2"><p>بکه گویم غم دل مانده ام از حیرت لال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست امیدگهم بهر مرادی جز خوان</p></div>
<div class="m2"><p>نیست هم مشورتم از پی فتحی جز فال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هست زین واقعه آزار معبر کارم</p></div>
<div class="m2"><p>هست زین واسطه شیوه جفای رمال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من ندانم بچنین درد و بلا استحقاق</p></div>
<div class="m2"><p>من ندارم بچنین محنت و غم استهمال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه خلق برینند که جز آصف عهد</p></div>
<div class="m2"><p>هیچ کس نیست که طی گردد ازو این اشکال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بسته ام دل چو فضولی بنهانی قلمش</p></div>
<div class="m2"><p>چشم دارم که شود بار دران طرفه خیال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آه اگر بخت کند سستی آن طایر قدس</p></div>
<div class="m2"><p>گردد از حال خسته بمن فارغ بال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دارم امید که آن ده هر سایل</p></div>
<div class="m2"><p>سوی غیری ندهد راه بمن بهر سؤال</p></div></div>