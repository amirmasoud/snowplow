---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>اگر بمن نبود پادشاه را لطفی</p></div>
<div class="m2"><p>نمی کنم گله کان هم نشان شفقت اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ضعف قالب من واقع است می داند</p></div>
<div class="m2"><p>که بار فاقه سبک تر ز بار منت اوست</p></div></div>