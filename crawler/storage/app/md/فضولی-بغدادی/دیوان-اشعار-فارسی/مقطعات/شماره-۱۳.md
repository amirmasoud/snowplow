---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>آفرین بر منعمی کز بهر اظهار ثنا</p></div>
<div class="m2"><p>فیض توفیق سخن ما را میسر کرده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملکهای بی حد اصناف نظم و نثر را</p></div>
<div class="m2"><p>جمله بر تیغ زبان ما مسخر کرده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست از تکرار الفاظ سخن ما را ملال</p></div>
<div class="m2"><p>هر یکی را نعمت غیر مکرر کرده است</p></div></div>