---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>من از اقلیم عرب حیرتی از ملک عجم</p></div>
<div class="m2"><p>هر دو کردیم به اظهار سخن کام طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یافتیم از دو کرم پیشه مراد دل خویش</p></div>
<div class="m2"><p>او زر از شاه عجم من نظر از شاه عرب</p></div></div>