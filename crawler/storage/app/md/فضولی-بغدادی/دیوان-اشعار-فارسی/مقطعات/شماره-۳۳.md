---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>در مقامی گر شود جان عزیزت منزجر</p></div>
<div class="m2"><p>رحم بر جان عزیزت کن برو جای دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تو آسانست تغییر مکان کردن ولی</p></div>
<div class="m2"><p>نیست آسان بی تو جان را عزم مأوای دگر</p></div></div>