---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>دوش طفلی پری رخی دیدم</p></div>
<div class="m2"><p>گفتم ای شوخ شکرین گفتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چرا از کمال استغنا</p></div>
<div class="m2"><p>فارغی از مشقت همه حار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پدر و مادرند در تک و دو</p></div>
<div class="m2"><p>تا ترا پرورند لیل و نهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت ما کاملان دورانیم</p></div>
<div class="m2"><p>ناقصانند این گروه کبار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانکه طفلیم ما و بر طفلان</p></div>
<div class="m2"><p>نیست واجب رعایت اطوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که شویم از خلاف آن عادت</p></div>
<div class="m2"><p>قابل رد ایزد جبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لیک این بالغان تا بالغ</p></div>
<div class="m2"><p>که دم از عقل می زنند و وقار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیستند آنچنان که می باید</p></div>
<div class="m2"><p>ناقصانند و ناتمام عیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناقصان گر کنند در عالم</p></div>
<div class="m2"><p>خدمت کاملان نباشد عار</p></div></div>