---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>مردم این دیار را با من</p></div>
<div class="m2"><p>اثر شفقت و عنایت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا درین قوم نیست معرفتی</p></div>
<div class="m2"><p>یا مرا هیچ قابلیت نیست</p></div></div>