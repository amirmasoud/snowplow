---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>عادت اینست فیض فطرت را</p></div>
<div class="m2"><p>در بنای وجود نوع بشر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که رسد تا زمان رشد و بلوغ</p></div>
<div class="m2"><p>طفل را مهربانی از مادر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهد او را کمال جسمانی</p></div>
<div class="m2"><p>مادر مهربان بخون جگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون رسد وقت آن که در جسدش</p></div>
<div class="m2"><p>بدمد روح جبرئیل هنر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دختر از مادرش جدا نشود</p></div>
<div class="m2"><p>زآنکه هستند جنس هم دیگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پسر از اقتضای جنسیت</p></div>
<div class="m2"><p>طلبد قرب و انتساب پدر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو همان طفلی ای دل غافل</p></div>
<div class="m2"><p>که نداری ز حال خویش خبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم صورتست مادر تو</p></div>
<div class="m2"><p>که هواخواه تست شام و سحر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پدرت فیض عالم علویست</p></div>
<div class="m2"><p>که در ایجاد تو ازوست اثر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند مانی مقید صورت</p></div>
<div class="m2"><p>بطلب رتبه ازین برتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چند گردی بگرد مادر خود</p></div>
<div class="m2"><p>نیستی دختر ای نکو محضر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرد شو مرد کز پدر یابی</p></div>
<div class="m2"><p>اثر التفات فیض نظیر</p></div></div>