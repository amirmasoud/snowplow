---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>در صدف صدق جناب متولی</p></div>
<div class="m2"><p>کز رای منیرش عتباتست منور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمریست دماغ دل سکان مشاهد</p></div>
<div class="m2"><p>از رایحه مکرمت اوست معطر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاکیزه نهادی که مراد دو جهانش</p></div>
<div class="m2"><p>از خدمت اولاد رسولست میسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرخنده مآلی که دلش راست همیشه</p></div>
<div class="m2"><p>اندیشه غمخواری اولاد پیمبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آرایش معموری هر مرقد عالی</p></div>
<div class="m2"><p>شد رتبه او را سبب رفعت دیگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای باد اگر فرصت گفتار بیابی</p></div>
<div class="m2"><p>در خدمت آن ذات مصفای مطهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ما برسان بندگی و عرض کن این حال</p></div>
<div class="m2"><p>کاری در همه جا در همه فن بر همه سرور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چند که در فضل و هنر مثل نداری</p></div>
<div class="m2"><p>غافل مشو از نکته گذاران سخن ور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما خاک نشینان سر کوی بلاییم</p></div>
<div class="m2"><p>ما را نتوان داشت بهر سفله برابر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما آینه داران بد و نیک جهانیم</p></div>
<div class="m2"><p>گر نیستی آگه بگشا دیده و بنگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خم یافته در خدمت ما قامت گردون</p></div>
<div class="m2"><p>پر گشته ز آوازه ما دهر سراسر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما راتبه خواران در آل رسولیم</p></div>
<div class="m2"><p>عمریست که این راتبه داریم مقرر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مسدود نگشته در این راتبه بر ما</p></div>
<div class="m2"><p>زانروی که هستیم بدین راتبه در خور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ماییم پسندیده دوران بقناعت</p></div>
<div class="m2"><p>پیران جوان بخت و فقیران توانگر</p></div></div>