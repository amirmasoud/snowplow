---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>ای که رای روشنت آیینه گیتی نماست</p></div>
<div class="m2"><p>هیچ سری نیست کز رای تو باشد در حجاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نقاب عنبرین هر دم عروس خامه ات</p></div>
<div class="m2"><p>شاهد صد راز را از چهره می گیرد نقاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک دانش راست از طرز مثالت انتظام</p></div>
<div class="m2"><p>گنج معنی راست از مفتاح کلکت فتح باب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرورا عمریست در تن جان بر لب آمده</p></div>
<div class="m2"><p>می کند در آرزوی پای بوست اضطراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آرزوی دولت پابوس خدام درت</p></div>
<div class="m2"><p>می رباید روز و شب از دل قرار از دیده خواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بلای دوریت معذورم از ضعف بدن</p></div>
<div class="m2"><p>چون کنم ناچار می باید کشیدن این عذاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه دورم غافل از عرض نیازی نیستم</p></div>
<div class="m2"><p>شد نیاز من بدرگاه تو بیرون از حساب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می نویسم حال دل اما نمی بخشد اثر</p></div>
<div class="m2"><p>می فرستم شرح غم اما نمی آید جواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند از خاک درت بوی رعایت نشنوم</p></div>
<div class="m2"><p>پیش تو تا کی دعای من نباشد مستجاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وه چه باشد گر کند نام مرا کلکت رقم</p></div>
<div class="m2"><p>وه چه باشد گر کنی در ذکر من کسب ثواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست دریا را چنان عادت که از نزدیک و دور</p></div>
<div class="m2"><p>می کند ارباب حاجت را بفیضی کامیاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که نزدیک است او را می دهد در از صدف</p></div>
<div class="m2"><p>بهر دوران نیز آبی می فرستد پا سحاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چشمه خورشید هم در طبع دارد حالتی</p></div>
<div class="m2"><p>می دهد هم دور و هم نزدیک خود را آب و تاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نی همین در آسمان مه می ستاند نور از او</p></div>
<div class="m2"><p>در زمین هم می برد از آتش او لعل آب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای که هم از آفتاب افزون هم از دریا بهی</p></div>
<div class="m2"><p>باش در احسان به از دریا فزون از آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به ز نزدیکان خود بر من که دورم رحم کن</p></div>
<div class="m2"><p>بر گناه دوریم منگر مکن بر من عتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر چه دورم از تو دارم بیش تر چشم کرم</p></div>
<div class="m2"><p>مه ز مهر از دور بهتر می کند نور اکتساب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از تو بر من گر رسد توقیع لطفی دور نیست</p></div>
<div class="m2"><p>بر زمین از آسمان رسم است تنزیل کتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا زمین را در جبلت هست امکان ثواب</p></div>
<div class="m2"><p>تا فلک را هست عادت در طبیعت انقلاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روی دولت را ز درگاهت نباشد انحراف</p></div>
<div class="m2"><p>رای رفعت را ز فرمانت نباشد اجتناب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جز دعایت نیست اوراد فضولی روز و شب</p></div>
<div class="m2"><p>کار او اینست و بس والله اعلم باالصواب</p></div></div>