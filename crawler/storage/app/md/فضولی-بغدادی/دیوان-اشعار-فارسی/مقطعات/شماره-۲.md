---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>حضرت مصطفی بسعی تمام</p></div>
<div class="m2"><p>خانه شرع را نهاد بنا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کمال ثبات و استحکام</p></div>
<div class="m2"><p>تا قیامت بری ز بیم فنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حفظ آن فرض بر وضیع و شریف</p></div>
<div class="m2"><p>ضبط آن حتم بر غنی و گدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا اثر از بنای عالم هست</p></div>
<div class="m2"><p>یارب آن بنیه مبارک را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دور ساز و نگاه دار مدام</p></div>
<div class="m2"><p>از فساد دو فرقه سفها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اول از قول و فعل طایفه</p></div>
<div class="m2"><p>که ندارند شمه ز حیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دل سختشان نکرده گذر</p></div>
<div class="m2"><p>اثر علم و طاعت و تقوا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خویش را کرده اند داخل آل</p></div>
<div class="m2"><p>با وجود هزار خبط و خطا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخنها می زنند بر اسلام</p></div>
<div class="m2"><p>ز خدا وز خلق بی سر و پا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوم آن جاهلان بی معنی</p></div>
<div class="m2"><p>که نشینند بر سریر قضا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلق را مرجع امور شوند</p></div>
<div class="m2"><p>حکم رانند مقتضای هوا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حکمهای خلاف شرع کنند</p></div>
<div class="m2"><p>در هوای زخارف دنیا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روز محشر که حق شود قاضی</p></div>
<div class="m2"><p>هر کسی حق خود کند دعوا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این دو قوم سفیه بد افعال</p></div>
<div class="m2"><p>که باسلام کرده اند جفا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا چه خواهند دید در دوزخ</p></div>
<div class="m2"><p>بفعالی که کرده اند جزا</p></div></div>