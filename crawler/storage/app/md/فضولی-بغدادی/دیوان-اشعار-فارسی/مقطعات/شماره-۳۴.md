---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>مرده دیدم پریشان گشته اجزای تنش</p></div>
<div class="m2"><p>کرده منزل استخوان کله اش از مار و مور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبع را از فکر اوصاف غم افزایش ملال</p></div>
<div class="m2"><p>دیده را از دیدن حال پریشانش نفور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم ای دوران چرا این نطفه پاکیزه را</p></div>
<div class="m2"><p>از سریر دولت آوردی بمحنت گاه گور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چیست جرم این که در عالم سزای خویش یافت</p></div>
<div class="m2"><p>فطرت او را چه شد واقع که واقع شد فتور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت ای از حکمت احوال دوران بی خبر</p></div>
<div class="m2"><p>غره بود این بی ادب اینست انجام غرور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قبل ازین خلقت وجودش را نبود این اعتبار</p></div>
<div class="m2"><p>ره نمودم هستی او را بصحرای ظهور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من شدم مشاطه حسنش بزلف و خط و خال</p></div>
<div class="m2"><p>من شدم استاد تعلیمش به ادراک و شعور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یافت چون تمکین استقلال قدر و منزلت</p></div>
<div class="m2"><p>گشت چون سرمست جام عشرت عیش و سرور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دید خاک و انجم و افلاک را محکوم خود</p></div>
<div class="m2"><p>کرد دعوای انانیت بتدریج و مرور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد چنان سرمست کز مستی ندانست از کجاست</p></div>
<div class="m2"><p>در طبیعت میل در دل معرفت در دیده نور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با وجود آن که می کردند دایم خدمتش</p></div>
<div class="m2"><p>طعنه می زد بر مدار چرخ و دوران و دهور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در جمیع عمر خود هرگز ز من راضی نشد</p></div>
<div class="m2"><p>گشت بر من نیز استرداد نعمتها ضرور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مستعار چند کز من داشت بگرفتم ازو</p></div>
<div class="m2"><p>من ازو چیزی که از وی بود بگرفتم بزور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حال او اینست حالا تا چه بیند عاقبت</p></div>
<div class="m2"><p>از جزای این عمل در موقف عرض امور</p></div></div>