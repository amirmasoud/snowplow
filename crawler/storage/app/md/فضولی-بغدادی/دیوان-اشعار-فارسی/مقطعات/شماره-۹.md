---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ای غره بر لطافت حسن و جمال خود</p></div>
<div class="m2"><p>آیا چرا جنون تو بر عقل غالب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شان تو مستعد کمالات معنوی</p></div>
<div class="m2"><p>ذات تو مستحق علو مراتب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در قید حسن صورت فانی چه فایده</p></div>
<div class="m2"><p>کسب کمال کن که ترا آن مناسب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گیرم که صبح روز جوانیست حسن تو</p></div>
<div class="m2"><p>نور بقا مجوی ز صبحی که کاذب است</p></div></div>