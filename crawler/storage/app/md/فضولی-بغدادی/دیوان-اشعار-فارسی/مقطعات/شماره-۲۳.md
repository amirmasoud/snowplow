---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>نوجوانان را خدا در اول نشو و نما</p></div>
<div class="m2"><p>چون ملک از هر خلل پاک و مطهر آفرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدت تکلیف و طاعت را از ایشان رفع کرد</p></div>
<div class="m2"><p>بر دل احباب نقش طاعت ایشان کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی تردد نعمت جنت بر ایشان وقف شد</p></div>
<div class="m2"><p>بی تعب از خوان قسمت روزی ایشان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بتدریج زمان و امتداد روزگار</p></div>
<div class="m2"><p>عابدان متقی گردند و پیران و رشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با زر و زور و حیل این فرقه معصوم را</p></div>
<div class="m2"><p>هر که از عصمت بیندازد نخواهد خیر دید</p></div></div>