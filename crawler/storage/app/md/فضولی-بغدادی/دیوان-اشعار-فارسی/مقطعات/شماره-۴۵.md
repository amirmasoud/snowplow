---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>اگر چه داشت ز کیفیت جمیع لغت</p></div>
<div class="m2"><p>خبر ز غایت عرفان صفای طبع نبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرف نگر که فرستاد حضرت ایزد</p></div>
<div class="m2"><p>ز بهر نسبت ذاتش کلام را عربی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعلم اوست معاصی جهالت بوجهل</p></div>
<div class="m2"><p>بنور اوست مقارن شرار بولهبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمال معجزش این بس که علم و حکمت او</p></div>
<div class="m2"><p>ز علم و حکمت انسان نبود مکتسبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظهور کرد وجودش که بود محض ادب</p></div>
<div class="m2"><p>از آن گروه که بودند محض بی ادبی</p></div></div>