---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>فضیلت نسب و اصل خارج ذاتست</p></div>
<div class="m2"><p>بفضل غیر خود ای سفله افتخار مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بانتساب سلاطین و خدمت امرا</p></div>
<div class="m2"><p>که زایلست مزن تکیه اعتبار مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بصنعتی که درو هست شرط صحت دست</p></div>
<div class="m2"><p>مشو مقید و خود را امیدوار مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بملک و مال که هستند زایل و ذاهب</p></div>
<div class="m2"><p>اساس بنیه امید استوار مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر تراست هوای فضیلت باقی</p></div>
<div class="m2"><p>بعلم کوش و ز تحصیل علم عار مکن</p></div></div>