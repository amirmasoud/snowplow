---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>فریاد ازین سپهر ستمگر که در جهان</p></div>
<div class="m2"><p>هرگز نگشته شاد ز دوران او دلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هر که هست برده فراغت مدار او</p></div>
<div class="m2"><p>نه عالمی ازو شده راضی نه جاهلی</p></div></div>