---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>پرسیدم از بتی که ترا در جهان چرا</p></div>
<div class="m2"><p>شام و سحر تعرض عشاق عادت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا که هست رغبت عشق بتان خطا</p></div>
<div class="m2"><p>آزار اهل عشق بتان را عبادت است</p></div></div>