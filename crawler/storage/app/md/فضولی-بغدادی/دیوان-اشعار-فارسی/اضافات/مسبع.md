---
title: >-
    مسبع
---
# مسبع

<div class="b" id="bn1"><div class="m1"><p>وقتست که شام غم هجران بسر آید</p></div>
<div class="m2"><p>در باغ امل نخل تمنا ببر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه غرض از مطلع امید بر آید</p></div>
<div class="m2"><p>در ظلمت شب مژده فیض سحر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از برج وبال اختر طالع بدر آید</p></div>
<div class="m2"><p>در آرزوی وصل دعا کارگر آید</p></div></div>
<div class="b2" id="bn4"><p>دلدار سفر کرده ما از سفر آید</p></div>
<div class="b" id="bn5"><div class="m1"><p>ای درد و بلا دوریت ارباب وفا را</p></div>
<div class="m2"><p>نزدیک شو و دور کن این درد و بلا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داریم تمنای لقای تو خدا را</p></div>
<div class="m2"><p>بردار ز رخ پرده و بنمای لقا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای کرده فراموش درین واقعه ما را</p></div>
<div class="m2"><p>رحمی کن و مگذار دگر پیک صبا را</p></div></div>
<div class="b2" id="bn8"><p>کان بی خبر از تو بمن بی خبر آید</p></div>
<div class="b" id="bn9"><div class="m1"><p>عمریست که شوق رخ نیکوی تو داریم</p></div>
<div class="m2"><p>در جان حزین آرزوی روی تو داریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دل شکن سلسله موی تو داریم</p></div>
<div class="m2"><p>در سینه هوای قد دلجوی تو داریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در سر هوس خاک سر کوی تو داریم</p></div>
<div class="m2"><p>پیوسته خیال خم ابروی تو داریم</p></div></div>
<div class="b2" id="bn12"><p>ای خوبتر از هر چه به پیش نظر آید</p></div>
<div class="b" id="bn13"><div class="m1"><p>ماییم که در دوستیت یک جهتانیم</p></div>
<div class="m2"><p>در راه تو هر عهد که بستیم بر آنیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عمریست که تو غایبی و ما نگرانیم</p></div>
<div class="m2"><p>در آرزوی روی تو با آه و فغانیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دور از تو بسی خسته دل سوخته جانیم</p></div>
<div class="m2"><p>مپسند کزین بیش درین غصه بمانیم</p></div></div>
<div class="b2" id="bn16"><p>مگذار که جان از تن فرسوده بر آید</p></div>
<div class="b" id="bn17"><div class="m1"><p>در محنت هجران تو ای سرو سمنبر</p></div>
<div class="m2"><p>داریم دل مضطرب و جان مکدر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کو مژده وصلت که دماغ دل مضطر</p></div>
<div class="m2"><p>گردد ز نسیم اثرش باز معطر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز انسان که نومید ظفر از ایزد داور</p></div>
<div class="m2"><p>در دشت احد وقت هجوم صف کافر</p></div></div>
<div class="b2" id="bn20"><p>بهر مدد لشگر خیرالبشر آید</p></div>
<div class="b" id="bn21"><div class="m1"><p>شاهی که سر چرخ برین خاک در اوست</p></div>
<div class="m2"><p>در تمشیت کار قضا کارگر اوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اوراق فلک دفتر فضل و هنر اوست</p></div>
<div class="m2"><p>ایجاد بشر پرتو فیض نظر اوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بحریست که ارواح ائمه گهر اوست</p></div>
<div class="m2"><p>نخلیست که توفیق ولایت ثمر اوست</p></div></div>
<div class="b2" id="bn24"><p>هیهات که از نخل دگر این ثمر آید</p></div>
<div class="b" id="bn25"><div class="m1"><p>ای ذره از خاک درت طینت آدم</p></div>
<div class="m2"><p>وز دولت پابوس تو آن خاک مکرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تشریف امامت بوجود تو مسلم</p></div>
<div class="m2"><p>ذات تو بمجموعه موجود مقدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای بنیه عالم بتولای تو محکم</p></div>
<div class="m2"><p>تا هست ز عالم اثر بنیه بعالم</p></div></div>
<div class="b2" id="bn28"><p>مشکل که وجودی ز تو پاکیزه تر آید</p></div>
<div class="b" id="bn29"><div class="m1"><p>شاها تو همانی که برین صفحه ایام</p></div>
<div class="m2"><p>در اول حال از تو رقم شد خط اسلام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پیش از تو ز اسلام نمی برد کسی نام</p></div>
<div class="m2"><p>حالا که جهان یافته با شرع تو آرام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر جمعی پریشان سیه نامه بد نام</p></div>
<div class="m2"><p>خواهد که این صبح بتزویر شود شام</p></div></div>
<div class="b2" id="bn32"><p>مپسند که تذویر چنین معتبر آید</p></div>
<div class="b" id="bn33"><div class="m1"><p>با تیغ دو سر قصد سر اهل خطا کن</p></div>
<div class="m2"><p>سر متصل از تن بسر تیغ جدا کن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>درد دل شوریده ما بین و دوا کن</p></div>
<div class="m2"><p>از لطف تو هر کام که داریم روا کن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در کار عدو قاعده صبر رها کن</p></div>
<div class="m2"><p>در رهگذر شرع خود اندیشه ما کن</p></div></div>
<div class="b2" id="bn36"><p>مگذار که خاری بسر رهگذر آید</p></div>
<div class="b" id="bn37"><div class="m1"><p>شاها اثر دوستیست رونق دین است</p></div>
<div class="m2"><p>خوش آنکه درین دوستی از اهل یقین است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر کس که درت را ز غلامان کمین است</p></div>
<div class="m2"><p>در انجمن اهل وفا صدر نشین است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مداحی تو کار فضولی حزین است</p></div>
<div class="m2"><p>حقا که چنین بوده و آغاز چنین است</p></div></div>
<div class="b2" id="bn40"><p>تا بلبل طبعش بفصاحت بسر آید</p></div>