---
title: >-
    بخش ۱۴ - نشأه جام هفتم
---
# بخش ۱۴ - نشأه جام هفتم

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی آن شهد شیرین مذاق</p></div>
<div class="m2"><p>که ما را باو هست صد اشتیاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بده بیش ازین تلخ کامم مدار</p></div>
<div class="m2"><p>بدین تلخ کامی چو جامم مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا ساقی آن منشاء هر کمال</p></div>
<div class="m2"><p>که کامل ازو می شود اهل حال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بمن ده که دفع ملالی کنم</p></div>
<div class="m2"><p>درین جهل کسب کمالی کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا ساقی آن لعل یاقوت رنگ</p></div>
<div class="m2"><p>که سنگست بر شیشه نام و ننگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بمن ده که بخشد صفای تمام</p></div>
<div class="m2"><p>دلم را ز اندیشه ننگ و نام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببین مستیم مست تر کن مرا</p></div>
<div class="m2"><p>نهفتم قدح بی خبر کن مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که در نشاه هفتمین بی حجاب</p></div>
<div class="m2"><p>کنم شمه شرح حال خراب</p></div></div>