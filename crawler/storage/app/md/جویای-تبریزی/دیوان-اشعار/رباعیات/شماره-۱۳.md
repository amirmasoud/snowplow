---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>خود را هر کس ز راستیها آراست</p></div>
<div class="m2"><p>پیش ابنای دهر خار دلهاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سفله که پشت و روی او نیست یکی</p></div>
<div class="m2"><p>در باغ زمانه میرزای رعناست</p></div></div>