---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>از لاله و گل به رنگ و بوتر می بال</p></div>
<div class="m2"><p>از هر چه نمو کند نکوتر می بال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم گریانم آبیار است ترا</p></div>
<div class="m2"><p>ای داغ ازین شکفته روتر می بال</p></div></div>