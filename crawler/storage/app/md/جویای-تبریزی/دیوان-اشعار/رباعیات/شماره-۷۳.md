---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>سرسبز شده جهان ز فیض نوروز</p></div>
<div class="m2"><p>یعنی که رسید روز عشرت اندوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشسته امیر مؤمنان جان نبی</p></div>
<div class="m2"><p>بگرفته قرار حق به مرکز امروز</p></div></div>