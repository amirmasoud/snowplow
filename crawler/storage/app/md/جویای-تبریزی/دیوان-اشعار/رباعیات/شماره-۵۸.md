---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>با چشم تو آنرا که سروکار بود</p></div>
<div class="m2"><p>در مخمصهٔ عجب گرفتار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یک نگهت جان به سلامت نبرد</p></div>
<div class="m2"><p>هر چند که دل داده جگردار بود</p></div></div>