---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>غرق است به بحر جرم تا گردن من</p></div>
<div class="m2"><p>شرمنده بود دلم ز بد کردن من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش که هر نقش قدم چون سوزن</p></div>
<div class="m2"><p>چاهی شود از بهر فرو رفتن من</p></div></div>