---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>لاهور که دلبریش بسی عیار است</p></div>
<div class="m2"><p>از شوخی طبع با که و مه یار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کنچنیش دست زد خلق بود</p></div>
<div class="m2"><p>عیبش نکنی طلای دست افشار است</p></div></div>