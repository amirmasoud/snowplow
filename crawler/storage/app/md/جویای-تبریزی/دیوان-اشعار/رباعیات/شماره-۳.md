---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>لعلی آخر چه بود آواخ ترا</p></div>
<div class="m2"><p>ای کاش ببر کشیدمی آخ ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودی در ناسفته ز قیمت انداخت</p></div>
<div class="m2"><p>آن بدگهری که کرد سوراخ ترا</p></div></div>