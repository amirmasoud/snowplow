---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>تا چند ز معشوق بمانی به حجاب</p></div>
<div class="m2"><p>از خویش دمی برآی یعنی ز نقاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد هستی ما حجاب ما در ره عشق</p></div>
<div class="m2"><p>خود پردهٔ خود شدیم چوم چشم حباب</p></div></div>