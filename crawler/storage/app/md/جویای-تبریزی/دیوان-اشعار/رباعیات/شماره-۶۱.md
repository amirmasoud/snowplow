---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>خوب است که زخم یار کاری نبود</p></div>
<div class="m2"><p>جز لذت درد امیدواری نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمشیر توام کرد به یک زخم شهید</p></div>
<div class="m2"><p>با دولت تیز پایداری نبود</p></div></div>