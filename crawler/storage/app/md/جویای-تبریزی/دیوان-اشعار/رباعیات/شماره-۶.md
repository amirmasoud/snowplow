---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>تخمی است غمت بدل فشاندیم او را</p></div>
<div class="m2"><p>وز آب دو دیده بردماندیم او را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بالید چنان که ما در او گم شده ایم</p></div>
<div class="m2"><p>بنگر کآخر کجا رساندیم او را</p></div></div>