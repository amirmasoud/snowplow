---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>آن دل که ز جام عشق مسرور بود</p></div>
<div class="m2"><p>در سینه اگر بماند مجبور بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی در قید جسم خاکی باشد</p></div>
<div class="m2"><p>آخر تا چند زنده در گور بود</p></div></div>