---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>گویند مدام عارفان آگاه</p></div>
<div class="m2"><p>کاندر خلق است جلوهٔ سراله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقصی به علوشان گردون نرسد</p></div>
<div class="m2"><p>هر چند که خویش را نماید در چاه</p></div></div>