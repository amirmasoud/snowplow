---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>ماند روش کبک به لغزیدن تو</p></div>
<div class="m2"><p>منت به زمین نهد خرامیدن تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لحم و شحم است بسکه سر تا پایت</p></div>
<div class="m2"><p>آغوش نظاره پر شد از دیدن تو</p></div></div>