---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>افسوس که فیض از دل آگه نبری</p></div>
<div class="m2"><p>راهی به خود از همت کوته نبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خویش برون خرام و در منزل باش</p></div>
<div class="m2"><p>ناکرده سفر ز خود به خود ره نبری</p></div></div>