---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>دل چند به فکر باغ و مسکن باشد</p></div>
<div class="m2"><p>آخر چو مآل کار مردن باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون هست فنا لازمهٔ هستی تو</p></div>
<div class="m2"><p>این آمدنت دلیل رفتن باشد</p></div></div>