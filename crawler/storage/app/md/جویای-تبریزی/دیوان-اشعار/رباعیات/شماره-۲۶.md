---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>نواب کزو رواج جود و کرم است</p></div>
<div class="m2"><p>سرهای سران پیش بزرگیش خم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سایهٔ عدلش همه را آرام است</p></div>
<div class="m2"><p>جز من که مرا دوری از آن در ستم است</p></div></div>