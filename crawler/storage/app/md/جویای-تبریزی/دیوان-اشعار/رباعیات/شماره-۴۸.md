---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>دل غیر تأنی به صفایی نرسد</p></div>
<div class="m2"><p>از بیتابی به مدعایی نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبنم که رسد به مهر از نرم روی است</p></div>
<div class="m2"><p>از گرم روی شرر به جایی نرسد</p></div></div>