---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>ایدل تا کی به گل رخان یار شوی</p></div>
<div class="m2"><p>بینم که تو هم چو دیده خونبار شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشفتهٔ حسن بد بلایم کردی</p></div>
<div class="m2"><p>یارب به بلای بد گرفتار شوی</p></div></div>