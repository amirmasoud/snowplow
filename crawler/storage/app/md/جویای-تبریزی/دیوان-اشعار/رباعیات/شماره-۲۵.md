---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>ای خواجه که بسیار خوری چون تو کم است</p></div>
<div class="m2"><p>بطنت چو دهل ز پای تا سرورم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سیری از گرسنگی می نالد</p></div>
<div class="m2"><p>هر آرغ تو نالهٔ درد شکم است</p></div></div>