---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>صحت ز علی مرتضی می خواهم</p></div>
<div class="m2"><p>درد خود را ازو دوا می خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر از تو علاج خود نجویم ز کسی</p></div>
<div class="m2"><p>یا شاه نجف از تو شفا می خواهم</p></div></div>