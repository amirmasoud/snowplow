---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>شاهنشاها کف تو بحرین عطاست</p></div>
<div class="m2"><p>تقوای تو زیب سلطنت نام خداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینت بخش صلاح باشد کرمت</p></div>
<div class="m2"><p>در دست تو سبحه چون گهر در دریا است</p></div></div>