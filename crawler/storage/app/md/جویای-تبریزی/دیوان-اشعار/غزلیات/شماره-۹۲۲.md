---
title: >-
    شمارهٔ ۹۲۲
---
# شمارهٔ ۹۲۲

<div class="b" id="bn1"><div class="m1"><p>در چمن با درد عشقت گر دمی منزل کنم</p></div>
<div class="m2"><p>برگ برگ غنچه ها را لخت لخت دل کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست آسان در غمش سامان درد اندوختن</p></div>
<div class="m2"><p>نقد داغ دل به صد خون جگر حاصل کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد معشوق ز خاطر رفته ذوق دیگر است</p></div>
<div class="m2"><p>در تلاشم کز تو خود را لحظه‌ای غافل کنم</p></div></div>