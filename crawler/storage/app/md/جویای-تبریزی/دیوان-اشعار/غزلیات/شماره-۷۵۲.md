---
title: >-
    شمارهٔ ۷۵۲
---
# شمارهٔ ۷۵۲

<div class="b" id="bn1"><div class="m1"><p>به رنگ شمع بگدازد ز سوز سینه ام تیرش</p></div>
<div class="m2"><p>چو موج باده گردد آب خون آلوده شمشیرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبیند در لحد هم کشتهٔ مژگانش آسایش</p></div>
<div class="m2"><p>که باشد هر کف خاکی به پهلو پنجهٔ شیرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به راه انتظار ناوکش خون دل حسرت</p></div>
<div class="m2"><p>چکد چون بخیه های زخم از مژگان نخجیرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان سنگین ز گرد کلفت خاطر بود آهم</p></div>
<div class="m2"><p>که چون آرم به لب از سینه باشد شور زنجیرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهدرو سوی خلوتخانهٔ دل از حیا جویا</p></div>
<div class="m2"><p>خیالم چو کشد بر پرده های دیده تصویرش</p></div></div>