---
title: >-
    شمارهٔ ۴۹۷
---
# شمارهٔ ۴۹۷

<div class="b" id="bn1"><div class="m1"><p>زفریادم نه بیجا کوهسار آهنگ بردارد</p></div>
<div class="m2"><p>زدرد ناله ام افغان دل هر سنگ بردارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ته سنگ گرفتاری نماند از نگین دستش</p></div>
<div class="m2"><p>تواند هر که دل از فکر نام و ننگ بردارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دل گرد کدورت برد اشارتهای ابرویش</p></div>
<div class="m2"><p>مگر این صیقل از آیینهٔ ما زنگ بردارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رنگ پرتوی کز شمع محفل هر طرف افتد</p></div>
<div class="m2"><p>هوا از پهلوی رخسارهٔ او رنگ بردارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر و کار دل دیوانه ام افتاد با طفلی</p></div>
<div class="m2"><p>که هر جا ناله بر می دارد این سنگ بردارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه تنها جذب حسنش می برد از سینه دل جویا</p></div>
<div class="m2"><p>صفا ز آیینه، از می نشئه وز گل بردارد</p></div></div>