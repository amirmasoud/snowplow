---
title: >-
    شمارهٔ ۸۱۹
---
# شمارهٔ ۸۱۹

<div class="b" id="bn1"><div class="m1"><p>کو زبانی که دهم شرح گرفتاری دل</p></div>
<div class="m2"><p>مگر از طرز نگاهم شنوی زاری دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد فرصت که به غمخواری دل خرج کنی</p></div>
<div class="m2"><p>صرف کن خانه خراب از پی غمخواری دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نه بیماری چشمان تو ساریست چرا</p></div>
<div class="m2"><p>شد مرا الفتشان باعث بیماری دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل قوی دارو به نیروی سعادت برسان</p></div>
<div class="m2"><p>بر زمین پشت فلک را به مددکاری دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ازو سور درون دود نیارد بیرون</p></div>
<div class="m2"><p>آه سردم شده سرگرم هواداری دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل عشاق براهت ز بس افتاده به خاک</p></div>
<div class="m2"><p>جاده ها عقد گهر گشته ز بسیاری دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق بسته است کمر حضرت دل را هشدار</p></div>
<div class="m2"><p>تا توانی مده از دست پرستاری دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حالیست طرفه که جویا غمم از پهلوی اوست</p></div>
<div class="m2"><p>گرچه عمرم همه شد در پی غمخواری دل</p></div></div>