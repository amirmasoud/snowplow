---
title: >-
    شمارهٔ ۶۶۵
---
# شمارهٔ ۶۶۵

<div class="b" id="bn1"><div class="m1"><p>آیینه با وقار تو سیماب می شود</p></div>
<div class="m2"><p>با شوخی تو برق رگ خواب می شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جرأت بود مکیدن آن لب که چون نبات</p></div>
<div class="m2"><p>تا در دهن گذاشته ای آب می شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن باده ای که حسن بمینای دل کند</p></div>
<div class="m2"><p>در پرده های دیدهٔ ما ناب می شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرگشتگیان چاه زنخدان یار را</p></div>
<div class="m2"><p>در گریه عضو عضو چو دولاب می شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرگشتگی ز صورت حالم عیان بود</p></div>
<div class="m2"><p>از عکس رویم آینه گرداب می شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جویا دلت بمهر علی روشن است از آن</p></div>
<div class="m2"><p>شبها ز ود آه تو مهتاب می شود</p></div></div>