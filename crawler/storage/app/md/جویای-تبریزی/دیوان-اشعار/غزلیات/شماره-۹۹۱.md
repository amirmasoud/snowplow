---
title: >-
    شمارهٔ ۹۹۱
---
# شمارهٔ ۹۹۱

<div class="b" id="bn1"><div class="m1"><p>ساقیا رحمی بر احوال من افتاده کن</p></div>
<div class="m2"><p>اینکه خونم می کنی در دل به جامم باده کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز زمین را بهره ای نبود ز ابر نوبهار</p></div>
<div class="m2"><p>خاک ره شو خویش را پیوسته فیض آماده کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو بوی گل که دارد در حریم گل وطن</p></div>
<div class="m2"><p>با وجود پایبندی خویش را آزاده کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستگیری پیشهٔ خود کن نیفتی تا ز پا</p></div>
<div class="m2"><p>هر کرابینی به خاک افتاده است استاده کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آرزوها را مده ره در حریم خاطرات</p></div>
<div class="m2"><p>یعنی از نقش تمنا لوح دل را ساده کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا دگر بیرونت از میخانه نتوانند کرد</p></div>
<div class="m2"><p>خویش را جویا به جای خرقه رهن باده کن</p></div></div>