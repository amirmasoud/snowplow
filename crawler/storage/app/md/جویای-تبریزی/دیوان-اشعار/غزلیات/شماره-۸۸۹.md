---
title: >-
    شمارهٔ ۸۸۹
---
# شمارهٔ ۸۸۹

<div class="b" id="bn1"><div class="m1"><p>زحال من چسان آگاه گردد شوخ خودکامم</p></div>
<div class="m2"><p>که قاصد را بلب ماند نی شد ناله پیغامم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان سیل سرشکم کرده طوفان در شب هجران</p></div>
<div class="m2"><p>که موج اشک شد انگشت حیرت بر لب بامم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحسرت بگذرانم بسکه دور از شکرین لعلی</p></div>
<div class="m2"><p>نگین دان چشم پرخونی شود از پهلوی نامم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجسم نازکش ترسم که سنگینی کند جویا</p></div>
<div class="m2"><p>قبا از نکهت گل گر کند در بر گل اندامم</p></div></div>