---
title: >-
    شمارهٔ ۴۶۲
---
# شمارهٔ ۴۶۲

<div class="b" id="bn1"><div class="m1"><p>بگذشت نوجوانی و جسم نزار ماند</p></div>
<div class="m2"><p>گردی درین ره از پی آن شهسوار ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو داد وصل یار و همان دل فسرده ایم</p></div>
<div class="m2"><p>این غنچه ناشگفته درین نوبهار ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افسوس از دلی که ز بیدردی آرمید</p></div>
<div class="m2"><p>آسوده خاطری که زغم بیقرار ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز نشد شکفته دل داغدار ما</p></div>
<div class="m2"><p>این لاله غنچه در چمن روزگار ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جویا شباب رفت و به دل حسرتم گذشت</p></div>
<div class="m2"><p>گل رخت بست ازین چمن و خارخار ماند</p></div></div>