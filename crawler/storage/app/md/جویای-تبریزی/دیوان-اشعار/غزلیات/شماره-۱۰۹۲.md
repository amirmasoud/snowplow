---
title: >-
    شمارهٔ ۱۰۹۲
---
# شمارهٔ ۱۰۹۲

<div class="b" id="bn1"><div class="m1"><p>تا که تو بر خویشتن سوار نباشی</p></div>
<div class="m2"><p>غازی میدان کارزار نباشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو توان داشت چشم مهر و مروت</p></div>
<div class="m2"><p>گر تو ز ابنای روزگار نباشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی رسدت ز آفتاب عشق نصیبی</p></div>
<div class="m2"><p>تا چو مه یک شبه نزار نباشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر تو غم روزگار دست نیابد</p></div>
<div class="m2"><p>تا که تو پابند اعتبار نباشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چربی و نرمی گزین! مباش گرانجان!‏</p></div>
<div class="m2"><p>تا به دل روزگار بار نباشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا تو به دریا نمی دهی دل خود را</p></div>
<div class="m2"><p>هرگز از این ورطه برکنار نباشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک سر کوی یار شو که چو جویا</p></div>
<div class="m2"><p>سرمهٔ بینش شوی غبار نباشی</p></div></div>