---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>بنگر رخ به آتش می تاب داده را</p></div>
<div class="m2"><p>حسن صفا به گوهر سیراب داده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هردم ز آسمان زبون کش رسد شکست</p></div>
<div class="m2"><p>همچون حباب خانه به سیلاب داده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سینه ام ز شوخی مژگان شکسته ای</p></div>
<div class="m2"><p>صد دشنه ای به زهر ستم آب داده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماند ز نازها که به ابرو کند مدام</p></div>
<div class="m2"><p>چشم تو مست پشت به محراب داده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جویا به طرز آن غزل بینش است این</p></div>
<div class="m2"><p>‏«ماند دلم سفینه به گرداب داده را»‏</p></div></div>