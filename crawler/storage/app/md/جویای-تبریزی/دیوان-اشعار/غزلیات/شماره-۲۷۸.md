---
title: >-
    شمارهٔ ۲۷۸
---
# شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>لعلت مرا به کام دل آب حیات ریخت</p></div>
<div class="m2"><p>گویی خدا ز شیرهٔ جان این نبات ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنگامه ساز صد چو زلیخا و یوسف است</p></div>
<div class="m2"><p>ته جرعه ای که حسن تو بر کاینات ریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نام خدا، لبت رگ ابری است درفشان</p></div>
<div class="m2"><p>از بس گهر زلعل تو حسن نکات ریخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هندوی چشم شوخ تو از سرخی خمار</p></div>
<div class="m2"><p>امروز رنگ میکده در سومنات ریخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حسن سیر مایه مرا در سبوی دل</p></div>
<div class="m2"><p>جویا می نگاه به قصد زکات ریخت</p></div></div>