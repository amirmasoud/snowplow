---
title: >-
    شمارهٔ ۷۳۴
---
# شمارهٔ ۷۳۴

<div class="b" id="bn1"><div class="m1"><p>گذشتم از سر عشقت من و خیال دگر</p></div>
<div class="m2"><p>گل دگر چمن دگر و نهال دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس است در شب هجر توام توانایی</p></div>
<div class="m2"><p>همین قدر که زحالی روم بحال دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امیدوار به عفوم، چنانکه می ترسم</p></div>
<div class="m2"><p>مباد بیم گناهم شود و بال دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشست تا به دلم چون نگین بر انگشتر</p></div>
<div class="m2"><p>فزوده جوهر حسن ترا جمال دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آه ما که شد امروز تیره آینه ات</p></div>
<div class="m2"><p>کشیده ایم ز روی تو انفعال دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز قید نفس رهایی بسعی ممکن نیست</p></div>
<div class="m2"><p>ز دام خویش پریدن توان به بال دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شنیدن خبر مرگ همگنان جویا</p></div>
<div class="m2"><p>بس است هر دل زنده گوشمال دگر</p></div></div>