---
title: >-
    شمارهٔ ۵۲۴
---
# شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>آنکه چون جامی خورد آتش به بزمی در زند</p></div>
<div class="m2"><p>آفت دوران شود گر ساغر دیگر زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می تواند گشت از خوان که و مه کامیاب</p></div>
<div class="m2"><p>دست تسلیم آنکه دایم چون مگس بر سر زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می تواند روز و شب تاج سر افلاک بود</p></div>
<div class="m2"><p>هر که استغنا چو مهر و ماه بر افسر زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدت ایام دولت پنج روزش بیش نیست</p></div>
<div class="m2"><p>مدعی جویا چو گل گیرم که طبل زر زند</p></div></div>