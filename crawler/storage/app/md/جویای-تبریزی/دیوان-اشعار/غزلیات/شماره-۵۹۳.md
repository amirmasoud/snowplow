---
title: >-
    شمارهٔ ۵۹۳
---
# شمارهٔ ۵۹۳

<div class="b" id="bn1"><div class="m1"><p>اشکم چون خون زدیده چکان بی تو می رود</p></div>
<div class="m2"><p>آهم به رنگ شعله طپان بی تو می رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر قامتم دو تاست همان سوز دل بجاست</p></div>
<div class="m2"><p>آهم چو ناوکی زکمان بی تو می رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور از تو یک نفس نتوان شد به اختیار</p></div>
<div class="m2"><p>در حیرتم که عمر چسان بی تو می رود</p></div></div>