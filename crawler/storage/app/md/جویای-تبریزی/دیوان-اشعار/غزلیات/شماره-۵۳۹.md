---
title: >-
    شمارهٔ ۵۳۹
---
# شمارهٔ ۵۳۹

<div class="b" id="bn1"><div class="m1"><p>به دام عشق هر آن دل که مبتلا گردد</p></div>
<div class="m2"><p>نواله ایست که در کام اژدها گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسان گرد یتیمی به جبههٔ گوهر</p></div>
<div class="m2"><p>کدورت ار گذرد در دلم صفا گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کوی عشق چو پروانه بر حوالی شمع</p></div>
<div class="m2"><p>بسی هما که به گرد سر گدا گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که در غم اهل و عیال سرگشته است</p></div>
<div class="m2"><p>برای روزی مردم چو آسیا گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس قوی شده بی او ضعیف نالی من</p></div>
<div class="m2"><p>عجب که شور فغانم ز کوه واگردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعار خود کنی ار ترک مدعا جویا</p></div>
<div class="m2"><p>امید هست که کارت به مدعا گردد</p></div></div>