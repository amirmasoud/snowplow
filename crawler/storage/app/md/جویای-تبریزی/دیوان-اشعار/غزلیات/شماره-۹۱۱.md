---
title: >-
    شمارهٔ ۹۱۱
---
# شمارهٔ ۹۱۱

<div class="b" id="bn1"><div class="m1"><p>بسکه تلخی چشانده هجرانم</p></div>
<div class="m2"><p>مزه گردانده شیرهٔ جانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریخت دردت چو غنچهٔ لاله</p></div>
<div class="m2"><p>یک بغل داغ در گریبانم</p></div></div>