---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>فلک به هرزه کمر بسته است جنگ مرا</p></div>
<div class="m2"><p>که نیست شیشه شکستن شعار سنگ مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین که مانده به جا در طلسم بی تابی</p></div>
<div class="m2"><p>شکسته شوخی پرواز بال رنگ مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآه نیم شب عاشق الحذر ای غیر</p></div>
<div class="m2"><p>که آسمان نشود سد ره خدنگ مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سخت جانی من عشق تا چه افسون خواند</p></div>
<div class="m2"><p>که شیشه شیشه نزاکت فزود سنگ مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم تو گرچه شبنم را به بیخودی گذراند</p></div>
<div class="m2"><p>به باد آه سحر داد نام و ننگ مرا</p></div></div>