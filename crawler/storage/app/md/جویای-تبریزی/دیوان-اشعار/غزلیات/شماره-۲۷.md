---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>می نماید در شب تاریک راه دور را</p></div>
<div class="m2"><p>چشمی از شمع است روشن تر عصای کور را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رستمی، تا چون کمان حلقه بر خود غالبی</p></div>
<div class="m2"><p>می کند گردآوری برگشتن از خود زور را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پناه عجز ایمن گشتم از جور سپهر</p></div>
<div class="m2"><p>رهزنی در پی نباشد کاروان مور را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوخی مژگان شد از بیماری چشم تو بیش</p></div>
<div class="m2"><p>اضطراب نبض افزون تر بود رنجور را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق را نازم که چون بر تخت استغنا نشست</p></div>
<div class="m2"><p>کرد کشکول گدایی کاسهٔ فغفور را</p></div></div>