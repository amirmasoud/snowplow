---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>با اشک تا ز دیده به رویم چکیده است</p></div>
<div class="m2"><p>دل عندلیب گلشن رنگ پریده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور از خرام سرو تو ماتم سراست باغ</p></div>
<div class="m2"><p>هر لاله بسملی است که در خون تپیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن بیخودی که محرم بزم وصال اوست</p></div>
<div class="m2"><p>بوی گل است یا نفس آرمیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در طالع کسی که بود داغ مهر او</p></div>
<div class="m2"><p>چون صبح از نخست گریبان دریده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چشم آن که واله نیرنگ رنگ نیست</p></div>
<div class="m2"><p>آز از شفق چو دامن در خون کشیده است</p></div></div>