---
title: >-
    شمارهٔ ۷۷۰
---
# شمارهٔ ۷۷۰

<div class="b" id="bn1"><div class="m1"><p>ز مستی گر رسد دستم به لب‌های نمک سودش</p></div>
<div class="m2"><p>شود یاقوت دست افشار لعل خنده‌آلودش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مروت نیست با طبعم گهی از ناز می‌گوید</p></div>
<div class="m2"><p>چه می‌کردم اگر انصاف هم یارب نمی‌بودش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگاه گرمی امشب آتشی افروخت در دل‌ها</p></div>
<div class="m2"><p>که چشم مهر و مه روشن بود از سرمهٔ دودش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به امید مروت صبر بر بیداد او کردم</p></div>
<div class="m2"><p>ندانستم جفا و جور جویا خواهد افزودش</p></div></div>