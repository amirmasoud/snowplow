---
title: >-
    شمارهٔ ۵۹۹
---
# شمارهٔ ۵۹۹

<div class="b" id="bn1"><div class="m1"><p>تا غنچهٔ دل بلبل گل پیرهنی شد</p></div>
<div class="m2"><p>هر قطرهٔ خون در تن زارم چمنی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان جهان کشتهٔ الماس ستم را</p></div>
<div class="m2"><p>هر رگ به تن از درد تو تار کفنی شد</p></div></div>