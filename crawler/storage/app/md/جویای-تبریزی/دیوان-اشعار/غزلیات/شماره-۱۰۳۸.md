---
title: >-
    شمارهٔ ۱۰۳۸
---
# شمارهٔ ۱۰۳۸

<div class="b" id="bn1"><div class="m1"><p>صورت حالش دگرگون شد ز گل رخساره‌ای</p></div>
<div class="m2"><p>کار دارد با دل مومینم آتشپاره‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>العطش گویان ز هر مژگان زبان بیرون فکند</p></div>
<div class="m2"><p>بسکه گردیده است چشمم تشنهٔ نظاره‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک نگه کافی است چون شمع از برای شش جهت</p></div>
<div class="m2"><p>چشم مستش را که دارد هر طرف آواره‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفتی و از خار خارت در چمن گردیده است</p></div>
<div class="m2"><p>چشم تر هر نرگس و هر گل دل صدپاره‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌برد از خویشتن ما را حنای پنجه‌اش</p></div>
<div class="m2"><p>می‌برد جویا ز دست امشب، حریفان چاره‌ای!‏</p></div></div>