---
title: >-
    شمارهٔ ۶۴۷
---
# شمارهٔ ۶۴۷

<div class="b" id="bn1"><div class="m1"><p>گل به آن روی نکو می ماند</p></div>
<div class="m2"><p>مل به لعل لب او می ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه اگر ماه شب چارده است</p></div>
<div class="m2"><p>بی کم و کاست به او می ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستم و جور بمن می گذرد</p></div>
<div class="m2"><p>به تو ای عربده جو می ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده تر طیب دماغم نکند</p></div>
<div class="m2"><p>سر خشکم بکدو می ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساغر می چو لبالب باشد</p></div>
<div class="m2"><p>به تو ای آینه رو می ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوهر تیغ ستم در کف چرخ</p></div>
<div class="m2"><p>کی به آن پیچش مو می ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفتم از سیر گل و غنچه ز دست</p></div>
<div class="m2"><p>این بجام آن به سبو می ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مژه بر دور دو چشمم جویا</p></div>
<div class="m2"><p>به کنار لب جو می ماند</p></div></div>