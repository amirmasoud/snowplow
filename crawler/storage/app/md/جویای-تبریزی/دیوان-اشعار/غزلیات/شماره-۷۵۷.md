---
title: >-
    شمارهٔ ۷۵۷
---
# شمارهٔ ۷۵۷

<div class="b" id="bn1"><div class="m1"><p>سرشته اند ز فیض هوای صبح تنش</p></div>
<div class="m2"><p>زموج پرتو ما هست تار پیرهنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تپد شهید نگاه تو در لحد تا حشر</p></div>
<div class="m2"><p>کنی ز پردهٔ چشم غزال گر کفنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توان ز حسن کلامش شنید بوی بهار</p></div>
<div class="m2"><p>به رنگ غنچهٔ خوشبو بود لب از سخنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بزم وصل توام برد بیخودی دل تنگ</p></div>
<div class="m2"><p>چو غنچه ای که برد گلفروش از چمنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وظیفه خوان صفات لبت بود جویا</p></div>
<div class="m2"><p>سزد چو غنچه پر از زرکنی اگر دهنش</p></div></div>