---
title: >-
    شمارهٔ ۹۰۴
---
# شمارهٔ ۹۰۴

<div class="b" id="bn1"><div class="m1"><p>تا نهاد آن مست رنگین جلوه پا در کلبه ام</p></div>
<div class="m2"><p>بال طاؤسیست هر موج هوا در کلبه ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بناگوشی ز بس سامان فیض اندوختم</p></div>
<div class="m2"><p>موج مهتاب است فرش بوریا در کلبه ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد رخسار تو زنگ کلفت از دل می برد</p></div>
<div class="m2"><p>خاکروبی می کند موج صفا در کلبه ام</p></div></div>