---
title: >-
    شمارهٔ ۹۲۳
---
# شمارهٔ ۹۲۳

<div class="b" id="bn1"><div class="m1"><p>خاک راه آن سیه مستم به هنگام خرام</p></div>
<div class="m2"><p>تا مگر سرو روانش را به خود مایل کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد آن زلف گرهگیرم چو در دل بگذرد</p></div>
<div class="m2"><p>ناخن اندیشه صرف عقدهٔ مشکل کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خویشتن‌پرور شدن خوش‌تر ز تن پروردن است</p></div>
<div class="m2"><p>تا به کی اوقات خود را صرف آب و گل کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جامهٔ هستی بر اندامم نمازی می‌شود</p></div>
<div class="m2"><p>داغ خواهش را گر از دامان دل زائل کنم</p></div></div>