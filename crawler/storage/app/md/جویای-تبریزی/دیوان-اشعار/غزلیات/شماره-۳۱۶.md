---
title: >-
    شمارهٔ ۳۱۶
---
# شمارهٔ ۳۱۶

<div class="b" id="bn1"><div class="m1"><p>محفل صهباپرستان بی نوای ساز نیست</p></div>
<div class="m2"><p>گرمیی با بزم ما بی شعلهٔ آواز نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگم از حیرانی دیدار برجا مانده است</p></div>
<div class="m2"><p>طائر تصویر را بال و پر پرواز نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینقدر صوت مغنی چون زند ناخن به دل</p></div>
<div class="m2"><p>حسن مستوری اگر در پردهٔ آواز نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خامشی مفتاح قفل بستهٔ دلها بود</p></div>
<div class="m2"><p>تا نبندد لب در فیضی به رویت باز نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رازها را جوهر آیینهٔ دل گفته اند</p></div>
<div class="m2"><p>آنچه بتوان بر زبان آورد آنرا راز نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخت می ترسم مبادا دعوی دیگر کند</p></div>
<div class="m2"><p>سحرکاریهای چشم او کم از اعجاز نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نالهٔ عاشق بقدر حسن معشوقش رساست</p></div>
<div class="m2"><p>بلبلی با ما در این گلراز هم آواز نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می تپد از تنگی میدان دلم در زیر چرخ</p></div>
<div class="m2"><p>هر فضایی در خور پرواز این شهباز نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلعت تحسین بی اندازه را آماده باش</p></div>
<div class="m2"><p>نیست جویا مصرعی اینجا که با آواز نیست</p></div></div>