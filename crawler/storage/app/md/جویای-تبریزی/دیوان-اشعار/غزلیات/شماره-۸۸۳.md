---
title: >-
    شمارهٔ ۸۸۳
---
# شمارهٔ ۸۸۳

<div class="b" id="bn1"><div class="m1"><p>اصلا بمی شکایتی از غم نکرده ایم</p></div>
<div class="m2"><p>هرگز ز زخم شکوه به مرهم نکرده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما در مصاف همدم شمشیر جرأتیم</p></div>
<div class="m2"><p>بازوی عجز پیش کسی خم نکرده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیوسته تلخ کام نشاط است امید ما</p></div>
<div class="m2"><p>ما لب چشمی ز چاشنی غم نکرده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنها ز دل بخون تمنا طپیده ایم</p></div>
<div class="m2"><p>بیگانه را به راز تو محرم نکرده ایم</p></div></div>