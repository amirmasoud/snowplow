---
title: >-
    شمارهٔ ۴۸۲
---
# شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>اگر در گریه خودداری کنم چشمم خطر دارد</p></div>
<div class="m2"><p>زضبط اشک ترسم این جراحت آب بردارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی را لاف حیرانی رسد در بزم دیدارش</p></div>
<div class="m2"><p>که چون گوهر به آب خشک دایم دیده تر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیندیشد زمردن هر که در ذکر خدا باشد</p></div>
<div class="m2"><p>چو بندد رخت هستی از زبان برگ سفر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رنگ بهله از سر پنجه اش کاری نمی آید</p></div>
<div class="m2"><p>ز بی مغزی رعونت پیشه دستی بر کمر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگاه او چه خونریز است از بالای مژگانش</p></div>
<div class="m2"><p>چو ماهی با خود این خنجر هزاران نیشتر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زعجز نالهٔ بلبل دلم را درد می گیرد</p></div>
<div class="m2"><p>کسی چون کام بردارد زمعشوقی که زر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه می بود اینکه چشمش ریخت در پیمانه ام جویا</p></div>
<div class="m2"><p>عجب نبود گرم تا صبح محشر بی خبر دارد</p></div></div>