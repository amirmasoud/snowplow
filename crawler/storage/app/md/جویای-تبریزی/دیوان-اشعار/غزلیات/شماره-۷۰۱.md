---
title: >-
    شمارهٔ ۷۰۱
---
# شمارهٔ ۷۰۱

<div class="b" id="bn1"><div class="m1"><p>سخن سازی چو چشم یار در دنیا نمی باشد</p></div>
<div class="m2"><p>بلایی در جهان مانند آن بالا نمی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حال هر که از خود رفت چون پی می توان بردن</p></div>
<div class="m2"><p>که در راه زخود رفتن نشان پا نمی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببر از خلق تا دایم توانی با خدا بودن</p></div>
<div class="m2"><p>کسی کو خود به تنهایی کند تنها نمی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غیر از دشت وسعت مشربی کان فیضها دارد</p></div>
<div class="m2"><p>چراغی در ته دامان هر صحرا نمی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تمکینت گرانبار ادب شد محفل مستان</p></div>
<div class="m2"><p>چو گل وا شو که در بزم می استغنا نمی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس نادیدهٔ مهر و وفایم خوبرویان را</p></div>
<div class="m2"><p>نمی دانم که می باشد مروت یا نمی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر تاری ز زلفش عالمی دل بستگی دارد</p></div>
<div class="m2"><p>بلی هر سر که بینی خالی از سودا نمی باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب هجر تو چندان گریه بر من زور می آرد</p></div>
<div class="m2"><p>که شور اشک خونینم کم از دریا نمی باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فور است از دورنگی تا به حدی خاطرم جویا</p></div>
<div class="m2"><p>که بتوان گفت در باغم گل رعنا نمی باشد</p></div></div>