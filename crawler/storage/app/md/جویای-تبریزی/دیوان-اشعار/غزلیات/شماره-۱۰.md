---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>کی به گوش او کند جا ناله های زار ما</p></div>
<div class="m2"><p>سرمهٔ آواز شد خون دل افگار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچه سان از شکوه گر لبریز خون دل شویم</p></div>
<div class="m2"><p>گل کند رنگ خموشی از لب اظهار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می رویم افتان و خیزان در ره دردت چو گره</p></div>
<div class="m2"><p>می توان بر حال ما بی پرداز رفتار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتگوی ما اسیران جز صغیر درد نیست</p></div>
<div class="m2"><p>می تراود ناله چون نی از لب گفتار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خیالش کلبه ام جویا تجلی زار شد</p></div>
<div class="m2"><p>حیرت دیدار دارد صورت دیوار ما</p></div></div>