---
title: >-
    شمارهٔ ۸۷۵
---
# شمارهٔ ۸۷۵

<div class="b" id="bn1"><div class="m1"><p>اگر حرفی ز سوز آتش عشقش بیان کردم</p></div>
<div class="m2"><p>به رنگ شمع محفل خویش را صرف زبان کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغانم از گداز آتش غم اشک حسرت شد</p></div>
<div class="m2"><p>بحمدالله که درس ناله را امشب روان کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشیدم بر سر آخر پردهٔ رسوایی عشقش</p></div>
<div class="m2"><p>به جیب پارهٔ خود غنچه سان خود را نهان کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیال مهر رویی بر دلم تابیده است امشب</p></div>
<div class="m2"><p>زمین را از فروغ کوکب اشک آسمان کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو صبح از ساغر خورشید جویا در دم پیری</p></div>
<div class="m2"><p>چراغ خویش روشن از شراب ارغوان کردم</p></div></div>