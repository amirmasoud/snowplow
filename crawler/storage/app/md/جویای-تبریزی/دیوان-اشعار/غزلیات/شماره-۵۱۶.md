---
title: >-
    شمارهٔ ۵۱۶
---
# شمارهٔ ۵۱۶

<div class="b" id="bn1"><div class="m1"><p>بهر دنیا بودنت غمگین زنادانی بود</p></div>
<div class="m2"><p>خط بطلان تو چین بر لوح پیشانی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گداز تن چه اندیشی اگر جان پروری</p></div>
<div class="m2"><p>پاس تن مانند شمع تدشمن جانی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که دانشور بود دانا نداند خویش را</p></div>
<div class="m2"><p>دعوی دانایی مردم ز نادانی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از غرور تو به عاصی تر شوند اهل ریا</p></div>
<div class="m2"><p>دامن زاهد، تر از اشک پشیمانی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا گشاید لب به رنگ غنچه رسوا می شود</p></div>
<div class="m2"><p>بر دل هر کس که جویا زخم پنهانی بود</p></div></div>