---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>گرنه اشک از دیده در هجران یار افتاده است</p></div>
<div class="m2"><p>گوهر است اما زچشم اعتبار افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوختم تا چشم خواهش بر گل رخسار یار</p></div>
<div class="m2"><p>بخیهٔ رسوایی ام بر روی کار افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش پا در اضطراب از شوخی رفتار اوست</p></div>
<div class="m2"><p>یا دل است این کز پی او بی قرار افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکر کز بیداد چشم او چنین افتاده ام</p></div>
<div class="m2"><p>وای بر بی طالعی کز چشم یار افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می نماید تیغ مژگان تو لنگردار تر</p></div>
<div class="m2"><p>باده پیما باش چشمت در خمار افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیشتر بی طاقتم جویا ز یاد آن کمر</p></div>
<div class="m2"><p>اشک حسرت زان میانم بر کنار افتاده است</p></div></div>