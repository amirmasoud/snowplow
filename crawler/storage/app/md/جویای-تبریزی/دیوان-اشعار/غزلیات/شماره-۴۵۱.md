---
title: >-
    شمارهٔ ۴۵۱
---
# شمارهٔ ۴۵۱

<div class="b" id="bn1"><div class="m1"><p>به چشمم بی رخت مینا دل افسرده را ماند</p></div>
<div class="m2"><p>قدح از موج صهبا بزم بر هم خورده را ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهانم همچو بوی غنچه در آغوش دلتنگی</p></div>
<div class="m2"><p>فضای شش جهت یک خاطر آزرده را ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبیند زخم تیغ عشق هرگز روی بهبودی</p></div>
<div class="m2"><p>که از هر بخیه دندان بر جگر افشرده را ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدح از جوش موج باده در چشم سیه مستان</p></div>
<div class="m2"><p>بعینه دیدهٔ مژگان بهم آورده را ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا کاری به زاهد نیست جویا مصرعی گفتم</p></div>
<div class="m2"><p>سویدای دل افسرده خون مرده را ماند</p></div></div>