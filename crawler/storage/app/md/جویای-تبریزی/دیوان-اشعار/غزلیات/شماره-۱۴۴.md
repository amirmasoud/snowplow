---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>مجلس گل گشته رنگین از نوای عندلیب</p></div>
<div class="m2"><p>می‌فزاید جوش گلشن را صدای عندلیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگ گل لخت دل و آب و هوا اشکست و آه</p></div>
<div class="m2"><p>بی‌تکلف تحفه‌ها دارم برای عندلیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما و او را عشق در یک آشیان پرورده است</p></div>
<div class="m2"><p>هیچ کس چون من نباشد آشنای عندلیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نالهٔ من بر سر کوی تو رنگین‌تر بود</p></div>
<div class="m2"><p>در چمن فریادها دارد نوای عندلیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر فشاند دامن حسنش غباری بر چمن</p></div>
<div class="m2"><p>بشکند رنگ از رخ معشوق های عندلیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پُر مکدر گشته‌ای، جویا به گلشن رو دمی!‏</p></div>
<div class="m2"><p>زنگ از دل می‌زداید ناله‌های عندلیب</p></div></div>