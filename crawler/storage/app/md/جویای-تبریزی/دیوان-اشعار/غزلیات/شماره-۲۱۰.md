---
title: >-
    شمارهٔ ۲۱۰
---
# شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>در زنگ ابر هر قدر آیینه هواست</p></div>
<div class="m2"><p>چشم و دل صراحی و پیمانه را صفاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن ز شمع بزم بود اهل دید را</p></div>
<div class="m2"><p>کاخر به چشم می رود آنکس که خودنماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهی به مدعا رسی از مدعا گذر</p></div>
<div class="m2"><p>زشت است مدعای تو گر ترک مدعاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناحق به خاک ریخته ای خون عیش را</p></div>
<div class="m2"><p>قاضی میان ما و تو ای محتسب، خداست!‏</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جویا فریب چشم سخنگوی او مخور</p></div>
<div class="m2"><p>کس از زبان آن مژه نشنیده است راست</p></div></div>