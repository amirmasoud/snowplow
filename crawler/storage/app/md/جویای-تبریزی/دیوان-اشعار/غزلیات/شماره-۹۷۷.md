---
title: >-
    شمارهٔ ۹۷۷
---
# شمارهٔ ۹۷۷

<div class="b" id="bn1"><div class="m1"><p>ساز شد در پردهٔ خاموشی آخر جنگ من</p></div>
<div class="m2"><p>با صدای دل طپیدن شد بلند آهنگ من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی لب لعل تو عکس چهره ام در جام می</p></div>
<div class="m2"><p>سودهٔ الماس ریزد از شکست رنگ من</p></div></div>