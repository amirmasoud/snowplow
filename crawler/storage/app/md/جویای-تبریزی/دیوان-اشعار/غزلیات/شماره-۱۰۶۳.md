---
title: >-
    شمارهٔ ۱۰۶۳
---
# شمارهٔ ۱۰۶۳

<div class="b" id="bn1"><div class="m1"><p>چند دل را به جهان گذران شاد کنی؟</p></div>
<div class="m2"><p>خانه بر رهگذر سیل چه بنیاد کنی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانهٔ دل که در او غیر هوا را ره نیست</p></div>
<div class="m2"><p>به هوس همچو حبابش ز چه آباد کنی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیچ و تاب الم عشق بود جوهر او</p></div>
<div class="m2"><p>گر دلت را همه چون بیضهٔ فولاد کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حلقهٔ دام تفکر شود از قامت او</p></div>
<div class="m2"><p>قاف تا قاف جهان صید پریزاد کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال دل بسکه خراب است ز تعمیر گذشت</p></div>
<div class="m2"><p>به دو پیمانه اش از نو مگر آباد کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بی عشق گرفتار هوس شد جویا</p></div>
<div class="m2"><p>کاش در بند غمش آری و آزاد کنی</p></div></div>