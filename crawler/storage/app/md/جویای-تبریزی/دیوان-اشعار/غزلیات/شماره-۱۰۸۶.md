---
title: >-
    شمارهٔ ۱۰۸۶
---
# شمارهٔ ۱۰۸۶

<div class="b" id="bn1"><div class="m1"><p>به دنیا پشت پا زن تا شه روی زمین باشی</p></div>
<div class="m2"><p>وز آن دل کز جهانش کنده ای صاحب نگین باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قضا را نیست پروایی ز شادی و غم دلها</p></div>
<div class="m2"><p>چرا تا می توان خرسند بود، اندوهگین باشی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در اندام تو صورت بسته از بس معنی خوبی</p></div>
<div class="m2"><p>به هر کس برخوری چون شعر رنگین دلنشین باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی آیی برون از عهدهٔ یک سجدهٔ شکرش</p></div>
<div class="m2"><p>به رنگ ماه نو از پای تا سر گر جبین باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوایی از شراب کهنه نیکوتر نمی باشد</p></div>
<div class="m2"><p>اگر زاهد تو در فکر علاج درد دین باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آسانی شکارانداز صید مدعا گردی</p></div>
<div class="m2"><p>اگر جویا توانی خویشتن را در کمین باشی</p></div></div>