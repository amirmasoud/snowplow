---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>لب چو مینا بگشود انجمنی خندان ساخت</p></div>
<div class="m2"><p>بزم را نام خدا از سخنی خندان ساخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ کس را ندهد خرمی از پهلوی خویش</p></div>
<div class="m2"><p>صد دل غنچه شکست و چمنی خندان ساخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ابنای زمان را خبر از شادی نیست</p></div>
<div class="m2"><p>همچو گل هر که بدیدم دهنی خندان ساخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشت از گریه فرح بخش چمن ابر بهار</p></div>
<div class="m2"><p>باغ را از مژه بر هم زدنی خندان ساخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاف اعجاز رسد پیر مغان را جویا</p></div>
<div class="m2"><p>دل آزردهٔ مانند منی خندان ساخت</p></div></div>