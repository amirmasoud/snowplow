---
title: >-
    شمارهٔ ۸۵۹
---
# شمارهٔ ۸۵۹

<div class="b" id="bn1"><div class="m1"><p>به شهربند تعلق دمی قرار ندارم</p></div>
<div class="m2"><p>سر مصاحبت اهل این دیار ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دماغ دیدن اغیار و جور یار ندارم</p></div>
<div class="m2"><p>منم که با بد و نیک زمانه کار ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مربی ام بود آب و هوای مملکت عشق</p></div>
<div class="m2"><p>چو نخل عشق بجز شعله برگ و بار ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنی گرم سرپایی به دامن تو زنم دست</p></div>
<div class="m2"><p>اگرچه خاک رهم، طبع برد بار ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مراست کنج قناعت هزار شکر خدا را</p></div>
<div class="m2"><p>که چشم لطفی از ابنای روزگار ندارم</p></div></div>