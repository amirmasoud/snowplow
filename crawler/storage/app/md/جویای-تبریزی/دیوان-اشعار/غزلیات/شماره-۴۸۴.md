---
title: >-
    شمارهٔ ۴۸۴
---
# شمارهٔ ۴۸۴

<div class="b" id="bn1"><div class="m1"><p>بی تو اخگر در درونم از جگر پرکاله بود</p></div>
<div class="m2"><p>دل مرا در تشت آتش همچو داغ لاله بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش بر دوش اثر تا خلوت دلها شدم</p></div>
<div class="m2"><p>شب که پروازم سپندآسا به بال ناله بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به گرد خویش می گشتم به جست و جوی یار</p></div>
<div class="m2"><p>از خود آغوشم تهی چون شعلهٔ جواله بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب چو شمع بزم تا در حلقهٔ مستان شدی</p></div>
<div class="m2"><p>دور صهبا ماه رخسار ترا چون هاله بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شب هجران او از بسکه عیشم می گزد</p></div>
<div class="m2"><p>بر لبم هر قطرهٔ می سوزش تبخاله بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب که جویا خاطرم افسرده بود از جور یار</p></div>
<div class="m2"><p>تا به مژگان می رسید از دل سرشکم ژاله بود</p></div></div>