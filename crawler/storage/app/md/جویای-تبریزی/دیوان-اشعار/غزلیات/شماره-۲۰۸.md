---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>دل که بزم عشقبازی را بسامان چیده است</p></div>
<div class="m2"><p>شمع ها در هر طرف از داغ حرمان چیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می تواند باعث احیای عالم شد دلت</p></div>
<div class="m2"><p>گر گل فیض سحر چون مهر تابان چیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنچه هم عرض بساط دردمندی می دهد</p></div>
<div class="m2"><p>ته به ته لخت دل خونین به دکان چیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خار در پیراهنش دست مکافات افکند</p></div>
<div class="m2"><p>گر گل شمعی حریفی زین شبستان چیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شادی بی اختیاری در گره دارد دلش</p></div>
<div class="m2"><p>زین چمن هر کس به رنگ غنچه دامان چیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرخ رو شد عاشق از پهلوی خوناب جگر</p></div>
<div class="m2"><p>مجلس رنگینی از مال یتیمان چیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش اهل درد خون بیگناهی ریخته است</p></div>
<div class="m2"><p>غافلی جویا گلی گر زین گلستان چیده است</p></div></div>