---
title: >-
    شمارهٔ ۱۰۹۵
---
# شمارهٔ ۱۰۹۵

<div class="b" id="bn1"><div class="m1"><p>به ششدر کار خود از شش جهت انداختی رفتی</p></div>
<div class="m2"><p>دلت را مهرهٔ بازیچه کردی باختی رفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی از تعمیر می‌شد چاره احوال خرابت را</p></div>
<div class="m2"><p>به یک پیمانه از نو خویشتن را ساختی رفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زدی در مستی امشب صد دهن تر خنده بر گلشن</p></div>
<div class="m2"><p>به یک دشنام خشکم زان دو لب ننواختی رفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گداز دل چو خس برداشت از جا جسم زارت را</p></div>
<div class="m2"><p>به بحر بیکرانی خویش را انداختی رفتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غبارت بر فلک سوده است سر از یاری صرصر</p></div>
<div class="m2"><p>میان همسران خود سری افراختی رفتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندادی توسنی جولان از آن چون چشم قربانی</p></div>
<div class="m2"><p>نگاه چند از حسرت به هر سو تاختی رفتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شدی همدست با مژگان پی دل بردن جویا</p></div>
<div class="m2"><p>به قصد ما نهانی با نگاهت ساختی رفتی</p></div></div>