---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>بسکه در راه طلب ضعف است دامنگیر ما</p></div>
<div class="m2"><p>می تواند نقش پا شد حلقهٔ زنجیر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما خراب از رنجش بیجای او گردیده ایم</p></div>
<div class="m2"><p>گر بیفشاند غبار از دل، شود تعمیر ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدرت ما پنجهٔ خورشید را تابیده است</p></div>
<div class="m2"><p>برق در کار رم است از هیبت شمشیر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگ رویم را نه تنها قوت پرواز نیست</p></div>
<div class="m2"><p>ناله هم برخاستن نتواند از زنجیر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرگردانی بی سبب آزار جویا می کند</p></div>
<div class="m2"><p>اینقدرها رنجش بیجا چرا تقصیر ما</p></div></div>