---
title: >-
    شمارهٔ ۷۶۵
---
# شمارهٔ ۷۶۵

<div class="b" id="bn1"><div class="m1"><p>به گلزاری که در رفتار آید سرو موزونش</p></div>
<div class="m2"><p>برافرازد پی نظاره قامت بید مجنونش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که نظاره از بس نازکی مژگان بهم سودن</p></div>
<div class="m2"><p>کم از دندان فشردن نیست بر لبهای میگونش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زده بر گوشهٔ دامان محشر تکیهٔ راحت</p></div>
<div class="m2"><p>مباش ایمن شهید عشق را خوابیده گر خونش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر با جعد مشکین تو سنبل همسری جوید</p></div>
<div class="m2"><p>کند مانند بوی گل نسیم از باغ بیرونش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز درد دل اگر جویا نمایم نکته ای انشا</p></div>
<div class="m2"><p>شود خون و چکد از هر شکنج نامه مضمونش</p></div></div>