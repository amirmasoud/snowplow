---
title: >-
    شمارهٔ ۹۰۳
---
# شمارهٔ ۹۰۳

<div class="b" id="bn1"><div class="m1"><p>بی‌خطا از بس رساند زخم بر بالای زخم</p></div>
<div class="m2"><p>تیر او انگشت زنهاری است در لب‌های زخم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکوه‌ای از دستبرد خنجر او کرده است</p></div>
<div class="m2"><p>می‌نهد مهر خموشی بخیه بر لب‌های زخم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیدلان را نیست تاب مرهم تیغ نگاه</p></div>
<div class="m2"><p>جامهٔ گلگون خون زیباست بر بالای زخم</p></div></div>