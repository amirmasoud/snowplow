---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>هر غنچه به گلشن دل مأیوس بهار است</p></div>
<div class="m2"><p>هر برگ که بینی کف افسوس بهار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد سحری بسکه بر او برگ گل افشاند</p></div>
<div class="m2"><p>هر سرو به بستان دم طاووس بهار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگشا رخ و بر باده مده عرض چمن را</p></div>
<div class="m2"><p>پرده به رخت پردهٔ ناموس بهار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگین شده از بسکه ز عکس گل و سوسن</p></div>
<div class="m2"><p>هر موج هوا شهپر طاووس بهار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر غنچه که گل گشته سراپا لب خواهش</p></div>
<div class="m2"><p>جویا مگر آمادهٔ پابوس بهار است</p></div></div>