---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>احمد مرسل که عالم جمله در تسخیر اوست</p></div>
<div class="m2"><p>ممکنات از حلقه های چرخ در زنجیر اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قاضی باز و کبوتر، غازی دلدل سوار</p></div>
<div class="m2"><p>تا قیامت برق در کار رم از شمشیر اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بضعهٔ خیرالبشر کدبانوی دنیا و دین</p></div>
<div class="m2"><p>آنکه نور چشم احمد، شبر و شبیر اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرور دنیا و دین زین العباد و باقرند</p></div>
<div class="m2"><p>آنگهی صادق که عالم روشن از تنویر اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمان جاه نور دیدهٔ موسی علی</p></div>
<div class="m2"><p>آنکه آرام زمین در سایهٔ توقیر اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاج عصمت بر نقی زیبا بود بعد از تقی</p></div>
<div class="m2"><p>آنکه نظم کار عالم بستهٔ تدبیر اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عسکری باشد امام اهل حق بعد از تقی</p></div>
<div class="m2"><p>آنکه نه قوس فلک در قبضهٔ تسخیر اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیر گردون را مرید خود نمی گیرد ز عار</p></div>
<div class="m2"><p>هر که جویا قایم آل محمد پیر اوست</p></div></div>