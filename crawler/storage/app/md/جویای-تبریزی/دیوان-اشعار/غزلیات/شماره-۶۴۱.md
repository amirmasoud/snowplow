---
title: >-
    شمارهٔ ۶۴۱
---
# شمارهٔ ۶۴۱

<div class="b" id="bn1"><div class="m1"><p>آنکه زلفش به دلم دست تطاول پیچد</p></div>
<div class="m2"><p>دود در خلوتش از نکهت سنبل پیچد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پلکها چون بگذارد بهم از مستی خواب</p></div>
<div class="m2"><p>سرمه از نرگس او در ورق گل پیچد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخزن گوهر مقصود شود همچو صدف</p></div>
<div class="m2"><p>پای سعی آنکه به دامان توکل پیچد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغ دارد دل مستغنی ما منعم را</p></div>
<div class="m2"><p>پنجهٔ بی طعمی دست تمول پیچد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفتی از ساحت گلزار و نسیم سحری</p></div>
<div class="m2"><p>بی تو چون شعله به بال و پر بلبل پیچد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گره از کار دل آسان بگشاید جویا</p></div>
<div class="m2"><p>دامن صبر چو بر دست توسل پیچد</p></div></div>