---
title: >-
    شمارهٔ ۸۲۷
---
# شمارهٔ ۸۲۷

<div class="b" id="bn1"><div class="m1"><p>شب غم، بی جمالش، ساغر، اخگر بود در دستم</p></div>
<div class="m2"><p>ز هر موجی قدح بال سمندر بود در دستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر و سامان دلجمعی است بی سامانی دنیا</p></div>
<div class="m2"><p>پریشان بوده ام تا همچو گل زر بود در دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و می بی تو خوردن وانگهی لاف مسلمانی؟</p></div>
<div class="m2"><p>پیاله بی جمالت، کافرم گر بود در دستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشا روزی که گلچین بهار وصل او بودم</p></div>
<div class="m2"><p>هوا گریان و گل خندان و ساغر بود در دستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب نبود اگر چون روز روشن شد شب وصلش</p></div>
<div class="m2"><p>که جام باده جویا مهر انور بود در دستم</p></div></div>