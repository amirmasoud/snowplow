---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>مه من چون دهد عرض صفای پیکر خود را</p></div>
<div class="m2"><p>کشد صبح از خجالت بر سر خود چادر خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن با چشم دل حیران حسن خوبرویانم</p></div>
<div class="m2"><p>که صنعت می نماید خوبی صنعتگر خود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان خبث یاران سر کند چون تیغ بازی را</p></div>
<div class="m2"><p>ز بس می ترسم از بزم چنین گیرم سر خود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بتی دارم که سازد لیلی شب از حجاب او</p></div>
<div class="m2"><p>نهان در حقهٔ مهر از کواکب زیور خود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر گیرند در وجه بهای یک دهن خنده</p></div>
<div class="m2"><p>بهار است و به رنگ غنچه گردآور زر خود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مده در موج خیز غم عنان صبر را از کف</p></div>
<div class="m2"><p>به ساحل می رسد هر کس نبازد لنگر خود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میفکن بر زبان سخت گویان خویش را جویا</p></div>
<div class="m2"><p>چرا عاقل زند بر سنگ خارا گوهر خود را</p></div></div>