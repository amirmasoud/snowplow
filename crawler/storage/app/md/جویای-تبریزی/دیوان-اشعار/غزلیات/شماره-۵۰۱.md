---
title: >-
    شمارهٔ ۵۰۱
---
# شمارهٔ ۵۰۱

<div class="b" id="bn1"><div class="m1"><p>دل مرا در پیری از قهر الهی می‌تپد</p></div>
<div class="m2"><p>تا قدم قلاب شد تن همچو ماهی می‌تپد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق زور آورد و تن خواهی نخواهی می‌تپد</p></div>
<div class="m2"><p>دل ز قلاب محبت همچو ماهی می‌تپد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد حسن شعله‌ناک آتش‌افروز دلت</p></div>
<div class="m2"><p>بی‌تو در چشمم سپندآسا سیاهی می‌تپد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسترن زار بناگوشی به یادم آورد</p></div>
<div class="m2"><p>در برم دل از نسیم صبحگاهی می‌تپد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه از تیغش دم آب دگر را تشنه است</p></div>
<div class="m2"><p>زخم بر اندام من مانند ماهی می‌تپد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دل از شوق کنارم در شب بیداد او</p></div>
<div class="m2"><p>تا نگردیده است اشک از دیده راهی می‌تپد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هوای گرم سیر عشق آسایش مجوی</p></div>
<div class="m2"><p>موج بحر از تشنگی اینجا چو ماهی می‌تپد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در حریم رحمتش غیر از گنه را بار نیست</p></div>
<div class="m2"><p>چون گنهکاران دلم بر بی‌گناهی می‌تپد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین گنه جویا که رفتم از حریم خاطرش</p></div>
<div class="m2"><p>چون دلم دایم زبان عذرخواهی می‌تپد</p></div></div>