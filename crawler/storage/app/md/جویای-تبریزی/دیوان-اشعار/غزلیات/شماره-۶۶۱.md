---
title: >-
    شمارهٔ ۶۶۱
---
# شمارهٔ ۶۶۱

<div class="b" id="bn1"><div class="m1"><p>به قدر دستگه پامال حب مال خواهی شد</p></div>
<div class="m2"><p>اگر دستی نخواهی داشت فارغ بال خواهی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خمار آرزویم بشکن و یک جرعه بر لب نه</p></div>
<div class="m2"><p>چو جام گل زصاف رنگ مالامال خواهی شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر طاووس رنگین جلوهٔ من در خرام آید</p></div>
<div class="m2"><p>تو هم زاهد سراپا چشم از دنبال خواهی شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تکلف بر طرف زین دست و بازو در نظر دارد</p></div>
<div class="m2"><p>که چون دست هما بر مایهٔ اقبال خواهی شد</p></div></div>