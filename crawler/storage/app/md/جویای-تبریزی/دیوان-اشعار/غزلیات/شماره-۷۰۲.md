---
title: >-
    شمارهٔ ۷۰۲
---
# شمارهٔ ۷۰۲

<div class="b" id="bn1"><div class="m1"><p>قصیده ای که درآن مدح مرتضی نبود</p></div>
<div class="m2"><p>چو سبحه ای است که از خاک کربلا نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وجود پاک تو و ذات حضرت نبود</p></div>
<div class="m2"><p>چو لفظ و معنی از یکدگر جدا نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود ز شیر فلک ربتهٔ سگت افزون</p></div>
<div class="m2"><p>گدای درگه تو کم ز پادشا نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کیش اهل محبت کسی که از دل و جان</p></div>
<div class="m2"><p>غلام تو نبود بندهٔ خدا نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که بغض تو در خاطرش گره کرده است</p></div>
<div class="m2"><p>نمی شود ولد حیض یا زنا نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همین بس است بهشتم که روز بازپسین</p></div>
<div class="m2"><p>مرا به درگه غیر تو التجا نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار شکر که از شعر و شاعری جویا</p></div>
<div class="m2"><p>به غیر مدح سراییم مدعا نبود</p></div></div>