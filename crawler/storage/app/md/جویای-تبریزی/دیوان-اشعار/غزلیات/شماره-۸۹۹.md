---
title: >-
    شمارهٔ ۸۹۹
---
# شمارهٔ ۸۹۹

<div class="b" id="bn1"><div class="m1"><p>ننگ منت بر نتابد همت مردانه ام</p></div>
<div class="m2"><p>می رسد از آب خود مانند گوهر دانه ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همتم هرگز نشد خجلت کش احسان کمی</p></div>
<div class="m2"><p>هیچگه از ریزش باران نشد تر دانه ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می رساند رزق ما هر جا بود خود را به ما</p></div>
<div class="m2"><p>همچو مور از خویش بیرون آورد پر دانه ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که از طور تجلی شعله افروز دلم</p></div>
<div class="m2"><p>کی گذارد برق بی زنهار پا بر دانه ام</p></div></div>