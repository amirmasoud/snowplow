---
title: >-
    شمارهٔ ۹۵۹
---
# شمارهٔ ۹۵۹

<div class="b" id="bn1"><div class="m1"><p>ز خویشتن نفسی گر نهی قدم بیرون</p></div>
<div class="m2"><p>همان بود که بتی آری از حرم بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جسم خاکی من خون دل تراوش کرد</p></div>
<div class="m2"><p>بلی سفال دهد تا پر است نم بیرون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کار خود چو نپرداختی دمی ظالم</p></div>
<div class="m2"><p>عبث قدم زدی از عالم عدم بیرون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سرنوشت، رضا ده! که کی بدل گردد</p></div>
<div class="m2"><p>هر آنچه روز نخست آمد از قلم بیرون</p></div></div>