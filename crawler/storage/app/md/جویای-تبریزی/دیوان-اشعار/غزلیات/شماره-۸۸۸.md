---
title: >-
    شمارهٔ ۸۸۸
---
# شمارهٔ ۸۸۸

<div class="b" id="bn1"><div class="m1"><p>بی تو از بس گرد غم بر چهرهٔ دل داشتیم</p></div>
<div class="m2"><p>در کنار اشک حسرت مهرهٔ گل داشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ز خود رفتیم در راه طلب همچون حباب</p></div>
<div class="m2"><p>گام اول پای در دامان منزل داشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد ایامی که چون از کوی جانان می شدیم</p></div>
<div class="m2"><p>چشم بر در، پای در گل، دست بر دل داشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک نفس دل بر ما غافل از دنیا نبود</p></div>
<div class="m2"><p>این فلاطون را عبث در فکر باطل داشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خیال ابرو و رخسار او شب تا سحر</p></div>
<div class="m2"><p>سیر ماه نو به روی بدر کامل داشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سالها در کویش از زاینده رود چشم تر</p></div>
<div class="m2"><p>همچو سرو جویباری پای در گل داشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با لب هر زخم جویا ما شهادت کشتگان</p></div>
<div class="m2"><p>خندهٔ قهقه به ضرب دست قاتل داشتیم</p></div></div>