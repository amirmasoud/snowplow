---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>بی زبانی روز محشر عذرخواه ما بس است</p></div>
<div class="m2"><p>خامشی در شرمساریها گواه ما بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما تنگ ظرفان به یاد باده مستی می کنیم</p></div>
<div class="m2"><p>در بهاران سایهٔ تا کی پناه ما بس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد راه نیستی شو تا به مقصد پی بری</p></div>
<div class="m2"><p>در محبت، رفتن از خود خضر راه ما بس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>احتیاج شاهدی در دعوی عشق تو نیست</p></div>
<div class="m2"><p>مهر داغی بر سر طومار آه ما بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شب هجران او جویا ز یادش می رویم</p></div>
<div class="m2"><p>اینقدر در عشق بازیها گناه ما بس است</p></div></div>