---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>ز وانمود پریشانی ام چو گل عار است</p></div>
<div class="m2"><p>که مشت بسته چو غنچه هزار دینار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مرد کار مجو جز ملایمت در خشم</p></div>
<div class="m2"><p>که چین جوهر ابروی تیغ هموار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال روی تو از بس به دیده صورت بست</p></div>
<div class="m2"><p>به رنگ عکس ز آیینه ها نمودار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به محفل تو سرش در کف نیاز بود</p></div>
<div class="m2"><p>دماغ هر که به رنگ پیا له سرشار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیار باده که فصل شکست توبهٔ ماست</p></div>
<div class="m2"><p>خط سیاه تو ابر بهار رخسار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا به بزم طلب از بلندی همت</p></div>
<div class="m2"><p>چو برگ لاله زبان سر به مهر اظهار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پر است بسکه دل از گرد کلفتم جویا</p></div>
<div class="m2"><p>خیال یار در او همچو نقش دیوار است</p></div></div>