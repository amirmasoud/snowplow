---
title: >-
    شمارهٔ ۸۲۰
---
# شمارهٔ ۸۲۰

<div class="b" id="bn1"><div class="m1"><p>کی غمی از پا و کی پروای از سر داشتم</p></div>
<div class="m2"><p>زان قیامت جلوه در دل شور محشر داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با زبان حال تا حال دلم گوید به یار</p></div>
<div class="m2"><p>نامهٔ صد پارهٔ چون بال کبوتر داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نزاکت ماند رخسار او جای نگاه</p></div>
<div class="m2"><p>چون ز بیم غیر از رویش نظر برداشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زورق تن بر کنار وصل او چون می رسید</p></div>
<div class="m2"><p>کز گرانجانی به بحر عشق لنگر داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا سیه مستیم از صهبای سودای تو بود</p></div>
<div class="m2"><p>من به رنگ لاله جام از کاسهٔ سر داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتاب عشق برق خرمن نخوت بود</p></div>
<div class="m2"><p>پیش پای او نهادم آنچه در سر داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آبرو گردآوری می کرد جویا همتم</p></div>
<div class="m2"><p>پا به دامان قناعت تا چو گوهر داشتم</p></div></div>