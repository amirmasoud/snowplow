---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>بوالهوس را راز عشق او نهفتن رسم نیست</p></div>
<div class="m2"><p>همچنان کز عاشق دل خسته گفتن رسم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک گل افشا کسی از باغ تمکینم نچید</p></div>
<div class="m2"><p>در ریاض راز دار است شکفتن رسم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاقبت چون غنچه های لاله رازم گل کند</p></div>
<div class="m2"><p>پیش ما داغ دل خونین نهفتن رسم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستبرد عم چو بیند انبساط از دل مجوی</p></div>
<div class="m2"><p>غنچه را در دامن گلچین شکتن رسم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه های لاله را هر صبحدم گوید نسیم</p></div>
<div class="m2"><p>با وجود نشئهٔ تریاک خفتن رسم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این به طور آنغزل جویا که طالب گفته است</p></div>
<div class="m2"><p>از رخ فرش چمن گلبرگ رفتن رسم نیست</p></div></div>