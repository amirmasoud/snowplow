---
title: >-
    شمارهٔ ۲۱۹
---
# شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>هر کرا دل بی غبار کینه است</p></div>
<div class="m2"><p>خشت دیوار تنش آیینه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل که پنهان در صفای سینه است</p></div>
<div class="m2"><p>پیچ و تابش جوهر آیینه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بادهٔ جامم گداز دل بود</p></div>
<div class="m2"><p>شیشهٔ بزمم صفای سینه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از زمین در کاسه اش کردیم خاک</p></div>
<div class="m2"><p>چرخ با ما دشمن دیرینه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کلاه معرفت پشمیش نیست</p></div>
<div class="m2"><p>هر که جویا خرقه اش پشمینه است</p></div></div>