---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>بی طلب آورد مستی دوش دلدار مرا</p></div>
<div class="m2"><p>آن گل خودرو چه رنگین کرد گلزار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بهاران بسکه لبریز طراوت شد چمن</p></div>
<div class="m2"><p>نکهت گل موج سیلاب ست دیوار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشته هر برگ گلی بر آتش دل دامنی</p></div>
<div class="m2"><p>در بهاران رونق دیگر بود کار مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داد و بیداد از لبم بی خواست گر خیزد چه دور؟</p></div>
<div class="m2"><p>ظرف، دلتنگی نماید شوق سرشار مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که جویا دورتر باشد به دل نزدیک تر</p></div>
<div class="m2"><p>رتبهٔ یاری بود پیش من اغیار مرا</p></div></div>