---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>خط پشت لب میگون تو عنوان گل است</p></div>
<div class="m2"><p>دهنت نقطهٔ بسم الله دیوان گل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شده از یاد تو هر قطرهٔ خونم چمنی</p></div>
<div class="m2"><p>موج خون جگرم جوش خیابان گل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مزه از شور دلم رفت چو زخمش به شد</p></div>
<div class="m2"><p>نمک نالهٔ بلبل لب خندان گل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهنت غنچهٔ نشکفتهٔ باغ سخن است</p></div>
<div class="m2"><p>خندهٔ زیر لبت چاک گریبان گل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نحسن رنگین تو از بس نمکین افتاده است</p></div>
<div class="m2"><p>چشم جویا ز خیال تو نمکدان گل است</p></div></div>