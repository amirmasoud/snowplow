---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>لایق نبود کینهٔ چرخ اهل صفا را</p></div>
<div class="m2"><p>معذور توان داشتن این بی سر و پا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دولت نتوان یافتن از پهلوی خست</p></div>
<div class="m2"><p>هرگز نکند کس به مگس صید هما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صیقل زده تردستی ابرش ز بس امروز</p></div>
<div class="m2"><p>آیینهٔ گلشن ز صفا کرد هوا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بس نبرد راه به جایی چو شبیه است</p></div>
<div class="m2"><p>فریاد دلم طاعت ارباب ریا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محتاج ملک نیستم امروز که آهم</p></div>
<div class="m2"><p>جویا ز تراکم به فلک برد دعا را</p></div></div>