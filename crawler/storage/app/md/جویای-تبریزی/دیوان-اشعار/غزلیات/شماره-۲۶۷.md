---
title: >-
    شمارهٔ ۲۶۷
---
# شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>زان لب که نوشداروی جانهای خسته است</p></div>
<div class="m2"><p>یک بوسه مومیایی این دلشکسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا جلوه ای ز صحن چمن رخت بسته است</p></div>
<div class="m2"><p>چشمی است داغ لاله که در خون نشسته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادی در این زمانه نباشد جدا زغم</p></div>
<div class="m2"><p>هر غنچه ای که می شکفد دلشکسته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سیر چارباغ چه طرف بست</p></div>
<div class="m2"><p>آن را که غم به سینه مربع نشسته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت از فراق سرو تو موزونی ام زطبع</p></div>
<div class="m2"><p>از خاطرم غزال غزل بی تو جسته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با آنکه از صفای بناگوشت آگه است</p></div>
<div class="m2"><p>چندین گهر برای چه بر خویش بسته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی عقدهٔ دلم بگشاید ز سیر باغ</p></div>
<div class="m2"><p>دستی است گل که حسن تو برچوب بسته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگز نگاه لطف، ز جویا مگیر باز</p></div>
<div class="m2"><p>پیرو خمیده قد و نزار است و خسته است</p></div></div>