---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>بی درد بر کنار مریز آب دیده را</p></div>
<div class="m2"><p>در کار دل کن آن می صاف چکیده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فیض عشق همت والای ما بدوش</p></div>
<div class="m2"><p>بگرفته جامهٔ ز دو عالم بریده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گلشنی که بلبل ما خامشی نو است</p></div>
<div class="m2"><p>باشد شبیه غنچه دهان دریده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین آتشی که در جگر ماست از غمت</p></div>
<div class="m2"><p>بی آب کرده ایم عقیق مکیده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیری که دید قامت رعنای او کشید</p></div>
<div class="m2"><p>بر بام هوش حلقهٔ قد خمیده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا خون نگردد اشک مده ره بدیده اش</p></div>
<div class="m2"><p>جویا مکن به شیشه می نارسیده را</p></div></div>