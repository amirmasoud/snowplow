---
title: >-
    شمارهٔ ۶۷۳
---
# شمارهٔ ۶۷۳

<div class="b" id="bn1"><div class="m1"><p>دایما خون دلم زان زلف و کاکل می‌چکد</p></div>
<div class="m2"><p>آب و رنگ گل نگر کز شاخ سنبل می‌چکد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو رنگین‌جلوه‌ام چون در خرام آید به ناز</p></div>
<div class="m2"><p>خون خجلت از پر طاووس گل‌گل می‌چکد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گلستانی که آب از جوی یکرنگی خورد</p></div>
<div class="m2"><p>خون گل پیوسته از منقار بلبل می‌چکد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشته‌ام در عشق او تریاکی تریاق صبر</p></div>
<div class="m2"><p>بس که زان تیغ مژه زهر تغافل می‌چکد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زینهار از هم‌زبانی فیض کیفیت مجوی</p></div>
<div class="m2"><p>کز لبش صاف تکلم بی‌تامل می‌چکد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرص دارد تشنهٔ صد آرزو جویا مرا</p></div>
<div class="m2"><p>ورنه آب گوهر از دست توکل می‌چکد</p></div></div>