---
title: >-
    شمارهٔ ۴۸۵
---
# شمارهٔ ۴۸۵

<div class="b" id="bn1"><div class="m1"><p>تو آفت دل و جان نزار خواهی شد</p></div>
<div class="m2"><p>تو برق خرمن صبر و قرار خواهی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین که جوش ترقی است آب و رنگ ترا</p></div>
<div class="m2"><p>گل سرسبد روزگار خواهی شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوی دمی که چو ماه تمام خرمن فیض</p></div>
<div class="m2"><p>زیمن دیدهٔ شب زنده دار خواهی شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین که لطف تنت دمبدم فزون گردد</p></div>
<div class="m2"><p>لطیف تر ز شمیم بهار خواهی شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا مدار ز دامان اشک دست امید</p></div>
<div class="m2"><p>که زیب و زینت جیب و کنار خواهی شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مه در آب، مبین خویش را در آیینه!‏</p></div>
<div class="m2"><p>ازین قرار تو هم بیقرار خواهی شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز صاف طینتی امیدوار شو جویا</p></div>
<div class="m2"><p>که همچو آینه منظور یار خواهی شد</p></div></div>