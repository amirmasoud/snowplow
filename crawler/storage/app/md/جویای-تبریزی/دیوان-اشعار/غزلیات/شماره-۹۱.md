---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>غم بود چاره حریف به غم آموخته را</p></div>
<div class="m2"><p>بستم از داغ تو بر زخم جگر سوخته را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پردهٔ شرم تو شد حیرت نظارهٔ ما</p></div>
<div class="m2"><p>کرده ای جامهٔ آن تن نظر دوخته را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خجالت کش رخسار تو نبود گل باغ</p></div>
<div class="m2"><p>از کجا آورد این رنگ برافروخته را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زود چون شمع بری راه به سر منزل وصل</p></div>
<div class="m2"><p>هادی خویش کنی گر نفس سوخته را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نونیاز است دل و چشم تو پرمایل ناز</p></div>
<div class="m2"><p>مکن از دست رها مرغ نوآموخته را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرده ای باز زهم صحبتی پیرمغان</p></div>
<div class="m2"><p>آتش خرمن طاقت رخ افروخته را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که خو کرده به هجران نبود طالب وصل</p></div>
<div class="m2"><p>هست شادی غم دیگر به غم آموخته را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوه را چون پرکاهی برد از جا جویا</p></div>
<div class="m2"><p>سردهم گر ز مژه گریهٔ اندوخته را</p></div></div>