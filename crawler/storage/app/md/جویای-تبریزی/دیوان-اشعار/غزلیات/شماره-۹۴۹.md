---
title: >-
    شمارهٔ ۹۴۹
---
# شمارهٔ ۹۴۹

<div class="b" id="bn1"><div class="m1"><p>بر لب منه پیالهٔ سرشار بیش ازین</p></div>
<div class="m2"><p>دامن مزن بر آتش رخسار بیش ازین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آن نیم که اینقدر جان بخواهمت</p></div>
<div class="m2"><p>می خواهمت بجان تو بسیار بیش ازین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز نهال من که به تمکین برآمده</p></div>
<div class="m2"><p>کسی روح را ندیده گرابنار بیش ازین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در راه سیل تندرو جان ستاده جسم</p></div>
<div class="m2"><p>راحت مجو سایهٔ دیوار بیش ازین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکبار بیش گل نرود جانب دکان</p></div>
<div class="m2"><p>یعنی مرو سراسر بازار بیش ازین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نور دیده گرچه نهانست از نظر</p></div>
<div class="m2"><p>جویا ندیده ایم پدیدار بیش ازین</p></div></div>