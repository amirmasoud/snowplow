---
title: >-
    شمارهٔ ۷۹۲
---
# شمارهٔ ۷۹۲

<div class="b" id="bn1"><div class="m1"><p>گر نیست آفتاب برین در کمین برف</p></div>
<div class="m2"><p>ریزد عرق ز واهمه چون از جبین برف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرش است نور صبح بروی بساط خاک</p></div>
<div class="m2"><p>یا این مکان بفیض رسید از مکین برف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فردا ز تیغ مهر مسخر شود زمین</p></div>
<div class="m2"><p>امروز اگرچه هست بزیر نگین برف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در برف می زنند ز بس دست و پا شدند</p></div>
<div class="m2"><p>هندوستانیان مگس انگبین برف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلهای عیش از بت سرخ و سفید چین</p></div>
<div class="m2"><p>بنگر ز عکس باده رخ آتشین برف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سیر برف بسکه دلم آب می خورد</p></div>
<div class="m2"><p>جویا قسم خورم بسر نازنین برف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باشم چرا به گوشهٔ کشمیر اسیر برف</p></div>
<div class="m2"><p>زین پس گرفته است دل از سردسیر برف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نیم قطره می بچکانند بر لبش</p></div>
<div class="m2"><p>بر روی آفتاب کند حمله شیر برف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد پخته نان روزی ابنای روزگار</p></div>
<div class="m2"><p>چون یافت مایه روی زمین از خمیر برف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حاصل قبول کرد زمیندار کوه و دشت</p></div>
<div class="m2"><p>دست فلک فکند به رویش چو تیر برف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز فیض نوبهار پس از فصل برف نیست</p></div>
<div class="m2"><p>دارد بشارت گل و سنبل بشیر برف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جویا به هر کجا که نشینی وطن مکن</p></div>
<div class="m2"><p>این وعظ مانده است به یادم ز پیر برف</p></div></div>