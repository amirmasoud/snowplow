---
title: >-
    شمارهٔ ۶۷۵
---
# شمارهٔ ۶۷۵

<div class="b" id="bn1"><div class="m1"><p>غمی به خاطرم از بیش و کم نمی آید</p></div>
<div class="m2"><p>به هر دلی که رضاجوست غم نمی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر چه می کنی، امیدوار بخشش باش!‏</p></div>
<div class="m2"><p>که از کریم به غیر از کرم نمی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گسیختن نتواند چو با تو دل پیوست</p></div>
<div class="m2"><p>ز هر که رام تو گردید رم نمی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فغان که غافلی از نکهت کباب دلم</p></div>
<div class="m2"><p>به هر کجا که تویی بوی غم نمی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امید رحم ز چشم تو عین ساده دلیست</p></div>
<div class="m2"><p>کزین سیاه درون جز ستم نمی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجوی جوهر مردی ز خودنما جویا</p></div>
<div class="m2"><p>که هیچ کار ز شیر علم نمی آید</p></div></div>