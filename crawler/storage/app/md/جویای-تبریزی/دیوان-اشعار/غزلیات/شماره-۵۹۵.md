---
title: >-
    شمارهٔ ۵۹۵
---
# شمارهٔ ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>در سینه آنچه این دل مایوس می کند</p></div>
<div class="m2"><p>کی در فرنگ نالهٔ ناقوس می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که سرو قد تو آراست بزم رقص</p></div>
<div class="m2"><p>رنگ پریده مستی طاؤس می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر حکیمانه زند کس چو تو ساغر جویا</p></div>
<div class="m2"><p>می نگوییم که افسرده ادراک زند</p></div></div>