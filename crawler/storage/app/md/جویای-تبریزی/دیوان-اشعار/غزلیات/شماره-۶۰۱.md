---
title: >-
    شمارهٔ ۶۰۱
---
# شمارهٔ ۶۰۱

<div class="b" id="bn1"><div class="m1"><p>باز دل زآتش سودای تو درمی گیرد</p></div>
<div class="m2"><p>سوختن شمع صفت باز ز سر می گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح پیری بصد آیین جوانی باشد</p></div>
<div class="m2"><p>در خزان گلشن ما رنگ دگر می گیرد</p></div></div>