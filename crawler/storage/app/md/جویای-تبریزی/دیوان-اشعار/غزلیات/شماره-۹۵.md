---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>گردد ز سیر صحرا کی حل مشکل ما</p></div>
<div class="m2"><p>شد پردهٔ بیابان قفل در دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خاک مقصد ما چون گرد درد خیزد</p></div>
<div class="m2"><p>باشد ز پردهٔ دل دامان منزل ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز تخم اشکی مژگان ما نیفشاند</p></div>
<div class="m2"><p>اتی همنشین چه پرسی فردا ز حاصل ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیم ز بزم بیرون همچون شرر ز خارا</p></div>
<div class="m2"><p>سنگین ز بار غم شد از بسکه محفل ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبهای وصل جویا از درد هجر نالم</p></div>
<div class="m2"><p>شرم نگه برویش گردید حایل ما</p></div></div>