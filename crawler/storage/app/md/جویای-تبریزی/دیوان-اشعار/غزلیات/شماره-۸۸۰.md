---
title: >-
    شمارهٔ ۸۸۰
---
# شمارهٔ ۸۸۰

<div class="b" id="bn1"><div class="m1"><p>در کدورت خون شدم از خویش شادان آمدم</p></div>
<div class="m2"><p>جسم رفتم در غم او از خود و جان آمدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو آب جو که در گل می نماید خویش را</p></div>
<div class="m2"><p>گرچه گشتم پای تا سر گریه، خندان آمدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تن زار از پی آن یوسف مصر جمال</p></div>
<div class="m2"><p>همچو گرد کاروان افتان و خیزان آمدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی اسیر عشق را بی بهره دارد فیض حسن</p></div>
<div class="m2"><p>گل ستان گشتم ز بس زانرو گلستان آمدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر نمی تابد تن روشن دلان بار لباس</p></div>
<div class="m2"><p>از عدم، جویا! به رنگ شعله عریان آمدم</p></div></div>