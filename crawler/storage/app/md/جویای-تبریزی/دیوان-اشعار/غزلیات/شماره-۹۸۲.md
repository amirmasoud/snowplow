---
title: >-
    شمارهٔ ۹۸۲
---
# شمارهٔ ۹۸۲

<div class="b" id="bn1"><div class="m1"><p>امروز چون توان به مدارا گریستن</p></div>
<div class="m2"><p>باید چو ابر دجله و دریا گریستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ماتم حسین علی گر نریزی اشک</p></div>
<div class="m2"><p>بر حال خویش گریه کن از ناگریستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارم در این غم است چو شمع سر مزار</p></div>
<div class="m2"><p>دوری ز خلق جستن و تنها گریستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دام ماهی از همه تن چشم تر شوی</p></div>
<div class="m2"><p>خواهی به مدعای دل ما گریستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ کباب وار از این غم بر آتشم</p></div>
<div class="m2"><p>کار من است با همه اعضا گریستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امروز اگر ز دیده فشانی سرشک غم</p></div>
<div class="m2"><p>فردا رسد به داد تو جویا، گریستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حضرت تو چشم شفاعت بود مرا</p></div>
<div class="m2"><p>دستم بگیر روز جزا یا گریستن!‏</p></div></div>