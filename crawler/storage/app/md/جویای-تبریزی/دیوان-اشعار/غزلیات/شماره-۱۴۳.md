---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>رشک آیدم به عشق خداداد عندلیب</p></div>
<div class="m2"><p>در ناله نیست هیچ کس استاد عندلیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشکم گلاب گشته ز مژگان فرو چکد</p></div>
<div class="m2"><p>هر گه به گوش دل شنوم داد عندلیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلریزوار از نفسم شعله می جهد</p></div>
<div class="m2"><p>آتش فروز دل شده فریاد عندلیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جویا به غیر یار به پیش کسی منال</p></div>
<div class="m2"><p>جز گوش گل که می شنود داد عندلیب؟</p></div></div>