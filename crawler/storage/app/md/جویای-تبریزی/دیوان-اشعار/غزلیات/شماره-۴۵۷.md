---
title: >-
    شمارهٔ ۴۵۷
---
# شمارهٔ ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>همچو جوهر همه تن دیدهٔ حیران کردند</p></div>
<div class="m2"><p>سخت مستغرقم این آینه رویان کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه چون غنچه به یک جنبش مژگان این قوم</p></div>
<div class="m2"><p>مشت چاک دل ما را به گریبان کردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنچه سان بسکه زخوناب جگر لبریزم</p></div>
<div class="m2"><p>خنده را بر لب ما زخم نمایان کردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذر قافلهٔ اشک به مژگان افتاد</p></div>
<div class="m2"><p>خار را فرش ره آبله پایان کردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ کس غنچه ای از باغ وصال تو نچید</p></div>
<div class="m2"><p>همه چون لاله گل داغ به دامان کردند</p></div></div>