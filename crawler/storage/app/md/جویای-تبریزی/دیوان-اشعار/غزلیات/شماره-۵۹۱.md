---
title: >-
    شمارهٔ ۵۹۱
---
# شمارهٔ ۵۹۱

<div class="b" id="bn1"><div class="m1"><p>رنگ حسن گل رخان هند را چون ریختند</p></div>
<div class="m2"><p>پرتو مهتاب در پرویزن گل بیختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می تواند گردهٔ رنگ بناگوشت شود</p></div>
<div class="m2"><p>با شمیم گل هوای صبح را آمیختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد ازین دشمن مروت دوستان جویا که باز</p></div>
<div class="m2"><p>رشتهٔ پیمان و الفت را ز هم بگسیختند</p></div></div>