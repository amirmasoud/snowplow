---
title: >-
    شمارهٔ ۳۷۳
---
# شمارهٔ ۳۷۳

<div class="b" id="bn1"><div class="m1"><p>دوش آمد و به روز سیاهم نشاند و رفت</p></div>
<div class="m2"><p>با من نبود جز دلی آنهم ستاند و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذشت نوبهار و فزون شد جنون مرا</p></div>
<div class="m2"><p>گل تخم خار خار تو در دل فشاند و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب را چسان به صبح رسانم کجا روم</p></div>
<div class="m2"><p>هر چند گفتمش مرو امشب نماند و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شرم ریزش مژگانم شب فراق</p></div>
<div class="m2"><p>ابر سیه تر آمد و دامن تکاند و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پا بر زمین ممال که بر بود گوی فیض</p></div>
<div class="m2"><p>زین عرصه هر که توسن همت جهاند و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبر آمد و زگریه فرو خوردم فشاند</p></div>
<div class="m2"><p>چندان سرشک کاتش دل را نشاند و رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جویا ببین که صاف نگه را به دیدنی</p></div>
<div class="m2"><p>از پرده های چشم و دل ما چکاند و رفت</p></div></div>