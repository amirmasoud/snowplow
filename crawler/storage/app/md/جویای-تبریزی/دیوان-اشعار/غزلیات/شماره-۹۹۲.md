---
title: >-
    شمارهٔ ۹۹۲
---
# شمارهٔ ۹۹۲

<div class="b" id="bn1"><div class="m1"><p>حیف باشد واله دنیای دون پرور شدن</p></div>
<div class="m2"><p>قطره در حبس ابد مانده است از گوهر شدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر کن بر ناتوانیها که می یابد هلال</p></div>
<div class="m2"><p>منصب حسن کمال از پهلوی لاغر شدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فقر در آئین ارباب توکل کیمیاست</p></div>
<div class="m2"><p>خاک گردیدن بود در مشرب ما زر شدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا توانی خویشتن را کمتر از هر کم شمار</p></div>
<div class="m2"><p>گر بود در خاطرات اندیشهٔ بهتر شدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ستمگر فکر خود می کن که باید عاقبت</p></div>
<div class="m2"><p>اخگر سوزنده را ناچار خاکستر شدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیخنا! آدم شدن گیرم که مقدور تو نیست</p></div>
<div class="m2"><p>پر ضروری هن نکرده است اینقدرها خر شدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سینه کندن، خون دل خوردن، شعار خویش ساز!‏</p></div>
<div class="m2"><p>ای که باشد چون عقیقت ذوق نام آور شدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پای در دامان عزلت کش چو شد مویت سفید</p></div>
<div class="m2"><p>حیف باشد باق خم حلقهٔ هر در شدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زینهار از خون خود خوردن منال ای دل که هست</p></div>
<div class="m2"><p>پیش ما کفران نعمت بدتر از کافر شدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم دارم سرمهٔ چشم ملک سازد مرا</p></div>
<div class="m2"><p>همچو جویا خاک راه آل پیغمبر شدن</p></div></div>