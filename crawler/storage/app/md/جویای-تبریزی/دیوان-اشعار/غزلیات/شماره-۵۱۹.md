---
title: >-
    شمارهٔ ۵۱۹
---
# شمارهٔ ۵۱۹

<div class="b" id="bn1"><div class="m1"><p>بزم عیشم یک نفس بی جلوهٔ خوبان مباد</p></div>
<div class="m2"><p>باده و نقلم به غیر از آن لب و دندان مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ خواری از کلف افتاده بر رخسار ماه</p></div>
<div class="m2"><p>هیچ کس از چون خودی شرمندهٔ احسان مباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پنجهٔ شاهین بود مژگان چو برهم می زند</p></div>
<div class="m2"><p>صعوهٔ دل صید چنگال سیه مستان مباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز دلم را از گل داغ جگرسوز غمت</p></div>
<div class="m2"><p>چاک پیراهن به رنگ لاله تا دامان مباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>استخوان تن چو شیرم از بن هر مو چکید</p></div>
<div class="m2"><p>کس گرفتار فشار پنجهٔ مژگان مباد</p></div></div>