---
title: >-
    شمارهٔ ۷۱۷
---
# شمارهٔ ۷۱۷

<div class="b" id="bn1"><div class="m1"><p>می رباید خط روی لاله گونم بیشتر</p></div>
<div class="m2"><p>در بهاران می شود سوز جنونم بیشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لحظه ای بنشین نمی خواهم شود آتش بلند</p></div>
<div class="m2"><p>می شود از رفتنت سوز درونم بیشتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه می دارم نهان در سینه دود آه را</p></div>
<div class="m2"><p>رنگ داغ لاله می ماند به خونم بیشتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف او در بردن دل هیچ کوتاهی نکرد</p></div>
<div class="m2"><p>می رباید لیک چشم پرفسونم بیشتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقصدم نبود در این صحرا بجز سرگشتگی</p></div>
<div class="m2"><p>می کند آواره جویا رهنمونم بیشتر</p></div></div>