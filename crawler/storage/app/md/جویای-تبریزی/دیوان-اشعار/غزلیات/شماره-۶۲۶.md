---
title: >-
    شمارهٔ ۶۲۶
---
# شمارهٔ ۶۲۶

<div class="b" id="bn1"><div class="m1"><p>زخم دلم چو غنچه فراهم نمی شود</p></div>
<div class="m2"><p>ممنون چرب نرمی مرهم نمی شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از منعمی بخواه که هر چند می دهد</p></div>
<div class="m2"><p>هیچ از خزانهٔ کرمش کم نمی شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد، به حسن خلق، گرفتم فرشته شد</p></div>
<div class="m2"><p>اما هزار حیف که آدم نمی شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با آنکه هست بر دل سنگین بنای او</p></div>
<div class="m2"><p>هرگز اساس عهد تو محکم نمی شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبحی نشد که جانب خورشید عارضت</p></div>
<div class="m2"><p>چشمم روان چو دیدهٔ شبنم نمی شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بتوان عیار مرد گرفت از فروتنی</p></div>
<div class="m2"><p>شمشیر اصیل تا نبود خم نمی شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اعجاز حسن بین که زگلزار عارضش</p></div>
<div class="m2"><p>جویا به چیدن تو گلی کم نمی شود</p></div></div>