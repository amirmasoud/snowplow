---
title: >-
    شمارهٔ ۴۲۰
---
# شمارهٔ ۴۲۰

<div class="b" id="bn1"><div class="m1"><p>چنان ز نکهت زلفش هوا معطر شد</p></div>
<div class="m2"><p>که از نفس رگ جانم فتیله عنبر شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مراد مرد ترقی است در مراتب عشق</p></div>
<div class="m2"><p>اگر نه به شد داغ دل تو بهتر شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بتی که نرگس مستش بلای جان و دل است</p></div>
<div class="m2"><p>کشید سرمه به چشم این بلای دیگر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سر نامهٔ دل تا دلت شود آگاه</p></div>
<div class="m2"><p>پرید رنگ چو از چهره ام کبوتر شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبرد شیر فلک عار باشدش جویا</p></div>
<div class="m2"><p>کسیکه چون تو سگ آستان حیدر شد</p></div></div>