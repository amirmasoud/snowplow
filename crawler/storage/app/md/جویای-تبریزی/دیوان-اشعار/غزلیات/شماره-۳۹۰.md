---
title: >-
    شمارهٔ ۳۹۰
---
# شمارهٔ ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>با تو در پیری دلم جویای الفت گشته است</p></div>
<div class="m2"><p>تا قدم خم گشت قلاب محبت گشته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب و رنگ حسنش از جوش نیاز عاشق است</p></div>
<div class="m2"><p>بر گل آن رو سرشک ما طراوت گشته است</p></div></div>