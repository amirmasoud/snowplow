---
title: >-
    شمارهٔ ۶۵۳
---
# شمارهٔ ۶۵۳

<div class="b" id="bn1"><div class="m1"><p>فصل نوبهاران است ابر در خروش آمد</p></div>
<div class="m2"><p>نغمه سنج شد بلبل، خون گل به جوش آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح روز باران است مفت می گساران است</p></div>
<div class="m2"><p>باده بی محابا زن ار پرده پوش آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچکس نمی ماند بی نصیب از قسمت</p></div>
<div class="m2"><p>صاف روزی گل شد لاله دردنوش آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر وصف رخسارش پیش حسن گفتارش</p></div>
<div class="m2"><p>جمله تن زبان غنچه گل تمام گوش آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دلش ز بس شوخی صد زبان نهان دارد</p></div>
<div class="m2"><p>گر ز شرم در ظاهر غنچه سان خموش آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موسم بهاران است دور دور مستان است</p></div>
<div class="m2"><p>محتسب غزلخوان است شیخ باده نوش آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقد جان به کف جویا می روم سوی گلشن</p></div>
<div class="m2"><p>مژده نوجوانان را باغ گل فروش آمد</p></div></div>