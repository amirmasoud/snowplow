---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>ترسم که خراشد تن نازک بدنم را</p></div>
<div class="m2"><p>از نکهت گل جامه مکن سیم تنم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون موج که برهم خورد از وی کف دریا</p></div>
<div class="m2"><p>در خاک درد دل ز طپیدن کفنم را</p></div></div>