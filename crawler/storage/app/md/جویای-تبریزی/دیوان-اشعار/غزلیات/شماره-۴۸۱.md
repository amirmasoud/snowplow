---
title: >-
    شمارهٔ ۴۸۱
---
# شمارهٔ ۴۸۱

<div class="b" id="bn1"><div class="m1"><p>از باغ رفت و گل خون دایم به جام دارد</p></div>
<div class="m2"><p>هر غنچهٔ پارهٔ دل بی او به کام دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عکس روی پنهان در گرد و کلفت غم</p></div>
<div class="m2"><p>آیینه ام ز جوهر در خاک دام دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر با خودم نبردی گیرم ز روی ناز است</p></div>
<div class="m2"><p>نام مرا نبردن آیا چه نام دارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ریحان زگل دمیدش جویا چرا ننالم</p></div>
<div class="m2"><p>لطفی که خاص من بود امروز عام دارد</p></div></div>