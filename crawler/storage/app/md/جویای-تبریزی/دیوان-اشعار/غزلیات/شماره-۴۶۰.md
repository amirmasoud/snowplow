---
title: >-
    شمارهٔ ۴۶۰
---
# شمارهٔ ۴۶۰

<div class="b" id="bn1"><div class="m1"><p>گر نباشد بزم حیرانی نظر نتوان گشود</p></div>
<div class="m2"><p>نیت گر بهر طپیدن بال و پر نتوان گشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیف باشد در تلاش برتری بر چون خودی</p></div>
<div class="m2"><p>یک نفس چون شیشهٔ ساعت کمر نتوان گشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نقاب شرم پرورده است از بس عصمتش</p></div>
<div class="m2"><p>ای مصور چهرهٔ آن سیمبر نتوان گشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل به غیر از هایهای گریه هرگز نشکفد</p></div>
<div class="m2"><p>این گره جز پنجهٔ مژگان تر نتوان گشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی شود دلبستگی ارباب دولت را علاج</p></div>
<div class="m2"><p>آری آری عقده از کار گهر نتوان گشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب و رنگ آن گل رو سایه پرورد حیاست</p></div>
<div class="m2"><p>بر رخش گستاخ آغوش نظر نتوان گشود</p></div></div>