---
title: >-
    شمارهٔ ۱۰۱۱
---
# شمارهٔ ۱۰۱۱

<div class="b" id="bn1"><div class="m1"><p>در سحرخیزی به ارباب سعادت یار شو</p></div>
<div class="m2"><p>آسمان گردآور فیض است هان بیدار شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو شبنم در هوای دوست شب را زنده دار</p></div>
<div class="m2"><p>صبح شد برخیز و مست جام استغفار شو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کس از فیض صبوحی کامیاب نشئه ای است</p></div>
<div class="m2"><p>صبحدم با چشم گریان محو روی یار شو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانده ای در بند رفعت از سبک مغزی چو ابر</p></div>
<div class="m2"><p>خاکساری پیشه کن، هم سنگ دریا بار شو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش کس گردن منه مالک رقاب وقت باش</p></div>
<div class="m2"><p>چشم از مردم بپوش و از اولی الابصار شو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برنمی تابد حجابت شوخ چشمیهای شمع</p></div>
<div class="m2"><p>خود چراغ بزم خود چون گوهر شهوار شو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرنه ای جویا نکو، با نیکوان منسوب باش!‏</p></div>
<div class="m2"><p>باغ را گر گل نه ای خار سر دیوار شو</p></div></div>