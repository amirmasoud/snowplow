---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>بهر روزی دلم غمین نیست</p></div>
<div class="m2"><p>گو کمتر باش بیش ازین نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو پادشهی که در پی نام</p></div>
<div class="m2"><p>دستش ته سنگ از نگین نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد از اشکی که نیست خونین</p></div>
<div class="m2"><p>آه از آهی که آتشین نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرزندهٔ عشق همچو شمعم</p></div>
<div class="m2"><p>داغم پنهان در آستین نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد همه جا چو آینه روی</p></div>
<div class="m2"><p>بر لوح جبین هر که چین نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر از دل بیقرار جویا</p></div>
<div class="m2"><p>ویرانهٔ پادشه نشین نیست</p></div></div>