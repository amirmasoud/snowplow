---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>باز مستی کرده خون آشام چشمان ترا</p></div>
<div class="m2"><p>در فشار دل ید طولاست مژگان ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیختند از پردهٔ دل گونیا گرد خطت</p></div>
<div class="m2"><p>ریختند از شیرهٔ جان نقل دندان ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زالتهاب سینهٔ سوزانم از بس نرم شد</p></div>
<div class="m2"><p>از دل من فرق نتوان کرد پیکان ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدن خواب پریشان عاشقان را تهمت است</p></div>
<div class="m2"><p>خواب گرد دیده کی گردد پریشان ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نظر بازیت چون شبنم رسد لاف کمال</p></div>
<div class="m2"><p>گر نگاه از جا رباید چشم گریان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که پا بر پای ارباب ندامت می نهی</p></div>
<div class="m2"><p>سربسر اشک ریا تر کرده دامان ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناز می بارد ز دیوار و در جولانگهت</p></div>
<div class="m2"><p>شوخی مژگان بود خار گلستان ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اینقدرها رو مده آئینه را ترسم مباد</p></div>
<div class="m2"><p>پنجهٔ بی طاقتی گیرد گریبان ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این چه دیدار است کاندر دیدهٔ جویای تو</p></div>
<div class="m2"><p>فرق نتوان کرد از گل روی خندان ترا</p></div></div>