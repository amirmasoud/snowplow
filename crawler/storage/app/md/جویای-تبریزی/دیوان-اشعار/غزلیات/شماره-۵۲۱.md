---
title: >-
    شمارهٔ ۵۲۱
---
# شمارهٔ ۵۲۱

<div class="b" id="bn1"><div class="m1"><p>اهل عالم جب به سوز عشق دل افسرده اند</p></div>
<div class="m2"><p>سینه ها گویی که فانوس چراغ مرده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می فریبد هر قدر دور از نظر باشد سراب</p></div>
<div class="m2"><p>گوشه گیران بازی دنیا فزون تر خورده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی مراد خلق بر روی زمین حاصل شود؟</p></div>
<div class="m2"><p>نوجوانان بسکه در خاک آرزوها برده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخت ترسم شاهباز غمزه اش خالی فتد</p></div>
<div class="m2"><p>مرغ دلها طبل وحشت از تپیدن خورده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست جویا دشمنی مانند خارا، شیشه را</p></div>
<div class="m2"><p>دایم از وضع جهان اهل جهان آزرده اند</p></div></div>