---
title: >-
    شمارهٔ ۷۰۰
---
# شمارهٔ ۷۰۰

<div class="b" id="bn1"><div class="m1"><p>اشتیاقم جام می را جان تصور می کند</p></div>
<div class="m2"><p>هر خمی را چشمهٔ حیوان تصور می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در غم عشق آنکه دندان بر جگر افشرده است</p></div>
<div class="m2"><p>خون دل را نعمت الوان تصور می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه باشد مستی اش از ساغر سرجوش غم</p></div>
<div class="m2"><p>چشم گریان را لب خندان تصور می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که کوشد در سرای تن پی تکریم جان</p></div>
<div class="m2"><p>صاحب این خانه را مهمان تصور می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کرا با عالم ارواح باشد نسبتی</p></div>
<div class="m2"><p>تنگنای چشم را زندان تصور می کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که چون مجنون ما در کار از خود رفتن است</p></div>
<div class="m2"><p>دشت را سیل سبک جولان تصور می کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که تن داده است چون جویا بتسلیم و رضا</p></div>
<div class="m2"><p>دردها را نوعی از درمان تصور می کند</p></div></div>