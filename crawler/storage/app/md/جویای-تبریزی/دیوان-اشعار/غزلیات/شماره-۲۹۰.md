---
title: >-
    شمارهٔ ۲۹۰
---
# شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>چرخ را از تو نکو روی تری یاد کجاست؟</p></div>
<div class="m2"><p>آدمی در دو جهان چون تو پریزاد کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچه آسا دلم از ضبط فغان بی تو شکافت</p></div>
<div class="m2"><p>خامشی کشت مرا، شوخی فریاد کجاست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرگ در حسرت دیدار چو جان سیر منست</p></div>
<div class="m2"><p>کاردانی به جوانمردی فرهاد کجاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخت ویران شدهٔ بی غمی ام درد کسی</p></div>
<div class="m2"><p>که دلم را زخرابی کند آباد کجاست؟</p></div></div>