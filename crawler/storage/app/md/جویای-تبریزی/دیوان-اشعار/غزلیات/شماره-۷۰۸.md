---
title: >-
    شمارهٔ ۷۰۸
---
# شمارهٔ ۷۰۸

<div class="b" id="bn1"><div class="m1"><p>اضطرابی دارم از آرام شوخ و شنگ تر</p></div>
<div class="m2"><p>ناله ای از خامشی یک پرده سیر آهنگ تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می شوم هر دم ز آغوشت جدا دل تنگ تر</p></div>
<div class="m2"><p>دربرت خواهم کشیدن آخر از دل تنگ تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدهٔ بد دور امروز از پریرویان تر است</p></div>
<div class="m2"><p>ساده تر رخسار و چشم شوخ پر نیرنگ تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنجهٔ مژدگان او در بردن دلهای سخت</p></div>
<div class="m2"><p>باشد از سرپنجهٔ فولاد زورین چنگ تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ز خود غافل چه عیب دیگران بینی که نیست</p></div>
<div class="m2"><p>از تو کس بی شرم تر، بی عارتر، بی ننگ تر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کسی چشم حمایت باشدم جویا که اوست</p></div>
<div class="m2"><p>مهربان تر، قدردان تر، یارتر، یک رنگ تر</p></div></div>