---
title: >-
    شمارهٔ ۸۴۷
---
# شمارهٔ ۸۴۷

<div class="b" id="bn1"><div class="m1"><p>بسکه با سامان شد از حسن ملیحی دیدنم</p></div>
<div class="m2"><p>بر کباب دل نمک پاشد نگه دزدیدنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه کز غم در شب هجران او فریاد را</p></div>
<div class="m2"><p>نالهٔ زنجیر می سازد به خود پیچیدنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دو عالم کفهٔ میزان سزد قدر مرا</p></div>
<div class="m2"><p>عقل کل از روی دقت خواهد ار سنجیدنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به قدر شوق جانان جسم را سامان دهند</p></div>
<div class="m2"><p>می شکافد نه فلک را چون قفس بالیدنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاطر افسرده ام جویا محیط عالم است</p></div>
<div class="m2"><p>دهر را ماتم سرایی می کند رنجیدنم</p></div></div>