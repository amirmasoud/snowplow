---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>زبسکه کرده مکرر ثنای واجب را</p></div>
<div class="m2"><p>ز هم گسیخته تار نفس کواکب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همین که بیم فتادن نباشد اقبال است</p></div>
<div class="m2"><p>به چشم کم منگر پستی مراتب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین که جای کند تنگ هم به کلبهٔ دل</p></div>
<div class="m2"><p>عجب که راه برآمد بود مطالب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که با تو طریق مناسبت جوید</p></div>
<div class="m2"><p>برون کند زدل اندیشهٔ مناصب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رود زدست هر آن کو به آن میان آویخت</p></div>
<div class="m2"><p>عبث نه پهلو تهی کرده است قالب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلا ز شوخی دنبال چشم یار مپرس</p></div>
<div class="m2"><p>خدا به خیر و صلاح آورد عواقب را</p></div></div>