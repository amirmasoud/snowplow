---
title: >-
    شمارهٔ ۵۶۴
---
# شمارهٔ ۵۶۴

<div class="b" id="bn1"><div class="m1"><p>مفت رندی است که کام از لب ساغر گیرد</p></div>
<div class="m2"><p>شمع سان ز آتش می مغز سرش درگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهنما راهزن سالک تجرید بود</p></div>
<div class="m2"><p>دشمن است آنکه سر دست شناور گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در تمنای سر زلف بهم خوردهٔ او</p></div>
<div class="m2"><p>دست بیتابی دل دامن محشر گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می زخم آمده چون مهر زکهسار برون</p></div>
<div class="m2"><p>ید بیضا بود آن دست که ساغر گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نامهٔ شوق چو سوی تو به پرواز آید</p></div>
<div class="m2"><p>هر نفس تازه کلاغی به کبوتر گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو طفلی که به خواهش بمکد پستان را</p></div>
<div class="m2"><p>زخم ما کام هوس زان سر خنجر گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر از جرأت مستی شب وصلش جویا</p></div>
<div class="m2"><p>گل شب بو به کف از زلف معنبر گیرد</p></div></div>