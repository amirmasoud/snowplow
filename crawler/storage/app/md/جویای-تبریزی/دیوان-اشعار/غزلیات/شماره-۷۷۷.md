---
title: >-
    شمارهٔ ۷۷۷
---
# شمارهٔ ۷۷۷

<div class="b" id="bn1"><div class="m1"><p>از آن چون آب در گل‌ها بود آهسته رفتارش</p></div>
<div class="m2"><p>که می‌ترسد بریزد آب و رنگ از حسن سرشارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشد مجنون ما پای طلب در دامن دشتی</p></div>
<div class="m2"><p>که دارد شوخی مژگان لیلی هر سر خارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریاضت شاهد اعمال هرکس را بیاراید</p></div>
<div class="m2"><p>گداز دل نهد آیینه پیش حسن کردارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درستی جوی در کار دل از فیض شکست دل</p></div>
<div class="m2"><p>شکستن در حقیقت خانهٔ دل راست معمارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حق مگذر بریدن سخت دشوار است ای ناصح</p></div>
<div class="m2"><p>ز کافر کیش شوخی کز رگ جان است زنارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبیند از شکست خانهٔ تن هیچکس نقصان</p></div>
<div class="m2"><p>که باشد گنج‌ها جویا، نهان در زیر دیوارش</p></div></div>