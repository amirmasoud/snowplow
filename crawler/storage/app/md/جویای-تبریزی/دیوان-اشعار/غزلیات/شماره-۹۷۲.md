---
title: >-
    شمارهٔ ۹۷۲
---
# شمارهٔ ۹۷۲

<div class="b" id="bn1"><div class="m1"><p>تا خزان آمد گل کامی نچیدیم از چمن</p></div>
<div class="m2"><p>غنچه تا نشکفت روی دل ندیدیم از چمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ گل در چشم ما از بو سبک جولانتر است</p></div>
<div class="m2"><p>تا حدیث بی ثباتیها شنیدیم از چمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسخهٔ اوراق گل را بارها گردیده ایم</p></div>
<div class="m2"><p>معنی بی اعتباری را رسیدیم از چمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناز بی اندازه را طاقت ندارد وحشتم</p></div>
<div class="m2"><p>ما ز شور نالهٔ بلبل رمیدیم از چمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گلستان گرچه هر گل را جدا کیفیت است</p></div>
<div class="m2"><p>ما پریشان خاطران سنبل گزیدیم از چمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چهره شد با نونال ما گلشن از سرکشی</p></div>
<div class="m2"><p>ما مگر امروز جویا کم کشیدیم از چمن؟</p></div></div>