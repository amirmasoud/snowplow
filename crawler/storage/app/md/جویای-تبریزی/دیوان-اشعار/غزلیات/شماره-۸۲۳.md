---
title: >-
    شمارهٔ ۸۲۳
---
# شمارهٔ ۸۲۳

<div class="b" id="bn1"><div class="m1"><p>کرده جا تا آن لب میگون به افسون در دلم</p></div>
<div class="m2"><p>غنچه سان شد برگ گل هر قطرهٔ خون در دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می توانی ساقی از جامی روان بخشم شوی</p></div>
<div class="m2"><p>مهربان شو می به ساغر کن، مکن خون در دلم!‏</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیر و دور وحشتم بیرون ز خود یک گام نیست</p></div>
<div class="m2"><p>ریخت عشق از گرد غم تا رنگ هامون در دلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سرم تنها نه همچون شمع بزم آشفتگی است</p></div>
<div class="m2"><p>دور از او هر قطره خون گشته مجنون در دلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرصت یک ابروار اشکست چشم از حدتم</p></div>
<div class="m2"><p>جوش طوفان می زند امروز جیحون در دلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دود آهی بیش در چشمش نبودی آسمان</p></div>
<div class="m2"><p>می نشستی گر بجای خم فلاطون در دلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر نفس در سینه ما را سرو آهی می شود</p></div>
<div class="m2"><p>بسکه جویا کرده جا آن قد موزون در دلم</p></div></div>