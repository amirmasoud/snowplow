---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>غافل از حق گر باو شد جام می هشیار نیست</p></div>
<div class="m2"><p>باز باشد دیدهٔ نرگس ولی بیدار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ریاضت در گداز دل چو پا قایم کنی</p></div>
<div class="m2"><p>همچو شمعت هیچ باک از کسوت زرتار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکوهٔ دنیا، دلیل حب دنیای تو بس</p></div>
<div class="m2"><p>با بد و نیک جهان جز طالبش را کار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تندخو سرکش شود ز آمیزش اهل نفاق</p></div>
<div class="m2"><p>هیچ مهمیزی سمند شعله را چون خار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غافلان در زندگی بینند دنیا را به خواب</p></div>
<div class="m2"><p>هر که پوشیده است چشم از کار حق بیدار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صاف باطن را نباشد بی اثر آه سحر</p></div>
<div class="m2"><p>کج رود تیر تفنگ گر از درون هموار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست و پا در کار دنیا هر که در باطن زند</p></div>
<div class="m2"><p>هیچ کم جویا در ارباب ریا از مار نیست</p></div></div>