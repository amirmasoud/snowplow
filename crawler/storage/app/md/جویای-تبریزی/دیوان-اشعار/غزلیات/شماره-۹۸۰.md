---
title: >-
    شمارهٔ ۹۸۰
---
# شمارهٔ ۹۸۰

<div class="b" id="bn1"><div class="m1"><p>غیر را امشب کباب از اختلاط یار کن</p></div>
<div class="m2"><p>باده بستان از کفش در کاسهٔ اغیار کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد و رفت نفس پیوسته در سوهانگری است</p></div>
<div class="m2"><p>چند بی اندامی ای دل، خویش را هموار کمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام طاقت را که در هجر تو پرخون دل است</p></div>
<div class="m2"><p>از شراب رنگ و بو چون گل بیا سرشار کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوختی چون در غم او، شکوه کافر نعمتی است</p></div>
<div class="m2"><p>داغهای سینه را مهر لب اظهار کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>العطش زن گشته سرتا پا وجودم ساقیا</p></div>
<div class="m2"><p>این سفال تشنه لب را رشحه ای در کار کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تبسم ریز در پیمانه ام صاف مراد</p></div>
<div class="m2"><p>باده در جام من و خون در دل اغیار کن</p></div></div>