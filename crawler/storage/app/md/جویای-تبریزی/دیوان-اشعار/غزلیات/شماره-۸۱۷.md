---
title: >-
    شمارهٔ ۸۱۷
---
# شمارهٔ ۸۱۷

<div class="b" id="bn1"><div class="m1"><p>آن سبک مغزی که بر تن پروری بنهاد دل</p></div>
<div class="m2"><p>از خریت شد اسیر منجلاب آب و گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خورم هر لحظه زخم تازه ای زان چشم شوخ</p></div>
<div class="m2"><p>بسکه از هر جنبش مژگان زند ناخن به دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اختلاط زاهد افسرده با اهل نشاط</p></div>
<div class="m2"><p>سخت ناچسبان بود چون خنده بر روی خجل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ننگ از افعال زشتت می کند دیو رجیم</p></div>
<div class="m2"><p>وای اگر جویا ز کار خود نباشی منفعل</p></div></div>