---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>هر که او با قامت خم گشته غافل خفته است</p></div>
<div class="m2"><p>بی خبر در سایهٔ دیوار مایل خفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سرپایی زنی با دست بخشش فرصت است</p></div>
<div class="m2"><p>دولت بیدار در دامان سایل خفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهد مست خیالش ز اول شب تا سحر</p></div>
<div class="m2"><p>همچو بوی غنچه ام در خلوت دل خفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیر عالم می کند از فیض بیداری دلش</p></div>
<div class="m2"><p>پای سعی هر که در دامن منزل خفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست غیر از سحرکاری چشم خواب آلود را</p></div>
<div class="m2"><p>کشته جویا خلق عالم را و قاتل خفته است</p></div></div>