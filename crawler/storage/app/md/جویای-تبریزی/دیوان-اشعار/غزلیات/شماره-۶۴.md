---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>غیر رسوایی چه حاصل از بیان ما به ما</p></div>
<div class="m2"><p>سوز دل روشن چو شمع ست از زبان ما به ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع آسا آتش افتد از زبان ما به ما</p></div>
<div class="m2"><p>می توان پی برد از طور بیان ما به ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همدهی در بزم یکرنگی نباشد چون کتاب</p></div>
<div class="m2"><p>داستانها می سراید از زبان ما به ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در قناعت دل صفا اندوز کی گردد چو شمع</p></div>
<div class="m2"><p>تا غذای تن نگردد استخوان ما به ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قامت خم گشته بار خاطر پیران بود</p></div>
<div class="m2"><p>شد زیاد از ضعف تن زور کمال ما به ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر قدر کاهد تنم لبریز جانان می شود</p></div>
<div class="m2"><p>سود می بخشد هلال آسا زیان ما به ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خویش را در راه عشق از ضعف تن گم کرده ایم</p></div>
<div class="m2"><p>نالهٔ دل می دهد جویا نشان ما به ما</p></div></div>