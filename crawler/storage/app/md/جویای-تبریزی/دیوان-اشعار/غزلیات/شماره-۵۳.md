---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>زدل بیرون می برد یاد ایام شبابم را</p></div>
<div class="m2"><p>عمارت می کند پیمانه ای حال خرابم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبان رمز می فهمی مشو غافل زمکتوبم</p></div>
<div class="m2"><p>به خود پیچیدن او می نماید پیچ و تابم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یک لبخند قانع نیست دل، ساقی سرت گردم</p></div>
<div class="m2"><p>تبسم بیشتر کن، شورتر گردان کبابم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب هجران دلم را می گزد پیمانه پیمایی</p></div>
<div class="m2"><p>زبان مار سازد دوریت موج شرابم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم افلاک را از هر طپش در لرزه اندازد</p></div>
<div class="m2"><p>به چشم کم نشاید دید جویا اضطرابم را</p></div></div>