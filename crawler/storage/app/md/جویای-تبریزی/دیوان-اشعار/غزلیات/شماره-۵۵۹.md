---
title: >-
    شمارهٔ ۵۵۹
---
# شمارهٔ ۵۵۹

<div class="b" id="bn1"><div class="m1"><p>چون نگاهی سوی من زان چشم مخمور افکند</p></div>
<div class="m2"><p>دل تپیدن‌ها مرا از بزم او دور افکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه در دل ناله را دزدیده ام از شرم غیر</p></div>
<div class="m2"><p>قطرهٔ اشکم به دریا گر فتد شور افکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دشت وسعت مشربی رنگ جهان تازه ای</p></div>
<div class="m2"><p>می تواند در فضای دیدهٔ مور افکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شراب جلوه ات یابد چمن گر خرمی</p></div>
<div class="m2"><p>جام لاله داغ را چون درد می دور افکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی از بیداد مژگان تو هر شب تا سحر</p></div>
<div class="m2"><p>بستر راحت دلم بر نیش زنبور افکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه از سر تا بپا جویا نمک دارد چه دور</p></div>
<div class="m2"><p>گر ز هم‌آغوشیش در بحر و کان شور افکند</p></div></div>