---
title: >-
    شمارهٔ ۱۰۴۹
---
# شمارهٔ ۱۰۴۹

<div class="b" id="bn1"><div class="m1"><p>نهانی در حجاب زندگانی</p></div>
<div class="m2"><p>برون آی از نقاب زندگانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قید جسم تا هستی گرفتار</p></div>
<div class="m2"><p>گل آلوده است آب زندگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوادنامه جز زیر و زبر نیست</p></div>
<div class="m2"><p>گذشتم بر کتاب زندگانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس را آمد و رفت پیاپی</p></div>
<div class="m2"><p>بود موج سراب زندگانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نترسد از خمار صبح محشر</p></div>
<div class="m2"><p>سیه مست شراب زندگانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غفلت رفت ایام حیاتت</p></div>
<div class="m2"><p>بشد عمرت به خواب زندگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر کن آمد و رفت نفس را</p></div>
<div class="m2"><p>ندانی گر شتاب زندگانی</p></div></div>