---
title: >-
    شمارهٔ ۴۱۵
---
# شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>خون جگر زهر بن مو بی تو سر شود</p></div>
<div class="m2"><p>هر موی بر تنم زغمت نیشتر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بویش نشان زگرد رهی می دهد مرا</p></div>
<div class="m2"><p>ترسم ز سیر گل غم دل بیشتر شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در باغ رنگ گل چو دل غنچه بشکند</p></div>
<div class="m2"><p>هرگه به ناز نوگل من جلوه گر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیش شباب را غم پیری بود ز پی</p></div>
<div class="m2"><p>باشد خمار بادهٔ شب چون سحر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سیر باغ بیشتر از خویش می روم</p></div>
<div class="m2"><p>دور از تو رنگ و بوی گلم بال و پر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی دوای درد تو جویا بگو که چیست</p></div>
<div class="m2"><p>وصلت بود علاج میسر اگر شود</p></div></div>