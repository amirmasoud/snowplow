---
title: >-
    شمارهٔ ۱۰۵۴
---
# شمارهٔ ۱۰۵۴

<div class="b" id="bn1"><div class="m1"><p>گشاید بر رخت درهای فیض از یمن هشیاری</p></div>
<div class="m2"><p>کلید قفل چشم بسته نبود غیر بیداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دماغ ناتمام نیم جان می سازد ای ساقی</p></div>
<div class="m2"><p>حباب آسا مرا برپای دارد فیض سرشاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر یکبار گلهای تماشا چیند از رویت</p></div>
<div class="m2"><p>کشد بلبل به گلشن از خیابان خط بیزاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مردم روشن است از موی مژگان تو این معنی</p></div>
<div class="m2"><p>که از بیمار داری نیست افزون ضعف بیماری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک را با تو کاری نیست تا خاک رهی، جویا!‏</p></div>
<div class="m2"><p>نپیچیده است دست زور هرگز پنجهٔ زاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که از سر تا به پا لبریز معنی چون خمی</p></div>
<div class="m2"><p>بر مدار از خویشتن دست طلب در خود گمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوهر معنی است پنهان در تو خود را کم مگیر!‏</p></div>
<div class="m2"><p>خویشتن را قطره می پنداری، اما قلزمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک نفس در دیده کی آرام می گیرد نگاه</p></div>
<div class="m2"><p>هر قدر بگریزی از مردم، عزیز مردمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پای تا سر در پی روزی، دهانی و شکم</p></div>
<div class="m2"><p>تا تو چون دست آس سرگردان مشت گندمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی بود کامل جنون را فکر پاییز و بهار</p></div>
<div class="m2"><p>نارسی، زان در رسیدنها به بند موسمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دور چشم بد زتمکین تو کاندر بزم می</p></div>
<div class="m2"><p>از شراب لعل گون کوه بدخشان چون خمی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کو صفاهان تا نهی جویا به سر تاج مراد</p></div>
<div class="m2"><p>تا به هندی همچو طاووس از پی زیب دمی</p></div></div>