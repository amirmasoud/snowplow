---
title: >-
    شمارهٔ ۲۹۵
---
# شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>همین نه مصرع موزون تراقد دلجوست</p></div>
<div class="m2"><p>که خط پشت لبت حسن مطلع ابروست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب فراق تو خوناب اشک سیلابی است</p></div>
<div class="m2"><p>که کبک را به سر کوهسار تا زانوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جنبش مژه چشمت گشود عقدهٔ دل</p></div>
<div class="m2"><p>برات خرمی ما به شاخ این آهوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و تو چون دو زبان قلم یکی شده ایم</p></div>
<div class="m2"><p>میان ما و تو جویا نگنجد ار یک موست</p></div></div>