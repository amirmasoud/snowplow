---
title: >-
    شمارهٔ ۷۲۳
---
# شمارهٔ ۷۲۳

<div class="b" id="bn1"><div class="m1"><p>پای هر نخلی که بوسیدم به دوران بهار</p></div>
<div class="m2"><p>در چمن گل بر سرم افشاند احسان بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زمین هر قطرهٔ باران برویاند گلی</p></div>
<div class="m2"><p>تخم گویی به خاک افشاند نیسان بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نباشد در چمن حیرت نگه نرگس که نیست</p></div>
<div class="m2"><p>چشم را سیری زنعمتهای الوان بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ببندد غنچه را زنار از رگهای ابر</p></div>
<div class="m2"><p>در فرنگستان گلشن شد کپستان بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم، نرگس، برگ گل، لب، سبزهٔ نوخیز، خط</p></div>
<div class="m2"><p>رخ، بهار و کاکلت، ابر پریشان بهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کف خالی بود دست نگارین دگر</p></div>
<div class="m2"><p>سایه گستر بر زمین شد ابر احسان بهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد نسیم نوبهاران روح در جسم زمین</p></div>
<div class="m2"><p>خاک را جویا روان بخشید باران بهار</p></div></div>