---
title: >-
    شمارهٔ ۹۴۰
---
# شمارهٔ ۹۴۰

<div class="b" id="bn1"><div class="m1"><p>جهان را محتسب، چندی به کام می پرستان کن!‏</p></div>
<div class="m2"><p>ز می خمخانه ها را رشک کهسار بدخشان کن!‏</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز قید بوریا هم درد اگر داری مجرد شو</p></div>
<div class="m2"><p>سموم آه را آتش فروز این نیستان کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرامت را بود کیفیت گردیدن ساغر</p></div>
<div class="m2"><p>زمین و آسمان را از شراب جلوه مستان کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر از بهر ما نبود، برای خود، خودآرا شو!‏</p></div>
<div class="m2"><p>ز عکست عالم آیینه را رشک گلستان کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خوان نعمت دیدار اگر محروم شد جویا</p></div>
<div class="m2"><p>دلش را از خیال حسن پرشور نمکدان کن</p></div></div>