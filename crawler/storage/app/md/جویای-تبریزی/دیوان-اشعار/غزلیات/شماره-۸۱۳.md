---
title: >-
    شمارهٔ ۸۱۳
---
# شمارهٔ ۸۱۳

<div class="b" id="bn1"><div class="m1"><p>می گشاید چشم بستن قفل درهای وصال</p></div>
<div class="m2"><p>پلک ها بر هم بود چسباندهٔ مشق خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد کلفت بسکه بر رخسارم از جوش غم است</p></div>
<div class="m2"><p>روی بر دیوار در آیینه ام دارد مثال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صدمه های دل طپیدن سخت زور آورده است</p></div>
<div class="m2"><p>بر فلک رفت استخوان پهلوی ما چون هلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهراه وصل جانان پیش پا افتاده است</p></div>
<div class="m2"><p>پای مالیدن ستم باشد ستم، چشمی بمال!‏</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم مخور جویا که زود از خاک برگیرد ترا</p></div>
<div class="m2"><p>آنکه سازد پنجهٔ زربخش او گل را نهال</p></div></div>