---
title: >-
    شمارهٔ ۷۷۹
---
# شمارهٔ ۷۷۹

<div class="b" id="bn1"><div class="m1"><p>سبزه پامال شد از نرمی خویش</p></div>
<div class="m2"><p>لاله داغست ز خون گرمی خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزلت آن کس که پی شهره گزید</p></div>
<div class="m2"><p>رفته در پردهٔ بی شرمی خویش</p></div></div>