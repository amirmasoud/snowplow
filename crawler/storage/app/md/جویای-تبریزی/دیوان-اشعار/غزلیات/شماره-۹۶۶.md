---
title: >-
    شمارهٔ ۹۶۶
---
# شمارهٔ ۹۶۶

<div class="b" id="bn1"><div class="m1"><p>خوبرو بسیار از کشمیر می آید برون</p></div>
<div class="m2"><p>گل بسی زین خاک دامنگیر می آید برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عضو عضوم بسکه می پیچد بهم در خاک و خون</p></div>
<div class="m2"><p>از تنم پیکان او زنجیر می آید برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برق عشقت آتش افروز نیستان تنم</p></div>
<div class="m2"><p>ناله ام از دل صدای شیر می آید برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سینه ام نورانی از فیض بناگوشی کسی است</p></div>
<div class="m2"><p>اشک از چشمم به رنگ شیر می آید برون</p></div></div>