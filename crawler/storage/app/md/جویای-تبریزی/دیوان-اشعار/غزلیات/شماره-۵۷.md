---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>گل با سر بازار بسنجد چو چمن را</p></div>
<div class="m2"><p>بازر به ترازو ننهد خاک وطن را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سرو، نسیم سحری برگ گل افشاند</p></div>
<div class="m2"><p>بنگر به گلستان دم طاووس چمن را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای هم نفسان سیر چمن فرع دماغ است</p></div>
<div class="m2"><p>دور از گل آن رو چه کنم سرو و سمن را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو دل که کس اندیشهٔ گلگشت نماید</p></div>
<div class="m2"><p>کو دیده که یک ره بتوان دید چمن را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عریان نشوی همچو گهر در همهٔ عمر</p></div>
<div class="m2"><p>از گرد یتیمی کنی ارجامهٔ تن را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جویا بر هر کس نتوان نکته سرا بود</p></div>
<div class="m2"><p>بر خاک میفکن در نایاب سخن را</p></div></div>