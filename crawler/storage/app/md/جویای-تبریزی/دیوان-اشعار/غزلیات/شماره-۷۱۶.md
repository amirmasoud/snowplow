---
title: >-
    شمارهٔ ۷۱۶
---
# شمارهٔ ۷۱۶

<div class="b" id="bn1"><div class="m1"><p>به بیخودی شد از آن همزبان من تصویر</p></div>
<div class="m2"><p>که هست محرم راز نهان من تصویر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کتاب بی خودی ام، حاشیه است تفسیرم</p></div>
<div class="m2"><p>زبان حیرتم و ترجمان من تصویر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گوش بی خبری از زبان خاموشی</p></div>
<div class="m2"><p>بیان نموده بسی داستان من تصویر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا نکرده اثر در دل آه و نالهٔ من</p></div>
<div class="m2"><p>وگرنه جامه درید از فغان من تصویر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز ماه یکشبه هم خامه را خفی تر کن</p></div>
<div class="m2"><p>کشی گر از مه ابروکمان من تصویر</p></div></div>