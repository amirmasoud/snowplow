---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>ساخت درد تازه محو از خاطرم آزارها</p></div>
<div class="m2"><p>می کند سوزن تهی پارا علاج خارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرنیان شعله می بافم زتار و پود آه</p></div>
<div class="m2"><p>می توان دریافت حالم از قماش کارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد کلفت بسکه آید با سرشک از دل به چشم</p></div>
<div class="m2"><p>پرده های دیده در ما و تو شد دیوارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر قدر طول امل، آزار مردم بیشتر</p></div>
<div class="m2"><p>هست درخورد درازی پیچ و تاب مارها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشتر مضراب چون بر رگ زنی طنبور را</p></div>
<div class="m2"><p>خون به جای نغمه می آید برون از تارها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با بزرگانست جویا نشئهٔ کوچک دلی</p></div>
<div class="m2"><p>هم‌زبان گردند با هر کودکی کهسارها</p></div></div>