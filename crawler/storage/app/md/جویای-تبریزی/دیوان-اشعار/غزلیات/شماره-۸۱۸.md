---
title: >-
    شمارهٔ ۸۱۸
---
# شمارهٔ ۸۱۸

<div class="b" id="bn1"><div class="m1"><p>هرگز نبوده غیر توام آرزوی دل</p></div>
<div class="m2"><p>یارب تهی مباد ازین می سبوی دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز غنچه ای که می شکفد از نسیم صبح</p></div>
<div class="m2"><p>از کس ندیده ایم درین باغ روی دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا خنجر ترا لب زخم دل مکید</p></div>
<div class="m2"><p>آمد مرا ز فیض تو آبی به جوی دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا با خودی ز حضرت دل دور مانده ای</p></div>
<div class="m2"><p>از خود برون خرام پی جستجوی دل</p></div></div>