---
title: >-
    شمارهٔ ۶۹۳
---
# شمارهٔ ۶۹۳

<div class="b" id="bn1"><div class="m1"><p>اشک ما موج شراب دل بود</p></div>
<div class="m2"><p>آه ما دود کباب دل بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خالهای عارض او جا به جا</p></div>
<div class="m2"><p>نقطه های انتخاب دل بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرمه و خال است باب چشم و روی</p></div>
<div class="m2"><p>درد بی اندازه باب دل بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می تواند لوح دل آیینه بود</p></div>
<div class="m2"><p>جوهرش گر اضطراب دل بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه دندان بر جگر افشرده است</p></div>
<div class="m2"><p>در فقیری کامیاب دل بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین غزل دانم که جویای ترا</p></div>
<div class="m2"><p>آشنایی با کتاب دل بود</p></div></div>