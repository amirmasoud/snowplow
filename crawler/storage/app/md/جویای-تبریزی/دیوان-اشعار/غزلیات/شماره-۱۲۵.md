---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>می کنم در سینه پنهان آه درد آلود را</p></div>
<div class="m2"><p>آتش ما در گره چون لاله دارد دود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رونداده حسن شوخش خط مشک اندود را</p></div>
<div class="m2"><p>با وجود آنکه لازم دارد آتش دود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگ برگ غنچه با صد ترزبانی گفته است</p></div>
<div class="m2"><p>واشدن باشد ز پی دلهای خون آلود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می توان در عاشقی دیدن عیار مرد کار</p></div>
<div class="m2"><p>سوختن عیب و هنر ظاهر نماید عود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اختیاری نیست گل را در زر افشاندن به خاک</p></div>
<div class="m2"><p>کی پریشانی تواند گشت مانع جود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تشنهٔ جام می ام ساقی مگر لطفت کند</p></div>
<div class="m2"><p>سایه افکن بر سرم آن اختر مسعود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا علی گو تا ز فیض نام او یابد ز غیب</p></div>
<div class="m2"><p>این غزل جویا خطاب عاقبت محمود را</p></div></div>