---
title: >-
    شمارهٔ ۸۷۷
---
# شمارهٔ ۸۷۷

<div class="b" id="bn1"><div class="m1"><p>ز شوق آنکه به گوشت رسیده آوازم</p></div>
<div class="m2"><p>به بال ناله بود چون سپند پروازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا کباب دل از روی گرم می سوزد</p></div>
<div class="m2"><p>اسیر خوی توام سایه پرور نازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر آفتاب جمال تو در نظر باشد</p></div>
<div class="m2"><p>به روی دل در فیض است دیدهٔ بازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پای خم سو اخلاص بر نمی گیرم</p></div>
<div class="m2"><p>مرا که هست فلاطون عقل دمسازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رموز عشق زمن گل نمی کند جویا</p></div>
<div class="m2"><p>که سر به مهر خموشیست غنچهٔ رازم</p></div></div>