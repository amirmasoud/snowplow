---
title: >-
    شمارهٔ ۳۶۵
---
# شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>به عاشقان نه از امروز بر سر جنگ است</p></div>
<div class="m2"><p>که کین ما به دلش چون شراره در سنگ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتد ز چشم تو بر حلقه های زلف شکست</p></div>
<div class="m2"><p>به سایهٔ خودش این مست بر سر جنگ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفاق و تفرقه زیر سر زبان باشد</p></div>
<div class="m2"><p>نوای بزم خموشی همه یک آهنگ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همیشه جلوهٔ او در لباس بی رنگی است</p></div>
<div class="m2"><p>قبای رنگ به بالای حسن او تنگ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اقربا چه عجب گر پی شکست تواند</p></div>
<div class="m2"><p>که بیشتر خطر آبگینه از سنگ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلید موج شرابم بگفتگو آرد</p></div>
<div class="m2"><p>مرا به بزم تو قفل زبان دل تنگ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شراره ای است که از شوخی و سبک روحی</p></div>
<div class="m2"><p>گه وقار و گرانی به کوه همسنگ است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمیم لخت دل از آه من جهانگیر است</p></div>
<div class="m2"><p>چو بوی گل که بساط هواش اورنگ است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی که ساخته جویا تنش به عریانی</p></div>
<div class="m2"><p>قبای چرخ به اندام همتش تنگ است</p></div></div>