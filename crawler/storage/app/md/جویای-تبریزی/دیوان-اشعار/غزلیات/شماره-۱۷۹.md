---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>راه عشق است اینکه در هر گام صد جا آتش است</p></div>
<div class="m2"><p>گرم رفتاران این ره را ته پا آتش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده ام آیینه دار صورت خوبی کیست؟</p></div>
<div class="m2"><p>چون شرر تار نگاهم را گره با آتش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خرام یاد رخسار تو می سوزد دلم</p></div>
<div class="m2"><p>همچو نخل شعله اندامت سراپا آتش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما سمندر طینتان غواص بحر خود شدیم</p></div>
<div class="m2"><p>عشق گوهر دل صدف گردیده دریا آتش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جای خونم نیست در تن بلکه لبریزم ز عشق</p></div>
<div class="m2"><p>زندگی جویا چو شمع محفلم با آتش است</p></div></div>