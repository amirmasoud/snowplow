---
title: >-
    شمارهٔ ۳۷۷
---
# شمارهٔ ۳۷۷

<div class="b" id="bn1"><div class="m1"><p>بدکاره رحمت از چه سبب چشمداشت داشت</p></div>
<div class="m2"><p>دهقان امید حاصل هر چیز کاشت داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم مگر زباده چو گل بشکفانمش</p></div>
<div class="m2"><p>می خورد و سرگرانی نازی که داشت داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزش مدام عید و شبش قدر بوده است</p></div>
<div class="m2"><p>هر کس نه فکر شام و نه امید چاشت داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین خاکدان کسی که بشد از دل سلیم</p></div>
<div class="m2"><p>نقدی که بهر توشه عقبی گذاشت داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جویا ندید از غم عشق تو فتح باب</p></div>
<div class="m2"><p>امیدها ز هر چه که بر دل گماشت داشت</p></div></div>