---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>سررشتهٔ امل همه ای دل به دست تست</p></div>
<div class="m2"><p>تعمیر قصر عافیت اندر شکست تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسیده چشم آینه رویان روزگار</p></div>
<div class="m2"><p>از صافیی که با قدرانداز شست تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قمری همین نه در گلوی تو است طوق</p></div>
<div class="m2"><p>آزاده سرو هم به چمن پای بست تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خط جام حلقه کند نام باده را</p></div>
<div class="m2"><p>این نشئه ای که در نگه نیم مست تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جویا به خون نشسته گل و لاله در چمن</p></div>
<div class="m2"><p>از رشک رنگ و بوی بت می پرست تست</p></div></div>