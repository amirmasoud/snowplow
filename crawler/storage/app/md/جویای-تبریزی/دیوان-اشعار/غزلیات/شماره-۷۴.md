---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>بس که بگذارد زشرم آن مه جبین آیینه را</p></div>
<div class="m2"><p>همچو آب از دست ریزد بر زمین آیینه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاک طینت قانع است از صاف لذتها به درد</p></div>
<div class="m2"><p>موم باشد خوبتر از انگبین آیینه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کف هر کس که باشد صفحهٔ تصویر اوست</p></div>
<div class="m2"><p>گشته از بس عکس رویش دلنشین آیینه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می نماید عارضش از حلقهٔ زلف سیاه</p></div>
<div class="m2"><p>یا نشانیده است بر انگشترین آیینه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهد ناب آمد به جوش از حلقه های جوهرش</p></div>
<div class="m2"><p>عکس لعلت کرده شان انگبین آیینه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون مصفا شد دل از اندوه دنیا فارغ ست</p></div>
<div class="m2"><p>ساده است از چین غم لوح جبین آیینه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوهرش افتد چو راز از سینهٔ مستان برون</p></div>
<div class="m2"><p>عکس او گر در طپش آرد چنین آیینه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سراپایش بسان چشمه میجوشد عرق</p></div>
<div class="m2"><p>کرده جویا بس که عکسش شرمگین آیینه را</p></div></div>