---
title: >-
    شمارهٔ ۵۷۴
---
# شمارهٔ ۵۷۴

<div class="b" id="bn1"><div class="m1"><p>می‌رود ظلمت چو خورشید از کمین پیدا شود</p></div>
<div class="m2"><p>غم نهان گردد چو آن نازآفرین پیدا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رم گم شد دل از همراهی سیل سرشک</p></div>
<div class="m2"><p>عاقبت در کوچه بند آستین پیدا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنمی را کی بود تاب فروغ آفتاب</p></div>
<div class="m2"><p>محو گردم هر کجا آن مه جبین پیدا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظلمت کلفت برد جویا زدل از یک نگاه</p></div>
<div class="m2"><p>شمع من چون با عذار آتشین پیدا شود</p></div></div>