---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>در خاک مرا ز آتش دل گشت بدن آب</p></div>
<div class="m2"><p>شد هممچو حبابم کفن از پهلوی تن آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هر گل این باغ شمیم جگر آید</p></div>
<div class="m2"><p>گویا که ز ابر مژه ام خورده چمن آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون غنچه بهر قطرهٔ اشکم گل زخمی است</p></div>
<div class="m2"><p>برداشته از چاک جگر دیدهٔ من آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از یاد شکرخند تو گردیده ز شبنم</p></div>
<div class="m2"><p>گرداب صفت غنچهٔ گل را بدهن آب</p></div></div>