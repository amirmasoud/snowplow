---
title: >-
    شمارهٔ ۵۷۱
---
# شمارهٔ ۵۷۱

<div class="b" id="bn1"><div class="m1"><p>ای خضر هر که مست شراب جنون بود</p></div>
<div class="m2"><p>حاشا که در متابعت رهنمون بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور از تو ذره ذرهٔ من دشمن همند</p></div>
<div class="m2"><p>سوهان استخوان تنم موج خون بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام فلک تهی است مدام از می مراد</p></div>
<div class="m2"><p>یارب که تا به حشر چنین سرنگون بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویا حرام باد غم عالم ار خوریم</p></div>
<div class="m2"><p>ما را که در پیاله می لعلگون بود</p></div></div>