---
title: >-
    شمارهٔ ۵۹۴
---
# شمارهٔ ۵۹۴

<div class="b" id="bn1"><div class="m1"><p>دلم را گریه بر مژگان آتش بار می آرد</p></div>
<div class="m2"><p>سرشکم راز پنهان را بروی کار می آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل غمدیده را آخر هجوم گریه کند از جا</p></div>
<div class="m2"><p>چو سیلابی که سنگ از دامن کهسار می آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس لبریز او شد تنگنای خاطرم جویا</p></div>
<div class="m2"><p>نفس از سینه ام بر لب پیام یار می آرد</p></div></div>