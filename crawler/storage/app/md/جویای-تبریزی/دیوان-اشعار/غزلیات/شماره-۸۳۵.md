---
title: >-
    شمارهٔ ۸۳۵
---
# شمارهٔ ۸۳۵

<div class="b" id="bn1"><div class="m1"><p>از غار راه ریزد عشق رنگ خانه ام</p></div>
<div class="m2"><p>همچو نقش پا ندارم بام و در، ویرانه ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه در بزمش زحیرت خشک برجا مانده است</p></div>
<div class="m2"><p>موج صهبا چون رگ سنگ است در پیمانه ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی هر کس بود در خورد استعداد او</p></div>
<div class="m2"><p>می نویسد بر صدف گردون برات دانه ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ز داغ آن گل رو سوختم فانوس وار</p></div>
<div class="m2"><p>شد عبیر پیرهن خاکستر پروانه ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کلبه ام را رتبهٔ دیگر بود از فیض عشق</p></div>
<div class="m2"><p>شیشه بندد بر فلک چینی نمای خانه ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برق هم از خرمنم جویا به استغنا گذشت</p></div>
<div class="m2"><p>کی به چشم مور آید از ضعیفی دانه ام</p></div></div>