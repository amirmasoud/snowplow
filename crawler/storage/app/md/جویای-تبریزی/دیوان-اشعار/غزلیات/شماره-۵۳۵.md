---
title: >-
    شمارهٔ ۵۳۵
---
# شمارهٔ ۵۳۵

<div class="b" id="bn1"><div class="m1"><p>به معشوقی سزاوار است حسنی گو ادا دارد</p></div>
<div class="m2"><p>وگرنه هر گلی کز خاک می‌روید صفا دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکست نفس از فیض کمال نفس می‌باشد</p></div>
<div class="m2"><p>ز غلتانی در یکدانهٔ ما آسیا دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رباید بیشتر دل را چو گردد حسن کامل‌تر</p></div>
<div class="m2"><p>به پیچ و تاب خط رخسار او موج صفا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر مویش دو عالم می‌دهم بیعانه خوش باشد</p></div>
<div class="m2"><p>اگر زلف سیاه او سر سودای ما دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی‌ترسد اگر از تیغ بازی‌های ابرویش</p></div>
<div class="m2"><p>چرا آیینه از جوهر زره زیر قبا دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به صحرایی که در وی خاک گردد کشتهٔ چشمت</p></div>
<div class="m2"><p>ز ریگش روغن بادام اگر گیرند جا دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پی آزار ما با دیگران شد سرگردان امشب</p></div>
<div class="m2"><p>نگاه از جانب هرکس که می‌دزدد به ما دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز درد می خمیر دل بود صهباپرستان را</p></div>
<div class="m2"><p>بلی آیینهٔ ما جوهر از موج صبا دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببین در صنعت نیرنگ سازی دست قدرت را</p></div>
<div class="m2"><p>که نه گوی از فلک پیوسته رقصان در هوا دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمیراند دلی را هرکه دارد مسلک جویا</p></div>
<div class="m2"><p>به کیش دردمندان شمع کشتن خونبها دارد</p></div></div>