---
title: >-
    شمارهٔ ۳۰۰
---
# شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>بیدلی را که گلشن داغ و چمن هامون است</p></div>
<div class="m2"><p>غنچهٔ گلبن امید دل پرخون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بید بی سرو خوش آینده نباشد در باغ</p></div>
<div class="m2"><p>آری آزادگیی لازمهٔ مجنون است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راستان را نبود ف رق زهم در باطن</p></div>
<div class="m2"><p>مصرع قد تو با سرو به یک مضمون است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لخت خون از مژه دیدم که بدامان می ریخت</p></div>
<div class="m2"><p>لیک از دل خبرم نیست که حالش چون ا ست</p></div></div>