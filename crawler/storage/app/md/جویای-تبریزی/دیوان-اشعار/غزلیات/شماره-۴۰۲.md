---
title: >-
    شمارهٔ ۴۰۲
---
# شمارهٔ ۴۰۲

<div class="b" id="bn1"><div class="m1"><p>شب که در صحن چمن آن مست مل خوابیده بود</p></div>
<div class="m2"><p>در ته یک پیرهن با بوی گل خوابیده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که با قد دو تا دست از سیه کاری نداشت</p></div>
<div class="m2"><p>آه از غفلت که بر بالای پل خوابیده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پست بود آوازهٔ گردون ز شور ناله ام</p></div>
<div class="m2"><p>پیش افغانم صدای این دهل خوابیده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست و پای سعیم از بی طالعی ها بسته شد</p></div>
<div class="m2"><p>ورنه عریان در برم آن مست شل خوابیده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محتسب جویا به دور چشم او شب تا سحر</p></div>
<div class="m2"><p>بی تعصب بر بساط صلح کل خوابیده بود</p></div></div>