---
title: >-
    شمارهٔ ۱۰۷۹
---
# شمارهٔ ۱۰۷۹

<div class="b" id="bn1"><div class="m1"><p>به قصد کشتنم افراخت قد خورشید سیمایی</p></div>
<div class="m2"><p>قیامت راست شد برخاست از جا سرو بالایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وفا بیگانه ای بی رحم بی بیباکی دل آزاری</p></div>
<div class="m2"><p>مروت دشمنی بد مست ترکی باده پیمایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر آبی خورم با او می ناب است پنداری</p></div>
<div class="m2"><p>وگر صهبا بنوشم دور ازو آب است پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیاض گردنی را در نظر دارم که از یادش</p></div>
<div class="m2"><p>چو گوهر خلوتم لبریز مهتاب است پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسیمی کز سرکویش سحر در اهتزاز آید</p></div>
<div class="m2"><p>بنای طاقتم را موج سیلاب است پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی کز داغ حرمانش دلم چون لاله درگیرد</p></div>
<div class="m2"><p>به دستم ساغر می جام خوناب است پنداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان در جستجویش صورت سرگشتگی گشتم</p></div>
<div class="m2"><p>که از عکس رخم آیینه گرداب است پنداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به صحرایی که وحشت گشته خضر راه ما جویا</p></div>
<div class="m2"><p>کمند برق بیتابی رگ خواباست پنداری</p></div></div>