---
title: >-
    شمارهٔ ۶۳۵
---
# شمارهٔ ۶۳۵

<div class="b" id="bn1"><div class="m1"><p>آشنا با عشق اگر شد عقده دل واشود</p></div>
<div class="m2"><p>قطره دریا می شود چون واصل دریا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قفل بر مخزن نشان مایه داریهای اوست</p></div>
<div class="m2"><p>راز عشق از بستن لب بیشتر رسوا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قامت و رفتار او را دید چون سرو چمن</p></div>
<div class="m2"><p>شد خرامان تا بلا گردان آن بالا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه دزدیم نگاه شرم ازرخسار یار</p></div>
<div class="m2"><p>چشم دارم دیدهٔ داغ دلم بینا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکته پیرا باش با من بهر پاس عزتم</p></div>
<div class="m2"><p>گر ببندی لب زبان مردمان گویا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو مجنون می تواند داد رسوایی دهد</p></div>
<div class="m2"><p>پردهٔ ناموس هر کس دامن صحرا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناامیدی در غمش سرمایهٔ دیوانگی است</p></div>
<div class="m2"><p>سوخت چون در پردهٔ دل آرزو سودا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون هوای صبحگاهی فیض می بارد ازو</p></div>
<div class="m2"><p>ابر عالمگیر آهم گر فلک پیما شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خویش را آویخت شاخ گل به دامان نسیم</p></div>
<div class="m2"><p>تا به آسانی بلاگردان آن بالا شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من غلام آنکه جویا بیند اسرار دو کون</p></div>
<div class="m2"><p>دیده ای کز سرمهٔ خاک درش بینا شود</p></div></div>