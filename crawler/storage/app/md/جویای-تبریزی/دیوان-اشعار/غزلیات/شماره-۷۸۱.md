---
title: >-
    شمارهٔ ۷۸۱
---
# شمارهٔ ۷۸۱

<div class="b" id="bn1"><div class="m1"><p>یار مست است امشب و من کامرانم از لبش</p></div>
<div class="m2"><p>میمکم چندان که خون خود ستانم از لبش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت بیدار از شکر خوابش مساعد شد مرا</p></div>
<div class="m2"><p>میستانم داد خود تا می توانم از لبش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناز قاصد برنتابد عالم یک رنگیم</p></div>
<div class="m2"><p>منکه همچون نامه با او همزبانم از لبش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنچه اش را می توانم گفتن الحق قوت روح</p></div>
<div class="m2"><p>بی تکلف همچو تن بالیده جانم از لبش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه خوناب نیاز و ناز میجو شد بهم</p></div>
<div class="m2"><p>گل کند مانند نی شور فغانم از لبش</p></div></div>