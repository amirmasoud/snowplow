---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>ساخت هر کس از توکل تکیه گاه خویش را</p></div>
<div class="m2"><p>از خس و خار خطر پرداخت راه خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو داغ لاله ام در دل گره گردیده است</p></div>
<div class="m2"><p>بسکه می دارم نهان در سینه آه خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در قطار بی گناهانت شمردن می توان</p></div>
<div class="m2"><p>گر توانی در شمار آری گناه خویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود در گهواره ام دل مرغ دست آموز عشق</p></div>
<div class="m2"><p>در کنار برق پروردم گیاه خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه لطفت هر سحر از خاک برمیگیردش</p></div>
<div class="m2"><p>مهر چون بر چرخ اندازد کلاه خویش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فیض بینش یافت جویا دیدهٔ داغ دلم</p></div>
<div class="m2"><p>بسکه دزدیدم ز شرم او نگاه خویش را</p></div></div>