---
title: >-
    شمارهٔ ۴۹۶
---
# شمارهٔ ۴۹۶

<div class="b" id="bn1"><div class="m1"><p>جان آزاد گرفتار تن زار بماند</p></div>
<div class="m2"><p>همچو گنجی که نهان در ته دیوار بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پریشان نظری گشته پریشان دلها</p></div>
<div class="m2"><p>دیده آن است که در حسرت دیدار بماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز را مهر به شب گر برساند عجب است</p></div>
<div class="m2"><p>بسکه حیران تو چون صورت دیوار بماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به باغ آمده ای دست و دل سرو و چنار</p></div>
<div class="m2"><p>در تماشای سراپای تو از کار بماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نگه در حرم دیدهٔ حیرت زدگان</p></div>
<div class="m2"><p>راز رسوای تو در پرده اسرار بماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو را شرم قدت سلسله بر پای نهاد</p></div>
<div class="m2"><p>دید تا طور خرام تو ز رفتار بماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو به حال دل خود هیچ نمی پردازی</p></div>
<div class="m2"><p>حیف کاین آینه در پردهٔ زنگار بماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو شبنم بود از بال نگه پروازش</p></div>
<div class="m2"><p>آن سبکروح که حیران رخ یار بماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی زکار دل خود بر بدر آری جویا</p></div>
<div class="m2"><p>چرخ سرگشتهٔ این نقطه چو پرگار بماند</p></div></div>