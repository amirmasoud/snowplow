---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>شد بهار و بیخودیم از نشئهٔ جام هوا</p></div>
<div class="m2"><p>توبهٔ ما را شکست امروز ابرام هوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوهٔ آهم هوا را بسکه رنگین کرده است</p></div>
<div class="m2"><p>صد تذرو اینجا گرفتارند در دام هوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینه چاک از پردهٔ عصمت دم بیرون نهد</p></div>
<div class="m2"><p>هر که همچون صبح می گردد می آشام هوا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می دهیدم می که با صد تر زبانی گفته است</p></div>
<div class="m2"><p>قاصد شبنم به گوش غنچه پیغام هوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زودتر جویا به گلشن رو که علوی گفته است:‏</p></div>
<div class="m2"><p>‏«نالهٔ بلبل به یاران بود پیغام هوا»‏</p></div></div>