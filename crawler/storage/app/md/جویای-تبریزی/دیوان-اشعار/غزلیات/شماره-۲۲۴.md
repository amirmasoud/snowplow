---
title: >-
    شمارهٔ ۲۲۴
---
# شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>می کند چشم تو تا بیخود ز ساغر گشته است</p></div>
<div class="m2"><p>آنقدر مستی که مژگان هم از او برگشته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ره شوق تو از بس پر برون آورده است</p></div>
<div class="m2"><p>نامه ام مستغنی از بال کبوتر گشته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارهای چرخ از بس بی نظام افتاده است</p></div>
<div class="m2"><p>آسمانها گوییا اوراق ابتر گشته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون توانم گرد دل را منع کلفت از غمش</p></div>
<div class="m2"><p>شیشه افلاک از آهم مکدر گشته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چارهٔ سرگشتگی ها را مجوی از آسمان</p></div>
<div class="m2"><p>چون تو جویا او هم از بیچارگی سرگشته است</p></div></div>