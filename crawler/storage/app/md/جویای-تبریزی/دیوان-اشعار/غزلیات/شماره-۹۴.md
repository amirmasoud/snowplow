---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>برد یاد او دل غم پیشه را</p></div>
<div class="m2"><p>جوش می برداشت از جا شیشه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع سان گلهای داغت بر سرم</p></div>
<div class="m2"><p>می دواند تا کف پا ریشه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمش از هر جنبش مژگان شوخ</p></div>
<div class="m2"><p>می زند یکجا به دل صد تیشه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیشتر از اقربا بینی شکست</p></div>
<div class="m2"><p>دشمنی چون سنگ نبود شیشه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تکیه گاه نشتر مژگان او</p></div>
<div class="m2"><p>کرده ام جویا رگ اندیشه را</p></div></div>