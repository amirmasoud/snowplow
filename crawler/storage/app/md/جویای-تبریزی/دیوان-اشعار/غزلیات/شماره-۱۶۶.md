---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>زدل جز عجز و زاری خوشنما نیست</p></div>
<div class="m2"><p>به غیر از بیقراری خوشنما نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو شهبازی به کار خویش می باش</p></div>
<div class="m2"><p>ترحم از شکاری خوشنما نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زما پیش تو هنگام جدایی</p></div>
<div class="m2"><p>بجز دل یادگاری خوشنما نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترحم کن که از شاهان گه خشم</p></div>
<div class="m2"><p>به غیر از بردباری خوشنما نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوای خنده از عشاق جویا</p></div>
<div class="m2"><p>چو کبک کوهساری خوشنما نیست</p></div></div>