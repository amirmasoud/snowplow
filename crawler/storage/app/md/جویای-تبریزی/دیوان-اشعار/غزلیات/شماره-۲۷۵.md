---
title: >-
    شمارهٔ ۲۷۵
---
# شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>رهزن دین و دلم آن نرگس جادو بس است</p></div>
<div class="m2"><p>تیر روی ترکش مژگان نگاه او بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم من بر ما ضعیفان ناز آن ابرو بس است</p></div>
<div class="m2"><p>با کمان سختی به قدر قوت بازو بس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل شود بیتاب تر از مهربانیهای یار</p></div>
<div class="m2"><p>برق خرمن سوز طاقت روی گرم او بس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کند تسخیر عالم تیغ بی زنهار عجز</p></div>
<div class="m2"><p>‏ از خم بازو مرا شمشیر چون ابرو بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یافتن بتوان به فکر آن معنی باریک را</p></div>
<div class="m2"><p>عینک ارباب دید آیینهٔ زانو بس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفرت محراب از این مردم نخواهد حجتی</p></div>
<div class="m2"><p>اینکه دزدیده ست از اهل ریا پهلو بس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می توان داد سخن سنجی ز فیض فکر داد</p></div>
<div class="m2"><p>طوطی نطق ترا آیینهٔ زانو بس است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گو نباشد با گل و سوسن زبان وصف یار</p></div>
<div class="m2"><p>سرو باغ انگشت حیرت بر لب هر جو بس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما زلطف یار زین پس با عتابش ساختیم</p></div>
<div class="m2"><p>شمع خلوتخانهٔ دل گرمی آن خو بس است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست گیری اهل همت را نباشد جز کرم</p></div>
<div class="m2"><p>مرد را حرز جوادی قوت بازو بس است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون بر انگیزد جمالش لشکر خط را زجای</p></div>
<div class="m2"><p>بهر تسخیر تو جویا تیغ آن ابرو بس است</p></div></div>