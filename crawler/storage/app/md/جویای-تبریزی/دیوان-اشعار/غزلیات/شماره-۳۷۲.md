---
title: >-
    شمارهٔ ۳۷۲
---
# شمارهٔ ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>به آب و رنگ حسن او چمن نیست</p></div>
<div class="m2"><p>به خوش حرفی لعل او سخن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جایی می رسد هر کس ز خود رفت</p></div>
<div class="m2"><p>که امید ترقی در وطن نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر لعل است اگر یاقوت اگر می</p></div>
<div class="m2"><p>به رنگ آن لب شکرشکن نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بر آسمان رفته است خورشید</p></div>
<div class="m2"><p>تکلف برطرف، چون ماه من نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر شمشاد اگر سرو ار صنوبر</p></div>
<div class="m2"><p>به اندام تو ای گل پیرهن نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز روی گرمت امشب چشم بد دور</p></div>
<div class="m2"><p>تفاوت تا به شمع انجمن نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به روی آسمان حسن جویا</p></div>
<div class="m2"><p>سهیلیی همچو آن سیب ذقن نیست</p></div></div>