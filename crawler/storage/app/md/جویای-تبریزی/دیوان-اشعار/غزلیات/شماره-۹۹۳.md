---
title: >-
    شمارهٔ ۹۹۳
---
# شمارهٔ ۹۹۳

<div class="b" id="bn1"><div class="m1"><p>خداوندا دل وارسته از دنیا کرامت کن</p></div>
<div class="m2"><p>به سویت افتقار از غیرت استغنا کرامت کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مقصد می‌رساند هرکسی را لطف از راهی</p></div>
<div class="m2"><p>مرا هم فتح بابی از در دل‌ها کرامت کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان گفتگو کردی عطا، توفیق غوری ده!</p></div>
<div class="m2"><p>چو موجم ترزبانی با ته دریا کرامت کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنون آب و هوای دشت وسعت مشربی خواهد</p></div>
<div class="m2"><p>مرا از پردهٔ دل دامن صحرا کرامت کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم را می‌فشارد تنگنای عالم امکان</p></div>
<div class="m2"><p>مرا دامان دشتی در خور سودا کرامت کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببینم نیک و بد را تا به یک چشم اندر این محفل</p></div>
<div class="m2"><p>به سان شمع بزمم دیدهٔ بینا کرامت کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبان نکته‌سنجی همچو صائب بخش جویا را</p></div>
<div class="m2"><p>خدایا قطره‌ام را شورش دریا کرامت کن</p></div></div>