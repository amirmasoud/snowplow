---
title: >-
    شمارهٔ ۸۱۲
---
# شمارهٔ ۸۱۲

<div class="b" id="bn1"><div class="m1"><p>یار می آید و به استقبال</p></div>
<div class="m2"><p>می روم دمبدم ز حال به حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه شد دلنشین عزیز بود</p></div>
<div class="m2"><p>مردم چشم آینه است مثال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تصور کنم میانش را</p></div>
<div class="m2"><p>هست باریکتر ز راه خیال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلستان ز شرم سرو قدش</p></div>
<div class="m2"><p>می گذارد چو نخل موم نهال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن به کاهش دهد چو بدر منیر</p></div>
<div class="m2"><p>هر که باشد به فکر کسب کمال</p></div></div>