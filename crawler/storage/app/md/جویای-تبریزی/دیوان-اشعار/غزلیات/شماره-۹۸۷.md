---
title: >-
    شمارهٔ ۹۸۷
---
# شمارهٔ ۹۸۷

<div class="b" id="bn1"><div class="m1"><p>چنان یکباره ترک تیغ باز من برید از من</p></div>
<div class="m2"><p>کز آن قطع نظر خونابهٔ حسرت چکید از من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نیروی محبت کرد صید خویشتن دل را</p></div>
<div class="m2"><p>بسان دام ماهی هر قدر دامن کشید از من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان هر قطره خون دیده ام بر خاک می غلتد</p></div>
<div class="m2"><p>که شد صحرای امکان محشر چندین شهید از من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا در عالم دیوانگی نازم ز تنهائی</p></div>
<div class="m2"><p>که وحشت رام من گردید اگر الفت برید از من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همین دل را نه تنها منصب بی طاقتی باشد</p></div>
<div class="m2"><p>که هر عضوی جدا در خون حسرت می تپید از من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبیه صفحهٔ تصویر گردد پردهٔ گوشش</p></div>
<div class="m2"><p>به یاد عارض او ناله ای هر کس شنید از من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غرور حسن و بیباکی و استغنا و ناز از تو</p></div>
<div class="m2"><p>نیاز و احتیاج و عجز و زاری و امید از من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم در دست ناز اوست لوح مشق معشوقی</p></div>
<div class="m2"><p>به هر کس سرگردان شد، انتقام او کشید از من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز روی غیر چشم لطف جویا بر نمی دارد</p></div>
<div class="m2"><p>نمی دانم نگاه نازپروردش چه دید از من</p></div></div>