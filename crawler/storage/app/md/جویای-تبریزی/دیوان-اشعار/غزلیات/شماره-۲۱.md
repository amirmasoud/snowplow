---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>تا گشته است پای خم آرامگاه ما</p></div>
<div class="m2"><p>گردیده است کوه بدخشان پناه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگین ز گرد کلفت دل بسکه گشته است</p></div>
<div class="m2"><p>بر پای ما چو سلسله افتاده آه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا آب تربیت نخورد از گداز دل</p></div>
<div class="m2"><p>چون داغ لاله قد نکشد سرو آه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در رنگ همچو رشتهٔ یاقوت غوطه خورد</p></div>
<div class="m2"><p>از پهلوی عذار تو مد نگاه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگین دلیست لنگر شمشیر ابروش</p></div>
<div class="m2"><p>کس جان بدر نبرد زمژگان سپاه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دل متاع درد به تاراج گریه رفت</p></div>
<div class="m2"><p>پنهان در اشک همچو حباب ست آه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بس به شوق دیدنت از جا درآمده</p></div>
<div class="m2"><p>چون شمع بر سر مژه باشد نگاه ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برقع ز رخ فکنده درآ در حریم وصل</p></div>
<div class="m2"><p>باشد نقاب روی تو شرم نگاه ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جویا به بزم حیرت او همچو پیک کنگ</p></div>
<div class="m2"><p>گویید خبر زحال دل ما نگاه ما</p></div></div>