---
title: >-
    شمارهٔ ۵۰۵
---
# شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>چو زلفت بیدلان را در پی تسخیر می گردد</p></div>
<div class="m2"><p>به تن هر قطره خونم دانهٔ زنجیر می گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیالش را ز بس در پرده های دل نهان دارم</p></div>
<div class="m2"><p>فلک از دود آهم صفحهٔ تصویر می گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بقدر سوز دل گر دود آه از سینه برخیزد</p></div>
<div class="m2"><p>در اندک فرصتی این ابر عالمگیر می گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه تنها گردن مینا چو مارم می گزد بی او</p></div>
<div class="m2"><p>قدح شبهای هجرانش دهان شیر می گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چسان بی او قدم در ساحت گلشن نهم جویا</p></div>
<div class="m2"><p>که بر رویم نسیم گل دم شمشیر می گردد</p></div></div>