---
title: >-
    شمارهٔ ۳۴۰
---
# شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>کام دو جهان در قدم زاری دل‌هاست</p></div>
<div class="m2"><p>هر کار که شد ساخته از یاری دل‌هاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی جلوهٔ دیدار تو بر صفحهٔ امکان</p></div>
<div class="m2"><p>هر مد نگاهی خط بیزاری دل‌هاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خویش رهایی مطلب بی‌مدد عشق</p></div>
<div class="m2"><p>آزادی جان‌ها ز گرفتاری دل‌هاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌قبضه محال است که شمشیر ببرد</p></div>
<div class="m2"><p>بازو قوی از فیض مددکاری دل‌هاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جویا نگه از درگه دل روی نتابی</p></div>
<div class="m2"><p>اقبال و ظفر در گرو یاری دل‌هاست</p></div></div>