---
title: >-
    شمارهٔ ۳۲۱
---
# شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>تا کام خواهش از می بی غش گرفته است</p></div>
<div class="m2"><p>برگ گلی است لعل تو کآتش گرفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب تا سحر هوازدهٔ آرزوی تست</p></div>
<div class="m2"><p>گر غنچه را دماغ مشوش گرفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا خواهشم به نعمت دیدار دست یافت</p></div>
<div class="m2"><p>صد بوسه زان دو لب به نمک چش گرفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تأثیر آه ماست که هر شام از شفق</p></div>
<div class="m2"><p>اطراف دامن فلک آتش گرفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بگسلم ز یار که جویا دل مرا</p></div>
<div class="m2"><p>با تارتار طرهٔ دلکش گرفته است</p></div></div>