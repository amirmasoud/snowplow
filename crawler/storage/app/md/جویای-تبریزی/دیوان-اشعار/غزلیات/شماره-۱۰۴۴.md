---
title: >-
    شمارهٔ ۱۰۴۴
---
# شمارهٔ ۱۰۴۴

<div class="b" id="bn1"><div class="m1"><p>از برم دل می برد هر دم به رنگ تازه ای</p></div>
<div class="m2"><p>دلبر نازک نهالی، شوخ و شنگ تازه ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تازجوش بولهوس رنگش با باد شرم رفت</p></div>
<div class="m2"><p>ریخت بر روی هوا طرح فرنگ تازه ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترک چشم مست او در هر مژه بر هم زدن</p></div>
<div class="m2"><p>با دل عشاق ریزد رنگ جنگ تازه ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک چشم او ز هر مژگان بهم سودن بریخت</p></div>
<div class="m2"><p>بر دل غمدیده یک ترکش خدنگ تازه ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می دهد جویا ز هر جامی که بر لب می نهد</p></div>
<div class="m2"><p>گلشن امید ما را آب و رنگ تازه ای</p></div></div>