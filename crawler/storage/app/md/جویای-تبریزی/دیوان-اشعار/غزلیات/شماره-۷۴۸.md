---
title: >-
    شمارهٔ ۷۴۸
---
# شمارهٔ ۷۴۸

<div class="b" id="bn1"><div class="m1"><p>غیر از ایام وصال بت دلخواه مپرس</p></div>
<div class="m2"><p>چند پرسی ز شب هجر، مپرس آه مپرس!‏</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو شمعم دم تقریر زبان درگیرد</p></div>
<div class="m2"><p>جان من! آه، از این آتش جانکاه مپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره عشق تو کردم قدم از سر چو شرار</p></div>
<div class="m2"><p>اولین گام ز خود رفتنم از راه مپرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این یکی جان گل آن شعلهٔ افلاک گداز</p></div>
<div class="m2"><p>از نسیم سحر و آه سحرگاه مپرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند پرسی که چه حال است ترا ای جویا</p></div>
<div class="m2"><p>مستم و نیستم از حال خود آگاه مپرس</p></div></div>