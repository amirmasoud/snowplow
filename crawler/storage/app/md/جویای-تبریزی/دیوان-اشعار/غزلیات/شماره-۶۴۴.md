---
title: >-
    شمارهٔ ۶۴۴
---
# شمارهٔ ۶۴۴

<div class="b" id="bn1"><div class="m1"><p>ز روز حشر شهید ترا چه بیم بود</p></div>
<div class="m2"><p>در دو لخت بهشت از دل دو نیم بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به روی لالهٔ داغ درون بغلتد آه</p></div>
<div class="m2"><p>چو صحن باغ که جولانگه نسیم بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کناره جوی مباش از شعار اهل کرم</p></div>
<div class="m2"><p>کز این میان چو شوی کار باکریم بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدام درد بود سخت تر ز بیدردی</p></div>
<div class="m2"><p>دل صحیح بر عاشقان سقیم بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همین سلامت نفس از خدا طلب جویا</p></div>
<div class="m2"><p>که روز حشر سوال از دل سلیم بود</p></div></div>