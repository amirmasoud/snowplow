---
title: >-
    شمارهٔ ۲۳۸
---
# شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>دل که از بالای چشمت در گناه افتاده است</p></div>
<div class="m2"><p>یوسفی از پهلوی اخوان به چاه افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر فلک آهی که امشب برق جولان گشته بود</p></div>
<div class="m2"><p>کز شفق آتش به جان صبحگاه افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایهٔ ابروست بر پشت لب میگون یار</p></div>
<div class="m2"><p>یا غبار سرمه زان چشم سیاه افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می توان دریافت داغ خواری روی طلب</p></div>
<div class="m2"><p>زین کلف هایی که بر رخسار ماه افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صدمهٔ بال و پرش تا پلک و مژگان مهر شد</p></div>
<div class="m2"><p>بر رخت در اضطراب از بس نگاه افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جلوهٔ رفتار او را تا تصور کرده ام</p></div>
<div class="m2"><p>خون دل از دیده ام جویا به راه افتاده است</p></div></div>