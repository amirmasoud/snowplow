---
title: >-
    شمارهٔ ۶۱۹
---
# شمارهٔ ۶۱۹

<div class="b" id="bn1"><div class="m1"><p>بگو به پیرمغان که شبها به روی میخواره درنبندد</p></div>
<div class="m2"><p>که هر که بهر شکست دلها کمر ببندد، کمر نبندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب فراقت سر تو گردم به گرد خاطر مگرد چندین</p></div>
<div class="m2"><p>حذر که آهم چو نخل شعله بری به غیر از شرر نبندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبخت واژون و جوش گریه ز طالع دون و فرط زاری</p></div>
<div class="m2"><p>هجوم اشکم شب وصالت عجب که راه نبندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ترک دنیا مگر توانی ز رنج مردن نجات یابی</p></div>
<div class="m2"><p>کسی که در شد به کنج عزلت کمر به قصد سفر نبندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آب و تاب گل جمالت به حسن رخسار و عقد دندان</p></div>
<div class="m2"><p>هوا نگرید چون نخندد سمن نروید گهر نبندد</p></div></div>