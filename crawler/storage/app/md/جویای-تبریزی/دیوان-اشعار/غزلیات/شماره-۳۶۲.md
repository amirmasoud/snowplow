---
title: >-
    شمارهٔ ۳۶۲
---
# شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>مه است روی تو یا آفتاب ازین دو کدام است</p></div>
<div class="m2"><p>مزه است لعل لبت یا شراب ازین دو کدام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو ابر و خط پشت لبت دو مطلع شوخند</p></div>
<div class="m2"><p>اسیر طور تو من انتخاب ازین دو کدام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان ز بادهٔ شوق تو سرخوشم که ندانم</p></div>
<div class="m2"><p>دل است در بر من یا کباب ازین دو کدام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دماغ روح ز بویی که تازگی بپذیرد</p></div>
<div class="m2"><p>شمیم زلف تو یا مشکناب ازین دو کدام است</p></div></div>