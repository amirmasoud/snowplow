---
title: >-
    شمارهٔ ۷۸۷
---
# شمارهٔ ۷۸۷

<div class="b" id="bn1"><div class="m1"><p>چو بیند آن عذار لاله گون شمع</p></div>
<div class="m2"><p>بریزد جای اشک از دیده خون شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رویت هر که چشمی کرده روشن</p></div>
<div class="m2"><p>رود از خود ز راه دیده چون شمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روم با اشک و آه از خود که باشد</p></div>
<div class="m2"><p>شب هجرم در این ره رهنمون شمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا جویا ز دلسوزی شب هجر</p></div>
<div class="m2"><p>برد با خویشتن از خود برون شمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست سوز سینه ام را نسبتی با سوز شمع</p></div>
<div class="m2"><p>کی بود تاریکی شبهای ما با روز شمع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شود فرش شبستان تو زینت داده اند</p></div>
<div class="m2"><p>مخمل مشکین شب را از گل زرد و زشمع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می گدازد استخوان پیکر ما همچو شمع</p></div>
<div class="m2"><p>چون برافروزد ز می آن سرو بالا همچو شمع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پی هم بسکه آب دیده بر رخسار ریخت</p></div>
<div class="m2"><p>جاده ها در راه اشکم گشته پیدا همچو شمع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امشب از آتش فشانیهای صهبا دور نیست</p></div>
<div class="m2"><p>پنبه در گیرد اگر بر فرق مینا همچو شمع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در ره تیرت که چون شمع آتشین پیکان بود</p></div>
<div class="m2"><p>هست با هر استخوانم چشم بینا همچو شمع</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از تب عشقش که شریانم رگ برق است ازو</p></div>
<div class="m2"><p>شعله ور گردد سر انگشت مسیحا همچو شمع</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در شب هجرش گل داغی اگر بر سر زنم</p></div>
<div class="m2"><p>می دواند ریشه جویا تا کف پا همچو شمع</p></div></div>