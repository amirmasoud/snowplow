---
title: >-
    شمارهٔ ۹۸۱
---
# شمارهٔ ۹۸۱

<div class="b" id="bn1"><div class="m1"><p>ساقی بیار باده و عیشم به کام کن</p></div>
<div class="m2"><p>زین بیش خون مکن به دلم می به جام کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خون آرزوی تو عمری است می تپد</p></div>
<div class="m2"><p>کار دلم به نیم نگاهی تمام کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند از خمار توان دردسر کشید؟</p></div>
<div class="m2"><p>دست هوس ز می کش و عیش مدام کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با یار باده نوش و مخور آب دور ازو</p></div>
<div class="m2"><p>تفریق در میان حلال و حرام کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چشم غیر در رگ جانم نهفته وار</p></div>
<div class="m2"><p>یعنی که تیغ آن مژه را در نیام کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جویا ز رهزنان هوا پاس دل بدار</p></div>
<div class="m2"><p>این کلبه را نمونهٔ دارالسلام کن</p></div></div>