---
title: >-
    شمارهٔ ۵۸۵
---
# شمارهٔ ۵۸۵

<div class="b" id="bn1"><div class="m1"><p>سرخی از لعل تو زایل به مکیدن نشود</p></div>
<div class="m2"><p>هیچ کم لاله ازین باغ به چیدن نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی به مقصد نبری تا نگریزی از خویش</p></div>
<div class="m2"><p>طی این مرحله جز پای بریدن نشود</p></div></div>