---
title: >-
    شمارهٔ ۹۶۳
---
# شمارهٔ ۹۶۳

<div class="b" id="bn1"><div class="m1"><p>نیست جای پانهادن بر زمین</p></div>
<div class="m2"><p>پرگل است امروز سرتاسر زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه بار منت زلفت کشد</p></div>
<div class="m2"><p>می گدازد نافه آهو بر زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غمش از تندباد آه ما</p></div>
<div class="m2"><p>باخت همچون آسمان لنگر زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله و گل می دهد بوی کباب</p></div>
<div class="m2"><p>تا شد از یک قطره اشکم تر زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نظر از جوش رنگارنگ گل</p></div>
<div class="m2"><p>کرده هر دم جلوهٔ دیگر زمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه پرکین دید از هر سبزه ای</p></div>
<div class="m2"><p>می کشد بر آسمان خنجر زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وادون از بسکه جویا پرگلست</p></div>
<div class="m2"><p>پا خورد هر کس رود زین سرزمین</p></div></div>