---
title: >-
    شمارهٔ ۵۴۰
---
# شمارهٔ ۵۴۰

<div class="b" id="bn1"><div class="m1"><p>از رفتنت خوشی ز چمن دور می شود</p></div>
<div class="m2"><p>هر غنچهٔ گلی دل رنجور می شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بی دماغی ام سر گلگشت باغ نیست</p></div>
<div class="m2"><p>کآنجا ز شور خندهٔ گل شور می شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظالم مکن ستم به ضعیفان که روز حشر</p></div>
<div class="m2"><p>هر مور صد برابر زنبور می شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانند ریگ شیشهٔ ساعت عجب مدان</p></div>
<div class="m2"><p>گر گور خاکسار پر از نور می شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل جراحتت نپذیرد علاج کس</p></div>
<div class="m2"><p>چون گل ز بخیه زخم تو ناسور می شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر روز اینچنین که شود روزگار تنگ</p></div>
<div class="m2"><p>این عرصه رفته رفته دل مور می شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جوش آرزو دل ابنای روزگار</p></div>
<div class="m2"><p>پرشورتر ز خانهٔ زنبور می شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جویا ز شوق جان سپرد یا علی، اگر</p></div>
<div class="m2"><p>داند که با سگان تو محشور می شود</p></div></div>