---
title: >-
    شمارهٔ ۱۰۴۳
---
# شمارهٔ ۱۰۴۳

<div class="b" id="bn1"><div class="m1"><p>باز از تغافلی صف پیمان شکسته ای</p></div>
<div class="m2"><p>طرف کلاه ناز بسامان شکسته ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد دشنهٔ به زهر بلا آب داده را</p></div>
<div class="m2"><p>در سینه ام ز شوخی مژگان شکسته ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آشناییم لب افسوس می گزی</p></div>
<div class="m2"><p>با ما نمک نخورده نمکدان شکسته ای</p></div></div>