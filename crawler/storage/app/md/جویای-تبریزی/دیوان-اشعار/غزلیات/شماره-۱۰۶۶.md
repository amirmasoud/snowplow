---
title: >-
    شمارهٔ ۱۰۶۶
---
# شمارهٔ ۱۰۶۶

<div class="b" id="bn1"><div class="m1"><p>ز چشمت سرمه گرد وحشت آهوست پنداری</p></div>
<div class="m2"><p>به لعلت سبز حسن مطلع ابروست پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا در پاس عزلت لذت عمر ابد باشد</p></div>
<div class="m2"><p>اگر آب حیاتی هست آب روست پنداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زشمشیر تغافل تا دو نیم افتاد در راهش</p></div>
<div class="m2"><p>دل آواره ام نقش پی آهوست پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا از دیدن خود تا به فکر خویش افتادی</p></div>
<div class="m2"><p>به زانو آینه، آئینه زانوست پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز فیض نکته سنجی بزم را رشک ارم کردی</p></div>
<div class="m2"><p>لب خاموشی جویا غنچهٔ بی بوست پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخسار تو از خوش آب و رنگی</p></div>
<div class="m2"><p>زد طعنه به قرمز فرنگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل در خم زلف مشکفام است</p></div>
<div class="m2"><p>چون آینه ای به دست زنگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افسوس که همچو برگ رعنا</p></div>
<div class="m2"><p>پیداست زنو گلم دو رنگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خط بررخ آن فرنگ حسنت</p></div>
<div class="m2"><p>پیچیده تر از خط فرنگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جویا به خیال آن دهن رفت</p></div>
<div class="m2"><p>افتاد دلش به دام تنگی</p></div></div>