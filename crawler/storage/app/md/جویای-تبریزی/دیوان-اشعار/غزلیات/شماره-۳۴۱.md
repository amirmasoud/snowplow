---
title: >-
    شمارهٔ ۳۴۱
---
# شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>آنکه از داغ فراقت جگرش سوخته است</p></div>
<div class="m2"><p>یار اغیار شدن بیشترش سوخته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اولین گام بود راه به مقصد چون برق</p></div>
<div class="m2"><p>هر که از گرم روی بال و پرش سوخته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوری از مال جهان جوی که آسوده شوی</p></div>
<div class="m2"><p>جگر لاله ز پهلوی زرش سوخته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شعلهٔ آه زلب تا مژه ام را دریافت</p></div>
<div class="m2"><p>عالم هجر نگر خشک و ترش سوخته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خویش را شمع صفت می دهد از آه به باد</p></div>
<div class="m2"><p>هر که از آتش عشقت جگرش سوخته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیر آهم ندهد داد فلک را جویا</p></div>
<div class="m2"><p>مگر از آتش دل بال و پرش سوخته است</p></div></div>