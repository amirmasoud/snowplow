---
title: >-
    شمارهٔ ۴۳۳
---
# شمارهٔ ۴۳۳

<div class="b" id="bn1"><div class="m1"><p>آنچه با دل چشم آن ترک ستمگر می کند</p></div>
<div class="m2"><p>نامسلمانم اگر کافر به کافر می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با هوای نفس کی آرام دل حاصل شود</p></div>
<div class="m2"><p>اضطراب بحر را صرصر فزونتر می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشته ام هم بزم بی باکی که از شوخی مدام</p></div>
<div class="m2"><p>عاشقان را خون به دل چون می بساغر می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کمین صید مطلب تا به کی خواهد نشست</p></div>
<div class="m2"><p>دام ازین راه است دایم خاک بر سر می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این جواب آن غزل جویا که بینش گفته است</p></div>
<div class="m2"><p>نامه ام را پاره چون بال کبوتر می کند</p></div></div>