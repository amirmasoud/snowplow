---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>چه جان باشد به پیش چشم او دل‌های سنگین را</p></div>
<div class="m2"><p>زمژگان در فلاخن می گدازد کوه تمکین را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحمدالله که در بزم محبت شمع تابانم</p></div>
<div class="m2"><p>به آتش داده ام از گرم خونیها شرائین را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب میگون جانان را چه نقصان از غبار خط</p></div>
<div class="m2"><p>ز رنگینی نیندازد مداد اشعار رنگین را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مستی نکته پیرا می شود لعلت از آن کز هم</p></div>
<div class="m2"><p>جدا سازد می از تردستی آن لبهای شیرین را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شوخی های مژگان چشم او دارد جگر خونم</p></div>
<div class="m2"><p>خدا رحمی به دل اندازد آن مست شرابین را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیندیشد دل دیوانه از جور فلک جویا</p></div>
<div class="m2"><p>چه پروا باشد از زور کمان بازوی زورین را</p></div></div>