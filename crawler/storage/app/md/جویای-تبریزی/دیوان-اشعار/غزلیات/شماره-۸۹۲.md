---
title: >-
    شمارهٔ ۸۹۲
---
# شمارهٔ ۸۹۲

<div class="b" id="bn1"><div class="m1"><p>یک‌رنگ بی‌خودی شده از خود بریده‌ام</p></div>
<div class="m2"><p>با وحشت آرمیده‌ام از بس رمیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرش برین مقام من از فیض بی‌خودی است</p></div>
<div class="m2"><p>از خویش رفته‌رفته به جایی رسیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سایه در ره تو به خاک اوفتاده‌ام</p></div>
<div class="m2"><p>گرد ره فنا شده رنگ پریده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر مگوی پرده‌دری‌های غنچه را</p></div>
<div class="m2"><p>جویا هزار بار ز بلبل شنیده‌ام</p></div></div>