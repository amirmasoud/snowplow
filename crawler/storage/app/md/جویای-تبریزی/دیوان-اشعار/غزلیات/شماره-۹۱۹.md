---
title: >-
    شمارهٔ ۹۱۹
---
# شمارهٔ ۹۱۹

<div class="b" id="bn1"><div class="m1"><p>گل دیدار ترا چون پی چیدن رفتم</p></div>
<div class="m2"><p>شبنم آسا همه تن دیده به دیدن رفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو آن شمع که در رهگذر باد بود</p></div>
<div class="m2"><p>بسکه بگریستم از غم به چکیدن رفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>استخوان را طپش نبض بود در بدنم</p></div>
<div class="m2"><p>موجم، از خود به پر و بال طپیدن رفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل الفت خورد از چشمهٔ یکرنگی آب</p></div>
<div class="m2"><p>تا شوم رام تو وحشی به رمیدن رفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لطف کن جام روان بخش! وگرنه چو حباب</p></div>
<div class="m2"><p>اینک از خویش به خمیازه کشیدن رفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیرتم بر سر شور است، حریفان عشقی!</p></div>
<div class="m2"><p>خامشی نغمه سرا شد به شنیدن رفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قامتم نیست دو تا گشته ز پیری جویا</p></div>
<div class="m2"><p>بسکه پر بار گناهم به خمیدن رفتم</p></div></div>