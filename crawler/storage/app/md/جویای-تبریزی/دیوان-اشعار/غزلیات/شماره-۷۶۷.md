---
title: >-
    شمارهٔ ۷۶۷
---
# شمارهٔ ۷۶۷

<div class="b" id="bn1"><div class="m1"><p>دل به عشق از بستگی وا می شود غمگین مباش</p></div>
<div class="m2"><p>عاقبت این قطره دریا می شود غمگین مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد جان بیعانهٔ یک بوسه زان لعل لب است</p></div>
<div class="m2"><p>شاد زی ایدل که سودا می شود غمگین مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حصول مدعا بیتابیی در کار نیست</p></div>
<div class="m2"><p>گر نشد امروز فردا می شود غمگین مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیش خود را تلخ از زهراب نومیدی مکن</p></div>
<div class="m2"><p>کام دل آخر مهیا می شود غمگین مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نشد کام دلت حاصل مشو در اضطراب</p></div>
<div class="m2"><p>صبر در کار است جویا! می شود، غمگین مباش!‏</p></div></div>