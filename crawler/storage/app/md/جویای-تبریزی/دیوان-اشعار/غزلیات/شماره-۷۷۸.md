---
title: >-
    شمارهٔ ۷۷۸
---
# شمارهٔ ۷۷۸

<div class="b" id="bn1"><div class="m1"><p>در بزم می چو آمده ای بی حجاب باش</p></div>
<div class="m2"><p>شوخ و حریف حرف مصاحب شراب باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که جا دهنده معراج عزتت</p></div>
<div class="m2"><p>با خلق گرم روی تر از آفتاب باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز مگوی جز صفت همنشین خویش</p></div>
<div class="m2"><p>در خلق، طاق چون نقط انتخاب باش!‏</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایدل غم زمانه نمی گویمت مخور</p></div>
<div class="m2"><p>پیوسته زیر سیلی موج شراب باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون نیازت از سر مژگان به ناز ریز</p></div>
<div class="m2"><p>ای دل! هم اشک مرغ چمن، هم گلاب باش!‏</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهی بود ز عرش برین رتبه ات بلند</p></div>
<div class="m2"><p>جویا غبار رهگذر بوتراب باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنده ام کن ساقی از یک جرعهٔ می زود باش</p></div>
<div class="m2"><p>انتظارم می کشد بی درد تا کی، زود باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر خریداری متاع درد را وقت است وقت</p></div>
<div class="m2"><p>نقد فرصت می رود از دست هی هی زود باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برفها خاست از روی زمین ساقی می آر</p></div>
<div class="m2"><p>رخت خود را بست یعنی موسم دی زود باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا ترا نشکسته پیری راه مقصد پیش گیر</p></div>
<div class="m2"><p>در جوانی این ره آسانتر شود طی زود باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبح شد مطرب، زمان سحرکاریها رسید</p></div>
<div class="m2"><p>شعله را پیراهنی در برکن از نی زود باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ساقی از یک جرعه صفرای خمارم نشکند</p></div>
<div class="m2"><p>وقت جویا خوش کن از جام پیاپی زود باش</p></div></div>