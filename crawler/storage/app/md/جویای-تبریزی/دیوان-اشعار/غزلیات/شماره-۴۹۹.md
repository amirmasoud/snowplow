---
title: >-
    شمارهٔ ۴۹۹
---
# شمارهٔ ۴۹۹

<div class="b" id="bn1"><div class="m1"><p>عیب نخوت در لباس فقر عریان تر شود</p></div>
<div class="m2"><p>در ضعیفی ها رگ گردن نمایان تر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گشاد کارهای بسته چندین غم مخور</p></div>
<div class="m2"><p>هر قدر پیچد گره بر خویش چسبان تر شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارهای مشکل آسان می توان شد به سعی</p></div>
<div class="m2"><p>لیک اگر بر خود نگیری مشکل آسان تر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل پشیمان شد ز ترک باده پیش از بهار</p></div>
<div class="m2"><p>چون به جوش آمد گل و بلبل پشیمان تر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان پاک از غفلت دلها فتد در اضطراب</p></div>
<div class="m2"><p>زان نفس در خواب راحت تند جولان تر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوخ من جویا نماید با پری نسبت درست</p></div>
<div class="m2"><p>جلوهٔ مستور او چندانکه پنهان تر شود</p></div></div>