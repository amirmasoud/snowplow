---
title: >-
    شمارهٔ ۶۹۴
---
# شمارهٔ ۶۹۴

<div class="b" id="bn1"><div class="m1"><p>هر که چون صبح خودنمایی کرد</p></div>
<div class="m2"><p>پیرهن بر تنش قبایی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشوهٔ لاجوردیی که تراست</p></div>
<div class="m2"><p>رنگ روی مرا طلایی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون توان دید ماه را کامشب</p></div>
<div class="m2"><p>چهره شد با تو بی حیایی کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدهٔ من ز دیدن رویت</p></div>
<div class="m2"><p>آشنایی به روشنایی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخورد آبی از زلال وصال</p></div>
<div class="m2"><p>دل که او خوی با جدایی کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوخ بالا بلند من جویا</p></div>
<div class="m2"><p>سرو را خنده بر رسایی کرد</p></div></div>