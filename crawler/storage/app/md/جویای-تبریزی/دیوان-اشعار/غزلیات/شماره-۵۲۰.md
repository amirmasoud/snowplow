---
title: >-
    شمارهٔ ۵۲۰
---
# شمارهٔ ۵۲۰

<div class="b" id="bn1"><div class="m1"><p>تا نفس باشد ستون خیمهٔ تن چون حباب</p></div>
<div class="m2"><p>جز هوایش در سر شوریده‌ام سامان مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد ازین جویا دلت در موج خیز اضطراب</p></div>
<div class="m2"><p>از فراق کامران بیگ و ملک سلطان مباد</p></div></div>