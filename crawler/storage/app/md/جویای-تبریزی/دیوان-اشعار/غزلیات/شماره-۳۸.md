---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>در عشق دل چو مخزن اسرار شد مرا</p></div>
<div class="m2"><p>آئینه تجلی دیدار شد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس نهان ز درد تو در گرد کلفتم</p></div>
<div class="m2"><p>رنگ شکسته رخنهٔ دیوار شد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برباد پای شوق و برون تاختم زخویش</p></div>
<div class="m2"><p>پست و بلند مرحله هموار شد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باشد مدام گرم پرافشانی از طپش</p></div>
<div class="m2"><p>دل عندلیب آن گل رخسار شد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنگی دل فزود به کتمان راز عشق</p></div>
<div class="m2"><p>قفل در خزینهٔ اسرار شد مرا</p></div></div>