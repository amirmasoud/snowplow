---
title: >-
    شمارهٔ ۶۷۹
---
# شمارهٔ ۶۷۹

<div class="b" id="bn1"><div class="m1"><p>لب خندان به تو ای غنچه دهن بخشیدند</p></div>
<div class="m2"><p>چشم گریان و دل خسته به من بخشیدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآشنایی سخن شر کن ای دل که ترا</p></div>
<div class="m2"><p>همه دادند اگر درد سخن بخشیدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب و رنگی که فزون زان لب و دندان آمد</p></div>
<div class="m2"><p>به یمن قدری و قدری به عدن بخشیدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا برد زخم دل غنچهٔ گل لذت درد</p></div>
<div class="m2"><p>نمک ناله به مرغان چمن بخشیدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیش شد حلقهٔ زنجیر گرفتاری ما</p></div>
<div class="m2"><p>زان دو چشمی که به آهوی ختن بخشیدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر قدر در سر آن زلف پریشانی بود</p></div>
<div class="m2"><p>همه را جمع نمودند و بمن بخشیدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دولت یاد بناگوش و قدی یافت دلم</p></div>
<div class="m2"><p>گرچه جویا بهمن سرو و سمن بخشیدند</p></div></div>