---
title: >-
    شمارهٔ ۱۰۴۸
---
# شمارهٔ ۱۰۴۸

<div class="b" id="bn1"><div class="m1"><p>چون بی تو چارهٔ دل محزون کند کسی</p></div>
<div class="m2"><p>خون می چکد ز قطع نظر چون کند کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی به یاد لعل لبی شام تا سحر</p></div>
<div class="m2"><p>دل در فشار پنجهٔ غم خون کند کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند بی تو پنجهٔ مژگان خویش را</p></div>
<div class="m2"><p>مرجان صفت ز اشک جگرگون کند کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرخوش شود زبوی می «لی مع اللهی»</p></div>
<div class="m2"><p>خود را دمی ز خویش چو بیرون کند کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باید نخست ساخت وضو ز آب دیده اش</p></div>
<div class="m2"><p>خواهد چو طوف تربت مجنون کند کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جویا اسیر طور تو، آخر مروتی!‏</p></div>
<div class="m2"><p>ظلم اینقدر به عاشق مفتون کند کسی؟</p></div></div>