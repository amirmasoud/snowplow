---
title: >-
    شمارهٔ ۶۸۵
---
# شمارهٔ ۶۸۵

<div class="b" id="bn1"><div class="m1"><p>دل را به قدر خمکده ها جوش داده اند</p></div>
<div class="m2"><p>تا منصب قبول تو می نوش داده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لخت دل زبان دگر شد مرا به کام</p></div>
<div class="m2"><p>تا همچو غنچه ام لب خاموش داده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این شیوه خاصهٔ مژه های دراز تست</p></div>
<div class="m2"><p>هر نیش را نه چاشنی نوش داده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روشندلان که مسلکشان عالم رضاست</p></div>
<div class="m2"><p>بر نیک و بد چو آینه آغوش داده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوش خیالت از سر ماکم نمی شود</p></div>
<div class="m2"><p>ما را مدام ساغر سر جوش داده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در روزگار اهل کمالند بی سخن</p></div>
<div class="m2"><p>خوش رتبه ای به مردم خاموش داده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جویا چو موج بی سر و پایان راه عشق</p></div>
<div class="m2"><p>دایم به لجهٔ خطر آغوش داده اند</p></div></div>