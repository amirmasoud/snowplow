---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>چسبیده به هم گرچه لب از شهد نکاتت</p></div>
<div class="m2"><p>دل خون شده بی گفت و شنود از حرکاتت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر ابدی در گروه کشتن نفس است</p></div>
<div class="m2"><p>این کشته تواند شدن اکسیر حیاتت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آب در دندان تو هنگام تبسم</p></div>
<div class="m2"><p>ترسم بگدازد لبک همچو نباتت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش باش! به دل داری اگر داغ غم عشق</p></div>
<div class="m2"><p>زینت به سجل یافته منشور نجاتت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عشق به انجام رسد کار تو جویا</p></div>
<div class="m2"><p>چون شمع گر از جا نرود پای ثباتت</p></div></div>