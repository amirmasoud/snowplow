---
title: >-
    شمارهٔ ۶۸۴
---
# شمارهٔ ۶۸۴

<div class="b" id="bn1"><div class="m1"><p>به شوخی برق را راه تپش بر بال و پر بندد</p></div>
<div class="m2"><p>به تمکین کوه را چون کوچک ابدالان کمر بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از ترک علایق کامیاب مدعا گردد</p></div>
<div class="m2"><p>بلی این نخل چون برگش فرو ریزد ثمر بندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فغان از موج خیز عشق کز دل تا سر مژگان</p></div>
<div class="m2"><p>به طفل اشک صد جا ره ز خوناب جگر بندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حلاوت تا به مغز استخوانش را فرو گیرد</p></div>
<div class="m2"><p>کمر بر کام بخشی آنکه همچون نیشکر بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم از بسکه جویا سردمهری دیده از خوبان</p></div>
<div class="m2"><p>سرشکم بر سر مژگان به آیین گهر بندد</p></div></div>