---
title: >-
    شمارهٔ ۱۰۶۰
---
# شمارهٔ ۱۰۶۰

<div class="b" id="bn1"><div class="m1"><p>زاهد از خشکی است کز مینا کند پهلو تهی</p></div>
<div class="m2"><p>ساحل لب تشنه از دریا کند پهلو تهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نسیم حرف سرد این نصیحت پیشگان</p></div>
<div class="m2"><p>شمع بزم ما مباد از ما کند پهلو تهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهرتش چون قاف بر اطراف عالم می دود</p></div>
<div class="m2"><p>همچو عنقا آنکه از دنیا کند پهلو تهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هراسند اهل دل از صحبت دیوانگان</p></div>
<div class="m2"><p>بحر از سیلاب از آن جویا کند پهلو تهی</p></div></div>