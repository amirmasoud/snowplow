---
title: >-
    شمارهٔ ۳۹۳
---
# شمارهٔ ۳۹۳

<div class="b" id="bn1"><div class="m1"><p>ابروست که بر چهرهٔ دلدار بلند است</p></div>
<div class="m2"><p>یا مطلع برجستهٔ بسیار بلند است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ظرف خیال تو محال است درآید</p></div>
<div class="m2"><p>پست است ترا دانش و اسرار بلند است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگرفت دل از تربیت جسم که خانه</p></div>
<div class="m2"><p>آید به نظر تنگ چو دیوار بلند است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز چو چیده گلی از فیض بهاران</p></div>
<div class="m2"><p>زان روی زبان گلهٔ خار بلند است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوته بود از ساغر می دست امیدم</p></div>
<div class="m2"><p>یا مصطبهٔ خانهٔ خمار بلند است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود را به مکان در و دیوار نسنجی</p></div>
<div class="m2"><p>هشدار که این مرتبه بسیار بلند است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پهلو زده بر مقطع او مطلع خورشید</p></div>
<div class="m2"><p>جویای ترا پایهٔ افکار بلند است</p></div></div>