---
title: >-
    شمارهٔ ۱۰۸۷
---
# شمارهٔ ۱۰۸۷

<div class="b" id="bn1"><div class="m1"><p>مستور گشت رویش زیر نقاب نیمی</p></div>
<div class="m2"><p>گویی که منکسف شد از آفتاب نیمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون برگ لاله را هر لخت دلی ز داغت</p></div>
<div class="m2"><p>خون گشته است نیمی، گشته کباب نیمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حیرتم که چون رفت از خط تمام حسنش</p></div>
<div class="m2"><p>بایست گردد آن رو بی آب و تاب نیمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دور رخ خط او نام خدا چو ماه است</p></div>
<div class="m2"><p>بیرون ابر نیمی، زیر سحاب نیمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لطف و قهرش امشب پیمانهٔ دل من</p></div>
<div class="m2"><p>نیمی پر از شراب است پرخون ناب نیمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جویا شب وصالش نصف دلت شده خوش</p></div>
<div class="m2"><p>بنمود چهره اما از بس حجاب نیمی</p></div></div>