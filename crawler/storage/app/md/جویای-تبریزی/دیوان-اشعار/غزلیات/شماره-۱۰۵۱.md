---
title: >-
    شمارهٔ ۱۰۵۱
---
# شمارهٔ ۱۰۵۱

<div class="b" id="bn1"><div class="m1"><p>تا مرا شد با تو ای گل پیرهن دلبستگی</p></div>
<div class="m2"><p>هست همچون غنچه ام با خویشتن دلبستگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر نزد یک غنچه هم از نونهال من هنوز</p></div>
<div class="m2"><p>برنتابد سرو آن نازک بدن دلبستگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمینی کاید از وی بوی جانان خلد ماست</p></div>
<div class="m2"><p>نیست همچون غنچه ما را با چمن دلبستگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من چرا واله نباشم پیش تنگ شیرین</p></div>
<div class="m2"><p>او که خود چون غنچه دارد با دهن دلبستگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچهٔ گل نافه را ماند به یاد زلف او</p></div>
<div class="m2"><p>کرده است از بسکه با مشک ختن دلبستگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم او مستغنی از زلف است در دل بردنم</p></div>
<div class="m2"><p>عاشقان را نیست محتاج رسن دلبستگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکته ای جویا ز لعل او در گوشم نشد</p></div>
<div class="m2"><p>غنچهٔ او راست گویا با سخن دلبستگی</p></div></div>