---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>زهر چشم آلوده بود این باده کاندر جام ریخت</p></div>
<div class="m2"><p>ساقی امشب لخت دل چون غنچه ام در کام ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه لبریز طراوت در خرام آمد به باغ</p></div>
<div class="m2"><p>آبروی صد خیابان گل از آن اندام ریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکوه ای کردم رقم از اشک ریزی های چشم</p></div>
<div class="m2"><p>چون ز گل شبنم ز حسرت نامه ام پیغام ریخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه شست آن داغ را کز یاد چشمت داشت دل</p></div>
<div class="m2"><p>حیف کز بسیاری باران گل بادام ریخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی شراب خوشدلی جویا به دست آسان فتد</p></div>
<div class="m2"><p>غنچه از بهر شکفتن خون دل در جام ریخت</p></div></div>