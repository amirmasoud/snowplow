---
title: >-
    شمارهٔ ۹۰۰
---
# شمارهٔ ۹۰۰

<div class="b" id="bn1"><div class="m1"><p>کرد دلها را به مژگان نرگس جانانه موم</p></div>
<div class="m2"><p>می کند فولاد را سرپنجهٔ مردانه موم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اضطرابم یار سرکش را ملایم می کند</p></div>
<div class="m2"><p>شمع محفل گشته از بیتابی پروانه موم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیسهٔ نقد وجودم سر به مهر حیرت است</p></div>
<div class="m2"><p>تا بشد از آتش عشقش دل دیوانه موم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در حریم چرب نرمی جوش عشرت می زند</p></div>
<div class="m2"><p>همچو زنبور عسل آن را که باشد خانه موم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قفل آتش را توان جویا به آسانی گشود</p></div>
<div class="m2"><p>گر کلید همت دل را بود دندانه موم</p></div></div>