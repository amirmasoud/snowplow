---
title: >-
    شمارهٔ ۸۵۴
---
# شمارهٔ ۸۵۴

<div class="b" id="bn1"><div class="m1"><p>گرنه از جوش نزاکت بر تنش سنگین بود</p></div>
<div class="m2"><p>تار و پود پیرهن از رشتهٔ جانش کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منصب مشعل فروزی داده عشقش سینه را</p></div>
<div class="m2"><p>شمعها از آه روشن در شبستانش کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خودبخود گل می کند اسرار دلها، تا به کی</p></div>
<div class="m2"><p>همچو بوی غنچه ضبط راز پنهانش کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو عروج طالعی جویا که تا مانند ماه</p></div>
<div class="m2"><p>جای در یک پیرهن با مهر تابانش کنم</p></div></div>