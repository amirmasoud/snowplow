---
title: >-
    شمارهٔ ۵۲۶
---
# شمارهٔ ۵۲۶

<div class="b" id="bn1"><div class="m1"><p>فلک تیغ ستم کی از من مهجور بردارد؟</p></div>
<div class="m2"><p>کجا از عاشقان زار دست زور بردارد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چشم اشک ندامت بسکه دردآلود می ریزد</p></div>
<div class="m2"><p>ز سیلاب سرشکم بحر رحمت شور بردارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم در خوردن خوناب غم زین پس نیندیشد</p></div>
<div class="m2"><p>نپرهیزد امید از خویش چون رنجور بردارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بس گردیده پر شور شرارت جای ان دارد</p></div>
<div class="m2"><p>که طرح خانهٔ خود از دلت زنبور بردارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمی با خود برآ تا رستم دوران شوی جویا</p></div>
<div class="m2"><p>چو بگردد کمان حلقه از خود زور بردارد</p></div></div>