---
title: >-
    شمارهٔ ۵۱۷
---
# شمارهٔ ۵۱۷

<div class="b" id="bn1"><div class="m1"><p>من که در سیر گلم بیخودی مل باشد</p></div>
<div class="m2"><p>می و پیمانه ام از بوی گل و گل باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خودنما گشته سر زلف تو از هر سر موی</p></div>
<div class="m2"><p>لازم طول امل عرض تجمل باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با دل سوخته ام گرمی سرشار مکن</p></div>
<div class="m2"><p>که علاجش به تباشیر تغافل باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور از آن زلف دلم بسکه پریشان حالست</p></div>
<div class="m2"><p>آه آشفته من سایهٔ سنبل باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که در بحر تمنای تو افتد چون موج</p></div>
<div class="m2"><p>دست و پا بازند اگر کوه تحمل باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کشد آخر کارش به پریشانحالی</p></div>
<div class="m2"><p>غنچه سان دل ز چه در بند تمول باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی به طوفان حوادث روم از جا جویا</p></div>
<div class="m2"><p>لنگر زورق دل بار تحمل باشد</p></div></div>