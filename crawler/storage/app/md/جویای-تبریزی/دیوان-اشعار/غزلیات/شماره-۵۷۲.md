---
title: >-
    شمارهٔ ۵۷۲
---
# شمارهٔ ۵۷۲

<div class="b" id="bn1"><div class="m1"><p>چون شدم از خویش ره در بزم یارم داده اند</p></div>
<div class="m2"><p>رفته ام تا از میان جا در کنارم داده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر مآل خویش خندم یا به اوضاع زمان</p></div>
<div class="m2"><p>من که همچون گل یک خنده وارم داده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سر مو جوش وحدت می زند بر پیکرم</p></div>
<div class="m2"><p>تا ز دل پیمانهٔ لبریز یارم داده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گل داغش بخندد در فضای سینه ام</p></div>
<div class="m2"><p>دیدهٔ گریان تر از ابر بهارم داده اند</p></div></div>