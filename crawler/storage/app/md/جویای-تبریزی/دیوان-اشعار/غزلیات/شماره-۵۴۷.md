---
title: >-
    شمارهٔ ۵۴۷
---
# شمارهٔ ۵۴۷

<div class="b" id="bn1"><div class="m1"><p>رازها را لب خاموش نگهبان باشد</p></div>
<div class="m2"><p>غنچه را پرده در دل لب خندان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشحه ای ریخته از خامهٔ بی رنگی او</p></div>
<div class="m2"><p>هر قدر نقش که بر صفحهٔ امکان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش افروز دلم آنکه به یک ساغر شد</p></div>
<div class="m2"><p>گر کشد جام دگر آفت دوران باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خال بیجاست بجز عارض او در هر جاست</p></div>
<div class="m2"><p>مسند مور کف دست سلیمان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم متاعی است که در سینه من ریخته است</p></div>
<div class="m2"><p>حسن، جنسی که به بازار تو ارزان باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر لبش شور فغان شیون زنجیر شود</p></div>
<div class="m2"><p>هر کرا زلف کجت سلسله جنبان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل جویا ز تمنای می و شاهد و شمع</p></div>
<div class="m2"><p>همچو پروانه و شبهای چراغان باشد</p></div></div>