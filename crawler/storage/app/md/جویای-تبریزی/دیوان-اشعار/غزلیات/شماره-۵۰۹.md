---
title: >-
    شمارهٔ ۵۰۹
---
# شمارهٔ ۵۰۹

<div class="b" id="bn1"><div class="m1"><p>آه از لب چون شکر خوبان سمرقند</p></div>
<div class="m2"><p>کش نیشکر قامتشان راست ثمرقند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لطف مزاجی که تو داری، نپسندی</p></div>
<div class="m2"><p>از شیرهٔ جان ریخته باشند اگر قند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبود نمک چشم تو با دیدهٔ بادام</p></div>
<div class="m2"><p>حاشا که بود چاشنی لعل تو در قند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیرینی گفتار وی از پهلوی لبهاست</p></div>
<div class="m2"><p>آری به عمل آمده جویا ز شکر، قند</p></div></div>