---
title: >-
    شمارهٔ ۸۰۹
---
# شمارهٔ ۸۰۹

<div class="b" id="bn1"><div class="m1"><p>چو گل ز فیض صبوحی پر است جام جمال</p></div>
<div class="m2"><p>به یک پیاله می از صاف رنگ مالامال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بد مجوی بجز فتنه چون بیابد دست</p></div>
<div class="m2"><p>زبان شورش زنبور نیست غیر از بال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین که سوز غمش در سر تماشاییست</p></div>
<div class="m2"><p>تو گویی از لب هر بام سرزده تبخال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو غنچه کیسهٔ لب بستگان تهی نبود</p></div>
<div class="m2"><p>به فکر زر مخروش و برای مال منال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جنبش مژه چشمش به گفتگو آمد</p></div>
<div class="m2"><p>چه نکته ها که ادا کرده با زبان خیال</p></div></div>