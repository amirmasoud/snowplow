---
title: >-
    شمارهٔ ۶۵۹
---
# شمارهٔ ۶۵۹

<div class="b" id="bn1"><div class="m1"><p>می به جام گل از آن رخسار زیبا ریختند</p></div>
<div class="m2"><p>رنگ سرو از سایهٔ آن قد رعنا ریختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلوت دل روشن از فیض ریاضت می شود</p></div>
<div class="m2"><p>شمع این بزم از گداز پیکر ما ریختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواستم تا در خیال آرم شکوه عشق را</p></div>
<div class="m2"><p>چشم چون برهم زدم در قطره دریا ریختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرم افشای حقیقت مهر لب شد هر قدر</p></div>
<div class="m2"><p>صاف منصوری به جام طاقت ما ریختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش نگاهان امشب از هر جنبش مژگان شوخ</p></div>
<div class="m2"><p>ترکش تیری مرا در سینه یکجا ریختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روشن از کیفیت معنی است بزم اینجا، مگر</p></div>
<div class="m2"><p>از گداز شیشهٔ دل شمع مینا ریختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کم کسی از آن حسن نیم رنگش آگهست</p></div>
<div class="m2"><p>معنیی بود اینکه جویا در دل ما ریختند</p></div></div>