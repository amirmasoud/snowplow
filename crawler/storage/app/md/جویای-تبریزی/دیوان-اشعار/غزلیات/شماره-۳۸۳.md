---
title: >-
    شمارهٔ ۳۸۳
---
# شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>آنکه کوه درد را بر آه بی بنیاد بست</p></div>
<div class="m2"><p>از نفس مشت غبار جسم را برباد بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می تواند نالهٔ دل را به گوش او رساند</p></div>
<div class="m2"><p>درد را از رشتهٔ آه آنکه بر فریاد بست</p></div></div>