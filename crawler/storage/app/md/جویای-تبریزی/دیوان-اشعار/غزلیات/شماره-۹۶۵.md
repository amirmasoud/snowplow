---
title: >-
    شمارهٔ ۹۶۵
---
# شمارهٔ ۹۶۵

<div class="b" id="bn1"><div class="m1"><p>ساعد او می زند بر شمع کافور آستین</p></div>
<div class="m2"><p>دست حسنش می فشاند بر رخ حور آستین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه روشن گشته بر دستم چراغ داغ عشق</p></div>
<div class="m2"><p>شد بسان پردهٔ فانوس پرنور آستین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود چون صبح روشن از فراغ روی او</p></div>
<div class="m2"><p>بر چراغ مرده افشاند گر از دور آستین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از لباس ظاهری یک ذره ای باطن بگیر!‏</p></div>
<div class="m2"><p>دست تا موجود باشد نیست منظور آستین</p></div></div>