---
title: >-
    شمارهٔ ۲۷۴
---
# شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>این ماه چارده که ترا در برابر است</p></div>
<div class="m2"><p>حقا که پیش روی تو نادر برابر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افشای عیب خود نکنی روبروی خلق</p></div>
<div class="m2"><p>چون معصیت کنی که خدا در برابر است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیینه خانه ای است جهان موسم بهار</p></div>
<div class="m2"><p>هر سو که نگریم هوا در برابر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای حق طلب ز دامن حیدر مدار دست</p></div>
<div class="m2"><p>آیینهٔ خدای نما در برابر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن دولتی که در طلبش عمرها گذشت</p></div>
<div class="m2"><p>جویا هزار شکر مرا در برابر است</p></div></div>