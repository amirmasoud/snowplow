---
title: >-
    شمارهٔ ۱۰۱۲
---
# شمارهٔ ۱۰۱۲

<div class="b" id="bn1"><div class="m1"><p>مستغنی از می آیم ز جان نگاه او</p></div>
<div class="m2"><p>ما را بس است کاسهٔ چشم سیاه او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ارباب جستجوی، به راهش سپرده اند</p></div>
<div class="m2"><p>آن را که نیست دیدهٔ بینش به راه او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدمستیی که چشم تو دیشب به کار برد</p></div>
<div class="m2"><p>باشد زبان هر مژه ات عذرخواه او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نسبتی که هست به روی تو ماه را</p></div>
<div class="m2"><p>بر آسمان فخر بر قصد کلاه او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دایم به ناز بالش راحت بداده پشت</p></div>
<div class="m2"><p>هر دل که نوک آن مژه شد تکیه گاه او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محروم مانده آنکه ز فیض انابت است</p></div>
<div class="m2"><p>جویا بس است جرم نکردن گناه او</p></div></div>