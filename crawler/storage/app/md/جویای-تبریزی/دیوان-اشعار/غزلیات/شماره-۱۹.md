---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>ایمن از جور حوادث ساخت عجز من مرا</p></div>
<div class="m2"><p>شیشه جانیها حصاری گشت از آهن مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نباشم خوشدل از بیتابیی کز درد اوست</p></div>
<div class="m2"><p>صبح نوروزیست هر چاکی ز پیراهن مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتشم؛ آتش، مکن تکلیف سیر گلشنم</p></div>
<div class="m2"><p>پر فشانیهای بلبل می زند دامن مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر نمی تابد مزاج نازکم گلگشت باغ</p></div>
<div class="m2"><p>بی دماغ از نکهت گل می کند گلشن مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پردهٔ فانوس نتواند حجاب شمع شد</p></div>
<div class="m2"><p>کی نهان در سینه می ماند دل روشن مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بار سنگین گریبان برنتابد وحشتم</p></div>
<div class="m2"><p>در جنون جویا چو صحرا بس بود دامن مرا</p></div></div>