---
title: >-
    شمارهٔ ۴۹۰
---
# شمارهٔ ۴۹۰

<div class="b" id="bn1"><div class="m1"><p>ترک همره رهنمای کوی ماه من بود</p></div>
<div class="m2"><p>چشم پوشیدن ز مردم شمع راه من بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که چشم عیب بینی را نمی پوشم زخلق</p></div>
<div class="m2"><p>سر زند از هر که تقصیری گناه من بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شمیم غنچه از خوناب دل جوشیده ام</p></div>
<div class="m2"><p>سرو رنگین جلوه ای گر هست آه من بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به ظاهر زان گل رو چشم می پوشم ولی</p></div>
<div class="m2"><p>هر بن مژگان کمینگاه نگاه من بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چرا لرزم به خویش از آفتاب روز حشر</p></div>
<div class="m2"><p>سایهٔ آل عبا جویا پناه من بود</p></div></div>