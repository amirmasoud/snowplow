---
title: >-
    شمارهٔ ۵۶۶
---
# شمارهٔ ۵۶۶

<div class="b" id="bn1"><div class="m1"><p>زورآوران که پنجهٔ افلاک می برند</p></div>
<div class="m2"><p>در کاسهٔ امل چه به جز خاک می برند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنانکه شبنم گل شب زنده داری اند</p></div>
<div class="m2"><p>فیض سحر به دیدهٔ نمناک می برند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنجا که نوش داروی شادی دهند ساز</p></div>
<div class="m2"><p>البته نسخه از ورق تاک می برند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پروانه طینتان که به شمع یقین رسند</p></div>
<div class="m2"><p>بال پرش ز شعلهٔ ادراک می برند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردان به روی خاک سربندگی نهند</p></div>
<div class="m2"><p>زان پیشتر که سر به ته خاک می برند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیینه خاطران که به روی تو واله اند</p></div>
<div class="m2"><p>بس فیضها که از نظر پاک می برند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زیر خاک واصل دریای رحمت اند</p></div>
<div class="m2"><p>آنانکه چون حباب دل پاک می برند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بهر قوت وقت سرانجام راه را</p></div>
<div class="m2"><p>رشندلان ز شعلهٔ ادراک می برند</p></div></div>