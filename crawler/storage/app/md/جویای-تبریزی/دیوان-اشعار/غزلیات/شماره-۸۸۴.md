---
title: >-
    شمارهٔ ۸۸۴
---
# شمارهٔ ۸۸۴

<div class="b" id="bn1"><div class="m1"><p>به او نزدیکم و از شرم خود را دور پندارم</p></div>
<div class="m2"><p>وصالش داده دست و خویش را مهجور پندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درونم شد نمکسود ملاحت بسکه از حسنش</p></div>
<div class="m2"><p>فشارم چون به دل دندان کاب شور پندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا در دل زبس امیدها در یکدگر جوشد</p></div>
<div class="m2"><p>فضای سینه ات را محشر زنبور پندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان دانسته چشمش سرگردانی می کند با من</p></div>
<div class="m2"><p>که او مست می ناز است و من مخمور پندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس ضعف تنم قوت گرفت از در هجر من</p></div>
<div class="m2"><p>ملایم طبعی معشوق را هم زور پندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اثر کرده است درد بی دوایم بسکه گلشن را</p></div>
<div class="m2"><p>دهان خندهٔ هر غنچه را ناسور پندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جدا زان نشتر مژگان چنان در ناله می آید</p></div>
<div class="m2"><p>که شریان را به تن جویا رگ طنبور پندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زخم تن از تیغ صیقل کردهٔ جانانه ام</p></div>
<div class="m2"><p>همچو فانوس گلین شد شمع خلوتخانه ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاف یکرنگی زنم با دشمن از روشندلی</p></div>
<div class="m2"><p>چون شرار از دودهٔ برق است گویی دانه ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو خشت هم که زور باده اش دور افکند</p></div>
<div class="m2"><p>شب ز جوش بزم از جا رفت سقف خانه ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جام امید است در خمیازهٔ صاف مراد</p></div>
<div class="m2"><p>بر لبم نه لب لبالب از می پیمانه ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پای تا سر بسکه داغش کرد رشک عزلتم</p></div>
<div class="m2"><p>جلوهٔ طاووس دارد جغد در ویرانه ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از زمین جویا نشد هرگز شررواری بلند</p></div>
<div class="m2"><p>در نهاد سنگ بودی کاش پنهان دانه ام</p></div></div>