---
title: >-
    شمارهٔ ۲۶۱
---
# شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>تا تو رفتی صحن گلشن کشت آفت دیده است</p></div>
<div class="m2"><p>نکهت گل در هوا ابری زهم پاشیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرنه طاق ابرو مردانه اش را دیده است</p></div>
<div class="m2"><p>از خجالت خویش را محراب چون دزدیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیشهٔ افلاک ترسم بیند آسیب شکست</p></div>
<div class="m2"><p>بسکه از بویش هوا بر خویشتن بالیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سیاهی از سر داغم نیفتد دور نیست</p></div>
<div class="m2"><p>تخم این گل خال او در سینه ام پاشیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد غبار خاطر آخر خاک ما غمدیدگان</p></div>
<div class="m2"><p>جای دارم در دل او تا زمن رنجیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز از شادی نمی آید لب زخمم بهم</p></div>
<div class="m2"><p>تا دهن غنچهٔ پیکان او بوسیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گریبان غنچه سان جویا نیارد سر برون</p></div>
<div class="m2"><p>عکس رخسار تو در آیینهٔ دل دیده است</p></div></div>