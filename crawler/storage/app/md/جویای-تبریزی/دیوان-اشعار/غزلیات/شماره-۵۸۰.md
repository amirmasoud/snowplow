---
title: >-
    شمارهٔ ۵۸۰
---
# شمارهٔ ۵۸۰

<div class="b" id="bn1"><div class="m1"><p>لاله آسا هر که در عشق تو خون آشام شد</p></div>
<div class="m2"><p>از حلاوت پای تا سر یک دهن چون جام شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد مردن هم به راه انتظار ناوکش</p></div>
<div class="m2"><p>استخوانم پای تا سر چشم چون بادام شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوهرش چون نبض عاشق بال بیتابی زند</p></div>
<div class="m2"><p>بسکه دور از عکس او آیینه بی آرام شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زآتش غم سوختند از بس به تن گل‌های داغ</p></div>
<div class="m2"><p>پوست بر اعضا اسیران ترا گلدام شد</p></div></div>