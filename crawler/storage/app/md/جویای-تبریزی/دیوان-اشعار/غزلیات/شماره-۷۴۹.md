---
title: >-
    شمارهٔ ۷۴۹
---
# شمارهٔ ۷۴۹

<div class="b" id="bn1"><div class="m1"><p>معنیی گر هست با رند می آشام است و بس</p></div>
<div class="m2"><p>چشم بیداری و به عالم گر بود جام است و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جاده ها دور از خرامش سینه چاک افتاده اند</p></div>
<div class="m2"><p>کامیاب از پای بوس او لب بام است و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر دولت بگذرانی از چه بی آرام عمر</p></div>
<div class="m2"><p>دولتی گر هست عمر من در آرام است و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قامتش از شیوه های دلبری مجموعه ایست</p></div>
<div class="m2"><p>سرو را رعنایی از بالای اندام است و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده نوشی بیشتر دل را گرفتارت کند</p></div>
<div class="m2"><p>خط جام امشب به چشمم حلقهٔ دام است و بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سعی بیجا خلق را قفل در روزی بود</p></div>
<div class="m2"><p>بستگی در کارها جویا ز ابرام است و بس</p></div></div>