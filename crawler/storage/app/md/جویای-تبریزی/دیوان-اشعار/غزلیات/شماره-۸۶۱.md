---
title: >-
    شمارهٔ ۸۶۱
---
# شمارهٔ ۸۶۱

<div class="b" id="bn1"><div class="m1"><p>بی تو بیگانه چو عکس رخت از هوش خودم</p></div>
<div class="m2"><p>با تو چون جوهر آیینه فراموش خودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست کیفیت خود گشته ام از دولت عشق</p></div>
<div class="m2"><p>بیخود از نشئهٔ توحید و قدح نوش خودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاصل محو خیالی است پریشان مغزی</p></div>
<div class="m2"><p>جام حیرت نگهم بیخود سرجوش خودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنج عزلت بودم غنچه صفت سینهٔ تنگ</p></div>
<div class="m2"><p>هست دلبستگیی با لب خاموش خودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه دارد به تنم رنگ تو هر قطرهٔ خون</p></div>
<div class="m2"><p>غنچه آسا به خیال تو در آغوش خودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد زبانست مرا در دل خونین پنهان</p></div>
<div class="m2"><p>غنچهٔ رازم و مهر لب خاموش خودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق قمری‌صفت افکنده به گردن جویا</p></div>
<div class="m2"><p>حلقهٔ بندگی سرو قباپوش خودم</p></div></div>