---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>از صفای خاطر ما هیچکس آگاه نیست</p></div>
<div class="m2"><p>عکس را در خلوت آیینهٔ ما راه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فارغ از قید عبادت نیست در عالم دلی</p></div>
<div class="m2"><p>این نگین را نقش شیر از بندهٔ درگاه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قامت خم بسته از حرص آنکه از طول امل</p></div>
<div class="m2"><p>رشتهٔ قلاب صید مطلبش کوتاه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغ خواری لازم روی طلب افتاده است</p></div>
<div class="m2"><p>دعوی ما را دلیل روشنی چون ماه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر طرف رو می کنی جویا به مقصد می رسی</p></div>
<div class="m2"><p>در ره از خویش رفتن هیچکس گمراه نیست</p></div></div>