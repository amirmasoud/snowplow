---
title: >-
    شمارهٔ ۹۱۳
---
# شمارهٔ ۹۱۳

<div class="b" id="bn1"><div class="m1"><p>شبی کز یاد آن مه بر جگر دندان بیفشارم</p></div>
<div class="m2"><p>چکد از دیده ام سیاره چون مژگان بیفشارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرم چون جاده باشد در کنار دامن منزل</p></div>
<div class="m2"><p>اگر پا در طریق طاعت یزدان بیفشارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کم از معنی ندانند اهل معنی لفظ رنگین را</p></div>
<div class="m2"><p>به بر جسم لطیفش را به شوق جان بیفشارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا با پنجهٔ سنگین دلی افشرده ای چندان</p></div>
<div class="m2"><p>ترا در خلوت آغوش صد چندان بیفشارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بزم امتحان چون دست عجزم پنجه ور گردد</p></div>
<div class="m2"><p>توانم بیضهٔ فولاد را آسان بیفشارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من تر دامن از فیض ولای ساقی کوثر</p></div>
<div class="m2"><p>نشانم آتش صد دوزخ ار دامان بیفشارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مذاقم کامیاب لقمهٔ بی شبه می گردد</p></div>
<div class="m2"><p>دمی کز جور گردون بر جگر دندان بیفشارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمین چون آسمان جویا به رقص شوق می آید</p></div>
<div class="m2"><p>دل خود را اگر در ساغر دوران بیفشارم</p></div></div>