---
title: >-
    شمارهٔ ۶۶۲
---
# شمارهٔ ۶۶۲

<div class="b" id="bn1"><div class="m1"><p>صید خلق اهل ریا در گوشه گیری دیده اند</p></div>
<div class="m2"><p>دامن خود را از آن چون دام ماهی چیده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مولوی پر تکیه بر علمت مکن پرگاروار</p></div>
<div class="m2"><p>آهنین پایان در این ره بیشتر لغزیده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدر خود بنگر به میزان تمیز اهل طبع</p></div>
<div class="m2"><p>مردم موزون سراسر مردم سنجیده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشه گیران فارغند از حلقهٔ اهل ریا</p></div>
<div class="m2"><p>خویش را مانند محراب از میان دزدیده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم به دل باشند اسباب تعلق برده را</p></div>
<div class="m2"><p>همچو گل تا بوده اند آزادگان خندیده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش اهل دید جویا خودستایان را بس است</p></div>
<div class="m2"><p>لاف فهمیدن دلیل آنکه نافهمیده اند</p></div></div>