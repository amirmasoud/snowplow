---
title: >-
    شمارهٔ ۶۵۸
---
# شمارهٔ ۶۵۸

<div class="b" id="bn1"><div class="m1"><p>غنچه مانند این دهن باشد</p></div>
<div class="m2"><p>چون دهان تو بی سخن باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی بوحدت بریم از کثرت</p></div>
<div class="m2"><p>خلوت ما در انجمن باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل داغ تو بر زمین دلم</p></div>
<div class="m2"><p>باغ باغ و چمن چمن باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیر و دورم ز بس سبکروحی</p></div>
<div class="m2"><p>بوی گل وار در وطن باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه با ترزبانی سرشار</p></div>
<div class="m2"><p>پیش نطق تو بی دهن باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدر شکر شکست زو جویا</p></div>
<div class="m2"><p>لب لعلش شکرشکن باشد</p></div></div>