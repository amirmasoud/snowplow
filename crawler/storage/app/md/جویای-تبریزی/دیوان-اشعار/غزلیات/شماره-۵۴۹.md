---
title: >-
    شمارهٔ ۵۴۹
---
# شمارهٔ ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>پیش رخت مراست دل داغ داغ بند</p></div>
<div class="m2"><p>پروانه را بس است فروغ چراغ بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یاری نسیم سحر عطسه ریز شد</p></div>
<div class="m2"><p>تا صبح بود غنچهٔ گل را دماغ بند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دست در چمن نگذارم پیاله را</p></div>
<div class="m2"><p>گویی چو نرگسم شده در کف ایاغ بند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزاد کردهٔ غم عشقست خاطرم</p></div>
<div class="m2"><p>باشد مرا ز شغل محبت فراغ بند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جویا روم به سیر چمن همره نسیم</p></div>
<div class="m2"><p>گر باغبان نهاده به درهای باغ بند</p></div></div>