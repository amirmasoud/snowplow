---
title: >-
    شمارهٔ ۷۷۳
---
# شمارهٔ ۷۷۳

<div class="b" id="bn1"><div class="m1"><p>ای دل هم آرمیده و هم می رمیده باشد</p></div>
<div class="m2"><p>آه به باد رفته و اشک چکیده باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمعیت دل ار طلبی راه درد گیر</p></div>
<div class="m2"><p>یعنی به رنگ غنچه گریبان دریده باش</p></div></div>