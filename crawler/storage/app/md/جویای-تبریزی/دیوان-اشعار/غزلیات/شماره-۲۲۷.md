---
title: >-
    شمارهٔ ۲۲۷
---
# شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>هر دلی آیینه دار صورت اندیشه ای است</p></div>
<div class="m2"><p>این پری هر دم برنگ تازه ای در شیشه ای است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم از عمر سبکسیر تو قدری کم شود</p></div>
<div class="m2"><p>آمد و رفت نفس در کندن جان تیشه ای است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه از آهم مکدر شد هوای گلستان</p></div>
<div class="m2"><p>هر رگ گل گوییا در خاک پنهان ریشه ای است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینقدرها در پی آزار خاطرها مکوش</p></div>
<div class="m2"><p>جان من دل در فضای سینه شیر بیشه ای است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که می پرسی ز احوال دل جویا مپرس</p></div>
<div class="m2"><p>دردمند عاشقی بیچاره ای غم پیشه ای است</p></div></div>