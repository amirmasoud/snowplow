---
title: >-
    شمارهٔ ۹۸۳
---
# شمارهٔ ۹۸۳

<div class="b" id="bn1"><div class="m1"><p>چراغ دودهٔ مجد و علا امام حسین</p></div>
<div class="m2"><p>فروغ دیدهٔ شیر خدا امام حسین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غریق لجهٔ کرب و بلا، شه مظلوم</p></div>
<div class="m2"><p>شهید تشنه لب کربلا امام حسین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی چشیدی از اشرار ز هر محنت و غم</p></div>
<div class="m2"><p>بسی کشیدی از اغیار یا امام حسین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چها رسید ز بیداد و جو زادهٔ هند</p></div>
<div class="m2"><p>به پارهٔ جگر مصطفی امام حسین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فروغ عالم امکان ز نور گوهر اوست</p></div>
<div class="m2"><p>چراغ انجمن کبریا امام حسین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غبار مرقد او نور چشم عافیت است</p></div>
<div class="m2"><p>بس است درد دلم را دوا امام حسین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدار چم نکویی ز آسمان جویا</p></div>
<div class="m2"><p>ببین که چرخ چها کرد با امام حسین</p></div></div>