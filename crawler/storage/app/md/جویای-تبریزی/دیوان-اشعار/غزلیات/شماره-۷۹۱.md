---
title: >-
    شمارهٔ ۷۹۱
---
# شمارهٔ ۷۹۱

<div class="b" id="bn1"><div class="m1"><p>هر سبزه سراپای زبانست در این باغ</p></div>
<div class="m2"><p>هر شبنمی از دیده ورانست در این باغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چشم گشوده است بود بیخود حیرت</p></div>
<div class="m2"><p>نرگس که ز صاحب نظرانست در این باغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرسبزی گلها نه ز باران بهاریست</p></div>
<div class="m2"><p>شد بی تو دلم آب و روانست در این باغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی او نه همین غنچه خورد خون دل از غم</p></div>
<div class="m2"><p>گل نیز ز خمیازه کشانست در این باغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نیم تبسم ز لب غنچه کند گل</p></div>
<div class="m2"><p>رازی که بصد پرده نهانست در این باغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد گرم طپش هر رگ گل چون دل بلبل</p></div>
<div class="m2"><p>تا مرغ دلم بال فشانست در این باغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاصیت سیماب اگر نیست بشبنم</p></div>
<div class="m2"><p>چون گوش گل امروز گرانست در این باغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب است که روح گل و جان تن خاکیست</p></div>
<div class="m2"><p>هر جوی که جاریست روانست در این باغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از جلوهٔ او دیدهٔ بد دور که دیده است</p></div>
<div class="m2"><p>جز قد تو سروی که روانست در این باغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امروز بوصف گل و سنبل دل جویا</p></div>
<div class="m2"><p>چون غنچه سراپای زبانست در این باغ</p></div></div>