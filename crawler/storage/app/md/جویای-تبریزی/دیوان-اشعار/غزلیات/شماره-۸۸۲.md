---
title: >-
    شمارهٔ ۸۸۲
---
# شمارهٔ ۸۸۲

<div class="b" id="bn1"><div class="m1"><p>حباب آسا فتد از دیدهٔ تر قطره اشکم</p></div>
<div class="m2"><p>ز بس آهی نهان گردیده در هر قطره اشکم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفت از بس دلم از سردمهریهای او امشب</p></div>
<div class="m2"><p>بدامن بسته می ریزد چو گوهر قطرهٔ اشکم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشم خویش دیدم امتزاج آب و آتش را</p></div>
<div class="m2"><p>برون آورده تا از چاک دل سر قطرهٔ اشکم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن طبلی که خورد از دل تپیدن در شب هجران</p></div>
<div class="m2"><p>پرد از شاخ مژگان چون کبوتر قطرهٔ اشکم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس اندوخت فیض نور از طور دلم جویا</p></div>
<div class="m2"><p>در گوش مه و خورشید شد هر قطرهٔ اشکم</p></div></div>