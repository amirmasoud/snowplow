---
title: >-
    شمارهٔ ۸۵۷
---
# شمارهٔ ۸۵۷

<div class="b" id="bn1"><div class="m1"><p>کبوتر را ره آگاهی از احوال خود بستم</p></div>
<div class="m2"><p>که حسرت نامه ام را بر شکست بال خود بستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن از عرش گوید مرغ دل تا آستانش را</p></div>
<div class="m2"><p>به سرو قامت آه بلند اقبال خود بستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رنگ صبح کی افسرده از تهمت پیری</p></div>
<div class="m2"><p>کمر از نور فیض جام مالامال خود بستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخجلت خویشتن را از دل خود هم نهان دارم</p></div>
<div class="m2"><p>در این آیینه ره بر صورت احوال خود بستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس رخساره ام در گرد کلفت شد نهان جویا</p></div>
<div class="m2"><p>به روی آینه دیواری از تمثال خود بستم</p></div></div>