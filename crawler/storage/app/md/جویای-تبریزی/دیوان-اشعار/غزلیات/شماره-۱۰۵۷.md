---
title: >-
    شمارهٔ ۱۰۵۷
---
# شمارهٔ ۱۰۵۷

<div class="b" id="bn1"><div class="m1"><p>ز هجرت پیکرم خواب فراموش است پنداری</p></div>
<div class="m2"><p>وجودم نالهٔ لبهای خاموش است پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدم با آنکه از پیری دو تا گردیده است، اما</p></div>
<div class="m2"><p>ز شوقش چون کمال سرتاسر آغوش است پنداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشد پژمرده از دم سردی دی غنچهٔ طبعم</p></div>
<div class="m2"><p>بهار تو جوانیهایش در جوش است پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز فیض باده هر مو بر تنم رنگین زبانی شد</p></div>
<div class="m2"><p>به بیهوشی قسم سرمایهٔ هوش است پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به شوق نکته ای کز غنچهٔ او سر زند جویا</p></div>
<div class="m2"><p>سراپای دلم مانند گل، گوش است پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی ام من؟ عاشق غم دیدهٔ بسیار محزونی</p></div>
<div class="m2"><p>نگه دیوانه ای، کاکل اسیری، زلف مفتونی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکست قلب عاشق راست در طالع مگر امشب</p></div>
<div class="m2"><p>لبت از پشتی خط می زند بر دل شبیخونی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شراب آن نگه در جام طاقت هر کراریزی</p></div>
<div class="m2"><p>چو خم بی دست و پا گردد اگر باشد فلاطونی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه تنها جاده می غلتد بخاک از طرز رفتارش</p></div>
<div class="m2"><p>بود هر نقش پا دنبال سروش چشم پرخونی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشد کهسار را جویا سر زنجیر افغانت</p></div>
<div class="m2"><p>مگر در قرنها خیزد ز صحرا چون تو مجنونی</p></div></div>