---
title: >-
    شمارهٔ ۴۵۳
---
# شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>غمم را واله شیرین و لیلا برنمی تابد</p></div>
<div class="m2"><p>که سیل گریه ام را کوه و صحرا برنمی تابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل چاکش به رنگ غنچه در جیب و بغل ریزد</p></div>
<div class="m2"><p>می زورین ما را ظرف مینا برنمی تابد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل پراضطراب من زضبط اشک تنگ آمد</p></div>
<div class="m2"><p>شکوه گوهرم را شور دریا برنمی تابد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیاد آرزو مانند گل از یکدگر باشد</p></div>
<div class="m2"><p>دل آزرده ام بار تمنا برنمی تابد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلعلش نوش دارویی مگر گردد روان بخشم</p></div>
<div class="m2"><p>مریض درد او ناز مسیحا برنمی تابد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخواهم گر همه در دست جویا از فلک هرگز</p></div>
<div class="m2"><p>دل وارسته ام ننگ تقاضا برنمی تابد</p></div></div>