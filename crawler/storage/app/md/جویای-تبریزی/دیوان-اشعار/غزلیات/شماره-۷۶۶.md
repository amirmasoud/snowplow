---
title: >-
    شمارهٔ ۷۶۶
---
# شمارهٔ ۷۶۶

<div class="b" id="bn1"><div class="m1"><p>چنان کز قهر او مانند صهبا آب شد آتش</p></div>
<div class="m2"><p>چو جام باده از لطفش گل سیراب شد آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرت گردم چه باشد اینکه در پیمانه می ریزی</p></div>
<div class="m2"><p>ز خوی گرمت آتش آب شد، یا آب شد آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان اضطراب شمع یعنی شعله می گوید</p></div>
<div class="m2"><p>زعکس آفتاب عارضی بیتاب شد آتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فکمند افسانهٔ سوز و گدازم شور در عالم</p></div>
<div class="m2"><p>میان سنگ آندم کز شرر در خواب شد آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز فیض عکس رخساری بود جویا در این دریا</p></div>
<div class="m2"><p>به رنگ شعلهٔ جواله گر گرداب شد آتش</p></div></div>