---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>گرچه باشد در بر ما دلبر می نوش ما</p></div>
<div class="m2"><p>نیست جز ما چون کمان حلقه در آغوش ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچگه آواز بوی غنچه ای نشنیده ای؟</p></div>
<div class="m2"><p>غافلی از جوش فریاد لب خاموش ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست خورشید اینکه صبح و شام بینی بر افق</p></div>
<div class="m2"><p>کف به لب می آورد دیگ فلک از جوش ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه غیر از غنچه اش حرفی زکس نشنیده ایم</p></div>
<div class="m2"><p>همچو گل لبریز رنگ و بوست دایم گوش ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پی وحشی غزالی بسکه از خود رفته ایم</p></div>
<div class="m2"><p>نوش بر دوش رم عنقاست جویا هوش ما</p></div></div>