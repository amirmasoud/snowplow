---
title: >-
    شمارهٔ ۲۴۳
---
# شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>مانده در جهل مرکب آنکه لوحش ساده است</p></div>
<div class="m2"><p>تیره بودن لازم چشم سفید افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصل باشد لازمش حرمان، که گردیده سفید</p></div>
<div class="m2"><p>چشم روزن تا به روی صبحدم افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزم می دانند سربازان همت رزم را</p></div>
<div class="m2"><p>پیش ما شمشیر خون آلود موج باده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم او پر دل چرا نبود پی خون ریز خلق</p></div>
<div class="m2"><p>هر که را دیدیم در عالم به او دل داده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشد از بس رفته ام در بزم حیرانی ز خویش</p></div>
<div class="m2"><p>صفحهٔ تصویر من هر جا که لوح ساده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمش از مژگان دو ترکش بسته جویا خیر باد!‏</p></div>
<div class="m2"><p>ترک بی باکی سیه مستی به جنگ آماده است</p></div></div>