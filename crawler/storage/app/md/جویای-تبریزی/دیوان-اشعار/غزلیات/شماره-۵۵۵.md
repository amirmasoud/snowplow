---
title: >-
    شمارهٔ ۵۵۵
---
# شمارهٔ ۵۵۵

<div class="b" id="bn1"><div class="m1"><p>با بار آن گل رو دل بی حجاب باشد</p></div>
<div class="m2"><p>زان روی با سرشکم بوی گلاب باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شاهی از فقیری است دارد نمود بی بود</p></div>
<div class="m2"><p>پست و بلند دنیا موج سراب باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آینه ز شوق رخسار با صفایش</p></div>
<div class="m2"><p>جوهر چو موج دریا در اضطراب باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر قطرهٔ سرشکم گشته محیط آهی</p></div>
<div class="m2"><p>چشمم شب فراقت چشم حباب باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پوشیده کی بماند کلفت زصاف باطن</p></div>
<div class="m2"><p>چینهای موج جویا بر روی آب باشد</p></div></div>