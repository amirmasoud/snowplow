---
title: >-
    شمارهٔ ۷۰۴
---
# شمارهٔ ۷۰۴

<div class="b" id="bn1"><div class="m1"><p>ز رنگ چهره ام بوی بهار درد می آید</p></div>
<div class="m2"><p>نفس تا می کشم از سینه آه سرد می آید</p></div></div>