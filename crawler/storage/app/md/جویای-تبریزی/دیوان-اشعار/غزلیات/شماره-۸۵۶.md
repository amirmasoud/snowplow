---
title: >-
    شمارهٔ ۸۵۶
---
# شمارهٔ ۸۵۶

<div class="b" id="bn1"><div class="m1"><p>بی او ز خون ناب دماغی چو تر کنیم</p></div>
<div class="m2"><p>دل را کباب اخگر لخت کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چین الم به ابروی موج هوا فتد</p></div>
<div class="m2"><p>طومار شکوه ات گر از آه سحر کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی مروتی که من و دل ز خویشتن</p></div>
<div class="m2"><p>دستی به دست هم بدهیم و سفر کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر نشتری که آن مژه در دیده بشکند</p></div>
<div class="m2"><p>از دیده برگرفته بکار جگر کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن بلبلیم ما که به شوق تو غنچه وار</p></div>
<div class="m2"><p>رنگین میان بیضه ز خون بال و پر کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل را به یاد شوخی مژگان، شب فراق</p></div>
<div class="m2"><p>تا صبحگاه تکیه گه نیشتر کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جویا مآل کردهٔ ما را زما مپرس</p></div>
<div class="m2"><p>تخمی نکشته ایم که فکر ثمر کنیم</p></div></div>