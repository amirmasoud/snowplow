---
title: >-
    شمارهٔ ۳۶۱
---
# شمارهٔ ۳۶۱

<div class="b" id="bn1"><div class="m1"><p>کجا روم که به دردم رسید و هیچ نگفت</p></div>
<div class="m2"><p>فغان که نالهٔ زارم شنید و هیچ نگفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو دید شوخی شبنم ببرگ گل در باغ</p></div>
<div class="m2"><p>لب از حجاب به دندان گزید و هیچ نگفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفتمش سر راهی به خاک و خون غلطان</p></div>
<div class="m2"><p>رسید بر سرم، آهی کشید و هیچ نگفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگفتمش که کباب نگاه کیست دلم</p></div>
<div class="m2"><p>به خنده جا نب من گرم دید و هیچ نگفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شنید شکوهٔ اغیار را ز من جویا</p></div>
<div class="m2"><p>ز غیر خاطرش از جان رمید و هیچ نگفت</p></div></div>