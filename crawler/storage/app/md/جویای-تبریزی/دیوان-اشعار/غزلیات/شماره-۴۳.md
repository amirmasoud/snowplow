---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>چسان عنقا کند با هوشم آهنگ پریدن‌ها</p></div>
<div class="m2"><p>که ریزد سستی پرواز او رنگ تپیدن‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عزم چیدن گل تا خرامت گلشن‌آرا شد</p></div>
<div class="m2"><p>بود از شوق دستت غنچه دلتنگ نچیدن‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی‌زیبد تواضع از تو با این قامت رعنا</p></div>
<div class="m2"><p>که زیبا نیست جیب سرو در چنگ خمیدن‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چمن پیرا شود او با لب میگون اگر روزی</p></div>
<div class="m2"><p>کند گل هم به رنگ غنچه آهنگ مکیدن‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز وحشت گشت با عنقا دلم هم آشیان جویا</p></div>
<div class="m2"><p>بود بر تخت، این دیوانه را ننگ رمیدن‌ها</p></div></div>