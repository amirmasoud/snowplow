---
title: >-
    شمارهٔ ۷۲۶
---
# شمارهٔ ۷۲۶

<div class="b" id="bn1"><div class="m1"><p>نباشد غیر عشق نیکوانم پیشهٔ دیگر</p></div>
<div class="m2"><p>به غیر از وصل خوبانم به دل اندیشهٔ دیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجز فکر وصال او که یارب باد روزافزون</p></div>
<div class="m2"><p>به وصل او که نبود در دلم اندیشهٔ دیگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر از تهمت یک شیشهٔ می محتسب ترسی</p></div>
<div class="m2"><p>توانم بر تو بستن هر نفس صد شیشهٔ دیگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان باشد چراگاه غزالان هوس جویا</p></div>
<div class="m2"><p>بود ماوای ما شیران همت بیشهٔ دیگر</p></div></div>