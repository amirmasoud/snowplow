---
title: >-
    شمارهٔ ۲۶۵
---
# شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>سوختن شمع شبستان زمن آموخته است</p></div>
<div class="m2"><p>گل ز رخسار تو افروختن آموخته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هوای تو سبک سیرتر از بوی گلی م</p></div>
<div class="m2"><p>به غریبی دل ما در وطن آموخته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر زمین ساغر سرشار به کف غلطیدن</p></div>
<div class="m2"><p>مست لایعقل من از چمن آموخته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست بی فایده در پیش تو استادن سرو</p></div>
<div class="m2"><p>از تو دامن به میان برزدن آموخته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش از ایجاد جهان بی سر و پا می گشتیم</p></div>
<div class="m2"><p>آسمان بیهوده گردی ز من آموخته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>‏ بی تکلف ز شکر ریزی صائب جویا</p></div>
<div class="m2"><p>طوطی نطق تو طرز سخن آموخته است</p></div></div>