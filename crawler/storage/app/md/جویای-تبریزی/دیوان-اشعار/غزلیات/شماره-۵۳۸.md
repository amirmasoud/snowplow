---
title: >-
    شمارهٔ ۵۳۸
---
# شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>گر صبا با نکهت زلفش سوی هامون شود</p></div>
<div class="m2"><p>نافهٔ آهو چو داغ لاله غرق خون شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که مجنون می تواند بود در فصل بهار</p></div>
<div class="m2"><p>بوالعجب دیوانه ای باشد که افلاطون شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکهت گل شد پر پرواز بر روی هوا</p></div>
<div class="m2"><p>هر که امروز آمد از خلوت برون مجنون شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از فشارش در نهاد سنگ خون گردد شرار</p></div>
<div class="m2"><p>حال دان زان پنجهٔ مژگان ندانم چون شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه همین از رفتنش پیراهن گل شد قبا</p></div>
<div class="m2"><p>دور از او هر غنچه ای جویا دل محزون شود</p></div></div>