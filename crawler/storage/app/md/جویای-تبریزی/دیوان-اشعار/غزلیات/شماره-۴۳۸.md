---
title: >-
    شمارهٔ ۴۳۸
---
# شمارهٔ ۴۳۸

<div class="b" id="bn1"><div class="m1"><p>چنان از اضطرابم خوش دل آن خودکام می گردد</p></div>
<div class="m2"><p>که از گردیدن حالم به بزمش جام می گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نصیبی بهر سروستان جنت تا به دست آرد</p></div>
<div class="m2"><p>رعونت روز و شب برگرد آن اندام می گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی لب را تکلم آشنا گردان سرت گردم</p></div>
<div class="m2"><p>خموشی چون شود از حد فزون ابرام می گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارد راه قاصد در حریم خاص یکرنگی</p></div>
<div class="m2"><p>میان ما و جانان خود به خود پیغام می گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آهنگ تو بلبل سر کند گر ناله ای جویا</p></div>
<div class="m2"><p>رگ گل همچو نبض خسته بی آرام می گردد</p></div></div>