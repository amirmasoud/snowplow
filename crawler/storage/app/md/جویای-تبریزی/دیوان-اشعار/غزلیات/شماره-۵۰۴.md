---
title: >-
    شمارهٔ ۵۰۴
---
# شمارهٔ ۵۰۴

<div class="b" id="bn1"><div class="m1"><p>میان او که در اندیشه در نمی آید</p></div>
<div class="m2"><p>عجب نباشد اگر در نظر نمی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه اضطراب که خورشید را به تن نفکند</p></div>
<div class="m2"><p>کسی به شوخی حسن تو برنمی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو هر قدر که شوی شوخ و شنگ معذوری</p></div>
<div class="m2"><p>که پاس خویش ز غلطان گهر نمی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یک نگاه کند با دل آنچه هر مژه اش</p></div>
<div class="m2"><p>هزار سال زصد نیشتر نمی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببین نزاکت موی میان او جویا</p></div>
<div class="m2"><p>که همچو تار نگه در نظر نمی آید</p></div></div>