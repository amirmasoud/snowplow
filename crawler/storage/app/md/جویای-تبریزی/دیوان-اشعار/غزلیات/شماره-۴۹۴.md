---
title: >-
    شمارهٔ ۴۹۴
---
# شمارهٔ ۴۹۴

<div class="b" id="bn1"><div class="m1"><p>حسن صوتی آتش افروز دل من می شود</p></div>
<div class="m2"><p>این چراغ از شعلهٔ آواز روشن می شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می توان در رفتن از خود بیشتر برداشت فیض</p></div>
<div class="m2"><p>مرغ را زان دست در پرواز دامن می شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با حریف تندخو دایم خموشی پیشه ام</p></div>
<div class="m2"><p>آنچه از تمکین او کم گردد از من می شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سختی و نرمی بهم در کار باشد زانکه تیغ</p></div>
<div class="m2"><p>می برد چون اتفاق آب و آهن می شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه از بیداد او جویا دلم در هم شکست</p></div>
<div class="m2"><p>طوطی این آیینه را گر بیند الکن می شود</p></div></div>