---
title: >-
    شمارهٔ ۱۰۴۰
---
# شمارهٔ ۱۰۴۰

<div class="b" id="bn1"><div class="m1"><p>آه که امشب چه‌ها با دل ما کرده‌ای</p></div>
<div class="m2"><p>بر در مستی زده باز چه‌ها کرده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوخته بودم به صبر چاک دل خویش را</p></div>
<div class="m2"><p>پیرهن طاقتم باز قبا کرده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه دلت رو به اوست قبلهٔ آمال اوست</p></div>
<div class="m2"><p>شیشهٔ دل را چنین قبله‌نما کرده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنبه ز آتش ندید، سنگ به مینا نکرد</p></div>
<div class="m2"><p>آنچه تو بی‌رحم با اهل وفا کرده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر جگرم از نگه سونش الماس ریز</p></div>
<div class="m2"><p>زخمی خود را اگر فکر دوا کرده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم مخور از روز حشر پرتو جویا علی است</p></div>
<div class="m2"><p>بندگی سرور هردو سرا کرده‌ای</p></div></div>