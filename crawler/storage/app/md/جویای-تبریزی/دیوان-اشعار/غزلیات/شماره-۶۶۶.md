---
title: >-
    شمارهٔ ۶۶۶
---
# شمارهٔ ۶۶۶

<div class="b" id="bn1"><div class="m1"><p>از شور جنون هیچ کس آگاه نمی بود</p></div>
<div class="m2"><p>گر سلسله جنبان دلم آه نمی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ضعف نمانده است مرا قوت ناله</p></div>
<div class="m2"><p>ای وای چه می کردم اگر آه نمی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می داشت اگر کوکب اقبال بلندی</p></div>
<div class="m2"><p>دستم ز سر زلف تو کوتاه نمی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ملک بدن شاه نمی بود دلت را</p></div>
<div class="m2"><p>گر نقش نگین بندهٔ درگاه نمی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کاهش تن شمع صفت باک ندارم</p></div>
<div class="m2"><p>کاش آتش بیداد تو جانکاه نمی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می بستی اگر دیدهٔ بینش ز دو بینی</p></div>
<div class="m2"><p>جویا احدی پیش تو گمراه نمی بود</p></div></div>