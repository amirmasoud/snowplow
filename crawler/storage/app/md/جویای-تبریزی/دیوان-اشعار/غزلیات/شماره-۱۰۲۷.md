---
title: >-
    شمارهٔ ۱۰۲۷
---
# شمارهٔ ۱۰۲۷

<div class="b" id="bn1"><div class="m1"><p>زان چشم مست کار صبوحی کند نگاه</p></div>
<div class="m2"><p>در هر رگم چو شمع از آن می دود نگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون شکست شیشه صدا می شود بلند</p></div>
<div class="m2"><p>در بزم می چو بر نگهی برخورد نگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهم که بینمش ولی از دور باش حسن</p></div>
<div class="m2"><p>مانند شمع بزم به چشمم تپد نگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حقت به جانب است که نظاره دشمنی</p></div>
<div class="m2"><p>دشنام دادنی بود افزون ز حد نگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظاره خار پیرهن اختلاط اوست</p></div>
<div class="m2"><p>چندان نگاه مکن که به طبعش خلد نگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شوق دیدن تو و از ضعف تن چه دور</p></div>
<div class="m2"><p>با خویشتن مرا اگر از جا برد نگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو طاقتی که از تواند ز جای خاست</p></div>
<div class="m2"><p>بر صفحهٔ جمال تو هر جا فتد نگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بینمت که مدعیان در کمینگه اند</p></div>
<div class="m2"><p>اینجا به گوشها چو فغان می رسد نگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جویا ز بس گداخته از شرم روی او</p></div>
<div class="m2"><p>چون قطرهٔ سرشک ز چشمم چکد نگاه</p></div></div>