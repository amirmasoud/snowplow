---
title: >-
    شمارهٔ ۵۳۶
---
# شمارهٔ ۵۳۶

<div class="b" id="bn1"><div class="m1"><p>آنانکه رو به خلوت آن دلربا کنند</p></div>
<div class="m2"><p>باید که خویش را ز خود اول جدا کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را به ناز چاشته خوار جفا کنند</p></div>
<div class="m2"><p>تا رفته رفته اش به بلا مبتلا کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آهی خمیر مایهٔ صد صبح روشن است</p></div>
<div class="m2"><p>آنجا که رو به عالم صدق و صفا کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا آبرویشان نرود همچو آب جوی</p></div>
<div class="m2"><p>پاکان به قطره ای چو گهر اکتفا کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنانکه شاه کشور شب زنده داری اند</p></div>
<div class="m2"><p>شب را قیاس سایهٔ بال هما کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچون شکست شیشه صدا می شود بلند</p></div>
<div class="m2"><p>اینجا اگر نگه به نگه آشنا کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جویا جماعتی که هوس می کنند عشق</p></div>
<div class="m2"><p>دانسته خویش را به بلا مبتلا کنند</p></div></div>