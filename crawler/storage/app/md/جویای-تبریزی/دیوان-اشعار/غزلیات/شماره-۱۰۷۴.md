---
title: >-
    شمارهٔ ۱۰۷۴
---
# شمارهٔ ۱۰۷۴

<div class="b" id="bn1"><div class="m1"><p>دل می بردم غنچهٔ خندان تو جتی</p></div>
<div class="m2"><p>جان می دهم خندهٔ پنهان تو جتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دم چو اثر کرد در او منصب دل یافت</p></div>
<div class="m2"><p>در پهلوی من غنچهٔ پیکان تو جتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیهوشی ام از نشئهٔ سرجوش طهور است</p></div>
<div class="m2"><p>باشد می و نقلم لب و دندان تو جتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدمرتبه افسرده تر از اشمع مزار است</p></div>
<div class="m2"><p>گر نیست دل سوخته سوزان تو جتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل راست چو شرمت بود از رنگ به رنگی</p></div>
<div class="m2"><p>حق نمک نعمت الوان تو جتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر مهر منیر است و گر مشتری و ماه</p></div>
<div class="m2"><p>قربان تو قربان تو قربان تو جتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون غنچهٔ لاله کچه ام گل کند آخر</p></div>
<div class="m2"><p>داغ است دل از آتش پنهان تو جتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریاد که بر سفرهٔ اخلاص نباشد</p></div>
<div class="m2"><p>جز خون جگر قسمت مهمان تو جتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غافل مشو از حال دلم کآتش عشقش</p></div>
<div class="m2"><p>باشد سبب گرمی دکان تو جتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خون دل کان ریزد و آب رخ دریا</p></div>
<div class="m2"><p>یاقوت لب و گوهر دندان تو جتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرداست که خواهد سوی کشمیر روان شد</p></div>
<div class="m2"><p>جویا دو سه روزی شده مهمان تو جتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از چه رو بی وجه با ما اینقدر بیگانگی</p></div>
<div class="m2"><p>چون کند تنها دلم با اینقدر بیگانگی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آشنایی دشمنان تا کی به کار ما کنند</p></div>
<div class="m2"><p>اینقدرها اینقدرها اینقدر بیگانگی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می کشد هجرانم آخر خون ناحق می کنی</p></div>
<div class="m2"><p>خوش نمی آید خدا را اینقدر بیگانگی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با وجود آنکه با لعلت نمک ها خورده است</p></div>
<div class="m2"><p>می کنی در کار جویا اینقدرها بیگانگی</p></div></div>