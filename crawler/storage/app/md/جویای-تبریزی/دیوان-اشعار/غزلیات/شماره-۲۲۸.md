---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>وارستگی از خویش ترا رهبر فیض است</p></div>
<div class="m2"><p>دلبستگی تست که قفل در فیض است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانند نسیم سحری در ره شوقش</p></div>
<div class="m2"><p>پرواز دل از سینه به بال و پر فیض است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جوش صفا طعنه زن طور تجلی است</p></div>
<div class="m2"><p>آن سینه که از یاد خدا مظهر فیض است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیداست ز در باری بحرین دو چشمم</p></div>
<div class="m2"><p>کز یاد کسی دل صدف گوهر فیض است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا جلوهٔ دیدار بود گرم تجلی</p></div>
<div class="m2"><p>جویا به رخم دیدهٔ حیران در فیض است</p></div></div>