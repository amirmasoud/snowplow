---
title: >-
    شمارهٔ ۹۷۰
---
# شمارهٔ ۹۷۰

<div class="b" id="bn1"><div class="m1"><p>چون شمع برافروختی از باده کشیدن</p></div>
<div class="m2"><p>گلها بتوان زآتش رخسار تو چیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر شهد لب یارزدم دستی و چون شمع</p></div>
<div class="m2"><p>گردیدم غذایم سر انگشت مکیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیدهٔ حیرت زدگان یه نباشد</p></div>
<div class="m2"><p>از خانهٔ بی سقف که دیده است چکیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم نیست ز بی بال و پری طائر دل را</p></div>
<div class="m2"><p>بی بهره نماند اگر از فیض طپیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر عاشق دل خسته ندانسته نگاهش</p></div>
<div class="m2"><p>صدبار فدای سر دانسته ندیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر رقت دل فیض تواضع بفزاید</p></div>
<div class="m2"><p>در گریه شود شمع ز بالای خمیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر گرد خود از جوش حیا تار نگه را</p></div>
<div class="m2"><p>چون پیله بود چشم تو در کار تنیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چیزی که شنیدیم، چو دیدیم، ندیدیم!‏</p></div>
<div class="m2"><p>زان صلح نمودیم ز دیدن به شنیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود را اگر امشب به لب او نرساند</p></div>
<div class="m2"><p>محروم بماناد می از فیض رسیدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جویا ز نزاکت شده چون برگ گل از بس</p></div>
<div class="m2"><p>بگداخته لعل شکرینش به مکیدن</p></div></div>