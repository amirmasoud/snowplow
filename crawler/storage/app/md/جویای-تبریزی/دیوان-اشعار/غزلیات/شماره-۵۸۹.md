---
title: >-
    شمارهٔ ۵۸۹
---
# شمارهٔ ۵۸۹

<div class="b" id="bn1"><div class="m1"><p>گل داغ سر پرشور سودا عالمی دارد</p></div>
<div class="m2"><p>تکلف بر طرف دیوانگیها عالمی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حریفی در دو عالم نیست چون چشم سیه مستت</p></div>
<div class="m2"><p>که دارد عالمی با غیر و با ما عالمی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل سودای داغ لاله بر سر می زنم جویا</p></div>
<div class="m2"><p>گریبان چاکی و دامان صحرا عالمی دارد</p></div></div>