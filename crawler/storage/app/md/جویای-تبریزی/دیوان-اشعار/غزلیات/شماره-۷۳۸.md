---
title: >-
    شمارهٔ ۷۳۸
---
# شمارهٔ ۷۳۸

<div class="b" id="bn1"><div class="m1"><p>چشم آینه پرآب است از مثال من هنوز</p></div>
<div class="m2"><p>سنگ راهم گریه می آید به حال من هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی تو از حد بگذرد در ضعف حال من هنوز</p></div>
<div class="m2"><p>موی چشم آیینه را باشد مثال من هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وادی رفتن ز خود طی کرده ام یکشب چو شمع</p></div>
<div class="m2"><p>گرچه جز یک پر نروییده ز بال من هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه طبعم عندلیب بیضهٔ دلتنگی است</p></div>
<div class="m2"><p>لامکان سیر است از وحشت خیال من هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش از این عمری به لعلش التماسی داشتم</p></div>
<div class="m2"><p>بال بیتابی زند از لب سوال من هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برنتابد سرو آن ازک بدن دلبستگی</p></div>
<div class="m2"><p>سر نزد یک غنچه هم از نونهال من هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم می پوشی و سوی ما نمی بینی هنوز</p></div>
<div class="m2"><p>ظاهرا با خاک راهت بر سر کینی هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محو در غفلت به کیش ما جمادی بیش نیست</p></div>
<div class="m2"><p>چون شرر در خاره مست خواب سنگینی هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعلهٔ طور و می شیراز و یاقوت فرنگ</p></div>
<div class="m2"><p>فیض رنگ از عارضت بردند و رنگینی هنوز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در رم و آرامی ای دل دایم از یاس و امید</p></div>
<div class="m2"><p>موج بحر اضطراب و کوه تمکینی هنوز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در هوای دیدنت هر دم به راهی می دویم</p></div>
<div class="m2"><p>سوی ما یک ره به استغنا نمی بینی هنوز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاک گردیدی و سر تا پا غبار خاطری</p></div>
<div class="m2"><p>در فراق یار جویا بسکه غمگینی هنوز</p></div></div>