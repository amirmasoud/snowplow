---
title: >-
    شمارهٔ ۷۸۲
---
# شمارهٔ ۷۸۲

<div class="b" id="bn1"><div class="m1"><p>بهره از یاری یاری نیستش</p></div>
<div class="m2"><p>هر که طبع بردباری نیستش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که دل در زلف یاری نیستش</p></div>
<div class="m2"><p>یک سر مو اعتباری نیستش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که ترک بندگی کرده شعار</p></div>
<div class="m2"><p>گوئیا پروردگاری نیستش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهد دنیا جمالش دلرباست</p></div>
<div class="m2"><p>لیک رنگ اعتباری نیستش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوی خامیش از کباب دل نرفت</p></div>
<div class="m2"><p>آنکه آه شعله باری نیستش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی نصیب از کاو کاو آنمژه است</p></div>
<div class="m2"><p>هر که در دل خارخاری نیستش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشت خاکم ما بباد آه رفت</p></div>
<div class="m2"><p>بر دل از جویا غباری نیستش</p></div></div>