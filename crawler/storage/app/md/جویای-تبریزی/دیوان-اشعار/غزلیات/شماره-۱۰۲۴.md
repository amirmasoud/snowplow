---
title: >-
    شمارهٔ ۱۰۲۴
---
# شمارهٔ ۱۰۲۴

<div class="b" id="bn1"><div class="m1"><p>برنتابد دیدهٔ خونین ما بار نگاه</p></div>
<div class="m2"><p>زان سبب با چشم دل باشیم در کار نگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز وصلت مردم چشمم بسان عنکبوت</p></div>
<div class="m2"><p>می دود از شوق دیدار تو بر تار نگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بستن می نماید جلوهٔ دیدار دوست</p></div>
<div class="m2"><p>هست حایل در من و معشوق دیوار نگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک چشمش باز امشب از سیه مستی بریخت</p></div>
<div class="m2"><p>در گریبان دل ما جام سرشار نگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیست آگه غیر دریا از ته کار حباب</p></div>
<div class="m2"><p>کس چو دریا جویا نفهمیده است اسرار نگاه</p></div></div>