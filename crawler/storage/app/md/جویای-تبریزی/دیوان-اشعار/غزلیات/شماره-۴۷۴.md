---
title: >-
    شمارهٔ ۴۷۴
---
# شمارهٔ ۴۷۴

<div class="b" id="bn1"><div class="m1"><p>چشم خودبینی که مست جام زیبایی بود</p></div>
<div class="m2"><p>همچو نرگس بی نصیب از فیض بینایی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای حکیم از جام یکرنگی به بزم ما بنوش</p></div>
<div class="m2"><p>مجلس احباب ما را فیض تنهایی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود احرام بند کعبهٔ مقصود دل</p></div>
<div class="m2"><p>کشتی می در محیط غم چو دریایی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حلقه چون گردد بقدر زور برگردد کمان</p></div>
<div class="m2"><p>خم به پشت چرخ درخورد توانایی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز از کوتاه پروازی به مقصد ره نبرد</p></div>
<div class="m2"><p>آنکه چون طاووس در بند خودآرایی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرد میدانش بود نور نگاه عاشقان</p></div>
<div class="m2"><p>بسکه فرش راه او چشم توانایی بود</p></div></div>