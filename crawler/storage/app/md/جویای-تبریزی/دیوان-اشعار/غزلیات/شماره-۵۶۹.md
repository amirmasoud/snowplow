---
title: >-
    شمارهٔ ۵۶۹
---
# شمارهٔ ۵۶۹

<div class="b" id="bn1"><div class="m1"><p>چون به صحرا نکهت آن زلف شبگون می‌رود</p></div>
<div class="m2"><p>نافه می‌بالد چنان کز پوست بیرون می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌کشد با آنکه بار یک جهان دل را به دوش</p></div>
<div class="m2"><p>سرو آزادم ببین نام خدا چون می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که چشم میْ‌پرستت دشمن هشیاری است</p></div>
<div class="m2"><p>گر فلاطون آید از بزم تو مجنون می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهل باشد رنجش از دخل کج ارباب جهل</p></div>
<div class="m2"><p>حرف در بیدردی یاران محزون می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نمی‌گویم تویی دزد دل اما چون کنم</p></div>
<div class="m2"><p>کز کنارم تا سر کویت پی خون می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر فتد جویا به دریا عکس روی ما من</p></div>
<div class="m2"><p>همچو سیل تند هامون سوی جیحون می‌رود</p></div></div>