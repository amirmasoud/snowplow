---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>بر نمی تابید شرم او حضور شمع را</p></div>
<div class="m2"><p>داشت بزمش چون گهر از خویش نور شمع را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابی تا نگردد از نزاکت رنگ یار</p></div>
<div class="m2"><p>بیختند از پردهٔ فانوس نور شمع را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوخ چشمان را به بزم عصمت او راه نیست</p></div>
<div class="m2"><p>طبع او مکروه می دارد ظهور شمع را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رگ گردن نبیند پیش پای خویشتن</p></div>
<div class="m2"><p>دارد امشب ترک مست من غرور شمع را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الحق امشب حسن او جویا ید بیضا نمود</p></div>
<div class="m2"><p>کرده رخسارش تجلی زار طور شمع را</p></div></div>