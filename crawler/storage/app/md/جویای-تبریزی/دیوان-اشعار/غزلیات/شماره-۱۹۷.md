---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>محتسب امشب سبوی باده ام را پاک ریخت</p></div>
<div class="m2"><p>خون عشرت را ز بی دردی عبث بر خاک ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو آب جو که برگ گل برون آرد ز باغ</p></div>
<div class="m2"><p>با سرشکم لخت دل از دیدهٔ غمناک ریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر گیاهی را رسد لاف فلاطونی زدن</p></div>
<div class="m2"><p>محتسب تا بر زمین افشردهٔ ادراک ریخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گل پیمانه می آید شمیم درد عشق</p></div>
<div class="m2"><p>باغبان خون دلم گویی به پای تاک ریخت</p></div></div>