---
title: >-
    شمارهٔ ۷۵۸
---
# شمارهٔ ۷۵۸

<div class="b" id="bn1"><div class="m1"><p>تا یاد ترا کرده دلم راهبر خویش</p></div>
<div class="m2"><p>پر در پر عنقا بپریدم ز بپریدم ز بر خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بام قفس قوت پرواز ندارم</p></div>
<div class="m2"><p>شرمنده ام از کوتهی بال و پر خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیوند سرین را به میان تو چو بیند</p></div>
<div class="m2"><p>عاشق بود ار کوه نبندد کمر خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکبار به گرد سر او گشتم و چون شمع</p></div>
<div class="m2"><p>گردم همهٔ عمر به قربان سر خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جویا شده ام واله این مصرع سالک</p></div>
<div class="m2"><p>‏«طاووس اسیر است به گلدام پر خویش»‏</p></div></div>