---
title: >-
    شمارهٔ ۶۴۹
---
# شمارهٔ ۶۴۹

<div class="b" id="bn1"><div class="m1"><p>دل به قدر عقل هر کس را اسیر غم شود</p></div>
<div class="m2"><p>چون سبک مغزی فزون شد سرگردانی کم شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آدمیت فوق خوبیها بود، خوارش مگیر</p></div>
<div class="m2"><p>آنکه بتواند ملک گردید، کاش آدم شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر غباری ز آستان عشق بنشیند به کوه</p></div>
<div class="m2"><p>سونش الماس جزء اعظم مرهم شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبحدم چون بی نقاب آمد به گلشن بوی گل</p></div>
<div class="m2"><p>از گداز شرم بر رخسار او شبنم شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه در هجر تو، جویا تن به سختی داده است</p></div>
<div class="m2"><p>در رگش مانند مژگان تو نشتر خم شود</p></div></div>