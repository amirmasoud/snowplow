---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>ذوق درویشی ام از عالم اسباب بس است</p></div>
<div class="m2"><p>شمع کافوری من پرتو مهتاب بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی از حدت طبعم نه ای آگاه هنوز</p></div>
<div class="m2"><p>تیغم و نیست مرا حاجت می، آب بس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به مطلب نرسم نیست ز دون همتی ام</p></div>
<div class="m2"><p>شده ام طالب آن گوهر نایاب، بس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیف از آن دیده کز او قطره اشکی نچکد</p></div>
<div class="m2"><p>باعث زینت دریا در سیراب بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>‏ روشن از پرتو عشق است چراغم جویا</p></div>
<div class="m2"><p>بر سرم سایهٔ آن مهر جهانتاب بس است</p></div></div>