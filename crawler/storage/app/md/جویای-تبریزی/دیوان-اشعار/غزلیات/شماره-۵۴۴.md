---
title: >-
    شمارهٔ ۵۴۴
---
# شمارهٔ ۵۴۴

<div class="b" id="bn1"><div class="m1"><p>در باغ چو گل برخ جانان نظر افکند</p></div>
<div class="m2"><p>فریاد که روز سیهم در بدر افکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزدود ز لوح دل ما نقش دویی را</p></div>
<div class="m2"><p>بیرنگی او آمد و رنگ دگر افکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن سر مگو را که به عشقش شده تفسیر</p></div>
<div class="m2"><p>در مجلس اغیار سرشکم بدر افکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جویا ره عشق است که در گام نخستین</p></div>
<div class="m2"><p>ناخن ز سر پنجهٔ شیران نر افکند</p></div></div>