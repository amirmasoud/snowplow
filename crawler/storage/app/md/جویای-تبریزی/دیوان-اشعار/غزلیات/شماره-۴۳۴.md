---
title: >-
    شمارهٔ ۴۳۴
---
# شمارهٔ ۴۳۴

<div class="b" id="bn1"><div class="m1"><p>نازنینان را شکوه حسن با تمکین کند</p></div>
<div class="m2"><p>جوش زینت در پرش طاؤس را رنگین کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیرتر هر چند گردد خصم دشمن تر شود</p></div>
<div class="m2"><p>حلقه گردیدن کمان را بیشتر زورین کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم لیلی از نگه سازی حباب باده را</p></div>
<div class="m2"><p>عکس لعلین موج صهبا را لب شیرین کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سحرگه کز صفا عالم شود آیینه دار</p></div>
<div class="m2"><p>چون رگ گل عکس می موج هوا رنگین کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی بهار جلوه اش جویا فضای دشت را</p></div>
<div class="m2"><p>فیض گلریزان اشکم دامن گلچین کند</p></div></div>