---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>هر قدر زور آورد عشقت شکیباییم ما</p></div>
<div class="m2"><p>شمع سان در سوختنها پای بر جاییم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس زحال تیره روزان جنون آگاه نیست</p></div>
<div class="m2"><p>همچو شب در خود نهان از جوش سوداییم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می رویم از خویشتن چون شمع با بال نگاه</p></div>
<div class="m2"><p>تا به رخسار تو سرگرم تماشاییم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشت خاک ما کند جولان قمری بر هوا</p></div>
<div class="m2"><p>گرد راه جلوهٔ آن سرو بالاییم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واله دیدار را نظاره از جا می برد</p></div>
<div class="m2"><p>همچو شبنم محو آن خورشید سیماییم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما نه ما باشیم تا مستیم اسیر خویشتن</p></div>
<div class="m2"><p>چون زخود رفتیم در راه طلب ماییم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنگنای شهر مانوس مزاج عشق نیست</p></div>
<div class="m2"><p>همچو مجنون از هواداران صحراییم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نظرشان دگر جویا قناعت پیشه راست</p></div>
<div class="m2"><p>پای در دامان اگر بردیم دریاییم ما</p></div></div>