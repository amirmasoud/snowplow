---
title: >-
    شمارهٔ ۳۹۵
---
# شمارهٔ ۳۹۵

<div class="b" id="bn1"><div class="m1"><p>شد در آگاهی یکی صد زو دل دانا چو موج</p></div>
<div class="m2"><p>محشر خورشید می گردد زند دریا چو موج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست مانند صدف دل بستگی با گوهرم</p></div>
<div class="m2"><p>می زنم بر بحر پشت پای استغنا چو موج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست سیماب دلم را زندگی در اضطراب</p></div>
<div class="m2"><p>دارد از فیض تپیدن خویش را بر پا چو موج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می دهی جان خودنمایی را زبالای لباس</p></div>
<div class="m2"><p>جلوه ات رنگین بود از پهلوی خارا چو موج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهرت بی صبریت در عشق عالمگیر شد</p></div>
<div class="m2"><p>از تپیدن طبل بی‌تابی زدی جویا چو موج</p></div></div>