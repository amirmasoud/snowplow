---
title: >-
    شمارهٔ ۱۰۶۹
---
# شمارهٔ ۱۰۶۹

<div class="b" id="bn1"><div class="m1"><p>کشید شور جنونم سوی بیابانی</p></div>
<div class="m2"><p>که نه فلک بودش گرد طرف دامانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز من غزال هوس کرده صد بیابان رم</p></div>
<div class="m2"><p>دلم به سینه چو شیری است در نیستانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب وصال چه حاجت به باده پیمایی</p></div>
<div class="m2"><p>بس است باده و نقلم لبی و دندانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم ز یاد نگاهش به خویش می لرزد</p></div>
<div class="m2"><p>هزار خنجر مژگان و او، من و جانی</p></div></div>