---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>چه چاره است دل سر زدیده بر زده را</p></div>
<div class="m2"><p>خدا علاج کند این جنون به سر زده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مخور فریب رعونت که از پشیمانی</p></div>
<div class="m2"><p>بسی زنند به سر دست بر کمر زده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا جدا ز تو هر شاخ گل به چشم تمیز</p></div>
<div class="m2"><p>مشابه آمده شریان نیشتر زده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدام پنجهٔ خورشید گرم زرپاشی است</p></div>
<div class="m2"><p>چه فیضها که بود بادهٔ سحر زده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سرزنش متقاعد نمی شود دشمن</p></div>
<div class="m2"><p>که پیچ و تاب بود بیش مار سرزده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدا نصیب دل دردمند جویا کرد</p></div>
<div class="m2"><p>خدنگ آن مژهٔ تکیه بر جگر زده را</p></div></div>