---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>از آن، شکفتگی ای بی تو گل به باغ نداشت</p></div>
<div class="m2"><p>که جام لاله بجز درد در ایاغ نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کدام قطرهٔ اشکم فرو چکید از چشم</p></div>
<div class="m2"><p>که آب و رنگ گل آتشین داغ نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسان شمع ترا پای تا بسر فرسود</p></div>
<div class="m2"><p>چون در تو بود عیان اینقدر سراغ نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدیم بی تو سراسر رو چمن چو نسیم</p></div>
<div class="m2"><p>گلی نبود که مانند لاله داغ نداشت</p></div></div>