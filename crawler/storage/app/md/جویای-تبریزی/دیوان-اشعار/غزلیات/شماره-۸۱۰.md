---
title: >-
    شمارهٔ ۸۱۰
---
# شمارهٔ ۸۱۰

<div class="b" id="bn1"><div class="m1"><p>از فیض عشق دید بسی فتح باب دل</p></div>
<div class="m2"><p>دریای رحمت است چو گردید آب دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیکان دل شکار کمان ابروی مرا</p></div>
<div class="m2"><p>زنجیر ساز آمده از پیچ و تاب دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمم ز جوش اشک شود بحر موج خیز</p></div>
<div class="m2"><p>در سینه دور ازو چو کند اضطراب دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر لاله اش شمیم کباب جگر دهد</p></div>
<div class="m2"><p>جایی که خون فشان گذرد چون سحاب دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از فیض اشک هر مژه ام چو نرگ گلیست</p></div>
<div class="m2"><p>تا خورده از طرات حسن تو آب دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چار موجه کشتی تن از عناصر است</p></div>
<div class="m2"><p>دارد عبث ملاحظه از انقلاب دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاشند در خمار شراب نگاه او</p></div>
<div class="m2"><p>جویا ز بخت شور نمک بر کباب دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی است در طلبت حکمران کشور دل</p></div>
<div class="m2"><p>که از گداز نفس ریخت می به ساغر دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آب گوهر مقصود می شوی غواص</p></div>
<div class="m2"><p>اگر به بازوی همت شدی شناور دل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سراغ خلوت دلدار یافتم در خویش</p></div>
<div class="m2"><p>زدم ز داغ تمنا چو حلقه بر در دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رسد به ساحل مقصود زورق سعیش</p></div>
<div class="m2"><p>تنی که یافت ز طوفان عشق لنگر دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به غیر آتش عشقی به سینه ام جویا</p></div>
<div class="m2"><p>بسان ماهی بی آب شد سمندر دل</p></div></div>