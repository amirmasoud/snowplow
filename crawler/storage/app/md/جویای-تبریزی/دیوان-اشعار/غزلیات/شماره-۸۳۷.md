---
title: >-
    شمارهٔ ۸۳۷
---
# شمارهٔ ۸۳۷

<div class="b" id="bn1"><div class="m1"><p>نه از بیگانه چشم مردمی نه زآشنا دارم</p></div>
<div class="m2"><p>غم عالم ندارم تکیه بر ذات خدا دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه شد گر از جفایش رفت بر باد فنا خاکم</p></div>
<div class="m2"><p>هنوز از بی وفایی های او چشم وفا دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مویی گشت جسم در هوای زلف مشکینش</p></div>
<div class="m2"><p>به رنگ بوی سنبل تکیه بر دوش صبا دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان تا گشت از من پادشاه وقت خود گشتم</p></div>
<div class="m2"><p>به سر از تیره بختی سایهٔ بال هما دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه چشم همرهی از جسم دارم نه ز جان جویا</p></div>
<div class="m2"><p>به راه بیخودی پرواز رنگی رهنما دارم</p></div></div>