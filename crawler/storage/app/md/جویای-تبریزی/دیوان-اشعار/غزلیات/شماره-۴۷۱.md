---
title: >-
    شمارهٔ ۴۷۱
---
# شمارهٔ ۴۷۱

<div class="b" id="bn1"><div class="m1"><p>در هر شکنی آهم لختی زجگر دارد</p></div>
<div class="m2"><p>زین نخل عجب دارم تا ریشه ثمر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش خط شده زان حسنش کز سبزهٔ پشت لب</p></div>
<div class="m2"><p>سرمشق خط یاقوت در مدنظر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر زندش فردا ز افسوس و پشیمانی</p></div>
<div class="m2"><p>آنکس که ز ناز امروز دستی به کمر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریاد که ننماید جا در دل چون سنگش</p></div>
<div class="m2"><p>هر چند که فریادم در سنگ اثر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بستهٔ زنجیرش جان هم شده نخجیرش</p></div>
<div class="m2"><p>از زلف تو می ترسم کاین مار دو سر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صیدگهش جویا شیران به کمند آیند</p></div>
<div class="m2"><p>دل دادهٔ او باشد آن کس که جگر دارد</p></div></div>