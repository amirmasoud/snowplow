---
title: >-
    شمارهٔ ۴ - ترجمهٔ حدیثی که فاضل مناقب السادات از تفسیر هندی در کتاب آیة السعداء بخاری نقل کرده است:‏
---
# شمارهٔ ۴ - ترجمهٔ حدیثی که فاضل مناقب السادات از تفسیر هندی در کتاب آیة السعداء بخاری نقل کرده است:‏

<div class="b" id="bn1"><div class="m1"><p>بحمدالله پس از تحمید آمد</p></div>
<div class="m2"><p>زبانم نعت پیرای محمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گشتم از ظهور نعت سرمست</p></div>
<div class="m2"><p>زدم بر دامن آل عبا دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز فضل شان چو کردم نکته رانی</p></div>
<div class="m2"><p>زبان شد برگ عیش زندگانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا چون کردم آهنگ ستودن</p></div>
<div class="m2"><p>چو غنچه دل شکفت از لب گشودن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زآب و رنگ مدح شان زبانم</p></div>
<div class="m2"><p>چو غنچه برگ گل شد در زبانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیثی را به کلک هوش و فرهنگ</p></div>
<div class="m2"><p>رقم زد فاضل هندی بدین رنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که فخر انبیا با آل اطهار</p></div>
<div class="m2"><p>برآمد چون پی نفرین کفار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شده در زیر چادر با پیمبر</p></div>
<div class="m2"><p>علی و فاطمه شبیر و شبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عبا ز انوارشان در منزل قرب</p></div>
<div class="m2"><p>شده فانوس شمع محفل قرب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو از تحت عبا بیرون خرامید</p></div>
<div class="m2"><p>ز حبیب صبح سر زد پنج خورشید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همان دم جبرئیل آن محرم قدس</p></div>
<div class="m2"><p>چنین آورد وحی از عالم قدس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که ای خورشید برج دین پناهی</p></div>
<div class="m2"><p>چنین رفته است فرمان الهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که بر فرق تو کردم گیسو آرا</p></div>
<div class="m2"><p>کنم منشور مرآل عبا را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به این منشور تا ممتاز باشی</p></div>
<div class="m2"><p>میان خلق سرافراز باشی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدا پیرایهٔ منشور اکنون</p></div>
<div class="m2"><p>نموده بر تو و آل تو مسنون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بر فرق نبوت گوهر وحی</p></div>
<div class="m2"><p>فرو بارید پیغام آور وحی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دلش بشکفت از پیغام باری</p></div>
<div class="m2"><p>به رنگ غنچه از باد بهاری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو از حکم الهی گشت خرسند</p></div>
<div class="m2"><p>سر فرمانبری در پیش افکند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زهم بگشاد مو در منزل قدس</p></div>
<div class="m2"><p>ازو شد سنبلستان محفل قدس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پی تعظیم جبریل امین خاست</p></div>
<div class="m2"><p>دو گیسو بر سر آن سرور آراست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بجز آن گیسوان عنبر اندود</p></div>
<div class="m2"><p>که دید از شمع بزم کبریا دود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بوی آن دو جعد مشک افشان</p></div>
<div class="m2"><p>شده موج هوا زلف پریشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نمود از بوی جعد آن سرافراز</p></div>
<div class="m2"><p>هوا از موج صندل سایی آغاز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هوا زان موی از بس فیض بو برد</p></div>
<div class="m2"><p>حباب از طبلهٔ عطارگو برد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پس آنکه بافت آن شمع هدایت</p></div>
<div class="m2"><p>دو گیسو بر سر شاه ولایت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دگر آراست آن ناموس اکبر</p></div>
<div class="m2"><p>دو گیسو بر سر زهرای ازهر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس آنکه دسته کرد آن شاه بطحا</p></div>
<div class="m2"><p>دو شاخ سنبل ریحانتین را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو جبریل امین این منزلت دید</p></div>
<div class="m2"><p>به سوی مصطفی از روی امید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زبان التماسش گشت گویا</p></div>
<div class="m2"><p>که ای منشور خوبی بر تو زیبا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به حکم حق بسی در روز میدان</p></div>
<div class="m2"><p>اعانت دیده از من شاه مردان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دلم از یاری زهرا بیاسود</p></div>
<div class="m2"><p>که با او دست من دستاس کش بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حسن را بارها از حکم باری</p></div>
<div class="m2"><p>کمر بستم پی خدمتگزاری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو مهد غنچه و باد بهاران</p></div>
<div class="m2"><p>حسین را بوده ام گهواره جنبان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چه خوش باشد گر ای محبوب باری</p></div>
<div class="m2"><p>مرا هم ز اهل بیت خود شماری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>میان قدسیان کن سرفرازم</p></div>
<div class="m2"><p>بدست خویش شو گیسو طرازم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اجابت کرد آن محبوب داور</p></div>
<div class="m2"><p>به فرقش بافت گیسوی معنبر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو سر خیل رسل شد جعد پیرا</p></div>
<div class="m2"><p>بدست لطف جبریل امین را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به دریای کف آن پاک گوهر</p></div>
<div class="m2"><p>انامل شد ز مویش موج عنبر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مکرم ساخت حق این خاندان را</p></div>
<div class="m2"><p>بنازم احترام و قدر و شان را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همین است اینکه در اوفواه جمهور</p></div>
<div class="m2"><p>مفارق پنج و ده گیسوست مشهور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به حق این ده و پنج اهل طاعات</p></div>
<div class="m2"><p>ز باری جسته یاری در مناجات</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رقم از خامهٔ عنبر شمامه</p></div>
<div class="m2"><p>خطاب این نظم را منشورنامه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شدم مأمور نظم این فضائل</p></div>
<div class="m2"><p>من از نواب گردون قدر عادل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فروغ محفل اقبال و دولت</p></div>
<div class="m2"><p>چراغ دودمان جاه و حشمت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پی نظم مهام این عالم پیر</p></div>
<div class="m2"><p>ز پیر رای او پرسیده تدبیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بود آنرا که حفظش گشته مآمن</p></div>
<div class="m2"><p>چو جوهر پشت بر دیوار آهن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مدام آرامگاهش با دل شاد</p></div>
<div class="m2"><p>به زیر سایهٔ آل عبا باد</p></div></div>