---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>محمد خدیو ملایک سپاه</p></div>
<div class="m2"><p>سر سروان زیبدش خاک راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حبیب خدا بهترین انام</p></div>
<div class="m2"><p>علیه الصلوات و علیه السلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عرش برین یافت چون برتری</p></div>
<div class="m2"><p>به کرسی نشانید پیغمبری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بحر و بر است اگر کوه و دشت</p></div>
<div class="m2"><p>طفیلی او جمله مخلوق گشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پی مهر توقیع عفو خطا</p></div>
<div class="m2"><p>نبی خاتم است و نگین مرتضی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سزد پیش ارباب فضل و یقین</p></div>
<div class="m2"><p>چنان خاتمی را نگینی چنین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علی ولی شاه مشکل گشا</p></div>
<div class="m2"><p>وکیل در خانهٔ کبریا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس بی کسان تاج تاج آوران</p></div>
<div class="m2"><p>دل دردمندان سر سروران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مثال خدا داشتی گر وجود</p></div>
<div class="m2"><p>در آئینهٔ ذات او می نمود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر شبه واجب نبودی محال</p></div>
<div class="m2"><p>ورا گفتمی کوست چون ذوالجلال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شهانرا ز شمشیر آن دین پناه</p></div>
<div class="m2"><p>به باد فنا رفته تخت و کلاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن تیغ چون تیغ مهر برین</p></div>
<div class="m2"><p>مسخر شد امروز روی زمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مگو تیغ کز معجز بوتراب</p></div>
<div class="m2"><p>محیط جهان گشته یک قطره آب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کرا قدرت آن که راند سخن</p></div>
<div class="m2"><p>ز فضل و کمال حسین و حسن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که اینجا تواند شدن مدحگر؟</p></div>
<div class="m2"><p>مگر این طاووس عالی گهر</p></div></div>