---
title: >-
    شمارهٔ ۷ - مثنوی در تعریف چراغان روی دل
---
# شمارهٔ ۷ - مثنوی در تعریف چراغان روی دل

<div class="b" id="bn1"><div class="m1"><p>تعالی الله ازین بزم دل افروز</p></div>
<div class="m2"><p>کز و شب طعنه زن گردیده بر روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین بزم چراغان چشم بد دور</p></div>
<div class="m2"><p>که شد چون صبحدم صادق مشرق نور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وفور نورش از فیض الهی</p></div>
<div class="m2"><p>ز داغ لاله بزدوده سیاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین شب سال و مه را یاد ناید</p></div>
<div class="m2"><p>که یادش تیرگی از دل زداید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تهی کرده است شب زین بزم پهلو</p></div>
<div class="m2"><p>چو خط روی خوبان رفته یک سو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهان در گوشه ای شب زین چراغان</p></div>
<div class="m2"><p>چو خال کنج لعل خوبرویان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب ظلمت سرشت از چشم مردم</p></div>
<div class="m2"><p>چو دود شمع شد در روشنی گم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز رشک پرتو این بزم روشن</p></div>
<div class="m2"><p>فتاده مار را آتش به خرمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نهان از رشک شد در رنگ گاهی</p></div>
<div class="m2"><p>مه امشب همچو شمع صبحگاهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بر تالاب دل از فیض ذوالمن</p></div>
<div class="m2"><p>بشد عکس چراغان پرتو افکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عیان شد هر طرف بر سطح آبش</p></div>
<div class="m2"><p>هلال و انجم از موج و حبابش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صفای دل ز عکس شمع کافور</p></div>
<div class="m2"><p>شده آئینهٔ نور علی نور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فتاده عکس شمع از بس در آبش</p></div>
<div class="m2"><p>پری در شیشه دارد هر حبابش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درین تالاب عکس شمع شب تاب</p></div>
<div class="m2"><p>نموده امتزاج آتش و آب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زعکس شمع کافتاده به دریا</p></div>
<div class="m2"><p>شده کیفیت سیرش دو بالا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صفای شمع از آبش نمودار</p></div>
<div class="m2"><p>چو تن از ته نما پیراهن یار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود هر موجش از عکس چراغان</p></div>
<div class="m2"><p>جبین های مقیش کار خوبان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حباب او ز شمع عکس کافور</p></div>
<div class="m2"><p>شده فانوس سان گردآور نور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو آتش پرتو افکن شد در آبش</p></div>
<div class="m2"><p>نگین لعل را ماند حبابش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زبس در قصر این دریای بیغش</p></div>
<div class="m2"><p>گهر یاقوت شد از تاب آتش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شده گردالها هر سو نمودار</p></div>
<div class="m2"><p>بلورین کاسه های ته نشان کار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فزود از بس زعکس شعله تابش</p></div>
<div class="m2"><p>سمندر گشته مرغابی در آبش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نماید زین چراغان در ته آب</p></div>
<div class="m2"><p>بعینه چشم ماهی در شب تاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که تصویر وصف این چراغان</p></div>
<div class="m2"><p>که شد از مشرق طبعم درخشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قلم شد در نهان کلک مذهب</p></div>
<div class="m2"><p>نقط بر صفحه تابان چون کواکب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زعکس شمع کافتاده است در آب</p></div>
<div class="m2"><p>مرصع شد بلورین جام گرداب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سوادش زین چراغان گشته روشن</p></div>
<div class="m2"><p>فتاده قرص مه را نان به روغن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به روی آب کشتیها منور</p></div>
<div class="m2"><p>بود چون ماه نو بر چرخ اخضر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به دریا شعله چون شد پرتو افکن</p></div>
<div class="m2"><p>به ارباب بصیرت گشت روشن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که از فیض عدالتهای نواب</p></div>
<div class="m2"><p>به یکجا جمع گشته آتش و آب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فلک تالاب دل انجم چراغان</p></div>
<div class="m2"><p>در او نواب چون ماه درخشان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مهین استاد مکتب خانهٔ فضل</p></div>
<div class="m2"><p>کزو آباد شد کاشانهٔ فضل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بهین دستور قانون وزارت</p></div>
<div class="m2"><p>از او روشن چراغ دین و دولت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ادب آموز نحریر خردمند</p></div>
<div class="m2"><p>کز او بگرفته استاد خرد، پند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جواد و معصیت پوش و خطابخش</p></div>
<div class="m2"><p>جهانرا نور عرفانش ضیابخش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جمالش نور پرورد الهی</p></div>
<div class="m2"><p>غلام خلقش از مه تا به ماهی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شده دلها ز عدلش فارغ از رنج</p></div>
<div class="m2"><p>زجودش گشته ویران خانهٔ گنج</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تهی کیسه بود کان از سخایش</p></div>
<div class="m2"><p>تنگ مایه است دریا از عطایش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو مهر و ماه با اقبال و دولت</p></div>
<div class="m2"><p>چراغش باد روشن تا قیامت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وجودش باد دایم سایه افکن</p></div>
<div class="m2"><p>عموما بر خلایق خاصه بر من</p></div></div>