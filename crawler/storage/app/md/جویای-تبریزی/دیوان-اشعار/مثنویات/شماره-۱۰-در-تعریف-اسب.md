---
title: >-
    شمارهٔ ۱۰ - در تعریف اسب
---
# شمارهٔ ۱۰ - در تعریف اسب

<div class="b" id="bn1"><div class="m1"><p>تعالی الله ز رخش باد رفتار</p></div>
<div class="m2"><p>که زیبد گرد راهش بوی گلزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رفتن چون نسیم است و به تک باد</p></div>
<div class="m2"><p>به تن قاف و به صورت چون پریزاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسیم آسا خرامش دلپسند است</p></div>
<div class="m2"><p>ز جستن جستنش آتش بلند است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب از تندی این برق آیین</p></div>
<div class="m2"><p>نسوزد آتشش گر خانهٔ زین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جوش حدت این آتشین دم</p></div>
<div class="m2"><p>بریزد میخ از نعلش چو شبنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر هم پویهٔ این مه جبین شد</p></div>
<div class="m2"><p>که برق از خود بکام اولین شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شوخیهای این اندیشه رفتار</p></div>
<div class="m2"><p>به دام حیرت از بس شد گرفتار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سقف آسمان برق درخشان</p></div>
<div class="m2"><p>به جا مانده چو تار عنکبوتان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رود گر از پس دیوار گلشن</p></div>
<div class="m2"><p>رگ گل همچو نبض آید بجستن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رود چون باد اگر بر روی دریا</p></div>
<div class="m2"><p>نجنبد آب او آئینه آسا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنی آئینه گر از نعل آن سم</p></div>
<div class="m2"><p>بود چون بحر دایم در تلاطم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان در پویه این آتش ز جا رفت</p></div>
<div class="m2"><p>که از دم دود او رو بر قفا رفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گه جستن عجب زین رشک گلگون</p></div>
<div class="m2"><p>نیاید گر چو مار از پوست بیرون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز یالش گردن او شاخ سنبل</p></div>
<div class="m2"><p>کفل از جوشن رنگین تل گل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود چون می شود گرم تکاپو</p></div>
<div class="m2"><p>دمش یک گل پیش از کاکل او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر سازند دام از موی یالش</p></div>
<div class="m2"><p>نگردد صید جز مرغ خیالش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کشد بر صفحه شکلش را چو بهزاد</p></div>
<div class="m2"><p>هوا گیرد به رنگ کاغذ باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز شوخی بسکه بی آرام باشد</p></div>
<div class="m2"><p>به بی آرامی از بس رام باشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بره نقش سم این تند جولان</p></div>
<div class="m2"><p>طپد همچون دل عاشق ز حرمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به هر جاده که گشته گرم گرفتار</p></div>
<div class="m2"><p>بجستن آمده چون نبض بیمار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به هر عضوش جدا انداز جستن</p></div>
<div class="m2"><p>گرفته چون شرر در کوه مسکن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز گردن رسته گوش این فلک شان</p></div>
<div class="m2"><p>چو برگ از شاخ گل در نوبهاران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز گوش و گردن این جعد کاکل</p></div>
<div class="m2"><p>دمید از شاخ سنبل غنچهٔ گل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز گوش و گردن این رشک غزاله</p></div>
<div class="m2"><p>عیان کرده صراحی و پیاله</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز بس خردی به چشم کس عیان نیست</p></div>
<div class="m2"><p>ز گوش او به جز نامی، نشان نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دو گوش از کوچکی زین کوه بالا</p></div>
<div class="m2"><p>نماید همچنان کز قاف عنقا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به چشم و گردن این جلوه پیرا</p></div>
<div class="m2"><p>نباشد نسبتی آهوی چین را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بهم چشمیش کو گردن فرازد</p></div>
<div class="m2"><p>ولی با گردنش همسر نباشد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگرچه چشم آهو شوخ و شنگ است</p></div>
<div class="m2"><p>به پیش چشم او داغ پلنگ است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز بس در تند رفتاری است استاد</p></div>
<div class="m2"><p>گره باشد سرینش لیک برباد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ثنایا در دهن چون در صدف در</p></div>
<div class="m2"><p>کفل همچون دل اهل حسد پر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بود در پیکر این شعله جولان</p></div>
<div class="m2"><p>سرین و دم سر و خرطوم پیلان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همانا اسب جادو زین قبیل است</p></div>
<div class="m2"><p>که نصفش اسب باشد نصف قیل است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به تک باد است دست این پریزاد</p></div>
<div class="m2"><p>خطاب باد دستش می توان داد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه تنها باد دست است به سبک خیز</p></div>
<div class="m2"><p>که از سم گشته کاسه سرنگون نیز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز طول کاکل این کبک رفتار</p></div>
<div class="m2"><p>نماید کاسهٔ سمهاش مودار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ندارد احتیاج شانه آن یال</p></div>
<div class="m2"><p>در او مرغ دل از بس می زند بال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز بس کاندر هنرمندی است دستور</p></div>
<div class="m2"><p>گذارش گر فتد بر تار طنبور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز موزون جستن این برق رفتار</p></div>
<div class="m2"><p>صدای نغمه بیرون آید از تار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سبک خیزی که بیرون ناید آهنگ</p></div>
<div class="m2"><p>بود گر جاده اش ابریشم چنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز دستش گشته آسان حل مشکل</p></div>
<div class="m2"><p>به یک ناخن گشوده عقدهٔ دل</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گزیدی جای خود گر پیش سم را</p></div>
<div class="m2"><p>نیفتادی گره در کار دم را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نهد بر سینه سم در قطع فرسنگ</p></div>
<div class="m2"><p>برد تا نعل مصقل از دلش زنگ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دمش دنبال این طاووس رفتار</p></div>
<div class="m2"><p>پریشان همچو عاشق از پی یار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ببینی چو دم این کبک رفتار</p></div>
<div class="m2"><p>به هر مویش ببندی دل گره وار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دم او از گره باشد نمودار</p></div>
<div class="m2"><p>بعینه همچو مار و مهرهٔ مار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زند طاق از دم خود گاه رفتار</p></div>
<div class="m2"><p>به رنگ ابروی خوبان گره دار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هلال آسا فکند این شوخ چالاک</p></div>
<div class="m2"><p>ز نعلش حلقه ها در گوش افلاک</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز خوش اندامی سم این پریروی</p></div>
<div class="m2"><p>ربود از لاله های سرنگون گوی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز نعلش میخ زرین آشکاره</p></div>
<div class="m2"><p>چو بر رخسار ماه نو ستاره</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>صدای هر سم این کوه بنیان</p></div>
<div class="m2"><p>شد از بالای اندامش دو چندان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دو چشم او ز مژگان بلاجو</p></div>
<div class="m2"><p>فکنده شاخها بر هم دو آهو</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>روان گاهی چو آب و گه چو باد است</p></div>
<div class="m2"><p>ندانم مرغ یا ماهی نژاد است</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همیشه خضربادا رهنمایش</p></div>
<div class="m2"><p>ز چشم بد نگهدار خدایش</p></div></div>