---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>شکر لله که هجو کس گفتن</p></div>
<div class="m2"><p>فطرت عالی مرا نه فن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشده در هجای کس جاری</p></div>
<div class="m2"><p>تا زبانم مجاور دهن است</p></div></div>