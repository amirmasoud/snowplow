---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>گفت امام عرش مسند قاسم خلد و جحیم</p></div>
<div class="m2"><p>آنکه بر فرقش برازنده است تاج انما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کادمی در خواب باشد تا به قید زندگی است</p></div>
<div class="m2"><p>می شود بیدار چون زین قید سازندش رها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اهل تغییر از هر آن چیزی که می بینی به خواب</p></div>
<div class="m2"><p>می نخواهند ای برادر غیر عکس مدعا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس چو مکروهی ببینی در جهان اصلا مرنج</p></div>
<div class="m2"><p>عکس آن را منتظر می‌باش از فضل خدا</p></div></div>