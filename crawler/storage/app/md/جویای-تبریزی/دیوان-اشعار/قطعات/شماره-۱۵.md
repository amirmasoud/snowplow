---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>حبذا نواب حفظ الله خان</p></div>
<div class="m2"><p>آنکه چشم مهر و مه مثلش ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست خیاط قضا نام خدا</p></div>
<div class="m2"><p>خلعت دولت بر اندامش برید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنجر قهرش دل گردان شکافت</p></div>
<div class="m2"><p>تیغ کینش پهلوی شیران درید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی تکلف پیش نور رای او</p></div>
<div class="m2"><p>صبح صادق کی تواند شد سفید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش قهرش شعله برگ لاله ای</p></div>
<div class="m2"><p>پیش خلقش گل غلام زر خرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب شد از بس ز بیم قهر او</p></div>
<div class="m2"><p>دل چو اشک از دیدهٔ شیران چکید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناوکش چون بال خونریزی گشاد</p></div>
<div class="m2"><p>رنگ از رخسارهٔ جرأت پرید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از صفای نور رای او مراد</p></div>
<div class="m2"><p>بر زبان خامه می گردد سفید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در زمان سعدش از لطف اله</p></div>
<div class="m2"><p>مژدهٔ افزایش منصب رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون پی تاریخ او رفتم به فکر</p></div>
<div class="m2"><p>دل به راه جستجو هر سو دوید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت در گوش دلم استاد عقل</p></div>
<div class="m2"><p>نیر اقبال و دولت بر دمید</p></div></div>