---
title: >-
    شمارهٔ ۱۱ - تاریخ وفات ملک فخرا -  ‏۱۱۱۰ ه
---
# شمارهٔ ۱۱ - تاریخ وفات ملک فخرا -  ‏۱۱۱۰ ه

<div class="b" id="bn1"><div class="m1"><p>اوستاد زمان ملک فخرا</p></div>
<div class="m2"><p>که خرد وصف او نیارد گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود چون گنج فضل دانش و علم</p></div>
<div class="m2"><p>ز آن سبب رخ به زیر خاک نهفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدر او را کسی نمی دانست</p></div>
<div class="m2"><p>رفت از کیسهٔ عزیزان مفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد ازین غمکدهٔ به سوی لحد</p></div>
<div class="m2"><p>رفت و در جامه خواب راحت خفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بهشت از نسیم لطف اله</p></div>
<div class="m2"><p>غنچه های امید او بشکفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه نتایج ازو به دهر نماند</p></div>
<div class="m2"><p>گوهر بکر سعی از بس سفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت چون زیر خاک حورالعین</p></div>
<div class="m2"><p>لحدش را به زلف مشکین رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سال تاریخ رحلتش را عقل</p></div>
<div class="m2"><p>‏«از ملک بود ملک فضل» بگفت</p></div></div>