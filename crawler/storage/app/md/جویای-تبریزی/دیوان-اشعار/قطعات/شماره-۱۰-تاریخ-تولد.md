---
title: >-
    شمارهٔ ۱۰ - تاریخ تولد
---
# شمارهٔ ۱۰ - تاریخ تولد

<div class="b" id="bn1"><div class="m1"><p>ز میرزا ابوالخیر والا نژاد</p></div>
<div class="m2"><p>گلی در ریاض نجابت شکافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر سوسو نسیمی کز آن گل وزید</p></div>
<div class="m2"><p>غبار کدورت ز دلها برفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خورشید نور جمالش بدید</p></div>
<div class="m2"><p>رخ خویش در پردهٔ شب نهفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب غنچه و گوش گل در چمن</p></div>
<div class="m2"><p>به این مژده دارند گفت و شنفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم بهر تاریخ میلاد او</p></div>
<div class="m2"><p>در معنی از منقب فکر سفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سراپا زبان غنچه سان شد نخست</p></div>
<div class="m2"><p>پس آنگه «گل باغ امید» گفت</p></div></div>