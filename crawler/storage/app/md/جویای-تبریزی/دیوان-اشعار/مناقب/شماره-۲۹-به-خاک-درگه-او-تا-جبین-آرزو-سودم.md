---
title: >-
    شمارهٔ ۲۹ - به خاک درگه او تا جبین آرزو سودم
---
# شمارهٔ ۲۹ - به خاک درگه او تا جبین آرزو سودم

<div class="b" id="bn1"><div class="m1"><p>به خاک درگه او تا جبین آرزو سودم</p></div>
<div class="m2"><p>به گوناگون نتایج بارور شد نخل امیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبی کز فکر رایش بود دل شمع تجلی زا</p></div>
<div class="m2"><p>سحرگه چون دم از خورشید زد قهقه بخندیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال قهر او چون ترکتاز آورد بر خاطر</p></div>
<div class="m2"><p>برون پاشید یک یک راز دل از بسکه لرزیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زقهرش گفتم و بگداختم چون شمع سر تا پا</p></div>
<div class="m2"><p>زلطفش گفتم و صد پیرهن چون غنچه بالیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تفاوت آنقدر دیدم که از معنی است تا صورت</p></div>
<div class="m2"><p>کف با جود او را با ید بیضا چو سنجیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به رنگ شمع فانوس خیال از یاد رای او</p></div>
<div class="m2"><p>برون از پرده های نه فلک بر عرش تابیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم را صد جهان نور از تولایش ببر دارد</p></div>
<div class="m2"><p>بحمدالله ز فیض داغ مهرش صد چو خورشیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کند مهر از غبارم اقتباس نور تا محشر</p></div>
<div class="m2"><p>به خاک درگهش تا جبههٔ اخلاص ساییدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غلامت را بود در دین و دنیا رتبهٔ شاهی</p></div>
<div class="m2"><p>نهادم تا به لب جام تولای تو جمشیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من و فکر کمال ذات او حاشا چه فکر است این</p></div>
<div class="m2"><p>ز روی عقل دوراندیش خود شرمنده گردیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زیاد لطف سرشارش زبیم قهر خونخوارش</p></div>
<div class="m2"><p>همه تن خندهٔ صبحم سراپا لرزهٔ بیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زدم تا چنگ در حبل المتین شرع آبایش</p></div>
<div class="m2"><p>زنهی نغمه گوش زهره را چون چنگ مالیدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قبای نه فلک بر پیکرم چون غنچه تنگ آمد</p></div>
<div class="m2"><p>ز بس بر خویشتن از شوق مداحیش بالیدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا نور یقین از مهر صادق بس بود جویا</p></div>
<div class="m2"><p>لباس خودنمایی را به مهر و ماه بخشیدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پناهی جز تو نبود تشنگان روز محشر را</p></div>
<div class="m2"><p>ز نیسان شفاعت سبز گردان کشت امیدم</p></div></div>