---
title: >-
    شمارهٔ ۱۵ - در منقبت حضرت امیرالمؤمنین علی ابن طالب علیه الصلواة و السلام
---
# شمارهٔ ۱۵ - در منقبت حضرت امیرالمؤمنین علی ابن طالب علیه الصلواة و السلام

<div class="b" id="bn1"><div class="m1"><p>هزار شکر که سرمستم از شراب طهور</p></div>
<div class="m2"><p>بری است شیشه ام از خاره و می ام از انگور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رساست مستی ام از جام همت سرشار</p></div>
<div class="m2"><p>سزاست ساغرم از کاسهٔ سر فغفور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سنگسار غم از استخوان سوده تنم</p></div>
<div class="m2"><p>به زخمهای درون بسته مرهم کافور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه دل نهی به غم روزگار شرمت باد</p></div>
<div class="m2"><p>که از تو خانه اندوه شد سرای سرور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشو به وادی ظلمت سرای تن خرسند</p></div>
<div class="m2"><p>ترا که در کف دل داده اند شمع شعور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سرخ و زرد جهان دل منه که پیوسته</p></div>
<div class="m2"><p>مزاج مرد نفور است از متاع غرور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرور خاطرت از آسمان امید مدار</p></div>
<div class="m2"><p>زمهربانی تو خاطری نشد مسرور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دستگیر ضعیفانی از بلا مهراس</p></div>
<div class="m2"><p>بس است پردهٔ افت به خرمن از پر مور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترا ز پهلوی خود گر رسد گزند سزاست</p></div>
<div class="m2"><p>دلت زجوش هوس گشته خانهٔ زنبور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز راز سینهٔ دلمردگان شدن آگاه</p></div>
<div class="m2"><p>بود به دیدهٔ اهل تمیز کشف قبور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر چه پای دلم سوده گشت تا زانو</p></div>
<div class="m2"><p>ولی به رفتن از خود ندارمش معذور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو چون به کعبهٔ رفتن ز خویش ره بردی</p></div>
<div class="m2"><p>به حیرتم که دگر آمدن به خود چه ضرور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز درد عشق توام داغ حرز بازوی دل</p></div>
<div class="m2"><p>ز فیض یاد تو غم در حریم سینه سرور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیا مرو که دل غم کشیدهٔ ما را</p></div>
<div class="m2"><p>بود ز آمدن و رفتن تو ماتم و سور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هر خرابه که سرو تو در خرام آید</p></div>
<div class="m2"><p>شود ز خانهٔ چشم نظارگی معمور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به زور گریه، دل یار را به درد آرم</p></div>
<div class="m2"><p>که آب جا به دل سنگ می کند به مرور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کدام شب که نه از جذب نشتر مژه اش</p></div>
<div class="m2"><p>رگم ز پوست برون جسته چون رگ طنبور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رخت ز پهلوی زلف است پر به دل نزدیک</p></div>
<div class="m2"><p>که پیش پای نماید شبانگه آتش دور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به رنگ جوهر آئینه از رخ و زلفش</p></div>
<div class="m2"><p>به پیچ و تاب اسیرم میان ظلمت و نور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز گردن تو عیار است خون ناحق خلق</p></div>
<div class="m2"><p>به رنگ بادهٔ لعل از صراحی بلور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برد ز یک گل رخسار صد گلستان فیض</p></div>
<div class="m2"><p>کند نسیم نگه بر گل رخت چو عبور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه حکمت است که نزدیک می شود با دل</p></div>
<div class="m2"><p>بود مزاج تو چندانکه از مروت دور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ترا کسی که در آغوش خویش گیرد تنگ</p></div>
<div class="m2"><p>بود چو کسوت فانوس گردآور نور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سفیدبختی عشاق صورتی دارد</p></div>
<div class="m2"><p>اگر ز تیرگی آید برون شب دیجور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گره ز رشتهٔ تقدیر از نتوان کرد</p></div>
<div class="m2"><p>شود کفت همه یک ناخن ار چو سم ستور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قماش خلعت هستی است اینکه می نگری</p></div>
<div class="m2"><p>چو تار و پود بهم درشده سنین و شهور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز فیض باطل دیوانگان مشو غافل</p></div>
<div class="m2"><p>کز آتش دل ما روشن است شمع شعور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگرچه سرمه شد او من غبار راه شدم</p></div>
<div class="m2"><p>سزد که جوش زند خون رشک از رگ طور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نشد ز مهر بما نرم شانه گردد یار</p></div>
<div class="m2"><p>کشیده ام ببرش چون کمان همیشه به زور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خوشا دلی که به ارباب درد می جوشد</p></div>
<div class="m2"><p>مجو ز خاطر مسرور خلق فیض سرور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همیشه باد گرفتار درد بی دردی</p></div>
<div class="m2"><p>دلی که نیست ز بیداد عافیت رنجور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به روی خاک نشین زنگ غم نمی باشد</p></div>
<div class="m2"><p>اگر تو دیده وری آینه است نعل ستور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز آسیا چه رسد دانه را به غیر شکست</p></div>
<div class="m2"><p>مدار از فلک بی مدار چشم حضور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نشسته بر دلم از بس ز چرخ گرد ملال</p></div>
<div class="m2"><p>چو شمع خلوت فانوس رفته زنده به گور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فلک به قصد گزندم به ثابت و سیار</p></div>
<div class="m2"><p>کدام شب که نشوریده خانهٔ زنبور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گه کنایه مرا ریزه خوانی اغیار</p></div>
<div class="m2"><p>فشانده سونش الماس در دل ناسور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کجا روم به که گویم که گشته است دلم</p></div>
<div class="m2"><p>ز وضع اهل زمان تنگتر ز دیدهٔ مور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حذر ز محفل یاران این زمانه حذر</p></div>
<div class="m2"><p>حذر ز خانهٔ زنبور یعنی از شر و شور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه بدست و زبان در پی گزند هم اند</p></div>
<div class="m2"><p>نه دوستی نه نمکخوارگی بود منظور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جدا ز هم چو شوند این جماعت از غیت</p></div>
<div class="m2"><p>بهم زنند ز دنبال نیش چون زنبور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز همگنان چنینم خدا نگهدارد</p></div>
<div class="m2"><p>که جمله دیو سرشتند وز آدمیت دور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نظر به باطن شان حق به جانب همه است</p></div>
<div class="m2"><p>که طبع شان بود از یکدگر همیشه نفور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شما که خوب توانید شد به صحبت خوب</p></div>
<div class="m2"><p>ز همنشینی او باش بد شدن چه ضرور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>قسم به صدق و صفای سحر که نبود و نیست</p></div>
<div class="m2"><p>به جز نصیحت ازین گفتگو مرا منظور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خداگو است که من خیرخواهم احبابم</p></div>
<div class="m2"><p>به خاطرم بدی هیچکس نکرده خطور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مرا که داده خدا منصب سخندانی</p></div>
<div class="m2"><p>به غیر مدح سرائی چرا کنم مذکور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دگر ز ذکر که شمع زبان مرا افروخت</p></div>
<div class="m2"><p>که طعنه زن شد ازو بر سحر شب دیجور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نسیم لطف خدیوی مگر وزید کزو</p></div>
<div class="m2"><p>شنیده بودی دم عیسوی دل رنجور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شهنشهی که پی سجدهٔ درش هر شام</p></div>
<div class="m2"><p>نهاد مهر سر بندگی به خاک از دور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شهنشهی که به درگاه قدر او فغفور</p></div>
<div class="m2"><p>ز سر به خاک مذلت نهاد تاج غرور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شهنشهی که گرههای بیضهٔ فولاد</p></div>
<div class="m2"><p>گشاده می شود از حکم او به ناخن مور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به رنگ غنچه زبان لخت خون به کاهش باد</p></div>
<div class="m2"><p>کسی که شد به لبش غیر یاعلی مذکور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز بیم شحنهٔ نهی تو تا به صبح نشور</p></div>
<div class="m2"><p>غنوده دختر رز در مشیمهٔ انگور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به چشم صبح که گنجور نقد خورشید است</p></div>
<div class="m2"><p>کشیده اند ز خاک در تو سرمهٔ نور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدور شحنهٔ عدل تو می سزد که کند</p></div>
<div class="m2"><p>چو مهره جا بسر مار بیضهٔ عصفور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>رسد به عالم دل پیروت به آسانی</p></div>
<div class="m2"><p>که گنج مخفی اسرار را تویی گنجور</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سفیدبختی اعدای تیره باطن تو</p></div>
<div class="m2"><p>نمونه ای بود از پرده های دیدهٔ کور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به جنب رای تو بی نور دیدهٔ خورشید</p></div>
<div class="m2"><p>به پیش خلق تو صد خلد معترف به قصور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به جز ولای تو صوم و زکات مجزی نیست</p></div>
<div class="m2"><p>به غیر مهر تو نبود نماز و ححج منظور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مخالف تو اگر در حیرم کعبه شود</p></div>
<div class="m2"><p>چنان بود که به دیوار دست مالد کور</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به ریگ بحر و بیابان محاسبان قضا</p></div>
<div class="m2"><p>فضایل تو شمردند و ماند نامحصور</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>من فقیر ز فضلت چه می توانم گفت</p></div>
<div class="m2"><p>به غیر اینکه شوم معترف به عجز و قصور</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که داد مدح سرائیت می تواند داد</p></div>
<div class="m2"><p>ز دامن صفتت کوته است دست شعور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز حضرت تو الهی امید میدارم</p></div>
<div class="m2"><p>که صبح روز جزا با وجود فسق و فجور</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به دوزخم نفرستی که دوزخ دگر است</p></div>
<div class="m2"><p>مرا به دشمن آل نبی شدن محشور</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز شعر و شاعریم اینقدر نتیجه بس است</p></div>
<div class="m2"><p>که در زمانه به مداحی توام مشهور</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بس است جایزهٔ نظم من همین جویا</p></div>
<div class="m2"><p>که روز حشر شوم با سگان او محشور</p></div></div>