---
title: >-
    شمارهٔ ۷ - غزل
---
# شمارهٔ ۷ - غزل

<div class="b" id="bn1"><div class="m1"><p>آن کسوت نازک که بر اندام تو بار است</p></div>
<div class="m2"><p>چون نکهت گل دست در آغوش بهار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود چو حبابش هوس صدر نشینی</p></div>
<div class="m2"><p>آن پاک گهر را که خبر از ته کار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس ره نبرد حال سیه روز غمت را</p></div>
<div class="m2"><p>در خویش نهان گشته به رنگ شب تار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آتش عشق تو برون آمده بیغش</p></div>
<div class="m2"><p>بر سینه زر داغ توام پاک عیار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زد دست هوس غیر بر آن سلسلهٔ مو</p></div>
<div class="m2"><p>غاف که سر زلف نکویان دم مار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر سیل سرشکم بود از جا عجبی نیست</p></div>
<div class="m2"><p>اینجاست که از ضعف نگه بر مژه بار ا ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر قطرهٔ خون بسته ز دم سردی ایام</p></div>
<div class="m2"><p>در پیکر صدچاک تو جویا چو انار است</p></div></div>