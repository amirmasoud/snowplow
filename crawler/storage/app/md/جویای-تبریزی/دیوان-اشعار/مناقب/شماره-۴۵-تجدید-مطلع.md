---
title: >-
    شمارهٔ ۴۵ - تجدید مطلع
---
# شمارهٔ ۴۵ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>ای که صاف مغفرت در جام عصیان ریختی</p></div>
<div class="m2"><p>در فضای دل ز مهرت رنگ ایمان ریختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه پاشیدی در و یاقوت از دست کرم</p></div>
<div class="m2"><p>آبروی قلزم و خون دل کان ریختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زور بازوی ترا نازم که با گرز گران</p></div>
<div class="m2"><p>از سر نخوت گزین مغز پریشان ریختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون مرگ از عضو عضو دشمنت گل گل شکفت</p></div>
<div class="m2"><p>بر تنش تا غنچهٔ سیراب پیکان ریختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرم شوخی ساختی گلگون رنگین جلوه را</p></div>
<div class="m2"><p>گل به دامان هوا از گرد جولان ریختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر فقیران دست جودت چون جواهر ریز شد</p></div>
<div class="m2"><p>هر طرف از بسکه مروارید غلطان ریختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماند مانند صدف خالی کف دریا ز در</p></div>
<div class="m2"><p>آبروی مایه داریهای عمان ریختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که بی برگ محبت در ره دین تو بود</p></div>
<div class="m2"><p>چون گلشن چاک جگر در جیب و دامان ریختی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز آب تیغ دلشکاف آن را که بدخواه تو بود</p></div>
<div class="m2"><p>بادهٔ زهر فنا در ساغر جان ریختی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده ام از قدرت معجز طرازت کز بدن</p></div>
<div class="m2"><p>درد را چون گرد از پیراهن آسان ریختی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از تو خواهم صحت جسم برادر یا امام</p></div>
<div class="m2"><p>عالمی را چون به جام درد درمان ریختی</p></div></div>