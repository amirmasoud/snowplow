---
title: >-
    شمارهٔ ۴ - قصیده در نعت آنسرور صلی الله علیه و آله
---
# شمارهٔ ۴ - قصیده در نعت آنسرور صلی الله علیه و آله

<div class="b" id="bn1"><div class="m1"><p>تن داد هر آن کو زغمت سوز و الم را</p></div>
<div class="m2"><p>چون شمع درین راه ز سر ساخت قدم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم ز خجالت بود از رنگ به رنگی</p></div>
<div class="m2"><p>رعنائی رفتار تو طاووس ارم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من می روم از خویش تو سرگرم فغان باش</p></div>
<div class="m2"><p>ای ناله درین بزم سپردم به تو دم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نالهٔ پرسوز خموشان تو آید</p></div>
<div class="m2"><p>چون گل کند از پرده دری گوش اصم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردید سیه روی طمع زانکه به خواری</p></div>
<div class="m2"><p>پیوسته خورد سیلی ارباب همم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگذر ز سر خاک نشینان به تکبر</p></div>
<div class="m2"><p>با چشم کم اینمجا منگر پایهٔ کم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کس دلش آبی خورد از خاک نشینی</p></div>
<div class="m2"><p>و قری نبود در نظرش مسند جم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تمکین تو چون آهوی تصویر ز شوخی</p></div>
<div class="m2"><p>در صورت آران نهان ساخته رم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سینهٔ پرآزروم داغ تو دارد</p></div>
<div class="m2"><p>قدری که بود در کف افلاس درم را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا نرگس تو محضر قتلم بنویسد</p></div>
<div class="m2"><p>از هر مژه با خویشتن آورده قلم را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون پیلک ناوک گه بدمستی آن چشم</p></div>
<div class="m2"><p>بر چوب ببندد مژه اش دست ستم را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون ابر که از بحر بود مایهٔ فیضش</p></div>
<div class="m2"><p>مژگان من از خون دل اندوخته نم را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برده ز خیال رخ زیبای تو چشمم</p></div>
<div class="m2"><p>فیضی که دهد نکهت گل قوت شم را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا نقد شکیبش بربایند بیغما</p></div>
<div class="m2"><p>در دل مژگان تو فشردند قدم را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون گریه کنم در غمش امشب که به چشمم</p></div>
<div class="m2"><p>سوز دل افروخته نگذاشته نم را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دادند دو چشم تو بهم دست ز مژگان</p></div>
<div class="m2"><p>وز نو بنهادند ره و رستم ستم را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر اهل ریا نشتر طعن ست زبانم</p></div>
<div class="m2"><p>بر رگ نخورد زاهد پاکیزه شیم را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بی نشئه فقر است سرش هر که بسنجد</p></div>
<div class="m2"><p>با جام سفالین گدا ساغر جم را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دوریست که گر شائبهٔ صدق ندارد</p></div>
<div class="m2"><p>چون لقمهٔ بی شبهه توان خورد قسم را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سودائی خط با رخ این ساده عذاران</p></div>
<div class="m2"><p>نقد دل و جان داده نهد رسم سلم را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زاغیار شنیدم خبر آمدن یار</p></div>
<div class="m2"><p>چون در کشم این شربت آغشته به سم را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از داغ تو دل در نظر پادشه عشق</p></div>
<div class="m2"><p>داده ز کواکب چو فلک شان حشم را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جز من نگه مست تو با هر که ستم کرد</p></div>
<div class="m2"><p>در دیدهٔ انصاف ستم رفته ستم را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خصمی دل و دیدهٔ عاشق زنخست است</p></div>
<div class="m2"><p>بسیار براندند به گل کشتی هم را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سرمستم از اندیشهٔ سرجوش جوانی</p></div>
<div class="m2"><p>پیچیده تنم گرچه به خود دلق هرم را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لیلی سیه خیمهٔ چشم است نگاهت</p></div>
<div class="m2"><p>کارآسته است از مژگان خیل و حشم را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خال رخت افزوده به حسن خط سبزت</p></div>
<div class="m2"><p>چون صفر که افزاید از او پایه رقم را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کم نیست که نشنیدنی از کس نشنیده است</p></div>
<div class="m2"><p>بسیار سزد شکر خداوند اصم را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گلزار دل از سبزهٔ بیگانه بپرداز</p></div>
<div class="m2"><p>زین خاک فرح خیز بکن ریشهٔ غم را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برده است غم سوء عمل زنده به گورم</p></div>
<div class="m2"><p>افشانده ام از بسکه به سر خاک ندم را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شادم که امیدم سپر سهم مکافات</p></div>
<div class="m2"><p>کرده است شفاعتگری فخر امم را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سلطان رسالت که به فرمودهٔ عدلش</p></div>
<div class="m2"><p>ناچار بود گرگ شبانی غنم را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مخلوق نخستین چو بود جوهر ذاتت</p></div>
<div class="m2"><p>پهلو زده از قرب حدوث تو قدم را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از لطف تو کرد آنکه به بر درع حمایت</p></div>
<div class="m2"><p>در خصمی او تیغ قضا باخته دم را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فیضی که ز سروت چمن عرش بیندوخت</p></div>
<div class="m2"><p>از دست و کنار تو بود لوح و قلم را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اعجاز تو بر خاک ره بندگی افکند</p></div>
<div class="m2"><p>اعیان عرب را و صنا دید عجم را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پاس ادبم از مژگان داده سرانجام</p></div>
<div class="m2"><p>چون خامهٔ نقاش به راه تو قدم را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از فیض فرحناکی عهد تو عجب نیست</p></div>
<div class="m2"><p>کز موج اثر چین نبود جبههٔ یم را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در روز وغا خصم تنک حوصله ات راست</p></div>
<div class="m2"><p>بی بود نمودی که بو شیر علم را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صبح از پی خونریزی اعدای تو تا حشر</p></div>
<div class="m2"><p>هر روز علم ساخته شمشیر دو دم را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون شیشهٔ ساغر نخورد خسم تو جز خاک</p></div>
<div class="m2"><p>بندد به خود از حرص به فرض ار دو شکم را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سرگشته بود چرخ به گرد سر کویت</p></div>
<div class="m2"><p>تا حلقهٔ درگاه تو سازد قد خم را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از واهمهٔ شحنهٔ نهی تو نمانده است</p></div>
<div class="m2"><p>اصلا اثر رنگ اثر روی نغم را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نی کرده بسی شحنهٔ عدل تو به ناخن</p></div>
<div class="m2"><p>از نالش نخجیر هژبران اجم را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زین نعمت ایمان که به خلق از تو رسیده است</p></div>
<div class="m2"><p>دست تو به معراج رسانیده کرم را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نبود ز سر تاجوران و نمک حسن</p></div>
<div class="m2"><p>این مرتبه کز خاک در تست قسم را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کی چاشنی نعمت اخلاص تو باشد</p></div>
<div class="m2"><p>در ذائقه بندگی انواع نعم را؟</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>آنی تو که گوش طلب کس نشنیده است</p></div>
<div class="m2"><p>هرگز ز زبان کرمت غیر نعم را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آنی که به فرمودهٔ رای تو زداید</p></div>
<div class="m2"><p>زآئینه شب مصقل مه زنگ ظلم را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آنی که چو در وصف روان بخشی خلقت</p></div>
<div class="m2"><p>بر صحنه کفم جلوه گری داد قلم را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر جادهٔ مسطر اثر معجز آن خلق</p></div>
<div class="m2"><p>چون قافلهٔ مور، روان ساخت قلم را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>وارست ز غم دل به جناب تو چو پیوست</p></div>
<div class="m2"><p>منشور نجات است به کف صید حرم را</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در معرکهٔ رزم خدنگ تو به اعدا</p></div>
<div class="m2"><p>داده است به انگشت نشان راه عدم را</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در مزرع خصم تو به فرض اربچرد نحل</p></div>
<div class="m2"><p>از شان عسل یافت توان لذت سم را</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در عهد تو هر گل که شکفتن کند آغاز</p></div>
<div class="m2"><p>باشد دهن خندهٔ گل باغ ارم را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هر بیش بر دست سخای تو بود کم</p></div>
<div class="m2"><p>داده است به بیشی کرمت پایهٔ کم را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از پرتو مهر آنچه رسیده است به سایه</p></div>
<div class="m2"><p>از سایهٔ دست تو رسد بخت دژم را</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در حضرتت استاده به پا خیل ملائک</p></div>
<div class="m2"><p>از دست ندادند ره و رسم خدم را</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز امینت دوران تو بر خاک نریزد</p></div>
<div class="m2"><p>دست ستم حادثه تا خون بقم را</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ای سنگ دل آسان نبود طوف حریمش</p></div>
<div class="m2"><p>در ساحت کعبه نتوان دید صنم را</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>صد شکر که تا پیشهٔ خود ساخته طبعم</p></div>
<div class="m2"><p>مداحی سلطان عرب، شاه عجم را</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>با معنی من نسبت فرهنگ فلاطون</p></div>
<div class="m2"><p>چون نسبت صوری که به چاقیست ورم را</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هر شبه که سر برزده از دقت طبعم</p></div>
<div class="m2"><p>مالیده بسی گوش ادب جذر اصم را</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مداح توام می رسد از طبع دقیقم</p></div>
<div class="m2"><p>از ذیل قوافی بدر انداخته ذم را</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ای ختم رسل لطف تو بس شاهد جویا</p></div>
<div class="m2"><p>کز توبه کشیده است به سر جام ندم را</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>آنروز مقدر که ببازند فلکها</p></div>
<div class="m2"><p>از باد فنا دیرک خرگاه و خیم را</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>خواهم ز تو ای فخر امم بازنگیری</p></div>
<div class="m2"><p>زین بندهٔ عاصی نظر لطف و کرم را</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>باشد به سر روز ز خور تا کله نور</p></div>
<div class="m2"><p>تا کرده شب داج به بر دلق ظلم را</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چون نقش قدم نقش جبین باد شب و روز</p></div>
<div class="m2"><p>بر درگه اقبال تو اصناف امم را</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>دیگر ز تو امید من آنست که جاوید</p></div>
<div class="m2"><p>فیض از تو رسد مرجع اصناف امم را</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نواب نوازشخان آن کز اثر جود</p></div>
<div class="m2"><p>دائم کف سائل شمرد دست کرم را</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>آن خان فلک رتبه که در وصف کمالش</p></div>
<div class="m2"><p>حالی شده این مطلع برجسته قلم را</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نسبت چون بذاتت نبود فصل و کرم را</p></div>
<div class="m2"><p>گیرند فراتر ز همه پایهٔ هم را</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>آنی که سیاستگری شحنهٔ عدلت</p></div>
<div class="m2"><p>از سین ستم اره کشد فرق ستم را</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>آنی تو که هرگز نخورد روی دل کس</p></div>
<div class="m2"><p>در عهد تو از دست قضا سیلی غم را</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>آنی که به جرم دو زبانی ز سیاهی</p></div>
<div class="m2"><p>انصاف تو پیوسته بگل رانده قلم را</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>امروز نباشد دگری جز تو مکرم</p></div>
<div class="m2"><p>تکریم نشاید مگر ارباب کرم را</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هر خامه که قاموس سخای تو نویسد</p></div>
<div class="m2"><p>در معنی لاثبت کند لفظ نعم را</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>از عدل تو با یکدگر آمیزش اضداد</p></div>
<div class="m2"><p>در هیچ تنی ره ندهد ضعف هرم را</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>امروز ز عدل تو زمانیست که عشاق</p></div>
<div class="m2"><p>از چشم بتان چشم ندارند ستم را</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>در عهد تو عامست ز بس رسم فراغت</p></div>
<div class="m2"><p>آرام رگ خواب بود نبض سقم را</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>در دور تو کس نیست که سرمست غنا نیست</p></div>
<div class="m2"><p>پیموده ز بس همت تو جام کرم را</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>زآغاز جهان چشم فلک دیده در این عهد</p></div>
<div class="m2"><p>از معدلتت آشتی گرگ و غنم را</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>در دم شود از مرحمتت طالع مسعود</p></div>
<div class="m2"><p>گر سایهٔ لطف تو فتد بخت دژم را</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>مشهور جهانی تو به شمشیر و سخاوت</p></div>
<div class="m2"><p>حاتم شده گر شهرهٔ آفاق کرم را</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>در معرکهٔ لاف ستانده است به نیرو</p></div>
<div class="m2"><p>مردانگیت نطع هژبران اجم را</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گیرد غضبت پیه دو چشم عدو آنگاه</p></div>
<div class="m2"><p>سازد بهمان بهر شگون چرب علم را</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>امروز درین کشور اگر هست رواجی</p></div>
<div class="m2"><p>باشد ز دل و دست تو شمشیر و قلم را</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>از خجلت شمشیر تو پیش از دو نفس صبح</p></div>
<div class="m2"><p>هرگز ننموده است علم تیغ دو دم را</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نرگس سر از آنرو به ته افکنده که پیوست</p></div>
<div class="m2"><p>شرمندگی از خامهٔ تست اهل قلم را</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>در عین بکا خشک کند آتش قهرت</p></div>
<div class="m2"><p>در دیدهٔ بدخواه تو چون آینه نم را</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>سالم نگذارد شرر قهر تو چون شمع</p></div>
<div class="m2"><p>از جسم بداندیش تو تا مغز قلم را</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>‏ خواهم که خدا روی به دولت بگشاید</p></div>
<div class="m2"><p>زین درگه امید عرب را و عجم را</p></div></div>