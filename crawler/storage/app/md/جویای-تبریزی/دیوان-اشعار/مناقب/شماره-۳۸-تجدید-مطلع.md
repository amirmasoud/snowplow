---
title: >-
    شمارهٔ ۳۸ - تجدید مطلع
---
# شمارهٔ ۳۸ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>ای فدای مرقد پاک تو سر تا پای من</p></div>
<div class="m2"><p>یا علی مولای من مولای من مولای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مظهر گل فاتح خیبر امیرالمؤمنین</p></div>
<div class="m2"><p>بندگی قنبرش فخر من و آبای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب و رنگ زینت گلزار هستی تا شدند</p></div>
<div class="m2"><p>چون گل رعنا به گیتی سید و مولای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد درک ذات پاک هر دو در یک آینه</p></div>
<div class="m2"><p>رای نعت آرای و طبع منقبت پیرای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون تویی مولای من یا ساقی کوثر چه باک</p></div>
<div class="m2"><p>سنگ اگر بارد بود چون پنبه بر مینای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسند آرای امامت یا امیرالمؤمنین</p></div>
<div class="m2"><p>بسکه لبریز محبت گشته سرتا پای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می توان دیدن ز فیض مهرت ای گلشن بهار</p></div>
<div class="m2"><p>جلوهٔ رخسار برگ گل ز نقش پای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نگویم آنچه باید گفت اعدای ترا</p></div>
<div class="m2"><p>می تراود خون دل چون پسته از لبهای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا امیرالمؤمنین خواهم که در روز جزا</p></div>
<div class="m2"><p>همچو نقش پا ته پای تو باشد جای من</p></div></div>