---
title: >-
    شمارهٔ ۴۷ - در منقبت قایم آل محمد حضرت صاحب الزمان
---
# شمارهٔ ۴۷ - در منقبت قایم آل محمد حضرت صاحب الزمان

<div class="b" id="bn1"><div class="m1"><p>تنگ عیشم دارد از بس دور چرخ چنبری</p></div>
<div class="m2"><p>چون شمیم غنچه ام در دام بی بال و پری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو بوی گل قوی پروازم از پهلوی ضعف</p></div>
<div class="m2"><p>هر نسیمی بال سعیم را نماید شهپری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوب و زشت هر بد و نیکی برم روشن بود</p></div>
<div class="m2"><p>در بغل دارم ز دل آئینهٔ اسکندری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هوایی آن گل رخسار چون از خود روم</p></div>
<div class="m2"><p>رنگم از رخ می مکند پرواز با بال پری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مانده است از طبع من معنی تراشی یادگار</p></div>
<div class="m2"><p>همچنان کز خامهٔ مانی فن صورت گری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زورق تن را که طوفانی است در دریای عشق</p></div>
<div class="m2"><p>دل تپیدن بادبانی گشت و حیرت لنگری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نسیم از هر گلی هر دم نصیبی می برم</p></div>
<div class="m2"><p>زین گلستان نگذرم مانند صرصر سرسری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نالهٔ پرسوز قمری دمبدم گوید بلند</p></div>
<div class="m2"><p>آتشی دارم نهان در خرقهٔ خاکستری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وسمه را بر سرمه ناز از پهلوی ابروی تست</p></div>
<div class="m2"><p>شانه در زلف تو دارد جلوهٔ بال پری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاهش دردت چه خواهد کرد با من بیش از این</p></div>
<div class="m2"><p>بخیه سان چسبیده ام بر پیرهن از لاغری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جا گرفتی تا چو دل در پهلوی غیر، از حسد</p></div>
<div class="m2"><p>می کند هر موج خون در جسم زارم خنجری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از پی آرام دل چون قطره خود را جمع ساز</p></div>
<div class="m2"><p>شورش دریاست آری در خور پهناوری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تیره روزان را بود فیض از دم روشن دلان</p></div>
<div class="m2"><p>صبحدم آئینه شب را کند صیقل گری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سینه ام از داغ سودای تو گلزار بهشت</p></div>
<div class="m2"><p>از گل نظاره ات در شیشهٔ اشکم پری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سامری شاگرد چشم مست او در ساحری</p></div>
<div class="m2"><p>ختم گردیده است از آنرو ساحری بر سامری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فوج آه من ز فیض شوخی یادش بود</p></div>
<div class="m2"><p>تا قیامت بر هوا در رقص چون خیل پری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قیمت نازش به بالا رفته است از قتل من</p></div>
<div class="m2"><p>موج خونم می کند شمشیر او را جوهری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون چنار آزاده از دست تهی شد سربلند</p></div>
<div class="m2"><p>همچو گل منعم پریشان است از صاحب زری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در پناه عجز ایمن مانی از بیداد چرخ</p></div>
<div class="m2"><p>صید را نبود حصاری خوبتر از لاغری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اهل دنیا عرض حال بینوایان نشنوند</p></div>
<div class="m2"><p>آری آری لازم گوش صدف باشد کری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>می طپد مانند دل در سینه جسمم در لحد</p></div>
<div class="m2"><p>گر چنین مستانه بر خام مزارم بگذری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر که از خود بگذرد شاهنشه وقت خود است</p></div>
<div class="m2"><p>داغ بر سر می کند دیوانگان را افسری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خاکساری تا کرا گردد نصیب از عاشقی</p></div>
<div class="m2"><p>تا که یابد از شهی بر زیردستان برتری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نسیت در چشم حقیقت بین به جز بازیچه ای</p></div>
<div class="m2"><p>انکسار بینوایی افتخار سروری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شیخ شهر از ناقبولی شد بکنج خانقاه</p></div>
<div class="m2"><p>آری این خاتون بود مستور از بی چادری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بوالهوس در سینه جا داده ز آه بی اثر</p></div>
<div class="m2"><p>تیر بی پیکان بسان ترکش کبک دری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کی بجا می ماندی از طوفان سیل اشک اگر</p></div>
<div class="m2"><p>کشتی تن را گرانجانی نکردی لنگری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا فرو بارد زکین بر فرق ابنای زمان</p></div>
<div class="m2"><p>روز و شب گردون دون غم می کند گردآوری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عاجزان را هم نیارد دید از روی حسد</p></div>
<div class="m2"><p>موی چشم تنگ چرخم با وجود لاغری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این قدر از عجز من ای مدعی بر خود مبال</p></div>
<div class="m2"><p>چون کنی گر ضعفم آید بر سر زور آوری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تابکی جویا غزل خواهی سرودن زانکه نیست</p></div>
<div class="m2"><p>مطلبی جز منقبت گویی ترا از شاعری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به که باشی مدح سنج آنکه بر خاک درش</p></div>
<div class="m2"><p>جبهه ساید هر سحرگه آفتاب خاوری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مسند آرای امامت مهدی هادی که هست</p></div>
<div class="m2"><p>چون شه مردان به ذات او مسلم سروری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آنکه پیش همت او می کند بیشی کمی</p></div>
<div class="m2"><p>آنکه در جنب شکوهش اکبری کرد اصغری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آنکه گر سازند در ایام عدل او بجاست</p></div>
<div class="m2"><p>از پر شهباز تیر ترکش کبک دری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>می سزد در بحر بی پایان قدرش گر کند</p></div>
<div class="m2"><p>مه حبابی هلاله گردابی فلک نیلوفری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دشمن ظالم بود چون عدل او بر خویشتن</p></div>
<div class="m2"><p>برگ برگ بید می لرزد ز شکل خنجری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حکم خردی گر نویسد بر بزرگان شوکتش</p></div>
<div class="m2"><p>می کند نه چرخ جا در حلقهٔ انگشتری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون نباشد بر سر بازار محشر او سفید</p></div>
<div class="m2"><p>هر که چون مه گشت نور مهر او را مشتری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بسکه شد شرمندهٔ دست گهر بارش چه دور</p></div>
<div class="m2"><p>گر رود در خود فرو دریا ز گرداب از تری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر زمین زد شام عید از ماه نو مضراب را</p></div>
<div class="m2"><p>حکم او چون زهره را مانع شد از خنیاگری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از پی در یوزه پیش گوشمال همتش</p></div>
<div class="m2"><p>بحر را بر فرق از گرداب باشد لنگری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>روز هیجا کرد از یاد عقاب ناوکش</p></div>
<div class="m2"><p>دل طپیدن مرغ روح خصم را بال و پری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بسکه گردیده است از اندیشهٔ قهر تو سست</p></div>
<div class="m2"><p>نیست گیرایی چو گل با پنجهٔ زورآوری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گلشن خلقت که جنت داغدار رشک اوست</p></div>
<div class="m2"><p>آید از هر گل زمینش بوی مهر مادری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>غیر آبای تو نشناسد کسی قدر ترا</p></div>
<div class="m2"><p>قیمت گوهر که می داند به غیر از جوهری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نعمت انوار از سر کار رای روشنت</p></div>
<div class="m2"><p>مهر را راتب بود هر صبحدم یک لنگری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا شدم در وصف رای روشنت مدحت نگار</p></div>
<div class="m2"><p>می کند هر نقطه در طومار شعرم اختری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در رکابت شیر مردان چون صف آرایی کنند</p></div>
<div class="m2"><p>چشم مهر و مه غبار آرد ز گرد لشکری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دیدهٔ او باد چونروی غلامانت سفید</p></div>
<div class="m2"><p>باشد آنکس را که از غیر تو چشم یاوری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مدح مانند تویی نبود مجال چون منی</p></div>
<div class="m2"><p>که تواند داد جویا داد مدحت گستری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به که زین پس منقبت را ختم سازم بر دعا</p></div>
<div class="m2"><p>تا ملک آمین سرا باشد به چرخ چنبری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تا ببخشد فیض آبادی بساط خاک را</p></div>
<div class="m2"><p>نقش نعلین تو یعنی آفتاب خاوری</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خاک خواری باد بر سر دشمن دین ترا</p></div>
<div class="m2"><p>دوستانت را بر اعدای تو باشد سروری</p></div></div>