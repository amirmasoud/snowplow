---
title: >-
    شمارهٔ ۱۳ - در منقبت حضرت امیرالمؤمنین علی ابن ابی طالب
---
# شمارهٔ ۱۳ - در منقبت حضرت امیرالمؤمنین علی ابن ابی طالب

<div class="b" id="bn1"><div class="m1"><p>اشکم نه بی تو از مژهٔ تر فرو چکد</p></div>
<div class="m2"><p>کز ابر تیره خرمن اخگر فروچکد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز اجزای نوشداروی جان پرور منست</p></div>
<div class="m2"><p>آن می که از لب تو به ساغر فروچکد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشک چکیده از مؤه را تشنهٔ غمت</p></div>
<div class="m2"><p>نو شد به ذوق آنکه مکرر فروچکد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبها به یاد آن گل رخسار تا سحر</p></div>
<div class="m2"><p>اشکم ز کنج دیده معطر فروچکد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کام خواهش دل بیداد عاشقم</p></div>
<div class="m2"><p>زهراب غم به لذت شکر فروچکد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوهر ز حدت سر مؤگان شوق او</p></div>
<div class="m2"><p>گردیده آب از دم خنجر فروچکد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبهای هجر دیدهٔ مشتاق گریه را</p></div>
<div class="m2"><p>عمان چو قطره مژهٔ تر فروچکد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در انتظار دوست چو شمعی به راه باد</p></div>
<div class="m2"><p>از دیده پیکرم چه عجب گر فروچکد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گیرم گر آب صاف به کف از کدورتم</p></div>
<div class="m2"><p>آن شربت زلال مکدر فروچکد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگدازد آن چنانکه گدازد ز شعله شمع</p></div>
<div class="m2"><p>خون از رگم چو بر سر نشتر فروچکد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شبها چو گرمخوانی او آیدم به یاد</p></div>
<div class="m2"><p>خونم ز دل به گرمی آذر فروچکد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خون نیاز ماست که گردیده مشک تر</p></div>
<div class="m2"><p>از موی موی زلف معنبر فروچکد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در گلشنی که از گل رویت نیافت زیب</p></div>
<div class="m2"><p>خوناب غم ز دیدهٔ عبهر فروچکد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از بیم تیغ بازی برق نگاه او</p></div>
<div class="m2"><p>خون گشته دل ز چشم غضنفر فروچکد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طاووس وار می دمد از هر پرش گلی</p></div>
<div class="m2"><p>گر باده ام به بال سمندر فروچکد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از شرم پرنیان صفا کآن لباس تست</p></div>
<div class="m2"><p>گردیده آب کسوت گوهر فروچکد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل با سرشکم از سر مژگان شب فراق</p></div>
<div class="m2"><p>صد بار اگر چکید که دیگر فروچکد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صاف طهور کو که مرا از زبان کلک</p></div>
<div class="m2"><p>رشحی به مدح ساقی کوثر فروچکد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاهی که از مهابت دوران عدل او</p></div>
<div class="m2"><p>از چشم باز خون کبوتر فروچکد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از شوق مدح او نفس عیسوی گداخت</p></div>
<div class="m2"><p>تا روزی از زبان ثناگر فروچکد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تب لرز بیم او چو فتد چرخ را به تن</p></div>
<div class="m2"><p>از آسمان عرق صفت اختر فروچکد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آب حیات دان عرقی را که از جبین</p></div>
<div class="m2"><p>بر درگه وصی پیمبر فروچکد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شمشیر او که قطرهٔ آبی است فی المثل</p></div>
<div class="m2"><p>در رزم چون به تارک کافر فروچکد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همچو سرشک شمع به پیرامن لگن</p></div>
<div class="m2"><p>مغز سرش به دامن مغفر فروچکد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر نیست موج چشمهٔ خورشید تیغ او</p></div>
<div class="m2"><p>زو خون چو اختر از چه منور فروچکد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>افتد گرش نظر به دم تیغ قهر او</p></div>
<div class="m2"><p>دل خون شود ز چشم غضنفر فروچکد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>احیای دین حق کند آبی که بر عدو</p></div>
<div class="m2"><p>از ذوالفقار حیدر صفدر فروچکد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آب حیات حکم مصاف ار ترا زلب</p></div>
<div class="m2"><p>در ساغر اطاعت چاکر فروچکد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در دم ز خون شرک به صفین کارزار</p></div>
<div class="m2"><p>دریا ز تیغ مالک اشتر فروچکد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از بس ز بیم نهی تو بگداخت دور نیست</p></div>
<div class="m2"><p>گر از لباس اهل دول زر فروچکد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باشد نهنگ بحر وغا قطره قطره اش</p></div>
<div class="m2"><p>زان خوی کت از جبین تکاور فروچکد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خون مشک گشته شاهد تیغ ترا به رزم</p></div>
<div class="m2"><p>از حلقه های طرهٔ جوهر فروچکد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خون مشک گشته شاهد تیغ ترا به رزم</p></div>
<div class="m2"><p>از حلقه های طرهٔ جوهر فرو چکد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ایجاد دوزخی کند از نو چو از هراس</p></div>
<div class="m2"><p>نام عدو به حشر ز دفتر فروچکد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون شیر از انامل اعجاز مصطفی</p></div>
<div class="m2"><p>خون عدو ز پنجهٔ حیدر فروچکد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آب حیات وصف توام از سر زبان</p></div>
<div class="m2"><p>شبنم صفت ز برگ گل تر فروچکد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خوناب شوق مرقد پاک تو روز و شب</p></div>
<div class="m2"><p>از موی موی این تن لاغر فروچکد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز ابر کف عطای تواش سالها بس است</p></div>
<div class="m2"><p>گر قطره ای به فرق ثناگر فروچکد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از بس ز شرم نعل سم تو سنت گداخت</p></div>
<div class="m2"><p>آئینه پیش روی سکندر فروچکد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وصف کجا و فکر تهی کیسه ام کجا</p></div>
<div class="m2"><p>نشگفت آب گشته دلم گر فروچکد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اول ز شرم مدح تو اندیشه خون شود</p></div>
<div class="m2"><p>آنگاه از زبان ثناگر فروچکد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جویا محبت شه دنیا و دین مرا</p></div>
<div class="m2"><p>از دل رودگر آب ز گوهر فروچکد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یارب به کام دشمن دین تو از نخست</p></div>
<div class="m2"><p>زهر فنا ز ثدیهٔ مادر فروچکد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دایم ز ابر لطف تو باران عافیت</p></div>
<div class="m2"><p>ما را به کشت زندگی اندر فروچکد</p></div></div>