---
title: >-
    شمارهٔ ۱۴ - در منقبت امام علی نقی (ع)‏
---
# شمارهٔ ۱۴ - در منقبت امام علی نقی (ع)‏

<div class="b" id="bn1"><div class="m1"><p>زجوش سبزه و گل کرده ابر فصل بهار</p></div>
<div class="m2"><p>بساط مخمل گدوز پهن در گلزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوای باغ جلوریز می برد دل را</p></div>
<div class="m2"><p>شود با باد چو بوی گل پیاده سوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چنگ نغمه سرایان ز فیض آب و هوا</p></div>
<div class="m2"><p>چو بال طوطی گردیده سبز موسیقار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدم زند چو تماشایی به صحن چمن</p></div>
<div class="m2"><p>کند چو باد بهاری به روی گل رفتار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بسکه بهره ور از مایهٔ رطوبت شد</p></div>
<div class="m2"><p>نمود موج هوا عقد گوهر شهوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درخت خشک ز موج نسیم شد سرسبز</p></div>
<div class="m2"><p>چنانکه از نفس عیسوی تن بیمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فیض باد بهاری شکفته مرغان را</p></div>
<div class="m2"><p>چو گل به گلشن کشمیر غنچهٔ منقار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز انبساط هوای نشاط افزایش</p></div>
<div class="m2"><p>همیشه خنده زند همچو کبک بوتیمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشا هوای دیاری که صرصر از خاکش</p></div>
<div class="m2"><p>برد شمیم گل و نسترن به جای غبار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صفا پذیر شد از بس زمین این کشور</p></div>
<div class="m2"><p>ز بس لطیف بود خاک این خجسته دیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنانکه شمع ز فانوس شیشه بنماید</p></div>
<div class="m2"><p>به باغ لاله نمایان شد از پس دیوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به هیچ دل نبرده ره غبار اندوهی</p></div>
<div class="m2"><p>که رفته موج هوایش ز آینه زنگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شکفتگی شده عام آنچنان ز فیض هوا</p></div>
<div class="m2"><p>که هیچ فرق ز پیکان نمانده تا سوفار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کجاست لشکر اندوه و غم که هر رگ ابر</p></div>
<div class="m2"><p>بود زموج هوا همچو تیغ جوهردار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز شور خندهٔ گل در فضای این گلشن</p></div>
<div class="m2"><p>به گوش کس نرسد صوت نغمه های هزار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رطوبت است ز بس با هواش چون مرجان</p></div>
<div class="m2"><p>زبس لطافت خاکش رگ زمردوار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به زیر آب نهان است پنجهٔ خورشید</p></div>
<div class="m2"><p>زخاک سبزش پیداست ریشهٔ اشجار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز فیض آب و هوای بهار این گلشن</p></div>
<div class="m2"><p>به بار آمده شاخ شکوفه بر دستار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به روی صفحهٔ آئینه از وفور نمو</p></div>
<div class="m2"><p>عجب مدان که دمد گل ز سبزهٔ زنگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خوشا لطافت این سرزمین که از خاکش</p></div>
<div class="m2"><p>چو مو ز آینه پیداست ریشهٔ اشجار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هواش بسکه بود مشک بیز لبریز است</p></div>
<div class="m2"><p>چو نافه هر کف خاک چمن ز مشک تتار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بسکه سبز شد از فیض آب و لطف هوا</p></div>
<div class="m2"><p>نمود هر کف خاش به رنگ برگ چنار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بود به گلشن این بوستان عشرت خیز</p></div>
<div class="m2"><p>ترانه سنج چو منقار عندلیبان خار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنان وفور گل و لاله کرد سنگینی</p></div>
<div class="m2"><p>که خون لعل برون جست از رگ کهسار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز لطف آب و زفیض هوای جان بخشش</p></div>
<div class="m2"><p>نخورده سیلی بادخزان رخ گلزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز داغ سینهٔ من باغبان این گلشن</p></div>
<div class="m2"><p>گرفته گویی تخم گل همیشه بهار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>درین چمن بود از حسن صوت بلبل را</p></div>
<div class="m2"><p>گره گشای دل غنچه ناخن منقار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دلی چه سود که مانند غنچهٔ تصویر</p></div>
<div class="m2"><p>نگشت باز گره از دل ستمکش زار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنار ازان کف افسوس گشته سرتا پا</p></div>
<div class="m2"><p>که نیست رنگ دوامی به چهرهٔ گلزار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ازان ز دل نتواند گشود عقدهٔ غم</p></div>
<div class="m2"><p>که هست پنجهٔ گل همچو دست بسته نگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گشاد کار ز تن پروران مجو زنهار</p></div>
<div class="m2"><p>که عقده نتواند گشود دست چنار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مدار چشم مروت ز آسمان دورنگ</p></div>
<div class="m2"><p>مخور فریب وفا زین ستمگر غدار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو خار در تن ماهی نهفته از نیرنگ</p></div>
<div class="m2"><p>هزار خنجر در دشنه ای که برده به کار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مباش ایمن از آن دل که از تو یافت شکست</p></div>
<div class="m2"><p>به شیشه ای که ز دستت فتاد پا مگذار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به دل خورد چو خدنگت ز بسکه تنگ دلم</p></div>
<div class="m2"><p>شبیه غنچهٔ پیکان شود لب سوفار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دلم به دامن مژگان ز جوش گریه فتاد</p></div>
<div class="m2"><p>چو آن غریق که طوفانش آورد به کنار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به ذوق گریه چو دامم تمام اعضا چشم</p></div>
<div class="m2"><p>ز شوق ناله همه تن گلو چو موسیقار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رسد به شوق نثار تو خونم از رگها</p></div>
<div class="m2"><p>به دامن مژه چو نهرها به دریا بار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز حرص پروری آخر ترا بدی زاید</p></div>
<div class="m2"><p>نشسته مرغ دلت بر فراز بیضهٔ مار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گشاده خلق لب از بس به کفر نعمت، نیست</p></div>
<div class="m2"><p>دلی که نبودش از رشتهٔ نفس زنار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز اختلاط دو یکدل مجوی جز آرام</p></div>
<div class="m2"><p>به روی آینه استاده آب ازان هموار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بود مضرت شاهان فزون ز سایر خلق</p></div>
<div class="m2"><p>چو تاجدار بود بیشتر بترس از مار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کند چو عزم سفر بیشتر برد حسرت</p></div>
<div class="m2"><p>زدار دنیا آنکس که هست دنیادار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عذار روز مکافات اهل نخوت راست</p></div>
<div class="m2"><p>کشد سری که بود مست باده درد خمار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زند همیشه دم از عشق بوالهوس ز آنرو</p></div>
<div class="m2"><p>که داغ تست دل مرده را چراغ مزار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مسافرانه زیم در جهان ندارد از آن</p></div>
<div class="m2"><p>به رنگ خامهٔ زین خانه ام در و دیوار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو تار چنگ رگ سنگ در فغان آید</p></div>
<div class="m2"><p>ز بسکه آهم اثر کرد در دل کهسار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به آن خدای که سازد تصور مثلش</p></div>
<div class="m2"><p>به سومنات بدنها عروق را زنار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به قادری که گل آتشین لاله دماند</p></div>
<div class="m2"><p>فشاند تا به دل کوهسار تخم شرار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به آن یگانه که از بس بزرگی ذاتش</p></div>
<div class="m2"><p>عقول را نبود در حریم کنهش بار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به مبدعی که ز کلک بدیع قدرت خویش</p></div>
<div class="m2"><p>کشیده بر ورق دهر نقش لیل و نهار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به صانعی که درین کارگاه کون و فساد</p></div>
<div class="m2"><p>چهار عنصر ازو گشته مصدر آثار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به خاک پای رسولی که گرد نعلینش</p></div>
<div class="m2"><p>کشد به چشم مه و مهر سرمهٔ انوار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به حکم او که فلک را ازوست رتبهٔ سیر</p></div>
<div class="m2"><p>به حکم او که ازو یافت سطح خاک قرار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به فیض گلشن قدر بهشت پیرایش</p></div>
<div class="m2"><p>که هست یک گل رعنای او خزان و بهار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به زور بازوی شاه نجف امیر عرب</p></div>
<div class="m2"><p>وصی احمد مختار حیدر کرار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به کان جود و جهان وقار و بحر سخا</p></div>
<div class="m2"><p>به مرتضی علی آن شیر بیشهٔ پیکار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به زهرها که چشیدند آل پیغمبر</p></div>
<div class="m2"><p>به دردها که کشیدند ائمه اخیار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به مصحف گل رویی که از نهایت شرم</p></div>
<div class="m2"><p>نسیم را نبود در حریم وصلش بار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به بلبلی که ز روی صحیفهٔ غنچه</p></div>
<div class="m2"><p>کند همیشه دعایی صباح را تکرار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به آن دلی که ز بس شوق درد غنچه صفت</p></div>
<div class="m2"><p>کشیده تنگ گل زخم یار را بکنار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به خنده لب زخم نخورده نیش رفو</p></div>
<div class="m2"><p>به داغ و اشک یتیمان به ثابت و سیار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به آن شهید که با داغ دل بخون غلطد</p></div>
<div class="m2"><p>چو لاله ای که بریزد به ساحت گلزار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به عجز خاک نشین و به نخوت منعم</p></div>
<div class="m2"><p>به نارسائی طالع به دولت بیدار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به ذوق لذت بیداد و زهر چشم عتاب</p></div>
<div class="m2"><p>به شوق گرسنه چشم و به نعمت دیدار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به طفل غنچه که بیدار سازدش از خواب</p></div>
<div class="m2"><p>گلاب پاشبی شبنم به باغ صبح بهار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به آن نسیم که از باغ رو به دشت نهد</p></div>
<div class="m2"><p>به آه عاشق بیدل ز یاد عارض یار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به خندهٔ لب زخم و به عاشق خوشدل</p></div>
<div class="m2"><p>به بردباری خصم و به خاکساری مار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به گوهری که بریزد ز دامن مژگان</p></div>
<div class="m2"><p>اسیر صبح بناگوش یار را به کنار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به چشم مست نکویان به ساغر لبریز</p></div>
<div class="m2"><p>به زلف سلسله مویان به نافهٔ تاتار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به بد شرابی یار و به بیقراری دل</p></div>
<div class="m2"><p>به تندخوئی آتش به اضطراب شرار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به خنده ای که زند سر زمست صاف غرور</p></div>
<div class="m2"><p>به ناله ای که برآید ز سینهٔ افگار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به تردماغی زهاد و خندهٔ لب گور</p></div>
<div class="m2"><p>به شب نشینی بی باده و چراغ مزار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به انبساط لب یار و زاری عاشق</p></div>
<div class="m2"><p>به شور قهقههٔ گل به ناله های هزار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به چاه غبغب خوبان که از وفور صفا</p></div>
<div class="m2"><p>برون تراود ازو آب گوهر شهوار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به ذکر و فکر فقیری که از نهایت حرص</p></div>
<div class="m2"><p>بود ز حلقه بگوشان مالک دینار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به نوک سوزن مژگان یار کز دلها</p></div>
<div class="m2"><p>به جنبشبی بتواند کشید نشتر خار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به مهربانی مستان بزم در صحبت</p></div>
<div class="m2"><p>به کینه جوئی مردان رزم در پیکار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به درگهی که به صد آه و ناله می آید</p></div>
<div class="m2"><p>به عشق بازی گل میخ او ز باغ هزار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>که آرزویی دگر در دلم ندارد راه</p></div>
<div class="m2"><p>به جز طواف در روضهٔ جهان وقار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>علی ابن محمد امام هر دو سرا</p></div>
<div class="m2"><p>خدیو دنیی و عقبی شه صغار و کبار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تویی که هیبت قهرت بسان گریهٔ بید</p></div>
<div class="m2"><p>فکنده دست و دل شیر شرزه را از کار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز بیم شعلهٔ دشمن گداز شمشیرت</p></div>
<div class="m2"><p>رود ز رنگ به رنگی فلک ز لیل و نهار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>خورد به حکم تو گر دست روز موج بحار</p></div>
<div class="m2"><p>ز بحر سیل رود باز پس سوی کهسار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به جسم دشمن دین تو در دم هیجا</p></div>
<div class="m2"><p>ز ضرب گرز گران سنگ و خنجر خونخوار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بود زخم عیان استخوان خورد شده</p></div>
<div class="m2"><p>چو دانه ها که نمایند از شکاف انار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>فتاد سایهٔ تیغت چو در تن خصمت</p></div>
<div class="m2"><p>شکافت هر قلم استخوانش چون منقار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز تندباد خزان مهابت تو فتد</p></div>
<div class="m2"><p>به خاک پنجهٔ خورشید همچو برگ چنار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز بسکه تیغ تو سرها به باد داد، بود</p></div>
<div class="m2"><p>به رزمگاه تو هر گردباد کله منار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چو کوه طور شود سنگ سرمه سرتاسر</p></div>
<div class="m2"><p>سموم قهر تو گر بگذرد سوی کهسار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>به عزم رزم چو بندی میان مردی را</p></div>
<div class="m2"><p>ز ضرب دشنهٔ دلدوز و تیغ آتشبار</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>برآید از تن خصم تو جان غم فرسود</p></div>
<div class="m2"><p>چنانکه نالهٔ پردرد از دل افگار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به دور شحنهٔ عدل تو شد زبان به قفا</p></div>
<div class="m2"><p>فتاد دیدهٔ نرگس اگر به ساق چنار</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>به عهد روشنی رای تو چنار ز برگ</p></div>
<div class="m2"><p>عجب نباشد اگر آفتاب آرد بار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ز بیم نهی تو هرگز ز کاسهٔ طنبور</p></div>
<div class="m2"><p>صدا نیامده بیرون چو چینی مودار</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>فلک سریر خدیوا ملک غلام شها</p></div>
<div class="m2"><p>تویی که سایهٔ گرز تو در دم پیکار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>فکنده مغز پریشان کاسهٔ سر خصم</p></div>
<div class="m2"><p>چنانکه کافتد از فرق می کشان دستار</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ز پنچه اش چو بدید استخوان خصم فشار</p></div>
<div class="m2"><p>به یکدگر همه چسبید همچو موسیقار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ز بسکه خشک شد از هیبت جلادت او</p></div>
<div class="m2"><p>ز زخم خصمش خون باز ماند از رفتار</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>به دور ابر کف او به دامن افلاس</p></div>
<div class="m2"><p>چنین که ریخته پیوسته گوهر شهوار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ز بسکه مایه ور از جنس ناروایی شد</p></div>
<div class="m2"><p>کسی به چشم طمع ننگرد به سوی بحار</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>زند چو بوسه به شستش گه کمانداری</p></div>
<div class="m2"><p>رسد بهم لب سوفار تیر چون منقار</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>یکی بود لب و دندان بسان مقراضش</p></div>
<div class="m2"><p>ز بس گزد لب افسوس خصم بدکردار</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>زبان طوطی مدحت سرای خامهٔ من</p></div>
<div class="m2"><p>چو وصف تندی یکران او کند تکرار</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>نقط ر صفحه جهد چون سپند از آتش</p></div>
<div class="m2"><p>به جنبش آید مسطر چو موج دریابار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>خوشا امید تو جویا که در نظر داری</p></div>
<div class="m2"><p>ثواب نعمت ز مدح ائمهٔ اطهار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بصورت اند جداگر ائمه از احمد</p></div>
<div class="m2"><p>به معنی اند یکی با پیمبر مختار</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>به چشم دید چو بینی به بحر متصل اند</p></div>
<div class="m2"><p>جدا اگر چه نمایند در نظر انهار</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>به روضه ات که بود قبله گاه اهل یقین</p></div>
<div class="m2"><p>به رهنمونی طالع خوش آنکه یابم بار</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>شوم نخست بدرگاه آسمان جاهت</p></div>
<div class="m2"><p>هلال وار سراپا جبین سجده گذار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>از آنکه هدیهٔ من لایق جناب تو نیست</p></div>
<div class="m2"><p>عرق فشان ز خجالت بسان ابر بهار</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>کنم به خاک ره زایران درگاهت</p></div>
<div class="m2"><p>همین درر که بسفتم به دست عجز نثار</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>پس از طواف تو چو رو به کربلا آرم</p></div>
<div class="m2"><p>در آن مطاف اجل تنگ گیردم به کنار</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بسر زنم گل آسودگی ز فیض حسین</p></div>
<div class="m2"><p>در آن زمین فلک قدر تا به روز شمار</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>شها ثنای تو نبود مجال ناطقه ام</p></div>
<div class="m2"><p>مرا چه حد که توانم شدن مدیح نگار</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چو نیست مدح تو یارای من همان بهتر</p></div>
<div class="m2"><p>که آشنا به دعایت کنم لب اظهار</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>گل سرسبد چرخ تا بود خورشید</p></div>
<div class="m2"><p>به گرد مرکز خاک است تا فلک دوار</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>امیدم آنکه لگدکوب حادثات بود</p></div>
<div class="m2"><p>تن عدوی تو مانند خاک راهگذار</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چو این قصیده در آفاق طبل شهرت زد</p></div>
<div class="m2"><p>خطاب یافت ریاض المناقب از ابرار</p></div></div>