---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>به جان و دل مرا پیوند از آن است</p></div>
<div class="m2"><p>که دردت در دل و داغت به جان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان در عاشقی افسانه گشتم</p></div>
<div class="m2"><p>که از من هر طرف صد داستان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عاشقان زان دو لب یک بوسه جانا</p></div>
<div class="m2"><p>به صد جان گر فروشی رایگان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و او را همین نام و نشان بس</p></div>
<div class="m2"><p>که من نامم و او بی نشان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفیق از هجر آن نامهربان ماه</p></div>
<div class="m2"><p>همه شب کار من آه و فغان است</p></div></div>