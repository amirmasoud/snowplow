---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>پاک و پاکیزه است اصل گوهرم</p></div>
<div class="m2"><p>خاکم از خلد است و آب از کوثرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روضهٔ رضوان و آب سلسبیل</p></div>
<div class="m2"><p>نیست از میخانه و می خوش‌ترم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساغر و مینا اگر خالی شود</p></div>
<div class="m2"><p>از می ناب و شراب احمرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شود خون دل و خوناب چشم</p></div>
<div class="m2"><p>باده ی مینا شراب ساغرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که بودم شاد با وی روز وصل</p></div>
<div class="m2"><p>بود زیر پر جهان سر تا سرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانده در دام غم هجران کنون</p></div>
<div class="m2"><p>طایر بی بال و مرغ بی پرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعد عمری کان جفاجو یار داد</p></div>
<div class="m2"><p>جای در سلک سگان آن درم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سگان کوی او در کوی او</p></div>
<div class="m2"><p>از وفا بیشم به عزت کمترم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه رفت و شد نهان از من رفیق</p></div>
<div class="m2"><p>نه شد از یاد و نه رفت از خاطرم</p></div></div>