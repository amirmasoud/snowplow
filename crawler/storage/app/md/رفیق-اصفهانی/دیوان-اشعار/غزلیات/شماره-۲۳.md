---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>آن را که نیست آگهی اصلا ز سر غیب</p></div>
<div class="m2"><p>گر درنیافت سر دهان ترا چه عیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر غیب کس نشد آگاه غم مخور</p></div>
<div class="m2"><p>می خور که هیچ کس نشد آگه ز سر غیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شک فکند زاهدم از می، و لیک من</p></div>
<div class="m2"><p>شستم به صاف می ز دل خویش شک و ریب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در محفلی که ساقی ما می، دهد، کسی</p></div>
<div class="m2"><p>کاول کند شروع به صهبا بود صهیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیند اگر ز چاک گریبان تن تو را</p></div>
<div class="m2"><p>در باغ گل ز شرم بود سر فرو به جیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردا که عمر رفت و نشد روشنم که چون</p></div>
<div class="m2"><p>فصل شباب طی شد و آمد زمان شیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داند رفیق کاش یک از عیبهای خویش</p></div>
<div class="m2"><p>آنکس که داند ز من مسکین هزار عیب</p></div></div>