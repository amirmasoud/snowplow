---
title: >-
    شمارهٔ ۲۷۲
---
# شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>جان و دلم راست صد رخنه هر سو</p></div>
<div class="m2"><p>زان تیر مژگان زان تیغ ابرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برد از کفم دل طفلی چه سازم</p></div>
<div class="m2"><p>من سست پنجه آن سخت بازو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بد غیر یارب نبیند</p></div>
<div class="m2"><p>آن عارض خوب آن روی نیکو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بردند از کف دین و دل من</p></div>
<div class="m2"><p>آن چشم ترک و آن خال هندو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خونم چو ریزی در کوی خود ریز</p></div>
<div class="m2"><p>کافتد سر من بر آن سر کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکدم نباشم یکدم نباشد</p></div>
<div class="m2"><p>بی فکر او من در فکر من او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا جان سپارد دل برندارد</p></div>
<div class="m2"><p>یکدم رفیق از آن سر کو</p></div></div>