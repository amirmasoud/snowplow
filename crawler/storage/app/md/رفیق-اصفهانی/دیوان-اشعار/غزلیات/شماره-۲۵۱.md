---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>نداند کاش قدر مهر من مهرآزمای من</p></div>
<div class="m2"><p>بقدر مهر من بر من کند گر جور وای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرستم گر بتی جز تو بت دیرآشنای من</p></div>
<div class="m2"><p>خدای من تویی ای بت تویی ای بت خدای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندیده پیشتر نشنیده افزون تر کسی هرگز</p></div>
<div class="m2"><p>جفایی از جفای تو وفایی از وفای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکش زارم میندیش از هلاک من که می باشد</p></div>
<div class="m2"><p>جز این نه مطلب من غیر از این نه مدعای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خونم چون کشی سویم نگاهی کن که در محشر</p></div>
<div class="m2"><p>بهای خون نخواهم از تو، بس این خونبهای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدرد هجر جانان چند [و] تا کی مبتلا باشم</p></div>
<div class="m2"><p>به جان خود که رحمی کن به جان مبتلای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای داغ و دردت مرهم و درمان نمی خواهم</p></div>
<div class="m2"><p>که داغت مرهم من باشد و دردت دوای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفیق آن مانده واپس از رفیقانم در این وادی</p></div>
<div class="m2"><p>که غیر از سایهٔ من کس نباشد در فقای من</p></div></div>