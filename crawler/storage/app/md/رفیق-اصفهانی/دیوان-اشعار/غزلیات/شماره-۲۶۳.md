---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>آن می که هست آّب خضر شرمسار از آن</p></div>
<div class="m2"><p>ساقی بیا و یک دوسه ساغر بیار از آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نیم جان که هست مرا سود با منست</p></div>
<div class="m2"><p>گیری بهای نیم نگه گر هزار از آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی کنم بزخم دگر کار تو تمام</p></div>
<div class="m2"><p>زحمت مکش دگر که گذشته است کار از آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر مهر و ماه گر گذری با چنین جمال</p></div>
<div class="m2"><p>گیری شکیب ازین و ربایی قرار از آن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوی نگارخانه ی چین رو که بسترند</p></div>
<div class="m2"><p>تمثالهای مانی صورت نگار از آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنی که حال صد چو منی گر دهی بباد</p></div>
<div class="m2"><p>بر دامن دلت ننشیند غبار از آن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جنس گرانبهاست محبت ولی رفیق</p></div>
<div class="m2"><p>از ما نمی خرند به مفت این دیار از آن</p></div></div>