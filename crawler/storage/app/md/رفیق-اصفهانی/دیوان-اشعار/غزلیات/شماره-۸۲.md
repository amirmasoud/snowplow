---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>مرا خاطر از آن بی غم نباشد</p></div>
<div class="m2"><p>که بی غم خاطرم خرم نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دل دردم نباشد کم ز درمان</p></div>
<div class="m2"><p>به جان داغم کم از مرهم نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بستم عهد یاری با تو گفتم</p></div>
<div class="m2"><p>که یاری چون تو در عالم نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانستم ترا ای سست پیمان</p></div>
<div class="m2"><p>بنای دوستی محکم نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی پنداشتم غیر از دل من</p></div>
<div class="m2"><p>در آن گیسوی خم در خم نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دیدم، در همه عالم دلی نیست</p></div>
<div class="m2"><p>که در آن طره ی درهم نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفیق و غیر، کی بود از حریمت</p></div>
<div class="m2"><p>که آن محروم و این محرم نباشد</p></div></div>