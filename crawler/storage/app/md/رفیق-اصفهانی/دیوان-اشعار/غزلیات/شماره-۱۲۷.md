---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه مرا کشته به میدان تو یابند</p></div>
<div class="m2"><p>چون گوی، سرم در خم چوگان تو یابند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زانکه بجویند شهیدان وفا را</p></div>
<div class="m2"><p>صد کشته به هر گوشه ی میدان تو یابند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر دیده نهند از سر تعظیم نکویان</p></div>
<div class="m2"><p>خاری که ز دیوار گلستان تو یابند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیسی نفسان چاشنی عمر ابد را</p></div>
<div class="m2"><p>چون خضر ز سرچشمه ی حیوان تو یابند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشا گره از زلف گره گیر که عشاق</p></div>
<div class="m2"><p>دلهای خود از زلف پریشان تو یابند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخرام که شرمنده همه سرو قدان را</p></div>
<div class="m2"><p>از جلوه ی شمشاد خرامان تو یابند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عهد تو تنها نه رفیقست که هر جا</p></div>
<div class="m2"><p>پیمانه کشی هست به پیمان تو یابند</p></div></div>