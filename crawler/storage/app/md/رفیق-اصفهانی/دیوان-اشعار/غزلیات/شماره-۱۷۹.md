---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>به گلشن بگذرد گر با چنین قد و چنان عارض</p></div>
<div class="m2"><p>شود شرمنده سرو و گل از آن قد و از آن عارض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز قد خویش سرو و از رخ خود گل خجل گردد</p></div>
<div class="m2"><p>نمایی گر خرامان قد و گر سازی عیان عارض</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سرو و گل نمایی گر قد و عارض تو، ننماید</p></div>
<div class="m2"><p>نه سرو از بوستان قد و نه گل از گلستان عارض</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگویم سرو و گل آن قد و عارض را که سرو و گل</p></div>
<div class="m2"><p>نه رعنا همچو آن قد است و نه زیبا چو آن عارض</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود سرو و گل من آن قد و عارض نگار من</p></div>
<div class="m2"><p>مکن پنهان ز من قد و مساز از من نهان عارض</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد پیش سر و قد و ماه عارضت جانا</p></div>
<div class="m2"><p>نه سرو بوستان قد و نه ماه آسمان عارض</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفیق آن سرو گلرخ می دهد می خیز و در پیری</p></div>
<div class="m2"><p>چو شاخ ارغوان کن قد، چو برگ ارغوان عارض</p></div></div>