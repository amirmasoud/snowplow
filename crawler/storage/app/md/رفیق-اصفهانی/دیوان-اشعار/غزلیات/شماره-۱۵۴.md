---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>ما را چو تو نیست یار دیگر</p></div>
<div class="m2"><p>یار تو چو من هزار دیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز کویت و جز سگت نداریم</p></div>
<div class="m2"><p>یار دگر و دیار دیگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بگذری از رهی بسویم</p></div>
<div class="m2"><p>هر دم من و رهگذار دیگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد بارم اگر چو سگ برانی</p></div>
<div class="m2"><p>آیم به در تو بار دیگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور از بر من کناره گیری</p></div>
<div class="m2"><p>آیم برت از کنار دیگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مثل تو به روزگار من نیست</p></div>
<div class="m2"><p>گو باش به روزگار دیگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن چشم شکار پیشه گیرد</p></div>
<div class="m2"><p>هر چشم زدن شکار دیگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز روی و خطت رفیق را نیست</p></div>
<div class="m2"><p>باغ دگر و بهار دیگر</p></div></div>