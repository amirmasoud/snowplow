---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>تا ماه رسیده آهم امشب</p></div>
<div class="m2"><p>آه ار نرسد به ماهم امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی ماه رخش نخفته چشمم</p></div>
<div class="m2"><p>ای ماه توئی گواهم امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیشب ز تو دیده ام نگاهی</p></div>
<div class="m2"><p>در حسرت آن نگاهم امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی جرم تو رانده دوشم از بزم</p></div>
<div class="m2"><p>بزم آمده عذر خواهم امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بزم تو بوده هر شبم جا</p></div>
<div class="m2"><p>اینجا ز چه نیست راهم امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر با تو به نقل و می کنم روز</p></div>
<div class="m2"><p>طاعت باشد گناهم امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغ سحری رفیق نالید</p></div>
<div class="m2"><p>از نالهٔ صبحگاهم امشب</p></div></div>