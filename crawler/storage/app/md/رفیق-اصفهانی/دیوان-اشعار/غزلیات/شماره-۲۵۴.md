---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>به غیر دل که کند تا سحر فغان با من</p></div>
<div class="m2"><p>بشب فراق تو کس نیست همزبان با من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا نمی کند ای ماه مهربان با من</p></div>
<div class="m2"><p>ببین چه می کند از کینه آسمان با من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جان تو که گرم دوستی تو باکی نیست</p></div>
<div class="m2"><p>شوند دشمن اگر جمله ی جهان با من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا تو دشمن و من دوستم ترا تا کی</p></div>
<div class="m2"><p>من این چنین به تو باشم تو آنچنان با من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش است غیر ز جورت به من وزین غافل</p></div>
<div class="m2"><p>که جور فاش تو لطفی بود نهان با من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین بقید تنم جان پاک مانده که هست</p></div>
<div class="m2"><p>سگ تو رام به این مشت استخوان با من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی شود که نباشد کنار من پرخون</p></div>
<div class="m2"><p>رفیق تا بود این چشم خونفشان با من</p></div></div>