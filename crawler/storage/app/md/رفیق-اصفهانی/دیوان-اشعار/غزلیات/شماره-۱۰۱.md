---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>نه مهر، خوبی روی ترا نه مه دارد</p></div>
<div class="m2"><p>خدا ز چشم بدت ای پسر نگه دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا ز ده نگذشته است سال و مهر رخت</p></div>
<div class="m2"><p>هزار طعنه به ماه چهارده دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا روم چکنم حال دل کرا گویم</p></div>
<div class="m2"><p>نه رند میکده نه شیخ خانقه دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیاز و عجز من ناتوان چه خواهد کرد</p></div>
<div class="m2"><p>باین غرور که آن ترک کج کله دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بطرف رخ منه آن زلف را چنین زنهار</p></div>
<div class="m2"><p>که روز روشن عشاق را سیه دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود به کیش تو گر دوستی گناه رفیق</p></div>
<div class="m2"><p>یقین که از همه کس بیشتر گنه دارد</p></div></div>