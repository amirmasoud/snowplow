---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>به قربان قد و بالات گردم</p></div>
<div class="m2"><p>بلاگردان سر تا پات گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه استغنا و ناز است این الهی</p></div>
<div class="m2"><p>فدای ناز و استغنات گردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خاک پایت ای بت کارزویم</p></div>
<div class="m2"><p>همین باشد که خاک پات گردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هلاک عارض زیبات باشم</p></div>
<div class="m2"><p>شهید قامت رعنات گردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همین گردد مرا در دل شب و روز</p></div>
<div class="m2"><p>که گرد منزل و مأوات گردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا دیدی و گفتی این سگ ماست</p></div>
<div class="m2"><p>به گرد دیدهٔ بینات گردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفیق آسا مرا دارد بر آن عشق</p></div>
<div class="m2"><p>که رسوایت شوم شیدات گردم</p></div></div>