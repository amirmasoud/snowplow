---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>به عمر خضر جدا از تو نیستم خرسند</p></div>
<div class="m2"><p>که پیش روی تو مردن هزار بهتر ازین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سگ تو یار من و کوی تو دیار من است</p></div>
<div class="m2"><p>چه یار بهتر ازین و دیار بهتر ازین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مهر و مه شب و روزم چه سود بی تو که هست</p></div>
<div class="m2"><p>شب سیه به ازین روز تار بهتر ازین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا که صید توام پاس دار بهتر ازین</p></div>
<div class="m2"><p>که در کمند نیفتد شکار بهتر ازین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سواره می روی و خلق می نمایندت</p></div>
<div class="m2"><p>بیکدگر که نباشد سوار بهتر ازین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بزم وصلم و از رشک غیر می گویم</p></div>
<div class="m2"><p>که درد هجر و غم انتظار بهتر ازین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفیق شهر پر و شهریار پر دیدم</p></div>
<div class="m2"><p>نه شهر دیدم و نه شهریار بهتر ازین</p></div></div>