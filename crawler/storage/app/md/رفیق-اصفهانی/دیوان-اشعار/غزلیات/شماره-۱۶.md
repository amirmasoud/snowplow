---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>از آن مژگان تر جاروب کردم آستانش را</p></div>
<div class="m2"><p>که با من در میان نبود غباری پاسبانش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در این باغم من آن بلبل که چون بست آشیان بر گل</p></div>
<div class="m2"><p>برد از شاخ اول از دوم شاخ آشیانش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مریض عشق بیمار است محمل کو در این عالم</p></div>
<div class="m2"><p>که می نالد ز درد و کس نمی فهمد زبانش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر با خار گیرد عندلیبش خو عجب نبود</p></div>
<div class="m2"><p>در آن گلشن که هست الفت به گلچین باغبانش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سواری را که شهری می نهد سر بر سم توسن</p></div>
<div class="m2"><p>کجا دست رفیق بینوا گیرد عنانش را</p></div></div>