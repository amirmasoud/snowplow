---
title: >-
    شمارهٔ ۲۴۱
---
# شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>از لطف نمی بینی سویم چه خطا کردم</p></div>
<div class="m2"><p>جز آنکه جفا دیدم جز آنکه وفا کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی ز غمم جایی کردی به کسی شکوه</p></div>
<div class="m2"><p>این حرف کرا گفتم این شکوه کجا کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درمان چو نشد حاصل بر مرگ نهادم دل</p></div>
<div class="m2"><p>یکچند دگر گیرم بیهوده دوا کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید که به صد خاری در خون کشیم آری</p></div>
<div class="m2"><p>عشق چو تو خونخواری انکار چرا کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک عمر جفا دیدم نادیده وفای تو</p></div>
<div class="m2"><p>نالم ز که چون من خود بر خویش جفا کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد دفعه فزون دیدم بیداد و ثنا گفتم</p></div>
<div class="m2"><p>صد بار فزون گفتی دشنام [و] دعا کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آواز درا هرگز زین قافله نشنیدم</p></div>
<div class="m2"><p>هر چند رفیق آواز مانند درا کردم</p></div></div>