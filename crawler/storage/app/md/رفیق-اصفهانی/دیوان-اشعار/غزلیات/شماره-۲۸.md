---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>نگار من چو تو زیبا نگار بسیار است</p></div>
<div class="m2"><p>نگار سرو قد و گلعذار بسیار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو گر به من نشوی دوست، دوست، هست بسی</p></div>
<div class="m2"><p>تو گر به من نشوی یار، یار، بسیار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن ز لطف کمم ناامید کز تو مرا</p></div>
<div class="m2"><p>امید در دل امیدوار، بسیار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در این دیار برای تو مانده ام ورنه</p></div>
<div class="m2"><p>بدهر یار پر است و دیار، بسیار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار خار جفا در دلم شکست از تو</p></div>
<div class="m2"><p>همان به دل ز توام خار خار، بسیار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گاه گاه که یادم کنی تو خوشنودم</p></div>
<div class="m2"><p>که این هم از تو فراموشکار، بسیار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشغل عشق ترا شغلها خوش است رفیق</p></div>
<div class="m2"><p>وگرنه شغل فراوان و کار، بسیار است</p></div></div>