---
title: >-
    شمارهٔ ۲۳۰
---
# شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>شنیدمت که به حسنی فزون ز ماه چو دیدم</p></div>
<div class="m2"><p>بسی فزونتری ای ماهرخ از آنچه شنیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دیده تا نرود نقش عارض تو چو رفتی</p></div>
<div class="m2"><p>خیال روی تو در کارگاه دیده کشیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی شنیدم و دیدم نگار خوب و لیکن</p></div>
<div class="m2"><p>به خوبی تو نگاری ندیدم و نشنیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به باغبانی نخل قد تو عمر عزیزم</p></div>
<div class="m2"><p>گذشت [و] هرگز از آن میوه ی مراد نچیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دام عشق تو آن طایرم فتاده که هرگز</p></div>
<div class="m2"><p>به باغ وصل تو گامی به کام دل نپریدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جیب جان نبود دسترس مرا ز فراقت</p></div>
<div class="m2"><p>چه سود از اینکه زدم جیب چاک و جامه دریدم</p></div></div>