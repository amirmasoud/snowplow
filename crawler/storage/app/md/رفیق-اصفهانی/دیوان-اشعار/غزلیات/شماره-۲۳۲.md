---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>دوستان را به خود از بهر تو دشمن کردم</p></div>
<div class="m2"><p>کس به دشمن نکند آنچه به خود من کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامنم گشت ز خون مژه گلگون این بود</p></div>
<div class="m2"><p>آن گل عیش که من بی تو به دامن کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرمن هستی خود سوختم از آه ببین</p></div>
<div class="m2"><p>که چه با خویش من سوخته خرمن کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کم دلت سوخت به حال دلم از آتش آه</p></div>
<div class="m2"><p>از چه سوز دل خود پیش تو روشن کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یادم از روی تو و کوی تو آمد هر گاه</p></div>
<div class="m2"><p>بی تو سیر گل و نظاره ی گلشن کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کردم اندیشه ی فردوس برون از سر خویش</p></div>
<div class="m2"><p>بر سر کوی تو آن روز که مسکن کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشدم کامروا از حرم و دیر رفیق</p></div>
<div class="m2"><p>به عبث پیروی شیخ و برهمن کردم</p></div></div>