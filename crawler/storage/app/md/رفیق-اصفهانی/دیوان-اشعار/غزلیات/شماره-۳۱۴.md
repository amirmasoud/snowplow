---
title: >-
    شمارهٔ ۳۱۴
---
# شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>ای دل ز تو خوش سالی و ماهی به نگاهی</p></div>
<div class="m2"><p>خوش کن دل من گاه به گاهی به نگاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دور نگاهی سوی من کن که نباشد</p></div>
<div class="m2"><p>از شاه و گدا دور نگاهی به نگاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشکست نگاهش صف خوبان که شنیدست</p></div>
<div class="m2"><p>طفلی شکند قلب سپاهی به نگاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر روز برد آن که نگاهش دلی از راه</p></div>
<div class="m2"><p>دی برد دلم بر سر راهی به نگاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگذاشتم آن دل که چو جان یکدمش از دست</p></div>
<div class="m2"><p>برد از کف من چشم سیاهی به نگاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد چه بود جرم نگاهی به نکویان</p></div>
<div class="m2"><p>جایی که بود کوه گناهی به نگاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دیده ی خونبار من آن قوم که خندند</p></div>
<div class="m2"><p>خون گریدشان دیده الهی به نگاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویند مه و مهری و گویم که نبرده است</p></div>
<div class="m2"><p>از کس دل و دین مهری و ماهی به نگاهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی درصدد صید دل زار رفیق است</p></div>
<div class="m2"><p>ماهی که رباید دل شاهی به نگاهی</p></div></div>