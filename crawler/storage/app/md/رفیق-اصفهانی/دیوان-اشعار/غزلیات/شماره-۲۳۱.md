---
title: >-
    شمارهٔ ۲۳۱
---
# شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>به از عشق و گدایی منصب و جاهی نمی دانم</p></div>
<div class="m2"><p>گدای عشقم و خود را کم از شاهی نمی دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیم از زور بازو کوهکن لیکن چو کار افتد</p></div>
<div class="m2"><p>به پیش همت خود کوه را کاهی نمی دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سوی مقصد ای خضرم خدا را رهنمایی کن</p></div>
<div class="m2"><p>غریب و بی کس و سرگشته ام راهی نمی دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکردی باخبر یار مرا از حال زار من</p></div>
<div class="m2"><p>چرا امشب دگر ای ناله کوتاهی نمی دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نباشد ای پسر حسنی چنین فرزند آدم را</p></div>
<div class="m2"><p>فرشته یا پری یا مهر یا ماهی نمی دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزارم درد دل باشد ولی از بی زبانیها</p></div>
<div class="m2"><p>چو می بینم ترا من ناله و آهی نمی دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفیق از من مپرس احوال من پیوسته در عشقش</p></div>
<div class="m2"><p>که گاهی حال خود می دانم و گاهی نمی دانم</p></div></div>