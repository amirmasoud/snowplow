---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>راضیم هر چه می داری به این خواری مرا</p></div>
<div class="m2"><p>دیگران را گر چنین داری که می داری مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با جفایت هم خوشم ترک جفاکاری مکن</p></div>
<div class="m2"><p>کز تو نبود بیش از این چشم وفاداری مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست با صیاد الفت بهر آب و دانه ام</p></div>
<div class="m2"><p>کرده پا بست قفس ذوق گرفتاری مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی دهم دامان وصل او به آسانی ز دست</p></div>
<div class="m2"><p>کامد اندر دست این دولت به دشواری مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاری من همچو غیر از بیم آزار تو نیست</p></div>
<div class="m2"><p>می کنم زاری که می ترسم نیازاری مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدعی تا کی ز کوی یار منعم، چون شود</p></div>
<div class="m2"><p>بگذری از من ز راه لطف و بگذاری مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم از درد تغافل ناتوان چند ای طبیب</p></div>
<div class="m2"><p>بینی از درد خود و نادیده انگاری مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه کند یادم به عمد و نه برد نامم به سهو</p></div>
<div class="m2"><p>برده است از یاد خود یکباره پنداری مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوش آن گل نیست چون بر گفتگوی من رفیق</p></div>
<div class="m2"><p>سود کی دارد چو بلبل نغز گفتاری مرا</p></div></div>