---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>گر مصور مه من نقش تو دلخواه کشد</p></div>
<div class="m2"><p>نکشد همچو تو دلخواه، مگر ماه کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن من کاهی و بار غم تو کوهی، چون</p></div>
<div class="m2"><p>بار چون کوه کسی با تن چون کاه کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به کی ای مه بی مهر ز هجرت شب و روز</p></div>
<div class="m2"><p>چشم من اشک نشاند، دل من آه کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هفته ی هجر تو ای ماه نماید سالی</p></div>
<div class="m2"><p>آه ای ماه اگر هجر تو یک ماه کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محتسب گوید اگر می نکشم در همه عمر</p></div>
<div class="m2"><p>مشنو از وی که اگر گه نکشد گاه کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خوش آن بزم که از اول شب تا دم صبح</p></div>
<div class="m2"><p>با تو من می کشم و غیر ز من آه کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز ز دست تو بت حور لقا گرچه رفیق</p></div>
<div class="m2"><p>از کف حور کشد باده به اکراه کشد</p></div></div>