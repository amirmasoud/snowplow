---
title: >-
    شمارهٔ ۲۱۳
---
# شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>امروز بی تو خاک چنین گر به سر کنم</p></div>
<div class="m2"><p>روز جزا عجب که سر از خاک بر کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند چاره کن [به] سفر عشق یار را</p></div>
<div class="m2"><p>یارم نمی کند چو سفر چون سفر کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی کنم بدامن گلچین نظاره گل</p></div>
<div class="m2"><p>کنج قفس کجاست که سر زیر پر کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی به حسرتش نگرم با رقیب و باز</p></div>
<div class="m2"><p>از گریه منع دیده ی حسرت نگر کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی ز خوان نعمت الوان روزگار</p></div>
<div class="m2"><p>با لخت دل قناعت [و] خون جگر کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک روز بر سرم زرهی تا گذر کنی</p></div>
<div class="m2"><p>هر روز جای بر سر هر رهگذر کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسیار تندخوست نکوروی من ولی</p></div>
<div class="m2"><p>رویش نمی هلد که ز خویش گذر کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از حال من تو فارغ و من در خیال تو</p></div>
<div class="m2"><p>روزی به شب رسانم و شامی سحر کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گریند اهل حشر به من پیش دادگر</p></div>
<div class="m2"><p>چون گریه از جفای تو بیدادگر کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیشت خوش آنکه گریم و گویی به خنده تو</p></div>
<div class="m2"><p>کم کن رفیق گریه و، من بیشتر کنم</p></div></div>