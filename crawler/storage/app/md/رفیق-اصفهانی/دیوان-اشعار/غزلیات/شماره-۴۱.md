---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>تا دست می دهد می و معشوق می پرست</p></div>
<div class="m2"><p>از کف منه پیاله و فرصت مده ز دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردی چو صید خو دل ما را مده ز دست</p></div>
<div class="m2"><p>مشکل فتد به دام چو صیدی ز دام جست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامن کشان مرو ز بر من خدای را</p></div>
<div class="m2"><p>بنشین دمی ز پا که دلم می رود ز دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم شست دست از دل و هم کند دل ز جان</p></div>
<div class="m2"><p>هر کس چو من به دست کسی گشت پای بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دستبرد حادثه در زیر خاک به</p></div>
<div class="m2"><p>در پای یار سر که نباشد چو خاک پست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پیش دیده یارم اگر رفت باک نیست</p></div>
<div class="m2"><p>جایی نمی رود ز دلم هر کجا که هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پهلوی غیر چند نشینی به رغم من</p></div>
<div class="m2"><p>پیش رفیق هم نفسی می توان نشست</p></div></div>