---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>سوی آن کز تو دلش خوش به نگاهی باشد</p></div>
<div class="m2"><p>سهل باشد که نگاهی ز تو گاهی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانع ای ابر کرم چیست که از رشحه ی تو</p></div>
<div class="m2"><p>قطره ای قسمت لب تشنه گیاهی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ارتو نسبت به من آن جور که باشد همه وقت</p></div>
<div class="m2"><p>به ز لطفی که نباشد گه و گاهی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایخوش آن خسته که چون بر سرش آئی او را</p></div>
<div class="m2"><p>قوه ناله نه و قدرت آهی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو که چشمت نگران نیست به راهی چه غمت</p></div>
<div class="m2"><p>که به راهت نگران چشم به راهی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی آن دم که نباشی کنمت یاد رفیق</p></div>
<div class="m2"><p>آندم این حرف بیاد تو الهی باشد</p></div></div>