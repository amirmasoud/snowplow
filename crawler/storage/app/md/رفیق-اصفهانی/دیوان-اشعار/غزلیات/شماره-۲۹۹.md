---
title: >-
    شمارهٔ ۲۹۹
---
# شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>در چمن جلوه گر ای سرو قد ساده کنی</p></div>
<div class="m2"><p>سرو را بندهٔ آن قامت آزاده کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ نمایی و ربائی دل و گردی پنهان</p></div>
<div class="m2"><p>آدمی زاده ای و کار پریزاده کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمرها شد که به راه توام افتاده که تو</p></div>
<div class="m2"><p>هر به عمری گذری بر من افتاده کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرو باده کن و رهن می ای زاهد چند</p></div>
<div class="m2"><p>فخر از خرقه، مباهات ز سجاده کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با گل اندامی و با سرو قدی در چمنی</p></div>
<div class="m2"><p>گر دهد دست که برگ طرب آماده کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهد کن جهد که در پای گل و سایه ی سرو</p></div>
<div class="m2"><p>شیشه خالی ز می و جام پر از باده کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باده با ساده رخی نوش که تا همچو رفیق</p></div>
<div class="m2"><p>باده نوشی و تماشای رخ ساده کسی</p></div></div>