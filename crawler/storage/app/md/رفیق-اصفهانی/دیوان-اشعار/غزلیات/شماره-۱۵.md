---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>از بهر یار دیده به کار است بس مرا</p></div>
<div class="m2"><p>چشم از برای دیدن یار است بس مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین نقشهای نغز در آینده در خیال</p></div>
<div class="m2"><p>نقشی که بست نقش نگار است، بس مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردی ز کوی یار بیار ای نسیم صبح</p></div>
<div class="m2"><p>کان توتیای دیده ی تار است بس مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن غرقه ی محیط ملالم که تا ابد</p></div>
<div class="m2"><p>در خاطر آنچه نقش کنار است بس مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از درد و داغ آنچه بگوئی رفیق هست</p></div>
<div class="m2"><p>چیزی که نیست صبر و قرار است بس مرا</p></div></div>