---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>خوش تر ز طوبی است به خوبی نهال تو</p></div>
<div class="m2"><p>طوبی کجا و قامت طوبی مثال تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماهی تو نه که ماه نباشد چنین جمیل</p></div>
<div class="m2"><p>مهری تو نه که مهر ندارد جمال تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاشا که آفتاب همه روزه در زوال</p></div>
<div class="m2"><p>باشد چو آفتاب رخ بی زوال تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ماه چارده تو به نیکی گذشته ای</p></div>
<div class="m2"><p>از چارده همان نگذشته است سال تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می زیبدش تحکم گل های بوستان</p></div>
<div class="m2"><p>سروی که در چمن بودش اعتدال تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرخنده باد طالع تو چون رخت مدام</p></div>
<div class="m2"><p>فرخنده فالم از رخ فرخنده فال تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دی دیدمت به غیر و ز من منفعل شدی</p></div>
<div class="m2"><p>من نیز منفعل شدم از انفعال تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوراب چشمی و لب خشکی مرا بس است</p></div>
<div class="m2"><p>ای خضر از تو چشمه ی آب زلال تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد ختم بر تو شیوه ی قول و غزل رفیق</p></div>
<div class="m2"><p>بالجمله قائلیم به حسن مقال تو</p></div></div>