---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>نگار من عجب زیبا نگاری است</p></div>
<div class="m2"><p>نگار سرو قد گلعذاری است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شهرآشوب رخ، آشوب شهریست</p></div>
<div class="m2"><p>بشور انگیز قد، شور دیاری است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به قید زلف طرفه صید بند است</p></div>
<div class="m2"><p>بدام خط عجب عاشق شکاری است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گدای اوست هر جا پادشاهی است</p></div>
<div class="m2"><p>غلام اوست هرجا شهریاری است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر کوئی ز دردش دردمندیست</p></div>
<div class="m2"><p>بهر سوئی ز داغش داغداری است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه محرومان کویش را حسابیست</p></div>
<div class="m2"><p>نه مشتاقان رویش را شماری است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفیق و شغل عشق او که این شغل</p></div>
<div class="m2"><p>خجسته پیشه و فرخنده کاری است</p></div></div>