---
title: >-
    شمارهٔ ۱۹۱
---
# شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>آمدی رفتی از برم غافل</p></div>
<div class="m2"><p>صبر و هوشم ربودی از سر و دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گل از عارض تو گشته خجل</p></div>
<div class="m2"><p>سرو پیش قد تو پا در گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای به رخ رشک لعبتان ختا</p></div>
<div class="m2"><p>ای به قد غیرت بتان چگل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلبر دیر صلح زود عتاب</p></div>
<div class="m2"><p>مه نامهربان مهر گسل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردی از لطف و جور روشن و تار</p></div>
<div class="m2"><p>غیر را مجلس و مرا محفل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلخن و گلشن از تو غیر و مرا</p></div>
<div class="m2"><p>شب و روز است مسکن و منزل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی تو و با تو تا کی و تا چند</p></div>
<div class="m2"><p>من دل افگار و مدعی خوشدل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی من ای یار کار تو آسان</p></div>
<div class="m2"><p>بی تو ای دوست کار من مشکل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی تو از دیگران ملول رفیق</p></div>
<div class="m2"><p>تو به رغمش به دیگران مایل</p></div></div>