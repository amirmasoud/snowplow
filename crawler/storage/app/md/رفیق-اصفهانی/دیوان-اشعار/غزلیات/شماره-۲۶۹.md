---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>گر سر ببرندم نکشم پا ز در تو</p></div>
<div class="m2"><p>پروای سر خویش ندارم به سر تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در راه تو آن خاک نشینم که نخیزد</p></div>
<div class="m2"><p>گر خاک شوم خاک من از رهگذر تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسیار کم افتد نظر لطف تو با من</p></div>
<div class="m2"><p>افتاده ام آیا بچه جرم از نظر تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اغیار به او محرم و محروم ازو من</p></div>
<div class="m2"><p>ای ناله چه شد سوز و کجا شد اثر تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی کشم از جور و جفای تو ز دل آه</p></div>
<div class="m2"><p>آه از دل از مهر و وفا بی خبر تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر قیمت هر بوسه دلی خواهی و جانی</p></div>
<div class="m2"><p>بهر دل و جان کیست که خواهد ضرر تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق تو رفیق از لب خشک تو نشد فاش</p></div>
<div class="m2"><p>رسوای جهان کرد ترا چشم تر تو</p></div></div>