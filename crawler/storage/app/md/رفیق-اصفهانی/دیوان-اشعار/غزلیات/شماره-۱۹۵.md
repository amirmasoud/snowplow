---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>بیا ای مونس شبهای تارم</p></div>
<div class="m2"><p>که از هجرت سیه شد روزگارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا جانا که تا رفتی تو رفته است</p></div>
<div class="m2"><p>هم از جان صبر و هم از دل قرارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشا روزی که با خیل سگانش</p></div>
<div class="m2"><p>در آن کو پاسبانی بود کارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدم تا از سگان کوی او دور</p></div>
<div class="m2"><p>به چشم مردمان بی اعتبارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آنم بعد ازین چون نیست راهی</p></div>
<div class="m2"><p>ز جور مدعی در کوی یارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشینم بر سر راهش که ناگاه</p></div>
<div class="m2"><p>از این ره بگذرد گر شهسوارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ابر دیده بارم اشک شاید</p></div>
<div class="m2"><p>کند رحمی به چشم اشکبارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس ترسیده ام از هجر زین پس</p></div>
<div class="m2"><p>به کوی یار اگر افتد گذارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفیق آنجا شوم گر خاک مشکل</p></div>
<div class="m2"><p>برد باد از سر کویش غبارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به من گر دشمن جان است یارم</p></div>
<div class="m2"><p>من او را از دل و جان دوست دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنارم شد ز خون دیده گلگون</p></div>
<div class="m2"><p>که رفت آن خرمن گل از کنارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دهم جان و خوشم گر روشن از تو</p></div>
<div class="m2"><p>نشد در زندگی شبهای تارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که شاید شمع رخسار تو گردد</p></div>
<div class="m2"><p>چراغ تربت و شمع مزارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به درد و غم از آن نالم شب و روز</p></div>
<div class="m2"><p>که دور از یار و مهجور از دیارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عنان نگرفتم او را آخر افسوس</p></div>
<div class="m2"><p>که رفت از کف عنان اختیارم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رفیق از آه گرمم می توان یافت</p></div>
<div class="m2"><p>که پنهان آتشی در سینه دارم</p></div></div>