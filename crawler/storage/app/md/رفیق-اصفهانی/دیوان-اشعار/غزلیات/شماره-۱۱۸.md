---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>کو عاشق آزاری چو او تا عاشق زارش کند</p></div>
<div class="m2"><p>شاید که درد عاشقی با عاشقان یارش کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم بتی چون یار من دل گیرد از دلدار من</p></div>
<div class="m2"><p>تا آن چه او در کار من کرده است در کارش کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غارت کند از یک نظر صبرش ز دل هوشش ز سر</p></div>
<div class="m2"><p>سازد ز خویشش بی خبر از من خبردارش کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیرون کند آن دلربا از خاطرش جور و جفا</p></div>
<div class="m2"><p>آموزدش مهر و وفا عاشق نگه دارش کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا آن بت پیمان شکن قدری فزاید قدر من</p></div>
<div class="m2"><p>یک چند پیش خویشتن بی قدر و مقدارش کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چشم خواب آلود خویش از لعل می آلود خویش</p></div>
<div class="m2"><p>خوابست بیدارش کند مست است هشیارش کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردد رفیق ممتحن خوش نغمه چون مرغ چمن</p></div>
<div class="m2"><p>گر آن بت غنچه دهن گوشی به گفتارش کند</p></div></div>