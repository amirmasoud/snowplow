---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>کشم جور از غمت ای نازنین ماه</p></div>
<div class="m2"><p>شب و روز از دل و جان ناله و آه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رود از دوریت تا کی شب و روز</p></div>
<div class="m2"><p>سرشکم تا به ماهی آه تا ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو خورشیدی و مه رویان کواکب</p></div>
<div class="m2"><p>نکورویان شهند و تو شهنشاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تفاوت از زمین تا آسمانست</p></div>
<div class="m2"><p>ز ماه عارضت تا عارض ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخوردم ای رفیق از قدش افسوس</p></div>
<div class="m2"><p>از آن نخل بلند و قد کوتاه</p></div></div>