---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>ز جور دوست گر خود را به کام دشمنان دیدم</p></div>
<div class="m2"><p>سزاوار است که دشمن دوستش گفتند نشنیدم (؟)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شنیدم یار بی مهر و وفا بسیار [و] دیدم پسر</p></div>
<div class="m2"><p>ندیدم چون تو بی مهر و وفا یاری و نشنیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تویی آن دلبر بدخوی شادی کاه غم افزا</p></div>
<div class="m2"><p>که من خود را ندیدم شاد دیگر تا ترا دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم آن عاشق رنجور کز بس بردباری‌ها</p></div>
<div class="m2"><p>همیشه رنج دیدم از تو و هرگز نرنجیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نچیدم جز گل حسرت ز گلزار وصال تو</p></div>
<div class="m2"><p>بمژگان گرچه عمری خار از راه تو برچیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدندان می گزم گر پشت دست خود کنون شاید</p></div>
<div class="m2"><p>که عمری چون رفیق آخر چرا پای تو بوسیدم</p></div></div>