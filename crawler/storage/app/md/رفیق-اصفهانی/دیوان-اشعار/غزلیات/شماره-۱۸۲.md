---
title: >-
    شمارهٔ ۱۸۲
---
# شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>منم از چشم سیاهی به نگاهی قانع</p></div>
<div class="m2"><p>به نگاهی شده از چشم سیاهی قانع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح تا شام به نظاره ی مهری محظوظ</p></div>
<div class="m2"><p>شام تا صبح به اندیشه ی ماهی قانع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تمنای رخی بر سر کویی ساکن</p></div>
<div class="m2"><p>به تماشای قدی بر سر راهی قانع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ار به آزردن من هر دم و هر لحظه حریص</p></div>
<div class="m2"><p>من به پرسیدن او گاه به گاهی قانع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست قانع دل من با رخش از سبزخطان</p></div>
<div class="m2"><p>هست تا گل نتوان شد به گیاهی قانع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میل نظاره ی ماه فلکم نیست رفیق</p></div>
<div class="m2"><p>که به ماهی شدم از طرف کلاهی قانع</p></div></div>