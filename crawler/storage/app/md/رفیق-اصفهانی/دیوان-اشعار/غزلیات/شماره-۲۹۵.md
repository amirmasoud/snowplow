---
title: >-
    شمارهٔ ۲۹۵
---
# شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>به غیر آن ماه را بی‌مهر با من مهربان کردی</p></div>
<div class="m2"><p>خلاف عادت خودکردنی ای آسمان کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهادی داغ هجرم بر دل و از دیده ام رفتی</p></div>
<div class="m2"><p>دلم را خون چکان و دیده ام را خونفشان کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشو از حرف بدگو بدگمان وز در مران ما را</p></div>
<div class="m2"><p>که عمری آزمودی روزگاری امتحان کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعشوه طاقت جان و تن شیخ و صبی بردی</p></div>
<div class="m2"><p>بغمزه غارت دین و دل پیر و جوان کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارد بر فغان و ناله ام گوشی رفیق ارنه</p></div>
<div class="m2"><p>بکویش روزها نالیدی و شبها فغان کردی</p></div></div>