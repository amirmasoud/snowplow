---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>زاهد به جرم مستی عشقم، شتاب چیست</p></div>
<div class="m2"><p>عاشق چو مست گشت گناه و ثواب چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عاشقان برون ز حسابند، پس مرا</p></div>
<div class="m2"><p>اندیشه ی حساب به روز حساب چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کس که محو روی تو باشد به روز و شب</p></div>
<div class="m2"><p>کی آگه است ماه چه و آفتاب چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی مده شراب که از چشم مست تو</p></div>
<div class="m2"><p>مستم من آنچنان که ندانم شراب چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک بوسه بیش نیست سؤال من از درت</p></div>
<div class="m2"><p>با من به تلخی از لب شیرین جواب چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بردار پرده از رخ خود ماه من کسی</p></div>
<div class="m2"><p>چون تاب دیدن تو ندارد نقاب چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساغر به عزم توبه منه بر زمین، رفیق</p></div>
<div class="m2"><p>در فصل گل ز باده ی ناب اجتناب چیست</p></div></div>