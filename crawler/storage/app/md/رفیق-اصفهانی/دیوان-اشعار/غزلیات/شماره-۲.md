---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>به سر دارم هوای سجده خاک آستانی را</p></div>
<div class="m2"><p>که شاهانند آنجا بنده کمتر پاسبانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گدای بی زر و سیمم که با خود یار می خواهم</p></div>
<div class="m2"><p>شه سیمین رکابی خسرو زرین عنانی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پیری بر جوانی عاشقم کز عاشقان دارد</p></div>
<div class="m2"><p>چو من هر گوشه پیری را چو خود هر نوجوانی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نچیدم میوه ای از باغ وصل او چو من هرگز</p></div>
<div class="m2"><p>نشد بی حاصلی حاصل ز باغی باغبانی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان سازد دل و جان منش هر جا فلک بیند</p></div>
<div class="m2"><p>بت مژگان خدنگی دلبر ابرو کمانی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وفا می ورزم و بیداد می بینم به امیدی</p></div>
<div class="m2"><p>که روزی مهربان بینم بخود نامهربانی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی پرسد نشان و نام من آن مه رفیق اما</p></div>
<div class="m2"><p>که می پرسد نشان و نام بی نام و نشانی را</p></div></div>