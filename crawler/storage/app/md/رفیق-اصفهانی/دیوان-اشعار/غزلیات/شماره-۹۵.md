---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>کی جز تو در دل من دلدار دیگر آید</p></div>
<div class="m2"><p>بیرون نمی روی تو تا دیگری درآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با من مگو که بگذار از دست دامن یار</p></div>
<div class="m2"><p>این کار نیست کاری کز دست من برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از صنع کلک آید بس نقش نیک اما</p></div>
<div class="m2"><p>مشکل ز نقش رویت نقش نکوتر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جا که حور و طوبی گویند پیش چشمم</p></div>
<div class="m2"><p>قدت شود مجسم، رویت مصور آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد سر خیالت گردم که در دل من</p></div>
<div class="m2"><p>صد بار بیش آید آن دم که کمتر آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم به بر کی آیی ای رشک سرو و مه، گفت</p></div>
<div class="m2"><p>کی سرو آورد بر، کی ماه دربر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گیرم رفیق گوید ترک هوای جانان</p></div>
<div class="m2"><p>آن کیست کز وی او را این حرف باور آید</p></div></div>