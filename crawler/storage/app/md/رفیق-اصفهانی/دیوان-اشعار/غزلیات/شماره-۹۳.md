---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>فغان که مدعیان از منت جدا کردند</p></div>
<div class="m2"><p>مرا به درد جدائیت مبتلا کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به درد مردم و آن کافران سنگین دل</p></div>
<div class="m2"><p>نه رحم بر من و نه شرم از خدا کردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آخرم ز تو می ساختند بیگانه</p></div>
<div class="m2"><p>چرا نخست مرا با تو آشنا کردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مباد دیدن رویت نصیب آنکس را</p></div>
<div class="m2"><p>که از نظاره ی روی تو منع ما کردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زسست مهری مه طلعتان فغان کاین قوم</p></div>
<div class="m2"><p>به کس نه مهر نمودند و نه وفا کردند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهید عشق نخوانند آن شهیدان را</p></div>
<div class="m2"><p>که زیر تیغ ستم فکر خونبها کردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غریب نیست نکردند رحم اگر به رفیق</p></div>
<div class="m2"><p>ازان گروه که کردند جور تا کردند</p></div></div>