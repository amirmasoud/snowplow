---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>بیا ای بت دمی در کعبه از بتخانه مسکن کن</p></div>
<div class="m2"><p>بگردان کعبه را بتخانه زاهد را برهمن کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن محرومم از فیض نگاه گاه گاه خود</p></div>
<div class="m2"><p>به چشم مرحمت گاهی نگاهی جانب من کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشد تا سرو و گل شرمندگی زان عارض و قامت</p></div>
<div class="m2"><p>گهی مأوا به بستان گیر گاهی جا به گلشن کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانی گر یقین حال دل ما و دل خود را</p></div>
<div class="m2"><p>گمان شیشه و خارا قیاس سنگ و آهن کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنی تا چند هر شب شمع بزم غیر آن رخ را</p></div>
<div class="m2"><p>شبی بزم مرا زان ماه عارض نیز روشن کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا با دوست کاری نیست چون جز دشمنی کردن</p></div>
<div class="m2"><p>به دشمن ناتوانی دوست را ای دوست دشمن کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قبای هستیم دست اجل گو بی تو چاک ای دل</p></div>
<div class="m2"><p>ز دامن تا گریبان وز گریبان تا به دامن کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفیق اکنون که ناوک بر نشان می افکند جانان</p></div>
<div class="m2"><p>بلی جان را نشان ناوک آن تیرافکن کن</p></div></div>