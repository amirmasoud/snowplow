---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>بهر تو مرا پیشکشی جز دل و جان نیست</p></div>
<div class="m2"><p>در پیش تو ای جان جهان دل چه و جان چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این سرو روان می رود و در عقبش جان</p></div>
<div class="m2"><p>جان می رود از رفتنش این سرو روان کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او وعده به روز دگرم می دهد و من</p></div>
<div class="m2"><p>در فکر که تا روز دگر چون بتوان زیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد ز میم توبه مفرما که به جز می</p></div>
<div class="m2"><p>از هر چه بود توبه مرا هست وزان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سی روز لب از می نتوان بست چه بودی</p></div>
<div class="m2"><p>شوال چهل بودی و روز رمضان بیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم قد رعنای تو را سرو چو دیدم</p></div>
<div class="m2"><p>در راستی اینست الف در کجی آن نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایدوست به بالین رفیق ایست زمانی</p></div>
<div class="m2"><p>کش جان به طفیل تو کند یک دور زمان ایست</p></div></div>