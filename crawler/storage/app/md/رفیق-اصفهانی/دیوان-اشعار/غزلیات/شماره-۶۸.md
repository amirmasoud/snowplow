---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>چنان گشته کامم ز ایام تلخ</p></div>
<div class="m2"><p>که شهدم چو زهر است در کام تلخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ناکامی است این که گر کام من</p></div>
<div class="m2"><p>بود صبح شیرین شود شام تلخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر شهد نوشم شود همچو زهر</p></div>
<div class="m2"><p>به کام من درد آشام تلخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند تا کند دلبرم تلخ، کام</p></div>
<div class="m2"><p>لب شکرین را به دشنام تلخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آغاز شد گر چه عشاق را</p></div>
<div class="m2"><p>می و ساغر و باده و جام تلخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن ساغر و جام تا این زمان</p></div>
<div class="m2"><p>چو من تلخکامی نشد کام تلخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود عشق آن به که باشد رفیق</p></div>
<div class="m2"><p>در آغاز شیرین در انجام تلخ</p></div></div>