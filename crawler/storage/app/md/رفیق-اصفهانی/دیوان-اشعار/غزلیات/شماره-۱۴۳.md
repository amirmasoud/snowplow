---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>بی وفائی که علاج دل پرخونم کرد</p></div>
<div class="m2"><p>ریخت از تیغ جفا خونم و ممنونم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو در حسن و وفا لیلی ثانی گشتی</p></div>
<div class="m2"><p>در وفای تو جهان ثانی مجنونم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که افسانه ام امروز به شیرین سخنی</p></div>
<div class="m2"><p>لب شیرین تو از یک سخن افسونم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه همین کرد جدا بخت بد از یار مرا</p></div>
<div class="m2"><p>کرد هر کار به من طالع وارونم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدعی از سر کوی تو نرفت این سهل است</p></div>
<div class="m2"><p>رفته رفته ز سر کوی تو بیرونم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در همه شهر ز خوبان به یکی بودم شاد</p></div>
<div class="m2"><p>کرد او نیز ز من دوری و محزونم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر گه آن سرو روان شعر مرا خواند رفیق</p></div>
<div class="m2"><p>آفرین بر سخن دلکش موزونم کرد</p></div></div>