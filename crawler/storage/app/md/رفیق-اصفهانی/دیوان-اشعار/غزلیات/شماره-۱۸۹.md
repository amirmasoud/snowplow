---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>باور کس نشود قصه ی بیماری دل</p></div>
<div class="m2"><p>تا گرفتار نگردد به گرفتاری دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار بی رحم و به جان من ز گرفتاری دل</p></div>
<div class="m2"><p>کیست یاران که در این حال کند یاری دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و دل زار چنانیم که شب ها نکنند</p></div>
<div class="m2"><p>مردم از زاری من خواب من از زاری دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من روز نیاساید از این چشم پرآب</p></div>
<div class="m2"><p>چشم من شب نکند خواب ز بیداری دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل گرانم ز غم دهر بیاور ساقی</p></div>
<div class="m2"><p>قدحی چند می از بهر سبکباری دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه در زلف تو دلهای پریشان جمعست</p></div>
<div class="m2"><p>شانه را راه در آن نیست ز بسیاری دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نگه دارم از آن رشک پری دل که رفیق</p></div>
<div class="m2"><p>پیش او حد بشر نیست نگهداری دل</p></div></div>