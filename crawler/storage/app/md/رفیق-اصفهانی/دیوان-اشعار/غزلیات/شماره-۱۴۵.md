---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>آن که لطفش گره از خاطر ما بگشاید</p></div>
<div class="m2"><p>گره خاطر او لطف خدا بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس برای دل ما دست عطایی نگشود</p></div>
<div class="m2"><p>هم مگر اهل دلی دست عطا بگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خدا جوی گشایش که نگردد هرگز</p></div>
<div class="m2"><p>بسته آنکار که از کارگشا بگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز به کوی تو دل ما نگشاید آری</p></div>
<div class="m2"><p>دل که آنجا نگشاید بکجا بگشاید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ریاضی که سر و کار گلش با خار است</p></div>
<div class="m2"><p>کی دل بلبل بی برگ و نوا بگشاید؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طالعی کو که برم مست شبی یا روزی</p></div>
<div class="m2"><p>کله از سر بنهد بند قبا بگشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو جهان جان و دل از هر شکن آید بیرون</p></div>
<div class="m2"><p>گرهی کز سر آن زلف دو تا بگشاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اشک و آه شب و روز آب و هوا نیست رفیق</p></div>
<div class="m2"><p>که گل دولت از آن آب و هوا بگشاید</p></div></div>