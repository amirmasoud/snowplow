---
title: >-
    شمارهٔ ۳۰۳
---
# شمارهٔ ۳۰۳

<div class="b" id="bn1"><div class="m1"><p>ز می نام و نشان تا هست باقی</p></div>
<div class="m2"><p>من و میخانه و مینا و ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشست از دست خوبان صفاهان</p></div>
<div class="m2"><p>می شیرازی و رطل عراقی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علاج تلخکامان چون ندارند</p></div>
<div class="m2"><p>نکورویان به این شیرین مذاقی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غیر من ترا مهر و وفا چند</p></div>
<div class="m2"><p>بود آن دایمی این اتفاقی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو در کوی توام ره نیست گردد</p></div>
<div class="m2"><p>میسر با توام چون هم وثاقی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفیق از فرقت جانان ازین پس</p></div>
<div class="m2"><p>نگوید جز غزل‌های فراقی</p></div></div>