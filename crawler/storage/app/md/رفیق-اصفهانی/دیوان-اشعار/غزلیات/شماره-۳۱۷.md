---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>جان من گر دل به دست دلستانی داشتی</p></div>
<div class="m2"><p>رحم بر خون دل آزرده جانی داشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیش از این بودی به عاشق مهربان ای سنگدل</p></div>
<div class="m2"><p>گر بت سنگین دل نامهربانی داشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی خبر کی بودی از درد نهان من چنین</p></div>
<div class="m2"><p>گر چو من در عاشقی درد نهانی داشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منع من کردی کجا ز آه و فغان در عشق اگر</p></div>
<div class="m2"><p>از غم معشوقه ای آه و فغانی داشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی گمان بد به من می داشتی در عشق خویش</p></div>
<div class="m2"><p>گر چو خود عاشق فریب و بدگمانی داشتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر دل من کی زدی ناوک اگر زخمی به دل</p></div>
<div class="m2"><p>از خدنگ غمزهٔ ابروکمانی داشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داشتی بر جان و دل گر درد و داغی ای رفیق</p></div>
<div class="m2"><p>آه آتشبار و چشم خون‌فشانی داشتی</p></div></div>