---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>با آنکه وفادارتر از من دگری نیست</p></div>
<div class="m2"><p>در راه وفا خوارتر از من دگری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار بود مایل روی تو ولیکن</p></div>
<div class="m2"><p>مایل به تو بسیار تر از من دگری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار تو شدم ترک کسان کردم و اکنون</p></div>
<div class="m2"><p>بی کس تر و بی یارتر از من دگری نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چاره ی من سعی فزون از دگران کن</p></div>
<div class="m2"><p>کز درد تو بیمارتر از من دگری نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدم ز خودم مست تر امروز رفیق آنک</p></div>
<div class="m2"><p>دی گفت که هشیارتر از من دگری نیست</p></div></div>