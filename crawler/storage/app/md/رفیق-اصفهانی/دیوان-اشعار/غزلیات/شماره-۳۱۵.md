---
title: >-
    شمارهٔ ۳۱۵
---
# شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>نگاه دلکش و رفتار دلستان که تو داری</p></div>
<div class="m2"><p>دلی زهر که شود گم برد گمان که تو داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برابر است به جان خاک آستان که تو داری</p></div>
<div class="m2"><p>توان کشید به جان ناز پاسبان که تو داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآید از دهنت کام من بیک سخن اما</p></div>
<div class="m2"><p>سخن چگونه برآید از آن دهان که تو داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارد از دل گم گشته ام کسی خبر و من</p></div>
<div class="m2"><p>ز خنده های نهان دارم این گمان که تو داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن به خوردن خون خوی ای پسر که ز خردی</p></div>
<div class="m2"><p>هنوز بوی لبن دارد این دهان که تو داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهای یکدم وصلش هزار جان بود ای دل</p></div>
<div class="m2"><p>ترا ازو چه تمتع به نیم جان که تو داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خامه ی دو زبان وام کن رفیق زبانی</p></div>
<div class="m2"><p>که شرح شوق نمی داند این زبان که تو داری</p></div></div>