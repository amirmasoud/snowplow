---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>بود اگر با قد موزون آفتاب</p></div>
<div class="m2"><p>چون تو بود ای تو به رخ چون آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آفتابی ماه من افزون بحسن</p></div>
<div class="m2"><p>آنقدر کز ماه افزون آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوه عشقت راست فرهاد آسمان</p></div>
<div class="m2"><p>دشت شوقت راست مجنون آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چمن سروی و در گلزار، گل</p></div>
<div class="m2"><p>بر فلک ماهی، به گردون آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون تو بود ای ماه از تو روی زرد</p></div>
<div class="m2"><p>چون تو بود ای از تو دل خون آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داشت گر این طره ی شبرنگ، ماه</p></div>
<div class="m2"><p>داشت گر این لعل میگون آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی چون ماه تو در چشم رفیق</p></div>
<div class="m2"><p>می نماید چون به جیحون آفتاب</p></div></div>