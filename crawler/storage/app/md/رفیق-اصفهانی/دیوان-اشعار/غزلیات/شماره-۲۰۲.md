---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>روزگاری روز خوش از وصل یاری داشتم</p></div>
<div class="m2"><p>داشتم روز خوش و خوش روزگاری داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فارغ از بیم فراغ آسوده از امید وصل</p></div>
<div class="m2"><p>نه غم هجری نه درد انتظاری داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده بر رخسار جانان دست در آغوش یار</p></div>
<div class="m2"><p>جرأت بوسی و یارای کناری داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه دلم بی صبر بود از غم نه جانم بی قرار</p></div>
<div class="m2"><p>هم بدل صبری و هم در جان قراری داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معتبر بودم به نزدیک سگان کوی دوست</p></div>
<div class="m2"><p>با همه بی اعتباری اعتباری داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه هرگز شیوه ی یاری نمی دیدم ز یار</p></div>
<div class="m2"><p>داشتم خاطر به این خرم که یاری داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود تا کویش دیارم تا سگش یارم رفیق</p></div>
<div class="m2"><p>خوش دیار و یاری و یار دیاری داشتم</p></div></div>