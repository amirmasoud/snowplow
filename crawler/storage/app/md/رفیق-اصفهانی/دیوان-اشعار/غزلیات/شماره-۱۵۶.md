---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>هر دم ای یار ز تو می کشم آزار دگر</p></div>
<div class="m2"><p>چه کنم نیست به غیر تو مرا یار دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز تو دلدار دگر نیست و گر باشد نیست</p></div>
<div class="m2"><p>دل دیگر که دهم جز تو به دلدار دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست ای یار جفاکار بسی یار اما</p></div>
<div class="m2"><p>همچو من نیست ترا یار وفادار دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوبرویان همه هستند ستمکار ولی</p></div>
<div class="m2"><p>به ستمکاری تو نیست ستمکار دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بکوی توام ای بار گذر افتد باز</p></div>
<div class="m2"><p>تا بود عمر از آن کو نروم بار دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناصح از عشق مکن منع من زار که هست</p></div>
<div class="m2"><p>کار من عشق و جز این نیست مرا کار دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رود از گلشن کوی تو کجا، نیست رفیق</p></div>
<div class="m2"><p>عندلیبی که شود بلبل گلزار دگر</p></div></div>