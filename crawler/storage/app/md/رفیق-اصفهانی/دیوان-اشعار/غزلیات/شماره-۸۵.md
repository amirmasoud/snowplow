---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>مرا به باده کشی کاش آنکه عیب کند</p></div>
<div class="m2"><p>رود به میکده تا سیر سر غیب کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوان کند می گلرنگ پیر را چه عجب</p></div>
<div class="m2"><p>از آنکه توبه ز می در زمان شیب کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به محفلی که تو ساقی شوی نمی دانم</p></div>
<div class="m2"><p>که اجتناب ز صهبا چسان صهیب کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گلشنی که گریبان جامه باز کنی</p></div>
<div class="m2"><p>ز انفعال تو گل سر فرو بجیب کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفیق، شک برد از دل شراب، اهل یقین</p></div>
<div class="m2"><p>نباشد آنکه در این نکته شک و ریب کند</p></div></div>