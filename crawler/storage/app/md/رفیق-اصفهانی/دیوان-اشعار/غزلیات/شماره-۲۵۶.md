---
title: >-
    شمارهٔ ۲۵۶
---
# شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>عاشقان را به جفا خوار مکن</p></div>
<div class="m2"><p>مکن ای شوخ جفا کار مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهر در ساغر احباب مریز</p></div>
<div class="m2"><p>می به پیمانه ی اغیار مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار اغیار به یکبار مساز</p></div>
<div class="m2"><p>ترک احباب به یکبار مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بتان جور و جفا با عشاق</p></div>
<div class="m2"><p>گر چه خوبست تو بسیار مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترسم از زاریم آزرده شوی</p></div>
<div class="m2"><p>مکن آزار من زار، مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقل و می خوردن شب با مستان</p></div>
<div class="m2"><p>روز با مردم هشیار مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا مپرس از غم ما یا ما را</p></div>
<div class="m2"><p>به غم خویش گرفتار مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار من ساخته حاشایت چیست</p></div>
<div class="m2"><p>هست این کار تو انکار مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بارها حرف کسان کردی گوش</p></div>
<div class="m2"><p>گوش کن حرف من این بار مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می کنی جور و جفا گر برفیق</p></div>
<div class="m2"><p>به رفیقان وفادار مکن</p></div></div>