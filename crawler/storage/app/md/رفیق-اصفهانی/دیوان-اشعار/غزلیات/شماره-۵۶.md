---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>نه روی گل چو روی دل آرای دلبر است</p></div>
<div class="m2"><p>نه قد سرو چو قد رعنای دلبر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشرت در آن سر است که دلبر بود در آن</p></div>
<div class="m2"><p>دولت بر آن سر است که بر پای دلبر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سودای دلبر از سر من کی رود چنین</p></div>
<div class="m2"><p>کاندر دلم همیشه تمنای دلبر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا جای دلبر است دلم خرمست و شاد</p></div>
<div class="m2"><p>ای شاد آن دلی که در آن جای دلبر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خلد اگر رود نگشاید به حور چشم</p></div>
<div class="m2"><p>آن را که دیده محو تماشای دلبر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس کوته است جامه ی خوبی به قد سرو</p></div>
<div class="m2"><p>این خوب جامه راست به بالای دلبر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جام دیگران می عشرت مجو رفیق</p></div>
<div class="m2"><p>کاین باده ی نشاط به مینای دلبر است</p></div></div>