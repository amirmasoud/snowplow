---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>نیاید تا به لب از ضعف جانم</p></div>
<div class="m2"><p>نمی آید به لب از دل فغانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به داغت سوختی جان من از هجر</p></div>
<div class="m2"><p>چه می خواهی ز جان ناتوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجو تاب و توان از من که بی تو</p></div>
<div class="m2"><p>شد از تن تاب و رفت از دل توانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جوی دیده اشک من روانست</p></div>
<div class="m2"><p>که رفت از دیده آن سرو روانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی بینی اگر خونین دلم را</p></div>
<div class="m2"><p>نگاهی کن به چشم خون فشانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیاید غیر فکرت در ضمیرم</p></div>
<div class="m2"><p>نباشد غیر ذکرت بر زبانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفیق از دوری آن مه شب و روز</p></div>
<div class="m2"><p>رود آه و فغان بر آسمانم</p></div></div>