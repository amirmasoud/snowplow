---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>ماهی تو و جات طرف بامت</p></div>
<div class="m2"><p>یا مهری و آسمان مقامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه مهرت و گاه ماه گویم</p></div>
<div class="m2"><p>تازین دو خوش آید از کدامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه مه بیند نه مهر، بیند</p></div>
<div class="m2"><p>چون مهر و مه آنکه صبح و شامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن مهری و آن مهی که در حسن</p></div>
<div class="m2"><p>چون مهر و مهست صد غلامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردد چه هلال مه به تعظیم</p></div>
<div class="m2"><p>بیند مه اگر ز طرف بامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابیات رفیق اگر نویسند</p></div>
<div class="m2"><p>بر صفحه ی مهر و مه کلامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوید شنود گر نظامی</p></div>
<div class="m2"><p>احسنت به نظم بانظامت</p></div></div>