---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>ز کوی یار به من زان خبر نمی آید</p></div>
<div class="m2"><p>که هر که می رود آنجا دگر نمی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مده به روز دگر وعده ی وصال و مرو</p></div>
<div class="m2"><p>که بی تو صبر ز من این قدر نمی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کار عاشقی ای عیب جو مکن عیبم</p></div>
<div class="m2"><p>که غیر از این هنر از من هنر نمی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فغان ز سختی آن دل که نرم کردن آن</p></div>
<div class="m2"><p>ز ناله ی شب و آه سحر نمی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به این جمال کسی را که در نظر آیی</p></div>
<div class="m2"><p>جمال مهر و مهش در نظر نمی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در این چمن منم آن باغبان که پروردم</p></div>
<div class="m2"><p>هزار نخل و یکی زان به بر نمی آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی شود ز تو کام دلش روا اما</p></div>
<div class="m2"><p>رفیق با دل خودکام برنمی آید</p></div></div>