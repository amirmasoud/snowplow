---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>به جانم از غم جانان چه سازم</p></div>
<div class="m2"><p>به جانان چون کنم با جان چه سازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجز جان تحفه ی جانان چه سازم</p></div>
<div class="m2"><p>ندارم تحفه ای جز جان چه سازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی داند ز دشمن آن پسر دوست</p></div>
<div class="m2"><p>نمی دانم به این نادان چه سازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلت دیر آشناخو، بی سبب رنج</p></div>
<div class="m2"><p>بسازم گر به این با آن چه سازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به امید دوا سازند با درد</p></div>
<div class="m2"><p>ندارد درد من درمان چه سازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفیق از ناله گیرم لب ببندم</p></div>
<div class="m2"><p>به این مژگان خون افشان چه سازم</p></div></div>