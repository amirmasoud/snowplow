---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>چه می در جام من پیر مغان کرد</p></div>
<div class="m2"><p>که در پیرانه سر بازم جوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه دربانم جدا زان آستان کرد</p></div>
<div class="m2"><p>جدا زان آستانم آسمان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گمان بد به من آن بی وفا برد</p></div>
<div class="m2"><p>مرا آن بی وفا چون خود گمان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمی کان رشک سرو و غیرت گل</p></div>
<div class="m2"><p>به عزم سیر جا در گلستان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بلبل گل از آن رخ ناله برداشت</p></div>
<div class="m2"><p>چو قمری سرو از آن قامت فغان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزاران روز شب کردی به اغیار</p></div>
<div class="m2"><p>شبی هم روز با ما می توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مزن بر ما به رندی طعنه ای شیخ</p></div>
<div class="m2"><p>ترا هر کس چنین، ما را چنان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفیق این دولت من بس، که بختم</p></div>
<div class="m2"><p>گدای درگه پیر مغان کرد</p></div></div>