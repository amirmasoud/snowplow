---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>از درت امروز و فردا ای دلارا می روم</p></div>
<div class="m2"><p>گر نرفتم از درت امروز فردا می روم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاقت و عقل و شکیب و صبر و هوش و جان و دل</p></div>
<div class="m2"><p>می گذارم جمله را پیش تو تنها می روم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست پای رفتنم گاه وداع از پیش تو</p></div>
<div class="m2"><p>همچو شمع استاده ام در گریه اما می روم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کجا آخر رسد کارم از این رفتن که من</p></div>
<div class="m2"><p>با دلی و یک جهان حسرت از اینجا می روم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می روم از کوی او بیرون و در باطن رفیق</p></div>
<div class="m2"><p>حسرتی دارم که پنداری ز دنیا می روم</p></div></div>