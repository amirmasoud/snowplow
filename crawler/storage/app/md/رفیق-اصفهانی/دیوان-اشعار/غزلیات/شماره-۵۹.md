---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>کسی چون تو بلا هرگز ندیده است</p></div>
<div class="m2"><p>چو من کس مبتلا هرگز ندیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من لب تشنه زان لب هر چه دیدم</p></div>
<div class="m2"><p>خضر ز آب بقا هرگز ندیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم آن دیده است از جذبه ی عشق</p></div>
<div class="m2"><p>که کاه از کهربا هرگز ندیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من آن بسیار از جان ناامیدم</p></div>
<div class="m2"><p>که درد من دوا هرگز ندیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جفا را دیدم از اغیار و یارم</p></div>
<div class="m2"><p>بسویم از وفا هرگز ندیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگاهم می کند ز آن سان که گوئی</p></div>
<div class="m2"><p>مرا در هیچ جا هرگز ندیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفیق از خار خار دل چه گویم</p></div>
<div class="m2"><p>که او خاری به پا هرگز ندیده است</p></div></div>