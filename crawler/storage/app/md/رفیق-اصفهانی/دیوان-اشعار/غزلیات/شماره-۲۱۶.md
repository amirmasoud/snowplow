---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>زار از سر کوی یار رفتیم</p></div>
<div class="m2"><p>رفتیم و به حال زار رفتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن بلبل بی خودیم کز باغ</p></div>
<div class="m2"><p>ناآمده نوبهار رفتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با شوق پر آمدیم لیکن</p></div>
<div class="m2"><p>با حسرت بی شمار رفتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماندیم به کنج هجر چندان</p></div>
<div class="m2"><p>کز خاطر روزگار رفتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تاب تر آمدیم ز اول</p></div>
<div class="m2"><p>هر بار ز کوی یار رفتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا غیر رود رفیق از آن کو</p></div>
<div class="m2"><p>با هم روزی سه چار رفتیم</p></div></div>