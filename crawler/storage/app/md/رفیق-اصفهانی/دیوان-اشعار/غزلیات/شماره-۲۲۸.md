---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>من توبه ز صهبا چکنم گر نتوانم</p></div>
<div class="m2"><p>ترک می و مینا چکنم گر نتوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد کند از روز جزا بیم و من امروز</p></div>
<div class="m2"><p>اندیشه ی فردا چکنم گر نتوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد چه کنی منع من از عشق نکویان</p></div>
<div class="m2"><p>منع دل شیدا چکنم گر نتوانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چنگ تو چون مرغ اسیرم پی پرواز</p></div>
<div class="m2"><p>بال و پر خود واچکنم گر نتوانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنها به تماشا چه روم جانب گلشن</p></div>
<div class="m2"><p>گل بی تو تماشا چکنم گر نتوانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصل تو شهان راست تمنا من درویش</p></div>
<div class="m2"><p>وصل تو تمنا چکنم گر نتوانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرمنده ام از نسبت بالای تو با سرو</p></div>
<div class="m2"><p>سر پیش تو بالا چکنم گر نتوانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیباست رفیق از همه کس صبر ولی من</p></div>
<div class="m2"><p>صبر از رخ زیبا چکنم گر نتوانم</p></div></div>