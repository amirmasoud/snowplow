---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>زین پریشانان مکش دامن که حیران تواند</p></div>
<div class="m2"><p>کاین پریشان خاطران، خاطر پریشان تواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر طرف شاهی و هر سو عالمی حیران تو</p></div>
<div class="m2"><p>از نگاه فتنه جو وز چشم فتان تواند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو من در خاک و خون افتاده هر جانب بسی</p></div>
<div class="m2"><p>از خدنگ غمزه و از تیغ مژگان تواند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من نه تنها کشته ی ناز توام کز عاشقان</p></div>
<div class="m2"><p>عالمی چون من به تیغ غمزه قربان تواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دهی جلوه سمند ناز خلقی هر طرف</p></div>
<div class="m2"><p>کشته ی ناز تو و قربان جولان تواند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچه گویی آنچه فرمایی به جان و دل همه</p></div>
<div class="m2"><p>بنده ی حکم تو و محکوم فرمان تواند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکته سنجان گلستان صفائی چون رفیق</p></div>
<div class="m2"><p>بلبلان باغ و مرغان گلستان تواند</p></div></div>