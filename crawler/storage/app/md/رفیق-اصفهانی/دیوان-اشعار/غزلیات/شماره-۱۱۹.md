---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>گفتم آن لعل لب از شیره جان ساخته‌اند</p></div>
<div class="m2"><p>گفت نه جان تو شیرین‌تر از آن ساخته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست هر عضو تو از عضو دگر شیرین‌تر</p></div>
<div class="m2"><p>مگر اعضای تو از شیرهٔ جان ساخته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نالد از درد به تن هر رگ من پنداری</p></div>
<div class="m2"><p>بند بندم چو نی از بهر فغان ساخته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هوای گل رخسار تو چون ابر مراد</p></div>
<div class="m2"><p>از سر هر مژه خونابه روان ساخته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راز عشقی که نهان داشتم از خلق رفیق</p></div>
<div class="m2"><p>اشک گلگون و رخ زرد عیان ساخته‌اند</p></div></div>