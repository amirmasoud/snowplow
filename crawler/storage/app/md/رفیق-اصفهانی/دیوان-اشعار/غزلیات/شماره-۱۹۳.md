---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>عیانست از رخ کاهی، ز اشک ارغوانی هم</p></div>
<div class="m2"><p>که دارم درد پنهانی به دل داغ نهانی هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خونم کش که مرگ و شربت مرگ از تو عاشق را</p></div>
<div class="m2"><p>ز عمر جاودانی به ز آب زندگانی هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توانی کشت در یک لحظه چون من صد اگر خواهی</p></div>
<div class="m2"><p>و گر خواهی به یک دم زنده کردن می توانی هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود از ماهرویان مهربانی خوش، تو آن ماهی</p></div>
<div class="m2"><p>که باشد مهربانی از تو خوش نامهربانی هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه من در عهد پیری شهره ی عشق جوانانم</p></div>
<div class="m2"><p>که در عشق جوانان شهره بودم در جوانی هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنم پیوسته تا منع رقیبان از سر کویش</p></div>
<div class="m2"><p>بر آن در روز دربانی کنم شب پاسبانی هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفیق ار بی زبانم من چه غم چون هست یار من</p></div>
<div class="m2"><p>زباندانی که می داند زبان بی زبانی هم</p></div></div>