---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>ای خوی بد ای دلبر بدخو که تو داری</p></div>
<div class="m2"><p>نیکو نبود با رخ نیکو که تو داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه رنگ وفاداری و نه بوی مروت</p></div>
<div class="m2"><p>حیف از گل رو ای گل خودرو که تو داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبع تو بسی نازک و خوی تو بسی تند</p></div>
<div class="m2"><p>فریاد ازین طبع و از این خو که تو داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه روی تو دارد گل نه موی تو سنبل</p></div>
<div class="m2"><p>دارد که چنین روی و چنین مو که تو داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی که وفادار و کرم پیشه ام آخر</p></div>
<div class="m2"><p>آئین وفا رسم کرم کو که تو داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو یار دگر جوی دلا ز آن که جوی نیست</p></div>
<div class="m2"><p>جویای وفا یار جفاجو که تو داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم نیست رفیق از غم او گر نشدی شاد</p></div>
<div class="m2"><p>شادی تو کافیست غم او که تو داری</p></div></div>