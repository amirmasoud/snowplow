---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>با کوی یار روضه ی دارالقرار چیست</p></div>
<div class="m2"><p>دارالقرار یار به جز کوی یار چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد یار من سگت به از این یار کیست یار</p></div>
<div class="m2"><p>شد کوی تو دیارم از این به دیار چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی برای وعده خلافی نکرده شب</p></div>
<div class="m2"><p>اورا چه آگهی که شب انتظار چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای کرده با حبیب شبی روز بی رقیب</p></div>
<div class="m2"><p>رو شکر کن شکایتت از روزگار چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حیرتم که با همه پیمان شکست ما</p></div>
<div class="m2"><p>عهد تو با رقیب چنین استوار چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نیست خار خار دل بلبلش غرض</p></div>
<div class="m2"><p>گل را به هرزه این همه الفت به خار چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زیر خاک خون دلی نیست گر به جوش</p></div>
<div class="m2"><p>این لاله ها رفیق به خاک مزار چیست</p></div></div>