---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>شد سبزه ی خط از لب آن ماه لقاسبز</p></div>
<div class="m2"><p>چون سبزه که گردد ز لب آب بقا سبز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم ز خط سبز شود خوبیش افزون</p></div>
<div class="m2"><p>خط گرد لبش سر زد و شد گفته ی ما سبز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر تا قدم ای سرو بکام دل مایی</p></div>
<div class="m2"><p>بهر دل ما کرده همانات خداسبز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشک گل و سروی به رخ و قد چه خرامی</p></div>
<div class="m2"><p>چون گل بسر سرو کله سرخ و قبا سبز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک روز به من سایه نیفکند به عمری</p></div>
<div class="m2"><p>آن سرو که کردم همه عمرش به دعا سبز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش آب و هوا شهر و دیاریست چه بودی</p></div>
<div class="m2"><p>گر تخم وفا گشتی از این آب و هوا سبز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد مهر رفیق از خط او بیش که او را</p></div>
<div class="m2"><p>شد گرد گل از سبزه ی خط، مهر گیا سبز</p></div></div>