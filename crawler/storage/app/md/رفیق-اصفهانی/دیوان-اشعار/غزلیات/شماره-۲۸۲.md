---
title: >-
    شمارهٔ ۲۸۲
---
# شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>عادت به این مکن که کنی جور و کین همه</p></div>
<div class="m2"><p>نیکوست جور و کین ز نکویان نه این همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دلبران کدام بنالند عاشقان</p></div>
<div class="m2"><p>هستند این گروه جفاجو چنین همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقص تو نیست جور و جفا زانکه بوده اند</p></div>
<div class="m2"><p>خوبان همه چنان و نکویان چنین همه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد درد بر دلست مرا غیر درد عشق</p></div>
<div class="m2"><p>ای کاش بود درد دل من چنین همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با روی این چنین گذری گر به چین شوند</p></div>
<div class="m2"><p>حیران روی خوب تو خوبان چین همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان لعل لب که خاتم خوبیست باشدت</p></div>
<div class="m2"><p>هر ملک دل که هست به زیر نگین همه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماهی نه چون رخ تو بود بر همه فلک</p></div>
<div class="m2"><p>سروی نه چون قد تو بود بر زمین همه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاهی به من ز روی ترحم نگاه کن</p></div>
<div class="m2"><p>بر رغم من به سوی رقیبان مبین همه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر جا رفیق خواند به وصف تو شعر خویش</p></div>
<div class="m2"><p>کردند اهل طبع بر آن آفرین همه</p></div></div>