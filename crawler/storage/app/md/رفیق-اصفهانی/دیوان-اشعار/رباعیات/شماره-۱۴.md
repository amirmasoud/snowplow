---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>گیرم که ز شوق رخ نیکوی تو من</p></div>
<div class="m2"><p>آیم به نظاره بر سر کوی تو من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو طالع آنکه سوی من بینی تو</p></div>
<div class="m2"><p>کو زهره ی آنکه بنگرم روی تو من</p></div></div>