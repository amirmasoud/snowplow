---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای قافله سالار دلی را دریاب</p></div>
<div class="m2"><p>افتاده جدا ز منزلی را دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای پیشرو قافله از سیل سرشک</p></div>
<div class="m2"><p>پس مانده ی پای در گلی را دریاب</p></div></div>