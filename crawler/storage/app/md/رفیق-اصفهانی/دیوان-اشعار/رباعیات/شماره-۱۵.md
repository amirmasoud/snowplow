---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>دور از رخ تو بدیده و دل ای ماه</p></div>
<div class="m2"><p>پیوسته در آتشم به دل گاه به گاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پا تا سر در آبم از سیل سرشک</p></div>
<div class="m2"><p>از سر تا پا در آتشم از تف آه</p></div></div>