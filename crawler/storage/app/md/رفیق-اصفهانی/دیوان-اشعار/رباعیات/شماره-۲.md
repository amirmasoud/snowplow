---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>چون نیست مرا راه ز بیم خویت</p></div>
<div class="m2"><p>در خانه ات آیم همه شب در کویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا روز نشینم به امیدی که مگر</p></div>
<div class="m2"><p>از خانه برون آئی و بینم رویت</p></div></div>