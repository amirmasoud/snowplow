---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>رسید نامه ای از حضرت وفا و شکفتم</p></div>
<div class="m2"><p>چو بینوا که رسد ناگهش ز غیب نوائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمام زهر شکایت نهان به شکر شکری</p></div>
<div class="m2"><p>همه معانی نفرین عیان به لفظ دعائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بی وفائی من کرده شکوه و دل خود را</p></div>
<div class="m2"><p>ز راه و رسم وفا کرده خوش به اسم وفائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جا نه اهل وفا با رفیق این گله داری</p></div>
<div class="m2"><p>ولی بر این گله دارد زهی جواب بجائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که هست شرط وفا گر رفیق درگه و بیگه</p></div>
<div class="m2"><p>ز روی لطف بپرسی ز راه مهر برآئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگویمش همه باشد اگر به رسم تعارف</p></div>
<div class="m2"><p>چه می کنی به چه کاری چه می خوری بکجائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه آن که هر پس سالی چو بنگریش بگوئی</p></div>
<div class="m2"><p>چو منعمی به فقیری چو خسروی به گدائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخوان و دعوت ما وقت وقت از چه نپوئی</p></div>
<div class="m2"><p>به بزم صحبت آن گاه گاه از چه نپائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخوانده نیست سوی خانه ی خدا ره و بنگر</p></div>
<div class="m2"><p>چه حرفها به خدائیست تا به خانه خدائی</p></div></div>