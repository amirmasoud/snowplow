---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>گفتی چه حاجت است ترا تا روا شود</p></div>
<div class="m2"><p>گفتن چه حاجت است بر اما چه حاجت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشعار فقر بر در حاتم چه لازمست</p></div>
<div class="m2"><p>اظهار درد پیش مسیحا چه حاجت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تدبیر کار هیچ ندانی چو من زمن</p></div>
<div class="m2"><p>کردن سئوال از چو تو دانا چه حاجت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیمار اگر فضول نباشد طبیب را</p></div>
<div class="m2"><p>آموختن طریق مداوا چه حاجت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کافیست بهر حاجت ما از تو کم سخن</p></div>
<div class="m2"><p>اینست اگر سخن سخن ما چه حاجت است</p></div></div>