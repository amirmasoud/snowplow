---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>جور کن کز بازوی پرزور و طبع پرغرور</p></div>
<div class="m2"><p>ایزدت بیهوده اسباب جهان کاری نکرد</p></div></div>