---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>داوود شها گرت به فرمان باد است</p></div>
<div class="m2"><p>مغرور مشو که حاصل آن باد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو ملک ابد طلب که نزدیک خرد</p></div>
<div class="m2"><p>تا ملک ابد ملک سلیمان باد است</p></div></div>