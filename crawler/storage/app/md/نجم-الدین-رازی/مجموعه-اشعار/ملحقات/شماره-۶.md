---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>تیر غمزه چو کند داد نشست</p></div>
<div class="m2"><p>تا پر اندر سخاخ سینهٔ من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر کوه قاف سایهٔ سیمرغ کامیاب</p></div>
<div class="m2"><p>کز دامنش عقاب بپرد به صد عتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخم پرش چنانکه سحر گه به جنگ شب</p></div>
<div class="m2"><p>دست سپیده دم بکشد تیغ آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جهان طرفه اتفاق افتاد</p></div>
<div class="m2"><p>بارگیر مرا که طاق افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از همه مرکبان برق صفت</p></div>
<div class="m2"><p>باد پیمای من براق افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آرایش رضوان ملک حور و قصور</p></div>
<div class="m2"><p>زایوان بهشت خوش بود پردهٔ نور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و آنگه خوشتر که نیک نزدیک، نه دور</p></div>
<div class="m2"><p>از پردهٔ نور، روی بنماید حور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خداوندگار و خداوند امیر</p></div>
<div class="m2"><p>که شاهان ندارند چون او وزیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو سیمرغ و آب حیات آمده است</p></div>
<div class="m2"><p>عزیز الوجود و عدیم النظیر</p></div></div>