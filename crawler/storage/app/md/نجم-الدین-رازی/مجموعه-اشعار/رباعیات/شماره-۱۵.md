---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>گفتا: هر دل به عشق ما بینا نیست</p></div>
<div class="m2"><p>هر جان صدف گوهر عشق ما نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودای وصال ما ترا تنها نیست</p></div>
<div class="m2"><p>لیکن قد این قبا به هر بالا نیست</p></div></div>