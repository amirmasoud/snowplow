---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>یا رب تو مرین سایهٔ یزدانی را</p></div>
<div class="m2"><p>بگذار بدین جهان جهانبانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر کنف عاطفت خویشش دار</p></div>
<div class="m2"><p>این حامی بیضهٔ مسلمانی را</p></div></div>