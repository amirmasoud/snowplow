---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>در عشق تو شادی و غمم هیچ نماند</p></div>
<div class="m2"><p>با وصل تو سور و ماتمم هیچ نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک نور تجلی توام کرد چنان</p></div>
<div class="m2"><p>کز نیک و بد و بیش و کمم هیچ نماند</p></div></div>