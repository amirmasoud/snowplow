---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>در بحر عمیق غوطه خواهم خوردن</p></div>
<div class="m2"><p>یا غرقه شدن یا گهری آوردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار تو مخاطره است خواهم کردن</p></div>
<div class="m2"><p>یا سرخ کنم روی ز تو یا گردن.</p></div></div>