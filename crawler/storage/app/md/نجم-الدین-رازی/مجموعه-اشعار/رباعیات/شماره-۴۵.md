---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>بازی که همی دست ملک را شاید</p></div>
<div class="m2"><p>منقار به مردار کجا آلاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دست ملک نشیند آزاد ز خویش</p></div>
<div class="m2"><p>در بند اشاراتی که او فرماید</p></div></div>