---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>شمع ار چه من داغ جدایی دارد</p></div>
<div class="m2"><p>با گریه و سوز آشنایی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر رشتهٔ شمع به که سر رشتهٔ من</p></div>
<div class="m2"><p>کان رشته سری به روشنایی دارد</p></div></div>