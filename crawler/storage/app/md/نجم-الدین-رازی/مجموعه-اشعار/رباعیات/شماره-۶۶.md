---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>افراز ملوک را نشیبی است مکن</p></div>
<div class="m2"><p>در هر دلکی از تو نهیبی است مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خلق ستم اگر به سیبی است مکن</p></div>
<div class="m2"><p>کز هر سیبی با تو حسیبی است مکن.</p></div></div>