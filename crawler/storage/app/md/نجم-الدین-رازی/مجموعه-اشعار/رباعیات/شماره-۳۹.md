---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>آن روز که دوختی مرا دلق وجود</p></div>
<div class="m2"><p>گفتند به طعنه مر ترا خلق وجود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خونریزی را چه می کنی راست بدان</p></div>
<div class="m2"><p>من خونریزم ولیکن از حلق وجود</p></div></div>