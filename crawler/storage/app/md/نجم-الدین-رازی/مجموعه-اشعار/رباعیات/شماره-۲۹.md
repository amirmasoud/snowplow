---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>صحرا به گل و لاله بیاراسته‌اند</p></div>
<div class="m2"><p>در عیش فزوده و ز غم کاسته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خاک عروسان چمن خفته بدند</p></div>
<div class="m2"><p>امروز قیامت است برخاسته‌اند</p></div></div>