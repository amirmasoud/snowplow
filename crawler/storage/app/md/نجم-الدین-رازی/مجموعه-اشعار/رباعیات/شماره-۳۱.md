---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>عشاق که آفتاب عالم‌تابند</p></div>
<div class="m2"><p>در دیده کشند خاک من گر یابند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد دشمن من ز جهل آن مشتی دون</p></div>
<div class="m2"><p>چون شب‌پرگان که دشمن آفتابند</p></div></div>