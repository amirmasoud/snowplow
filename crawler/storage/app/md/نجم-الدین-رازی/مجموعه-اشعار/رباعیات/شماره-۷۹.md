---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>ای دل تو اگر مست نه ای هشیاری</p></div>
<div class="m2"><p>ز آن پیش که بگذرد جهان بگذاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کم خسب به وقت صبح کاندر پی تست</p></div>
<div class="m2"><p>خوابی که قیامتش بود بیداری.</p></div></div>