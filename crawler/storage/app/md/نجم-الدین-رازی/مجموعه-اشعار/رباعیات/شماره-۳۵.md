---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>مردان رهش زنده به جانی دگرند</p></div>
<div class="m2"><p>مرغان هواش ز آشیانی دگرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منگر تو بدین دیده بدیشان کایشان</p></div>
<div class="m2"><p>بیرون ز دو کون در جهانی دگرند</p></div></div>