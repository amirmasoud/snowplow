---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>عشاق تو از الست مست آمده اند</p></div>
<div class="m2"><p>سر مست ز بادهٔ الست آمده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می می نوشند و پند می ننیوشند</p></div>
<div class="m2"><p>کایشان ز الست می پرست آمده اند</p></div></div>