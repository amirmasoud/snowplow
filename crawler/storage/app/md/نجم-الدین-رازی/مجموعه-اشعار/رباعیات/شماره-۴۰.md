---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>خوی سبعی ز نفست ار باز شود</p></div>
<div class="m2"><p>مرغ روحت به آشیان باز شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس کرکس روح روسوی علو نهد</p></div>
<div class="m2"><p>بر دست ملک نشیند و باز شود</p></div></div>