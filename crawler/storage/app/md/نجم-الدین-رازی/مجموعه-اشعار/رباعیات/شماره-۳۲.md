---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>ز آن پیش که نور بر ثریا بستند</p></div>
<div class="m2"><p>وین منطقه بر میان جوزا بستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عهد ازل بسان آتش بر شمع</p></div>
<div class="m2"><p>عشقت به هزار رشته بر ما بستند</p></div></div>