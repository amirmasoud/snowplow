---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>گفتم: که زد این چنین دم سرد که من؟</p></div>
<div class="m2"><p>بلبل ز درخت سر فرو کرد که: من!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم: به شب این غصه کسی خورد که من</p></div>
<div class="m2"><p>نیلوفر از آب سر برآورد که: من!</p></div></div>