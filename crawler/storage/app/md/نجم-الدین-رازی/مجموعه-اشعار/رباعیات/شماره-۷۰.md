---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>ای دل بیدل به نزد آن دلبر رو</p></div>
<div class="m2"><p>در بارگه وصال او بی سر رو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنهان ز همه خلق چو رفتی به درش</p></div>
<div class="m2"><p>خود را به درش بمان و آنگه در رو.</p></div></div>