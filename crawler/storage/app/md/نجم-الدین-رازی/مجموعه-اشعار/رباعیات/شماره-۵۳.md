---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>تا باغم عشق تو هم آواز شدم</p></div>
<div class="m2"><p>صد باره زیادت به عدم باز شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آن سوی عدم نیز بسی پیمودم</p></div>
<div class="m2"><p>«رازی» بودم کنون همه راز شدم</p></div></div>