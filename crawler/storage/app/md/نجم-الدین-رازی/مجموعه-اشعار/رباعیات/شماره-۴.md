---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>عشقت که دوای جان این دلریش است</p></div>
<div class="m2"><p>ز اندازهٔ هر هوس پرستی بیش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیزی است که از ازل مرا در سر بود</p></div>
<div class="m2"><p>کاری است که تا ابد مرا در پیش است</p></div></div>