---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>آن روز که کار وصل را ساز آید</p></div>
<div class="m2"><p>وین مرغ ازین قفص به پرواز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شه چو صفیر «ارجعی» روح شنید</p></div>
<div class="m2"><p>پرواز کنان به دست شه باز آید</p></div></div>