---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>ای ساقی خوش بادهٔ ناب اندر ده</p></div>
<div class="m2"><p>مستان شده ایم هین شراب اندر ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس نیست ز ما که نه خراب است و یباب</p></div>
<div class="m2"><p>آواز بدین ده خراب اندر ده.</p></div></div>