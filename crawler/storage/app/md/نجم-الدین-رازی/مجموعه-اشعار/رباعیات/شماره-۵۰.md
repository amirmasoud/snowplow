---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>ای آنکه نشسته اید پیرامن شمع</p></div>
<div class="m2"><p>قانع گشته به خوشه از خرمن شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانه صفت نهید جان بر کف دست</p></div>
<div class="m2"><p>تابوک کنید دست در گردن شمع</p></div></div>