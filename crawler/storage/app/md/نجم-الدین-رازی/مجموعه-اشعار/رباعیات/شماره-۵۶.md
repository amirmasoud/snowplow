---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>از عشق مهی چو برلب آمد جانم</p></div>
<div class="m2"><p>گفتم بکنی به وصل خود درمانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا اگرت وصال ما می باید</p></div>
<div class="m2"><p>رو هیچ ممان تو تا همه من مانم</p></div></div>