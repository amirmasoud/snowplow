---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>ای پیر مغان می مغانی درده</p></div>
<div class="m2"><p>و آن جام گران خسروانی درده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیف است که باده و میش می خوانند</p></div>
<div class="m2"><p>آن مایهٔ آب زندگانی درده.</p></div></div>