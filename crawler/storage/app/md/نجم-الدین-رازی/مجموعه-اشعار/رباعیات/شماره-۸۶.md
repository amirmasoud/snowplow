---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>ای نسخهٔ نامهٔ الهی که تویی</p></div>
<div class="m2"><p>وی آینهٔ جمال شاهی که تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون ز تو نیست هر چه در عالم هست</p></div>
<div class="m2"><p>در خود بطلب هر آنچه خواهی که تویی.</p></div></div>