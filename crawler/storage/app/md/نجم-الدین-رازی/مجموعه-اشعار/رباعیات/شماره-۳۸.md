---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>ای دل این ره به قیل و قالت ندهند</p></div>
<div class="m2"><p>جز بر در نیستی وصالت ندهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آنگاه در آن هوا که مرغان ویند</p></div>
<div class="m2"><p>تا با پر و بالی پر و بالت ندهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک طلبش به هر سلیمان ندهند</p></div>
<div class="m2"><p>منشور غمش به هر دل و جان ندهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درمان طلبان ز درد او محرومند</p></div>
<div class="m2"><p>کین درد به طالبان درمان ندهند</p></div></div>