---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>هر که را این عشقبازی در ازل آموختند</p></div>
<div class="m2"><p>تا ابد در جان او شمعی ز عشق افروختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آن دلی را کز برای وصل او پرداختند</p></div>
<div class="m2"><p>همچو بازش از دو عالم دیده ها بردوختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس درین منزل چگونه تاب هجر آرند باز</p></div>
<div class="m2"><p>بیدلانی کاندر آن منزل به وصل آموختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاجرم چون شمع گاه از هجر او بگداختند</p></div>
<div class="m2"><p>گاه چون پروانه بر شمع وصالش سوختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خرابات فنا ساقی چو جام اندر فکند</p></div>
<div class="m2"><p>هر چه بود اندر دو عالمشان به می بفروختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>«نجم رازی» را مگر رازی ازین معلوم شد</p></div>
<div class="m2"><p>هر چه غم بد در دو عالم بهر او اندوختند</p></div></div>