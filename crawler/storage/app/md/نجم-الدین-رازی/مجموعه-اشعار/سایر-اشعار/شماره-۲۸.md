---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>کرده‌ای تکیه بر جهان و هنوز</p></div>
<div class="m2"><p>غدر و مکر جهان نمی‌دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز خواهد هر آنچه داد به تو</p></div>
<div class="m2"><p>عاریت، بی‌گمان نمی‌دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بازمانی ز دولت جاوید</p></div>
<div class="m2"><p>تا غم جاودان نمی‌دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا توانی دلت بر آر ز جهل</p></div>
<div class="m2"><p>خویشتن ناتوان نمی‌دانی</p></div></div>