---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>گر زعشقت خبر نداشتمی</p></div>
<div class="m2"><p>داغ آن برجگر نداشتمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق تو از کجا و من زکجا</p></div>
<div class="m2"><p>گر زحسنت خبر نداشتمی</p></div></div>