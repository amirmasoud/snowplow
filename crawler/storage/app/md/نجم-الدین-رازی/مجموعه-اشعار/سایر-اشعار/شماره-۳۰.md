---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>دوشم سحرگهی ندای حق به جان رسید</p></div>
<div class="m2"><p>کای روح پاک مرتع حیوان چه می‌کنی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو نازنین عالم عصمت بدی کنون</p></div>
<div class="m2"><p>با خواری و مذلت عصیان چه می‌کنی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پروردهٔ حظائر قدسی به ناز وصل</p></div>
<div class="m2"><p>اینجا اسیر محنت هجران چه می‌کنی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خو کرده‌ای به رقهٔ الطاف حضرتی</p></div>
<div class="m2"><p>سرگشته در تصرف دوران چه می‌کنی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو صافی «الست بِرَبّک» چشیده‌ای</p></div>
<div class="m2"><p>با دردی و ساوس شیطان چه می‌کنی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زندان روح تن بود ار هیچ عاقلی</p></div>
<div class="m2"><p>غافل چنین نشسته به زندان چه می‌کنی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو انس با جمال و جلالم گرفته‌ای</p></div>
<div class="m2"><p>وحشت‌سرای عالم انسان چه می‌کنی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در وسعت هوای هویت پریده‌ای</p></div>
<div class="m2"><p>در تنگنای عرصهٔ دو جهان چه می‌کنی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر پر سوی نشیمن اول چو باز شاه</p></div>
<div class="m2"><p>چون بوم خس نه‌ای تو به ویران چه می‌کنی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گیرم که مال و ملک سلیمان به تو رسید</p></div>
<div class="m2"><p>باقی چو نیست ملک سلیمان چه می‌کنی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون چار گز نشیب زمین است مسکنت</p></div>
<div class="m2"><p>چندین بلند درگه و ایوان چه می‌کنی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرگ از پِیَت دو اسبه شب و روز می‌دود</p></div>
<div class="m2"><p>تو خواب خوش چو مردم نادان چه می‌کنی؟</p></div></div>