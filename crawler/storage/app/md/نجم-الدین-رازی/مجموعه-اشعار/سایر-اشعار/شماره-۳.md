---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>عشق آمد و کرد عقل غارت</p></div>
<div class="m2"><p>ای دل تو به جان بر این بشارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترک عجبی است عشق دانی</p></div>
<div class="m2"><p>کز ترک عجیب نیست غارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد عقل که در عبارت آرد</p></div>
<div class="m2"><p>وصف رخ او به استعارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع رخ او زبانه ای زد</p></div>
<div class="m2"><p>هم عقل بسوخت هم عبارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر بیع و شرای عقل می خند</p></div>
<div class="m2"><p>سودش بنگر ازین تجارت.</p></div></div>