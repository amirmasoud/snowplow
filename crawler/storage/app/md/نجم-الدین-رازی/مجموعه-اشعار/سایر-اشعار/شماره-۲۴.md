---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>در عشق یار بین که چه عیار می‌رویم</p></div>
<div class="m2"><p>سر زیر پا نهاده چه شطار می‌رویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نقطهٔ مراد بدین دور ما رسیم</p></div>
<div class="m2"><p>زیرا به سر همیشه چو پرگار می‌رویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانی که هست مان فدی یار کرده‌ایم</p></div>
<div class="m2"><p>ور حکم می‌کند به سردار می‌رویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرگ ار کسی به جان بفروشد همی‌خریم</p></div>
<div class="m2"><p>عیاروار ز آنکه بر یار می‌رویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را چه غم ز دوزخ و با خُلدمان چه کار؟</p></div>
<div class="m2"><p>دلداده‌ایم ما بر دلدار می‌رویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بار امانتش به دل و جان کشیده، پس</p></div>
<div class="m2"><p>در بارگاه عزت بی‌بار می‌رویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با ظلمت نفوس و طبایع درآمدیم</p></div>
<div class="m2"><p>در جان هزارگونه ز انوار می‌رویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آن پس که بوده‌ایم بسی در حریم جهل</p></div>
<div class="m2"><p>این فضل بین که محرم اسرار می‌رویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عمری اگرچه در ظلمات هوا بُدیم</p></div>
<div class="m2"><p>آب حیات خورده خضروار می‌رویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه چو چرخ کور و کبود آمدیم لیک</p></div>
<div class="m2"><p>با صد هزار دیده فلک سار می‌رویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در نقطهٔ مراد بدین دور ما رسیم</p></div>
<div class="m2"><p>زیرا به سر همیشه چو پرگار می‌رویم</p></div></div>