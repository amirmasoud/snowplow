---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>دولت این جهان اگر چه خوش است</p></div>
<div class="m2"><p>دل مبند اندرو که دوست کش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که را همچو شاه بنوازد</p></div>
<div class="m2"><p>چون پیاده به طرح بندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست دنیا و دولتش چو سراب</p></div>
<div class="m2"><p>در فریبد ولیک ندهد آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس که آورد چرخ شاه و وزیر</p></div>
<div class="m2"><p>ملکشان داد و گنج و تاج و سریر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کارها را به کام ایشان کرد</p></div>
<div class="m2"><p>خلق را جمله رام ایشان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چو نمرود مایه دار شدند</p></div>
<div class="m2"><p>همه فرعون روزگار شدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون درویشکان مکیدندی</p></div>
<div class="m2"><p>مغز بیچارگان کشیدندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه مشغول ماه وسال شده</p></div>
<div class="m2"><p>همه مغرور جاه و مال شده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناگهان تند باد قهر وزید</p></div>
<div class="m2"><p>وز سر تخت شان به تخته کشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تنشان را به خاک ریمن داد</p></div>
<div class="m2"><p>ملکشان را به دست دشمن داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وزر اینها بدان جهان بردند</p></div>
<div class="m2"><p>مالشان دیگران همی خوردند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و آنکه حقش به لطف خود بنواخت</p></div>
<div class="m2"><p>نیک و بد را به نور حق بشناخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باز دانست نار را از نور</p></div>
<div class="m2"><p>دل نبست اندرین سرای غرور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باقی عمر خویشتن دریافت</p></div>
<div class="m2"><p>به صلاح معاد خویش شتافت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غم آن خورد کو ازین منزل</p></div>
<div class="m2"><p>چون کند کوچ، شادمان، خوشدل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر چه از ملک و گنج و شاهی داشت</p></div>
<div class="m2"><p>برد با خویشتن جوی نگذاشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لاجرم چون رسید کار به کار</p></div>
<div class="m2"><p>رفت با صد هزار استظهار</p></div></div>