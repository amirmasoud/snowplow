---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>دستی چو نیست جیفهٔ مردار بس خسند</p></div>
<div class="m2"><p>آن‌ها که دل به جیفهٔ مردار می‌دهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست از جهان بدار و ازو پای بازکش</p></div>
<div class="m2"><p>کان رایگان به کافر تاتار می‌دهند</p></div></div>