---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>دشمن ما را سعادت یار باد</p></div>
<div class="m2"><p>از جهان در عمر برخوردار باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که خاری می نهد در راه ما</p></div>
<div class="m2"><p>خار ما در راه او گلزار باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که چاهی می کند در راه ما</p></div>
<div class="m2"><p>چاه ما در راه او هموار باد.</p></div></div>