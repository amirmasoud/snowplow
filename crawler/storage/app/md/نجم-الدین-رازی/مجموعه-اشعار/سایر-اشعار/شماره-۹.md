---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>دل بی تو ز تو خبر ندارد</p></div>
<div class="m2"><p>در عشق تو خواب و خور ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با یار بگوی عاشقان را</p></div>
<div class="m2"><p>زین بیش بلند بر ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از رحمت عاشقان به رویت</p></div>
<div class="m2"><p>باد سحری گذر ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از غیرت عاشقان به کویت</p></div>
<div class="m2"><p>جبریل امین گذر ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار غمت ارچه بس گران است</p></div>
<div class="m2"><p>عاشق چه کند که بر ندارد</p></div></div>