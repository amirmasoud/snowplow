---
title: >-
    شمارهٔ  ۱۰
---
# شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>گشت مسلم ز عشق ملک معانی مرا</p></div>
<div class="m2"><p>شهره آفاق کرد عشق نهانی مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مدد شاه عشق ملک بقا یافتم</p></div>
<div class="m2"><p>کی بفریبد کنون ملکت خانی مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غرقه دریا شدم لاجرم از بهر آب</p></div>
<div class="m2"><p>هیچ نباید کشید رنج اوانی مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد و جراحات عشق کم مکن از جان من</p></div>
<div class="m2"><p>ای که بهنگام درد راحت جانی مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از که بود عزتم گر تو ذلیلم کنی</p></div>
<div class="m2"><p>کیست که خواند بخویش گر تو برانی مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کرم دیگران رنج روانم رسید</p></div>
<div class="m2"><p>با همه فقر و الم گنج روانی مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلوت خاص حقی قصر شه مطلقی</p></div>
<div class="m2"><p>ای دل اهل صفا قبله از آنی مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست مرا حاصلی بی تو ز جان و جهان</p></div>
<div class="m2"><p>ای که بلطف و کرم جان و جهانی مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنچه بدانم توئی قبله جانم توئی</p></div>
<div class="m2"><p>نیست بجز سوی تو دل نگرانی مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از غمت ایماه من پیر شدم چون حسین</p></div>
<div class="m2"><p>آه که آمد بشب روز جوانی مرا</p></div></div>