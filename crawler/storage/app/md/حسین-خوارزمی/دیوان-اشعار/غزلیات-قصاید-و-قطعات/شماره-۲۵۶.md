---
title: >-
    شمارهٔ  ۲۵۶
---
# شمارهٔ  ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>گفتم دلا ببین که جفای که میکشی</p></div>
<div class="m2"><p>وین درد دل ز بهر رضای که میکشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دشمنان کشند جفا بهر دوستان</p></div>
<div class="m2"><p>چون دوست دشمن است برای که میکشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کس که بر وفای حبیبی جفا کشد</p></div>
<div class="m2"><p>باری تو بر امید وفای که میکشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون عیسی شکسته دلان از تو فارغ است</p></div>
<div class="m2"><p>این درد دل ز بهر دوای که میکشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او را سر هوای تو چون نیست بیش از این</p></div>
<div class="m2"><p>بیهوده درد سر بهوای که میکشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گیرم که از بلای بتانت گزیر نیست</p></div>
<div class="m2"><p>باری نگر که بار بلای که میکشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل گفت شرم دار از این گفتگو حسین</p></div>
<div class="m2"><p>بگشای چشم و بین که جفای که میکشی</p></div></div>