---
title: >-
    شمارهٔ  ۹۵
---
# شمارهٔ  ۹۵

<div class="b" id="bn1"><div class="m1"><p>خرم دل آن کس که تمنای تو دارد</p></div>
<div class="m2"><p>شادی کسی کو غم سودای تو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا حشر بود سجده گه اهل محبت</p></div>
<div class="m2"><p>جائی که نشانی ز کف پای تو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاید که ز خورشید فلک دیده بدوزد</p></div>
<div class="m2"><p>هر کس که نظر در رخ زیبای تو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز بسوی طوبی و جنت نکند میل</p></div>
<div class="m2"><p>آن دل که هوای قد رعنای تو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اصلا نکند جانب فردوس نگاهی</p></div>
<div class="m2"><p>هر دیده که امکان تماشای تو دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پروانه صفت گر تو بسوزی ز غم دل</p></div>
<div class="m2"><p>آن شمع شب افروز چه پروای تو دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دوست حسین این همه سرمایه سودا</p></div>
<div class="m2"><p>از سلسله زلف سمن سای تو دارد</p></div></div>