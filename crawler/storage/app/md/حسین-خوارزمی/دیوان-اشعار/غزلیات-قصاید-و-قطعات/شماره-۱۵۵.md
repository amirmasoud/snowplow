---
title: >-
    شمارهٔ  ۱۵۵
---
# شمارهٔ  ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>تو پادشاه و من از بندگان درگاهم</p></div>
<div class="m2"><p>بغیر تو ز تو چیز دگر نمی خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سزد که بر سر عالم علم برافرازم</p></div>
<div class="m2"><p>کز آن زمان که غلام توام شهنشاهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسوز آتش سودای تو همی سازم</p></div>
<div class="m2"><p>سمندرم من و این آتش است دلخواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه بیخود و مجنون شدم هزاران شکر</p></div>
<div class="m2"><p>که از لطافت لیلی خویش آگاهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آستان تو چون راستان مقیم شوم</p></div>
<div class="m2"><p>اگر بصدر جلالت نمیدهی راهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خدمتت نروم زانکه از غلامی تست</p></div>
<div class="m2"><p>همه سعادت و اقبال و منصب و جاهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پیش خویش بخوانی شبی حسینی را</p></div>
<div class="m2"><p>به گوش تو چو رسد ناله سحرگاهم</p></div></div>