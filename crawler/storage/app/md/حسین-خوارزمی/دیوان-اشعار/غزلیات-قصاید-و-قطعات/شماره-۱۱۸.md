---
title: >-
    شمارهٔ  ۱۱۸
---
# شمارهٔ  ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>به چشم او نظر می‌کن دلا در ماه رخسارش</p></div>
<div class="m2"><p>تو هم با دیده جان می‌توانی دید دیدارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظلال عالم صورت سبل شد دیده دل را</p></div>
<div class="m2"><p>بهل صورت که تا بینی جهانی پر ز انوارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلستان حقایق را چه ریحان‌هاست روح‌افزا</p></div>
<div class="m2"><p>مشام جان چو بگشایی رسد بویی ز گلزارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند شادی بود خرم دلی کز عشق دارد غم</p></div>
<div class="m2"><p>شود آزاد در عالم هر آنکو شد گرفتارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شه کنعانیم چون مه ببازار آمده ناگه</p></div>
<div class="m2"><p>عزیزان وفاپیشه بجان گشته خریدارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه راحتها که می بینم جراحتهای جانانرا</p></div>
<div class="m2"><p>چه مستیها که من دارم ز چشم شوخ خمارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هدف گشته مرا سینه ز تیغ غمزه مستش</p></div>
<div class="m2"><p>صدف گشته مرا دیده ز یاقوت گهر بارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا چون تو گواهی را پسندد یار خود هرگز</p></div>
<div class="m2"><p>شد این کنج دل ویران محل گنج اسرارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو گنج خاص سلطانی نباشد جز بویرانی</p></div>
<div class="m2"><p>شهی کاندر همه عالم به خوبی نیست کس یارش</p></div></div>