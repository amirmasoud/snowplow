---
title: >-
    شمارهٔ  ۲۳
---
# شمارهٔ  ۲۳

<div class="b" id="bn1"><div class="m1"><p>خلق عالم بجمالت نگرانند ای دوست</p></div>
<div class="m2"><p>وز غمت نعره زنان جامه درانند ای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما بر آنیم که مانند تو منصوری نیست</p></div>
<div class="m2"><p>همه ارباب نظر نیز بر آنند ای دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاقلانی که ملامت ز غم عشق کنند</p></div>
<div class="m2"><p>مگر از حسن رخت بیخبرانند ای دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهدان سرمه ز خاک قدمت گر نزنند</p></div>
<div class="m2"><p>ظاهر آنست که بس بی بصرانند ای دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مخلصانی که نظر بر چو تو منصور کنند</p></div>
<div class="m2"><p>نی چو اصحاب هوا کج نظرانند ای دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک پائی که بجان زینت افسر سازند</p></div>
<div class="m2"><p>سرورانی که همه تاج ورانند ای دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون حسین از همه مخلص تر و بیچاره تر است</p></div>
<div class="m2"><p>از چه مخصوص عنایت دگرانند ای دوست</p></div></div>