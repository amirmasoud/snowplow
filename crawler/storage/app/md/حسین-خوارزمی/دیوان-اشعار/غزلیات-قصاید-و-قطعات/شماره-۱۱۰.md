---
title: >-
    شمارهٔ  ۱۱۰
---
# شمارهٔ  ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>زهی بوعده وصل تو جان ما مسرور</p></div>
<div class="m2"><p>بیا که چشم بد از تو همیشه بادا دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چگونه دیده بدوزم ز منظرت که ندید</p></div>
<div class="m2"><p>نظر نظیر تو در کائنات یک منظور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که طلعت حسن عذار عذرا دید</p></div>
<div class="m2"><p>بود هر آینه وامق به پیش او معذور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدور باده چشانی چشم مخمورت</p></div>
<div class="m2"><p>چگونه مستی ارباب دل بود مستور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن خم آر شرابی برای دفع خمار</p></div>
<div class="m2"><p>روا بود چو تو ساقی و ما چنین مخمور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مثال کعبه و مانند بیت معمور است</p></div>
<div class="m2"><p>خرابه دل ما چون بعشق شد معمور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چگونه کنه جمال ترا کند ادراک</p></div>
<div class="m2"><p>اگر دو دیده ز دیدار تو نیابد نور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مردن از پی تو بخت پایدار آمد</p></div>
<div class="m2"><p>ز پای دار نترسد حسینی منصور</p></div></div>