---
title: >-
    شمارهٔ  ۲۲۴
---
# شمارهٔ  ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>ای ز دردت عاشقان خسته درمان یافته</p></div>
<div class="m2"><p>وز جراحتهای تو دل راحت جان یافته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خازن حسن از سویدای دل سودائیان</p></div>
<div class="m2"><p>از برای گنج عشقت کنج ویران یافته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرزومندان دیدار تو از سیلاب اشگ</p></div>
<div class="m2"><p>کشتی هستی خود در موج طوفان یافته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت دیدارت که آن میقات عید اکبر است</p></div>
<div class="m2"><p>تیغ عشق تو ز جان خسته قربان یافته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خضر در ظلمات عمری جست آب زندگی</p></div>
<div class="m2"><p>عاشقان از خاک کویت آب حیوان یافته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قاصدان کعبه کوی تو در وادی شوق</p></div>
<div class="m2"><p>سندس و استبرق از خار مغیلان یافته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشته سلطان اقالیم محبت چون حسین</p></div>
<div class="m2"><p>هر که از دیوان عشق دوست فرمان یافته</p></div></div>