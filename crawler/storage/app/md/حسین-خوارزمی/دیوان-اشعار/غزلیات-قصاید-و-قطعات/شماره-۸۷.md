---
title: >-
    شمارهٔ  ۸۷
---
# شمارهٔ  ۸۷

<div class="b" id="bn1"><div class="m1"><p>کسی که شیفته روی آن صنم باشد</p></div>
<div class="m2"><p>ز طعن و سرزنش دشمنش چه غم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برای دیدن دیدار دوست از دشمن</p></div>
<div class="m2"><p>توان کشیدن اگر صد هزار الم باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهیچ رو ز در او نمیروم باری</p></div>
<div class="m2"><p>گدا ملازم درگاه محتشم باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقیبم از سر کویش بجور میراند</p></div>
<div class="m2"><p>گدای شهر مبادا که محترم باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که قدر شب وصل دوست نشناسد</p></div>
<div class="m2"><p>اگر ز هجر بمیرد هنوز کم باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر آنکه سر غم عشق بر زبان راند</p></div>
<div class="m2"><p>زبان بریده سیه روی چون قلم باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفت با تو دمی همنفس شوم روزی</p></div>
<div class="m2"><p>ندانم آن دم خرم کدام دم باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر بحال من خسته دل کند نظری</p></div>
<div class="m2"><p>ز عین مردمی و غایت کرم باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسین خسته جگر را سزد که بنوازد</p></div>
<div class="m2"><p>به گوشه نظری گر چه صبحدم باشد</p></div></div>