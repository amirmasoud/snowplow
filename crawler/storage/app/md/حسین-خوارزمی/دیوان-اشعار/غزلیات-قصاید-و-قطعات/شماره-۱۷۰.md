---
title: >-
    شمارهٔ  ۱۷۰
---
# شمارهٔ  ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>ما بار تن ز کوی وصال تو می بریم</p></div>
<div class="m2"><p>وز بهر توشه عشق جمال تو میبریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دوست را ز دوست بود یادگارئی</p></div>
<div class="m2"><p>دل با تو میدهیم و خیال تو میبریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلهای ما بدام بلا میشود اسیر</p></div>
<div class="m2"><p>هردم که نام دانه خال تو میبریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مصریان بضاعت ما تنگ شکر است</p></div>
<div class="m2"><p>زیرا که نکته ای ز مقال تو میبریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مانند خضر چاشنی چشمه حیات</p></div>
<div class="m2"><p>از لفظ همچو آب زلال تو میبریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ننگ وجود خویشتن از روی مسکنت</p></div>
<div class="m2"><p>از خاک آستان جلال تو میبریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانا حسین هست مقیم درت ولیک</p></div>
<div class="m2"><p>بار بدن ز بیم ملال تو می بریم</p></div></div>