---
title: >-
    شمارهٔ  ۲۸
---
# شمارهٔ  ۲۸

<div class="b" id="bn1"><div class="m1"><p>وای که از حال من دلبرم آگاه نیست</p></div>
<div class="m2"><p>آه که از دست او زهره یک آه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر در او میکشم جان ز پی تحفه لیک</p></div>
<div class="m2"><p>هدیه این بینوا لایق درگاه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طالب هر دو جهان ره نبرد سوی او</p></div>
<div class="m2"><p>راه گداپیشگان در حرم شاه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند بود خاک پاک بسته این تیره خاک</p></div>
<div class="m2"><p>یوسف مصری ما در خور این چاه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع شبستان ما روی دلارای او است</p></div>
<div class="m2"><p>مجلس عشاق را روشنی از ماه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه مرا بندگان هست به از من بسی</p></div>
<div class="m2"><p>لیک مرا غیر آن هیچ شهنشاه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حلقه زدم بر درش گفت برو ای حسین</p></div>
<div class="m2"><p>تا تو بخود بسته ای پیش منت راه نیست</p></div></div>