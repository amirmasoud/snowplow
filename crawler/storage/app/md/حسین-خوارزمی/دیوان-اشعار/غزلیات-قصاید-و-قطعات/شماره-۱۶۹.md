---
title: >-
    شمارهٔ  ۱۶۹
---
# شمارهٔ  ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>شکیبم از رخ جانان نمی شود چکنم</p></div>
<div class="m2"><p>جدا شدن ز تو ای جان نمی شود چکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شراب اشک و کباب جگر مهیا شد</p></div>
<div class="m2"><p>ولی خیال تو مهمان نمی شود چکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار جهد نمودم که راز نگشایم</p></div>
<div class="m2"><p>ز دست دیده گریان نمی شود چکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آن شدم که دگر آه آتشین نزنم</p></div>
<div class="m2"><p>ز سوز سینه بریان نمی شود چکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میسرم نشود سر عشق پوشیدن</p></div>
<div class="m2"><p>فروغ مهر چو پنهان نمی شود چکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بصد نیاز دهم جان برای عشوه و ناز</p></div>
<div class="m2"><p>چو این معامله آسان نمی شود چکنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوای درد دل خود ز من مجوی حسین</p></div>
<div class="m2"><p>علاج عشق بدرمان نمی شود چکنم</p></div></div>