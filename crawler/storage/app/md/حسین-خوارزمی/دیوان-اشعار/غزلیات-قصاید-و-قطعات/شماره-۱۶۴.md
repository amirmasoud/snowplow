---
title: >-
    شمارهٔ  ۱۶۴
---
# شمارهٔ  ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>گر چه از دست غمت حال پریشان دارم</p></div>
<div class="m2"><p>نکنم ترک غم عشق تو تا جان دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان چه باشد که از او دل نتوانم برداشت</p></div>
<div class="m2"><p>من که در سر هوس صحبت جانان دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مقیمان مقام سر کویت چو شدم</p></div>
<div class="m2"><p>کافرم گر هوس روضه رضوان دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این همه شور من از شکر شیرین تو است</p></div>
<div class="m2"><p>وین همه گریه از آن پسته خندان دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بادم سرد و رخ زرد و دل غم پرورد</p></div>
<div class="m2"><p>نتوانم که دمی درد تو پنهان دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماجرای دل من دیده بمردم بنمود</p></div>
<div class="m2"><p>زان شکایت همه از دیده گریان دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خیالت شب دوشینه شکایت کردم</p></div>
<div class="m2"><p>که طبیبی ز تو من حال پریشان دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بکرشمه نظری کرد بسوی من و گفت</p></div>
<div class="m2"><p>تو کدامی که چو تو خسته فراوان دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روی بنما و بدان قامت رعنا بخرام</p></div>
<div class="m2"><p>که هوای سمن و سرو خرامان دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کم مبادا نفسی درد تو از جان حسین</p></div>
<div class="m2"><p>که من خسته هم از درد تو درمان دارم</p></div></div>