---
title: >-
    شمارهٔ  ۱۸۵
---
# شمارهٔ  ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>الا ای طایر سدره نشیمن</p></div>
<div class="m2"><p>چرا کردی در این کاشانه مسکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا از بهر جولانگاه نزهت</p></div>
<div class="m2"><p>فراز عرش رحمانست گلشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو ای شهباز قدسی چون کبوتر</p></div>
<div class="m2"><p>طناب حرص کردی طوق گردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هلا ای رستم پیکار وحدت</p></div>
<div class="m2"><p>فرو مگذار اندر چاه بیژن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو جغد ای طایر قدسی نشاید</p></div>
<div class="m2"><p>بسر بردن در این ویرانه گلخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو اندر خانه تاریک و عالم</p></div>
<div class="m2"><p>ز خورشید حقایق گشته روشن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر از خانه برون نتوانی آمد</p></div>
<div class="m2"><p>برای روشنی بگذار روزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل مردان نرفتی زانکه هردم</p></div>
<div class="m2"><p>فریبت میدهد نیرنگ این زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو چون طفلی و عالم چون مشیمه</p></div>
<div class="m2"><p>مخور خون زانکه شد هنگام زادن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قبائی از بقا چون داد شاهت</p></div>
<div class="m2"><p>ز دوش جان لباس تن بیفکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برای اقتباس نور بگذر</p></div>
<div class="m2"><p>ز رخت خویش در وادی ایمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دهن بسته چو غنچه چند باشی</p></div>
<div class="m2"><p>چو گل خنده زنان بیرون شو از تن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو خواندی نکته الحق عریان</p></div>
<div class="m2"><p>چو کرم پیله گرد خویش کم تن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز سر عشق آبستن شود دل</p></div>
<div class="m2"><p>اگر نفس از هوا گردد ستردن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گریبانت بدست آور ز چاکی</p></div>
<div class="m2"><p>بکش بر طارم افلاک دامن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو در جنگ آمدی با نفس و شیطان</p></div>
<div class="m2"><p>بچنگ آور ز حکمت تیغ و جوشن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز چنگ دیو نفس ار باز رستی</p></div>
<div class="m2"><p>نتابد پنجه تو گیو و بهمن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسان طره مشکین خوبان</p></div>
<div class="m2"><p>دل مسکین هر بیچاره مشکن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که از آه جگر سوز ضعیفان</p></div>
<div class="m2"><p>بسوزد ماه را ناگاه خرمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روا داری که بر دیوار عمرت</p></div>
<div class="m2"><p>رسد از آهشان سنگ فلاخن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر مرد رهی دست ارادت</p></div>
<div class="m2"><p>بدامان شه آفاق در زن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدرگاه علی نه روی خدمت</p></div>
<div class="m2"><p>که درگاه علی اعلا و اعلن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>معانی حقایق زو محقق</p></div>
<div class="m2"><p>مبانی و دقایق زو مبین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز یمن ذات او احکام ملت</p></div>
<div class="m2"><p>باقوای حجج گشته مبرهن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من از تعلیم آن شاه یگانه</p></div>
<div class="m2"><p>فرو خواندم ز علم دین چنان فن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که در شرح معانی و بدیعش</p></div>
<div class="m2"><p>زبان عقل کل گشته است الکن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همای همتم از یمن جاهش</p></div>
<div class="m2"><p>فراز عرش میسازد نشیمن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرا بر خوان همت نسر طایر</p></div>
<div class="m2"><p>بود کمتر ز یک مرغ مسیمن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سریر سدره ادنی پایه دیدم</p></div>
<div class="m2"><p>چو بر درگاه او گشتم ممکن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بچشم همت من می نماید</p></div>
<div class="m2"><p>سپهر و هرچه در وی نیم ارزن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>الا ای ساقی خمخانه عشق</p></div>
<div class="m2"><p>بده دردی درد عشقم ازدن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا بر چهره خود ساز واله</p></div>
<div class="m2"><p>درخت عقل من از بیخ برکن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بیک جرعه ز لوح دل فرو شوی</p></div>
<div class="m2"><p>روایات احادیث معنعن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرا در نفی کلی محو گردان</p></div>
<div class="m2"><p>خلاصم ده ز احوال لم و لن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تولا چون بدرگاه تو کردم</p></div>
<div class="m2"><p>تبرا میکنم از شر خود من</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از ایرا در همه اطراف گیتی</p></div>
<div class="m2"><p>مرا بدتر ز من کس نیست دشمن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حسین خسته را از فضل دریاب</p></div>
<div class="m2"><p>که فضل تست عین فیض ذوالمن</p></div></div>