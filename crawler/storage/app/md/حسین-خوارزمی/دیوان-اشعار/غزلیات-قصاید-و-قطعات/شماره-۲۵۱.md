---
title: >-
    شمارهٔ  ۲۵۱
---
# شمارهٔ  ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>ای سروناز رونق بستان ما توئی</p></div>
<div class="m2"><p>ای نور دیده شمع شبستان ما توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بار غم چه غم چو توئی دستگیر ما</p></div>
<div class="m2"><p>وز درد دل چه باک چو درمان ما توئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را بر آنچه حکم کنی اعتراض نیست</p></div>
<div class="m2"><p>ما بنده ایم و حاکم و سلطان ما توئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرمان ما برند سلاطین روزگار</p></div>
<div class="m2"><p>گر گوئیم که بنده فرمان ما توئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم بطره تو شبی کز تطاولت</p></div>
<div class="m2"><p>دیوانه ایم و سلسله جنبان ما توئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>احوال ما بدوست بگو مو بمو از آنک</p></div>
<div class="m2"><p>واقف ز حال زار پریشان ما توئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای یوسف مسیح دم از پیش ما مرو</p></div>
<div class="m2"><p>کارام روح و روح دل و جان ما توئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنج دل حسین نشد جای هیچکس</p></div>
<div class="m2"><p>مانند گنج در دل ویران ما توئی</p></div></div>