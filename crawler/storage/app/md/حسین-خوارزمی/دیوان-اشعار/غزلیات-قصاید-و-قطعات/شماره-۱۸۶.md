---
title: >-
    شمارهٔ  ۱۸۶
---
# شمارهٔ  ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>ای دل از وحشت سرای دار گیتی کن کران</p></div>
<div class="m2"><p>بال همت باز کن بر پر بر اوج لامکان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون قبای جان تو دارد طرازی از بقا</p></div>
<div class="m2"><p>دامن همت ز گرد عالم فانی فشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نورد این فرش خاکی را که هنگام عروج</p></div>
<div class="m2"><p>هست مرغ همتت را عرش کمتر آشیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مغیلان گاه غولانت چرا باید نشست</p></div>
<div class="m2"><p>چون چراگاهت مقرر گشت در گلزار جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرمه چشم دل از خاک سیاه فقر کن</p></div>
<div class="m2"><p>پیش از آنساعت که گردد استخوانت سرمه دان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتی عمرت از این غرقاب کی یابد نجات</p></div>
<div class="m2"><p>تا هوای نفس تو باد است و شهوت بادبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون همای همتت بگشاد بال کبریا</p></div>
<div class="m2"><p>باشد از یک بیضه کمتر پیش او هفت آسمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پی اسرار اسری شبروی کن شبروی</p></div>
<div class="m2"><p>تا براق دولتت را برق نبود همعنان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بخلوتخانه وحدت ترا باری بود</p></div>
<div class="m2"><p>خویشتن چون حلقه باری از درونشان در نشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلدل دل در چراگاه از ریاض خلد ساز</p></div>
<div class="m2"><p>چشم آخر بین تو بند از آخور آخر زمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نوید عاطفت والله یدعوا گوش کن</p></div>
<div class="m2"><p>تا ترا رضوان شود در روضه کمتر میزبان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>توشه ای از خوشه چرخ و ثوابت کم طلب</p></div>
<div class="m2"><p>چون خران کاه کش کمجوی راه کهکشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چشم بر قرص مه و خورشید تا کی باشدت</p></div>
<div class="m2"><p>بگذر و بگذار با دونان گیتی این دونان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین ابای بی نمک دستت میالا تا شوی</p></div>
<div class="m2"><p>بر سر خوان ابیت عند ربی میهمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پاسبان بر بام قصرت از قصور همت است</p></div>
<div class="m2"><p>بندگی کن تا شود حفظ خدایت پاسبان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سایبان از فضل حق گر هست هیچت باک نیست</p></div>
<div class="m2"><p>بر در و دیوار قصرت گر نباشد سایبان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همدمی چون نیست پیدا راز پنهان خوشتر است</p></div>
<div class="m2"><p>محرمی چون نیست حاصل مهر بهتر بر دهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زین زبان دانی شوی فردا زبانی را زبون</p></div>
<div class="m2"><p>گر تو نتوانی شدن امروز مالک بر زبان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر در و دیوار کثرت آتش دل چون زنی</p></div>
<div class="m2"><p>یابی از توفیق حق بر بام وحدت نردبان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر غبار بندگی سازی طراز آستین</p></div>
<div class="m2"><p>بر در قربت توانی گشت خاک آستان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دم ز آوا برکش و با رنگ بی رنگی بساز</p></div>
<div class="m2"><p>رنگ و آوا را بهل با ارغوان و ارغنان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بادیه پر غول و تو در خواب غفلت مانده ای</p></div>
<div class="m2"><p>با چنین خفتن عجب باشد اگر یابی امان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کعبه مقصود دور است و تو غافل خفته ای</p></div>
<div class="m2"><p>خیز و محمل بند چون در جنبش آمد کاروان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قافله بگذشت و تو بانگ درا می نشنوی</p></div>
<div class="m2"><p>زانکه هست از جوش غفلت گوش جان تو گران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مال و سر افشان بپای فقر و جان ایثار کن</p></div>
<div class="m2"><p>کین متاع نازنین ناید بدستت رایگان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون نجیب فقر آمد زیر زینت کی کند</p></div>
<div class="m2"><p>حادثات دهر سوی تو جنیبتها روان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دیده از عیب همه اسرار باید دوختن</p></div>
<div class="m2"><p>تا زبانت گردد از اسرار غیبی ترجمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرد معنی را ز قول و فعل میباید شناخت</p></div>
<div class="m2"><p>راه حق نتوان سپردن با رداء و طیلسان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>طیلسان بر دوش تو سودی نخواهد داشتن</p></div>
<div class="m2"><p>چون تو با معجر برون آئی از این طی لسان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا تو با خویشی نیابی هرگز از جانان خبر</p></div>
<div class="m2"><p>بی نشان شو تا توانی یافت از وصلش نشان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از هویت دم زنی باشی عزیز هر دو کون</p></div>
<div class="m2"><p>با هوا همراه گردی آیدت ذل هوان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کی رسی از لا بالا تا نباشد مرترا</p></div>
<div class="m2"><p>مرکب لاهوت از الا و هو در زیر ران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دل بوسواس امل دور افتد از حضرت بلی</p></div>
<div class="m2"><p>آدم از یک وسوسه بیرون شد از صدر جنان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>راه حق در پیش و رهبر نفس هشداری حسین</p></div>
<div class="m2"><p>منزلت پر آفتست و غول داری دیده بان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نفس چون در ملک خورسندی برافرازد علم</p></div>
<div class="m2"><p>خسروش خاسر نماید هم بود طاغی طغان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر ز سر نیستی و هستیت باشد خبر</p></div>
<div class="m2"><p>کی شود از نیستی غمگین ز هستی شادمان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عمر کوته شد سکندر را بدان ملکی که هست</p></div>
<div class="m2"><p>خضر را با مفلسی بنگر حیات جاودان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای خداوندی که بر مرصاد جانها حاکمی</p></div>
<div class="m2"><p>جان ما را زین رصدگاه حوادث وارهان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فکر سودای جهان جان مرا محبوس کرد</p></div>
<div class="m2"><p>جان خلاصم ده ز فکر اینم و سودای آن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پادشاها از کمال لطف خود ده جذبه ای</p></div>
<div class="m2"><p>وانگه این بیچاره را از ننگ هستی وارهان</p></div></div>