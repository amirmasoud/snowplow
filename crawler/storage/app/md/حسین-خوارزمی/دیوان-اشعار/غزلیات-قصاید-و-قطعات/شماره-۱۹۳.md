---
title: >-
    شمارهٔ  ۱۹۳
---
# شمارهٔ  ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>اگر چه شد دل ریشم ز دست هجر تو خون</p></div>
<div class="m2"><p>نشد خیال وصال تو از سرم بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وفا و مهر تو از جان و دل همی ورزم</p></div>
<div class="m2"><p>اگر چه میکشم از تو جفای گوناگون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عین جهل بود گر ز عشق برگردم</p></div>
<div class="m2"><p>ز بار غم الف قدم ار شود چون نون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نماند طاقتم از هجر و صبر من کم شد</p></div>
<div class="m2"><p>ولیک عشق تو هر لحظه میشود افزون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برفته از دل من نقش غیرت از غیرت</p></div>
<div class="m2"><p>درون مسکن دل عشق تو نمود سکون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نه بسته زنجیر طره ات گردم</p></div>
<div class="m2"><p>خرد هر آینه نسبت کند مرا مجنون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بسکه گریه کنان از در تو میگذرم</p></div>
<div class="m2"><p>شده ست کوی تو از خون دیده ام گلگون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزار کس چو حسین آمدند بر در تو</p></div>
<div class="m2"><p>دمی ز خانه برون شو برای اهل درون</p></div></div>