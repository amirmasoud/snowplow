---
title: >-
    شمارهٔ  ۱۴۵
---
# شمارهٔ  ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>قفل در ما بستی و پندار تو دیدیم</p></div>
<div class="m2"><p>با خویش مشو بسته که ما جمله کلیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مفتاح ترا نیست در این باب فتوحی</p></div>
<div class="m2"><p>کشاف ترا لایق این کشف ندیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هستی ما آتش عشقش چو درافتاد</p></div>
<div class="m2"><p>از بستگی بند بیکبار رهیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا وصله اقبال بدوزیم ز وصلش</p></div>
<div class="m2"><p>در عشق بسی خرقه ناموس دریدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاصل همه این است که ای یار در این راه</p></div>
<div class="m2"><p>پیوسته بیاریم چو از خویش بریدیم</p></div></div>