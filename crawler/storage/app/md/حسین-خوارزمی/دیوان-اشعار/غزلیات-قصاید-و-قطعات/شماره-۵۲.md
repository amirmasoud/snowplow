---
title: >-
    شمارهٔ  ۵۲
---
# شمارهٔ  ۵۲

<div class="b" id="bn1"><div class="m1"><p>مرا چو کعبه دولت حریم خانه اوست</p></div>
<div class="m2"><p>براستان که سر من بر آستانه اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه محض گناهم امیدواری من</p></div>
<div class="m2"><p>بفیض شامل الطاف بی کرانه اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار طایر قدسی باختیار چو من</p></div>
<div class="m2"><p>اسیر طره خال چو دام و دانه اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه نیست یکی ذره بی نشان رخش</p></div>
<div class="m2"><p>هنوز دیده ما طالب نشانه اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بروز حشر نیاید بخویشتن آری</p></div>
<div class="m2"><p>کسیکه مست و خراب از می شبانه اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجاست مطرب ما تا نوای ساز کند</p></div>
<div class="m2"><p>که رقص حالت عشاق از ترانه اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مسیح خسته دلان گوئیا نمیسازد</p></div>
<div class="m2"><p>که خسته همچو من زار از زمانه اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر بکلبه احزان ما نهد تشریف</p></div>
<div class="m2"><p>سزد که خانه این بنده بنده خانه اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا که طبع حسین از پی نثار آورد</p></div>
<div class="m2"><p>از آن جواهر غیبی که در خزانه اوست</p></div></div>