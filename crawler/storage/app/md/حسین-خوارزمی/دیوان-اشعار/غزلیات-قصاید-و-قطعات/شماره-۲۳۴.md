---
title: >-
    شمارهٔ  ۲۳۴
---
# شمارهٔ  ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>بشارت باد ای عاشق که یار آمد بمهمانی</p></div>
<div class="m2"><p>سبک جان را نثارش کن مکن دیگر گرانجانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا آشفته عقلی گر از عشقش خبر داری</p></div>
<div class="m2"><p>چرا وابسته جانی اگر جویای جانانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر خواهی که عشق او گریبان گیر جان گردد</p></div>
<div class="m2"><p>برافشان دامن همت ز گرد عالم فانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الا ای طایر قدسی در این گلشن چه میپوئی</p></div>
<div class="m2"><p>مگر یادت نمی آید ز گلشنهای روحانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بومان گرد هر ویران چرا سرگشته میکردی</p></div>
<div class="m2"><p>بسوی شاه خود بازآ که تو شهباز سلطانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآور یکنفس از جان بسوزان این دو عالم را</p></div>
<div class="m2"><p>که تا جانان پدید آید از این جلباب ظلمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تیغ عشق قربان شو شهید عشق جانان شو</p></div>
<div class="m2"><p>که تا عمر ابد یابی بحکم نص فرقانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلا در بوته عشقش دمی بگداز و صافی شو</p></div>
<div class="m2"><p>که نقد قلب نستانند صرافان ربانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسین ار بنده فرمان شوی سلطان عشقش را</p></div>
<div class="m2"><p>سلاطین جهان الحق کنندت بنده فرمانی</p></div></div>