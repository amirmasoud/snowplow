---
title: >-
    شمارهٔ  ۴۲
---
# شمارهٔ  ۴۲

<div class="b" id="bn1"><div class="m1"><p>دوش از حضور تو دل ما خوش سرور داشت</p></div>
<div class="m2"><p>وز منظر تو چشم نظر باز نور داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کنج این خرابه دلم با تو ای پری</p></div>
<div class="m2"><p>نی آرزوی روضه نه سودای حور داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خدمت تو صحبت حور بهشت را</p></div>
<div class="m2"><p>زاهد از آن گزید که عقلش قصور داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دیده دید ماه جمالت زیاده شد</p></div>
<div class="m2"><p>مهری که با تو جان ستمکش ز دور داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پرتو تجلی انوار عارضت</p></div>
<div class="m2"><p>هم دیده روشنائی و هم دل سرور داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب تا سحر ز شام خط و صبح عارضت</p></div>
<div class="m2"><p>خاطر همان حکایت موسی و طور داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حلقه های زلف پریشان دلکشت</p></div>
<div class="m2"><p>تا بامداد حلقه دلها حضور داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می یافت گوش جان من از صوت دلگشت</p></div>
<div class="m2"><p>آن لذتی که نغمه صاحب زبور داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امروز حاسدان تو در ماتمند از آنک</p></div>
<div class="m2"><p>با تو حسین دلشده دوشینه سور داشت</p></div></div>