---
title: >-
    شمارهٔ  ۷۵
---
# شمارهٔ  ۷۵

<div class="b" id="bn1"><div class="m1"><p>مرا ز مدرسه عشقت بخانقاه کشید</p></div>
<div class="m2"><p>بصدر صفه دولت ز پایگاه کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیث آتش عشقم مگر رسید به نی</p></div>
<div class="m2"><p>که نی ز سوز درون صد هزار آه کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم چو پای ارادت نهاد در ره عشق</p></div>
<div class="m2"><p>نخست دست تمنا ز مال و جاه کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که بدرقه اش عشق شد بکعبه وصل</p></div>
<div class="m2"><p>نه خوف بادیه دید و نه رنج راه کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکست لشکر صبر و گریخت شحنه عقل</p></div>
<div class="m2"><p>چو در دیار دلم عشق تو سپاه کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخت بدعوی خونم نوشت خط آنگاه</p></div>
<div class="m2"><p>دو ترک کافر سرمست را گواه کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن نزار من زار شد هلاک از غم</p></div>
<div class="m2"><p>که بار کوه نیارم به برگ کاه کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه غم ز سرزنش یار و طعنه اغیار</p></div>
<div class="m2"><p>مرا که سایه لطف تو در پناه کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر گناه بود سر بپایت افکندن</p></div>
<div class="m2"><p>حسین دست نخواهد از این گناه کشید</p></div></div>