---
title: >-
    شمارهٔ  ۷۴
---
# شمارهٔ  ۷۴

<div class="b" id="bn1"><div class="m1"><p>گر پریچهره مه پیکر من باز آید</p></div>
<div class="m2"><p>روشنائی ببصر جان ببدن باز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرتو نور تجلی رسد از جانب طور</p></div>
<div class="m2"><p>نفس رحمت رحمان ز یمن باز آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سر طره آن ترک خطائی بمشام</p></div>
<div class="m2"><p>نکهت نافه آهوی ختن باز آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه کار من دلسوخته چون زر گردد</p></div>
<div class="m2"><p>اگر آن سنگدل و سیم ذقن باز آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردم دیده من درج پر از در دارد</p></div>
<div class="m2"><p>همچو غواص که از بحر بدن باز آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش بود گر ز جفای کاری و بیدادگری</p></div>
<div class="m2"><p>آن بت عشوه ده عهدشکن باز آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طوطی طبع من از چه دهن نطق ببست</p></div>
<div class="m2"><p>بگشاید اگر آن پسته دهن باز آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارم امید که بر رغم حسودان ز سفر</p></div>
<div class="m2"><p>مونس جان حسین ابن حسن باز آید</p></div></div>