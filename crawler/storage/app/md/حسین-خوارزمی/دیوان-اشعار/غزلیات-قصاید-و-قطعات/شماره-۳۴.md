---
title: >-
    شمارهٔ  ۳۴
---
# شمارهٔ  ۳۴

<div class="b" id="bn1"><div class="m1"><p>تا چند ز دیدار تو مهجور توان زیست</p></div>
<div class="m2"><p>تو جان عزیزی ز تو چون دور توان زیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکس که نظر بر چو تو منظور بینداخت</p></div>
<div class="m2"><p>گوید که جدا گشته ز منظور توان زیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریاب مرا چون رمقی هست کز این بیش</p></div>
<div class="m2"><p>سودای محال است که مهجور توان زیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر بوی یکی پرسشت ای عیسی جانها</p></div>
<div class="m2"><p>عمری چو من آشفته و رنجور توان زیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آرزوی آب زلالی ز وصالت</p></div>
<div class="m2"><p>در آتش هجران تو محرور توان زیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کوی تو بر بوی تو ای حور پریوش</p></div>
<div class="m2"><p>فارغ شده از روضه و بی حور توان زیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چشم حسین از غم تو اشک ببارد</p></div>
<div class="m2"><p>ناگشته بسودای تو مشهور توان زیست</p></div></div>