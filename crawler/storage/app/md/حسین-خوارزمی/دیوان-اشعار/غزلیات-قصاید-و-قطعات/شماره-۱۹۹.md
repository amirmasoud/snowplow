---
title: >-
    شمارهٔ  ۱۹۹
---
# شمارهٔ  ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>ای غم سودای تو خلوت نشین جان من</p></div>
<div class="m2"><p>درد روح افزای تو سرمایه درمان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو گل باغ بهشت و جان من بستان آن</p></div>
<div class="m2"><p>تا برفتی رفت بی تو رونق بستان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم من جام شرابست و دل زارم کباب</p></div>
<div class="m2"><p>تا مگر گردد خیال تو شبی مهمان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بصورت گشته ام غایب ز جانان باک نیست</p></div>
<div class="m2"><p>نیست غایب جان و دل از حضرت جانان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با سر کویت فراغت دارم از باغ بهشت</p></div>
<div class="m2"><p>نیست غیر از کوی جانان روضه رضوان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوشوار جان کند از در الفاظ حسین</p></div>
<div class="m2"><p>گر رسد شعرت به گوش شاه معنی‌دان من</p></div></div>