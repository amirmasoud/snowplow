---
title: >-
    شمارهٔ  ۲۱۰
---
# شمارهٔ  ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>گفته ای الیوم اکملت لکم دین الهدی</p></div>
<div class="m2"><p>آن زمان کین رحمت مهداة اهدا کرده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ز مهر او تواند صبح صادق دم زدن</p></div>
<div class="m2"><p>غرّه او را ز نور مهر غرّا کرده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بود شب آیتی از گیسوی مشکین او</p></div>
<div class="m2"><p>طره‌های لیل را از وی مطرا کرده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نسیم جعد او همراه کرده نکهتی</p></div>
<div class="m2"><p>زو همه آفاق را پر مشک سارا کرده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمه‌ای را از نسیم گلستان خلق او</p></div>
<div class="m2"><p>رشک انفاس روان‌بخش مسیحا کرده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن ملاحت داده‌ای او را که از یک دیدنش</p></div>
<div class="m2"><p>یوسفان شش جهت را چون زلیخا کرده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بهار شرع از باغ ریاحین و خضر</p></div>
<div class="m2"><p>صحن غبرا را چو سطح چرخ خضرا کرده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوس سبحانی بنام آن شه گیتی زده</p></div>
<div class="m2"><p>مهر منشور جلال او را منیرا کرده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در معارج از مدارج داده او را ارتقا</p></div>
<div class="m2"><p>کم کسی را واقف از اسرار اسری کرده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گاه رمی او ز قول ما رمیت اذرمیت</p></div>
<div class="m2"><p>بر رموز مخفی توحید احیا کرده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اصفیا را صف زدن فرموده بر درگاه او</p></div>
<div class="m2"><p>عیش ارباب صفا زیشان مصفا کرده‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر زبان نطق مهر خامشی پس چون زنم</p></div>
<div class="m2"><p>چون تو کشف سر عشق از من تقاضا کرده‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کشور جان را گرفته از کف سلطان عقل</p></div>
<div class="m2"><p>با سپاه عشق شورانگیز یغما کرده‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خسروانه نکته شیرین به گوش جان من</p></div>
<div class="m2"><p>خوانده و آفاق را پرشور و غوغا کرده‌ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کرده غارت جملگی سرمایه عقل مرا</p></div>
<div class="m2"><p>جان غم فرسود من آماج سودا کرده‌ای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ما ظلومیم و جهول از احتمال بار یار</p></div>
<div class="m2"><p>گر چه رسواییم یارب نی تو رسوا کرده‌ای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کی پذیرد شأن ما پستی ز طعن قدسیان</p></div>
<div class="m2"><p>قدر ما را چون ز کرمنا تو اعلا کرده‌ای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از حمال بار کی ترسیم چون تو از کرم</p></div>
<div class="m2"><p>حمل ما را صد تلافی از حملنا کرده‌ای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از طریق لطف و احسان وارهان ما را ز ما</p></div>
<div class="m2"><p>ایکه مجموع حجاب ما هم از ما کرده‌ای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غیر لااحصی چه گوید در ثنای تو حسین</p></div>
<div class="m2"><p>زان که حمد خویشتن را هم تو احصا کرده‌ای</p></div></div>