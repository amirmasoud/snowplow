---
title: >-
    شمارهٔ  ۱۲۴
---
# شمارهٔ  ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>ای ستمگر که نداری خبر از بیدل خویش</p></div>
<div class="m2"><p>ز آتش هجر مسوزان دل ریشم زین بیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هلاک چو منی کی بود اندیشه ترا</p></div>
<div class="m2"><p>پادشا را چه تفاوت ز هلاک درویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نگویم که دوای دل ریشم فرمای</p></div>
<div class="m2"><p>راضیم گر بزنی زخم دگر بر دل ریش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست در وصل تو ما را هوس روضه و حور</p></div>
<div class="m2"><p>نیست در عشق تو ما را سر بیگانه و خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیگران را اگر از تیر بلا بیمی هست</p></div>
<div class="m2"><p>هست در کوی تو قربان شدنم مذهب و کیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناوک غمزه تو آنچه کند بر دل من</p></div>
<div class="m2"><p>تیغ قصاب ستمگر نکند بر تن میش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه از طره مشگین تو بوئی برده ست</p></div>
<div class="m2"><p>عنبر و غالیه بویا عرق گل اندیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو مورم کمر بندگیت بسته هنوز</p></div>
<div class="m2"><p>گرچه صدبار مگس وار براندیم از پیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشت چون خانه زنبور دل ریش حسین</p></div>
<div class="m2"><p>غمزه شهد لبی بس که بر او زد سر نیش</p></div></div>