---
title: >-
    شمارهٔ  ۶۸
---
# شمارهٔ  ۶۸

<div class="b" id="bn1"><div class="m1"><p>جان فدای آنکه ما را بی‌محابا می‌کشد</p></div>
<div class="m2"><p>کشته را جان می‌دهد پنهان و پیدا می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیر دلدوز خدنگ غمزه خونریز خود</p></div>
<div class="m2"><p>سوی دیگر می‌کشد آن شوخ و ما را می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن قد و بالا بلای جان عاشق شد بلی</p></div>
<div class="m2"><p>چون بلای ناگهان آید ز بالا می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بود فردا میان گشتگان عشق دوست</p></div>
<div class="m2"><p>عاشق آن نازنین خود را به عمدا می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشتنش آب حیات عاشقان آمد از آن</p></div>
<div class="m2"><p>زنده می‌گردم من آشفته‌دل تا می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیگران را گر تقاضا می‌کند میر اجل</p></div>
<div class="m2"><p>عاشق بیچارهٔ خود را بی‌تقاضا می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر به قصد کشتن آید دوست منعش کم کنید</p></div>
<div class="m2"><p>تا حسین خسته را بکشد که زیبا می‌کشد</p></div></div>