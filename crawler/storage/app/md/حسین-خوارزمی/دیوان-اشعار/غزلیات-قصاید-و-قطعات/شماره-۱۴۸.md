---
title: >-
    شمارهٔ  ۱۴۸
---
# شمارهٔ  ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>وقت آنست که ما جانب میخانه شویم</p></div>
<div class="m2"><p>چون پری ساقی ما شد همه دیوانه شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جرعه ای چون بچشیدیم ز میخانه عشق</p></div>
<div class="m2"><p>عهد و پیمان شکنیم از پی پیمانه شویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشنای حرم عشق چو گشتیم کنون</p></div>
<div class="m2"><p>خویش را ترک کنیم از همه بیگانه شویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجلس ما چو ز شمع رخ او روشن شد</p></div>
<div class="m2"><p>بال و پر سوخته از عشق چو پروانه شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما که از جام تجلی جمالش مستیم</p></div>
<div class="m2"><p>حاش لله که دگر عاقل و فرزانه شویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنج ویران چو بود مخزن گنج شاهی</p></div>
<div class="m2"><p>از پی گنج حقایق همه ویرانه شویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قطره هائیم جدا گشته ز بحر احدی</p></div>
<div class="m2"><p>غوطه در بحر خوریم و همه دردانه شویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو آیینه صافی همه یکرو باشیم</p></div>
<div class="m2"><p>چند دو روی و دو سر همچو سر شانه شویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حبذا شادی و آن حال که ما همچو حسین</p></div>
<div class="m2"><p>بی‌خود و مست از آن غمزه مستانه شویم</p></div></div>