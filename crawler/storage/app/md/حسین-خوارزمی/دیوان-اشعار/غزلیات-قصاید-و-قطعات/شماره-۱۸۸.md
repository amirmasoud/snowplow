---
title: >-
    شمارهٔ  ۱۸۸
---
# شمارهٔ  ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>ای سر کویت بلای روضه رضوان من</p></div>
<div class="m2"><p>درد روح افزای عشقت راحت و درمان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا مرا با چون تو جانان آشنائی دست داد</p></div>
<div class="m2"><p>گشت از غیر تو بیگانه ز غیرت جان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهد معنی چو از جلباب صورت رخ نمود</p></div>
<div class="m2"><p>نیست از غیر تو آگه جان معنی دان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا شدم مرآة عشق و عشق بر من جلوه کرد</p></div>
<div class="m2"><p>من شدم حیران او و عالمی حیران من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من کیم ای عشق مطلق بنده فرمان تو</p></div>
<div class="m2"><p>تو که باشی مر مرا سلطان من سلطان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر کنم اندیشه وصلت توئی اندیشه ام</p></div>
<div class="m2"><p>ور بنالم از فراقت هم توئی افغان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساختم از سر قدم غواص دریاها شدم</p></div>
<div class="m2"><p>گوهری چون تو برآمد ناگه از عمان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غمزه ات ای عشق چون هردم کند غمازئی</p></div>
<div class="m2"><p>آشکارا چون نگردد حالت پنهان من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن من گردد سعادت‌ها که در کونین هست</p></div>
<div class="m2"><p>گر حسین خسته را گویی که او هست آن من</p></div></div>