---
title: >-
    شمارهٔ  ۱۷۹
---
# شمارهٔ  ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>تا خاک صفت معتکف آن سر کویم</p></div>
<div class="m2"><p>بی دردم اگر روضه فردوس بجویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آن صنم موی میان رفت ز چشمم</p></div>
<div class="m2"><p>از ناله چو نائی شده وز مویه چو مویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر شهره شهری شدم از شوق عجب نیست</p></div>
<div class="m2"><p>چون رفت ز شهر آنکه من آشفته اویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیسی دم من چون سر بیمار ندارد</p></div>
<div class="m2"><p>پیش که روم درد دل خود بکه گویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردم قدم از سر که روم راه هوایش</p></div>
<div class="m2"><p>کین راه نشاید که بدین پای بپویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا روی نهم بر کف پایت دهدم دست</p></div>
<div class="m2"><p>کز خاک سر کوی تو چون سبزه برویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیف است که اغیار برد میوه وصلت</p></div>
<div class="m2"><p>وز باغ رخت من گل سیراب نبویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی که حسین از در ما چون نرود هیچ</p></div>
<div class="m2"><p>من چون روم ای جان که گدای سر کویم</p></div></div>