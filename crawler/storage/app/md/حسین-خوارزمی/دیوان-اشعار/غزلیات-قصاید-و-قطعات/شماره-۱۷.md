---
title: >-
    شمارهٔ  ۱۷
---
# شمارهٔ  ۱۷

<div class="b" id="bn1"><div class="m1"><p>ای بر دل شکسته ز درد تو داغ‌ها</p></div>
<div class="m2"><p>در سینه‌ام ز آتش عشقت چراغ‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون هر دلی شده که به داغ تو مبتلاست</p></div>
<div class="m2"><p>نگشاید از تفرج گلزار و باغ‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان‌های ما به داغ جدایی بسوختی</p></div>
<div class="m2"><p>باشد که رخت خویش شناسی به داغ‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ورطه بلای تو دل گمشده است و جان</p></div>
<div class="m2"><p>شد سال‌ها که می‌کند از وی سراغ‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرح شمایل تو حسین ار کند شود</p></div>
<div class="m2"><p>ز انفاس جان‌فزاش معطر دماغ‌ها</p></div></div>