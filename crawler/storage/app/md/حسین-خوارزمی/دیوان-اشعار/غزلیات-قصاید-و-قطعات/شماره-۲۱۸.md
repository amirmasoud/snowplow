---
title: >-
    شمارهٔ  ۲۱۸
---
# شمارهٔ  ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>ای آنکه در دیار دلم خانه کرده ای</p></div>
<div class="m2"><p>گنجی از آنمقام بویرانه کرده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه عقلها بنرگس مخمور برده</p></div>
<div class="m2"><p>گه فتنه ها بغمزه مستانه کرده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم پر از روایح و مشک و عبیر شد</p></div>
<div class="m2"><p>چون گیسوی معنبر خود شانه کرده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ای پری سلاسل مشکین نموده ای</p></div>
<div class="m2"><p>ارباب عقل را همه دیوانه کرده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آرزوی لعل شکر بار خویشتن</p></div>
<div class="m2"><p>چشم مرا خزینه دردانه کرده ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من در بروی غیر ز غیرت چو بسته ام</p></div>
<div class="m2"><p>تا در حریم جان و دلم خانه کرده ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغ دل مرا که نشیمن ز سدره داشت</p></div>
<div class="m2"><p>ایشمع دلفروز تو پروانه کرده ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود کرده آشنا بمن ای شوخ دلربا</p></div>
<div class="m2"><p>آنگه روش چو مردم بیگانه کرده ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برخوان وصل داده صلا اهل عشق را</p></div>
<div class="m2"><p>یاد حسین بیدل شیدا نکرده ای</p></div></div>