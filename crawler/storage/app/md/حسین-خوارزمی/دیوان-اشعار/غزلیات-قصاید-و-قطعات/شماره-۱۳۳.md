---
title: >-
    شمارهٔ  ۱۳۳
---
# شمارهٔ  ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>عزیزان وفاپیشه مبارکباد این منزل</p></div>
<div class="m2"><p>که می‌افزاید از نورش صفای جان اهل دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمعنی کعبه جانهاست این منزلگه عالی</p></div>
<div class="m2"><p>برای طوفش از هر سو ببسته قدسیان محمل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خاکپای این درگه طلب کن دولت ای عاشق</p></div>
<div class="m2"><p>که هست این آیت رحمت شده بر خاکیان نازل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلا بیدار شو یکدم که جان عزم سفر دارد</p></div>
<div class="m2"><p>چه جای خواب این مسکن که همراهست مستعجل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وداع عمر نزدیک و تو خود دوری نه ای آگه</p></div>
<div class="m2"><p>رفیقان بار بستند و تو خود بنشسته ای غافل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا این خویشتن بینی سبل شد دیده دل را</p></div>
<div class="m2"><p>اگر دیدار میخواهی ز دید خویشتن بگسل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا در پیش دلداران بود جان باختن آسان</p></div>
<div class="m2"><p>ولیکن زیستن یکدم بود بی دوستان مشکل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسین از یار چون دوری چه عیش از عمر میجوئی</p></div>
<div class="m2"><p>چو رفت آن حاصل عمرت چه سود از عمر بی‌حاصل</p></div></div>