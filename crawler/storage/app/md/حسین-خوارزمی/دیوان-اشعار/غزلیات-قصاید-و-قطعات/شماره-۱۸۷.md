---
title: >-
    شمارهٔ  ۱۸۷
---
# شمارهٔ  ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>ماه من چون آگهی از ناله شب‌های من</p></div>
<div class="m2"><p>رحمتی کن بر دل بیچاره شیدای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآتش سودایت ای شمع جهان افروز دل</p></div>
<div class="m2"><p>سوختم پروانه وار و نیستت پروای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز روی لطف خاکپای خود خوانی مرا</p></div>
<div class="m2"><p>عرش و کرسی تاج سر سازند خاکپای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آستینم بوسه جای خسروان دین بود</p></div>
<div class="m2"><p>گر ز خاک آستان خویش سازی جای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر رود از دست من سرمایه سود دو کون</p></div>
<div class="m2"><p>کم نخواهد شد ز جان سوخته سودای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آبروئی میبرم از سجده خاک درت</p></div>
<div class="m2"><p>تا شناسد روز محشر هر کسی سیمای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشنائی کرد با من عشق عالم سوز او</p></div>
<div class="m2"><p>کله بر افلاک بندد آه دودآسای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا ز خاکپای تو روشن شده چشم حسین</p></div>
<div class="m2"><p>جز تو در عالم ندیده دیده بینای من</p></div></div>