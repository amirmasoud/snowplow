---
title: >-
    شمارهٔ  ۱۴
---
# شمارهٔ  ۱۴

<div class="b" id="bn1"><div class="m1"><p>دوای درد دل خسته ام بکن یارا</p></div>
<div class="m2"><p>بیا که نیست مرا بی تو زیستن یارا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جستجوی تو یارا روان همیسازم</p></div>
<div class="m2"><p>ز چشمه های دو دیده هزار دریا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جماعتی که بکوی تو راه مییابند</p></div>
<div class="m2"><p>کجا کنند تمنا بهشت اعلا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برد خیال تو از ره هزار زاهد را</p></div>
<div class="m2"><p>کند جمال تو شیدا هزار دانا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان ز هوش برفتم ز عشق بالایت</p></div>
<div class="m2"><p>که باز می نشناسم نشیب و بالا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همان زمان که بروی تو دیده بگشادم</p></div>
<div class="m2"><p>بروی غیر تو بستم در سویدا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال حسن ترا زان نمیرسد نقصان</p></div>
<div class="m2"><p>که ساعتی بنوازی حسین شیدا را</p></div></div>