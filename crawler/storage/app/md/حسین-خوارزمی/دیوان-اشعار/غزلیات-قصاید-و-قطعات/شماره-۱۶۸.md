---
title: >-
    شمارهٔ  ۱۶۸
---
# شمارهٔ  ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>من که بر جان و دل از درد تو داغی دارم</p></div>
<div class="m2"><p>با سر کوی تو از روضه فراغی دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خیال قد چون سرو و رخ گل رنگت</p></div>
<div class="m2"><p>راستی در نظر آراسته باغی دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تو در انجمن آئی مه تابان چکنم</p></div>
<div class="m2"><p>پیش خورشید چه پروای چراغی دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال دل بی تو خرابست تو دانی ز دلم</p></div>
<div class="m2"><p>من رسولم بخدا رسم بلاغی دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بفریاد رقیب از سر کویت نروم</p></div>
<div class="m2"><p>شاهبازم چه غم از بانگ کلاغی دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که بر روی تو از طره ات آشفته ترم</p></div>
<div class="m2"><p>نیست عیبی اگر آشفته دماغی دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یادگارم ز تو این است که من همچو حسین</p></div>
<div class="m2"><p>بر دل از آتش سودای تو داغی دارم</p></div></div>