---
title: >-
    شمارهٔ  ۲۵۲
---
# شمارهٔ  ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>بسته ام دل بغم عشق پری رخساری</p></div>
<div class="m2"><p>صنم سیمبری حور ملک کرداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادی خاطر هر شیفته غمگینی</p></div>
<div class="m2"><p>مرهم سینه هر سوخته بیماری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلبری سرو قدی سیمبری مهروئی</p></div>
<div class="m2"><p>بت شکرشکن و طوطی خوش گفتاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچکس را نبود رغبت مشک تاتار</p></div>
<div class="m2"><p>گر برد باد صبا از سر زلفش تاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واقف از حال دل غمزدگان خویش است</p></div>
<div class="m2"><p>نه چو خوبان زمان عشوه ده غداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه پیش من دلخسته نیامد یکدم</p></div>
<div class="m2"><p>بر دل خسته من نیست از او آزاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای حسین از سر جان بگذر و بگزین عشقش</p></div>
<div class="m2"><p>که نباشد به از این در همه عالم کاری</p></div></div>