---
title: >-
    شمارهٔ  ۲۰۵
---
# شمارهٔ  ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>بیا در بزم عشق ای دل حریف درد جانان شو</p></div>
<div class="m2"><p>برافشان جان بروی یار و از سر تا قدم جان شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر ذوق و صفا خواهی نثار دوست کن جانرا</p></div>
<div class="m2"><p>وگر کیش وفاداری به تیر عشق قربان شو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شاه عشق با چوگان سوی میدان جان آمد</p></div>
<div class="m2"><p>ببوی لذت زخمش برغبت گوی میدان شو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی دان و یکی بین شو ترا آخر که میگوید</p></div>
<div class="m2"><p>که گاهی در پی این باش و گاهی طالب آن شو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر خواهی که ره یابی بخلوتخانه وحدت</p></div>
<div class="m2"><p>ز انس انس دل بگسل چو جن از خلق پنهان شو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسین از دامن مردی به چشم جان بکش گردی</p></div>
<div class="m2"><p>سری بر پای مردان نه به خاک راه یکسان شو</p></div></div>