---
title: >-
    شمارهٔ  ۴۱
---
# شمارهٔ  ۴۱

<div class="b" id="bn1"><div class="m1"><p>روئی است روی دوست که هیچش نظیر نیست</p></div>
<div class="m2"><p>زانرو بهیچ رویم از آن رو گزیر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق آن پری چه ملامت کنی مرا</p></div>
<div class="m2"><p>دیوانه چون ز عقل نصیحت پذیر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکس که داد دست ارادت به پیر عشق</p></div>
<div class="m2"><p>هیچش خبر ز طعنه برنا و پیر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم نظر بعارض خورشید منظری</p></div>
<div class="m2"><p>کز ماه رو بجمله جهانش نظیر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آزاد بنده که شود پای بند او</p></div>
<div class="m2"><p>برگشته طالعی که در این دام اسیر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارم ضمیر روشن و رای منیر از آنک</p></div>
<div class="m2"><p>جز مهر روی دوست مرا در ضمیر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با تاب آفتاب رخش روز و شب مرا</p></div>
<div class="m2"><p>حاجت بمهر انور و بدر منیر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا تا با دزد به طره عنبر فشان او</p></div>
<div class="m2"><p>ما را هوای نکهت مشک و عبیر نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دوست دستگیر حسین شکسته را</p></div>
<div class="m2"><p>کو را بجز تو هیچکسی دستگیر نیست</p></div></div>