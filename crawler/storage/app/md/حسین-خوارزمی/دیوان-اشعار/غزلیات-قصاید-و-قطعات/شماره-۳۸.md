---
title: >-
    شمارهٔ  ۳۸
---
# شمارهٔ  ۳۸

<div class="b" id="bn1"><div class="m1"><p>ای لعل دلپذیر تو سرمایه حیات</p></div>
<div class="m2"><p>ای روی بی نظیر تو خورشید کائنات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتی ز پیش دیده و مردم ز هجر تو</p></div>
<div class="m2"><p>آری فراق روح بود موجب ممات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندان گرفت آتش عشقت دلم که شد</p></div>
<div class="m2"><p>با دل بلای عشق تو بس خوشتر از نجات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عشق تست درد خوش آینده چون دوا</p></div>
<div class="m2"><p>وز دست تست زهر گوارنده چون نبات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آرزوی دیدن رویت همی شود</p></div>
<div class="m2"><p>هر دم ز چشمه های دو چشمم روان فرات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم مودت از تو ندارم از آنکه تو</p></div>
<div class="m2"><p>چون دهر بیوفائی و چون عمر بی ثبات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوی وفا رسد بمشام وصال من</p></div>
<div class="m2"><p>گر بگذری بتربت من از پس وفات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چند از فراق تو سوزد دل حسین</p></div>
<div class="m2"><p>ای سرو ماه پیکر و حور ملک صفات</p></div></div>