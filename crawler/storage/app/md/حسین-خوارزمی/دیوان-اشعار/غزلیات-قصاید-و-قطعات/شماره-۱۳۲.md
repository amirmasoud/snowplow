---
title: >-
    شمارهٔ  ۱۳۲
---
# شمارهٔ  ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>میبرد قد تو از سرو خرامان رونق</p></div>
<div class="m2"><p>پیش روی تو ندارد مه تابان رونق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از می لعل تو یابد دل غمدیده نشاط</p></div>
<div class="m2"><p>وز گل روی تو دارد چمن جان رونق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل بعهد تو نیارد شدن از پرده برون</p></div>
<div class="m2"><p>زانکه او را نبود پیش تو چندان رونق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رونق جمله جهان گر چه زمان جویا شد</p></div>
<div class="m2"><p>نیست بی روی تو در مجمع خوبان رونق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شود مجلس ارباب نظر گر یکشب</p></div>
<div class="m2"><p>گیرد از روی تو ای شمع شبستان رونق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم از نور مه و مهر چه رونق گیرد</p></div>
<div class="m2"><p>گیرد از نور رخت دیده من آن رونق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن اشعار حسین از صفت تست آری</p></div>
<div class="m2"><p>دارد از نعت نبی گفته حسان رونق</p></div></div>