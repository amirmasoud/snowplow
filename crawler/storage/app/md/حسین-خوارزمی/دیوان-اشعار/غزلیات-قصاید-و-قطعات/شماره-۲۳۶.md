---
title: >-
    شمارهٔ  ۲۳۶
---
# شمارهٔ  ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>ای عشق منم از تو سرگشته و سودایی</p></div>
<div class="m2"><p>واندر همه عالم مشهور به شیدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نامه مجنون تا از نام من آغازند</p></div>
<div class="m2"><p>زین بیش اگر بردم سر دفتر دانایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای باده‌فروش من سرمایهٔ جوش من</p></div>
<div class="m2"><p>از تست خروش من من نایم و تو نایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرمایه ناز از تو هم اصل نیاز از تو</p></div>
<div class="m2"><p>هم وامق شیدایی هم دلبر عذرایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر زندگیم خواهی بر من نفسی در دم</p></div>
<div class="m2"><p>من مرده صدساله تو جان مسیحایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اول تو و آخر تو ظاهر تو و باطن تو</p></div>
<div class="m2"><p>مستور ز هر چشمی در عین هویدایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیری ستم‌اندوزی بر دیده من دوزی</p></div>
<div class="m2"><p>آخر چه جگرسوزی یارب چه دلارایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پروانه‌صفت سوزان از شوق فشانم جان</p></div>
<div class="m2"><p>تا گوییم ای جانان تو سوخته مایی</p></div></div>