---
title: >-
    شمارهٔ  ۸۹
---
# شمارهٔ  ۸۹

<div class="b" id="bn1"><div class="m1"><p>یک لحظه مرا بی رخت آرام نباشد</p></div>
<div class="m2"><p>دل را بجز از لعل لبت کام نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیهات که من با تو توانم که نشینم</p></div>
<div class="m2"><p>چون سوی توام زهره پیغام نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در صحبت ما زاهد افسرده نگنجد</p></div>
<div class="m2"><p>در مجلس دلسوختگان خام نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو از چه جهت خوانمت ایسرور خوبان</p></div>
<div class="m2"><p>چون سرو سمن ساق و گل اندام نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بهر گرفتاری مرغ دل عشاق</p></div>
<div class="m2"><p>حقا که چو زلف سیهت دام نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دام فدای تو دل و جان که بجوئی</p></div>
<div class="m2"><p>چون نرگس پر خواب تو با دام نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ظلمت خط تاب جمالت نشود کم</p></div>
<div class="m2"><p>نقصان مه از تیرگی شام نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر مرغ دلی کو بپرد از قفس تن</p></div>
<div class="m2"><p>جز در خم زلف تواش آرام نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکو چو حسین از غم عشق تو خرابست</p></div>
<div class="m2"><p>او را سر ناموس و غم نام نباشد</p></div></div>