---
title: >-
    شمارهٔ  ۱۱۳
---
# شمارهٔ  ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>ای خسرو خوبان لبت از شهد شیرین کاره تر</p></div>
<div class="m2"><p>هم دیده نادیده ز تو عیاره ای عیاره تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نازنین با ناز اگر بیچارگانرا میکشی</p></div>
<div class="m2"><p>اول مرا کش چون منم از دیگران بیچاره تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عشق رخسار و لبت دل خونشده جان سوخته</p></div>
<div class="m2"><p>وز سوز جان و خون دل لب خشگم و رخساره تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانهای خوبان سوختی تنها نه عاشق میکشی</p></div>
<div class="m2"><p>ای از تو خوبان خورده خون تو از همه خونخواره تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر بوی تو گل در چمن صد چاک زد جامه چو من</p></div>
<div class="m2"><p>وز روی غیرت جان من از جامه گل پاره تر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک رهت گشتم ولی از بیم گرد دامنت</p></div>
<div class="m2"><p>دارم ز آب چشم خود خاک رهت همواره تر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگداخت سنگ از آه من لیکن چسود ایماه من</p></div>
<div class="m2"><p>کان دل که داری نیست آن از سنگ خارا خاره تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آواره عشقت بسی هستند در عالم ولی</p></div>
<div class="m2"><p>انصاف ده خود دیده ای هیچ از حسین آواره تر</p></div></div>