---
title: >-
    شمارهٔ  ۲۰۱
---
# شمارهٔ  ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>روزی اگر گذار تو افتد بخاک من</p></div>
<div class="m2"><p>فریاد بشنوی ز دل دردناک من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعلت حیات میدهد ای دوست باک نیست</p></div>
<div class="m2"><p>گر غمزه تو سعی کند در هلاک من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عشق تست جامه جانم هزار چاک</p></div>
<div class="m2"><p>گر خلق بنگرند گریبان چاک من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عشق روی و موی تو چون خاک گشته ام</p></div>
<div class="m2"><p>نشگفت اگر دمد گل و ریحان ز خاک من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تاک وجود من عنب عشق بر دهد</p></div>
<div class="m2"><p>بنگر چه پاک اصل فتاد است تاک من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مه را ز مهر نور فزاید از آن فزود</p></div>
<div class="m2"><p>حسن رخت ز مهر دل و جان پاک من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای عشق تیغ برکش و قتل حسین کن</p></div>
<div class="m2"><p>تا از میانه دور شود اشتراک من</p></div></div>