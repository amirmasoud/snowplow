---
title: >-
    شمارهٔ  ۲۰۲
---
# شمارهٔ  ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>ای در همه عالم پنهان تو و پیدا تو</p></div>
<div class="m2"><p>هم درد دل عاشق هم اصل مداوا تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با ما چو درآمیزی گویم ز سر مستی</p></div>
<div class="m2"><p>ما جمله توایم ای جان یا خود همگی ما تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کسوت هر دلبر هم چهره تو بنموده</p></div>
<div class="m2"><p>در دیده هر عاشق هم کرده تماشا تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پوینده بهر پائی گیرنده بهر دستی</p></div>
<div class="m2"><p>با چشم و زبان ما بینا تو و گویا تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نیستی و هستی صد مرتبه افزونی</p></div>
<div class="m2"><p>برتر ز همه اشیا اندر همه اشیا تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عشق توئی عاشق در کسوت معشوقی</p></div>
<div class="m2"><p>هم وامق شیدائی هم دلبر عذرا تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه ناز کنی با ما گاهی بنیاز آئی</p></div>
<div class="m2"><p>این هر دو ترا زیبد مجنون تو و لیلا تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دیده هر عاقل پیوسته توئی پنهان</p></div>
<div class="m2"><p>وندر نظر عارف همواره هویدا تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در میکده وحدت از عقل بتشویشیم</p></div>
<div class="m2"><p>در ده قدح باده ای ساقی و صهبا تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من نقد دل و جان را در پای تو افشانم</p></div>
<div class="m2"><p>گر دست دهد خلوت ای دوست شبی با تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با غمزه فتانت از بهر حسین الحق</p></div>
<div class="m2"><p>انگیخته ای ای جان صد فتنه به تنها تو</p></div></div>