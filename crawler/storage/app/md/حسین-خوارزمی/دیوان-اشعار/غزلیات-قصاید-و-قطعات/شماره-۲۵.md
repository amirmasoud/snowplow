---
title: >-
    شمارهٔ  ۲۵
---
# شمارهٔ  ۲۵

<div class="b" id="bn1"><div class="m1"><p>خسته هجر گشته ام با تو وصالم آرزوست</p></div>
<div class="m2"><p>تیره شده است چشم من نور جمالم آرزوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تف کار عشق جان سوخت بنار تشنگی</p></div>
<div class="m2"><p>از لب روح بخش تو آب زلالم آرزوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی تو حرام شد مرا با دگری نفس زدن</p></div>
<div class="m2"><p>از نفس مبارکت سحر حلالم آرزوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی تو خیال شد تنم و ز هوس خیال تو</p></div>
<div class="m2"><p>نیست خیال خواب و خور آب خیالم آرزوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دام خطت بمرغ دل گفت اسیر چون شدی</p></div>
<div class="m2"><p>گفت از آنکه دمبدم دانه خالم آرزوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل بحر اشک خود غوطه همیخورم از آنک</p></div>
<div class="m2"><p>دیدن آن دو رشته عقد لئالم آرزوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه وصال او حسین آرزوئی است بس محال</p></div>
<div class="m2"><p>آرزو را چو عیب نیست چونکه وصالم آرزوست</p></div></div>