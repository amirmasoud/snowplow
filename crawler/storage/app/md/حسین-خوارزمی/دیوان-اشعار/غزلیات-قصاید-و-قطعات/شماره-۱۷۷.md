---
title: >-
    شمارهٔ  ۱۷۷
---
# شمارهٔ  ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>شبی اگر بکشد درد آرزوی توام</p></div>
<div class="m2"><p>نسیم صبح دهد زندگی ببوی توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن از هوای لحد خاک تیره گشت و هنوز</p></div>
<div class="m2"><p>ز دل نمیرود ای جان هوای روی توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا چه زهره که لاف از غلامی تو زنم</p></div>
<div class="m2"><p>غلام حلقه بگوش سگان کوی توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن امید که روزی وصال دریابم</p></div>
<div class="m2"><p>گذشت عمر گرامی بجستجوی توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشان کشان ببهشتم برند و من نروم</p></div>
<div class="m2"><p>که دل نمیکشد ای دوست جز بسوی توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیث جنت و دوزخ کنند مردم لیک</p></div>
<div class="m2"><p>مرا از آن چه خبر چون بگفتگوی توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آرزوی تو عمرم گذشت همچو حسین</p></div>
<div class="m2"><p>هنوز واله و شیدا از آرزوی توام</p></div></div>