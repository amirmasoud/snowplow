---
title: >-
    شمارهٔ  ۲۴۷
---
# شمارهٔ  ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>تو که شاه ملک حسنی و سریر و جاه داری</p></div>
<div class="m2"><p>دل همچو من گدائی عجب ار نگاه داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز توام امید رحمت بکدام روی باشد</p></div>
<div class="m2"><p>که نه غم از آب دیده نه خبر ز آه داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز میان ماهرویان رسدت بحسن دعوی</p></div>
<div class="m2"><p>که چو آفتاب روشن ز دو رخ گواه داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مپسند در دل من همه خار حسرت ایگل</p></div>
<div class="m2"><p>تو مرا بهل در آنجا که نه جایگاه داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خلوت درون را چو بروی غیر بستم</p></div>
<div class="m2"><p>پس از آن چنانکه خواهی تو بیا که راه داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خبری ز پیر کنعان چه شود اگر بپرسی</p></div>
<div class="m2"><p>که تو یوسف زمانی کمر و کلاه داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بحدیث سر رندی حسین رو مگردان</p></div>
<div class="m2"><p>بکمال آشنائی که بسر شاه داری</p></div></div>