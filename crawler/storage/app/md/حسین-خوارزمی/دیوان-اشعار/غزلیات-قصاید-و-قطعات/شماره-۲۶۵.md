---
title: >-
    شمارهٔ  ۲۶۵
---
# شمارهٔ  ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>جان و جهان فدایت ای آنکه به ز جانی</p></div>
<div class="m2"><p>ذوقی است جان سپردن چون جان تو می ستانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردن بداغ دردت عیش است بی نهایت</p></div>
<div class="m2"><p>گشتن قتیل عشقت عمریست جاودانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حال مست این ره هشیار نیست آگه</p></div>
<div class="m2"><p>ساقی بیار جامی زان باده ای که دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گریه دو چشمم غماز حال من شد</p></div>
<div class="m2"><p>نشگفت اگر بماند راز دلم نهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی همدمان یکدل از زندگی چه حاصل</p></div>
<div class="m2"><p>ذوقی چنان ندارد بی دوست زندگانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دوست جوئی ای دل از خویش بی نشان شو</p></div>
<div class="m2"><p>تا زو نشان بیابی در عین بی نشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایمرغ سدره منزل بگشای بال و بر پر</p></div>
<div class="m2"><p>زین خارزار صورت در گلشن معانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یارب چه عیش باشد در گلشنی نشستن</p></div>
<div class="m2"><p>کایمن بود بهارش از آفت خزانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر تخت ملک سرمد دارد حسین مسند</p></div>
<div class="m2"><p>گر بگذرد چه نقصان زین خاکدان فانی</p></div></div>