---
title: >-
    شمارهٔ  ۱۲۶
---
# شمارهٔ  ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>سحر ز هاتف غیبم رسید مژده به گوش</p></div>
<div class="m2"><p>اگر تو طالب یاری به جان و دل بخروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگوی راز بهر کس چو دیک منمائی</p></div>
<div class="m2"><p>دهان بسته برآور چو خم صهبا جوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخواب دیده که از دوست گشته ای مهجور</p></div>
<div class="m2"><p>بمال چشم که چشم است مرترا روپوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش آندمی که ز خواب گران چو برخیزی</p></div>
<div class="m2"><p>نگار خویش تو بینی گرفته در آغوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیار ساقی گلرخ شراب دوشینه</p></div>
<div class="m2"><p>که باز مست بدوشم کشید چون شب دوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقله گشت مرا عقل ساقیا برخیز</p></div>
<div class="m2"><p>عقال عقل بدران بداروی بیهوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن شراب بیاور که روح قدسی را</p></div>
<div class="m2"><p>نسیم جرعه آن میکند چو من مدهوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا حریف خرابات عشق و زین ساقی</p></div>
<div class="m2"><p>بجز شراب خدائی خویشتن مفروش</p></div></div>