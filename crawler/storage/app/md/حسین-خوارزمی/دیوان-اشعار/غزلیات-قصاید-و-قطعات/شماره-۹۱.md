---
title: >-
    شمارهٔ  ۹۱
---
# شمارهٔ  ۹۱

<div class="b" id="bn1"><div class="m1"><p>دردا که دوست هیچ رعایت نمی‌کند</p></div>
<div class="m2"><p>مردیم از عتاب و عنایت نمی‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قربان تیر دشمن بدکیش گشته‌ام</p></div>
<div class="m2"><p>این جور بین که دوست حمایت نمی‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دست هجر دیده غمدیده آنچه دید</p></div>
<div class="m2"><p>جز با خیال دوست حکایت نمی‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانم ز دفتر غم جانان به نزد خلق</p></div>
<div class="m2"><p>فصلی و باب هیچ روایت نمی‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌یار در دیار دلم شحنه غمش</p></div>
<div class="m2"><p>کرد آنچه پادشاه ولایت نمی‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارم ز اشک و چهره بسی سیم و زر ولیک</p></div>
<div class="m2"><p>وجهی است اینکه کار کفایت نمی‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دست دشمن است همه ناله حسین</p></div>
<div class="m2"><p>ور نی ز جور دوست شکایت نمی‌کند</p></div></div>