---
title: >-
    شمارهٔ  ۲۳۳
---
# شمارهٔ  ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>اگر تو عاشق حسنی چرا وابسته جانی</p></div>
<div class="m2"><p>روان بگذر ز جان ای دل اگر جویای جانانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم سودای عاشق را چه شادیهاست اندر پی</p></div>
<div class="m2"><p>جراحتهای جانان را چه راحتهاست پنهانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر سلطانیت باید بیا درویش این در شو</p></div>
<div class="m2"><p>که سلطانی ست درویشی و درویشی ست سلطانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درخت آتشین عشقست اندر وادی ایمن</p></div>
<div class="m2"><p>اناالله بشنوی از وی اگر موسی عمرانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر آتش فرو گیرد همه آفاق عالم را</p></div>
<div class="m2"><p>سمندروار ای عاشق در آتش رو بآسانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلیل عشق جانانی مپرهیز از تف آتش</p></div>
<div class="m2"><p>که آتش با خلیل او کند رسم گلستانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حجاب او توئی ای دل برو از خویشتن بگسل</p></div>
<div class="m2"><p>که از سبحات وجه او رسد انوار سبحانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو میدانی که گنج شه بود در کنج ویرانها</p></div>
<div class="m2"><p>برای نقد عشق او رضا در ده بویرانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخلوت خانه وصلت مرا ره ده که در عشقت</p></div>
<div class="m2"><p>به جان آمد حسین ای جان در این وادی ز حیرانی</p></div></div>