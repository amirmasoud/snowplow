---
title: >-
    شمارهٔ  ۸۱
---
# شمارهٔ  ۸۱

<div class="b" id="bn1"><div class="m1"><p>بهار و عید می‌آید که عالم را بیاراید</p></div>
<div class="m2"><p>ولیکن بلبل دل را نسیم یار می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلی کز هجر گل روئی چو لاله داغها دارد</p></div>
<div class="m2"><p>شمیم وصل اگر نبود ز باغ و روضه نگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بی دوست جنت را بصد زینت بیارایند</p></div>
<div class="m2"><p>بجان دوست کاندر وی دل عاشق بیاساید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در و دیوار جنت را بآه دل بسوزانم</p></div>
<div class="m2"><p>اگر دلدار اهل دل در او دیدار ننماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نور جان هر مقبل صفائی دارد این منزل</p></div>
<div class="m2"><p>ولی بی وصل اهل دل دلم را خوش نمیآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمال طلعت جانان تواند دید مشتاقی</p></div>
<div class="m2"><p>که او آیینه دل را ز زنگ غیر بزداید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسین ار دوست جانت را بناز و عشوه میسوزد</p></div>
<div class="m2"><p>ترا باید رضا دادن بهر چه دوست فرماید</p></div></div>