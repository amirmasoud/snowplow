---
title: >-
    شمارهٔ  ۱۴۰
---
# شمارهٔ  ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>ما که در بادیه عشق تو سرگردانیم</p></div>
<div class="m2"><p>کعبه کوی ترا قبله دلها دانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم ما گر همه با ناوک محنت دوزند</p></div>
<div class="m2"><p>دیده بر دوختن از دیده تو نتوانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوی تو کعبه و دیدار تو عید اکبر</p></div>
<div class="m2"><p>کیش ما این که در آن عید ترا قربانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عوض کوی تو گر روضه رضوان بدهند</p></div>
<div class="m2"><p>هم بخاک سر کوی تو که ما نستانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغها بر جگر ماست چو لاله لیکن</p></div>
<div class="m2"><p>به نسیمی ز وصال تو گل خندانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما گدای در یاریم ولیکن چو حسین</p></div>
<div class="m2"><p>اندر اقلیم وفاداری او سلطانیم</p></div></div>