---
title: >-
    شمارهٔ  ۱۲۱
---
# شمارهٔ  ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>اگر تو محرم عشقی مگوی اسرارش</p></div>
<div class="m2"><p>چو جان خویش ز خلق جهان نگهدارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سر عشق خبر دار نیست هر عاشق</p></div>
<div class="m2"><p>حدیث عشق ز منصور پرس و از دارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو لاله تا ز غمش داغ بر جگر دارم</p></div>
<div class="m2"><p>فراغتی ست مرا از بهشت و گلزارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که جستجوی نمود و بکام دل نرسید</p></div>
<div class="m2"><p>بجو سعادت آن تا شوی طلبکارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو پر و بال چو پروانه پیش شمع بسوز</p></div>
<div class="m2"><p>که شد پدید جهان از فروغ انوارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز وصل یار گرامی اثر نمی یابد</p></div>
<div class="m2"><p>دلی که آتش سودا نسوخت آثارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز من برید دل خسته و بعشق آویخت</p></div>
<div class="m2"><p>ز دست عشق ندانم که چون شود کارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسین میل نکردی بروضه رضوان</p></div>
<div class="m2"><p>اگر بروضه نبودی امید دلدارش</p></div></div>