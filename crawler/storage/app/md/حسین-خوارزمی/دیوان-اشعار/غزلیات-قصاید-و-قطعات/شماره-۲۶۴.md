---
title: >-
    شمارهٔ  ۲۶۴
---
# شمارهٔ  ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>اگر بگوشه چشمی بسوی ما نگری</p></div>
<div class="m2"><p>ز جمع گوشه نشینان هزار دل ببری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر کسی که نمائی جمال خود هیهات</p></div>
<div class="m2"><p>دریغ جان من از حسن خویش بیخبری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنوش لعل لب خویش راحت روحی</p></div>
<div class="m2"><p>به نیش غمزه اگر چه جراحت جگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم که شاهی عالم بهیچ نشمارم</p></div>
<div class="m2"><p>اگر مرا تو کمینه غلام خود شمری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خاک من بمشامت رسد شمیم وفا</p></div>
<div class="m2"><p>پس از وفات اگر تو بتربتم گذری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من و توئیم یکی در مقام وحدت عشق</p></div>
<div class="m2"><p>بصورت ارچه منم دیگر و تو هم دگری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر مراد طلب میکنی حسین از دوست</p></div>
<div class="m2"><p>بآه نیم شبی ساز و گریه سحری</p></div></div>