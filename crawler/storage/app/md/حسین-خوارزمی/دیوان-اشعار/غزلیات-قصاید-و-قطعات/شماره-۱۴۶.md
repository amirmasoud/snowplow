---
title: >-
    شمارهٔ  ۱۴۶
---
# شمارهٔ  ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>ای گشته مست عشقت روز الست جانم</p></div>
<div class="m2"><p>مستی جان بماند روزی که من نمانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر ذره ای ز خاکم سرمست عشق باشد</p></div>
<div class="m2"><p>چون ذره ها برآید از خاک استخوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فکر بهشت و دوزخ دارند اهل دانش</p></div>
<div class="m2"><p>من مست عشق جانان فارغ ز این و آنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی بغیر منگر گر طالب حبیبی</p></div>
<div class="m2"><p>والله که در دو گیتی غیر از تو من ندانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از روی مهربانی ای مه بیا خرامان</p></div>
<div class="m2"><p>تا نقد جان و دل را در پای تو فشانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون هیچکس نشانی با خود نیافت از تو</p></div>
<div class="m2"><p>در جستن نشانت از خویش بی نشانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسرار عشق جانان دانم حسین لیکن</p></div>
<div class="m2"><p>چون محرمی ندارم گفتن نمیتوانم</p></div></div>