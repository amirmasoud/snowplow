---
title: >-
    شمارهٔ  ۷۳
---
# شمارهٔ  ۷۳

<div class="b" id="bn1"><div class="m1"><p>از من خبر به جانب جانان که می‌برد</p></div>
<div class="m2"><p>پیغام عندلیب به بستان که می‌برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعقوب را دو دیده ز بس گریه تیره گشت</p></div>
<div class="m2"><p>آخر خبر به یوسف کنعان که می‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون آدم از بهشت برون اوفتاده‌ام</p></div>
<div class="m2"><p>بازم به سوی روضهٔ رضوان که می‌برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌روی دوست مجلس ما را فروغ نیست</p></div>
<div class="m2"><p>پیغام ما بدان مه تابان که می‌برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حال ما خبر که تواند بدو رساند</p></div>
<div class="m2"><p>نام گدا به حضرت سلطان که می‌برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر زان که نامه‌ای بنویسم به خون دل</p></div>
<div class="m2"><p>آن را بدان مراد دل و جان که می‌برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهم که جان و دل بفرستم بدو حسین</p></div>
<div class="m2"><p>جان و دلم به جانب جانان که می‌برد</p></div></div>