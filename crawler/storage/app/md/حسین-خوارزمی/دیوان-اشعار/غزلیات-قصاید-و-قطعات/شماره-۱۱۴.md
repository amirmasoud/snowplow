---
title: >-
    شمارهٔ  ۱۱۴
---
# شمارهٔ  ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>سینه خلوتخانه یار است خالی کن ز غیر</p></div>
<div class="m2"><p>ره مده در کعبه بت را زانکه کعبه نیست دیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سلیمان با وجود سلطنت درویش باش</p></div>
<div class="m2"><p>تا ترا تلقین کند روح القدس اسرار طیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد و سوز عشق حاصل کن که بی این پر و بال</p></div>
<div class="m2"><p>طایر جانت نیارد کرد سوی دوست سیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشته جان را کشم وز هر مژه سوزن کنم</p></div>
<div class="m2"><p>دیده از غیرت بدوزم تا نبیند روی غیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهدان را روضه رضوان و ما را کوی دوست</p></div>
<div class="m2"><p>من رضا دادم حسین این صلح را والصلح خیر</p></div></div>