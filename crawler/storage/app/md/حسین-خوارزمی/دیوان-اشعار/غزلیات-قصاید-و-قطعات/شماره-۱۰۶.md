---
title: >-
    شمارهٔ  ۱۰۶
---
# شمارهٔ  ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>ز شست عشق چو تیر بلا روان کردند</p></div>
<div class="m2"><p>نخست جان من خسته را نشان کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که زد قدمی در ره وفاداری</p></div>
<div class="m2"><p>بهر جفا و بهر جورش امتحان کردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مس وجود دهی کیمیای عشق بری</p></div>
<div class="m2"><p>بیا بگو که در این ره که رازیان کردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار جان گرامی بیک نفس دادند</p></div>
<div class="m2"><p>اگر چه دل بربودند و قصد جان کردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسر نکته اوحی الیه ما یو حی</p></div>
<div class="m2"><p>رموز عاشق و معشوق را بیان کردند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برای جلوه حسن و جمال خویش حبیب</p></div>
<div class="m2"><p>چو ساخت آینه ای نام او جهان کردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمال دوست بر عارفان بود پیدا</p></div>
<div class="m2"><p>اگر چه در نظر غافلان نهان کردند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا به بندگی از هر دو کون داده خلاص</p></div>
<div class="m2"><p>ترا فریفته بند این و آن کردند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلیل عشق مگر در دل حسین آمد</p></div>
<div class="m2"><p>کز آتش دل او باغ و گلستان کردند</p></div></div>