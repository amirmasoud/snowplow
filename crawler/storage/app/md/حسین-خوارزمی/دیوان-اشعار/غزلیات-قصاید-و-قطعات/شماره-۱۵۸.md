---
title: >-
    شمارهٔ  ۱۵۸
---
# شمارهٔ  ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>گرد زمین و آسمان من سال‌ها گردیده‌ام</p></div>
<div class="m2"><p>روشن مبادا چشم من چون تو مهی گر دیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دل به عشقت بسته‌ام از قید هستی رسته‌ام</p></div>
<div class="m2"><p>چون با غمت پیوسته‌ام از خویشتن ببریده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خارزار آب و گل چون غنچه گشتم تنگدل</p></div>
<div class="m2"><p>در گلشن روحانیان منزل از آن بگزیده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکس به بازار جهان سودای سودی می‌کند</p></div>
<div class="m2"><p>من سودها بفروخته سودای تو بخریده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا جان به گلزار رضا شد عندلیب جان‌فزا</p></div>
<div class="m2"><p>از قربت خار بلا ریحان راحت چیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکس علاج درد خود جوید پی آرام جان</p></div>
<div class="m2"><p>لیکن من آشفته‌دل با دردت آرامیده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون راه علم و عقل را دیدم که پیچاپیچ بود</p></div>
<div class="m2"><p>ای یار من یکبارگی در عاشقی پیچیده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاقل به ملک عافیت پیوسته گو تنها نشین</p></div>
<div class="m2"><p>کز عشق آن بالا بلا از عافیت ببریده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا چون حسین از اهل دل یابم صفای خاطری</p></div>
<div class="m2"><p>عمری به خاک بندگی روی وفا مالیده‌ام</p></div></div>