---
title: >-
    شمارهٔ  ۱۴۷
---
# شمارهٔ  ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>رضا دادم بعشق او اگر غارت کند جانم</p></div>
<div class="m2"><p>که جان صد چو من بادا فدای عشق جانانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزخم عشق او سازم که زخمش مرهم جانست</p></div>
<div class="m2"><p>بداغ درد او سازم که درد اوست درمانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمی کز عشق یار آید بشادی بر سرش گیرم</p></div>
<div class="m2"><p>بهر چه دوست فرماید غلام بنده فرمانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا همت چو طور آمد ارادت وادی اقدس</p></div>
<div class="m2"><p>درخت آتشین عشقست و من موسی بن عمرانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا همت چنان آمد که گر از تشنگی میرم</p></div>
<div class="m2"><p>بمنت آب حیوان را ز دست خضر نستانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا گویند کآرام دل از دیدار دیگر جو</p></div>
<div class="m2"><p>معاذالله که در عالم دلارامی دگر دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز روی پاکبازانش هنوزم خجلتی باشد</p></div>
<div class="m2"><p>بگاه جلوه حسنش اگر صد جان برافشانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا چون نیست کس محرم ز عشقش چون برآرم دم</p></div>
<div class="m2"><p>چو دیوانه نمی یابم چرا زنجیر جنبانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسین از گفتگو بگذر مگو با کس حدیث او</p></div>
<div class="m2"><p>که تا اسرار پنهانی بگوش تو فرو خوانم</p></div></div>