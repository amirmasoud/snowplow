---
title: >-
    شمارهٔ  ۱۵۰
---
# شمارهٔ  ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>دو دیده بر سر راه امید می‌دارم</p></div>
<div class="m2"><p>که کی بود که رسد قاصدی ز دلدارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کراست زهره که آرد ز یار من خبری</p></div>
<div class="m2"><p>و یا ز من ببرد خدمتی سوی یارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چگونه نامه نویسم بخدمت تو که من</p></div>
<div class="m2"><p>ز بیم مدعیان دم زدن نمی آرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خون دیده شود روی زرد من گلگون</p></div>
<div class="m2"><p>شب فراق چو از روز وصل یاد آرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر کشند مرا دشمنان بجور و جفا</p></div>
<div class="m2"><p>من آن نیم که دل از مهر دوست بردارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کردم ای صنم بیوفا چه دیدستی</p></div>
<div class="m2"><p>ز من که رفتی و ماندی به زاری زارم</p></div></div>