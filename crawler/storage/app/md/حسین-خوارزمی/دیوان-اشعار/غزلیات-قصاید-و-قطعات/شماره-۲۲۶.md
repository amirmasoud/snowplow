---
title: >-
    شمارهٔ  ۲۲۶
---
# شمارهٔ  ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>دلا تا کی ز نادانی همه نقش جهان بینی</p></div>
<div class="m2"><p>صفا ده دیده خود را که تا دیدار جان بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو در بند صور باشی همه خاک آیدت گیتی</p></div>
<div class="m2"><p>چو از صورت برون آئی جهان پر گلستان بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عشق پرده‌سوز ای دل به عالم آتشی افکن</p></div>
<div class="m2"><p>که تا در زیر هر پرده جمال دلستان بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از این و آن حجاب آمد ترا در راه عشق ای دل</p></div>
<div class="m2"><p>چو در دلدار پیوندی نه این بینی نه آن بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز کثرت جان خرم را غم و اندوه میزاید</p></div>
<div class="m2"><p>بوحدت آی تا خود را همیشه شادمان بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن از دیدار جان مانع شود چشم جهان بین را</p></div>
<div class="m2"><p>حجاب تن چو برداری جمال جان عیان بینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کف تیره حجاب آمد ز آب صافی ای صوفی</p></div>
<div class="m2"><p>هلا بشکاف این کف را که تا آب روان بینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صدف تا نشکنی گوهر نیاید در نظر پیدا</p></div>
<div class="m2"><p>چو بشکستی صدف در وی بسی گوهر نهان بینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سحاب تیره چون آمد ز مهر و مه شود حایل</p></div>
<div class="m2"><p>چو ابر از پیش برخیزد تو مهر و مه عیان بینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مجرد شو ز خویش آنگه در این دریا قدم درنه</p></div>
<div class="m2"><p>که چون با خویشتن آئی نهنگ جان ستان بینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر با خویشتن عمری بسر در راه او پوئی</p></div>
<div class="m2"><p>نه از مقصد نشان یابی نه این ره را کران بینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز خاک درگه مردی بچشم دل بکش گردی</p></div>
<div class="m2"><p>پس آنگه در جهان بنگر که تا جان جهان بینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز فیض رحمت ایزد طراز آستین یابی</p></div>
<div class="m2"><p>اگر در چشم دل زان در غبار آستان بینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بجیب همت ارزانی بدارالملک ربانی</p></div>
<div class="m2"><p>ز سوی حضرت قدسی جنیبتها روان بینی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پی معراج روحانی برآ زین فرش ظلمانی</p></div>
<div class="m2"><p>که تا بر عرش رحمانی ز جذبه نردبان بینی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>براق برق جنبش را چو در میدان برانگیزی</p></div>
<div class="m2"><p>کمینه جای جولانش ز اوج آسمان بینی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در آن میدان چو قلاشان سبکره کی توانی شد</p></div>
<div class="m2"><p>ز جوش غفلت از دوشت چو گوش دل گران بینی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر دست غم عشقش عنان همتت گیرد</p></div>
<div class="m2"><p>ملک اندر رکاب آید فلک را همعنان بینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نقوش نفس شهوانی چو از خاطر برون رانی</p></div>
<div class="m2"><p>رموز سر غیبی را ز خاطر ترجمان بینی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سمند همت ار یابی بهل آرایش حکمت</p></div>
<div class="m2"><p>چه حاجت مرکب جم را که تا بر گستوان بینی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز شیطان از چه پرهیزی چو با رحمان بود کارت</p></div>
<div class="m2"><p>ز رهزن از چه اندیشی چو حق را پاسبان بینی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز گفتار و زبان دانی چو در حیرت فرومانی</p></div>
<div class="m2"><p>بگاه کشف اسرارش همه تن را زبان بینی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر از تن برون آئی درآئی در حریم جان</p></div>
<div class="m2"><p>وگر از خود فنا گردی بقای جاودان بینی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر ای طایر قدسی ز حبس تن برون آئی</p></div>
<div class="m2"><p>ز شاخ سدره طوبی نخستین آشیان بینی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بده جان و غمش بستان از ایرا اندرین سودا</p></div>
<div class="m2"><p>نه در دنیا پشیمانی نه در عقبی زیان بینی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خلیل آسا ز عشق او درآ در آتش سوزان</p></div>
<div class="m2"><p>که در هر گوشه آتش هزاران بوستان بینی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حذر کم کن اگر آتش بود پر اخگر و شعله</p></div>
<div class="m2"><p>کز اخگر لاله ها یابی ز شعله ارغوان بینی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو از خود ناشده فانی نیابی وصلت باقی</p></div>
<div class="m2"><p>کنار دوست چون یابی که خود را در میان بینی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خیانت چیست میدانی در اینره خویشتن دیدن</p></div>
<div class="m2"><p>ز خود بگذر در او بنگر امین شو تا امان بینی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر چون روح ربانی خدا خواهی شرف یابی</p></div>
<div class="m2"><p>وگر چون نفس شهوانی هوا جوئی هوان بینی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز غیر او ستان دل را چو او را دلستان دانی</p></div>
<div class="m2"><p>ز عیب آخر تبرا کن چو او را غیب دان بینی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز دست دل مده دردش اگر درمان همیخواهی</p></div>
<div class="m2"><p>مشو دور از بر عیسی چو خود را ناتوان بینی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مشو مغرور این عالم که چون بر هم نهی دیده</p></div>
<div class="m2"><p>نه تاج خسروان یابی نه طغرای طغان بینی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گه از حسن و جمال او نهاد تو شود فرخ</p></div>
<div class="m2"><p>گه از نقش خیال او بهار اندر خزان بینی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه آن فرخ نهار است آنکه باشد ظلمت شامش</p></div>
<div class="m2"><p>نه آن خرم بهار است این که آنرا مهرگان بینی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز ویرانی مترس ای جان که چون دل گشت ویرانه</p></div>
<div class="m2"><p>غمش در کنج این ویران چو گنج شایگان بینی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز کبر و از ریا بگذر بکوی کبریا تا تو</p></div>
<div class="m2"><p>ز قبض و رحمت ایزد ردا و طیلسان بینی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جهان شو از جهان زیرا جهان دیر مغان آمد</p></div>
<div class="m2"><p>که در وی اختر و گردون هم آتش هم دخان بینی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بخاک فرش ظلمانی میالا دامن همت</p></div>
<div class="m2"><p>که تا عرش جهان بانی و رای لامکان بینی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو دل از درد خرم شد دل از دلدار برتابی</p></div>
<div class="m2"><p>چو قلب از عشق صافی شد جهان اندر جنان بینی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حسین از دامن مردی بچشم جان بکش گردی</p></div>
<div class="m2"><p>که با این چشم نورانی نشان بی نشان بینی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو گرد آلوده موئی را زمین بوسی کنی یکدم</p></div>
<div class="m2"><p>ز یمن همتش خود را خداوند زمان بینی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برافشان دست از دستان بیا با دوستان بنشین</p></div>
<div class="m2"><p>که تا ز اسرار روحانی هزاران داستان بینی</p></div></div>