---
title: >-
    شمارهٔ  ۱۲
---
# شمارهٔ  ۱۲

<div class="b" id="bn1"><div class="m1"><p>دلا اگر نفسی می‌زنی به صدق و صفا</p></div>
<div class="m2"><p>به جان بکوش که باشی غلام اهل وفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر قبیله چه گردی اگر تو مجنونی</p></div>
<div class="m2"><p>بیا و قبله گزین از قبیله لیلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو تشنه لب به بیابان هلاک خواهی شد</p></div>
<div class="m2"><p>غنیمتی شمر ای دوست صحبت دریا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر تو لذت ناز حبیب میدانی</p></div>
<div class="m2"><p>شفا ز رنج بجوی و ز درد خواه دوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر ستم رسد از دوست هم بدوست گریز</p></div>
<div class="m2"><p>کجا رود بجهان وامق از در عذرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجوی جانب جانان ز عقل راهبری</p></div>
<div class="m2"><p>که عشق دوست بود سوی دوست راهنما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میان شب بچراغ آفتاب نتوان جست</p></div>
<div class="m2"><p>که آفتاب هم از نور خود شود پیدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بپای مورچه نتوان بکوه قاف رسید</p></div>
<div class="m2"><p>برو تو تعبیه کن خویش در پر عنقا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طریق عقل رها کن بعشق ساز حسین</p></div>
<div class="m2"><p>اگر تو عاشق عشقی و عشق را جویا</p></div></div>