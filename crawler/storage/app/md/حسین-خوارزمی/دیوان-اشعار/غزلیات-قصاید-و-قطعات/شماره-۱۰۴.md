---
title: >-
    شمارهٔ  ۱۰۴
---
# شمارهٔ  ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>بر آستان خرابات عشق مستانند</p></div>
<div class="m2"><p>که نقد هر دو جهان را بهیچ نستانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>براق همت عالی بتازیانه شوق</p></div>
<div class="m2"><p>در آن فضا که بجز دوست نیست میرانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هر چه هست بکلی دو دیده بردوزند</p></div>
<div class="m2"><p>ولی ز روی دلارام خویش نتوانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر حرام شناسند جز بروی حبیب</p></div>
<div class="m2"><p>بغیر دوست خود اندر جهان نمیدانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گدای کوی نیازند و خاک راه ولیک</p></div>
<div class="m2"><p>فراز مسند اقلیم عشق سلطانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهان بی حشم و مفلسان محتشمند</p></div>
<div class="m2"><p>از این طوایف رسمی بکس نمیمانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتاده بی سر و پایند بر در دلدار</p></div>
<div class="m2"><p>ولی بگاه روش سروران میدانند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو لاله گرچه بسی داغ بر جگر دارند</p></div>
<div class="m2"><p>ز شوق چون گل سوری همیشه خندانند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برای آنکه ز غیرت بغیر دل ندهند</p></div>
<div class="m2"><p>بر آستانه دل چون حسین دربانند</p></div></div>