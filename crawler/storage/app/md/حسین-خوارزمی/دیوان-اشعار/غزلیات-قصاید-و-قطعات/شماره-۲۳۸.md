---
title: >-
    شمارهٔ  ۲۳۸
---
# شمارهٔ  ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>رخ خویش اگر نمائی دل عالمی ربائی</p></div>
<div class="m2"><p>دو جهان بهم برآید ز نقاب اگر برآئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مشارق هویت چو بتابد آفتابت</p></div>
<div class="m2"><p>ز ظلال اثر نماند ز کمال روشنائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هله ای شه مجرد بنمای طلعت خود</p></div>
<div class="m2"><p>نه توئی بهل نه اوئی نه منی بمان نه مائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم خویش با که گویم بکدام راه پویم</p></div>
<div class="m2"><p>خبر تو از که جویم تو که در صفت نیائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجمال لایزالت بکمال بیزوالت</p></div>
<div class="m2"><p>بگداز هستی ما که نماند این جدائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل و دین چو میربائی ز پس هزار پرده</p></div>
<div class="m2"><p>چه قیامتی که باشد چو نقاب برگشائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بنزد تست روشن که بحسن بی نظیری</p></div>
<div class="m2"><p>عجب ار جمال خود را بکسی دگر نمائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو خلیل عشق اوئی مگریز ز آتش دل</p></div>
<div class="m2"><p>که گل و سمن بروید چو بآتش اندرآئی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو سبوی پر ز آبی بکنار بحر وحدت</p></div>
<div class="m2"><p>اگرت سبو شکسته تو شکسته دل چرائی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز لباس هستی خود چو حسین شو مجرد</p></div>
<div class="m2"><p>پس از آن درآ بدریا که تو مرد آشنائی</p></div></div>