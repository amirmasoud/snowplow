---
title: >-
    شمارهٔ  ۲۱۳
---
# شمارهٔ  ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>دست بختم کوته است از دامن وصلت که هست</p></div>
<div class="m2"><p>پایه من سست قدرت بخت والا آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوهر طبعم نثار خاک پایت کی سزد</p></div>
<div class="m2"><p>گرچه از روی شرف لؤلؤی لالا آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظم من در خورد جاهت کی بود با آنکه هست</p></div>
<div class="m2"><p>در شعرم خوشتر از دری شعرا آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ز آب مرحمت شسته لباس دین ما</p></div>
<div class="m2"><p>تا ز چرک شرک صافی و مصفا آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنج ویران جای گنج آمد از آن مهر ترا</p></div>
<div class="m2"><p>در دل ویران من پیوسته مأوی آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پای مردیهای لطفت میرساند دمبدم</p></div>
<div class="m2"><p>آنچه از درگاه حق ما را تمنا آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم ز لطف خویشتن درمان درد ما بکن</p></div>
<div class="m2"><p>ای ز لطفت درد جانها را مداوا آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای با دل شکسته ترا کار آمده</p></div>
<div class="m2"><p>درد تو مرهم دل افکار آمده</p></div></div>