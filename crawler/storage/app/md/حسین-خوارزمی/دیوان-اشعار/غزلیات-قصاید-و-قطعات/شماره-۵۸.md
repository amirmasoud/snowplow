---
title: >-
    شمارهٔ  ۵۸
---
# شمارهٔ  ۵۸

<div class="b" id="bn1"><div class="m1"><p>ای راحت جان از نفس روح فزایت</p></div>
<div class="m2"><p>دارد نگه از چشم بداندیش خدایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درمان طلبان از تو دوا جسته ولیکن</p></div>
<div class="m2"><p>من سوخته دل ساخته با درد و بلایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون هست وفا شیوه عشاق بلاکش</p></div>
<div class="m2"><p>جانا چکنم گر نکشم بار جفایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کس طلبیده ز تو کامی و مرادی</p></div>
<div class="m2"><p>کام دل سودا زده ماست رضایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر لحظه تو یار دگری گر چه گزیدی</p></div>
<div class="m2"><p>ما هیچکسی را نگزیدیم بجایت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر در قدمت باختمی وقت قدومت</p></div>
<div class="m2"><p>گر زانکه سری داشتمی لایق پایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتار حسین ای صنم شوخ خوش آید</p></div>
<div class="m2"><p>از پسته شکر شکن نغمه سرایت</p></div></div>