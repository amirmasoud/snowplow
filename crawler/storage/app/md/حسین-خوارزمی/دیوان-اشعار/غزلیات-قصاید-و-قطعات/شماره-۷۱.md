---
title: >-
    شمارهٔ  ۷۱
---
# شمارهٔ  ۷۱

<div class="b" id="bn1"><div class="m1"><p>بی نشان گردم اگر از تو نشانی نرسد</p></div>
<div class="m2"><p>مرده باشم اگرم مژده جانی نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه از تلخی آن حال کز آن شیرین لب</p></div>
<div class="m2"><p>بهر دلجوئی من شهد بیانی نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وای از آن تیره زمانی که ز خورشید رخت</p></div>
<div class="m2"><p>از پی روشنی جان لمعانی نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل مجروح مرا نیست امید مرهم</p></div>
<div class="m2"><p>اگر از غمزه تو زخم سنانی نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صید شاهین غمت تا نشود طایر جان</p></div>
<div class="m2"><p>در هوای جبروتش طیرانی نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رایض عشق چو بر دلدل دل زین ننهند</p></div>
<div class="m2"><p>در فضای جبروتش جولانی نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا تو پیدا نکنی ذوق مقالات حسین</p></div>
<div class="m2"><p>هیچ در گوش دلت راز نهانی نرسد</p></div></div>