---
title: >-
    شمارهٔ  ۹۹
---
# شمارهٔ  ۹۹

<div class="b" id="bn1"><div class="m1"><p>دل همیشه تکیه بر فضل الهی می‌کند</p></div>
<div class="m2"><p>جان گدای او شده است و پادشاهی می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه از مستی جام عشق ملک جم نخواست</p></div>
<div class="m2"><p>سلطنت از اوج مه تا پشت ماهی می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غره شاهی مشو درویش این درگاه باش</p></div>
<div class="m2"><p>در حقیقت هرکه درویش است شاهی می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای شده مغرور ملک نیمروز آگاه شو</p></div>
<div class="m2"><p>زان اثرهایی که آه صبحگاهی می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما خجالت‌ها بسی داریم لیکن آن کریم</p></div>
<div class="m2"><p>از کمال لطف هردم عذرخواهی می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه اندیشد ز خجلت‌های هنگام شمار</p></div>
<div class="m2"><p>من عجب دارم که چون میل مناهی می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موسپید و دل‌سیه گشت از گنه زان رو حسین</p></div>
<div class="m2"><p>آب دیده لعل‌گون و چهره کاهی می‌کند</p></div></div>