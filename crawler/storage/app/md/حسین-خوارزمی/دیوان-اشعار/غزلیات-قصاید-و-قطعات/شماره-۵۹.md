---
title: >-
    شمارهٔ  ۵۹
---
# شمارهٔ  ۵۹

<div class="b" id="bn1"><div class="m1"><p>کدام دل که گرفتار و مبتلای تو نیست</p></div>
<div class="m2"><p>کدام سر که سراسیمه هوای تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کدام طایر قدسی نشد گرفتارت</p></div>
<div class="m2"><p>کدام جان گرامی که آن فدای تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کدام سینه نشد آستان درد و غمت</p></div>
<div class="m2"><p>کدام دل هدف ناوک بلای تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا ز گوش چو سود ار حدیث تو نبود</p></div>
<div class="m2"><p>مرا ز دیده چه حاصل اگر لقای تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا بمنظره چشم روشنم بنشین</p></div>
<div class="m2"><p>که خانه دل تاریک بنده جای تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا چو دید دلم شد ز خویش بیگانه</p></div>
<div class="m2"><p>بخویش بسته بود هر که آشنای تو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گریختن ز جفا شرط عاشقی نبود</p></div>
<div class="m2"><p>جفای تو بر عاشق کم از وفای تو نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بکس مراد میندیش کام خویش برآر</p></div>
<div class="m2"><p>که کام این دل شوریده جز رضای تو نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهان خویش به مشک و گلاب شست حسین</p></div>
<div class="m2"><p>هنوز گفته او لایق ثنای تو نیست</p></div></div>