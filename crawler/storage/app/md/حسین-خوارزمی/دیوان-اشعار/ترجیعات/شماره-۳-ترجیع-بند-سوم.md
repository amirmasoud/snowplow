---
title: >-
    شمارهٔ  ۳ - ترجیع بند سوم
---
# شمارهٔ  ۳ - ترجیع بند سوم

<div class="b" id="bn1"><div class="m1"><p>کس نشد آگه از بدایت عشق</p></div>
<div class="m2"><p>نیست جز نیستی نهایت عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق را پایدار یکپای است</p></div>
<div class="m2"><p>خود تو بین تا کجاست غایت عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه چیز آیت نشان دارد</p></div>
<div class="m2"><p>بی نشان گشتن است غایت عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی از قال و قیل اهل مقال</p></div>
<div class="m2"><p>بشنو از عاشقان حکایت عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشگ من لعل کرد و رویم زرد</p></div>
<div class="m2"><p>هست از این وجه ها کفایت عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخدا هیچ طالبی بخدا</p></div>
<div class="m2"><p>ره نبرده ست بی هدایت عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دفتر درد عشق را کافی ست</p></div>
<div class="m2"><p>در هدایه مجو روایت عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شدن کار عالمی بنظام</p></div>
<div class="m2"><p>هست موقوف یک عنایت عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر زمانی بگوش جان حسین</p></div>
<div class="m2"><p>این خطاب آید از ولایت عشق</p></div></div>
<div class="b2" id="bn10"><p>که مراد از همه جهان عشق است</p>
<p>جمله عالم تن است و جان عشق است</p></div>
<div class="b" id="bn11"><div class="m1"><p>ای رخت آفتاب روشن دل</p></div>
<div class="m2"><p>غم تو طایر نشیمن دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس قبای بقا که چاک زده ست</p></div>
<div class="m2"><p>دست عشقت گرفته دامن دل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوخت از آه جان سوختگان</p></div>
<div class="m2"><p>آتشی در زده بخرمن دل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رام گشته بتازیانه شوق</p></div>
<div class="m2"><p>دلدل تیز گام توسن دل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل بدام بلا ز دیده فتاد</p></div>
<div class="m2"><p>من مسکین ز شیوه فن دل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آه از این دل که اوست دشمن من</p></div>
<div class="m2"><p>وای از این دیده کوست دشمن دل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غم تو خون دل ز دیده نخواست</p></div>
<div class="m2"><p>ماند خونم بتا بگردن دل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هدف ناوکی است جان حسین</p></div>
<div class="m2"><p>که گذر میکند ز جوشن دل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر دم از بلبلان نغمه سرای</p></div>
<div class="m2"><p>غلغلی میفتد بگلشن دل</p></div></div>
<div class="b2" id="bn20"><p>که مراد از همه جهان عشق است</p>
<p>جمله عالم تن است و جان عشق است</p></div>
<div class="b" id="bn21"><div class="m1"><p>تا که عشق نهان نشد پیدا</p></div>
<div class="m2"><p>اثری از جهان نشد پیدا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا دل از سوز نار عشق نسوخت</p></div>
<div class="m2"><p>پرتو نور جان نشد پیدا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عشق تا جان ما نشانه نکرد</p></div>
<div class="m2"><p>خبر از بی نشان نشد پیدا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کنت کنزا بیان این نکته است</p></div>
<div class="m2"><p>آه کین نکته دان نشد پیدا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عشق تا جلوه بدیع نکرد</p></div>
<div class="m2"><p>زین معانی بیان نشد پیدا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دوستان بشنوید نکته عشق</p></div>
<div class="m2"><p>که چنین داستان نشد پیدا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هیچ عاشق کنار دوست نیافت</p></div>
<div class="m2"><p>عشق تا در میان نشد پیدا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا جهان است فتنه ای چون عشق</p></div>
<div class="m2"><p>در زمین و زمان نشد پیدا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا حسین از حدیث عشق نگفت</p></div>
<div class="m2"><p>در بر این و آن نشد پیدا</p></div></div>
<div class="b2" id="bn30"><p>که مراد از همه جهان عشق است</p>
<p>جمله عالم تن است و جان عشقست</p></div>
<div class="b" id="bn31"><div class="m1"><p>آه کاندر زمانه محرم نیست</p></div>
<div class="m2"><p>دم نیارم زدن که همدم نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو بتو صد جراحت جان هست</p></div>
<div class="m2"><p>که یکی را امید مرهم نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خلفی صدق از خلیفه حق</p></div>
<div class="m2"><p>در خلافت سرای آدم نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شادئی میکنم بدولت عشق</p></div>
<div class="m2"><p>که گرم هیچ نیست غم هم نیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من چو بیگانه ام ز خویش مرا</p></div>
<div class="m2"><p>سر خویشی هر دو عالم نیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>صرف کردم بعشق مس وجود</p></div>
<div class="m2"><p>که محبت ز کیمیا کم نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نازنینا حسین را دریاب</p></div>
<div class="m2"><p>که بنای حیات محکم نیست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بفراقم مکش که در قدمت</p></div>
<div class="m2"><p>گر بمیرم ز مردنم غم نیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دل من خاتم سلیمان است</p></div>
<div class="m2"><p>که جز این نکته نقش خاتم نیست</p></div></div>
<div class="b2" id="bn40"><p>که مراد از همه جهان عشق است</p>
<p>جمله عالم تن است و جان عشق است</p></div>
<div class="b" id="bn41"><div class="m1"><p>اگر از عشق پیشوا یابی</p></div>
<div class="m2"><p>ره بدرگاه کبریا یابی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در ره عشق اگر گدا گردی</p></div>
<div class="m2"><p>دولت قرب پادشا یابی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر کنی چاک خرقه هستی</p></div>
<div class="m2"><p>از بقای ابد قبا یابی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>این سعادت بجستجو یابند</p></div>
<div class="m2"><p>جان من پس بجوی تا یابی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آستین بر جهان گر افشانی</p></div>
<div class="m2"><p>بر سر عرش استوا یابی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>این مقام نیازمندانست</p></div>
<div class="m2"><p>نازنینا تو این کجا یابی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>درد نادیده کی دوا بینی</p></div>
<div class="m2"><p>رنج نابرده چون شفا یابی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کارت از خلق گشت بر تو دراز</p></div>
<div class="m2"><p>بگذر از خلق تا خدا یابی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گوشه ای گیر و گوش دار حسین</p></div>
<div class="m2"><p>تا ز هر گوشه این ندا یابی</p></div></div>
<div class="b2" id="bn50"><p>که مراد از همه جهان عشق است</p>
<p>همه عالم تن است و جان عشق است</p></div>
<div class="b" id="bn51"><div class="m1"><p>عشقبازی طریق بازی نیست</p></div>
<div class="m2"><p>بجز از سوز و جان گدازی نیست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خرقه کانرا بخون نمی شویند</p></div>
<div class="m2"><p>در ره عاشقی نمازی نیست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هر کرا عاقبت نشد محمود</p></div>
<div class="m2"><p>هرگز او قابل ایازی نیست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>باری اندر حریم خلوت ناز</p></div>
<div class="m2"><p>بار هر مروزی و رازی نیست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بنده عشق شو کز این بهتر</p></div>
<div class="m2"><p>پادشاهی و سرفرازی نیست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تو بدو دل نداده ای ورنه</p></div>
<div class="m2"><p>کار او غیر دلنوازی نیست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کشته عشق گشته ام آری</p></div>
<div class="m2"><p>چون من و او شهید و غازی نیست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چون حسین ار فنای عشق شوی</p></div>
<div class="m2"><p>بعد از این این سخن مجازی نیست</p></div></div>
<div class="b2" id="bn59"><p>که مراد از همه جهان عشق است</p>
<p>جمله عالم تن است و جان عشق است</p></div>
<div class="b" id="bn60"><div class="m1"><p>گرچه ای عشق رهنمای منی</p></div>
<div class="m2"><p>دشمن جان مبتلای منی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از تو یابم دوای هر دردی</p></div>
<div class="m2"><p>گر چه تو درد بی دوای منی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اثری از دلم نشد پیدا</p></div>
<div class="m2"><p>تا تو ای عشق دلربای منی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر بصد عشوه خون من ریزی</p></div>
<div class="m2"><p>راضیم زانکه خونبهای منی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>از تو جاوید زنده خواهم بود</p></div>
<div class="m2"><p>که تو جانان و جانفزای منی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گشته ام من ز خویش بیگانه</p></div>
<div class="m2"><p>زان نفس باز کاشنای منی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>پادشاه جهان شوم چو حسین</p></div>
<div class="m2"><p>گر بگوئی که تو گدای منی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>در بیان صفات خویش ای عشق</p></div>
<div class="m2"><p>هم تو برگو که تو بجای منی</p></div></div>
<div class="b2" id="bn68"><p>که مراد از همه جهان عشق است</p>
<p>جمله عالم تن است و جان عشق است</p></div>
<div class="b" id="bn69"><div class="m1"><p>هر چه میگویم ای نگار امروز</p></div>
<div class="m2"><p>نه بخویشم فرو گذار امروز</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>شهریاری مرا ربود از من</p></div>
<div class="m2"><p>که ندارد بشهریار امروز</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>توتیائی برد ز خاک رهش</p></div>
<div class="m2"><p>دیده دیده انتظار امروز</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>سوخت اغیار ز آتش غیرت</p></div>
<div class="m2"><p>که تجلی نمود یار امروز</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دل شوریده هر چه میطلبید</p></div>
<div class="m2"><p>دارد آن جمله در کنار امروز</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همچو منصور پای دار مرا</p></div>
<div class="m2"><p>هست اقبال پایدار امروز</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>در خرابات عشق هست حسین</p></div>
<div class="m2"><p>مست آن چشم پر خمار امروز</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>وه که خواهد شدن ز بیخویشی</p></div>
<div class="m2"><p>سر این نکته آشکار امروز</p></div></div>
<div class="b2" id="bn77"><p>که مراد از همه جهان عشق است</p>
<p>جمله عالم تن است و جان عشق است</p></div>
<div class="b" id="bn78"><div class="m1"><p>در خرابات عشق بیدل و مست</p></div>
<div class="m2"><p>میروم روز و شب سبو در دست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گرو عشق کرده جامه جان</p></div>
<div class="m2"><p>از پی جرعه ای ز جام الست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>گشته از درد درد مست و خراب</p></div>
<div class="m2"><p>با خراباتیان باده پرست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>از سر هر چه بود دل برخاست</p></div>
<div class="m2"><p>تا شود خاکپای اهل نشست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>محرم بزم اهل درد نشد</p></div>
<div class="m2"><p>تا دل از بند ننگ و نام نرست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>مست ناگشته کس نشد هشیار</p></div>
<div class="m2"><p>نیست نابوده کی توان شد هست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>عشق در ملک دل چو سلطان شد</p></div>
<div class="m2"><p>شحنه عقل از میانه بجست</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>پیش هر کس درست گشت این قول</p></div>
<div class="m2"><p>که حسین شکسته توبه شکست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چون گشادم سر جریده عشق</p></div>
<div class="m2"><p>در دلم نقش این حدیث به بست</p></div></div>
<div class="b2" id="bn87"><p>که مراد از همه جهان عشق است</p>
<p>جمله عالم تن است و جان عشقست</p></div>