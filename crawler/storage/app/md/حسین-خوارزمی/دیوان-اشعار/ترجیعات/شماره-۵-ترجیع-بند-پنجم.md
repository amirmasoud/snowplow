---
title: >-
    شمارهٔ  ۵ - ترجیع بند پنجم
---
# شمارهٔ  ۵ - ترجیع بند پنجم

<div class="b" id="bn1"><div class="m1"><p>الا ای گوهر بحر مصفا</p></div>
<div class="m2"><p>که در عالم توئی پنهان و پیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وجودت بهر اظهار کمالات</p></div>
<div class="m2"><p>چو از غیب هویت شد هویدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای جلوه عشق جهانسوز</p></div>
<div class="m2"><p>بسی آئینه ها کردی ز اشیاء</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هر آئینه دیداری نمودی</p></div>
<div class="m2"><p>بهر چشمی در او کردی تماشا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان آسوده در کتم عدم بود</p></div>
<div class="m2"><p>برآوردی ز عالم شور و غوغا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهی با جان مجنون عشق بازی</p></div>
<div class="m2"><p>گهی دلها بری با حسن لیلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو هم عشقی و معشوقی و عاشق</p></div>
<div class="m2"><p>تو هم دردی و هم اصل مداوا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توئی پیرایه معشوق دلبر</p></div>
<div class="m2"><p>توئی سرمایه عشاق شیدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیاز وامق بیچاره از تست</p></div>
<div class="m2"><p>هم از تو عشوه ها و ناز عذرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بچشم عارفانت می نماید</p></div>
<div class="m2"><p>جهان جمله تن و تو جان تنها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ولیکن عاشقان با دیده دوست</p></div>
<div class="m2"><p>جهان گم دیده در نور تجلا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شناسندت بفردانیت امروز</p></div>
<div class="m2"><p>که حاجت نیست ایشانرا بفردا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سخن مستانه میگوید حسینت</p></div>
<div class="m2"><p>که دادش ساقی عشق تو صهبا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منم معذور ای عشق ار بگویم</p></div>
<div class="m2"><p>چو چشمم گشت در نور تو بینا</p></div></div>
<div class="b2" id="bn15"><p>که در عالم نمی بینم بجز یار</p>
<p>و ما فی الدار غیرالله دیار</p></div>
<div class="b" id="bn16"><div class="m1"><p>چو شاه عشق مطلق رایت افراخت</p></div>
<div class="m2"><p>ز صحرای عدم لشگر روان ساخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بمیدان شهادت روی آورد</p></div>
<div class="m2"><p>ز ملک غیب چون رایت برافراخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خزینه خانه اسم و صفت را</p></div>
<div class="m2"><p>چو در بگشاد و خلق کون بنواخت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بحسن خود تجلی کرد اول</p></div>
<div class="m2"><p>که میبایست گوی عاشقی باخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل عشاق را از آتش شوق</p></div>
<div class="m2"><p>چو زر خالص اندر بوته بگداخت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شهنشه را در این جلباب صورت</p></div>
<div class="m2"><p>بوحدت هیچکس چون یار نشناخت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در این عالم برای سلب روپوش</p></div>
<div class="m2"><p>ز عشق آوازه یغما درانداخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صور چون گشت زایل جان عاشق</p></div>
<div class="m2"><p>دل از اغیار بهر یار پرداخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو تیغ غیرت آن شاه یگانه</p></div>
<div class="m2"><p>برای کشتن بیگانه می آخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حسین آن دید و در میدان معنی</p></div>
<div class="m2"><p>سمند بادپا زینگونه میتاخت</p></div></div>
<div class="b2" id="bn26"><p>که در عالم نمی بینم بجز یار</p>
<p>و ما فی الدار غیرالله دیار</p></div>
<div class="b" id="bn27"><div class="m1"><p>چو با عشق جمالت یار گشتم</p></div>
<div class="m2"><p>بجان خویشتن اغیار گشتم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو دیدم هستی جاوید مطلق</p></div>
<div class="m2"><p>من از هستی خود بیزار گشتم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مقام از آشیان عشق کردم</p></div>
<div class="m2"><p>مقیم خانه خمار گشتم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گشادم پر و بال جان چو عنقا</p></div>
<div class="m2"><p>بکوه قاف چون طیار گشتم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زمانی در پس ظل خیالات</p></div>
<div class="m2"><p>چو خفته بسته بیزار گشتم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو خورشید جمالت تافت بر من</p></div>
<div class="m2"><p>از آن خواب گران بیدار گشتم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به بوئی گشته عمری قانع از گل</p></div>
<div class="m2"><p>سراسیمه بگرد خار گشتم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو گلزار جمال خود نمودی</p></div>
<div class="m2"><p>چو بلبل بر رخ گل زار گشتم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کجا در چشم من آیند اغیار</p></div>
<div class="m2"><p>چو با عشق تو یار غار گشتم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو دیدم عیسی دلخسته گانی</p></div>
<div class="m2"><p>من آشفته دل و بیمار گشتم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو با هستی مقید بودم اول</p></div>
<div class="m2"><p>بگرد هر دری بسیار گشتم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو حلقه پیش در خود را بمانده</p></div>
<div class="m2"><p>ندیدم خلوت اسرار گشتم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حسین آسا اگر گویم عجب نیست</p></div>
<div class="m2"><p>چو از دیدار برخوردار گشتم</p></div></div>
<div class="b2" id="bn40"><p>که در عالم نمی بینم بجز یار</p>
<p>و ما فی الدار غیرالله دیار</p></div>
<div class="b" id="bn41"><div class="m1"><p>بیا ای قبله اهل معانی</p></div>
<div class="m2"><p>که تو جان همه خلق جهانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جهان را زندگی از تست زیرا</p></div>
<div class="m2"><p>همه عالم تن و در وی تو جانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو جانی لیک از جسمی منزه</p></div>
<div class="m2"><p>تو ماهی لیک اندر لامکانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو در پنهانی خویشی هویدا</p></div>
<div class="m2"><p>تو در عین هویدائی نهانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو مستوری ز چشم اهل غفلت</p></div>
<div class="m2"><p>اگر چه پیش اهل دل عیانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز قدوسی خود برتر ز عقلی</p></div>
<div class="m2"><p>ز صبوحی برون از هر گمانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جهان پر آیت حسن تو لیکن</p></div>
<div class="m2"><p>چنین آیات خواندن تو توانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز خود فانی شو ای دل در ره عشق</p></div>
<div class="m2"><p>که تا یابی بقای جاودانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>صدفهای قوالب چون شکستی</p></div>
<div class="m2"><p>نماید گوهر بحر معانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو اندر عشق محو یار باشی</p></div>
<div class="m2"><p>شناسی اینک او را نیست ثانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جمالش چون بچشم او ببینی</p></div>
<div class="m2"><p>بگوئی هم بطور ترجمانی</p></div></div>
<div class="b2" id="bn52"><p>که در عالم نمی بینم بجز یار</p>
<p>و مافی الدار غیرالله دیار</p></div>
<div class="b" id="bn53"><div class="m1"><p>بیا ای برده آرام و قرارم</p></div>
<div class="m2"><p>که من بی تو سر عالم ندارم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بسوزد عالم و آدم بیکبار</p></div>
<div class="m2"><p>اگر آهی ز سوز دل برآرم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شب دوشینه در خمخانه عشق</p></div>
<div class="m2"><p>ز درد درد میدادی عقارم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بده امروز جام دیگر ایدوست</p></div>
<div class="m2"><p>که از دردی دردت در خمارم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو ای عذرا چو از چشمم برفتی</p></div>
<div class="m2"><p>من وامق چگونه خون نبارم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مرا معذور دار ای مردم چشم</p></div>
<div class="m2"><p>بخون دل همی شوید عذارم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مرا ای عشق برگیر از میانه</p></div>
<div class="m2"><p>که تا دلدار آید در کنارم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گر از بیگانه و خویشم برآری</p></div>
<div class="m2"><p>چه غم دارم توئی خویش و تبارم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گر از شادی عالم بی نصیبم</p></div>
<div class="m2"><p>چه غم چون هست دردت غمگسارم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز غم شاید که مستانه بگویم</p></div>
<div class="m2"><p>چو از نور تجلی مست یارم</p></div></div>
<div class="b2" id="bn63"><p>که در عالم نمی بینم بجز یار</p>
<p>و مافی الدار غیرالله دیار</p></div>
<div class="b" id="bn64"><div class="m1"><p>بیا ساقی که از عشق تو مستم</p></div>
<div class="m2"><p>ز مستی رفت دین و دل ز دستم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بلی مستی من مستور نبود</p></div>
<div class="m2"><p>که من سرمست صهبای الستم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چگونه برنخیزم از سر عقل</p></div>
<div class="m2"><p>چو در خمخانه عشقت نشستم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو تو یکبار روی خود نمودی</p></div>
<div class="m2"><p>دو چشم از دیدن غیر تو بستم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بسوزان هستی من زاتش عشق</p></div>
<div class="m2"><p>اگر دانی که یکدم بی تو هستم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو نور هستی مطلق بدیدم</p></div>
<div class="m2"><p>ز قید هستی خود باز هستم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>منم پنجاه ساله عاشق ایماه</p></div>
<div class="m2"><p>چو ماهی کی بود پروای شستم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نخواهم جست غیر قید عشقت</p></div>
<div class="m2"><p>بچستی چون ز جوی عقل جستم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>من دلخسته ضربتها چشیدم</p></div>
<div class="m2"><p>ولی هرگز دل مردم نخستم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بلند است اختر اقبال و بختم</p></div>
<div class="m2"><p>که بر درگاه تو چون خاک هستم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>حسین آسا بگویم بی تحاشا</p></div>
<div class="m2"><p>چو از جام تجلای تو مستم</p></div></div>
<div class="b2" id="bn75"><p>که در عالم نمی بینم بجز یار</p>
<p>و ما فی الدار غیرالله دیار</p></div>
<div class="b" id="bn76"><div class="m1"><p>زهی جانی که جانانش تو باشی</p></div>
<div class="m2"><p>خوشا دردی که درمانش تو باشی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>قدم سازند از سر عاشقانت</p></div>
<div class="m2"><p>در آن راهی که پایانش تو باشی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بزخم تیغ دشمن طالب دوست</p></div>
<div class="m2"><p>کجا میرد اگر جانش تو باشی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>خلیل الله زاتش کی هراسد</p></div>
<div class="m2"><p>چو در آتش نگهبانش تو باشی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چرا یوسف به تنگ آید ز زندان</p></div>
<div class="m2"><p>چو راحت بخش زندانش تو باشی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>همیشه عاقبت محمود باشد</p></div>
<div class="m2"><p>در آن کاری که سامانش تو باشی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نباشد میل شاهی دو عالم</p></div>
<div class="m2"><p>گدائی را که سلطانش تو باشی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بعالم کی نظر اندازد آن کس</p></div>
<div class="m2"><p>که نور چشم گریانش تو باشی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو پروانه چرا عاشق نسوزد</p></div>
<div class="m2"><p>اگر شمع شبستانش تو باشی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چو جانان خلوتی در جان گزیند</p></div>
<div class="m2"><p>دلا باید که دربانش تو باشی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو عید اکبر ار دیدار یابی</p></div>
<div class="m2"><p>به تیر عشق قربانش تو باشی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>اگر فرمان بجانبازی کند دوست</p></div>
<div class="m2"><p>غلام بنده فرمانش تو باشی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>حسین از عشق هر ساعت بگوید</p></div>
<div class="m2"><p>اگر یار سخندانش تو باشی</p></div></div>
<div class="b2" id="bn89"><p>که در عالم نمی بینم بجز یار</p>
<p>وما فی الدار غیرالله دیار</p></div>