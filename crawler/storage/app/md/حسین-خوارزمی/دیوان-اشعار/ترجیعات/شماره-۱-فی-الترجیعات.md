---
title: >-
    شمارهٔ  ۱ - فی الترجیعات
---
# شمارهٔ  ۱ - فی الترجیعات

<div class="b" id="bn1"><div class="m1"><p>طلع العشق ایها العشاق</p></div>
<div class="m2"><p>و استنارت بنوره الافاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رش من نور شوقه و به</p></div>
<div class="m2"><p>اشرقت ارض قلبی المشتاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرتو افکند آنچنان بدری</p></div>
<div class="m2"><p>که نه بیند ز دور چرخ محاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شده طالع چنان مهی که از او</p></div>
<div class="m2"><p>پر ز خورشید گشت هفت طباق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهوشان پیش طاق ابرویش</p></div>
<div class="m2"><p>دعوی حسن در نهاده بطاق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یارب این ماه را مباد افول</p></div>
<div class="m2"><p>یارب این وصل را مباد فراق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه دیوانه گشته ای ای دل</p></div>
<div class="m2"><p>زان پری صورت ملک اخلاق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست در زن بشوق دوست که اوست</p></div>
<div class="m2"><p>بهر معراج اهل عشق براق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون بدرگاه یار یابی بار</p></div>
<div class="m2"><p>پس تو بینی بدیده عشاق</p></div></div>
<div class="b2" id="bn10"><p>که جهان مظهر است و ظاهر دوست</p>
<p>همه عالم پر از تجلی اوست</p></div>
<div class="b" id="bn11"><div class="m1"><p>عشق رایات سلطنت افراخت</p></div>
<div class="m2"><p>پس اقالیم عقل غارت ساخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن یکی را بسان عود بسوخت</p></div>
<div class="m2"><p>واندگر را مثال چنگ نواخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاهد روی پوش حجله غیب</p></div>
<div class="m2"><p>پرده کبریا ز روی انداخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا نیاید بچشم ما جز دوست</p></div>
<div class="m2"><p>بر سر غیر تیغ غیرت آخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جانم از غیرتش چو آگه شد</p></div>
<div class="m2"><p>خانه و دل ز غیر او پرداخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل من در قمارخانه عشق</p></div>
<div class="m2"><p>بیکی ضربه هر چه داشت بباخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیش صراف عشق قلب بود</p></div>
<div class="m2"><p>دل که در بوته بلا بگداخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عالمی بنده شهی است که او</p></div>
<div class="m2"><p>علم عشق در جهان افراخت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در هوای هویتش جولان</p></div>
<div class="m2"><p>کند آن کس که اسب همت تاخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از کرم دوست چون تجلی کرد</p></div>
<div class="m2"><p>گوید آنکس که سر عشق شناخت</p></div></div>
<div class="b2" id="bn21"><p>که جهان مظهر است و ظاهر دوست</p>
<p>همه عالم پر از تجلی اوست</p></div>
<div class="b" id="bn22"><div class="m1"><p>طلعت عشق اگر عیان بینی</p></div>
<div class="m2"><p>روی جانان بچشم جان بینی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از تلون اگر برون آئی</p></div>
<div class="m2"><p>نقش یکرنگی جهان بینی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر ز حبس خرد توانی رست</p></div>
<div class="m2"><p>ساحت عشق بیکران بینی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>منگر جز بوحدت نقاش</p></div>
<div class="m2"><p>تا بکی نقش این و آن بینی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خانه دل ز غیر خالی کن</p></div>
<div class="m2"><p>تا در او روی دلستان بینی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بی نشان شو ز خویشتن ای دل</p></div>
<div class="m2"><p>تا نشانی ز پی نشان بینی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در هوای هویت ار بپری</p></div>
<div class="m2"><p>جان جولان ز لامکان بینی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>طایر دل چو بال بگشاید</p></div>
<div class="m2"><p>عرش را کمتر آشیان بینی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کوش اسرار چین بدست آور</p></div>
<div class="m2"><p>تا ز هر ذره ترجمان بینی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر ترا آرزوی دلدار است</p></div>
<div class="m2"><p>دیده بگشای تا عیان بینی</p></div></div>
<div class="b2" id="bn32"><p>که جهان مظهر است و ظاهر دوست</p>
<p>همه عالم پر از تجلی اوست</p></div>
<div class="b" id="bn33"><div class="m1"><p>ای بدرگاه تو نیاز همه</p></div>
<div class="m2"><p>روی تو قبله نماز همه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پرده از روی خویشتن برگیر</p></div>
<div class="m2"><p>تا حقیقت شود مجاز همه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گاه گاهی دل مرا بنواز</p></div>
<div class="m2"><p>ای شهنشاه دلنواز همه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ما غباری ز خاکپای توایم</p></div>
<div class="m2"><p>از پی تست ترک و تاز همه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر چه بیچاره ایم باکی نیست</p></div>
<div class="m2"><p>کرم تست چاره ساز همه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نازنینا ز بی نیازی تست</p></div>
<div class="m2"><p>با چنان ناز تو نیاز همه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عاشقان گر چه راز دارانند</p></div>
<div class="m2"><p>زین سخن فاش گشت راز همه</p></div></div>
<div class="b2" id="bn40"><p>که جهان مظهر است و ظاهر دوست</p>
<p>همه عالم پر از تجلی اوست</p></div>
<div class="b" id="bn41"><div class="m1"><p>هر که را دل ز عاشقی خون شد</p></div>
<div class="m2"><p>محرم بارگاه بیچون شد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آنکه درمان خرید و دردش داد</p></div>
<div class="m2"><p>پیش ارباب عشق مغبون شد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سوخت جانم ز داغ غم لیکن</p></div>
<div class="m2"><p>شوقم از درد عشق افزون شد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شاهد عشق بود حجله نشین</p></div>
<div class="m2"><p>با لباس قیود بیرون شد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آنکه آزاد بود از چه و چون</p></div>
<div class="m2"><p>بسته این چرا و آن چون شد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وندر آئینه مظاهر خلق</p></div>
<div class="m2"><p>روی خود را چو دید مفتون شد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از سر ناظری و منظوری</p></div>
<div class="m2"><p>گاه لیلی و گاه مجنون شد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بگسل ای دل ز خویشتن که مسیح</p></div>
<div class="m2"><p>از تجرد بسوی گردون شد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دل ز قید صور چو یافت خلاص</p></div>
<div class="m2"><p>نوبت این حدیث اکنون شد</p></div></div>
<div class="b2" id="bn50"><p>که جهان مظهر است و ظاهر دوست</p>
<p>همه عالم پر از تجلی اوست</p></div>
<div class="b" id="bn51"><div class="m1"><p>ای همه کائنات مست از تو</p></div>
<div class="m2"><p>خورده جانها می الست از تو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تا تو ساقی دردی دردی</p></div>
<div class="m2"><p>زاهدان گشته می پرست از تو</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>آخر ای شاهباز سدره نشین</p></div>
<div class="m2"><p>طایر جان ما نرست از تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون مگس میزنند شهبازان</p></div>
<div class="m2"><p>بر سر خویشتن دو دست از تو</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عقل کل با کمال دانش خویش</p></div>
<div class="m2"><p>کرد چستی ولی نجست از تو</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>داغها دارد از تو مه در دل</p></div>
<div class="m2"><p>زانکه بازار او شکست از تو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو ورای اشارتی چکنم</p></div>
<div class="m2"><p>گر چه بالا پرست و پست از تو</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خرم آن دل که در کشاکش عشق</p></div>
<div class="m2"><p>نیست گردد ز خویش و هست از تو</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عرش و کرسی ز عشق تو مستند</p></div>
<div class="m2"><p>ما نه تنها شدیم مست از تو</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چون تو اظهار خویشتن کردی</p></div>
<div class="m2"><p>در دل خسته نقش بست از تو</p></div></div>
<div class="b2" id="bn61"><p>که جهان مظهر است و ظاهر دوست</p>
<p>همه عالم پر از تجلی اوست</p></div>
<div class="b" id="bn62"><div class="m1"><p>ساقیا بهر چاره مخمور</p></div>
<div class="m2"><p>اسق خمرا مزاجها کافور</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>غمزای از تو و هزار جنون</p></div>
<div class="m2"><p>جرعه ای زان شراب و صد شر و شور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زان شرابی که از نسیمش خاست</p></div>
<div class="m2"><p>های و هوئی ز مردگان قبور</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بر سر خاک جرعه ای افشان</p></div>
<div class="m2"><p>تا هویدا شود صفات نشور</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>با می و طلعت تو ای ساقی</p></div>
<div class="m2"><p>فارغیم از بهشت و چهره حور</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هر کسی را نظر به مهروئی</p></div>
<div class="m2"><p>ما نداریم غیر تو منظور</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>احول است او که جز تو می بیند</p></div>
<div class="m2"><p>آنچنان چشم بد ز روی تو دور</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نتواند ترا شناخت مگر</p></div>
<div class="m2"><p>دیده ای کز رخ تو دارد نور</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تا بکی راز خود نهان داریم</p></div>
<div class="m2"><p>مستی ما نمی شود مستور</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>در قیود صور مباش حسین</p></div>
<div class="m2"><p>تا رسد سر این سخن بظهور</p></div></div>
<div class="b2" id="bn72"><p>که جهان مظهر است و ظاهر دوست</p>
<p>همه عالم پر از تجلی اوست</p></div>
<div class="b" id="bn73"><div class="m1"><p>ما که دردی کشان خماریم</p></div>
<div class="m2"><p>جام جم در نظر نمیآریم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گشته در فکر دوست مستغرق</p></div>
<div class="m2"><p>وز دو عالم فراغتی داریم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>او چو ناز آورد نیاز آریم</p></div>
<div class="m2"><p>ور بیازارد او نیازاریم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>سر ما گر چه پایمال شود</p></div>
<div class="m2"><p>دامن او ز دست نگذاریم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>گر بجنت تجلئی نکند</p></div>
<div class="m2"><p>از نعیم بهشت بیزاریم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ور در آتش رویم همچو خلیل</p></div>
<div class="m2"><p>با خیالش درون گلزاریم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>آه کز ناشناسی و حیرت</p></div>
<div class="m2"><p>یار با ما و طالب یاریم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بنده ماست هر کجا شاهی ست</p></div>
<div class="m2"><p>تا اسیر کمند دلداریم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گر نه بینیم غیر او چه عجب</p></div>
<div class="m2"><p>ما که از واقفان اسراریم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ور بگویم مسیح عیبی نیست</p></div>
<div class="m2"><p>از تجلی چو غرق انواریم</p></div></div>
<div class="b2" id="bn83"><p>که جهان مظهر است و ظاهر دوست</p>
<p>همه عالم پر از تجلی اوست</p></div>
<div class="b" id="bn84"><div class="m1"><p>مدتی شد که مبتلای توایم</p></div>
<div class="m2"><p>تو شهنشاه و ما گدای توایم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>تا تو خورشیدوش همی تابی</p></div>
<div class="m2"><p>ما چو ذرات در هوای توایم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>از شرف تاج تارک عرشیم</p></div>
<div class="m2"><p>زانکه ایدوست خاکپای توایم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>می نبندیم جز تو هیچ نگار</p></div>
<div class="m2"><p>ما که عشاق بینوای توایم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>میکش ایدوست تیغ و میکش باز</p></div>
<div class="m2"><p>زانکه ما طالب رضای توایم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>در وفایت طمع نمی بندیم</p></div>
<div class="m2"><p>شکر کاندر خور جفای توایم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>هر کسی از برای دلداری ست</p></div>
<div class="m2"><p>ما شکسته دلان برای توایم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>قاصریم از ادای شکر هنوز</p></div>
<div class="m2"><p>روز و شب گر چه در ثنای توایم</p></div></div>
<div class="b2" id="bn92"><p>که جهان مظهر است و ظاهر دوست</p>
<p>همه عالم پر از تجلی اوست</p></div>