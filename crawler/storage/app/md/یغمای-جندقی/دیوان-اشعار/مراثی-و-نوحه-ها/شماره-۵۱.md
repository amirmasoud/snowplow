---
title: >-
    شمارهٔ  ۵۱
---
# شمارهٔ  ۵۱

<div class="b" id="bn1"><div class="m1"><p>تفریق جسم و جان است بدرود دوستداران</p></div>
<div class="m2"><p>با آتش جدائی باد است پند یاران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل دجله دیده دریا برق آه و اشک باران</p></div>
<div class="m2"><p>بگذار تا بگرئیم چون ابر در بهاران</p></div></div>
<div class="b2" id="bn3"><p>کز سنگ ناله خیزد روز وداع یاران</p></div>
<div class="b" id="bn4"><div class="m1"><p>آن کم به گریه خندد حرمان ندیده باشد</p></div>
<div class="m2"><p>مقدار وصل داند هجر ار کشیده باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوق نشاط از آن پرس کش غم رسیده باشد</p></div>
<div class="m2"><p>هر کو شراب فرقت روزی چشیده باشد</p></div></div>
<div class="b2" id="bn6"><p>داند که تلخ باشد قطع امیدواران</p></div>
<div class="b" id="bn7"><div class="m1"><p>انگیخت عکس عادت عمان سراب چشمم</p></div>
<div class="m2"><p>طغیان موج دل ساخت دریا حباب چشمم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قلزم به جای قطره بارد سحاب چشمم</p></div>
<div class="m2"><p>با ساربان بگوئید احوال آب چشمم</p></div></div>
<div class="b2" id="bn9"><p>تا بر شتر نبندد محمل بروز باران</p></div>
<div class="b" id="bn10"><div class="m1"><p>ایشان پیاده از خیل ما بر رکاب حسرت</p></div>
<div class="m2"><p>چون دیده و دل ما در آب و تاب حسرت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رفتند و خوش گرفتند هنجار خواب حسرت</p></div>
<div class="m2"><p>بگذاشتند ما را در دیده آب حسرت</p></div></div>
<div class="b2" id="bn12"><p>گریان چو در قیامت چشم گناه کاران</p></div>
<div class="b" id="bn13"><div class="m1"><p>از بار سر سنان را برگ رشاقت آمد</p></div>
<div class="m2"><p>وز اسر ما عدو را ساز افاقت آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هان ای پدر چه پائی گاه رفاقت آمد</p></div>
<div class="m2"><p>ای صبح شب نشینان جانم به طاقت آمد</p></div></div>
<div class="b2" id="bn15"><p>از بسکه دیر ماندی چون شام روزه داران</p></div>
<div class="b" id="bn16"><div class="m1"><p>عمری چونی میان بست دل بر ادای عشقت</p></div>
<div class="m2"><p>یک دم نزیست خاموش نای از نوای عشقت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نامد به گفتگو راست شرح بلای عشقت</p></div>
<div class="m2"><p>چندانکه بر شمردم از ماجرای عشقت</p></div></div>
<div class="b2" id="bn18"><p>اندوه دل نگفتم الا یک از هزاران</p></div>
<div class="b" id="bn19"><div class="m1"><p>میلت نوشته بر جان شوقت سرشته در گل</p></div>
<div class="m2"><p>با احتمال دوری تابوت به ز محمل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در راه عشق سستی کاری است سخت مشکل</p></div>
<div class="m2"><p>سعدی به روزگاران مهری نشسسته بر دل</p></div></div>
<div class="b2" id="bn21"><p>بیرون نمی توان کرد الا به روزگاران</p></div>
<div class="b" id="bn22"><div class="m1"><p>هر دم غمیم از نو بر غم کند سرایت</p></div>
<div class="m2"><p>لیک از غمان چه درمان یغما مرا شکاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر هر که هر چه بایست راندم از این روایت</p></div>
<div class="m2"><p>تا کی کنم حکایت شرح اینقدر کفایت</p></div></div>
<div class="b2" id="bn24"><p>باقی نمی توان گفت الا به غمگساران</p></div>