---
title: >-
    شمارهٔ  ۸۳
---
# شمارهٔ  ۸۳

<div class="b" id="bn1"><div class="m1"><p>از کید اختر از جور گردون علی</p></div>
<div class="m2"><p>از ظلم دشمن از بخت وارون علی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو روانت ماه تابانت علی</p></div>
<div class="m2"><p>افتاده بر خاک غلطیده در خون علی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افسوس از این روی واین سنبل و موی علی</p></div>
<div class="m2"><p>کز ریشه افتاد این سرو موزون علی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم به شادی وقت دامادی علی</p></div>
<div class="m2"><p>گردم به عشرت دمساز و مقرون علی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از روزگارم و از کار و بارم علی</p></div>
<div class="m2"><p>این گونه ایام کی بود مظنون علی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیل مصیبت فوج مرارت علی</p></div>
<div class="m2"><p>آورده اینک بر ما شبیخون علی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش روزگارت و انجام کارت علی</p></div>
<div class="m2"><p>کز دهر بردی خرگاه بیرون علی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنگر که مادر بی یار و یاور علی</p></div>
<div class="m2"><p>گشته روانه در دشت و هامون علی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد حیف و صد حیف کز تیر و خنجر علی</p></div>
<div class="m2"><p>کز ریشه افتاد این سرو موزون علی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از تشنه کامی قلب تو بریان علی</p></div>
<div class="m2"><p>وزچشم مادر جاری است جیحون علی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر حال یغما ای ماه تابان علی</p></div>
<div class="m2"><p>بنما نگاهی از لطف بی چون علی</p></div></div>