---
title: >-
    شمارهٔ  ۷۵
---
# شمارهٔ  ۷۵

<div class="b" id="bn1"><div class="m1"><p>به دشت کربلا سلطان دین را چون گذار آمد</p></div>
<div class="m2"><p>برای قتلش از هر سو سپاه بی شمار آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار و نهصد و پنجاه و یک زخمش به تن وارد</p></div>
<div class="m2"><p>ز وارون گردشی های سپهر کجمدار آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسید ازکوفه و شامش صف اندر صف ز پی اعدا</p></div>
<div class="m2"><p>زکرکوک وری و مصرش هزار اندر هزار آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز داغ اکبر از اشک جگرگون دامنش گلشن</p></div>
<div class="m2"><p>به سوک اصغر از زاری کنارش لاله زار آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک وارون شوی از ظلمت ای بیدادگر آخر</p></div>
<div class="m2"><p>یزید از قتل فرزند پیمبر کامکار آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گردون باز ماندی کاشکی ای چرخ دون پرور</p></div>
<div class="m2"><p>ز زین چون بر زمین آن آسمان اقتدار آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تفو ای چرخ گردون بر تو کز راه ستمکاری</p></div>
<div class="m2"><p>حریم مصطفی بر ناقه عریان سوار آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو اندر بزم جان دادن به نصرت کس نماند او را</p></div>
<div class="m2"><p>پی یاری برون از مهد طفل شیرخوار آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سوک لاله رویان گر نبودی از چه رو لاله</p></div>
<div class="m2"><p>به گلزار جهان زینسان به چهر داغدار آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز قانون عزا غافل مشو یغما که در دوران</p></div>
<div class="m2"><p>ترا از جد و باب این شیوه شیوا شعار آمد</p></div></div>