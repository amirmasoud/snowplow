---
title: >-
    شمارهٔ  ۶۳
---
# شمارهٔ  ۶۳

<div class="b" id="bn1"><div class="m1"><p>چو از گردش چرخ زینت عرش برین</p></div>
<div class="m2"><p>از عرشه زین فتاد بر فرش زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فرش غبار خاکیان خاست به عرش</p></div>
<div class="m2"><p>عرش از در غم به فرش شد خاک نشین</p></div></div>