---
title: >-
    شمارهٔ  ۲۱
---
# شمارهٔ  ۲۱

<div class="b" id="bn1"><div class="m1"><p>آن که با موکب او قافله ها دل برود</p></div>
<div class="m2"><p>در رکابش دل دیوانه و عاقل برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز جمالش غم جان خارج و داخل برود</p></div>
<div class="m2"><p>گفتمش سیر ببینم مگر از دل برود</p></div></div>
<div class="b2" id="bn3"><p>آنچنان جای گرفته است که مشکل برود</p></div>
<div class="b" id="bn4"><div class="m1"><p>گر برم زآتش دل بر مه و خورشید شعاع</p></div>
<div class="m2"><p>مکن از مهر نصیحت مکن از کینه نزاع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز میدان وداع است نه ایوان سماع</p></div>
<div class="m2"><p>دلی از سنگ بباید به سر راه وداع</p></div></div>
<div class="b2" id="bn6"><p>تا تحمل کند آن لحظه که محمل برود</p></div>
<div class="b" id="bn7"><div class="m1"><p>نظر آسا چو پی قافله پو می گیرم</p></div>
<div class="m2"><p>تا ز دل جو نشود دیده گلو می گیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور کند چشمه به مژگان سر جو می گیرم</p></div>
<div class="m2"><p>اشک حسرت به سرانگشت فرو می گیرم</p></div></div>
<div class="b2" id="bn9"><p>که اگر راه دهم قافله در گل برود</p></div>
<div class="b" id="bn10"><div class="m1"><p>از نظر می رودم چهر دلارای حبیب</p></div>
<div class="m2"><p>کی بپاید ز پیش پای دل از پند ادیب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عقل و سر می برود در قدمش دست و رکیب</p></div>
<div class="m2"><p>عجب است ار نرود قاعده صبر و شکیت</p></div></div>
<div class="b2" id="bn12"><p>پیش هر دیده که آن شکل و شمایل برود</p></div>
<div class="b" id="bn13"><div class="m1"><p>فاصله کوری و دیدم به نظر یک سرمو است</p></div>
<div class="m2"><p>نکنم فرق که این دیده من یالب جو است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاید ار پی نبرم کاین سر من و آن ره اوست</p></div>
<div class="m2"><p>ره ندیدم چو برفت از نظرم صورت دوست</p></div></div>
<div class="b2" id="bn15"><p>همچو چشمی که چراغش ز مقابل برود</p></div>
<div class="b" id="bn16"><div class="m1"><p>صحتش درد اگر فاطمه بیمار تو نیست</p></div>
<div class="m2"><p>راحتش رنج دل ار خسته و افگار تو نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای تو جان همه تنها دل و جان زار تو نیست</p></div>
<div class="m2"><p>کس ندانم که درین شهر گرفتار تو نیست</p></div></div>
<div class="b2" id="bn18"><p>مگر آن کس که به شهر آید و غافل برود</p></div>
<div class="b" id="bn19"><div class="m1"><p>بارها خون دل از شش جهتم راه ببست</p></div>
<div class="m2"><p>جوش عمان نظر سیل به قلزم پیوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لیک خود پاره ای تخته نیارست گسست</p></div>
<div class="m2"><p>موج این بار چنان کشتی طاقت بشکست</p></div></div>
<div class="b2" id="bn21"><p>که عجب دارم اگر تخته به ساحل برود</p></div>
<div class="b" id="bn22"><div class="m1"><p>سوی کویم مکش از یارم سفر کرده هجر</p></div>
<div class="m2"><p>خانه گور بود منزل دل مرده هجر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>راحت وصل بود مرگ به افسرده هجر</p></div>
<div class="m2"><p>قیمت وصل نداند مگر آزرده هجر</p></div></div>
<div class="b2" id="bn24"><p>خسته آسوده بخسبد چو به منزل برود</p></div>
<div class="b" id="bn25"><div class="m1"><p>ز آتش عشق خود اندر تب و تابم می کشت</p></div>
<div class="m2"><p>یا خیال لبش از دیده در آبم می کشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر به رحمت به مثل یا به عذابم می کشت</p></div>
<div class="m2"><p>لطف بود آنکه به شمشیر عتابم می کشت</p></div></div>
<div class="b2" id="bn27"><p>قتل صاحب نظر آن است که قاتل برود</p></div>
<div class="b" id="bn28"><div class="m1"><p>دولت وصل تو را طالب غیبیم و شهود</p></div>
<div class="m2"><p>همه را در ره سودات زیان مایه سود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>والی ار مهر نورزد چه ورا حاصل بود</p></div>
<div class="m2"><p>سعدی ار عشق نبازد چکند ملک وجود</p></div></div>
<div class="b2" id="bn30"><p>حیف باشد که همه عمر به باطل برود</p></div>