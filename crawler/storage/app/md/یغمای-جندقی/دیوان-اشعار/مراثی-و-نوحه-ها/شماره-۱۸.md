---
title: >-
    شمارهٔ  ۱۸
---
# شمارهٔ  ۱۸

<div class="b" id="bn1"><div class="m1"><p>رزم شامی و حجازی کم تر از محشر نباشد</p></div>
<div class="m2"><p>یک نظر در نینوا کن گر ترا باور نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرنه ثارالله آمد خون شاه نینوائی</p></div>
<div class="m2"><p>جاودانی این مصیبت را چرا آخر نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سگی چند آهو آسا شرزه شیری نیم بسمل</p></div>
<div class="m2"><p>بینم اندر خاک و خون یارب علی اکبر نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوخت خواهد دل کجا بر تیره روز ام لیلی</p></div>
<div class="m2"><p>هرکرا روشن چراغی در ره صرصر نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تشنه لب بی شیر کودک بی گنه آغشته در خون</p></div>
<div class="m2"><p>وقعه کبری اجل از ماتم اصغر نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این به خون حلق غلطان آن طپان بر خاک ماتم</p></div>
<div class="m2"><p>کس به روز و سوز این فرزند و آن مادر نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر تن شمشیر زن تیر آزما زوبین گذاران</p></div>
<div class="m2"><p>کو سر موئی که خنجر بر سر خنجر نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک داند باد سنجد آب سوز تشنه کامان</p></div>
<div class="m2"><p>فارغ است از تاب اسپند آنکه در آذر نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک تن و چندان جراحت حاش لله کاینقدرها</p></div>
<div class="m2"><p>بوستان را گل نروید چرخ را اختر نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پای تا سر طرف هامون پشت وادی روی صحرا</p></div>
<div class="m2"><p>خار خشکی نیست کز خون شهیدی تر نباشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خود چه داند حال آن کش خاربستر خشت بالین</p></div>
<div class="m2"><p>تا کسی را جایگه برخشت و خاکستر نباشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا در آئین حرمت ذریه احمد هبا شد</p></div>
<div class="m2"><p>یا مگر اولاد زهرا آل پیغمبر نباشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرو بالای علی دان یا مه رخسار قاسم</p></div>
<div class="m2"><p>گر سری را تن نبینی یا تنی را سر نباشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بند بر پا روی در شام است مسکین دختران را</p></div>
<div class="m2"><p>تا کجا یارد پریدن طایری کش پر نباشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عقل خواهد گشت شیدا شرع خواهد خفت در خون</p></div>
<div class="m2"><p>این تحکم را اگر خشم خدا داور نباشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دیده ها نشگفت اگر آمد ز دل پهلوی دارا</p></div>
<div class="m2"><p>آخر این یک قطره خون خود سد اسکندر نباشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باده پیمائی و دردی ریختن بر جام لعلی</p></div>
<div class="m2"><p>کاوست نیکوتر به دل جاوید اگر کوثر نباشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وای اگر یاسای احمد خون این مینا نریزد</p></div>
<div class="m2"><p>آه اگر عدل الهی سنگ این ساغر نباشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جز به ذکر تشنه کامان هر که راند رای یغما</p></div>
<div class="m2"><p>جاودانش طبع و نطق و خامه ودفتر نباشد</p></div></div>