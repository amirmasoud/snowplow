---
title: >-
    شمارهٔ  ۶۸
---
# شمارهٔ  ۶۸

<div class="b" id="bn1"><div class="m1"><p>در سوگ تو آسمان زمین زیر و زبر</p></div>
<div class="m2"><p>دشت خسک اولی و تل خاکستر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا در شکنم جهان جهان خار به پای</p></div>
<div class="m2"><p>تا بر فکنم فلک فلک خاک به سر</p></div></div>