---
title: >-
    شمارهٔ  ۵۰
---
# شمارهٔ  ۵۰

<div class="b" id="bn1"><div class="m1"><p>ناوک کینه در کمان لشکر فتنه در کمین</p></div>
<div class="m2"><p>گاه زغش به سرنگون گه زعطش به لب نگین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست گسسته از فلک پای شکسته از زمین</p></div>
<div class="m2"><p>از در مهلت و امان از پی ناصر و معین</p></div></div>
<div class="b2" id="bn3"><p>ایل علی مرتضی آل محمد امین</p></div>
<div class="b" id="bn4"><div class="m1"><p>خون حواشی و حشم خاک موالی و حشر</p></div>
<div class="m2"><p>ریخت به خاک تن به تن رفت بباد سر به سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میر مصاف نینوا با لب خشک و چشم تر</p></div>
<div class="m2"><p>بست به نفس محترم ساز جهاد را کمر</p></div></div>
<div class="b2" id="bn6"><p>باد صبا به زیر ران جان جهان به روی زین</p></div>
<div class="b" id="bn7"><div class="m1"><p>تاخت دو اسبه یک تنه بر صف خصم ده دله</p></div>
<div class="m2"><p>تیغ یلی به چنگ در رخش مجاهدت یله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیر گرسنه در غنم گرگ گسسته در گله</p></div>
<div class="m2"><p>صورت تیغ و توسنش معنی برق و زلزله</p></div></div>
<div class="b2" id="bn9"><p>عاقبت آسمان عز خوار فتاده بر زمین</p></div>
<div class="b" id="bn10"><div class="m1"><p>سست رگان شام را ساخت زمانه سخت پی</p></div>
<div class="m2"><p>حمله ور از چهارسو با شل و خشت و تیغ و نی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن به هوای مرزشام این به خیال ملک ری</p></div>
<div class="m2"><p>پهن فراخ آرزو تنگ گرفته گرد وی</p></div></div>
<div class="b2" id="bn12"><p>طعنه زن این به کعب نی زخمه آن به تیغ کین</p></div>
<div class="b" id="bn13"><div class="m1"><p>چرخ ستاد از روش، خاک فتاد از سکون</p></div>
<div class="m2"><p>توفت سمک به تاب وتب خفت فلک به خاک و خون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هوش ز مغز منقطع عقل ملازم جنون</p></div>
<div class="m2"><p>سیرت دهر منقلب وضع زمانه واژگون</p></div></div>
<div class="b2" id="bn15"><p>آنکه قیامتش لقب کرد قیام راستین</p></div>
<div class="b" id="bn16"><div class="m1"><p>خیل حرامی از شره رانده برون ز چارسو</p></div>
<div class="m2"><p>پشت به حرمت نبی در به حرم نهاده رو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اهل حرم شکسته دل خسته جگر گشاده مو</p></div>
<div class="m2"><p>زیور و جامه و حلی رفته به غارت عدو</p></div></div>
<div class="b2" id="bn18"><p>بسته به چشم درفشان عقد جواهر از جبین</p></div>
<div class="b" id="bn19"><div class="m1"><p>نهب گران کوفه را دست به غارت آشنا</p></div>
<div class="m2"><p>از بر این سلب ستان وزسر آن حلی ربا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از حرم خدایگان رفته به بار کبریا</p></div>
<div class="m2"><p>ویله زار بی کسان لیک به گوش اشقیا</p></div></div>
<div class="b2" id="bn21"><p>بانگ سرود خار کن ناله چنگ رامتین</p></div>
<div class="b" id="bn22"><div class="m1"><p>سبز عمامه تو را چرخ سیاه طیلسان</p></div>
<div class="m2"><p>ساخت به خون حلق سرخ از در کام ناکسان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گشت تبه گل از خسک لاله بکاست از خسان</p></div>
<div class="m2"><p>زانده آنکه مانده ام جیش ترا ز واپسان</p></div></div>
<div class="b2" id="bn24"><p>می کشدم نفس نفس حسرت روز واپسین</p></div>
<div class="b" id="bn25"><div class="m1"><p>بار بود به دوش تن سر که نه در بپای تو</p></div>
<div class="m2"><p>با لب خشک و چشم تر رفتی و در عزای تو</p></div></div>
<div class="b2" id="bn26"><p>ماند به چشم خون چکان یغما و آه آتشین</p></div>