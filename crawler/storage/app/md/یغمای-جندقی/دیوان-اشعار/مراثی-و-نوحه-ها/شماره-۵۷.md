---
title: >-
    شمارهٔ  ۵۷
---
# شمارهٔ  ۵۷

<div class="b" id="bn1"><div class="m1"><p>خون بود دل ز هجرم نیمی کباب نیمی</p></div>
<div class="m2"><p>ز امید و بیم نیمی خراب نیمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو از شام هستیم رفت صبح شباب نیمی</p></div>
<div class="m2"><p>آن مه ز مهر برداشت طرف نقاب نیمی</p></div></div>
<div class="b2" id="bn3"><p>امروز یا برآمد ز ابر آفتاب نیمی</p></div>
<div class="b" id="bn4"><div class="m1"><p>از هول آن کم افتد ای بارم از تو در گل</p></div>
<div class="m2"><p>زان چهر و لب سرانجام افغان و اشک حاصل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون آبگون عقیقت و آن آذرین شمایل</p></div>
<div class="m2"><p>از موج دجله چشم وز تاب شعله دل</p></div></div>
<div class="b2" id="bn6"><p>پیوسته ام در آتش نیمی در آب نیمی</p></div>
<div class="b" id="bn7"><div class="m1"><p>نگذاردم سوی رزم خوی بهانه جویت</p></div>
<div class="m2"><p>ور رای رخصت آرد نارد اجازه رویت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون لعل غنچه رنگت چون زلف مشکبویت</p></div>
<div class="m2"><p>دارم جدا ز رویت روئی ز دست خویت</p></div></div>
<div class="b2" id="bn9"><p>نیم از طپانچه نیلی وزخون خضاب نیمی</p></div>
<div class="b" id="bn10"><div class="m1"><p>ساقی اگر شهادت جز سوی او نتازم</p></div>
<div class="m2"><p>ور خون حلق باده از اوست برگ و سازم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با این شراب و ساقی برد است اگر ببازم</p></div>
<div class="m2"><p>گر ملک هر دو کونم بخشد خدای سازم</p></div></div>
<div class="b2" id="bn12"><p>نیمی فدای ساقی رهن شراب نیمی</p></div>
<div class="b" id="bn13"><div class="m1"><p>ای مر مرا محرم از طلعت تو نوروز</p></div>
<div class="m2"><p>دانم نثار فرض است بر آن جمال فیروز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مائیم و قطره ای خون وان نیز باد و صد سوز</p></div>
<div class="m2"><p>یعنی زجان گذشته مسکین دلی شب و روز</p></div></div>
<div class="b2" id="bn15"><p>زان چهر و زلف در تب نیمی به تاب نیمی</p></div>
<div class="b" id="bn16"><div class="m1"><p>بالجمله گر خیال این و آن است اگر جمالت</p></div>
<div class="m2"><p>در مرگ هم نبرم پیوند خط و خالت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون چشم خود چو در خون خسبم به احتمالت</p></div>
<div class="m2"><p>بینم مگر جمالت با صورت خیالت</p></div></div>
<div class="b2" id="bn18"><p>چشمی مراست بیدار نیمی به خواب نیمی</p></div>
<div class="b" id="bn19"><div class="m1"><p>این شب نگر که پیوند با روز حشر پیوست</p></div>
<div class="m2"><p>تا بر فراخت بالا شد رستخیز از او پست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه شب از او به پایان وزوی نه صبح در دست</p></div>
<div class="m2"><p>تحقیق شام هجران یغما چه می کنی هست</p></div></div>
<div class="b2" id="bn21"><p>از نیم این قیامت روز حساب نیمی</p></div>