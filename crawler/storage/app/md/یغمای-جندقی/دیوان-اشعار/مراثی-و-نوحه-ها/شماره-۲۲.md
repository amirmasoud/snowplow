---
title: >-
    شمارهٔ  ۲۲
---
# شمارهٔ  ۲۲

<div class="b" id="bn1"><div class="m1"><p>بیش از این در غم هجران تو خون خورد نشاید</p></div>
<div class="m2"><p>ای سفر کرده سفر کرده چنین دیر نیاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت آن است که بازآئی و بختم بسراید</p></div>
<div class="m2"><p>بخت باز آید از آن در که یکی چون تو در آید</p></div></div>
<div class="b2" id="bn3"><p>روی میمون تو دیدن در دولت بگشاید</p></div>
<div class="b" id="bn4"><div class="m1"><p>نه تو گفتی که به من با غم هجران نستیزی</p></div>
<div class="m2"><p>کشته خود به زمین برنگذاری نگریزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این سفر جز به هلاکم ننشینی و نخیزی</p></div>
<div class="m2"><p>گر حلال است که خون همه عالم تو بریزی</p></div></div>
<div class="b2" id="bn6"><p>آن که روی از همه عالم به تو آورد نشاید</p></div>
<div class="b" id="bn7"><div class="m1"><p>ای پدر رجعت امروز مینداز به فردا</p></div>
<div class="m2"><p>دست شستم ز علی اکبر و عباس تو فرد آ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با وجودت چه نیاز است به کلثوم و به کبری</p></div>
<div class="m2"><p>اگرم هیچ نباشد نه به دنیا نه به عقبی</p></div></div>
<div class="b2" id="bn9"><p>چون تو دارم همه دارم دگرم هیچ نباید</p></div>
<div class="b" id="bn10"><div class="m1"><p>تا کی ای خامه احباب سرودی نسرائی</p></div>
<div class="m2"><p>تلخ کامی ز مذاقم به درودی نزدائی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من نه تنها خمش آیم چو به گفتار درآئی</p></div>
<div class="m2"><p>نیشکر با همه شیرینی اگر لب بگشائی</p></div></div>
<div class="b2" id="bn12"><p>پیش لفظ شکرینت چو نی انگشت بخاید</p></div>
<div class="b" id="bn13"><div class="m1"><p>نه همین دل که غمت سوخت سما را و سمک را</p></div>
<div class="m2"><p>من نه تنها کالمت کاست بشر را و ملک را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از تو ای باب نبرم چه یقین را و چه شک را</p></div>
<div class="m2"><p>صبر بسیار بباید پدر پیر فلک را</p></div></div>
<div class="b2" id="bn15"><p>تا دگر مادر گیتی چو تو فرزند بزاید</p></div>
<div class="b" id="bn16"><div class="m1"><p>عمه تا چند سکینه به بر و دوش تو خسبد</p></div>
<div class="m2"><p>گه به جان گه به دل دیر فراموش تو خسبد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سزد از ناله بنگذارم اگر گوش تو خسبد</p></div>
<div class="m2"><p>قهرم از پیرهن آید که در آغوش تو خسبد</p></div></div>
<div class="b2" id="bn18"><p>زهرم از غالیه آید که بر اندام تو ساید</p></div>
<div class="b" id="bn19"><div class="m1"><p>آبم آتش نشود گر بدهی خاک ببادم</p></div>
<div class="m2"><p>نکنم از تو فرامش بری ار نام زیادم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به خلاف تو نخیزم چو به مهر تو فتادم</p></div>
<div class="m2"><p>دل به سختی بنهادم پس از آن دل به تو دادم</p></div></div>
<div class="b2" id="bn21"><p>هر که از دوست تحمل نکند عهد نپاید</p></div>