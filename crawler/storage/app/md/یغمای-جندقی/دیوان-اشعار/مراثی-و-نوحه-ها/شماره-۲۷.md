---
title: >-
    شمارهٔ  ۲۷
---
# شمارهٔ  ۲۷

<div class="b" id="bn1"><div class="m1"><p>اوفتد یوسف دیگر ز تو در چاه دگر</p></div>
<div class="m2"><p>در هراسم ز دم گرگ تو ای خیره سحر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز این دم گرگ بر آهوی حرم یوز متاز</p></div>
<div class="m2"><p>هان و هان این دم شیر است ببازی مشمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دم گرگ تو شیران خدا رو به صید</p></div>
<div class="m2"><p>از دم گرگ تو سگ های هوا شیر شکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم هژبران جدل را دم گرگت دم شیر</p></div>
<div class="m2"><p>هم غزالان حرم را دم شیرت سر خر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرمی ای چرخ و به بنگاه فنا گاه گزین</p></div>
<div class="m2"><p>رحمی ای مهر و به چه سار عدم راه سپر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باش و تا شام ابد بر قدمی پیش منه</p></div>
<div class="m2"><p>رو و تا روز قیامت نظری پس منگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سهم بدخواه بس ای چرخ نهان ساز کمان</p></div>
<div class="m2"><p>نیزه خصم بس ای مهر بینداز سپر</p></div></div>