---
title: >-
    شمارهٔ  ۳۶
---
# شمارهٔ  ۳۶

<div class="b" id="bn1"><div class="m1"><p>شکوه از چرخ ستمگر چکنم گر نکنم</p></div>
<div class="m2"><p>چکنم گر نکنم</p></div></div>
<div class="b2" id="bn2"><p>گله از گردش اختر چکنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn3"><div class="m1"><p>غم عباس بلاکش چکشم گر نکشم</p></div>
<div class="m2"><p>چکشم گر نکشم</p></div></div>
<div class="b2" id="bn4"><p>ناله بر حسرت اکبر چه کنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn5"><div class="m1"><p>شیرمردان ولایت همه چو آهو بچگان</p></div>
<div class="m2"><p>چنگ فرسود سگان</p></div></div>
<div class="b2" id="bn6"><p>گله ز اهمال غضنفر چکنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn7"><div class="m1"><p>رنج ناکامی قاسم چه برم گر نبرم</p></div>
<div class="m2"><p>چه برم گر نبرم</p></div></div>
<div class="b2" id="bn8"><p>یاد محرومی اصغر چکنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn9"><div class="m1"><p>موج خون بر طرف افکند چو ناچیز صدف</p></div>
<div class="m2"><p>در دریای نجف</p></div></div>
<div class="b2" id="bn10"><p>دیدگان لجه گوهر چکنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn11"><div class="m1"><p>آه بر خواری خواهر چه کشم گر نکشم</p></div>
<div class="m2"><p>چه کشم گر نکشم</p></div></div>
<div class="b2" id="bn12"><p>گریه در سوک برادر چه کنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn13"><div class="m1"><p>آنچه بر آل علی کینه و عدوان وعناد</p></div>
<div class="m2"><p>رفت ز اولاد زیاد</p></div></div>
<div class="b2" id="bn14"><p>داوری پیش پیمبر چکنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn15"><div class="m1"><p>سنگ از این غایله بر دل چه زنم گر نزنم</p></div>
<div class="m2"><p>چه زنم گر نزنم</p></div></div>
<div class="b2" id="bn16"><p>خاک از این واقعه بر سر چکنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn17"><div class="m1"><p>تن شاهی که فراز فلکش پای گهست</p></div>
<div class="m2"><p>خفته بر خاک رهست</p></div></div>
<div class="b2" id="bn18"><p>خاک با چرخ برابر چکنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn19"><div class="m1"><p>سر به زنجیر اسیری چه دهم گر ندهم</p></div>
<div class="m2"><p>چه دهم گر ندهم</p></div></div>
<div class="b2" id="bn20"><p>گردن آماده چنبر چکنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn21"><div class="m1"><p>کودکان بسته زنجیر و زنان خسته بند</p></div>
<div class="m2"><p>دختران در به کمند</p></div></div>
<div class="b2" id="bn22"><p>التجا جانب مادر چکنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn23"><div class="m1"><p>دل به بیداد اعادی چه نهم گر ننهم</p></div>
<div class="m2"><p>چه نهم گر ننهم</p></div></div>
<div class="b2" id="bn24"><p>صبر برغارت لشکر چه کنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn25"><div class="m1"><p>هر قدم زخمه دیگر چه خورم گر نخورم</p></div>
<div class="m2"><p>چه خورم گر نخورم</p></div></div>
<div class="b2" id="bn26"><p>هر نفس ناله دیگر چکنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn27"><div class="m1"><p>میل در دیده از این غم چه کشم گر نکشم</p></div>
<div class="m2"><p>چه کشم گر نکشم</p></div></div>
<div class="b2" id="bn28"><p>نیل از این عارضه در بر چکنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>
<div class="b" id="bn29"><div class="m1"><p>والی هیچ بها را که در این خیل رهی است</p></div>
<div class="m2"><p>سر صاحب کلهی است</p></div></div>
<div class="b2" id="bn30"><p>خاکپای تواش افسر چه کنم گر نکنم</p>
<p>چکنم گر نکنم</p></div>