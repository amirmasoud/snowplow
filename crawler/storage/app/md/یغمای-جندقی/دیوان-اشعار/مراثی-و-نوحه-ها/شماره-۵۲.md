---
title: >-
    شمارهٔ  ۵۲
---
# شمارهٔ  ۵۲

<div class="b" id="bn1"><div class="m1"><p>سوی مدینه ای صبا افتد اگر عبور تو</p></div>
<div class="m2"><p>گفت بگو به فاطمه زینب ناصبور تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در به بقیع بی خبر خفته تو و به کربلا</p></div>
<div class="m2"><p>مال هبا و خون هدر با تن پاره پور تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نرسانم از زمین آتش دل بر اختران</p></div>
<div class="m2"><p>شد به اثیر هیر من، رفت به خاک هور تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانی اگر چگونه زد برق عطش به تشنگان</p></div>
<div class="m2"><p>شعله بر آسمان برد ناله ز خاک گور تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوخت سموم تشنگی برگ بهار عیش من</p></div>
<div class="m2"><p>خورد به مگر کشتگان لطمه سوگ سور تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذری ار به کربلا شب نگری و روز ما</p></div>
<div class="m2"><p>عیش نه ماتم آورد زانده ما سرور تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تلخ به زهر تشنگی چند زید مذاق ما</p></div>
<div class="m2"><p>وقت شد آنکه بر دمد چشمه آب شور تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ظلم فلک به ظلمتم بست قوی گشای رخ</p></div>
<div class="m2"><p>بو که ز دود نار غم باز رهم به نور تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه به خیال ما پدر، نی تو به فکر مادری</p></div>
<div class="m2"><p>غفلت او شد از چه ره یا سبب غرور تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواهی جنت دگر بر به جحیم ما گذر</p></div>
<div class="m2"><p>کوثر تو سرشک ما دیده ما قصور تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنچه به ما زغیبتت رفت در این مجاهدت</p></div>
<div class="m2"><p>کی به مشاهدت کشد تا نفتد حضور تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرصت غیر تا به کی سوخت شرار غیرتم</p></div>
<div class="m2"><p>چند مغایرت کند حوصله غیور تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کیفر این خلاف را وعده به محشر افکنی</p></div>
<div class="m2"><p>ماند قریب حسرتم دل ز قصاص دور تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خیز و گذر به ماریه پس پی داد و داوری</p></div>
<div class="m2"><p>قصه رستخیز ما واقعه نشور تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با همه خردی از قضا والی و ویله امان</p></div>
<div class="m2"><p>نیست بزرگ اگر زند پنجه به پیل مور تو</p></div></div>