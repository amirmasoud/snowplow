---
title: >-
    شمارهٔ  ۳۵
---
# شمارهٔ  ۳۵

<div class="b" id="bn1"><div class="m1"><p>همه زانداز توام بهره غم افتاد فلک</p></div>
<div class="m2"><p>از تو فریاد فلک</p></div></div>
<div class="b2" id="bn2"><p>سال و ماه و شب و روز از تو نیم شاد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn3"><div class="m1"><p>صرصر قهر تو در ماریه از آل زیاد</p></div>
<div class="m2"><p>آتشی ریخت که داد</p></div></div>
<div class="b2" id="bn4"><p>خاک اولاد پیمبر همه بر باد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn5"><div class="m1"><p>روزی آبستن شب های غم و آتش و دود</p></div>
<div class="m2"><p>ترسم آید به وجود</p></div></div>
<div class="b2" id="bn6"><p>کاش اندیشه صحبت شود از یاد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn7"><div class="m1"><p>هم پلنگان حجاز از دم گرگ تو به رم</p></div>
<div class="m2"><p>هم غزالان حرم</p></div></div>
<div class="b2" id="bn8"><p>شیر و آهو همه صیداند و تو صیاد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn9"><div class="m1"><p>خاندانی که بدو یافت عمارت گل و آب</p></div>
<div class="m2"><p>دورت ای خانه خراب</p></div></div>
<div class="b2" id="bn10"><p>ساخت ویران به ستم خانه ات آباد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn11"><div class="m1"><p>بر مراد عدمی چند کنی نفی وجود</p></div>
<div class="m2"><p>از در غیب و شهود</p></div></div>
<div class="b2" id="bn12"><p>آنکه آمد همه را موجب ایجاد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn13"><div class="m1"><p>چند پوئی به غلط از در اعزاز لئام</p></div>
<div class="m2"><p>راه ایذای کرام</p></div></div>
<div class="b2" id="bn14"><p>در پی بنده مجو خواری آزاد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn15"><div class="m1"><p>کرده ای راست در این معرکه غایله زار</p></div>
<div class="m2"><p>فتنه حادثه بار</p></div></div>
<div class="b2" id="bn16"><p>خواهد این فتنه بلای عجبی زاد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn17"><div class="m1"><p>صبر سندان به چنین سخت بلاسنگ و سبوی</p></div>
<div class="m2"><p>تو همان آهن و روی</p></div></div>
<div class="b2" id="bn18"><p>شرمی آخر نه ای از خاره و پولاد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn19"><div class="m1"><p>مرد و زن پیر و جوان دخت و پسر عبدو امیر</p></div>
<div class="m2"><p>کشته گشتند و اسیر</p></div></div>
<div class="b2" id="bn20"><p>با شش و پنج تو چه هفت و چه هفتاد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn21"><div class="m1"><p>تشنه لب تفته دل از خون بنین و اشک بنات</p></div>
<div class="m2"><p>راندی از نیل فرات</p></div></div>
<div class="b2" id="bn22"><p>سوی جیحون و ارس دجله بغداد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn23"><div class="m1"><p>تشنگی سوختگی خسته دلی خون جگری</p></div>
<div class="m2"><p>بی کسی در بدری</p></div></div>
<div class="b2" id="bn24"><p>بر تنی نیست روا این همه بیداد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn25"><div class="m1"><p>دل و دیده همه را تا زحل از تخته گل</p></div>
<div class="m2"><p>پی آن دیده و دل</p></div></div>
<div class="b2" id="bn26"><p>جاودان عهد سرشک آمد و فریاد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn27"><div class="m1"><p>مصطفی مات و علی محو و خدا تعزیه دار</p></div>
<div class="m2"><p>روز محشر شب تار</p></div></div>
<div class="b2" id="bn28"><p>از ستم های تو پیش که برم داد فلک</p>
<p>از تو فریاد فلک</p></div>
<div class="b" id="bn29"><div class="m1"><p>والی از سینه و چشم آذر و نیسان آورد</p></div>
<div class="m2"><p>تا زند از سر درد</p></div></div>
<div class="b2" id="bn30"><p>آه زن اشک فشان بهمن و مرداد فلک</p>
<p>از تو فریاد فلک</p></div>