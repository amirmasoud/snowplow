---
title: >-
    شمارهٔ  ۶
---
# شمارهٔ  ۶

<div class="b" id="bn1"><div class="m1"><p>حریم عصمت آنگه ناقه عریان سواری‌ها</p></div>
<div class="m2"><p>نگون باد ازهیون چرخ این زرین عماری‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سراری عز و دولت را ستیزه چرخ کرد آخر</p></div>
<div class="m2"><p>به دل دولت به درویشی عوض عزت به خواری‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی چونان که نیلوفر در آب از اشک ناکامی</p></div>
<div class="m2"><p>یکی چون لاله در آذر به داغ سوگواری‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه تن از تاب آسوده نه جان از رنج مستخلص</p></div>
<div class="m2"><p>نه دل از آه مستغنی نه چشم از اشکباری‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبون بر دست بیگانه روان در چشم نامحرم</p></div>
<div class="m2"><p>نوان در کنج ویرانه به صد بی‌اعتباری‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه از اقبال پیروزی نه از ایام بهروزی</p></div>
<div class="m2"><p>نه از اختر مددکاری نه از افلاک یاری‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی چون چشم خود در خون ز زخم ناشکیبایی</p></div>
<div class="m2"><p>یکی چون موی خود پیچان ز تاب بی‌قراری‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه اینان را نهفتن روی دست از چشم نامحرم</p></div>
<div class="m2"><p>نه آن بی‌چشم و رو نامحرمان را شرمساری‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عنا محرم بلا برقع سرا بی در ستم دربان</p></div>
<div class="m2"><p>غذا خون فرش خاکستر زهی حرمت‌گذاری‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی بیمار و مسکین خشت و خاکش بالش و بستر</p></div>
<div class="m2"><p>یکی لخت جگر بر کف پی بیمارداری‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه از تیمار و رنج آن را تمنای تن‌آسایی</p></div>
<div class="m2"><p>نه از آسیب بند آن را امید رستگاری‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گدایان دمشقی را نگر سامان سلطانی</p></div>
<div class="m2"><p>خداوندان یثرب را شمار زنگباری‌ها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نبیند تا چنین روزی دریغا دسترس بودی</p></div>
<div class="m2"><p>در آن روزت که سر غلتان به پا در جان‌سپاری‌ها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو زال از سوگ رستم بر به گرگان ماتمت گیرد</p></div>
<div class="m2"><p>ز بهمن در فرات ار فوت شد اسفندیاری‌ها</p></div></div>