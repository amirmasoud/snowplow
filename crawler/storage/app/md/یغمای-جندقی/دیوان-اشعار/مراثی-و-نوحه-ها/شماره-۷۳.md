---
title: >-
    شمارهٔ  ۷۳
---
# شمارهٔ  ۷۳

<div class="b" id="bn1"><div class="m1"><p>تا زچمن خانه زین بر زمین</p></div>
<div class="m2"><p>نخل برازنده اکبر فتاد</p></div></div>
<div class="b2" id="bn2"><p>جلوه گری رفت ز بالای سرو</p>
<p>رعشه بر اندام صنوبر فتاد</p></div>
<div class="b" id="bn3"><div class="m1"><p>شبه رسول امین</p></div>
<div class="m2"><p>شست به خون تا جبین</p></div></div>
<div class="b2" id="bn4"><p>از دم ششمیر کین</p>
<p>خفت به روی زمین</p></div>
<div class="b" id="bn5"><div class="m1"><p>گرد برآمد ز مزار حبیب</p></div>
<div class="m2"><p>رفت برون پای ظفر از رکیب</p></div></div>
<div class="b2" id="bn6"><p>چهره ناهید به خون شد خضیب</p>
<p>روشنی از دیده اختر فتاد</p></div>
<div class="b" id="bn7"><div class="m1"><p>عرش برین شد نگون</p></div>
<div class="m2"><p>گشت افق غرق خون</p></div></div>
<div class="b2" id="bn8"><p>ماند ز ره چرخ دون</p>
<p>گشت زمین بی سکون</p></div>
<div class="b" id="bn9"><div class="m1"><p>قائمه عرش معلا شکست</p></div>
<div class="m2"><p>شیشه نه منظر مینا شکست</p></div></div>
<div class="b2" id="bn10"><p>ماهچه رایت بیضا شکست</p>
<p>از سر مهر فلک افسر فتاد</p></div>
<div class="b" id="bn11"><div class="m1"><p>ریخت زتاثیر غم</p></div>
<div class="m2"><p>نظم کواکب زهم</p></div></div>
<div class="b2" id="bn12"><p>چهره مه شد دژم</p>
<p>صبح فرو برد دم</p></div>
<div class="b" id="bn13"><div class="m1"><p>گشت چو آن کاکل مشکین رسن</p></div>
<div class="m2"><p>از نم خون نافه مشک ختن</p></div></div>
<div class="b2" id="bn14"><p>باز شد از طره سنبل شکن</p>
<p>رایحه از طبله عنبر فتاد</p></div>
<div class="b" id="bn15"><div class="m1"><p>زد به فضای چمن</p></div>
<div class="m2"><p>چتر عزا نسترن</p></div></div>
<div class="b2" id="bn16"><p>خفت به خونین کفن</p>
<p>غنچه نازک بدن</p></div>
<div class="b" id="bn17"><div class="m1"><p>سد شد از این واقعه بر ممکنات</p></div>
<div class="m2"><p>عرصه امکان و طریق جهات</p></div></div>
<div class="b2" id="bn18"><p>بازی شطرنج فلک گشت مات</p>
<p>مهره خورشید به ششدر فتاد</p></div>
<div class="b" id="bn19"><div class="m1"><p>شام ز غم مو برید</p></div>
<div class="m2"><p>صبح گریبان درید</p></div></div>
<div class="b2" id="bn20"><p>گشت به عالم پدید</p>
<p>فتنه یوم الوعید</p></div>
<div class="b" id="bn21"><div class="m1"><p>ناله یغما شد اگر پرده در</p></div>
<div class="m2"><p>ناید از این پرده عجب در نظر</p></div></div>
<div class="b2" id="bn22"><p>کز حرکات فلک پرده در</p>
<p>پرده ز اوضاع جهان برفتاد</p></div>