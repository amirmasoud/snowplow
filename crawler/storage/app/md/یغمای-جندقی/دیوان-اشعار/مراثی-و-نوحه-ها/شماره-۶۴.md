---
title: >-
    شمارهٔ  ۶۴
---
# شمارهٔ  ۶۴

<div class="b" id="bn1"><div class="m1"><p>از خیل بشر آه به گردون خیزد</p></div>
<div class="m2"><p>از چشم ملک به چشم ها خون خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ماتمت آه و اشک اگر اینستی</p></div>
<div class="m2"><p>آتش به فلک سیل ز هامون خیزد</p></div></div>