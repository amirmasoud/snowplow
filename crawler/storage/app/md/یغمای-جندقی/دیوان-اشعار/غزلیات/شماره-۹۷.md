---
title: >-
    شمارهٔ  ۹۷
---
# شمارهٔ  ۹۷

<div class="b" id="bn1"><div class="m1"><p>شد دلم شیفته زلف گره گیر دگر</p></div>
<div class="m2"><p>باز دیوانه در افتاد به زنجیر دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر جراحت چه نهی مرهمم آن به که کنی</p></div>
<div class="m2"><p>زخم شمشیر مرا چاره به شمشیر دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوار شد صید دلم پیش تو خوش آنکه نبود</p></div>
<div class="m2"><p>هر سر موی تو در گردن نخجیر دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساعد آلوده به خون دگران داری و نیست</p></div>
<div class="m2"><p>جز هلاک خودم از دست تو تدبیر دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیش ما با لب و دندان بتان شهد و فقیه</p></div>
<div class="m2"><p>زهرها خورده به یاد عسل و شیر دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته بود آنچه به من پیر مغان گفت مرا</p></div>
<div class="m2"><p>واعظ شهر همان، لیک به تقریر دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهی ار زر کنی این قلب مس اندوده مجوی</p></div>
<div class="m2"><p>به جز از خاک در میکده اکسیر دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دفتر عشق ز یک نکته فزون نیست ولی</p></div>
<div class="m2"><p>هر کسی شرحی از او گفته به تفسیر دگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار یغما نشد از پیر خرد راست کجاست</p></div>
<div class="m2"><p>خضر راهی که شتابم ز پی پیر دگر</p></div></div>