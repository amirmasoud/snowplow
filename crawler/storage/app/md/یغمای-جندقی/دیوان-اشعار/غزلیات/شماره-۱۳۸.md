---
title: >-
    شمارهٔ  ۱۳۸
---
# شمارهٔ  ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>روم به جلد سگ پاسبان که گاه به گاهی</p></div>
<div class="m2"><p>مگر به مغلطه یابم بر آستان تو راهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وحشتی است دل از خیل غمزه در خم زلفش</p></div>
<div class="m2"><p>که بی دلی شب تاریک بر خورد به سیاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ تو ماه شمردم دل تو سنگ و چو دیدم</p></div>
<div class="m2"><p>مثال ذره به خورشید بود و کوه به کاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گوشه گوشه چپ و راست ز ابرویت چه گریزم</p></div>
<div class="m2"><p>که غیر سایه شمشیر فتنه نیست پناهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه سایه ای ز تو بر سر نه نوری از تو به روزن</p></div>
<div class="m2"><p>مرا از آن چه که سروی مرا از این چه که ماهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهار تو است بتان را خزان خرمی ای خط</p></div>
<div class="m2"><p>هزار سال نروئی ندانمت چه گیاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشیده خنجر و جوید بهانه مدعی ای کاش</p></div>
<div class="m2"><p>کند ثواب و مرا متهم کند به گناهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دل رقیبم از آن ره نمی دهد که مبادا</p></div>
<div class="m2"><p>خدای نکرده از این ره کنم به کوی تو راهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همینم از شب زلف تو حاصل است که دارم</p></div>
<div class="m2"><p>به دست روز پریشان و روزگار سیاهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه شام را خبری از سحر اثر نه دعا را</p></div>
<div class="m2"><p>شب فراق تو افتاده ام به روز سیاهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ربود غارت خط تاج نخوت از سر حسنش</p></div>
<div class="m2"><p>مگر رسد سر یغما از این نمد به کلاهی</p></div></div>