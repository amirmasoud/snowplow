---
title: >-
    شمارهٔ  ۷۲
---
# شمارهٔ  ۷۲

<div class="b" id="bn1"><div class="m1"><p>کودکی سنگ به کف چون سوی ویرانه رود</p></div>
<div class="m2"><p>ماجراها به سرم زین دل دیوانه رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام طفلی چو رود آیدم از سینه برون</p></div>
<div class="m2"><p>دل دیوانه و ویرانه به ویرانه رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانی از توبه مستان غرض ناصح چیست</p></div>
<div class="m2"><p>تا بدین واسطه گه گاه به میخانه رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پی سلسله زلف جنون فرمائی</p></div>
<div class="m2"><p>دل دیوانه از اینجا نه به آن خانه رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ته جامی به فلک ده بود از بی خبری</p></div>
<div class="m2"><p>دوره ای بر روش گردش پیمانه رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رشک بر قرب کسم نیست که گر خود دل ماست</p></div>
<div class="m2"><p>آشنا آید و از کوی تو بیگانه رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرزدت خط ز زنخدان و محال است که پیش</p></div>
<div class="m2"><p>کار خوبان پس از این از تو بدین چانه رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل زد از آتش می شمع رخش مرغ چمن</p></div>
<div class="m2"><p>ادب آنست که بر سنت پروانه رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گندم خال تو آن روز که دیدم گفتم</p></div>
<div class="m2"><p>خرمن طاعت ما بر سر این دانه رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یار طفل است و تو دیوانه چه ترسی یغما</p></div>
<div class="m2"><p>کار را باش که طفل از پی دیوانه رود</p></div></div>