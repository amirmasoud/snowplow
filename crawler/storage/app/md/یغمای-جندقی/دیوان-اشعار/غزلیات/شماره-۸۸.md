---
title: >-
    شمارهٔ  ۸۸
---
# شمارهٔ  ۸۸

<div class="b" id="bn1"><div class="m1"><p>تا یار شکر خنده ز در بند نیاید</p></div>
<div class="m2"><p>از مصر به ری قافله قند نیاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرین نشود کام من از تلخی هجران</p></div>
<div class="m2"><p>تا بوسی از آن لعل شکر خند نیاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی که ز شیرین دهنش دیده فرودوز</p></div>
<div class="m2"><p>خاموش که در گوش من این پند نیاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر پیر فلک تازه کند عهد جوانی</p></div>
<div class="m2"><p>از مادر گیتی چو تو فرزند نیاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرسندی وغم هر که زهم باز شناسد</p></div>
<div class="m2"><p>الا به غم عشق تو خرسند نیاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب رخت از دامن البرز نخیزد</p></div>
<div class="m2"><p>کار دلت از قله الوند نیاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچون زنخ و غبغب تو سیب و ترنجی</p></div>
<div class="m2"><p>یک بار ز ساری و دماوند نیاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمشاد و سمن با همه زیبائی و کشی</p></div>
<div class="m2"><p>با طلعت و بالای تو مانند نیاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترکی چو تو دلبند ودلاویز و دلارام</p></div>
<div class="m2"><p>ز ایران چه که از خلخ و خوقند نیاید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نقاش که یا نقش چه کاین صورت مطبوع</p></div>
<div class="m2"><p>غیر از قلم صنع خداوند نیاید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا پسته نوشین تو را قند ثمر شد</p></div>
<div class="m2"><p>در هیچ دهن نام سمرقند نیاید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیم است که مجنون صفت آفاق بگردم</p></div>
<div class="m2"><p>زان طره به پای دلم ار بند نیاید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طاقی به هنر از همه خوبان و ترا جفت</p></div>
<div class="m2"><p>صد دور بدین شیوه هنرمند نیاید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یغما به دهن خاکت اگر زین غزل ازیار</p></div>
<div class="m2"><p>جز بوسه همت جایزه ای چند نیاید</p></div></div>