---
title: >-
    شمارهٔ  ۱۲۲
---
# شمارهٔ  ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>از صومعه زاهد به خرابات سفر کن</p></div>
<div class="m2"><p>طامات صفائی ندهد فکر دگر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آدم به نشاط غمش از گلشن مینو</p></div>
<div class="m2"><p>بگذشت تو هم گر خلفی کار پدر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا در ره او پای کند پویه قدم زن</p></div>
<div class="m2"><p>تا بر در او دست دهد خاک به سر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید که به گوشش رسی ای ناله رسا شو</p></div>
<div class="m2"><p>باشد که ترحم کند ای آه اثر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خندم شب هجران چو شب وصل مگر چرخ</p></div>
<div class="m2"><p>رشک آرد و گوید به شب آغاز سحر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشکت بخراشد جگر مردم و ترسم</p></div>
<div class="m2"><p>غمگین شود ای مردمک دیده حذر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهی به سلامت گذری از نظر دوست</p></div>
<div class="m2"><p>یغما تن و جان را هدف تیر نظر کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خشنودی مفتی و مریدان نظر شیخ</p></div>
<div class="m2"><p>یغما خری اندر وحل افتاد خبر کن</p></div></div>