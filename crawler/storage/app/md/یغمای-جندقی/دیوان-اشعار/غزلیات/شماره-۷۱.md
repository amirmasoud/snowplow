---
title: >-
    شمارهٔ  ۷۱
---
# شمارهٔ  ۷۱

<div class="b" id="bn1"><div class="m1"><p>گرچه دانم ره عشق تو به سر می‌نرود</p></div>
<div class="m2"><p>می‌روم زان که دلم راه دگر می‌نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم از صبح شب هجر چنان شد نومید</p></div>
<div class="m2"><p>کِش به غفلت به زبان نام سحر می‌نرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته‌ام از لب شیرین تو روزی سخنی</p></div>
<div class="m2"><p>همچنان از دهنم طعم شکر می‌نرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دجله در عهد سرشکم ورق نام بشُست</p></div>
<div class="m2"><p>بحر چون موج زند نام شمر می‌نرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه‌گاهی خودی ای ناله به گوشش برسان</p></div>
<div class="m2"><p>گرچه هرگز به تو امّید اثر می‌نرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده پُرآبم از آن است که یک چشم زدن</p></div>
<div class="m2"><p>آفتاب رخش از پیش نظر می‌نرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم میگون ترا فتنه نخسبد هرگز</p></div>
<div class="m2"><p>مستی از خانه خمار به در می‌نرود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه فسون کرد زلیخا که مه کنعانی</p></div>
<div class="m2"><p>در ضمیرش به غلط یاد پدر می‌نرود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاری از رود کسان خواهم و گوید یعقوب</p></div>
<div class="m2"><p>این گمانی است که در حق پسر می‌نرود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زاهد ار پایه یغمات نه از جای مرو</p></div>
<div class="m2"><p>به مقامی که مسیحا شده خر می‌نرود</p></div></div>