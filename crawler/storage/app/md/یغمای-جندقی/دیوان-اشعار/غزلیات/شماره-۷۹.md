---
title: >-
    شمارهٔ  ۷۹
---
# شمارهٔ  ۷۹

<div class="b" id="bn1"><div class="m1"><p>در بتکده گر خانه ای آباد توان کرد</p></div>
<div class="m2"><p>از کعبه مسلمانم اگر یاد توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آهن دلش از ناله نشد نرم چه حاصل</p></div>
<div class="m2"><p>کز سینه من کوره حداد توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انصاف که تا سینه توان کند به ناخن</p></div>
<div class="m2"><p>در کیش وفا بحث به فرهاد توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر مایه تنعم که ز گلزار شنیدیم</p></div>
<div class="m2"><p>عیشی است که در خانه صیاد توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس تجربه کردیم همان شام اجل بود</p></div>
<div class="m2"><p>در هجر تو روزی که از اویاد توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش خواجگی عشق که صد بنده چو یوسف</p></div>
<div class="m2"><p>شکرانه این بندگی آزاد توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند دلش نرم توان کرد به فریاد</p></div>
<div class="m2"><p>آری بود ار قوت فریاد توان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یغما ز چه آب و گلی آخر که ز خاکت</p></div>
<div class="m2"><p>نه صومعه نه بتکده آباد توان کرد</p></div></div>