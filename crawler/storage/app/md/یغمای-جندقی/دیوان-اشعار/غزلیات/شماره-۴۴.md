---
title: >-
    شمارهٔ  ۴۴
---
# شمارهٔ  ۴۴

<div class="b" id="bn1"><div class="m1"><p>کرده در آیینه حسن رخ خود شیدایت</p></div>
<div class="m2"><p>طره ز آن سلسله‌ها ریخته اندر پایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخت بر بام سموات کشد فتنه اگر</p></div>
<div class="m2"><p>جلوه ناز بدین شیوه کند بالایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمتر از خون مدد دیده کن ای دل ترسم</p></div>
<div class="m2"><p>که به طوفان دهد این قطره دریا زایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشت پایان تو پیدا مگر ای دشت جنون</p></div>
<div class="m2"><p>بر نتابید به رسوائی ما صحرایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخل نوخیز نه بر چوب کند تکیه به پا</p></div>
<div class="m2"><p>تا در آغوش کشم سر و قد رعنایت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آه شب عالمی آسوده زید خون دلم</p></div>
<div class="m2"><p>گر بدین دست کشد چشم قدح پیمایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنه از سر غم زلفش که نبینم یغما</p></div>
<div class="m2"><p>جز پریشانی دل سودی از این سودایت</p></div></div>