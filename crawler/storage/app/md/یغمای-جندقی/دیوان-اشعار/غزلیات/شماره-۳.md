---
title: >-
    شمارهٔ  ۳
---
# شمارهٔ  ۳

<div class="b" id="bn1"><div class="m1"><p>صبا از من بگو بیگانه مشرب آشنائی را</p></div>
<div class="m2"><p>جفا نادیده ارباب وفاکش بی وفائی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روا چون داشتی برداشتن از جان سپاری دل</p></div>
<div class="m2"><p>که باری دل به دست آورده باشی ناروائی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ناز آسوده بر دیبای سلطانی چه غم دارد</p></div>
<div class="m2"><p>بود گر خار و خارا بستر و بالین گدائی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چین سر زلف تو را مشک ختن گفتم</p></div>
<div class="m2"><p>پریشانم خطا شد در گذر از من خطائی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم بر خیل مژگانش زد آوخ تا چه پیش آید</p></div>
<div class="m2"><p>تن تنها میان لشکری بی دست و پائی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طفیل خود شمار ندم گدایان سر کویش</p></div>
<div class="m2"><p>مگر افتاد بر من سایه دولت همائی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن از ناله در این کاروان ای ساربان منعم</p></div>
<div class="m2"><p>چه سودت از هزاران گر زبان بندی درائی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فکندم پنجه یغما گر چه می دانم نمی آرم</p></div>
<div class="m2"><p>بدین سرپنجه گفتن پنجه زور آزمائی را</p></div></div>