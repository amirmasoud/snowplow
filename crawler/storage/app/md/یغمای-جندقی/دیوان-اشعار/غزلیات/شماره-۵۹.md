---
title: >-
    شمارهٔ  ۵۹
---
# شمارهٔ  ۵۹

<div class="b" id="bn1"><div class="m1"><p>آن مرغ که جز در چمن آرام ندارد</p></div>
<div class="m2"><p>پیداست که مسکین خبر از دام ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کافر نکند آنچه کند چشم تو با خلق</p></div>
<div class="m2"><p>این ترک سپاهی مگر اسلام ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آغاز مکن راه غم عشق که صد ره</p></div>
<div class="m2"><p>من رفتم وباز آمدم انجام ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط آمد و کار از کف زلفش ستد ای دل</p></div>
<div class="m2"><p>بگریز که نو سلسله فرجام ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن نیست که ارباب ریا باده ننوشند</p></div>
<div class="m2"><p>هر ننگ که در صومعه شد نام ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با روز وصالت ز شب هجر نترسم</p></div>
<div class="m2"><p>این صبح بهشت است و ز پی شام ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنرا که چو جم دست دهد جام صبوحی</p></div>
<div class="m2"><p>دارای عراق است اگر شام ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محمود غلامی است اگر عشق نورزد</p></div>
<div class="m2"><p>جمشید گدائی است اگر جام ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باخاک درت فرقی اگر هست فلک را</p></div>
<div class="m2"><p>این است که ماهی چو تو بر بام ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آن روز که در گردش جام آمده یغما</p></div>
<div class="m2"><p>دیگر غمی از گردش ایام ندارد</p></div></div>