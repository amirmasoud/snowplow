---
title: >-
    شمارهٔ  ۱۲۹
---
# شمارهٔ  ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>تا دم معجز از آن لعل شکرخا زده‌ای</p></div>
<div class="m2"><p>سنگ بر شیشه ناموس مسیحا زده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه دل است اینکه تو داری که یکی سنگ سیاه</p></div>
<div class="m2"><p>به کف آورده و بر شیشه دل‌ها زده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانی احوال دلم با دل سنگین بتان</p></div>
<div class="m2"><p>به مثل وقتی اگر شیشه به خارا زده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور آن دور غم و گردش ای گردش کام</p></div>
<div class="m2"><p>به چه نسبت مثل چرخ به مینا زده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک چشم ار کندم ملک دل اینگونه خراب</p></div>
<div class="m2"><p>تا زنی چشم به هم خیمه به صحرا زده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردم و صید چه در شهر و چه در دشت نماند</p></div>
<div class="m2"><p>آستین تا ز پی خون که بالا زده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده‌ام دفتر پیمان ترا فرد به فرد</p></div>
<div class="m2"><p>هرکجا حرف وفا آمده منها زده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرده‌ای دجله گمان اشک مرا، چشم بمال</p></div>
<div class="m2"><p>تا ببینی مثل قطره به دریا زده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی ای سرو که خاک ره بالای ویم</p></div>
<div class="m2"><p>قدمی نیز فرود آی که بالا زده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شکست دل احباب سپه می‌رانی</p></div>
<div class="m2"><p>آنچنان خوش که مگر بر صف اعدا زده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زده‌ای دست به پیمانه شکستن زاهد</p></div>
<div class="m2"><p>خبرت نیست که بر عمر ابد پا زده‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه دل داده و دل برده ندانم جز دوست</p></div>
<div class="m2"><p>تهمت است اینکه تو بر وامق و عذرا زده‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با رقیبان زده‌ای تا زده‌ای باده مهر</p></div>
<div class="m2"><p>سنگ کین تا زده بر ساغر یغما زده‌ای</p></div></div>