---
title: >-
    شمارهٔ  ۹۴
---
# شمارهٔ  ۹۴

<div class="b" id="bn1"><div class="m1"><p>یار نامد به سرم تا به لبم جان نرسید</p></div>
<div class="m2"><p>بهتر آن بود که این درد به درمان نرسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک قدم نیست فزون مرحله عشق و عجب</p></div>
<div class="m2"><p>راه چندانکه بریدیم به پایان نرسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترک من تاختن از اصل نداند که به صید</p></div>
<div class="m2"><p>بارها رفت ولی کار به جولان نرسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر جهان گوشه دامان مرا منت هاست</p></div>
<div class="m2"><p>که به تدبیر وی این قطره به طوفان نرسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بارها برد دل از طره به چشمش فریاد</p></div>
<div class="m2"><p>شحنه کفر به فریاد مسلمان نرسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ز زلف تو به لعل تو رسد کیست که کرد</p></div>
<div class="m2"><p>طی ظلمات و به سرچشمه حیوان نرسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چه آب و گلی ای میکده یا رب که سرم</p></div>
<div class="m2"><p>تا نشد خاک به راه تو به سامان نرسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خطت سر نسپارم چه نشاط از حرم است</p></div>
<div class="m2"><p>هر که را سرزنش از خار مغیلان نرسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم آن سوخته مرغ همه حسرت یغما</p></div>
<div class="m2"><p>کآشین ماند و شد از دام به بستان نرسید</p></div></div>