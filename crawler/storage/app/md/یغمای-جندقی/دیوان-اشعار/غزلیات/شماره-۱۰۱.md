---
title: >-
    شمارهٔ  ۱۰۱
---
# شمارهٔ  ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>دل بر آن طره چه سود ار ز خطم بستی باز</p></div>
<div class="m2"><p>مرغ پر ریخته را رشته چه کوته چه دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزی ای صعود دلشاد که بالت بستم</p></div>
<div class="m2"><p>به کمندی که پر مرغ حرم آمده باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم ای شیخ چرا این همه شاهد بازی</p></div>
<div class="m2"><p>گفت در شرع بود مرد خدا شاهد باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر از زلف و زنخدان بتی افتادم</p></div>
<div class="m2"><p>از فرازی به نشینی که ندیده است فراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت زاهد به ره دین تو نیائی با من</p></div>
<div class="m2"><p>خاک بر فرق مسیحی که ز خر ماند باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانه خال مگو گندم آدم خواره</p></div>
<div class="m2"><p>سنبل زلف مخوان خوشه خرمن پرداز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سجده یغما بر آن بت چه بر ابرو و چه ذقن</p></div>
<div class="m2"><p>روی در کعبه بهر رکن صحیح است نماز</p></div></div>