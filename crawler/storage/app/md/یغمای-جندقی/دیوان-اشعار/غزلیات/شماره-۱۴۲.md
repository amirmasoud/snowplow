---
title: >-
    شمارهٔ  ۱۴۲
---
# شمارهٔ  ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>زاهدم می دهد از سلسله زلف تو پندی</p></div>
<div class="m2"><p>کاش می داشتم از سلسله زلف تو بندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر به پای تو تن انداخت برانگیز سمندی</p></div>
<div class="m2"><p>تن به فتراک تو سر داد بینداز کمندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه بی پرده خوشی روی فرو پوش که ترسم</p></div>
<div class="m2"><p>بیند از دیده بد روی نکوی تو گزندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان بها دادم و کامم نشد از وصل تو حاصل</p></div>
<div class="m2"><p>آخر ای جان چه متاعی مگر ای بوسه به چندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل دیوانه به زنجیر ادب می نپذیرد</p></div>
<div class="m2"><p>مگرش بر نهم از سلسله زلف تو بندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنده کردی که به تیغم زده بر خاک فکندی</p></div>
<div class="m2"><p>لیک می میرم از این غم که به فتراک نبندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پست کردی به رهش عاقبتم عذر چه گویم</p></div>
<div class="m2"><p>تا بدین پایه ندانستمت ای بخت بلندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوده خال تو آخر ز جهان دود بر آورد</p></div>
<div class="m2"><p>وه عجب آتشی افتاد به عالم ز سپندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشنا شد به تو بیگانه و در خویش نگنجد</p></div>
<div class="m2"><p>زین نشاطم دل غمگین که تو بیگانه پسندی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نالدم دل چو جرس از غم محرومی محمل</p></div>
<div class="m2"><p>ورنه ای خار حریری تو و ای خاره پرندی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک قلم خاصه به وصف لب شیرین نکویان</p></div>
<div class="m2"><p>شهد باری مگر ای خامه یغما نی قندی</p></div></div>