---
title: >-
    شمارهٔ  ۱۱۸
---
# شمارهٔ  ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>تا ز مینای غم عشق تو صهبا زده‌ام</p></div>
<div class="m2"><p>عقل را شیشه ناموس به خارا زده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پی میمنت عیش مرا تیغ شراب</p></div>
<div class="m2"><p>تا به دست آمده بر گردن تقوا زده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می مسیحا و من غم‌زده رنجور مکن</p></div>
<div class="m2"><p>عیب اگر دست به دامان مسیحا زده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاکل و زلف بتان دوده جور و ستمند</p></div>
<div class="m2"><p>به تظلم در این سلسله شب‌ها زده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمان چند مرا شیشه دل می‌شکنی</p></div>
<div class="m2"><p>شرمی آخر مگرت سنگ به مینا زده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با لب و قد تو انصاف ز کوته‌نظری است</p></div>
<div class="m2"><p>گر به غفلت مثل کوثر و طوبی زده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبقتم بی‌جهتی نیست ز ارباب سخن</p></div>
<div class="m2"><p>دو سه روزی است که گام از پی یغما زده‌ام</p></div></div>