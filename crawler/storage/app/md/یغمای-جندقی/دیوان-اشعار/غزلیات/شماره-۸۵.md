---
title: >-
    شمارهٔ  ۸۵
---
# شمارهٔ  ۸۵

<div class="b" id="bn1"><div class="m1"><p>سر زهی دولت اگر در قدمت خاک آید</p></div>
<div class="m2"><p>دولتی دیگر اگر بسته فتراک آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان روز ز خورشید بر افروخت چراغ</p></div>
<div class="m2"><p>بسکه از آتش من دود بر افلاک آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده مسواک فقیه از پی تطهیر دهان</p></div>
<div class="m2"><p>آنچه او خورده کجا پاک به مسواک آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم آن روز که آن زلف معقرب دیدم</p></div>
<div class="m2"><p>آنقدر خون خورد این مارکه ضحاک آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه ز اندیشه غرق است مرا بیم سرشک</p></div>
<div class="m2"><p>ترسم از چهره غبار در او پاک آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنقدر می خورم امروز که چون خاک شوم</p></div>
<div class="m2"><p>هر گیاهی که ز خاکم بدمد تاک آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جامه ها کرده قبا عشق مه کنعان را</p></div>
<div class="m2"><p>عجبی نیست اگر پیرهنی چاک آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مفتیم رقعه به خود داد خدایا مپسند</p></div>
<div class="m2"><p>نایب مسند شرع این همه سفاک آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر یغما چو لگدکوب اجل خواهد شد</p></div>
<div class="m2"><p>به که خاک ره آن قامت چالاک آید</p></div></div>