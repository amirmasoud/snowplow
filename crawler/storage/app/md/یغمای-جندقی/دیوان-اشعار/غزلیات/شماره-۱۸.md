---
title: >-
    شمارهٔ  ۱۸
---
# شمارهٔ  ۱۸

<div class="b" id="bn1"><div class="m1"><p>مفتی شهر ندارد چوبه میخانه ما</p></div>
<div class="m2"><p>باری از رشک زند سنگ به پیمانه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت پرستند مقیمان حرم می ترسم</p></div>
<div class="m2"><p>تا ز زنار شود سبحه صد دانه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کردم از باده تهی خمکده ها لیک هنوز</p></div>
<div class="m2"><p>نشنیده است کسی ناله مستانه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چه افتاد که سجاده به محراب افکند</p></div>
<div class="m2"><p>آنکه صد خرقه گرو داشت به میخانه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود پرستی کم از اصنام نه تا حکمت چیست</p></div>
<div class="m2"><p>کآشنایان حقیقی شده بیگانه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شادزی ای دل دیوانه که اندر همه شهر</p></div>
<div class="m2"><p>نیست طفلی که نداند ره کاشانه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه یغما نکنم قصه ولی شوق وطن</p></div>
<div class="m2"><p>می‌توان یافت ز فریاد غریبانه ما</p></div></div>