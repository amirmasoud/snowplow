---
title: >-
    شمارهٔ  ۱۴
---
# شمارهٔ  ۱۴

<div class="b" id="bn1"><div class="m1"><p>هرگز مباد کوثر و جنت هوس مرا</p></div>
<div class="m2"><p>جام شراب و گوشه میخانه بس مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی به کنج صومعه ام ذکر سبحه چیست</p></div>
<div class="m2"><p>ای کاش برده بود به زندان عسس مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هامون چه پویم از پی محمل که می رسد</p></div>
<div class="m2"><p>از راه دل به گوش، صدای جرس مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ننگ آیدم ز ظل هما گرچه چرخ دون</p></div>
<div class="m2"><p>می پرورد به سایه بال مگس مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر ز سخت گیری صیاد و باغبان</p></div>
<div class="m2"><p>پر ریخت در میانه باغ و قفس مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت آیمت به سر دم مردن فغان که گشت</p></div>
<div class="m2"><p>آغاز وعده حسرت آخر نفس مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین پس به کنج صومعه نوشم شراب امن</p></div>
<div class="m2"><p>کانجا بدین لباس نگیرد عسس مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم به کوی دوست پی از گریه گم کنم</p></div>
<div class="m2"><p>طوفان اشک بست ره از پیش و پس مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنمودمی حقیقت آب بقا به خضر</p></div>
<div class="m2"><p>بودی به خاک پای تو گر دست رس مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یغما خوشم به خرقه که عمری در این لباس</p></div>
<div class="m2"><p>بودم شراب خواره و نشناخت کس مرا</p></div></div>