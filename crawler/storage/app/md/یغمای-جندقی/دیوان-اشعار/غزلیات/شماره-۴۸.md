---
title: >-
    شمارهٔ  ۴۸
---
# شمارهٔ  ۴۸

<div class="b" id="bn1"><div class="m1"><p>از قد و رخسار و لب طوبی و خلد و کوثر است</p></div>
<div class="m2"><p>یا رب این بستان مینو یا بهشت دیگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور ساقی متفق از دور گردون خوشتر است</p></div>
<div class="m2"><p>کآفتاب و ماه چرخ او شراب و ساغر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نام خشت خم مبر زاهد که بر نارم گرفت</p></div>
<div class="m2"><p>سر از این بالین اگر دانم که خاکم بستر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر گدائی جام می دارد به کف در کیش من</p></div>
<div class="m2"><p>هست سلطانی که هم جمشید و هم اسکندر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر کنار عارضش در زیر زلف آن خال نیست</p></div>
<div class="m2"><p>هندوئی با سلسله در آفتاب محشر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم و مژگان و نگاهت چیست می دانی بهم</p></div>
<div class="m2"><p>ترک سر مستی به دستی جام و دستی خنجر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیضه دولت به زیر بال بینم تا مرا</p></div>
<div class="m2"><p>سایه پر همای مهر آقا بر سر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فخر اشراف بشر سید فلان کز روی مجد</p></div>
<div class="m2"><p>آستانش را فلک با آن علو خاک در است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه در انگشت حکم خاتم توقیع او</p></div>
<div class="m2"><p>حلقه نه آسمان چون حلقه انگشتر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>او سپهسالار و اصناف افاضل لشکرند</p></div>
<div class="m2"><p>اوست شاهنشاه و اقطاع شریعت کشور است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فربه آهوئی که در عرف لغت ملک است و مال</p></div>
<div class="m2"><p>در به چشم شیر استغناش صیدی لاغر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بندگان پرور خداوندا بپرس از عمرو و زید</p></div>
<div class="m2"><p>ور نمی پرسی بگویم از منت گر باور است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تاکنون کم سی گذشت از روزگار شاعری</p></div>
<div class="m2"><p>کافرم یک حرف اگر مدح کسم در دفتر است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شعرها دارم به گوهر رشک لولوی خوشاب</p></div>
<div class="m2"><p>لیک وصف باده لعلی و لعل دلبر است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر که بندد بر میان تیغی نگویم کآن علی است</p></div>
<div class="m2"><p>هر که پوشد خرقه ای نسرایم این پیغمبر است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون توئی را محمدت زیبد که از روی نسب</p></div>
<div class="m2"><p>هم حسب از این دو دریای سعادت گوهر است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حق مدحت زآن نمی گویم که ترسم مسلمین</p></div>
<div class="m2"><p>بر سر بازارها گویند یغما کافر است</p></div></div>