---
title: >-
    شمارهٔ  ۱۳۴
---
# شمارهٔ  ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>گرنه فدای آن سری ور نه به پای آن تنی</p></div>
<div class="m2"><p>دل نه که سنگ پهلوئی سر نه که بار گردنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قبله دیر و مسجدی، کعبه زهد و زاهدی</p></div>
<div class="m2"><p>غارت دین و دانشی، فتنه کوی و برزنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر به صلاح دشمنان، طره و ابروی و مژه</p></div>
<div class="m2"><p>در به مصاف دوستان، تیر و کمان و جوشنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدق و ارادت مرا این دو گواه عدل بس</p></div>
<div class="m2"><p>کز همه با تو دوستم و از همه ام تو دشمنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر رخ تست مرد و زن فتنه کزان خط و ذقن</p></div>
<div class="m2"><p>رهزن صد منیژه و چاه هزار بیژنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راست به قدو زلف و خط، صاف به چهر و کتف و لب</p></div>
<div class="m2"><p>کشمر و چین وتبتی، خلخ و مصر و ارمنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از مژه و ابروی بتان بر سر و تیغ و خنجرم</p></div>
<div class="m2"><p>لیک ترا چه کت بپا در نخلیده سوزنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یغما از مجاهدت کی کشی آستین او</p></div>
<div class="m2"><p>بیهده بر در طلب دست هزار دامنی</p></div></div>