---
title: >-
    شمارهٔ  ۹۰
---
# شمارهٔ  ۹۰

<div class="b" id="bn1"><div class="m1"><p>جز آفتاب تو و آن غنچه شراب آلود</p></div>
<div class="m2"><p>که دیده باده بی درد و آتش بی دود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فراز سرو تو یک نیزه آفتاب جمال</p></div>
<div class="m2"><p>هزار کوکب بخت است و کوکب مسعود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن ز یوسف گل ران و داستان هزار</p></div>
<div class="m2"><p>چه جای بزم سلیمان و نغمه داود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیار منطق شیرین بتاب نافه زلف</p></div>
<div class="m2"><p>بساز پرده بربط، بسوز مجمر عود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا میان و دهان هم نهفته هم پیداست</p></div>
<div class="m2"><p>زهی شگفت که با هم شنید غیب و شهود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر قدح بگشا در ببند بر مه و مهر</p></div>
<div class="m2"><p>مخواه خوشتر از این از ستاره بست و گشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بساط باغ شود دیده ای که روی تو دید</p></div>
<div class="m2"><p>نشاط باده فزاید لبی که لعل تو سود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوای سرخ گلت از فزایش خط سبز</p></div>
<div class="m2"><p>به عمر در نشنیدم زیان فزاید سود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه زان میان ودهان من شدم بباد که ریخت</p></div>
<div class="m2"><p>به خاک این دو عدم خون صد هزار وجود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خاکپای غلامانت ساید ار رخ زلف</p></div>
<div class="m2"><p>سر ایاز ببرم به دامن محمود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز آن دهان و دل و اشک چشم والیه نیست</p></div>
<div class="m2"><p>ز لاله گر بدمد سنگ و قطره زاید رود</p></div></div>