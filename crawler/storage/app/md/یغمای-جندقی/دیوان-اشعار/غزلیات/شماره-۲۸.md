---
title: >-
    شمارهٔ  ۲۸
---
# شمارهٔ  ۲۸

<div class="b" id="bn1"><div class="m1"><p>خضر پنداری نهانی کرده قدری می در آب</p></div>
<div class="m2"><p>ورنه کی بودی بقای زندگانی کی در آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیستم ماهی، سمندر نیز، لیک از چشم ودل</p></div>
<div class="m2"><p>رفت ایامم در آتش، گشت عمرم طی در آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم چشم مرا گرخانه ویران شد چه شد</p></div>
<div class="m2"><p>دیر کی پاید بنائی را که باشد پی در آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حالم ای همدم مپرس از آه سرد و موج اشک</p></div>
<div class="m2"><p>خود چه باشد حال مسکینی که باشد وی در آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه و اشک من اگر بر کوه و وادی بگذرد</p></div>
<div class="m2"><p>کوه خون گرید بقم روید بجای نی در آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم نخورد ار چشم لیلی بر دل مجنون نسوخت</p></div>
<div class="m2"><p>نجد بگذارد در آتش غرق گردد حی در آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کمر در آبم از تردامنی ساقی بده</p></div>
<div class="m2"><p>آب آتشگون که لطفی تازه دارد می در آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شه بر آب چشم مظلومان نبخشد ور نه من</p></div>
<div class="m2"><p>صد ره از اشک تظلم غرق کردم ری در آب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غیر گو خوش زی که با یغما در آن کوه زآه و اشک</p></div>
<div class="m2"><p>هم من افتادم در آتش، غرق شد هم وی در آب</p></div></div>