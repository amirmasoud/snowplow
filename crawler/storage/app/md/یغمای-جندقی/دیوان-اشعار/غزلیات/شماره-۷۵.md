---
title: >-
    شمارهٔ  ۷۵
---
# شمارهٔ  ۷۵

<div class="b" id="bn1"><div class="m1"><p>هدف تیر نظر چون دل اغیار کند</p></div>
<div class="m2"><p>همه پیکان جهان بر دل من کار کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر از این دست دهد آن بت ترسابچه می</p></div>
<div class="m2"><p>ای بسا صوف سفید اطلس گلنار کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم رود حرف به هشیاری مستان ریا</p></div>
<div class="m2"><p>ایزد ار صومعه را خانه خمار کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشت نزدیک دو راه حرم و دیر ای کاش</p></div>
<div class="m2"><p>خضر توفیق مرا قافله سالار کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معنی عزت ما خاک نشینان دانی</p></div>
<div class="m2"><p>بر سر کوی کسی عشقت اگر خوار کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه شد ار کرد به زنار بدل سبحه ما</p></div>
<div class="m2"><p>آری آن زلف از این شعبده بسیار کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط کجا زلف کجا هر سپه آرا مردی</p></div>
<div class="m2"><p>نشود رستم اگر خیمه زنگار کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا ز خود بی خبر افتم ز تماشا قدریم</p></div>
<div class="m2"><p>پیشتر ز آمدن خویش خبردار کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اتحادی است میان من و غیرت که خدنگ</p></div>
<div class="m2"><p>بر تن او چو زنی بر دل من کار کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من وبرگردن از آن چاه ذقن بیژن دل</p></div>
<div class="m2"><p>رستم عهدم اگر جهد من این کار کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که را خاتمه بر عافیت و آزادی است</p></div>
<div class="m2"><p>بخت نیکش به کمند تو گرفتار کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن خداوند که از بندگی خاک درش</p></div>
<div class="m2"><p>چرخ زیبد به خداوندیم اقرار کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فهم اسرار دهانش مکن اندیشه که وهم</p></div>
<div class="m2"><p>حل این نکته سربسته به پندار کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به خدا مفتی اگر بنده خویشت خوانم</p></div>
<div class="m2"><p>گر دو عالم به خداوندیت اقرار کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه عجب جو شدم ار خون دل از هربن موی</p></div>
<div class="m2"><p>سیل چون گرد شود رخنه به دیوار کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گویدم سجده اصنام مکن زاهد بین</p></div>
<div class="m2"><p>که به بیدینی خود نزد من اقرار کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هوشیاران شب آدینه به مستان نکنند</p></div>
<div class="m2"><p>آنچه مستان تو با مردم هشیار کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنکه از ما به خداوندیت اقرار گرفت</p></div>
<div class="m2"><p>چشم دارم که تو را بنده نگهدار کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آه یغما نکند در دل سنگین تو کار</p></div>
<div class="m2"><p>اگر آشت ز دل خاره پدیدار کند</p></div></div>