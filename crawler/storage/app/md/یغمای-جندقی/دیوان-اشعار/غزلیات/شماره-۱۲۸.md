---
title: >-
    شمارهٔ  ۱۲۸
---
# شمارهٔ  ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>مکن ای فقیه منعم ز حدیث جام و باده</p></div>
<div class="m2"><p>که حرام کرده می را برو ای حلال زاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زگشاد و بست اختر شبی آن بود که بینم</p></div>
<div class="m2"><p>در خانقاه بسته، سر جام می گشاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رهی که نعل ریزد ز تحیر اولین پی</p></div>
<div class="m2"><p>همه اسب شهسواران به کجا رسم پیاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زند آن چنان زبانه تب غم ز بند بندم</p></div>
<div class="m2"><p>که گمان برند مردم به نی آتش اوفتاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه خط است و خال و مویش بر آفتاب رویش</p></div>
<div class="m2"><p>که صفی گناه کاران به قیامت ایستاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ره تو خاکم اما به سرم کجا گذاری</p></div>
<div class="m2"><p>مگر آن زمان که دانی فلکم به باد داده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل تیره روز یغما به شکنج زلف او بین</p></div>
<div class="m2"><p>تو به شاخ سرو گوئی زغن آشیان نهاده</p></div></div>