---
title: >-
    شمارهٔ  ۱۰۲
---
# شمارهٔ  ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>نشئه می نغمه نی دیگران را خوش که بس</p></div>
<div class="m2"><p>نفس ما را خون دل دمساز و افغان هم نفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد سوالت جز بدان کش کس... نیست</p></div>
<div class="m2"><p>می نیاید یک جواب از هیچ سو وز هیچ کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طایری کز بیضه در دام اوفتد ناید شگفت</p></div>
<div class="m2"><p>گر نداند باز قید از آشیان دام از قفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در طریق رهروی آن راست پیشی کاوفتاد</p></div>
<div class="m2"><p>بر به مقصد پشت از این سرگشتگان گامی دو پس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق نهراسد همی از عقل و مشتاق از رقیب</p></div>
<div class="m2"><p>برق نندیشد ز خار آتش نپرهیزد ز خس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حوزه اسلام اگر این است و تسبیح نجات</p></div>
<div class="m2"><p>ای دریغا حلقه زنجیر و زندان عسس</p></div></div>