---
title: >-
    شمارهٔ  ۳۹
---
# شمارهٔ  ۳۹

<div class="b" id="bn1"><div class="m1"><p>در گهر غیرت هفتاد گرامی پسر است</p></div>
<div class="m2"><p>آن پری دخت که مامش خم و تاکش پدر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا میان در به صلیب است وسبوبر به سراست</p></div>
<div class="m2"><p>مر مرا تاج ز خورشید و ز جوزا کمر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهر از آن اختر عقرب سپر آزرده و من</p></div>
<div class="m2"><p>رنجه ز آن عقرب شبرنگ که اختر سپر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر به زین اندرش آن گونه آذرگون بین</p></div>
<div class="m2"><p>نتوان گفت همی آذر برزین دگر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آبگین گونه ز آهم همه آژنگ آری</p></div>
<div class="m2"><p>این نه آهست و نه آئینه شمال و شمر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتنه دیدی ثمر تیر و کمان وین نه شگفت</p></div>
<div class="m2"><p>فتنه بنگر که همی تیر و کمانش ثمر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد نوشین لبش ار خط معقرب بدمید</p></div>
<div class="m2"><p>کی شگفت آرم جراره نتیجه شکر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز تو کت نیست میان نقص ندیدم که کمال</p></div>
<div class="m2"><p>جز تو کت نیست دهان عیب ندیدم هنر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بید مجنون را مانی تو بدین پیکر و زلف</p></div>
<div class="m2"><p>بید مجنونی کورا دل و دین برگ و بر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر برم نام بدان ابروی و آن کاکل و زلف</p></div>
<div class="m2"><p>شرف دولت شمشیر و کلاه وکمر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فیض عارض اگر این فتنه قامت اگر آن</p></div>
<div class="m2"><p>خاک فردوس هبا خون قیامت هدر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخت تر شد دلش از آتش آهم یغما</p></div>
<div class="m2"><p>گفت شعری شرر از سنگ نه سنگ از شرراست</p></div></div>