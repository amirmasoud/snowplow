---
title: >-
    شمارهٔ  ۴۱
---
# شمارهٔ  ۴۱

<div class="b" id="bn1"><div class="m1"><p>به بزم یار همدم گر چه جان است</p></div>
<div class="m2"><p>حضور غیر بر عاشق گران است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هلاکم کرد و از هجرم بر آسود</p></div>
<div class="m2"><p>که می گوید اجل نامهربان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جرس امشب ننالد چون شب دوش</p></div>
<div class="m2"><p>همانا لیلی اندر کاروان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نماند بلبلان را ذوق فریاد</p></div>
<div class="m2"><p>در آن گلشن که گلچین باغبان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گردون زان نمی نالم که ما را</p></div>
<div class="m2"><p>شکایت هر چه هست از آسمان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان مشغول صیادم که گوئی</p></div>
<div class="m2"><p>مرا گلشن قفس دام آشیان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نسیم رحمت آید بر مشامم</p></div>
<div class="m2"><p>مگر این راه بر دیر مغان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لبش گر نیست آب زندگانی</p></div>
<div class="m2"><p>چرا پس در سواد خط نهان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندانم آن که می گوید سخن کیست</p></div>
<div class="m2"><p>همی دانم که یغما ترجمان است</p></div></div>