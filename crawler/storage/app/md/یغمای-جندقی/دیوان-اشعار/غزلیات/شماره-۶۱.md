---
title: >-
    شمارهٔ  ۶۱
---
# شمارهٔ  ۶۱

<div class="b" id="bn1"><div class="m1"><p>از عمر نمانده است مرا غیر دمی چند</p></div>
<div class="m2"><p>وقت است اگر رنجه نمائی قدمی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگشای از آن طره پرتاب خمی بند</p></div>
<div class="m2"><p>آزاد کن از زحمت مرغان حرمی چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دیده به گل ریخته ام تخم امیدی</p></div>
<div class="m2"><p>دارم ز تو ای ابر عطا چشم نمی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیم قفسی مژده وصلی بده آخر</p></div>
<div class="m2"><p>تا شاد کنم خاطر خود را به غمی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین بیش به حرمان نتوان زیست خدا را</p></div>
<div class="m2"><p>شایستگی لطف ندارم سمتی چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه خال و خط و کاکل و زلفست که حسنش</p></div>
<div class="m2"><p>آورده پی کشتن یغما رقمی چند</p></div></div>