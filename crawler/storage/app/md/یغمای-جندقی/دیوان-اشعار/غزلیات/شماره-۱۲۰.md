---
title: >-
    شمارهٔ  ۱۲۰
---
# شمارهٔ  ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>چو چشمت ترک مستی در کمینم</p></div>
<div class="m2"><p>چه سود ار بگذرد اختر زکینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهان زین چشم طوفان زا به مردم</p></div>
<div class="m2"><p>چه منت ها که دارد آستینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکم فولاد بازو پنجه بر تافت</p></div>
<div class="m2"><p>دلی باید از این پس آهنینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و اندیشه لعلت که خوشتر</p></div>
<div class="m2"><p>زصد ملک سلیمان این نگینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه طرف از کفر بستم نی زاسلام</p></div>
<div class="m2"><p>هم از آن توبه باید هم ز اینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن میدان که از هر سوست زنهار</p></div>
<div class="m2"><p>کسی نشنیده غیر از آفرینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شدم در رهگذر تو سنت خاک</p></div>
<div class="m2"><p>نکوشد کآسمان زد بر زمینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهی دولت اگر هندوی زلفت</p></div>
<div class="m2"><p>کشد داغ غلامی بر جبینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گزاف از من نیاید کفر زلفش</p></div>
<div class="m2"><p>اگر این است یغما وای دینم</p></div></div>