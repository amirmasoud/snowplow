---
title: >-
    شمارهٔ  ۱۰۴
---
# شمارهٔ  ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>نه آن دیبای گلناری است بر سرو خرامانش</p></div>
<div class="m2"><p>که دست خون ناحق کشتگان بگرفته دامانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپاه خط مگر بر کشور حسنش شبیخون زد</p></div>
<div class="m2"><p>که بر گیسو شکست افتاد و بر گردید مژگانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سراپا خاکیان مستند یا مخمور پنداری</p></div>
<div class="m2"><p>بنای آدم از لای ته خم بود بنیانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میارا بر به مشکین طره ترسم ظالمی گوید</p></div>
<div class="m2"><p>که بگرفته است دود آه مظلومان گریبانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وجودم هندوی خال غلامی شد که می روید</p></div>
<div class="m2"><p>به جای سبزه خط یوسف از چاه زنخدانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه زاهد بهر پاس دین ننوشد می از آن ترسد</p></div>
<div class="m2"><p>که گردد آشکارا وقت مستی کفر پنهانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز می تائب شد اما پاس عهد توبه کی دارد</p></div>
<div class="m2"><p>لب یغما که با پیمانه عمری بود پیمانش</p></div></div>