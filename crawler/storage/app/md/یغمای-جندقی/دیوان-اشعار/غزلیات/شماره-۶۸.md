---
title: >-
    شمارهٔ  ۶۸
---
# شمارهٔ  ۶۸

<div class="b" id="bn1"><div class="m1"><p>رابطه دل به غمزه راه ندارد</p></div>
<div class="m2"><p>کشور ما تاب این سپاه ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به نگاهش مده که ترک سپاهی</p></div>
<div class="m2"><p>ملک بگیرد ولی نگاه ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین تن کاهیده در رمد دل سنگش</p></div>
<div class="m2"><p>کوه نگر کاحتمال کاه ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چیست جدا ز آفتاب روی تو روزم</p></div>
<div class="m2"><p>شب ولی آن تیره شب که ماه ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون ملک گر به زیر عرش بریزی</p></div>
<div class="m2"><p>چون تو صنم قاتلی گواه ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وهم بماند در اشتباه دهانت</p></div>
<div class="m2"><p>عقل در این نکته اشتباه ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه به گه احوال دل بپرس ز زلفت</p></div>
<div class="m2"><p>پرسش زندانیان گناه ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر در فقر و فنا به فرگدائی</p></div>
<div class="m2"><p>سلطنتی یافتم که شاه ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوشه دیوار بیخودی مده از دست</p></div>
<div class="m2"><p>گیتی از این امن تر پناه ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که سرت بر نهد به پای ممالیک</p></div>
<div class="m2"><p>خسرو ملک است اگر کلاه ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبر توقع مکن ز دل که نخواهند</p></div>
<div class="m2"><p>باج ز بیچاره ای که آه ندارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نه کم است از سگان کوی تو یغما</p></div>
<div class="m2"><p>از چه در آن آستانه راه ندارد</p></div></div>