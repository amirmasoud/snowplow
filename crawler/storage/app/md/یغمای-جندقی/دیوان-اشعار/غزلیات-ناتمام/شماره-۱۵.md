---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>نشانی تا مرا از استخوان هست</p></div>
<div class="m2"><p>شماری با سگ آن آستان هست</p></div></div>