---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>به ناله تا نفسم آه صبحگاه گرفت</p></div>
<div class="m2"><p>زمین به ناله درآمد زمانه آه گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتوح مملکت دل که صد سپاه نکرد</p></div>
<div class="m2"><p>هجوم نرگس مستت به یک نگاه گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن زمان که خطت گرد بدر هاله کشید</p></div>
<div class="m2"><p>به سینه طشت همی می زنم که ماه گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فقیه گفت که یغما پیاله می گیرد</p></div>
<div class="m2"><p>بلی گرفت و به فرمان پادشاه گرفت</p></div></div>