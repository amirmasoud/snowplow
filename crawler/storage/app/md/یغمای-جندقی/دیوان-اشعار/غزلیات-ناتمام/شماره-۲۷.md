---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>سوی غیری چو خدنگش ز کمان می‌گذرد</p></div>
<div class="m2"><p>ناوک غیرتم از جوشن جان می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس ز زحمت نبرد بهره که از غایت ناز</p></div>
<div class="m2"><p>تا تو شمشیر برآری ز میان می‌گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار در بزم و نمی‌آرمش از بیم نگاه</p></div>
<div class="m2"><p>فصل گل بین که بهارم به خزان می‌گذرد</p></div></div>