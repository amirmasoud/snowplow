---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>به تعب می کشدم چرخ چه شد غمزه یار</p></div>
<div class="m2"><p>که به یک چشم زدن بگذردم کار از کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیش و کم عشق کشنده است چه دجله چه محیط</p></div>
<div class="m2"><p>آب یک نی چو همی بگذرد از سر چه هزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نو به نو تختگه خسرو دوری است دلم</p></div>
<div class="m2"><p>طرح یارب به چه طالع شده این کهنه حصار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا شد آن زلف معقرب زکفم نیش زند</p></div>
<div class="m2"><p>بر تنم هر سر مو چون دم عقرب سر مار</p></div></div>