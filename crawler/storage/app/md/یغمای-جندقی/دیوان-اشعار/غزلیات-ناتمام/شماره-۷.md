---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>فتح ملوک از سپه است ای سپاه خط</p></div>
<div class="m2"><p>چون شد که شاه حسن شکستن ز لشکر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد سینه ام چو پهلوی دارا ز موج خون</p></div>
<div class="m2"><p>و آن دل به جای خویش چو سد سکندر است</p></div></div>