---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ساقی من و صهبا مثل ماهی و آب است</p></div>
<div class="m2"><p>پیش آر شط باده که بغداد خراب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تلخ است مرا عیش که اندر پی یک بوس</p></div>
<div class="m2"><p>امروز میان من و ساقی شکرآب است</p></div></div>