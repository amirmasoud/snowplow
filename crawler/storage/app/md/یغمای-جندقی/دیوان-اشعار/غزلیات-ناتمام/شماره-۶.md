---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ماه چاه نخشب خم در نقاب ساغر است</p></div>
<div class="m2"><p>یا به چینی پرده در خاقان چین را دختر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که شب‌ها آتشم از تاب دل در بستر است</p></div>
<div class="m2"><p>کس نداند کاین منم یا تودهٔ خاکستر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف آن بر روی او یا مجمر اندر آذر است</p></div>
<div class="m2"><p>روی او در موی او یا آذر اندر مجمر است</p></div></div>