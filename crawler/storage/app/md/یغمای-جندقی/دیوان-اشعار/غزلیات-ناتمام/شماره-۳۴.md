---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>دانه خالت مرا شد دام هوش</p></div>
<div class="m2"><p>آه از این گندم‌نمای جوفروش</p></div></div>