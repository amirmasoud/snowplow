---
title: >-
    شمارهٔ  ۱
---
# شمارهٔ  ۱

<div class="b" id="bn1"><div class="m1"><p>ببرد شوق رخت صبر از دلم یارا</p></div>
<div class="m2"><p>خراب کرد غمت خانه شکیبا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چوآفتاب ز مطلع طلوع کرد و گرفت</p></div>
<div class="m2"><p>شعاع پرتو حسنت تمام دنیا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز دست تو نوشم ایاغ از زهری</p></div>
<div class="m2"><p>کند معالجه درد جمله اعضا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر تو عارض زیبای خویش را بینی</p></div>
<div class="m2"><p>تو خود به شرح دهی حال خسته ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر به کشتن من بسته ای دریغ مدار</p></div>
<div class="m2"><p>بریز خون مرا، دارم این تمنا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زیر تیغ تو گویم که دی مبارک باد</p></div>
<div class="m2"><p>به خون من که حنا بسته‌ای کف پا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوشت کلک قضا نامه رخ یوسف</p></div>
<div class="m2"><p>به شهر مصر که رسوا کند زلیخا را</p></div></div>