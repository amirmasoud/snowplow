---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>تا بو مگر به سنگدلی مهربان کنند</p></div>
<div class="m2"><p>سنگین دلت که سنگ به سنگ امتحان کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم ز مردمی به در آیم به جلد سگ</p></div>
<div class="m2"><p>وانگه در آستان توام پاسبان کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر و رقیب و مدعی آنگه به اتفاق</p></div>
<div class="m2"><p>روزی شبی گذار بر آن آستان کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند خردسال جوانان میکده</p></div>
<div class="m2"><p>پیران سال خورده به جامی جوان کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من پیر سال خورده جوانان خردسال</p></div>
<div class="m2"><p>از یک پیاله کاش مرا امتحان کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>...</p></div>
<div class="m2"><p>زاغان که بر گل از خس و خار آشیان کنند</p></div></div>