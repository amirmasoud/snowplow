---
title: >-
    شمارهٔ  ۳۶ -  به میرزا احمد صفائی نگاشته
---
# شمارهٔ  ۳۶ -  به میرزا احمد صفائی نگاشته

<div class="n" id="bn1"><p>احمد، محمد علی استیفای خاکبوس خداوند طوس فرمود. ظهر بیست و یکم است. رنج جسمانی نیست. از غره شوال تا پنجم ماه اندیشه افت و انداز بازگشت دادیم، تا خواست پاک یزدان چه باشد. قبض تریاک، یبس روزه، خشکیهای سودا، افسردگی های پیری، زیان حرارت ذاتی، و تیمارهای کوری و کری و نادانی و گیجی، دست بهم کرده پاک درهم خوشیده ام. خاک وجودم گوئی یک قطعه سنگ است.</p></div>
<div class="n" id="bn2"><p>باری غفلت از مبداء و مآب، و درنگ و شتاب، و توارد این خطرات گوناگون کاری کرده که یک چشم زد از تفرقه فراغت نداریم. خوشا حال آنان که به خیالی خاطر خود را خوش کرده آرام و استقامتی دارند. بسیار دلم می خواهد تا ورود من مادرت در سمنان باشد. پرستار ندارم و کار زیست شکست. در این خیال باش که او را به سمنان برسانی یا کاری کنی که به نیروی دعا و حصول اطمینان از شر خصمای دور و نزدیک رفته، در کنج مزرعه «دادکین» با سنگ و چوب محشور باشم.</p></div>
<div class="n" id="bn3"><p>سردی و سیری مرا از صحبت خویش و بیگانه، به اعتزال آن کنج کوه رضا کرد. یکی از این دو را همت بگمار. مرحوم حاجی سید محمد تقی قزوینی اجازه ختم حرز یمانی را به من داده است، من هم به تو دادم. از خدا دست عنف بداندیش را از سروقت روزگار ما کوتاه خواه. مغرب تا مشرق هزار سال زنده باشند، همین قدر که بی جهت ما را اذیت نکنند، از ایشان ممنون خواهیم بود.</p></div>
<div class="n" id="bn4"><p>چه بگویم و از که بگویم، همه گناه از سفاهت و خوش باوری و حسن ظن خود من بر من وارد است. البته ختم یمانی را از سلب استیلای بداندیش کوتاهی مکن. نورچشمی ملاباشی هم حتما به همین قصد بخواند، به قصد دیگر راضی نیستم. زوال و مرگ کسی را نمی خواهم. سلب قدرت و اذیت دشمن عقلا و شرعا جایز است. حرف همین است، خبر قبول ملاباشی باید بمن برسد. حرره یغما.</p></div>