---
title: >-
    شمارهٔ  ۲۰ -  وصیت نامه دیگری از یغما
---
# شمارهٔ  ۲۰ -  وصیت نامه دیگری از یغما

<div class="n" id="bn1"><p>در حال هوش و حواس و کمال خرسندی و رضا، اقل خلیقه ابوالحسن یغما وصی شرعی خویش نمودم فرزند ارجمند سعادتمند خود میرزا احمد صفایی را که پس از وفات من مبلغ دویست و پنجاه تومان رایج خزانه از انقد مایملک من اعم از نقد و جنس منقول و غیر منقول برداشته به مصارف مفصله ذیل برساند.</p></div>
<div class="n" id="bn2"><p>مخارج تعزیت ۵۰ تومن</p></div>
<div class="n" id="bn3"><p>حجه بلدی ۷۰ تومن</p></div>
<div class="n" id="bn4"><p>رد مظالم ۷۰ تومن</p></div>
<div class="n" id="bn5"><p>صوم وصلوه ۶۰ تومن</p></div>
<div class="n" id="bn6"><p>و دوزوجه من مادر فرزند ارشدم میرزا اسمعیل و مادر خود را هر یک مبلغ سی تومان از مال من نقد یا ملک تسلیم نماید، ورخوت و سایر اسباب زنانه از طلا آلات و غیره که در دست دارند، به خود ایشان باز گذارند، و جزو ترکه نسازد، و رخوت و اسباب طلا آلات و غیره که به اسم و رسم جهیز برای دخترهای من فراهم آورده یا خودکار کرده اند، و در دست مادرشان است از آنها نستاند، و داخل ارث ورثه ننماید. و اسباب خانه داری و دربایست زندگانی از مسینه آلات و فرش و رخت خواب و سایر چیزها که از مال من در خانه بزرگ در دست اسمعیل و وابستگان اوست دو قسمت نموده یک سهم را اسمعیل متصرف شرعی شود و سهم دیگر را تسلیم نورچشمی خطر سازد و اسباب خانه داری هم از مس و فرش و غیره نیز که در دست والده خود اوست به ابراهیم واگذارد، و در ترکه نیارد و کتبی که دارم سوای آنچه به موجب نوشته شرعی به پسرها بخشیده و خواهم بخشید، چهار قسمت نموده هر رسدی را پسری بردارد، و سایر اموال و املاک و اسباب مرا از ملک و تنخواه و شتر و گوسفند و گاو و خر و غیره نقدا جنسا آنچه مراست به قاعده و قانون احکام الهی و شریعت جناب رسالت پناهی بر کل ورثه پسرها و دخترها و زوجات قسمت نماید و زجر این زحمات را اجر از خدای بخواهد و مرا در زیر خاک آسوده گذارد. وصیت صحیحه شرعیه و کان ذلک فی شهر ذی الحجه ۱۲۶۵.</p></div>