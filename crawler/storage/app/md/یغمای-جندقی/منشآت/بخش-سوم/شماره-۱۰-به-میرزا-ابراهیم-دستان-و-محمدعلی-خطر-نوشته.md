---
title: >-
    شمارهٔ  ۱۰ -  به میرزا ابراهیم دستان و محمدعلی خطر نوشته
---
# شمارهٔ  ۱۰ -  به میرزا ابراهیم دستان و محمدعلی خطر نوشته

<div class="n" id="bn1"><p>ابراهیم، محمدعلی: اگر از من توقع پدری و تربیت دارید این تکلیفات را حتما متحمل شوید و تخلف مکنید و بهوای نفس خود راه مروید. با سید و عامه طایفه مطاع مکرم سلطان به صفا و راستی راه بروید. از کوچکی و پند و دستوری اسمعیل سر موئی تجاوز مکنید. زبان از یاوه دربندید. محمدعلی حتما درس بخواند تا پیش خان دام اقباله نوکر شود و به عقل حرکت کند. هر دو یابوهای خود را بعد از علف یا پیش از علف حتما حکما بفروشید. بی صلاح و رضای اسمعیل قدمی برمدارید. تا من احضار نکنم حتما در سمنان بمانید.</p></div>
<div class="n" id="bn2"><p>در پاس ادب و حرمت و مکاتیه سرکار نایب زید مجده ساعی باشید. در خدمت سرکار نایب الحکومه در همه حال جاهد باشید. بد از احدی مگوئید. حتما اسب ها را بفروشید. به صوابدید میرزا اسمعیل در خرج مراقب باشید. چنانچه جز این باشد میان من و شما جاودانه تفریق خواهد شد.</p></div>
<div class="n" id="bn3"><p>و در حاشیه نامه:</p></div>
<div class="n" id="bn4"><p>هر دو را وصیت می کنم که اگر از جانب سید در سمنان یا طهران یا ولایت یا هر جا حرف خلاف و حرکت دشمنی نسبت به شما احیانا سر بزند باید حتما متحمل شوید و در صدد تلافی نباشید، رجوع کنید به میرزا اسمعیل آنچه او صلاح بیند اطاعت کنید مختار اوست . حرره یغما.</p></div>
<div class="n" id="bn5"><p>در پشت همان نامه آمده است:</p></div>
<div class="n" id="bn6"><p>فرزندهای من؛ من داخل امواتم صلاح شما با میرزا اسمعیل بطور صداقت و بندگی راه رفتن را با طایفه سرکار سلطان صلح و سازش و یگانگی است. غیر از این خلاف عقل است. من این سفر ظلم و بی حقیقتی و معادات و رشک و هرزگی مردم را به تحقیق فهمیدم. قسم می خورم بد بد بد ایشان از خوب خوب خوب اهالی این ولایت الا معدودی بهتر است.</p></div>
<div class="n" id="bn7"><p>ما که غرض و مرض و بی حسابی و بد اندیشی نسبت به احدی نداریم، چرا باید با ایشان که خویش اند و به عقل و کفایت و ثبات و کاردانی از همه بیش، خلاف بکنیم. اتفاق ما با هم عین فرزانگی است و اختلاف محض دیوانگی. هر کس می تواند بسازد و اگر اغوای مردم و فریب نفس او را قوه سازش ندهد برود، شق ثالث ندارد. حرره یغما.</p></div>