---
title: >-
    شمارهٔ  ۸۴
---
# شمارهٔ  ۸۴

<div class="n" id="bn1"><p>رسول خوب و مراسله مرغوب محبوب واصل، چندان لب و دیده بر بیاض آن و سواد این سودم که آن چون روی بخت من سیاه و این چون دیده ملازمان سفید شد، درباب قبا و چکن رخت به خانه خیاط کشیده جامه مطالبت بر انداختم بر قامت قبولش قصیرآمد، زیرا که در مطالبه اجرت دست و بغل گشاده به ذراع دراز نفسی و مقراض تند زبانی نپیموده گز می کرد و گز نکرده می درید، چندان درید و دوخت که پیراهن صبرم قبا گشت. دیدم چنانچه سر سوزنی ار خالق کهنه او را حوار انگاشته خیاطه مثال باریک شده سوراخ به سوراخش کشم، مادام ظهور حتی جمل فی سم الخیاط، سر ما بی کلاه خواهد بود، لاجرم کیک مناظرت از شلوارش برآوردم و پنبه وارش پوشیده دستش از شال کوتاه کردم، پولش دادم و گرفتم و به رسول سپردم و رفتم سگ یوسف عذار طریق مصر وصال را چون صیت حسن زلیخا پی سپر آمده، قطعه:</p></div>
<div class="b" id="bn2"><div class="m1"><p>میرفت بکبر و ناز و می گفت</p></div>
<div class="m2"><p>بی من چه کنی به لابه گفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنشینم وصبر پیش گیرم</p></div>
<div class="m2"><p>دنباله کار خویش گیرم</p></div></div>