---
title: >-
    شمارهٔ  ۱۹ -  به میرزا مهدی طهرانی وزیر اسدالله میرزا نگاشته
---
# شمارهٔ  ۱۹ -  به میرزا مهدی طهرانی وزیر اسدالله میرزا نگاشته

<div class="n" id="bn1"><p>فدایت شوم خطاب خوش لهجت والی بهجت را در ملک محبت نشستی خسروانه داد و تیمار و ملالت را که ولات جور و عصات اند فرمان عزل و ازالت به طغرای حکم و توقیع حتم موشح فرمود. ارادت اسمعیل را نسبت به سرکار خویش اشارتی داده اند و این دیرین عبادت را از قبول خدمت وی و شمول رحمت خود بشارتی فرستاده، مصرع: عبادت لازم است و بنده ملزوم.</p></div>
<div class="n" id="bn2"><p>سی سال است ارادت آورده ایم و سعادت برده، توسل جسته ایم وتحمل اندوخته.این عشق را انشاء الله زوالی و این تحویل را انتقالی نیست. شنیدم از روی بریده زبانی و شکسته دهانی مرا مخالف ستوده و بر نواب والا سروده.</p></div>
<div class="n" id="bn3"><p>مثل: مذکور است مردی از آقا محمد بیدآبادی پرسید که گروهی بر آنند، افلاطون بر دعوت عیسوی دامن افشاند و از الزام یاسای او گردن کشید، فرمود سبحان الله این خود تواند بود. شیخ سلیمان کاشی در غیاب احمد(ص) پس از هزار و دویست با این سفاهت تسلیم اسلام کند، و حکیم کذا در حضور مسیح با آن فطانت کافر بمیرد. کدام ابله باور خواهد کرد؟ پاکار مومن آباد با سخافت ذوق بر موالفت نواب اشرف والا تمکین آرد و یغما با شرافت عقل در مخالفت خدام سابق التعظیم اقدام ورزد، فرد:</p></div>
<div class="b" id="bn4"><div class="m1"><p>در دهر چو من یکی و آنهم کافر</p></div>
<div class="m2"><p>پس در همه دهر یک مسلمان نبود</p></div></div>
<div class="n" id="bn5"><p>قطع دارم این بهتان سرد و هذیان خام را در گوش و هوش سرکارش وزن قبول نخواهد بود، ولی چون هرگز شرفیاب میمون شهودش نیامد، و رستگی های مرا از عالم و بستگی ها با نواب پدر و اعمام بزرگوارش خبر ندارد، دور نیست گاه و بیگاهش بدگمانی فرخنده روان آزرده سازد، به زبان و بیانی که صلاح وقت و سزای هنگام است غبار این اندیشه باز پردازد، مصرع: در دل دارم که بندگی هاش کنم.</p></div>
<div class="n" id="bn6"><p>پسرم را وقف خدمت ایشان کرده، خود نیز با فرط پیری و شرط غرلت خیال صروف سمنان و التزام خدمت دارم خدا روی جنس دو پا را سیاه کند و سامان آرامش بر همگان تباه، که مار گریبانند و خار دامان، فرد:</p></div>
<div class="b" id="bn7"><div class="m1"><p>غالب آنان را که مردم تر ستائی در قیاس</p></div>
<div class="m2"><p>چون به دقت بنگری ز نقحبه تر ز نقحبه اند</p></div></div>