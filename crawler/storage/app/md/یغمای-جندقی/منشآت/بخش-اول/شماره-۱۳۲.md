---
title: >-
    شمارهٔ  ۱۳۲
---
# شمارهٔ  ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>ای از بر من دور همانا خبرت نیست</p></div>
<div class="m2"><p>کز مویه چو موئی شدم از ناله چونالی</p></div></div>
<div class="n" id="bn2"><p>سعید را گفتم آن بخت مسعود را از ورود مملوک خویش خبرده که پس از دیدار آقا بزم بندگان را به مقدم میمون خود مقام محمود دارد. بعد معلوم افتاد که تازه به عیادت فرموده اند، و وقت دلجوئی ارباب ارادت و اصحاب عبادت نیست استغفرالله، بیت:</p></div>
<div class="b" id="bn3"><div class="m1"><p>اختیار خود داری هر چه می کنی جانا</p></div>
<div class="m2"><p>گر به خضر جان بخشی ور کشی مسیحا را</p></div></div>
<div class="n" id="bn4"><p>به دلخواه خود حرکت کن.</p></div>