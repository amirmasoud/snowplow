---
title: >-
    بخش ۲۱۲ - خطاب به مصطفی کمال پاشا ایده الله
---
# بخش ۲۱۲ - خطاب به مصطفی کمال پاشا ایده الله

<div class="b2" id="bn1"><p>جولائی </p></div>
<div class="b" id="bn2"><div class="m1"><p>امئی بود که ما از اثر حکمت او</p></div>
<div class="m2"><p>واقف از سر نهانخانه تقدیر شدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اصل ما یک شرر باخته رنگی بود است</p></div>
<div class="m2"><p>نظری کرد که خورشید جهانگیر شدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکتهٔ عشق فرو شست ز دل پیر حرم</p></div>
<div class="m2"><p>در جهان خوار به اندازهٔ تقصیر شدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد صحراست که با فطرت ما در سازد</p></div>
<div class="m2"><p>از نفسهای صبا غنچهٔ دلگیر شدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه آن غلغله کز گنبد افلاک گذشت</p></div>
<div class="m2"><p>ناله گردید چو پابند بم و زیر شدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بسا صید که بی دام به فتراک زدیم</p></div>
<div class="m2"><p>در بغل تیر و کمان کشتهٔ نخچیر شدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>«هر کجا راه دهد اسپ، بران تاز که ما</p></div>
<div class="m2"><p>بارها مات درین عرصه بتدبیر شدیم»</p></div></div>
<div class="b2" id="bn9"><p>نظیری</p></div>