---
title: >-
    بخش ۱۷۲ - حیات جاوید
---
# بخش ۱۷۲ - حیات جاوید

<div class="b" id="bn1"><div class="m1"><p>گمان مبر که به پایان رسید کار مغان</p></div>
<div class="m2"><p>هزار بادهٔ ناخورده در رگ تاک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چمن خوش است ولیکن چو غنچه نتوان زیست</p></div>
<div class="m2"><p>قبای زندگیش از دم صبا چاک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز رمز حیات آگهی مجوی و مگیر</p></div>
<div class="m2"><p>دلی که از خلش خار آرزو پاک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خود خزیده و محکم چو کوهساران زی</p></div>
<div class="m2"><p>چو خش مزی که هوا تیز و شعله بی‌باک است</p></div></div>