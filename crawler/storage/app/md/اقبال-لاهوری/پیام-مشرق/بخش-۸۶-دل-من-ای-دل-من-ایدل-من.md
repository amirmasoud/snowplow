---
title: >-
    بخش ۸۶ - دل من ای دل من ایدل من
---
# بخش ۸۶ - دل من ای دل من ایدل من

<div class="b" id="bn1"><div class="m1"><p>دل من ای دل من ایدل من</p></div>
<div class="m2"><p>یم من کشتی من ساحل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو شبنم بر سر خاکم چکیدی</p></div>
<div class="m2"><p>و یا چون غنچه رستی از گل من</p></div></div>