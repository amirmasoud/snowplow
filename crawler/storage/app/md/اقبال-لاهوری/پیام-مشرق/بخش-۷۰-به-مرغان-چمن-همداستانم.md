---
title: >-
    بخش ۷۰ - به مرغان چمن همداستانم
---
# بخش ۷۰ - به مرغان چمن همداستانم

<div class="b" id="bn1"><div class="m1"><p>به مرغان چمن همداستانم</p></div>
<div class="m2"><p>زبان غنچه های بی زبانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو میرم با صبا خاکم بیامیز</p></div>
<div class="m2"><p>که جز طوف گلان کاری ندانم</p></div></div>