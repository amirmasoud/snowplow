---
title: >-
    بخش ۲۰۴ - کشمیر
---
# بخش ۲۰۴ - کشمیر

<div class="b" id="bn1"><div class="m1"><p>رخت به کاشمر گشا کوه و تل و دمن نگر</p></div>
<div class="m2"><p>سبزه جهان جهان ببین لاله چمن چمن نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد بهار موج موج مرغ بهار فوج فوج</p></div>
<div class="m2"><p>صلصل و سار زوج زوج بر سر نارون نگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نفتد به زینتش چشم سپهر فتنه باز</p></div>
<div class="m2"><p>بسته به چهرهٔ زمین برقع نسترن نگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله ز خاک بر دمید موج به آب جو تپید</p></div>
<div class="m2"><p>خاک شرر شرر ببین آب شکن شکن نگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زخمه به تار ساز زن باده بساتگین بریز</p></div>
<div class="m2"><p>قافلهٔ بهار را انجمن انجمن نگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دخترکی برهمنی لاله رخی سمن بری</p></div>
<div class="m2"><p>چشم بروی او گشا باز بخویشتن نگر</p></div></div>