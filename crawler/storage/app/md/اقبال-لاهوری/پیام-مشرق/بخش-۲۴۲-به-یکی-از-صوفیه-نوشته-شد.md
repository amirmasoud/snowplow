---
title: >-
    بخش ۲۴۲ - به یکی از صوفیه نوشته شد
---
# بخش ۲۴۲ - به یکی از صوفیه نوشته شد

<div class="b" id="bn1"><div class="m1"><p>هوس منزل لیلی نه تو داری و نه من</p></div>
<div class="m2"><p>جگر گرمی صحرا نه تو داری و نه من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من جوان ساقی و تو پیر کهن میکده ئی</p></div>
<div class="m2"><p>بزم ما تشنه و صهبا نه تو داری و نه من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل و دین در گرو زهره وشان عجمی</p></div>
<div class="m2"><p>آتش شوق سلیمی نه تو داری و نه من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خزفی بود که از ساحل دریا چیدیم</p></div>
<div class="m2"><p>دانهٔ گوهر یکتا نه تو داری و نه من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر از یوسف گمگشته سخن نتوان گفت</p></div>
<div class="m2"><p>تپش خون زلیخا نه تو داری و نه من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به که با نور چراغ ته دامان سازیم</p></div>
<div class="m2"><p>طاقت جلوهٔ سینا نه تو داری و نه من</p></div></div>