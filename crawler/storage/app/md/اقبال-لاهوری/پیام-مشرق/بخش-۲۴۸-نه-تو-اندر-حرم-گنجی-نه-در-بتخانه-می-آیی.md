---
title: >-
    بخش ۲۴۸ - نه تو اندر حرم گنجی نه در بتخانه می‌آیی
---
# بخش ۲۴۸ - نه تو اندر حرم گنجی نه در بتخانه می‌آیی

<div class="b" id="bn1"><div class="m1"><p>نه تو اندر حرم گنجی نه در بتخانه می‌آیی</p></div>
<div class="m2"><p>ولیکن سوی مشتاقان چه مشتاقانه می‌آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدم بی‌باک‌تر نه در حریم جان مشتاقان</p></div>
<div class="m2"><p>تو صاحبخانه‌ای آخر چرا دزدانه می‌آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به غارت می‌بری سرمایهٔ تسبیح‌خوانان را</p></div>
<div class="m2"><p>به شبخون دل زناریان ترکانه می‌آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه صد لشکر انگیزی که خون دوستان ریزی</p></div>
<div class="m2"><p>گهی در انجمن با شیشه و پیمانه می‌آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو بر نخل کلیمی بی‌محابا شعله می‌ریزی</p></div>
<div class="m2"><p>تو بر شمع یتیمی صورت پروانه می‌آیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا اقبال جامی از خمستان خودی در کش</p></div>
<div class="m2"><p>تو از میخانهٔ مغرب ز خود بیگانه می‌آیی</p></div></div>