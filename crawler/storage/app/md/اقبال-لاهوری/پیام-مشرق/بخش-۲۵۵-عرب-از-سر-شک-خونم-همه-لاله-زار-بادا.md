---
title: >-
    بخش ۲۵۵ - عرب از سر شک خونم همه لاله زار بادا
---
# بخش ۲۵۵ - عرب از سر شک خونم همه لاله زار بادا

<div class="b" id="bn1"><div class="m1"><p>عرب از سر شک خونم همه لاله زار بادا</p></div>
<div class="m2"><p>عجم رمیده بو را نفسم بهار بادا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تپش است زندگانی تپش است جاودانی</p></div>
<div class="m2"><p>همه ذره های خاکم دل بیقرار بادا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه بجاده ئی قرارش نه بمنزلی مقامش</p></div>
<div class="m2"><p>دل من مسافر من که خداش یار بادا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حذر از خرد که بندد همه نقش نامرادی</p></div>
<div class="m2"><p>دل ما برد به سازی که گسسته تار بادا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو جوان خام سوزی ، سخنم تمام سوزی</p></div>
<div class="m2"><p>غزلی که می سرایم به تو سازگار بادا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بجان من در آئی دگر آرزو نبینی</p></div>
<div class="m2"><p>مگر اینکه شبنم تو یم بی کنار بادا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشود نصیب جانت که دمی قرار گیرد</p></div>
<div class="m2"><p>تب و تاب زندگانی به تو آشکار بادا</p></div></div>