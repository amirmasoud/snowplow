---
title: >-
    بخش ۲۵۸ - اگرچه زیب سرش افسر و کلاهی نیست
---
# بخش ۲۵۸ - اگرچه زیب سرش افسر و کلاهی نیست

<div class="b" id="bn1"><div class="m1"><p>اگرچه زیب سرش افسر و کلاهی نیست</p></div>
<div class="m2"><p>گدای کوی تو کمتر ز پادشاهی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخواب رفته جوانان و مرده دل پیران</p></div>
<div class="m2"><p>نصیب سینهٔ کس آه صبحگاهی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به این بهانه بدشت طلب ز پا منشین</p></div>
<div class="m2"><p>که در زمانهٔ ما آشنای راهی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز وقت خویش چه غافل نشسته ئی دریاب</p></div>
<div class="m2"><p>زمانه ئی که حسابش ز سال و ماهی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین رباط کهن چشم عافیت داری</p></div>
<div class="m2"><p>ترا به کشمکش زندگی نگاهی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گناه ما چه نویسند کاتبان عمل</p></div>
<div class="m2"><p>نصیب ما ز جهان تو جز نگاهی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا که دامن اقبال را بدست آریم</p></div>
<div class="m2"><p>که او ز خرقه فروشان خانقاهی نیست</p></div></div>