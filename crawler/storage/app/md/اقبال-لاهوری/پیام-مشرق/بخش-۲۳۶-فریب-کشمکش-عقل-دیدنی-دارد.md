---
title: >-
    بخش ۲۳۶ - فریب کشمکش عقل دیدنی دارد
---
# بخش ۲۳۶ - فریب کشمکش عقل دیدنی دارد

<div class="b" id="bn1"><div class="m1"><p>فریب کشمکش عقل دیدنی دارد</p></div>
<div class="m2"><p>که میر قافله و ذوق رهزنی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشان راه ز عقل هزار حیله مپرس</p></div>
<div class="m2"><p>بیا که عشق کمالی ز یک فنی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرنگ گرچه سخن با ستاره میگوید</p></div>
<div class="m2"><p>حذر که شیوهٔ او رنگ جوزنی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز مرگ و زیست چه پرسی درین رباط کهن</p></div>
<div class="m2"><p>که زیست کاهش جان مرگ جانکنی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر مزار شهیدان یکی عنان در کش</p></div>
<div class="m2"><p>که بی زبانی ما حرف گفتنی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر بدشت عرب خیمه زن که بزم عجم</p></div>
<div class="m2"><p>می گذشته و جام شکستنی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه شیخ شهر نه شاعر نه خرقه پوش اقبال</p></div>
<div class="m2"><p>فقیر راه نشین است و دل غنی دارد</p></div></div>