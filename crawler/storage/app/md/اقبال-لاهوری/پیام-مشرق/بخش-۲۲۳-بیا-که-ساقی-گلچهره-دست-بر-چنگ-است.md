---
title: >-
    بخش ۲۲۳ - بیا که ساقی گلچهره دست بر چنگ است
---
# بخش ۲۲۳ - بیا که ساقی گلچهره دست بر چنگ است

<div class="b" id="bn1"><div class="m1"><p>بیا که ساقی گلچهره دست بر چنگ است</p></div>
<div class="m2"><p>چمن ز باد بهاران جواب ارژنگ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حنا ز خون دل نو بهار می بندد</p></div>
<div class="m2"><p>عروس لاله چه اندازه تشنه رنگ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگاه میرسد از نغمهٔ دل افروزی</p></div>
<div class="m2"><p>به معنیی که برو جامهٔ سخن تنگ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چشم عشق نگر تا سراغ او گیری</p></div>
<div class="m2"><p>جهان بچشم خرد سیمیا و نیرنگ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عشق درس عمل گیر و هر چه خواهی کن</p></div>
<div class="m2"><p>که عشق جوهر هوش است و جان فرهنگ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلند تر ز سپهر است منزل من و تو</p></div>
<div class="m2"><p>براه قافله خورشید میل فرسنگ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خود گذشته ئی ای قطره محال اندیش</p></div>
<div class="m2"><p>شدن به بحر و گهر برنخاستن ننگ است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو قدر خویش ندانی بها ز تو گیرد</p></div>
<div class="m2"><p>وگرنه لعل درخشنده پاره سنگ است</p></div></div>