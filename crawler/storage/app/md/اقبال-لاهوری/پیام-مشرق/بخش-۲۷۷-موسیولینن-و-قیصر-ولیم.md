---
title: >-
    بخش ۲۷۷ - موسیولینن و قیصر ولیم
---
# بخش ۲۷۷ - موسیولینن و قیصر ولیم

<div class="b2" id="bn1"><p>موسیولینن </p></div>
<div class="b" id="bn2"><div class="m1"><p>بسی گذشت که آدم درین سرای کهن</p></div>
<div class="m2"><p>مثال دانه ته سنگ آسیا بودست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریب زاری و افسون قیصری خورد است</p></div>
<div class="m2"><p>اسیر حلقهٔ دام کلیسیا بودست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلام گرسنه دیدی که بر درید آخر</p></div>
<div class="m2"><p>قمیص خواجه که رنگین ز خون ما بودست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرار آتش جمهور کهنه سامان سوخت</p></div>
<div class="m2"><p>ردای پیر کلیسا قبای سلطان سوخت</p></div></div>
<div class="b2" id="bn6"><p>قیصر ولیم </p></div>
<div class="b" id="bn7"><div class="m1"><p>گناه عشوه و ناز بتان چیست؟</p></div>
<div class="m2"><p>طواف اندر سرشت برهمن هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دمادم نو خداوندان تراشد</p></div>
<div class="m2"><p>که بیزار از خدایان کهن هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز جور رهزنان کم گو که رهرو</p></div>
<div class="m2"><p>متاع خویش را خود رهزن هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر تاج کئی جمهور پوشد</p></div>
<div class="m2"><p>همان هنگامه ها در انجمن هست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هوس اندر دل آدم نمیرد</p></div>
<div class="m2"><p>همان آتش میان مرزغن هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عروس اقتدار سحر فن را</p></div>
<div class="m2"><p>همان پیچاک زلف پر شکن هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>«نماند ناز شیرین بی خریدار</p></div>
<div class="m2"><p>اگر خسرو نباشد کوهکن هست»</p></div></div>