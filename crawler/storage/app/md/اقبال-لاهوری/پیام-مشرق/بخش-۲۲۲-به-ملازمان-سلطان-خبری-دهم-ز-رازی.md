---
title: >-
    بخش ۲۲۲ - به ملازمان سلطان خبری دهم ز رازی
---
# بخش ۲۲۲ - به ملازمان سلطان خبری دهم ز رازی

<div class="b" id="bn1"><div class="m1"><p>به ملازمان سلطان خبری دهم ز رازی</p></div>
<div class="m2"><p>که جهان توان گرفتن بنوای دلگدازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به متاع خود چه نازی که بشهر دردمندان</p></div>
<div class="m2"><p>دل غزنوی نیرزد به تبسم ایازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه ناز بی نیازی همه ساز بینوائی</p></div>
<div class="m2"><p>دل شاه لرزه گیرد ز گدای بی نیازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز مقام من چه پرسی به طلسم دل اسیرم</p></div>
<div class="m2"><p>نه نشیب من نشیبی نه فراز من فرازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ره عاقلی رها کن که به او توان رسیدن</p></div>
<div class="m2"><p>به دل نیازمندی به نگاه پاکبازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ره تو ناتمامم ز تغافل تو خامم</p></div>
<div class="m2"><p>من و جان نیم سوزی تو و چشم نیم بازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ره دیر تختهٔ گل ز جبین سجده ریزم</p></div>
<div class="m2"><p>که نیاز من نگنجد به دو رکعت نمازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ستیز آشنایان چه نیاز و ناز خیزد</p></div>
<div class="m2"><p>دلکی بهانه سوزی نگهی بهانه سازی</p></div></div>