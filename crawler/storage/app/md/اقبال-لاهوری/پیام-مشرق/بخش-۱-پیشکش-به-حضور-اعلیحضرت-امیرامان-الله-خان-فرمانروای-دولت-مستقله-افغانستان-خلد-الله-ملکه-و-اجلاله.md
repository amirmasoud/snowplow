---
title: >-
    بخش ۱ - پیشکش به حضور اعلیحضرت امیرامان الله خان فرمانروای دولت مستقلهٔ افغانستان خلد الله ملکه و اجلاله
---
# بخش ۱ - پیشکش به حضور اعلیحضرت امیرامان الله خان فرمانروای دولت مستقلهٔ افغانستان خلد الله ملکه و اجلاله

<div class="b" id="bn1"><div class="m1"><p>ای امیر کامگار ای شهریار</p></div>
<div class="m2"><p>نوجوان و مثل پیران پخته کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم تو از پردگیهای محرم است</p></div>
<div class="m2"><p>دل میان سینه ات جام جم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عزم تو پاینده چون کهسار تو</p></div>
<div class="m2"><p>حزم تو آسان کند دشوار تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همت تو چون خیال من بلند</p></div>
<div class="m2"><p>ملت صد پاره را شیرازه بند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هدیه از شاهنشهان داری بسی</p></div>
<div class="m2"><p>لعل و یاقوت گران داری بسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای امیر ابن امیر ابن امیر</p></div>
<div class="m2"><p>هدیه ئی از بینوائی هم پذیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا مرا رمز حیات آموختند</p></div>
<div class="m2"><p>آتشی در پیکرم افروختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک نوای سینه تاب آورده ام</p></div>
<div class="m2"><p>عشق را عهد شباب آورده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیر مغرب شاعر المانوی</p></div>
<div class="m2"><p>آن قتیل شیوه های پهلوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بست نقش شاهدان شوخ و شنگ</p></div>
<div class="m2"><p>داد مشرق را سلامی از فرنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در جوابش گفته ام پیغام شرق</p></div>
<div class="m2"><p>ماهتابی ریختم بر شام شرق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا شناسای خودم خود بین نیم</p></div>
<div class="m2"><p>با تو گویم او که بود و من کیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او ز افرنگی جوانان مثل برق</p></div>
<div class="m2"><p>شعلهٔ من از دم پیران شرق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>او چمن زادی چمن پرورده ئی</p></div>
<div class="m2"><p>من دمیدم از زمین مرده ئی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>او چو بلبل در چمن فردوس گوش</p></div>
<div class="m2"><p>من بصحرا چون جرس گرم خروش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر دو دانای ضمیر کائنات</p></div>
<div class="m2"><p>هر دو پیغام حیات اندر ممات</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر دو خنجر صبح خند آئینه فام</p></div>
<div class="m2"><p>او برهنه من هنوز اندر نیام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر دو گوهر ارجمند و تاب دار</p></div>
<div class="m2"><p>زادهٔ دریای ناپیدا کنار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>او ز شوخی در ته قلزم تپید</p></div>
<div class="m2"><p>تا گریبان صدف را بر درید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من به آغوش صدف تابم هنوز</p></div>
<div class="m2"><p>در ضمیر بحر نایابم هنوز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آشنای من ز من بیگانه رفت</p></div>
<div class="m2"><p>از خمستانم تهی پیمانه رفت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من شکوه خسروی او را دهم</p></div>
<div class="m2"><p>تخت کسری زیر پای او نهم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>او حدیث دلبری خواهد ز من</p></div>
<div class="m2"><p>رنگ و آب شاعری خواهد ز من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کم نظر بیتابی جانم ندید</p></div>
<div class="m2"><p>آشکارم دید و پنهانم ندید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فطرت من عشق را در بر گرفت</p></div>
<div class="m2"><p>صحبت خاشاک و آتش در گرفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حق رموز ملک و دین بر من گشود</p></div>
<div class="m2"><p>نقش غیر از پردهٔ چشمم ربود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برگ گل رنگین ز مضمون من است</p></div>
<div class="m2"><p>مصرع من قطرهٔ خون من است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا نپنداری سخن دیوانگیست</p></div>
<div class="m2"><p>در کمال این جنوان فرزانگیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از هنر سرمایه دارم کرده اند</p></div>
<div class="m2"><p>در دیار هند خوارم کرده اند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>لاله و گل از نوایم بی نصیب</p></div>
<div class="m2"><p>طایرم در گلستان خود غریب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بسکه گردون سفله و دون پرور است</p></div>
<div class="m2"><p>وای بر مردی که صاحب جوهر است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دیده ئی ای خسرو کیوان جناب</p></div>
<div class="m2"><p>آفتاب «ما توارت بالحجاب»</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ابطحی در دشت خویش از راه رفت</p></div>
<div class="m2"><p>از دم او سوز الا الله رفت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مصریان افتاده در گرداب نیل</p></div>
<div class="m2"><p>سست رگ تورانیان ژنده پیل</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آل عثمان در شکنج روزگار</p></div>
<div class="m2"><p>مشرق و مغرب ز خونش لاله زار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عشق را آئین سلمانی نماند</p></div>
<div class="m2"><p>خاک ایران ماند و ایرانی نماند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سوز و ساز زندگی رفت از گلش</p></div>
<div class="m2"><p>آن کهن آتش فسرده اندر دلش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مسلم هندی شکم را بنده ئی</p></div>
<div class="m2"><p>خود فروشی دل ز دین بر کنده ئی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در مسلمان شأن محبوبی نماند</p></div>
<div class="m2"><p>خالد و فاروق و ایوبی نماند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای ترا فطرت ضمیر پاک داد</p></div>
<div class="m2"><p>از غم دین سینهٔ صد چاک داد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تازه کن آئین صدیق و عمر</p></div>
<div class="m2"><p>چون صبا بر لالهٔ صحرا گذر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ملت آوارهٔ کوه و دمن</p></div>
<div class="m2"><p>در رگ او خون شیران موج زن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زیرک و روئین تن و روشن جبین</p></div>
<div class="m2"><p>چشم او چون جره بازان تیز بین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>قسمت خود از جهان نا یافته</p></div>
<div class="m2"><p>کوکب تقدیر او نا تافته</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در قهستان خلوتی ورزیده ئی</p></div>
<div class="m2"><p>رستخیز زندگی نادیده ئی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جان تو بر محنت پیهم صبور</p></div>
<div class="m2"><p>کوش در تهذیب افغان غیور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا ز صدیقان این امت شوی </p></div>
<div class="m2"><p>بهر دین سرمایهٔ قوت شوی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زندگی جهد است و استحقاق نیست</p></div>
<div class="m2"><p>جز به علم انفس و آفاق نیست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گفت حکمت را خدا خیر کیثر</p></div>
<div class="m2"><p>هر کجا این خیز را بینی بگیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سید کل صاحب ام الکتاب</p></div>
<div class="m2"><p>پردگیها بر ضمیرش بی حجاب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گرچه عین ذات را بی پرده دید</p></div>
<div class="m2"><p>«رب زدنی» از زبان او چکید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>علم اشیا «علم الاسماستی»</p></div>
<div class="m2"><p>هم عصا و هم ید بیضا ستی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>علم اشیا داد مغرب را فروغ</p></div>
<div class="m2"><p>حکمت او ماست می بندد ز دوغ</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جان ما را لذت احساس نیست</p></div>
<div class="m2"><p>خاک ره جز ریزهٔ الماس نیست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>علم و دولت نظم کار ملت است</p></div>
<div class="m2"><p>علم و دولت اعتبار ملت است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آن یکی از سینهٔ احرار گیر</p></div>
<div class="m2"><p>وان دگر از سینهٔ کهسار گیر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دشنه زن در پیکر این کائنات</p></div>
<div class="m2"><p>در شکم دارد گهر چون سومنات</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>لعل ناب اندر بدخشان تو هست</p></div>
<div class="m2"><p>برق سینا در قهستان تو هست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کشور محکم اساسی بایدت</p></div>
<div class="m2"><p>دیدهٔ مردم شناسی بایدت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ای بسا آدم که ابلیسی کند</p></div>
<div class="m2"><p>ای بسا شیطان که ادریسی کند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>رنگ او نیرنگ و بود او نمود</p></div>
<div class="m2"><p>اندرون او چو داغ لاله دود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>پاکباز و کعبتین او دغل</p></div>
<div class="m2"><p>ریمن و غدر و نفاق اندر بغل</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در نگر ای خسرو صاحب نظر</p></div>
<div class="m2"><p>نیست هر سنگی که می تابد گهر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مرشد رومی حکیم پاک زاد</p></div>
<div class="m2"><p>سر مرگ و زندگی بر ما گشاد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>«هر هلاک امت پیشین که بود</p></div>
<div class="m2"><p>زانکه بر جندل گمان بردند عود» </p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سروری در دین ما خدمتگری است</p></div>
<div class="m2"><p>عدل فاروقی و فقر حیدری است</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>در هجوم کارهای ملک و دین</p></div>
<div class="m2"><p>با دل خود یک نفس خلوت گزین</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>هر که یکدم در کمین خود نشست</p></div>
<div class="m2"><p>هیچ نخچیر از کمند او نجست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>در قبای خسروی درویش زی</p></div>
<div class="m2"><p>دیده بیدار و خدا اندیش زی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>قاید ملت شهنشاه مراد</p></div>
<div class="m2"><p>تیغ او را برق و تندر خانه زاد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هم فقیری هم شه گردون فری </p></div>
<div class="m2"><p>ارد شیری با روان بوذری</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>غرق بودش در زره بالا و دوش</p></div>
<div class="m2"><p>در میان سینه دل موئینه پوش</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>آن مسلمانان که میری کرده اند</p></div>
<div class="m2"><p>در شهنشاهی فقیری کرده اند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در امارت فقر را افزوده اند</p></div>
<div class="m2"><p>مثل سلمان در مدائن بوده اند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>حکمرانی بود و سامانی نداشت</p></div>
<div class="m2"><p>دست او جز تیغ و قرآنی نداشت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هر که عشق مصطفی سامان اوست</p></div>
<div class="m2"><p>بحر و بر در گوشهٔ دامان اوست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سوز صدیق و علی از حق طلب</p></div>
<div class="m2"><p>ذره ئی عشق نبی از حق طلب</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>زانکه ملت را حیات از عشق اوست</p></div>
<div class="m2"><p>برگ و ساز کائنات از عشق اوست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>جلوهٔ بی پرده او وانمود</p></div>
<div class="m2"><p>جوهر پنهان که بود اندر وجود</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>روح را جز عشق او آرام نیست</p></div>
<div class="m2"><p>عشق او روزیست کو را شام نیست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>خیز و اندر گردش آور جام عشق </p></div>
<div class="m2"><p>در قهستان تازه کن پیغام عشق</p></div></div>