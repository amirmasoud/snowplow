---
title: >-
    بخش ۲۱۷ - حلقه بستند سر تربت من نوحه کران
---
# بخش ۲۱۷ - حلقه بستند سر تربت من نوحه کران

<div class="b" id="bn1"><div class="m1"><p>حلقه بستند سر تربت من نوحه کران</p></div>
<div class="m2"><p>دلبران زهره وشان گل برنان سیم بران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چمن قافلهٔ لاله و گل رخت گشود</p></div>
<div class="m2"><p>از کجا آمده اند این همه خونین جگران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایکه در مدرسه جوئی ادب و دانش و ذوق</p></div>
<div class="m2"><p>نخرد باده کس از کارگه شیشه گران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرد افزود مرا درس حکیمان فرنگ</p></div>
<div class="m2"><p>سینه افروخت مرا صحبت صاحبنظران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر کش آن نغمه که سرمایه آب و گل تست</p></div>
<div class="m2"><p>ای ز خود رفته تهی شو ز نوای دگران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس ندانست که من نیز بهائی دارم</p></div>
<div class="m2"><p>آن متاعم که شود دست زد بی بصران</p></div></div>