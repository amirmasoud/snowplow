---
title: >-
    بخش ۲۱۵ - تهذیب
---
# بخش ۲۱۵ - تهذیب

<div class="b" id="bn1"><div class="m1"><p>انسان که رخ ز غازهٔ تهذیب بر فروخت</p></div>
<div class="m2"><p>خاک سیاه خویش چو آئینه وانمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پوشید پنجه را ته دستانه حریر</p></div>
<div class="m2"><p>افسونی قلم شد و تیغ از کمر گشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این بوالهوس صنم کدهٔ صلح عام ساخت</p></div>
<div class="m2"><p>رقصید گرد او به نواهای چنگ و عود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدم چو جنگ پرده ناموس او درید</p></div>
<div class="m2"><p>جز یسفک الدما و «خصیم مبین» نبود</p></div></div>