---
title: >-
    بخش ۲۰۵ - عشق
---
# بخش ۲۰۵ - عشق

<div class="b" id="bn1"><div class="m1"><p>عقلی که جهان سوزد یک جلوهٔ بی‌باکش</p></div>
<div class="m2"><p>از عشق بیاموزد آیین جهان‌تابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق است که در جانت هر کیفیت انگیزد</p></div>
<div class="m2"><p>از تاب و تب رومی تا حیرت فارابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این حرف نشاط‌آور می‌گویم و می‌رقصم</p></div>
<div class="m2"><p>از عشق دل آساید با این همه بی‌تابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر معنی پیچیده در حرف نمی‌گنجد</p></div>
<div class="m2"><p>یک لحظه به دل در شو شاید که تو دریابی</p></div></div>