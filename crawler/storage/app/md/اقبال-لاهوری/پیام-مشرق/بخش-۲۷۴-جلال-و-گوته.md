---
title: >-
    بخش ۲۷۴ - جلال و گوته
---
# بخش ۲۷۴ - جلال و گوته

<div class="b" id="bn1"><div class="m1"><p>نکته دان المنی را در ارم</p></div>
<div class="m2"><p>صحبتی افتاد با پیر عجم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاعری کو همچو آن عالی جناب</p></div>
<div class="m2"><p>نیست پیغمبر ولی دارد کتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواند بر دانای اسرار قدیم</p></div>
<div class="m2"><p>قصهٔ پیمان ابلیس و حکیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت رومی ای سخن را جان نگار</p></div>
<div class="m2"><p>تو ملک صید استی و یزدان شکار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکر تو در کنج دل خلوت گزید</p></div>
<div class="m2"><p>این جهان کهنه را باز آفرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوز و ساز جان به پیکر دیده ئی</p></div>
<div class="m2"><p>در صدف تعمیر گوهر دیده ئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کسی از رمز عشق آگاه نیست</p></div>
<div class="m2"><p>هر کسی شایان این درگاه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>«داند آن کو نیکبخت و محرم است</p></div>
<div class="m2"><p>زیرکی ز ابلیس و عشق از آدم است» </p></div></div>
<div class="b2" id="bn9"><p>رومی</p></div>