---
title: >-
    بخش ۲۴۶ - سوز سخن ز نالهٔ مستانهٔ دل است
---
# بخش ۲۴۶ - سوز سخن ز نالهٔ مستانهٔ دل است

<div class="b" id="bn1"><div class="m1"><p>سوز سخن ز نالهٔ مستانهٔ دل است</p></div>
<div class="m2"><p>این شمع را فروغ ز پروانهٔ دل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشت گلیم و ذوق فغانی نداشتیم</p></div>
<div class="m2"><p>غوغای ما ز گردش پیمانهٔ دل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این تیره خاکدان که جهان نام کرده ئی</p></div>
<div class="m2"><p>فرسوده پیکری ز صنم خانهٔ دل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر رصد نشسته حکیم ستاره بین</p></div>
<div class="m2"><p>در جستجوی سرحد ویرانهٔ دل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاهوتیان اسیر کمند نگاه او</p></div>
<div class="m2"><p>صوفی هلاک شیوه ترکانهٔ دل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محمود غزنوی که صنم خانه ها شکست</p></div>
<div class="m2"><p>زناری بتان صنم خانهٔ دل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غافل تری ز مرد مسلمان ندیده ام</p></div>
<div class="m2"><p>دل در میان سینه و بیگانهٔ دل است</p></div></div>