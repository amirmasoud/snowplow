---
title: >-
    بخش ۲۲۹ - بیار باده که گردون بکام ما گردید
---
# بخش ۲۲۹ - بیار باده که گردون بکام ما گردید

<div class="b" id="bn1"><div class="m1"><p>بیار باده که گردون بکام ما گردید</p></div>
<div class="m2"><p>مثال غنچه نواها ز شاخسار دمید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورم بیاد تنک نوشی امام حرم</p></div>
<div class="m2"><p>که جز به صحبت یاران رازدان نچشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فزون قبیلهٔ آن پخته کار باد که گفت</p></div>
<div class="m2"><p>چراغ راه حیات است جلوهٔ امید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوا ز حوصلهٔ دوستان بلند تر است</p></div>
<div class="m2"><p>غزل سرا شدم آنجا که هیچکس نشنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیار معرفت مشتری است جنس سخن</p></div>
<div class="m2"><p>خوشم از آنکه متاع مرا کسی نخرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شعر دلکش اقبال میتوان در یافت</p></div>
<div class="m2"><p>که درس فلسفه میداد و عاشقی ورزید</p></div></div>