---
title: >-
    بخش ۲۵۳ - بیا که بلبل شوریده نغمه پرداز است
---
# بخش ۲۵۳ - بیا که بلبل شوریده نغمه پرداز است

<div class="b" id="bn1"><div class="m1"><p>بیا که بلبل شوریده نغمه پرداز است</p></div>
<div class="m2"><p>عروس لاله سراپا کرشمه و ناز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوا ز پردهٔ غیب است ای مقام شناس</p></div>
<div class="m2"><p>نه از گلوی غزل خوان نه از رگ ساز است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که زخمه رساند به تار ساز حیات</p></div>
<div class="m2"><p>ز من بگیر که آن بنده محرمراز است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا ز پردگیان جهان خبر دادند</p></div>
<div class="m2"><p>ولی زبان نگشایم که چرخ کج باز است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن درشت مگو در طریق یاری کوش</p></div>
<div class="m2"><p>که صحبت من و تو در جهان خدا ساز است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجاست منزل این خاکدان تیره نهاد</p></div>
<div class="m2"><p>که هر چه هست چو ریگ روان به پرواز است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنم گلی ز خیابان جنت کشمیر</p></div>
<div class="m2"><p>دل از حریم حجاز و نوا ز شیراز است</p></div></div>