---
title: >-
    بخش ۱۸۴ - حقیقت
---
# بخش ۱۸۴ - حقیقت

<div class="b" id="bn1"><div class="m1"><p>عقاب دوربین جوئینه را گفت</p></div>
<div class="m2"><p>نگاهم آنچه می بیند سراب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوابش داد آن مرغ حق اندیش</p></div>
<div class="m2"><p>تو می بینی و من دانم که آب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صدای ماهی آمد از ته بحر</p></div>
<div class="m2"><p>که چیزی هست و هم در پیچ و تاب است</p></div></div>