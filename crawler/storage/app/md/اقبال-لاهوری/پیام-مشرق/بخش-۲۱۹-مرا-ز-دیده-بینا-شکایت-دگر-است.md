---
title: >-
    بخش ۲۱۹ - مرا ز دیدهٔ بینا شکایت دگر است
---
# بخش ۲۱۹ - مرا ز دیدهٔ بینا شکایت دگر است

<div class="b" id="bn1"><div class="m1"><p>مرا ز دیدهٔ بینا شکایت دگر است</p></div>
<div class="m2"><p>که چون بجلوه در آئی حجاب من نظر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نوریان ز من پا به گل پیامی گوی</p></div>
<div class="m2"><p>حذر ز مشت غباری که خویشتن نگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوا زنیم و به بزم بهار می سوزیم</p></div>
<div class="m2"><p>شرر به مشت پر ما ز ناله سحر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خود رمیده چه داند نوای من ز کجاست</p></div>
<div class="m2"><p>جهان او دگر است و جهان من دگر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مثال لاله فتادم بگوشهٔ چمنی</p></div>
<div class="m2"><p>مرا ز تیر نگاهی نشانه بر جگر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کیش زنده دلان زندگی جفا طلبی است</p></div>
<div class="m2"><p>سفر به کعبه نکردم که راه بی خطر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار انجمن آراستند و بر چیدند</p></div>
<div class="m2"><p>درین سراچه که روشن ز مشعل قمر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خاک خویش به تعمیر آدمی بر خیز</p></div>
<div class="m2"><p>که فرصت تو بقدر تبسم شرر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر نه بوالهوسی با تو نکته ئی گویم</p></div>
<div class="m2"><p>که عشق پخته تر از ناله های بی اثر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نوای من به عجم آتش کهن افروخت</p></div>
<div class="m2"><p>عرب ز نغمهٔ شوقم هنوز بی خبر است</p></div></div>