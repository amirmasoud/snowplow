---
title: >-
    بخش ۱۵۴ - به گوشم آمد از خاک مزاری
---
# بخش ۱۵۴ - به گوشم آمد از خاک مزاری

<div class="b" id="bn1"><div class="m1"><p>به گوشم آمد از خاک مزاری</p></div>
<div class="m2"><p>که در زیر زمین هم میتوان زیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس دارد ولیکن جان ندارد</p></div>
<div class="m2"><p>کسی کو بر مراد دیگران زیست</p></div></div>