---
title: >-
    بخش ۲۰۸ - چیستان شمشیر
---
# بخش ۲۰۸ - چیستان شمشیر

<div class="b" id="bn1"><div class="m1"><p>آن سخت‌کوش چیست که گیرد ز سنگ آب</p></div>
<div class="m2"><p>محتاج خضر مثل سکندر نمی‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مثل نگاه دیدهٔ نمناک پاک رو</p></div>
<div class="m2"><p>در جوی آب و دامن او تر نمی‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مضمون او به مصرع برجسته‌ای تمام</p></div>
<div class="m2"><p>منت‌پذیر مصرع دیگر نمی‌شود</p></div></div>