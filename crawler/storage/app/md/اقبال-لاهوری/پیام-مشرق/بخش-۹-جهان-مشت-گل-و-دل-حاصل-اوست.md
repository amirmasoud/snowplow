---
title: >-
    بخش ۹ - جهان مشت گل و دل حاصل اوست
---
# بخش ۹ - جهان مشت گل و دل حاصل اوست

<div class="b" id="bn1"><div class="m1"><p>جهان مشت گل و دل حاصل اوست</p></div>
<div class="m2"><p>همین یک قطرهٔ خون مشکل اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاه ما دو بین افتاد ورنه</p></div>
<div class="m2"><p>جهان هر کسی اندر دل اوست</p></div></div>