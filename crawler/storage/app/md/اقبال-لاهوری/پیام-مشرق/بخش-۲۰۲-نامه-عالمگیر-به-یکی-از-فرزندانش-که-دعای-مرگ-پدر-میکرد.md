---
title: >-
    بخش ۲۰۲ - نامهٔ عالمگیر (به یکی از فرزندانش که دعای مرگ پدر میکرد)
---
# بخش ۲۰۲ - نامهٔ عالمگیر (به یکی از فرزندانش که دعای مرگ پدر میکرد)

<div class="b" id="bn1"><div class="m1"><p>ندانی که یزدان دیرینه بود</p></div>
<div class="m2"><p>بسی دید و سنجید و بست و گشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ما سینه چاکان این تیره خاک</p></div>
<div class="m2"><p>شنید است صد نالهٔ درد ناک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی همچو شبیر در خون نشست</p></div>
<div class="m2"><p>نه یک ناله از سینهٔ او گسست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه از گریهٔ پیر کنعان تپید</p></div>
<div class="m2"><p>نه از درد ایوب آهی کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مپندار آن کهنه نخچیر گیر</p></div>
<div class="m2"><p>بدام دعای تو گردد اسیر</p></div></div>