---
title: >-
    بخش ۲۵۰ - مثل آئینه مشو محو جمال دگران
---
# بخش ۲۵۰ - مثل آئینه مشو محو جمال دگران

<div class="b" id="bn1"><div class="m1"><p>مثل آئینه مشو محو جمال دگران</p></div>
<div class="m2"><p>از دل و دیده فرو شوی خیال دگران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش از ناله مرغان حرم گیر و بسوز</p></div>
<div class="m2"><p>آشیانی که نهادی به نهال دگران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان بال و پر خویش گشودن آموز</p></div>
<div class="m2"><p>که پریدن نتوان با پر و بال دگران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد آزادم و آن گونه غیورم که مرا</p></div>
<div class="m2"><p>می توان کشت بیک جام زلال دگران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایکه نزدیک تر از جانی و پنهان ز نگه</p></div>
<div class="m2"><p>هجر تو خوشترم آید ز وصال دگران</p></div></div>