---
title: >-
    بخش ۲۸۵ - )
---
# بخش ۲۸۵ - )

<div class="b" id="bn1"><div class="m1"><p>میخورد هر ذره ما پیچ و تاب</p></div>
<div class="m2"><p>محشری در هر دم ما مضمر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با سکندر خضر در ظلمات گفت</p></div>
<div class="m2"><p>مرگ مشکل ، زندگی مشکل تر است</p></div></div>