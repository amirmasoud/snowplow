---
title: >-
    بخش ۱۸۵ - حدی
---
# بخش ۱۸۵ - حدی

<div class="b2" id="bn1"><p>نغمهٔ ساربان حجاز</p></div>
<div class="b" id="bn2"><div class="m1"><p>ناقهٔ سیار من</p></div>
<div class="m2"><p>آهوی تاتار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درهم و دینار من</p></div>
<div class="m2"><p>اندک و بسیار من</p></div></div>
<div class="b2" id="bn4"><p>دولت بیدار من</p></div>
<div class="b2" id="bn5"><p>تیزترک گام زن منزل ما دور نیست</p></div>
<div class="b" id="bn6"><div class="m1"><p>دلکش و زیباستی</p></div>
<div class="m2"><p>شاهد رعناستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روکش حوراستی</p></div>
<div class="m2"><p>غیرت لیلاستی</p></div></div>
<div class="b2" id="bn8"><p>دختر صحراستی</p></div>
<div class="b2" id="bn9"><p>تیزترک گام زن منزل ما دور نیست</p></div>
<div class="b" id="bn10"><div class="m1"><p>در تپش آفتاب</p></div>
<div class="m2"><p>غوطه زنی در سراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم به شب ماهتاب</p></div>
<div class="m2"><p>تند روی چون شهاب</p></div></div>
<div class="b2" id="bn12"><p>چشم تو نادیده خواب</p></div>
<div class="b2" id="bn13"><p>تیزترک گام زن منزل ما دور نیست</p></div>
<div class="b" id="bn14"><div class="m1"><p>لکه ابر روان</p></div>
<div class="m2"><p>کشتی بی بادبان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مثل خضر راه دان</p></div>
<div class="m2"><p>بر تو سبک هر گران</p></div></div>
<div class="b2" id="bn16"><p>لخت دل ساربان</p></div>
<div class="b2" id="bn17"><p>تیزترک گام زن منزل ما دور نیست</p></div>
<div class="b" id="bn18"><div class="m1"><p>سوز تو اندر زمام</p></div>
<div class="m2"><p>ساز تو اندر خرام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بی خورش و تشنه کام</p></div>
<div class="m2"><p>پا به سفر صبح و شام</p></div></div>
<div class="b2" id="bn20"><p>خسته شوی از مقام</p></div>
<div class="b2" id="bn21"><p>تیزترک گام زن منزل ما دور نیست</p></div>
<div class="b" id="bn22"><div class="m1"><p>شام تو اندر یمن</p></div>
<div class="m2"><p>صبح تو اندر قرن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ریگ درشت وطن</p></div>
<div class="m2"><p>پای ترا یاسمن</p></div></div>
<div class="b2" id="bn24"><p>ای چو غزال ختن</p></div>
<div class="b2" id="bn25"><p>تیزترک گام زن منزل ما دور نیست</p></div>
<div class="b" id="bn26"><div class="m1"><p>مه ز سفر پا کشید</p></div>
<div class="m2"><p>در پس تل آرمید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صبح ز مشرق دمید</p></div>
<div class="m2"><p>جامهٔ شب بر درید</p></div></div>
<div class="b2" id="bn28"><p>باد بیابان وزید</p></div>
<div class="b2" id="bn29"><p>تیزترک گام زن منزل ما دور نیست</p></div>
<div class="b" id="bn30"><div class="m1"><p>نغمهٔ من دلگشای</p></div>
<div class="m2"><p>زیر و بمش جانفرای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قافله ها را درای</p></div>
<div class="m2"><p>فتنه ربا فتنه زای</p></div></div>
<div class="b2" id="bn32"><p>ای به حرم چهره سای</p></div>
<div class="b2" id="bn33"><p>تیزترک گام زن منزل ما دور نیست</p></div>