---
title: >-
    بخش ۱۷۸ - پند باز با بچهٔ خویش
---
# بخش ۱۷۸ - پند باز با بچهٔ خویش

<div class="b" id="bn1"><div class="m1"><p>تو دانی که بازان ز یک جوهرند</p></div>
<div class="m2"><p>دل شیر دارند و مشت پرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکو شیوه و پخته تدبیر باش</p></div>
<div class="m2"><p>جسور و غیور و کلان گیر باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میامیز با کبک و تورنگ و سار</p></div>
<div class="m2"><p>مگر اینکه داری هوای شکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه قومی فرو مایهٔ ترسناک</p></div>
<div class="m2"><p>کند پاک منقار خود را به خاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد آن باشه نخچیر نخچیر خویش</p></div>
<div class="m2"><p>که گیرد ز صید خود آئین و کیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسا شکره افتاده بر روی خاک</p></div>
<div class="m2"><p>شد از صحبت دانه چینان هلاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگه دار خود را و خورسند زی</p></div>
<div class="m2"><p>دلیر و درشت و تنومند زی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن نرم و نازک به تیهو گذار</p></div>
<div class="m2"><p>رگ سخت چون شاخ آهو بیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نصیب جهان آنچه از خرمی است</p></div>
<div class="m2"><p>ز سنگینی و محنت و پر دمی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه خوش گفت فرزند خود را عقاب</p></div>
<div class="m2"><p>که یک قطره خون بهتر از لعل ناب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مجو انجمن مثل آهو و میش</p></div>
<div class="m2"><p>به خلوت گرا چون نیاکان خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین یاد دارم ز بازان پیر</p></div>
<div class="m2"><p>نشیمن بشاخ درختی مگیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنامی نگیریم در باغ و کشت</p></div>
<div class="m2"><p>که داریم در کوه و صحرا بهشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز روی زمین دانه چیدن خطاست</p></div>
<div class="m2"><p>که پهنای گردون خدا داد ماست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نجیبی که پا بر زمین سوده است</p></div>
<div class="m2"><p>ز مرغ سرا سفله تر بوده است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پی شاهبازان بساط است سنگ</p></div>
<div class="m2"><p>که بر سنگ رفتن کند تیز چنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو از زرد چشمان صحراستی</p></div>
<div class="m2"><p>به گوهر چو سیمرغ والاستی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جوانی اصیلی که در روز جنگ</p></div>
<div class="m2"><p>برد مردمک را ز چشم پلنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به پرواز تو سطوت نوریان</p></div>
<div class="m2"><p>به رگهای تو خون کافوریان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ته چرخ گردندهٔ کوژ پشت</p></div>
<div class="m2"><p>بخور آنچه گیری ز نرم و درشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز دست کسی طعمهٔ خود مگیر</p></div>
<div class="m2"><p>نکو باش و پند نکویان پذیر</p></div></div>