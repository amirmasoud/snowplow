---
title: >-
    بخش ۲۶۳ - شو پنهاور و نیچه
---
# بخش ۲۶۳ - شو پنهاور و نیچه

<div class="b" id="bn1"><div class="m1"><p>مرغی ز آشیانه به سیر چمن پرید</p></div>
<div class="m2"><p>خاری ز شاخ گل بتن نازکش خلید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد گفت فطرت چمن روزگار را</p></div>
<div class="m2"><p>از درد خویش و هم ز غم دیگران تپید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغی ز خون بی گنهی لاله را شمرد</p></div>
<div class="m2"><p>اندر طلسم غنچه فریب بهار دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت اندرین سرا که بنایش فتاده کج</p></div>
<div class="m2"><p>صبحی کجا که چرخ درو شامها نچید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نالید تا به حوصلهٔ آن نوا طراز</p></div>
<div class="m2"><p>خون گشت نغمه و ز دو چشمش فرو چکید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوز فغان او بدل هدهدی گرفت</p></div>
<div class="m2"><p>با نوک خویش خار ز اندام او کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتش که سود خویش ز جیب زیان بر آر</p></div>
<div class="m2"><p>گل از شکاف سینه زر ناب آفرید</p></div></div>
<div class="b2" id="bn8"><p>درمان ز درد ساز اگر خسته تن شوی</p>
<p>خوگر به خار شو که سراپا چمن شوی</p></div>