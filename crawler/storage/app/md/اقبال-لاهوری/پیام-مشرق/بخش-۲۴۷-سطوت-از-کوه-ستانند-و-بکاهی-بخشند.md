---
title: >-
    بخش ۲۴۷ - سطوت از کوه ستانند و بکاهی بخشند
---
# بخش ۲۴۷ - سطوت از کوه ستانند و بکاهی بخشند

<div class="b" id="bn1"><div class="m1"><p>سطوت از کوه ستانند و بکاهی بخشند</p></div>
<div class="m2"><p>کلهٔ جم به گدای سر راهی بخشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ره عشق فلان ابن فلان چیزی نسیت</p></div>
<div class="m2"><p>ید بیضای کلیمی به سیاهی بخشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه شاهی به جگر گوشهٔ سلطان ندهند</p></div>
<div class="m2"><p>گاه باشد که بزندانی چاهی بخشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فقر را نیز جهانبان و جهانگیر کنند</p></div>
<div class="m2"><p>که به این راه نشین تیغ نگاهی بخشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق پامال خرد گشت و جهان دیگر شد</p></div>
<div class="m2"><p>بود آیا که مرا رخصت آهی بخشند</p></div></div>