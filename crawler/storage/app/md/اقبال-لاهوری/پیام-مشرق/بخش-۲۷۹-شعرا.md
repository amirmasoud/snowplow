---
title: >-
    بخش ۲۷۹ - شعرا
---
# بخش ۲۷۹ - شعرا

<div class="b2" id="bn1"><p>برونینگ: </p></div>
<div class="b" id="bn2"><div class="m1"><p>بی پشت بود بادهٔ سر جوش زندگی</p></div>
<div class="m2"><p>آب خضر بگیرم و در ساغر افکنم</p></div></div>
<div class="b2" id="bn3"><p>بایرن: </p></div>
<div class="b" id="bn4"><div class="m1"><p>از منت خضر نتوان کرد سینه داغ</p></div>
<div class="m2"><p>آب از جگر بگیرم و در ساغر افکنم</p></div></div>
<div class="b2" id="bn5"><p>غالب: </p></div>
<div class="b" id="bn6"><div class="m1"><p>«تا باده تلخ تر شود و سینه ریش تر</p></div>
<div class="m2"><p>بگدازم آبگینه و در ساغر افکنم»</p></div></div>
<div class="b2" id="bn7"><p>رومی: </p></div>
<div class="b" id="bn8"><div class="m1"><p>آمیزشی کجا گهر پاک او کجا</p></div>
<div class="m2"><p>از تاک باده گیرم و در ساغر افکنم</p></div></div>