---
title: >-
    بخش ۲۸۰ - خرابات فرنگ
---
# بخش ۲۸۰ - خرابات فرنگ

<div class="b" id="bn1"><div class="m1"><p>دوش رفتم به تماشای خرابات فرنگ</p></div>
<div class="m2"><p>شوخ گفتاری رندی دلم از دست ربود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت این نیست کلیسا که بیابی در وی</p></div>
<div class="m2"><p>صحبت دخترک زهره وش و نای و سرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این خرابات فرنگ است و ز تأثیر میش</p></div>
<div class="m2"><p>آنچه مذموم شمارند ، نماید محمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیک و بد را به ترازوی دگر سنجیدیم</p></div>
<div class="m2"><p>چشمه ئی داشت ترازوی نصاری و یهود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوب ، زشت است اگر پنجهٔ گیرات شکست</p></div>
<div class="m2"><p>زشت ، خوب است اگر تاب و توان تو فزود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو اگر در نگری جز به ریا نیست حیات</p></div>
<div class="m2"><p>هر که اندر گرو صدق و صفا بود نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دعوی صدق و صفا پردهٔ ناموس ریاست</p></div>
<div class="m2"><p>پیر ما گفت مس از سیم بباید اندود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فاش گفتم بتو اسرار نهانخانهٔ زیست</p></div>
<div class="m2"><p>به کسی باز مگو تا که بیابی مقصود</p></div></div>