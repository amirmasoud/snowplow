---
title: >-
    بخش ۲۸۴ - آزادی بحر
---
# بخش ۲۸۴ - آزادی بحر

<div class="b" id="bn1"><div class="m1"><p>بطی می گفت بحر آزاد گردید</p></div>
<div class="m2"><p>چنین فرمان ز دیوان خضر رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهنگی گفت رو هر جا که خواهی</p></div>
<div class="m2"><p>ولی از ما نباید بی خبر رفت</p></div></div>