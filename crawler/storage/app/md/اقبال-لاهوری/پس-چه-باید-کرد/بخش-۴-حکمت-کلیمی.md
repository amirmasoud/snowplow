---
title: >-
    بخش ۴ - حکمت کلیمی
---
# بخش ۴ - حکمت کلیمی

<div class="b" id="bn1"><div class="m1"><p>تا نبوت حکم حق جاری کند</p></div>
<div class="m2"><p>پشت پا بر حکم سلطان میزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نگاهش قصر سلطان کهنه دیر</p></div>
<div class="m2"><p>غیرت او بر نتابد حکم غیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پخته سازد صحبتش هر خام را</p></div>
<div class="m2"><p>تازه غوغائی دهد ایام را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درس او «الله بس باقی هوس»</p></div>
<div class="m2"><p>تا نیفتد مرد حق در بند کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نم او آتش اندر شاخ تاک</p></div>
<div class="m2"><p>در کف خاک از دم او جان پاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معنی جبریل و قرآن است او</p></div>
<div class="m2"><p>فطرة الله را نگهبان است او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حکمتش برتر ز عقل ذوفنون</p></div>
<div class="m2"><p>از ضمیرش امتی آید برون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حکمرانی بی نیاز از تخت و تاج</p></div>
<div class="m2"><p>بی کلاه و بی سپاه و بی خراج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نگاهش فرودین خیزد ز دی</p></div>
<div class="m2"><p>درد هر خم تلخ تر گردد ز می </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اندر آه صبحگاه او حیات</p></div>
<div class="m2"><p>تازه از صبح نمودش کائنات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بحر و بر از زور طوفانش خراب</p></div>
<div class="m2"><p>در نگاه او پیام انقلاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درس «لا خوف علیهم» می دهد</p></div>
<div class="m2"><p>تا دلی در سینهٔ آدم نهد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عزم و تسلیم و رضا آموزدش</p></div>
<div class="m2"><p>در جهان مثل چراغ افروزدش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من نمیدانم چه افسون می کند</p></div>
<div class="m2"><p>روح را در تن دگرگون می کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صحبت او هر خزف را در کند</p></div>
<div class="m2"><p>حکمت او هر تهی را پر کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بندهٔ درمانده را گوید که خیز</p></div>
<div class="m2"><p>هر کهن معبود را کن ریز ریز»</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرد حق افسون این دیر کهن</p></div>
<div class="m2"><p>از دو حرف «ربی الاعلی» شکن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فقر خواهی از تهی دستی منال</p></div>
<div class="m2"><p>عافیت در حال و نی در جاه و مال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صدق و اخلاص و نیاز و سوز و درد</p></div>
<div class="m2"><p>نی زر و سیم و قماش سرخ و زرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگذر از کاؤس و کی ای زنده مرد </p></div>
<div class="m2"><p>طوف خود کن گرد ایوانی مگرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از مقام خویش دور افتاده ئی</p></div>
<div class="m2"><p>کرگسی کم کن که شاهین زاده ئی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرغک اندر شاخسار بوستان</p></div>
<div class="m2"><p>بر مراد خویش بندد آشیان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو که داری فکرت گردون مسیر</p></div>
<div class="m2"><p>خویش را از مرغکی کمتر مگیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دیگر این نه آسمان تعمیر کن</p></div>
<div class="m2"><p>بر مراد خود جهان تعمیر کن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون فنا اندر رضای حق شود</p></div>
<div class="m2"><p>بندهٔ مؤمن قضای حق شود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چار سوی با فضای نیلگون</p></div>
<div class="m2"><p>از ضمیر پاک او آید برون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در رضای حق فنا شو چون سلف</p></div>
<div class="m2"><p>گوهر خود را برون آر از صدف</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در ظلام این جهان سنگ و خشت</p></div>
<div class="m2"><p>چشم خود روشن کن از نور سرشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا نگیری از جلال حق نصیب</p></div>
<div class="m2"><p>هم نیابی از جمال حق نصیب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ابتدای عشق و مستی قاهری است</p></div>
<div class="m2"><p>انتهای عشق و مستی دلبری است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرد مؤمن از کمالات وجود</p></div>
<div class="m2"><p>او وجود و غیر او هر شی نمود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر بگیرد سوز و تاب از لااله</p></div>
<div class="m2"><p>جز بکام او نگردد مهر و مه</p></div></div>