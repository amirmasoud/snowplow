---
title: >-
    بخش ۲ - تمهید
---
# بخش ۲ - تمهید

<div class="b" id="bn1"><div class="m1"><p>پیر رومی مرشد روشن ضمیر</p></div>
<div class="m2"><p>کاروان عشق و مستی را امیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منزلش برتر ز ماه و آفتاب</p></div>
<div class="m2"><p>خیمه را از کهکشان سازد طناب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور قرآن در میان سینه اش</p></div>
<div class="m2"><p>جام جم شرمنده از آئینه اش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نی آن نی نواز پاکزاد</p></div>
<div class="m2"><p>باز شوری در نهاد من فتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت «جانها محرم اسرار شد</p></div>
<div class="m2"><p>خاور از خواب گران بیدار شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جذبه های تازه او را داده اند</p></div>
<div class="m2"><p>بندهای کهنه را بگشاده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز تو ای دانای اسرار فرنگ</p></div>
<div class="m2"><p>کس نکو ننشست در نار فرنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باش مانند خلیل الله مست</p></div>
<div class="m2"><p>هر کهن بتخانه را باید شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امتان را زندگی جذب درون</p></div>
<div class="m2"><p>کم نظر این جذب را گوید جنون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچ قومی زیر چرخ لاجورد</p></div>
<div class="m2"><p>بی جنون ذوفنون کاری نکرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مؤمن از عزم و توکل قاهر است</p></div>
<div class="m2"><p>گر ندارد این دو جوهر کافر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خیر را او باز میداند ز شر</p></div>
<div class="m2"><p>از نگاهش عالمی زیر و زبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کوهسار از ضربت او ریز ریز</p></div>
<div class="m2"><p>در گریبانش هزاران رستخیز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا می از میخانهٔ من خورده ئی</p></div>
<div class="m2"><p>کهنگی را از تماشا برده ئی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در چمن زی مثل بو مستور و فاش</p></div>
<div class="m2"><p>در میان رنگ پاک از رنگ باش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عصر تو از رمز جان آگاه نیست</p></div>
<div class="m2"><p>دین او جز حب غیر الله نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فلسفی این رمز کم فهمیده است</p></div>
<div class="m2"><p>فکر او بر آب و گل پیچیده است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دیده از قندیل دل روشن نکرد</p></div>
<div class="m2"><p>پس ندید الا کبود و سرخ و زرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای خوش آن مردی که دل با کس نداد</p></div>
<div class="m2"><p>بند غیر الله را از پا گشاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سر شیری را نفهمد گاو و میش</p></div>
<div class="m2"><p>جز به شیران کم بگو اسرار خویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با حریف سفله نتوان خورد می</p></div>
<div class="m2"><p>گرچه باشد پادشاه روم و ری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یوسف ما را اگر گرگی برد</p></div>
<div class="m2"><p>به که مردی ناکسی او را خرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اهل دنیا بی تخیل بی قیاس</p></div>
<div class="m2"><p>بوریا بافان اطلس ناشناس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اعجمی مردی چه خوش شعری سرود</p></div>
<div class="m2"><p>سوزد از تأثیر او جان در وجود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>«نالهٔ عاشق بگوش مردم دنیا</p></div>
<div class="m2"><p>بانگ مسلمانی و دیار فرنگ است»</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>معنی دین و سیاست باز گوی</p></div>
<div class="m2"><p>اهل حق را زین دو حکمت باز گوی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>«غم خور و نان غم افزایان مخور</p></div>
<div class="m2"><p>زانکه عاقل غم خورد کودک شکر»</p></div></div>
<div class="b2" id="bn28"><p>رومی</p></div>
<div class="b" id="bn29"><div class="m1"><p>خرقه خود بار است بر دوش فقیر</p></div>
<div class="m2"><p>چون صبا جز بوی گل سامان مگیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قلزمی با دشت و در پیهم ستیز</p></div>
<div class="m2"><p>شبنمی خود را به گلبرگی بریز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سر حق بر مرد حق پوشیده نیست</p></div>
<div class="m2"><p>روح مؤمن هیچ میدانی که چیست؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قطرهٔ شبنم که از ذوق نمود</p></div>
<div class="m2"><p>عقدهٔ خود را بدست خود گشود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از خودی اندر ضمیر خود نشست</p></div>
<div class="m2"><p>رخت خویش از خلوت افلاک بست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رخ سوی دریای بی پایان نکرد</p></div>
<div class="m2"><p>خویشتن را در صدف پنهان نکرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اندر آغوش سحر یکدم تپید</p></div>
<div class="m2"><p>تا بکام غنچهٔ نورس چکید»</p></div></div>