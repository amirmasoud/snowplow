---
title: >-
    بخش ۲۲ - قندهار و زیارت خرقه مبارک
---
# بخش ۲۲ - قندهار و زیارت خرقه مبارک

<div class="b" id="bn1"><div class="m1"><p>قندهار آن کشور مینو سواد</p></div>
<div class="m2"><p>اهل دل را خاک او خاک مراد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ ها ، بوها ، هواها ، آب ها</p></div>
<div class="m2"><p>آب ها تابنده چون سیماب ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله ها در خلوت کهسار ها</p></div>
<div class="m2"><p>نارها یخ بسته اندر نارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوی آن شهر است ما را کوی دوست</p></div>
<div class="m2"><p>ساربان بر بند محمل سوی دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می سرایم دیگر از یاران نجد</p></div>
<div class="m2"><p>از نوائی ، ناقه را آرم به وجد</p></div></div>
<div class="b2" id="bn6"><p>غزل</p></div>
<div class="b" id="bn7"><div class="m1"><p>از دیر مغان آیم بی گردش صهباست</p></div>
<div class="m2"><p>در منزل لا بودم از باده الا مست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانم که نگاه او ظرف همه کس بیند</p></div>
<div class="m2"><p>کرد است مرا ساقی از عشوه و ایما مست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وقت است که بگشایم میخانهٔ رومی باز</p></div>
<div class="m2"><p>پیران حرم دیدم در صحن کلیسا مست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این کار حکیمی نیست دامان کلیمی گیر</p></div>
<div class="m2"><p>صد بندهٔ ساحل مست یک بنده دریا مست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل را به چمن بردم از باد چمن افسرد</p></div>
<div class="m2"><p>میرد بخیابانها این لالهٔ صحرا مست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از حرف دلاویزش اسرار حرم پیدا</p></div>
<div class="m2"><p>دی کافرکی دیدم در وادی بطحا مست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سینا است که فاران است یارب چه مقام است این</p></div>
<div class="m2"><p>هر ذره خاک من چشمی است تماشا مست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرقهٔ آن «برزخ لایبغیان»</p></div>
<div class="m2"><p>دیدمش در نکتهٔ «لی خرقتان»</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دین او آئین او تفسیر کل</p></div>
<div class="m2"><p>در جبین او خط تقدیر کل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عقل را او صاحب اسرار کرد</p></div>
<div class="m2"><p>عشق را او تیغ جوهر دار کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کاروان شوق را او منزل است</p></div>
<div class="m2"><p>ما همه یک مشت خاکیم او دل است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آشکارا دیدنش اسرای ماست</p></div>
<div class="m2"><p>در ضمیرش مسجد اقصای ماست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آمد از پیراهن او بوی او</p></div>
<div class="m2"><p>داد ما را نعره الله هو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با دل من شوق بی پروا چه کرد</p></div>
<div class="m2"><p>بادهٔ پر زور با مینا چه کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رقصد اندر سینه از زور جنون</p></div>
<div class="m2"><p>تا ز راه دیده میآید برون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت «من جبریلم و نور مبین»</p></div>
<div class="m2"><p>پیش ازین او را ندیدم اینچنین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شعر رومی خواند و خندید و گریست</p></div>
<div class="m2"><p>یا رب این دیوانهٔ فرزانه کیست؟ </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در حرم با من سخن زندانه گفت</p></div>
<div class="m2"><p>از می و مغ زاده و پیمانه گفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفتمش این حرف بیباکانه چیست</p></div>
<div class="m2"><p>لب فرو بند این مقام خامشی ست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من ز خون خویش پروردم ترا</p></div>
<div class="m2"><p>صاحب آه سحر کردم ترا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بازیاب این نکته را ای نکته رس</p></div>
<div class="m2"><p>عشق مردان ضبط احوال است و بس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت عقل و هوش آزار دل است</p></div>
<div class="m2"><p>مستی و وارفتگی کار دل است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نعره ها زد تا فتاد اندر سجود</p></div>
<div class="m2"><p>شعلهٔ آواز او بود او نبود</p></div></div>