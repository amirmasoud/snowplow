---
title: >-
    بخش ۱۱ - سیاسیات حاضره
---
# بخش ۱۱ - سیاسیات حاضره

<div class="b" id="bn1"><div class="m1"><p>می کند بند غلامان سخت تر</p></div>
<div class="m2"><p>حریت می خواند او را بی بصر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرمی هنگامهٔ جمهور دید</p></div>
<div class="m2"><p>پرده بر روی ملوکیت کشید </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلطنت را جامع اقوام گفت</p></div>
<div class="m2"><p>کار خود را پخته کرد و خام گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در فضایش بال و پر نتوان گشود</p></div>
<div class="m2"><p>با کلیدش هیچ در نتوان گشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت با مرغ قفس «ای دردمند</p></div>
<div class="m2"><p>آشیان در خانهٔ صیاد بند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که سازد آشیان در دشت و مرغ»</p></div>
<div class="m2"><p>او نباشد ایمن از شاهین و چرغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از فسونش مرغ زیرک دانه مست</p></div>
<div class="m2"><p>ناله ها اندر گلوی خود شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حریت خواهی به پیچاکش میفت</p></div>
<div class="m2"><p>تشنه میر و بر نم تاکش میفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الحذر از گرمی گفتار او</p></div>
<div class="m2"><p>الحذر از حرف پهلو دار او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم ها از سرمه اش بی نور تر</p></div>
<div class="m2"><p>بندهٔ مجبور ازو مجبور تر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از شراب ساتگینش الحذر</p></div>
<div class="m2"><p>از قمار بدنشینش الحذر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خودی غافل نگردد مرد حر</p></div>
<div class="m2"><p>حفظ خود کن حب افیونش مخور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش فرعونان بگو حرف کلیم</p></div>
<div class="m2"><p>تا کند ضرب تو دریا را دونیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داغم از رسوائی این کاروان</p></div>
<div class="m2"><p>در امیر او ندیدم نور جان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تن پرست و جاه مست و کم نگه</p></div>
<div class="m2"><p>اندرونش بی نصیب از لااله</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در حرم زاد و کلیسا را مرید</p></div>
<div class="m2"><p>پردهٔ ناموس ما را بر درید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دامن او را گرفتن ابلهی است</p></div>
<div class="m2"><p>سینهٔ او از دل روشن تهی است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اندرین ره تکیه بر خود کن که مرد</p></div>
<div class="m2"><p>صید آهو با سگ کوری نکرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آه از قومی که چشم از خویش بست</p></div>
<div class="m2"><p>دل به غیر الله داد ، از خود گسست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا خودی در سینهٔ ملت بمرد</p></div>
<div class="m2"><p>کوه ، کاهی کرد و باد او را ببرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرچه دارد لااله اندر نهاد</p></div>
<div class="m2"><p>از بطون او مسلمانی نزاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنکه بخشد بی یقینان را یقین</p></div>
<div class="m2"><p>آنکه لرزد از سجود او زمین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنکه زیر تیغ گوید لااله</p></div>
<div class="m2"><p>آنکه از خونش بروید لااله</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن سرور ، آن سوز مشتاقی نماند</p></div>
<div class="m2"><p>در حرم صاحبدلی باقی نماند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای مسلمان اندرین دیر کهن</p></div>
<div class="m2"><p>تا کجا باشی به بند اهرمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهد با توفیق و لذت در طلب</p></div>
<div class="m2"><p>کس نیابد بی نیاز نیم شب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زیستن تا کی به بحر اندر چو خس</p></div>
<div class="m2"><p>سخت شو چون کوه از ضبط نفس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گرچه دانا حال دل با کس نگفت</p></div>
<div class="m2"><p>از تو درد خویش نتوانم نهفت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا غلامم در غلامی زاده ام</p></div>
<div class="m2"><p>ز آستان کعبه دور افتاده ام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون بنام مصطفی خوانم درود</p></div>
<div class="m2"><p>از خجالت آب می گردد وجود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عشق میگوید که ای محکوم غیر</p></div>
<div class="m2"><p>سینهٔ تو از بتان مانند دیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا نداری از محمد رنگ و بو</p></div>
<div class="m2"><p>از درود خود میالا نام او</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از قیام بی حضور من مپرس</p></div>
<div class="m2"><p>از سجود بی سرور من مپرس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جلوهٔ حق گرچه باشد یک نفس</p></div>
<div class="m2"><p>قسمت مردان آزاد است و بس</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مردی آزادی چو آید در سجود</p></div>
<div class="m2"><p>در طوافش گرم رو چرخ کبود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ما غلامان از جلالش بیخبر</p></div>
<div class="m2"><p>از جمال لازوالش بیخبر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از غلامی لذت ایمان مجو</p></div>
<div class="m2"><p>گرچه باشد حافظ قرآن ، مجو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مؤمن است و پیشهٔ او آزری است</p></div>
<div class="m2"><p>دین و عرفانش سراپا کافری است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در بدن داری اگر سوز حیات</p></div>
<div class="m2"><p>هست معراج مسلمان در صلوت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ور نداری خون گرم اندر بدن</p></div>
<div class="m2"><p>سجدهٔ تو نیست جز رسم کهن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عید آزادان شکوه ملک و دین</p></div>
<div class="m2"><p>عید محکومان هجوم مؤمنین</p></div></div>