---
title: >-
    بخش ۱۰ - اشکی چند بر افتراق هندیان
---
# بخش ۱۰ - اشکی چند بر افتراق هندیان

<div class="b" id="bn1"><div class="m1"><p>ای هماله ! ای اطک ، ای رود گنگ</p></div>
<div class="m2"><p>زیستن تا کی چنان بی آب و رنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیر مردان از فراست بی نصیب</p></div>
<div class="m2"><p>نوجوانان از محبت بی نصیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرق و غرب آزاد و ما نخچیر غیر</p></div>
<div class="m2"><p>خشت ما سرمایهٔ تعمیر غیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زندگانی بر مراد دیگران</p></div>
<div class="m2"><p>جاودان مرگست ، نی خواب گران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست این مرگی که آید ز آسمان</p></div>
<div class="m2"><p>تخم او می بالد ز اعماق جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صید او نی مرده شو خواهد نه گور</p></div>
<div class="m2"><p>نی هجوم دوستان از نزد و دور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جامهٔ کس در غم او چاک نیست</p></div>
<div class="m2"><p>دوزخ او آنسوی افلاک نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هجوم روز حشر او را مجو</p></div>
<div class="m2"><p>هست در امروز او فردای او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که اینجا دانه کشت اینجا درود</p></div>
<div class="m2"><p>پیش حق آن بنده را بردن چه سود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امتی کز آرزو نیشی نخورد</p></div>
<div class="m2"><p>نقش او را فطرت از گیتی سترد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اعتبار تخت و تاج از ساحری است</p></div>
<div class="m2"><p>سخت چون سنگ این زجاج از ساحریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در گذشت از حکم این سحر مبین</p></div>
<div class="m2"><p>کافری از کفر ، دینداری ز دین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هندیان با یکدگر آویختند</p></div>
<div class="m2"><p>فتنه های کهنه باز انگیختند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا فرنگی قومی از مغرب زمین</p></div>
<div class="m2"><p>ثالث آمد در نزاع کفر و دین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کس نداند جلوهٔ آب از سراب</p></div>
<div class="m2"><p>انقلاب ای انقلاب ای انقلاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای ترا هر لحظه فکر آب و گل</p></div>
<div class="m2"><p>از حضور حق طلب یک زنده دل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آشیانش گرچه در آب و گل است</p></div>
<div class="m2"><p>نه فلک سر گشته این یک دل است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا نپنداری که از خاک است او</p></div>
<div class="m2"><p>از بلندی های افلاک است او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این جهان او را حریم کوی دوست</p></div>
<div class="m2"><p>از قبای لاله گیرد بوی دوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر نفس با روزگار اندر ستیز</p></div>
<div class="m2"><p>سنگ ره از ضربت او ریز ریز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آشنای منبر و دار است او</p></div>
<div class="m2"><p>آتش خود را نگهدار است او</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آب جوی و بحر ها دارد به بر</p></div>
<div class="m2"><p>می دهد موجش ز طوفانی خبر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زنده و پاینده بی نان تنور</p></div>
<div class="m2"><p>میرد آن ساعت که گردد بی حضور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون چراغ اندر شبستان بدن</p></div>
<div class="m2"><p>روشن از وی خلوت و هم انجمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اینچنین دل ، خود نگر ، الله مست</p></div>
<div class="m2"><p>جز به درویشی نمی آید بدست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای جوان دامان او محکم بگیر</p></div>
<div class="m2"><p>در غلامی زاده ئی آزاد میر</p></div></div>