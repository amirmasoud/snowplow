---
title: >-
    بخش ۸ - مرد حر
---
# بخش ۸ - مرد حر

<div class="b" id="bn1"><div class="m1"><p>مرد حر محکم ز ورد «لاتخف»</p></div>
<div class="m2"><p>ما بمیدان سر بجیب او سر بکف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرد حر از لااله روشن ضمیر</p></div>
<div class="m2"><p>می نگردد بندهٔ سلطان و میر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد حر چون اشتران باری برد</p></div>
<div class="m2"><p>مرد حر باری برد خاری خورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای خود را آنچنان محکم نهد</p></div>
<div class="m2"><p>نبض ره از سوز او بر می جهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان او پاینده تر گردد ز موت</p></div>
<div class="m2"><p>بانگ تکبیرش برون از حرف و صوت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که سنگ راه را داند زجاج</p></div>
<div class="m2"><p>گیرد آن درویش از سلطان خراج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرمی طبع تو از صهبای اوست</p></div>
<div class="m2"><p>جوی تو پروردهٔ دریای اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پادشاهان در قباهای حریر</p></div>
<div class="m2"><p>زرد رو از سهم آن عریان فقیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر دین ما را خبر ، او را نظر</p></div>
<div class="m2"><p>او درون خانه ما بیرون در</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما کلیسا دوست ، ما مسجد فروش</p></div>
<div class="m2"><p>او ز دست مصطفی پیمانه نوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نی مغان را بنده ، نی ساغر بدست</p></div>
<div class="m2"><p>ما تهی پیمانه او مست الست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چهره گل از نم او احمر است</p></div>
<div class="m2"><p>ز آتش ما دود او روشنتر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دارد اندر سینه تکبیر امم</p></div>
<div class="m2"><p>در جبین اوست تقدیر امم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قبلهٔ ما گه کلیسا ، گاه دیر</p></div>
<div class="m2"><p>او نخواهد رزق خویش از دست غیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ما همه عبد فرنگ او عبده </p></div>
<div class="m2"><p>او نگنجد در جهان رنگ و بو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صبح و شام ما به فکر ساز و برگ</p></div>
<div class="m2"><p>آخر ما چیست تلخیهای مرگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در جهان بی ثبات او را ثبات</p></div>
<div class="m2"><p>مرگ او را از مقامات حیات</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اهل دل از صحبت ما مضمحل</p></div>
<div class="m2"><p>گل ز فیض صحبتش دارای دل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کار ما وابستهٔ تخمین و ظن</p></div>
<div class="m2"><p>او همه کردار و کم گوید سخن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ما گدایان کوچه گرد و فاقه مست</p></div>
<div class="m2"><p>فقر او از لااله تیغی بدست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ما پر کاهی اسیر گرد باد</p></div>
<div class="m2"><p>ضربش از کوه گران جوئی گشاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>محرم او شو ز ما بیگانه شو</p></div>
<div class="m2"><p>خانه ویران باش و صاحب خانه شو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شکوه کم کن از سپهر گرد گرد</p></div>
<div class="m2"><p>زنده شو از صحبت آن زنده مرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صحبت از علم کتابی خوشتر است</p></div>
<div class="m2"><p>صحبت مردان حر آدم گر است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرد حر دریای ژرف و بیکران</p></div>
<div class="m2"><p>آب گیر از بحر و نی از ناودان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سینهٔ این مردمی جوشد چو دیگ</p></div>
<div class="m2"><p>پیش او کوه گران یک توده ریگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روز صلح آن برگ و ساز انجمن</p></div>
<div class="m2"><p>هم چو باد فرودین اندر چمن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روز کین آن محرم تقدیر خویش</p></div>
<div class="m2"><p>گور خود می کندد از شمشیر خویش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای سرت گردم گریز از ما چو تیر</p></div>
<div class="m2"><p>دامن او گیر و بیتابانه گیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>می نروید تخم دل از آب و گل</p></div>
<div class="m2"><p>بی نگاهی از خداوندان دل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اندر این عالم نیرزی با خسی</p></div>
<div class="m2"><p>تا نیاویزی بدامان کسی</p></div></div>