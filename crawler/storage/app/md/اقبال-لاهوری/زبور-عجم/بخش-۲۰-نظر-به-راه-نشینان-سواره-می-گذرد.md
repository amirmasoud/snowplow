---
title: >-
    بخش ۲۰ - نظر به راه‌نشینان سواره می‌گذرد
---
# بخش ۲۰ - نظر به راه‌نشینان سواره می‌گذرد

<div class="b" id="bn1"><div class="m1"><p>نظر به راه‌نشینان سواره می‌گذرد</p></div>
<div class="m2"><p>مرا بگیر که کارم ز چاره می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دیگران چه سخن گسترم ز جلوهٔ دوست</p></div>
<div class="m2"><p>به یک نگاه مثال شراره می‌گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهی به منزل آن ماه سخت دشوار است</p></div>
<div class="m2"><p>چنانکه عشق به دوش ستاره می‌گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پرده‌بندی گردون چه جای نومیدی‌ست</p></div>
<div class="m2"><p>که ناوک نظر ما ز خاره می‌گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یمی است شبنم ما کهکشان کنارهٔ اوست</p></div>
<div class="m2"><p>به یک شکستن موج از کناره می‌گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خلوتش چو رسیدی نظر به او مگشا</p></div>
<div class="m2"><p>که آن دمی است که کار از نظاره می‌گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من از فراق چه نالم که از هجوم سرشک</p></div>
<div class="m2"><p>ز راه دیده دلم پاره‌پاره می‌گذرد</p></div></div>