---
title: >-
    بخش ۱۱۴ - قلندران که به تسخیر آب و گل کوشند
---
# بخش ۱۱۴ - قلندران که به تسخیر آب و گل کوشند

<div class="b" id="bn1"><div class="m1"><p>قلندران که به تسخیر آب و گل کوشند</p></div>
<div class="m2"><p>ز شاه باج ستانند و خرقه می پوشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جلوت اند و کمندی به مهر و مه پیچند</p></div>
<div class="m2"><p>به خلوت اند و زمان و مکان در آغوشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بروز بزم سراپا چو پرنیان و حریر</p></div>
<div class="m2"><p>بروز رزم خود آگاه و تن فراموشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظام تازه بچرخ دو رنگ می بخشند</p></div>
<div class="m2"><p>ستاره های کهن را جنازه بر دوشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانه از رخ فردا گشود بند نقاب</p></div>
<div class="m2"><p>معاشران همه سر مست بادهٔ دوشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلب رسید مرا آن سخن که نتوان گفت</p></div>
<div class="m2"><p>بحیرتم که فقیهان شهر خاموشند</p></div></div>