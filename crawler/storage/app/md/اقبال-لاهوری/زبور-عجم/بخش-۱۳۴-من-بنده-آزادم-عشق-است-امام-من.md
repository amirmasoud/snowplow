---
title: >-
    بخش ۱۳۴ - من بندهٔ آزادم عشق است امام من
---
# بخش ۱۳۴ - من بندهٔ آزادم عشق است امام من

<div class="b" id="bn1"><div class="m1"><p>من بندهٔ آزادم عشق است امام من</p></div>
<div class="m2"><p>عشق است امام من عقل است غلام من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنگامهٔ این محفل از گردش جام من</p></div>
<div class="m2"><p>این کوکب شام من این ماه تمام من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان در عدم آسودهٔ بی ذوق تمنا بود</p></div>
<div class="m2"><p>مستانه نوا ها زد در حلقه دام من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای عالم رنگ و بو این صحبت ما تا چند</p></div>
<div class="m2"><p>مرگست دوام تو عشق است دوام من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیدا به ضمیرم او پنهان به ضمیرم او</p></div>
<div class="m2"><p>این است مقام او دریاب مقام من</p></div></div>