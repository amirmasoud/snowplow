---
title: >-
    بخش ۱۰۷ - ز رسم و راه شریعت نکرده ام تحقیق
---
# بخش ۱۰۷ - ز رسم و راه شریعت نکرده ام تحقیق

<div class="b" id="bn1"><div class="m1"><p>ز رسم و راه شریعت نکرده ام تحقیق</p></div>
<div class="m2"><p>جز اینکه منکر عشق است کافر و زندیق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقام آدم خاکی نهاد دریا بند</p></div>
<div class="m2"><p>مسافران حرم را خدا دهد توفیق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از طریق نپرسم ، رفیق می جویم</p></div>
<div class="m2"><p>که گفته اند نخستین رفیق و باز طریق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند تلافی ذوق آنچنان حکیم فرنگ</p></div>
<div class="m2"><p>فروغ باده فزون تر کند بجام عقیق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار بار نکو تر متاع بی بصری</p></div>
<div class="m2"><p>ز دانشی که دل او را نمی کند تصدیق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیچ و تاب خرد گرچه لذت دگر است</p></div>
<div class="m2"><p>یقین ساده دلان به ز نکته های دقیق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلام و فلسفه از لوح دل فروشستم</p></div>
<div class="m2"><p>ضمیر خویش گشادم به نشتر تحقیق </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آستانهٔ سلطان کناره می گیرم</p></div>
<div class="m2"><p>نه کافرم که پرستم خدای بی توفیق</p></div></div>