---
title: >-
    بخش ۷۴ - ز سلطان کنم آرزوی نگاهی
---
# بخش ۷۴ - ز سلطان کنم آرزوی نگاهی

<div class="b" id="bn1"><div class="m1"><p>ز سلطان کنم آرزوی نگاهی</p></div>
<div class="m2"><p>مسلمانم از گل نسازم الهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بی نیازی که در سینه دارم</p></div>
<div class="m2"><p>گدارا دهد شیوهٔ پادشاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز گردون فتد آنچه بر لالهٔ من</p></div>
<div class="m2"><p>فرو ریزم او را به برگ گیاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو پروین فرو ناید اندیشهٔ من</p></div>
<div class="m2"><p>به دریوزهٔ پرتو مهر و ماهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر آفتابی سوی من خرامد</p></div>
<div class="m2"><p>به شوخی بگردانم او را ز راهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آن آب و تابی که فطرت ببخشد</p></div>
<div class="m2"><p>درخشم چو برقی به ابر سیاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ره و رسم فرمانروایان شناسم</p></div>
<div class="m2"><p>خران بر سر بام و یوسف بچاهی</p></div></div>