---
title: >-
    بخش ۶۷ - دگر ز ساده دلیهای یار نتوان گفت
---
# بخش ۶۷ - دگر ز ساده دلیهای یار نتوان گفت

<div class="b" id="bn1"><div class="m1"><p>دگر ز ساده دلیهای یار نتوان گفت</p></div>
<div class="m2"><p>نشسته بر سر بالین من ز درمان گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبان اگرچه دلیر است و مدعا شیرین</p></div>
<div class="m2"><p>سخن ز عشق چه گویم جز اینکه نتوان گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشا کسی که فرو رفت در ضمیر وجود</p></div>
<div class="m2"><p>سخن مثال گهر بر کشید و آسان گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خراب لذت آنم که چون شناخت مرا</p></div>
<div class="m2"><p>عتاب زیر لبی کرد و خانه ویران گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمین مشو که جهان راز خود برون ندهد</p></div>
<div class="m2"><p>که آنچه گل نتوانست مرغ نالان گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیام شوق که من بی حجاب می گویم</p></div>
<div class="m2"><p>به لاله قطره شبنم رسید و پنهان گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر سخن همه شوریده گفته ام چه عجب</p></div>
<div class="m2"><p>که هر که گفت ز گیسوی او پریشان گفت</p></div></div>