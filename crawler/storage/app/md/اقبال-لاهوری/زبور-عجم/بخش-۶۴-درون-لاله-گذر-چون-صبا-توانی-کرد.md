---
title: >-
    بخش ۶۴ - درون لاله گذر چون صبا توانی کرد
---
# بخش ۶۴ - درون لاله گذر چون صبا توانی کرد

<div class="b" id="bn1"><div class="m1"><p>درون لاله گذر چون صبا توانی کرد</p></div>
<div class="m2"><p>بیک نفس گره غنچه وا توانی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیات چیست جهان را اسیر جان کردن</p></div>
<div class="m2"><p>تو خود اسیر جهانی کجا توانی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقدر است که مسجود مهر و مه باشی</p></div>
<div class="m2"><p>ولی هنوز ندانی چها توانی کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر ز میکدهٔ من پیاله ئی گیری</p></div>
<div class="m2"><p>ز مشت خاک جهانی بپا توانی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چسان به سینه چراغی فروختی اقبال</p></div>
<div class="m2"><p>به خویش آنچه توانی به ما توانی کرد</p></div></div>