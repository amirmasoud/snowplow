---
title: >-
    بخش ۷۸ - عرب که باز دهد محفل شبانه کجاست
---
# بخش ۷۸ - عرب که باز دهد محفل شبانه کجاست

<div class="b" id="bn1"><div class="m1"><p>عرب که باز دهد محفل شبانه کجاست</p></div>
<div class="m2"><p>عجم که زنده کند رود عاشقانه کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزیر خرقهٔ پیران سبوها چه خالی است</p></div>
<div class="m2"><p>فغان که کس نشناسد می جوانه کجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین چمنکده هر کس نشیمنی سازد</p></div>
<div class="m2"><p>کسی که سازد و وا سوزد آشیانه کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار قافله بیگانه وار دید و گذشت</p></div>
<div class="m2"><p>دلی که دید به انداز محرمانه کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو موج خیز و به یم جاودانه می آویز</p></div>
<div class="m2"><p>کرانه می طلبی؟ بی خبر کرانه کجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا که در رگ تاک تو خون تازه دوید</p></div>
<div class="m2"><p>دگر مگوی که آن بادهٔ مغانه کجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیک نورد فرو پیچ روزگاران را</p></div>
<div class="m2"><p>ز دیر و زود گذشتی دگر زمانه کجاست</p></div></div>