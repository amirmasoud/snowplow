---
title: >-
    بخش ۱۰۳ - بیا که خاوریان نقش تازه‌ای بستند
---
# بخش ۱۰۳ - بیا که خاوریان نقش تازه‌ای بستند

<div class="b" id="bn1"><div class="m1"><p>بیا که خاوریان نقش تازه‌ای بستند</p></div>
<div class="m2"><p>دگر مرو به طواف بتی که بشکستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه جلوه‌ای‌ست که دل‌ها به لذت نگهی</p></div>
<div class="m2"><p>ز خاک راه مثال شراره برجَستند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجاست منزل تورانیان شهرآشوب</p></div>
<div class="m2"><p>که سینه‌های خود از تیزی نفس خستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو هم به ذوق خودی رس که صاحبان طریق</p></div>
<div class="m2"><p>بریده از همه عالم به خویش پیوستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چشم مرده‌دلان کائنات زندانی است</p></div>
<div class="m2"><p>دو جام باده کشیدند و از جهان رستند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غلام همت بیدار آن سوارانم</p></div>
<div class="m2"><p>ستاره را به سنان سُفته در گره بستند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرشته را دگر آن فرصت سجود کجاست</p></div>
<div class="m2"><p>که نوریان به تماشای خاکیان مستند</p></div></div>