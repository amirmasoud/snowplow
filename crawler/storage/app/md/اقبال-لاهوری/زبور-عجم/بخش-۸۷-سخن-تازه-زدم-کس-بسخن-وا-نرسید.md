---
title: >-
    بخش ۸۷ - سخن تازه زدم کس بسخن وا نرسید
---
# بخش ۸۷ - سخن تازه زدم کس بسخن وا نرسید

<div class="b" id="bn1"><div class="m1"><p>سخن تازه زدم کس بسخن وا نرسید</p></div>
<div class="m2"><p>جلوه خون گشت و نگاهی بتماشا نرسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگ می باش و درین کارگه شیشه گذر</p></div>
<div class="m2"><p>وای سنگی که صنم گشت و به مینا نرسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کهنه را در شکن و باز به تعمیر خرام</p></div>
<div class="m2"><p>هر که در ورطهٔ «لا» ماند به «الا‘ نرسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایخوش آن جوی تنک مایه که از ذوق خودی</p></div>
<div class="m2"><p>در دل خاک فرو رفت و بدریا نرسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کلیمی سبق آموز که دانای فرنگ</p></div>
<div class="m2"><p>جگر بحر شکافید و به سینا نرسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق انداز تپیدن ز دل ما آموخت</p></div>
<div class="m2"><p>شرر ماست که برجست و به پروانه رسید</p></div></div>