---
title: >-
    بخش ۱۴۲ - در فن تعمیر مردان آزاد
---
# بخش ۱۴۲ - در فن تعمیر مردان آزاد

<div class="b" id="bn1"><div class="m1"><p>یک زمان با رفتگان صحبت گزین</p></div>
<div class="m2"><p>صنعت آزاد مردان هم ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیز و کار ایبک و سوری نگر</p></div>
<div class="m2"><p>وا نما چشمی اگر داری جگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خویش را از خود برون آورده اند</p></div>
<div class="m2"><p>این چنین خود را تماشا کرده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنگها با سنگها پیوسته اند</p></div>
<div class="m2"><p>روزگاری را به آنی بسته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدن او پخته تر سازد ترا</p></div>
<div class="m2"><p>در جهان دیگر اندازد ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش سوی نقشگر می آورد</p></div>
<div class="m2"><p>از ضمیر او خبر می آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همت مردانه و طبع بلند</p></div>
<div class="m2"><p>در دل سنگ این دو لعل ارجمند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سجده گاه کیست این از من مپرس</p></div>
<div class="m2"><p>بی خبر روداد جان از تن مپرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وای من از خویشتن اندر حجاب</p></div>
<div class="m2"><p>از فرات زندگی ناخورده آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وای من از بیخ و بن بر کنده ئی</p></div>
<div class="m2"><p>از مقام خویش دور افکنده ئی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محکمی ها از یقین محکم است</p></div>
<div class="m2"><p>وای من شاخ یقینم بی نم است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در من آن نیروی الا الله نیست</p></div>
<div class="m2"><p>سجده ام شایان این درگاه نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک نظر آن گوهر نابی نگر</p></div>
<div class="m2"><p>تاج را در زیر مهتابی نگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرمرش ز آب روان گردنده تر</p></div>
<div class="m2"><p>یک دم آنجا از ابد پاینده تر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عشق مردان سر خود را گفته است</p></div>
<div class="m2"><p>سنگ را با نوک مژگان سفته است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عشق مردان پاک و رنگین چون بهشت</p></div>
<div class="m2"><p>می گشاید نغمه ها از سنگ و خشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عشق مردان نقد خوبان را عیار</p></div>
<div class="m2"><p>حسن را هم پرده در هم پرده دار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همت او آنسوی گردون گذشت</p></div>
<div class="m2"><p>از جهان چند و چون بیرون گذشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زانکه در گفتن نیاید آنچه دید</p></div>
<div class="m2"><p>از ضمیر خود نقابی بر کشید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از محبت جذبه ها گردد بلند</p></div>
<div class="m2"><p>ارج می گیرد ازو ناارجمند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بی محبت زندگی ماتم همه</p></div>
<div class="m2"><p>کاروبارش زشت و نامحکم همه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عشق صیقل می زند فرهنگ را</p></div>
<div class="m2"><p>جوهر آئینه بخشد سنگ را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اهل دل را سینهٔ سینا دهد</p></div>
<div class="m2"><p>با هنرمندان ید بیضا دهد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیش او هر ممکن و موجود مات</p></div>
<div class="m2"><p>جمله عالم تلخ و او شاخ نبات</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرمی افکار ما از نار اوست</p></div>
<div class="m2"><p>آفریدن جان دمیدن کار اوست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عشق مور و مرغ و آدم را بس است</p></div>
<div class="m2"><p>«عشق تنها هر دو عالم را بس است»</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دلبری بی قاهری جادوگری است</p></div>
<div class="m2"><p>دلبری با قاهری پیغمبری است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر دو را در کار ها آمیخت عشق</p></div>
<div class="m2"><p>عالمی در عالمی انگیخت عشق</p></div></div>