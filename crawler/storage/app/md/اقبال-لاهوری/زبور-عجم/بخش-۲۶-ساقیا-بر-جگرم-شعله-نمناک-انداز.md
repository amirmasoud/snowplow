---
title: >-
    بخش ۲۶ - ساقیا بر جگرم شعلهٔ نمناک انداز
---
# بخش ۲۶ - ساقیا بر جگرم شعلهٔ نمناک انداز

<div class="b" id="bn1"><div class="m1"><p>ساقیا بر جگرم شعلهٔ نمناک انداز</p></div>
<div class="m2"><p>دگر آشوب قیامت به کف خاک انداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او بیک دانهٔ گندم به زمینم انداخت</p></div>
<div class="m2"><p>تو بیک جرعه آب آنسوی افلاک انداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق را باده مرد افکن و پرزور بده</p></div>
<div class="m2"><p>لای این باده به پیمانه ادراک انداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حکمت و فلسفه کرده است گران خیز مرا</p></div>
<div class="m2"><p>خضر من از سرم این بار گران پاک انداز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرد از گرمی صهبا بگدازی نرسید</p></div>
<div class="m2"><p>چارهٔ کار به آن غمزه چالاک انداز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزم در کشمکش بیم و امید است هنوز</p></div>
<div class="m2"><p>همه را بی خبر از گردش افلاک انداز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میتوان ریخت در آغوش خزان لاله و گل</p></div>
<div class="m2"><p>خیز و بر شاخ کهن خون رگ تاک انداز</p></div></div>