---
title: >-
    بخش ۱۴۱ - مذهب غلامان
---
# بخش ۱۴۱ - مذهب غلامان

<div class="b" id="bn1"><div class="m1"><p>در غلامی عشق و مذهب را فراق</p></div>
<div class="m2"><p>انگبین زندگانی بد مذاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقی ، توحید را بر دل زدن</p></div>
<div class="m2"><p>وانگهی خود را بهر مشکل زدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غلامی عشق جز گفتار نیست</p></div>
<div class="m2"><p>کار ما گفتار ما را یار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاروان شوق بی ذوق رحیل</p></div>
<div class="m2"><p>بی یقین و بی سبیل و بی دلیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دین و دانش را غلام ارزان دهد</p></div>
<div class="m2"><p>تا بدن را زنده دارد جان دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه بر لبهای او نام خداست</p></div>
<div class="m2"><p>قبلهٔ او طاقت فرمانرواست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طاقتی نامش دروغ با فروغ</p></div>
<div class="m2"><p>از بطون او نزاید جز دروغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این صنم تا سجده اش کردی خداست</p></div>
<div class="m2"><p>چون یکی اندر قیام آئی فناست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن خدا نانی دهد جانی دهد</p></div>
<div class="m2"><p>این خدا جانی برد نانی دهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن خدا یکتا ست این صد پاره ایست</p></div>
<div class="m2"><p>آن همه را چاره این بیچاره ایست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن خدا درمان آزار فراق</p></div>
<div class="m2"><p>این خدا اندر کلام او نفاق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنده را با خویشتن خوگر کند</p></div>
<div class="m2"><p>چشم و گوش و هوش را کافر کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون بجان عبد خود راکب شود</p></div>
<div class="m2"><p>جان به تن لیکن ز تن غایب شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زنده و بیجان چه رازست این نگر</p></div>
<div class="m2"><p>با تو گویم معنی رنگین نگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مردن و هم زیستن ای نکته رس</p></div>
<div class="m2"><p>این همه از اعتبارات است و بس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ماهیان را کوه و صحرا بی وجود</p></div>
<div class="m2"><p>بهر مرغان قعر دریا بی وجود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرد کر سوز نوا را مرده ئی</p></div>
<div class="m2"><p>لذت صوت و صدا را مرده ئی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیش چنگی مست و مسرور است کور</p></div>
<div class="m2"><p>پیش رنگی زنده در گور است کور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روح با حق زنده و پاینده ایست</p></div>
<div class="m2"><p>ورنه این را مرده آن را زنده ایست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنکه حی لایموت آمد حق است</p></div>
<div class="m2"><p>زیستن باحق حیات مطلق است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر که بی حق زیست جز مردار نیست</p></div>
<div class="m2"><p>گرچه کس در ماتم او زار نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از نگاهش دیدنی ها در حجاب</p></div>
<div class="m2"><p>قلب او بی ذوق و شوق انقلاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سوز مشتاقی به کردارش کجا</p></div>
<div class="m2"><p>نور آفاقی به گفتارش کجا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مذهب او تنگ چون آفاق او</p></div>
<div class="m2"><p>از عشا تاریک تر اشراق او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زندگی بار گران بر دوش او</p></div>
<div class="m2"><p>مرگ او پروردهٔ آغوش او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عشق را از صحبتش آزار ها</p></div>
<div class="m2"><p>از دمش افسرده گردد نار ها</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نزد آن کرمی که از گل بر نخاست</p></div>
<div class="m2"><p>مهر و ماه و گنبد گردان کجاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از غلامی ذوق دیداری مجوی</p></div>
<div class="m2"><p>از غلامی جان بیداری مجوی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دیدهٔ او محنت دیدن نبرد</p></div>
<div class="m2"><p>در جهان خورد و گران خوابید و مرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حکمران بگشایدش بندی اگر</p></div>
<div class="m2"><p>می نهد بر جان او بندی دگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سازد آئینی گره اندر گره</p></div>
<div class="m2"><p>گویدش می پوش ازین آئین زره</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ریز پیز قهر و کین بنمایدش</p></div>
<div class="m2"><p>بیم مرگ ناگهان افزایدش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا غلام از خویش گردد ناامید</p></div>
<div class="m2"><p>آرزو از سینه گردد ناپدید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گاه او را خلعت زیبا دهد</p></div>
<div class="m2"><p>هم زمام کار در دستش نهد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مهره را شاطر ز کف بیرون جهاند</p></div>
<div class="m2"><p>بیذق خود را به فرزینی رساند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نعمت امروز را شیداش کرد</p></div>
<div class="m2"><p>تا به معنی منکر فرداش کرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تن ستبر از مستی مهر ملوک</p></div>
<div class="m2"><p>جان پاک از لاغری مانند دوک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گردد ار زار و زبون یک جان پاک</p></div>
<div class="m2"><p>به که گردد قریهٔ تن ها هلاک</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بند بر پا نیست بر جان و دل است</p></div>
<div class="m2"><p>مشکل اندر مشکل اندر مشکل است</p></div></div>