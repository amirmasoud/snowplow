---
title: >-
    بخش ۳۵ - مرا براه طلب بار در گل است هنوز
---
# بخش ۳۵ - مرا براه طلب بار در گل است هنوز

<div class="b" id="bn1"><div class="m1"><p>مرا براه طلب بار در گل است هنوز</p></div>
<div class="m2"><p>که دل به قافله و رخت و منزل است هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا ست برق نگاهی که خانمان سوزد</p></div>
<div class="m2"><p>مرا با معامله با کشت و حاصل است هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی سفینهٔ این خام را به طوفان ده</p></div>
<div class="m2"><p>ز ترس موج نگاهم بساحل است هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تپیدن و نرسیدن چه عالمی دارد</p></div>
<div class="m2"><p>خوشا کسی که بدنبال محمل است هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که از دو جهان خویش را برون نشناخت</p></div>
<div class="m2"><p>فریب خوردهٔ این نقش باطل است هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگاه شوق تسلی به جلوه ئی نشود</p></div>
<div class="m2"><p>کجا برم خلشی را که در دل است هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حضور یار حکایت دراز تر گردید</p></div>
<div class="m2"><p>چنانکه این همه نا گفته در دل است هنوز</p></div></div>