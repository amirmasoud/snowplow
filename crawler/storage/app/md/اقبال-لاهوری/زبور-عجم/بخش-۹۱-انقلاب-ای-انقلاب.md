---
title: >-
    بخش ۹۱ - انقلاب! ای انقلاب!
---
# بخش ۹۱ - انقلاب! ای انقلاب!

<div class="b" id="bn1"><div class="m1"><p>خواجه از خون رگ مزدور سازد لعل ناب</p></div>
<div class="m2"><p>از جفای دهخدایان کشت دهقانان خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انقلاب!</p></div>
<div class="m2"><p>انقلاب ای انقلاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیخ شهر از رشته تسبیح صد مؤمن به دام</p></div>
<div class="m2"><p>کافران ساده‌دل را برهمن زنار تاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انقلاب!</p></div>
<div class="m2"><p>انقلاب ای انقلاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میر و سلطان نردباز و کعبتین شان دغل</p></div>
<div class="m2"><p>جان محکومان ز تن بردند محکومان به خواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انقلاب!</p></div>
<div class="m2"><p>انقلاب ای انقلاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واعظ اندر مسجد و فرزند او در مدرسه</p></div>
<div class="m2"><p>آن به پیری کودکی این پیر در عهد شباب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انقلاب!</p></div>
<div class="m2"><p>انقلاب ای انقلاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای مسلمانان فغان از فتنه‌های علم و فن</p></div>
<div class="m2"><p>اهرمن اندر جهان ارزان و یزدان دیریاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>انقلاب!</p></div>
<div class="m2"><p>انقلاب ای انقلاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شوخی باطل نگر اندر کمین حق نشست</p></div>
<div class="m2"><p>شب‌پر از کوری شبیخونی زند بر آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>انقلاب!</p></div>
<div class="m2"><p>انقلاب ای انقلاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در کلیسا ابن‌مریم را به دار آویختند</p></div>
<div class="m2"><p>مصطفی از کعبه هجرت کرده با ام‌الکتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>انقلاب!</p></div>
<div class="m2"><p>انقلاب ای انقلاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من درون شیشه‌های عصر حاضر دیده‌ام</p></div>
<div class="m2"><p>آنچنان زهری که از وی مارها در پیچ و تاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>انقلاب!</p></div>
<div class="m2"><p>انقلاب ای انقلاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با ضعیفان گاه نیروی پلنگان می‌دهند</p></div>
<div class="m2"><p>شعله‌ای شاید برون آید ز فانوس حباب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>انقلاب!</p></div>
<div class="m2"><p>انقلاب ای انقلاب</p></div></div>