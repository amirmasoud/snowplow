---
title: >-
    بخش ۲۱ - بر عقل فلک پیما ترکانه شبیخون به
---
# بخش ۲۱ - بر عقل فلک پیما ترکانه شبیخون به

<div class="b" id="bn1"><div class="m1"><p>بر عقل فلک پیما ترکانه شبیخون به</p></div>
<div class="m2"><p>یک ذره درد دل از علم فلاطون به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی مغبچه ئی با من اسرار محبت گفت</p></div>
<div class="m2"><p>اشکی که فرو خوردی از بادهٔ گلگون به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن فقر که بی تیغی صد کشور دل گیرد</p></div>
<div class="m2"><p>از شوکت دارا به از فر فریدون به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دیر مغان آئی مضمون بلند آور</p></div>
<div class="m2"><p>در خانقه صوفی افسانه و افسون به</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جوی روان ما بی منت طوفانی</p></div>
<div class="m2"><p>یک موج اگر خیزد آن موج ز جیحون به</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیلی که تو آوردی در شهر نمی گنجد</p></div>
<div class="m2"><p>این خانه بر اندازی در خلوت هامون به</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اقبال غزل خوان را کافر نتوان گفتن</p></div>
<div class="m2"><p>سودا بدماغش زد از مدرسه بیرون به</p></div></div>