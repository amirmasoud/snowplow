---
title: >-
    بخش ۱۲۷ - این هم جهانی آن هم جهانی
---
# بخش ۱۲۷ - این هم جهانی آن هم جهانی

<div class="b" id="bn1"><div class="m1"><p>این هم جهانی آن هم جهانی</p></div>
<div class="m2"><p>این بیکرانی آن بیکرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دو خیالی هر دو گمانی</p></div>
<div class="m2"><p>از شعلهٔ من موج دخانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این یک دو آنی آن یک دو آنی</p></div>
<div class="m2"><p>من جاودانی من جاودانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این کم عیاری آن کم عیاری</p></div>
<div class="m2"><p>من پاک جانی نقد روانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اینجا مقامی آنجا مقامی</p></div>
<div class="m2"><p>اینجا زمانی آنجا زمانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینجا چه کارم آنجا چه کارم</p></div>
<div class="m2"><p>آهی فغانی آهی فغانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این رهزن من آن رهزن من</p></div>
<div class="m2"><p>اینجا زیانی آنجا زیانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر دو فروزم هر دو بسوزم</p></div>
<div class="m2"><p>این آشیانی آن آشیانی</p></div></div>