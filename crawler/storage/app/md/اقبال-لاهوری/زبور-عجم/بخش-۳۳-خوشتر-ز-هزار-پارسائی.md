---
title: >-
    بخش ۳۳ - خوشتر ز هزار پارسائی
---
# بخش ۳۳ - خوشتر ز هزار پارسائی

<div class="b" id="bn1"><div class="m1"><p>خوشتر ز هزار پارسائی</p></div>
<div class="m2"><p>گامی به طریق آشنائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سینهٔ من دمی بیاسای</p></div>
<div class="m2"><p>از محنت و کلفت خدائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را ز مقام ما خبر کن</p></div>
<div class="m2"><p>مائیم کجا و تو کجائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن چشمک محرمانه یاد آر</p></div>
<div class="m2"><p>تا کی به تغافل آزمائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دی ماه تمام گفت با من</p></div>
<div class="m2"><p>در ساز به داغ نارسائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش گفت ولی حرام کردند</p></div>
<div class="m2"><p>در مذهب عاشقان جدائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش تو نهاده ام دل خویش</p></div>
<div class="m2"><p>شاید که تو این گره گشائی</p></div></div>