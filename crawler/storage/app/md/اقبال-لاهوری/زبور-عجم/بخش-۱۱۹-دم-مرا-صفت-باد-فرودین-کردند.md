---
title: >-
    بخش ۱۱۹ - دم مرا صفت باد فرودین کردند
---
# بخش ۱۱۹ - دم مرا صفت باد فرودین کردند

<div class="b" id="bn1"><div class="m1"><p>دم مرا صفت باد فرودین کردند</p></div>
<div class="m2"><p>گیاه را ز سرشکم چو یاسمین کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمود لالهٔ صحرا نشین ز خونابم</p></div>
<div class="m2"><p>چنانکه بادهٔ لعلی به ساتگین کردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلند بال چنانم که بر سپهر برین</p></div>
<div class="m2"><p>هزار بار مرا نوریان کمین کردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فروغ آدم خاکی ز تازه کاریهاست</p></div>
<div class="m2"><p>مه و ستاره کنند آنچه پیش ازین کردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چراغ خویش بر افروختم که دست کلیم</p></div>
<div class="m2"><p>درین زمانه نهان زیر آستین کردند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آبسجده و یاری ز خسروان مطلب</p></div>
<div class="m2"><p>که روز فقر نیاکان ما چنین کردند</p></div></div>