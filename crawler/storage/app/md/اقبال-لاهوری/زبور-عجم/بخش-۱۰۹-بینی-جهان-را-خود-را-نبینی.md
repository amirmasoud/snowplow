---
title: >-
    بخش ۱۰۹ - بینی جهان را خود را نبینی
---
# بخش ۱۰۹ - بینی جهان را خود را نبینی

<div class="b" id="bn1"><div class="m1"><p>بینی جهان را خود را نبینی</p></div>
<div class="m2"><p>تا چند نادان غافل نشینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور قدیمی شب را بر افروز</p></div>
<div class="m2"><p>دست کلیمی در آستینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیرون قدم نه از دور آفاق</p></div>
<div class="m2"><p>تو پیش ازینی تو بیش ازینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از مرگ ترسی ای زنده جاوید؟</p></div>
<div class="m2"><p>مرگ است صیدی تو در کمینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانی که بخشد دیگر نگیرند</p></div>
<div class="m2"><p>آدم بمیرد از بی یقینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورت گری را از من بیاموز</p></div>
<div class="m2"><p>شاید که خود را باز آفرینی</p></div></div>