---
title: >-
    بخش ۱۸ - اندرز میر نجات نقشبند المعروف به بابای صحرائی که برای مسلمانان هندوستان رقم فرموده است
---
# بخش ۱۸ - اندرز میر نجات نقشبند المعروف به بابای صحرائی که برای مسلمانان هندوستان رقم فرموده است

<div class="b" id="bn1"><div class="m1"><p>ای که مثل گل ز گل بالیده‌ای</p></div>
<div class="m2"><p>تو هم از بطن خودی زائیده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خودی مگذر بقا انجام باش</p></div>
<div class="m2"><p>قطره‌ای می باش و بحر آشام باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو که از نور خودی تابنده‌ای</p></div>
<div class="m2"><p>گر خودی محکم کنی پاینده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سود در جیب همین سوداستی</p></div>
<div class="m2"><p>خواجگی از حفظ این کالاستی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هستی و از نیستی ترسیده‌ای</p></div>
<div class="m2"><p>ای سرت گردم غلط فهمیده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون خبر دارم ز ساز زندگی </p></div>
<div class="m2"><p>با تو گویم چیست راز زندگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غوطه در خود صورت گوهر زدن</p></div>
<div class="m2"><p>پس ز خلوت گاه خود سر بر زدن </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر خاکستر شرار اندوختن</p></div>
<div class="m2"><p>شعله گردیدن نظرها سوختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خانه سوز محنت چل ساله شو</p></div>
<div class="m2"><p>طوف خود کن شعله ی جواله شو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زندگی از طوف دیگر رستن است</p></div>
<div class="m2"><p>خویش را بیت الحرم دانستن است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پر زن و از جذب خاک آزاد باش</p></div>
<div class="m2"><p>همچو طایر ایمن از افتاد باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو اگر طایر نه‌ای ای هوشمند</p></div>
<div class="m2"><p>بر سر غار آشیان خود مبند </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای که باشی در پی کسب علوم</p></div>
<div class="m2"><p>با تو می گویم پیام پیر روم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>«علم را بر تن زنی ماری بود</p></div>
<div class="m2"><p>علم را بر دل زنی یاری بود»</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آگهی از قصه ی آخوند روم</p></div>
<div class="m2"><p>آنکه داد اندر حلب درس علوم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پای در زنجیر توجیهات عقل</p></div>
<div class="m2"><p>کشتیش طوفانی «ظلمات» عقل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>موسی بیگانه ی سینای عشق</p></div>
<div class="m2"><p>بیخبر از عشق و از سودای عشق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از تشکک گفت و از اشراق گفت</p></div>
<div class="m2"><p>وز حکم صد گوهر تابنده سفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عقده های قول مشائین گشود</p></div>
<div class="m2"><p>نور فکرش هر خفی را وانمود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرد و پیشش بود انبار کتب</p></div>
<div class="m2"><p>بر لب او شرح اسرار کتب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیر تبریزی ز ارشاد کمال</p></div>
<div class="m2"><p>جست راه مکتب ملا جلال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت این غوغا و قیل و قال چیست</p></div>
<div class="m2"><p>این قیاس و وهم و استدلال چیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مولوی فرمود نادان لب ببند</p></div>
<div class="m2"><p>بر مقالات خردمندان مخند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پای خویش از مکتبم بیرون گذار</p></div>
<div class="m2"><p>قیل و قال است این ترا با وی چه کار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قال ما از فهم تو بالاتر است</p></div>
<div class="m2"><p>شیشه ی ادراک را روشنگر است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سوز شمس از گفته ی ملا فزود</p></div>
<div class="m2"><p>آتشی از جان تبریزی گشود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر زمین برق نگاه او فتاد</p></div>
<div class="m2"><p>خاک از سوز دم او شعله زاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آتش دل خرمن ادراک سوخت</p></div>
<div class="m2"><p>دفتر آن فلسفی را پاک سوخت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مولوی بیگانه از اعجاز عشق</p></div>
<div class="m2"><p>ناشناس نغمه های ساز عشق</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفت این آتش چسان افروختی</p></div>
<div class="m2"><p>دفتر ارباب حکمت سوختی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفت شیخ ای مسلم زنار دار</p></div>
<div class="m2"><p>ذوق و حال است این ترا با وی چه کار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حال ما از فکر تو بالاتر است</p></div>
<div class="m2"><p>شعله ی ما کیمیای احمر است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ساختی از برف حکمت ساز و برگ</p></div>
<div class="m2"><p>از سحاب فکر تو بارد تگرگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آتشی افروز از خاشاک خویش</p></div>
<div class="m2"><p>شعله‌ای تعمیر کن از خاک خویش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>علم مسلم کامل از سوز دل است</p></div>
<div class="m2"><p>معنی اسلام ترک آفل است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون ز بند آفل ابراهیم رست</p></div>
<div class="m2"><p>در میان شعله ها نیکو نشست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>علم حق را در قفا انداختی</p></div>
<div class="m2"><p>بهر نانی نقد دین در باختی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گرم رو در جستجوی سرمه‌ای</p></div>
<div class="m2"><p>واقف از چشم سیاه خود نه‌ای</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آب حیوان از دم خنجر طلب</p></div>
<div class="m2"><p>از دهان اژدها کوثر طلب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سنگ اسود از در بتخانه خواه</p></div>
<div class="m2"><p>نافه ی مشک از سگ دیوانه خواه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سوز عشق از دانش حاضر مجوی</p></div>
<div class="m2"><p>کیف حق از جام این کافر مجوی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مدتی محو تک و دو بوده ام</p></div>
<div class="m2"><p>رازدان دانش نو بوده ام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>باغبانان امتحانم کرده اند</p></div>
<div class="m2"><p>محرم این گلستانم کرده اند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گلستانی لاله زار عبرتی</p></div>
<div class="m2"><p>چون گل کاغذ سراب نکهتی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا ز بند این گلستان رسته ام</p></div>
<div class="m2"><p>آشیان بر شاخ طوبی بسته ام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دانش حاضر حجاب اکبر است</p></div>
<div class="m2"><p>بت پرست و بت فروش و بتگر است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>پا بزندان مظاهر بسته‌ای</p></div>
<div class="m2"><p>از حدود حس برون نا جسته‌ای</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در صراط زندگی از پا فتاد</p></div>
<div class="m2"><p>بر گلوی خویشتن خنجر نهاد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آتشی دارد مثال لاله سرد</p></div>
<div class="m2"><p>شعله‌ای دارد مثال ژاله سرد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فطرتش از سوز عشق آزاد ماند</p></div>
<div class="m2"><p>در جهان جستجو ناشاد ماند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>عشق افلاطون علت های عقل</p></div>
<div class="m2"><p>به شود از نشترش سودای عقل</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>جمله عالم ساجد و مسجود عشق</p></div>
<div class="m2"><p>سومنات عقل را محمود عشق</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>این می دیرینه در میناش نیست</p></div>
<div class="m2"><p>شور «یارب» ، قسمت شبهاش نیست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>قیمت شمشاد خود نشناختی</p></div>
<div class="m2"><p>سرو دیگر را بلند انداختی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مثل نی خود را ز خود کردی تهی</p></div>
<div class="m2"><p>بر نوای دیگران دل می نهی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ای گدای ریزه‌ای از خوان غیر</p></div>
<div class="m2"><p>جنس خود می جوئی از دکان غیر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بزم مسلم از چراغ غیر سوخت</p></div>
<div class="m2"><p>مسجد او از شرار دیر سوخت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>از سواد کعبه چون آهو رمید</p></div>
<div class="m2"><p>ناوک صیاد پهلویش درید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شد پریشان برگ گل چون بوی خویش</p></div>
<div class="m2"><p>ای ز خود رم کرده باز آ سوی خویش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ای امین حکمت ام الکتاب</p></div>
<div class="m2"><p>وحدت گمگشته ی خود بازیاب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ما که دربان حصار ملتیم</p></div>
<div class="m2"><p>کافر از ترک شعار ملتیم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ساقی دیرینه را ساغر شکست</p></div>
<div class="m2"><p>بزم رندان حجازی بر شکست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کعبه آباد است از اصنام ما</p></div>
<div class="m2"><p>خنده زن کفر است بر اسلام ما</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شیخ در عشق بتان اسلام باخت</p></div>
<div class="m2"><p>رشته ی تسبیح از زنار ساخت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پیر ها پیر از بیاض مو شدند</p></div>
<div class="m2"><p>سخره بهر کودکان کو شدند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دل ز نقش لااله بیگانه‌ای</p></div>
<div class="m2"><p>از صنم های هوس بتخانه‌ای</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>می شود هر مو درازی خرقه پوش</p></div>
<div class="m2"><p>آه ازین سوداگران دین فروش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>با مریدان روز و شب اندر سفر</p></div>
<div class="m2"><p>از ضرورت های ملت بی خبر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دیده ها بی نور مثل نرگس اند</p></div>
<div class="m2"><p>سینه ها از دولت دل مفلس اند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>واعظان هم صوفیان منصب پرست</p></div>
<div class="m2"><p>اعتبار ملت بیضا شکست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>واعظ ما چشم بر بتخانه دوخت</p></div>
<div class="m2"><p>مفتی دین مبین فتوی فروخت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چیست یاران بعد ازین تدبیر ما</p></div>
<div class="m2"><p>رخ سوی میخانه دارد پیر ما</p></div></div>