---
title: >-
    بخش ۲۰ - دعا
---
# بخش ۲۰ - دعا

<div class="b" id="bn1"><div class="m1"><p>ای چو جان اندر وجود عالمی</p></div>
<div class="m2"><p>جان ما باشی و از ما می رمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نغمه از فیض تو در عود حیات</p></div>
<div class="m2"><p>موت در راه تو محسود حیات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز تسکین دل ناشاد شو</p></div>
<div class="m2"><p>باز اندر سینه ها آباد شو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز از ما خواه ننگ و نام را</p></div>
<div class="m2"><p>پخته تر کن عاشقان خام را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از مقدر شکوه ها داریم ما</p></div>
<div class="m2"><p>نرخ تو بالا و ناداریم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تهیدستان رخ زیبا مپوش</p></div>
<div class="m2"><p>عشق سلمان و بلال ارزان فروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم بیخواب و دل بیتاب ده</p></div>
<div class="m2"><p>باز ما را فطرت سیماب ده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آیتی بمنا ز آیات مبین</p></div>
<div class="m2"><p>تا شود اعناق اعدا خاضعین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوه آتش خیز کن این کاه را</p></div>
<div class="m2"><p>ز آتش ما سوز غیر الله را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رشته ی وحدت چو قوم از دست داد</p></div>
<div class="m2"><p>صد گره بر روی کار ما فتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما پریشان در جهان چون اختریم</p></div>
<div class="m2"><p>همدم و بیگانه از یکدیگریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باز این اوراق را شیرازه کن</p></div>
<div class="m2"><p>باز آئین محبت تازه کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باز ما را بر همان خدمت گمار</p></div>
<div class="m2"><p>کار خود با عاشقان خود سپار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رهروان را منزل تسلیم بخش</p></div>
<div class="m2"><p>قوت ایمان ابراهیم بخش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عشق را از شغل لا آگاه کن</p></div>
<div class="m2"><p>آشنای رمز الاالله کن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منکه بهر دیگران سوزم چو شمع</p></div>
<div class="m2"><p>بزم خود را گریه آموزم چو شمع</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یارب آن اشکی که باشد دلفروز</p></div>
<div class="m2"><p>بیقرار و مضطر و آرام سوز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کارمش در باغ و روید آتشی</p></div>
<div class="m2"><p>از قبای لاله شوید آتشی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دل بدوش و دیده بر فرداستم</p></div>
<div class="m2"><p>در میان انجمن تنها ستم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>«هر کسی از ظن خود شد یار من</p></div>
<div class="m2"><p>از درون من نجست اسرار من»</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در جهان یارب ندیم من کجاست</p></div>
<div class="m2"><p>نخل سینایم کلیم من کجاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ظالمم بر خود ستم ها کرده ام</p></div>
<div class="m2"><p>شعله ئی را در بغل پرورده ام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شعله ئی غارت گر سامان هوش</p></div>
<div class="m2"><p>آتشی افکنده در دامان هوش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عقل را دیوانگی آموخته</p></div>
<div class="m2"><p>علم را سامان هستی سوخته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آفتاب از سوز او گردون مقام</p></div>
<div class="m2"><p>برقها اندر طواف او مدام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همچو شبنم دیده ی گریان شدم</p></div>
<div class="m2"><p>تا امین آتش پنهان شدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شمع را سوز عیان آموختم</p></div>
<div class="m2"><p>خود نهان از چشم عالم سوختم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شعله ها آخر ز هر مویم دمید</p></div>
<div class="m2"><p>از رگ اندیشه ام آتش چکید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عندلیبم از شرر ها دانه چید</p></div>
<div class="m2"><p>نغمه ی آتش مزاجی آفرید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سینه ی عصر من از دل خالی است</p></div>
<div class="m2"><p>می تپد مجنون که محمل خالی است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شمع را تنها تپیدن سهل نیست</p></div>
<div class="m2"><p>آه یک پروانه ی من اهل نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>انتظار غمگساری تا کجا</p></div>
<div class="m2"><p>جستجوی راز داری تا کجا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای ز رویت ماه و انجم مستنیر</p></div>
<div class="m2"><p>آتش خود را ز جانم باز گیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>این امانت بازگیر از سینه ام</p></div>
<div class="m2"><p>خار جوهر برکش از آئینه ام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یا مرا یک همدم دیرینه ده</p></div>
<div class="m2"><p>عشق عالم سوز را آئینه ده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>موج در بحر است هم پهلوی موج</p></div>
<div class="m2"><p>هست با همدم تپیدن خوی موج</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر فلک کوکب ندیم کوکبست</p></div>
<div class="m2"><p>ماه تابان سر بزانوی شب است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>روز پهلوی شب یلدا زند</p></div>
<div class="m2"><p>خویش را امروز بر فردا زند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هستی جوئی بجوئی گم شود</p></div>
<div class="m2"><p>موجه ی بادی ببوئی گم شود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هست در هر گوشه ی ویرانه رقص</p></div>
<div class="m2"><p>می کند دیوانه با دیوانه رقص</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گرچه تو در ذات خود یکتاستی</p></div>
<div class="m2"><p>عالمی از بهر خویش آراستی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من مثال لاله ی صحراستم</p></div>
<div class="m2"><p>درمیان محفلی تنهاستم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خواهم از لطف تو یاری همدمی</p></div>
<div class="m2"><p>از رموز فطرت من محرمی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همدمی دیوانه ئی فرزانه ئی</p></div>
<div class="m2"><p>از خیال این و آن بیگانه ئی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا بجان او سپارم هوی خویش</p></div>
<div class="m2"><p>باز بینم در دل او روی خویش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سازم از مشت گل خود پیکرش</p></div>
<div class="m2"><p>هم صنم او را شوم هم آزرش</p></div></div>