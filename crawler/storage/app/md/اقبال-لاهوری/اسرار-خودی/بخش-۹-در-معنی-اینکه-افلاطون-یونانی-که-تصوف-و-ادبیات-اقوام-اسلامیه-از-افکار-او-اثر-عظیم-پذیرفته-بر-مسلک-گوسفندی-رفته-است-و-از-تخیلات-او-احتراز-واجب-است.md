---
title: >-
    بخش ۹ - در معنی اینکه افلاطون یونانی که تصوف و ادبیات اقوام اسلامیه از افکار او اثر عظیم پذیرفته بر مسلک گوسفندی رفته است و از تخیلات او احتراز واجب است
---
# بخش ۹ - در معنی اینکه افلاطون یونانی که تصوف و ادبیات اقوام اسلامیه از افکار او اثر عظیم پذیرفته بر مسلک گوسفندی رفته است و از تخیلات او احتراز واجب است

<div class="b" id="bn1"><div class="m1"><p>راهب دیرینه افلاطون حکیم</p></div>
<div class="m2"><p>از گروه گوسفندان قدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخش او در ظلمت معقول گم</p></div>
<div class="m2"><p>در کهستان وجود افکنده سم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچنان افسون نامحسوس خورد</p></div>
<div class="m2"><p>اعتبار از دست و چشم و گوش برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت سر زندگی در مردن است</p></div>
<div class="m2"><p>شمع را صد جلوه از افسردن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر تخیلهای ما فرمان رواست</p></div>
<div class="m2"><p>جام او خواب آور و گیتی رباست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوسفندی در لباس آدم است</p></div>
<div class="m2"><p>حکم او بر جان صوفی محکم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل خود را بر سر گردون رساند</p></div>
<div class="m2"><p>عالم اسباب را افسانه خواند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار او تحلیل اجزای حیات</p></div>
<div class="m2"><p>قطع شاخ سرو رعنای حیات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فکر افلاطون زیان را سود گفت</p></div>
<div class="m2"><p>حکمت او بود را نابود گفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فطرتش خوابید و خوابی آفرید</p></div>
<div class="m2"><p>چشم هوش او سرابی آفرید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسکه از ذوق عمل محروم بود</p></div>
<div class="m2"><p>جان او وارفته ی معدوم بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منکر هنگامه ی موجود گشت</p></div>
<div class="m2"><p>خالق اعیان نامشهود گشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زنده جان را عالم امکان خوش است</p></div>
<div class="m2"><p>مرده دل را عالم اعیان خوش است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آهوش بی بهره از لطف خرام</p></div>
<div class="m2"><p>لذت رفتار بر کبکش حرام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شبنمش از طاقت رم بی نصیب</p></div>
<div class="m2"><p>طایرش را سینه از دم بی نصیب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ذوق روئیدن ندارد دانه اش</p></div>
<div class="m2"><p>از طپیدن بی خبر پروانه اش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>راهب ما چاره غیر از رم نداشت</p></div>
<div class="m2"><p>طاقت غوغای این عالم نداشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دل بسوز شعله ی افسرده بست</p></div>
<div class="m2"><p>نقش آن دنیای افیون خورده بست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از نشیمن سوی گردون پر گشود</p></div>
<div class="m2"><p>باز سوی آشیان نامد فرود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در خم گردون خیال او گم است</p></div>
<div class="m2"><p>من ندانم درد یا خشت خم است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قومها از سکر او مسموم گشت</p></div>
<div class="m2"><p>خفت و از ذوق عمل محروم گشت</p></div></div>