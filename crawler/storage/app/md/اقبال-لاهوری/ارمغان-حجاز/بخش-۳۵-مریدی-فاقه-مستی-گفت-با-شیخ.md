---
title: >-
    بخش ۳۵ - مریدی فاقه مستی گفت با شیخ
---
# بخش ۳۵ - مریدی فاقه مستی گفت با شیخ

<div class="b" id="bn1"><div class="m1"><p>مریدی فاقه مستی گفت با شیخ</p></div>
<div class="m2"><p>که یزدان را ز حال ما خبر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بما نزدیک تر از شهرگ ماست</p></div>
<div class="m2"><p>ولیکن از شکم نزدیکتر نیست</p></div></div>