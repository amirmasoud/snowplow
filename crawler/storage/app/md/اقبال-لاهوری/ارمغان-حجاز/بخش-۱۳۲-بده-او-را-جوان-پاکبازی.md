---
title: >-
    بخش ۱۳۲ - بده او را جوان پاکبازی
---
# بخش ۱۳۲ - بده او را جوان پاکبازی

<div class="b" id="bn1"><div class="m1"><p>بده او را جوان پاکبازی</p></div>
<div class="m2"><p>سرورش از شراب خانه سازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قوی بازوی او مانند حیدر </p></div>
<div class="m2"><p>دل او از دو گیتی بی نیازی</p></div></div>