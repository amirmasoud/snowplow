---
title: >-
    بخش ۶۲ - جهان چار سو اندر بر من
---
# بخش ۶۲ - جهان چار سو اندر بر من

<div class="b" id="bn1"><div class="m1"><p>جهان چار سو اندر بر من</p></div>
<div class="m2"><p>هوای لامکان اندر سر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بگذشتم ازین بام بلندی</p></div>
<div class="m2"><p>چو گرد افتاد پرواز از پر من</p></div></div>