---
title: >-
    بخش ۹۱ - بدست می کشان خالی ایاغ است
---
# بخش ۹۱ - بدست می کشان خالی ایاغ است

<div class="b" id="bn1"><div class="m1"><p>بدست می کشان خالی ایاغ است</p></div>
<div class="m2"><p>که ساقی را به بزم من فراغ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگه دارم درون سینه آهی </p></div>
<div class="m2"><p>که اصل او ز دود آن چراغ است</p></div></div>