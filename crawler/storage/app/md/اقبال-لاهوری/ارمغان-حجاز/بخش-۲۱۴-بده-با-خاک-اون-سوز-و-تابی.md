---
title: >-
    بخش ۲۱۴ - بده با خاک اون سوز و تابی
---
# بخش ۲۱۴ - بده با خاک اون سوز و تابی

<div class="b" id="bn1"><div class="m1"><p>بده با خاک اون سوز و تابی</p></div>
<div class="m2"><p>که زاید از شب اوفتابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوان زن که از فیض تو او را</p></div>
<div class="m2"><p>دگر بخشند ذوق انقلابی</p></div></div>