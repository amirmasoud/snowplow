---
title: >-
    بخش ۲۷۷ - بخود باز او دامان دلی گیر
---
# بخش ۲۷۷ - بخود باز او دامان دلی گیر

<div class="b" id="bn1"><div class="m1"><p>بخود باز او دامان دلی گیر</p></div>
<div class="m2"><p>درون سینه خود منزلی گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بده این کشت را خونابهٔ خویش</p></div>
<div class="m2"><p>فشاندم دانه من تو حاصلی گیر</p></div></div>