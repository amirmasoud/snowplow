---
title: >-
    بخش ۱۹۹ - خودی تا گشت مهجور خدائی
---
# بخش ۱۹۹ - خودی تا گشت مهجور خدائی

<div class="b" id="bn1"><div class="m1"><p>خودی تا گشت مهجور خدائی</p></div>
<div class="m2"><p>به فقرموختداب گدائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چشم مست رومی وام کردم</p></div>
<div class="m2"><p>سروری از مقام کبریائی</p></div></div>