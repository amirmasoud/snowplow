---
title: >-
    بخش ۲۱۳ - مسلمان بنده مولا صفات است
---
# بخش ۲۱۳ - مسلمان بنده مولا صفات است

<div class="b" id="bn1"><div class="m1"><p>مسلمان بنده مولا صفات است</p></div>
<div class="m2"><p>دل او سری از اسرار ذات است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمالش جز به نور حق نه بینی</p></div>
<div class="m2"><p>که اصلش در ضمیر کائنات است</p></div></div>