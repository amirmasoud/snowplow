---
title: >-
    بخش ۱۰۶ - زبان ما غریبان از نگاهیست
---
# بخش ۱۰۶ - زبان ما غریبان از نگاهیست

<div class="b" id="bn1"><div class="m1"><p>زبان ما غریبان از نگاهیست</p></div>
<div class="m2"><p>حدیث دردمندان اشک و آهیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشادم چشم و بر بستم لب خویش</p></div>
<div class="m2"><p>سخن اندر طریق ما گناهیست</p></div></div>