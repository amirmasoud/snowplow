---
title: >-
    بخش ۳۵۳ - فقیرم ساز و سامانم نگاهی است
---
# بخش ۳۵۳ - فقیرم ساز و سامانم نگاهی است

<div class="b" id="bn1"><div class="m1"><p>فقیرم ساز و سامانم نگاهی است</p></div>
<div class="m2"><p>به چشمم کوه یاران برگ کاهی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز من گیر این که زاغ دخمه بهتر</p></div>
<div class="m2"><p>ازن بازی که دستموز شاهیست</p></div></div>