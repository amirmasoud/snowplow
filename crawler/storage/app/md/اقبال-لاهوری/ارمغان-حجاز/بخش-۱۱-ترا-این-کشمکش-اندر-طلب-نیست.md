---
title: >-
    بخش ۱۱ - ترا این کشمکش اندر طلب نیست
---
# بخش ۱۱ - ترا این کشمکش اندر طلب نیست

<div class="b" id="bn1"><div class="m1"><p>ترا این کشمکش اندر طلب نیست</p></div>
<div class="m2"><p>ترا این درد و داغ و تاب وتب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن از لامکان بگریختم من</p></div>
<div class="m2"><p>که آنجا ناله های نیم شب نیست</p></div></div>