---
title: >-
    بخش ۱۸۴ - گرفتم حضرت ملا ترش روست
---
# بخش ۱۸۴ - گرفتم حضرت ملا ترش روست

<div class="b" id="bn1"><div class="m1"><p>گرفتم حضرت ملا ترش روست</p></div>
<div class="m2"><p>نگاهش مغز را نشناسد از پوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر با این مسلمانی که دارم</p></div>
<div class="m2"><p>مرا از کعبه میراند حق او ست</p></div></div>