---
title: >-
    بخش ۸۰ - تن مرد مسلمان پایدار است
---
# بخش ۸۰ - تن مرد مسلمان پایدار است

<div class="b" id="bn1"><div class="m1"><p>تن مرد مسلمان پایدار است</p></div>
<div class="m2"><p>بنای پیکر او استوار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبیب نکته رس دید از نگاهش</p></div>
<div class="m2"><p>خودی اندر وجودش رعشه دار است</p></div></div>