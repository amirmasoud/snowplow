---
title: >-
    بخش ۳۱۷ - گهی جویندهٔ حسن غریبی
---
# بخش ۳۱۷ - گهی جویندهٔ حسن غریبی

<div class="b" id="bn1"><div class="m1"><p>گهی جویندهٔ حسن غریبی </p></div>
<div class="m2"><p>خطیبی منبر او از صلیبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی سلطان با خیل و سپاهی</p></div>
<div class="m2"><p>ولی از دولت خود بی نصیبی</p></div></div>