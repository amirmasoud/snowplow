---
title: >-
    بخش ۲۷۰ - جوانی خوش گلی رنگین کلاهی
---
# بخش ۲۷۰ - جوانی خوش گلی رنگین کلاهی

<div class="b" id="bn1"><div class="m1"><p>جوانی خوش گلی رنگین کلاهی</p></div>
<div class="m2"><p>نگاه او چو شیران بی پناهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مکتب علم میشی را بیاموخت</p></div>
<div class="m2"><p>میسر نایدش برگ گیاهی</p></div></div>