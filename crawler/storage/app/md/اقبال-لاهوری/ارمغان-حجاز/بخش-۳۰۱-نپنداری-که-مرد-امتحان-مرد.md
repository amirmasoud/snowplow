---
title: >-
    بخش ۳۰۱ - نپنداری که مرد امتحان مرد
---
# بخش ۳۰۱ - نپنداری که مرد امتحان مرد

<div class="b" id="bn1"><div class="m1"><p>نپنداری که مرد امتحان مرد</p></div>
<div class="m2"><p>نمیرد گرچه زیرسمان مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا شایان چنین مرگ است ورنه</p></div>
<div class="m2"><p>زهر مرگی که خواهی میتوان مرد</p></div></div>