---
title: >-
    بخش ۷۵ - دگر پاکیزه کن آب و گل او
---
# بخش ۷۵ - دگر پاکیزه کن آب و گل او

<div class="b" id="bn1"><div class="m1"><p>دگر پاکیزه کن آب و گل او</p></div>
<div class="m2"><p>جهانی آفرین اندر دل او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوا تیز و بدامانش دو صد چاک</p></div>
<div class="m2"><p>بیندیش از چراغ بسمل او</p></div></div>