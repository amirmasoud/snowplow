---
title: >-
    بخش ۴۳ - نگاهی داشتم بر جوهر دل
---
# بخش ۴۳ - نگاهی داشتم بر جوهر دل

<div class="b" id="bn1"><div class="m1"><p>نگاهی داشتم بر جوهر دل</p></div>
<div class="m2"><p>تپیدم ، آرمیدم در بر دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رمیدم از هوای قریه و شهر</p></div>
<div class="m2"><p>به باد دشت وا کردم در دل</p></div></div>