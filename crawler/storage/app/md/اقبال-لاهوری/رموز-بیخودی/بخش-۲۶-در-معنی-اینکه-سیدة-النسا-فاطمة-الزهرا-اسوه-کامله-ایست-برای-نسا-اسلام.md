---
title: >-
    بخش ۲۶ - در معنی اینکه سیدة النساء فاطمة الزهراء اسوه کامله ایست برای نساء اسلام
---
# بخش ۲۶ - در معنی اینکه سیدة النساء فاطمة الزهراء اسوه کامله ایست برای نساء اسلام

<div class="b" id="bn1"><div class="m1"><p>مریم از یک نسبت عیسی عزیز</p></div>
<div class="m2"><p>از سه نسبت حضرت زهرا عزیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور چشم رحمة للعالمین</p></div>
<div class="m2"><p>آن امام اولین و آخرین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه جان در پیکر گیتی دمید</p></div>
<div class="m2"><p>روزگار تازه آئین آفرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بانوی آن تاجدار «هل اتی»</p></div>
<div class="m2"><p>مرتضی مشکل گشا شیر خدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پادشاه و کلبه ئی ایوان او</p></div>
<div class="m2"><p>یک حسام و یک زره سامان او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مادر آن مرکز پرگار عشق</p></div>
<div class="m2"><p>مادر آن کاروان سالار عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن یکی شمع شبستان حرم</p></div>
<div class="m2"><p>حافظ جمعیت خیرالامم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نشیند آتش پیکار و کین</p></div>
<div class="m2"><p>پشت پا زد بر سر تاج و نگین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وان دگر مولای ابرار جهان</p></div>
<div class="m2"><p>قوت بازوی احرار جهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در نوای زندگی سوز از حسین</p></div>
<div class="m2"><p>اهل حق حریت آموز از حسین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیرت فرزند ها از امهات</p></div>
<div class="m2"><p>جوهر صدق و صفا از امهات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مزرع تسلیم را حاصل بتول</p></div>
<div class="m2"><p>مادران را اسوهٔ کامل بتول</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهر محتاجی دلش آنگونه سوخت</p></div>
<div class="m2"><p>با یهودی چادر خود را فروخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نوری و هم آتشی فرمانبرش</p></div>
<div class="m2"><p>گم رضایش در رضای شوهرش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن ادب پروردهٔ صبر و رضا</p></div>
<div class="m2"><p>آسیا گردان و لب قرآن سرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گریه های او ز بالین بی نیاز</p></div>
<div class="m2"><p>گوهر افشاندی بدامان نماز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اشک او بر چید جبریل از زمین</p></div>
<div class="m2"><p>همچو شبنم ریخت بر عرش برین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رشتهٔ آئین حق زنجیر پاست</p></div>
<div class="m2"><p>پاس فرمان جناب مصطفی است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ورنه گرد تربتش گردیدمی</p></div>
<div class="m2"><p>سجده ها بر خاک او پاشیدمی</p></div></div>