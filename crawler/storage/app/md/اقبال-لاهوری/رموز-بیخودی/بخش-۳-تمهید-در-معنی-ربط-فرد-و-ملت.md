---
title: >-
    بخش ۳ - تمهید : در معنی ربط فرد و ملت
---
# بخش ۳ - تمهید : در معنی ربط فرد و ملت

<div class="b" id="bn1"><div class="m1"><p>فرد را ربط جماعت رحمت است</p></div>
<div class="m2"><p>جوهر او را کمال از ملت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاتوانی با جماعت یار باش</p></div>
<div class="m2"><p>رونق هنگامه ی احرار باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حرز جان کن گفته ی خیرالبشر</p></div>
<div class="m2"><p>هست شیطان از جماعت دور تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرد و قوم آئینه ی یک دیگرند</p></div>
<div class="m2"><p>سلک و گوهر کهکشان و اخترند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرد می گیرد ز ملت احترام</p></div>
<div class="m2"><p>ملت از افراد می یابد نظام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرد تا اندر جماعت گم شود</p></div>
<div class="m2"><p>قطره ی وسعت طلب قلزم شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مایه دار سیرت دیرینه او</p></div>
<div class="m2"><p>رفته و آینده را آئینه او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصل استقبال و ماضی ذات او</p></div>
<div class="m2"><p>چون ابد لا انتها اوقات او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دلش ذوق نمو از ملت است</p></div>
<div class="m2"><p>احتساب کار او از ملت است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیکرش از قوم و هم جانش ز قوم</p></div>
<div class="m2"><p>ظاهرش از قوم و پنهانش ز قوم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در زبان قوم گویا می شود</p></div>
<div class="m2"><p>بر ره اسلاف پویا می شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پخته تر از گرمی صحبت شود</p></div>
<div class="m2"><p>تا بمعنی فرد هم ملت شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وحدت او مستقیم از کثرت است</p></div>
<div class="m2"><p>کثرت اندر وحدت او وحدت است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لفظ چون از بیت خود بیرون نشست</p></div>
<div class="m2"><p>گوهر مضمون بجیب خود شکست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برگ سبزی کز نهال خویش ریخت</p></div>
<div class="m2"><p>از بهاران تار امیدش گسیخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که آب از زمزم ملت نخورد</p></div>
<div class="m2"><p>شعله های نغمه در عودش فسرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرد تنها از مقاصد غافل است</p></div>
<div class="m2"><p>قوتش آشفتگی را مایل است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قوم با ضبط آشنا گرداندش</p></div>
<div class="m2"><p>نرم رو مثل صبا گرداندش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پا به گل مانند شمشادش کند</p></div>
<div class="m2"><p>دست و پا بندد که آزادش کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون اسیر حلقه ی آئین شود</p></div>
<div class="m2"><p>آهوی رم خوی او مشکین شود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو خودی از بیخودی نشناختی</p></div>
<div class="m2"><p>خویش را اندر گمان انداختی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جوهر نوریست اندر خاک تو</p></div>
<div class="m2"><p>یک شعاعش جلوه ی ادراک تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عیشت از عیشش غم تو از غمش</p></div>
<div class="m2"><p>زنده ئی از انقلاب هر دمش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>واحدستو بر نمی تابد دوئی</p></div>
<div class="m2"><p>من ز تاب او من استم تو توئی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خویش دار و خویش باز و خویش ساز</p></div>
<div class="m2"><p>نازها می پرورد اندر نیاز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آتشی از سوز او گردد بلند</p></div>
<div class="m2"><p>این شرر بر شعله اندازد کمند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فطرتش آزاد و هم زنجیری است</p></div>
<div class="m2"><p>جزو او را قوت کل گیری است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خوگر پیکار پیهم دیدمش</p></div>
<div class="m2"><p>هم خودی هم زندگی نامیدش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون ز خلوت خویش را بیرون دهد</p></div>
<div class="m2"><p>پای در هنگامه ی جلوت نهد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نقش گیر اندر دلش «او» می شود</p></div>
<div class="m2"><p>«من» ز هم می ریزد و «تو» می شود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جبر ، قطع اختیارش می کند</p></div>
<div class="m2"><p>از محبت مایه دارش می کند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ناز تا ناز است کم خیزد نیاز</p></div>
<div class="m2"><p>ناز ها سازد بهم خیزد نیاز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در جماعت خود شکن گردد خودی</p></div>
<div class="m2"><p>تا ز گلبرگی چمن گردد خودی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>«نکته ها چون تیغ پولاد است تیز</p></div>
<div class="m2"><p>گر نمی فهمی ز پیش ما گریز»</p></div></div>