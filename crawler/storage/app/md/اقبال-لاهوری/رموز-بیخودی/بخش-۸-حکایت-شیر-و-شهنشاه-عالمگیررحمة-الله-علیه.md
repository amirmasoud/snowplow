---
title: >-
    بخش ۸ - حکایت شیر و شهنشاه عالمگیررحمة الله علیه
---
# بخش ۸ - حکایت شیر و شهنشاه عالمگیررحمة الله علیه

<div class="b" id="bn1"><div class="m1"><p>شاه عالمگیر گردون آستان</p></div>
<div class="m2"><p>اعتبار دودمان گورگان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پایه ی اسلامیان برتر ازو</p></div>
<div class="m2"><p>احترام شرع پیغمبر ازو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان کارزار کفر و دین </p></div>
<div class="m2"><p>ترکش ما را خدنگ آخرین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تخم الحادی که اکبر پرورید</p></div>
<div class="m2"><p>باز اندر فطرت دارا دمید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع دل در سینه ها روشن نبود</p></div>
<div class="m2"><p>ملت ما از فساد ایمن نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حق گزید از هند عالمگیر را</p></div>
<div class="m2"><p>آن فقیر صاحب شمشیر را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پی احیای دین مأمور کرد</p></div>
<div class="m2"><p>بهر تجدید یقین مأمور کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برق تیغش خرمن الحاد سوخت</p></div>
<div class="m2"><p>شمع دین در محفل ما بر فروخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کور ذوقان داستانها ساختند</p></div>
<div class="m2"><p>وسعت ادراک او نشناختند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعله ی توحید را پروانه بود</p></div>
<div class="m2"><p>چون براهیم اندرین بتخانه بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در صف شاهنشان یکتاستی</p></div>
<div class="m2"><p>فقر او از تربتش پیداستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روزی آن زیبنده ی تاج و سریر</p></div>
<div class="m2"><p>آن سپهدار و شهنشاه و فقیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صبحگاهان شد به سیر بیشه ئی</p></div>
<div class="m2"><p>با پرستاری وفا اندیشه ئی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر خوش از کیفیت باد سحر</p></div>
<div class="m2"><p>طایران تسبیح خوان بر هر شجر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاه رمز آگاه شد محو نماز</p></div>
<div class="m2"><p>خیمه بر زد در حقیقت از مجاز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شیر ببر آمد پدید از طرف دشت</p></div>
<div class="m2"><p>از خروش او فلک لرزنده گشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بوی انسان دادش از انسان خبر</p></div>
<div class="m2"><p>پنجه عالمگیر را زد بر کمر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دست شه نادیده خنجر بر کشید </p></div>
<div class="m2"><p>شرزه شیری را شکم از هم درید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دل بخود راهی نداد اندیشه را</p></div>
<div class="m2"><p>شیر قالین کرد شیر بیشه را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باز سوی حق رمید آن ناصبور</p></div>
<div class="m2"><p>بود معراجش نماز با حضور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این چنین دل خود نما و خود شکن</p></div>
<div class="m2"><p>دارد اندر سینه ی مؤمن وطن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بنده ی حق پیش مولا لاستی</p></div>
<div class="m2"><p>پیش باطل از نعم بر جاستی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو هم ای نادان دلی آور بدست</p></div>
<div class="m2"><p>شاهدی را محملی آور بدست </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خویش را در باز و خود را بازگیر</p></div>
<div class="m2"><p>دام گستر از نیاز و ناز گیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عشق را آتش زن اندیشه کن</p></div>
<div class="m2"><p>روبه حق باش و شیری پیشه کن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خوف حق عنوان ایمان است و بس</p></div>
<div class="m2"><p>خوف غیر از شرک پنهان است و بس</p></div></div>