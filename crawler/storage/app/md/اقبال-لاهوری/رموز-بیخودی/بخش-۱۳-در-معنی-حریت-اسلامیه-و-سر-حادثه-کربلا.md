---
title: >-
    بخش ۱۳ - در معنی حریت اسلامیه و سر حادثهٔ کربلا
---
# بخش ۱۳ - در معنی حریت اسلامیه و سر حادثهٔ کربلا

<div class="b" id="bn1"><div class="m1"><p>هر که پیمان با هوالموجود بست</p></div>
<div class="m2"><p>گردنش از بند هر معبود رست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مؤمن از عشق است و عشق از مؤمنست</p></div>
<div class="m2"><p>عشق را ناممکن ما ممکن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل سفاک است و او سفاک تر</p></div>
<div class="m2"><p>پاک تر چالاک تر بیباک تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل در پیچاک اسباب و علل</p></div>
<div class="m2"><p>عشق چوگان باز میدان عمل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق صید از زور بازو افکند</p></div>
<div class="m2"><p>عقل مکار است و دامی میزند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل را سرمایه از بیم و شک است</p></div>
<div class="m2"><p>عشق را عزم و یقین لاینفک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن کند تعمیر تا ویران کند</p></div>
<div class="m2"><p>این کند ویران که آبادان کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل چون باد است ارزان در جهان</p></div>
<div class="m2"><p>عشق کمیاب و بهای او گران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل محکم از اساس چون و چند</p></div>
<div class="m2"><p>عشق عریان از لباس چون و چند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل می گوید که خود را پیش کن</p></div>
<div class="m2"><p>عشق گوید امتحان خویش کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عقل با غیر آشنا از اکتساب</p></div>
<div class="m2"><p>عشق از فضل است و با خود در حساب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقل گوید شاد شو آباد شو</p></div>
<div class="m2"><p>عشق گوید بنده شو آزاد شو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشق را آرام جان حریت است</p></div>
<div class="m2"><p>ناقه اش را ساربان حریت است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن شنیدستی که هنگام نبرد</p></div>
<div class="m2"><p>عشق با عقل هوس پرور چه کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن امام عاشقان پور بتول</p></div>
<div class="m2"><p>سرو آزادی ز بستان رسول</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>الله الله بای بسم الله پدر</p></div>
<div class="m2"><p>معنی ذبح عظیم آمد پسر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بهر آن شهزاده ی خیر الملل</p></div>
<div class="m2"><p>دوش ختم المرسلین نعم الجمل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سرخ رو عشق غیور از خون او</p></div>
<div class="m2"><p>شوخی این مصرع از مضمون او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در میان امت ان کیوان جناب</p></div>
<div class="m2"><p>همچو حرف قل هو الله در کتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>موسی و فرعون و شبیر و یزید</p></div>
<div class="m2"><p>این دو قوت از حیات آید پدید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زنده حق از قوت شبیری است</p></div>
<div class="m2"><p>باطل آخر داغ حسرت میری است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون خلافت رشته از قرآن گسیخت</p></div>
<div class="m2"><p>حریت را زهر اندر کام ریخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خاست آن سر جلوه ی خیرالامم</p></div>
<div class="m2"><p>چون سحاب قبله باران در قدم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر زمین کربلا بارید و رفت</p></div>
<div class="m2"><p>لاله در ویرانه ها کارید و رفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا قیامت قطع استبداد کرد</p></div>
<div class="m2"><p>موج خون او چمن ایجاد کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بهر حق در خاک و خون غلتیده است</p></div>
<div class="m2"><p>پس بنای لااله گردیده است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مدعایش سلطنت بودی اگر</p></div>
<div class="m2"><p>خود نکردی با چنین سامان سفر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دشمنان چون ریگ صحرا لاتعد</p></div>
<div class="m2"><p>دوستان او به یزدان هم عدد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سر ابراهیم و اسمعیل بود</p></div>
<div class="m2"><p>یعنی آن اجمال را تفصیل بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عزم او چون کوهساران استوار</p></div>
<div class="m2"><p>پایدار و تند سیر و کامگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تیغ بهر عزت دین است و بس</p></div>
<div class="m2"><p>مقصد او حفظ آئین است و بس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ماسوی الله را مسلمان بنده نیست</p></div>
<div class="m2"><p>پیش فرعونی سرش افکنده نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خون او تفسیر این اسرار کرد</p></div>
<div class="m2"><p>ملت خوابیده را بیدار کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تیغ لا چون از میان بیرون کشید</p></div>
<div class="m2"><p>از رگ ارباب باطل خون کشید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نقش الا الله بر صحرا نوشت</p></div>
<div class="m2"><p>سطر عنوان نجات ما نوشت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رمز قرآن از حسین آموختیم</p></div>
<div class="m2"><p>ز آتش او شعله ها اندوختیم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شوکت شام و فر بغداد رفت</p></div>
<div class="m2"><p>سطوت غرناطه هم از یاد رفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تار ما از زخمه اش لرزان هنوز</p></div>
<div class="m2"><p>تازه از تکبیر او ایمان هنوز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای صبا ای پیک دور افتادگان</p></div>
<div class="m2"><p>اشک ما بر خاک پاک او رسان</p></div></div>