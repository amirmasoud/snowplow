---
title: >-
    بخش ۱۷ - در معنی اینکه نظام ملت غیر از آئین صورت نبندد و آئین ملت محمدیه قرآن است
---
# بخش ۱۷ - در معنی اینکه نظام ملت غیر از آئین صورت نبندد و آئین ملت محمدیه قرآن است

<div class="b" id="bn1"><div class="m1"><p>ملتی را رفت چون آئین ز دست</p></div>
<div class="m2"><p>مثل خاک اجزای او از هم شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستی مسلم ز آئین است و بس</p></div>
<div class="m2"><p>باطن دین نبی این است و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگ گل شد چون ز آئین بسته شد</p></div>
<div class="m2"><p>گل ز آئین بسته شد گلدسته شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نغمه از ضبط صدا پیداستی</p></div>
<div class="m2"><p>ضبط چون رفت از صدا غوغاستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گلوی ما نفس موج هواست</p></div>
<div class="m2"><p>چون هوا پابند نی گردد ، نواست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو همی دانی که آئین تو چیست؟</p></div>
<div class="m2"><p>زیر گردون سر تمکین تو چیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن کتاب زنده قرآن حکیم</p></div>
<div class="m2"><p>حکمت او لایزال است و قدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسخه ی اسرار تکوین حیات</p></div>
<div class="m2"><p>بی ثبات از قوتش گیرد ثبات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حرف او را ریب نی تبدیل نی</p></div>
<div class="m2"><p>آیه اش شرمنده ی تأویل نی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پخته تر سودای خام از زور او</p></div>
<div class="m2"><p>در فتد با سنگ ، جام از زور او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می برد پابند و آزاد آورد</p></div>
<div class="m2"><p>صید بندان را بفریاد آورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نوع انسان را پیام آخرین </p></div>
<div class="m2"><p>حامل او رحمة للعالمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ارج می گیرد ازو ناارجمند</p></div>
<div class="m2"><p>بنده را از سجده سازد سر بلند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رهزنان از حفظ او رهبر شدند</p></div>
<div class="m2"><p>از کتابی صاحب دفتر شدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دشت پیمایان ز تاب یک چراغ</p></div>
<div class="m2"><p>صد تجلی از علوم اندر دماغ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه دوش کوه بارش بر نتافت</p></div>
<div class="m2"><p>سطوت او زهره ی گردون شکافت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بنگر آن سرمایه ی آمال ما</p></div>
<div class="m2"><p>گنجد اندر سینه ی اطفال ما</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن جگر تاب بیابان کم آب</p></div>
<div class="m2"><p>چشم او احمر ز سوز آفتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خوشتر از آهو رم جمازه اش</p></div>
<div class="m2"><p>گرم چون آتش دم جمازه اش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رخت خواب افکنده در زیر نخیل</p></div>
<div class="m2"><p>صبحدم بیدار از بانگ رحیل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دشت سیر از بام و در ناآشنا</p></div>
<div class="m2"><p>هرزه گردد از حضر ناآشنا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا دلش از گرمی قرآن تپید</p></div>
<div class="m2"><p>موج بیتابش چو گوهر آرمید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خواند ز آیات مبین او سبق</p></div>
<div class="m2"><p>بنده آمد ‘ خواجه رفت از پیش حق</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از جهانبانی نوازد ساز او</p></div>
<div class="m2"><p>مسند جم گشت پا انداز او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شهر ها از گرد پایش ریختند</p></div>
<div class="m2"><p>صد چمن از یک گلش انگیختند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای گرفتار رسوم ایمان تو</p></div>
<div class="m2"><p>شیوه های کافری زندان تو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قطع کردی امر خود را در زبر</p></div>
<div class="m2"><p>جاده پیمای الی «شئی نکر»</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر تو میخواهی مسلمان زیستن</p></div>
<div class="m2"><p>نیست ممکن جز بقرآن زیستن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صوفی پشمینه پوش حال مست</p></div>
<div class="m2"><p>از شراب نغمه ی قوال مست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آتش از شعر عراقی در دلش</p></div>
<div class="m2"><p>در نمی سازد بقرآن محفلش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از کلاه و بوریا تاج و سریر</p></div>
<div class="m2"><p>فقر او از خانقاهان باج گیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>واعظ دستان زن افسانه بند</p></div>
<div class="m2"><p>معنی او پست و حرف او بلند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از خطیب و دیلمی گفتار او</p></div>
<div class="m2"><p>با ضعیف و شاذ و مرسل کار او</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از تلاوت بر تو حق دارد کتاب</p></div>
<div class="m2"><p>تو ازو کامی که میخواهی بیاب</p></div></div>