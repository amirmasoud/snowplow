---
title: >-
    بخش ۲ - پیشکش به حضور ملت اسلامیه
---
# بخش ۲ - پیشکش به حضور ملت اسلامیه

<div class="b" id="bn1"><div class="m1"><p>منکر نتوان گشت اگر دم زنم از عشق</p></div>
<div class="m2"><p>این نشه بمن نیست اگر با دگری هست</p></div></div>
<div class="b2" id="bn2"><p>عرفی</p></div>
<div class="b" id="bn3"><div class="m1"><p>ای ترا حق خاتم اقوام کرد</p></div>
<div class="m2"><p>بر تو هر آغاز را انجام کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای مثال انبیا پاکان تو</p></div>
<div class="m2"><p>همگر دلها جگر چاکان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای نظر بر حسن ترسازاده ئی</p></div>
<div class="m2"><p>ای ز راه کعبه دور افتاده ئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای فلک مشت غبار کوی تو</p></div>
<div class="m2"><p>«ای تماشا گاه عالم روی تو»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو موج ، آتش ته پا میروی</p></div>
<div class="m2"><p>«تو کجا بهر تماشا میروی»</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رمز سوز آموز از پروانه ئی</p></div>
<div class="m2"><p>در شرر تعمیر کن کاشانه ئی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طرح عشق انداز اندر جان خویش</p></div>
<div class="m2"><p>تازه کن با مصطفی پیمان خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاطرم از صحبت ترسا گرفت</p></div>
<div class="m2"><p>تا نقاب روی تو بالا گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم نوا از جلوه ی اغیار گفت</p></div>
<div class="m2"><p>داستان گیسو و رخسار گفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر در ساقی جبین فرسود او</p></div>
<div class="m2"><p>قصه ی مغ زادگان پیمود او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من شهید تیغ ابروی تو ام</p></div>
<div class="m2"><p>خاکم و آسوده ی کوی تو ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از ستایش گستری بالاترم</p></div>
<div class="m2"><p>پیش هر دیوان فرو ناید سرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از سخن آئینه سازم کرده اند</p></div>
<div class="m2"><p>وز سکندر بی نیازم کرده اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بار احسان بر نتابد گردنم</p></div>
<div class="m2"><p>در گلستان غنچه گردد دامنم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سخت کوشم مثل خنجر در جهان</p></div>
<div class="m2"><p>آب خود می گیرم از سنگ گران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرچه بحرم موج من بیتاب نیست</p></div>
<div class="m2"><p>بر کف من کاسه ی گرداب نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پرده ی رنگم شمیمی نیستم</p></div>
<div class="m2"><p>صید هر موج نسیمی نیستم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در شرار آباد هستی اخگرم</p></div>
<div class="m2"><p>خلعتی بخشد مرا خاکسترم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر درت جانم نیاز آورده است</p></div>
<div class="m2"><p>هدیه ی سوز و گداز آورده است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز آسمان آبگون یم می چکد</p></div>
<div class="m2"><p>بر دل گرمم دمادم می چکد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من ز جو باریکتر می سازمش</p></div>
<div class="m2"><p>تا به صحن گلشنت اندازمش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زانکه تو محبوب یار ماستی</p></div>
<div class="m2"><p>همچو دل اندر کنار ماستی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عشق تا طرح فغان در سینه ریخت</p></div>
<div class="m2"><p>آتش او از دلم آئینه ریخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مثل گل از هم شکافم سینه را</p></div>
<div class="m2"><p>پیش تو آویزم این آئینه را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا نگاهی افکنی بر روی خویش</p></div>
<div class="m2"><p>می شوی زنجیری گیسوی خویش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باز خوانم قصه ی پارینه ات</p></div>
<div class="m2"><p>تازه سازم داغهای سینه ات</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از پی قوم ز خود نامحرمی</p></div>
<div class="m2"><p>خواستم از حق حیات محکمی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در سکوت نیم شب نالان بدم</p></div>
<div class="m2"><p>عالم اندر خواب و من گریان بدم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جانم از صبر و سکون محروم بود</p></div>
<div class="m2"><p>ورد من یاحی و یاقیوم بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آرزوئی داشتم خون کردمش</p></div>
<div class="m2"><p>تا ز راهدیده بیرون کردمش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سوختن چون لاله پیهم تا کجا</p></div>
<div class="m2"><p>از سحر دریوز شبنم تا کجا؟</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اشک خود بر خویش می ریزم چو شمع</p></div>
<div class="m2"><p>با شب یلدا در آویزم چو شمع</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جلوه را افزودم و خود کاستم</p></div>
<div class="m2"><p>دیگران را محفلی آراستم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یک نفس فرصت ز سوز سینه نیست</p></div>
<div class="m2"><p>هفته ام شرمنده ی آدینه نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جانم اندر پیکر فرسوده ئی</p></div>
<div class="m2"><p>جلوه ی آهی است گرد آلوده ئی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون مرا صبح ازل حق آفرید</p></div>
<div class="m2"><p>ناله در ابریشم عودم تپید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ناله ئی افشا گر اسرار عشق</p></div>
<div class="m2"><p>خونبهای حسرتگفتار عشق</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فطرت آتش دهد خاشاک را</p></div>
<div class="m2"><p>شوخی پروانه بخشد خاک را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عشق را داغی مثال لاله بس</p></div>
<div class="m2"><p>در گریبانش گل یک ناله بس</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من همین یک گل بدستارت زنم</p></div>
<div class="m2"><p>محشری بر خواب سرشارت زنم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا ز خاکت لاله زار آید پدید</p></div>
<div class="m2"><p>از دمت باد بهار آید پدید</p></div></div>