---
title: >-
    بخش ۲۱ - در معنی اینکه حیات ملیه مرکز محسوس میخواهد و مرکز ملت اسلامیه بیت الحرام است
---
# بخش ۲۱ - در معنی اینکه حیات ملیه مرکز محسوس میخواهد و مرکز ملت اسلامیه بیت الحرام است

<div class="b" id="bn1"><div class="m1"><p>می گشایم عقده از کار حیات</p></div>
<div class="m2"><p>سازمت آگاه اسرار حیات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خیال از خود رمیدن پیشه اش</p></div>
<div class="m2"><p>از جهت دامن کشیدن پیشه اش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان دیر و زود آید چسان</p></div>
<div class="m2"><p>وقت او فردا و دی زاید چسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نظرداری یکی بر خود نگر</p></div>
<div class="m2"><p>جز رم پیهم نه ئی ای بیخبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نماید تاب نامشهود خویش</p></div>
<div class="m2"><p>شعله ی او پرده بند از دود خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیر او را تا سکون بیند نظر</p></div>
<div class="m2"><p>موج جویش بسته آمد در گهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش او دم بخویش اندر کشید</p></div>
<div class="m2"><p>لاله گردید و ز شاخی بر دمید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فکر خام تو گران خیز است و لنگ</p></div>
<div class="m2"><p>تهمت گل بست بر پرواز رنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زندگی مرغ نشیمن ساز نیست</p></div>
<div class="m2"><p>طایر رنگ است و جز پرواز نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در قفس وامانده و آزاد هم</p></div>
<div class="m2"><p>با نواها می زند فریاد هم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پرش پرواز شوید دمبدم</p></div>
<div class="m2"><p>چاره ی خود کرده جوید دمبدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقده ها خود می زند در کار خویش</p></div>
<div class="m2"><p>باز آسان می کند دشوار خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پا بگل گردد حیات تیزگام</p></div>
<div class="m2"><p>تا دو بالا گرددش ذوق خرام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سازها خوابیده اندر سوز او</p></div>
<div class="m2"><p>دوش و فردا زاده ی امروز او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دمبدم مشکل گر و آسان گذار</p></div>
<div class="m2"><p>دمبدم نو آفرین و تازه کار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرچه مثل بو سراپایش رم است</p></div>
<div class="m2"><p>چون وطن در سینه ئی گیرد دم است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رشته های خویش را بر خود تند</p></div>
<div class="m2"><p>تکمه ئی گردد گره بر خود زند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در گره چون دانه دارد برگ و بر</p></div>
<div class="m2"><p>چشم بر خود وا کند گردد شجر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خلعتی از آب و گل پیدا کند</p></div>
<div class="m2"><p>دست و پا و چشم و دل پیدا کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خلوت اندر تن گزیند زندگی</p></div>
<div class="m2"><p>انجمن ها آفریند زندگی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همچنان آئین میلاد امم</p></div>
<div class="m2"><p>زندگی بر مرکزی آید بهم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حلقه را مرکز چو جان در پیکر است</p></div>
<div class="m2"><p>خط او در نقطه ی او مضمر است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قوم را ربط و نظام از مرکزی</p></div>
<div class="m2"><p>روزگارش را دوام از مرکزی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>راز دار و راز ما بیت الحرم</p></div>
<div class="m2"><p>سوز ما هم ساز ما بیت الحرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون نفس در سینه او را پروریم</p></div>
<div class="m2"><p>جان شیرین است او ما پیکریم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تازه رو بستان ما از شبنمش</p></div>
<div class="m2"><p>مزرع ما آب گیر از زمزمش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تاب دار از ذره هایش آفتاب</p></div>
<div class="m2"><p>غوطه زن اندر فضایش آفتاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دعوی او را دلیل استیم ما</p></div>
<div class="m2"><p>از براهین خلیل استیم ما</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در جهان ما را بلند آوازه کرد</p></div>
<div class="m2"><p>با حدوث ما قدم شیرازه کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ملت بیضا ز طوفش هم نفس</p></div>
<div class="m2"><p>همچو صبح آفتاب اندر قفس</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از حساب او یکی بسیاریت</p></div>
<div class="m2"><p>پخته از بند یکی خودداریت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو ز پیوند حریمی زنده ئی</p></div>
<div class="m2"><p>تا طواف او کنی پاینده ئی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در جهان جان امم جمعیت است</p></div>
<div class="m2"><p>در نگر سر حرم جمعیت است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عبرتی ای مسلم روشن ضمیر</p></div>
<div class="m2"><p>از مآل امت موسی بگیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>داد چون آن قوم مرکز را ز دست</p></div>
<div class="m2"><p>رشته ی جمعیت ملت شکست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آنکه بالید اندر آغوش رسل</p></div>
<div class="m2"><p>جزو او داننده ی اسرار کل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دهر سیلی بر بنا گوشش کشید</p></div>
<div class="m2"><p>زندگی خون گشت و از چشمش چکید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رفت نم از ریشه های تاک او</p></div>
<div class="m2"><p>بید مجنون هم نروید خاک او</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از گل غربت زبان گم کرده ئی</p></div>
<div class="m2"><p>هم نوا هم آشیان گم کرده ئی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شمع مرد و نوحه خوان پروانه اش</p></div>
<div class="m2"><p>مشت خاکم لرزد از افسانه اش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ای ز تیغ جور گردون خسته تن</p></div>
<div class="m2"><p>ای اسیر التباس و وهم و ظن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پیرهن را جامه احرام کن</p></div>
<div class="m2"><p>صبح پیدا از غبار شام کن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مثل آبا غرق اندر سجده شو</p></div>
<div class="m2"><p>آنچنان گم شو که یکسر سجده شو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مسلم پیشین نیازی آفرید</p></div>
<div class="m2"><p>تا به ناز عالم آشوبی رسید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در ره حق پا به نوک خار خست</p></div>
<div class="m2"><p>گلستان در گوشه ی دستار بست</p></div></div>