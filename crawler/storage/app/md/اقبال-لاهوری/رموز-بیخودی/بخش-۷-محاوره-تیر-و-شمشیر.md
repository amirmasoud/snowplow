---
title: >-
    بخش ۷ - محاورهٔ تیر و شمشیر
---
# بخش ۷ - محاورهٔ تیر و شمشیر

<div class="b" id="bn1"><div class="m1"><p>سر حق تیر از لب سوفار گفت</p></div>
<div class="m2"><p>تیغ را در گرمی پیکار گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای پریها جوهر اندر قاف تو</p></div>
<div class="m2"><p>ذوالفقار حیدر از اسلاف تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قوت بازوی خالد دیده ئی</p></div>
<div class="m2"><p>شام را بر سر شفق پاشیده ئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش قهر خدا سرمایه ات</p></div>
<div class="m2"><p>جنت الفردوس زیر سایه ات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هوایم یا میان ترکشم</p></div>
<div class="m2"><p>هر کجا باشم سراپا آتشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کمان آیم چو سوی سینه من</p></div>
<div class="m2"><p>نیک می بینم به توی سینه من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نباشد در میان قلب سلیم</p></div>
<div class="m2"><p>فارغ از اندیشه های یأس و بیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چاک چاک از نوک خود گردانمش</p></div>
<div class="m2"><p>نیمه ئی از موج خون پوشانمش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور صفای او ز قلب مؤمن است</p></div>
<div class="m2"><p>ظاهرش روشن ز نور باطن است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از تف او آب گردد جان من</p></div>
<div class="m2"><p>همچو شبنم می چکد پیکان من</p></div></div>