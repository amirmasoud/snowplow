---
title: >-
    بخش ۲۷ - خطاب به مخدرات اسلام
---
# بخش ۲۷ - خطاب به مخدرات اسلام

<div class="b" id="bn1"><div class="m1"><p>ای ردایت پردهٔ ناموس ما</p></div>
<div class="m2"><p>تاب تو سرمایهٔ فانوس ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طینت پاک تو ما را رحمت است</p></div>
<div class="m2"><p>قوت دین و اساس ملت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کودک ما چون لب از شیر تو شست</p></div>
<div class="m2"><p>لااله آموختی او را نخست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می تراشد مهر تو اطوار ما</p></div>
<div class="m2"><p>فکر ما گفتار ما کردار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برق ما کو در سحابت آرمید</p></div>
<div class="m2"><p>بر جبل رخشید و در صحرا تپید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای امین نعمت آئین حق</p></div>
<div class="m2"><p>در نفسهای تو سوز دین حق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دور حاضر تر فروش و پر فن است</p></div>
<div class="m2"><p>کاروانش نقد دین را رهزن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کور و یزدان ناشناس ادراک او</p></div>
<div class="m2"><p>ناکسان زنجیری پیچاک او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم او بیباک و ناپرواستی</p></div>
<div class="m2"><p>پنجهٔ مژگان او گیراستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صید او آزاد خواند خویش را</p></div>
<div class="m2"><p>کشتهٔ او زنده داند خویش را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب بند نخل جمعیت توئی</p></div>
<div class="m2"><p>حافظ سرمایهٔ ملت توئی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از سر سود و زیان سودا مزن</p></div>
<div class="m2"><p>گام جز بر جادهٔ آبا مزن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هوشیار از دستبرد روزگار</p></div>
<div class="m2"><p>گیر فرزندان خود را در کنار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این چمن زادان که پر نگشاده اند</p></div>
<div class="m2"><p>ز آشیان خویش دور افتاده اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فطرت تو جذبه ها دارد بلند</p></div>
<div class="m2"><p>چشم هوش از اسوهٔ زهرا مبند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا حسینی شاخ تو بار آورد</p></div>
<div class="m2"><p>موسم پیشین بگلزار آورد</p></div></div>