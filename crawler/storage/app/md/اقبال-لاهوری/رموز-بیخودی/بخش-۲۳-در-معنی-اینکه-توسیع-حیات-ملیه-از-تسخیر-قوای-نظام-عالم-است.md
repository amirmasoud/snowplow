---
title: >-
    بخش ۲۳ - در معنی اینکه توسیع حیات ملیه از تسخیر قوای نظام عالم است
---
# بخش ۲۳ - در معنی اینکه توسیع حیات ملیه از تسخیر قوای نظام عالم است

<div class="b" id="bn1"><div class="m1"><p>ای که با نادیده پیمان بسته‌ای</p></div>
<div class="m2"><p>همچو سیل از قید ساحل رسته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نهال از خاک این گلزار خیز</p></div>
<div class="m2"><p>دل بغائب بند و با حاضر ستیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هستی حاضر کند تفسیر غیب</p></div>
<div class="m2"><p>می شود دیباچهٔ تسخیر غیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما سوا از بهر تسخیر است و بس</p></div>
<div class="m2"><p>سینهٔ او عرضهٔ تیر است و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کن حق ما سوا شد آشکار</p></div>
<div class="m2"><p>تا شود پیکان تو سندان گذار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رشته ئی باید گره اندر گره</p></div>
<div class="m2"><p>تا شود لطف گشودن را فره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنچه ئی؟ از خود چمن تعبیر کن</p></div>
<div class="m2"><p>شبنمی؟ خورشید را تسخیر کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تو می آید اگر کار شگرف</p></div>
<div class="m2"><p>از دمی گرمی گداز این شیر برف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که محسوسات را تسخیر کرد</p></div>
<div class="m2"><p>عالمی از ذره ئی تعمیر کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنکه تیرش قدسیان را سینه خست</p></div>
<div class="m2"><p>اول آدم را سر فتراک بست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عقدهٔ محسوس را اول گشود</p></div>
<div class="m2"><p>همت از تسخیر موجود آزمود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کوه و صحرا دشت و دریا بحر و بر</p></div>
<div class="m2"><p>تختهٔ تعلیم ارباب نظر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای که از تأثیر افیون خفته ئی</p></div>
<div class="m2"><p>عالم اسباب را دون گفته ئی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خیز و وا کن دیدهٔ مخمور را</p></div>
<div class="m2"><p>دون مخوان این عالم مجبور را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غایتش توسیع ذات مسلم است</p></div>
<div class="m2"><p>امتحان ممکنات مسلم است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>می زند شمشیر دوران بر تنت</p></div>
<div class="m2"><p>تا ببینی هست خون اندر تنت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سینه را از سنگ زوری ریش کن</p></div>
<div class="m2"><p>امتحان استخوان خویش کن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حق جهان را قسمت نیکان شمرد</p></div>
<div class="m2"><p>جلوه اش با دیدهٔ مؤمن سپرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کاروان را رهگذار است این جهان</p></div>
<div class="m2"><p>نقد مؤمن را عیار است این جهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گیر او را تا نه او گیرد ترا</p></div>
<div class="m2"><p>همچو می اندر سبو گیرد ترا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دلدل اندیشه ات طوطی پر است</p></div>
<div class="m2"><p>آنکه گامش آسمان پهناور است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>احتیاج زندگی میراندش</p></div>
<div class="m2"><p>بر زمین گردون سپر گرداندش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا ز تسخیر قوای این نظام </p></div>
<div class="m2"><p>ذوفنونیهای تو گردد تمام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نایب حق در جهان آدم شود</p></div>
<div class="m2"><p>بر عناصر حکم او محکم شود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تنگی ات پهنا پذیرد در جهان</p></div>
<div class="m2"><p>کار تو اندام گیرد در جهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خویش را بر پشت باد اسوار کن</p></div>
<div class="m2"><p>یعنی این جمازه را ماهار کن </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دست رنگین کن ز خون کوهسار</p></div>
<div class="m2"><p>جوی آب گوهر از دریا برآر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صد جهان در یک فضا پوشیده اند</p></div>
<div class="m2"><p>مهر ها در ذره ها پوشیده اند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از شعاعش دیده کن نادیده را</p></div>
<div class="m2"><p>وا نما اسرار نافهمیده را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تابش از خورشید عالم تاب گیر</p></div>
<div class="m2"><p>برق طاق افروز از سیلاب گیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ثابت و سیاره گردون وطن</p></div>
<div class="m2"><p>آن خداوندان اقوام کهن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اینهمه ای خواجه آغوش تو اند</p></div>
<div class="m2"><p>پیش خیز وحلقه در گوش تو اند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جستجو را محکم از تدبیر کن</p></div>
<div class="m2"><p>انفس و آفاق را تسخیر کن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چشم خود بگشا و در اشیا نگر</p></div>
<div class="m2"><p>نشه زیر پردهٔ صهبا نگر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا نصیب از حکمت اشیا برد</p></div>
<div class="m2"><p>ناتوان باج از توانایان خورد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>صورت هستی ز معنی ساده نیست</p></div>
<div class="m2"><p>این کهن ساز از نوا افتاده نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>برق آهنگ است هشیارش زنند</p></div>
<div class="m2"><p>خویش را چون زخمه بر تارش زنند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو که مقصود خطاب انظری</p></div>
<div class="m2"><p>پس چرا این راه چون کوران بری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>قطره ئی کز خود فروزی محرم است</p></div>
<div class="m2"><p>باده اندر تاک و بر گل شبنم است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون بدریا در رود گوهر شود</p></div>
<div class="m2"><p>جوهرش تابنده چون اختر شود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون صبا بر صورت گلها متن</p></div>
<div class="m2"><p>غوطه اندر معنی گلزار زن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آنکه بر اشیا کمند انداخت است</p></div>
<div class="m2"><p>مرکب از برق و حرارت ساخت است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>حرف چون طایر به پرواز آورد</p></div>
<div class="m2"><p>نغمه را بی زخمه از ساز آورد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای خرت لنگ از ره دشوار زیست</p></div>
<div class="m2"><p>غافل از هنگامهٔ پیکار زیست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همرهانت پی به منزل برده اند</p></div>
<div class="m2"><p>لیلی معنی ز محمل برده اند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تو بصحرا مثل قیس آواره ئی</p></div>
<div class="m2"><p>خسته ئی وامانده ئی بیچاره ئی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>علم اسما اعتبار آدم است</p></div>
<div class="m2"><p>حکمت اشیا حصار آدم است</p></div></div>