---
title: >-
    بخش ۱ - رموز بیخودی
---
# بخش ۱ - رموز بیخودی

<div class="b" id="bn1"><div class="m1"><p>جهد کن در بیخودی خود را بیاب</p></div>
<div class="m2"><p>زود تر والله اعلم بالصواب</p></div></div>
<div class="b2" id="bn2"><p>مولانای روم</p></div>