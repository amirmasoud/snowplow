---
title: >-
    بخش ۶ - در معنی اینکه یأس و حزن و خوف ام الخبائث است و قاطع حیات و توحید ازالهٔ این امراض خبیثه می کند
---
# بخش ۶ - در معنی اینکه یأس و حزن و خوف ام الخبائث است و قاطع حیات و توحید ازالهٔ این امراض خبیثه می کند

<div class="b" id="bn1"><div class="m1"><p>مرگ را سامان ز قطع آرزوست</p></div>
<div class="m2"><p>زندگانی محکم از لاتقنطوا ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا امید از آرزوی پیهم است</p></div>
<div class="m2"><p>نا امیدی زندگانی را سم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نا امیدی همچو گور افشاردت</p></div>
<div class="m2"><p>گرچه الوندی ز پا می آردت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناتوانی بنده ی احسان او</p></div>
<div class="m2"><p>نامرادی بسته ی دامان او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندگی را یأس خواب آور بود</p></div>
<div class="m2"><p>این دلیل سستی عنصر بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم جانرا سرمه اش اعمی کند</p></div>
<div class="m2"><p>روز روشن را شب یلدا کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دمش میرد قوای زندگی</p></div>
<div class="m2"><p>خشک گردد چشمه های زندگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خفته با غم در ته یک چادر است</p></div>
<div class="m2"><p>غم رگ جان را مثال نشتر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای که در زندان غم باشی اسیر</p></div>
<div class="m2"><p>از نبی تعلیم لاتحزن بگیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این سبق صدیق را صدیق کرد</p></div>
<div class="m2"><p>سر خوش از پیمانه ی تحقیق کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از رضا مسلم مثال کوکب است</p></div>
<div class="m2"><p>در ره هستی تبسم بر لب است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر خدا داری ز غم آزاد شو</p></div>
<div class="m2"><p>از خیال بیش و کم آزاد شو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قوت ایمان حیات افزایدت</p></div>
<div class="m2"><p>ورد «لا خوف علیهم» بایدت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون کلیمی سوی فرعونی رود</p></div>
<div class="m2"><p>قلب او از لاتخف محکم شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیم غیر الله عمل را دشمن است</p></div>
<div class="m2"><p>کاروان زندگی را رهزن است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عزم محکم ممکنات اندیش ازو</p></div>
<div class="m2"><p>همت عالی تأمل کیش ازو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تخم او چون در گلت خود را نشاند</p></div>
<div class="m2"><p>زندگی از خود نمائی باز ماند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فطرت او تنگ تاب و سازگار</p></div>
<div class="m2"><p>با دل لرزان و دست رعشه دار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دزدد از پا طاقت رفتار را</p></div>
<div class="m2"><p>می رباید از دماغ افکار را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دشمنت ترسان اگر بیند ترا</p></div>
<div class="m2"><p>از خیابانت چو گل چیند ترا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ضرب تیغ او قوی تر می فتد</p></div>
<div class="m2"><p>هم نگاهش مثل خنجر می فتد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بیم چون بند است اندر پای ما</p></div>
<div class="m2"><p>ورنه صد سیل است در دریای ما</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر نمی آید اگر آهنگ تو</p></div>
<div class="m2"><p>نرم از بیم است تار چنگ تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گوشتابش ده که گردد نغمه خیز</p></div>
<div class="m2"><p>بر فلک از ناله آرد رستخیز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بیم ، جاسوسی است از اقلیم مرگ</p></div>
<div class="m2"><p>اندرونش تیره مثل میم مرگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چشم او برهمزن کار حیات</p></div>
<div class="m2"><p>گوش او بزگیر اخبار حیات</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر شر پنهان که اندر قلب تست</p></div>
<div class="m2"><p>اصل او بیم است اگر بینی درست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لابه و مکاری و کین و دروغ</p></div>
<div class="m2"><p>این همه از خوف می گیرد فروغ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پرده ی زور و ریا پیراهنش</p></div>
<div class="m2"><p>فتنه را آغوش مادر دامنش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زانکه از همت نباشد استوار</p></div>
<div class="m2"><p>می شود خوشنود با ناسازگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر که رمز مصطفی فهمیده است</p></div>
<div class="m2"><p>شرک را در خوف مضمر دیده است</p></div></div>