---
title: >-
    بخش ۳۰ - لم یلد و لم یولد
---
# بخش ۳۰ - لم یلد و لم یولد

<div class="b" id="bn1"><div class="m1"><p>قوم تو از رنگ و خون بالاتر است</p></div>
<div class="m2"><p>قیمت یک اسودش صد احمر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطرهٔ آب وضوی قنبری</p></div>
<div class="m2"><p>در بها برتر ز خون قیصری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فارغ از باب و ام و اعمام باش</p></div>
<div class="m2"><p>همچو سلمان زادهٔ اسلام باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکته ئی ای همدم فرزانه بین</p></div>
<div class="m2"><p>شهد را در خانه های لانه بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قطره ئی از لالهٔ حمراستی</p></div>
<div class="m2"><p>قطره ئی از نرگس شهلاستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این نمی گوید که من از عبهرم</p></div>
<div class="m2"><p>آن نمی گوید من از نیلوفرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملت ما شان ابراهیمی است</p></div>
<div class="m2"><p>شهد ما ایمان ابراهیمی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نسب را جزو ملت کرده ئی</p></div>
<div class="m2"><p>رخنه در کار اخوت کرده ئی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در زمین ما نگیرد ریشه ات</p></div>
<div class="m2"><p>هست نا مسلم هنوز اندیشه ات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابن مسعود آن چراغ افروز عشق</p></div>
<div class="m2"><p>جسم و جان او سراپا سوز عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوخت از مرگ برادر سینه اش</p></div>
<div class="m2"><p>آب گردید از گداز آئینه اش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گریه های خویش را پایان ندید</p></div>
<div class="m2"><p>در غمش چون مادران شیون کشید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>« ای دریغا آن سبق خوان نیاز</p></div>
<div class="m2"><p>یار من اندر دبستان نیاز» </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>«آه آن سرو سهی بالای من</p></div>
<div class="m2"><p>در ره عشق نبی همپای من»</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>«حیف او محروم دربار نبی</p></div>
<div class="m2"><p>چشم من روشن ز دیدار نبی»</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست از روم و عرب پیوند ما</p></div>
<div class="m2"><p>نیست پابند نسب پیوند ما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل به محبوب حجازی بسته ایم</p></div>
<div class="m2"><p>زین جهت با یکدگر پیوسته ایم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رشتهٔ ما یک تولایش بس است</p></div>
<div class="m2"><p>چشم ما را کیف صهبایش بس است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مستی او تا بخون ما دوید</p></div>
<div class="m2"><p>کهنه را آتش زد و نو آفرید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عشق او سرمایهٔ جمعیت است</p></div>
<div class="m2"><p>همچو خون اندر عروق ملت است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عشق در جان و نسب در پیکر است</p></div>
<div class="m2"><p>رشتهٔ عشق از نسب محکم تر است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عشق ورزی از نسب باید گذشت</p></div>
<div class="m2"><p>هم ز ایران و عرب باید گذشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>امت او مثل او نور حق است</p></div>
<div class="m2"><p>هستی ما از وجودش مشتق است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>«نور حق را کس نجوید زاد و بود</p></div>
<div class="m2"><p>خلعت حق را چه حاجت تار و پود»</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر که پا در بند اقلیم و جد است</p></div>
<div class="m2"><p>بی خبر از لم یلد لم یولد است</p></div></div>