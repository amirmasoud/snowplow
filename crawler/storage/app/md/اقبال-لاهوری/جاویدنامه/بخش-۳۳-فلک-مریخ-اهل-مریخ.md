---
title: >-
    بخش ۳۳ - فلک مریخ  اهل مریخ
---
# بخش ۳۳ - فلک مریخ  اهل مریخ

<div class="b" id="bn1"><div class="m1"><p>چشم را یک لحظه بستم اندر آب</p></div>
<div class="m2"><p>اندکی از خود گسستم اندر آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخت بردم زی جهانی دیگری</p></div>
<div class="m2"><p>با زمان و با مکانی دیگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتاب ما به آفاقش رسید</p></div>
<div class="m2"><p>روز و شب را نوع دیگر آفرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن ز رسم و راه جان بیگانه ایست</p></div>
<div class="m2"><p>در زمان و از زمان بیگانه ایست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان ما سازد بهر سوزی که هست</p></div>
<div class="m2"><p>وقت او خرم بهر روزی که هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می نگردد کهنه از پرواز روز</p></div>
<div class="m2"><p>روزها از نور او عالم فروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز و شب را گردش پیهم ازوست</p></div>
<div class="m2"><p>سیر او کن زانکه هر عالم ازوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرغزاری با رصدگاه بلند</p></div>
<div class="m2"><p>دور بین او ثریا در کمند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلوت نه گنبد خضراست این</p></div>
<div class="m2"><p>یا سواد خاکدان ماست این</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گاه جستم وسعت او را کران</p></div>
<div class="m2"><p>گاه دیدم در فضای آسمان </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیر روم آن مرشد اهل نظر</p></div>
<div class="m2"><p>گفت «مریخ است این عالم نگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون جهان ما طلسم رنگ و بوست</p></div>
<div class="m2"><p>صاحب شهر و دیار و کاخ و کوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ساکنانش چون فرنگان ذوفنون</p></div>
<div class="m2"><p>در علوم جان و تن از ما فزون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر زمان و بر مکان قاهرترند</p></div>
<div class="m2"><p>زانکه در علم فضا ماهرترند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر وجودش آنچنان پیچیده اند</p></div>
<div class="m2"><p>هر خم و پیچ فضا را دیده اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خاکیان را دل به بند آب و گل</p></div>
<div class="m2"><p>اندرین عالم بدن در بند دل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون دلی در آب و گل منزل کند</p></div>
<div class="m2"><p>هر چه می خواهد به آب و گل کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مستی و ذوق و سرور از حکم جان</p></div>
<div class="m2"><p>جسم را غیب و حضور از حکم جان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در جهان ما دو تا آمد وجود</p></div>
<div class="m2"><p>جان و تن آن بی نمود آن با نمود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خاکیان را جان و تن مرغ و قفس</p></div>
<div class="m2"><p>فکر مریخی یک اندیش است و بس</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون کسی را میرسد روز فراق</p></div>
<div class="m2"><p>چسصت تر می گردد از سوز فراق</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یک دو روزی پیشتر از آن مرگ</p></div>
<div class="m2"><p>می کند پیش کسان اعلان مرگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جانشان پروردهٔ اندام نیست</p></div>
<div class="m2"><p>لاجرم خو کردهٔ اندام نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تن بخویش اندر کشیدن مردن است</p></div>
<div class="m2"><p>از جهان در خود رمیدن مردن است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برتر از فکر تو آمد این سخن</p></div>
<div class="m2"><p>زانکه جان تست محکوم بدن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رخت اینجا یکدو دم باید گشاد</p></div>
<div class="m2"><p>اینچین فرصت خدا کس را نداد</p></div></div>