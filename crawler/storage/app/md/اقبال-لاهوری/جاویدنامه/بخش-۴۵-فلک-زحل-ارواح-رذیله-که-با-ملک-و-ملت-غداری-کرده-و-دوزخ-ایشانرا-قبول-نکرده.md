---
title: >-
    بخش ۴۵ - فلک زحل  ارواح رذیله که با ملک و ملت غداری کرده و دوزخ ایشانرا قبول نکرده
---
# بخش ۴۵ - فلک زحل  ارواح رذیله که با ملک و ملت غداری کرده و دوزخ ایشانرا قبول نکرده

<div class="b" id="bn1"><div class="m1"><p>پیر رومی آن امام راستان</p></div>
<div class="m2"><p>آشنای هر مقام راستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت ای گردون نورد سخت کوش</p></div>
<div class="m2"><p>دیده ئی آن عالم زنار پوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه بر گرد کمر پیچیده است</p></div>
<div class="m2"><p>از دم استاره ئی دزدیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گران سیری خرام او سکون</p></div>
<div class="m2"><p>هر نکو از حکم او زشت و زبون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیکر او گرچه از آب و گل است</p></div>
<div class="m2"><p>بر زمینش پا نهادن مشکل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد هزار افرشتهٔ تندر بدست</p></div>
<div class="m2"><p>قهر حق را قاسم از روز الست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دره پیهم می زند سیاره را</p></div>
<div class="m2"><p>از مدارش بر کند سیاره را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالمی مطرود و مردود سپهر</p></div>
<div class="m2"><p>صبح او مانند شام از بخل مهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منزل ارواح بی یوم النشور</p></div>
<div class="m2"><p>دوزخ از احراقشان آمد نفور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اندرون او دو طاغوت کهن</p></div>
<div class="m2"><p>روح قومی کشته از بهر دو تن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جعفر از بنگال و صادق از دکن</p></div>
<div class="m2"><p>ننگ آدم ، ننگ دین ، ننگ وطن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نا قبول و ناامید و نامراد</p></div>
<div class="m2"><p>ملتی از کارشان اندر فساد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ملتی کو بند هر ملت گشاد</p></div>
<div class="m2"><p>ملک و دینش از مقام خود فتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می ندانی خطهٔ هندوستان</p></div>
<div class="m2"><p>آن عزیز خاطر صاحبدلان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خطه ای هر جلوه اش گیتی فروز</p></div>
<div class="m2"><p>در میان خاک و خون غلطد هنوز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در گلش تخم غلامی را که کشت</p></div>
<div class="m2"><p>این همه کردار آن ارواح زشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در فضای نیلگون یکدم بایست</p></div>
<div class="m2"><p>تا مکافات عمل بینی که چیست</p></div></div>