---
title: >-
    بخش ۲۴ - ارض ملک خداست
---
# بخش ۲۴ - ارض ملک خداست

<div class="b" id="bn1"><div class="m1"><p>سر گذشت آدم اندر شرق و غرب</p></div>
<div class="m2"><p>بهر خاکی فتنه های حرب و ضرب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک عروس و شوهر او ما همه</p></div>
<div class="m2"><p>آن فسونگر بی همه هم با همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشوه های او همه مکر و فن است</p></div>
<div class="m2"><p>نی از آن تو نه از آن من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در نسازد با تو این سنگ و حجر</p></div>
<div class="m2"><p>این ز اسباب حضر تو در سفر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اختلاط خفته و بیدار چیست</p></div>
<div class="m2"><p>ثابتی را کار با سیار چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حق زمین را جز متاع ما نگفت</p></div>
<div class="m2"><p>این متاع بی بها مفت است مفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ده خدایا نکته ئی از من پذیر</p></div>
<div class="m2"><p>رزق و گور از وی بگیر او را مگیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صحبتش تا کی تو بود و او نبود</p></div>
<div class="m2"><p>تو وجود و او نمود بی وجود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو عقابی طایف افلاک شو</p></div>
<div class="m2"><p>بال و پر بگشا و پاک از خاک شو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باطن «الارض ﷲ» ظاهر است</p></div>
<div class="m2"><p>هر که این ظاهر نبیند کافر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من نگویم در گذر از کاخ و کوی</p></div>
<div class="m2"><p>دولت تست این جهان رنگ و بوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دانه دانه گوهر از خاکش بگیر</p></div>
<div class="m2"><p>صید چون شاهین ز افلاکش بگیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تیشهٔ خود را به کهسارش بزن</p></div>
<div class="m2"><p>نوری از خود گیر و بر نارش بزن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از طریق آزری بیگانه باش</p></div>
<div class="m2"><p>بر مراد خود جهان نو تراش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل به رنگ و بوی و کاخ و کو مده</p></div>
<div class="m2"><p>دل حریم اوست جز با او مده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مردن بی برگ و بی گور و کفن</p></div>
<div class="m2"><p>گم شدن در نقره و فرزند و زن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر که حرف لااله از بر کند</p></div>
<div class="m2"><p>عالمی را گم بخویش اندر کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فقر جوع و رقص و عریانی کجاست؟</p></div>
<div class="m2"><p>فقر سلطانی است رهبانی کجاست؟</p></div></div>