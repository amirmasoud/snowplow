---
title: >-
    بخش ۵۸ - پیغام سلطان شهید به رود کاویری
---
# بخش ۵۸ - پیغام سلطان شهید به رود کاویری

<div class="b2" id="bn1"><p>حقیقت حیات و مرگ و شهادت </p></div>
<div class="b" id="bn2"><div class="m1"><p>رود کاویری یکی نرمک خرام</p></div>
<div class="m2"><p>خسته ئی شاید که از سیر دوام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کهستان عمر ها نالیده ئی</p></div>
<div class="m2"><p>راه خود را با مژه کاویده ئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای مرا خوشتر ز جیحون و فرات</p></div>
<div class="m2"><p>ای دکن را آب تو آب حیات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه شهری کو در آغوش تو بود</p></div>
<div class="m2"><p>حسن نوشین جلوه از نوش تو بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کهنه گردیدی شباب تو همان</p></div>
<div class="m2"><p>پیچ و تاب و رنگ و آب تو همان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موج تو جز دانه گوهر نزاد</p></div>
<div class="m2"><p>طره تو تا ابد شوریده باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای ترا سازی که سوز زندگی است</p></div>
<div class="m2"><p>هیچ میدانی که این پیغام کیست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه میکردی طواف سطوتش</p></div>
<div class="m2"><p>بوده ئی آئینه دار دولتش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنکه صحرا ها ز تدبیرش بهشت</p></div>
<div class="m2"><p>آنکه نقش خود بخون خود نوشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه خاکش مرجع صد آرزوست</p></div>
<div class="m2"><p>اضطراب موج تو از خون اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه گفتارش همه کردار بود</p></div>
<div class="m2"><p>مشرق اندر خواب و او بیدار بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای من و تو موجی از رود حیات</p></div>
<div class="m2"><p>هر نفس دیگر شود این کائنات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زندگانی انقلاب هر دمی است</p></div>
<div class="m2"><p>زانکه او اندر سراغ عالمی است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تار و پود هر وجود از رفت و بود</p></div>
<div class="m2"><p>اینهمه ذوق نمود از رفت و بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جاده ها چون رهروان اندر سفر</p></div>
<div class="m2"><p>هر کجا پنهان سفر پیدا حضر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کاروان و ناقه و دشت و نخیل</p></div>
<div class="m2"><p>هر چه بینی نالد از درد رحیل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در چمن گل میهمان یک نفس</p></div>
<div class="m2"><p>رنگ و آبش امتحان یک نفس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>موسم گل ماتم و هم نای و نوش</p></div>
<div class="m2"><p>غنچه در آغوش و نعش گل بدوش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لاله را گفتم یکی دیگر بسوز</p></div>
<div class="m2"><p>گفت راز ما نمی دانی هنوز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از خس و خاشاک تعمیر وجود</p></div>
<div class="m2"><p>غیر حسرت چیست پاداش نمود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در سرای هست و بود آئی میا</p></div>
<div class="m2"><p>از عدم سوی وجود آئی میا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور بیائی چون شرار از خود مرو</p></div>
<div class="m2"><p>در تلاش خرمنی آواره شو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تاب و تب داری اگر مانند مهر</p></div>
<div class="m2"><p>پا بنه در وسعت آباد سپهر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کوه و مرغ و گلشن و صحرا بسوز</p></div>
<div class="m2"><p>ماهیان را در ته دریا بسوز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سینه ئی داری اگر در خورد تیر</p></div>
<div class="m2"><p>در جهان شاهین بزی شاهین بمیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زانکه در عرض حیات آمد ثبات</p></div>
<div class="m2"><p>از خدا کم خواستم طول حیات</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زندگی را چیست رسم و دین و کیش</p></div>
<div class="m2"><p>یک دم شیری به از صد سال میش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زندگی محکم ز تسلیم و رضاست</p></div>
<div class="m2"><p>موت نیرنج و طلسم و سیمیاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بندهٔ حق ضیغم و آهوست مرگ</p></div>
<div class="m2"><p>یک مقام از صد مقام اوست مرگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>می فتد بر مرگ آن مرد تمام</p></div>
<div class="m2"><p>مثل شاهینی که افتد بر حمام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر زمان میرد غلام از بیم مرگ</p></div>
<div class="m2"><p>زندگی او را حرام از بیم مرگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بندهٔ آزاد را شأنی دگر</p></div>
<div class="m2"><p>مرگ او را میدهد جانی دگر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>او خود اندیش است مرگ اندیش نیست</p></div>
<div class="m2"><p>مرگ آزادان ز آنی بیش نیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بگذر از مرگی که سازد با لحد</p></div>
<div class="m2"><p>زانکه این مرگست مرگ دام و دد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرد مؤمن خواهد از یزدان پاک</p></div>
<div class="m2"><p>آن دگر مرگی که بر گیرد ز خاک</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن دگر مرگ انتهای راه شوق</p></div>
<div class="m2"><p>آخرین تکبیر در جنگاه شوق</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گرچه هر مرگ است بر مؤمن شکر</p></div>
<div class="m2"><p>مرگ پور مرتضی چیزی دگر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جنگ شاهان جهان غارتگری است</p></div>
<div class="m2"><p>جنگ مؤمن سنت پیغمبری است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جنگ مؤمن چیست؟ هجرت سوی دوست</p></div>
<div class="m2"><p>ترک عالم اختیار کوی دوست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آنکه حرف شوق با اقوام گفت</p></div>
<div class="m2"><p>جنگ را رهبانی اسلام گفت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کس نداند جز شهید این نکته را</p></div>
<div class="m2"><p>کو بخون خود خرید این نکته را</p></div></div>