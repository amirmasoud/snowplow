---
title: >-
    بخش ۵۰ - آن سوی افلاک  مقام حکیم آلمانی نیچه
---
# بخش ۵۰ - آن سوی افلاک  مقام حکیم آلمانی نیچه

<div class="b" id="bn1"><div class="m1"><p>هر کجا استیزه ی بود و نبود</p></div>
<div class="m2"><p>کس نداند سر این چرخ کبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا مرگ آورد پیغام زیست</p></div>
<div class="m2"><p>ایخوش آنمردی که داند مرگ چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا مانند باد ارزان حیات</p></div>
<div class="m2"><p>بی ثبات و با تمنای ثبات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم من صد عالم شش روزه دید</p></div>
<div class="m2"><p>تا حد این کائنات آمد پدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر جهان را ماه و پروینی دگر</p></div>
<div class="m2"><p>زندگی را رسم و آئینی دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقت هر عالم روان مانند زو</p></div>
<div class="m2"><p>دیر یاز اینجا و آنجا تند رو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سال ما اینجا مهی آنجا دمی</p></div>
<div class="m2"><p>بیش این عالم به آن عالم کمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل ما اندر جهانی ذوفنون</p></div>
<div class="m2"><p>در جهان دیگری خوار و زبون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر ثغور این جهان چون و چند</p></div>
<div class="m2"><p>بود مردی با صدای دردمند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیدهٔ او از عقابان تیز تر</p></div>
<div class="m2"><p>طلعت او شاهد سوز جگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دمبدم سوز درون او فزود</p></div>
<div class="m2"><p>بر لبش بیتی که صد بارش سرود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>«نه جبریلی نه فردوسی نه حوری نی خداوندی</p></div>
<div class="m2"><p>کف خاکی که میسوزد ز جان آرزومندی»</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من به رومی گفتم این دیوانه کیست</p></div>
<div class="m2"><p>گفت« این فرزانهٔ المانوی ست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در میان این دو عالم جای اوست</p></div>
<div class="m2"><p>نغمهٔ دیرینه اندر نای اوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باز این حلاج بی دار و رسن</p></div>
<div class="m2"><p>نوع دیگر گفته آن حرف کهن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حرف او بی باک و افکارش عظیم</p></div>
<div class="m2"><p>غربیان از تیغ گفتارش دو نیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همنشین بر جذبه او پی نبرد</p></div>
<div class="m2"><p>بندهٔ مجذوب را مجنون شمرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عاقلان از عشق و مستی بی نصیب</p></div>
<div class="m2"><p>نبض او دادند در دست طبیب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با پزشکان چیست غیر از ریو و رنگ</p></div>
<div class="m2"><p>وای مجذوبی که زاد اندر فرنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ابن سینا بر بیاضی دل نهد</p></div>
<div class="m2"><p>رگ زند یا حب خواب آور دهد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود حلاجی به شهر خود غریب</p></div>
<div class="m2"><p>جان ز ملا برد و کشت او را طبیب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرد ره دانی نبود اندر فرنگ</p></div>
<div class="m2"><p>پس فزون شد نغمه اش از تار چنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>راهرو را کس نشان از ره نداد</p></div>
<div class="m2"><p>صد خلل در واردات او فتاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نقد بود و کس عیار او را نکرد</p></div>
<div class="m2"><p>کاردانی مرد کار او را نکرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عاشقی در آه خود گم گشته ئی</p></div>
<div class="m2"><p>سالکی در راه خود گم گشته ئی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مستی او هر زجاجی را شکست</p></div>
<div class="m2"><p>از خدا ببرید و هم از خود گسست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خواست تا بیند به چشم ظاهری</p></div>
<div class="m2"><p>اختلاط قاهری با دلبری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خواست تا از آب و گل آید برون</p></div>
<div class="m2"><p>خوشه ئی کز کشت دل آید برون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنچه او جوید مقام کبریاست</p></div>
<div class="m2"><p>این مقام از عقل و حکمت ماوراست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زندگی شرح اشارات خودی است</p></div>
<div class="m2"><p>لا و الا از مقامات خودی است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>او به لا درماند و تا الا نرفت</p></div>
<div class="m2"><p>از مقام عبده بیگانه رفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>با تجلی همکنار و بی خبر</p></div>
<div class="m2"><p>دور تر چون میوه از بیخ شجر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چشم او جز رؤیت آدم نخواست</p></div>
<div class="m2"><p>نعره بیباکانه زد «آدم کجاست»</p></div></div>
<div class="b2" id="bn34"><p>ق</p></div>
<div class="b" id="bn35"><div class="m1"><p>ورنه او از خاکیان بیزار بود</p></div>
<div class="m2"><p>مثل موسی طالب دیدار بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کاش بودی در زمان احمدی</p></div>
<div class="m2"><p>تا رسیدی بر سرور سرمدی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عقل او با خویشتن در گفتگوست</p></div>
<div class="m2"><p>تو ره خود رو که راه خود نکوست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پیش نه گامی که آمد آن مقام</p></div>
<div class="m2"><p>کاندرو بی حرف می روید کلام»</p></div></div>