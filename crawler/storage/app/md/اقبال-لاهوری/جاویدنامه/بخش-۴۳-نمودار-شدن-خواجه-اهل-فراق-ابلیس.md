---
title: >-
    بخش ۴۳ - نمودار شدن خواجهٔ اهل فراق ابلیس
---
# بخش ۴۳ - نمودار شدن خواجهٔ اهل فراق ابلیس

<div class="b" id="bn1"><div class="m1"><p>صحبت روشندلان یک دم ، دو دم</p></div>
<div class="m2"><p>آن دو دم سرمایهٔ بود و عدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق را شوریده تر کرد و گذشت</p></div>
<div class="m2"><p>عقل را صاحب نظر کرد و گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بر بربستم که با خود دارمش</p></div>
<div class="m2"><p>از مقام دیده در دل آرمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناگهان دیدم جهان تاریک شد</p></div>
<div class="m2"><p>از مکان تا لامکان تاریک شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر آن شب شعله ئی آمد پدید</p></div>
<div class="m2"><p>از درونش پیر مردی بر جهید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک قبای سرمه ئی اندر برش</p></div>
<div class="m2"><p>غرق اندر دود پیچان پیکرش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت رومی خواجهٔ اهل فراق</p></div>
<div class="m2"><p>آن سراپا سوز و آن خونین ایاق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کهنهٔ کم خندهٔ اندک سخن</p></div>
<div class="m2"><p>چشم او بینندهٔ جان در بدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رند و ملا و حکیم و خرقه پوش</p></div>
<div class="m2"><p>در عمل چون زاهدان سخت کوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فطرتش بیگانه ذوق وصال</p></div>
<div class="m2"><p>زهد او ترک جمال لایزال۔</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا گسستن از جمال آسان نبود</p></div>
<div class="m2"><p>کار پیش افکند از ترک سجود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اندکی در واردات او نگر</p></div>
<div class="m2"><p>مشکلات او ثبات او نگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غرق اندر رزم خیر و شر هنوز</p></div>
<div class="m2"><p>صد پیمبر دیده و کافر هنوز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جانم اندر تن ز سوز او تپید</p></div>
<div class="m2"><p>بر لبش آهی غم آلودی رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت و چشم نیم وا بر من گشود</p></div>
<div class="m2"><p>«در عمل جز ما که بر خوردار بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنچنان بر کار ها پیچیده ام</p></div>
<div class="m2"><p>فرصت آدینه را کم دیده ام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نی مرا افرشته ئی نی چاکری</p></div>
<div class="m2"><p>وحی من بی منت پیغمبری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نی حدیث و نی کتاب آورده ام</p></div>
<div class="m2"><p>جان شیرین از فقیهان برده ام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رشتهٔ دین چون فقیهان کس نرشت</p></div>
<div class="m2"><p>کعبه را کردند آخر خشت خشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کیش ما را اینچنین تأسیس نیست</p></div>
<div class="m2"><p>فرقه اندر مذهب ابلیس نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در گذشتم از سجود ای بیخبر</p></div>
<div class="m2"><p>ساز کردم ارغنون خیر و شر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از وجود حق مرا منکر مگیر</p></div>
<div class="m2"><p>دیده بر باطن گشا ظاهر مگیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر بگویم نیست این از ابلهی است</p></div>
<div class="m2"><p>زانکه بعد از دید نتوان گفت نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من «بلی» در پردهٔ «لا» گفته ام</p></div>
<div class="m2"><p>گفتهٔ من خوشتر از نا گفته ام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا نصیب از درد آدم داشتم</p></div>
<div class="m2"><p>قهر یار از بهر او نگذاشتم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شعله ها از کشتزار من ارمن دمید</p></div>
<div class="m2"><p>او ز مجبوری به مختاری رسید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زشتی خود را نمودم آشکار</p></div>
<div class="m2"><p>با تو دادم ذوق ترک و اختیار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو نجاتی ده مرا از نار من</p></div>
<div class="m2"><p>وا کن ای آدم گره از کار من</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ایکه اندر بند من افتاده ئی</p></div>
<div class="m2"><p>رخصت عصیان به شیطان داده ئی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در جهان با همت مردانه زی</p></div>
<div class="m2"><p>غمگسار من ز من بیگانه زی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بی نیاز از نیش و نوش من گذر</p></div>
<div class="m2"><p>تا نگردد نامه ام تاریک تر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در جهان صیاد با نخچیرهاست</p></div>
<div class="m2"><p>تا تو نخچیری به کیشم تیر هاست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صاحب پرواز را افتاد نیست</p></div>
<div class="m2"><p>صید اگر زیرک شود صیاد نیست»</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفتمش «بگذر ز آئین فراق</p></div>
<div class="m2"><p>ابغض الاشیاء عندی الطلاق»</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفت «ساز زندگی ، سوز فراق</p></div>
<div class="m2"><p>ای خوشا سر مستی روز فراق</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر لبم از وصل می ناید سخن</p></div>
<div class="m2"><p>وصل اگر خواهم نه او ماند نه من»</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حرف وصل او را ز خود بیگانه کرد</p></div>
<div class="m2"><p>تازه شد اندر دل او سوز و درد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اندکی غلطید اندر دود خویش</p></div>
<div class="m2"><p>باز گم کردید اندر دود خویش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ناله ئی زان دود پیچان شد بلند</p></div>
<div class="m2"><p>ای خنک جانی که گردد درد مند</p></div></div>