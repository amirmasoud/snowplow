---
title: >-
    بخش ۳۷ - تذکیر نبیهٔ مریخ
---
# بخش ۳۷ - تذکیر نبیهٔ مریخ

<div class="b" id="bn1"><div class="m1"><p>ای زنان ای مادران ای خواهران</p></div>
<div class="m2"><p>زیستن تا کی مثال دلبران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبری اندر جهان مظلومی است</p></div>
<div class="m2"><p>دلبری محکومی و محرومی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دو گیسو شانه گردانیم ما</p></div>
<div class="m2"><p>مرد را نخچیر خود دانیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد صیادی به نخچیری کند</p></div>
<div class="m2"><p>گرد تو گردد که زنجیری کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود گدازیهای او مکر و فریب</p></div>
<div class="m2"><p>درد و داغ و آرزو مکر و فریب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه آن کافر حرم سازد ترا</p></div>
<div class="m2"><p>مبتلای درد و غم سازد ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همبر او بودن آزار حیات</p></div>
<div class="m2"><p>وصل او زهر و فراق او نبات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مار پیچان از خم و پیچش گریز</p></div>
<div class="m2"><p>زهرهایش را بخون خود مریز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از امومت زرد روی مادران</p></div>
<div class="m2"><p>ای خنک آزادی بی شوهران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وحی یزدان پی به پی آید مرا</p></div>
<div class="m2"><p>لذت ایمان بیفزاید مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آمد آن وقتی که از اعجاز فن</p></div>
<div class="m2"><p>می توان دیدن جنین اندر بدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حاصلی برداری از کشت حیات</p></div>
<div class="m2"><p>هر چه خواهی از بنین و از بنات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نباشد بر مراد ما جنین،</p></div>
<div class="m2"><p>بی محابا کشتن او عین دین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در پس این عصر اعصار دگر</p></div>
<div class="m2"><p>آشکارا گردد اسرار دگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پرورش گیرد جنین نوع دگر</p></div>
<div class="m2"><p>بی شب ارحام دریابد سحر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا بمیرد آن سراپا اهرمن</p></div>
<div class="m2"><p>همچو حیوانات ایام کهن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لاله ها بی داغ و با دامان پاک</p></div>
<div class="m2"><p>بی نیاز از شبنمی خیزد ز خاک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خود بخود بیرون فتد اسرار زیست</p></div>
<div class="m2"><p>نغمه بی مضراب بخشد تار زیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنچه از نیسان فرو ریزد مگیر</p></div>
<div class="m2"><p>ای صدف در زیر دریا تشنه میر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خیز و با فطرت بیا اندر ستیز</p></div>
<div class="m2"><p>تا ز پیکار تو حر گردد کنیز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رستن از ربط دو تن توحید زن</p></div>
<div class="m2"><p>حافظ خود باش و بر مردان متن</p></div></div>
<div class="b2" id="bn22"><p>رومی</p></div>
<div class="b" id="bn23"><div class="m1"><p>مذهب عصر نو آئینی نگر</p></div>
<div class="m2"><p>حاصل تهذیب لادینی نگر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زندگی را شرع و آئین است عشق</p></div>
<div class="m2"><p>اصل تهذیب است دین ، دین است عشق</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ظاهر او سوزناک و آتشین</p></div>
<div class="m2"><p>باطن او نور رب العالمین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از تب و تاب درونش علم و فن</p></div>
<div class="m2"><p>از جنون ذوفنونش علم و فن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دین نگردد پخته بی آداب عشق</p></div>
<div class="m2"><p>دین بگیر از صحبت ارباب عشق</p></div></div>