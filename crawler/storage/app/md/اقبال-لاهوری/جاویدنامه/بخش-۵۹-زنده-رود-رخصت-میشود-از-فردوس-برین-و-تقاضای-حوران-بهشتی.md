---
title: >-
    بخش ۵۹ - زنده رود رخصت میشود از فردوس برین و تقاضای حوران بهشتی
---
# بخش ۵۹ - زنده رود رخصت میشود از فردوس برین و تقاضای حوران بهشتی

<div class="b" id="bn1"><div class="m1"><p>شیشهٔ صبر و سکونم ریز ریز</p></div>
<div class="m2"><p>پیر رومی گفت در گوشم که خیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن حدیث شوق و آن جذب و یقین</p></div>
<div class="m2"><p>آه آن ایوان و آن کاخ برین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با دل پر خون رسیدم بر درش</p></div>
<div class="m2"><p>یک هجوم حور دیدم بر درش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر لب شان زنده رود ای زنده رود</p></div>
<div class="m2"><p>زنده رود ای صاحب سوز و سرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شور و غوغا از یسار و از یمین</p></div>
<div class="m2"><p>یکدو دم با ما نشین ، با ما نشین</p></div></div>
<div class="b2" id="bn6"><p>زنده رود</p></div>
<div class="b" id="bn7"><div class="m1"><p>راهرو کو داند اسرار سفر</p></div>
<div class="m2"><p>ترسد از منزل ز رهزن بیشتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق در هجر و وصال آسوده نیست</p></div>
<div class="m2"><p>بی جمال لایزال آسوده نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابتدا پیش بتان افتادگی</p></div>
<div class="m2"><p>انتها از دلبران آزادگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق بی پروا و هر دم در رحیل</p></div>
<div class="m2"><p>در مکان و لامکان ابن السبیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کیش ما مانند موج تیز گام</p></div>
<div class="m2"><p>اختیار جاده و ترک مقام</p></div></div>
<div class="b2" id="bn12"><p>حوران بهشت</p></div>
<div class="b" id="bn13"><div class="m1"><p>شیوه ها داری مثال روزگار</p></div>
<div class="m2"><p>یک نوای خوش دریغ از ما مدار</p></div></div>