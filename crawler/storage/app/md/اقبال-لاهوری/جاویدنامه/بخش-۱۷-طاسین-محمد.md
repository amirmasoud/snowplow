---
title: >-
    بخش ۱۷ - طاسین محمد
---
# بخش ۱۷ - طاسین محمد

<div class="b2" id="bn1"><p>نوحهٔ روح ابوجهل در حرم کعبه </p></div>
<div class="b" id="bn2"><div class="m1"><p>سینهٔ ما از محمد داغ داغ</p></div>
<div class="m2"><p>از دم او کعبه را گل شد چراغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هلاک قیصر و کسری سرود</p></div>
<div class="m2"><p>نوجوانان را ز دست ما ربود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساحر و اندر کلامش ساحری است</p></div>
<div class="m2"><p>این دو حرف لااله خود کافری است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بساط دین آبا در نورد</p></div>
<div class="m2"><p>با خداوندان ما کرد آنچه کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پاش پاش از ضربتش لات و منات</p></div>
<div class="m2"><p>انتقام از وی بگیر ای کائنات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل به غایب بست و از حاضر گسست</p></div>
<div class="m2"><p>نقش حاضر را فسون او شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده بر غایب فرو بستن خطاست</p></div>
<div class="m2"><p>آنچه اندر دیده می ناید کجاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش غایب سجده بردن کوری است</p></div>
<div class="m2"><p>دین نو کور است و کوری دوری است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خم شدن پیش خدای بی جهات</p></div>
<div class="m2"><p>بنده را ذوقی نبخشد این صلوت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مذهب او قاطع ملک و نسب</p></div>
<div class="m2"><p>از قریش و منکر از فضل عرب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در نگاه او یکی بالا و پست</p></div>
<div class="m2"><p>با غلام خویش بر یک خوان نشست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قدر احرار عرب نشناخته</p></div>
<div class="m2"><p>با کلفتان حبش در ساخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>احمران با اسودان آمیختند</p></div>
<div class="m2"><p>آبروی دودمانی ریختند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این مساوات این مواخات اعجمی است</p></div>
<div class="m2"><p>خوب میدانم که سلمان مزدکی است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ابن عبدالله فریبش خورده است</p></div>
<div class="m2"><p>رستخیزی بر عرب آورده است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عترت هاشم ز خود مهجور گشت</p></div>
<div class="m2"><p>از دو رکعت چشم شان بی نور گشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اعجمی را اصل عدنانی کجاست</p></div>
<div class="m2"><p>گنگ را گفتار سحبانی کجاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چشم خاصان عرب گردیده کور</p></div>
<div class="m2"><p>بر نیائی ای زهیر از خاک گور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای تو ما را اندرین صحرا دلیل</p></div>
<div class="m2"><p>بشکن افسون نوای جبرئیل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باز گوی ای سنگ اسود باز گوی</p></div>
<div class="m2"><p>آنچه دیدیم از محمد باز گوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای هبل ، ای بنده را پوزش پذیر</p></div>
<div class="m2"><p>خانهٔ خود را ز بی کیشان بگیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گلهٔ شان را به گرگان کن سبیل</p></div>
<div class="m2"><p>تلخ کن خرمایشان را بر نخیل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صرصری ده با هوای بادیه</p></div>
<div class="m2"><p>«انهم اعجاز نخل خاویه»</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای منات ای لات ازین منزل مرو</p></div>
<div class="m2"><p>گر ز منزل میروی از دل مرو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای ترا اندر دو چشم ما وثاق</p></div>
<div class="m2"><p>مهلتی ، ان کنت ازمعت الفراق»</p></div></div>