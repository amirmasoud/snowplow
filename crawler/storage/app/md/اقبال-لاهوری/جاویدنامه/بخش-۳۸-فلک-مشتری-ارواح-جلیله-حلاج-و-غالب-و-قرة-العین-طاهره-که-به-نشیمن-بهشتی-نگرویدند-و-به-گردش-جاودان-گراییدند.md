---
title: >-
    بخش ۳۸ - فلک مشتری  ارواح جلیلهٔ حلاج و غالب و قرة العین طاهره که به نشیمن بهشتی نگرویدند و به گردش جاودان گراییدند
---
# بخش ۳۸ - فلک مشتری  ارواح جلیلهٔ حلاج و غالب و قرة العین طاهره که به نشیمن بهشتی نگرویدند و به گردش جاودان گراییدند

<div class="b" id="bn1"><div class="m1"><p>من فدای این دل دیوانه‌ای</p></div>
<div class="m2"><p>هر زمان بخشد دگر ویرانه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بگیرم منزلی گوید که خیز</p></div>
<div class="m2"><p>مرد خود رس بحر را داند قفیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانکه آیات خدا لا انتهاست</p></div>
<div class="m2"><p>ای مسافر جاده را پایان کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار حکمت دیدن و فرسودن است</p></div>
<div class="m2"><p>کار عرفان دیدن و افزودن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن بسنجد در ترازوی هنر</p></div>
<div class="m2"><p>این بسنجد در ترازوی نظر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن بدست آورد آب و خاک را</p></div>
<div class="m2"><p>این بدست آورد جان پاک را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن نگه را بر تجلی می زند</p></div>
<div class="m2"><p>این تجلی را بخود گم می کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در تلاش جلوه های پی به پی</p></div>
<div class="m2"><p>طی کنم افلاک و می نالم چو نی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این همه از فیض مردی پاک زاد</p></div>
<div class="m2"><p>آنکه سوز او بجان من فتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاروان این دو بینای وجود</p></div>
<div class="m2"><p>بر کنار مشتری آمد فرود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن جهان آن خاکدانی ناتمام</p></div>
<div class="m2"><p>در طواف او قمر ها تیز گام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خالی از می شیشه تاکش هنوز</p></div>
<div class="m2"><p>آرزو نارسته از خاکش هنوز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیم شب از تاب ماهان نیم روز</p></div>
<div class="m2"><p>نی برودت در هوای او نه سوز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من چو سوی آسمان کردم نظر</p></div>
<div class="m2"><p>کوکبش دیدم بخود نزدیک تر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هیبت نظاره از هوشم ربود</p></div>
<div class="m2"><p>شد دگرگون نزد و دور و دیر و زود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیش خود دیدم سه روح پاکباز</p></div>
<div class="m2"><p>آتش اندر سینه شان گیتی گداز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در برشان حله های لاله گون</p></div>
<div class="m2"><p>چهره ها رخشنده از سوز درون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در تب و تابی ز هنگام الست</p></div>
<div class="m2"><p>از شراب نغمه های خویش مست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت رومی «این قدر از خود مرو</p></div>
<div class="m2"><p>از دم آتش نوایان زنده شو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شوق بی پروا ندیدستی ، نگر</p></div>
<div class="m2"><p>زور این صهبا ندیدستی ، نگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غالب و حلاج و خاتون عجم</p></div>
<div class="m2"><p>شورها افکنده در جان حرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این نواها روح را بخشد ثبات</p></div>
<div class="m2"><p>گرمی او از درون کائنات»</p></div></div>