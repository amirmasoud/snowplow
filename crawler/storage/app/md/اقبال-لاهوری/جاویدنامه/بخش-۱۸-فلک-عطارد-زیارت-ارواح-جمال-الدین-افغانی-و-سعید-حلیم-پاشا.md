---
title: >-
    بخش ۱۸ - فلک عطارد  زیارت ارواح جمال الدین افغانی و سعید حلیم پاشا
---
# بخش ۱۸ - فلک عطارد  زیارت ارواح جمال الدین افغانی و سعید حلیم پاشا

<div class="b" id="bn1"><div class="m1"><p>مشت خاکی کار خود را برده پیش</p></div>
<div class="m2"><p>در تماشای تجلی های خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا من افتادم بدام هست و بود</p></div>
<div class="m2"><p>یا بدام من اسیر آمد وجود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندرین نیلی تتق چاک از من است</p></div>
<div class="m2"><p>من ز افلاکم که افلاک از من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا ضمیرم را فلک در بر گرفت</p></div>
<div class="m2"><p>یا ضمیر من فلک را در گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندرونست این که بیرون است چیست؟</p></div>
<div class="m2"><p>آنچه می بیند نگه چون است چیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر زنم بر آسمانی دیگری</p></div>
<div class="m2"><p>پیش خود بینم جهانی دیگری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عالمی با کوه و دشت و بحر و بر</p></div>
<div class="m2"><p>عالمی از خاک ما دیرینه تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالمی از ابرکی بالیده ئی</p></div>
<div class="m2"><p>دستبرد آدمی نادیده ئی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقشها نابسته بر لوح وجود</p></div>
<div class="m2"><p>خرده گیر فطرت آنجا کس نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من به رومی گفتم این صحرا خوش است</p></div>
<div class="m2"><p>در کهستان شورش دریا خوش است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من نیابم از حیات اینجا نشان</p></div>
<div class="m2"><p>از کجا می آید آواز اذان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت رومی این مقام اولیاست</p></div>
<div class="m2"><p>آشنا این خاکدان با خاک ماست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بوالبشر چون رخت از فردوس بست</p></div>
<div class="m2"><p>یک دو روزی اندرین عالم نشست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این فضاها سوز آهش دیده است</p></div>
<div class="m2"><p>ناله های صبحگاهش دیده است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زائران این مقام ارجمند</p></div>
<div class="m2"><p>پاک مردان از مقامات بلند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پاک مردان چون فضیل و بوسعید</p></div>
<div class="m2"><p>عارفان مثل جنید و با یزید </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خیز تا ما را نماز آید بدست</p></div>
<div class="m2"><p>یک دو دم سوز و گداز آید بدست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رفتم و دیدم دو مرد اندر قیام</p></div>
<div class="m2"><p>مقتدی تاتار و افغانی امام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پیر رومی هر زمان اندر حضور</p></div>
<div class="m2"><p>طلعتش بر تافت از ذوق و سرور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت «مشرق زین دو کس بهتر نزاد</p></div>
<div class="m2"><p>ناخن شان عقده های ما گشود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سید السادات مولانا جمال</p></div>
<div class="m2"><p>زنده از گفتار او سنگ و سفال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ترک سالار آن حلیم دردمند</p></div>
<div class="m2"><p>فکر او مثل مقام او بلند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با چنین مردان دو رکعت طاعت است</p></div>
<div class="m2"><p>ورنه آن کاری که مزدش جنت است»</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قرأت آن پیر مرد سخت کوش</p></div>
<div class="m2"><p>سوره والنجم و آن دشت خموش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قرأتی کز وی خلیل آید به وجد</p></div>
<div class="m2"><p>روح پاک جبرئیل آید به وجد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دل ازو در سینه گردد ناصبور</p></div>
<div class="m2"><p>شور الا الله خیزد از قبور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اضطراب شعله بخشد دود را</p></div>
<div class="m2"><p>سوز و مستی میدهد داؤد را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آشکارا هر غیاب از قرأتش</p></div>
<div class="m2"><p>بی حجاب ام الکتاب از قرأتش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من ز جا بر خاستم بعد از نماز</p></div>
<div class="m2"><p>دست او بوسیدم از راه نیاز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفت رومی «ذرهٔ گردون نورد</p></div>
<div class="m2"><p>در دل او یک جهان سوز و درد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چشم جز بر خویشتن نگشاده ئی</p></div>
<div class="m2"><p>دل بکس ناداده ئی آزاده ئی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تند سیر اندر فراخای وجود</p></div>
<div class="m2"><p>من ز شوخی گویم او را زنده رود»</p></div></div>
<div class="b2" id="bn33"><p>افغانی</p></div>
<div class="b" id="bn34"><div class="m1"><p>زنده رود از خاکدان ما بگوی</p></div>
<div class="m2"><p>از زمین و آسمان ما بگوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خاکی و چون قدسیان روشن بصر</p></div>
<div class="m2"><p>از مسلمانان بده ما را خبر</p></div></div>
<div class="b2" id="bn36"><p>زنده رود</p></div>
<div class="b" id="bn37"><div class="m1"><p>در ضمیر ملت گیتی شکن</p></div>
<div class="m2"><p>دیده ام آویزش دین و وطن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>روح در تن مرده از ضعف یقین</p></div>
<div class="m2"><p>ناامید از قوت دین مبین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ترک و ایران و عرب مست فرنگ</p></div>
<div class="m2"><p>هر کسی را در گلو شست فرنگ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مشرق از سلطانی مغرب خراب</p></div>
<div class="m2"><p>اشتراک از دین و ملت برده تاب</p></div></div>
<div class="b2" id="bn41"><p>افغانی</p></div>