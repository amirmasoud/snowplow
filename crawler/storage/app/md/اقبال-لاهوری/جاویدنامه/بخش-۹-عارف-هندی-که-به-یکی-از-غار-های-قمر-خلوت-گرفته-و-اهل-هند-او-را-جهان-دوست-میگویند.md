---
title: >-
    بخش ۹ - عارف هندی که به یکی از غار های قمر خلوت گرفته ، و اهل هند او را «جهان دوست» میگویند
---
# بخش ۹ - عارف هندی که به یکی از غار های قمر خلوت گرفته ، و اهل هند او را «جهان دوست» میگویند

<div class="b" id="bn1"><div class="m1"><p>من چوکوران دست بر دوش رفیق</p></div>
<div class="m2"><p>پا نهادم اندر آن غار عمیق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه را از ظلمتش دل داغ داغ</p></div>
<div class="m2"><p>اندرو خورشید محتاج چراغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وهم و شک بر من شبیخون ریختند</p></div>
<div class="m2"><p>عقل و هوشم را بدار آویختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه رفتم رهزنان اندر کمین</p></div>
<div class="m2"><p>دل تهی از لذت صدق و یقین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نگه را جلوه ها شد بی حجاب</p></div>
<div class="m2"><p>صبح روشن بی طلوع آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وادی هر سنگ او زنار بند</p></div>
<div class="m2"><p>دیو سار از نخلهای سر بلند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سرشت آب و خاک است این مقام</p></div>
<div class="m2"><p>یا خیالم نقش بندد در منام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هوای او چو می ذوق و سرور</p></div>
<div class="m2"><p>سایه از تقبیل خاکش عین نور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی زمینش را سپهر لاجورد</p></div>
<div class="m2"><p>نی کنارش از شفقها سرخ و زرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور در بند ظلام آنجا نبود</p></div>
<div class="m2"><p>دود گرد صبح و شام آنجا نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زیر نخلی عارف هندی نژاد</p></div>
<div class="m2"><p>دیده ها از سرمه اش روشن سواد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>موی بر سر بسته و عریان بدن</p></div>
<div class="m2"><p>گرد او ماری سفیدی حلقه زن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آدمی از آب و گل بالاتری</p></div>
<div class="m2"><p>عالم از دیر خیالش پیکری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وقت او را گردش ایام نی</p></div>
<div class="m2"><p>کار او با چرخ نیلی فام نی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت با رومی که همراه تو کیست؟</p></div>
<div class="m2"><p>در نگاهش آرزوی زندگیست</p></div></div>
<div class="b2" id="bn16"><p>رومی</p></div>
<div class="b" id="bn17"><div class="m1"><p>مردی اندر جستجو آواره ئی</p></div>
<div class="m2"><p>ثابتی با فطرت سیاره ئی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پخته تر کارش ز خامی های او</p></div>
<div class="m2"><p>من شهید ناتمامی های او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شیشهٔ خود را به گردون بسته طاق</p></div>
<div class="m2"><p>فکرش از جبریل میخواهد صداق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون عقاب افتد به صید ماه و مهر</p></div>
<div class="m2"><p>گرم رو اندر طواف نه سپهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حرف با اهل زمین رندانه گفت</p></div>
<div class="m2"><p>حور و جنت را بت و بتخانه گفت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شعله ها در موج دودش دیده ام</p></div>
<div class="m2"><p>کبریا اندر سجودش دیده ام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر زمان از شوق مینالد چو نال</p></div>
<div class="m2"><p>می کشد او را فراق و هم وصال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من ندانم چیست در آب و گلش</p></div>
<div class="m2"><p>من ندانم از مقام و منزلش</p></div></div>
<div class="b2" id="bn25"><p>جهان دوست</p></div>
<div class="b" id="bn26"><div class="m1"><p>عالم از رنگست و بی رنگی است حق</p></div>
<div class="m2"><p>چیست عالم ، چیست آدم ، چیست حق؟</p></div></div>
<div class="b2" id="bn27"><p>رومی</p></div>
<div class="b" id="bn28"><div class="m1"><p>آدمی شمشیر و حق شمشیر زن</p></div>
<div class="m2"><p>عالم این شمشیر را سنگ فسن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شرق حق را دید و عالم را ندید</p></div>
<div class="m2"><p>غرب در عالم خزید از حق رمید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چشم بر حق باز کردن بندگی است</p></div>
<div class="m2"><p>خویش را بی پرده دیدن زندگی است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بنده چون از زندگی گیرد برات</p></div>
<div class="m2"><p>هم خدا آن بنده را گوید صلوت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر که از تقدیر خویش آگاه نیست</p></div>
<div class="m2"><p>خاک او با سوز جان همراه نیست</p></div></div>
<div class="b2" id="bn33"><p>جهان دوست</p></div>
<div class="b" id="bn34"><div class="m1"><p>بر وجود و بر عدم پیچیده است</p></div>
<div class="m2"><p>مشرق این اسرار را کم دیده است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کار ما افلاکیان جز دید نیست</p></div>
<div class="m2"><p>جانم از فردای او نومید نیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دوش دیدم بر فراز قشمرود</p></div>
<div class="m2"><p>ز آسمان افرشته ئی آمد فرود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از نگاهش ذوق دیداری چکید</p></div>
<div class="m2"><p>جز بسوی خاکدان ما ندید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گفتمش از محرمان رازی مپوش</p></div>
<div class="m2"><p>تو چه بینی اندر آن خاک خموش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از جمال زهره ئی بگداختی</p></div>
<div class="m2"><p>دل به چاه بابلی انداختی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گفت «هنگام طلوع خاور است</p></div>
<div class="m2"><p>آفتاب تازه او را در بر است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>لعل ها از سنگ ره آید برون</p></div>
<div class="m2"><p>یوسفان او ز چه آید برون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رستخیزی در کنارش دیده ام</p></div>
<div class="m2"><p>لرزه اندر کوهسارش دیده ام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رخت بندد از مقام آزری</p></div>
<div class="m2"><p>تا شود خوگر ز ترک بت گری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای خوش آن قومی که جان او تپید</p></div>
<div class="m2"><p>از گل خود خویش را باز آفرید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عرشیان را صبح عید آن ساعتی</p></div>
<div class="m2"><p>چون شود بیدار چشم ملتی»</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پیر هندی اندکی دم در کشید</p></div>
<div class="m2"><p>باز در من دید و بی تابانه دید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گفت مرگ عقل؟ گفتم ترک فکر</p></div>
<div class="m2"><p>گفت مرگ قلب؟ گفتم ترک ذکر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گفت تن؟ گفتم که زاد از گرد ره</p></div>
<div class="m2"><p>گفت جان؟ گفتم که رمز لااله</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گفت آدم؟ گفتم از اسرار اوست</p></div>
<div class="m2"><p>گفت عالم؟ گفتم او خود روبروست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گفت این علم و هنر؟ گفتم که پوست</p></div>
<div class="m2"><p>گفت حجت چیست؟ گفتم روی دوست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گفت دین عامیان؟ گفتم شنید</p></div>
<div class="m2"><p>گفت دین عارفان؟ گفتم که دید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از کلامم لذت جانش فزود</p></div>
<div class="m2"><p>نکته های دل نشین بر من گشود</p></div></div>