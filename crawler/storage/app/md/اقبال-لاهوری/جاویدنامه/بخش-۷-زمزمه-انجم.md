---
title: >-
    بخش ۷ - زمزمهٔ انجم
---
# بخش ۷ - زمزمهٔ انجم

<div class="b" id="bn1"><div class="m1"><p>عقل تو حاصل حیات ، عشق تو سر کائنات</p></div>
<div class="m2"><p>پیکر خاک خوش بیا ، این سوی عالم جهات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهره و ماه و مشتری از تو رقیب یکدگر</p></div>
<div class="m2"><p>از پی یک نگاه تو کشمکش تجلیات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره دوست جلوه هاست تازه بتازه نوبنو</p></div>
<div class="m2"><p>صاحب شوق و آرزو دل ندهد بکلیات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدق و صفاست زندگی نشوونماست زندگی</p></div>
<div class="m2"><p>«تا ابد از ازل بتاز ملک خداست زندگی» </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوق غزل سرای را رخصت های و هو بده</p></div>
<div class="m2"><p>باز به رند و محتسب باده سبو سبو بده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شام و عراق و هند و پارس خوبه نبات کرده اند</p></div>
<div class="m2"><p>خوبه نبات کرده را تلخی آرزو بده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به یم بلند موج معرکه ئی بنا کند</p></div>
<div class="m2"><p>لذت سیل تند رو با دل آب جو بده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرد فقیر آتش است میری و قیصری خس است</p></div>
<div class="m2"><p>فال و فر ملوک را حرف برهنه ئی بس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دبدبهٔ قلندری ، طنطنهٔ سکندری</p></div>
<div class="m2"><p>آن همه جذبهٔ کلیم این همه سحر و سامری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن به نگاه می کشد این به سپاه می کشد</p></div>
<div class="m2"><p>آن همه صلح و آشتی این همه جنگ و داوری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر دو جهان گشاستند هر دو دوام خواستند</p></div>
<div class="m2"><p>این به دلیل قاهری ، آن به دلیل دلبری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ضرب قلندری بیار سد سکندری شکن</p></div>
<div class="m2"><p>رسم کلیم تازه کن رونق ساحری شکن</p></div></div>