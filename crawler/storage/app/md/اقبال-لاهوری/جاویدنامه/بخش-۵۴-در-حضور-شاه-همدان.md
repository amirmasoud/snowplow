---
title: >-
    بخش ۵۴ - در حضور شاه همدان
---
# بخش ۵۴ - در حضور شاه همدان

<div class="b2" id="bn1"><p>زنده رود</p></div>
<div class="b" id="bn2"><div class="m1"><p>از تو خواهم سر یزدان را کلید</p></div>
<div class="m2"><p>طاعت از ما جست و شیطان آفرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زشت و ناخوش را چنان آراستن</p></div>
<div class="m2"><p>در عمل از ما نکوئی خواستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو پرسم این فسون سازی که چه</p></div>
<div class="m2"><p>با قمار بدنشین بازی که چه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشت خاک و این سپهر گرد گرد</p></div>
<div class="m2"><p>خود بگو می زیبدش کاری که کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار ما ، افکار ما ، آزار ما</p></div>
<div class="m2"><p>دست با دندان گزیدن کار ما</p></div></div>
<div class="b2" id="bn7"><p>شاه همدان</p></div>
<div class="b" id="bn8"><div class="m1"><p>بنده ئی کز خویشتن دارد خبر</p></div>
<div class="m2"><p>آفریند منفعت را از ضرر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بزم با دیو است آدم را وبال</p></div>
<div class="m2"><p>رزم با دیو است آدم را جمال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خویش را بر اهرمن باید زدن</p></div>
<div class="m2"><p>تو همه تیغ آن همه سنگ فسن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تیز تر شو تا فتد ضرب تو سخت</p></div>
<div class="m2"><p>ورنه باشی در دو گیتی تیره بخت</p></div></div>
<div class="b2" id="bn12"><p>زنده رود</p></div>
<div class="b" id="bn13"><div class="m1"><p>زیر گردون آدم آدم را خورد</p></div>
<div class="m2"><p>ملتی بر ملتی دیگر چرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان ز اهل خطه سوزد چون سپند</p></div>
<div class="m2"><p>خیزد از دل ناله های دردمند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زیرک و دراک و خوش گل ملتی است</p></div>
<div class="m2"><p>در جهان تر دستی او آیتی است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ساغرش غلطنده اندر خون اوست</p></div>
<div class="m2"><p>در نی من ناله از مضمون اوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از خودی تا بی نصیب افتاده است</p></div>
<div class="m2"><p>در دیار خود غریب افتاده است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دستمزد او بدست دیگران</p></div>
<div class="m2"><p>ماهی رودش به شست دیکران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کاروانها سوی منزل گام گام</p></div>
<div class="m2"><p>کار او نا خوب و بی اندام و خام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از غلامی جذبه های او بمرد</p></div>
<div class="m2"><p>آتشی اندر رگ تاکش فسرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا نپنداری که بود است اینچین</p></div>
<div class="m2"><p>جبهه را همواره سود است اینچنین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در زمانی صف شکن هم بوده است</p></div>
<div class="m2"><p>چیره و جانباز و پر دم بوده است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کوههای خنگ سار او نگر</p></div>
<div class="m2"><p>آتشین دست چنار او نگر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در بهاران لعل میریزد ز سنگ</p></div>
<div class="m2"><p>خیزد از خاکش یکی طوفان رنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لکه های ابر در کوه و دمن</p></div>
<div class="m2"><p>پنبه پران از کمان پنبه زن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کوه و دریا و غروب آفتاب</p></div>
<div class="m2"><p>من خدارا دیدم آنجا بی حجاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با نسیم آواره بودم در نشاط</p></div>
<div class="m2"><p>«بشنو از نی» می سرودم در نشاط</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرغکی می گفت اندر شاخسار</p></div>
<div class="m2"><p>با پشیزی می نیرزد این بهار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لاله رست و نرگس شهلا دمید</p></div>
<div class="m2"><p>باد نو روزی گریبانش درید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عمرها بالید ازین کوه و کمر</p></div>
<div class="m2"><p>نستر از نور قمر پاکیزه تر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عمر ها گل رخت بر بست و گشاد</p></div>
<div class="m2"><p>خاک ما دیگر شهاب الدین نزاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نالهٔ پر سوز آن مرغ سحر</p></div>
<div class="m2"><p>داد جانم را تب و تاب دگر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا یکی دیوانه دیدم در خروش</p></div>
<div class="m2"><p>آنکه برد از من متاع صبر و هوش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>«بگذر ز ما و نالهٔ مستانه ئی مجوی</p></div>
<div class="m2"><p>بگذر ز شاخ گل که طلسمی است رنگ و بوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفتی که شبنم از ورق لاله می چکد</p></div>
<div class="m2"><p>غافلی دلی است اینکه بگرید کنار جوی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>این مشت پر کجا و سرود اینچنین کجا</p></div>
<div class="m2"><p>روح غنی است ماتمی مرگ آرزوی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>باد صبا اگر به جنیوا گذر کنی،</p></div>
<div class="m2"><p>حرفی ز ما به مجلس اقوام باز گوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دهقان و کشت و جوی و خیابان فروختند</p></div>
<div class="m2"><p>قومی فروختند و چه ارزان فروختند»</p></div></div>
<div class="b2" id="bn39"><p>شاه همدان</p></div>
<div class="b" id="bn40"><div class="m1"><p>با تو گویم رمز باریک ای پسر</p></div>
<div class="m2"><p>تن همه خاک است و جان والا گهر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جسم را از بهر جان باید گداخت</p></div>
<div class="m2"><p>پاک را از خاک می باید شناخت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر ببری پارهٔ تن را ز تن</p></div>
<div class="m2"><p>رفت از دست تو آن لخت بدن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>لیکن آن جانی که گردد جلوه مست</p></div>
<div class="m2"><p>گر ز دست او را دهی آید بدست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جوهرش با هیچ شی مانند نیست</p></div>
<div class="m2"><p>هست اندر بند و اندر بند نیست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر نگهداری بمیرد در بدن</p></div>
<div class="m2"><p>ور بیفشانی ، فروغ انجمن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چیست جان جلوه مست ای مرد راد</p></div>
<div class="m2"><p>چیست جان دادن ز دست ایمرد راد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چیست جان دادن بحق پرداختن</p></div>
<div class="m2"><p>کوه را با سوز جان بگداختن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جلوه مستی خویش را دریافتن</p></div>
<div class="m2"><p>در شبان چون کوکبی بر تافتن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خویش را نایافتن نابودن است</p></div>
<div class="m2"><p>یافتن خود را بخود بخشودن است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر که خود را دید و غیر از خود ندید</p></div>
<div class="m2"><p>رخت از زندان خود بیرون کشید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جلوه بد مستی که بیند خویش را</p></div>
<div class="m2"><p>خوشتر از نوشینه و داند نیش را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در نگاهش جان چو باد ارزان شود</p></div>
<div class="m2"><p>پیش او زندان او لرزان شود</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تیشهٔ او خاره را بر می درد</p></div>
<div class="m2"><p>تا نصیب خود ز گیتی می برد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا ز جان بگذشت جانش جان اوست</p></div>
<div class="m2"><p>ورنه جانش یکدو دم مهمان اوست</p></div></div>
<div class="b2" id="bn55"><p>زنده رود</p></div>
<div class="b" id="bn56"><div class="m1"><p>گفته ئی از حکمت زشت و نکوی</p></div>
<div class="m2"><p>پیر دانا نکتهٔ دیگر بگوی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرشد معنی نگاهان بوده ئی</p></div>
<div class="m2"><p>محرم اسرار شاهان بوده ئی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ما فقیر و حکمران خواهد خراج</p></div>
<div class="m2"><p>چیست اصل اعتبار تخت و تاج</p></div></div>
<div class="b2" id="bn59"><p>شاه همدان</p></div>
<div class="b" id="bn60"><div class="m1"><p>اصل شاهی چیست اندر شرق و غرب</p></div>
<div class="m2"><p>یا رضای امتان یا حرب و ضرب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>فاش گویم با تو ای والا مقام</p></div>
<div class="m2"><p>باج را جز با دو کس دادن حرام</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>یا «اولی الامری» که «منکم» شأن اوست</p></div>
<div class="m2"><p>آیهٔ حق حجت و برهان اوست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یا جوانمردی چو صرصر تند خیز</p></div>
<div class="m2"><p>شهر گیر و خویش باز اندر ستیز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>روز کین کشور گشا از قاهری</p></div>
<div class="m2"><p>روز صلح از شیوه های دلبری</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>می توان ایران و هندوستان خرید</p></div>
<div class="m2"><p>پادشاهی را ز کس نتوان خرید</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>جام جم را ای جوان باهنر</p></div>
<div class="m2"><p>کس نگیرد از دکان شیشه گر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ور بگیرد مال او جز شیشه نیست</p></div>
<div class="m2"><p>شیشه را غیر از شکستن پیشه نیست</p></div></div>
<div class="b2" id="bn68"><p>غنی</p></div>
<div class="b" id="bn69"><div class="m1"><p>هند را این ذوق آزادی که داد</p></div>
<div class="m2"><p>صید را سودای صیادی که داد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>آن برهمن زادگان زنده دل</p></div>
<div class="m2"><p>لالهٔ احمر ز روی شان خجل</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تیزبین و پخته کار و سخت کوش</p></div>
<div class="m2"><p>از نگاهشان فرنگ اندر خروش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>اصلشان از خاک دامنگیر ماست</p></div>
<div class="m2"><p>مطلع این اختران کشمیر ماست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>خاک ما را بی شرر دانی اگر</p></div>
<div class="m2"><p>بر درون خود یکی بگشا نظر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>اینهمه سوزی که داری از کجاست</p></div>
<div class="m2"><p>این دم باد بهاری از کجاست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>این همان باد است کز تأثیر او</p></div>
<div class="m2"><p>کوهسار ما بگیرد رنگ و بو</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هیچ میدانی که روزی در ولر</p></div>
<div class="m2"><p>موجه ئی می گفت با موج دگر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چند در قلزم به یکدیگر زنیم</p></div>
<div class="m2"><p>خیز تا یک دم بساحل سر زنیم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>زادهٔ ما یعنی آن جوی کهن</p></div>
<div class="m2"><p>شور او در وادی و کوه و دمن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>هر زمان بر سنگ ره خود را زند</p></div>
<div class="m2"><p>تا بنای کوه را بر می کند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>آن جوان کو شهر و دشت و در گرفت</p></div>
<div class="m2"><p>پرورش از شیر صد مادر گرفت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سطوت او خاکیان را محشری است</p></div>
<div class="m2"><p>این همه از ماست، نی از دیگری است</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>زیستن اندر حد ساحل خطاست</p></div>
<div class="m2"><p>ساحل ما سنگی اندر راه ماست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>با کران در ساختن مرگ دوام</p></div>
<div class="m2"><p>گرچه اندر بحر غلتی صبح و شام</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>زندگی جولان میان کوه و دشت</p></div>
<div class="m2"><p>ای خنک موجی که از ساحل گذشت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ایکه خواندی خط سیمای حیات</p></div>
<div class="m2"><p>ای به خاور داده غوغای حیات</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ای ترا آهی که می سوزد جگر</p></div>
<div class="m2"><p>تو ازو بیتاب و ما بیتاب تر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ای ز تو مرغ چمن را های و هو</p></div>
<div class="m2"><p>سبزه از اشک تو می گیرد وضو</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ایکه از طبع تو کشت گل دمید</p></div>
<div class="m2"><p>ای ز امید تو جانها پر امید</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>کاروانها را صدای تو درا</p></div>
<div class="m2"><p>تو ز اهل خطه نومیدی چرا</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>دل میان سینهٔ شان مرده نیست</p></div>
<div class="m2"><p>اخگر شان زیر یخ افسرده نیست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>باش تا بینی که بی آواز صور</p></div>
<div class="m2"><p>ملتی بر خیزد از خاک قبور</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>غم مخور ای بندهٔ صاحب نظر</p></div>
<div class="m2"><p>بر کش آن آهی که سوزد خشک و تر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>شهر ها زیر سپهر لاجورد</p></div>
<div class="m2"><p>سوخت از سوز دل درویش مرد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>سلطنت نازکتر آمد از حباب</p></div>
<div class="m2"><p>از دمی او را توان کردن خراب</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>از نوا تشکیل تقدیر امم</p></div>
<div class="m2"><p>از نوا تخریب و تعمیر امم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>نشتر تو گرچه در دلها خلید</p></div>
<div class="m2"><p>مر ترا چونانکه هستی کس ندید</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>پردهٔ تو از نوای شاعری است</p></div>
<div class="m2"><p>آنچه گوئی ماورای شاعری است</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>تازه آشوبی فکن اندر بهشت</p></div>
<div class="m2"><p>یک نوا مستانه زن اندر بهشت</p></div></div>
<div class="b2" id="bn99"><p>زنده رود</p></div>
<div class="b" id="bn100"><div class="m1"><p>با نشئه درویشی در ساز و دمادم زن</p></div>
<div class="m2"><p>چون پخته شوی خود را بر سلطنت جم زن</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>گفتند جهان ما آیا بتو می سازد</p></div>
<div class="m2"><p>گفتم که نمی سازد گفتند که برهم زن</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>در میکده ها دیدم شایسته حریفی نیست</p></div>
<div class="m2"><p>با رستم دستان زن با مغچه ها کم زن</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ای لاله صحرائی تنها نتوانی سوخت</p></div>
<div class="m2"><p>این داغ جگر تابی بر سینه آدم زن</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>تو سوز درون او تو گرمی خون او</p></div>
<div class="m2"><p>باور نکنی چاکی در پیکر عالم زن</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>عقل است چراغ تو در راهگذاری نه</p></div>
<div class="m2"><p>عشق است ایاغ تو با بندهٔ محرم زن</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>لخت دل پر خونی از دیده فرو ریزم</p></div>
<div class="m2"><p>لعلی ز بدخشانم بردار و بخاتم زن</p></div></div>