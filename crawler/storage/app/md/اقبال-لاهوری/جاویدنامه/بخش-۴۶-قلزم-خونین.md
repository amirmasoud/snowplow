---
title: >-
    بخش ۴۶ - قلزم خونین
---
# بخش ۴۶ - قلزم خونین

<div class="b" id="bn1"><div class="m1"><p>آنچه دیدم می نگنجد در بیان</p></div>
<div class="m2"><p>تن ز سهمش بیخبر گردد ز جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چه دیدم قلزمی دیدم ز خون</p></div>
<div class="m2"><p>قلزمی ، طوفان برون ، طوفان درون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هوا ماران چو در قلزم نهنگ</p></div>
<div class="m2"><p>کفچه شبگون بال و پر سیماب رنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موجها درنده مانند پلنگ</p></div>
<div class="m2"><p>از نهیبش مرده بر ساحل نهنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحر ساحل را امان یک دم نداد</p></div>
<div class="m2"><p>هر زمان که پاره ئی در خون فتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موج خون با موج خون اندر ستیز</p></div>
<div class="m2"><p>درمیانش زورقی در افت و خیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر آن زورق دو مرد زرد روی</p></div>
<div class="m2"><p>زرد رو عریان بدن آشفته موی</p></div></div>