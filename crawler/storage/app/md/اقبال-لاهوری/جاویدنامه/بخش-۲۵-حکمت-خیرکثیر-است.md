---
title: >-
    بخش ۲۵ - حکمت خیرکثیر است
---
# بخش ۲۵ - حکمت خیرکثیر است

<div class="b" id="bn1"><div class="m1"><p>گفت حکمت را خدا خیر کثیر</p></div>
<div class="m2"><p>هر کجا این خیر را بینی بگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علم حرف و صوت را شهپر دهد</p></div>
<div class="m2"><p>پاکی گوهر به نا گوهر دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علم را بر اوج افلاک است ره</p></div>
<div class="m2"><p>تا ز چشم مهر بر کندد نگه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسخهٔ او نسخهٔ تفسیر کل</p></div>
<div class="m2"><p>بستهٔ تدبیر او تقدیر کل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دشت را گوید حبابی ده ، دهد</p></div>
<div class="m2"><p>بحر را گوید سرابی ده ، دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم او بر واردات کائنات</p></div>
<div class="m2"><p>تا ببیند محکمات کائنات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل اگر بندد به حق پیغمبری است</p></div>
<div class="m2"><p>ور ز حق بیگانه گردد کافری است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علم را بی سوز دل خوانی شر است</p></div>
<div class="m2"><p>نور او تاریکی بحر و بر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عالمی از غاز او کور و کبود</p></div>
<div class="m2"><p>فرودینش برگ ریز هست و بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بحر و دشت و کوهسار و باغ و راغ</p></div>
<div class="m2"><p>از بم طیارهٔ او داغ داغ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سینه افرنگ را ناری ازوست</p></div>
<div class="m2"><p>لذت شبخون و یلغاری ازوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیر واژونی دهد ایام را</p></div>
<div class="m2"><p>می برد سرمایهٔ اقوام را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قوتش ابلیس را یاری شود</p></div>
<div class="m2"><p>نور ، نار از صحبت ناری شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کشتن ابلیس کاری مشکل است</p></div>
<div class="m2"><p>زانکه او گم اندر اعماق دل است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خوشتر آن باشد مسلمانش کنی</p></div>
<div class="m2"><p>کشتهٔ شمشیر قرآنش کنی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از جلال بی جمالی الامان</p></div>
<div class="m2"><p>از فراق بی وصالی الامان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>علم بی عشق است از طاغوتیان</p></div>
<div class="m2"><p>علم با عشق است از لاهوتیان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بی محبت علم و حکمت مرده ئی</p></div>
<div class="m2"><p>عقل تیری بر هدف ناخورده ئی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کور را بیننده از دیدار کن</p></div>
<div class="m2"><p>بولهب را حیدر کرار کن</p></div></div>
<div class="b2" id="bn20"><p>زنده رود</p></div>
<div class="b" id="bn21"><div class="m1"><p>محکماتش وانمودی از کتاب</p></div>
<div class="m2"><p>هست آن عالم هنوز اندر حجاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پرده را از چهره نگشاید چرا</p></div>
<div class="m2"><p>از ضمیر ما برون ناید چرا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیش ما یک عالم فرسوده ایست</p></div>
<div class="m2"><p>ملت اندر خاک او آسوده ایست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رفت سوز سینهٔ تاتار و کرد</p></div>
<div class="m2"><p>یا مسلمان مرد یا قرآن بمرد</p></div></div>
<div class="b2" id="bn25"><p>سعید حلیم پاشا</p></div>
<div class="b" id="bn26"><div class="m1"><p>دین حق از کافری رسوا تر است</p></div>
<div class="m2"><p>زانکه ملا مؤمن کافر گر است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شبنم ما در نگاه ما یم است</p></div>
<div class="m2"><p>از نگاه او یم ما شبنم است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از شگرفیهای آن قرآن فروش</p></div>
<div class="m2"><p>دیده ام روح الامین را در خروش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زانسوی گردون دلش بیگانه ئی</p></div>
<div class="m2"><p>نزد او ام الکتاب افسانه ئی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بی نصیب از حکمت دین نبی</p></div>
<div class="m2"><p>آسمانش تیره از بی کوکبی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کم نگاه و کور ذوق و هرزه گرد</p></div>
<div class="m2"><p>ملت از قال و اقولش فرد فرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مکتب و ملا و اسرار کتاب</p></div>
<div class="m2"><p>کور مادر زاد و نور آفتاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دین کافر فکر و تدبیر جهاد</p></div>
<div class="m2"><p>دین ملا فی سبیل الله فساد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرد حق جان جهان چار سوی</p></div>
<div class="m2"><p>آن بخلوت رفته را از من بگوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای ز افکار تو مؤمن را حیات</p></div>
<div class="m2"><p>از نفسهای تو ملت را ثبات</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>حفظ قرآن عظیم آئین تست</p></div>
<div class="m2"><p>حرف حق را فاش گفتن دین تست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو کلیمی چند باشی سرنگون</p></div>
<div class="m2"><p>دست خویش از آستین آور برون</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سر گذشت ملت بیضا بگوی</p></div>
<div class="m2"><p>با غزال از وسعت صحرا بگوی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فطرت تو مستنیر از مصطفی است</p></div>
<div class="m2"><p>باز گو آخر مقام ما کجاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرد حق از کس نگیرد رنگ و بو</p></div>
<div class="m2"><p>مرد حق از حق پذیرد رنگ و بو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر زمان اندر تنش جانی دگر</p></div>
<div class="m2"><p>هر زمان او را چو حق شانی دگر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رازها با مرد مؤمن باز گوی</p></div>
<div class="m2"><p>شرح رمز «کل یوم» باز گوی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جز حرم منزل ندارد کاروان</p></div>
<div class="m2"><p>غیر حق در دل ندارد کاروان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>من نمی گویم که راهش دیگر است</p></div>
<div class="m2"><p>کاروان دیگر نگاهش دیگر است</p></div></div>
<div class="b2" id="bn45"><p>افغانی</p></div>
<div class="b" id="bn46"><div class="m1"><p>از حدیث مصطفی داری نصیب</p></div>
<div class="m2"><p>دین حق اندر جهان آمد غریب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>با تو گویم معنی این حرف بکر</p></div>
<div class="m2"><p>غربت دین نیست فقر اهل ذکر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بهر آن مردی که صاحب جستجوست</p></div>
<div class="m2"><p>غربت دین ندرت آیات اوست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>غربت دین هر زمان نوع دگر</p></div>
<div class="m2"><p>نکته را دریاب اگر داری نظر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دل به آیات مبین دیگر ببند</p></div>
<div class="m2"><p>تا بگیری عصر نو را در کمند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کس نمی داند ز اسرار کتاب</p></div>
<div class="m2"><p>شرقیان هم غربیان در پیچ و تاب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>روسیان نقش نوی انداختند</p></div>
<div class="m2"><p>آب و نان بردند و دین در باختند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>حق ببین حق گوی و غیر از حق مجوی</p></div>
<div class="m2"><p>یک دو حرف از من به آن ملت بگوی</p></div></div>