---
title: >-
    بخش ۱۶ - طاسین مسیح
---
# بخش ۱۶ - طاسین مسیح

<div class="b2" id="bn1"><p>رویای حکیم تولستوی </p></div>
<div class="b" id="bn2"><div class="m1"><p>در میان کوهسار هفت مرگ</p></div>
<div class="m2"><p>وادی بی طایر و بی شاخ و برگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاب مه از دود گرد او چو قیر</p></div>
<div class="m2"><p>آفاب اندر فضایش تشنه میر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رود سیماب ، اندر آن وادی روان</p></div>
<div class="m2"><p>خم به خم مانند جوی کهکشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش او پست و بلند راه هیچ</p></div>
<div class="m2"><p>تند سیر و موج موج و پیچ پیچ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرق در سیماب مردی تا کمر</p></div>
<div class="m2"><p>با هزاران ناله های بی اثر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قسمت او ابر و باد و آب نی</p></div>
<div class="m2"><p>تشنه و آبی به جز سیماب نی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برکران دیدم زنی نازک تنی</p></div>
<div class="m2"><p>چشم او صد کاروان را رهزنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کافری آموز پیران کنشت</p></div>
<div class="m2"><p>از نگاهش زشت خوب و خوب زشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتمش تو کیستی نام تو چیست</p></div>
<div class="m2"><p>این سراپا ناله و فریاد کیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت در چشمم فسون سامری است</p></div>
<div class="m2"><p>نامم افرنگین و کارم ساحری است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناگهان آن جوی سیمین یخ ببست</p></div>
<div class="m2"><p>استخوان آن جوان در تن شکست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بانگ زد ای وای بر تقدیر من</p></div>
<div class="m2"><p>وای بر فریاد بی تأثیر من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت افرنگین «اگر داری نظر</p></div>
<div class="m2"><p>اندگی اعمال خود را هم نگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پور مریم آن چراغ کائنات</p></div>
<div class="m2"><p>نور او اندر جهات و بی جهات</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن فلاطوس آن صلیب آن روی زرد</p></div>
<div class="m2"><p>زیر گردون تو چه کردی او چه کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای بجانت لذت ایمان حرام</p></div>
<div class="m2"><p>ای پرستار بتان سیم خام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قیمت روح القدس نشناختی</p></div>
<div class="m2"><p>تن خریدی نقد جان در باختی»</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طعنهٔ آن نازنین جلوه مست</p></div>
<div class="m2"><p>آن جوان را نشتر اندر دل شکست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت «ای گندم نمای جو فروش</p></div>
<div class="m2"><p>از تو شیخ و برهمن ملت فروش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عقل و دین از کافریهای تو خوار</p></div>
<div class="m2"><p>عشق از سوداگریهای تو خوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مهر تو آزار و آزار نهان</p></div>
<div class="m2"><p>کین تو مرگ است و مرگ ناگهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صحبتی با آب و گل ورزیده ئی</p></div>
<div class="m2"><p>بنده را از پیش حق دزدیده ئی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حکمتی کو عقدهٔ اشیا گشاد</p></div>
<div class="m2"><p>با تو غیر از فکر چنگیزی نداد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>داند آن مردی که صاحب جوهر است</p></div>
<div class="m2"><p>جرم تو از جرم من سنگین تر است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از دم او رفته جان آمد بتن</p></div>
<div class="m2"><p>از تو جان را دخمه میگردد بدن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آنچه ما کردیم با ناسوت او</p></div>
<div class="m2"><p>ملت او کرد با لاهوت او</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرگ تو اهل جهان را زندگی است</p></div>
<div class="m2"><p>باش تا بینی که انجام تو چیست»</p></div></div>