---
title: >-
    بخش ۳۱ - فرو رفتن بدریای زهره و دیدن ارواح فرعون و کشنر را
---
# بخش ۳۱ - فرو رفتن بدریای زهره و دیدن ارواح فرعون و کشنر را

<div class="b" id="bn1"><div class="m1"><p>پیر روم آن صاحب «ذکر جمیل» </p></div>
<div class="m2"><p>ضرب او را سطوت ضرب خلیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این غزل در عالم مستی سرود</p></div>
<div class="m2"><p>هر خدای کهنه آمد در سجود</p></div></div>
<div class="b2" id="bn3"><p>غزل </p></div>
<div class="b" id="bn4"><div class="m1"><p>«باز بر رفته و آینده نظر باید کرد</p></div>
<div class="m2"><p>هله بر خیز که اندیشه دگر باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق بر ناقهٔ ایام کشد محمل خویش</p></div>
<div class="m2"><p>عاشقی راحله از شام و سحر باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر ما گفت جهان بر روشی محکم نیست</p></div>
<div class="m2"><p>از خوش و ناخوش او قطع نظر باید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو اگر ترک جهان کرده سر او داری</p></div>
<div class="m2"><p>پس نخستین ز سر خویش گذر باید کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتمش در دل من لات و منات است بسی</p></div>
<div class="m2"><p>گفت این بتکده را زیر و زبر باید کرد»</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز با من گفت «بر خیز ای پسر</p></div>
<div class="m2"><p>جز بدامانم میاویز ای پسر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن کهستان آن جبال بی کلیم</p></div>
<div class="m2"><p>آنکه از برف است چون انبار سیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در پس او قلزم الماس گون</p></div>
<div class="m2"><p>آشکارا تر درونش از برون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نی بموج و نی به سیل او را خلل</p></div>
<div class="m2"><p>در مزاج او سکون لم یزل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این مقام سر کشان زور مست</p></div>
<div class="m2"><p>منکران غایب و حاضر پرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن یکی از شرق و آن دیگر ز غرب</p></div>
<div class="m2"><p>هر دو با مردان حق در حرب و ضرب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن یکی بر گردنش چوب کلیم</p></div>
<div class="m2"><p>وان دگر از تیغ درویشی دو نیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر دو فرعون این صغیر و آن کبیر</p></div>
<div class="m2"><p>هر دو در آغوش دریا تشنه میر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر کسی با تلخی مرگ آشناست</p></div>
<div class="m2"><p>مرگ جباران ز آیات خداست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درپی من پا بنه از کس مترس</p></div>
<div class="m2"><p>دست در دستم بده از کس مترس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سینهٔ دریا چو موسی بر درم</p></div>
<div class="m2"><p>من ترا اندر ضمیر او برم»</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بحر بر ما سینهٔ خود را گشود</p></div>
<div class="m2"><p>یا هوا بود و چو آبی وانمود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قعر او یک وادی بیرنگ و بو</p></div>
<div class="m2"><p>وادی تاریکی او تو به تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیر رومی سورهٔ طه سرود</p></div>
<div class="m2"><p>زیر دریا ماهتاب آمد فرود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کوههای شسته و عریان و سرد</p></div>
<div class="m2"><p>اندر آن سر گشته و حیران دو مرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سوی رومی یک نظر نگریستند</p></div>
<div class="m2"><p>باز سوی یکدگر نگریستند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت فرعون این سحر این جوی نور</p></div>
<div class="m2"><p>از کجا این صبح و این نور و ظهور</p></div></div>
<div class="b2" id="bn26"><p>رومی</p></div>
<div class="b" id="bn27"><div class="m1"><p>هر چه پنهان است ازو پیداستی</p></div>
<div class="m2"><p>اصل این نور از یدبیضاستی</p></div></div>
<div class="b2" id="bn28"><p>فرعون</p></div>
<div class="b" id="bn29"><div class="m1"><p>آه نقد عقل و دین در باختم</p></div>
<div class="m2"><p>دیدم و این نور را نشناختم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای جهانداران سوی من بنگرید</p></div>
<div class="m2"><p>ای زیانکاران سوی من بنگرید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وای قومی از هوس گردیده کور</p></div>
<div class="m2"><p>می برد لعل و گهر از خاک گور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پیکری کو در عجایب خانه ایست</p></div>
<div class="m2"><p>بر لب خاموش او افسانه ایست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از ملوکیت خبرها می دهد</p></div>
<div class="m2"><p>کور چشمان را نظرها می دهد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چیست تقدیر ملوکیت ، شقاق</p></div>
<div class="m2"><p>محکمی جستن ز تدبیر نفاق</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از بد آموزی زبون تقدیر ملک</p></div>
<div class="m2"><p>باطل و آشفته تر تدبیر ملک</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>باز اگر بینم کلیم الله را</p></div>
<div class="m2"><p>خواهم از وی یک دل آگاه را</p></div></div>
<div class="b2" id="bn37"><p>رومی</p></div>
<div class="b" id="bn38"><div class="m1"><p>حاکمی بی نور جان خام است خام</p></div>
<div class="m2"><p>بی یدبیضا ملوکیت حرام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حاکمی از ضعف محکومان قویست</p></div>
<div class="m2"><p>بیخش از حرمان محرومان قویست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تاج از باج است و از تسلیم باج</p></div>
<div class="m2"><p>مرد اگر سنگ است میگردد زجاج</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فوج و زندان و سلاسل رهزنی است</p></div>
<div class="m2"><p>اوست حاکم کز چنین سامان غنی است</p></div></div>
<div class="b2" id="bn42"><p>ذوالخرطوم</p></div>
<div class="b" id="bn43"><div class="m1"><p>مقصد قوم فرنگ آمد بلند</p></div>
<div class="m2"><p>از پی لعل و گهر گوری نکند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سر گذشت مصر و فرعون و کلیم</p></div>
<div class="m2"><p>می توان دیدن ز آثار قدیم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>علم و حکمت کشف اسرار است و بس</p></div>
<div class="m2"><p>حکمت بی جستجو خوار است و بس</p></div></div>
<div class="b2" id="bn46"><p>فرعون</p></div>
<div class="b" id="bn47"><div class="m1"><p>قبر ما را علم و حکمت بر گشود</p></div>
<div class="m2"><p>لیکن اندر تربت مهدی چه بود</p></div></div>