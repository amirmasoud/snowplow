---
title: >-
    بخش ۶۱ - حضور
---
# بخش ۶۱ - حضور

<div class="b" id="bn1"><div class="m1"><p>گرچه جنت از تجلی های اوست</p></div>
<div class="m2"><p>جان نیاساید به جز دیدار دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما ز اصل خویشتن در پرده ایم</p></div>
<div class="m2"><p>طائریم و آشیان گم کرده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علم اگر کج فطرت و بد گوهر است</p></div>
<div class="m2"><p>پیش چشم ما حجاب اکبر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علم را مقصود اگر باشد نظر</p></div>
<div class="m2"><p>می شود هم جاده و هم راهبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می نهد پیش تو از قشر وجود</p></div>
<div class="m2"><p>تا تو پرسی چیست راز این نمود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جاده را هموار سازد اینچین</p></div>
<div class="m2"><p>شوق را بیدار سازد اینچنین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد و داغ و تاب و تب بخشد ترا</p></div>
<div class="m2"><p>گریه های نیم شب بخشد ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علم تفسیر جهان رنگ و بو</p></div>
<div class="m2"><p>دیده و دل پرورش گیرد ازو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر مقام جذب و شوق آرد ترا</p></div>
<div class="m2"><p>باز چون جبریل بگذارد ترا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق کس را کی بخلوت می برد</p></div>
<div class="m2"><p>او ز چشم خویش غیرت می برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اول او هم رفیق و هم طریق</p></div>
<div class="m2"><p>آخر او راه رفتن بی رفیق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در گذشتم زان همه حور و قصور</p></div>
<div class="m2"><p>زورق جان باختم در بحر نور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غرق بودم در تماشای جمال</p></div>
<div class="m2"><p>هر زمان در انقلاب و لایزال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گم شدم اندر ضمیر کائنات</p></div>
<div class="m2"><p>چون رباب آمد بچشم من حیات</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه هر تارش رباب دیگری</p></div>
<div class="m2"><p>هر نوا از دیگری خونین تری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ما همه یک دودمان نار و نور</p></div>
<div class="m2"><p>آدم و مهر و مه و جبریل و حور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیش جان آئینه ئی آویختند</p></div>
<div class="m2"><p>حیرتی را با یقین آمیختند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صبح امروزی که نورش ظاهر است</p></div>
<div class="m2"><p>در حضورش دوش و فردا حاضر است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حق هویدا با همه اسرار خویش</p></div>
<div class="m2"><p>با نگاه من کند دیدار خویش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دیدنش افزودن بی کاستن</p></div>
<div class="m2"><p>دیدنش از قبر تن برخاستن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عبد و مولا در کمین یکدگر</p></div>
<div class="m2"><p>هر دو بیتاب اند از ذوق نظر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زندگی هر جا که باشد جستجوست</p></div>
<div class="m2"><p>حل نشد این نکته من صیدم که اوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عشق جان را لذت دیدار داد</p></div>
<div class="m2"><p>با زبانم جرأت گفتار داد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای دو عالم از تو با نور و نظر</p></div>
<div class="m2"><p>اندکی آن خاکدانی را نگر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بندهٔ آزاد را ناسازگار</p></div>
<div class="m2"><p>بر دمد از سنبل او نیش خار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>غالبان غرق اند در عیش و طرب</p></div>
<div class="m2"><p>کار مغلوبان شمار روز و شب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از ملوکیت جهان تو خراب</p></div>
<div class="m2"><p>تیره شب در آستین آفتاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دانش افرنگیان غارتگری</p></div>
<div class="m2"><p>دیر ها خیبر شد از بی حیدری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنکه گوید لااله بیچاره ایست</p></div>
<div class="m2"><p>فکرش از بی مرکزی آواره ایست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چار مرگ اندر پی این دیر میر</p></div>
<div class="m2"><p>سود خوار و والی و ملا و پیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اینچنین عالم کجا شایان تست</p></div>
<div class="m2"><p>آب و گل داغی که بر دامان تست</p></div></div>
<div class="b2" id="bn32"><p>ندای جمال</p></div>
<div class="b" id="bn33"><div class="m1"><p>کلک حق از نقشهای خوب و زشت</p></div>
<div class="m2"><p>هر چه ما را سازگار آمد نوشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چیست بودن دانی ای مرد نجیب؟</p></div>
<div class="m2"><p>از جمال ذات حق بردن نصیب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آفریدن جستجوی دلبری</p></div>
<div class="m2"><p>وانمودن خویش را بر دیگری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اینهمه هنگامه های هست و بود</p></div>
<div class="m2"><p>بی جمال ما نیاید در وجود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زندگی هم فانی و هم باقی است</p></div>
<div class="m2"><p>این همه خلاقی و مشتاقی است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زنده ئی؟ مشتاق شو خلاق شو</p></div>
<div class="m2"><p>همچو ما گیرندهٔ آفاق شو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در شکن آنرا که ناید سازگار</p></div>
<div class="m2"><p>از ضمیر خود دگر عالم بیار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بندهٔ آزاد را آید گران</p></div>
<div class="m2"><p>زیستن اندر جهان دیگران</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر که او را قوت تخلیق نیست</p></div>
<div class="m2"><p>پیش ما جز کافر و زندیق نیست</p></div></div>
<div class="b2" id="bn42"><p>ق</p></div>
<div class="b" id="bn43"><div class="m1"><p>از جمال ما نصیب خود نبرد</p></div>
<div class="m2"><p>از نخیل زندگانی بر نخورد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرد حق برنده چون شمشیر باش</p></div>
<div class="m2"><p>خود جهان خویش را تقدیر باش</p></div></div>
<div class="b2" id="bn45"><p>زنده رود</p></div>
<div class="b" id="bn46"><div class="m1"><p>چیست آئین جهان رنگ و بو</p></div>
<div class="m2"><p>جز که آب رفته می ناید بجو</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زندگانی را سر تکرار نیست</p></div>
<div class="m2"><p>فطرت او خوگر تکرار نیست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زیر گردون رجعت او را نارواست</p></div>
<div class="m2"><p>چون ز پا افتاد قومی برنخاست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ملتی چون مرد ، کم خیزد ز قبر</p></div>
<div class="m2"><p>چارهٔ او چیست غیر از قبر و صبر</p></div></div>
<div class="b2" id="bn50"><p>ندای جمال</p></div>
<div class="b" id="bn51"><div class="m1"><p>زندگانی نیست تکرار نفس</p></div>
<div class="m2"><p>اصل او از حی و قیوم است و بس</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>قرب جان با آنکه گفت «انی قریب»</p></div>
<div class="m2"><p>از حیات جاودان بردن نصیب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فرد از توحید لاهوتی شود</p></div>
<div class="m2"><p>ملت از توحید جبروتی شود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بایزید و شبلی و بوذر ازوست</p></div>
<div class="m2"><p>امتان را طغرل و سنجر ازوست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بی تجلی نیست آڈم را ثبات</p></div>
<div class="m2"><p>جلوهٔ ما فرد و ملت را حیات</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هر دو از توحید می گیرد کمال</p></div>
<div class="m2"><p>زندگی این را جلال ، آنرا جمال</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>این سلیمانی است آن سلمانی است</p></div>
<div class="m2"><p>آن سراپا فقر و این سلطانی است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آن یکی را بیند ، این گردد یکی</p></div>
<div class="m2"><p>در جهان با آن نشین با این بزی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چیست ملت ایکه گوئی لااله</p></div>
<div class="m2"><p>با هزاران چشم بودن یک نگه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اهل حق را حجت و دعوی یکیست</p></div>
<div class="m2"><p>خیمه های ما جدا دلها یکی است</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ذره ها از یک نگاه آفتاب</p></div>
<div class="m2"><p>یک نگه شو تا شود حق بی حجاب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>یک نگاهی را بچشم کم مبین</p></div>
<div class="m2"><p>از تجلی های توحید است این</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ملتی چون می شود توحید مست</p></div>
<div class="m2"><p>قوت و جبروت می آید بدست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>روح ملت را وجود از انجمن</p></div>
<div class="m2"><p>روح ملت نیست محتاج بدن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا وجودش را نمود از صحبت است</p></div>
<div class="m2"><p>مرد چون شیرازهٔ صحبت شکست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>مرده ئی از یک نگاهی زنده شو</p></div>
<div class="m2"><p>بگذر از بی مرکزی پاینده شو</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>وحدت افکار و کردار آفرین</p></div>
<div class="m2"><p>تا شوی اندر جهان صاحب نگین</p></div></div>
<div class="b2" id="bn68"><p>زنده رود</p></div>
<div class="b" id="bn69"><div class="m1"><p>من کیم تو کیستی عالم کجاست</p></div>
<div class="m2"><p>در میان ما و تو دوری چراست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>من چرا در بند تقدیرم بگوی</p></div>
<div class="m2"><p>تو نمیری من چرا میرم بگوی</p></div></div>
<div class="b2" id="bn71"><p>ندای جمال</p></div>
<div class="b" id="bn72"><div class="m1"><p>بوده ئی اندر جهان چار سو</p></div>
<div class="m2"><p>هر که گنجد اندرو میرد درو</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>زندگی خواهی خودی را پیش کن</p></div>
<div class="m2"><p>چار سو را غرق اندر خویش کن</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>باز بینی من کیم تو کیستی</p></div>
<div class="m2"><p>در جهان چون مردی و چون زیستی</p></div></div>
<div class="b2" id="bn75"><p>زنده رود</p></div>
<div class="b" id="bn76"><div class="m1"><p>پوزش این مرد نادان در پذیر</p></div>
<div class="m2"><p>پرده را از چهرهٔ تقدیر گیر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>انقلاب روس و آلمان دیده ام</p></div>
<div class="m2"><p>شور در جان مسلمان دیده ام</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>دیده ام تدبیر های غرب و شرق</p></div>
<div class="m2"><p>وانما تقدیر های غرب و شرق</p></div></div>
<div class="b2" id="bn79"><p>افتادن تجلی جلال</p></div>
<div class="b" id="bn80"><div class="m1"><p>ناگهان دیدم جهان خویش را</p></div>
<div class="m2"><p>آن زمین و آسمان خویش را</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>غرق در نور شفق گون دیدمش</p></div>
<div class="m2"><p>سرخ مانند طبر خون دیدمش</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>زان تجلی ها که در جانم شکست</p></div>
<div class="m2"><p>چون کلیم الله فتادم جلوه مست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نور او هر پردگی را وانمود</p></div>
<div class="m2"><p>تاب گفتار از زبان من ربود</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>از ضمیر عالم بی چند و چون</p></div>
<div class="m2"><p>یک نوای سوزناک آمد برون</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>«بگذر از خاور و افسونی افرنگ مشو</p></div>
<div class="m2"><p>که نیرزد بجوی اینهمه دیرینه و نو</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>آن نگینی که تو با اهرمنان باخته ئی</p></div>
<div class="m2"><p>هم به جبریل امینی نتوان کرد گرو</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>زندگی انجمن آرا و نگهدار خود است</p></div>
<div class="m2"><p>ای که در قافله ئی ، بی همه شو با همه رو</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>تو فروزنده تر از مهر منیر آمده ئی</p></div>
<div class="m2"><p>آنچنان زی که بهر ذره رسانی پرتو</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چون پرکاه که در رهگذر باد افتاد</p></div>
<div class="m2"><p>رفت اسکندر و دارا و قباد و خسرو</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>از تنک جامی تو میکده رسوا گردید</p></div>
<div class="m2"><p>شیشه ئی گیر و حکیمانه بیاشام و برو»</p></div></div>