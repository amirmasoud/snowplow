---
title: >-
    بخش ۴۱ - نوای طاهره
---
# بخش ۴۱ - نوای طاهره

<div class="b" id="bn1"><div class="m1"><p>گر به تو افتدم نظر چهره به چهره رو به رو</p></div>
<div class="m2"><p>شرح دهم غم تو را نکته به نکته مو به مو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پی دیدن رخت همچو صبا فتاده‌ام</p></div>
<div class="m2"><p>خانه به خانه، در به در، کوچه به کوچه، کو به کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌رود از فراق تو خون دل از دو دیده‌ام</p></div>
<div class="m2"><p>دجله به دجله، یم به یم، چشمه به چشمه، جو به جو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهر تو را دل حزین بافته بر قماش جان</p></div>
<div class="m2"><p>رشته به رشته، نخ به نخ، تار به تار، پو به پو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دل خویش طاهره، گشت و ندید جز تو را</p></div>
<div class="m2"><p>صفحه به صفحه، لا به لا، پرده به پرده، تو به تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوز و ساز عاشقان دردمند</p></div>
<div class="m2"><p>شورهای تازه در جانم فکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشکلات کهنه سر بیرون زدند</p></div>
<div class="m2"><p>باز بر اندیشه‌ام شبخون زدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قلزم فکرم سراپا اضطراب</p></div>
<div class="m2"><p>ساحلش از زور طوفانی خراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت رومی «وقت را از کف مده</p></div>
<div class="m2"><p>ای که میخواهی گشوده هر گره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند در افکار خود باشی اسیر</p></div>
<div class="m2"><p>این قیامت را برون ریز از ضمیر»</p></div></div>