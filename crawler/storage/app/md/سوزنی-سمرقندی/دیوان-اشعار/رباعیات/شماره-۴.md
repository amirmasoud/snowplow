---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>جود کف تست هر که نانی دارد</p></div>
<div class="m2"><p>در خدمت تست هر که جانی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر اسب خطا کرد بر او عیب مگیر</p></div>
<div class="m2"><p>یک اسب چه طاقت جهانی دارد</p></div></div>