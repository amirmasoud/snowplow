---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>گفم که غم عشق تو میمون کندم</p></div>
<div class="m2"><p>کی دانستم که دیده پرخون کندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایجان جهان من از تو کی برگردم</p></div>
<div class="m2"><p>دور از تو مگر اجل شبیخون کندم</p></div></div>