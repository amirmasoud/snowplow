---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>صدرا باد آنمحشرت نامه سپید</p></div>
<div class="m2"><p>تا حشر مبادات سر خامه سپید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افتد که ز بهر من کنی خامه سیاه</p></div>
<div class="m2"><p>تو خامه سیه کنی و من جامه سپید</p></div></div>