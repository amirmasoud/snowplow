---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>بر جان چو گشاده کرده ای دست ستم</p></div>
<div class="m2"><p>تهدید مکن مل مرا بیش بغم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که من تو عشق تو باشیم بهم</p></div>
<div class="m2"><p>من خود صنما ترا ندارم محرم</p></div></div>