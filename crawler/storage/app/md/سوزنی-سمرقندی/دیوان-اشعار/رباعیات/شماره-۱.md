---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>یک شهر همه حدیث آنروی نکوست</p></div>
<div class="m2"><p>دلهای همه جهانیان بسته اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما می کوشیم و دیگران میکوشند</p></div>
<div class="m2"><p>تا دست کرا دهد کرا خواهد دوست</p></div></div>