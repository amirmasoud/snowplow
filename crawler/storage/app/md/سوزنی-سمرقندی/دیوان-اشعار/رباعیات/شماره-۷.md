---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>باقی است در آن لب مزه شیر هنوز</p></div>
<div class="m2"><p>منسوخ نشد زان خط چون قیر هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط را یله کن که از کمان ابروی تو</p></div>
<div class="m2"><p>چشم ز چپ و راست میزند تیر هنوز</p></div></div>