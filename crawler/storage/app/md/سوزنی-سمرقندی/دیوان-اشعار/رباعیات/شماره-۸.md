---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>تا عشق گل رخ تو در دل دارم</p></div>
<div class="m2"><p>چون گل ز غم تو پای در گل دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا زیر خم زلف تو منزل دارم</p></div>
<div class="m2"><p>چون زلف تو کار خویش مشکل دارم</p></div></div>