---
title: >-
    شمارهٔ ۱۴۸ - در مدح علی بن احمد
---
# شمارهٔ ۱۴۸ - در مدح علی بن احمد

<div class="b" id="bn1"><div class="m1"><p>ای از کمال قدر تو تیری در آسمان</p></div>
<div class="m2"><p>وز ذهن تو خجل شده تیر اندر آسمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست از کمال حلم تو اندر زمین نصیب</p></div>
<div class="m2"><p>چون از کمال قدر تو تیر اندر آسمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر آسمان ز حشمت تو داشتی سپر</p></div>
<div class="m2"><p>نمرود کی کشیدی تیر اندر آسمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مهتری پدیدی چون آفتاب و ماه</p></div>
<div class="m2"><p>در روز روشن و شب تیر اندر آسمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صدر سپهر فخری و فرزند فخر دین</p></div>
<div class="m2"><p>آن زمین چو بدر منیر اندر آسمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همنام ابن عم پیمبر علی که بود</p></div>
<div class="m2"><p>مداح او سروش کبیر اندر آسمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای صدر و سروری که نهد بخت مرترا</p></div>
<div class="m2"><p>از قدر و جایگاه سریر اندر آسمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو در زمین نظیر نداری بمهتری</p></div>
<div class="m2"><p>چونانکه آفتاب نظیر اندر آسمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورشید و ماه نور جمال از تو یافتند</p></div>
<div class="m2"><p>کاین شد چو شاه و آن چو وزیر اندر آسمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیر ار نه در موافقت رای تو کنند</p></div>
<div class="m2"><p>هر هفت گم کنند مسیر اندر آسمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کیوان که از نحوست گردنده رای او</p></div>
<div class="m2"><p>اهل زمین برند نفیر اندر آسمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر مشتریست اختر بدخواه جاه تو</p></div>
<div class="m2"><p>او سوی خود کشد بزفیر اندر آسمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهرام خون خصم تو ریزد بتیغ کین</p></div>
<div class="m2"><p>کان تیغ نیست رنگ پذیر اندر آسمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خورشید چون جمال تو بیند بجنب خود</p></div>
<div class="m2"><p>گردد چو ذره خورا و حقیر اندر آسمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ناهید رود ساز بامید بزم تو</p></div>
<div class="m2"><p>دارد بدست جام عصیر اندر آسمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا تیر و مه تفحص احوال تو کنند</p></div>
<div class="m2"><p>مه شه برید و تیر دبیر اندر آسمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر شب که تو نشاط کنی عندلیب وار</p></div>
<div class="m2"><p>سیارگان زنند صفیر اندر آسمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو باده برگرفته و از دست مطربانت</p></div>
<div class="m2"><p>افتاده ناله بم و زیر اندر آسمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو بر زمین نشسته و از لطف خلق تو</p></div>
<div class="m2"><p>افکنده باد بوی عبیر اندر آسمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر آسمان نیلی گر بنگری بخشم</p></div>
<div class="m2"><p>گردد پدید رنگ زریر اندر آسمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا روز حاسدان تو گردد سیه چو قیر</p></div>
<div class="m2"><p>بی شب رسد سیاهی قیر اندر آسمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جز از زمین جود تو قسمت نکرده اند</p></div>
<div class="m2"><p>نانی بنام هیچ فقیر اندر آسمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حکم ازل چو مائده دشمن ترا</p></div>
<div class="m2"><p>لوزینه . . . است بسیر اندر آسمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کوهان گاو روغن کرد است تا پزند</p></div>
<div class="m2"><p>خوان ترا کرنج بشیر اندر آسمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تف سعیر در نظر هیبت تو است</p></div>
<div class="m2"><p>چونانکه هست تف اثیر اندر آسمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هان تا مگر شعیر براقت شود شد است</p></div>
<div class="m2"><p>امسال برج خوشه شعیر اندر آسمان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خصمت ببرج ماهی اگر بر شود ز چاه</p></div>
<div class="m2"><p>بریان شود ز تف سعیر اندر آسمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مردی حکیم کرد مرا امتحان و گفت</p></div>
<div class="m2"><p>ای کلک تو فکنده صریر اندر آسمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شعری نپیر قافیه گو اندرین ردیف</p></div>
<div class="m2"><p>شعری نهاد مرتبه گیر اندر آسمان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفتم سپاس دارم و گویم چو بنگرم</p></div>
<div class="m2"><p>نیکو بچشم عقل خطیر اندر آسمان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا قافیه حواله دهد از خمیر طبع</p></div>
<div class="m2"><p>بندم بدست نظم فطیر اندر آسمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هست آسمان چو سفره و خورشید همچو قرص</p></div>
<div class="m2"><p>انجم چو کوزومه چو پنیر اندر آسمان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا نیست انجم و مه و خورشید را مدام</p></div>
<div class="m2"><p>از سیر برج برج گزیر اندر آسمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سیرت ببرج لهو طرب باد سال و ماه</p></div>
<div class="m2"><p>ای طلعتت چو مهر منیر اندر آسمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بادا بزیر سایه بخت جوان تو</p></div>
<div class="m2"><p>چندین هزار اختر پیر اندر آسمان</p></div></div>