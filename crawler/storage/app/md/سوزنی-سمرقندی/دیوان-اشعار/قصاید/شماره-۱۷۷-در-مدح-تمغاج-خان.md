---
title: >-
    شمارهٔ ۱۷۷ - در مدح تمغاج خان
---
# شمارهٔ ۱۷۷ - در مدح تمغاج خان

<div class="b" id="bn1"><div class="m1"><p>ای جهانداری که داری بر جهانداران سری</p></div>
<div class="m2"><p>پیش تو جز بندگی دعوی شاهان سرسری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیستی اسکندر و دارا و اندر ذات تو</p></div>
<div class="m2"><p>شوکت دارائی است و حکمت اسکندری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر صف آرائی صف آرائی بمیدان نبرد</p></div>
<div class="m2"><p>در سپاه خویش صفداری عدو را صفدری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در امان تو رود آهو گرازان پیش یوز</p></div>
<div class="m2"><p>سایه شاهین بود آسایش کبک دری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز و شب عدل ترا گویند و انصاف ترا</p></div>
<div class="m2"><p>ای مه هر روزنی وی آفتاب هر دری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوهر شاه قلج تمغاج خان و رکن دین</p></div>
<div class="m2"><p>از برای قمع اعدا چون قلج با گوهری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو محمود قول و فعل و شان و سیرتی</p></div>
<div class="m2"><p>داور مسعود نام و فال و بخت و اختری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از قراخان حسن تا پادشا افراسیاب</p></div>
<div class="m2"><p>وارث گاه نگین و مهر و تیغ و افسری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بود انگشتری را زینت از انگشت تو</p></div>
<div class="m2"><p>آسمان زیر نگین تو بود چون بنگری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده اعدای تو چون چشم افعی برکند</p></div>
<div class="m2"><p>گر زمرد پیش اعدا داری از انگشتری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعد اکبر از فلک ناظر باقبال تو است</p></div>
<div class="m2"><p>تا تو از اقبال خود ناظر بسعد اکبری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرا فریدونی از آبا و از اجداد خویش</p></div>
<div class="m2"><p>لاجرم چون فرا فریدونی افریدون فری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ملک دینرا شاه ملک آرای حق پرور سزد</p></div>
<div class="m2"><p>تو سزی زیرا که ملک آرائی و حق پروری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دادگستر پادشاهی از ره انصاف و داد</p></div>
<div class="m2"><p>نسپری جز در جنان راهی که اینجا بسپری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عالم از انصاف و عدل تست چون فردوس عدن</p></div>
<div class="m2"><p>ما بفردوس اندریم و تو بفردوس اندری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سایه اوراق طوبی سایه چتر تو شد</p></div>
<div class="m2"><p>از قیاس ای سایه یزدان که تو هم درخوری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در پناه عدل تو چون اهل حضرت برخورند</p></div>
<div class="m2"><p>بس دعا گویند تا از ملک و دولت برخوری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ماه شعبان سایه افکند ای ملک بر ماه صوم</p></div>
<div class="m2"><p>مسرعان در پویه انداز مشرقی و خاوری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا بماه صوم میمون و همایون روز عید</p></div>
<div class="m2"><p>از مشبکهای شعبان روز و شب را بشمری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر مه شعبان مه پیغمبر است ای پادشاه</p></div>
<div class="m2"><p>تو مقیم شاهراه سنت پیغمبری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بارگاه این مه تر از اهل خبر آباد کن</p></div>
<div class="m2"><p>کز ملوک و از سلاطین افضلی و اخبری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از مقالاتی که از بهر زه و احسنت عام</p></div>
<div class="m2"><p>شاعران بر خسروان بندند پاکی و بری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مشتری دیدار شاهی هر که دیدار تو دید</p></div>
<div class="m2"><p>خاک پایت را بنور دیدگان شد مشتری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از روان عنصری در خواب تلقین یافتم</p></div>
<div class="m2"><p>کای جهانرا دیدن روی تو فال مشتری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا برین وزن و قوافی آفرین گفتم ترا</p></div>
<div class="m2"><p>آفرینها میفرستم بر روان عنصری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کسوت مدح تو خوش دوزند خیاطان نظم</p></div>
<div class="m2"><p>چون من اندر وقت معنی میکنم سوزنگری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا که برخیزد بحکم داور بیچون و چند</p></div>
<div class="m2"><p>از میان خاک و آب و باد و آتش داوری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دیده آبی با دو تن خاکی بد اندیش ترا</p></div>
<div class="m2"><p>سر ز شمشیر تواش بادی و آتش داوری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا بود محبوب دلها دولت و طول بقا</p></div>
<div class="m2"><p>دولت و طول بقای تو مبادا اسپری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر جهانداران سری جاوید بادا امر تو</p></div>
<div class="m2"><p>ای جهانداری که داری بر جهانداران سری</p></div></div>