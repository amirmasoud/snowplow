---
title: >-
    شمارهٔ ۱۷۴ - در مدح محمد بن یوسف
---
# شمارهٔ ۱۷۴ - در مدح محمد بن یوسف

<div class="b" id="bn1"><div class="m1"><p>سری که خلق جهانرا ویست پشت و پناه</p></div>
<div class="m2"><p>امین دین الله است و سعد ملکت شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستوده فخر خراسان محمد یوسف</p></div>
<div class="m2"><p>که چون محمد و یوسف جمال دارد و جاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر محمد و یوسف ندیده اند بهم</p></div>
<div class="m2"><p>کنون ببینند ار اندرو گنبد نگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محمد از سر انگشت خود اشارت کرد</p></div>
<div class="m2"><p>مه تمام بدو قسم شد بحکم اله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مه صیام بدو قسم کرد او و گذاشت</p></div>
<div class="m2"><p>بقسم روز بصوم و بقسم شب بصلوه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنان مصر بریرند دست اگر دیدند</p></div>
<div class="m2"><p>جمال یوسف یکبار بر گذر ناگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار مرد ستمکاره دست ظلم برند</p></div>
<div class="m2"><p>کنون بعهدش از آن بیم اگر شوند آگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر محمد اندر مقام محمود است</p></div>
<div class="m2"><p>گناه امت خود را ز حق شفاعتخواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنزد خاقان محمود او رعیت را</p></div>
<div class="m2"><p>همی شفاعت خواهد ز گونه گونه گناه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برادرانرا یوسف چو داد گندم و جو</p></div>
<div class="m2"><p>بها گرفت ازیشان بضاعت مز جاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر بضاعت مزجاه پشم و پنبه بود</p></div>
<div class="m2"><p>نبود گندم و جو نیز جز که تخم گیاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میان تخم و گیاه و میان پنبه و پشم</p></div>
<div class="m2"><p>بسی تفاوت نبود چو عقل بیند راه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بجای گندم و جو او همی دهد زر و سیم</p></div>
<div class="m2"><p>بآشنا و به بیگانه فی سبیل الله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدل ستاند ازیشان بجای پنبه و پشم</p></div>
<div class="m2"><p>چه شعرهای رکیک و چه فصلهای تباه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آنچه می بدهد تا بدانچه میگیرد</p></div>
<div class="m2"><p>تفاوتست چو از در کاه تا پر کاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همیشه تا بمه روزه در مجالس علم</p></div>
<div class="m2"><p>بود درود محمد رونده بر افواه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس از درود محمد ثنا و مدحت تو</p></div>
<div class="m2"><p>رونده با بر افواه خلق بی اکراه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همیشه تا که بگویند بر چه سیرت بود</p></div>
<div class="m2"><p>نشست یوسف در صدر پادشاهی و گاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بصدر عزت بادی نشسته چون یوسف</p></div>
<div class="m2"><p>سران ملک بخدمتگرست بر درگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به پیش روی تو از امت محمد پیش</p></div>
<div class="m2"><p>نشسته یوسف رویان با قبا و کلاه</p></div></div>