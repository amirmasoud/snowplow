---
title: >-
    شمارهٔ ۱۵۴ - در مدح افتخار الدین عمر
---
# شمارهٔ ۱۵۴ - در مدح افتخار الدین عمر

<div class="b" id="bn1"><div class="m1"><p>آمد خجسته موسم قربان بمهرگان</p></div>
<div class="m2"><p>خون ریز این بهم شد با برگ ریز آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با مهرگان چو نیک فتاد اتفاق عید</p></div>
<div class="m2"><p>خونریز و برگ ریز پدید آمد از میان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خونریزی ار خلاف بدی پیش ازین چرا</p></div>
<div class="m2"><p>خونریزی از موافقت امد بدین زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد خزان و خون عروسان باغ ریخت</p></div>
<div class="m2"><p>زان تا کند موافقت عید را بیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خونریز این بسازد برگ و هوای بزم</p></div>
<div class="m2"><p>خون ریز آن بسازد برگ و نهاد خوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون ریز این قنینه می را گران کند</p></div>
<div class="m2"><p>خونریز آن ترازوی طاعت کند گران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر میان باغ چو بگذشت نوبهار</p></div>
<div class="m2"><p>کم گشت ارغوان تر و تازه ناگهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ارغوان ز باغ نهان کرد روی خویش</p></div>
<div class="m2"><p>شد برگ هر درخت ز غم همچو زعفران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عید و خزان موافق یکدیگر آمدند</p></div>
<div class="m2"><p>خلقند از موافقت هر دو شادمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عید و خزان ز خلق بسی شادمانترند</p></div>
<div class="m2"><p>از افتخار دین نبی صدر خاندان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرزانه سید اجل مرتضی رضا</p></div>
<div class="m2"><p>کاولاد مرتضی و رضا راست پهلوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سلطان کامران شد بر ملکت هنر</p></div>
<div class="m2"><p>از تربیت نمودن سلطان کامران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرزند شمس دین عمر آن کز جمال خود</p></div>
<div class="m2"><p>چون شمس آسمان فکند نور بر جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آسمان بقدر و بهمت رفیعتر</p></div>
<div class="m2"><p>پاکیزه تر باصل و نسب زآب آسمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از شمس دین چه آید جز افخار دین</p></div>
<div class="m2"><p>لابد که باز باز پراند ز آشیان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از اصل نیک هیچ عجب نیست فرع نیک</p></div>
<div class="m2"><p>باشد پسر چنین چو پدر باشد آنچنان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای صدر خاندان نبوت چو باب خویش</p></div>
<div class="m2"><p>خورشید اقربا شدی و فخر دودمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در تو یقین شد است گمانهای شمس دین</p></div>
<div class="m2"><p>فرزند شمس دینی ازیرا تو بی گمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از جود بی نهایت و از فضل بی قیاس</p></div>
<div class="m2"><p>محبوب هردلی ت و مذکور هر زبان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن باهنر توئی که زهر دانشی دلت</p></div>
<div class="m2"><p>آراسته است همچو بهر نعمتی جنان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر خاطر گشاده و روشن ضمیر تو</p></div>
<div class="m2"><p>پوشیده نیست سری جز سر غیبدان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اندر سر مروت بایسته ای چو چشم</p></div>
<div class="m2"><p>وندر تن فتوت شایسته ای چو جان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از کلک تو بگاه کفایت جهان شود</p></div>
<div class="m2"><p>تیر فلک ز شرم چو تیر تو از کمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ساحر نئی و جد تو ساحر نبود چون</p></div>
<div class="m2"><p>تو ساحری نمائی از کلک و از بنان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا جاودان بیابد سالی و بگذرد</p></div>
<div class="m2"><p>آید دو بار عید و یکی بار مهرگان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر مهرگان و عید که آید بخرمی</p></div>
<div class="m2"><p>خوش بگذران بدولت و اقبال جاودان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بی برگ باد خصم تو چون در خزان درخت</p></div>
<div class="m2"><p>جون گوسپند عید فدای تو کرده جان</p></div></div>