---
title: >-
    شمارهٔ ۲۶ - در مدح وزیر صدرالدین
---
# شمارهٔ ۲۶ - در مدح وزیر صدرالدین

<div class="b" id="bn1"><div class="m1"><p>صدری که بر صدور زمانه مقدم است</p></div>
<div class="m2"><p>درگاه او چو کعبه شریف و معظم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر میان آدمیان چون فرشته است</p></div>
<div class="m2"><p>وندر دل فریشتگان همچو آدم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زی آنکه او فریشته و آدم آفرید</p></div>
<div class="m2"><p>جون آدم و فرشته عزیز و مکرم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن صدر کیست صاحب عادل که در جهان</p></div>
<div class="m2"><p>صاحب قران و صاحب صدر مسلم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رأی روشن وی و تدبیر محکمش</p></div>
<div class="m2"><p>روشن جهان و قاعده ملک محکم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ملکت عجم سر هر محترم ویست</p></div>
<div class="m2"><p>چون در عرب سر سال از محرم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیر نگین اوست سراسر همه جهان</p></div>
<div class="m2"><p>گوئی مگر که خاتم او خاتم جم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ذخرالملوک اوست و سراسر همه جهان</p></div>
<div class="m2"><p>از دولت خجسته سلطان عالم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دانست آن شهنشه عالم که آن بزرگ</p></div>
<div class="m2"><p>اندر خور وزارت سلطان اعظم است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اندر بزرگواری او نیست هیچ شک</p></div>
<div class="m2"><p>وندر بزرگواران مانند او کم است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای صاحبی که کف حداد تو روز بزم</p></div>
<div class="m2"><p>خورشید ذره پرور و ابر گهر نم است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرخنده فال صدری و دیدار روی تو</p></div>
<div class="m2"><p>منشور شادمانی و بیزاری از غم است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با همت رفیع و کف راد تو</p></div>
<div class="m2"><p>پست است اگر سپهر و بخیل است اگر یم است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر فی المثل باکمه و ابرص نظر کنی</p></div>
<div class="m2"><p>بی آنکه در تو معجز عیس بن مریم است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بینا شود بهمت تو آنکه اکمه است</p></div>
<div class="m2"><p>گویا شود بمدحت تو آنکه ابرص است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در زیر نعل مرکبت ار خاگ گل شود</p></div>
<div class="m2"><p>آن بر جراحت دل مظلوم مرهم است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور باد جود تو بوزد بر نیازمند</p></div>
<div class="m2"><p>با وی همه خزائن قارون فراهم است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از عدل تو هر آنچه بگفتم نه مشکل است</p></div>
<div class="m2"><p>وز جود تو هر آنچه بگویم نه مبهم است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در دین پاک خاتم پیغمبران ز عدل</p></div>
<div class="m2"><p>تو خاتمی و نام تو چون نقش خاتم است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خورشید اهل دین ببقای تو روشن است</p></div>
<div class="m2"><p>دیبای آفرین بثنای تو معلم است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در خورد تست خاتم اقبال و سروری</p></div>
<div class="m2"><p>چونانکه رخش رستم در خورد رستم است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با حاسد تو دولت چون آب و روغنست</p></div>
<div class="m2"><p>با ناصح تو ساخته چون زیر پا نم است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مهر تو بر ولی و خلاف تو بر عدو</p></div>
<div class="m2"><p>آن چون بر جنان خرم این چون جهنم است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گویم اگر عدوی تو کلبست راستست</p></div>
<div class="m2"><p>ور چند با شجاعت و باسهم ضیغم است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرچ آورد بچنگ همه بهره تو است</p></div>
<div class="m2"><p>وین اندرو نشانه کلب معلم است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از عکس و لمع اختر رخشنده هر شبی</p></div>
<div class="m2"><p>تا آسمان بگونه پیروزه تارم است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر تارم هوای دل خود نشاط کن</p></div>
<div class="m2"><p>با مهوشی که قبله یغما و تارم است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گردون خمیده میرود ار راست بنگری</p></div>
<div class="m2"><p>تعظیم آستان ترا پشت او خم است</p></div></div>