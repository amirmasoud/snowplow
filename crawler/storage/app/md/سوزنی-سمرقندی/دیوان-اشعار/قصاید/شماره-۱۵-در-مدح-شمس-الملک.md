---
title: >-
    شمارهٔ ۱۵ - در مدح شمس الملک
---
# شمارهٔ ۱۵ - در مدح شمس الملک

<div class="b" id="bn1"><div class="m1"><p>سوی ختا بسفر شد بعزم و رأی صواب</p></div>
<div class="m2"><p>بدفع شر خشم شاه شرع با اصحاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پادشاه ختا جست عدل نوشروان</p></div>
<div class="m2"><p>چو یافت آنچه بجست آمد از خطا بصواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امان خطه اسلام بود ز اهل خطا</p></div>
<div class="m2"><p>درآمد و شد او از مسبب الاسباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو سنگ را نتواند گزید و بوسه دهد</p></div>
<div class="m2"><p>کسی که باشد دعوی نمای معنی یاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمر چو نتوان بستن بجاهد الکفار</p></div>
<div class="m2"><p>گشاده به به لکم دینکم ولی دین باب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رقاب اهل هدی در طناب جور و ستم</p></div>
<div class="m2"><p>کسیکه خواست کشیدن کشیده شد بطناب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملک تعالی مالک رقاب عادل داد</p></div>
<div class="m2"><p>که خسروانرا در طوق امر اوست رقاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شراب عدل چشاند شکار خصم کند</p></div>
<div class="m2"><p>رعیت و حشم آسوده زین شکار و شراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو در سفر برکاب ملک عنان پیوست</p></div>
<div class="m2"><p>بحضرت آمد با شاه همعنان و رکاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مراد شاه اولوالعزم ازو مختص شد</p></div>
<div class="m2"><p>چنین بود اثر علم یا اولوالالباب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زهی نبیره برهان و سیف و شمس و حسام</p></div>
<div class="m2"><p>حسام حجت برهان سوآل سیف جواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن حسامی وارث که سیف حجت او</p></div>
<div class="m2"><p>بخصم حجت بنمود و در نشد بضراب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سحاب خوانم یا شمس یا همین و همان</p></div>
<div class="m2"><p>بنور زائی شمس و بکف راد سحاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز ذره ها که نماید بنور شمس فلک</p></div>
<div class="m2"><p>فضائل تو زیادت بود ز روی حساب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر صحیفه القاب تست صفحه لوح</p></div>
<div class="m2"><p>ستوده القاب از تست نی تو از القاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>توئی چو جد و پدر خسرو ممالک شرع</p></div>
<div class="m2"><p>سپهبدان تو صفدار منبر و محراب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>متابعان تو از شام تا سحر بسهر</p></div>
<div class="m2"><p>بر اهل بدعت در حرب رستم و سهراب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مبارزانت بتیغ زبان و رمح قلم</p></div>
<div class="m2"><p>خضاب کرده بخون مداد روی کتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کجا باشهد ان لا اله الا الله</p></div>
<div class="m2"><p>عمل کنند در آنجا نهاده ای نواب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز حد چین و ختن تا بحد مصر و یمن</p></div>
<div class="m2"><p>بود ائمه دین را بتو مصیر و مآب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کسی بخواب نبیند نظیر تو چو بدید</p></div>
<div class="m2"><p>گه تعلم تعلیم ناظران تو خواب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزرگواری میراث داری از اسلاف</p></div>
<div class="m2"><p>مؤثر است ز اسلاف خیر در اعقاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر آل برهان شاهی بر آل سیف ملک</p></div>
<div class="m2"><p>ترا سزاست ملکشاه اهل علم خطاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بحشر در سر پل هر که روزنامه شرع</p></div>
<div class="m2"><p>بر آل برهان خوانده است رسته شد زعقاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عزیز آل دو عبدالعزیزی از دو طرف</p></div>
<div class="m2"><p>یکی ز جانب مام و دگر ز جانب باب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عزیز مصر بخارا توئی بدین دو نسب</p></div>
<div class="m2"><p>عزیز بادی تا مدت فلا انساب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همیشه تا خطبا نام آن عمر گویند</p></div>
<div class="m2"><p>که هست باب ترا جد و باب او خطاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خطیب منبر مصر ثنا و مدح ترا</p></div>
<div class="m2"><p>فصیح باد زبان بر معاشر احباب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر دگر شعرا کاذبند باکی نیست</p></div>
<div class="m2"><p>بنظم مدحت تو نیست سوزنی کذاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ترا ز رحمت ناب آفرید خالق خلق</p></div>
<div class="m2"><p>کجا تو باشی باشد مکان رحمت ناب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بهر کجا که روی یا ز هر کجا آئی</p></div>
<div class="m2"><p>مباد جز بطریق بهی مجئی و ذهاب</p></div></div>