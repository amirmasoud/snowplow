---
title: >-
    شمارهٔ ۱۱۷ - در مدح نظام الدین
---
# شمارهٔ ۱۱۷ - در مدح نظام الدین

<div class="b" id="bn1"><div class="m1"><p>جاودان ماند کریم از مدح شاعر زنده نام</p></div>
<div class="m2"><p>زین بود شاعر نوازی عادت و رسم کرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدحت از گفتار شاعر محمل صدقست و کذب</p></div>
<div class="m2"><p>صدق در حق کرام و کذب در حق لئام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاعر آن در زیست دانا کو باندام کریم</p></div>
<div class="m2"><p>راست آرد کسوت مدحت بمقراض کلام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر لئیمی پوشد آن کسوت بچشم اهل عقل</p></div>
<div class="m2"><p>هست بر پوشیده بی اندام و بر در زی سلام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبع شاعر هست چون دارالسلم از خرمی</p></div>
<div class="m2"><p>جز کریم اندر نیاید از در دارالسلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راد با شاعر تواند بود در یک پیرهن</p></div>
<div class="m2"><p>زفت نگذارد به پیراهن که تا گوید سلام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست شعر آن خوش حدیثی کاستماعش کرده اند</p></div>
<div class="m2"><p>هم نبی و هم وصی و هم امیر و هم امام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعر حسان بن ثابت را بخوش طبعی شنود</p></div>
<div class="m2"><p>پادشاه دین رسول ابطحی خیر الانام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داد دستاری بحسان اندران یکتار موی</p></div>
<div class="m2"><p>بهتر از دستار دستار از خراج مصر و شام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سنت شاعر نوازی پادشاه دین نهاد</p></div>
<div class="m2"><p>ای همه شاهان دنیا مرغلامش را غلام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ملوک و از صدور از بعد آن سلطان دین</p></div>
<div class="m2"><p>در صلات شاعران کردند سنت را قیام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رودکی را اندران جامه که وصف باده بود</p></div>
<div class="m2"><p>داد دیناری هزار از زر آتشگون و فام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرد عتبی با کسائی همچنین کردار خوب</p></div>
<div class="m2"><p>ماند عتبی از کسائی تا قیامت زنده نام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قیمت عیار را هم فام کرد از دیگری</p></div>
<div class="m2"><p>بلعمی عیاروار از رودکی بفکند فام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اوستاد مشرق و مغرب رشیدی را بشعر</p></div>
<div class="m2"><p>داد سعد الملک قطر میر زی از سیم خام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خوب کرداری ز بهر زنده نامی کرده اند</p></div>
<div class="m2"><p>زنده نامی بهتر است از زندگی لحم و عظام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرخی هندی غلامی از قهستانی بخواست</p></div>
<div class="m2"><p>سی غلام ترک دادش خوش لقا و خوش خرام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عنصری از خسرو غازی شه ز اول بشعر</p></div>
<div class="m2"><p>پیلوار زر گرفت و دیبه و اسب و سیام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر ورق یابی ز دیوانش چو میدانی در او</p></div>
<div class="m2"><p>خسرو ز اول کشیده تیغ هندی از نیام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تیغ هندی خدمت کلک نظام الدین کند</p></div>
<div class="m2"><p>چون نظام الدین دهد کار ممالک را نظام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صدر ممدوحان نظام الدین که نظم مدح او</p></div>
<div class="m2"><p>از شنیدن گوش خوش گردد بگفتن حلق و کام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کام و رای او ز عالم هست شاعر پروری</p></div>
<div class="m2"><p>شاعران را مدح او گفتن بگیتی رأی و کام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از کریمانی که بردم نام شاعر پروری</p></div>
<div class="m2"><p>دیده ایشان نبیند صورت لا در منام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هم قهستانی و عتبی را بهم با بلعمی</p></div>
<div class="m2"><p>زو شود نادیده دیدی چون ورا دیدی تمام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در سخاوت صد یک او نیستند و هر یکی</p></div>
<div class="m2"><p>در هنر صد چند هر یک هست و پیش از هر کدام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قطر باران در شود در خورد سنگ مدح او</p></div>
<div class="m2"><p>شاعر از دریای فکرت چون برانگیزد غمام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جود او دامی است شاعر را نه دام خلق گیر</p></div>
<div class="m2"><p>دست گیرد تا نگیرد دست پیش خاص و عام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خود غلط گفتم که جودش هست دام حلق گیر</p></div>
<div class="m2"><p>تا نگیرم مدح هر کس چون بود بر حلق دام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شاعر سخته سخن یابد بهر بیتی ازو</p></div>
<div class="m2"><p>بدره بدره زر پخته کیسه کیسه سیم خام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای هزاران شاعر سخته سخن را همت آنک</p></div>
<div class="m2"><p>مدح آرایند بر نام تو ممدوح همام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون بود در حق فرزند اهتمام مام و باب</p></div>
<div class="m2"><p>همچنان باشد ترا در حق مداح اهتمام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دایه الطاف تو اطفال اهل نظم را</p></div>
<div class="m2"><p>تربیت زانسان کند چون طفل خود را باب و مام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر که از خم می مدح تو جامی نوش کرد</p></div>
<div class="m2"><p>تا نگردد مست طافح کی نهد از دست جام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صدر تو بیت الحرام اهل نظم است از قیاس</p></div>
<div class="m2"><p>بنده از سالی بسالی زایر بیت الحرام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر نه ابراهیم نامم خواهم ابراهیم وار</p></div>
<div class="m2"><p>تا دران بیت الحرام از مدح تو گیرم مقام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همت تو از بلندی بام عرش است از مثل</p></div>
<div class="m2"><p>گر سپهر برترین را سایه عرش است بام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا ترا بنشاند بر صدر وزارت شاه شرق</p></div>
<div class="m2"><p>وزر ورزی در زمین ملک شه ننهاد گام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کلک منقاد حسامست و نباشد بس عجب</p></div>
<div class="m2"><p>کلک نواب ترا گرانقیاد آرد حسام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گردن اعدات بادا از حسام غم زده</p></div>
<div class="m2"><p>غمزده اعدات و احباب تو زان غم شادکام</p></div></div>