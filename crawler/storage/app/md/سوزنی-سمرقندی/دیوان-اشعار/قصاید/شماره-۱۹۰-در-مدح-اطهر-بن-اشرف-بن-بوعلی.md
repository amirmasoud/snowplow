---
title: >-
    شمارهٔ ۱۹۰ - در مدح اطهر بن اشرف بن بوعلی
---
# شمارهٔ ۱۹۰ - در مدح اطهر بن اشرف بن بوعلی

<div class="b" id="bn1"><div class="m1"><p>ای که در ملک سیادت خسرو دریا دلی</p></div>
<div class="m2"><p>مفخری بر عترت مختار بی آل ولی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر حدیث از لفظ تو دریست از دریای لفظ</p></div>
<div class="m2"><p>از دل دریا برآید در تو دریا دلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زینت آل حسین بن علی المرتضی</p></div>
<div class="m2"><p>میر میران اطهر بن اشرف بن بوعلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوعلی از اشرف و اشرف ز تو نازد بحشر</p></div>
<div class="m2"><p>پیش مختار و علی آن شاه کافی و ملی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن علی کو عمر و عنتر را بزخم ذوالفقار</p></div>
<div class="m2"><p>سر جدا کرد از قفا همچو ترنج آملی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنعلی کاندر مصاف صد هزاران خصم خواست</p></div>
<div class="m2"><p>هر یکی چون رستم دستان و زال زاولی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن علی را از نژاد بوعلی اندر جهان</p></div>
<div class="m2"><p>نیست همتای تو فرزندی بوالله العلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کسی گوید که همتای تو دیدم سیدی</p></div>
<div class="m2"><p>هم ترا دیده بود وان دیده دارد احولی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صدر و بدر مرسلان بدسید آخر زمان</p></div>
<div class="m2"><p>تو بنسبت صد رو بد عترت آن مرسلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سید اول وی است و سید آخر وی است</p></div>
<div class="m2"><p>سید آخر وئی گر آخری یا اولی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر کند با تو کسی دعوی بصاحب گیسوئی</p></div>
<div class="m2"><p>گیسو از شرمت فرو ریزد پدید آید کلی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خرمی هر محفلی از صدر آن محفل بود</p></div>
<div class="m2"><p>خرم آن محفل که تو صدر و سر آن محفلی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محسن و مجمل بود در خور بمدح و آفرین</p></div>
<div class="m2"><p>آفرین بر تو که تو هم محسن و هم مجملی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تیز رو باشد بسوی راه دوزخ روز حشر</p></div>
<div class="m2"><p>هرکه اینجا در ره مهرت رود با کاهلی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>منت ایزد را که من باری نیم زان کاهلان</p></div>
<div class="m2"><p>کاهلی آن ره بود یا خارجی یا حنبلی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر مرا آئینه خاطر شود زنگار گیر</p></div>
<div class="m2"><p>زنگ برخیزد چو از مدح تو سازم صیقلی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>منزل آل ولی الله اعلی منزل است</p></div>
<div class="m2"><p>وز همه آل ولی الله اعلی منزلی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دین حق را از کمال جاه تو قوت فزود</p></div>
<div class="m2"><p>گر کمال الدین لقب داری که نی بر باطلی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن کمال الدین توئی ای اطهر اشرف نسب</p></div>
<div class="m2"><p>کز همه عالم بگو هر اشرف و اکملی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیش حلم و جود تو هرگز نیارد کرد جز</p></div>
<div class="m2"><p>کوه جودی ذرگی دریای قلزم جدولی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آفتاب جودت از نور افکند برمد خلی</p></div>
<div class="m2"><p>در زمان چون سایه بگریزد ز طبعش مدخلی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شکر حنظل زکین مهر تو پیدا شدند</p></div>
<div class="m2"><p>بر موالی شکری و بر معادی حنظلی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خار و گل دارند نعت عنف و وصف لطف تو</p></div>
<div class="m2"><p>تا ولیرا بوی بخشی و عدو را دل خلی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شاعران از هر طرف نزدیک تو شعر آورند</p></div>
<div class="m2"><p>من بصدر تو خموش این نیست جز از تنبلی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وحی منزل بود مدح جد تو از آسمان</p></div>
<div class="m2"><p>تو چو جد خود سزای مدح و وحی منزلی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر بدانم کز ثنا و مدح من خوشآیدت</p></div>
<div class="m2"><p>در ثنای او نخواهم کرد هرگز کاهلی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ور بدان کز طبع من زاید بوی راضی رسد</p></div>
<div class="m2"><p>کاروان بر کاروان و خنگلی بر خنگلی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حاصل آندان گر پسند آید ترا اشعار من</p></div>
<div class="m2"><p>یکدم از گفتن نیاسایم بود بی حاصلی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جویم از درگاه تو مر خویشتن را آبروی</p></div>
<div class="m2"><p>همچو از درگاه هرون بو سحاق موصلی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باد درگاه تو دایم جایگاه اهل فضل</p></div>
<div class="m2"><p>گرچه در هر فضل از هر اهل فضلی افضلی</p></div></div>