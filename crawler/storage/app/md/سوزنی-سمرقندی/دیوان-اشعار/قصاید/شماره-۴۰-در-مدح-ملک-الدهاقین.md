---
title: >-
    شمارهٔ ۴۰ - در مدح ملک الدهاقین
---
# شمارهٔ ۴۰ - در مدح ملک الدهاقین

<div class="b" id="bn1"><div class="m1"><p>صدر جهان ز مجلس جهان رسید</p></div>
<div class="m2"><p>مهمان همی رسد شه و او میزبان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آراست خانمان به خجسته لقای خویش</p></div>
<div class="m2"><p>کز پیش تخت خوان بسوی خانمان رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلد شهر نخشب و از خلد خوشتر است</p></div>
<div class="m2"><p>صد ره از آنکه صدر بدو شادمان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدری که آستان رفیقش بمرتبت</p></div>
<div class="m2"><p>گز زآسمان نه برتر تا بآسمان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آسمان بساید فرق سر از شرف</p></div>
<div class="m2"><p>هر کز قدم بخدمت این آستان رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آستان او ز ره جاه و منزلت</p></div>
<div class="m2"><p>آسان بآسمان برین بر توان رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صدری که بر دهاقین دارد ملک لقب</p></div>
<div class="m2"><p>زی ملک خویش چون ملک کامران رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این لفظ بر زبان دهاقین نخشب است</p></div>
<div class="m2"><p>شادی کنیم چون ملک از نزد خان رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جسمند اهل نخشب بی جان چوبی و بند</p></div>
<div class="m2"><p>واکنون که او رسید سوی جسم و جان رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دست روزگار ستمگر بعهد او</p></div>
<div class="m2"><p>زی اهل شهر نخشب خط امان رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دریای جود و کان سخا کف راد اوست</p></div>
<div class="m2"><p>کاحسان او بجمله خلق جهان رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از شهر نخشب است شرف بر همه جهان</p></div>
<div class="m2"><p>کامروز سوی نخشب دریا و کان رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شه بوستان دولت نخشب بعدل شاه</p></div>
<div class="m2"><p>یک سرو در دو بستان کسرا گمان رسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سرو روان بود که بهر بوستان رسد</p></div>
<div class="m2"><p>این سرو سرفراز بدین و بدان رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک چند گه نیابت آن بوستان گذشت</p></div>
<div class="m2"><p>وین چندگه نیابت این بوستان رسید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای آنکه هرکه دید ترا زاهل این دیار</p></div>
<div class="m2"><p>پند است مادر و پدر مهربان رسید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پنداشت نیست هست حقیقت درین سخن</p></div>
<div class="m2"><p>زینسان سخن بگوش تو از هر زبان رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر تو زبان اهل زمانه دعاگر است</p></div>
<div class="m2"><p>جود و سخای تو چو باهل زمان رسید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نزدیک شه مکانت شه بین و ظن مبر</p></div>
<div class="m2"><p>در کس که کس بدین شرف و این مکان رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از رسم و سان خوب رسیدی بدین محل</p></div>
<div class="m2"><p>هر کس بدین محل بهمین رسم و سان رسید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باری سپاس از ملک غیب دان پذیر</p></div>
<div class="m2"><p>کاین جاه و دولت از ملک غیب دان رسید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گویند مهدی آید صاحب قران برون</p></div>
<div class="m2"><p>چون مدت زمانه خوهد بر کران رسید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صاحبقران تو بادی و مدت بسر مباد</p></div>
<div class="m2"><p>چون مملکت جهان بتو صاحب قران رسید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون آمد از ثنا بدعا بقای تو</p></div>
<div class="m2"><p>شد مستجاب و مژده ور جادان رسید</p></div></div>