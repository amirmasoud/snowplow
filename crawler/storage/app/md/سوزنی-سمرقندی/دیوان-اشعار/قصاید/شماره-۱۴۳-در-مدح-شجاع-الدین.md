---
title: >-
    شمارهٔ ۱۴۳ - در مدح شجاع الدین
---
# شمارهٔ ۱۴۳ - در مدح شجاع الدین

<div class="b" id="bn1"><div class="m1"><p>علی است روز مصاف و نبرد و کوشش و کین</p></div>
<div class="m2"><p>سر سپه شکنان بوعلی شجاع الدین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهاء دولت عالی مبارز الحضرت</p></div>
<div class="m2"><p>پناه حضرت سلطان ملک روی زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبارزی که مراو را بروز بار و مصاف</p></div>
<div class="m2"><p>هرآنکه دید به بیند بچشم روشن بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار حاتم طائی نشسته در یک تخت</p></div>
<div class="m2"><p>هزار رستم دستان سام در یک زین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بچشم او ننماید بحرب جز بازی</p></div>
<div class="m2"><p>نبرد و کوشش و پیکار رستم و روئین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنانگور اگر روی سوی چین آرد</p></div>
<div class="m2"><p>ز سهم ان فزع اندر فتد بلشکر چین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بیم ضربت صمصام آبدار ورا</p></div>
<div class="m2"><p>رخ مخالف شه چون زره شود پرچین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس شجاعت او بر دهان مادح او</p></div>
<div class="m2"><p>سخن رود که تو گوئی درست گشت و یقین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که کردگار بهنگام خلقت آدم</p></div>
<div class="m2"><p>ابوعلی و علی را سرشت از یک طین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز هر مصافی آید مظفر و منصور</p></div>
<div class="m2"><p>بدان صفت که علی آمد از صف صفین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قد عدوش بسان کمان شود پر خم</p></div>
<div class="m2"><p>چو او زخم کین یر عدو گشاد کمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شهاب ثاقب گردد خدنگ او ز گشاد</p></div>
<div class="m2"><p>عدوش سوخته گردد ازو چو دیو امین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برند کیفر از چاه و بند و تخته او</p></div>
<div class="m2"><p>مخالفان خداوند تاج و تخت و نگین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایا بنزد خداوند تخت و خاتم و تاج</p></div>
<div class="m2"><p>همیشه بوده ز شایستگی عزیز و مکین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رعیت توامان یافته ز دست رستم</p></div>
<div class="m2"><p>از آن سبب که نئی بر ستم کننده امین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بجاه خسرو گیتی ستان ستانی داد</p></div>
<div class="m2"><p>ز ملک گیتی چونانکه خسرو از شیرین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کسی که عیش تو بر او تلخ کرد آفت دهر</p></div>
<div class="m2"><p>شود ز دیدن تو عیش تلخ او شیرین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو آفتاب زمینی برأی روشن بین</p></div>
<div class="m2"><p>که هست رأی ترا بنده آفتاب مبین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بجود بحر محیطی نه زانکه بحر محیط</p></div>
<div class="m2"><p>کف جواد ترا هست چون رهی و رهین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رهین منت انعام تست در عالم</p></div>
<div class="m2"><p>فزون ز ذره آن و فزون ز قطره این</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رمیدگان و کراشیده گشته گان ز وطن</p></div>
<div class="m2"><p>ترا خوهند ز ایزد بدعوت و آئین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که تا بدولت و اقبال و جاه و حشمت تو</p></div>
<div class="m2"><p>روند تا ز وطن چند بیوه و مسکین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزیر سایه عدل تو روزگار کشند</p></div>
<div class="m2"><p>که عدل تست چو طوبی جهان چو خلد برین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه تا چکد از ابر قطره باران</p></div>
<div class="m2"><p>ز کف راد برافشان بخلق در تمثین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز دست آنکه چو نسرین و لاله دارد رخ</p></div>
<div class="m2"><p>بگیر جام و مئی نوش همچو ماه معین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو یار خلق خدائی خدای یار تو باد</p></div>
<div class="m2"><p>بهر کجا که روی حافظ تو باد و معین</p></div></div>