---
title: >-
    شمارهٔ ۷۹ - در مدح تاج الدین محمود
---
# شمارهٔ ۷۹ - در مدح تاج الدین محمود

<div class="b" id="bn1"><div class="m1"><p>گسترد نام نیک چو محمود تاجدار</p></div>
<div class="m2"><p>محمود تاج شد و ز احرار روزگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شاهوار بخشش او ظن بری که او</p></div>
<div class="m2"><p>محمود تاج نیست که محمود تاجدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او تاجدار ملک هنر زیبد و عدوش</p></div>
<div class="m2"><p>در بارگاه حشمت او گشته تاج دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا آمد از دیار خراسان بماورا</p></div>
<div class="m2"><p>النهر نهر دولت او گشت چون بحار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر فرق اهل فضل زرافشان شود هوا</p></div>
<div class="m2"><p>هرگاه از آن بحار شود بر هوا بخار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیل و فرات و دجله و جیحون موج زن</p></div>
<div class="m2"><p>با کف داد او چو سرابند هر چهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگه که سیر کلک کمردار او کند</p></div>
<div class="m2"><p>سردل دوات کلهدارش آشکار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آرد برات او امرای کلام را</p></div>
<div class="m2"><p>بر دوش طوق منت و در گوش گوشوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محمود شاه غازی شاعر نواختن</p></div>
<div class="m2"><p>آیین نهاد و سنت و رسم و ره و شعار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از سرگذشت بود و نبود همه جهان</p></div>
<div class="m2"><p>دیوان عنصریست ز محمود یادگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وز خادمان مجلس محمود تاج دین</p></div>
<div class="m2"><p>چون عنصری هزار برآید بیک شمار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>محمود سومنات گشای صنم شکن</p></div>
<div class="m2"><p>در غزو سیگزی بسنان ز ره گذار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن مرتبت نیافت که محمود تاج دین</p></div>
<div class="m2"><p>از یک بدست کلک بریده سر نزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای تاج کز جواهر دانش مرصعی</p></div>
<div class="m2"><p>بر فرق دین سید و شاه بنی نزار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نور دلی و راحت روح و سدید دین</p></div>
<div class="m2"><p>عبدالکریم صدر کرام و سر کبار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عبدالکریم صدری کز وی کریمتر</p></div>
<div class="m2"><p>عبدی نیافریده کریم آفریدگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او اصل مهتریست مران اصل را تو فرع</p></div>
<div class="m2"><p>تا زان بتو چو جسم بروح و شجر ببار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مستوفی ممالک مشرق توئی و هست</p></div>
<div class="m2"><p>بر کلک بی قرار تو هر ملک را قرار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز ابنای روزگار نیاید کسی چو تو</p></div>
<div class="m2"><p>بر مرکب کفایت و فضل و هنر سوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر کار تو بعقد بنانست و سیر کلک</p></div>
<div class="m2"><p>اندر کشی ذرایر خورشید را بکار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بی سهو و بی غلط بجریده نشان کنی</p></div>
<div class="m2"><p>از پیش باد اگر بهزیمت رود غبار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دانی شمار آن و ندانی که سیم و زر</p></div>
<div class="m2"><p>بر شاعران ز جود تو چندین شود نثار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر شاعران ثنای تو در سال سنت است</p></div>
<div class="m2"><p>بر من رهی فریضه بروزی هزار بار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر شعر بنده هست بدین چاشنی پسند</p></div>
<div class="m2"><p>در یک دو مه بمدح تو دیوان کنم نگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا از برای گفت و شنود است خلق را</p></div>
<div class="m2"><p>گوش سخن نیوش و زبان سخن گذار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سیماب باد ریخته در گوش آنکسی</p></div>
<div class="m2"><p>کو دارد از شنودن مدح و ثنات عار</p></div></div>