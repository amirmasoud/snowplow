---
title: >-
    شمارهٔ ۱۰۱ - در مدح وزیر
---
# شمارهٔ ۱۰۱ - در مدح وزیر

<div class="b" id="bn1"><div class="m1"><p>همه سلامت آن باد کو بجان و بدل</p></div>
<div class="m2"><p>خوهد سلامت احوال صاحب عادل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه مراد کسی باد حاصل از عالم</p></div>
<div class="m2"><p>که او مراد ورا خواهد از جهان حاصل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانه بنده اقبال صاحب است بدان</p></div>
<div class="m2"><p>که در زمانه چنو نیست صاحب مقبل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وزیر مشرق کز داد او همیشه ستم</p></div>
<div class="m2"><p>بود گریزان چون ز آفتاب مشرق ظل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آسمان بزمین هیچ دولتی ناید</p></div>
<div class="m2"><p>مگر بدانکه کند در سرای او منزل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بروی او نگر از جمله بنی آدم</p></div>
<div class="m2"><p>اگر نه آدمئی دیده ای فریشته دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنای هیچ عمل جز بعلم بر ننهد</p></div>
<div class="m2"><p>جز او کس از وزرا نیست عالم و عادل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجز برأی و بتدبیر و نیک عهدی او</p></div>
<div class="m2"><p>بود سلاطین را ملک داشتن مشکل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه بی شکوهش پیراسته بود ملکت</p></div>
<div class="m2"><p>نه بی جمالش آراسته بود محفل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همیشه منزل دولت نماید آن خانه</p></div>
<div class="m2"><p>که ساعتی بنشاط اندرو بود نازل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سزای غل بود آن گردنی که بر صاحب</p></div>
<div class="m2"><p>بجهل سینه خود کان کینه سازد و غل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگاهدارد در هر چه هست کار خدای</p></div>
<div class="m2"><p>خدای ازین نکند هیچ حق او باطل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه ایزد است ولیکن بحکم ایزد نیست</p></div>
<div class="m2"><p>ز هیچ نیک و بد بندگان خود غافل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جمال داد و برافروزد جاه و حشمت او</p></div>
<div class="m2"><p>چنین کنند بزرگان محسن و مجمل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بپیش آنکه ازو آفتابرا خجل است</p></div>
<div class="m2"><p>ز بی خبر بدن از کار خویش هست خجل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بلی خجل شود از پادشه که ناگاهان</p></div>
<div class="m2"><p>بآستانه او میهمان رسد طغرل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز جاه صاحب عادل ملک بگرداند</p></div>
<div class="m2"><p>گزند چشم بدو مکر حاسد و عاذل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عزیز باد همیشه بنزد خلق و خدای</p></div>
<div class="m2"><p>نگاهدار تن و جان او معز و مذل</p></div></div>