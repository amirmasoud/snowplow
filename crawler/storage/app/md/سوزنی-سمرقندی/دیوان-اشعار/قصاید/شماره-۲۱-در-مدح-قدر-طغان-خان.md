---
title: >-
    شمارهٔ ۲۱ - در مدح قدر طغان خان
---
# شمارهٔ ۲۱ - در مدح قدر طغان خان

<div class="b" id="bn1"><div class="m1"><p>بخت یار قدر طغان خانست</p></div>
<div class="m2"><p>فتح کار قدر طغان خانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت یار کسی است کز بن گوش</p></div>
<div class="m2"><p>بختیار قدر طغان خانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صاحب ذوالفقار از آنکه بنام</p></div>
<div class="m2"><p>در جوار قدر طغان خانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدل ذوالفقار او به نبرد</p></div>
<div class="m2"><p>ذوالفقار قدر طغان خانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم ذوالفقار نصرت حق</p></div>
<div class="m2"><p>حق گذار قدر طغان خانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدرت آل نوح در کشور</p></div>
<div class="m2"><p>ز اقتدار قدر طغان خانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جهان هر کجا جهانداریست</p></div>
<div class="m2"><p>از تبار قدر طغان خانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قبله جمله جهانداران</p></div>
<div class="m2"><p>صدر بار قدر طغان خانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گردن سرکشان ز بار منن</p></div>
<div class="m2"><p>زیر بار قدر طغان خانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شجر ملک و دین ملت را</p></div>
<div class="m2"><p>برگ و بار قدر طغان خانست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حسن جمشید و فر افریدون</p></div>
<div class="m2"><p>در عذار قدر طغان خانست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از ره بندگی بگوش سپهر</p></div>
<div class="m2"><p>گوشوار قدر طغان خانست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر فلک آفتاب شیر سوار</p></div>
<div class="m2"><p>نی سوار قدر طغان خانست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شیر گردون بترس و بیم و هراس</p></div>
<div class="m2"><p>از شکار قدر طغان خانست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آسمان گر شکار شیر کند</p></div>
<div class="m2"><p>مرغزار قدر طغان خانست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روز بازار شغل عزرائیل</p></div>
<div class="m2"><p>کارزار قدر طغان خانست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ملک جان ستان ز دشمن ملک</p></div>
<div class="m2"><p>جان سپار قدر طغان خانست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سبزه زار سر عدو بمصاف</p></div>
<div class="m2"><p>لاله زار قدر طغان خانست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تیغ نیلوفری از آنکه بدست</p></div>
<div class="m2"><p>لاله کار قدر طغان خانست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خصم را بهترین ظفر که مباد</p></div>
<div class="m2"><p>زینهار قدر طغان خانست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از همه کارها جوانمردی</p></div>
<div class="m2"><p>اختیار قدر طغان خانست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عارض سیم و چهره دینار</p></div>
<div class="m2"><p>بنگار قدر طغان خانست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اصل و فرع ستایش شعرا</p></div>
<div class="m2"><p>از شعار قدر طغان خانست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یمنی تیغ در یمین و نگین</p></div>
<div class="m2"><p>در یسار قدر طغان خانست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این چهار است اصل و باقی فرع</p></div>
<div class="m2"><p>هر چهار قدر طغان خانست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدعا و ثنا شبانروزی</p></div>
<div class="m2"><p>یاددار قدر طغان خانست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در کنار فلک قرار زمین</p></div>
<div class="m2"><p>از وقار قدر طغان خانست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا زمین است ملک روی زمین</p></div>
<div class="m2"><p>برقرار قدر طغان خانست</p></div></div>