---
title: >-
    شمارهٔ ۱۶۷ - در مدح نصیرالدین علی بن احمد
---
# شمارهٔ ۱۶۷ - در مدح نصیرالدین علی بن احمد

<div class="b" id="bn1"><div class="m1"><p>ای دو لب تو قافله قند شکسته</p></div>
<div class="m2"><p>قد تو سر سرو سمرقند شکسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بادام دو چشم تو بعیاری و شوخی</p></div>
<div class="m2"><p>صدبار بهر لحظه درکند شکسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هیبت مژگان دو بادام تویی جنگ</p></div>
<div class="m2"><p>چنگال هژبران زلفین رند شکسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جنگ نیارامدی آنچشم چو بادام</p></div>
<div class="m2"><p>از سنگ دو سه سنگ زن رند شکسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سنگ شد آن کندی بادام تو از چنگ</p></div>
<div class="m2"><p>در پوست چو از باد صبا بند شکسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر لعل شکرخند که نرخ شکر و لعل</p></div>
<div class="m2"><p>کردی بدو لعل شکر آکند شکسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش باش بدان دو لب خوشخند که کردی</p></div>
<div class="m2"><p>بازار شکر زان لب خوشخند شکسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بادی که شکنهای دو زلف تو پراکند</p></div>
<div class="m2"><p>زان باد دلی چند پراکند شکسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چین و شکن زلف تو چند است ندانم</p></div>
<div class="m2"><p>دانم که در او هست دلی چند شکسته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جای دل من نیست در آنزلف که نبود</p></div>
<div class="m2"><p>هرگز دل مداح خداوند شکسته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ممدوح هنرمند که از هیچ هنرمند</p></div>
<div class="m2"><p>نه مهر گسسته است و نه پیوند شکسته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>. . . که ندارد ز هنرمندان مانند</p></div>
<div class="m2"><p>تا هست ز مانند چو مانند شکسته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای بنده نوازی که کف راد تو دارد</p></div>
<div class="m2"><p>آز دل خرسند بخرسند شکسته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دارد شره جود بر آنگونه که گوئی</p></div>
<div class="m2"><p>دیوانه شدستی کف تو بند شکسته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرزانه نصیرالدین کز دولت او نیست</p></div>
<div class="m2"><p>ارز هنر و قدر هنرمند شکسته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نپذیرد اگر پند دهندش که مده مال</p></div>
<div class="m2"><p>نبود شره جود وی از پند شکسته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جز مدح تو ترفند بود هرچه نویسم</p></div>
<div class="m2"><p>کردم قلم از یافه و ترفند شکسته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون خاطر من مدحت تو زاید فرزند</p></div>
<div class="m2"><p>موئی نخوهم بر سر فرزند شکسته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر آتش مدح دگران بایدش افروخت</p></div>
<div class="m2"><p>تا سوخته تر باشد یارند شکسته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در مدح تو گردد بدرستی سخن من</p></div>
<div class="m2"><p>آنکس که بیابی سخن افکند شکسته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دارد ببقای تو فلک بیعت و سوگند</p></div>
<div class="m2"><p>هرگز نخوهد بیعت و سوگند شکسته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دارد دل اعدای تو سوزی که نگردد</p></div>
<div class="m2"><p>آن سوز بکافور و بریوند شکسته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر بستر غم خفت حسود تو چنان زار</p></div>
<div class="m2"><p>کش تن شود از بار قزاکند شکسته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وز دار محن گشت عدوی تو نگونسار</p></div>
<div class="m2"><p>چون خوشه انگور براوند شکسته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حسادترا در دل و در پشت شکسته است</p></div>
<div class="m2"><p>جز پشت و دل حاسد مپسند شکسته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا شهره بود غمزه معشوق غنوده</p></div>
<div class="m2"><p>تا طرفه بود طره دلبند شکسته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در طره آن قند لب آویز که مژگانش</p></div>
<div class="m2"><p>دارد صف جادوی دماوند شکسته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از قند لبی بوسه همی خواه که دارد</p></div>
<div class="m2"><p>از دو لب خود قافله قند شکسته</p></div></div>