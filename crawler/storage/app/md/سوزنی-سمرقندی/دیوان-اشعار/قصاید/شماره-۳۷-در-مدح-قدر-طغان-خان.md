---
title: >-
    شمارهٔ ۳۷ - در مدح قدر طغان خان
---
# شمارهٔ ۳۷ - در مدح قدر طغان خان

<div class="b" id="bn1"><div class="m1"><p>پادشاه جهان ز راه رسید</p></div>
<div class="m2"><p>ملک نو شد چو پادشاه رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه شاهان قدر طغان خاقان</p></div>
<div class="m2"><p>از سفر با کمال و جاه رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتح بر عطف زین ببسته برفت</p></div>
<div class="m2"><p>نصر بر پره کلاه رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو سکندر برفت و همچون خضر</p></div>
<div class="m2"><p>بلب چشمه حیات رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از میان سپاه دیو و پری</p></div>
<div class="m2"><p>چون سلیمان برفت و گاه رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داستان از درست و دیو زنند</p></div>
<div class="m2"><p>او درست آمد و بگاه رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد بعون اله سوی سفر</p></div>
<div class="m2"><p>باز در عصمت اله رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حق ایزد نگاهداشته رفت</p></div>
<div class="m2"><p>ایزدش داد تا بگاه رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اهل دین را ز خوف لشکر کفر</p></div>
<div class="m2"><p>مأمن و ملجاء و پناه رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وان اسیران ممتحن شده را</p></div>
<div class="m2"><p>فرخ و ملجاء و نجاه رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کسی این سفر گناه انگاشت</p></div>
<div class="m2"><p>شه بآمرزش گناه رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد بتدبیر اولیا بسفر</p></div>
<div class="m2"><p>وز سفر قامع العداء رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بفنا بردن معادی را . . .</p></div>
<div class="m2"><p>همچو صرصر بسوی گاه رسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنما دادن موالی چون</p></div>
<div class="m2"><p>نم رحمت سوی گیاه رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملکداری بخواب غفلت بود</p></div>
<div class="m2"><p>از طغانخان بانتباه رسید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خشم افزون حضم کاسته خواست</p></div>
<div class="m2"><p>حشم افزون و حضم کاه رسید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شد بدعوی ملک و صفحه تیغ</p></div>
<div class="m2"><p>حجت آورد و با گواه رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این بشارت ز جوشن ماهی</p></div>
<div class="m2"><p>تا بخفتان سبز ماه رسید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هیچ شه را بسالها نرسد</p></div>
<div class="m2"><p>آنچه او را بیک دو ماه رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از حزاسیدن و رسیدن شاه</p></div>
<div class="m2"><p>چو بشارت سوی سپاه رسید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دولت از خیمه کبود سپهر</p></div>
<div class="m2"><p>بسر خرگه سپاه رسید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وز خشم ده هزار یکتا دل</p></div>
<div class="m2"><p>پیش شه قامت و تاه رسید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رفتن بارگاه او همه را</p></div>
<div class="m2"><p>زینت عارض و جباه رسید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه بر آیینه دل کس از او</p></div>
<div class="m2"><p>بد زنگ و غبار راه رسید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سرمه دیده چشم شد و آن</p></div>
<div class="m2"><p>نیک خواه و نکو نگاه رسید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باد ار اندیشه دل تباه آنرا</p></div>
<div class="m2"><p>کز سر اندیشه تباه رسید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حاسد از رشک جاه عالی شاه</p></div>
<div class="m2"><p>خائبا خاسرا بچاه رسید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در دریای ملک شاه گرفت</p></div>
<div class="m2"><p>حضم را غوطه و شناه رسید</p></div></div>