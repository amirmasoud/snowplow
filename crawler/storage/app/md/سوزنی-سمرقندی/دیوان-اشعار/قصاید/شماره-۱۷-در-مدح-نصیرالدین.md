---
title: >-
    شمارهٔ ۱۷ - در مدح نصیرالدین
---
# شمارهٔ ۱۷ - در مدح نصیرالدین

<div class="b" id="bn1"><div class="m1"><p>ای رخ خوبت بمثل آفتاب</p></div>
<div class="m2"><p>چون بمثل گویم بل آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ شناسی تو که روی ترا</p></div>
<div class="m2"><p>خوانده رهی از چه قبل آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی ترا نیست بخوبی بدل</p></div>
<div class="m2"><p>گرچه خوهد بود بدل آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکل رخ و زلف تو گیرد اگر</p></div>
<div class="m2"><p>بندد از مشک کلل آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وان دهن تنگ تو گوئی بنیش</p></div>
<div class="m2"><p>جست مگر نخل عسل آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدن تو آب دواند ز چشم</p></div>
<div class="m2"><p>آب دواند ز مقل آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بصفات رخ رخشان تو</p></div>
<div class="m2"><p>یافته مقدار و محل آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اکثر اوصاف چگویم که هست</p></div>
<div class="m2"><p>روی ترا وصف اقل آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خط مشکین خلل اندر میار</p></div>
<div class="m2"><p>زانکه نپوشد بخلل آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باده فراز آر چو خون حمل</p></div>
<div class="m2"><p>کامد زی برج حمل آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برج حمل خانه و کوی تو شد</p></div>
<div class="m2"><p>طلعت دهقان اجل آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شهره نصیرالدین صدری که هست</p></div>
<div class="m2"><p>بر فلک دین و دول آفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن شرف دولت عالی که هست</p></div>
<div class="m2"><p>رایش بی هیچ علل آفتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر که ورا دید نشسته بصدر</p></div>
<div class="m2"><p>گوید او هست لعل آفتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صبحدمان از قبل خدمتش</p></div>
<div class="m2"><p>سازد چون ماه عجل آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر در دربارش بوسه دهد</p></div>
<div class="m2"><p>گشت چو پیدا ز قلل آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اختر مسعود ورا بر فلک</p></div>
<div class="m2"><p>هست ز خدام و خول آفتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از پی اسباب تنعم سزد</p></div>
<div class="m2"><p>مجلس او چرخ و حمل آفتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گشت سپروار و ازو دفع کرد</p></div>
<div class="m2"><p>تیر نحوسات زحل آفتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای شده در صدر بزرگان عصر</p></div>
<div class="m2"><p>پیدا چون بر سر تل آفتاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نور تو پنهان نشود تا نشد</p></div>
<div class="m2"><p>پنهان در زیر بغل آفتاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عدل تو برداشت ستم از جهان</p></div>
<div class="m2"><p>چون ز گیا قطره طل آفتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جمله بخرج کف راد تو داد</p></div>
<div class="m2"><p>آنچه بکان کرد عمل آفتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حیلت خصم تو نپوشد ترا</p></div>
<div class="m2"><p>زانکه نپوشد بحیل آفتاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با تن خصم تو کند خشم تو</p></div>
<div class="m2"><p>آنچه کند بر سر کل آفتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر سر تو جز بسعادت نتافت</p></div>
<div class="m2"><p>از ره تقدیر ازل آفتاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا که سپروار مقابل کند</p></div>
<div class="m2"><p>خود را با تیغ جبل آفتاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در پی اعدای تو بادا بکین</p></div>
<div class="m2"><p>کرده بکف تیغ اجل آفتاب</p></div></div>