---
title: >-
    شمارهٔ ۵۷ - در مدح فخر الدین احمد
---
# شمارهٔ ۵۷ - در مدح فخر الدین احمد

<div class="b" id="bn1"><div class="m1"><p>ای بت گلرنگ روی آن باده گلگون بیار</p></div>
<div class="m2"><p>کز فروغ او شود گلرنگ روی باده خوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده ای کز وی خورد بیمار گردد تندرست</p></div>
<div class="m2"><p>باده ای کز وی خورد بیکار بازآید بکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده ای کز وی جدا گردد بخیل از رادمرد</p></div>
<div class="m2"><p>باده ای کز وی شود پیدا حکیم از بادسار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده ای چون گوهر رخشان که اندر نیک و بد</p></div>
<div class="m2"><p>گوهر پنهان مردم گردد از وی آشکار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده سوری بکف گیر ای بت گلرنگ روی</p></div>
<div class="m2"><p>آن گل سوری که بر سرو روان آید ببار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در میان انجمن بخرام و ساقی باش از آنک</p></div>
<div class="m2"><p>باده سوری زمرد گلرخ آید خوشگوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی از سرور روان خیزد چو کرد آغاز سور</p></div>
<div class="m2"><p>صاحب اقبالی شهی زاولاد صاحب ذوالفقار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاه بی معزولی از ملک شرف اشرف که هست</p></div>
<div class="m2"><p>تا ابد این ملک را در خاندان او قرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرد خویشی با بزرگی کز بزرگان جهان</p></div>
<div class="m2"><p>خواند بتوان خسرو صاحبقران روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فخر دین احمد که تا با مصطفی خیزد بحشر</p></div>
<div class="m2"><p>خویشتن را ساخت از اولاد او خویش و تبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که او خویش و تبار آل پیغمبر بود</p></div>
<div class="m2"><p>در دو گیتی باشد ایمن از خسار و از تبار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مهتران دین و دنیا بر مراد یکدگر</p></div>
<div class="m2"><p>دین و دنیا را گرفتند این و آن اندر کنار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فخر دین و اشرف از خویشی بیاری آمدند</p></div>
<div class="m2"><p>زین چه به باشد که گرد یار خویش و خویش یار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای شه آل نبی رایات شادی بر فراز</p></div>
<div class="m2"><p>تا هواخواهان تو دل هدیه آرند و نثار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تیره ماه آمد بخدمت تا کند در باغها</p></div>
<div class="m2"><p>شاخساران را بروز سور تو دینار بار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دست نقاشان چین و کله بندان ساختند</p></div>
<div class="m2"><p>در سرای تو بهاری خرم از نقش و نگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تاج صاحبدولتی از بهر سورت شد پدید</p></div>
<div class="m2"><p>تیر مه در باغ نو وندر سرایت نوبهار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا بهار از تیر مه یک فصل بودی در میان</p></div>
<div class="m2"><p>در سر او باغ باشد هر دو در یک فصل یار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای نکوخواهان تو پیوسته شادان و عزیز</p></div>
<div class="m2"><p>وی بداندیشان تو همواره اندر خار خار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حاصل آمد فخر دینرا و ترا از یکدگر</p></div>
<div class="m2"><p>صد هزاران عز بی ذل فخر هم بی هیچ عار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خاندان پاک ختم انبیا را بی خلاف</p></div>
<div class="m2"><p>عز و جاه و فخر و سنگ است از تو تا روز شمار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برخور از فرزند زیبا اظهر اشرف نسب</p></div>
<div class="m2"><p>ای شه سادات اشرف سیرت اظهر شعار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مقتدای مشرق و مغرب بجاه و سروری</p></div>
<div class="m2"><p>اطهر و اشرف بدند از جمع سادات کبار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون تو باشی اظهر و اشرف بود فرزند تو</p></div>
<div class="m2"><p>نام نیک و نسبت پاک از تو ماند یادگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا بود ملک شرف باقی بر آل مصطفی</p></div>
<div class="m2"><p>کاندرین شرکت ندارد هیچ شه در روزگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خسرو ملک شرف بادی و پیش و پس ترا</p></div>
<div class="m2"><p>لشکری زال نبی فرمانبر و طاعت گذار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>امت جد تو پیش تخت تو از طبع خویش</p></div>
<div class="m2"><p>چون غلامان پادشاهان را مطیع و بردبار</p></div></div>