---
title: >-
    شمارهٔ ۸۵ - در مدح وزیری گوید
---
# شمارهٔ ۸۵ - در مدح وزیری گوید

<div class="b" id="bn1"><div class="m1"><p>وزارتست باهل وزارت آمده باز</p></div>
<div class="m2"><p>سرای دولت میرانیان شده در باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظام دین شه میرانیان که بر شاهان</p></div>
<div class="m2"><p>خجسته فال تراست از همای و از شهباز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چهار سال چو شهباز از آشیانه ملک</p></div>
<div class="m2"><p>بهر هوائی پرواز کرد و آمد باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بمستقر و سرا و سریر و مسند خویش</p></div>
<div class="m2"><p>بدان نسق که بمعشوق عاشق دلباز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفت صدر وزارت بفرخی تا کرد</p></div>
<div class="m2"><p>همای دولت و پیروزی از سرش پرواز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عدم شود ستم از کلک عدل گستر او</p></div>
<div class="m2"><p>چو شد منادی انصاف او بلند آواز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سیر کوفته دارد سر ستم پیشه</p></div>
<div class="m2"><p>خبر دهد ستم اندیش را زنرخ پیاز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شود بکلک وی آراسته ممالک شرق</p></div>
<div class="m2"><p>برند نامه انصاف او بشام و حجاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شمع دولت او برفروخت بفروزد</p></div>
<div class="m2"><p>بنور عدلش گیتی همه نشیب و فراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایا حسود تو از جاه تو بغیرت و رشک</p></div>
<div class="m2"><p>زرشک تو سرانگشت خود گزیده بگاز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شمع باد بداندیش تو ز شب تا روز</p></div>
<div class="m2"><p>بگاز داده سر از سوز و تن زسر بگداز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیاز بود چنین ملک را بچون تو وزیر</p></div>
<div class="m2"><p>در آرزوی تو میبود روزگار دراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بشعر تهنیت این ملکرا کنم نه ترا</p></div>
<div class="m2"><p>که ملک داشت بشغل وزارت تو نیاز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به بی نیازی ایزد اگر خورم سوگند</p></div>
<div class="m2"><p>که نیست همچو منی شاعر سخن پرداز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خلاف باشد و اندازه من آن نبود</p></div>
<div class="m2"><p>که نیستم چو حکیمان وقت حکم انداز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدیهه حسبی گفتم بوسع طاقت طبع</p></div>
<div class="m2"><p>ضعیف و سست بانجام بردم از آغاز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر بد آمد اگر نیک هیچ حاجت نیست</p></div>
<div class="m2"><p>ترا بمدحت من چون خدایرا بنماز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من و دعا و نماز و ثنای مجلس تو</p></div>
<div class="m2"><p>چو نیست بهتر ازین سه برین کنم ایجاز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همیشه تا که نبینند آز را سیری</p></div>
<div class="m2"><p>بقات بادا بدانکه سیر گردد آز</p></div></div>