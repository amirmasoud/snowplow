---
title: >-
    شمارهٔ ۱۶۲ - در مدح سعدالدین
---
# شمارهٔ ۱۶۲ - در مدح سعدالدین

<div class="b" id="bn1"><div class="m1"><p>سعد دین مدح خواجه مستو</p></div>
<div class="m2"><p>فی شنیدی و در دل آمد سو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دای آن بر طریق و کردی تح</p></div>
<div class="m2"><p>سین بران وزن شعر و قافیه مو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قوف تا کرد بهر ذکر توخا</p></div>
<div class="m2"><p>طر من زان بسبق مدح تو مو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زون زهی مهتر سخی سخن</p></div>
<div class="m2"><p>دان که آورد سیر اختر و دو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان چرخ از گذشت صاحب و سح</p></div>
<div class="m2"><p>بان وائل چو تو بدانش و دو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لت و مردی و مردمی زاکا</p></div>
<div class="m2"><p>بر اخسیک آنکه منشاء و مو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لد اسلاف و اصل گوهر پا</p></div>
<div class="m2"><p>ک تو از خطه ویست وز او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لاد دهقانی و عزیز که فر</p></div>
<div class="m2"><p>غانیان بنده اند و چاکر مو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لای آن گوهر شریف تو آ</p></div>
<div class="m2"><p>زاده را بندگی کنند بطو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ع و برغبت چو تربیت ز تو یا</p></div>
<div class="m2"><p>بند ایشان و ما و با هر قو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می که در عالمست با وی عل</p></div>
<div class="m2"><p>می است در حق آن تو پائی تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فیق احسان و مکرمت چه بدس</p></div>
<div class="m2"><p>تت جواد عطاده و به تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قیع کلکی که مشک را برکا</p></div>
<div class="m2"><p>فور نقش افکند چو بر رخ حو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>را سر زلف جعد جعده و مر</p></div>
<div class="m2"><p>غول و زان نقش شاعران را تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جیه زر است و سیم و اطلس و اک</p></div>
<div class="m2"><p>سون و دمیاطی و عنابی و تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زی و کتان و دق و فرش و اوا</p></div>
<div class="m2"><p>نی و دریای عیش و عمر برو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود ترتیب در مدیح تو فک</p></div>
<div class="m2"><p>رت یکی کرده با عروضی دو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>می کهخ تا آفرین و مدح تو گو</p></div>
<div class="m2"><p>یند ازین نوع یکدیگر را فو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ری که دانند مر بدین سر من</p></div>
<div class="m2"><p>رعه نثر کار نظم درو</p></div></div>