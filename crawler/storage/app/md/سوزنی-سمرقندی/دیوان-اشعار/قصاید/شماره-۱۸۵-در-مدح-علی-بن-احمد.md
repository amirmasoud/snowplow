---
title: >-
    شمارهٔ ۱۸۵ - در مدح علی بن احمد
---
# شمارهٔ ۱۸۵ - در مدح علی بن احمد

<div class="b" id="bn1"><div class="m1"><p>چو تیر غمزه بناز و کرشمه اندازی</p></div>
<div class="m2"><p>نشانه از دل مسکین من کن ای غازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخست با تو بالبازی اندر آمده ام</p></div>
<div class="m2"><p>چو دل نماند تن در دهم بجانبازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا چو جان بباری شد است قربانت</p></div>
<div class="m2"><p>بود همیشه رو اگر بجان من تازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهم بغمزه زهراب داده خسته کنی</p></div>
<div class="m2"><p>گهم ببوسه بیجاده مرهمی سازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو هیچ زخم تو ایدوست بی نوازش نیست</p></div>
<div class="m2"><p>مرا بغمزه بزن تا ببوسه بنوازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار عاشق داری و من هزار و یکم</p></div>
<div class="m2"><p>بمن نیائی و زانان بمن نپردازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یگانه ای بنکوئی یگانه ایم بعشق</p></div>
<div class="m2"><p>همیخوریم غم عشق تو بانبازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا بعشق تو طشت ای پسر زبام افتاد</p></div>
<div class="m2"><p>چه راز ماند طشتی بدین خوش آوازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش است عشق تو گو آشکار یا راز است</p></div>
<div class="m2"><p>خوش است با توام از آشکار با رازی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بچاره سازی با خصم تو همی کوشم</p></div>
<div class="m2"><p>که مروزی را افتاده کار با رازی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپر نیفکنم از خصم تو همیکوشم</p></div>
<div class="m2"><p>که خصم نبود بی طاعتی و طنازی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو مشک عشق تو غماز من شد ای دل و جان</p></div>
<div class="m2"><p>بدیع نبود از عشق و مشک غمازی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خبر بمجلس ممدوح من رسید که تو</p></div>
<div class="m2"><p>چگونه بر دل مداح او همی تازی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپهر فخر و علا افتخار دین که بدو</p></div>
<div class="m2"><p>کند تفاخر دین پیمبر تازی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز چرخ صید کند نسر طایر و واقع</p></div>
<div class="m2"><p>عقار غمت او از بلند پروازی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ایا بزرگ و سرافراز مهتری کت نیست</p></div>
<div class="m2"><p>نه در بزرگی یار و نه در سرافرازی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نسیم خلق تو از آهوان تاتاریست</p></div>
<div class="m2"><p>سموم خشم تو از کژدمان اهوازی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بطبع پاک زیادت کننده خردی</p></div>
<div class="m2"><p>بکف راد زین بر کننده آزی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مهیب تر ز هژبری بروز زرمی و باز</p></div>
<div class="m2"><p>لطیف تر ز غزالی ببزم بگمازی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیاز دیده بروی تو باز کرد از آنک</p></div>
<div class="m2"><p>نیاز دیده نئی پروریده نازی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بنیک نامی مشهور گشتی و معروف</p></div>
<div class="m2"><p>از آنکه با کف رادی و با در بازی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخای حاتم پیش سخای تو زفت است</p></div>
<div class="m2"><p>نبرد رستم پیش نبرد تو بازی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همیشه غالب و قاهر بوی باعدا بر</p></div>
<div class="m2"><p>مگر که اعدا کبکند و تو مگر بازی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بمدح تو سخن من بهفتمین گردون</p></div>
<div class="m2"><p>رسید بی رسن از چاه هفتصد بازی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هزار سیخ بیکدست اگر بدست آری</p></div>
<div class="m2"><p>بدست دیگر هم در زمان براندازی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بنزد تو همه اعزاز اهل دانش راست</p></div>
<div class="m2"><p>که اهل دانشی و مستحق اعزازی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هزار سال ترا عمر باد در اعزاز</p></div>
<div class="m2"><p>که گر شمار غلط گردد از سر آغازی</p></div></div>