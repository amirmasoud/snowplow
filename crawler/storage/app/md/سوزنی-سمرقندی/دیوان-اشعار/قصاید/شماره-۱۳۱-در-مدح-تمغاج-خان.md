---
title: >-
    شمارهٔ ۱۳۱ - در مدح تمغاج خان
---
# شمارهٔ ۱۳۱ - در مدح تمغاج خان

<div class="b" id="bn1"><div class="m1"><p>ملک مانند گوی بود بمیدان</p></div>
<div class="m2"><p>آمده از هر گروه در خم چوگان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه بچوگان کوی ملک ربودن</p></div>
<div class="m2"><p>کوی ز یال یلان ربود بمیدان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوی ربایان بدشت معرکه دادند</p></div>
<div class="m2"><p>گوی بچوگان شه ز گوی گریبان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تن بی جان نمود حضرت بی شاه</p></div>
<div class="m2"><p>شاه خرامید و بهره یافت تن از جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منبر و مهر و نگین و سکه تجمل</p></div>
<div class="m2"><p>یافت ز القاب و نام و کنیت خاقان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه جهان رکن دین و دنیا مسعود</p></div>
<div class="m2"><p>آنکه نزاید چنو ز انجم و ارکان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه حسن نسبت و حسین سیر و خله</p></div>
<div class="m2"><p>تابع و مأمور حق بعدل و باحسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالی تمغاج خان عالم عادل</p></div>
<div class="m2"><p>چشمه خورشید عدل و سایه یزدان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو اسلام کز حمیت دین است</p></div>
<div class="m2"><p>حامی صدبار صد هزار مسلمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست بدنیا چو ظل عرش بعقبی</p></div>
<div class="m2"><p>سایه چترش پناه . . . ایمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پدر کامگار خود ملک شرق</p></div>
<div class="m2"><p>شاه جهان داور دلیر قراخان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا پسر آبتین بگوهر عالیست</p></div>
<div class="m2"><p>خسرو و مالکرقاب و نافذ فرمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وز پسر آبتین خلف بخلف شاه</p></div>
<div class="m2"><p>تا ملک آب و طین خلیفه کیهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای بسلاطین پر از شجاعت و مردی</p></div>
<div class="m2"><p>قاهر و غالب چو بر رعیت سلطان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تاج فریدون ترا و تو نه فریدون</p></div>
<div class="m2"><p>ملک سلیمان ترا و تو نه سلیمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ناظر خورشید رخ بچشم ستاره</p></div>
<div class="m2"><p>چون تو نه بیند جهان ستان و جهانبان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زر کند از خاک تیره تابش خورشید</p></div>
<div class="m2"><p>تا کف رادت کند ببزم زرافشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا بصف رزم سر فشانی بهرام</p></div>
<div class="m2"><p>تیغ فسان کرده برکشد ز دل کان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زرگر و آهنگر تواند دو اختر</p></div>
<div class="m2"><p>بزم ترا این بکار و رزم ترا آن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تیغ گهردار تست چون ز زبرجد</p></div>
<div class="m2"><p>لوح مرصع شده بلؤلؤ عمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لوح زبرجد درخت مرجان سازی</p></div>
<div class="m2"><p>لؤلؤعمان کنی چو لاله نعمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از همه شاهان تراست آنکه بهیجا</p></div>
<div class="m2"><p>لؤلؤ و لالا کنی زبرجد و مرجان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در صف هیجا ز میخ نعل مهلل</p></div>
<div class="m2"><p>باره سندان سمت بسنبد سندان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پای چو اندر رکاب یکران آری</p></div>
<div class="m2"><p>نعل بیفتد ز آتش تک یکران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>داغ کنی در شکارگه بتکاپوی</p></div>
<div class="m2"><p>گوره خران را بنعل یکران یکران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خفته کمان تراست قبضه ز نصرت</p></div>
<div class="m2"><p>راست خدنگ ترا ظفر پر و پیکان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از زه و زاغ کمان تست پس قاف</p></div>
<div class="m2"><p>عنقا همچون تذرو و در خس پنهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صر صر پر خدنگ عنقا صیدت</p></div>
<div class="m2"><p>برکند از جای قاف را ز بیابان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سایه عدل تو پادشاه همایون</p></div>
<div class="m2"><p>ظل همایست بر ممالک توران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حضرت جلت که دار ملک تو شاه است</p></div>
<div class="m2"><p>جنت دنیاتس بلکه جنت رضوان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رضوان پروردگان رعیت و در وی</p></div>
<div class="m2"><p>جور و ستم نی بقدر نیم سپندان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عدل تو بر بندگان ز ایزد فضل است</p></div>
<div class="m2"><p>فضل ورا بر تمام گفتن نتوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از شعرائی که مدح سید گفتند</p></div>
<div class="m2"><p>کس نبد ای شاه خوب شعر چو حسان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مدحت حسان ستوده گشت بسید</p></div>
<div class="m2"><p>مدحت مار ابحق خویش همان دان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کسوت مدح تو پادشاه جوانبخت</p></div>
<div class="m2"><p>پیر سخن بخیه زد بسوزن کمسان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز اهل سخن تا بشاهنامه طوسی</p></div>
<div class="m2"><p>خوانده شود داستان رستم دستان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>باد کمین بنده تو در صف مردی</p></div>
<div class="m2"><p>رستم دستان بزور تن نه بدستان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ملک تو بستان آفرین خدای است</p></div>
<div class="m2"><p>عدل ترا اعتدال سرو ببستان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فرق سرت سبز باد همچو سر سرو</p></div>
<div class="m2"><p>تا که سر سرو سبز باشد یکسان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا بدم صور چرخ اخضر و اختر</p></div>
<div class="m2"><p>بسته بسر سبزی تو بیعت و پیمان</p></div></div>