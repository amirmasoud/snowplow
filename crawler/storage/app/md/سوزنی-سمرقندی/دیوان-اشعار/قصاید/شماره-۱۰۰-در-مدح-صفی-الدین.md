---
title: >-
    شمارهٔ ۱۰۰ - در مدح صفی الدین
---
# شمارهٔ ۱۰۰ - در مدح صفی الدین

<div class="b" id="bn1"><div class="m1"><p>هلال روزه میمون لقای فرخ فال</p></div>
<div class="m2"><p>نمود روی ز گردون نیل فام چو نال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خمیده قامت و خدمت نموده بر گردون</p></div>
<div class="m2"><p>ز روی قبله بدرگاه آفتاب جلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اجل صاحب عادل که مثل او گیتی</p></div>
<div class="m2"><p>پدید نارد و ناورده صاحب اقبال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کبیر عالم کاندر صلاح ذات ویست</p></div>
<div class="m2"><p>صلاح خلق کبیر مهمین متعال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفی دولت عالی معین ملت حق</p></div>
<div class="m2"><p>وزیر نیک پی نیک رسم نیک خصال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ثبات علم و عمل هست بر رعیت شاه</p></div>
<div class="m2"><p>بسیرت علما و بصورت عمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو کارها به نیتهاست گفت صاحب شرع</p></div>
<div class="m2"><p>وراست پر ز نکوئی جریده اعمال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد است مشفق بر عامه رعیت شاه</p></div>
<div class="m2"><p>چو مادر و پدر خوب مهر بر اطفال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشاده کرد در داد و بست دست ستم</p></div>
<div class="m2"><p>یکی ز بهر ثواب و یکی ز بیم وبال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز عدل او نه عجب باشد ار بکوه و بدشت</p></div>
<div class="m2"><p>پلنگ و یوز شود پاسبان عزم و غزال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنوک کلک بگسترد عدل در عالم</p></div>
<div class="m2"><p>بر آن قیاس که همنام او بزخم دوال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بباب عدل ز همنام او چو در گذری</p></div>
<div class="m2"><p>کسی نیابی او را دگر نظیر و همال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سران دهر و بزرگان عصر او را نیست</p></div>
<div class="m2"><p>بجز بخدمت درگاه او مآب و مآل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رسیده اند جهانی ز خدمت در او</p></div>
<div class="m2"><p>بآب و حشمت و جاه و بناز و نعمت و مال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وفا شود ز کف راد او بجود و سخا</p></div>
<div class="m2"><p>اگر بجان گرامی ازو کنند سوآل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برد ز جود کف او کمینه سائل او</p></div>
<div class="m2"><p>ببدره زر عیار و بکیسه سیم حلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همای جاه تو پرواز کرد بر سر خلق</p></div>
<div class="m2"><p>کسی که سایه او یافت رسته شد زاهوال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بزیر سایه او باشد این جهان یکسر</p></div>
<div class="m2"><p>چو آن همای همایون بگسترد پر و بال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ایا همایون صدری که فر طلعت تو</p></div>
<div class="m2"><p>به از همای همایون بود بفر و بفال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زمانه نیک سگالیست پادشاهی را</p></div>
<div class="m2"><p>که هست او را چون تو وزیر نیک سگال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ترا بصاحب ری گر کسی قیاس کند</p></div>
<div class="m2"><p>ورا بود نه ترا اندرین قیاس جمال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر که صاحب ری بودی اندرین ایام</p></div>
<div class="m2"><p>کجا تو باشی او باشدی بصف تعال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کمال صاحب ری هرگز اندرین نرسد</p></div>
<div class="m2"><p>در آنچه هست سزای تو نیست او بکمال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>محل و قدر ترا کردگار کرد فزون</p></div>
<div class="m2"><p>هرآنچه کرد و کند کردگار نیست محال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر آسمان بزرگی و بوستان سری</p></div>
<div class="m2"><p>تو بدر و سروی و دیگر کسان هلال و خلال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنانکه بدر بتابد ز آسمان تو بتاب</p></div>
<div class="m2"><p>چنانکه سرو ببالد ببوستان تو ببال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حسود جاه تو بادا ز بار محنت و غم</p></div>
<div class="m2"><p>نحیف تن چو خلال و خمیده قد چو هلال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همیشه تا مه روزه است بهترین شهور</p></div>
<div class="m2"><p>هماره تا شب قدر است بهترین لیال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بقدر باد ز عمر تو هر شبی شب قدر</p></div>
<div class="m2"><p>بخیر همچو مه روزه هر مه تو ز سال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هزار سال ترا عمر باد و هر روزی</p></div>
<div class="m2"><p>ز سال عمر تو چون روز اول شوال</p></div></div>