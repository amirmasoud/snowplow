---
title: >-
    شمارهٔ ۱۲۱ - در مدح مؤید الدین
---
# شمارهٔ ۱۲۱ - در مدح مؤید الدین

<div class="b" id="bn1"><div class="m1"><p>جز آینه که کند گلرخا ترا معلوم</p></div>
<div class="m2"><p>که از حبش حشم آرد بدست کردن روم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طلیعه آید و آنگه سپاه بر اثرش</p></div>
<div class="m2"><p>پدید خواهد گشتن حقیقت از موهوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من آن نگویم اگر کس بر غم من گوید</p></div>
<div class="m2"><p>زهی سپاه بنفرین خهی طلیعه شوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بچهره بودی محسود نیکوان ختا</p></div>
<div class="m2"><p>خط آمده است که محسود را کند مرحوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خطی چو دایره اندر کشی و پنداری</p></div>
<div class="m2"><p>خط تو دایره عصمت است و تو معصوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل طریست رخت خط بنفشه طبری</p></div>
<div class="m2"><p>رقم بنفشه و گلبرگ ازو شده مرقوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من از خط تو نخواهم بخط شد ار بمثل</p></div>
<div class="m2"><p>برآید از گلبرگ کامگار تو کوم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر آن نهادم کز لعل نوش پاسخ تو</p></div>
<div class="m2"><p>بجای بوسه برآید زمرد مسموم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببوسه سخت گمانی ندارم از تو طمع</p></div>
<div class="m2"><p>وگر گمان سپهر آیدت کمان لزوم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه از لب تو سزد هیچ عاشقی مأیوس</p></div>
<div class="m2"><p>نه از مؤید دین هیچ سائلی محروم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان مجد و معالی مؤید بن جمال</p></div>
<div class="m2"><p>که جزو علم ویست از زمانه کل علوم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میان اهل زمان هیچگونه دانش نیست</p></div>
<div class="m2"><p>که آن بخاطر او مشکل است و نامفهوم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میان انجمن اهل فضل و اهل هنر</p></div>
<div class="m2"><p>بود چو بدر درخشنده در میان نجوم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایا کریم نژادی که تا شدی پیدا</p></div>
<div class="m2"><p>ز جود تو بجهان نام بخل شد معدوم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آنکه موم دلی در سخا بمهر سؤال</p></div>
<div class="m2"><p>بمهر مهر تو آهن دلان شدند چو موم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو ز آشیانه باز سپید خاسته ای</p></div>
<div class="m2"><p>ز باز خانه نپرد بهیچ خالی بوم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نظیر تو ز کریمان بدهر پیدا نیست</p></div>
<div class="m2"><p>بهیچ شهر و نواحی بهیچ برزن و بوم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سخاوت و کرم و جود و مردمی هنر</p></div>
<div class="m2"><p>ز خانواده تو شد نیام تو توم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جمال دین پدر خویش را همی مانی</p></div>
<div class="m2"><p>ستوده سیرت آیین و شأن و فعل و رسوم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه خصال تو و رسم تست نامعیوب</p></div>
<div class="m2"><p>همه نهاد تو و فعل تست نامذموم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سران عصر ترا مادحند و تو ممدوح</p></div>
<div class="m2"><p>مهان دهر ترا خادمند و تو مخدوم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخن که جز بمدیح تو نظم کرده شود</p></div>
<div class="m2"><p>سخن سرای بود ظالم و سخن مظلوم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزرگوارا دانی که بنده را هر سال</p></div>
<div class="m2"><p>بود رسومی از بذل وجود تو مرسوم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز سال پنج مه اندر گذشت و عیب منست</p></div>
<div class="m2"><p>که قصه رفع نکردم چو کهتران خدوم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خطی نویس بسوی وکیل خاصه خویش</p></div>
<div class="m2"><p>علی الخصوص بنام رهی بدن معلوم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر چه لؤلؤ منشور باشد آن ببها</p></div>
<div class="m2"><p>ز طبع بنده بها گیر لؤلؤ منظوم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نمیشه تا غم و شادی و کام و ناکامی است</p></div>
<div class="m2"><p>بحکم یزدان بر بندگان او محکوم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بقای عمر تو بادا بکام دل جاوید</p></div>
<div class="m2"><p>دل ولی تو شاد و دل عدو مغموم</p></div></div>