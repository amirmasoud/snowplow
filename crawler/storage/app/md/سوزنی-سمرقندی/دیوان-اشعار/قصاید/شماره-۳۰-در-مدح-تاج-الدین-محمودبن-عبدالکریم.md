---
title: >-
    شمارهٔ ۳۰ - در مدح تاج الدین محمودبن عبدالکریم
---
# شمارهٔ ۳۰ - در مدح تاج الدین محمودبن عبدالکریم

<div class="b" id="bn1"><div class="m1"><p>تاج دین محمودبن عبدالکریم است آنکه هست</p></div>
<div class="m2"><p>از می احسان او گیتی پر از هشیار روست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاحب دیوان استیفا که اهل فضل را</p></div>
<div class="m2"><p>اندر او اهلیت صاحب قرانی بود و هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دوات کله گیسوی منیر افسر بکلک</p></div>
<div class="m2"><p>بر سر اهل هنر افسر نهاد و کله بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون برآرد ماهی زرین نقش انگیز او</p></div>
<div class="m2"><p>قطره از قلزم قطران بقدر نیم شست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر بساط سیمگون از دست دریا جود او</p></div>
<div class="m2"><p>نقش مشک آثار خیزد دام دام و شست شست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمرو و زید عصر دل خستند و در بستند بخل</p></div>
<div class="m2"><p>سائلان و زائران را پشت خست و دل شکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاج دین در عمر خود روزی چو عمرو و زید عصر</p></div>
<div class="m2"><p>زائری را در نبست و سائلی را دل نخست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رسم و آئین بخیلی جود او منسوخ کرد</p></div>
<div class="m2"><p>شد یقین کان رسم و آئین تباه است و تبست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همتی دارد چنان عالی که چرخ برترین</p></div>
<div class="m2"><p>با فرودین پایگاه همتش دونست و نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بچشم همت خود بنگرد بر دست خویش</p></div>
<div class="m2"><p>آید اندر چشم او چون بر سخا بگشاد دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اطلس رومی عبا زر نشابوری سرب</p></div>
<div class="m2"><p>در عمانی شبه یاقوت رمانی جمست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هست فتوی فتوت را قلم در دست او</p></div>
<div class="m2"><p>پاسخ فتوی نعم راند بجای لاولست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لفظ و او شیرینتری دعوی کند بر انگبین</p></div>
<div class="m2"><p>این کسی داند که داند انگبین را از کبست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر که اندر سایه اقبال او مسکن گرفت</p></div>
<div class="m2"><p>از سموم فاقه ادبار و محنت جست و رست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زانکه در کردار نیکش چشم بد را راه نیست</p></div>
<div class="m2"><p>بدسگال دولتش را دل درید و دیده خست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خلق ایزد را ازو شکر است و آزادی مدام</p></div>
<div class="m2"><p>همچنین باشد همی آزاده و ایزد پرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خوار باد و خسته دل بدخواه جاه و دولتش</p></div>
<div class="m2"><p>گر ببغداد اسپ و ری یا در تخارستان و بست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سوزنی در مدح وی با قافیت کشتی گرفت</p></div>
<div class="m2"><p>قاضیت شد نرم گردن گر چه توسن بود گست</p></div></div>