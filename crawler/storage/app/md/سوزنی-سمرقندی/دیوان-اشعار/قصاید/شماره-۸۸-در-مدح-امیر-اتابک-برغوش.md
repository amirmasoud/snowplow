---
title: >-
    شمارهٔ ۸۸ - در مدح امیر اتابک برغوش
---
# شمارهٔ ۸۸ - در مدح امیر اتابک برغوش

<div class="b" id="bn1"><div class="m1"><p>اندر آورد سپهر از ره تشریف بگوش</p></div>
<div class="m2"><p>حلقه بندگی میراتابک بر غوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ در گوش کشد حلقه فرمان ورا</p></div>
<div class="m2"><p>دهر مرغاشیه دولت او را بر دوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کله گوشه رسانید ز اقبال بچرخ</p></div>
<div class="m2"><p>داد اعدای ورا دست زمان مالش گوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیش بر دشمن او تلخ شد از گشت فلک</p></div>
<div class="m2"><p>اینت تلخی که کند عیش جهانی خوشنوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او شجاعی است که هنگام وغا روز نبرد</p></div>
<div class="m2"><p>نعره او ببرد شیر ژیان را از هوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیبت اهرمنان دارد اندر صف جنگ</p></div>
<div class="m2"><p>باز در صدر سران سیرت و سیمای سروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش او پای ندارد که سرافکنده بود</p></div>
<div class="m2"><p>دشمن حیله گر کینه کش دستان کوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش او دست نیارد که غنی گشته بود</p></div>
<div class="m2"><p>سائل عاجز درمانده دل خلقان پوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کمترین بنده او گر بخوهد روز دغا</p></div>
<div class="m2"><p>بر رخ وران هژبران بنهد داغ و دروش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنشاند بسر تیغ و به بازوی قوی</p></div>
<div class="m2"><p>هرکجا خاسته شد فتنه چو دریای بجوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلق از فتنه و بیداد خروشید و کنون</p></div>
<div class="m2"><p>کند از عدل همی فتنه و بیداد خروش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای خداوند اگر زنده بدی رستم زال</p></div>
<div class="m2"><p>داشتی فخر اگر بردی در پیش تو توش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با خداوندان در صدر بزرگی من</p></div>
<div class="m2"><p>باده نوش و طرب و لهو کن و مدح نیوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رامش و لهو گزین لاله رخان اندر پیش</p></div>
<div class="m2"><p>عشرت و عیش کن و سیمبران در آغوش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیده حاسد و بدخواه تو بادا همه سال</p></div>
<div class="m2"><p>خسته از خار عنا وز سر مژگان خون نوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روز ناآمده را تا که بود فرد انام</p></div>
<div class="m2"><p>تا بود نام شب و روز گذشته دی و دوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در شب و روز میاسای ز شادی و طرب</p></div>
<div class="m2"><p>نیمساعت مشو از نزهت و رامش خاموش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون سلیمان نبی فال تو فرخنده و باد</p></div>
<div class="m2"><p>زیر فرمان تو دیو و دد و انسان و وحوش</p></div></div>