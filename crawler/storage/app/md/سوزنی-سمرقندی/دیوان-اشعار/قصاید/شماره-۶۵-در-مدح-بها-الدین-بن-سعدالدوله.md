---
title: >-
    شمارهٔ ۶۵ - در مدح بهاء الدین بن سعدالدوله
---
# شمارهٔ ۶۵ - در مدح بهاء الدین بن سعدالدوله

<div class="b" id="bn1"><div class="m1"><p>آمد چنانکه کرد ستاره شمر شمار</p></div>
<div class="m2"><p>شاه ستارگان بحمل شهریار وار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بوستان بتابش شاه ستارگان</p></div>
<div class="m2"><p>بر شاخ آسمان گون آرد ستار بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بستان شود چنانکه ندانیش ز آسمان</p></div>
<div class="m2"><p>چون ابر گشت بر رخ بستان ستاره بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر شاخسار بستان بلبل نوازند</p></div>
<div class="m2"><p>نوی است خوش نوائی بلبل ز شاخسار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جویبار سرو ببالد ز بهر آن</p></div>
<div class="m2"><p>تا فاخته بنالد بر سرو جویبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی آب دیده بر طرف جویبار گل</p></div>
<div class="m2"><p>قمری غریو دارد بر جستجوی یار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنگام را محابا نبود مثل زنند</p></div>
<div class="m2"><p>تا آن مثل زدند شد از عاشقان قرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنگام گل رسید ز گلروی لعبتی</p></div>
<div class="m2"><p>بر بوسه رام گشته محابا مکن کنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش بر کنار گیر و نشان در کنار خویش</p></div>
<div class="m2"><p>مگذار کز کنار تو گیرد دمی کنار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خاک و خار و خاره باردیبهشت ماه</p></div>
<div class="m2"><p>روید بنفشه زار و سمن زار و لاله زار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اردیبهشت ماه بساقی کند ندا</p></div>
<div class="m2"><p>خیز ای بت بهشتی وان جام می بیار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا شهریار وار بدستوری خرد</p></div>
<div class="m2"><p>جام می از تو گیرد دستور شهریار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صدر کبیر عالم عادل بهاء دین</p></div>
<div class="m2"><p>آن هر حدیث او ببها در شاهوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دستور دادگستر و سلطان دادگر</p></div>
<div class="m2"><p>مسعود سعد ملکت و مسعود کامکار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرزند سعد دولت و فرزند سعد ملک</p></div>
<div class="m2"><p>چون جد و چون پدر شرف دوده و تبار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از دوده و تبار وی افکنده دور چرخ</p></div>
<div class="m2"><p>در دوده و تبار بداندیش او تبار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای صدر روزگار که در روزگار خویش</p></div>
<div class="m2"><p>نور دل گرامی و تاج سر کبار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیروزه گون سپهر بزیر نگین تست</p></div>
<div class="m2"><p>از دولت شهنشه پیروز روزگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>داری دو کف دو کفه شاهین مکرمت</p></div>
<div class="m2"><p>بخشندگان سیم حلال و زر عیار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اندر یمین تو بسخا بیعت و یمین</p></div>
<div class="m2"><p>خلق از یسار تو شده با عدت و یسار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در چشم تو که چشم بدان دور ازو سخن</p></div>
<div class="m2"><p>چون زر عزیز باشد و زر عزیز خوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آید بحاصل اهل سخن را بمدح تو</p></div>
<div class="m2"><p>آنرا که شعر باشد رسم و ره و شعار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نامی چنانکه در پس آن نام نیست ننگ</p></div>
<div class="m2"><p>فخری چنانکه در پس آن فخر نیست عار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در باغ عمر سوزنی ای صدر روز به</p></div>
<div class="m2"><p>هفتاد شد تموز و خزان و دی و بهار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون هفده سالگان نتواند نگاشتن</p></div>
<div class="m2"><p>بر روی کار نامه خود لعبت بهار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بسیار منت است ترا بر من از قیاس</p></div>
<div class="m2"><p>کانرا بعمرها نتوان بود حق گذار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از شکر نعمت تو ز پیری مقصرم</p></div>
<div class="m2"><p>کرد است باز بر تو شکر مرا شکار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا در شکارگاه بتان عاشقی بلب</p></div>
<div class="m2"><p>باشد شکر شکار چه پنهان چه آشکار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از بوسه گاه خوبان شکر شکار باش</p></div>
<div class="m2"><p>تا پیشگاه باشی و اقبال پیشکار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا در زبان تازی بستان بود بهشت</p></div>
<div class="m2"><p>نام هزار دستان در بوستان هزار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شاعر هزار بار ببستان مدح تو</p></div>
<div class="m2"><p>تا چون هزار دستان دستان زند هزار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سال بقای عمر تو پیش از ستاره باد</p></div>
<div class="m2"><p>صد بار زانکه کرد ستاره شمر شمار</p></div></div>