---
title: >-
    شمارهٔ ۷۸ - در مدح علی بن ذوالفقار
---
# شمارهٔ ۷۸ - در مدح علی بن ذوالفقار

<div class="b" id="bn1"><div class="m1"><p>آیین ابن علی است سخاوت چنانکه بود</p></div>
<div class="m2"><p>آیین ابن عم نبی سید البشر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ذوالفقار جود وی آهخته شد بدهر</p></div>
<div class="m2"><p>شد خون عمرو و عنتر و بخل از جهان بدر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای آنکه در زمانه باحسان و مردمی</p></div>
<div class="m2"><p>شد نام تو چو مردی هم نام تو سمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جنب رأی روشن و کف جواد تو</p></div>
<div class="m2"><p>خورشید کم ز ذره و دریا کم از شمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی بار شکر منت احسان و جود تو</p></div>
<div class="m2"><p>آزاده ای نیابی در کل بحر و بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در باغ مکرمت شجر همت تراست</p></div>
<div class="m2"><p>زاحسان وجود و برو سخا شاخ و برگ وبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکتن ز اهل فضل نیابی درین دیار</p></div>
<div class="m2"><p>زان باغ بی نصیبه و بی بهره زان شجر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بودند اهل حضرت جلت ز دیر باز</p></div>
<div class="m2"><p>از جاه و از جلالت تو باجمال و فر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بالین مهتران و سران آستان تست</p></div>
<div class="m2"><p>وز آفتاب تو بفلکشان رسیده سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خدمت تو آمده مخدوم پیشه گان</p></div>
<div class="m2"><p>بسته بصدر بار تو چون بندگان کمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وین بندگان نخشب مانده ز جاه تو</p></div>
<div class="m2"><p>محروم چون پیمبر کنعانی از پسر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در هجر تو بجان یکایک ضرر رسید</p></div>
<div class="m2"><p>چون آمدی بنفع بدل شد همه ضرر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد نخشب از جمال تو یار دگر چنانک</p></div>
<div class="m2"><p>گویند نخشب است این یا جنت دگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا ماه روزه و شب قدر است در جهان</p></div>
<div class="m2"><p>تا عید در رسد چو مه روزه شد بسر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا حشر ماه صوم و شب قدر و روز عید</p></div>
<div class="m2"><p>بر تو بخیر باد و بر اعدای تو به شر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چندان بزی که از عدد سال عمر تو</p></div>
<div class="m2"><p>عاجز بود خواطر و حیران شود فکر</p></div></div>