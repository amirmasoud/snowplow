---
title: >-
    شمارهٔ ۱۵۹ - در مدح فخرالدین احمد
---
# شمارهٔ ۱۵۹ - در مدح فخرالدین احمد

<div class="b" id="bn1"><div class="m1"><p>سپاس از خداوند بیمثل و بیچون</p></div>
<div class="m2"><p>که با طالع سعد و با بخت میمون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خداوند را دیدم و روز بر من</p></div>
<div class="m2"><p>بدیدار میمون او شد همایون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اجل فخر دین احمد آن صدر دنیا</p></div>
<div class="m2"><p>که با قدر والاش گردون بود دون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجز بر مراد دل او نباشد</p></div>
<div class="m2"><p>نه سیر کواکب نه دوران گردون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعالم کس از امر او سر نتابد</p></div>
<div class="m2"><p>جز آن کاید از عالم عقل بیرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل و جان دولت برو هست عاشق</p></div>
<div class="m2"><p>چو بر روی لیلی دل و جان مجنون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه خلق مفتون نازند و نزهت</p></div>
<div class="m2"><p>بر او ناز و نزهت همه ساله مفتون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایا صدر دنیا که ارباب دین را</p></div>
<div class="m2"><p>چو دینست مهر تو در سینه مخزون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر آندل که مهر ترا شد خزانه</p></div>
<div class="m2"><p>ز مخزون خود شاد باشد نه محزون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ثنای تو فرض است بر اهل حکمت</p></div>
<div class="m2"><p>که بارند در مدح تو در مکنون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من آن در حکمت ندارم مهیا</p></div>
<div class="m2"><p>که عرضه دهم بر تو هزمان دگرگون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>توانم که در رشته مدحت آرم</p></div>
<div class="m2"><p>بصدر تو خس مهره ای چند موزون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ثنای تو طبع از بدیها بشوید</p></div>
<div class="m2"><p>بدانسان که جامه بشوید بصابون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ثنای تو ناگفته عینی است فاحش</p></div>
<div class="m2"><p>مبادا ثناگوی صدر تو مغبون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل حاسدانت شود خون ز حسرت</p></div>
<div class="m2"><p>چو آید بر ایشان ز قهرت شبیخون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شبیخون قهر تو که برندارد</p></div>
<div class="m2"><p>که از سهم بیم تو خارا شود خون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل حاسدانت ز احسان و برت</p></div>
<div class="m2"><p>بدست هوای تو گشته است مرهون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کریمانه بخشی و منت نخواهی</p></div>
<div class="m2"><p>عطای کریمان بود غیر ممنون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بهامون همی تا ددان را بود جا</p></div>
<div class="m2"><p>عمارات اعدای تو باد هامون</p></div></div>