---
title: >-
    شمارهٔ ۴۲ - در مدح دهقان سعدالدین
---
# شمارهٔ ۴۲ - در مدح دهقان سعدالدین

<div class="b" id="bn1"><div class="m1"><p>ای رخ و زلفت چنانک ماه بمشکین کمند</p></div>
<div class="m2"><p>ساخته نظاره گاه بر سر سرو بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منظر ماه منیر از بر سرو سهی</p></div>
<div class="m2"><p>طرفه و نادر بود خاصه بمشگین کمند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بت بادام چشم پسته دهان قند لب</p></div>
<div class="m2"><p>در غم عشق تو نیست چاره این مستمند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که شبی تا بروز وعده وصلم دهی</p></div>
<div class="m2"><p>وی که بنقلم نهی پسته و بادام و قند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی چو دو لعل تو هست گر بدو نیمه کنی</p></div>
<div class="m2"><p>از سر سرو سهی دانه نار خجند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی چو دو جزع تو هست گر بقلم برکشی</p></div>
<div class="m2"><p>زیر دو مشکین کمان نقش دو بادام کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گونه رخسار تست آتش افروخته</p></div>
<div class="m2"><p>خال سیاهت بر او سوخته تخم سپند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبح گر از چشم بد بر تو گزندی رسد</p></div>
<div class="m2"><p>خال و رخ تو ز دور دفع کنند آن گزند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست پسند من آنک از تو بوم با نصیب</p></div>
<div class="m2"><p>صحبت من بی نصاب بر تو بیاید پسند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند من اندر پیت زار بگریم همی</p></div>
<div class="m2"><p>طیره گری را تو زان گریه من خند خند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گریه من خنده شد چون بسعادت رسید</p></div>
<div class="m2"><p>گنج هنر سعد دین از سفر اورجند</p></div></div>