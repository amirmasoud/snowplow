---
title: >-
    شمارهٔ ۹۷ - در مدح نظام الدین
---
# شمارهٔ ۹۷ - در مدح نظام الدین

<div class="b" id="bn1"><div class="m1"><p>ای پایگاه قدر تو بر چرخ نیمرنگ</p></div>
<div class="m2"><p>دور ورا شتاب و بقای ترا درنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر شتاب اوست درنگ ترا مدد</p></div>
<div class="m2"><p>وندر درنگ تست شتاب ورا درنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیدا دو رنگ او دو زبان کلک تو کند</p></div>
<div class="m2"><p>چون بر بیاض روم نگارد سواد زنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیینه ضمیر تو اندر مقابله</p></div>
<div class="m2"><p>بزداید از دو آینه چرخ ریم زنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چرخ نیل رنگ چه نالند حاسدانت</p></div>
<div class="m2"><p>از سیر کلک تو شده با ناله و غرنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیر خدنگ شاه بکلک تو داد شغل</p></div>
<div class="m2"><p>تا راستی و راست روی گیرد از خدنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستوفی ممالک مشرق نظام دین</p></div>
<div class="m2"><p>کز کلک تست تیر فلکرا مسیر تنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تنگ شکر حدیث ترا بندگی کند</p></div>
<div class="m2"><p>کاندر عبارت تو شکر هست تنگ تنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون تو سوار فضل کجا در همه جهان</p></div>
<div class="m2"><p>بر مرکب کمال و هنر بسته تنگ تنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زرین سخن سواری از شعر عسجدیست</p></div>
<div class="m2"><p>بر دست چون سوار عنان سخن بچنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از مدحت تو سوزنی پیر شد جوان</p></div>
<div class="m2"><p>چون تیر کرد قد خمیده چو پشت چنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیکن بگرد عسجدی اندر کجا رسد</p></div>
<div class="m2"><p>چون هست ترکتازی او با خران لنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از تربیت نمودن تو مهتر کریم</p></div>
<div class="m2"><p>روباه شیر گردد و صعوه شود کلنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر شهسوار فضل که شد همعنان تو</p></div>
<div class="m2"><p>یابد بگرد گردن از الزام پالهنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در ذات تو نهاده ملک عز اسمه</p></div>
<div class="m2"><p>ذهن و ذکاو و فطنت و فرهنگ و هوش و هنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جستن نظیر تو بهتر پر مکابره است</p></div>
<div class="m2"><p>نایافته نمودن بر عقل شالهنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>امن تو است احسان نیکیت مکرمت</p></div>
<div class="m2"><p>نبود در آل میران آیین جز این دبنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>منت نهنگ دم زن دریا مردمی است</p></div>
<div class="m2"><p>در مردمی ندارد دریای تو نهنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>احسان تو بسان دبنگ است و سله است</p></div>
<div class="m2"><p>در خوشاب خوشه انگور بر دبنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در خدمت تو بودن فخر است و نیست عار</p></div>
<div class="m2"><p>وز مدحت تو گفتن نام است و نیست ننگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اهل ثنا و مدحت ارباب نظم و نثر</p></div>
<div class="m2"><p>مطلق توئی و نیست درین باب ریو و رنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بی حشمت نشان تو از هیأت فلک</p></div>
<div class="m2"><p>نکند نظر پلنگ بتربیع سوی رنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با اهل صلح صلح بتوقیع کلک تست</p></div>
<div class="m2"><p>برداشتن برأی تو از اهل جنگ جنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از لطف و سازگاری تو با سران عصر</p></div>
<div class="m2"><p>در دانه زلال همی داده چنگ چنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خشم تو آذرست و حسود تو نان خشک</p></div>
<div class="m2"><p>هر نان خشک را رسد از آذر آذرنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آید هر آنکه با تو کند استری بفعل</p></div>
<div class="m2"><p>در هاون هوان بضرورت چو استرنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در موسم بهار که دریا شود جهان</p></div>
<div class="m2"><p>بستان و باغ گردد همچون بهشت کنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در مجلس تو زورق باده رونده باد</p></div>
<div class="m2"><p>هر چند نیست عادت زورق روان بکنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا برکند حسود تو سبلت بدست خویش</p></div>
<div class="m2"><p>در سبلت حسود تو افتاده باد کنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو سیم و زر بآتش و سنگ امتحان کنند</p></div>
<div class="m2"><p>مردان کار دیده چه مصلح چه رند و شنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>منت پذیر باشی منت نهنده نی</p></div>
<div class="m2"><p>کز تو غنی شوند بروزی هزار دنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در راه عشق آتش رویان سنگدل</p></div>
<div class="m2"><p>سیم و زر امتحان کن و درباز هوش و هنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جد مرا ز هزل بباید نصیبه ای</p></div>
<div class="m2"><p>هر چند یک مزه نبود شهد با شرنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا بنگ و کوکنار بدیوانگی کشد</p></div>
<div class="m2"><p>دیوانه باد خصم تو چون کوکنار و بنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا باد ساریش بسر آید ادب نمای</p></div>
<div class="m2"><p>آن سرخ باد سار چو سر کفته بادرنگ</p></div></div>