---
title: >-
    شمارهٔ ۱۲۶ - در مدح نظام الدین
---
# شمارهٔ ۱۲۶ - در مدح نظام الدین

<div class="b" id="bn1"><div class="m1"><p>آمد از بستان دولت اهل حکمت را نسیم</p></div>
<div class="m2"><p>کز قدوم خواجه نوشه دولت آباد قدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شه نظام الدین میران منعم ارباب فضل</p></div>
<div class="m2"><p>در مقام صاحب عادل عمر نعم المقیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سپهر از مهر و ماه و انجم آرایش گرفت</p></div>
<div class="m2"><p>جای آن صدر کبیر از جاه این صدر کریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد بجنات النعیم آنصدر و ماند از وی سرای</p></div>
<div class="m2"><p>تا شد از فر نظام الدین چو جنات نعیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حکمت آرایان روشن رأی را عقل صحیح</p></div>
<div class="m2"><p>جز بدین درگاه ننماید صراط مستقیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کرا عقل صحیح است از امیران سخن</p></div>
<div class="m2"><p>در نظام الدین میران مدح او ناید سقیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاصه در دولت سرائی کاندر او مدحت سرای</p></div>
<div class="m2"><p>تنگ سیم اندوزد و بیرون شود با تنگ سیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاطر مدحت سرایان بحر دان سینه صدف</p></div>
<div class="m2"><p>مدحت صدر نظام الدین در او در یتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شغل دیوان حق ز باطل فرق کلک تو کند</p></div>
<div class="m2"><p>کلک ملک آرای چون فرق بشکافی دو نیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قاف تا قاف از کفایت ذره خورشید را</p></div>
<div class="m2"><p>در شمار آری و بنگاری بقاف و لام و میم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ار صیانت وز خیانت عاملان ملک را</p></div>
<div class="m2"><p>جوف کلک تست پنهان خانه امید و بیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بپیش کف راد تست از غایت جود و سخا</p></div>
<div class="m2"><p>در شبه دینار اکسون کسا اطلس گلیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در امان ایزدی از غرق و حرق روزگار</p></div>
<div class="m2"><p>همچو در آتش خلیل و همچو در دریا کلیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همچو خورشید از فلک روی زمین زرین کند</p></div>
<div class="m2"><p>گر بیفتد سایه دست تو بر دست لئیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر وزیر و میر و مستوفی مدیحی نظم داد</p></div>
<div class="m2"><p>سوزنی از خاطر دراک فیاض فهیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون وزیر و میر و مستوفی تو باشی کی بود</p></div>
<div class="m2"><p>مدحت آرای وزیر و میر و مستوفی ذمیم</p></div></div>