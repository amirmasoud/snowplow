---
title: >-
    شمارهٔ ۹۰ - در مدح علی بن احمد
---
# شمارهٔ ۹۰ - در مدح علی بن احمد

<div class="b" id="bn1"><div class="m1"><p>منم منم زده در دل ز عشق یار آتش</p></div>
<div class="m2"><p>گمان مبر که یک آتش که صدهزار آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نار شد دلم از عشق ناردان لب دوست</p></div>
<div class="m2"><p>میان دل همه چون دانه های نار آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آفریده بترسد ز آتش و دل من</p></div>
<div class="m2"><p>همی خوهد بدعا زآفریدگان آتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نه بر بره دیو است بیهده دل من</p></div>
<div class="m2"><p>چرا چو دیو کند خیره اختیار آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش است آتش عشق من و زین معنی</p></div>
<div class="m2"><p>همی کنم بدل خویش بر نثار آتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه دل قرار پذیرد نه در دل آتش عشق</p></div>
<div class="m2"><p>چه بی قرار دلست و چه بی قرار آتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آب دیده و تاب دلم از آنکه رخش</p></div>
<div class="m2"><p>چه آبدار گل است و چو تابدار آتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دیده و دل خود کسوتی همی پوشم</p></div>
<div class="m2"><p>چه کسوتی که بود پود آب و تار آتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بسکه از مژه بارم سرشک آتش گون</p></div>
<div class="m2"><p>گمان برند که دارم همه کنار آتش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلم نگارپرستی گرفت بر رخ دوست</p></div>
<div class="m2"><p>بود سزای پرستنده نگار آتش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اسیر عشق نگاری شدم بجان و بدل</p></div>
<div class="m2"><p>که عنبر است بدو زلف و دو عذار آتش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگار من چو سر زلف بر عذار زند</p></div>
<div class="m2"><p>زنند گوئی در تبت و تتار آتش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حدیث زلف و عذار و تتار و تبت را</p></div>
<div class="m2"><p>بر آب مانم و در زد بهر چهار آتش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حدیث خلق خداوندگار خود گویم</p></div>
<div class="m2"><p>که بوی مشک دهد نایدش بکار آتش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهان جود و سخا افتخار دین که کند</p></div>
<div class="m2"><p>ز بهر سوختن خصمش افتخار آتش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>علی که همچو خداوند ذوالفقار زند</p></div>
<div class="m2"><p>بجان و جسم عدو در ز ذوالفقار آتش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بزرگواری کز باد خشم و هیبت او</p></div>
<div class="m2"><p>فرو شود بدل خاک آب وار آتش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آیا سپهر معالی که از سیاست تو</p></div>
<div class="m2"><p>همی خوهد گه باشم تو زینهار آتش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز رشک همت عالیت هر زمان بنفیر</p></div>
<div class="m2"><p>سوی اثیر فرست همی شرار آتش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از آنکه تا بکف زرفشان تو ماند</p></div>
<div class="m2"><p>همی جدا کند از خود زر عیار آتش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وزآنکه تا نپزد خام دشمن تو</p></div>
<div class="m2"><p>بود در آهن و درسنگ استوار آتش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بهر سوختن خصم تو در آهن و سنگ</p></div>
<div class="m2"><p>اگر چه هست نهان گردد آشکار آتش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کسیکه گرد خود از حشمت تو دایره کرد</p></div>
<div class="m2"><p>تفی بدو نرسد گر همه دیار آتش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدولت تو سیاوخش وار برگذرد</p></div>
<div class="m2"><p>که خوی نیارد بر مرکب سوار آتش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگرم و سرد زمانش بیازماید چرخ</p></div>
<div class="m2"><p>چو بر یمین بودش آب و بر یسار آتش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر از تباری یکتن دم از خلاف تو زد</p></div>
<div class="m2"><p>درافکند بهمه دوده و تبار آتش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دم خلاف تو ناچیزشان کند بدمی</p></div>
<div class="m2"><p>برآن صفت که درافتد بمرغزار آتش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بزیر سایه سروی که دشمن تو نشست</p></div>
<div class="m2"><p>زند درخش دران سرو جویبار آتش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گل بهار که برمیدهد بدشمن تو</p></div>
<div class="m2"><p>چو خار گردد واندر فتد بخار آتش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زمانه دست حسود تو بشکند چو چنار</p></div>
<div class="m2"><p>کز او سخاوت ناید چو از چنار آتش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همیشه تا نبود باد و خاک را بجهان</p></div>
<div class="m2"><p>ز روی طبع جهان آب جفت و یار آتش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زباد ساری خصم تو باد رفته بخاک</p></div>
<div class="m2"><p>در آب دیده شده غرق و در کنار آتش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدانگهی که تو گلبرگ کامکار گری</p></div>
<div class="m2"><p>بر او فشانده چو گلبرگ کامکار آتش</p></div></div>