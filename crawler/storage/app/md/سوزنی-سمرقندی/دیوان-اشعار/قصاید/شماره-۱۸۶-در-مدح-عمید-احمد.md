---
title: >-
    شمارهٔ ۱۸۶ - در مدح عمید احمد
---
# شمارهٔ ۱۸۶ - در مدح عمید احمد

<div class="b" id="bn1"><div class="m1"><p>من ندارم باور ار گوئی که به زانسان پری</p></div>
<div class="m2"><p>روی آن زیبا پسر بین تا بود زینسان پری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جمال آن پسر بنگر که اندر روی او</p></div>
<div class="m2"><p>خیره ماند آدمی و عاجز و حیران پری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با پری گر گوی نیکوئی بمیدان بفکند</p></div>
<div class="m2"><p>گوی و چوگان بفکند بگریز از میدان پری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایکه اوصاف پری دانی جمال او به بین</p></div>
<div class="m2"><p>تا بود ماننده دیوار آن جانان پری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قامت چون سرو بستانست جانان مرا</p></div>
<div class="m2"><p>نیست از قامت چو سرو بوستان ایجان پری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سروچون ماند بقد آن نگارین بیشتر</p></div>
<div class="m2"><p>در میان سرو بستانی کند اوان پری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درو مرجانست دندان و لب جانان و نیست</p></div>
<div class="m2"><p>با لب و دندان همچون در و چون مرجان پری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوی سیمین دارد و چوگان مشکین آن پسر</p></div>
<div class="m2"><p>با چنین گوی و چنین چوگان کند جولان پری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درو مرجان لب و دندان او را هر زمان</p></div>
<div class="m2"><p>بندگی خواهد نمودن از بن دندان پری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با چنین گوی و چنین چوگان بمیدان نبرد</p></div>
<div class="m2"><p>بفکند گوی از جمالت بشکند چوگان پری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر پری زانسان بخوبی نه بدی هرگز بدی</p></div>
<div class="m2"><p>سالها متواری و پنهانی از انسان پری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن پری کوهست پیدا نیست زانسان خوبتر</p></div>
<div class="m2"><p>چشم انسان خیره ماند در جمال آن پری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آدمی پنهان شود همچون پری از شرم او</p></div>
<div class="m2"><p>بر خلاف آنکه گردد زآدمی پنهان پری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هست برهان آن پری را کآدمی صورت شود</p></div>
<div class="m2"><p>ور چنین گوئی ندارد هرگز این برهان پری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اینک اینک چون غلامان عرصه می خواهد زدن</p></div>
<div class="m2"><p>عارض خود پیش صدر عارض غلمان پری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواجه عالم حکیم عارض احمد آنکه او</p></div>
<div class="m2"><p>فخر انسانست و او را میبرد فرمان پری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صف زده بینی پری رویان به عرش تخت او</p></div>
<div class="m2"><p>چون سلیمانست گوئی خواجه و ایشان پری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مجلس او همچو بستان سلمانست باز</p></div>
<div class="m2"><p>صف کشیده پیش او چون سرو در بستان پری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر پری کز امر او بیرون شود شیطان شود</p></div>
<div class="m2"><p>چون درآرد سر بخط او شود شیطان پری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خدمت او تاج دارد بر سر ایمان خویش</p></div>
<div class="m2"><p>گر زدینداران همی دارد درست ایمان پری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون پری بینم به پیش خدمتش گردد یقین</p></div>
<div class="m2"><p>کافریدست از برای خدمتش یزدان پری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا چو خلق او بگیرد بهره از بوی خوشش</p></div>
<div class="m2"><p>گاهگاهی آدمی را زان کند نقصان پری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کرد پیمان خواجه تا شعری برآرم در دریف</p></div>
<div class="m2"><p>من ردیف شعر خود کردم بدان پیمان پری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هست انسان بنده احسان درست است این سخن</p></div>
<div class="m2"><p>او زانسان پیش دارد بنده احسان پری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جان و انسان بنده فرمانبرش بادا مدام</p></div>
<div class="m2"><p>تا بتازی هست انسان آدمی و جان پری</p></div></div>