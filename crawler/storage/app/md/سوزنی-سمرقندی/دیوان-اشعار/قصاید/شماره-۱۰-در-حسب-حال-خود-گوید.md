---
title: >-
    شمارهٔ ۱۰ - در حسب حال خود گوید
---
# شمارهٔ ۱۰ - در حسب حال خود گوید

<div class="b" id="bn1"><div class="m1"><p>چو شست گشت کمان قامت چو تیر مرا</p></div>
<div class="m2"><p>چو شست راست برآمد بهار و تیر مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو تیر کان زکمان از گشاد شست پرد</p></div>
<div class="m2"><p>پرید عمرو کمان گشت و شست تیر مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شست زلف کمان ابروان و تیر قدان</p></div>
<div class="m2"><p>نماند بهره و حظ و نصیب و تیر مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو تیر محترقم ز آفتاب و با پیری</p></div>
<div class="m2"><p>فتاده کار چو با آفتاب و تیر مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تحیر است چو از دیدن ستاره بروز</p></div>
<div class="m2"><p>ز دیدن قمر اندر شبان تیر مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان بنور دو چشمم رسیده نقصانی</p></div>
<div class="m2"><p>که جز سها ننماید مه منیر مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنون دو چشم مرا لاله و زریر یکی است</p></div>
<div class="m2"><p>چرا که عارض چون لاله شد زریر مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بفحش و هزل جوانی به پیری آوردم</p></div>
<div class="m2"><p>که هیچ شرم نبود از جوان و پیر مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی به دو نه برآمد شمار طاعت من</p></div>
<div class="m2"><p>برآمد از گنهان مبلغ خطیر مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بفسق و عصیان اندر تف سعیر شدم</p></div>
<div class="m2"><p>که دم نشد زندامت چو زمهریر مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسی گناه صغیر و کبیر کردم کسب</p></div>
<div class="m2"><p>که نز کبیر خطر بود و نز صغیر مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهر صغیر عذابی کبیر را اهلم</p></div>
<div class="m2"><p>اگر نه عفو کند خالق کبیر مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز پادشاه و دبیر است شر و خیر نویس</p></div>
<div class="m2"><p>که یک نفس نبود زان و این گزیر مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دبیر خیر ز من فارغ و نوشته شده است</p></div>
<div class="m2"><p>هزار نامه شر از دگر دبیر مرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیامد از من خیری و در دلم همه آن</p></div>
<div class="m2"><p>که حق پذیرد بی خیر خیر خیر مرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیک ندم بپذیرد حق ار بود یکدم</p></div>
<div class="m2"><p>زبان و سینه حق گول حق پذیر مرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بهر گناه مشار الیه خلق شدم</p></div>
<div class="m2"><p>از آنکه وسوسه دیو بد مشیر مرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نماند در همه عالم ره بدی الاک</p></div>
<div class="m2"><p>هماره بود در آن راه بد مسیر مرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پیاز نیکی من هیچگونه تن نگرفت</p></div>
<div class="m2"><p>بدین سزد که بکوبند سر چو سیر مرا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو مصر جامعم از هر بدی و میترسم</p></div>
<div class="m2"><p>از آنکه سوی جهنم بود مسیر مرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بآب فسق و فساد و خطا و جرم و زلل</p></div>
<div class="m2"><p>نیافریده خداوند من نظیر مرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ورا از آنکه نگویم نظیر و نشناسم</p></div>
<div class="m2"><p>ز جور این تن جابر بود مجیر مرا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز دست شیطان در پای دام معصیتم</p></div>
<div class="m2"><p>جز او نباشد ازین دام دستگیر مرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز رفتن در سلطان بکسب کردن زر</p></div>
<div class="m2"><p>نگاه دارد سلطان بی وزیر مرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنانکه دایه دهد انگبین و شیر بطفل</p></div>
<div class="m2"><p>دهد ز کوثر فضل انگبین و شیر مرا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در آفرینش خود چون نگه کنم گویم</p></div>
<div class="m2"><p>سرشته شد ز بدی مایه خمیر مرا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تنور عفو تو گرم آمد ای خدای ودود</p></div>
<div class="m2"><p>بدست توبه شود بسته یک فطیر مرا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گمان من بتو است آنکه عاقبت نکنی</p></div>
<div class="m2"><p>نه از قلیل عقوبت نه از کثیر مرا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نقیر و قطمیر از من گناه اگر بودی</p></div>
<div class="m2"><p>مکن خطاب ز قطمیر و از نقیر مرا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز نفس خود بنفیر آمدم تو رس فریاد</p></div>
<div class="m2"><p>ز نفس من که نفس آمد از نفیر مرا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من ار بمیرم شمع ضمیر من نمرد</p></div>
<div class="m2"><p>که چشم دل بود از نور او قریر مرا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز بحر جرم نماند اثر برحمت تو</p></div>
<div class="m2"><p>اگر بود ز ثری جرم تا اثیر مرا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر بظاهر در ظلمتم ز جرم و زلل</p></div>
<div class="m2"><p>ز نور دین تو شعله است در ضمیر مرا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بوقت مرگ چو با دیو کارزار کنم</p></div>
<div class="m2"><p>تو باش تا نبرد دیو دین نصیر مرا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دم از ندم چو برارم ز قعر سینه بلب</p></div>
<div class="m2"><p>مران بسوی دو لب دوزخ قعیر مرا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بمن فرست بتسلیم و قبض جان ملکی</p></div>
<div class="m2"><p>که از سلامت ایمان بود بشیر مرا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بزیر خاک ملقن تو باش وقت سوآل</p></div>
<div class="m2"><p>که تا صواب رود پاسخ نکیر مرا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رسول گفت امیر سخن بود شاعر</p></div>
<div class="m2"><p>بدین قصیده سزد خوانی ار امیر مرا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>امیر اگر بود از اهل تیغ و تاج و سریر</p></div>
<div class="m2"><p>ز فضل تاج ده و از خرد سریر مرا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو دار تیغ زبان مرا چنان جاری</p></div>
<div class="m2"><p>که گاه نظم نداند کس از جریر مرا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو سوزنی لقب آمد ز حر نار سفر</p></div>
<div class="m2"><p>برون جهان چو سر سوزن از صریر مرا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز من بجد شبیر و شبر سلام رسان</p></div>
<div class="m2"><p>بحشر با شبرانگیز و با شبیر مرا</p></div></div>