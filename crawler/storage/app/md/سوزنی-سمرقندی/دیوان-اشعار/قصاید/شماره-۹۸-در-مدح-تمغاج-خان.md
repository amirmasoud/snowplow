---
title: >-
    شمارهٔ ۹۸ - در مدح تمغاج خان
---
# شمارهٔ ۹۸ - در مدح تمغاج خان

<div class="b" id="bn1"><div class="m1"><p>ز آمدن سال نو بفرخی فال</p></div>
<div class="m2"><p>شاه جهانراست فتح و نصرت و اقبال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سال نو آمد بخدمت قدم شاه</p></div>
<div class="m2"><p>لشگر انواع گل مقدمه سال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خسرو سیارگان چرخ بتعجیل</p></div>
<div class="m2"><p>از سر ماهی برون گذشت و زدنبال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در علم آل شاه فتح و ظفر دید</p></div>
<div class="m2"><p>کرد بلند از سر حمل علم آل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان علم آل نصرت متوالی است</p></div>
<div class="m2"><p>در حشم شاه و در عشرت و در آل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موسم جشن خدایگان جهانست</p></div>
<div class="m2"><p>نوبت بخشیدنست و موسم ابذال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منتظر جشن شاه مطرب و بستان</p></div>
<div class="m2"><p>بلبل دستانسرای و قمری قوال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاج مرصع نهاده بر سر طاوس</p></div>
<div class="m2"><p>فاخته افکنده طوق مشکین در بال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه سلیمان مثال و طیر سخنگوی</p></div>
<div class="m2"><p>از ظفر پادشاه بنده امثال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلطان طمغاج خان که سلطنت او</p></div>
<div class="m2"><p>برکند از هر هژبر پنجه و چنگال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با علم گاو سار شیر نشانش</p></div>
<div class="m2"><p>شیر فلک روبه است و دمنه محتال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای ملک بی عدیل عالم عادل</p></div>
<div class="m2"><p>خسرو مسعود نام محمود احفال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخت تو با نام تو مساعد و با تخت</p></div>
<div class="m2"><p>طالع سعدت قرین همیشه بهر حال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر فلک پیر سعد اصغر و اکبر</p></div>
<div class="m2"><p>از تو گشایند بر سعادت تو فال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خسرو افراسیاب هیبتی و هست</p></div>
<div class="m2"><p>از تو در اعدای تو هزاهز و زلزال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زلزله لشکر تو روز ملاقات</p></div>
<div class="m2"><p>به ز نبرد آزمای روستم زال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چشم جهان چون تو پادشاه نبیند</p></div>
<div class="m2"><p>بزمی و رزمی عدوی مال و عدو مال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکه بفرمان تست تابع و راغب</p></div>
<div class="m2"><p>با نعم و عزتست و رتبه و با مال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وانکه ز درگاه تست طاغی و یاغی</p></div>
<div class="m2"><p>جفت هوانست و یار شدت و اهوال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسکه درآورد حاسدان ترا چرخ</p></div>
<div class="m2"><p>ایدی و اعناق در سلاسل و اغلال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حنجر آمال دشمنانت ببرید</p></div>
<div class="m2"><p>دهر ز دست قضا بخنجر آجال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای خلف آخر از خلیفه اول</p></div>
<div class="m2"><p>آنکه سرشته شد از سلاله صلصال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملک سلاطین گیتی از وی با تو</p></div>
<div class="m2"><p>کرد محول همی محول احوال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر تو و شهزادگان تست بحق وقف</p></div>
<div class="m2"><p>ملک زمین تا ابد بقسمت آزال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مهدی صاحبقران روی زمینی</p></div>
<div class="m2"><p>امر ترا رام گشته مهدی و دجال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خواهد بودن بملکداری تو شاه</p></div>
<div class="m2"><p>نصرت عیسی و رقص کردن دجال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سوزنیا با مدیح شاه جوانبخت</p></div>
<div class="m2"><p>از دل و جان خو کن و مدیحش بسگال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در سخن دانه چین نما و بسلک آر</p></div>
<div class="m2"><p>دانه و که را فر و بیز بغربال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>طبع سخن زای راز حشو نگه دار</p></div>
<div class="m2"><p>باش بابیات خود چو دایه بر اطفال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا شود از مدح شاه دفتر شعرت</p></div>
<div class="m2"><p>همچو رخ نیکوان بزلف و خط یال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عمر ابد خواه پادشاه جهانرا</p></div>
<div class="m2"><p>در شرف و عز لایزال و لازال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>باد بر این اتصال شاه همایون</p></div>
<div class="m2"><p>با رغد عیش و با رفاه در افعال</p></div></div>