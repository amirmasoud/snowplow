---
title: >-
    شمارهٔ ۱۶۵ - در مدح وزیر صدرالدین
---
# شمارهٔ ۱۶۵ - در مدح وزیر صدرالدین

<div class="b" id="bn1"><div class="m1"><p>ای صدر دین و دنیا بادا بقای تو</p></div>
<div class="m2"><p>منشیندا کسی بجز از تو بجای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانه باد با تو غم و آشنا طرب</p></div>
<div class="m2"><p>بادا ببحر لهو طرب آشنای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر کلک تست تکیه گه ملکت زمین</p></div>
<div class="m2"><p>وز قدر بر سپهر برین متکای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کلک تو چون عصای کلیم پیمبر است</p></div>
<div class="m2"><p>پیدا در و کرامت بی منتهای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر کلک خویش تکیه زن اندر ثبات ملک</p></div>
<div class="m2"><p>او اژدهای دشمن تست و عصای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر شفای تست همه خلقرا شفا</p></div>
<div class="m2"><p>زانرو همی خوهند خلایق شفای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای مهتران ملک همه زیر دست تو</p></div>
<div class="m2"><p>وی سروران دهر همه خاک پای تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک موی تو که قیمت جان خلایقست</p></div>
<div class="m2"><p>هست این کمین فضیلت قدر و بهای تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ارزنده ای بدان که فشانند بر تو جان</p></div>
<div class="m2"><p>ای صد هزار جان گرامی فدای تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خردی که سوی خلد شد از نسل بزرگ</p></div>
<div class="m2"><p>بادا کمی بقاش فزونی بقای تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او را هزار سال بقا بود و حق نخواست</p></div>
<div class="m2"><p>تا در بقای خویش نبیند فنای تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود از عطای یزدان نزد تو تا کنون</p></div>
<div class="m2"><p>واکنون بنزد رضوان بود او عطای تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرخ لقای خویش نهان کرد از جهان</p></div>
<div class="m2"><p>در آرزوی فرخ و میمون لقای تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رفت از سرای خویش بتخت و سرای خلد</p></div>
<div class="m2"><p>اندر دریغ و حسرت تخت و سرای تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاطر همه بمدح سرائیت بسته کرد</p></div>
<div class="m2"><p>وین مرثیت نگوید مدحت سرای تو</p></div></div>