---
title: >-
    شمارهٔ ۱۱ - در موعظه و نصیحت
---
# شمارهٔ ۱۱ - در موعظه و نصیحت

<div class="b" id="bn1"><div class="m1"><p>در این جهان که سرای غمست و تاسه و تاب</p></div>
<div class="m2"><p>چو کاسه بر سر آبیم و تیره دل چو سراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراب عالم و ما جغدوار و این نه عجب</p></div>
<div class="m2"><p>عجب از آنکه نمانند جغد را بخراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخواب غفلت خفتیم خورده شربت جهل</p></div>
<div class="m2"><p>که تا شدیم زبیدار فتنه بی خور و خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کباب آتش حرصیم و آن ز خامی ماست</p></div>
<div class="m2"><p>حقیقت است که هر خام را کنند کباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کباب خویش نخوانیم و زو عمل نکنیم</p></div>
<div class="m2"><p>که ناگزیر ستایندمان ز اهل کتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بحرص خواسته ورزیم تا شود بر ما</p></div>
<div class="m2"><p>وبال خواسته چونانکه موی بر سنجاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تقی و عاقبت اندیش نیست از ما کس</p></div>
<div class="m2"><p>ازین شدیم سزاوار گونه گونه عقاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقاب طاعت ما باز مانده از پرواز</p></div>
<div class="m2"><p>شدیم صید معاصی چو کبک صید عقاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه طریق صواب از خطا همیدانم</p></div>
<div class="m2"><p>گرفته راه خطائیم و باز مانده صواب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عنان ز طاعت حق تافتیم و بر باطل</p></div>
<div class="m2"><p>بر اسب معصیت آورده پای را برکاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر خدای تعالی حساب خواهد و بس</p></div>
<div class="m2"><p>بس است ما را گر عاقلیم شرم حساب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که دست طاقت آن گر ملک نهد بر ما</p></div>
<div class="m2"><p>گرانترین گنهی را سبکترین عذاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی پزیم همه در تنور چوبین نان</p></div>
<div class="m2"><p>همی بریم همه جامه بر تن از مهتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دماغ ما ز خرد نیستی اگر خالی</p></div>
<div class="m2"><p>نرانده ایمی گستاخ وار جز بخلاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درین جهان که دو دم بیش نیست مایه عمر</p></div>
<div class="m2"><p>درنگ سود ندارد چو دم بود بشتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آن چه به که یکی زین دو دم بتوبه زنیم</p></div>
<div class="m2"><p>چو باب توبه نه بستست ایزد تواب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شدیم جمله بگرداب معصیت گردان</p></div>
<div class="m2"><p>که هم امید خلاص است و هم ز غرق بآب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دو دیده را زندم سیل بار باید کرد</p></div>
<div class="m2"><p>بر آن امید که سیلاب می کند گرداب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بآب دیده بشوئیم نامه عصیان</p></div>
<div class="m2"><p>که هست نامه عصیان چو ریم خورده بتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر به نسبت سلمانیم ز روی پدر</p></div>
<div class="m2"><p>نسب چو سود چو گوید فلک فلاانساب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که تا بسیرت سلمان شوم دعائی کن</p></div>
<div class="m2"><p>مگر دعای تو در حق من شود ایجاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بحکم ایزد وهاب تا بخواهد داشت</p></div>
<div class="m2"><p>سپهر روشن دوران بگرد تیره تراب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه بوتراب و جنید و شفیق و شبلی باش</p></div>
<div class="m2"><p>دو دیده بر ره فرمان ایزد وهاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درود باد ز ما و تو بر رسول خدای</p></div>
<div class="m2"><p>فزون ز ذره خورشید و قطره های سحاب</p></div></div>