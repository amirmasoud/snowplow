---
title: >-
    شمارهٔ ۱۵۲ - در مدح نصیرالدین احمد
---
# شمارهٔ ۱۵۲ - در مدح نصیرالدین احمد

<div class="b" id="bn1"><div class="m1"><p>خورشید نوربخش چو رای نصیرالدین</p></div>
<div class="m2"><p>آمد بسوی برج حمل روشن و مبین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نور فر او رخ بستان و باغ شد</p></div>
<div class="m2"><p>آراسته چو سیرت و طبع نصیر دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کف آن بزرگ بیاموخت ابر جود</p></div>
<div class="m2"><p>بگشاد بر جهان صدف لؤلؤ ثمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز خلق آن کریم صبا یافت بهره ای</p></div>
<div class="m2"><p>در بوستان پدید سمن گشت و یاسمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در باغ رسم بزم ورا دید شاخسار</p></div>
<div class="m2"><p>چون دست او فشاند زر و نقره بر زمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دشمنانش ابر بگرید زمان زمان</p></div>
<div class="m2"><p>چون حاسدانش رعد کند ناله و انین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر میان گریه ابر و خروش رعد</p></div>
<div class="m2"><p>چون ناصحانش برق بخندد بآن و این</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در باغ سبزی سر او خواست شاخ بند</p></div>
<div class="m2"><p>شد سبز و مشگبوی چو گیسوی حور عین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی افرین سرائی بلبل بهار و باغ</p></div>
<div class="m2"><p>پدرام نیست گر چه چمن شد بهار چین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در باغ بلبلان شده اند آفرین سرای</p></div>
<div class="m2"><p>تا بر نصیر دین بسرایند آفرین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای در سرشت عالمیان آفرین تو</p></div>
<div class="m2"><p>وز آفرین سرشته ترا عالم آفرین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زیر نگین تست همه ملک پادشاه</p></div>
<div class="m2"><p>ملک از تو قدر یافته چون خاتم از نگین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کس نیست همنشین تو در صدر مهتری</p></div>
<div class="m2"><p>و اقبال و دولتند بصدر تو همنشین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وز سروران ملک قرین تو نیست کس</p></div>
<div class="m2"><p>زین روی بخت نیک تو با تو بود قرین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جز نیک نیست در تو گمان جهانیان</p></div>
<div class="m2"><p>بر تو بنیک باد گمانها شده یقین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد کعبه آستان تو کازاردگان بطبع</p></div>
<div class="m2"><p>سایند بر ستانه درگاه تو جبین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آزادگان ز بنده نوازی که در تو هست</p></div>
<div class="m2"><p>کردند بنگیت بر ازادگی گزین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خاک در تو سرمه بینائی آن کند</p></div>
<div class="m2"><p>کورا دلیست روشن و دانا و دوربین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر پای خویش بند کند خانه رکاب</p></div>
<div class="m2"><p>آنکس که بر تو تیر گشاد از کمان کین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیش کمینه بنده تو بندگی کند</p></div>
<div class="m2"><p>هرکس که بنده وار برون آید از کمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با دولت تو هست فلکرا یمین چنانک</p></div>
<div class="m2"><p>ار بشکنی فلکرا او نشکند یمین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وز عون کردگار جهان همچو دو ملک</p></div>
<div class="m2"><p>یسر است بر یسار تو و یمن بر یمین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حفظ و عنایت فلکی نایدت بکار</p></div>
<div class="m2"><p>چون کردگار هست ترا حافظ و معین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا از سرشک ابر برآید بنوبهار</p></div>
<div class="m2"><p>در باغ و راغ سبزه و لاله ز روی و طین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون لاله باد و سبزه دو رخسار و فرق تو</p></div>
<div class="m2"><p>طبع تو شاد و طبع بداندیش تو حزین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون لاله باد خصم تو و باده باد لعل</p></div>
<div class="m2"><p>در دست ساقئی ز رخش لاله شرمگین</p></div></div>