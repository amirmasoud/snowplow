---
title: >-
    شمارهٔ ۲ - در مدح علاء الدین محمد بن سلیمان
---
# شمارهٔ ۲ - در مدح علاء الدین محمد بن سلیمان

<div class="b" id="bn1"><div class="m1"><p>آورد گرد فتح و ظفر پیش چشم ما</p></div>
<div class="m2"><p>باد از رکاب عالی لازال عالیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد از رکاب عالی بر نصرت و ظفر</p></div>
<div class="m2"><p>در دیده رعیت باشد چو توتیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالی علاء دولت و دین آنکه تا بحشر</p></div>
<div class="m2"><p>هرگز مباد دولت و دین را جز او علا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاقان محمد بن سلیمان که ملک او</p></div>
<div class="m2"><p>دارد نهاد ملک سلیمان پادشا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن پادشا که تا که خدایست نصرتیست</p></div>
<div class="m2"><p>بر دشمنان مراو را هر روز از خدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناصر ویست دین خدای و رسول را</p></div>
<div class="m2"><p>نصرت بجز ورا بجهان کی بود روا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیک آمد و بد آمد خلق خدا ازوست</p></div>
<div class="m2"><p>آن به بود که قدرت و قوت بود روا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون گند ناز روی زمین دشمنان دین</p></div>
<div class="m2"><p>سر بر زدند از حد چین تا در ختا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست فلک ربود سر دشمنان دین</p></div>
<div class="m2"><p>از تیغ گندنا شبه او چو گندنا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنان که بر مخالفت پادشاه دین</p></div>
<div class="m2"><p>بودند دست برده بمکر و بسیمیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه سیمیا و مکر بفر همای شاه</p></div>
<div class="m2"><p>زیشان نشان دهد نه زسیمرغ و کیمیا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن پادشا که هرکه خلافش صواب دید</p></div>
<div class="m2"><p>شمشیر او صواب جدا کرد از خطا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن پادشا که هیبت زور سپاه او</p></div>
<div class="m2"><p>افکند فتنه در ختن و خطه ختا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دشمن شکر شهی که چو عزم شکار کرد</p></div>
<div class="m2"><p>از هر کجا که روی نهد تا بهر کجا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون گردناست نیزه آتش سنان او</p></div>
<div class="m2"><p>دشمن چو مرغ گردان در گرد گردنا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یاقوت را شنیدم کز روی خاصیت</p></div>
<div class="m2"><p>دفع وبا کند چو عفونت بود هوا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روی هوا ز لشکر کفار شد عفن</p></div>
<div class="m2"><p>از گونه گونه وسوسه فاسد و هوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیکان تیر شاه چو یاقوت سرخ گشت</p></div>
<div class="m2"><p>از خون دشمنان و درافکندشان ز پا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گردافع و با بد یاقوت ور نبود</p></div>
<div class="m2"><p>آرنده وبا بچه معنی شد و چرا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خاقان قضای ایزد باربست از قیاس</p></div>
<div class="m2"><p>بر دشمنان دین همه شور و شر و بلا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خواهند کز قضا و بلا درکشند روی</p></div>
<div class="m2"><p>کارد فرود بر سر ایشان بلا قضا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کوشد اگر بجهد کسی با قضا بجنگ</p></div>
<div class="m2"><p>مغلوب گردد و بودش جهد نابجا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ایزد سزای نصرت مرشاه را گزید</p></div>
<div class="m2"><p>چون شاه عزم کرد بآوردن غزا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نصرت سزای شاه بدو شه سزای او</p></div>
<div class="m2"><p>واقبال ره نمود سزا را سوی سزا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از کردگار نصرت و از شاه کوشش است</p></div>
<div class="m2"><p>از کافران هزیمت و از مؤمنان دعا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دشمن قفای لشکر شه دیده کی کند</p></div>
<div class="m2"><p>مادام تا که دعوت نیکوست در قفا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ایزد خدایگان جهانرا بقا دهاد</p></div>
<div class="m2"><p>بیرون ز حد غایت و بیرون زانتها</p></div></div>