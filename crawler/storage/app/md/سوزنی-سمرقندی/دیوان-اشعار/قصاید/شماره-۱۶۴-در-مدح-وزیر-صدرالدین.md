---
title: >-
    شمارهٔ ۱۶۴ - در مدح وزیر صدرالدین
---
# شمارهٔ ۱۶۴ - در مدح وزیر صدرالدین

<div class="b" id="bn1"><div class="m1"><p>ای صدر دین و دنیا دنیا آفرین تو</p></div>
<div class="m2"><p>خالی نیند یکنفس از آفرین تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم آفرین کنندت و هم آفرین خوهند</p></div>
<div class="m2"><p>مرجان و بر تن تو ز جان آفرین تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صدق و یقین تست بیزدان تو درست</p></div>
<div class="m2"><p>تا کار تست در خور صدق و یقین تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تاج بر سراست ترا شغلهای دین</p></div>
<div class="m2"><p>زاین است شغل دنیا زیر نگین تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نازش وزیر بتاج و نگین تو است</p></div>
<div class="m2"><p>انصاف تست و عدل تو تاج و نگین تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آستان تست بخدمت جبین خلق</p></div>
<div class="m2"><p>چونان که شکر و خدمت حق را جبین تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای تو زاستان تو جز بر ره سخا</p></div>
<div class="m2"><p>ناید برون چو دست تو از آستین تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صاحبقران بود که نباشد کسش قرین</p></div>
<div class="m2"><p>صاحبقران وقتی کس نی قرین تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر شاهرا دفین در و گوهر است و هست</p></div>
<div class="m2"><p>اندر دل تو نیست نیکو دفین تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر دم دفین خویش کنی بر جهان نثار</p></div>
<div class="m2"><p>تا عالمی شوند رهی و رهین تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روی ستم نیاید پیدا بچشم کس</p></div>
<div class="m2"><p>از بیم هیبت تو و ازهان و هین تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر سال و ماه ابر بلا بارد و عنا</p></div>
<div class="m2"><p>نبت ستم نروید اندر زمین تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنگه که ایزد از طین آدم بیافرید</p></div>
<div class="m2"><p>اندر مکان طین عمر بود طین تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو خلدی از قیاس همه خلق را و هست</p></div>
<div class="m2"><p>خلق خوش تو کوثر و ماه معین تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جان و سر تو گشت یمین پادشاه را</p></div>
<div class="m2"><p>داده یمین ببیعت عهد و یمین تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا مهر ملک و سلطنت اندر یسار اوست</p></div>
<div class="m2"><p>ملک و زار تست و سری در یمین تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون پادشاه را پدر به گزین توئی</p></div>
<div class="m2"><p>شد نزد پادشا پدر به گزین تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رأی مبین تو سفرش اختیار کرد</p></div>
<div class="m2"><p>خیر اختیار با دارای مبین تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فرزند نازنین تو گر نزد شاه شد</p></div>
<div class="m2"><p>رنجور دل مباد تن نازنین تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر کس بنزد شاه امینی گزین کند</p></div>
<div class="m2"><p>فرزند هر آینه بهتر امین تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرخ بود بدو نظر پادشاه دین</p></div>
<div class="m2"><p>این رأی نیک دید و دل پاک بین تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زودا که بینی آمده او را به نزد خویش</p></div>
<div class="m2"><p>بر تخت عز و جاه شده همنشین تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا در جهان شهور و سنین خواهد آمدن</p></div>
<div class="m2"><p>در عز و ناز باد شهور و سنین تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا کار دین و دنیا دارد و طریقها</p></div>
<div class="m2"><p>جز بر طریق راست مباد آن و این تو</p></div></div>