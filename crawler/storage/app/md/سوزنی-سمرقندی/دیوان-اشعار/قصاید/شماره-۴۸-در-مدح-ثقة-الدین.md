---
title: >-
    شمارهٔ ۴۸ - در مدح ثقة الدین
---
# شمارهٔ ۴۸ - در مدح ثقة الدین

<div class="b" id="bn1"><div class="m1"><p>ای سرو سرمایه کرام سمرقند</p></div>
<div class="m2"><p>نام تو مشهورتر ز نام سمرقند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمس امینان و صائبان ثقة الدین</p></div>
<div class="m2"><p>معتمد شاه و خاص و عام سمرقند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>احمد بن الامام آنکه ز رتبت</p></div>
<div class="m2"><p>سرور و سرمایه کرام سمرقند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو پسر صاین و امین سمرقند</p></div>
<div class="m2"><p>پور و پدر مفتی و امام سمرقند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بسمرقند هیچ نعمت نبود</p></div>
<div class="m2"><p>فر تو بس نعمت تهام سمرقند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خوشی و خرمی چو دار سلام است</p></div>
<div class="m2"><p>با فر و زیب تو هر مقام سمرقند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهد دار السلام تا تو دروئی</p></div>
<div class="m2"><p>کاید هر روز بر سلام سمرقند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که درو بنگرد بدیده تعظیم</p></div>
<div class="m2"><p>گردد از جمله عظام سمرقند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیت حرامست خانه تو ز تعظیم</p></div>
<div class="m2"><p>حامی او اهل احترام سمرقند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خان و در جود او نهاده گشاده</p></div>
<div class="m2"><p>از ره انعام بر عوام سمرقند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست ز خوان تو ای کریم بسی خلق</p></div>
<div class="m2"><p>بیخبر از عشرت طعام سمرقند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوان نه و نان ده کریم وار و میندیش</p></div>
<div class="m2"><p>از حسد و طعنه لئام سمرقند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرد ترا مام و باب راد بدینسان</p></div>
<div class="m2"><p>از شفقت نیک باب و مام سمرقند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بشنوی ارچه زبان ندارد شکرت</p></div>
<div class="m2"><p>از در و دیوار و صحن و بام سمرقند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر چه سمرقند بی کلام نگوید</p></div>
<div class="m2"><p>گوید مدح تو بی کلام سمرقند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شهری نبود در او همامی نبود</p></div>
<div class="m2"><p>ای تو و فرزند تو همام سمرقند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر تو و فرزند تست امن و صیانت</p></div>
<div class="m2"><p>بر قلم بوئیان قوام سمرقند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کار شما بر نظام باد و به رونق</p></div>
<div class="m2"><p>ای ز شما رونق و نظام سمرقند</p></div></div>