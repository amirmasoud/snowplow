---
title: >-
    شمارهٔ ۱۹۳ - غزل به صفات تو گویم
---
# شمارهٔ ۱۹۳ - غزل به صفات تو گویم

<div class="b" id="bn1"><div class="m1"><p>بر من آمد دوش آن در چشم بینائی</p></div>
<div class="m2"><p>ز بهر جستن تدبیر رای فردائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرآنچه داشت بدل راز پیش من بگشاد</p></div>
<div class="m2"><p>بلی چنین سزد از یکدلی و یکتائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه گفت گفت بخواهم شدن ز تو یکچند</p></div>
<div class="m2"><p>که تا ز فتنه خصمان من برآسائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر آب کرد چو دریا دو چشم و از غم هجر</p></div>
<div class="m2"><p>برخ از مژه بارید در دریائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آه گفت رفیقان مرا همی بایند</p></div>
<div class="m2"><p>کنار گیر و وداعی هلاکه رابائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببر گرفت مرا تنگ و تنگ و اسب فراق</p></div>
<div class="m2"><p>ببست و گفت که یارا تو بر چه سودائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه اوفتاد و چه کردم گنه بجای تو من</p></div>
<div class="m2"><p>چرا بجستن هجران چنین مهیائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر وصال منت ناپسند بود بدل</p></div>
<div class="m2"><p>که بر براق فراقم سوار بنمائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهجر خنجر بر پای وصل من چه زنی</p></div>
<div class="m2"><p>بر این غریبی و برنائیم نبخشائی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عجب بدی که نبودی نصیب من مسکین</p></div>
<div class="m2"><p>فراق یار و غریبی و عشق و برنائی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بجان گرانی هجران چگونه ای دانی</p></div>
<div class="m2"><p>بسان خنجر زهراب داده بر پائی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی گر ستم میگفتم از رکاب بدیع</p></div>
<div class="m2"><p>کجا روی و کجا باشی و چه وقت آئی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفت رفتن از تو ضرورتست مرا</p></div>
<div class="m2"><p>گمان مبر که ز خود کامسیت خودرائی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهر کجا که بوم در وفا و مهر توام</p></div>
<div class="m2"><p>بگفتم ایدل و جان خود هم این چنین آئی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگفت تا بو باز آیم آنچنان باید</p></div>
<div class="m2"><p>که دفتر از غزل و مدح من بیارائی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جوابدادم کای نور چشم و راحت جان</p></div>
<div class="m2"><p>شد این مراد تو حاصل دگر چه فرمائی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه غزل بصفات جمال تو گویم</p></div>
<div class="m2"><p>بمدح ناصر دین سیدی و دلخوائی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جلال امت مجدالائمه ناصر دین</p></div>
<div class="m2"><p>اساس فضل و بزرگی و اصل و دانائی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حسد ببرده بدوگر حسود آتشخوی</p></div>
<div class="m2"><p>بخاک بسته آبش زباد پیمائی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بمدح خلقت و خلق محامدش شب و روز</p></div>
<div class="m2"><p>هماره طوطی طبعم کند شکرخائی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ببیند آنچه نبینند دیگران آن کس</p></div>
<div class="m2"><p>که خاک درگه او کرد کحل بینائی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گسسته باد همی رشته دم آنکس را</p></div>
<div class="m2"><p>که دم زند بر او از منی و از مائی</p></div></div>