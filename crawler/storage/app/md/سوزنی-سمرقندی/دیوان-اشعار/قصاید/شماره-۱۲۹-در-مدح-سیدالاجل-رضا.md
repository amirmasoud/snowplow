---
title: >-
    شمارهٔ ۱۲۹ - در مدح سیدالاجل رضا
---
# شمارهٔ ۱۲۹ - در مدح سیدالاجل رضا

<div class="b" id="bn1"><div class="m1"><p>ماه صیام کرد بنیک اختری سلام</p></div>
<div class="m2"><p>بر خلعت شهنشه بر عمدة الانام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر عمدة الانام بشادی خجسته باد</p></div>
<div class="m2"><p>تشریف پادشاه و سلام مه صیام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرزانه سید احل مرتضی رضا</p></div>
<div class="m2"><p>آن صفوة الخلاقه و آن عمدة الامام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه شرف امیر خراسان که نام او</p></div>
<div class="m2"><p>گسترده شد بجود و هنر در عران و شام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی که تا دمید فلک صبح دولتش</p></div>
<div class="m2"><p>روز مراد دشمن او شد نماز شام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرورده و گزیده شاهنشه ملوک</p></div>
<div class="m2"><p>سنجر که یافت بر همه شاهان دهر نام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آباد گشت گیتی از خلق او چنان</p></div>
<div class="m2"><p>کز شرق تا بغرب توان رفت بام بام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنی که پادشاه جهان خسرو ملوک</p></div>
<div class="m2"><p>در روی تو نگه نکند جز باحترام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیغمبر خدای ترا داشت در کنار</p></div>
<div class="m2"><p>فخر القضاة مرو چنین دید در منام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تشبیه کرد چشم تو با چشم خود رسول</p></div>
<div class="m2"><p>یعنی که از منست و بمن ماند این پیام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای در میان آ . . . بمیر بسروری</p></div>
<div class="m2"><p>چون در میان انجم بر چرخ ماه تام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آمد هلال روزه و بنمود روی خویش</p></div>
<div class="m2"><p>مانند نعل زرین از چرخ نیلفام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یعنی مرا به بین که سزم نعل مرکبت</p></div>
<div class="m2"><p>چون شهریار داد بتو مرکب و ستام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر مرکب نشاط دل و نزهت و سرور</p></div>
<div class="m2"><p>بادی سوار تا ابدالدهر شادکام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر چند طبع سیر نگردد ز مدح تو</p></div>
<div class="m2"><p>بیت دعا بگویم کوته کنم کلام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا نام سال عام بود در نعیم و ناز</p></div>
<div class="m2"><p>عمر تو باد افزون از صد هزار عام</p></div></div>