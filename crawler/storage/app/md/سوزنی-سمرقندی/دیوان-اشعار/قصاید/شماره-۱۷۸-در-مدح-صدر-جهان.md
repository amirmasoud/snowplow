---
title: >-
    شمارهٔ ۱۷۸ - در مدح صدر جهان
---
# شمارهٔ ۱۷۸ - در مدح صدر جهان

<div class="b" id="bn1"><div class="m1"><p>صدر جهان رسید بشادی و خرمی</p></div>
<div class="m2"><p>در دوستان فزونی و در دشمنان کمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه جهان و صدر جهان شاد و خرم است</p></div>
<div class="m2"><p>جاوید باد شاه بشادی و خرمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای شاه راز طلعت فرخنده فال تو</p></div>
<div class="m2"><p>در دیده روشنائی و در سینه بی غمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هستند ناصحانت زنار و نعم غمی</p></div>
<div class="m2"><p>چونانکه حاسدانت ز بار نعم غمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صدر زمین تواضع و خورشید طلعتی</p></div>
<div class="m2"><p>وز طلعت تو یافته خورشید برزمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشیدی و سحاب چه خورشید و سخا</p></div>
<div class="m2"><p>خورشید جود ذره سحاب سخا نمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرامت رسول علیه السلام را</p></div>
<div class="m2"><p>در علم شرع صاحب و صدر مسلمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالی عبارت خوش عذب فصیح تو</p></div>
<div class="m2"><p>از الکن الکنی برد از ابکم ابکمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلطان ملک دینی و دنیا هم آن تست</p></div>
<div class="m2"><p>چون نیکخواه دولت شاه معظمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مردم شناس شاهی و نزدیک اهل عقل</p></div>
<div class="m2"><p>مردم توئی و شاه شناسا بمردمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون آدمی بصورت و معنی فرشته ای</p></div>
<div class="m2"><p>گوئی که هم فریشته ای و هم آدمی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با آنکه اعلم العلمائی بعلم شرع</p></div>
<div class="m2"><p>فتوی نشان کننده بوالله اعلمی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>الله اعلم ار ز تو باش کریمتر</p></div>
<div class="m2"><p>. . . ز علم از همه خلق اکرمی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در مدح تو بصورت تضمین ادا کنم</p></div>
<div class="m2"><p>. . . بیت رودکی را در حق بلعمی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صدر جهان جهان همه تاریک شب شدست</p></div>
<div class="m2"><p>از بهر ما سپیده صادق همی دمی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بینند جسم را و نه بینند روح را</p></div>
<div class="m2"><p>بینیم مرترا که تو روح مجسمی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کردند قصد جسم تو و روح تو بسی</p></div>
<div class="m2"><p>آهوئی و فزرمی و کرکوتی و رمی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگرفتشان زمین و زمان کرد خاکسار</p></div>
<div class="m2"><p>تو همچنان عزیز و شریف و مکرمی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا جای گنج قارون ایشان فرو شدند</p></div>
<div class="m2"><p>تو برشده بطارم عیسی بن مریمی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ایوان تو زطارم پیروزه فلک</p></div>
<div class="m2"><p>بگذشت از آنکه صاحب ایوان و طارمی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در جویبار سنت و در باغ علم شرع</p></div>
<div class="m2"><p>چون سرو راستی چو بنفشه همی غمی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>طاوس وار در چمن فقه و باغ علم</p></div>
<div class="m2"><p>زینسو همی خرامی و زانسو همی چمی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از حشمت تو بی ربض و خندق و سلاح</p></div>
<div class="m2"><p>سد سکندر است بخارا ز محکمی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اسلاف تو برحمت حق حامی ویند</p></div>
<div class="m2"><p>بی زحمت پیاده و سرهنگ دیلمی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حق کی گذاشتی که بخارای چون بهشت</p></div>
<div class="m2"><p>ویران شود بجمله مشتی جهنمی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شمس حسام برهان دانی که تو که ای</p></div>
<div class="m2"><p>درد بخاریان را درمان و مرهمی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در حضرت سمرقند از فر پادشاه</p></div>
<div class="m2"><p>شاهنشه ائمه دین فخر عالمی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تدریس تو دعای شهنشاه اعظم است</p></div>
<div class="m2"><p>تو خاص دوستدار شهنشاه اعظمی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا چرخ تیز دور ز دوران نیارمد</p></div>
<div class="m2"><p>باید که از دعای شهنشه نیارمی</p></div></div>