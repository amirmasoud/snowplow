---
title: >-
    شمارهٔ ۵۸ - در مدح تاج الامرا حسن
---
# شمارهٔ ۵۸ - در مدح تاج الامرا حسن

<div class="b" id="bn1"><div class="m1"><p>نوشد بجهان جهان دیگر</p></div>
<div class="m2"><p>چرخ دگر و زمان دیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان نقش شد ارسلان سلیمان</p></div>
<div class="m2"><p>آمد نقش ارسلان دیگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالار صف سپاه دین آنک</p></div>
<div class="m2"><p>هست از شرف آسمان دیگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاج الامرا حسن کز احسان</p></div>
<div class="m2"><p>بحر دگر است و کان دیگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن شیردلی که همچنو نیست</p></div>
<div class="m2"><p>در خلخ پهلوان دیگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میری که سپهر پیر ناورد</p></div>
<div class="m2"><p>زیباتر از او جوان دیگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در روز مصاف رایت اوست</p></div>
<div class="m2"><p>چون رایت کاویان دیگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از مردی او زنند مردان</p></div>
<div class="m2"><p>هر روزی داستان دیگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میدان صف مبارزت را</p></div>
<div class="m2"><p>پندارد بوستان دیگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دردی بسر بنفشه گون تیغ</p></div>
<div class="m2"><p>کار و گل و ارغوان دیگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر روز کند به نیکنامی</p></div>
<div class="m2"><p>فعل و ره و رسم و سان دیگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخشب بجمال او شد امروز</p></div>
<div class="m2"><p>از بعد جنان جنان دیگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جز سایه عدل او بنخشب</p></div>
<div class="m2"><p>کو جایگه امان دیگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نام پدر و نیا بنگذاشت</p></div>
<div class="m2"><p>ضایع بکف کسان دیگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وین حشمت خاندان خود را</p></div>
<div class="m2"><p>نفکند بخاندان دیگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای همچو پدر بروز هیجا</p></div>
<div class="m2"><p>شیر یله ژیان دیگر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بعد از ملکی که جان ستاند</p></div>
<div class="m2"><p>شمشیر تو جان ستان دیگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در ملک شهنشهی که ندهند</p></div>
<div class="m2"><p>در دهر چنو نشان دیگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تیغ تو بس است پاسبانش</p></div>
<div class="m2"><p>بی منت پاسبان دیگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صفی که زیک کران بحیله</p></div>
<div class="m2"><p>دیدن نتوان کران دیگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تنها شکنی چو حمله کردی</p></div>
<div class="m2"><p>بی زحمت همعنان دیگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رمح تو زبس صواب زخمی</p></div>
<div class="m2"><p>سنبد بسنان سنان دیگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جز حلق مخالفان نشاید</p></div>
<div class="m2"><p>مرتیغ ترا فسان دیگر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برنده خدنگ تست بیجان</p></div>
<div class="m2"><p>هر روز بقصد جان دیگر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرغیست که جز دل مخالف</p></div>
<div class="m2"><p>نپسندد آشیان دیگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دشمن که هوای تو نکوشد</p></div>
<div class="m2"><p>هر لحظه کشد هوان دیگر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آرایش کار ملکرا نیست</p></div>
<div class="m2"><p>جز رأی تو قهرمان دیگر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای بر حشم و رعیت خویش</p></div>
<div class="m2"><p>خال وعم مهربان دیگر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>امروز بعید میزبانی</p></div>
<div class="m2"><p>نبود چو تو میزبان دیگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مهمان تو هست شاه شاهان</p></div>
<div class="m2"><p>زین بهتر میهمان دیگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مداح تو صد هزار کس هست</p></div>
<div class="m2"><p>هر سو بیکی زبان دیگر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زیشان چو محمد بن مسعود</p></div>
<div class="m2"><p>نی کهتر و مدح خوان دیگر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر لحظه فزون خوهد زمدحت</p></div>
<div class="m2"><p>در خاطر خود توان دیگر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وز جود کف تو هر زمانی</p></div>
<div class="m2"><p>یابد صلت گران دیگر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مادام که تا مرین جهانرا</p></div>
<div class="m2"><p>نازند بدل جهان دیگر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در ملک جهان مباد جز تو</p></div>
<div class="m2"><p>کس والی و کامران دیگر</p></div></div>