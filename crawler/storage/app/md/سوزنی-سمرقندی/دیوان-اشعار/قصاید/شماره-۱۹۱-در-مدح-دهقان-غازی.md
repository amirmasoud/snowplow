---
title: >-
    شمارهٔ ۱۹۱ - در مدح دهقان غازی
---
# شمارهٔ ۱۹۱ - در مدح دهقان غازی

<div class="b" id="bn1"><div class="m1"><p>سپهر برین را همه بر سرفرازی</p></div>
<div class="m2"><p>شد از همت و قدر دهقان غازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنون همچو بازیگران گاه کشتن</p></div>
<div class="m2"><p>کند همتش را همی بندبازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود کهتری آرزو مهتران را</p></div>
<div class="m2"><p>که او رای دارد بکهتر نوازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیاز آور و هر که یک روز پیشش</p></div>
<div class="m2"><p>بماند همه عمر در بی نیازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایا سرفرازی که از خلق نیکو</p></div>
<div class="m2"><p>بر احرار شاید که تو سرفرازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خلق خوش تست شرمنده دایم</p></div>
<div class="m2"><p>چه مشک تتاری چه بان حجازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تو خلق پرورده عز و نازند</p></div>
<div class="m2"><p>که تو اصل سرمایه عز و نازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پدر از تو فرزند نازد ترا هم</p></div>
<div class="m2"><p>چنان باد فرزند کز وی نیازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بکم شاعری آن دهد کف رادت</p></div>
<div class="m2"><p>کی محمود غازی به مسعود رازی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی ترک تازی زبان آمدستم</p></div>
<div class="m2"><p>به مهمان بی عشرت و عیش و بازی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ترکی و نازی بر او علم گفتم</p></div>
<div class="m2"><p>سر اندر نیارد به ترکی و تازی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به سیم و بمی کرد خواهم من امشب</p></div>
<div class="m2"><p>بر آن ترک تازی زبان ترک تازی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرانی به صدر تو آوردم امروز</p></div>
<div class="m2"><p>که تا کار مهمانی ما بسازی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به دریای آن سرو نازنده بالا</p></div>
<div class="m2"><p>کف راد خود را سوی کیسه بازی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرامشب گشاده شود حصن آب بت</p></div>
<div class="m2"><p>کنم باز فردا به پشتت غمازی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درازی این قصه کوتاه کردم</p></div>
<div class="m2"><p>همه در بقای تو بادا درازی</p></div></div>