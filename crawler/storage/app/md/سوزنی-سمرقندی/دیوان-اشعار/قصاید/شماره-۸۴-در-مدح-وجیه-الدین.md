---
title: >-
    شمارهٔ ۸۴ - در مدح وجیه الدین
---
# شمارهٔ ۸۴ - در مدح وجیه الدین

<div class="b" id="bn1"><div class="m1"><p>این منم یارب بصدر مهتر کهتر نواز</p></div>
<div class="m2"><p>از ندیمان یافته بر خواندن مدحت جواز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قفل درج طبع بگشاه بمفتاح زبان</p></div>
<div class="m2"><p>آشکارا کرده هر دری که در دل بود راز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهتر کهتر نواز از مدحت من شاد و خوش</p></div>
<div class="m2"><p>من خوش و شاد از قبول مهتر کهتر نواز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهتر آزادگان والا وجیه الدین که خلق</p></div>
<div class="m2"><p>مدح او خوانند چون وجهت وجهی در نماز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محترم صدری که در ساز سرایش در عجم</p></div>
<div class="m2"><p>حرمت آباد است چون بیت الحرام اندر حجاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه در بستان و باغ رادی و آزادگی است</p></div>
<div class="m2"><p>سوسن آزاده و آزاده سر و سرفراز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه تا آزادگی بر نام او تحقیق شد</p></div>
<div class="m2"><p>سرو و سوسن را شد است آزادگی نام مجاز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه باشد بر سریر بی نیازی متکی</p></div>
<div class="m2"><p>شد سریر جود او تکیه گه اهل نیاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سفره جود ورا تا باز گستردند شد</p></div>
<div class="m2"><p>بخل را آژنگ ابرو چهره چون سفره فراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با سخای او حدیث آز گفتند اندکی</p></div>
<div class="m2"><p>گفت هرگز من خبر دارد نداند نام آز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آز اندک باشد اندر لفظ ترکی و بعمر</p></div>
<div class="m2"><p>ساقی بر و عطای من نداند داد آز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منت از سائل بجان بردارد از چیزی که خواست</p></div>
<div class="m2"><p>ورچه جان خواهد دهد بی منت و با اعتذاز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچو طفل نازنین از باب و مام مهربان</p></div>
<div class="m2"><p>سایلان و زایران از لفظ او یابند ناز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر بصدر او درآید سایلی عریان چو سیر</p></div>
<div class="m2"><p>با حریر و حله تو بر تو رود همچون پیاز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مهتران از بهر حرزمال خود سازند گنج</p></div>
<div class="m2"><p>او ز حرزمال باشد روز و شب در احتراز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر زکف بخشش او سایه افتد بر زمین</p></div>
<div class="m2"><p>در زمین افتد ز بهر گنج قارون اهتزاز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل ز مهر سیم صافی صافتر دارد ز سیم</p></div>
<div class="m2"><p>ندهد اندر زرگدازی بین چو زر اندر گداز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای ز سهم پهلوان وز رأی عدل آموز تو</p></div>
<div class="m2"><p>یوز ز آهو در گریز افتاده و گرگ از گراز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قار اگر بازو زند بر باز عدل پهلوان</p></div>
<div class="m2"><p>چرخ عنقاوار متواری شود از بیم قاز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صعوه در ظل همای عدل و داد پهلوان</p></div>
<div class="m2"><p>مر عقاب ظلم را بر بردراند قاز قاز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در پناه پهلوان کبک و تذرو آرد برون</p></div>
<div class="m2"><p>جوجه گان دانه چین از بیضه شاهین و باز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ملک توران مهره کردار است بر روی بساط</p></div>
<div class="m2"><p>رأی ملک آرای تو بر مهره ما هر مهره باز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیر پرور دایه لطف تو است آنکو نکرد</p></div>
<div class="m2"><p>هیچ دانا را ز طفلی تا بپیری شیر باز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کرد ره گم کرده دم در فراق صدر تو</p></div>
<div class="m2"><p>کرد ره گم را جاهت براه آورد باز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آمدم تا طبع را سازم ز مدح تو غذا</p></div>
<div class="m2"><p>مدح تو طبع مرا باشد غذای طبع ساز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در امل تا دیر بازی و درازی ممکنست</p></div>
<div class="m2"><p>چون امل بادا ترا عمر دراز و دیر باز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدسگالان تو از هر شادئی کوتاه دست</p></div>
<div class="m2"><p>مانده از اقبال کوتاه اندر ادبار دراز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا زنند از حسن خوبان طراز و چین مثل</p></div>
<div class="m2"><p>از نکویان مجلس بزم تو چین با دو طراز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کسوت عمر ترا تا تادامن آخر زمان</p></div>
<div class="m2"><p>از بزرگی نام تو بر آستین بادا طراز</p></div></div>