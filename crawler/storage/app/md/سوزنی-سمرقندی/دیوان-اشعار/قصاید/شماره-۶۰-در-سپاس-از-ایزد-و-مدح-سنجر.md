---
title: >-
    شمارهٔ ۶۰ - در سپاس از ایزد و مدح سنجر
---
# شمارهٔ ۶۰ - در سپاس از ایزد و مدح سنجر

<div class="b" id="bn1"><div class="m1"><p>هست بر پرورده شکر نعمت پروردگار</p></div>
<div class="m2"><p>واجب از روی دیانت هم نهان هم آشکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستم آن پرورده نعمت که اندر عمر خویش</p></div>
<div class="m2"><p>داد نتوانم شمردن نعمت پروردگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شمار نعمت حق را ندانم بر شمرد</p></div>
<div class="m2"><p>کی توانم بر طریق شکر بودن حق گذار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر زبان شکر دارم صد هزاران نعمتش</p></div>
<div class="m2"><p>تا بعجز خود مقر نایم نگیرد دل قرار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه با من کرد از نیکی خداوند جهان</p></div>
<div class="m2"><p>گفت نتوانم بعمر خود یکی از صد هزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر یکی خود شناسا کرد تا بشناسمش</p></div>
<div class="m2"><p>کو یکی بود و یکی باشد نه از روی شمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کردگار گیتی و پروردگار عالمست</p></div>
<div class="m2"><p>رازق خلق و پدید آرنده لیل و نهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خالق کونین و هر چیزی که هست اندر دو کون</p></div>
<div class="m2"><p>صانع گردون گردان کردگار نور و نار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرسل پیغمبران حق بنزد بندگان</p></div>
<div class="m2"><p>ایزد دارالقرار و داور دارالبوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنکه از تقدیر و حکم او نشاید بنده را</p></div>
<div class="m2"><p>جز رضا در نیک و بد در هیچ وقت و هیچ کار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر چه آید بر من از تقدیر او دارم رضا</p></div>
<div class="m2"><p>بنده ام امروز را طاعت نمایم بنده وار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دلی صافی و طبعی پاک و ایمانی درست</p></div>
<div class="m2"><p>بر ره توحید حق باشم قوی و استوار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از پی توحید او گویم ثنای مصطفی</p></div>
<div class="m2"><p>احمد مختار کو از انبیا بود اختیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صاحب تاج و لوای حمد و معراج و براق</p></div>
<div class="m2"><p>صاحب فرمان و حج و عز و صاحب ذوالفقار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز پی حمد و درود وی ثنا گویم بسی</p></div>
<div class="m2"><p>بر امامان پسندیده گزیده هر چهار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کار دین آرایم از تحمید یاران نبی</p></div>
<div class="m2"><p>کار دنیا را برآرایم بمدح شهریار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پادشاه سنجر مغزدین و دنیا آنکه هست</p></div>
<div class="m2"><p>کارهای دین و دنیای من از وی چون نگار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یافتم از خدمت سلطان سلطانان دهر</p></div>
<div class="m2"><p>حشمت و جاه و شکوه و دولت و عز و وقار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هم بفر دولت سلطان اعظم یافتم</p></div>
<div class="m2"><p>خویشتن بر ملک خاقان کامران و کامکار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کار من بالا گرفت از اعتقاد نیک من</p></div>
<div class="m2"><p>کار من هر روز به شد تا برآمد روزگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مال بخشیدم نکو کردم بحق خاص و عام</p></div>
<div class="m2"><p>خاص من بودم نگفتن خاص دار و عام دار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر رعیت از حشم نامد بعهد من ستم</p></div>
<div class="m2"><p>باز ماند از عدل من باز شکاری از شکار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عدل ورزیدم بعهد خویش چون همنام خویش</p></div>
<div class="m2"><p>نا بعقبی باشم اندر خلد با همنام یار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مال خود بر کهتران خویشتن کردم فدا</p></div>
<div class="m2"><p>تا فدای من شوند آنگه که باشد گیر و دار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از ره نیک اعتقادی در ره نیکو دلی</p></div>
<div class="m2"><p>خواستم مرکهتران خویشتن را کار و بار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرکبان تیز تک دادم مر آنها را کجا</p></div>
<div class="m2"><p>جز پیاده می نرفتندی بهر شهر و دیار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در سر آنها قصب بستم که با بسیار جهد</p></div>
<div class="m2"><p>می نبودیشان بپا اندر بجز کهنه ازار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دیبه زربفت پوشانیدم آنها را کجا</p></div>
<div class="m2"><p>بر قبا و پیرهنهاشان نبودی پود و تار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بردگان ترک بخشیدم کسانی را که ترک</p></div>
<div class="m2"><p>جز نتق ناوردشان خط رئیس یادگار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>داشتم بر گنجهای گوهر آنها را امین</p></div>
<div class="m2"><p>کز نفایه کس نداند شان سفال آبخوار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بس که بردند از بر من آشکارا و نهان</p></div>
<div class="m2"><p>کیسه ها سیم حلال و بدره ها زر عیار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همچو موران مال من در لانه خود کرده جمع</p></div>
<div class="m2"><p>وانگهی کردند بر من تیز دندانها چو مار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حق مال و نعمت من هیچگون نشناختند</p></div>
<div class="m2"><p>آن سگان نابکار و آن خسان نابکار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کس بمال خویش چندین دشمن انگیزد که من</p></div>
<div class="m2"><p>ای خداوندان مال الاعتبار الاعتبار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دشمنی کردند و بدگوئی بر خاقان مرا</p></div>
<div class="m2"><p>در دل خاقان فکندند از خلاف من غبار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خانمان من در آنروزی که آن هرگز مباد</p></div>
<div class="m2"><p>غارت آن کردی که با من بود همچون یار غار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زر و سیم و تر و خشک من همه بر باد شد</p></div>
<div class="m2"><p>هم بر آن جمله که آتش افتد اندر مرغزار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گنجهای خواسته بی حجتی در خواستند</p></div>
<div class="m2"><p>وز پس این خواسته گشتند جانرا خواستار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فضل کرد ایزد بمن تا بر من از حضمان خویش</p></div>
<div class="m2"><p>جان برون بردم چو مردان از میانشان برکنار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چونکه بر سلطان سلطانان خبر شد حال من</p></div>
<div class="m2"><p>کرد بر نیک آمد من حالی از جیحون گذار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پیش سلطان جهانداران چو بوسیدم زمین</p></div>
<div class="m2"><p>باز بگشاد آسمان بر من زبان اعتذار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زر و گوهر یافتم خلقت ازو چون پیش او</p></div>
<div class="m2"><p>از سرشک دیدگان وز خون دل بردم نثار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر مرادی کز خداوند جهان درخواستم</p></div>
<div class="m2"><p>زو پدید آمد اجابت بی درنگ و بی نثار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دولت و اقبال سلطانی بمن بنمود روی</p></div>
<div class="m2"><p>گفت چون گفتی باندک حاجتی کرد اختصار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>باز دیگر ره توانگر گشتم از احسان او</p></div>
<div class="m2"><p>حج اسلام است مرمرد توانگر را شعار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از خداوند جهان خواهم بقای عمر شاه</p></div>
<div class="m2"><p>ساعتی کان حلقه را در ساعد آرم چون سوار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عدل سلطان جهان خواهم ز جبار جهان</p></div>
<div class="m2"><p>چون بهنگام تضرع بر حجر مالم عذار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در زیارتگاه یثرب برکت عمرش خوهم</p></div>
<div class="m2"><p>زانکه در دنیا نباشد زان مبارکتر مزار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>باری از دیدار تو بی کم خلاف آورده اند</p></div>
<div class="m2"><p>دورتر باشم بسالی و بفرسنگی هزار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا نباید مرمرا پاداشن ایشان نمود</p></div>
<div class="m2"><p>هم توانم کرد حاصل طاعت پروردگار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دست رس دارم که با خصمان خود گر بد کنم</p></div>
<div class="m2"><p>سخت آسان باشدم زایشان برآوردن دمار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>عهد یزدان نشکنم با خلق نکنم هیچ بد</p></div>
<div class="m2"><p>ور بدی کردند با من درگذارم مرد وار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تا زباد صبح در بستان ز آب چشم ابر</p></div>
<div class="m2"><p>بشکفد هر سال گلها را بهنگام بهار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>روی احباب خداوند جهان بادا چو گل</p></div>
<div class="m2"><p>دیده های بدسگالانش چو ابر تندبار</p></div></div>