---
title: >-
    شمارهٔ ۱۱۲ - در مدح وزیر
---
# شمارهٔ ۱۱۲ - در مدح وزیر

<div class="b" id="bn1"><div class="m1"><p>صاحب عادل وزیر شاه معظم</p></div>
<div class="m2"><p>صدر خجسته پی عزیز مکرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرور عالم که هست از نیت نیک</p></div>
<div class="m2"><p>عالمیانرا بشفقت پدر و عم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرم و خوش باش کز قبل اوست</p></div>
<div class="m2"><p>پیش ممالک مرفه و خوش و خرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا سوی در غم نشاط کرد و خرامید</p></div>
<div class="m2"><p>شد در غم بسته بر نواحی در غم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در غم چون بادیه شده ز غم آب</p></div>
<div class="m2"><p>گشت چو دریا پر آب و رفت همه غم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا که بدر غم نیامد او بتماشا</p></div>
<div class="m2"><p>در غمیانرا جز آب دیده نشدنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست کنون بیم آن کز آب فراوان</p></div>
<div class="m2"><p>در غم پنهان شو چو یونان در یم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر همه عالم موفق است بتیمار</p></div>
<div class="m2"><p>شفقت او پرورنده همه عالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آدمئی نیست گر عنایت او نیست</p></div>
<div class="m2"><p>آدمیانرا مگر وصی شد از آدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلق گریزنده و رمیده و طان</p></div>
<div class="m2"><p>باز بعدل وی آمدند فراهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نام عمر زنده کرد و داد بگسترد</p></div>
<div class="m2"><p>نام ستم کرد از نهاد جهان کم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرور هر محفل است و صاحب عادل</p></div>
<div class="m2"><p>چون سر هر سال هست ماه محرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دهر بجز بر رضای او نکند کار</p></div>
<div class="m2"><p>بخت بجز بر مراد او نزند دم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا نبود خط و کلک کار گشایش</p></div>
<div class="m2"><p>شغل همه عالم است مشکل و مبهم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مصلحت شاه و لشکری و رعیت</p></div>
<div class="m2"><p>بر قلم او مفوض است و مسلم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در خور انگشت اوست خاتم دولت</p></div>
<div class="m2"><p>عیش ابد نقش بر نگینه خاتم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا حرم کعبه معظم را هست</p></div>
<div class="m2"><p>حرمت بر آستانهای مقدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باد بحرمت سرای دولت عالیش</p></div>
<div class="m2"><p>چون حرم کعبه شریف معظم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دولت او باد تا قیامت عالی</p></div>
<div class="m2"><p>حکم روانش همیشه نافذ و محکم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا که جهانست او همی . . . باد</p></div>
<div class="m2"><p>نافذ فرمان و فخر دوده آدم</p></div></div>