---
title: >-
    شمارهٔ ۳۱ - در مدح تاج الدین
---
# شمارهٔ ۳۱ - در مدح تاج الدین

<div class="b" id="bn1"><div class="m1"><p>ای یافته تاج نسب از صاحب معراج</p></div>
<div class="m2"><p>هستی به لقب دین همایون ورا تاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همنام تو است و پدر تو بدو خوشنام</p></div>
<div class="m2"><p>جد تو رسول قرشی صاحب معراج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه شرفی تاج تو است از نسب تو</p></div>
<div class="m2"><p>تاجی که نه غصب است نه آورده زتاراج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک تو نه ملکی است بشمشیر گرفته</p></div>
<div class="m2"><p>کی ملک بشمشیر توان کردن از عاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نسل حسین بن علی شاه شهیدی</p></div>
<div class="m2"><p>نز تخمه جمشیدی و نز گوهر مهراج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن شاه که گویند بجنت برد آن را</p></div>
<div class="m2"><p>از جور که مرخون ورا ریخت زاو داج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منهاج سخا و کرم و جود و فتوت</p></div>
<div class="m2"><p>جد تو نهاد دست و توئی رهرو منهاج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از منهج مهر تو بجز خارجی شوم</p></div>
<div class="m2"><p>از امت جدت نکند هیچکس اخراج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طاوس ملایک بنوا مدح تو خواند</p></div>
<div class="m2"><p>اندر قفس سدره چو قمری و چو دراج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گیسوی تو شهبال همای نبوی دان</p></div>
<div class="m2"><p>بوینده چو مشک تبت و تنکت تمغاج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر مدعیان گیسوی مشگین تو بینند</p></div>
<div class="m2"><p>دانند که نز جنس همانست غلیواج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از نور جبین تو بود روز منور</p></div>
<div class="m2"><p>وز گیسوی مشگین سیاه تو شب داج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از روی هواخواهی سادات دلم هست</p></div>
<div class="m2"><p>پیوسته بدیدار شب و روز تو محتاج</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تصویر کنم مدح تو بر خاطر روشن</p></div>
<div class="m2"><p>وز نوک قلم نقش کنم غالبه بر عاج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیباچه دیوان خود از مدح تو سازم</p></div>
<div class="m2"><p>تا هر ورقی گیرد ازو قیمت دیباج</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در مدحت صدر تو منم شوشتری باف</p></div>
<div class="m2"><p>دیگر شعرا آستری باف چو نساج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بی فکرت مداحی صدر تو همه عمر</p></div>
<div class="m2"><p>حاشا که زنم یک مژه را بر مژه با کاج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جفت دل من کرد هوا خواهی سادات</p></div>
<div class="m2"><p>هنگام مزاج تن من خالق ازواج</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نامم بثناگوئی و مدح تو نوشتند</p></div>
<div class="m2"><p>آنگه که سرشته شدم از نطفه امشاج</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا بدرقه از دوستی آل علی نیست</p></div>
<div class="m2"><p>بر قافله دین هدی دیو، نهد باج</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کعبه است دل من که بدان کعبه نیاید</p></div>
<div class="m2"><p>بیدوستی آل نبی قافله حاج</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرگز بسوی کعبه معمور دل من</p></div>
<div class="m2"><p>حجاجی ملعون نخوهد گشتن حجاج</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا تاج بود زینت فرق سر شاهان</p></div>
<div class="m2"><p>وانداختن تیر بود رسم بآماج</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو تا حور ملک شرف بادی و اعدات</p></div>
<div class="m2"><p>بر آتش غم سوخته همواره چو در تاج</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آماجگه تیر عنا باد حسودت</p></div>
<div class="m2"><p>وز خون جگر برزخ اعدای تو امواج</p></div></div>