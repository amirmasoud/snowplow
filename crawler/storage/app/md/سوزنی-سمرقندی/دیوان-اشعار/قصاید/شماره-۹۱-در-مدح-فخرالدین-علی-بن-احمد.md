---
title: >-
    شمارهٔ ۹۱ - در مدح فخرالدین علی بن احمد
---
# شمارهٔ ۹۱ - در مدح فخرالدین علی بن احمد

<div class="b" id="bn1"><div class="m1"><p>ایدل ز عشق یار چو از دانه نار باش</p></div>
<div class="m2"><p>گر دانه نار باشد گو دانه نار باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ورا شک من ز جور تو چون ناروان شود</p></div>
<div class="m2"><p>در عشق آندو لعل چو یکدانه نار باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر جان خیال صورت جانان نگار کن</p></div>
<div class="m2"><p>وندر میان جان سمران نگار باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل هوای عذرا وامق چگونه داشت</p></div>
<div class="m2"><p>تو همچنان بر آن بت مشکین عذار باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند مستی از می مهر و هوای او</p></div>
<div class="m2"><p>تا پی ز پی خطا ننهی هوشیار باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست از تو شستم ایدل و دادم ترا بدوست</p></div>
<div class="m2"><p>در زلف او قرار کن و استوار باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار ار برأی تو نرود روی ازو متاب</p></div>
<div class="m2"><p>در روی کار بنگر و بر رأی یار باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر باد بی قرار کند زلف دوست را</p></div>
<div class="m2"><p>در خط گریز و گاه طلب بیقرار باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا در تن و روان تو تاب و توان در است</p></div>
<div class="m2"><p>در زلف مشکبوی و خط مشکبار باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با بوی مشک و با غزل خوش بمجلس آی</p></div>
<div class="m2"><p>هشیار گرد و مادح صدر کبار باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرزند فخر دین که ز جان نبی بدو</p></div>
<div class="m2"><p>آمد ندا که دین مرا افتخار باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دهقان علی که جان علی گویدش ز خلد</p></div>
<div class="m2"><p>با حضم دین همیشه بکف ذوالفقار باش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای صدر مهتران و بزرگان روزگار</p></div>
<div class="m2"><p>خوش عیش و خوش طبیعت و خوش روزگار باش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پروردگان غر شدند از نعیم تو</p></div>
<div class="m2"><p>دایم غریق نعمت پروردگار باش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کار بزرگواران شادی و عشرتست</p></div>
<div class="m2"><p>تا فارغیت باشد مشغول کار باش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دینار بار بر کف آزاده زادگان</p></div>
<div class="m2"><p>آزاده وار با کف دینار بار باش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در دهر کار به زشراب و شکار نیست</p></div>
<div class="m2"><p>زین هر دو کار دایم با اختیار باش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گاهی شراب نوش کن از سیم ساعدان</p></div>
<div class="m2"><p>وز بسدین نگاران شکر شکار باش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از عشق و از عقار طرب را سبب گزین</p></div>
<div class="m2"><p>در سینه عشق و در کف جام عقار باش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خوبان پیاده پیش تو باشند صف زده</p></div>
<div class="m2"><p>بر مرکب نشاط دل خود سوار باش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در دهر نیست چون تو یکی ور بود هزار</p></div>
<div class="m2"><p>از مهتری تو صدر و سر صد هزار باش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خورشید مهترانی و جمشید سروران</p></div>
<div class="m2"><p>چون این جهان فروز و چو آن ملکدار باش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خورشیدوار از فلک مهتری بتاب</p></div>
<div class="m2"><p>بر تخت کامرانی جمشیدوار باش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اندر جهان چو بی هنری عیب و عار نیست</p></div>
<div class="m2"><p>با فخر و با هنر زی و بی عیب و عار باش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فخر از هنر نمای و باهل هنر گرای</p></div>
<div class="m2"><p>وز عیب و عار بی هنری بر کنار باش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اقبال و عز و جاه و جوانی قرین تست</p></div>
<div class="m2"><p>با هر قرین بمهر زمانه گذار باش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هستند هر چهار ترا چون چهار طبع</p></div>
<div class="m2"><p>جاوید بر طبیعت این هر چهار باش</p></div></div>