---
title: >-
    شمارهٔ ۴۹ - در مدح مؤید
---
# شمارهٔ ۴۹ - در مدح مؤید

<div class="b" id="bn1"><div class="m1"><p>ای عامل خراج کفایت نمای راد</p></div>
<div class="m2"><p>دستور خسرو و شرف دست میرزاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید جاودان مؤید یمین دین</p></div>
<div class="m2"><p>کز سایه یمین تو زفتان شوند راد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رادیست حرفت کف و کلک و بنان تو</p></div>
<div class="m2"><p>خلقی زحرفت کف و کلک و بنانت راد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا از بنان و کلک و کف تو بمن رسید</p></div>
<div class="m2"><p>تشریف و خلعتی که نشاید گرفت و داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مادح نماند جز من و ممدوح جز توئی</p></div>
<div class="m2"><p>من مادح از نژاد و تو ممدوح از نژاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان مهتران نئی تو که در خدمت و ثنات</p></div>
<div class="m2"><p>بستن میان نشاید و نتوان زبان گشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست و در دل تو گشاد است و طبع نیز</p></div>
<div class="m2"><p>پاینده چون در دل و دستت گشاده باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعر مرا هر آینه از هزل چاشنی</p></div>
<div class="m2"><p>ماند بجای بلبل و گشنیز و بغمخواد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بر حسود تو برم آن چاشنی بکار</p></div>
<div class="m2"><p>کوبم در اجازه که تا بگذرم چو باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر کیقباد و کسری گردد حسود تو</p></div>
<div class="m2"><p>صد . . . در . . . زن کسری و کیقباد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناگفته خوبتر بتو از حاسدان تو</p></div>
<div class="m2"><p>ایشان کنید خود که از ایشان کنند یاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از حب خویش یاد کنم وآنچه بایدم</p></div>
<div class="m2"><p>خواهم ز مجلس تو چو شاگرد از اوستاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای صدر اهل فضل مرا نان و جامه نیست</p></div>
<div class="m2"><p>در گردنم هم از غله خانه غل فتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر مجلس رفیع تو اطناب قصه را</p></div>
<div class="m2"><p>این بنده رفع کرد و بر ایجاب دل نهاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ده ساله کدخدائی شاهان بیک زمان</p></div>
<div class="m2"><p>داری و بیش دارد ازین امر تو نفاذ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یک ماهه کدخدائی کردم ز تو سوآل</p></div>
<div class="m2"><p>جودت سوآل من باجابت قرین کناد</p></div></div>