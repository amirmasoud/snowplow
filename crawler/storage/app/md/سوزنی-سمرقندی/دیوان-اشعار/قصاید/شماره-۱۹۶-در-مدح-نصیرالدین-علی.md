---
title: >-
    شمارهٔ ۱۹۶ - در مدح نصیرالدین علی
---
# شمارهٔ ۱۹۶ - در مدح نصیرالدین علی

<div class="b" id="bn1"><div class="m1"><p>نصیر دین که چشم پادشائی</p></div>
<div class="m2"><p>نبیند چون تو فرخ کدخدائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان را کدخدائی جز تو نبود</p></div>
<div class="m2"><p>چنان چون نیست جز یزدان خدائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر گویم بهمت آسمانی</p></div>
<div class="m2"><p>بمن بر هر کسی گیرد خطائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجنب همت تو آسمان هست</p></div>
<div class="m2"><p>چو دست آسی بپیش آسیائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان کز همت عالیت زیبد</p></div>
<div class="m2"><p>نها دستی یکی عالی بنائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهات تو بنا کردی پس آنگاه</p></div>
<div class="m2"><p>همی خواهی جهانی را سرائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر این زیبا جهان خرم اندر</p></div>
<div class="m2"><p>بران چندانکه داری کام ورائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نصیر دین یزدانی و دین را</p></div>
<div class="m2"><p>نیاید چون تو کس نصرت فزائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرد را نیست اندر هر طریقی</p></div>
<div class="m2"><p>چو رای روشن تو رهنمائی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بجز بر کلک و بر کافی کف تو</p></div>
<div class="m2"><p>جهان را نیست بندی و گشائی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عطای ایزدی بر خلق و کس نیست</p></div>
<div class="m2"><p>که نگرفت از کف رادت عطائی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان فانی شدستی لیکن الحق</p></div>
<div class="m2"><p>بجاه تو همی ماند بقائی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان خواهد بقای دولت تو</p></div>
<div class="m2"><p>بدان تا مرورا ناید فنائی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ببحر مدح ت با صد تکلف</p></div>
<div class="m2"><p>نیارد عنصری زد آشنائی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بجز طبع سخن سنجان کامل</p></div>
<div class="m2"><p>نباشد مدحتت را آشنائی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود وصف کمال تو بحدی</p></div>
<div class="m2"><p>بود قصر جلال ت بجائی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که نه آنجا رسد هرگز خیالی</p></div>
<div class="m2"><p>نه ره دارد درینجا هیچ رائی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>توام گستاخ کردی تا درین بحر</p></div>
<div class="m2"><p>بدیهه میزنم دستی و پائی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دعا گویم ترا زین پس چو شوان</p></div>
<div class="m2"><p>سزای صدر تو گفتن ثنائی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خدا آنچه ترا به باد بدهاد</p></div>
<div class="m2"><p>ازین بهتر ندانستم دعائی</p></div></div>