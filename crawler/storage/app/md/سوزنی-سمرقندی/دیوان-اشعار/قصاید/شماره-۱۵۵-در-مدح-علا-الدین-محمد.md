---
title: >-
    شمارهٔ ۱۵۵ - در مدح علاء الدین محمد
---
# شمارهٔ ۱۵۵ - در مدح علاء الدین محمد

<div class="b" id="bn1"><div class="m1"><p>هوای آل نبی را دل منست وطن</p></div>
<div class="m2"><p>دمی مباد که بی این هوا بود دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلام دشمن خویشم بدین هوا که مراست</p></div>
<div class="m2"><p>اگر بطعنه هوادار خواندم دشمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین هوا که منم رنگ و بوی بدعت نیست</p></div>
<div class="m2"><p>که این هوا همه عین شریعت است و سنن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه این هوا چو هوائیست تیره و تاری</p></div>
<div class="m2"><p>که این هوا چو هوائیست صافی و روشن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من از هوای جگرگوشگان پیغمبر</p></div>
<div class="m2"><p>نه برکنم دل تا جان بود موافق تن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه هوای من آنست تا شود ماهر</p></div>
<div class="m2"><p>بمدح آل نبی طبع من بنظم سخن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا فصاحت حسان و من بر آل نبی</p></div>
<div class="m2"><p>ثنا بگویم چه من فصیح و چه الکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن چه به که مزین شود مرا دیوان</p></div>
<div class="m2"><p>بمدح عترت کرار شیر شیر اوژن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا رضای عمر سیر اجل سعید</p></div>
<div class="m2"><p>که شاه آل حسن بود و فخر آل زمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اجل میر خراسان که نام او سمر است</p></div>
<div class="m2"><p>بنیکوئی بعراق و حجاز و شام و یمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر زبان خود از یاد او فرو بندم</p></div>
<div class="m2"><p>بگوش من مر سادا حدیث من ز دهن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز شاه آل حسن سید اجل چو مرا</p></div>
<div class="m2"><p>فراق داد جفای زمانه ریمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کمر بخدمت شاه حسینیان بندم</p></div>
<div class="m2"><p>که در پناه ویند اهل بیت آل حسن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>علاء دین پسر سید اجل حیدر</p></div>
<div class="m2"><p>که شاه حیدر زور است روز جنگ و فتن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>محمدی که محمد که مفخر رسل است</p></div>
<div class="m2"><p>کند تفاخر ازو روز حشر پاداشن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گزیده ای که همه قول اوست مستحکم</p></div>
<div class="m2"><p>ستوده ای که همه فعل اوست مستحسن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>میان عترت و اولاد مرتضی و نبی</p></div>
<div class="m2"><p>چو بدر باشد بر آسمان میان پرن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>میان انجمن سروران روی زمین</p></div>
<div class="m2"><p>چو سرو باشد در بوستان میان چمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بزرگواری آزاده ای که در گیتی</p></div>
<div class="m2"><p>ز بار منتش آزاد نیست یک گردن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز بوی خلقش ورد و سمن دمد در حال</p></div>
<div class="m2"><p>ز خار خارا اندر مه دی و بهمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دم منازع او زین بود چو بهمن و دی</p></div>
<div class="m2"><p>رخ متابع او زان بود چو ورد و سمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بابر بهمن ماند کفش اگر بارد</p></div>
<div class="m2"><p>ز ابر بهمن زر عیار و در عدن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ایا سپهر معالی و صدر آل علی</p></div>
<div class="m2"><p>تراست خلق و خصال علی بسرو علن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو آن عدیم همالی که نیست در عالم</p></div>
<div class="m2"><p>همالت از همه آل پیمبر ذوالمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دلی که مهر و هوای تو اندران دل نیست</p></div>
<div class="m2"><p>در او چه دین خدای و چه کیش اهریمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر آستان تو بالین سر کنم ز شرف</p></div>
<div class="m2"><p>رسد بگنبد پیروزه گون بی روزن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز دور گنبد پیروزه رنگ تا باشد</p></div>
<div class="m2"><p>شب سیاه بروز سپید آبستن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شب بقای ترا باد روز دولت و عز</p></div>
<div class="m2"><p>شب بقای حسود تو روز ذل و محن</p></div></div>