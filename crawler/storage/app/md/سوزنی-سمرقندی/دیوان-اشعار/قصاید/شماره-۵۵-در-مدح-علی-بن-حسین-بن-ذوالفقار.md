---
title: >-
    شمارهٔ ۵۵ - در مدح علی بن حسین بن ذوالفقار
---
# شمارهٔ ۵۵ - در مدح علی بن حسین بن ذوالفقار

<div class="b" id="bn1"><div class="m1"><p>ای شهریار شرق و شه آل ذوالفقار</p></div>
<div class="m2"><p>با شاه ذوالفقار بنام و نبرد یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ذوالفقار و بازوی تو آفرین کند</p></div>
<div class="m2"><p>روز نبرد جان علی شیر ذوالفقار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روح از هوا بحرب علی گفت لافتی</p></div>
<div class="m2"><p>الاعلی چو شد زعلی کشته ذوالحمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اکنون همان منادی روحست و بر تو جست</p></div>
<div class="m2"><p>کز تست زنده نام حسین بن ذوالفقار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید حمله حشم تو رعیت است</p></div>
<div class="m2"><p>بیش شماره ذره شمار سپاه دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشیدوار نیزه تو نور افکند</p></div>
<div class="m2"><p>جمشیدواران بنشینی بصدر بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر سر حسام تو باشد قرار ملک</p></div>
<div class="m2"><p>وندر نیام نیست حسام ترا قرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خدمت رکاب تو گردان لشکرند</p></div>
<div class="m2"><p>با همت تهمتن و زور سفندیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هریک بگاه حمله چو صرصر مصاف گر</p></div>
<div class="m2"><p>در حمله چون سکندر گرد مصاف وار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زارست کار آنکه بوقت مبارزت</p></div>
<div class="m2"><p>با کمترین غلام تو افتد بکارزار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از شیر رایت تو درافتد بروز حرب</p></div>
<div class="m2"><p>ترس و هراس و بیم بشیران مرغزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز در مصاف دشمن تو سیر طعمه نیست</p></div>
<div class="m2"><p>شیر اجل چو تیز کند پنجه بر شکار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم در میان بیشه زتأثیر عدل تو</p></div>
<div class="m2"><p>آهو بشیر سر کند و بره شیر خوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیش سنان نیزه سندان گداز تو</p></div>
<div class="m2"><p>چون عنکبوت خانه بود آهنین حصار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در پیش اژدهای دمان در محاربت</p></div>
<div class="m2"><p>بر تار عنکبوت دو اسبه شوی سوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در حصن و آهنی بامان باشد آنکه بست</p></div>
<div class="m2"><p>از عنکبوت هیبت تو بر میانش تار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر دشمنی که کین تو در سینه راز داشت</p></div>
<div class="m2"><p>شد بر زبان خنجر تو رازش آشکار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اندر مصاف رستم دستانی ارچه خصم</p></div>
<div class="m2"><p>چون روزگار حیله و دستان برد بکار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گوئی که در تو گفت امام سخن رشید</p></div>
<div class="m2"><p>ای در مصاف رستم دستان روزگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن روزگار خویش بآزادگی گذاشت</p></div>
<div class="m2"><p>کز روزگار بندگیت کرد اختیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یک ساعت سخای یمین و یسار تو</p></div>
<div class="m2"><p>تا تو یمین خویش ندانستی از یسار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در چشم همیت تو کزو دور چشم بد</p></div>
<div class="m2"><p>سیم حلال بی خطر است و زر عیار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از بسکه خازن تو بزوار زر دهد</p></div>
<div class="m2"><p>باشد چو تنگ زر کف دستش پر از نگار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باد سخاوت تو اگر بر زمین وزد</p></div>
<div class="m2"><p>بر سائلت خزانه قارون کند نثار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آبادتر ولایت توران بعهد تو</p></div>
<div class="m2"><p>کز عدل تست کشور توران بهشت وار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با سرکشان توران آهنگ باده کن</p></div>
<div class="m2"><p>ای باده هوای تو بی زحمت خمار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا میر مجلس تو بساقی کند خطاب</p></div>
<div class="m2"><p>خیز ای بهشتی و بمن آن جام می بیار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا آسمان بشکل چو لشکر گهی است گرد</p></div>
<div class="m2"><p>سیارگان چو لشکر و خورشید شهریار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بارند لشکر تو ز سیارگان فزون</p></div>
<div class="m2"><p>بگرفته زین کنار جهان تا بدان کنار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو شهریاروار چو خورشید آسمان</p></div>
<div class="m2"><p>گسترده نور عدل بهر کشور و دیار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بادا هزار سال بشادی و خرمی</p></div>
<div class="m2"><p>بر هر یک از هزار زیادت شده هزار</p></div></div>