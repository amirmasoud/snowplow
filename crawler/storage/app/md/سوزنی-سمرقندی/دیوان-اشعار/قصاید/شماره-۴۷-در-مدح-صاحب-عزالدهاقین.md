---
title: >-
    شمارهٔ ۴۷ - در مدح صاحب عزالدهاقین
---
# شمارهٔ ۴۷ - در مدح صاحب عزالدهاقین

<div class="b" id="bn1"><div class="m1"><p>ترک من مهر و وفا سیرت و آیین نکند</p></div>
<div class="m2"><p>تا که بر برگ گل از غالیه آذین نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر آذین وی آیین وفا دست امید</p></div>
<div class="m2"><p>تا که نومید ز آذین بود آیین نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیقین دانم کان ترک ستمکاره من</p></div>
<div class="m2"><p>از پی رغم مرا آن کند و این نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه خط بر رخ آن دلبر من خواهد کرد</p></div>
<div class="m2"><p>گر بود مانی بر روی بت چین نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف پرچینش چه بس فتنه و بیداد که کرد</p></div>
<div class="m2"><p>چو خط آرد دگر آن زلف پر از چین نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کند از غالیه پیراهن گل را پر چین</p></div>
<div class="m2"><p>تا کس آن باغ پر از گل را گلچین نکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود خطا باشد انصاف همی باید داد</p></div>
<div class="m2"><p>کس چنان باغ پر از گل را پرچین نکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خط نامده هرچند سخن دانم راست</p></div>
<div class="m2"><p>زلف مشکینش خطا داند و تمکین نکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو در آرد خط مشکین و برآراید رخ</p></div>
<div class="m2"><p>زلف یک لحظه خلاف خط مشکین نکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رازها گوید هر سوی خط آورده چنان</p></div>
<div class="m2"><p>کان بجز صاحب ما عز دهاقین نکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هنری عین دهاقین که خداوند هنر</p></div>
<div class="m2"><p>بجز او را بخداوندی تعیین نکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آسمان پایه تخت شرف و قدر ورا</p></div>
<div class="m2"><p>جای جز فرقگه فرقد و پروین نکند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از بزرگی و زاحسان که کند بر همه خلق</p></div>
<div class="m2"><p>از همه خلق کسش نیست که تحسین نکند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دین پاکیزه و عقل و خرد کامل او</p></div>
<div class="m2"><p>مرورا جز همه نیکوئی و تلقین نکند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کین او کان بلا گردد در سینه خصم</p></div>
<div class="m2"><p>زانکه او با همه کس مهر کند کین نکند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دشمن جاه ورا زهره و یارا نبود</p></div>
<div class="m2"><p>کانچه او گوید در ساعت و در حین نکند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن کند با سر دشمن چو قلم برگیرد</p></div>
<div class="m2"><p>که قلم کند شود بر وی و مسکین نکند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باده کین ورا هر که بنوشد عجب است</p></div>
<div class="m2"><p>گر عسل باشد ایامش غسلین نکند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لفظ شیرین ورا هر که بنوشد عجب آنک</p></div>
<div class="m2"><p>تلخی گوش بگوش اندر شیرین نکند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از زمین سایه علم خود اگر بردارد</p></div>
<div class="m2"><p>تا قیامت ز می از زلزله تسکین نکند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا صبا رایحه خلق در او در ندمد</p></div>
<div class="m2"><p>چهره باغ پر از تازه ریاحین نکند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ابر نایافته از کف جوادش تعلیم</p></div>
<div class="m2"><p>لؤلؤ افشانی در باغ و بساتین نکند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر که جود و کرم او بعیان دیده بود</p></div>
<div class="m2"><p>بیهده گوش بافسانه افشین نکند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسخا صید کند کف جوادش دل خلق</p></div>
<div class="m2"><p>ز سخا کس بجز او با شه شاهین نکند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر که میزان سخن سنجی داند کردن</p></div>
<div class="m2"><p>بجز از راستی مدحش شاهین نکند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرکب دانش و فضل و هنر و دولت را</p></div>
<div class="m2"><p>بجز از بهر ورا دست و زبان زین نکند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شاه شطرنج کفایت را یک بیدق او</p></div>
<div class="m2"><p>لعب کمتر زد و اسب و رخ و فرزین نکند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر دلی کز قبل شادی او شاد بود</p></div>
<div class="m2"><p>گرش طوفان غمان بارد غمگین نکند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر کرا عقل و بصر باشد خاک در او</p></div>
<div class="m2"><p>بجز از سرمه دو چشم جهان بین نکند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا بدانگه که سر و کار شیاطین از نار</p></div>
<div class="m2"><p>سجده بر آدم پیدا شده از طین نکند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باد بر کل بنی آدم فرمانش روا</p></div>
<div class="m2"><p>که همی کار بفرمان شیاطین نکند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا که از ملک بود نام و نشان از آئین</p></div>
<div class="m2"><p>کس جز او تربیت ملک بآیین نکند</p></div></div>