---
title: >-
    شمارهٔ ۵۴ - در مدح رکن الدین
---
# شمارهٔ ۵۴ - در مدح رکن الدین

<div class="b" id="bn1"><div class="m1"><p>هم ز آفریدگان و هم از آفریدگار</p></div>
<div class="m2"><p>بر شاه باد هر نفسی آفرین شمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهی که اصل و فرع نهال نهاد او</p></div>
<div class="m2"><p>از آفرین بنشو و نما یافت برگ و بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشد ملک ملقن هر مالک سخن</p></div>
<div class="m2"><p>در نظم آفرین ملک در سرای تار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی آفرین شاه نباشد بهیچ وقت</p></div>
<div class="m2"><p>هیچ آفرین سرائی در هیچ روزگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جایت اگر ندارد هیچ آفریده را</p></div>
<div class="m2"><p>بی آفرین شه ملک آفریده دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارای ملک مشرق و چین رکن دین قلج</p></div>
<div class="m2"><p>تمغاج خان فتح یمین و ظفر یسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاهی که با عطای یمین و یسار او</p></div>
<div class="m2"><p>دریا و کوه را نبود عدت و یسار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاهنشه سلاطین مسعود بن حسن</p></div>
<div class="m2"><p>مسعود بخت شاه حسن خلق شهریار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شهریار شهر سمرقند را نداشت</p></div>
<div class="m2"><p>از شهرهای روی زمین هیچ شهریار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا داد ملک شهر سمرقند شد ترا</p></div>
<div class="m2"><p>تو دار ملک داری و اعدات ملک دار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حضرت بهشت روی زمین بود و از تو شد</p></div>
<div class="m2"><p>اندر بهشت روی زمین آسمان نگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور ملک تو نشان زبهشت و زآسمان</p></div>
<div class="m2"><p>شهر از بهشت خرم و از آسمان حصار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از منظر حصار چو خورشید از آسمان</p></div>
<div class="m2"><p>تابی ز برج عدل و منور کنی دیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خورشید ملک و سایه یزدان توئی شها</p></div>
<div class="m2"><p>خورشید و سایه ای که بشبدیز شد سوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از نور و نار مهر و هوای تو خلق را</p></div>
<div class="m2"><p>دل هست ازان قیاس که باشد زدانه نار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خورشید نور و نار بود نور و نار باش</p></div>
<div class="m2"><p>باشد ز بهر مصلحت خلق نور و نار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در روز کارزار تو زار است کار خصم</p></div>
<div class="m2"><p>خصم از کجا و کی و کدام و چه کارزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خورشید وار از فلک خسروی بتاب</p></div>
<div class="m2"><p>هم روز بار دادن و هم روز کارزار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا ذره وار بر تو موالی دهند عرض</p></div>
<div class="m2"><p>تا منهزم شوند معادی ستاره وار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از بیشمار یاغی و طاغی که جمع شد</p></div>
<div class="m2"><p>شمشیر تو کشید قلم دو خط شمار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون در شکار شیر نمودی یگانگی</p></div>
<div class="m2"><p>گشتند جمله شیر شکاران تراشکار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از هیبت تو شیر شکاران نهان شدند</p></div>
<div class="m2"><p>زانسان که تا بحشر نگردند آشکار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>طاقی ز ملکداران باقی بمان بملک</p></div>
<div class="m2"><p>وز تیغ جان طاغی و یاغی ز تن برآر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر اهل بغی و طغیان چون بر گوزن گور</p></div>
<div class="m2"><p>تیری همی گشای و سنانی همی گذار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کام دل از هزار یکی . . . ده ای بران</p></div>
<div class="m2"><p>تا از مخالفانت نماند یک از هزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دنیا که هست مزرعه حرت در او</p></div>
<div class="m2"><p>از بهر داس فضل ملک تخم عدل کار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عدلست و فضل و مرحمت و بر و مکرمت</p></div>
<div class="m2"><p>کار تو شاه و هر چه جز اینست نیست کار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کار جهان اگر گذرانست باک نیست</p></div>
<div class="m2"><p>مگذر از این جهان و جهانرا همی گذار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای سوزنی برشته خاطر برشته کن</p></div>
<div class="m2"><p>در مدح شاه عالمیان در شاهوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر پادشاه عالمیان باد آفرین</p></div>
<div class="m2"><p>هم ز آفریدگان و هم از آفریدگار</p></div></div>