---
title: >-
    شمارهٔ ۲۵ - ای پسر
---
# شمارهٔ ۲۵ - ای پسر

<div class="b" id="bn1"><div class="m1"><p>ای پسری کان دو زلف بر زده داری</p></div>
<div class="m2"><p>وآتش رویت به زلف در زده داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرزده زلف تا بعشق رخ خویش</p></div>
<div class="m2"><p>سرزده ما را بزلف سرزده داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرزده عشق من از نسیم دو زلفت</p></div>
<div class="m2"><p>تا که سر زلف ای پسر زده داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیرگی از زلف و روشنائی از روی</p></div>
<div class="m2"><p>بر زبر صبح شام برزده داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رایت خوبی چو برفرازی و رخسار</p></div>
<div class="m2"><p>از بر خورشید زده داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بارگه عسکریست دولت شیرینت</p></div>
<div class="m2"><p>بر صدف درو بر شکر زده داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیر مژه ات را چنانکه بر دل تنها</p></div>
<div class="m2"><p>بر تن و بر جان و بر جگر زده داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دو لب ارغوان و لاله ز عشقت</p></div>
<div class="m2"><p>بر دو رخ من سرای سرزده داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو رخ چون آذر تو یکنظرم برد</p></div>
<div class="m2"><p>سر ز گریبان ناز بر زده داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من در خدمت ز دستم و نتوان گفت</p></div>
<div class="m2"><p>جز در خدمت کدام در زده داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنده پذیری کن و مگوی بجانت</p></div>
<div class="m2"><p>رو که جز این در دگر زده داری</p></div></div>