---
title: >-
    شمارهٔ ۱۴ - خواهی بوفا گوش بتا خواهی کین توز
---
# شمارهٔ ۱۴ - خواهی بوفا گوش بتا خواهی کین توز

<div class="b" id="bn1"><div class="m1"><p>بنمود بمن روی نگارین خود امروز</p></div>
<div class="m2"><p>دلبند من آن کرد که مه روی کله دوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آرزوی روی نگاریش بدم دی</p></div>
<div class="m2"><p>آن آرزوی دینه من راست شد امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میتافت بکف رشته و میدوخت بسوزن</p></div>
<div class="m2"><p>ترک کله آن روی چو روی گل نوروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من شسته بنظاره و انگشت همی گز</p></div>
<div class="m2"><p>آب از مژه بگشاده و غلطان شده چون کوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که ایا با تو دلم چون قد تو راست</p></div>
<div class="m2"><p>چون زلف تو شد پشت من اندر غم تو کوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندین غم تو خوردم و ناز تو کشیدم</p></div>
<div class="m2"><p>از عشق من و ناز خود آگاه نئی نوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیدا نتوانست جواب سخنم داد</p></div>
<div class="m2"><p>از شرم برافروخت دو رخسار دل افروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیروزه نگین خاتم از انگشت بمن داد</p></div>
<div class="m2"><p>یعنی که شود عاقبت کار تو پیروز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من بر سر میدان تو گردانم چون گوی</p></div>
<div class="m2"><p>خواهی به وفا کوش بتا خواهی کین توز</p></div></div>