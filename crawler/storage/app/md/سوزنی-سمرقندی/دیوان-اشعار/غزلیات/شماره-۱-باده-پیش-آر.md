---
title: >-
    شمارهٔ ۱ - باده پیش آر
---
# شمارهٔ ۱ - باده پیش آر

<div class="b" id="bn1"><div class="m1"><p>ساقیا پیش آر باز آن آب آتش‌فام را</p></div>
<div class="m2"><p>جام گردان کن ببر غم‌های بی‌انجام را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآنکه ایام نشاط و عشرت و شادی شده است</p></div>
<div class="m2"><p>بد بود بیهوده ضایع کردن این ایام را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجلسی در ساز در بستان و هر سوئی نشان</p></div>
<div class="m2"><p>لعبتان گلرخ و حوران سیم اندام را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده پیش آور که هنگامست اینک باده را</p></div>
<div class="m2"><p>هیچگون روی محابا نیست این هنگام را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خام طبع است آنکه میگوید بچنگ و کف نگیر</p></div>
<div class="m2"><p>زلفکان خم خم و جام نبید خام را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجلس عیش و طرب برساز و چون برساختی</p></div>
<div class="m2"><p>پیش خوان آن مطرب مه روی طوبی نام را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کجا طوبی بود آنجا بود خلد برین</p></div>
<div class="m2"><p>نزد ما پیغمبر آورده است این پیغام را</p></div></div>