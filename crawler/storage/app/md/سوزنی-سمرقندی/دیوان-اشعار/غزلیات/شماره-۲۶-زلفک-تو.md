---
title: >-
    شمارهٔ ۲۶ - زلفک تو
---
# شمارهٔ ۲۶ - زلفک تو

<div class="b" id="bn1"><div class="m1"><p>چرا کند بسر زلفک تو گل سپری</p></div>
<div class="m2"><p>چو کرد بایدش از باد پیش گل سپری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بوی زلف تو و رنگ و بوی گل پسرا</p></div>
<div class="m2"><p>شگفت نیست که گردند مشک و گل سپری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل رخ تو ندانم چرا همی سازد</p></div>
<div class="m2"><p>بگرد ماه بر از مشک ناب حلقه گری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که تا به بینم زلف تو و بگریم زه</p></div>
<div class="m2"><p>بدین شکنج پذیری بد است و حلقه وری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه وقت بوسه دهم بر گل دو عارض تو</p></div>
<div class="m2"><p>گرفته حلقه زلف تو لاله برگ طری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا چه زهره و یارای این سخن باشد</p></div>
<div class="m2"><p>گزاف لافی گفتم بدین گشاده دری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا پسند ندارم ز لعب زلف و رخت</p></div>
<div class="m2"><p>غزل سرائی بر هر دوان بلفظ دری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس عزیزی و بی باکی ای پسر گویم</p></div>
<div class="m2"><p>بجان بنده یا بجان بنده دری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزار بار بگویم که راز عشق ترا</p></div>
<div class="m2"><p>نهان کنم نکنم بیدلی و پرده دری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بیدلان بسر کار خویش باز روم</p></div>
<div class="m2"><p>چو ناگهان بسر کوی بنده درگذری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فراق تو اسفا گوی کرد خلفی را</p></div>
<div class="m2"><p>بدانسبب که ز یوسف بسی تو خوبتری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلا و فتنه و بیداد تو گرفت جهان</p></div>
<div class="m2"><p>پس ای پسر تو ستمکاره چرا عمری</p></div></div>