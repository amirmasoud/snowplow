---
title: >-
    شمارهٔ ۴ - عهد و وفا
---
# شمارهٔ ۴ - عهد و وفا

<div class="b" id="bn1"><div class="m1"><p>ای شده عهد تو بر کینه و پیکار درست</p></div>
<div class="m2"><p>بوفا عهده تو ناآمده یکبار درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با من ار عهد ترا نیست درستی و وفا</p></div>
<div class="m2"><p>هست با تو بوفا عهد من ای یار درست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بت دلداری و من عاشق دلداده تو</p></div>
<div class="m2"><p>عهد من با بود چاره و ناچار درست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر مرا عهد تو ایدوست شکسته است رواست</p></div>
<div class="m2"><p>آن شکسته است که ندهمش ببسیار درست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعزیزیست مرا عهد تو هم قیمت زر</p></div>
<div class="m2"><p>زرخی زرد و شکسته نه چو دینار درست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای نمودار ز بتخانه فرخار بما</p></div>
<div class="m2"><p>بتو گردد صفت لعبت فرخار درست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاروانهای تبت دارد زلف تو بهم</p></div>
<div class="m2"><p>بیکی تار شکسته بیکی تار درست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شکنهای سیه جعد تو بای پرسید</p></div>
<div class="m2"><p>خبر گمشدگان ره تاتار درست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه در حسن و مال تو بدیدم عیان</p></div>
<div class="m2"><p>آنچه از یوسف مصریست باخبار درست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ترا گویم صد یوسف گویم که بدین</p></div>
<div class="m2"><p>صد یک از وصف تو شد گفته مپندار درست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با من اوصاف تو نایافته گر رو بکنم</p></div>
<div class="m2"><p>پیش دهقان اجل احمد سمسار درست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هنری عین دهاقین که کجا و چه خرد</p></div>
<div class="m2"><p>جز بعین هنرش ندهد دیدار درست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن خداوندی که رای و روش روشن اوست</p></div>
<div class="m2"><p>به همه شغل صواب و به همه کار درست</p></div></div>