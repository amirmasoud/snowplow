---
title: >-
    شمارهٔ ۲۷ - دو نرگس او
---
# شمارهٔ ۲۷ - دو نرگس او

<div class="b" id="bn1"><div class="m1"><p>بسم ز نرگس سیراب و لاله خودروی</p></div>
<div class="m2"><p>که نرگس بت من لاله درکشید بروی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیاه و لعل چو لاله است نرگس بت من</p></div>
<div class="m2"><p>سیه چو روز من از عشق و لعل چون رخ اوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بآینه نگرید آن یگار و دید در انو</p></div>
<div class="m2"><p>بنور نرگس سیراب و لاله خود روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد آن دو نرگس او فتنه بر دو عارض او</p></div>
<div class="m2"><p>چو من بر آن صنم سرو قد مشکین موی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جوی نرگس بر لاله راند او بر جزع</p></div>
<div class="m2"><p>چنانکه گشت هان لاله را و نرگس موی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ستاره نامی و مه عارضی و غالیه موی</p></div>
<div class="m2"><p>مه و ستاره گرفت از تو نور و غالیه بوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ستاره بارم خویش از غم آن</p></div>
<div class="m2"><p>که تو بفالیه مه را بپوشی ای مه روی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منجمی را گفتم که هیچ نجم فلک</p></div>
<div class="m2"><p>بود چو نجم کله دوز پیش من بر گوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جوابداد که بر آستان حسن و جمال</p></div>
<div class="m2"><p>یکیست نجم کله دوز و تو منجم اوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منجم توام ای نجم آسمان جلال</p></div>
<div class="m2"><p>همیشه از نظر وصل تو سعادت جوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بچشم دل نظری کن بمن بین که مرا</p></div>
<div class="m2"><p>ز چشم سر بدو رخ بر روان شدست دو جوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بآب دیده چو من خویشتن همی شویم</p></div>
<div class="m2"><p>تو دل ز مهر و وفای من ای دو دیده مشوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بمن نویس یکی نامه پیش از آنکه رخت</p></div>
<div class="m2"><p>ز خط مشگین چوگان بزد بسیمین گوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیاد رویه نخشب دو زلف بر رخ زن</p></div>
<div class="m2"><p>که تا دهد همه را عبده گل خود روی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بپیش باد نه آن نامه تا بمن برسد</p></div>
<div class="m2"><p>که هیچ خنک نیابی چو باد با تک و پوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بکوی و صافی آن نامه را بزن عنوان</p></div>
<div class="m2"><p>به پیش نامه تو با خواره بندم گوی</p></div></div>