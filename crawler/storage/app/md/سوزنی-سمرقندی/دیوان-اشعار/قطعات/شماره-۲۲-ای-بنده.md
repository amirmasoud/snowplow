---
title: >-
    شمارهٔ ۲۲ - ای بنده
---
# شمارهٔ ۲۲ - ای بنده

<div class="b" id="bn1"><div class="m1"><p>ای بنده طول عمر تو خواهنده از خدا</p></div>
<div class="m2"><p>از بنده یک حدیثک موجز توان شنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فصل زررز است، بدینگاه دست گیر</p></div>
<div class="m2"><p>چندانکه نیمدانک بزر، رز توان خرید</p></div></div>