---
title: >-
    شمارهٔ ۱۱ - قطعه
---
# شمارهٔ ۱۱ - قطعه

<div class="b" id="bn1"><div class="m1"><p>سالار بک که بخشش کف جواد تو</p></div>
<div class="m2"><p>کمتر فزونتر آمد از گنج شایگان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشریف یافتی و جمال از صفی دین</p></div>
<div class="m2"><p>و اشراف بر سر رمه های خدایگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بادت خجسته این عملی را که یافتی</p></div>
<div class="m2"><p>هرگز بود مبارکبادم به رایگان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانی که دیرگاه برآمد که داده ای</p></div>
<div class="m2"><p>در باب سیم مادر زر را به دایگان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو مقصری به حق سیم تار من</p></div>
<div class="m2"><p>یکباره شرمگینم زین . . . و خدایگان</p></div></div>