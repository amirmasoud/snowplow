---
title: >-
    شمارهٔ ۷ - سرای بهشت
---
# شمارهٔ ۷ - سرای بهشت

<div class="b" id="bn1"><div class="m1"><p>در خاک شایقی و نجیبی نگاه کرد</p></div>
<div class="m2"><p>شیخ اجل رشیدی و دید آن موافقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دنیا فرو گذاست برین ناموافقان</p></div>
<div class="m2"><p>بر موجب وفاق نجیبی و شایقی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جنت موافقان را آماده کرده اند</p></div>
<div class="m2"><p>هست از موافقان سخن بی منافقی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب جزای هر سه موافق بهشت کن</p></div>
<div class="m2"><p>کامرزگار جرم و گناه خلایقی</p></div></div>