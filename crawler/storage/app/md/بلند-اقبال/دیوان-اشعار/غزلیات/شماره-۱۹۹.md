---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>آن سرو قد به سیر گل و لاله میرود</p></div>
<div class="m2"><p>وز تاب می ز نسترنش ژاله می رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا گل ز نسبت رخ تورنگ و بو گرفت</p></div>
<div class="m2"><p>زاین رشک داغ ها به دل لاله می رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن رخت فزون شده ازخط اگر چه ماه</p></div>
<div class="m2"><p>ناقص شود زنور چو در هاله می رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس ترک می به پیروی زاهد ار کند</p></div>
<div class="m2"><p>چون سامری است کز پی گوساله می رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از باده دو ساله خوری گر سه چار جام</p></div>
<div class="m2"><p>یکباره از دلت غم صد ساله می رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل وصال اگر طلبی روز وشب بنال</p></div>
<div class="m2"><p>کاری به پیش اگر رود از ناله می رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر پیش شاهزاده رود این غزل ز من</p></div>
<div class="m2"><p>همچون شکر بود که به بنگاله می رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشتر بود ار عمر رود هر چه به سر زود</p></div>
<div class="m2"><p>عمری که به تلخی گذرد گوبگذر زود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر شب شب وصل است بگو روز نگردد</p></div>
<div class="m2"><p>دیر است شب هجر شود هر چه سحر زود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان کرده بتم قیمت بوسی ز لب ای دل</p></div>
<div class="m2"><p>جانی بده و بوسه ای از یار بخر زود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان کیست پیامی برد از من بر دلدار</p></div>
<div class="m2"><p>وآرد سوی من از بر دلدار خبر زود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتی به نگاهی برم ازدست تودل را</p></div>
<div class="m2"><p>تا خون نشده است از غم هجر تو ببر زود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نزدیک شد از هجر که جانم رود از تن</p></div>
<div class="m2"><p>جانی ز نو آید به تنم آئی اگر زود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای دل اگر اقبال بلند است تو را باز</p></div>
<div class="m2"><p>آن ترک سفر کرده بیاید ز سفر زود</p></div></div>