---
title: >-
    شمارهٔ ۳۷۳
---
# شمارهٔ ۳۷۳

<div class="b" id="bn1"><div class="m1"><p>ساقی امشب می دهی گر می به من</p></div>
<div class="m2"><p>ساغر از کف نه به من می ده به من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو به خادم تا رود در پیش یار</p></div>
<div class="m2"><p>گوید او راتا بیاید پیش من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمدی در پیش من خوش آمدی</p></div>
<div class="m2"><p>ای نگار گلرخ نسرین بدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه چورخسارت بود ماه فلک</p></div>
<div class="m2"><p>نه چو بالایت بود سروچمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماه راکی بوده زلف عنبرین</p></div>
<div class="m2"><p>سرو را کی بوده بر مشک ختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیز وجامی ده بلنداقبال را</p></div>
<div class="m2"><p>تا بگوید وصف میرمؤتمن</p></div></div>