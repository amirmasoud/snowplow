---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>چهر توچون آتش آمد زلف توچون دودشد</p></div>
<div class="m2"><p>چشم من از آتش ودود تواشک آلود شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیستی داوود وزلفت جوشن داوود گشت</p></div>
<div class="m2"><p>نیستی نمرود وچهرت آتش نمرود شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورده ای خون دل ما را وحاشا می کنی</p></div>
<div class="m2"><p>پس چرا لعل لبت اینگونه خون آلود شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاجت خود وزره نبود ترا درروز رزم</p></div>
<div class="m2"><p>بر سر دوش تو زلفت هم زره هم خود شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یافتم کز چیست نایددر کنارم آن نگار</p></div>
<div class="m2"><p>زانکه می بیند کنارم ز اشک چشمم رودشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک چشم ما چوسیم وچهر ما شد همچوزر</p></div>
<div class="m2"><p>هر چه درعشقش زیان کردیم آخر سودشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت در هجرم بلنداقبال آخر جان دهد</p></div>
<div class="m2"><p>جان به هجرش دادم آخر آنچه می فرمود شد</p></div></div>