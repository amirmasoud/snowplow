---
title: >-
    شمارهٔ ۳۳۳
---
# شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>ز آب چشم از بسکه روز و شب زمین را تر کنم</p></div>
<div class="m2"><p>مشت خاکی دست ندهد کز غمت بر سر کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاسه چشم است وافغان دل وخون جگر</p></div>
<div class="m2"><p>بی تو گر مطرب طلب یا باده در ساغر کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشته ام درانتظار از بس ز وصلت ناامید</p></div>
<div class="m2"><p>کافرم گر چون دل آئی در برم باور کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون شودبی شک چو اول روز کاندر نافه بود</p></div>
<div class="m2"><p>وصف زلفت را اگر در پیش مشک تر کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سیاهی چون محک زلفت چو آید در نظر</p></div>
<div class="m2"><p>تا بدوسایم رخی رخ را به زردی زرکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعر من آشفته دیوانم پریشان شد زبس</p></div>
<div class="m2"><p>شرح مشکین طره ات را ثبت دردفتر کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش سرو قامتت نه نامی از طوبی برم</p></div>
<div class="m2"><p>با می لعل لبت نه یادی از کوثر کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آسمان را رشکها از دامنم باشد به دل</p></div>
<div class="m2"><p>دامنم را شب ز اشک از بسکه پراختر کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهره اندر چرخ با یاران خود می گفت دوش</p></div>
<div class="m2"><p>از بلنداقبال کوشعری که تا از برکنم</p></div></div>