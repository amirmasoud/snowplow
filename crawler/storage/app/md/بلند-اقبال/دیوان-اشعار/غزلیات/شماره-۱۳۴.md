---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>درخواب بسی آن پری آزارمرا کرد</p></div>
<div class="m2"><p>بخت بد من آه که بیدار مرا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که دلم هست بسی طالب دیدار</p></div>
<div class="m2"><p>بنمود رخ وصورت دیوار مرا کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسیمه سر ازچهره چون نار مرا ساخت</p></div>
<div class="m2"><p>آشفته دل از طره طرار مرا کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دین و دل وهوش وخرد از دست مرا برد</p></div>
<div class="m2"><p>از یک نگه آسوده زهر چار مرا کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلقند از آن لعل شکر بار مرا داد</p></div>
<div class="m2"><p>ز آن نرگس بیمار چو بیمار مرا کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این می ز کجا بودکه ساقی به یکی جام</p></div>
<div class="m2"><p>برد از سر من مستی و هشیار مرا کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرموده مگر خواجه قبولم به غلامی</p></div>
<div class="m2"><p>کامروز همی جانب بازار مرا کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آزار مکن بر دلم ای ترک دل آزار</p></div>
<div class="m2"><p>کز جان وجهان جور تو بیزار مرا کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الحمد چه اقبال بلندی است که دارم</p></div>
<div class="m2"><p>منصور صفت عشق تو بردار مرا کرد</p></div></div>