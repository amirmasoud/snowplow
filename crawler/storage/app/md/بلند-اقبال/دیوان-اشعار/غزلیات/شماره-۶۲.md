---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>شمع این نوری که می بینی به سر بگرفته است</p></div>
<div class="m2"><p>پرتوی از روی یار ما به بر بگرفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآن به گرداو پرد پروانه تا سوزد همی</p></div>
<div class="m2"><p>او ندانم از که این سر راخبر بگرفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش دیدم در برش معشوق ما بنشسته بود</p></div>
<div class="m2"><p>وز رخ اودر بر این نور اثر بگرفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که را برند سر از تن دهدجان در زمان</p></div>
<div class="m2"><p>شمع را نازم ز سر رسم دیگر بگرفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تنش هر گه بری سر فورا آرد جان پدید</p></div>
<div class="m2"><p>در تن خون جان عالم رامگر بگرفته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می توان او را به بزم قرب جانان جای داد</p></div>
<div class="m2"><p>هر که همچون شمع نوری درجگر بگرفته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهره خواند در فلک شعر بلنداقبال را</p></div>
<div class="m2"><p>نسخه گویا از عطارد وز قمر بگرفته است</p></div></div>