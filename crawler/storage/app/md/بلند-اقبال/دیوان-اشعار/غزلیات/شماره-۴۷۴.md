---
title: >-
    شمارهٔ ۴۷۴
---
# شمارهٔ ۴۷۴

<div class="b" id="bn1"><div class="m1"><p>مگرنه جان منی پس چرا ز من دوری</p></div>
<div class="m2"><p>بیا که راحت روحی ومایه سوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آدمی چو توحاشا که دروجود آید</p></div>
<div class="m2"><p>فرشته ای وز نوری پری ویا حوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنانکه جان به تن و دل به بر بود مستور</p></div>
<div class="m2"><p>میان جان ودل من مدام مستوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تورا چه حاجت کافور و مشک ازعطار</p></div>
<div class="m2"><p>که خودبهزلف چومشک وبه رخ چو کافوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسد اگر به لبت شهد شورخواهد شد</p></div>
<div class="m2"><p>به شهد آن نمکین لب ز بسکه پرشوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب نه کآب دهان توانگبین باشد</p></div>
<div class="m2"><p>چرا که ازمژه مانند نیش زنبوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محال عقل بودجمع نور با ظلمت</p></div>
<div class="m2"><p>ز زلف وچهره توچون جمع ظلمت ونوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنم ز مرمر وبلور بستر وبالین</p></div>
<div class="m2"><p>به سینه ز آنکه چومرمر به ساق بلوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل ار بگفت تورا تا مرا کشی از هجر</p></div>
<div class="m2"><p>بکن فدای توگردم بدآنچه مأموری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا هلاک کن از درد هجر وباک مدار</p></div>
<div class="m2"><p>به حکمآنکه چومأمور بوده معذوری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شدم به عشق تومشهور چون بلنداقبال</p></div>
<div class="m2"><p>بدان صفت که تودر حسن روی مشهوری</p></div></div>