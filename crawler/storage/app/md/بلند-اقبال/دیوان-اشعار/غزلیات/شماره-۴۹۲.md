---
title: >-
    شمارهٔ ۴۹۲
---
# شمارهٔ ۴۹۲

<div class="b" id="bn1"><div class="m1"><p>بوی یار مهربان آیدهمی</p></div>
<div class="m2"><p>بر مشامم بوی جان آید همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانه را ای دل ز آلایش بروب</p></div>
<div class="m2"><p>زآنکه سویت میهمان آید همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نمی خواهم بگویم راز دل</p></div>
<div class="m2"><p>بیخود از دل بر زبان آیدهمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن پری کز چشم مردم شد نهان</p></div>
<div class="m2"><p>بینمش هر سوعیان آید همی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش گفتم شرحی از گیسوی او</p></div>
<div class="m2"><p>بوی مشکم از دهان آید همی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون سخن گویم ز اوصافش ملک</p></div>
<div class="m2"><p>بر زمین از آسمان آید همی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ثابت ای دل در محبت شو که دوست</p></div>
<div class="m2"><p>درمقام امتحان آید همی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خار وخارا در برم از عشق تو</p></div>
<div class="m2"><p>چون پرند وپرنیان آید همی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با تو درگلخن اگر منزل کنم</p></div>
<div class="m2"><p>پیش چشمم گلستان آید همی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناید ار یک ذره لطفت در میان</p></div>
<div class="m2"><p>سود دو عالم زیان آید همی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای بلنداقبال از این گفتارها</p></div>
<div class="m2"><p>از تو هر کس بدگمان آید همی</p></div></div>