---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>ای بلبل بی دل منال آخر گلت پیدا شود</p></div>
<div class="m2"><p>اردیبهشت آید ز نو وز سر جهان برنا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من هم دلی دارم غمین از هجر یار نازنین</p></div>
<div class="m2"><p>امید دارم کز کمین چون فروردین پیدا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من هرچه می گویم به من بوسی دهی گوئی که لا</p></div>
<div class="m2"><p>زآن لاله رو خواهددلم آسوده از الا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنی که من دارم به تن گورو بر جانان من</p></div>
<div class="m2"><p>کآب روان از هر کجا لابد سوی دریا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذار جان و دل به کف شو پیش تیر اوهدف</p></div>
<div class="m2"><p>باران رود چون در صدف ز آن لوء لوء لا لا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی زدنیا غم خوری به باشد ار غم کم خوری</p></div>
<div class="m2"><p>زیرا که دنیا را غنی ناید اگر بی ما شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم بلند اقبال را آشفته گوئی تا به کی</p></div>
<div class="m2"><p>گفتاکه در زنجیر آن گیسو مرا تا جا شود</p></div></div>