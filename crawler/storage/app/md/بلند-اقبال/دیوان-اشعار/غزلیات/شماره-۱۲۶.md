---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>نه ماهی چورویت زخط هاله دارد</p></div>
<div class="m2"><p>نه قندی چولعل تو بنگاله دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که از رنگ ورویت خبر داده اورا</p></div>
<div class="m2"><p>که داغی ز عشقت به دل لاله دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه همسایگان خواب دارند ونه من</p></div>
<div class="m2"><p>دلم هر شب از غم ز بس ناله دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ساقی بگو درددل را که درکف</p></div>
<div class="m2"><p>دوای غم و درد صد ساله دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلند است اقبال آن کس که دلبر</p></div>
<div class="m2"><p>ز عشق خود اوچو من واله دارد</p></div></div>