---
title: >-
    شمارهٔ ۲۴۱
---
# شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>ساقی دگر شراب به جام از سبو مریز</p></div>
<div class="m2"><p>مستم ز چشم تو میم اندر گلومریز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناسورکرد زخم دل من ز بوی تو</p></div>
<div class="m2"><p>با شانه مشک این همه از چین مومریز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رحمی به بلبل آور ودرگلستان مرو</p></div>
<div class="m2"><p>از رنگ وبوی خویش ز گل رنگ وبو مریز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از زلف دل ز پیر وجوان بیش از این مبر</p></div>
<div class="m2"><p>چوگان خویش را ببر این قدر گو مریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهم که جان به پای تو ریزم به أذن تو</p></div>
<div class="m2"><p>قابل نی وقبول کن از من مگو مریز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچون مگس به سر زند از غم شکرفروش</p></div>
<div class="m2"><p>شیرین لبا دگر شکر ازگفتگو مریز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست ار رقیب با تو مشوتندوترش رو</p></div>
<div class="m2"><p>ای دوست پیش خصم مرا آبرو مریز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیف است کز نظر برود رنگ لعل دوست</p></div>
<div class="m2"><p>ای دیده خون دل دگر از هجر او مریز</p></div></div>