---
title: >-
    شمارهٔ  ۲۳
---
# شمارهٔ  ۲۳

<div class="b" id="bn1"><div class="m1"><p>نه چوبالای توسروی بود اندر چمنا</p></div>
<div class="m2"><p>نه کشدفاخته اندر چمن افغان چومنا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سراغ قدچون سرو توگر نیست زچیست</p></div>
<div class="m2"><p>فاخته اینهمه کوکو زند اندر چمنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنچه لعل تو را دیده همانا که زند</p></div>
<div class="m2"><p>چاک هر صبحدم از عشق تو گل پیرهنا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منکر نقطه موهوم نمی گشت حکیم</p></div>
<div class="m2"><p>گر که می آمد ومی دید که داری دهنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه خوانند تو را ماه ونیابند که ماه</p></div>
<div class="m2"><p>نه سخن گوست نه زلفینش بود پرشکنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه دانند تو را سرو و ندانند که سرو</p></div>
<div class="m2"><p>نه چمیدن چو تو دارد نه بودسیمتنا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواستم عشق تو را فاش نسازم دیدم</p></div>
<div class="m2"><p>داستانی است که گویند به هر انجمنا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلف طرار تو دزد دلم ار نیست ز چیست</p></div>
<div class="m2"><p>شده لرزان و پریشان و اسیر رسنا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من اگر دل به سر زلف تو دادم چه عجب</p></div>
<div class="m2"><p>تو بری دین و دل از دست اویس قرنا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جامه مهر تو حاشا که زتن دور کنم</p></div>
<div class="m2"><p>مگر آن روز که پوشند به جسمم کفنا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون میانت ز میان رفت بلند اقبالت</p></div>
<div class="m2"><p>از میان تو بیامد به میان چون سخنا</p></div></div>