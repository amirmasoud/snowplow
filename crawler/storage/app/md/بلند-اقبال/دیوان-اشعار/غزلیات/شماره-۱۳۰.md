---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>آنکه شد عاشق ومردازغم ودرد</p></div>
<div class="m2"><p>با خبر شد که به من عشق چه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل انداخت سپر دربر عشق</p></div>
<div class="m2"><p>گفت من با تونیم مرد نبرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که گفتی چه هنر داردعشق</p></div>
<div class="m2"><p>اشک را سرخ کندرخ را زرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم را تر کند ولب را خشک</p></div>
<div class="m2"><p>جسم را گرم کنددم را سرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کندبا دل و با جان کاری</p></div>
<div class="m2"><p>که مثالش نتوانم آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه رامات کند در شطرنج</p></div>
<div class="m2"><p>مهر ومه را بکشد مهره به نرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برباید ز سر چرخ کلاه</p></div>
<div class="m2"><p>بر فشاند ز کف دریا گرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه درخدمت اویکسانند</p></div>
<div class="m2"><p>پیر وبرنا شه ومسکین زن ومرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچومن هر که بلنداقبال است</p></div>
<div class="m2"><p>عشق ورزید ونترسید ز درد</p></div></div>