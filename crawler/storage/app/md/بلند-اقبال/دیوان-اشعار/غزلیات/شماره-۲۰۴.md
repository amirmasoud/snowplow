---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>گلگون چو روی یار من از غازه می شود</p></div>
<div class="m2"><p>داغ دل من از غم اوتازه می شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یادچشم وچهره او اشک وبخت من</p></div>
<div class="m2"><p>سرخ وسیه چو سرمه وچون غازه می شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اوجا به شهر دارد ودارم عجب از آنک</p></div>
<div class="m2"><p>بیرون به سیر باغ ز دروازه می شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر گه به یاد آیدم آن چشم نیمخواب</p></div>
<div class="m2"><p>اعضای من زشوق به خمیازه می شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی پرده دید هر که رخ دوست را چومن</p></div>
<div class="m2"><p>داندکه حسن تا به چه اندازه می شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل نگفتمت که مگو وصف روی دوست</p></div>
<div class="m2"><p>دیدی نگفته شهر پر آوازه می شود</p></div></div>