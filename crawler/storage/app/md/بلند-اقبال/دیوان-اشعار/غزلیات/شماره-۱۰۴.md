---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>داده شه فرمان که عطاران معافند از خراج</p></div>
<div class="m2"><p>برده است از مشک وعنبر بسکه گیسویت رواج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راستی دانی که زلفت راستگردن از چه کج</p></div>
<div class="m2"><p>از پریشانی به سیمت کرده پیدا احتیاج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواب می دیدم شبی بازی به زلفت می کنم</p></div>
<div class="m2"><p>روزها رفته است وآید بوی مشکم از دواج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بازی چوگان وگوخواهم که از پستان وزلف</p></div>
<div class="m2"><p>ز آبنوس آورده ای چوگان و داری گوی عاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی ار دانی که از دست دلت چون شددلم</p></div>
<div class="m2"><p>سنگی اند رچنگ آور بر زن او را بر زجاج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تودارم زخم از آن مرهم نمی خواهم ز کس</p></div>
<div class="m2"><p>از تو دارم درد از آن هرگز نمی جویم علاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بارک الله بس که شیرین است شهد لعل دوست</p></div>
<div class="m2"><p>شد بلند اقبال از یک بوسه محروری مزاج</p></div></div>