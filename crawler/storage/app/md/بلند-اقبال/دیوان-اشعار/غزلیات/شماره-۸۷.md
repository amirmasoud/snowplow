---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>بی وفایی چو تو درعالم نیست</p></div>
<div class="m2"><p>حاصل از عشق توام جز غم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمی از مردن ما نیست تو را</p></div>
<div class="m2"><p>زآنکه در خیل توچون ما کم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ز درد تو مرا درمان نی</p></div>
<div class="m2"><p>به ز زخم تو مرا مرهم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم دل با تو بگویم نه به غیر</p></div>
<div class="m2"><p>که به غیر از تو مرا محرم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست چون عارض توماه که ماه</p></div>
<div class="m2"><p>به رخش زلف خم اندر خم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست چشم تواگر پور پشنگ</p></div>
<div class="m2"><p>دل من نیز کم از رستم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که را عشق تو بر سر نبود</p></div>
<div class="m2"><p>هست نسناس بنی آدم نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه شب تا به سحر بی تو مرا</p></div>
<div class="m2"><p>به جز از ناله کسی همدم نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلم ازعشق بلنداقبال است</p></div>
<div class="m2"><p>لیک یکدم به جهان خرم نیست</p></div></div>