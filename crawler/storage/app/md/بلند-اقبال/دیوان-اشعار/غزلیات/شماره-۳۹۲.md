---
title: >-
    شمارهٔ ۳۹۲
---
# شمارهٔ ۳۹۲

<div class="b" id="bn1"><div class="m1"><p>ای دل چنین بازی مکن با طره طرار او</p></div>
<div class="m2"><p>ماری است پیچان طره اش اندیشه کن از مار او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنبر فراوان گشته است از زلف عنبر ریز وی</p></div>
<div class="m2"><p>شکر چه ارزان گشته است از لعل شکر بار او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم دهی بوسی به من گفتا دهم ندهم به مفت</p></div>
<div class="m2"><p>آسوده دل گردیده ام ز اقرار واز انکار او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از راست و ز چپ زلف او بردوش زناری کند</p></div>
<div class="m2"><p>ترسم مرا ترسا کند آن زلف چون زنار او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نرگس بیمار او بیماری از صحت به است</p></div>
<div class="m2"><p>ای دل توهم بیمار شو چون نرگس بیمار او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کوچه دلبر دلا هر گه گذارت اوفتد</p></div>
<div class="m2"><p>آهسته روکز هر طرف سر بشکند دیوار او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمد بلنداقبال من چون گل شکفت احوال من</p></div>
<div class="m2"><p>آورد باد صبحدم چون بوئی از گلزار او</p></div></div>