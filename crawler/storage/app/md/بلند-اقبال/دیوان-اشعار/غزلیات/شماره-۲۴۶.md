---
title: >-
    شمارهٔ ۲۴۶
---
# شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>خواه اندر کعبه باش وخواه در بتخانه باش</p></div>
<div class="m2"><p>هرکجا باشی به یاد آن بت جانانه باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش شمع رخت چون برفروزد چون کنم</p></div>
<div class="m2"><p>گفت بهر سوختن آماده چون پروانه باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش رخ چون پری داری وچون زنجیر زلف</p></div>
<div class="m2"><p>گفت اگر دانسته ای هست این چنین دیوانه باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش خواهم که در زلفت رهی پیدا کنم</p></div>
<div class="m2"><p>گفت دردست غمم دل ریش تر از شانه باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش جا در کجا داری که آیم پیش تو</p></div>
<div class="m2"><p>گفت گاهی درحرم روگاه درمیخانه باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش نام تو را خواهم کنم ورد زبان</p></div>
<div class="m2"><p>گفت یادم کن به دل بی منت از صد دانه باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بلند اقبال گفتم بلبل باغ توام</p></div>
<div class="m2"><p>گفتم نه دربندآب و نه به فکر دانه باش</p></div></div>