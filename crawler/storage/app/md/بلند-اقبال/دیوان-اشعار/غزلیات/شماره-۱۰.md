---
title: >-
    شمارهٔ  ۱۰
---
# شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>از پریشانی من گر خبری بود تو را</p></div>
<div class="m2"><p>به من وحال دل من نظری بود تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنده گردم ز لحد رقص کنان برخیزم</p></div>
<div class="m2"><p>بعد مرگ ار به مزارم گذری بود تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شبیه قد ورخسار بت من بودی</p></div>
<div class="m2"><p>به سرای سروچمن گر قمری بود تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا که چون طره او مشک ترت بود ثمر</p></div>
<div class="m2"><p>یاخرامیدن و پای دگری بود تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل چون آهن آن سیم بدن نرم شدی</p></div>
<div class="m2"><p>اگر ای آه دل من اثری بودتو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخت خوابیده من چشم گشودی از خواب</p></div>
<div class="m2"><p>ای شب هجر گر از پی سحری بود تورا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون من خون شده دل بودبلند اقبالت</p></div>
<div class="m2"><p>گر به دل عشق بت سیم بری بود تو را</p></div></div>