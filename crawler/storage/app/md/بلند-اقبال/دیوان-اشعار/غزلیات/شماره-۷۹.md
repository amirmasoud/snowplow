---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>باشد دلم ز روی روی جناب دوست</p></div>
<div class="m2"><p>آشفته تر ز طره پرپیچ وتاب دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادیم نقد هستی خود را که داشتیم</p></div>
<div class="m2"><p>اما نرفته هیچ به خرج حساب دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر از طریق جور و رسوم ستمگری</p></div>
<div class="m2"><p>کاتب نکرده ثبت مگر درکتاب دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدیم دوست را دل مشکل پسند داشت</p></div>
<div class="m2"><p>ای دل چه کرده ای که شدی انتخاب دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با دوست عشق هیچ ندارد تفاوتی</p></div>
<div class="m2"><p>با عاشقان پس از چه بوداجتناب دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از قحط آب مزرع امید ما بسوخت</p></div>
<div class="m2"><p>تا زاین سپس چه فیض رسد از سحاب دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او آفتاب از رخ وما مشتری به دل</p></div>
<div class="m2"><p>نی نی چوذره ایم بر آفتاب دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویند از ملازمت اوملامتم</p></div>
<div class="m2"><p>من درخیال دادن جان در رکاب دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دیده غائب ار شده حاضر بود به دل</p></div>
<div class="m2"><p>عین حضور در نظرم شد غیاب دوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حکم کسوف کرد منجم چوشدخبر</p></div>
<div class="m2"><p>کامروز دور می شود از رخ نقاب دوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن کس که گفت سروندیدم چمان شود</p></div>
<div class="m2"><p>غافل بود مگر ز ذهاب و ایاب دوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اقبال من بلندتر از این شوداگر</p></div>
<div class="m2"><p>آید به دست طره چون مشک ناب دوست</p></div></div>