---
title: >-
    شمارهٔ ۱۹۴
---
# شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>دوش از برما یار نهان گشت وبری بود</p></div>
<div class="m2"><p>دل بردن و پنهان شدن آئین پری بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدیم به صید دل ما هست چو شهباز</p></div>
<div class="m2"><p>شوخی که به هنگام روش کبک دری بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شدکه شبیهش به رخ یار نمائیم</p></div>
<div class="m2"><p>بر روی مه ار زلف چومشک تتری بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز اعجاز زد ار پیش لب دلبر ما دم</p></div>
<div class="m2"><p>عیسی مکنش عیب که از بی پدری بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار است برما وهمی ما به سراغش</p></div>
<div class="m2"><p>چون فاخته کوکو گو از بی بصری بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر بی سروپائی چومن از عشق زنددم</p></div>
<div class="m2"><p>کی عشق رخ یار بدین مختصری بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آه دل ما نرم نگردید دل دوست</p></div>
<div class="m2"><p>کی آتش سوزنده بدین بی اثری بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس که بلند اقبال او را شده چون من</p></div>
<div class="m2"><p>از وصل رخ دوست ز آه سحری بود</p></div></div>