---
title: >-
    شمارهٔ ۳۱۶
---
# شمارهٔ ۳۱۶

<div class="b" id="bn1"><div class="m1"><p>تعالی الله زجانانی که دارم</p></div>
<div class="m2"><p>ز جانان است این جانی که دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سیر سرو بستانم چه حاجت</p></div>
<div class="m2"><p>از این سرو خرامانی که دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نور ماه وخورشیدم چه خواهش</p></div>
<div class="m2"><p>از این شمع شبستانی که دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن ترسم که شهری را برد سیل</p></div>
<div class="m2"><p>از این چشمان گریانی که دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب نی گر بسوزد خشک وتر را</p></div>
<div class="m2"><p>زهجرت آه سوزانی که دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به من گفتی که چون بخت تو‌آمد</p></div>
<div class="m2"><p>من این برگشته مژگانی که دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلند اقبال هم گوید ز زلفت</p></div>
<div class="m2"><p>بود حال پریشانی که دارم</p></div></div>