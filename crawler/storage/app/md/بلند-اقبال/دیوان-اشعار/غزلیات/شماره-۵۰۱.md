---
title: >-
    شمارهٔ ۵۰۱
---
# شمارهٔ ۵۰۱

<div class="b" id="bn1"><div class="m1"><p>چنگی به زلف اوزدم گفتا جسارت می کنی</p></div>
<div class="m2"><p>گفتم بده بوسی ز لب گفتا حرارت می کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که جان ودل زمن بستان بهای بوسه ات</p></div>
<div class="m2"><p>گفت از که است این مایه ات کاکنون تجارت می کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برکف گرفتم غبغبش هی بوسه کردم بر لبش</p></div>
<div class="m2"><p>گفتا که چون غارتگران تا چند غارت می کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من هم زدست جور تو دارم دل ویرانه ای</p></div>
<div class="m2"><p>ویرانه دل ها را اگر میل عمارت می کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خنجر ز مژگان می کشد هر کس که بیندمی کشد</p></div>
<div class="m2"><p>با چشم مست خود بگو تا کی شرارت می کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من خود به قتل خویشتن خود را بشارت می دهم</p></div>
<div class="m2"><p>وقتی به قتل من اگر بینم اشارت می کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی که ترک عاشقی تا جان به تن داری مکن</p></div>
<div class="m2"><p>ای دل بلنداقبال را نیکو وزارت می کنی</p></div></div>