---
title: >-
    شمارهٔ ۲۰۱
---
# شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>سر بزن از زلف اگر خواهی که دلبرتر شود</p></div>
<div class="m2"><p>شمع هم روشن تر آید هر زمان بی سر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبری چیز دیگر داردنه هر خوش منظری</p></div>
<div class="m2"><p>چشم وابروئی نکو دارد توان دلبر شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بت عابد فریب ای آفت صبر وشکیب</p></div>
<div class="m2"><p>چهره بنما تا هر آنکس بیندت کافر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشته ام از همت عشق رخت رشک فلک</p></div>
<div class="m2"><p>بسکه هر شب دامنم ازاشک پراختر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر شبی درخواب بینم زلف مشکین تو را</p></div>
<div class="m2"><p>بسترم خوشبوی تر از طبله عنبر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا بده صبری که دیگر طاقت دوری نماند</p></div>
<div class="m2"><p>یا بفرما تا که جان زارم از تن در شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیش ازین مپسند درهجرت بلند اقبال را</p></div>
<div class="m2"><p>در قفس افتاده همچون طایر بی پر شود</p></div></div>