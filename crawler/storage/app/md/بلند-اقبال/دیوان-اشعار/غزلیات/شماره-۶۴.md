---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>زلف بر رخسار آن والاگهر افتاده است</p></div>
<div class="m2"><p>یا که هندوئی است در آتش به سر افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خلاف اینکه می گویند در چین است مشک</p></div>
<div class="m2"><p>زلف او را بین که چین در مشک ترافتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا منجم دیده افتاده است در عقرب قمر</p></div>
<div class="m2"><p>من کنون بینم که عقرب در قمر افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در میان مژه هر کس چشم اورا دید گفت</p></div>
<div class="m2"><p>آهویی باشد بهچنگ شیر نر افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست بر شکر مگش آن خال بر شیرین لبش</p></div>
<div class="m2"><p>خسرو پرویز بر روی شکر افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سرین ودرمیانش لغزدم پای نظر</p></div>
<div class="m2"><p>زآنکه او را راه درکوه وکمر افتاده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش یاقوت لبش کوقوت مرجان شدمرا</p></div>
<div class="m2"><p>لعل ومرجان وعقیقم از نظر افتاده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آتشین رخسار وآب لعل آتش رنگ او</p></div>
<div class="m2"><p>آتشی سوزنده ما را بر جگر افتاده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دل زار بلنداقبال دارد آگهی</p></div>
<div class="m2"><p>طایری کاندر قفس بشکسته پر افتاده است</p></div></div>