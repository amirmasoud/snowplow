---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>التفاتی به ما ندارد یار</p></div>
<div class="m2"><p>که زما یاد می نیارد یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار ما ابرر حمت است ودریغ</p></div>
<div class="m2"><p>هیچ برکشت ما نبارد یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینکه دایم ز دیده خونبارم</p></div>
<div class="m2"><p>دل ما را همی فشارد یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر از بندگان درگه خویش</p></div>
<div class="m2"><p>بنده اش را نمی شمارد یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه چه گفتم ز بیخودی که به خود</p></div>
<div class="m2"><p>مر مرا وانمی گذارد یار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دمبدم فیض بخش روح من است</p></div>
<div class="m2"><p>نکند از عطا مرا رد یار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبود کس چومن بلنداقبال</p></div>
<div class="m2"><p>کام من را اگر برآرد یار</p></div></div>