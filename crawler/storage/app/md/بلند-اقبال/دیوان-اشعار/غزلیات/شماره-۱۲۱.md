---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>خون به دل از بوی مویت نافه تاتار دارد</p></div>
<div class="m2"><p>نافه تاتار کی مشکی چومویت بار دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خال رخسار تورا خوانم خلیل الله زیرا</p></div>
<div class="m2"><p>کاندر آتش رفته وآتش را به خودگلزار دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترک چشم مستت از مژگان به کف بگرفته خنجر</p></div>
<div class="m2"><p>نه همی با ما که جنگ او با در و دیوار دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسبتی نبودبه سرو وماهت از رخسار وقامت</p></div>
<div class="m2"><p>سروکی چالش نماید مه کجا گفتار دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرق ننهددوست را چشم تو از مستی ز دشمن</p></div>
<div class="m2"><p>لیکن اندر دلبری هوش دو صد هشیار دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم یار منبود بیمار ای دل ناله کم کن</p></div>
<div class="m2"><p>کس ننالد اینچنین در بر اگر بیمار دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست از جور و جفا گر ترک من ضحاک دوران</p></div>
<div class="m2"><p>از دوگیسو بر دو دوش خودچرا دومار دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یار ما هست ار چه هر جائی ولیکن لن ترانی</p></div>
<div class="m2"><p>می دهد پاسخ به هر کس خواهش دیدار دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن قیامت قامت ازقامت قیامت کرده برپا</p></div>
<div class="m2"><p>با بلنداقبال گوئی تا قیامت کار دارد</p></div></div>