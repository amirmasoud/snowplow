---
title: >-
    شمارهٔ ۳۱۳
---
# شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>ز بسکه روز وشب اندر خیال دلدارم</p></div>
<div class="m2"><p>به هر کجا که نشینم چو نقش دیوارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برو طبیب مکن در علاج من کوشش</p></div>
<div class="m2"><p>که من ز نرگس بیمار دوست بیمارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عشق روی تومنعم کندهمی زاهد</p></div>
<div class="m2"><p>نه آگه است همانا که من گرفتارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرابی دل خود را طلب کنم ز خدا</p></div>
<div class="m2"><p>شنیده ام چوتورا کرده اند معمارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مپرس حال دل ازمن که گفتنی نبود</p></div>
<div class="m2"><p>ببین درآینه آگه شو از دل زارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کشتنم همه عالم شونداگر همدست</p></div>
<div class="m2"><p>گمان مکن که ز عشق تو دست بردارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای بندگیت گو چو یوسف اندازند</p></div>
<div class="m2"><p>گهی به چاه و فروشند گه به بازارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر مرا چو خلیل افکنند در آتش</p></div>
<div class="m2"><p>به یاد روی تو آن آتش است گلزارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز آنکه از مدد عشق شد بلند اقبال</p></div>
<div class="m2"><p>کسی نگشت و نگردد خبر ز اسرارم</p></div></div>