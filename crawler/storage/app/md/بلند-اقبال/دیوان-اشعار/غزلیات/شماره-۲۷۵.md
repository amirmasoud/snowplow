---
title: >-
    شمارهٔ ۲۷۵
---
# شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>گفتم ازهجر توخونین جگرم گفت چه باک</p></div>
<div class="m2"><p>گفتم از زلف تو آشفته ترم گفت چه باک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم آن دل که ز دستم به اسیری بردی</p></div>
<div class="m2"><p>روزگاری است کز او بی خبرم گفت چه باک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش دل زغمت خون شدو پیوسته همی</p></div>
<div class="m2"><p>ریخت بر چهره ام از چشم ترم گفت چه باک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم ای مه تو ز بس جابری ومن صابر</p></div>
<div class="m2"><p>در برتیر ملامت سپرم گفت چه باک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم از شوخ شکر لب ز فراقت به مذاق</p></div>
<div class="m2"><p>تلخ تر گشته ز حنظل شکرم گفت چه باک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم افتاده ز بیدادتو درکنج قفس</p></div>
<div class="m2"><p>همچو آن طایر بشکسته پرم گفت چه باک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش در شب هجران تو از آتش غم</p></div>
<div class="m2"><p>سوخت چون شمع ز پا تا به سرم گفت چه باک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم از عشق تو هر چند بلند اقبالم</p></div>
<div class="m2"><p>بی دل وخون جگر و در به درم گفت چه باک</p></div></div>