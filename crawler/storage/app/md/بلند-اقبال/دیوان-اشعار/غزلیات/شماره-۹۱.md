---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>مشکی عجب ز گیسو آن ترک ماه رو داشت</p></div>
<div class="m2"><p>من درختن ندیدم مشکی که چین اوداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه پا شناسم از سر نه خیر دانم از شر</p></div>
<div class="m2"><p>درحیرتم که ساقی امشب چه درسبوداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیوانه کرد وبنمود دل را به زلف زنجیر</p></div>
<div class="m2"><p>دل هم به دل همین را پیوسته آرزو داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیشب ز عشق آن ماه خوابم نبرد تا روز</p></div>
<div class="m2"><p>دل با من از فراقش از بس که گفتگو داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتا شدم اسیرش گفتم چگونه گفتا</p></div>
<div class="m2"><p>از خال دانه واز زلف دامی زچار سوداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که زخمت ای دل کرده است از چه ناسور</p></div>
<div class="m2"><p>گفتا ز بوی مشکی کان زلف مشکبوداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که زلف جانان بود از چه رو پریشان</p></div>
<div class="m2"><p>گفتا خبر ز حالت از بسکه موبه مو داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم کهطره یار مانند صولجان بود</p></div>
<div class="m2"><p>گفتا بلی مرا هم درپیش اوچوگوداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اقبال هر که درعشق چون من بلندگردید</p></div>
<div class="m2"><p>در پیش زلف دلبر او نیز آبرو داشت</p></div></div>