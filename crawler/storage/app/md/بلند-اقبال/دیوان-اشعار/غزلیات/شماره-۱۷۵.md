---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>هر ناله ای که بربط وطنبور می کند</p></div>
<div class="m2"><p>پیغام دلبری است که مذکور می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوید که گر شوی توچوبهرام گورگیر</p></div>
<div class="m2"><p>ناگه شکار عاقبت گور میکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستان بر اینکه باده دهد نشئه هوشیار</p></div>
<div class="m2"><p>داندکه هر چه می کندانگور می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق مگر چه دیده که اسرار عشق را</p></div>
<div class="m2"><p>چون اسم اعظم از همه مستور می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی که صد هزار سلیمان گدای اوست</p></div>
<div class="m2"><p>بنگر چه لطف ها که به هر مورمی کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نزدیکتر هر آنچه شود یار ما به ما</p></div>
<div class="m2"><p>ما را هوی پرستی از اودور میکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلبر چوآفتاب عیان ونهان ز چشم</p></div>
<div class="m2"><p>کی ماه جلوه در نظر کور میکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای نور پاک باک نه گر ما چوظلمتیم</p></div>
<div class="m2"><p>ظلمت همه معرفی از نور می کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای ترک مشک طره کافور روی من</p></div>
<div class="m2"><p>موی مرا غم تو چوکافور می کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل زخم دار وقصه زلف تو بر زبان</p></div>
<div class="m2"><p>آخر ز بوی مشک تو ناسور می کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پنهان به زیر طشت نخواهد شد آفتاب</p></div>
<div class="m2"><p>عشقت مرا چوحسن تومشهور می کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اقبال هر که را که بلنداست روزگار</p></div>
<div class="m2"><p>او رازجام عشق تو مخمور می کند</p></div></div>