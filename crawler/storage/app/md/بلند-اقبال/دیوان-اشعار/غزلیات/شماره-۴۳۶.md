---
title: >-
    شمارهٔ ۴۳۶
---
# شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>دلبر ما را به حال ما اگر بود التفاتی</p></div>
<div class="m2"><p>می شدی حاصل ز بندغم دل ما را نجاتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معنی این را بگویم تا بدانی هجر و وصل است</p></div>
<div class="m2"><p>اینکه می گویند می باشد مماتی وحیاتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه شطرنج ار بشد پیش رخت مات این عجب تر</p></div>
<div class="m2"><p>بی رخش من در شط رنج و غمم چون شاه ماتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در برم خون گشته دل زآنرو عزیز آمد که دارد</p></div>
<div class="m2"><p>از دهان و لعل یار از رنگ و از تنگی صفاتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه کسی درچین چوزلفت دیده مشکی عنبرین بو</p></div>
<div class="m2"><p>نه کسی درمصر چون لعل توروح افزا نباتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آینه قدرت نمای کیست این جسم چو روحت</p></div>
<div class="m2"><p>چون تودر عالم ندیدم خوش سرشت وپاک ذاتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم مستت درخمار افکنده ما را جامی از می</p></div>
<div class="m2"><p>بر سرلعلت بده از عنبرین خطت براتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون توئی دارد بلنداقبال تا حالش چه گردد</p></div>
<div class="m2"><p>داشت حافظ هم به عهد خویشتن شاخ نباتی</p></div></div>