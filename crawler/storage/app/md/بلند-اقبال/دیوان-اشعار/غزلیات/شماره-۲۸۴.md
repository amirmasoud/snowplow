---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>مرا به جز غم وحسرت ز عشق یار چه حاصل</p></div>
<div class="m2"><p>به غیر حیرت و غفلت به روزگار چه حاصل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو رابه نشئه هستی به غیر مرگ چه صرفه</p></div>
<div class="m2"><p>بود ز باده و مستی به جز خمار چه حاصل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفتم این که سپردم به دست لاله رخان دل</p></div>
<div class="m2"><p>سوای خون دل و چشم اشکبار چه حاصل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در این دیار ندیدیم دلبری که برد دل</p></div>
<div class="m2"><p>دگر توقف ما اندر این دیار چه حاصل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که سال و مه عمر اوست بهمن ودی مه</p></div>
<div class="m2"><p>چو فرودین رسد وفصل نوبهار چه حاصل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خزان هوا وتماشای باغ داده چه لذت</p></div>
<div class="m2"><p>چو رفت گل ز گلستان ز سیر خار چه حاصل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آنکه آمده اقبال او بلندبه عالم</p></div>
<div class="m2"><p>نباشد ارچومن خسته خاکسار چه حاصل</p></div></div>