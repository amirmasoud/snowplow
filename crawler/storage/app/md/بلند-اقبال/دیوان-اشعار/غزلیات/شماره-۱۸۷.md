---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>من پی دلبر واو را بر دل منزل بود</p></div>
<div class="m2"><p>دل بیهوش مرا بین چه عجب غافل بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تحفه ای از دل وجان در برجانان بردم</p></div>
<div class="m2"><p>لیک مقبول نیفتاد که ناقابل بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>«شربتی از لب لعلش نچشیدیم آخر»</p></div>
<div class="m2"><p>کوشش ما ودل ما همه بیحاصل بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر اندر خم زلف توبه زنجیر افتاد</p></div>
<div class="m2"><p>دل که دیوانه شد از عشق رخت عاقل بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی گنه طره طرار تواندر بند است</p></div>
<div class="m2"><p>ترک مستت دل مسکین مرا قاتل بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل به دل دارد اگر راه چرا پس دل من</p></div>
<div class="m2"><p>مایل مهر ودل تو به جفا مایل بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد شهر که می داد ز می توبه به ما</p></div>
<div class="m2"><p>کار او بود ریا وعلمش باطل بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوش دیدیم به میخانه به پای خم می</p></div>
<div class="m2"><p>مست افتاده چه لایشعر ولایعقل بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می شدم شهره عالم به بلنداقبالی</p></div>
<div class="m2"><p>التفات تو به حال دلم ار شامل بود</p></div></div>