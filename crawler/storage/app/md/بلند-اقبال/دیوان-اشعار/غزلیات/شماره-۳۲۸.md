---
title: >-
    شمارهٔ ۳۲۸
---
# شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>از فراقت نه چنان تنگ دلم</p></div>
<div class="m2"><p>که توان گفت چه سان تنگ دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چه سان تنگ بود دیده مور</p></div>
<div class="m2"><p>تنگ گردیده چنان تنگ دلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به محشر ببرندم به بهشت</p></div>
<div class="m2"><p>بی تو درقصر جنان تنگ دلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با همه تنگ دلی کوه غمی</p></div>
<div class="m2"><p>جای بگرفته در آن تنگ دلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کسی تنگدل است از جهتی</p></div>
<div class="m2"><p>من از آن تنگ دهان تنگ دلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکوه ها از غم هجران میگفت</p></div>
<div class="m2"><p>داشت گر نطق وبیان تنگ دلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگرم نام بلندا قبال است</p></div>
<div class="m2"><p>از چه این سان به جهان تنگ دلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست ای دل چوتو صاحب نظری درعالم</p></div>
<div class="m2"><p>که جز از دوست نبینی دگری درعالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پی کاری مروار اهل دلی عاشق شو</p></div>
<div class="m2"><p>که به از عشق نباشد هنری درعالم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بت به بتخانه بسی هست ولی نیست چوتو</p></div>
<div class="m2"><p>آهنین دل صنم سیم بری در عالم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل سخت تو نشدنرم ونشد گرم به ما</p></div>
<div class="m2"><p>نیست آه دل ما را اثری در عالم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درجان پرتوحسنت چو تجلی فرمود</p></div>
<div class="m2"><p>به جز از عشق نباشدخبری در عالم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دیده ام بس سحر وشام ندیدم گاهی</p></div>
<div class="m2"><p>چون رخ وزلف تو شام وسحری در عالم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همچوقند لبت ای شوخ نداردهرگز</p></div>
<div class="m2"><p>شهدوشیرینی و لذت شکری درعالم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به جز از سروقد و طلعت همچون قمرت</p></div>
<div class="m2"><p>برسر سرو ندیدم قمری درعالم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همچوگیسوی خم اندر خم مشک افشانت</p></div>
<div class="m2"><p>سر زد از نافه کجا مشک تری درعالم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>می توان گفت نکوبخت وبلنداقبالم</p></div>
<div class="m2"><p>دهد ار نخل امیدم ثمری درعالم</p></div></div>