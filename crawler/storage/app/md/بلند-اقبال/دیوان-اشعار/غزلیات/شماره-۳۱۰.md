---
title: >-
    شمارهٔ ۳۱۰
---
# شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>نیارد سروهرگز میوه من چیزی عجب دیدم</p></div>
<div class="m2"><p>به سروقامت دلبر ز شیرین لب رطب دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیدم در رطب نه در شکر نه درعسل هرگز</p></div>
<div class="m2"><p>من این شیرین وشهدی که درآن لعل لب دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین حسنی که دارد پارسی گوترک شوخ من</p></div>
<div class="m2"><p>نه از شهر عجم خیزد نه درملک عرب دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سزد از وصل رخسارت گرم در راحت اندازی</p></div>
<div class="m2"><p>که از درد وغم هجرت بسی رنج وتعب دیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زند گه چنگ در گوشت کندگه جای بر دوشت</p></div>
<div class="m2"><p>مگر زلف توبدمست است کو را بی ادب دیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به رویت ماه اگر از روشنی گفتم مرنج از من</p></div>
<div class="m2"><p>ندیدم خوب رویت را چو درتاریک شب دیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر دوش از شراب وصل جانان جرعه نوش آمد</p></div>
<div class="m2"><p>بلنداقبال را امروز در وجد وطرب دیدم</p></div></div>