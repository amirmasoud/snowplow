---
title: >-
    شمارهٔ ۴۹۴
---
# شمارهٔ ۴۹۴

<div class="b" id="bn1"><div class="m1"><p>آشوب شهری شورجهانی</p></div>
<div class="m2"><p>ماه زمینی شاه زمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رخ عدوی اسلام وکفری</p></div>
<div class="m2"><p>وز قد بلای پیر وجوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم سست عهدی هم سخت روئی</p></div>
<div class="m2"><p>درتلخ گوئی شیرین زبانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو به دوران نبودولیکن</p></div>
<div class="m2"><p>این است کای ماه نامهربانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم سروقدی هم ماه روئی</p></div>
<div class="m2"><p>هم دل فریبی هم جان ستانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سروت ندانم ماهت نخواهم</p></div>
<div class="m2"><p>کز قامت و رخ به زاین وآنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل مگو هیچ وصف از دهانش</p></div>
<div class="m2"><p>کی هیچ از هیچ گفتن توانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی شنیدم اگرگاهی از لبت سخنی</p></div>
<div class="m2"><p>نبودهیچ گمانم که باشدت دهنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به حسن مادر گیتی نزاده همچو توئی</p></div>
<div class="m2"><p>به عشق دیده دوران ندیده همچومنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه کس چوخد تو دیده است ماه در فلکی</p></div>
<div class="m2"><p>نه همچو قد تو رسته است سرودرچمنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به غیر از این که تو را تن عیان به پیرهن است</p></div>
<div class="m2"><p>که دیده روح مجسم میان پیرهنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به چاه یوسف یعقوب اگر اسیر افتاد</p></div>
<div class="m2"><p>تویوسف منی وباشدت چه ذقنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منم تووتومنی هیچکس به جز من وتو</p></div>
<div class="m2"><p>ندیده است که باشد دو روح در بدنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر آنکه از می لعل توقطره ای بچشد</p></div>
<div class="m2"><p>به کام اوندهد لذتی شراب دنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آن به زلف توافتاده پیچ وتاب وشکنج</p></div>
<div class="m2"><p>دل شکسته ز بس بسته ای به هر شکنی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کسی ندیده به بتخانه ها بتی چون تو</p></div>
<div class="m2"><p>که باشدش دلی از آهن وز سیم تنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو آنغزال ز صیادکرده رم هرگز</p></div>
<div class="m2"><p>به دام کس نتوان آردت به هیچ فنی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دمید عاقبت الامر خط به گرد لبت</p></div>
<div class="m2"><p>ببین که خاتم جم شد نصیب اهرمنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به زلف خویش بگو رهزنی دگر نکند</p></div>
<div class="m2"><p>که سر برند به هر جا که هست راهزنی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز عشق دوست کشد دست کی بلنداقبال</p></div>
<div class="m2"><p>اگر بمیرد و پوشند برتنش کفنی</p></div></div>