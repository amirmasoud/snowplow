---
title: >-
    شمارهٔ ۳۵۵
---
# شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>ز سوز عشق تودرسینه آتشی داریم</p></div>
<div class="m2"><p>در آتشیم وعجب حالت خوشی داریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شودنصیب دل ما هر آن بلا که رسد</p></div>
<div class="m2"><p>دل شکسته زار بلاکشی داریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چشمهای توما را دلی بود بیمار</p></div>
<div class="m2"><p>ز طره های توحال مشوشی داریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرفته نقش خیال رخ تو دردل ما</p></div>
<div class="m2"><p>بیا که منزل خاص منقشی داریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ما مگو زچه شوریده ایم و دیوانه</p></div>
<div class="m2"><p>مگر نه چون تو نگار پریوشی داریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توراست سیم ز ساق ومراست زر از چهر</p></div>
<div class="m2"><p>عجب من وتوزر وسیم بی غشی داریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفتمش چه رخت کرده با بلند اقبال</p></div>
<div class="m2"><p>بگفت بر شه شطرنج اوکشی داریم</p></div></div>