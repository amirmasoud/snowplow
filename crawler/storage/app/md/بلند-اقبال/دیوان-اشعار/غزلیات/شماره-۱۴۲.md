---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>در بر دلدار کی رفتم که دلداری نکرد</p></div>
<div class="m2"><p>کی غم دل پیش او گفتم که غمخواری نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی به اوگفتم که بیمارم چو چشمت از غمت</p></div>
<div class="m2"><p>کو به بالینم نشدحاضر پرستاری نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی به اوگفتم که از هجرت دلی دارم خراب</p></div>
<div class="m2"><p>کو به تعمیرش به وصل خویش معماری نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی به گلزار دل خود راه داد او را کسی</p></div>
<div class="m2"><p>کو نشدخود باغبان آنجا وگلکاری نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی به عمر از ما خطاکاران خطایی دید دوست</p></div>
<div class="m2"><p>کونشد ستار عیب ما وغفاری نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با کسی گر داوری می بود ما را در جهان</p></div>
<div class="m2"><p>یاوری کی خواستیم از او که او یاری نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دلم کرد آنچه دلبر از لب شنگرف گون</p></div>
<div class="m2"><p>با همه کینی که دارد چرخ زنگاری نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر دل من صد هزاران آفرین از کردگار</p></div>
<div class="m2"><p>کانچه دید آزار صابر آمد وزاری نکرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطر زلفش آنچه با مغز بلند اقبال کرد</p></div>
<div class="m2"><p>عنبر سارا نکرد ومشک تاتاری نکرد</p></div></div>