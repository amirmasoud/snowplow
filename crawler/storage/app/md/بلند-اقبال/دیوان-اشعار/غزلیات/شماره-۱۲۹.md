---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>شب از خیال روی توخوابم نمی برد</p></div>
<div class="m2"><p>در حیرتم ز گریه که آبم نمی برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم دلی کباب ولی چشمش از غرور</p></div>
<div class="m2"><p>مست است ودست سوی کبابم نمی برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی شبی به خواب توآیم خبر شدی</p></div>
<div class="m2"><p>گویا که شب ز هجر تو خوابم نمی برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب ها به کوی اوبه گدایی روم چنانک</p></div>
<div class="m2"><p>کس بوئی از ذهاب وایابم نمی برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن توجمع من شد وعشق تو خرج من</p></div>
<div class="m2"><p>کس ره به جمع وخرج حسابم نمی برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم به لب چوشکر نابی بگفت لیک</p></div>
<div class="m2"><p>لذت کسی ز شکر نابم نمی برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بندگی خیانتی ار کرده ام چرا</p></div>
<div class="m2"><p>دلبر ز زلف زیر طنابم نمی برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشق به یار وشاکیم از جور او ولی</p></div>
<div class="m2"><p>هیچ اسمی از گناه وثوابم نمی برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کس ز عشق دوست شداقبال او بلند</p></div>
<div class="m2"><p>گوید که عقل ره به جنابم نمی برد</p></div></div>