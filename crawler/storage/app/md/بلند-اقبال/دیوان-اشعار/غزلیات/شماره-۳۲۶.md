---
title: >-
    شمارهٔ ۳۲۶
---
# شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>خیمه زدسلطان عشق اندر دلم</p></div>
<div class="m2"><p>کار بس گردیده مشکل بر دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشت خون وز دیده ام آمد برون</p></div>
<div class="m2"><p>از غم شوخی پری پیکر دلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرمش با ریشه از پیکر به در</p></div>
<div class="m2"><p>جز توخواهد گر کس دیگ ردلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بندی ازگیسو بنه بر پای او</p></div>
<div class="m2"><p>تاجنون را در کند از سر دلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر ندارد دست از ابروی دوست</p></div>
<div class="m2"><p>گر به خون غلطد از آن خنجر دلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهدا کفر چه وایمان چه</p></div>
<div class="m2"><p>من اسیر آن بت کافر دلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در برم دل چون بلنداقبال نیست</p></div>
<div class="m2"><p>زآنکه باشد دربر دلبر دلم</p></div></div>