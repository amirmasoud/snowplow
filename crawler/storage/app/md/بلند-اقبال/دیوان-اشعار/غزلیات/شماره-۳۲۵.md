---
title: >-
    شمارهٔ ۳۲۵
---
# شمارهٔ ۳۲۵

<div class="b" id="bn1"><div class="m1"><p>ای دل آزار مباش از پی آزار دلم</p></div>
<div class="m2"><p>که نباشد به کسی جز تو سروکار دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد از ابروو مژگان به کفش تیر وکمان</p></div>
<div class="m2"><p>چشم توهستم گر بر سر پیکار دلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترک خونخوار توگردیده عدوی دل من</p></div>
<div class="m2"><p>که خدا باد ازین فتنه نگهدار دلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من بختی مست است وندارد پروا</p></div>
<div class="m2"><p>هر چه خواهی بکن ای دوست ز غم بار دلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم بیمار توکرده است دلم را بیمار</p></div>
<div class="m2"><p>جز غم عشق توکس نیست پرستار دلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل من خون شد و از دیده من بیرون شد</p></div>
<div class="m2"><p>آفرین بر دل من باد وبه کردار دلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز غم از دل نکشم آه که ترسم سوزد</p></div>
<div class="m2"><p>نه فلک از تف یک آه شرر بار دلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست حاشا که ز عشق تودل آزار کشم</p></div>
<div class="m2"><p>هر چه ناصح به نصیحت کندآزار دلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که شدخوار دل او رانبود بهره زعیش</p></div>
<div class="m2"><p>من دل داده از آن رو است چنین خوار دلم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای که پرسی ز دلم مر نه سپردم به تودل</p></div>
<div class="m2"><p>دل بر توست نیی ازچه خبر دار دلم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به گرفتاری دل چاره گری کن ورنه</p></div>
<div class="m2"><p>همه دانندطبیبا که گرفتار دلم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تاچوجان در دل من جای گرفتی ای دوست</p></div>
<div class="m2"><p>شده اند اهل جهان دشمن خونخوار دلم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با رگ وریشه برون آرمش از پیکر خویش</p></div>
<div class="m2"><p>تا نگردد کسی آگاه ز اسرار دلم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیده دل شده روشن چوبلنداقبالم</p></div>
<div class="m2"><p>شده تا عارض توشمع شب تار دلم</p></div></div>