---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>نه من به عشق تو امروز خسته وزارم</p></div>
<div class="m2"><p>که کرده اندبه روز ازل گرفتارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفتم از پی خوبان دلا مروگفتا</p></div>
<div class="m2"><p>گمانم اینکه یقین کرده ای که مختارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر به دست من است اختیار من بگذار</p></div>
<div class="m2"><p>که تا بسوزم و سازم کنی چه آزارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دست هیچ کسی اختیار کاری نیست</p></div>
<div class="m2"><p>توگرخبر نیی از کار من خبر دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گردن است مرا رشته ای ز طره دوست</p></div>
<div class="m2"><p>به هر طرف که کشد می روم چو ناچارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهی مرا به یمین می بردگهی به یسار</p></div>
<div class="m2"><p>گهی عزیز نماید گهی کند خوارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که آمده اقبال اوبلند از عشق</p></div>
<div class="m2"><p>چرا خبر نبود از من و ز اسرارم</p></div></div>