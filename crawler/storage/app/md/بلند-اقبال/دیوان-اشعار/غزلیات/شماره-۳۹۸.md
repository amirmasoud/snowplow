---
title: >-
    شمارهٔ ۳۹۸
---
# شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>من چیستم که دم زنم از عشق روی تو</p></div>
<div class="m2"><p>من کیستم که راه دهندم به کوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هر طرف به سوی تو راه است واین عجب</p></div>
<div class="m2"><p>کز هیچ سوی کس نبرد ره به سوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر غائبی ز دیده مرا حاضری به دل</p></div>
<div class="m2"><p>ز آنروهمی مراست به دل گفتگوی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غفلت نگر که روز وشب اندربر منی</p></div>
<div class="m2"><p>من هر طرف فتاده پی جستجوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر صبح وشام در نظری زآنکه صبح وشام</p></div>
<div class="m2"><p>نوری بود ز روی تو عکسی ز موی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خفاش تاب دیدن خورشید نیستش</p></div>
<div class="m2"><p>نقصان ز ما بود که نبینیم روی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چیزی نمانده تا که ز مستی شوم هلاک</p></div>
<div class="m2"><p>ساقی چه باده بود مگر در سبوی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمر ابد ز آب حیات ار گرفت خضر</p></div>
<div class="m2"><p>ما راست نیز عمر ابد ز آب جوی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اقبال من بلنداست از آنکه چون قدت</p></div>
<div class="m2"><p>آمد دلم صنوبری از آرزوی تو</p></div></div>