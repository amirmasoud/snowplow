---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>سر زلف تومی گردد به رخسار توگاهی کج</p></div>
<div class="m2"><p>و یا بهر گزیدن می شود مار سیاهی کج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو طفل اندر گه تعلیم پیش اوستاد خود</p></div>
<div class="m2"><p>همی زلفت شود ازبادگاهی راست گاهی کج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفگتم راستی نسبت دهم بامشک زلفت را</p></div>
<div class="m2"><p>به پیچ وتاب رفت وکردبررویم نگاهی کج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زلفت مشک چین گفتم غلط کردم خطا گفتم</p></div>
<div class="m2"><p>پریشان بودم وآشفته افتادم به راهی کج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو قدت راست بالا شداگر سرو چمن لیکن</p></div>
<div class="m2"><p>چوتو بر دوش وسردارد کجا زلف وکلاهی کج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجز ابروی توبر روی توهرگز ندیدم من</p></div>
<div class="m2"><p>که محرابی بود درمسجدی یا خانقاهی کج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صف برگشته مژگانت پی تاراج جان ودل</p></div>
<div class="m2"><p>بدان ماند که کرده بهر غارت قد سیاهی کج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه باکت گر بلند اقبال قدش از غمت کج شد</p></div>
<div class="m2"><p>ندارد باغبان غم گر شود شاخ گیاهی کج</p></div></div>