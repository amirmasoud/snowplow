---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>دلبر ما نه همی طره پر خم با اوست</p></div>
<div class="m2"><p>هر چه حسن است در آفاق مسلم با اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشمر ای دل صفت حسن که دارد همه را</p></div>
<div class="m2"><p>مهربانی بود وبس که همی کم با اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه بیمار از آنیم که یار است طبیب</p></div>
<div class="m2"><p>همه مجروح دلانیم که مرهم با اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمت از مژه اگر لشکر توران دارد</p></div>
<div class="m2"><p>دل ما فتح کندشوکت رستم با اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک محرم به همه سال بود با دو ربیع</p></div>
<div class="m2"><p>چهر و زلف تو ربیعی دو محرم با اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می چونوشی وشوی مست عذارت ز عرق</p></div>
<div class="m2"><p>به نظر تازه گلی هست که شبنم با اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم ازعشق رخ دوست بلنداقبال است</p></div>
<div class="m2"><p>با وجودی که غم ومحنت عالم با اوست</p></div></div>