---
title: >-
    شمارهٔ ۴۶۳
---
# شمارهٔ ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>دل ز آهن تو سیم تن داری</p></div>
<div class="m2"><p>یا به من کینه کهن داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کینه هائی که داری اندر دل</p></div>
<div class="m2"><p>همه را از برای من داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه همی چین به زلف خم درخم</p></div>
<div class="m2"><p>که دراو تبت و ختن داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خودتوئی گلعذار وغنچه دهان</p></div>
<div class="m2"><p>چه هوای گل و چمن داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه به گلشن روی که از قد وبر</p></div>
<div class="m2"><p>خود سهی سرو ونسترن داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رشک می آیدم که می بینم</p></div>
<div class="m2"><p>به بر خویش پیرهن داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم از چشم مورتنگ تر است</p></div>
<div class="m2"><p>تنگ تر از دلم دهن داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبری دل چرا ز دشمن ودوست</p></div>
<div class="m2"><p>که تو ترکان راهزن داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>توئی آن بت که چون بلنداقبال</p></div>
<div class="m2"><p>در کلیسا بسی شمن داری</p></div></div>