---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>از غم عشق تو رسوای جهان گردیده ام</p></div>
<div class="m2"><p>بی دل و آشفته وآتش به جان گردیده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه خبر دارم ز دین ونه به دل پروای کفر</p></div>
<div class="m2"><p>فارغ از عشقت هم از این هم از آن گردیده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه خبر دارم ز دین ونه به دل پروای کفر</p></div>
<div class="m2"><p>فارغ از عشقت هم از این هم از آن گردیده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامنم از بسکه پر اختر شده است از اشک چشم</p></div>
<div class="m2"><p>می سزد دعوی نمایم آسمان گردیده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در برم دل خون نشد یکبارگی از عشق تو</p></div>
<div class="m2"><p>ز این تکاهل از دل خود بدگمان گردیده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شود روی گلستان چون رسد فصل خزان</p></div>
<div class="m2"><p>من هم از درد فراقت آن چنان گردیده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبح کردم شام هجرت را به امید وصال</p></div>
<div class="m2"><p>درغمت روئین تن وصاحبقران گردیده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بلند اقبال اندر نظم ونثر از عشق تو</p></div>
<div class="m2"><p>شور شهر وفتنه آخر زمان گردیده ام</p></div></div>