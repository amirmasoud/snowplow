---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>دلا غلام در دوست باش و سلطان باش</p></div>
<div class="m2"><p>هر آنچه حکم نماید مطیع فرمان باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تورا چوخاتم فرماندهی به کف دادند</p></div>
<div class="m2"><p>به پشت باد بزن تخت را سلیمان باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نصیحتی کنمت ترک آرزوها کن</p></div>
<div class="m2"><p>زکار خویش وزکردار خود پشیمان باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو صبح عید رسد از پی تقرب خویش</p></div>
<div class="m2"><p>به پیش دلبر خود گوسفند قربان باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را ز یوسف گمگشته چون به دل داغ است</p></div>
<div class="m2"><p>به کنج بیت حزن همچو پیر کنعان باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چگونه جمع شود عاشقی وخاطر جمع</p></div>
<div class="m2"><p>چو زلف یا راگر عاشقی پریشان باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غمین مباش غم عالم ار شود یارت</p></div>
<div class="m2"><p>چوبختیان قوی پشت سخت کوهان باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به یاد آن صنم سروقد گل رخسار</p></div>
<div class="m2"><p>چه حاجتت به گلستان تو خود گلستان باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرت هوا است که چون من شوی بلنداقبال</p></div>
<div class="m2"><p>ز جان ودل بگذر محو روی جانان باش</p></div></div>