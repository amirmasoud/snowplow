---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>بسکه مشکین مو ز بس نیکورخ است</p></div>
<div class="m2"><p>ترک من آشوب چین و خلخ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست با گلشن سرو کارم که او</p></div>
<div class="m2"><p>سرو قامت یاسمن بو گل رخ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شه شطرنج هر جا روکند</p></div>
<div class="m2"><p>پیش پای پیلتن اسبش رخ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تند خو ترکی مرا باشد کز او</p></div>
<div class="m2"><p>هر چه خواهم گر چه یخ باشد یخ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کنم هر گه تمنای وصال</p></div>
<div class="m2"><p>لن ترانی بر زبانش پاسخ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هاله خط ماه رویش را گرفت</p></div>
<div class="m2"><p>ای دل آوخ گو که جای آوخ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بلند اقبال رویش هر که دید</p></div>
<div class="m2"><p>طالعش مسعود و بختش فرخ است</p></div></div>