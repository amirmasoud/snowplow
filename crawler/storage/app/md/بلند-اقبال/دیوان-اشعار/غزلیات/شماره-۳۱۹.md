---
title: >-
    شمارهٔ ۳۱۹
---
# شمارهٔ ۳۱۹

<div class="b" id="bn1"><div class="m1"><p>من همی گویم که عاشق بر رخ آن دلبرم</p></div>
<div class="m2"><p>آبرو بردم زعشق ای خاک عالم بر سرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن سمندر بود کاندر آتش سوزان بسوخت</p></div>
<div class="m2"><p>من ندارم تاب این کز پیش اتش بگذرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد دارم اینکه با شمعی شبی پروانه گفت</p></div>
<div class="m2"><p>عاشقم بر روی تو نبود غم ار سوزد پرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع با پروانه گفت از عشقم ار سوزی تو پر</p></div>
<div class="m2"><p>من ز عشق انگبین می سوزد از پا تا سرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق را با دوست چون بینم که باشدمتفق</p></div>
<div class="m2"><p>دوست را بینم به چشم سر چو برخود بنگرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق دلبر نیست امروزی که من دارم به دل</p></div>
<div class="m2"><p>پرورش می داد با شیرین به پستان مادرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هستم از عشق رخ جانان بلند اقبال لیک</p></div>
<div class="m2"><p>پست تر از خاک ره ووز ذره پیشش کمترم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصلت امشب کرده روزی کرکرم</p></div>
<div class="m2"><p>ده ز لب یک بوسه داری گر کرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخ نمی تابم از اوحربا صفت</p></div>
<div class="m2"><p>آفتاب عارضت را کرگرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آهوی چشم توباشد شیر گیر</p></div>
<div class="m2"><p>نی عجب از اوکند گر گرگ رم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشنوم تا پند ناصح را زعشق</p></div>
<div class="m2"><p>شکر گویم کرده گر کرک کرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون بلند اقبال اقبالم بلند</p></div>
<div class="m2"><p>گردد از لعلت دهی بوسی گرم</p></div></div>