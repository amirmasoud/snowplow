---
title: >-
    شمارهٔ ۳۸۲
---
# شمارهٔ ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>این صنم کیست که نبود بشری بهتر از این</p></div>
<div class="m2"><p>کس ندیده است ونبیند دگر بهتر از این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر که غلمان پدری گردد وحوری مادر</p></div>
<div class="m2"><p>که نیارند به عالم بشری بهتر از این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش از نظری دین ودلم بردی گفت</p></div>
<div class="m2"><p>می کنم باز به حالت نظری بهتر از این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش طلعت و زلفت به چه ماندگفتا</p></div>
<div class="m2"><p>کی به عقرب شده طالع قمری بهتر از این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش زلف تو دیگر به چه می ماند گفت</p></div>
<div class="m2"><p>با هما بوده کجا بال وپری بهتر از این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش باز بگو باز بخندید وبگفت</p></div>
<div class="m2"><p>ناید از نافه برون مشک تری بهتر از این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم او را که چه شد آن لب شیرینت گفت</p></div>
<div class="m2"><p>هرگز از هند نخیزد شکری بهتر از این</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتمش شهد لبت را چو رطب دیدم گفت</p></div>
<div class="m2"><p>نخل فردوس نیارد ثمری بهتر ازاین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم ابروی تو راسینه سپر کردم گفت</p></div>
<div class="m2"><p>پیش شمشیر من آور سپری بهتر از این</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم آئینه روی تو غباری دارد</p></div>
<div class="m2"><p>گفت آه تو ندارد اثری بهتر از این</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم از عشق تو دارم رخ و اشکی گفتا</p></div>
<div class="m2"><p>یابی از همت ما سیم وزری بهتر از این</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتمش در کفت اشعار بلند اقبال است</p></div>
<div class="m2"><p>گفت کی بوده به عمان گهری بهتر از این</p></div></div>