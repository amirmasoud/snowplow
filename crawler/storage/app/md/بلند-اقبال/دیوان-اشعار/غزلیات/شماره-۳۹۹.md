---
title: >-
    شمارهٔ ۳۹۹
---
# شمارهٔ ۳۹۹

<div class="b" id="bn1"><div class="m1"><p>بسته دلم را به بند حلقه گیسوی تو</p></div>
<div class="m2"><p>کرده قدم را کمان حالت ابروی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرورود دررکوع گر تو درآئی به باغ</p></div>
<div class="m2"><p>مه ننماید طلوع پیش مه روی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاریه بگرفته رنگ سرخ گل از عارضت</p></div>
<div class="m2"><p>وام نموده است بوی مشک تر از بوی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوی گلی کارگر نیست مرا بر مشام</p></div>
<div class="m2"><p>زآنکه بود در زکام مغز من از بوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای تو چوخورشید ومن پیش توحر با صفت</p></div>
<div class="m2"><p>جلوه به هر سو کنی روی کنم سوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوه گران را زجای کس نتواند کند</p></div>
<div class="m2"><p>کنده دلم را زجای قوت بازوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آتش دوزخ مرا نیست دگر هیچ باک</p></div>
<div class="m2"><p>زآنکه به عمری مراست پرورش از خوی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حور و بهشتی دگر هیچ نمی خواستم</p></div>
<div class="m2"><p>بود مرا گر به دهر جا به سر کوی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بودگر اقبال من چون سر زلفت بلند</p></div>
<div class="m2"><p>چون سر زلفت سرم بود به زانوی تو</p></div></div>