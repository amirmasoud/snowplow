---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>بسکه درزلفت دل من ذکر یا رب میکند</p></div>
<div class="m2"><p>خواب خلقی راحرام از دیده هر شب می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون رخت بینم به زلفت میشوم گریان بلی</p></div>
<div class="m2"><p>بارش آید مه چو جا در برج عقرب می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می سزدزلف تورا خواند اگر کس لف و نشر</p></div>
<div class="m2"><p>زآنکه خود را گه مشوش گه مرتب می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل پر ازخون گرددم از درد وحسرت چون انار</p></div>
<div class="m2"><p>هرکه پیشم وصفی از آن سیب غبغب می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلفش از بس کافر آمد ز ابروی چون ذوالفقار</p></div>
<div class="m2"><p>حیدر آسا ز آن دو نیمش همچو مرحب میکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسمان را رشک ها هر شب ز من آید به دل</p></div>
<div class="m2"><p>دامنم را دیده از بس پر ز کوکب می کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آن بلنداقبال در شیرین کلامی شهره شد</p></div>
<div class="m2"><p>بسکه وصف لعل آن شوخ شکر لب می کند</p></div></div>