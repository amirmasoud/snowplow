---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>بارم اندر کوی یارم درگل است</p></div>
<div class="m2"><p>درگل است ار بارم اندر منزل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادن جان باشد آسان درغمش</p></div>
<div class="m2"><p>زندگانی بی وجودش مشکل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع را می بینم امشب بر سر است</p></div>
<div class="m2"><p>از غم یار آنچه ما را در دل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خیال زلف چون زنجیر او</p></div>
<div class="m2"><p>هر که مجنون کرده خود را عاقل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست خاکی کز غمش بر سر کنم</p></div>
<div class="m2"><p>هر کجا خاکی است از اشکم گل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس نپردازد به عقل از عشق دوست</p></div>
<div class="m2"><p>آب تا باشد تیمم باطل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بت سیمین بدن سنگین دلت</p></div>
<div class="m2"><p>تا کی از حال دل ما غافل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جورکم کن با بلنداقبال کو</p></div>
<div class="m2"><p>مدح گوی پادشاه عادل است</p></div></div>