---
title: >-
    شمارهٔ  ۳۱
---
# شمارهٔ  ۳۱

<div class="b" id="bn1"><div class="m1"><p>می دهی درد سرم چند ای طبیب</p></div>
<div class="m2"><p>دردما را چاره باید ازحبیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستم از دامان وصلش کوته است</p></div>
<div class="m2"><p>ای خدا یا وصل یا مرگ رقیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز و شب نالم ز عشق روی او</p></div>
<div class="m2"><p>چون به گلشن در بهاران عندلیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی عجب چشمش گر از من برد دل</p></div>
<div class="m2"><p>چشم مست اوبودعابد فریب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیف کو دور از لب ودندان ماست</p></div>
<div class="m2"><p>گو که به باشد زنخدانش ز سیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک نگه کرد وزمن شش چیز بود</p></div>
<div class="m2"><p>دین ودل تاب وتوان صبر وشکیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می سزد از پی فراقش را وصال</p></div>
<div class="m2"><p>چون فرازی دارد از پی هر نشیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو من نبود بلنداقبال کس</p></div>
<div class="m2"><p>گر وصال او مرا گردد نصیب</p></div></div>