---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>هیچ دانی چه به من کرده غمت</p></div>
<div class="m2"><p>در برم دل شده خون از ستمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم پریشان دلم ازطره تو</p></div>
<div class="m2"><p>هم قدم خم شده از ابروی خمت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برده اند از تن من تاب وتوان</p></div>
<div class="m2"><p>قهر بسیار توولطف کمت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه مگر با من دل خسته بسی</p></div>
<div class="m2"><p>رام بودی دگر از چیست رمت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیش کز دست تو نوش است مرا</p></div>
<div class="m2"><p>می خورم گر که به جام است سمت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الغرض رفتهام از پای مگر</p></div>
<div class="m2"><p>دستگیری بنماید کرمت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته بودی که بیایی به برم</p></div>
<div class="m2"><p>سر وجانم به فدای قدمت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم از عشق هلاکم گفتا</p></div>
<div class="m2"><p>چه تفاوت به وجود وعدمت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم از چیست بلند اقبالم</p></div>
<div class="m2"><p>گفت ازخشک لب وچشم نمت</p></div></div>