---
title: >-
    شمارهٔ ۲۷۹
---
# شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>گر ببیند رخ تو را بلبل</p></div>
<div class="m2"><p>خار گردد به پیش چشمش گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم تو برده خواب از نرگس</p></div>
<div class="m2"><p>زلف توبرده تاب از سنبل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه گویند مه نموده خسوف</p></div>
<div class="m2"><p>گر پریشان کنی به رخ کاکل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلفت افتاده بر زنخدانت</p></div>
<div class="m2"><p>همچوهاروت در چه بابل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر قد تو بودخورشید</p></div>
<div class="m2"><p>بر سر سرو اگر بود صلصل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم ز رخ طعنه می زنی بر ماه</p></div>
<div class="m2"><p>هم به لب نشئه می بری از مل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر مرا کرده ای بلنداقبال</p></div>
<div class="m2"><p>مددی کن که بگذرم از پل</p></div></div>