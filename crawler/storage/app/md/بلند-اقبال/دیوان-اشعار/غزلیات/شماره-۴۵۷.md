---
title: >-
    شمارهٔ ۴۵۷
---
# شمارهٔ ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>اگر زآن زلف مشک افشان به چنگ افتدا مرا تاری</p></div>
<div class="m2"><p>نپندارم که باشد ملک چینی یا که تاتاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم را برده از کف سنگدل شوخ دل آزاری</p></div>
<div class="m2"><p>که با ما نیست او را جز دل آزاری دگرکاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنی یار اگر آزارم که دست از عشق بردارم</p></div>
<div class="m2"><p>به جانتگر بری از تن سرم را نیست آزاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا بگشا نقاب از چهر وبنما روی چون مه را</p></div>
<div class="m2"><p>که دارد ناصح من با من از عشق توانکاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببالد مشک تاتاری اگر از بوخطا باشد</p></div>
<div class="m2"><p>به کف باد صبا را باشد از زلف توتا تاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخت گنجی بوداز حسن زلفت بی سبب نبود</p></div>
<div class="m2"><p>که چنبر گشته وخوابیده رویش چون سیه ماری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو چشم مست توکاین سان بردهوش وخرد از سر</p></div>
<div class="m2"><p>نپندارم که دیگر باشد اندر شهر هشیاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر از دردهجرت گشته ام بیمار غم نبود</p></div>
<div class="m2"><p>که باشد یاد وصلت مر مرا نیکو پرستاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نباشد از وفا کس چون بلنداقبال درعالم</p></div>
<div class="m2"><p>اگرهمچون تو باشند از جفا وجوربسیاری</p></div></div>