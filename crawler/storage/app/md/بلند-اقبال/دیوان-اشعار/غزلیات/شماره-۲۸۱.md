---
title: >-
    شمارهٔ ۲۸۱
---
# شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>ای نگار سیم تن ای بی وفای سنگدل</p></div>
<div class="m2"><p>چون دهان خود مرا تا چند داری تنگدل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای می لعل لبت زنگ دل ما را ببر</p></div>
<div class="m2"><p>زآنکه افتاده است از هجرت مرا در زنگ دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نگاهی آهوی چشم تو از چنگش برد</p></div>
<div class="m2"><p>گر ببیند شیر گردون را بود در چنگ دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بر آن بودم که دیگر دل به کس ندهم ولی</p></div>
<div class="m2"><p>بردناگه از کفم آن ترک شوخ و شنگ دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بلنداقبال گردد درجهان آسوده حال</p></div>
<div class="m2"><p>هر که را آسوده گشت از فکرنام وننگ دل</p></div></div>