---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>تورابه است ز به غبغب وز سیب زنخ</p></div>
<div class="m2"><p>به دستم آید اگر این دو یکزمان بخ بخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز کشته ها همی از بس که پشته ها سازی</p></div>
<div class="m2"><p>به هرکجا که گذر می کنی شودمسلخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفو بچاک دل ما نمی شود جز این</p></div>
<div class="m2"><p>که گیری از مژه سوزن ز تار گیسو نخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیش چهر تو آتش بدان همه گرمی</p></div>
<div class="m2"><p>روا بود شود افسرده تر اگر از یخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نثار بزم حضورت اگر کنم سرو جان</p></div>
<div class="m2"><p>همان حدیث سلیمان شده است و ران ملخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا به جان تو نزدیک تر ز جان ودلی</p></div>
<div class="m2"><p>کننددورم اگر از برت دو صدفرسخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه فخرها که به خاقان کند بلنداقبال</p></div>
<div class="m2"><p>چو زنگیش دهی ار جا به گوشه مطبخ</p></div></div>