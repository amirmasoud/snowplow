---
title: >-
    شمارهٔ ۴۵۱
---
# شمارهٔ ۴۵۱

<div class="b" id="bn1"><div class="m1"><p>تاکس نبیندت ز نظرها نهان شدی</p></div>
<div class="m2"><p>تا پی کسی سویت نبرد لامکان شدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود تو را مکان و گرفتی به دل قرار</p></div>
<div class="m2"><p>گشتی زما نهان وبه هر سوعیان شدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کس که داشت جان ودلی بردی از کفش</p></div>
<div class="m2"><p>از بس همی بلای دل وخصم جان شدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما درددل به جز تونگوئیم پیش کس</p></div>
<div class="m2"><p>زیرا که محرم دل پیر وجوان شدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناگفته دادی آنچه دلم داشت آرزو</p></div>
<div class="m2"><p>ای غائب از نظر چه عجب غیب دان شدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بودی هر آنچه بودی وهستی وجز تو نیست</p></div>
<div class="m2"><p>گفتم بسی غلط که چنین یا چنان شدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اقبال ما که گشته چنین در جهان بلند</p></div>
<div class="m2"><p>زآنروبود که بادل ما مهربان شدی</p></div></div>