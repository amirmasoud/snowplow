---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>امروز عمر هم به فراق توشام شد</p></div>
<div class="m2"><p>درانتظار زندگی ما حرام شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ماه تا ز دیده من لایری شدی</p></div>
<div class="m2"><p>دل لامکان ودیده مرا لاینام شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر تو را ز خلق نهان داشتم ولی</p></div>
<div class="m2"><p>افشا ز دردهجر به خاط وبه عام شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمم گر از غم تونگرید عجب مدار</p></div>
<div class="m2"><p>کز بس گریست خون دل من تمام شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دید آن زمان که مرغ دلم زلف وخال تو</p></div>
<div class="m2"><p>رفت از برای دانه گرفتار دام شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جستم بهزلف تو دل گم گشته را زبس</p></div>
<div class="m2"><p>گاهی مثال دال وگهی شکل لام شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عنبر چودید زلف توفیروزه خط تو</p></div>
<div class="m2"><p>فیروزه ات کنیزک وعنبر غلام شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر بلنداقبال از وصل لعل تو</p></div>
<div class="m2"><p>اندر غزل سرائی شیرین کلام شد</p></div></div>