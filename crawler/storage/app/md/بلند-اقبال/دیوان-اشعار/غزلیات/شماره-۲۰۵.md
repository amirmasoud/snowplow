---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>ای باعث ایجاد جهان احمد محمود</p></div>
<div class="m2"><p>ای گشته جهان وآنچه در او بهر تو موجود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعل تو روانبخش تر از عیسی مریم</p></div>
<div class="m2"><p>زلف تو زره سازتر از حضرت داود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش که برم حاجت وآرم به کجا روی</p></div>
<div class="m2"><p>گردم اگر از درگه تو رانده ومردود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچون دگران بود وصال تو نصیبم</p></div>
<div class="m2"><p>می بود مرا نیز اگر طالع مسعود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر تن من نیست به جز عشق تومدغم</p></div>
<div class="m2"><p>اندر دل من نیست مگر وصل تومقصود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوراه که روی سوی توآریم به زاری</p></div>
<div class="m2"><p>کزچار جهت بر رخ ما ره شده مسدود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اقبال بلندی که مرا بود ز عشقت</p></div>
<div class="m2"><p>شد از بر منز انده هجران تو مفقود</p></div></div>