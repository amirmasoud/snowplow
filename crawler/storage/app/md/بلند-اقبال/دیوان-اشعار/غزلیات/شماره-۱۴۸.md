---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>سزد بلبل به گل گر در گلستان هم نفس باشد</p></div>
<div class="m2"><p>چرا پس جان من در تن گرفتار قفس باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چومرغی کز قفس باشد هوای گلشنش بر سر</p></div>
<div class="m2"><p>به سیر عالم علوی مرا در دل هوس باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جان منچه سزد کاین چنین شد پای بست تن</p></div>
<div class="m2"><p>به مستی فی المثل ماند که در قیدعسس باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جسمانی علایق دمبدم کاهد همی جانم</p></div>
<div class="m2"><p>چو حلوایی که گردش روز وشب مور ومگس باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا آسایش ار خواهی بیفکن پیرهن از تن</p></div>
<div class="m2"><p>که بار پیرهن برتن تو را از پوست بس باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلند اقبال جسمش آن چنان کاهیده شد از غم</p></div>
<div class="m2"><p>که هر گه دلبر او را دید گوید این چه کس باشد</p></div></div>