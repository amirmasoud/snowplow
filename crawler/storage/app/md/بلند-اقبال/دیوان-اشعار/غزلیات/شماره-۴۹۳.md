---
title: >-
    شمارهٔ ۴۹۳
---
# شمارهٔ ۴۹۳

<div class="b" id="bn1"><div class="m1"><p>آن مه فکنده از زلف بررخ حجاب نیمی</p></div>
<div class="m2"><p>یا قرص آفتاب است زیر سحاب نیمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رفته درخسوف است یا مانده درکسوف است</p></div>
<div class="m2"><p>نیمی ز ماهتابان وز آفتاب نیمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل دور از جمالش وز وعده وصالش</p></div>
<div class="m2"><p>نیمی شده است آباد مانده خراب نیمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ابرو وطره جانان بگرفت از تنم جان</p></div>
<div class="m2"><p>نیمی به ضرب شمشیر زیر طناب نیمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل کی ز زلف دلبر گرددرها که هر سو</p></div>
<div class="m2"><p>نیمی شکنج وچین است پرپیچ وتاب نیمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خواب رخ نماید یا از درم درآید</p></div>
<div class="m2"><p>بیدار مانده چشمم نیمی به خواب نیمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرمایه وجودم از آه واشک دیده</p></div>
<div class="m2"><p>نیمی بسوخت ز آتش شد غرق آب نیمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آنچشم مست میگون بریان دلم شد وخون</p></div>
<div class="m2"><p>نیمی شراب او شد او راکباب نیمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غارتگری نمودند دین و دلم ربودند</p></div>
<div class="m2"><p>نیمی شراب و ساقی چنگ ورباب نیمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دردوغم دوعالم خواهی اگر بدانی</p></div>
<div class="m2"><p>نیمی شب فراق است روز حساب نیمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کن ساقیا خرابم اما نه ز آن شرابم</p></div>
<div class="m2"><p>کو چون حروفش آمد نیمی شر آب نیمی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز‌آن باده کن مرا مست کز مستیش شوم هست</p></div>
<div class="m2"><p>نیمی ز جام احمد وز بوتراب نیمی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اقبال من بلند است فیروز وارجمند است</p></div>
<div class="m2"><p>نیمی از آن شهنشاه وز اینجناب نیمی</p></div></div>