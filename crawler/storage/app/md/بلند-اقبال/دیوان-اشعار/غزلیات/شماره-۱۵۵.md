---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>تا به زلف تو دلم ملحق شد</p></div>
<div class="m2"><p>این پریشانی از او مشتق شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تومگر روی به مه بنمودی</p></div>
<div class="m2"><p>که مه از نورجمالت شق شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بت ما رفت به بتخانه مگر</p></div>
<div class="m2"><p>که چنین بتکده بی رونق شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دود آه دل ما شد به فلک</p></div>
<div class="m2"><p>که چنین روی فلک ازرق شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه با عشق تواش نیست سری</p></div>
<div class="m2"><p>درهمان روز ازل احمق شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درمیان حق وباطل چه کند</p></div>
<div class="m2"><p>آنکه جاهل به حد مرفق شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما نگیریم قرار اندر نار</p></div>
<div class="m2"><p>زآنکه ما را دل وجان زیبق شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی از عشق بلند اقبال است</p></div>
<div class="m2"><p>که شب وروز دلش با حق شد</p></div></div>