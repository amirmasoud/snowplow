---
title: >-
    شمارهٔ ۴۴۰
---
# شمارهٔ ۴۴۰

<div class="b" id="bn1"><div class="m1"><p>چه شد که مه بریدی وعهد بشکستی</p></div>
<div class="m2"><p>مرا به بند ببستی خود از میان جستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنی به ساغر ما سنگ و بر رخ ما چنگ</p></div>
<div class="m2"><p>تو را به ما سر جنگ است یا که بدمستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گناه سستی بخت من است بسکه چنین</p></div>
<div class="m2"><p>توسخت دل دل آزرده مرا خستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من آنچه گفتم وگویم خلاف نیست در آن</p></div>
<div class="m2"><p>تو هر چه عهد ببستی نبسته بشکستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توچون به چشم ودلت نیست بخشش وهمت</p></div>
<div class="m2"><p>اگر به دولت قارون رسی تهی دستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر نه ما و تو را راه مرگ در پیش است</p></div>
<div class="m2"><p>بگیر توشه ای از بهر راه تا هستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا که گفته نهی نام خود بلند اقبال</p></div>
<div class="m2"><p>تو کاین چنین ز غم هجر آن صنم پستی</p></div></div>