---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>چه خوردی که گشتی چنین ارغوان رخ</p></div>
<div class="m2"><p>بگوتا کنیم ارغوانی از آن رخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو قد و رخت سرو ومه بود بودی</p></div>
<div class="m2"><p>گر این را چنین زلف و آنرا چنان رخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شفق لاله گون گشته از عکس رویت</p></div>
<div class="m2"><p>مگر دوش کردی سوی آسمان رخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود باغ خرم تر از نوبهاران</p></div>
<div class="m2"><p>به باغ ار گشانی به فصل خزان رخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دیدار خود تا دهی قوت جان ها</p></div>
<div class="m2"><p>چو یوسف نشان ده به پیر وجوان رخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شطرنج عشق تو شه مات گردد</p></div>
<div class="m2"><p>برش باز کن از پی امتحان رخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنی دلبری چون پری زآنکه هر دم</p></div>
<div class="m2"><p>بری هی دل از ما کنی هی نهان رخ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد اقبال اوهمچو من از بلندی</p></div>
<div class="m2"><p>تورا هر که سائیده بر آستان رخ</p></div></div>