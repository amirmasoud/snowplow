---
title: >-
    شمارهٔ ۴۰۱
---
# شمارهٔ ۴۰۱

<div class="b" id="bn1"><div class="m1"><p>بر درد دل خسته ام دوست دوا شو</p></div>
<div class="m2"><p>بنما رخ و غارتگر دین ودل ما شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنهان مشو از ما بنما گوشه ابرو</p></div>
<div class="m2"><p>مانند مه یک شبه انگشت نما شو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاچند جفا میکنی ای شوخ دل آزار</p></div>
<div class="m2"><p>کن ترک جفا با من بی دل به صفا شو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای طره دلدار که چون پر غرابی</p></div>
<div class="m2"><p>کن همتکی بر سر ما بال هما شو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای پیر خرابات همه گمره ومستیم</p></div>
<div class="m2"><p>ما را به خدا راهبر وراهنما شو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اقبال بلند ار طلبی ای دل غمگین</p></div>
<div class="m2"><p>روخاک نشین بر در دلبر چوگدا شو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاچند روی بهر دو نان از پی دونان</p></div>
<div class="m2"><p>حاجت مطلب از کس ومحتاج خدا شو</p></div></div>