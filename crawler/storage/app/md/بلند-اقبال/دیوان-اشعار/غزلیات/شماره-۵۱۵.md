---
title: >-
    شمارهٔ ۵۱۵
---
# شمارهٔ ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>از چیست ترشروئی بهر چه تلخ گوئی</p></div>
<div class="m2"><p>اندر شکستن عهدتا کی بهانه جوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردی سیاه روزم از بس سیاه چشمی</p></div>
<div class="m2"><p>کردی سیاه بختم از بس سیاه موئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر درد داری از من درمان چرا نخواهی</p></div>
<div class="m2"><p>ورغم توراست در دل بامن چرا نگوئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چندتندخوئی درعهدو مهر کندی</p></div>
<div class="m2"><p>مریخی از طبیعت با اینکه ماهروئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حورت اگر بگویم هستی نکوتراز وی</p></div>
<div class="m2"><p>غلمانت ار بخوانم صد باره به از اوئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمری بودکه با من باشد بدی شعارت</p></div>
<div class="m2"><p>بدهم نکوست ازتو از بس همی نکوئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشکل که کردی ای دل چون من بلنداقبال</p></div>
<div class="m2"><p>زیرا که گاه بیگاه در قید آرزوئی</p></div></div>