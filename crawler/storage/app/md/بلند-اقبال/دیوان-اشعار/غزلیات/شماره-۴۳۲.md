---
title: >-
    شمارهٔ ۴۳۲
---
# شمارهٔ ۴۳۲

<div class="b" id="bn1"><div class="m1"><p>روی تو را ندیده دلم را ربوده ای</p></div>
<div class="m2"><p>در دلبری چه چابک و چالاک بوده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دین ودلی سراغ ندارم دگر به کس</p></div>
<div class="m2"><p>غارت همی ز بسکه دل ودین نموده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داری کجا ز حالت شب های ما خبر</p></div>
<div class="m2"><p>بیدار ما ز درد وتوفارغ غنوده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزادی از غم دوجهان داده ای مرا</p></div>
<div class="m2"><p>زنگ علایق از دل من تا زدوده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اقبال من بلند شد از اینکه درجهان</p></div>
<div class="m2"><p>از عشق بر رخم در دولت گشوده ای</p></div></div>