---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>این صنم کیست که غارتگر ایمان من است</p></div>
<div class="m2"><p>دل ودین از کف من برده پی جان من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندبیمم دهی ازمحنت محشر زاهد</p></div>
<div class="m2"><p>به خدا روز قیامت شب هجران من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلرخا غنچه لباسرو قداکچ کلها</p></div>
<div class="m2"><p>راستی روی تو غارتگر ایمان من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب لعل شکرین توچوجان شیرین است</p></div>
<div class="m2"><p>لیک افسوس که دور از لب ودندان مناست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زرد از آن مهر چو رخسار بلند اقبال است</p></div>
<div class="m2"><p>کش به دل حسرت روی مه جانان من است</p></div></div>