---
title: >-
    شمارهٔ ۲۲۷
---
# شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>زد سر زلف خود آن دلبر که اکنون دل ببر</p></div>
<div class="m2"><p>هر که جان دارد به تن از دست اوچون دل ببر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبری آموخت لیلی از نگار من که گفت</p></div>
<div class="m2"><p>پرده را از روی بردار وز مجنون دل ببر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از غم زلفت دلم در قید حسرت شد اسیر</p></div>
<div class="m2"><p>چهره بنما قید حسرت را ز پر خون دل ببر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت اسرار الهی در لبم باشدنهان</p></div>
<div class="m2"><p>گفتمش ز آن گفتمت کز لعل میگون دل ببر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت درایران وتوران دل دگر نگذاشتم</p></div>
<div class="m2"><p>گفتمش کز کوه قاف ور بع مسکون دل ببر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست چون گردی ز می از ماه گردون دل بری</p></div>
<div class="m2"><p>ناز کم کن باده زن ازماه گردون دل ببر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم ازوصل قدت طبع بلند اقبال تو</p></div>
<div class="m2"><p>کوتهی دارد بگفت از طبع موزون دل ببر</p></div></div>