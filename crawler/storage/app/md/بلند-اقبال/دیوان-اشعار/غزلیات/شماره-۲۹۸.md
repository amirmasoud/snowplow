---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>من نه مستم از شراب از چشم یار مستم</p></div>
<div class="m2"><p>از نگاهی کرده است آن دلبر عیار مستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه وشیخ وشحنه شهر آگهند از مستی من</p></div>
<div class="m2"><p>من به بزم شاه وهم درکوچه وبازار مستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرحریفان جمله مستنداز شراب می فروشان</p></div>
<div class="m2"><p>من ز صهبائی که خود پرورده آن دلدار مستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهی فرموه است شاه از مستی وآزار مردم</p></div>
<div class="m2"><p>من اگر مستم ولی بنگر که بی آزار مستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نه کورم پای تا سر چشم وچشم پر زنورم</p></div>
<div class="m2"><p>گر به رفتن دست دارم بر در ودیوار مستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطربا نی زن دمی شاید برم از دل غمی را</p></div>
<div class="m2"><p>ساقیا می ده کمی زیرا که من بسیار مستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم مست او ربود از دست هوش ودانشم را</p></div>
<div class="m2"><p>چون بلند اقبال اگر گویم چنین اشعار مستم</p></div></div>