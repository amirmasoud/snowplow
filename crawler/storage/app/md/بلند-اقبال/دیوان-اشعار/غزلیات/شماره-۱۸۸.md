---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>از غم رویش مبین کز گریه چشمم نم بود</p></div>
<div class="m2"><p>دامنم را بین که اندر هر کنارش یم بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل هر آدمی باشد در این عالم غمی</p></div>
<div class="m2"><p>من غمی دارم به دل کاندر دل عالم بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدچون تیرم کمان آسا چه غم گر گشته خم</p></div>
<div class="m2"><p>آن هلال ابروان را هم که دیدم خم بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهر اگر باشد به دست وی شودخوشتر ز نوش</p></div>
<div class="m2"><p>زخم اگر آید ز تیغ او بهاز مرهم بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمت از افراسیاب است وشدش مژگان سپاه</p></div>
<div class="m2"><p>نیست پروائی مرا هم چون دل رستم بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پری زادی یقین گر نیستی حوری نژاد</p></div>
<div class="m2"><p>کاینچنین صورت نه از نسل بنی آدم بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بلند اقبال گردیدم ز عشق آن نگار</p></div>
<div class="m2"><p>می سزد گر از وصالش هم دلم خرم بود</p></div></div>