---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>تا صبا را تاری از زلفت به دست افتاده است</p></div>
<div class="m2"><p>دررواج کار عطاران شکست افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خال در دنبال چشمت گر نباشم در غلط</p></div>
<div class="m2"><p>شحنه ای باشد که دردنبال مست افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشد از حال دل من درکمند زلف تو</p></div>
<div class="m2"><p>آگه آن ماهی که در خشکی ز شست افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دل من در شکنج طره ات دارد خبر</p></div>
<div class="m2"><p>هر کجا مرغی به دامی پای بست افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم تومخمور وزلفت می کندمستی همی</p></div>
<div class="m2"><p>با وجود اینکه لعلت می پرست افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهر ورزی با تونبودکار امروزی مرا</p></div>
<div class="m2"><p>عاشق ومعشوق را عشق از الست افتاده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بلنداقبال را از وصل کردی سرافراز</p></div>
<div class="m2"><p>پیش قدر اوبلندافلاک پست افتاده است</p></div></div>