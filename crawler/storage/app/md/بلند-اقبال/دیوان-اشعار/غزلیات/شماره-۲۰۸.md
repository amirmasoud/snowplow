---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>دردعشق یار را تدبیر نتوانم نمود</p></div>
<div class="m2"><p>پنجه اندر پنجه تقدیر نتوانم نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ره سیلاب اشک چشم بنیان دلم</p></div>
<div class="m2"><p>شد چنان ویرانه کش تعمیر نتوانم نمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چاره دیوانه این باشدکه زنجیرش کنند</p></div>
<div class="m2"><p>من دل دیوانه را زنجیر نتوانم نمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ازل دلبر پرست وباده نوش امد دلم</p></div>
<div class="m2"><p>این چنینم تا ابد تزویر نتوانم نمود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیخ را گفتم بیا با عشق او دمساز شو</p></div>
<div class="m2"><p>گفت بازی با دم این شیر نتوانم نمود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک مستش دارد از ابروبهکف شمشیر ومن</p></div>
<div class="m2"><p>جان یقین سالم از این شمشیر نتوانم نمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوره والشمس را و آیه واللیل را</p></div>
<div class="m2"><p>جز ز روی وموی اوتفسیر نتوانم نمود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بامعبر گفتم اندر خواب دیدم زلف دوست</p></div>
<div class="m2"><p>گفت من آشفته ام تعبیر نتوانم نمود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برتن من هر سر مویم شودگر صد زبان</p></div>
<div class="m2"><p>صدیک از حسن رخش تقریر نتوانم نمود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوزد انگشتم چوانگشت آتش افتددرقلم</p></div>
<div class="m2"><p>شرح دردهجر را تحریر نتوانم نمود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر دلش مایل شود بر کشتنم گردن نهم</p></div>
<div class="m2"><p>ز آنکه آن دلدار را دلگیر نتوانم نمود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با بلنداقبال گفتم ترک می کن تا به کی</p></div>
<div class="m2"><p>گفت شد حکم از ازل تغییر نتوانم نمود</p></div></div>