---
title: >-
    شمارهٔ ۳۹۷
---
# شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>ای قدم خم گشته تر ز ابروی تو</p></div>
<div class="m2"><p>وی دلم آشفته تر از موی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معنی ز والقلم را یافتم</p></div>
<div class="m2"><p>چون بدیدم بینی وابروی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیه واللیل باشد بخت من</p></div>
<div class="m2"><p>سوره والشمس آمد روی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون به دل ماه از رخ نیکوی تو است</p></div>
<div class="m2"><p>یا به گل سرو از قد دلجوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم بندی می کنی یا ساحری</p></div>
<div class="m2"><p>چنگل شیر است با آهوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صولجان هر گه به کف گیری ز زلف</p></div>
<div class="m2"><p>دل همی خواهد که گردگوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عارفان در فکر نار ونورتو</p></div>
<div class="m2"><p>صوفیان در ذکرهای و هوی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشقان را از سر شب تا به صبح</p></div>
<div class="m2"><p>قصه ها از طلعت وگیسوی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درکلامم نیست الا وصف تو</p></div>
<div class="m2"><p>درمشامم نیست غیر از بوی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابرو باران خواست هر کس تخم کشت</p></div>
<div class="m2"><p>کشت من شد سبز ز آب جویتو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صدهزاران منزل ار ره طی کنم</p></div>
<div class="m2"><p>باز بینم هستم اندر کوی تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرحبا بر عشق وبرکردار عشق</p></div>
<div class="m2"><p>چون منی راکرده هم زانوی تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بردی از دست بلنداقبال دل</p></div>
<div class="m2"><p>آفرین بر قوت بازوی تو</p></div></div>