---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>دلم ازدیدن آن طره طرار می ترسد</p></div>
<div class="m2"><p>بلی هرکس ندارد دل چو بیندمار می ترسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن زاری دلا اندر بر چشم سیاه او</p></div>
<div class="m2"><p>که گر آواز زاری بشنود بیمار می ترسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بیم آن زلف لرزد پیش چشم وابروی جانان</p></div>
<div class="m2"><p>بلی هر کس که باشدرهزن وطرار می ترسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر زلفش زره پوش است واز خنجر نیندیشد</p></div>
<div class="m2"><p>که هر کس بنگری از مست خنجر دار می ترسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیم عاشق از آن گل گر کنم دست طلب کوته</p></div>
<div class="m2"><p>مخوان گلچین هر آنکس را که دست از خار می ترسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا درسوختن از عشق پروانه چوپروانه</p></div>
<div class="m2"><p>نمی باشد خلیل الله کسی کز نار می ترسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلنداقبال حرف حق زن ار ریزند خونت را</p></div>
<div class="m2"><p>نه منصور انا الحق گواست آن کز دار می ترسد</p></div></div>