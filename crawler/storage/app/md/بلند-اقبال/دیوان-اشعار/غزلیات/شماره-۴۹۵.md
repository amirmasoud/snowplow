---
title: >-
    شمارهٔ ۴۹۵
---
# شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>گفتی از چیست که شیرین سخنی</p></div>
<div class="m2"><p>ای صنم بسکه توشیرین دهنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصف لعل شکرین تو مرا</p></div>
<div class="m2"><p>شهره کرده است به شیرین سخنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش زلفت ندهد بوبه مشام</p></div>
<div class="m2"><p>عنبر اشهب ومشک ختنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوبه چشمت نکند فتنه بپا</p></div>
<div class="m2"><p>گوبه زلفت نکند راهزنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که شوداگه اگر شهزاده</p></div>
<div class="m2"><p>که چنین فتنه هر انجمنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردن زلف تو راخواهد زد</p></div>
<div class="m2"><p>بهتر است اینکه خوداورا بزنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ببنیم به چشمت چه کند</p></div>
<div class="m2"><p>مختصر گویمت اندر محنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه چه گفتم که اگر شهزاده</p></div>
<div class="m2"><p>بیندت با همه صاحب فطنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کند چشم تو را تیر انداز</p></div>
<div class="m2"><p>دهدت منصب ضیغم فکنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با همه اهرمنی زلف تو را</p></div>
<div class="m2"><p>می دهد قرب اویس قرنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با همه راهزنی می دهدش</p></div>
<div class="m2"><p>در سپه داری خودمؤتمنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیشت آید چو بلنداقبالت</p></div>
<div class="m2"><p>مکن آن روز به او ما ومنی</p></div></div>