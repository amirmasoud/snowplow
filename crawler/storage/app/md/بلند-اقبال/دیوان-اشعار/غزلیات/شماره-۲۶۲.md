---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>ای تیر مژگان تورا جان ودل مسکین هدف</p></div>
<div class="m2"><p>جان ودل ما عاقبت خواهدشد از عشقت تلف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که مژگان تو را بیند به گردچشم تو</p></div>
<div class="m2"><p>گوید که چنگیز است این گردش سپاهی بسته صف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می گفتمت سروی اگر می بود سیمین ساق او</p></div>
<div class="m2"><p>می خواندمت ماهی نبود ار ماه بر رویش کلف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کس که بیند روی تو انگشت گیرد بردهان</p></div>
<div class="m2"><p>وآنکس که بیند چهر من هی ساید از غم کف به کف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانا به جانت آفرین بادا ز رب العالمین</p></div>
<div class="m2"><p>همچون تویکتای گوهری پرورده هرگز کی صدف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پوست پوشان توام وز حلقه گوشان توام</p></div>
<div class="m2"><p>گه درخروشم گه خموشافتاده درچنگت چودف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را وجود از هست تو آشفته ایم ومست تو</p></div>
<div class="m2"><p>ای ماچو ذره توچومهر ای توچوقلزم ما چوکف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر برآید دلبرم و آسوده گردد خاطرم</p></div>
<div class="m2"><p>گر کوکب اقبال من از نکبت آید درشرف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از چشم وگیسویت رها گردد بلند اقبال کی</p></div>
<div class="m2"><p>اینش کشد از یک جهت آنش کشد از یک طرف</p></div></div>