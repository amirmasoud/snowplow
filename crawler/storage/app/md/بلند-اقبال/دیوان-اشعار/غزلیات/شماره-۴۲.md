---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>چشم من خواب ندارد به شب وخونبار است</p></div>
<div class="m2"><p>بلکه همسایه هم از ناله من بیدار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز به دیوار نگویم غم دل پیش کسی</p></div>
<div class="m2"><p>کسی ار باز بود محرم دل دیوار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته بودم که بگویم به تو درد دل خویش</p></div>
<div class="m2"><p>نتوان گفت که درد دل من بسیار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز دلدار رسد درد به از درمان است</p></div>
<div class="m2"><p>وگر از یار بود نار به از گلزار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماه را چون توکجا قامت همچون سرواست</p></div>
<div class="m2"><p>سرو را همچو توکی طره عنبر بار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه چوچشم تودراین شهر دگر قصاب است</p></div>
<div class="m2"><p>نه ز زلف تودراین ملک دگر عطار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سروی الحق نه چوبالای تو در کشمیر است</p></div>
<div class="m2"><p>مشکی انصاف نه چون زلف تودر تاتار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست بر دل حرج از دست تو گرناله کند</p></div>
<div class="m2"><p>که هم ازعشق تو دیوانه وهم بیمار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه از دولت عشق است بلنداقبالم</p></div>
<div class="m2"><p>لیک این منصب وعزت همه از دلدار است</p></div></div>