---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>پرده را از روی خود انداخت یار</p></div>
<div class="m2"><p>وزنگاهی کار ما را ساخت یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود شب تاریک ومن گم کرده راه</p></div>
<div class="m2"><p>دستگیرم شد مرا بشناخت یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل چو سیم و خودچو زر بیغش شدم</p></div>
<div class="m2"><p>بسکه درکوره غمم بگداخت یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست دل را غیر تسلیم و رضا</p></div>
<div class="m2"><p>بر سر دل هر چه آرد تاخت یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواست دلگرمم به سر بازی کند</p></div>
<div class="m2"><p>ورنه کی این نرد را می باخت یار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقیا بردار و در پیمانه کن</p></div>
<div class="m2"><p>این بهی کاندر قدح انداخت یار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با همه پستی بلند اقبال شد</p></div>
<div class="m2"><p>چون به حال زار دل پرداخت یار</p></div></div>