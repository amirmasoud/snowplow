---
title: >-
    شمارهٔ ۳۶۱
---
# شمارهٔ ۳۶۱

<div class="b" id="bn1"><div class="m1"><p>روز است وآفتاب است شمع و چراغ جوئیم</p></div>
<div class="m2"><p>دلبر بر دل ماست واز اوسراغ جوئیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافل که خضر بر خلق از حق دلیل راه است</p></div>
<div class="m2"><p>ما گمرهان دلیل راه از کلاغ جوئیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرگس بنفشه سنبل نسرین ولاله وگل</p></div>
<div class="m2"><p>در پیش دلبر وما از صحن باغ جوئیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باشدعلاج هر درد از شربت لب دوست</p></div>
<div class="m2"><p>ما دردخویشتن را درمان ز داغ جوئیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درروزگار نبودجز نامی از فراغت</p></div>
<div class="m2"><p>ما رانگر که دایم از وی فراغ جوئیم</p></div></div>