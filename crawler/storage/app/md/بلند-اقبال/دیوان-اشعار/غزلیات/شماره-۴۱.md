---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>به پیش آتش چهر توزلف تو دود است</p></div>
<div class="m2"><p>ز دود توست مرا دیده گریه آلود است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو رابه معرکه حاجت به خود وجوشن نیست</p></div>
<div class="m2"><p>که موی بر سر ودوش تو جوشن وخود است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسم به وصل توهر چند زودتر دیر است</p></div>
<div class="m2"><p>رهم ز هجر تو هر چند دیرتر زود است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر مرض ز تو باشد مراست به ز علاج</p></div>
<div class="m2"><p>وگر زیان ز توآید مرا به از سود است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دردهجر عجب خشک گشته کام ولبم</p></div>
<div class="m2"><p>عجب تر اینکه کنارم ز اشک چون روداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا به عشق توجشنی است باده خون جگر</p></div>
<div class="m2"><p>پیاله کاسه چشمم دل از فغان رود است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علاج درد فراق تورا به صبر کنند</p></div>
<div class="m2"><p>ولی به پیش فراق تو صبر نابود است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه عهد شکستی ولی بلند اقبال</p></div>
<div class="m2"><p>هنوز از تورضامند وبازخشنود است</p></div></div>