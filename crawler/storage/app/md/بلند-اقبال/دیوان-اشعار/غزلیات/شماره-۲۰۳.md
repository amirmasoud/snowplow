---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>جای من وتوزهدا کی به بهشت می شود</p></div>
<div class="m2"><p>خاک من وتو عاقبت کوزه وخشت می شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قهر خدا ولطف اوهست که جلوه میکند</p></div>
<div class="m2"><p>آن بهتودورخ این به من باغ بهشت می شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بخوریم ما وتو باده ز یک قدح به تو</p></div>
<div class="m2"><p>آتش خرمن و به من شبنم کشت می شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ورنه دو طفل از چه رو از پدری و مادری</p></div>
<div class="m2"><p>خوب یکی شود یکی همچو تو زشت می شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آگهی ار تو ز این سبب شرم مکن به من بگو</p></div>
<div class="m2"><p>بهر چه این پلید و آن پاک سرشت می شود</p></div></div>