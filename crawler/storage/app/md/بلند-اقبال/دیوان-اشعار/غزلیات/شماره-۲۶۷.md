---
title: >-
    شمارهٔ ۲۶۷
---
# شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>ندیده روی دلبر را شدم از جان و دل عاشق</p></div>
<div class="m2"><p>بلی عاشق چنین گردد اگر باشد کسی لایق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هر جانب که آن خورشید تابد آیمش حربا</p></div>
<div class="m2"><p>به هر صورت که گرددجلوه گر گردم بدو عاشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر در کسوت لیلی در آید می شوم مجنون</p></div>
<div class="m2"><p>وگر بر هیئت عذرا برآید گردمش وامق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حسن ودلبری دیگر چو آن ماه پری پیکر</p></div>
<div class="m2"><p>نه دیده نه ببیند کس چه از سابق چه در لاحق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی گنجد دلم در بر طپد چون طایر بی سر</p></div>
<div class="m2"><p>همی در روز وشب از بس به دیدارش بود شایق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چهر و اشک من بنگر که قولم را کنی باور</p></div>
<div class="m2"><p>چو اشک و چهر خونین شد گواه عاشق صادق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببین بر هر تنی باشد سری در هر سری سری</p></div>
<div class="m2"><p>اگر عاقل وگر جاهل اگر عادل وگر فاسق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خبرازنیت کس کس ندارد جز خداوندی</p></div>
<div class="m2"><p>که کردش از عدم موجود وهستش خالق ورازق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن باکس دل آزاری اگر راهی به حق داری</p></div>
<div class="m2"><p>بلنداقبال آسا کار را بگذار با خالق</p></div></div>