---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>آتشی کز هجر یوسف در دل یعقوب بود</p></div>
<div class="m2"><p>در دل خونین من دوش از غم محبوب بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو نوح از اشک چشمم کردطوفانی به پا</p></div>
<div class="m2"><p>بر دل من آفرین کز صبر چون ایوب بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر موسی جلوه گر نوری که شددرکوه طور</p></div>
<div class="m2"><p>پرتوی از عارض آن شوخ شهر آشوب بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو را کردم شبیه قامت رعنای دوست</p></div>
<div class="m2"><p>منفعل گشتم چو دیدم ساق سرو از چوب بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماه راگفتم به روی یار دارد نسبتی</p></div>
<div class="m2"><p>خوب چون دیدم رخ ماه از کلف معیوب بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک من خوب است سر تا پا همین خویش بداست</p></div>
<div class="m2"><p>کاش چون پا تا سر اوخوی اوهم خوب بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کردم اندر مقدم جانان سرو جان را نثار</p></div>
<div class="m2"><p>گفت چیزی دیگر آر این تحفه نامرغوب بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود بس شاعر ولی چون منکسی از عشق دوست</p></div>
<div class="m2"><p>نه بلنداقبال ونه شعرش بدین اسلوب بود</p></div></div>