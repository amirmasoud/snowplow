---
title: >-
    شمارهٔ ۴۲۷
---
# شمارهٔ ۴۲۷

<div class="b" id="bn1"><div class="m1"><p>چرا تواینهمه ای ماه بی وفا شده ای</p></div>
<div class="m2"><p>به دوستان همه بی مهر یا به ما شده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جفاکشی شده ما را شعار ودلشادیم</p></div>
<div class="m2"><p>از آن زمان که به ما مایل جفا شده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جدا شود چو نی از غصه بنداز بندم</p></div>
<div class="m2"><p>ببینم ازبر من گر دمی جدا شده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ملک حسن تو شاهنشهی زهی احسان</p></div>
<div class="m2"><p>که همنشین به من خسته گدا شده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به من به حالت بیگانگان کنی رفتار</p></div>
<div class="m2"><p>ولیک با همه می بینم آشنا شده ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار درد تو را بی دوا شفا بخشد</p></div>
<div class="m2"><p>اگر به درد دل خسته ای دوا شده ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا چه شد که چو من گشته ای بلنداقبال</p></div>
<div class="m2"><p>مگر به حکم قضا و قدر رضا شده ای</p></div></div>