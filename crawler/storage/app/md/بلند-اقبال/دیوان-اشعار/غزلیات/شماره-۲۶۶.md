---
title: >-
    شمارهٔ ۲۶۶
---
# شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>دامن آن ماهم ار افتد به کف</p></div>
<div class="m2"><p>آفتاب بختم آید در شرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خدا زلف تو را ثعبان نمود</p></div>
<div class="m2"><p>شد به من هم امر از او خدلاتخف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سروی از قد لیک سرو سیم ساق</p></div>
<div class="m2"><p>ماهی از رخ لیک ماه بی کلف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیر تیغت افکنم از سر سپر</p></div>
<div class="m2"><p>پیش تیرت آورم ازجان هدف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اشک چشمم گر جهان دریا نشد</p></div>
<div class="m2"><p>دامنم گردیده پر در چون صدف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا دل شب دوش همچون زلف یار</p></div>
<div class="m2"><p>داشتم آشفته حالی وا اسف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم ای ساقی مخسب از جای خیز</p></div>
<div class="m2"><p>کز غم دل روزگارم شد تلف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چه داری در میان جام ریز</p></div>
<div class="m2"><p>ای جم تا دلم آرد شعف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می به من من من ده وبنگر به من</p></div>
<div class="m2"><p>تا شوی آگه ز سر من عرف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت ساقی عشرت دنیا و دین</p></div>
<div class="m2"><p>جوی از او در میان جام ودف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا بلنداقبال گردی درجهان</p></div>
<div class="m2"><p>هم مدد خواه از شهنشاه نجف</p></div></div>