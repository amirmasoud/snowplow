---
title: >-
    شمارهٔ ۴۵۹
---
# شمارهٔ ۴۵۹

<div class="b" id="bn1"><div class="m1"><p>بینم ای ترک عجب خونخواری</p></div>
<div class="m2"><p>خود بگو بار منی یا یاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم ازدست توخون شد به برم</p></div>
<div class="m2"><p>بسکه بد عهد وجفا کرداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشک نارند زتاتار به فارس</p></div>
<div class="m2"><p>که تواز طره دوصدتاتاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بردی ازیک نگه ازمن دل ودین</p></div>
<div class="m2"><p>بوالعجب حیله گر وسحاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیستی از چه نگهدار دلم</p></div>
<div class="m2"><p>آخر ای سنگدل ار دلداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عهدکردی که به مستی دهمت</p></div>
<div class="m2"><p>بوسه ای هر چه کنم هشیاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرسی از حال دلم دل ز کفم</p></div>
<div class="m2"><p>تو ربودی چه عجب عیاری</p></div></div>