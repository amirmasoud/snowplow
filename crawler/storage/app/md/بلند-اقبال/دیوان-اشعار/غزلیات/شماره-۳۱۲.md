---
title: >-
    شمارهٔ ۳۱۲
---
# شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>چنان اندر کمند طره جانان گرفتارم</p></div>
<div class="m2"><p>که امید رهائی ره ندارد در دل زارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عشق مورخط یار ومار زلف دلدارم</p></div>
<div class="m2"><p>تو پنداری به گوشم رفته مور و در بغل مارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر زاهد کند از عشق روی دوست انکارم</p></div>
<div class="m2"><p>چه غم کو را نه انسان از دواب الارض پندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند یارم گر آزارم که دست از عشق بردارم</p></div>
<div class="m2"><p>سرم را گر برد از تن نخواهد بود آزارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه باک اندر ره عشقش اگر سر رفت ودستارم</p></div>
<div class="m2"><p>ندارم دست تا دامان وصلش را به دست آرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو زلف یار تا در گردن او دست در نارم</p></div>
<div class="m2"><p>ز غم درخون دل آغشته همچون دانه درنارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز عشق روی وموی یار صبح وشام درنارم</p></div>
<div class="m2"><p>ز غم می سوزم ومی سازم ودم هیچ درنارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه غم عشق ار نهد بر دل غمی هر دم دگر بارم</p></div>
<div class="m2"><p>دل من بختی مست است وپروا نیست از بارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نباشد چون بلند اقبال سال ومه جز این کارم</p></div>
<div class="m2"><p>که تخم عشق جانان را به مهر آباد دل کارم</p></div></div>