---
title: >-
    شمارهٔ ۴۱۳
---
# شمارهٔ ۴۱۳

<div class="b" id="bn1"><div class="m1"><p>ز مو قیمت مشک وعنبر شکسته</p></div>
<div class="m2"><p>به لب نرخ قند مکرر شکسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه پرسی دلم ازچه بشکسته در بر</p></div>
<div class="m2"><p>دلم در بر از دست دلبر شکسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بت من به هنگام مستی مکرر</p></div>
<div class="m2"><p>سر ساقیان را به ساغر شکسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر جا که بنشسته وخوره صهبا</p></div>
<div class="m2"><p>بسی دست وپا سینه وسرشکسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دیوار ودرگاهی از شورمستی</p></div>
<div class="m2"><p>زده خنجر آن سان که خنجر شکسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکن شادش آزادی ار خواهی از غم</p></div>
<div class="m2"><p>دلا بینی از غم دلی گر شکسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا گر بلند است اقبال از چه</p></div>
<div class="m2"><p>چنین از غم دلبرم پرشکسته</p></div></div>