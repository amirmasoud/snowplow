---
title: >-
    شمارهٔ ۳۵۱
---
# شمارهٔ ۳۵۱

<div class="b" id="bn1"><div class="m1"><p>در تصور ما که ما بینای روی دوستیم</p></div>
<div class="m2"><p>دوست پیش ما وما درجستجوی دوستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پریشان حالی است این حالت ونبود شگفت</p></div>
<div class="m2"><p>و این چنین آشفته دل وآشفته موی دوستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برمشام ما نگردد بوی گلها کارگر</p></div>
<div class="m2"><p>زآنکه دایم در زکام از عطر بوی دوستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوست چون خورشید رخشان است وما حربا صفت</p></div>
<div class="m2"><p>رو به هر سو کونماید روبه سوی دوستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک دل کردیم وترک جان وترک آرزو</p></div>
<div class="m2"><p>زآنکه هر گام وهرره کامجوی دوستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چمن زنگاری از باران شود چون خط یار</p></div>
<div class="m2"><p>ما چنین سرسبز وخرم زآب جوی دوستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبح وظهر ومغربی زاهد کند ذکری وما</p></div>
<div class="m2"><p>سال وماه وروز و شب درگفتگوی دوستیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زآتش دوزخ حکایت کم کن ای واعظ که ما</p></div>
<div class="m2"><p>عمر را پرورده سوزندخوی دوستیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون گدا برمال وهمچون تشنه بر آب زلال</p></div>
<div class="m2"><p>در خیال یار واندر آرزوی دوستیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باده ای ساقی مکن درجام بهر ما که ما</p></div>
<div class="m2"><p>مست وبیخوداز می خم وسبوی دوستیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون بلنداقبال برافلاکشد هیهای ما</p></div>
<div class="m2"><p>بسکه روز وشب همی درهای وهوی دوستیم</p></div></div>