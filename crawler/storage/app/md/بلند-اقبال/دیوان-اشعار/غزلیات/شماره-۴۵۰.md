---
title: >-
    شمارهٔ ۴۵۰
---
# شمارهٔ ۴۵۰

<div class="b" id="bn1"><div class="m1"><p>دیوانه ام ای شوخ پری وار توکردی</p></div>
<div class="m2"><p>آشفته ام از طره طرار توکردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نقطه قطب وسط دایره بودم</p></div>
<div class="m2"><p>سرگشته ام از عشق چو پرکار تو کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غارتگر دل ای مه طرار توگشتی</p></div>
<div class="m2"><p>تاراج خرد ای بت عیار توکردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و آن مصحف وتسبیح مرا ای بت ترسا</p></div>
<div class="m2"><p>از زلف ورخ خودبت وزنار تو کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طالع چه خطا کرد وکواکب چه گنه داشت</p></div>
<div class="m2"><p>تهمت به که بندیم همه کار توکردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون خواستی آزار دهدخار به گلچین</p></div>
<div class="m2"><p>گل ساختی وهمدم اوخار تو کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمرود که می بود که آتش کندودود</p></div>
<div class="m2"><p>از بهر خلیل آتش وگلزار توکردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از حق مگذر خود بده انصاف مگر نه</p></div>
<div class="m2"><p>منصورکه شد بر زبر دار توکردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اقبال من از عشق بلند است هم این را</p></div>
<div class="m2"><p>از عشق رخ خویشتن ای یار توکردی</p></div></div>