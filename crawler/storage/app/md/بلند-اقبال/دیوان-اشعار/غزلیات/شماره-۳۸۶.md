---
title: >-
    شمارهٔ ۳۸۶
---
# شمارهٔ ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>گفتمش چون بینمت ای نازنین</p></div>
<div class="m2"><p>گفت رودر آینه خود در ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش اندر کجا جویم تو را</p></div>
<div class="m2"><p>گفت در دلهای محزون غمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش گویند هستی لامکان</p></div>
<div class="m2"><p>گفت اندر هر مکان هستم مکین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش ره کو که آیم سوی تو</p></div>
<div class="m2"><p>گفت راه است از یسار واز یمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش دادم به عشقت جان ودل</p></div>
<div class="m2"><p>گفت شرط دوستی باشد همین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش خواهم پرستیدن تو را</p></div>
<div class="m2"><p>گفت بیرون رو ز فکر کفر ودین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش چون شد بلند اقبال من</p></div>
<div class="m2"><p>گفتم لطف ما به حالت شد قرین</p></div></div>