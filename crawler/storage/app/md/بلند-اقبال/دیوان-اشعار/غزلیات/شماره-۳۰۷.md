---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>از چهر آتشین تو آتش به جان شدم</p></div>
<div class="m2"><p>آتش کند چه سان به نیستان چنان شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آوخ که اشتم قدی از راستی چو تیر</p></div>
<div class="m2"><p>ز ابروی چون هلال تو خم چون کمان شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که نور روی تو گردد چراغ من</p></div>
<div class="m2"><p>در پیش ماهتاب تو تارکتان شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون حالت دهان تو گشتم خوشم ازین</p></div>
<div class="m2"><p>کز بی نشان دهان تو من بی نشان شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردم شب فراق تو را صبح وزنده ام</p></div>
<div class="m2"><p>در ملک عشق خسرو صاحبقران شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیگر مرا به دل نبود هیچ آرزو</p></div>
<div class="m2"><p>از دولت وصال تو چون کامران شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بودم زهجر خسته و پیر و نزار و زار</p></div>
<div class="m2"><p>نبود عجب ز وصل تو کز سر جوان شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ اطلاعی از الف و با نداشتم</p></div>
<div class="m2"><p>کردی تو تربیت که ادیب جهان شدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در روزگار نام ونشانی ز من نبود</p></div>
<div class="m2"><p>از عشق یار صاحب نام ونشان شدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی جان ترم ز سایه و بی سایه تر ز جان</p></div>
<div class="m2"><p>از بس چو سایه در پی جانان روان شدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا بت پرستیم شده مذهب به عشق دوست</p></div>
<div class="m2"><p>فارغ ز کفر و دین وز سود و زیان شدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پرسیدم از زحل ز چه رفعت گرفته ای</p></div>
<div class="m2"><p>گفتا به کوی یار تو چون پاسبان شدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اقبال من ز عشق تو اول بلند بود</p></div>
<div class="m2"><p>آخر ز هجر روی تو پست و نوان شدم</p></div></div>