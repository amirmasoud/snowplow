---
title: >-
    شمارهٔ  ۳۲
---
# شمارهٔ  ۳۲

<div class="b" id="bn1"><div class="m1"><p>دل گشته در برم خون از طبع زودرنجت</p></div>
<div class="m2"><p>افتاده درشکنجم از زلف پر شکنجت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خالت اوفتادم در ششدر غم و رنج</p></div>
<div class="m2"><p>تا بر سرم چه آید از نقش چار وپنجت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی تو گنج حسن است مویت سیاه ماری</p></div>
<div class="m2"><p>کان مار پاسبان است دایم به روی گنجت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با خویش گفته بودم ندهم دگر به کس دل</p></div>
<div class="m2"><p>چون ضبط دل توان کرد با عشوه ها و غنجت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موی بلند اقبال از غم سفید گردید</p></div>
<div class="m2"><p>دور از خط چو زنگار وز نقطه سرنجت</p></div></div>