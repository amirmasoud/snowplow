---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>گهی که طره طرار او به تاب رود</p></div>
<div class="m2"><p>ز دست من دل واز دل قرار و تاب رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفت کز ره مهر آیمت شبی درخواب</p></div>
<div class="m2"><p>بگفتمش که کجا چشم من به خواب رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگفتمی به منجم کی است وقت خسوف</p></div>
<div class="m2"><p>بگفت آن بت مه رو چودرنقاب رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبیه زلف ورخ یار آیدم به نظر</p></div>
<div class="m2"><p>به برج عقرب ومیزان چوآفتاب رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر که گردش چشم توام دهد مستی</p></div>
<div class="m2"><p>وگرنه پیش لبت نشئه از شراب رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب مدار شود شهر اگر خراب از سیل</p></div>
<div class="m2"><p>همی ز چشمه چشمم ز بسکه آب رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رواست نالد اگر روز وشب بلند اقبال</p></div>
<div class="m2"><p>ز بس که از تو به او جور بی حساب رود</p></div></div>