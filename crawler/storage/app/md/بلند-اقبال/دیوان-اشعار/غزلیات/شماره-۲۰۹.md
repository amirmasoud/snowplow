---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>دیشب خیال چشم تومستم چنان نمود</p></div>
<div class="m2"><p>کز من قرار وطاقت وهوش و خرد ربود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر تا به پا ز آتش عشق تو تا به صبح</p></div>
<div class="m2"><p>می سوختم چوشمعی وپیدا نبود دود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمم هر آنچه خون ز دلم می نمودکم</p></div>
<div class="m2"><p>عشق رخ تو خون به دلم باز می فزود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می بود خون دیده روان ازکنار من</p></div>
<div class="m2"><p>مانند آب سیل که گرددروان به رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستحکم است رشته الفت هزار شکر</p></div>
<div class="m2"><p>گر بگسلانم غم هجر تو تار وپود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون من به عاشقی چو تو درحسن و دلبری</p></div>
<div class="m2"><p>نه چشم کس بدیده ونه گوش کس شنود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاید که همچو من شود اقبال او بلند</p></div>
<div class="m2"><p>هر کس که زنگ آرزو از لوح دل زدود</p></div></div>