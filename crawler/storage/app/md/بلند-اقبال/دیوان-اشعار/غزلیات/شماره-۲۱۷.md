---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>از آن زمان که تو را شیوه دلبری گردید</p></div>
<div class="m2"><p>قسم به جان تودل از برم بری گردید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر به قد تودل داشت الفتی زازل</p></div>
<div class="m2"><p>که همچو قدتوشکلش صنوبری گردید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه حکمت است که هر کس بدید چشم تورا</p></div>
<div class="m2"><p>ز دردعشق تو بیمار بستری گردید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر که زلف تو داود نیست پس از چیست</p></div>
<div class="m2"><p>که شغل اوگه و بیگه زره گری گردید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار شکر که ما راستم کشی است شعار</p></div>
<div class="m2"><p>شعار تو به جهان چون ستمگری گردید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو آن مهی که چوبرداشتی ز چهره نقاب</p></div>
<div class="m2"><p>خجل به پیش رخت مهر خاوری گردید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه مشتری به رختشد همی بلنداقبال</p></div>
<div class="m2"><p>ستاره روی تورا دید ومشتری گردید</p></div></div>