---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>ز غم هر دم دل خون دیده ام خونبارتر گردد</p></div>
<div class="m2"><p>چوخونم کم شود از دل غمم بسیار تر گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم چون چشم بیمار تو بیمار است و هر ساعت</p></div>
<div class="m2"><p>که بیندچشم بیمار تو رابیمارتر گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر خورشید رخشانی که چشمم چون جمالت را</p></div>
<div class="m2"><p>ببیندتار گردد چون نبیند تارتر گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می آرد مستی اندر بردن دلچشم مست تو</p></div>
<div class="m2"><p>ز می چندانکه گردد مست تر هشیار تر گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا در عشق اگر پرمایه ای افتادگی بگزین</p></div>
<div class="m2"><p>درخت افکنده سرتر گردد ار پربارتر گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز من دلجوئی افزون تر کند چشم تو در مستی</p></div>
<div class="m2"><p>بلی چون ترک مست ازمی شود خونخوارتر گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دیدار جمال خودعزیز وسرفرازش کن</p></div>
<div class="m2"><p>بلند اقبال رامپسند کز این خوارتر گردد</p></div></div>