---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>کافرم خوانند چون عاشق بدان دلبر شدم</p></div>
<div class="m2"><p>کفر اگر این است دلشادم که من کافر شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست غم زاهد اگر گوید که من درظلمتم</p></div>
<div class="m2"><p>کو چنان چشمی که تا بیند مه انور شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلبر من دل نبرد از من چو دیدم روی او</p></div>
<div class="m2"><p>خود گرفتم دل به دست وخود برش دل بر شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی شود ممکن مرا پروا ز گلزار وصال</p></div>
<div class="m2"><p>من که در هجرش بتر از طایر بی پر شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر خلیل الله اندر آتش افتاد و نسوخت</p></div>
<div class="m2"><p>من به هجران عمر را پرورده آذر شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فاش گویم مر مرا دلبر پرستی مذهب است</p></div>
<div class="m2"><p>گرمسلمان خوانیم یا گوئی از دین در شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داشت روئی حیدر آسا ابروئی چون ذوالفقار</p></div>
<div class="m2"><p>شد بلنداقبال یارم بر درش قنبر شدم</p></div></div>