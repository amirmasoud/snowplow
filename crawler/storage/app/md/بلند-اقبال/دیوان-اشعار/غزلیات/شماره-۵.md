---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>توبرانی اگر ز درما را</p></div>
<div class="m2"><p>نیست ره بر در دگر ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکری کز تونیست باشد زهر</p></div>
<div class="m2"><p>زهر تو هست چون شکرما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازگدایان خاکسار توایم</p></div>
<div class="m2"><p>پادشاهی دهی اگر ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>«گر توانی دلی به دست آور»</p></div>
<div class="m2"><p>مکن اینقدر خون جگر ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاره ای کن که تیر مژگانت</p></div>
<div class="m2"><p>شده در سینه کارگر ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یادی ازما نمی کند گاهی</p></div>
<div class="m2"><p>کرده محو از نظر مگر ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاجتی نیستش به سردابه</p></div>
<div class="m2"><p>آنکه گاهی ندیده گرما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که را داده درد دل دلدار</p></div>
<div class="m2"><p>درد دل داده دادگر ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسکه افغان کشد بلند اقبال</p></div>
<div class="m2"><p>از غم هجر کرده کر ما را</p></div></div>