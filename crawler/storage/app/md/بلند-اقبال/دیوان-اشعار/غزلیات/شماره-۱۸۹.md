---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>من نه آن مستم که باک از شحنه وشاهم بود</p></div>
<div class="m2"><p>داده شه فرمان به هر کاری که دلخواهم بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من به بزم شاه خوردم می خود آگاه است شاه</p></div>
<div class="m2"><p>اسم شب دانم چه غم گر شحنه در راهم بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من به راه عشق خواهم رفت تا در کوی دوست</p></div>
<div class="m2"><p>گر به هر گامی که بردارم دو صدچاهم بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست غم هست ار شب تاریک وراه سنگلاخ</p></div>
<div class="m2"><p>چون خیال زلف وروی او شب ماهم بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد از می خوردنم اندیشه از دوزخ مده</p></div>
<div class="m2"><p>هفت دوزخ یک شرار از شعله آهم بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه شیرین است عشقش از غم شیرویه سان</p></div>
<div class="m2"><p>همچودارا زخم خنجر برجگر گاهم بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در فلک گویدملک با زهره گر خوانی دگر</p></div>
<div class="m2"><p>غیر اشعار بلند اقبال اکراهم بود</p></div></div>