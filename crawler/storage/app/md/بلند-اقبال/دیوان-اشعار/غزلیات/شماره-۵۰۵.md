---
title: >-
    شمارهٔ ۵۰۵
---
# شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>اگر در آینه روزی جمال خویش ببینی</p></div>
<div class="m2"><p>زحال من شوی آگه به روز من بنشینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهی به صنعت باری زماه فرق نداری</p></div>
<div class="m2"><p>جز این که اومه گردون بودتوماه زمینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه آفتاب ونه نوری و نه پری و نه حوری</p></div>
<div class="m2"><p>نه آدمی نه فرشته هم آن تمام وهم اینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانمت به چه مانی که جان وجان جهانی</p></div>
<div class="m2"><p>نگیری ار به خطایم نگارخانه چینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو پا به عشق نهادم دلی به دست تودادم</p></div>
<div class="m2"><p>بگوکجاست چه شد کو تو را که گفت امینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر که ناصر دین شه شود ز حال تو آگه</p></div>
<div class="m2"><p>که خصم دولت وجانی وشورملت ودینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کند سیاست وگوید که فتنه ای توبه ملکم</p></div>
<div class="m2"><p>بگو جواب چه گویی چو گویدت که چنینی</p></div></div>