---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>خواست از دولب او یک بوس</p></div>
<div class="m2"><p>زیر لب خنده زد و گفت افسوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بسا جان که رودبر کف دست</p></div>
<div class="m2"><p>می کنی جانی اگر قیمت بوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای سیه زلف توچون پر غراب</p></div>
<div class="m2"><p>وی لب سرخ توچون چشم خروس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست پیدا ونهان عشق رخت</p></div>
<div class="m2"><p>دل دلم شعله صفت در فانوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف بر روی توهندوئی است</p></div>
<div class="m2"><p>کایستاده است به پیش شه روس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا سر زلف چوزنار تو دید</p></div>
<div class="m2"><p>گشت نالان دل من چون ناقوس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر از هجر بلنداقبالت</p></div>
<div class="m2"><p>مردوگردید ز وصلت مأیوس</p></div></div>