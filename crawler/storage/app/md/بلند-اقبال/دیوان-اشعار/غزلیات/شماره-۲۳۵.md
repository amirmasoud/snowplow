---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>نصیب جان ودلم شد بلای هجرت باز</p></div>
<div class="m2"><p>به وصل خود در دولت به رویمن کن باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه روز رفته نه عمر گذشته باز آید</p></div>
<div class="m2"><p>توباز آکه ببینم هر دوآمد باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلیم سان ید بیضا عیان نما از رخ</p></div>
<div class="m2"><p>مسیح وش به تن مرده جان ده از اعجاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چنگ زلف تو دارد خبر ز حال دلم</p></div>
<div class="m2"><p>کبوتری که شودصید چنگل شهباز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آنچه عشق تورا میکنم به دل پنهان</p></div>
<div class="m2"><p>سرشک سرخ و رخ زرد می شودغماز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به راه عشق توپویم مرا بودتا جان</p></div>
<div class="m2"><p>چه غم زراهزن ودوری ونشیب وفراز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برفت عمر ونیامد به چنگ طره دوست</p></div>
<div class="m2"><p>توعمر کوته ما بین وآرزوی دراز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فغان که درتن من مرغ روح گشته اسیر</p></div>
<div class="m2"><p>خوشا دمی که دهندش از این قفس پرواز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بکن نوازشی از لطف بر بلند اقبال</p></div>
<div class="m2"><p>به شکر آنکه ز خوبان عالمی ممتاز</p></div></div>