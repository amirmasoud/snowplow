---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>عاشق رویتو راکاری به کفر ودین نباشد</p></div>
<div class="m2"><p>رهرو کوی تو را راهی به آن واین نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو بهخسرو شکر ار شیرین نباشد نیست شکر</p></div>
<div class="m2"><p>گر چه شکر هست شکر لب ولی شیرین نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچوقدنازنینت سروی اندر باغ نبود</p></div>
<div class="m2"><p>همچوزلف پر زچینت مشکی اندرچین نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود گرفتم سرو دارد همچو بالای توقدی</p></div>
<div class="m2"><p>لیکن اورا زلف مشکین وبر سیمین نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر کسی بیند لب وقد ورخت را دیگر اورا</p></div>
<div class="m2"><p>حاجتی برکوثر وطوبی وحور العین نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند می گوئی صبوری پیشه کن غمگین مکن دل</p></div>
<div class="m2"><p>نیست دل آن دل که عاشق باشد و غمگین نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر کسی درعاشقی خواهدبلند اقبال گردد</p></div>
<div class="m2"><p>بایدش در پیش جانان شیوه جز تمکین نباشد</p></div></div>