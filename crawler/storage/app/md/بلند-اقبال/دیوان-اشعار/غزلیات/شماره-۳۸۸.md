---
title: >-
    شمارهٔ ۳۸۸
---
# شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>هر صفائی که دارد آن مه رو</p></div>
<div class="m2"><p>هست من را چون هستیم هست او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه همرنگ یار نیست بلی</p></div>
<div class="m2"><p>نیست عشقش به غیر رنگ وبو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من وآن شوخ هردو خم داریم</p></div>
<div class="m2"><p>من قد از غم و آن صنم ابرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دو آشفته و پریشانیم</p></div>
<div class="m2"><p>من زمسکین دل او زمشکین مو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من ودلدار هر دو می شکنیم</p></div>
<div class="m2"><p>من ز می تو به او رخ گیسو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من و یاریم هر دو آتش باز</p></div>
<div class="m2"><p>من ز آه دل او زسوزان خو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من واو هر دو دلبری داریم</p></div>
<div class="m2"><p>من زطبع روان واو از رو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلبر از چشم و من ز شیرین شعر</p></div>
<div class="m2"><p>هر دوهستیم فتنه وجادو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دو هستیم بس بلند اقبال</p></div>
<div class="m2"><p>یار ازحسن ومن ز عشق او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وه که اوبا من است ومن به سراغ</p></div>
<div class="m2"><p>قمری آسا همی زنم کوکو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من پی جستجوی وغافل از این</p></div>
<div class="m2"><p>که بودجلوه گری وی از هر سو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زندگانی به ذکر دوست بود</p></div>
<div class="m2"><p>ها به هر دم از آن زنم هی هو</p></div></div>