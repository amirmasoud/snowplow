---
title: >-
    شمارهٔ ۲۳۹
---
# شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>داد آن هفته ز بس یار شرابم دو سه روز</p></div>
<div class="m2"><p>کرد چون نرگس بیمار خرابم دوسه روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز آن ماه گراید به وثاقم دو سه شب</p></div>
<div class="m2"><p>سر بباید که زهر کار بتابم دو سه روز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور از آن ماه دوهفته که زدآتش به دلم</p></div>
<div class="m2"><p>غرقه ازگریه بسیار درآبم دوسه روز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا مگر او به عیادت گذر آرد به سرم</p></div>
<div class="m2"><p>به که چون مردم بیمار بخوابم دو سه روز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هفته ای رفته کز آن ماه ندارم خبری</p></div>
<div class="m2"><p>روم اندر پی دلدار شتابم دوسه روز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می توان گفت جوان بخت وبلنداقبالم</p></div>
<div class="m2"><p>بر در یار اگر بار بیابم دو سه روز</p></div></div>