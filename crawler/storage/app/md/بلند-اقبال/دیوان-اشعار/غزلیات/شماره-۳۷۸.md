---
title: >-
    شمارهٔ ۳۷۸
---
# شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>باز افتاده به یاد رخ جانان دل من</p></div>
<div class="m2"><p>که چو نی می کشد امشب همی افغان دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر اندیشه ز دوزخ ننماید دل من</p></div>
<div class="m2"><p>گوید ار قصه ای از غصه هجران دل من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد چودور از لب و دندان من آن تنگ دهان</p></div>
<div class="m2"><p>تنگ چون دیده موری شده از آن دل من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش محراب دوابروی وی آوردنماز</p></div>
<div class="m2"><p>شد ز کافر بچه ای تازه مسلمان دل من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل من زلف تو را کرده پریشان ای دوست</p></div>
<div class="m2"><p>یا که از زلف تو گردیده پریشان دل من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو بالائی وگل چهره و سنبل گیسو</p></div>
<div class="m2"><p>با وجود تو ندارد سر بستان دل من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم مست تو شنیدم بودش میل کباب</p></div>
<div class="m2"><p>کاین چنین ز آتش عشقت شده بریان دل من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسته گشتم ز سراغ دل گمگشته خود</p></div>
<div class="m2"><p>شده از عشق توچون بی سروسامان دل من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یوسف آسا به زنخدان وخم طره تو</p></div>
<div class="m2"><p>گاه درچاه وگه افتاده به زندان دل من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرد بدبختی وسختی که نشد خون زغمت</p></div>
<div class="m2"><p>اینک ازکرده خودگشته پشیمان دل من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکه بشنید مرا گفت بلنداقبال است</p></div>
<div class="m2"><p>چون رسید از لب لعب تو به درمان دل من</p></div></div>