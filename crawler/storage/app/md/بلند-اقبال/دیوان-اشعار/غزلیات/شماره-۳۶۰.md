---
title: >-
    شمارهٔ ۳۶۰
---
# شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>به که ما دیوانه از عشق پری روئی شویم</p></div>
<div class="m2"><p>تا مگر زنجیری زنجیر گیسوئی شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوزخ سوزان چو از بهر گنهکاران بود</p></div>
<div class="m2"><p>همنشین باید به ترک آتشین خوئی شویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما که نتوانیم جان در بردن از چنگال مرگ</p></div>
<div class="m2"><p>کشته آن خوشتر که ازشمشیر ابروئی شویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاک زد بر پهلوی ما خنجر مژگان یار</p></div>
<div class="m2"><p>چاره جو ز آن لعل لب از نوشداروئی شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یارمیل بازی چوگان وگودارد دلا</p></div>
<div class="m2"><p>پیش چوگان سر زلفش بیا گوئی شویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر شود دریا پر از می کی دهدما را کفاف</p></div>
<div class="m2"><p>لیک مست از باده عشق تواز بوئی شویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاید از آشفتگی ما را شود حاصل نجات</p></div>
<div class="m2"><p>چون بلنداقبال اگر آشفته از موئی شویم</p></div></div>