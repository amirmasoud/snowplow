---
title: >-
    شمارهٔ ۳۶۵
---
# شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>چه بد آغاز وسرانجام چه خواهدبودن</p></div>
<div class="m2"><p>ثمر صبح اثر شام چه خواهد بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودم اول به کجا میروم آخر به کجا</p></div>
<div class="m2"><p>ماحصل از من گمنام چه خواهدبودن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان به تن نالدم آری به جز از این کاری</p></div>
<div class="m2"><p>مرغ را در قفس و دام چه خواهد بودن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا خیز وبده باده گلگون که به غم</p></div>
<div class="m2"><p>چاره غیر از می گلفام چه خواهد بودن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام می آر به گردش که ندانست کسی</p></div>
<div class="m2"><p>غرض از گردش ایام چه خواهد بودن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر معشوق چو کاری نرود پیش به عجز</p></div>
<div class="m2"><p>کار عاشق به جز ابرام چه خواهد بودن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باده پیش آر ومکش رنج که داند پس از این</p></div>
<div class="m2"><p>حالت زهره وبهرام چه خواهدبودن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می بکن نوش و مده گوش به پند زاهد</p></div>
<div class="m2"><p>معنی گفته هر عام چه خواهد بودن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون من از دولت عشق آنکه بلنداقبال است</p></div>
<div class="m2"><p>فکر او غیر می وجام چه خواهد بودن</p></div></div>