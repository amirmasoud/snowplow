---
title: >-
    شمارهٔ ۳۳۲
---
# شمارهٔ ۳۳۲

<div class="b" id="bn1"><div class="m1"><p>گفتمش دیوانه ام گفتا که زنجیرت کنم</p></div>
<div class="m2"><p>گفتمش ویرانه ام گفتا که تعمیرت کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم ازعشق توچون زلف توپرچین شد رخم</p></div>
<div class="m2"><p>گفت درعهدجوانی خواستم پیرت کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش تا چندباشم تشنه دیدار تو</p></div>
<div class="m2"><p>گفت زآب کوثر لب غم مخور سیرت کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم ازخیل سگان درگهت خواهم شوم</p></div>
<div class="m2"><p>گفت اگرگردی سگ این آستان شیرت کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش داری ز مژگان تیر وا ز ابروکمان</p></div>
<div class="m2"><p>گفت هستم درخیال اینکه نخجیرت کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم اندر پای تو خواهم کهتا میرم ز عشق</p></div>
<div class="m2"><p>گفت تا میری در اقلیم بقا میرت کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بلنداقبال گفتم هر زمانم حالتی است</p></div>
<div class="m2"><p>گفت میخواهم که تا آگه زتقدیرت کنم</p></div></div>