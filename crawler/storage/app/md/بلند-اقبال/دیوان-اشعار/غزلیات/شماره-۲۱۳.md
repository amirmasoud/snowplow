---
title: >-
    شمارهٔ ۲۱۳
---
# شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>دل ازتو بر نگیرم تا جان ز تن برآید</p></div>
<div class="m2"><p>دست از تو بر ندارم تا عمر من سر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودی چو ماه اگر ماه چون توشدی زره پوش</p></div>
<div class="m2"><p>بودی چو سرواگر سروچون توسمن برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چین زلف تو چین هر تار اوست تاتار</p></div>
<div class="m2"><p>باشد خطا به شیراز مشک از ختن گر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیشب ز طره ات گفت دل قصه درازی</p></div>
<div class="m2"><p>چشمت ولی به چشمم زو راهزن تر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو چمن چو من سر بنهد به پیش پایت</p></div>
<div class="m2"><p>گر سروقامت تو اندر چمن درآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برچهر آتشینت آن دل که نیست عاشق</p></div>
<div class="m2"><p>درسوختن بباید همچون سمندر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاید بلند اقبال گرددکسی زعشقت</p></div>
<div class="m2"><p>لیکن گمان مفرما چون من سخنور آید</p></div></div>