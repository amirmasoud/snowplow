---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>تیر مژگان تو ما را برجگر بنشسته است</p></div>
<div class="m2"><p>مرحبا ز ایندست وبازو تا به پر بنشسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکجا سروی بودقمری نشیند بر سرش</p></div>
<div class="m2"><p>بر سر سرو قدت بینم قمر بنشسته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر لب لعل توهرکس دید خالت را بگفت</p></div>
<div class="m2"><p>کاین مگس باشد که بر شیرین شکر بنشسته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیک من گویم که بر شیرین لب توخال تو</p></div>
<div class="m2"><p>خسرو پرویز گویا با شکر بنشسته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف رقاصت مسلم گشته در بازیگری</p></div>
<div class="m2"><p>پای چنبر کرده بر دوشت به سر بنشسته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع هم چون من مگر عاشق به رخسار توشد</p></div>
<div class="m2"><p>کز غمت می سوزد و شب تا سحر بنشسته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون من او را هم بلند اقبال اگر خوانی رواست</p></div>
<div class="m2"><p>هرکه را همچون تو دلداری به بر بنشسته است</p></div></div>