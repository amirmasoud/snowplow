---
title: >-
    شمارهٔ ۱۷۷
---
# شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>هیچ می دانی که هجرانت چه با من می کند</p></div>
<div class="m2"><p>می کندبا من همان کاتش به خرمن می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو آزاد ار ببیند قامت دلجوی تو</p></div>
<div class="m2"><p>بندگی را طوق چون قمری به گردن می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افتد ارچشم مسافر برجمالت عمر را</p></div>
<div class="m2"><p>درهمان جائی که می باشی تومسکن می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاجت تیر و زره نبود تو را در روز رزم</p></div>
<div class="m2"><p>زلف ومژگان تو کار تیر وجوشن می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خویشتن را زلف توچون زاهد وسواس دار</p></div>
<div class="m2"><p>پیش چشم مست توبرچیده دامن می کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درکلیسا گر گذار آرد بت ترسای من</p></div>
<div class="m2"><p>کافرم گر سجده پیش بت برهمن می کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از رخ وزلف وخط وچشم ودهان وقد خویش</p></div>
<div class="m2"><p>هر کجا بنشیند آنجا را چو گلشن می کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می نجنبد از لب چون شکرش خال مگس</p></div>
<div class="m2"><p>هر چه زلفش خویشتن رابادبیزن می کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میکندیغما دل و دین از کف پیر وجوان</p></div>
<div class="m2"><p>نه هراس از مرد ونه اندیشه از زن می کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلبر ما برخلاف رسم اهل روزگار</p></div>
<div class="m2"><p>دوست را محروم واحسان ها به دشمن می کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رستگار آن کس بود ای دل که اندر هر مقام</p></div>
<div class="m2"><p>نه نعم گوید نه لا نه ما و نه من می کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تیشه و بازوی فرهاد ار چه درکار است لیک</p></div>
<div class="m2"><p>بیستون را بیستون شیرین ار من می کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون بنفشه روسیاهی عاقبت بار آورد</p></div>
<div class="m2"><p>هر که خود را ده زبان مانندسوسن می کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر بلنداقبال دنیا همچو چشم سوزن است</p></div>
<div class="m2"><p>بس که خود را تنگدل چون چشم سوزن میکند</p></div></div>