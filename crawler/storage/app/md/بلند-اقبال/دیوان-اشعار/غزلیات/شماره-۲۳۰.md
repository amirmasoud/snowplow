---
title: >-
    شمارهٔ ۲۳۰
---
# شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>هر زمان بر دل ما می کنی آزار دگر</p></div>
<div class="m2"><p>جز دل آزاری ما نیست توراکار دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نتوان داد زآزار تودل را تسکین</p></div>
<div class="m2"><p>نتوانم که دهم دل به دل آزار دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلرخی غنچه لبی سروقدی سنبل موی</p></div>
<div class="m2"><p>با وجود تو ندارم سر گلزار دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست درشهر چو من عاشق دلداده بسی</p></div>
<div class="m2"><p>نیست در دهر ولی همچو تو دلدار دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظری کردی و دین ودلم از کف بردی</p></div>
<div class="m2"><p>کن به حال من بی دین نظری بار دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بده از لعل لبت بوسی وبستان دل وجان</p></div>
<div class="m2"><p>مده این گوهر خود را به خریدار دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد ز بیماری چشم تو دل من بیمار</p></div>
<div class="m2"><p>به جز از چشم توام نیست پرستاردگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرحمت بین که پرستار دلم شد چشمش</p></div>
<div class="m2"><p>گشته بیمار پرستار به بیمار دگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا رود از دلم اندوه به یاد رخ دوست</p></div>
<div class="m2"><p>ساقی از باده بده ساغر سرشار دگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چومن آنکس که وفا دار و بلنداقبال است</p></div>
<div class="m2"><p>نکند روز ز در یار به دربار دگر</p></div></div>