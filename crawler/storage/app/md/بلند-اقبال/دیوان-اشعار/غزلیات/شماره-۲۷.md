---
title: >-
    شمارهٔ  ۲۷
---
# شمارهٔ  ۲۷

<div class="b" id="bn1"><div class="m1"><p>ماهی صفت فرو شده ماهی به زیر آب</p></div>
<div class="m2"><p>ماهی نگشته طالع گاهی به زیر آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اخترشناس گفت به چرخ است ماه کاش</p></div>
<div class="m2"><p>می بود و می نمود نگاهی به زیر آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گیسوی یار در نظرم جلوه میکند</p></div>
<div class="m2"><p>یا درشنا است مار سیاهی به زیر آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردند اندر آب همه ماهیان کباب</p></div>
<div class="m2"><p>گر بر کشم ز سوز دل آهی به زیر آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب است زیر چاه وعجب اینکه لعل دوست</p></div>
<div class="m2"><p>هست آب خضر و دارد چاهی به زیر آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز جسم من که ساکن وآب از سرم گذشت</p></div>
<div class="m2"><p>گاهی ندیده کس پر کاهی به زیر آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باشد بلنداقبال از آه واشک خویش</p></div>
<div class="m2"><p>گاهی به روی آتش وگاهی به زیر آب</p></div></div>