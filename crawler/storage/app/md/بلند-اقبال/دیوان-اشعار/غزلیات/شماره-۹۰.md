---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>غمین مباش که چیزی ز عمر باقی نیست</p></div>
<div class="m2"><p>روا بود خوری ار غم که جام وساقی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا به زاهد این شهر دوستی نبود</p></div>
<div class="m2"><p>که درزمانه چواوکس به بدسیاقی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلاف ساقی ومطرب که درهمه عالم</p></div>
<div class="m2"><p>یکی چواین دوبه خوبی وخوش مذاقی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تمام نائی از آن دلبر حجازی گفت</p></div>
<div class="m2"><p>حکایتی به لبش زآن بت عراقی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمی نمی شود از پیش چشم دل غایب</p></div>
<div class="m2"><p>وصال عاشق ومعشوق بالتلافی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جوروکینه اهل جهان بلنداقبال</p></div>
<div class="m2"><p>غمین مباش که چیزی ز عمر باقی نیست</p></div></div>