---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>به فصل گل می گلگون به جام باید کرد</p></div>
<div class="m2"><p>علاج غم به می لعل فام باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدام چون یکی از نام های باده بود</p></div>
<div class="m2"><p>به جام پس میگلگون مدام باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شراب خواره اگر خون اومباح بود</p></div>
<div class="m2"><p>بگوبه شیخ که پس قتل عام باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر حلال شود خون ما ز حرمت می</p></div>
<div class="m2"><p>پس اجتناب چرا ز این حرام باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر ز باده رود نام و ننگ ما بر باد</p></div>
<div class="m2"><p>به باده ترک چنین ننگ ونام باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تنگدستی اگر نیست وجه می ممکن</p></div>
<div class="m2"><p>ز پیر میکده ناچار وام باید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ما رمیده دل آن غزال وحشی را</p></div>
<div class="m2"><p>به جام باده گلرنگ رام باید کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به باده نفس شقی را اسیر باید ساخت</p></div>
<div class="m2"><p>به می هوی وهوس را لجام باید کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی به یاد لب دوست چون بلنداقبال</p></div>
<div class="m2"><p>به فصل گل می گلگون به جام باید کرد</p></div></div>