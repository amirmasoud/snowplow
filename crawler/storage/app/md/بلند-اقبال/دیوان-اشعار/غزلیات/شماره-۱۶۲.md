---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>به وطن یار سفرکرده ما خوب آمد</p></div>
<div class="m2"><p>یوسفی بودکه اندر بر یعقوب آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقان درطرب آئید که معشوق رسید</p></div>
<div class="m2"><p>طالبان در طلب آئید که مطلوب آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که دل بر سر دل ریخت همی در قدمش</p></div>
<div class="m2"><p>در سرگشته ما سخت لگدکوب آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هجر تونوح صفت کرد به پا طوفانی</p></div>
<div class="m2"><p>دل ما بود که درصبر چو ایوب آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو را با قد تو می نتوان نسبت داد</p></div>
<div class="m2"><p>سیم ساقی چو تو و ساق وی از چوب آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می توان خواند ز شاهان بلند اقبالش</p></div>
<div class="m2"><p>هر که درخیل گدایان تومحسوب آمد</p></div></div>