---
title: >-
    شمارهٔ ۳۴۷
---
# شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>ما عاشقان مست دل از دست داده ایم</p></div>
<div class="m2"><p>از دست رفته ایم وز پا اوفتاده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم ازجهان وهرچه در اوهست بسته ایم</p></div>
<div class="m2"><p>بر روی خویشتن در دولت گشاده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جا که عاشقی است به پیشش نشسته ایم</p></div>
<div class="m2"><p>هر جا که دلبری به برش ایستاده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با درد وغم اگر چه دچاریم خرمیم</p></div>
<div class="m2"><p>هر چندپر ز نقش ونگاریم ساده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه ساکت وخموش چورندان خرقه پوش</p></div>
<div class="m2"><p>گه درخروش وجوش چوخم های باده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را مبین به روزکه درویش مسلکیم</p></div>
<div class="m2"><p>شبها به صدر میکده بین شاهزاده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهدکند ز رفتن میخانه منع ما</p></div>
<div class="m2"><p>گویا نه آگهست کز این خانواده ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امروز ما به میکده ساکن نگشته ایم</p></div>
<div class="m2"><p>روز الست پا و سر آنجا نهاده ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن شه چورخ نمودبه شطرنج عشق او</p></div>
<div class="m2"><p>ما مات مانده در شط رنج و پیاده ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درجمع وخرج عشق رخ خود نگار ما</p></div>
<div class="m2"><p>ما را نوشته باقی اگر چه زیاده ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرگه پی شکار شود شاه ما سوار</p></div>
<div class="m2"><p>اندر رکاب اوسگ سر در قلاده ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اقبال ما ز عشق بلنداست وارجمند</p></div>
<div class="m2"><p>ما را مبین که پست تر از خاک جاده ایم</p></div></div>