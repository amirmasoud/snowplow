---
title: >-
    شمارهٔ ۳۹۰
---
# شمارهٔ ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>ای بت ماه روی مشکین مو</p></div>
<div class="m2"><p>دل به چوگان زلف توچون گو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سنان قامت ای زره گیسو</p></div>
<div class="m2"><p>تیر مژگانی وکمان ابرو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رستم داستان حسنی تو</p></div>
<div class="m2"><p>نه خدایا که گشته ای بر زو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا که روزی نشینیم به کنار</p></div>
<div class="m2"><p>شه کنارم از اشک چشم چوجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز وشب در سراغ سروقدت</p></div>
<div class="m2"><p>قمری آسا همی زنم کوکو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زدم از غم ز بس به زانو دست</p></div>
<div class="m2"><p>پیل پا گشتم وشتر زانو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به وصلت شدم بلنداقبال</p></div>
<div class="m2"><p>شهره شهر گشتم ازهر سو</p></div></div>