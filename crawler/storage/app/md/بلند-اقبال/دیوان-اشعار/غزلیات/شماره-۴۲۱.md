---
title: >-
    شمارهٔ ۴۲۱
---
# شمارهٔ ۴۲۱

<div class="b" id="bn1"><div class="m1"><p>عجب از روح مجسم بدنی ساخته ای</p></div>
<div class="m2"><p>کس نداند به چه تدبیر و فنی ساخته ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آزر از سنگ بت ار ساخت تو خود ای بت من</p></div>
<div class="m2"><p>دلی از آهن واز سیم تنی ساخته ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ ازنقطه موهوم نشان نیست پدید</p></div>
<div class="m2"><p>دل گمان برده که از اودهنی ساخته ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا گرفتار کنی مرغ دلم را در دام</p></div>
<div class="m2"><p>دانه از خال وز گیسو رسنی ساخته ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یوسف از عشق تو خود را به چه اندازد وباز</p></div>
<div class="m2"><p>گر ببیند که چه چاه ذقنی ساخته ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نخواهی دل ما را شکنی از چه به رخ</p></div>
<div class="m2"><p>خم به خم زلف شکن در شکنی ساخته ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر از تبت وچین مشک نیارند به فارس</p></div>
<div class="m2"><p>که تو از زلف ختا وختنی ساخته ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل رخی غنچه لبی سرو قدی نسرین بر</p></div>
<div class="m2"><p>بزم ما را به حقیقت چمنی ساخته ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دارم ازخون جگر باده کباب از دل ریش</p></div>
<div class="m2"><p>شرمسارم که به حال چومنی ساخته ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرده از وصل خود ار یار بلنداقبالت</p></div>
<div class="m2"><p>ای دل ازچیست که بیت الحزنی ساخته ای</p></div></div>