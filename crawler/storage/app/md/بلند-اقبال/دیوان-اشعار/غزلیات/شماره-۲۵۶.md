---
title: >-
    شمارهٔ ۲۵۶
---
# شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>خیز ای نگار باده بیاور پیش</p></div>
<div class="m2"><p>از شاه وشیخ وشحنه مکن تشویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردمرا به باده بکن درمان</p></div>
<div class="m2"><p>مرهم مرا ز باده بنه بر ریش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهد است اگر زجام تو نوشم سم</p></div>
<div class="m2"><p>نوش است گر زدست تو بینم نیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تریاق آید ار توچشانی زهر</p></div>
<div class="m2"><p>جدوار آید ار تو خورانی بیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذشته ام ز عشق رخت از جان</p></div>
<div class="m2"><p>دل کنده ام ز دردغمت از خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کس ز عشق گشته بلند اقبال</p></div>
<div class="m2"><p>یکسان به پیش اوست شه ودرویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ا زعدل شاهزاده بترس ای شوخ</p></div>
<div class="m2"><p>با ما جفا وجور مکن ز این بیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهزاده ای که آمده در عهدش</p></div>
<div class="m2"><p>ا زکوه و دشت گرگ شبان برمیش</p></div></div>