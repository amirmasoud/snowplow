---
title: >-
    شمارهٔ ۳۲۲
---
# شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>دل من هست چو عمان دهان شد صدفم</p></div>
<div class="m2"><p>صدفم پر بود از گوهر وریزد به کفم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم از کار خود وکرده خود یأس ولی</p></div>
<div class="m2"><p>باشد امید نجاتی به دل از لاتخفم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسف دین ودلم تا شده دور از بر من</p></div>
<div class="m2"><p>همچو یعقوب رودتا به فلک وا اسفم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قوه ناطقه ام بخت به انسانی داد</p></div>
<div class="m2"><p>خودچوحیوان شده دایم پی آب وعلفم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طلعتی بود مرا روشن وصافی چوماه</p></div>
<div class="m2"><p>تارتر از شبم از بس به رخ آمد کلفم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم مگر فضل خداوند شود شامل حال</p></div>
<div class="m2"><p>ورنه هر کس نگرم بسته کمر بر تلفم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه تیغی به کف آورده بسازد سپرم</p></div>
<div class="m2"><p>هر که تیری به کمان هشته نماید هدفم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چوبلنداقبالم اندیشه ندارم ز جزا</p></div>
<div class="m2"><p>زآنکه مداح وگدای در شاه نجفم</p></div></div>