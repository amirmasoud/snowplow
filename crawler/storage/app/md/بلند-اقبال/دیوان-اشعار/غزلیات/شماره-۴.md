---
title: >-
    شمارهٔ  ۴
---
# شمارهٔ  ۴

<div class="b" id="bn1"><div class="m1"><p>افزوده غم عشق تو درد وتب ما را</p></div>
<div class="m2"><p>کرده است سیه هجر توروز وشب ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما عاشق ومستیم و به جز یار ندانیم</p></div>
<div class="m2"><p>زاهد ز چه جویاست همی مذهب ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آگه نشد از طالع ما هیچ منجم</p></div>
<div class="m2"><p>می سوختی ای کاش فلک کوکب ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بوسه زدم بر لب شیرین وی ازشهد</p></div>
<div class="m2"><p>تب خاله زد از فرط حرارت لب ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ترک بکن ترک جفا در دل شبها</p></div>
<div class="m2"><p>گوشت نشنیده است مگر یا رب ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی سوی که آریم به غیر از تو که جز تو</p></div>
<div class="m2"><p>کس نیست برآورده کند مطلب ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را بسی اقبال بلند است که دلدار</p></div>
<div class="m2"><p>دربانی خود کرده یقین منصب ما را</p></div></div>