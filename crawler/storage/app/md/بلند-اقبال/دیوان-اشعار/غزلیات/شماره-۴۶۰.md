---
title: >-
    شمارهٔ ۴۶۰
---
# شمارهٔ ۴۶۰

<div class="b" id="bn1"><div class="m1"><p>نگرددماه نوطالع گر از رخ پرده برداری</p></div>
<div class="m2"><p>نروید سرو در بستان به بستان گر گذار آری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توئی آن بت که دین بت پرستی را دهند از کف</p></div>
<div class="m2"><p>اگر بینند رویت را برهمن های فرخاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگر از هند سوی فارس نارد کاروان شکر</p></div>
<div class="m2"><p>تعالی الله ز شیرین لب ز بس قندوشکرباری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه حاجت آنکه مشک آرنداز چین وختن دیگر</p></div>
<div class="m2"><p>تو اندر چین زلف خویش از بس مشک تر داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم عشق تومی باشد گران باری به دوش من</p></div>
<div class="m2"><p>مکن وامانده ام از درد هجر خودز سربازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دامان وصالت کی رسد دست من مسکین</p></div>
<div class="m2"><p>به حال من کند رحمی ز فضل خود مگر باری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلند اقبال شاید گردد آزاد از غم هجرت</p></div>
<div class="m2"><p>کند گردون اگر شفقت کند طالع اگر یاری</p></div></div>