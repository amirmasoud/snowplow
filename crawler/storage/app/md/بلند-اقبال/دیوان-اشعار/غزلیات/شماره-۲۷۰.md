---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>فغان که دل به برم خون شد ازجفای فراق</p></div>
<div class="m2"><p>خدا به داد دل من دهد سزای فراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روای طبیب مده درد سر مرا وبه خویش</p></div>
<div class="m2"><p>که غیر مرگ نباشد دگر دوای فراق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراق را نمک ما دودیده کور کند</p></div>
<div class="m2"><p>که تا یکی کشد از هر طرف عصای فراق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر فراق بمیرد ز دیده خون باریم</p></div>
<div class="m2"><p>من از برای دل خود دل از برای فراق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بقای هیچ کسی نیست در فنای کسی</p></div>
<div class="m2"><p>ولی بقای دل ما است در فنای فراق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوای نی عجب امشب به گوش جانسوز است</p></div>
<div class="m2"><p>مگر که نائی ما می زند نوای فراق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فراق یار چو آید ز پیش یار سزد</p></div>
<div class="m2"><p>که درز اشک نمایم نثار پای فراق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قلم بسوخت چوانگشت اندر انگشتم</p></div>
<div class="m2"><p>چوخواستم بنویسم زماجرای فراق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مدام ذکر دلش این بود بلنداقبال</p></div>
<div class="m2"><p>که دور ساز خدایا زما قضای فراق</p></div></div>