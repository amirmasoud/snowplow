---
title: >-
    شمارهٔ ۵۰۸
---
# شمارهٔ ۵۰۸

<div class="b" id="bn1"><div class="m1"><p>ای وجودت مظهر لطف الهی</p></div>
<div class="m2"><p>در رخت پیدا شکوه پادشاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست دست هیچکس بالای دستت</p></div>
<div class="m2"><p>حاکمی و مقتدر کن هر چه خواهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک ملک توست مشرق تا بهمغرب</p></div>
<div class="m2"><p>بنده فرمانت از مه تا به ماهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نبودی باعث ایجاد عالم</p></div>
<div class="m2"><p>بود اندر پرده آثار الهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهره وزلفی نمودی در زمانه</p></div>
<div class="m2"><p>زآن سپیدی گشت پیدا زآن سیاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بت پرستم گر مرا گردی توآمر</p></div>
<div class="m2"><p>دین دهم از کف گرم باش توناهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کن بلنداقبال را رخ ارغوانی</p></div>
<div class="m2"><p>کز غمت گردیده چهرش رنگ کاهی</p></div></div>