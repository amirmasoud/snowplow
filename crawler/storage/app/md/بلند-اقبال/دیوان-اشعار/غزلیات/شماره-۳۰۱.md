---
title: >-
    شمارهٔ ۳۰۱
---
# شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>لب لعل تو یاقوت است مرجان راست یاقوتم</p></div>
<div class="m2"><p>که ازاوچهرمن شد کهربا گون اشک یاقوتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو از گیسو اگر مانی به برج عقرب ومیزان</p></div>
<div class="m2"><p>مراهم منزل است از اشک چشمان دلو وخود حوتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوم زنده کفن درم ز جا خیزم پس از مردن</p></div>
<div class="m2"><p>کسی ذکر ار کند نام تورا در پیش تابوتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز غم سوزم چنان کز من نماندهیچ خاکستر</p></div>
<div class="m2"><p>که پیش آتش رویت به حراقی چو باروتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکردم درجوانی چارده درددل خود را</p></div>
<div class="m2"><p>چه بر میآید از دستم که اکنون پیر فرتوتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دل گفتم که پیدا نیستی هستی کجا گفتا</p></div>
<div class="m2"><p>که درچاه زنخدانش معلق همچو هاروتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلند اقبال را گفتم که چونی از غمش گفتا</p></div>
<div class="m2"><p>که در دریای اشک دیده جا گردیده چون حوتم</p></div></div>