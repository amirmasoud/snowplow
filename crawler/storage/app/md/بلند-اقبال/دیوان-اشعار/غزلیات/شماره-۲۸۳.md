---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>شب فراق تو بیرون نمی روم زخیال</p></div>
<div class="m2"><p>خیال روی توام کرده است همچو هلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وصل تو مشتاقم آن چنان ای دوست</p></div>
<div class="m2"><p>که تشنگان وگدایان به سیم وآب زلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا به عمر اگر حظ بود بود زآن خط</p></div>
<div class="m2"><p>مرا به زندگی ار حال هست از آن خال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر در آئینه کن تا مثال خود بینی</p></div>
<div class="m2"><p>چنان مدان که تو را نیست درزمانه مثال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زچشم می مفروش این قدر نیی خمار</p></div>
<div class="m2"><p>به پشت مشک مکش این همه نیی حمال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود به چهر تو آثار جلوه مهدی</p></div>
<div class="m2"><p>بود دو چشم تو آشوب و فتنه دجال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه در فراق تو ماند قرار در دل من</p></div>
<div class="m2"><p>نه باددرخم چنبر نه آب در غربال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا ز هجر خود این قدر می کنی خوارم</p></div>
<div class="m2"><p>اگر زلطف مرا خوانده ای بلند اقبال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به عهدمعتمد الدوله نیست اندر فارس</p></div>
<div class="m2"><p>به غیر زلف تو ومن کسی پریشان حال</p></div></div>