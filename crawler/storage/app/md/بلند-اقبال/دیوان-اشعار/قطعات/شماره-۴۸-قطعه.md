---
title: >-
    شمارهٔ  ۴۸ - قطعه
---
# شمارهٔ  ۴۸ - قطعه

<div class="b" id="bn1"><div class="m1"><p>ای رخت چون گل از لطافت ولون</p></div>
<div class="m2"><p>آرزوی دلم توئی به دوکون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه چوزلف تو سنبل است از بو</p></div>
<div class="m2"><p>نه چورخسار توگل است از لون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رویت از روشنی کف موسی</p></div>
<div class="m2"><p>مویت از تیرگی دل فرعون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به وصالت رسد بلند اقبال</p></div>
<div class="m2"><p>شودش گر خدای عالم عون</p></div></div>