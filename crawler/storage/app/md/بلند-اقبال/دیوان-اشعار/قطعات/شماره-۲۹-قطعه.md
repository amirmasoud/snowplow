---
title: >-
    شمارهٔ  ۲۹ - قطعه
---
# شمارهٔ  ۲۹ - قطعه

<div class="b" id="bn1"><div class="m1"><p>ای امیری که چون تو در بخشش</p></div>
<div class="m2"><p>نیست در زیر گنبد دوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بگویم کفت بود چون ابر</p></div>
<div class="m2"><p>اب رگاهی نگشته گوهر بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور بگویم قدت بود چون سرو</p></div>
<div class="m2"><p>سروگاهی نباشدش رفتار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور بگویم رخت بود چون ماه</p></div>
<div class="m2"><p>ماه گاهی نمی کند گفتار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست ما را بضاعتی که خوریم</p></div>
<div class="m2"><p>سود ازمال خویش چون تجار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست ما را شقاوتی که بریم</p></div>
<div class="m2"><p>مال کس رابه کوچه وبازار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست ما را مواجبی که شود</p></div>
<div class="m2"><p>مددی بر معاش لیل و نهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم نداریم ایل وطایفه ای</p></div>
<div class="m2"><p>تا که غارتگری کنیم شعار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شغل خود را نموده جو کاری</p></div>
<div class="m2"><p>تا به نان جوی کنیم مدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگذارند نان جو را هم</p></div>
<div class="m2"><p>کرد باید به آب سرد افطار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کس از دست ترک مینالد</p></div>
<div class="m2"><p>ما ز بیداد رهزنان کوار</p></div></div>