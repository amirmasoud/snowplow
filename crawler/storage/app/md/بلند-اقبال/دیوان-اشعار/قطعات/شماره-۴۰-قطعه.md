---
title: >-
    شمارهٔ  ۴۰ - قطعه
---
# شمارهٔ  ۴۰ - قطعه

<div class="b" id="bn1"><div class="m1"><p>خونین جگرم ز گردش چرخ</p></div>
<div class="m2"><p>افسرده دلم ز دور ایام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شام است همی که می شودصبح</p></div>
<div class="m2"><p>صبح است همی که می شود شام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وآگاه نشد کسی به دوران</p></div>
<div class="m2"><p>کآغاز چه بوده چیست انجام</p></div></div>