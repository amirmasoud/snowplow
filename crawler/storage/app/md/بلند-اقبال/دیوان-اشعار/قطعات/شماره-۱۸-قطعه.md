---
title: >-
    شمارهٔ  ۱۸ - قطعه
---
# شمارهٔ  ۱۸ - قطعه

<div class="b" id="bn1"><div class="m1"><p>سینه باشد صد در لفظ عرب</p></div>
<div class="m2"><p>صدر دیوانخانه ماهست پشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد هزاران حکم ناحق می دهد</p></div>
<div class="m2"><p>گر گذاری یک قران او را به مشت</p></div></div>