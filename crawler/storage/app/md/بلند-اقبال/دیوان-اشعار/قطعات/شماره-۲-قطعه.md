---
title: >-
    شمارهٔ  ۲ - قطعه
---
# شمارهٔ  ۲ - قطعه

<div class="b" id="bn1"><div class="m1"><p>خدا به ظلم نه راضی و بنده ظلم کند</p></div>
<div class="m2"><p>رضای بنده چرا برده از رضای خدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلا خموش شو ودم مزن از این اسرار</p></div>
<div class="m2"><p>بدان که نیست کسی مقتدر سوای خدا</p></div></div>