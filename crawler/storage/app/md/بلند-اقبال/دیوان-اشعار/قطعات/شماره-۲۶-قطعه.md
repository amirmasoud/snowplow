---
title: >-
    شمارهٔ  ۲۶ - قطعه
---
# شمارهٔ  ۲۶ - قطعه

<div class="b" id="bn1"><div class="m1"><p>آخر کار چون بود مردن</p></div>
<div class="m2"><p>این چنینت چرا غرور بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانه سازی منقش از چه سبب</p></div>
<div class="m2"><p>عاقبت منزلت چو گور بود</p></div></div>