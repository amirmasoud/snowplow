---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>ای پریشان زلف تو طومار جمع ملک چین</p></div>
<div class="m2"><p>جمع چین راهیچ مستوفی نبسته است این چنین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمع چین هم گر به بسیاری چین زلف توست</p></div>
<div class="m2"><p>پس بود خاقان چین شاهنشه روی زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناصر الدین شه از این معنی اگر آگه شود</p></div>
<div class="m2"><p>عنقریب از ری کشد لشکر پی تسخیر چین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شنیدم من ز عطاران که درچین است مشک</p></div>
<div class="m2"><p>چون بدیدم زلف تودیدم که درمشک است چین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من دلی دارم تو زلفی هیچ کس آگه نشد</p></div>
<div class="m2"><p>کاین شدآشفته از آن یا آن پریشان شد از این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود بگویم دل مرا آشفته شد از زلف تو</p></div>
<div class="m2"><p>با پریشان زلفت از بس شد دل من همنشین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دل وجان باورم شد اینکه می گویندخلق</p></div>
<div class="m2"><p>کسب خو از هم کنندار کس به کس گردد قرین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلف تو چون قنبر است وابرویت چون ذوالفقار</p></div>
<div class="m2"><p>پس به رویت هم توان گفتن امیر المؤمنین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد بلنداقبال چون زلف بلندت سرفراز</p></div>
<div class="m2"><p>تاچو زلفت شد غلامی از علی مولای دین</p></div></div>
<div class="b2" id="bn10"><p>مظهر لطف الهی باعث خلق جهان</p>
<p>پادشاه هر دو عالم خواجه کون ومکان</p></div>
<div class="b" id="bn11"><div class="m1"><p>ای به رفعت بارگاهت قبه کرده ز آفتاب</p></div>
<div class="m2"><p>بر سرا پرده جلالت طره حوران طناب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نمی سائید رخ دائم به خاک درگهت</p></div>
<div class="m2"><p>کی جهان آرا وعالمتاب می شدآفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از توگرفرمان شودصادر که صلح آرندپیش</p></div>
<div class="m2"><p>خاک بنشیند بر باد آتش افروزد ز آب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این قمر واین چرخ اطلس را خدا فرمود خلق</p></div>
<div class="m2"><p>تا به رخش وزین خود از این کنی جل ز آن رکاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کس نباشدجز تو یزدان دارد ار قائم مقام</p></div>
<div class="m2"><p>کس ندارد جز تو ایزد خواهد ار نایب مناب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بوترابت نام وتا شد مدفنت خاک نجف</p></div>
<div class="m2"><p>ذکر عرش و فرش شد یا لیتنی کنت تراب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سرکه شدهر کس شراب آورد در خاک درت</p></div>
<div class="m2"><p>پس گنه هم می شودالبته در آنجا ثواب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از حساب محشرم پروا نباشد چون توئی</p></div>
<div class="m2"><p>منشی دیوان حق مستوفی روزحساب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از توفتح باب خواهم زآنکه فرموده نبی</p></div>
<div class="m2"><p>شهر علمم من پسر عمم علی او راست باب</p></div></div>
<div class="b2" id="bn20"><p>سرفرازی ده به دیدارت بلنداقبال را</p>
<p>کن مبارک بر بلنداقبال ماه وسال را</p></div>
<div class="b" id="bn21"><div class="m1"><p>ای به خوان نعمت رزاق عالم صبح وشام</p></div>
<div class="m2"><p>قاسم الارزاق حق بر بندگان ازخاص وعام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ذات پاکت آفرینش را سبب شد ورنه کی</p></div>
<div class="m2"><p>عالمی می بود وآدم یا که صبحی بود وشام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیست اندر عالم وآدم به غیر از ذات تو</p></div>
<div class="m2"><p>ابتدا وا نتها وافتتاح و اختتام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر نبودی رهنمای خضر در ظلمات دهر</p></div>
<div class="m2"><p>بودتا روز قیامت ز آب حیوان تشنه کام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جز به توشاها به دیگر کس نگردم ملتجی</p></div>
<div class="m2"><p>کانکه آوردالتجا سوی تو شد مقضی المرام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جز به مهر تودل من خونیندازد به کس</p></div>
<div class="m2"><p>همچو آن طفلی که نشناسد به عالم غیر مام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سجده گاه مرد وزن شد قبله پیر وجوان</p></div>
<div class="m2"><p>مولدذات شریفت گشت چون بیت الحرام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر مصلی بی تولای توباطل باشدش</p></div>
<div class="m2"><p>هم رکوع و هم سجود وهم قعود وهم قیام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هفت دریا گر مرکب هفت اقلیم ارقلم</p></div>
<div class="m2"><p>هفت گردون گر ورق مدحت نخواهد شدتمام</p></div></div>
<div class="b2" id="bn30"><p>کی بلند اقبال بتواند که مدحت را سرود</p>
<p>بر تو باداز پاک یزدان صد تحیت صد درود</p></div>
<div class="b" id="bn31"><div class="m1"><p>ای که آمد سوره والشمس وصف روی تو</p></div>
<div class="m2"><p>وی که باشد آیه واللیل شرح موی تو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>این همه وصفی که در دینا ز طوبی می کنند</p></div>
<div class="m2"><p>کی به زیبائی بود چون قامت دلجوی تو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حاش لله خون نگردد هرگز اندرنافه مشک</p></div>
<div class="m2"><p>برمشام جان اوعطر ار نبخشد بوی تو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اینکه می گوینددرعالم بهشت ودوزخی است</p></div>
<div class="m2"><p>دوزخ از قهر تو شد پیدا بهشت از خوی تو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>درهمان روزی که یزدان چرخ را فرمود خلق</p></div>
<div class="m2"><p>کرد قامت را دوتا تا بوسد اوزانوی تو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اول هر ماه هرگز بدر کی گردد هلال</p></div>
<div class="m2"><p>گر بدوناید اشارت ازخم ابروی تو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>توچو خورشید درخشانی ومن حربا صفت</p></div>
<div class="m2"><p>توبه هر سو کاوری رو روی آرم سوی تو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فی المثل میل ار به لعب صولجان وگوکنی</p></div>
<div class="m2"><p>آسمان را آرزواید که گردد گوی تو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همچویعقوب از غم یوسف شم از گریه کور</p></div>
<div class="m2"><p>کی شودیارب که افتد چشم من بر روی تو</p></div></div>
<div class="b2" id="bn40"><p>گر بلنداقبال را از وصل سازی سرفراز</p>
<p>فخر برگردون کندبر آفتاب وماه ناز</p></div>
<div class="b" id="bn41"><div class="m1"><p>کافرم خوانند اگر گویم خدا باشد علی</p></div>
<div class="m2"><p>نه خدا باشد علی نه زو جدا باشدعلی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هیچ کاری را نکرده بی علی گویا خدا</p></div>
<div class="m2"><p>خودخدا فرموده چون دست خدا باشد علی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر که را رنجی رسد او راعلی گرددشفا</p></div>
<div class="m2"><p>هر که را دردی بوداورا دوا باشد علی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای دل از زندان تنگ گوردروحشت مباش</p></div>
<div class="m2"><p>زآنکه درهر جا به هردل آشنا باشد علی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نیست پروا ز این که طومار عمل دارم سیاه</p></div>
<div class="m2"><p>چون که دانم شافع روز جزا باشد علی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پاک یزدان هر چه عالم خلق فرمود وکند</p></div>
<div class="m2"><p>روبه هر عالم که آری پادشا باشد علی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عالم وآدم سراسر چون همه فانی شوند</p></div>
<div class="m2"><p>من ندارم شک به دل کاندم به جا باشد علی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تاخداگوید که ملک از کیست اوگوید ز تو</p></div>
<div class="m2"><p>تا خدا باقی است گویا در بقا باشد علی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دو بیند از از علی کو را به حق نبود دوئی</p></div>
<div class="m2"><p>پس به چشم سر ببینحق را که تا باشد علی</p></div></div>
<div class="b2" id="bn50"><p>حرف حق از بس بلنداقبال گویدآشکار</p>
<p>عاقبت ترسم کشندش همچو منصوری به دار</p></div>
<div class="b" id="bn51"><div class="m1"><p>بس پریشان روزگارم یا امیر المؤمنین</p></div>
<div class="m2"><p>چاره ای فرما به کارم یا امیر المؤمنین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همچو فراری که او را نیست در آتش قرار</p></div>
<div class="m2"><p>از غم دل بی قرارم یا امیر المؤمنین</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دامنم شد چشمه ساری بسکه از شوقت ز چشم</p></div>
<div class="m2"><p>اشک ریز و اشکبارم یا امیر المؤمنین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>غیر جرم الا گنه جز معصیت دون از خطا</p></div>
<div class="m2"><p>روز و شب کاری ندارم یا امیر المؤمنین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دارم امید اینکه یزدانم ببخشد چون توئی</p></div>
<div class="m2"><p>شافع روز شمارم یا امیر المؤمنین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وای بر حالم نباشی گر مرا فریاد رس</p></div>
<div class="m2"><p>چون شودجا در مزارم یا امیر المؤمنین</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چون به دل مهر تودارم نه دگر باک از گنه</p></div>
<div class="m2"><p>نه بود پروا زنارم یا امیر المؤمنین</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گر بگیرم آتش اندر کف نمی سوزدمرا</p></div>
<div class="m2"><p>طلق بر دست ار گذارم یا امیر المؤمنین</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مهر تو درخاصیت از طلق کی کمتر بود</p></div>
<div class="m2"><p>تن به مهرت می سپارم یا امیر المؤمنین</p></div></div>
<div class="b2" id="bn60"><p>چون بلنداقبال رامهر تو شدشامل به حال</p>
<p>آتش او راکی بسوزد این بود امری محال</p></div>
<div class="b" id="bn61"><div class="m1"><p>خسته جانم یا امیر المؤمنین سندن مدد</p></div>
<div class="m2"><p>ناتوانم یا امیر المؤمنین سن دن مدد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سال وماه و روز وشب ز اندوه ورنج و درد وغم</p></div>
<div class="m2"><p>در فغانم یا امیر المؤمنین سن دن مدد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از غم ودرد ومحن گردیده چون سوهان به تن</p></div>
<div class="m2"><p>استخوانم یا امیر المؤمنین سن دن مدد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دامنم چوننافه پر خون گشته است از بس ز چشم</p></div>
<div class="m2"><p>خون فشانم یا امیر المؤمنین سن دن مدد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در زمستان چون شود روی گلستان در جهان</p></div>
<div class="m2"><p>آن چنانم یا امیر المؤمنین سن دن مدد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چون پرکاهی که آتش سوزدش از غم بسوخت</p></div>
<div class="m2"><p>جسم وجانم یا امیر المؤمنین سن دن مدد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دهر روئین تن مرا پنداشته است وگشته است</p></div>
<div class="m2"><p>هفت خوانم یا امیر المؤمنین سن دن مدد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>روز محشر در سؤال ودرجواب اخرس شود</p></div>
<div class="m2"><p>چون زبانم یا امیرالمؤمنین سن دن مدد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>در جزای جرمم اندر آتش دوزخ شود</p></div>
<div class="m2"><p>چون مکانم یا امیرالمؤمنین سن دن مدد</p></div></div>
<div class="b2" id="bn70"><p>در صف محشر که کس را نیست در دل فکر کس</p>
<p>یا علی آن دم به فریاد بلنداقبال رس</p></div>