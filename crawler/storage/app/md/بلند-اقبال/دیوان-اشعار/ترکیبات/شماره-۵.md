---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>فتد آتش به آب وخاک شیراز</p></div>
<div class="m2"><p>رود بر باد خاک پاک شیراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه نشئه در می ونه می در انگور</p></div>
<div class="m2"><p>نه انگور است اندر تاک شیراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرنگی می کند درکام شهدم</p></div>
<div class="m2"><p>مرا زهری دهد تریاک شیراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تفاوت مشک را از پشک ننهند</p></div>
<div class="m2"><p>چه شد آن خلق با ادراک شیراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریدونی رسان یا رب که گردد</p></div>
<div class="m2"><p>زوال دولت ضحاک شیراز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه خواهم نه بمانم گردد از شه</p></div>
<div class="m2"><p>تیولم گر همه املاک شیراز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر مردی است نواب است ورنه</p></div>
<div class="m2"><p>ندیدم مردی اندر خاک شیراز</p></div></div>
<div class="b2" id="bn8"><p>شود هر ماهی از عمرش چو سالی</p>
<p>نبیند کوکب بختش وبالی</p></div>
<div class="b" id="bn9"><div class="m1"><p>قلم آندم که اندر دست گیرم</p></div>
<div class="m2"><p>حظ از خط می برددرویش ومیرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر از منشیان پرسی در انشا</p></div>
<div class="m2"><p>همه دانند بی مثل و نظیرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر از مستوفیان خواهی حسابم</p></div>
<div class="m2"><p>جوان وپیر آنها را امیرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مجو از شاعران انصاف خود ده</p></div>
<div class="m2"><p>به طرز شعر سعدی یا ظهیرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برو ز اخترشناسان جو که خوانند</p></div>
<div class="m2"><p>یکایک خواجه خواجه نصیرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر دررمل پرسی دانیالم</p></div>
<div class="m2"><p>جنایا و ضمائر را خبیرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگر جوئی ز جفر از حرف حاصل</p></div>
<div class="m2"><p>جواب هر سؤالی را بصیرم</p></div></div>
<div class="b2" id="bn16"><p>مگو ز اعداد کادریس زمانم</p>
<p>طلسم قاف را از بر بخوانم</p></div>
<div class="b" id="bn17"><div class="m1"><p>مگر کاری کند الطاف نواب</p></div>
<div class="m2"><p>که بخت ما شود بیدار از خواب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگرنه راه امید است مسدود</p></div>
<div class="m2"><p>ز هرکوی وزهر سوی وزهر باب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه یاری خواهم ار برنا نه از پیر</p></div>
<div class="m2"><p>نه کاری آید از شیخ و نه از شاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>علاج درد ما را کی تواند</p></div>
<div class="m2"><p>طبیبی کو نکرد از شهد جلاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مگر از ابر لطف وهمت او</p></div>
<div class="m2"><p>شودکشت امیدم سبز و سیراب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مگردرمان دردم اوکندکو</p></div>
<div class="m2"><p>دهدالفت میان آتش وآب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بود پیوسته خصمش درتب ولرز</p></div>
<div class="m2"><p>چو روی آتش اندر بوته سیماب</p></div></div>
<div class="b2" id="bn24"><p>دل او خرم ومسرور بادا</p>
<p>زجانش رنج ومحنت دور بادا</p></div>
<div class="b" id="bn25"><div class="m1"><p>فلک قدرا نظر فرما به حالم</p></div>
<div class="m2"><p>که از دست زمانه پایمالم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کشیده تر مرا بود از الف قد</p></div>
<div class="m2"><p>کنون از بارغم خم تر ز دالم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز بس مویم همه خوانندمویم</p></div>
<div class="m2"><p>ز بس نالم همه دانند نالم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنان گردیده جسمم از غم ورنج</p></div>
<div class="m2"><p>که نتواند کس آرد درخیالم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بردهمچون غباری تا جنوبم</p></div>
<div class="m2"><p>اگر بر تن وزد باد شمالم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به بخت خود زنم هر چند قرعه</p></div>
<div class="m2"><p>نقی وانگیس می آید به فالم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مگر از التفات ویاری تو</p></div>
<div class="m2"><p>در آید کوکب بخت از وبالم</p></div></div>
<div class="b2" id="bn32"><p>بیا از مرحمت یاری به من کن</p>
<p>کرم فرما مددکاری به من کن</p></div>
<div class="b" id="bn33"><div class="m1"><p>مشیر الملک اندرسال پارم</p></div>
<div class="m2"><p>بسی عزت نمود واعتبارم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پی خدمت به نیریزم فرستاد</p></div>
<div class="m2"><p>چو محرم دید وبس خدمتگذارم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو درخدمت مرا دیدندقابل</p></div>
<div class="m2"><p>حسودان رشک بردندی ز کارم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز من بدها به پیش خواجه گفتند</p></div>
<div class="m2"><p>که تا خائن کنندو خوار وزارم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مشیر الملک هم بشنید از ایشان</p></div>
<div class="m2"><p>سیه ز آنرو چنین شد روزگارم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به زیر بار خوددرمانده بودم</p></div>
<div class="m2"><p>ز نوباری گران کردندبارم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنان گردیده ام اکنون که دردل</p></div>
<div class="m2"><p>نمی باشد خیالی جز فرارم</p></div></div>
<div class="b2" id="bn40"><p>دوحاجت دارم از سرکار نواب</p>
<p>که آسان تر بو از خوردن آب</p></div>
<div class="b" id="bn41"><div class="m1"><p>یکی هر قسم دانی وتوانی</p></div>
<div class="m2"><p>مفاصای حسابم را ستانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دوم اذنی بگیری تا ز شیراز</p></div>
<div class="m2"><p>روم جائی که نامش را ندانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چه گردد گر دل ویرانه ام را</p></div>
<div class="m2"><p>شوی بانی ز لطف ومهربانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به منزل بی خطر خواهم رسیدن</p></div>
<div class="m2"><p>کندلطف توام گر همعنانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مشیر الملک را از این دو مطلب</p></div>
<div class="m2"><p>اگر دیدی که دارد سرگرانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مگو دیگر سخن خاموش بنشین</p></div>
<div class="m2"><p>مبادا در دل آرد بدگمانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>وگر افکند دیدی چین درابرو</p></div>
<div class="m2"><p>بخوان اخلاص یا سبع المثانی</p></div></div>
<div class="b2" id="bn48"><p>زجا برخیز و می کن قصه کوتاه</p>
<p>سخن دیگر مگو الحکم لله</p></div>
<div class="b" id="bn49"><div class="m1"><p>ز شیراز ار روم نایم دگر باز</p></div>
<div class="m2"><p>نخواهم برد دیگر نام شیراز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>حساب ما به محشر اوفتاده</p></div>
<div class="m2"><p>به مستوفی بگو دفتر مکن باز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شوم چون صعوه تاکی صید هر کس</p></div>
<div class="m2"><p>روم صیاد گردم همچو شهباز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کنم از فارس تا بر هم زنی چشم</p></div>
<div class="m2"><p>ز قید تن چو مرغ روح پرواز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>وطن در کشوری گیرم که خلقم</p></div>
<div class="m2"><p>کنند از جان ودل اکرام و اعزاز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>روم جائی که باشد قدردانی</p></div>
<div class="m2"><p>دهندم رتبه سازندم سرافراز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شوم از مرتبت درخلق مخصوص</p></div>
<div class="m2"><p>شوم از منزلت در دهرممتاز</p></div></div>
<div class="b2" id="bn56"><p>چرا درفارس خوار وزار باشم</p>
<p>روم جائی که تا سالار باشم</p></div>