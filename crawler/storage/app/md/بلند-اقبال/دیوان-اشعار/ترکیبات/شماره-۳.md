---
title: >-
    شمارهٔ  ۳
---
# شمارهٔ  ۳

<div class="b" id="bn1"><div class="m1"><p>از چه رو ای زلف پرچین وشکن گردیده ای</p></div>
<div class="m2"><p>پای تا سر چین چو پیشانی من گردیده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل کمندرستم وسام نریمان خواندت</p></div>
<div class="m2"><p>بسکه پرپیچ وخم وچین وشکن گردیده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وجوداینکه اندر بنددلداری اسیر</p></div>
<div class="m2"><p>شور شهر وفتنه هر انجمن گردیده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دشمن جان وتن خورد ودرشت وشیخ وشاب</p></div>
<div class="m2"><p>آفت دین ودل هر مردوزن گردیده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زآن همی ترسم که روزی سر برندت عاقبت</p></div>
<div class="m2"><p>بسکه هستی سرکش از بس راهزن گردیده ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سجده آری هر دم از بس پیش رخسار بتم</p></div>
<div class="m2"><p>هر که می بیند تو راگوید شمن گردیده ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چهره جانان اگر برهان یزدان آمده</p></div>
<div class="m2"><p>هم تو درعالم دیل اهرمن گردیده ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش دل شعله ورتر می شود هر دم مرا</p></div>
<div class="m2"><p>بسکه درجنبش همی چون بادزن گردیده ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشک خیز ومشک بیز و مشک سای ومشک زای</p></div>
<div class="m2"><p>رشک تبت حسرت چین وختن گردیده ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تاکه بنمائی عیار حسن روی او به خلق</p></div>
<div class="m2"><p>چون محک اندر بر آن سیم تن گردیده ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پاسبان گنج حسن روی خویشت کرده یار</p></div>
<div class="m2"><p>با همه دزدی امین ومؤتمن گردیده ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم ای زلف از مکافات عمل غافل مشو</p></div>
<div class="m2"><p>دیدی آخر دل پریشان تر ز من گردیده ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون بلنداقبال برخورشید ومه نازی دگر</p></div>
<div class="m2"><p>بنده ای از بندگان بوالحسن گردیده ای</p></div></div>
<div class="b2" id="bn14"><p>شیریزدان شاه اژدر در امیر المؤمنین</p>
<p>خواجه هفتم سپهر ومالک هفتم زمین</p></div>
<div class="b" id="bn15"><div class="m1"><p>معتقد ای زلف دلبر بر چه مذهب بینمت</p></div>
<div class="m2"><p>گاه کافر گاه در دل ذکر یارب بینمت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درتودلها بسکه می گویند یا رب تا به روز</p></div>
<div class="m2"><p>معبد عباد یا رب گوی هر شب بینمت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نوروظلمت روز وشب را می نماید صبح وشام</p></div>
<div class="m2"><p>زآن به روشن روز رویش تیره چون شب بینمت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گه به گنج حسن دلبر مار ارقم یابمت</p></div>
<div class="m2"><p>گه به مهتاب رخش جراره عقرب بینمت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لف و نشر ار خوانمت ای زلف می بینم به جاست</p></div>
<div class="m2"><p>گه مشوش می شوی گاهی مرتب بینمت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خواستم نال قلم خوانم تو را دیدم خطاست</p></div>
<div class="m2"><p>در سیاهی زآنکه مانندمرکب بینمت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از پی تعلیم گاهی راست گردی گاه کج</p></div>
<div class="m2"><p>در بر استاد چون طفلان مکتب بینمت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همچو مستان درخرام افتی گه از چپ گه ز راست</p></div>
<div class="m2"><p>مست وسرخوش از شراب ناب آن لب بینمت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سر به زانو هشته می لرزی به پیش آفتاب</p></div>
<div class="m2"><p>هندوی عریان لرز آورده از تب بینمت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای وجودت واجب اندر روزگار دلبری</p></div>
<div class="m2"><p>واجب اندر روی دلبر بلکه اوجب بینمت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مانی آن هندوی را کاندر کفش باشد ترنج</p></div>
<div class="m2"><p>هر زمان کاندر بر آن سیب غبغب بینمت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از پریشان حالی ما بازگو درگوش یار</p></div>
<div class="m2"><p>ای که در درگاه روی اومقرب بینمت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با وجود اینکه داری در بر دلدار جای</p></div>
<div class="m2"><p>چون بلنداقبال دایم تیره کوکب بینمت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روی دلبر حیدر است و ابروی او ذوالفقار</p></div>
<div class="m2"><p>خیره سرای زلف تیره دل چو مرحب بینمت</p></div></div>
<div class="b2" id="bn29"><p>کرده دو نیمت زبس می بیندت کافر شعار</p>
<p>دریمین یک نیمه ات افکنده نیمی دریسار</p></div>
<div class="b" id="bn30"><div class="m1"><p>همچو من ای زلف دلبر از چه درهم گشته ای</p></div>
<div class="m2"><p>عاشق یاری همانا درهم از غم گشته ای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>راستی از بس پریشانی برآن سیم تن</p></div>
<div class="m2"><p>کرده گردن کج ز وی خواهان درهم گشته ای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>می کشی بار دل پرحسرت ما را به دوش</p></div>
<div class="m2"><p>زآن ره ای زلف این چنین حمال سان خم گشته ای</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گاه بودی لام وگاهی لام الف لا گاه دال</p></div>
<div class="m2"><p>حال زیر خال دلبر ذال معجم گشته ای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چهره دلدار راخوانند اگر روز ربیع</p></div>
<div class="m2"><p>می توان گفتن شب قتل محرم گشته ای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جای داری گه به دوش وگه در آغوش نگار</p></div>
<div class="m2"><p>باشد از افتادگی کاین سان مکرم گشته ای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>می کندناسور می گویند زخم از بوی مشک</p></div>
<div class="m2"><p>زخم دلها را تو خود مشکی ومرهم گشته ای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ای مجعد زلف یار از بسکه هستی مشکبار</p></div>
<div class="m2"><p>فتنه چین وختن آشوب دیلم گشته ای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تادل ما را دهی راهی به قصر روی یار</p></div>
<div class="m2"><p>پایه پایه تا سر همچو سلم گشته ای</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در بهشت روی گندم گون دلدار از ازل</p></div>
<div class="m2"><p>همچوشیطان رهزن حوا و آدم گشته ای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دلبر ما کرده از قامت قیامت چون به پا</p></div>
<div class="m2"><p>مو به مو اعمال کفاری مجسم گشته ای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون بلنداقبال اندر طور وطرز شاعری</p></div>
<div class="m2"><p>هم تو اندر دلبری الحق مسلم گشته ای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جا به زیر سایه ات خورشید ومه دارد مگر</p></div>
<div class="m2"><p>قنبر درگاه مولای دو عالم گشته ای</p></div></div>
<div class="b2" id="bn43"><p>خواجه رکن و مقام وصاحب خیل وحرم</p>
<p>معنی طه و یس مظهر نون والقلم</p></div>
<div class="b" id="bn44"><div class="m1"><p>شافع فردای محشر حیدر صفدر علی</p></div>
<div class="m2"><p>باب شبیر وشبر داماد پیغمبر علی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>درنظر ای زلف چون افعی ارقم گشته ای</p></div>
<div class="m2"><p>از پی بلعیدن دل اژدها دم گشته ای</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>داری از بس پیچ وتاب وحلقه و چین وشکن</p></div>
<div class="m2"><p>چون کمند پرخم سهراب ورستم گشته ای</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ترک ما شد موی جوشن درع خط صورت سپر</p></div>
<div class="m2"><p>رمح بالا و سنان مژگان تو پرچم گشته ای</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>صید چنگ شیر از حال دلم دارد خبر</p></div>
<div class="m2"><p>بینمت هر گه که چون چنگال ضیغم گشته ای</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خنجر ابروی دلبر را همی گیری به کف</p></div>
<div class="m2"><p>بیگنه خونریزی ما را مصمم گشته ای</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بینمت همخانه با خورشید رخشان روز و شب</p></div>
<div class="m2"><p>می سزد گر گویمت عیسی ابن مریم گشته ای</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از رکوع و از سجود واز قیام واز قعود</p></div>
<div class="m2"><p>هر که دیدت گفت ابراهیم ادهم گشته ای</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نیستی پیر از چه بر رویت چنین افتاده چین</p></div>
<div class="m2"><p>غم نداری از چه بی آرام ودرهم گشته ای</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پاسبان گنج حسن روی دلبر بینمت</p></div>
<div class="m2"><p>با همه دزدی چه شدکاین گونه محرم گشته ای</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گه به دوشش گه به گوشش می نهی سر بی ادب</p></div>
<div class="m2"><p>با پری بنشسته ای با آدمی کم گشته ای</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خود بلنداقبال را کشتی وخود از قتل او</p></div>
<div class="m2"><p>کرده ای در بر سیه وز اهل ماتم گشته ای</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بوی مشک ونکهت عنبر تو را باشد مگر</p></div>
<div class="m2"><p>مشک را داماد وعنبر را پسر عم گشته ای</p></div></div>
<div class="b2" id="bn57"><p>چون علی کو بن عم و داماد پیغمبر بود</p>
<p>روز محشر تشنگان را ساقی کوثر بود</p></div>
<div class="b" id="bn58"><div class="m1"><p>دانم ای زلف از چه رودائم پریشان می شوی</p></div>
<div class="m2"><p>در تو جا دارد دل آشفته ام ز آن می شوی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همنشینی با پریشان دل تو را گفتم مکن</p></div>
<div class="m2"><p>کآخر از حال پریشانش پریشان می شوی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هستی از بس مشک خیز ومشک بیز ومشک ریز</p></div>
<div class="m2"><p>مشک ارزان می شود هر گه که لرزان می شوی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چون تو را دلبر به روی خود پریشان می کند</p></div>
<div class="m2"><p>دشمن هوش و خردخصم دل وجان می شوی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نیستی گر زاهدو وسواست ار نبود چرا</p></div>
<div class="m2"><p>پیش چشم مست او برچیده دامن می شوی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گه زره پوشی وچون داودد گه سازی زره</p></div>
<div class="m2"><p>گاه دیگر پای تا سر خود زره سان می شوی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گر نیی افراسیاب ورستم دستان چرا</p></div>
<div class="m2"><p>فتنه ایران زمین آشوب توران می شوی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون که بینی ابروی جانان کمان آرش است</p></div>
<div class="m2"><p>چون کمند پر خم سام نریمان می شوی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ای سیه زلف نگار ار نیستی ابر از چه رو</p></div>
<div class="m2"><p>بینمت گاهی حجاب مهر رخشان می شوی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گاه می بینم که چون عقرب به جولان می روی</p></div>
<div class="m2"><p>گاه می بینم که همچون مار پیچان می شوی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گاه درهم رفته چون خط ترسل بینمت</p></div>
<div class="m2"><p>گه به هم پیچیده چون توقیع فرمان می شوی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>یار چون بینیم که سیمین گوی دارد از زنخ</p></div>
<div class="m2"><p>پیش سیمین گوی اومانند چوگان می شوی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>طره حوا بردرشک از تو چون جاروب سان</p></div>
<div class="m2"><p>خاک روب درگه مولای سلمان می شوی</p></div></div>
<div class="b2" id="bn71"><p>فخر عالم ذخر آدم پادشاه هر دو کون</p>
<p>ملجاء هر مسلم وکافر پناده هر دوکون</p></div>