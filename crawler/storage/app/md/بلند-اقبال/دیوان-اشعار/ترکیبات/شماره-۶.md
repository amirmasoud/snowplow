---
title: >-
    شمارهٔ  ۶
---
# شمارهٔ  ۶

<div class="b" id="bn1"><div class="m1"><p>یا رب از ظلم مشیرالملک خواری تا به کی</p></div>
<div class="m2"><p>از تعدی های او افغان و زاری تا به کی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز رفت و هفته رفت و ماه رفت و سال رفت</p></div>
<div class="m2"><p>صبر تاکی تاب تا کی بردباری تا به کی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کافری باشد اگر گویم که مأیوسم ولی</p></div>
<div class="m2"><p>رحمتت را صبر در امیدواری تا به کی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من نه ایوبم نه یعقوبم که آرم صبر وتاب</p></div>
<div class="m2"><p>بیقراری تا به چند و اشکباری تا به کی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر درگاه او ای ذوالجلال بی زوال</p></div>
<div class="m2"><p>خاکبوسی تا به چند و خاکساری تا به کی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوخت ما را ز آتش کین خرمن وبرباد داد</p></div>
<div class="m2"><p>مزرع امید او را آبیاری تا به کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه دانی مصلحت خوبست لیک ای کردگار</p></div>
<div class="m2"><p>اینهمه در کار او احسان و یاری تا به کی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ندانی مصلحت کو را در اندازی ز پای</p></div>
<div class="m2"><p>گو بدو با بندگانم کین شعاری تا به کی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساختی از کشته ها بس پشته ها ای پهلوان</p></div>
<div class="m2"><p>تیغ بازی تا به چند و نیزه داری تا به کی</p></div></div>
<div class="b2" id="bn10"><p>ما تو را عبد ذلیلیم ای خداوند جلیل</p>
<p>این و آن راضی مشو ما را کنند از کین ذلیل</p></div>
<div class="b" id="bn11"><div class="m1"><p>سیر از ظلم مشیر الملک از جان گشته ایم</p></div>
<div class="m2"><p>یا رب از حلم تو و صبر تو حیران گشته ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز آن ستم پرور نه من تنها چنین آشفته ام</p></div>
<div class="m2"><p>از جفا وجور اوجمعی پریشان گشته ایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای بسا مردم که می خوردند نان از خوان ما</p></div>
<div class="m2"><p>پیش دونان خود کنون محتاج یک نان گشته ایم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر سر ما کوبد از بس پتک ظلم آن کینه جو</p></div>
<div class="m2"><p>معتقد خلقی که ما پولاد سندان گشته ایم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حال ما در عهد او از جور و کین او به فارس</p></div>
<div class="m2"><p>عمر را گوئی اسیر بند و زندان گشته ایم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد غذای ما دل بریان وخوناب جگر</p></div>
<div class="m2"><p>گاه گاهی بر سر خوانش چومهمان گشته ایم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شدکمال ما وبال ما کند خصمی به ما</p></div>
<div class="m2"><p>زآنکه می بیند سخن گوی وسخندان گشته ایم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آمدیم از فضل و لطفت از عدم سوی وجود</p></div>
<div class="m2"><p>بار الها با چنین حالت پشیمان گشته ایم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه رهین منت کس در جهان گردیده ایم</p></div>
<div class="m2"><p>نه کسی را تاکنون ممنون احسان گشته ایم</p></div></div>
<div class="b2" id="bn20"><p>پاک یزدانا مینداز احتیاج ما به کس</p>
<p>هم مکن ما را رهین منت کس ز این سپس</p></div>
<div class="b" id="bn21"><div class="m1"><p>ای خدای بی شریک ای کردگار بی نظیر</p></div>
<div class="m2"><p>از مشیر الملک ظالم داد مظلومان بگیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حلم را بردار از او فرصتش دیگر مده</p></div>
<div class="m2"><p>دست ظلمش را بکن کوته ز دارا و فقیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جز ستمکاری نیارد آن جفا جو درخیال</p></div>
<div class="m2"><p>غیر غداری ندارد آن بد آئین در ضمیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>موی دارد چون سپید وقلب دارد چونسیاه</p></div>
<div class="m2"><p>هم ز رنج ودرد رویش زرد بادا چون زریر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نیست زو آرام یک دل ای خدا از شیخ وشاب</p></div>
<div class="m2"><p>نیست زو آسوده یک تن یارب از برنا و پیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خود تو آگاهی که ازجور مشیر الملک ما</p></div>
<div class="m2"><p>از حیات خویش بیزاریم وازجانیم سیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یارب ار ما مستحق ظلم می باشیم وجور</p></div>
<div class="m2"><p>ارحم اللهم یا ذوالجود والفضل الکثیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رحم فرما عفو فرما بگذر از تقصیر ما</p></div>
<div class="m2"><p>رفته ایم از پای ما را از کرم شو دستگیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آگهی از حاجت ما ای خدای غیب دان</p></div>
<div class="m2"><p>حاجت اظهار نبود هم سمیعی هم بصیر</p></div></div>
<div class="b2" id="bn30"><p>رحم کن بر حال ما درکار ما فرما نظر</p>
<p>ور گنهکاریم یا رب از گنه مان درگذر</p></div>
<div class="b" id="bn31"><div class="m1"><p>چشم امید از مشیر الملک یارب کور شد</p></div>
<div class="m2"><p>آرزوها ازجفای او زدلها دور شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آفتاب مکرمت در آسمان عهد او</p></div>
<div class="m2"><p>زیر ابر کینه و بغض وحسد مستور شد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر کجا صاحبدلی ز اعمال او مغموم گشت</p></div>
<div class="m2"><p>هر کجا لایعقلی از مال او مسرور شد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر کجا دیوی به عهد او سلیمانی نمود</p></div>
<div class="m2"><p>هر کجا بودی سلیمانی ز ظلمش مور شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این چنین دون پروری هرگز ندارد کس به یاد</p></div>
<div class="m2"><p>با سپهر کج روش درجور طبعش جور شد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بس دل معمور کاندر عصر او ویرانه گشت</p></div>
<div class="m2"><p>بس کل ویرانه کاندر عهد او معمور شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حیف کز بیداد آن بیدادگر در ملک فارس</p></div>
<div class="m2"><p>شد قلمدان ها قشو شمشیر ها ساطور شد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کامها شور این قدر گردیده است از شور او</p></div>
<div class="m2"><p>کز نمک گر حال پرسی گوید از حد شور شد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای خوشا آنکس که پیش از عهد ودوران مشیر</p></div>
<div class="m2"><p>عمر را سر کرد وجای او بقعر گور شد</p></div></div>
<div class="b2" id="bn40"><p>بی خبر باشد شهنشه گوئی از کردار او</p>
<p>ورنه در دنیا نبودی تاکنون آثار او</p></div>
<div class="b" id="bn41"><div class="m1"><p>یارب از ظلم مشیر الملک ما را ده نجات</p></div>
<div class="m2"><p>گشته ایم از زندگانی سیر وبیزار از حیات</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یا بده او را به دل رحمی که رحمی آورد</p></div>
<div class="m2"><p>یا پی آسودگی ما راکرم فرما ممات</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تلخکامی داده است از بس مرا ازجور خویش</p></div>
<div class="m2"><p>درمذاقم تلخ تر می آید از حنظل نبات</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>قرعه هر چند از برای خود زنم درعهداو</p></div>
<div class="m2"><p>عقله وانگیس وحمره بینم اندر امهات</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کرده ما را پیش خلقی سرشکسته چون قلم</p></div>
<div class="m2"><p>دارد از بس دل زجور وکین سیه تر از دوات</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بی خیال وبی مجال وبی سؤال ای ذوالجلال</p></div>
<div class="m2"><p>قادری کز دست ظلم اودهی ما را نجات</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از صفات او دلم عارف بذاتش گشته است</p></div>
<div class="m2"><p>پی بسوی ذات باید برد آری از صفات</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کی شود آندم خداوندا که از غیبم سروش</p></div>
<div class="m2"><p>سرنهد در گوش دل گوید مشیر الملک مات</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پیش ظلم وجور او از ما نخواهد ماند اثر</p></div>
<div class="m2"><p>پشه پیش تندباد آری کجا آرد ثبات</p></div></div>
<div class="b2" id="bn50"><p>ای کریم بی مثال ای ذوالجلال بی زوال</p>
<p>دستگیری کن مرا مگذار گردم پایمال</p></div>