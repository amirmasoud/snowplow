---
title: >-
    شمارهٔ  ۴
---
# شمارهٔ  ۴

<div class="b" id="bn1"><div class="m1"><p>زلفکان تومگر عطارند</p></div>
<div class="m2"><p>کاینهمه مشک به دامان دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشت موئی نه فزونند از بو</p></div>
<div class="m2"><p>گوئی از مشک دو صد خروارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بگوئیم که مشکند خطاست</p></div>
<div class="m2"><p>که به هر تار دوصدتاتارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب هجران تو یا بخت منند</p></div>
<div class="m2"><p>که چنین تیره بدین سان تارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرح آشفتگی حال مرا</p></div>
<div class="m2"><p>از ازل تا به ابد طومارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اوفتندت ز یمین و ز یسار</p></div>
<div class="m2"><p>گاه چون رشته گهی زنارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوشدارو طلبند از لب تو</p></div>
<div class="m2"><p>مگرازعشق رخت بیمارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از درازی چو شب یلدایند</p></div>
<div class="m2"><p>وز سیاهی چو دل کفارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برده اند از کف خلقی دل ودین</p></div>
<div class="m2"><p>بوالعجب حیله گر وسحارند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر نه طاووس چرا در خلدند</p></div>
<div class="m2"><p>گر سمندر نه چرا در نارند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرنه عقرب زچه در جولانند</p></div>
<div class="m2"><p>ورنه افعی ز چه پر آزارند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گزنه دزدند چرا دربندند</p></div>
<div class="m2"><p>از چه لرزند نه گر عیارند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرنه مشکند چرا مشک افشان</p></div>
<div class="m2"><p>ورنه عنبر ز چه عنبر بارند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مارکانند که در بستانند</p></div>
<div class="m2"><p>زاغکانند که درگلزارند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زنگیانند که از زنگ به روم</p></div>
<div class="m2"><p>ارمغان در بر قیصر آرند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یادو هندوست که در باغ ارم</p></div>
<div class="m2"><p>گاه گلچین وگهی گلکارند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گنج حسن است رخ وزلفینت</p></div>
<div class="m2"><p>زده چنبر به سرش چون مارند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پی تاراج دل وغارت دین</p></div>
<div class="m2"><p>سپه حسن تو را سالارند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سر ز ایشان مبر ای یار ارچه</p></div>
<div class="m2"><p>سرکش وراهزن و طرارند</p></div></div>
<div class="b2" id="bn20"><p>که غلامی ز غلامان شهند</p>
<p>سایه افکن به سر مهر ومهند</p></div>