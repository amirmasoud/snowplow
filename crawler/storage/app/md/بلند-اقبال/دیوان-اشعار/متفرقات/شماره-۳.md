---
title: >-
    شمارهٔ  ۳
---
# شمارهٔ  ۳

<div class="b" id="bn1"><div class="m1"><p>قوت اگر نیستت ز شیر وشکر</p></div>
<div class="m2"><p>قوت روح هست خون جگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کف بود جام آبخور گر نیست</p></div>
<div class="m2"><p>جام بلور یا پیاله زر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در و گوهر گرت میسر نیست</p></div>
<div class="m2"><p>اشک چشمان تو را در وگوهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایه وآفتاب پیرهن است</p></div>
<div class="m2"><p>پیرهن گر نباشد اندر بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر ندارد کله اگر غم نیست</p></div>
<div class="m2"><p>کله ازمویخویش دارد سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بالش وبستر از پرندار نیست</p></div>
<div class="m2"><p>خاک راهست بالش وبستر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرت آتش نباشد اندر دل</p></div>
<div class="m2"><p>آتش عشق بس تو را در بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرت ار نیست رو پیاده به راه</p></div>
<div class="m2"><p>کآدمی خود نه کمتر است از خر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در غم ار یاورت کسی نشود</p></div>
<div class="m2"><p>غم چه داری خدا بودیاور</p></div></div>
<div class="b2" id="bn10"><p>یاور کس چو حی داور شد</p>
<p>زهر درکام اوچو شکر شد</p></div>