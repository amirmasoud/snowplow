---
title: >-
    شمارهٔ  ۱ - در مرثیه بر شهیدان کربلا
---
# شمارهٔ  ۱ - در مرثیه بر شهیدان کربلا

<div class="b" id="bn1"><div class="m1"><p>از دست ظلم شمر ستمگر به کربلا</p></div>
<div class="m2"><p>گریان شدند مؤمن وکافر به کربلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز کسی ندیده ونشنیده درجهان</p></div>
<div class="m2"><p>جوری که شد به آل پیمبر به کربلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پنداشتی قیامت کبری پدید گشت</p></div>
<div class="m2"><p>کافتاده بود شورش محشر به کربلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افغان العطش زچه میرفت بر فلک</p></div>
<div class="m2"><p>میبود اگر که ساقی کوثر به کربلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید منکسف شد ومهگشت منسخف</p></div>
<div class="m2"><p>از شرم ظلم شمر بداختر به کربلا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برنیزه رفت بسکه سر سروان دین</p></div>
<div class="m2"><p>طالع شد آفتاب ز هر سر به کربلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مسدود بود راه نظر طایران تیر</p></div>
<div class="m2"><p>از بسکه میزدند همی پر به کربلا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجنون شوم به حالت لیلا در آن زمان</p></div>
<div class="m2"><p>کز کین شهید شد علی اکبر به کربلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در برکشید مشک خطان را ز بس همی</p></div>
<div class="m2"><p>ز آنروی خاک گشته معطر به کربلا</p></div></div>
<div class="b2" id="bn10"><p>یا رب به آه وناله دلهای دردناک</p>
<p>ما رانصیب کن که درآنجا شویم خاک</p></div>