---
title: >-
    شمارهٔ  ۷
---
# شمارهٔ  ۷

<div class="b" id="bn1"><div class="m1"><p>بس که ای زلف عنبرین موئی</p></div>
<div class="m2"><p>عنبر افشان وعنبرین بوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمت مشک چین خطا گفتم</p></div>
<div class="m2"><p>که تو صد مشک چین به هر موئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درشکنجی وپیچ وتاب مگر</p></div>
<div class="m2"><p>عاشق آن بت جفاجوئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از درازی شب زمستانی</p></div>
<div class="m2"><p>وز سیاهی پر پرستوئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلقه حلقه شکن شکن خم خم</p></div>
<div class="m2"><p>از بر دوش تا به زانوئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنخ یار گوئی از سیم است</p></div>
<div class="m2"><p>توچوچوگان به پیش او گوئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی قراری به روی یار چرا</p></div>
<div class="m2"><p>گرنه آشفته رخ اوئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بسنجی وفا ومهر مرا</p></div>
<div class="m2"><p>شده آونگ چون ترازوئی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لام برچهره می کشد هندو</p></div>
<div class="m2"><p>شکل لا می توگر چه هندوئی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با وجودی که بوی مشک تو راست</p></div>
<div class="m2"><p>بر دل ریش نوشداروئی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روزم از تیره شب سیاه تر است</p></div>
<div class="m2"><p>زآنکه دایم حجاب آن روئی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرکه در بند بیندت گوید</p></div>
<div class="m2"><p>که چودزدان بسته باروئی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرده ای جا به دوش یار مگر</p></div>
<div class="m2"><p>مار ضحاک دوش جادوئی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آفتاب است زیر سایه تو</p></div>
<div class="m2"><p>مر چو منشاه را ثنا گوئی</p></div></div>
<div class="b2" id="bn15"><p>شیریزدان امیر اژدر در</p>
<p>شافع حشر خواجه قنبر</p></div>