---
title: >-
    شمارهٔ  ۴ - تاریخ فوت مشیر وقوام
---
# شمارهٔ  ۴ - تاریخ فوت مشیر وقوام

<div class="b" id="bn1"><div class="m1"><p>مشیری و قوامی داشت شیراز</p></div>
<div class="m2"><p>که هر دو در ستم بودند یکه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برفتند از جهان ز آن پس که هشتند</p></div>
<div class="m2"><p>ز بدعت ها به روی فارس لکه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگفتم با خرد تاریخشان را</p></div>
<div class="m2"><p>رقم زن چون بهزر وسیم سکه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی بیرون شد وگفتا که برگو</p></div>
<div class="m2"><p>به دوزخ هر دو را گردید دکه</p></div></div>
<div class="c"><p>۱۳۰۱ قمری</p></div>