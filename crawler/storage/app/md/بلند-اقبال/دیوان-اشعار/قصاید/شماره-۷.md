---
title: >-
    شمارهٔ  ۷
---
# شمارهٔ  ۷

<div class="b" id="bn1"><div class="m1"><p>در شب غره ماه رمضانم دلبر</p></div>
<div class="m2"><p>سرکش وتندودژآهنج درآمد از در</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیسویش چین چین از فرق فتاده به میان</p></div>
<div class="m2"><p>طره اش خم خم از دوش رسیده به کمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلفش افتاده به دوشش ز یمین وز یسار</p></div>
<div class="m2"><p>چون دو زاغی که نشینند به شاخ عرعر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا نه گفتی که دو زنگی بر میری رومی</p></div>
<div class="m2"><p>خم به تعظیم شدندی ز یمین وز یسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش ای بت شنگول چرائی توملول</p></div>
<div class="m2"><p>کز ملامت زدی اندر دل وجانم آذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت دانی به من شیفته دل دیگر بار</p></div>
<div class="m2"><p>از ره کینه چه کرد این فلک بداختر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساعتی پیش هلالی ز افق کرد پدید</p></div>
<div class="m2"><p>شرمی از ابروی من هیچ نکرد آن کافر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلبران گفتند افراخته قامت عاشق</p></div>
<div class="m2"><p>عاشقان گفتند ابروی نموده دلبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و آن نبد ابروی یار ونه قد عاشق زار</p></div>
<div class="m2"><p>بد هلال همه روزه که چنین بد لاغر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من تو دانی که دمی زیست نتانم گر نیست</p></div>
<div class="m2"><p>ساقی وساغر و می مطرب وچنگ و مزمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مصلحت چیست ندانم چه کنم چون سازم</p></div>
<div class="m2"><p>به توگفتم غم دل تا تو کنی چاره مگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم ای شوخ چه گوئی رمضان آمد باز</p></div>
<div class="m2"><p>خیز از جا که کنیم الآن آهنگ سفر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در حدیث است اگر چه زرسول مختار</p></div>
<div class="m2"><p>که سفر نزد خرد قطعه ای آمد ز سقر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیک صد ره ز حضر هست سفر کردن به</p></div>
<div class="m2"><p>خارسان خوار شوی چون به نظرها به حضر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باز فردا است که درمیکده پیر خمار</p></div>
<div class="m2"><p>معتکف گردد و بندد به رخ از انده در</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باز فردا است کز اندیشه زاهد نبود</p></div>
<div class="m2"><p>به در دیر مغان کس را یارای گذر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باز فردا است که بندند در میخانه</p></div>
<div class="m2"><p>وز غمش رند قدح نوش خورد خون جگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باز فردا است که بر منبر واعظ گوید</p></div>
<div class="m2"><p>کایها القوم بترسید ز روز محشر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در حدیث است که باشد به سقر مارانی</p></div>
<div class="m2"><p>که بسی دندانشان تیز تر است از خنجر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عقربی هست چنان کو را نیشی است چنین</p></div>
<div class="m2"><p>که به کوه ار گذرد کوه شود خاکستر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرکه میخواره شنیدستم در روز جزا</p></div>
<div class="m2"><p>بهره اش هیچ نباشد ز شراب کوثر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه دانید که غیبت بتر آمد ز زنا</p></div>
<div class="m2"><p>پس هم از این هم از آن جمله نمائید حذر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زن ومرد از گنه پیش کنید استغفار</p></div>
<div class="m2"><p>پس از این هیچ معاصی ننمائید دگر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شرمی از ابروی و زلف تو ندارد واعظ</p></div>
<div class="m2"><p>که نهد روی به محراب وپا بر منبر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هان در این شهر در این شهر که نتوان می خورد</p></div>
<div class="m2"><p>از چه سازیم مکان بهر چه گیریم مقر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>روزه تا شرعا هر روزه خوریم وگوئیم</p></div>
<div class="m2"><p>بر مسافر نبود روزه به حکم داور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز این بلد ما وتو تجار صفت روآریم</p></div>
<div class="m2"><p>به حبش یا به ختن یا به ختا یا کشمر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غرض این است کز اینجا چو بدان ملک رسیم</p></div>
<div class="m2"><p>همه گویند که ماه رمضان آمد سر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بهر سرمایه تو از سینه وساق آور سیم</p></div>
<div class="m2"><p>من هم از چهره گذارم به بر سیم توزر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر چه سود از زر وسیم من وتو پیدا شد</p></div>
<div class="m2"><p>من هم از چهره گذارم به بر سیم تو زر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر چه سودا از زر وسیم من وتو پیدا شد</p></div>
<div class="m2"><p>من برم ده دوتوده یک ببری حصه اگر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زآنکه سرمایه زر از من بود و سیم از تو</p></div>
<div class="m2"><p>همه دانند که زر هست ز سیم افزونتر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نی خطا گفتم هرگز نخرد زر مرا</p></div>
<div class="m2"><p>شوشه سیم تو را گر که ببیند زرگر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وگر از رنج سفر هست تو را اندیشه</p></div>
<div class="m2"><p>نیست غم دارم تدبیر دگر ز این بهتر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روزها روزه بگیرم ولیکن شب ها</p></div>
<div class="m2"><p>هی بریزیم می از بلبله اندر ساغر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>لیک بیش از دو سه ساغر نتوان خورد که زود</p></div>
<div class="m2"><p>بویش از کام رود رنج خمارش از سر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یا چنین خورد که ازمستی افتاد چنان</p></div>
<div class="m2"><p>که بهوش آمد اندر سحر شام دگر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>روز چون گردد ازخانه به مسجد آئیم</p></div>
<div class="m2"><p>جای گیریم بر زاهدکی افسونگر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گوید از حالت ما دوش به احیا بودند</p></div>
<div class="m2"><p>چه خبر آری کس راست ز سر مضمر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سخن از هیچ نگوئیم جز از صوم و صلوة</p></div>
<div class="m2"><p>مدحت از کس نسرائیم جز از فخر بشر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گه بپرسیم ازو مسئله در باب وضو</p></div>
<div class="m2"><p>که چه سان گشته مقرر به طریق جعفر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گاه گوئیم که در شک میان سه وچار</p></div>
<div class="m2"><p>بازگو آنچه رسیده است ز شرع انور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>باز چون شب شود از مسجد درخانه رویم</p></div>
<div class="m2"><p>باز گیریم به کفجام شراب احمر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>من ننوشم به شب قدر ولی هیچ شراب</p></div>
<div class="m2"><p>تا مگر قدرم قدری شود افزون ز قدر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همه اعمال شب قدر به جا آرم از آنچ</p></div>
<div class="m2"><p>درحدیث است وخبر داده از آن پیغمبر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نیم شب نیز بخوانم به خشوع و به خضوع</p></div>
<div class="m2"><p>آن دعائی که ابو حمزه همی خواندسحر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون که فارغ شوم از خواندن قرآن ونماز</p></div>
<div class="m2"><p>هم پدر را به دعا شاد کنم هم مادر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پس کنم سجده وجز وصل تو ومرگ رقیب</p></div>
<div class="m2"><p>مطلبی دیگر خواهش نکنم از کرکر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به شب عید بیارایم لیکن جشنی</p></div>
<div class="m2"><p>که بود حسرت سلجوق شه و شه سنجر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کاسه در خوان نهم ازکاسه فرق فغفور</p></div>
<div class="m2"><p>پرده بردر کشم از دیبه روی قیصر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مشک سایم چو سر زلف تو اندر هاون</p></div>
<div class="m2"><p>عود سوزم چوخم جعد تو اندر مجمر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ساقی از یک جا با بانگ دف و بربط و نی</p></div>
<div class="m2"><p>هی بریزد زگلوی بط خون کوثر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مطرب از یک سوبا نشئه صهبا به فلک</p></div>
<div class="m2"><p>زهره را گوش کند کر ز نوای مزمر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فکند آن یک بیهوشش اندر دهلیز</p></div>
<div class="m2"><p>غنود این یک سرخوش ز می اندر منظر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>صبح چون گشت بشوئیم رخ وروآریم</p></div>
<div class="m2"><p>به درفخر جهان قدوه ارباب هنر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تهنیت گویم وبنشینم وزآن پس خوانم</p></div>
<div class="m2"><p>مدح شاهنشه دین فاتح خیبر حیدر</p></div></div>