---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>سلطان چرخ دوش چوشد سوی خاورا</p></div>
<div class="m2"><p>روی سپهر شد ز ثریا مجدرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم زهجر ان بت بی مهر ماه چهر</p></div>
<div class="m2"><p>اختر گهی شمرد وگهی ریخت اخترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاهی ز گریه بودم چون ابر نوبهار</p></div>
<div class="m2"><p>گاهی ز ناله گشتم مانندتندرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خون دیده گشت کنارم چولاله سرخ</p></div>
<div class="m2"><p>کردم چو یاد از آن خط سبز چو سعترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی ز ناله دارم نسبت به ققنا</p></div>
<div class="m2"><p>گفتی ز گریه دارم خویشی به کودرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بسکه گریه کردم خود گفتمی مگر</p></div>
<div class="m2"><p>صدچشمه گشته تعبیه درچشم مرمرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناگاه خادم آمدوگفتا نگفتمت</p></div>
<div class="m2"><p>مسپار دل به لاله رخان سمنبرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کز مهر دم زنند وزمردم برنددل</p></div>
<div class="m2"><p>آنگه کنندجور چو بر قحبه شوهرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاری که سال پار بدت روز و شب به بر</p></div>
<div class="m2"><p>ماند یک روان که بود در دوپیکرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امسال کوکجاست که می نایدت به بر</p></div>
<div class="m2"><p>واندر نظر نیاردت از شوکت وفرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با عاشقی چه کار تو را هان بگویمت</p></div>
<div class="m2"><p>بشنوزمن که عشق تو رانیست در خورا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نشنیده ای مگر ز بزرگان که گفته اند</p></div>
<div class="m2"><p>نبود هر آنکه سر بتراشد قلندرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز این گفته شد ز یده مرا خون دل روان</p></div>
<div class="m2"><p>با صدهزار ناله بگفتم به زاورا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دنیا به یک قرار نمانده است غم مخور</p></div>
<div class="m2"><p>تنها همی نه یار من استی ستمگرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>«ناچار هر که صاحب روی نکو بود»</p></div>
<div class="m2"><p>سنگین دل است اگر همه باشد پیمبرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یک چند صبرکن به غم دل که کردگار</p></div>
<div class="m2"><p>خوش دارد از کسی که به غم هست صابرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بنازد ار به وصل وگدازد گرم زهجر</p></div>
<div class="m2"><p>هست اختیار من همه دردست اهورا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بودیم ما وخادم گرم سخنوری</p></div>
<div class="m2"><p>ناگه طراق سندان برخاست از درا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جستم زجا ورفتم وگفتم که کیستی</p></div>
<div class="m2"><p>گفتا منم شناختمش کوست دلبرا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در بر رخش گشودم وگفتم چگونه شد</p></div>
<div class="m2"><p>کت یادازمن آمد بخ بخ ز در درا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ترکی ز دردرآمد خوی کرده می زده</p></div>
<div class="m2"><p>چهرش زتاب باده فروزان چواخگرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رویش به روشنائی چون ماه نخشبا</p></div>
<div class="m2"><p>قدش به دلربائی چون سرو کشمرا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نی نی خطا سرودم چون قدوروی او</p></div>
<div class="m2"><p>نی سرو کشمر ومه نخشب برابرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کی داشت ماه نخشب زلفی چو سنبلا</p></div>
<div class="m2"><p>کی داشت سروکشمر چشمی چوعبهرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه سرو کشمری را بدجامه بر تنا</p></div>
<div class="m2"><p>نه ماه نخشبی را بدتاج بر سرا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هم جامه داشت او به تن از اطلس وحریر</p></div>
<div class="m2"><p>هم تاج داشت او به سر از مشک اذفرا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>افتاده زلف او به سر دوش اوچنانک</p></div>
<div class="m2"><p>زاغی نشسته باشد برشاخ عرعرا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با شام بود موی سیاهش پسر عما</p></div>
<div class="m2"><p>با ماه بود روی سپیدش برادرا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هی هی تنش به نرمی خلاق قاقما</p></div>
<div class="m2"><p>بخ بخ رخش به خوبی سلطان نسترا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زلفش دو صد لطیمه از مشک تبتا</p></div>
<div class="m2"><p>چشمش دوصدقنینه از خمر خولرا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دیدم چوزلف اوست پریشان وبیقرار</p></div>
<div class="m2"><p>کردم یقین که هست جهان دار کیفرا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گوهر به یک تکلم لعلش شود حجر</p></div>
<div class="m2"><p>گیرم دگر ز عمان نارندگوهرا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عنبر به یک تحرک زلفش شودگیاه</p></div>
<div class="m2"><p>گیرم دگر نیارند از بحر عنبرا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بنشاندمش به صدر وفشاندم به آستین</p></div>
<div class="m2"><p>گرد رهش ز روی چوماه منورا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وز روی شادمانی در پیش اوزدم</p></div>
<div class="m2"><p>سر بر زمین کله را بر چرخ اخضرا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون زلف خویش گشت پریشان مرا چودید</p></div>
<div class="m2"><p>گفت ای افغان مگر که به خوابستم اندرا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چونی چه می کنی چه شدت کاینچنین شدی</p></div>
<div class="m2"><p>زرد وضعیف وخسته و رنجور ومضطرا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>میباشدت مگربه میانم میانه ای</p></div>
<div class="m2"><p>کاینسان شده است جسم توچون موی لاغرا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چشمان من مگر به توفرمود کاینچنین</p></div>
<div class="m2"><p>بیمار و زار گردی و بی خواب و بی خورا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خواهی کتاب عشق نویسی مگر که هست</p></div>
<div class="m2"><p>رگها عیان به جسم توچون خط مسطرا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گفتم بدو که برخی جان توجان من</p></div>
<div class="m2"><p>شبهای هجر کرده بدین روزم اکثرا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفتا ز دردهجر نمردی زهی توان</p></div>
<div class="m2"><p>گفتم درآتش است حیات سمندرا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>القصه چون که پاسی از شب گذشت گفت</p></div>
<div class="m2"><p>خیز ومی آر می شودت گر میسرا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زآن می که گر بنوشد یک قطره دیو از او</p></div>
<div class="m2"><p>گردد فرشته طینت وهم حورمنظرا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زآن می که گربنوشد بی دانشی از او</p></div>
<div class="m2"><p>درگل فروبماند تا حشر چون خرا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زآن می که قطره ای خورد ار تیره زاغ از او</p></div>
<div class="m2"><p>گردد خجل ز جلوه او طاوس نرا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زآن می که بوی او رسد ار بر مشام مور</p></div>
<div class="m2"><p>گویدغلام درگه ما بد سکندرا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گفتم تو ساده روئیبا با باده ات چه کار</p></div>
<div class="m2"><p>ساقی مخواه وزومطلب باده دیگرا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>با ساده روئی آور باکی ز مردما</p></div>
<div class="m2"><p>وز باده خواری آخر شرمی ز داورا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ترسم خدا نکرده به یغما رود زتو</p></div>
<div class="m2"><p>تو بار سیم داری و رندان به معبرا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شهزاده گوش هرکه خورد باده می برد</p></div>
<div class="m2"><p>ترسم خبر شوندت از این سر مضمرا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گفتا به خنده پاسخم از لعل شکرین</p></div>
<div class="m2"><p>کای آسمان دانش از انصاف مگذرا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خوردن شراب بهتر یا درزمین مدح</p></div>
<div class="m2"><p>تخم امید کشتن وبردن جفا برا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خوردن شراب بهتر یا همچون این وآن</p></div>
<div class="m2"><p>هر صبح و شام روی نمودن به هر درا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خوردن شراب بهتر یا هر زمان شدن</p></div>
<div class="m2"><p>در کاخ شاهزاده به صد غرچه همسرا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خوردن شراب بهتر یا همچواهل فارس</p></div>
<div class="m2"><p>کردن نفاق وگشتن از ذره کمترا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خوردن شراب بهتر یا همچون غرچگان</p></div>
<div class="m2"><p>ثلث از وظیفه کسر نهادن ز مضطرا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>«از هر چه بگذری سخن دوست خوشتر است»</p></div>
<div class="m2"><p>برخیز ویک دوساغر صهبا بیاورا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>جستم ز جا ورفتم درکوی می فروش</p></div>
<div class="m2"><p>گفتم بدوبه لابه مرا ای تو سروا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مهمانکی عزیز مرا کرده سرفراز</p></div>
<div class="m2"><p>از من شراب ناب طلب کرده ایدرا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بستان به رهن خرقه ودستار را ز من</p></div>
<div class="m2"><p>کاین نیمشب نه سیم مرا هست ونه زرا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اما نه چون شراب شرآبی که می دهی</p></div>
<div class="m2"><p>نیمیش آب باشدو نیم دیگر شرا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از من گرفت خرقه ودستار را به رهن</p></div>
<div class="m2"><p>آورد یک دومینا صهبای احمرا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>آهسته زوگرفتم وهر جا دمان دمان</p></div>
<div class="m2"><p>تا آمدم به خدمت آن نیک مخبرا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گفتم می است هین بستان گفت هان وچنین</p></div>
<div class="m2"><p>نقلی بیار و ز گل فرشی بگسترا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>مشکی بسای چون سر زلفم به هاونا</p></div>
<div class="m2"><p>عودی بسوز چون خم جعدم به مجمرا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آراستم پس آنگه بزمی چنانکه بود</p></div>
<div class="m2"><p>رشک بهشت وحسرت خاقان وقیصرا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شمع وشمامه شاهد و شیرین وشراب</p></div>
<div class="m2"><p>چنگ وچغانه بربط وطنبور ومزمرا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>هم کردمش به جام شراب مروقا</p></div>
<div class="m2"><p>هم بردمش به پیش گلاب مقطرا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پنداشتی که کلبه من دشت چین بود</p></div>
<div class="m2"><p>گر دیده بد به هر جهت از بس معطرا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>کر گشت گوش زهره چنگی در آسمان</p></div>
<div class="m2"><p>بنواخت بسکه مطرب مزمار ومزمرا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ساغر به نای بلبله ناگه دهان گشود</p></div>
<div class="m2"><p>چون طفل شیرخواره به پستان مادرا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>او هی ز من شراب طلب کرد ومن همی</p></div>
<div class="m2"><p>دادم به دست او ز می صاف ساغرا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>او مست گشت از می ومن از دوچشم او</p></div>
<div class="m2"><p>مستی من ز مستی او بود برترا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گفت آلت قمار اگرت هست پیش آر</p></div>
<div class="m2"><p>شطرنج و نرد را بنهادمش در برا</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گفت ار بری تو من ز لبت می دهم دو بوس</p></div>
<div class="m2"><p>ور من برم بخوان تو یکی قطعه از برا</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اما نه همچو شعرم شعری بود کز او</p></div>
<div class="m2"><p>دلها پریش گردد و جانها مکدرا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گفتم مرا تو هر چه کنی شرط حرف نی</p></div>
<div class="m2"><p>اما منم پیاده تو شاه مظفرا</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گر تو بری ز من بستانی به زور حسن</p></div>
<div class="m2"><p>ور من برم نمی شودم بخت یاورا</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>زاین گفته شد چو زلف خود آشفته در غضب</p></div>
<div class="m2"><p>گفت ار بری دهم به خداوند اکبرا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سویم وزیر و بیدق آن شه روان چو کرد</p></div>
<div class="m2"><p>هشتم به پای پیل تن اسبش رخ و سرا</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>آورد رخ چو پیش من آن شاه حسن شد</p></div>
<div class="m2"><p>شاهش ز غصه مات وفکند از سر افسرا</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>برجستم وز شادی در پیش روی او</p></div>
<div class="m2"><p>برداشتم سه چار معلق چو کوترا</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تنگش به بر کشیدم چون جان نازنین</p></div>
<div class="m2"><p>وز لعل او گرفتم هی بوسه بی مرا</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>گفتا مگر که آگهیت از حساب نیست</p></div>
<div class="m2"><p>گفتم حساب چیست در این مرز وکشورا</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گفتا دو بوسه شرط بکردم چه می کنی</p></div>
<div class="m2"><p>گفتم مگر نه دو بهعدد ده شد ایدرا</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گفتا گرفتم اینکه دو ده در عدد شود</p></div>
<div class="m2"><p>بوسیده ای تو اینکم از صد فزون ترا</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گفتم سه چار بوسه فزونتر نکردمت</p></div>
<div class="m2"><p>گر نیست باورت بشماریم از سرا</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>گفتا بیار نرد کز این شاه واین وزیر</p></div>
<div class="m2"><p>یک لخت خون شده است مرا دل به پیکرا</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>از کعبتین حاصل من بود چار و سه</p></div>
<div class="m2"><p>کوکرد پنج خانه خود را مسخرا</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>عشقش چار من شد وحیران که چون کنم</p></div>
<div class="m2"><p>کافتادم از دو خالش ناگه به ششدرا</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>خندان بروی من نظر افکند وگفت هان</p></div>
<div class="m2"><p>چونی کنون بخوان غزلی روح پرورا</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چون این بگفت ناگه از فیض سرمدی</p></div>
<div class="m2"><p>این مطلبم بدیهه بیامد به خاطرا</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تا لعل شکرین تو را دید شکرا</p></div>
<div class="m2"><p>همچون مگس ز حسرت زد دست بر سرا</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>دیگر نبات را نخرد مشتری کنی</p></div>
<div class="m2"><p>یکبار اگر تبسم شیرین چو شکرا</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>امشب که با منی بودم به ز روز عید</p></div>
<div class="m2"><p>شامی که بی توام گذرد صبح محشرا</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>یکدم وصال روی تو خوشتر بود از آنک</p></div>
<div class="m2"><p>از باختر دهند مرا تا به خاورا</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>آئین کند به زیور هر کس که خوبروست</p></div>
<div class="m2"><p>توخوب رو ز رخ کنی آئین به زیورا</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>برخیز تا نشانم بر چشم روشنت</p></div>
<div class="m2"><p>گر حجره چون دلم شده تار ومحقرا</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>زلفت اگر نه قنبر حیدر بود ز چیست</p></div>
<div class="m2"><p>کز ماه کرده بالین وز مهر بسترا</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چون این غزل زمن بشنید آن غزال چین</p></div>
<div class="m2"><p>گفتا که این غزل را گو کیست شاعرا</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>گفتم که خود بدیهه کنون گفتمی بگفت</p></div>
<div class="m2"><p>بالله نایدم ز تو این گفته باورا</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>تو مر نه می نکردی از قافیه ردیف</p></div>
<div class="m2"><p>ت مر نه می نکردی سعتر از سغبرا</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چون شد که ناگهان شدی اینگونه فاضلا</p></div>
<div class="m2"><p>چندی نرفته چون شدی این سان سخنورا</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>گفتم بتا من آنچه بدم هستمی ولیک</p></div>
<div class="m2"><p>این قدر وپایه یافتم از مدح حیدرا</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>شاهی که یافت هستی از هستیش ز نیست</p></div>
<div class="m2"><p>در دهر ما سوی الله از حکمم داورا</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>شاهی که کمترین خدمش راست ننگ وعار</p></div>
<div class="m2"><p>از فر ز تخت سلجق و ازتاج سنجرا</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>شاهی که می توان به یک انگشت برکشد</p></div>
<div class="m2"><p>بر هیئتی دگر دو نه افلاک دیگرا</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>آنکو بر وقارش کوه است چون کها</p></div>
<div class="m2"><p>آنکو بر سخاویش بحر است فرغرا</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>اسماء‌هست هر چه بود اوست معنیا</p></div>
<div class="m2"><p>اعراض هست هر چه بود اوست جوهرا</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>پوشیده ام ز مهرش برجسم جوشنا</p></div>
<div class="m2"><p>بنهاده ام ز عشقش بر فرق مغفرا</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>در روز کین ز سم ستور و صدای کوس</p></div>
<div class="m2"><p>هم کور گردد ار ملک وهم فلک کرا</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>کاری کند به اعدا کآرند هر زمان</p></div>
<div class="m2"><p>یاد از عذاب ومحنت واندوه محشرا</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>هم طبل الرحیل غریود ز ایمنا</p></div>
<div class="m2"><p>هم بانگ الفرار برآید ز ایسرا</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>آنرا که نیست بهره ای از مهر او به عمر</p></div>
<div class="m2"><p>خیرش همه شر آمد و نفعش همه ضرا</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>برخی به نار گوینداز مهر او روند</p></div>
<div class="m2"><p>بالله من نمی کنم این قصه باورا</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>طلق ار به تن بمالی آتش نسوزدت</p></div>
<div class="m2"><p>مهر علی مگر بود از طلق کمترا</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>آنرا کهذره ای بود از مهر او به دل</p></div>
<div class="m2"><p>یاقوت سان نسوزد جسمش ز آذرا</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>ای شاه دین پناه که در وصف تو نبی</p></div>
<div class="m2"><p>فرمود شهر علمم و باشد علی درا</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>یک حرف از صفات تو نتوان نوشت اگر</p></div>
<div class="m2"><p>دریا شود مداد ونه افلاک دفترا</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>هم با اعانت تو شود مور ضعیفا</p></div>
<div class="m2"><p>هم با اهانت تو شود باز کوترا</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>خشم تو است مرگ شود گر مجسما</p></div>
<div class="m2"><p>لطف تو است عمر شود گر مصورا</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>شاخ شجر به مدح تو گردیده ناطقا</p></div>
<div class="m2"><p>قطره مطر به وصف تو گردیده جانورا</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>لطف تو گرنه شامل حال مسیح بود</p></div>
<div class="m2"><p>هرگز نمی شدی ز دمش زنه عاذرا</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>پهلو زمهر تو خالی نمی کنم</p></div>
<div class="m2"><p>پهلویم ار چودارا گردد به جمدرا</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>تازم به دشت عشق تو کو بار ناچخا</p></div>
<div class="m2"><p>پویم به راه مهر توکو روی کلمرا</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>آن راکه نیست مهر تو در دل به روزگار</p></div>
<div class="m2"><p>هر مو که باشدش به تن آید چو نشترا</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>بر پا چگونه گشتی طاق سپهر اگر</p></div>
<div class="m2"><p>در کشور وجود نبودی غلیگرا</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>تا داردم ز مهر تو فانوس شمع دل</p></div>
<div class="m2"><p>غم نی وزد هر آنچه ز آفات صرصرا</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>من مست جام عشق توام نیست حاجتم</p></div>
<div class="m2"><p>روز جزا به چشمه تسنیم و کوثرا</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>دارم امید از کرمت اینکه روز حشر</p></div>
<div class="m2"><p>از آتش جحیم نگردم محررا</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>دارم امید دیگر کز لطف بی شمر</p></div>
<div class="m2"><p>آنکو مراست خواجه تو را هست چاکرا</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>آنکو تو را سمی شد و زاین فخر می سزد</p></div>
<div class="m2"><p>بر نه فلک اگر کند از رتبه تسخرا</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>آنکو بود به حکمت ارسط و فلاطنا</p></div>
<div class="m2"><p>آنکو بود به طاعت سلمان وبوذرا</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>هم داریش نگاه ز هر رنج و محنتا</p></div>
<div class="m2"><p>هم باشیش پناه به هر فتنه وشرا</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>مدح تو خوانده است به هر روز و هر شبا</p></div>
<div class="m2"><p>وصف تو گفته است به محراب ومنبرا</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>شعرم هزار طعنه زند بر به گوهرا</p></div>
<div class="m2"><p>طبعم به بحر مدح تو تا شد شناورا</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>ای آنکه پیش قدر تو الوند شدکها</p></div>
<div class="m2"><p>وی آنکه نزد فضل تو اروند شد لرا</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>بس قرنها گذشتن باید به روزگار</p></div>
<div class="m2"><p>تا آورد دگر چو تو فرزند مادرا</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>تو یونسی وحوت است این خاکدان ریو</p></div>
<div class="m2"><p>تو یوسفی وچاه است این دار ششدرا</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>خصم تو را چه حاجت خنجر زدن بود</p></div>
<div class="m2"><p>کاندر تن است هر سر مویش چو خنجرا</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>وصف تو را چنانکه توئی چون کنم خیال</p></div>
<div class="m2"><p>کز هر چه آیدم به خیالی فزونترا</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>آرند آب وآتش اگر داوری به تو</p></div>
<div class="m2"><p>خصمی دگر بهم نکنند آب و آذرا</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>زاین پس ز بیم تو نشود طالع آفتاب</p></div>
<div class="m2"><p>از ابر تا به سر نکشد تیره چادرا</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>صدرا سپهر قدرا ای آنکه شخص تو</p></div>
<div class="m2"><p>در ملک فضل و دانش آمد خدیورا</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>غمهای روزگار مرا در دل ای فغان</p></div>
<div class="m2"><p>ستخوان شده است همچو در انگور تکترا</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>جز تو کسی نبینم دانا که در جهان</p></div>
<div class="m2"><p>از درد خود شوم بر او خویش کیورا</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>در ذلت است هر کس به آهنگ وهش بود</p></div>
<div class="m2"><p>با عزت است هر کس باشد لتنبرا</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>عاجز بود ز چاره این دردها مسیح</p></div>
<div class="m2"><p>هم چاره ای کند مگر الطاف کرکرا</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>آمد بلنداقبال اخرس به وصف تو</p></div>
<div class="m2"><p>هم اخرس است هر که ازو هست اشعرا</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>شعرم چو شعر یار چه غم گر بود پریش</p></div>
<div class="m2"><p>کاشفته دل نگشته جز آشفته دیگرا</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>آن به که بر دعای تو ختم سخن کنم</p></div>
<div class="m2"><p>تا کس نگویدم که فلانی است مهمرا</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>تا مشک آورند ز چین و در از عدن</p></div>
<div class="m2"><p>تا عنبر از تتار و دیبه هم از ملک ششترا</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>تا هست چار عنصر ونه چرخ و هشت خلد</p></div>
<div class="m2"><p>تا هست تیر ومشتری و زهره وخورا</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>باشد سپید تا تن دلدار همچو سیم</p></div>
<div class="m2"><p>زرد است تا که چهره عشاق چون زرا</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>یار تو رامباد به جز عیش همدما</p></div>
<div class="m2"><p>خصم تو را مباد به جز دردغمخورا</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>غم نیست گر که قافیه بعضی مکرر است</p></div>
<div class="m2"><p>نیکوتر است آری قند مکررا</p></div></div>