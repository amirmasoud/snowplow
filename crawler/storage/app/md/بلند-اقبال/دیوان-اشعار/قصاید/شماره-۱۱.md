---
title: >-
    شمارهٔ  ۱۱
---
# شمارهٔ  ۱۱

<div class="b" id="bn1"><div class="m1"><p>سال پارم بود یاری ماهروی ومهربان</p></div>
<div class="m2"><p>دشمن دین آفت دل شور تن آشوب جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با لب ومژگان وچشم وزلف وروی وقد او</p></div>
<div class="m2"><p>دل مرا آسوده بود از سیر باغ وبوستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پهن تر روش از سپر پر چین ترش زلف ازکمند</p></div>
<div class="m2"><p>راست تر قدش ز تیر ابروش خم تر از کمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفت یک نخشب از رخسار چونماه منیر</p></div>
<div class="m2"><p>غارت یک کشمر از بالای چون سرو روان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قد دلبندش چو طوبی لعل نوشش سلسبیل</p></div>
<div class="m2"><p>خوی سوزانش چو دوزخ روی نیکویش جنان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می نمود از زلف پرچین رخنه اندر دل مرا</p></div>
<div class="m2"><p>پهلوانی بین که می کرد از زهر کار سنان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر رخم هر گه نظر می کرد می خندید و من</p></div>
<div class="m2"><p>باورم می شد که بخشد خنده حاصل زعفران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در قصب می هشت گاهی کوه کانیستم سرین</p></div>
<div class="m2"><p>بر کمر می بست گاهی موی کانیستم میان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاه می گفتا که گر دور از تومانم یک نفس</p></div>
<div class="m2"><p>هم بگردم جان نژند وهم بمانم دل نوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نمی بینم تو را گریم ز انده ابروار</p></div>
<div class="m2"><p>چون تو را بینم شوم خندان زشادی برق رسان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر کسی شعری ز من در پیش اوخواندهمی</p></div>
<div class="m2"><p>آفرین گفتی وگردیدی ز حیرت لب گزان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور کسی از اسم ورسم من زوی جویا شدی</p></div>
<div class="m2"><p>گفت کاین شاهی است در اقلیم دانش حکمران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وصف او نتوان که وصف اوست افزون از حدیث</p></div>
<div class="m2"><p>مدح او نتوان که مدح اوست بیرون از بیان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دارد از اشعار بس اعجاز گردعوی کند</p></div>
<div class="m2"><p>کز سخن سنجی منم پیغمبر آخر زمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روز روشن گفتم ارحالی بود تاریک شب</p></div>
<div class="m2"><p>کرد تصدیق ار چه بد خورشید اندر آسمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتمش روزی بشوخی کای نگار سنگدل</p></div>
<div class="m2"><p>بی وفائی تا به کی با ما کنی فرمود هان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بی وفا خوانی مرا بالله نباشم این چنین</p></div>
<div class="m2"><p>سنگدل دانی مرا هرگز نبودم آنچنان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با منش پیوسته بودی در نهان وآشکار</p></div>
<div class="m2"><p>مهرهای بی حساب ولطف های بیکران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گاه و بیگه الغرض اندر بر من داشت جای</p></div>
<div class="m2"><p>بودمی پنداشتی اندر دو تن مان یک روان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کاسه ام از می چوخالی گشت و از زر کیسه ام</p></div>
<div class="m2"><p>هفته ای نگذشته دیدم دارد آن مه سرگران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حال اگر از اسم ورسم من ز وی جویا شوند</p></div>
<div class="m2"><p>گوید این باشد فضولی ناقبولی قلتبان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاعری دارد اشعار وشعر اوچون شعر من</p></div>
<div class="m2"><p>جز پریشانی نبخشد هیچ حاصل درجهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر کجا بنشسته میری باشد آنجا پیشکار</p></div>
<div class="m2"><p>هر کجا گسترده خوانی باشد آنجا میزبان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هفته باشد کنون کز هجر آن بی مهر ماه</p></div>
<div class="m2"><p>زحمتی بینم که دید اسفندیار از هفت خوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>روزی از بس شوق دیدار رخش را داشتم</p></div>
<div class="m2"><p>جستم از جا وبرفتم تاش بوسم آستان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون بدیدم روی اوکردم سلام وگفتمش</p></div>
<div class="m2"><p>دارد از آفات دوران کردگارت درامان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لحظه ای ننشسته دیدم گشت با انده قرین</p></div>
<div class="m2"><p>لمحه ای نگذشته دیدم گشت با غم توأمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روی درهم کرد یعنی آمدی پیشم چرا</p></div>
<div class="m2"><p>برجبین افکندچین یعنی برو اینجا ممان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چهره ام از بس ز خجلت زرد شد ترسیدمی</p></div>
<div class="m2"><p>کم بساید هندوئی برجبهه جای زعفران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفتم ای شوخ از چه با اندوه وغم گشتی قرین</p></div>
<div class="m2"><p>مشتری را با زحل افتاده پنداری قران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفت با انده قرینم با توام چون همنشین</p></div>
<div class="m2"><p>گفت با غم توأمانم با توام تا همعنان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تومرا باشی چو مرگ ومن تو را هستم چوعمر</p></div>
<div class="m2"><p>مرگ می دانی توخود از عمر نگذارد نشان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تویکی راغی که وصل من تو را شد فرودین</p></div>
<div class="m2"><p>من یکی باغم که روی تو مرا شد مهرگان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مر مرا ننگ است وعار از همنشینی با چوتو</p></div>
<div class="m2"><p>من امیری کامرانم توفقیری ناتوان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو ثوابت چه که با چون من بگردی مقترن</p></div>
<div class="m2"><p>من گناهم چه که با چون توبجویم اقتران</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چیست عصیان من آخر تا ز روی چون توئی</p></div>
<div class="m2"><p>کردگارم گویداندر نار دوزخ کن مکان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>توکجا ومن کجا روزاز محبت دم مزن</p></div>
<div class="m2"><p>ماکجا و توکجا خیز اینقدر افسون مخوان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>طالب یوسف اگر هستی عزیز مصر شو</p></div>
<div class="m2"><p>ورنه نتوانی خریدش با کلاف ریسمان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گفتم ای ترک ستمگر اینهمه توسن متاز</p></div>
<div class="m2"><p>گفتم ای شوخ دل آزار اینقدر مرکب مران</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>محوت از خاطر مگرگردیده گفتار نبی</p></div>
<div class="m2"><p>کش گرامی دار کافر باشدت گر میهمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دلبرا شوخا جفا کارا دل آزارا بتا</p></div>
<div class="m2"><p>ای که در اقلیم حسنی دلبران را قهرمان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چون سپند از جا نمی جستم به عزم دیدنت</p></div>
<div class="m2"><p>گر که دانستم زنی آتش مرا در دودمان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ناصم گفتا عنان دل مده در دست کس</p></div>
<div class="m2"><p>پند او نشنیده به دست توعنان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خوب کردی هر چه بد کردی ز بی مهری به من</p></div>
<div class="m2"><p>خود به خود کردم سزای من نبود آری جز آن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز آتش خود سوخت جسم وجان من آری چنار</p></div>
<div class="m2"><p>برفروزد آتشی از خویش و سوزد ناگهان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>منکه رو رزم چون بر رخش می گشتم سوار</p></div>
<div class="m2"><p>کس دگر از رستم دستان نگفتی داستان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آنچنان از دردعشقت گشته ام زار ونزار</p></div>
<div class="m2"><p>کاوفتم بر ره شود باد صبا هرگه وزان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بند از بندم چونی با تیغ اگر سازی جدا</p></div>
<div class="m2"><p>از توحاشا کز دل خونین من خیزد فغان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>با بلنداقبال لیکن کم جفا کن زآن بترس</p></div>
<div class="m2"><p>کز توآرد شکوه بر درگاه شاه راستان</p></div></div>