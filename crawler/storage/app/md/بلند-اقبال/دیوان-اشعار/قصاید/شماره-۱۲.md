---
title: >-
    شمارهٔ  ۱۲
---
# شمارهٔ  ۱۲

<div class="b" id="bn1"><div class="m1"><p>نیست به جز ظلم کار صاحب دیوان</p></div>
<div class="m2"><p>لعنت حق بر شعار صاحب دیوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاحب دیوان وحکمرانی در فارس</p></div>
<div class="m2"><p>بخت عجب گشته یار صاحب دیوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر وزبر فارس گر شود عجبی نیست</p></div>
<div class="m2"><p>از ستم بی شمار صاحب دیوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بشود کس دچار گرگ بیابان</p></div>
<div class="m2"><p>به که بگردد دچار صاحب دیوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ به جز غل وغش به دست نیارد</p></div>
<div class="m2"><p>هرکه بسنجد عیار صاحب دیوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه گدا شد به فارس سیم وزرش را</p></div>
<div class="m2"><p>باخته اندر قمار صاحب دیوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن درکی را که وصف اوست به قرآن</p></div>
<div class="m2"><p>هست بلاشک مزار صاحب دیوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس فتد ار در جوار شمر به دوزخ</p></div>
<div class="m2"><p>به که بود در جوار صاحب دیوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کس فتد ار درجوار شمر به دوزخ</p></div>
<div class="m2"><p>به که بود در جوار صاحب دیوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه دل افکار بود گفتمش از کیست</p></div>
<div class="m2"><p>گفت که هستم فکار صاحب دیوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیچ قراریش برقرار نباشد</p></div>
<div class="m2"><p>دل ندهی برقرار صاحب دیوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خورده خوانین ز بسکه حکم نمایند</p></div>
<div class="m2"><p>رفته ز کف اختیار صاحب دیوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر بخورد خون حیض مادر خود را</p></div>
<div class="m2"><p>به که خورد کس نهار صاحب دیوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ظلم بدین سان به اهل فارس نمی کرد</p></div>
<div class="m2"><p>فارس نبود ار دیار صاحب دیوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گمره از آن شد کشند خورده خوانین</p></div>
<div class="m2"><p>بسکه ز هر سو مهار صاحب دیوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تیره شب خلق را ز پی سحر آید</p></div>
<div class="m2"><p>شام شود گر نهار صاحب دیوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چشم بپوشیده شه ز مملکت فارس</p></div>
<div class="m2"><p>رفته پی اعتبار صاحب دیوان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صاحب دکان وملک نیست دگر کس</p></div>
<div class="m2"><p>گشت جمیعا نثار صاحب دیوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا ز می مرگ اجل به اونچشاند</p></div>
<div class="m2"><p>کی رود از سر خمار صاحب دیوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فارس ندانم چرا نسوخت سراسر</p></div>
<div class="m2"><p>از تف سوزنده نار صاحب دیوان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فارس سراسر اگر خراب بگردد</p></div>
<div class="m2"><p>نیست غمی بر زهار صاحب دیوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اینکه گذارند بدعتی همه دم هست</p></div>
<div class="m2"><p>صدچوعمر دستیار صاحب دیوان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خیر نبیندبه عمر خویش الهی</p></div>
<div class="m2"><p>هرکه بود دوستدار صاحب دیوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حاجتم این است از خدا که به زودی</p></div>
<div class="m2"><p>تیره شود روزگار صاحب دیوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>امن وامان است ملک فارس ولیکن</p></div>
<div class="m2"><p>این نبود ز اقتدار صاحب دیوان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>معتمدالدوله نظم داده که سالم</p></div>
<div class="m2"><p>آمده و رفته بار صاحب دیوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هرکه چو منصور حرف حق زده بنگر</p></div>
<div class="m2"><p>کالبدش را به دار صاحب دیوان</p></div></div>