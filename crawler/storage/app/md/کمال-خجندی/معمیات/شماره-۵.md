---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>آنکه در حسن مه چهارده بود</p></div>
<div class="m2"><p>دی شنیدم که بپیوست بسی</p></div></div>