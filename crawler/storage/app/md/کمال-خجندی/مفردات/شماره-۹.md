---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ز چیست قهقه شیشه‌های مِی دانی؟</p></div>
<div class="m2"><p>به ریش محتسب شهر می‌کند خنده</p></div></div>