---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>این تکلف‌های من در شعر من</p></div>
<div class="m2"><p>کلمینی یا حمیرای من است</p></div></div>