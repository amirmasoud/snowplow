---
title: >-
    شمارهٔ ۸۴۰
---
# شمارهٔ ۸۴۰

<div class="b" id="bn1"><div class="m1"><p>خواجه چرا نشسته خیز که رفت کاروان</p></div>
<div class="m2"><p>بار به بند و شو توهم در پی کاروان روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نصر آمل چه میکنی روضه دلگشا بین</p></div>
<div class="m2"><p>کلیة قر خوشتر از شاه نشین خسروان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریخت بهار زندگی برگ خود و تو بیخیر</p></div>
<div class="m2"><p>بر سر گل چو نرگسی مست شراب ارغوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفسی که کوه برکند مرد خدا بیفکند</p></div>
<div class="m2"><p>پنجه شیر بشکند زور هزار پهلوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزه گرفته پارسا ورد چه خواند و دعا</p></div>
<div class="m2"><p>گرسنه سه روزه را بر سر خوان بگره بخوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر حریص باشد و هست ز حرص پیریست</p></div>
<div class="m2"><p>اینکه به جنت آئی و باز شوی ز سر جوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چیست کمال جنت عدن که نگذره از آن</p></div>
<div class="m2"><p>از همه میتوان گذشت از در او نمیتوان</p></div></div>