---
title: >-
    شمارهٔ ۸۶۶
---
# شمارهٔ ۸۶۶

<div class="b" id="bn1"><div class="m1"><p>طوطی ب نو دید و در افتاد در سخن</p></div>
<div class="m2"><p>برد از دهان ننگ نو ننگ شکر سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فندق تو هیچ نخیزد به جز نبات</p></div>
<div class="m2"><p>در پسته تو هیچ نگنجد مگر سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اول حدیث روی نو گویند بلبلان</p></div>
<div class="m2"><p>بر شاخسار گل چو در آیند در سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با آمل عشق عادت تو تلخ گفتن است</p></div>
<div class="m2"><p>آری چو از لب تو ندارد خبر سخن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل را به پیش لعل تو قلب است نقد جان</p></div>
<div class="m2"><p>تا همچو سکه با تو نگوید بزر سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر باد رفت عمر عزیز آخر ای صبا</p></div>
<div class="m2"><p>در پیش آن نگار بگوی این قدر سخن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقصود گفت و گوی کمال از میان تویی</p></div>
<div class="m2"><p>گفت آنچه داشت با تو نگوید دگر سخن</p></div></div>