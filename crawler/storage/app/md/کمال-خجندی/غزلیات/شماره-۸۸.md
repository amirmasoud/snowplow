---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>ای که از زلف توخون در جگر مشک ختاست</p></div>
<div class="m2"><p>روی زیبای تو آنینه الطاف خداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه را روشنی از روی تو میباید جست</p></div>
<div class="m2"><p>سرو را راستی از قد تر می باید خواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر رخسار تو سوزیست که درجان من است</p></div>
<div class="m2"><p>خط سبز تو غباریست که در خاطر ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو ای سرو خرامان ننشینی از پای</p></div>
<div class="m2"><p>ای بسا فتنه که از قد تو بر خواهد خواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو لاله دل من سوخته خون جگرست</p></div>
<div class="m2"><p>که چرا سنبل گیسوی تو در دست صباست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو صبع از اثر مهر رخت جان بدهد</p></div>
<div class="m2"><p>هر که را در ره عشق تو دم از صدق وصفاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچنان زار و نزارست ز سوز تو کمال</p></div>
<div class="m2"><p>که چو ماه نو از ابروی تو انگشت نماست</p></div></div>