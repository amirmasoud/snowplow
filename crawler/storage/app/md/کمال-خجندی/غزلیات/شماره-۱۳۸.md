---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>در سر زلف تو تنها به دل شیدا رفت</p></div>
<div class="m2"><p>و دل هر دو به هم در سر این سودا رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت دل بکنه چون باد در آن حلقه زلف</p></div>
<div class="m2"><p>شب تاریک زهی دل که چنین تنها رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سر زلف تو دوشینه حکایات دراز</p></div>
<div class="m2"><p>همه گفتند ولی باد صبا تنها رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر درت گرچه زدم خاک به چشمان رقیب</p></div>
<div class="m2"><p>حیف از آن سرمه که در دیده نابینا رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانة خال به بالای لبت دانی چیست؟</p></div>
<div class="m2"><p>زین دل سوخته دودی است که بر بالا رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی ننموده به یک زاهد و میخواره هنوز</p></div>
<div class="m2"><p>از تو در صومعه و میکده صد غوغا رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سماعی که غزلهای تو خواندند کمال</p></div>
<div class="m2"><p>صوفیان را همه از سر هوس حلوا رفت</p></div></div>