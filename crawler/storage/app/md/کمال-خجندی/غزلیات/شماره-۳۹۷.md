---
title: >-
    شمارهٔ ۳۹۷
---
# شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>دگر گفتی نجویم بر تو بیداد</p></div>
<div class="m2"><p>مبارک مرد و آنگه کردی آزاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه منت باشد از میاد بیرحم</p></div>
<div class="m2"><p>که پای مرغ بسمل کرده بگشاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه حاصل زآنکه شیرین از لب خویش</p></div>
<div class="m2"><p>پس از کشتن دهد حلوای فرهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فراموشم نخواهی شد چو الحمد</p></div>
<div class="m2"><p>در آن دم که به تکبیر آوری باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بادت می فرستم خدمت و باز</p></div>
<div class="m2"><p>نمی خواهم که بر تو بگذرد باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شدم خاک و به هر سو برد بادم</p></div>
<div class="m2"><p>کسی کز دوست دور افتد چنین باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال از خون دل تر سازه نامه</p></div>
<div class="m2"><p>سلام خشک چون نتوان فرستاد</p></div></div>