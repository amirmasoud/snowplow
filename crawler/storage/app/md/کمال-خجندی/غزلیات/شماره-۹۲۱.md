---
title: >-
    شمارهٔ ۹۲۱
---
# شمارهٔ ۹۲۱

<div class="b" id="bn1"><div class="m1"><p>ای دل ریش من از جور تو غمگین گونه</p></div>
<div class="m2"><p>لبت از خوردن خونم شده رنگین گونه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه بر خاک ره انداخته بشکسته دلم</p></div>
<div class="m2"><p>چون سر زلف خودم سات مشکین گونه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو بلبل من و بیداری و صد گونه خروش</p></div>
<div class="m2"><p>تا که باشد گل رخسار تو با این گونه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرسد قند به شیرینی لبهای نو لیک</p></div>
<div class="m2"><p>به دهانت چو رسیده شده شیرین گونه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرخروئی بودم پیش رقیبان همه وقت</p></div>
<div class="m2"><p>که به خون رنگ دهی اشک مرا زین گونه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه هم رندم و هم رند ستا شکر خدا</p></div>
<div class="m2"><p>که نیم باری ازین زاهد خود بین گونه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر ورق ریخت مگر سرخی اشک تو کمال</p></div>
<div class="m2"><p>که سخنهاست به دیوان تو چندین گونه</p></div></div>