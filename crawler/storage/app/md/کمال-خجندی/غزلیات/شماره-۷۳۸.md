---
title: >-
    شمارهٔ ۷۳۸
---
# شمارهٔ ۷۳۸

<div class="b" id="bn1"><div class="m1"><p>رویت گل سیراب نگوییم چه گوییم</p></div>
<div class="m2"><p>آن لب شکر ناب نگوییم چه گوییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن زلف کمندافکن و رخسار و جبین را</p></div>
<div class="m2"><p>دزد و شب مهتاب نگوییم چه گوییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تفسیر دو ابروی تو کان سورهٔ نون است</p></div>
<div class="m2"><p>پیوسته به محراب نگوییم چه گوییم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدیم شبی زلف تو در فقه و بیداد</p></div>
<div class="m2"><p>تعبیر چنین خواب نگوییم چه گوییم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چوگان سر زلف ترا شد دل ما گوی</p></div>
<div class="m2"><p>این که به احباب نگوییم چه گوییم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این چهره و این اشک روان را به دویدن</p></div>
<div class="m2"><p>لوح زر و سیماب نگوییم چه گوییم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نمد کمال از غزل آن صورت زیباست</p></div>
<div class="m2"><p>شعر تر چون آب نگوییم چه گوییم</p></div></div>