---
title: >-
    شمارهٔ ۶۷۳
---
# شمارهٔ ۶۷۳

<div class="b" id="bn1"><div class="m1"><p>زاهد شهرم ز رندی می کند هر دم سؤال</p></div>
<div class="m2"><p>ساقیا میده که در سر ندارد جز خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نالهٔ دلسوز و آو خستگان بی درد نیست</p></div>
<div class="m2"><p>ای که دردی نیست باری از پی دردی بنال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلوت وصل است درگیر ای چراغ صبحدم</p></div>
<div class="m2"><p>تا نیابد شمع راهی در شبستان وصال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بارها در حیرت باد سحرگاهم که چون</p></div>
<div class="m2"><p>با چنان بی طاقتی باید در آن حضرت مجال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان به رویت همچو گل گفتم برافشانم روان</p></div>
<div class="m2"><p>زآن همی ترسم که طبع نازکت گیر ملال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با چنین بخت پریشانی که در طالع مراست</p></div>
<div class="m2"><p>دولت وصل تو می خواهی زهی فکر محال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر تو روزی از سگان کوی خود خوانی مرا</p></div>
<div class="m2"><p>خود همین باشد کمال دولت و بخت کمال</p></div></div>