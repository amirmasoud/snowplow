---
title: >-
    شمارهٔ ۲۹۱
---
# شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>ز من که عاشق و رندم مجری زهد و صلاح</p></div>
<div class="m2"><p>که روز مستم و شب هم زهی صباح و رواح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قبه و واعظ ما را که بحر علم نهند</p></div>
<div class="m2"><p>همان حکایت کالبحردان و کاملاح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا که نیست صلاحیت نظر بازی</p></div>
<div class="m2"><p>در آن نظر بود ار خوانمت ز اهل صلاح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پرتو رخ تو آفتاب را چه فروغ</p></div>
<div class="m2"><p>علی الخصوص چراغی که بر کنی به صباح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهوش رغز نظرها که در شریعت عشق</p></div>
<div class="m2"><p>گرفته اند تماشای روی خوب مباح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمان حادثه ساقی بریز می در جام کمال</p></div>
<div class="m2"><p>چو باد فتنه وزد در زجاج به مصباح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتسب آمد به جنگ خیز نو نیز</p></div>
<div class="m2"><p>به باده غسل برآور که الوضوء سلاح</p></div></div>