---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>شوخ چشمی خان و مان ما به یغما برد و رفت</p></div>
<div class="m2"><p>دید عقل و دل بر ما هر دو یکجا برد و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر ما خاکیان از غیب آمد ناگهی</p></div>
<div class="m2"><p>همچو جان تنها و هوش از جمله تنها برد و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواستم زلفش گرفتن از سر دیوانگی</p></div>
<div class="m2"><p>او زما دیوانه تر زنجیر در پا برد و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در درون آمد خیال روی او شد عقل و هوش</p></div>
<div class="m2"><p>بود دزدی با چراغ انواع کالا برد و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردم نظارگی را اشکم از هر مو ربود</p></div>
<div class="m2"><p>هرچه میدیدم به ساحل موج دریا برد و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقی روزی به صف واعظ ما پا نهاد</p></div>
<div class="m2"><p>یک به یک انگشت های پاش سرما برد و رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نشاند بر قد و بالاش نقد خود کمال</p></div>
<div class="m2"><p>جان علوی را ز پستی سوی بالا برد و رفت</p></div></div>