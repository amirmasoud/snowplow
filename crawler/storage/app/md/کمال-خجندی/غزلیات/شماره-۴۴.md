---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>گر به جستن باف گشتی یار ما</p></div>
<div class="m2"><p>غیر جویانی نبودی کار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شدی دیدار او دیدن به خواب</p></div>
<div class="m2"><p>خواب جستی دیده بیدار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به داغش سینه زخمی بافتی</p></div>
<div class="m2"><p>یافتی مرهم دل افگار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس دوای ما و درد ما نیافت</p></div>
<div class="m2"><p>چند می جوید طبیب آزار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان و سر در حلقه سودای او</p></div>
<div class="m2"><p>گر به هیچ ارزد زهی بازار ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر حکایت کز لب او می کنیم</p></div>
<div class="m2"><p>بوی جان می آید از گفتار ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بار چون بشنید گفتارت کمال</p></div>
<div class="m2"><p>گفت مولانائی و عطار ما</p></div></div>