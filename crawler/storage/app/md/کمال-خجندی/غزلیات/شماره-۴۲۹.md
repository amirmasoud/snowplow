---
title: >-
    شمارهٔ ۴۲۹
---
# شمارهٔ ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>روی تو به جز آینه دیدن که تواند</p></div>
<div class="m2"><p>زلف تو به جز شانه کشیدن که تواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قند دهنت شربت خاصی که ز لب سخت</p></div>
<div class="m2"><p>دیدن نتوان خاصه چشیدن که تواند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندانکه نوئی آرزوی جان عزیزان</p></div>
<div class="m2"><p>با آروزی خویش رسیدن که تواند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زیر لب از بیم رقیب تو بر آن روی</p></div>
<div class="m2"><p>ما فاتحه خواندیم دمیدن که تواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشاطه دلی داشت چو پولاد به سختی</p></div>
<div class="m2"><p>ورنه ز چنان زلف بریدن که تواند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نیست کمال از سخنان تو گزین تر</p></div>
<div class="m2"><p>کس را به سخن بر تو گزیدن که تواند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنجا که بخوانند بلند این سخنان را</p></div>
<div class="m2"><p>دیگر سخن پست شنیدن که تواند</p></div></div>