---
title: >-
    شمارهٔ ۴۰۲
---
# شمارهٔ ۴۰۲

<div class="b" id="bn1"><div class="m1"><p>دل در طلیت روی به صحرای غم آورد</p></div>
<div class="m2"><p>جان بیدهنت رخت بکوی عدم آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را هوس زلف تو در کوی نو انداخت</p></div>
<div class="m2"><p>حاجی ز پی حلقه قدم در حرم آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محروم مران از در خویشم که گدا را</p></div>
<div class="m2"><p>امید عطا بر در اهل کرم آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی که بسر وقت من آنی همه گویند</p></div>
<div class="m2"><p>شاهیست که در کوی گدائی قدم آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریاد من از غمزه شوخ تو که در دهر</p></div>
<div class="m2"><p>آئین جفا کاری و رسم ستم آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد این سر سودا زده خاک ره آن باد</p></div>
<div class="m2"><p>کز کوی نو جان در تن ما دم بدم آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقش دل و دین شست کمال از ورق جان</p></div>
<div class="m2"><p>تا وصف خط و خال بتان در قلم آورد</p></div></div>