---
title: >-
    شمارهٔ ۱۰۷۸
---
# شمارهٔ ۱۰۷۸

<div class="b" id="bn1"><div class="m1"><p>ما رسی هیچ شب ای مه از وطن جانب ما نیامدی</p></div>
<div class="m2"><p>همچو شهان به مرحمت سوی گدا نیامدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوخت غمم چو از دعا حاجت ما روا نشد</p></div>
<div class="m2"><p>هیکل خویش سوختم چون به دعا نیامدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمده به قصد جان هجر تو کشته شب مرا</p></div>
<div class="m2"><p>درد و دریغ جان من دوش چرا نیامدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست سزای دیدنت دیده بگیر ز آینه</p></div>
<div class="m2"><p>زانکه جمال خویش را جز تو سزا نیامدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سر زلف دلکشت کس نشنید بوی جان</p></div>
<div class="m2"><p>تا به سر شکستگان همچو صبا نیامدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان نفشانده بر در کعبه وصل دلبران</p></div>
<div class="m2"><p>طرف مکن چو از سره صدق و صفا نیامدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست کمال گفتیم این همه درد گرد تو</p></div>
<div class="m2"><p>درد کجا رود دگر چون تو دوا نیامدی</p></div></div>