---
title: >-
    شمارهٔ ۷۴۵
---
# شمارهٔ ۷۴۵

<div class="b" id="bn1"><div class="m1"><p>سر زلف تو کرد آخر به سودایی گرفتارم</p></div>
<div class="m2"><p>که دیگر از پریشانی دمی سر بر نمی‌آرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبیب من علاجی کن به هر حالی که می‌دانی</p></div>
<div class="m2"><p>که پیش چشم تو میرم کزین اندیشه بیمارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به علت برده ام بونی از آن افتاده در دیرم</p></div>
<div class="m2"><p>به زلفت بسته‌ام عهدی از آن دربند زنارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شوق چشم جادویت به ذوق طاق ابرویت</p></div>
<div class="m2"><p>گهی در گوشه مجد گهی در کنج خمارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه جای خرقه ارزق که در میخانه عشقت</p></div>
<div class="m2"><p>به خاک پای خود کآنجا چو زلف خویش نگذارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمال از رندی و مستی چو یک ساعت نشد خالی</p></div>
<div class="m2"><p>ندانم عاقلان از چه سبب خوانند هشیارم</p></div></div>