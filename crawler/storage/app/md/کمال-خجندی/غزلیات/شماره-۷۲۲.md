---
title: >-
    شمارهٔ ۷۲۲
---
# شمارهٔ ۷۲۲

<div class="b" id="bn1"><div class="m1"><p>در دست در درونم درمان آن ندانم</p></div>
<div class="m2"><p>ساقی بیار جامی پ ز زهرو وارهانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پیش بر گرفتم رخت وجود پیش آی</p></div>
<div class="m2"><p>تا یک نفی ببینم روی نو پس نمانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوراست کوی جانان ای باد و من ضعیفم</p></div>
<div class="m2"><p>فریاد جان من رس و آن جایگاه رسانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانم بکاست چون شمع ای باد صبح آخر</p></div>
<div class="m2"><p>از گشتنم چه خواهی من خود ز مردگانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جور من از طبیب است درد من از جیب است</p></div>
<div class="m2"><p>آن غصه با که گویم این قصه با که رانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بند تن دریغ است جان پری نژادم</p></div>
<div class="m2"><p>دیوانه وار ناگه رنجیر بگسلانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حال کمال گفتم یک شمه ای بگویم</p></div>
<div class="m2"><p>چون مهربان ندیدم شهر است بر دهانم</p></div></div>