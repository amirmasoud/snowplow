---
title: >-
    شمارهٔ ۶۵۳
---
# شمارهٔ ۶۵۳

<div class="b" id="bn1"><div class="m1"><p>کسی که دوست ندارد ز جان ندارد حظ</p></div>
<div class="m2"><p>که جسم دور ز جان از جهان ندارد حظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حظر چو کوثر است به خلد آب خضر در ظلمات</p></div>
<div class="m2"><p>رواست گر لب ما زان دهان ندارد حظ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه رنگ از رخ خوبان رقیب را جز جنگ</p></div>
<div class="m2"><p>که باغبان ز گل و گلستان ندارد حظ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکرده زلف به کف زآن ذقن چه بهره برم</p></div>
<div class="m2"><p>که تشنه از چَهْ بی‌ریسمان ندارد حظ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش آیدش که ببیند تنم رنیم و رفات</p></div>
<div class="m2"><p>مگر پریست که جز استخوان ندارد حظ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشان خاک درشه پارسا گرفتم بافت</p></div>
<div class="m2"><p>ز سرمه دیده اعمی چنان ندارد حظ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به زاهد این سخن تره اثر نکرد کمال</p></div>
<div class="m2"><p>درخت خشک ز آب روان ندارد حظ</p></div></div>