---
title: >-
    شمارهٔ ۳۰۰
---
# شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>آن شوخه به ما جز سر بیداد ندارد</p></div>
<div class="m2"><p>با وعده دل غمزده شاد ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرد از من دل شیفت آن عهد شکن باز</p></div>
<div class="m2"><p>آن گونه فراموش که کس باد ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل چه فرستد سوی گل تحفه که در دست</p></div>
<div class="m2"><p>بیچاره به جز ناله و فریاد ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر عهد تو تکیه نتوان کرد و وفا نیز</p></div>
<div class="m2"><p>کین هر دو بنانیست که بنیاد ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دله که نپوشد نظر از گوشه آن چشم</p></div>
<div class="m2"><p>مرغیست که اندیشه صیاد ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو جنگ میاموز بدان غمزه که آن شوخ</p></div>
<div class="m2"><p>در فتنه گری حاجت استاد ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر حال کمال ار نکنی رحم عجب نیست</p></div>
<div class="m2"><p>شیرین ز تجمل سره فرهاد ندارد</p></div></div>