---
title: >-
    شمارهٔ ۵۱۷
---
# شمارهٔ ۵۱۷

<div class="b" id="bn1"><div class="m1"><p>من بر سر آن کر بچه کارم همه دانند</p></div>
<div class="m2"><p>در سر هوس روی که دارم همه دانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رانی چو سگم از در و گونی که بکن عقو</p></div>
<div class="m2"><p>تا حشر من این در نگذارم همه دانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر آه من آن سرو نداند که بلند است</p></div>
<div class="m2"><p>مرغان چمن ناله زارم همه دانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گیرم که به خون زخم بپوشم ز طبیبان</p></div>
<div class="m2"><p>از ناله دل و جان نگارم همه دانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیرم ز بزرگی سگ خویشم شمرد بار</p></div>
<div class="m2"><p>من کیستم و در چه شمارم همه دانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باران اگرت جان و سر آرند بتحقه</p></div>
<div class="m2"><p>من نیز به باران تو بارم همه دانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر خلق ندانند کمال این سخن کیست</p></div>
<div class="m2"><p>چون معنی نو در قلم آرم همه دانند</p></div></div>