---
title: >-
    شمارهٔ ۱۰۰۸
---
# شمارهٔ ۱۰۰۸

<div class="b" id="bn1"><div class="m1"><p>چشم شوخ و دل سنگین بر سیمین داری</p></div>
<div class="m2"><p>خال مشکین رخ رنگین لب شیرین داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چه دانی ز من و حال من ای شمع چگل</p></div>
<div class="m2"><p>که چو من عاشق دل سوخته چندین داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی نیازی و نیازت بمن بیدله نیست</p></div>
<div class="m2"><p>پادشاهی و فراغ از من مسکین داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتة رسم وفا دارم و آئین جفا</p></div>
<div class="m2"><p>آن نداری سر یک موی ولی این داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای صبا نکهت آن زلف مگر نشنیدی</p></div>
<div class="m2"><p>که هوای چمن و برگ ریاحین داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دعوی زنده دلی از تو تکونیست کمال</p></div>
<div class="m2"><p>گر شب فرقت جانان بر بالین داری</p></div></div>