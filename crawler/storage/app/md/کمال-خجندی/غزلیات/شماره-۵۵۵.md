---
title: >-
    شمارهٔ ۵۵۵
---
# شمارهٔ ۵۵۵

<div class="b" id="bn1"><div class="m1"><p>هیچ آن دهان شیرین کس را عیان نباشد</p></div>
<div class="m2"><p>تو کوزه نباتی زانت دهان نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم که سازم از نوه همچون قلم زبانی</p></div>
<div class="m2"><p>نام لب تو بردن حد زبان نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر لوح چهره اشکم خطها کشد به سرخی</p></div>
<div class="m2"><p>زینسان محرر آنرا خط روان نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوزم به آه سینه جانهای دردمندان</p></div>
<div class="m2"><p>تا بر در تو جز من کس جان فشان نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز آن میان نیابد هرگز نشان بجستن</p></div>
<div class="m2"><p>بر تن منبران را از مو نشان نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آه من به بستان دور از تو بر درختان</p></div>
<div class="m2"><p>مرغی نگشته بریان در آشیان نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نتوان کمال بستن طرف از میان خوبان</p></div>
<div class="m2"><p>جان و سری که داری تا در میان نباشد</p></div></div>