---
title: >-
    شمارهٔ ۴۹۹
---
# شمارهٔ ۴۹۹

<div class="b" id="bn1"><div class="m1"><p>ما بساط نیکنامی باز طی خواهیم کرد</p></div>
<div class="m2"><p>خرقه و سجاده رهن نقل و می خواهیم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهد و تقوی سر بسر این نام و این آوازه را</p></div>
<div class="m2"><p>در سر آواز چنگ و بانگ نی خواهیم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوبهارست و جوانی و اوان عاشقی</p></div>
<div class="m2"><p>گر کنون نکنیم ترک توبه کی خواهیم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بزاهد رندی و مستی نمی کردیم فاش</p></div>
<div class="m2"><p>بعد ازین این کارها در پیش وی خواهیم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می چو لیلی گر شود در شهر ما دشوار یاب</p></div>
<div class="m2"><p>ماچومجنون جست وجویش حئ بحی خواهیم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش ما پیکی که آرد مژده اقبال پار</p></div>
<div class="m2"><p>نام آن پیک مبارک نیک پی خواهیم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون رسد در دفتر نهاد نام ما کمال</p></div>
<div class="m2"><p>آن ورق گردان که ما آن نامه طی خواهیم کرد</p></div></div>