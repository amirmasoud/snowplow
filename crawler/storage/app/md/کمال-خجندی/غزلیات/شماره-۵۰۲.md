---
title: >-
    شمارهٔ ۵۰۲
---
# شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>ما را بپای بوسی تو گر دسترس بود</p></div>
<div class="m2"><p>در دولت غم تو همین پایه پس بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سر هوای تست مرا بهترین هوس</p></div>
<div class="m2"><p>باقی هر آنچه هست هوا و هوس بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوسی بر آستان تو داریم التماس</p></div>
<div class="m2"><p>ما را بر آن در از تو همین ملتمس بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی زحمت رقیب دمی با شکر لبی</p></div>
<div class="m2"><p>گر دست میدهد شکری بی مگس بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد اگر قدم ز کرامت نهد بر آب</p></div>
<div class="m2"><p>نزدیک ما به مرتبه کمتر ز خس بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو محتسب ز شحنه مترسان مرا که من</p></div>
<div class="m2"><p>از پادشاه فارغم او خود چه کسی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی زیر لب بمیکده میخواندت کمال</p></div>
<div class="m2"><p>بشنو که مقبلی ز قول نفس بود</p></div></div>