---
title: >-
    شمارهٔ ۴۳۸
---
# شمارهٔ ۴۳۸

<div class="b" id="bn1"><div class="m1"><p>ز خوان وصل تو تا با من گدا چه رسد</p></div>
<div class="m2"><p>به جز جگر به گدایان بینوا چه رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>البته که پر شکرست آن به هیچ کس نرسید</p></div>
<div class="m2"><p>ازان دهان که ز هیچ است که مرا چه رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار تشنه به آبه لبی چو قطره آب</p></div>
<div class="m2"><p>میان آن همه از قطره بما چه رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو کیستی و من ای دل که جرعه زین جام</p></div>
<div class="m2"><p>بصد چو جم نرسد تا من و تا چه رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین که بر سر کوی نو نیغ میبارد</p></div>
<div class="m2"><p>به جز بلا بسر عاشق از هوا چه رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نیزه بازی مژگان شوخ چشمانم</p></div>
<div class="m2"><p>سنان به سینه رسید و هنوز تا چه رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال چون نرسد جز جفا ز اهل وفا</p></div>
<div class="m2"><p>قیاس کن که ز خویان بیوفا چه رسد</p></div></div>