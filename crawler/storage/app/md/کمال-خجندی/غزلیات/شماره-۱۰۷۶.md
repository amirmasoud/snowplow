---
title: >-
    شمارهٔ ۱۰۷۶
---
# شمارهٔ ۱۰۷۶

<div class="b" id="bn1"><div class="m1"><p>هر لحظه بما از نو رسد تحفة دردی</p></div>
<div class="m2"><p>اگر این نبدی عاشق درویش چه خوردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل چاره درد تو به این کرد که خون شد</p></div>
<div class="m2"><p>این چاره نبودی دل بیچاره چه کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میسوخت سراپای وجودم ز دل گرم</p></div>
<div class="m2"><p>گرمی نزدم هر دم ازین غم دم سردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حوران کفن من همه در روی بمالند</p></div>
<div class="m2"><p>با خاک لحد گر برم از کوی تو گردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق بشه فرد یگانه ننشیند</p></div>
<div class="m2"><p>گر نیست چو فرزین ز دو عالم شده فردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو بار سبک روح که بهر دل مجروح</p></div>
<div class="m2"><p>سازیم ز خاک قدمش مرهم دردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چند کمال این همه درمان طلبیدنه</p></div>
<div class="m2"><p>رنجی بر و دردی طلاب از باطن مردی</p></div></div>