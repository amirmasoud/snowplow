---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>خطت چو خضر به آب حیات نزدیک است</p></div>
<div class="m2"><p>به آن لبان چو شکر نبات نزدیک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خاک پای تو سر سبزی ایست سرها را</p></div>
<div class="m2"><p>به این سخن سر زلف دوتات نزدیک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشان کوثر و طوبی که میدهند از دور</p></div>
<div class="m2"><p>به چشم ما وقد دلربات نزدیک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حکایت دل پرخون ما پرسش از جام</p></div>
<div class="m2"><p>که پیش لعل لب جانفزات نزدیک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه گربه کنان دور از آن لبیم و کنار</p></div>
<div class="m2"><p>به چشم تشنه خیال فرات نزدیک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به رخ چگونه ترانه پادوهای سرشک</p></div>
<div class="m2"><p>چنین که شاه دل از غم به مات نزدیک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال جان به لب آورد بر امید وفات</p></div>
<div class="m2"><p>دلش بجوی که وقت وفات نزدیک است</p></div></div>