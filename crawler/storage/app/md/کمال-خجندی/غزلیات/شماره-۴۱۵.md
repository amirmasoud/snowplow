---
title: >-
    شمارهٔ ۴۱۵
---
# شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>دوش در خانه ما ماه فرود آمده بود</p></div>
<div class="m2"><p>خانه روشن شد و دیدیم همو آمده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به بینیم به طلعت میمون فالش</p></div>
<div class="m2"><p>فرعه انداخته بودیم و نکو آمده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نا تمامی مه آنشب همه را روشن شد</p></div>
<div class="m2"><p>که چو آئینه به او روی برو آمده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با خیال لب و آن عارض نازک در چشم</p></div>
<div class="m2"><p>آب دولت همه را باز بجو آمده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می دمید از دم مشکین صبا بوی بهشت</p></div>
<div class="m2"><p>بوی بردیم که او زآن سر کو آمده بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که دیدیم چو چشم و سر زلفش آنجا</p></div>
<div class="m2"><p>مست و آشفته آن سلسله مو آمده بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل دیوانه خود سوخته چون عود کمال</p></div>
<div class="m2"><p>آن پری روی ملک خوی ببو آمده بود</p></div></div>