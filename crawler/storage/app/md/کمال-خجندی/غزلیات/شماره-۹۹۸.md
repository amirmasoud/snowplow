---
title: >-
    شمارهٔ ۹۹۸
---
# شمارهٔ ۹۹۸

<div class="b" id="bn1"><div class="m1"><p>تب چرا درد سر آورد به نازک‌بدنی</p></div>
<div class="m2"><p>که چو گل تاب نیاورد به جز پیرهنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تن نازک او همچو عرق لرزانست</p></div>
<div class="m2"><p>هر کجا هست تر و تازه گلی در چمنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکرش دارد و بادام زیان پنداری</p></div>
<div class="m2"><p>چشم نگشاید از آن روی و نگوید سخنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدن نبض اشارت به مسیحا کردند</p></div>
<div class="m2"><p>گفت حیف است چنان دست بدست چو منی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی رگ زدن از کار بفصاد افتد</p></div>
<div class="m2"><p>نیست استاد تر از غمزة او نیش زنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بفدای تن رنجور نو و جان تو باد</p></div>
<div class="m2"><p>هر کرا هست در ایام تو جانی و تنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صحت جان و تنت چون به دعا خواست کمال</p></div>
<div class="m2"><p>بود آمین به زبان آمده در هر دهنی</p></div></div>