---
title: >-
    شمارهٔ ۸۰۵
---
# شمارهٔ ۸۰۵

<div class="b" id="bn1"><div class="m1"><p>نقد جان چیست که در دامن جانان ریزم</p></div>
<div class="m2"><p>گر بخواهد ز سر هر دو جهان برخیزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی گناه در همه نیغم بزند بار عزیز</p></div>
<div class="m2"><p>ادب آنست که گردن نهم و نستیزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسم باشد که گریزند غلامان ز جفا</p></div>
<div class="m2"><p>من غلام تو چنانم که کشی نگریزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در رحمت بگشایند به رویم فردا</p></div>
<div class="m2"><p>گر بود سلسله زلف تو دستاویزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می برد از شکر ناب به شیرینی دست</p></div>
<div class="m2"><p>با * حدیث لب او نکته درد آمیزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر گناهست چنین روی به دیدن چو کمال</p></div>
<div class="m2"><p>من نه آنم که ازین گونه گنه پرهیزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که خوارزم گرفتم به سخن‌های غریب</p></div>
<div class="m2"><p>نبود میل عراق و هوس تبریزم</p></div></div>