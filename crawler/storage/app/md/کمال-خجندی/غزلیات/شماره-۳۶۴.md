---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>ترا رحمی به آن چشمان اگر باشد عجب باشد</p></div>
<div class="m2"><p>مسلمانی بترکستان اگر باشد عجب باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فقیهم توبه فرماید به شرع مصطفی از تو</p></div>
<div class="m2"><p>ابوجهل اینچنین نادان اگر باشد عجب باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بروز هجر میجویم ترا گریان و می گویم</p></div>
<div class="m2"><p>شب باران مه تابان اگر باشد عجب باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخ رنگین ز مشتی خس بپوشیدی ولی خس را</p></div>
<div class="m2"><p>نجات از آتش پنهان اگر باشد عجب باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلی کز خاک ما روید بجای غنچه های او</p></div>
<div class="m2"><p>از آن نازک بجز پیکان اگر باشد عجب باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شفای جان عاشق نیست الا شربت دردت</p></div>
<div class="m2"><p>طبیبانرا ازین درمان اگر باشد عجب باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال احسنت گو بردی بشیرین کاری از خسرو</p></div>
<div class="m2"><p>چنین طوطی به هندستان اگر باشد عجب باشد</p></div></div>