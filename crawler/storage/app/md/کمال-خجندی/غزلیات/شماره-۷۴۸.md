---
title: >-
    شمارهٔ ۷۴۸
---
# شمارهٔ ۷۴۸

<div class="b" id="bn1"><div class="m1"><p>شب که ز حسرت رخت چشم به ماه کرده ام</p></div>
<div class="m2"><p>سوخته ماه و زهره را سینه چو آه کرده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خور تیغ دیده ام پیش تو فرق خویش را</p></div>
<div class="m2"><p>از تو به آفتاب اگر نیز نگاه کرده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه ز خون کشتگان گشت رقیب سرخ روی</p></div>
<div class="m2"><p>باز منش به درد دل روی سیاه کرده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناصع اگر به بینیم روی به خاک راه او</p></div>
<div class="m2"><p>هیچ مگوی کز تو به روی به راه کرده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود همیشه جان من رسم تو بی گنه کشی</p></div>
<div class="m2"><p>هیچ نمی کشی مرا من چه گناه کرده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط چو دید بر رخت مهر دلم زیاده شد</p></div>
<div class="m2"><p>نام خطت به آن سببه مهر گیاه کرده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچه کمال از آن دو رخ کرد بیان درین غزل</p></div>
<div class="m2"><p>سهل مبین که فکر آن من بدو ماه کرده ام</p></div></div>