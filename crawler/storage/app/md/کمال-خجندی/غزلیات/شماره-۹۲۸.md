---
title: >-
    شمارهٔ ۹۲۸
---
# شمارهٔ ۹۲۸

<div class="b" id="bn1"><div class="m1"><p>ای نبات قد سبزت شکرستان همه</p></div>
<div class="m2"><p>قد شمشادوشت سرو خرامان همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رونق کفر بیفزاید از آن روی که برد</p></div>
<div class="m2"><p>فر خال سیهت رونق بازار همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سر هندوی زلفین نو آن سودائیست</p></div>
<div class="m2"><p>که به تاراج دهد عقل و دل و جان همه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر نظاره که در کوی نو آبند کسان</p></div>
<div class="m2"><p>هست چون گوی سرم در خم چوگال همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای در دامن صبر از چه کنم چون شب و روز</p></div>
<div class="m2"><p>دست عشق نو گرفته ست گریبان همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آبروی من دل سوخته بر باد مده</p></div>
<div class="m2"><p>که حسودی به غرض خام کند نان همه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل باغ وصال تو کمال است اگر</p></div>
<div class="m2"><p>در جهان بسته شود باغ و گلستان همه</p></div></div>