---
title: >-
    شمارهٔ ۶۱۹
---
# شمارهٔ ۶۱۹

<div class="b" id="bn1"><div class="m1"><p>گفتمش نام تو گفتا از مه تابان پرس</p></div>
<div class="m2"><p>گفتمش نام لبت گفت این حدیث از جان بپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش باری نشانی زان دهان با من بگوی</p></div>
<div class="m2"><p>زیر لب خندان شد و گفت از گل خندان بپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش دلها که دزدید آن همه شب با چراغ</p></div>
<div class="m2"><p>خال و خط بنمود و گفت اینها ازین و آن بپرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش در پای تو غلطان سرم چون گو چراست</p></div>
<div class="m2"><p>گفت با زلفم بگو یعنی که از چوگان بپرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش بر سینه ریشم هزاران زخم چیست</p></div>
<div class="m2"><p>گفت گو با غمزه ام یعنی که از پیکان بپرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش در غارت چشمان دلم بردی اسیر</p></div>
<div class="m2"><p>گفت اگر خواهی نشان آن ز ترکستان بپرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش چون پی برد اندر سر زلفت کمال</p></div>
<div class="m2"><p>گفت با باد صبا شو راه هندستان بپرس</p></div></div>