---
title: >-
    شمارهٔ ۸۸۱
---
# شمارهٔ ۸۸۱

<div class="b" id="bn1"><div class="m1"><p>میزند بر آتش دل جوش می آب اینچنین</p></div>
<div class="m2"><p>غم ز دلها میزداید باده ناب اینچنین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صید دلها می کند دلدار با نیر نگاه</p></div>
<div class="m2"><p>دلبران را باشد آری رسم ارعاب اینچنین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ متاب از دوستداران ای نگار سنگدل</p></div>
<div class="m2"><p>بین که از هجر رخت در گشته بی تاب اینچنین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شبی در خواب می بینم که سنگین دلتری</p></div>
<div class="m2"><p>باشد آری عادت بخت گرانخواب اینچنین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که از من گشتی ای دلدار سیمین تن جدا</p></div>
<div class="m2"><p>می چکد بر دامنم از دیده خوناب اینچنین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست ما را در قبال جور جز مهر و وفا</p></div>
<div class="m2"><p>زانکه باشد اهل دل را رسم و آداب اینچنین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاد رویت می کند چون ماه را بیند کمال</p></div>
<div class="m2"><p>می برد لذت به شبها دل ز مهتاب اینچنین</p></div></div>