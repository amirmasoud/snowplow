---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>درآمد از در ارباب خرقه ناگه دوست</p></div>
<div class="m2"><p>برآمد از دل درویش خسته الله دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آفتاب نشست و چراغ ها افروخت</p></div>
<div class="m2"><p>درون خلوت دلها به روی چون مه دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رهگذار دل و دیده سیلهاست ز خون</p></div>
<div class="m2"><p>چگونه بگذرد ای دوستان برین ره دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرت ز ذوق درونی نهفته حالت هاست</p></div>
<div class="m2"><p>گمان مبر که ز خال تو نیست اگه دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگو نشین به دلت درد و ناله چون برخاست</p></div>
<div class="m2"><p>که درد می کند آنجا مقام و آنگه دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مریض عشق به عمر دوباره شد مخصوص</p></div>
<div class="m2"><p>به پرسشی چو قدم رنجه کرد که که دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنند پرسش من دوستان که کبست کمال</p></div>
<div class="m2"><p>درون جان نو بالله جیپ و تالله دوست</p></div></div>