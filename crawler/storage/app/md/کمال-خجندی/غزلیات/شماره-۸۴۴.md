---
title: >-
    شمارهٔ ۸۴۴
---
# شمارهٔ ۸۴۴

<div class="b" id="bn1"><div class="m1"><p>داری لب و دهانی شیرین ولی چه شیرین</p></div>
<div class="m2"><p>بر رخ خطی و خالی مشکین ولی چه مشکین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غارتگریست زلفت ظالم ولی چه ظالم</p></div>
<div class="m2"><p>عاشق کشیست چشمت بیدین ولی چه بیدین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ماه رنگ گیرد هر چیز و اشک ما هم</p></div>
<div class="m2"><p>از عکس آن دو رخ شد رنگین ولی چه رنگین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بینم بهشت شاید در خواب خوش که شبها</p></div>
<div class="m2"><p>دارم ز آستانت بالین ولی چه بالین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیمار بود عاشق آن لب که نوش بادش</p></div>
<div class="m2"><p>از قند ساخت شربت شیرین ولی چه شیرین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آبستن آب آن بر در آب سنگ باشد</p></div>
<div class="m2"><p>در پردلی نرا هم سنگین ولی چه سنگین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خیل دلبرانی سلطان ولی چه سلطان</p></div>
<div class="m2"><p>پیشت کمال بیدل مسکین ولی چه مسکین</p></div></div>