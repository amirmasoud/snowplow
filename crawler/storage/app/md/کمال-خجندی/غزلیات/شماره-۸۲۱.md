---
title: >-
    شمارهٔ ۸۲۱
---
# شمارهٔ ۸۲۱

<div class="b" id="bn1"><div class="m1"><p>ای عادت قدیمت دلهای ما شکستن</p></div>
<div class="m2"><p>بر خود درست کردی عهد و وفا شکستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که پای نازک آزرده سازی از دل</p></div>
<div class="m2"><p>این آبگینه ناکی در زیر پا شکستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طرف دو رخ رها کن تا بشکنیم زلفت</p></div>
<div class="m2"><p>یک آرزو چه باشد در ماهها شکستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بادام و پسته غمزی کردند از آن لب و چشم</p></div>
<div class="m2"><p>چشم و دهان هر یک باید جدا شکستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر بر خط تو دارم همچون قلم چه موجب</p></div>
<div class="m2"><p>راندن بگفت مردم هر دم مرا شکستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صوفی شهر ما را بت شد عصای توبه</p></div>
<div class="m2"><p>در عشق فرض باشد بر وی عصا شکستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش کمال وصلت ملکه در عالم ه ارزد</p></div>
<div class="m2"><p>رسمیست مشتری را اول بها شکستن</p></div></div>