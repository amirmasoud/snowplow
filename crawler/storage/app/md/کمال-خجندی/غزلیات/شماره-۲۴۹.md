---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>مرا از چشم تو نازی نیاز است</p></div>
<div class="m2"><p>به نازی کش مرا چندین چه ناز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم بنواز یعنی سوز و بگداز</p></div>
<div class="m2"><p>که دل مسکین غمت مسکین نواز است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت دارند و خط بیچارگان دوست</p></div>
<div class="m2"><p>که این بیچاره سوز آن چاره ساز است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مده گو لب چو زلف آمد به دستم</p></div>
<div class="m2"><p>که گر روزش نبوسم شب دراز است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لبش نرم گدازد از دم من</p></div>
<div class="m2"><p>که آه سینه سوزم جان گداز است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به رویش واعظا شد سجده واجب</p></div>
<div class="m2"><p>سخن کوتاه کن وقت نماز است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال از زلف او بونی نیابی</p></div>
<div class="m2"><p>گرت از صد سر و جان احتراز است</p></div></div>