---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>مجلس معطرست و به آن وقت ما خوش است</p></div>
<div class="m2"><p>کز خال و روی یار عبیری در آتش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با درد عشق ناله بلانی است سینه سوز</p></div>
<div class="m2"><p>اور مسکین دل ضعیف که دایم بلاکش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داری سر نظاره نشین در سرای چشم</p></div>
<div class="m2"><p>کز اشک سرخ بام و در او منفش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی که ما ز بار کشی بس نمی کنیم</p></div>
<div class="m2"><p>این نکته باز گوی به باران که پس خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد به خستگی سر پیکان او هنوز</p></div>
<div class="m2"><p>صیدی که زخم خورد؛ آن تیر و ترکش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گناه خویش نوشتن فرشته را</p></div>
<div class="m2"><p>در دستش ار معارضه با آن پریوش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طومار زلف یار که شب خوانیش کمال</p></div>
<div class="m2"><p>پیش چراغ خوان که سوادی مشوش است</p></div></div>