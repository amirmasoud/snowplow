---
title: >-
    شمارهٔ ۸۰۴
---
# شمارهٔ ۸۰۴

<div class="b" id="bn1"><div class="m1"><p>نرود نقش خیال تو زمانی ز ضمیرم</p></div>
<div class="m2"><p>خود من ساده درون صورت غیری نپذیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرده ام در هوس آنکه بود فرصت آنم</p></div>
<div class="m2"><p>که نهی پای درین دیده و در پای نو میرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال خود با که بگویم که شکایت ز تو دارم</p></div>
<div class="m2"><p>با خلاص از که بجویم که به دام تو اسیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به قلم صورت اخلاص نوشتن چه ضرورت</p></div>
<div class="m2"><p>چون ضمیر تو بود واقف اسرار ضمیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته حال کمال از غم من در چه نصایست</p></div>
<div class="m2"><p>و چه توان گفت همان عاجز و مسکین و فقیرم</p></div></div>