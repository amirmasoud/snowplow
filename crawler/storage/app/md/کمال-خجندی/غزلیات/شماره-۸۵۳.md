---
title: >-
    شمارهٔ ۸۵۳
---
# شمارهٔ ۸۵۳

<div class="b" id="bn1"><div class="m1"><p>دوستان مرحمتی بر دل بیچاره من</p></div>
<div class="m2"><p>که برفت از بر من بار ستمکاره من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل نهادم من مسکین به هلاک تن خویش</p></div>
<div class="m2"><p>چه کنم در غم او نیست جز این چاره من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وای بر جان من از بی کسی و تنهانی</p></div>
<div class="m2"><p>گر نبودی غم او مونس و غمخوارة من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوس لعل لب او به خرابات مغان</p></div>
<div class="m2"><p>کرد صد باره گرو خرقه صد پاره من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای صبا گر گذر از کوی دلارام کنی</p></div>
<div class="m2"><p>باز پرسی خبری زآن دل آوارة من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارم امروز سر آنکه کتم جانبازی</p></div>
<div class="m2"><p>تا قدم رنجه کند دوست به نظاره من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نیاره به زبان سوز تو چون شمع کمال</p></div>
<div class="m2"><p>خود گواهست بر او گونه رخسارة من</p></div></div>