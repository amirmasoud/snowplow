---
title: >-
    شمارهٔ ۹۸۱
---
# شمارهٔ ۹۸۱

<div class="b" id="bn1"><div class="m1"><p>باز بگذشتی بر آن زلفه ای نسیم مشکبوی</p></div>
<div class="m2"><p>در شب تاریک چون رفتی برآن راه چو موی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش بر لوح رخسار تو بی معنیست خط</p></div>
<div class="m2"><p>گفت خط خالی ز معنی نیست بی معنی مگوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه رقت آن عارض چون آب باز از جوی چشم</p></div>
<div class="m2"><p>چشم آن دارم که آب رفته باز آید بجوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو مثر شبنم عذار لاله و رخسار گل</p></div>
<div class="m2"><p>تا به تو کمتر فروشد حسن هر ناشنه روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ا گر بجوئی در زکات حسن مسکینتر کسی</p></div>
<div class="m2"><p>چون دل من از همه مسکینتر است او را بجوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به بازی زلف او بشکستم و زلفش دلم</p></div>
<div class="m2"><p>بشکند آری به بازی اینچنین چوگان و گوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون ما آن غمزه می‌ریزد به زلف و رخ کمال</p></div>
<div class="m2"><p>عاشقان را ناز و شیوه می‌کشد نه رنگ روی</p></div></div>