---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>گل لاف ن با رخ آن سرو قد زد است</p></div>
<div class="m2"><p>باد صباش نیک بزن گو که به زده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زد پای بر سرم شدم از خود چو آن بدید</p></div>
<div class="m2"><p>در خنده رفت و گفت که بختش لگد زده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این دل به عاشقی نه از امروز شد علم</p></div>
<div class="m2"><p>کوس محبت ز ازل تا ابد زده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باید حکیم را سوی بیمار خانه برد</p></div>
<div class="m2"><p>گر در زمان حسن تو لاف از خرد زده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد چو آه حسرت و ما باده می کشیم</p></div>
<div class="m2"><p>سنگی که زد به شیشه ما از حسد زده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باشد به دور چشم تو از حد برون خطا</p></div>
<div class="m2"><p>هرمست را که تحتسب شهر حد زده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن شب که رفت و پای سگش بوسه زد کمال</p></div>
<div class="m2"><p>تا روز بوسه ها به کف پای خود زده است</p></div></div>