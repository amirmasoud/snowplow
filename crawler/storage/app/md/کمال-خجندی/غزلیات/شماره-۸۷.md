---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>ای گله گوشه حسن آمده بر فرق تو راست</p></div>
<div class="m2"><p>سرو را نیست چنین قامت رعنا که تو راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنبل زلف تو مشاطة گلبرگ تر است</p></div>
<div class="m2"><p>لاله روی نر آرایش بستان صفاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جعد بر حسن خطت نافه مشک ختن است</p></div>
<div class="m2"><p>طاق ابروی خوشت قبله خوبان ختاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کسی منکر خورشید جمالت نشود</p></div>
<div class="m2"><p>خط شبرنگ تو بر محضر دعویش گواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا مگر شیفته سرو سهی قامت تست</p></div>
<div class="m2"><p>که بر آهنگ نوا زد همه در پرده راست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خبر وصل تو از باد صبا می پرسم</p></div>
<div class="m2"><p>زآنکه ما را طمع وصل تو از باد صباست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی جرعة عشقت به کمال است مدام</p></div>
<div class="m2"><p>گر چه از میکد، وصل تو پیوسته جداست</p></div></div>