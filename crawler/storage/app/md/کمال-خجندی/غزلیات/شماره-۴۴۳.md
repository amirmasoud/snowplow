---
title: >-
    شمارهٔ ۴۴۳
---
# شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>ز ماهتاب جمالت ز ماه تاب رود</p></div>
<div class="m2"><p>چه جای ماه سخن هم در آفتاب رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو آن دری که از پیش نظر اگر بروی</p></div>
<div class="m2"><p>مرا ز دیده گریان در خوشاب رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن به خونه دلم چشم سرخ زآنکه کسی</p></div>
<div class="m2"><p>طمع نکرد به خوتی که از کباب رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحسرتت نگرم سوی گل ولی به سراب</p></div>
<div class="m2"><p>ز جان تشنه کجا آرزوی آب رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشیدم از نو جفای جهان که می دانست</p></div>
<div class="m2"><p>که بر من از ملک رحمت این عذاب رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو رفت در سر او سر نو هم برو ای جان</p></div>
<div class="m2"><p>که با تو نیز نباید که این عتاب رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال چشم تو گر میپرده شب هجران</p></div>
<div class="m2"><p>خیال خواب چنانش بزن که خواب رود</p></div></div>