---
title: >-
    شمارهٔ ۴۹۸
---
# شمارهٔ ۴۹۸

<div class="b" id="bn1"><div class="m1"><p>البش جان عاشق هوس میکند</p></div>
<div class="m2"><p>شکر آرزوی مگس می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دعا گوی و سپری ز دشنام تو</p></div>
<div class="m2"><p>از حلوای قندی که بس میکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رود دل در آن زلف ترسان ز چشم</p></div>
<div class="m2"><p>چو شبرو که خوف از عس می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه کس را بر آن در گذارد رقیب</p></div>
<div class="m2"><p>نه او التفاتی بکس میکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تیر تو دارد نظر آنکة صید</p></div>
<div class="m2"><p>دوان است و رو باز پس میکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سوزم وخت راست هنگامه گرم</p></div>
<div class="m2"><p>چو آتش که گرمی بخس میکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگلزار بی یار نالد کمال</p></div>
<div class="m2"><p>چو بلبل که بانگ از قفس می کند</p></div></div>