---
title: >-
    شمارهٔ ۴۶۰
---
# شمارهٔ ۴۶۰

<div class="b" id="bn1"><div class="m1"><p>عاشقان خط تو را مشک ختا می‌گویند</p></div>
<div class="m2"><p>گر خط آن‌است که داری نه خطا می‌گویند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاق ابروی تو را گوشه‌نشینان جهان</p></div>
<div class="m2"><p>قبله‌ی دعوت و محراب دعا می‌گویند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌دلان زخم تو را مرهم جان می‌شمرند</p></div>
<div class="m2"><p>خستگان درد تو را عین دوا می‌گویند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اهل مسجد که در احیا چو مسیحند امروز</p></div>
<div class="m2"><p>پیش لعلت همه از علم شما می‌گویند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سال‌ها رفت که بر بوی وفا منتظران</p></div>
<div class="m2"><p>قصه‌ی عهد تو با باد صبا می‌گویند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط سبز و لب میگون تو را زنده‌دلان</p></div>
<div class="m2"><p>به لطافت خضر و آب بقا می‌گویند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به وصف تو گشودیم زبان پیش کمال</p></div>
<div class="m2"><p>قدسیان بر فلک از گفته‌ی ما می‌گویند</p></div></div>