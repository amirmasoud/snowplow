---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>به چین زلف تو کان رشک صورت چین است</p></div>
<div class="m2"><p>از وقت شیر مزیدن لب نو شیرین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دمی ز دیده پرخون نمی شوی بیرون</p></div>
<div class="m2"><p>بدان سبب که تو طفلی و خانه رنگین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگر فوس کنانم مگو که زان توأم</p></div>
<div class="m2"><p>که سوختم ز دروغ تو راستی این است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از مهر کرد و وفا نوبه آن دل سنگین</p></div>
<div class="m2"><p>چگونه توبه او بشکنم که سنگین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به درد و غم چه نهی مستم ز نو ستمی</p></div>
<div class="m2"><p>کرم نمای که آن لطفهای دیرین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برم سر از تن و بر آستانت اندازم</p></div>
<div class="m2"><p>گرش به خواب به بینم که میل بالین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای وصل تو خواند کمال ورد و دعا</p></div>
<div class="m2"><p>شنیده که دعاها برای آمین است</p></div></div>