---
title: >-
    شمارهٔ ۷۶۴
---
# شمارهٔ ۷۶۴

<div class="b" id="bn1"><div class="m1"><p>گر تو سر خواهی ز من سر با تو ببارم به چشم</p></div>
<div class="m2"><p>سر چه باشد هرچه دارم در نظر آرم به چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفته بردار از خاک در ما روی خویش</p></div>
<div class="m2"><p>گرچه گردست آن نه رو آن گرد بردارم به چشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته نظاره رویم به چشم من گذار</p></div>
<div class="m2"><p>چون نوع چشم دیگری غم نیست بگذارم به چشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفته از دور به شمر عقدهای زلف من</p></div>
<div class="m2"><p>گرچه بی انگشت دشوارست بشمارم به چشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته سر نامه عشقم به خون دل نگار</p></div>
<div class="m2"><p>سازم از مژگان قلم و آن نامه بنگارم به چشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته پایم ببوس و هیچ با مزگان مسای</p></div>
<div class="m2"><p>مردمی چشمی دل مردم نیازارم به چشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته از ما نگه دار آب روی خود کمال</p></div>
<div class="m2"><p>خاک کوی تست آب رو نگه دارم به چشم</p></div></div>