---
title: >-
    شمارهٔ ۳۷۴
---
# شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>چشمت به سعی غمزه در فتنه باز کرد</p></div>
<div class="m2"><p>زلفت به ظلم دست تطاول دراز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محمود را چه جرم که شد پای بند عشق</p></div>
<div class="m2"><p>آن فتنه ها همه سر زلف ایاز کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویند ناز پر بېرة مهر و عشق من</p></div>
<div class="m2"><p>شد بیشتر بروی تو چندان که ناز کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من در زمانه پایه و قدری نداشتم</p></div>
<div class="m2"><p>سودای قامت تو مرا سرفراز کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی تو برد از دلم اندیشه بهشت</p></div>
<div class="m2"><p>ناز تو از نعیم مرا بی نیاز کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفتم بر طبیب که پرسم علاج درد</p></div>
<div class="m2"><p>چون ناله ام شنید روان در فراز کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ننشست بر وجود ضعیفت مگس کمال</p></div>
<div class="m2"><p>از تار عنکبوت مگر احتراز کرد</p></div></div>