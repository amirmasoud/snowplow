---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>گر حال دل به دوست نه امکان گفتن است</p></div>
<div class="m2"><p>بر شمع سوز سینه پروانه روشن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از من بگو به مدعی ای یار آشنا</p></div>
<div class="m2"><p>من فارغم ز قصد تو چون دوست با من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنرا که دل سوی لب او می کشد چو جام</p></div>
<div class="m2"><p>بر سر نوشته اند که خونت بگردن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان نگذرد ز کوی تو کان عندلیب عیب</p></div>
<div class="m2"><p>مرغی است کش حظیرة قدسی نشیمن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن دوستدار کز تو جدا می کند مرا</p></div>
<div class="m2"><p>وان هم به حق صحبت دبرین که دشمن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق شکسته پای نه در پیش تست و بس</p></div>
<div class="m2"><p>هر هر جا فتد چو زلف تو مسکین فروتن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل چو بشنوی خبر وصل از آن دهان</p></div>
<div class="m2"><p>باور مکن که آن سخن نا معین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نام کمال رفت به پاکیزه دامنی</p></div>
<div class="m2"><p>تا در غمت به خون دل آلوده دامن است</p></div></div>