---
title: >-
    شمارهٔ ۳۴۷
---
# شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>به مجلسی که از روی نو پرده بر گیرند</p></div>
<div class="m2"><p>چراغ و شمع بر افروختن ز سر گیرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو در محاوره آنی به منطق شیرین</p></div>
<div class="m2"><p>لب و دهان تو صد نکته بر شکر گیرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خاک راه تو گو روی ما غبار بگیر</p></div>
<div class="m2"><p>که اهل عشق چنین خاک را به زر گیرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دوستی که اگر پای بر دو دیده نهی</p></div>
<div class="m2"><p>هنوزت اهل دل از دیده دوستتر گیرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ار مقابل آن ابروان نهد مه نو</p></div>
<div class="m2"><p>گناه او همه بر چشم کج نظر گیرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از باده در سر رندان جنون شود مستی</p></div>
<div class="m2"><p>به یاد روی نوار ساغری دگر گیرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آستان نو جانها ز سوز و آه کمال</p></div>
<div class="m2"><p>اگر نه آب زند گریه جمله در گیرند</p></div></div>