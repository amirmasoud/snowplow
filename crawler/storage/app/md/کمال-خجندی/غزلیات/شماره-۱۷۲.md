---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>زلف تو از غالیه مشکین ترست</p></div>
<div class="m2"><p>اشک من از لعل تو رنگین ترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شکر انگور سمرقندیان</p></div>
<div class="m2"><p>سیب زنخدان تو شیرین ترست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد ز دستت که ز ترکان مست</p></div>
<div class="m2"><p>چشم جفا کیش تو بیدین ترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به مساکین نظری میکنی</p></div>
<div class="m2"><p>بر دل من، کز همه مسکین ترست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسبت خارا نکنم با دلت</p></div>
<div class="m2"><p>چون دل بی رحم تو سنگین ترست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به سر غمزدگان میروی</p></div>
<div class="m2"><p>خاطر من از همه غمگین ترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه لبت خشک شد از غم کمال</p></div>
<div class="m2"><p>چهره ات از دیده خونین ترست</p></div></div>