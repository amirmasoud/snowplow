---
title: >-
    شمارهٔ ۵۹۴
---
# شمارهٔ ۵۹۴

<div class="b" id="bn1"><div class="m1"><p>آرزو برده ام که چشم تو باز</p></div>
<div class="m2"><p>کشدم که به عشره گاه به ناز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما خریدیم اگر فروشد دوست</p></div>
<div class="m2"><p>نیم ناری بصد هزار نیاز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گره کشی خوان وصل لب بگشای</p></div>
<div class="m2"><p>که نخست از نمک کنند آغاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر ما زیر پا فکن جان نیز</p></div>
<div class="m2"><p>هرچه گفتیم بر زمین انداز باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم از زلفت از چه دورم؟ گفت:</p></div>
<div class="m2"><p>رفتی به فکر دور و دراز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شکر ریز فکر خویش کمال</p></div>
<div class="m2"><p>قند هر یک سخن مکرر ساز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بیابد به چاشنی گیری</p></div>
<div class="m2"><p>شکر از مصر و سعدی از شیراز</p></div></div>