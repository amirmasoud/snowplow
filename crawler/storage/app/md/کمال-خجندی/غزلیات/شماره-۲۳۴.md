---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>گلی چون سرو ما در هر چمن نیست</p></div>
<div class="m2"><p>و گر باشد چنین نازک بدن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به باریکی لبهاش ار سخن هست</p></div>
<div class="m2"><p>در آن سوی میان باری سخن نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن حلوای لبها صوفیان را</p></div>
<div class="m2"><p>بجز انگشت حسرت در دهن نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا بیمار پرسی آمد و گفت</p></div>
<div class="m2"><p>بحمد الله که خونه زیستن نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیاساید شهید عشق در خاک</p></div>
<div class="m2"><p>گرش گردی ز کویت به کفن نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشد دل جز میان بار و من گم</p></div>
<div class="m2"><p>به او باشد بنین باری به من نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال آن مشک مو را نیک دریاب</p></div>
<div class="m2"><p>کزین آهر به صحرای ختن نیست</p></div></div>