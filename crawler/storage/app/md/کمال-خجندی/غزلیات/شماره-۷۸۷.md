---
title: >-
    شمارهٔ ۷۸۷
---
# شمارهٔ ۷۸۷

<div class="b" id="bn1"><div class="m1"><p>مرا گویند باران کیست بار تو چرا گویم</p></div>
<div class="m2"><p>ز مهروبان کدامست اختیار نو چرا گویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشسته بر سر راه طلبکاری چو مشتاقان</p></div>
<div class="m2"><p>برای کیست چندین انتظار تو چرا گویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نماند از خوردن غمهای تو نام و نشانی هم</p></div>
<div class="m2"><p>نگونی چیست نام غمگسار نو چرا گویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقیب ار گویدم کای بیخبر از کار و بار خود</p></div>
<div class="m2"><p>به من باری بگو تا چیست کار تو چرا گویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرم باشد مجال نطق پیش تو به روز و شب</p></div>
<div class="m2"><p>سخن جز از سر زلف و عذار تو چرا گویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و اگر باز از قرار دوستی آن زلف بر گردد</p></div>
<div class="m2"><p>چرا و چون به زلف بی قرار تو چرا گویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر خون کمال آن غمزه ریزد از سر مستی</p></div>
<div class="m2"><p>من این رنجش به چشم پر خمار تو چرا گویم</p></div></div>