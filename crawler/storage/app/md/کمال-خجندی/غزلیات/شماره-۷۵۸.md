---
title: >-
    شمارهٔ ۷۵۸
---
# شمارهٔ ۷۵۸

<div class="b" id="bn1"><div class="m1"><p>عید می آید و وقتست که در مه نگریم</p></div>
<div class="m2"><p>پرده برگی که از مه به تو مشتاق تریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جمال تو که عیدست و به همه ماند راست</p></div>
<div class="m2"><p>گر گماریم نظر بر به نو کج نظریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست در عید دگر کشتن ما فکر بعید</p></div>
<div class="m2"><p>پیش روی تو چه محتاج به عید دگریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر زلفت شب قدرست و غنیمت شب قدر</p></div>
<div class="m2"><p>یک شب آن عقد بگیریم و غنیمت شمریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقیا باده ده و نقل که شد نوبت آن</p></div>
<div class="m2"><p>که دگر روزه خوریم و غم روزی نخوریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پست شد غلغل تسبیح و تراویح هنوز</p></div>
<div class="m2"><p>به حق روزه کز آن و لوله با دردسریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزه خوردیم و فسم هم به نماز تو کمال</p></div>
<div class="m2"><p>که دگر دردسر خویش به مسجد نبریم</p></div></div>