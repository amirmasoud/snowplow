---
title: >-
    شمارهٔ ۸۶۱
---
# شمارهٔ ۸۶۱

<div class="b" id="bn1"><div class="m1"><p>سوخت جانم تا ز پا افتاد زلفت بر ذقن</p></div>
<div class="m2"><p>تشنه را جان سوزد آری چون به چاه افتد رسن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده تا میم دهان و نون ابروی تو دید</p></div>
<div class="m2"><p>نقش آن بستم به دل چون بود هر دو نقش من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلیران را از برون پیرهن باشد خیال</p></div>
<div class="m2"><p>زآن میان او را خیالی در درون پیرهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میکند سرو از تولی پیش آن گلپا دراز</p></div>
<div class="m2"><p>ای صبا چندانکه پایش بشکنی بروی بزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر در آرد سر به مهر آن زلف بر رخسار نه</p></div>
<div class="m2"><p>چون مسلمان شد بگر زنار بر آتش فکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما قیریم و گدا دانم ندارد گوش ما</p></div>
<div class="m2"><p>چون به زر او را تعلقهاست چون در عدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیستی و تنگدستی ه باشدت دایم کمال</p></div>
<div class="m2"><p>چون نداری دل که داری دست از آن شیرین دهن</p></div></div>