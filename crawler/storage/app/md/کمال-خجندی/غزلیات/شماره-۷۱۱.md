---
title: >-
    شمارهٔ ۷۱۱
---
# شمارهٔ ۷۱۱

<div class="b" id="bn1"><div class="m1"><p>چرا رنجید یار از من گناه خود نمی دانم</p></div>
<div class="m2"><p>چگونه پاک سازم باز راه خود نمی دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر ند گریز افتد مرا از جور چشم او</p></div>
<div class="m2"><p>بجز در سایه زلفش پناه خود نمی دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سوی او گرم چون آب و آتش قاصدی باید</p></div>
<div class="m2"><p>چنین قاصد برون از اشک و آه خود نمی دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بمه دیدن کسان را هست عید و شادمانیها</p></div>
<div class="m2"><p>مرااین عید کی باشد بماه خود نمیدانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پری رویان همه جسمند و او نورو درین دعوی</p></div>
<div class="m2"><p>ز روی دوست روشنتر گواه خود نمی دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا در جنت اعلی قرار دل کجا باشد</p></div>
<div class="m2"><p>که جز خاک درش آرامگاه خود نمی دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر گوید کمال از خاک راه ماست هم کمتر</p></div>
<div class="m2"><p>من این بی حرمتی جز عز و جاه خود نمی دانم</p></div></div>