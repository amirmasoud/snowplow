---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>خیال روی او در دیده نور است</p></div>
<div class="m2"><p>مخوانش دل که از دلبر صبور است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آن رخ میکند دعوی خویشی</p></div>
<div class="m2"><p>به تابان و لیکن خویش دور است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان نیستی دیدیم و هستی</p></div>
<div class="m2"><p>میان بار ما خیر الأمور است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا با آن بهشتی رو به آتش</p></div>
<div class="m2"><p>سلاسل خوشتر از گیسوی حور است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمال این یک غزل گوباش کوتاه</p></div>
<div class="m2"><p>ز کوتاهی چه نقصان زېور است</p></div></div>