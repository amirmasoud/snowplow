---
title: >-
    شمارهٔ ۳۲۹
---
# شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>باز عید آمد و لب‌ها ز طرب خندان شد</p></div>
<div class="m2"><p>شادی عید پدیدار تو صد چندان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه در عید نپوشد رخ و باشد پیدا</p></div>
<div class="m2"><p>پرده برگیر که دیگر نتوان پنهان شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابرویت داد به مردم ز مه عید نشان</p></div>
<div class="m2"><p>همه را چشم به نظاره او حیران شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که دیدت چو مه عید شب از گوشه بام</p></div>
<div class="m2"><p>مست چون چشم تو در خانه خود غلتان شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پسته هر عید گران بودی و بادام به قدر</p></div>
<div class="m2"><p>از لب و چشم تو این عید همه ارزان شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عادت این است که در عید نخستین بکشد</p></div>
<div class="m2"><p>غمزه را از چه به نا کشتن ما فرمان شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبر تا عید دگر چون نتوانست کمال</p></div>
<div class="m2"><p>کرد عید دگر و بر در او قربان شد</p></div></div>