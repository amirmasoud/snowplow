---
title: >-
    شمارهٔ ۸۱۷
---
# شمارهٔ ۸۱۷

<div class="b" id="bn1"><div class="m1"><p>آمد لب تو باز بصد نازه در سخن</p></div>
<div class="m2"><p>شیرین حکایتیست که گوید شکر سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاجت بگفت نیست ترا چشم و غمزه هست</p></div>
<div class="m2"><p>گر میکنی بمردم صاحب نظر سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیوار گوش دارد و اغیار نیز چشم</p></div>
<div class="m2"><p>ما چون کنیم با نوز بیرون در سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با گیسویت شبی که به پایان برم حدیث</p></div>
<div class="m2"><p>خواهم گرفت با سر زلفت ز سر سخن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ماجرای اشک منت هم شدی وقوف</p></div>
<div class="m2"><p>گه گه اگر بگوش تو کردی گهر سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق رخ تو دید و سخن بسته شد برو</p></div>
<div class="m2"><p>چون شد تمام کشته نگوید دگر سخن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصف رخت کمال چو آورد در میان</p></div>
<div class="m2"><p>گفت از همه نکوتر و باریکتره سخن</p></div></div>