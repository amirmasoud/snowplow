---
title: >-
    شمارهٔ ۴۰۵
---
# شمارهٔ ۴۰۵

<div class="b" id="bn1"><div class="m1"><p>دل کجا شد خبرش غمزهٔ او می‌داند</p></div>
<div class="m2"><p>مست هرجا که کبابست بو می‌داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر پریشانی و آشوب که جان را ز قاست</p></div>
<div class="m2"><p>دل دیوانه از آن سلسله مو می‌داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از آن سرو که به دیده نشاندم نبرم</p></div>
<div class="m2"><p>باغبان قیمت سرو لب جو می‌داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار گویند چه خواهد به تو داد از لب خویش</p></div>
<div class="m2"><p>من چه دانم کرم دوست همو می‌داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر درت طاقت بیداری من کس را نیست</p></div>
<div class="m2"><p>نیست حاجت به گواهم سنگ کو می‌داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناصِحا مصلحت من هوس روی نکوست</p></div>
<div class="m2"><p>هرکسی مصلحت خویش نکو می‌داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد چون زلف تو با غمزه فرو داشت کمال</p></div>
<div class="m2"><p>زآن که بدمستی آن عربده‌جو می‌داند</p></div></div>