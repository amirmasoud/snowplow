---
title: >-
    شمارهٔ ۵۴۶
---
# شمارهٔ ۵۴۶

<div class="b" id="bn1"><div class="m1"><p>هرکه وصلش طلبد ترک سرش باید کرد</p></div>
<div class="m2"><p>ورنه اندیشه کار دگرش باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه خواهد که نهان از سر کویش گذرد</p></div>
<div class="m2"><p>صبح خیزی چو نسیم سحرش باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر کوی نو تحمل نکند درد سری</p></div>
<div class="m2"><p>هر که از آنجا گذرد ترک سرش باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه دردم ز طبیب است ولی آخر کار</p></div>
<div class="m2"><p>چون رسد کار بجان هم خبرش باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف آشفته او موجب جمعیت ماست</p></div>
<div class="m2"><p>چون چنین است و پس آشفته ترش باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که او را خبر از حالت مستان نبود</p></div>
<div class="m2"><p>به یکی جرعه می بیخبرش باید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه او را هوس گوشه نشینی باشد</p></div>
<div class="m2"><p>از کمانخانه ابرو حذرش باید کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یارب این درد جگر سوز چه مشکل دردیست</p></div>
<div class="m2"><p>که مداوا همه خون جگرش باید کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر کمال آرزوی محبت جانان دارد</p></div>
<div class="m2"><p>زود زین کلبه احزان سفرش باید کرد</p></div></div>