---
title: >-
    شمارهٔ ۳۷۰
---
# شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>جمع باش ای دل که این وقت پریشان بگذرد</p></div>
<div class="m2"><p>گرچه مشکل مینماید لیک آسان بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم یعقوب از نسیم پیرهن روشن شود</p></div>
<div class="m2"><p>وز سر یوسف بلای چاه و زندان بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ حالی را بقایی نیست بی صبری مکن</p></div>
<div class="m2"><p>چون شب صحبت دراید روز هجران بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاخ امیدت شود سر سبز و روی عیش سرخ</p></div>
<div class="m2"><p>باز در جوی مودت آب حیوان بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در غم و شادی بباید ساختن با روزگار</p></div>
<div class="m2"><p>زانکه از دور زمان هم این و هم آن بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تازه گردد باغ عیشت از نسیم اعتدال</p></div>
<div class="m2"><p>بوی جانبخش بهار آید زمستان بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای کمال از غربت و حرمان مشو غمگین که زود</p></div>
<div class="m2"><p>محنت غربت نماند ذل حرمان بگذرد</p></div></div>