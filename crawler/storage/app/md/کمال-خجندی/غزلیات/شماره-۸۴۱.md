---
title: >-
    شمارهٔ ۸۴۱
---
# شمارهٔ ۸۴۱

<div class="b" id="bn1"><div class="m1"><p>خوشا در کوی دلبر آرمیدن</p></div>
<div class="m2"><p>گل از گلزار وصل بار چیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگار خویش را در بر گرفتن</p></div>
<div class="m2"><p>شراب وصل از لعلش چشیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا باشد دلارامی پریروی</p></div>
<div class="m2"><p>که چون آهو بود خویش رمیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تمنا میکنم چون از لبش بوس</p></div>
<div class="m2"><p>بود کارش به دندان لب گزیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود شرمنده سرو از قامت او</p></div>
<div class="m2"><p>کند در باغ چون عزم چمیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولیکن بسکه می باشد دلازار</p></div>
<div class="m2"><p>ز جورش باید از جان دل بریدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشیدم دوش دل در پای او گفت</p></div>
<div class="m2"><p>کمال از جور تا کی سر کشیدن</p></div></div>