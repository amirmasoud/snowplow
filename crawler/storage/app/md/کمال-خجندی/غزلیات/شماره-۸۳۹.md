---
title: >-
    شمارهٔ ۸۳۹
---
# شمارهٔ ۸۳۹

<div class="b" id="bn1"><div class="m1"><p>خبری یافتم از یار مپرسید ز من</p></div>
<div class="m2"><p>تا نیارید بر من خبر دار و رسن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبر دار و رسن رابت منصور بود</p></div>
<div class="m2"><p>خبر رایت و منصور بود قلب شکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خبری یافته ام از گل و از باد بهار</p></div>
<div class="m2"><p>خبر من برسانید به مرغان چمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خبر مرغ چمن باغ و گلستان باشد</p></div>
<div class="m2"><p>خبر باغ و گلستان چه کند دفع حزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خبری یافتم از نکهت پیراهن دوست</p></div>
<div class="m2"><p>به خطا چند دوید از پی آهوی ختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خبر نکهت جانان چه بود مژده جان</p></div>
<div class="m2"><p>مژده جان چه بود صحبت عقل و دل و تن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خبری یافتم ای جوهری از معدن لعل</p></div>
<div class="m2"><p>تو چرا میروی از بهر عقیقی به یمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خبر معدن لعل آن لب شیرین باشد</p></div>
<div class="m2"><p>لب شیرین ببرد تلخی غمها ز دهن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خبری یافتم از دولت وصلت بنوی</p></div>
<div class="m2"><p>تو کجا میروی از بهر اویسی بقرن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خبر وصل بتی مژد: آن دوست کمال</p></div>
<div class="m2"><p>ختم شد نه آن روی بوجه احسن</p></div></div>