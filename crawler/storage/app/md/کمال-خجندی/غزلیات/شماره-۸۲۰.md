---
title: >-
    شمارهٔ ۸۲۰
---
# شمارهٔ ۸۲۰

<div class="b" id="bn1"><div class="m1"><p>ای خوش آن باد که از کوی تو آید بر من</p></div>
<div class="m2"><p>مشت خاک درت باز نهه بر سر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفروزد شبم از مه که فتد بر در و بام</p></div>
<div class="m2"><p>خانه روشن کن و چون شمع درآ از در من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیره جانیست دل سوخته بر دیده نشین</p></div>
<div class="m2"><p>که بود دیده تره خانه روشنتر من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شربت وصل بده از لب جانبخش مرا</p></div>
<div class="m2"><p>ک ز نب هجر تو بگداخت تن لاغر من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد بیزن که کسی بر من بیماره زند</p></div>
<div class="m2"><p>از ضعیفی چر مگس باد برد پیکر من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچکس گرد من خسته نگردد جز اشک</p></div>
<div class="m2"><p>آه و فریاد ز بر گشتگی اختر من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه جز شرح غمت در قلم آورد کمال</p></div>
<div class="m2"><p>آب چشم آمد و شست از ورق دفتر من</p></div></div>