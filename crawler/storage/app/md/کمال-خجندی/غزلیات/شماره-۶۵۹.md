---
title: >-
    شمارهٔ ۶۵۹
---
# شمارهٔ ۶۵۹

<div class="b" id="bn1"><div class="m1"><p>زهی بدایت حسن رخنه نهایت لطف</p></div>
<div class="m2"><p>خط تو حجت حسن و لب تو آیت لطف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم تو قاصد جان شد خط و لبت نگذاشت</p></div>
<div class="m2"><p>زهی رعایت حسن و زهی حمایت لطف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یک خط و دو ورق شرح کرده اند و بیان</p></div>
<div class="m2"><p>خطت مقالت حسن و لبت حکایت لطف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وجود من ز خیالت چنان لطیف شدست</p></div>
<div class="m2"><p>که آب میچکد از دیده ام ز غایت لطف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به از نهایت حسن گل است ز خنده او</p></div>
<div class="m2"><p>دهان تنگ تو چون غنچه در هدایت لطف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا که ورد زبان ذکر آن لب و دهن است</p></div>
<div class="m2"><p>خطاست گر نکنم در سخن رعایت لطف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال بر تو سخن ختم شد برو خوبی</p></div>
<div class="m2"><p>که حد حسن همین باشد و نهایت لطف</p></div></div>