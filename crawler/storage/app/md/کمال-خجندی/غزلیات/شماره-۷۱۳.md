---
title: >-
    شمارهٔ ۷۱۳
---
# شمارهٔ ۷۱۳

<div class="b" id="bn1"><div class="m1"><p>چه بودی گر شبی در خواب رفتی چشم بیدارم</p></div>
<div class="m2"><p>مگر دیدار بنمودی ز روی مرحمت بارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر صاحبدلی بودی که بر من مرحمت کردی</p></div>
<div class="m2"><p>بگوش او رسانیدی حدیث چشم بیدارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم دلبستگی دارد که بر خاک درش میرد</p></div>
<div class="m2"><p>ولی هرگز بکوی او گذر کردن نمی بارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر آن بدمهر سنگین دل نگیرد دست من روزی</p></div>
<div class="m2"><p>شبی در حضرت باری بزاری دست بردارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود رشک گلستان ارم صحن سرای من</p></div>
<div class="m2"><p>اگر روزی چو بخت از در در آید سرو گلبارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر سرو قبا پوشم بخاکم بگذرد روزی</p></div>
<div class="m2"><p>کفن برخورد با سازم هزاران نعره بردارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال خسته را دیگر نصیحت کی قبول افتد</p></div>
<div class="m2"><p>خدا را بگذر ای ناصح بحال خویش بگذارم</p></div></div>