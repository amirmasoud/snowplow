---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>از باد مکش طره جانانه ما را</p></div>
<div class="m2"><p>زنجیر مجنبان دل دیوانه ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شمع چگل گو که برقص آرد و پرواز</p></div>
<div class="m2"><p>این سوخته دلهای چو پروانه ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کردند زیان آنکه به صد گنج فریدون</p></div>
<div class="m2"><p>کردند بها گوهر یکدانه ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدند سرشکم همه همسایه و گفتند</p></div>
<div class="m2"><p>این سیل عجب گر نبرد خانه ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل گر چه خرابست ز غم چون تو درائی</p></div>
<div class="m2"><p>آباد کنی کلبه ویرانه ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواب خوش صبحت برد از دیده مخمور</p></div>
<div class="m2"><p>شب گر شنوی نعره مستانه ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهد گله ها کرد کمال امشب از آن زلف</p></div>
<div class="m2"><p>شبهای چنین گوش کن افسانه ما را</p></div></div>