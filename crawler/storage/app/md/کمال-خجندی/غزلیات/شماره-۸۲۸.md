---
title: >-
    شمارهٔ ۸۲۸
---
# شمارهٔ ۸۲۸

<div class="b" id="bn1"><div class="m1"><p>بدیده سوی تو حیف آیدم گذر کردن</p></div>
<div class="m2"><p>نشان پای نو آزرده نظر کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهادهایم همه سوی آستان تو روی</p></div>
<div class="m2"><p>بعزم کعبه مبارک بود سفر کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب تو همدم ما چون بریم از آن سر زلف</p></div>
<div class="m2"><p>ز ذوق جان که تواند بترک سر کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دعای جان تو گویم همیشه پیش رقیب</p></div>
<div class="m2"><p>که بیدعا نتوان از بلا حذر کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رقیب تیز کند گفتی از برای تو تیغ</p></div>
<div class="m2"><p>کراست صبر بفرمای تیز تر کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بیم آنکه بدرمان حوالتم نکنی</p></div>
<div class="m2"><p>ز درد خویش نیارم ترا خبر کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علاج درد خود ار پرسی از طبیب کمال</p></div>
<div class="m2"><p>در آن مقام زبان بایدت بدر کردن</p></div></div>