---
title: >-
    شمارهٔ ۸۵۷
---
# شمارهٔ ۸۵۷

<div class="b" id="bn1"><div class="m1"><p>ز نشاط و عیش بادا لب تو همیشه خندان</p></div>
<div class="m2"><p>شکرست آن نه لب‌ها گهرست آن نه دندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دهان تنگ فرما که ز حقه مرهمی ده</p></div>
<div class="m2"><p>چو به خنده تازه کردی سر ریش دردمندان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به غبار گرد روی تو خطی نوشته دیدم</p></div>
<div class="m2"><p>که به حسن از آنچه بودی شده هزار چندان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلم مصوران گو سر خود بگیر و میرو</p></div>
<div class="m2"><p>نوبیا و صورت خود بنما به نقش‌بندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بتان آهنین‌دل نشوی دلا مقابل</p></div>
<div class="m2"><p>که تو آبگینه داری و نبی حریف سندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مجال بوسه افتاد به به نیاز صوفی</p></div>
<div class="m2"><p>تو و آستین زاهد من و آستان رندان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ننهی کمال خود را ز سگان آستانش</p></div>
<div class="m2"><p>که به پایه بزرگی نرسند خودپسندان</p></div></div>