---
title: >-
    شمارهٔ ۱۰۳۶
---
# شمارهٔ ۱۰۳۶

<div class="b" id="bn1"><div class="m1"><p>صنما در خط سنبل مه تابان داری</p></div>
<div class="m2"><p>بر سر شاخ صنوبر گل خندان داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم عیسی همه از لعل شکر بار دهی</p></div>
<div class="m2"><p>حسن یوسف همه در چاه زنخدان داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سر زلف تو برهم نزند عالم را</p></div>
<div class="m2"><p>صورت خویشتن از آینه پنهان داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای شه گلرخ شیرین دهن شور انگیز</p></div>
<div class="m2"><p>تا کی احوال من خسته پریشان داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تشنه شد لعل تو بر خون دل ما هر دم</p></div>
<div class="m2"><p>گر چه در درج گهر چشمه حیوان داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ربانی ز دل سوختگان گوی فرار</p></div>
<div class="m2"><p>گرد بر گرد به از غالیه چوگان داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناله زار کمال است چو بلبل شب و روز</p></div>
<div class="m2"><p>با تو در سبزه خوشبوی گلستان داری</p></div></div>