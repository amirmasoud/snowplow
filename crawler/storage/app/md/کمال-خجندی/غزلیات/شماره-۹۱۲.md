---
title: >-
    شمارهٔ ۹۱۲
---
# شمارهٔ ۹۱۲

<div class="b" id="bn1"><div class="m1"><p>ما به کلی طمع وصل بریدیم از تو</p></div>
<div class="m2"><p>مرحبانی نزده دست کشیدیم از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل که در عشق تو خود را به غلامی بفروخت</p></div>
<div class="m2"><p>تا به هیچش ندهی باز خریدیم از تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالها گرچه نهادیم به تو چشم امید</p></div>
<div class="m2"><p>جز جفا و ستم و جور ندیدیم از تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سؤالی و دعانی که بر آن در کردیم</p></div>
<div class="m2"><p>غیر دشنام جوابی نشنیدیم از تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه درختی تو که تا در چمن جان رستی</p></div>
<div class="m2"><p>بر نخوردیم و گلی نیز نچیدیم از تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دو لب رنگ برنگ این همه حلوا که تراست</p></div>
<div class="m2"><p>و ای عجب چاشنی هم نچشیدیم از تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفتی از چشم ترو گریه کنان گفت کمال</p></div>
<div class="m2"><p>رفت عمر و بمرادی نرسیدیم از تو</p></div></div>