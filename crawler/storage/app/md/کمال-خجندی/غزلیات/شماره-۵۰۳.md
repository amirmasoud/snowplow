---
title: >-
    شمارهٔ ۵۰۳
---
# شمارهٔ ۵۰۳

<div class="b" id="bn1"><div class="m1"><p>ما را دگر بر آن در خواب شبان نباشد</p></div>
<div class="m2"><p>بالین دردمندان جز آستان نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم ستاره گیرد شبها بخواب رفتن</p></div>
<div class="m2"><p>گر آه و ناله ما بر آسمان نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش تو بر ندارد صوفی صر غرامت</p></div>
<div class="m2"><p>پر شکرانه وار جانش تا در میان نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من کی چنان دلی را پهلوی خود نشانم</p></div>
<div class="m2"><p>کز ناوک تو صد جا بر وی نشان نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل از تو بر گرفتن بر دیگری نهادن</p></div>
<div class="m2"><p>در عقله این نگنجد در خاطر آن نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم تو دوست دارم و آن تیر غمزه را هم</p></div>
<div class="m2"><p>آزار دوستانم بر دل گران نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داری کمال جانی بر دوستان برافشان</p></div>
<div class="m2"><p>عاشق جوی نیرزد گرجان فشان نباشد</p></div></div>