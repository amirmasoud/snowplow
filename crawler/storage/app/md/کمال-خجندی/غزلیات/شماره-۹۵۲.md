---
title: >-
    شمارهٔ ۹۵۲
---
# شمارهٔ ۹۵۲

<div class="b" id="bn1"><div class="m1"><p>مرا که روی تو بینم چه حاجت است</p></div>
<div class="m2"><p>که کسی نظر نکند با وجود گل به گیاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ماه اگر ز حسن رخت بافتی نشان یوسف</p></div>
<div class="m2"><p>ز شرم روی تو بیرون نیامدی از چاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرد چو قدرت حق در رخت مشاهده کرد</p></div>
<div class="m2"><p>بگفت اشهدان لااله الا الله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر به صورت خوبان اگر گناه بود</p></div>
<div class="m2"><p>صحیفة عمل بنده پر بود ز گناه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمال، چون تو شدی بنده همه خوبان</p></div>
<div class="m2"><p>اگر تو جای نداری به او بیار پناه</p></div></div>