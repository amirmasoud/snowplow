---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>داغ عشقت بر رخ جانها نشان دولت است</p></div>
<div class="m2"><p>هر که محروم است ازین دولت سزای محنت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بلا افزون فرستی من بدین نعمت هنوز</p></div>
<div class="m2"><p>شکر می گویم که در شکرت مزید نعمت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بزرگی گر سگ خود خوانیم که گه رواست</p></div>
<div class="m2"><p>هر که شد خاک درت او را به از صد عزت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به بینی عاشقی در گربه ای زاهد چو اشک</p></div>
<div class="m2"><p>از نظر مگریز کان باران ز ابر رحمت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زحمت آن در مده ای سر که از ما دوست را</p></div>
<div class="m2"><p>این گرانی بس که جان بر آستان خدمت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تو در دوزخ مرا نار وعذاب سلسله</p></div>
<div class="m2"><p>خوشتر از رخسار و زلف حوریان جنت است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست جز وصلی ازو در پوزه جان کمال</p></div>
<div class="m2"><p>آفرین بر جان درویشی که صاحب همت است</p></div></div>