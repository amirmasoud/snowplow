---
title: >-
    شمارهٔ ۳۸۲
---
# شمارهٔ ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>چنین که سوز فراقم ز سینه دود برآورد</p></div>
<div class="m2"><p>عجب مدار گرم ابر دیده سیل ببارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیاه پوش از آن گشته است مردم چشمم</p></div>
<div class="m2"><p>که هر درنگ جگر گوشه به خاک سپارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وجود خاکی ما را بسوخت آتش هجران</p></div>
<div class="m2"><p>گر آب دیده نباشد بکوی دوست که آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو آفتاب جهانی روا مدار که چشمم</p></div>
<div class="m2"><p>در انتظار تو شب تا سحر ستاره شمارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن نفس که شنیدم حکایتی ز دهانت</p></div>
<div class="m2"><p>بجان تو که دل من هوای هیچ ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امید من ز خیالت چنین نبد ز کمالت</p></div>
<div class="m2"><p>ک رانه گیرد و زارم بدست هجر گذارد</p></div></div>