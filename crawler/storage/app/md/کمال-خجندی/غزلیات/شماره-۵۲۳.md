---
title: >-
    شمارهٔ ۵۲۳
---
# شمارهٔ ۵۲۳

<div class="b" id="bn1"><div class="m1"><p>مه نامهربان من وفاداری نمیداند</p></div>
<div class="m2"><p>بر اهل دل به جز ظلم وستمکاری نمی داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو دادم دل بدست او به پای محنت افکندش</p></div>
<div class="m2"><p>چه دانستم من بیدل که دلداری نمی دانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نزدیک طبیب احوال درد خویش می گفتم</p></div>
<div class="m2"><p>ولی او چاره این نوع بیماری نمی داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه سود از ناله و زاری برین در داد خواهانرا</p></div>
<div class="m2"><p>که سلطان حال مسکینان بازاری نمی داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مراد خاطر ما نیک میداند حی اما</p></div>
<div class="m2"><p>تغافل می کند زانسانه که پنداری نمی داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب و دندان چون اونی بکام چون منی اولی</p></div>
<div class="m2"><p>که کسی شیرین تر از طوطی شکرخواری نمی داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال از خلق نتوانست پوشیدن نظر بازی</p></div>
<div class="m2"><p>که او رندست و چون زهاد طراری نمی داند</p></div></div>