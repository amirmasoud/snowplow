---
title: >-
    شمارهٔ ۹۰۰
---
# شمارهٔ ۹۰۰

<div class="b" id="bn1"><div class="m1"><p>دل سوی او رفت جان هم گو برو</p></div>
<div class="m2"><p>جان و سر گو در سر دلجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سر شوریده چون گو برفت</p></div>
<div class="m2"><p>برو بر سر چوگان زلفش گو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نیفتد راز ما برروی روز</p></div>
<div class="m2"><p>برو ای سرشک امشب ز پیش رو برو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوی چشم آب روان دارد هنوز</p></div>
<div class="m2"><p>گر ملولی لب لب این جو برو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل از جام عدم دل برمگیر</p></div>
<div class="m2"><p>در پی روی نکو نیکو برو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای صبا گر جانفشانیت آرزوست</p></div>
<div class="m2"><p>بر سر آن ستبل گلبو برو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سراید بر گل رویش کمال</p></div>
<div class="m2"><p>از چمن گو بلبل خوشگر برو</p></div></div>