---
title: >-
    شمارهٔ ۴۴۰
---
# شمارهٔ ۴۴۰

<div class="b" id="bn1"><div class="m1"><p>از سوز جان من آن بیوفا چه غم دارد</p></div>
<div class="m2"><p>اگر چراغ بمیرد صبا چه غم دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که بر نکند سر ز خواب چشمانش</p></div>
<div class="m2"><p>ز آه و ناله شبهای ما چه غم دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان عیش و طرب پادشاه نعمت و ناز</p></div>
<div class="m2"><p>بر آستان ز نیاز گدا چه غم دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر مرا ز بلا دوستان مترسانید</p></div>
<div class="m2"><p>و دلی که شد همه درد از بلا چه غم دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکوی او نروی زاهدا مرو هرگز</p></div>
<div class="m2"><p>تو گر بهشت نه بینی مرا چه غم دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عوام نیر ملامت به عاشق ار بزنند</p></div>
<div class="m2"><p>ز سهم لشکریان پادشا چه غم دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رقیب گو شنو آنچ ار در تو خواست کمال</p></div>
<div class="m2"><p>گدای کوز سگ آشنا چه غم دارد</p></div></div>