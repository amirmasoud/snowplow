---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>آنجا که وصف گیری آن دلربا کنند</p></div>
<div class="m2"><p>از مشک اگر کنند حدیثی خطا کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کام اوست ریختن خون عاشقان</p></div>
<div class="m2"><p>آن به که کامش از دل شیدا روا کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیهوده رنج می برد از دست ما طبیب</p></div>
<div class="m2"><p>این درد عشق نیست که آن را دوا کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را نظر به روی تو بر خط خال نیست</p></div>
<div class="m2"><p>صاحبدلان نظارة صنع خدا کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر گفته کمال فشانند زر چو آب</p></div>
<div class="m2"><p>آنان که خاک را به نظر کیمیا کنند</p></div></div>