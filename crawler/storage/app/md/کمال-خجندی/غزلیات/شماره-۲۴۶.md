---
title: >-
    شمارهٔ ۲۴۶
---
# شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>مائیم و دلی پر خون بر خاک سر کویت</p></div>
<div class="m2"><p>غمگین بهمه رونی در آرزوی رویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو سروی و ما چون آب آورده به پایت سر</p></div>
<div class="m2"><p>می ماند و ما نشه بز خاک سر کویت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راضیم بدشنامی گر باد کئی ورنه</p></div>
<div class="m2"><p>تا عمر بود باقی مانیم دعا گویت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما با در جهان گردیم قسمت همه عالم را</p></div>
<div class="m2"><p>ایشان و جهان ای جان مائیم و یکی مویت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف تو دریغ آید ای جان که به باد افتد</p></div>
<div class="m2"><p>تدبیر که هم حیف است گر گل شنود بویت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر من دل خود جویم در کوی تو نگذاری</p></div>
<div class="m2"><p>پسند جفا چندین بر عاشق دلجویت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند کمال این ره تا چند همی پوشی</p></div>
<div class="m2"><p>تا هست تک و پونی مانیم و نک و پویت</p></div></div>