---
title: >-
    شمارهٔ ۶۰۴
---
# شمارهٔ ۶۰۴

<div class="b" id="bn1"><div class="m1"><p>کشت چشم تو ام به شیوه و ناز</p></div>
<div class="m2"><p>نظری سوی کشتگان انداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خستگان را به پرسشی دریاب</p></div>
<div class="m2"><p>بیدلان را به وعده‌ای بنواز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بیچاره شد ز هجر تو خون</p></div>
<div class="m2"><p>چاره کار او به وصل بساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امشب افکن ز روی خویش نقاب</p></div>
<div class="m2"><p>شمع مجلس ز شرم گر بگداز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما گداییم و مفلس و نوکر</p></div>
<div class="m2"><p>ما غریبیم و تو غریب نواز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تو سر گر طلب کنند ای دل</p></div>
<div class="m2"><p>جان بنه بر سر و روان درباز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقبت زلف او به دست آری</p></div>
<div class="m2"><p>گر بیابی کمال عمر دراز</p></div></div>