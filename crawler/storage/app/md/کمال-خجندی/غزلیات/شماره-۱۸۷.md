---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>صبا زعشوه پنهان دوست الله دوست</p></div>
<div class="m2"><p>از ساغر لب پنهان دوست الله دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زپسته ده او که هیچ پیدا نیست</p></div>
<div class="m2"><p>نصیبه ای به قیران دوست الله دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تیر غمزه خونریز یار الله داد</p></div>
<div class="m2"><p>اگلی ز گلشن بستان دوست الله دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخیل سنبل بستان بار شیدالله</p></div>
<div class="m2"><p>را زچشم سر خوش فتان دوست الله دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمال زنده به بوی گلی ز گلشن اوست</p></div>
<div class="m2"><p>پر صبا ز گلستان دوست الله دوست</p></div></div>