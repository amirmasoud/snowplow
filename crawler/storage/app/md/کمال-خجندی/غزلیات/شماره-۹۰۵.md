---
title: >-
    شمارهٔ ۹۰۵
---
# شمارهٔ ۹۰۵

<div class="b" id="bn1"><div class="m1"><p>غلام پیر خراباتم و طبیعت او</p></div>
<div class="m2"><p>که نیست جز می و شاهد حریف صحبت او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن زمان که نن ماه غبار خواهد بود</p></div>
<div class="m2"><p>نشسته باشم بر آستان خدمت او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو نیست در کف زاهد بضاعت اخلاص</p></div>
<div class="m2"><p>چه فسق و معصیت ما چه زهد و طاعت او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مپوش رخ زمن ای پارسا بعیب گناه</p></div>
<div class="m2"><p>گناه بنده چه بینی نگر به رحمت او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار بار خرد کرد حل نکته عشق</p></div>
<div class="m2"><p>هنوز هیچ ندانست از حقیقت او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هیچ قبله نباید فرو سر او باش</p></div>
<div class="m2"><p>زهی مراتب رند و علو عمت او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال خاک خرابات جوهریسته شریف</p></div>
<div class="m2"><p>که هر کسی نشناسند قدر و قیمت او</p></div></div>