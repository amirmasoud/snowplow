---
title: >-
    شمارهٔ ۹۰۹
---
# شمارهٔ ۹۰۹

<div class="b" id="bn1"><div class="m1"><p>گفتمش ماه پر است آن چهره گفتا پر مگو</p></div>
<div class="m2"><p>کز زمین تا آسمان فرق است از ما تا بدو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم آن موی میان هیچ است هیچ ار بنگری</p></div>
<div class="m2"><p>گفت اگر دلبستگی داری بدو هیچش مگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش آن رنگ و نکهت در گل مشک از چه خاست</p></div>
<div class="m2"><p>گفت هر یک برده اند از روی و مریم رنگ و بو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش دل فکر روی و رای قدت می کند</p></div>
<div class="m2"><p>گفت این رائیست عالی و آن دگر فکر نکو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم از چاه زنخدان تو دل در حیرتست</p></div>
<div class="m2"><p>گفت از رفتند بسیاری درین حیرت فرو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم ار با دیده بگشایم چه باشد راز دل</p></div>
<div class="m2"><p>گفت پیش مردمت ترسم که ریزد آبرو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم از مهر رخت کی دل تهی سازد کمال</p></div>
<div class="m2"><p>گفت آن ساعت که سازد چرخ از خاکش سبو</p></div></div>