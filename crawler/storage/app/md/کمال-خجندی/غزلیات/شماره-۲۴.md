---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>چه رها کنی به شوخی سر زلف دلربا را</p></div>
<div class="m2"><p>که ازو بهم برآری همه وقت حلقه ها را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دوصد ادب برآن در چو خطاست برگذشتن</p></div>
<div class="m2"><p>حرکات نامناسب ز چه رو بود صبا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشود ز گرد فتنه سر کوی دوست خالی</p></div>
<div class="m2"><p>بدو زلف اگر بروبد همه عمر خاک پا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب و روز غیر دردی نخورم بر آستانت</p></div>
<div class="m2"><p>که دوای خوبرویان نرسد من گدا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه دهی دلم که بخشم ز بلای خود امانت</p></div>
<div class="m2"><p>به عطا مکن حوالت به بلا سپار ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بدست خویش تیغم بزنی دمی رها کن</p></div>
<div class="m2"><p>که ز ساعدت بگیرم به حواله خون بها را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدهید گو طبیبان به کمال مرهم جان</p></div>
<div class="m2"><p>چو سپرد جان به جانان چه کند دگر دوا را</p></div></div>