---
title: >-
    شمارهٔ ۱۰۴۸
---
# شمارهٔ ۱۰۴۸

<div class="b" id="bn1"><div class="m1"><p>گر بری دست به آئینه و در خود نگری</p></div>
<div class="m2"><p>ببری دست ز عشاق به صاحب نظری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ننگری دود درونها که به بالا ز تو رفت</p></div>
<div class="m2"><p>شرم داری مگر از ما که به بالا نگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز وصلم ز شب هجر بتر سوزی جان</p></div>
<div class="m2"><p>همچو آتش که به خرمن چو رسی تیز تری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش از سر گذرد خرمن دل سوخته را</p></div>
<div class="m2"><p>چون به سروقت جگر سوختگان در گذری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان و سر هر دو به پای تو از آن می سپرم</p></div>
<div class="m2"><p>که اگر خاک شوم باز به پایت سپری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد ز خون شیشه دلها پرو دور لب تست</p></div>
<div class="m2"><p>فرصتت باد که این می به تمامی بخوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد از روی تو مهجور و به خود مغرورست</p></div>
<div class="m2"><p>خویشتن بینی او بین به چنین بی بصری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محتسب را ز من رند خبردار کنید</p></div>
<div class="m2"><p>که من از سوی یکی هستم و تو بی خبری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کسی جان ببرة تحفه بر دوست کمال</p></div>
<div class="m2"><p>سر ببر تو چه کنی جان نتوانی که بری</p></div></div>