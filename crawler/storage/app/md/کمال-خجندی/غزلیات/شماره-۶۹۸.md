---
title: >-
    شمارهٔ ۶۹۸
---
# شمارهٔ ۶۹۸

<div class="b" id="bn1"><div class="m1"><p>بگذار تا به گلشن روی تو بگذریم</p></div>
<div class="m2"><p>در باغ وصل از گل روی تو برخوریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد اسیر چشم گدایان پادشاه</p></div>
<div class="m2"><p>بردار پرده تا که رخت سیر بنگریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوری دیده گو بشکن حور پای ما</p></div>
<div class="m2"><p>اگر سر بغرف هاش چو طوبی در آوریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خلوتی که ثانی اثنین آن صباست</p></div>
<div class="m2"><p>خود را ز خیل رابعهم نیز نشمریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای باد اهل روضه زحسرت بسوختند</p></div>
<div class="m2"><p>دیگر به کس مگوی که ما خاک آن دریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را به روز واقعه خاطر به آن خوش است</p></div>
<div class="m2"><p>کز خاک آستان تو تصدیع می بریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر جان طلب کند ز تو جانان بده کمال</p></div>
<div class="m2"><p>تا جنس عاریت به خداوند بسپریم</p></div></div>