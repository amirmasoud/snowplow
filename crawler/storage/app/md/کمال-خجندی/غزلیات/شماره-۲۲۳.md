---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>گر مرا از نظر انداختی این هم نظری است</p></div>
<div class="m2"><p>هر جفائی که رسد از تو وفای دگری است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مجروح مرا هست برآن تیر گرفت</p></div>
<div class="m2"><p>که چرا از حرم خاص تو او را گذری است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باش تا حسن نو روزی به ظهور انجامد</p></div>
<div class="m2"><p>که از آن روز هنوز این رخ زیبا سحری است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای حسود از سر کین عیب محبان تا چند</p></div>
<div class="m2"><p>عیب خود بین تو که پنداشتهای آن هنری است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برسانید ز من با سگ کویش امشب</p></div>
<div class="m2"><p>عفو فرما گرت از ناله ما دردسری است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی رقیب از لب او داد به من مژده قتل</p></div>
<div class="m2"><p>دهنش پر ز شکر باد که این خوش خبری است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصل او می طلبی مختصر این است کمال</p></div>
<div class="m2"><p>کین تما نه به اندازه هر مختصری است</p></div></div>