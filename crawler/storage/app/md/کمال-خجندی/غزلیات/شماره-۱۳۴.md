---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>درد تو به از دواست ای دوست</p></div>
<div class="m2"><p>اندوه تو جانفزاست ای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریوزه گر در تو از تو</p></div>
<div class="m2"><p>جز درد و بلا نخواست ای دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با آنکه ز مفلسی ندارم</p></div>
<div class="m2"><p>چیزی که ترا سزاست ای دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش نو نهم دو چشم روشن</p></div>
<div class="m2"><p>گریم نظر مناست ای دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی کشمت ولی روا نیست</p></div>
<div class="m2"><p>اگر دوست کشد رواست ای دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل هرچه به ومن قامتت گفت</p></div>
<div class="m2"><p>آورد خدای راست ای دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کردم به قد تو این غزل راست</p></div>
<div class="m2"><p>بنویس کمال راست ای دوست</p></div></div>