---
title: >-
    شمارهٔ ۸۵۹
---
# شمارهٔ ۸۵۹

<div class="b" id="bn1"><div class="m1"><p>سرو می ماند به قد یار من</p></div>
<div class="m2"><p>ز خاک پای سرو از آن رو شد چمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کنند از لطف خود با تو حدیث</p></div>
<div class="m2"><p>غنچه و سوسن زبان بین و دهن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل ترا و او مرا یار عزیز</p></div>
<div class="m2"><p>صحبت بوسن به از صد پیرهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف تو دائم رسن تابی کند</p></div>
<div class="m2"><p>تا کشد دلها از آن چاه ذقن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقل جان افشان ز لب بر خوان عشق</p></div>
<div class="m2"><p>باز شوری در نمکدانها نکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نمی آیی تو پیش عاشقان</p></div>
<div class="m2"><p>عاشقانرا جان نمی آید بتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهشت دل بود بردی از کمال</p></div>
<div class="m2"><p>جان من دیگر چه می خواهی ز من</p></div></div>