---
title: >-
    شمارهٔ ۵۴۰
---
# شمارهٔ ۵۴۰

<div class="b" id="bn1"><div class="m1"><p>هر قطره خون که از مژه بر روی ما چکد</p></div>
<div class="m2"><p>آید دوان دوان که بر آن خاک پا چکد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غمزه نیش زد به جگر ساحری نگر</p></div>
<div class="m2"><p>کونیش بر کجا زد و خون از کجا چکد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن دم که نیغ فرقت ازوه سازدم جدا</p></div>
<div class="m2"><p>از دیده خون چکد از دل جداه چکد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ریزد ستاره روز قیامت عجب مدار</p></div>
<div class="m2"><p>روز وداع اشک گره از دید ها چکد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر زیر با دلم بشکستی چو زلف خویش</p></div>
<div class="m2"><p>دایم ز شیشه دل من خون چرا چکد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر شب دو دیده آب چکان در هوای تست</p></div>
<div class="m2"><p>شینم که دیدن همه جا از هوا چکد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابر بلاست هجر تو و گریه کمال</p></div>
<div class="m2"><p>باران محنتی که از ابر بلا چکد</p></div></div>