---
title: >-
    شمارهٔ ۴۸۲
---
# شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>گر به سنگ سنمم عشق تو دندان شکند</p></div>
<div class="m2"><p>دل ز لبهای تو دندان طمع بر نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچنان ساده رخی داری و لغزان که برو</p></div>
<div class="m2"><p>گر نشیند مگسی افتد و پایش شکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به قانون نظر وصل بتان ممکن نیست</p></div>
<div class="m2"><p>بی تو دل صبر ضروری چه کند گر نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد از گریه گره انداخت مصلی بر آب</p></div>
<div class="m2"><p>عاشق روی تو سجاده در آتش فکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کتم از مگس خال تو بس کر پس مرگ</p></div>
<div class="m2"><p>عنکبوت آید و بر خاک مزارم بتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل فرهاد برفت از لب شیرین ورنی</p></div>
<div class="m2"><p>هیچ کسی جو به لب چشمه حیوان نکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر شود آگه از استادی آن غمزه کمال</p></div>
<div class="m2"><p>پیش او ساحر بابل رضی الله بزند</p></div></div>