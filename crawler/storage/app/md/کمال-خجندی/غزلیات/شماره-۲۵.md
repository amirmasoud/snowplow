---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>حلال باد می خلد و حور زاهد را</p></div>
<div class="m2"><p>که واگذاشت به رندان شراب و شاهد را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مبر ز گردن صوفی قلاده تسبیح</p></div>
<div class="m2"><p>گذار تا ببرد گردن مقلد را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز فکر و ذکر و ریاضت دماغ را خلل است</p></div>
<div class="m2"><p>بگیر جام و بمان فکرهای فاسد را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برغم زاهد خود بین چو می کشم از جام</p></div>
<div class="m2"><p>به آبگینه کشم میل چشم حاسد را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشو به میکده غایب ز چشم پیر مغان</p></div>
<div class="m2"><p>که با مرید نظرهاست پیر مرشد ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب که شحنه نگشت از امام واقف ما</p></div>
<div class="m2"><p>که خرج کرد به می وقف های مسجد را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال لاف عبادت مزن که چشم بنان</p></div>
<div class="m2"><p>به یک نظر برد از ره هزار عابد را</p></div></div>