---
title: >-
    شمارهٔ ۸۷۸
---
# شمارهٔ ۸۷۸

<div class="b" id="bn1"><div class="m1"><p>من نخواهم دیده از رویت دگر برداشتن</p></div>
<div class="m2"><p>مشکل است از دیده روشن نظر برداشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم داری ای کبوتر این چه گستاخیست باز</p></div>
<div class="m2"><p>نامة کآنجاست نام او بپر برداشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو بر مونیست از جا بر گرفتن بار کوه</p></div>
<div class="m2"><p>پیش آن سوی میان بار کمر برداشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده گریان خواستگردی از درش خندید و گفت</p></div>
<div class="m2"><p>چون توان ای دیده گرد از خاک تر برداشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باره شبهای فراقت چون تواند بر گرفت</p></div>
<div class="m2"><p>آنکه نتواند ز ضعف آه سحر برداشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای مگس منشین بر آن لب جان شیرین گوش دار</p></div>
<div class="m2"><p>بار تو نتواند از لطف شکر برداشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر مقر بود چون بنهاد بر پایت کمال</p></div>
<div class="m2"><p>از خجالت باز نتوانست بر برداشتن</p></div></div>