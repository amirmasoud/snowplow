---
title: >-
    شمارهٔ ۸۳۷
---
# شمارهٔ ۸۳۷

<div class="b" id="bn1"><div class="m1"><p>حدیث یار شیرین لب نگنجد در دهان من</p></div>
<div class="m2"><p>که باشم من که نام او برآبد بر زبان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنیم روزی از چشمت بکشتن داد پیغامی</p></div>
<div class="m2"><p>هنوز آن مژده دولت نرفت از گوش جان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسیم دوستی آبد سگان آستانش را</p></div>
<div class="m2"><p>پس از صدسال اگر یک یک ببوین استخوان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گمان میبردمی کأن به بسرو بوستان ماند</p></div>
<div class="m2"><p>چو دیدم شکل او شد راست از قدش کمان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم او نا توان دارد بجان میجوید آزارم</p></div>
<div class="m2"><p>چه میجوید نمیدانم ز جان ناتوان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمال ار بشنود سعدی دوبیتی زین غزل گوید</p></div>
<div class="m2"><p>که خاک باغ طبعت باد آب بوستان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین مرغ خوش الحانی که من باشم روا باشه</p></div>
<div class="m2"><p>که خارستان بار اشکنه باشد گلستان من</p></div></div>