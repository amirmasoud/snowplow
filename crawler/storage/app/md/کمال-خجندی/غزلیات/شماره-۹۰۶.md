---
title: >-
    شمارهٔ ۹۰۶
---
# شمارهٔ ۹۰۶

<div class="b" id="bn1"><div class="m1"><p>گر تیر کشی از طرف غمزهٔ جادو</p></div>
<div class="m2"><p>صد آه کشد از جگر سوخته آهو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خونم چو شود ریخته مستی کند آن چشم</p></div>
<div class="m2"><p>از ریخته ذوق است و طرب در سر هندو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد حسن به آن رخ تو به یک دفعه فروشی</p></div>
<div class="m2"><p>مه رفت به میزان که فرو شد به ترازو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان چشم دل گمشده پرسیدم و زآن خال</p></div>
<div class="m2"><p>خاک تو نشان داد به لب چشم به ابرو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم به درختان که قد بار کدام است</p></div>
<div class="m2"><p>هر لحظه در آینده در زلف تو به زانو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشگفت کمال از تو بهر جا گل معنی</p></div>
<div class="m2"><p>مرغی ز سر سرو بزد بانگ که کو کو</p></div></div>