---
title: >-
    شمارهٔ ۱۰۳۷
---
# شمارهٔ ۱۰۳۷

<div class="b" id="bn1"><div class="m1"><p>طبیب عاشقان آمد بیا بگذار بیدردی</p></div>
<div class="m2"><p>چه میخواهی ازین رحمت دوائی جو که به گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طریق عاشقی بر گیر و سروی دردمندان شو</p></div>
<div class="m2"><p>که بیعشقی و بیدردی نباشد شیوه مردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت گر زردشد زین درد کار خویش چون زر دان</p></div>
<div class="m2"><p>که چون زر سرخ روبنهاست عاشق را ز رخ دردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلا جز خون مزگانی نرفت از پیش یک کارت</p></div>
<div class="m2"><p>درون ریش درویشی مگر پیموجب آزردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرت ئیت نه روی اوست از هر سجده در قبله</p></div>
<div class="m2"><p>بگیر از سر نماز خود که در نیت خطا کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بروی زرد بنمایم نشسته خاک کویت را</p></div>
<div class="m2"><p>به عقبی گر به پرسندم که از دنیا چه آوردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم و اندوه بی یاریز بیدردان نباید خوش</p></div>
<div class="m2"><p>کمال اینها نرا زید که صاحب دردی و فردی</p></div></div>