---
title: >-
    شمارهٔ ۶۳۰
---
# شمارهٔ ۶۳۰

<div class="b" id="bn1"><div class="m1"><p>دل مسکین که می بینی ازینسان بی زر و زورش</p></div>
<div class="m2"><p>به کوی میکده کردند خوبان مفلس و عورش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شراب لعل می نوش من از جام زمرد گون</p></div>
<div class="m2"><p>ا س ت که زاهد افعی وقت است و میسازم بدین کورش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به قصد جام ما در دست دارد سنگها بارب رسد</p></div>
<div class="m2"><p>نماند محتسب وآنها بماند بر سر گورش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از حال رفتگان ما مگر با ما دگر ساقی</p></div>
<div class="m2"><p>که سازد بادة تلخ تو آب دیده ده شورش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیمان کر که در جوف هوا تعهدش کشیدندی</p></div>
<div class="m2"><p>کنون چون جو شد اندر خاک و هر سو می کشد مورش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان با جمله لذآتش به زنبور عسل ماند</p></div>
<div class="m2"><p>که شیرینیش بسیار است و زان افزون شر و شورش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال از ضعف تن چون شمع دارد زرقشان چهره</p></div>
<div class="m2"><p>میر کی شود وصل بتان با این زر و زورش</p></div></div>