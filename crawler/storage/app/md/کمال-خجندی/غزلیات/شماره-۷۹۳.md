---
title: >-
    شمارهٔ ۷۹۳
---
# شمارهٔ ۷۹۳

<div class="b" id="bn1"><div class="m1"><p>من دل خسته به درد تو دوا یافته ام</p></div>
<div class="m2"><p>رنج ها دیده و امروز شفا یافته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرده با درد تو و زندهٔ جاوید شده</p></div>
<div class="m2"><p>شده در عشق تو فانی و بقا یافته ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده اند اهل نظر خاک درم سرمه چشم</p></div>
<div class="m2"><p>من خاکی نظر لطف تو تا بافته ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفته ام بر اثر باد به بویت همه عمر</p></div>
<div class="m2"><p>خاک کوی تو نه از باد هوا یافته ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دولت آن نیست که بابم دو جهان زیر نگین</p></div>
<div class="m2"><p>دولت آنسته و سعادت که ترا بافته ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهدان بر سر سجاده گرت یافته اند</p></div>
<div class="m2"><p>من میخواره ترا در همه جا یافته ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر ایزد که ازین در به دعاهای کمال</p></div>
<div class="m2"><p>هرچه دل خواسته بود آن همه را بافته ام</p></div></div>