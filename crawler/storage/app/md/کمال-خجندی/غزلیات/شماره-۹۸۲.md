---
title: >-
    شمارهٔ ۹۸۲
---
# شمارهٔ ۹۸۲

<div class="b" id="bn1"><div class="m1"><p>باز دست از جانفشانان بر فشاندی</p></div>
<div class="m2"><p>داد بیدادی ز مظلومان ستاندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتی و آن عارض چون آب و آتش</p></div>
<div class="m2"><p>یاد گارم در دل و در دیده ماندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر تو گفتی سوره ای خوانم چو میری</p></div>
<div class="m2"><p>مردم و الحمدالله هم نخواندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داشتی در سر که خونم ریزی از چشم</p></div>
<div class="m2"><p>کامت این بود از دلم این نیز راندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جای ده اشک مرا بر خاک آن در</p></div>
<div class="m2"><p>کز پی این وعده بسیارش دواندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می رسد بر آسمان دود دل من</p></div>
<div class="m2"><p>قصه سوزم بدین غایت رساندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش خود بنشان کمال او را ازین پس</p></div>
<div class="m2"><p>غم مخور از سوختن آتش نشاندی</p></div></div>