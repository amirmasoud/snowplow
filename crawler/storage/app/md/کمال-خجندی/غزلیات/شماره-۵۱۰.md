---
title: >-
    شمارهٔ ۵۱۰
---
# شمارهٔ ۵۱۰

<div class="b" id="bn1"><div class="m1"><p>مرا ز پیش براندی جفا همین باشد</p></div>
<div class="m2"><p>نهایت ستم ای بیوفا همین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدانچه شکر نکردم وصال روی ترا</p></div>
<div class="m2"><p>گر انتقام نمانی جزا همین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی کند دل ما جز بور قد نو میل</p></div>
<div class="m2"><p>علو همت مشتی گدا همین باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی زنیم نفس جز به باد آن لب لعل</p></div>
<div class="m2"><p>نشان ناز کی طبع ما همین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آستان تو مردن سعادتیست عظیم</p></div>
<div class="m2"><p>از بخت خویش توقع مرا همین باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمال اگر ز گدایان حضرت اونی</p></div>
<div class="m2"><p>مقام سلطنت پادشاه همین باشد</p></div></div>