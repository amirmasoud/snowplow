---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>دل زان نست و دیده بدینم نزاع نیست</p></div>
<div class="m2"><p>اینست که آن دو پیش تو چندان متاع نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی بابم از دهان تو ز آن لب نشان که هیچ</p></div>
<div class="m2"><p>بر سر غیب جان مرا اطلاع نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی بوی صحبت تو مریض فراق را</p></div>
<div class="m2"><p>گر نکهت گل است ازو جز صداع نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق چو عندلیب به بوی گل است مست</p></div>
<div class="m2"><p>جوش و خروش اوز شراب و سماع نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیکو فتاده اند به هم آن رخ و جبین</p></div>
<div class="m2"><p>خورشید و ماه را به ازین اجتماع نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم تو هر که دید ز جان بایدش برید</p></div>
<div class="m2"><p>چون گوشه ای گزید به از انقطاع نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملک وصال بأیدت از سر گذر کمال</p></div>
<div class="m2"><p>خلعت به لشکری نرسد تا شجاع نیست</p></div></div>