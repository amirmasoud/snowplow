---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>آن رخ از مه خجسته فال تراست</p></div>
<div class="m2"><p>لب ز کوثر بی زلال تر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان سر زلف چون پر طاوس</p></div>
<div class="m2"><p>مرغ جانم شکسته بال نر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازو کی رسد به دانه خال</p></div>
<div class="m2"><p>که ز موری ضعیف حال تر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر سودائیان به خاک رهش</p></div>
<div class="m2"><p>از سر زلف پایمال تر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبر در دل مرا و رحم او را</p></div>
<div class="m2"><p>هر دو از یکدیگر محال تر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش چین گرچه دلکش است کمال</p></div>
<div class="m2"><p>نقش کلک تر پر خیال تر است</p></div></div>