---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>دل بردی و دین رواست اینها</p></div>
<div class="m2"><p>ای جان جهان چهاست اینها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بندم ز غمت جدا شد از بند</p></div>
<div class="m2"><p>از جور و ستم جداست اینها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی دهمت هزار دشنام</p></div>
<div class="m2"><p>دشنام مگو دعاست اینها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک ره و گرد پاش گرد آر</p></div>
<div class="m2"><p>ای دیده که توتیاست اینها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر روی تو خال های مشکین</p></div>
<div class="m2"><p>بر دل همه داغ هاست اینها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم خوش و خال خوش خط خوش</p></div>
<div class="m2"><p>از جمله بتان کراست اینها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل شد ز کمال غایب و عقل</p></div>
<div class="m2"><p>گر نیست به تو کجاست اینها</p></div></div>