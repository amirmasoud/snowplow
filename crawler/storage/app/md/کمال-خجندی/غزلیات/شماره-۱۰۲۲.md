---
title: >-
    شمارهٔ ۱۰۲۲
---
# شمارهٔ ۱۰۲۲

<div class="b" id="bn1"><div class="m1"><p>دل رفت به باد دلپذیری</p></div>
<div class="m2"><p>کسی را نبود زجان گزیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عشق بتان جوان شود پیر</p></div>
<div class="m2"><p>این نکه شنیده ام ز پیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گیرم سر زلف و دارمش دوست</p></div>
<div class="m2"><p>زینگونه کراست دار و گیری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد چرخ زند بر آتش از ذوق</p></div>
<div class="m2"><p>صیدی که تو افکنی به تیری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پایم که دل منت به دست است</p></div>
<div class="m2"><p>گر زانکه گرفته ضمیری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیند مگر دو دیده در آب</p></div>
<div class="m2"><p>الطف بدن ترا نظیری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گم کرد کمال دل در آن کوی</p></div>
<div class="m2"><p>بازآ و بجودل فقیری</p></div></div>