---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>چو آفتاب نکند از رخ زمانه نقاب</p></div>
<div class="m2"><p>بریز در قدح گوهرین عقیق مذاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خروش ناله مستان به گوش او نرسید</p></div>
<div class="m2"><p>و گرنه مردم چشمش کجا شدی در خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مطرب غم او چنگ زد به دامن من</p></div>
<div class="m2"><p>از گوشمال جفا ناله می کنم چو رباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جیب پیرهن اندام نازنین بینش</p></div>
<div class="m2"><p>چنانکه از تنه شیشه قطره های گلاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه ریختن خون به حکم شرع خطاست</p></div>
<div class="m2"><p>بریز خون صراحی که هست عین صواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا به جان و سر خود که دردمندان را</p></div>
<div class="m2"><p>به مرهمی که توانی زمان زمان دریاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر که در سر زلفین او وزید صبا</p></div>
<div class="m2"><p>که می وزد ز گلستان نسیم عنبر ناب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا به چشمه حیوان چرا کنم تشییه</p></div>
<div class="m2"><p>که هست تشنه لعل تو گوهر سیراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون که جور فراق از تو بر کمال آمد</p></div>
<div class="m2"><p>ز دست دیده فتادم چو کاسه بر سر آب</p></div></div>