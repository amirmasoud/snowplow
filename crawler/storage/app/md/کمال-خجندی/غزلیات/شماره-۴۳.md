---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>کعبه کویش مراد است این دل آواره را</p></div>
<div class="m2"><p>با مراد دل رسان بارب من بیچاره را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل دران کو رفت و شد آواره من هم می روم</p></div>
<div class="m2"><p>تا ازان آواره تر سازم دل آواره را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان خار و خارا گر تونی همراه من</p></div>
<div class="m2"><p>گل شناسم خار را دیبا شمارم خاره را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر از آن دامن باین درویش اصلی می رسد</p></div>
<div class="m2"><p>پاره ای می دوختم این جان پاره پاره را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوی زلفش رفتم و دیدم که دربند دلست</p></div>
<div class="m2"><p>جز من شبرو که داند مگر این عباره را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش ناهن چه حاصل ذکر پردازی کمال</p></div>
<div class="m2"><p>دانه گوهر چه ریزی مرغ ارزن خواره را</p></div></div>