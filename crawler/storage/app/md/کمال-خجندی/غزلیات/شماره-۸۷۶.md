---
title: >-
    شمارهٔ ۸۷۶
---
# شمارهٔ ۸۷۶

<div class="b" id="bn1"><div class="m1"><p>ما باز دل نهادیم بر جور دلستانان</p></div>
<div class="m2"><p>ما را به ما گذارید باران و مهربانان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بیم بد زبانان بردن نمی توانیم</p></div>
<div class="m2"><p>الا به زیر لبها نام شکر دهانان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با چشم و غمزه تو افتاده جان شیرین</p></div>
<div class="m2"><p>همچون مویزه امانت در دست ترکمانان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خال تو خورد خونم تا داشت باغ آن رخ</p></div>
<div class="m2"><p>آری حرام خواره باشند باغبانان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمان بکشتن ما تا چند رنجه سازی</p></div>
<div class="m2"><p>بخشای نا توانی بر جان ناتوانان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در زلف تو مقید جانیست هر تنی را</p></div>
<div class="m2"><p>بگذار تا نشانند آن زلف جان فشانان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلبر چو خط برآرد سوزد کمال جانت</p></div>
<div class="m2"><p>این حرف یاد دارم از نا نوشته خوانان</p></div></div>