---
title: >-
    شمارهٔ ۷۰۶
---
# شمارهٔ ۷۰۶

<div class="b" id="bn1"><div class="m1"><p>ترا در دل وفا باشد چه دانم</p></div>
<div class="m2"><p>ز خوبان این کرا باشد چه دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکندی وصل خود با روز دیگر</p></div>
<div class="m2"><p>پس از مردن دوا باشد چه دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکش گفتم مرا گفتی روا نیست</p></div>
<div class="m2"><p>چنین کشتن روا باشد چه دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دعا ناگفته داری نسد دشنام</p></div>
<div class="m2"><p>عطا پیش از دعا باشد چه دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دیدن قانعم گفتم ز تو گفت</p></div>
<div class="m2"><p>قناعت در گدا باشد چه دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا گفتی کجا باشد دل تو</p></div>
<div class="m2"><p>چنین مسکین کجا باشد چه دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال این ریش را مرهم صبوریست</p></div>
<div class="m2"><p>و لیکن آن ترا باشد چه دانم</p></div></div>