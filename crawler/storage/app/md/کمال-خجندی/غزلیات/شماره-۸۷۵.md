---
title: >-
    شمارهٔ ۸۷۵
---
# شمارهٔ ۸۷۵

<div class="b" id="bn1"><div class="m1"><p>گر قد همچو سروش در بر توان گرفتن</p></div>
<div class="m2"><p>عمر گذشته دیگر از سر توان گرفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند دل ز جانان بر گیر حاش لله</p></div>
<div class="m2"><p>هرگز چگونه از جان دل بر توان گرفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عمر خود گرفتم یک بوسه از دهانش</p></div>
<div class="m2"><p>گر بخت بار باشد دیگر توان گرفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز بود که یک شب مست از درم در آید</p></div>
<div class="m2"><p>کان فتنه را به مستی در بر توان گرفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی به بوی زلفش مجمر توان نهادن</p></div>
<div class="m2"><p>تا کی به یاد لعلش ساغر توان گرفتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان و سرو دل و زر کردم نثار پایش</p></div>
<div class="m2"><p>بهر متاع سهلی دل بر توان گرفتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دایم کمال شعرش در روی خود بمالد</p></div>
<div class="m2"><p>سحر حلال باشد در زر توان گرفتن</p></div></div>