---
title: >-
    شمارهٔ ۱۰۳۰
---
# شمارهٔ ۱۰۳۰

<div class="b" id="bn1"><div class="m1"><p>ز دیده در دل دیوانه رفتی</p></div>
<div class="m2"><p>ز منظرها به خلوت خانه رفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلت می خواست چون گنجی روان گشت</p></div>
<div class="m2"><p>روان گشتی سوی ویرانه رفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبا بادت بریده با بصد جا</p></div>
<div class="m2"><p>چرا در زلف او چون شانه رفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکویش آمدن ای دل ترا ساخت</p></div>
<div class="m2"><p>که هشیار آمدی دیوانه رفتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مور افتان و خیزان از ضعیفی</p></div>
<div class="m2"><p>سوی خالش بحرص دائه رفتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در او مانده گر رفتی به کعبه</p></div>
<div class="m2"><p>ز کعبه بر در بتخانه رفتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال از کعبه رفتی بر در بار</p></div>
<div class="m2"><p>هزارت آفرین مردانه رفتی</p></div></div>