---
title: >-
    شمارهٔ ۳۱۳
---
# شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>از لیش هر گه که خواهم کام دشنام دهد</p></div>
<div class="m2"><p>اگر نه مطفل است و خورد بازی چرا کام دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساحری بنگر که چون نقلی بخواهم زان دهان</p></div>
<div class="m2"><p>بسته بنماید ز لب وز غمزه بادامم دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویدم یک روز سیمین ساعدم بینی بدست</p></div>
<div class="m2"><p>از انتظارم سوخت تا کی وعده خامم دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستنی خواهم که هشیاری نباشد هر گزش</p></div>
<div class="m2"><p>ساقئی کر تا به یاد لعل او جامم دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قاصد آنم که جان افشانمش از هر طرف</p></div>
<div class="m2"><p>قاصدی گر زآن طرف آید که پیغامم دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بهای خاک پایش نیستم نقدی دریغ</p></div>
<div class="m2"><p>کو فریدون تا دوصد گنج گهر وامم دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلق گویند از سخن مشهور عالم شد کمال</p></div>
<div class="m2"><p>معنی خاص است و بس کو شهرت عامم دهد</p></div></div>