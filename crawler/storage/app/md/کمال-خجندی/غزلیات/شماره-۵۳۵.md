---
title: >-
    شمارهٔ ۵۳۵
---
# شمارهٔ ۵۳۵

<div class="b" id="bn1"><div class="m1"><p>وصل او مانده چرا دولت دنیا طلبید</p></div>
<div class="m2"><p>دولتی را که به از دینی و عقبی طلبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوستداران به جز از دوست خواهید ز دوست</p></div>
<div class="m2"><p>که نباشد به ازو هر چه ازو میطلبید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کنید از سر هستی هوس خاک درش</p></div>
<div class="m2"><p>از پس خاک شدن جنت اعلی طلبید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش بالا و لب او خنکیهاست همه</p></div>
<div class="m2"><p>سایه و آب که از کوثر و طوبی طلبید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوش داروست لبش درد ندارید دریغ</p></div>
<div class="m2"><p>چند شربت ز شفاخانه عیسی طلبید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پسر تربت مجنون چو بسوزید عبیر</p></div>
<div class="m2"><p>شکر و عود ز خال و لب لیلی طلبید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر از میکده پرسید خبرهای کمال</p></div>
<div class="m2"><p>تا کی اش بر سر سجاده تقوی طلبید</p></div></div>