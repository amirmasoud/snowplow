---
title: >-
    شمارهٔ ۱۰۶۸
---
# شمارهٔ ۱۰۶۸

<div class="b" id="bn1"><div class="m1"><p>ندارد دلم طاقت بی توی</p></div>
<div class="m2"><p>که کردست چشم توام جادوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نو ابروبت ساخت شیدا مرا</p></div>
<div class="m2"><p>چنینها کند ماه نو در توی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشودند چشمان تو ترک و هند</p></div>
<div class="m2"><p>به ناوک کشی و کمان ابروی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه دولت که آن پای را در سرست</p></div>
<div class="m2"><p>که دارد به زلف تو هم زانوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ایام بدحالی از جور زلف</p></div>
<div class="m2"><p>رخت کرد با ما بسی نیکوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مصور اگر نسخه زان رخ برد</p></div>
<div class="m2"><p>به معنی کشد صورت مانوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال آن سر زلف هر دم نگیر</p></div>
<div class="m2"><p>که بارش بجنبد رگ هندوی</p></div></div>