---
title: >-
    شمارهٔ ۲۸۱
---
# شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>هوس بار گر آزار دل افگار است</p></div>
<div class="m2"><p>نخورد غم دل انگار که با آن یار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب وصلت سخن از صبر نگویم که کم است</p></div>
<div class="m2"><p>فتنه شوق چه گویم به تو چون بسیار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکند عاشق تالانت ز غم روی تو خواب</p></div>
<div class="m2"><p>عندلیب از هوس گل همه شب بیدار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از توأم هر شرف و قدر که می باید هست</p></div>
<div class="m2"><p>قیمتی نیست مرا پیش تو این مقدار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز وصل توأم از بهر نثار قدمت</p></div>
<div class="m2"><p>کاش سر نیز در میبود چو چشمم چار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه دیدار تو صد بار شود دیده مرا</p></div>
<div class="m2"><p>دیده را بار دگر آرزوی دیدار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صوفیان مست شدند از سخنان تو کمال</p></div>
<div class="m2"><p>که در انفاسی تو بوی سخن عطار است</p></div></div>