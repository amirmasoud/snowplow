---
title: >-
    شمارهٔ ۱۰۱۷
---
# شمارهٔ ۱۰۱۷

<div class="b" id="bn1"><div class="m1"><p>دارم ز ابروان تو چشم عنایتی</p></div>
<div class="m2"><p>کر نازم اره کشی نکنندم حمایتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم تو بیگه کش و من زنده همچنین</p></div>
<div class="m2"><p>از غمزه تو نیست جز اینم شکایتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیرون از آن که بی‌تو نخواهم وجود خویش</p></div>
<div class="m2"><p>از بنده در وجود نیاید جنایتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رویت که آیتیست ز رحمت بر ابروان</p></div>
<div class="m2"><p>زاهد چو دید خواند به محراب آیتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنی که دارد آن به و این غم کرو مراست</p></div>
<div class="m2"><p>آن غایتی ندارد و این هم نهایتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش رقیب قدر سگ کو شناختم</p></div>
<div class="m2"><p>کو می‌کند بندر گدارا رعایتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بر درت رقیب گدا باش با کمال</p></div>
<div class="m2"><p>غوغا بود دو پادشه اندر ولایتی</p></div></div>