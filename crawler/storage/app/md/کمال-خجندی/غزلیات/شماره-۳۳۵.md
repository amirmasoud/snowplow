---
title: >-
    شمارهٔ ۳۳۵
---
# شمارهٔ ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>با من درد کش سبو بدهید</p></div>
<div class="m2"><p>متنی بر سرم از و بنهید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بار ساقیست ابها العشاق</p></div>
<div class="m2"><p>توبه گر بشکنید بی گنهید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عشق اگر دهند انصاف</p></div>
<div class="m2"><p>زاهدان بی ره و شما به رهید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه شه رخ نماید از چپ و راست</p></div>
<div class="m2"><p>که چو فرزین نشسته پیش شهید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای طبیان بدرد عشق حبیب</p></div>
<div class="m2"><p>شربت تا مخالفم مدهید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرهم جانستان دهید مرا</p></div>
<div class="m2"><p>تا ز درد سرم چو من برهید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سماعی که نیست شعره کمال</p></div>
<div class="m2"><p>صوفیان هر یک از سوی بجهید</p></div></div>