---
title: >-
    شمارهٔ ۶۶۳
---
# شمارهٔ ۶۶۳

<div class="b" id="bn1"><div class="m1"><p>هوای وصل تو دارد غریق بحر فراق</p></div>
<div class="m2"><p>چو تشنه که به آب روان بود مشتاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شنیده ام که سگم خوانده عفاک الله</p></div>
<div class="m2"><p>من قیر بدین هم ندارم استحقاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار بار به گرد جهان مه و خورشید</p></div>
<div class="m2"><p>بر آمدند و نظیرت ندید در آفاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اساس عقل بر افتاد تا به ابرو و چشم</p></div>
<div class="m2"><p>بنای حسن نهادی و بر کشیدی طاق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حدیث زلف درازت به گوش جان چو رسید</p></div>
<div class="m2"><p>به هم برآمد از آن حلقه حلقه عشاق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحیفهای ملون حواشی گل را</p></div>
<div class="m2"><p>فروغ روی نو آتش فکند در اوراق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوند اهل سپاهان غلام طبع کمال</p></div>
<div class="m2"><p>گر این دو بیت سرایند مطربان عراق</p></div></div>