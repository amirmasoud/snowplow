---
title: >-
    شمارهٔ ۶۵۱
---
# شمارهٔ ۶۵۱

<div class="b" id="bn1"><div class="m1"><p>کجا کنند به تیغ از تو عاشقان اعراض</p></div>
<div class="m2"><p>ز شمع باری پروانه کی برد مقراض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا که بر تو کم عرض سوز و در نهان</p></div>
<div class="m2"><p>که از طبیب نپوشند خستگان امراض</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به لعل و در نکند نسبت آن لب و دندان</p></div>
<div class="m2"><p>که لطف جواهر شناسد از اعراض</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل کدام ریاضت بود قوی تر از آن</p></div>
<div class="m2"><p>تو مستعد نظر شو کمال و قابل فیض</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سواد چشم من از گریه شد تر و ابتر</p></div>
<div class="m2"><p>کنون محرر اشکم همی برد به بیاض</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بوی دوست شنیدیم و کوی او دیدیم</p></div>
<div class="m2"><p>دگر هوای ریاحین چرا کنیم و ریاض</p></div></div>