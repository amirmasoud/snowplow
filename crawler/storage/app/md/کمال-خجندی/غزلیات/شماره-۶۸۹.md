---
title: >-
    شمارهٔ ۶۸۹
---
# شمارهٔ ۶۸۹

<div class="b" id="bn1"><div class="m1"><p>باز در عشق یکی دل به غلامی دادم</p></div>
<div class="m2"><p>خواجه را گو که بیاید به مبارکبادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنده را از تو چه جای گله، آزادیهاست</p></div>
<div class="m2"><p>هست جای گله وقتی که کنی آزادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو چه شاخ گلی و بی تو مرا رخ شده زرد</p></div>
<div class="m2"><p>منم آن برگ که از شاخ جدا افتادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر فلک ناله من گوش ملایک کر ساخت</p></div>
<div class="m2"><p>آه اگر در دل شبها شنوی فریادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از می عشق تو ساقی قدحی داد مرا</p></div>
<div class="m2"><p>که می خلد و لب حور برفت از بادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تو بنیاد نهم باتز طرب خانه عشق</p></div>
<div class="m2"><p>طاق ابروی تو گر بر نکنه بنیادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الف قد تو آن روز برد راه کمال</p></div>
<div class="m2"><p>که به مکتب الف و بی بنوشت استادم</p></div></div>