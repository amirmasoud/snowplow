---
title: >-
    شمارهٔ ۲۴۳
---
# شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>ما را نه غم ننگ و نه اندیشه نام است</p></div>
<div class="m2"><p>در مذهب ما مذهب ناموس حرام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو خلق بدانید که پیوسته فلان را</p></div>
<div class="m2"><p>رخ بر رخ جانانه و لب بر لب جام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سجاده نشین عارف و دانا نه که عامی است</p></div>
<div class="m2"><p>مادام که در بند قبولیت عام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آرزوی مجلس ما زاهد مغرور</p></div>
<div class="m2"><p>چون عود همی سوزد و این طرفه که خام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی می دوشینه اگر رفت به اتمام</p></div>
<div class="m2"><p>ما را ز لب لعل تو یک جرعه تمام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سودا زده را گوشه سجاده نسازد</p></div>
<div class="m2"><p>ای مطرب ره زن ره میخانه کدام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برخاست کمال از ورع و گوشه نشینی</p></div>
<div class="m2"><p>چون دید که میخانه به از هر دو مقام است</p></div></div>