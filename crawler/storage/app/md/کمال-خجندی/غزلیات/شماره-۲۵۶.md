---
title: >-
    شمارهٔ ۲۵۶
---
# شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>مرد بی درد مرد این ره نیست</p></div>
<div class="m2"><p>غافل از ذوق درد آگه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی رخ زرد و اشک سرخ بر رو</p></div>
<div class="m2"><p>دعوی عاشقی موجه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روشن و خوش صباح زنده دلان</p></div>
<div class="m2"><p>ا جز به بیداری سحرگه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سالک باکرو نخوانندش</p></div>
<div class="m2"><p>آنکه از م اسوی منزه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آستین کوته است شیخ چه سود</p></div>
<div class="m2"><p>چون از دنیاش دست کوته نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواجه تا کی زند زهستی دم</p></div>
<div class="m2"><p>که شود زیر خاک ناگه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان برین خاک ره فشاند کمال</p></div>
<div class="m2"><p>گر زند لال عشق بیره نیست</p></div></div>