---
title: >-
    شمارهٔ ۵۳۴
---
# شمارهٔ ۵۳۴

<div class="b" id="bn1"><div class="m1"><p>ورق روی تو عشاق نکو می خوانند</p></div>
<div class="m2"><p>چون رسد کار به زلفت همه در میمانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورنت صاحب معنی ز ملک به دانست</p></div>
<div class="m2"><p>لیکن اهل نظرته بهتر ازین میدانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساعد و دست نوام بیم نمایند به نیغ</p></div>
<div class="m2"><p>نشته را این همه از آب چه می ترسانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفتی و ماند خیال دهنت با دل تنگ</p></div>
<div class="m2"><p>چرا ندارند اثری هر دو بهم می مانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کنم پیش رقیبان بقدت نسبت تی</p></div>
<div class="m2"><p>تا چو آنی بسر و چشم منت بنشانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک را خاک درت سایل بیحاصل خواند</p></div>
<div class="m2"><p>هر که شد راندهٔ درگاه چنینش خوانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند پوشی ز کسان راز دل و دیده کمال</p></div>
<div class="m2"><p>کین دو چون امر محال است که پنهان مانند</p></div></div>