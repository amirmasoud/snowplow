---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>عشق تو سراسر همه سوز و همه دردست</p></div>
<div class="m2"><p>رین شیوه به اندازه مردی است که مردست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکس که درین صرف نکردست همه عمر</p></div>
<div class="m2"><p>بیچاره ندانم که همه عمر چه کردست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد چه عجب گر کند از عشق تو پرهیز</p></div>
<div class="m2"><p>کس لذت این باده چه داند که نخوردست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق که نه گرمست چو شمع از سر سوزی</p></div>
<div class="m2"><p>گر آتش محض است به جان تو که سردست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشکی که بود سرخ چو رخسار تو داریم</p></div>
<div class="m2"><p>ما را ز تو نشریف نه تنها رخ زردست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی شب که بر آن در من خاکی ز ضعیفی</p></div>
<div class="m2"><p>بنشستم و پنداشت رقیب نو که گردست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر هست کمال از دو جهان فرد عجب نیست</p></div>
<div class="m2"><p>این نیز کمالی است که آزاده و فردست</p></div></div>