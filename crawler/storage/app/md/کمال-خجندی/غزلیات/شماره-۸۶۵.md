---
title: >-
    شمارهٔ ۸۶۵
---
# شمارهٔ ۸۶۵

<div class="b" id="bn1"><div class="m1"><p>شه لشکر کش ما برد از ما عقل و هوش و دین</p></div>
<div class="m2"><p>چرا آن ترک کافر کیش غارت می کند چندین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن صف کو سپه رائد به قصد غارت دلها</p></div>
<div class="m2"><p>دلی کآنجا نخواهه شد اسیر او زهی مسکین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دود آه خود با او رساندم سوخت چشمانش</p></div>
<div class="m2"><p>چه بینی زرق خود صوفی تو کافر سوزی من بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهانگیری همین باشد که چون برقع براندازی</p></div>
<div class="m2"><p>رخت فی الحال بگشاید خط زلفت بگیرد چین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا هر لحظه با تیر تو جنگ زرگری باشد</p></div>
<div class="m2"><p>چو بیئم نوک آن پیکان به خون دیگری رنگین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به گلگون گر هوس داری که بنشینی به شیرینی</p></div>
<div class="m2"><p>دوچشمم شد به خون گلگون بیا بر چشم من بنشین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال امسال چندی شد غزل بر اسب گفت اکثر</p></div>
<div class="m2"><p>سخنهایه سراسری نباشد غالبا به زین</p></div></div>