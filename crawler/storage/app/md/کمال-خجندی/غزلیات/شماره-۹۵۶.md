---
title: >-
    شمارهٔ ۹۵۶
---
# شمارهٔ ۹۵۶

<div class="b" id="bn1"><div class="m1"><p>باز خود را چو گل تازه بر آراسته ای</p></div>
<div class="m2"><p>باغ رخسار بگلهای نر آراسته ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق بر یکدگر افتاده ز نظاره تو</p></div>
<div class="m2"><p>که دو رخ خوبتر از یکدگر آراسته ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابروی شوخ که با ماه نوش سر بسر است</p></div>
<div class="m2"><p>بسر زلف سیه سر بسر آراسته ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوخی و فتنه گر و سنگدل و عهد شکن</p></div>
<div class="m2"><p>چشم بد دور بچندین هنر آراسته ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با وجود لب تو نیست بسائی محتاج</p></div>
<div class="m2"><p>مجلس ما که بنقل و شکر آراسته ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست مهمان نور آن به مگر ای دل که ز اشک</p></div>
<div class="m2"><p>خانه دیده بلعل و گهر آراستهای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی آراسته بنمای خصوصة به کمال</p></div>
<div class="m2"><p>که تو خاص از پی اهل نظر آراسته ای</p></div></div>