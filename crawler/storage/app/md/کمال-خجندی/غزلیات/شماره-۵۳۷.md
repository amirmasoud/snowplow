---
title: >-
    شمارهٔ ۵۳۷
---
# شمارهٔ ۵۳۷

<div class="b" id="bn1"><div class="m1"><p>هدایه خواندی و هیچت هدایتی نرسید</p></div>
<div class="m2"><p>عناه کشید و زآن سو عنایتی نرسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خوان علم که پر نقل حکمت است ترا</p></div>
<div class="m2"><p>برون ز نقل حدیث و روایتی نرسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گوش نهادی شبه نزول و همین</p></div>
<div class="m2"><p>ز لحن غیب بگوش تو آبتی نرسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا چه سود بروز جزا و قابه و جزو</p></div>
<div class="m2"><p>چو از وقابة عفوش حمایتی نرسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندیده دیده بینا بابت این راه</p></div>
<div class="m2"><p>به هر مکاشفه تا در نهایتی نرسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سالکان خبری یافتم بغایت راست</p></div>
<div class="m2"><p>که یک رونده درین ره بغایتی نرسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن دهان که «ذو العلم» لفظ اوست کمال</p></div>
<div class="m2"><p>بگوش حرف شنو جز حکایتی نرسید</p></div></div>