---
title: >-
    شمارهٔ ۵۷۷
---
# شمارهٔ ۵۷۷

<div class="b" id="bn1"><div class="m1"><p>چهره ام دیده چه حاصل که به خون کرد نگار</p></div>
<div class="m2"><p>که برون نقش و نگارست و درون ناله زار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بار گویند که دارد سر عاشق کشتن</p></div>
<div class="m2"><p>خبر عاشقی من برسانید به یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این محال است که ما هر دو ز هم می طلبیم</p></div>
<div class="m2"><p>من ز تو مهر و وفا و تو ز من صبر و قرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن در ساعد منما بیش به صاحب نظران</p></div>
<div class="m2"><p>که ربودی دل خلقی ز یمین وز یسار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مژه تا خاک درت پیشتر از دیده برفت</p></div>
<div class="m2"><p>در میان مژه و دیده فتاد است غبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب می است و بدنت سیم چو هست اینهمه خام</p></div>
<div class="m2"><p>خام باشد ز تو ما را طمع بوس و کنار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر در ناله و فریاد بسر برد کمال</p></div>
<div class="m2"><p>در تو درد دل او کار نکرد آخر کار</p></div></div>