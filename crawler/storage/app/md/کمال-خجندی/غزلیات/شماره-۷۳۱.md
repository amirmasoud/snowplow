---
title: >-
    شمارهٔ ۷۳۱
---
# شمارهٔ ۷۳۱

<div class="b" id="bn1"><div class="m1"><p>رحمت آری و کنی چاره این درد نهانم</p></div>
<div class="m2"><p>گر بدانی که ز هجر تو چسان میگذرانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند در کوی تو بربوی تو برخاک نشینم</p></div>
<div class="m2"><p>آتش سینه به آب مژه تا چند نشانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کمند خودم آوردی و چون نیر بجستی</p></div>
<div class="m2"><p>کی کمان ابروی من بر نو به این بود گمانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی زردم نگر و روی گردان که نشاید</p></div>
<div class="m2"><p>اشک من بین و چو اشک از نظر خویش مرانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان به رسمی چو به پای تو کمال اندازد</p></div>
<div class="m2"><p>قدمی رنجه کن ای جان و ز خود باز رهانم</p></div></div>