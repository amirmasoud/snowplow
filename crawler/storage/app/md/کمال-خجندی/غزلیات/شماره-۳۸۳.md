---
title: >-
    شمارهٔ ۳۸۳
---
# شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>چو آن شاخ گل از بستان بر آمد</p></div>
<div class="m2"><p>زهر شاخی گلی از پا درآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان پر شد زچشمت چشم نرگس</p></div>
<div class="m2"><p>بازار س که آب خجلتش از سر بر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زدی لاف نهال گل به آن سرو</p></div>
<div class="m2"><p>گل تو یک ورق زآن دفتر آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرو رفتم به حیرت زآن رخ و زلف</p></div>
<div class="m2"><p>که چون از لاله سبزه بر سر آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چوبستان پر شد از بوی خوش او</p></div>
<div class="m2"><p>دگر بوی گل آنجا کمتر آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمال آن به کز او بابی نسیمی</p></div>
<div class="m2"><p>که از صد بوی گل آن خوشتر آمد</p></div></div>