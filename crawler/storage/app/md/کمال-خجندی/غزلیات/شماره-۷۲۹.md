---
title: >-
    شمارهٔ ۷۲۹
---
# شمارهٔ ۷۲۹

<div class="b" id="bn1"><div class="m1"><p>دوش با خود ترانه می‌گفتم</p></div>
<div class="m2"><p>غزل عاشقانه می‌گفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام بر کف حکایت لب بار</p></div>
<div class="m2"><p>به شراب مغانه می‌گفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیم از زلف او چو بود دراز</p></div>
<div class="m2"><p>با خیالش نسانه می‌گفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفت دانه‌های گوهر اشک</p></div>
<div class="m2"><p>پیش در یگانه می‌گفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در میان ستاره‌ها مه را</p></div>
<div class="m2"><p>پیش حسنش سهانه می‌گفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمزه‌اش را چو نی می‌گفتند</p></div>
<div class="m2"><p>دل خود را نشانه می‌گفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آتش روی مجلس‌افروزش</p></div>
<div class="m2"><p>شمع را یک زبانه می‌گفتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر زلفش چو شانه می‌زد باد</p></div>
<div class="m2"><p>اصلح الله شانه می‌گفتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ز سر می‌گذشت آب دو چشم</p></div>
<div class="m2"><p>با کس این ماجرا نه می‌گفتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا دم صبح سرگذشت کمال</p></div>
<div class="m2"><p>سر بر آن آستانه می‌گفتم</p></div></div>