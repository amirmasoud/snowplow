---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>از گلستان رخت حسن بتان یک ورق است</p></div>
<div class="m2"><p>حالیا از ورق عشق تو اینم سبق است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن گل کم شد و مشتاقی بلبل هم کاست</p></div>
<div class="m2"><p>عشق من برنوجوحسنت به همان یک نسق است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چرا در شب هجران توأم زنده هنوز</p></div>
<div class="m2"><p>تن رنجور من از خجلت آن در عرق است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اتفاق تو گر این است که خونم ریزی</p></div>
<div class="m2"><p>هرچه رأی نو دل و دیده بر آن متفق است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل باطل شمرد چشم تو هر خون که کند</p></div>
<div class="m2"><p>غالبا بی خبر از نکتة العین حق است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهد از شوق حدیث تو قلم سوخت کمال</p></div>
<div class="m2"><p>در قلم خود سختی نیست سخن در ورق است</p></div></div>