---
title: >-
    شمارهٔ ۳۰۴
---
# شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>آنها که لب چون شکرستان نو پابند</p></div>
<div class="m2"><p>آن نقل همان در خور دندان نو پابند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیر قدمت خاک شده جان عزیزست</p></div>
<div class="m2"><p>هر گرد که بر گوشه دامان تو پابند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چشمه حیوان نئوان بافت همه عمر</p></div>
<div class="m2"><p>آن لطف که در چاه زنخدان پر پابند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنجا که به خط سبز کنی خوان ملاحت</p></div>
<div class="m2"><p>طاووس ملایک مگس خوان تو پابند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خاک شهیدان گل رحمت شکنانه</p></div>
<div class="m2"><p>مر غنچه که در سینه ز پیکان نو پابند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زینگونه که من بافتم آن لعل روان بخش</p></div>
<div class="m2"><p>گر جوی بهشت است که جویان پر پابند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جنت طلبان هرچه بجویند ز طوبی</p></div>
<div class="m2"><p>در قامت چون سرو خرامان تو بابند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر خضر بقا چون خطت از آب بقا بافت</p></div>
<div class="m2"><p>عشاق حبات از لب خندان نو پابند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بردی دل عشاق کمال از سخن خوب</p></div>
<div class="m2"><p>خوبان عمل نه ز دیوان نو پابند</p></div></div>