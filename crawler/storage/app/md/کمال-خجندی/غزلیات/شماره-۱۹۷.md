---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>عشق آئین پارسایان نیست</p></div>
<div class="m2"><p>سلطنت رسم بینوایان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می به صوفی مده که آن صافی</p></div>
<div class="m2"><p>در خور حال هی صفایان نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر آن دل که برقرار خودست</p></div>
<div class="m2"><p>واقف از حال بیقراران نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار بیگانه شد چنان امروز</p></div>
<div class="m2"><p>کش دگر بار آشنایان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه مشغول نعمت و نازست</p></div>
<div class="m2"><p>هیچش اندوه بینوایان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دولت وصل خواستم گفتند</p></div>
<div class="m2"><p>سلطنت در خور گدایان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهبران چون کمال این ره را</p></div>
<div class="m2"><p>سالها رفته اند و پایان نیست</p></div></div>