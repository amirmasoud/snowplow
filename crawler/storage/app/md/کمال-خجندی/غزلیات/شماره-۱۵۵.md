---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>دل قبله خود خاک سر کوی تو دانست</p></div>
<div class="m2"><p>جان طاعت احسن هوس روی تو دانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محراب دو شد زاهد سجاده نشین را</p></div>
<div class="m2"><p>ز آن روز که محراب دو ابروی تو دانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق ز دل و دین نظر عقل بپوشید</p></div>
<div class="m2"><p>تا کافری غمزه جادوی نور دانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل از په عشق عنان باز به پیچید</p></div>
<div class="m2"><p>تا سلسله جنبانی گیوی نو دانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وجه نظر و دور و نلل به بدیهی</p></div>
<div class="m2"><p>عقل از نظر روی تو و موی تو دانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این نکته که کسی را ز تو نه رنگ و نه بویست</p></div>
<div class="m2"><p>از رنگ تو دریافت دل از بوی نو دانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیش است کمال از همه زان روز که خود را</p></div>
<div class="m2"><p>در مرتبه کمتر ز سگ کوی تو دانست</p></div></div>