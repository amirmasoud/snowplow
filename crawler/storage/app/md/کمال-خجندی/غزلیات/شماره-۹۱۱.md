---
title: >-
    شمارهٔ ۹۱۱
---
# شمارهٔ ۹۱۱

<div class="b" id="bn1"><div class="m1"><p>گفته‌ای از ما دلت بردار زنهار این مگو</p></div>
<div class="m2"><p>جان من با آن لب و گفتار زنهار این مگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفته راه وفا ما نیکه نتوانیم رفت</p></div>
<div class="m2"><p>با چنان قد خوش و رفتار زنهار این مگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته خواهم بریدن از تو دیگر باره مهر</p></div>
<div class="m2"><p>هم به مهر خود که دیگر بار زنهار این مگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفته صبح امیدت من نیاوردم به شام</p></div>
<div class="m2"><p>از رخ و از زلف شرمی دار زنها این مگو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته در آفتاب و به توان هرگز رسید</p></div>
<div class="m2"><p>وصل رویم هم همان انگار زنهار این مگو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته آب خوشی هرگز کسی خورد از سراب</p></div>
<div class="m2"><p>وعده ما هم همان پندار زنهار این مگو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته از دوستی جان خودم خواندی کمال</p></div>
<div class="m2"><p>هرچه گونی این مگو زنهار زنهار این مگو</p></div></div>