---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>سروی ز باغ حسن به لطف قدت نخاست</p></div>
<div class="m2"><p>از آن برتر است قد تو کآید به شرح راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان به لب رسیده ما را به بوسه‌ای</p></div>
<div class="m2"><p>دریاب کز دهان تو در معرض فناست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا از لبت وظیفهٔ دشنام کرده‌ای</p></div>
<div class="m2"><p>دعای دولت تو بر زبان ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر رشته قرار شد از دست و همچنان</p></div>
<div class="m2"><p>انگشت پیچ چون سخن زلف دلرباست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را از روی خوب مکن منع ای فقیه</p></div>
<div class="m2"><p>کین فق در شریعت اهل نظر رواست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنجا که خادمان ز عقیدت نفس زنند</p></div>
<div class="m2"><p>از ما سر ارادت و از دوست خاک پاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر شبوة کمال بپرسد کسی زانو</p></div>
<div class="m2"><p>گوسوفی ایست و رند ولی بارسا نماست</p></div></div>