---
title: >-
    شمارهٔ ۷۵۲
---
# شمارهٔ ۷۵۲

<div class="b" id="bn1"><div class="m1"><p>صحبت یارِ بهشتی‌ست پُر از ناز و نعیم</p></div>
<div class="m2"><p>ای خوش آن دم که به ما آید از آن روضه نسیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حور چشم سیهش خواند به از نرگس و گفت</p></div>
<div class="m2"><p>نتوان خوانْد ازین بِهْ که سوادی‌ست سقیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست خاک در او خالی از آمدشدِ اشک</p></div>
<div class="m2"><p>رو نهاد سایلِ افتاده به درگاه کریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌کنم بام و در دیده به خون مژه نقش</p></div>
<div class="m2"><p>تا شوی ار پی نظاره در آن گوشه مقیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک دلی دارم و خواهد ز من آن غمزه و خال</p></div>
<div class="m2"><p>تیغ هندی‌ست چو در پیش تو سازش به دو نیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده نقش دهنت دل به رخت فال گشاد</p></div>
<div class="m2"><p>بی‌ملامت کشد از عشق تو چون آید میم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر قدم‌های تو خواهد که زند بوسه کمال</p></div>
<div class="m2"><p>باز بنما قدمی خاصه به مشتاق قدیم</p></div></div>