---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>طاقت درد تو زین بیش ندارم بارا</p></div>
<div class="m2"><p>چاره ای کن به نظر درد دل شیدا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوس روی توأم کرد پریشان احوال</p></div>
<div class="m2"><p>زلفت انداخت مگر در دل من سودا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کسی را ز لبت لذت جان حاصل شد</p></div>
<div class="m2"><p>کام بی ذوق چه داند مزه این حلوا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طاقت خنده ندارد لبت از غایت لطف</p></div>
<div class="m2"><p>به سخن رنجه مکن آن لب شکر خارا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کند باد صبا غالیه سانی به چمن</p></div>
<div class="m2"><p>برفشان بر سر گل سنبل عنبر سا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصف روی تو کمال ار نکند نقصان نیست</p></div>
<div class="m2"><p>نبود حاجت مشاطه رخ زیبا را</p></div></div>