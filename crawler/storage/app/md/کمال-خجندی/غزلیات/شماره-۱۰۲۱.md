---
title: >-
    شمارهٔ ۱۰۲۱
---
# شمارهٔ ۱۰۲۱

<div class="b" id="bn1"><div class="m1"><p>دگر باره تیغ جفا بر کشیدی</p></div>
<div class="m2"><p>ز باران دیرینه باری بریدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قتل محبان شدی باز رنجه</p></div>
<div class="m2"><p>بنابادت ای دوست زحمت کشیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از حسرتت گرچه مردم خوشم هم</p></div>
<div class="m2"><p>که باری تو با آرزویی رسیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا هر چه گفتیم گفتی شنیدم</p></div>
<div class="m2"><p>حدیثی شنیدی ولی کی شنیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه دانی ز حال من ای جان شیرین</p></div>
<div class="m2"><p>که تو تلخی هجر کمتر کشیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکوی تو چون آب هرگز نرفتم</p></div>
<div class="m2"><p>که چون سرو دامن ز من در کشیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال آرزو داشتی خاک پایش</p></div>
<div class="m2"><p>به چشم خود الحمد لله که دیدی</p></div></div>