---
title: >-
    شمارهٔ ۴۷۳
---
# شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>غنچه از رشک دهان نو دهان گرد آورد</p></div>
<div class="m2"><p>سوسن از تر سخنی تو دهان گرد آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواست اندیشه برد را به میان و بدهانت</p></div>
<div class="m2"><p>تنگ و باریک رهی دید عنان گرد آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رویت آورد خطی گرد که شدانده جان</p></div>
<div class="m2"><p>از کجا روی تو این انده جان گرد آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا که عطار چمن لخلخة زلف تو دید</p></div>
<div class="m2"><p>طبله در بیست و همه رخت دکان گرد آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به لبت لعل چه ماند که همان است آخر</p></div>
<div class="m2"><p>آنکه خورشید تو پروردش و گان گرد آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته ای جان و دلت زود بگیریم کمال</p></div>
<div class="m2"><p>تا که بشنید حدیث نو روان گرد آورد</p></div></div>