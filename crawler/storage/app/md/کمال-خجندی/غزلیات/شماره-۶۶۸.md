---
title: >-
    شمارهٔ ۶۶۸
---
# شمارهٔ ۶۶۸

<div class="b" id="bn1"><div class="m1"><p>ای رایت جمال تو نقش نگین دل</p></div>
<div class="m2"><p>عشقت نهاده داغ جفا بر جبین دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلوتسرای عشق تو دارالامان جان</p></div>
<div class="m2"><p>مشکل گشای سر تو روح الامین دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دامن وصال تو خواهم که ریختن</p></div>
<div class="m2"><p>نقد وجود خویشتن از آستین دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب چه تیره میشودم روز زندگی</p></div>
<div class="m2"><p>زیرا که نیست صبح وصالت قرین دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حال زار خویش کمال ار کند بیان</p></div>
<div class="m2"><p>حاجت روا کنی که ندارد حزین دل</p></div></div>