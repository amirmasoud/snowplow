---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>گنجی و نرا بیطلبیدن نتوان یافت</p></div>
<div class="m2"><p>راحت ز تو بی رنج کشیدن نتوان یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شربت خاصی که شفای همه جانهاست</p></div>
<div class="m2"><p>بی چاشنی درد پشیدن نتوان یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داری سر یوسف ببر از هر چه عزیز است</p></div>
<div class="m2"><p>امکان وصل به یک دست بریدن نتوان یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن بخت که در دامن وصلش برسد دست</p></div>
<div class="m2"><p>بی پیرهن صبر دریدن نتوان یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بر ظلکم دست رسد، بر تو محال است</p></div>
<div class="m2"><p>کان پایه به صد عرش رسیدن نتوان یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با گرم روی واقف این راه چه خوش گفت</p></div>
<div class="m2"><p>آهسته که این را به دویدن نتوان یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر خلق شئو آنچه کمال از دهنت گفت</p></div>
<div class="m2"><p>زین جنس معما بشنیدن نتوان یافت</p></div></div>