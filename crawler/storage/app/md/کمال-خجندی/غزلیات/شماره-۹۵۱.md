---
title: >-
    شمارهٔ ۹۵۱
---
# شمارهٔ ۹۵۱

<div class="b" id="bn1"><div class="m1"><p>ما جگر سوختگان داغ تو داریم همه</p></div>
<div class="m2"><p>مرهمی بخش که مجروح و نگاریم همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سافیا گر نظری هست به مخمورانت</p></div>
<div class="m2"><p>بدو چشم تو که در عین خماریم همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد دردی زخم عشق به پیمانه برار</p></div>
<div class="m2"><p>کز طرب نعره مستانه برآریم همه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیل مژگان و نم دیده اگر می طلبی</p></div>
<div class="m2"><p>هر چه زینها طلبی در نظر آریم همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مفلسانیم اگر دست نداریم به هیچ</p></div>
<div class="m2"><p>چون تو داریم به معنی همه داریم همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود عهدی که نگیریم دمی بیتو قرار</p></div>
<div class="m2"><p>همچنان بر سر عهدیم و قراریم همه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر و جان خواستی ای جان گرامی ز کمال</p></div>
<div class="m2"><p>همه سهل است بیا تا بسپاریم همه</p></div></div>