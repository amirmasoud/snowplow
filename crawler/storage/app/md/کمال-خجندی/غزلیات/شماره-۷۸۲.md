---
title: >-
    شمارهٔ ۷۸۲
---
# شمارهٔ ۷۸۲

<div class="b" id="bn1"><div class="m1"><p>ما درین شهر به دام صنمی در بندیم</p></div>
<div class="m2"><p>که به دشنام ازو شاد و به غم خرسندیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در غم فرقت او ناله کنان با دل ریش</p></div>
<div class="m2"><p>گه گهی زار بگرییم و گهی می خندیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو پرگار ز باریم جدا سرگران</p></div>
<div class="m2"><p>تا درین دایره کی باز به هم پیوندیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دل سوخته ما چه خبر دارد شمع</p></div>
<div class="m2"><p>بیش ازین نیست که در گریه به هم مانندیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک ره از باد صبا پرس که ما دلشدگان</p></div>
<div class="m2"><p>جمع در حلقه آن زلف پریشان چندیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرح آن زلف پراکنده دراز است مپرس</p></div>
<div class="m2"><p>بهتر آنست کز آن قصه زبان در بند یم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه رندیم و نظر باز مکن عیب کمال</p></div>
<div class="m2"><p>این هنر بس که نه صوفی و نه دانشمند بم</p></div></div>