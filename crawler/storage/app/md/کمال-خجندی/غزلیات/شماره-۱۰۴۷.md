---
title: >-
    شمارهٔ ۱۰۴۷
---
# شمارهٔ ۱۰۴۷

<div class="b" id="bn1"><div class="m1"><p>گر بردرت این اشک چو سیلاب گذشتی</p></div>
<div class="m2"><p>در کوی تو این خس هم از این باب گذشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار مژه گر دور شدی از گذر اشک</p></div>
<div class="m2"><p>بر دیدة غمدیده شی خواب گذشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر پیرو این اشک شدی صوفی و این آه</p></div>
<div class="m2"><p>بر روی هوا رفتی و از آب گذشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابروی تو گر دیده شدی گوشه نشین راج</p></div>
<div class="m2"><p>از غصه و غم پشت ز محراب گذشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ان نیز گذشتی چو مگس ز آن لب شیرین</p></div>
<div class="m2"><p>گر زآنکه مگس از شکر ناب گذشتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز لاله که نمی رست کمال از ولیانکوه</p></div>
<div class="m2"><p>گر سیل سرشک تو ز سرخاب گذشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک نامه رسیدی بنو از جانب تبریز</p></div>
<div class="m2"><p>گر یاد تو بر خاطر احباب گذشتی</p></div></div>