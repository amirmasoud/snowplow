---
title: >-
    شمارهٔ ۹۶۰
---
# شمارهٔ ۹۶۰

<div class="b" id="bn1"><div class="m1"><p>لیست آن بگو با شکر خورده ای</p></div>
<div class="m2"><p>ز خود خورده باشی اگر خورده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا میدهد زآن دهان بوی جان</p></div>
<div class="m2"><p>چو دایم ز لبها جگر خورده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرم با سگ خویش بخشی نصیب</p></div>
<div class="m2"><p>غم من ازو بیشتر خورده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا با من ای کاه یکرنگی است</p></div>
<div class="m2"><p>مگر با رخ بنده زر خورده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از المطاف آن غمزه ای دل منال</p></div>
<div class="m2"><p>چو هر لحظه تیری دگر خورده ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سرگشتگیهای ما ای صبا</p></div>
<div class="m2"><p>تو دانی که گرد سفر خورده ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو آن سرو دیدی یقین دان کمال</p></div>
<div class="m2"><p>که از شاخ امید بر خورده ای</p></div></div>