---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>ما عاشقیم و رند، خرابات گوی ماست</p></div>
<div class="m2"><p>روی شرابخانه و عشرت بوی ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شیخ اگر به صومعه ها دارو گیر نیست</p></div>
<div class="m2"><p>ا مارخانه ها همه پر های و هوی ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما با کسی نگفته حدیثی میان شهر</p></div>
<div class="m2"><p>هر جا که مجمعی ست همه گفتگوی ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را به رنگ و بوی جهان التفات نیست</p></div>
<div class="m2"><p>گلزار دمر اگر چه پر از رنگ و بوی ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آبی کز آن حیات ابد بافت جان خضر</p></div>
<div class="m2"><p>از ما بجو که رشحه رشیع سبوی ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی شرابخوارگی و عشق خوی تست</p></div>
<div class="m2"><p>آری شرابخوارگی و عشق خوی ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کردم ز دوست آرزونی گفت ای کمال</p></div>
<div class="m2"><p>بگذر ز آرزو اگرت آرزوی ماست</p></div></div>