---
title: >-
    شمارهٔ ۸۰۱
---
# شمارهٔ ۸۰۱

<div class="b" id="bn1"><div class="m1"><p>من همچو گردم در رهت زآن رو طلبکار توام</p></div>
<div class="m2"><p>مهر تو دارم ذره سان وز جان هوادار توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشنو که با یوسف چه گفت آن پیرزن گریه کنان</p></div>
<div class="m2"><p>گر بر درم قادر نیم باری خریدار توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر خشت کز خاکم زند دست اجل کردم بحل</p></div>
<div class="m2"><p>لیکن به شرطی کافکند درپای دیوار توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک شب غمت در میزدی گفتم که آیا کیست آن</p></div>
<div class="m2"><p>گفتا درم بگشا که من یار وفادار توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر آیدم باز آن طبیب این نکته خواهم گفتنش</p></div>
<div class="m2"><p>حالم چه می پرسی چو میدانی که بیمار توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخت آید از تیغت مرا گو هر نفس بر هم زند</p></div>
<div class="m2"><p>گر راحتی بر دل رسد از لطف آزار توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی کمال از کار خود غافل مشو کاری بکن</p></div>
<div class="m2"><p>اینست کار من که شد سر در سر کار توام</p></div></div>