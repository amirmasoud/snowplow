---
title: >-
    شمارهٔ ۴۰۶
---
# شمارهٔ ۴۰۶

<div class="b" id="bn1"><div class="m1"><p>دل که از درد تو پر شد ناله را چون کم کند</p></div>
<div class="m2"><p>مرهم و درمان کجا این درد افزون کم کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خروش کشتگان گر زحمتی باشد ترا</p></div>
<div class="m2"><p>غمزه بیمار را فرمای تا خون کم کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب چشمم کم نشد چندانکه مژگان بر گرفت</p></div>
<div class="m2"><p>کس به پرویزن چگونه آب جیحون کم کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با دو صد گنج و گهر گر کردمت قیمت مرنج</p></div>
<div class="m2"><p>مشتری نیز از بهاری در مکنون کم کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بمن بوسی ببخشی کم نگردد زان جمال</p></div>
<div class="m2"><p>با زکات کی گدا از گنج قارون کم کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رشکم از زاهد نمی آید که گه گه بیندت</p></div>
<div class="m2"><p>طبع ناموزون چو میل شکل موزون کم کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چو شمع خلونت سوزد زبان مگشا کمال</p></div>
<div class="m2"><p>نصه سوز درون عاشق به بیرون کم کند</p></div></div>