---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>دوستان گر کشت ما را دوست ما دانیم و دوست</p></div>
<div class="m2"><p>چون هلاک ما رضای اوست ما دانیم و دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نوازد ور گدازد جان ما کس را چه کار</p></div>
<div class="m2"><p>ور به جان دشمن شود با دوست ما دانیم و دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده گربان ما در پای هر سرو و گلی</p></div>
<div class="m2"><p>گر بجست و جوی او چون جوست ما دائیم و دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس نداند از برای کیست رو بر خاک راه</p></div>
<div class="m2"><p>آنکه دایم بر سر آن کوست ما دانیم و دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند پیچیدن درین کز غم نشت شد رشته ای</p></div>
<div class="m2"><p>گر ازین غم کم ز ثار موست ما دانیم و دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این سخنها تا کیت گفتن که بیرحم است و مهر</p></div>
<div class="m2"><p>گردلش دل نیست سنگ و روستا دانیم و دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با نکو خواهان و بدگویان بگو از ما کمال</p></div>
<div class="m2"><p>دوست با ما بد و گر نیکوست ما دانیم و دوست</p></div></div>