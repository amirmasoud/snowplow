---
title: >-
    شمارهٔ ۵۳۳
---
# شمارهٔ ۵۳۳

<div class="b" id="bn1"><div class="m1"><p>نور چشمی تو ما را نظری می‌باید</p></div>
<div class="m2"><p>گر رسد صد نظر از تو دگری می‌باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز بنما رخ زیا چو بریدی سر زلف</p></div>
<div class="m2"><p>منقطع شد شب پره سحری می‌باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عبادت سخنی گوی که رنجوران را</p></div>
<div class="m2"><p>از شفاخانه آن لب شکری می‌باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توتیا را نتوانم که ببینم به دو چشم</p></div>
<div class="m2"><p>سرمه چشم من از خاک دری می‌باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل عشاق گرفتی به سر زلف سپار</p></div>
<div class="m2"><p>تا به هم بر نرود ملک سری می‌باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کبوتر چه فرستم که برد نامه شوق</p></div>
<div class="m2"><p>که مرا سوی تو بال و پری می‌باید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه متاعی‌ست سخن‌های دلاویز کمال</p></div>
<div class="m2"><p>لایق گوش تو به زین گهری می‌باید</p></div></div>