---
title: >-
    شمارهٔ ۶۵۶
---
# شمارهٔ ۶۵۶

<div class="b" id="bn1"><div class="m1"><p>بار بنشست به مجلس بنشانید چراغ</p></div>
<div class="m2"><p>روی او نور تجلیست مخوانید چراغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابیست زه طالع شده همسایه ما</p></div>
<div class="m2"><p>نه شب است این که از همسایه ستانید چراغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه را روشنی آن چشم و چراغ است امشب</p></div>
<div class="m2"><p>بگذارید همه شمع و بمائید چراغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم دارید و به به نسبت آن روی کنید</p></div>
<div class="m2"><p>نیست تاریک ز پروانه بدانید چراغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر ندیدید که چون دور شود سایه ز نوره</p></div>
<div class="m2"><p>یک شب از پیش رخ او گذرانید چراغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر نتابیده به حسن رخ او ای مه و مهر</p></div>
<div class="m2"><p>گرچه هریک برخ از نور فشانید چراغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا سحر امشب دیگر من و آن زلف کمال</p></div>
<div class="m2"><p>شب دراز است عجب گر برسانید چراغ</p></div></div>