---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>شب سوی ما هوس آمدن است آن مه را</p></div>
<div class="m2"><p>دیدهها پاک بروبید به مژگان ره را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو بر گوشه نشینان گذری چشم و مژه</p></div>
<div class="m2"><p>آب و جاروب زده صومعه و خانقه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بچه منصوبه ندانیم بریمت به وثاق</p></div>
<div class="m2"><p>تو شهی می توان برد به بازی شه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان ما بیش مسوزان چو بر آوردی خط</p></div>
<div class="m2"><p>دود برخاست منه بر سر آتش که را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی صلای سحری مرغ سحر بیدار است</p></div>
<div class="m2"><p>حاجت بانگ زدن نیست دل آگه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوید از صحبت ما زاهد پر حیله گریز</p></div>
<div class="m2"><p>طاقت پنجه شیران نبود روبه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آن زلف که بادش شب ما کرد دراز</p></div>
<div class="m2"><p>عاشقان دوست ندارند شب کونته را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گشت رنگین ز سخن دفتر اشعار کمال</p></div>
<div class="m2"><p>گر به سرخی بنویسید و ایضا له را</p></div></div>