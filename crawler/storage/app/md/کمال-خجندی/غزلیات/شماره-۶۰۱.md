---
title: >-
    شمارهٔ ۶۰۱
---
# شمارهٔ ۶۰۱

<div class="b" id="bn1"><div class="m1"><p>رفت عمر و نشد آن شوخ به ما بار هنوز</p></div>
<div class="m2"><p>می کند جور به باران وفادار هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طرفه کاری که رسید از غم او کار به جان</p></div>
<div class="m2"><p>نکند زاری ما در دل او کار هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دی در اثنای سخن گفت فلانی سگ ماست</p></div>
<div class="m2"><p>هست در گوش من آن لذت گفتار هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من از آن چشم خوش آن روز شدم توبه شکن</p></div>
<div class="m2"><p>که نبود از می و از میکده آثار هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون توان داشت ازو چشم عنایت چون هست</p></div>
<div class="m2"><p>چشم مرد افکن او بر سر آزار هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهدان از پی آن عاشق رفتار خودند</p></div>
<div class="m2"><p>که نیند آگه از آن قامت و رفتار هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه دل در مرض عشق تو از خویش برفت</p></div>
<div class="m2"><p>آرزوی تو نرفت از دل بیمار هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس از بند غمی یافت نجات و کمال</p></div>
<div class="m2"><p>همچنان هست به قید تو گرفتار هنوز</p></div></div>