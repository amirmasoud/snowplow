---
title: >-
    شمارهٔ ۶۴۸
---
# شمارهٔ ۶۴۸

<div class="b" id="bn1"><div class="m1"><p>نیستم دسترسی آنکه ببوسم پایش</p></div>
<div class="m2"><p>در برفت وز سر من نرود سودایش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق ار سر دل خویش نیارد به زبان</p></div>
<div class="m2"><p>خود گواهی دهد از حال درون سیمایش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال بیداری شمع از دل رنجور بپرس</p></div>
<div class="m2"><p>که چه آمد به سر از دیده شب پیمایش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد چنان گرم به رخسار خود آن شمع چگل</p></div>
<div class="m2"><p>که به پروانه دلان نیست دگر بروایش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جای آنست که چون سایه روڈ سرو ز جای</p></div>
<div class="m2"><p>گر بگویند به بستان صفت بالایش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طوطی از گفته مانند مکرر چند</p></div>
<div class="m2"><p>تا براندیم حدیث از لب شکر خایش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر قدم رنجه کن آن سرو به سروقت کمال</p></div>
<div class="m2"><p>که سری دارد و خواهد که نهد بر پایش</p></div></div>