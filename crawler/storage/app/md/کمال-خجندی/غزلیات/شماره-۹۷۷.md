---
title: >-
    شمارهٔ ۹۷۷
---
# شمارهٔ ۹۷۷

<div class="b" id="bn1"><div class="m1"><p>این چه نبهاست وین چه شیرینی</p></div>
<div class="m2"><p>وآن چه گفتار و آن شکر چینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت جان در آب عارض بین</p></div>
<div class="m2"><p>با چنان رخ رواست خود بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرمنت پیش خویش بنشانم</p></div>
<div class="m2"><p>تو نه آن آتشی که بنشینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوز جانم که کشته آنم</p></div>
<div class="m2"><p>ریز خونم که تشنه اینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهدا مستم از لبش منو تو</p></div>
<div class="m2"><p>بیخبر از شراب رنگینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در نگیرد به هیچ نر آتش</p></div>
<div class="m2"><p>دامن از آه ما چه در چینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو فتادی به زلف یار کمال</p></div>
<div class="m2"><p>بینی افتادگی و مسکینی</p></div></div>