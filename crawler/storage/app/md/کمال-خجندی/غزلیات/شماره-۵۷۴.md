---
title: >-
    شمارهٔ ۵۷۴
---
# شمارهٔ ۵۷۴

<div class="b" id="bn1"><div class="m1"><p>چراغ عمر ندارد فروغ بی رخ پار</p></div>
<div class="m2"><p>کراس زهره که بیند طلوع انور یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیث عشق مگو جز به رند باده پرست</p></div>
<div class="m2"><p>که اهل زهد ندانند سر این اسرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از علم زهد گذر دست از آن بیر و بشوی</p></div>
<div class="m2"><p>که سر حجاب را تست مخامه خود دستار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو گر ز مستی و هستی ز حمله بیخبری</p></div>
<div class="m2"><p>بنوش جام می عشق تا شوی هشیار هزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بروی دوست نه تنها من و تو حیرانیم</p></div>
<div class="m2"><p>بلیل مستند اندر این گلزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز کوی عشق مرو سوی کعبه اسلام اضام</p></div>
<div class="m2"><p>که هیچ کار نیاید ز صورت دیوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر کمال بسوزد ز عشق نقصان نیست</p></div>
<div class="m2"><p>به سوختن نرود زر سرخ را مقدار</p></div></div>