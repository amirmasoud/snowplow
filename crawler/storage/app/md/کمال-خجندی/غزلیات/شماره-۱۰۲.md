---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>بر لب لعل خط سبز ترا پیروزی است</p></div>
<div class="m2"><p>بر زنخدان چو به خال را بهروزی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرد روشن همه آفاق تجلی رخت</p></div>
<div class="m2"><p>عادت طلعت خورشید جهان افروزی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه عالم به تماشای تو شادند آری</p></div>
<div class="m2"><p>نومه عیدی و روی تو گل نوروزی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل بیچاره همیشه ز تو صد پاره چراست</p></div>
<div class="m2"><p>تیر مژگان نرا قاعده چون دلدوزی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی دل ز ازل زلف دوتای تو فتاد</p></div>
<div class="m2"><p>دل بیچاره نظر کن چه پریشان روزی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر تربتم آنی و نیفشانی اشک</p></div>
<div class="m2"><p>شمع را بر من خاکی به ازین دلسوزی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر ز قیدت نکشد با تو چو آموخت کمال</p></div>
<div class="m2"><p>مرغ مألوف گرفتار ز دست آموزی است</p></div></div>