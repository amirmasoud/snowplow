---
title: >-
    شمارهٔ ۳۴۱
---
# شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>بر دل از غمزه خدنگی زدی آن هم گذرد</p></div>
<div class="m2"><p>چون گذشت از سپر سینه ز جان هم گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من اگر سینه ز پولاد بسازم چو دلت</p></div>
<div class="m2"><p>گر خدنگ نظر این است از آن هم گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو اگر بگذری از سرو بخوش رفتاری</p></div>
<div class="m2"><p>اشک گلگون من از آب روان هم گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دهنده اهل نظر پیش تو دشنام رقیب</p></div>
<div class="m2"><p>ما نخواهیم که نامش به زبان هم گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگذرد گریهام از ابر بهاران تنها</p></div>
<div class="m2"><p>کز فلک بینو مرا آه و فغان هم گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر عاشق اگر سیل بلا آبد باز</p></div>
<div class="m2"><p>از دل و دیده خونابه چکان هم گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی از سر گذرد در طلب دوست کمال</p></div>
<div class="m2"><p>سر چه باشد ز سر و جان و جهان هم گذرد</p></div></div>