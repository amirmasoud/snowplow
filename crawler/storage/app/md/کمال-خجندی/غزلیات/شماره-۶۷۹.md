---
title: >-
    شمارهٔ ۶۷۹
---
# شمارهٔ ۶۷۹

<div class="b" id="bn1"><div class="m1"><p>مرا گویند عاشق گرد و بیدل</p></div>
<div class="m2"><p>چه کار آید مرا تحصیل حاصل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیث آب چشم خویش با دوست</p></div>
<div class="m2"><p>نگفتم کأن حدیثی بود نازل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا چون دید گریان گفت رفتم</p></div>
<div class="m2"><p>که باران است و خواهد راه شد گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه اختر بود کامشب بر سرم تاخت</p></div>
<div class="m2"><p>که به در خانه من ساخت منزل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به أو خستگان دارد بی میل</p></div>
<div class="m2"><p>بود سرو سهی با باد مایل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دل گفتم که هیچ آن زلف دلبند</p></div>
<div class="m2"><p>گشاید مشکل ما گفت مشکل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکو خواندنه ماه آسمانت</p></div>
<div class="m2"><p>بقین بودست الألقابه تنزل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درو بامت پر از دلها عجب نیست</p></div>
<div class="m2"><p>تو عیاری بود عیار پر دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کمال آن دم که روز رحلت اوست</p></div>
<div class="m2"><p>نخواهد بست جز مهرت به محمل</p></div></div>