---
title: >-
    شمارهٔ ۳۹۴
---
# شمارهٔ ۳۹۴

<div class="b" id="bn1"><div class="m1"><p>در عشق تو ترک سر چه باشد</p></div>
<div class="m2"><p>از دوست عزیزتر چه باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان نیز اگر فرستم آنجا</p></div>
<div class="m2"><p>این تحفه مختصر چه باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای مردم چشم روشن من</p></div>
<div class="m2"><p>بر من فکنی نظر چه باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی چه کنی اگر کشم تیغ</p></div>
<div class="m2"><p>بسم الله گره دگر چه باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کشتن بنده بر نو سهل است</p></div>
<div class="m2"><p>لطفی کنی این قدر چه باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچند کم است فرصت وصل</p></div>
<div class="m2"><p>خوش زندگی نیست هرچه باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند کمال در دلت چیست</p></div>
<div class="m2"><p>اندیشه او دگر چه باشد</p></div></div>