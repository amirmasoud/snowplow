---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>غارت چشم تو ما را مفلس و بیچاره ساخت</p></div>
<div class="m2"><p>مؤمنانرا کافری از خان و مان آواره ساخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لب شیرین تراش بوس کردی کوه کن</p></div>
<div class="m2"><p>گر توانستی دل بی رحم او را چاره ساخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه خورد آن نوش لب خون دل فرهاد بود</p></div>
<div class="m2"><p>حوض شیرش چون زچشم خون فشان فراره ساخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واعظ گریان چه می سازند مردم منبرت</p></div>
<div class="m2"><p>طفلی و در گریه می باید ترا گهواره ساخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صوفیان را زد به محراب آتش و پشمینه سوخت</p></div>
<div class="m2"><p>آنکه آن طاق دو ابروبست و آن رخساره ساخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تماشای تو بی معنی است منع عاشقان</p></div>
<div class="m2"><p>چون مصور صورت خوب از پی نظاره ساخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد حمایل بکشی در گردنش دست کمال</p></div>
<div class="m2"><p>آن حمایل را ز غیرت خواستم سی پاره ساخت</p></div></div>