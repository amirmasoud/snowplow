---
title: >-
    شمارهٔ ۴۱۶
---
# شمارهٔ ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>دوشم خیال روی تو در سر فتاده بود</p></div>
<div class="m2"><p>گوشی در بهشت برویم گشاده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو ز در درآنی و مجلس دهی فروغ</p></div>
<div class="m2"><p>شب نا بروز شمع به پا ایستاده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی به یاد روی توام هر قدح که داد</p></div>
<div class="m2"><p>آب حیات بود که خوردم نه باده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام از لب تو خواست گذشتن به ناز کی</p></div>
<div class="m2"><p>آن صاف دل بین که چه مقدار ساده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خواب دیدمت که بمن دست می دهی</p></div>
<div class="m2"><p>دولت نگر که دوش مرا دست داده بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرگشته که بود روان پیش تو چو شمع</p></div>
<div class="m2"><p>جانی بدست کرده و بر کف نهاده بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد ارچه کم نبود ز هر سو کمال را</p></div>
<div class="m2"><p>دوش از فراق روی تو چیزی زیاده بود</p></div></div>