---
title: >-
    شمارهٔ ۹۲۲
---
# شمارهٔ ۹۲۲

<div class="b" id="bn1"><div class="m1"><p>ای رخ و زلف سیاه تو شب تیره و ماه</p></div>
<div class="m2"><p>مردم دیده که باشد؟ که کند در تو نگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم چشم تو ماتم زده عشاقند</p></div>
<div class="m2"><p>ورنه رنگ «مژه ها» بهر چه گردید سیاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق روی ترا برگ گل بستان نی</p></div>
<div class="m2"><p>بلبل باغ ارم میل ندارد به گیاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نشان قدمت بر سرراهی یابم</p></div>
<div class="m2"><p>برنگیرم رخ اگر خاک شوم برسرراه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستم از چشم تو در عین پریشانکاریست</p></div>
<div class="m2"><p>خود سر زلف تو دال است چه حاجت به گواه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش رخ بهم بر رخ زیبای تو گفت</p></div>
<div class="m2"><p>خرمن گل نتواند که کشد زحمت گاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قصة زلف تو گفتیم به پایان نرسید</p></div>
<div class="m2"><p>کاین حکایت نه حدیثی ست که گردد کوتاه</p></div></div>