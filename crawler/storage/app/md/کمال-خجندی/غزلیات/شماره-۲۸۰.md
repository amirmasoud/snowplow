---
title: >-
    شمارهٔ ۲۸۰
---
# شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>هزار شکر که آن چشم پر خمارم کشت</p></div>
<div class="m2"><p>وگرنه حسرت آن خواست زار زارم کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر واجب است به هر گشتن توأم شکری</p></div>
<div class="m2"><p>هزار شکر که چشمت هزار بارم کشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دعای زندگیم گو مکن کس از یاران</p></div>
<div class="m2"><p>بس است زندگی من همین که بارم کشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب فراق بشارت بکشتنم دادی</p></div>
<div class="m2"><p>چه منت است ز نو کآن شب انتظار کشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرم تو دل ندهی چون رهم ز دست رقیب</p></div>
<div class="m2"><p>که جز به سنگ من آن مار را ندارم کشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پیچ و تاب چو دامی که صید را بکشد</p></div>
<div class="m2"><p>درون هر گره آن زلف ثا بدارم کشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نرفت آب خوشی بی لبش به حلق کمال</p></div>
<div class="m2"><p>مگر دمی که به شمشیر آبدارم کشت</p></div></div>