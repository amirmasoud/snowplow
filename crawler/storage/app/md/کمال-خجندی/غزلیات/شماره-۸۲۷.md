---
title: >-
    شمارهٔ ۸۲۷
---
# شمارهٔ ۸۲۷

<div class="b" id="bn1"><div class="m1"><p>بحر عشقت بحر بی پایاب گفتن میتوان</p></div>
<div class="m2"><p>در وصلت گوهر نایاب گفتن میتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق گریان که گوید با تو دستی ده بما</p></div>
<div class="m2"><p>گر چه گستاخیست در غرقاب گفتن میتوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کنم با چشم و دل گه گه ز بخت خود حدیث</p></div>
<div class="m2"><p>پیش بیداران حدیث خواب گفتن میتوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابرویت از گوشه گیران دل بناحق میبرد</p></div>
<div class="m2"><p>قول حق در گوشه محراب گفتن میتوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنشد گفتن به آهو وصف چشمت چون گریخت</p></div>
<div class="m2"><p>از خطت حرفی به مشک ناب گفتن میتوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبلان شب تا سحر از گل حکایت می کنند</p></div>
<div class="m2"><p>این حکایت با گل سیراب گفتن میتوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم مخور چون زاهدان خشک از پیری کمال</p></div>
<div class="m2"><p>تا غزلهای تر چون آب گفتن میتوان</p></div></div>