---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>ما بی به روی تو آهم ز ثریا بگذشت</p></div>
<div class="m2"><p>بسیاری دیده دریا شد و هر قطره ز دریا بگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه در مجمع دل درد بود صدرنشین</p></div>
<div class="m2"><p>ناله چون برتر ازو بود به بالا بگذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر صبا آمد و بوی تو ز ما داشت دریغ</p></div>
<div class="m2"><p>شاکریم از تو به هر حال که بر ما بگذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چمن جان مرا غنچه شادی بشکفت</p></div>
<div class="m2"><p>تا خیال دهنت در دل شیدا بگذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو می خواست به پابوس تو آید چون آب</p></div>
<div class="m2"><p>لیکن از جو نتوانست به یک پا بگذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی که فرمودم از آن لب دل خود را پرهیز</p></div>
<div class="m2"><p>صوفی ما نتوانست ز حلوا بگذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که گفتی ببرم نه تو پیش طبیب</p></div>
<div class="m2"><p>در این رنج که کارم ز مداوا بگذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دی بر آن خاک در از جان رمقی داشت کمال</p></div>
<div class="m2"><p>جعل الجنة مثواه همانجا بگذشت</p></div></div>