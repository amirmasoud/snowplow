---
title: >-
    شمارهٔ ۴۸۳
---
# شمارهٔ ۴۸۳

<div class="b" id="bn1"><div class="m1"><p>گر بگذری سوی چمن سرو سهی از جا رود</p></div>
<div class="m2"><p>ور زآنکه برقع افکنی صبر از دل شیدا رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هست بر لوح بقا از جان نشان باور مکن</p></div>
<div class="m2"><p>کر دیده صاحبدلان نفش رخ زیبا رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرشد سرم در کار آن زلف عبیر افشان چه شد</p></div>
<div class="m2"><p>شوریدگانرا دائما سر در سر سودا رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم رسان ای دل برو از آب چشم من خبر</p></div>
<div class="m2"><p>دل گفت ما راکی رسد کآنجا حدیث ما رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوی وفا داری رود تا روز حشر از آب و گل</p></div>
<div class="m2"><p>در هر زمینی در من و عشقش حکایتها رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هان ای رقیب از دامنش دست تصرف بگسلان</p></div>
<div class="m2"><p>بگذار کامشب همچو مه هر جا رود تنها رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با بیخبر کم کن کمال از خاک پای او سخن</p></div>
<div class="m2"><p>چه سود اگر کحل بصر در چشم نابینا رود</p></div></div>