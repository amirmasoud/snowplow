---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>خضرجان آب حیات از لب دلجوی نو یافت</p></div>
<div class="m2"><p>موسی انوار تجلی همه از روی تو یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرده را زنده از آن کرد مسیحا به دمی</p></div>
<div class="m2"><p>کردم باد سحر گاه ازل بوی تو یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواجه هر دو سرا احمد محمود خصال</p></div>
<div class="m2"><p>حسن اخلاق و مکارم همه از خوی تو یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که را بود سری در سر سودای تو شد</p></div>
<div class="m2"><p>وانکه را بود دلی گمشده در کوی نو یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در میان ظلمات سر زلف تو کمال</p></div>
<div class="m2"><p>همچو خضر آب حیات از لب دلجوی تو یافت</p></div></div>