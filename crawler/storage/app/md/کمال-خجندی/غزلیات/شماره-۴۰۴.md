---
title: >-
    شمارهٔ ۴۰۴
---
# شمارهٔ ۴۰۴

<div class="b" id="bn1"><div class="m1"><p>دل غمدیده شکایت ز غم او نکند</p></div>
<div class="m2"><p>طالب درد فغان از الم او نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیست در خور که رسد دوست بفریاد دلش</p></div>
<div class="m2"><p>آنکه فریاد ز جور و ستم او نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که خورسند نباشد به جفاهای حبیب</p></div>
<div class="m2"><p>ناسپاسیست که شکر نعم او نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم زاهد نشود پاک ز خود بینی خویش</p></div>
<div class="m2"><p>تا چو با سرمه ز خاک قدم او نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پارسا پشت فراغت چه نه بر محراب</p></div>
<div class="m2"><p>گر کند تکیه چرا بر کرم او نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شربت درد تو هر تشنه که نوشید دمی</p></div>
<div class="m2"><p>التفاتی بمسیحا و دم او نکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بگرد در تو طوف کنان است کمال</p></div>
<div class="m2"><p>هوس کعبه و یاد حرم او نکند</p></div></div>