---
title: >-
    شمارهٔ ۲۹۹
---
# شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>آن سرو ناز رفت بگلشن نظر کنید</p></div>
<div class="m2"><p>در باغ گل برآمد و سوسن نظر کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل را ز شوق نکهت آن پیرهن چو من</p></div>
<div class="m2"><p>صد داغ خون به گوشه دامن نظر کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتشکده است جان من از سوز سینه به آه</p></div>
<div class="m2"><p>دودی که بر گذشت ز روزن نظر کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با چشم نیز بین نظری بر دهان او</p></div>
<div class="m2"><p>گر ممکن است یکسر سوزن نظر کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او دیده ایست روشن اگر برقع افکند</p></div>
<div class="m2"><p>ای عاشقان به دید، روشن نظر کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بر شما حقیقت جانست ملتمس</p></div>
<div class="m2"><p>از پیرهن لطاقت آن تن نظر کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنها که می کنند لبش آرزو کمال</p></div>
<div class="m2"><p>گر در حلاوت سخن من نظر کنید</p></div></div>