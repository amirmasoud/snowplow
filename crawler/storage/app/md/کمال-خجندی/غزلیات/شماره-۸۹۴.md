---
title: >-
    شمارهٔ ۸۹۴
---
# شمارهٔ ۸۹۴

<div class="b" id="bn1"><div class="m1"><p>ای دل غلام أو شدی ای من غلام تو</p></div>
<div class="m2"><p>بادت مبارک اینکه جهان شد به کام تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از من به رسم بنده نوازی به او بگو</p></div>
<div class="m2"><p>مشتاق خدمت است غلام غلام تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر نه از توأم همه وقت آمدی پیام</p></div>
<div class="m2"><p>آخر کجا شد آن کرم مستدام تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش از سلام پیش روم قاعد ترا</p></div>
<div class="m2"><p>گر در نماز باشم و آرد سلام تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نام کتار و بوس چو بردن نمی توان</p></div>
<div class="m2"><p>هم بر کنار نامه ببوسیمه نام تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد گوش دیگرم ز خدا باشد آرزو</p></div>
<div class="m2"><p>روزی که بشنوم ز رسولی پیام تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای کاش نامه روی به پیچیدی از کمال</p></div>
<div class="m2"><p>تا او بگوش خویش شنیدی کلام تو</p></div></div>