---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>بی تو از دردم آرمیدن نیست</p></div>
<div class="m2"><p>وز توأم طاقت بریدن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو شمشیر میکشی ما را</p></div>
<div class="m2"><p>زهرة آه بر کشیدن نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه ما با نو کی رسد کانجا</p></div>
<div class="m2"><p>باد را ممکن رسیدن نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار در پیش چشم تست ای اشک</p></div>
<div class="m2"><p>حاجت هر طرف دویدن نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواستم بوسه از آن دهان نشنید</p></div>
<div class="m2"><p>رسم خردان سخن شنیدن نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش از دهانت ای بت چین</p></div>
<div class="m2"><p>چین در ابرو فکند و گفت کمال</p></div></div>