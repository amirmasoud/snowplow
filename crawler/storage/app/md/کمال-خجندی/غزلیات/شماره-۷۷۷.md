---
title: >-
    شمارهٔ ۷۷۷
---
# شمارهٔ ۷۷۷

<div class="b" id="bn1"><div class="m1"><p>ما با غم تو خرم و آسوده‌خاطریم</p></div>
<div class="m2"><p>زآن لب به کام ما شکری نی و شاکریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غایب نه ز چشم جهان بین ما چو نور</p></div>
<div class="m2"><p>ا نو حاضری همیشه و ما با تو ناظریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظارگی به حیرته از آن صورتست و ما</p></div>
<div class="m2"><p>حیران جان نگاری کلک مصوریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زآن دم که نام جام بر آن لب نهاده اند</p></div>
<div class="m2"><p>آن را که نیست معتقد باده منکریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم به دیر با تو رسم یا به کعبه گفت</p></div>
<div class="m2"><p>ما را به هر مقام که جوینده حاضریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دید کز تطاول آن زلف بیقرار</p></div>
<div class="m2"><p>شوریده روزگار و پراکنده خاطریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببرید زلف و گفت به افسوس با کمال</p></div>
<div class="m2"><p>گر دیر میرسیم به خدمت مقصریم</p></div></div>