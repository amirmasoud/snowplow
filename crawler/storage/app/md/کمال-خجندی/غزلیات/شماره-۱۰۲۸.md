---
title: >-
    شمارهٔ ۱۰۲۸
---
# شمارهٔ ۱۰۲۸

<div class="b" id="bn1"><div class="m1"><p>دل می کنی جراحت و مرهم نمیدهی</p></div>
<div class="m2"><p>عیسی دمی و آب به آدم نمیدهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داروی جان ز حقه لبهات میدهد</p></div>
<div class="m2"><p>با جان خسته چاشنی هم نمیدهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوی تو کعبه و لب لعل تو زمزم است</p></div>
<div class="m2"><p>آبی چرا به تشنه زمزم نمیدهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست رقیب نیز به آن لب نمی رسد</p></div>
<div class="m2"><p>باری بدیو شکر که خانم نمی دهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وردم دعای نسبت به محراب ابروان</p></div>
<div class="m2"><p>کز درد و غم وظیفه من کم نمیدهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نامحرمان کجا بحریم نور را برند</p></div>
<div class="m2"><p>چون را در آن مقام به محرم نمیدهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیبد گدانی در دلبر ترا کمال</p></div>
<div class="m2"><p>کان سلطنت به ملک دو عالم نمیدهی</p></div></div>