---
title: >-
    شمارهٔ ۷۷۶
---
# شمارهٔ ۷۷۶

<div class="b" id="bn1"><div class="m1"><p>ما از لب تو کام ندیدیم و گذشتیم</p></div>
<div class="m2"><p>تشنه به لب چشمه رسیدیم و گذشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتیم دعای تو و از بخت مخالف</p></div>
<div class="m2"><p>از لفظ تو دشنام شنیدیم و گذشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با داغ فراق تو که جانسوز عذابست</p></div>
<div class="m2"><p>از زندگی امید بریدیم و گذشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک شب نکشیدیم ترا دربر و هر روز</p></div>
<div class="m2"><p>صد جور و جفا از تو کشیدیم و گذشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بیشه دنیا که چراگاه دل ماست</p></div>
<div class="m2"><p>روزی دو چریدیم و چمیدیم و گذشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهد لب تو شربت وصل دگران بود</p></div>
<div class="m2"><p>ما زهر فراق تو چشیدیم و گذشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مانند کمال از هوس آن گل رخسار</p></div>
<div class="m2"><p>صد جامه به باد نو دریدیم و گذشتیم</p></div></div>