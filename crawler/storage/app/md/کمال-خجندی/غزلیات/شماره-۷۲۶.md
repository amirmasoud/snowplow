---
title: >-
    شمارهٔ ۷۲۶
---
# شمارهٔ ۷۲۶

<div class="b" id="bn1"><div class="m1"><p>دل گرفت از بتان به رویم</p></div>
<div class="m2"><p>راست گویم دروغ میگویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستم از بوی عنبرین مویان</p></div>
<div class="m2"><p>نیست هشیار بک سر مویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میکنم زآن لب و دهان پرسش</p></div>
<div class="m2"><p>عاشقم نقل و باده میجویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نام آن لب چو میبرم به زبان</p></div>
<div class="m2"><p>لب به آب حیات میشویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شادی وقت من که در همه عمر</p></div>
<div class="m2"><p>باغم روی و محنت اویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد اگر خاک من برد هرسو</p></div>
<div class="m2"><p>نکشد مهر دل جز آن سویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آبرو بایدت بگوی کمال</p></div>
<div class="m2"><p>بتفاخر که خاک آن کویم</p></div></div>