---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>روی تو قبله مناجات است</p></div>
<div class="m2"><p>دیدنت احسن العبادات است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آگه از راز آن دهان و میان</p></div>
<div class="m2"><p>عالم السر والخفیات است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخلصان را وصال تست خیال</p></div>
<div class="m2"><p>مخلصی باعث خیالات است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر بساط چمن به صد رخ گل</p></div>
<div class="m2"><p>پیش نقش رخ تو رخ مات است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو روانی به قد به لب جانی</p></div>
<div class="m2"><p>زندگی بی تو از محالات است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بنازم کشی مکن تأخیر</p></div>
<div class="m2"><p>که ز تأخیر بیم آفات است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنده تر شد ز کشتن تو کمال</p></div>
<div class="m2"><p>عاشقان را بی کرامات است</p></div></div>