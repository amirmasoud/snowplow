---
title: >-
    شمارهٔ ۸۹۵
---
# شمارهٔ ۸۹۵

<div class="b" id="bn1"><div class="m1"><p>ای کاش رفتمی چو صبا در حریم تو</p></div>
<div class="m2"><p>تا زنده گشتمی نفسی از نسیم تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو امید قطع کنم این روا بود</p></div>
<div class="m2"><p>ما را امیدهاست به لطف عمیم تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بگذری نو از سر عهد قدیم ما</p></div>
<div class="m2"><p>ز ما نگذریم از سر عهد قدیم تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آنکه منع می کنی از عاشقی مرا</p></div>
<div class="m2"><p>فریاد از این طبیعت نا مستقیم تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را به صحبت خود اگر راه نمی دهی</p></div>
<div class="m2"><p>باری رقیب کیست که باشد ندیم تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیا چگونه بر کند در غم فراق</p></div>
<div class="m2"><p>پرورده در وصال به ناز و نعیم تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مغرور عشوه شده باز ای کمال</p></div>
<div class="m2"><p>او از سلامت نر و طبع سلیم تو</p></div></div>