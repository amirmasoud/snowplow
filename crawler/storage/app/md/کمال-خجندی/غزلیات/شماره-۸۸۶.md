---
title: >-
    شمارهٔ ۸۸۶
---
# شمارهٔ ۸۸۶

<div class="b" id="bn1"><div class="m1"><p>نیست بازی با رخ او عشق پنهان باختن</p></div>
<div class="m2"><p>با چنان رخ غایبانه نیست آسان باختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان بسی در باخت عاشق تا به آن رخ عشق باخت</p></div>
<div class="m2"><p>پاکباز آمد مقامر از فراوان باختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بری از من به بازی جان و سر وآنگه روان</p></div>
<div class="m2"><p>خواهم این شطرنج با تو تا به پایان باختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون به لب بازی کتی در عشوه جان بازم منت</p></div>
<div class="m2"><p>هرچه خواهد باخت باید با حریفان باختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در میان گریه با زلف تو چون بازم نظر</p></div>
<div class="m2"><p>روز باران نیست گوئی وقت چوگان باختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست بازی خوش بود که با تو گه با زلف تو</p></div>
<div class="m2"><p>این میسر نیست الأ بر سر و جان باختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دهانش پیش آن عارض نظر بازی کمال</p></div>
<div class="m2"><p>چون نوان کانگشتری در روز نتوان باختن</p></div></div>