---
title: >-
    شمارهٔ ۸۵۴
---
# شمارهٔ ۸۵۴

<div class="b" id="bn1"><div class="m1"><p>راز عشقت ز دل آمد به زبان</p></div>
<div class="m2"><p>مهر در ذره نهفتن نتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی از چشم تو خون می آید</p></div>
<div class="m2"><p>هرچه می آید ازو در گذران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهنت دیدم و گفتم شکر است</p></div>
<div class="m2"><p>گفتمت هرچه خوش آمد به دهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الاف اگر زد به قدت سرو چمن</p></div>
<div class="m2"><p>گویش اینک گز و اینک میدان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسبت روی تو کردیم به ماه</p></div>
<div class="m2"><p>ماه چرخی بزد از شادی آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته خون تو ریزیم و کمال</p></div>
<div class="m2"><p>از انتظارم چه کشی باش بر آن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاکمی خواه بکش خواه ببخش</p></div>
<div class="m2"><p>بندهامه خواه بخوان خواه بران</p></div></div>