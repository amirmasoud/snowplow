---
title: >-
    شمارهٔ ۹۶۵
---
# شمارهٔ ۹۶۵

<div class="b" id="bn1"><div class="m1"><p>اگر ز محنت دنیا خلاص می‌طلبی</p></div>
<div class="m2"><p>بنوش باده گلگون ز شیشه حلبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان به آب عنب تشنه‌ام که صورت آن</p></div>
<div class="m2"><p>برون نمی‌رودم از حدیقة عنبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شراب و شاهد و سیم و زرم طفیل تو باد</p></div>
<div class="m2"><p>فداک اصل مرامی و تها طلبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نه سایهٔ میخانه بر سرت باشد</p></div>
<div class="m2"><p>ز روزگار ببینی هزار بوالعجبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا چو صحبت امن و کفایتی باشد</p></div>
<div class="m2"><p>به عیش کوش و به عشرت دگر چه می‌طلبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شراب نوش به فصل بهار و فارغ باش</p></div>
<div class="m2"><p>لا یلیق زمان الشباب فی کربی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال را چو مداوا به باده فرمایند</p></div>
<div class="m2"><p>رواست گر بخورد می به حکم شرع نبی</p></div></div>