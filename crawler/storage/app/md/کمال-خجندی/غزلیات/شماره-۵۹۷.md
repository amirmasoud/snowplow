---
title: >-
    شمارهٔ ۵۹۷
---
# شمارهٔ ۵۹۷

<div class="b" id="bn1"><div class="m1"><p>با چشم خوش ای شوخ مرا جنگ مینداز</p></div>
<div class="m2"><p>محنت زده را به بلا جنگ مینداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دست صبا سلسله زلف میفکن</p></div>
<div class="m2"><p>شوریده دلان را به صبا جنگ مینداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب بر در تو بوده ام این راز نهان دار</p></div>
<div class="m2"><p>آن حاسة سگ را به گدا جنگ مینداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی بر باران بردمه کشتن یاری</p></div>
<div class="m2"><p>نا آمده در مجلس ما جنگ مینداز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک ناوک دیگر بزن و راست رسانتر</p></div>
<div class="m2"><p>با جان دل مجروع مرا جنگ مینداز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دست کمال آن سر زلف افکن وشو صلح</p></div>
<div class="m2"><p>خود را بمن بیسر و پا جنگ مینداز</p></div></div>