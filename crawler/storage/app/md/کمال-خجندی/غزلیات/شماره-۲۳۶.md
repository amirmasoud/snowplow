---
title: >-
    شمارهٔ ۲۳۶
---
# شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>گو خلق بدانید که دلدار من این است</p></div>
<div class="m2"><p>معشوق ستمکار جفاکاره من این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محبوب من و جان من و همنفس من</p></div>
<div class="m2"><p>خویش من و پیوند من و بار من این است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی سر زلفش به من آرد همه شب باد</p></div>
<div class="m2"><p>از همنفسان یار وفادار من این است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من خاک رهم بلکه بسی کمتر از آن نیز</p></div>
<div class="m2"><p>در حضرت او قیمت و مقدار من این است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنواخت به نیر دگری کشته خود را</p></div>
<div class="m2"><p>از غمزه صید افکنش آزار من این است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با آنکه طبیب است شود شاد به دردم</p></div>
<div class="m2"><p>داند که دوای دل بیما من این است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند کمال از پی او چند گنی جان</p></div>
<div class="m2"><p>تا هست ز جانم رمقی کار من این است</p></div></div>