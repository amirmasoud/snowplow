---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>ما به کفر زلف او داریم ایمانی درست</p></div>
<div class="m2"><p>بابت پیمان شکن عهدی و پیمانی درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه چشم می گویدم جویم دلت لیکن که یافت</p></div>
<div class="m2"><p>قول مستی راست عهد نامسلمانی درست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عهدها بندد که سازم عاقبت دل با تو راست</p></div>
<div class="m2"><p>راست گویم این سخن هم نیست چندانی درست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر زبانها نا گذشت آن لب رقیب جنگجوی</p></div>
<div class="m2"><p>در دهان عاشقان نگذشت دندانی درست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار ما گر آستین افشان در آید در سماع</p></div>
<div class="m2"><p>کس نه بیند خرقه پوشی با گریبانی درست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوی دلها بسکه از هر سو ربودند و شکست</p></div>
<div class="m2"><p>نیست بر دوشبتان از زلف چرگانی درست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاره سازند اهل معنی جامه ها بر تن کمال</p></div>
<div class="m2"><p>گر بخواند هفت بیت تو غزالخوانی درست</p></div></div>