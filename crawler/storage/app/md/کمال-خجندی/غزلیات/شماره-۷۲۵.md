---
title: >-
    شمارهٔ ۷۲۵
---
# شمارهٔ ۷۲۵

<div class="b" id="bn1"><div class="m1"><p>دل ز چشم او به نازی مست شد بی خویش هم</p></div>
<div class="m2"><p>ناز خود گو بیش کن تا میرمش زین بیش هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون به آن قانع نشد کز غمزه دلها ریش ساخت</p></div>
<div class="m2"><p>میفشان گر از لب خندان نمک ریش هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرزنش در عشق او دل را بدان ماند که ریش</p></div>
<div class="m2"><p>پر بود از درد و بر سر میزنندش نیش هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک پای او ندیده گفته بودم نونباست</p></div>
<div class="m2"><p>نیکبودست آن نظر دیدم به چشم خویش هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده ام اندیشه نیکی که دیگر نشنوم</p></div>
<div class="m2"><p>در غم او قول ناصح پند نیک اندیش هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقت قتل ای تیغ اگر بی جرمیم بینی ز شرم</p></div>
<div class="m2"><p>سرخ گردی در دم و سر افکنی در پیش هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته بودی ده پلاست ز اطلس ما به کمال</p></div>
<div class="m2"><p>با همه عالم پلاسی و با من درویش هم</p></div></div>