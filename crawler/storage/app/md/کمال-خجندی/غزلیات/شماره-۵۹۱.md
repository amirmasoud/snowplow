---
title: >-
    شمارهٔ ۵۹۱
---
# شمارهٔ ۵۹۱

<div class="b" id="bn1"><div class="m1"><p>مگر ترک جفا و بکن جفای دگر</p></div>
<div class="m2"><p>که باشد از تو جفای دگر وفای دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلا فرستی و من باز بسته دل به امید</p></div>
<div class="m2"><p>که از تو باز رسد بر سرم بلای دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سری که داشم انداختم به پای تو حیف</p></div>
<div class="m2"><p>که نیستم سر دیگر برای پای دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گدایی از تو همین باشدم که نگذاری</p></div>
<div class="m2"><p>که بر در تو رود غیر من گدای دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دعای مردن من میکنی چه حاجت آن</p></div>
<div class="m2"><p>بقای عمر تو بادا بکنه دعای دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه نسبت رویت به آفتاب کنند</p></div>
<div class="m2"><p>تو جای دیگری و آفتاب جای دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال حسن طبیعت همین بود که مرا</p></div>
<div class="m2"><p>ورای دیدن آن روی نیست رای دگر</p></div></div>