---
title: >-
    شمارهٔ ۵۱۳
---
# شمارهٔ ۵۱۳

<div class="b" id="bn1"><div class="m1"><p>مرا لطف گفتارش از راه برد</p></div>
<div class="m2"><p>لبش هوشم از جان آگاه برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ ماه را ماند آن رخ مگر</p></div>
<div class="m2"><p>بشطرنج خوبی رخ ماه برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمش هر کجا در دلی خانه ساخت</p></div>
<div class="m2"><p>برای گل از روی ما کاه برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان پر ز آوازه عاشقیست</p></div>
<div class="m2"><p>مگر نیت او ناله و آه برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرادم تونی از تو خواهم مراد</p></div>
<div class="m2"><p>که درویش حاجت بر شاه برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآن استان در خجالت سریست</p></div>
<div class="m2"><p>که تصنیع خود گاه و بیگاه برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه گوهر شناس است چشم کمال</p></div>
<div class="m2"><p>که سرمه از آن خاک درگاه برد</p></div></div>