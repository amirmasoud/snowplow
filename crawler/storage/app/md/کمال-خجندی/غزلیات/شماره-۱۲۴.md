---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>حلقه بر در میزند هر دم خیال روی دوست</p></div>
<div class="m2"><p>گوش دار این حلقه را ای دل گرت سودای اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبحگاهی می گرفتم عقد گیسویش به خواب</p></div>
<div class="m2"><p>زان زمان دست خیالم تا به اکنون مشگ بوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل که چون گویست در میدان عشق آشفته حال</p></div>
<div class="m2"><p>گر به چوگان نسبت زلفش کند بیهوده گوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر بلندی بین که باز از دولت رندی مرا</p></div>
<div class="m2"><p>بر سر دوشی که دی سجاده بود امشب سبوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاف یکرنگی مزن با دوست هر ساعت کمال</p></div>
<div class="m2"><p>تا چو گل بیرون نیائی خرم و خندان ز پوست</p></div></div>