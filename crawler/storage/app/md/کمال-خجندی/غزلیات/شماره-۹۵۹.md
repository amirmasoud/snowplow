---
title: >-
    شمارهٔ ۹۵۹
---
# شمارهٔ ۹۵۹

<div class="b" id="bn1"><div class="m1"><p>گر همه وقتی همه دلخون نه ای</p></div>
<div class="m2"><p>لیلی وقتی تو و مجنون نه ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست چو ما قابل خون خوردنت</p></div>
<div class="m2"><p>در خور این باده گلگون نه ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در طلبت زر چه کئی گنج عشق</p></div>
<div class="m2"><p>ر خواجه گدائی تو، فریدون نه ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او پیش دهان و لبش ای قند مصر</p></div>
<div class="m2"><p>قد چه خوانیم ترا چون نه ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جای تو با دیدن ما با دل است</p></div>
<div class="m2"><p>زین در بقین است که بیرون نه ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صفت جستن دوری ز مهر</p></div>
<div class="m2"><p>کم نه ای از ماه گر افزون نه ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای به در خانه تو أو کمال</p></div>
<div class="m2"><p>چون شنوی زانکه به گردون نه ای</p></div></div>