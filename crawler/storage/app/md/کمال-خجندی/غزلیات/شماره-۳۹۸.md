---
title: >-
    شمارهٔ ۳۹۸
---
# شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>دلبرا چشم خوشت آفت مستان آمد</p></div>
<div class="m2"><p>نشنة لعل تو سر چشمه حیوان آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرتوی ز آینه روی جهان آرایت</p></div>
<div class="m2"><p>مطلع حسن و لطافت مه تابان آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمه ای از سر گیسوی عبیر افشانت</p></div>
<div class="m2"><p>نافه آهوی چین طرف ریحان آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا رسید از سر کوی تو نسیمی به بهشت</p></div>
<div class="m2"><p>میر بنده خاک درت روضه رضوان آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سالها پیش وصال تو بنتوان گفتن</p></div>
<div class="m2"><p>کآنچه بر جان من از محنت هجران آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل به امید سرا پرده وصلت هیهات</p></div>
<div class="m2"><p>رفت چندانکه ره عمر به پایان آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که را در دو جهان آرزوی روی تو نیست</p></div>
<div class="m2"><p>حیوانیست که در صورت انسان آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که دل می طلبی در شکن زلفش جوی</p></div>
<div class="m2"><p>زآنکه او مجمع دلهای پریشان آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که رساند به کمال از سر کوی تو نشان</p></div>
<div class="m2"><p>پای امید چو اندر ره نقصان آمد</p></div></div>