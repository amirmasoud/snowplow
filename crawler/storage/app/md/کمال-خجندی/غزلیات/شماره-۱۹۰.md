---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>عارف پنهان ز پیدا خوشتر است</p></div>
<div class="m2"><p>گنج را گنجینه مأوا خوشتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم آزادگی خوش عالمی است</p></div>
<div class="m2"><p>ای دل آنجا رو که آنجا خوشتر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندرین پستی دلت نگرفت هیچ</p></div>
<div class="m2"><p>عزم بالا کن که بالا خوشتر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقان را دل به وحدت می کشد</p></div>
<div class="m2"><p>مرغ آبی را به دریا خوشتر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواجه انکار قیامت سرو می کند</p></div>
<div class="m2"><p>زانکه امروزش ز فردا خوشتر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک نظر قانع شو از عالم کمال</p></div>
<div class="m2"><p>نخل مومین را تماشا خوشتر است</p></div></div>