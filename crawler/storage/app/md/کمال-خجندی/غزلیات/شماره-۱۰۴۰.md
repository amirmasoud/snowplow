---
title: >-
    شمارهٔ ۱۰۴۰
---
# شمارهٔ ۱۰۴۰

<div class="b" id="bn1"><div class="m1"><p>کاش که سرو ناز ما از در ما در آمدی</p></div>
<div class="m2"><p>تا شب هجر کم شدی روز جفا سر آمدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش بود ار سحر گهی نزد ستم رسیدگان</p></div>
<div class="m2"><p>از دم فاصنة صبا مزده دلبر آمدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دم آخر ار بدی بر من خسته اش گذر</p></div>
<div class="m2"><p>جان به لب رسیده ام خرم و خوش بر آمدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگه به چمن در آمدی شاهد سرو قد ما</p></div>
<div class="m2"><p>گل ز حیای روی او سرخ به هم بر آمدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنده وقت آن دمم کان بث شوخ عشوه گر</p></div>
<div class="m2"><p>وعده بدادی از رهی وز ره دیگر آمدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زودترش فرو رود پای به گل درین هوس</p></div>
<div class="m2"><p>بر سر کوی زیر کی هر که بود سر آمدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جور و جفای بیحدش از دل ما به در شدی</p></div>
<div class="m2"><p>روزی اگر کمال را مونس و غمخور آمدی</p></div></div>