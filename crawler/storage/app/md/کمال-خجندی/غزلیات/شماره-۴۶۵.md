---
title: >-
    شمارهٔ ۴۶۵
---
# شمارهٔ ۴۶۵

<div class="b" id="bn1"><div class="m1"><p>عاشقان قصه های نو شنوید</p></div>
<div class="m2"><p>که شما نیز عاشقان نوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می رسد از خدا نسیم نوی</p></div>
<div class="m2"><p>خس نهایه از نسیم زنده شوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بالغی نی و دعوی پیران</p></div>
<div class="m2"><p>کشت خود نا رسیده میدروید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما گهر بافتیم چند شما</p></div>
<div class="m2"><p>در ره سنگک و مجوره دوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این گهر با شما بنفروشم</p></div>
<div class="m2"><p>تا به دکان و خانه در گروید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صف صوفیان صاف امید</p></div>
<div class="m2"><p>گر مریدان مصلحت شنوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن افشان ز خان ومان چو کمال</p></div>
<div class="m2"><p>بر در شیخ مصلحت بروید</p></div></div>