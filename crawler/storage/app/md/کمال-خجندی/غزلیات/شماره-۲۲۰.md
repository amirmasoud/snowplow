---
title: >-
    شمارهٔ ۲۲۰
---
# شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>گر قصد خون ماست پس از دل ربودنت</p></div>
<div class="m2"><p>میباید آن رخ از پس برقع نمودنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون مشر ز دیده که با آن جمال و زیب</p></div>
<div class="m2"><p>زیبد درون خانه پس پرده بودنت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند خوبتر شود از بستن انگبین</p></div>
<div class="m2"><p>شیرین ترست از آن به سخن لب گشودنت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دل شب فراق چنین ناله ها کشد</p></div>
<div class="m2"><p>ای دل کسی به خواب نه بینه غنودنت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریاد ما شنو، بنو گونیم نشنوی</p></div>
<div class="m2"><p>فریاد و آه ما ز سخن ناشنودنت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بوی گل ترا به کف پاش نسبت است</p></div>
<div class="m2"><p>مرغ چمن چنین نتواند ستودنت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آزردن از گزاف بود نور دیده را</p></div>
<div class="m2"><p>تا کی کمال دیده بر آن پای سودنت</p></div></div>