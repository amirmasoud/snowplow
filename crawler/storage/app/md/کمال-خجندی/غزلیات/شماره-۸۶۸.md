---
title: >-
    شمارهٔ ۸۶۸
---
# شمارهٔ ۸۶۸

<div class="b" id="bn1"><div class="m1"><p>عاشقی چیست مقیم در جانان بودن</p></div>
<div class="m2"><p>روی بر خاک در دوست به عزت سودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترک جان گفتن و از تیغ نچیدن روی</p></div>
<div class="m2"><p>سر قلم کردن و این راه بسر پیمودن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غمش بودن و بستن دهن از جوهر راز</p></div>
<div class="m2"><p>در نو پیوستن و سر تو بکس نگشودن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باختن در خم چوگان سر زلف تو جان</p></div>
<div class="m2"><p>و آنگهی گوی سعادت زمان بر بودن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهدم دعوت کوثر کند و عین خطاست</p></div>
<div class="m2"><p>باوجود لب تو دست به آن آلودن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست پوشیده که از دیده چرائی پنهان</p></div>
<div class="m2"><p>کانچنان روی به مردم نتوان بنمودن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن عشق بدان پایه رسانید کمال</p></div>
<div class="m2"><p>که بر آن کس نتواند سخنی افزودن</p></div></div>