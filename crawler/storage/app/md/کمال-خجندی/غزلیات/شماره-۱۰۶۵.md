---
title: >-
    شمارهٔ ۱۰۶۵
---
# شمارهٔ ۱۰۶۵

<div class="b" id="bn1"><div class="m1"><p>من کیم گفتی که گویم خاک نعلین منی</p></div>
<div class="m2"><p>ماه من تا چند نسل باژگونه می‌زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفته بودی دامنم روزی به دست افتد ترا</p></div>
<div class="m2"><p>وعده افتادگان در پای تا کی افکنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دم به دم آهنگ رفتن می‌کنی از پیش من</p></div>
<div class="m2"><p>عمری ای اندک وفا چون عمر از آن در رفتنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من سزایم گفته در کشتن تو ای رقیب</p></div>
<div class="m2"><p>راست می‌گویی تو دشمن خود نه‌ای ای کشتنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گلستان جمالت بر نگیرم چشم تو</p></div>
<div class="m2"><p>فی‌المثل گر چشم من چون چشم نرگس بر کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرمی رنگین زند در شیشه با لعل تو دم</p></div>
<div class="m2"><p>هرچه آید زو فرو خور زان که خام است و دنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌تو چون بیند جهان چشم جهان بین کمال</p></div>
<div class="m2"><p>چون به چشم خویش می‌بیند که چشم روشنی</p></div></div>