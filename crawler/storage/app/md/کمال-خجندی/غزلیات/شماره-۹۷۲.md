---
title: >-
    شمارهٔ ۹۷۲
---
# شمارهٔ ۹۷۲

<div class="b" id="bn1"><div class="m1"><p>ای دهان تو قند و لب همه می</p></div>
<div class="m2"><p>قند پیش لب تو لیس بشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیر از آن قد نهاده سر بگریز</p></div>
<div class="m2"><p>بیشکر دور نیست ناله نی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راز ما فاش کرد خون سرشک</p></div>
<div class="m2"><p>تو کمان را چه می کنی در پی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوختی جان ما به غمزه و زلف</p></div>
<div class="m2"><p>چند ریزیم خاک بر سر وی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتاب از جمال تو خجل است</p></div>
<div class="m2"><p>ناز تا چند و سرکشی تا کی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زندگی بافت از لب تو کمال</p></div>
<div class="m2"><p>که ز رخسارها چکاند خوی</p></div></div>