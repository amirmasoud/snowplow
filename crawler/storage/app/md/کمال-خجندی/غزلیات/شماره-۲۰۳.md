---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>عمریست که با او دل مسکین نگران است</p></div>
<div class="m2"><p>ما در غم و او شادی جان دگران است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای باد مبر خاک کف پاش به هر سو</p></div>
<div class="m2"><p>کان روشنی دیده صاحب نظران است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بلبل و گل بافته بویت به گلستان</p></div>
<div class="m2"><p>این نعره زنان از غم و آن جامه دران است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بر دل مجروح رسد نیر نو سهل است</p></div>
<div class="m2"><p>این هم گذرد چون همه چیزی گذران است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دات نتوان گفت که بر سینه عذاب است</p></div>
<div class="m2"><p>بارت نتوان گفته که بر دیده گران است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم عمر به آخر شده و هم بسته به پایان</p></div>
<div class="m2"><p>این راه مطلب را به کنار ونه کران است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ریختن خون کمال است مرادت</p></div>
<div class="m2"><p>ما نیز بر آنیم که تیغ تو بران است</p></div></div>