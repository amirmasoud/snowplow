---
title: >-
    شمارهٔ ۷۷۱
---
# شمارهٔ ۷۷۱

<div class="b" id="bn1"><div class="m1"><p>اگر من از عشق آن دو رخ میرم</p></div>
<div class="m2"><p>ای گل روضه دامنت گیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاشی سازند از گلم مرغی</p></div>
<div class="m2"><p>با کمان ابرونی زند تیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدم آن رخ بخواب خوش سحری</p></div>
<div class="m2"><p>بخت و روز نکوست تعبیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش رو ار نشانیم چون زلف</p></div>
<div class="m2"><p>باید اول نهاد زنجیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد سپیدم چو شیر موی و هنوز</p></div>
<div class="m2"><p>بابت طفل چون می و شیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرص پیران فزون بود به بتان</p></div>
<div class="m2"><p>در گذر ای جوان که من پیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش خط لب گشود و گفت کمال</p></div>
<div class="m2"><p>لطف تحریر بین و تقریرم</p></div></div>