---
title: >-
    شمارهٔ ۴۷۲
---
# شمارهٔ ۴۷۲

<div class="b" id="bn1"><div class="m1"><p>غم عشقت دل ما را همیشه شاد میدارد</p></div>
<div class="m2"><p>چنین ملک خرابی را به ظلم آباد میدارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مده تعلیم خون ریزی به تاز آن چشم جادو را</p></div>
<div class="m2"><p>که خود را اندرین صنعت نوی استاد میدارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا از گریه بیحد مترسانید ای باران</p></div>
<div class="m2"><p>که گرگی اینچنین باران فراوان باد میدارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خیل بندگان خود شمردی سرو بستانرا</p></div>
<div class="m2"><p>که خود را چون غلامان فضول آزاد میدارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدور قامتت برکنده باد آن چشم گونه بین</p></div>
<div class="m2"><p>که چون نرگس نظر بر سرو و بر شمشاد میدارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به باد از خاک پای خود فرستم گفت پیک دل</p></div>
<div class="m2"><p>دو چشم از راه مشتاقی به راه باد میدارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال این درد را زان لب دوائی ممکنست اما</p></div>
<div class="m2"><p>کجا شیرین بیغم را غم فرهاد می دارد</p></div></div>