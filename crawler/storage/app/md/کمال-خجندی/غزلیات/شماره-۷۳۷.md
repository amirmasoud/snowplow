---
title: >-
    شمارهٔ ۷۳۷
---
# شمارهٔ ۷۳۷

<div class="b" id="bn1"><div class="m1"><p>روز و شب از غم عشق تو در اندیشه درم</p></div>
<div class="m2"><p>گرنه از صبر از هجر تو به هر لحظه درم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشک همچون در و رخساره چون زر دارم</p></div>
<div class="m2"><p>غیر ازین هست و مرا نیست دگر وجه درم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه در خانه دلگیر فراقم ناگاه</p></div>
<div class="m2"><p>در حریم حرم وصل کشانید درم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرض عشق تو را صبر دوا می‌سازم</p></div>
<div class="m2"><p>نو جفا میکنی و من به وفای تو درم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ببینم رخ زیبای تو ناگاه ز دور</p></div>
<div class="m2"><p>من دلخسته کوی تو از آن در گذرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ملک صورت خوب تو چو شد ملک دلم</p></div>
<div class="m2"><p>جان ز دست غم عشقت به سلامت نبرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست از عشق تو و پای ز کویت نکشم</p></div>
<div class="m2"><p>اگر اندر ره عشق تو ببرّند سرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سر هر دو جهان بگذرم از شادی آن</p></div>
<div class="m2"><p>گر کند چشم نواز لطف دگر یک نظرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زانکه در خاطر من عشق گرفته ست کمال</p></div>
<div class="m2"><p>هست در خاطر از آن درد و الم بیشترم</p></div></div>