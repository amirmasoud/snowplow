---
title: >-
    شمارهٔ ۴۴۲
---
# شمارهٔ ۴۴۲

<div class="b" id="bn1"><div class="m1"><p>زلفت که بر سمن گرهی عنبرین زند</p></div>
<div class="m2"><p>توقیع حسن بر ورق یاسمین زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهریست نقش خاتم دولت که آفتاب</p></div>
<div class="m2"><p>آنرا ز مهر عارضی نو بر زمین زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیف است اگر به خاک سر کویت ای صنم</p></div>
<div class="m2"><p>رضوان دم از لطافت خلد برین زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورتگری که نقش تو بیند غریب نیست</p></div>
<div class="m2"><p>گر خط نسخ در رخ خوبان چین زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شاهدی که شهد لبان چون قند تو</p></div>
<div class="m2"><p>صد بار طعنه بر شکر و انگبین زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرد از نهاد گوشه نشینان برآورد</p></div>
<div class="m2"><p>ترک کمانکش تو چو تیر از کمین زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کی کمال را هوس خاک پای تو</p></div>
<div class="m2"><p>آبی ز دیده بر جگر آتشین زند</p></div></div>