---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>آن چشم نیمه مست جهانی خراب ساخت</p></div>
<div class="m2"><p>دلها بسوخت تیمی و نیمی کباب ساخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صیاد وار غمزة شوخش ز زلف و خال</p></div>
<div class="m2"><p>بنهاد دام و دانه و خود را به خواب ساخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرمنده اند از رخ زیباش نو خطان</p></div>
<div class="m2"><p>آری سیاه رو همه را آفتاب ساخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از قند تا بساخت شراب آن لب لطیف</p></div>
<div class="m2"><p>ما را نساخت شربت دیگر شراب ساخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حقه کرد و برد دهان تر از میان</p></div>
<div class="m2"><p>آن لب مفرحی که ز یاقوت ناب ساخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کوی بار دیده گریان برای خویش</p></div>
<div class="m2"><p>همچون حباب خانه به بالای آب ساخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب با کمال ده چو ز جان ناله برکشید</p></div>
<div class="m2"><p>ساقی شراب دار که مطرب رباب ساخت</p></div></div>