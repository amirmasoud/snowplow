---
title: >-
    شمارهٔ ۹۸۶
---
# شمارهٔ ۹۸۶

<div class="b" id="bn1"><div class="m1"><p>بچشم جان چو چراغی که در میان ز جاجی</p></div>
<div class="m2"><p>ز عشق آب حیاتی ز عقل ملح اجاجی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین مرقع اگر چون کلاه صاحب ترکی</p></div>
<div class="m2"><p>ز قالب ارچه شوی دور بر سر همه تاجی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر به شیوه منصور دم زنی ز اناالحق</p></div>
<div class="m2"><p>یقین شود دم آخر که چند مرده حلاجی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعلم و عقل فرو ماندی از همه عجب است این</p></div>
<div class="m2"><p>که فیل داری و اسب و پیاده چون شه عاجی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر دماغ تو صوفی به بانگ چنگ شود تر</p></div>
<div class="m2"><p>که از قدح نکشیدی عظیم خشک مزاجی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درون دل بفروز ای خیال دوست که ما را</p></div>
<div class="m2"><p>هزار درد اگرت هست ازو کمال مخور غمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین سراچه تیره تو نور بخش سراجی</p></div>
<div class="m2"><p>چو درد دوست بود قابل هزار علاجی</p></div></div>