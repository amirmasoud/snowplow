---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>هر که در عالم کم از یک لحظه دور از بار زیست</p></div>
<div class="m2"><p>کرد نقد زندگانی شایع اره پسیار زیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق نالان می نگرفت بی رویش قرار</p></div>
<div class="m2"><p>عندلیب زار نتوانست بی گلزار زیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ا گر شنیدی بوی تر از خود برفی بیخبر</p></div>
<div class="m2"><p>زاهد خود بین که عمری عاقل و هوشیار زیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با خیال بار عاشق شب به عمر خود نخفت</p></div>
<div class="m2"><p>شمع چندانی که بودش زندگی بیدار زیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شربت دردت مریض عشق را باید حلال</p></div>
<div class="m2"><p>گر کسی درمان بجست او سالها بیمار زیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با رقیبانت به بوی وصل خوشدل میزیم</p></div>
<div class="m2"><p>بر امید گل چو بلبل می توان با خار زیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ا گر برآید سرو شاید از سر خاک کمال</p></div>
<div class="m2"><p>سالها چون با خیال آن تد و رخسار زیست</p></div></div>