---
title: >-
    شمارهٔ ۴۸۰
---
# شمارهٔ ۴۸۰

<div class="b" id="bn1"><div class="m1"><p>گدای کوی ترا پادشاه میخوانند</p></div>
<div class="m2"><p>چو راه یافته بر آن در براه میخوانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمر به خدمت تو هر که بست شاهانش</p></div>
<div class="m2"><p>بقدر مرتبه صاحب کلام می خوانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آستان تو درویش بی سر و پا را</p></div>
<div class="m2"><p>سرانه ملک و اصحاب جاه میخوانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیال روی نو هر پارسا که قبله نساخت</p></div>
<div class="m2"><p>همه عبادت او را گناه میخوانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که لوح دل از نقش غیر پاک نشست</p></div>
<div class="m2"><p>بحشر نامه او را سیاه میخوانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلا رهی بحریم وصال جو باری</p></div>
<div class="m2"><p>ترا چو محرم آن بارگاه میخوانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرید وپیر بسر رقص می کنند کمال</p></div>
<div class="m2"><p>چو گفت های تو در خانقاه میخوانند</p></div></div>