---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>چشمت به غمزه کشن من بیگناه را</p></div>
<div class="m2"><p>خود زلف را چه گویم و خال سیاه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آه و روی زرد ز خالت شدیم دور</p></div>
<div class="m2"><p>باد آمد و ز دانه جدا کرد کاه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم ز مه حساب گرفتند سالها</p></div>
<div class="m2"><p>نگرفت در حساب جمال تو ماه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوهر که قیمتی است کشندش به احتیاط</p></div>
<div class="m2"><p>من هم به دیده می کشم آن خاک راه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از همت گدای تو باشد فرو هنوز</p></div>
<div class="m2"><p>بی عرش اگر کشند شهان بارگاه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلطان حسن گو سوی دلها نظر گمار</p></div>
<div class="m2"><p>ملک آن اوست کو بنوازد سپاه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نام کمال خواجه که درویش خوانده ای</p></div>
<div class="m2"><p>درویش خوانده ای به غلط پادشاه را</p></div></div>