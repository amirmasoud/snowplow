---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>دام دلهاست زلف دلبر ما</p></div>
<div class="m2"><p>خوانمش دام ظله ابدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صید از آن دام زلف چو بجهد</p></div>
<div class="m2"><p>زآنکه دامی است پیچ پیچ و دوتا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا جدا ساختی ز بند دو زلف</p></div>
<div class="m2"><p>دل من ساختی ز بند جدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه کشم ناز و گه کشم زلفت</p></div>
<div class="m2"><p>بنگر کز تو می کشیم چها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ریخت خونهای تازه در کویت</p></div>
<div class="m2"><p>تا بریدند سر دو زلف ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوید آن زلف لا چو خواهم وصل</p></div>
<div class="m2"><p>چند گوید سیاه رو لالا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک راه تو شد کمال و تو زلف</p></div>
<div class="m2"><p>هم نکردی به خاک راه رها</p></div></div>