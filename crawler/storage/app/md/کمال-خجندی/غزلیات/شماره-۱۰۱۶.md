---
title: >-
    شمارهٔ ۱۰۱۶
---
# شمارهٔ ۱۰۱۶

<div class="b" id="bn1"><div class="m1"><p>خواهی که به هیچ غم نمیری</p></div>
<div class="m2"><p>تا دست دهد پیاله گیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می نوش به شادی و شو از او</p></div>
<div class="m2"><p>آن دم که به دست غم اسیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی گفت به زیر لب همین است</p></div>
<div class="m2"><p>اگر اهل دلی نفس پذیری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سرزمی ات چو تاج لعل است</p></div>
<div class="m2"><p>سلطانی و صاحب سریری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من درد کشم نه شاه و درویش</p></div>
<div class="m2"><p>فارغ ز بزرگی و حقیری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عشق جوانم و توانگر</p></div>
<div class="m2"><p>غم نیست ز پیری و قیری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سرو روان عصای پیری</p></div>
<div class="m2"><p>شد پیر کمال بایدش ساخت</p></div></div>