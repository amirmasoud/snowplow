---
title: >-
    شمارهٔ ۵۳۸
---
# شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>هر سحر کز سر کوی تو صبا برخیزد</p></div>
<div class="m2"><p>عالمی با دل آشفته ز جا برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که رساند بر کوی نو خاک تن من</p></div>
<div class="m2"><p>مگر این کار هم از دست صبا برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برنخیزم پس از این از سر کویت هرگز</p></div>
<div class="m2"><p>هرکه در کوی تو بنشست کجا برخیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتنه ها خاست از آن زلف که هندوی تو بود</p></div>
<div class="m2"><p>تا از آن ترک کماندار چها برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شود گر نفی خوش بنشینی با ما</p></div>
<div class="m2"><p>تا به یک بار غم از خاطر ما برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو مپندار که بیچاره کمال از در تو</p></div>
<div class="m2"><p>به بلا دور شود باز جفا برخیزد</p></div></div>