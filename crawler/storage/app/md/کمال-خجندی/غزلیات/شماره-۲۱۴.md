---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>گر جانب محب نظری با حبیب هست</p></div>
<div class="m2"><p>غم نیست گر هزار هزارش رقیب هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با کس مگو که چاره کنند درد عشق را</p></div>
<div class="m2"><p>ای خواجه گر طبیب نباشد حبیب هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر در مکش ز ناله ما ای درخت ناز</p></div>
<div class="m2"><p>هرجا که هست شاخ گلی عندلیب هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشی که شد به حلقه عشق بنان گران</p></div>
<div class="m2"><p>نشنیده ام که قابل پند ادیب هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شحنه می برد سر واعظ به تیغ کند</p></div>
<div class="m2"><p>کار شمشیر زنگ خورده بدست خطیب هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خورد گوش بار بدست من غریب</p></div>
<div class="m2"><p>گر نیست گوهری سخنان غریب هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جام وصل هم رسدت قطرة کمال</p></div>
<div class="m2"><p>کز جرعه خاک را همه وقتی نصیب هست</p></div></div>