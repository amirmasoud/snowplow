---
title: >-
    شمارهٔ ۷۰۲
---
# شمارهٔ ۷۰۲

<div class="b" id="bn1"><div class="m1"><p>تا خانه دل جای نمنای تو کردیم</p></div>
<div class="m2"><p>در خانه چراغ از رخ زیبای تو کردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوریده سری جمله گرفتیم بگردن</p></div>
<div class="m2"><p>آنگه چو سر زلف تو سودای تو کردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدیم دل و عقل ز خود دور به صد گام</p></div>
<div class="m2"><p>آن روز که از دور تماشای تو کردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پستی و بالا همه کس نعره برآورد</p></div>
<div class="m2"><p>هر جا که حدیث قد و بالای تو کردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر لحظه بما گرم تری از ستم و جور</p></div>
<div class="m2"><p>تا در دل آنشکده مأوای تو کردیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سینه ما چند نهد فرقت نو داغ</p></div>
<div class="m2"><p>آخر نه به این سینه تمنای تو کردیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما از مناع دو جهان بود دل و دین</p></div>
<div class="m2"><p>آن نیز نثار قد رعنای تو کردیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون رفت کمال از نظرت طلعت دلدار</p></div>
<div class="m2"><p>قطع نظر از دیدهٔ بینای تر کردیم</p></div></div>