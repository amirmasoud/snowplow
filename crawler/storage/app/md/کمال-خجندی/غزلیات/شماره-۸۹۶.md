---
title: >-
    شمارهٔ ۸۹۶
---
# شمارهٔ ۸۹۶

<div class="b" id="bn1"><div class="m1"><p>ای نور دیده را نگرانی بسوی تو</p></div>
<div class="m2"><p>جانا تعلقیست دلم را بکوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دیگران ز وصل تو درمان طلب کنند</p></div>
<div class="m2"><p>ما را بس است درد تو و آرزوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم جهان به ماه رخت دین سالهاست</p></div>
<div class="m2"><p>بگذشت روز ما و ندیدست روی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رهگذار بار چه برخیزد ار دمی</p></div>
<div class="m2"><p>دل را گشایشی رسد از بند موی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با ما دمی برآر که جان غریب ما</p></div>
<div class="m2"><p>ماندست در بدن متعلق ببویه تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنشین دمی بجوی دل ما که سالها</p></div>
<div class="m2"><p>ننشسته ایم بکنفس از جست و جوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گونی حکایتی زلبش گفته کمال</p></div>
<div class="m2"><p>کاب حیات میچکد از گفتگوی تو</p></div></div>