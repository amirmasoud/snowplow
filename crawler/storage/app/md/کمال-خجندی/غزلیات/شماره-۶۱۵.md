---
title: >-
    شمارهٔ ۶۱۵
---
# شمارهٔ ۶۱۵

<div class="b" id="bn1"><div class="m1"><p>دارم من از جهان غم باری همین و بس</p></div>
<div class="m2"><p>در سر خیال روی نگاری همین و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما از بنان موی میان شکر دهان</p></div>
<div class="m2"><p>بوسی طمع کنیم و کناری همین و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سودای هر کسی زر و سیم است و آن ما</p></div>
<div class="m2"><p>سودای بار سیم عذاری همین و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی او به هر چه حکم کند بار می کنیم</p></div>
<div class="m2"><p>صبری نمی کنیم و فراری همین و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زینسان که خاک راه شدیم از گذار تو</p></div>
<div class="m2"><p>میکن به خاک راه گذاری همین و بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لشکر به قصد ملک دل ما چه می کشی</p></div>
<div class="m2"><p>زین سو روانه ساز سواری همین و بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر میکنی غبار ز چشم کمال دور</p></div>
<div class="m2"><p>از خاک پا فرست غباری همین و بس</p></div></div>