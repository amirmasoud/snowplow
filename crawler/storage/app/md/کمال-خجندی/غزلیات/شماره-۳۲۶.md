---
title: >-
    شمارهٔ ۳۲۶
---
# شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>بادی که نیست از سر کوی تو نیست باد</p></div>
<div class="m2"><p>دور هست و نیست همره بوی نو نیست باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هست در با اثر حسینی و نیست</p></div>
<div class="m2"><p>باد آشفته سلاسل موی تو نیست باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کس کهیافت بوی تو آنگه ز شوق آن</p></div>
<div class="m2"><p>چون باد نیست در تک و پوی تو نیست باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو شو خراب خانه چشمم ز سیل اشک</p></div>
<div class="m2"><p>چشمی که هست بر لب جوی تو نیست باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفتم به باغ بی تو و گفتم به باغبان</p></div>
<div class="m2"><p>هر گل که هست بر لب جوی تو نیست باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو دیر ز میکده ای رند درد نوش</p></div>
<div class="m2"><p>زاهد که سنگ زد به سبوی تو نیست باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر گوییم کمال ز من حاجتی بخواه</p></div>
<div class="m2"><p>گویم رقیب از سر کوی تو نیست باد</p></div></div>