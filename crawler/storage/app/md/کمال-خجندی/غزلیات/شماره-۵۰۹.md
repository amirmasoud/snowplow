---
title: >-
    شمارهٔ ۵۰۹
---
# شمارهٔ ۵۰۹

<div class="b" id="bn1"><div class="m1"><p>مرا دلیست که از بار بار میطلبد</p></div>
<div class="m2"><p>بسوز سینه انگار بار می طلبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا دلیست که گر مست باشد و هوشیار</p></div>
<div class="m2"><p>زمست خواه ز هشیار بار می طلبد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکنج صومعه هوشیار در طلب نه و مست</p></div>
<div class="m2"><p>فتاده بر در خمار یار می طلبد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از طرف بر در و دیوار کعبه اوست مراد</p></div>
<div class="m2"><p>که عاشق از در و دیوار بار می طلبد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخواست جنت اعلی و حور صاحب طور</p></div>
<div class="m2"><p>زیار طالب دیدار بار می طلبد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شاخسار طلب عندلیب شب</p></div>
<div class="m2"><p>نشسته با دل بیدار بار می طلبد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه شب دو کون طالب گلزار جتند و کمال</p></div>
<div class="m2"><p>از بوستان و گلزار بار می طلبد</p></div></div>