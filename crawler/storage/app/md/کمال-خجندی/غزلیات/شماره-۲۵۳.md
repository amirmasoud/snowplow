---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>مرا که ساغر چشم از غم تو پر خون است</p></div>
<div class="m2"><p>چه جای ساقی و جام و شراب گلگون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حکایت نو به تفسیر شرح نتوان کرد</p></div>
<div class="m2"><p>که جور و محت خوبان ز وصف بیرون است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به لب رسید مرا از غم تو جان هرگز</p></div>
<div class="m2"><p>از راه لطف نپرسی که حال تو چون است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه اعتبار به عهد تو حسن لیلی را</p></div>
<div class="m2"><p>که زیر هرخم زلفت هزار مجنون است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو جان من به لب آمد رقیب را چه خبر</p></div>
<div class="m2"><p>که من غریقم و او بر کنار جیجون است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آن شمایل موزون چگونه دل نرود</p></div>
<div class="m2"><p>علی الخصوص کسی را که طبع موزون است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشست اگر به حدیث کمال داری گوش</p></div>
<div class="m2"><p>لطافت سخنانش چو در مکنون است</p></div></div>