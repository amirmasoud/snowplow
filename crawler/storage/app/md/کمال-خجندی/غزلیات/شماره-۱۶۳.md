---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>دوست در جان و نیست خبرت</p></div>
<div class="m2"><p>تشنه میری و آب در نظرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام دریا دلی برآوردی</p></div>
<div class="m2"><p>طرفه اینه کآب نیست بر جگرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه پیش تو رفت ذکر فرات</p></div>
<div class="m2"><p>صفت آب کرد تشنه ترت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برهد جانت از تعطش آب</p></div>
<div class="m2"><p>که بسره وقت ما فتد گذرت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خدا و بهشت مژده دهان</p></div>
<div class="m2"><p>به خدا می دهند درد سرت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آدم از خود بهشت نیک بهشت</p></div>
<div class="m2"><p>مرد باید به همت پدرت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دو عالم نظر من چو کمال</p></div>
<div class="m2"><p>تا نمایند عالم دگرت</p></div></div>