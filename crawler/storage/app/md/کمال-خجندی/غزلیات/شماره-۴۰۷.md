---
title: >-
    شمارهٔ ۴۰۷
---
# شمارهٔ ۴۰۷

<div class="b" id="bn1"><div class="m1"><p>دل گرمم ز نو بر آتش غم سوخته باد</p></div>
<div class="m2"><p>آتش عشق تو دره جان من افروخته باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان که خو کرده به نشریف جفاهای تو بود</p></div>
<div class="m2"><p>چون تو رفتی به بلاهای تو آموخته باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جگر خسته ز پیکان تو گر پاره شود</p></div>
<div class="m2"><p>هم از آن کیش به یک تیر دگر دوخته باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نظر دوخت به هر نیر نو چشم آن همه تیر</p></div>
<div class="m2"><p>یک به یک، در نظر دوخته اندوخته باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قیمت بنده چه داند که بصد جان عزیز</p></div>
<div class="m2"><p>هم نسیم سر یک موی تو بفروخته باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو برخ شمعی و پروانه جانسوز کمال</p></div>
<div class="m2"><p>شمع افروخته پروانه او سوخته باد</p></div></div>