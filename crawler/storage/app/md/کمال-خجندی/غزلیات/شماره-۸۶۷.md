---
title: >-
    شمارهٔ ۸۶۷
---
# شمارهٔ ۸۶۷

<div class="b" id="bn1"><div class="m1"><p>عاشق کیست دلم باز نخواهم گفتن</p></div>
<div class="m2"><p>سر مونی بکس این راز نخواهم گفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصف آن روی کز آسیب نظرهاست نهان</p></div>
<div class="m2"><p>پیش رندان نظر باز نخواهم گفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بپرسد ز من آن غمزه که خون نوکه ریخت</p></div>
<div class="m2"><p>هرگز این راز بغماز نخواهم گفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گله ناز و عتاب تو به آن ابرو و چشم</p></div>
<div class="m2"><p>کشی صد رهم از ناز نخواهم گفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش بالات کز آن قامت طوبی پست است</p></div>
<div class="m2"><p>سخن سرو سرافراز نخواهم گفتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مقامی که برانم سخن از سنگدلان</p></div>
<div class="m2"><p>جز حدیث تو در آغاز نخواهم گفتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بگویم ز سگ کوی نو وصفی به کمال</p></div>
<div class="m2"><p>جز به اکرام و به اعزاز نخواهم گفتن</p></div></div>