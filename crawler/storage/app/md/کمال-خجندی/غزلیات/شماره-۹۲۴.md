---
title: >-
    شمارهٔ ۹۲۴
---
# شمارهٔ ۹۲۴

<div class="b" id="bn1"><div class="m1"><p>ای شیشهٔ دل ما در زیر پا شکسته</p></div>
<div class="m2"><p>سنگیندلی گزیده عهد و وفا شکسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با طاق های ابرو دلها شکسته هر سو</p></div>
<div class="m2"><p>ما را بسیار شیشه دیدم از طاق ها شکسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود آرزوی زلفت دلهای عاشقانرا</p></div>
<div class="m2"><p>آن آرزوی دلها باد صبا شکسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با قامت تو طوبی در لطف کرده دعوی</p></div>
<div class="m2"><p>شرط ادب ندانست آن شاخ پا شکسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نامت زبان خامه چون برده پیش نامه</p></div>
<div class="m2"><p>از غصه جدائی هر یک جدا شکسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون غنچه درنگنجم در پیرهن زشادی</p></div>
<div class="m2"><p>آن دم که بهر قلم عطف قبا شکسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دی گفت خاک پایم خون کمال ارزد</p></div>
<div class="m2"><p>بر عادت بزرگی خود را بها شکسته</p></div></div>