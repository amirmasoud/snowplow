---
title: >-
    شمارهٔ ۱۰۵۴
---
# شمارهٔ ۱۰۵۴

<div class="b" id="bn1"><div class="m1"><p>گر لذت خونریزی آن غمزه شناسی</p></div>
<div class="m2"><p>از تیغ نترسی و ز کشتن نهراسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل همه رفتند ز دلبر به شکایت</p></div>
<div class="m2"><p>صد شکر کزین درد تو در شکر و سپاسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نیست به اندازه روی تو که در حسن</p></div>
<div class="m2"><p>افزونی از اندازه و بیرون ز لباسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزرده چه سازی به لباس آن تن نازک</p></div>
<div class="m2"><p>جانی تو سراپای چه محتاج لباسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درویش نرا جای پر از اطلس چرخ است</p></div>
<div class="m2"><p>خوش باش دو سه روز که در زیر پلاسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای شه سر کاوس اگرت کاس شرابست</p></div>
<div class="m2"><p>یاد آرز دوری که تواش جامی و کاسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دستی نتوان برد کمال از فلک و مهر</p></div>
<div class="m2"><p>مادام که بازیچه این مهره و طاسی</p></div></div>