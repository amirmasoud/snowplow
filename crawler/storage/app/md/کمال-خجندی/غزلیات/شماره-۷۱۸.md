---
title: >-
    شمارهٔ ۷۱۸
---
# شمارهٔ ۷۱۸

<div class="b" id="bn1"><div class="m1"><p>حقوق ناز و عتاب حبیب من دانم</p></div>
<div class="m2"><p>تو حق شناس نئی ای رقیب من دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهاده بر سر خوان عشق او کباب جگر</p></div>
<div class="m2"><p>به نیت که نهاد آن نصیب من دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو من کشیده ام از جور او بسی فریاد</p></div>
<div class="m2"><p>چه‌ها کشید ز گل عندلیب من دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهفته معنی نازک بسبست در خط بار</p></div>
<div class="m2"><p>تو فهم آن نکنی ای ادیب من دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم به زلف تو چونست ازین غریب مپرس</p></div>
<div class="m2"><p>که شام چون گذرد بر غریب من دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبا چه گفت شنیدی به من رها کن زلف</p></div>
<div class="m2"><p>که عطرسای منم قدر طببه من دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال غم مخور از درد دل که دلبر گفت</p></div>
<div class="m2"><p>که این علاج نداند طبیب من دانم</p></div></div>