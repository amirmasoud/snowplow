---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>از تو یک ساعت جدایی خوش نمی آید مرا</p></div>
<div class="m2"><p>با دگر کس آشنایی خوش نمی آید مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی ام رو زین در وسلطان وقت و خویش باش</p></div>
<div class="m2"><p>بعد سلطانی گدایی خوش نمی آید مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چاکرانت را نمی گویم که خاک آن درم</p></div>
<div class="m2"><p>با بزرگان خود ستایی خوش نمی آید مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش در آب عارض عکس جان با ما نمای</p></div>
<div class="m2"><p>گفت هر دم خود نمایی خوش نمی آید مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لب لعلت نپرهیزم بدور آن دو چشم</p></div>
<div class="m2"><p>پیش مستان پارسایی خوش نمی آید مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منکر زهدم برویت تا نظر باز آمدم</p></div>
<div class="m2"><p>پاکبازم من دغایی خوش نمی آید مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صوفیان گویند چون ما خیز و در رقص آ کمال</p></div>
<div class="m2"><p>حالت و وجد ریایی خوش نمی آید مرا</p></div></div>