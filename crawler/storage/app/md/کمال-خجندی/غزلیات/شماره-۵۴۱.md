---
title: >-
    شمارهٔ ۵۴۱
---
# شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>هر کجا با باد آن لب مجلسی انگیختند</p></div>
<div class="m2"><p>می پرستان می بکف از هر طرف در ریختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تلخکامی برد جام از ما به دوران لبت</p></div>
<div class="m2"><p>ساقیان در بادهها گونی شکر آمیختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آهوان بر گوشه گلزار دیدند آن دو چشم</p></div>
<div class="m2"><p>هر بکی زآن تیر غمزه جانبی بگریختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تازگی و لطف دزدید از بناگوش تو در</p></div>
<div class="m2"><p>غوط ها دادند در آب آنگهش آویختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سجود آمد در و دیوار پیش آن جمال</p></div>
<div class="m2"><p>از نو در بتخانه ها هر سورتی کانگیختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصه دردم فرو می ریخت گردم پیش دوست</p></div>
<div class="m2"><p>گر پس از من دوستان خاک مرا می پیختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدعی گوید کمال از عشق رویت شد چو زر</p></div>
<div class="m2"><p>چند گوید چون مرا از بوته زینسان ریختند</p></div></div>