---
title: >-
    شمارهٔ ۵۰۰
---
# شمارهٔ ۵۰۰

<div class="b" id="bn1"><div class="m1"><p>ما بکوی یار خود بخود سفر خواهیم کرد</p></div>
<div class="m2"><p>سر بر رخ او هم به چشم او نظر خواهیم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کسی از سرزمینی سر بر آرد روز حشر</p></div>
<div class="m2"><p>از خاک آستانش سر بدر خواهیم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جنگها داریم با زلفش ولی در پای او</p></div>
<div class="m2"><p>باز اگر افتیم با همسر بر خواهیم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سپر مانع شود نیری که بر ما افکند</p></div>
<div class="m2"><p>بار دیگر جنگ سخنی با سپر خواهیم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناگهانی کز تو تشریف بلا کمتر رسد</p></div>
<div class="m2"><p>زانای ناگهان هر یک حذر خواهیم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شنو ای داناه حکایتهای ما دیوانگان</p></div>
<div class="m2"><p>ورته ما از خود ترا دیوانه تر خواهیم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور به ما همراه خواهی شد ز خود بگذر کمال</p></div>
<div class="m2"><p>زانکه ما از منزل هستی سفر خواهیم کرد</p></div></div>