---
title: >-
    شمارهٔ ۷۹۵
---
# شمارهٔ ۷۹۵

<div class="b" id="bn1"><div class="m1"><p>من ز جانان به جان گریخته‌ام</p></div>
<div class="m2"><p>وز جفای جهان گریخته‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفرین بر گریزپایی من</p></div>
<div class="m2"><p>کز غم این و آن گریخته‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلق در خانه‌ام کجا یابند</p></div>
<div class="m2"><p>که من از خان و مان گریخته‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر درش دیده‌ام رقیبان را</p></div>
<div class="m2"><p>چون گدا از سگان گریخته‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت: از من گریخت نتوانی</p></div>
<div class="m2"><p>گفتمش من از آن گریخته‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنده هرگز گریخت ز آزادی</p></div>
<div class="m2"><p>از در او من آن گریخته‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر تو ناگه گریختی ز کمال</p></div>
<div class="m2"><p>من از او هر زمان گریخته‌ام</p></div></div>