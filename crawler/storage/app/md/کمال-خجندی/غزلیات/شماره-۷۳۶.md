---
title: >-
    شمارهٔ ۷۳۶
---
# شمارهٔ ۷۳۶

<div class="b" id="bn1"><div class="m1"><p>روز نشاطست و عیش باده بیارید و جام</p></div>
<div class="m2"><p>زآنکه به جان آمدم در غم ناموس و نام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست مرا آرزو یک دو مراد از جهان</p></div>
<div class="m2"><p>صحبت یاران خوش صحت و شرب مدام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور نبود هیچ ازین دولت حسن تو باد</p></div>
<div class="m2"><p>عشق تو ما را بس است درد تو ما را تمام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذوق درونی و درد لازمه عاشقیست</p></div>
<div class="m2"><p>هر که در و این دو نیست عشق برو شد حرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه بشویم به می سینه پر جوش را</p></div>
<div class="m2"><p>از سر من کی رود این همه سودای خام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده بد فرصئی کور بود دایما</p></div>
<div class="m2"><p>عیش کنم لایزال نوش کنم می مدام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پشت و پناه من است سرو قد عالیت</p></div>
<div class="m2"><p>سایه عالیت باد بر سر ما مستدام </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به تو وابسته ام کلی و جزوی کار</p></div>
<div class="m2"><p>فارغم از نیک و بد ایمنم از خاص و عام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعی بسی کرده ام تا به تو ره برده ام</p></div>
<div class="m2"><p>غابت سعی کمال هست همین والسلام</p></div></div>