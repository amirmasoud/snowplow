---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>ای به جان عاشقان خریدارت</p></div>
<div class="m2"><p>غمزها نیز کرده بازارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کنی قصد کشتن یاران</p></div>
<div class="m2"><p>در چنین کارها منم بارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تو آرام جان ز ما رفتی</p></div>
<div class="m2"><p>رفت آرام جان ز رفتارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیم کشته شدم به یک دیدن</p></div>
<div class="m2"><p>کاشکی دیدمی دگر بارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان شیرین تو منم گفتی</p></div>
<div class="m2"><p>جان شیرین فدای گفتارت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم بیمار بر عیادت تست</p></div>
<div class="m2"><p>نظری کن به چشم بیمارت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر نگیرد سر از در تو کمال</p></div>
<div class="m2"><p>گر بمیرد به پای دیوارت</p></div></div>