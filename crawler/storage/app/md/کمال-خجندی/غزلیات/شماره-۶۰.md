---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>عنبرست آن دام دل با مشک ناب</p></div>
<div class="m2"><p>باز سنبل پر گل سوری نقاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز شعر سبز بر به سایبان</p></div>
<div class="m2"><p>با حریر ست آن به گرد آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درج باقوت است با آب حیات</p></div>
<div class="m2"><p>با نهان در لعل میگون در ناب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دم از لعل لب جان پرورت</p></div>
<div class="m2"><p>میرود سرچشمه حیوان در آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارم از چشمت عجایب حالتی</p></div>
<div class="m2"><p>من خراب مست و او مست خراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ندارد بیلب لعلت طرب</p></div>
<div class="m2"><p>بی نمک ذوقی نمی یابد کباب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طوطی طبع کمال از ذوق تو</p></div>
<div class="m2"><p>می فشاند در سخن در خوشاب</p></div></div>