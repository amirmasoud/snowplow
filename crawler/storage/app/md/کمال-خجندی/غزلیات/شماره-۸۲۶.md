---
title: >-
    شمارهٔ ۸۲۶
---
# شمارهٔ ۸۲۶

<div class="b" id="bn1"><div class="m1"><p>با درد تو آرمید نتوان</p></div>
<div class="m2"><p>از داغ تو هم رهیة نتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تیغ به باره از تو بر سر</p></div>
<div class="m2"><p>از همچو توئی برید نتوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون از همه دلبران گزینی</p></div>
<div class="m2"><p>بر تو دگری گزید نتوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخسار چو زر چه سود مارا</p></div>
<div class="m2"><p>وصلت چو بزر خرید نتوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رویت مهه عید عاشقانست</p></div>
<div class="m2"><p>هر دم مه عید دید نتوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیب ذقن شکر دهانان</p></div>
<div class="m2"><p>بوسید توان گزید نتوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند کمال پست کن آه</p></div>
<div class="m2"><p>پست است سخن شنید نتوان</p></div></div>