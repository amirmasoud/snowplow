---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>دل به از وصل رخت در جان تمنایی نیافت</p></div>
<div class="m2"><p>دیده از دیدار تو خوشتر تماشایی نیافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل در دور رخت چندان که هر جا کرد گشت</p></div>
<div class="m2"><p>چون سر زلفت سری خالی ز سودایی نیافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون زمان وصل رویت بود نازک فرصتی</p></div>
<div class="m2"><p>هیچ عاشق فرصت بوسیدن پایی نیافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو نرگس مست عشق از صد قدح سرخوش نشد</p></div>
<div class="m2"><p>تا سر خود زیر پای سرو بالایی نیافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با خیالش آشنا شد دیدهٔ گریان و گفت</p></div>
<div class="m2"><p>همچو این گوهر کسی در هیچ دریایی نیافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل چه داند زین میان چون از دهانش پی نبرد</p></div>
<div class="m2"><p>کی کند فهم دقایق چون معمایی نیافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یافت جانی خوشتر از جنت در او را کمال</p></div>
<div class="m2"><p>لیکن از بسیاری بر خویش را جانی نیافت</p></div></div>