---
title: >-
    شمارهٔ ۳۱۰
---
# شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>از سر هوای وصل تو بیرون نمی‌رود</p></div>
<div class="m2"><p>سودای لیلی از دل مجنون نمی‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم نظر به غیر جمالت نمی‌کند</p></div>
<div class="m2"><p>باد نو از طبیعت موزون نمی‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا دورم از کنار تو یک لحظه نگذرد</p></div>
<div class="m2"><p>کاندر میان دیده و دل خون نمی‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن صورتی که با تو مرا دست داده بود</p></div>
<div class="m2"><p>تا بسته است نقش در دل و بیرون نمی‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آری مگر علاج به قانون نمی‌رود</p></div>
<div class="m2"><p>تا بسته است نقش در دل و بیرون نمی‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی نمی‌رود به دلت آرزوی من</p></div>
<div class="m2"><p>ای آرزوی دیده و دل چون نمی‌رود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خوش کن ای کمال و شکایت مکن ز دوست</p></div>
<div class="m2"><p>گر بر مراد رای تو گردون نمی‌رود</p></div></div>