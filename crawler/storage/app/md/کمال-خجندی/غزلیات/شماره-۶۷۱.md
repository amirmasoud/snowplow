---
title: >-
    شمارهٔ ۶۷۱
---
# شمارهٔ ۶۷۱

<div class="b" id="bn1"><div class="m1"><p>بی تو مرا خواب و خیال وصال</p></div>
<div class="m2"><p>آن همه خوابست به چشم و خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانسوی بالای نو بیدله نرفت</p></div>
<div class="m2"><p>مرغ به بالا نرود جز به بال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاجتم از روی نو یک دیدنست</p></div>
<div class="m2"><p>دور محل نظرست و سؤال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر ورق گل قلم صنع را</p></div>
<div class="m2"><p>خوشتر از آن نقطه نیفتاد خال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوختگان روی به آتش نهند</p></div>
<div class="m2"><p>گر تو به جنت ننمائی جمال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان و سر و هرچه برم پیش تو</p></div>
<div class="m2"><p>هیچ نگری ز من الأ ملال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف تو خواهم به تفال گرفت</p></div>
<div class="m2"><p>دال گرفتند مبارک به قال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الامیه گفتم غزلی تا بری</p></div>
<div class="m2"><p>بر گذر قافیه نام کمال</p></div></div>