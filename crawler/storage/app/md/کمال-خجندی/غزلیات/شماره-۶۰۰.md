---
title: >-
    شمارهٔ ۶۰۰
---
# شمارهٔ ۶۰۰

<div class="b" id="bn1"><div class="m1"><p>دریغ از جورت آمد وز جفا نیز</p></div>
<div class="m2"><p>که با من آن نمی داری روا نیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو دشنامم دهی بهتر که غیری</p></div>
<div class="m2"><p>بخواند رحمتم گوید دعا نیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه صیدم من که یکبارم بفتراک</p></div>
<div class="m2"><p>نمی بندی نمی سازی رها نیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنوز اندر سرم مهر تو باشد</p></div>
<div class="m2"><p>اگر از خاک من روید گیا نیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به قتل من هوس تنها نه او راست</p></div>
<div class="m2"><p>که هست آن آرزو در جان مرا نیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دراز افتاده است آن رشته زلف</p></div>
<div class="m2"><p>رسد آن رشته یک روزی مرا نیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال آئین خونریزی گر این است</p></div>
<div class="m2"><p>مخور انده که نگذارد ترا نیز</p></div></div>