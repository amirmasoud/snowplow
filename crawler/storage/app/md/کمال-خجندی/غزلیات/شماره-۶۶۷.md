---
title: >-
    شمارهٔ ۶۶۷
---
# شمارهٔ ۶۶۷

<div class="b" id="bn1"><div class="m1"><p>ز رویم وقت کشتن می رود رنگ</p></div>
<div class="m2"><p>که میترسم بگیر تیغ او زنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشت از خون من نارانده شمشیر</p></div>
<div class="m2"><p>چه حکمت بود پیش از آشتی جنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بازی گل زدم ناگه برو گفت</p></div>
<div class="m2"><p>چرا بر شاخ نازک میزنی سنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بوسی میدهی باریه روانتر</p></div>
<div class="m2"><p>که دارم با دهانش فرصتی تنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سگم می خواند و می خواهدم عذر</p></div>
<div class="m2"><p>سگی باشم اگر دارم ازین ننگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به من هر غم کرو آید رود آه</p></div>
<div class="m2"><p>به استقبال او تا نیم فرسنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال از دل نیاری ناله بیرون</p></div>
<div class="m2"><p>که رسوائیست چون خارج شد آهنگ</p></div></div>