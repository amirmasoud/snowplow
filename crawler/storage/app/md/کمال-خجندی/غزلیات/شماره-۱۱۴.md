---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>تا خیالت را دلمه منزلگه است</p></div>
<div class="m2"><p>از مه تو منزل من پر مه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر لبت بوسم ز بسمل چاره نیست</p></div>
<div class="m2"><p>کافتتاح ملح از بسم الله است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک شبی با ما نشین کز دور عمر</p></div>
<div class="m2"><p>یک شبی ماندست و آن هم کوته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محنت هجر تو ساعت ساعت است</p></div>
<div class="m2"><p>دولت وصل تو ناگه ناگه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چه گونی حاضرم و مستمع</p></div>
<div class="m2"><p>چاکران را گوش بر حکم شه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به دزدی گیرم آن چاه ذقن</p></div>
<div class="m2"><p>کانکه عقل کل ببردست آن چه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ریختی بر هر رهی خون کمال</p></div>
<div class="m2"><p>تا نگویند این چه خون بیره است</p></div></div>