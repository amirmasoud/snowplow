---
title: >-
    شمارهٔ ۳۳۹
---
# شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>به خال لب خط سبزت قرابتی دارد</p></div>
<div class="m2"><p>لب تو از دم عیسی نیابتی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر محزر اشکم که ساخت سرخیها</p></div>
<div class="m2"><p>به لوح چهره خیال کتابتی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب فراق تو تیره است و من از آن به هراس</p></div>
<div class="m2"><p>شبی که ماه ندارد مهابتی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو پهلوی رخت افتم نیاز بوسه کتم</p></div>
<div class="m2"><p>دعای صبح، امید اجابتی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که دید لب لعلت از می رنگین</p></div>
<div class="m2"><p>ندیده ایم که میل انابتی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشسته خوش من و ساقی بکار خود چستیم</p></div>
<div class="m2"><p>اگر چه محتسب ما صلابتی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال گفته تو دلپذیر از آن معنی است</p></div>
<div class="m2"><p>با که معنی سخنانت قرابتی دارد</p></div></div>