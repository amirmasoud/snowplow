---
title: >-
    شمارهٔ ۴۵۸
---
# شمارهٔ ۴۵۸

<div class="b" id="bn1"><div class="m1"><p>صوفی از رندان بپوشد می که در خلوت بنوشد</p></div>
<div class="m2"><p>شد کهن بالای خمها خرقه اش تا کی بپوشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلق و سجاده نهاده دم بدم در رهن باده</p></div>
<div class="m2"><p>باز در بازار دعوی پارسائیها فروشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ز شوق گلرخان نالم نه از جور رقیبان</p></div>
<div class="m2"><p>گر چه خارش دل خراشد بلبل از مستی خروشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر رخش چون دیده بنهادم سرشک آمد بجوشش</p></div>
<div class="m2"><p>آب گرمی مینهم بالای آتش چون بجوشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون دلها خوش نباید خوردنش بی ناله ما</p></div>
<div class="m2"><p>دلبر نازک طبیعت باده بی مطرب ننوشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدم آن لب بر وی از مشک به این خط نوشته</p></div>
<div class="m2"><p>گر نه شیرین است اینجا این همه مور از چه جوشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جست و جوی آن دهان میکن کمال امکان که بایی</p></div>
<div class="m2"><p>خاتم جم با کف آرد هر که در جستن بکوشد</p></div></div>