---
title: >-
    شمارهٔ ۹۷۱
---
# شمارهٔ ۹۷۱

<div class="b" id="bn1"><div class="m1"><p>ای دل این بیچارگی و مستمندی تا به کی</p></div>
<div class="m2"><p>چون نداری روی درمان دردمندی تا به کی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دل پرخون من بگریست امشب چشم جام</p></div>
<div class="m2"><p>شمع مجلس را بگو کاین هرزه‌خندی تا به کی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هواداران ما و تو چو مستغنی است یار</p></div>
<div class="m2"><p>ای رقیب این چاپلوسی و لوندی تا به کی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش قد بارهای سرو سهی شرمی بدار</p></div>
<div class="m2"><p>در چمن با پای چوبین سربلندی تا به کی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو خود را کرد مانندی گل از باد هوا</p></div>
<div class="m2"><p>گفت در رویش صبا کاین خودپسندی تا به کی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمزه جادویت از ما چند پوشاند نظر</p></div>
<div class="m2"><p>عالمی کردی مسخر چشم‌بندی تا به کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویی‌ام هردم که بیرون شو کمال از شهر ما</p></div>
<div class="m2"><p>این سمرقندی گریه‌های خجندی تا به کی</p></div></div>