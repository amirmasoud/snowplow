---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>من به شطرنج غمت جان و جهان خواهم باخت</p></div>
<div class="m2"><p>آن دو رخ دیده ام این بار روان خواهم باخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باختم عشق به آن روی و دلم برد ز دست</p></div>
<div class="m2"><p>تا برد بار دگر باز همان خواهم باخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب چو بازم به رفیقان خود انگشترنی</p></div>
<div class="m2"><p>به خیال لبه آن تنگه دهان خواهم باخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو رسن باز که بازد سر و جان هم بر سر</p></div>
<div class="m2"><p>من به زلفت سر و جان نیز چنان خواهم باخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلفش آمد که به سودازدگان کج بازد</p></div>
<div class="m2"><p>ابرویش جسته که من کجتر از آن خواهم باخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به میان و دهن ننگ نو از بیم رقیب</p></div>
<div class="m2"><p>بعد از امروز نظرهای نهان خواهم باخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه بسیار سر و جان به تو در باخت کمال</p></div>
<div class="m2"><p>من زخجلت که کم است آن در جهان خواهم باخت</p></div></div>