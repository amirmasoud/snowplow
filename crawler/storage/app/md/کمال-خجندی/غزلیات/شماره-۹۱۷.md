---
title: >-
    شمارهٔ ۹۱۷
---
# شمارهٔ ۹۱۷

<div class="b" id="bn1"><div class="m1"><p>ای از حدیث زلف توام بر زبان گره</p></div>
<div class="m2"><p>بگشای برقع از رخ و از زلف آن گره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم گلی نچید ز باغ رخت هنوز</p></div>
<div class="m2"><p>تا کی زند دو زلف تو بر ابروان گره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلفت دلم بیست و در آویخت از هوا</p></div>
<div class="m2"><p>جز باد دلگشا که گشاید چنان گره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوبان که دانه دانه کنند اشک عاشقان</p></div>
<div class="m2"><p>از ساحری زننده بر آب روان گره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابرو ترش کنی و بگویم زهی کمان</p></div>
<div class="m2"><p>آری فتد همیشه ز زه بر کمان گره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موی میان او به کمر هست در خیال</p></div>
<div class="m2"><p>چون رشته که باشدش اندر میان گره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظم کمال بسته به هم رشته درست</p></div>
<div class="m2"><p>گفتار دیگران همه بر ریسمان گره</p></div></div>