---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>شادی نیافت هر که غم دلبری نداشت</p></div>
<div class="m2"><p>در سر هوای مهر پری گستری نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حیرتم زآدمینی که به عمر خویش</p></div>
<div class="m2"><p>سودای عشق روی پر پیکری نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با ما سری به وصل در آور که گیسویت</p></div>
<div class="m2"><p>در پا از آن فتاد که با ما سری نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون باد رفت کشتی عمرم بر آب چشم</p></div>
<div class="m2"><p>اگر چه ثقیل بود ولی لنگری نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و دل در سواد زلف تو گم کرد راه عقل</p></div>
<div class="m2"><p>شب بود و او غریب مگر رهبری نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هر جهت کمال به سوی تو کرد روی</p></div>
<div class="m2"><p>زیرا که چشم مرحمت از دیگری نداشت</p></div></div>