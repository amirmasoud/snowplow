---
title: >-
    شمارهٔ ۲۷۵
---
# شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>هر که ترا بافت دولت در جهان بافت</p></div>
<div class="m2"><p>دولت ازین به نیافت گشته که جان یاخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ز تو بو برده دل ازو اثری نیست</p></div>
<div class="m2"><p>ک خبر او نیافت گز تو نشان یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه نهان شد که آشکار و طلبکار</p></div>
<div class="m2"><p>از نو نشانی به آشکار و نهان یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یافت نشد آن به جد و جهد چه تدبیر</p></div>
<div class="m2"><p>دولت وقت، کسی که دولت آن یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیم نظر همتی که بابی از آن جو</p></div>
<div class="m2"><p>زآنکه کسی هرچه بافت جمله از آن یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یافت درین به یکی گهر دگری خاک</p></div>
<div class="m2"><p>همت جوینده هرچه بود همان یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لان انالحق بزن کمال که رفته است</p></div>
<div class="m2"><p>بر موی تو چون ز دوست نشان یافت</p></div></div>