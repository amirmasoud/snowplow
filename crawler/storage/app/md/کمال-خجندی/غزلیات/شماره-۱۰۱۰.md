---
title: >-
    شمارهٔ ۱۰۱۰
---
# شمارهٔ ۱۰۱۰

<div class="b" id="bn1"><div class="m1"><p>چو گل به لطف نو زد ان نازک اندامی</p></div>
<div class="m2"><p>درید پیرهن نیکوئی به بد نامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم بشام سر زلف نست و میترسم</p></div>
<div class="m2"><p>که باز بشکنی این آبگینه شامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی که میبرد آرام دل به شیوه چشم</p></div>
<div class="m2"><p>چه چشم دارم ازو شیوه دلارامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نکهت سر زلف تو باز دم زد عود</p></div>
<div class="m2"><p>عجب که سوخته و از سر نمی نهد خامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی به حلقة ما ذکر عصمت میرفت</p></div>
<div class="m2"><p>شدند حلقه بگوش تو عارف و عامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی که هیچ نبردی حدیث می به زبان</p></div>
<div class="m2"><p>البانو دید و مثل شد بدرد آشامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال اگر ز دهانش نیافتی کامی</p></div>
<div class="m2"><p>مباش ننگدل و صبر کن بناکامی</p></div></div>