---
title: >-
    شمارهٔ ۶۸۶
---
# شمارهٔ ۶۸۶

<div class="b" id="bn1"><div class="m1"><p>ای زنگیان زلف ترا شاه چین غلام</p></div>
<div class="m2"><p>آئینه دار حاجب رویت به تمام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد روشنم که منزل سرو است جویبار</p></div>
<div class="m2"><p>کز چشم من نمی رود آن سرو خوش خرام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل انتظار وعده وصل تو میکشد</p></div>
<div class="m2"><p>مسکین دل شکسته که دارد خیال خام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانم به لب رسید چو زلف تو بگذرد</p></div>
<div class="m2"><p>آید به لب هر آینه چون بگذرد ز کام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمم چو دید روی تو در تاب زلف گفت</p></div>
<div class="m2"><p>صبح امید است که شد پایبند شام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خونش حلال باد که بی موجبی کند</p></div>
<div class="m2"><p>نظارهٔ جمال نو بر عاشقان حرام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کس ز ننگ و نام طریقی گزیده اند</p></div>
<div class="m2"><p>با عشق تو کمال بر آمد ز ننگ و نام</p></div></div>