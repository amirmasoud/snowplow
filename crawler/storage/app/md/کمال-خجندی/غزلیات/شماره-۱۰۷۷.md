---
title: >-
    شمارهٔ ۱۰۷۷
---
# شمارهٔ ۱۰۷۷

<div class="b" id="bn1"><div class="m1"><p>هر لحظه غمزه‌ها به جفا نیز می‌کنی</p></div>
<div class="m2"><p>باز این چه فتنه‌هاست که انگیز می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل‌های ما نخست به تاراج می‌بری</p></div>
<div class="m2"><p>وآنگه اسپر زلف دلاویز می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خون چکانی از دل عاشق کراست جنگ</p></div>
<div class="m2"><p>شاهی به قلب رفته و خونریز می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بحر دیده آب کجا ایستد ز حوش</p></div>
<div class="m2"><p>زینسان که آتش دل ما نیز می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواب شبان مبند به چشمم دگر خیال</p></div>
<div class="m2"><p>چون همدم به آه سحرخیز می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خون ما چه توبه دهی چشم مست را</p></div>
<div class="m2"><p>از باده حلال چه پرهیز می‌کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهر سرای چون دلت آشفته شد کمال</p></div>
<div class="m2"><p>وقتست اگر عزیمت تبریز می‌کنی</p></div></div>