---
title: >-
    شمارهٔ ۶۶۴
---
# شمارهٔ ۶۶۴

<div class="b" id="bn1"><div class="m1"><p>ز حسرت خاک شد این چشم غمناک</p></div>
<div class="m2"><p>به خاک ار پا نهی باری برین خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکر آموخت آن چشم از تو شوخی</p></div>
<div class="m2"><p>چه زود استاد شد شاگرد چالاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معلقها زند از شادی آن صید</p></div>
<div class="m2"><p>که آویزی پس از بسمل به فتراک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو از رخ خوی به دامن پاک سازی</p></div>
<div class="m2"><p>شود پاکیومتر آن دامن پاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شبگردی چه ترسم بار در چشم</p></div>
<div class="m2"><p>ندارم روز روشن از عسس پاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مرگ محتسب کم خور دل غم</p></div>
<div class="m2"><p>به مقدار مصیبت جامه زن چاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال از خس شپارد کمترت دوست</p></div>
<div class="m2"><p>مگر در دوستی افتاد خاشاک</p></div></div>