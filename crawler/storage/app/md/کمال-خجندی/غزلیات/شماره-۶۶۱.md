---
title: >-
    شمارهٔ ۶۶۱
---
# شمارهٔ ۶۶۱

<div class="b" id="bn1"><div class="m1"><p>به مسجد هفته از تو کجا یک سجده لایق</p></div>
<div class="m2"><p>که در آدینه ای زاهد به شش روز دگر فاسق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>له فی کل موجود علامات و آثار</p></div>
<div class="m2"><p>دو عالم پر ز معشوق است کو یک عاشق صادق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیاوردی کسی در گوش آواز خطیبان را</p></div>
<div class="m2"><p>فلو لا بسمعوا منهم هو المطعم هوالرازق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا از دوست وا مانی به لذت های جسمانی</p></div>
<div class="m2"><p>غم لیلی خوری اولی که شهد و شکر فابق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مریض العشق لایفنی به سکر الموت و المحمی</p></div>
<div class="m2"><p>خوشا سرمستی مجنونه خنکه دلگرمی وامق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسیم الورد بحییکم رحیق الحب یشفیکم</p></div>
<div class="m2"><p>من الظلمات ینجیکم بدون الشمس و الشارق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلت گرم است با دنیا مپوشان ای کمال این تب</p></div>
<div class="m2"><p>چو در دارالشفای دین طبیبی بافتی حاذق</p></div></div>