---
title: >-
    شمارهٔ ۱۰۵۸
---
# شمارهٔ ۱۰۵۸

<div class="b" id="bn1"><div class="m1"><p>مبارک منزلی خوش سرزمینی</p></div>
<div class="m2"><p>که آنجا سر برآرد نازنینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>براین من که گر باشد جز این نیست</p></div>
<div class="m2"><p>که حوری هست و فردوس برینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یقین دانی که چشمش عین فتنه است</p></div>
<div class="m2"><p>گرت حاصل شود عین الیقینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به آن لب ملک دلها شد مسلم</p></div>
<div class="m2"><p>سلیمان ملک راند به انگبینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جو پیش رخ خط آری سوزیم جان</p></div>
<div class="m2"><p>شد این حرفم درست از پیش بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشوید چشمم از غیرت به صد آب</p></div>
<div class="m2"><p>چو بینم بر درت نقش جبینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال از سینه گل مهر آن سرو</p></div>
<div class="m2"><p>تزید صدر پی بالا نشینی</p></div></div>