---
title: >-
    شمارهٔ ۱۰۷۱
---
# شمارهٔ ۱۰۷۱

<div class="b" id="bn1"><div class="m1"><p>نیست بهای جان بسی پیش تو چون کشد کسی</p></div>
<div class="m2"><p>در نظرت جهان و جان نیست به قیمت خسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادی جان اگر توئی نیست غم جهان مرا</p></div>
<div class="m2"><p>غصه چه وحشت آورد با رخ چون تو مونسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لب و غمزة توأم باده پرست و مست هم</p></div>
<div class="m2"><p>باده و ساقئی چنین نیست به هیچ مجلسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیر دو لبه سه بوسه ام گفتی و چشم چار شد</p></div>
<div class="m2"><p>چون به یکی نمیرسی وعده چه میدهی بسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو به من چو بلبلان نالم و بس که در چمن</p></div>
<div class="m2"><p>می رخ زرد و چشم تر نیست گلی و نرگسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سهر قلم ز بیخودی باز نداند از رقم</p></div>
<div class="m2"><p>نقطة خالت ار فتد بر ورق مهندسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یافت کمال وصل تو دولت نقد او ببین</p></div>
<div class="m2"><p>نقد چنین کم اوفتد خاصه به دست مفلسی</p></div></div>