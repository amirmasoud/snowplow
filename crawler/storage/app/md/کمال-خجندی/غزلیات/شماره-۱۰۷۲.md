---
title: >-
    شمارهٔ ۱۰۷۲
---
# شمارهٔ ۱۰۷۲

<div class="b" id="bn1"><div class="m1"><p>ورای آن چه سعادت بود که ناگاهی</p></div>
<div class="m2"><p>به حال بی سروپائی نظر کند شاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چراغ صبحدم دل فروز عالم را</p></div>
<div class="m2"><p>چه کم شود که شود رهنمای گمراهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسیم را چه زیان گر ز راه هم نفسی</p></div>
<div class="m2"><p>کند عنایت دل خسته ای سحرگاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جان و دل شده ام پای بند بند گیت</p></div>
<div class="m2"><p>نه از سر غرضی نه ز روی اکراهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چگونه دست توان داشت از چنین سروی</p></div>
<div class="m2"><p>چگونه روی توان تافت از چنین ماهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هلال ابروی او را ز حسن موئی کم</p></div>
<div class="m2"><p>نگردد ار نگرد سوی ما به هر ماهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن دراز شد و خالص سخن این است</p></div>
<div class="m2"><p>که چون منت نبود مخلص و هواخواهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کمال عز قبول نر از سعادت یافت</p></div>
<div class="m2"><p>که بافت از همه اقران خود چنین جاهی</p></div></div>