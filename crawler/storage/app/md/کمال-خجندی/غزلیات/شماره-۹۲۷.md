---
title: >-
    شمارهٔ ۹۲۷
---
# شمارهٔ ۹۲۷

<div class="b" id="bn1"><div class="m1"><p>ای منت جانفشان دیرینه</p></div>
<div class="m2"><p>داغ عشقت نشان دیرینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفراموشیت نیامده نیز</p></div>
<div class="m2"><p>بادی از عاشقان دیرینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بینو بودم هلاک خویش گمان</p></div>
<div class="m2"><p>کردی گمان دیرینه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو غمم خور جگر که نیست دریغ</p></div>
<div class="m2"><p>میچ ازین میهمان دیرینه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیر گشت و هنوز هست رقیب</p></div>
<div class="m2"><p>آه ازین سخت جان دیرینه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو گلی چون تو بایدم نه بهشت</p></div>
<div class="m2"><p>چه کنم بوستان دیرینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سگ کویت چو دید لاغرییم</p></div>
<div class="m2"><p>بو نکرد استخوان دیرینه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر ندارد کمال تا دمه حشر</p></div>
<div class="m2"><p>سر ازین استان دیرینه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا چو مجنون بساخت دفتر عشق</p></div>
<div class="m2"><p>تازه شد داستان دیرینه</p></div></div>