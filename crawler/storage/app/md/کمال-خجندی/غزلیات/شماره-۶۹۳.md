---
title: >-
    شمارهٔ ۶۹۳
---
# شمارهٔ ۶۹۳

<div class="b" id="bn1"><div class="m1"><p>به خالت نسبت مشک ختنا کردم خطا کردم</p></div>
<div class="m2"><p>من این تشبیه بینیت چرا کردم چرا کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبا انداخت در دستم شی زلف چو چوگانش</p></div>
<div class="m2"><p>چه گویم کآن نفس با او چه کردم چه‌ها کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دیدم قبله روی تو صد ساله نماز خود</p></div>
<div class="m2"><p>به محراب دو ابرویت نضا کردم قضا کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفییت تربیت فرمود یکبارم به دشنامی</p></div>
<div class="m2"><p>من از شادی دوبار او را دعا کردم دعا کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل خود را که از ریش کهن شد نو به نو خسته</p></div>
<div class="m2"><p>به داروخانه دردت دوا کردم دوا کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کز تو نگریزم به خون خود خطی و آنگه</p></div>
<div class="m2"><p>دو چشمت را بر این معنی گوا کردم گوا کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال ار اندکی زآن خط غباری داشت بر خاطر</p></div>
<div class="m2"><p>چو دیدم روی او با او صفا کردم صفا کردم</p></div></div>