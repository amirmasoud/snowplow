---
title: >-
    شمارهٔ ۸۷۱
---
# شمارهٔ ۸۷۱

<div class="b" id="bn1"><div class="m1"><p>کارم ز دست شد نظری کن بکار من</p></div>
<div class="m2"><p>بنگر بکار بنده خداوندگار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فارغ شدم ز جنت و فردوسی و حورعین</p></div>
<div class="m2"><p>تا اوفتاد بر سر کویش گذار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس جان نازنین که چو گل میرود به باد</p></div>
<div class="m2"><p> در پای سروناز تو ای گلعذار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> تا جلوه کرد سرو قدت بر کنار چشم</p></div>
<div class="m2"><p>خالی نگشت آب روان از کنار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی شبی بیایم و بستانم از تو جان</p></div>
<div class="m2"><p>زنهار جان من که مده انتظار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقتی که بگذری بسر تربت کمال</p></div>
<div class="m2"><p>راحت رسد بسی به تن خاکسار من</p></div></div>