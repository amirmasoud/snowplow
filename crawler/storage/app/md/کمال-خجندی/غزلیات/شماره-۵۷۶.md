---
title: >-
    شمارهٔ ۵۷۶
---
# شمارهٔ ۵۷۶

<div class="b" id="bn1"><div class="m1"><p>چندان بگریم بر در آن بیوفا شام و سحر</p></div>
<div class="m2"><p>کز آب چشمم آورد سروی از آنجا سر بدر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنگی که می بود از حمید با آن سگان کو مرا</p></div>
<div class="m2"><p>دوشینه با خاک درش کردیم با هم در بدر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نکهت او بشنود آن زلف در هر جانبی</p></div>
<div class="m2"><p>گردید با باد صبا گلزارها را سر بسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سینه سازد دل سپر از شوق پیکان تو جان</p></div>
<div class="m2"><p>آید به سینه منتظر چون تیر از آن سوی بر جگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسکینی و افتادگی زیبد ز زلف و خال او</p></div>
<div class="m2"><p>هر یک چو با روی نکو دارند سودای دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دیده لوح چهره را با اشک رنگین نقش کن</p></div>
<div class="m2"><p>نقش رخی داری هوس بنویس پند ما به زر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیوسته دارند آن دو لب مهر خموشی بر زبان</p></div>
<div class="m2"><p>دارند گونی بی سخن رازی میان یکدگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بر کبوتر نامه شوقم گرانی می کند</p></div>
<div class="m2"><p>گو نامه بگذار و مرا در بر بگیر آنجا ببر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جنگ رفت آن صف شکن آمد سپرمانع شدن</p></div>
<div class="m2"><p>او جنگ با تیر و کمان کرد و عاشق با سپر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زینسان که دارد چشم او هر سوز مژگان نینها</p></div>
<div class="m2"><p>از ما کمال آن شوخ را آسان بود قطع نظر</p></div></div>