---
title: >-
    شمارهٔ ۳۳۴
---
# شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>با منت لطف جز ستم نبود</p></div>
<div class="m2"><p>ننگ چشمی ترا کرم نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمت از خون ما پشیمان نیست</p></div>
<div class="m2"><p>مرحمت موجب ندم نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه فرستم بر نو جان خراب</p></div>
<div class="m2"><p>پیش تو این متاع کم نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با لبت شهد اگر چه شیرین است</p></div>
<div class="m2"><p>آنچنان حلقه سوز هم نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته سوزمت بر آتش غم</p></div>
<div class="m2"><p>گر غم روی تست غم نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در وقا پای ما نداشت رقیب</p></div>
<div class="m2"><p>ناجوانمرد را قدم نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ننویسد فرشته جرم کمال</p></div>
<div class="m2"><p>بر سر بیدلان قلم نبود</p></div></div>