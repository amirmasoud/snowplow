---
title: >-
    شمارهٔ ۶۸۴
---
# شمارهٔ ۶۸۴

<div class="b" id="bn1"><div class="m1"><p>از لب و زلف او نشان جستم و باز یافتم</p></div>
<div class="m2"><p>آب حیات خوردم و عمر دراز یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر خداست نقطه ان دهن و در آن سخن</p></div>
<div class="m2"><p>واقف سر غیب را محرم راز یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راهروان کعبه گو بر پی من نهید پی</p></div>
<div class="m2"><p>کز سر کوی او رهی سوی حجاز یافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنت نسیه زاهدا تو به نماز یافتی</p></div>
<div class="m2"><p>دولت وصل نقد را من به نیاز یافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به نظاره آمدم روز شکار دلبران</p></div>
<div class="m2"><p>دام دل سبکتکین زلف ایاز یافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی توبه بندگان خود جور و ستم کنی و بس</p></div>
<div class="m2"><p>جمله شهان حسن را بنده نواز یافتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر کوی دلبران بود کمال گم شده</p></div>
<div class="m2"><p>گرد در تو اش طلب کردم و باز یافتم</p></div></div>