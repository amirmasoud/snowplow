---
title: >-
    شمارهٔ ۵۷۸
---
# شمارهٔ ۵۷۸

<div class="b" id="bn1"><div class="m1"><p>خاک راه تر از آن روز که آمد به نظر</p></div>
<div class="m2"><p>خواب در چشم من خسته نیاید دیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تونیا روشنی دیده اگر داشت چرا</p></div>
<div class="m2"><p>دید آن خاک قدم در نظر آورد دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم نگنجد به دل از ذوق دهان تو مرا</p></div>
<div class="m2"><p>صحبت تنگ فتادست مگس را به شکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تینهای مژه بر خاک درت دادم آب</p></div>
<div class="m2"><p>تا کند چشم من از کحل بصر قطع نظر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشنو آه سحر من که پریشان نکنی</p></div>
<div class="m2"><p>خاطر نازک خود همچو گل از باد سحر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون تن لاغر من نیر تو زان شد باریک</p></div>
<div class="m2"><p>که به خونابه خورا می گذراند به جگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دی طبیبم بر خود خواند ز ضعفم پشه ای</p></div>
<div class="m2"><p>بگرفت و بر او ناله کنان برد به پر تازه پر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لحظه لحظه رخم از خاک درت تازه ترست</p></div>
<div class="m2"><p>میشود از خاک بلی گونه زر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوش می گشت بسر بر در تو بی تو کمال</p></div>
<div class="m2"><p>گر نمی گشت چنین با تو نمی گشت بسر</p></div></div>