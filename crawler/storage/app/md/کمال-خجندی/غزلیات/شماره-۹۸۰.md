---
title: >-
    شمارهٔ ۹۸۰
---
# شمارهٔ ۹۸۰

<div class="b" id="bn1"><div class="m1"><p>باز به ناز کش مرا چیست که ناز می‌کنی</p></div>
<div class="m2"><p>ناز نمی‌کنم دگر گونی و باز می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چو شهید عشقم و بر در تو بهشتیم</p></div>
<div class="m2"><p>بر رخ من در بهشت از چه فراز می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دهنت چو می‌رود پیش دو لب حکایتی</p></div>
<div class="m2"><p>جان مرا در آن سخن محرم راز می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو چگونه جان برم چون تو به مرغ آن حرم</p></div>
<div class="m2"><p>حمله باز می‌کنی چشم چو باز می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم به عارضش دلا چیست ز زلف او گله</p></div>
<div class="m2"><p>وقت چنین لطیف و تو قصه دراز می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با رخ دوست زاهدا رو چو به قبله شد ترا</p></div>
<div class="m2"><p>عرض نیاز کن چرا عرض نماز می‌کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زایر کعبه را بگو حلقه به گوش این درم</p></div>
<div class="m2"><p>گوش که می‌کند که تو وصف حجاز می‌کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کمال تا ابد خاک یک آستان و بس</p></div>
<div class="m2"><p>بندگی شهی گزین گر چو ایاز می‌کنی</p></div></div>