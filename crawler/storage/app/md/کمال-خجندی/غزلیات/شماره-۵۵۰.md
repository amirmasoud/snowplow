---
title: >-
    شمارهٔ ۵۵۰
---
# شمارهٔ ۵۵۰

<div class="b" id="bn1"><div class="m1"><p>هر گل که ز خاک من بروید</p></div>
<div class="m2"><p>عاشق شود آنکه آن ببوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دامن دوست خواهد آویخت</p></div>
<div class="m2"><p>خاری که ز تربتم بروید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معشوق شهید عشق خود را</p></div>
<div class="m2"><p>با اشک بشوید و بموید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دیده شود به خاک آن پای</p></div>
<div class="m2"><p>عاشق ره وه به دیده پوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوید دلم آن دهن همیشه</p></div>
<div class="m2"><p>چیزی که نیافت کی چه جوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصف دهنت کمال دائم</p></div>
<div class="m2"><p>در قافیه های تنگ گوید</p></div></div>