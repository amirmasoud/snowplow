---
title: >-
    شمارهٔ ۸۸۲
---
# شمارهٔ ۸۸۲

<div class="b" id="bn1"><div class="m1"><p>نخواهم بیش از این از خلق راز خویش پوشیدن</p></div>
<div class="m2"><p>نمی آید ز من کاری بغیر از باده نوشیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه دیدن خوبان همه عین بلا باشد</p></div>
<div class="m2"><p>به هر صورت که می بینیم دیدن به ز نادیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل و دین را ز درویشی ببخشیدم به درویشی</p></div>
<div class="m2"><p>بیاموزید ای شاهان از این درویش بخشیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه عشق ناگاهان به خاطرها فرود آید</p></div>
<div class="m2"><p>ولیک او را به مدتها بیاید نیز ورزیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک گو هر زمان میگرد از این اوضاع بی حاصل</p></div>
<div class="m2"><p>نخواهد مرکز خاکی ز وضع خویش گردیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نصیحت گو اگر پندی دهد سهل است گر میگو</p></div>
<div class="m2"><p>زبان او و آن گفتار و گوش ما و نشنیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال خسته خاطر را خوش آمد صبحدم ناله</p></div>
<div class="m2"><p>بلی خوش باشد از بلبل بوقت صبح نالیدن</p></div></div>