---
title: >-
    شمارهٔ ۷۵۶
---
# شمارهٔ ۷۵۶

<div class="b" id="bn1"><div class="m1"><p>عمریست کز دیار تو محروم مانده ایم</p></div>
<div class="m2"><p>وز شوق نامهای تو سطری نخوانده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دامنت بدست ارادت گرفته ایم</p></div>
<div class="m2"><p>دامن ز هر چه غیر تو باشد نشانده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حیرتم که بینو چرا مرده نیستم</p></div>
<div class="m2"><p>در خجلتم که بینو چرا زنده مانده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بهر گرد سم سمند تو هر دمی</p></div>
<div class="m2"><p>گلگون اشک در عقبشه تند رانده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیزارم از وجود خود و ماجرای تو</p></div>
<div class="m2"><p>این با کمال ساده درون باز مانده ایم</p></div></div>