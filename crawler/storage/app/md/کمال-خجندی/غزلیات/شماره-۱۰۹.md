---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>بی توقف من از این شهر به در خواهم رفت</p></div>
<div class="m2"><p>ر بی تردد ز پی بار به سر خواهم رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بارها بار گران بر دل و جان بر کف دست</p></div>
<div class="m2"><p>رفته ام از پی مقصود و دگر خواهم رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای عزیزان که ندارید سر همراهی</p></div>
<div class="m2"><p>به اجازت که هم اکنون به سفر خواهم رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با وجود تن بیمار و گرانباری عشق</p></div>
<div class="m2"><p>صبحدم در عقب باد سحر خواهم رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کنم دیده غمدیده به رویش روشن</p></div>
<div class="m2"><p>پیش آن شمع دل اهل نظر خواهم رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوی جمعیت از آن راهگذر می آید</p></div>
<div class="m2"><p>من بدان بوی بر آن راهگذر خواهم رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناز مین بوس در شاه جهان دریابم</p></div>
<div class="m2"><p>اندر این ره چو فلک زیر و زبر خواهم رفت</p></div></div>