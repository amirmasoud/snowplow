---
title: >-
    شمارهٔ ۲۷۹
---
# شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>هرگز ز جان من غم سودای او نرفت</p></div>
<div class="m2"><p>وز خاطر شک تمنای او نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دل سیاه باد که سودای او نپخت</p></div>
<div class="m2"><p>وان سر بریده باد که در پای او نرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با این همه جفا که دل از دست او کشید</p></div>
<div class="m2"><p>سودای دوستی ز سویدای او نرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد عروس گل به چمن با هزار حسن</p></div>
<div class="m2"><p>وز کوی دوست کسی به تماشای او نرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیک نفس که مژده رسان حیات اوست</p></div>
<div class="m2"><p>بی حکم او نیامد و بی رای او نرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسکین کمال در سر غوغای عشق شد</p></div>
<div class="m2"><p>وآن کیست خود که در سر غوغای او نرفت</p></div></div>