---
title: >-
    شمارهٔ ۲۳۷
---
# شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>لبت را هر که چون شگر مزیده است</p></div>
<div class="m2"><p>یقین میدان که عمرش پر مزید است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه بیند تلخی جان کندن آن کس</p></div>
<div class="m2"><p>که لعل جانفزایت را گزید است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرنجم از تو گر تابی ز من روی</p></div>
<div class="m2"><p>که از خورشید دایم این سزید است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخواهم دید من روی صبا را</p></div>
<div class="m2"><p>ازین غیرت که در کویت وزید است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصالت را در عالم نیست آمد</p></div>
<div class="m2"><p>هنوز اندر مقام من یزید است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بوی حلقه زنجیر مشکین</p></div>
<div class="m2"><p>دل دیوانه در زلفت خزید است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال خسته را ای دوست دریاب</p></div>
<div class="m2"><p>که از جان در غم عشقت مزید است</p></div></div>