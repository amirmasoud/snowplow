---
title: >-
    شمارهٔ ۳۴۸
---
# شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>بوی خوشت چو همدم باد سحر شود</p></div>
<div class="m2"><p>حال دلم ز زلف تو آشفته تر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا عقل خرده دان نبرد پی به نیستی</p></div>
<div class="m2"><p>مشکل که از دهان تو هیچش خبر شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیرینی لب تو چه گویم که وصف آن</p></div>
<div class="m2"><p>گر بر زبان خامه رود نی شکر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عکس جمال در قدح می نکن که گل</p></div>
<div class="m2"><p>خوبست و چون در آب فته خوبتر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آستانت سجده شکر آرم ار مرا</p></div>
<div class="m2"><p>روزی از آن مقام مجال گذر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبعم چنان به نکهت زلف تو شد لطیف</p></div>
<div class="m2"><p>کر باد مشک بوی مرا درد سر شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از زلف او سخن به درازی کند کمال</p></div>
<div class="m2"><p>ز صف دهانش کن که سخن مختصر شود</p></div></div>