---
title: >-
    شمارهٔ ۸۳۵
---
# شمارهٔ ۸۳۵

<div class="b" id="bn1"><div class="m1"><p>چو زلف یار ز خود لازم است ببریدن</p></div>
<div class="m2"><p>گر اختیار کنی خاک پاش بوسیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلا چو در حرم عشق میروی خود را</p></div>
<div class="m2"><p>چو شمع جمع ادب نیست در میان دیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خاک بوسی پایت هنوز دارم چشم</p></div>
<div class="m2"><p>در آن زمان که بگیرم به خاک پوسیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نه داعیة شبروست زلف ترا</p></div>
<div class="m2"><p>چه موجیست بدامن چراغ پوشیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکشت پیچش آن زلف تا بدار مرا</p></div>
<div class="m2"><p>چنان که دام کشد مرغ را ز پیچیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همیشه گرد تو خواهیم چون کمر گردید</p></div>
<div class="m2"><p>که گرد موی میانان خوش است گردیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال وصف میانش اگر کنی تحریر</p></div>
<div class="m2"><p>قلم بباید باریکتر تراشیدن</p></div></div>