---
title: >-
    شمارهٔ ۱۰۰۶
---
# شمارهٔ ۱۰۰۶

<div class="b" id="bn1"><div class="m1"><p>چرا به تحفه دردم همیشه ننوازی</p></div>
<div class="m2"><p>به ناز و شیوه نسوزی مرا و نگدازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خس توایم همه کار خس چه باشد سوز</p></div>
<div class="m2"><p>تو آتشی و توانی که کار ما سازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دست نیغ تو کار جراحت دل ریش</p></div>
<div class="m2"><p>مام نا شده خواهم که از سر آغازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زیر پا شکند هرچه افتد این عجیبست</p></div>
<div class="m2"><p>که بشکند دلم از زیر پا نیندازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبرد دست به زلفت صبا به بازی نیز</p></div>
<div class="m2"><p>حریف زیر برست و نمی‌خورد بازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه سرور بستان صنوبر آمد و سرو</p></div>
<div class="m2"><p>ترا رسد بسر سروران سرافرازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال باز گزیدی هوای قامت بار</p></div>
<div class="m2"><p>بدت میاد که مرغ بلند پروازی</p></div></div>