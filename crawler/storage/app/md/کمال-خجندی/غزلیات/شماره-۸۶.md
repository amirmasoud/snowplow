---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>ای ز نوش شکرستان لبت رسته نبات</p></div>
<div class="m2"><p>تشنه پستة شکر شکنت آب حیات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو هرچند که دارد به چمن زیبائی</p></div>
<div class="m2"><p>راستی نیستش این قامت شیرین حرکات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوردهام شربت هجرت به تمنای وصال</p></div>
<div class="m2"><p>داده ام عمر گرانمایه به امید وفات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ دل باز چنان صید سر زلف تو شد</p></div>
<div class="m2"><p>کش ازین دام نباشد دگر امید نجات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه بیند رخ زیبای تو خواند تکبیر</p></div>
<div class="m2"><p>هرکه بیند قد و بالای تو گوید صلوات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جفای تو اگر کشته شوم سهل مگیر</p></div>
<div class="m2"><p>کشته تیغ تو باشند رفع الدرجات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخ نو بدر منیرست عیان از شب تا</p></div>
<div class="m2"><p>لعل نو چشمه خضرست نهان در ظلمات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ر نیست در دیده من نقش دهانت چه خیال</p></div>
<div class="m2"><p>هیچ کس دید بهم چشمه حیوان و فرات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز بر بیدق دل اسب غمت پیل انداخت</p></div>
<div class="m2"><p>هم خوش است ارنظری هست به آنرخ سوی مات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نتواند که کند وصف جمال تو کمال</p></div>
<div class="m2"><p>زانکه هست آئینه حسن تو بیرون ز صفات</p></div></div>