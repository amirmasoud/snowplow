---
title: >-
    شمارهٔ ۹۷۰
---
# شمارهٔ ۹۷۰

<div class="b" id="bn1"><div class="m1"><p>ای درد درون جان چه باشی</p></div>
<div class="m2"><p>ای سوز درون نهان چه باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خون دل از زمین چه جویی</p></div>
<div class="m2"><p>ای ناله بر آسمان چه باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای اشک روان برون شو از چشم</p></div>
<div class="m2"><p>در خانه مردمان چه باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بی تو تنم ننی ز جان دور</p></div>
<div class="m2"><p>دور از من ناتوان چه باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ساکن کوی ماهرویان</p></div>
<div class="m2"><p>در منزل نا امان چه باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای آنکه ز پیش راندیم دی</p></div>
<div class="m2"><p>امروز دگر بر آن چه باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای شوخ کمال سوخت بیتو</p></div>
<div class="m2"><p>زین سوخته بر کران چه باشی</p></div></div>