---
title: >-
    شمارهٔ ۸۷۷
---
# شمارهٔ ۸۷۷

<div class="b" id="bn1"><div class="m1"><p>مرا که خرقة ارزق به باده شد گلگون</p></div>
<div class="m2"><p>هوای شاهد و می کی رود ز سر بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر قدح که بیاید تبسم لب یار</p></div>
<div class="m2"><p>حباب وار از او عقل را کشم بیرون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنه رواق فلک برتر است خانه عشق</p></div>
<div class="m2"><p>گمان مبر که کس آنجا رسد به همت دون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمال عشق همین باشد و نهایت فکر</p></div>
<div class="m2"><p>کاری که جز تصور لیلی نمیکند مجنون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجز وصال دعایش ز دست برناید</p></div>
<div class="m2"><p>مراد آن به اجابت نمی شود مقرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه سود از آنکه بپوشم بدامن آتش دل</p></div>
<div class="m2"><p>که میکند رخ شمعی میان سوز درون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جور دوست رضا ده کمال و هیچ مگوی</p></div>
<div class="m2"><p>که در طریق محبت چرا نگنجد و چون</p></div></div>