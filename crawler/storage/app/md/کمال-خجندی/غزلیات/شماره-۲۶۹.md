---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>نیست مرا دوستر از دوست دوست</p></div>
<div class="m2"><p>اوست مرا دوست مرا دوست اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم ز رخ دوست زند آینه</p></div>
<div class="m2"><p>در نظر مردم از آن دوست روست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل خم ابروی تو دارد هوس</p></div>
<div class="m2"><p>صدر نشین بین که چه محراب جوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوی چه ماند به زنخدان یار</p></div>
<div class="m2"><p>این زنخ مردم بیهوده گوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه ز هر رنگ می از خم مرا</p></div>
<div class="m2"><p>باده یک رنگ ببارد سبوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نافه چین را که نسیم تو داشت</p></div>
<div class="m2"><p>در طلب از شوق تو بدرید پوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو لب جوست قدت ز آن مرا</p></div>
<div class="m2"><p>دیده لب جوی و لب جوست دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چیست ز غم حال تو گفتی کمال</p></div>
<div class="m2"><p>تا رخ زیبای تو دیدم نکوست</p></div></div>