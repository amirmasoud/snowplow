---
title: >-
    شمارهٔ ۵۱۶
---
# شمارهٔ ۵۱۶

<div class="b" id="bn1"><div class="m1"><p>من به درد دل خوشم جان مرا صحت چه سود</p></div>
<div class="m2"><p>نوش آنلب در خورست این تشنه را شربت چه سود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آروزمند قد و قند ب ر روی ترا</p></div>
<div class="m2"><p>سایه طوبی و آب کوثر و جنت چه سود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناز نو سازد مرا به نعمت و ناز جان قسمتی</p></div>
<div class="m2"><p>گر نباشد ناز تو از ناز و از نعمت چه سود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کنم درد و بلا را بر دل و جان قسمتی</p></div>
<div class="m2"><p>چون مرا این بود از خوان غمت قسمت چه سود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه مصروفست همت در طلوع صبح مهر</p></div>
<div class="m2"><p>اختری چون نیست در طالع مرا همت چه سود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته فرمایش زد گر بدت گوید؟ رقیب</p></div>
<div class="m2"><p>نیک می فرمایی اما کشتی را لت چه سود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نداری در خور مخدومیش وجهی کمال</p></div>
<div class="m2"><p>روی گرد آلود سودن بر در خدمت چه سود</p></div></div>