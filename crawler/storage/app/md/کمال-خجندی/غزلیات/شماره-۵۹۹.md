---
title: >-
    شمارهٔ ۵۹۹
---
# شمارهٔ ۵۹۹

<div class="b" id="bn1"><div class="m1"><p>چو عشق آمد ای عقل خیز و گریز</p></div>
<div class="m2"><p>که خاشاک نکند به آتش ستیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر دیده خواهد که یابد دلم</p></div>
<div class="m2"><p>بگو خاک پایش به مژگان ببیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر خود به نیغ تو خواهم فروخت</p></div>
<div class="m2"><p>که به زین ندارم خریدار نیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دید آهر از دور آن چشم مست</p></div>
<div class="m2"><p>روان از همانجا بگشت و گریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دل ریخت آن غمزه خون کمال</p></div>
<div class="m2"><p>اگر می نهم جرم خونم بریز</p></div></div>