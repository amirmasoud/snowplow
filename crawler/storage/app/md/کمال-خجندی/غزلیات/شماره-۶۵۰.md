---
title: >-
    شمارهٔ ۶۵۰
---
# شمارهٔ ۶۵۰

<div class="b" id="bn1"><div class="m1"><p>به نینی که بر آن در بریم سجده خاص</p></div>
<div class="m2"><p>همیشه فاتحه خوانیم از سر اخلاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلا چو طالب وصلی ز آب دیده منال</p></div>
<div class="m2"><p>که در به چنگ نیاره چو دم زند غواص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مراد هر دو جهان یافتم به دولت دوست</p></div>
<div class="m2"><p>دمی که یافتم از محنت رقیب خلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اب تو گشت مرا ساقیا برسم فدا</p></div>
<div class="m2"><p>بریز خون صراحی که والجروح نصاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زاهدان نرسد بوی نو به رندان نیز</p></div>
<div class="m2"><p>عوام را چه رسد چون نمی رسد به خواص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیث سیمیذاران کران گران شنوند</p></div>
<div class="m2"><p>گرفته اند مگر گوش پارسا به رصاص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال شهر گرفتی به فضاهای غریب</p></div>
<div class="m2"><p>که عام گیر بود در سخن معانی خاص</p></div></div>