---
title: >-
    شمارهٔ ۶۰۲
---
# شمارهٔ ۶۰۲

<div class="b" id="bn1"><div class="m1"><p>زاهد شهرم و صاحب نظر و شاهد باز</p></div>
<div class="m2"><p>شسته سجاده به می کرده به میخانه نماز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاعت باده پرستان چه به مسجد چه بدیر</p></div>
<div class="m2"><p>عشق ورزیدن رندان په حقیقت چه مجاز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگران معتقد زهد و نمازند هنوز</p></div>
<div class="m2"><p>ما به مقصود رسیدیم برندی و نیاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آستان در تو منزل شهبازان است</p></div>
<div class="m2"><p>مگسان را چه محل بر سر کویت پرواز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنده طالع شوریده خویشم که مرا</p></div>
<div class="m2"><p>در سر زلف پریشانش بشد عمر دراز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برسان هر سئم و جور که خواهی به کمال</p></div>
<div class="m2"><p>خوش بود بر دل محمود ستم های ایاز</p></div></div>