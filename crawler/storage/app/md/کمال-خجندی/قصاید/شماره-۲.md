---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای بر کمال قدرت تو عقل کل گواه</p></div>
<div class="m2"><p>بر بر لوح کبریایی تو توقیع لا اله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشفتگان خاک رهت رهروان دین</p></div>
<div class="m2"><p>دردی‌کشان جان غمت سالکان راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شبنم عطای تو یک قطره بحر و کان</p></div>
<div class="m2"><p>وز پرتوی جمال تو یک ذره مهر و ماه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ امید از کف جود تو دانه‌جوی</p></div>
<div class="m2"><p>دست نیاز بر دل در عدل تو دادخواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نام تو صیقلی ست کز آئینه وجود</p></div>
<div class="m2"><p>بیرون برد به نور خرد زنگ اشتباه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلطان عزت تو به فرمان کن فکان</p></div>
<div class="m2"><p>گرد از ره وجود برآورد بی سپاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آثار صنع توست که بر طاق نیلگون</p></div>
<div class="m2"><p>صبح سفید روی نمود از شب سیاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انوار حسن توست که از جیب آسمان</p></div>
<div class="m2"><p>خورشید سر کشید چو یوسف قمر ز چاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنجا که آب لطف تو صد نیش گشته نوش</p></div>
<div class="m2"><p>وانجا به باد قهر تو صد کوه گشته کاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گاه از جان تو برد به صفا پیر دردنوش</p></div>
<div class="m2"><p>گاه از تو خون خورد به جفا طفل بی‌گناه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنهد نسیم لطف تو در ناف لاله مشک</p></div>
<div class="m2"><p>بندد سموم قهر تو بر شاخ گل گیاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>موسی کلیم بارگه توست و پاسبان</p></div>
<div class="m2"><p>فرعون رانده نظر توست و پادشاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طاعت چه سود زاهد پرهیزکار را</p></div>
<div class="m2"><p>گر بر در قبول تواش نیست آب و جاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای آنکه سالکان در کبریات را</p></div>
<div class="m2"><p>نبود به جز سرداق احسان تو پناه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخشای بر کمال که نقصان‌پذیر نیست</p></div>
<div class="m2"><p>گر بر خورند از تو محبان بارگاه</p></div></div>