---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای مه رخسار تو مطلع صبح یقین</p></div>
<div class="m2"><p>غاشیه کبریات شهپر روح الامین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آینه دار رخت عارض ماه تمام</p></div>
<div class="m2"><p>تکیه گه منبرت پایه چرخ برین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایه قد تو دید در چمن دلبری</p></div>
<div class="m2"><p>کز سر خجلت بماند سرو سهی بر زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گل رخسار تست لاله سیراب را</p></div>
<div class="m2"><p>قطره آبی که هست بر جگر آتشین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خط جبین تو بود آنکه شدست آشکار</p></div>
<div class="m2"><p>بر ورق کاینات نقش رسول الامین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آدم خاکی که بود پیش رو انبیا</p></div>
<div class="m2"><p>داغ قبول تو داشت بر سر لوح جبین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شحنه حکم تو را تیر قضا در کمان</p></div>
<div class="m2"><p>بازوی امر تو را تیغ ظفر در کمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر رکاب تواند شاهسواران ملک</p></div>
<div class="m2"><p>غاشیه داران تو کارگزاران دین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاتم اقبال تست آنکه به مهر قبول</p></div>
<div class="m2"><p>خشک و تر کاینات داشت بزیر نگین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی تو کجا پی برد در حرم کبریا</p></div>
<div class="m2"><p>صوفی پرهیزگار زاهد خلوت نشین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاک کف پای تست دامن آخر زمان</p></div>
<div class="m2"><p>دست تو زان برفشاند بر دو جهان آستین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مدعیان نشنوند نعت کمال ترا</p></div>
<div class="m2"><p>لایق هر گوش نیست دانه دُر ثمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سبحه کروبیان ورد ثنای تو باد</p></div>
<div class="m2"><p>تا که صبح نشور بر تو کنند آفرین</p></div></div>