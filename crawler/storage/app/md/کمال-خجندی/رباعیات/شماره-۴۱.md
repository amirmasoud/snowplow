---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>گفتم جانا گفت بگو گر مردی</p></div>
<div class="m2"><p>گفتم مردم گفت که نیکو کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم چشمم گفت بس این بی آبی</p></div>
<div class="m2"><p>گفتم نفسم گفت مکن دم سردی</p></div></div>