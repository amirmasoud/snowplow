---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>شمعی که به رخسار نکو بودی گرم</p></div>
<div class="m2"><p>دید آن رخ و چون موم شدش آن دل نرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش قد و چشم و خدمتش در بستان</p></div>
<div class="m2"><p>نرگس ز حیا برآید و یرو از شرم</p></div></div>