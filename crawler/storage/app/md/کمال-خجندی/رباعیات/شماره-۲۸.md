---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>تا فکرت من نهاد بنیان سخن</p></div>
<div class="m2"><p>آباد شد از من طرب آباد سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میخواست سخن ز دست بی طبعان داد</p></div>
<div class="m2"><p>دادم باشارت خرد داد سخن</p></div></div>