---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>گفتم که چه ریزد ز لبت گفت قند</p></div>
<div class="m2"><p>گفتم که چه خیزدت ز مو گفت کمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که بفرما سخنی گفت خموش</p></div>
<div class="m2"><p>گفتم بشکر خنده درا گفت مخند</p></div></div>