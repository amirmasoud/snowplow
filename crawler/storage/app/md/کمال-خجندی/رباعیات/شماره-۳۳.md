---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>در ملک وجودم بجز از دوست مجو</p></div>
<div class="m2"><p>در خانه دل نیست کسی غیر از او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانند کبوتر این دل شیدایم</p></div>
<div class="m2"><p>پر میزند و مدام گوید یا هو</p></div></div>