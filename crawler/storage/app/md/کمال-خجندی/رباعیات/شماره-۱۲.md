---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>امروز چو شعر هر که در خط کوشد</p></div>
<div class="m2"><p>خطی ز خطت بصد غزل نفروشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پوشید خط خوب تو عیب سخنت</p></div>
<div class="m2"><p>همچون خط خوبان که زنخ را پوشد</p></div></div>