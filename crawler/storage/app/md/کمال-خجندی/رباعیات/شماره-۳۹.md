---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>با قامتت ای لاله رخ سوسن بوی</p></div>
<div class="m2"><p>از جای رود چو آب سرو لب جوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش رخ تو زسیلی باد صبا</p></div>
<div class="m2"><p>گل هم بطبانچه سرخ میدارد روی</p></div></div>