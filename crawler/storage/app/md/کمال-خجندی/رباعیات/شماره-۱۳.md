---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>گر گل نه بخدمتت ز جا برخیزد</p></div>
<div class="m2"><p>بهر زدنش باد صبا برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش قد تو سرو سهی را در باغ</p></div>
<div class="m2"><p>چندانکه نشانند ز پا برخیزد</p></div></div>