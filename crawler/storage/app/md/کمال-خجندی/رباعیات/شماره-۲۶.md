---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>کی باشد ازین تنگ برون آمدنم</p></div>
<div class="m2"><p>نامست ازین ننگ برون آمدنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی مگر از سنگ برون می آید</p></div>
<div class="m2"><p>پروانه از سنگ برون آمدنم</p></div></div>