---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>گفتم چه کند دفع غمم گفت که می</p></div>
<div class="m2"><p>گفتم چه زند راه دلم گفت که نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که تو داری دل من گفت که کو</p></div>
<div class="m2"><p>گفتم ز غمت جان بدهم گفت که کی</p></div></div>