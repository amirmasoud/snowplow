---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>بر گوش رسد همی نوا خوانی دل</p></div>
<div class="m2"><p>جان بی خبر است از غم پنهانی دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سوز درون هیچ نگویم لیکن</p></div>
<div class="m2"><p>از چهره عیانست پریشانی دل</p></div></div>