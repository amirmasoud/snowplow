---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>گفتم چه زنم در غم تو گفت که آه</p></div>
<div class="m2"><p>گفتم چه کنم در پی تو گفت نگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که کجا روم ز دست غم تو</p></div>
<div class="m2"><p>گفتا که بتون و تنجه و آب سیاه</p></div></div>