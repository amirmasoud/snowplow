---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>کس خوبتر از تودر جهان ممکن نیست</p></div>
<div class="m2"><p>بس خوبتر از تو در جهان ممکن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خوبی ماه پیکران بد مهریست</p></div>
<div class="m2"><p>پس خوبتر از تو در جهان ممکن نیست</p></div></div>