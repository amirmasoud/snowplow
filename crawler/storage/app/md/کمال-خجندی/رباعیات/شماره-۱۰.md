---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>انسان بمثل آینه باشد بالذات</p></div>
<div class="m2"><p>همواره بود مظهر حق این مرات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیبد که بشر فخر و مباهات کند</p></div>
<div class="m2"><p>زین موهبت عظیم بر موجودات</p></div></div>