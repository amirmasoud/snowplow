---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>ای آنکه تویی سوار در هر هنری</p></div>
<div class="m2"><p>از وعده اسب دادیم دی خبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی همتی است اسب تنهها بتو داد</p></div>
<div class="m2"><p>خواهیم روانه کرد اسبی و خری</p></div></div>