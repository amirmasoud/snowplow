---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>ما ز تشریف میر عبدالله</p></div>
<div class="m2"><p>نیک اسوده و قوی شادیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست ما را ز صحبتش گله ای</p></div>
<div class="m2"><p>لیکن از گوش او بفریادیم</p></div></div>