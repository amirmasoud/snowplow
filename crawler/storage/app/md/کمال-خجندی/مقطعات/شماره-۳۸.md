---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>مرا هست اکثر غزل هفت بیت</p></div>
<div class="m2"><p>چو گفتار سلمان نرفته زیاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که حافظ همی خواندش در عراق</p></div>
<div class="m2"><p>بلند و روان همچو سبع شداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بنیاد هر هفت چون آسمان</p></div>
<div class="m2"><p>کزین جنس بیتی ندارد عماد</p></div></div>