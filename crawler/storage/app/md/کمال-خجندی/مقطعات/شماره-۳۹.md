---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>مرا یار از شکارستان مشگین</p></div>
<div class="m2"><p>در آهو بره مشگین فرستاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو افتادند دور از لاله و گل</p></div>
<div class="m2"><p>بصحرای عدمشان رخت افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر آهو برگان را کرد اجل صید</p></div>
<div class="m2"><p>بقای آهوان چشم او باد</p></div></div>