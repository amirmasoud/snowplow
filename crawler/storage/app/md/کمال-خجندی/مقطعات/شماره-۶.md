---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>حافظ بربط نواز چنگ ساز</p></div>
<div class="m2"><p>بامنت از بی نوایی جنگ چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از برای سوختن از زیر دیگ</p></div>
<div class="m2"><p>گفته هیزم ندارم چنگ چیست</p></div></div>