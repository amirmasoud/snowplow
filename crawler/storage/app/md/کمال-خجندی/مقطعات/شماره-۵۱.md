---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>کردم از سید راگوی سوالی که ترا</p></div>
<div class="m2"><p>هست جز رای و جز اندیشه سودای دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت صد رای دگر با تو بگویم لیکن</p></div>
<div class="m2"><p>که من از دست تو فردا بروم جای دگر</p></div></div>