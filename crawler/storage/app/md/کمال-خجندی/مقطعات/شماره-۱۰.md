---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>دهقان فضل عالم بردان هلال دین</p></div>
<div class="m2"><p>آنی که جنه است چو خمی پرزگندم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پردانی تو ساخت تنت را چوخم بزرگ</p></div>
<div class="m2"><p>تن پروران برند گمان کز تنعم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تو فقیه خشکی و مسکر نمیخوری</p></div>
<div class="m2"><p>دستار تو همیشه چرا بر سر خم است</p></div></div>