---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>گفتم از مصر معانی بفرستم بتو باز</p></div>
<div class="m2"><p>سخن چند که آید به دهانت چو شکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز ترسیدم ازین نکته که گویی چو همام</p></div>
<div class="m2"><p>شکر از مصر به تبریز میارید دگر</p></div></div>