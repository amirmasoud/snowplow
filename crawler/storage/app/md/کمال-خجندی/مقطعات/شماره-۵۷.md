---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>پهلوان فقاعی ار ناگاه</p></div>
<div class="m2"><p>زیر یخ رفت و داد جان و نفیس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پسرش را بگو بگور پدرش</p></div>
<div class="m2"><p>برد الله مضجعه بنویس</p></div></div>