---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>براه گرم بغداد این سلمان</p></div>
<div class="m2"><p>در آن حالت که از جان می بریدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبودش گوییا شعر پدر یاد</p></div>
<div class="m2"><p>که آنرا خواندی و بر خود دمیدی</p></div></div>