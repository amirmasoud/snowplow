---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>گر گوشه بسازد سلطان حسین ما را</p></div>
<div class="m2"><p>در قلب شهر نبود کس را بما نزاعی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با مطربان خوشگو شام و صباح باشد</p></div>
<div class="m2"><p>در گوشه حسینی عشاق را سماعی</p></div></div>