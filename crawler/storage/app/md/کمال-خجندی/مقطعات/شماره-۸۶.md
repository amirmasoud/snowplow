---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>گله کردی که زرنجور نکردی پرسش</p></div>
<div class="m2"><p>تو ببرس از من بیدل که بروزان و شبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مونسی نیست مرا در بر و مشهور است این</p></div>
<div class="m2"><p>دلبری هست ترا در برو معروفست آن</p></div></div>