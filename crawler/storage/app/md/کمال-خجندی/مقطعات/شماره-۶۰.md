---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>جستم از یاری نشان آن پسر</p></div>
<div class="m2"><p>کاب حیوانست جویای لبش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت بیگناهان بجیحونش طلب</p></div>
<div class="m2"><p>کان زمان باشد خلاص از مکتبش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر نماز دیگری آیم برون</p></div>
<div class="m2"><p>جانب جیحون و جویم لب لبش</p></div></div>