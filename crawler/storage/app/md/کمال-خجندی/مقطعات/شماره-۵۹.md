---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>بهر بریان امیر زاده ما</p></div>
<div class="m2"><p>گوسفندی خرید و فربه و خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زود با ورجیان مطبخ وی</p></div>
<div class="m2"><p>گو سپند افکنند در آتش</p></div></div>