---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>به سمع شیخ محمد ایا صبا برسان</p></div>
<div class="m2"><p>که باد پیرهن صبر ما ز دست تو چاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین جهان که بود رنج و راحتش گذران</p></div>
<div class="m2"><p>نه دوستی است که باشی تو شاد و ما غمناک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که او پی دنیا ز دست داد دلی</p></div>
<div class="m2"><p>فروخت دامن دنیا به کم‌ترین خاشاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذشت مدت شش ماه و قرب سالی شد</p></div>
<div class="m2"><p>که تحفه‌ای فرستادم از عقیده پاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدان امید که تشریف بنده بفرستی</p></div>
<div class="m2"><p>ز بندگان خود و از کسی نداری باک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شنیده‌ام که هنوزت نیامدست به دست</p></div>
<div class="m2"><p>غلامکی که سبک‌روح باشد و چالاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا غلام به ایام زندگی باید</p></div>
<div class="m2"><p>نه آنکه بعد وفاتم بود مجاور خاک</p></div></div>