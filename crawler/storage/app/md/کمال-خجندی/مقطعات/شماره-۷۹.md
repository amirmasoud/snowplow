---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>کمال اشعار اقرانت ز اعجاز</p></div>
<div class="m2"><p>گرفتم سر بسر وحی است و الهام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو خالی از خیال خاص باشد</p></div>
<div class="m2"><p>خیالست اینکه گیرد شهرت عام</p></div></div>