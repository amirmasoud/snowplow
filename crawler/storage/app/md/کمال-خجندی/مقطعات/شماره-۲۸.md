---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>به مجمعی که دف از قول خویش میزند لاف</p></div>
<div class="m2"><p>جواب دادنی اش در نفس به بانگ بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که در برابر من ای فراغ چنبر پوش</p></div>
<div class="m2"><p>ز لطف لاف زنی تن زن و به طلبه مخند</p></div></div>