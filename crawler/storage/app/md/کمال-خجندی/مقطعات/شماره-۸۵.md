---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>پیش چنگ دلخراشت صوفیانرا حافظا</p></div>
<div class="m2"><p>نعره ها باید به وقت نقش بنمودن زدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اردشیری تو اگر در مجلسی آرند چنگ</p></div>
<div class="m2"><p>مردم مجلس ترا خواهند فرمودن زدن</p></div></div>