---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>معایبی که در اشعار خواجه عصار است</p></div>
<div class="m2"><p>نوشته آن همگی در درون دیوانهاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جداولی که بسرخی کشید در دیوان</p></div>
<div class="m2"><p>نه جدول است به معنی که خون دیوانهاست</p></div></div>