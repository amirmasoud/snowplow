---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>دعای من این است در هر نمازی</p></div>
<div class="m2"><p>به خلوت که یا ملجایی یا ملاذی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگه دار اصحاب ذوق و طرب را</p></div>
<div class="m2"><p>ز چنگ ملاطی و شعر معاذی</p></div></div>