---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>شیر مردانه بگفتم پندیت</p></div>
<div class="m2"><p>روبهی باشی اگر بپذیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برکس آن به که نگیری آهو</p></div>
<div class="m2"><p>که سگی باشی ار آهو بگیری</p></div></div>