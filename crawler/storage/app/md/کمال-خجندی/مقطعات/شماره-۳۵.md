---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>خواستم از صاحب مطبخ حساب</p></div>
<div class="m2"><p>بره کان کشت و سه پایه را برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت بر رسم فداکان سود تست</p></div>
<div class="m2"><p>حشو آن همسایه بی مایه برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیه و کرده حاجی سقا گرفت</p></div>
<div class="m2"><p>شیردانرا گنده پیر دایه برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش دلرا کجا بردی که نیست</p></div>
<div class="m2"><p>گفت دلرا دختر همسایه برد</p></div></div>