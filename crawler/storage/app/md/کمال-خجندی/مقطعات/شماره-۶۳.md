---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>دوش گفتم خلیل اچکو را</p></div>
<div class="m2"><p>تا کی این لهو و چند عیش و نشاط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت شیخا برو تو خود را باش</p></div>
<div class="m2"><p>کل شاه برجلها استنباط</p></div></div>