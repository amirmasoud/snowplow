---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>حافظ نیکخوان نیک نویس</p></div>
<div class="m2"><p>هرکه حق گویدت شنو سخنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنگ را بیش بر کنار مزن</p></div>
<div class="m2"><p>بسر خود که بر زمین نزنش</p></div></div>