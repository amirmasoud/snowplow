---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>کیسه مکن پر زر و سیم ای پسر</p></div>
<div class="m2"><p>کیسه برانند درین رهگذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیسه تهی باش و بیاسا کمال</p></div>
<div class="m2"><p>هر که تهی کیسه تر آسوده تر</p></div></div>