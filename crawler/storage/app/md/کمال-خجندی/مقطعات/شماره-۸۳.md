---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>با اکبر مسگر به دکان گفتم من</p></div>
<div class="m2"><p>زانگشت سیه شد به تنت پیراهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا که ز عشق روی احمد سوخت</p></div>
<div class="m2"><p>بر آتش دل پیرهن پاره من</p></div></div>