---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>آواز حزین سوزنی را</p></div>
<div class="m2"><p>مشنو که کنند عیب بسیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خشک است همین و نیز باریک</p></div>
<div class="m2"><p>چون سوزن خار های دیوار</p></div></div>