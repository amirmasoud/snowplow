---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>به سمع معجری ای پیک عاشقان برسان</p></div>
<div class="m2"><p>حدیث شوق ملاقات و آرزمندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بعد آنکه زدی حلقه بر در و خود را</p></div>
<div class="m2"><p>در آن جناب همایون چو حلقه افکندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگویش این قدر از من که ای به رتبت و فضل</p></div>
<div class="m2"><p>گذشته قدر تو از پایه هنرمندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه گل شکفت ازاینت که بر سبیل خلاف</p></div>
<div class="m2"><p>درخت مهر و محبت ز بیخ برکندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر از تنیده یاری گسسته شد تاری</p></div>
<div class="m2"><p>چه باشد ار به سرانگشت عفو پیوندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا خود از تو چه نفع و تو را ز من چه ضرر</p></div>
<div class="m2"><p>که من تو را بپسندم مرا تو نپسندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نظم و نثر گرفتم که سعدی وقتیم</p></div>
<div class="m2"><p>نه من ز خاک خجندم تو از سمرقندی</p></div></div>