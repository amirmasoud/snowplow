---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>گفت صاحبدلی به من که چراست</p></div>
<div class="m2"><p>که تورا شعر هست و دیوان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم از بهر آنکه چون دگران</p></div>
<div class="m2"><p>سخن من پر و فراوان نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت هر چند گفته تو کم است</p></div>
<div class="m2"><p>کمتر از گفته های ایشان نیست</p></div></div>