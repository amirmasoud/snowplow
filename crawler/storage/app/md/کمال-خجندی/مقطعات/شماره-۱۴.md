---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>سوال کرد یکی از علای دین گلکار</p></div>
<div class="m2"><p>که تو غلام نئی روی تو سیاه چراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جواب داد که هر موریی که میسازیم</p></div>
<div class="m2"><p>چو آتشی بکنی دود آن بجانب ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیاه رویی من عارضی است اصلی نیست</p></div>
<div class="m2"><p>سیاه رویی بنده ز دود موریهاست</p></div></div>