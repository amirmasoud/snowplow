---
title: >-
    رباعی شمارهٔ ۱۰۹
---
# رباعی شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>در مسلخ عشق جز نکو را نکشند</p></div>
<div class="m2"><p>روبه صفتان زشت خو را نکشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عاشق صادقی ز کشتن مگریز</p></div>
<div class="m2"><p>مردار بود هر آنکه او را نکشند</p></div></div>