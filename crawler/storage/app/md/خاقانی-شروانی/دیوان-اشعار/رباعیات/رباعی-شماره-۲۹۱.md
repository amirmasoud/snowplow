---
title: >-
    رباعی شمارهٔ ۲۹۱
---
# رباعی شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>هر روز بود تو را جفایی نو نو</p></div>
<div class="m2"><p>تا جامهٔ صبر من بدرد جو جو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ذره ز نیکیت ندیدم همه عمر</p></div>
<div class="m2"><p>بیرحم کسی تو آزمودم، رو رو</p></div></div>