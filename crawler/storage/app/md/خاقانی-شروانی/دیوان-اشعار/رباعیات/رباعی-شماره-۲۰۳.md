---
title: >-
    رباعی شمارهٔ ۲۰۳
---
# رباعی شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>زرین چکنم قدح گلین آر ای دل</p></div>
<div class="m2"><p>پای از گل غم مرا برون آر ای دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا از گل گورم ندمد خار ای دل</p></div>
<div class="m2"><p>گلگون می در گلین قدح دار ای دل</p></div></div>