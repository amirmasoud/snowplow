---
title: >-
    رباعی شمارهٔ ۱۰۶
---
# رباعی شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>تا در لب تو شهد سخنور باشد</p></div>
<div class="m2"><p>نشگفت اگر شهد تب آور باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاید که تب تو حسن پرور باشد</p></div>
<div class="m2"><p>خورشید به تب لرزه نکوتر باشد</p></div></div>