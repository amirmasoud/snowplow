---
title: >-
    رباعی شمارهٔ ۱۹۰
---
# رباعی شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>خاقانی اگر نه خس نهادی خوش باش</p></div>
<div class="m2"><p>گام از سر کام در نهادی خوش باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند به ناخوشی فتادی خوش باش</p></div>
<div class="m2"><p>پندار در این دور نزادی خوش باش</p></div></div>