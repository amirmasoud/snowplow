---
title: >-
    رباعی شمارهٔ ۵۳
---
# رباعی شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>ملاح که بهر ماه من مهد آراست</p></div>
<div class="m2"><p>گفتی کشتی مرا چو کشتی شد راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان خبرم بود که او کشتی خواست</p></div>
<div class="m2"><p>در آب نشست و آتش از من برخاست</p></div></div>