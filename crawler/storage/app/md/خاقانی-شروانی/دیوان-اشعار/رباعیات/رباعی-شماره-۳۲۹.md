---
title: >-
    رباعی شمارهٔ ۳۲۹
---
# رباعی شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>گر یک دو نفس بدزدم اندر ماهی</p></div>
<div class="m2"><p>تا داد دلی بخواهم از دل‌خواهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بینی فلک انگیخته لشکرگاهی</p></div>
<div class="m2"><p>از غم رصدی نشانده بر هر راهی</p></div></div>