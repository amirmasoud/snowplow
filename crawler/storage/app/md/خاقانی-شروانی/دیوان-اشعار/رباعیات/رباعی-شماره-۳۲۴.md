---
title: >-
    رباعی شمارهٔ ۳۲۴
---
# رباعی شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>ترسا صنمی کز پی هر غم‌خواری</p></div>
<div class="m2"><p>بر هر در دیری زده دارد داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آن زلف صلیب شکل دادی باری</p></div>
<div class="m2"><p>یک موی کزو ببستمی زناری</p></div></div>