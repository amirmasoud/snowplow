---
title: >-
    رباعی شمارهٔ ۲۶۳
---
# رباعی شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>در کوی تو خاطری ندیدم محزون</p></div>
<div class="m2"><p>زاهد از عقل شاد و عاشق ز جنون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی سر گرم باده، مطرب خواهند</p></div>
<div class="m2"><p>کل حزب بما لدیهم فرحون</p></div></div>