---
title: >-
    رباعی شمارهٔ ۸۲
---
# رباعی شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>خاقانی اسیر یار زرگر نسب است</p></div>
<div class="m2"><p>دل کوره و تن شوشهٔ زرین سلب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کورهٔ آتش چه عجب شفشهٔ زر</p></div>
<div class="m2"><p>در شفشهٔ زر کورهٔ آتش عجب است</p></div></div>