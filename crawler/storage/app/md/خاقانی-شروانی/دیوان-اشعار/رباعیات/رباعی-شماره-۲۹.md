---
title: >-
    رباعی شمارهٔ ۲۹
---
# رباعی شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>خاقانی از آن ریزش همت که توراست</p></div>
<div class="m2"><p>جستن ز فلک ریزهٔ روزی نه رواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهروزی و روزی ز فلک نتوان خواست</p></div>
<div class="m2"><p>کان ریزه کشی از در روزی‌ده ماست</p></div></div>