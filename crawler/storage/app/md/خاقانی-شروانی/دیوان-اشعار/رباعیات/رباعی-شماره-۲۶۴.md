---
title: >-
    رباعی شمارهٔ ۲۶۴
---
# رباعی شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>شد باغ ز شمع گل رعنا روشن</p></div>
<div class="m2"><p>وز مشعل لاله گشت صحرا روشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پرتو روی آتشین رخساری</p></div>
<div class="m2"><p>گردید چراغ دیدهٔ ما روشن</p></div></div>