---
title: >-
    رباعی شمارهٔ ۲۴
---
# رباعی شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>آب جگرم به آتش غم برخاست</p></div>
<div class="m2"><p>سوز جگرم فزود تا صبر بکاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند جگر به صبر می‌ماند راست</p></div>
<div class="m2"><p>صبر از جگر سوخته چون شاید خواست</p></div></div>