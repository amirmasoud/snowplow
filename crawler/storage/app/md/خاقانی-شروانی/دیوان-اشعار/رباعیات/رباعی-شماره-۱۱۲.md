---
title: >-
    رباعی شمارهٔ ۱۱۲
---
# رباعی شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>خواهی شرف مردم دانا باشد</p></div>
<div class="m2"><p>عزت مطلب فروتنی تا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با صدر نشینان منشین کز میزان</p></div>
<div class="m2"><p>هر سنگ سبک‌تر است بالا باشد</p></div></div>