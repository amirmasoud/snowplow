---
title: >-
    رباعی شمارهٔ ۱۳۰
---
# رباعی شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>هرکس که ز ارباب عبادت باشد</p></div>
<div class="m2"><p>بر چهرهٔ او نور سعادت باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایام وجود او به او فخر کنند</p></div>
<div class="m2"><p>در خدمت او بخت ارادت باشد</p></div></div>