---
title: >-
    رباعی شمارهٔ ۲
---
# رباعی شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>غم کرد ریاض جان مه و سال مرا</p></div>
<div class="m2"><p>آئینه ندارد دل خوشحال مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صیاد ز بس که دوستم می‌دارد</p></div>
<div class="m2"><p>بسته است در آغوش قفس بال مرا</p></div></div>