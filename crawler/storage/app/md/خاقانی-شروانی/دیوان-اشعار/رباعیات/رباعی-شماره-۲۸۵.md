---
title: >-
    رباعی شمارهٔ ۲۸۵
---
# رباعی شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>کردم به قمار دل دو عالم به گرو</p></div>
<div class="m2"><p>تن نیز به دستخون سپردم به گرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماندم همه و نماند چیزی با من</p></div>
<div class="m2"><p>من ماندم و نیم جان و یکدم به گرو</p></div></div>