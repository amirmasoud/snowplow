---
title: >-
    رباعی شمارهٔ ۱۵۹
---
# رباعی شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>ای از دل دردناک خاقانی شاد</p></div>
<div class="m2"><p>غمهای تو کرد خاک خاقانی باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که کنی هلاک خاقانی یاد</p></div>
<div class="m2"><p>برخی تو جان پاک خاقانی باد</p></div></div>