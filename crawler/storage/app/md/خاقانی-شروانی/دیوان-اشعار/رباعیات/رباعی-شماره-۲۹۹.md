---
title: >-
    رباعی شمارهٔ ۲۹۹
---
# رباعی شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>خواهی که شود دل تو چون آئینه</p></div>
<div class="m2"><p>ده چیز برون کن از میان سینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرص و دغل و بخل و حرام و غیبت</p></div>
<div class="m2"><p>بغض و حسد و کبر و ریا و کینه</p></div></div>