---
title: >-
    رباعی شمارهٔ ۱۶۷
---
# رباعی شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>خاقانی اگر یار نماید رخسار</p></div>
<div class="m2"><p>رخسار چو زر به ناخنان خسته مدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ناخن و زر چهره برناید کار</p></div>
<div class="m2"><p>کز تو همه زر ناخنی خواهد یار</p></div></div>