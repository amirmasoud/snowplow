---
title: >-
    رباعی شمارهٔ ۱۶
---
# رباعی شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>خاقانی را ز بس که بوسید آن لب</p></div>
<div class="m2"><p>دور از لب تو گرفت تبخال از تب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آری لبت آتش است خندان ز طرب</p></div>
<div class="m2"><p>از آتش اگر آبله خیزد چه عجب</p></div></div>