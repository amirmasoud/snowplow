---
title: >-
    رباعی شمارهٔ ۶
---
# رباعی شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>می‌ساخت چو صبح لاله‌گون رنگ هوا</p></div>
<div class="m2"><p>با توبهٔ من داشت نمک جنگ هوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لکهٔ ابرم چو عزائم خوانی</p></div>
<div class="m2"><p>در شیشه پری کرد ز نیرنگ هوا</p></div></div>