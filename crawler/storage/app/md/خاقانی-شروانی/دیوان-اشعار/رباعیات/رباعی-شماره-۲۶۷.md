---
title: >-
    رباعی شمارهٔ ۲۶۷
---
# رباعی شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>خاقانی اگر توئی ز صافی نفسان</p></div>
<div class="m2"><p>بر گردن کس دست به سیلی مرسان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا که چو بر گردن آزاد کسان</p></div>
<div class="m2"><p>شمشیر رسد به که رسد دست خسان</p></div></div>