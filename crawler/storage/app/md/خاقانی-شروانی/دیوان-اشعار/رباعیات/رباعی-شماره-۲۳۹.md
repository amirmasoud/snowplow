---
title: >-
    رباعی شمارهٔ ۲۳۹
---
# رباعی شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>از حلقهٔ زلف تو سر افکنده‌ترم</p></div>
<div class="m2"><p>وز جرعهٔ جام پراکنده‌ترم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه ز شبه دل تو آزادتر است</p></div>
<div class="m2"><p>از لعل نگین تو تو را بنده‌ترم</p></div></div>