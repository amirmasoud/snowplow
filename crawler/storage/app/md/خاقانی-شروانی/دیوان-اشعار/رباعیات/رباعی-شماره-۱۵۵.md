---
title: >-
    رباعی شمارهٔ ۱۵۵
---
# رباعی شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>رخسارهٔ عاشقان مزعفر باید</p></div>
<div class="m2"><p>ساعت ساعت زمان زمان‌تر باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که چو مه نگار در بر باید</p></div>
<div class="m2"><p>دامن دامن، کله کله زر باید</p></div></div>