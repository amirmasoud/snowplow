---
title: >-
    رباعی شمارهٔ ۲۴۰
---
# رباعی شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>چون سایه اگر باز به کنجی تازم</p></div>
<div class="m2"><p>همسایهٔ من سایه نبیند بازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور سایه ز من کم کند آن طنازم</p></div>
<div class="m2"><p>از سایهٔ خود هم نفسی بر سازم</p></div></div>