---
title: >-
    رباعی شمارهٔ ۱۵۴
---
# رباعی شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>خاقانی را جور فلک یاد آید</p></div>
<div class="m2"><p>گر مرغ دلش زین قفس آزاد آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رقص آید چو دل به فریاد آید</p></div>
<div class="m2"><p>وز فریادش عهد ازل یاد آید</p></div></div>