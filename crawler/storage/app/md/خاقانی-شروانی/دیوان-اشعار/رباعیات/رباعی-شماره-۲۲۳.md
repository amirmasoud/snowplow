---
title: >-
    رباعی شمارهٔ ۲۲۳
---
# رباعی شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>امروز که خورشید سمای سخنم</p></div>
<div class="m2"><p>کس را نرسددست به پای سخنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید که پادشاه هفت اقلیم است</p></div>
<div class="m2"><p>در کوی جهان است گدای سخنم</p></div></div>