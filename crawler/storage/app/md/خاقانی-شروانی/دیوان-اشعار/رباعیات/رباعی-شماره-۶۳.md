---
title: >-
    رباعی شمارهٔ ۶۳
---
# رباعی شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>ناوک زن سینه‌ها شود مژگانت</p></div>
<div class="m2"><p>افسون‌گر دردها شود مرجانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون درد بدید آن لب افسون خوانت</p></div>
<div class="m2"><p>از دست لبت گریخت در دندانت</p></div></div>