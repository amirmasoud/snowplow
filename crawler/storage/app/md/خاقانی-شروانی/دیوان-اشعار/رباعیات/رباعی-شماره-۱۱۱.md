---
title: >-
    رباعی شمارهٔ ۱۱۱
---
# رباعی شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>پیغام غمت سوی دلم می‌آید</p></div>
<div class="m2"><p>زخمت همه بر روی دلم می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل پیش درت به خاک خواهم کردن</p></div>
<div class="m2"><p>کز خاک درت بوی دلم می‌آید</p></div></div>