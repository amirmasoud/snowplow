---
title: >-
    رباعی شمارهٔ ۸۵
---
# رباعی شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>خاقانی از آن شاه بتان طمع گسست</p></div>
<div class="m2"><p>در کار شکسته‌ای چو خود دل دربست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانه چه مرد عشق خورشید بود</p></div>
<div class="m2"><p>کورا به چراغ مختصر باشد دست</p></div></div>