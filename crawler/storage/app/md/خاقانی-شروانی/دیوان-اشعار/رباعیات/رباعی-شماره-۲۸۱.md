---
title: >-
    رباعی شمارهٔ ۲۸۱
---
# رباعی شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>کو آن می دیرسال زودافکن تو</p></div>
<div class="m2"><p>محراب دل من ز حیات تن تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میخانه مقام من به و مسکن تو</p></div>
<div class="m2"><p>خم بر سر من، سبوی در گردن تو</p></div></div>