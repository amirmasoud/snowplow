---
title: >-
    رباعی شمارهٔ ۱۸۶
---
# رباعی شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>در طبع بهیمه سار مردم خو باش</p></div>
<div class="m2"><p>با عادت دیوسان ملک نیرو باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون جان به نکو داشت بود با او باش</p></div>
<div class="m2"><p>گر حال بد است کالبد را گو باش</p></div></div>