---
title: >-
    رباعی شمارهٔ ۸
---
# رباعی شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ای تیر هنر صهیل و برجیس لقا</p></div>
<div class="m2"><p>شعری فش و فرقدفر و ناهید صفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش رخ تو ماه و سماک و جوزا</p></div>
<div class="m2"><p>خوارند چو پیش مهر پروین و سها</p></div></div>