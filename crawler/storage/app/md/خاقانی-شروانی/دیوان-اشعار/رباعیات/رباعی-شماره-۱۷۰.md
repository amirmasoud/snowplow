---
title: >-
    رباعی شمارهٔ ۱۷۰
---
# رباعی شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>ای داده تو را دست سپهر و دل دهر</p></div>
<div class="m2"><p>از بخت تو را تخت و هم از دولت بهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر تو کند به لطف و کین تو به قهر</p></div>
<div class="m2"><p>از شوره گل، از غوره مل، از شکر زهر</p></div></div>