---
title: >-
    رباعی شمارهٔ ۱۲
---
# رباعی شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>سنگ اندر بر بسی دویدیم چو آب</p></div>
<div class="m2"><p>بار همه خار و خس کشیدیم چو آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر به وطن نیارمیدیم چو آب</p></div>
<div class="m2"><p>رفتیم و ز پس باز ندیدم چو آب</p></div></div>