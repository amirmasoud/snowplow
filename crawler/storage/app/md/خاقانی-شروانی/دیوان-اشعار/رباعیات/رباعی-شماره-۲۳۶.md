---
title: >-
    رباعی شمارهٔ ۲۳۶
---
# رباعی شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>دل دل طلبید از پی ره دلجویم</p></div>
<div class="m2"><p>بدرود کنان کرد گذر در کویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که ز راه راه و دل دل کم کن</p></div>
<div class="m2"><p>بنگر که من آه آه و دل دل گویم</p></div></div>