---
title: >-
    رباعی شمارهٔ ۱۴۸
---
# رباعی شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>صد باره وجود را فرو ریخته‌اند</p></div>
<div class="m2"><p>تا همچو تو صورتی برانگیخته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبحان الله ز فرق سر تا قدمت</p></div>
<div class="m2"><p>در قالب آرزوی ما ریخته‌اند</p></div></div>