---
title: >-
    رباعی شمارهٔ ۵۹
---
# رباعی شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>با یار سر انداختنم سود نداشت</p></div>
<div class="m2"><p>در کار حیل ساختنم سود نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کژ باخته‌ام بو که نمانم یکدست</p></div>
<div class="m2"><p>هم ماندم و کژ باختنم سود نداشت</p></div></div>