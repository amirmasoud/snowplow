---
title: >-
    رباعی شمارهٔ ۳۴۸
---
# رباعی شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>خاقانی اگر سر زدهٔ یار آیی</p></div>
<div class="m2"><p>در سرزدگی مگر کله دار آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میکوش که گم کردهٔ دلدار آیی</p></div>
<div class="m2"><p>کر گمشدگی مگر پدیدار آیی</p></div></div>