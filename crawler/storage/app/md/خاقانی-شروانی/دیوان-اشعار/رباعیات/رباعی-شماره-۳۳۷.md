---
title: >-
    رباعی شمارهٔ ۳۳۷
---
# رباعی شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>هر نیمه شبم تبم مرتب بینی</p></div>
<div class="m2"><p>ناخن چو فلک، عرق چو کوکب بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چاشتگهم کوفتهٔ تب بینی</p></div>
<div class="m2"><p>از تب خالم آبله بر لب بین</p></div></div>