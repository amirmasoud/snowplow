---
title: >-
    رباعی شمارهٔ ۱۷۷
---
# رباعی شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>هرکس که شود به مال دنیا فیروز</p></div>
<div class="m2"><p>در چشم کسان بزرگ باشد شب و روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بخت سعید و حسن طالع داری</p></div>
<div class="m2"><p>از مال جهان گنج سعادت اندوز</p></div></div>