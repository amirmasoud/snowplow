---
title: >-
    رباعی شمارهٔ ۳۲۲
---
# رباعی شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>تا بود جوانی آتش جان افزای</p></div>
<div class="m2"><p>جان باز چو پروانه بدم شیفته رای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرد آتش و اوفتاد پروانه ز پای</p></div>
<div class="m2"><p>خاکستر و خاک ماند از آن هر دو بجای</p></div></div>