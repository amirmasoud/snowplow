---
title: >-
    رباعی شمارهٔ ۲۸۰
---
# رباعی شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>خاقانی ازین کوچهٔ بیداد برو</p></div>
<div class="m2"><p>تسلیم کن این غمکده را شاد برو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانی ز فلک یافتهٔ بند تو اوست</p></div>
<div class="m2"><p>جان را به فلک باز ده آزاد برو</p></div></div>