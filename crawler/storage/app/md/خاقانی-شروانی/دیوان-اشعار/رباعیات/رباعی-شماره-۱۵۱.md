---
title: >-
    رباعی شمارهٔ ۱۵۱
---
# رباعی شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>خاقانی امید بر تو بیشی نکند</p></div>
<div class="m2"><p>کس بر تو بگاه عهد پیشی نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خویشان کهن عهد چو بیگانه شدند</p></div>
<div class="m2"><p>بیگانهٔ نو رسیده خویشی نکند</p></div></div>