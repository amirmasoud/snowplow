---
title: >-
    رباعی شمارهٔ ۱۵
---
# رباعی شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>ای تیغ تو آب روشن و آتش ناب</p></div>
<div class="m2"><p>آبی چو خماهن، آتشی چون سیماب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هیبت آن آب تن آتش تاب</p></div>
<div class="m2"><p>رفت آتشی از آتش و آبی از آب</p></div></div>