---
title: >-
    رباعی شمارهٔ ۲۹۲
---
# رباعی شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>چشمم به گل است و مرغ دستان زن تو</p></div>
<div class="m2"><p>میلم به می است و رطل مرد افکن تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین پس من و صحرای دل روشن تو</p></div>
<div class="m2"><p>من چون تو و تو چون من و من بی من تو</p></div></div>