---
title: >-
    رباعی شمارهٔ ۱۹۱
---
# رباعی شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>ماند به بهشت آن رخ گندم گونش</p></div>
<div class="m2"><p>عشاق چو آدم است پیرامونش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاقانی را نرفته بر گندم دست</p></div>
<div class="m2"><p>عمدا ز بهشت می‌کند بیرونش</p></div></div>