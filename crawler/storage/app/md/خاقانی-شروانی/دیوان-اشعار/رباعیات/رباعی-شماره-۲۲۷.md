---
title: >-
    رباعی شمارهٔ ۲۲۷
---
# رباعی شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>بر فرق من آتش تو فشانی و دلم</p></div>
<div class="m2"><p>بر رهگذر غم تو نشانی و دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جور تو جان رفت تو مانی و دلم</p></div>
<div class="m2"><p>من ترک تو گفته‌ام تو دانی و دلم</p></div></div>