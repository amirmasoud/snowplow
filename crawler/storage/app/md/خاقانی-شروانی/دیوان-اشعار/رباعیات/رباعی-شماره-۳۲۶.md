---
title: >-
    رباعی شمارهٔ ۳۲۶
---
# رباعی شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>تا کی به هوس چون سگ تازی تازی</p></div>
<div class="m2"><p>روباه صفت به حیله سازی سازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لهو و لعب نه‌ای دمی واقف خویش</p></div>
<div class="m2"><p>ترسم که همه عمر به بازی بازی</p></div></div>