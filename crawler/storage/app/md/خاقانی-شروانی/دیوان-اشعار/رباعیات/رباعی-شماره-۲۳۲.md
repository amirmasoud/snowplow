---
title: >-
    رباعی شمارهٔ ۲۳۲
---
# رباعی شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>در عشق شکسته بسته دانی چونم</p></div>
<div class="m2"><p>لب بسته و دل شکسته دانی چونم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو مجلس می نشانده دانم چونی</p></div>
<div class="m2"><p>من غرقهٔ خون نشسته دانی چونم</p></div></div>