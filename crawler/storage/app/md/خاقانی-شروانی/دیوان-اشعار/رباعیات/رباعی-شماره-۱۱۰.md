---
title: >-
    رباعی شمارهٔ ۱۱۰
---
# رباعی شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>این رافضیان که امت شیطانند</p></div>
<div class="m2"><p>بی‌دینانند و سخت بی‌ایمانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که خطا فهم و غلط پیمانند</p></div>
<div class="m2"><p>خاقانی را خارجی می‌دانند</p></div></div>