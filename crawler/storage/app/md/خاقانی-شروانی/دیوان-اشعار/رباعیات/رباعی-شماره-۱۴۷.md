---
title: >-
    رباعی شمارهٔ ۱۴۷
---
# رباعی شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>غم شحنهٔ عشق است و بلا انگیزد</p></div>
<div class="m2"><p>جان خواهد شحنگی و رنگ آمیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاقانی اگر سرشک خونین ریزد</p></div>
<div class="m2"><p>گو ریز که سیم شحنه زین برخیزد</p></div></div>