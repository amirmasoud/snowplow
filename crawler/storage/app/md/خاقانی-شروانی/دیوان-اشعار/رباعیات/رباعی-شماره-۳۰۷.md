---
title: >-
    رباعی شمارهٔ ۳۰۷
---
# رباعی شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>تا زلف تو بر بست به رخ پیرایه</p></div>
<div class="m2"><p>بر عارض تو فکند مشکین سایه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای حور جنان تو پیش من راست بگو</p></div>
<div class="m2"><p>شیر تو که داده است، که بودت دایه؟</p></div></div>