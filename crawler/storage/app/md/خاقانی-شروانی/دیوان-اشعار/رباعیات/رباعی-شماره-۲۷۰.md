---
title: >-
    رباعی شمارهٔ ۲۷۰
---
# رباعی شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>مجلس ز می دو ساله گردد روشن</p></div>
<div class="m2"><p>چشم طرب از پیاله گردد روشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پژمرده بود گل قدح بی می ناب</p></div>
<div class="m2"><p>از آب چراغ لاله گردد روشن</p></div></div>