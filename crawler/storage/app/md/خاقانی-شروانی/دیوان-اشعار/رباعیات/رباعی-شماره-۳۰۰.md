---
title: >-
    رباعی شمارهٔ ۳۰۰
---
# رباعی شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>خاقانی را بی‌قلم کاتب شاه</p></div>
<div class="m2"><p>انگشت شد انگشت و قلم ز آتش آه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم بی‌قلمش کاتب گردون صد راه</p></div>
<div class="m2"><p>بگریست قلم‌وار به خوناب سیاه</p></div></div>