---
title: >-
    رباعی شمارهٔ ۱۳۲
---
# رباعی شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>در باغچهٔ عمر من غم پرورد</p></div>
<div class="m2"><p>نه سرو نه سبزه ماند، نه لاله، نه ورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خرمن ایام من از غایت درد</p></div>
<div class="m2"><p>نه خوشه نه دانه ماند، نه کاه نه گرد</p></div></div>