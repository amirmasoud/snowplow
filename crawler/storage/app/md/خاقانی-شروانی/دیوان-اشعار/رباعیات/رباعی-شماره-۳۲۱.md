---
title: >-
    رباعی شمارهٔ ۳۲۱
---
# رباعی شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>چون مجلس عیش سازی استاد علی</p></div>
<div class="m2"><p>جان تو و قطرهٔ می قطربلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون باز به طاعت آئی از پاک دلی</p></div>
<div class="m2"><p>یحیی‌بن معاذی و معاذ جبلی</p></div></div>