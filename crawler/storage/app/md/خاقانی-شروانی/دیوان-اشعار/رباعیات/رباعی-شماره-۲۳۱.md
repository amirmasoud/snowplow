---
title: >-
    رباعی شمارهٔ ۲۳۱
---
# رباعی شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>من دست به شاخ مه مثالی زده‌ام</p></div>
<div class="m2"><p>دل دادم و بس صلای مالی زده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او خود نپذیرد دل و مالم اما</p></div>
<div class="m2"><p>اختر بهگذشتن است، و فالی زده‌ام</p></div></div>