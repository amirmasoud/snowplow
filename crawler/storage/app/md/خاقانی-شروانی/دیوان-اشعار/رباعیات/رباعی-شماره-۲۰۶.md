---
title: >-
    رباعی شمارهٔ ۲۰۶
---
# رباعی شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>بنمود بهار تازه رخسار ای دل</p></div>
<div class="m2"><p>بر باد نهاده باده پیش آر ای دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که گشاد چهره گلزار ای دل</p></div>
<div class="m2"><p>ما و می گلرنگ و لب یار ای دل</p></div></div>