---
title: >-
    رباعی شمارهٔ ۲۰۵
---
# رباعی شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>از آتش عشق آب دهانم همه سال</p></div>
<div class="m2"><p>در آب چو آتش به فغانم همه سال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خاک چو باد بی‌نشانم همه سال</p></div>
<div class="m2"><p>بر باد چو خاک جان‌فشانم همه سال</p></div></div>