---
title: >-
    رباعی شمارهٔ ۱۶۲
---
# رباعی شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>آن شب که دلم نزد تو مهمان باشد</p></div>
<div class="m2"><p>جانم همه در روضهٔ رضوان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانم بر توست لیک فرمان باشد</p></div>
<div class="m2"><p>کامشب تن من نیزد بر جان باشد</p></div></div>