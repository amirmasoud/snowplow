---
title: >-
    رباعی شمارهٔ ۲۱
---
# رباعی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>خاقانی اگرچه در سخن مردوش است</p></div>
<div class="m2"><p>در دست مخنثان عجب دستخوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود هر هنری که مرد ازو زهرچش است</p></div>
<div class="m2"><p>انگشت نمای نیست، انگشت‌کش است</p></div></div>