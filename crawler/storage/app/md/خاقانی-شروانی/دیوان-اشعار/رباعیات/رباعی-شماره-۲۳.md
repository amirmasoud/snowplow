---
title: >-
    رباعی شمارهٔ ۲۳
---
# رباعی شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>گم شد دل خاقانی و جان بر دو یکی است</p></div>
<div class="m2"><p>وز غدر فلک خلاص را هم به شک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر مائده‌ای که دست‌ساز فلک است</p></div>
<div class="m2"><p>یا بی‌نمک است یا سراسر نمک است</p></div></div>