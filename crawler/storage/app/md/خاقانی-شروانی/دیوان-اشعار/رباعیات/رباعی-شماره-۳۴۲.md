---
title: >-
    رباعی شمارهٔ ۳۴۲
---
# رباعی شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>امروز به خشک جان تو مهمان منی</p></div>
<div class="m2"><p>جان پیش کشم چرا که جانان منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیشت به دمی ز درد تو خواهم مرد</p></div>
<div class="m2"><p>دردت بکشم بیا که درمان منی</p></div></div>