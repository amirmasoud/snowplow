---
title: >-
    رباعی شمارهٔ ۲۰۱
---
# رباعی شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>نه خاک توام به آدمی کردهٔ عشق</p></div>
<div class="m2"><p>نه مرغ توام به دانه پروردهٔ عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس بر چو منی پرده دری را مگزین</p></div>
<div class="m2"><p>کآهنگ شناس نیست در پردهٔ عشق</p></div></div>