---
title: >-
    رباعی شمارهٔ ۴۲
---
# رباعی شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>خاکی دلم ای بت ز نهان بازفرست</p></div>
<div class="m2"><p>خون آلود است همچنان باز فرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بازاری که جان ز من، دل ز تو بود</p></div>
<div class="m2"><p>چون بیع به سر نرفت جان باز فرست</p></div></div>