---
title: >-
    رباعی شمارهٔ ۳۴۰
---
# رباعی شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>خاک شومی گرنه چنین خون خوریی</p></div>
<div class="m2"><p>نازت برمی گرنه چنین کافریی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر با دل من به دوستی درخوریی</p></div>
<div class="m2"><p>زین دیده بران دیده گرامی‌تریی</p></div></div>