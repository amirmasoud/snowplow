---
title: >-
    رباعی شمارهٔ ۲۶۶
---
# رباعی شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>تا رخت بیفکند به صحرا دل من</p></div>
<div class="m2"><p>سرمایه زیان کرد ز سودا دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک موی نماند از اجل تا دل من</p></div>
<div class="m2"><p>القصه بطولها دریغا دل من</p></div></div>