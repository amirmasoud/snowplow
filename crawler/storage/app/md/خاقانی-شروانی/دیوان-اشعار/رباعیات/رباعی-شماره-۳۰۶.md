---
title: >-
    رباعی شمارهٔ ۳۰۶
---
# رباعی شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>گفتم پس از آن روز وصال ای دلخواه</p></div>
<div class="m2"><p>شب‌های فراقت چه دراز آمد آه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا شب را در این درازی چه گناه</p></div>
<div class="m2"><p>شب روز وصال است که گردیده سیاه</p></div></div>