---
title: >-
    رباعی شمارهٔ ۶۸
---
# رباعی شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>چون سوی تو نامه‌ای نویسم ز نخست</p></div>
<div class="m2"><p>یا از پی قاصدی کمر بندم چست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد سحری نامه رسان من و توست</p></div>
<div class="m2"><p>ای باد چه مرغی که پرت باد درست</p></div></div>