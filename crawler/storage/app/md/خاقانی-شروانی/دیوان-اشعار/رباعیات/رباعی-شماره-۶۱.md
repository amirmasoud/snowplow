---
title: >-
    رباعی شمارهٔ ۶۱
---
# رباعی شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>گرچه صنما همدم عیسی است دمت</p></div>
<div class="m2"><p>روح القدسی چگونه خوانم صنمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون موی شدم ز بس که بردم ستمت</p></div>
<div class="m2"><p>موئی موئی که موی مویم ز غمت</p></div></div>