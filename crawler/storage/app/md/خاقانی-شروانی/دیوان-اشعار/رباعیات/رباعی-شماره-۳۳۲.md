---
title: >-
    رباعی شمارهٔ ۳۳۲
---
# رباعی شمارهٔ ۳۳۲

<div class="b" id="bn1"><div class="m1"><p>از گردون بر نتابم این بی‌آبی</p></div>
<div class="m2"><p>خون شد دل و اشک آتشی سیمابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی به سرشک و نالهٔ چون دولاب</p></div>
<div class="m2"><p>آتش فکنم در فلک دولابی</p></div></div>