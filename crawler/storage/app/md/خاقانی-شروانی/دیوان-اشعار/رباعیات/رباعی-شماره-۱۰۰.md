---
title: >-
    رباعی شمارهٔ ۱۰۰
---
# رباعی شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>چون قهر الهی امتحان تو کند</p></div>
<div class="m2"><p>حصن تو نهنگ جان‌ستان تو کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وآنجا که کرم نگاهبان تو کند</p></div>
<div class="m2"><p>از کام نهنگ حصن جان تو کند</p></div></div>