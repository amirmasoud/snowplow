---
title: >-
    رباعی شمارهٔ ۲۹۰
---
# رباعی شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>صد ساله ره است از طلب من تا تو</p></div>
<div class="m2"><p>در بادیهٔ طلب من آیم یا تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانی به سه بوسه شرط کردم با تو</p></div>
<div class="m2"><p>شرطی به غلط نرفت ها من، ها تو</p></div></div>