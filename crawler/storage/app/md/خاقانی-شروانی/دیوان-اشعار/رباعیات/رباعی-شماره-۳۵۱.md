---
title: >-
    رباعی شمارهٔ ۳۵۱
---
# رباعی شمارهٔ ۳۵۱

<div class="b" id="bn1"><div class="m1"><p>گر من نه به دل داغ برافکنده‌امی</p></div>
<div class="m2"><p>با تو ز غم آزاد و تو را بنده‌امی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور من نه ز دست چرخ پر کنده‌امی</p></div>
<div class="m2"><p>رد پای تو کشته و به تو زنده‌امی</p></div></div>