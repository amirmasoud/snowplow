---
title: >-
    رباعی شمارهٔ ۱۸
---
# رباعی شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>گر من به وفای عشق آن حور نسب</p></div>
<div class="m2"><p>در دام دگر بتان نیفتم چه عجب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاشا که چو گنجشک بوم دانه طلب</p></div>
<div class="m2"><p>کان ماه مرا همای داده است لقب</p></div></div>