---
title: >-
    رباعی شمارهٔ ۱۹۵
---
# رباعی شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>برداشت فلک به خون خاقانی تیغ</p></div>
<div class="m2"><p>تا ماه مرا کرد نهان اندر میغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی بوسه زدم بر آن لب نوش آمیغ</p></div>
<div class="m2"><p>امروز که بر خاک زنم وای دریغ</p></div></div>