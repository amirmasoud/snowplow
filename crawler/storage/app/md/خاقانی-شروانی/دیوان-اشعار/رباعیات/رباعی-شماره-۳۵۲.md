---
title: >-
    رباعی شمارهٔ ۳۵۲
---
# رباعی شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>دود تو برون شود ز روزن روزی</p></div>
<div class="m2"><p>مرغ تو بپرد از نشیمن روزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم که به کام دوست باشی دو سه سال</p></div>
<div class="m2"><p>ناکام شوی به کام دشمن روزی</p></div></div>