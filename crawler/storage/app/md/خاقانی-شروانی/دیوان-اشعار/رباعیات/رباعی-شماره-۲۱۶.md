---
title: >-
    رباعی شمارهٔ ۲۱۶
---
# رباعی شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>ما ژنده سلب شدیم در خز نخزیم</p></div>
<div class="m2"><p>جز خار نخائیم و به جز گز نگزیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لعل بتان شکر رامز نمزیم</p></div>
<div class="m2"><p>رخسار به خون دختر رز نرزیم</p></div></div>