---
title: >-
    رباعی شمارهٔ ۱۳۹
---
# رباعی شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>چون نامهٔ تو نزد من آمد شب بود</p></div>
<div class="m2"><p>برخواندم و زو شبی دگر کردم سود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس نور معانی تو سر بر زد زود</p></div>
<div class="m2"><p>اندر دو شبم هزار خورشید نمود</p></div></div>