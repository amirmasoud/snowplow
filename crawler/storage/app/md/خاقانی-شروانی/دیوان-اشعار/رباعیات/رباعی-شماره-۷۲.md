---
title: >-
    رباعی شمارهٔ ۷۲
---
# رباعی شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>از کوههٔ چرخ مملکت مه در گشت</p></div>
<div class="m2"><p>وز گوشهٔ نطع مکرمت شه درگشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسکندر ثانی است که از گه در گشت</p></div>
<div class="m2"><p>یا سد سکندر که به ناگه در گشت</p></div></div>