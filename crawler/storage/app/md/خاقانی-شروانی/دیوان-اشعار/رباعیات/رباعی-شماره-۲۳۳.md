---
title: >-
    رباعی شمارهٔ ۲۳۳
---
# رباعی شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>چون پای غم ار ز مجلست بیرونم</p></div>
<div class="m2"><p>از دست غمت چو می در آب و خونم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو مجلس می نشانده دانم چونی</p></div>
<div class="m2"><p>من غرقه خون نشسته دانی چونم</p></div></div>