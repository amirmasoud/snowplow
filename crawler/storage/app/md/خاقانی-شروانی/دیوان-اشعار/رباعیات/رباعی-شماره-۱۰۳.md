---
title: >-
    رباعی شمارهٔ ۱۰۳
---
# رباعی شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>روزی فلکم بخت اگر بازآرد</p></div>
<div class="m2"><p>یار از دل گم بوده خبر بازآرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجران بشود آتشم از دل ببرد</p></div>
<div class="m2"><p>وصل آید و آبم به جگر بازآرد</p></div></div>