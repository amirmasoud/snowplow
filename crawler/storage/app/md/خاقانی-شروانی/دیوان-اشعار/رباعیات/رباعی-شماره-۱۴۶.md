---
title: >-
    رباعی شمارهٔ ۱۴۶
---
# رباعی شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>رخسار تو را که ماه و گل بنده بود</p></div>
<div class="m2"><p>لشکر گه آن زلف سر افکنده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلفت به شکار دل پراکند آری</p></div>
<div class="m2"><p>لشکر به شکارگه پراکنده بود</p></div></div>