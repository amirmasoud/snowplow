---
title: >-
    رباعی شمارهٔ ۳۱۷
---
# رباعی شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>ای یافته از فضل خدا تمکینی</p></div>
<div class="m2"><p>گاهی که شود دچار با مسکینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باید که نوازشی بیابد از تو</p></div>
<div class="m2"><p>از جود رسانی به دلش تسکینی</p></div></div>