---
title: >-
    رباعی شمارهٔ ۱۹۳
---
# رباعی شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>خاقانی اسیر توست مازار و مکش</p></div>
<div class="m2"><p>صیدی است فکندهٔ تو بردار و مکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغی است گرفتهٔ تو مگذار و مکش</p></div>
<div class="m2"><p>گر بگریزد به بند باز آر و مکش</p></div></div>