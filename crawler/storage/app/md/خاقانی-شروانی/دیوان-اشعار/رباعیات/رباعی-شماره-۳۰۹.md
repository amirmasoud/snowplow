---
title: >-
    رباعی شمارهٔ ۳۰۹
---
# رباعی شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>ای با تو مرا دوستی سی روزه</p></div>
<div class="m2"><p>از خدمت تو وصل کنم دریوزه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که چرا تو آب را نادیده</p></div>
<div class="m2"><p>ای جان جهان سبک کشیدی موزه</p></div></div>