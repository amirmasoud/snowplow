---
title: >-
    رباعی شمارهٔ ۱۲۰
---
# رباعی شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>کو آنکه به پرهیز و به توفیق و سداد</p></div>
<div class="m2"><p>هم باقر بود هم رضا هم سجاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر عیار دانش اکنون به بلاد</p></div>
<div class="m2"><p>کو صیرفی و کو محک و کو نقاد</p></div></div>