---
title: >-
    رباعی شمارهٔ ۵۶
---
# رباعی شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>در پیش رخ تو ماه را تاب کجاست</p></div>
<div class="m2"><p>عشاق تو را به دیده در خواب کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید ز غیرتت چنین می‌گوید</p></div>
<div class="m2"><p>کز آتش تو بسوختم آب کجاست</p></div></div>