---
title: >-
    رباعی شمارهٔ ۳۴۶
---
# رباعی شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>ای زلف بتم عقرب مه جولانی</p></div>
<div class="m2"><p>جادو صفتی گرچه به ثعبان مانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر نه بهشت حسن را رضوانی</p></div>
<div class="m2"><p>دوزخ چه نهی در جگر خاقانی</p></div></div>