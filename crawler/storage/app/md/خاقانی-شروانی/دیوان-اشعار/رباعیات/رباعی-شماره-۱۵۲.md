---
title: >-
    رباعی شمارهٔ ۱۵۲
---
# رباعی شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>تا چشم رهی چشم تو را چشمک داد</p></div>
<div class="m2"><p>از چشمهٔ چشم من دو صد چشمه گشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچشم که از چشم بدش چشم رسید</p></div>
<div class="m2"><p>در چشمهٔ چشم تو چنان چشم مباد</p></div></div>