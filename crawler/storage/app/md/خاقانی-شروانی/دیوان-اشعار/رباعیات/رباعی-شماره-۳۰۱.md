---
title: >-
    رباعی شمارهٔ ۳۰۱
---
# رباعی شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>یاران جهان را همه از که تا مه</p></div>
<div class="m2"><p>دیدیم به تحقیق در این دیه از ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همدگر اختلاط چون بند قبا</p></div>
<div class="m2"><p>دارند ولی نیند خال ز گره</p></div></div>