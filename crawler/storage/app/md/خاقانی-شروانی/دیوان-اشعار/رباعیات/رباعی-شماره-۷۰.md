---
title: >-
    رباعی شمارهٔ ۷۰
---
# رباعی شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>آن ماه دو هفته کرده عمدا هر هفت</p></div>
<div class="m2"><p>آمد بر خاقانی و عذرش پذرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناچار که خورشید سوی ذره شود</p></div>
<div class="m2"><p>ذره سوی خورشید کجا داند رفت</p></div></div>