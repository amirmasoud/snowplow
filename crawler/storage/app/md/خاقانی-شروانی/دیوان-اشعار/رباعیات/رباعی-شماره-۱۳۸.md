---
title: >-
    رباعی شمارهٔ ۱۳۸
---
# رباعی شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>زلف تو بنفشه ار غلامی فرمود</p></div>
<div class="m2"><p>زین روی بنفشه حلقه درگوش نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در باغ بنفشه را شرف زان افزود</p></div>
<div class="m2"><p>کو حلقه به گوش زلف تو خواهد بود</p></div></div>