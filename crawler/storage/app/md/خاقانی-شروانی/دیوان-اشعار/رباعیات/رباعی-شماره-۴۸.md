---
title: >-
    رباعی شمارهٔ ۴۸
---
# رباعی شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>چون سقف تو سایه نکند قاعده چیست</p></div>
<div class="m2"><p>چون نان تو موری نخورد مائده چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون منقطعان راه را نان ندهی</p></div>
<div class="m2"><p>پس ز آمدن فید بگو فائده چیست</p></div></div>