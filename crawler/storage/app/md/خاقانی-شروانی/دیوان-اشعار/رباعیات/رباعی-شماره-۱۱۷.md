---
title: >-
    رباعی شمارهٔ ۱۱۷
---
# رباعی شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>ساقی رخ من رنگ نمی‌گرداند</p></div>
<div class="m2"><p>ناله ز دل آهنگ نمی‌گرداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده چه فزون دهی چو کم فایده نیست</p></div>
<div class="m2"><p>کن سیل تو این سنگ نمی‌گرداند</p></div></div>