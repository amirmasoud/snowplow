---
title: >-
    رباعی شمارهٔ ۵۸
---
# رباعی شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>عشق آمد و عقل رفت و منزل بگذاشت</p></div>
<div class="m2"><p>غم رخت فرو نهاد و دل، دل برداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصلی که در اندیشه نیارم پنداشت</p></div>
<div class="m2"><p>نقشی است که آسمان هنوزش ننگاشت</p></div></div>