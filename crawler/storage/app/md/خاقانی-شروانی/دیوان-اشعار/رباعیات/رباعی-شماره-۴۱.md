---
title: >-
    رباعی شمارهٔ ۴۱
---
# رباعی شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>خاقانی را دل تف از درد بسوخت</p></div>
<div class="m2"><p>صبر آمد و لختی غم دل خورد بسوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانه چو شمع را دلی سوخته دید</p></div>
<div class="m2"><p>با سوخته‌ای موافقت کرد بسوخت</p></div></div>