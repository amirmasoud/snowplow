---
title: >-
    غزل شمارهٔ ۱۲۵
---
# غزل شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>اول از خود بری توانم شد</p></div>
<div class="m2"><p>پس تو را مشتری توانم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر تیغ عشق سر بنهم</p></div>
<div class="m2"><p>گر پیت سرسری توانم شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق تو چون خلاف مذهب‌هاست</p></div>
<div class="m2"><p>خصم مذهب‌گری توانم شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به اسلام عشق تو برسم</p></div>
<div class="m2"><p>بندهٔ کافری توانم شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان من تا ز توست آنجائی</p></div>
<div class="m2"><p>من کجا ایدری توانم شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار چون لشکری شود من نیز</p></div>
<div class="m2"><p>بر پیش لشکری توانم شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت خاقانی از خدا برهم</p></div>
<div class="m2"><p>گر ز عشق بری توانم شد</p></div></div>