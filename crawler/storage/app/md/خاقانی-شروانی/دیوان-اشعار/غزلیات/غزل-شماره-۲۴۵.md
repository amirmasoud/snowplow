---
title: >-
    غزل شمارهٔ ۲۴۵
---
# غزل شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>چون تلخ سخن رانی تنگ شکرت خوانم</p></div>
<div class="m2"><p>چون کار به جان آری جان دگرت خوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهر غم خویشم ده تا عمر خوشت گویم</p></div>
<div class="m2"><p>خاک در خویشم خوان تا تاج سرت خوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشک و رخ من هر دو سرخ است و کبود از تو</p></div>
<div class="m2"><p>خوش رنگرزی زین پس عیسی هنرت خوانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون درد توام گیرد دامان غمت گیرم</p></div>
<div class="m2"><p>آیم به سر کویت وز در به درت خوانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین خواندن بی‌حاصل بستم لب و بس کردم</p></div>
<div class="m2"><p>هم کم شنوی دانم گر بیشترت خوانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی که چو وقت آید کارت به ازین سازم</p></div>
<div class="m2"><p>این عشوه مده کانگه افسوس گرت خوانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از محنت خاقانی بس بی‌خبری ویحک</p></div>
<div class="m2"><p>دانم نشوی در خط گر بی‌خبرت خوانم</p></div></div>