---
title: >-
    غزل شمارهٔ ۲۳
---
# غزل شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>دل پیشکش تو جان نهاده است</p></div>
<div class="m2"><p>عشقت به دل جهان نهاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان گر همه با همه دلی داشت</p></div>
<div class="m2"><p>با عشق تو در میان نهاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نام تو بر زبان بیفتاد</p></div>
<div class="m2"><p>دل مهر تو بر زبان نهاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندک سخنی زبانت را عذر</p></div>
<div class="m2"><p>از نیستی دهان نهاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظاره قندز هلالت</p></div>
<div class="m2"><p>موئی به هزار جان نهاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نالهٔ من رقیب در گوش</p></div>
<div class="m2"><p>انگشت خدای خوان نهاده است</p></div></div>