---
title: >-
    غزل شمارهٔ ۱۵۶
---
# غزل شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>روی تو چون نوبهار جلوه‌گری می‌کند</p></div>
<div class="m2"><p>زلف تو چون روزگار پرده‌دری می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>والله اگر سامری کرد به عمری از آنک</p></div>
<div class="m2"><p>چشم تو از سحرها ماحضری می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مفلسی من تو را از بر من می‌برد</p></div>
<div class="m2"><p>سرکشی تو مرا از تو بری می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بکشم که گهی زلف دراز تو را</p></div>
<div class="m2"><p>طرهٔ طرار تو طیره‌گری می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راضیم از عشق تو گر به دلی راضی است</p></div>
<div class="m2"><p>لیک بدان نیست او جمله بری می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل نه همتای توست کز تو زند لاف عشق</p></div>
<div class="m2"><p>می‌نشناسد حریف خیره سری می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشوه‌گری می‌کند لعل تو و طرفه آنک</p></div>
<div class="m2"><p>عقل چو خاقانیی عشوه خری می‌کند</p></div></div>