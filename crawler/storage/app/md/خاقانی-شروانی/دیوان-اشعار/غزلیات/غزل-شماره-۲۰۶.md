---
title: >-
    غزل شمارهٔ ۲۰۶
---
# غزل شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>هر دل که غم تو داغ کردش</p></div>
<div class="m2"><p>خون جگر آمد آب‌خوردش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کوشم با غمت که گردون</p></div>
<div class="m2"><p>کوشید و نبود هم نبردش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در درد فراق تو دل من</p></div>
<div class="m2"><p>جان داد و نکرد هیچ دردش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور از تو گذشت روز عمرم</p></div>
<div class="m2"><p>نزدیک شد آفتاب زردش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بابل اگر نهند شمعی</p></div>
<div class="m2"><p>زینجا بکشم به باد سردش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصل تو دواسبه رفت چون باد</p></div>
<div class="m2"><p>هیهات کجا رسم به گردش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاقانی را جهان سرآمد</p></div>
<div class="m2"><p>دریاب که نیست پایمردش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاصه که به شعر بی‌نظیر است</p></div>
<div class="m2"><p>در جملهٔ آفتاب گردش</p></div></div>