---
title: >-
    غزل شمارهٔ ۶۴
---
# غزل شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>کیست که در کوی تو فتنهٔ روی نیست</p></div>
<div class="m2"><p>وز پی دیدار تو بر سر کوی تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتنه به بازار عشق بر سر کار است از آنک</p></div>
<div class="m2"><p>راستی کار او جز خم موی تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی تو جان پرورد خوی تو خونم خورد</p></div>
<div class="m2"><p>آه که خوی بدت در خور روی تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با غم هجران تو شادم ازیرا مرا</p></div>
<div class="m2"><p>طاقت هجر تو هست طاقت خوی تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی من از هیچ آب بهره ندارد از آنک</p></div>
<div class="m2"><p>آب من از هیچ روی بابت جوی تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوی تو باد آورد دشمن بادی از آنک</p></div>
<div class="m2"><p>جان چو خاقانیی محرم بوی تو نیست</p></div></div>