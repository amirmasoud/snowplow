---
title: >-
    غزل شمارهٔ ۲۶
---
# غزل شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>آگه نه‌ای که بر دلم از غم چه درد خاست</p></div>
<div class="m2"><p>محنت دواسبه آمد و از سینه گرد خاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سینه داغ واقعه نقش‌الحجر بماند</p></div>
<div class="m2"><p>وز دل برای نقش حجر لاجورد خاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان شد سیاه چون دل شمع از تف جگر</p></div>
<div class="m2"><p>پس همچو شمع از مژه خوناب زرد خاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم سنگ خویش گریهٔ خون راندم از فراق</p></div>
<div class="m2"><p>تا سنگ را ز گریهٔ من دل به درد خاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کار عشق دیده مرا پایمرد بود</p></div>
<div class="m2"><p>هر دردسر که دیدم ازین پایمرد خاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل یاد کرد یار فراموش کی کند</p></div>
<div class="m2"><p>در خون نشستن من ازین یاکرد خاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل تشنهٔ مرادم و سیر آمده ز عمر</p></div>
<div class="m2"><p>دل بین کز آتش جگرش آبخورد خاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دردا که بخت من چو زمین کند پای گشت</p></div>
<div class="m2"><p>این کندپائی از فلک تیزگرد خاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در تخت نرد خاکی اسیر مششدرم</p></div>
<div class="m2"><p>زین مهرهٔ دو رنگ کز این تخته‌نرد خاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خصمم که پایمال بلا دید دست کوفت</p></div>
<div class="m2"><p>تا باد سردم از دم گردون نورد خاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر باد خیزد ای عجب از دست کوفتن</p></div>
<div class="m2"><p>از دست کوب خصم مرا باد سرد خاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاقانیا منال که غم را چو تو بسی است</p></div>
<div class="m2"><p>کاول نشست جفت و به فرجام فرد خاست</p></div></div>