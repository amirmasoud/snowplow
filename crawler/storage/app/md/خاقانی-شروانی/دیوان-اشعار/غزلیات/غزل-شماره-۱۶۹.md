---
title: >-
    غزل شمارهٔ ۱۶۹
---
# غزل شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>مرد که با عشق دست در کمر آید</p></div>
<div class="m2"><p>گر همه رستم بود ز پای درآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ورزش عشق بتان چو پردهٔ غیب است</p></div>
<div class="m2"><p>هر دم ازو بازویی دگر بدر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست به عالم تنی که محرم عشق است</p></div>
<div class="m2"><p>گر به وفا ذم کنیش کارگر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پس عمری اگر یکی به من افتد</p></div>
<div class="m2"><p>آن بود آن کز همه جهان به سر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طفل گزین یار تا طفیل نباشی</p></div>
<div class="m2"><p>کانکه دگر دید با تو هم دگر آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتنه شدن بر گیاه خشک نه مردی است</p></div>
<div class="m2"><p>خاصه به وقتی که تازه گل به برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که به معشوق سال‌خورده دهد دل</p></div>
<div class="m2"><p>چون دل خاقانی از مراد برآید</p></div></div>