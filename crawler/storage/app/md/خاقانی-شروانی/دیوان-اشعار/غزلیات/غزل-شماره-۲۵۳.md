---
title: >-
    غزل شمارهٔ ۲۵۳
---
# غزل شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>طبع تو دمساز نیست چاره چه سازم</p></div>
<div class="m2"><p>کین تو کمتر نگشت مهر چه بازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیر جفایت گشاد راه سرشکم</p></div>
<div class="m2"><p>تیغ فراقت درید پردهٔ رازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شب هجران بپرس تا به چه روزم</p></div>
<div class="m2"><p>ز آتش سودا ببین که در چه گدازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهرهٔ آن نیستم که پای تو بوسم</p></div>
<div class="m2"><p>پس به چه دل دست سوی زلف تو یازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز نیازم به شاهد و می و شمع است</p></div>
<div class="m2"><p>هر سه توئی ز آن به سوی توست نیازم</p></div></div>