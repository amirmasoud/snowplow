---
title: >-
    غزل شمارهٔ ۳۲۵
---
# غزل شمارهٔ ۳۲۵

<div class="b" id="bn1"><div class="m1"><p>ای راحت جان‌ها به تو، آرام جان کیستی</p></div>
<div class="m2"><p>دل در هوس جان می‌دهد، تو دلستان کیستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گلبن نادیده دی اصل تو چه وصل تو کی</p></div>
<div class="m2"><p>با بوی مشک و رنگ می از گلستان کیستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از از بتان دلخواه تو، در حسن شاهنشاه تو</p></div>
<div class="m2"><p>ما را بگو ای ماه تو، کز آسمان کیستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگشا صدف یعنی دهن بفشان گهر یعنی سخن</p></div>
<div class="m2"><p>پنهان مکن یعنی ز من تا عشق‌دان کیستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زیر هر مویی جدا یک شهر جان داری نوا</p></div>
<div class="m2"><p>خامی بود گفتن تو را جانا که جان کیستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با مایی و ما را نه‌ای، جانی از آن پیدا نه‌ای</p></div>
<div class="m2"><p>دانم کز آن ما نه‌ای، برگو از آن کیستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاقانی از تیمار تو حیران شد اندر کار تو</p></div>
<div class="m2"><p>ای جان او غم‌خوار تو، تو غم‌نشان کیستی</p></div></div>