---
title: >-
    غزل شمارهٔ ۲۵۲
---
# غزل شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>امروز دو هفته است که روی تو ندیدم</p></div>
<div class="m2"><p>و آن ماه دو هفت از خم موی تو ندیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه منی و عید من و من مه عیدی</p></div>
<div class="m2"><p>زان روی ندیدم که به روی تو ندیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بوی تو دیدم نفس صبح و ز غیرت</p></div>
<div class="m2"><p>در آینهٔ صبح به بوی تو ندیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن غرقهٔ خون رفتم و دل تشنهٔ امید</p></div>
<div class="m2"><p>کز آب وفا قطره به جوی تو ندیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سگ‌جان شدم از بس ستم عالم سگ‌دل</p></div>
<div class="m2"><p>روزی نظری از سگ کوی تو ندیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با درد فراق تو به جان می‌زنم الحق</p></div>
<div class="m2"><p>درمان ز که جویم که ز خوی تو ندیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر هیچ در صومعه‌ای برنگذشتم</p></div>
<div class="m2"><p>کانجا چو خودی در تک و پوی تو ندیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پای طلبم سست شد از سخت دویدن</p></div>
<div class="m2"><p>هر سو که شدم راه به سوی تو ندیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاقانی اگر بیهده گفت از سرمستی</p></div>
<div class="m2"><p>مستی به ازو بیهده گوی تو ندیدم</p></div></div>