---
title: >-
    غزل شمارهٔ ۱۰۲
---
# غزل شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>نگارینا به صحرا رو که صحرا حله می‌پوشد</p></div>
<div class="m2"><p>ز شادی ارغوان با گل شراب وصل می‌نوشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گل بلبل همی گوید که نرگس می‌کند شوخی</p></div>
<div class="m2"><p>مگر نرگس نمی‌داند که خون لاله می‌جوشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه پندم می‌دهد سوسن که گرد عشق کمتر گرد</p></div>
<div class="m2"><p>مگر سوسن نمی‌داند که عاشق پند ننیوشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نثار باغ را گردون به دامن در همی پیچد</p></div>
<div class="m2"><p>گل اندر لکهٔ زمرد ز حجله رخ همی پوشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرو زنهار در بستان که گر خاری به نادانی</p></div>
<div class="m2"><p>سرانگشت تو بخراشد دلم در سینه بخروشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگارا گر چنین زیبا میان باغ بخرامی</p></div>
<div class="m2"><p>کلاهت لاله برگیرد قبایت سرو درپوشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر باد صبا در باغ بوی زلف تو یابد</p></div>
<div class="m2"><p>به دل مهرت خرد حالی به صد جان باز نفروشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خصومت خیزد و آزار و آنگه مردمان گویند</p></div>
<div class="m2"><p>که آن بی‌عقل را بینید چون با باد می‌کوشد</p></div></div>