---
title: >-
    غزل شمارهٔ ۲۸۵
---
# غزل شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>بر سر بازار عشق آزاد نتوان آمدن</p></div>
<div class="m2"><p>بنده باید بودن و در بیع جانان آمدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عتاب دوستان چون سایه نتوان در رمید</p></div>
<div class="m2"><p>جان فشاندن باید و چون سایه بیجان آمدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقبازان را برای سر بریدن سنت است</p></div>
<div class="m2"><p>بر سر نطع ملامت پای‌کوبان آمدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیم شب پنهان به کوی دوست گم نامان شوند</p></div>
<div class="m2"><p>شهره‌نامان را مسلم نیست پنهان آمدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر گنج آن شود کو پی به تاریکی برد</p></div>
<div class="m2"><p>مشعله برکرده سوی گنج نتوان آمدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان در این ره نعل کفش آمد بیندازش ز پای</p></div>
<div class="m2"><p>کی توان با نعل پیش تخت سلطان آمدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه تنگ است ای پسر با پر نگنجد هیچ مرغ</p></div>
<div class="m2"><p>بال و پر بگذار تا بتوانی آسان آمدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرط خاقانی است از کفر آشکارا دم زدن</p></div>
<div class="m2"><p>پس نهان از خاکیان در خون ایمان آمدن</p></div></div>