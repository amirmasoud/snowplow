---
title: >-
    غزل شمارهٔ ۳۸۹
---
# غزل شمارهٔ ۳۸۹

<div class="b" id="bn1"><div class="m1"><p>داور جانی، پس این فریاد جان چون نشنوی</p></div>
<div class="m2"><p>یارب آخر یارب فریاد خوان چون نشنوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد خواهم بر درت در خاک و خون افغان کنان</p></div>
<div class="m2"><p>گیر داد عاشقان ندهی فغان چون نشنوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه سوزان کز ره دل می‌برم سوی دهان</p></div>
<div class="m2"><p>سوی دل باز آرم از ره دهان چون نشنوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر زمان گوئی بگو تا خود نشان عشق چیست</p></div>
<div class="m2"><p>من چه دانم داد عشقت را نشان چون نشنوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کمین غمزها ترکان کمان کش داشتی</p></div>
<div class="m2"><p>گاه تیر افشاندن آواز کمان چون نشنوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوش دریای سرشکم گوش ماهی بشنود</p></div>
<div class="m2"><p>چون در آن دریا تو راندی جوش آن چون نشنوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرسی از حال دلم چون بشنوی فریاد من</p></div>
<div class="m2"><p>حال دل چون پرسی از من هر زمان چون نشنوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوش زیر زلف و زیور زان نهان کردی که آه</p></div>
<div class="m2"><p>نشنوی پیدا ز من باری نهان چون نشنوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویمت کامروز جانم رفت زودش برزنی</p></div>
<div class="m2"><p>چون توئی جان داور ای جان حال جان چون نشنوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر دمت خاقانی از چشم و زبان گنجی دهد</p></div>
<div class="m2"><p>نام خاقانی به گوش دوستان چون نشنوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوه سیمینی و در کوه اوفتد آواز گنج</p></div>
<div class="m2"><p>آخر این آوازهٔ گنج روان چون نشنوی</p></div></div>