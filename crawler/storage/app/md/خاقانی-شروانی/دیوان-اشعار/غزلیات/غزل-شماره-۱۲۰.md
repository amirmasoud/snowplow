---
title: >-
    غزل شمارهٔ ۱۲۰
---
# غزل شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>دوست مرا رطل عشق تا خط بغداد داد</p></div>
<div class="m2"><p>لاجرم از خط صبر کار برون اوفتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبر هزیمت گرفت کز صف مژگان او</p></div>
<div class="m2"><p>غمزه کمان درکشید، فتنه کمین برگشاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق به اول مرا همچو گل از پای سود</p></div>
<div class="m2"><p>دوست به آخر مرا همچو گل از دست داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا در امید من هجر به مسمار کرد</p></div>
<div class="m2"><p>یاد وصالش مرا نعل در آتش نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌کند از بدخوئی آنچه نکرده است کس</p></div>
<div class="m2"><p>گرچه بدی می‌کند، چشم بدش دورباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سینهٔ خاقانی است سوختهٔ عشق او</p></div>
<div class="m2"><p>او به جفا می‌دهد سوختگان را به باد</p></div></div>