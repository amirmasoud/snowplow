---
title: >-
    غزل شمارهٔ ۲۹۲
---
# غزل شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>درد دل گویم از نهان بشنو</p></div>
<div class="m2"><p>راز، بی‌زحمت زبان بشنو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوش دریای غصه باور کن</p></div>
<div class="m2"><p>موج خون بنگر و فغان بشنو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر کنار دو جوی دیدهٔ من</p></div>
<div class="m2"><p>بانگ دولاب آسمان بشنو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لرزهٔ برق در سحاب دل است</p></div>
<div class="m2"><p>نالهٔ رعد ز امتحان بشنو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش کوه ار غمان من گویی</p></div>
<div class="m2"><p>کوه را بانگ الامان بشنو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بخندد عدو ز گریهٔ من</p></div>
<div class="m2"><p>دل به خشمم کند که هان بشنو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تندرستی ورای سلطانی است</p></div>
<div class="m2"><p>از دو تن پرس و شرح آن بشنو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا ز دربان تن‌درست بپرس</p></div>
<div class="m2"><p>یا ز سلطان ناتوان بشنو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حال شب‌های هجر خاقانی</p></div>
<div class="m2"><p>چون بخواهی ز این و آن بشنو</p></div></div>