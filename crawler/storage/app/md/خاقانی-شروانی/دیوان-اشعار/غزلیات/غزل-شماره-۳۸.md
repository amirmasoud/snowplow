---
title: >-
    غزل شمارهٔ ۳۸
---
# غزل شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>چه نشینم که فتنه بر پای است</p></div>
<div class="m2"><p>رایت عشق پای برجای است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچه بایست داشتم الحق</p></div>
<div class="m2"><p>محنت عشق نیز می‌بایست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبر با این بلا ندارد پای</p></div>
<div class="m2"><p>بگریزد نه بند بر پای است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راستی به که صبر معذوراست</p></div>
<div class="m2"><p>بر سر تیغ چون توان پای است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیخ امید من ز بن برکند</p></div>
<div class="m2"><p>آنکه شاخ زمانه پیرای است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار من بد شده است و بدتر ازین</p></div>
<div class="m2"><p>هم شود، تا فلک بر این رای است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از که نالم بگو ز کارگزار</p></div>
<div class="m2"><p>یا از آن کس که کار فرمای است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناله دارد ز زخم، مار سلیم</p></div>
<div class="m2"><p>مار از آن کس که ما را فسای است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیز خاقانی از نشیمن خاک</p></div>
<div class="m2"><p>که نه بس جای راحت افزای است</p></div></div>