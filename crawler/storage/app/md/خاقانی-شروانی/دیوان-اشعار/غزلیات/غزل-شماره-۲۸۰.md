---
title: >-
    غزل شمارهٔ ۲۸۰
---
# غزل شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>دلم دردمند است باری برافکن</p></div>
<div class="m2"><p>بر افکندهٔ خود نظر بهتر افکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میندیش اگر صبر من لشکری شد</p></div>
<div class="m2"><p>دلت سنگ شد سنگ بر لشکر افکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر با غمت گرم در کار نایم</p></div>
<div class="m2"><p>ز دم‌های سردم گره دربر افکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نزل عشقت به جز جان فرستم</p></div>
<div class="m2"><p>به خاکش فرو نه، برون در افکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را طوق سیمین درافکند غبغب</p></div>
<div class="m2"><p>مرا نیز از آن زلف طوقی برافکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پی از هر خسی سایه پرورد بگسل</p></div>
<div class="m2"><p>نظر بر عزیزان جان پرور افکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که فرمایدت کشنای خسان شو</p></div>
<div class="m2"><p>که گوید که هرای زر بر خر افکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشو در خط از پند خاقانی ای جان</p></div>
<div class="m2"><p>که این خوش حدیثی است بر دفتر افکن</p></div></div>