---
title: >-
    غزل شمارهٔ ۱۶
---
# غزل شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>به یکی نامهٔ خودم دریاب</p></div>
<div class="m2"><p>به دو انگشت کاغذم دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به فراقی که سوزدم کشتی</p></div>
<div class="m2"><p>به پیامی که سازدم دریاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد من بر طبیب عرض مکن</p></div>
<div class="m2"><p>تو مسیح منی خودم دریاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کارم از دست شد ز دست فراق</p></div>
<div class="m2"><p>دست در دامنت زدم دریاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من از خیره‌کش فراق هنوز</p></div>
<div class="m2"><p>دیت وصل نستدم دریاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الله الله که از عذاب سفر</p></div>
<div class="m2"><p>به علی‌الله درآمدم دریاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دردمندم ز نقل خانهٔ آب</p></div>
<div class="m2"><p>به گلاب و طبرزدم دریاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من که در یک دو نه سه چار یکی</p></div>
<div class="m2"><p>بستهٔ ششدر آمدم دریاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من که خاقانیم به دست عنا</p></div>
<div class="m2"><p>چون خیال مشعبدم دریاب</p></div></div>