---
title: >-
    غزل شمارهٔ ۳۲۲
---
# غزل شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>ای دل ای دل هلاک تن کردی</p></div>
<div class="m2"><p>بس کن ای دل که کار من کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر من زان جهان همی آید</p></div>
<div class="m2"><p>که ره جان به پای تن کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سگان کی به زهرهٔ شیر</p></div>
<div class="m2"><p>که شکار آهوی ختن کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب مهتاب چون به سر بازی</p></div>
<div class="m2"><p>قصد خورشید غمزه زن کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شبستان آفتاب شدی</p></div>
<div class="m2"><p>آه من آسمان شکن کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر سلیمان نه‌ای به دیودلی</p></div>
<div class="m2"><p>در پری خانه چون وطن کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لاجرم بهر یک شبه طربت</p></div>
<div class="m2"><p>برگ صد سالم از حزن کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توئی آن مرغ کآتش آوردی</p></div>
<div class="m2"><p>خود به خود قصد سوختن کردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیشه در بیشهٔ بلا بردی</p></div>
<div class="m2"><p>هر سر شاخ بابزن کردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دانهٔ دست پایدام تو گشت</p></div>
<div class="m2"><p>از که نالی که خویشتن کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای چو زنبور کلبهٔ قصاب</p></div>
<div class="m2"><p>که سر اندر سر دهن کردی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخن اندر زر است خاقانی</p></div>
<div class="m2"><p>تو همه تکیه بر سخن کردی</p></div></div>