---
title: >-
    غزل شمارهٔ ۲۷۲
---
# غزل شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>دلا زارت برون نتوان نهادن</p></div>
<div class="m2"><p>قدم در موج خون نتوان نهادن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر اسب عمر هرای جوانی است</p></div>
<div class="m2"><p>بر او زین سرنگون نتوان نهادن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو را هر دم غم صد ساله روزی است</p></div>
<div class="m2"><p>ذخیره زین فزون نتوان نهادن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کتف عمر میکش بار محنت</p></div>
<div class="m2"><p>که بر دهر حرون نتوان نهادن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نامت چون توان کرد ابلقی را</p></div>
<div class="m2"><p>که داغش بر سرون نتوان نهادن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در این منزل رصد جهان می‌ستاند</p></div>
<div class="m2"><p>گنه بر رهنمون نتوان نهادن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خراب است آن جهان کاول تو دیدی</p></div>
<div class="m2"><p>اساسی نو کنون نتوان نهادن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به صد غم ریسمان جان گسسته است</p></div>
<div class="m2"><p>غمی را پنبه چون نتوان نهادن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلی کز جنس برکندی نگهدار</p></div>
<div class="m2"><p>که بر ناجنس و دون نتوان نهادن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرت خاقانیا در بیم راهی است</p></div>
<div class="m2"><p>کز آنجا پی برون نتوان نهادن</p></div></div>