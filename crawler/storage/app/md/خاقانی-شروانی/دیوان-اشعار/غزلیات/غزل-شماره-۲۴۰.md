---
title: >-
    غزل شمارهٔ ۲۴۰
---
# غزل شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>کفر است راز عشقت پنهان چرا ندارم</p></div>
<div class="m2"><p>دارم به کفر عشقت ایمان چرا ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوزی ز ساز عشقت در دل چرا نگیرم</p></div>
<div class="m2"><p>رمزی ز راز مهرت در جان چرا ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش به خاک پنهان دارند صبح خیزان</p></div>
<div class="m2"><p>من خاک عشقم آتش پنهان چرا ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عید است این که بر جان کشتن حواله کردی</p></div>
<div class="m2"><p>چون کشتنی است جانم، قربان چرا ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی کم سعادت است این کامد غم تو در دل</p></div>
<div class="m2"><p>چون دل سرای غم شد شادان چرا ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا خود پرست بودم کارم نداشت سامان</p></div>
<div class="m2"><p>چون بی‌خودی است کارم سامان چرا ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهتاب را به ویران رسم است نور دادن</p></div>
<div class="m2"><p>پس من سراچهٔ جان ویران چرا ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ریحان هر سفالی پیداست آن من کو</p></div>
<div class="m2"><p>من دل سفال کردم ریحان چرا ندارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاقانیم نه والله سیمرغ نیست هستم</p></div>
<div class="m2"><p>پس هست و نیست گیتی یکسان چرا ندارم</p></div></div>