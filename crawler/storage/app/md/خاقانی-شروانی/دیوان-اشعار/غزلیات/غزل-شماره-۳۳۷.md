---
title: >-
    غزل شمارهٔ ۳۳۷
---
# غزل شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>تا حلقه‌های زلف به هم برشکسته‌ای</p></div>
<div class="m2"><p>بس توبه‌های ما که بهم درشکسته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه از ستیزه گوش فلک برکشیده‌ای</p></div>
<div class="m2"><p>گاه از کرشمه دیدهٔ اختر شکسته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانم که مه جبینی ای آسمان شکن</p></div>
<div class="m2"><p>اما ندانم آنکه چه لشکر شکسته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آهسته‌تر، نه ملک خراسان گرفته‌ای</p></div>
<div class="m2"><p>و آسوده‌تر، نه رایت سنجر شکسته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شاه‌راه عشق تو هر محملی که بود</p></div>
<div class="m2"><p>بر دل شکستگان قلندر شکسته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گوشه‌ها هزار جگر گوشه خورده‌ای</p></div>
<div class="m2"><p>وز کبر گوشهٔ کله اندر شکسته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک مشت خاک غارت کردن نه مشکل است</p></div>
<div class="m2"><p>بس کن که نه طلسم سکندر شکسته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درهم شکسته‌ای دل خاقانی از جفا</p></div>
<div class="m2"><p>تاوان بده ز لعل که گوهر شکسته‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاقانیا نشیمن شروان نه جای توست</p></div>
<div class="m2"><p>بر پر سوی عراق نه شهپر شکسته‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رو کز کمان گروههٔ خاطر به مهره‌ای</p></div>
<div class="m2"><p>بر چرخ، پر تیر سخنور شکسته‌ای</p></div></div>