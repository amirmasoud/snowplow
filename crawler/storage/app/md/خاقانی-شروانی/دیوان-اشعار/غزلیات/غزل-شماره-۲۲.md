---
title: >-
    غزل شمارهٔ ۲۲
---
# غزل شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>تا جهان است از جهان اهل وفائی برنخاست</p></div>
<div class="m2"><p>نیک عهدی برنیامد، آشنائی برنخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئی اندر کشور ما بر نمی‌خیزد وفا</p></div>
<div class="m2"><p>یا خود اندر هفت کشور هیچ جائی برنخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون به خون می‌شوی کز راحت نشانی مانده نیست</p></div>
<div class="m2"><p>خود به خود می ساز کز همدم وفائی برنخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از مزاج اهل عالم مردمی کم جوی از آنک</p></div>
<div class="m2"><p>هرگز از کاشانهٔ کرکس همائی برنخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باورم کن کز نخستین تخم آدم تاکنون</p></div>
<div class="m2"><p>از زمین مردمی مردم گیائی برنخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وحشتی داری برو با وحش صحرا انس گیر</p></div>
<div class="m2"><p>کز میان انس و جان وحشت زدائی برنخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوس وحدت زن درین پیروزه گنبد کاندراو</p></div>
<div class="m2"><p>از نوای کوس وحدت به نوائی برنخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درنورد از آه سرد این تخت نرد سبز را</p></div>
<div class="m2"><p>کاندر او تا اوست خصل بی‌دغائی برنخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میل در چشم امل کش تا نبیند در جهان</p></div>
<div class="m2"><p>کز جهان تاریک‌تر زندان سرائی برنخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از امل بیمار دل را هیچ نگشاید از آنک</p></div>
<div class="m2"><p>هرگز از گوگرد تنها کیمیائی برنخاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از کس و ناکس ببر خاقانی آسا کز جهان</p></div>
<div class="m2"><p>هیچ صاحب درد را صاحب دوائی برنخاست</p></div></div>