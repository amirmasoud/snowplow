---
title: >-
    غزل شمارهٔ ۲
---
# غزل شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>طبعِ تو دم‌ساز نیست عاشقِ دل‌سوز را</p></div>
<div class="m2"><p>خویِ تو یاری‌گر است یارِ بدآموز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست‌خوشِ تو من‌ام دستِ جفا برگشای</p></div>
<div class="m2"><p>بر دلِ من برگمار تیرِ جگردوز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پیِ آن را که شب پردهٔ رازِ من است</p></div>
<div class="m2"><p>خواهم کز دودِ دل پرده کنم روز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیک ز بیمِ رقیب وز پیِ نفیِ گمان</p></div>
<div class="m2"><p>راهِ برون بسته‌ام آهِ درون‌سوز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل چه شناسد که چیست قیمتِ سودایِ تو</p></div>
<div class="m2"><p>قدر چه داند صدف دُرِّ شب‌افروز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر اثرِ رویِ تو سویِ گلستان رسد</p></div>
<div class="m2"><p>بادِ صبا رد کند تحفهٔ نوروز را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا دلِ خاقانی است از تو همی‌نگذرد</p></div>
<div class="m2"><p>بو که درآرد به مهر آن دلِ کین‌توز را</p></div></div>