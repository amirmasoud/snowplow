---
title: >-
    غزل شمارهٔ ۳۰۵
---
# غزل شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>خیال روی توام غم‌گسار و روی تو نه</p></div>
<div class="m2"><p>به هر سوئی که کنم راه، راه سوی تو نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال تو همه شب ره به کوی من دارد</p></div>
<div class="m2"><p>اگرچه بخت مرا رهنما به کوی تو نه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریغ کاش تو را خوی چون خیال بدی</p></div>
<div class="m2"><p>که خرمم ز خیال تو و زخوی تو نه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من آرزوی وصل می‌کند چه کنم</p></div>
<div class="m2"><p>که آرزوی من این است و آرزوی تو نه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا به نوک مژه غمزهٔ تو دعوت کرد</p></div>
<div class="m2"><p>بخورد خونم و گفتا برو که خوی تو نه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بوئی از تو شدم قانع و همی دانم</p></div>
<div class="m2"><p>که هیچ رنگ مرا از تو جز که بوی تو نه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار جوی هوس رفته است در دل تو</p></div>
<div class="m2"><p>که هیچ آب غم من روان به جوی تو نه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز جستجوی تو حیرت نصیب خاقانی است</p></div>
<div class="m2"><p>تو کیمیائی و او مرد جستجوی تو نه</p></div></div>