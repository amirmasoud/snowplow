---
title: >-
    غزل شمارهٔ ۳۴۷
---
# غزل شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>هر زمان بر جان من باری نهی</p></div>
<div class="m2"><p>وین دل غم‌خواره را خاری نهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس کم آزار می‌نپندارم که تو</p></div>
<div class="m2"><p>مهر بر چون من کم‌آزاری نهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا برداری انگشت جفا</p></div>
<div class="m2"><p>زود بر حرف وفاداری نهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچت افتد کاین دل دیوانه را</p></div>
<div class="m2"><p>از سر رغبت سر و کاری نهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای اگر در کار من ننهی به وصل</p></div>
<div class="m2"><p>دست شفقت بر سرم باری نهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور ببخشی بوسهٔ آخر به لطف</p></div>
<div class="m2"><p>مرهمی بر جان افگاری نهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار خاقانی بسازی زین قدر</p></div>
<div class="m2"><p>کار او را نام بی‌کاری نهی</p></div></div>