---
title: >-
    غزل شمارهٔ ۳۵۱
---
# غزل شمارهٔ ۳۵۱

<div class="b" id="bn1"><div class="m1"><p>درآ کز یک نظر جان تازه کردی</p></div>
<div class="m2"><p>بسا عشق کهن کان تازه کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو می در جان نشین تا غم نشانی</p></div>
<div class="m2"><p>که چون می مجلس جان تازه کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می چون بوستان افروز ده زانک</p></div>
<div class="m2"><p>سفال دل چو ریحان تازه کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیالت در برم باغ طرب داشت</p></div>
<div class="m2"><p>رسیدی ز آب حیوان تازه کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز برق خنده‌های سر به مهرت</p></div>
<div class="m2"><p>به مجلس بوسه باران تازه کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قیامت‌هاست در زلف تو پنهان</p></div>
<div class="m2"><p>قیامت را به پنهان تازه کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سیمین تخته و مشکین ده آیت</p></div>
<div class="m2"><p>دبیران را دبستان تازه کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جزعین پردهٔ قیری عروسان</p></div>
<div class="m2"><p>امیران را شبستان تازه کردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شبانگه آفتاب آوردی از رخ</p></div>
<div class="m2"><p>مرا عهد سلیمان تازه کردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلیمانم نه خاقانی که جانم</p></div>
<div class="m2"><p>بدان داودی الحان تازه کردی</p></div></div>