---
title: >-
    غزل شمارهٔ ۳۱۰
---
# غزل شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>در صبوح آن راح ریحانی بخواه</p></div>
<div class="m2"><p>دانهٔ مرغان روحانی بخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک دو جام از راه مخموری بخور</p></div>
<div class="m2"><p>یک دو جنس از روی یکسانی بخواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساغری چون اشک داودی به رنگ</p></div>
<div class="m2"><p>از پری‌روی سلیمانی بخواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدبان عقل را بربند چشم</p></div>
<div class="m2"><p>چشم بندش آنچه می‌دانی بخواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهدان را آشکارا می بده</p></div>
<div class="m2"><p>شاهدان را بوسه پنهانی بخواه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام خم کن جرعه بر خامان بریز</p></div>
<div class="m2"><p>عذر تشویر از پشیمانی بخواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست برکن، زلف بت‌رویان بگیر</p></div>
<div class="m2"><p>پوزش خجلت ز نادانی بخواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سفالین گاو سیمین آهوان</p></div>
<div class="m2"><p>عید جان را خون قربانی بخواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر به مستی دست یابی بر فلک</p></div>
<div class="m2"><p>زو قصاص جان خاقانی بخواه</p></div></div>