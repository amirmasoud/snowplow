---
title: >-
    غزل شمارهٔ ۳۰
---
# غزل شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>زخم زمانه را در مرهم پدید نیست</p></div>
<div class="m2"><p>دارو بر آستانهٔ عالم پدید نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زیر آبنوس شب و روز هیچ دل</p></div>
<div class="m2"><p>شمشادوار تازه و خرم پدید نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرک اندرون پنجرهٔ آسمان نشست</p></div>
<div class="m2"><p>از پنجهٔ زمانه مسلم پدید نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل به غم نشین که سلامت نهفته ماند</p></div>
<div class="m2"><p>وی جم به ماتم آی که خاتم پدید نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردا که چنگ عمر شد زا ساز و بدتر آنک</p></div>
<div class="m2"><p>سرنای گم به بودهٔ ماتم پدید نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاقانیا دمی که وبال حیات توست</p></div>
<div class="m2"><p>در سینه کن به گور که همدم پدید نیست</p></div></div>