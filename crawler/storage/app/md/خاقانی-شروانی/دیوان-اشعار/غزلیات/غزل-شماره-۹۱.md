---
title: >-
    غزل شمارهٔ ۹۱
---
# غزل شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>دل پیش خیال تو صد دیده برافشاند</p></div>
<div class="m2"><p>در پای تو هر ساعت جانی دگر افشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعلت به شکرخنده بر کار کسی خندد</p></div>
<div class="m2"><p>کو وقت نثار تو بر تو شکر افشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شو آینه حاضر کن در خنده ببین آن لب</p></div>
<div class="m2"><p>گر دیده نه‌ای هرگز کاتش گهر افشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هجر تو در چشمم خورشید شود سفته</p></div>
<div class="m2"><p>از بس که مرا الماس اندر بصر افشاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیش سر مژگانت ببرید رگ جانم</p></div>
<div class="m2"><p>زان هر نفسی چشمم خون جگر افشاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر در همه عمر از تو وصلی رسدم یک شب</p></div>
<div class="m2"><p>مرغ سحری بینی حالی که پر افشاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر تارک خاقانی از وصل کلاهی نه</p></div>
<div class="m2"><p>تا دامن خرسندی از خلق برافشاند</p></div></div>