---
title: >-
    غزل شمارهٔ ۲۱۵
---
# غزل شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>از گشت چرخ کار به سامان نیافتم</p></div>
<div class="m2"><p>وز دور دهر عمر تن آسان نیافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین روزگار بی‌بر و گردون کژ نهاد</p></div>
<div class="m2"><p>یک رنج بازگوی که من آن نیافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نطقم از آن گسست که همدم ندیده‌ام</p></div>
<div class="m2"><p>دردم از آن فزود که درمان نیافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از قبضهٔ کمان فلک بر دلم به قهر</p></div>
<div class="m2"><p>تیری چنان گذشت که پیکان نیافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوانی نهاد دهر به پیشم ز خوردنی</p></div>
<div class="m2"><p>جز قرص آفتاب در آن خوان نیافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر ابلق امید نشستم به جد و جهد</p></div>
<div class="m2"><p>جولان نکرد بخت که میدان نیافتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر چرخ هفتمین شدم از نحس روزگار</p></div>
<div class="m2"><p>یک هم‌نشین سعد چو کیوان نیافتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پشتم شکست چرخ که رویم نگه نداشت</p></div>
<div class="m2"><p>آبم ببرد دهر کز او نان نیافتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در مصر انتظار چو یوسف بمانده‌ام</p></div>
<div class="m2"><p>بسیار جهد کردم و کنعان نیافتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوئی سکندرم ز پی آب زندگی</p></div>
<div class="m2"><p>عمرم گذشت و چشمهٔ حیوان نیافتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز افراسیاب دهر خراب است ملک دل</p></div>
<div class="m2"><p>دردا که زور رستم دستان نیافتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گویا ترم ز بلبل لیکن ز غم چو باز</p></div>
<div class="m2"><p>خاموش از آن شدم که سخندان نیافتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاقانیا تو غم خور کز جور روزگار</p></div>
<div class="m2"><p>یک رادمرد خوش‌دل و خندان نیافتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داد سخن دهم که زمانه به رمز گفت</p></div>
<div class="m2"><p>آن یافتم ز تو که ز حسان نیافتم</p></div></div>