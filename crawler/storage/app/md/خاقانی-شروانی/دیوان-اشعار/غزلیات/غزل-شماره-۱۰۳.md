---
title: >-
    غزل شمارهٔ ۱۰۳
---
# غزل شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>عشق تو به هر دلی فرو ناید</p></div>
<div class="m2"><p>و اندوه تو هر تنی نفرساید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کتم عدم هنوز موقوف است</p></div>
<div class="m2"><p>آن سینه که سوزش تو را شاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هجر تو ایمنم چو می‌دانم</p></div>
<div class="m2"><p>کو دست به خون من نیالاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با خوی تو صورتم نمی‌بندد</p></div>
<div class="m2"><p>کز عشق تو جز دریغ برناید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با دستان غم تو می‌سازم</p></div>
<div class="m2"><p>گر ناز تو زخمه در نیفزاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن می‌کنی از جفا که لاتسل</p></div>
<div class="m2"><p>تا کیست که گوید این نمی‌شاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اندیشهٔ تو قرار من رفته است</p></div>
<div class="m2"><p>گر لطف کنی قرار باز آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون طشت میان تهی است خاقانی</p></div>
<div class="m2"><p>زان راحت‌ها که روح را باید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون زخم رسد به طشت بخروشد</p></div>
<div class="m2"><p>انشگت بر او نهی بیاساید</p></div></div>