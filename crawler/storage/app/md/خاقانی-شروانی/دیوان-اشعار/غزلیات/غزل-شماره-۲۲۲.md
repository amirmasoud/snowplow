---
title: >-
    غزل شمارهٔ ۲۲۲
---
# غزل شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>تا چند ستم رسیده باشم</p></div>
<div class="m2"><p>چون سایه ز خود رمیده باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب بسته گلو گرفته چون نای</p></div>
<div class="m2"><p>نالان و ستم رسیده باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انصاف بده چرا ننالم</p></div>
<div class="m2"><p>کانصاف ز کس ندیده باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند از سگ ابلق شب و روز</p></div>
<div class="m2"><p>افتادهٔ سگ گزیده باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند از پی آب‌دست هر خس</p></div>
<div class="m2"><p>چون بلبله قد خمیده باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی چو ترازو از زبانی</p></div>
<div class="m2"><p>در گردن زه کشیده باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طیار شوم زبان ببرم</p></div>
<div class="m2"><p>تا راست روی گزیده باشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون صبح و محک به راست گویی</p></div>
<div class="m2"><p>گویای زبان بریده باشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوئی که ز غم مجوش و مخروش</p></div>
<div class="m2"><p>این پند بسی شنیده باشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درجوش و خروش ابر و بحرم</p></div>
<div class="m2"><p>نتوانم کرمیده باشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاقانی دلفکارم آری</p></div>
<div class="m2"><p>اندیک نه شوخ‌دیده باشم</p></div></div>