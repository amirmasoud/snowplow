---
title: >-
    غزل شمارهٔ ۳۷۴
---
# غزل شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>گر بر در وصالت امید بار بودی</p></div>
<div class="m2"><p>بس دیده کز جمالت امیدوار بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این فتنه‌ها نرفتی از روزگار بر ما</p></div>
<div class="m2"><p>گرنه جمال رویت در روزگار بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را غم فراقت بحری است بی‌کرانه</p></div>
<div class="m2"><p>ای کاش با چنین غم دل در کنار بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب چه رونق استی بازار ساحری را</p></div>
<div class="m2"><p>گر چون دو چشمت او را یک کیسه‌دار بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بر فلک رسیدی از روی تو خیالی</p></div>
<div class="m2"><p>در چشم هر ستاره صد لاله‌زار بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفتی چو آن گل ما از بهر صید گلشن</p></div>
<div class="m2"><p>گل را به چشم بلبل کی اعتبار بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاقانی ار نبودی مداح خوبی تو</p></div>
<div class="m2"><p>خاقان اکبر او را کی خواستار بودی</p></div></div>