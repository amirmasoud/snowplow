---
title: >-
    غزل شمارهٔ ۸۲
---
# غزل شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>علم عشق عالی افتاده است</p></div>
<div class="m2"><p>کیسهٔ صبر خالی افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اختیاری نبود عشق مرا</p></div>
<div class="m2"><p>که ضروری و حالی افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اختر عشق را به طالع من</p></div>
<div class="m2"><p>صفت بی‌زوالی افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست بر شاخ وصل او نرسد</p></div>
<div class="m2"><p>ز آنکه در اصل عالی افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش بخندم چو زلف او بینم</p></div>
<div class="m2"><p>زآنکه شکلش هلالی افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه دارد ضمیر خاقانی</p></div>
<div class="m2"><p>در غمش حسب حالی افتاده است</p></div></div>