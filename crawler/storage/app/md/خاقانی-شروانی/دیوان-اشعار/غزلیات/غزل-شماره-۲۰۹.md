---
title: >-
    غزل شمارهٔ ۲۰۹
---
# غزل شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>خسته‌ام نیک از بد ایام خویش</p></div>
<div class="m2"><p>طیره‌ام بر طالع پدرام خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سپیدی کار طالع بخت را</p></div>
<div class="m2"><p>بس سیه بینم زبان و کام خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل سبوی غم تهی بر من کند</p></div>
<div class="m2"><p>من ز خون دل کنم پر جام خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل هم از من دوست‌گیر است ای عجب</p></div>
<div class="m2"><p>بر زبان غم دهد پیغام خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به دندان گوشهٔ دل چون خورم</p></div>
<div class="m2"><p>کو چنان در گوشه دید آرام خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل نه پیکان است، هم خون است و گوشت</p></div>
<div class="m2"><p>گوشت نتوان خوردن از اندام خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آسمان هردم کشد وانگه دهد</p></div>
<div class="m2"><p>کشتگان را طعمهٔ اجرام خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کلبهٔ قصاب چند آرد برون</p></div>
<div class="m2"><p>سرخ زنبوران خون آشام خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وام بستانم دهم خواهنده را</p></div>
<div class="m2"><p>پس ز گنج غیب بدهم وام خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سایلان از من چنین خوش‌دل روند</p></div>
<div class="m2"><p>من چنین ناخوش‌دل از ایام خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سایل ار خرم شود زاکرام من</p></div>
<div class="m2"><p>من شوم خرم‌تر از اکرام خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از برای شادی سائل به رنگ</p></div>
<div class="m2"><p>زعفران سازم رخ زرفام خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دانگی از خود باز گیرم بهر قوت</p></div>
<div class="m2"><p>پس دهم دیناری از انعام خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کام من بالله که ناکام من است</p></div>
<div class="m2"><p>تا به ناکامی برآرم کام خویش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دست همت بس فراخ آمد مرا</p></div>
<div class="m2"><p>پای همت تنگ دارم گام خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>او به نسبت خوانده خاقانی مرا</p></div>
<div class="m2"><p>من کنم خاقان همت نام خویش</p></div></div>