---
title: >-
    غزل شمارهٔ ۳۴۵
---
# غزل شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>چه کردم کاستین بر من فشاندی</p></div>
<div class="m2"><p>مرا کشتی و پس دامن فشاندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جفا پل بود، بر عاشق شکستی</p></div>
<div class="m2"><p>وفا گل بود، بر دشمن فشاندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خورشید آمدی بر روزن دل</p></div>
<div class="m2"><p>برفتی خاک بر روزن فشاندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبالب جام با دو نان کشید</p></div>
<div class="m2"><p>پیاپی جرعه‌ها بر من فشاندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا صد دام در هر سو نهادی</p></div>
<div class="m2"><p>هزاران دانه پیرامن فشاندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو را باد است در سر خاصه اکنون</p></div>
<div class="m2"><p>که گرد مشک بر سوسن فشاندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو هم ناورد خاقانی نه‌ای ز آنک</p></div>
<div class="m2"><p>سلاح مردمی از تن فشاندی</p></div></div>