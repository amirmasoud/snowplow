---
title: >-
    غزل شمارهٔ ۵۷
---
# غزل شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>بس لابه که بنمودم و دل‌دار نپذرفت</p></div>
<div class="m2"><p>صد بار فغان کردم و یک‌بار نپذرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست غم هجر به زنهار وصالش</p></div>
<div class="m2"><p>انگشت زنان رفتم و زنهار نپذرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه سینه ز غم سوختم و دوست نبخشود</p></div>
<div class="m2"><p>گه تحفه ز جان ساختم و یار نپذرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس شب که نوان بودم بر درگه وصلش</p></div>
<div class="m2"><p>تا روز مرا در زد و دیدار نپذرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که به مسمار بدوزم در هجرش</p></div>
<div class="m2"><p>بسیار حیل کردم و مسمار نپذرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر دشمن من زر به خروار برافشاند</p></div>
<div class="m2"><p>وز دامن من در به انبار نپذرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پذرفت مرا اول و رد کرد به آخر</p></div>
<div class="m2"><p>هان ای دل خاقانی پندار نپذرفت</p></div></div>