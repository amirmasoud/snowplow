---
title: >-
    غزل شمارهٔ ۳۰۸
---
# غزل شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>ای از پی آشوب ما از رخ نقاب انداخته</p></div>
<div class="m2"><p>لعل تو سنگ سرزنش بر افتاب انداخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه با خیال روی تو، گم‌گشته اندر کوی تو</p></div>
<div class="m2"><p>شب با جمال موی تو، مشکین حجاب انداخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای عاقلان را بارها بر لب زده مسمارها</p></div>
<div class="m2"><p>وی خستگان را خارها در جای خواب انداخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای کرده غارت منزلم آتش زده آب و گلم</p></div>
<div class="m2"><p>زلف تو در حلق دلم مشکین طناب انداخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان نرگس جادو نسب جان مرا بگرفته تب</p></div>
<div class="m2"><p>خواب مرا هم نیم شب بسته به آب انداخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بر خسی بگماشتی کز خاک ره برداشتی</p></div>
<div class="m2"><p>خاکی دلم بگذاشتی در خون ناب انداخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون چنگ خود نوحه‌کنان مانند دف بر رخ زنان</p></div>
<div class="m2"><p>وز نای حلق افغان‌کنان بانگ رباب انداخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آسیب دست دلبرش نیلی شده سیمین برش</p></div>
<div class="m2"><p>سیارها نیلوفرش بر افتاب انداخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای خوش بتو ایام ما بر دفتر تو نام ما</p></div>
<div class="m2"><p>مدح تو اندر کام ما ذوق شراب انداخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاقانی دل‌سوخته با جور توست آموخته</p></div>
<div class="m2"><p>در دل عنا افروخته، جان در عذاب انداخته</p></div></div>