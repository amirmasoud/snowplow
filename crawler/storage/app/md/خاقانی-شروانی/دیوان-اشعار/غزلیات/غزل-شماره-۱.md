---
title: >-
    غزل شمارهٔ ۱
---
# غزل شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای آتشِ سودایِ تو خون کرده جگرها</p></div>
<div class="m2"><p>بر باد شده در سرِ سودایِ تو سرها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گلشنِ امّید به شاخِ شجرِ من</p></div>
<div class="m2"><p>گل‌ها نشکفتند و برآمد نه ثمرها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای در سرِ عشّاق ز شورِ تو شغب‌ها</p></div>
<div class="m2"><p>وی در دلِ زهّاد ز سوزِ تو اثرها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آلوده به خونابهٔ هجرِ تو روان‌ها</p></div>
<div class="m2"><p>پالوده ز اندیشهٔ وصلِ تو جگرها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وی مهرهٔ امّیدِ مرا زخمِ زمانه</p></div>
<div class="m2"><p>در ششدرِ عشقِ تو فروبسته گذرها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کردم خطر و بر سرِ کویِ تو گذشتم</p></div>
<div class="m2"><p>بسیار کند عاشق ازین گونه خطرها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاقانی از آن‌گه که خبر یافت ز عشقت</p></div>
<div class="m2"><p>از بی‌خبری او به جهان رفت خبرها</p></div></div>