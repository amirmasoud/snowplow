---
title: >-
    غزل شمارهٔ ۴۰۰
---
# غزل شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>لاله رخا سمن برا سرو روان کیستی</p></div>
<div class="m2"><p>سنگ‌دلا، ستم‌گرا، آفت جان کیستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیر قدی کمان کشی زهره رخی و مهوشی</p></div>
<div class="m2"><p>جانت فدا که بس خوشی جان و جهان کیستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گل سرخ رسته‌ای نرگس دسته بسته‌ای</p></div>
<div class="m2"><p>نرخ شکر شکسته‌ای پسته دهان کیستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای تو به دلبری سمر، شیفتهٔ رخت قمر</p></div>
<div class="m2"><p>بسته به کوه بر کمر، موی میان کیستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دام نهاده می‌روی مست ز باده می‌روی</p></div>
<div class="m2"><p>مشت گشاده می‌روی سخت کمان کیستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهد و شکر لبان تو جمله جهان از آن تو</p></div>
<div class="m2"><p>در عجبم به جان تو تاخود از آن کیستی</p></div></div>