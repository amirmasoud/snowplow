---
title: >-
    غزل شمارهٔ ۲۵۶
---
# غزل شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>من در طلب یارم ز اغیار نیندیشم</p></div>
<div class="m2"><p>پایم به سر گنج است از مار نیندیشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبرم به عیار او هیچ است و دو جو کمتر</p></div>
<div class="m2"><p>من هم جو زرینم کز نار نیندیشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوجو شدم از عشقش او جو به جو این داند</p></div>
<div class="m2"><p>او را به جوی زین غم غم‌خوار نیندیشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر زان رخ گندم‌گون اندک نظری یابم</p></div>
<div class="m2"><p>زین جان که جوی ارزد بسیار نیندیشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکی دل من خون شد ور خون من اندیشد</p></div>
<div class="m2"><p>اندیشم از آزارش ز آزار نیندیشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر هیچ رسد بر دل دندان سگ کویش</p></div>
<div class="m2"><p>تشریف سر دندان هر بار نیندیشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور جان ز بن دندان در عرض لبش آرم</p></div>
<div class="m2"><p>هم پیش‌کشی دانم بازار نیندیشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کار من از عشقش با شحنه و دار افتد</p></div>
<div class="m2"><p>از شحنه نترسم من وز دار نیندیشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر با سر تیغ افتد کار سر خاقانی</p></div>
<div class="m2"><p>بر تیغ سر اندازم وز کار نیندیشم</p></div></div>