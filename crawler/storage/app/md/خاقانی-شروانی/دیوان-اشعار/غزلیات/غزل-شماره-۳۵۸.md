---
title: >-
    غزل شمارهٔ ۳۵۸
---
# غزل شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>دلم که مرغ تو آمد به دام باز گرفتی</p></div>
<div class="m2"><p>نه خاک تو شدم از من چه گام باز گرفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا به نیم کرشمه تمام کشتی و آنگه</p></div>
<div class="m2"><p>نظر ز کام دل من تمام باز گرفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سه بوسه خواستم از تو ز من دو اسبه برفتی</p></div>
<div class="m2"><p>چو وقت خون من آمد لگام باز گرفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مترس ماه نگیرد، گرم نمائی یاری</p></div>
<div class="m2"><p>خبر فرستی اگرچه سلام باز گرفتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال تو ز تو طیره خجل خجل به من آمد</p></div>
<div class="m2"><p>ز شرم آنکه ز کویم خرام باز گرفتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا خیال تو بالله که غم‌گسارتر از توست</p></div>
<div class="m2"><p>خیال باز مگیر ار پیام باز گرفتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلی است بر تو مرا وام و جان وظیفه بر آن لب</p></div>
<div class="m2"><p>وظیفه چشم چه دارم که وام باز گرفتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شگرف عاشق خاقانیم تو نام نهادی</p></div>
<div class="m2"><p>ز من چه ننگ رسیدت که نام بازگرفتی</p></div></div>