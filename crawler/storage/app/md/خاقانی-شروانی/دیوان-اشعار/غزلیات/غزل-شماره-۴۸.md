---
title: >-
    غزل شمارهٔ ۴۸
---
# غزل شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>از حال خود شکسته دلان را خبر فرست</p></div>
<div class="m2"><p>تسکین جان سوختگان یک نظر فرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان در تب است از آن شکرستان لعل خویش</p></div>
<div class="m2"><p>از بهر تب بریدن جان نیشکر فرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم به دل که تحفهٔ آن بارگاه انس</p></div>
<div class="m2"><p>گر زر خشک نیست سخن‌های تر فرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بودم در این حدیث که آمد خیال تو</p></div>
<div class="m2"><p>کای خواجه ما سخن نشناسیم زر فرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الماس و زهر بر سر مژگان چو داشتی</p></div>
<div class="m2"><p>این سوی دل روان کن و آن زی جگر فرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر خواستی ز من هم ازین پای باز گرد</p></div>
<div class="m2"><p>شمشیر و طشت راست کن و سوی سر فرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاقانیا سپاه غم آمد دو منزلی</p></div>
<div class="m2"><p>جان را دو اسبه خیز به خدمت به در فرست</p></div></div>