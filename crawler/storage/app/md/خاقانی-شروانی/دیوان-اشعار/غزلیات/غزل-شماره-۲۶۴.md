---
title: >-
    غزل شمارهٔ ۲۶۴
---
# غزل شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>تا من پی آن زلف سرافکنده همی دارم</p></div>
<div class="m2"><p>چون شمع گهی گریه و گه خنده همی دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه لوح وصالش را سربسته همی خوانم</p></div>
<div class="m2"><p>گه پاس خیالش را شب زنده همی دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلطان جمال است او من بر در ایوانش</p></div>
<div class="m2"><p>تن خاک همی سازم جان بنده همی دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کرد مرا بسته بادام دو چشم او</p></div>
<div class="m2"><p>چون پسته دل از حسرت آکنده همی دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان تحفهٔ او کردم هم نیست سزای او</p></div>
<div class="m2"><p>زین روی سر از خجلت افکنده همی دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر حال گذشتهٔ ما هرگز نکنی حسرت</p></div>
<div class="m2"><p>امید به الطافش آینده همی دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از مصحف عشق او فال دل خاقانی</p></div>
<div class="m2"><p>گر خود به هلاک آید فرخنده همی دارم</p></div></div>