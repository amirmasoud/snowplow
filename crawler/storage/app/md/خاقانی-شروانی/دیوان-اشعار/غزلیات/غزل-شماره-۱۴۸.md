---
title: >-
    غزل شمارهٔ ۱۴۸
---
# غزل شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>بر دل غم فراقت آسان چگونه باشد</p></div>
<div class="m2"><p>دل را قیامت آمد شادان چگونه باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو کامران حسنی از خود قیاس میکن</p></div>
<div class="m2"><p>آن کو اسیر هجر است آسان چگونه باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیغام داده بودی گفتی که چونی از غم</p></div>
<div class="m2"><p>آن کز تو دور ماند می‌دان چگونه باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر لحظه چون گوزنان هوئی برآرم از جان</p></div>
<div class="m2"><p>سگ‌جانم ارنه چندین هجران چگونه باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نالندهٔ فراقم وز من طبیب عاجز</p></div>
<div class="m2"><p>درماندهٔ اجل را درمان چگونه باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهم که راز عشقت پنهان کنم ز یاران</p></div>
<div class="m2"><p>صحرای آب و آتش پنهان چگونه باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش پیام و نامه‌ات بر خاک باز غلطم</p></div>
<div class="m2"><p>در خون و خاک صیدی غلطان چگونه باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نامه به موی بندی وز اشک مهر سازی</p></div>
<div class="m2"><p>در مهر تر نگوئی عنوان چگونه باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر موی بند نامه‌ات طوفات گریست چشمم</p></div>
<div class="m2"><p>چندین به گرد موئی طوفان چگونه باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاقانی است و آهی صد جا شکسته دربر</p></div>
<div class="m2"><p>یارب که من چنینم جانان چگونه باشد</p></div></div>