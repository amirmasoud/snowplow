---
title: >-
    غزل شمارهٔ ۲۷۱
---
# غزل شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>از گلستان وصل نسیمی شنیده‌ام</p></div>
<div class="m2"><p>دامن گرفته بر اثر آن دویده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌بدرقه به کوی وصالش گذشته‌ام</p></div>
<div class="m2"><p>بی‌واسطه به حضرت خاصش رسیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینجا گذاشته پر و بالی که داشته</p></div>
<div class="m2"><p>آنجا که اوست هم به پر او پریده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این مرغ آشیان ازل را به تیغ عشق</p></div>
<div class="m2"><p>پیش سرای پردهٔ او سر بریده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وین مرکب سرای بقا را به رغم خصم</p></div>
<div class="m2"><p>جل درکشیده پیش در او کشیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاهی لبش گزیده و گاهی به یاد او</p></div>
<div class="m2"><p>آن می که وعده کرد ز دستش مزیده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود نام من ز خاطر من رفته بود پاک</p></div>
<div class="m2"><p>خاقانی آن زمان ز زبانش شنیده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جمله دیدم آنچه ز عشاق کس ندید</p></div>
<div class="m2"><p>اما دریغ چیست که در خواب دیده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوئی که بر جنیبت وهم از ره خیال</p></div>
<div class="m2"><p>در باغ فضل صدر افاضل چریده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>والا جمال دین محمد، محمد آنک</p></div>
<div class="m2"><p>از کل کون خدمت او برگزیده‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جبریل‌وار باد معانی به فر او</p></div>
<div class="m2"><p>در آستین مریم خاطر دمیده‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شک نیست کز سلالهٔ نثر بلند اوست</p></div>
<div class="m2"><p>این روی تازگان که به نظم آفریده‌ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای آنکه تا عنان به هوای تو داده‌ام</p></div>
<div class="m2"><p>از ناوک سخن صف خصمان دریده‌ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هود هدی توئی و من از تو چو صرصری</p></div>
<div class="m2"><p>بر عادیان جهل به عادت بزیده‌ام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آزرده‌ام ز زخم سگ غرچه لاجرم</p></div>
<div class="m2"><p>خط فراق بر خط شروان کشیده‌ام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لیکن بدان دیار نیابم ز ترس آنک</p></div>
<div class="m2"><p>پرآبهاست در ره و من سگ گزیده‌ام</p></div></div>