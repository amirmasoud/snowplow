---
title: >-
    غزل شمارهٔ ۱۱۳
---
# غزل شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>دل که در دام تو افتاد غم جان نبرد</p></div>
<div class="m2"><p>جان که در زلف تو شد راه به ایمان نبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل کو غاشیهٔ عشق تو بر دوش گرفت</p></div>
<div class="m2"><p>گر همه باد شود تخت سلیمان نبرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باد کو خاک کف پای تو را بوسه دهد</p></div>
<div class="m2"><p>سر فرو نارد تا افسر سلطان نبرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه هستند به فردوس بسی خاتونان</p></div>
<div class="m2"><p>تا تو را بیند رضوان غم ایشان نبرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در میان دل و دین حاصل عشاق تو چیست</p></div>
<div class="m2"><p>که چو حکم تو درآید ز میان آن نبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آهوی غمزهٔ تو دم نزند تا به فریب</p></div>
<div class="m2"><p>مهرهٔ صابری از بازوی شیران نبرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشک آن طایفه طوفان دگر گشت ولیک</p></div>
<div class="m2"><p>عشق نوح است که اندیشهٔ طوفان نبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر خسی وصل تو نایافته گر لاف زند</p></div>
<div class="m2"><p>با تو زان لاف زدن گوی ز میدان نبرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غول بر خویشتن از خضر نهد نام چه سود</p></div>
<div class="m2"><p>که خدایش به سرچشمهٔ حیوان نبرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست در حضرت زلف تو مرا باک رقیب</p></div>
<div class="m2"><p>خاصهٔ خلوت شه طاعت دربان نبرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو به حمد الله چون بر سر پیمان منی</p></div>
<div class="m2"><p>کس دگر کار مرا از سر و سامان نبرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمعی از قهر قضا فرقت ما می‌خواهند</p></div>
<div class="m2"><p>هان و هان تات قضا از سر پیمان نبرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان خاقانی کز ملک وصالت شاد است</p></div>
<div class="m2"><p>به جوی پاک همه ملکت خاقان نبرد</p></div></div>