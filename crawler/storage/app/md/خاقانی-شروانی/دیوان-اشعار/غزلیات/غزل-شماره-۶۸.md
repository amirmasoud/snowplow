---
title: >-
    غزل شمارهٔ ۶۸
---
# غزل شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>ای دل به عشق بر تو که عشقت چه درخور است</p></div>
<div class="m2"><p>در سر شدی ندانمت ای دل چه در سر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد کهنت بود برآورد روزگار</p></div>
<div class="m2"><p>این درد تازه روی نگوئی چه نوبر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهری غریب دشمن و یاری غریب حسن</p></div>
<div class="m2"><p>اینجا چه جای غم‌زدگان قلندر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم مورز عشق بتان گرچه جور عشق</p></div>
<div class="m2"><p>انصاف می‌دهم که ز انصاف خوش‌تر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اینجا و در دمشق ترازوی عاشقی است</p></div>
<div class="m2"><p>لاف از دمشق بس که ترازوت بی‌زر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اکنون که دیدی آن سر زنجیر مشک پاش</p></div>
<div class="m2"><p>زنجیر می‌گسل که خرد حلقه بر در است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوجو شدی برابر آن مشک و طرفه نیست</p></div>
<div class="m2"><p>هرجا که مشک بینی جوجو برابر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کس دیت مخواه که خون‌ریز تو تویی</p></div>
<div class="m2"><p>نقب از برون مجوی که دزد اندرون در است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاقانی است و چند هزار آرزوی دل</p></div>
<div class="m2"><p>دل را چه جای عشق و چه پروای دلبر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیچاره زاغ را که سیاه است جمله تن</p></div>
<div class="m2"><p>از جمله تن سپیدی چشمش چه درخور است</p></div></div>