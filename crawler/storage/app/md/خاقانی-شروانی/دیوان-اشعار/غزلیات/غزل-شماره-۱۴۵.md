---
title: >-
    غزل شمارهٔ ۱۴۵
---
# غزل شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>دل دادم و کار برنیامد</p></div>
<div class="m2"><p>کام از لب یار برنیامد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با او سخن از کنار گفتم</p></div>
<div class="m2"><p>در خط شد و کار برنیامد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل گفت حدیث بوسه میکن</p></div>
<div class="m2"><p>اکنون که کنار برنیامد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در معنی بوسهٔ تهی هم</p></div>
<div class="m2"><p>گفتم دو سه بار برنیامد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس کردم ازین سخن که چندان</p></div>
<div class="m2"><p>نقدی به عیار برنیامد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هرکه به کوی او فروشد</p></div>
<div class="m2"><p>جز من به شمار برنیامد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در راه غمش دواسبه راندم</p></div>
<div class="m2"><p>یک ذره غبار برنیامد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مقصود نیافت هر که در عشق</p></div>
<div class="m2"><p>خاقانی وار بر نیامد</p></div></div>