---
title: >-
    غزل شمارهٔ ۱۹۱
---
# غزل شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>خون‌ریزی و نندیشی، عیار چنین خوش‌تر</p></div>
<div class="m2"><p>دل دزدی و نگریزی، طرار چنین خوشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان غمزهٔ دود افکن آتش فکنی در من</p></div>
<div class="m2"><p>هم دل شکنی هم تن، دل‌دار چنین خوش‌تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر روز به هشیاری نو نو دلم آزاری</p></div>
<div class="m2"><p>مست آیی و عذر آری، آزار چنین خوش‌تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوری و نهان از من، حوری و رمان از من</p></div>
<div class="m2"><p>بوس از تو و جان از من، بازار چنین خوش‌تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الحق جگرم خوردی خون‌ریز دلم کردی</p></div>
<div class="m2"><p>موئیم نیازردی، پیکار چنین خوش‌تر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغی عجب استادم در دام تو افتادم</p></div>
<div class="m2"><p>غم می‌خورم و شادم غم‌خوار چنین خوش‌تر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من کشته دلم بالله تو عیسی و جان درده</p></div>
<div class="m2"><p>هم عاشق ازینسان به هم یار چنین خوش‌تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این زنده منم بی‌تو، گم باد تنم بی‌تو</p></div>
<div class="m2"><p>کز زیستنم بی‌تو بسیار چنین خوش‌تر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاقانی جان افشان بر خاک در جانان</p></div>
<div class="m2"><p>کز عاشق صوفی جان ایثار چنین خوش‌تر</p></div></div>