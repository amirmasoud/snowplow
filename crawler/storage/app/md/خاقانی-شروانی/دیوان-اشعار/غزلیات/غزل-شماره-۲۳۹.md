---
title: >-
    غزل شمارهٔ ۲۳۹
---
# غزل شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>از تف دل آتشین دهانم</p></div>
<div class="m2"><p>زان نام تو بر زبان نرانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که چو صبر از غم تو</p></div>
<div class="m2"><p>نام تو بسوزد از زبانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریاد کز آتش دل من</p></div>
<div class="m2"><p>فریاد بسوخت در دهانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بالای سر ایستاد روزم</p></div>
<div class="m2"><p>در پستی غم فتاد جانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشتی خاکم سبکتر از باد</p></div>
<div class="m2"><p>هم کشتی آهن گرانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر آهن نیستی تف آه</p></div>
<div class="m2"><p>با خود بردی بر آسمانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ریمهن ز بند آهن</p></div>
<div class="m2"><p>پالودهٔ سوخته روانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب‌تشنه‌ترم ز سگ گزیده</p></div>
<div class="m2"><p>از دست کس آب چون ستانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز کوی کس آب چون توان خواست</p></div>
<div class="m2"><p>کآتش ندهند رایگانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دور از تو ز بی‌تنی که هستم</p></div>
<div class="m2"><p>چون وصل تو هست بی‌نشانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مجهول کسی نیم، شناسند</p></div>
<div class="m2"><p>من شاعر صاحب القرانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از من اثری نماند ماناک</p></div>
<div class="m2"><p>خاقانی دیگرم، نه آنم</p></div></div>