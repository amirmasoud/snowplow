---
title: >-
    غزل شمارهٔ ۲۹
---
# غزل شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>زآتش اندیشه جانم سوخته است</p></div>
<div class="m2"><p>وز تف یارب دهانم سوخته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فلک در سینهٔ من آتشی است</p></div>
<div class="m2"><p>کز سر دل تا میانم سوخته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوز غمها کار من کرده است خام</p></div>
<div class="m2"><p>خامی گردون روانم سوخته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شعله‌های آه من در پیش خلق</p></div>
<div class="m2"><p>پردهٔ راز نهانم سوخته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دولتی جستم، وبالم آمده است</p></div>
<div class="m2"><p>آتشی گفتم، زبانم سوخته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده‌ای آتش که چون سوزد پرند</p></div>
<div class="m2"><p>برق محنت همچنانم سوخته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعر من زان سوزناک آمد که غم</p></div>
<div class="m2"><p>خاطر گوهر فشانم سوخته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سخن من نایب خاقانیم</p></div>
<div class="m2"><p>آسمان زین رشک جانم سوخته است</p></div></div>