---
title: >-
    غزل شمارهٔ ۹۲
---
# غزل شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>صد یک حسن تو نوبهار ندارد</p></div>
<div class="m2"><p>طاقت جور تو روزگار ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق تو گر برقرار کار بماند</p></div>
<div class="m2"><p>کار جهان تا ابد قرار ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیغ جفا در نیام کن که زمانه</p></div>
<div class="m2"><p>مرد نبرد چو تو سوار ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر تو مرا اختیار نیست که شرط است</p></div>
<div class="m2"><p>کانکه تو را دارد اختیار ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو نشاید گریخت خاصه در این دور</p></div>
<div class="m2"><p>مردم آزاده زینهار ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه غم عشق توست ناگزرانش</p></div>
<div class="m2"><p>عذر چه آرد که غم‌گسار ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوی تو دانم حدیث بوسه نگویم</p></div>
<div class="m2"><p>مار گزیده قوام مار ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دل خاقانی از سلامت بس کن</p></div>
<div class="m2"><p>عشق و سلامت بهم شمار ندارد</p></div></div>