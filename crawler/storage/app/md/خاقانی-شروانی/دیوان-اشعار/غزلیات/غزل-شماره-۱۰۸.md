---
title: >-
    غزل شمارهٔ ۱۰۸
---
# غزل شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>با یاد تو زهر بر شکر خندد</p></div>
<div class="m2"><p>با روی تو شام بر سحر خندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درماه نو از چه روی می‌خندی</p></div>
<div class="m2"><p>کان روی به آفتاب برخندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق همه زهر خندد از عشقت</p></div>
<div class="m2"><p>گر عشق این است ازین بتر خندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنجا که تو تیر غمزه اندازی</p></div>
<div class="m2"><p>آفاق بر آهنین سپر خندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و آنجا که من از جگر کشم آهی</p></div>
<div class="m2"><p>عشاق بر آتش سقر خندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من در غم تو عقیق می‌گریم</p></div>
<div class="m2"><p>دانم که عقیق تو شکر خندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون لعل تو بیند اشک خاقانی</p></div>
<div class="m2"><p>از شرم چو گل به پوست درخندد</p></div></div>