---
title: >-
    غزل شمارهٔ ۱۳۲
---
# غزل شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>چه روح افزا و راحت باری ای باد</p></div>
<div class="m2"><p>چه شادی بخش و غم برداری ای باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کبوتروارم آری نامهٔ دوست</p></div>
<div class="m2"><p>که پیک نازنین رفتاری ای باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پیوند تو دارم چشم روشن</p></div>
<div class="m2"><p>که بوی یوسف من داری ای باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سوسن بوی و توسن خوی ترکم</p></div>
<div class="m2"><p>پیام راز من بگزاری ای باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگوئی حال و باز آری جوابم</p></div>
<div class="m2"><p>که خاموش روان گفتاری ای باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خاک پای او کز خاک پایش</p></div>
<div class="m2"><p>سرم را سرمهٔ چشم آری ای باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به زلف او که یک موی از دو زلفش</p></div>
<div class="m2"><p>بدزدی و به من بسپاری ای باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من از زلفش سخن راندن نیارم</p></div>
<div class="m2"><p>تو بر زلفش زدن چون یاری ای باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلم زنهاری است آنجا، در آن کوش</p></div>
<div class="m2"><p>که باز آری دل زنهاری ای باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر او نگذارد آوردن دلم را</p></div>
<div class="m2"><p>درو آویزی و نگذاری ای باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان پنهانی و پیداست سحرت</p></div>
<div class="m2"><p>که خاقانی توئی پنداری ای باد</p></div></div>