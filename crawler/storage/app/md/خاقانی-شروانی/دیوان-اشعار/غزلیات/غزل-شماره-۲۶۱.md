---
title: >-
    غزل شمارهٔ ۲۶۱
---
# غزل شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>نیم شب پی گم کنان در کوی جانان آمدم</p></div>
<div class="m2"><p>همچو جان بی‌سایه و چون سایه بی‌جان آمدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سگان دوست هم پیش سگان کوی دوست</p></div>
<div class="m2"><p>داغ بر رخ، طوق بر گردن خروشان آمدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوی او جان را شبستان بود زحمت برنتافت</p></div>
<div class="m2"><p>سایه بر در ماند چون من در شبستان آمدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش رخسار او دیدم سپند او شدم</p></div>
<div class="m2"><p>بی‌من از من نعره سر برزد پشیمان آمدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چراغ آسان نشاید بر سر گنج آمدن</p></div>
<div class="m2"><p>من چراغ آه چون بنشاندم آسان آمدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوزن مژگانش از دیبای رخسارش مرا</p></div>
<div class="m2"><p>خلعتی نو دوخت کو را دوش مهمان آمدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوست جام می کشید و جرعه‌ها بر من فشاند</p></div>
<div class="m2"><p>خاک او بودم سزای جرعه‌ها زان آمدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از حسودانش نیندیشم که دارم وصل او</p></div>
<div class="m2"><p>باک غوغاکی برم چون خاص سلطان آمدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شام‌گه زین سرنه عاشق، کستان بوسی شدم</p></div>
<div class="m2"><p>صبح‌دم زان سر نه خاقانی، که خاقان آمدم</p></div></div>