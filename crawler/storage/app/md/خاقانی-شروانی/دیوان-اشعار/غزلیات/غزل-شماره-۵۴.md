---
title: >-
    غزل شمارهٔ ۵۴
---
# غزل شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>شمع شب‌ها به جز خیال تو نیست</p></div>
<div class="m2"><p>باغ جان‌ها به جز جمال تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو که خورشید عشق را همه روز</p></div>
<div class="m2"><p>طالعی به ز اتصال تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شو که سلطان فتنه را همه سال</p></div>
<div class="m2"><p>سپهی به ز زلف و خال تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخش شوخی مران که عالم را</p></div>
<div class="m2"><p>طاقت ضربت دوال تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سغبهٔ وعدهٔ محال توام</p></div>
<div class="m2"><p>کیست کو سغبهٔ محال تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه روز ار ز روی تو دورست</p></div>
<div class="m2"><p>همه شب خالی از خیال تو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آرزوها که داشت خاقانی</p></div>
<div class="m2"><p>هیچ و همی به جز وصال تو نیست</p></div></div>