---
title: >-
    غزل شمارهٔ ۲۵۷
---
# غزل شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>دل را به غم تو باز بستیم</p></div>
<div class="m2"><p>جان را کمر نیاز بستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن کو سگ توست هم به کویت</p></div>
<div class="m2"><p>بر شاخ گلش به ناز بستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دل به دلت رسول کردیم</p></div>
<div class="m2"><p>وز دیده زبان راز بستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدیم رخت که قبلهٔ ماست</p></div>
<div class="m2"><p>زآنسو که توئی نماز بستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خونین تتق از پی خیالت</p></div>
<div class="m2"><p>بر چشم خیال باز بستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر بوی خیال زود سیرت</p></div>
<div class="m2"><p>خواب شب دیر باز بستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان از پی گرد موکب تو</p></div>
<div class="m2"><p>بر شه ره ترکتاز بستیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرغی که کبوتر هوائی است</p></div>
<div class="m2"><p>بر گوشهٔ دام باز بستیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جوری که ز غمزهٔ تو دیدیم</p></div>
<div class="m2"><p>بر عالم کینه ساز بستیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاقانی‌وار لاشهٔ عمر</p></div>
<div class="m2"><p>بر آخور حرص و آز بستیم</p></div></div>