---
title: >-
    غزل شمارهٔ ۳۴
---
# غزل شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>طره مفشان که غرامت بر ماست</p></div>
<div class="m2"><p>طیره منشین که قیامت برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزه بر کشتن من تیز مکن</p></div>
<div class="m2"><p>کان نه غمزه است که شمشیر قضاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که از خصم توام بیم سر است</p></div>
<div class="m2"><p>بر سر این همه خشم تو چراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر عتابی ز سر ناز برفت</p></div>
<div class="m2"><p>مرو از جای که صحبت برجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت بیهوده بر انگشت مپیچ</p></div>
<div class="m2"><p>بر کسی کو به تو انگشت نماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ بد در تو نگفتم بالله</p></div>
<div class="m2"><p>خود خیال تو بر این گفته گواست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این قدر گفتم کان روی چو گل</p></div>
<div class="m2"><p>بستهٔ دیدهٔ هر خس نه رواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من همانم تو همان باش به مهر</p></div>
<div class="m2"><p>که همه شهر حدیث تو و ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنده خاقانی اگر کرد گناه</p></div>
<div class="m2"><p>عذر آن کرده به جان خواهد خواست</p></div></div>