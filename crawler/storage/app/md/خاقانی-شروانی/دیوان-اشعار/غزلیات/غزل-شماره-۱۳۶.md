---
title: >-
    غزل شمارهٔ ۱۳۶
---
# غزل شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>فراقت ز خون‌ریز من در نماند</p></div>
<div class="m2"><p>سر کویت از لاف زن در نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ار باشم ار نه سگ آستانت</p></div>
<div class="m2"><p>ز هندوی گژمژ سخن درنماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو گر خواهی و گرنه میدان عشقت</p></div>
<div class="m2"><p>ز رندان لشکر شکن درنماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آویزش زلفت آویخت جانم</p></div>
<div class="m2"><p>که صید از نگون‌سر شدن درنماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل از هشت باغ رخت درنیاید</p></div>
<div class="m2"><p>هم از چار دیوار تن درنماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخت را به پیوند چشمم چه حاجت</p></div>
<div class="m2"><p>که شمع بهشت از لگن درنماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خون چو من خاکیی دست درکش</p></div>
<div class="m2"><p>که هجران خود از کار من درنماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو در بیشهٔ روزگار افتد آتش</p></div>
<div class="m2"><p>چو من مرغی از بابزندر نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم دل مخور کو غم تو ندارد</p></div>
<div class="m2"><p>دل از روزی خویشتن درنماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خون ریز خاقانی اندیشه کم کن</p></div>
<div class="m2"><p>که ایام ازین انجمن درنماند</p></div></div>