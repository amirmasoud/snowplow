---
title: >-
    غزل شمارهٔ ۳۱۲
---
# غزل شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>ای دل به جفات جان نهاده</p></div>
<div class="m2"><p>جان پیش‌کشت جهان نهاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهری همه ز آهنین دل تو</p></div>
<div class="m2"><p>قفلی زده بر دهان نهاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر طرف لب تو جان عیسی</p></div>
<div class="m2"><p>از نیل و بقم دکان نهاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از کوی سوار چون برآئی</p></div>
<div class="m2"><p>شب‌پوش بر ابروان نهاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترکان کمین غمزهٔ تو</p></div>
<div class="m2"><p>یاسج همه بر کمان نهاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو عاشق صید و تیغ بر کف</p></div>
<div class="m2"><p>عشاق تو دل بر آن نهاده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من پیش تو بر زمین نهم سر</p></div>
<div class="m2"><p>کای پای بر آسمان نهاده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسب از در من مران و مگذر</p></div>
<div class="m2"><p>هان نعل بهات جان نهاده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاقانی را در آتش عشق</p></div>
<div class="m2"><p>نعل هوس از نهان نهاده</p></div></div>