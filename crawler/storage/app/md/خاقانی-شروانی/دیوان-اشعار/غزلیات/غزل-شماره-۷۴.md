---
title: >-
    غزل شمارهٔ ۷۴
---
# غزل شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>سر و زر کو که منت یارم جست</p></div>
<div class="m2"><p>فرصت آمدنت یارم جست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بن مویی ز دلم کم نشود</p></div>
<div class="m2"><p>سر موئی ز تنت یارم جست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه میی از قدحت یارم خواست</p></div>
<div class="m2"><p>نه گلی از چمنت یارم جست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه من آیم نه توام دانی خواند</p></div>
<div class="m2"><p>نه تو آئی نه منت یارم جست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گم شد از من دل من چون دهنت</p></div>
<div class="m2"><p>نه دلم نه دهنت یارم جست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون کنم قصه لبت کشت مرا</p></div>
<div class="m2"><p>که قصاص از سخنت یارم جست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم شوم زنده چو تخم قز اگر</p></div>
<div class="m2"><p>جای در پیرهنت یارم جست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر تو نظاره هزار انجمن است</p></div>
<div class="m2"><p>از کدام انجمنت یارم جست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من کیم کز شکر و پستهٔ تو</p></div>
<div class="m2"><p>بوس فندق‌شکنت یارم جست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وطنت در دل خاقانی باد</p></div>
<div class="m2"><p>تا مگر زان وطنت یارم جست</p></div></div>