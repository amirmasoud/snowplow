---
title: >-
    غزل شمارهٔ ۲۵۹
---
# غزل شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>یارب از عشق چه سرمستم و بی‌خویشتنم</p></div>
<div class="m2"><p>دست گیریدم تا دست به زلفش نزنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به میدان رود آن بت مگذارید دمی</p></div>
<div class="m2"><p>بو که هشیار شوم برگ نثاری بکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگذارم که جهانی به جمالش نگرند</p></div>
<div class="m2"><p>شوم از خون جگر پرده به پیشش بتنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا مرا بر در میخانهٔ آن ماه برید</p></div>
<div class="m2"><p>که خمار من از آنجاست هم آنجا شکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صورت من همه او شد صفت من همه او</p></div>
<div class="m2"><p>لاجرم کس من و ما نشنود اندر سخنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نزنم هیچ دری تام نگویند آن کیست</p></div>
<div class="m2"><p>چو بگویند مرا باید گفتن که منم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیم جان دارم و جان سایه ندارد به زمین</p></div>
<div class="m2"><p>من به جان می‌زیم و سایهٔ جان است تنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ضعیفی که تنم هست نهان گشته چنانک</p></div>
<div class="m2"><p>سال‌ها هست که در آرزوی خویشتنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر مرا پرسی و چیزی به تو آواز دهد</p></div>
<div class="m2"><p>آن نه خاقانی باشد، که بود پیرهنم</p></div></div>