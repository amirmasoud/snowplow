---
title: >-
    غزل شمارهٔ ۲۴۱
---
# غزل شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>نازی است تو را در سر، کمتر نکنی دانم</p></div>
<div class="m2"><p>دردی است مرا در دل، باور نکنی دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیره چه سراندازم بر خاک سر کویت</p></div>
<div class="m2"><p>گر بوسه زنم پایت، سر برنکنی دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی بدهم کامت اما نه بدین زودی</p></div>
<div class="m2"><p>عمری شد و زین وعده، کمتر نکنی دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسیم عطا کردی، زان کرده پشیمانی</p></div>
<div class="m2"><p>دانی که خطا کردی، دیگر نکنی دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر کشتنیم باری هم دست تو و تیغت</p></div>
<div class="m2"><p>خود دست به خون من، هم تر نکنی دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه‌گه زنی از شوخی حلقهٔ در خاقانی</p></div>
<div class="m2"><p>خانه همه خون بینی، سر درنکنی دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هان ای دل خاقانی سر در سر کارش کن</p></div>
<div class="m2"><p>الا هوس وصلش، در سر نکنی دانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه به عراق اندر سلطان سخن گشتی</p></div>
<div class="m2"><p>جز خاک در سلطان افسر نکنی دانم</p></div></div>