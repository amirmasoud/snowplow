---
title: >-
    غزل شمارهٔ ۳۶۸
---
# غزل شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>هدیهٔ پای تو زر بایستی</p></div>
<div class="m2"><p>رشوهٔ رای تو زر بایستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم عشقت طرب افزای من است</p></div>
<div class="m2"><p>طرب افزای تو زر بایستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان چه خاک است که پیش تو کشم</p></div>
<div class="m2"><p>پیشکش‌های تو زر بایستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده در پای تو گشتن هوس است</p></div>
<div class="m2"><p>کشته در پای تو زر بایستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد هم اجرای امروز تو جان</p></div>
<div class="m2"><p>خرج فردای تو زر بایستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترش روی است زر صفرا بر</p></div>
<div class="m2"><p>وقت صفرای تو زر بایستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش بسته گشاید همه کار</p></div>
<div class="m2"><p>کار پیرای تو زر بایستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌زری داشت تو را بر سر جنگ</p></div>
<div class="m2"><p>صلح فرمای تو زر بایستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوه سیمینی و هم سنگ توام</p></div>
<div class="m2"><p>در تمنای تو زر بایستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا کنم بر سر و بالات نثار</p></div>
<div class="m2"><p>هم به بالای تو زر بایستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دید سیمای مرا عشق تو گفت</p></div>
<div class="m2"><p>که چو سیمای تو زر بایستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل سودائی خاقانی را</p></div>
<div class="m2"><p>هم به سودای تو زر بایستی</p></div></div>