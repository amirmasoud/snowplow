---
title: >-
    غزل شمارهٔ ۳۳۵
---
# غزل شمارهٔ ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>گلی از باغ وفا آمده‌ای</p></div>
<div class="m2"><p>خود خس و خار نما آمده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا پای نهی گل روید</p></div>
<div class="m2"><p>تا ندانی ز کجا آمده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذرهٔ ذات تو خورشید لقاست</p></div>
<div class="m2"><p>بحری و قطره قضا آمده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایهٔ خار تو سروستان است</p></div>
<div class="m2"><p>خرمن نشو و نما آمده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نور آئینه به خود پنهان است</p></div>
<div class="m2"><p>قبلهٔ قبله نما آمده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی دلت تاب نگاهی دارد</p></div>
<div class="m2"><p>آفت آینه‌ها آمده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خار و گل نام خدا می‌گویند</p></div>
<div class="m2"><p>ای سهی قد ز کجا آمده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مستی و شوخی و عالم سوزی</p></div>
<div class="m2"><p>چه بگویم که چها آمده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بین که در باغ جهان خاقانی</p></div>
<div class="m2"><p>از پی کسب هوا آمده‌ای</p></div></div>