---
title: >-
    غزل شمارهٔ ۳۴۰
---
# غزل شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>تاطرف کلاه برشکستی</p></div>
<div class="m2"><p>قدر کله قمر شکستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حلق دلم فتاد زنجیر</p></div>
<div class="m2"><p>تا حلقهٔ زلف برشکستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان زلف شکسته عاشقان را</p></div>
<div class="m2"><p>صد کار به کار درشکستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد دل ما به بوسه بردی</p></div>
<div class="m2"><p>و آوازهٔ گل‌شکر شکستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلقهٔ در اختیار ما را</p></div>
<div class="m2"><p>چندان بزدی که درشکستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاقانی را ز غیرت عشق</p></div>
<div class="m2"><p>ناله همه در جگر شکستی</p></div></div>