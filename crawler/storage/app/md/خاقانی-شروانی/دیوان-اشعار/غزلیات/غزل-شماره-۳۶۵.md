---
title: >-
    غزل شمارهٔ ۳۶۵
---
# غزل شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>هرگز بود به شوخی چشم تو عبهری</p></div>
<div class="m2"><p>یا راست‌تر ز قد تو باشد صنوبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا داشت خوبتر ز تو معشوق، عاشقی</p></div>
<div class="m2"><p>یا زاد شوخ‌تر ز تو فرزند، مادری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بگذرم به کوی تو روزی هزار بار</p></div>
<div class="m2"><p>بینم نشسته بر سر کویت مجاوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا دست بر دلی ز تو یا پای در گلی</p></div>
<div class="m2"><p>یا باد در کفی ز تو یا خاک بر سری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردی ز بیدلی تو مرا در جهان سمر</p></div>
<div class="m2"><p>نی بی‌دلی است چون من و نی چون تو دلبری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی چون من است در همه عالم ستم‌کشی</p></div>
<div class="m2"><p>نی چون تو هست در همه گیتی ستم‌گری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پران شود ز زیر کله زاغ زلف تو</p></div>
<div class="m2"><p>تا بر پرد ز بر دل من چون کبوتری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان زلف عنبرینت رخم چنبری شود</p></div>
<div class="m2"><p>تا پشت من خمیده شود همچو چنبری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی چرا کشی سر زلف معنبرم</p></div>
<div class="m2"><p>گویم که سازمش ز دل خویش مجمری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوئی که شکر منت آید به آرزو</p></div>
<div class="m2"><p>گویم حدیث در دهنت باد شکری</p></div></div>