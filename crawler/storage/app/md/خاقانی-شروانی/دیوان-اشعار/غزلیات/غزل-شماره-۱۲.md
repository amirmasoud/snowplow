---
title: >-
    غزل شمارهٔ ۱۲
---
# غزل شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>درد زده است جان من میوهٔ جان من کجا</p></div>
<div class="m2"><p>درد مرا نشانه کرد درد نشان من کجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش ز چشم مردمان اشک به وام خواستم</p></div>
<div class="m2"><p>این همه اشک عاریه است اشک روان من کجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او ز من خراب دل کرد چو گنج پی نهان</p></div>
<div class="m2"><p>من که خرابه اندرم گنج نهان من کجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار ز من گسست و من بهر موافقت کنون</p></div>
<div class="m2"><p>بند روان گسسته‌ام انس روان من کجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه گهی آن شکرفشان سرکه فشان ز لب شدی</p></div>
<div class="m2"><p>گرم جگر شدم ز تب سرکه‌فشان من کجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز به روز بر فلک بخشش عافیت بود</p></div>
<div class="m2"><p>آن همه را رسیده بخش ای فلک آن من کجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نالهٔ خاقانی اگر دادستان شد از فلک</p></div>
<div class="m2"><p>نالهٔ من نبست غم دادستان من کجا</p></div></div>