---
title: >-
    غزل شمارهٔ ۴۰۱
---
# غزل شمارهٔ ۴۰۱

<div class="b" id="bn1"><div class="m1"><p>باز از نوای دلبری سازی دگرگون می‌زنی</p></div>
<div class="m2"><p>دیر است تا در پرده‌ای از پرده بیرون می‌زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا مهره وامالیده‌ای کژ باختن بگزیده‌ای</p></div>
<div class="m2"><p>نقشی که در کف دیده‌ای نه کم نه افزون می‌زنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه از دل پر خون من زین درد روز افزون من</p></div>
<div class="m2"><p>هر شب برای خون من رای شبیخون می‌زنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاقانی از چشم و زبان شد پیش تو گوهرفشان</p></div>
<div class="m2"><p>تو عمر او را هر زمان کیسه به صابون می‌زنی</p></div></div>