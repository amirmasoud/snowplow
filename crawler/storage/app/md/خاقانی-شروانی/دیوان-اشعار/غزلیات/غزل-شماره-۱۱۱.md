---
title: >-
    غزل شمارهٔ ۱۱۱
---
# غزل شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>پردهٔ نو ساخت عشق، زخمهٔ نو در فزود</p></div>
<div class="m2"><p>کرد به من آنچه خواست، برد ز من آنچه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لشکر عشق تو باز بر دل من ران گشاد</p></div>
<div class="m2"><p>گر همه در خون کشد، پشت نباید نمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ز کفم شد دریغ سود ندارد کنون</p></div>
<div class="m2"><p>سنگ پیاله شکست گربه نواله ربود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آتش هجران تو دود به مغزم رسید</p></div>
<div class="m2"><p>اشک ز چشمم گشاد مایهٔ اشک است دود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق چو یکسر بود هجران خوشتر ز وصل</p></div>
<div class="m2"><p>باده چو دردی بود دیر نکوتر که زود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتن من یاد کن، یاد دگر کس مکن</p></div>
<div class="m2"><p>گوش مرا مشنوان آنچه نیارم شنود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم سیاه تو دید دل ز برم برپرید</p></div>
<div class="m2"><p>فتنهٔ خاقانی است این دل کور کبود</p></div></div>