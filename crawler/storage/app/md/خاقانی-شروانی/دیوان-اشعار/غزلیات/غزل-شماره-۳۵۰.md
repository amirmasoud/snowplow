---
title: >-
    غزل شمارهٔ ۳۵۰
---
# غزل شمارهٔ ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>کاشکی جز تو کسی داشتمی</p></div>
<div class="m2"><p>یا به تو دسترسی داشتمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا در این غم که مرا هر دم هست</p></div>
<div class="m2"><p>هم‌دم خویش کسی داشتمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی غمم بودی اگر در غم تو</p></div>
<div class="m2"><p>نفسی، هم‌نفسی داشتمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر لبت آن منستی ز جهان</p></div>
<div class="m2"><p>کافرم گر هوسی داشتمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوان عیسی بر من وانگه من</p></div>
<div class="m2"><p>باک هر خرمگسی داشتمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر و زر ریختمی در پایت</p></div>
<div class="m2"><p>گر از این دست، بسی داشتمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرنه عشق تو بدی لعب فلک</p></div>
<div class="m2"><p>هر رخی را فرسی داشتمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرنه خاقانی خاک تو شدی</p></div>
<div class="m2"><p>کی جهان را به خسی داشتمی</p></div></div>