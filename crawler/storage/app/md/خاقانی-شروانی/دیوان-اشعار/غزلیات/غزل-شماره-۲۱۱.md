---
title: >-
    غزل شمارهٔ ۲۱۱
---
# غزل شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>از دو عالم دامن جان درکشم هر صبح‌دم</p></div>
<div class="m2"><p>پای نومیدی به دامان درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سایه با من هم‌نشین و ناله با من هم‌دم است</p></div>
<div class="m2"><p>جام غم بر روی ایشان درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقیی دارم چو اشک و مطربی دارم چو آه</p></div>
<div class="m2"><p>شاهد غم را ببر زان درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق مهمان دل است و جان و دل مهمان او</p></div>
<div class="m2"><p>من دل و جان پیش مهمان درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناگزیر جان بود جانان و از جان ناگزیر</p></div>
<div class="m2"><p>پیش جانان شاید ار جان درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم مژه مسمار سازم هم بهای نعل را</p></div>
<div class="m2"><p>دیده پیش اسب جانان درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که می‌جویم سواری بر سر میدان عقل</p></div>
<div class="m2"><p>تا عنان گیرم به میدان درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر شب از سلطان عشقم در ستکانی‌ها رسد</p></div>
<div class="m2"><p>تا به یاد روی سلطان در کشم هر صبح‌دم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوستکانی کان به مهر خاص سلطان آورند</p></div>
<div class="m2"><p>گر همه زهر است آسان درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نوش خندیدن به وقت زهر خوردن واجب است</p></div>
<div class="m2"><p>من بسا زهرا که خندان درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوستان خون رزان پنهان کشند از دور و من</p></div>
<div class="m2"><p>آشکارا خون مژگان درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر همه مستند از آن راوق منم هم مست از آنک</p></div>
<div class="m2"><p>خون چشم رواق افشان درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دهر ویران را به جز آرایش طاقی نماند</p></div>
<div class="m2"><p>خویشتن زین طاق ویران درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آفتم عقل است میل آتشین سازم ز آه</p></div>
<div class="m2"><p>پس به چشم عقل پنهان درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چند ازین دوران که هستند این خدا دوران در او</p></div>
<div class="m2"><p>شاید ار دامن ز دوران درکشم هر صبح‌دم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از خود و غیری چنان فارغ شدم کز فارغی</p></div>
<div class="m2"><p>خط به خاقانی و خاقان درکشم هر صبح‌دم</p></div></div>