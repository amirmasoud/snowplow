---
title: >-
    غزل شمارهٔ ۲۶۸
---
# غزل شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>ز باغ عافیت بوئی ندارم</p></div>
<div class="m2"><p>که دل گم گشت و دل‌جویی ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنالم کرزوبخشی ندیدم</p></div>
<div class="m2"><p>بگریم کشنارویی ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برانم بازوی خون از رگ چشم</p></div>
<div class="m2"><p>که با غم زور بازویی ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک پل بر دلم خواهد شکستن</p></div>
<div class="m2"><p>کز آب عافیت جویی ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسازم مجلسی از سایهٔ خویش</p></div>
<div class="m2"><p>که آنجا مجلس آشویی ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه پویم بر پی مردان عالم</p></div>
<div class="m2"><p>کز آن سر مرحباگویی ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر مویی مرا واخواست از کیست</p></div>
<div class="m2"><p>که اینجا محرم مویی ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر از حلوای هر خوان بی‌نصیبم</p></div>
<div class="m2"><p>نه سکبای هر ابرویی ندارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در این عالم که آب روی من رفت</p></div>
<div class="m2"><p>بدان عالم شدن رویی ندارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من آن زن فعلم از حیض خجالت</p></div>
<div class="m2"><p>که بکری دارم و شویی ندارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه خاقانی من است و من نه اویم</p></div>
<div class="m2"><p>که تاب درد چون اویی ندارم</p></div></div>