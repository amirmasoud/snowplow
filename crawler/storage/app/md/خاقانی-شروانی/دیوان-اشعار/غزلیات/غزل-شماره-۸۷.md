---
title: >-
    غزل شمارهٔ ۸۷
---
# غزل شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>طریق عشق رهبر برنتابد</p></div>
<div class="m2"><p>جفای دوست داور برنتابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عیاری توان رفتن ره عشق</p></div>
<div class="m2"><p>که این ره دامن تر برنتابد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوا چون شحنه شد بر عالم دل</p></div>
<div class="m2"><p>خراج از عقل کمتر برنتابد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سری را کاگهی دادند ازین سر</p></div>
<div class="m2"><p>گران‌باری افسر برنتابد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر معشوق داری سر درانداز</p></div>
<div class="m2"><p>که عاشق زحمت سر برنتابد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به وام از عشق جانی چند برگیر</p></div>
<div class="m2"><p>که یک جان ناز دلبر برنتابد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز کوی عشق خاقانی برون شو</p></div>
<div class="m2"><p>که او یار قلندر بر نتابد</p></div></div>