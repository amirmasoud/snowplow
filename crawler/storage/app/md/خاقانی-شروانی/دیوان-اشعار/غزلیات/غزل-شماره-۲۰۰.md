---
title: >-
    غزل شمارهٔ ۲۰۰
---
# غزل شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>ای دل آن زنار نگسستی هنوز</p></div>
<div class="m2"><p>رشتهٔ پندار نگسستی هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک هر پی خون توست از کوی یار</p></div>
<div class="m2"><p>پی ز کوی یار نگسستی هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سر کار هوا شد دین و دل</p></div>
<div class="m2"><p>هم نظر زان کار نگسستی هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن چو جان از دیده نادیدار ماند</p></div>
<div class="m2"><p>دیده ز آن دیدار نگسستی هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر بازار عشق آبت برفت</p></div>
<div class="m2"><p>پای ز آن بازار نگسستی هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاختی بر اسب همت سال‌ها</p></div>
<div class="m2"><p>تنگ آن رهوار نگسستی هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رشتهٔ جانت ز غم یک تار ماند</p></div>
<div class="m2"><p>شکر کن کان تار نگسستی هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لاف یک‌رنگی مزن خاقانیا</p></div>
<div class="m2"><p>کز میان زنار نگسستی هنوز</p></div></div>