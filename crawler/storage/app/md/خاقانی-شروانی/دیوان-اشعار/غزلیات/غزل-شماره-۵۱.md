---
title: >-
    غزل شمارهٔ ۵۱
---
# غزل شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>به باغ وصل تو خاری، رقیب صد ورد است</p></div>
<div class="m2"><p>به یاد روی تو دردی، طبیب صد درد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار جان مقدس فدای روی تو باد</p></div>
<div class="m2"><p>که زیر دامن زلف تو سایه پرورد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به روزگار هوای تو کم شود نی نی</p></div>
<div class="m2"><p>هوای تو عرضی نیست مادر آورد است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسول من سوی تو باد صبح‌دم باشد</p></div>
<div class="m2"><p>ازین قبل نفس باد صبح‌دم سرد است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپر به مهر فکندم گمان به کینه مکش</p></div>
<div class="m2"><p>به تیر غمزه بگو کو نه مرد ناورد است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دل اسیر هوای تو گشت خاقانی</p></div>
<div class="m2"><p>اگر به جان برهد هم سعادتی مرد است</p></div></div>