---
title: >-
    غزل شمارهٔ ۳۷۳
---
# غزل شمارهٔ ۳۷۳

<div class="b" id="bn1"><div class="m1"><p>مرا تا جان بود جانان تو باشی</p></div>
<div class="m2"><p>ز جان خوش‌تر چه باشد آن تو باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل دل هم تو بودی تا به امروز</p></div>
<div class="m2"><p>وزین پس نیز جان جان تو باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر زخمی مرا مرهم تو سازی</p></div>
<div class="m2"><p>به هر دردی مرا درمان تو باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بده فرمان به هر موجب که خواهی</p></div>
<div class="m2"><p>که تا باشم، مرا سلطان تو باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر گیرم شمار کفر و ایمان</p></div>
<div class="m2"><p>نخستین حرف سر دیوان تو باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دین و کفر مفریبم کز این پس</p></div>
<div class="m2"><p>مرا هم کفر و هم ایمان تو باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خاقانی مزن دم چون تو آئی</p></div>
<div class="m2"><p>چه خاقانی که خود خاقان تو باشی</p></div></div>