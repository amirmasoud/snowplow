---
title: >-
    غزل شمارهٔ ۲۳۵
---
# غزل شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>زنگ دل از آب روی شستیم</p></div>
<div class="m2"><p>وز درد هوا سبوی شستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را به کنار جوی بردیم</p></div>
<div class="m2"><p>وز یار کناره جوی شستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شهر شما دواسبه راندیم</p></div>
<div class="m2"><p>از خون سر چار سوی شستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان را به وداع آفرینش</p></div>
<div class="m2"><p>از عالم تنگ خوی شستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سجاده به هشت باغ بردیم</p></div>
<div class="m2"><p>دراعه به چار جوی شستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه قندز شب نه قاقم روز</p></div>
<div class="m2"><p>چون دست ز هر دو موی شستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی که دهان به هفت خاک آب</p></div>
<div class="m2"><p>از یاد خسان بشوی، شستیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی ز جهان نشسته‌ای دست</p></div>
<div class="m2"><p>در گوش جهان بگوی شستیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از زن صفتی به آب مردی</p></div>
<div class="m2"><p>حیض همه رنگ و بوی شستیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان نفس که آب روی جستی</p></div>
<div class="m2"><p>ما دست ز آبروی شستیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاقانی‌وار تختهٔ عمر</p></div>
<div class="m2"><p>از ابجد گفت‌گوی شستیم</p></div></div>