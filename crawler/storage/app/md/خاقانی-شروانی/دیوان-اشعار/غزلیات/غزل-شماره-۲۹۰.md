---
title: >-
    غزل شمارهٔ ۲۹۰
---
# غزل شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>در یک سخن آن همه عتیبش بین</p></div>
<div class="m2"><p>در یک نظر این همه فریبش بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید که ماه در عنان دارد</p></div>
<div class="m2"><p>چون سایه دویده در رکیبش بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاموشی لعل او چه می‌بینی</p></div>
<div class="m2"><p>جماشی چشم پر عتیبش بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چشم نظاره زو خبر ندهد</p></div>
<div class="m2"><p>هم نور جمال او حجیبش بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن عقل که برد نام بالایش</p></div>
<div class="m2"><p>سر چون سر خامه در نشیبش بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از درد جگر به شب ز هجرانش</p></div>
<div class="m2"><p>ای بر دل من همه نهیبش بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزی که حساب کشتگان گیرد</p></div>
<div class="m2"><p>خاقانی را در آن حسیبش بین</p></div></div>