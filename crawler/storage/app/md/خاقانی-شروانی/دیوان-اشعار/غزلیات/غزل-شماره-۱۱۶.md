---
title: >-
    غزل شمارهٔ ۱۱۶
---
# غزل شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>با کفر زلفت ای جان ایمان چه کار دارد</p></div>
<div class="m2"><p>آنجا که دردت آید درمان چه کار دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سحرا که کرده‌ای تو با زلف و عارض ارنه</p></div>
<div class="m2"><p>در گلشن ملایک شیطان چه کار دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بی‌نسیم وصلت تنها چه خاک بیزد</p></div>
<div class="m2"><p>جان در شکنج زلفت پنهان چه کار دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردی شگرف دارد دل در غم تو دایم</p></div>
<div class="m2"><p>در زلف تو ندانم تا جان چه کار دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تنگنای دیده وصلت کجا درآید</p></div>
<div class="m2"><p>در بنگه گدایان سلطان چه کار دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریه بهانه سازی تا روی خود ببینی</p></div>
<div class="m2"><p>آئینه با رخ تو چندان چه کار دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ترک جان گرفتم در عشق روی چون تو</p></div>
<div class="m2"><p>بر من فلان چه گوید بهمان چه کار دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاقانی از زمانه چون دست شست بر وی</p></div>
<div class="m2"><p>سنجر چه حکم راند خاقان چه کار دارد</p></div></div>