---
title: >-
    غزل شمارهٔ ۱۵
---
# غزل شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>مست تمام آمده است بر در من نیم شب</p></div>
<div class="m2"><p>آن بت خورشید روی و آن مه یاقوت لب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوفت به آواز نرم حلقهٔ در کای غلام</p></div>
<div class="m2"><p>گفتم کاین وقت کیست بر در ما ای عجب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت منم آشنا گرچه نخواهی صداع</p></div>
<div class="m2"><p>گفت منم میهمان گرچه نکردی طلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او چو در آمد ز در بانگ برآمد ز من</p></div>
<div class="m2"><p>کانیت شکاری شگرف وینت شبی بوالعجب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردم برجان رقم شکر شب و مدح می</p></div>
<div class="m2"><p>کامدن دوست را بود ز هر دو سبب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرنه شبستی رخش کی شودی بی‌نقاب</p></div>
<div class="m2"><p>ورنه میستی سرش کی شودی پر شغب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم اگرچه مرا توبه درست است لیک</p></div>
<div class="m2"><p>درشکنم طرف شب با تو به شکر طرب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم کز بهر خرج هدیه پذیرد ز من</p></div>
<div class="m2"><p>عارض سیمین تو این رخ زرین سلب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت که خاقانیا روی تو زرفام نیست</p></div>
<div class="m2"><p>گفتم معذور دار زر ننماید به شب</p></div></div>