---
title: >-
    غزل شمارهٔ ۱۴
---
# غزل شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>گر مدعی نه‌ای غم جانان به جان طلب</p></div>
<div class="m2"><p>جان چون به شهر عشق رسد نورهان طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون خرد بریز و دیت بر عدم نویس</p></div>
<div class="m2"><p>برگ هوا بساز و نثار از روان طلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دی یاسجی ز ترکش جانانت گم شده است</p></div>
<div class="m2"><p>دل و اشکاف و یاسح او در میان طلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نیست گشتی از خود و با تو توئی نماند</p></div>
<div class="m2"><p>از نیستی در آینهٔ دل نشان طلب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا از طلب به یافت رسی سالهاست راه</p></div>
<div class="m2"><p>بس کن حدیث یافت طلب را به جان طلب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاقانیا پیاده شو از جان که دل توراست</p></div>
<div class="m2"><p>بر دل سوار گرد و فلک در عنان طلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اقطاع این سوار ورای خرد شناس</p></div>
<div class="m2"><p>میدان این براق برون از جهان طلب</p></div></div>