---
title: >-
    غزل شمارهٔ ۷۲
---
# غزل شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>هر که در عاشقی قدم نزده است</p></div>
<div class="m2"><p>بر دل از خون دیده نم نزده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او چه داند که چیست حالت عشق</p></div>
<div class="m2"><p>که بر او عشق، تیر غم نزده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق را مرتبت نداند آنک</p></div>
<div class="m2"><p>همه جز در وصال کم نزده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل و جان باخته است هر دو بهم</p></div>
<div class="m2"><p>گرچه با دل‌ربای دم نزده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش عشق دوست در شب و روز</p></div>
<div class="m2"><p>بجز اندر دلم علم نزده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یارب این عشق چیست در پس و پیش</p></div>
<div class="m2"><p>هیچ عاشق در حرم نزده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه از آن سوخته‌دل بریان</p></div>
<div class="m2"><p>کو به جز در هوات دم نزده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز شادیش کس ندید و چه روز</p></div>
<div class="m2"><p>باد شادی قفاش هم نزده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شادمان آن دل از هوای بتی</p></div>
<div class="m2"><p>که بر او درد و غم رقم نزده است</p></div></div>