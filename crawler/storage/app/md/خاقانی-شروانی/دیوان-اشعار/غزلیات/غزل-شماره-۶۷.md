---
title: >-
    غزل شمارهٔ ۶۷
---
# غزل شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>دل را ز دم تو دام روزی است</p></div>
<div class="m2"><p>وز صاف تو درد خام روزی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ساقی مجلس تو ما را</p></div>
<div class="m2"><p>از دور خیال جام روزی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان خاک تو شد که خاک را هم</p></div>
<div class="m2"><p>از جرعهٔ ناتمام روزی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغی است دلم بلندپرواز</p></div>
<div class="m2"><p>اما ز قضاش دام روزی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناکام شدم به کام دشمن</p></div>
<div class="m2"><p>تا خود ز توام چه کام روزی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان پای بر آتشم که دل را</p></div>
<div class="m2"><p>بر خاک درت مقام روزی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماندم به شمار هجر و وصلت</p></div>
<div class="m2"><p>تا زین دو مرا کدام روزی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتواست به خون من غمت را</p></div>
<div class="m2"><p>الحق غم تو حرام روزی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاقانی را زیاد خواندی</p></div>
<div class="m2"><p>کورا ز وجود نام روزی است</p></div></div>