---
title: >-
    غزل شمارهٔ ۲۸۳
---
# غزل شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>از عشق دوست بین که چه آمد به روی من</p></div>
<div class="m2"><p>کز غم مرا بکشت و نیازرد موی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عشق یار روی ندارم که دم زنم</p></div>
<div class="m2"><p>کز عشق روی او چه غم آمد به روی من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باری کبوترا تو ز من نامه‌ای ببر</p></div>
<div class="m2"><p>نزدیک یار و پاسخش آور به سوی من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد دلم ببین که دلم وصل جوی اوست</p></div>
<div class="m2"><p>آه ای کبوتر از دل سیمرغ جوی من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنهار تا به برج دگر کس بنگذری</p></div>
<div class="m2"><p>برجت سرای من به و صحرات کوی من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گستاخ برمپر که مبادا که ناگهی</p></div>
<div class="m2"><p>شاهین بود نشانده به راهت عدوی من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر پای بندمت زر چهره که حاسدان</p></div>
<div class="m2"><p>بی‌رنگ زر رها نکنندت به بوی من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاقانی است جوجو در آرزوی او</p></div>
<div class="m2"><p>او خود به نیم جو نکند آرزوی من</p></div></div>