---
title: >-
    غزل شمارهٔ ۷۱
---
# غزل شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>به دو میگون لب و پسته دهنت</p></div>
<div class="m2"><p>به سه بوس خوش و فندق شکنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زره پوش قد تیر وشت</p></div>
<div class="m2"><p>به کمان‌کش مژهٔ تیغ زنت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به حریر تن و دیبای رخت</p></div>
<div class="m2"><p>به ترنج بر و سیب ذقنت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دو نرگس، به دو سنبل، به دو گل</p></div>
<div class="m2"><p>بر سر سرو صنوبر فکنت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به می عبهر آن سرخ گلت</p></div>
<div class="m2"><p>به خوی عنبر آن یاسمنت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به گهرهای تر از لعل لبت</p></div>
<div class="m2"><p>به حلی‌های زر از سیم تنت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فروغ رخ زهره صفتت</p></div>
<div class="m2"><p>به فریب دل هاروت فنت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نگین لب و طوق غببت</p></div>
<div class="m2"><p>این ز برگ گل و آن، از سمنت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دو مخمور عروس حبشیت</p></div>
<div class="m2"><p>خفته در حجلهٔ جزع یمنت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به بناگوش تو و حلقهٔ گوش</p></div>
<div class="m2"><p>به دو زنجیر شکن بر شکنت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به سرشک تر و خون جگرم</p></div>
<div class="m2"><p>بسته بیرون و درون دهنت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به شرار دل و دود نفسم</p></div>
<div class="m2"><p>مانده بر عارض جعد کشنت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به نیاز دل من در طلبت</p></div>
<div class="m2"><p>به گداز تن من در حزنت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به دو تا موی که تعویذ من است</p></div>
<div class="m2"><p>یادگار از سر مشکین رسنت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به نشانی که میان من و توست</p></div>
<div class="m2"><p>نوش مرغان و نوای سخنت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که مرا تا دل و جان است بجای</p></div>
<div class="m2"><p>جای باشد به دل و جان منت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دوست‌تر دارمت از هر دو جهان</p></div>
<div class="m2"><p>دوست‌تر دارم از خویشتنت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو بمان دیر که خاقانی را</p></div>
<div class="m2"><p>دل نمانده است ز دیر آمدنت</p></div></div>