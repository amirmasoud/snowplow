---
title: >-
    غزل شمارهٔ ۳۷۲
---
# غزل شمارهٔ ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>چه کرد این بنده جز آزادمردی</p></div>
<div class="m2"><p>که گرد خاطر او برنگردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دل گفتی نخواهم جست، جستی</p></div>
<div class="m2"><p>جفا گفتی نخواهم کرد، کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه بر حرف هجران داری انگشت</p></div>
<div class="m2"><p>چه باشد این ورق را در نوردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من مست توست او را میفکن</p></div>
<div class="m2"><p>که مستان را فکندن نیست مردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا یارم که با تو باز کوشم</p></div>
<div class="m2"><p>که تو با رستم ای جان هم نبردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه سود ار من رسم در گرد اسبت؟</p></div>
<div class="m2"><p>که تو صد ساله ره ز آن سوی گردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای آنکه نقش تو نگارند</p></div>
<div class="m2"><p>دل خاقانی آمد لاجوردی</p></div></div>