---
title: >-
    غزل شمارهٔ ۱۷۷
---
# غزل شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>آباد بر آن شب که شب وصلت ما بود</p></div>
<div class="m2"><p>زیرا که نه شب بود که تاریخ بقا بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودند بسی سوختگان گرد در او</p></div>
<div class="m2"><p>لیکن به سرا پردهٔ او بار مرا بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من سایه شدم او ز پس چشم رقیبان</p></div>
<div class="m2"><p>بر صورت من راست چو خورشید سما بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر چشم من آن ماه جهان‌سوز رقم بود</p></div>
<div class="m2"><p>بر عشق وی این آه جهان‌سوز گوا بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از وی طلب عهد و ز من لفظ بلی بود</p></div>
<div class="m2"><p>از من سخن عذر و ازو عین رضا بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیرون ز قضا و ز قدر بود وصالش</p></div>
<div class="m2"><p>چه جای قدر بود و چه پروای قضا بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر نعت که در وصف مثالش بشنودم</p></div>
<div class="m2"><p>با صورت وصلش همه آن وصف خطا بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من شیفته از شادی و پرسان ز دل خویش</p></div>
<div class="m2"><p>کای دل به جهان اینکه مرا بود که را بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من بودم و او و صفت حال من و او</p></div>
<div class="m2"><p>صاحب خبران صبح‌دم و باد صبا بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا لاجرم امروز سمر شد که شب دوش</p></div>
<div class="m2"><p>پروانه‌ای اندر حرم شمع صفا بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آواز ز عشاق برآمد که فلان شب</p></div>
<div class="m2"><p>معراج دگر نوبت خاقانی ما بود</p></div></div>