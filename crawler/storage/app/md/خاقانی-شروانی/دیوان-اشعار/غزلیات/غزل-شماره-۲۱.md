---
title: >-
    غزل شمارهٔ ۲۱
---
# غزل شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>پای گریز نیست که گردون کمان‌کش است</p></div>
<div class="m2"><p>جای فزاع نیست که گیتی مشوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماویز در فلک که نه بس چرب مشرب است</p></div>
<div class="m2"><p>برخیز از جهان که نه بس خوب مفرش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون مار ارقم است جهان گاه آزمون</p></div>
<div class="m2"><p>کاندر درون کشنده و بیرون منقش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با خویشتن بساز و ز کس مردمی مجوی</p></div>
<div class="m2"><p>کان کو فرشته بود کنون اهرمن‌وش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با هر که انس گیری از او سوخته شوی</p></div>
<div class="m2"><p>بنگر که انس چیست مصحف ز آتش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم نگشت و ما و تو گردنده‌ایک از آنک</p></div>
<div class="m2"><p>گردون هنوز هفت و جهت همچنان شش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بند دور چرخ هم ارکان، هم انجم است</p></div>
<div class="m2"><p>در زیر ران دهر هم ادهم، هم ابرش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاقانیا منال که این ناله‌های تو</p></div>
<div class="m2"><p>برساز روزگار نه بس زخمهٔ خوش است</p></div></div>