---
title: >-
    غزل شمارهٔ ۳۸۱
---
# غزل شمارهٔ ۳۸۱

<div class="b" id="bn1"><div class="m1"><p>عتاب رنگ به من نامه‌ای فرستادی</p></div>
<div class="m2"><p>مرا به پردهٔ تشریف راه نو دادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صحیفه‌های معانی نوشتی و سر آن</p></div>
<div class="m2"><p>به دست مهر ببستی و مهر بنهادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو نقش عارض و زلف تو نوک خامهٔ تو</p></div>
<div class="m2"><p>نمود بر ورق روز از شب استادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا نمودی کای پای بست محنت ما</p></div>
<div class="m2"><p>به غم مباش که ما را هنوز بر یادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مترس اگرچه به صد درد و بند بسته شدی</p></div>
<div class="m2"><p>کنون که بندهٔ مائی ز هر غم آزادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن زمان که بدیدم نگار خامهٔ تو</p></div>
<div class="m2"><p>نگار نامهٔ من گشت نامت از شادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز لطف‌ها که نمودی گمان برم که همی</p></div>
<div class="m2"><p>در بهشت بر اهل نیاز بگشادی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز فصل‌ها که نوشتی یقین شدم که همی</p></div>
<div class="m2"><p>دم مسیح بر مردگان فرستادی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلیل که از غم غربت چو دیر بود خراب</p></div>
<div class="m2"><p>به روزگار تو چون کعبه شد به آبادی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز رغم آنکه مرا در غم تو طعنه زنند</p></div>
<div class="m2"><p>غم تو شادی من شد که شادمان بادی</p></div></div>