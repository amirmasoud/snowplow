---
title: >-
    غزل شمارهٔ ۲۵۸
---
# غزل شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>خیز تا رخت دل براندازیم</p></div>
<div class="m2"><p>وز پی نیکوئی سر اندازیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با حریفان درد مهرهٔ مهر</p></div>
<div class="m2"><p>بر بساط قلندر اندازیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دین و دنیا حجاب همت ماست</p></div>
<div class="m2"><p>هر دو در پای دلبر اندازیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوست در روی ما چو سنگ انداخت</p></div>
<div class="m2"><p>ما به شکرانه شکر اندازیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردم دیده را سپند کنیم</p></div>
<div class="m2"><p>پیش روی بر آذر اندازیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه از توسنی چو طالع ماست</p></div>
<div class="m2"><p>ما کمند وفا دراندازیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بدین حیله صید شد بخ‌بخ</p></div>
<div class="m2"><p>ورنه کاری دگر براندازیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا کی از غصه‌های بدگویان</p></div>
<div class="m2"><p>قصه‌ها پیش داور اندازیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرح این حال پیش دوست کنیم</p></div>
<div class="m2"><p>سنگ فتنه به لشکر اندازیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تحفه سازیم جان خاقانی</p></div>
<div class="m2"><p>پیش خاقان اکبر اندازیم</p></div></div>