---
title: >-
    غزل شمارهٔ ۳۱۷
---
# غزل شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>از زلف هر کجا گرهی برگشاده‌ای</p></div>
<div class="m2"><p>بر هر دلی هزار گره برنهاده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در روی من ز غمزه کمان‌ها کشیده‌ای</p></div>
<div class="m2"><p>بر جان من ز طره کمین‌ها گشاده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر هرچه در زمانه سواری به نیکوئی</p></div>
<div class="m2"><p>الا بر وفا و مهر کز این دو پیاده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی جفا نه کار من است ای سلیم دل</p></div>
<div class="m2"><p>تو خود ز مادر از پی این کار زاده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدی که دل چگونه ز من در ربوده‌ای</p></div>
<div class="m2"><p>پنداشتی که بر سر گنج اوفتاده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی که روز سختی فریاد تو رسم</p></div>
<div class="m2"><p>سخت است کار بهر چه روز ایستاده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاقانی از جهان به پناه تو درگریخت</p></div>
<div class="m2"><p>او را به دست خصم چرا باز داده‌ای</p></div></div>