---
title: >-
    غزل شمارهٔ ۳۶۳
---
# غزل شمارهٔ ۳۶۳

<div class="b" id="bn1"><div class="m1"><p>زین نیم جان که دارم جانان چه خواست گوئی</p></div>
<div class="m2"><p>کرد آنچه خواست با دل از جان چه خواست گوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم کمان‌کش او ترکی است یاسج افکن</p></div>
<div class="m2"><p>چون صبر کرد غارت ز ایمان چه خواست گوئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در وعده خورد خونم پس داد وعدهٔ کژ</p></div>
<div class="m2"><p>زان خون که نیست چندین، چندان چه خواست گوئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بلبلم بر آتش نعره زنان و سوزان</p></div>
<div class="m2"><p>کز زیره آب دادن جانان چه خواست گوئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هجرانش آتش غم در کشت عمر من زد</p></div>
<div class="m2"><p>زین کشت زرد عمرم هجران چه خواست گوئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم رسم به وصلت مژگان بر ابروان زد</p></div>
<div class="m2"><p>زین بر زدن به ابرو مژگان چه خواست گوئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من سر نهم به پایش او روی تابد از من</p></div>
<div class="m2"><p>من پشت دست خایم کو زان چه خواست گوئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طوفان آب و آتش بر باد داد خاکم</p></div>
<div class="m2"><p>زین هست و نیست موئی طوفان چه خواست گوئی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محرم نزاد دوران ور زاد کشت خیره</p></div>
<div class="m2"><p>زین خیره کشتن آوخ دوران چه خواست گوئی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان همدمان یک‌دل یک نازنین نمانده است</p></div>
<div class="m2"><p>این دور بی‌وفایان ز ایشان چه خواست گوئی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاقانیا دلت را ز افغان چه حاصل آید</p></div>
<div class="m2"><p>چون دل نیافت دارو ز افغان چه خواست گوئی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شروان ز باغ سلوت بس دور کرد ما را</p></div>
<div class="m2"><p>زین دور کردن ما شروان چه خواست گوئی</p></div></div>