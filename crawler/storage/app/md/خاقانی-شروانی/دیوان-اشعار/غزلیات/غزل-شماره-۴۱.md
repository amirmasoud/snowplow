---
title: >-
    غزل شمارهٔ ۴۱
---
# غزل شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>ای قول دل به رفیع‌الدرجات</p></div>
<div class="m2"><p>وز برائت به جهان داده برات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنجم چار صفی از ملکان</p></div>
<div class="m2"><p>هشتم هفت تنی از طبقات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رای رخشان تو بر چشمهٔ خضر</p></div>
<div class="m2"><p>رفته بی‌زحمت راه ظلمات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خصم تو کور و تو آیینهٔ شرع</p></div>
<div class="m2"><p>کور آیینه شناسد؟ هیهات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاسد ار در تو گشاده است زبان</p></div>
<div class="m2"><p>هم کنونش رسد آفات وفات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک دو آواز برآید ز چراغ</p></div>
<div class="m2"><p>گه مردن که بود در سکرات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که بناگه ز وطن کردی نقل</p></div>
<div class="m2"><p>بیش یابی ز مانه حسنات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن نبینی که یکی ده گردد</p></div>
<div class="m2"><p>چون ز آحاد رسد در عشرات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و آنکه جای تو گرفت است آنجا</p></div>
<div class="m2"><p>هیچ کس دانمش از روی صفات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که الف چون بشد از منزل یک</p></div>
<div class="m2"><p>صفر بر جای الف کرد ثبات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز تو تا غیر تو فرق است ارچه</p></div>
<div class="m2"><p>نسب از آدم دارند به ذات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرچه هر دو ز جلبت سنگند</p></div>
<div class="m2"><p>فرق باشد ز منی تا به منات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دایم از باغ بقای تو رساد</p></div>
<div class="m2"><p>به همه خلق نسیم برکات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرقه‌داران تو مقبول چو لا</p></div>
<div class="m2"><p>بدسگالان تو معزول چو لات</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گررسد جنبش کلک تو به من</p></div>
<div class="m2"><p>هیچ نقصت نرسد زین حرکات</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که دل خستهٔ خاقانی را</p></div>
<div class="m2"><p>از تحیات توبخشند حیات</p></div></div>