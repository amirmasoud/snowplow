---
title: >-
    غزل شمارهٔ ۱۲۲
---
# غزل شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>لعلت اندر سخن شکر خاید</p></div>
<div class="m2"><p>رویت انگشت بر قمر خاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که با یاد تو شرنگ خورد</p></div>
<div class="m2"><p>هم‌چنان دان که نیشکر خاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که او پای بست روی تو شد</p></div>
<div class="m2"><p>پشت دست از نهیب سرخاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرکب جان به مرغزار غمت</p></div>
<div class="m2"><p>بدل سبزه عود تر خاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنده تا دید سیم دندانت</p></div>
<div class="m2"><p>لب همه ز آرزوی زر خاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشقت آن اژدهاست در تن من</p></div>
<div class="m2"><p>که دلم درد و جگر خاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوش کن حسب حال خاقانی</p></div>
<div class="m2"><p>گرچه او ژاژ بیشتر خاید</p></div></div>