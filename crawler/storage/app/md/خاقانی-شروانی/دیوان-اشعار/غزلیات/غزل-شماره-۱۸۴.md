---
title: >-
    غزل شمارهٔ ۱۸۴
---
# غزل شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>با درد تو کس منت مرهم نپذیرد</p></div>
<div class="m2"><p>با وصل تو کس ملکت عالم نپذیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنگ است در وصل تو زان هیچ قدی نیست</p></div>
<div class="m2"><p>کو بر در وصل تو رسد خم نپذیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن کس که نگین لب تو یافت به صد جان</p></div>
<div class="m2"><p>در عرض وی انگشتری جم نپذیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش لب تو تحفه فرستم دل و دین را</p></div>
<div class="m2"><p>دانم که کست تحفه ازین کم نپذیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار غم من صبر نپذرفت و عجب نیست</p></div>
<div class="m2"><p>بر کوه اگر عرض کنی هم نپذیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در معرکهٔ عشق تو عقلم سپر افکند</p></div>
<div class="m2"><p>کان حمله که او آرد رستم نپذیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی سر خاقانی دارم به سر و چشم</p></div>
<div class="m2"><p>ای شوخ برو کز تو کس این دم نپذیرد</p></div></div>