---
title: >-
    غزل شمارهٔ ۱۵۹
---
# غزل شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>ماه را با نور رویش بیش مقداری نماند</p></div>
<div class="m2"><p>مشک را با بوی زلفش بس خریداری نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا برآمد در جهان آوازهٔ زلف و رخش</p></div>
<div class="m2"><p>کیمیای کفر و دین را روز بازاری نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان هر جا که یاد آن لب میگون گذشت</p></div>
<div class="m2"><p>ناشکسته توبه و نابسته زناری نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر در این آتش که عشق اوست در درگاه او</p></div>
<div class="m2"><p>آبروئی ماند کس را آب ما باری نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن زمان کز بهر دو نان عشق او خلعت برید</p></div>
<div class="m2"><p>ای عفی‌الله خود نصیب من کله‌واری نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واندر آن بستان کز او دست خسان را گل رسید</p></div>
<div class="m2"><p>ای عجب گوئی برای چشم من خاری نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرط خاقانی است با جور و جفایش ساختن</p></div>
<div class="m2"><p>خاصه اکنون کاندرین عالم وفاداری نماند</p></div></div>