---
title: >-
    غزل شمارهٔ ۳۳۹
---
# غزل شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>آن لعل شکر خنده گر از هم بگشایی</p></div>
<div class="m2"><p>حقا که به یک خنده دو عالم بگشایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ورچه نگشائی لب و در پوست بخندی</p></div>
<div class="m2"><p>از رشتهٔ جانم گره غم بگشایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجروح توام شاید اگر زخم ببندی</p></div>
<div class="m2"><p>رحمی کن ار حقهٔ مرهم بگشایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاری است فرو بسته، گشادن تو توانی</p></div>
<div class="m2"><p>صد مشکل ازین‌گونه به یک‌دم بگشایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندیشه مکن سلسلهٔ چرخ نبرد</p></div>
<div class="m2"><p>گر کار چو زنجیر من از هم بگشایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی چو فلک دست جفا برنگشایم</p></div>
<div class="m2"><p>ایمن نشوم، گر تو توئی هم بگشایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هان ای دل خاقانی از آه سحری کوش</p></div>
<div class="m2"><p>کاین چنبر افلاک خم از خم بگشایی</p></div></div>