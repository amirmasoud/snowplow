---
title: >-
    غزل شمارهٔ ۲۷۰
---
# غزل شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>دارم سر آنکه سر برآرم</p></div>
<div class="m2"><p>خود را ز دو کون بر سر آرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر هامهٔ ره روان نهم پای</p></div>
<div class="m2"><p>همت ز وجود برتر آرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر لاشهٔ عجز بر نهم رخت</p></div>
<div class="m2"><p>تا رخش قدر عنان درآرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این دار خلافت پدر را</p></div>
<div class="m2"><p>در زیر نگین مسخر آرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وین هودج کبریای دل را</p></div>
<div class="m2"><p>بر کوههٔ چرخ اخضر آرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وین تاج دواج یوسفی را</p></div>
<div class="m2"><p>درمصر حقیقت اندر آرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌واسطهٔ خیال با دوست</p></div>
<div class="m2"><p>خلوت کنم و دمی برآرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در حجرهٔ خاص او فلک را</p></div>
<div class="m2"><p>مانندهٔ حلقه بر در آرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب را ز برای زنده ماندن</p></div>
<div class="m2"><p>تا نفخهٔ صور همبر آرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر پرده‌دری کند تف صبح</p></div>
<div class="m2"><p>از دود دلش رفوگر آرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در کعبهٔ شش جهت که عشق است</p></div>
<div class="m2"><p>خاقانی را مجاور آرم</p></div></div>