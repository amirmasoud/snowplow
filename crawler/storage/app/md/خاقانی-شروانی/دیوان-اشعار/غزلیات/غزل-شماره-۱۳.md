---
title: >-
    غزل شمارهٔ ۱۳
---
# غزل شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>سر به عدم درنه و یاران طلب</p></div>
<div class="m2"><p>بوی وفا خواهی ازیشان طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر عالم شو و هم جنس جوی</p></div>
<div class="m2"><p>در تک دریا رو و مرجان طلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرکز خاکی نبود جای تو</p></div>
<div class="m2"><p>مرتبهٔ گنبد گردان طلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مائدهٔ جان چو نهی در میان</p></div>
<div class="m2"><p>جان به میانجی نه و مهمان طلب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی زمین خیل شیاطین گرفت</p></div>
<div class="m2"><p>شمع برافروز و سلیمان طلب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل خاقانی مجروح خیز</p></div>
<div class="m2"><p>اهل به دست آور و درمان طلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهر سفر نوش کن اول چو خضر</p></div>
<div class="m2"><p>پس برو و چشمهٔ حیوان طلب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خطهٔ شروان نشود خیروان</p></div>
<div class="m2"><p>خیر برون از خط شروان طلب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سنگ به قرابهٔ خویشان فکن</p></div>
<div class="m2"><p>خویش و قرابات دگرسان طلب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یوسف دیدی که ز اخوة چه دید</p></div>
<div class="m2"><p>پشت بر اخوة کن و اخوان طلب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشرب شروان ز نهنگان پر است</p></div>
<div class="m2"><p>آبخور آسان به خراسان طلب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روی به دریا نه و چون بگذری</p></div>
<div class="m2"><p>در طبرستان طربستان طلب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مقصد آمال ز آمل شناس</p></div>
<div class="m2"><p>یوسف گم کرده به گرگان طلب</p></div></div>