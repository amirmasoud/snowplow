---
title: >-
    غزل شمارهٔ ۲۰
---
# غزل شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>انصاف در جبلت عالم نیامده است</p></div>
<div class="m2"><p>راحت نصیب گوهر آدم نیامده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مادر زمانه نزاده است هیچکس</p></div>
<div class="m2"><p>کوهم ز دهر نامزد غم نیامده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از موج غم نجات کسی راست کو هنوز</p></div>
<div class="m2"><p>بر شط کون و عرصهٔ عالم نیامده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ساغر زمانه که نوشید شربتی</p></div>
<div class="m2"><p>کان نوش جانگزای‌تر از سم نیامده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیتی تو را ز حادثه ایمن کجا کند؟</p></div>
<div class="m2"><p>کورا ز حادثات امان هم نیامده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دزدی است چرخ نقب‌زن اندر سرای عمر</p></div>
<div class="m2"><p>آری به هرزه قامت او خم نیامده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آسودگی مجوی که کس را به زیر چرخ</p></div>
<div class="m2"><p>اسباب این مراد فراهم نیامده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خستگی بساز که ما را ز روزگار</p></div>
<div class="m2"><p>زخم آمده است حاصل و مرهم نیامده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جامهٔ کبود فلک بنگر و بدان</p></div>
<div class="m2"><p>کاین چرخ جز سراچهٔ ماتم نیامده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاقانیا فریب جهان را مدار گوش</p></div>
<div class="m2"><p>کورا ز ده، دو قاعده محکم نیامده است</p></div></div>