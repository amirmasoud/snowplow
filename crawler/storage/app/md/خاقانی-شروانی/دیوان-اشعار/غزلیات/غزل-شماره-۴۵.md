---
title: >-
    غزل شمارهٔ ۴۵
---
# غزل شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>لعل او بازار جان خواهد شکست</p></div>
<div class="m2"><p>خندهٔ او مهر کان خواهد شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عابدان را پرده این خواهد درید</p></div>
<div class="m2"><p>زاهدان را توبه آن خواهد شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هودج نازش نگنجد در جهان</p></div>
<div class="m2"><p>لیک محمل برجهان خواهد شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرنیان جوئی به پای پیل غم</p></div>
<div class="m2"><p>دل چو پیل پرنیان خواهد شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی گندم گون او در چشم ماه</p></div>
<div class="m2"><p>خار راه کهکشان خواهد شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمزه‌ش ار غوغا کند هیچش مگوی</p></div>
<div class="m2"><p>کو طلسم آسمان خواهد شکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دشمنان از داغ هجرش رسته‌اند</p></div>
<div class="m2"><p>پل همه بر دوستان خواهد شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جای فریاد است خاقانی که چرخ</p></div>
<div class="m2"><p>نالهٔ فریاد خوان خواهد شکست</p></div></div>