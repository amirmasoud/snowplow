---
title: >-
    غزل شمارهٔ ۵۸
---
# غزل شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>شوری ز دو عشق در سر ماست</p></div>
<div class="m2"><p>میدان دل از دو لشکر آراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یک نظرم دو دلبر افتاد</p></div>
<div class="m2"><p>وز یک جهتم دو قبه برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید پرست بودم اول</p></div>
<div class="m2"><p>اکنون همه میل من به جوزاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مشرق و مغرب دل من</p></div>
<div class="m2"><p>هم بدر و هم آفتاب پیداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانم ز دو حور در بهشت است</p></div>
<div class="m2"><p>کارم ز دو ماه بر ثریاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر یافته‌ام دو در عجب نیست</p></div>
<div class="m2"><p>زیرا که دو چشم من دو دریاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بالله که خطاست هرچه گفتم</p></div>
<div class="m2"><p>والله که هرآنچه رفت سوداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاقانی را چه روز عشق است</p></div>
<div class="m2"><p>با این غم روزگار کور است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزی دارد سیاه چونانک</p></div>
<div class="m2"><p>دشمن به دعای نیم شب خواست</p></div></div>