---
title: >-
    غزل شمارهٔ ۳۳۳
---
# غزل شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>تا بیش دل خراب داری</p></div>
<div class="m2"><p>دل بیش کند ز جان‌سپاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کار مرا به دولت تو</p></div>
<div class="m2"><p>افتاد قرار بی‌قراری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل خوش کردم چنین که دانی</p></div>
<div class="m2"><p>تن دردادم چنان که داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک ناخن کم نمی‌کنی جور</p></div>
<div class="m2"><p>تا خون دلم به ناخن آری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان کاهی و اندهان فزائی</p></div>
<div class="m2"><p>سیبی به دو کرده روزگاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آوازه فراخ شد به عالم</p></div>
<div class="m2"><p>درگاه تو را به تنگ باری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر لحظه کشی ز صف عشاق</p></div>
<div class="m2"><p>چندان که به دست چپ شماری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این باقی عمر با تو باشم</p></div>
<div class="m2"><p>کز عمر گذشته یادگاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک در تو رساند آخر</p></div>
<div class="m2"><p>خاقانی را به تاجداری</p></div></div>