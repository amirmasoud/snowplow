---
title: >-
    غزل شمارهٔ ۲۲۳
---
# غزل شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>نماند اهل رنگی که من داشتم</p></div>
<div class="m2"><p>برفت آب و سنگی که من داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بوی دل یار یک‌رنگ بود</p></div>
<div class="m2"><p>به منزل درنگی که من داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برد رنگ دیبا هوا لاجرم</p></div>
<div class="m2"><p>هوا برد رنگی که من داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خزان شد بهاری که من یافتم</p></div>
<div class="m2"><p>کمان شد خدنگی که من داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجز با لب و چشم خوبان نبود</p></div>
<div class="m2"><p>همه صلح و جنگی که من داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شیر، آتشین چنگ و چست آمدم</p></div>
<div class="m2"><p>پی هر پلنگی که من داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنون جز به تعویذ طفلان درون</p></div>
<div class="m2"><p>نبینند چنگی که من داشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه خاقانیم نام گم کن مرا</p></div>
<div class="m2"><p>که شد نام و ننگی که من داشتم</p></div></div>