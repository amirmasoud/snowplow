---
title: >-
    غزل شمارهٔ ۱۳۹
---
# غزل شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>آتش عیاره‌ای آب عیارم ببرد</p></div>
<div class="m2"><p>سیم بناگوش او سکهٔ کارم ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف چلیپا خمش در بن دیرم نشاند</p></div>
<div class="m2"><p>لعل مسیحا دمش بر سر دارم ببرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله کنان می‌دوم سنگ به بر در، چو آب</p></div>
<div class="m2"><p>کاب من و سنگ من غمزهٔ یارم ببرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوجوم از عشق آنک خالش مشکین جو است</p></div>
<div class="m2"><p>دل جو مشکینش دید خر شد و بارم ببرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت قراری بدانک دل به دو زلفش دهم</p></div>
<div class="m2"><p>دل به قراری که رفت رفت و قرارم ببرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دید دلم وقف عشق خانهٔ بام آسمان</p></div>
<div class="m2"><p>خانه فروشی بزد دل ز کنارم ببرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق برون آورد مهره ز دندان مار</p></div>
<div class="m2"><p>آمد و دندان کنان در دم مارم ببرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت که خاقانیا آب رخت چون نماند</p></div>
<div class="m2"><p>آب رخم هم به آب گریهٔ زارم ببرد</p></div></div>