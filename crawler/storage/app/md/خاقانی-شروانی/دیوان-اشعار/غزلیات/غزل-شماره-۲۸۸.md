---
title: >-
    غزل شمارهٔ ۲۸۸
---
# غزل شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>تا مرا سودای تو خالی نگرداند ز من</p></div>
<div class="m2"><p>با تو ننشینم به کام خویشتن بی‌خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار راه خود منم خود را ز خود فارغ کنم</p></div>
<div class="m2"><p>تا دوئی یکسو شود هم من تو گردم هم تو من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باقی آن گاهی شوم کز خویشتن یابم فنا</p></div>
<div class="m2"><p>مرده اکنونم که نقش زندگی دارم کفن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان فشان و راد زی و راه‌کوب و مرد باش</p></div>
<div class="m2"><p>تا شوی باقی چو دامن برفشانی زین دمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای طریق جستجویت همچو خویت بوالعجب</p></div>
<div class="m2"><p>راه من سوی تو چون زلفت دراز و پرشکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که چو کژدم ندارم چشم و بی‌پایم چو مار</p></div>
<div class="m2"><p>چون توانم دید ره یا گام چون دانم زدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغ جان من در این خاکی قفس محبوس توست</p></div>
<div class="m2"><p>هم تو بالش برگشا و هم تو بندش برشکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا اگر پران شود کوی تو سازد آشیان</p></div>
<div class="m2"><p>یا گرش قربان کنی زلف تو باشد بابزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سالها شد تا دل جان‌پاش ازرق‌پوش من</p></div>
<div class="m2"><p>معتکف‌وار اندر آن زلف سیه دارد وطن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از در تو برنگردم گرچه هر شب تا به روز</p></div>
<div class="m2"><p>پاسبانان بینم آنجا انجمن در انجمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در ازل بر جان خاقانی نهادی مهر مهر</p></div>
<div class="m2"><p>تا ابد بی‌رخصت خاقان اعظم برمکن</p></div></div>