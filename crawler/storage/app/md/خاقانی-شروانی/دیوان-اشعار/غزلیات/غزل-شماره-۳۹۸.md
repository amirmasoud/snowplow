---
title: >-
    غزل شمارهٔ ۳۹۸
---
# غزل شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>چو عمر رفته تو کس را به هیچ کار نیایی</p></div>
<div class="m2"><p>چو عمر نامده هم اعتماد را به نشایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزیز بودی چون عمر و همچو عمر برفتی</p></div>
<div class="m2"><p>چو عمر رفته ز دستم نداند آنکه کی آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا چو عمر جوانی فریب دادی رفتی</p></div>
<div class="m2"><p>تو همچو عمر جوانی، برو نه اهل وفایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم تو را و جهان را وداع کرد به عمری</p></div>
<div class="m2"><p>که او به ترک سزا بود و تو به هجر سزایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو عمر نفس‌پرستان که بر محال گذشت آن</p></div>
<div class="m2"><p>برفتی از سر غفلت نپرسمت که کجایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو را به سلسلهٔ صبر خواستم که ببندم</p></div>
<div class="m2"><p>ولی تو شیفته چون عمر بیش بند نپایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دست عمر سبک پای سرگران به تو نالم</p></div>
<div class="m2"><p>که عمر من ز تو آموخت این گریخته پایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو هم‌چو روزی بسیار نارسیده بهی ز آن</p></div>
<div class="m2"><p>که عمر کاهی اگرچه نشاط دل بفزایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا ز تو همه عمر است ماتم همه روزه</p></div>
<div class="m2"><p>که هم‌چو عید به سالی دوبار روی نمایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو عمر رفته به محنت که غم فزاید یادش</p></div>
<div class="m2"><p>به یاد نارمت ایرا که یادگار بلایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو روز فرقت یاران که نشمرند ز عمرش</p></div>
<div class="m2"><p>ز عمر نشمرم آن ساعتی که پیش من آیی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز خوان وصل تو کردم خلال و دست بشستم</p></div>
<div class="m2"><p>به آب دیده ز عشقت که زهر عمر گزایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا به سال مزن طعنه گر کهن شده سروم</p></div>
<div class="m2"><p>که تو به تازگی عمر هم‌چو گل به نوایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تویی که نقب زنی در سرای عمر و به آخر</p></div>
<div class="m2"><p>نه نقد وقت بری کیسهٔ حیات ربایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنان که از دیت خون بود حیات دوباره</p></div>
<div class="m2"><p>دوباره عمر شمارم که یابم از تو جدایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من از غم تو و از عمر سیر گشتم ازیرا</p></div>
<div class="m2"><p>چو غم نتیجهٔ عمری چو عمر دام بلایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به عمرم از تو چه اندوختم جزین زر چهره</p></div>
<div class="m2"><p>به زر مرا چه فریبی که کیمیای جفایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برو که تشنهٔ دیرینه‌ای به خون من آری</p></div>
<div class="m2"><p>نپرسم از تو که چون عمر زود سیر چرایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تنم ببندی و کارم به عمرها نگشایی</p></div>
<div class="m2"><p>که کم عیاری اگرچه چو عمر بیش بهایی</p></div></div>