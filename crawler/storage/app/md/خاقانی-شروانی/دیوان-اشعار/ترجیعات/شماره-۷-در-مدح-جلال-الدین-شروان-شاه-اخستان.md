---
title: >-
    شمارهٔ ۷ - در مدح جلال الدین شروان شاه اخستان
---
# شمارهٔ ۷ - در مدح جلال الدین شروان شاه اخستان

<div class="b" id="bn1"><div class="m1"><p>جو به جو راز جهان بنمود صبح</p></div>
<div class="m2"><p>مشک جو جو از دهان بنمود صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح گوئی زلف شب را عاشق است</p></div>
<div class="m2"><p>کز دم عاشق نشان بنمود صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در وداع شب همانا خون گریست</p></div>
<div class="m2"><p>روی خون آلود از آن بنمود صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام فرعونی خبر ده تا کجاست</p></div>
<div class="m2"><p>کاتش موسی عیان بنمود صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ تیز آهنگ لختی پر فشاند</p></div>
<div class="m2"><p>چون عمود زرفشان بنمود صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قفل رومی برگرفت از درج روز</p></div>
<div class="m2"><p>چون کلید هندوان بنمود صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سماع کوس و بر رقص خروس</p></div>
<div class="m2"><p>خرقه بازی در نهان بنمود صبح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نافهٔ شب را چو زد سیمین کلید</p></div>
<div class="m2"><p>مشک تر در پرنیان بنمود صبح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر محک شب سپیدی شد پدید</p></div>
<div class="m2"><p>چون عیار آسمان بنمود صبح</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا برآرد یوسفی از چاه شب</p></div>
<div class="m2"><p>دلو سیمین ریسمان بنمود صبح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در کمین شرق زال زر هنوز</p></div>
<div class="m2"><p>پر عنقا دیدبان بنمود صبح</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حلقه دیدستی به پشت آینه</p></div>
<div class="m2"><p>حلقهٔ مه همچنان بنمود صبح</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گوئی اندر بر حمایل چرخ را</p></div>
<div class="m2"><p>خنجر شاه اخستان بنمود صبح</p></div></div>
<div class="b2" id="bn14"><p>سام کیخسرو مکان در شرق و غرب</p>
<p>خضر اسکندر نشان در شرق و غرب</p></div>
<div class="b" id="bn15"><div class="m1"><p>صبح خیزان وام جان درخواستند</p></div>
<div class="m2"><p>داد عمری ز آسمان درخواستند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیش کان قرا شود سبوح خوان</p></div>
<div class="m2"><p>در صبوح عیش جان در خواستند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در مناجاتی که سرمستان کنند</p></div>
<div class="m2"><p>جرم آن سبوح خوان در خواستند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نازنینانی که دیر آگه شدند</p></div>
<div class="m2"><p>زود جام زرفشان درخواستند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون به خوابی صبح ازیشان فوت شد</p></div>
<div class="m2"><p>روز را رطل گران درخواستند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر قدح‌های صبوحی شد ز دست</p></div>
<div class="m2"><p>هم به رطلی عذر آن درخواستند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون نهنگان از پی دریا کشی</p></div>
<div class="m2"><p>ساغر کشتی نشان درخواستند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کوه زهره عاشقانند این چنین</p></div>
<div class="m2"><p>کآتشین دریا چنان درخواستند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از زکات جرعهٔ دریاکشان</p></div>
<div class="m2"><p>مفلسان گنج روان درخواستند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جور خواران را جهان انصاف داد</p></div>
<div class="m2"><p>کز خود انصاف جهان درخواستند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ساقیان نیز از پی یک بوس خشک</p></div>
<div class="m2"><p>با زر تر نقد جان درخواستند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون کناری را بها گفتیم چند</p></div>
<div class="m2"><p>صد بهای کاویان درخواستند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چرخ و انجم بر طراز روز نو</p></div>
<div class="m2"><p>کنیت شاه اخستان درخواستند</p></div></div>
<div class="b2" id="bn28"><p>بوالمظفر ظل حق چون آفتاب</p>
<p>مالک الملک جهان در شرق و غرب</p></div>
<div class="b" id="bn29"><div class="m1"><p>پند آن پیر مغان یاد آورید</p></div>
<div class="m2"><p>بانگ مرغ زند خوان یاد آورید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دجله دجله تا خط بغداد جام</p></div>
<div class="m2"><p>می دهید و از کیان یاد آورد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خفتگان را در صبوح آگه کنید</p></div>
<div class="m2"><p>پیل را هندوستان یاد آورید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دانهٔ مرغ بهشتی در دهید</p></div>
<div class="m2"><p>مرغ جان را ز آشیان یاد آورید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر شما بادا که خون رز خورید</p></div>
<div class="m2"><p>خاکیان را در میان یاد آورید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خوان نهید و خوانچهٔ مستان کنید</p></div>
<div class="m2"><p>بی‌خودان را زیر خوان یادآورید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون ز جرعه خاک را رنگی دهید</p></div>
<div class="m2"><p>هم به بوئی ز آسمان یاد آورید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خاص را در آستین جا کرده‌اید</p></div>
<div class="m2"><p>عام را بر آستان یاد آورید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کعبتین را گر سه شش خواهید نقش</p></div>
<div class="m2"><p>نام رندان بر زبان یاد آورید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دوستان تشنه لب را زیر خاک</p></div>
<div class="m2"><p>از نسیم جرعه دان یاد آورید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در شبستان چون زمانی خوش بوید</p></div>
<div class="m2"><p>از شبیخون زمان یاد آورید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>روز شادی را شب غم درقفاست</p></div>
<div class="m2"><p>چون در این باشید از آن یاد آورید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جام زر افشان به خاقانی دهید</p></div>
<div class="m2"><p>خاطرش را درفشان یاد آورید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>راویان را بر زبان تهنیت</p></div>
<div class="m2"><p>مدحت شاه اخستان یاد آورید</p></div></div>
<div class="b2" id="bn43"><p>کسری اسلام، خاقان کبیر</p>
<p>خسرو سلطان نشان در شرق و غرب</p></div>
<div class="b" id="bn44"><div class="m1"><p>راز مستان از میان بیرون فتاد</p></div>
<div class="m2"><p>الصبوح آواز آن بیرون فتاد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ساقی از قیفال خم می‌راند خون</p></div>
<div class="m2"><p>طشت زرین ز آسمان بیرون فتاد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زاهد کوه آستینی برفشاند</p></div>
<div class="m2"><p>ز او کلید خمستان بیرون فتاد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>صوفی قرا کبودی چاک زد</p></div>
<div class="m2"><p>ساغریش از بادبان بیرون فتاد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>باد، دستار مؤذن در ربود</p></div>
<div class="m2"><p>کعبتینی از میان بیرون فتاد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سبحه در کف می‌گذشتم بامداد</p></div>
<div class="m2"><p>بانگ ناقوس مغان بیرون فتاد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مصحفی در بر حمایل داشتم</p></div>
<div class="m2"><p>می فروشی از دکان بیرون فتاد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بند زر از مصحفم در وجه می</p></div>
<div class="m2"><p>بستد و راز نهان بیرون فتاد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پشت خم در خم شدم وز درد خام</p></div>
<div class="m2"><p>خوردم و هوش از روان بیرون فتاد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>یک نشان از درد بر دراعه ماند</p></div>
<div class="m2"><p>دوستی دید و نشان بیرون فتاد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دشمنان بیرون ندادند این حدیث</p></div>
<div class="m2"><p>این حدیث از دوستان بیرون فتاد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جور می‌کش همچنین خاقانیا</p></div>
<div class="m2"><p>خاصه کانصاف از جهان بیرون فتاد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کشتی بهروزی از دریای غیب</p></div>
<div class="m2"><p>بر در شاه اخستان بیرون فتاد</p></div></div>
<div class="b2" id="bn57"><p>چار ملت را سوم جمشید دان</p>
<p>بل دوم مهدیش خوان در شرق و غرب</p></div>
<div class="b" id="bn58"><div class="m1"><p>کوس را دیدی فغان برخاسته</p></div>
<div class="m2"><p>بانگ مرغان بین چنان برخاسته</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اختران آبله مانند را</p></div>
<div class="m2"><p>از رخ گردون نشان برخاسته</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شب چو جعد زنگیان کوته شده</p></div>
<div class="m2"><p>وز عذار آسمان برخاسته</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>روز چون رخسار ترکان از کمال</p></div>
<div class="m2"><p>خال نقصان از میان برخاسته</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مجلس از جام و تنوره گرم و خوش</p></div>
<div class="m2"><p>باد و آتش زاین و آن برخاسته</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>آتش از انگشت بین سر بر زده</p></div>
<div class="m2"><p>روم از هندوستان برخاسته</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نغمهٔ مطرب شده چون نفخ صور</p></div>
<div class="m2"><p>تا قیامت در جهان برخاسته</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>می چو عیسی و ز رومی ارغنون</p></div>
<div class="m2"><p>غنهٔ انجیل‌خوان برخاسته</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گوش بربط تا به چوب انباشته</p></div>
<div class="m2"><p>ناله‌ش از راه زبان برخاسته</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نای بی‌گوش و زبان بسته گلو</p></div>
<div class="m2"><p>از ره چشمش فغان برخاسته</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چنگ بین چون ناقهٔ لیلی وز او</p></div>
<div class="m2"><p>بانگ مجنون هر زمان برخاسته</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بهر دستینه رباب از جام و می</p></div>
<div class="m2"><p>زر و بسد رایگان برخاسته</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>لحن زهره بر دف سیمین ماه</p></div>
<div class="m2"><p>بر در شاه اخستان برخاسته</p></div></div>
<div class="b2" id="bn71"><p>رایت و چتر جلال الدین سزد</p>
<p>صبح و شام آسمان در شرق و غرب</p></div>
<div class="b" id="bn72"><div class="m1"><p>آن نه زلف است آنچنان آویخته</p></div>
<div class="m2"><p>سلسله است از آسمان آویخته</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سلسله گر بهر عدل آویختند</p></div>
<div class="m2"><p>بهر ظلم است او چنان آویخته</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>حلقهٔ گوشت چو عیاران به حلق</p></div>
<div class="m2"><p>زیر زلفت بین نهان آویخته</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>در سر زلف گنه کارت نگر</p></div>
<div class="m2"><p>بی‌گناهان را روان آویخته</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تا سرینت با میان درساخته است</p></div>
<div class="m2"><p>کوهی از مویی روان آویخته</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دل که با بار غمت پیوست، هست</p></div>
<div class="m2"><p>مویی از کوه گران آویخته</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هر زمان یاسج زنان صیاد وار</p></div>
<div class="m2"><p>آئی از بازو کمان آویخته</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>آهوی چشمت بدان زنجیر زلف</p></div>
<div class="m2"><p>جان شیران جهان آویخته</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>عنبرین دستارچه گرد رخت</p></div>
<div class="m2"><p>طوق غبغب در میان آویخته</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>فتنه در فتراک تو بسته عنان</p></div>
<div class="m2"><p>داد خواهان در عنان آویخته</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ای به موئی آسمان را از جفا</p></div>
<div class="m2"><p>بر سر من هر زمان آویخته</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>در تو آویزم چو مویی کز غمت</p></div>
<div class="m2"><p>شد به مویی کار جان آویخته</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>جور بس کن خاصه چون کسری به عدل</p></div>
<div class="m2"><p>شاه زنجیر امان آویخته</p></div></div>
<div class="b2" id="bn85"><p>برق تیغش دیدبان در ملک و دین</p>
<p>ابر جودش میزبان در شرق و غرب</p></div>
<div class="b" id="bn86"><div class="m1"><p>نامرادی را به جان در بسته‌ام</p></div>
<div class="m2"><p>خدمت غم را میان در بسته‌ام</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>عالمی پر تیر باران جفاست</p></div>
<div class="m2"><p>بر حقم گر چشم جان در بسته‌ام</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>آمدم تسلیم در هرچه آیدم</p></div>
<div class="m2"><p>دیدهٔ امید از آن در بسته‌ام</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>سر به تیغ دشمنان در داده‌ام</p></div>
<div class="m2"><p>در به روی دوستان در بسته‌ام</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>روز هم‌جنسان فرو شد لاجرم</p></div>
<div class="m2"><p>روزن دل ز آسمان در بسته‌ام</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>سایهٔ خود هم نبینم تا زیم</p></div>
<div class="m2"><p>آن چنان چشم از جهان در بسته‌ام</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>تا دم من گوش من هم نشنود</p></div>
<div class="m2"><p>سوی لب راه فغان در بسته‌ام</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>تا نیاید غور این غم‌ها پدید</p></div>
<div class="m2"><p>گریه را راه نهان در بسته‌ام</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>هرچه خواهد چرخ گو می‌کن ز جور</p></div>
<div class="m2"><p>کز مکن گفتن زبان در بسته‌ام</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>راز مرغان را سلیمانی نماند</p></div>
<div class="m2"><p>پیش دیوان ز آن دهان در بسته‌ام</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بر زبانم مهر مردان کرده‌اند</p></div>
<div class="m2"><p>همچو طفلان گفت از آن در بسته‌ام</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>خاک در لب کرد خاقانی و گفت</p></div>
<div class="m2"><p>در فروشی را دکان در بسته‌ام</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>همت از کار جهان برداشته</p></div>
<div class="m2"><p>دل به شاه شه‌نشان دربسته‌ام</p></div></div>
<div class="b2" id="bn99"><p>کمترین اقطاع سگبانان اوست</p>
<p>قندهار و قیروان در شرق و غرب</p></div>
<div class="b" id="bn100"><div class="m1"><p>گر جهان شاه جهان می‌خواندش</p></div>
<div class="m2"><p>آسمان هم آسمان می‌خواندش</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>مفخر اول بشر خوانش که دهر</p></div>
<div class="m2"><p>مهدی آخر زمان می‌خواندش</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ز آنکه شیطان سوز و دجال افکن است</p></div>
<div class="m2"><p>آدم مهدی مکان می‌خواندش</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ور صدائی آید از طاق فلک</p></div>
<div class="m2"><p>هم فلک کیوان نشان می‌خواندش</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>آهن تیغش دل اعدا بخورد</p></div>
<div class="m2"><p>مردم، آهن خای از آن می‌خواندش</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>دیده‌ای دندان که خاید استخوان</p></div>
<div class="m2"><p>کادمی هم استخوان می‌خواندش</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>خطبهٔ مدحش چو برخواند آفتاب</p></div>
<div class="m2"><p>مشتری حرز امان می‌خواندش</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>سکهٔ قدرش چو بنوشت آسمان</p></div>
<div class="m2"><p>ماه لوح غیب دان می‌خواندش</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>تیغ شه ماند به لوحی کز دو روی</p></div>
<div class="m2"><p>ملک محراب کیان می‌خواندش</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>نصرت نو زاده تا با تیغ اوست</p></div>
<div class="m2"><p>چرخ طفل لوح‌خوان می‌خواندش</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ابجد تایید بین کز لوح ملک</p></div>
<div class="m2"><p>طفل نصرت چون روان می‌خواندش</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>رنگ جبریل است تیغش را که عقل</p></div>
<div class="m2"><p>وحی پیروزی رسان می‌خواندش</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>خصم شه تا عدهٔ‌دار آرزوست</p></div>
<div class="m2"><p>عاقل آبستن نشان می‌خواندش</p></div></div>
<div class="b2" id="bn113"><p>در شب و روزش دو خادم روز و شب</p>
<p>جوهر این و عنبر آن در شرق و غرب</p></div>
<div class="b" id="bn114"><div class="m1"><p>دست و شمشیرش چنان بینی به هم</p></div>
<div class="m2"><p>کآفتاب و آسمان بینی به هم</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>شاه ملت پاسبان را بر فلک</p></div>
<div class="m2"><p>هفت سلطان پاسبان بینی به هم</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>از نهیبش در چهار ارکان خصم</p></div>
<div class="m2"><p>چار طوفان هر زمان بینی به هم</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>آب خضر و نار موسی یافت شاه</p></div>
<div class="m2"><p>عزم و حزمش زین و آن بینی به هم</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>شه سکندر قدر و اندر موکبش</p></div>
<div class="m2"><p>خضر و موسی همعنان بینی به هم</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>حکم عزرائیل و برهان مسیح</p></div>
<div class="m2"><p>در کف و تیغش عیان بینی به هم</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>دوست و دشمن را رضا و خشم او</p></div>
<div class="m2"><p>عمر بخش و جان ستان بینی به هم</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>چون دو نفخ صور در خشم و رضاش</p></div>
<div class="m2"><p>زهر و پازهر روان بینی به هم</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>خنجر سبزش چو سرخ آید به خون</p></div>
<div class="m2"><p>حصرم و می را نشان بینی به هم</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>تا نه بس دیر از کمال عدل شاه</p></div>
<div class="m2"><p>مصر و ری در شابران بینی به هم</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>از نسیم عدل او هر پنج وقت</p></div>
<div class="m2"><p>چار ملت را امان بینی به هم</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>بر دعای دولتش در شش جهت</p></div>
<div class="m2"><p>هفت مردان یک زبان بینی به هم</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>در ریاض عشرتش در هفت روز</p></div>
<div class="m2"><p>هشت جنت نقل‌دان بینی به هم</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>کنیتش چون بشمری هر هشت حرف</p></div>
<div class="m2"><p>نه فلک را حرز جان بینی به هم</p></div></div>
<div class="b2" id="bn128"><p>خاص بهر لشکرش برساخت چرخ</p>
<p>ترک و هندو دیدبان در شرق و غرب</p></div>
<div class="b" id="bn129"><div class="m1"><p>رمحش از طوفان نشان خواهد نمود</p></div>
<div class="m2"><p>معجز نوح از سنان خواهد نمود</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>تیغ هندیش از مخالف سوختن</p></div>
<div class="m2"><p>در خزر هندوستان خواهد نمود</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>بر ثبات دولت او تا ابد</p></div>
<div class="m2"><p>جنبش عدلش نشان خواهد نمود</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>صبحگاهی کز شبیخون ران گشاد</p></div>
<div class="m2"><p>تیغ چون خور خون‌فشان خواهد نمود</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>سرخی شام آگهی داده است از آنک</p></div>
<div class="m2"><p>روز خوشی در جهان خواهد نمود</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>شبروی کرده کلنگ آسا به روز</p></div>
<div class="m2"><p>همچو شاهین کامران خواهد نمود</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>حلق خصمت در تثاوب جان دهد</p></div>
<div class="m2"><p>کو تمطی بر کمان خواهد نمود</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>چون کمان و تیر شد نون والقلم</p></div>
<div class="m2"><p>نشرهٔ فتح این و آن خواهد نمود</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>جوشن ناخن تنش بدخواه را</p></div>
<div class="m2"><p>تن چو ناخن ز استخوان خواهد نمود</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>شاه موسی کف چو خنجر برکشد</p></div>
<div class="m2"><p>زیر ران طوری روان خواهد نمود</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>خصم فرعونی نسب هم‌چون زنان</p></div>
<div class="m2"><p>دو کدان در زیر ران خواهد نمود</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>پنبه کن ای جان دشمن ز آن تنی</p></div>
<div class="m2"><p>کو ز ترکش دو کدان خواهد نمود</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>سگ گزیده خصم و تیغ شه چو آب</p></div>
<div class="m2"><p>کآتش مرگش عیان خواهد نمود</p></div></div>
<div class="b2" id="bn142"><p>زله‌خوار تیغ و مور خوان اوست</p>
<p>وحش و طیر انس و جان در شرق و غرب</p></div>
<div class="b" id="bn143"><div class="m1"><p>زیرکان کاسرار جان دانسته‌اند</p></div>
<div class="m2"><p>علم جزوی ز آسمان دانسته‌اند</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>از رصدها سیزده سال دگر</p></div>
<div class="m2"><p>خسف بادی در جهان دانسته‌اند</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>قرن‌ها را حکم پیشی کرده‌اند</p></div>
<div class="m2"><p>تا قران‌ها در میان دانسته‌اند</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>در سر میزان ز جمع اختران</p></div>
<div class="m2"><p>بیست و یک نوع از قران دانسته‌اند</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>نابریده برج خاکی را تمام</p></div>
<div class="m2"><p>برج بادیشان مکان دانسته‌اند</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>گرچه هفت اختر به یک جا دیده‌اند</p></div>
<div class="m2"><p>جای کیوان بر کران دانسته‌اند</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>من یقین دانم که ضد آن بود</p></div>
<div class="m2"><p>کاین حکیمان از گمان دانسته‌اند</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>حکمشان باطل‌تر است از علمشان</p></div>
<div class="m2"><p>کاختران را کامران دانسته‌اند</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>هفت هارون بر در سلطان غیب</p></div>
<div class="m2"><p>از چه‌سان فرمان روان دانسته‌اند</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>هفت بیدق عاجز شاه قدر</p></div>
<div class="m2"><p>از چه‌شان لجلاج سان دانسته‌اند</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>عارفان اجرام را در راه امر</p></div>
<div class="m2"><p>هفت پیک رایگان دانسته‌اند</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>کار پیکان نامه بردن دان و بس</p></div>
<div class="m2"><p>پیک را کی نامه‌خوان دانسته‌اند</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>دفع این طوفان بادی را سبب</p></div>
<div class="m2"><p>دولت شاه اخستان دانسته‌اند</p></div></div>
<div class="b2" id="bn156"><p>خاک درگاهش به عرض مصحف است</p>
<p>جای سوگند کیان در شرق و غرب</p></div>
<div class="b" id="bn157"><div class="m1"><p>شاه مغرب کامران ملک باد</p></div>
<div class="m2"><p>آفتاب خاندان ملک باد</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>پیش او هر تاجداری همچو تاج</p></div>
<div class="m2"><p>پشت خم بر آستان ملک باد</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>از پی طغرای منشور ظفر</p></div>
<div class="m2"><p>تیر حکمش بر کمان ملک باد</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>خطی او همچو خط استوا</p></div>
<div class="m2"><p>ناگزیر آسمان ملک باد</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>ظل کعبش کاوفتد بر ساق عرش</p></div>
<div class="m2"><p>زاد سرو بوستان ملک باد</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>تا به جان بینند جنبش سایه را</p></div>
<div class="m2"><p>سایهٔ بالاش جان ملک باد</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>بهر تعویذ سلاطین از ثناش</p></div>
<div class="m2"><p>اسم اعظم در زبان ملک باد</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>کام بختش چون دعای مادران</p></div>
<div class="m2"><p>در اجابت هم‌عنان ملک باد</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>از سر تیغش چو داغ تازیان</p></div>
<div class="m2"><p>ران شیران را نشان ملک باد</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>بر زبان ملک چون نامش رود</p></div>
<div class="m2"><p>آب حیوان در دهان ملک باد</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>از شعاع طلعتش در جام می</p></div>
<div class="m2"><p>نجم سعدین در قران ملک باد</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>بس بقائم ریخت با عدلش جهان</p></div>
<div class="m2"><p>کو چو قائم در جهان ملک باد</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>فضل یزدان در ضمان عمر اوست</p></div>
<div class="m2"><p>عمر او هم در ضمان ملک باد</p></div></div>
<div class="b2" id="bn170"><p>بخت بادش پاسبان و اسلام را</p>
<p>باس عدل پاسبان در شرق و غرب</p></div>