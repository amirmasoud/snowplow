---
title: >-
    شمارهٔ ۱ - در مدح جلال الدین اخستان شروان شاه
---
# شمارهٔ ۱ - در مدح جلال الدین اخستان شروان شاه

<div class="b" id="bn1"><div class="m1"><p>جام ز می دو قله کن خاص برای صبح‌دم</p></div>
<div class="m2"><p>فرق مکن دو قبله دان جام و صفای صبح‌دم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تن چنگ بند رگ وز رگ خم گشای خون</p></div>
<div class="m2"><p>کآتش و مشک زد به هم نافه‌گشای صبح‌دم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام چو دور آسمان درده و زمین فشان</p></div>
<div class="m2"><p>جرعه چنان که برچکد خون به قفای صبح‌دم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرخ قرابهٔ تهی است پارهٔ خاک در میان</p></div>
<div class="m2"><p>پری آن قرابه ده جرعه برای صبح‌دم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلق و لب قنینه بین سرفه‌کنان و خنده زن</p></div>
<div class="m2"><p>خنده بهار عیش دان، سرفه نوای صبح‌دم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقی اگر نه سیب تر بر سر آتش افکند</p></div>
<div class="m2"><p>این همه بوی چون دهد می به هوای صبح‌دم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صورت جام و باده بین معجز دست ساقیان</p></div>
<div class="m2"><p>ماه نو و شفق نگر نور فزای صبح‌دم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باده به گوش ماهیی بیش مده که در جهان</p></div>
<div class="m2"><p>هیچ نهنگ بحرکش نیست سزای صبح‌دم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبح شد از وداع شب با دم سرد و خون دل</p></div>
<div class="m2"><p>جامه دران گرفت کوه، اینت وفای صبح‌دم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمع که در عنان شب زردهٔ بش سیاه بود</p></div>
<div class="m2"><p>از لگد براق جم، مرد بقای صبح‌دم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موکب صبح را فلک دید رکابدار شه</p></div>
<div class="m2"><p>داد حلی اختران نعل بهای صبح‌دم</p></div></div>
<div class="b2" id="bn12"><p>شاه معظم اخستان شهر گشای راستین</p>
<p>داد ده ظفر ستان، ملک خدای راستین</p></div>
<div class="b" id="bn13"><div class="m1"><p>رطل کشان صبح را نزل و نوای تازه بین</p></div>
<div class="m2"><p>زخمه زنان بزم را ساز و نوای تازه بین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رنگ بشد ز مشک شب بوی نماند لاجرم</p></div>
<div class="m2"><p>باد برآبگون صدف غالیه‌سای تازه بین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بید بسوز و باده کن راوق و لعل باده را</p></div>
<div class="m2"><p>چون دم مشک و عود تر عطر فزای تازه بین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سوخته بید و باده‌بین رومی و هندویی بهم</p></div>
<div class="m2"><p>عشرت زنگیانه را برگ و نوای تازه بین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نافهٔ چین کلید زد صبح و کلید عیش را</p></div>
<div class="m2"><p>بر در عده‌دار خم قفل گشای تازه بین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ترک سلاح پوش را زلف چو برهم اوفتد</p></div>
<div class="m2"><p>عقل صلاح کوش را مست هوای تازه بین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاهد روز کز هوا غالیه‌گون غلاله شد</p></div>
<div class="m2"><p>شاهد توست جام می زو تو هوای تازه بین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیست جهان تنگ را جای طرب که دم زنی</p></div>
<div class="m2"><p>ز آن سوی خیمهٔ فلک خم زن و جای تازه بین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زیر پل فلک مجوی آب وفا ز جوی کس</p></div>
<div class="m2"><p>بگذر از این پل کهن آب وفای تازه بین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لهجهٔ راوی مرا منطق طیر در زبان</p></div>
<div class="m2"><p>بر در شاه جم نگین، تحفه دعای تازه بین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قلعهٔ گلستان شه قلهٔ بوقبیس دان</p></div>
<div class="m2"><p>حصن شما خیش حرم کعبه سرای تازه بین</p></div></div>
<div class="b2" id="bn24"><p>رستم کیقباد فر حیدر مصطفی ظفر</p>
<p>همره رخش و دل دلش فتح و غزای راستین</p></div>
<div class="b" id="bn25"><div class="m1"><p>بر ره قول کاسه‌گر نوای نو زند</p></div>
<div class="m2"><p>بر سر خوانچهٔ طرب مرغ صلای نو زند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرغ قنینه چون زبان در دهن قدح کند</p></div>
<div class="m2"><p>جان قدح به صد زبان لاف صفای نو زند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>طاس چو بحر بصره بین جزر و مدش به جرعه‌ای</p></div>
<div class="m2"><p>ساحل خاک را ز در موج عطای نو زند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بزم چو هشت باغ بین باده چهار جوی دان</p></div>
<div class="m2"><p>خاصه که ساز عاشقان حور لقای نو زند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سنگ به لشکر افکند منهی عقل و آخرش</p></div>
<div class="m2"><p>قاضی لشکر مغان حد جفای نو زند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>و آن می عقل دزد هم نقب زند سرای غم</p></div>
<div class="m2"><p>لاجرمش صفیر خوش چنگ سرای نو زند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چنگ بریشمین سلب کرده پلاس دامنش</p></div>
<div class="m2"><p>چون تن زاهدان کز او بوی ریای نو زند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نای چو زاغ کنده پر نغز نوا چو بلبلان</p></div>
<div class="m2"><p>زاغ که بلبلی کند طرفه نوای نو زند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دست رباب را مجس تیز و ضعیف و هر نفس</p></div>
<div class="m2"><p>نبض‌شناس بر رگش نیش عنای نو زند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بربط اگر دم از هوا زد به زبان بی‌دهان</p></div>
<div class="m2"><p>نی به دهان بی زبان دم ز هوای نو زند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنبر دف شود فلک مطرب بزم شاه را</p></div>
<div class="m2"><p>ماه دو تا سبو کشد زهره ستای نو زند</p></div></div>
<div class="b2" id="bn36"><p>شاه خزر گشای را هند و خزر شرف دهد</p>
<p>بر پسر سبکتکین هند گشای راستین</p></div>
<div class="b" id="bn37"><div class="m1"><p>جام و تنوره بین به هم باغ و سرای زندگی</p></div>
<div class="m2"><p>ز آتش و می بهار و گل زاده برای زندگی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر در درج خط قدح از افق تنوره بین</p></div>
<div class="m2"><p>عکس دو آفتاب را نورفزای زندگی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حجرهٔ آهنین نگر، حقهٔ آبگینه بین</p></div>
<div class="m2"><p>لعل در این و زر در آن، کیسه‌گشای زندگی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جام پری در آهن است از همه طرفه‌تر ولی</p></div>
<div class="m2"><p>نقش پری به شیشه بین سحرنمای زندگی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دائرهٔ تنوره بین ریخته نقطه‌های زر</p></div>
<div class="m2"><p>کرده چو سطح آسمان خط سرای زندگی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شبه سپید باز بین بر سر کوه پر طلا</p></div>
<div class="m2"><p>باز سپید روز بین بسته قبای زندگی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>قطره و میغ تیره بین شیره سفید و تخمه کان</p></div>
<div class="m2"><p>عالم دردمند را کرده دوای زندگی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سال نو است و قرص خور خوانچهٔ ماهی افکند</p></div>
<div class="m2"><p>وز بره خوان نو نهد بهر نوای زندگی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تابهٔ زر ندیده‌ای بر سر ماهی آمده</p></div>
<div class="m2"><p>چشمهٔ خور به حوت بین وقت صفای زندگی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ابر چو پیل هندوان آمد و باد پیل‌بان</p></div>
<div class="m2"><p>دیمه روس طبع را کشته به پای زندگی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>روز یکم ز سال نو جشن سکندر دوم</p></div>
<div class="m2"><p>خاک ز جمرهٔ سوم کرده قضای زندگی</p></div></div>
<div class="b2" id="bn48"><p>شاه سکندر هدی، چشمهٔ خضر رای او</p>
<p>بی‌ظلمات چشمه بین زاده ز رای راستین</p></div>
<div class="b" id="bn49"><div class="m1"><p>ای به هزار جان دلم مست وفای روی تو</p></div>
<div class="m2"><p>خانهٔ جان به چار حد وقف هوای روی تو</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>رشتهٔ جان برون کشم هر مژه سوزنی کنم</p></div>
<div class="m2"><p>دیده بدوزم از جهان بهر وفای روی تو</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا چو کبوتران مرا بام تو نقش دیده شد</p></div>
<div class="m2"><p>کافرم ار طلب کنم کعبه به جای روی تو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گرچه چو پشت آینه حلقه به گوش تو شدم</p></div>
<div class="m2"><p>آینه کردم اشک را خاص برای روی تو</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از همه تا همه مرا نیم دل است و یک نفس</p></div>
<div class="m2"><p>هر دو به مهر کرده‌ام بهر رضای روی تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>قفل به سینه برزدم کوست خزینهٔ غمت</p></div>
<div class="m2"><p>قفل خزینه ساختم دست‌گشای روی تو</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>غمزه زنان چو بگذری سنبله موی و مه قفا</p></div>
<div class="m2"><p>روی بتان قفا شود پیش صفای روی تو</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چون به قفای جان دود عمر به پای روز وشب</p></div>
<div class="m2"><p>عمر فشان همی دود جان به قفای روی تو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هر که نظارهٔ تو شد دست بریده می‌شود</p></div>
<div class="m2"><p>یوسف عهدی و جهان نیم بهای روی تو</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هستی خاقنی اگر نیست شد از تو جو به جو</p></div>
<div class="m2"><p>بر دل او به نیم جو باد بقای روی تو</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سمع خدایگان شود چون دهن تو گنج در</p></div>
<div class="m2"><p>چون به زبان من رود مدح و ثنای روی تو</p></div></div>
<div class="b2" id="bn60"><p>پانصد هجرت از جهان هیچ ملک چنو نزاد</p>
<p>از خلفای سلطنت تا خلفای راستین</p></div>
<div class="b" id="bn61"><div class="m1"><p>نیست به پای چون منی راه هوای چون تویی</p></div>
<div class="m2"><p>خود نرسد به هر سری تیغ جفای چون تویی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دل چه سگ است تا بر او قفل وفای تو زنم</p></div>
<div class="m2"><p>کی رسد آن خرابه را قفل وفای چون تویی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بوسه خرانت را همه زر تر است در دهن</p></div>
<div class="m2"><p>وان من است خشک جان بوسه بهای چون تویی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گر چه چراغ در دهن زر عیار دارمی</p></div>
<div class="m2"><p>کی شودی لبم محک از کف پای چون تویی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گه گه اگر زکات لب بوسه دهی به بنده ده</p></div>
<div class="m2"><p>تا به خراج ری زنم لاف عطای چون تویی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همچو سپند پیش تو سوزم و رقص می‌کنم</p></div>
<div class="m2"><p>خود به فدا چنین شود مرد برای چون تویی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گفتی اگرچه خسته‌ای غم مخور این سخن سزد</p></div>
<div class="m2"><p>خود به دلم گذر کند غم به بقای چون تویی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>با همه خستگی دلم بوسه رباید از لبت</p></div>
<div class="m2"><p>گربهٔ شیردل نگر لقمه ربای چون تویی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نوبهٔ خواجگی زنم بهر هوای تو مگر</p></div>
<div class="m2"><p>نشکند از شکستگان قدر هوای چون تویی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بر سر خاقانی اگر دست فرو کنی سزد</p></div>
<div class="m2"><p>کوست دلی و نیم جان روی نمای چون تویی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>از تو به بارگاه شه لاف دو کون می‌زنم</p></div>
<div class="m2"><p>کم ز خراج این دوده نزل گدای چون تویی</p></div></div>
<div class="b2" id="bn72"><p>از شه عیسوی نفس عازر ملک زنده شد</p>
<p>معجزه را همین قدر هست گوای راستین</p></div>
<div class="b" id="bn73"><div class="m1"><p>اهل نماند بر زمین، اینت بلای آسمان</p></div>
<div class="m2"><p>خاک بر آسمان فشان هم ز جفای آسمان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چون پس هر هزار سال اهل دلی نیاورد</p></div>
<div class="m2"><p>این همه جان چه می‌کند دور برای آسمان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ای مه مگو کآسمان اهل برون نمی‌دهد</p></div>
<div class="m2"><p>اهل که نامد از عدم چیست خطای آسمان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>کوه کوه می‌رسد، چون نرسد دل به دل؟</p></div>
<div class="m2"><p>غصهٔ بی‌دلی نگر هم ز عنای آسمان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>با همه دل شکستگی روی به آسمان کنم</p></div>
<div class="m2"><p>آه که قبلهٔ دگر نیست ورای آسمان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>محنت و حال ناپسند، اینت فتوح روز و شب</p></div>
<div class="m2"><p>پلپل و چشم دردمند، اینت دوای آسمان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>باد دریغ در دلم کشت چراغ زندگی</p></div>
<div class="m2"><p>بوی چراغ کشته شد سوی هوای آسمان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بر سر پای جان کنان گردم و طالع مرا</p></div>
<div class="m2"><p>پا و سری پدید نه چون سر و پای آسمان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گرچه به موئی آسمان داشته‌اند بر سرم</p></div>
<div class="m2"><p>موی به موی دیده‌ام تعبیه‌های آسمان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>زعم من است کآسمان سجدهٔ سگدلان کنم</p></div>
<div class="m2"><p>زان چو دم سگان بود پشت دوتای آسمان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بس که قفای آسمان خوردم و یافتم ادب</p></div>
<div class="m2"><p>تا ادب اذ السما کوفت قفای آسمان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>جیب دریده می‌رود گرد قوارهٔ زمین</p></div>
<div class="m2"><p>بو که رسم به محرمی زیر وطای آسمان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نیست فرود آسمان محرم هیچ ناله‌ای</p></div>
<div class="m2"><p>نالهٔ خاقانی از آن رفت ورای آسمان</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>یا کند آسمان قضا عمر مرا که شد به غم</p></div>
<div class="m2"><p>یا کنم از بقای شه دفع قضای آسمان</p></div></div>
<div class="b2" id="bn87"><p>از گهر یزیدیان زاده علی شجاعتی</p>
<p>کز سر ذوالفقار او زاده قضای راستین</p></div>
<div class="b" id="bn88"><div class="m1"><p>تاجور جهان چو جم تخت خدای مملکت</p></div>
<div class="m2"><p>خاتم دیوبند او بند گشای مملکت</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>انس و پریش چون ملک زله‌ربای مائده</p></div>
<div class="m2"><p>دام و ددش چو مورچه هدیه فزای مملکت</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>دیودلان سرکشش حامل عرش سلطنت</p></div>
<div class="m2"><p>مرغ پران ترکشش پیک سبای مملکت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>افسر گوهر کیان، گوهر افسر سران</p></div>
<div class="m2"><p>خاک درش چو کیمیا بیش بهای مملکت</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>عقل که دید طلعتش حرز بر او دمید و گفت</p></div>
<div class="m2"><p>اینت شه ملک سپه، عرش لوای مملکت</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>گفت جهانش ای ملک تو ز کیانی از کیان</p></div>
<div class="m2"><p>گفت ز تخم آرشم نجل بقای مملکت</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>گفت به تیغش آسمان کای گهری تو کیستی</p></div>
<div class="m2"><p>گفت من آتش اجل زهر گیای مملکت</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>گرچه به باطل اختران افسر عاجزان برند</p></div>
<div class="m2"><p>اوست مظفری به حق خانه خدای مملکت</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>مار به ظلم اگر برد خایهٔ موش ناسزا</p></div>
<div class="m2"><p>جان پلنگ چون برد کوست سزای مملکت</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>مشتری از پی ملک کرد سجل خط بقا</p></div>
<div class="m2"><p>بست بنات نعش را عقد برای مملکت</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بدر ستاره لشکر است اوج طراز آسمان</p></div>
<div class="m2"><p>بحر نهنگ خنجر است ابر سخای مملکت</p></div></div>
<div class="b2" id="bn99"><p>بدر چو شعری سیم بحر چو کسری دوم</p>
<p>دولت ظلم کاه او عدل فزای راستین</p></div>
<div class="b" id="bn100"><div class="m1"><p>چون شه پیل‌تن کشد تیغ برای معرکه</p></div>
<div class="m2"><p>غازی هند را نهد پیل به جای معرکه</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بینی از اژدها دلان صف زدگان چو مورچه</p></div>
<div class="m2"><p>خایهٔ مورچه شده چرخ ورای معرکه</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>تیغ نیام بفکند چون گه حشر تن کفن</p></div>
<div class="m2"><p>راست چو صور دردمند از سر نای معرکه</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>اسب به چار صولجان گوی زمین کند هبا</p></div>
<div class="m2"><p>طاق فلک به پا کند هم به هبای معرکه</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بیشه ستان نیزه‌ها ایمن از آتش سنان</p></div>
<div class="m2"><p>شیردلان ز نیزه‌ها بیشه فزای معرکه</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>قلزم تیغ‌ها زده موج به فتح باب کین</p></div>
<div class="m2"><p>زاده ز موج تیغ‌ها صاعقه زای معرکه</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>تیغ کبود غرق خون صوفی کار آب کن</p></div>
<div class="m2"><p>زاغ سیاه پوش را گفته صلای معرکه</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>مغز سران کدوی خشک اشک یلان زرشک تر</p></div>
<div class="m2"><p>زین دو به تیغ چون نمک پخته ابای معرکه</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>تختهٔ خاک رزم را جذر اصم شده ظفر</p></div>
<div class="m2"><p>خنجر شه چو هندوئی جذر گشای معرکه</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>رایت شه تذرو وش لیک عقاب حمله‌بر</p></div>
<div class="m2"><p>پرچم شه غراب گون لیک همای معرکه</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>رشتهٔ جان دشمنان مهرهٔ پشت گردنان</p></div>
<div class="m2"><p>چون به هم آورد کند عقد برای معرکه</p></div></div>
<div class="b2" id="bn111"><p>حلقهٔ تن عدوی او بر سر شه ره اجل</p>
<p>شه چو سماک نیزه‌ور حلقه ربای راستین</p></div>
<div class="b" id="bn112"><div class="m1"><p>عرش نگر به جای تخت آمده پای شاه را</p></div>
<div class="m2"><p>کعبه نگر به قبله درساخته جای شاه را</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>جام کیان به دست شه زمزم مکیان شده</p></div>
<div class="m2"><p>بر مکیان زکات چین گنج عطای شاه را</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>برده مهندس بقا ز آن سوی خطهٔ فلک</p></div>
<div class="m2"><p>خندق حصن ملک را حد سرای شاه را</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چون ز سواد شابران سوی خزر سپه کشد</p></div>
<div class="m2"><p>روس والان نهند سر خدمت پای شاه را</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>ور به سریر بگذرد رایت شاه صاحبش</p></div>
<div class="m2"><p>تاج و سریر خود نهد نعل بهای شاه را</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>هود هدایت است شاه اهل سریر عادیان</p></div>
<div class="m2"><p>صرصر رستخیز دان قوت رای شاه را</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چرخ چو باز ازرق است این شب و روز چون دو سگ</p></div>
<div class="m2"><p>باز و سگ‌اند نامزد صید و هوای شاه را</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>مرغ که آبکی خورد سر سوی آسمان کند</p></div>
<div class="m2"><p>گوئی اشارتی است آن بهر دعای شاه را</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>دهر شکست پشت من نیست به رویش آب شرم</p></div>
<div class="m2"><p>ورنه چنین نداشتی مدح سرای شاه را</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>چرخ چرا به خاک زد گوهر شب چراغ من</p></div>
<div class="m2"><p>کافسر گوهران کنم در ثنای شاه را</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>دیدهٔ شرق و غرب را بر سخنم نظر بود</p></div>
<div class="m2"><p>آه که نیست این نظر عین رضای شاه را</p></div></div>
<div class="b2" id="bn123"><p>دزد بیان من بود هرکه سخنوری کند</p>
<p>شاه سخنوران منم شاه ستای راستین</p></div>
<div class="b" id="bn124"><div class="m1"><p>باد مثال را حکم قضای ایزدی</p></div>
<div class="m2"><p>بر سر هر مثال او مهر رضای ایزدی</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>هفت فلک به خدمتش یکدل و تا ابد زده</p></div>
<div class="m2"><p>چار ملک سه نوبتش در دو سرای ایزدی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>رخنه ز دست هیبتش ناخن شیر آسمان</p></div>
<div class="m2"><p>ناخن دست همتش بحر عطای ایزدی</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>باد دل جهانیان والهٔ نور طلعتش</p></div>
<div class="m2"><p>چون نظر بهشتیان مست لقای ایزدی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>قوت روان خسروان شمهٔ خاک درگهش</p></div>
<div class="m2"><p>چون غذی ملائکه باد ثنای ایزدی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>باد چو باد عیسوی گرد سم براق او</p></div>
<div class="m2"><p>ای پی چشم درد جان شاف شفای ایزدی</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>خامهٔ مار پیکرش باد رقیب گنج دین</p></div>
<div class="m2"><p>مهره و زهر در سرش درد و دوای ایزدی</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>کرده ضمان ازو ظفر فتح و سریر و روس را</p></div>
<div class="m2"><p>او به فزودن ظفر شکرفزای ایزدی</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چرخ ز خنجر زحل ساخته درع دولتش</p></div>
<div class="m2"><p>آینه‌های درع او فر و بهای ایزدی</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>دهر ز چرخ اطلسش کرده ردای کبریا</p></div>
<div class="m2"><p>نقش طراز آن ردا عین بقای ایزدی</p></div></div>
<div class="b2" id="bn134"><p>شاه جهان گشای را از شب و روز آن جهان</p>
<p>باد هزار سال عمر، اینت دعای راستین</p></div>