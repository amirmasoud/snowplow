---
title: >-
    شمارهٔ ۳ - در مدح سلطان مظفر الدین قزل ارسلان
---
# شمارهٔ ۳ - در مدح سلطان مظفر الدین قزل ارسلان

<div class="b" id="bn1"><div class="m1"><p>لاف از دم عاشقان زند صبح</p></div>
<div class="m2"><p>بی‌دل دم سرد از آن زند صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شعلهٔ آه بی‌دلان نقب</p></div>
<div class="m2"><p>در گنبد جان ستان زند صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بازیچهٔ روزگار بیند</p></div>
<div class="m2"><p>بس خنده که بر جهان زند صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبح ارنه مرید آفتاب است</p></div>
<div class="m2"><p>چون آه مریدسان زند صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عاشق شاه اختران نیست</p></div>
<div class="m2"><p>پس چون دم صبح جان فشان زند صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شاهد و شاه بیند از دور</p></div>
<div class="m2"><p>خنده ز میان جان زند صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاهد پس پرده دارد اینک</p></div>
<div class="m2"><p>شاید که دم از نهان زند صبح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن یک دو نفس که دارد از عمر</p></div>
<div class="m2"><p>با شاهد رایگان زند صبح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس بی‌خبر است ز اندکی عمر</p></div>
<div class="m2"><p>ز آن خندهٔ غافلان زند صبح</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معشوق من است صبح اگر نی</p></div>
<div class="m2"><p>چون خندهٔ بی‌دهان زند صبح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نافهٔ مشک شب بسوزد</p></div>
<div class="m2"><p>بس عطسه که آن زمان زند صبح</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوش خوش چو یهود پارهٔ زرد</p></div>
<div class="m2"><p>بر ازرق آسمان زند صبح</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وز زیور اختران به نوروز</p></div>
<div class="m2"><p>تاج قزل ارسلان زند صبح</p></div></div>
<div class="b2" id="bn14"><p>دارای جهان، جهان دولت</p>
<p>بل داور جان و جان دولت</p></div>
<div class="b" id="bn15"><div class="m1"><p>صبح آتشی از نهان برآورد</p></div>
<div class="m2"><p>راز دل آسمان برآورد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن مؤذن سرخ چشم سرمست</p></div>
<div class="m2"><p>قامت به سر زبان برآورد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>امروز به که عمود زد صبح</p></div>
<div class="m2"><p>پس خنجر زرفشان برآورد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جائی که عمود و خنجر آمد</p></div>
<div class="m2"><p>آنجا چه نفس توان برآورد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن کیست که بی‌میانجی صبح</p></div>
<div class="m2"><p>دست طرب از میان برآورد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کاس می و قول کاسه‌گر خواه</p></div>
<div class="m2"><p>چون کوس پگه فغان برآورد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بربط که به طفل خفته ماند</p></div>
<div class="m2"><p>بانگ از بر دایگان برآورد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وز چوب زدن رباب فریاد</p></div>
<div class="m2"><p>چون کودک عشر خوان برآورد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنگ است پلاس پوش پیری</p></div>
<div class="m2"><p>سینه سوی کتف از آن برآورد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دف کز تن آهوان سلب داشت</p></div>
<div class="m2"><p>آواز گوزن سان برآورد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نای است گلو فشرده پس چیست</p></div>
<div class="m2"><p>کز سرفه قنینه جان برآورد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از بس که ره دهان گرفته است</p></div>
<div class="m2"><p>بانگ از ره دیدگان برآورد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون شاه حبش دم تظلم</p></div>
<div class="m2"><p>پیش قزل ارسلان برآورد</p></div></div>
<div class="b2" id="bn28"><p>سلطان کرم مظفر الدین</p>
<p>در جسم ظفر روان دولت</p></div>
<div class="b" id="bn29"><div class="m1"><p>ساغر گوهر از دهان فرو ریخت</p></div>
<div class="m2"><p>ساقی شکر از زبان فرو ریخت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در جام صدف دو بحر دارد</p></div>
<div class="m2"><p>یک دجله به جرعه دان فرو ریخت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون خون سیاوشان صراحی</p></div>
<div class="m2"><p>خوناب دل از دهان فرو ریخت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در کین سیاوش ارغنون زن</p></div>
<div class="m2"><p>آن زخمهٔ درفشان فرو ریخت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گوئی سر زخمه شاخ طوبی است</p></div>
<div class="m2"><p>کو میوهٔ جان چنان فرو ریخت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یا مریم نخل خشک بفشاند</p></div>
<div class="m2"><p>خرمای تر از میان فرو ریخت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون عاشق بوسه زن لب خم</p></div>
<div class="m2"><p>در حلق قنینه جان فرو ریخت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر جان که ز خم ستد قنینه</p></div>
<div class="m2"><p>در باطیه جان کنان فرو ریخت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نالان چو کبوتری که از حلق</p></div>
<div class="m2"><p>خون در لب بچگان فرو ریخت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گوئی که مسیح مرغ جان ساخت</p></div>
<div class="m2"><p>وز دم ببرش روان فرو ریخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سرخاب رخ فلک ده از می</p></div>
<div class="m2"><p>گو آبله از رخان فرو ریخت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از جرعه زمین چو آسمان کن</p></div>
<div class="m2"><p>چون گوهر آسمان فرو ریخت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>صبح از نم ژاله اشک داود</p></div>
<div class="m2"><p>بر مرغ زبور خوان فرو ریخت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در دری ابر خاطر من</p></div>
<div class="m2"><p>پیش قزل ارسلان فرو ریخت</p></div></div>
<div class="b2" id="bn43"><p>اسکندر نامجوی گیتی</p>
<p>کیخسرو کامران دولت</p></div>
<div class="b" id="bn44"><div class="m1"><p>تاج گهر آسمان برانداخت</p></div>
<div class="m2"><p>زرین صدف از نهان برانداخت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>روز آمد و کعبتین بی‌نقش</p></div>
<div class="m2"><p>زان رقعهٔ اختران برانداخت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا یافت محک شب از سپیدی</p></div>
<div class="m2"><p>صراف فلک دکان برانداخت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گوئی خم صرع‌دار شد چرخ</p></div>
<div class="m2"><p>کان زرد کف از دهان برانداخت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>افعی زمردین بپیچید</p></div>
<div class="m2"><p>مهره به سر زبان برانداخت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سرد است هوا هنوز خورشید</p></div>
<div class="m2"><p>بر کوه دواج از آن برانداخت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اینک ز تنوره لشکر جن</p></div>
<div class="m2"><p>بر لشکر دیو جان برانداخت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گوئی شرری که جست از انگشت</p></div>
<div class="m2"><p>هندو به هوا سنان برانداخت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مریخ چو با زحل درآمیخت</p></div>
<div class="m2"><p>پروین سهیل‌سان برانداخت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>طاوس غراب‌خوار هر دم</p></div>
<div class="m2"><p>گاورس ز چینه‌دان برانداخت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در خرگه دوخت روبه سرخ</p></div>
<div class="m2"><p>چون سوزن بی‌کران برانداخت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گوئی که دوباره تیر خونین</p></div>
<div class="m2"><p>نمردود به آسمان برانداخت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یا تاج زر از سر شه زنگ</p></div>
<div class="m2"><p>تیغ قزل ارسلان برانداخت</p></div></div>
<div class="b2" id="bn57"><p>تاج سر و گوهر سلاطین</p>
<p>بل گوهر تاج از آن دولت</p></div>
<div class="b" id="bn58"><div class="m1"><p>مجلس به دو گلستان بر افروز</p></div>
<div class="m2"><p>دیده به دو دلستان برافروز</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یک شب به دو آفتاب بگذار</p></div>
<div class="m2"><p>یک دل به دو عشق دان برافروز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ساقی دو طلب قدح دو بستان</p></div>
<div class="m2"><p>بزم دل ازین و آن برافروز</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از لالهٔ آن و سوسن این</p></div>
<div class="m2"><p>در سینه دو بوستان برافروز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هست از حجر و شجر دو آتش</p></div>
<div class="m2"><p>زان دیده وز آن رخان برافروز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در سوختهٔ شب از دو آتش</p></div>
<div class="m2"><p>یک شعله زن و جهان برافروز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چون صبح و شفق دو جام درخواه</p></div>
<div class="m2"><p>شب چون دل عاشقان برافروز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بر روی دو مه که چون دوصبحند</p></div>
<div class="m2"><p>تا وقت دو صبح جان برافروز</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>با چار لب و دو شاهد از می</p></div>
<div class="m2"><p>سه یک بخور و روان برافروز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>خاشاک دو رنگ روز و شب را</p></div>
<div class="m2"><p>آتش زن و در زمان برافروز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چون روز رسد دو روزن چشم</p></div>
<div class="m2"><p>ز آن خوانچهٔ زرفشان برافروز</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خوانچه کن و از دومی زمین را</p></div>
<div class="m2"><p>چون خوانچهٔ آسمان برافروز</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>دل عود کن و دو دیده مجمر</p></div>
<div class="m2"><p>پیش قزل ارسلان برافروز</p></div></div>
<div class="b2" id="bn71"><p>سردار ملوک هفت اقلیم</p>
<p>روئین‌تن هفت‌خوان دولت</p></div>
<div class="b" id="bn72"><div class="m1"><p>راز زمی آسمان برافکند</p></div>
<div class="m2"><p>بنیاد دی از جهان برافکند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نوروز دو اسبه یک سواری است</p></div>
<div class="m2"><p>کسیب به مهرگان برافکند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>از پشت سیاه زین فرو کرد</p></div>
<div class="m2"><p>بر زردهٔ کامران برافکند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>سلطان یک اسبه سایهٔ چتر</p></div>
<div class="m2"><p>بر ماهی آسمان برافکند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ماهی چو صدف گرش فرو خورد</p></div>
<div class="m2"><p>چون یونسش از دهان برافکند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>پرواز گرفت روز و بر شب</p></div>
<div class="m2"><p>تب‌های دق از نهان برافکند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چون روز کشید دهرهٔ عدل</p></div>
<div class="m2"><p>شب زهرهٔ خون‌فشان برافکند</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گوئی صف آقسنقر آواز</p></div>
<div class="m2"><p>بر خیل قراطغان برافکند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ابر آمد و چون گوزن نالید</p></div>
<div class="m2"><p>بر کوه لعاب از آن برافکند</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گرچه کفن سپید یک چند</p></div>
<div class="m2"><p>بر سبزهٔ مرده‌سان برافکند</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>باد آن کفن سپید برداشت</p></div>
<div class="m2"><p>بس سندس و پرنیان برافکند</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بر چادر کوه گازر آسا</p></div>
<div class="m2"><p>از داغ سیه نشان برافکند</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بر کتف جهان ردای نوروز</p></div>
<div class="m2"><p>فر قزل ارسلان برافکند</p></div></div>
<div class="b2" id="bn85"><p>چون حیدر خانه‌دار اسلام</p>
<p>شاهنشه خاندان دولت</p></div>
<div class="b" id="bn86"><div class="m1"><p>یک اهل دل از جهان ندیدم</p></div>
<div class="m2"><p>دل کو؟ که ز دل نشان ندیدم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چند از دل و دل که در دو عالم</p></div>
<div class="m2"><p>یک دلدل دل روان ندیدم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>صد قافلهٔ وفا فرو شد</p></div>
<div class="m2"><p>یک منقطع از میان ندیدم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>سر نامهٔ روزگار خواندم</p></div>
<div class="m2"><p>عنوان وفا بر آن ندیدم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بیداد به دشمنان نکردم</p></div>
<div class="m2"><p>و انصاف ز دوستان ندیدم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چون طفل که هشت ماهه زاید</p></div>
<div class="m2"><p>می بگذرم و جهان ندیدم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>صد روزه به درد دل گرفتم</p></div>
<div class="m2"><p>عیدی به مراد جان ندیدم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>از خشمگنی کز آسمانم</p></div>
<div class="m2"><p>ماه نو از آسمان ندیدم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چون سگ به زبان جراحت خویش</p></div>
<div class="m2"><p>می‌شویم و مهربان ندیدم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>هرچند جراحت از زبان است</p></div>
<div class="m2"><p>مرهم به جز از زبان ندیدم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چون عیسی فارغم که با خود</p></div>
<div class="m2"><p>جز سوزن سوزیان ندیدم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چون سوزن اگر شکسته گشتم</p></div>
<div class="m2"><p>جز چشم وسری زیان ندیدم</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>از دام دورنگی زمانه</p></div>
<div class="m2"><p>خاقانی را امان ندیدم</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>عادل‌تر خسروان عالم</p></div>
<div class="m2"><p>الا قزل ارسلان ندیدم</p></div></div>
<div class="b2" id="bn100"><p>چون عدل سپاهدار اسلام</p>
<p>چون عقل نگاهبان دولت</p></div>
<div class="b" id="bn101"><div class="m1"><p>از عشوهٔ آسمان مرا بس</p></div>
<div class="m2"><p>وز چاشنی جهان مرا بس</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>آن پرده و این خیال بازی است</p></div>
<div class="m2"><p>از زحمت این و آن مرا بس</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>زین ابلق روزگار دیدن</p></div>
<div class="m2"><p>بر آخور آسمان مرا بس</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>در دخمهٔ چرخ مردگانند</p></div>
<div class="m2"><p>زین جادوی دخمه‌بان مرا بس</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بر بی‌نمکی خوان گیتی</p></div>
<div class="m2"><p>این چشم نمک‌فشان مرا بس</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>دل ندهد و جان ستاند ایام</p></div>
<div class="m2"><p>زین ده دل و جان‌ستان مرا بس</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>موقوف روانم و روان هیچ</p></div>
<div class="m2"><p>زین هودج ناروان مرا بس</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>بیم سرم از سر زبان است</p></div>
<div class="m2"><p>این درد سر زبان مرا بس</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>تا درد سرم فرو نشاند</p></div>
<div class="m2"><p>این اشک گلاب سان مرا بس</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>رنجور نفاق دوستانم</p></div>
<div class="m2"><p>ز آمیزش دوستان مرا بس</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>با صورت خلوه، جلوه کردم</p></div>
<div class="m2"><p>این شاهد غم نشان مرا بس</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>خاقانی را سخن همین است</p></div>
<div class="m2"><p>کز گفتن جان و جان مرا بس</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چرخ ار ندهد قصاص خونم</p></div>
<div class="m2"><p>عدل قزل ارسلان مرا بس</p></div></div>
<div class="b2" id="bn114"><p>جمشید زمانه شاه مغرب</p>
<p>اقطاع ده جهان دولت</p></div>
<div class="b" id="bn115"><div class="m1"><p>ای دل به نوای جان چه باشی</p></div>
<div class="m2"><p>بی‌برگ و نوا نوان چه باشی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>تاری است روان گسسته ده‌جای</p></div>
<div class="m2"><p>چندین به غم روان چه باشی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>لوح ازل و ابد فرو خوان</p></div>
<div class="m2"><p>بنگر که تو زین و آن چه باشی</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>آینده و رفته را نگه کن</p></div>
<div class="m2"><p>بشمر که تو در میان چه باشی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بر خوان فلک جز این دو نان نیست</p></div>
<div class="m2"><p>آتش خور این دو نان چه باشی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>جز آتش خور گرت خورش نیست</p></div>
<div class="m2"><p>در مطبخ آسمان چه باشی</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>روئین دژت ار گشادنی نیست</p></div>
<div class="m2"><p>در محنت هفت‌خوان چه باشی</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>با عبرت گورخانهٔ جان</p></div>
<div class="m2"><p>در عشرت گورخان چه باشی</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>با این همهٔ کرهٔ جهانی</p></div>
<div class="m2"><p>جز در رمهٔ جهان چه باشی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>تقویم مهین حکم شش روز</p></div>
<div class="m2"><p>امروز تویی نهان چه باشی</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>هر سال چو پنج روز تقویم</p></div>
<div class="m2"><p>گم بودهٔ بی‌نشان چه باشی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>از کیسهٔ سال و مه چو آن پنج</p></div>
<div class="m2"><p>دزدیدهٔ رایگان چه باشی</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>خاقانی عاریه است عمرت</p></div>
<div class="m2"><p>از عاریه شادمان چه باشی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>گردانهٔ لطف خواهی الا</p></div>
<div class="m2"><p>مرغ قزل ارسلان چه باشی</p></div></div>
<div class="b2" id="bn129"><p>استاد سرای اوست تقدیر</p>
<p>استاده بر آستان دولت</p></div>
<div class="b" id="bn130"><div class="m1"><p>عزمش گره گمان گشاید</p></div>
<div class="m2"><p>حزمش رصد زمان گشاید</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>با قوت عزم او عجب نیست</p></div>
<div class="m2"><p>گر چنبر آسمان گشاید</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>هر عقدهٔ جوز هرکه مه راست</p></div>
<div class="m2"><p>رمحش به سر سنان گشاید</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>بند دم کژدم فلک را</p></div>
<div class="m2"><p>زان نیزهٔ مارسان گشاید</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>خضر الهامی که چون سکندر</p></div>
<div class="m2"><p>لشکر کشد و جهان گشاید</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>وز خاک سکندر و پی خضر</p></div>
<div class="m2"><p>صد چشمه به امتحان گشاید</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>دریا چو نمک ببندد از سهم</p></div>
<div class="m2"><p>چون لشکر شاه ران گشاید</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>وز بس دم دی مهی عدو را</p></div>
<div class="m2"><p>بز چهره نمک‌ستان گشاید</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>رانده است منجم قدر حکم</p></div>
<div class="m2"><p>کفاق شه کیان گشاید</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>حصنی است فلک دوازده برج</p></div>
<div class="m2"><p>کاقبال خدایگان گشاید</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>هر عقده که روزگار بندد</p></div>
<div class="m2"><p>دست شه کامران گشاید</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>وز گرد مصاف روی نصرت</p></div>
<div class="m2"><p>شاهنشه شه‌نشان گشاید</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>یعنی که نقاب شهربانو</p></div>
<div class="m2"><p>فاروق عجم‌ستان گشاید</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>ابخاز که هست ششدر کفر</p></div>
<div class="m2"><p>گرزش به یکی زمان گشاید</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>روئین دژ روس را علی روس</p></div>
<div class="m2"><p>تیغ قزل ارسلان گشاید</p></div></div>
<div class="b2" id="bn145"><p>چرخ است کبودهٔ به داغش</p>
<p>افشرده به زیر ران دولت</p></div>
<div class="b" id="bn146"><div class="m1"><p>سندان به سنان چنان شکافد</p></div>
<div class="m2"><p>چون صور که آسمان شکافد</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>گر تخت کیان زند به توران</p></div>
<div class="m2"><p>جیحون به سر بنان شکافد</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>دیدی که شکاف مصطفی ماه</p></div>
<div class="m2"><p>او خورشید آنچنان شکافد</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>گر نیل روان شکافت موسی</p></div>
<div class="m2"><p>او دریای دمان شکافد</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>چون خنجر زهرگون کشد شاه</p></div>
<div class="m2"><p>بس زهره که آن زمان شکافد</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>چون تیغ زند سر پلنگان</p></div>
<div class="m2"><p>همچون سم آهوان شکافد</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>بس سینه که چون زبان افعی</p></div>
<div class="m2"><p>زان تیغ نهنگ‌سان شکافد</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>شمشیر دو قطعتش به یک زخم</p></div>
<div class="m2"><p>پهلوی سه پهلوان شکافد</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>گر تیغ علی شکافت فرقی</p></div>
<div class="m2"><p>او البرز از سنان شکافد</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>چاکر به ثنا زبان کند موی</p></div>
<div class="m2"><p>تا موی به امتحان شکافد</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>بکران بهشت جعد سازند</p></div>
<div class="m2"><p>زان موی که این زبان شکافد</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>آه از دل پر زنم چو پسته</p></div>
<div class="m2"><p>کز پری دل دهان شکافد</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>دریای سخن منم اگرچه</p></div>
<div class="m2"><p>هرکس صدف بیان شکافد</p></div></div>
<div class="b2" id="bn159"><p>امروز منم زبان عالم</p>
<p>تیغ تو شها زبان دولت</p></div>
<div class="b" id="bn160"><div class="m1"><p>بی‌حکم تو آسمان نجنبد</p></div>
<div class="m2"><p>بر اسب قضا عنان نجنبد</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>از گوشهٔ چار بالش تو</p></div>
<div class="m2"><p>اقبال به سالیان نجنبد</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>مسجود زمین و آسمان است</p></div>
<div class="m2"><p>تخت تو که از مکان نجنبد</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>یعنی که به عرش و کعبه ماند</p></div>
<div class="m2"><p>چون کعبه و عرش از آن نجنبد</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>بی‌عزم تو رایض فلک را</p></div>
<div class="m2"><p>رگ در تن مرکبان نجنبد</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>مهماز ز پای عزم بگشای</p></div>
<div class="m2"><p>تا ابلق آسمان نجنبد</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>عدل تو اساس شد جهان را</p></div>
<div class="m2"><p>تا مسمار جهان نجنبد</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>لنگی است صلاح پای لنگر</p></div>
<div class="m2"><p>تا کشتی سر گران نجنبد</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>چون حیدر ذوالفقار برکش</p></div>
<div class="m2"><p>تا چرخ جهودسان نجنبد</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>افیون لب فتنه را چنان ده</p></div>
<div class="m2"><p>کز خواب به امتحان نجنبد</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>از خرمگس زمانه فریاد</p></div>
<div class="m2"><p>کز مروحهٔ زمان نجنبد</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>لال است عدوت گرچه اه گفت</p></div>
<div class="m2"><p>کز گفتن اه زبان نجنبد</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>بی‌مدحت تو کلید گفتار</p></div>
<div class="m2"><p>اندر غلق دهان نجنبد</p></div></div>
<div class="b2" id="bn173"><p>پیشت کند آسمان زمین بوس</p>
<p>کای درگهت آسمان دولت</p></div>
<div class="b" id="bn174"><div class="m1"><p>چتر ظفرت نهان مبینام</p></div>
<div class="m2"><p>بی‌رایت تو جهان مبینام</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>پرواز همای بختت الا</p></div>
<div class="m2"><p>بر کرکس آسمان مبینام</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>ماوی گه جیفهٔ حسودت</p></div>
<div class="m2"><p>جز سینهٔ کرکسان مبینام</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>در سرسام حسد عدو را</p></div>
<div class="m2"><p>دردی است که نضج آن مبینام</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>چون شمع و قلم به صورت او را</p></div>
<div class="m2"><p>جز زرد و سیه زبان مبینام</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>بر منشور کمال طغرا</p></div>
<div class="m2"><p>الا قزل ارسلان مبینام</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>بی‌جلوهٔ سکهٔ قبولت</p></div>
<div class="m2"><p>یک نقد هنر روان مبینام</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>بر سکهٔ ملک و خاتم دین</p></div>
<div class="m2"><p>جز نام تو جاودان مبینام</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>بر قلهٔ نه حصار مینا</p></div>
<div class="m2"><p>جز قدر تو دیدبان مبینام</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>همچون هرمان حصار عمرت</p></div>
<div class="m2"><p>محتاج به پاسبان مبینام</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>بر ملکت مصر و قاهره هم</p></div>
<div class="m2"><p>جز قهر تو قهرمان مبینام</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>زین دزد صفیر زن که چرخ است</p></div>
<div class="m2"><p>نقبیت به باغ جان مبینام</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>بی‌مدحت تو به باغ دانش</p></div>
<div class="m2"><p>یک مرغ صفیرخوان مبینام</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>صدر تو که کعبهٔ معالی است</p></div>
<div class="m2"><p>جز قبلهٔ انس و جان مبینام</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>تا دیدهٔ خصم را بدوزی</p></div>
<div class="m2"><p>جز تیز تو در کمان مبینام</p></div></div>
<div class="b2" id="bn189"><p>لطف ازلیت پاسبان باد</p>
<p>شمشیر تو پاسبان دولت</p></div>