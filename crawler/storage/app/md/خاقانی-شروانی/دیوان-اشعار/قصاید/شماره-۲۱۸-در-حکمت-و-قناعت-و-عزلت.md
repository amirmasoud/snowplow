---
title: >-
    شمارهٔ ۲۱۸ - در حکمت و قناعت و عزلت
---
# شمارهٔ ۲۱۸ - در حکمت و قناعت و عزلت

<div class="b" id="bn1"><div class="m1"><p>چو گل بیش ندهم سران را صداعی</p></div>
<div class="m2"><p>کنم بلبلان طرب را وداعی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه از کاس نوشم، نه از کس نیوشم</p></div>
<div class="m2"><p>صبوحی میی، بوالفتوحی سماعی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مه جام و ز افلاک صوت اسم و دارم</p></div>
<div class="m2"><p>چو عیسی بر آن صوت و جام اطلاعی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم گاو دل تا شدم شیر طالع</p></div>
<div class="m2"><p>که طالع کند با دل من نزاعی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین شیر طالع بلرزم چو خوشه</p></div>
<div class="m2"><p>که از شیر لرزد دل هر شجاعی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا طالع ارتفاعی است دیدم</p></div>
<div class="m2"><p>کز این هفت ده نایدم ارتفاعی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنم قصد نه شهر علوی که همت</p></div>
<div class="m2"><p>ازین هفت سفلی نمود امتناعی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی خانه بر یخ بنا دارد ار من</p></div>
<div class="m2"><p>ز چرخ سدابی گشایم فقاعی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازین شقه بر قد همت چه برم</p></div>
<div class="m2"><p>که پیمودمش کمتر است از ذراعی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان نیز چون تنگ چشمان دور است</p></div>
<div class="m2"><p>ازین تنگ چشمی، ازین تنگ باعی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه از جاه جویان توان یافت جاهی</p></div>
<div class="m2"><p>نه از صاع خواهان توان یافت صاعی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه روشندلی زاید از تیره اصلی</p></div>
<div class="m2"><p>نه نیلوفری روید از شوره قاعی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نهم چار بالش در ایوان عزلت</p></div>
<div class="m2"><p>زنم چند نوبت چو میر مطاعی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو یوسف برآیم به تخت قناعت</p></div>
<div class="m2"><p>درآویزم از چهره زرین قناعی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ندارم دل جمعیت، تفرقه به</p></div>
<div class="m2"><p>ببین تا چه بیند مه از اجتماعی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز انسان گریزم کدام انس ایمه</p></div>
<div class="m2"><p>که وحشی صفاتی، بهیمی طباعی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من و سایه هم‌زانو و هم‌نشینی</p></div>
<div class="m2"><p>من و ناله هم‌کاسه و هم رضاعی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنم دفتر عمر وقف قناعت</p></div>
<div class="m2"><p>نویسم بهر صفحه‌ای لایباعی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کرم مرد پس مرثیت گویم او را</p></div>
<div class="m2"><p>ندارم به مدحت دل اختراعی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شب بخل سایه برافکند و اینک</p></div>
<div class="m2"><p>نماند آفتاب کرم را شعاعی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>علی‌القطع نپذیرم اقطاع شاهان</p></div>
<div class="m2"><p>من و ترک اقطاع و پس انقطاعی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو مار و نعامه خورم خاک و آتش</p></div>
<div class="m2"><p>بمیر و نعیمش ندارم طماعی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو نانند کون سوخته و آب رفته</p></div>
<div class="m2"><p>من از آب و نانشان چه سازم ضیاعی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه نان است پس چیست؟ نار الجحیمی</p></div>
<div class="m2"><p>نه آب است پس چیست؟ سر الضباعی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ندارم سپاس خسان، چون ندارم</p></div>
<div class="m2"><p>سوی مال و نان پاره میل و نزاعی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به اول نشاط شراب آن نیرزد</p></div>
<div class="m2"><p>که آخر خمارم رساند صداعی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کتابت نهادن به هر مسجدی به</p></div>
<div class="m2"><p>که جستن به هر مجلسی اصطناعی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مؤدب شوم یا فقیه و محدث</p></div>
<div class="m2"><p>کاحادیث مسند کنم استماعی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به صف النعال فقیهان نشینم</p></div>
<div class="m2"><p>که در صدر شاهان نماند انتفاعی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ور از فقه درمانم آیم به مکتب</p></div>
<div class="m2"><p>نویسم خط ثلث و نسخ و رقاعی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ولیکن گرفتم که هرگز نجویم</p></div>
<div class="m2"><p>نه ملک و منالی، نه مال و متاعی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه ترکی و شاقی، نه تازی براقی</p></div>
<div class="m2"><p>نه رومی بساطی، نه مصری شراعی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هم آخر بنگزیرد از نقد و جنسی</p></div>
<div class="m2"><p>که مستغنیم دارد از انتجاعی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه جامه بباید ز خیر الثیابی</p></div>
<div class="m2"><p>نه جائی بباید به خیر البقاعی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به روزی دو بارم بباید طعامی</p></div>
<div class="m2"><p>به ماهی دو وقتم بباید جماعی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر این اختصار است دیگر نجویم</p></div>
<div class="m2"><p>معاشی که مفرد بود یا مشاعی</p></div></div>