---
title: >-
    شمارهٔ ۶۸ - مطلع دوم
---
# شمارهٔ ۶۸ - مطلع دوم

<div class="b" id="bn1"><div class="m1"><p>تا خیال کعبه نقش دیدهٔ جان دیده‌اند</p></div>
<div class="m2"><p>دیده را از شوق کعبه زمزم افشان دیده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق برکرده به مکه آتشی کز شرق و غرب</p></div>
<div class="m2"><p>کعبه را هر هفت کردهٔ هفت مردان دیده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم بر آن آتش ز هند و چین و بغداد آمده</p></div>
<div class="m2"><p>ماه ذی القعده به روی دجله تابان دیده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماه نو را نیمهٔ قندیل عیسی یافته</p></div>
<div class="m2"><p>دجله را پر حلقهٔ زنجیر مطران دیده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر دجله گذشته تا مداین خضروار</p></div>
<div class="m2"><p>قصر کسری و زیارتگاه سلمان دیده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طاق ایوان جهان‌گیر و وثاق پیر زن</p></div>
<div class="m2"><p>از نکونامی طراز فرش ایوان دیده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تحیر گشته چون زنجیر پیچان کان زمان</p></div>
<div class="m2"><p>بر در ایوان نه زنجیر و نه دربان دیده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاجدارش رفته و دندانه‌های قصر شاه</p></div>
<div class="m2"><p>بر سر دندانه‌های تاج گریان دیده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رانده ز آنجا تا به خاک حله و آب فرات</p></div>
<div class="m2"><p>موقف الشمس و مقام شیر یزدان دیده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس به کوفه مشهد پاک امیر النحل را</p></div>
<div class="m2"><p>همچو جیش نحل جوش انسی و جان دیده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس پلنگان گوزن افکن که چون شاخ گوزن</p></div>
<div class="m2"><p>پشت خم در خدمت آن شیر مردان دیده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در تنور آنجای طوفان دیده اندر چشم و دل</p></div>
<div class="m2"><p>هم تنور غصه هم طوفان احزان دیده‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رانده از رحبه دواسبه تا مناره یکسره</p></div>
<div class="m2"><p>از سم گوران سر شیران هراسان دیده‌اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بختیان چون نوعروسان پای کوبان در سماع</p></div>
<div class="m2"><p>اختران شب پلاس و چرخ کوهان دیده‌اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شب طلاق خواب داده دیده بانان بصر</p></div>
<div class="m2"><p>تا شکر ریز عروسان بیابان دیده‌اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روزها کم خور چو شب‌ها نو عروسان در زفاف</p></div>
<div class="m2"><p>زقه‌هاشان از درای مطرب الحان دیده‌اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حله‌هاشان از پلاس و گیسوانشان از مهار</p></div>
<div class="m2"><p>پاره‌ها خلخال و مشاطه شتربان دیده‌اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در زناشوئی شده سنگ و قدمشان لاجرم</p></div>
<div class="m2"><p>سنگ را از خون بکری رنگ مرجان دیده‌اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سرخ رویانی چو می بی می همه مست خراب</p></div>
<div class="m2"><p>بر هم افتاده چو میگون لعل جانان دیده‌اند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پختگان چون بختیان افتان و خیزان مست شوق</p></div>
<div class="m2"><p>نی نشانی از می و ساقی و می‌دان دیده‌اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وان کژاوه چیست میزان دو کفه باردار</p></div>
<div class="m2"><p>باز جوزایی دو کفه شکل میزان دیده‌اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بارداری چون فلک خوش رو مه و خور در شکم</p></div>
<div class="m2"><p>وز دو سو چون مشرفین او را دو زهدان دیده‌اند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون دو دست اندر تیمم یک به دیگر متصل</p></div>
<div class="m2"><p>در یکی محمل دو تن هم پای و هم ران دیده‌اند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جبرئیل استاده چون اعرابی اشتر سوار</p></div>
<div class="m2"><p>وز پی حاجش دلیل ره فراوان دیده‌اند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بادیه بحر است و بختی کشتی و اعراب موج</p></div>
<div class="m2"><p>واقصه سرحد بحر و مکه پایان داده‌اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دست بالا همت مردم که کرده زیر پای</p></div>
<div class="m2"><p>پای شیبی کان عقوبت گاه شیطان دیده‌اند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بادیه چون غمزهٔ ترکان سنان دار از عرب</p></div>
<div class="m2"><p>جای خون ریزان چو نرگس زار نیسان دیده‌اند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بهر دفع درد چشم رهروان ز آب و گیاش</p></div>
<div class="m2"><p>شیر مادر دختر و گشنیز پستان دیده‌اند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از گلاب ژاله و کافور صبحش در سموم</p></div>
<div class="m2"><p>خیش خانه کسری و سرداب خاقان دیده‌اند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دائرهٔ افلاک را بالای صحن بادیه</p></div>
<div class="m2"><p>کم ز جزم نحویان بر حرف قرآن دیده‌اند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بادیه باغ بهشت و بر سر خوان‌های حاج</p></div>
<div class="m2"><p>پر طاووس بهشتی را مگس ران دیده‌اند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وز طناب خیمه‌ها بر گرد لشکرگاه حاج</p></div>
<div class="m2"><p>صد هزار اشکال اقلیدس به برهان دیده‌اند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>قاع صفصف دیده وصف صف سپهداران حاج</p></div>
<div class="m2"><p>کوس را از زیر دستان زیر و دستان دیده‌اند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چار صف‌های ملک در صفه‌های نه فلک</p></div>
<div class="m2"><p>بر زباله جای استسقای باران دیده‌اند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر سر چاه شقوق از تشنگان صف صف چنانک</p></div>
<div class="m2"><p>پیش یوسف گرسنه چشمان کنعان دیده‌اند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرم گاهی کآفتاب استاده در قلب اسد</p></div>
<div class="m2"><p>سنگ و ریگ ثعلبیه بید و ریحان دیده‌اند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تیره چشمان روان ریگ روان را در زرور</p></div>
<div class="m2"><p>شاف شافی هم ز حصرم هم ز رمان دیده‌اند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از پی حج در چنین روزی ز پانصد سال باز</p></div>
<div class="m2"><p>بر در فید آسمان را منقطع سان دیده‌اند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>من به دور مقتفی دیدم به دی مه بادیه</p></div>
<div class="m2"><p>کاندر او ز آب و گیا قحط فراوان دیده‌اند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پس به عهد مستضی امسال دیدم در تموز</p></div>
<div class="m2"><p>کز تیمم گاه صد نیلوفرستان دیده‌اند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از سحاب فضل و اشک حاج و آب شعر من</p></div>
<div class="m2"><p>برکها را برکه‌های بحر عمان دیده‌اند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کوه محروق آنکه همچون زربه شفشاهنگ در</p></div>
<div class="m2"><p>دیو را زو در شکنجهٔ حبس خذلان دیده‌اند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از دم پاکان که بنشاندی چراغ آسمان</p></div>
<div class="m2"><p>ناف باحورا به حاجر ماه آبان دیده‌اند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وز پی خضر و پر روح القدس چون خط دوست</p></div>
<div class="m2"><p>در سمیرا سدره بر جای مغیلان دیده‌اند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز آب شور نقره و ریگ عسیله ز اعتقاد</p></div>
<div class="m2"><p>سالکان از نقره کان و از عسل شان دیده‌اند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از بسی پر ملک گسترده زیر پای حاج</p></div>
<div class="m2"><p>حاج زیر پای فرش سندس الوان دیده‌اند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سبزی برگ حنا در پای دیده لیک ز اشک</p></div>
<div class="m2"><p>سرخی رنگ حنا در نوک مژگان دیده‌اند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خه‌خه آن ماه نو ذی‌الحجه کز وادی العروس</p></div>
<div class="m2"><p>چون خم تاج عروسان از شبستان دیده‌اند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ماه نو در سایهٔ ابر کبوتر فام راست</p></div>
<div class="m2"><p>جون سحای نامه یا چون عین عنوان دیده‌اند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز آب و خاک سارقیه صفینه پیش چشم</p></div>
<div class="m2"><p>بس دواء المسک و تریافاکه اخوان دیده‌اند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در میان سنگلاخ مسلخ و عمره ز شوق</p></div>
<div class="m2"><p>خار و حنظل گل شکرهای صفاهان دیده‌اند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دشت محرم صحن محشر گشته وز لبیک خلق</p></div>
<div class="m2"><p>نفخهٔ صور اندر این پیروزه پنگان دیده‌اند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از نشاط کعبه در شیر ز قوم احرامیان</p></div>
<div class="m2"><p>شیرهٔ بستان قرین شیر پستان دیده‌اند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شیر زدگان امید و سینه رنجوران عشق</p></div>
<div class="m2"><p>در زقومش هم دو پستان هم سپستان دیده‌اند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زندگان کشته نفس آنجا کفن بر سرکشان</p></div>
<div class="m2"><p>زعفران رخ حنوط نفس ایشان دیده‌اند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شیر مردان چون گوزنان هوی هوی اندر دهان</p></div>
<div class="m2"><p>از هو الله بر خدنگ آه پیکان دیده‌اند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بر در امیدشان قفل از فقل حسبی زده</p></div>
<div class="m2"><p>تا ز دندانه کلیدش سین سبحان دیده‌اند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آمده تانخلهٔ محمود در راه از نشاط</p></div>
<div class="m2"><p>حنظل مخروط را نارنج گیلان دیده‌اند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>جمله در غرقاب اشک و کرده هم سیراب از اشک</p></div>
<div class="m2"><p>خاک غرقاب مصحف را که عطشان دیده‌اند</p></div></div>