---
title: >-
    شمارهٔ ۵ - در نعت پیغمبر اکرم صلی‌الله علیه و آله
---
# شمارهٔ ۵ - در نعت پیغمبر اکرم صلی‌الله علیه و آله

<div class="b" id="bn1"><div class="m1"><p>طفلی هنوز بستهٔ گهوارهٔ فنا</p></div>
<div class="m2"><p>مرد آن زمان شوی که شوی از همه جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهدی بکن که زلزلهٔ صور در رسد</p></div>
<div class="m2"><p>شاه دل تو کرده بود کاخ را رها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان از درون به فاقه و طبع از برون به برگ</p></div>
<div class="m2"><p>دیو از خورش به هیضه و جمشید ناشتا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن به که پیش هودج جانان کنی نثار</p></div>
<div class="m2"><p>آن جان که وقت صدمهٔ هجران شود فنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخش تو را بر آخور سنگین روزگار</p></div>
<div class="m2"><p>برگ گیا نه و خر تو عنبرین چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر پردهٔ عدم زن زخمه ز بهر آنک</p></div>
<div class="m2"><p>برداشته است بهر فرو داشت این نوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در رکعت نخست گرت غفلتی برفت</p></div>
<div class="m2"><p>اینجا سجود سهو کن و در عدم قضا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر حلهٔ حیات مطرز نگرددت</p></div>
<div class="m2"><p>اندیک درنماندت این کسوت از بها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پیل کم نه‌ای که چو مرگش فرا رسد</p></div>
<div class="m2"><p>در حال استخوانش بیرزد بدان بها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از استخوان پیل ندیدی که چرب دست</p></div>
<div class="m2"><p>هم پیل سازد از پی شطرنج پادشا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>امروز سکه ساز که دل دار ضرب توست</p></div>
<div class="m2"><p>چون دل روانه شد نشود نقد تو روا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اکنون طلب دوا که مسیح تو بر زمی است</p></div>
<div class="m2"><p>کانگه که رفت سوی فلک فوت شد دوا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیمار به سواد دل اندر نیاز عشق</p></div>
<div class="m2"><p>مجروح به قبای گل از جنبش صبا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عشق آتشی است کاتش دوزخ غذای اوست</p></div>
<div class="m2"><p>پس عشق روزه دار و تو در دوزخ هوا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در ایرمان سرای جهان نیست جای دل</p></div>
<div class="m2"><p>دیر از کجا و خلعت بیت الله از کجا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بنگر چه ناخلف پسری کز وجود تو</p></div>
<div class="m2"><p>دار الخلافهٔ پدر است ایرمان سرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در جستجوی حق شو و شبگیر کن از آنک</p></div>
<div class="m2"><p>ناجسته خاک ره به کف آید نه کیمیا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بالا برآر نفس چلیپا پرست از آنک</p></div>
<div class="m2"><p>عیسی توست نفس و صلیب است شکل لا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر در سموم بادیهٔ لا تبه شوی</p></div>
<div class="m2"><p>آرد نسیم کعبهٔ الا اللهت شفا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لا را ز لات باز ندانی به کوی دین</p></div>
<div class="m2"><p>گر بی‌چراغ عقل روی راه انبیا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اول ز پیشگاه قدم عقل زاد و بس</p></div>
<div class="m2"><p>آری که از یکی یکی آید به ابتدا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عقل جهان طلب در آلودگی زند</p></div>
<div class="m2"><p>عقل خدا پرست زند درگه صفا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کتف محمد از در مهر نبوت است</p></div>
<div class="m2"><p>بر کتف بیور اسب بود جای اژدها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>با عقل پای کوب که پیری است ژنده پوش</p></div>
<div class="m2"><p>بر فقر دست کش که عروسی است خوش لقا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جان را به فقر باز خر از حادثات از آنک</p></div>
<div class="m2"><p>خوش نیست این غریب نوآئین در این نوا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اندر جزیره‌ای و محیط است گرد تو</p></div>
<div class="m2"><p>زین سوت موج محنت و زان سو شط بلا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از رمز درگذر که زمین چون جزیره‌ای است</p></div>
<div class="m2"><p>گردون به گرد او چو محیط است در هوا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از گشت روزگار سلامت مجوی از آنک</p></div>
<div class="m2"><p>هرگز سراب پر نکند قربهٔ سقا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در قمرهٔ زمانه فتادی به دست خون</p></div>
<div class="m2"><p>وامال کعبتین که حریفی است بس دغا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فرسوده دان مزاج جهان را به ناخوشی</p></div>
<div class="m2"><p>آلوده دان دهان مشعبد به گندنا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اینجا مساز عیش که بس بینوا بود</p></div>
<div class="m2"><p>در قحط سال کنعان دکان نانوا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زین غرقگان رو که نهنگ است برگذر</p></div>
<div class="m2"><p>زین سبزه زار خیز که زهر است در گیا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گیتی سیاه خانه شد از ظلمت وجود</p></div>
<div class="m2"><p>گردون کبود جامه شد از ماتم وفا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از خشک سال حادثه در مصطفی گریز</p></div>
<div class="m2"><p>کاینک به فتح باب ضمان کرد مصطفی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ورد تو این بس است که ای غیث، الغیاث</p></div>
<div class="m2"><p>کز فیض او به سنگ فسرده رسد نما</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بودند تا نبود نزولش در این سرای</p></div>
<div class="m2"><p>این چار مادر و سه موالید بینوا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شاهنشهی است احمد مرسل که ساخت حق</p></div>
<div class="m2"><p>تاج ازل کلاهش و درع ابد قبا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آن قابل امانت در قالب بشر</p></div>
<div class="m2"><p>وان عامل ارادت در عالم جزا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون نوبت نبوت او در عرب زدند</p></div>
<div class="m2"><p>از جودی و احد صلوات آمدش صدا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر خوان این جهان زده انگشت بر نمک</p></div>
<div class="m2"><p>ناخورده دست شسته ازین بی‌نمک ابا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آزاد کردهٔ در او بود عقل و او</p></div>
<div class="m2"><p>چون عقل هم شهنشه و هم پاسبان ما</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>او رحمت خداست جهان خدای را</p></div>
<div class="m2"><p>از رحمت خدای شوی خاصهٔ خدا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ای هست‌ها ز هستی ذات تو عاریت</p></div>
<div class="m2"><p>خاقانی از عطای تو هست آیت ثنا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرغی چنین که دانه و آبش ثنای توست</p></div>
<div class="m2"><p>مپسند کز نشیمن عالم کشد جفا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از عالم دو رنگ فراغت دهش چنانک</p></div>
<div class="m2"><p>دیگر ندارد این زن رعناش در عنا</p></div></div>