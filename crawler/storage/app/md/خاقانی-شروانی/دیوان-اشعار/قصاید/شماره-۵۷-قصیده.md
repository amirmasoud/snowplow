---
title: >-
    شمارهٔ ۵۷ - قصیده
---
# شمارهٔ ۵۷ - قصیده

<div class="b" id="bn1"><div class="m1"><p>بس بس ای طالع خاقانی چند</p></div>
<div class="m2"><p>چند چندش به بلا داری بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جو به جو راز دلش دانستی</p></div>
<div class="m2"><p>که به یک نان جوین شد خرسند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدوانش که دوانیدن تو</p></div>
<div class="m2"><p>مرکب عزم وی از پای فکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ را چون بدوانند نخست</p></div>
<div class="m2"><p>بکشندش ز پی دفع گزند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ازو مرغ نداری، مدوان</p></div>
<div class="m2"><p>ور دوانیدی کشتن مپسند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس ندیده است نمد زینش خشک</p></div>
<div class="m2"><p>سست شد لاشه به جاییش ببند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مچشانش به تموز آب سقر</p></div>
<div class="m2"><p>مفشان بر سر آتش چو سپند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فصل با حورا، آهنگ به شام</p></div>
<div class="m2"><p>وصل با حوران خوش‌تر به خجند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم توانیش به تبریز نشاند</p></div>
<div class="m2"><p>هم توانیش ز شروان بر کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طفل‌خو گشت میازارش بیش</p></div>
<div class="m2"><p>بر چنین طفل مزن بانگ بلند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دایگی کن به نوازش که نزاد</p></div>
<div class="m2"><p>پانصد هجرت ازو به فرزند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست جز اشک کسش هم زانو</p></div>
<div class="m2"><p>نیست جز سایه کسش هم پیوند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حکم حق رانش چون قاضی خوی</p></div>
<div class="m2"><p>نطق دستانش چون پیر مرند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از برون در خوی خوییش مدار</p></div>
<div class="m2"><p>وز درونش دل مجروح مرند</p></div></div>