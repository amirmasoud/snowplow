---
title: >-
    شمارهٔ ۳ - در پند و اندرز و مدح پیامبر بزرگوار
---
# شمارهٔ ۳ - در پند و اندرز و مدح پیامبر بزرگوار

<div class="b" id="bn1"><div class="m1"><p>عروس عافیت آنگه قبول کرد مرا</p></div>
<div class="m2"><p>که عمر بیش‌بها دادمش به شیربها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو کشت عافیتم خوشه در گلو آورد</p></div>
<div class="m2"><p>چو خوشه باز بریدم گلوی کام و هوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خروس کنگرهٔ عقل پر بکوفت چو دید</p></div>
<div class="m2"><p>که در شب امل من سپیده شد پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو ماه سی شبه ناچیز شد خیال غرور</p></div>
<div class="m2"><p>چو روز پانزده ساعت کمال یافت ضیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسیح وار پی راستی گرفت آن دل</p></div>
<div class="m2"><p>که باژ گونه روی بود چون خط ترسا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مرغزار سلامت در مراست خبر</p></div>
<div class="m2"><p>که هم مسیح خبر دارد از مزاج گیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا طبیب دل اندرز گونه‌ای کرده است</p></div>
<div class="m2"><p>کز این سواد بترس از حوادث سودا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تلخ و ترش رضا ده به خوان گیتی بر</p></div>
<div class="m2"><p>که نیشتر خوری ار بیشتر خوری حلوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسیر طبع مخالف مدار جان و خرد</p></div>
<div class="m2"><p>زبون چارزبانی مکن دو حور لقا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که پوست پاره‌ای آمد هلاک دولت آن</p></div>
<div class="m2"><p>که مغز بی‌گنهان را دهد به اژدها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا شهنشه وحدت ز داغ گاه خرد</p></div>
<div class="m2"><p>به شیب و مقرعه دعوت همی کند که بیا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از این سراچهٔ آوا و رنگ دل بگسل</p></div>
<div class="m2"><p>به ارغوان ده رنگ و به ارغنون آوا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در این رصد گه خاکی چه خاک می‌بیزی</p></div>
<div class="m2"><p>نه کودکی نه مقامر ز خاک چیست تو را؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به دست آز مده دل که بهر فرش کنشت</p></div>
<div class="m2"><p>ز بام کعبه ند زدند مکیان دیبا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به بوی نفس مکن جان که بهر گردن خوک</p></div>
<div class="m2"><p>کسی نبرد زنجیر مسجد الاقصا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ببین که کوکبهٔ عمر خضر وار گذشت</p></div>
<div class="m2"><p>تو بازمانده چو موسی به تیه خوف و رجا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پریر نوبت حج بود و مهد خواجه هنوز</p></div>
<div class="m2"><p>از آن سوی عرفات است چشم بر فردا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به چاه جاه چه افتی و عمر در نقصان</p></div>
<div class="m2"><p>به قصد فصد چه کوشی و ماه در جوزا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برفت روز و تو چون طفل خرمی آری</p></div>
<div class="m2"><p>نشاط طفل نماز دگر بود عذرا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو عمر دادی دنیا بده که خوش نبود</p></div>
<div class="m2"><p>به صد خزینه تبذل به دانگی استقصا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دو رنگی شب و روز سپهر بوقلمون</p></div>
<div class="m2"><p>پرند عمر تو را می‌برند رنگ و بها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دو چشمه‌اند یکی قیر و دیگری سیماب</p></div>
<div class="m2"><p>شب بنفشه وش و روز یاسمین سیما</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو غرق چشمهٔ سیماب و قیر و پنداری</p></div>
<div class="m2"><p>که گرد چشمهٔ حیوان و کوثری به چرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جهان به چشمی ماند در او سیاه و سپید</p></div>
<div class="m2"><p>سپید ناخنه دار تو سیاه نابینا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ببر طناب هوس پیش از آنکه ایامت</p></div>
<div class="m2"><p>چهار میخ کند زیر خیمهٔ خضرا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به صور نیم شبی درفکن رواق فلک</p></div>
<div class="m2"><p>به ناوک سحری بر شکن مصاف فضا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهان به بوالعجبی تا کیت نماید لعب</p></div>
<div class="m2"><p>به هفت مهرهٔ زرین و حقهٔ مینا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو را به مهره و حقه فریفتند ایراک</p></div>
<div class="m2"><p>چو حقه بی‌دل و مغزی چو مهره بی سر و پا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فریب گنبد نیلوفری مخور که کنون</p></div>
<div class="m2"><p>اجل چو گنبد گل برشکافدت عمدا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز خشک سال حوادث امید امن مدار</p></div>
<div class="m2"><p>که در تموز ندارد دلیل برف هوا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه جای راحت و امن است و دهر پر نکبت</p></div>
<div class="m2"><p>چه روز باشه و صید است دست پر نکبا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مگو که دهر کجا خون خورد که نیست دهانش</p></div>
<div class="m2"><p>ببین به پشه که زوبین زن است و نیست کیا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مساز عیش که نامردم است طبع جهان</p></div>
<div class="m2"><p>مخور کرفس که پر کژدم است بوم و سرا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز روزگار وفا هم به روزگار آید</p></div>
<div class="m2"><p>که حصرم از پس شش ماه می‌شود صهبا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه خوش بوی که درون وحشت است و بیرون غم</p></div>
<div class="m2"><p>کجا روی که ز پیش آتش است و پس دریا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خوشی طلب کنی از دهر، ساده دل مردا</p></div>
<div class="m2"><p>که از زکات ستانان زکات خواست عطا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سلاح کار خود اینجا ز بی زبانی ساز</p></div>
<div class="m2"><p>که بی زبان دفع زبانیه است آنجا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو خوشه چند شوی صد زبان نمی‌خواهی</p></div>
<div class="m2"><p>که یک زبان چون ترازو بوی به روز جزا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در این مقام کسی کو چو مار شد دو زبان</p></div>
<div class="m2"><p>چو ماهی است بریده زبان در آن ماوا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خرد خطیب دل است و دماغ منبر او</p></div>
<div class="m2"><p>زبان به صورت تیغ و دهان نیام آسا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>درون کام نهان کن زبان که تیغ خطیب</p></div>
<div class="m2"><p>برای نام بود در برش نه بهر وغا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زبان به مهر کن و جز بگاه لا مگشای</p></div>
<div class="m2"><p>که در ولایت قالوابلی رسی از لا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دو اسبه بر اثر لا بران بدان شرطی</p></div>
<div class="m2"><p>که رخت نفکنی الا به منزل الا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مگر معاملهٔ لا اله الا الله</p></div>
<div class="m2"><p>درم خرید رسول اللهت کند به بها</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زبان ثناگر درگاه مصطفی خوشتر</p></div>
<div class="m2"><p>که بارگیر سلیمان نکوتر است صبا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ثنای او به دل ما فرو نیاید از آنک</p></div>
<div class="m2"><p>عروس سخت شگرف است و حجله نا زیبا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سپید روی ازل مصطفی است کز شرفش</p></div>
<div class="m2"><p>سیاه گشت به پیرانه سر، سر دنیا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فلک به دایگی دین او در این مرکز</p></div>
<div class="m2"><p>زنی است بر سر گهواره‌ای بمانده دوتا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دمش خزینه‌گشای مجاهز ارواج</p></div>
<div class="m2"><p>دلش خلیفهٔ کتاب علم الاسما</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به پیش کاتب وحیش دوات دار، خرد</p></div>
<div class="m2"><p>به فرق حاجب بارش نثار بار خدا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هزار فصل ربیعش جنیبه دار جمال</p></div>
<div class="m2"><p>هزار فضل ربیعش خریطه دار سخا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زبان در آن دهن پاک گوئیا که مگر</p></div>
<div class="m2"><p>میان چشمهٔ خضر است ماهیی گویا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دو شاخ گیسوی او چون چهار بیخ حیات</p></div>
<div class="m2"><p>به هر کجا که اثر کرد اخرج المرعی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نه باد گیسوی او ز آتش بهار کم است</p></div>
<div class="m2"><p>که آب و گل را آبستنی دهد ز نما</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عروس دهر و سرور جهان نخواست از آنک</p></div>
<div class="m2"><p>نداشت از غم امت به این و آن پروا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از این حریف گلو بر حذر گزید حذر</p></div>
<div class="m2"><p>وز این ابای گلوگیر ابا نمود ابا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چهار یارش تا تاج اصفیا نشدند</p></div>
<div class="m2"><p>نداشت ساعد دین یاره داشتن یارا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>الهی از دل خاقانی آگهی که در او</p></div>
<div class="m2"><p>خزینه خانهٔ عشق است در به مهر رضا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از آن شراب که نامش مفرح کرم است</p></div>
<div class="m2"><p>به رحمت این جگر گرم را بساز دوا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز هرچه زیب جهان است و هرکه ز اهل جهان</p></div>
<div class="m2"><p>مرا چو صفر تهی دار و چون الف تنها</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>قنوت من به نماز و نیاز در این است</p></div>
<div class="m2"><p>که عافنا و قنا شر ما قضیت لنا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مرا به منزل الا الذین فرود آور</p></div>
<div class="m2"><p>فرو گشای ز من طمطراق الشعرا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یقین من تو شناسی ز شک مختصران</p></div>
<div class="m2"><p>که علم توست شناسای ربنا ارنا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مرا ز آفت مشتی زیاد باز رهان</p></div>
<div class="m2"><p>که بر زنای زن زید گشته‌اند گوا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خلاص ده سخنم را ز غارت گرهی</p></div>
<div class="m2"><p>که مولع‌اند به نقش ریا و قلب ریا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به روز حشر که آواز لاتخف شنوند</p></div>
<div class="m2"><p>به گوش خاطر ایشان رسان که لابشری</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو کاسه باز گشاده دهان ز جوع الکلب</p></div>
<div class="m2"><p>چو کوزه پیش نهاده شکم ز استسقا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>اگر خسیسی بر من گران سر است رواست</p></div>
<div class="m2"><p>که او زمین کثیف است و من سمای سنا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گر او نشسته و من ایستاده‌ام شاید</p></div>
<div class="m2"><p>نشسته باد زمین و ستاده باد سما</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ور او به راحت و من در مشقتم چه عجب</p></div>
<div class="m2"><p>که هم زمین بود آسوده و آسمان دروا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سخن به است که ماند ز مادر فکرت</p></div>
<div class="m2"><p>که یادگار هم اسما نکوتر از اسما</p></div></div>