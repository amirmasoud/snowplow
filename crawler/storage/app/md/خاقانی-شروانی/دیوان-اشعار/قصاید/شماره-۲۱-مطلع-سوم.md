---
title: >-
    شمارهٔ ۲۱ - مطلع سوم
---
# شمارهٔ ۲۱ - مطلع سوم

<div class="b" id="bn1"><div class="m1"><p>صبح دمان دوش خضر بر درم آمد به تاب</p></div>
<div class="m2"><p>کرد به آواز نرم صبحک الله خطاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قدمش چون فلک رقص کنان شد زمین</p></div>
<div class="m2"><p>هم چو ستاره به صبح خانه گرفت اضطراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیک جهان رو چو چرخ، پیر جوان‌وش چو صبح</p></div>
<div class="m2"><p>یافته پیرانه سر رونق فصل شباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علم چهل صبح را مکتبی آراسته</p></div>
<div class="m2"><p>روح مثاله نویس نوح خلیفه کتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکهت و جوشش ز عشق مشک فشان از فقاع</p></div>
<div class="m2"><p>شیبت مویش به صبح برف نمای از سداب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دید مرا مست صبح با دلم از هر دو کون</p></div>
<div class="m2"><p>عشق ببسته گرو فقر کشیده جناب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آبلهٔ سینه دید زلزلهٔ آه من</p></div>
<div class="m2"><p>سقف فلک را به صبح کرد خراب و یباب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت دمیده است صبح منشین خاقانیا</p></div>
<div class="m2"><p>حضرت خاقان شناس مقصد حسن المآب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زادهٔ خاطر بیار کز دل شب زاد صبح</p></div>
<div class="m2"><p>کرد در این سبز طشت خایهٔ زرین عزاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاطر تو مرغ وار هست به پرواز عقل</p></div>
<div class="m2"><p>یافته هر صبح دم دانهٔ اهل ثواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیز به شمشیر صبح سر ببر این مرغ را</p></div>
<div class="m2"><p>تحفهٔ نوروز ساز پیش شه کامیاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه عراقین طراز کز پی توقیع او</p></div>
<div class="m2"><p>کاغذ شامی است صبح خامهٔ مصری شهاب</p></div></div>