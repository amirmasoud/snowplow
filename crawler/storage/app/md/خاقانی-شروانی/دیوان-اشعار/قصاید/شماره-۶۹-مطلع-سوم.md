---
title: >-
    شمارهٔ ۶۹ - مطلع سوم
---
# شمارهٔ ۶۹ - مطلع سوم

<div class="b" id="bn1"><div class="m1"><p>دشت موقف را لباس از جوهر جان دیده‌اند</p></div>
<div class="m2"><p>کوه رحمت را اساس از گوهر کان دیده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرضگاه دشت موقف عرض جنات است از آنک</p></div>
<div class="m2"><p>مصنع او کوثر و سقاش رضوان دیده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حوت و سرطان است جای مشتری وان برکه هست</p></div>
<div class="m2"><p>مشتری صفوت که در وی حوت و سرطان دیده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه رحمت حرمتی دارد که پیش قدر او</p></div>
<div class="m2"><p>کوه قاف و نقطهٔ فا هر دو یکسان دیده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگ ریزهٔ کوه رحمت برده‌اند از بهر کحل</p></div>
<div class="m2"><p>دیده‌بانانی که عرض از کوه لبنان دیده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اصفیا را پیش کوه استاده سوزان دل چو شمع</p></div>
<div class="m2"><p>همچو شمع از اشک غرق و خشک دامان دیده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هشتم ذیحجه در موقف رسیده چاشت گاه</p></div>
<div class="m2"><p>شامگه خود را به هفتم چرخ مهمان دیده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب فراز کوه، ز اشک شور جمع و نور شمع</p></div>
<div class="m2"><p>ابر در افشان و خورشید زر افشان دیده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افتاب از غرب گفتی بازگشت از بهر حاج</p></div>
<div class="m2"><p>چون نماز دیگری بهر سلیمان دیده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتی از مغرب به مشرق کرد رجعت آفتاب</p></div>
<div class="m2"><p>لاجرم حاج از حد بابل خراسان دیده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نسیم مغفرت کآبی و خاکی یافته</p></div>
<div class="m2"><p>آتشی را از انا گفتن پشیمان دیده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وز فراوان ابر رحمت ریخته باران فضل</p></div>
<div class="m2"><p>رانده‌ای را بر امید عفو شادان دیده‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حج ما آدینه و ما غرق طوفان کرم</p></div>
<div class="m2"><p>خود به عهد نوح هم آدینه طوفان دیده‌اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون کریمان کز عطای داده‌شان نسیان بود</p></div>
<div class="m2"><p>عفو حق را از خطای خلق نسیان دیده‌اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خلق هفتاد و سه فرقت کرده هفتاد و دو حج</p></div>
<div class="m2"><p>انسی و جنی و شیطانی مسلمان دیده‌اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حاج رانو نو در افزای از ملادک کرده حق</p></div>
<div class="m2"><p>هر چه در شش صد هزار اعداد نقصان دیده‌اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای برید صبح سوی شام و ایران بر خبر</p></div>
<div class="m2"><p>زین شرف کامسال اهل شام و ایران دیده‌اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای زبان آفتاب احرار کیهان را بگوی</p></div>
<div class="m2"><p>دولتی کز حج اکبر حاج کیهان دیده‌اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نز سموم آسیب و نز باران بخیلی یافته</p></div>
<div class="m2"><p>نز خفاجه بیم و نز غزیه عصیان دیده‌اند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رانده زاول شب بر آن که پایه و بشکسته سنگ</p></div>
<div class="m2"><p>نیم شب مشعل به مشعر نور غفران دیده‌اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بامدادان نفس حیوان کرده قربان در منی</p></div>
<div class="m2"><p>لیک قربان خواص از نفس انسان دیده‌اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با سیاهی سنگ کعبه همبر آید در شرف</p></div>
<div class="m2"><p>سرخی سنگ منی کز خون حیوان دیده‌اند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سعد ذابح بهر قربان تیغ مریخ آخته</p></div>
<div class="m2"><p>جرم کیوانش چو سنگ مکی افسان دیده‌اند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون بره کید به مادر گوسپند چرخ را</p></div>
<div class="m2"><p>سوی تیغ حاج پویان و غریوان دیده‌اند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی‌زبانان با زبان بی‌زبانی شکر حق</p></div>
<div class="m2"><p>گفته وقت کشتن و حق را زباندان دیده‌اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در سه جمره بود پیش مسجد خیف اهل خوف</p></div>
<div class="m2"><p>سنگ را کانداخته بر دیو غضبان دیده‌اند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آمده در مکه و چون قدسیان بر گرد عرض</p></div>
<div class="m2"><p>عرش را بر گرد کعبه طوف و جولان دیده‌اند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پیش کعبه گشته چون باران زمین بوس از نیاز</p></div>
<div class="m2"><p>و آسمان را در طوافش هفت دوران دیده‌اند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عید ایشان کعبه وز ترتیب پنج ارکان حج</p></div>
<div class="m2"><p>رکن پنجم هفت طوف چار ارکان دیده‌اند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رفته و سعی صفا و مروه کرده چار و سه</p></div>
<div class="m2"><p>هم بر آن ترتیب کز سادات و اعیان دیده‌اند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پس برای عمره کردن سوی تنعیم آمده</p></div>
<div class="m2"><p>هم بر آن آئین که حج را ساز و سامان دیده‌اند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حاج را دیوان اعمال است وانگه عمره را</p></div>
<div class="m2"><p>ختم اعمال و فذلک‌های دیوان دیده‌اند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کعبه در دست سیاهان عرب دیده چنانک</p></div>
<div class="m2"><p>چشمهٔ حیوان به تاریکی گروگان دیده‌اند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آنچه دیده دشمنان کعبه از مرغان به سنگ</p></div>
<div class="m2"><p>دوستان کعبه از غوغا دو چندان دیده‌اند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بهترین جایی به دست بدترین قومی گرو</p></div>
<div class="m2"><p>مهرهٔ جاندار و اندر مغز ثعبان دیده‌اند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نی ز ایزد شرم و نی از کعبه آزرم ای دریغ</p></div>
<div class="m2"><p>جای شیران را سگان سور سکان دیده‌اند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در طواف کعبه چون شوریدگان از وجد و حال</p></div>
<div class="m2"><p>عقل را پیرانه سر در ام صبیان دیده‌اند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ذات حق سلطانان و کعبه دار ملک</p></div>
<div class="m2"><p>مصطفی را شحنه و منشور قرآن دیده‌اند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون ز راه مکه خاقانی به یثرب داد روی</p></div>
<div class="m2"><p>پیش صدر مصطفی ثانی حسان دیده‌اند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بنده خاقانی سگ تازی است بر درگاه او</p></div>
<div class="m2"><p>بخ بخ آن تازی سگی کش پارسی خوان دیده‌اند</p></div></div>