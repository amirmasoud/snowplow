---
title: >-
    شمارهٔ ۱۹۱ - مطلع دوم
---
# شمارهٔ ۱۹۱ - مطلع دوم

<div class="b" id="bn1"><div class="m1"><p>ای تیر باران غمت، خون دل ما ریخته</p></div>
<div class="m2"><p>نگذاشت طوفان غمت، خون دلی ناریخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای صد یک از عشقت خرد، جان صیدت از یک تا به صد</p></div>
<div class="m2"><p>چشم تو در یک چشم زد، صد خون تنها ریخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای ریخته سیل ستم بر جان ما سر تا قدم</p></div>
<div class="m2"><p>پس ذرهٔ ناکرده کم، ما تن زده تا ریخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماهی و جوزا زیورت، وز رشک زیور در برت</p></div>
<div class="m2"><p>از غمزهٔ چون نشترت مه خون جوزا ریخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محراب قیصر کوی تو، عید مسیحا روی تو</p></div>
<div class="m2"><p>عود الصلیب موی تو، آب چلیپا ریخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گیرم‌نه‌ای چون آب نرم، آتش مباش از جوش گرم</p></div>
<div class="m2"><p>آهسته باش ای آب شرم، از چشم رعنا ریخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلفت چو هر غوغائیی، چون زیر هر سودائیی</p></div>
<div class="m2"><p>چشمت بهر رعنائیی، آب رخ ما ریخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پختن سودای تو خام است ما را رای تو</p></div>
<div class="m2"><p>ما زر و سر در پای تو، خاقانی آسا ریخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز نو است و فخر دین بر آسمان مجلس نشین</p></div>
<div class="m2"><p>ما زر چهره بر زمین، تو سیم سیما ریخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاقان اکبر کز فلک بانگ آمدش کالامر لک</p></div>
<div class="m2"><p>در پای او دست ملک، روح معلا ریخته</p></div></div>