---
title: >-
    شمارهٔ ۳۳ - در مدح خاقان کبیر ابوالمظفر اخستان شروان شاه و ملکه
---
# شمارهٔ ۳۳ - در مدح خاقان کبیر ابوالمظفر اخستان شروان شاه و ملکه

<div class="b" id="bn1"><div class="m1"><p>دل روی مراد از آن ندیده است</p></div>
<div class="m2"><p>کز اهل دلی نشان ندیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل هر دو جهان سه باره پیمود</p></div>
<div class="m2"><p>یک اهل در این میان ندیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شیب و فراز این دو منزل</p></div>
<div class="m2"><p>یک پیک وفا روان ندیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرخ آمده کعبتین بی‌نقش</p></div>
<div class="m2"><p>کس نقش وفا از آن ندیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنسی که من از جهان ندیدم</p></div>
<div class="m2"><p>پیش از من هم جهان ندیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از منقطعان راه امید</p></div>
<div class="m2"><p>یک تن رصد امان ندیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز آمد و روز شد جهان را</p></div>
<div class="m2"><p>کس یک پی کاروان ندیده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا پشت وفا زمانه بشکست</p></div>
<div class="m2"><p>کس راستی از زمان ندیده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پشت شکستهٔ وفا به</p></div>
<div class="m2"><p>بازوی فلک کمان ندیده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاقانی سود و مایهٔ عمر</p></div>
<div class="m2"><p>الا ز زبان زیان ندیده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آویختگی سر ترازو</p></div>
<div class="m2"><p>الا ز سر زبان ندیده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عالم ز همه ملوک عالم</p></div>
<div class="m2"><p>جنس ملک اخستان ندیده است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاقان کبیر، کز جلالت</p></div>
<div class="m2"><p>آن دید که خضر خان ندیده است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شروان شه آفتاب دولت</p></div>
<div class="m2"><p>کورا دوم آسمان ندیده است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جمشید کیان که دین جز او را</p></div>
<div class="m2"><p>روئین‌تن هفت خوان ندیده است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گو در ملک اخستان نگر آنک</p></div>
<div class="m2"><p>کیخسرو باستان ندیده است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گو رایت بوالمظفری بین</p></div>
<div class="m2"><p>آنک اختر کاویان ندیده است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گویند که مرز تور و ایران</p></div>
<div class="m2"><p>چون رستم پهلوان ندیده است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن کیست که در صف غلامانش</p></div>
<div class="m2"><p>صد رستم سیستان ندیده است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر نیزهٔ او سماک رامح</p></div>
<div class="m2"><p>کمتر ز زحل سنان ندیده است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جز بانو و شاه کوه و دریا</p></div>
<div class="m2"><p>کس در یک دودمان ندیده است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دو ابر و دو آفتاب و دو بحر</p></div>
<div class="m2"><p>کس جز کف هر دوان ندیده است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو روح و دو نور کس جز ایشان</p></div>
<div class="m2"><p>بر یک سر خوان و خان ندیده است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گیتی افق سپهر عصمت</p></div>
<div class="m2"><p>جز حضرت بانوان ندیده است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جمشید ملک نظیر بلقیس</p></div>
<div class="m2"><p>جز بانوی کامران ندیده است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قیدافهٔ مملکت که دهرش</p></div>
<div class="m2"><p>جز رابعهٔ کیان ندیده است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>او رابعهٔ بنات نعش است</p></div>
<div class="m2"><p>خود رابعه کس چنان ندیده است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جز نه زن سیدش به ده نوع</p></div>
<div class="m2"><p>کس مثل به صد قران ندیده است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رح القدس آن صفا کز او دید</p></div>
<div class="m2"><p>از مریم پاک جان ندیده است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر پردهٔ مریم دوم چرخ</p></div>
<div class="m2"><p>جز قیصر پاسبان ندیده است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از قصر جلالتش به صد دور</p></div>
<div class="m2"><p>خورشید یک آستان ندیده است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یک خوان شرف نساخت کایام</p></div>
<div class="m2"><p>سیمرغش مورخوان ندیده است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برخوان کفش طفیل امید</p></div>
<div class="m2"><p>جز رضوان میزبان ندیده است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در مجلس و خوانش چاشنی گیر</p></div>
<div class="m2"><p>جز جنت نقلدان ندیده است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر سو که همای بخت پرید</p></div>
<div class="m2"><p>الا درش آشیان ندیده است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا نخل گرفت بوی عدلش</p></div>
<div class="m2"><p>کس در رطب استخوان ندیده است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بیند قلمش به گاه توقیع</p></div>
<div class="m2"><p>هرک آتش در فشان ندیده است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا نامد مهد دولت او</p></div>
<div class="m2"><p>کس شروان خیروان ندیده است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ملاح خرد به کشتی وهم</p></div>
<div class="m2"><p>در بحر دلش کران ندیده است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در جنب سخاش بحر و کان را</p></div>
<div class="m2"><p>کس قوت امتحان ندیده است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زین پس کفش آفتاب بخشد</p></div>
<div class="m2"><p>کاندر خور بخش کان ندیده است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کس بی‌کف راد صفوة الدین</p></div>
<div class="m2"><p>در جسم کرم روان ندیده است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در پرده نهان چو راز غیب است</p></div>
<div class="m2"><p>غیب از دل خود نهان ندیده است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چون کعبه مجاور حجاب است</p></div>
<div class="m2"><p>آن کعبه که کس عیان ندیده است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ذات ملکه است جنت عدن</p></div>
<div class="m2"><p>کس جنت بی‌گمان ندیده است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شاه ادریس است و خود جز ادریس</p></div>
<div class="m2"><p>از مردان کس جنان ندیده است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر نه فلک او ستارهٔ قطب</p></div>
<div class="m2"><p>کس قطب سبک عنان ندیده است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>با قطب جز این دو قرة العین</p></div>
<div class="m2"><p>کس مرقد فرقدان ندیده است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بر روس و حبش که روز و شب راست</p></div>
<div class="m2"><p>جز داغ ادب نشان ندیده است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>این روس و حبش دو خادمش دان</p></div>
<div class="m2"><p>کاین خادم روی آن ندیده است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ای بانوی خاندان جمشید</p></div>
<div class="m2"><p>جم زین به خاندان ندیده است</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ای ساره صفات و آسیه زهد</p></div>
<div class="m2"><p>کس چون تو زبیده سان ندیده است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هر کس که ثنات بر زبان راند</p></div>
<div class="m2"><p>جز کوثر در دهان ندیده است</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بر آتش هر که مدح راند</p></div>
<div class="m2"><p>جز طوبی و ضیمران ندیده است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خاک در تو هر آنکه بوسید</p></div>
<div class="m2"><p>جز گوهر رایگان ندیده است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چون تو ملکه نبود و چون من</p></div>
<div class="m2"><p>کس شاعر مدح خوان ندیده است</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>من دانم داستان مدحت</p></div>
<div class="m2"><p>کس زین به داستان ندیده است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آن دید ضمیرم از ثنایت</p></div>
<div class="m2"><p>کز نیسان بوستان ندیده است</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>و آن بیند بزمت از زبانم</p></div>
<div class="m2"><p>کز بلبل گلستان ندیده است</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ذکر تو به باغ خاطر من</p></div>
<div class="m2"><p>شاخی است که مهرگان ندیده است</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>این مدحت تازه بر در تو</p></div>
<div class="m2"><p>مشکی است که پرنیان ندیده است</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بنده ز دکان شعر برخاست</p></div>
<div class="m2"><p>چون بازاری در آن ندیده است</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>حلاج، دکان گذاشت ایراک</p></div>
<div class="m2"><p>جز آتش در دکان ندیده است</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بانوی جهان نپرسدش حال</p></div>
<div class="m2"><p>کو حال دل نوان ندیده است</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از هیچ کسی به هیچ دردی</p></div>
<div class="m2"><p>تسکین شفارسان ندیده است</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از هر که علاج خواست الا</p></div>
<div class="m2"><p>درد دل ناتوان ندیده است</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>قرب دو سه سال هست کز شاه</p></div>
<div class="m2"><p>یک حرمت و نیم نان ندیده است</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>اقطاع و برات رفت و از کس</p></div>
<div class="m2"><p>یک پرسش غم نشان ندیده است</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شاه است گران سر ار چه رنجی</p></div>
<div class="m2"><p>زین بندهٔ جان گران ندیده است</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گفته است به ترک خدمت اکنون</p></div>
<div class="m2"><p>کانعام خدایگان ندیده است</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دستوری خواهد از خداوند</p></div>
<div class="m2"><p>کز درگه شه مکان ندیده است</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زنهاری توست و از تو بهتر</p></div>
<div class="m2"><p>یک داور مهربان ندیده است</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>خواهد ز تو استعانت ایرا</p></div>
<div class="m2"><p>بهتر ز تو مستعان ندیده است</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دادش بده و فغانش بشنو</p></div>
<div class="m2"><p>کاندوخته جز فغان ندیده است</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>این شعر وداعی از زبانم</p></div>
<div class="m2"><p>سحر است و کس این بیان ندیده است</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>مرغ دو زبان چو کلک من کس</p></div>
<div class="m2"><p>بر گلبن ده بنان ندیده است</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بر نطق سوارم و عطارد</p></div>
<div class="m2"><p>این مرکب، زیر ران ندیده است</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>باغی است بقای بانوی عصر</p></div>
<div class="m2"><p>کز باد فنا، خزان ندیده است</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بر لوح فرشته نامش ایام</p></div>
<div class="m2"><p>جز بانوی انس و جان ندیده است</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>صد عید چنین ضمان کند عمر</p></div>
<div class="m2"><p>دولت به ازین ضمان ندیده است</p></div></div>