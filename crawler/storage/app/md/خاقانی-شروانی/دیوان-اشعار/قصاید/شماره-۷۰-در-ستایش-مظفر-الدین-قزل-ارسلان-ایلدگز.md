---
title: >-
    شمارهٔ ۷۰ - در ستایش مظفر الدین قزل ارسلان ایلدگز
---
# شمارهٔ ۷۰ - در ستایش مظفر الدین قزل ارسلان ایلدگز

<div class="b" id="bn1"><div class="m1"><p>صبح خیزان کز دو عالم خلوتی برساختند</p></div>
<div class="m2"><p>مجلسی بر یاد عید از خلد خوش تر ساختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هاتف خم خانه داد آواز کای جمع الصبوح</p></div>
<div class="m2"><p>پاسخش را آب لعل و کشتی زر ساختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسم جور از ساقی منصف به نصفی خواستند</p></div>
<div class="m2"><p>بس حیل خوردند و ساغر بحر اخضر ساختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دهان روزه‌داران داشت مهر از آفتاب</p></div>
<div class="m2"><p>سایه پروردان خم را مهر بر در ساختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون لب خم شد موافق با دهان روزه دار</p></div>
<div class="m2"><p>سر به مشک آلوده یک ماهش معطر ساختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پس یک ماه سنگ انداز در جام بلور</p></div>
<div class="m2"><p>عده داران رزان را حجله‌ها برساختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم صبوح عید به کز بهر سنگ انداز عمر</p></div>
<div class="m2"><p>روزهٔ جاوید را روزی مقدر ساختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرخ جامی چون شفق در دست وانگه در صبوح</p></div>
<div class="m2"><p>لخلخه از صبح و دستنبو ز اختر ساختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کف در آن ساغر معلق زن چو طفل غازیان</p></div>
<div class="m2"><p>کز بلور لوریانش طوق و چنبر ساختند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هات غلغل حلق خامان را که با خیر العمل</p></div>
<div class="m2"><p>غلغل حلق صراحی را برابر ساختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بلبله در قلقل آمد قل‌قل ای بلبل نفس</p></div>
<div class="m2"><p>تازه کن قولی که مرغان قلندر ساختند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن می و میدان زرین بین که پنداری بهم</p></div>
<div class="m2"><p>آتش موسی و گاو سامری در ساختند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از مسام گاو زرین شد روان گاورس زر</p></div>
<div class="m2"><p>چون صراحی را سر و حلق کبوتر ساختند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ریسمان سبحه بگسستند و کستی بافتند</p></div>
<div class="m2"><p>گوهر قندیل بشکستند و ساغر ساختند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آتش قندیل بنشست آب سبحه هم برفت</p></div>
<div class="m2"><p>کاتش و آب از قدح قندیل دیگر ساختند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خانهٔ زنبور شهد آلود رفت از صحن خوان</p></div>
<div class="m2"><p>چون ز غمزه ساقیان زنبور کافر ساختند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صحن مجلس در مدور جان نوشین چشمه یافت</p></div>
<div class="m2"><p>کانچنان هم چشمه چشمه هم مدور ساختند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اوفتان خیزان زمین سرمست شد چون آسمان</p></div>
<div class="m2"><p>کز نسیم جرعه خاکش را معنبر ساختند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وانکه از روی تواضع پیش روی شاهدان</p></div>
<div class="m2"><p>دیده‌ها را جرعه چین خاک اغبر ساختند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون به زر آب قدح کردند مژگان را طلی</p></div>
<div class="m2"><p>میخ نعل مرکبان شاه کشور ساختند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آفتاب گوهر سلجق که نعل رخش اوست</p></div>
<div class="m2"><p>اصل آن گوهر کز او شمشیر حیدر ساختند</p></div></div>