---
title: >-
    شمارهٔ ۱۱۲ - در پند و اندرز و ستایش رکن الدین مفتی خوی و رکن الدین عالم ری و تاج الدین رازی ابن امین الدین
---
# شمارهٔ ۱۱۲ - در پند و اندرز و ستایش رکن الدین مفتی خوی و رکن الدین عالم ری و تاج الدین رازی ابن امین الدین

<div class="b" id="bn1"><div class="m1"><p>الصبوح الصبوح کامد کار</p></div>
<div class="m2"><p>النثار النثار کامد یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاری از روشنی چو آب خزان</p></div>
<div class="m2"><p>یاری از خرمی چو باد بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ بر کار و یار ما به صبوح</p></div>
<div class="m2"><p>می‌کند لعبتان دیده نثار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام فرعونی اندرآر که صبح</p></div>
<div class="m2"><p>دست موسی برآرد از کهسار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سفال خم آتشی است که هست</p></div>
<div class="m2"><p>عقل حراق او و روح شرار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کف از جام خنگ بت بنگر</p></div>
<div class="m2"><p>بر رخ از باده سرخ بت بنگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاصه کایام بست پردهٔ کام</p></div>
<div class="m2"><p>خاصه دوران گشاد رشتهٔ کار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرغ دل یافت دانهٔ سلوت</p></div>
<div class="m2"><p>برق می سوخت کشتهٔ تیمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بار مشک است و زعفران در جام</p></div>
<div class="m2"><p>پس خط جام چون خط طیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کو تذوران بزم و کوثر جام</p></div>
<div class="m2"><p>کز سمن زار بشکفد گل زار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این این الکؤس والا قداح</p></div>
<div class="m2"><p>این این الشموس و الاقمار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به مغان آی تا مرا بینی</p></div>
<div class="m2"><p>که ز حبل المتین کنم زنار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عقل اگر دم زند به دست میش</p></div>
<div class="m2"><p>چون زره بر دهان زنم مسمار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خوانچه کن سنت مغان می‌آر</p></div>
<div class="m2"><p>وز بلورین رکاب می‌بگسار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عجب است این رکاب و می‌گویی</p></div>
<div class="m2"><p>کآمد از ماه نو شفق دیدار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>می‌کشد عقل را به زیر رکاب</p></div>
<div class="m2"><p>چون رکاب گران کشند احرار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آفتاب ار سوار شد بر شیر</p></div>
<div class="m2"><p>هست می شیر آفتاب سوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جرعه‌ای گر به آسمان بخشی</p></div>
<div class="m2"><p>شود از خفتگی زمین کردار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ور زمین را دهی ز می جرعه</p></div>
<div class="m2"><p>گردد از مستی آسمان رفتار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>می‌کند در طبایع اربع</p></div>
<div class="m2"><p>ظلمات ثلاث را انوار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ساقی آرد گه خمار شکن</p></div>
<div class="m2"><p>فقع شکرین ز دانهٔ نار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نار به نقل چون شراب خوریم</p></div>
<div class="m2"><p>نقل ما نار بینی از لب یار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تیغ خونین کشد می کافر</p></div>
<div class="m2"><p>زخمه گوید که جاهد الکفار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر به مستی رسی و می نرسد</p></div>
<div class="m2"><p>نرسد دست بر می بازار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر فلک شو ز تیغ صبح مترس</p></div>
<div class="m2"><p>که نترسد ز تیغ و سر عیار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر فلک خوانچه کن به دولت می</p></div>
<div class="m2"><p>ز اختران خواه نز خم خمار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ماه نو کن قدح چو هست توان</p></div>
<div class="m2"><p>وز شفق گیر می چو هست یسار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ها ثریا نه خوشهٔ عنب است</p></div>
<div class="m2"><p>دست برکن ز خوشه می بفشار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مار کز روی زهد خاک خورد</p></div>
<div class="m2"><p>ریزد از کام زهر جان او بار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نحل کاب عنب خورد بر تاک</p></div>
<div class="m2"><p>آرد از لب شراب نوش گوار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مثل جام و پارسایان هست</p></div>
<div class="m2"><p>لب دریا و مرغ بوتیمار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پارسا را چه لذت از عشرت</p></div>
<div class="m2"><p>خنفسا را چه کار با عطار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر که جوید محال ناممکن</p></div>
<div class="m2"><p>هست ممکن که نیست زیرک سار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لیکن ار کس حریف پنداری</p></div>
<div class="m2"><p>عقل طعن آورد بر این پندار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یا اگر گوئی اهل دل کس هست</p></div>
<div class="m2"><p>گویدت دل خطاست این گفتار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر تو در وهم همدمی جویی</p></div>
<div class="m2"><p>در ره جست گم کنی هنجار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به خطائی که بگذرد در وهم</p></div>
<div class="m2"><p>عاقلان را سزاست استغفار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دوستکانی به هفت مردان بخش</p></div>
<div class="m2"><p>سر به مهرش کن و به خضر سپار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از زکات سر قدح گاهی</p></div>
<div class="m2"><p>جرعه‌ای کن به خاکیان ایثار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بس بس ای دل ز کار آب که عقل</p></div>
<div class="m2"><p>هست از آب کار او بیزار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مدت لهو را غم است انجام</p></div>
<div class="m2"><p>بادهٔ نیک را بد است خمار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر طرب را مقابل است کرب</p></div>
<div class="m2"><p>هر یمین را برابر است یسار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سنگ را آب بردمد ز شکم</p></div>
<div class="m2"><p>آب را سنگ درفتد به زهار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یک فرح را هزار غم ز پس است</p></div>
<div class="m2"><p>که پس هر فرح غم است هزار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر چه زین روی کعبتین یک و دوست</p></div>
<div class="m2"><p>بر دگر روی او شش است و چهار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گاو عنبر فکن برهنه تن است</p></div>
<div class="m2"><p>خر بربط بریشمین افسار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دل تصاویر خانهٔ نظر است</p></div>
<div class="m2"><p>شهد الله نبشته گرد عذار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>حرز عقل است مرهم دل ریش</p></div>
<div class="m2"><p>تیغ روز است صیقل شب تار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چون رباب است دست بر سر عقل</p></div>
<div class="m2"><p>از دم وصل تو تظلم دار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همچو دف کاغذینش پیراهن</p></div>
<div class="m2"><p>همچو چنگش پلاس بین شلوار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>باده را بر خرد مکن غالب</p></div>
<div class="m2"><p>دیو را بر فلک مکن سالار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چند خواهی ز آهوی سیمین</p></div>
<div class="m2"><p>گاو زرین که می‌خورد گلنار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر بود ز آن می چو زهرهٔ گاو</p></div>
<div class="m2"><p>خاطر گاو زهره شیر شکار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هم ز می دان که شاه باز خرد</p></div>
<div class="m2"><p>کبک زهره شود به سیرت سار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از من آموز دم زدن به صبوح</p></div>
<div class="m2"><p>دم مسغفرین بالاسحار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جام کیخسرو است خاطر من</p></div>
<div class="m2"><p>که کند راز کائنات اظهار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سلسبیل حلال خور زین جام</p></div>
<div class="m2"><p>وز حمیم حرام شو بیزار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فیض ابن السحاب خور چو صدف</p></div>
<div class="m2"><p>حیض ابن العنب بجا بگذار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شیر پستان شیر خوردستی</p></div>
<div class="m2"><p>حیض خرگوش پس مخور زنهار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز آب رنگین حجاب عقل مساز</p></div>
<div class="m2"><p>شعلهٔ نار پیش شیر میار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بول شیطان مکن به قاروره</p></div>
<div class="m2"><p>پیش چشم طبیب عقل مدار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>عیش اسلاف در سفال مدان</p></div>
<div class="m2"><p>گل سیراب در سراب مکار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>لهو و لذت دو مار ضحاکند</p></div>
<div class="m2"><p>هر دو خون خوار و بی‌گناه آزار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>عقل و دین لشکر فریدونند</p></div>
<div class="m2"><p>که برآرند از دو مار، دمار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گر چه خاقانی اهل حضرت نیست</p></div>
<div class="m2"><p>یاد دربانش هست دست افزار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نیست چون پیل مست معرکه لیک</p></div>
<div class="m2"><p>عنکبوتی است روی بر دیوار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سار مسکین که نیست چون بلبل</p></div>
<div class="m2"><p>رومی ارغنون زن گلزار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>لاجرم شاید ار به رستهٔ بید</p></div>
<div class="m2"><p>زنگی چار پاره زن شد سار</p></div></div>