---
title: >-
    شمارهٔ ۹۵ - در رثاء امام ابو عمر و اسعد
---
# شمارهٔ ۹۵ - در رثاء امام ابو عمر و اسعد

<div class="b" id="bn1"><div class="m1"><p>بیدقی مدح شاه می‌گوید</p></div>
<div class="m2"><p>کوکبی وصف ماه می‌گوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلکه مزدور دار خانهٔ نحل</p></div>
<div class="m2"><p>صفت عدل شاه می‌گوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذره در بارگاه خورشید است</p></div>
<div class="m2"><p>سخن از بارگاه می‌گوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مور در پایگاه جمشید است</p></div>
<div class="m2"><p>قصه از پیشگاه می‌گوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاطرم وصف او نداند گفت</p></div>
<div class="m2"><p>گر چه هر چند گاه می‌گوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز پرسید تا مناقب او</p></div>
<div class="m2"><p>مویه‌گر بر چه راه می‌گوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نور پیغمبرش همی خواند</p></div>
<div class="m2"><p>یاش سایهٔ الاه می‌گوید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مفتی مطلقش همی خواند</p></div>
<div class="m2"><p>داور دین پناه می‌گوید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امتش دین فزای می‌خواند</p></div>
<div class="m2"><p>ملتش کفرگاه می‌گوید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آفتابش به صد هزار زبان</p></div>
<div class="m2"><p>سایهٔ پادشاه می‌گوید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پشت دنیا ز مرگ او بشکست</p></div>
<div class="m2"><p>روی دین ترک جاه می‌گوید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از سر دین کلاه عزت رفت</p></div>
<div class="m2"><p>سر دریغا کلاه می‌گوید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چشم بیدار شرع شد در خواب</p></div>
<div class="m2"><p>راز با خوابگاه می‌گوید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>والله ار کس ثناش داند گفت</p></div>
<div class="m2"><p>هر که گوید تباه می‌گوید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاطرم نیز عذر می‌خواهد</p></div>
<div class="m2"><p>که نه بر جایگاه می‌گوید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر حدیثی گناه می‌شمرد</p></div>
<div class="m2"><p>پس حدیث از گناه می‌گوید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اشک من چون زبان خونین هم</p></div>
<div class="m2"><p>حیلت عذرخواه می‌گوید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرثیت‌های او مگر دل خاک</p></div>
<div class="m2"><p>بر زبان گیاه می‌گوید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>غم آن صبح صادق ملت</p></div>
<div class="m2"><p>آسمان شام گاه می‌گوید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر سوار از جگر سپه سازد</p></div>
<div class="m2"><p>غم دل با سپاه می‌گوید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چشم خور اشک ران به خون شفق</p></div>
<div class="m2"><p>راز با قعر چاه می‌گوید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دانش من گواه عصمت اوست</p></div>
<div class="m2"><p>بشنو آنچ این گواه می‌گوید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آه کز فرقت امام جهان</p></div>
<div class="m2"><p>جان خاقانی آه می‌گوید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا شد از عالم اسعد بو عمرو</p></div>
<div class="m2"><p>عالونم وا اسعداه می‌گوید</p></div></div>