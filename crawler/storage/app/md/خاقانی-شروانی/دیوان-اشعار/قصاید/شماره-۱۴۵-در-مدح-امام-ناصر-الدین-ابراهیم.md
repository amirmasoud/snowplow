---
title: >-
    شمارهٔ ۱۴۵ - در مدح امام ناصر الدین ابراهیم
---
# شمارهٔ ۱۴۵ - در مدح امام ناصر الدین ابراهیم

<div class="b" id="bn1"><div class="m1"><p>در این دامگاه ارچه همدم ندارم</p></div>
<div class="m2"><p>بحمدالله از هیچ غم غم ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا با غم از نیستی هست سری</p></div>
<div class="m2"><p>که کس را در این باب محرم ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارم دل خلق و گر راست خواهی</p></div>
<div class="m2"><p>سر صحبت خویشتن هم ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو از عالم خویش بیگانه گشتم</p></div>
<div class="m2"><p>سر خویشی هر دو عالم ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سیمرغ مانم ز روی حقیقت</p></div>
<div class="m2"><p>که از هیچ مخلوق همدم ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نام و به وحدت چنو سر فرازم</p></div>
<div class="m2"><p>که این هر دو معنی ازو کم ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا کشت زاری است در طینت دل</p></div>
<div class="m2"><p>که حاجت به حوا و آدم ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا عز و ذلی است در راه همت</p></div>
<div class="m2"><p>که پروای موسی و بلعم ندارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پیش کس از بهر یک خندهٔ خوش</p></div>
<div class="m2"><p>قد خویش چون ماه نو خم ندارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو در سبز پوشان بالا رسیدم</p></div>
<div class="m2"><p>دگر جامهٔ حرص معلم ندارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به کافور عزلت خنک شد دل من</p></div>
<div class="m2"><p>سزد گر ز مشک عمل شم ندارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دهان خشک و دل خسته‌ام لیکن از کس</p></div>
<div class="m2"><p>تمنای جلاب و مرهم ندارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به پازهر کس نگرم گرچه بر خوان</p></div>
<div class="m2"><p>یکی لقمه بی‌شربت سم ندارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به دیو امل عقل غره نسازم</p></div>
<div class="m2"><p>به باد طمع طبع خرم ندارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا باد و دیو است خارم اگرچه</p></div>
<div class="m2"><p>سلیمان نیم حکم و خاتم ندارم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیاده نباشم ز اسباب دانش</p></div>
<div class="m2"><p>گر اسباب دنیا فراهم ندارم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هنر درخور معرکه دارم آخر</p></div>
<div class="m2"><p>اگر ساخت درخورد ادهم ندارم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آنم به ماتم که زنده است نفسم</p></div>
<div class="m2"><p>چو مرد از پسش هیچ ماتم ندارم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گلستان جان آرزومند آب است</p></div>
<div class="m2"><p>از آن دیده را هیچ بی‌نم ندارم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو از حبس این چار ارکان گذشتم</p></div>
<div class="m2"><p>طربگاه جز هفت طارم ندارم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگرچه بریده پرم، جای شکر است</p></div>
<div class="m2"><p>که بند قفس سخت محکم ندارم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برآرم پر و برپرم کشیانه</p></div>
<div class="m2"><p>به از قبهٔ چرخ اعظم ندارم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه خاقانیم گر همی عزم تحویل</p></div>
<div class="m2"><p>مصمم از این کلبهٔ غم ندارم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا پای بسته است خاقانی ایدر</p></div>
<div class="m2"><p>چرا عزم رفتن مصمم ندارم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همانا که این رخصت از بهر خدمت</p></div>
<div class="m2"><p>ز درگاه صدر معظم ندارم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>امام امم ناصر الدین که در دین</p></div>
<div class="m2"><p>امامت جز او را مسلم ندارم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>براهیم خوش‌نام کز مدحش الا</p></div>
<div class="m2"><p>صفات براهیم ادهم ندارم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فلک خورد سوگند بر همت او</p></div>
<div class="m2"><p>که در کون جز تو مقدم ندارم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز خصمی که ناقص فتاده است نفسش</p></div>
<div class="m2"><p>کمال تو را هیچ مبهم ندارم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر او هست دجال خلقت برغمش</p></div>
<div class="m2"><p>تو را کم ز عیسی مریم ندارم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وگر فعل ارقم کند من که چرخم</p></div>
<div class="m2"><p>زمرد جز از بهر ارقم ندارم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زهی دین طرازی که بی‌نقش نامت</p></div>
<div class="m2"><p>در آفاق یک حرف معجم ندارم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از آنگه که خاک درت سرمه کردم</p></div>
<div class="m2"><p>به چشم سعادت درون نم ندارم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگرچه ز انصاف با دشمن و دوست</p></div>
<div class="m2"><p>دم مدح رانم سر ذم ندارم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به دنبال تو چون سگی برنیایم</p></div>
<div class="m2"><p>که طبع هنر کم ز ضیغم ندارم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر تن به حضرت نیارم عجب نی</p></div>
<div class="m2"><p>که رخشی سزاوار رستم ندارم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رخ از آب زمزم نشویم ازیرا</p></div>
<div class="m2"><p>که آلوده‌ام روی زمزم ندارم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز صدر تو گر غائبم جز به شکرت</p></div>
<div class="m2"><p>زبان بر ثنای دمادم ندارم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دعاهات گفتم به خیرات بپذیر</p></div>
<div class="m2"><p>اگرچه دعای مقسم ندارم</p></div></div>