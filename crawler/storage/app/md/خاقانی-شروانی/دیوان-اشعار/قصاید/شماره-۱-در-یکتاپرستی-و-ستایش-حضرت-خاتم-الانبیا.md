---
title: >-
    شمارهٔ ۱ - در یکتاپرستی و ستایش حضرت خاتم الانبیاء
---
# شمارهٔ ۱ - در یکتاپرستی و ستایش حضرت خاتم الانبیاء

<div class="b" id="bn1"><div class="m1"><p>جوشن صورت برون کن در صف مردان درآ</p></div>
<div class="m2"><p>دل طلب کز دار ملک دل توان شد پادشا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو خود را پای بستی باد داری در دو دست</p></div>
<div class="m2"><p>خاک بر خود پاش کز خود هیچ نگشاید تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تو قرب قاب قوسین آنگه افتد عشق را</p></div>
<div class="m2"><p>کز صفات خود به بعد المشرقین افتی جدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن خویشی، چند گوئی آن اویم آن او</p></div>
<div class="m2"><p>باش تا او گوید ای جان آن مائی آن ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست عاشق گشتن الا بودنش پروانه وار</p></div>
<div class="m2"><p>اولش قرب و میانه سوختن، آخر فنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاف یک رنگی مزن تا از صفت چون آینه</p></div>
<div class="m2"><p>از درون سو تیرگی داری و بیرون سو صفا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتشین داری زبان و دل سیاهی چون چراغ</p></div>
<div class="m2"><p>گرد خود گردی از آن تردامنی چون آسیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخت از این گنبد برون بر، گر حیاتی بایدت</p></div>
<div class="m2"><p>زان که تا در گنبدی با مردگانی هم وطا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس عیسی جست خواهی راه کن سوی فلک</p></div>
<div class="m2"><p>نقش عیسی در نگارستان راهب کن رها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر گذر زین تنگنای ظلمت اینک روشنی</p></div>
<div class="m2"><p>درگذر زین خشک سال آفت اینک مرحبا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر در فقر آی تا پیش آیدت سرهنگ عشق</p></div>
<div class="m2"><p>گوید ای صاحب خراج هر دو گیتی اندر آ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شرب عزلت ساختی از سر ببر باد هوس</p></div>
<div class="m2"><p>باغ وحدت یافتی از بن بکن بیخ هوا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با قطار خوک در بیت المقدس پا منه</p></div>
<div class="m2"><p>با سپاه پیل بر درگاه بیت الله میا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر بنه کاینجا سری را صد سر آید در عوض</p></div>
<div class="m2"><p>بلکه بر سر هر سری را صد کلاه آید عطا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر چه جز نور السموات از خدائی عزل کن</p></div>
<div class="m2"><p>گر تو را مشکوة دل روشن شد از مصباح لا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون رسیدی بر در لاصدر الا جوی از آنک</p></div>
<div class="m2"><p>کعبه را هم دید باید چون رسیدی در منا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور تو اعمی بوده‌ای بر دوش احمد دار دست</p></div>
<div class="m2"><p>کاندر این ره قائد تو مصطفی به مصطفا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اوست مختار خدا و چرخ و ارواح و حواس</p></div>
<div class="m2"><p>زان گرفتند از وجودش منت بی‌منتها</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هشت خلد و هفت چرخ و شش جهات و پنج حس</p></div>
<div class="m2"><p>چار ارکان و سه ارواح و دو کون از یک خدا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون مرا در نعت چون اویی رود چندین سخن</p></div>
<div class="m2"><p>از جهان بر چون منی تا کی رود چندین جفا</p></div></div>