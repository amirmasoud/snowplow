---
title: >-
    شمارهٔ ۵۸ - این قصیدهٔ را در جواب امام مجد الدین سروده و به مدح شاه اخستان پایان داده است
---
# شمارهٔ ۵۸ - این قصیدهٔ را در جواب امام مجد الدین سروده و به مدح شاه اخستان پایان داده است

<div class="b" id="bn1"><div class="m1"><p>الصبوح ای دل که جان خواهم فشاند</p></div>
<div class="m2"><p>دست هستی بر جهان خواهم فشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش مرغان سر کوی مغان</p></div>
<div class="m2"><p>دانهٔ دل رایگان خواهم فشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده می‌پالای و گیتی خاک پای</p></div>
<div class="m2"><p>جرعه‌های این بر آن خواهم فشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک در رقص است و ناله در سماع</p></div>
<div class="m2"><p>بر سماع و رقص جان خواهم فشاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر خاک از جفای آسمان</p></div>
<div class="m2"><p>خاک هم بر آسمان خواهم فشاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوستان چون از نفاق آگنده‌اند</p></div>
<div class="m2"><p>آستین بر دوستان خواهم فشاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دشمنان چون بر غمم بخشوده‌اند</p></div>
<div class="m2"><p>بر سر دشمن روان خواهم فشاند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کیسه‌ای کز زندگی بردوختم</p></div>
<div class="m2"><p>بر زمانه هر زمان خواهم فشاند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر زری کز خاک بیزی یافتم</p></div>
<div class="m2"><p>بر سراین خاکدان خواهم فشاند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر سحر خاقانی آسا بر فلک</p></div>
<div class="m2"><p>ناوک آتش فشان خواهم فشاند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این ستارهٔ دری و در دری</p></div>
<div class="m2"><p>بر همام بحرسان خواهم فشاند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این زر اکسیر نفس ناطقه</p></div>
<div class="m2"><p>بر سر صدر زمان خواهم فشاند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این دو طفل نوری اندر مهد چشم</p></div>
<div class="m2"><p>بر بزرگ خرده‌دان خواهم فشاند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این سه گنج نفس از قصر دماغ</p></div>
<div class="m2"><p>بر امام انس و جان خواهم فشاند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این چهار اجساد کان کائنات</p></div>
<div class="m2"><p>بر مراد کن فکان خواهم فشاند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کس چه داند کاین نثار از بهر کیست</p></div>
<div class="m2"><p>تا نگویم بر فلان خواهم فشاند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر جلال و مجد مجد الدین خلیل</p></div>
<div class="m2"><p>در مدحت بیکران خواهم فشاند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر شکر کز لفظ او برچید سمع</p></div>
<div class="m2"><p>هم بر آن لفظ و بنان خواهم فشاند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر گهر کز کلک او دزدید طبع</p></div>
<div class="m2"><p>هم بر آن کلک و بنان خواهم فشاند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>داورم کی دست فرماید برید</p></div>
<div class="m2"><p>کانچه دزدیدم همان خواهم فشاند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شرع را گنج روان از کلک اوست</p></div>
<div class="m2"><p>عقل بر گنج روان خواهم فشاند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ملک را حرز امان از رای اوست</p></div>
<div class="m2"><p>روح بر حرز امان خواهم فشاند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر خضر گردم بر آن غمر الردا</p></div>
<div class="m2"><p>هم ردا هم طیلسان خواهم فشاند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ور ملک باشم بر آن عیسی نفس</p></div>
<div class="m2"><p>سبحهٔ پروین نشان خواهم فشاند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زیر پای اسبش ار دستم رسد</p></div>
<div class="m2"><p>افسر نوشین روان خواهم نشاند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قحط دانش را به اعجاز ثناش</p></div>
<div class="m2"><p>من و سلوی از لسان خواهم فشاند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون کند پروانه جان افشان به شمع</p></div>
<div class="m2"><p>من بر او جان هم‌چنان خواهم فشاند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خود کیم من وز سگان کیست جان</p></div>
<div class="m2"><p>تا بر آن فخر جهان خواهم فشاند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ابلهم تا فضلهٔ مء الحمیم</p></div>
<div class="m2"><p>بر لب حوض جنان خواهم فشاند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گمرهم تا بر سر بیت الحرام</p></div>
<div class="m2"><p>آب دست پیلبان خواهم فشاند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حشنیم تا ریزهٔ ریم آهنی</p></div>
<div class="m2"><p>بر سر تیغ یمان خواهم فشاند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یا نحوس کید قاطع را ز جهل</p></div>
<div class="m2"><p>بر سعود شعریان خواهم فشاند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یا سم گوساله و دنبال گرگ</p></div>
<div class="m2"><p>بر سر طور و شبان خواهم فشاند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یا کلاهی کز گیا بافد شبان</p></div>
<div class="m2"><p>بر سر تاج کیان خواهم فشاند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یا دم الحیضی که از خرگوش ریخت</p></div>
<div class="m2"><p>بر سر شیر ژیان خواهم فشاند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یا غبار لاشهٔ دیو سفید</p></div>
<div class="m2"><p>بر سوار سیستان خواهم فشاند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یا لعاب اژدهای حمیری</p></div>
<div class="m2"><p>بر درفش کاویان خواهم فشاند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اینت جهل ار فضلهٔ گوی جعل</p></div>
<div class="m2"><p>بر ند مدهامتان خواهم فشاند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اینت کفر ار گرد نعلین یزید</p></div>
<div class="m2"><p>بر یل خیبر ستان خواهم فشاند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر چه در حلق سماکین افکنم</p></div>
<div class="m2"><p>چون کمند امتحان خواهم فشاند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ور چه پر تیر گردون بشکنم</p></div>
<div class="m2"><p>چون خدنگی از کمان خواهم فشاند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>لیک با تیغ یقین او سپر</p></div>
<div class="m2"><p>بر سر آب گمان خواهم فشاند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پیش کلک دور باش آساش تیغ</p></div>
<div class="m2"><p>بر سر خاک هوان خواهم فشاند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در حضورش لالی آرم در زبان</p></div>
<div class="m2"><p>نه لالی از زبان خواهم فشاند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پیش نطقش کبم آرم از دهان</p></div>
<div class="m2"><p>خاک توبه بر دهان خواهم فشاند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بیضه چون طاوس نر خواهم گشاد</p></div>
<div class="m2"><p>وز برون آشیان خواهم فشاند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عقد نظمش کبم آرم از دهان</p></div>
<div class="m2"><p>بر سر شاه اخستان خواهم فشاند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زیور نثرش فرو خواهم گسست</p></div>
<div class="m2"><p>بر شه صاحب قران خواهم فشاند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بر خط دستش که هند و چین در اوست</p></div>
<div class="m2"><p>هفت گنج شایگان خواهم فشاند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چون به هندوچین او دستم رسد</p></div>
<div class="m2"><p>دست بر چیپال و خان خواهم فشاند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر سه تشریفش که خواندم یک به یک</p></div>
<div class="m2"><p>هر دو ساعت چارکان خواهم فشاند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هست هر سه چار خوان و هشت خلد</p></div>
<div class="m2"><p>من سه جان بر چار خوان خواهم فشاند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چون از آن خوان لقمه‌ای خواهم چشید</p></div>
<div class="m2"><p>بر سگ کهف استخوان خواهم فشاند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>باد چون جان جاودان عمرش که من</p></div>
<div class="m2"><p>جان بر او هم جاودان خواهم فشاند</p></div></div>