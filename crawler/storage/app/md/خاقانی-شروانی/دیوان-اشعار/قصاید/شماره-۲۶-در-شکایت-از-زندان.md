---
title: >-
    شمارهٔ ۲۶ - در شکایت از زندان
---
# شمارهٔ ۲۶ - در شکایت از زندان

<div class="b" id="bn1"><div class="m1"><p>راحت از راه دل چنان برخاست</p></div>
<div class="m2"><p>که دل اکنون ز بند جان برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفسی در میان میانجی بود</p></div>
<div class="m2"><p>آن میانجی هم از میان برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایه‌ای مانده بود هم گم شد</p></div>
<div class="m2"><p>وز همه عالمم نشان برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چار دیوار خانه روزن شد</p></div>
<div class="m2"><p>بام بنشست و آستان برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل خاکی به دست خون افتاد</p></div>
<div class="m2"><p>اشک خونین دیت ستان برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب شور از مژه چکید و ببست</p></div>
<div class="m2"><p>زیر پایم نمکستان برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دل من کمان کشید فلک</p></div>
<div class="m2"><p>لرز تیرم ز استخوان برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آه من دوش تیر باران کرد</p></div>
<div class="m2"><p>ابر خونبار از آسمان برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غصه‌ای بر سر دلم بنشست</p></div>
<div class="m2"><p>که بدین سر نخواهد آن برخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آمد آن مرغ نامه آور دوست</p></div>
<div class="m2"><p>صبح‌گاهی کز آشیان برخاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دید کز جای برنخاستمش</p></div>
<div class="m2"><p>طیره بنشست و دل گران برخاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اژدها بود خفته بر پایم</p></div>
<div class="m2"><p>نتوانستم آن زمان برخاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پای من زیر کوه آهن بود</p></div>
<div class="m2"><p>کوه بر پای چون توان برخاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پای خاقانی ار گشادستی</p></div>
<div class="m2"><p>داندی از سر جهان برخاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مار ضحاک ماند بر پایم</p></div>
<div class="m2"><p>وز مژه گنج شایگان برخاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سوزش من چو ماهی از تابه</p></div>
<div class="m2"><p>زین دو مار نهنگ سان برخاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون تنورم به گاه آه زدن</p></div>
<div class="m2"><p>کاتشین مارم از دهان برخاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در سیه خانه دل کبودی من</p></div>
<div class="m2"><p>از سپیدی پاسبان برخاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سگ دیوانه پاسبانم شد</p></div>
<div class="m2"><p>خوابم از چشم سیل ران برخاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سگ گزیده ز آب ترسد از آن</p></div>
<div class="m2"><p>ترسم از آب دیدگان برخاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در تموزم ببندد آب سرشک</p></div>
<div class="m2"><p>کز دمم باد مهرگان برخاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه شب سرخ روی چون شفقم</p></div>
<div class="m2"><p>کز سرشک آب ناردان برخاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ساقم آهن بخورد و از کعبم</p></div>
<div class="m2"><p>سیل خونین به ناودان برخاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بل که آهن ز آه من بگداخت</p></div>
<div class="m2"><p>ز آهن آواز الامان برخاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا چو بازم در آهنین خلخال</p></div>
<div class="m2"><p>چو جلاجل ز من فغان برخاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تن چو تار قز و بریشم وار</p></div>
<div class="m2"><p>ناله زین تار ناتوان برخاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رنگ رویم فتاد بر دیوار</p></div>
<div class="m2"><p>نام کهگل به زعفران برخاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خون دل زد به چرخ چندان موج</p></div>
<div class="m2"><p>که گل از راه کهکشان برخاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بلبلم در مضیق خارستان</p></div>
<div class="m2"><p>که امیدم ز گلستان برخاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چند نالم که بلبل انصاف</p></div>
<div class="m2"><p>زین مغیلان باستان برخاست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جگر از بس که هم جگر خورد است</p></div>
<div class="m2"><p>معده را ذوق آب و نان برخاست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جان شد اینجا چه خاک بیزد تن</p></div>
<div class="m2"><p>که دکان‌دار از دکان برخاست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خاک شد هر چه خاک برد به دوش</p></div>
<div class="m2"><p>کابخوردش ز خاکدان برخاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جامهٔ گازر آب سیل ببرد</p></div>
<div class="m2"><p>شاید ار درزی ار دکان برخاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چرخ گوئی دکان قصابی است</p></div>
<div class="m2"><p>کز سر تیغ خون فشان برخاست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بره زان سو ترازوی زینسو</p></div>
<div class="m2"><p>چرب و خشکی از این میان برخاست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قسم هر ناکسی سبک فربه</p></div>
<div class="m2"><p>قسم من لاغر و گران برخاست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر سقط گردنی است پهلوسای</p></div>
<div class="m2"><p>زان ز دل طمع گرد ران برخاست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر برفت آبروی ترس برفت</p></div>
<div class="m2"><p>گله مرد و غم شبان برخاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کاروان منقطع شد از در شهر</p></div>
<div class="m2"><p>رصد از راه کاروان برخاست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اشتر اندر وحل به برق بسوخت</p></div>
<div class="m2"><p>باج اشتر ز ترکمان برخاست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نیک عهدی گمان همی بردم</p></div>
<div class="m2"><p>یار، بد عهد شد گمان برخاست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دل خرد مرا غمان بزرگ</p></div>
<div class="m2"><p>از بزرگان خرده دان برخاست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خواری من ز کینه توزی بخت</p></div>
<div class="m2"><p>از عزیزان مهربان برخاست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ای برادر بلای یوسف نیز</p></div>
<div class="m2"><p>از نفاق برادران برخاست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>قوت روزم غمی است سال آورد</p></div>
<div class="m2"><p>که نخواهد به سالیان برخاست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اینت کشتی شکاف طوفانی</p></div>
<div class="m2"><p>که ازین سبز بادبان برخاست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>قضی‌الامر کفت طوفان</p></div>
<div class="m2"><p>به بقای خدایگان برخاست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نیست غم چون به خواستاری من</p></div>
<div class="m2"><p>خسرو صاحب القران برخاست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بعد کشتن قصاص خاقانی</p></div>
<div class="m2"><p>از در شاه شه‌نشان برخاست</p></div></div>