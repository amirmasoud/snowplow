---
title: >-
    شمارهٔ ۶۶ - مطلع سوم
---
# شمارهٔ ۶۶ - مطلع سوم

<div class="b" id="bn1"><div class="m1"><p>تا غبار از چتر شاه اختران افشانده‌اند</p></div>
<div class="m2"><p>فرش سلطانیش در برتر مکان افشانده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شحنهٔ نوروز نعل نقره خنگش ساخته است</p></div>
<div class="m2"><p>هر زری کاکسیر سازان خزان افشانده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسته چون یوسف ز چاه و دلو پیشش ابر و صبح</p></div>
<div class="m2"><p>گوهر از الماس و مشک از پرنیان افشانده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در رکابش هفت گیسودار و شش خاتون ردیف</p></div>
<div class="m2"><p>بر سرش هر هفت و شش عقد جمان افشانده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیست و یک پیکر که از صقلاب دارد خیلتاش</p></div>
<div class="m2"><p>گرد راه خیل او تا قیروان افشانده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا که شد نوروز سلطان فلک را میزبان</p></div>
<div class="m2"><p>عاملان طبع جان بر میزبان افشانده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا که آن سلطان به خوان ماهی آمد میهمان</p></div>
<div class="m2"><p>خازنان بحر در بر میهمان افشانده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وز برای آنکه ماهی بی‌نمک ندهد مزه</p></div>
<div class="m2"><p>ابر و باد آنک نمک‌ها پیش خوان افشانده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بدی مه بر زمین مرده از بهر حنوط</p></div>
<div class="m2"><p>تودهٔ کافور و تنگ زعفران افشانده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور مزاج گوهران را از تناسل بازداشت</p></div>
<div class="m2"><p>طبع کافوری که وقت مهرگان افشانده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خورد خواهد شاهد و شاه فلک محرور وار</p></div>
<div class="m2"><p>آن همه کافور کز هندوستان افشانده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا جهان ناقه شد از سرسام دی ماهی برست</p></div>
<div class="m2"><p>چار مادر بر سرش توش و توان افشانده‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باز نونو در رحم‌های عروسان چمن</p></div>
<div class="m2"><p>نطفهٔ روحانیان بین کز نهان افشانده‌اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مغز گردون را زکام است از دم باد شمال</p></div>
<div class="m2"><p>کابهاش از مغز بر شاخ جوان افشانده‌اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چشم دردی داشت بستان کز سر پستان ابر</p></div>
<div class="m2"><p>شیر بر اطراف چشم بوستان افشانده‌اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاخ طفلی بود و نوخط گشت و بالغ شد کنون</p></div>
<div class="m2"><p>گرد زمرد بر عذارش زان عیان افشانده‌اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کاروان سبزه تا از قاع صف صف کرد ارم</p></div>
<div class="m2"><p>صف صف از مرغان روان بر کاروان افشانده‌اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باد مشک‌آلود گوئی سیب تر بر آتش است</p></div>
<div class="m2"><p>کاندر او قدری گلاب اصفهان افشانده‌اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روز و شب گرگ آشتی کردند و اینک مهر و ماه</p></div>
<div class="m2"><p>نور خود بر یوسف مصر آستان افشانده‌اند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مهر و مه گوئی به باغ از طور نور آورده‌اند</p></div>
<div class="m2"><p>بر سر شروان شه موسی بنان افشانده‌اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یا روان‌های فریبرز و منوچهر از بهشت</p></div>
<div class="m2"><p>نور و فر بر فرق شاه کامران افشانده‌اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خسرو مشرق جلال الدین خلیفهٔ ذو الجلال</p></div>
<div class="m2"><p>کاختران بر فر قدرش فرقدان افشانده‌اند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیشکارانش خراج از هند و چین آورده‌اند</p></div>
<div class="m2"><p>چاوشانش دست بر چیپال و خان افشانده‌اند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آستان بوسان او کز بیژن و گرگین مهند</p></div>
<div class="m2"><p>آستین بر اردشیر و اردوان افشانده‌اند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا زبان شکل است شمشیرش همه شیران رزم</p></div>
<div class="m2"><p>بس که دندان‌ها ز بیم آن زبان افشانده‌اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نیزه دارانش که از شیر نیستان کین کشند</p></div>
<div class="m2"><p>خون و آتش زان نی چون خیز ران افشانده‌اند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نی ز آتش سوزد و اینان ز نی‌های رماح</p></div>
<div class="m2"><p>دشمنان را آتش اندر دودمان افشانده‌اند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زهر خندد بخت بد بر زورق آن خاکسار</p></div>
<div class="m2"><p>کاتشین قاروره‌اش بر بادبان افشانده‌اند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سنگ، خون گرید به عبرت بر سر آن شیشه‌گر</p></div>
<div class="m2"><p>کز هوا سنگ عراده‌ش در دکان افشانده‌اند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عالمی کز ابر جودش در بهار نعمت‌اند</p></div>
<div class="m2"><p>حاسدان را صاعقه در خان و مان افشانده‌اند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خاصگان مریم از نخل کهن خرمای نو</p></div>
<div class="m2"><p>خورده‌اند و بر جهودان استخوان افشانده‌اند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از پی پرواز مرغ دولت او بود و بس</p></div>
<div class="m2"><p>دانها کاین نه رواق باستان افشانده‌اند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وز پی افروزش بزم جلالش دان و بس</p></div>
<div class="m2"><p>نورها کاین هفت شمع بی‌دخان افشانده‌اند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در زمین چار عنصر هفت حراث فلک</p></div>
<div class="m2"><p>تخم دولت تاکنون بر امتحان افشانده‌اند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن چنان تخمی چنین کشورستانی داد بر</p></div>
<div class="m2"><p>بر چنین آید ز تخمی کانچنان افشانده‌اند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر کمندی وقتی اندر حلق سکساران روم</p></div>
<div class="m2"><p>سرکشان لشکر الب ارسلان افشانده‌اند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بندگان شه کمند از چرم شیران کرده‌اند</p></div>
<div class="m2"><p>در کمر گاه پلنگان جهان افشانده‌اند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز آتش تیغی که خاکستر کند دیو سپید</p></div>
<div class="m2"><p>شعله در شیر سیاه سیستان افشانده‌اند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ابرها از تیغ و باران‌ها ز پیکان کرده‌اند</p></div>
<div class="m2"><p>برق‌ها ز آئینهٔ برگستوان افشانده‌اند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تاج کیوان است نعل اسب آن تاج کیان</p></div>
<div class="m2"><p>کز سخا دست و دلش دریا و کان افشانده‌اند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از صهیل اسب شیر آشوب او خرگوش وار</p></div>
<div class="m2"><p>بس دم الحیضا که شیران ژیان افشانده‌اند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دست و بازوش از پی قصر مخالف سوختن</p></div>
<div class="m2"><p>ز آتشین پیکان شررها قصرسان افشانده‌اند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر به عهد موسی امت را گه قحط از هوا</p></div>
<div class="m2"><p>باز من و سلوی سلوت رسان افشانده‌اند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شکر الله کز بقای شاه موسی دست ما</p></div>
<div class="m2"><p>بر شماخی میوه و مرغ جنان افشانده‌اند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>روشنان در عهدش از شروان مدائن کرده‌اند</p></div>
<div class="m2"><p>زیر پایش افسر نوشیروان افشانده‌اند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا به دور دولت او گشت شروان خیروان</p></div>
<div class="m2"><p>عرشیان فیض روان بر خیروان افشانده‌اند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عاقلان دیدند آب عز شروان خاک ذل</p></div>
<div class="m2"><p>بر هری و بلخ و مرو شاهجان افشانده‌اند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر حقند آنان که با عیسی نشستند ار زرشک</p></div>
<div class="m2"><p>خاک بر روی طبیب مهربان افشانده‌اند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آسمان گرید بر آنان کز درش برگشته‌اند</p></div>
<div class="m2"><p>پیش غیری جان به طمع نام و نان افشانده‌اند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ماه تابان کوری پروانگان را بین که جان</p></div>
<div class="m2"><p>بر نتیجه سنگ و موم و ریسمان افشانده‌اند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پیش تیغش کاتش نمرود را ماند ز چرخ</p></div>
<div class="m2"><p>کرکسان پر بر سر خاک هوان افشانده‌اند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>جنیان ترسند ز آهن لیک از عشق کفش</p></div>
<div class="m2"><p>دیدها بر آهن تیغ یمان افشانده‌اند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تازیانش کابل و بلغار دارند آبخور</p></div>
<div class="m2"><p>گرد پی ز آنسوی نیل و عسقلان افشانده‌اند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مغز گردون عطسه داد و حلق دریا سرفه کرد</p></div>
<div class="m2"><p>زان غبار ره که ایام الرهان افشانده‌اند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آتش و باد مجسم دیده‌ای کز گرد و خوی</p></div>
<div class="m2"><p>کوه البرز از سم و قلزم زران افشانده‌اند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از دو سندان چار دندان زحل درهم شکست</p></div>
<div class="m2"><p>جفته‌ای کز نیم راه آسمان افشانده‌اند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دی غباری بر فلک می‌رفت گفتم کاین غبار</p></div>
<div class="m2"><p>مرکبان شه ز راه کهکشان افشانده‌اند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تا فلک گفتا ز نعل مرکبانش من بهم</p></div>
<div class="m2"><p>روشنان خاک سیاهش در دهان افشانده‌اند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کوکب دری است یا در دری کز هر دری</p></div>
<div class="m2"><p>دست و کلکش گاه توقیع از بنان افشانده‌اند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>پنج شاخ دست رادش کز صنوبر رسته‌اند</p></div>
<div class="m2"><p>بر جهان صد نوبر از شاخ امان افشانده‌اند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا قلم را مار گنج پادشاهی کرده‌اند</p></div>
<div class="m2"><p>از دهان مار گنج شایگان افشانده‌اند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بر لعاب گاو کوهی دیدهٔ آهوی دشت</p></div>
<div class="m2"><p>از لعاب زرد مار کم زیان افشانده‌اند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ترجمان یوسف غیب است آن مصری قلم</p></div>
<div class="m2"><p>کاب نیل از تارک آن ترجمان افشانده‌اند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گوئی آندم کز چه مغرب ره مشرق نوشت</p></div>
<div class="m2"><p>میغ بر مهر و زحل بر زبرقان افشانده‌اند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون ز تاریکی به بلغار آمد و قندز فشاند</p></div>
<div class="m2"><p>اهل بابل بر رهش نزل گران افشانده‌اند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>این منم یارب که در بزم چنین اسکندری</p></div>
<div class="m2"><p>چشمهٔ حیوانم از لفظ و لسان افشانده‌اند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چار جوی و هشت خلدست این که در مدحش مرا</p></div>
<div class="m2"><p>از ره کلک و بنان طبع و جنان افشانده‌اند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>داستانی نیست در دست جهان به زین سخن</p></div>
<div class="m2"><p>راستان جان بر سر این داستان افشانده‌اند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تا شب است و ماه نو گوئی که از گوی زمین</p></div>
<div class="m2"><p>گرد بر گردون ز سیمین صولجان افشانده‌اند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>صولجان و گوی شه باد از دل و پشت عدو</p></div>
<div class="m2"><p>کز کفش بر خلق فیض جاودان افشانده‌اند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بر ولی و خصمش از برجیس و از کیوان نثار</p></div>
<div class="m2"><p>سعد و نحسی کان دو علوی در قران افشانده‌اند</p></div></div>