---
title: >-
    شمارهٔ ۳۴ - در مدح صفوة الدین بانوی شروان شاه
---
# شمارهٔ ۳۴ - در مدح صفوة الدین بانوی شروان شاه

<div class="b" id="bn1"><div class="m1"><p>این پرده کاسمان جلال آستان اوست</p></div>
<div class="m2"><p>ابری است کافتاب شرف در عنان اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این ابر بین که معتکف اوست آفتاب</p></div>
<div class="m2"><p>وین آفتاب کابر کرم سایبان اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این پرده گرنه صحن بهشت است پس چرا</p></div>
<div class="m2"><p>رضوان مجاور حرم روضه سان اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این پرده گرنه بحر محیط است پس چرا</p></div>
<div class="m2"><p>اصداف ملک را گهر اندر نهان اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این پرده گرنه عرش مجید است پس چرا</p></div>
<div class="m2"><p>ارواح قدس را قدم اندر میان اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این پرده گرنه چرخ رفیع است پس چرا</p></div>
<div class="m2"><p>سعد السعود را شرف اندر قران اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این پرده گرنه صخرهٔ کعبه است پس چرا</p></div>
<div class="m2"><p>لب‌های عرشیان همه بوسه ستان اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برجیس موسوی کف و کیوان طور حلم</p></div>
<div class="m2"><p>هارون آستانهٔ گردون مکان اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورشید کرد میل زمین بوس او ازآنک</p></div>
<div class="m2"><p>سایه‌اش هزار میل بر از آسمان اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خط امان ستانه‌ش و لب‌های خسروان</p></div>
<div class="m2"><p>العبد بر نوشته به خط امان اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در صف و سجده از قد و پیشانی ملوک</p></div>
<div class="m2"><p>نون و القلم رقم زده بر آستان اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاک درش ز چشم و لب میر زادگان</p></div>
<div class="m2"><p>لاله ستان جنت و عبهرستان اوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ناهید زخمه زن گه چوبک زدن به شب</p></div>
<div class="m2"><p>چابک زن خراجی چوبک زنان اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خورشید روم پرور و ماه حبش نگار</p></div>
<div class="m2"><p>سایه نشین ساحت طوبی نشان اوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا روز و شب دو خادم رومی و نوبی‌اند</p></div>
<div class="m2"><p>هر یک به صدق عنبر جان بر میان اوست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاگرد خادمان در اوست روزگار</p></div>
<div class="m2"><p>کاستاد بحر دست جواهر فشان اوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شروان به عز شاه ز بغداد درگذشت</p></div>
<div class="m2"><p>تا شاهزاده صفوة دین بانوان اوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بانوی شرق و غرب که چون خوان نهد به بزم</p></div>
<div class="m2"><p>عنقا مگس مثال، طفیلی خوان اوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هست آسیه به زهد و زلیخا به ملک از آنک</p></div>
<div class="m2"><p>تسلیم مصر و قاهره بر قهرمان اوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باز سپید دولت و شیر سیاه ملک</p></div>
<div class="m2"><p>کاین پرده هم نشیمن و هم نیستان اوست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این پرده سد دولت و خاقان سکندر است</p></div>
<div class="m2"><p>اسکندر دوم که دوم سد از آن اوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بلقیس بانوان و سلیمان شه اخستان</p></div>
<div class="m2"><p>کز عدل و دین مبشر مهدی زمان اوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جمشید پیل تن نه که خورشید نیل کف</p></div>
<div class="m2"><p>کافلاک تنگ مرکب انجم توان اوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در رزم یازده رخ و با دهر ده دله</p></div>
<div class="m2"><p>تا نه سپهر و هشت جنان هفت خوان اوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز آن تیغ کو بنفش‌تر است از پر مگس</p></div>
<div class="m2"><p>منقار کرکسان فلک میهمان اوست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر چه به خاندانش سلاطین شرف کنند</p></div>
<div class="m2"><p>این بانوی جهان شرف خاندان اوست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زیبد منیژه خادمهٔ بانوان چنانک</p></div>
<div class="m2"><p>افراسیاب نیزه‌کش اخستان اوست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر دست راست و چپ ملکان مادح ویند</p></div>
<div class="m2"><p>خاقانی از زبان ملک مدح خوان اوست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پار آن قصیده گفت که تعویذ عقل بود</p></div>
<div class="m2"><p>و امسال این قصیده که هم حرز جان اوست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر مدح بانوان ز پی سیم و زر کند</p></div>
<div class="m2"><p>زنار کفر خوک خوران طیلسان اوست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ور جز بقای بانو و شاه است کام او</p></div>
<div class="m2"><p>پس داستان سگ صفتان داستان اوست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وردی است بر زبان همه کس را به صبح و شام</p></div>
<div class="m2"><p>وز مدح بانوان همه ورد زبان اوست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یارب به تازگی شرف جاودانش ده</p></div>
<div class="m2"><p>کاسلام تازه از شرف جاودان اوست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>امیدوار باد به بخت ملک چنانک</p></div>
<div class="m2"><p>کامید چرخ پیر به بخت جوان اوست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>او سال را به دولت و تایید ضامن است</p></div>
<div class="m2"><p>نوروز تازه روی ز روی ضمان اوست</p></div></div>