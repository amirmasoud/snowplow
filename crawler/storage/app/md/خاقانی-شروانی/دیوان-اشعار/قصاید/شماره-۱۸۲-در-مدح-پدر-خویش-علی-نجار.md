---
title: >-
    شمارهٔ ۱۸۲ - در مدح پدر خویش علی نجار
---
# شمارهٔ ۱۸۲ - در مدح پدر خویش علی نجار

<div class="b" id="bn1"><div class="m1"><p>سلسلهٔ ابر گشت زلف زره سان او</p></div>
<div class="m2"><p>قرصهٔ خورشید شد گوی گریبان او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنجهٔ شیران شکست قوت سودای او</p></div>
<div class="m2"><p>جوشن مردان گسست ناوک مژگان او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش نمکی شد لبش، تره تر عارضش</p></div>
<div class="m2"><p>بر نمک و تره بین دل‌ها مهمان او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگ به سبزی زند چهره او را مگر</p></div>
<div class="m2"><p>سوی برون داد رنگ پستهٔ خندان او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه ز مهری که نیست، نیست دلش ز آن من</p></div>
<div class="m2"><p>هست بهرسان که هست هستی من ز آن او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دازم زنگار دل، دارم شنگرف اشک</p></div>
<div class="m2"><p>کیست که نقشی کند زین دو بر ایوان او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر من اندر غمش رفت چو ناخن بسر</p></div>
<div class="m2"><p>ماندم ناخن کبود از تب هجران او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه شکر خنده زد بر دل چون آتشم</p></div>
<div class="m2"><p>آتش من مگذرد بر شکرستان او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیلم تازی میان اوست، من از چشم و سر</p></div>
<div class="m2"><p>هندوکی اعجمی، بندهٔ دربان او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق به بانگ بلند گفت که خاقانیا</p></div>
<div class="m2"><p>یار عزیز است سخت، جان تو و جان او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دی پدر من به وهم دایره‌ای برکشید</p></div>
<div class="m2"><p>دید در آن دایره نقطهٔ مرجان او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صانع زرین عمل، پیر صناعت علی</p></div>
<div class="m2"><p>کز ید بیضا گذشت دست عمل ران او</p></div></div>