---
title: >-
    شمارهٔ ۵۴ - در ستایش رکن الدین محمد بن عبد الرحمن طغان یزک
---
# شمارهٔ ۵۴ - در ستایش رکن الدین محمد بن عبد الرحمن طغان یزک

<div class="b" id="bn1"><div class="m1"><p>جام طرب کش که صبح کام برآمد</p></div>
<div class="m2"><p>خندهٔ صبح از دهان جام برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح فلک بین که بر موافقت جام</p></div>
<div class="m2"><p>دم زد و بوی میش ز کام برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهرهٔ شادی نشست و ششدره برخاست</p></div>
<div class="m2"><p>نقش سه شش بر سه زخم کام برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داو طرب کن تمام خاصه که اکنون</p></div>
<div class="m2"><p>عدهٔ خاتون خم تمام برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما و شکر ریز عیش کز در خمار</p></div>
<div class="m2"><p>نامزد خرمی به بام برآمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساغر گلفام خواه کز دهن کوس</p></div>
<div class="m2"><p>نغمهٔ گلبام وقت بام برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبله کبکی است خون گرفته به منقار</p></div>
<div class="m2"><p>کز دهنش نالهٔ حمام برآمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاو سفالین که آب لالهٔ تر خورد</p></div>
<div class="m2"><p>ارزن زرینش از مسام برآمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زآن می گلگون که بید سوخته پرورد</p></div>
<div class="m2"><p>بوی گل و مشکبید خام برآمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در صف دریا کشان بزم صبوحی</p></div>
<div class="m2"><p>جام چو کشتی کش خرام بر آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوان صبوحی به شیب مقرعه کن لاش</p></div>
<div class="m2"><p>کابرش روز آتشین ستام برآمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود فلک جام رنگ و جام فلک سان</p></div>
<div class="m2"><p>روز ندانم که از کدام برآمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست قراسنقر فلک سپر افکند</p></div>
<div class="m2"><p>خنجر آقسنقر از نیام برآمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گوش رباب از هوا پیام طرب داشت</p></div>
<div class="m2"><p>از سه زبان راز آن پیام برآمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حلقهٔ ابریشم است موی خوش چنگ</p></div>
<div class="m2"><p>چون مه نو کز خط ظلام برآمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر چه تن چنگ شبه ناقهٔ لیلی است</p></div>
<div class="m2"><p>نالهٔ مجنون ز چنگ رام برآمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیست و چهارش زمام ناقه و لیکن</p></div>
<div class="m2"><p>ناله نه از ناقه از زمام برآمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نای چو شه زادهٔ حبش که ز نه چشم</p></div>
<div class="m2"><p>بانگش از آهنگ ده غلام برآمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از پی دستینهٔ رباب کف می</p></div>
<div class="m2"><p>چون گهر عقد یک نظام برآمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهر حلی‌های گوش و گردن بر بط</p></div>
<div class="m2"><p>سیم و زر از ساغر و مدام برآمد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از حیوان شکار گاه دف آواز</p></div>
<div class="m2"><p>تهنیت شاه را مدام برآمد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاه عجم رکن دین کز آیت عدلش</p></div>
<div class="m2"><p>نام عجم روضة السلام برآمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ناصر اسلام سیف دین که ز حکمش</p></div>
<div class="m2"><p>بر سر دهر حرون لگام برآمد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رستم ثانی که در طبیعتش اول</p></div>
<div class="m2"><p>دانش زال و دهای سام بر آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صیت جلالش به شرق و غرب بپیچید</p></div>
<div class="m2"><p>شکر نوالش ز سام و حام برآمد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پهلو ایران گرفت رقعهٔ ملکت</p></div>
<div class="m2"><p>وز دگران بانگ شاهقام برآمد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دام به دریا فکنده بود سلیمان</p></div>
<div class="m2"><p>خازن انگشتری به دام برآمد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ذات جهان پهلوانش صبح جلال است</p></div>
<div class="m2"><p>کز افق چرخ احتشام برآمد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در کنف صبح فر میر محمد</p></div>
<div class="m2"><p>راست چو خورشید نور تام برآمد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تاجوری یافت تخت و ملکت ایران</p></div>
<div class="m2"><p>تا ز برش سیدالانام برآمد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر پدر از تخت ملک شد پسر اینک</p></div>
<div class="m2"><p>بر زبر تخت احترام برآمد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر علم صبح آب رنگ فروشد</p></div>
<div class="m2"><p>رایت خورشید نارفام برآمد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تارک گشتاسب یافت افسر لهراسب</p></div>
<div class="m2"><p>زال همایون به تخت سام برآمد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نوبت کاوس شد چو پای منوچهر</p></div>
<div class="m2"><p>بر سر کرسی احتشام برآمد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روز به مغرب شده چو مملکت او</p></div>
<div class="m2"><p>ماه چو بدر از حجاب شام برآمد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آرزوی جان ملک عدل و همم بود</p></div>
<div class="m2"><p>از ملک عادل همام برآمد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دولت شروان کلید دولت او بود</p></div>
<div class="m2"><p>زآن همه کارش به انتظام برآمد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر چه محمد پیمبری به عرب یافت</p></div>
<div class="m2"><p>صبح کمالش ز حد شام برآمد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دیر زی ای بحر کف که عطسهٔ جودت</p></div>
<div class="m2"><p>چشمهٔ مهر است کز غمام برآمد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مژده ده ای تاجور که ینصرک الله</p></div>
<div class="m2"><p>فال تو از مصحف دوام برآمد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا که حسامت قوام ملک عجم شد</p></div>
<div class="m2"><p>آه ز اعدای ناقوام برآمد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چون نم ژاله ز خایه از تف خورشید</p></div>
<div class="m2"><p>جان حسود از تف حسام برآمد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جرم زمین تا قرار یافت ز عدلت</p></div>
<div class="m2"><p>بس نفس شکر کز هوام برآمد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دوش چنین دیده‌ام به خواب که نخلی</p></div>
<div class="m2"><p>بر لب دریا در آن مقام برآمد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نخل موصل شده ترنج و رطب داشت</p></div>
<div class="m2"><p>سایه و شایه‌ش فراخ و تام برآمد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مرغی دیدم گرفته نامه به منقار</p></div>
<div class="m2"><p>کز بر آن نخل شادکام برآمد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بود یکی منبر از رخام بر نخل</p></div>
<div class="m2"><p>پیری بر منبر رخام برآمد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نامه ز منقار مرغ بستد و برخواند</p></div>
<div class="m2"><p>نعرهٔ تحسین ز خاص و عام برآمد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>من به تعجب به خود فروشده زین خواب</p></div>
<div class="m2"><p>کز خضر آواز السلام برآمد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جستم و این خواب پیش خضر بگفتم</p></div>
<div class="m2"><p>از نفسش اصدق الکلام برآمد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گفت که نخل است رکن دین که ز نصرت</p></div>
<div class="m2"><p>شهپر عنقاش بر سهام برآمد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مرغ بقا دان و نامه بخت کز این دو</p></div>
<div class="m2"><p>کار دو ملک از یک اهتمام برآمد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>منبر تخت است و پیر مشتری چرخ</p></div>
<div class="m2"><p>کز بر تختش سه چار گام برآمد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ای درت آن آسمان که از افق او</p></div>
<div class="m2"><p>کوکب بهروزی کرام برآمد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از دم خلق تو در مسدس گیتی</p></div>
<div class="m2"><p>بوی مثلث به هر مشام برآمد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ملک تو کشتی است چرخ نوح کهن سال</p></div>
<div class="m2"><p>کش ز شب و روز حام و سام برآمد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>عیسی عهدی که از تو قالب ملکت</p></div>
<div class="m2"><p>چون تن عازر به یک قیام برآمد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>رو که ز میخ سرای پردهٔ قدرت</p></div>
<div class="m2"><p>فلکهٔ این نیل گون خیام برآمد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>قدر محیط کفت جهان چه شناسد</p></div>
<div class="m2"><p>کو به سراب کف لئام برآمد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از نفس مشک هیچ حظ و خبر نیست</p></div>
<div class="m2"><p>مغز جعل را که با زکام برآمد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از سر تیغت که ماه ازوست برص دار</p></div>
<div class="m2"><p>برتن شیر فلک جذام برآمد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خوان ددان را به کاسهٔ سر اعدا</p></div>
<div class="m2"><p>زآتش شمشیر تو طعام برآمد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بر درت از بس که جن و انس و ملک هست</p></div>
<div class="m2"><p>جان شیاطین ز ازدحام برآمد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گوئی کانبوه حافظان مناسک</p></div>
<div class="m2"><p>گرد در مسجد الحرام برآمد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از حرمت هر کبوتری که بپرید</p></div>
<div class="m2"><p>نامهٔ او عنبرین ختام برآمد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سهم تو در زین کشید پشت زمین را</p></div>
<div class="m2"><p>گر چه ز من بود قعده رام برآمد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بحر محیط از زمین بزاد و عجب نیست</p></div>
<div class="m2"><p>کان خوی ازین مرکب جمام برآمد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زایجهٔ طالعت مطالعه کردم</p></div>
<div class="m2"><p>سلطنت از موضع السهام برآمد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آرزوی حضرت تو دارم اگر چه</p></div>
<div class="m2"><p>صبح من از غم به رنگ شام برآمد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>در ره خدمت درست عهدم لیکن</p></div>
<div class="m2"><p>نام من از نامهٔ سقام برآمد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هست نیازم ز جان و آن دگر کس</p></div>
<div class="m2"><p>از زر و سیم جهان حطام برآمد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گوهر جان وام کردم از پی تحفه</p></div>
<div class="m2"><p>تحفه بزرگ است از آن به وام برآمد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>پیش چنین تحفه کو تمیمهٔ عقل است</p></div>
<div class="m2"><p>واحزن از جان بوتمام برآمد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گوهر سحر حلال من شکند آنک</p></div>
<div class="m2"><p>گوهرش از نطفهٔ حرام برآمد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دزد بیان من است هر که در این عهد</p></div>
<div class="m2"><p>بر سمت شاعریش نام برآمد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>نیم شبت چون صف خواص دعا گفت</p></div>
<div class="m2"><p>هر نفسی آمینی از عوام برآمد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>باد جهانت به کام کز ظفر تو</p></div>
<div class="m2"><p>کامهٔ صد جان مستهام برآمد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ملک جهان ران که بر صحیفهٔ ایام</p></div>
<div class="m2"><p>مدت عمرت هزار عام برآمد</p></div></div>