---
title: >-
    شمارهٔ ۲۲۲ - در مدح ابوالمظفر جلال الدین شروان شاه اخستان
---
# شمارهٔ ۲۲۲ - در مدح ابوالمظفر جلال الدین شروان شاه اخستان

<div class="b" id="bn1"><div class="m1"><p>بردار زلفش از رخ تا جان تازه بینی</p></div>
<div class="m2"><p>وز نیم کشت غمزه‌اش قربان تازه بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک سو فکن دو زلفش و ایمانت تازه گردان</p></div>
<div class="m2"><p>کاندر حجاب کفرش ایمان تازه بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پروانهٔ غمش را هر دم به خون خلقی</p></div>
<div class="m2"><p>شمشیر تیز یابی، فرمان تازه بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترکان غمزهٔ او چون درکشند یاسج</p></div>
<div class="m2"><p>در هر دلی که جویی پیکان تازه بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مجلسی که بگذشت از یاد او حدیثی</p></div>
<div class="m2"><p>در هر لب سفالین ریحان تازه بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دم ز برق خندش چون کرد بوسه باران</p></div>
<div class="m2"><p>بر کشت‌زار عمرم باران تازه بینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانی به باد دستی بر خاک پایش افشان</p></div>
<div class="m2"><p>کنگه مزید بر سر صد جان تازه بینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاقانیا در آتش سرمست شو ز عشقش</p></div>
<div class="m2"><p>تا در میان آتش بستان تازه بینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر در ره عراقت دردی گذشت بر دل</p></div>
<div class="m2"><p>ز اقبال شاه شروان درمان تازه بینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ز آستان سلطان باز آمدی ممکن</p></div>
<div class="m2"><p>در بارگاه خاقان امکان تازه بینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان‌بخش ابوالمظفر شاه اخستان که هر دم</p></div>
<div class="m2"><p>با عهد او بقا را پیمان تازه بینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عادل جلال دین آن کز فضل ذو الجلالش</p></div>
<div class="m2"><p>بر دعوی ممالک، برهان تازه بینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کعبه است حضرت او کز چار پای تختش</p></div>
<div class="m2"><p>بیرون ز چار ارکان، ارکان تازه بینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خود حضرتش جهانی است کز عنصر کمالش</p></div>
<div class="m2"><p>برتر ز هفت بنیان، بنیان تازه بینی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در سایهٔ رکابش فتنه بخفت و دین را</p></div>
<div class="m2"><p>در جذبهٔ عنانش جولان تازه بینی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بختش به صبح خیزی تا کوفت کوس دولت</p></div>
<div class="m2"><p>گل‌بانگ کوس او را دستان تازه بینی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او جان عالم آمد در صحن عالم جان</p></div>
<div class="m2"><p>چوگان و گوی او را میدان تازه بینی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواهد سپهر کاندم خورشی گوی گردد</p></div>
<div class="m2"><p>چون در کفش هلالی چوگان تازه بینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صدرش چون باغ رضوان یاصفهٔ سلیمان</p></div>
<div class="m2"><p>کز منطق الطیورش الحان تازه بینی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صف بسته خوان او را عقلی که چون سلیمان</p></div>
<div class="m2"><p>بر کرسی دماغش سلطان تازه بینی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در خطبه شاه کیهان خوانیش گر بجویی</p></div>
<div class="m2"><p>بر تخت طاقدیسش کیهان تازه بینی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زو عالم خرف را، برنای نغز یابی</p></div>
<div class="m2"><p>زو گنبد کهن را، دوران تازه بینی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سر بر کن ای منوچهر از خاک تا پس از خود</p></div>
<div class="m2"><p>ز اقبال بوالمظفر شروان تازه بینی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شروان مدائن آمد چون بنگری به حضرت</p></div>
<div class="m2"><p>کسری وقت یابی، ایوان تازه بینی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یارب چه دولت او سرسامی است عالم</p></div>
<div class="m2"><p>کز فتنه هر زمانش بحران تازه بینی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عیدی است پیش بزمش کز نزل آسمانی</p></div>
<div class="m2"><p>چون دعوت مسیحش صد خوان تازه بینی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هست آسمان سیاست وز آفتاب فضلش</p></div>
<div class="m2"><p>دی ماه بندگان را نیسان تازه بینی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ملکش بخلد ماند در هشت خلد ملکش</p></div>
<div class="m2"><p>از ذات شهریاری رضوان تازه بینی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دستش به کان چه ماند کز لعل تاج شاهان</p></div>
<div class="m2"><p>بر خاک درگه او صد کان تازه بینی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خصمش ز کم بقائی ماند به کرم پیله</p></div>
<div class="m2"><p>کورا ز کردهٔ خود زندان تازه بینی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تیرش زحل بسوزد کز کام حوت گردون</p></div>
<div class="m2"><p>بر قبضهٔ کمانش دندان تازه بینی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دریاست آستانش کز اشک داد خواهان</p></div>
<div class="m2"><p>بر هر کران دریا مرجان تازه بینی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>طفلی است شیرخواره بختش که در لب او</p></div>
<div class="m2"><p>ناهید را به هر دم پستان تازه بینی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نوروز ران گشاده است از موکب جلالش</p></div>
<div class="m2"><p>تا پیکر جهان را خندان تازه بینی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خورشید گویی از نو سالار خوان او شد</p></div>
<div class="m2"><p>کورا ز ماهی اکنون بریان تازه بینی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شرح مناقبش را باد آسمان صحیفه</p></div>
<div class="m2"><p>تا در کف عطارد دیوان تازه بینی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بادش کمال دولت تا هردم از کمالش</p></div>
<div class="m2"><p>در ملک آل سامان، سامان تازه بینی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فهرست ملک بادا نامش که تا قیامت</p></div>
<div class="m2"><p>زو نامهٔ کرم را، عنوان تازه بینی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خمسین الف بادا ثلث بقاش کز وی</p></div>
<div class="m2"><p>بر اهل ربع مسکون احسان تازه بینی</p></div></div>