---
title: >-
    شمارهٔ ۱۶ - مطلع دوم
---
# شمارهٔ ۱۶ - مطلع دوم

<div class="b" id="bn1"><div class="m1"><p>ای رای تو صیقل اختران را</p></div>
<div class="m2"><p>افسر توئی افسر سران را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک در تو به عرض مصحف</p></div>
<div class="m2"><p>جای قسم است داوران را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر هفته ز تیغ تو عطیت</p></div>
<div class="m2"><p>هفت اقلیم است سروران را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کعبهٔ حضرت تو جبریل</p></div>
<div class="m2"><p>دست آب دهد مجاوران را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شاخ گوزن بر در تو</p></div>
<div class="m2"><p>قامت شده خم غضنفران را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دایه شده بر قریش و برمک</p></div>
<div class="m2"><p>صدق و کرم تو جعفران را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا محضر نصرتت نوشتند</p></div>
<div class="m2"><p>آوازه شکست دیگران را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کانجا که محمد اندر آمد</p></div>
<div class="m2"><p>دعوت نرسد پیمبران را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر دهر حرونیی نموده است</p></div>
<div class="m2"><p>چون رام تو گشت منگر آن را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنگر که چو دست یافت یوسف</p></div>
<div class="m2"><p>چه لطف کند برادران را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از عالم زاده‌ای و پیشت</p></div>
<div class="m2"><p>عالم تبع است چاکران را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم رد مکنش که راد مردان</p></div>
<div class="m2"><p>حرمت دارند مادران را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قدرت ز برای کار تو ساخت</p></div>
<div class="m2"><p>این قبهٔ نغز بی‌کران را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر خاتم دست تو نزیبد</p></div>
<div class="m2"><p>هم حلقه نشاید استران را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صحن فلک از بزان انجم</p></div>
<div class="m2"><p>ماند رمهٔ مضمران را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست از پی بر نشست خاصت</p></div>
<div class="m2"><p>امید خصی شدن نران را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صاحب غرضند روس و خزران</p></div>
<div class="m2"><p>منکر شده صاحب افسران را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تیغ تو مزوری عجب ساخت</p></div>
<div class="m2"><p>بیماری آن مزوران را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فتح تو به جنگ لشکر روس</p></div>
<div class="m2"><p>تاریخ شد آسمان قران را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رایات تو روس را علی روس</p></div>
<div class="m2"><p>صرصر شده ساق ضمیران را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیکان شهاب رنگ چون آب</p></div>
<div class="m2"><p>آتش زده دیو لشکران را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در زهرهٔ روس رانده زهر آب</p></div>
<div class="m2"><p>کانداخته یغلق پران را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یک سهم تو خضروار بشکافت</p></div>
<div class="m2"><p>هفتاد و سه کشتی ابتران را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مقراضهٔ بندگان چو مقراض</p></div>
<div class="m2"><p>اوداج بریده منکران را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بس دوخته سگ زنت چو سوزن</p></div>
<div class="m2"><p>در زهره جگر مبتران را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اقبال تو کاب خضر خورده است</p></div>
<div class="m2"><p>دل داده نهنگ خنجران را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وز بس که ز خصم بر لب بحر</p></div>
<div class="m2"><p>خون رفت بریده حنجران را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هم بر لب بحر بحر کردار</p></div>
<div class="m2"><p>خون شد چو شفق دل اشقران را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با ترکشت اژدهای موسی</p></div>
<div class="m2"><p>بنمود مجوس مخبران را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در روم ز اژدهای تیرت</p></div>
<div class="m2"><p>زهر است نواله قیصران را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون از مه نو زنی عطارد</p></div>
<div class="m2"><p>مریخ هدف شود مرآن را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر زال ببست پر سیمرغ</p></div>
<div class="m2"><p>بر تیر، هلاک صفدران را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر تیر تو پر جبرئیل است</p></div>
<div class="m2"><p>آفت شده دیو جوهران را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن بیلک جبرئیل پرت</p></div>
<div class="m2"><p>عزرائیل است جانوران را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بسته کمر آسمان چو پیکان</p></div>
<div class="m2"><p>ماند به درت مسخران را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شیران شده یاوران رزمت</p></div>
<div class="m2"><p>اقبال تو نجده یاوران را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سیمرغ به نامه بردن فتح</p></div>
<div class="m2"><p>می رشک برد کبوتران را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نصرت که دهد به بد سگالت</p></div>
<div class="m2"><p>هرا که برافکند خران را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>با لطف تو در میان نهاده است</p></div>
<div class="m2"><p>خاقانی امید بیکران را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کز لطف تو هم نشد گسسته</p></div>
<div class="m2"><p>امید بهشت، کافران را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در مدحت تو به هفت اقلیم</p></div>
<div class="m2"><p>شش ضربه دهد سخنوران را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شهباز سخن به دولت تو</p></div>
<div class="m2"><p>منقار برید نو پران را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با گاو زری که سامری ساخت</p></div>
<div class="m2"><p>گوساله شمار زرگران را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر هست سخن گهر، چرا نیست</p></div>
<div class="m2"><p>آهنگ بدو گهر خران را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر شادی دل ز زعفران خاست</p></div>
<div class="m2"><p>چون رنگ غم است زعفران را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا حشر فذلک بقا باد</p></div>
<div class="m2"><p>توقیع تو داد گستران را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در جنت مجلست چراگاه</p></div>
<div class="m2"><p>آهو حرکات احوران را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بزمت فلک و سرات منزل</p></div>
<div class="m2"><p>ماهان ستاره زیوران را</p></div></div>