---
title: >-
    شمارهٔ ۲۰۲ - مطلع سوم
---
# شمارهٔ ۲۰۲ - مطلع سوم

<div class="b" id="bn1"><div class="m1"><p>مهر است یا زرین صدف خرچنگ را یار آمده</p></div>
<div class="m2"><p>خرچنگ ناپروا ز تف، پروانهٔ نار آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیمار بوده جرم خور سرطانش داده زور و فر</p></div>
<div class="m2"><p>معجون سرطانی نگر داروی بیمار آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن کعبهٔ محرم نشان، وان زمزم آتش فشان</p></div>
<div class="m2"><p>در کاخ مه دامن کشان یک مه به پروار آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سنگ را گر ساحری کرده صبا میناگری</p></div>
<div class="m2"><p>از خشت زر خاوری میناش دینار آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع روان بین در هوا آتش فشان بین در هوا</p></div>
<div class="m2"><p>بر کرکسان بین در هوا پرواز دشوار آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشید زرین دهره بین صحرای آتش چهره بین</p></div>
<div class="m2"><p>در مغز افعی مهره بین چون دانهٔ نار آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی سپهر چنبری بگرفت رنگ اغبری</p></div>
<div class="m2"><p>بر آینهٔ اسکندری خاکستر انبار آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر فرش سقلاطون که مه صباغ او بوده سه مه</p></div>
<div class="m2"><p>از آتش گردون سیه چون داغ قصار آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفاق را از جرم‌خور هم قرص و هم آتش نگر</p></div>
<div class="m2"><p>هم مطبخ و هم خوان زر هم میده سالار آمده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بلبل بسیار گو، بست از فراق گل گلو</p></div>
<div class="m2"><p>گلگون صراحی بین در او بلبل به گفتار آمده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر می‌دهی ممزوج ده، کاین وقت می ممزوج به</p></div>
<div class="m2"><p>بر می گلاب ناب نه چون اشک احرار آمده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کافور خواه و بیدتر، در خیش‌خانه باده خور</p></div>
<div class="m2"><p>با ساقی فرخنده فر زو خانه فرخار آمده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ماورد و ریحان کن طلب توزی و کتان کن سلب</p></div>
<div class="m2"><p>وز می گلستان کن دو لب آنجا که این چار آمده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گه‌گه کن از باغ آرزو آن آفتاب زرد رو</p></div>
<div class="m2"><p>پیرامنش ده ماه نو هر سال یک بار آمده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چرخ از سموم گرمگه، زاده و با هر چاشتگه</p></div>
<div class="m2"><p>دفع وبا را جام شه یاقوت کردار آمده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تریاق ما چهر ملک، پور منوچهر ملک</p></div>
<div class="m2"><p>با طاعن مهر ملک طاعون سزاوار آمده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خاقان اعظم چون پدر شاه معظم چون پدر</p></div>
<div class="m2"><p>فخر دو عالم چون پدر وز عالمش عار آمده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گردون دوان در کار او چون سایه در زنهار او</p></div>
<div class="m2"><p>خورشید در دیدار او چون ذره دیدار آمده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از بوس لب‌های سران بر پای اسب اخستان</p></div>
<div class="m2"><p>از نعل اسبش هر زمان یاقوت مسمار آمده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عدلش بدان سامان شده کاقلیم‌ها یکسان شده</p></div>
<div class="m2"><p>سنقر به هندستان شده، طوطی به بلغار آمده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رایش چو دست موسوی در ملک برهانی قوی</p></div>
<div class="m2"><p>دادش چو باد عیسوی تعویذ انصار آمده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شمشیر او قصار کین شسته به خون روی زمین</p></div>
<div class="m2"><p>پیکان او خیاط دین دل‌دوز کفار آمده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سام نریمان چاکرش، رستم نقیب لشکرش</p></div>
<div class="m2"><p>هوشنگ هارون درش، جم حاجب بار آمده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مردان علوی هفت تن، درگاه او را نوبه زن</p></div>
<div class="m2"><p>خصمان سفلی چار زن، پیشش پرستار آمده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باتیغ گردون پیکرش گردون شده خاک درش</p></div>
<div class="m2"><p>وز رای گیتی داورش گیتی نمودار آمده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با دولت شاه اخستان، منسوخ دان هر داستان</p></div>
<div class="m2"><p>کز خسروان باستان در صحف اخبار آمده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تیرش که دستان ساخته، زو رجم شیطان ساخته</p></div>
<div class="m2"><p>عقرب ز پیکان ساخته تنین ز سوفار آمده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>او نور و بدخواهانش خاک از ظلمت خاکی چه باک</p></div>
<div class="m2"><p>آن را که حصن جان پاک از نور انوار آمده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر تیر او پرپری صرصر صفت در صفدری</p></div>
<div class="m2"><p>تیرش چو تیغ حیدری از خلد ابرار آمده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اشرار مشتی بازپس، رانده به کین او نفس</p></div>
<div class="m2"><p>پیکانش چون پر مگس در چشم اشرار آمده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ناکرده مکر مکیان جان محمد را زیان</p></div>
<div class="m2"><p>چون عنکبوتی در میان پروانهٔ غار آمده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای خانه دار ملک و دین تیغت حصار ملک و دین</p></div>
<div class="m2"><p>بهر عیار ملک و دین رای تو معیار آمده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پیشت صف بهرامیان بسته غلامی را میان</p></div>
<div class="m2"><p>در خانهٔ اسلامیان عدل تو معمار آمده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای چنبر کوست فلک، کرده زمین بوست فلک</p></div>
<div class="m2"><p>وز خصم منحوست فلک، چون بخت بیزار آمده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نیکان ملت را به دین، یاد تو تسبیح مهین</p></div>
<div class="m2"><p>پیکان نصرت را به کین عزم تو هنجار آمده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بادت ز غایات هنر بر عرش رایات خطر</p></div>
<div class="m2"><p>در شانت آیات ظفر، از فضل دادار آمده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تابع فلک فرمانت را، دربان ملک ایوانت را</p></div>
<div class="m2"><p>سرهای بدخواهانت را هم رمح تو دار آمده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>لاف از درت اسلام را فال از برت اجرام را</p></div>
<div class="m2"><p>تا ابلق ایام را از چرخ مضمار آمده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از مدح تو اشعار من رونق فزا در کار من</p></div>
<div class="m2"><p>دولت همیشه یار من با بخت بیدار آمده</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من جان سپار مدح تو صورت نگار مدح تو</p></div>
<div class="m2"><p>با آب کار مدح تو الفاظم ابکار آمده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>امروز احرار زمن خوانندم استاد سخن</p></div>
<div class="m2"><p>صد عنصری در پیش من شاگرد اشعار آمده</p></div></div>