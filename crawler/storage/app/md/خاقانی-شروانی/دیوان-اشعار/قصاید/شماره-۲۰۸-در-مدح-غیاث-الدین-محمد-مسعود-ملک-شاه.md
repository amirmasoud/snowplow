---
title: >-
    شمارهٔ ۲۰۸ - در مدح غیاث الدین محمد مسعود ملک‌شاه
---
# شمارهٔ ۲۰۸ - در مدح غیاث الدین محمد مسعود ملک‌شاه

<div class="b" id="bn1"><div class="m1"><p>ما فتنه بر توایم و تو فتنه بر آینه</p></div>
<div class="m2"><p>ما رانگاه در تو، تو را اندر آینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا آینه جمال تو دید و تو حسن خویش</p></div>
<div class="m2"><p>تو عاشق خودی ز تو عاشق‌تر آینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از روی تو در آینه جان‌ها شود خیال</p></div>
<div class="m2"><p>زین روی نازها کند اندر سر آینه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز نور روی و صفوت لعل تو آورد</p></div>
<div class="m2"><p>در یک مکان هم آتش و هم کوثر آینه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ناخدای ترس مشو آینه‌پرست</p></div>
<div class="m2"><p>رنج دلم مخواه و منه دل بر آینه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کز آه دل بسوزم هر جا که آهنی است</p></div>
<div class="m2"><p>تا هیچ صیقلی نکند دیگر آینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قبله مساز ز آینه هر چند مر تو را</p></div>
<div class="m2"><p>صورت هر آینه بنماید هر آینه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آینه دریغ بود صورتی کز او</p></div>
<div class="m2"><p>بیند هزار صورت جان پرور آینه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صورت نمای شد رخ خاقانی از سرشک</p></div>
<div class="m2"><p>رخسار او نگر صنما منگر آینه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از رای شاه گیرد نور وضو آفتاب</p></div>
<div class="m2"><p>وز روی تو پذیرد زیب و فر آینه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سلطان اعظم آنکه اشارات او ز غیب</p></div>
<div class="m2"><p>چونان دهد نشانی کز پیکر آینه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاهنشهی که بهر عروس جلال اوست</p></div>
<div class="m2"><p>هفت آسمان مشاطه و هفت اختر آینه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز اقبال عدل‌پرور او جای آن، بود</p></div>
<div class="m2"><p>کز ننگ زنگ باز رهد یکسر آینه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای خسروی که خاطر تو آن صفا گرفت</p></div>
<div class="m2"><p>کز وی نمونه‌ای است به هر کشور آینه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سازد فلک ز حزم تو دایم سلاح خویش</p></div>
<div class="m2"><p>دارد شجاع روز وغا در بر آینه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر منظر تو نور بر آئینه افکند</p></div>
<div class="m2"><p>روح القدس نماید از آن منظر آینه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرد خلافت ار برود در دیار خصم</p></div>
<div class="m2"><p>بی‌کار ماند آنجا تا محشر آینه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ماند به نوک کلک تو و جان بد سگال</p></div>
<div class="m2"><p>چون در حجاب زنگ شود مضمر آینه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باشد چو طبع مهر من اندر هوای تو</p></div>
<div class="m2"><p>چون تاب گیرد از حرکات خور آینه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من آینه ضمیرم و تو مشتری همم</p></div>
<div class="m2"><p>از تو جمال همت و از چاکر آینه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در خدمت تو تر نتوان آمدن از آنک</p></div>
<div class="m2"><p>گردد سیاه روی چو گردد تر آینه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر در دل تو یافت توانم نشان خویش</p></div>
<div class="m2"><p>طبعم شود ز لطف چو از جوهر آینه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>طوطی هر آن سخن که بگوئی ز بر کند</p></div>
<div class="m2"><p>هرگه که شکل خویش ببیند در آینه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر لطف تو خرید مرا بس شگفت نیست</p></div>
<div class="m2"><p>کاهل بصر خزند به سیم و زر آینه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور ناکسی فروخت مرا هم روا بود</p></div>
<div class="m2"><p>کاعمی و زشت را نبود درخور آینه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر جز تو را ستودم بر من مگیر از آنک</p></div>
<div class="m2"><p>مردم ضرورتی کند از خنجر آینه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دانم تو را ز من نگزیرد برای آنک</p></div>
<div class="m2"><p>گه‌گه کند پاک به خاکستر آینه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از نیم شاعران هنر من مجوی از آنک</p></div>
<div class="m2"><p>ناید همی ز آهن بد گوهر آینه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شاید که ناورم دل مجروح بر درت</p></div>
<div class="m2"><p>زیبد که ننگرم به رخ اصفر آینه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کز بیم رجم برنشود دیو بر فلک</p></div>
<div class="m2"><p>وز بهر عیب کم طلبد اعور آینه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر نه ردیف شعر مرا آمدی به کار</p></div>
<div class="m2"><p>مانا که خود نساختی اسکندر آینه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>این را نقیضه‌ای است که گفتم بدین طریق</p></div>
<div class="m2"><p>گر ذره‌ای ز نور تو افتد بر آینه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بادت جلال و مرتبه چندان که آسمان</p></div>
<div class="m2"><p>هر صبح‌دم برآورد از خاور آینه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حاسد ز دولت تو گرفتار آن مرض</p></div>
<div class="m2"><p>کز مس کند برای وی آهنگر آینه</p></div></div>