---
title: >-
    شمارهٔ ۱۲۱ - در ستایش فخر الدین منوچهر شروان شاه به التزام لفظ «عید» در هر بیت
---
# شمارهٔ ۱۲۱ - در ستایش فخر الدین منوچهر شروان شاه به التزام لفظ «عید» در هر بیت

<div class="b" id="bn1"><div class="m1"><p>رخسار صبح را نگر از برقع زرش</p></div>
<div class="m2"><p>کز دست شاه جامهٔ عیدی است در برش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردون به شکل مجمر عیدی به بزم شاه</p></div>
<div class="m2"><p>صبح آتش ملمع و شب مشک اذفرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشرق به عود سوخته دندان سپید کرد</p></div>
<div class="m2"><p>چون بوی عطر عید برآمد ز مجمرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردون فرو گذاشت هزاران حلی که داشت</p></div>
<div class="m2"><p>صاعی بساخت کز پی عید است درخورش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ سحر شناعت از آن زد چو مصریان</p></div>
<div class="m2"><p>کان صاع دید ببار سحر درش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آری به صاع عید همی ماند آفتاب</p></div>
<div class="m2"><p>از نام شاه و داغ نهاده مشهرش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغی است بر جبین سپهر از سه حرف عید</p></div>
<div class="m2"><p>ماه نوابتدای سه حرف است بنگرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فصاد بود صبح که قیفال شب گشاد</p></div>
<div class="m2"><p>خورشید طشت خون و مه عید نشترش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مه روزه دار بود همانا از آن شده است</p></div>
<div class="m2"><p>تن چون خلال مایدهٔ عید لاغرش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا حلقه‌گویی از پی آن شد که روز عید</p></div>
<div class="m2"><p>خسرو به نوک نیزه رباید ز خاورش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاقان اکبر آنکه ز دیوان نصرت است</p></div>
<div class="m2"><p>بر صد هزار عید برات مقررش</p></div></div>