---
title: >-
    شمارهٔ ۴۱ - قصیده
---
# شمارهٔ ۴۱ - قصیده

<div class="b" id="bn1"><div class="m1"><p>خسرو بدار ملک جم ایوان تازه کرد</p></div>
<div class="m2"><p>در هشت خلد مملکه بستان تازه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیخسرو تهمتن بر زال سیستان</p></div>
<div class="m2"><p>در ملک نیم روز شبستان تازه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این کعبه را که سد سکندر حریم اوست</p></div>
<div class="m2"><p>خضر خلیل مرتبه بنیان تازه کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر ثبات ملک چنین کعبهٔ جلال</p></div>
<div class="m2"><p>از بوقبیس حلم خود ارکان تازه کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصری که عرش کنگرهٔ اوست آسمان</p></div>
<div class="m2"><p>از عقد انجمش گهر افشان تازه کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانا که بهر تاختن مرکبان عقل</p></div>
<div class="m2"><p>مهدی به عالم آمد و میدان تازه کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا عالمی ز لطف برآورد کردگار</p></div>
<div class="m2"><p>وانگه در او معادن حیوان تازه کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست کرم گشاد شه و پای بخل بست</p></div>
<div class="m2"><p>تا پیشگاه قصر شرف وان تازه کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قحط سخا ز کشور امید برگرفت</p></div>
<div class="m2"><p>گر خلق بهر عاطفه باران تازه کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاهی که بهر کوههٔ زین‌های ختلیانش</p></div>
<div class="m2"><p>ماهی به چرخ تحفه ز دندان تازه کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاقان اعظم آنکه بقا با سعادتش</p></div>
<div class="m2"><p>هم‌شیرهٔ ابد شد و پیمان تازه کرد</p></div></div>