---
title: >-
    شمارهٔ ۳۵ - در بی‌اعتنایی به دنیا
---
# شمارهٔ ۳۵ - در بی‌اعتنایی به دنیا

<div class="b" id="bn1"><div class="m1"><p>نه به دولت نظری خواهم داشت</p></div>
<div class="m2"><p>نه ز سلوت اثری خواهم داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه از آن روز فرو رفتهٔ عمر</p></div>
<div class="m2"><p>پس پیشین خبری خواهم داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میوه دارم که به دی مه شکفد</p></div>
<div class="m2"><p>که نه برگی نه بری خواهم داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرم شب تابم در تابش روز</p></div>
<div class="m2"><p>که نه زوری نه فری خواهم داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وه که سد ره من جان و دل است</p></div>
<div class="m2"><p>که به سدره مقری خواهم داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه نه کارم ز فلک نیک بد است</p></div>
<div class="m2"><p>من هراس از بتری خواهم داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیشه‌ای بینم پر دیو و پری</p></div>
<div class="m2"><p>من پی هر بشری خواهم داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بر عالم گوساله پرست</p></div>
<div class="m2"><p>رخت بر گاو ثری خواهم داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیر باران بلا پیش و پس است</p></div>
<div class="m2"><p>از فراغت سپری خواهم داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه روز و شب عمرم خواب است</p></div>
<div class="m2"><p>خواب شب مختصری خواهم داشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز اعمی است شب انده من</p></div>
<div class="m2"><p>که نه چشم سحری خواهم داشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخت گویند که در خواب خر است</p></div>
<div class="m2"><p>مه نه دنبال خری خواهم داشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر چه چون آب همه تن زرهم</p></div>
<div class="m2"><p>نه امید ظفری خواهم داشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون زره گرچه همه تن چشمم</p></div>
<div class="m2"><p>نه به دیدن بصری خواهم داشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به زمستان چو تموز از تف آه</p></div>
<div class="m2"><p>تاب خانهٔ جگری خواهم داشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خانه جان دارم و خوانچه سرخوان</p></div>
<div class="m2"><p>که نه طبخی نه خوری خواهم داشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چارپایی دو سه و یک دو غلام</p></div>
<div class="m2"><p>چارپا هم بکری خواهم داشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه جنیبت نه ستام و نه سلاح</p></div>
<div class="m2"><p>نز وشاقان نفری خواهم داشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کاه برگی تن و جو سنگی صبر</p></div>
<div class="m2"><p>کاه و جو این قدری خواهم داشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از فلک خیمه و از خاک بساط</p></div>
<div class="m2"><p>وز سرشک آب خوری خواهم داشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون ز تبریز رسم سوی هرات</p></div>
<div class="m2"><p>هم به ری رهگذری خواهم داشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عقرب از طالع تبریز و ری است</p></div>
<div class="m2"><p>نه ز عقرب ضرری خواهم داشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من چو برجیس ز حوت آمده‌ام</p></div>
<div class="m2"><p>سرطان مستقری خواهم داشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر چه دریاست عراق از سفرش</p></div>
<div class="m2"><p>نه امید گهری خواهم داشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تشنه لب بر لب دریا چو صدف</p></div>
<div class="m2"><p>سرو تن پی سپری خواهم داشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صدفش چشم ندارم لیکن</p></div>
<div class="m2"><p>از نهنگش حذری خواهم داشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عزلتی دارم و امن اینت نعیم</p></div>
<div class="m2"><p>زین دو نعمت بطری خواهم داشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هیچ درها سوی درها نبرم</p></div>
<div class="m2"><p>که نه زین به درری خواهم داشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گرچه آتش سرم و باد کلاه</p></div>
<div class="m2"><p>نه پی تاجوری خواهم داشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه در هیچ سری خواهم کوفت</p></div>
<div class="m2"><p>نه سر هیچ دری خواهم داشت</p></div></div>