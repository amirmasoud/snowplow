---
title: >-
    شمارهٔ ۱۶۰ - مطلع دوم
---
# شمارهٔ ۱۶۰ - مطلع دوم

<div class="b" id="bn1"><div class="m1"><p>ای لب و خالت بهم طوطی و هندوستان</p></div>
<div class="m2"><p>پیش جمالت منم هندوی جان بر میان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رخ و زلف تو رست در دل من آبنوس</p></div>
<div class="m2"><p>وز لب و خال تو گشت دیدهٔ من آبدان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابرش خورشید را ناخنه آمد ز رشک</p></div>
<div class="m2"><p>تا تو به شب رنگ حسن تاخته‌ای در جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رو که ز عکس لبت خوشهٔ پروین شده است</p></div>
<div class="m2"><p>خوشهٔ خرمای تر بر طبق آسمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبر من از بی‌دلی است از تو که مجروح را</p></div>
<div class="m2"><p>چاره ز بی‌مرهمی است سوختن پرنیان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با همه کآزاد نیست یک سر مویم ز تو</p></div>
<div class="m2"><p>نیست تو را از وفا بر سر موئی نشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه ز افغان مرا با تو زبان موی شد</p></div>
<div class="m2"><p>در همه عالم منم موی شکاف از زبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طبع چو خاقانیی بستهٔ سودا مدار</p></div>
<div class="m2"><p>بشکن صفراتی او ز آن لب چون ناردان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عهد کهن تازه کن کو سخنان تازه کرد</p></div>
<div class="m2"><p>خاصه ثنای ملک کرد ضمیرش ضمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناصر ملت طراز، قاهر بدعت گداز</p></div>
<div class="m2"><p>شاه خلیفه پناه خسرو سلطان نشان</p></div></div>