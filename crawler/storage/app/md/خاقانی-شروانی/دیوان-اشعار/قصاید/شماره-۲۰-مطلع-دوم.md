---
title: >-
    شمارهٔ ۲۰ - مطلع دوم
---
# شمارهٔ ۲۰ - مطلع دوم

<div class="b" id="bn1"><div class="m1"><p>شاهد سرمست من صبح درآمد ز خواب</p></div>
<div class="m2"><p>کرد صراحی طلب، دید صبوحی صواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در برم آمد چو چنگ گیسو در پاکشان</p></div>
<div class="m2"><p>من شده از دست صبح دست بسر چون رباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد لبش از نمک بوی بنفشه به صبح</p></div>
<div class="m2"><p>بر نمکش ساختم مردم دیده کباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی چو صبحش مرا از الم دل رهاند</p></div>
<div class="m2"><p>عیسی و آنگه الم جنت و آنگه عذاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبح دم آب حیات خوردم از آن چاه سیم</p></div>
<div class="m2"><p>عقل بر آن چاه و آب صرف کنان جاه و آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف من گرگ مست باده به کف صبح فام</p></div>
<div class="m2"><p>وز دو لب باده رنگ سرکه فشان از عتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یافت درستی که من توبه نخواهم شکست</p></div>
<div class="m2"><p>کرد چو صبح نخست روی نهان در نقاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت چرا در صبوح باده نخواهی کنونک</p></div>
<div class="m2"><p>حجله برانداخت صبح حجره بپرداخت خواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتمش ای صبح دل سکهٔ کارم مبر</p></div>
<div class="m2"><p>زر و سر اینک ز من سکه رخ برمتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من نکنم کار آب کو ببرد آب کار</p></div>
<div class="m2"><p>صبح خرد چون دمید آب شود کار آب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من به تو ای زود سیر تشنهٔ دیرینه‌ام</p></div>
<div class="m2"><p>دشنه مکش هم چو صبح تشنه مکش چون سراب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نقب زدم در لبت روی تو رسوام کرد</p></div>
<div class="m2"><p>کفت نقاب هست صب حدم و ماه تاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرغ تو خاقانی است داعی صبح وصال</p></div>
<div class="m2"><p>منطق مرغ شناس شاه سلیمان رکاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاه مجسطی گشای، خسرو هیت شناس</p></div>
<div class="m2"><p>رهرو صبح یقین رهبر علم الکتاب</p></div></div>