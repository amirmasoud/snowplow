---
title: >-
    شمارهٔ ۱۵۲ - در ستایش مظفر الدین قزل ارسلان ایلدگز
---
# شمارهٔ ۱۵۲ - در ستایش مظفر الدین قزل ارسلان ایلدگز

<div class="b" id="bn1"><div class="m1"><p>هر صبح که نو جهان ببینم</p></div>
<div class="m2"><p>از منزل جان نشان ببینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح آینه‌ای شود که در وی</p></div>
<div class="m2"><p>نقش دل آسمان ببینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پویم پی کاروان وسواس</p></div>
<div class="m2"><p>غم بدرقه هم عنان ببینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر بار نفس که برگشایم</p></div>
<div class="m2"><p>غم تعبیه در میان ببینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صحرای دلم هزار فرسنگ</p></div>
<div class="m2"><p>آتش‌گه کاروان ببینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیزم که کمین‌گه فلک را</p></div>
<div class="m2"><p>یک شیردل از نهان ببینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جویم که رصدگه زمان را</p></div>
<div class="m2"><p>تنها روی آن زمان ببینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کهف نیاز شیرمردان</p></div>
<div class="m2"><p>جان را سگ آستان ببینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون سر به سر دو زانو آرم</p></div>
<div class="m2"><p>قرب دو سر کمان ببینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس بی‌نمک است عیش، وقت است</p></div>
<div class="m2"><p>کز دیده نمک فشان ببینم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشگفت که چون نمک بر آتش</p></div>
<div class="m2"><p>لب را مدد از فغان ببینم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از جفتی غم به باد غصه</p></div>
<div class="m2"><p>دل حاملهٔ گران ببینم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خون گریم و از دو هندوی چشم</p></div>
<div class="m2"><p>رومی بچگان روان ببینم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر هر مژه در چو اشک داود</p></div>
<div class="m2"><p>برکرده به ریسمان ببینم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می‌جویم داد و نیست ممکن</p></div>
<div class="m2"><p>کاین نادره در جهان ببینم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صورت نکنم که صورت داد</p></div>
<div class="m2"><p>در گوهر انس و جان ببینم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در صد غم تازه‌تر گریزم</p></div>
<div class="m2"><p>گر یک غم جان ستان ببینم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو تب‌خالی که تب نشاند</p></div>
<div class="m2"><p>دل را غم غم نشان ببینم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ترسم که به چشم ابلق عمر</p></div>
<div class="m2"><p>از ناخنه استخوان ببینم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عمر است بهار نخل بندان</p></div>
<div class="m2"><p>کش هر نفسی خزان ببینم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتی بروم به وهم نونو</p></div>
<div class="m2"><p>سوز جگر فلان ببینم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو سوز مرا گران نبینی</p></div>
<div class="m2"><p>من وهم تو را گران ببینم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عمری به کران کنم که اهلی</p></div>
<div class="m2"><p>زین کوچهٔ باستان ببینم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر غوره چهار مه کنم صبر</p></div>
<div class="m2"><p>تا باده به خم‌ستان ببینم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دل نشکنم از عتاب باری</p></div>
<div class="m2"><p>چون بالش پرنیان ببینم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر آینه چشم از آن گمارم</p></div>
<div class="m2"><p>کز هم‌جنسی نشان ببینم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سازم دل مرده را حنوطی</p></div>
<div class="m2"><p>کز آینه زعفران ببینم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر شب که به صفه‌های افلاک</p></div>
<div class="m2"><p>صف‌ها زده میهمان ببینم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جوشم ز حسد که از ثریا</p></div>
<div class="m2"><p>شش همدم مهربان ببینم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من خود نکنم طمع که شش یار</p></div>
<div class="m2"><p>در شش سوی هفت‌خوان ببینم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هم ظن نبرم که کعبتین را</p></div>
<div class="m2"><p>شش نقش به سالیان ببینم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اندیک دو دست فرقدان وار</p></div>
<div class="m2"><p>در یک در آشیان ببینم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پس گویم دیده گیر کآخر</p></div>
<div class="m2"><p>هم فرقت فرقدان ببینم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر مه که به یک وطن مه و خور</p></div>
<div class="m2"><p>با همچو دو عیش ران ببینم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حالی به وداع از اشک هر دو</p></div>
<div class="m2"><p>لون شفق ارغوان ببینم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خور در تب و صرع دار یابم</p></div>
<div class="m2"><p>مه در دق و ناتوان ببینم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از قحط کرم کجا گریزم</p></div>
<div class="m2"><p>کانجا دل میزبان ببینم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جانی چو مزاج مشتری پاک</p></div>
<div class="m2"><p>ز آلایش سوزیان ببینم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>طبعی چو بنات نعش ز آمال</p></div>
<div class="m2"><p>دوشیزهٔ جاودان ببینم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دیری است که این فلک نگون است</p></div>
<div class="m2"><p>زودش چو زمین ستان ببینم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گویم که فلک علوفه‌گاهی است</p></div>
<div class="m2"><p>کورا ره کهکشان ببینم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مه ز آن به اسد رسد به هر ماه</p></div>
<div class="m2"><p>تا در دم شیر نان ببینم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گو چرخ مکن ضمان روزی</p></div>
<div class="m2"><p>همت بدل ضمان ببینم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از شیر شتر خوشی نجویم</p></div>
<div class="m2"><p>چون ترشی ترکمان ببینم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>روزی چه طلب کنم به خواری</p></div>
<div class="m2"><p>خود بی‌طلب و هوان ببینم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر موم که پاسبان درج است</p></div>
<div class="m2"><p>نگذاشت که لعل کان ببینم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون بر سر تاج شاه شد لعل</p></div>
<div class="m2"><p>بی‌منت پاسبان ببینم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نی‌نی به گمان نیکم از بخت</p></div>
<div class="m2"><p>کارم همه چون گمان ببینم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بختی که سیاه داشت در زین</p></div>
<div class="m2"><p>خنگیش به زیر ران ببینم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دل رفت گر اهل دل بیابم</p></div>
<div class="m2"><p>زین مرهم زخم آن ببینم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خسته نشوم ز خار نااهل</p></div>
<div class="m2"><p>ز آن خار گل جنان ببینم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بهرام نیم که طیره گردم</p></div>
<div class="m2"><p>چون مقنع و دوکدان ببینم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>این تازه سخن که کردم ابداع</p></div>
<div class="m2"><p>در روی زمین روان ببینم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دیوان مرا که گنج عرشی است</p></div>
<div class="m2"><p>عین الله گنج‌بان ببینم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>طرارانی که دزد گنج‌اند</p></div>
<div class="m2"><p>هم دست بریده‌شان ببینم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>طرار بریده سر چو طیار</p></div>
<div class="m2"><p>آویخته بی‌زبان ببینم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>امید به طالع است کز عمر</p></div>
<div class="m2"><p>هیلاج بقا چنان ببینم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کاندر سنهٔ ثون اختر سعد</p></div>
<div class="m2"><p>در طالع کامران ببینم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شش سال دگر قران انجم</p></div>
<div class="m2"><p>در آذر و مهرگان ببینم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هر هفت رسد به برج میزان</p></div>
<div class="m2"><p>با بیست و یکش قران ببینم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نشگفت که چون نمک بر آتش</p></div>
<div class="m2"><p>لب را مدد از فغان ببینم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کیوان به کناره بینم ار چه</p></div>
<div class="m2"><p>هر هفت به یک مکان ببینم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر خط شمال خسف گیرد</p></div>
<div class="m2"><p>زی مکه روم امان ببینم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>در حد حجاز امن یابم</p></div>
<div class="m2"><p>گر سوی خزر زیان ببینم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در شانهٔ گوسپند گردون</p></div>
<div class="m2"><p>من حکم به از شبان ببینم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تا ظن نبری که هیچ نکبت</p></div>
<div class="m2"><p>زین حکم دروغ‌سان ببینم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ره سوی یقین ندارد این حکم</p></div>
<div class="m2"><p>هر چند ره بیان ببینم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>حقا که دروغ داستانی است</p></div>
<div class="m2"><p>بطلانی داستان ببینم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خاقانی را زبان حالت</p></div>
<div class="m2"><p>از نا بده ترجمان ببینم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>از خسف چه باک چون پناهم</p></div>
<div class="m2"><p>درگاه خدایگان ببینم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دیدار سپاه دار ایران</p></div>
<div class="m2"><p>در آینهٔ روان ببینم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بر هفت فلک فراخته سر</p></div>
<div class="m2"><p>تاج قزل ارسلان ببینم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>با کوکبهٔ مظفر الدین</p></div>
<div class="m2"><p>دین همره و همرهان ببینم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>امر ملک الملوک مغرب</p></div>
<div class="m2"><p>هم رتبت کن فکان ببینم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>جم ملکت و جم خصال و جم خوست</p></div>
<div class="m2"><p>جم را ملک الزمان ببینم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>کیخسرو دین که در سپاهش</p></div>
<div class="m2"><p>صد رستم پهلوان ببینم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>پرویز هدی که در بلادش</p></div>
<div class="m2"><p>صد نعمان مرزبان ببینم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>تاج سر خاندان سلجوق</p></div>
<div class="m2"><p>بر تخت زر کیان ببینم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بر شاه کیان گهر فشانم</p></div>
<div class="m2"><p>کورا گهر و کیان ببینم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>خورشید اسد سوار یابم</p></div>
<div class="m2"><p>بهرام زحل سنان ببینم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>از رایتش آفتاب نصرت</p></div>
<div class="m2"><p>در مشرق دودمان ببینم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>در بارگه دوم سلیمان</p></div>
<div class="m2"><p>سیمرغ کرم عیان ببینم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چون خوان سخا نهد سلیمان</p></div>
<div class="m2"><p>عیسیش طفیل خوان ببینم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>گر سنگ پذیرد آب جودش</p></div>
<div class="m2"><p>ز آتش زنه ضیمران ببینم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>دستارچهٔ سیاه نیزه‌اش</p></div>
<div class="m2"><p>چتر سر خضرخان ببینم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>شیب سر تازیانه‌ش از قدر</p></div>
<div class="m2"><p>حبل الله شه طغان ببینم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>در یک سر ناخن از دو دستش</p></div>
<div class="m2"><p>صد شیر نر ژیان ببینم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>او شاه سه وقت و چار ملت</p></div>
<div class="m2"><p>بر شاه مدیح خوان ببینم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>دهر از فزعش به پنج هنگام</p></div>
<div class="m2"><p>در ششدر امتحان ببینم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>از هفت سپهر و هشت خلدش</p></div>
<div class="m2"><p>روز آخور و شب ستان ببینم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نه چرخ ز قلزم کف شاه</p></div>
<div class="m2"><p>مستسقی ده بنان ببینم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>روئین تن عالم است و قصدش</p></div>
<div class="m2"><p>هر هفته به هفت‌خوان ببینم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ماند به هلال شاه مغرب</p></div>
<div class="m2"><p>کافزونش فروتر آن ببینم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>نشگفت کز آن هلال دولت</p></div>
<div class="m2"><p>عید دل خاندان ببینم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>آری شه مغرب آن هلال است</p></div>
<div class="m2"><p>کاندر حد قیروان ببینم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بر خاک درش ز بوس شاهان</p></div>
<div class="m2"><p>نقش رخ آبدان ببینم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>گر بر سر چرخ شد حسودش</p></div>
<div class="m2"><p>هم در بن خاکدان ببینم</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>کرکس که به مکر شد سوی چرخ</p></div>
<div class="m2"><p>بر خاک چو ماکیان ببینم</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>گر خصمش امیر مصر گردد</p></div>
<div class="m2"><p>کورا عدن و عمان ببینم</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>پندار سر خر و بن خار</p></div>
<div class="m2"><p>در عرصهٔ بوستان ببینم</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>انگار خروس پیرزن را</p></div>
<div class="m2"><p>بر پایهٔ نردبان ببینم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ای تاجور اردشیر اسلام</p></div>
<div class="m2"><p>کاجری خورت اردوان ببینم</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ای سایهٔ حق که عقل کل را</p></div>
<div class="m2"><p>ز اخلاق تو دایگان ببینم</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>گردد فلک المحیط گویت</p></div>
<div class="m2"><p>گر دست تو صولجان ببینم</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>زیبد فلک البروج کوست</p></div>
<div class="m2"><p>کز نوبه زدن نوان ببینم</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>کیوانت شها، به عرض پرچم</p></div>
<div class="m2"><p>بر رمح چو خیزران ببینم</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>از پرز پلاس آخور تو</p></div>
<div class="m2"><p>برجیس به طیلسان ببینم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>شمشیر هدی توئی که مریخ</p></div>
<div class="m2"><p>شمشیر تو را فسان ببینم</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>خورشید ز برق نعل رخشت</p></div>
<div class="m2"><p>ناری است که بی‌دخان ببینم</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ناهید سزد هزاردستان</p></div>
<div class="m2"><p>کایوان تو گلستان ببینم</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ز اوصاف تو تیر هندسی را</p></div>
<div class="m2"><p>باد رطب اللسان ببینم</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>هارون تو ماه وز ثریاش</p></div>
<div class="m2"><p>شش زنگله در میان ببینم</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>امر تو و ابلق شب و روز</p></div>
<div class="m2"><p>یک فحل و دو مادیان ببینم</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>محمود کفی که سیستانت</p></div>
<div class="m2"><p>محکوم چو سیسجان ببینم</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>فتح تو به سومنات یابم</p></div>
<div class="m2"><p>غزو تو به مولتان ببینم</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چتر سیه و سپید پیلت</p></div>
<div class="m2"><p>مالش ده سیستان ببینم</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چون قصد کنی فتوح قنوج</p></div>
<div class="m2"><p>ملت ز تو شادمان ببینم</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>گرد سپهت به نهرواله</p></div>
<div class="m2"><p>سهم تو به نهروان ببینم</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>تو خسرو خاور و ز امرت</p></div>
<div class="m2"><p>تعظیم به خاوران ببینم</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>تو دامغ روم و از حسامت</p></div>
<div class="m2"><p>زلزال به دامغان ببینم</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>دریا هبتی و کوه هیبت</p></div>
<div class="m2"><p>کز ذات تو این و آن ببینم</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>از رای تو صیقل فلک را</p></div>
<div class="m2"><p>هفت آینه در دکان ببینم</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>گر هیچ سپه کشی سوی شام</p></div>
<div class="m2"><p>آنجا سقر و جنان ببینم</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>از خلق تو خار و حنظل شام</p></div>
<div class="m2"><p>گل شکر اصفهان ببینم</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>صور و عکه در امان امرت</p></div>
<div class="m2"><p>چون ارمن و نخجوان ببینم</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>سگبانت شه فرنگ یابم</p></div>
<div class="m2"><p>دربان شه عسقلان ببینم</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>تو قاهر مصر و چاوشت را</p></div>
<div class="m2"><p>بر قاهره قهرمان ببینم</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>روزی که در ابرسان یمینت</p></div>
<div class="m2"><p>برق گهر یمان ببینم</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>شیر فلک از نهیب گرزت</p></div>
<div class="m2"><p>چون گاو زمین جبان ببینم</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>از ماه درفش تو مه چرخ</p></div>
<div class="m2"><p>سوزان چو ز مه کتان ببینم</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>طوفان شود آشکار کز خون</p></div>
<div class="m2"><p>شمشیر تو سیل ران ببینم</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>خنگ تو روان چو کشتی نوح</p></div>
<div class="m2"><p>اندر طوفان روان ببینم</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>چون فال برآرمت ز مصحف</p></div>
<div class="m2"><p>نصر الله در قرآن ببینم</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>در شان تو بینم آیت فتح</p></div>
<div class="m2"><p>کاسباب نزول و شان ببینم</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>ای عرش سریر آسمان صدر</p></div>
<div class="m2"><p>گر بزم تو خلد جان ببینم</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>در کعبهٔ خلد صدر بزمت</p></div>
<div class="m2"><p>کوثر، نم ناودان ببینم</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>بر خاک در تو آب حیوان</p></div>
<div class="m2"><p>چون آتش رایگان ببینم</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>در خواب جلالت تو دیدم</p></div>
<div class="m2"><p>در بیداری همان ببینم</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>زین شهر دو رنگ نشکنم دل</p></div>
<div class="m2"><p>کورا دل ایرمان ببینم</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>زین هفت رصد نیفکنم بار</p></div>
<div class="m2"><p>کانصاف تو دیده‌بان ببینم</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>این هفت رصد بیفکنم باز</p></div>
<div class="m2"><p>تا منزل کاروان ببینم</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>از جور دو مار بر نجوشم</p></div>
<div class="m2"><p>چون رایت کاویان ببینم</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>فر تو خبر دهد که چندان</p></div>
<div class="m2"><p>تایید ظفر رسان ببینم</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>کز عمر هزار ساله چون نوح</p></div>
<div class="m2"><p>صد دولت دیرمان ببینم</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>برگ همه دوستان بسازم</p></div>
<div class="m2"><p>مرگ همه دشمنان ببینم</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>بر خاک درت زکات دربان</p></div>
<div class="m2"><p>گنج زر شایگان ببینم</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>این فال ز سعد مستعار است</p></div>
<div class="m2"><p>هستیش ز مستعان ببینم</p></div></div>