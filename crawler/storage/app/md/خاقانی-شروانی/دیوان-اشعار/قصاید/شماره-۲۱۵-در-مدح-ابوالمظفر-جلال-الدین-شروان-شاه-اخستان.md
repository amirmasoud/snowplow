---
title: >-
    شمارهٔ ۲۱۵ - در مدح ابوالمظفر جلال الدین شروان‌شاه اخستان
---
# شمارهٔ ۲۱۵ - در مدح ابوالمظفر جلال الدین شروان‌شاه اخستان

<div class="b" id="bn1"><div class="m1"><p>پیش که صبح بر درد شقهٔ چتر عنبری</p></div>
<div class="m2"><p>خیز مگر به برق می برقع صبح بر دری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش که غمزه زن شود چشم ستارهٔ سحر</p></div>
<div class="m2"><p>بر صدف فلک رسان خندهٔ جام گوهری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برکش میخ غم ز دل پیش که صبح برکشد</p></div>
<div class="m2"><p>این خشن هزار میخ از سر چرخ چنبری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساخت فرو کند ز اسب، آینه بندد آسمان</p></div>
<div class="m2"><p>صبح قبا زره زند، ابر کند زره‌گری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زآنکه برهنگی بود زیور تیغ صبح فش</p></div>
<div class="m2"><p>صبح برهنه می‌کند بر تن چرخ زیوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه چو حال عاشقان صبح کند ملونی</p></div>
<div class="m2"><p>گه چو حلی دلبران مرغ کند نواگری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون به صبوح بلبله قهقهه کرد و خنده‌نی</p></div>
<div class="m2"><p>خنده کند نه قهقهه، صبح چو نوگل طری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز به روزت از فلک نزل دو صبح می‌رسد</p></div>
<div class="m2"><p>صبح سه گردد ار به کف جام صبوحی آوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوبر صبح یک دم است، اینت شگرف اگر دهی</p></div>
<div class="m2"><p>داد دمی که می‌دهد صبح‌دمت به نوبری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرض صبوح عید را کز تو به خواب فوت شد</p></div>
<div class="m2"><p>صدره اگر قضا کنی تا ز صبوح نشمری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست ز نامده خبر وز دم رفته حاصلی</p></div>
<div class="m2"><p>حاصل وقت را نگر تا دم رفته ننگری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عمر پلی است رخنه‌سر، حادثه سیل پل شکن</p></div>
<div class="m2"><p>کوش که نارسیده سیل، از پل رخنه بگذری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه غم جهان خورد، کی ز حیات برخورد</p></div>
<div class="m2"><p>پس تو غم جهان مخور، تا ز حیات برخوری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آهوکا! سگ توام می خور و گرگ مست شو</p></div>
<div class="m2"><p>خواب پلنگ نه ز سر گرچه پلنگ گوهری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برگ می صبوح کن، سرکه فروختن که چه</p></div>
<div class="m2"><p>گرچه ز خواب جسته‌ای خوش ترش و گران سری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواب تو می‌نشاندم بر سر آتش هوس</p></div>
<div class="m2"><p>کان همه مشک بر سرت وین همه مغز را تری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شو به گلاب اشک من خواب جهان ز عبهرت</p></div>
<div class="m2"><p>تا به دو لاله درکشی جام گلاب عبهری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم به گلاب لعل بر، درد سرم که از فلک</p></div>
<div class="m2"><p>با همه درد دل مرا درد سری است بر سری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برق تویی و بید من، سوختهٔ توام کنون</p></div>
<div class="m2"><p>سوخته بید خواه اگر رواق عید پروری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رقص کنان نگر خره لعل غبب چو روی تو</p></div>
<div class="m2"><p>طوق کشان سرودمش چون خطت از معنبری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر غبب و دم خزه خیز و رکاب باده ده</p></div>
<div class="m2"><p>چون دمش از مطوقی چون غببش ز احمری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>منتظری که از فلک خوانچهٔ زر برآیدت</p></div>
<div class="m2"><p>خوانچه کن و چمانه‌کش خوانچهٔ زر چه می‌بری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جز جگری نخورده‌ای بر سر خوانچهٔ زر برآیدت</p></div>
<div class="m2"><p>عمر تو می‌خورد تو هم در غم خوانچهٔ زری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کردهٔ چرخ جو به جو دیده و آزموده‌ای</p></div>
<div class="m2"><p>کرده به جور جو جوت هم به جوال او دری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در ده از آن چکیده خون ز آبلهٔ تن رزان</p></div>
<div class="m2"><p>کبلهٔ رخ فلک، برد عروس خاوری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از پس زر اختران کامده بر محک شب</p></div>
<div class="m2"><p>رفت سیاهی از محک، ماند سپید پیکری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تیره شد آب اختران ز آتش روز و می‌کند</p></div>
<div class="m2"><p>بر درجات خط جام آب چو آتش اختری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چرخ کبود جامه بین ریخته اشک‌ها ز رخ</p></div>
<div class="m2"><p>تا تو ز جرعه بر زمین جامهٔ عید گستری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن می و جام بین بهم گوئی دست شعبده</p></div>
<div class="m2"><p>کرده ز سیم ده دهی صرهٔ زر شش سری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در کف ساقی از قدح حقهٔ لعل آتشی</p></div>
<div class="m2"><p>در گلوی قدح ز کف رشتهٔ عقد عنبری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ساقی بزم چون پری جام به کف چو آینه</p></div>
<div class="m2"><p>او نرمد ز جام اگر ز آینه می‌رمد پری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در کف آهوان بزم آب رز است و گاو زر</p></div>
<div class="m2"><p>آتش موسوی است آن در بر گاو سامری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از قطرات جرعه‌ها ژالهٔ زرد ریخته</p></div>
<div class="m2"><p>یافته چون رخ فلک پشت زمین مجدری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دختر آفتاب ده در تتق سپهر گون</p></div>
<div class="m2"><p>گشته به زهرهٔ فلک حامله هم به دختری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کرده به جلوه کردنش باد مسیح مریمی</p></div>
<div class="m2"><p>کرده به نقش بستنش نار خلیل آزری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مطرب سحرپیشه بین در صور هر آلتی</p></div>
<div class="m2"><p>آتش و آب و باد و گل کرده بهم ز ساحری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بربط اعجمی صفت هشت زبانش در دهان</p></div>
<div class="m2"><p>از سر زخمه ترجمان کرده به تازی و دری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نای عروسی از حبش ده ختنش به پیش و پس</p></div>
<div class="m2"><p>تاج نهاده بر سرش از نی قند عسکری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنگ برهنه فرق را پای پلاس پوش بین</p></div>
<div class="m2"><p>خشک رگی کشیده خون ناله کنان ز لاغری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دست رباب و سر یکی بسته به ده رسن گلو</p></div>
<div class="m2"><p>زیر خزینهٔ شکم کاسهٔ سر ز مضطری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنبر دف شکارگه ز آهو و گور و یوز و سگ</p></div>
<div class="m2"><p>لیک به هیچ وقت ازو هیچ شکار نشکری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>روز رسید و محرمان عید کنند زین سبب</p></div>
<div class="m2"><p>روز چو محرمان زند لاف سپید چادری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در عرفات بختیان بادیه کرده پی‌سپر</p></div>
<div class="m2"><p>ما و تو بسپریم هم بادیهٔ قلندری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در عرفات عاشقان بختی بی‌خبر توئی</p></div>
<div class="m2"><p>کز همه بارکش‌تری وز همه بی‌خبرتری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دی به نماز دیگری موقف اگر تمام شد</p></div>
<div class="m2"><p>چون تو صبوح کرده‌ای مرد نماز دیگری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ور سوی مشعر الحرام آمده‌اند محرمان</p></div>
<div class="m2"><p>محرم می شویم ما میکده کرده مشعری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ور به منی خورد زمین خون حلال جانوران</p></div>
<div class="m2"><p>ما بخوریم خون رز تا نرسد به جانوری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هر که کبوتری کشد هم به ثواب در رسد</p></div>
<div class="m2"><p>خیز و ببر گلوی دل، کو کندت کبوتری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سنگ فشان کنند خلق از پی دین به جمره در</p></div>
<div class="m2"><p>ما همه جان فشان کنیم از پی خم به می خوری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ور به طواف کعبه‌اند از سر پای سر زنان</p></div>
<div class="m2"><p>ما و تو و طواق دیر از سر دل، نه سرسری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ور همه سنگ کعبه را بوسه زنند حاجیان</p></div>
<div class="m2"><p>ما همه بوسه گه کنیم آن سر زلف سعتری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کوی مغان و ما و تو هر سر سنگ کعبه‌ای</p></div>
<div class="m2"><p>پای تو کرده زمزمی، دست تو کرده ساغری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>طاعت ماست با گنه کز پی نام درخورد</p></div>
<div class="m2"><p>روی سپید جامه را داغ سیاه گازری</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کعبه به زاهدان رسد، دیر به ما سبو کشان</p></div>
<div class="m2"><p>بخشش اصل دان همه، ما و تو از میان بری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زهد شما و فسق ما چون همه حکم داور است</p></div>
<div class="m2"><p>داورتان خدای بس، اینهمه چیست داوری</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر حج و عمره کرده‌اند از در کعبه رهروان</p></div>
<div class="m2"><p>ما حج و عمره می‌کنیم از در خسرو سری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خاطر خاقانی از آن کعبه شناس شد که او</p></div>
<div class="m2"><p>در حرم خدایگان کرده به جان مجاوری</p></div></div>