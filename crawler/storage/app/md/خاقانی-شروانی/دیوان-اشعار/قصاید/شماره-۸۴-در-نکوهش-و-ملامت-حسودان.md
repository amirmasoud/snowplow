---
title: >-
    شمارهٔ ۸۴ - در نکوهش و ملامت حسودان
---
# شمارهٔ ۸۴ - در نکوهش و ملامت حسودان

<div class="b" id="bn1"><div class="m1"><p>مشتی خسیس ریزه که اهل سخن نیند</p></div>
<div class="m2"><p>با من قران کنند وقرینان من نیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ماه نخشبند مزور از آن چو من</p></div>
<div class="m2"><p>انجم فروز گنبد هر انجمن نیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هول صور فکرت من در قیامتند</p></div>
<div class="m2"><p>گر چه چو اهل صور فکنده کفن نیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پروردگان مائدهٔ خاطر منند</p></div>
<div class="m2"><p>گر خود به جمله جز پسر ذوالیزن نیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بل نایبان یاوگیان ولایتند</p></div>
<div class="m2"><p>زیرا که شه طغان جهان سخن نیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاوی کنند و چون صدف آبستنند لیک</p></div>
<div class="m2"><p>از طبع گوهر آور و عنبر فکن نیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون طشت بی‌سرند و چو در جنبش آمدند</p></div>
<div class="m2"><p>الا شناعتی و دریده دهن نیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه فریب دمنهٔ افسون گرند لیک</p></div>
<div class="m2"><p>روز هنر غضنفر لشکر شکن نیند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ارقم از درون همه زهرند و از برون</p></div>
<div class="m2"><p>جز کآبش رنگ رنگ و شگال شکن نیند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اوباش آفرینش و حشو طبیعتند</p></div>
<div class="m2"><p>کالا به دست حرص و حسد مرتهن نیند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندر چه اثیر اسیرند تا ابد</p></div>
<div class="m2"><p>زان جز شکسته پای و گسسته رسن نیند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گویند در خلافه ولیعهد آدمیم</p></div>
<div class="m2"><p>مشنو خلافشان که جز ابلیس فن نیند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گویند عیسی دگریم از طریق نطق</p></div>
<div class="m2"><p>برکن بروتشان که به جز گور کن نیند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خود را همای دولت خوانند و غافلند</p></div>
<div class="m2"><p>کالا غراب ریمن و جغد دمن نیند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر قله‌های کوه ریاضت کشیده‌اند</p></div>
<div class="m2"><p>ارباب تهمتند ولی برهمن نیند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از روی مخرقه همه دعوی دین کنند</p></div>
<div class="m2"><p>وز کوی زندقه به جز اهل فتن نیند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون شمع صبح‌گاهی و چون مرغ بی‌گهی</p></div>
<div class="m2"><p>الا سزاید کشتن و گردن زدن نیند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من میوه دار حکمتم از نفس ناطقه</p></div>
<div class="m2"><p>و ایشان ز روح نامیه جز نارون نیند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جمعند بر تفرق عالم ولی ز ضعف</p></div>
<div class="m2"><p>موران با پرند و سپاه پرن نیند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تازند رخش بدعت و سازند تیر کید</p></div>
<div class="m2"><p>اما سفندیار مرا تهمتن نیند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرعونیان بی‌فر و عونند لاجرم</p></div>
<div class="m2"><p>اصحاب بینش ید بیضای من نیند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خود عذرشان نهم که جعل پیشه‌اند پاک</p></div>
<div class="m2"><p>ز آن طالبان مشک و نسیم سمن نیند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آری به آب نایژه خو کرده‌اند از آنک</p></div>
<div class="m2"><p>مستسقیان لجهٔ بحر عدن نیند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بل تا مرض کشند ز خوان‌های بد گوار</p></div>
<div class="m2"><p>کارزانیان لذت سلوی و من نیند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بینا دلان ز گفتهٔ من در بشاشت‌اند</p></div>
<div class="m2"><p>کوری آن گروه که جز در حزن نیند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جائی است ضیمران ضمیر مرا چمن</p></div>
<div class="m2"><p>کارواح قدس جز طرف آن چمن نیند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نساج نسبتم که صناعات فکر من</p></div>
<div class="m2"><p>الا ز تار و پود خرد جامه تن نیند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نجار گوهرم که نجیبان طبع من</p></div>
<div class="m2"><p>جز زیر تیشهٔ پدر خویشتن نیند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وین جاهلان ملمع کارند و منتحل</p></div>
<div class="m2"><p>ز آن گاه امتحان به جز از ممتحن نیند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از نوک خامه دفتر دلشان سیه کنم</p></div>
<div class="m2"><p>کایشان زنخ زنند، همه خامه زن نیند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنجا که من فقاع گشایم ز جیب فضل</p></div>
<div class="m2"><p>الا ز درد دل چو یخ افسرده تن نیند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>معصوم کی شوند ز طوفان لفظ من</p></div>
<div class="m2"><p>کز نوح عصمت الا فرزند و زن نیند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در کون هم طویلهٔ خاقانیند لیک</p></div>
<div class="m2"><p>از نقش و نطرتند ز نفس و فطن نیند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حقا به جان شاه که هم شاه آگه است</p></div>
<div class="m2"><p>کایشان سزای حضرت شاه ز من نیند</p></div></div>