---
title: >-
    شمارهٔ ۲۷ - در فقر و گوشه نشینی و گله از سفر
---
# شمارهٔ ۲۷ - در فقر و گوشه نشینی و گله از سفر

<div class="b" id="bn1"><div class="m1"><p>قلم بخت من شکسته سر است</p></div>
<div class="m2"><p>موی در سر ز طالع هنر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت نیک، آرزو رسان دل است</p></div>
<div class="m2"><p>که قلم نقش بند هر صور است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش امید چون تواند بست</p></div>
<div class="m2"><p>قلمی کز دلم شکسته‌تر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده دارد سپید بخت سیاه</p></div>
<div class="m2"><p>این سپید آفت سیاه سر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخت را در گلیم بایستی</p></div>
<div class="m2"><p>این سپیدی برص که در بصر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم زاغ است بر سیاهی بال</p></div>
<div class="m2"><p>گر سپیدی به چشم زاغ در است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوه را زر چه سود بر کمرش</p></div>
<div class="m2"><p>که شهان را زر از در کمر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن چو ناخن شد استخوانم از آنک</p></div>
<div class="m2"><p>بخت را ناخته به چشم در است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>استخوان پیش‌کش کنم غم را</p></div>
<div class="m2"><p>زآنکه غم میهمان سگ جگر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز دانش زوال یافت که بخت</p></div>
<div class="m2"><p>به من راست فعل کژ نگر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس به پیشین ندیده‌ای خورشید</p></div>
<div class="m2"><p>که چو کژ سر نمود کژ نظر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون نفس می‌زنم کژم نگرد</p></div>
<div class="m2"><p>چرخ کژ سیر کاهرمن سیر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون صفیرش زنی کژت نگرد</p></div>
<div class="m2"><p>اسب کورا نظر بر آب‌خور است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا مگر راست می‌کند کژ من</p></div>
<div class="m2"><p>که مرا از کژی هنوز اثر است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترک آن کژ نگه کند در تیر</p></div>
<div class="m2"><p>تا شود راست کالت ظفر است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه روز اعور است چرخ ولیک</p></div>
<div class="m2"><p>احول است آن زمان که کینه‌ور است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر که را روی راست، بخت کژ است</p></div>
<div class="m2"><p>مار کژ بین که بر رخ سپر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بس نبالد گیابنی که کژ است</p></div>
<div class="m2"><p>بس نپرد کبوتری که تر است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دهر صیاد و روز و شب دو سگ است</p></div>
<div class="m2"><p>چرخ باز کبود تیز پر است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه عالم شکارگه بینی</p></div>
<div class="m2"><p>کاین دو سگ زیر و باز بر زبر است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عقل سگ جان هوا گرفت چو باز</p></div>
<div class="m2"><p>کاین سگ و باز چون شکارگر است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من چو کبک آب زهره ریخته رنگ</p></div>
<div class="m2"><p>صید باز و سگی که بوی بر است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیک بد حال و سخت سست دلم</p></div>
<div class="m2"><p>حال و دل هر دو یک نه بر خطر است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عافیت آرزو کنم هیهات</p></div>
<div class="m2"><p>این تمناست یافتن دگر است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آرزو را ذخیره امید است</p></div>
<div class="m2"><p>وصل امید عمر جانور است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آرزو چون نشاند شاخ طمع</p></div>
<div class="m2"><p>طلبش بیخ و یافت برگ و بر است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>طمع آسان ولی طلب صعب است</p></div>
<div class="m2"><p>صعبی یافت از طلب بتر است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آرزویی که از جهان خواهم</p></div>
<div class="m2"><p>بدهد زآنکه مست و بی‌خبر است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لیکن آن داده را به هشیاری</p></div>
<div class="m2"><p>واستاند که نیک بد گهر است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در دبستان روزگار، مرا</p></div>
<div class="m2"><p>روز و شب لوح آرزو به بر است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هیچ طفلی در این دبستان نیست</p></div>
<div class="m2"><p>که ورا سورهٔ وفا ز بر است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون برد آیت وفا از یاد؟</p></div>
<div class="m2"><p>کآخر اوفوا بعهدی از سور است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خاطرم بکر و دهر نامرد است</p></div>
<div class="m2"><p>نزد نامرد، بکر کم‌خطر است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نالش بکر خاطرم ز قضاست</p></div>
<div class="m2"><p>گلهٔ شهربانو از عمر است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سایهٔ من خبر ندارد از آنک</p></div>
<div class="m2"><p>آه من چرخ‌سوز و کوه در است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جوش دریا در دیده زهرهٔ کوه</p></div>
<div class="m2"><p>گوش ماهی بنشنود که کر است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مر ما مر من حساب العمر</p></div>
<div class="m2"><p>چون به پنجه رسد حساب مر است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ناودان مژه ز بام دماغ</p></div>
<div class="m2"><p>قطره ریز است و آرزو خضر است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سبب آبروی آب مژه است</p></div>
<div class="m2"><p>صیقل تیغ کوه تیغ خور است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نکنم زر طلب که طالب زر</p></div>
<div class="m2"><p>همچو زر نثار پی‌سپر است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عاقبت هرکه سر فراخت به زر</p></div>
<div class="m2"><p>همچو سکه نگون و زخم خور است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>روی عقل از هوای زر همه را</p></div>
<div class="m2"><p>آبله خورده همچو روی زر است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از شمار نفس فذلک عمر</p></div>
<div class="m2"><p>هم غم است ار چه غم نفس شمراست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>غم هم از عالم است و در عالم</p></div>
<div class="m2"><p>می‌نگنجد که بس قوی حشر است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عالم از جور مایهٔ زای غم است</p></div>
<div class="m2"><p>بتر از هیمه مایه شرر است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چون شرر شد قوی همه عالم</p></div>
<div class="m2"><p>طعمه سازد چه حاجت تبر است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لهو، یک جزو و غم هزار ورق</p></div>
<div class="m2"><p>غصه مجموع و قصه مختصر است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>قابل گل منم که گل همه تن</p></div>
<div class="m2"><p>رنگ خون است و خار نیشتر است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>غم ز دل زاد و خورد خون دلم</p></div>
<div class="m2"><p>خون مادر غذا ده پسر است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آتشی کز دل شجر زاید</p></div>
<div class="m2"><p>طعمهٔ او هم از تن شجر است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چرخ بازیچه گون چون بازیچه</p></div>
<div class="m2"><p>در کف هفت طفل جان شکر است</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بدو خیط ملون شب و روز</p></div>
<div class="m2"><p>در گشایش بسان باد فر است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شب که ترکان چرخ کوچ کنند</p></div>
<div class="m2"><p>کاروان حیات بر حذر است</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خیل ترکان کنند بر سر کوچ</p></div>
<div class="m2"><p>غارت کاروان که بر گذر است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خواجه چون دید دردمند دلم</p></div>
<div class="m2"><p>گفت کین دردناکی از سفر است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هان کجائی چه می‌خوری؟ گفتم</p></div>
<div class="m2"><p>می‌خورم خون خود که ما حضر است</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چه خورش کو خورش کدام خورش</p></div>
<div class="m2"><p>دست خون مانده را چه جای خور است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گوید آخر چه آرزو داری</p></div>
<div class="m2"><p>آرزو زهر و غم نه کام و گر است</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نیم جنسی و یک‌دلی خواهم</p></div>
<div class="m2"><p>آرزوم از جهان همین قدر است</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از دو یک دم که در جهان یابم</p></div>
<div class="m2"><p>ناگزیر است و از جهان گذر است</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نگذرد دیگ پایه را ز حجر</p></div>
<div class="m2"><p>نگذرد آتشی که در حجر است</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به مقامی رسیده‌ام که مرا</p></div>
<div class="m2"><p>خار و حنظل بجای گل شکر است</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کو سر تیغ کرزوی من است</p></div>
<div class="m2"><p>کانس وحشی به سبزه و شمر است</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بر سر تیغ به سری که سر است</p></div>
<div class="m2"><p>خرج قصاب به بزی که نر است</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ابله از چشم زخم کم رنج است</p></div>
<div class="m2"><p>اکمه از درد چشم کم ضرر است</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>جاهل آسوده، فاضل اندر رنج</p></div>
<div class="m2"><p>فضل مجهول و جهل معتبر است</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سفله مستغنی و سخی محتاج</p></div>
<div class="m2"><p>این تغابن ز بخشش قدر است</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همه جور زمانه بر فضلاست</p></div>
<div class="m2"><p>بوالفضول از حفاش زاستر است</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>سوس را با پلاس کینی نیست</p></div>
<div class="m2"><p>کین او با پرند شوشتر است</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>حال مقلوب شد که بر تن دهر</p></div>
<div class="m2"><p>ابره کرباس و دیبه آستر است</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>عالم از علم مشتق است و لیک</p></div>
<div class="m2"><p>جهل عالم به عالمی سمر است</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>معنی از اشتقاق دور افتاد</p></div>
<div class="m2"><p>کز صلف کبر و از اصف کبر است</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>قوت مرغ جان به بال دل است</p></div>
<div class="m2"><p>قیمت شاخ کز به زال زر است</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دل پاکان شکستهٔ فلک است</p></div>
<div class="m2"><p>زال دستان فکندهٔ پدر است</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>جان دانا عجب بزرگ دل است</p></div>
<div class="m2"><p>تن ادریس بس بلند پر است</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>در گلستان عمر و رستهٔ عهد</p></div>
<div class="m2"><p>پس گل، خار و بعد نفع، ضر است</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از پس هر مبارکی شومی است</p></div>
<div class="m2"><p>وز پی هر محرمی صفر است</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>فقر کن نصب عین و پیش خسان</p></div>
<div class="m2"><p>رفع قصه مکن نه وقت جر است</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>دهر اگر خوان زندگانی ساخت</p></div>
<div class="m2"><p>خورد هر چاشنی که کام و گر است</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سال کو خرمن جوانی دید</p></div>
<div class="m2"><p>سوخت هر خوشه‌ای که زیب و فر است</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>درزیی صدرهٔ مسیح برید</p></div>
<div class="m2"><p>علمش برد و گفت گوش خر است</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>کشت امید چون نرویاند</p></div>
<div class="m2"><p>گریه کو فتح باب هر نظر است</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>وقت تب چون به نی نبرد تب</p></div>
<div class="m2"><p>شیر گر نیستانش مستقر است</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دفع عین الکمال چون نکند</p></div>
<div class="m2"><p>رنگ نیلی که بر رخ قمر است</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>دی همی گفتم آه کز ره چشم</p></div>
<div class="m2"><p>دل من نیم کشتهٔ عبر است</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>مرگ یاران شنیدم از ره گوش</p></div>
<div class="m2"><p>دلم امروز کشتهٔ فکر است</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>هر که از راه گوش کشته شود</p></div>
<div class="m2"><p>زاندرون پوست خون او هدر است</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>آری آری هم از ره گوش است</p></div>
<div class="m2"><p>کشتن قندزی که در خزر است</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نقطهٔ خون شد از سفر دل من</p></div>
<div class="m2"><p>خود سفر هم به نقطه‌ای سقر است</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>تا به غربت فتاده‌ام همه سال</p></div>
<div class="m2"><p>نه مهم غیبت و سه مه حضر است</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نی نی از بخت شکرها دارم</p></div>
<div class="m2"><p>چند شکری که شوک بی‌ثمر است</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>صورت بخت من طویل‌الذیل</p></div>
<div class="m2"><p>در وفا چون قصیر با قصر است</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بخت ملاح کشتی طرب است</p></div>
<div class="m2"><p>بخت فلاح کشته بطر است</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چشم بد دور بر در بختم</p></div>
<div class="m2"><p>چرخ حلقه به گوش همچو در است</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بخت، مرغ نشیمن امل است</p></div>
<div class="m2"><p>روز، طفل مشیمهٔ سحر است</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>هم ز بخت است کز مقالت من</p></div>
<div class="m2"><p>همه عالم غرائب و غرر است</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>استراحت به بخت یا نعم است</p></div>
<div class="m2"><p>استطابت به آب یا مدر است</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>فخر من یاد کرد شروان به</p></div>
<div class="m2"><p>که مباهات خور به باختر است</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>لیک تبریز به اقامت را</p></div>
<div class="m2"><p>که صدف قطره را بهین مقر است</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>هم به مولد قرار نتوان کرد</p></div>
<div class="m2"><p>که صدف حبس خانهٔ درر است</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>گرچه تبریز شهره‌تر شهری است</p></div>
<div class="m2"><p>لیک شروان شریفتر ثغر است</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>خاک شروان مگو که وان شر است</p></div>
<div class="m2"><p>کان شرفوان به خیر مشتهر است</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>هم شرفوان نویسمش لیکن</p></div>
<div class="m2"><p>حرف علت از آن میان بدر است</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>عیب شروان مکن که خاقانی</p></div>
<div class="m2"><p>هست از آن شهر کابتداش شر است</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>عیب شهری چرا کنی به دو حرف</p></div>
<div class="m2"><p>کاول شرع و آخر بشر است</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>جرم خورشید را چه جرم بدانک</p></div>
<div class="m2"><p>شرق و غرب ابتدا شراست و غر است</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>گر چه ز اول غر است حرف غریب</p></div>
<div class="m2"><p>مرد نامی غریب بحر و بر است</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چه کنی نقص مشک کاشغری</p></div>
<div class="m2"><p>که غر آخر حروف کاشغر است</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>گرچه هست اول بدخشان بد</p></div>
<div class="m2"><p>به نتیجه نکوترین گهر است</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>نه تب اول حروف تبریز است</p></div>
<div class="m2"><p>لیک صحت رسان هر نفر است</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>دیدی آن جانور که زاید مشک</p></div>
<div class="m2"><p>نامش آهو و او همه هنر است</p></div></div>