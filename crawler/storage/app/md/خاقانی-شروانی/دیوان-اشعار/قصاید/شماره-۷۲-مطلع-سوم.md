---
title: >-
    شمارهٔ ۷۲ - مطلع سوم
---
# شمارهٔ ۷۲ - مطلع سوم

<div class="b" id="bn1"><div class="m1"><p>طره مفشان کز هلالت عید جان برساختند</p></div>
<div class="m2"><p>طیره منشین کز جمالت عید لشکر ساختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه نو دیدی لبت بین، رشتهٔ جانم نگر</p></div>
<div class="m2"><p>کاین سه را از بس که باریکند همبر ساختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش بالایت به بالایت فرو ریزم گهر</p></div>
<div class="m2"><p>زانکه صد نوبر مرا زان یک صنوبر ساختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون کمر حلقه به گوشم، چشم پیش از شرم آنک</p></div>
<div class="m2"><p>چون کمر گاه تو بازم کیسه لاغر ساختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آن لب چون آتش تر هدیه کن یک بوس خشک</p></div>
<div class="m2"><p>گر چه بر آتش تو را مهری ز عنبر ساختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من نی خشکم و گر چه طعمهٔ آتش نی است</p></div>
<div class="m2"><p>طعمهٔ این خشک نی ز آن آتش تر ساختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرگذشت حال خاقانی به دفتر ساز از آنک</p></div>
<div class="m2"><p>نو به نو غمهاش تو بر تو چو دفتر ساختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوخته عود است و دلبندان بدو دندان سپید</p></div>
<div class="m2"><p>شوق شاهش آتش و شروانش مجمر ساختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نصرة الاسلام گیتی پهلوان کاجرام چرخ</p></div>
<div class="m2"><p>چارپای تختش از تاج دو پیکر ساختند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ظل حق فرزند شمس الدین اتابک کز جلال</p></div>
<div class="m2"><p>بر سر عرش از ظلال قدرش افسر ساختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هشت حرف است از قزل تا ارسلان چون بنگری</p></div>
<div class="m2"><p>هفت گردون را در آن هر هشت مضمر ساختند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رستم توران ستان است این خلف کز فر او</p></div>
<div class="m2"><p>الدگز را ملک کیخسرو میسر ساختند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مملکت بخشی که نقش هشت حرف نام اوست</p></div>
<div class="m2"><p>بیضهٔ مهری که بر کتف پیمبر ساختند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عکس یک جامش دو گیتی می‌نماید کز صفاش</p></div>
<div class="m2"><p>آب خضر و آینهٔ جان سکندر ساختند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هست اتابک چون فریدون نیست باک ار کافران</p></div>
<div class="m2"><p>خویشتن ضحاک شور و اژدها سر ساختند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آب گز گاو سارش باد کو را عرشیان</p></div>
<div class="m2"><p>آتش ضحاک سوز و اژدها خور ساختند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هست اتابک مصطفی تایید و اسکندر خصال</p></div>
<div class="m2"><p>کاین دو را هم در یتیمی ملک پرور ساختند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور یکیشان در قبائل قابل فرمان نشد</p></div>
<div class="m2"><p>آخرش چوندعنصر اول مبتر ساختند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مصطفی در شصت و سه، اسکندر اندر سی و دو</p></div>
<div class="m2"><p>دشمنان را مسخ کردند و مسخر ساختند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هست اتابک آسمانی کاین خلف خورشید اوست</p></div>
<div class="m2"><p>آسمان را افسر از خورشید انور ساختند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هست اتابک بهمن آسا کاین خلف دارای اوست</p></div>
<div class="m2"><p>لاجرم در ملتش دارا و داور ساختند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیش یاجوجی که ظلمت خانهٔ الحاد راست</p></div>
<div class="m2"><p>دست و تیغ این سکندر سد اکبر ساختند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خستگان دیو ظلم از خاک درگاهش به لب</p></div>
<div class="m2"><p>نشره کردند و به آب رخ مزعفر ساختند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیش سقف بارگاهش خانهٔ موری است چرخ</p></div>
<div class="m2"><p>کز شبستان سلیمانیش منظر ساختند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کعبهٔ ملک است صحن بارگاهش کز شرف</p></div>
<div class="m2"><p>باغ رضوان را کبوتر خانه ایدر ساختند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بلکه تا این کعبه رضوان را کبوتر خانه شد</p></div>
<div class="m2"><p>چون کبوتر کعبه را گردش مچاور ساختند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زو مظالم توز و ظالم سوزتر شاهی نبود</p></div>
<div class="m2"><p>تا تظلم گاه این میدان اغبر ساختند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کشتی سلجوقیان بر جودی عدل ایستاد</p></div>
<div class="m2"><p>تا صواعق بار طوفانش ز خنجر ساختند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کافرم گر پیش از او یا پیش از او اسلام را</p></div>
<div class="m2"><p>زین نمط کو ساخت تمهید موفر ساختند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از پس عهد کیومرث کیان تا دور شاه</p></div>
<div class="m2"><p>کارداران فلک آئین منکر ساختند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گه به ناپاکی ز باد انجیر بید انگیختند</p></div>
<div class="m2"><p>گه به خودرائی ز بید انجیر عرعر ساختند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شیر خواران را به مغز و شیر مردان را به جان</p></div>
<div class="m2"><p>طعمهٔ مار و شکار گرگ حمیر ساختند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پس به آخر این نکو کردند کاندر صد قران</p></div>
<div class="m2"><p>این یکی صاحب قران را شاه و سرور ساختند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پایگاه تازیانش ساختند ایوان روم</p></div>
<div class="m2"><p>بلکه خوک پایگاهش جان قیصر ساختند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حاسدان در زخم خوردن سرنگون چون سکه‌اند</p></div>
<div class="m2"><p>تا به نامش سکهٔ ایران مشهر ساختند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وز پی تعظیم سکه‌ش را ز روهینای هند</p></div>
<div class="m2"><p>شاه جن را جنیان دیهیم و افسر ساختند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر سلاطین پرچم شب‌رنگ با پر خدنگ</p></div>
<div class="m2"><p>از پر مرغ و دم شیر دلاور ساختند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>میر ما را از پر روح الامین و زلف حور</p></div>
<div class="m2"><p>پر تیر و پرچم رخش مضمر ساختند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آن نگویم کز دم شیر فلک وز آفتاب</p></div>
<div class="m2"><p>پرچم و طاسش برای خنگ و اشقر ساختند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سهو شد بر عقل کاول رستم ثانیش خواند</p></div>
<div class="m2"><p>گر چه از اقلیم رومش هفت خوان بر ساختند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کز پی میر آخوری در پایگاه رخش او</p></div>
<div class="m2"><p>آخشیجان جان رستم را مکرر ساختند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ساحت این هفت کشور برنتابد لشکرش</p></div>
<div class="m2"><p>شاید ار خضرای نه چرخش معسکر ساختند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پار دیدی کاین سر سلجوقیان بر اهل کفر</p></div>
<div class="m2"><p>چون شبیخون ساخت کایشان غول رهبر ساختند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چون دو لشکر بر هم افتادند چون گیسوی حور</p></div>
<div class="m2"><p>هفت گیسودار چرخ از گرد معجر ساختند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نوک پیکان‌ها چو درهم خانهٔ عیسی رسید</p></div>
<div class="m2"><p>چرخ ترسا جامه را دجال اعور ساختند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در میان اب و آتش کاین سلاح است آن سمند</p></div>
<div class="m2"><p>شیر مردان چون سلحفات و سمندر ساختند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شه خلیل اعجاز و هیجا آتش و گرد خلیل</p></div>
<div class="m2"><p>از بهار و گل نگارستان آزر ساختند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مرکبان شاه را چون جوزهر بر بسته دم</p></div>
<div class="m2"><p>گفتی از هر جوزهر جوزای ازهر ساختند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چون همای فتح پور الدگز بگشاد بال</p></div>
<div class="m2"><p>کرکسان چرخ از آن خون خوارگان خور ساختند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از دل و رخسارشان خوردند چندان کرکسان</p></div>
<div class="m2"><p>کز شبه منقار و از زرنیخ ژاغر ساختند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر چنان فتحی که این شاه ملایک پیشه کرد</p></div>
<div class="m2"><p>هم ملایک شاهدالحالند و محضر ساختند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دشمنانش همره غولند اگر خود بهر حرز</p></div>
<div class="m2"><p>هشت حرفش هفت هیکل‌وار دربر ساختند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بخت گم کردند چون یاری ز کافر خواستند</p></div>
<div class="m2"><p>روی کژ دیدند چون آئینه مغفر ساختند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تخت نرد ملک را ز آن سو که بد خواهان اوست</p></div>
<div class="m2"><p>هفت نراد فلک خانه مششدر ساختند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نو عروس از ره نشینان شکر چون گوید از آنک</p></div>
<div class="m2"><p>دام عنین از سقنقور مزور ساختند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ای که مردان عجم پیشت چو طفلان عرب</p></div>
<div class="m2"><p>طوق در حلقند و نامت تاج مفخر ساختند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ناخنی از معن و جعفر کم نکردی فضل از آنک</p></div>
<div class="m2"><p>فضلهٔ هر ناخنت را معن و جعفر ساختند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تا درت بینم به دیگر جای نفرستم ثنا</p></div>
<div class="m2"><p>کز درت دعوتگه روح مطهر ساختند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کودکی را سوی بستان خواند عم کودک چه گفت؟</p></div>
<div class="m2"><p>گفت: رو بستان ما پستان مادر ساختند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شعر من فالی است نامش سعد اکبر گیر از آنک</p></div>
<div class="m2"><p>راوی من در ثنات از سعد اصغر ساختند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چون کف و خلقت به تازی هست خارا و نسیج</p></div>
<div class="m2"><p>خانهٔ من حله و بغداد و ششتر ساختند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همت و لطف تو را در خوانده، اینجا بخششم</p></div>
<div class="m2"><p>زر و زربفت و غلام و طوق و استر ساختند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>عدل ورزا خسروا پیوند عمرت باد عدل</p></div>
<div class="m2"><p>کز جهان عدل است و بس کاو را معمر ساختند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>عید باقی ساز کز ساعات روز عمر تو</p></div>
<div class="m2"><p>ساعتی را هفته‌ای از روز محشر ساختند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ملک و عقل و شرع زیر خاتم و کلک تو باد</p></div>
<div class="m2"><p>کاین سه را ز اقبال این دو بخت یاور ساختند</p></div></div>