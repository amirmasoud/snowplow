---
title: >-
    شمارهٔ ۱۸۵ - در عزلت و حکمت و موعظه و ریاضت و انتباه و ارشاد
---
# شمارهٔ ۱۸۵ - در عزلت و حکمت و موعظه و ریاضت و انتباه و ارشاد

<div class="b" id="bn1"><div class="m1"><p>ما را دلی است زله خور خوان صبح‌گاه</p></div>
<div class="m2"><p>جانی است خاک جرعهٔ مستان صبح‌گاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان شد نهنگ بحرکش از جام نیم شب</p></div>
<div class="m2"><p>دل گشت مور ریزه خور از خوان صبح‌گاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غربال بیختیم به عمری که یافتیم</p></div>
<div class="m2"><p>زر عیاردار به میزان صبح‌گاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس نقد گم ببودهٔ مردان که یافتند</p></div>
<div class="m2"><p>رندان خاک بیز به میدان صبح‌گاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دولت دوید و هفت در آسمان گشاد</p></div>
<div class="m2"><p>چون بر زدیم حلقه به سندان صبح‌گاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین یک نفس درآمد و بیرون شد حیات</p></div>
<div class="m2"><p>بردیم روزنامه به دیوان صبح‌گاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اول شب ایتکین وثاق آمدیم بلیک</p></div>
<div class="m2"><p>الب ارسلان شدیم به پایان صبح‌گاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌آرزوی ملک به زیر گلیم فقر</p></div>
<div class="m2"><p>کوبیم کوس بر در ایوان صبح‌گاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غوغا کنیم یک تنه چون رستم و دریم</p></div>
<div class="m2"><p>درع فراسیاب به پیکان صبح‌گاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نقب افکنیم نیم شب از دور تا بریم</p></div>
<div class="m2"><p>پی بر سر خزینهٔ پنهان صبح‌گاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌ترس تیغ و دار بگوئیم تا که‌ایم</p></div>
<div class="m2"><p>نقب افکن خزینهٔ ترکان صبح‌گاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صور روان خفته دلانیم چون خروس</p></div>
<div class="m2"><p>آهنگ دان پردهٔ دستان صبح‌گاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چندین هزار جرعه که این سبز طشتراست</p></div>
<div class="m2"><p>نوشیم چون شویم به مهمان صبح‌گاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو آب روی درنکشیم ارچه درکشیم</p></div>
<div class="m2"><p>بحری ز دست ساقی دوران صبح‌گاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتی شما چگونه و چون است نزلتان</p></div>
<div class="m2"><p>ماشا و نزل ما ز شبستان صبح‌گاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آتش زنیم هفت علف‌خانهٔ فلک</p></div>
<div class="m2"><p>چون بنگریم نزل فراوان صبح‌گاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خواهی که نزل ما دهدت ده کیای دهر</p></div>
<div class="m2"><p>بستان گشاد نامه به عنوان صبح‌گاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو کی شناسی این چه معماست چون هنوز</p></div>
<div class="m2"><p>ابجد نخوانده‌ای به دبستان صبح‌گاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیاع خان جان مجاهز دلان عشق</p></div>
<div class="m2"><p>جز صبح نیست جان تو و جان صبح‌گاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفتی شما که‌اید و چه مرغید و چیستید</p></div>
<div class="m2"><p>سیمرغ نیم‌روز و سلیمان صبح‌گاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ما مرغ عرشییم که بر بانگ ما روند</p></div>
<div class="m2"><p>مرغان شب شناس نواخوان صبح‌گاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صبح شما دمی است، دم ما هزار صبح</p></div>
<div class="m2"><p>هر پنج وقت ما شده یکسان صبح‌گاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ما را به هر دو صبح دو عید است و جان ما</p></div>
<div class="m2"><p>مرغی است فربه از پی قربان صبح‌گاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تسکین جان گرم دلان را کنیم سرد</p></div>
<div class="m2"><p>چون دم برآوریم به دامان صبح‌گاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سحرا که بر قوارهٔ سیمین مه کنیم</p></div>
<div class="m2"><p>چون برکشیم سر ز گریبان صبح‌گاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بهر بخور مجلس روحانیان عشق</p></div>
<div class="m2"><p>سازیم سینه مجمر سوزان صبح‌گاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر چشم ما گلاب فشان شد عجب مدار</p></div>
<div class="m2"><p>دل‌های ماست آینه‌گردان صبح‌گاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خاقانیا مرنج که سلطان گدات خواند</p></div>
<div class="m2"><p>آری گدای روزی و سلطان صبح‌گاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون ژاله و صبا و شباهنگ هم‌چنین</p></div>
<div class="m2"><p>معزول روز باش و عمل‌ران صبح‌گاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جیحون فشان ز اشک و سمرقند گیر از آه</p></div>
<div class="m2"><p>تا ما نهیم نام تو خاقان صبح‌گاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از دم سیاه کن رخ دیو سپید روز</p></div>
<div class="m2"><p>چون دیو نفس توست سلیمان صبح‌گاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>میلی بساز ز آه وبزن بر پلاس شب</p></div>
<div class="m2"><p>درکش به چشم روز به فرمان صبح‌گاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از خوان دل به نزل سرای ازل درآی</p></div>
<div class="m2"><p>بفرست زله‌ای سوی اخوان صبح‌گاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یک گوش ماهیی بده از می که حاضرند</p></div>
<div class="m2"><p>دریاکشان ره زده عطشان صبح‌گاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ریزی بریز از آن می ریحانی سرشک</p></div>
<div class="m2"><p>وز بوی جرعه کن دم ریحان صبح‌گاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون ماهی ار بریده زبانی دلت بجاست</p></div>
<div class="m2"><p>دل در تو یونسی است زبان دان صبح‌گاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر شاه نیم‌روز کمین کن که آه توست</p></div>
<div class="m2"><p>هر نیم شب کمان‌کش مردان صبح‌گاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر صبح فتح باب کن از انجم سرشک</p></div>
<div class="m2"><p>بنشان غبار غصه به باران صبح‌گاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون بر بطت زبان چه بکار است بهتر آنک</p></div>
<div class="m2"><p>چون نای بی‌زبان زنی الحان صبح‌گاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گم کن زبان که مار نگهبان گنج توست</p></div>
<div class="m2"><p>بر گنج خود تو باش نگهبان صبح‌گاه</p></div></div>