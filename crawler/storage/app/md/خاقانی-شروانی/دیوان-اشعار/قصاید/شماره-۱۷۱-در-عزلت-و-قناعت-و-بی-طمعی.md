---
title: >-
    شمارهٔ ۱۷۱ - در عزلت و قناعت و بی‌طمعی
---
# شمارهٔ ۱۷۱ - در عزلت و قناعت و بی‌طمعی

<div class="b" id="bn1"><div class="m1"><p>زین بیش آبروی نریزم برای نان</p></div>
<div class="m2"><p>آتش دهم به روح طبیعی به جای نان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون جگر خورم نخورم نان ناکسان</p></div>
<div class="m2"><p>در خون جان شوم نشوم آشنای نان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با این پلنگ همتی از سگ بتر بوم</p></div>
<div class="m2"><p>گر زین سپس چو سگ دوم اندر قفای نان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جرم ماه و قرصهٔ خورشید ننگرم</p></div>
<div class="m2"><p>هرگه که دیدها شودم رهنمای نان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چشم زیبق آرم و در گوش ریزمش</p></div>
<div class="m2"><p>تا نشنوم ز سفرهٔ دونان صلای نان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم به ترک نان سپید سیه دلان</p></div>
<div class="m2"><p>هل تا فنای جان بودم در فنای نان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نانشان چو برف لیک سخنشان چو ز مهریر</p></div>
<div class="m2"><p>من زادهٔ خلیفه نباشم گدای نان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن را دهند گرده که او گرد گو دوید</p></div>
<div class="m2"><p>من کیمیای جان ندهم در بهای نان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون آب آسیا سر من در نشیب باد</p></div>
<div class="m2"><p>گر پیش کس دهان شودم آسیای نان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از قوت در نمانم گو نان مباش از آنک</p></div>
<div class="m2"><p>قوتی است معدهٔ حکما را ورای نان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون آهوان گیا چرم از صحن‌های دشت</p></div>
<div class="m2"><p>اندیک نگذرم به در ده‌کیای نان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا چند نان و نان که زبانم بریده باد</p></div>
<div class="m2"><p>کب امید برد امید عطای نان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آدم برای گندمی از روضه دور ماند</p></div>
<div class="m2"><p>من دور ماندم از در همت برای نان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آدم ز جنت آمد و من در سقر شدم</p></div>
<div class="m2"><p>او در بلای گندم و من در بلای نان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یارب ز حال آدم ورنج من آگهی</p></div>
<div class="m2"><p>خود کن عتاب گندم و خود ده جزای نان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا کی ز دست ناکس و کس زخمها زنند</p></div>
<div class="m2"><p>بر گردهای ناموران گردهای نان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نانم نداد چرخ ندانم چه موجب است</p></div>
<div class="m2"><p>ای چرخ ناسزا نبدم من سزای نان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر آسمان فرشتهٔ روزی به بخت من</p></div>
<div class="m2"><p>منسوخ کرد آیت رزق از ادای نان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خاقانیا هوان و هوا هم طویله‌اند</p></div>
<div class="m2"><p>تا نشکنند قدر تو، بشکن هوای نان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نانی که از کسان طلبی بر خدا نویس</p></div>
<div class="m2"><p>کاخر خدای جانت به از کدخدای نان</p></div></div>