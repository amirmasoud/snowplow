---
title: >-
    شمارهٔ ۹۱ - مطلع دوم
---
# شمارهٔ ۹۱ - مطلع دوم

<div class="b" id="bn1"><div class="m1"><p>شه اختران زان زر افشان نماید</p></div>
<div class="m2"><p>که اکسیر زرهای آبان نماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآرد ز جیب فلک دست موسی</p></div>
<div class="m2"><p>زر سامری نقد میزان نماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه خورشید هم خانهٔ عیسی آمد</p></div>
<div class="m2"><p>چه معنی که معلول و حیران نماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نارنج اگر طفل سازد ترازو</p></div>
<div class="m2"><p>نه نارنج و زر هر دو یکسان نماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک طفل خوئی است کاندر ترازو</p></div>
<div class="m2"><p>ز خورشید نارنج گیلان نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر خیمه سلطان انجم برون زد</p></div>
<div class="m2"><p>که ابر خزان چتر سلطان نماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوا پشت سنجاب بلغار گردد</p></div>
<div class="m2"><p>شمر سینهٔ باز خزران نماید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دمهای سنجاب نقاش آبان</p></div>
<div class="m2"><p>به زرنیخ تصویر بستان نماید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دامان شب پاره‌ای در فزاید</p></div>
<div class="m2"><p>از آن صدرهٔ روز نقصان نماید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قراسنقر آنگه که نصرت پذیرد</p></div>
<div class="m2"><p>بر آقسنقر آثار خذلان نماید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خزان از درختان چو صبح از کواکب</p></div>
<div class="m2"><p>نثار سر شاه کیهان نماید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شهنشاه اسلام خاقان اکبر</p></div>
<div class="m2"><p>که تاج سر آل سامان نماید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهدار اسلام منصور اتابک</p></div>
<div class="m2"><p>که کمتر غلامش قدرخان نماید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر آل بهرام کز بهر تیغش</p></div>
<div class="m2"><p>سر تیغ بهرام افسان نماید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سکندر جهادی و خضر اعتقادی</p></div>
<div class="m2"><p>که خاک درش آب حیوان نماید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهان دار شاه اخستان کز طبیعت</p></div>
<div class="m2"><p>کیومرث طهمورث امکان نماید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به تایید مهدی خصالی که تیغش</p></div>
<div class="m2"><p>روان سوز دجال طغیان نماید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فلک در بر او چو چوب در او</p></div>
<div class="m2"><p>سگی حلقه در گوش فرمان نماید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قبولش ز هاروت ناهید سازد</p></div>
<div class="m2"><p>کمالش ز بابل خراسان نماید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز باسش زمان دست انصاف بوسد</p></div>
<div class="m2"><p>ز جودش جهان مست احسان نماید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز یک نفخهٔ روح عدلش چو مریم</p></div>
<div class="m2"><p>عقیم خزان بکر نیسان نماید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عجوز جهان مادر یحیی آسا</p></div>
<div class="m2"><p>ازو حامل تازه زهدان نماید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به ناخن رسد خون دل بحر و کان را</p></div>
<div class="m2"><p>که هر ناخنش معن و نعمان نماید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز یک عکس شمشیرش این هفت رقعه</p></div>
<div class="m2"><p>تصاویر این هفت ایوان نماید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در ایوان شاهی در دولتش را</p></div>
<div class="m2"><p>فلک حلقه و ماه سندان نماید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مزور پزد خنجر گوشت خوارش</p></div>
<div class="m2"><p>عدو را که بیمار عصیان نماید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خیالی که بندد عدو را عجب نی</p></div>
<div class="m2"><p>که سرسام سوداش بحران نماید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر بوی خشمش برد مغز دریا</p></div>
<div class="m2"><p>تیمم گهی در بیابان نماید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وگر رنگ عفوش پذیرد بیابان</p></div>
<div class="m2"><p>چو دریاش نیلوفرستان نماید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وگر باد خلقش وزد بر جهنم</p></div>
<div class="m2"><p>زبانی مقامات رضوان نماید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز گل شکر لفظ و تفاح خلقش</p></div>
<div class="m2"><p>شماخی نظیر صفاهان نماید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در اقلیم ایران چو خیلش بجنبد</p></div>
<div class="m2"><p>هزاهز در اقلیم توران نماید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به تعلیم اقلیم گیری ملک را</p></div>
<div class="m2"><p>ملک شاه طفل دبستان نماید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تف تیغ هندیش هندوستان را</p></div>
<div class="m2"><p>علی الروس در روس و الان نماید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر خود فرشته شود بد سگالش</p></div>
<div class="m2"><p>هم از سگ نژادان شیطان نماید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو بر خنگ ختلی خرامد به میدان</p></div>
<div class="m2"><p>امیر آخورش شاه ختلان نماید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پلاس افکن آخور استرانش</p></div>
<div class="m2"><p>فنا خسرو و تخت ایران نماید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شبی کز شبیخون کشد تیغ چون خور</p></div>
<div class="m2"><p>چو ماه از کواکب سپه ران نماید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز شاه فلک تیغ و مه مرکب او</p></div>
<div class="m2"><p>زحل خود و مریخ خفتان نماید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شراری جهد ز آهن نعل اسبش</p></div>
<div class="m2"><p>که حراقش اروند و ثهلان نماید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز بس کاس سرها و خون جگرها</p></div>
<div class="m2"><p>اجل ساقی و وحش مهمان نماید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>لب و کام وحش از دل و روی خصمان</p></div>
<div class="m2"><p>همه رنگ زرنیخ و قطران نماید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو پیکانش از حصن ترکش برآید</p></div>
<div class="m2"><p>بر این حصن فیروزه غضبان نماید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اسد گاو دل، کرکسان کلک زهره</p></div>
<div class="m2"><p>از آن خرمگس رنگ پیکان نماید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تن قلعه‌ها پیش پولاد تیغش</p></div>
<div class="m2"><p>چو قلعی حل کرده لرزان نماید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر گرز سندان شکافش عجب نی</p></div>
<div class="m2"><p>که البرز تخم سپندان نماید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در اعجاز تیغ ملک بوالمظفر</p></div>
<div class="m2"><p>سپهر از سر عجز حیران نماید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو روئین تن اسفندیار است هر دم</p></div>
<div class="m2"><p>بر او فتح روئین دژ آسان نماید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از آنگه که بالغ شد اقبالش او را</p></div>
<div class="m2"><p>عروس ظفر در شبستان نماید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مرا بین که آیات ابیات مدحش</p></div>
<div class="m2"><p>نه تعویذ جان، حرز ایمان نماید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدیهه همی بارم از خاطر این در</p></div>
<div class="m2"><p>کز او گوش‌ها بحر عمان نماید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ازین شعر خجلت رسد عنصری را</p></div>
<div class="m2"><p>وگر عنصری جان حسان نماید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بخندم به نظم هر ابله اگر چه</p></div>
<div class="m2"><p>زبان ساحر و خامه ثعبان نماید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بلی نخل خرمای مریم بخندد</p></div>
<div class="m2"><p>بر آن نخل مومین که علان نماید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ملک منطق الطیر طیار داند</p></div>
<div class="m2"><p>ز ژاژ مطین که طیان نماید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بماناد شاه جهان کز جلاش</p></div>
<div class="m2"><p>سریر کیان تاج کیوان نماید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>برات بقا باد بر دست عمرش</p></div>
<div class="m2"><p>نه عمری که تا حشر پایان نماید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>قوی چار بینان ارکانش چندان</p></div>
<div class="m2"><p>که دور فلک هفت بنیان نماید</p></div></div>