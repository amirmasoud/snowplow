---
title: >-
    شمارهٔ ۸۵ - قصیده
---
# شمارهٔ ۸۵ - قصیده

<div class="b" id="bn1"><div class="m1"><p>آمد بهار و بخت که عشرت فزا شود</p></div>
<div class="m2"><p>از هر طرف هزار گل فتح وا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلشن شود نشیمن سلطان نوبهار</p></div>
<div class="m2"><p>چون بهر شاه تخت مرصع بنا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کان زر و جواهر بحر در و گهر</p></div>
<div class="m2"><p>شد جمع تا نشیمن بحر سخا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برگش زمرد است و گلش لعل آبدار</p></div>
<div class="m2"><p>گلزار تخت شه که بر آب بقا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توران سزد به پادشهی کز سر پری</p></div>
<div class="m2"><p>لعلی به صد هزار بدخشان بها شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد وقت کز نسیم قدوم بهار ملک</p></div>
<div class="m2"><p>در باغ تخت غنچهٔ یاقوت وا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عید قدم مبارک نوروز مژده داد</p></div>
<div class="m2"><p>کامسال تازه از پی هم فتح‌ها شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عید مبارک است کزان پای بخت شاه</p></div>
<div class="m2"><p>چون شاهدان ز خون عدو پرحنا شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاقانی عید آمد و خاقان به یمن خود</p></div>
<div class="m2"><p>هر کار کز خدای بخواهد روا شود</p></div></div>