---
title: >-
    شمارهٔ ۹۰ - در ستایش اتابک منصور فرمانروای شماخی و ابوالمظفر شروان شاه
---
# شمارهٔ ۹۰ - در ستایش اتابک منصور فرمانروای شماخی و ابوالمظفر شروان شاه

<div class="b" id="bn1"><div class="m1"><p>مرا صبح دم شاهد جان نماید</p></div>
<div class="m2"><p>دم عاشق و بوی پاکان نماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم سرد از آن دارد و خندهٔ خوش</p></div>
<div class="m2"><p>که آه من و لعل جانان نماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب یار من شد دم صبح مانا</p></div>
<div class="m2"><p>که سرد آتش عنبرافشان نماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر صبح بر اندکی عمر خندد</p></div>
<div class="m2"><p>که دارد دم سرد و خندان نماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخندد چو پسته درون پوست و آنگه</p></div>
<div class="m2"><p>چو بادام از آن پوست عریان نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقاب شکرفام بندد هوا را</p></div>
<div class="m2"><p>چو صبح از شکر خنده دندان نماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر پستهٔ سبز خندان ندیدی</p></div>
<div class="m2"><p>بسوی فلک بین که آن سان نماید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخ صبح، قندیل عیسی فروزد</p></div>
<div class="m2"><p>تن ابر زنجیر رهبان نماید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فلک را یهودانه بر کتف ازرق</p></div>
<div class="m2"><p>یکی پارهٔ زرد کتان نماید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فلک دایهٔ سالخورد است و در بر</p></div>
<div class="m2"><p>زمین را چو طفل ز من زان نماید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سراسیمه چون صرعیان است کز خود</p></div>
<div class="m2"><p>به پیرانه‌سر ام صبیان نماید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به شب گرچه پستان سیاه است بر تن</p></div>
<div class="m2"><p>هزاران نقط شیر پستان نماید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به صبح آن نقط‌ها فرو شوید از تن</p></div>
<div class="m2"><p>یتیم دریده گریبان نماید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به روز از پی این دو خاتون بینش</p></div>
<div class="m2"><p>یکی زال آیینه گردان نماید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به شام از رگ جان مردم بریدن</p></div>
<div class="m2"><p>ز خون شفق سرخ دامان نماید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو می‌خور صبوحی تو را از فلک چه</p></div>
<div class="m2"><p>که چون غول نیرنگ الوان نماید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو و دست دستان و مرغول مرغان</p></div>
<div class="m2"><p>گر آن غول صد دست دستان نماید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لگام فلک گیر تا زیر رانت</p></div>
<div class="m2"><p>کبود استری داغ بر ران نماید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر جرعه‌ای بر زمین ریزی از می</p></div>
<div class="m2"><p>زمین چون فلک مست دوران نماید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وگر بوئی از جرعه بخشی فلک را</p></div>
<div class="m2"><p>فلک چون زمین خفته ارکان نماید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درآر آفتابی که در برج ساغر</p></div>
<div class="m2"><p>سطرلاب او جان دهقان نماید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دواسبه درآی و رکابی درآور</p></div>
<div class="m2"><p>کز او چرمهٔ صبح یکران نماید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قدح قعده کن ساتکینی جنیبت</p></div>
<div class="m2"><p>کز این دو جهان تنگ میدان نماید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رکاب است چو حلقهٔ نیزه‌داران</p></div>
<div class="m2"><p>که عیدی به میدان خاقان نماید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ببین دست خاصان که چون رمح خاقان</p></div>
<div class="m2"><p>به حلقه ربائی چه جولان نماید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به شاه جهان بین که کیخسرو آسا</p></div>
<div class="m2"><p>ز یک عکس جامش دو کیهان نماید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بخواه از مغان در سفال آتش تر</p></div>
<div class="m2"><p>کز آتش سفال تو ریحان نماید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شفق خواهی و صبح می‌بین و ساغر</p></div>
<div class="m2"><p>اگر در شفق صبح پنهان نماید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز آهوی سیمین طلب گاو زرین</p></div>
<div class="m2"><p>که عیدی در او خون قربان نماید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صبوحی زناشوئی جام و می را</p></div>
<div class="m2"><p>صراحی خطیبی خوش‌الحان نماید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون آبستنان عدهٔ توبه بشکن</p></div>
<div class="m2"><p>درآر آنچه معیار مردان نماید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قدح‌های چو اشک داودی از می</p></div>
<div class="m2"><p>پری خانهای سلیمان نماید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کمرکن قدح را ز انگشت کو خود</p></div>
<div class="m2"><p>کمرها ز پیروزهٔ کان نماید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>می احمر از جام تا خط ازرق</p></div>
<div class="m2"><p>ز پیروزه لعل بدخشان نماید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو قوس قزح جام بینی ملمع</p></div>
<div class="m2"><p>کز او جرعه‌ها لعل باران نماید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همانا خروس است غماز مستان</p></div>
<div class="m2"><p>که تشنیع او راز ایشان نماید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ندانم خمار است یا چشم دردش</p></div>
<div class="m2"><p>که در چشم سرخی فراوان نماید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز بس کورد چشم دردش به افغان</p></div>
<div class="m2"><p>گلوی خراشیده ز افغان نماید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مگر روز قیفال او زد که از خون</p></div>
<div class="m2"><p>در آن طشت زر رنگ بر جان نماید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به جام صدف نوش بحری که عکسش</p></div>
<div class="m2"><p>ز تف ماهی چرخ بریان نماید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ببین بزم عیدی چو ایوان قیصر</p></div>
<div class="m2"><p>که چنگش سیه پوش مطران نماید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>صراحی نوآموز در سجده کردن</p></div>
<div class="m2"><p>یکی رومی نو مسلمان نماید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>قدح لب کبود است و خم در خوی تب</p></div>
<div class="m2"><p>چرا زخمه تب لرزه چندان نماید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو ده عاق فرزند لرزان که هر یک</p></div>
<div class="m2"><p>ز آزار پیری پشیمان نماید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رسن در گلو بر بط از چوب خوردن</p></div>
<div class="m2"><p>چو طفل رسن تاب کسلان نماید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>رباب از زبان‌ها بلا دیده چون من</p></div>
<div class="m2"><p>بلا بیند آنکو زبان دان نماید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سیه خانهٔ آبنوسین نائی</p></div>
<div class="m2"><p>به نه روزن و ده نگهبان نماید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مگر باد را بند سازد سلیمان</p></div>
<div class="m2"><p>که باد مسیحا به زندان نماید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خم چنبر دف چو صحرای جنت</p></div>
<div class="m2"><p>در او مرتع امن حیوان نماید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ببین زخمه کز پیش کیخسرو دین</p></div>
<div class="m2"><p>به کین سیاوش چه برهان نماید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به گردون در افتد صدا ارغنون را</p></div>
<div class="m2"><p>مگر کوس شاه جهانبان نماید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>جهان زیور عید بربندد از نو</p></div>
<div class="m2"><p>مگر مجلس شاه شروان نماید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>رود کعبه در جامهٔ سبز عیدی</p></div>
<div class="m2"><p>مگر بزم خاقان ایران نماید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو کعبه است بزمش که خاقانی آنجا</p></div>
<div class="m2"><p>سگ تازی پارسی خوان نماید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو راوی خاقانی آوا برآرد</p></div>
<div class="m2"><p>صریر در شاه ایران نماید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سر خسروان افسر آل سلجق</p></div>
<div class="m2"><p>که سائس تر از آل ساسان نماید</p></div></div>