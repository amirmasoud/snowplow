---
title: >-
    شمارهٔ ۱۵۸ - خاقانی درجواب ابوالفضایل احمد سیمگر گوید
---
# شمارهٔ ۱۵۸ - خاقانی درجواب ابوالفضایل احمد سیمگر گوید

<div class="b" id="bn1"><div class="m1"><p>الامان ای دل که وحشت زحمت آورد الامان</p></div>
<div class="m2"><p>بر کران شو زین مغیلانگاه غولان بر کران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگذر زین سردسیر ظلمت اینک روشنی</p></div>
<div class="m2"><p>درگذر زین خشک‌سال آفت اینک گلستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان یوسف زاد را کازاد کرد حضرت است</p></div>
<div class="m2"><p>وارهان زین چار میخ هفت زندان وارهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابلقی را کاسمان کمتر چراگاه وی است</p></div>
<div class="m2"><p>چند خواهی بست بر خشک آخور آخر زمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نگارستان نخوانی طارم ایام را</p></div>
<div class="m2"><p>کز برون‌سو زرنگار است از درون‌سو خاکدان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جای نزهت نیست گیتی را که اندر باغ او</p></div>
<div class="m2"><p>نیشکر چون برگ سنبل زهر دارد در میان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز و شب جان‌سوزی و آنگاه از ناپختگی</p></div>
<div class="m2"><p>روز چون نیلوفری چالاک و شب چون زعفران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا کی این روز و شب و چندین مغاک و تیرگی</p></div>
<div class="m2"><p>آن درخت آبنوس این صورت هندوستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نسیم انس بی‌بهره است سروستان دل</p></div>
<div class="m2"><p>وز ترنج عافیت خالی است نخلستان جان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اندر این خطه که دل خطبه به نام غم کند</p></div>
<div class="m2"><p>سکهٔ گیتی نخواهد داشت نقش جاودان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل منه بر عشوه‌های آسمان زیرا که هست</p></div>
<div class="m2"><p>بی‌سر و بن کارهای آسمان چون آسمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زود بینی چون بنات النعش گشتی سرنگون</p></div>
<div class="m2"><p>تا روی بر باد این پیروزه پیکر بادبان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با امل همراه وحدت چون شاه عزلت ران گشاد</p></div>
<div class="m2"><p>مرد چوبین اسب با بهرام چوبین همعنان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در ببند آمال را چون شاه عزلت ران گشاد</p></div>
<div class="m2"><p>جان بهای نهل را در پای اسب او فشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پر نیازی را که هم دل تفته بینی هم جگر</p></div>
<div class="m2"><p>شرب عزلت هم تباشیرش دهد هم  استخوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهد کن تا ریزه‌خوار خوان دل باشی از آنک</p></div>
<div class="m2"><p>نسر طائر را مگس بینی چو دل بنهاد خوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن زمان کز در درآید آفتاب دل تو را</p></div>
<div class="m2"><p>گر توانی سایهٔ خود را برون در نشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون تو مهر نیستی را بر گریبان بسته‌ای</p></div>
<div class="m2"><p>هیچ دامانت نگیرد هستی کون و مکان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چهرهٔ خورشید وانگه زحمت مشاطگی</p></div>
<div class="m2"><p>مرکب جمشید وانگه حاجب برگستوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در دبیرستان خرسندی نوآموزی هنوز</p></div>
<div class="m2"><p>کودکی کن دم مزن چون مهر داری بر زبان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست اندر گوهر آدم خواص مردمی</p></div>
<div class="m2"><p>بر ولیعهدان شیطان حرف کرمنا مخوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خلوتی کز فقر سازی خیمهٔ مهدی شناس</p></div>
<div class="m2"><p>زحمتی کز خلق بینی موکب دجال دان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شش جهت یاجوج بگرفت ای سکندر الغیاث</p></div>
<div class="m2"><p>هفت کشور دیو بستد ای سلیمان الامان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تخت نرد پاک بازان در عدم گسترده‌اند</p></div>
<div class="m2"><p>گر سرش داری برانداز این بساط باستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرد همدم آنگه اندوزد که آید در عدم</p></div>
<div class="m2"><p>موم از آتش آنگه افروزد که دارد ریسمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دل رمیده کی تواند ساخت با ساز وجود</p></div>
<div class="m2"><p>سگ گزیده کی تواند دید در آب روان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا به نااهلان نگوئی سر وحودت هین و هین</p></div>
<div class="m2"><p>تا ز ناجنسان نجوئی برگ سلوت هان و هان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عیسی از گفتار نااهلی برآمد بر فلک</p></div>
<div class="m2"><p>آدم از وسواس ناجنسی برون رفت از جنان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چند چون هدهد تهدد بینی از رنج و عذاب</p></div>
<div class="m2"><p>تو برای رهنمای ملک پیک رایگان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این گره بادند از ایشان کار سازی کم طلب</p></div>
<div class="m2"><p>کآتشی بالای سر دارند و آبی زیر ران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا جدائی زین و آن بر سر نشینی چون الف</p></div>
<div class="m2"><p>چون بپیوستی به پایان اوفتی هم در زمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عقل چون گربه سری در تو همی مالد ز مهر</p></div>
<div class="m2"><p>تا نبرد رشتهٔ جان تو چون موش این و آن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر تو هستی خستهٔ زخم پلنگ حادثات</p></div>
<div class="m2"><p>پس تو را از خاصیت هم گربه بهتر پاسبان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چار تکبیری بکن بر چار قصل روزگار</p></div>
<div class="m2"><p>چار بالش‌های چار ارکان را به دو نان بازمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چند بر گوسالهٔ زرین شوی صورت پرست</p></div>
<div class="m2"><p>چند بر بزغالهٔ پر زهر باشی میهمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ناقهٔ همت به راه فاقه ران تا گرددت</p></div>
<div class="m2"><p>توشه خوشهٔ چرخ و منزل‌گاه راه کهکشان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هم‌چنین بازی درویشان همی زی زانکه هست</p></div>
<div class="m2"><p>جبرئیل اجری کش این قوم و رضوان میزبان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جان مده در عشق زور و زر که ندهد هیچ طفل</p></div>
<div class="m2"><p>لعبت چشم از برای لعبتی از استخوان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اولین برج از فلک صفر است چون تو بهر فقر</p></div>
<div class="m2"><p>اولین پایه گرفتی صفر بهتر خان و مان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون سرافیل قناعت تا ابد جاندار توست</p></div>
<div class="m2"><p>گو مکن دیوان میکائیل روزی را ضمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خیز خاقانی ز کُنج فقر خلوت‌خانه ساز</p></div>
<div class="m2"><p>کز چنین توان اندوخت گنج شایگان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آتش اندر جاه زن گو باد در دست تکین</p></div>
<div class="m2"><p>آب رخ در چاه کن گو خاک بر فرق طغان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تخت ساز از حرص تا فرمان دهی بر تاج‌بخش</p></div>
<div class="m2"><p>پشت کن بر آز تا پهلو زنی با پهلوان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نی صفی الملک را بینی صفائی بر جبین</p></div>
<div class="m2"><p>نی رضی الدوله را یابی رضائی در جنان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر به رنگِ جامه عیبت کرد جاهل، باک نیست</p></div>
<div class="m2"><p>تابش مه را ز بانگ سگ کجا خیزد زیان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چون تو یک‌رنگی بدل گر رنگ‌رنگ آید لباس</p></div>
<div class="m2"><p>کی عجب چون عیسی دل بر درت دارد دکان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گرچه رنگین‌کسوتی صاحب‌خبر هستی ز عقل</p></div>
<div class="m2"><p>کلک رنگین‌جامه هم صاحب برید است از روان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چون کتاب الله به سرخ و زرد می‌شاید نگاشت</p></div>
<div class="m2"><p>گر تو سرخ و زرد پوشی هم بشاید بی‌گمان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نی کم از مور است زنبور منقش در هنر</p></div>
<div class="m2"><p>نی کم از زاغ است طاووس بهشتی ز امتحان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>باش با عشاق چون گل در جوانی پیر دل</p></div>
<div class="m2"><p>چند ازین زُهاد همچون سرو در پیری جوان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر زمین زن صحبت این زاهدان جاه‌جوی</p></div>
<div class="m2"><p>مشتری‌صورت ولی مریخ‌سیرت در نهان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چون تنور از نار نخوت هرزه‌خوار و تیزدَم</p></div>
<div class="m2"><p>چون فطیر از روی فطرت بد‌گوار و جان‌گزان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اربعین‌شان را ز خمسین نصاری دان مدد</p></div>
<div class="m2"><p>طیلسان‌شان را ز زنار مجوسی دان نشان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نیست اندر جامهٔ ازرق حفاظ و مردمی</p></div>
<div class="m2"><p>چرخ ازرق‌پوش اینک عمر‌کاه و جان‌ستان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چند نالی چند ازین محنت‌سرای زاد و بود</p></div>
<div class="m2"><p>کز برای رای تو شروان نگردد خیروان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بچهٔ بازی برو بر ساعد شاهان نشین</p></div>
<div class="m2"><p>بر مگس‌خواران قولنجی رها کن آشیان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ای عزیز مادر و جان پدر تا کی تو را</p></div>
<div class="m2"><p>این به زیر تیشه دارد و آن به سایهٔ دوکدان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ای درین گهوارهٔ وحشت چو طفلان پای‌بست</p></div>
<div class="m2"><p>غم تو را گهواره جنبان و حوادث دایگان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شیر مردی خیز و خوی شیر خوردن کن رها</p></div>
<div class="m2"><p>تا کی این پستان زهرآلود داری در دهان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گر حوادث پشت امیدت شکست اندیشه نیست</p></div>
<div class="m2"><p>مومیائی هست مدح صاحب صاحب‌قران</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>حجة الاسلام نجم الدین که گردون بر درش</p></div>
<div class="m2"><p>چون زمین بوسد نگارد عبده بر آستان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>جاه او در یک دو ساعت بر سه بعد و چار طبع</p></div>
<div class="m2"><p>پنج نوبت می‌زند در شش سوی این هفت‌خوان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تا بت بدعت شکست اقبال نجم سیمگر</p></div>
<div class="m2"><p>سکه نقش بت به زر دادن نیارد در جهان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چارپای منبرش را هشت حمالان عرش</p></div>
<div class="m2"><p>بر کتف دارند کاین مرکز ندارد قدر آن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ای وصی آدم و کارم ز گردون ناتمام</p></div>
<div class="m2"><p>وی مسیح علام و جانم ز گیتی ناتوان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گر نداری هیچ فرزندی شرف داری که حق</p></div>
<div class="m2"><p>هم شرف زین دارد اینک لم‌یلد خوان از قران</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بیضه بشکن بچه بیرون آر چون طاووس نر</p></div>
<div class="m2"><p>بیضه پروردن به گنجشکان گذار و ماکیان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کاین نتایج‌های فکر تو تو را بس ذریت</p></div>
<div class="m2"><p>وین معانی‌های بکر تو تو را بس خاندان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چون خود و چون من نبینی هیچ‌کس در شرع و شعر</p></div>
<div class="m2"><p>قاف تا قاف ار بجویی قیروان تا قیروان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>زادهٔ طبع منند اینان که خصمان منند</p></div>
<div class="m2"><p>آری آری گربه هست از عطسهٔ شیر ژیان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دشمن جاه منند این قوم کی باشند دوست</p></div>
<div class="m2"><p>جون من از بسطام باشم این گروه از دامغان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز آن کرامت‌ها که حق با این دروگر زاده کرد</p></div>
<div class="m2"><p>می‌کشند از کینه چون نمرود بر گردون کمان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>پا شکستم زین خران، گرچه درست از من شدند</p></div>
<div class="m2"><p>خوانده‌ای تا عیسی از مقعد چه دیدآخر زیان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>جان کنند از ژاژخائی تا به گرد من رسند</p></div>
<div class="m2"><p>کی رسد سیر السوافی در نجیب ساربان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>صد هزاران پوست از شخص بهائم برکشند</p></div>
<div class="m2"><p>تا یکی زانها کند گردون درفش کاویان</p></div></div>