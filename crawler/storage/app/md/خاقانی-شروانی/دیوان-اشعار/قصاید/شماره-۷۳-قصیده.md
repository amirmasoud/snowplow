---
title: >-
    شمارهٔ ۷۳ - قصیده
---
# شمارهٔ ۷۳ - قصیده

<div class="b" id="bn1"><div class="m1"><p>سرورانی که مرا تاج سرند</p></div>
<div class="m2"><p>از سر قدر همه تاجورند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به لقا و به لقب عالم را</p></div>
<div class="m2"><p>عز اسلام و ضیاء بصرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آدمی نفس و ملایک نفس‌اند</p></div>
<div class="m2"><p>پادشا سار و پیمبر سیرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برتر از نقطهٔ خاک‌اند به ذات</p></div>
<div class="m2"><p>نه به پرگار نه افلاک درند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به همم صاحب صدر فلک‌اند</p></div>
<div class="m2"><p>به قلم نائب حکم قدرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نی عسکری ملک طراز</p></div>
<div class="m2"><p>عسکر آرای ملوک بشرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا دوات همه پر نیشکر است</p></div>
<div class="m2"><p>همه شیران گرو نیشکرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تب برد شیر و پناهد سوی نی</p></div>
<div class="m2"><p>تا به نی بو که تب او ببرند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سفرهٔ مائده پرداز همه است</p></div>
<div class="m2"><p>تا همه سفره نشین سفرند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوانشان خوانچهٔ خورشید سزد</p></div>
<div class="m2"><p>که به همت همه عیسی هنرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که گهی خوردی ترکان طلبند</p></div>
<div class="m2"><p>که همه در رخ ترکان نگرند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه ترکان فلک را پس از این</p></div>
<div class="m2"><p>خلق تتماجی ایشان شمرند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خورد ترکانه عجب می‌سازند</p></div>
<div class="m2"><p>هندویی دو که مرا طبخ گرند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرچه محور سپرد قرصهٔ خور</p></div>
<div class="m2"><p>قرص خوربین که به محور سپرند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هندوانند سپر ساز از سیم</p></div>
<div class="m2"><p>لیک دارندهٔ تیر خزرند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به سر تیغ به صد پاره کنند</p></div>
<div class="m2"><p>چون به تیرش به سر بار برند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هندوان بینی در مطبخ من</p></div>
<div class="m2"><p>که چو دیلم همه سیمین سپرند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خورشی کرده به تیر است و به تیغ</p></div>
<div class="m2"><p>تا بزرگان به سر نیزه خورند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این چنین ماحضری ساخته شد</p></div>
<div class="m2"><p>که دو عالم ببرش مختصرند</p></div></div>