---
title: >-
    شمارهٔ ۱۳ - مطلع سوم
---
# شمارهٔ ۱۳ - مطلع سوم

<div class="b" id="bn1"><div class="m1"><p>نافهٔ آهو شده است ناف زمین از صبا</p></div>
<div class="m2"><p>عقد دو پیکر شده است پیکر باغ از هوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روح روان است آب بی‌عمل امتحان</p></div>
<div class="m2"><p>زر خلاص است خاک بی‌اثر کیمیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاخ شکوفه فشان سنقر کانند خرد</p></div>
<div class="m2"><p>هر نفسی بال و پر ریخته‌شان از قضا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دفتر گل را فلک کرد به شنگرف رنگ</p></div>
<div class="m2"><p>زرین شیرازه زد هر ورقی را جدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر قد لاله قمر دوخت قباهای رش</p></div>
<div class="m2"><p>خشتک نفطی نهاد بر سر چینی قبا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش نسیم سحر بر در من حلقه زد</p></div>
<div class="m2"><p>گفتم هان کیست؟ گفت : قاصدیم آشنا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان مرا هدیه کرد بوی سر زلف یار</p></div>
<div class="m2"><p>از نفحات ربیع در حرکات صبا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم ز اسرار باغ هیچ شنیدی بگوی</p></div>
<div class="m2"><p>گفت دل بلبل است در کف گل مبتلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم کامروز کیست تازه سخن در جهان</p></div>
<div class="m2"><p>گفت که خاقانی است بلبل باغ ثنا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مادح شیخ امام، عالم عامل که هست</p></div>
<div class="m2"><p>ناصر دین خدای مفتخر اولیا</p></div></div>