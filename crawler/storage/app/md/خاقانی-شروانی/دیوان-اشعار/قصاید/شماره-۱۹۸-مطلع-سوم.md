---
title: >-
    شمارهٔ ۱۹۸ - مطلع سوم
---
# شمارهٔ ۱۹۸ - مطلع سوم

<div class="b" id="bn1"><div class="m1"><p>این آتشین کاسه نگر، دولاب مینا داشته</p></div>
<div class="m2"><p>از آب کوثر کاسه‌تر و آهنگ دریا داشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دلو نور افشان شده، ز آنجا به ماهی دان شده</p></div>
<div class="m2"><p>ماهی از او بریان شده یک ماهه نعما داشته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماهی و قرص خور بهم حوت است و یونس در شکم</p></div>
<div class="m2"><p>ماهی همه گنج درم، خور زر گونا داشته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انجم نثار افشان او، اجرا خوران از خوان او</p></div>
<div class="m2"><p>از ماهی بریان او نزل مهنا داشته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید نو تاثیر بین، حوتش بهین توفیر بین</p></div>
<div class="m2"><p>جمشید ماهی گیر بین، نو ملک زیبا داشته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گنج بهار اینک روان، میغ اژدهای گنج‌بان</p></div>
<div class="m2"><p>رخش سحاب اینک دوان وز برق هرا داشته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون روغن طلق است طل بحر دمان زیبق عمل</p></div>
<div class="m2"><p>خورشید در تصعید وحل آتش در اعضا داشته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون آتش آمد آشنا زیبق پرید اندر هوا</p></div>
<div class="m2"><p>اینک هوا سیمین هبا زیبق مجزا داشته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین پس و شاقان چمن نو خط شوند و غمزه زن</p></div>
<div class="m2"><p>طوق خط و چاه ذقن پر مشک سارا داشته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در هر چمن عاشق وشان بر ساقی و می جان‌فشان</p></div>
<div class="m2"><p>پیر خرد ز انصافشان با می مواسا داشته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گردان بر هر نوبری گل سارغ از مل ساغری</p></div>
<div class="m2"><p>وان مل محک هر زری با گل محاکا داشته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جام است یا جوز است آن یا خود بیضاست آن</p></div>
<div class="m2"><p>یا تیغ بوالهیجاست آن در قلب هیجا داشته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نوروز پیک نصرتش، میقات‌گاه عشرتش</p></div>
<div class="m2"><p>نه مه بهار از حضرتش دل ناشکیبا داشته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نوروز نو شروان‌شهی چل صبح و شش روزش رهی</p></div>
<div class="m2"><p>جاسوس بختش ز آگهی دل علم فردا داشته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاقان اکبر کز دمش عشری است جان عالمش</p></div>
<div class="m2"><p>نه چرخ زیر خاتمش هر هفت غبرا داشته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برجیس حکم، افلاک ظل، ادریس جان، جبریل دل</p></div>
<div class="m2"><p>از خط کل تا شط گل عالم به تنها داشته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا عالمش دریافته پیران سر افسر یافته</p></div>
<div class="m2"><p>هم شرع داور یافته هم ملک دارا داشته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پروانه چرخ اخضرش پرواز نسرین از فرش</p></div>
<div class="m2"><p>پرواز سعدین بر سرش چندان که پروا داشته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شمشیر او طوبی مثال او را جنان تحت الظلال</p></div>
<div class="m2"><p>انوار عز فوق الکمال از حق تعالی داشته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گردون و هفت اجرام او تحت الشعاع جام او</p></div>
<div class="m2"><p>فوق الصفه ز اکرام او دین مجد والا داشته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دریای عقلی در دلش، صحرای قدسی منزلش</p></div>
<div class="m2"><p>از نفس کل آب و گلش صفوت در اجزا داشته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ذاتش مراد کاف و نون از علت عالم برون</p></div>
<div class="m2"><p>دل را به عصمت رهنمون بر ترک اشیا داشته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لب‌های شاهان درگهش کوثر دم از خاک رهش</p></div>
<div class="m2"><p>جنت به خاک درگهش روی تولا داشته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خوانده به چتر شاه بر چرخ آیة الکرسی ز بر</p></div>
<div class="m2"><p>چترش همائی زیر پر عرش معلا داشته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چل صبح آدم هم‌دمش ، ملک خلافت ز آدمش</p></div>
<div class="m2"><p>هم بوده اسم اعظمش هم علم اسما داشته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون از عدم درتاخته، دیده فلک دست آخته</p></div>
<div class="m2"><p>انصاف پنهان ساخته، ظلم آشکارا داشته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ملکت گرفته رهزنان، برده نگین اهریمنان</p></div>
<div class="m2"><p>دین نزد این تردامنان نه جا نه ملجا داشته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر خوک خواری بر زمین دهقان و عیسی خوشه چین</p></div>
<div class="m2"><p>هر پشهٔ طارم نشین، پیلان به سرما داشته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شاه است عدل انگیخته دست فلک بربیخته</p></div>
<div class="m2"><p>هم خون ظالم ریخته هم ملک آبا داشته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چندان برون رانده سپه کاتش گرفته فرق مه</p></div>
<div class="m2"><p>نه باد را بر خاک ره نی آب مجرا داشته</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چرخ و زمان کرده ندا کای تیغ تو جان هدی</p></div>
<div class="m2"><p>ما خاک پایت را فدا تو دست بر ما داشته</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ملک ابد را رایگان مخلص بر او کرد آسمان</p></div>
<div class="m2"><p>ملکی ز مقطع کم زیان وز عدل مبدا داشته</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از فتح اران نام را زیور زده ایام را</p></div>
<div class="m2"><p>فتح عراق و شام را وقتی مسما داشته</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بحری است تیغش و آسمان بر گوهرش اختر فشان</p></div>
<div class="m2"><p>ز آن گوهری تیغ اختران چشم مدارا داشته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن روض دوزخ بار بین، حور زبانی سار بین</p></div>
<div class="m2"><p>بحر نهنگ اوبار بین آهنگ اعدا داشته</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>معمار دین آثار او، دین زنده از کردار او</p></div>
<div class="m2"><p>گنجی است آن دیوار او از خضر بنا داشته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جسته نظیر او جهان، نادیده عنقا را نشان</p></div>
<div class="m2"><p>اینک جهان را غیب دان زین خرده برپا داشته</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خط کفش حرز شفا، تیغش در او عین الصفا</p></div>
<div class="m2"><p>چون نور مهر مصطفی جان بحیرا داشته</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دهر است خندان بر عدو کو جاه شه کرد آرزو</p></div>
<div class="m2"><p>مقل است بار نخل او، او چشم خرما داشته</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پران ملک پیرامنش، چون چرخ دائر بر تنش</p></div>
<div class="m2"><p>چون بادریسه دشمنش یک چشم بینا داشته</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ای تاج گردون گاه تو، مهدی دل آگاه تو</p></div>
<div class="m2"><p>یک بندهٔ درگاه تو صد چین و یغما داشته</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بر بندگان پاشی گهر هر بنده‌ای را بر کمر</p></div>
<div class="m2"><p>ز آن لعبتان کز صلب خور ارحام خارا داشته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>افلاک تنگ ادهمت، خورشید موم خاتمت</p></div>
<div class="m2"><p>دل مرده گیتی از دمت امید احیا داشته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خوش غمزه چشم خور ز تو شب طره پر عنبر ز تو</p></div>
<div class="m2"><p>پیشانی اختر ز تو داغ اطعنا داشته</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خصمت ز دولت بینوا و آنگه درت کرده رها</p></div>
<div class="m2"><p>چشمش به درد او توتیا بر باد نکبا داشته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر با تو خصم آرش بود هم جفت او آتش بود</p></div>
<div class="m2"><p>صحنات کمتر خوش بود، با صحن حلوا داشته</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هر موی رخشت رستمی مدهامتان وش ادهمی</p></div>
<div class="m2"><p>طاس زرش هر پرچمی از زلف حورا داشته</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>باد سلیمان در برش و زنار موسی منظرش</p></div>
<div class="m2"><p>طیر است گوئی پیکرش، طور است مانا داشته</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از نعل او مه را گله، بر چشم خورشید آبله</p></div>
<div class="m2"><p>کاه و جوش ز آن سنبله کاین سبز صحرا داشته</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>باد از سعادات ابد بیت الحیاتت را مدد</p></div>
<div class="m2"><p>هیلاج عمرت را عدد غایات اقصی داشته</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>برتر ز عرشت قدر و قد، رایت ورای حزر و حد</p></div>
<div class="m2"><p>ذاتت به دست جود و جد گیتی مطرا داشته</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در سجده صف‌های ملک پیش تو خاشع یک به یک</p></div>
<div class="m2"><p>چندان که محراب فلک پیران و برنا داشته</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مولات بی‌نام آسمان، باجت رساد از اختران</p></div>
<div class="m2"><p>صف غلامانت جهان شرقا و غربا داشته</p></div></div>