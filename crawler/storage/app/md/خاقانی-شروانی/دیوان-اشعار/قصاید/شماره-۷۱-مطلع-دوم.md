---
title: >-
    شمارهٔ ۷۱ - مطلع دوم
---
# شمارهٔ ۷۱ - مطلع دوم

<div class="b" id="bn1"><div class="m1"><p>دوش چون خورشید را مصروع خاور ساختند</p></div>
<div class="m2"><p>ماه نو را چون حمایل چفته پیکر ساختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قرص خور مصروع از آن شد کز حمایل باز ماند</p></div>
<div class="m2"><p>کآن حمایل هم برای قرصهٔ خور ساختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوشهٔ جام شکسته سوی خاور شد پدید</p></div>
<div class="m2"><p>یک جهان نظاره کن کآن جام از چه گوهر ساختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محتسب گویی به ماه روزه جام می شکست</p></div>
<div class="m2"><p>کآن شکسته جام را رسوای خاور ساختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا شبانگه فصد کردند اختران تب زده</p></div>
<div class="m2"><p>کآسمان طشت و شفق خون، ماه نشتر ساختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخ جادوپیشه چون زرین قُواره کرد گم</p></div>
<div class="m2"><p>دامن کُحلیش را جَیبی مُقَّور ساختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درزیان چرخ را گوئی که سهو افتاده بود</p></div>
<div class="m2"><p>کآن زه سیمین بر آن دامن نه در خور ساختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماه نو چون حلقهٔ ابریشم و شب موی چنگ</p></div>
<div class="m2"><p>موی و ابریشم بهم چون عود و شکر ساختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهر چون در خوشه یک مه ساخت خرمن، روشنان</p></div>
<div class="m2"><p>ماه را صاع زر شاه مظفر ساختند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیمهٔ قندیل عیسی بود یا محراب روح</p></div>
<div class="m2"><p>تا مثال طوق اسب شاه صفدر ساختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوش چون من ماه نو دیدم به روی تخت شاه</p></div>
<div class="m2"><p>از ریاض خاطرم این قطعه نوبر ساختند</p></div></div>