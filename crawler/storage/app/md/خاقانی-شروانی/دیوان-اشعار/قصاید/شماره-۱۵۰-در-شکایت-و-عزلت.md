---
title: >-
    شمارهٔ ۱۵۰ - در شکایت و عزلت
---
# شمارهٔ ۱۵۰ - در شکایت و عزلت

<div class="b" id="bn1"><div class="m1"><p>به دل در خواص بقا می‌گریزم</p></div>
<div class="m2"><p>به جان زین خراس فنا می‌گریزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن چرخ چون باز بر دوخت چشمم</p></div>
<div class="m2"><p>که باز از گزند بلا می‌گریزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو باز ارچه سر کوچکم دل بزرگم</p></div>
<div class="m2"><p>نخواهم کله وز قبا می‌گریزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درخت وفا را کنون برگ ریز است</p></div>
<div class="m2"><p>ازین برگ ریز وفا می‌گریزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه از سایهٔ غیر سر می‌رهانم</p></div>
<div class="m2"><p>گه از خود چو سایه جدا می‌گریزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بیگانه‌ای مانم از سایهٔ خود</p></div>
<div class="m2"><p>ولی در دل آشنا می‌گریزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم دردمند است و هم درد بهتر</p></div>
<div class="m2"><p>طبیب دلم کز دوا می‌گریزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا چشم درد است و خورشید خواهم</p></div>
<div class="m2"><p>که از زحمت توتیا می‌گریزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا چون خرد بند تکلیف سازد</p></div>
<div class="m2"><p>ز بند خرد در هوا می‌گریزم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دهان صبا مشک نکهت شد از می</p></div>
<div class="m2"><p>به بوی می اندر صبا می‌گریزم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگو با مغان کاب کاری شما راست</p></div>
<div class="m2"><p>که در کار آب شما می‌گریزم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا ز اربعین مغان چون نپرسی</p></div>
<div class="m2"><p>که چل صبح در مغ سرا می‌گریزم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به انصاف دریاکشانند کانجا</p></div>
<div class="m2"><p>ز جور نهنگ عنا می‌گریزم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مغان را خرابات کهف صفا دان</p></div>
<div class="m2"><p>در آن کهف بهر صفا می‌گریزم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من آن هشتم هفت مردان کهفم</p></div>
<div class="m2"><p>که از سرنوشت جفا می‌گریزم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بده جام فرعونیم کز تزهد</p></div>
<div class="m2"><p>چو فرعونیان ز اژدها می‌گریزم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به من آشکارا ده آن می که داری</p></div>
<div class="m2"><p>به پنهان مده کز ریا می‌گریزم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا از من و ما به یک رطل برهان</p></div>
<div class="m2"><p>که من، هم ز من، هم ز ما می‌گریزم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من از باده گویم تو از توبه گویی</p></div>
<div class="m2"><p>مگو کز چنین ماجرا می‌گریزم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حریف صبوحم نه سبوح خوانم</p></div>
<div class="m2"><p>که از سبحهٔ پارسا می‌گریزم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرا سجده گه بیت نبت العنب بس</p></div>
<div class="m2"><p>که از بیت ام القری می‌گریزم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا مرحبا گفتن سفره داران</p></div>
<div class="m2"><p>نباید، کز آن مرحبا می‌گریزم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قدح‌ها ملا کن به من ده که من خود</p></div>
<div class="m2"><p>ز قوت اللسان برملا می‌گریزم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه‌نه می‌نگیرم که میگون سرشکم</p></div>
<div class="m2"><p>که خود زین می کم بها می‌گریزم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سگ ابلق روز و شب جان‌گزای است</p></div>
<div class="m2"><p>ازین ابلق جان‌گزا می‌گریزم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ندارم سر می که چون سگ گزیده</p></div>
<div class="m2"><p>جگر تشنه‌ام از سقا می‌گریزم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کشش خود نخواهم من آهنین جان</p></div>
<div class="m2"><p>که از سنگ آهن ربا می‌گریزم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هم از دوست آزرده‌ام هم ز دشمن</p></div>
<div class="m2"><p>پس از هر دو تن در خدا می‌گریزم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مسیحم که گاه از یهودی هراسم</p></div>
<div class="m2"><p>گه از راهب هرزه‌لا می‌گریزم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنانم دل آزرده از نقش مردم</p></div>
<div class="m2"><p>که از نقش مردم‌گیا می‌گریزم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گریزد ز شکل عصا مار و گوید</p></div>
<div class="m2"><p>عصا شکلم و از عصا می‌گریزم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قفا چون ز دست امل خوردم اکنون</p></div>
<div class="m2"><p>ز تیغ اجل در قفا می‌گریزم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به بزغاله گفتند بگریز، گفتا:</p></div>
<div class="m2"><p>که قصاب در پی کجا می‌گریزم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همه حس من یک به یک هست سلطان</p></div>
<div class="m2"><p>از این سگ مشام گدا می‌گریزم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من آن دانهٔ دست کشت کمالم</p></div>
<div class="m2"><p>کزین عمرسای آسیا می‌گریزم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من آبم که چون آتشی زیر دارم</p></div>
<div class="m2"><p>ز ننگ زمین در هوا می‌گریزم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدیدم عیار جهان کم ز هیچ است</p></div>
<div class="m2"><p>ازین بهرج ناروا می‌گریزم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سیاه است بختم ز دست سپیدش</p></div>
<div class="m2"><p>ور این پیر ازرق‌وطا می‌گریزم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز بیم فلک در ملک می‌پناهم</p></div>
<div class="m2"><p>ز ترس تبر در گیا می‌گریزم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو روز است روشن که بخت است تاری</p></div>
<div class="m2"><p>به شب زین شبانگه لقا می‌گریزم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>صلای سر و تیغ می‌گوئی و من</p></div>
<div class="m2"><p>نه سر می‌کشم، نز صلا می‌گریزم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گرم ساز یکتا زنی یا دوتائی</p></div>
<div class="m2"><p>در اندازمت کز سه تا می‌گریزم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وغا در سه و چار بینی نه در یک</p></div>
<div class="m2"><p>من و نقش یک کز وغا می‌گریزم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>قماری زنم بر سر پای وانگه</p></div>
<div class="m2"><p>ز سر پای سازم به پا می‌گریزم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اسیرم به بندخیالات و جان را</p></div>
<div class="m2"><p>نوا می‌دهم وز نوا می‌گریزم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز کی تا به کی پای‌بست وجودم</p></div>
<div class="m2"><p>ندارم سر و دست و پا می‌گریزم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گریزانم از کائنات اینت همت</p></div>
<div class="m2"><p>نه اکنون، که عمری است تا می‌گریزم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز تنگی مکان و دورنگی زمان بس</p></div>
<div class="m2"><p>به جان آمدم زین دو تا می‌گریزم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرا منتهای طلب نیست سدره</p></div>
<div class="m2"><p>که زا سدرة المنتهی می‌گریزم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به آهی بسوزم جهان را ز غیرت</p></div>
<div class="m2"><p>که در حضرت پادشا می‌گریزم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نه زین هفت ده خاکدانم گریزان</p></div>
<div class="m2"><p>که از هشت شهر سما می‌گریزم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مرا دان بر از هفت و نه متکائی</p></div>
<div class="m2"><p>که در ظل آن متکا می‌گریزم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نه عیسی صفت زین خرابات ظلمت</p></div>
<div class="m2"><p>در ایوان شمس الضحی می‌گریزم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نه ادریس وارم به زندان خوفی</p></div>
<div class="m2"><p>که در هشت باغ رجا می‌گریزم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>صباح و مسا نیست در راه وحدت</p></div>
<div class="m2"><p>منم کز صباح و مسا می‌گریزم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو جغد ار برون راندم آسیابان</p></div>
<div class="m2"><p>بر این بام هفت آسیا می‌گریزم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بقا دوستان را، فنا عاشقان را</p></div>
<div class="m2"><p>من آن عاشقم کز بقا می‌گریزم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو هستی است مقصد در او نیست گردم</p></div>
<div class="m2"><p>که از خود همه در فنا می‌گریزم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شوم نیست در سایهٔ هست مطلق</p></div>
<div class="m2"><p>که در نیستی مطلقا می‌گریزم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همه نعل مرکب زنم باژگونه</p></div>
<div class="m2"><p>به وقتی کز این تنگ جا می‌گریزم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بسی زانیانند دور فلک را</p></div>
<div class="m2"><p>ازین دیر دار الزنا می‌گریزم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وباخانه‌ای چرخ و خلقی ز جیفه</p></div>
<div class="m2"><p>هلاک است، ازان از وبا می‌گریزم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو غوغا کند بر دلم نامرادی</p></div>
<div class="m2"><p>من اندر حصار رضا می‌گریزم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نیاز عطا داشتم تا به اکنون</p></div>
<div class="m2"><p>نیازم نماند از عطا می‌گریزم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>طمع حیض مرد است و من می‌برم سر</p></div>
<div class="m2"><p>طمع را کز اهل سخا می‌گریزم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>که خرگوش حیض النسا دارد و من</p></div>
<div class="m2"><p>پلنگم ز حیض‌النسا می‌گریزم</p></div></div>