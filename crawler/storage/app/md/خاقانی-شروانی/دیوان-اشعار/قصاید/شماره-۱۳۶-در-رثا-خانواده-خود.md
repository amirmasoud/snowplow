---
title: >-
    شمارهٔ ۱۳۶ - در رثاء خانوادهٔ خود
---
# شمارهٔ ۱۳۶ - در رثاء خانوادهٔ خود

<div class="b" id="bn1"><div class="m1"><p>بی‌باغ رخت جهان مبینام</p></div>
<div class="m2"><p>بی‌داغ غمت روان مبینام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌وصل تو کاصل شادمانی است</p></div>
<div class="m2"><p>تن را دل شادمان مبینام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌لطف تو کآب زندگانی است</p></div>
<div class="m2"><p>از آتش غم امان مبینام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل زنده شدی به بوی بویت</p></div>
<div class="m2"><p>کان بوی ز دل نهان مبینام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌بوی تو کاشنای جان است</p></div>
<div class="m2"><p>رنگی ز حیات جان مبینام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا جان گرو دمی است با جان</p></div>
<div class="m2"><p>جز داو غمت روان مبینام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دیدهٔ‌خویش چون کبوتر</p></div>
<div class="m2"><p>جز نام تو جاودان مبینام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌سرو قد تو جعد شمشاد</p></div>
<div class="m2"><p>بر جبهت بوستان مبینام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک دانهٔ آفتاب بی‌تو</p></div>
<div class="m2"><p>بر گردن آسمان مبینام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دانهٔ دل ز کشت شادی</p></div>
<div class="m2"><p>یک خوشه به سالیان مبینام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آینهٔ دل از خیالت</p></div>
<div class="m2"><p>جز صورت جان عیان مبینام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در آینهٔ خیالت از خود</p></div>
<div class="m2"><p>جز موی خیال سان مبینام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا وصل تو زان جهان نیاید</p></div>
<div class="m2"><p>دل را سر این جهان مبینام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جز اشک وداعی من و تو</p></div>
<div class="m2"><p>طوفان جهان ستان مبینام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون حقهٔ سینه برگشایم</p></div>
<div class="m2"><p>جز نام تو در میان مبینام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر عمر کران کنم به سودات</p></div>
<div class="m2"><p>سودای تو را کران مبینام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتی دگری کنی، مفرمای</p></div>
<div class="m2"><p>کاین در ورق گمان مبینام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بی‌تو من و عیش حاش‌لله</p></div>
<div class="m2"><p>کز خواب خیال آن مبینام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خاقانی را ز دل چه پرسی</p></div>
<div class="m2"><p>کانست که کس چنان مبینام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حالی که به دشمنان نخواهم</p></div>
<div class="m2"><p>حسب دل دوستان مبینام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غمخوار تو را به خاک تبریز</p></div>
<div class="m2"><p>جز خاک تو غم نشان مبینام</p></div></div>