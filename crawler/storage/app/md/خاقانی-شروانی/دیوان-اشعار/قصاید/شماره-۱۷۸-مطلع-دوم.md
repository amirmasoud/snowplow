---
title: >-
    شمارهٔ ۱۷۸ - مطلع دوم
---
# شمارهٔ ۱۷۸ - مطلع دوم

<div class="b" id="bn1"><div class="m1"><p>غارت دل می‌کنی شرط وفا نیست این</p></div>
<div class="m2"><p>کار من از سایه شد سایه برافکن ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصل ندیده به خواب فرض کنی خوش‌دلی</p></div>
<div class="m2"><p>بر سر خوان تهی کس نکند آفرین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غمت ای زود سیر تشنهٔ دیرینه‌ام</p></div>
<div class="m2"><p>تشنه به جز من که دید آب‌خورش آتشین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان چو سزای تو نیست باد به دست جهان</p></div>
<div class="m2"><p>مهر چو مقبول نیست خاک به فرق نگین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلبن وصل تو را خار جفا در ره است</p></div>
<div class="m2"><p>مهره چه بینی به کف مار نگر در کمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق توآم پوستین گر بدرد گو بدر</p></div>
<div class="m2"><p>سوختهٔ گرم رو تا چکند پوستین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همت خاقانی است طالب چرب آخوری</p></div>
<div class="m2"><p>چون سر کوی تو هست نیست مزیدی بر این</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست لب لعل تو کوثر آتش نمای</p></div>
<div class="m2"><p>هست کف شهریار گوهر دریا یمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرخ به هرسان که هست زادهٔ شمشیر اوست</p></div>
<div class="m2"><p>گربه بهر هر حال هست عطسهٔ شیر عرین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای به تو صاحب درفش چتر فریدون ملک</p></div>
<div class="m2"><p>وی ز تو طالب نگین دست سلیمان دین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پر خدنگ تو هست شهپر روح القدس</p></div>
<div class="m2"><p>پرچم رخش تو هست ناصیهٔ حور عین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نوبتی بدعه را قهر تو برد طناب</p></div>
<div class="m2"><p>صیرفی شرع را قدر تو زیبد امین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاصه سیمرغ کیست جز پدر روستم؟</p></div>
<div class="m2"><p>قاتل ضحاک کیست جز پسر آبتین؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرنه سپهر برین آبده دست توست</p></div>
<div class="m2"><p>از چه سبب خم گرفت پشت سپهر برین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عدل تو «شین» راز «را» کرد جدا چون بدید</p></div>
<div class="m2"><p>کالت رای است «را» صورت شین است «شین»</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ملک چو تیغ تو یافت یک دو شود کار او</p></div>
<div class="m2"><p>شصت به سیصد رسد چون سه نقط یافت سین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تیغ تو نه ماهه بود حامله از نه فلک</p></div>
<div class="m2"><p>لاجرمش فتح و نصر هست بنات و بنین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر به مثل روز رزم رخش تونعل افکند</p></div>
<div class="m2"><p>یاره کند در زمانش دست شهور و سنین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون ز خروش دو صف وقت هزاهز کند</p></div>
<div class="m2"><p>چشم جهان اختلاج، گوش زمانه طنین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کوس و غبار سیاه طوطی و صحرای هند</p></div>
<div class="m2"><p>خنجر و خون سپاه آینه و بحر چین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صاحب بدر و حنین از تو گشاید فقاع</p></div>
<div class="m2"><p>کان گهر چون سداب برکشی از بهر کین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گنبد نیلوفری گنبدهٔ گل شود</p></div>
<div class="m2"><p>پیش سنانت کز اوست قصر ممالک حصین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تیغ زبان شکل تو از بر خواند چو آب</p></div>
<div class="m2"><p>ابجد لوح ظفر از خط دست یقین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از پی خون خسان تیغ چه باید کشید</p></div>
<div class="m2"><p>چون ملک الموت هست در کف رایت رهین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خلق تو از راه لطف جان برباید ز خلق</p></div>
<div class="m2"><p>چون حرکات هزار در نغمات حزین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از عدوی سگ صفت حلم و تواضع مجوی</p></div>
<div class="m2"><p>زانکه به قول خدای نیست شیاطین ز طین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای همه هستی که هست از کف تو مسعار</p></div>
<div class="m2"><p>نیست نیازی که نیست بر در تو مستعین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر که به درگاه تو سجده برد روز حشر</p></div>
<div class="m2"><p>آیت لاتقنطوا نقش زند بر جبین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون تویی اندر جهان شاه طغان کرم</p></div>
<div class="m2"><p>کی رود اهل هنر بر در تاش و تکین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرد که فردوس دید کی نگرد خاکدان</p></div>
<div class="m2"><p>وانکه به دریا رسید کی طلبد پارگین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بنده ز بی‌دولتی نیست به حضرت مقیم</p></div>
<div class="m2"><p>دیو ز بی‌عصمتی نیست به جنت مکین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شاید اگر در حرم سگ ندهد آب دست</p></div>
<div class="m2"><p>زیبد اگر در ارم بز نبود میوه چین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر ز درت غایب است جسم طبیعت پذیر</p></div>
<div class="m2"><p>معتکف صدر توست جان طریقت گزین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سیرت یوسف تو راست صورت چاهی مجوی</p></div>
<div class="m2"><p>معنی آدم تو راست صورت چاهی مجوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مهره نگر، گو مباش افعی مردم گزای</p></div>
<div class="m2"><p>نافه طلب، گو مباش آهوی صحرا نشین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کی رسد آلوده‌ای بر در پاکان که حق</p></div>
<div class="m2"><p>بست در آسمان بر رخ دیو لعین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر ره خدمت نجست بنده عجب نی ازانک</p></div>
<div class="m2"><p>گرگ گزیده نخواست چشمهٔ ماء معین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بنده سخن تازه کرد وانچه کهن داشت شست</p></div>
<div class="m2"><p>کان همه خر مهره بود وین همه در ثمین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سنگ در اجزای کان زرد شد آنگاه لعل</p></div>
<div class="m2"><p>نطفه در ارحام خلق مضغه شد آنگه جنین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اول روز اندک است زیب و فر آفتاب</p></div>
<div class="m2"><p>بعد گیا ظاهر است خیل گل و یاسمین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مبتدع و مبدعند بر درت اهل سخن</p></div>
<div class="m2"><p>مبدع این شیوه اوست مبتدعند آن و این</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حاجت گفتار نیست ز آنکه شناسد خرد</p></div>
<div class="m2"><p>سندس خصر از پلاس عبقری از گور دین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گرچه در این فن یکی است او و دگر کس به نام</p></div>
<div class="m2"><p>آن مگس سگ بود وین مگس انگبین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای ملکوت و ملک داعی درگاه تو</p></div>
<div class="m2"><p>ظل خدایی که باد فضل خدایت معین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بارهٔ بخت تو را باد ز جوزا رکاب</p></div>
<div class="m2"><p>مرکب خصم تو را باد نگون‌سار زین</p></div></div>