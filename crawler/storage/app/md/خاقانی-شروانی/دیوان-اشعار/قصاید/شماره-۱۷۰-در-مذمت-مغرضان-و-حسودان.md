---
title: >-
    شمارهٔ ۱۷۰ - در مذمت مغرضان و حسودان
---
# شمارهٔ ۱۷۰ - در مذمت مغرضان و حسودان

<div class="b" id="bn1"><div class="m1"><p>کژ خاطران که عین خطا شد صوابشان</p></div>
<div class="m2"><p>مخراق اهل مخرقه مالک رقابشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلقند پر خلاف و شیاطین مر انس را</p></div>
<div class="m2"><p>ننگند و هم ز ننگ نسوزد شهابشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر باطلند از آنکه پدرشان پدید نیست</p></div>
<div class="m2"><p>وز حق نه آدم است و نه عیسی خطابشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رهبان رهبرند در این عالم و در آن</p></div>
<div class="m2"><p>نه آبشان به کار و نه کاری به آبشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچون خزینه خانهٔ زنبور خشک سال</p></div>
<div class="m2"><p>از باد چشمه چشمه دماغ خرابشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان‌شان گران چو خاک و سر باد سنجشان</p></div>
<div class="m2"><p>بی‌سنگ چون ترازوی یوالحسابشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون قوم نوح خشک نهالان بی‌برند</p></div>
<div class="m2"><p>باد از تنود پیرزنی فتح بابشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابلیس وار پیر و جوانند از آنکه کرد</p></div>
<div class="m2"><p>ابلیس هم به پیرو مصحف خطابشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در مسجدند و ساخته چون مهد کودکان</p></div>
<div class="m2"><p>هم آب خانه در وی و هم جای خوابشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم لوح و هم طویله و ارواح مرده را</p></div>
<div class="m2"><p>اجسام دیو و چهرهٔ آدم نقابشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلشان گسسته نور چو شمع و ثاقشان</p></div>
<div class="m2"><p>دینشان شکسته نام چو اهل حجابشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ایشان ز رشک در تب سرد آن‌گهی مرا</p></div>
<div class="m2"><p>کردند پوستین و نکردم عتابشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هستند از قیاس چو فرسوده هاونی</p></div>
<div class="m2"><p>سر نی و بن همیشه ز سودن خرابشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این شیشه گردنان در این خیمهٔ کبود</p></div>
<div class="m2"><p>بینام چون قرابه به گردن طنابشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زنبور نحل و کرم قزند از نیاز و آز</p></div>
<div class="m2"><p>رنج و وبال حاصل تاب و شتابشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون دهر کس فروبر و ناکس برآورند</p></div>
<div class="m2"><p>ز آن در وفا چو دهر بود انقلابشان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیش از بروتشان نگذشته است و نگذرد</p></div>
<div class="m2"><p>اشعارشان چو دعوت نامستجابشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آب نطقشان که گشاید فقع که هست</p></div>
<div class="m2"><p>افسرده‌تر ز برف دل چون سدابشان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از طبع خشکشان نتوان یافت شعر تر</p></div>
<div class="m2"><p>نیلوفر آرزو که کند از سرابشان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سحر حلال من چو خرافات خود نهند</p></div>
<div class="m2"><p>آری یکی است بولهب و بوترابشان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کورند زیر طشت فلک لاجرم ز دور</p></div>
<div class="m2"><p>بنماید آفتابهٔ زر آفتابشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سرسام جهل دارند این خر جبلتان</p></div>
<div class="m2"><p>وز مطبخ مسیح نیاید جو آبشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جایم فرود خویش کنند و روا بود</p></div>
<div class="m2"><p>نفطند و هم به زیر نشیند گلابشان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون ماهی ارچه کنده زبانند پیش من</p></div>
<div class="m2"><p>چون مار در قفا همه زهر است نابشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا خاطرم خزینهٔ گوگر سرخ شد</p></div>
<div class="m2"><p>چون زیبق است در تب سرد اضطرابشان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ایشان ز رشک در تب سرد آنگهی مرا</p></div>
<div class="m2"><p>کردند پوستین و نکردم عتابشان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ایمه جوابشان چه دهم کز زبان چرخ</p></div>
<div class="m2"><p>موتوا بغیظکم نه بس آید جوابشان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تیغ زبانشان نتواند ببرید موی</p></div>
<div class="m2"><p>گر من فسن نسازم ازین سحر نابشان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وین ناوک ضمیر مرا پر جبرئیل</p></div>
<div class="m2"><p>کرد است بی‌نیاز ز پر عقابشان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دلشان ز میوه‌دار حدیثم خورد غذا</p></div>
<div class="m2"><p>انجیر خور غریب نباشد غرابشان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر نان طلب کنند در من زنند از آنک</p></div>
<div class="m2"><p>بی‌دانهٔ من آب زده است آسیابشان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روباه وار بر پی شیران نهند پی</p></div>
<div class="m2"><p>تا آید از کفلگه گوران کبابشان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر کرده‌اند بیژن جاه مرا به چاه</p></div>
<div class="m2"><p>هم من به آه صبح بسوزم جنابشان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من رستم کمان کشم اندر کمین شب</p></div>
<div class="m2"><p>خوش باد خواب غفلت افراسیابشان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خاقانیا ز غرش بیهوده‌شان مترس</p></div>
<div class="m2"><p>جز آب و نار هیچ ندارد سحابشان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر چهرهٔ عروس معانی مشاطه‌وار</p></div>
<div class="m2"><p>زلف سخن بتاب و ز حسرت بتابشان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ای مالک سعیر بر این راندگان خلد</p></div>
<div class="m2"><p>زحمت مکن که زحمت من بس عذابشان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در هفت دوزخ از چه کنی چار میخشان</p></div>
<div class="m2"><p>ویل لهم عقیلهٔ من بس عقابشان</p></div></div>