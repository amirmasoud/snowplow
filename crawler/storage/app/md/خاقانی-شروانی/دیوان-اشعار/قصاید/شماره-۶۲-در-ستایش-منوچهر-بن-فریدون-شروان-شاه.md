---
title: >-
    شمارهٔ ۶۲ - در ستایش منوچهر بن فریدون شروان شاه
---
# شمارهٔ ۶۲ - در ستایش منوچهر بن فریدون شروان شاه

<div class="b" id="bn1"><div class="m1"><p>می و مشک است که با صبح برآمیخته‌اند</p></div>
<div class="m2"><p>یا بهم زلف و لب یار درآمیخته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح چون خنده گه دوست شده است آتش سرد</p></div>
<div class="m2"><p>آتش سرد به عنبر مگر آمیخته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا نه بی‌سنگ و صدف غالیه سایان فلک</p></div>
<div class="m2"><p>صبح را غالیهٔ تازه‌تر آمیخته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش خوش ساخت فلک غالیه دان از مه نو</p></div>
<div class="m2"><p>بهر آن غالیه کاندر سحر آمیخته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می عیدی نگر و جام صبوحی که مگر</p></div>
<div class="m2"><p>شفق آورده و با صبح بر آمیخته‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقیان ترک فنک عارض و قند ز مژگان</p></div>
<div class="m2"><p>کز رخ و زلف حبش با خزر آمیخته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خال رخسار زره کرده و خط ماه سپر</p></div>
<div class="m2"><p>زلف و رخسار زره با سپر آمیخته‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس یک ماه کلوخ اندازان سنگ دلان</p></div>
<div class="m2"><p>در بلورین قدحی لعل تر آمیخته‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاهدان از پی نقل دل و جان از خط و لب</p></div>
<div class="m2"><p>بس گوارش که ز عود و شکر آمیخته‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشقان از زر رخساره و یاقوت سرشک</p></div>
<div class="m2"><p>بس مفرح که ز یاقوت و زر آمیخته‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماه نو دیدی و در روی مه نو شب عید</p></div>
<div class="m2"><p>لعل می با قدح سیم بر آمیخته‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دم روزه دهن شسته به هفت آب و ز می</p></div>
<div class="m2"><p>هفت تسکین دل غصه خور آمیخته‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ماه نو در شفق و شفقشان می و جام</p></div>
<div class="m2"><p>با دو ماه و دو شفق یک نظر آمیخته‌اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طاس سیمابی مه تافته از پرچم شب</p></div>
<div class="m2"><p>طاس زر با می آتش گهر آمیخته‌اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کرده می راوق از اول شب و بازش به صبوح</p></div>
<div class="m2"><p>با گلاب طبری از طبر آمیخته‌اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>راوُق جا م فرو ریخته از سوخته بید</p></div>
<div class="m2"><p>آب گل گوئی با مُعْصَفَر آمیخته‌اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه با درد سر از بوی خمار شب عید</p></div>
<div class="m2"><p>به صبح از نو رنگی دگر آمیخته‌اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ژاله و صبح بهم یافته کافور و گلاب</p></div>
<div class="m2"><p>زاین و آن داروی هر درد سر آمیخته‌اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه سنگ افشان در آب‌خور عالم خاک</p></div>
<div class="m2"><p>و آگه از زهر که در آب‌خور آمیخته‌اند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از سر بی‌خبری داده ز عشرت خبری</p></div>
<div class="m2"><p>تن و جان را که بهم بی‌خبر آمیخته‌اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه دریاکش و چون دریا سرمست همه</p></div>
<div class="m2"><p>طبع با می چو صدف با گهر آمیخته‌اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خطری کرده و در گنج طرب نقب زده</p></div>
<div class="m2"><p>نقب کران همه ره با خطر آمیخته‌اند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زهره بر چیده چو خورشیدنم هر جرعه</p></div>
<div class="m2"><p>که در آن خاک چنان بی‌خطر آمیخته‌اند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خیک ماند به زن زنگی شش پستان لیک</p></div>
<div class="m2"><p>شیر پستانش به خون جگر آمیخته‌اند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جرعه‌ای کان به زمین داده زکات سر جام</p></div>
<div class="m2"><p>زو حنوط ز می پی سپر آمیخته‌اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مجمر عیدی و آن عود و شکر هست بهم</p></div>
<div class="m2"><p>زحل و زهره که با قرص خور آمیخته‌اند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نکهت کام صراحی چو دم مجمر عید</p></div>
<div class="m2"><p>زو بخور فلک جان شکر آمیخته‌اند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رود سازان همه در کاسهٔ سرها به سماع</p></div>
<div class="m2"><p>شربت جان ز ره کاسه‌گر آمیخته‌اند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پرده در پرده و آهنگ در آهنگ چو مرغ</p></div>
<div class="m2"><p>دم بدم ساخته و دربه در آمیخته‌اند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر بط از هشت زبان گوید و خود ناشنواست</p></div>
<div class="m2"><p>زیبقش گوئی با گوش کر آمیخته‌اند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نای افعی تن و بس بر دهنش بوسه زدند</p></div>
<div class="m2"><p>با تن افعی جان بشر آمیخته‌اند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنگ زاهد سر و دامانش پلاسین لیکن</p></div>
<div class="m2"><p>با پلاسش رگ و پی سر به سر آمیخته‌اند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>محبس دست رباب است شعیف ار چه قوی است</p></div>
<div class="m2"><p>چار طبعش که به انصاف در آمیخته‌اند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خم دف حلقه بگوشی شده چون کاسهٔ یوز</p></div>
<div class="m2"><p>کهو و گورش با شیر نر آمیخته‌اند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>صوت مرغان بدرد چرخ مگر با دم خویش</p></div>
<div class="m2"><p>بانگ کوس ملک تاجور آمیخته‌اند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>راویانند گهر پاش مگر با لب خویش</p></div>
<div class="m2"><p>کف شاهنشه خورشیدفر آمیخته‌اند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خاصگان گوهر بحر دل خاقانی را</p></div>
<div class="m2"><p>با کلاه ملک بحر و بر آمیخته‌اند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چاشنی گیران از چشمهٔ حیوان گوئی</p></div>
<div class="m2"><p>شربت شاه سکندر سیر آمیخته‌اند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مالک ملک جلال الدین کاندر تیغش</p></div>
<div class="m2"><p>آتش و آب بهم بی‌ضررآمیخته‌اند</p></div></div>