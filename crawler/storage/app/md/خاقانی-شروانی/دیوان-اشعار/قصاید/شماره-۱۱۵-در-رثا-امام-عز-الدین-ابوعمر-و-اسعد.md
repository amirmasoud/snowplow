---
title: >-
    شمارهٔ ۱۱۵ - در رثاء امام عز الدین ابوعمر و اسعد
---
# شمارهٔ ۱۱۵ - در رثاء امام عز الدین ابوعمر و اسعد

<div class="b" id="bn1"><div class="m1"><p>کو دلی کانده کسارم بود و بس</p></div>
<div class="m2"><p>از جهان زو بوده‌ام خشنود و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ دیدی کو رباید دانه را</p></div>
<div class="m2"><p>محنت این دل هم چنان بربود و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ز چرخ آبگون نان خواستم</p></div>
<div class="m2"><p>او جگر اجری من فرمود و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرخ بر من عید کرد و هر مهم</p></div>
<div class="m2"><p>ماه نوصاع تهی بنمود و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من زکات استان او در قحط سال</p></div>
<div class="m2"><p>هم بصاعی باد می‌پیمود و بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آتش دولت چو در شب ز اختران</p></div>
<div class="m2"><p>گرمیی نادیده دیدم، دود و بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مایهٔ سلوت به غربت شد ز دست</p></div>
<div class="m2"><p>دل زیان افتاد و محنت سود و بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به تبریزم دو چیزم حاصل است</p></div>
<div class="m2"><p>نیم نان و آب مهران رود و بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیر خاک آساید آن کز تخم ماست</p></div>
<div class="m2"><p>تخم هم در زیر خاک آسود و بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بروید تخم محنت‌ها کشد</p></div>
<div class="m2"><p>محنت داسش که سر بدرود و بس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آتش از دست فلک سودم به دست</p></div>
<div class="m2"><p>کو به پای غم چو خاکم سود و بس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عودی خاک آتشین اطلس کنم</p></div>
<div class="m2"><p>ز آب خونین کاین مژه پالود و بس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر چه غم فرسودهٔ دوران بدم</p></div>
<div class="m2"><p>مرگ عز الدین مرا فرسود و بس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر سر خاکش خجل بنشست چرخ</p></div>
<div class="m2"><p>نیم رو خاکی و خون آلود و بس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مه به اشک از خاک راه کهکشان</p></div>
<div class="m2"><p>گل گرفت و خاک او اندود وبس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتم ای چرخ این چنین چون کرده‌ای</p></div>
<div class="m2"><p>پس به خون ما توئی ماخوذ و بس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هم ز عذر خود تظلم کرد چرخ</p></div>
<div class="m2"><p>کان تظلم گوش من بشنود و بس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر لباس دین طراز شرع را</p></div>
<div class="m2"><p>لفظ و کلکش بود تار و پود و بس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مهدی دین بود لیکن چون مسیح</p></div>
<div class="m2"><p>بر دل بیمارم او بخشود و بس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جاه و جانی بس به تمکین و حضور</p></div>
<div class="m2"><p>بر تن و جان من او افزود و بس</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر چه در تبریز دارم دوستان</p></div>
<div class="m2"><p>دوستی جانی مرا او بود و بس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بعد از او در خاک تبریزم چکار</p></div>
<div class="m2"><p>کابروی کار من او بود و بس</p></div></div>