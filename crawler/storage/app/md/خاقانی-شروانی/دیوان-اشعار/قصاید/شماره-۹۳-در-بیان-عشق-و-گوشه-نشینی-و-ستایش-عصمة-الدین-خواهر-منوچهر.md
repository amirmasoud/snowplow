---
title: >-
    شمارهٔ ۹۳ - در بیان عشق و گوشه نشینی و ستایش عصمة الدین خواهر منوچهر
---
# شمارهٔ ۹۳ - در بیان عشق و گوشه نشینی و ستایش عصمة الدین خواهر منوچهر

<div class="b" id="bn1"><div class="m1"><p>از همه عالم کران خواهم گزید</p></div>
<div class="m2"><p>عشق دل جویی به جان خواهم گزید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دولت یک روزه در سودای عشق</p></div>
<div class="m2"><p>بر همه ملک جهان خواهم گزید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابی از شبستان وفا</p></div>
<div class="m2"><p>بی‌سپاس آسمان خواهم گزید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم من دریای گوهر هست لیک</p></div>
<div class="m2"><p>گوهری بیرون از آن خواهم گزید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داستان شد عشق مجنون در جهان</p></div>
<div class="m2"><p>از جهان این داستان خواهم گزید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کجا زنبور خانهٔ عاشقی است</p></div>
<div class="m2"><p>جای چون شه در میان خواهم گزید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوست با درد وفا خواهم گرفت</p></div>
<div class="m2"><p>تیغ در خورد میان خواهم گزید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه غدر دوستان از حد گذشت</p></div>
<div class="m2"><p>هم وفای دوستان خواهم گزید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کبک مهرم کز قفس بیرون شوم</p></div>
<div class="m2"><p>هم قفس را آشیان خواهم گزید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با خیال یار ناپیدا هنوز</p></div>
<div class="m2"><p>خلوتا کاندر نهان خواهم گزید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من کنم یاری طلب هرگز مدان</p></div>
<div class="m2"><p>کز طلب کردن کران خواهم گزید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این طلب بی‌خویشتن خواهم نمود</p></div>
<div class="m2"><p>این رطب بی‌استخوان خواهم گزید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نیابم یار باری بر امید</p></div>
<div class="m2"><p>هم نشین غم نشان خواهم گزید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر ز نومیدی شوم مجروح دل</p></div>
<div class="m2"><p>محرمی مرهم رسان خواهم گزید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گوشه‌ای از خلق و کنجی از جهان</p></div>
<div class="m2"><p>بر همه گنج روان خواهم گزید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زیر این روئین دژ زنگار خورد</p></div>
<div class="m2"><p>هر سحر گه هفت خوان خواهم گزید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دیدم این منزل عجب خشک آخور است</p></div>
<div class="m2"><p>از قناعت میزبان خواهم گزید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در بن دژ چون کمین گاه بلاست</p></div>
<div class="m2"><p>از بصیرت دیدبان خواهم گزید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر در این هفت ده قحط وفاست</p></div>
<div class="m2"><p>راه شهرستان جان خواهم گزید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیست در ده جز علف خانه بدان</p></div>
<div class="m2"><p>کز علف قوت روان خواهم گزید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون به بازار جوان مردان رسم</p></div>
<div class="m2"><p>در صف لالان دکان خواهم گزید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر دکان قفل گر خواهم گذشت</p></div>
<div class="m2"><p>قفلی از بهر دهان خواهم گزید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون مرا آفت ز گفتن می‌رسد</p></div>
<div class="m2"><p>بی‌زبانی بر زبان خواهم گزید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر چه گم کردم کلید نطق را</p></div>
<div class="m2"><p>مدح بلقیس زمان خواهم گزید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ورچه آزادم ز بند هر غرض</p></div>
<div class="m2"><p>مهر شاه بانوان خواهم گزید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عصمة الدین شاه مریم آستین</p></div>
<div class="m2"><p>کآستانش بر جنان خواهم گزید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گوهر کان فریدون ملک</p></div>
<div class="m2"><p>کز جوار او مکان خواهم گزید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بارگاهش کعبهٔ ملک است و من</p></div>
<div class="m2"><p>قبله‌گاه از آستان خواهم گزید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آسمان ستر و ستاره رفعت است</p></div>
<div class="m2"><p>رفعتش بر فرقدان خواهم گزید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آسیه توفیق و ساره سیرت است</p></div>
<div class="m2"><p>سیرتش بر انس و جان خواهم گزید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رابعه زهد و زبیده همت است</p></div>
<div class="m2"><p>کزدرش حصن امان خواهم گزید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حرمت از درگاه او خواهم گرفت</p></div>
<div class="m2"><p>گوهر اصلی زکان خواهم گزید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یک سر موی از سگان در گهش</p></div>
<div class="m2"><p>بر هزبر سیستان خواهم گزید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خاک پای خادمانش را به قدر</p></div>
<div class="m2"><p>بر کلاه اردوان خواهم گزید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شاه انجم خادم لالای اوست</p></div>
<div class="m2"><p>خدمت لالاش از آن خواهم گزید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گنج بخشا یک دو حرف از مدح تو</p></div>
<div class="m2"><p>بر سه گنج شایگان خواهم گزید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر به خدمت کم رسم معذور دار</p></div>
<div class="m2"><p>کز پی عنقا نشان خواهم گزید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سرپرستی رنج و خدمت آفت است</p></div>
<div class="m2"><p>من فراق این و آن خواهم گزید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سال‌ها رای ریاضت داشتم</p></div>
<div class="m2"><p>از پس دوری همان خواهم گزید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پیل را مانم که چون جستم ز خواب</p></div>
<div class="m2"><p>صحبت هندوستان خواهم گزید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خفته بودم همتم بیدار کرد</p></div>
<div class="m2"><p>این ریاضت جاودان خواهم گزید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر به زر گویمت مدح، آنم که بت</p></div>
<div class="m2"><p>بر خدای غیب‌دان خواهم گزید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کافرم دان گر مدیح چون توئی</p></div>
<div class="m2"><p>بر امید سوزیان خواهم گزید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در دعای حضرت تو هر سحر</p></div>
<div class="m2"><p>آفرین از قدسیان خواهم گزید</p></div></div>