---
title: >-
    شمارهٔ ۷۴ - قصیده
---
# شمارهٔ ۷۴ - قصیده

<div class="b" id="bn1"><div class="m1"><p>امروز مال و جاه خسان دارند</p></div>
<div class="m2"><p>بازار دهر بوالهوسان دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در غم سرای عاریت از شادی</p></div>
<div class="m2"><p>گر هیچ هست هیچ کسان دارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عزلت گزین ز پیش‌گه گیتی</p></div>
<div class="m2"><p>کان پیشگاه باز پسان دارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیکان عهد را به بدی کردن</p></div>
<div class="m2"><p>عذری بنه که دسترس آن دارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سفلگان نوال طلب کم کن</p></div>
<div class="m2"><p>کایشان دم و بال رسان دارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیرون همه صفا و درون تیره</p></div>
<div class="m2"><p>گویی نهاد آینه سان دارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دولت به اهل جهل دهند آری</p></div>
<div class="m2"><p>خوان مسیح خرمگسان دارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اقلیم، خادمان و زنان بردند</p></div>
<div class="m2"><p>آفاق، خواجگان و خسان دارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاقانیا نفس که زنی خوش زن</p></div>
<div class="m2"><p>کانجا قبول خوش نفسان دارند</p></div></div>