---
title: >-
    شمارهٔ ۱۷۹ - در مدح ابوالمظفر شروان شاه
---
# شمارهٔ ۱۷۹ - در مدح ابوالمظفر شروان شاه

<div class="b" id="bn1"><div class="m1"><p>کوی عشق آمد شد ما برنتابد بیش از این</p></div>
<div class="m2"><p>دامن تر بردن آنجا برنتابد بیش از این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سر بازار عشق از جان و جان گفتن بس است</p></div>
<div class="m2"><p>کاین قدر سرمایه سودا برنتابد بیش از این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر امید کشتن اندر پای وصلش زنده‌ام</p></div>
<div class="m2"><p>پر نیازان را تمنا برنتابد بیش از این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر کویش ببوسیم آستان و بگذریم</p></div>
<div class="m2"><p>کاستان تنگ است ما را برنتابد بیش از این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما به جان مهمان زلف او و او با ما به جنگ</p></div>
<div class="m2"><p>کاین شبستان زحمت ما برنتابد بیش از این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رشتهٔ جان تا دو تا بود انده تن می‌کشید</p></div>
<div class="m2"><p>چون شد اکنون رشته یکتا برنتابد بیش از این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل ز بستان خیال او به بوئی خرم است</p></div>
<div class="m2"><p>مرغ زندانی تماشا برنتابد بیش از این</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با بلورین جام بهر می مدارا کردمی</p></div>
<div class="m2"><p>چون شکسته شد مدارا برنتابد بیش از این</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سرشک خون حشر کردی مکن خاقانیا</p></div>
<div class="m2"><p>عشق سلطان است، غوغا برنتابد بیش از این</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آب ما چون نیست روشن ظلمت ما خاکیان</p></div>
<div class="m2"><p>بارگاه شاه دنیا برنتابد بیش از این</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درد سر دادیم حضرت را و حضرت روح قدس</p></div>
<div class="m2"><p>روح قدسی دردسرها برنتابد بیش از این</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کعبه را یک بار حج فرض است و حضرت کعبه‌وار</p></div>
<div class="m2"><p>حج ما هر هفته عمدا برنتابد بیش از این</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نفس طاها راست یک شب قاب قوسین نزد حق</p></div>
<div class="m2"><p>گر دو گردد نفس طاها برنتابد بیش از این</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شخص انسان را ز حق یک نور عقلانی عطاست</p></div>
<div class="m2"><p>روح ده دانست کاعضا برنتابد بیش از این</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عید هر سالی دوبار آید که آفاق جهان</p></div>
<div class="m2"><p>بستن آذین زیبا برنتابد بیش از این</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن سعادت بخش حضرت، بخش نارد کرد ازآنک</p></div>
<div class="m2"><p>دیو را فردوس ماوی برنتابد بیش از این</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خبث ما را بارگاه قدس دور افکند از آنک</p></div>
<div class="m2"><p>خوک را محراب اقصی برنتابد بیش از این</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ننگ ما زان درگه اعلا برون افتاد از آنک</p></div>
<div class="m2"><p>کعبه پیلان را مفاجا برنتابد بیش از این</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حضرت پاک از چو ما آلودگان آسوده‌اند</p></div>
<div class="m2"><p>جیفه را بحر مصفا برنتابد بیش از این</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شیر هشیار از سگ دیوانه وحشت برنتافت</p></div>
<div class="m2"><p>نور جبهه شور عوا برنتابد بیش از این</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نی عجب گر گاوریشی زرگر گوساله ساز</p></div>
<div class="m2"><p>طبع صاحب کف بیضا برنتابد بیش از این</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گرچه عفریت آورد عرش سبائی نزد جم</p></div>
<div class="m2"><p>دیدنش جمشید والا برنتابد بیش از این</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آری آری با نوای ارغنون اسقفان</p></div>
<div class="m2"><p>بانگ خر سمع مسیحا برنتابد بیش از این</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرچه صهبا را به بید سوخته راوق کنند</p></div>
<div class="m2"><p>بید را کاسات صهبا برنتابد بیش از این</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از در خاقان کجا پیل افکند محمود را</p></div>
<div class="m2"><p>بدره بردن پیل بالا برنتابد بیش از این</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دست چون جوزاش دادی کلک زر چون آفتاب</p></div>
<div class="m2"><p>گنج زر دادن به یغما برنتابد بیش از این</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مشتری هر سال زی برجی رود ما را چو ماه</p></div>
<div class="m2"><p>هر مهی رفتن به جوزا برنتابد بیش از این</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ما شرف داریم و غیری نعمت از درگاه شاه</p></div>
<div class="m2"><p>رشک بردن بهر نعما برنتابد بیش از این</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر ملخ را نیست بر پا موزهٔ زرین سار</p></div>
<div class="m2"><p>ران او رانین دیبا برنتابد بیش از این</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در حضور انعام دیدیم ار بغیبت نیست آن</p></div>
<div class="m2"><p>وام احسان را تقاضا برنتابد بیش از این</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>طفل را گر جده وقت آبله خرما دهد</p></div>
<div class="m2"><p>چون به سرسام است خرما برنتابد بیش از این</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شاه جان بخش است و ما بر شاه جان کرده نثار</p></div>
<div class="m2"><p>آب بفزودن به دریا برنتابد بیش از این</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خسرو مشرق جلال الدین که برق خنجرش</p></div>
<div class="m2"><p>هفت چشم چرخ خضرا برنتابد بیش از این</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ایزد از تیغش پی مالک جحیمی نو کند</p></div>
<div class="m2"><p>کان جحیم ارواح اعدا برنتابد بیش از این</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کاشکی قدرت ز حلمش نوزمینی ساختی</p></div>
<div class="m2"><p>کاین زمین گرزش به تنها برنتابد بیش از این</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وز بن نیزه‌اش سر گاو زمین لرزد از آنک</p></div>
<div class="m2"><p>ذره بار کوه خارا برنتابد بیش از این</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کرم قز میرد ز بانگ رعد و تنین فلک</p></div>
<div class="m2"><p>میرد از کوسش که آوا برنتابد بیش از این</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دولتش را نوعروسی دان که عکس زیورش</p></div>
<div class="m2"><p>دیدهٔ این زال رعنا برنتابد بیش از این</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>طالعش را شهسواری دان که بار هودجش</p></div>
<div class="m2"><p>کوههٔ عرش معلا برنتابد بیش از این</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>رخش همت را ز گردون تنگ می‌بست آفتاب</p></div>
<div class="m2"><p>گفت بس کاین تنگ پهنا برنتابد بیش از این</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا شد اقبالش همای قاف تا قاف جهان</p></div>
<div class="m2"><p>کوه قاف ادبار عنقا برنتابد بیش از این</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بوالمظفر حق نواز و خصم باطل پرور است</p></div>
<div class="m2"><p>دور باطل حق تعالی برنتابد بیش از این</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ظل حق است اخستان همتاش مهدی چون نهی</p></div>
<div class="m2"><p>ظل حق فرد است، همتا برنتابد بیش از این</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نام شه زان اول و آخر الف کردند و نون</p></div>
<div class="m2"><p>یعنی اندر ملک طغرا برنتابد بیش از این</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا شد از ابر کرم سودا نشان هر مغز را</p></div>
<div class="m2"><p>کس ز بحر طبع سودا برنتابد بیش از این</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خاک پایش ز آب خضر و باد عیسی بهتر است</p></div>
<div class="m2"><p>قیمت یاقوت حمرا برنتابد بیش از این</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شه سلیمان است و من مرغم مرا خوانده است شاه</p></div>
<div class="m2"><p>دانهٔ مرغان دانا برنتابد بیش از این</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از مثال شه امید مردهٔ من زنده گشت</p></div>
<div class="m2"><p>روح را برهان احیا برنتابد بیش از این</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خط دست شاه دیدم کش معما خواند عقل</p></div>
<div class="m2"><p>عقل را خط معما برنتابد بیش از این</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نوک کلک شاه را حورا به گیسو بسترد</p></div>
<div class="m2"><p>غالیه زلفین حورا برنتابد بیش از این</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>عقل را گفتم چگویی شاه درد سر ز من</p></div>
<div class="m2"><p>برتواند یافت؟ گفتا برنتابد بیش از این</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پس خیال شاه گفت از من یقین بشنو که شاه</p></div>
<div class="m2"><p>گویدت برتابم اما برنتابد بیش از این</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هم چنین از دور عاشق باش و مدحش بیش گوی</p></div>
<div class="m2"><p>دردسر کمتر ده ایرا برنتابد بیش از این</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زحمت آنجا چون توان بردن که برخوان مسیح</p></div>
<div class="m2"><p>خرمگس را صحن حلوا برنتابد بیش از این</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هم به جان شاه کز درگاه شاهان فارغم</p></div>
<div class="m2"><p>حرص را دادن تبرا برنتابد بیش از این</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شاید ار مغز زکام آلود را عذری نهند</p></div>
<div class="m2"><p>کو نسیم مشک‌سا را برنتابد بیش از این</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بر قیاس شاه مشرق کارسلان خان سخاست</p></div>
<div class="m2"><p>دیدن بکتاش و بغرا برنتابد بیش از این</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر امید زعفران کو قوت دل بردهد</p></div>
<div class="m2"><p>معصفر خوردن به سکبا برنتابد بیش از این</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عمر دادم بر امید جاه وحاصل هیچ نی</p></div>
<div class="m2"><p>مشک را دادن به نکبا برنتابد بیش از این</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>من همه همت بر اسباب سفر دارم مرا</p></div>
<div class="m2"><p>در حضر ساز مهیا برنتابد بیش از این</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خاطرم فحل است کو صحرا نورد آمد چو شیر</p></div>
<div class="m2"><p>شیر بستن گربه آسا برنتابد بیش از این</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زخم مهماز و بلای تنگ و آسیب لگام</p></div>
<div class="m2"><p>فحل بر دست توانا برنتابد بیش از این</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>پیل را کز گرمسیر هند بیرون آورند</p></div>
<div class="m2"><p>در خزر بودن به سرما برنتابد بیش از این</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سنقرای را کز خزر با سرد سیر آموخته است</p></div>
<div class="m2"><p>در حبش بردن به گرما برنتابد بیش از این</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>مدح شه چون جابجا منزل به منزل گفتنی است</p></div>
<div class="m2"><p>ماندن مداح یکجا برنتابد بیش از این</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>شه مرا زر داد، گوهر دادمش زر را عوض</p></div>
<div class="m2"><p>آن کرامت را مکافا برنتابد بیش از این</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یک رضای شاه، شاه آمد عروس طبع را</p></div>
<div class="m2"><p>از کرم کابین عذرا برنتابد بیش از این</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تیر چرخ از نیزه وش کلک سپر افکند از آنک</p></div>
<div class="m2"><p>هیچ تیغی نطق هیجا برنتابد بیش از این</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>من به مدح شاه نقبی برده‌ام در گنج غیب</p></div>
<div class="m2"><p>بردن نقب آشکارا برنتابد بیش از این</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کند پایم در حضور اما زبان تیزم به مدح</p></div>
<div class="m2"><p>تیزی شمشیر گویا برنتابد بیش از این</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>از پس تحریر نامه کرده‌ام مبدا به شعر</p></div>
<div class="m2"><p>معجز آوردن به مبدا برنتابد بیش از این</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دادمش تصدیع نثر و می‌دهم ابرام نظم</p></div>
<div class="m2"><p>دانم ابرام مثنا برنتابد بیش از این</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>از سر خجلت مرا چون آینه با آینه</p></div>
<div class="m2"><p>خوی برون دادن به سیما برنتابد بیش از این</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بر بدیهه راندم این منظوم و بستردم قلم</p></div>
<div class="m2"><p>هیچ خاطر وقت انشا برنتابد بیش از این</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چون تجاسر کرد خاطر مختصر کردم سخن</p></div>
<div class="m2"><p>کاین تجاسر سمع اعلا برنتابد بیش از این</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>باد خضرای فلک لشکر گهش کاعلام او</p></div>
<div class="m2"><p>ساحت این هفت غبرا برنتابد بیش از این</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ملک و ملت را به اقبالش تولا باد و بس</p></div>
<div class="m2"><p>کاهل عالم را تولا برنتابد بیش از این</p></div></div>