---
title: >-
    شمارهٔ ۱۹ - در مدح خاقان اکبر شروان شاه منوچهربن فریدون و بستن سد باقلانی و التزام صبح در هر بیت
---
# شمارهٔ ۱۹ - در مدح خاقان اکبر شروان شاه منوچهربن فریدون و بستن سد باقلانی و التزام صبح در هر بیت

<div class="b" id="bn1"><div class="m1"><p>جبههٔ زرین نمود چهرهٔ صبح از نقاب</p></div>
<div class="m2"><p>خندهٔ شب گشت صبح خندهٔ صبح آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزهٔ اختر ببست خندهٔ رخسار صبح</p></div>
<div class="m2"><p>سرمهٔ گیتی بشست گریهٔ چشم سحاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح چو پشت پلنگ کرد هوا را دو رنگ</p></div>
<div class="m2"><p>ماه چو شاخ گوزن روی نمود از حجاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهره برانداخت صبح، زهره برافکند شب</p></div>
<div class="m2"><p>پیکر آفاق گشت غرقهٔ صفاری ناب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مائده سالار صبح نزل سحرگه فکند</p></div>
<div class="m2"><p>از پی جلاب خاص ریخت ز ژاله گلاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح نشینان چو شمع ریخته اشک طرب</p></div>
<div class="m2"><p>اشک فشرده قدح شمع گشاده شراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پنجهٔ ساقی گرفت مرغ صراحی به دام</p></div>
<div class="m2"><p>ز آتش صبح اوفتاد دانهٔ دلها به تاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبح همه جان چو می، می همه صفوت چو روح</p></div>
<div class="m2"><p>جرعه شده خاک بوس خاک ز جرعه خراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ترنجی به صبح ساخته نارنج زر</p></div>
<div class="m2"><p>از پی دست ملک، مالک رق و رقاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صبح سپهر جلال، خسرو موسی سخن</p></div>
<div class="m2"><p>موسی خضر اعتقاد خضر سکندر جناب</p></div></div>