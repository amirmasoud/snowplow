---
title: >-
    شمارهٔ ۱۶۹ - در مدح ملک الوزراء زین‌الدین دستور عراق
---
# شمارهٔ ۱۶۹ - در مدح ملک الوزراء زین‌الدین دستور عراق

<div class="b" id="bn1"><div class="m1"><p>دوش چو سلطان چرخ تافت به مغرب عنان</p></div>
<div class="m2"><p>گشت ز سیر شهاب روی هوا پر سنان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد به گیتی ظلام سایهٔ خاک سیاه</p></div>
<div class="m2"><p>یافت ز انجم فروغ انجمن کهکشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشت چو جنت ز نور قبهٔ چرخ از نجوم</p></div>
<div class="m2"><p>شد چو جهنم به وصف دمهٔ ارض از دخان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شام مشعبد نمود حقهٔ ماه و به لعب</p></div>
<div class="m2"><p>مهرهٔ زرین مهر کرد نهان در دهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سپر زر مهر گشت نهان زیر خاک</p></div>
<div class="m2"><p>ناچخ سیمین ماه کرد پدید آسمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطرد سرخ شفق دست هوا کرد شق</p></div>
<div class="m2"><p>پیکر جرم هلال گشت پدید از میان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راست چو از آینه عکس خیال پری</p></div>
<div class="m2"><p>گاه همی شد پدید، گاه همی شد نهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیدن و نادیدنش بود به نزدیک خلق</p></div>
<div class="m2"><p>گه چو جمال یقین، گه چو خیال گمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز بر ایوان ماه بارگهی بود خوب</p></div>
<div class="m2"><p>ساکن آن خواجهٔ فاضل و نیکو بیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نسخت اسرار غیب دفتر او بر کنار</p></div>
<div class="m2"><p>قاسم ارزاق خلق، خامهٔ او در بنان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وز بر آن بارگاه بزم‌گهی بود خوش</p></div>
<div class="m2"><p>حوروشی اندر آن غیرت حور جنان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرو قد و ماه روی لاله رخ و مشک موی</p></div>
<div class="m2"><p>چنگ زن و باده نوش رقص کن و شعر خوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وز بر آن بزم‌گاه، نوبتی خسروی</p></div>
<div class="m2"><p>همچو قضا کام‌کار، همچو قدر کام ران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خسرو شمشیر و شیر باعث لیل و نهار</p></div>
<div class="m2"><p>والی اوج و حضیض، عامل دریا و کان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز بر آن نوبتی خیمهٔ ترکی که هست</p></div>
<div class="m2"><p>خونی خنجر گزار، صفدر آهن کمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آتشیی کز هوا آب سر تیغ او</p></div>
<div class="m2"><p>گرد برآرد به حکم گاه وبال و قران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وز بر آن خیمه بود خوابگه خواجه‌ای</p></div>
<div class="m2"><p>کوست به تاثیر سعد صورت معنی و جان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مفتی کل علوم، خواجهٔ چرخ و نجوم</p></div>
<div class="m2"><p>صاحب صدر زمان، زیور کون و مکان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وز بر آن خوابگه طارم پیری مسن</p></div>
<div class="m2"><p>همچو امل دوربین، همچو اجل جان ستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برده به هنگام زخم در صف میدان جنگ</p></div>
<div class="m2"><p>حربهٔ هندی او حرمت تیغ یمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گشت ز سیارگان رتبت او بیش از آنک</p></div>
<div class="m2"><p>بام خداوند را اوست به شب پاسبان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدر سپهر کرم، صدر کرام عجم</p></div>
<div class="m2"><p>صاحب سیف و قلم، فخر زمین و زمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شمع هدی زین دین، خواجهٔ روی زمین</p></div>
<div class="m2"><p>مفخر کلک و نگین سرور و صدر جهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>منعم روی زمین کوست به عدل و سخا</p></div>
<div class="m2"><p>چون علی و چون عمر گرد جهان داستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرکم دریا نوال، صفدر بدخواه مال</p></div>
<div class="m2"><p>خواجهٔ گیتی گشای، صاحب خسرو نشان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رایت میمون او وقت ملاقات خصم</p></div>
<div class="m2"><p>بر ظفر آموخته چون علم کاویان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لفظ گهر بار او غیرت ابر بهار</p></div>
<div class="m2"><p>دست زر افشان او طعنهٔ باد خزان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عمر ابد را شده مدت او پیش کار</p></div>
<div class="m2"><p>سر ازل را شده خامهٔ او ترجمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا خبر باس او در ملکوت اوفتاد</p></div>
<div class="m2"><p>سبحهٔ روح الامین نیست مگر الامان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رای صوابش ببین کز مدد نه فلک</p></div>
<div class="m2"><p>خان ختا را نهاد مائدهٔ هفت خوان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای شده بد خواه تو مضطرب از اضطراب</p></div>
<div class="m2"><p>همچو بداندیش تو ممتحن امتحان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وی به صدای صریر خامهٔ جان بخش تو</p></div>
<div class="m2"><p>تاج‌ده اردشیر، تخت نه اردوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بخشش تو چون هوا ز آن همه کس را نصیب</p></div>
<div class="m2"><p>کوشش تو چون قضا زو به همه جا نشان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>قوت حزم تو را کوه به زیر رکاب</p></div>
<div class="m2"><p>سرعت عزم تو را باد به زیر عنان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هم سبب امن را رافت تو کیقباد</p></div>
<div class="m2"><p>هم اثر عدل را رای تو نوشین روان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون رخ و اشک عدوت از شفق شام و صبح</p></div>
<div class="m2"><p>کاشته در باغ چرخ معصفر و زعفران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دشمن تو کی بود با تو برابر به جاه؟</p></div>
<div class="m2"><p>شیر علم کی شود همبر شیر ژیان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خصم اگر برخلاف نقص تو گوید شود</p></div>
<div class="m2"><p>ز آتش دل در دهانش همچو زبانه زبان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خنجر فتنه چو گشت کند در ایام تو</p></div>
<div class="m2"><p>حنجر خصم تو گشت خنجر او را فسان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کرد بسی جستجوی در همه عالم ندید</p></div>
<div class="m2"><p>تازه‌تر از جود تو چشم امل میزبان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پای تو را بوسه داد ز آن سبب آخر زمین</p></div>
<div class="m2"><p>گشت بری از بلا فتنهٔ آخر زمان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کینهٔ عدل تو هست در دل فتنه مدام</p></div>
<div class="m2"><p>هست قدیمی بلی کینهٔ گرگ و شبان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بحر کفا از کرام در همه علام توئی</p></div>
<div class="m2"><p>کاهل هنر را ز توست قاعدهٔ نام و نان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خاصه در این عهد ما کز سبب بخل این</p></div>
<div class="m2"><p>خاصه در این دور ما کز اثر جهل آن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>روی سخا گشته است زردتر از شنبلید</p></div>
<div class="m2"><p>اشک سخن گشته است سرخ‌تر از ارغوان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>لاجرم از عشق نعت وز شعف مدح تو</p></div>
<div class="m2"><p>ز آتش خاطر مراست شعر چو آب روان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>غایت مطلوب من خدمت درگاه توست</p></div>
<div class="m2"><p>ای در تو خلق گشته به روزی ضمان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نیست جهانم بکار بی در میمون تو</p></div>
<div class="m2"><p>ور بودم فی‌المثل عمر در او جاودان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خاک در تو مرا گر نبود دستگیر</p></div>
<div class="m2"><p>خاک ز دست فنا بر سر این خاکدان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بگذرد ار باشدش از تو قبولی به جاه</p></div>
<div class="m2"><p>افضل شیرین سخن بیشکی از فرقدان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا ز شفق وقت شام دامن گردون شود</p></div>
<div class="m2"><p>همچو ز خون روز جنگ دامن بر گستوان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کوکب ناهید باد بر در تو پرده دار</p></div>
<div class="m2"><p>پرچم خورشید باد بر سر تو سایبان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شعلهٔ رای تو باد عاقلهٔ مهر و ماه</p></div>
<div class="m2"><p>فضلهٔ خوان تو باد مائدهٔ انس و جان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>باد مسلم شده کف و بنان تو را</p></div>
<div class="m2"><p>خنجر گوهر نگار، خامهٔ گوهر فشان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جاه تو را مدح گوی عقل و زبان خرد</p></div>
<div class="m2"><p>حکم تو را زیردست دولت و بخت جوان</p></div></div>