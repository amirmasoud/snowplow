---
title: >-
    شمارهٔ ۵۰ - در رثاء امام محمد بن یحیی و حادثهٔ حبس سنجر در فتنهٔ غز
---
# شمارهٔ ۵۰ - در رثاء امام محمد بن یحیی و حادثهٔ حبس سنجر در فتنهٔ غز

<div class="b" id="bn1"><div class="m1"><p>آن مصر مملکت که تو دیدی خراب شد</p></div>
<div class="m2"><p>و آن نیل مکرمت که شنیدی سراب شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو سعادت از تف خذلان زگال گشت</p></div>
<div class="m2"><p>و اکنون بر آن زگال جگرها کباب شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سیل اشک بر سر طوفان واقعه</p></div>
<div class="m2"><p>خوناب قبه قبه به شکل حباب شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چل گز سرشک خون ز برخاک بر گذشت</p></div>
<div class="m2"><p>لابل چهل قدم ز بر ماهتاب شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم پیکر سلامت و هم نقش عافیت</p></div>
<div class="m2"><p>از دیدهٔ نظارگیان در نقاب شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل سرد کن ز دهر که هم دست فتنه گشت</p></div>
<div class="m2"><p>اندیشه کن ز پیل که هم جفت خواب شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایام سست رای و قدر سخت گیر گشت</p></div>
<div class="m2"><p>اوهام کند پای و قدر تیز تاب شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دفع قضا به آه شب کندرو کنید</p></div>
<div class="m2"><p>هر چند بارگیر قضا تیزتاب شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر آتش درشت عذابی است بر نبات</p></div>
<div class="m2"><p>آن آب نرم بین که بر او چون عذاب شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاقل کجا رود؟ که جهان، دار ظلم گشت</p></div>
<div class="m2"><p>نحل از کجا چرد؟ که گیا زهر ناب شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ربع زمین بسان تب ربع برده پیر</p></div>
<div class="m2"><p>از لرزه و هزاهز در اضطراب شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کار جهان و بال جهان دان که بر خدنگ</p></div>
<div class="m2"><p>پر عقاب آفت جان عقاب شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>افلاک را پلاس مصیبت بساط گشت</p></div>
<div class="m2"><p>اجرام را وقایهٔ ظلمت حجاب شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ماتم سرای گشت سپهر چهارمین</p></div>
<div class="m2"><p>روح الامین به تعزیت آفتاب شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از بهر آنکه نامه بر تعزیت شوند</p></div>
<div class="m2"><p>شام و سحر دوپیک کبوتر شتاب شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در ترک تاز فتنه ز عکس خیال خون</p></div>
<div class="m2"><p>کیوان به شکل هندوی اطلس نقاب شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دوش آن زمان که طرهٔ شب شانه کرد چرخ</p></div>
<div class="m2"><p>موی سپید دهر به عنبر خضاب شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بی‌دست ارغنون زن گردون به رنگ و شکل</p></div>
<div class="m2"><p>شب موی گشت و ماه کمانچهٔ رباب شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دیدم صف ملائکهٔ چرخ نوحه‌گر</p></div>
<div class="m2"><p>چندان که آن خطیب سحر در خطاب شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفتم به گوش صبح که این چشم زخم چیست</p></div>
<div class="m2"><p>کاشکال و حال چرخ چنین ناصواب شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صبح آه آتشین ز جگر برکشید و گفت</p></div>
<div class="m2"><p>دردا که کارهای خراسان ز آب شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گردون سر محمد یحیی به باد داد</p></div>
<div class="m2"><p>محنت نصیب سنجر مالک رقاب شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از حبس این خدیو، خلیفه دریغ خورد</p></div>
<div class="m2"><p>وز قتل آن امام، پیمبر مصاب شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدعت ز روی حادثه پشت هدی شکست</p></div>
<div class="m2"><p>شیطان خلاف قاعده رجم شهاب شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای آفتاب حربهٔ زرین مکش که باز</p></div>
<div class="m2"><p>شمشیر سنجری ز قضا در قراب شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وی مشتری ردا بنه از سر که طیلسان</p></div>
<div class="m2"><p>در گردن محمد یحیی طناب شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای آدم الغیاث که از بعد این خلف</p></div>
<div class="m2"><p>دار الخلافهٔ تو خراب و یباب شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای عندلیب گلشن دین زار نال زار</p></div>
<div class="m2"><p>کز شاخ شرع طوطی حاضر جواب شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای ذوالفقار دست هدی زنگ گیر، زنگ</p></div>
<div class="m2"><p>کن بوتراب علم به زیر تراب شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خاقانیا وفا مطلب ز اهل عصر از آنک</p></div>
<div class="m2"><p>در تنگنای دهر وفا تنگیاب شد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن کعبه وفا که خراسانش نام بود</p></div>
<div class="m2"><p>اکنون به پای پیل حوادث خراب شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عزمت که زی جناب خراسان درست بود</p></div>
<div class="m2"><p>برهم شکن که بوی امان ز آن جناب شد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر طاق نه حدیث سفر ز آنکه روزگار</p></div>
<div class="m2"><p>چون طالع تو نامزد انقلاب شد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در حبس گاه شروان با درد دل بساز</p></div>
<div class="m2"><p>کان درد راه توشهٔ یوم الحساب شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گل در میان کوره بسی درد سر کشید</p></div>
<div class="m2"><p>تا بهر دفع دردسر آخر گلاب شد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از چاه دولت آب کشیدن طمع مدار</p></div>
<div class="m2"><p>کان دلوها درید و رسن‌ها ز تاب شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دولت به روزگار تواند اثر نمود</p></div>
<div class="m2"><p>حصرم به چار ماه تواند شراب شد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فتح سعادت از سر عزلت برآیدت</p></div>
<div class="m2"><p>کوکشت زرد عمر تو را فتح باب شد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عقل از برات عزلت، صاحب خراج گشت</p></div>
<div class="m2"><p>ابر از زکات دریا صاحب نصاب شد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سیمرغ را خلیفهٔ مرغان نهاده‌اند</p></div>
<div class="m2"><p>هر چند هم لباس خلیفهٔ غراب شد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>معجز عنان کش سخن توست اگر چه دهر</p></div>
<div class="m2"><p>با هر فسرده‌ای به وفا هم رکاب شد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اول به ناقصان نگرد دهر کز نخست</p></div>
<div class="m2"><p>انگشت کوچک است که جای حساب شد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از طمطراق این گره تر مترس از آنک</p></div>
<div class="m2"><p>باد است کو دهل زن خیل سحاب شد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر قصر عقل نام تو خیر الطیور گشت</p></div>
<div class="m2"><p>در تیه جهل خصم تو شر الدواب شد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گفتی که یارب از کف آزم خلاص ده</p></div>
<div class="m2"><p>آمین چه می‌کنی که دعا مستجاب شد</p></div></div>