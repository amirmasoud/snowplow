---
title: >-
    شمارهٔ ۸۲ - این قصیدهٔ به نام کنز الرکاز است و خاقانی آن را در ستایش پیغمبر اکرم و در جوار تربت مقدس آن حضرت سروده است
---
# شمارهٔ ۸۲ - این قصیدهٔ به نام کنز الرکاز است و خاقانی آن را در ستایش پیغمبر اکرم و در جوار تربت مقدس آن حضرت سروده است

<div class="b" id="bn1"><div class="m1"><p>مقصد اینجاست ندای طلب اینجا شنوند</p></div>
<div class="m2"><p>بختیان را ز جرس صبح‌دم آوا شنوند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عارفان نظری را فدی اینجا خواهند</p></div>
<div class="m2"><p>هاتفان سحری را ندی اینجا شنوند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاکیان را ز دل گرم روان آتش عشق</p></div>
<div class="m2"><p>باد سرد از سر خوناب سویدا شنوند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه سگ‌جان و چو سگ ناله کنانند به صبح</p></div>
<div class="m2"><p>صبح دم نالهٔ سگ بین که چه پیدا شنوند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک پر سبحهٔ قرا شود از اشک نیاز</p></div>
<div class="m2"><p>وز دل خاک همان نالهٔ قرا شنوند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک اگر گرید و نالد چه عجب کاتش را</p></div>
<div class="m2"><p>بانگ گریه ز دل صخرهٔ صما شنوند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گریه آن گریه که از دیدهٔ آتش بینند</p></div>
<div class="m2"><p>ناله آن ناله که از سینهٔ خارا شنوند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بلرزد علم صبح و بنالد دم کوس</p></div>
<div class="m2"><p>کوه را نالهٔ تب لرزه چو دریا شنوند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبح گلفام شد ارواح طلب تا نگرند</p></div>
<div class="m2"><p>کوس گلبام زد ابدال بگو تا شنوند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه در پردهٔ شب راز دل عشاق است</p></div>
<div class="m2"><p>کان نفس جز به قیامت نه همانا شنوند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبح شد هدهد جاسوس کز او وا پرسند</p></div>
<div class="m2"><p>کوس شد طوطی غماز کز او وا شنوند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون به پای علم روز، سر شب ببرند</p></div>
<div class="m2"><p>چه عجب کز دم مرغ آه دریغا شنوند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کشته شد دیو به پای علم لشکر حاج</p></div>
<div class="m2"><p>شاید ار تهنیت از کوس مفاجا شنوند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کوس حاج است که دیو از فزعش گردد کر</p></div>
<div class="m2"><p>زو چو کرنای سلیمان دم عنقا شنوند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یارب این کوس چه هاروت فن و زهره نواست</p></div>
<div class="m2"><p>که ز یک پرده صد الحانش به عمدا شنوند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه کند کوس که امروز قیامت نکند</p></div>
<div class="m2"><p>بند آرد نفس صور که فردا شنوند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کوس را بین خم ایوان سلیمان که در او</p></div>
<div class="m2"><p>لحن داود به آهنگ دل آرا شنوند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کوس چون صومعهٔ‌پیر ششم چرخ کز او</p></div>
<div class="m2"><p>بانگ شش دانهٔ تسبیح ثریا شنوند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کوس ماند به کمان فلک اما عجب آنک</p></div>
<div class="m2"><p>زو صریر قلم تیر به جوزا شنوند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کوس را دل نی و دردی نه، چرانالد زار</p></div>
<div class="m2"><p>نالهٔ زار ز درد دل دروا شنوند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کوس چون مار شده حلقه و کو بند سرش</p></div>
<div class="m2"><p>بانگ آن کوفتن از کعبه به صنعا شنوند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخت سر کوفته دارندش و او نالد زار</p></div>
<div class="m2"><p>نالهٔ مرد ز سرکوبهٔ اعدا شنوند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خم کوس است که ماه نو ذیحجه نمود</p></div>
<div class="m2"><p>گر ز مه لحن خوش زهرهٔ زهرا شنوند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خود فلک خواهد تا چنبر این کوس شود</p></div>
<div class="m2"><p>تا صداش از حبل‌الرحمهٔ بطحا شنوند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر دم چنبر چو بین که شنودند خوش است</p></div>
<div class="m2"><p>پس دم آن خوش تر کز چنبر مینا شنوند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از پی حرمت کعبه چه عجب گر پس از این</p></div>
<div class="m2"><p>بانگ دق الکوس از گنبد خضرا شنوند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مشتری قرعهٔ توفیق زند بر ره حاج</p></div>
<div class="m2"><p>بانگ آن قرعه بر این رقعهٔ غبرا شنوند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عرشیان بانگ وللله علی الناس زنند</p></div>
<div class="m2"><p>پاسخ از خلق سمعنا و اطعنا شنوند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از سر و پای در آیند سراپا به نیاز</p></div>
<div class="m2"><p>تا تعال از ملک العرش تعالی شنوند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>روضه روضه همه ره باغ منور بینند</p></div>
<div class="m2"><p>برکه برکه همه جا آب مصفا شنوند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر سر روضه همه جای تنزه شمرند</p></div>
<div class="m2"><p>بر لب برکه همه جای تماشا شنوند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>انجم ماه وش آمادهٔ حج آمده‌اند</p></div>
<div class="m2"><p>تا خواص از همه لبیک مثنا شنوند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه را نسخهٔ اجزای مناسک در دست</p></div>
<div class="m2"><p>از پی کسب جزا خواندن اجزا شنوند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه صحیفه است فلک هفت ده آیت ز برش</p></div>
<div class="m2"><p>عاشقان این همه از سورهٔ سودا شنوند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه صحیفه که به ده بند یکایک بستند</p></div>
<div class="m2"><p>تا نه بس دیر چو سی پاره مجزا شنوند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خام پوشند و همه اطلس پخته شمرند</p></div>
<div class="m2"><p>زهر نوشند و همه بانگ هنیا شنوند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زندگیشان به حق و نام بر ارواح چراست</p></div>
<div class="m2"><p>کبشان ابر دهد لاف ز سقا شنوند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گنج پروردهٔ فقرند و کم کم شده لیک</p></div>
<div class="m2"><p>گم گم گنج سرا پردهٔ بالا شنوند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فقر نیکوست به رنگ ارچه به آواز بد است</p></div>
<div class="m2"><p>عامه زین رنگ هم آواز تبرا شنوند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شبه طاووس شمر فقر که طاوسان را</p></div>
<div class="m2"><p>رنگ زیباست گر آواز نه زیبا شنوند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سفر کعبه نمودار ره آخرت است</p></div>
<div class="m2"><p>گر چه رمز رهش از صورت دیبا شنوند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جان معنی است باسم صوری داده برون</p></div>
<div class="m2"><p>خاصگان معنی و عامان همه اسما شنوند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کعبه را نام به میدانگه عام عرفات</p></div>
<div class="m2"><p>حجرهٔ خاص جهان داور دارا شنوند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عابدان نعره برآرند به میدانگه از آنک</p></div>
<div class="m2"><p>نعرهٔ شیر دلان در صف هیجا شنوند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عارفان خامش و سر بر سر زانو چو ملخ</p></div>
<div class="m2"><p>نه چو زنبور کز او شورش و غوغا شنوند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ساربانا به وفا بر تو که تعجیل نمای</p></div>
<div class="m2"><p>کز وفای تو ز من شکر موفا شنوند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>حاش لله اگر امسال ز حج وامانم</p></div>
<div class="m2"><p>نز قصور من و تقصیر تو حاشا شنوند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دوستان یافته میقات و شده زی عرفات</p></div>
<div class="m2"><p>من به فید و ز من آوازه به بطحا شنوند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هیچ اگر سایه پذیرد منم آن سایهٔ هیچ</p></div>
<div class="m2"><p>که مرا نام نه در دفتر اشیا شنوند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ها و ها باشد اگر محمل ما سازی و هم</p></div>
<div class="m2"><p>برسانیم بکم زانکه ز من ها شنوند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر در کعبه که بیت الله موجودات است</p></div>
<div class="m2"><p>که مباهات امم زان در والا شنوند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بار عام است و در کعبه گشاده است کز او</p></div>
<div class="m2"><p>خاصگان بانگ در جنت ماوا شنوند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پس چو رضوان در جنات گشاید ملکان</p></div>
<div class="m2"><p>بانگ حلقه زدن کعبهٔ علیا شنوند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زان کلیدی که نبی نزد بنی‌شیبه سپرد</p></div>
<div class="m2"><p>بانگ پر ملک و زیور حورا شنوند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چون جرس دار نجیبان ره یثرب سپرند</p></div>
<div class="m2"><p>ساربان را همه الحان، جرس آسا شنوند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در فلک صوت جرس زنگل نباشان است</p></div>
<div class="m2"><p>که خروشیدنش از دخمهٔ دارا شنوند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به سلام آمدگان حرم مصطفوی</p></div>
<div class="m2"><p>ادخلوها به سلام از حرم آوا شنوند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>النبی النبی آرند خلایق به زبان</p></div>
<div class="m2"><p>امتی امتی از روضهٔ غرا شنوند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از صریر در او چار ملایک به سه بعد</p></div>
<div class="m2"><p>پنج هنگام دوم صور به یک جا شنوند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بر در مرقد سلطان هدی ز ابلق چرخ</p></div>
<div class="m2"><p>مرکب داشته را نالهٔ هرا شنوند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خود جنیبت به درش داشته بینند براق</p></div>
<div class="m2"><p>کز صهیلش نفس روح معلا شنوند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>موسی استاده و گم کرده ز دهشت نعلین</p></div>
<div class="m2"><p>ارنی گفتنش از هبر تجلا شنوند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بهر وایافتن گم شده نعلین کلیم</p></div>
<div class="m2"><p>والضحی خواندن خضر از در طاها شنوند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بنده خاقانی و نعت سر بالین رسول</p></div>
<div class="m2"><p>تاش تحسین ز ملک در صف اعلی شنوند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>فخر من بنده ز خاک در احمد بینند</p></div>
<div class="m2"><p>لاف دریا ز دم عنبر سارا شنوند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نعت صدر نبوی که به غربت گویم</p></div>
<div class="m2"><p>بانگ کوس ملکی به که به صحرا شنوند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نکنم مدح که من مرثیه گوی کرمم</p></div>
<div class="m2"><p>چون کرم مرد ز من بانگ معزا شنوند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زنده کردم سخن ار شاکر من شد چه عجب</p></div>
<div class="m2"><p>که ز عازر صفت شکر مسیحا شنوند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شاید ار لب به حدیث قدما نگشایند</p></div>
<div class="m2"><p>ناقدانی که ادای سخن ما شنوند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>آب هر آهن و سنگ ار بشود نیست عجب</p></div>
<div class="m2"><p>که دم آتش طور از ید بیضا شنوند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شاعران حیض حسد یافته چون خرگوشند</p></div>
<div class="m2"><p>تا ز من شیر دلان نکتهٔ عذرا شنوند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خصم سگدل ز حسد نالد و چون جبهت ماه</p></div>
<div class="m2"><p>نور بی‌صرفه دهد وع وع عوا شنوند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>از سر خامه کنم معجزه انشا، به خدای</p></div>
<div class="m2"><p>گر چنین معجزه بینند سران یا شنوند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>راویان کیت انشای من انشاد کنند</p></div>
<div class="m2"><p>بارک الله همه بر صاحب انشا شنوند</p></div></div>