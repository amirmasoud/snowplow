---
title: >-
    در وصف بغداد
---
# در وصف بغداد

<div class="b" id="bn1"><div class="m1"><p>امشرب الخضر ماء بغداد</p></div>
<div class="m2"><p>او نار موسی لقاء بغداد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوثرنا دجلة و جنتنا الکرخ</p></div>
<div class="m2"><p>و طوبی هواء بغداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و قل لمصر بذکر مصراتت</p></div>
<div class="m2"><p>فما لمصر سناء بغداد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تالله للنیل صفو دجلة لا</p></div>
<div class="m2"><p>ولا لمصر صفاء بغداد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیهات این استقال مصرکم</p></div>
<div class="m2"><p>و این این اعتلاء بغداد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرتک مصر بقاهرة</p></div>
<div class="m2"><p>قاهرها کبریاء بغداد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نادتک بغداد فانها رغبا</p></div>
<div class="m2"><p>ینسیک مصرا نداء بغداد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فامس بغداد یومها و کذا</p></div>
<div class="m2"><p>خمیسها اربعاء بغداد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امدح بغداد ثم احبسها</p></div>
<div class="m2"><p>مصدا و هذا هجاء بغداد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وابتغی من لئام مصر سنا</p></div>
<div class="m2"><p>و انجمی اسخیاء بغداد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و میم مصر اذل من الف</p></div>
<div class="m2"><p>الوصل ادلاح باء بغداد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و هذه الاحرف الثلثة لی</p></div>
<div class="m2"><p>ماب خیر فناء بغداد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تبت یدا من یذم تربتها</p></div>
<div class="m2"><p>فتبت ذا بناء بغداد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مسکه روح الجنان تمسکه</p></div>
<div class="m2"><p>ذالمسک لابل رخاء بغداد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قبحا لمن قال لاسخاء لها</p></div>
<div class="m2"><p>فجاد ربعی سخاء بغداد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اف لمن قال لا وفاء بها</p></div>
<div class="m2"><p>فمد ضیفی وفاء بغداد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ان غاض ماء السخاء عندکم</p></div>
<div class="m2"><p>لاباس فالورد ماء بغداد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>والعرش مرآت کل ذی فکر</p></div>
<div class="m2"><p>فیه تجلی رواء بغداد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سئلتنی عن بناء بیضتها</p></div>
<div class="m2"><p>فاسمع فنفسی فداء بغداد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>الجن من قبل آدم اعتقلت</p></div>
<div class="m2"><p>طیبا و روض عراء بغداد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فلقیت روضتها لمرتعه</p></div>
<div class="m2"><p>بغدادها ابتداء بغداد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>و آدم استنزلته همته</p></div>
<div class="m2"><p>لما اتاه رجاء بغداد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فکان لما هوی بمهبطه</p></div>
<div class="m2"><p>اهوی هواه ابتغاء بغداد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اقسم بالله ان فی جلدی</p></div>
<div class="m2"><p>روضة خلد غناء بغداد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ادویة الهند جل ادویة</p></div>
<div class="m2"><p>و خیرها هند باء بغداد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یرکض خیل المنی بعرصتها</p></div>
<div class="m2"><p>فلی یرود هباء بغداد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ابناء دهری عبیده و کذا</p></div>
<div class="m2"><p>بنات فکری اماء بغداد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کنت ربیعا و حاجنی لهبی</p></div>
<div class="m2"><p>و ربع لهوی جناء بغداد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صرت خریفا و من لظی کبدی</p></div>
<div class="m2"><p>یحول صیفا شتاء بغداد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یا قبح شروان خذ کتابی‌ها</p></div>
<div class="m2"><p>واحمل ففیه ثناء بغداد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یلثمه الدهر حین اختمه</p></div>
<div class="m2"><p>و فوق ختمی سحاء بغداد</p></div></div>