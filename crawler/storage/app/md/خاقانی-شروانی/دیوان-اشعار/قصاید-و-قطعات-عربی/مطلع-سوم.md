---
title: >-
    مطلع سوم
---
# مطلع سوم

<div class="b" id="bn1"><div class="m1"><p>انی لاخدم ناصح الخلفاء</p></div>
<div class="m2"><p>مرد الائمة خاضع الحنفاء</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتحیة مشفوعة بمحامد</p></div>
<div class="m2"><p>و محامد مقرونة بدعاء</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و تعارف اکبرته بتذکر</p></div>
<div class="m2"><p>و تذکر و شخصة بثناء</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و لدیه لی مشفع من خلقه</p></div>
<div class="m2"><p>قد سرنی لازال فی السراء</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لقبول مدی حبه حرمته</p></div>
<div class="m2"><p>مسودة و حمامة البیضاء</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و قبوله فرشات معارا الامعارا</p></div>
<div class="m2"><p>یسود کحلی کل اماء</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ریح لجمت سلیمان الحجی</p></div>
<div class="m2"><p>تختارها من عاصف و رخاء</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طور کسبعة ابحر من رحله</p></div>
<div class="m2"><p>ذو اربع من امهات هواء</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فلک یدور منه هلال سرجه</p></div>
<div class="m2"><p>یعلوه بدر صادق الالاء</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ذوهمة و بهذا عزکانه</p></div>
<div class="m2"><p>لیل تبرقع من بریق ضحاء</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لما تمسست الخلال حسبته</p></div>
<div class="m2"><p>نوحا کجودی ای علاء</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یلقی کلام‌الله فارة طوره</p></div>
<div class="m2"><p>و یری حبیب‌الله فوق حداء</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ولو استطعت نثره کنوز لالی</p></div>
<div class="m2"><p>لیراعه الغواص فی الداماء</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هو قسورة و دواته صیادة</p></div>
<div class="m2"><p>و ابیح عین المسک للادواء</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عین بصفرتهایری وجه‌المنی</p></div>
<div class="m2"><p>هی تقمع السوداء باصفراء</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مضحاک وجه وجه کل مطالب</p></div>
<div class="m2"><p>مسقام عین عین کل دواء</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عین کعین الشمس بالیرقان بل</p></div>
<div class="m2"><p>وجه کوجه الماء للغرباء</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لطمت ید الضراب سنة وجهها</p></div>
<div class="m2"><p>فبدالها حائلا عدواء</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرموقة الافاق بل مرفوقة</p></div>
<div class="m2"><p>الاخلاق بل مخلوقة الاضواء</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جوالة البلدان بل قیالة الحزان</p></div>
<div class="m2"><p>بل ختالة الاراء</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جرح الشهود و عدل دیوان القضا</p></div>
<div class="m2"><p>اقضی القضاة و اشفع الشفعاء</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عمر الیهود لها و لون غیارهم</p></div>
<div class="m2"><p>لکن مسیح العهد فی الاحیاء</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عمرت بهدم الفضل عنوان الهوی</p></div>
<div class="m2"><p>هدم العقول عمارة الاهواء</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جرم کجمر جامد مسائلتی</p></div>
<div class="m2"><p>یزکی به قندیل کل رخاء</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فالجمر یخمدلا یلوح ضیاؤه</p></div>
<div class="m2"><p>و لها خمود فی جلوة رضیاء</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خمر السعتر یری رخیصا شعره</p></div>
<div class="m2"><p>فی خمرة کالمسک ذات علاء</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فکانما کماء الحار بعینه</p></div>
<div class="m2"><p>اضحی بساطا خامد الاجزاء</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شرق من عرش احبها فاحبها</p></div>
<div class="m2"><p>فانی بعرش فارک رعناء</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عرش مطلقة الرجال تعوذت</p></div>
<div class="m2"><p>سجن‌الیدین و ساق کل نساء</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سرقت عقول الناس فی حربه</p></div>
<div class="m2"><p>بالضرب ثم القطع للاشلاء</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نبهتها بالشهب فی افلاکها</p></div>
<div class="m2"><p>هذا نوا الیل علی الحرباء</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شکل المجن قلب مجن ذوالغنی</p></div>
<div class="m2"><p>کیلا یضاف بسهم کل جفاء</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لکن مجن القلب لا یحمی اذا</p></div>
<div class="m2"><p>قلب المحن علیه قلب قضاء</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جرم صغیر شانه متعظم</p></div>
<div class="m2"><p>کالقلب فی صغر و عظم دهاء</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نور جماد ساکن متقابس</p></div>
<div class="m2"><p>کالظل وان اخذه ببناء</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سهل یمینها و صعب بنیلها</p></div>
<div class="m2"><p>امن الکتاب بمعرة العظماء</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عقدت علی ساق الحمام صغارها</p></div>
<div class="m2"><p>لکن یسهل اصعب الاشیاء</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ما هذه العین التی عاینتها</p></div>
<div class="m2"><p>اخت النهی ابنت شمس سماء</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>لابل ابوالفضل المغیث بعیشه</p></div>
<div class="m2"><p>حتی یعد عدای ابوالوضاء</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دع کیسه مجهولة هو عسجد</p></div>
<div class="m2"><p>شبیه الکواکب و اسمه الجوزاء</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مبدا عنصره اصفهان عقوده</p></div>
<div class="m2"><p>احدی و عین عقوده اعطاء</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>روحت مهجة باصفهان بمدحتی</p></div>
<div class="m2"><p>اضعاف ما قدلی بهجاء</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کتب الخلیفة للکلام و سیدی</p></div>
<div class="m2"><p>سلطان تاج العلم فی الاکفاء</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اهدی له بذی الخلافة اسودا</p></div>
<div class="m2"><p>و سواد بعض الذی للخلفاء</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>قرضته بقصیدة الفیته</p></div>
<div class="m2"><p>والخیر فرطنی بحرف الیاء</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یعنی له التقدیم کالالف التی</p></div>
<div class="m2"><p>قفیتها و اتی ابوالنقطاء</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هذة القصیدة عصبته شعراتی</p></div>
<div class="m2"><p>و بدت لرازی حیضة الشعرا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ارایت حیض لرایت شبابها</p></div>
<div class="m2"><p>جوف الاسود فی سواد الهیجاء</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اسد السماء اذا طال ذراعه</p></div>
<div class="m2"><p>فصرت لجبهته برا العوجاء</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>قلمی کمنقار الحمام براسه</p></div>
<div class="m2"><p>هلک الغراب و منطق الببغاء</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>لومسه الطائی یصیر عزمه</p></div>
<div class="m2"><p>صدق الغراب متی الاشیاء</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ضمنت نصف البیت للطائی وها</p></div>
<div class="m2"><p>و سمت باسم الحتری الطائی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اظننت حتی کدت اعرق خجلة</p></div>
<div class="m2"><p>فی نصفه المحرم للرخصاء</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نفسی کتبت و ان افشیتها</p></div>
<div class="m2"><p>من خجلتی تمشی علی استحیاء</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دامت جلال الخیر وافیه الهوی</p></div>
<div class="m2"><p>و وقاه الاله اجل وقاء</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یا فاضل الحرمان بکل موطن</p></div>
<div class="m2"><p>ما طاول الهرمان کل بناء</p></div></div>