---
title: >-
    شمارهٔ ۳۳۹
---
# شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>ای بزم تو فروخته رایات خرمی</p></div>
<div class="m2"><p>در شان عهدت آمده آیات محکمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غایت احاطت و از قوت و شرف</p></div>
<div class="m2"><p>هم جرم آفتابی و هم چرخ اعظمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقت است کز برای هلاک مخالفان</p></div>
<div class="m2"><p>افلاک را کنی به سیاست معلمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آسمان فتح خرامی چو آفتاب</p></div>
<div class="m2"><p>از برج خرمی به سوی چرخ خرمی</p></div></div>