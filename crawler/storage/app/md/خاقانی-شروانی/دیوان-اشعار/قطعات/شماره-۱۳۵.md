---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>نیت من نکوست در حق دوست</p></div>
<div class="m2"><p>دوستان را نیت نکو باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد او نیک من بود چه عجب</p></div>
<div class="m2"><p>زشت من نیز خوب او باشد</p></div></div>