---
title: >-
    شمارهٔ ۳۴۹
---
# شمارهٔ ۳۴۹

<div class="b" id="bn1"><div class="m1"><p>صانعا شکر تو واجب شمرم</p></div>
<div class="m2"><p>که وجود همه ممکن تو کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کائنا من کان خاک در توست</p></div>
<div class="m2"><p>که زخاک این همه کائن تو کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه از وجه عدم عین وجود</p></div>
<div class="m2"><p>نتوان کرد ولیکن تو کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل خاقانی اگر کوه غم است</p></div>
<div class="m2"><p>هم در آن کوه معاون تو کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خزائن نهی اندر نفسش</p></div>
<div class="m2"><p>وز صفا مهر خزائن تو کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر حسودانش مساوی گویند</p></div>
<div class="m2"><p>آن مساویش محاسن تو کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امن و بیم از تو همی دارد و بس</p></div>
<div class="m2"><p>که تو سوزانی و ساکن تو کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور ره امن تو پیش آری هم</p></div>
<div class="m2"><p>در ره بیم هم ایمن تو کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طاعنان خسته دلش می‌دارند</p></div>
<div class="m2"><p>خار در دیدهٔ طاعن تو کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تاج بر فرق محمد تو نهی</p></div>
<div class="m2"><p>خاک بر تارک کاهن تو کنی</p></div></div>