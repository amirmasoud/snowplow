---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>خاقانیا قبول و رد از کردگار دان</p></div>
<div class="m2"><p>زو ترس و بس که ترس تو پا زهر زهر اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوان فرشتگانند آنجا که لطف اوست</p></div>
<div class="m2"><p>مردان، مخنثانند آنجا که قهر اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر حکم را که دوست کند دوستدار باش</p></div>
<div class="m2"><p>مگریز و سر مکش همه شهر شهر اوست</p></div></div>