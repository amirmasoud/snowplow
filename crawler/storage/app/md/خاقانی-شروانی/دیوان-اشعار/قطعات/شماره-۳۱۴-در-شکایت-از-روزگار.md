---
title: >-
    شمارهٔ ۳۱۴ - در شکایت از روزگار
---
# شمارهٔ ۳۱۴ - در شکایت از روزگار

<div class="b" id="bn1"><div class="m1"><p>اهل دلی ز اهل روزگار نیابی</p></div>
<div class="m2"><p>انس طلب چون کنی که یار نیابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دگری ز اتفاق هم‌نفسی یافت</p></div>
<div class="m2"><p>چون تو بجوئی به اختیار نیابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش نفسی نیست بی‌گرانی کامروز</p></div>
<div class="m2"><p>نافهٔ بی ثرب در تتار نیابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آینهٔ خاک تیره کار چه بینی</p></div>
<div class="m2"><p>ز آینهٔ تیره نور کار نیابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز وفا آفتاب زرد گذشته است</p></div>
<div class="m2"><p>شب خوشی از لطف روزگار نیابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقطهٔ کاری کناره کن که زره را</p></div>
<div class="m2"><p>ساز جز از نقطهٔ کنار نیابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر بازار دهر خاک چه بیزی</p></div>
<div class="m2"><p>کآخر ازین خاک جز غبار نیابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دهر همانا که خاکبیزتر از توست</p></div>
<div class="m2"><p>زآنکه دو نقدش به یک عیار نیابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگذر ازین آبگون پلی که فلک راست</p></div>
<div class="m2"><p>کب کرم را در او گذار نیابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قاعده عمر زیر گنبد بی‌آب</p></div>
<div class="m2"><p>گنبد آب است کاستوار نیابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست طمع کفچه چون کنی که به هردم</p></div>
<div class="m2"><p>طعمی ازین چرخ کاسه‌وار نیابی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرخ تهی کز پی فریب تو جنبد</p></div>
<div class="m2"><p>کاسهٔ یوزه است کش قرار نیابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کشت کرم را نه خوشه ماند و نه دانه</p></div>
<div class="m2"><p>کاهی ازین دو به کشت‌زار نیابی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاک جگر تشنه را ز کاس کریمان</p></div>
<div class="m2"><p>از نم جرعه امیدوار نیابی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جرعه بود یادگار کاس و بر این خاک</p></div>
<div class="m2"><p>بوئی از آن جرعه یادگار نیابی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یاد تو خاقانیا ز داد چه سود است</p></div>
<div class="m2"><p>کز ستم دهر زینهار نیابی</p></div></div>