---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>گر کهان مه شدند خاقانی</p></div>
<div class="m2"><p>تو در ایشان به مهتری منگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کهتری را که مهتری باشد</p></div>
<div class="m2"><p>هم بدان چشم کهتری منگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرد شاخی که شد درخت بزرگ</p></div>
<div class="m2"><p>در بزرگیش سرسری منگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر ذلیلی که حق عزیز کند</p></div>
<div class="m2"><p>در عزیزیش منکری منگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاو را چون خدا به بانگ آورد</p></div>
<div class="m2"><p>عمل دست سامری منگر</p></div></div>