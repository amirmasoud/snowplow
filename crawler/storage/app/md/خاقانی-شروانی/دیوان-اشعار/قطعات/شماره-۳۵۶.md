---
title: >-
    شمارهٔ ۳۵۶
---
# شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>می تا خط جام آر به رنگ لب دل‌جوی</p></div>
<div class="m2"><p>کز سبزه خط سبزه برآورد لب جوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که چمن سبز سلب گشت سه لب داشت</p></div>
<div class="m2"><p>یعنی لب جام و لب جوی و لب دل‌جوی</p></div></div>