---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>خواجه بد گویدم معاذ الله</p></div>
<div class="m2"><p>که به بد گفتنش سخن رانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او به ده نوع قدح من خواند</p></div>
<div class="m2"><p>من به ده جنس مدح او خوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او بدی گوید و چنان داند</p></div>
<div class="m2"><p>من نکو گویم و چنین دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه گویم هزار چندان است</p></div>
<div class="m2"><p>وانچه گوید هزار چندانم</p></div></div>