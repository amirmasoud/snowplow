---
title: >-
    شمارهٔ ۳۲۱ - شکرانهٔ صلت اسپهبد کیالواشیر
---
# شمارهٔ ۳۲۱ - شکرانهٔ صلت اسپهبد کیالواشیر

<div class="b" id="bn1"><div class="m1"><p>ای جهان داوری که دوران را</p></div>
<div class="m2"><p>عهد نامهٔ بقا فرستادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی کیان گوهری که کیوان را</p></div>
<div class="m2"><p>مدد از کبریا فرستادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عزم را چند روزه ره به کمین</p></div>
<div class="m2"><p>راه گیر قضا فرستادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش مهدی که پیشگاه هدی است</p></div>
<div class="m2"><p>عدل را پیشوا فرستادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب دین رفته بود از آتش کفر</p></div>
<div class="m2"><p>رفته را باز جا فرستادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقت قدرت سهیل را ز یمن</p></div>
<div class="m2"><p>به سلام سها فرستادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز کین اژدهای رایت را</p></div>
<div class="m2"><p>به مصاف و غزا فرستادی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرکسان را ز چرخ چون گنجشک</p></div>
<div class="m2"><p>در دم اژدها فرستادی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به سم کوه پیکران در رزم</p></div>
<div class="m2"><p>کوه را در هوا فرستادی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آب تیغ کیالواشیری</p></div>
<div class="m2"><p>آتش اندر وغا فرستادی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آخر نام خویش را بر چرخ</p></div>
<div class="m2"><p>بیم نار بلا فرستادی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از سنا برق آتش شمشیر</p></div>
<div class="m2"><p>عرشیان را سنا فرستادی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شررش در کواکب افکندی</p></div>
<div class="m2"><p>دودش اندر سما فرستادی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کوه را زهره آب گشت وببست</p></div>
<div class="m2"><p>کامتحانش از دها فرستادی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهرهٔ آب گشتهٔ کوه است</p></div>
<div class="m2"><p>که ثنا را جزا فرستادی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نی نی آن زر ز نور خلق تو زاد</p></div>
<div class="m2"><p>که به خلق خدا فرستادی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرچه خورشید زاده بود از خاک</p></div>
<div class="m2"><p>هم به خورشید وا فرستادی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اعظم اسپهبدا به خاقانی</p></div>
<div class="m2"><p>گنج خاقان عطا فرستادی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدره‌ها دادی از نهان و کنون</p></div>
<div class="m2"><p>حله‌ها بر ملا فرستادی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چشمه‌ها راندی از مکارم و باز</p></div>
<div class="m2"><p>قلزمی از سخا فرستادی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اسمانی که اختران دادی</p></div>
<div class="m2"><p>مهر و مه بر قفا فرستادی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر زری کافتاب زاد از کان</p></div>
<div class="m2"><p>به رهی بارها فرستادی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پس ازین آفتاب بخشی از آنک</p></div>
<div class="m2"><p>نقد کان را فنا فرستادی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پارم امسال شد به سعی عطات</p></div>
<div class="m2"><p>که مثال رضا فرستادی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جان مصروع شوق را ز مثال</p></div>
<div class="m2"><p>خط حرز و شفا فرستادی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو سه حرف میانهٔ نامت</p></div>
<div class="m2"><p>از قبولم لوا فرستادی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خاطرم مریمی است حامل بکر</p></div>
<div class="m2"><p>که دمیش از صبا فرستادی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مریمی کش هزار و یک درد است</p></div>
<div class="m2"><p>صد هزارش دوا فرستادی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من به جان کشتهٔ هوای توام</p></div>
<div class="m2"><p>کشته را خون بها فرستادی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خون بها گر هزار دینار است</p></div>
<div class="m2"><p>تو دو چندان مرا فرستادی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زین صلت کو قصاص کشتن راست</p></div>
<div class="m2"><p>من شدم زنده تا فرستادی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گنج عرشی گشایمت به زبان</p></div>
<div class="m2"><p>که مرا کیمیا فرستادی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه دزدان گنج من کورند</p></div>
<div class="m2"><p>تا مرا توتیا فرستادی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من نیایش‌گر نیای توام</p></div>
<div class="m2"><p>که صلت چون نیا فرستادی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بخشش تو به قدر همت توست</p></div>
<div class="m2"><p>نه به قدر ثنا فرستادی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هم‌چنین بخش تا چنین گویند</p></div>
<div class="m2"><p>که سزا را سزا فرستادی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فضل و فطنت سپاس‌دار تو اند</p></div>
<div class="m2"><p>کاین عطیت به ما فرستادی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نشنوی آنکه حاسدان گویند</p></div>
<div class="m2"><p>کاین همه زر چرا فرستادی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نفخهٔ روح اول البشر است</p></div>
<div class="m2"><p>که به مردم گیا فرستادی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سال قحط انگبین و شیر بهشت</p></div>
<div class="m2"><p>به لبی ناشتا فرستادی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ماه دی کرم پیله را از قوت</p></div>
<div class="m2"><p>پیل بالا نوا فرستادی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کرم شب‌تاب را شب یلدا</p></div>
<div class="m2"><p>در بن چه ضیا فرستادی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در سراب وحش به نیلوفر</p></div>
<div class="m2"><p>ز ابر همت نما فرستادی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شاه‌باز کلاه گمشده را</p></div>
<div class="m2"><p>در زمستان قبا فرستادی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بد نکردی و خود نکو دانی</p></div>
<div class="m2"><p>کاین نکوئی کجا فرستادی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دانم از جان که را ستودم و باز</p></div>
<div class="m2"><p>دانی احسان که را فرستادی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>افسر زر چو شاه دابشلیم</p></div>
<div class="m2"><p>بر سر بید پا فرستادی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ثانی اسکندری، ارسطو را</p></div>
<div class="m2"><p>گنج بی‌منتها فرستادی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شاه نعمان کفی و نابغه را</p></div>
<div class="m2"><p>زر و فر و بها فرستادی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مصطفی دولتا سوی حسان</p></div>
<div class="m2"><p>خلعه چون مصطفا فرستادی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مرتضی صولتا سوی قنبر</p></div>
<div class="m2"><p>هدیه چون مرتضی فرستادی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>برگشایم در فلک به دعات</p></div>
<div class="m2"><p>که کلید دعا فرستادی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>باش تاج کیان که بر سر چرخ</p></div>
<div class="m2"><p>تاج عز و علا فرستادی</p></div></div>