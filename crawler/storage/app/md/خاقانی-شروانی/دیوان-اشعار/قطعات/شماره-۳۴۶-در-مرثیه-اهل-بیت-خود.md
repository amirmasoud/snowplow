---
title: >-
    شمارهٔ ۳۴۶ - در مرثیهٔ اهل بیت خود
---
# شمارهٔ ۳۴۶ - در مرثیهٔ اهل بیت خود

<div class="b" id="bn1"><div class="m1"><p>چشمهٔ خون ز دلم شیفته‌تر کس را نی</p></div>
<div class="m2"><p>خون شو ای چشم که این سوز جگر کس را نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنم از اشک به زر رشتهٔ خونین ماند</p></div>
<div class="m2"><p>هیچ زر رشته ازین تافته‌تر کس را نی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ کس عمر گرامی نفروشد به عدم</p></div>
<div class="m2"><p>سر این بیع مرا هست اگر کس را نی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد دل بر که کنم عرضه که درمان دلم</p></div>
<div class="m2"><p>کیمیائی است کز او هیچ اثر کس را نی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن جگر تر کن من کو که ز نادیدن او</p></div>
<div class="m2"><p>خشک آخورتر ازین دیدهٔ تر کس را نی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم او بر دل من پردهٔ زنگاری بست</p></div>
<div class="m2"><p>کس چو داند که بر این پرده گذر کس را نی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه و دردا که چراغ من تاریک بمرد</p></div>
<div class="m2"><p>باورم کن که ازین درد بتر کس را نی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غلطم من که چراغی همه کس را میرد</p></div>
<div class="m2"><p>لیک خورشید مرا مرد و دگر کس را نی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل خاقانی ازین درد درون پوست بسوخت</p></div>
<div class="m2"><p>وز برون غرقهٔ خون گشت خبر کس را نی</p></div></div>