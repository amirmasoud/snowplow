---
title: >-
    شمارهٔ ۲۰۱ - در مذمت کیمیاگری و صنعت اکسیر
---
# شمارهٔ ۲۰۱ - در مذمت کیمیاگری و صنعت اکسیر

<div class="b" id="bn1"><div class="m1"><p>علم دین کیمیاست خاقانی</p></div>
<div class="m2"><p>کیمیائی سزای گنج ضمیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مس زنگار خورده داری نفس</p></div>
<div class="m2"><p>از چنین کیمیات نیست گزیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز از این هرچه کیمیا گویند</p></div>
<div class="m2"><p>آن سخن مشنو و مکن تصویر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمل اژدهات پیش آرند</p></div>
<div class="m2"><p>کاب هست اژدهای حلقه پذیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اژدها سر به دم رساند و باز</p></div>
<div class="m2"><p>سر دم اژدها خورد بر خیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مپذیر این هوس که نپذیرند</p></div>
<div class="m2"><p>بهرج قلب ناقدان بصیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چنین جهل علم دین بشناس</p></div>
<div class="m2"><p>که شناسند نافه مشک به سیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اول این امتحان سکندر کرد</p></div>
<div class="m2"><p>از ارسطو که بود خاص وزیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برنیاورد کام تا خوردند</p></div>
<div class="m2"><p>هم سکندر هم ارسطو تشویر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدعت فاضلان منحوس است</p></div>
<div class="m2"><p>این صناعت برای میره و میر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ز خامان خام طبع کنند</p></div>
<div class="m2"><p>مال میر اثیافته تبذیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مدبری را که قاطع ره توست</p></div>
<div class="m2"><p>واصلی خوانی از پی توفیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کید قاطع مگو که واصل ماست</p></div>
<div class="m2"><p>کید چون گردد آفتاب منیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که کند زر چو افتاب از خاک</p></div>
<div class="m2"><p>زحلی کاهنی کند به زحیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کافتاب از پیام خاکی زر</p></div>
<div class="m2"><p>نکند بی‌هزار ساله مسیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آفتاب است کیمیاگر و پس</p></div>
<div class="m2"><p>واصلی صانعی قوی تاثیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کی کند زر میان بوتهٔ خاک</p></div>
<div class="m2"><p>دم او آسمان و بوته اثیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این همه درد سر ز عشق زر است</p></div>
<div class="m2"><p>ورنه روزی ضمان کند تقدیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زر که بیند قراضه چون مه نو</p></div>
<div class="m2"><p>حرص دیوانه بگسلد زنجیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زر خرد بزرگ قیمت را</p></div>
<div class="m2"><p>هست جرمی عظیم و جرم حقیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکنزون الذهب نکردی درس</p></div>
<div class="m2"><p>یوم یحمی نخواندی از تفسیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر زمین هر کجا فلک زده‌ای است</p></div>
<div class="m2"><p>بینوائی به دست فقر اسیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شغل او شاعری است یا تنجیم</p></div>
<div class="m2"><p>هوسش فلسفه است یا اکسیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چیست تنجیم و فسفه تعطیل</p></div>
<div class="m2"><p>چیست اکسیر و شاعری تزویر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کفر و کذب این دو راست خرمن کوب</p></div>
<div class="m2"><p>نحس و فقر آن دو راست دامنگیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در ترازوی شرع و رستهٔ عقل</p></div>
<div class="m2"><p>فلسفه فلس دان و شعر شعیر</p></div></div>