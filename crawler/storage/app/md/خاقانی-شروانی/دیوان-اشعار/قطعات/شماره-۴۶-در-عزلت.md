---
title: >-
    شمارهٔ ۴۶ - در عزلت
---
# شمارهٔ ۴۶ - در عزلت

<div class="b" id="bn1"><div class="m1"><p>شاکرم از عزلتی که فاقه و فقر است</p></div>
<div class="m2"><p>فارغم از دولتی که نعمت و ناز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون ز رگ آرزو براندم و زین روی</p></div>
<div class="m2"><p>رفت ز من آن تبی کز آتش آز است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر قد همت قبای عزله بریدم</p></div>
<div class="m2"><p>گرچه به بالای روزگار دراز است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی جوئی طراز آستی من</p></div>
<div class="m2"><p>نیست مرا آستین چه جای طراز است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دور فلک را به گرد من نرسد وهم</p></div>
<div class="m2"><p>گرچه مهندس نهاد و شعوذه باز است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به صفت کدخدای حجرهٔ رازم</p></div>
<div class="m2"><p>شکل فلک چیست حلقهٔ در راز است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهر نه جای من است بگذرم از وی</p></div>
<div class="m2"><p>مسکن زاغان نه آشیانهٔ باز است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تک و تازم ندامت است که آخر</p></div>
<div class="m2"><p>نیستی است آنچه حاصل تک و تاز است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آقچهٔ زر گر هزار سال بماند</p></div>
<div class="m2"><p>عاقبتش جای هم دهانه گاز است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواه ظلم پاش خواه نور گزین پس</p></div>
<div class="m2"><p>دیدهٔ خاقانی از زمانه فراز است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کار من آن به که این و آن نه طرازند</p></div>
<div class="m2"><p>کانکه مرا آفرید کار طراز است</p></div></div>