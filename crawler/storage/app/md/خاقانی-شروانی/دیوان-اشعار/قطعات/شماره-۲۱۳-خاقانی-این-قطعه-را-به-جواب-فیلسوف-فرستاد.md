---
title: >-
    شمارهٔ ۲۱۳ - خاقانی این قطعه را به جواب فیلسوف فرستاد
---
# شمارهٔ ۲۱۳ - خاقانی این قطعه را به جواب فیلسوف فرستاد

<div class="b" id="bn1"><div class="m1"><p>گنج فضائل افضل ساوی شناس و بس</p></div>
<div class="m2"><p>کز علم مطلق آیت دوران شناسمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>استاد حکمت آمد و شاگرد حکم دین</p></div>
<div class="m2"><p>کز چند فن فلاطن یونان شناسمش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون عقل و جان عزیز و غریب است لاجرم</p></div>
<div class="m2"><p>جاندار عقل و عاقلهٔ جان شناسمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدرش عراقیان چه شناسند کز سخن</p></div>
<div class="m2"><p>چون آفتاب امیر خراسان شناسمش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن زر سرخ را که سیاهی محک شناخت</p></div>
<div class="m2"><p>نه شاهد محک خلف کان شناسمش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلطانش امیر خواند و من بر جهان فضل</p></div>
<div class="m2"><p>سلطان شناسمش نه به سلطان شناسمش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با آنکه مور حوصله و دیو گوهرم</p></div>
<div class="m2"><p>هم مرغ او شوم که سلیمان شناسمش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او خواندم به سخره سلیمان ملک شعر</p></div>
<div class="m2"><p>من جان به صدق، مورچهٔ خوان شناسمش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر هشت حرف افضل ساوی است نزد من</p></div>
<div class="m2"><p>حرزی که هفت هیکل رضوان شناسمش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا عقل را خلیفه کتاب اوست گرچه خضر</p></div>
<div class="m2"><p>پیر من است طفل دبستان شناسمش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او خود مرا حیات ابد داد خضروار</p></div>
<div class="m2"><p>ز آن قطعه‌ای که چشمهٔ حیوان شناسمش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دارم دل و دو دیده، ز اشعار او سه بیت</p></div>
<div class="m2"><p>تا خوانده‌ام چهارم ایشان شناسمش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در خط او چو نقطه و اعراب بنگرم</p></div>
<div class="m2"><p>خال رخ برهنهٔ ایمان شناسمش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر حرف او چو دائرهٔ جزم بشمرم</p></div>
<div class="m2"><p>در گوش عقل حلقهٔ فرمان شناسمش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا ز آبنوس روز و شب آمد دوات او</p></div>
<div class="m2"><p>من روز و شب جهان سخندان شناسمش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا دیدم آن دوات پر از کلک تیغ فعل</p></div>
<div class="m2"><p>زرادگاه رستم دستان شناسمش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کمتر تراشهٔ قلم او عطارد است</p></div>
<div class="m2"><p>زشت آید ار عطارد کیهان شناسمش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نجم زحل سواد دواتش نهم چنانک</p></div>
<div class="m2"><p>جرم سهیل ادیم قلمدان شناسمش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اشعارش از عراق ره آورد می‌برم</p></div>
<div class="m2"><p>که اکسیر گنج خانهٔ شروان شناسمش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر عیش بدگوارم اگر گل شکر دهند</p></div>
<div class="m2"><p>شعرش گوارشی است که به ز آن شناسمش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تفاح جان و گل شکر عقل شعر اوست</p></div>
<div class="m2"><p>کاین دو به ساوه هست سپاهان شناسمش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خود را مثال او نهم از دانش اینت جهل</p></div>
<div class="m2"><p>قطران تیره قطرهٔ باران شناسمش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرچه کشف چو پسته بود سبز و گوژپشت</p></div>
<div class="m2"><p>حاشا که مثل پستهٔ خندان شناسمش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جانم نثار اوست که از عقل همچو عقل</p></div>
<div class="m2"><p>فهرست آفرینش انسان شناسمش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خاقانی از ادیم معالیش قدوه‌ای است</p></div>
<div class="m2"><p>آن قدوه‌ای که قبلهٔ خاقان شناسمش</p></div></div>