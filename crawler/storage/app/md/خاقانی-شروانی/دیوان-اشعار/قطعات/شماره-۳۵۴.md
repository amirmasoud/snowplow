---
title: >-
    شمارهٔ ۳۵۴
---
# شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>صبح کرم و وفا فرو شد</p></div>
<div class="m2"><p>خاقانی ازین دو جنس کم جوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای طلب از کرم فرو بند</p></div>
<div class="m2"><p>دست از صفت وفا فرو شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شو تعزیت کرم همی‌دار</p></div>
<div class="m2"><p>رو مرثیهٔ وفا همی گوی</p></div></div>