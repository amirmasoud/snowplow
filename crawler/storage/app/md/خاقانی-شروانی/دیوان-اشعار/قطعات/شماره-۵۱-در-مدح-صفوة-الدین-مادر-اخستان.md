---
title: >-
    شمارهٔ ۵۱ - در مدح صفوة الدین مادر اخستان
---
# شمارهٔ ۵۱ - در مدح صفوة الدین مادر اخستان

<div class="b" id="bn1"><div class="m1"><p>ای شاه بانوی ایران به هفت جد</p></div>
<div class="m2"><p>اقلیم چارم از تو چو فردوس هشتم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلقیس روزگار توئی کز جلال و قدر</p></div>
<div class="m2"><p>شروان شه از کمال سلیمان دوم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود خاتم بزرگ سلیمان به دست توست</p></div>
<div class="m2"><p>کانگشت کوچک تو چو دریای قلزم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اعدای مار فعل تو را زخم کین تو</p></div>
<div class="m2"><p>سوزنده‌تر ز سوزن دنبال کژدم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا از جمال مهد تو شروان جمال یافت</p></div>
<div class="m2"><p>قحطش همه نعیم و نیازش تنعم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بانوی شرق و غرب توئی بر درت مرا</p></div>
<div class="m2"><p>قصه دمادم است که غصه دمادم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب کرم نماند و به وقت نماز عید</p></div>
<div class="m2"><p>اینک مرا به خاک در تو تیمم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفتند خسروان گهر بخش زیر خاک</p></div>
<div class="m2"><p>از ما نصیبشان رضی الله عنهم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مظلومم از زمانه و محرومم از فلک</p></div>
<div class="m2"><p>ای بانوی الغیاث که جای ترحم است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون آدمم ز جنت ایوان شه برون</p></div>
<div class="m2"><p>بی‌آنکه مرغ همت من صید گندم است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من رانده ارچه از لب عیسی نفس زنم</p></div>
<div class="m2"><p>خوانده کسی است کو خر دجال را دم است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شیر سیه برهنه ز هر زر و زیوری</p></div>
<div class="m2"><p>سگ را قلاده در گلو و طوق در دم است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نامم همای دولت و شهباز حضرت است</p></div>
<div class="m2"><p>نه کرکس فرخجی و نه زاغ تخجم است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سلطان مرا شناسد و دان خلیفه هم</p></div>
<div class="m2"><p>مجهول کس نیم همه معلوم مردم است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نان تهی نه و همه آفاق نام من</p></div>
<div class="m2"><p>گنج روان نه و همه آاق گم گم است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خلی نه آخر از خم تا کی مزاج چرخ</p></div>
<div class="m2"><p>که آنجا مرا نخست قدم بر سر خم است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آگاهی از غلام و براتی که گفته بود</p></div>
<div class="m2"><p>شاه فلک غلام که سلطان انجم است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برد آن برات و بازگرفت این غرامت است</p></div>
<div class="m2"><p>داد آن غلام و باز ستد این تحکم است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من بر امید مهتری ای بانو عمر خویش</p></div>
<div class="m2"><p>اینجا چه گم کنم که غلامی به من گم است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر ناتوان کرم کن و این قصه را بخوان</p></div>
<div class="m2"><p>هرچند خط مزور و کاغذ لهاشم است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیدار باد بخت جوانت که چرخ پیر</p></div>
<div class="m2"><p>در مکتب رضای تو طفل تعلم است</p></div></div>