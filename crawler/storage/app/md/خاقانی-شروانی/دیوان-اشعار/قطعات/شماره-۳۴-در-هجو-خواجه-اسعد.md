---
title: >-
    شمارهٔ ۳۴ - در هجو خواجه اسعد
---
# شمارهٔ ۳۴ - در هجو خواجه اسعد

<div class="b" id="bn1"><div class="m1"><p>خواجه اسعد چو می خورد پیوست</p></div>
<div class="m2"><p>طرفه شکلی شود چو گردد مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پارسا روی هست لیکن نیست</p></div>
<div class="m2"><p>قلتبان شکل نیست لیکن هست</p></div></div>