---
title: >-
    شمارهٔ ۱۸۷ - در تولد دختر خود
---
# شمارهٔ ۱۸۷ - در تولد دختر خود

<div class="b" id="bn1"><div class="m1"><p>یکی دو زایند آبستنان مادر طبع</p></div>
<div class="m2"><p>ز من بزاد به یکباره صدهزار پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکان یکان حبشی چهره و یمانی اصل</p></div>
<div class="m2"><p>همه بلال معانی، همه اویس هنر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یگانهٔ دو سرا و سه وقت و چار ارکان</p></div>
<div class="m2"><p>امیر پنج حس و شش جهات و هفت اختر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا چه نقصان گر جفت من بزاد کنون</p></div>
<div class="m2"><p>به چشم زخم هزاران پسر یکی دختر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که دختری که ازینسان برادران دارد</p></div>
<div class="m2"><p>عروس دهرش خوانند و بانوی کشور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر بمیرد باشد بهشت را خاتون</p></div>
<div class="m2"><p>وگر بماند زیبد مسیح را خواهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگرچه هست بدینسان خداش مرگ دهاد</p></div>
<div class="m2"><p>که گور بهتر داماد و دفن اولی‌تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر نخواندی نعم الختن برو برخوان</p></div>
<div class="m2"><p>وگر ندیدی دفن البنات شو بنگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا به زادن دختر چه خرمی زاید</p></div>
<div class="m2"><p>که  کاش مادر من هم نزادی از مادر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن که زادهٔ خاقانی است دیر زیاد</p></div>
<div class="m2"><p>که آن ز نه فلک آمد نه از چهار گهر</p></div></div>