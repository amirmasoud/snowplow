---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>وبالت نه از سر نهفتن درست</p></div>
<div class="m2"><p>که از گوهر راز سفتن درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگو راست بندیش خاقانیا</p></div>
<div class="m2"><p>همه آفت از راست گفتن درست</p></div></div>