---
title: >-
    شمارهٔ ۶۹ - در مدح نظام الملک قوام الدین
---
# شمارهٔ ۶۹ - در مدح نظام الملک قوام الدین

<div class="b" id="bn1"><div class="m1"><p>کسرای عهد بین که در ایوان نو نشست</p></div>
<div class="m2"><p>خورشید در نطاق شبستان نو نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنقا به باغ بخت و سلیمان به تخت عز</p></div>
<div class="m2"><p>با جاه نو رسید و به امکان نو نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ادریس دین حدیقهٔ فردوس تازه یافت</p></div>
<div class="m2"><p>رضوان ملک بر در بستان نو نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این هفت تاب خانه مشبک شد از دعا</p></div>
<div class="m2"><p>تا شاه در مقرنس ایوان نو نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در طارمی که هست سه وقت اندر او سه عید</p></div>
<div class="m2"><p>با طالع سعید به برهان نو نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخ آن دو قرص زرد و سپید اندر آستین</p></div>
<div class="m2"><p>آمد بر آستانش و بر خوان نو نشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر درگهش که فرق فلک خاک خاک اوست</p></div>
<div class="m2"><p>دهر کهن به پهلوی دربان نو نشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کفش پاسبانش هر سنگ ریزه‌ای</p></div>
<div class="m2"><p>چون گوهری بر افسر سلطان نو نشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در درس دعوت از پی هارونی درش</p></div>
<div class="m2"><p>پیرانه سر فلک به دبستان نو نشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رایش که مشرفی قضا کرد عاقبت</p></div>
<div class="m2"><p>ملک ابد گرفت و به دیوان نو نشست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عکسی ز آخشیج حسامش هوا گرفت</p></div>
<div class="m2"><p>بالای سدره عنصر و ارکان نو نشست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مهر سپهر ملک بماناد کز کفش</p></div>
<div class="m2"><p>بر فرق فرقد افسر احسان نو نشست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگذشت عهد ماتم و عهد بقا رسید</p></div>
<div class="m2"><p>بر کاینات یکسره فرمان نو نشست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جاوید باد کز کرمش جان هر گهر</p></div>
<div class="m2"><p>بر گنج نو برآمد و بر کان نو نشست</p></div></div>