---
title: >-
    شمارهٔ ۳۴۷ - در هجو رشید الدین وطواط
---
# شمارهٔ ۳۴۷ - در هجو رشید الدین وطواط

<div class="b" id="bn1"><div class="m1"><p>رشیدکا ز تهی مغزی و سبک خردی</p></div>
<div class="m2"><p>پری به پوست همی دان که بس گران جانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه شناس قبول از دبور بی‌خبری</p></div>
<div class="m2"><p>گه تمیز قبل از دبر نمی‌دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخنت را نه عبارت لطیف و نه معنی</p></div>
<div class="m2"><p>عروس زشت و حلی دون و لاف لامانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنی به سخره برآمد به بام گلخن و گفت</p></div>
<div class="m2"><p>که دور چشم بد از کاخ من به ویرانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخنت بلخی و معنیش گیر خوارزمی</p></div>
<div class="m2"><p>ز بلخی آخر تفسیر این سخن دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفتمت که هزاران متاع ازین سان هست</p></div>
<div class="m2"><p>کدام حیله کنی تا فروخت بتوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث بوزنه خواندی و رشم گرد ناو</p></div>
<div class="m2"><p>چو طیره گشت کفایت ده خراسانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه گفت بوزنه را گفت: کون دریده زنا</p></div>
<div class="m2"><p>برای رشم فروشیت کو زبان دانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زبان بران زمانه به گشتن‌اند، مگوی</p></div>
<div class="m2"><p>که در زمانه منم هم‌زبان خاقانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سقاطه‌های تو آن است و شعر من این است</p></div>
<div class="m2"><p>به تو چه مانم؟ ویحک به من چه می‌مانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قیاس خویش به من کردن احمقی باشد</p></div>
<div class="m2"><p>که ابن اربدی امروز تو نه حسانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلیل حمق تو طعن تو در سنائی بس</p></div>
<div class="m2"><p>که احمقی است سر کرده‌های شیطانی</p></div></div>