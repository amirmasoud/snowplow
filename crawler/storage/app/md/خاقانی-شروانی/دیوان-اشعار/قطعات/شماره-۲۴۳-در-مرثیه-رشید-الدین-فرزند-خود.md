---
title: >-
    شمارهٔ ۲۴۳ - در مرثیهٔ رشید الدین فرزند خود
---
# شمارهٔ ۲۴۳ - در مرثیهٔ رشید الدین فرزند خود

<div class="b" id="bn1"><div class="m1"><p>پسر داشتم چون بلند آفتابی</p></div>
<div class="m2"><p>ز ناگه به تاری مغاکش سپردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به درد پسر مادرش چون فروشد</p></div>
<div class="m2"><p>به خاک آن تن دردناکش سپردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی بکر چون دختر نعش بودم</p></div>
<div class="m2"><p>به روشن‌دلی چون سماکش سپردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دختر سپردم به داماد گفتم</p></div>
<div class="m2"><p>که گنج زر است این به خاکش سپردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بماندم من و ماند عبد المجیدی</p></div>
<div class="m2"><p>ودیعت به یزدان پاکش سپردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر کس نباشد پناهش به شروان</p></div>
<div class="m2"><p>پناهش بس است آن خداکش سپردم</p></div></div>