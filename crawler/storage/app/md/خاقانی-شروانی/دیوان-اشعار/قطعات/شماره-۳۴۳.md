---
title: >-
    شمارهٔ ۳۴۳
---
# شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>گر از غم خلاصی طلب کردمی</p></div>
<div class="m2"><p>هم از نای و نوشی سبب کردمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا غم ندیم است خاص ارنه من</p></div>
<div class="m2"><p>چو عامان به نوعی طرب کردمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر غم طلاق از دلم بستدی</p></div>
<div class="m2"><p>نکاح بنات العنب کردمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرم دست رفتی لگام ادب</p></div>
<div class="m2"><p>بر این ابلق روز و شب کردمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر کردهٔ چرخ بشمردمی</p></div>
<div class="m2"><p>شمارش سوی دست چپ کردمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلید زبان گر نبودی وبال</p></div>
<div class="m2"><p>کی از خامشی قفل لب کردمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بری‌خوردمی آخر از دست کشت</p></div>
<div class="m2"><p>اگرنه ز مومی رطب کردمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر فضل من ناقص است ارنه من</p></div>
<div class="m2"><p>بر او تکیه‌گاهی عجب کردمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ادب داشتم دولتم برنداشت</p></div>
<div class="m2"><p>ادب کاشکی کم طلب کردمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عصای کلیم ار به دستم بدی</p></div>
<div class="m2"><p>به چوبش ادب را ادب کردمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر در هنرها هنر دیدمی</p></div>
<div class="m2"><p>به خاقانی آن را نسب کردمی</p></div></div>