---
title: >-
    شمارهٔ ۱۱۵ - در مدح مجد الدین افتخار الاسلام
---
# شمارهٔ ۱۱۵ - در مدح مجد الدین افتخار الاسلام

<div class="b" id="bn1"><div class="m1"><p>با آینهٔ ضمیر مخدوم</p></div>
<div class="m2"><p>خواهد که نفس زند نیارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجد الدین افتخار اسلام</p></div>
<div class="m2"><p>که اسلام بدو تفاخر آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحری است نهنگ سار کلکش</p></div>
<div class="m2"><p>کالا گهر از دهن نبارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ظلمت حال خاطر اندوه</p></div>
<div class="m2"><p>با نور خیال او گسارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پر کحل جواهر آیدش چشم</p></div>
<div class="m2"><p>چون بر خط او نظر گمارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل یاد کند فضایل او</p></div>
<div class="m2"><p>چندان که به دست چپ شمارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر یاد محقق مهینه</p></div>
<div class="m2"><p>انگشت کهینه بسته دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر چه حساب گیرد انگشت</p></div>
<div class="m2"><p>کورا ز میان فرو گذارد</p></div></div>