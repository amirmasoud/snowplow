---
title: >-
    شمارهٔ ۲۳۶ - در شکر ایادی شمس الدین رئیس
---
# شمارهٔ ۲۳۶ - در شکر ایادی شمس الدین رئیس

<div class="b" id="bn1"><div class="m1"><p>من که خاقانیم جفای وطن</p></div>
<div class="m2"><p>برده‌ام وز جفا گریخته‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خسان چو سار شور انگیز</p></div>
<div class="m2"><p>چون ملخ بر ملا گریخته‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه بازم هوا گرفته بلی</p></div>
<div class="m2"><p>کز کمین بلا گریخته‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه نه شهباز چه؟ که گنجشکم</p></div>
<div class="m2"><p>کز دم اژدها گریخته‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه آزرده‌ام ز دست خسان</p></div>
<div class="m2"><p>دست بر سر چرا گریخته‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترسم از قهر ناخدا ترسان</p></div>
<div class="m2"><p>لاجرم در خدا گریخته‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از کمین کمان کشان قضا</p></div>
<div class="m2"><p>در حصار رضا گریخته‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من ز ارجیش ز ابر دست رئیس</p></div>
<div class="m2"><p>وقت سیل سخا گریخته‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن نه سیل است چیست طوفان است</p></div>
<div class="m2"><p>پس ز طوفان سرا گریخته‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>الغریق الغریق می‌گویم</p></div>
<div class="m2"><p>ز آن چناند سیل تا گریخته‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر همه کس ز منع بگریزد</p></div>
<div class="m2"><p>منم آن کز عطا گریخته‌ام</p></div></div>