---
title: >-
    شمارهٔ ۲۷۷
---
# شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>بر اصفهان گذشتن من بود یک زمان</p></div>
<div class="m2"><p>در وی شدن همان و برون آمدن همان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر صدر خواستمی اصفهان کنون</p></div>
<div class="m2"><p>چون صدر غایب است چه کرمان چه اصفهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم آسمان به واسطهٔ آفتاب دید</p></div>
<div class="m2"><p>بی‌آفتاب چشم چه بیند در آسمان</p></div></div>