---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>وقت آن است کز این دار فنا درگذریم</p></div>
<div class="m2"><p>کاروان رفته و ما بر سر راه سفریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاد ره هیچ ندانیم چه تدبیر کنیم</p></div>
<div class="m2"><p>سفری دور و دراز است ولی بی‌خبریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پدر و مادر و فرزند و عزیزان رفتند</p></div>
<div class="m2"><p>وه چه ما غافل و مستیم و چه کوته نظریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دم‌بدم می‌گذرند از نظر ما یاران</p></div>
<div class="m2"><p>اینقدر دیده نداریم که بر خود نگریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانه و خانقه و منزل ما زیر زمین</p></div>
<div class="m2"><p>ما به تدبیر سرا ساختن و بام و دریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر همه مملکت و مال جهان جمع کنیم</p></div>
<div class="m2"><p>لیک جز پیرهن گور ز دنیا نبریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خانهٔ اصلی ما گوشهٔ گورستان است</p></div>
<div class="m2"><p>خرم آن روز که این رخت بر آن خانه بریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پادشاها تو کریمی و رحیمی و غفور</p></div>
<div class="m2"><p>دست ما گیر که درماندهٔ بی‌بال و پریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یارب از لطف و کرم عاقبت خاقانی</p></div>
<div class="m2"><p>خیر گردان تو که ما در طلب خواب و خوریم</p></div></div>