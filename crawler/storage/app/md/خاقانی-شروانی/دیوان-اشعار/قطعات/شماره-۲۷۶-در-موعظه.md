---
title: >-
    شمارهٔ ۲۷۶ - در موعظه
---
# شمارهٔ ۲۷۶ - در موعظه

<div class="b" id="bn1"><div class="m1"><p>هر که را من به مهر خواندم دوست</p></div>
<div class="m2"><p>چون دگر کس شناخت شد دشمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه پی دشمنان شود به خلاف</p></div>
<div class="m2"><p>چه دم دوستان خورد به سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواه با دشمن است سر در جیب</p></div>
<div class="m2"><p>خواه با دوست پای در دامن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب و آتش یکی است بر تن مشک</p></div>
<div class="m2"><p>خواه آب آر و خواه آتش زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تنش بوی دشمنی آید</p></div>
<div class="m2"><p>چون شود دوست آشنای دو تن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوست از هر دو تن دو رنگ شود</p></div>
<div class="m2"><p>دل از آن کو دو رنگ شد برکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوست کاول شناخت دشمن و دوست</p></div>
<div class="m2"><p>شد چو عالم دو رنگ در هر فن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه گه از خود هم آیدم غیرت</p></div>
<div class="m2"><p>که بود دوست هم سرا با من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایهٔ خویش هم نهان خواهم</p></div>
<div class="m2"><p>چون شود سرو دوست سایه فکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صد هزار است غیرتم بر دوست</p></div>
<div class="m2"><p>آنچه یک غیرت آیدم بر زن</p></div></div>