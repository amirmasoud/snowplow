---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>سر سخنان نغز خاقانی</p></div>
<div class="m2"><p>از خواجه شنو که علمش او دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تشنه بپرس ارز آب ایرا</p></div>
<div class="m2"><p>ارز او داند که آرزو دارد</p></div></div>