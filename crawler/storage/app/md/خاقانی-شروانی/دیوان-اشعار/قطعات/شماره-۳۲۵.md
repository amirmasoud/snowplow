---
title: >-
    شمارهٔ ۳۲۵
---
# شمارهٔ ۳۲۵

<div class="b" id="bn1"><div class="m1"><p>طبع روشن داشت خاقانی حوادث تیره کرد</p></div>
<div class="m2"><p>ور نکردی خاطر او نور پیوند آمدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کلید خاطرش نشکستی اندر قفل غم</p></div>
<div class="m2"><p>از خزانهٔ غیب لفظش وحی مانند آمدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به اول نستندندی اصل شیرینی ز موم</p></div>
<div class="m2"><p>نخل مومین را رطب شیرین تر از قند آمدی</p></div></div>