---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>مهترا بلبل انسی پس از این</p></div>
<div class="m2"><p>بجز از دست ادب دانه مخور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فی‌المثل تو خود اگر آب خوری</p></div>
<div class="m2"><p>جز ز جوی دل فرزانه مخور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سفر سفره گزین خوان چه مخواه</p></div>
<div class="m2"><p>مرد خوان باش غم خانه مخور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حصه‌ای زین دل آبادتر است</p></div>
<div class="m2"><p>غصهٔ عالم ویرانه مخور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاقل شیر دلی باده مگیر</p></div>
<div class="m2"><p>حض خرگوش به پیمانه مخور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آب آن میوه که روباه خورد</p></div>
<div class="m2"><p>آب کون سگ دیوانه مخور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عارفانه بزی اندر ره شرع</p></div>
<div class="m2"><p>از اباحت دم فرغانه مخور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشنای دل بیگانه شدی</p></div>
<div class="m2"><p>آب و نان از در بیگانه مخور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مادر روزی ار افگانه فکند</p></div>
<div class="m2"><p>غم مبر انده افگانه مخور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آز چون نیست در سفله مزن</p></div>
<div class="m2"><p>موی چون نیست غم شانه مخور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچنین در پی یاران می‌باش</p></div>
<div class="m2"><p>یار یارا زن و بهنانه مخور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتی ار من به معسکر برسم</p></div>
<div class="m2"><p>نان ترکان خورم آن خانه مخور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نان ترکان مخور و بر سر خوان</p></div>
<div class="m2"><p>به ادب نان خور و ترکانه مخور</p></div></div>