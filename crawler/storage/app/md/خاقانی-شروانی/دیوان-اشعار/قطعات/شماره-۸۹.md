---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>امن جستی مجوی خاقانی</p></div>
<div class="m2"><p>کاین مراد از جهان نخواهی یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر افلاس خانهٔ گیتی</p></div>
<div class="m2"><p>کیمیای امان نخواهی یافت</p></div></div>