---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>مهتر قالیان و نور مرند</p></div>
<div class="m2"><p>میلشان جز به سربلندی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو کریمند راست باید گفت</p></div>
<div class="m2"><p>که مرا طبع کژ پسندی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا دل شکسته‌ای بینند</p></div>
<div class="m2"><p>کارشان جز شکسته بندی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیک چون طالعم به صحبتشان</p></div>
<div class="m2"><p>نیست، در دل مرا نژندی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مهذب مراست وان دو نه‌اند</p></div>
<div class="m2"><p>عافیت هست و دردمندی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون مرا سندس است و استبرق</p></div>
<div class="m2"><p>شاید ار قالی مرندی نیست</p></div></div>