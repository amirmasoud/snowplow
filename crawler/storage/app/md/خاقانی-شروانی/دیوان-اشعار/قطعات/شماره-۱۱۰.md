---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>زندگی خفتگی است خاقانی</p></div>
<div class="m2"><p>خفته آگه به یک نفس گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایهنمه کارهای پهن و دراز</p></div>
<div class="m2"><p>تنگ و کوته به یک نفس گردد</p></div></div>