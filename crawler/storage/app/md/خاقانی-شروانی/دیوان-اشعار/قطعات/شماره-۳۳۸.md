---
title: >-
    شمارهٔ ۳۳۸
---
# شمارهٔ ۳۳۸

<div class="b" id="bn1"><div class="m1"><p>دی شبانگه به غلط تا به لب دجله شدم</p></div>
<div class="m2"><p>باجگه دیدم و نظاره بتان حرمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر لب دجله ز بس نوش لب نوش‌لبان</p></div>
<div class="m2"><p>غنچه غنچه شده چون پشت فلک روی زمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نازنینان عرب دیدم و رندان عجم</p></div>
<div class="m2"><p>تشنه‌دل ز آرزو و غرقه تن از محتشمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیری از دور بیامد عجمی زاد و غریب</p></div>
<div class="m2"><p>چشم پوشیده و نالان ز برهنه قدمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهنش خشک و شکفته رخش از ابر مژه</p></div>
<div class="m2"><p>جگرش گرم و فسرده تنش از سرد دمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تشنگی بایه برده به لب دجله فتاد</p></div>
<div class="m2"><p>سست تن مانده و از سست تنی سخت غمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب برداشتن از دجله مگر زور نداشت</p></div>
<div class="m2"><p>که نوان بود ز لرزان تنی و پشت خمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شربتی آب طلب کرد ز ملاحی و گفت</p></div>
<div class="m2"><p>هات یا شیخ ذهیبا حرمی الرقم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیر گفت ای فتی آن زر که ندارم چه دهم</p></div>
<div class="m2"><p>گفت: اخسا قطع الله یمین العجمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آبی از دجله چوبینم که به پیری ندهند</p></div>
<div class="m2"><p>من ز بغداد چه گویم صفت بی‌کرمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌درم لاف ز بغداد مزن خاقانی</p></div>
<div class="m2"><p>گر چه امروز به میزان سخن یک درمی</p></div></div>