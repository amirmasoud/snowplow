---
title: >-
    شمارهٔ ۳۱۱
---
# شمارهٔ ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>تو همه کاخ طرب سازی و خاقانی را</p></div>
<div class="m2"><p>در همه تبریز اندهکده‌ای بینم جای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او بدین یک درهٔ خویش تکلف نکند</p></div>
<div class="m2"><p>تو بدین ششدرهٔ خویش تفاخر منمای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه در هفت فلک خانه یکی دارد و بس</p></div>
<div class="m2"><p>زحل نحس ز من راست به یک جا دو سرای</p></div></div>