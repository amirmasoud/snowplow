---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>هست او سیاه چرده و من هم سپید سر</p></div>
<div class="m2"><p>با یار، من موافقه زین باب می‌کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او بر رخ سیاه، سپیداب می‌کند</p></div>
<div class="m2"><p>من بر سر سپید، سیاه آب می‌کنم</p></div></div>