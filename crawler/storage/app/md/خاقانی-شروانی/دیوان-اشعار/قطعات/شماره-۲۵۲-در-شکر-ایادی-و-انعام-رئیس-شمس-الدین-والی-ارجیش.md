---
title: >-
    شمارهٔ ۲۵۲ - در شکر ایادی و انعام رئیس شمس الدین والی ارجیش
---
# شمارهٔ ۲۵۲ - در شکر ایادی و انعام رئیس شمس الدین والی ارجیش

<div class="b" id="bn1"><div class="m1"><p>رفیقا شناسی که من ز اهل شروان</p></div>
<div class="m2"><p>نه از بیم جان در شما می‌گریزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطائی نکردم به‌حمدالله آنجا</p></div>
<div class="m2"><p>که اینجا ز بیم خطا می‌گریزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خوش گفت سالار موران که با جم</p></div>
<div class="m2"><p>نکردم بدی زو چرا می‌گریزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بهر فراغت سفر می‌گزینم</p></div>
<div class="m2"><p>پی نزهت اندر فضا می‌گریزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا زحمت صادر و وارد آنجا</p></div>
<div class="m2"><p>عنا می‌نمود از عنا می‌گریزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قضا هم ز داغ فراق عزیزان</p></div>
<div class="m2"><p>دلم سوخت هم زان قضا می‌گریزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلی بودم از غم چو سیماب لرزان</p></div>
<div class="m2"><p>چو سیماب از آن جابه جا می‌گریزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تبریز هم پای‌بند عیالم</p></div>
<div class="m2"><p>از آن پای بند بلا می‌گریزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تبریز چون سوی ارمن بیایم</p></div>
<div class="m2"><p>هم از ظلمتی در ضیا می‌گریزم</p></div></div>