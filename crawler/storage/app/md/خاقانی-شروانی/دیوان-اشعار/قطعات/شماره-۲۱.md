---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>بشنو ای پیر پند خاقانی</p></div>
<div class="m2"><p>خاک توست این جوان علم طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان علم است فقر و علم تن است</p></div>
<div class="m2"><p>علم جان جوی و جان علم طلب</p></div></div>