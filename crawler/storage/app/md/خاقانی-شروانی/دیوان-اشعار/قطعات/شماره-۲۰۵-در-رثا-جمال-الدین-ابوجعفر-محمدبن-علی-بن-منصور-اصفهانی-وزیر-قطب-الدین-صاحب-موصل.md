---
title: >-
    شمارهٔ ۲۰۵ - در رثاء جمال الدین ابوجعفر محمدبن علی بن منصور اصفهانی وزیر قطب الدین صاحب موصل
---
# شمارهٔ ۲۰۵ - در رثاء جمال الدین ابوجعفر محمدبن علی بن منصور اصفهانی وزیر قطب الدین صاحب موصل

<div class="b" id="bn1"><div class="m1"><p>جمال صفاهان نظام دوم</p></div>
<div class="m2"><p>که گیتی سیم جعفر انگاشتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو قحط کرم دید در مرز دهر</p></div>
<div class="m2"><p>علی‌وار تخم کرم کاشتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهان جهان نالهٔ آز داشت</p></div>
<div class="m2"><p>به در سخاوت بینباشتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سلطانی جود چون سر فراشت</p></div>
<div class="m2"><p>قضا چتر دولت برافراشتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به معماری کعبه چون دست برد</p></div>
<div class="m2"><p>زمانه براهیم پنداشتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن کآفتاب سخا بود چرخ</p></div>
<div class="m2"><p>ز روی زمین سایه برداشتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان را همین یک جوان مرد بود</p></div>
<div class="m2"><p>فلک هم حسد برد و نگذاشتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان سوخت خاقانی از سوگ او</p></div>
<div class="m2"><p>که با شام برمی‌زند چاشتش</p></div></div>