---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>خاقانیا به سائل اگر یک درم دهی</p></div>
<div class="m2"><p>خواهی جزای آن دو بهشت از خدای خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس نام آن کرم کنی ای خواجه برمنه</p></div>
<div class="m2"><p>نام کرم به دادهٔ روی و ریای خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر دادهٔ تو نام کرم کی بود سزا</p></div>
<div class="m2"><p>تا داده را بهشت ستانی سزای خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا یک دهی به خلق و دو خواهی ز حق جزا</p></div>
<div class="m2"><p>آن را ربا شمر که شمردی عطای خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانی کرم کدام بود آنکه هرچه هست</p></div>
<div class="m2"><p>بدهی بهر که هست و نخواهی جزای خویش</p></div></div>