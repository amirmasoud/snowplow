---
title: >-
    شمارهٔ ۳۲۶
---
# شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>اگر معزی و جاحظ به روزگار منندی</p></div>
<div class="m2"><p>به نظم و نثر همانا که پیش‌کار منندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بورشید و ز عبدک مثل زنند به شروان</p></div>
<div class="m2"><p>وگر به دور منندی دوات‌دار منندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به زور و زر نفریبم چو زور و زر وزیران</p></div>
<div class="m2"><p>که فخر زور و زرستی گر اختیار منندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آسمان وزارت گر انجم هنرستی</p></div>
<div class="m2"><p>وزارت و هنر امروز در شمار منندی</p></div></div>