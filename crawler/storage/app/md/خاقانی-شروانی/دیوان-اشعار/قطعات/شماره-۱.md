---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>مرا شاه بالای خواجه نشانده است</p></div>
<div class="m2"><p>از آن خواجه آزرده برخاست از جا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه بایستش آزردن از سایهٔ حق</p></div>
<div class="m2"><p>که نوری است این سایه از حق تعالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه زیر قلم جای لوح است چونان</p></div>
<div class="m2"><p>که بالای کرسی است عرش معلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نداند که از دور پرگار قدرت</p></div>
<div class="m2"><p>بود نقطهٔ کل بر از خط اجزا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معما بر از ابجد آمد به معنی</p></div>
<div class="m2"><p>چو معنی که هم برتر آمد ز اسما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخور از بر عنبر آمد به مجلس</p></div>
<div class="m2"><p>عقول از بر انفس آمد به مبدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کواکب بود زیر پای ملایک</p></div>
<div class="m2"><p>حواری بود بر زبردست حورا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببین نه طبق برتر از هفت قلعه</p></div>
<div class="m2"><p>ببین هفت خاتون بر از چار ماما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمین زیر به کو کثیف است و ساکن</p></div>
<div class="m2"><p>فلک به ز بر کو لطیف است و دروا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>الف را بر اعداد مرقوم ببینی</p></div>
<div class="m2"><p>که اعداد فرعند و او اصل و والا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه شاخ از بر بیخ باشد مرتب</p></div>
<div class="m2"><p>نه بار از بر برگ باشد مهیا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قیاس از درختان بستان چه گیری</p></div>
<div class="m2"><p>ببین شاخ و بیخ درختان دانا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هنرمند کی زیر نادان نشنید</p></div>
<div class="m2"><p>که بالای سرطان نشسته است جوزا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه لعل از بر خاتم زر نشیند</p></div>
<div class="m2"><p>نه لعل و زر کل چنین است عمدا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دبیری چو من زیردست وزیری</p></div>
<div class="m2"><p>ندارند حاشا که دارند حاشا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دبیر است خازن به اسرار پنهان</p></div>
<div class="m2"><p>وزیر است ضامن به اشکال پیدا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دبیری ورای وزیری است یعنی</p></div>
<div class="m2"><p>عطارد ورای قمر یافت ماوا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو ریگی است تیره‌گران سایه نادان</p></div>
<div class="m2"><p>چو آبی است روشن سبک‌روح دانا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه آب از بر ریگ باشد به چشمه</p></div>
<div class="m2"><p>نه عنبر بر از آب باشد به دریا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گران سایه زیر سبک‌روح بهتر</p></div>
<div class="m2"><p>چو سنگ سیه زیر آب مصفا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دو سنگ است بالا و زیر اسیا را</p></div>
<div class="m2"><p>گران سیر زیر و سبک سیر بالا</p></div></div>