---
title: >-
    شمارهٔ ۲۷۴
---
# شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>در دو عالم کار ما داریم کز غم فارغیم</p></div>
<div class="m2"><p>الصبوح ای دل که از کار دو عالم فارغیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کم زدیم و عالم خاکی به خاکی باختیم</p></div>
<div class="m2"><p>و آن دگر عالم گرو دادیم و از کم فارغیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل اگر در کشت‌زار خاک آدم ده کیاست</p></div>
<div class="m2"><p>ما چنان کز عقل بیزاریم از آدم فارغیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک عشق از خون عقلی به که غم‌بار آورد</p></div>
<div class="m2"><p>ما که ترک عقل گفتیم از همه غم فارغیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق داریم از جهان گر جان مباشد گو مباش</p></div>
<div class="m2"><p>چون سلیمان حاضر است تخت و خاتم فارغیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همدم ما گر به بوی جرعه مستی شد تمام</p></div>
<div class="m2"><p>ما به دریا نیم مستیم و ز همدم فارغیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محرم از بهر نهان کاران به کار آید حریف</p></div>
<div class="m2"><p>ما که می پیدا خوریم از کار محرم فارغیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این لب خاکین ما را در سفالی باده ده</p></div>
<div class="m2"><p>جام جم بر سنگ زن کز جام و از جم فارغیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرخ و اختر چیست طاق آرایشی و طارمی است</p></div>
<div class="m2"><p>ما خراب دوستیم از طاق و طارم فارغیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن سپر کردیم پیش تیر باران جفا</p></div>
<div class="m2"><p>هرچه زخم آید ببوسیم و ز مرهم فارغیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر شما دین و دلی دارید و از ما فارغید</p></div>
<div class="m2"><p>ما نه دین داریم و نه دل وز شما هم فارغیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چند دام از زهد سازی و دم از طاعت زنی</p></div>
<div class="m2"><p>ما هم از دام تو دوریم و هم از دم فارغیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لاف آزادی زنی با ما مزن باری که ما</p></div>
<div class="m2"><p>از امید جنت و بیم جهنم فارغیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چند یاد از کعبه و زمزم کنی خاقانیا</p></div>
<div class="m2"><p>باده ده کز کعبه آزاد و ز زمزم فارغیم</p></div></div>