---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>همه هم شهریان خاقانی</p></div>
<div class="m2"><p>با وی از کبر درنیامیزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه عجب زاد را به یک‌جایند</p></div>
<div class="m2"><p>لیک با یکدگر نیامیزند</p></div></div>