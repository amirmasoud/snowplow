---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>شکست این دلم نادرست اعتقادی</p></div>
<div class="m2"><p>به سم خار در دیدهٔ آرزو زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطا کرد پرگار غمزش همانا</p></div>
<div class="m2"><p>که زخمی بر آن سینهٔ نیک‌خو زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شنیدی که زنبور کافر بمیرد</p></div>
<div class="m2"><p>هر آنگه که نیشی به مردم فرو زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه کژدم سر نیش زد عالمی را</p></div>
<div class="m2"><p>که او را وبال آمد آن نیش کو زد</p></div></div>