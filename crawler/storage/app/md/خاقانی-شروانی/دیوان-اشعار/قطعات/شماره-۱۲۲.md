---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>تو مار صورتی و همیشه شکرخوری</p></div>
<div class="m2"><p>خاقانی است طوطی و دایم جگر خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این هم ز بخشش فلک و جود عالم است</p></div>
<div class="m2"><p>کان را که خاک باید خوردن، شکر خورد</p></div></div>