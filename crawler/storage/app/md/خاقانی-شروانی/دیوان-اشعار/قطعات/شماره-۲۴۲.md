---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>آرزو بود نعمتم لیکن</p></div>
<div class="m2"><p>از خسان ز من نپذرفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیش می‌خواستم زمانه نداد</p></div>
<div class="m2"><p>کم همی داد من نپذرفتم</p></div></div>