---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>هرکه را غره کرد دولت نیز</p></div>
<div class="m2"><p>غدر آن دولتش هلاک رساند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک بر فرق دولتی که تو را</p></div>
<div class="m2"><p>از سر خاک بر سماک رساند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه نه صد جان نثار آن دولت</p></div>
<div class="m2"><p>که تواند تو را به خاک رساند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد اگر برد خاک را بر چرخ</p></div>
<div class="m2"><p>بازش از چرخ بر مغا رساند</p></div></div>