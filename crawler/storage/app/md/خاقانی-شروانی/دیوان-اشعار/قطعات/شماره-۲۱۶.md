---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>من که خاقانیم نموداری</p></div>
<div class="m2"><p>مختصر دیده‌ام ز طالع خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه هر کوکب سعادت بخش</p></div>
<div class="m2"><p>برگذر دیده‌ام ز طالع خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیت اولاد و بیت اخوان را</p></div>
<div class="m2"><p>بسته در دیده‌ام ز طالع خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیکن از هشتم و ششم خود را</p></div>
<div class="m2"><p>کم ضرر دیده‌ام ز طالع خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که بیت الحیات را ز نخست</p></div>
<div class="m2"><p>شیر نر دیده‌ام ز طالع خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز وقت ظفر به بیت‌المال</p></div>
<div class="m2"><p>سگ‌تر دیده‌ام ز طالع خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر خر کو به خواب در بخت است</p></div>
<div class="m2"><p>دورتر دیده‌ام ز طالع خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس به بیداری آزمایش را</p></div>
<div class="m2"><p>دم خر دیده‌ام ز طالع خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست صد عیب طالعم را لیک</p></div>
<div class="m2"><p>یک هنر دیده‌ام ز طالع خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که نماند دراز دشمن من</p></div>
<div class="m2"><p>من اثر دیده‌ام ز طالع خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر کس آزار من مبارک نیست</p></div>
<div class="m2"><p>اینقدر دیده‌ام ز طالع خویش</p></div></div>