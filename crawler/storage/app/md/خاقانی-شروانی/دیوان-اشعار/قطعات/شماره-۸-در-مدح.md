---
title: >-
    شمارهٔ ۸ - در مدح
---
# شمارهٔ ۸ - در مدح

<div class="b" id="bn1"><div class="m1"><p>ضمانش کرد به صد سال عمر و مهر نهاد</p></div>
<div class="m2"><p>قباله‌دار ازل نامهٔ ضمانش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حکم هدیهٔ نوروزی آسمان هر سال</p></div>
<div class="m2"><p>تبرک از شرف آوردی آستانش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر که هرچه شرف داد پای پیش کشید</p></div>
<div class="m2"><p>کنون بقای ابد هدیه داد جانش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امام و سرور هر دو جهان که مفتی عقل</p></div>
<div class="m2"><p>ز لوح محفوظ املا کند لسانش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سوزیان معانی کنی خرید و فروخت</p></div>
<div class="m2"><p>که راس مال کمال است سوزیانش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرد به استفادهٔ او برگماشت وقت تمام</p></div>
<div class="m2"><p>به انتجاع رود گوش من بیانش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چند وجه مرا هم پناه و هم پدر است</p></div>
<div class="m2"><p>که حق پناه کند از فنا زمانش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرچه پیشهٔ من نیست زیر تیشه شدن</p></div>
<div class="m2"><p>به زیر تیشه شدم خامه و بنانش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپهر قدرا هرکس که بر کشیدهٔ توست</p></div>
<div class="m2"><p>سپهر درنکشد خط خط امانش را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس از چه بود که در من کمان کشید فلک</p></div>
<div class="m2"><p>نرفته هیچ خدنگی خطا کمانش را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدان قرابهٔ آویخته همی مانم</p></div>
<div class="m2"><p>که در گلو ببرد موش ریسمانش را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر به غصهٔ خصمان فرو شود دل من</p></div>
<div class="m2"><p>روا بود که نکاهد محل روانش را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که قدر مرد کم از پیل نیست کو چو بمرد</p></div>
<div class="m2"><p>همان بها بود آن لحظه استخوانش را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخن برای زبان در غلاف کام کنند</p></div>
<div class="m2"><p>کجا برات نویسند نام و نانش را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حصار شهر به دست مخالفان بینی</p></div>
<div class="m2"><p>چو تو رقاده نهی چشم پاسبانش را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگرچه اسب سخن زیر ران خاقانی است</p></div>
<div class="m2"><p>هنوز داغ به نام تو است رانش را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر سعادت او عمر جاودانی باد</p></div>
<div class="m2"><p>که سر جریده توئی نام جاودانش را</p></div></div>