---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>چشم خونین همه شب قامت شب پیمایم</p></div>
<div class="m2"><p>تا ز خونین جگرش لعل قبا آرایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریسمان از رگ جان سازم و سوزن ز مژه</p></div>
<div class="m2"><p>دیده را دوختن لعل قبا فرمایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اول از عودم خائیدهٔ دندان کسان</p></div>
<div class="m2"><p>آخر از سوختهٔ عالم دندان خایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به من دندان کردند سپید این رمزی است</p></div>
<div class="m2"><p>کاول و آخر دندان کسان را شایم</p></div></div>