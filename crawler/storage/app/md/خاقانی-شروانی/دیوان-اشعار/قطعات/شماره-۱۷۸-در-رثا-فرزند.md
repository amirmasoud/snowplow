---
title: >-
    شمارهٔ ۱۷۸ - در رثاء فرزند
---
# شمارهٔ ۱۷۸ - در رثاء فرزند

<div class="b" id="bn1"><div class="m1"><p>وقت مردن رشید را گفتم</p></div>
<div class="m2"><p>که بخواه آنچه آرزوت آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت کو عمر کارزو خواهم</p></div>
<div class="m2"><p>کارزو بهر عمر می‌باید</p></div></div>