---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>عذر داری بنال خاقانی</p></div>
<div class="m2"><p>کاهل کم داری آشنا کمتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشمنانت ز خاک بیشترند</p></div>
<div class="m2"><p>دوستانت ز کیمیا کمتر</p></div></div>