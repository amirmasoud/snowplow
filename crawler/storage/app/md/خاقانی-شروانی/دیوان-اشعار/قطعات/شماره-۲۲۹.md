---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>مال کم راحت است و افزون رنج</p></div>
<div class="m2"><p>لاجرم مال می نخواهد عقل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو می کاندکش فزاید روح</p></div>
<div class="m2"><p>لیک بسیار او بکاهد عقل</p></div></div>