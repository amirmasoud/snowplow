---
title: >-
    شمارهٔ ۳۰۸
---
# شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>زهر است مرا غذای هر روزه</p></div>
<div class="m2"><p>زین کاسهٔ سرنگون پیروزه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز دهر سیاه کاسه در کاسم</p></div>
<div class="m2"><p>صد ساله غم است شرب یک روزه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهر است کمینه کاسه گردانی</p></div>
<div class="m2"><p>از کیسهٔ او خطاست دریوزه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کوزه نگر به شکل مستسقی</p></div>
<div class="m2"><p>مستسقی را چه راحت از کوزه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چرخ طمع ببر که شیران را</p></div>
<div class="m2"><p>دریوزه نشاید از در یوزه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاقانی صبح خیز، هر شامی</p></div>
<div class="m2"><p>نگشاید جز به خون دل روزه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر تن ز سرشک جامهٔ عیدی</p></div>
<div class="m2"><p>در ماتم دوستان دل سوزه</p></div></div>