---
title: >-
    شمارهٔ ۳۱۲
---
# شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>پسر، خاندان را بود خانه‌دار</p></div>
<div class="m2"><p>چو جان پدر شد به دیگر سرای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر شیر برجا نماند رواست</p></div>
<div class="m2"><p>ولی عطسهٔ شیر ماند بجای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برون بیشه را شیر به میزبان</p></div>
<div class="m2"><p>درون خانه را گربه به کدخدای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان را بنگزیرد از گربه لیک</p></div>
<div class="m2"><p>گزیرد ز شیر نبرد آزمای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که در خانه آواز یک گربه به</p></div>
<div class="m2"><p>که ده غرش شیر دندان نمای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که ده چار دیوار گردد خراب</p></div>
<div class="m2"><p>ز دندان یک موش آفت فزای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه پرویز پرداخت لنگر بری</p></div>
<div class="m2"><p>چو از خشم بهرام بد کرد رای</p></div></div>