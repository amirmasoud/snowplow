---
title: >-
    شمارهٔ ۴۳ - در مدح نظام الملک قوام الدین
---
# شمارهٔ ۴۳ - در مدح نظام الملک قوام الدین

<div class="b" id="bn1"><div class="m1"><p>ایا نظام ممالک قوام روی زمین</p></div>
<div class="m2"><p>تو آفتابی و صدر تو آسمان‌وار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دور خامهٔ تو شرق و غرب بیرون نیست</p></div>
<div class="m2"><p>که بر محیط جهان خامهٔ تو پرگار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس که بر سم اسبت لب کفات رسید</p></div>
<div class="m2"><p>سم سمند تو را لعل نعل و مسمار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دست عدل تو باشه پر عقاب برید</p></div>
<div class="m2"><p>کبوتران را مقراض نوک منقار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فسون خصم تو بحران مغز سرسام است</p></div>
<div class="m2"><p>که مغز خصم به سرسام حقد بیمار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا به دولت تو همتی است رفعت جوی</p></div>
<div class="m2"><p>نه در خور نسب و نه سزای مقدار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نیم بیت مرا بدره‌ها دهند ملوک</p></div>
<div class="m2"><p>تو کدخدای ملوکی تو را همین کار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدان طمع که رسانی بهای دستارم</p></div>
<div class="m2"><p>شریف وعده که فرموده‌ای دوم بار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به انتظار اشارات تو که هان فردا</p></div>
<div class="m2"><p>دلم نماند بجای و چه جای گفتار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به سعد و نحسی کاین آید آن دگر برود</p></div>
<div class="m2"><p>گذشت مدتی و خاطرم گران بار است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه لفظ من به تقاضای سرد معروف است</p></div>
<div class="m2"><p>نه صدر تو به مواعید کژ سزاوار است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدای داند اگر آن، بها به نیم سخن</p></div>
<div class="m2"><p>کراکند وگر آن خود هزار دینار است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرم که نیم جو ارزد به نزد همت تو</p></div>
<div class="m2"><p>به بخشش زر و دستار بس گران بار است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر این جگر خوری ارزد بهای صد دستار</p></div>
<div class="m2"><p>سرم چنان که سبک‌بار هست سگسار است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به دل معاینه آید مرا که دستاری</p></div>
<div class="m2"><p>ز من برند که این را بها و بازار است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون به عرض صله خاطر من آشوب است</p></div>
<div class="m2"><p>کنون به جای درم در کف من آزار است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو گر بها دهی آن داده را زکات شمار</p></div>
<div class="m2"><p>بده زکات بدان کس که گنج اسرار است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به وام کن زر و زین مختصر مرا دریاب</p></div>
<div class="m2"><p>چه وام خیزد ازین مختصر پدیدار است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کرم کن و بخر از دست وام خواهانم</p></div>
<div class="m2"><p>که بر من از کرمت وام‌های بسیار است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز گنج مردی این مایه وام من بگزار</p></div>
<div class="m2"><p>که وام شکر تو بر گردن من انبار است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ازین معامله ار خود زیان کند کرمت</p></div>
<div class="m2"><p>دلم ز خدمت تو وز خدای بیزار است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بده قراضگکی تا عطات پندارم</p></div>
<div class="m2"><p>مگو که سوختهٔ من چه خام پندار است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به چشم‌های جگر گوشه‌ات که بیش مرا</p></div>
<div class="m2"><p>مخور جگر که مرا خود فلک جگر خوار است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به جان شاه که در نگذرانی از امروز</p></div>
<div class="m2"><p>که نگذرم ز سر این صداع و ناچار است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به خاک پای تو کان هست خون بهای سرم</p></div>
<div class="m2"><p>که حاجتم به بهاء تمام دستار است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به شعر گر صله خواهم تو مال‌ها بخشی</p></div>
<div class="m2"><p>بر آن مگیر که این مایه حق اشعار است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به یک دو بیت نود اقچه داد کافی کور</p></div>
<div class="m2"><p>به راوی من کو مدح خوان احرار است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو را که صاحب کافی خریطه‌کش زیبد</p></div>
<div class="m2"><p>چهل درست که بخشش کنی چه دشوار است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به مرد مردمی آخر که صلت چو منی</p></div>
<div class="m2"><p>کم از قراضهٔ معلول قلب کردار است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بهای خیر طلب می‌کنم بدین زاری</p></div>
<div class="m2"><p>تبارک الله کارم نگر که چون زار است</p></div></div>