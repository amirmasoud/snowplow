---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>جدلی فلسفی است خاقانی</p></div>
<div class="m2"><p>تا به فلسی نگیری احکامش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلسفه در جدل کند پنهان</p></div>
<div class="m2"><p>وانگهی فقه برنهد نامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مس بدعت به زر بیالاید</p></div>
<div class="m2"><p>پس فروشد به مردم خامش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دام در افکند مشعبدوار</p></div>
<div class="m2"><p>پس بپوشد به خار و خس دامش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ را هم به لطف صید کنند</p></div>
<div class="m2"><p>پس ببرند سر به ناکامش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علم دین پیشت آورد وانگه</p></div>
<div class="m2"><p>کفر باشد سخن به فرجامش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار او و تو تا گه تطهیر</p></div>
<div class="m2"><p>کار طفل است و آن حجامش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکرش در دهان نهد و آنگه</p></div>
<div class="m2"><p>ببرد پاره‌ای ز اندامش</p></div></div>