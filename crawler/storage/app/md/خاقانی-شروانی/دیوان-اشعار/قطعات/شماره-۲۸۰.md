---
title: >-
    شمارهٔ ۲۸۰
---
# شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>ز آل غانم اگرچه نفعی نیست</p></div>
<div class="m2"><p>باری آسوده‌اند عالمیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وای بر عالم ار فکندی حق</p></div>
<div class="m2"><p>کار عالم به دست غانمیان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقت آن کز نسب نهد خود را</p></div>
<div class="m2"><p>از ملایک نهد نه ز آدمیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اول از شیر سرخ لاف زند</p></div>
<div class="m2"><p>پس درآید سگ سیه ز میان</p></div></div>