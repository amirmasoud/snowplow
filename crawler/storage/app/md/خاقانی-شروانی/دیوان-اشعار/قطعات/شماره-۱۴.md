---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>من به ری عزم خراسان داشتم</p></div>
<div class="m2"><p>ز آن که جان بود آرزومندش مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>والی ری بند بر عزمم نهاد</p></div>
<div class="m2"><p>نیک دامن‌گیر شد بندش مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از یمین الدین شکایت کردمی</p></div>
<div class="m2"><p>لیک شرم آمد ز فرزندش مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس فسادی کافت اخیار شد</p></div>
<div class="m2"><p>ار ضمیر روح مانندش مرا</p></div></div>