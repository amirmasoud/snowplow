---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>دار عزلت گزید خاقانی</p></div>
<div class="m2"><p>که به از دار ملک خاقان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورش از مشرب قناعت ساخت</p></div>
<div class="m2"><p>که چو زمزم هم آب حیوان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبرد تا تواند انده رزق</p></div>
<div class="m2"><p>کانده رزق بر جهانبان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمرا گر بهر رزق موقوف است</p></div>
<div class="m2"><p>رزق موقوف بهر فرمان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نپذیرد ز کس حوالهٔ رزق</p></div>
<div class="m2"><p>که ضمان‌دار رزق یزدان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مور را روزی از سلیمان نیست</p></div>
<div class="m2"><p>گه ز روزی ده سلیمان است</p></div></div>