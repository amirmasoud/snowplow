---
title: >-
    شمارهٔ ۱۸۲
---
# شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>که گفت آنکه خاقانی سحرپیشه</p></div>
<div class="m2"><p>دگر خاص درگاه سلطان نشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلی راست گفت او و پی بردم آن را</p></div>
<div class="m2"><p>که دیو آبدار سلیمان نشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرانی ببردم ز درگاهش ایرا</p></div>
<div class="m2"><p>مرید سبک دل گران جان نشاید</p></div></div>