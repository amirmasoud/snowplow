---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>خواجه بر استر رومی خر مصری می‌دید</p></div>
<div class="m2"><p>گفتم از صد خر مصری است به آن دل دل تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو به قیمت ز خر مصر نه‌ای کم به یقین</p></div>
<div class="m2"><p>نه ز بانگ خر مصری است کم آن غلغل تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن خر مصر عبائی است و ز اطلس جل او</p></div>
<div class="m2"><p>تو خر اطلسی و هست عبائی جل تو</p></div></div>