---
title: >-
    شمارهٔ ۳۶۱
---
# شمارهٔ ۳۶۱

<div class="b" id="bn1"><div class="m1"><p>نیست سالم دو ده ولی به سخن</p></div>
<div class="m2"><p>نه فلک یک جوان ندیده چو من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن ار فضل هست، دولت نیست</p></div>
<div class="m2"><p>فضل بی‌دولت اسم بی‌معنی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه طعنم زنند مشتی دون</p></div>
<div class="m2"><p>چه توان کرد؟ الجنون فنون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کین نجویم گر آن دراز شود</p></div>
<div class="m2"><p>طعنه‌شان خود به عکس باز شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کان صفت کوه را تواند بود</p></div>
<div class="m2"><p>کز صدا باز گوید آنچه شنود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن صدا را تو زو چه پنداری</p></div>
<div class="m2"><p>جز گران جانی و سبکساری</p></div></div>