---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>خوش سواری است عمر خاقانی</p></div>
<div class="m2"><p>صیدگه دهر و بارگیر اوقات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش کان زین خود ز پشت حیات</p></div>
<div class="m2"><p>بفکند نفل صید نعل کن حسنات</p></div></div>