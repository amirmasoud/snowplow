---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>مرد باید که چون هنر ورزد</p></div>
<div class="m2"><p>بحر باشد که امتحان ارزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه ازو هر خسی دری ببرد</p></div>
<div class="m2"><p>گاه ازو هر سگی دمی بخورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نش از آن در کمی پدید شود</p></div>
<div class="m2"><p>نز زبان سگی پلید شود</p></div></div>