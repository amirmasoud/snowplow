---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>برد بیرون مرا ز ظلمت شک</p></div>
<div class="m2"><p>این چراغ یقین که من دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کعب همت به ساق عرش رساند</p></div>
<div class="m2"><p>این دو تن عقل و دین که من دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیل غوغای آز بشکستند</p></div>
<div class="m2"><p>این دو صف در کمین که من دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود سگی کردنم نفرمایند</p></div>
<div class="m2"><p>این دو شیر عرین که من دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدما گرچه سحرها دارند</p></div>
<div class="m2"><p>کس ندارد چنین که من دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنم از شوره خاک شیرهٔ پاک</p></div>
<div class="m2"><p>این کرامات بین که من دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبرد ذل برآستان ملوک</p></div>
<div class="m2"><p>این دل نازنین که من دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه ز سردان خورم طپانچهٔ گرم</p></div>
<div class="m2"><p>این رخ شرمگین که من دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسبی الله مراست نقش نگین</p></div>
<div class="m2"><p>جم ندید این نگین که من دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تخم همت ستاره بر دهدم</p></div>
<div class="m2"><p>فلک است این زمین که من دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل مرا در خرابه‌ای بنشاند</p></div>
<div class="m2"><p>اینست گنج مهین که من دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همتم سر ز تاج در دزدد</p></div>
<div class="m2"><p>اینت گنج مهین که من دارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من که خاقانیم ندانم هم</p></div>
<div class="m2"><p>که چه شاهی است این که من دارم</p></div></div>