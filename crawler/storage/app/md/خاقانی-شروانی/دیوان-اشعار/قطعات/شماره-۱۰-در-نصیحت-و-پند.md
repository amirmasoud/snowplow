---
title: >-
    شمارهٔ ۱۰ - در نصیحت و پند
---
# شمارهٔ ۱۰ - در نصیحت و پند

<div class="b" id="bn1"><div class="m1"><p>بترس از بد خلق خاقانیا</p></div>
<div class="m2"><p>ولیکن ز بد ده امان خلق را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وفا طبع گردان و ایمن مباش</p></div>
<div class="m2"><p>ز غدری که طبع است آن خلق را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دروغی مران بر زبان و مدان</p></div>
<div class="m2"><p>که صدقی بود بر زبان خلق را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در افعال خلق آشکارا شود</p></div>
<div class="m2"><p>قضائی که آید نهان خلق را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم از خلق سر بزرند از زمین</p></div>
<div class="m2"><p>بدی کاید از آسمان خلق را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بد خلق هرچت فزون‌تر رسد</p></div>
<div class="m2"><p>نکوئی فزون‌تر رسان خلق را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه دوستی ورز با خلق لیک</p></div>
<div class="m2"><p>به دل دشمن خویش دان خلق را</p></div></div>