---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>عیسی دورانم و این کور شد دجال من</p></div>
<div class="m2"><p>قدر عیسی کی نهد دجال ناموزون کور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر راهم چو بازآیم ز اقلیم عراق</p></div>
<div class="m2"><p>هم بسوزم هم بریزم جان کور و خون کور</p></div></div>