---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>منصب تدریس خون گوید از آنک</p></div>
<div class="m2"><p>فر عز الدین بوعمران نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاید ار هر سامری گاوی کند</p></div>
<div class="m2"><p>کب و جاه موسی عمران نماند</p></div></div>