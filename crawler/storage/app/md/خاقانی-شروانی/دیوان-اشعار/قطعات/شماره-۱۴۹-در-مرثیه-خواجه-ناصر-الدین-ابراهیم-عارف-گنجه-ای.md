---
title: >-
    شمارهٔ ۱۴۹ - در مرثیهٔ خواجه ناصر الدین ابراهیم عارف گنجه‌ای
---
# شمارهٔ ۱۴۹ - در مرثیهٔ خواجه ناصر الدین ابراهیم عارف گنجه‌ای

<div class="b" id="bn1"><div class="m1"><p>خاقانیا عروس صفا را به دست فقر</p></div>
<div class="m2"><p>هر هفت کن که هفت تنان در رسیده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وجد و حال بین چو کبوتر زنند چرخ</p></div>
<div class="m2"><p>بازان کز آشیان طریق پریده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچون گوزن هوی برآورده در سماع</p></div>
<div class="m2"><p>شیران کز آتش شب شبهت رمیده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلطان دلان به عرش براهیم بنده‌وار</p></div>
<div class="m2"><p>از بهر آب دست سراب قد خمیده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر نام او به سنت همنام او همه</p></div>
<div class="m2"><p>مرغان نفس را ز درون سر بریده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خضر ارچه حاضر است نیارد نهاد دست</p></div>
<div class="m2"><p>بر خرقه‌های او که ز نور آفریده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیران هفت چرخ به معلوم هشت خلد</p></div>
<div class="m2"><p>یک ژندهٔ دوتائی او را خریده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بهر پاره پیر فلک را به دست صبح</p></div>
<div class="m2"><p>دلق هزار میخ ز سر برکشیده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>واینک پی موافقت صف صوفیان</p></div>
<div class="m2"><p>صوف سپید بر تن مشرق دریده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در مشرق آفتاب چنان جامه خرقه کرد</p></div>
<div class="m2"><p>کواز خرق جامه به مغرب شنیده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا گنجه را ز خاک براهیم کعبه‌ای است</p></div>
<div class="m2"><p>مردان کعبه گنجه نشینی گزیده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من دیده‌ام که حد مقامات او کجاست</p></div>
<div class="m2"><p>آنان ندیده‌اند که کوتاه دیده‌اند</p></div></div>