---
title: >-
    شمارهٔ ۸۶ - در بحث با معطل
---
# شمارهٔ ۸۶ - در بحث با معطل

<div class="b" id="bn1"><div class="m1"><p>دی جدل با معطلی کردم</p></div>
<div class="m2"><p>که ز توحید هیچ ساز نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آستین فضول می‌افشاند</p></div>
<div class="m2"><p>که ز ایمان بر او طراز نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخرش هم مصاف بشکستم</p></div>
<div class="m2"><p>که سلاحی به جز مجاز نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیک دور از خدای بود ز من</p></div>
<div class="m2"><p>بد او جز خدای باز نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌نیازا تو نصرتم دادی</p></div>
<div class="m2"><p>بر کسی کو به تو نیاز نداشت</p></div></div>