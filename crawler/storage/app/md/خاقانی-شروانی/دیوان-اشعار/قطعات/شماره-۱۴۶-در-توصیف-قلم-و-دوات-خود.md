---
title: >-
    شمارهٔ ۱۴۶ - در توصیف قلم و دوات خود
---
# شمارهٔ ۱۴۶ - در توصیف قلم و دوات خود

<div class="b" id="bn1"><div class="m1"><p>دوات من ز برون جدول و درون دریاست</p></div>
<div class="m2"><p>نهنگ و آب سیاهش عجب بدان ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمود صبح ندیدی سواد شام در او</p></div>
<div class="m2"><p>دوات من به دو معنی بدان نشان ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رواست کو ید بیضای موسوی است دوات</p></div>
<div class="m2"><p>که خامه نیز به ثعبان درفشان ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبان خامهٔ جوشن در زره بر من</p></div>
<div class="m2"><p>به دور باش سنان فعل و تیرسان ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو خسروان گذرم بر مصاف نطق و دوات</p></div>
<div class="m2"><p>از آن به خانهٔ زراد خسروان ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عنان جیحون در دست طبع خاقانی است</p></div>
<div class="m2"><p>از آن جهت به سمرقند خضرخان ماند</p></div></div>