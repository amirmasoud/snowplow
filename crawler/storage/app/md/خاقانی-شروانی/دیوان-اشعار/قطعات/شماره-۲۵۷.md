---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>منم که یک رگ جانم هزار بازوی خون راند</p></div>
<div class="m2"><p>از آنکه دست حوادث زده است بر دل ریشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رگ گشادهٔ جانم به دست مهر که بندد</p></div>
<div class="m2"><p>که از خواص به دوران نه دوست ماند و نه خویشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه هیچ کام برآید ز میر و میرهٔ شهرم</p></div>
<div class="m2"><p>نه هیچ کار گشاید ز صدر و صاحب جیشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار درد دلم هست و هیچ جنس به نوعی</p></div>
<div class="m2"><p>نساخت داروی دردم، نکرد مرهم ریشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز کس سخن چه نیوشم حدیث خوش چه سرایم</p></div>
<div class="m2"><p>تنور گرم نبینم فطیرها چه سریشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز غصه چون بره نالم که سوی میش گذاری</p></div>
<div class="m2"><p>که برنیارد شاخم بره نیارد میشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سردی نفس من تموز دی گردد</p></div>
<div class="m2"><p>چه حاجت است در این دی به خیش خانه و خیشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز چار نامه عیان شد که من موحد نامم</p></div>
<div class="m2"><p>به چار کیش خبر شد که من مقدس کیشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو نان طلب کنم از شاه عشوه سازد قوتم</p></div>
<div class="m2"><p>چو آب خواهم از ایام زهر دارد پیشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خدایگانا در باب آن معاش که گفتی</p></div>
<div class="m2"><p>صداع ندهم بیشت جگر مخور بیشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرنج اگرت بگویم بنان و جامه مرنجان</p></div>
<div class="m2"><p>بنان و جامه رسان از بنان و خامهٔ خویشم</p></div></div>