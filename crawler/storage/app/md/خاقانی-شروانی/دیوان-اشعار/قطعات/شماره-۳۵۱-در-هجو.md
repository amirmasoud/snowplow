---
title: >-
    شمارهٔ ۳۵۱ - در هجو
---
# شمارهٔ ۳۵۱ - در هجو

<div class="b" id="bn1"><div class="m1"><p>اهل بغداد را زنان بینی</p></div>
<div class="m2"><p>طبقات طبق ز نان بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هاون سیم زعفران سایان</p></div>
<div class="m2"><p>فارغ از دستهٔ گران بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زعفران سای گشته هاون‌ها</p></div>
<div class="m2"><p>تنگ چون تنگ زعفران بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حقه‌های بلور سیم‌افشان</p></div>
<div class="m2"><p>هر دو هفته عقیق‌دان بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غار سیمین و سبزه پیرامن</p></div>
<div class="m2"><p>در برش چشمهٔ روان بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماده بر ماده اوفتان دو به دو</p></div>
<div class="m2"><p>همچو جوزا و فرقدان بینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چار بالش چو نقره از پس و پیش</p></div>
<div class="m2"><p>دو رفاده ز پرنیان بینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون طبق بر طبق زنند افغان</p></div>
<div class="m2"><p>در طبق‌های آسمان بینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوس کوبی است این ... کوبی</p></div>
<div class="m2"><p>که همه عالمش فغان بینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای برادر بیا و جلدی کن</p></div>
<div class="m2"><p>... می‌زن چو آن‌چنان بینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب ... ینه رفت و رونق ...</p></div>
<div class="m2"><p>تا علمشان بدین نشان بینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس کن این هزل چیست خاقانی</p></div>
<div class="m2"><p>که ز هزل آفت روان بینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر به نقش زنان فرود آئی</p></div>
<div class="m2"><p>همچو نقش زنان زیان بینی</p></div></div>