---
title: >-
    شمارهٔ ۱۰۶ - در بارهٔ رشید الدین وطواط
---
# شمارهٔ ۱۰۶ - در بارهٔ رشید الدین وطواط

<div class="b" id="bn1"><div class="m1"><p>نیک بد رائی با خلق جهان</p></div>
<div class="m2"><p>که بدی نیک سوی جانت رساد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو نیکان را جز بد نرسید</p></div>
<div class="m2"><p>که دعای بد نیکانت رساد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پیت یارب پنهان من است</p></div>
<div class="m2"><p>یارب آن یارب پنهانت رساد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه خاقانی از آتش بتر است</p></div>
<div class="m2"><p>یارب آن آتش سوزانت رساد</p></div></div>