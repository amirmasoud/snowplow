---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>وفا جستن از خلق خاقانیا بس</p></div>
<div class="m2"><p>که جستن به اندازهٔ جهد باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگو کز جهان دیده‌ام نیک عهدی</p></div>
<div class="m2"><p>غلط دیده باشی که بدعهد باشد</p></div></div>