---
title: >-
    شمارهٔ ۹۱ - در رثاء دختر خویش
---
# شمارهٔ ۹۱ - در رثاء دختر خویش

<div class="b" id="bn1"><div class="m1"><p>پیش بین دختر نو آمد من</p></div>
<div class="m2"><p>دید کفاتش از پس است برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تحفه‌ای تازه کآمد از ره غیب</p></div>
<div class="m2"><p>دید کاین منزل خس است برفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهری خرد بود و نیک شناخت</p></div>
<div class="m2"><p>کاین جهان بد گهر کس است برفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورتش بست کز رسیدن او</p></div>
<div class="m2"><p>خاطر من مهوس است برفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دید در پرده دختر دگرم</p></div>
<div class="m2"><p>گفت محنت یکی بس است برفت</p></div></div>