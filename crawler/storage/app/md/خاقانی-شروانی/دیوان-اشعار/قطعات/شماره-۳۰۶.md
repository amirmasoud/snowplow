---
title: >-
    شمارهٔ ۳۰۶
---
# شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>راز دارم مرا ز دست مده</p></div>
<div class="m2"><p>بی‌خودان را به خودپرست مده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نجده ساز از دل شکسته‌دلان</p></div>
<div class="m2"><p>این چنین نجده را شکست مده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شست تو همت است و صید تو مال</p></div>
<div class="m2"><p>صید بدهی رواست، شست مده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهرهٔ مار بهر مار زده است</p></div>
<div class="m2"><p>به کسی کز گزند رست مده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عافیت کیمیاست دولت خاک</p></div>
<div class="m2"><p>کیمیا را به خاک پست مده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گنج معنی توراست خاقانی</p></div>
<div class="m2"><p>شو کلیدش به هرکه هست مده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پایگه یافتی، به پای مزن</p></div>
<div class="m2"><p>دستگه یافتی ز دست مده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میده تنها توراست تنها خور</p></div>
<div class="m2"><p>به سگان ده، به هم نشست مده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمع غیبی به پیش کور مسوز</p></div>
<div class="m2"><p>تیغ عقلی به دست مست مده</p></div></div>