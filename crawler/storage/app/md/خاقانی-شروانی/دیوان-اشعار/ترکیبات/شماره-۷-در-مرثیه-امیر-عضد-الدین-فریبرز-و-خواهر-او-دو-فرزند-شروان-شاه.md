---
title: >-
    شمارهٔ ۷ - در مرثیهٔ امیر عضد الدین فریبرز و خواهر او، دو فرزند شروان شاه
---
# شمارهٔ ۷ - در مرثیهٔ امیر عضد الدین فریبرز و خواهر او، دو فرزند شروان شاه

<div class="b" id="bn1"><div class="m1"><p>ای روز رفتگان جگر شب فرو درید</p></div>
<div class="m2"><p>آن آفتاب از آن جگر شب برآورید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب چیست خاک خاک نگر آفتاب خوار</p></div>
<div class="m2"><p>خاکی که آفتاب خورد خون او خورید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای رفته آفتاب شما در کسوف خاک</p></div>
<div class="m2"><p>چون تختهٔ محاسب از آن خاک بر سرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفت آفتاب و صبح ره غیب درنوشت</p></div>
<div class="m2"><p>چو میغ و شب پلاس مصیبت بگسترید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه چرخ گوشهٔ جگر شاهتان بخورد</p></div>
<div class="m2"><p>هین زخم آه و گردهٔ چرخ ار دلاورید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رمح سماک و دهرهٔ بهرام بشکنید</p></div>
<div class="m2"><p>چتر سحاب و بیرق خورشید بردرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم ار ز گریه ناخنه آرد به ناخنان</p></div>
<div class="m2"><p>پلپل در او کنید و به خونش بپرورید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تابوت اوست غرقهٔ زیور عروس‌وار</p></div>
<div class="m2"><p>هر هفت کرده هشت بهشت است بنگرید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تشنه است خاک او ز سرچشمهٔ جگر</p></div>
<div class="m2"><p>خون سوی حوض دیده ز کاریز می‌برید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در پیش گنبدش فلک آید جنیبه‌دار</p></div>
<div class="m2"><p>گاه جنیبتان بکشید ارنه سنجرید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شبدیز و نقره خنگ فلک را به مرگ او</p></div>
<div class="m2"><p>پی برکشید و دم ببرید ار وفا گرید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر گوشتان اشارت غیبی شنیده نیست</p></div>
<div class="m2"><p>بر خاک روضه‌وار فریبرز بگذرید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا با شما صریح بگوید که هان و هان</p></div>
<div class="m2"><p>عبرت ز خاک ما که نه از ما جوانترید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنگه به نوحه باز پس آیید و پیش حق</p></div>
<div class="m2"><p>بهر بقای شاه تضرع برآوردید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کامروز رسته‌اید به جان از سموم ظلم</p></div>
<div class="m2"><p>کاندر ظلال دولت خاقان اکبرید</p></div></div>
<div class="b2" id="bn16"><p>شه زاده رفت باغ بقا باد جای شاه</p>
<p>خون کرد چرخ، قصاصش بقای شاه</p></div>
<div class="b" id="bn17"><div class="m1"><p>گیتی ز دست نوحه به پای اندر آمده</p></div>
<div class="m2"><p>رخنه به سقف هفت سرای اندر آمده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از اشک گرم تفته دلان در سواد خاک</p></div>
<div class="m2"><p>طوفان آب آتش زای اندر آمده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این زال گوژپشت که دنیاست همچو چنگ</p></div>
<div class="m2"><p>از سر بریده موی و به پای اندر آمده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ناهید دست بر سر ازین غم رباب‌وار</p></div>
<div class="m2"><p>نوحه‌کنان نشید سرای اندر آمده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا شاه باز بیضهٔ شاهی گرفته مرگ</p></div>
<div class="m2"><p>نا فرخی به فر همای اندر آمده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا نور جان و ظل خدائی نهفته خاک</p></div>
<div class="m2"><p>بی‌رونقی به خلق خدای اندر آمده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رمحش به حمله حلقهٔ مه درربوده باز</p></div>
<div class="m2"><p>رخنه به رمح حلقه ربای اندر آمده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر گرد نعش آن مه لشکر بنات نعش</p></div>
<div class="m2"><p>صدره شکاف و جعد گشای اندر آمده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر خاک او ز مشک شب و دهن آفتاب</p></div>
<div class="m2"><p>دست زمانه غالیه‌سای اندر آمده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تب کرده کژدمی و چو مارش گزیده سخت</p></div>
<div class="m2"><p>سستی به دست مارفسای اندر آمده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آه خدایگان که فلک زیر کعب اوست</p></div>
<div class="m2"><p>جذر اصم شنیده به وای اندر آمده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مسکین طبیب را که سیه دیده روی حال</p></div>
<div class="m2"><p>کاهش به عقل نور فزای اندر آمده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شریانش دیده چون رگ بربط، نه خون نه حس</p></div>
<div class="m2"><p>خال و خسش به دیدهٔ رای اندر آمده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گردون قبا زره زده بر انتقام مرگ</p></div>
<div class="m2"><p>مرگش ز راه درز قبای اندر آمده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گوئی شبی به خنجر روز و عمود صبح</p></div>
<div class="m2"><p>بینیم پای مرگ ز جای اندر آمده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یا تیغ شاه گردن مرگ آنچنان زده</p></div>
<div class="m2"><p>کسیب آن ز حلق بنای اندر آمده</p></div></div>
<div class="b2" id="bn33"><p>اختر شد، آفتاب امم تا ابد زیاد</p>
<p>بیدق برفت، شاه کرم تا ابد زیاد</p></div>
<div class="b" id="bn34"><div class="m1"><p>ای گوهر از صفای تو دریا گریسته</p></div>
<div class="m2"><p>بر ماهت آفتاب و ثریا گریسته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اجرام هفت خانهٔ زرین به سوک تو</p></div>
<div class="m2"><p>بر هفت بام خانهٔ مینا گریسته</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از رفتنت ز بیضهٔ آفاق کوه قاف</p></div>
<div class="m2"><p>بر نوپران بیضهٔ عنقا گریسته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از حسرت کلاه تو دریای حامله</p></div>
<div class="m2"><p>چون ابر بر جواهر عذرا گریسته</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا کشوری در آب و در آتش نهفت خاک</p></div>
<div class="m2"><p>شش کشور از وفات تو بر ما گریسته</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مردم به جای اشک به یکدم دو مردمک</p></div>
<div class="m2"><p>بر خاک تو جنابه چو جوزا گریسته</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>رزم از پیت به دیدهٔ درع و دهان تیر</p></div>
<div class="m2"><p>الماس خورده، لعل مصفا گریسته</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بزم از پست به دست رباب و به چشم نای</p></div>
<div class="m2"><p>ساغر شکسته بر سر و صهبا گریسته</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>این سبز غاشیه که سیاهش کناد مرگ</p></div>
<div class="m2"><p>بر زین سرنگون تو صد جا گریسته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بر بند موی و حلقهٔ زرین گوش تو</p></div>
<div class="m2"><p>سنگین دلان حلقهٔ خضرا گریسته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ما را بصر ز چشمهٔ حسن تو خورده آب</p></div>
<div class="m2"><p>آن آب نوش زهر شده تا گریسته</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گریند بر تو جانوران تا به حد آنک</p></div>
<div class="m2"><p>عقرب ز راه نیش و زبانا گریسته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چندان گریسته دل خارا به سوک تو</p></div>
<div class="m2"><p>تا آبگینه بر دل خارا گریسته</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اکنون به ناز در تتق خلد پیش تو</p></div>
<div class="m2"><p>خندیده گل قنینهٔ حمرا گریسته</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شاه جهان گشاده اقالیم را به تیغ</p></div>
<div class="m2"><p>تیغش به خنده زهره بر اعدا گریسته</p></div></div>
<div class="b2" id="bn49"><p>آن، ماه نو کجاست که مه خاکپای اوست</p>
<p>الجیجک آنکه حجرهٔ جنات جای اوست</p></div>
<div class="b" id="bn50"><div class="m1"><p>ای چرخ از آن ستارهٔ رعنا چه خواستی</p></div>
<div class="m2"><p>و ای باد از آن شکوفهٔ زیبا چه خواستی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ای روزگار گرگ دل، افغان ز دست تو</p></div>
<div class="m2"><p>تا تو ز جان یوسف دلها چه خواستی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ای زال مستحاضه که آبستنی ز شر</p></div>
<div class="m2"><p>ز آن خوش عذار غنچهٔ عذرا چه خواستی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ما را جگر دریغ نبود از تو هیچوقت</p></div>
<div class="m2"><p>آخر ز گوشهٔ جگر ما چه خواستی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گیرم که آتش سده در جان ما زدی</p></div>
<div class="m2"><p>ز آن مشک‌ریز شاخ چلیپا چه خواستی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گر دیده داشتی و نداری بدیدمت</p></div>
<div class="m2"><p>ز آن نو هلال ناشده پیدا چه خواستی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بر سقف چرخ نرگسه داری هزار صف</p></div>
<div class="m2"><p>از بند آن دو نرگس شهلا چه خواستی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز آن بر که بادریسه هنوز نخسته بود</p></div>
<div class="m2"><p>ای بادریسه چشم بگو تا چه خواستی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گوهر شکن کسی وگرت آب شرم بود</p></div>
<div class="m2"><p>ز آن گوهرین دو آتش گویا چه خواستی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>آخر تو آسمان شکنی یا گوهرشکن</p></div>
<div class="m2"><p>از درج در و برج ثریا چه خواستی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چون خاتم ارنه دیدهٔ دجال داشتی</p></div>
<div class="m2"><p>پس ز آن نگین لعل مسیحا چه خواستی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ای کم ز موی عاریه آخر ز چهره‌ای</p></div>
<div class="m2"><p>گلگونه نارسیده به سیما چه خواستی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ای اژدهادم ارنه چو ضحاک خون خوری</p></div>
<div class="m2"><p>از طفل پادشاه جم آسا چه خواستی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر زانکه چون ترازوی دونان دو سر نه‌ای</p></div>
<div class="m2"><p>زآن شیرزاد سنبله بالا چه خواستی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>قاف از تو رخنه سر شد و عنقا شکسته پر</p></div>
<div class="m2"><p>از زال خرد یک تنه تنها چه خواستی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دست تو بر نژاد زبردست چون رسید</p></div>
<div class="m2"><p>بد گوهرا گوهر والا چه خواستی</p></div></div>
<div class="b2" id="bn66"><p>هان تا حسام شاه کشد کینه از تو باش</p>
<p>از غو غصه صفر کند سینه از تو باش</p></div>
<div class="b" id="bn67"><div class="m1"><p>ای بر سر ممالک دهر افسر آمده</p></div>
<div class="m2"><p>وی گوهرت در افسر دین گوهر آمده</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ای صاحب افسران گرو پای بوس تو</p></div>
<div class="m2"><p>تو افسر سر همه را افسر آمده</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ای هر که افسری است سرش را چو کوکنار</p></div>
<div class="m2"><p>پیشت چو لاله بی‌سر و دامن‌تر آمده</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ای خاک بارگاه تو و خوک پایگاه</p></div>
<div class="m2"><p>هم قصر قیصریه و هم قیصر آمده</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بر هر دو روی سکهٔ ایام نام تو</p></div>
<div class="m2"><p>خاقان عدل ورز و هنر پرور آمده</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>آورده‌ام سه بیت به تضمین ز شعر خویش</p></div>
<div class="m2"><p>در مرثیه به نام نریمان آمده</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>آباد عدل تو که مطرا کند جهان</p></div>
<div class="m2"><p>آیینه‌ای است صیقل خاکستر آمده</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>از بیم زخم گرز تو بانگ شکستگی</p></div>
<div class="m2"><p>از پهلوی زمانهٔ مردم‌خور آمده</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ای ز آسمان به صد درجه سرشناس‌تر</p></div>
<div class="m2"><p>سر دقایق ازلت از برآمده</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>عالم همه به سوک جگر گوشهٔ تواند</p></div>
<div class="m2"><p>ای از چهار گوشهٔ عالم سرآمده</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>پیش سپید مهرهٔ مرگ اصفیا نگر</p></div>
<div class="m2"><p>از مهره‌های نرد پریشان‌تر آمده</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>تضمین کنم ز شهر خود آن بیت را که هست</p></div>
<div class="m2"><p>با اشک چشم سوز دلت درخور آمده</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>کشتی ز صبر ساز که داری ز سوز و اشک</p></div>
<div class="m2"><p>دل چون تنور گشته و طوفان برآمده</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>دیوان عمر تو ز فنا بی‌گزند باد</p></div>
<div class="m2"><p>ای ملک را بقای تو سر دفتر آمده</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ملکت چو ملک‌سام و سکندر بساز و تو</p></div>
<div class="m2"><p>همسان سام و همسر اسکندر آمده</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نی خوش نگفته‌ام ز در بارگاه تو</p></div>
<div class="m2"><p>هم‌سام و هم سکندرت اجرا خور آمده</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نعل سم سمند تو را نام در جهان</p></div>
<div class="m2"><p>کحال دیدهٔ ملک اکبر آمده</p></div></div>
<div class="b2" id="bn84"><p>حکم تو دیوبند و حسامت جهان گشای</p>
<p>اقبال بر در تو در آسمان گشای</p></div>