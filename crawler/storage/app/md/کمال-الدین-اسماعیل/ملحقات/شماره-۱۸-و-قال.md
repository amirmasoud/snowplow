---
title: >-
    شمارهٔ ۱۸ - و قال
---
# شمارهٔ ۱۸ - و قال

<div class="b" id="bn1"><div class="m1"><p>زهی سپهر محلی که دون رتبت توست</p></div>
<div class="m2"><p>هرآنچه عقل زاقسام آفرین داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تویی که شخص هنر از طوارق حدثان</p></div>
<div class="m2"><p>حریم جاه ترامعقلی حصین داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان نشاط فلک گرد خویش میگردد</p></div>
<div class="m2"><p>که خویشتن را با قدر تو قرین داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بآفتاب و سحابش چه التفات بود</p></div>
<div class="m2"><p>کسی که راه بدان دست و آستین داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طلایۀ کرمت بر طریق اهل هنر</p></div>
<div class="m2"><p>کجا که از سپه نیستی کمین داند ؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک که رای تو شد مقتدای افعالش</p></div>
<div class="m2"><p>رضا و خشم ترا اصل مهر و کین داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جدا ز سایۀ تو نیست ذرّۀ خورشید</p></div>
<div class="m2"><p>از آنکه روشنی کار خود درین داند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زسایۀ تو شدست آفتاب روی شناس</p></div>
<div class="m2"><p>که همنشین را هر کس بهمنشین داند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کجا رسد سوی درگاه تو قلاوزوهم</p></div>
<div class="m2"><p>که راه خود همه تا چرخ هفتمین داند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسالها نرسد آفتاب روشن دل</p></div>
<div class="m2"><p>درآن دقیقه که آن رای دوربین داند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزرگوارا داعی مرید دولت تست</p></div>
<div class="m2"><p>فلک بمهر تو جان مرا رهین داند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن بحضرت تو کمتر آورد زحمت</p></div>
<div class="m2"><p>که او ملالت آن طبع نازنین داند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شب دراز به مدح تو می کنم کوتاه</p></div>
<div class="m2"><p>گواه صادق من صبح راستین داند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخن بصدر تو آرم که بر تو مقصورست</p></div>
<div class="m2"><p>کسی که قیمت این گوهر ثمین داند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لطیف طبعان دانند قدر لطف سخن</p></div>
<div class="m2"><p>که قدر باد صبا برگ یاسمین داند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سخنسرایان هستند، لیک صاحب ذوق</p></div>
<div class="m2"><p>حدیث چشمۀ حیوان و پارگین داند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خدای داند اگر دانم اندرین شعرا</p></div>
<div class="m2"><p>کسی که ظاهر تفسیر حورعین داند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سخن چگونه برم نزد آنکه از غفلت</p></div>
<div class="m2"><p>شمال را به بسی جهداز یمین داند؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زمن چه فرق بود تا بدیگران آنرا</p></div>
<div class="m2"><p>که رخش رستم چون صورت گلین داند؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چگونه داروی درد خود از کسی طلبند</p></div>
<div class="m2"><p>که خار را زعداد ترنجبین داند؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه فروتنی من زمردری طمع است</p></div>
<div class="m2"><p>خرد زحال من این ماجرا یقین داند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>طمع چو منقطع آمد... زن آنکس</p></div>
<div class="m2"><p>که خویشتن را کمتر از آن و این داند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زپیر عقل سؤالی پرپر می کردم</p></div>
<div class="m2"><p>که اوست آنکه دوای دل خزین داند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که کیست آنکه غم اهل فضل داند خورد؟</p></div>
<div class="m2"><p>جواب داد صاحب بهاء دین داند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مگیر باز زداعی وظایف الطاف</p></div>
<div class="m2"><p>که او ذخیرۀ خویش از جهان همین داند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ترا مربی خود دانم و دعا گویم</p></div>
<div class="m2"><p>خدای جل جلاله زمن چنین داند</p></div></div>