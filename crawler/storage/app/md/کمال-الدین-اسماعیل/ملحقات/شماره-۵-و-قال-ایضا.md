---
title: >-
    شمارهٔ ۵ - و قال ایضا
---
# شمارهٔ ۵ - و قال ایضا

<div class="b" id="bn1"><div class="m1"><p>غریب و خسته و درمانده ام خداوندا</p></div>
<div class="m2"><p>ز فیض فضل، تو یک شربت شفا بفرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو لاله غرقه بخونم در آتش شوقت</p></div>
<div class="m2"><p>نسیم لطفی از عالم بقا بفرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببوی رحمت تو بنده کرد جان بازی</p></div>
<div class="m2"><p>بدست عفو تو پروانۀ عطا بفرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه مرهم جانست زخم در ره تو</p></div>
<div class="m2"><p>ز نوش داروی رحمت نصیب ما بفرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون خون جان من اندر ره تو ریخته شد</p></div>
<div class="m2"><p>هم از خزینۀ لطف تو خون بها بفرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبیب حال شناسی ، ترا نیارم گفت</p></div>
<div class="m2"><p>که دور کن ز من این درد یا دوا بفرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آنچه مصلحت کار من در آن دانی</p></div>
<div class="m2"><p>اگر شفاست وگر مرگ ای خدا بفرست</p></div></div>