---
title: >-
    شمارهٔ ۱۱ - و قال فی وصف البناء
---
# شمارهٔ ۱۱ - و قال فی وصف البناء

<div class="b" id="bn1"><div class="m1"><p>..... از ارم لطیف ترست</p></div>
<div class="m2"><p>کعبۀ فضل و قبلۀ هنرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنبد او کلاه کیوانست</p></div>
<div class="m2"><p>گرچه از روی وضع مختصرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره پایداری ارکانش</p></div>
<div class="m2"><p>کرده با کوه دست در کمرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه در او روزگار را تاثیر</p></div>
<div class="m2"><p>نه بر او حادثات را گذرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی نقشهای دیوارش</p></div>
<div class="m2"><p>چرخ و خورشید لاجور دوزرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راست گویی که از طریق نهاد</p></div>
<div class="m2"><p>نسختی از بهشت هشت درست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطلع آفتاب اقبالست</p></div>
<div class="m2"><p>تکیه جای سعادت و ظفرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صدر عالم درو ممکن باد</p></div>
<div class="m2"><p>تا فلک را مدار برمدرست</p></div></div>