---
title: >-
    شمارهٔ ۳۰ - وله ایضا
---
# شمارهٔ ۳۰ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>شگرف برگ نها دست دررزان انگور</p></div>
<div class="m2"><p>در خزانه گشادست بر خزان انگور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگر نگر که ز یک دانه ها هزاران شاخ</p></div>
<div class="m2"><p>ز سوی ساحل بحرین کاروان انگور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدر لؤلوی خوشاب پیش تخت عریش</p></div>
<div class="m2"><p>چو تاج سلطنت فرق خسروان انگور(؟)</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی عقیق، دگر کهر با ، دگر یاقوت</p></div>
<div class="m2"><p>گرفت نسخت از گنج شایگان انگور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر که هست ستامی ز موکب پروین</p></div>
<div class="m2"><p>میان کوکبۀ ممسک العنان انگور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مثال رفرف خضرست و فرش سندس برگ</p></div>
<div class="m2"><p>بگونۀ و جنا الجنتین دان انگور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیاه چشم چو حوران قاصرات الطرف</p></div>
<div class="m2"><p>میان سبز تتقهای پرنیان انگور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دست زرگر باد صبا فرستادست</p></div>
<div class="m2"><p>بر عروس رزان حلۀ جنان انگور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شاخهای ز مرد بدل گرفت شبه</p></div>
<div class="m2"><p>درین معامله جان می کند زیان انگور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نهاد بر سر آن یک هلال چون طوقش</p></div>
<div class="m2"><p>هزار کوکب ثابت چو آسمان انگور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو بر گیاه تباشیر خورد شیر تمام</p></div>
<div class="m2"><p>سیه کند سر پستان چو دایگان انگور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهاب آب حیاتست زانکه دز دیدست</p></div>
<div class="m2"><p>حلاوت از لب آن ترک خوش زبان انگور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مهی که چون سوی رزرفت رنگ می آرد</p></div>
<div class="m2"><p>ز شرم عارض خوبش زمان زمان انگور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدیده رویش از پوست چون برون ندوید؟</p></div>
<div class="m2"><p>چه سخت سخت دلی داشت در میان انگور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر بود همه جایی باستخوان در مغز</p></div>
<div class="m2"><p>چرا بپوشد در مغز استخوان انگور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر نه بر سر آنست تا طرب زاید</p></div>
<div class="m2"><p>بیک شکم ز چه آورد توامانانگور؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هزار چشم چو جالوس و در شکم دندان</p></div>
<div class="m2"><p>منافقی دو دلست آنک از نهان انگور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو چرخ دیده ور و همچو دهر مردافکن</p></div>
<div class="m2"><p>یقین بدان که جهانست در جهان انگور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو هست شهره به مردانگی چرا گیرد</p></div>
<div class="m2"><p>نگار در سرانگشت چون زنان انگور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در انتظار خرابات هر شبی تا روز</p></div>
<div class="m2"><p>گشاد چشم چو زنگی پاسبان انگور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هوای عالم دل معتدل به آب ویست</p></div>
<div class="m2"><p>دریغ نیست بدین کنج خاکدان انگور؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو قوت قوه جان داشت عاقبت جان را</p></div>
<div class="m2"><p>بخون دل طلبید اینت مهربان انگور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مزاج مرد دگرگون کند، زهی دم گرم</p></div>
<div class="m2"><p>که کرد از او بصفت پیر را جوان انگور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز لطف اوست مددهای روح حیوانی</p></div>
<div class="m2"><p>درست کرد نسب نیک باروان انگور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز خاک پاک چو مستان پیچ پیچ آمد</p></div>
<div class="m2"><p>که داشت در همه رگهای سوزیان انگور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو بود طبع ترش گرم جست شیرین جست</p></div>
<div class="m2"><p>ز غورۀ ترش سرد ناگهان انگور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بنقل عدل خزان در برای وام طرب</p></div>
<div class="m2"><p>شدست گویی در عهدۀ ضمان انگور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همی تو گویی مگر شیشه های نارنجست</p></div>
<div class="m2"><p>لبالب از می در صحن بوستان انگور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بریز خونش که زنبور خانۀ فتنه ست</p></div>
<div class="m2"><p>مگر چو روح شود راحت آشیان انگور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بهر سویی نگران همچو شهرگان شده است</p></div>
<div class="m2"><p>بشوخ چشمی در شهر داستان انگور(؟)</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز دستلاف همی سودده، همی دارش</p></div>
<div class="m2"><p>بپای محنت پرخشت ناتوان انگور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مدار زانکه نهد پیش در گه خواجه</p></div>
<div class="m2"><p>چوسا یلان درش سر بر آستان انگور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سیاهۀ دل باغت و از نهاد لطیف</p></div>
<div class="m2"><p>چنانکه خواجه خطیرست دلستان انگور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو خفت قامت گوژش چه سودگر مالد</p></div>
<div class="m2"><p>خضاب وسمه و گلگونه در رخان انگور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بگردن اندزرنجیر همچو دیوانه</p></div>
<div class="m2"><p>نشست خیره بسی همچو کودکان انگور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فراز تاک پر از پیچ و خم همی ماند</p></div>
<div class="m2"><p>بمهرۀ سرافعی چو شد دمان انگور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو گشت برگ زمرد، چو بود افعی تاک</p></div>
<div class="m2"><p>چرا ز سر بنیفکند دیدگان انگور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گمان بری چو کنی سوی شاخ تاک نظر</p></div>
<div class="m2"><p>که هست هیکل گل مهره بر کمان انگور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سیه چو کیوان، در جام سرخ چون بهرام</p></div>
<div class="m2"><p>طرب نواز چو زهره است بی گمان انگور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بسر دسیر خزان در میان بیشه و نی</p></div>
<div class="m2"><p>نهاده دیده بره همچو دیدبان انگور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بشکل مهره از آنست تا به بلعجبی</p></div>
<div class="m2"><p>خیال بازد بر عقل کاردان انگور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برای آنکه شود پای عقل را زنجیر</p></div>
<div class="m2"><p>بداد جعد مسلسل بباغبان انگور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برید جان و سفیر تن و ندیدم دلست</p></div>
<div class="m2"><p>ضمیر بسته زبان راست ترجمان انگور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>برود بار اگر آب او گذر یابد</p></div>
<div class="m2"><p>هزار خنده بر آرد ززعفران انگور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>میان جان غم..، یک سخن سازد</p></div>
<div class="m2"><p>بسی خمار شکسته بر ارغوان انگور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سیاه جامۀ سوکست در برش، عجبست</p></div>
<div class="m2"><p>که حله های طرب راست پودو تان انگور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به پیش کلک نی آورد ز آبنوس دوات</p></div>
<div class="m2"><p>مدیح خواجه مگر می کند بیان انگور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز لطف خوی خوشت شمه‌ای گرفت مگر</p></div>
<div class="m2"><p>که بر جهان نشاطست کامران انگور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به آبروی اگر دانه پر وری کندی</p></div>
<div class="m2"><p>...بجان یافتی امان انگور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بباغ عیش تو سر سبز باش تا که ز مرگ</p></div>
<div class="m2"><p>چو دشمنان شده گیر ندخان و مان انگور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو دشمنانت هر چند خود نگو سارست</p></div>
<div class="m2"><p>معلق آمد گردن بریسمان انگور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز جان اوست طربهای کل شی حی</p></div>
<div class="m2"><p>چو جان خواجه بماناد جاودان انگور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بزگوارا، قومی ز اهل دعوی فضل</p></div>
<div class="m2"><p>بخواستند ز طبعم بامتحان انگور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر بدیدی انگور نظم انگورم</p></div>
<div class="m2"><p>گریختی ز سنۀ سبع باتمان انگور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ازین سپس چو ز شعرم زمانه سرمستست</p></div>
<div class="m2"><p>بکار آب نیاید در اصفحان انگور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سوار مرکب فضلم اگر بفرمایی</p></div>
<div class="m2"><p>هزار بیت بگویم ردیف آن انگور</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همیشه تا که بود نشئه در نهاد شراب</p></div>
<div class="m2"><p>همیشه تا که بود شاه بوستان انگور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تراز کام دمد نکبت شراب طهور</p></div>
<div class="m2"><p>تراز خار برآید چو فرقدان انگور</p></div></div>