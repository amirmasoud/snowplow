---
title: >-
    شمارهٔ ۲۰ - و قال ایضاً
---
# شمارهٔ ۲۰ - و قال ایضاً

<div class="b" id="bn1"><div class="m1"><p>نیست معلونه مگر این شجره</p></div>
<div class="m2"><p>که بجز غصه ندارد ثمره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قربت این شجره هست مرا</p></div>
<div class="m2"><p>همچو بر آدم قرب شجره</p></div></div>