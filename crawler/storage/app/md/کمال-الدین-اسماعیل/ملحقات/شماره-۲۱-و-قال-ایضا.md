---
title: >-
    شمارهٔ ۲۱ - و قال ایضاً
---
# شمارهٔ ۲۱ - و قال ایضاً

<div class="b" id="bn1"><div class="m1"><p>مفتی ملت انعام و کرم</p></div>
<div class="m2"><p>اندرین حال چه می فرماید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حق شخصی درمانده چنان</p></div>
<div class="m2"><p>که برو خلق همی بخشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خفته بیمار بکنجی اندر</p></div>
<div class="m2"><p>وندر باد همی پیماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرج بیماری ناچار بود</p></div>
<div class="m2"><p>و آنگهش خانه عمارت باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز ترتیب زمستان بر پی</p></div>
<div class="m2"><p>که ازو بوی بلا می آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وانگه او سیم ندارد چندان</p></div>
<div class="m2"><p>که بدان آینه یی بزداید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چنین حالتی از منعم خویش</p></div>
<div class="m2"><p>گر تقاضا بکند زر شاید؟</p></div></div>