---
title: >-
    شمارهٔ ۲۵ - ایضاً له
---
# شمارهٔ ۲۵ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>هر کرا قربت تو بیشترست</p></div>
<div class="m2"><p>دانک او از همه درویشترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانک او دورترست از برتو</p></div>
<div class="m2"><p>بعنایت ز همه پیشترست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست چپ گرچه بدل نزدیکست</p></div>
<div class="m2"><p>قوت راست ازو بیشترست</p></div></div>