---
title: >-
    شمارهٔ ۳ - وله ایضا
---
# شمارهٔ ۳ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>اسبی دارم که دور از اسبت</p></div>
<div class="m2"><p>همواره در آرزوی کاهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خسبد روز همچو شب زانک</p></div>
<div class="m2"><p>آفاق بچشم او سیاهست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خاک ز بهر قوت، خاشاک</p></div>
<div class="m2"><p>می جوید ازین سبب دو تا هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پوشیده پلاس و خاک بر سر</p></div>
<div class="m2"><p>پیوسته ز جوع داد خواهست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسوده بماند پشتش از زین</p></div>
<div class="m2"><p>زانکش شکم تهی پنا هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین پس نرود پیاده یک گام</p></div>
<div class="m2"><p>کان گوشه نشین نه مرد را هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در تک ببرد سبق بر این اسب</p></div>
<div class="m2"><p>چو بین اسبی که جفت شاهست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بد تر جایی بمذهب او</p></div>
<div class="m2"><p>در زیر سپهر پایگا هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه کاه در و ، نه جو، نه سبزه</p></div>
<div class="m2"><p>این آخر او چه جایگاهست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>افسانۀ جو فرامشش شد</p></div>
<div class="m2"><p>زیرا که ندید دیر گا هست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این حال جوست و بی تکلف</p></div>
<div class="m2"><p>تا کاه نخورد یک دو ما هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا روزه بشب بدان گشاید</p></div>
<div class="m2"><p>ترتیب ببرگش آب چا هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تیغی یمنی بخورد روزی</p></div>
<div class="m2"><p>... ا هست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دندان گیرد ز روی من زانک</p></div>
<div class="m2"><p>با کاه بر نگش اشتبا هست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عالم همه تا بگاه دیوار</p></div>
<div class="m2"><p>بر گرسنگی او گوا هست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فریادش اگر رسی کنون رس</p></div>
<div class="m2"><p>کش حال ز حد برون تبا هست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو غره مشو که می زند دم</p></div>
<div class="m2"><p>یک دم باشد زنیست تا هست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک بار الحمد و تو بری کاه</p></div>
<div class="m2"><p>در کارش کن که بی گنا هست</p></div></div>