---
title: >-
    شمارهٔ ۱۷ - و قال
---
# شمارهٔ ۱۷ - و قال

<div class="b" id="bn1"><div class="m1"><p>هیچ صحبت مباد با عامت</p></div>
<div class="m2"><p>که چو خود مختصر کند نامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صحبت عام در بهشت ، آباد</p></div>
<div class="m2"><p>مرگ بهتر ، که مرگ عامی باد</p></div></div>