---
title: >-
    شمارهٔ ۳۵۰ - وله ایضا
---
# شمارهٔ ۳۵۰ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ای خواجه سال و ماه تو تلوین همی‌کند</p></div>
<div class="m2"><p>بی‌آنکه علم صنعت اکسیر خوانده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر سیه‌گری ید بیضا نموده‌ای</p></div>
<div class="m2"><p>تلبیس خویش بر همه عالم برانده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی دو موی داری و موی دو روی و خود</p></div>
<div class="m2"><p>سرگشته از دورنگی ایشان بمانده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تزویر کار خویش بدانسته‌ای از آن</p></div>
<div class="m2"><p>رویش سیاه کرده و بر خر نشانده‌ای</p></div></div>