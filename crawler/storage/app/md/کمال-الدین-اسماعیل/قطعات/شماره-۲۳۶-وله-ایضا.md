---
title: >-
    شمارهٔ ۲۳۶ - وله ایضا
---
# شمارهٔ ۲۳۶ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>شبی به خوان تو حاضر شدم به ماه سیام</p></div>
<div class="m2"><p>نخفت چشم من آن شب ز اشتیاق طعام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز روز روزه نبد هیچ فرق آن شب را</p></div>
<div class="m2"><p>ز بهر آنکه نیفتاد اتّفاق طعام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سحور نیز بوقت خودم نیاوردند</p></div>
<div class="m2"><p>وصال روزه معیّن شد از فراق طعام</p></div></div>