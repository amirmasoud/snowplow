---
title: >-
    شمارهٔ ۱۱۱ - وله ایضا
---
# شمارهٔ ۱۱۱ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ای سخا پیشه یی کامید مرا</p></div>
<div class="m2"><p>سوی بخت تو رهنمون آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرتو همّت تو چون آتش</p></div>
<div class="m2"><p>رخ بدین چرخ آبگون آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بدیدم که نشتر از ره فصد</p></div>
<div class="m2"><p>از تن نازنینت خون آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب آمد مرا از ین حالت</p></div>
<div class="m2"><p>هر زمان حیرتم فزون آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم این دست بحر بود،چرا</p></div>
<div class="m2"><p>همچو کان لعل از درون آورد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست سرو، ارغوان شکوفه کند؟</p></div>
<div class="m2"><p>یاسمین بار لاله چون آورد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شفق از افتاب طالع شد ؟</p></div>
<div class="m2"><p>فلک این رسم نو کنون آورد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر الامر معنیی بس خوب</p></div>
<div class="m2"><p>در دلم عقل رهنمون آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست تو بر مثال دریاییست</p></div>
<div class="m2"><p>که دو صد بحر را زبون آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو غوّاص سر بدو در برد</p></div>
<div class="m2"><p>شاخ مرجان ازو برون آورد</p></div></div>