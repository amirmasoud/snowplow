---
title: >-
    شمارهٔ ۴۷ - وله ایضا
---
# شمارهٔ ۴۷ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>هر جا سبکی حرام خواریست</p></div>
<div class="m2"><p>باشه چو پیاله باده نوشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می گیرد عقد زر برانگشت</p></div>
<div class="m2"><p>هر کو چو شمامه زرد گوشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بند، شکم تهیست آن کس</p></div>
<div class="m2"><p>کز طبع چونی شکر فروشست</p></div></div>