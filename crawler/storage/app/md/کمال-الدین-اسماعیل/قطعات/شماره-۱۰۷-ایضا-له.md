---
title: >-
    شمارهٔ ۱۰۷ - ایضا له
---
# شمارهٔ ۱۰۷ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>چون محرّم رسید و عاشورا</p></div>
<div class="m2"><p>خنده بر لب حرام باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز پی ماتم حسین علی</p></div>
<div class="m2"><p>گریه از ابر وام باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لعنت دشمنانش باید گفت</p></div>
<div class="m2"><p>دوستداری تمام باید کرد</p></div></div>