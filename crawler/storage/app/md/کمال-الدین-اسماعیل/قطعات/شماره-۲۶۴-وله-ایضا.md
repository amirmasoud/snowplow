---
title: >-
    شمارهٔ ۲۶۴ - وله ایضا
---
# شمارهٔ ۲۶۴ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>گشت یکباره حضرت خواجه</p></div>
<div class="m2"><p>مجمع ناکسان و بی هنران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز بازار فضل بود و شدست</p></div>
<div class="m2"><p>جای بازاریان و برزگران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیمۀ او مگر ز پاردمست</p></div>
<div class="m2"><p>که درو حاضرند کون خزان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی غلط می کنم که حضرت او</p></div>
<div class="m2"><p>با خطر شد ز جمع بی خطران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مصر جامع شدست زانکه درو</p></div>
<div class="m2"><p>جمع گشتند جمله پیشه وران</p></div></div>