---
title: >-
    شمارهٔ ۳۲۲ - وله ایضا
---
# شمارهٔ ۳۲۲ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>در ضمیرم اگر چه کم گویم</p></div>
<div class="m2"><p>سخن نغز هست بسیاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه کنم دست همّت ممدوح</p></div>
<div class="m2"><p>بر دهانم ز دست مسماری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقد گوهر کجا کنم عرضه</p></div>
<div class="m2"><p>چون نبینم همی خریداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست در روزگار ممدوحی</p></div>
<div class="m2"><p>که ازو نیست بر من انکاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طوطیان خموش می خواهند</p></div>
<div class="m2"><p>تا نیفتد زیان دیناری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبل مرده دوست میدارند</p></div>
<div class="m2"><p>تا نباید علوفه شان باری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>