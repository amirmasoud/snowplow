---
title: >-
    شمارهٔ ۸۵ - این قطعه در پشت تقویم نویسد و با ناصرالّدین منگلی نورّالله قبره فرستد.
---
# شمارهٔ ۸۵ - این قطعه در پشت تقویم نویسد و با ناصرالّدین منگلی نورّالله قبره فرستد.

<div class="b" id="bn1"><div class="m1"><p>جهان پناها،سال نوت همایون باد</p></div>
<div class="m2"><p>کمال عدل تو معمار ربع مسکون باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در اختیار قضایای عالم علوی</p></div>
<div class="m2"><p>رموز کلک تو تقویم ساز گردون باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستوده ناصردین منگلی که طالع تو</p></div>
<div class="m2"><p>قرین طالع اسکندر و فریدون باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دقایق کرمت از شمار بگذشتست</p></div>
<div class="m2"><p>تصاعد در جاتت ز وهم بیرون باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چرخ ملک تو دیوی گر استراق کند</p></div>
<div class="m2"><p>شهاب وار ز رمحت بروشبیخون باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به حلّ عقدة رأس و ذنب گر آری روی</p></div>
<div class="m2"><p>به دست فکر تو آسان شده هم اکنون باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شوق آنکه نهد بوسه برسم اسبت</p></div>
<div class="m2"><p>ز انحنا الف خطّ استوا نون باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر اقتضا که قرآن سعود را باشد</p></div>
<div class="m2"><p>ز اتّصال بدین حضرت همایون باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهندویّی درت گر ز حل نیارد فخر</p></div>
<div class="m2"><p>ز ترکتا ز تو اوجش چو صحن ها مون باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قضا چو نامۀ حکمی بنام عدل تو بست</p></div>
<div class="m2"><p>بدان اجازت قاضیّ چرخ مقرون باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هر غرض که زبان باز کرد سوفارت</p></div>
<div class="m2"><p>زبان خنجر مرّیخ گفته، کایدون باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر آفتاب نه در سایه ات گذارد روز</p></div>
<div class="m2"><p>ز لطمه های کسوفش عذار شبگون باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نوای زهره که در بزم رامش تو زند</p></div>
<div class="m2"><p>چو ضرب تیغ تو در روز رزم موزون باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دبیر چرخ چو اقطاع کاینات دهد</p></div>
<div class="m2"><p>بدست او ز اشارات شاه قانون باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برید گردون هر روز از دگر منزل</p></div>
<div class="m2"><p>به خدمت آمده با مژدۀ دگرگون باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هوای ملک چواز دولت تو معتدلست</p></div>
<div class="m2"><p>بهار عمر ترا روزها بر افزون باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رگی که با تو نه چون مسطر ست بر خط راست</p></div>
<div class="m2"><p>بسان جدول تقویم عرقه در خون باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وصول خسرو سیّارگان به برج شرف</p></div>
<div class="m2"><p>چنان که طالع این سال بر تو میمون باد</p></div></div>