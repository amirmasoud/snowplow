---
title: >-
    شمارهٔ ۲۴۵ - ایضا له
---
# شمارهٔ ۲۴۵ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>ایا رسیده ز فضل و هنر بدان رتبت</p></div>
<div class="m2"><p>که تیر چرخ خطابت کند خداوندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علوّ قدر ترا با فلک نهم همبر</p></div>
<div class="m2"><p>پس آنگهی بنشینم که من خردمندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک شدست غبار ستانة تو و لیک</p></div>
<div class="m2"><p>بر آستان تواش خود غبار نپسندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیث شوق ره مدح بر زبان بگرفت</p></div>
<div class="m2"><p>ماند قوّت از این بیش جان بسی کندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیک کرشمه که با من خیال لطف تو کرد</p></div>
<div class="m2"><p>همه جواهر اشک از نظر بیفکندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمانه از پی اظهار قدر خدمت تو</p></div>
<div class="m2"><p>همه جواهر اشک از نظر بیفکندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمانه از پی اظهار قدر خدمت تو</p></div>
<div class="m2"><p>ز حضرت تو جدا کرد روزکی چندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو از عنایت لطف تو عرصه خالی یافت</p></div>
<div class="m2"><p>به گوشمال حوادث همی دهد پندم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بریده بادا پیوند او ز مرکز خویش</p></div>
<div class="m2"><p>چنانکه چرخ ببرّید از تو پیوندم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشسته بر در و لب کرده مهر و چشم براه</p></div>
<div class="m2"><p>همیشه بهر خبر همچو قفل در بندم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگرچه از فضلات این سرشک نامضبوط</p></div>
<div class="m2"><p>به استین و بدامن بسی پراگندم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نثار خاک درت را ز اشک دزدیده</p></div>
<div class="m2"><p>چو نار اغشیة دل به لعل آگندم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دریچه های نظر را ز بس تزاحم اشک</p></div>
<div class="m2"><p>نمی توان که به مسمار خواب در بندم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فراق تست نه کاری دگر که افتادست</p></div>
<div class="m2"><p>سخن ز گریه چه رانم؟ به خویش می خندم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قسم به ملهم فکری که داد استغنا</p></div>
<div class="m2"><p>بنور صدق ضمیرت ز ذکر سوگندم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که نیست هرگز تشنه به آب و مرده به جان</p></div>
<div class="m2"><p>چنان که من به لقای تو آرزومندم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیادگار من این بیتهای خون آلود</p></div>
<div class="m2"><p>بدار تا بجنایت مگر که پیوندم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که گر نگردم سوی تو زود پای گشاد</p></div>
<div class="m2"><p>جوانب لطفت دست بسته آرندم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فذلک همه تفصیل رنج من این بس</p></div>
<div class="m2"><p>که از لقای شریفت به نامه خرسندم</p></div></div>