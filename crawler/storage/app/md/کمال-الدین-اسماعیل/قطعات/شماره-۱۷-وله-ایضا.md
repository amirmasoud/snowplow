---
title: >-
    شمارهٔ ۱۷ - وله ایضا
---
# شمارهٔ ۱۷ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ندانم چه افتاد مال ترا</p></div>
<div class="m2"><p>که چون خاطرت در پراکندگیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دیناری از وی صلت می کنی</p></div>
<div class="m2"><p>پدیدست کت آخر زندگسیت</p></div></div>