---
title: >-
    شمارهٔ ۱۱۷ - و له ایضاً
---
# شمارهٔ ۱۱۷ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>ای که گر لطف تو فرماندۀ ایّام شود</p></div>
<div class="m2"><p>از جهان قاعدۀ جور و جفا برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ را گویی بنشین و مرو بنشیند</p></div>
<div class="m2"><p>کوه را گویی برخیز و بیا برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر سر کلک تو رویی بخراشد بمثل</p></div>
<div class="m2"><p>وجه ارزاق خلایق ز کجا برخیزد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موج دریا بنشیند ، زند رعد نفس</p></div>
<div class="m2"><p>هرکجا دست جوادت بسخا برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر اشارت رود از قدر تو زی مرکز خاک</p></div>
<div class="m2"><p>از پی خدمت او چست زجا برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تویی آنکس که بتأیید ثنایت هر دم</p></div>
<div class="m2"><p>همه انواع غم از خاطر ما برخیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا که در عهدۀ ارزاق نشست گفت</p></div>
<div class="m2"><p>هرکسی در طلب رزق چرا برخیزد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه از شربت حرمان تو مخمور افتاد</p></div>
<div class="m2"><p>از دم صور بصد رنج و عنا برخیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با کران سنگی حلم تو شگفتم ناید</p></div>
<div class="m2"><p>گرسبکساری از طبع هوا برخیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرخ در حقّ حسود تو شفاعت ناید</p></div>
<div class="m2"><p>که بدافتاد، قضا گفت : که تا برخیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکجا تنگدلی یافت چو غنچه کرمت</p></div>
<div class="m2"><p>بدل آرایی او همچو صبا برخیزد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای کریمی که هرآنکس که بتو باز افتاد</p></div>
<div class="m2"><p>زاستان تو بصد برگ و نوا برخیزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پای طبعم چو شود آبله در راه هوات</p></div>
<div class="m2"><p>عذر لنگ آرد و از راه ثنا برخیزد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیک نتواند خاموش نشستن چه کند ؟</p></div>
<div class="m2"><p>سحری از پی یوزش بدعا برخیزد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در سر آمد ز جفاهای فلک شخص هنر</p></div>
<div class="m2"><p>دست گیرش ز کرم بو که بپا برخیزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نکته یی با تو در اندازم از گستاخی</p></div>
<div class="m2"><p>که کجا لطف تو بنشست حیا برخیزد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فقر را سوی عدم توشه همی باید دار</p></div>
<div class="m2"><p>وین چنین کاری از دست شما برخیزد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر سر راه کرم چشم اهل منتظرست</p></div>
<div class="m2"><p>می چه فرمایی، بنشیند یا برخیزد؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه الطاف الهی مدد جان تو باد</p></div>
<div class="m2"><p>تا که این عالم فانی بفنا برخیزد</p></div></div>