---
title: >-
    شمارهٔ ۳۴۴ - وله ایضا
---
# شمارهٔ ۳۴۴ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ای مقادیر فضل و افضالت</p></div>
<div class="m2"><p>بیش از اندازۀ بیان رهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندرین روزها که گشت هوا</p></div>
<div class="m2"><p>نا خوش و سرد هم بسان رهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که هر دم ز لشکر بهمن</p></div>
<div class="m2"><p>لرزه افتد بر استخوان رهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیم آنست حاش من یسمع</p></div>
<div class="m2"><p>کآورند از عدم نشان رهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاکران را بنزد مخدومان</p></div>
<div class="m2"><p>عذرها هست خاصه آن رهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تن از خدمت تو محرومست</p></div>
<div class="m2"><p>لازم حضرتست جان رهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه شد بسته این زبان چو آب</p></div>
<div class="m2"><p>از دم سرد در دهان رهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفس ز مهریر می خواهد</p></div>
<div class="m2"><p>عذر تقصیر از زبان رهی</p></div></div>