---
title: >-
    شمارهٔ ۳۲۳ - وله ایضا فی مذمة الدّنیا
---
# شمارهٔ ۳۲۳ - وله ایضا فی مذمة الدّنیا

<div class="b" id="bn1"><div class="m1"><p>تا کی این رنج روزگار بری ؟</p></div>
<div class="m2"><p>بار این پیر نابکار بری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهد کن تا ز موج خیز بلا</p></div>
<div class="m2"><p>کشتی عمر بر کنار بری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امن جانها ز حصن اسلامست</p></div>
<div class="m2"><p>کوش تا جان درین حصار بری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترسم از گلبن جهان به طمع</p></div>
<div class="m2"><p>گل نچینیّ و زخم خار بری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزگار تو زان عزیزترست</p></div>
<div class="m2"><p>که تو اندوه فخر و عار بری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مال و نعمت ترا بدان دادند</p></div>
<div class="m2"><p>که تو در بندگی بکار بری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه بدان تا تو ساز جنگ کنی</p></div>
<div class="m2"><p>یا که او را بکارزار بری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به شترمرغ مانی ای خواجه</p></div>
<div class="m2"><p>نه بپّری همی نه بار بری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزگارت ببرد عمر و هنوز</p></div>
<div class="m2"><p>تو بر آنی که روزگار بری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش بود خواب و لذّت مستی</p></div>
<div class="m2"><p>باش تا کیفر خمار بری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زبانی همه دروغ و دغل</p></div>
<div class="m2"><p>دهدت دل که نام یار بری؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آفرینش ترا برد فرمان</p></div>
<div class="m2"><p>گر تو فرمان کردگار بری</p></div></div>