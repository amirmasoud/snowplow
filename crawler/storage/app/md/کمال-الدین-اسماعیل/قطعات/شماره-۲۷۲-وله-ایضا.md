---
title: >-
    شمارهٔ ۲۷۲ - وله ایضا
---
# شمارهٔ ۲۷۲ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>قدوۀ اهل مهانی ای که هست</p></div>
<div class="m2"><p>مهر تو همواره یار غار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مایه از طبع تو اندوزد همی</p></div>
<div class="m2"><p>چشم دریا طبع گوهر بار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدح تو همچون شهادت می رود</p></div>
<div class="m2"><p>بر زبان خامة بیمار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باشمت تا آسمان منّت پذیر</p></div>
<div class="m2"><p>گر زمین گردد ترا رخسار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بارها بستست نوک کلک تو</p></div>
<div class="m2"><p>عقد مروارید اندر بار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با کمال قوّت نظم سخن</p></div>
<div class="m2"><p>قاصرست از شکر تو افکار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کجا شعری فروشم بر کسی</p></div>
<div class="m2"><p>گاه دلّالی و گه سمسار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشتری شاگرد دکّان منست</p></div>
<div class="m2"><p>تا تو دادی رونق بازار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیر گردون خاک بر سر می کند</p></div>
<div class="m2"><p>تا تو بودی راوی اشعار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در جهانت جز غم من غم مباد</p></div>
<div class="m2"><p>ای بتحقیق از کرم غمخوار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خود بجز تیمارکی من خوردمی</p></div>
<div class="m2"><p>گر نخوردی لطف تو تیمار من؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور نباشد پای لطفت در میان</p></div>
<div class="m2"><p>سر به چیزی در نیارد کار من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با خیالت دوش تا وقت سحر</p></div>
<div class="m2"><p>گفت صد بار این دل افکار من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای شفای دردمندان یاد تو</p></div>
<div class="m2"><p>چونی از درد سر بسیار من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پای مردی دیگرم دانی که کیست</p></div>
<div class="m2"><p>خود به خود تمهید کن اعذار من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من به اقبال تو بس مستظهرم</p></div>
<div class="m2"><p>تا قیامت باد استظهار من</p></div></div>