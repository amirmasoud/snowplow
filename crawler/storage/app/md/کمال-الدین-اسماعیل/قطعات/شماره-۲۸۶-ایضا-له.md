---
title: >-
    شمارهٔ ۲۸۶ - ایضا له
---
# شمارهٔ ۲۸۶ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>بس کن ای سرد ناخوش احمق</p></div>
<div class="m2"><p>چند و تا چند حیلت و فن تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش ازینم طمع چو می بودی</p></div>
<div class="m2"><p>به عتابیّ و خزّ ادکن تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می فتادم به خاک و می دادم</p></div>
<div class="m2"><p>بوسه بر پای تو چو دامن تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مرا نیست هیچ بهره از آن</p></div>
<div class="m2"><p>بند و غل باد جامعه بر تن تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببریدم طمع بیکباره</p></div>
<div class="m2"><p>رستم از بارنامه کردن تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برنشینم ازین سپس همه جای</p></div>
<div class="m2"><p>چون زه پیرهن به گردن تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه می خواستم بخواهم گفت</p></div>
<div class="m2"><p>فار غم .... در .... زن تو</p></div></div>