---
title: >-
    شمارهٔ ۶۲ - وله ایضا
---
# شمارهٔ ۶۲ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>نظر می کنم در جهان بخت را</p></div>
<div class="m2"><p>به از درگاه تو منظور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یقین شد ظفر را که در روزگار</p></div>
<div class="m2"><p>بجز رایت خواجه منصور نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا قهر تو سایه بر وی فکند</p></div>
<div class="m2"><p>در آن خطّه خورشید را نور نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دماغ جهان از سر کلک تو</p></div>
<div class="m2"><p>شب و روز بی مشک و کافور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا پیش لطف تو دم می زند</p></div>
<div class="m2"><p>صبا گر به لطف تو مغرور نیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلوص دعا گو بر آن سان که هست</p></div>
<div class="m2"><p>زرای منیر تو مستور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیه کن چو شب روزم، ار صدق من</p></div>
<div class="m2"><p>بنزد تو چون صبح مشهور نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه مرد عتاب تو باشد رهی ؟</p></div>
<div class="m2"><p>که این پایۀ خان و فغفور نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر نیست پذرفته اعذار من</p></div>
<div class="m2"><p>پس اندر جهان هیچ معذور نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دعاییست در دست من، چون کنم؟</p></div>
<div class="m2"><p>مرا چون جز این قدر مقدور نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر آنچ آن صوابست نزدیک تو</p></div>
<div class="m2"><p>دعاگو از آن مصلحت دور نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو کام جهان از برم دور باد</p></div>
<div class="m2"><p>دلم گر بدان خدمت آزور نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه زلّتی هست در جای عفو</p></div>
<div class="m2"><p>مگر شرک کان جرم مغفور نیست</p></div></div>