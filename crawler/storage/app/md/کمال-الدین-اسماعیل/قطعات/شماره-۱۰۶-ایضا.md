---
title: >-
    شمارهٔ ۱۰۶ - ایضاً
---
# شمارهٔ ۱۰۶ - ایضاً

<div class="b" id="bn1"><div class="m1"><p>صدر ملّت که دعا گویی تو</p></div>
<div class="m2"><p>از سر صدق و صفا باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکجال قهر تو پیشانی کرد</p></div>
<div class="m2"><p>خصم را روی قفا باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر بوسیدن خاک در تو</p></div>
<div class="m2"><p>چرخ را پشت دوتا باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا سر انگشت تو بارنده بود</p></div>
<div class="m2"><p>خواهش از ابر چرا باید کرد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابر را تا کف تو ناموزد</p></div>
<div class="m2"><p>او چه داند که عطا باید کرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برامید دم خلقت ما را</p></div>
<div class="m2"><p>روی در روی صبا باید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو را تربیت اهل هنر</p></div>
<div class="m2"><p>نیک دانی که ترا باید کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه بی کار نبی ، یک ساعت</p></div>
<div class="m2"><p>نیز در کار خدا باید کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ورچه عالی نظری از سر لطف</p></div>
<div class="m2"><p>نظری هم سوی ما باید کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ماجرا ئیست دعا گوی ترا</p></div>
<div class="m2"><p>که بناچار ادا باید کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون حیا مانع روزی آمد</p></div>
<div class="m2"><p>لاجرم ترک حیا باید کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه حیا ترک حیات اولیتر</p></div>
<div class="m2"><p>زآنکه مرسوم رها باید کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>داده یی وعدۀ تشریف رهی</p></div>
<div class="m2"><p>لابد آن وعده وفا باید کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر صوا بست همه ساله کنی</p></div>
<div class="m2"><p>ورنه یکبار خطا باید کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وجه قرضی که مرا جمع شدست</p></div>
<div class="m2"><p>نیک دانم زکجا باید کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه سر سبزی انعام تو باد</p></div>
<div class="m2"><p>کوشناسد که چها باید کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن آینده ادا خود باشد</p></div>
<div class="m2"><p>آن بگذشته قضا باید کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من بانعام تو حاجتمندم</p></div>
<div class="m2"><p>حاجت بنده روا باید کرد</p></div></div>