---
title: >-
    شمارهٔ ۲۹۲ - و له ایضا فی صفة صندوقچه
---
# شمارهٔ ۲۹۲ - و له ایضا فی صفة صندوقچه

<div class="b" id="bn1"><div class="m1"><p>ای از پی حلّ و عقد دایم</p></div>
<div class="m2"><p>در بند و گشایش او فتاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز با محرم ز غایت حفظ</p></div>
<div class="m2"><p>راز دل خود برون نداده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرکوفته یی و از صلابت</p></div>
<div class="m2"><p>هم بر سر پای ایستاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بلعجی زبانت اوّل</p></div>
<div class="m2"><p>گویا شده پس دهان گشاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاموشی و گاه نطق لفظت</p></div>
<div class="m2"><p>بی صورت همه حروف ساده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویا بزبان حال کز من</p></div>
<div class="m2"><p>نتوان طلبید نا نهاده</p></div></div>