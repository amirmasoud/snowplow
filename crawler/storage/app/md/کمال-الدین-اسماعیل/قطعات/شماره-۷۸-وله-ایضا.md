---
title: >-
    شمارهٔ ۷۸ - وله ایضا
---
# شمارهٔ ۷۸ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>عالم لطف علاء الدّین معلومت هست</p></div>
<div class="m2"><p>که مرا بر تو زبان جز به ثنا می نرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تو مهریست مرا هردم ازین روی چو صبح</p></div>
<div class="m2"><p>سخنم با تو جزا ز صدق و صفا می نرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدر از کلک تو انگشت بد ندان بر دست</p></div>
<div class="m2"><p>که چون تو کس به سر سرّ قضا می نرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلم منشی دیوان فتوّت امروز</p></div>
<div class="m2"><p>جز به پروانۀ فرمان شما می نرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ جایی نرود خاطر خورشید وشت</p></div>
<div class="m2"><p>که معنایش چو سایه ز قفا می نرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذات پر معنی تو خود همه محض هنرست</p></div>
<div class="m2"><p>ذکر لطف و کرم و فضل و سخا می نرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوستان بسزا را چو فراموش کنی</p></div>
<div class="m2"><p>نیک می دان که ز تو این بسزا می نرود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نپندارد لطف تو کزو این گله ها</p></div>
<div class="m2"><p>هر سحر گاهی با باد صبا می نرود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه در خدمت تخفیف نگه میدارم</p></div>
<div class="m2"><p>هیچ تقصیری در باب دعا می نرود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باد تو می نرود یک نفس از خاطر من</p></div>
<div class="m2"><p>ورچه بر خاطر تو یاد ز ما می نرود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیوفایی مکن ای خواجه که در این شیوه</p></div>
<div class="m2"><p>که ترا می برود کار مرا می نرود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من ندانم که چه کردست وفا در عهدت</p></div>
<div class="m2"><p>که دمی عهد تو خود راه وفا می نرود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه خیالست خیالت را؟ با من می گوی</p></div>
<div class="m2"><p>که یکی لحظه ام از پیش فرا می نرود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر خطا چون که قلم می نرود بهر چرا؟</p></div>
<div class="m2"><p>نام ما بر قلم تو بخطا می نرود</p></div></div>