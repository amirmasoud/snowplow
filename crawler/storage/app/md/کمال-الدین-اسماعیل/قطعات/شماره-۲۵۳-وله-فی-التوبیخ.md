---
title: >-
    شمارهٔ ۲۵۳ - وله فی التّوبیخ
---
# شمارهٔ ۲۵۳ - وله فی التّوبیخ

<div class="b" id="bn1"><div class="m1"><p>من که شب و روز گنه می کنم</p></div>
<div class="m2"><p>بندگی حرص و شره می کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرم ندارم که به موی سپید</p></div>
<div class="m2"><p>نامۀ اعمال سیه می کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست ز من باز پس افتاده تر</p></div>
<div class="m2"><p>از چپ و از راست نگه می کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیّت توبت کنم و بر عقب</p></div>
<div class="m2"><p>ای یکی معصیه ده می کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طعمۀ سگ را نپسندد خرد</p></div>
<div class="m2"><p>آنچه منش توشۀ ره می کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طرفه تر اینست که بر درگهش</p></div>
<div class="m2"><p>عذر گنه هم به گنه می کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با همه نا اهلی خود گه گهی</p></div>
<div class="m2"><p>بر در او پشت دوته می کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم نشوم از کرمش نا امید</p></div>
<div class="m2"><p>گر چه همه کار تبه می کنم</p></div></div>