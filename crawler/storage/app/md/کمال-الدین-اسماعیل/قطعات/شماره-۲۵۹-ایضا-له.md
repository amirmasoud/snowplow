---
title: >-
    شمارهٔ ۲۵۹ - ایضا له
---
# شمارهٔ ۲۵۹ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>پریر جود تو با من حدیث بخشش کرد</p></div>
<div class="m2"><p>ز بهر آنکه منش شکر جاودان گویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از تکلّف گفتم که نی معاذالله</p></div>
<div class="m2"><p>که من ثنای تو از بهر سوزیان گویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگویش خویش فرو گوی از زبان رهی</p></div>
<div class="m2"><p>تو کار خویش همی کن که من خود آن گویم</p></div></div>