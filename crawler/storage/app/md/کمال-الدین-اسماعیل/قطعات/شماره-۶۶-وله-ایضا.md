---
title: >-
    شمارهٔ ۶۶ - وله ایضا
---
# شمارهٔ ۶۶ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>خدایگان کریمان مشرق و مغرب</p></div>
<div class="m2"><p>که همّتت سر اجرام آسمان بفراشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرد خانة اندیشه بر صحیفة دل</p></div>
<div class="m2"><p>لطیف تر ز ثنای تو صورتی ننگاشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عطای دست او بر مدح من سبق می برد</p></div>
<div class="m2"><p>و لیک عاقبتش بخت شور من نگذاشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه بنده بمقدار وسع خود دانم</p></div>
<div class="m2"><p>بدت کم طمعی چشمۀ نیاز انباشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرور ملک قناعت چو در دماغ گرفت</p></div>
<div class="m2"><p>همه خزااین عالم از آن خود پنداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مساس حاجت چندان که کرد تحریضش</p></div>
<div class="m2"><p>به ذرّ ه یی نظر حرص بر جهان نگماشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و لیک رتبت تشریف تو از آن بیشست</p></div>
<div class="m2"><p>که بی حصول و فواتش یکی توان انگاشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جواب لطف تو دید و زمین حضرت تو</p></div>
<div class="m2"><p>امید گفت که تخم طمع بباید کاشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرم گر از تو نبینم پس از که خواهم دید؟</p></div>
<div class="m2"><p>طمع گراز تو ندارم پس از که خواهم داشت؟</p></div></div>