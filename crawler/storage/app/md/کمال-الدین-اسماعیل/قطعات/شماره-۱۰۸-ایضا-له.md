---
title: >-
    شمارهٔ ۱۰۸ - ایضا له
---
# شمارهٔ ۱۰۸ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>شنیدم که مخدوم اهل هنر</p></div>
<div class="m2"><p>به سمع رضا شعر من گوش کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ذوقی تمام آن شراب گران</p></div>
<div class="m2"><p>که من داده بودم سبک نوش کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو سرمست شد فکر تش زان شراب</p></div>
<div class="m2"><p>که جان را به یک جرعه بیهوش کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشد با عروسان افکار من</p></div>
<div class="m2"><p>دو دست قبول اندر آغوش کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولیکن چو کابینشان خواست کرد</p></div>
<div class="m2"><p>به اقبال من خود فراموش کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بخشش همی راند کلکش سخن</p></div>
<div class="m2"><p>ندانم مر او را که خاموش کرد</p></div></div>