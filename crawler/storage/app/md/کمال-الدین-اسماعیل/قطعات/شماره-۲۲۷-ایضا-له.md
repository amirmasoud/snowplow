---
title: >-
    شمارهٔ ۲۲۷ - ایضا له
---
# شمارهٔ ۲۲۷ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>ای شده ذات تو مستجمع انواع کمال</p></div>
<div class="m2"><p>نو عروسان سخن راز ثنای تو جمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم صریر قلمت ترجمۀ لفظ کرم</p></div>
<div class="m2"><p>هم صدای سخنت طیره ده سحر حلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره فهم معانی تو ارباب سخن</p></div>
<div class="m2"><p>بس که کرده اند سقط یاوگی وهم و خیال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیشکر را ز حسد طعم دهان تلخ شدست</p></div>
<div class="m2"><p>تا به باغ سخن از کلک تو بر رست نهال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آبدارست ز رشح کرمت تیغ هنر</p></div>
<div class="m2"><p>در عرق غرق ز شرم سخنت آب زلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو با صورت دیوار در آیی بسخن</p></div>
<div class="m2"><p>جانور گردد از لطف حدیثت در حال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ گفتا من و قدرت، خردش گفت خموش</p></div>
<div class="m2"><p>که ز پیران نبود خوب سخنهای محال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد دهان سخن از شکر گفت گوشا گوش</p></div>
<div class="m2"><p>تا کنار طمع از جود تو شد مالامال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پی جلوه گه مدح آراسته اند</p></div>
<div class="m2"><p>خوب رویان سخن راخم زلف و خط و خال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا که شد طبع کریم تو خریدار سخن</p></div>
<div class="m2"><p>تیر گردون را بررست ز شادی پر و بال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای ز الفاظ تو تنگ آمده بر شکّر جای</p></div>
<div class="m2"><p>در ثنای تو سخن را چه فراخست مجال؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با بزرگیّ تو هم جای سخن باشد نیز</p></div>
<div class="m2"><p>گر فرستیم بجای سخنت عقد لآل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از طلبکاری تو سر به فلک باز نهد</p></div>
<div class="m2"><p>سخن بنده که باریک تر امد ز هلال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو سخن خواسته یی از من و من خود همه عمر</p></div>
<div class="m2"><p>خواستم تا سخن خویش رسانم بکمال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لیک معذور همی دار که از دهشت تو</p></div>
<div class="m2"><p>شد زبان سخن اندر دهن ناطقه لال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باد پایان سخن را قلمم زان پی کرد</p></div>
<div class="m2"><p>تا از ایشان به بساطت نرسد گرد هلال</p></div></div>