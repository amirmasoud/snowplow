---
title: >-
    شمارهٔ ۳۲ - ایضا له
---
# شمارهٔ ۳۲ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>بخدایی که وصف بیچونیش</p></div>
<div class="m2"><p>بر اشارات انبیا رفتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قلم استقامت صنعش</p></div>
<div class="m2"><p>همه بر خطّ استوا رفتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر بندگان بخواهد راند</p></div>
<div class="m2"><p>هر چه اندر ازل قضا رفتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاندرین مدّت دراز آهنگ</p></div>
<div class="m2"><p>که ز عهد فراق ما رفتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه خیالت زچشم دورشدست</p></div>
<div class="m2"><p>نه ز دل یاد تو فرا رفتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ضمیرم همه ثنای تو بود</p></div>
<div class="m2"><p>بر زبانم همه دعل رفتست</p></div></div>