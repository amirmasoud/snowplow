---
title: >-
    شمارهٔ ۹۷ - وله ایضا
---
# شمارهٔ ۹۷ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>به عمرهای چنین تیز تاز زود گذر</p></div>
<div class="m2"><p>فراقهای چنین دیر یاز در نخورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر به قرصۀ خورشید برکشد آنها</p></div>
<div class="m2"><p>که در فراق تو بر جان ما همی گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهان مشرق ازو نام بر زبان نارد</p></div>
<div class="m2"><p>گلوی مغرب از او لقمه یی فرو نبرد</p></div></div>