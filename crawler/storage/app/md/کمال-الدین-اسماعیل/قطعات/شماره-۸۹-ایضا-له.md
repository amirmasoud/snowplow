---
title: >-
    شمارهٔ ۸۹ - ایضاً له
---
# شمارهٔ ۸۹ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>تا از این نام ازل برره دل دام نهد</p></div>
<div class="m2"><p>ای بسا جان که سر اندر سر این نام نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دام دلگیر بگسترد ز بسم الله و پس</p></div>
<div class="m2"><p>دانۀ نقطهّ با در پس آن دام نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از رحیمیّ وز رحمانی آغاز گرفت</p></div>
<div class="m2"><p>تا دل سوخته دل بر طمع خام نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا درین نام شود هر دو جهان مستغرق</p></div>
<div class="m2"><p>لاجرم اوّل نام از الف و لام نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مایۀ رامش و آرامش جانست این نام</p></div>
<div class="m2"><p>که درو از پی دل مایۀ ارام نهاد</p></div></div>