---
title: >-
    شمارهٔ ۷۵ - وله ایضا
---
# شمارهٔ ۷۵ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>صدرا روا مدار ز انعام خود مرا</p></div>
<div class="m2"><p>مرحوم مانده دایم و آنرا بهانه هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز بامداد نهم رخ به درگهت</p></div>
<div class="m2"><p>یک دل پر از امید وپس آنگه شبانه هیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندین هزار تیر معانی ز شست طبع</p></div>
<div class="m2"><p>کردم گشاد و نامداز آن بر نشانه هیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنجاه سال خدمت این خانه کرده ام</p></div>
<div class="m2"><p>و امروز نیست همره من جز فسانه هیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مستحقّ هیچ نیم من و آفتاب چرخ</p></div>
<div class="m2"><p>پس نیست مستحّق عطا در زمانه هیچ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از طالعست این که من و آفتاب چرخ</p></div>
<div class="m2"><p>مشهور عالمیم و براین آستانه هیچ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زانم همی دهی که ترا در خزانه نیست</p></div>
<div class="m2"><p>یعنی کریم را نبود در خزانه هیچ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لایق بود ز نعمت تو هر که درجهان</p></div>
<div class="m2"><p>اندر میان نعمت و من بر کرانه هیچ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر منهج امید من از وعده های تو</p></div>
<div class="m2"><p>دامیست بس شگرف و در آن دام دانه هیچ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در رستۀ قبول تو بازار من قویست</p></div>
<div class="m2"><p>لیکن چه حاصلست چو نارم به خانه هیچ؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد چون دهان دلبر من وعده های تو</p></div>
<div class="m2"><p>سرچشمۀ حیات و خود اندر میانه هیچ</p></div></div>