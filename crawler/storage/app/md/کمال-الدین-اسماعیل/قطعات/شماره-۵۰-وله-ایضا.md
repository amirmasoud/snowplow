---
title: >-
    شمارهٔ ۵۰ - وله ایضا
---
# شمارهٔ ۵۰ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>خواجه وقت ستدن سخت پیست</p></div>
<div class="m2"><p>بگه دادن اگر سست رگست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به چشمست به صورت لیکن</p></div>
<div class="m2"><p>نجس العین به معنی چو سگست</p></div></div>