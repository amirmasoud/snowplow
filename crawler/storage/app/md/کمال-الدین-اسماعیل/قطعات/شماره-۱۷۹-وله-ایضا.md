---
title: >-
    شمارهٔ ۱۷۹ - وله ایضا
---
# شمارهٔ ۱۷۹ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>هر کرا رسم و عادت آن باشد</p></div>
<div class="m2"><p>که همه ساله گیرد و ندهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وعده یی گر دهد ترا در عمر</p></div>
<div class="m2"><p>اندر آن غصّه میرد و ندهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بخیلان بخیل تر که بود</p></div>
<div class="m2"><p>آنکه چیزی پذیرد و ندهد</p></div></div>