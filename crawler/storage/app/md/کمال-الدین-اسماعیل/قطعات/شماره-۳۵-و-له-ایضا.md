---
title: >-
    شمارهٔ ۳۵ - و له ایضاً
---
# شمارهٔ ۳۵ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>زهی بلند جنایی که سایۀ جاهت</p></div>
<div class="m2"><p>همیشه بر سر خورشید آسمان گردست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بروزگار تو مه شد بشب روی منسوب</p></div>
<div class="m2"><p>زهیبت تور رخش زان چو زعفران زردست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآفتابش اگرچه هزار دلگرمیست</p></div>
<div class="m2"><p>بنزد خاطر تو صبحدم همان سردست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر اندر دیدۀ منت سیل بر جهان و هنوز</p></div>
<div class="m2"><p>میان شادی و طبعم همان چنان گردست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس که در دل من دردهای بسیارست</p></div>
<div class="m2"><p>نمی توانم گفتن مرا فلان در دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه بنده ز آثار بی عنایتیت</p></div>
<div class="m2"><p>ز هرچه شغل و عمل بود این زمان فردست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خاک پای تو بیزارم ار کسی هرگز</p></div>
<div class="m2"><p>چو بنده خدمت تو از میان جان کردست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوسال شد که زحرمان همی زند نشخوار</p></div>
<div class="m2"><p>زنعمتی که ازین پیش در جهان خوردست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زگلستان عزایت چو قسم من خوارست</p></div>
<div class="m2"><p>مرا در آنچه که در دست دیگران ور دست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حکایت من و این کارنامه ها اکنون</p></div>
<div class="m2"><p>همایون کلید در جامعه دان و آن مردست</p></div></div>