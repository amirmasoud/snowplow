---
title: >-
    شمارهٔ ۲۲۲ - ایضا له
---
# شمارهٔ ۲۲۲ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>در جستن رضای تو عمری بقدر وسع</p></div>
<div class="m2"><p>بردم بکار هر چه توانستم از حیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقدور آدمی دل و تن باشد و زبان</p></div>
<div class="m2"><p>کردم برای خدمت تو هر سه مبتدل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن خدمت تو کرد و زبان مدحت تو گفت</p></div>
<div class="m2"><p>دل در خلوص معتقدی داشت بی خلل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بعد از این همه ز تو اینست حاصلم</p></div>
<div class="m2"><p>معلوم شد مرا که جز اینست بر عمل</p></div></div>