---
title: >-
    شمارهٔ ۳۴۲ - ایضا له
---
# شمارهٔ ۳۴۲ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>فرستادم بخدمت کاردی خوب</p></div>
<div class="m2"><p>که ارزد گوهر او هر چه خواهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببین بر دسته تیغش ، گر ندیدی</p></div>
<div class="m2"><p>زبان مار در دندان ماهی</p></div></div>