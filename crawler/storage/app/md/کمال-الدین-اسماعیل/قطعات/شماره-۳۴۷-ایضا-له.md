---
title: >-
    شمارهٔ ۳۴۷ - ایضا له
---
# شمارهٔ ۳۴۷ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>ز ابر چون برف سیم باریدی</p></div>
<div class="m2"><p>گر بدی چون دل تو دریایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد اومید با کفت گستاخ</p></div>
<div class="m2"><p>هر زمان می کند تمنّایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز چرخ خرف دگر باره</p></div>
<div class="m2"><p>با من از سر گرفت ایذایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناگهان در میان فصل ربیع</p></div>
<div class="m2"><p>برفی آغاز کرد و سرمایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز استینم برون نشد دستی</p></div>
<div class="m2"><p>ز استانم برون نشد پایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه ز انگشت آتشم تبشی</p></div>
<div class="m2"><p>نه ز هیزم خلال بالایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طمع خام گفت رو لختی</p></div>
<div class="m2"><p>هیزم آخر بخواه از جایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چو در مطبخ تو چیزی نیست</p></div>
<div class="m2"><p>ما بدان می پزیم سودایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر سخای تو مصلحت بیند</p></div>
<div class="m2"><p>بکند اینقدر مواسایی</p></div></div>