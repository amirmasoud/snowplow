---
title: >-
    شمارهٔ ۹۱ - ایضا له
---
# شمارهٔ ۹۱ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>کریم طبع سخی دل کسی بود کانعام</p></div>
<div class="m2"><p>به دست خویش کند گاه و گاه بفرستد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر به شهر بود خود عطای او نقدست</p></div>
<div class="m2"><p>وگرنه نباشد از آن جایگاه بفرستد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آفتاب که چون حاضرست نور دهد</p></div>
<div class="m2"><p>چو گشت غایب، بر دست ماه بفرستد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توقعّست کز آنجا که دلنوازی تست</p></div>
<div class="m2"><p>رسوم خادم داعی بگاه بفرستد</p></div></div>