---
title: >-
    شمارهٔ ۲۰۳ - وله ایضا
---
# شمارهٔ ۲۰۳ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ای کریم جهان، خبر داری؟</p></div>
<div class="m2"><p>که شدم ز انتظار تو بیمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منفعل شد مزاج طبعم از آنک</p></div>
<div class="m2"><p>شربت صبر می خورد بسیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که می گردد از قرار هجو</p></div>
<div class="m2"><p>رود گانی خاطرم افکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترسم از من رها شود حاشا</p></div>
<div class="m2"><p>در هجای تو بیتکی سه چهار</p></div></div>