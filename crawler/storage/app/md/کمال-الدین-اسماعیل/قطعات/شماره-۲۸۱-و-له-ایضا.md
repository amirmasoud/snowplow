---
title: >-
    شمارهٔ ۲۸۱ - و له ایضاً
---
# شمارهٔ ۲۸۱ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>ای به گه جود چو گل تازه رو</p></div>
<div class="m2"><p>داده کفت آرزوی آرزو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آتش خشم تو آب آمده</p></div>
<div class="m2"><p>بر لب شمشیر تو جان عدو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح اگر دم بخلافت زند</p></div>
<div class="m2"><p>بشکندش پای نفس در گلو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاسمن از دست گل خلق تو</p></div>
<div class="m2"><p>خورده قفابر سر هر چارسو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده بر اعدای تو اقبال پشت</p></div>
<div class="m2"><p>برده زدرگاه تو چرخ آبرو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پردۀ هرکس که بدرّیده فقر</p></div>
<div class="m2"><p>سوزن انعام تو کردش رفو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه نیلاورد درت را نماز</p></div>
<div class="m2"><p>کرد بخون جگر خود وضو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با کف در یار تو هر دم زرشک</p></div>
<div class="m2"><p>ابر زند بر رخ دریا تفو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای ز منی مهر تو مارا مدام</p></div>
<div class="m2"><p>خانۀ دل پر طرب و های و هو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من که دعاگوی توام روز و شب</p></div>
<div class="m2"><p>کرده درین خدمت از جان غلو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه مرا هست بخروار فضل</p></div>
<div class="m2"><p>نیست زدانگانه مرا یک تسو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گاه برهنه قدمم همچو سرو</p></div>
<div class="m2"><p>گاه برهنه ست سرم چون کدو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طاق و رواقم زیکی طاق بود</p></div>
<div class="m2"><p>خود بجهان طاق نبودست دو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دوچو دوهم لفظ خراسانیست</p></div>
<div class="m2"><p>عفو کن این لحن که هستی عفو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درید غصبست و تلف گشتنش</p></div>
<div class="m2"><p>هست معلّق بیکی تارمو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بشنو و بر طاق منه این سخن</p></div>
<div class="m2"><p>تا نکنم تاج سر از خاک کو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جز که ز انعام تو اکنون مرا</p></div>
<div class="m2"><p>وجه دگر طاق در افاق کو؟</p></div></div>
<div class="b2" id="bn18"><p>ما حال من کان له واحد</p>
<p>غیّب عنه ذلک الواحد</p></div>