---
title: >-
    شمارهٔ ۴۶ - وله ایضا
---
# شمارهٔ ۴۶ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>نور دین ای که در فنون هنر</p></div>
<div class="m2"><p>فضل تو همچو نور مشهور ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جزوی از سر گذشت خامۀ تست</p></div>
<div class="m2"><p>هر چه اندر کتاب مسطور ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با عروسان خاطرت ما را</p></div>
<div class="m2"><p>فکر بر نقص حور مقصورست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاب مهر تو تا به من پیوست</p></div>
<div class="m2"><p>همچو صبحم دلی پر از نورست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لطف تو عام و خاص در حق من</p></div>
<div class="m2"><p>دایماً سعیهای مشکورست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به خدمت نمی رسد داعی</p></div>
<div class="m2"><p>اندرین چند روزه معذورست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>متصدّی عذر می نشوم</p></div>
<div class="m2"><p>که نه چیزست آن که مقدورست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب تا ناف و وحل تا زانو</p></div>
<div class="m2"><p>پای من لنگ و راه من دورست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جرم بدبختیی منه بروی</p></div>
<div class="m2"><p>که به لطف تو نیک مغرورست</p></div></div>