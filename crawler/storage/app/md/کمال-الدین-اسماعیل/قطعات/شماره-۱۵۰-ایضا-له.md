---
title: >-
    شمارهٔ ۱۵۰ - ایضا له
---
# شمارهٔ ۱۵۰ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>ای که خورشید بی رضای تو سر</p></div>
<div class="m2"><p>از گریبان صبح بر نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز بعون بنان تو دریا</p></div>
<div class="m2"><p>دامن ابر پر گهر نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوه دستی که زیر سنگ ز تست</p></div>
<div class="m2"><p>با وقار تو در کمر نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خادم ارچه ز اعتماد کرم</p></div>
<div class="m2"><p>که گهی لفظ پاکتر نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست راد ترا ز گستاخی</p></div>
<div class="m2"><p>بحر خواند وزان حذر نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش لطفت ادب نگهدارد</p></div>
<div class="m2"><p>سخن طوطی و شکر نکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر تو او را غلام خود خوانی</p></div>
<div class="m2"><p>با همه خواجه سر بسر نکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر همّت تو بس عالیست</p></div>
<div class="m2"><p>زان بکارش درون نظر نکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیک دانی که خادم داعی</p></div>
<div class="m2"><p>خدمت تو ز بهر زر نکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لیک معذور نیست نزد خرد</p></div>
<div class="m2"><p>که ز حال خودت خبر نکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه از غایت غوایت جهل</p></div>
<div class="m2"><p>کرد کاری که هیچ خبر نکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سفری کرد ناگهان و کسی</p></div>
<div class="m2"><p>ارتکاب چنین خطر نکند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روزگارش همی کند تادیب</p></div>
<div class="m2"><p>تا چنین کارها دگر نکند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا در این شهر آمدست رهی</p></div>
<div class="m2"><p>جز ثنایت همی ز بر نکند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رفت ماهی و هیچکس سوی او</p></div>
<div class="m2"><p>التفاتی بخیر و شر نکند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وجه ترتیب قوت خود هر شب</p></div>
<div class="m2"><p>جز ز خونابۀ جگر نکند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خانه یی دارد آنچنان که درو</p></div>
<div class="m2"><p>هیچ دیوانه یی مقر نکند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زین سیه چاه گونۀ دلگیر</p></div>
<div class="m2"><p>کافتاب از برش گذر نکند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خاکش از مدبری بدان رتبت</p></div>
<div class="m2"><p>کش صبا نیز پی سپر نکند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من نشسته به انتظار که وای</p></div>
<div class="m2"><p>اگرم خواجه بهره ور نکند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گاه گویم فراموشم کردست</p></div>
<div class="m2"><p>گاه گویم که نی ، مگر نکند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گاه خود را همی دهم عشوه</p></div>
<div class="m2"><p>کو عطاهای مختصر نکند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حرص می گویدم کند لابد</p></div>
<div class="m2"><p>عقل می گویدم وگر نکند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روز و شب خاطرم در این سوداست</p></div>
<div class="m2"><p>که دمی از خودش بدر نکند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو خود از کارمن چنان فارغ</p></div>
<div class="m2"><p>کین سخن در تو هیچ اثر نکند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>غم اهل هنر تو خورکاینجا</p></div>
<div class="m2"><p>کس همی یاد از هنر نکند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بحر جود ترا چه عذر بود؟</p></div>
<div class="m2"><p>که لبی خشک گشته تر نکند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لایق او بساز ترتیبی</p></div>
<div class="m2"><p>کو قناعت به ماحضر نکند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یا بفرمای توشۀ راهش</p></div>
<div class="m2"><p>آنچنان کش از آن گذر نکند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یاش سوگند ده که تا پس از این</p></div>
<div class="m2"><p>بر بدیهه چنین سفر نکند</p></div></div>