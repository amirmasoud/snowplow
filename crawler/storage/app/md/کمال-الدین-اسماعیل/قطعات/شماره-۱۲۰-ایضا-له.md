---
title: >-
    شمارهٔ ۱۲۰ - ایضا له
---
# شمارهٔ ۱۲۰ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>ای بزرگی که بر علم تو ظاهر باشد</p></div>
<div class="m2"><p>هر چه مدفون زوایای سرایر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر زمان کلک تو چون آب فرو می خواند</p></div>
<div class="m2"><p>هر چه بر صفحة اسرار ضمایر باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گام بر تارک خورشید گزارد ز شرف</p></div>
<div class="m2"><p>گر عطارد نفسی با تو مناظر باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در نفسهای تو هرگه که کنی نشر علوم</p></div>
<div class="m2"><p>هست بویی که در انفاس مجاهر باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اقتضغی همه اسباب سعادات کند</p></div>
<div class="m2"><p>هر تاره که بدان رای تو ناظر باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پی فایده در حلقة درست برجیس</p></div>
<div class="m2"><p>چون جواب تو بسی خواست که حاضر باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده را نیز خیالست که بی استحقاق</p></div>
<div class="m2"><p>اندر آن حلقه هم از جمع اصاغر باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه در خدمت صدر تو هنرمندانند</p></div>
<div class="m2"><p>وین رهی باردل و زحمت خاطر باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لیک شرطست که برخوان ملوک از پی رسم</p></div>
<div class="m2"><p>تره اول و حلوا آخر باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آهن ارچند ندارد خطری،بازرسرخ</p></div>
<div class="m2"><p>در ترازو بگه وزن مجاور باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جنبش هفت فلک بر نهنج کام تو باد</p></div>
<div class="m2"><p>تا که اجسام مرکّب ز عناصر باشد</p></div></div>