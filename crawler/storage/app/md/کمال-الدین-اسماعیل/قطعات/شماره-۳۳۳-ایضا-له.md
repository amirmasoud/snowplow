---
title: >-
    شمارهٔ ۳۳۳ - ایضا له
---
# شمارهٔ ۳۳۳ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>کرده یی ژار با من انعامی</p></div>
<div class="m2"><p>که کم و کیف آن تو خود دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسم پارم همی دهی امسال</p></div>
<div class="m2"><p>یا از آن داده هم پشیمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا تمامست این کرم امسال</p></div>
<div class="m2"><p>کآن پارینه باز نستانی؟</p></div></div>