---
title: >-
    شمارهٔ ۳۲۰ - در مدح صدر قوام الملّة و الدّین ابراهیم بنداری گوید و به دمشق فرستد.
---
# شمارهٔ ۳۲۰ - در مدح صدر قوام الملّة و الدّین ابراهیم بنداری گوید و به دمشق فرستد.

<div class="b" id="bn1"><div class="m1"><p>نسیم باد صبا هیچ عزم آن داری</p></div>
<div class="m2"><p>که این تکاسل طبعیّ خویش بگذاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پای تو چو دو گامست طول و عرض جهان</p></div>
<div class="m2"><p>به گاه قطع مسافت ز تیز رفتاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توقّعی ز تو دارم ز روی همنفسی</p></div>
<div class="m2"><p>اگر به ثقل نداری و رنج نشماری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تجشّمی کن و یکدم به کارها پرداز</p></div>
<div class="m2"><p>ناتوانی اگر چه مزاج‌ها داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سحرگهی که به عون دعای شبخیزان</p></div>
<div class="m2"><p>سبک ترک شده باشی ز رنج بیماری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اصفهان حرکت کن به شام صبحدمی</p></div>
<div class="m2"><p>بنزد صدرافاضل قوام بنداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو با نسایم اخلاق او در آمیزی</p></div>
<div class="m2"><p>ز روی نسبت هم پیشگیّ و همکاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگاه دار ز بهر دماغ مشتاقان</p></div>
<div class="m2"><p>ز خاک پایش اگر شمّه یی بدست آری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ودیعه های دعا و ثنای من چندان</p></div>
<div class="m2"><p>که حصر آن متعذّر بود ز بسیاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سزد که بر طبق شوق و معرض اخلاص</p></div>
<div class="m2"><p>چنان که من بسپارم تو نیز بسپاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از آن سپس که ببوسی زمین حضرت او</p></div>
<div class="m2"><p>پیام من به زبان ثناتو بگزاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگو که ای ز معانیّ خوب و سیرت نیک</p></div>
<div class="m2"><p>بجای آنکه بهر مدحتی سزاواری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به رسته های چمن بر مجاهزان بهار</p></div>
<div class="m2"><p>همه ز کیسۀ خلقت کنند عطّاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو تو عرایس افکار خویش جلوه دهی</p></div>
<div class="m2"><p>شود ز شرم رخت آفتاب گلناری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سیاه روی کند همچو زاغ طوطی را</p></div>
<div class="m2"><p>زبان کلک تو انکام نغز گفتاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ستارگان فلک با کمال شبخیزی</p></div>
<div class="m2"><p>ز دولت تو کنند التماس بیداری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز عکس خون دل حاسدان تو هر شام</p></div>
<div class="m2"><p>چو مغز پسته شود آسمان زنگاری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز هیچگونه برین صوب ما نمی گذری</p></div>
<div class="m2"><p>دل تو عادت راحت گرفت پنداری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا چو نام شریف تو بر زبان گذرد</p></div>
<div class="m2"><p>ز لب به چشم رسد نوبت گهر باری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز معظمات امور ارچه نیست پروایت</p></div>
<div class="m2"><p>که نام ها به سرانگشت لطف بنگاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از آن مکارم اخلاق نیست مستبعد</p></div>
<div class="m2"><p>که یاد می کند از ما به وقت بیکاری</p></div></div>