---
title: >-
    شمارهٔ ۱۱۹ - وله ایضا
---
# شمارهٔ ۱۱۹ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>می کنی دوستیّ دشمن من</p></div>
<div class="m2"><p>تا از آن حشمتت فزون باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود جز این چشمم از تو کی دارد</p></div>
<div class="m2"><p>هر که را عقل رهنمون باشد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چنین دون و بدگهر که تویی</p></div>
<div class="m2"><p>به منت التفات چون باشد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردمان سوی مردمی یا زند</p></div>
<div class="m2"><p>میل دونان بسوی دون باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل را راه در دماغ بود</p></div>
<div class="m2"><p>تیز را رهگذار ... باشد</p></div></div>