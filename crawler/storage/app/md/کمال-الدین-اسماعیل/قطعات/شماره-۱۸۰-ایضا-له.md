---
title: >-
    شمارهٔ ۱۸۰ - ایضا له
---
# شمارهٔ ۱۸۰ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>مدحتی گفتمت که چون زیور</p></div>
<div class="m2"><p>در همه مجمعی کنند پدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلعتی دادیم که چون عورت</p></div>
<div class="m2"><p>از همه کس ببایدم پوشید</p></div></div>