---
title: >-
    شمارهٔ ۱۴۶ - و له ایضاً
---
# شمارهٔ ۱۴۶ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>زهی سپهر محلّی که گرچه تیزروند</p></div>
<div class="m2"><p>به سایۀ تک عزم تو ماه و خور نرسند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقول اگرچه زافلاک نردبان سازند</p></div>
<div class="m2"><p>زبام خانۀ قدر تو بر زبر نرسند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنکته های تو افهام بابسی کوشش</p></div>
<div class="m2"><p>بکنه معنی یک لفظ مختصر نرسند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو تیر چیره زبانان اگرچه برتازند</p></div>
<div class="m2"><p>بآستان ثنایت هنوز بر نرسند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستاده هفت قلک بر فراز یکدیگر</p></div>
<div class="m2"><p>زشخص معنی تو بیش تا کمر نرسند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زتابش و نم خورشید و ابر عالم را</p></div>
<div class="m2"><p>چه سودگر کف و کلک تو بر اثر نرسند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کدام منزل اومید کاندرو هر دم</p></div>
<div class="m2"><p>زکاروان سخایت نفر نفر نرسند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین زمانه کز انبوهی سپاه بلا</p></div>
<div class="m2"><p>بهیچ خانه نباشد که صد حشر نرسند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهیچ گوشه برون شو نسازد اندیشه</p></div>
<div class="m2"><p>که رهزنان بلاهاش برگذر نرسند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهیچ کنج درون عافیت وطن نکند</p></div>
<div class="m2"><p>که جوق جوقش فتنه همی بسر نرسند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گمان مبر که ز غوغاییان حادثه ها</p></div>
<div class="m2"><p>دمی بود که مرا صد ببام و در نرسند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زر مصادره اصحابنا چگونه دهند</p></div>
<div class="m2"><p>تا بشامگه ایشان بچاشت درنرسند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طمع چه کیسه برآن مفلسان تو اندوخت</p></div>
<div class="m2"><p>که از هزار تکلّف به ما حضر نرسند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خزینه هاشان پر گوهر سخن باشد</p></div>
<div class="m2"><p>ولیک جز بتمنّی بسیم و زر نرسند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بلا و محنت و غم را سبب درین ایّام</p></div>
<div class="m2"><p>اگر هزار بود هیچ در هنر نرسند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فتاده گیر نگون رایت سلامت من</p></div>
<div class="m2"><p>مدد زلطف تو گر هیچ زودتر نرسند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زبنده خانه همه رخت عافیت ببرند</p></div>
<div class="m2"><p>زاهتمام توام حامیان اگر نرسند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کراشناسی فریادرس در این ایّام ؟</p></div>
<div class="m2"><p>گر اهل معنی فریاد یکدگر نرسند</p></div></div>