---
title: >-
    شمارهٔ ۳۶ - وله ایضا
---
# شمارهٔ ۳۶ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>چون چناری میان تهیست فلان</p></div>
<div class="m2"><p>که همه آبها زین خوردست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از درون خالی از برون بی بر</p></div>
<div class="m2"><p>وانگه از حرص پای تا سردست</p></div></div>