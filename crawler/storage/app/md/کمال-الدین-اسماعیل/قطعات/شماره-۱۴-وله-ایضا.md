---
title: >-
    شمارهٔ ۱۴ - وله ایضا
---
# شمارهٔ ۱۴ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>نور دین ای ذات تو کان هنر</p></div>
<div class="m2"><p>کان چه باشد؟ خود سراسر گوهرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنده همچون شمع از نور دلست</p></div>
<div class="m2"><p>هر کرا تا بی ز مهرت در سرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از برای نوعروس خاطرت</p></div>
<div class="m2"><p>حقّه های آسمان پر زیورست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عنبر اندر بحر باشد، پس چرا؟</p></div>
<div class="m2"><p>بحر شعرت در میان عنبرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بدید آن طبع گوهر زای تو</p></div>
<div class="m2"><p>از خجالت دامن دریا ترست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعر می خواهی و خادم مدّتیست</p></div>
<div class="m2"><p>تا ز شعر و شاعری فارغ ترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعر را گر بود وقتی رونقی</p></div>
<div class="m2"><p>این زمان باری عجب مستنکرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کجا از فضل و دانش حلقه ییست</p></div>
<div class="m2"><p>گوشها زان حلقه یکسر بر درست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبل طبعم نوا کم میزند</p></div>
<div class="m2"><p>زانکه شاخ جود بی برگ و برست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشتیاهل هنر بر خشک ماند</p></div>
<div class="m2"><p>کابها را ره به جویی دیگرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان چو سوسن خامشم کاین قوم را</p></div>
<div class="m2"><p>همچو نرگس چشم یکسر برزرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در هران خانه که زاید دختری</p></div>
<div class="m2"><p>خامشی آنجا بمرد درخورست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من چرا خامش نباشم کز سخن</p></div>
<div class="m2"><p>در کنارم زاده چندین دخترست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا برین صورت بود کار هنر</p></div>
<div class="m2"><p>وای آن مسکین که معنی پرورست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم فرستادم بخدمت چند بیت</p></div>
<div class="m2"><p>تا بدانی کین رهی فرمان برست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیستم در خدمتت محتاج عذر</p></div>
<div class="m2"><p>لطف تو خود عذر خواه چاکرست</p></div></div>