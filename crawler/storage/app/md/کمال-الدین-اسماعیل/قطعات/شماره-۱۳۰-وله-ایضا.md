---
title: >-
    شمارهٔ ۱۳۰ - وله ایضا
---
# شمارهٔ ۱۳۰ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>توقّف چون بود در آنچه صاحب</p></div>
<div class="m2"><p>بنوک کلک خود فرموده باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همانا در نیارد بست گردون</p></div>
<div class="m2"><p>دری کاقبال تو بگشوده باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولیک از روی طالع کارمن خود</p></div>
<div class="m2"><p>میسّر بی جگر کم بوده باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رهی کو خرمن انفاس خود را</p></div>
<div class="m2"><p>به کیل مدحتت پیموده باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس از توقیع عالی کاندرین شهر</p></div>
<div class="m2"><p>نباشد کس که آن نشنوده باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سزد کز نعمتت چون گرگ یوسف</p></div>
<div class="m2"><p>شکم خالی ،خالی آلوده باشد</p></div></div>