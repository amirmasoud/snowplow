---
title: >-
    شمارهٔ ۱۷۷ - ایضاً له
---
# شمارهٔ ۱۷۷ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>ای بزرگی که خدمت تو کند</p></div>
<div class="m2"><p>هرکه پیوند جان و تن خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جلال تو کسوتی پوشد</p></div>
<div class="m2"><p>مهر را گوی پیرهن خواهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور ضمیر تو شمعی افزود</p></div>
<div class="m2"><p>ماه رخشنده را لگن خواهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاخ خلق ترا بجنباند</p></div>
<div class="m2"><p>باد چون طیرۀ چمن خواهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیور از لطف تو اوام کند</p></div>
<div class="m2"><p>غنچه چون زیب انجمن خواهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عذر انعامهات را اومید</p></div>
<div class="m2"><p>بکدامین لب و دهن خواهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچنان راستی که عدل تراست</p></div>
<div class="m2"><p>بدعا شاخ نارون خواهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاریت از قد بد اندیشت</p></div>
<div class="m2"><p>زلف سنبل همی شکن خواهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یزک خشمت اوفتد در پیش</p></div>
<div class="m2"><p>هرکجا مرگ تاختن خواهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رقم خصمیت کشد بر وی</p></div>
<div class="m2"><p>هرکرا چرخ ممتحن خواهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از لقایت چمن بدریوزه</p></div>
<div class="m2"><p>آب روی گل و سمن خواهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بوی خلقت شنبد با صبا</p></div>
<div class="m2"><p>از خدا مرگ نسترن خواهد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر دمی خلق تو بطیرۀ مشک</p></div>
<div class="m2"><p>خون نافه بریختن خواهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قلمت روی سیاهی عالم</p></div>
<div class="m2"><p>از پی لؤلؤ عدن خواهد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر کند رأی نظم خاطر تو</p></div>
<div class="m2"><p>از فلک خوشۀ پرن خواهد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیک شرمنده ام که لطف تو چون</p></div>
<div class="m2"><p>از من بی زبان سخن خواهد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه طریقست تا بدست آرم</p></div>
<div class="m2"><p>پای مردی که عذر من خواهد؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عذر این سردی و گران جانی</p></div>
<div class="m2"><p>مگر اروند خویشتن خواهد</p></div></div>