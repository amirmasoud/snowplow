---
title: >-
    شمارهٔ ۱۷۳ - وله ایضا
---
# شمارهٔ ۱۷۳ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>خورشید ذرّه پرور، شاه هنر نواز</p></div>
<div class="m2"><p>ای آن که در جهان چون تو صاحبقرآن نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید کو بتیغ زدن بر سر آمدست</p></div>
<div class="m2"><p>در جنب دست و بازوی تو همچنان نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون آفتاب از تو توانگر شد، آنکه را</p></div>
<div class="m2"><p>جز قرص ماه یکشبه در خانه نان نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و آنگاه که آفتاب و شاق در تو بود</p></div>
<div class="m2"><p>اندر جهان هنوز اساس جهان نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و آنجا که تیر خطبۀ مدح تو می نوشت</p></div>
<div class="m2"><p>بر لوح کاینات سخن را نشان نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی آب خنجر تو ظفر رونقی نیافت</p></div>
<div class="m2"><p>بی نوک خامۀ تو هنر را زبان نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه به صفدری مثل از نیزه می زنند</p></div>
<div class="m2"><p>حقّا که مرد آن قلم ناتوان نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوسن صفت به مدح تو نگشاد کس زبان</p></div>
<div class="m2"><p>کوزا چو گل ز جود تو پر زر دهان نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنها که آزرا زبنانت میسّرست</p></div>
<div class="m2"><p>از بحر و کانش صد یک آن در گمان نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای درگه تو قبلۀ احرار روزگار</p></div>
<div class="m2"><p>آن کیست خود که بندۀ این خاندان نبود؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زانگه که از جمال تو برگشت چشم من</p></div>
<div class="m2"><p>از مدح تو تهی دهنم یک زمان نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از بهر حضرت تو دل از من نثار خواست</p></div>
<div class="m2"><p>درماند چون مرا مدد از سوزیان نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسیار سعی کرد که چیزی بکف کند</p></div>
<div class="m2"><p>پس عاقبت برون ز سخن در میان نبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گستاخ کرد لطف تو با خویشتن مرا</p></div>
<div class="m2"><p>ورنه بجان تو که مرا روی آن نبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جان منست اگر چه گرانست شعر من</p></div>
<div class="m2"><p>با آنکه جان به هیچ بهایی گران نبود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آورده ام بخدمت تو جان نازنین</p></div>
<div class="m2"><p>بپذیر از آنکه دست رسم جز به جان نبود</p></div></div>