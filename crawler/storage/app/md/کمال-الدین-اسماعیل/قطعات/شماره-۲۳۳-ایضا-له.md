---
title: >-
    شمارهٔ ۲۳۳ - ایضا له
---
# شمارهٔ ۲۳۳ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>بنا میزد دلی چون شیر دارم</p></div>
<div class="m2"><p>زبانی بر سخن‌ها چیر دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو پنداری به دقت جنگ و کوشش</p></div>
<div class="m2"><p>دلی از زندگانی سیر دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زخم خنجر و زوبین و ناوک</p></div>
<div class="m2"><p>تنی بسته به صد کفشیر دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نامردی ندارم هر چه دارم</p></div>
<div class="m2"><p>به زخم بازو و شمشیر دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بپرهیزد ز زخم او اگر من</p></div>
<div class="m2"><p>زبان خویش پیش شیر دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سری کز آسمان بر می فرازم</p></div>
<div class="m2"><p>چرا از بهر دو نان زیر دارم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندارم مردمی زین مردمان چشم</p></div>
<div class="m2"><p>که گر این چشم دارم دیر دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دانش کیسه بر اقبال دوزند</p></div>
<div class="m2"><p>من از وی مایة ادبیر دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز چندین گفته‌های نغز حاصل</p></div>
<div class="m2"><p>مرا گویی چه داری؟.... دارم</p></div></div>