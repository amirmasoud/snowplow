---
title: >-
    شمارهٔ ۱۱۸ - وله ایضا
---
# شمارهٔ ۱۱۸ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>مژده ای دل که کار دیگر شد</p></div>
<div class="m2"><p>و انچه می خواستی میسّر شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار از راه جور برگردید</p></div>
<div class="m2"><p>مشفق و مهربان و چاکر شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار اگر بسته بد گشایش یافت</p></div>
<div class="m2"><p>عیش اگر زهر بود شکّر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل که چون لفظ او مقیّد بود</p></div>
<div class="m2"><p>هم بسعی خطش محرّر شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نامه فرمود و دل خوشیها داد</p></div>
<div class="m2"><p>چون که حال منش مقرّر شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلک بیمارش احتما بشکست</p></div>
<div class="m2"><p>با من از آنچه بود بهتر شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشتهیّ دروغ کرد آغاز</p></div>
<div class="m2"><p>با سر پرسش مزوّز شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر گرفتم ز درج درّش مهر</p></div>
<div class="m2"><p>دامنم پر ز درّ و گوهر شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردم چه مشم ابن مقلمة وقت</p></div>
<div class="m2"><p>بندۀ آن خط چو عبهر شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر بیاض خودش سوادی کرد</p></div>
<div class="m2"><p>که از او چشم جان منوّر شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیده بر حرفهاش مالیدم</p></div>
<div class="m2"><p>حالی از اب لطف او تر شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خط مشکین او چو بر خواندم</p></div>
<div class="m2"><p>مغز جانم ازو معطّر شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاخ طبعم گهر ببار آورد</p></div>
<div class="m2"><p>چون کش الفاظ او مصوّر شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر چه دشنام و خشم بود از من</p></div>
<div class="m2"><p>به دعا و ثنا برابر شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کلک او کرده بود عربده زانک</p></div>
<div class="m2"><p>زان معانیش باده در سر شد</p></div></div>