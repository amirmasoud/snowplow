---
title: >-
    شمارهٔ ۷۱ - وله ایضا
---
# شمارهٔ ۷۱ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>به خدمت آمدم دی بامدادان</p></div>
<div class="m2"><p>نبودی در وثاق مرده ریگت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذارم بر طریق مطبخ افتاد</p></div>
<div class="m2"><p>بدیدم لوت و پوت همچو ریگت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخار جوع کلبی از چهل گام</p></div>
<div class="m2"><p>به مغز من همی آمد ز دیگت</p></div></div>