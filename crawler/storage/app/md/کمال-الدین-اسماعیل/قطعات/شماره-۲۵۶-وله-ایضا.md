---
title: >-
    شمارهٔ ۲۵۶ - وله ایضا
---
# شمارهٔ ۲۵۶ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>من که در خانه منزوی شده ام</p></div>
<div class="m2"><p>عذر خویش از گناه می خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست از خواستن بداشته ام</p></div>
<div class="m2"><p>که نه مال و نه جاه می خواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه اگر زین شکسته تر باشم</p></div>
<div class="m2"><p>مددی از سپاه می خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه گر از تشنگی بخواهم مرد</p></div>
<div class="m2"><p>شربتی آب چاه می خواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه اگر صد هزار ظلم کشم</p></div>
<div class="m2"><p>دادی از پادشاه می خواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه اگر می کنند صد دعوی</p></div>
<div class="m2"><p>بیّنت یا گواه می خواهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه همی داو خواهم اندر نرد</p></div>
<div class="m2"><p>نه بشطرنج شاه می خواهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه گر اینجا مقام خواهم ساخت</p></div>
<div class="m2"><p>به بزرگان پناه می خواهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه اگر عزم رفتنم باشد</p></div>
<div class="m2"><p>از کسی برگ راه می خواهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لیک یک حاجتم بنزد تو هست</p></div>
<div class="m2"><p>گر تو گویی بخواه می خواهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اسب بیچاره سخت درمانده ست</p></div>
<div class="m2"><p>بهر او از تو کاه می خواهم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امر معروف شرط اسلامست</p></div>
<div class="m2"><p>عذر این هم بگاه می خواهم</p></div></div>