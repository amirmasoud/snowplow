---
title: >-
    شمارهٔ ۸۲ - ایضا له
---
# شمارهٔ ۸۲ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>هر که این هر دو قطعه بر خواند</p></div>
<div class="m2"><p>که ازین پیش کردم آنرا یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرسد از من که خواجه نیز ترا</p></div>
<div class="m2"><p>چند خروار غلّه بفرستاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیک باشد که من جوابش را</p></div>
<div class="m2"><p>بنویسم که نیم جو بنداد؟</p></div></div>