---
title: >-
    شمارهٔ ۹۵ - ایضا له
---
# شمارهٔ ۹۵ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>خواجۀ خواجگان خطیر الرّین</p></div>
<div class="m2"><p>کز کفت بحر بی خطر گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام پاک ترا چو بنگارند</p></div>
<div class="m2"><p>دهن کلک پر شکر گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمانی ز رشک بحر کفت</p></div>
<div class="m2"><p>دامن آفتاب تر گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دم نیارد زدن نسیم صبا</p></div>
<div class="m2"><p>گر ز لطف تو با خبر گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر زمان روی دشمن از بیمت</p></div>
<div class="m2"><p>زرد و پرچین چو روی زر گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخ را آرزو بود ،کورا</p></div>
<div class="m2"><p>خاک پای تو تاج سر گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که هر دم ز بار همّت تو</p></div>
<div class="m2"><p>تارک چرخ پی سپر گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار خادم بدست و می ترسد</p></div>
<div class="m2"><p>گه از ین نیز هم بتر گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دم از آه سرد و آتش دل</p></div>
<div class="m2"><p>جگرش خون و خون جگر گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر زمان بهر محنتش گردون</p></div>
<div class="m2"><p>گرد دیگر بهانه بر گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>انبساطی نمود با کرمت</p></div>
<div class="m2"><p>مگرش کارها دگر گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر تو در کار او نظر فکنی</p></div>
<div class="m2"><p>همه غمهاش مختصر گردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دولتت در زمانه باقی باد</p></div>
<div class="m2"><p>تا زمین چون سپهر در گردد</p></div></div>