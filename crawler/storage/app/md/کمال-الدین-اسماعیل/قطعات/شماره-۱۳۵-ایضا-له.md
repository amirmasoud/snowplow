---
title: >-
    شمارهٔ ۱۳۵ - ایضا له
---
# شمارهٔ ۱۳۵ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>گفتم اکنون میوه های خوش خوریم</p></div>
<div class="m2"><p>کین دو شاخ نو بهم پیوسته شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود ندانستم که ققل و پرّه اند</p></div>
<div class="m2"><p>کین بدان پیوسته شد در بسته شد</p></div></div>