---
title: >-
    شمارهٔ ۳۴۵ - فی مدح ابن بدرالدّین باسمعیل
---
# شمارهٔ ۳۴۵ - فی مدح ابن بدرالدّین باسمعیل

<div class="b" id="bn1"><div class="m1"><p>بنامیزد چنان فرّخ لقایی</p></div>
<div class="m2"><p>که گویی سایۀ پرّ همایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بگشایی زبان لبها ببندی</p></div>
<div class="m2"><p>چو در بندگی قبا گیتی گشایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشمشیر تو نصرت را تفاخر</p></div>
<div class="m2"><p>ببازار تو معنی را روایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمربند دو پیگر بگسلانی</p></div>
<div class="m2"><p>اگر با آسمان زور آزمایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکایک برکنی دندان پروین</p></div>
<div class="m2"><p>اگر دندان کین بر چرخ سایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهانگیری اگر دعوی کنی تو</p></div>
<div class="m2"><p>زبان خنجرت بدهد گوایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان بر زرفشانی چیره دستی</p></div>
<div class="m2"><p>که گر با دشمنان کوشش نمایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسودت در پناه روی چون زر</p></div>
<div class="m2"><p>تواند یافت از دستت رهایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبود از روزگارم این توقّع</p></div>
<div class="m2"><p>که تو با ما چنین فارغ درآیی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من از خدمت بکاهانیده و آنگاه</p></div>
<div class="m2"><p>تو در لطف و تواضع می فزایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بغیبت داده بی تشریف خادم</p></div>
<div class="m2"><p>ز روی مردمیّ و خوب رایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا در خانه از تشریف تو عید</p></div>
<div class="m2"><p>من اندر روستا از بی نوایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم از بخت منست این ارنه هرگز</p></div>
<div class="m2"><p>نباشد عید خود بی روستایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نخواهم عذر تشریفت ، چرا؟ زانک</p></div>
<div class="m2"><p>بحمدالله تو بیش از عذرمایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که خورشید ارچه بر چرخ بلندست</p></div>
<div class="m2"><p>بهر جایی رساند روشنایی</p></div></div>