---
title: >-
    شمارهٔ ۱۹۳ - وله ایضا
---
# شمارهٔ ۱۹۳ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>زهی زرفعت تو خورده آسمان تشویر</p></div>
<div class="m2"><p>زهی ندیده ترا چشم روزگار نظیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پناه اهل هنر ، زین دین ، یگانه عصر</p></div>
<div class="m2"><p>که افتخار کنند مملکت به چون تو وزیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمینه پایۀ قدر تو آسمان بلند</p></div>
<div class="m2"><p>کمینه شعلۀ رای تو افتاب منیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فسادر را نبود دست بر قواعد کون</p></div>
<div class="m2"><p>اگر به رأی تو باشد زمانه را تدبیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد از نیابت تحریر تو عطارد شاد</p></div>
<div class="m2"><p>به بندگان نرسد شادیی به از تحریر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گران رکایی حزم تو بازگرداند</p></div>
<div class="m2"><p>عنان جنبش خاصیّت از ره تاثیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همیشه کلک تو از بهر آن کمر بستست</p></div>
<div class="m2"><p>که تا معایش اهل هنر کند تقریر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کفایت تو چنان با کرم زیک خانه ست</p></div>
<div class="m2"><p>که زر ببخشد و نام نیکو کند توفیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز هیبت تو برفتی به باد استخفاف</p></div>
<div class="m2"><p>اگر نکردی حلم تو کوه را توقیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تویی که وقت هنر در مقام تیغ و قلم</p></div>
<div class="m2"><p>چو آفتاب و عطارد مبارزی و دبیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مخالفان ترا تیغهای همچون آب</p></div>
<div class="m2"><p>بدست بر شود از باد هیبتت ز نجیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز خاک بوسی گویی که تیر آما جست</p></div>
<div class="m2"><p>ز بس که بوسه دهد خاک درگهت را تیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از آنکه کاغذ در عهد تودورویی کرد</p></div>
<div class="m2"><p>همیشه باشد چون دشمنت نشانۀ تیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اسیر دام خطت زان شدست دانۀ دل</p></div>
<div class="m2"><p>که هست خطّ تو چون زلف نیکوان دلگیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آن نگین که برو نام دشمنت نقش است</p></div>
<div class="m2"><p>گمان مبر که بود طمع موم نقش پذیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو صبح صادقم اندر هوایت و هر دم</p></div>
<div class="m2"><p>فروغ مهر تو بدرخشم ز طی ضمیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زبان عذر ندارم از آن که بس خجلم</p></div>
<div class="m2"><p>ز نوع نوع صداع و ز گونه گون تقصیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عروس شعر مرا لطف تو چوو خطبت کرد</p></div>
<div class="m2"><p>بگویمت که چه بودست موجب تأخیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سبک برفتم و با عقل مشورت کردم</p></div>
<div class="m2"><p>که اوست عاقلة خلق و مستشار و مشیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو دید بررخ ناشسته زلف شوریده</p></div>
<div class="m2"><p>مپرس خود که چه فریاد کرد و بانگ و نفیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که این چه لایق آن حضرتست؟شرمت نیست</p></div>
<div class="m2"><p>که دیو را پر طاووس بر نهی به سریر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر چه بوددر این باب حق بدست خرد</p></div>
<div class="m2"><p>ز امتثال اشارت همم نبود گزیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>میان ببستم چون زلف و نفس لوّامه</p></div>
<div class="m2"><p>چو چشم خوبان می کرد هر دم تعییر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به خدمت تو فرستادمش کنون ترسان</p></div>
<div class="m2"><p>چنانکه نقد دغل پیش ناقدان بصیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به نام و ننگش ترتیبکی بدادم هم</p></div>
<div class="m2"><p>چنان که لایق من باشد از قلیل و کثیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>محفّه ش از قصب درّی قلم کردم</p></div>
<div class="m2"><p>تتق ز کلّة اکون و بسترش ز حریر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز اشک و چهرة من غرقه در زرو گوهر</p></div>
<div class="m2"><p>ز خلق و خامة من در میان مشک و عبیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>میان ببسته به لالائیش دو صد لولو</p></div>
<div class="m2"><p>دهان گشاده بچاووشیش زبان صربر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز خانه ها دوسه معروف همرهش کردم</p></div>
<div class="m2"><p>همه جوان به حقیقت ولی به صورت پیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بکردم این همه و عاقبت همی دانم</p></div>
<div class="m2"><p>که از ثنای تو هم خورد بایدم تشویر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>توقّعست ز مشّاطۀ کرم که کنون</p></div>
<div class="m2"><p>به جلوه گاه قبولش نکو کند تصویر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر چه زشت و گرانست نازنین منست</p></div>
<div class="m2"><p>به چشم مهر نگر سوی نازنین اسیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بناز دار چگر گوشۀ ضمیر مرا</p></div>
<div class="m2"><p>که من به خون دلش پروریده ام نه به شیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حلال زادگی و اصل پاک و گوهر بین</p></div>
<div class="m2"><p>نگه مکن به سیه چردگیّ و شکل حقیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه چشم کابین دارد ز کس نه گوش نثار</p></div>
<div class="m2"><p>به رایگانش از بهر بندگی بپذیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وگرنباشد بر ذوق خاطر اشرف</p></div>
<div class="m2"><p>تو از بزرگی خود در گذار و خرده مگیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بساط جاه عریض تو باد چرخ بسیط</p></div>
<div class="m2"><p>ز ذیل عمر طویل تو دست دهر قصیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>
<div class="b" id="bn39"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>
<div class="b" id="bn40"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>
<div class="b" id="bn41"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>
<div class="b" id="bn42"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>