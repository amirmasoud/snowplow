---
title: >-
    شمارهٔ ۱۲۳ - ایضاً له
---
# شمارهٔ ۱۲۳ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>دست آن به که خود قلم باشد</p></div>
<div class="m2"><p>کش سر و کار باقلم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی ز نی کن ، قلم زنی بگذار</p></div>
<div class="m2"><p>کانک این کرد محترم باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهره را کار از آن بساز و نو است</p></div>
<div class="m2"><p>که همه جفت زیر و بم باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان عطارد بجرم آن سوزد</p></div>
<div class="m2"><p>که چو من با قلم بهم باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الف راست قامت انگشت</p></div>
<div class="m2"><p>با قلم همچو نون بخم باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مبدأ عطلت نکورویان</p></div>
<div class="m2"><p>از خط تیرۀ دژم باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیر گویند چون زشست برفت</p></div>
<div class="m2"><p>رجعتش در خیال کم باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیر گردون زشست چون بگذشت</p></div>
<div class="m2"><p>زو برجعت یکی قدم باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وین و بال و تراجعش زانست</p></div>
<div class="m2"><p>کزدبیری برو رقم باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو شیر علم زباد زید</p></div>
<div class="m2"><p>هرکه در علمها علم باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکه او کاتبست همچو قلم</p></div>
<div class="m2"><p>تیره روز و تهی شکم باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاصه آن کش یکی ورق کاغذ</p></div>
<div class="m2"><p>نه زدینار و نز درم باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نی که کتب خلاصۀ هنرست</p></div>
<div class="m2"><p>مرد باید که محتشم باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اندرین دور همچو مخدومم</p></div>
<div class="m2"><p>کز کفش بخل در عدم باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن ولّی النّعم که از انعام</p></div>
<div class="m2"><p>همه الفاظ او نعم باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نرود بر زبان او هرگز</p></div>
<div class="m2"><p>هرچه از جنس الاولم باشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هست از آینۀ ئلش روشن</p></div>
<div class="m2"><p>هرچه در عالم قدم باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عقل در پیش لطف و هیبت او</p></div>
<div class="m2"><p>راست چون صیددر حرم باشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بخشش اوست زرّ در کاغذ</p></div>
<div class="m2"><p>مهر چون در سپیده دم باشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سرفرازا اگر چه در خدمت</p></div>
<div class="m2"><p>زحمت بنده دم بدم باشد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مدّتی شد که نیک بیکارم</p></div>
<div class="m2"><p>مرد بی کار متّهم باشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پارۀ کاغذ ار بفرمایی</p></div>
<div class="m2"><p>بعد منّت ثواب هم باشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور بود اندکی و پیچیده</p></div>
<div class="m2"><p>آن خود از غایت کرم باشد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا زبان قلم سیاه بود</p></div>
<div class="m2"><p>در دهان دویت نم باشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کاغذین باد جامۀ خصمت</p></div>
<div class="m2"><p>بس که از غم برو ستم باشد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خود ز کاغذ سزد لباس کسی</p></div>
<div class="m2"><p>کو سیه روی چون خطم باشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رسته بادی زهر غمی ترا</p></div>
<div class="m2"><p>با چنان طبع خود چه غم باشد</p></div></div>