---
title: >-
    شمارهٔ ۳۴۳ - وله ایضا
---
# شمارهٔ ۳۴۳ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>صاحبا عدّت قیامت را</p></div>
<div class="m2"><p>از من ممتحن چه می خواهی ؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس از گونه یی همی گوید</p></div>
<div class="m2"><p>تو بگو خویشتن چه می خواهی ؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمانی عتابی آغازی</p></div>
<div class="m2"><p>معنی این سخن چه می خواهی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردی از پای من برون شلوار</p></div>
<div class="m2"><p>بدرم پیرهن ، چه می خواهی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کنی تو ز پیش دفع سؤال</p></div>
<div class="m2"><p>ورنه زین مکروفن چه می خواهی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من نمی خواهم از تو هیچ، برو</p></div>
<div class="m2"><p>ای مسلمان ز من چه می خواهی؟</p></div></div>