---
title: >-
    شمارهٔ ۲۲۴ - وله ایضا
---
# شمارهٔ ۲۲۴ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>کمال الدّین که چرخ پیر نارد</p></div>
<div class="m2"><p>جوانی چون تو در انواع کامل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امل از کیسة جود تو فربه</p></div>
<div class="m2"><p>فلک با رفعست قدر تو نازل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زده از یکدلی اندر هوایت</p></div>
<div class="m2"><p>که مقصودی از آن آرد به حاصل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غریبی از تو می خواهد دومن می</p></div>
<div class="m2"><p>مکن بر خویش و بروی کار مشکل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگو چون و کجا؟ امروز و فردا</p></div>
<div class="m2"><p>به همّت گوی کالهّم سهّل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بده گر دست دار و وگرنه</p></div>
<div class="m2"><p></p></div></div>