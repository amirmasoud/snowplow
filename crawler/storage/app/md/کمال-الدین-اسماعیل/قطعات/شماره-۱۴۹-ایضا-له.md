---
title: >-
    شمارهٔ ۱۴۹ - ایضا له
---
# شمارهٔ ۱۴۹ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>عصای کلیمست این شعر من</p></div>
<div class="m2"><p>که دریا بخشکی چو ساحل کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یقین بد مرا کآن هنر نیستش</p></div>
<div class="m2"><p>که چیزی ز ممدوح حاصل کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانستم این خاصیت باشدش</p></div>
<div class="m2"><p>که انعام پذرفته باطل کند</p></div></div>