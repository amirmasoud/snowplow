---
title: >-
    شمارهٔ ۹۲ - وله ایضا
---
# شمارهٔ ۹۲ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>همچو ابرست دست خواجه فلان</p></div>
<div class="m2"><p>خود کرا دستی آنچنان افتد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه چنان ابر کز ترشّح آن</p></div>
<div class="m2"><p>تشنه را قطره در دهان افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیک ابری گران سایه فکن</p></div>
<div class="m2"><p>که ازو خلق را زیان افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نور کز آفتاب می تابد</p></div>
<div class="m2"><p>نگذارد که بر جهان افتد</p></div></div>