---
title: >-
    شمارهٔ ۱۲۶ - و له ایضاً
---
# شمارهٔ ۱۲۶ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>ای آنکه فلک سغبۀ ایّام تو باشد</p></div>
<div class="m2"><p>دوران فلک بر حسب کام تو باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این آز شکم خوار که سیری نشناسد</p></div>
<div class="m2"><p>ضامن بکفافش کف مطعام تو باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاشا که کف راد نرا بحر کنم نام</p></div>
<div class="m2"><p>خودکی چو منی را دل دشنام تو باشد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن چشمه که یک رشحه از آن آب حیاتست</p></div>
<div class="m2"><p>نزدیک خرد جرعه یی از جام تو باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر صید تواند اهل هنر هیچ عجب نیست</p></div>
<div class="m2"><p>چون دانۀ دلها همه در دام تو باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کوی تو خورشید کند مشعله داری</p></div>
<div class="m2"><p>خاصه که زحل هندوک بام تو باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقلی که باندام تراز وی نبود خلق</p></div>
<div class="m2"><p>خواهد که یکی موی بر اندام تو باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از عهد تو تا منقرض عالم ازین پس</p></div>
<div class="m2"><p>تاریخ هنر پروری ایّام تو باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آورد دگر باره بنزد تو صداعی</p></div>
<div class="m2"><p>خادم که همه ساله درابرام تو باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معماریی آغاز نهادست که او را</p></div>
<div class="m2"><p>الّا که معاون کرم عام تو باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی برگیش از کاه همی باشد و او را</p></div>
<div class="m2"><p>بیرون شو از این کار باعلام تو باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مقصود نه کاهست ولی تا همه چیزش</p></div>
<div class="m2"><p>تا کاه بدیوار ز انعام تو باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کاه از پی تخفیف همی خواهد لیکن</p></div>
<div class="m2"><p>گر تو بکرم جو بدهی نام تو باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این کار علی الجملع که درپیش دعاگوست</p></div>
<div class="m2"><p>کاریست که موقوف بر اتمام تو باشد</p></div></div>