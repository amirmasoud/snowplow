---
title: >-
    شمارهٔ ۷۷ - ایضا له
---
# شمارهٔ ۷۷ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>ای ز ظلم تو همچو لاله ستان</p></div>
<div class="m2"><p>گشته از خون تو جهانی سرخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکل تو در قبای سرخ چنانک</p></div>
<div class="m2"><p>بر در اگنده جامه دانی سرخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا چو در جامه کشته و مرده</p></div>
<div class="m2"><p>کرده آماس ترکمانی سرخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در لحاف تو هر شبی خسبند</p></div>
<div class="m2"><p>قحبه یی زرد و قلتبانی سرخ</p></div></div>