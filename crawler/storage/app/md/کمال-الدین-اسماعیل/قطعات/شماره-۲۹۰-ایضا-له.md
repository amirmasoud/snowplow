---
title: >-
    شمارهٔ ۲۹۰ - ایضا له
---
# شمارهٔ ۲۹۰ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>خواجگانی که پیش ازین بودند</p></div>
<div class="m2"><p>عرض خود داشتند نیک نگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر و سیم جهان همی دادند</p></div>
<div class="m2"><p>تا نگویندشان حدیث تباه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواجگانی که اندرین عهدند</p></div>
<div class="m2"><p>با هجی گوی خویش بیگه و گاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزبانی فصیح می گویند</p></div>
<div class="m2"><p>هر چه خواهی بگوی و سیم مخواه</p></div></div>