---
title: >-
    شمارهٔ ۱۷۶ - ایضا له
---
# شمارهٔ ۱۷۶ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>سرورا عرضها نمی باید</p></div>
<div class="m2"><p>که به دست سخن بسوده شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعر آیینه ییست کاندر وی</p></div>
<div class="m2"><p>صورت حالها نموده شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا تخم مردمی کارند</p></div>
<div class="m2"><p>خوشۀ شکر از آن دروده شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنگ این ننگ از صحیفه نام</p></div>
<div class="m2"><p>نه همانا که خود ز دوده شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که از شاعران طمع دارد</p></div>
<div class="m2"><p>به کدامین زبان ستوده شود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که نا گفتنی شود گفته</p></div>
<div class="m2"><p>هر کجا این سخن شنوده شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ عاقل به خویش نپسندد</p></div>
<div class="m2"><p>هر چه از مال ما ربوده شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست نقصان عرض و وصمت جاه</p></div>
<div class="m2"><p>مال کز سیم ما فزوده شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زشت نبود که آن که کان دارد</p></div>
<div class="m2"><p>به گدایی به خاک توده شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه گشاید ترا از آن صندوق</p></div>
<div class="m2"><p>که به حرف هجا گشوده شود</p></div></div>