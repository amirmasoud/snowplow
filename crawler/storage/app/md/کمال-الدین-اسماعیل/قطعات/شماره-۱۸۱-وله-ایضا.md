---
title: >-
    شمارهٔ ۱۸۱ - وله ایضا
---
# شمارهٔ ۱۸۱ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>امام ملّت و مفتیّ مشرق و مغرب</p></div>
<div class="m2"><p>بیان کند که شریعت چه حکم فرماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آنکه شخصی از بهر دعوی شرعی</p></div>
<div class="m2"><p>خود و غریمی در مجلس قضا آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدست ظلم و تطاول یکی زنا اهلان</p></div>
<div class="m2"><p>غریم او را از وی به قهر بر باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو این تظلّم بر شاه شرع عرض کند</p></div>
<div class="m2"><p>ز روی ضبط شریعت برو نبخشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخواند او را واو با کمال ناجنسی</p></div>
<div class="m2"><p>عدول از نهج اعتراف ننماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو معترف شود و ملتزم که با آن شخص</p></div>
<div class="m2"><p>بر آنچه حکم شریعت بود نیفزاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو ماه بگذرد، این مدعّی بیچاره</p></div>
<div class="m2"><p>ز گفتگوی و تقاضا زبان بفرساید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فزون ازین نبود حاصل تقاضاهاش</p></div>
<div class="m2"><p>که او بچرب زبانی سرش بینداید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه راز سینۀ او کس بگوش راه دهد</p></div>
<div class="m2"><p>نه کار بستۀ او هیچ خلق بگشاید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه اضطراب کند، گه به عجز تن بنهد</p></div>
<div class="m2"><p>گهی خموش بود، گاه ژاژ میخاید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدایگان شریعت چو حال می داند</p></div>
<div class="m2"><p>اگر ز نصرت مظلوم تن زند شاید؟</p></div></div>