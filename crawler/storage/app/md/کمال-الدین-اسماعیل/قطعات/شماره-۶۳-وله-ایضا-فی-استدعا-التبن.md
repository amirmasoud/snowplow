---
title: >-
    شمارهٔ ۶۳ - وله ایضا فی استدعا التّبن
---
# شمارهٔ ۶۳ - وله ایضا فی استدعا التّبن

<div class="b" id="bn1"><div class="m1"><p>اسبم دی گفت می روم من</p></div>
<div class="m2"><p>کاریت بجانب عدم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که دمی بپای و گفتا</p></div>
<div class="m2"><p>درآخور تو برون زدم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میمیرم از آرزوی کاهی</p></div>
<div class="m2"><p>و اندر تو به نیم جو کرم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر برگ ستور داریت نیست</p></div>
<div class="m2"><p>بفروش چه داریم ستم نیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جو ز آخر چرب باز کردی</p></div>
<div class="m2"><p>یک توبره کاه خشک هم نیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی ز نشت وزین بر پشت؟</p></div>
<div class="m2"><p>خود زین شکم تهیت غم نیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز راه به پشت من ندانی</p></div>
<div class="m2"><p>می پنداری مرا شکم نیست</p></div></div>