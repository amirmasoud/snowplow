---
title: >-
    شمارهٔ ۳۰۶ - وله فی صفة القحط و التماس الغلّه
---
# شمارهٔ ۳۰۶ - وله فی صفة القحط و التماس الغلّه

<div class="b" id="bn1"><div class="m1"><p>ای خداوندی که اندر خشک سال قحط جود</p></div>
<div class="m2"><p>پخته شد از آب انعام تو نان گرسنه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآنکه تو مشهور آفاقی بنان دادن چو صبح</p></div>
<div class="m2"><p>سر بدر گاهت نهادست آسمان گرسنه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیل انعام تو هردم دروثاق سایلان</p></div>
<div class="m2"><p>آنچنان افتد که آتش در روان گرسنه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکل اخلاق حسودت گر کنم بر روی نان</p></div>
<div class="m2"><p>بوی آن از نان بگرداند عنان گرسنه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو مشرق قرص گرمش میفرستد جود تو</p></div>
<div class="m2"><p>ار دهندش زان سوی مغرب نشان گرسنه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست بی یاد سخایت داستان اهل فضل</p></div>
<div class="m2"><p>آری از نان نیست خالی داستان گرسنه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندین دوران که میگردد سیه چون روی فضل</p></div>
<div class="m2"><p>روی قرص ماه و خورشید از فغان گرسنه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قرص خور بر خود همی لرزد ، چرا؟ از بهر آنک</p></div>
<div class="m2"><p>تیز کرد اختر برو دندان بسان گرسنه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشته بی آبان بخون یکدیگر تشنه چنانک</p></div>
<div class="m2"><p>نان همی آرند بیرون از دهان گرسنه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پردلان را نان سیر از لقمه های بیوه زن</p></div>
<div class="m2"><p>گرد نانرا دیگ چرب از گردران گرسنه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکجا دیدی دو نان پیدا بدست عاجزی</p></div>
<div class="m2"><p>در زمانبینی بدو یازان سنان گرسنه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صبح پنهان میکند در زیر چادر قرص خویش</p></div>
<div class="m2"><p>زین سیه کامان چون شب نان ستان گرسنه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر گذار نان دهنها باز کرده چون تنور</p></div>
<div class="m2"><p>تیغ داران چو آتش خون فشان گرسنه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در فراق قرص تن چون ریسمان بگداخته</p></div>
<div class="m2"><p>همچو شمع از آتش دل ناتوان گرسنه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر نگردد صورت تدبیر نان پیشش سپر</p></div>
<div class="m2"><p>زخم شمشیر فنا ندهد امان گرسنه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ترسم آید از زبان من خطایی در وجود</p></div>
<div class="m2"><p>زانکه دارد رنگ دیوانه جوان گرسنه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خواجگانی را که باشد معدۀ انبار سیر</p></div>
<div class="m2"><p>احترازی شرط باشد از زبان گرسنه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زآنکه از آتش نباشد پنبه را چندان خطر</p></div>
<div class="m2"><p>کاهل نعمت را کنون از شاعران گرسنه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صاحبا ! گر دست مطمعامت ندارد دست پیش</p></div>
<div class="m2"><p>شکند سیلاب تنگی بند جان گرسنه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>میزبان لطف را گو تا که باشد تازه روی</p></div>
<div class="m2"><p>زانکه ناخوانده رسیدش میهمان گرسنه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرکرا بر خوان همّت هست نان مردمی</p></div>
<div class="m2"><p>نگسلد از درگه او کاروان گرسنه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>و آنکه چون یوسف بود ملک خزاین در کفش</p></div>
<div class="m2"><p>چاره نبود زانکه باشد مهربان گرسنه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دفع کن ز انبار خود عین الکمال از بهر آنک</p></div>
<div class="m2"><p>چشم را تاثیر باشد خاصۀ آن گرسنه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کرد مستغنی ز تعریفم ردیف شعر از آنک</p></div>
<div class="m2"><p>بر سر این گفته بنوشم فلان گرسنه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باد در چنگ حوادث خصم پرآهوی تو</p></div>
<div class="m2"><p>همچو آهو در کف شیر ژیان گرسنه</p></div></div>