---
title: >-
    شمارهٔ ۲۳۰ - وله ایضا
---
# شمارهٔ ۲۳۰ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>دی در اسباب سیّدالوزرا</p></div>
<div class="m2"><p>که قیاسش نداند الّا عقل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خود اندر شدم به اندیشه</p></div>
<div class="m2"><p>تا برآورد سر بسودا عقل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون همه یک بیک چنان دیدم</p></div>
<div class="m2"><p>که پسندیده داشت آنرا عقل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواستم تا ستایشی کنمش</p></div>
<div class="m2"><p>در سخن رفت فکرتم با عقل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم این خواجۀ بدین عظمت</p></div>
<div class="m2"><p>که شکوهش ببرد از ما عقل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانچه در خواجگی بکار اید</p></div>
<div class="m2"><p>چه ندارد تمام؟ گفتا عقل</p></div></div>