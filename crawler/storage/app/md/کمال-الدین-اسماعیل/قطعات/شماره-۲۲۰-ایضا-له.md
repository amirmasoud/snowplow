---
title: >-
    شمارهٔ ۲۲۰ - ایضا له
---
# شمارهٔ ۲۲۰ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>ای بزرگی که ریش قهر ترا</p></div>
<div class="m2"><p>نتوان داشت التیام طمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظرش بر عبادت و خط تست</p></div>
<div class="m2"><p>هر که دارد شکر ز شام طمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر گه از دور بینیم گویی</p></div>
<div class="m2"><p>طمع آورده یی، کدام طمع؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخدا کز توام پس از سه سلام</p></div>
<div class="m2"><p>نبود پاسخ سلام طمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راضیم کز تو سر بسر بر هم</p></div>
<div class="m2"><p>گر چه دارم به خاص و عام طمع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش ازین داشتم به دولت تو</p></div>
<div class="m2"><p>نعمت و جاه و احترام طمع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این زمان با وثایق شرعی</p></div>
<div class="m2"><p>می ندارم ادای وام طمع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون عنان سخن دراز کنم</p></div>
<div class="m2"><p>بر سرم می کند لگام طمع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنچنانم مکن ز نومیدی</p></div>
<div class="m2"><p>که ببّرم ز تو تمام طمع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بار ممدوح چون کشد مادح</p></div>
<div class="m2"><p>خواجه چون دارد از غلام طمع؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نگونساری جهان باشد</p></div>
<div class="m2"><p>که صراحی کند به جام طمع</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اندرین عهد کر تسلّط بخل</p></div>
<div class="m2"><p>گشت بر طامعان حرام طمع</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با چنین خواجگان سوخته...</p></div>
<div class="m2"><p>وای بر شاعران خام طمع</p></div></div>