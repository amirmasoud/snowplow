---
title: >-
    شمارهٔ ۱۴۲ - وله ایضا
---
# شمارهٔ ۱۴۲ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>صاحبا عمریست تا از عدل تو</p></div>
<div class="m2"><p>عالمی در انتهاز فرصه اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نظر در کار آنها افگنی</p></div>
<div class="m2"><p>کز تظلّم رافع این قصّه اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لعبها بینند در شطرنج ملک</p></div>
<div class="m2"><p>آنکه ساکن بر کنار عرصه اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کنند از غصّه افغان بر درت</p></div>
<div class="m2"><p>در دل آن قومی که صاحب حصّه اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وین عجب نبود که خود صاحبدلان</p></div>
<div class="m2"><p>هر کجا هستند اسیر غصّه اند</p></div></div>