---
title: >-
    شمارهٔ ۱۹۹ - وله ایضا
---
# شمارهٔ ۱۹۹ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>منم ان چرب دست شیرین کار</p></div>
<div class="m2"><p>کاب طبع مراست آتش بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورتم آشیانۀ معنی</p></div>
<div class="m2"><p>فکرتم کنج خانه اسرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حرکاتم چو گام عمر سبک</p></div>
<div class="m2"><p>سخنانم چو باده نوشگوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو گل عالمی بخنداند</p></div>
<div class="m2"><p>بلبل طبع من گه گفتار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بستانم بهزل مال ملوک</p></div>
<div class="m2"><p>بر ضعیفان کنم به حکم ایثار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان که مقدار خویشتن دانم</p></div>
<div class="m2"><p>باشدم پیش هر کسی مقدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شادی آنکسی به جان جویم</p></div>
<div class="m2"><p>که ز دل جوید او مرا آزار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکنم تکیه بر زمانه از آنک</p></div>
<div class="m2"><p>واقفم بر زمانة غدّار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بستانم به لطف و خوش بدهم</p></div>
<div class="m2"><p>زانکه هستم بخوش دمی چو بهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون خزان بر سرم زرافشانست</p></div>
<div class="m2"><p>زان که هستم لطیف و خوش دیدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان مانیّ و صورت آزر</p></div>
<div class="m2"><p>بر سر دست من گرفته قرار</p></div></div>