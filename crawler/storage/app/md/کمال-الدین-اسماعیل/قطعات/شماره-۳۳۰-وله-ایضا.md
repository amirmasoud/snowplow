---
title: >-
    شمارهٔ ۳۳۰ - وله ایضا
---
# شمارهٔ ۳۳۰ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>بجان آفرینی که نزدیک عملش</p></div>
<div class="m2"><p>ز پیدا تفاوت ندارد نهانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو امرش ز صنع اقتضا کرد فطرت</p></div>
<div class="m2"><p>بهم ممتزج گشت جسمیّ و جانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همای خرد سایه گسترد بر سر</p></div>
<div class="m2"><p>چو بفراشت این خانة استخوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدین منظر دیده صنعش به قدرت</p></div>
<div class="m2"><p>دو هندو نشاند از پی دیده بانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که رد معرض کلک شکر زبانت</p></div>
<div class="m2"><p>حرامست بر پسته شیرین زبانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو فکرت به معراج معنی خرامد</p></div>
<div class="m2"><p>همه حورعین آورد ارمغانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سنگی که بر وی نگارند شعرت</p></div>
<div class="m2"><p>گشاده شود چشمۀ زندگانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهی رسم کلک تو گوهر نگاری</p></div>
<div class="m2"><p>زهی شغل صیت تو گیتی ستانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خطّ تو زلف بتان دلشکسته</p></div>
<div class="m2"><p>ز کلک تو در غمزه ها ناتوانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز دریا دلی طبع تو هر زمانم</p></div>
<div class="m2"><p>فرستد عقود گهر رایگانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی قطعه دیدم ز انشای طبعت</p></div>
<div class="m2"><p>چو گوهر که در مغز عنبر نشانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترو تازه همچون گل نو شکفته</p></div>
<div class="m2"><p>خوش و نغز چون روزگار جوانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو طبع تو دروی فنون لطافت</p></div>
<div class="m2"><p>چو ذات تو دروی هزاران معانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بحطّی چو زنجیر مشکین مقیّد</p></div>
<div class="m2"><p>ولیکن روان همچو آب از روانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سخن ز آسمان بر زمین آمد اوّل</p></div>
<div class="m2"><p>کنونش تو بر آسمان میرسانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر در جواب تو تاخیر کردم</p></div>
<div class="m2"><p>گه از ناتوانی و گاه از توانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز شرمت بدانگونه باریک گشتم</p></div>
<div class="m2"><p>که از معنی خویش بازم ندانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به تقصیر خود معترف بودم امّا</p></div>
<div class="m2"><p>به نوعی دگر برده بودی کمانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سبک چون فرستم بنزد تو شعری؟</p></div>
<div class="m2"><p>که دور از تو ، نفزاید الّا گرانی</p></div></div>