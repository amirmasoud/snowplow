---
title: >-
    شمارهٔ ۱۴۷ - ایضا له
---
# شمارهٔ ۱۴۷ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>مشو ایمن ز کید خصم ضعیف</p></div>
<div class="m2"><p>کز تو اندیشۀ گریغ کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تار ابریشم ار چه باریکست</p></div>
<div class="m2"><p>وقت باشد که کار تیغ کند</p></div></div>