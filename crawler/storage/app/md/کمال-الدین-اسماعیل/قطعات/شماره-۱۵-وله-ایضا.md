---
title: >-
    شمارهٔ ۱۵ - وله ایضا
---
# شمارهٔ ۱۵ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ای سروری که مخزن اسرارغیب رابهتر</p></div>
<div class="m2"><p>بهتر کلیدخاطر مشکل گشای تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجاست نزهت دل و دانش که روی تست</p></div>
<div class="m2"><p>وانجاست قبلة مه واختر که رای تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عزم تو جز منازل اقبال نسپرد</p></div>
<div class="m2"><p>تا نور رأی روشن تو رهنمای تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید کیمیاگر و دریای جوهری</p></div>
<div class="m2"><p>هر یک چو بنگری بحقیقت گدای تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر رتبت معالی تو عقل کی رسد؟</p></div>
<div class="m2"><p>کانجا که انتهای ویست ابتدای تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اجزای کاینات دعای تو می کنند</p></div>
<div class="m2"><p>زیرا که از مصالح کلّی بقای تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در غیبت تو هر سحری بر در نیاز</p></div>
<div class="m2"><p>در دست جان صحیفة ورد دعای تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک دل پر از امید مرا پیش روی تست</p></div>
<div class="m2"><p>زیرا دو چشم پر ز سر شکم قفای تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جانم که در تنست به مهر تو محکمست</p></div>
<div class="m2"><p>عمرم که می رود گذرش بر هوای تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درحضرت تو گر چه بر آن آب نیستم</p></div>
<div class="m2"><p>بیگانه چون شوم که دلم آشنای تست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر گوش می کنم، هوس من حدیث تست</p></div>
<div class="m2"><p>ورچشم می زنم ،نظرم بر لقای تست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیرست تا که بر در ابنای روزگار</p></div>
<div class="m2"><p>مکیال خرمن نفس من ثنای تست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترسم زبالکانۀ دیده برون جهد</p></div>
<div class="m2"><p>این چند قطره خون که محلّ وفای تست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون بر در تو حلقة گستاخیی زنم</p></div>
<div class="m2"><p>دربان احتشام تو گوید چه جای تست؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پروانه داده یی که رسوم تو رایجست</p></div>
<div class="m2"><p>رسمی که ناگزیر منست آن رضای تست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مشنو تو این حدث ازو، از کرم شنو</p></div>
<div class="m2"><p>کآواز می دهد که فلان خاکپای تست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کردم هزینه در ره مدح تو نقد عمر</p></div>
<div class="m2"><p>وراندکی بمانداز آن هم برای تست</p></div></div>