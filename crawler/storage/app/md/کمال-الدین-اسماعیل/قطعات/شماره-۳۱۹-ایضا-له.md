---
title: >-
    شمارهٔ ۳۱۹ - ایضا له
---
# شمارهٔ ۳۱۹ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>جهان لطف و کرم افتخار اهل قلم</p></div>
<div class="m2"><p>که نیست فضل و هنر را به از تو غمخواری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه هرگز از تو رسیده به مویی آژنگی</p></div>
<div class="m2"><p>نه هرگز از تو رسیده به موری آزاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا حکایت آزاد مردی تو رود</p></div>
<div class="m2"><p>بر آن زبنده و آزاد نیست انکاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه جز تو بسی خواجگان قلم دارند</p></div>
<div class="m2"><p>ولی تو دیگری و دیگران دگر آری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طمع بخامۀ بیمار تو همی دارم</p></div>
<div class="m2"><p>که بی جگر بدهد آرزوی بیماری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخدمت تو از آن نامدم که نیست هنوز</p></div>
<div class="m2"><p>مرا نه رنگ نشستی نه روی رفتاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولیک زحمتت آورده ام به دست کسان</p></div>
<div class="m2"><p>چگونه زحمت، دانی چنان که هر باری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بتو که صاحب دستاری التجا کردم</p></div>
<div class="m2"><p>که تا وسیک جنسیّتی بود باری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن حوالت من بر در کله داران</p></div>
<div class="m2"><p>که خاک بر سر هر مدبری کله داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز باد سبلت و قند ز مدمّغند چنان</p></div>
<div class="m2"><p>که پیش ایشان نرزد سری به دستاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمال قسمت این بقعۀ خراب که نیست</p></div>
<div class="m2"><p>بهیچ حالی مرغوب هیچ هشیاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرت میسر گردد بکن مسامحتی</p></div>
<div class="m2"><p>بنام خادم داعی به چند دیناری</p></div></div>