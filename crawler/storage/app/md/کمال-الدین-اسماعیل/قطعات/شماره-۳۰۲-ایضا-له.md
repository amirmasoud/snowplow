---
title: >-
    شمارهٔ ۳۰۲ - ایضا له
---
# شمارهٔ ۳۰۲ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>من نه مردار خوارم ای خواجه</p></div>
<div class="m2"><p>چه فرستی بنزد من لاشه؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چه مرغی که باز نشناسی</p></div>
<div class="m2"><p>طوطی از زاغ و بلبل از باشه؟</p></div></div>