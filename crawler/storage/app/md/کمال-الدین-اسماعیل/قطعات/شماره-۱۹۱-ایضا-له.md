---
title: >-
    شمارهٔ ۱۹۱ - ایضا له
---
# شمارهٔ ۱۹۱ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>این واقعۀ هایل جانسوز ببینید</p></div>
<div class="m2"><p>وین حادثۀ صعب جگر سوز ببینید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر باز ببینید ستم کردن گنجشگ</p></div>
<div class="m2"><p>بر شیر شغالان شده پیروز ببینید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن سلطنت و قاعده و حکم که دی بود</p></div>
<div class="m2"><p>وین عجز و پریشانی امروز ببینید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دود دل خلق درین ماتم خون بار</p></div>
<div class="m2"><p>یک شهر پر از آتش دلسوز ببینید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور عیسی یک روزه ندیدی که سخن گفت</p></div>
<div class="m2"><p>نقّالی این طفل نو آموز ببینید</p></div></div>