---
title: >-
    شمارهٔ ۲۱۸ - وله ایضا
---
# شمارهٔ ۲۱۸ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>بزرگوارا، خّط و عبارتت ماند</p></div>
<div class="m2"><p>به شاهدی که به رخ بر کشد نقابی خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که چاشنیی یافت از عبارت تو</p></div>
<div class="m2"><p>به ذوق او نبود در جهان شرابی خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو دست گوهر بار و شکوه طلعت تو</p></div>
<div class="m2"><p>چو نو بهاران باران و آفتابی خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو خلق فایح تو بر ضمیر من گذرد</p></div>
<div class="m2"><p>ز طبع من مترشّح بود گلابی خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پریر، زهره همی گفت، زهره نیست مرا</p></div>
<div class="m2"><p>ز بیم حسبت تو بر زدن زبانی خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بارگاه تو تا من حدیث خویش کنم</p></div>
<div class="m2"><p>شب دراز ببایست و ماهتابی خوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ملک و جاه تو آیا چه نقص ره یابد؟</p></div>
<div class="m2"><p>که گردد از تو دل ریش دردیابی خوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عتابهای تو با بنده ناخوشیها کرد</p></div>
<div class="m2"><p>وگرچه باشدم از تو همه عتابی خوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی شود ز جگر خوردنم عتاب تو سیر</p></div>
<div class="m2"><p>مگر بدست نمی آیدش کبابی خوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدان طمع که رضای تو گرددم حاصل</p></div>
<div class="m2"><p>شدست بر دل تنگم همه عذابی خوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هزار بار مرا عفو کرده یی و هنوز</p></div>
<div class="m2"><p>نگشت طبع تو با من به هیچ با بی خوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر که مدّت ده سال هست یا افزون</p></div>
<div class="m2"><p>که از شماتت اعدا نخوردم آبی خوش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به لفظ شیرین از تو سؤالکی کردم</p></div>
<div class="m2"><p>بدان طمع که کنم از تو اجتذابی خوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرفتم آنکه چهل سال آن نه من بودم</p></div>
<div class="m2"><p>که شب نکردم از اندیشة تو خوابی خوش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرفتم آنکه نه من بوده ام که ساخته ام</p></div>
<div class="m2"><p>ز مدحت تو و اسلاف تو کتابی خوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنان قصیده، چو من بنده، در چنان معرض</p></div>
<div class="m2"><p>به چون تو خواجه فرستد، کم از جوابی خوش</p></div></div>