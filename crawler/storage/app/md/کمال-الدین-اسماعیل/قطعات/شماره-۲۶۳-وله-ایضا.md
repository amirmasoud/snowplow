---
title: >-
    شمارهٔ ۲۶۳ - وله ایضا
---
# شمارهٔ ۲۶۳ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ای که در خانۀ تو بیگه و گاه</p></div>
<div class="m2"><p>اندر اید همه کس جز مهمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سفره نان تو گر عورت نیست</p></div>
<div class="m2"><p>چه کنی از همه خلقش پنهان؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیست جز نان تو در خانۀ تو</p></div>
<div class="m2"><p>که تو او را ننمودی دندان؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رو که از اهل بهشتی گر زانک</p></div>
<div class="m2"><p>اعتقاد تو درستست چو نان</p></div></div>