---
title: >-
    شمارهٔ ۱۷۸ - ایضا له
---
# شمارهٔ ۱۷۸ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>خدایگان شریعت امام روی زمین</p></div>
<div class="m2"><p>که شمع رای تو از آسمان لگن خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه زحمت بسیار می دهم همه وقت</p></div>
<div class="m2"><p>زبان حال بهر حال عذر من خواهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه کم طمع بود آن شاعری که از ممدوح</p></div>
<div class="m2"><p>بهای جبّه و دستار خویشتن خواهد</p></div></div>