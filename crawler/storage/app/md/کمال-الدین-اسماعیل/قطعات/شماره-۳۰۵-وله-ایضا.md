---
title: >-
    شمارهٔ ۳۰۵ - وله ایضا
---
# شمارهٔ ۳۰۵ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>جود خواجه سیاه رویم کرد</p></div>
<div class="m2"><p>هم بر آنسان که رنگ رز جامه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد و جنسی که داده بود مرا</p></div>
<div class="m2"><p>شرح بشنو ز زّر واز جامه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثلثی بود ز اصل ده دینار</p></div>
<div class="m2"><p>ثلثانی ز هشت گز جامه</p></div></div>