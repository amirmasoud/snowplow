---
title: >-
    شمارهٔ ۲۶۷ - وله ایضا
---
# شمارهٔ ۲۶۷ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>امام عالم و قطب جهان جمال الّدین</p></div>
<div class="m2"><p>که شد ز خاطر تو منفجر زهاب سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان معنی روشن شود چو بدرخشد</p></div>
<div class="m2"><p>ز صبح صادق انفاست آفتاب سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرد بلب گزد انگشت پیش خامۀ تو</p></div>
<div class="m2"><p>هرانگهی که رود نکته یی ز باب سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آب تیرۀ کلکت که قلزمی دگرست</p></div>
<div class="m2"><p>همی پدید شود گوهر خوشاب سخن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر سخنها در چنبر خطت زانست</p></div>
<div class="m2"><p>که هست خامۀ تو مالک الرّقب سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپای در فندش عقل از سر مستی</p></div>
<div class="m2"><p>چو ساقی قلمت در دهد شراب سخن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد بچشم تعجّی به سوی او نگرد</p></div>
<div class="m2"><p>چو کلک تو کند از مشک تر نقاب سخن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن بکنه معالی تو چو می نرسد</p></div>
<div class="m2"><p>بخیره خیر چرا می دهم عذاب سخن؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن چگونه فرستم بنزد تو که کند</p></div>
<div class="m2"><p>عطارد از سر کلک تو اجتذاب سخن؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز صد یکی نپسندد برای مدحت تو</p></div>
<div class="m2"><p>خرد که پیشۀ او هست انتخاب سخن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تری شعر من از غایت لطافت نیست</p></div>
<div class="m2"><p>که بیشتر ز خوی خجلتست آب سخن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اسیر دهشت این حضرتست طبع رهی</p></div>
<div class="m2"><p>نشان آن بتوان دید از اظطراب سخن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سخن به هیچ حساب ارچه در نمی آید</p></div>
<div class="m2"><p>نه هر چه گفته شود باشد از حساب سخن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی نیارم کآرم سخن بحضرت تو</p></div>
<div class="m2"><p>ولیک لطف تو میآورد مرا بسخن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عروس خاطر من مهر کرد بر تو حلال</p></div>
<div class="m2"><p>که استماع تو خود بس بود ثواب سخن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جواب شعر، قبول از تو چشم می دارم</p></div>
<div class="m2"><p>صدا بود که فرستد سخن جواب خن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>درازتر زین با تو مرا سخنها بود</p></div>
<div class="m2"><p>اگر نه بیم ملالت شدی حجاب سخن</p></div></div>