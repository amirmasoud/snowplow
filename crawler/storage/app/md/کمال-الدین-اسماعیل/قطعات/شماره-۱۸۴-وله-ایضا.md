---
title: >-
    شمارهٔ ۱۸۴ - وله ایضاً
---
# شمارهٔ ۱۸۴ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>فصل دیماه بخوارزم اندر</p></div>
<div class="m2"><p>جامع گرهست یکی صد باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمد و آتش و آبست کنون</p></div>
<div class="m2"><p>عوض از خیش و زگنبد باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب چون بیضۀ بّلور شدست</p></div>
<div class="m2"><p>خانه پر خرمن بّد باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر فرشتست دراین فصل او را</p></div>
<div class="m2"><p>بضرورت سلب دد باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پوستینی ببد و نیک مرا</p></div>
<div class="m2"><p>گر بود نیک و گر بد باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پوستینی ز تو دق خواهم کرد</p></div>
<div class="m2"><p>گرچه دانم که تو را خود باید</p></div></div>