---
title: >-
    شمارهٔ ۲۱۶ - ایضا له
---
# شمارهٔ ۲۱۶ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>ای ضمیر تو غیب را جاسوس</p></div>
<div class="m2"><p>وی تو مسعود و دشمنت منحوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدّتی رفت تا مرا کرمت</p></div>
<div class="m2"><p>نه ز مطعوم داد و نه ملبوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده یی حبس رسم من بی جرم</p></div>
<div class="m2"><p>وین هم از بخت و طالع معکوس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چیست موجب که از میان رسوم</p></div>
<div class="m2"><p>رسم من گشت ناگهان مطموس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن ایخواجه رسم من بفرست</p></div>
<div class="m2"><p>مشکن بیش از این مرا ناموس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور گناهی حوالت است به من</p></div>
<div class="m2"><p>غلّه مطلق کن و مرا محبوس</p></div></div>