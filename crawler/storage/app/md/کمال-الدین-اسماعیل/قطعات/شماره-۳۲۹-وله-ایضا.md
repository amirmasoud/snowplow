---
title: >-
    شمارهٔ ۳۲۹ - وله ایضا
---
# شمارهٔ ۳۲۹ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ای لطف تو آب زندگانی</p></div>
<div class="m2"><p>وی ذات تو عالم معانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چشم خرد ز روی معنی</p></div>
<div class="m2"><p>بایسته تری ز زندگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در طبع هنر ز راه صورت</p></div>
<div class="m2"><p>شایسته تری ز شادمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ننهفته ز منهی ضمیرت</p></div>
<div class="m2"><p>اجرام سپهر سوزیانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدار تو از خوشیّ و راحت</p></div>
<div class="m2"><p>چون دولت و مستی و جوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهر تو مرا چو جان عزیزست</p></div>
<div class="m2"><p>از کف ندهم برایگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دل باشد دعای خادم</p></div>
<div class="m2"><p>نه چون دگران سر زبانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تشریف رهی نداد این بار</p></div>
<div class="m2"><p>کلک تو به عذر ناتوانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راضی شدم ار ز ناتوانیست</p></div>
<div class="m2"><p>اندی که نباشد از توانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر من که سبک دلم ز شوقت</p></div>
<div class="m2"><p>از بهر چه کرد سرگرانی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتی که دعا نمی نویسی</p></div>
<div class="m2"><p>این شیوه به من مبر گمانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر بنده نوشتن است و آنرا</p></div>
<div class="m2"><p>دادن به الاغ و کاروانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیکن نتواندش نگه داشت</p></div>
<div class="m2"><p>از آفتهای آسمانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این هم ز شقاوت دعاگوست</p></div>
<div class="m2"><p>گر خدمت او تو می نخوانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گه گاه ز روی لطف آخر</p></div>
<div class="m2"><p>یاد آر ز بنده گر توانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر یاد کنی ز من وگرنه</p></div>
<div class="m2"><p>من آن تو ام دگر تو دانی</p></div></div>