---
title: >-
    شمارهٔ ۱۹۸ - ایضا له
---
# شمارهٔ ۱۹۸ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>حسن رایت دانم ار آگه شود</p></div>
<div class="m2"><p>زینچ من کردم درین وقت اختیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بتر جایم کند حالی بخشم</p></div>
<div class="m2"><p>پنچ شش خروار ازین دار چنار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>