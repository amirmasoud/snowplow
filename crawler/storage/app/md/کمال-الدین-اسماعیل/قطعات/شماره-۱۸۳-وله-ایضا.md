---
title: >-
    شمارهٔ ۱۸۳ - وله ایضا
---
# شمارهٔ ۱۸۳ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>نیم مستست چشم دلبر من</p></div>
<div class="m2"><p>خوابش از هیچگونه می ناید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم دارم که تو بحکم کرم</p></div>
<div class="m2"><p>زان شرابی که جان بیفزاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم او را به خواب بسته کنی</p></div>
<div class="m2"><p>تا ازو کار بنده بگشاید</p></div></div>