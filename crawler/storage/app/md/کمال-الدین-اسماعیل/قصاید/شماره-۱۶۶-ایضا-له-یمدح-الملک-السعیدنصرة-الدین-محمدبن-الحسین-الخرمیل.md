---
title: >-
    شمارهٔ ۱۶۶ - ایضا له یمدح الملک السّعیدنصرة الدّین محمّدبن الحسین الخرمیل
---
# شمارهٔ ۱۶۶ - ایضا له یمدح الملک السّعیدنصرة الدّین محمّدبن الحسین الخرمیل

<div class="b" id="bn1"><div class="m1"><p>زهی برفلک سوده پرّ کلاه</p></div>
<div class="m2"><p>سزاوار دیهیم وزیبای گاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک نصرة الدّین،پناه ملوک</p></div>
<div class="m2"><p>که خورشیدملکی وظلّ اله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوشته کفت نام دریا برآب</p></div>
<div class="m2"><p>فکنده دلت نام بیژن بچاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شودچون قبا سینۀ خصم چاک</p></div>
<div class="m2"><p>چوتوبرنهادی زآهن کلاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز زخم سرنیزۀ تو هنوز</p></div>
<div class="m2"><p>نشانی بماندست برروی ماه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمندگلوگیر تو صبح را</p></div>
<div class="m2"><p>ببندد همی برنفس راه آه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهد لطف تو آرزو رانوید</p></div>
<div class="m2"><p>کند سهم تو مغز فکرت تباه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا نور برسایه پیشی کند</p></div>
<div class="m2"><p>بروعدلت ار زانکه گیرد گواه؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بفرمان توتیغ، جز کلک را</p></div>
<div class="m2"><p>نبرّید هرگز سربی گناه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زندخنده در روی خواهندگان</p></div>
<div class="m2"><p>دهان زر از نام تو قاه قاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوی شست تابد بفرمان تو</p></div>
<div class="m2"><p>سرتیر پرتابی از نیمه راه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کمان توسختی بسی میکشد</p></div>
<div class="m2"><p>ازآن پشت داردهمیشه دوتاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درآن خطّه کش قهرمان رای تست</p></div>
<div class="m2"><p>نگردد هوا برخرد پادشاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برو بد بمژگان چشم،آفتاب</p></div>
<div class="m2"><p>غبار درت بامدادان بگاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گهر زان برآورد شمشیر تو</p></div>
<div class="m2"><p>که دربحردستت رودگاه گاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپهر بلند از ره کهکشان</p></div>
<div class="m2"><p>خدنگ تراساخت آماجگاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سنان تواندر تن بدسگال</p></div>
<div class="m2"><p>چو آبی نهفتست در زیرکاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هلال شب عید فتح و ظفر</p></div>
<div class="m2"><p>به ازنعل شبدیز خسرو مخواه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که روز وغا هرکجاشد پدید</p></div>
<div class="m2"><p>بود چشم نصرت بدان جایگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگرسوی گردون کندگاه خشم</p></div>
<div class="m2"><p>کمانت بدنبال ابرو نگاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زسهم خدنگت بروز سپید</p></div>
<div class="m2"><p>درآید بچشم خور آب سیاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وگرسایۀ دستت افتد براو</p></div>
<div class="m2"><p>برآید زسنگ ترازو گیاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بروزیکه باشد از آوای کوس</p></div>
<div class="m2"><p>زخواب سکون فتنه را انتباه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به پشتی خنجر بودآب روی</p></div>
<div class="m2"><p>بمقدارمردی بود قدر و جاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شودتیره سرچشمۀ زندگی</p></div>
<div class="m2"><p>زگردی که خیزد میان سپاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرنیزه سازد زدل تکیه جای</p></div>
<div class="m2"><p>لب تیغ گردد زجان بوسه خواه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرانیّ حمله کنددل سبک</p></div>
<div class="m2"><p>درازیّ نیزه شود عمرکاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>براومید بیرون شو از موج خون</p></div>
<div class="m2"><p>اجل میزند دست وپای شناه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زبس رخنه کزنیزه درتن بود</p></div>
<div class="m2"><p>نفس را فتد در ممر اشتباه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چوروی توبیند ، بداندیش را</p></div>
<div class="m2"><p>نماند بجز پشت کردن پناه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ببرّد زبیم تو گر ناوکت</p></div>
<div class="m2"><p>ندارددل دشمن آن دم نگاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کشف وار درسینه پنهان شود</p></div>
<div class="m2"><p>سردشمن اززخم کوپال شاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ایا پادشاهی که زیبد که عقل</p></div>
<div class="m2"><p>بیاموزد ازعدلت آیین و راه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدرگاه توگرکم آید رهی</p></div>
<div class="m2"><p>بودهم زتعظیم این بارگاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که ترسد که از دهشت آن مقام</p></div>
<div class="m2"><p>کندپای اوزحمتی برجباه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بماناد چندان که ازبس شمار</p></div>
<div class="m2"><p>بماند شمارندۀ سال وماه</p></div></div>