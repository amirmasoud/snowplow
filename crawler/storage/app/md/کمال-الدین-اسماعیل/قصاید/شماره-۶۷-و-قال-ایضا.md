---
title: >-
    شمارهٔ ۶۷ - و قال ایضاً
---
# شمارهٔ ۶۷ - و قال ایضاً

<div class="b" id="bn1"><div class="m1"><p>اسبی دارم که هرگز ایزد</p></div>
<div class="m2"><p>قانع ترازو نیافریند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا روز ز عشق جو همه شب</p></div>
<div class="m2"><p>از خرمن ماه خوشه چیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با حشر فکند دیدن جو</p></div>
<div class="m2"><p>دانه که درین جهان نبیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتند که جو نماند وزین غم</p></div>
<div class="m2"><p>میخواست که تعزیت گزیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پوشید پلاس و پاره یی کاه</p></div>
<div class="m2"><p>میخواهد تا درو نشیند</p></div></div>