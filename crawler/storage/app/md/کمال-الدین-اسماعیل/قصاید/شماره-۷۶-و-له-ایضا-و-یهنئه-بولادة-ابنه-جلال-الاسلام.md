---
title: >-
    شمارهٔ ۷۶ - و له ایضاً و یهنئه بولادة ابنه جلال الاسلام
---
# شمارهٔ ۷۶ - و له ایضاً و یهنئه بولادة ابنه جلال الاسلام

<div class="b" id="bn1"><div class="m1"><p>مرا ز خواب برانگیخت دوش وقت سحر</p></div>
<div class="m2"><p>نسیم باد صبا چون زگلستان بوزید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگوش جانم در گفت مژده کین ساعت</p></div>
<div class="m2"><p>یکی مسافر فرخنده پی زغیب رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآسمان بزرگی هلالی از نو تافت</p></div>
<div class="m2"><p>ببوستان معالی گلی ز نو شکفید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهان او فلک از آفتاب پر زر کرد</p></div>
<div class="m2"><p>بدین بشارت خوش صبح چون زبان بکشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نثار مقدم او را سپهر از انجم</p></div>
<div class="m2"><p>بریخت حالی قرّابه های مروارید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدانکه تا نرسد چشم زخمش از اختر</p></div>
<div class="m2"><p>بخواند فاتحه یی صبح و بر جهان بدهید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهر مدخنه آسا بر آتش خورشید</p></div>
<div class="m2"><p>چو دخنه سوخت هر آن دانۀ ستاره که دید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجب شبی ! به دو خورشید گشته آبستن</p></div>
<div class="m2"><p>که هر دو در نفس صبح آمدند پدید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بشب، ولادت او اتّفاق از آن افتاد</p></div>
<div class="m2"><p>که هم زغیب سوی مطرح سیاه چمید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب سیاه بلالایگیّ او برخاست</p></div>
<div class="m2"><p>چو در کنارش آرد خوش دروخندید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درست مغربی خور نهاد بر رویش</p></div>
<div class="m2"><p>سپهرچونکه بدین ماه پاره درنگردید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دویت و کاغذ ترتیب کرد از شب و صبح</p></div>
<div class="m2"><p>دبیر چرخ بدان تا نویسدش تعویذ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صبا که منهی اخبار روح پرور اوست</p></div>
<div class="m2"><p>برای انها این حال سوی باغ دوید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گل ارچه مفلس و بی برگ بود هم در حال</p></div>
<div class="m2"><p>قبای لعل و کلاه زمرّدش بخشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو آفتاب تباشیر غرّه اش را دید</p></div>
<div class="m2"><p>ز رشک قرطۀ کحلّی خویشتن بدرید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز نور آن گهر شب چراغ روز افروز</p></div>
<div class="m2"><p>شب سیاه سلب دامن از جهان درچید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پی قماط وار دست دایۀ تقدیر</p></div>
<div class="m2"><p>زاطلس شفق چرخ جامه ها ببرید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برای ساعد دست مبارکش گردون</p></div>
<div class="m2"><p>زخطّ ابیض واسود کلاوه یی بتنید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فلک زصبحتش پستان شیر پیش آورد</p></div>
<div class="m2"><p>بدایگانی پنداشت کوبخواست مزید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زهی خرف که فلک بو او نمی دانست</p></div>
<div class="m2"><p>کزین نژاد کسی شیر سفلگان نمکید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیاد شادی فرخنده طالع سعدش</p></div>
<div class="m2"><p>چو زهره مشتری اندر کشید جام نبید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فضای چرخ پر آواز خیر مقدم گشت</p></div>
<div class="m2"><p>چو گوش گیتی شرح قدوم او بشنید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ودیعه که زابر کرم صدف میداشت</p></div>
<div class="m2"><p>بروزگار گهر گشت و دوش ازو بچکید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خدایگان شریعت که نیز نپسندد</p></div>
<div class="m2"><p>براق همّتش از سبزه زار گردون خوید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زهی که خنجر سر تیز باهمه حدّت</p></div>
<div class="m2"><p>زهیبت تو نیارد بدست در بچخید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بعهد عدل تو رمح ارتطاولی کردست</p></div>
<div class="m2"><p>بتنگنای دل خصم در چو مار خزید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو خامه با سر ببریده هم تواند زیست</p></div>
<div class="m2"><p>زفیض طبع تو هرکس که شربتی بچشید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیاد قهر تو زهرش فرا دهان آمد</p></div>
<div class="m2"><p>زبان مار از این روی در دهان بکفید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هم احتشام تو در کوزۀ فقاعش کرد</p></div>
<div class="m2"><p>مخالفی که چو سیماب بر تو می بطپید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نگرکه دوستیت را بها چه باشد خود</p></div>
<div class="m2"><p>چو دشمنیّ ترا دشمنت بجای بخرید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نمانده است فلک را بر اهل معنی دست</p></div>
<div class="m2"><p>ز رشک قدر تو از بس که پشت دست گزید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زبان از آن ننهند در مخالفت خنجر</p></div>
<div class="m2"><p>که پاک گوهر پرهیزد از زبان پلید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زمالش ستم انصاف هیچ باقی نیست</p></div>
<div class="m2"><p>که داد عدل تو ترتیب او چنانکه سزید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که دست بوس تو چون خاتم تو اندر یافت ؟</p></div>
<div class="m2"><p>که نه ز بار زر و لعل قامتش بخمید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گشاده گردد بند طلسم اسکندر</p></div>
<div class="m2"><p>گر اهتمام تو دندان نمایدش چو کلید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سپهر قد را! اندر ادای مدحت تو</p></div>
<div class="m2"><p>رهیت خامشی از عجز و اضطرار گزید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گرفتم آنکه بهفتم فلک رسید سخن</p></div>
<div class="m2"><p>بر آستان جلال تو ، کار هست رسید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سخن زشوق ثنای تو گرچه صد پر شد</p></div>
<div class="m2"><p>هنوز می نتواند برآن مقام پرید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نه زیر دست من آمد سخن ، پس او که بود؟</p></div>
<div class="m2"><p>که پای قدر رفیع تو بایدش بوسید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بزدی تو شاد، که چشم بدان زبد چشمی</p></div>
<div class="m2"><p>زحضرت تو بدین مسند سیه برمید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گشاده بود یکی مهره بر بساط جلال</p></div>
<div class="m2"><p>وزین سبب دل خلقی همی نیارامید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کنون که گشت قوی پشت ازین دگر هم پشت</p></div>
<div class="m2"><p>زمانه دست تصرّف زهردو بازکشید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگرچه قافیه لحنست از برای دعا</p></div>
<div class="m2"><p>بگفت خواهم بیتی بذوق نیک لذیذ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همیش سایۀ این آفتاب ملّت و دین</p></div>
<div class="m2"><p>بدین دو پیکر پاینده باد تا جاوید</p></div></div>