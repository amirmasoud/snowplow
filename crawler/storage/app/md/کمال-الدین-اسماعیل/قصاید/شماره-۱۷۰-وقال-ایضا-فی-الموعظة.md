---
title: >-
    شمارهٔ ۱۷۰ - وقال ایضاً فی الموعظة
---
# شمارهٔ ۱۷۰ - وقال ایضاً فی الموعظة

<div class="b" id="bn1"><div class="m1"><p>ایا بگام هوس راه عمر پیموده</p></div>
<div class="m2"><p>هنوز سیر نگشتی ز کار بیهوده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روا بود که توعمری بسربری که درآن</p></div>
<div class="m2"><p>نه تو زخودنه کسی ازتوگرددآسوده؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میاز دست بخوان جهان که عقل براو</p></div>
<div class="m2"><p>ندیدجزدل بریان واشک پالوده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی توقع بخشایش ازتوچون دارد</p></div>
<div class="m2"><p>بعمرخوش توبرخویشتن نبخشوده؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گره برابرو و کیسه نهاده یی وآنگاه</p></div>
<div class="m2"><p>زبان ودست بدشنام و جور بگشوده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روان آدم می نازد از چو تو خلفی</p></div>
<div class="m2"><p>که حورعین بفروشی بشاة موقوده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زعرش تابثری ازپی تودربیگار</p></div>
<div class="m2"><p>توجزکفایت خودرادرآن بنستوده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل شکسته پسندندناقدان بصیر</p></div>
<div class="m2"><p>درست قلب نخواهندروی اندوده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگرخودآتشی ای میر،هم فرو میری</p></div>
<div class="m2"><p>وگرخودآهنی ای خواجه،هم شوی سوده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکونات نپیچند سر ز فرمانت</p></div>
<div class="m2"><p>اگرتودست بداری خلاف فرموده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بچشم خویش بدیدی وباورت هم نیست</p></div>
<div class="m2"><p>عجایبی که چنان هیچ گوش نشنوده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد از بسیط جهان کاسته سه چاراقلیم</p></div>
<div class="m2"><p>ترا بیک جو در اعتبار نفزوده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه تخمهای برومندرابباغ جهان</p></div>
<div class="m2"><p>زمانه کشته وپس نارسیده بدروده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه شمعهای دل افروز رابباداجل</p></div>
<div class="m2"><p>جهان بکشته واندوه بررخش دوده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کجاشدندسلاطین که چرخ باعظمت</p></div>
<div class="m2"><p>غبار درگهشان جزبدیده نبسوده؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سرسنان یکی روی مه خراشیده</p></div>
<div class="m2"><p>سم سمند یکی پشت گاوفرسوده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شب دراز ز آواز پاسبانانشان</p></div>
<div class="m2"><p>ستارگان را تا روز دیده نغنوده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان بخواب عدم درشدندناگاهان</p></div>
<div class="m2"><p>که شد ز هستی ایشان وجود پالوده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خراب وهالک،درپای مستی افتادند</p></div>
<div class="m2"><p>بکاسۀ سرشان بادخاک پیموده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تن ملوک جهان بین درآرزوی کفن</p></div>
<div class="m2"><p>زخاک خوار تر افتاده توده برت وده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بپای اسب خران همچونعل سوده سری</p></div>
<div class="m2"><p>کلاه گوشۀ نخوت برآسمان سوده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به پشت پای ملامت زده وحوش وسباع</p></div>
<div class="m2"><p>رخی ز ناز بآیینه روی ننموده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شکیل پای ستوران شده سرزلفی</p></div>
<div class="m2"><p>کز او گره بجز از دست شانه نگشوده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کجاست آن تن و اندام سایه پرورده؟</p></div>
<div class="m2"><p>کجاست آن رخ چون آفتاب نزدوده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه کردآن همه سیم بغارت آورده؟</p></div>
<div class="m2"><p>که خورد آن همه زر بزور بربوده؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زپشت اسب جداگشته شاه رخ برخاک</p></div>
<div class="m2"><p>پیاده مانده سرش پای پیل بشخوده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رخی که سایۀ برگ گلش نیازرده</p></div>
<div class="m2"><p>لبی که هم زخودش بوسه آرزو بوده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زبان تیغ بلب روی این بخاییده</p></div>
<div class="m2"><p>دهان سگ بزبان کام آن بیالوده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه هیچ فایده این را زعدت ولشکر</p></div>
<div class="m2"><p>نه هیچ حاصلی آنرازرقیه وعوده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ببینی،ارتوکنی بازچشم عبرت بین</p></div>
<div class="m2"><p>که نسیه هاهمه نقدست وبوده نابوده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زخاک سجده گه وآب چشم یاری خواه</p></div>
<div class="m2"><p>که جزبدین نشودپاک جان آلوده</p></div></div>