---
title: >-
    شمارهٔ ۹۱ - فی الّذم
---
# شمارهٔ ۹۱ - فی الّذم

<div class="b" id="bn1"><div class="m1"><p>دی مرا گفت دوستی که مرا</p></div>
<div class="m2"><p>با فلان خواجه از پی دو سه کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخنی چند هست وز پی آن</p></div>
<div class="m2"><p>خلوتی می ببایدم ناچار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلوتی آن چنان که اندر وی</p></div>
<div class="m2"><p>هیچ مخلوق را نباشد بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم این فرصت ار توانی یافت</p></div>
<div class="m2"><p>وقت نان خوردنش نگه می دار</p></div></div>