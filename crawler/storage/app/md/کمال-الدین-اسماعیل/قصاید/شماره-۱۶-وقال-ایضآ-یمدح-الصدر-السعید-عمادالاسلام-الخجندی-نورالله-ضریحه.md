---
title: >-
    شمارهٔ ۱۶ - وقال ایضآ یمدح الصّدر السّعید عمادالاسلام الخجندی نورالله ضریحه
---
# شمارهٔ ۱۶ - وقال ایضآ یمدح الصّدر السّعید عمادالاسلام الخجندی نورالله ضریحه

<div class="b" id="bn1"><div class="m1"><p>جهان سروری و پشت دودمان خجند</p></div>
<div class="m2"><p>که بندگیّ ترا اسمان بجان برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهال تو بر بستان سرای دانش و فضل</p></div>
<div class="m2"><p>که با مکاشفه ات روشن از نهان برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو برگرفت بیانت تتق ز روی ضمیر</p></div>
<div class="m2"><p>خرد چه گفت؟ زهی سحر کز بیان برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان ز پیری یکباره در سر آمده بود</p></div>
<div class="m2"><p>بدستگیری این دولت جوان برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ثبات حزم تو گویی بزد، زمین بنشست</p></div>
<div class="m2"><p>شکوه قدر ترا دید آسمان برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمانه نعرۀ الله اکبر اندر بست</p></div>
<div class="m2"><p>چو تیر عزم تو از خانۀ کمان برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخست روز که دست تو رسم جود نهاد</p></div>
<div class="m2"><p>غریو گرد ز هستیّ بحر و کان برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشست بر قلم انگشتت و منادی زد</p></div>
<div class="m2"><p>که از ذخیرۀ دریا و کان امان برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خارپشت بقصد عدو هم از تن خویش</p></div>
<div class="m2"><p>بجای هر سر مویش یکی سنان برخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز خلق و خوی تو می کرد سوسن آزادی</p></div>
<div class="m2"><p>برای بندگیش سرو بوستان برخاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فروغ رای تو در نیم شب تجلّی کرد</p></div>
<div class="m2"><p>هزار صبح بیک دم زهر کران برخاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میان آب تیّمم گزید مردم چشم</p></div>
<div class="m2"><p>بدان غبارکت از خاک آستان برخاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خمیر مایۀ ادبار بود خصم ترا</p></div>
<div class="m2"><p>بمانده بود ترش تا ز بهر نان برخاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عروس فضل ترا باش تا بیارایند</p></div>
<div class="m2"><p>که خود زبستر تحصیل این زمان برخاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مبارکیّ دم خلق تو بباغ رسید</p></div>
<div class="m2"><p>ز خواب نرگس بیمار ناتوان برخاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نمی دهم بقلم شرح شوق، زانکه مرا</p></div>
<div class="m2"><p>بدین سبب قلم از خاطر و بنان برخاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه من ز فرقت صدرت چه عاشقی که بقهر</p></div>
<div class="m2"><p>سحرگهی ز برش یاردلستان برخاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهی مقصّر وآنگه توقّع تشریف</p></div>
<div class="m2"><p>چنین ظریف جوانی ز اصفهان برخاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بزرگوارا بشنو حکایتی که پریر</p></div>
<div class="m2"><p>دلم بعربده با من ز ناگهان برخاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که شد ز موسم انعام خواجه مدّتها</p></div>
<div class="m2"><p>تو خفته یی و نخواهی برای آن برخاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چراش یاد نیاری، ز خامشی مانا</p></div>
<div class="m2"><p>که طفل ناطقت از حجرۀ دهان برخاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بخشم گفتمش ایمه چه ژاژ میخایی</p></div>
<div class="m2"><p>که این فلانه چنین خفت و آن فلان برخاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برو تو فارغ بنشین که رسم تو برسد</p></div>
<div class="m2"><p>اگر دو روز پس ماند نه جهان برخاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنین حدیثی رفتست و حق بدست ویست</p></div>
<div class="m2"><p>بیک ره از سر انصاف چون توان برخاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز سر من برون نشود ذوق آن عمامه مرا</p></div>
<div class="m2"><p>که تاج کسری با او ز یک مکان برخاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آن شرف سر من بر سر آمد از همه تن</p></div>
<div class="m2"><p>وزین حسد ز تنم ناله و فغان برخاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو برنخیزد دستار هرگز از سرما</p></div>
<div class="m2"><p>نشاید از سر دستار جاودان برخاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گرفتم از سر دستار خویش بر خیزم</p></div>
<div class="m2"><p>توانم از سر دستار خواجگان برخاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مکن ملامت بنده که اصل این فتنه</p></div>
<div class="m2"><p>نخست باری از آن دست در فشان برخاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بعون لطف تو دستار هم بدست آرم</p></div>
<div class="m2"><p>وگر چه واسطۀ عون از میان برخاست</p></div></div>