---
title: >-
    شمارهٔ ۱۸۰ - وله ایضاً یمدحه ببلد النّشابور
---
# شمارهٔ ۱۸۰ - وله ایضاً یمدحه ببلد النّشابور

<div class="b" id="bn1"><div class="m1"><p>جهان کرم پادشاه شریعت</p></div>
<div class="m2"><p>که هستت بر اقلیم دین شهر یاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو آن سرفرازی که فیض بنانت</p></div>
<div class="m2"><p>بریزد همی آب ابر بهاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو آنی که روی قدرت توانی</p></div>
<div class="m2"><p>که پیشانی شیر گردون بخاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو آن فیض بخشی که در روز جودت</p></div>
<div class="m2"><p>چو کان گشت دریا زبس خاکساری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک از سر صدق تو صبگاهی</p></div>
<div class="m2"><p>کند در هوایت چو من جان سپاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مزاج صبازان سبب روح بخش است</p></div>
<div class="m2"><p>که کردست با خلقت آمیزگاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درختان لطف ترا میوه آبی</p></div>
<div class="m2"><p>نهنگان خشم ترا معده ناری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قضا کی شدی ضامن رزق مردم ؟</p></div>
<div class="m2"><p>اگر نه کفت را گرفتی بیاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشایش زجود تو می یابد ارنی</p></div>
<div class="m2"><p>عروق امل را ببندد مجاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگاورسۀ مشک بر صفحۀ سیم</p></div>
<div class="m2"><p>کند کلک تو هر زمان خرده کاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بقای ابد را به محشر همانا</p></div>
<div class="m2"><p>بمسمار مهرت بود استواری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خور تیغ زن گرچه هرشب زبأست</p></div>
<div class="m2"><p>درین خاک توده گزیند تواری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ضمیر تو هر روز گیسو کشانش</p></div>
<div class="m2"><p>ببازار گیتی برآرد بخواری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وقار ترا کوه می خوانم انصاف</p></div>
<div class="m2"><p>ازین بیشتر چون بود بردباری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسیلاب انعام تو شسته گردد</p></div>
<div class="m2"><p>ز روی جهان وصمت خاکساری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قضا را بس است این قدر شغل کورا</p></div>
<div class="m2"><p>بدیوان حکمت بود پیشکاری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کسی را که یک ذرّه در سایه گیری</p></div>
<div class="m2"><p>زخورشید تابان سرش برگذاری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سوی مهر اگر بنگرد تیز کینت</p></div>
<div class="m2"><p>چو سایه بخاک اندر افتد بزاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو سلطان سیّار کان وجودی</p></div>
<div class="m2"><p>چو خورشید ازین روی لندر مداری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بقدر و بزرگی علی رغم دشمن</p></div>
<div class="m2"><p>بحمدالله امروز هریک هزاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فلک رفعتا ! پیش صدر توام هیچ</p></div>
<div class="m2"><p>زبان سخن نیست از شرمساری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درین چند روز از جفا آن کشیدم</p></div>
<div class="m2"><p>که گر برشمارم تو باور نداری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه از خاصۀ خود، چه از خویش و پیوند</p></div>
<div class="m2"><p>چه از شرمساری ، چه از سوگواری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همانا که اندر ازل کار ما را</p></div>
<div class="m2"><p>قرار افتادست بر بی قراری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کسی را که تیره شود آب دولت</p></div>
<div class="m2"><p>زآب حیاتش بود ناگواری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سزاوار آنی و در خورد اینم</p></div>
<div class="m2"><p>که بر ما و بر عجز ما رحمت آری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از آن می نمائیم بر جرم اقدام</p></div>
<div class="m2"><p>که عفوت زما می کند خواستاری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غریب و پراکنده و مستمندیم</p></div>
<div class="m2"><p>تبه حال و حیران زبد روزگاری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نباشد ترا ضایع از کردکارت</p></div>
<div class="m2"><p>اگر بی کسان را کنی دستیاری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حقوق قدیمیِ ما خود رها کن</p></div>
<div class="m2"><p>نه هستیم آخر ترا زینهاری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو هرکس رسیدند از دولت تو</p></div>
<div class="m2"><p>باسب و ستور و مهد و عماری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر خسته یی را زشوق رکابت</p></div>
<div class="m2"><p>کند فی المثل آرزوی سواری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>توقّع چنانست کز من دعا گوی</p></div>
<div class="m2"><p>بحکم کرم این گنه در گذاری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر اهل نیشابور فرخنده بادا</p></div>
<div class="m2"><p>قدوم تو در دولت کامکاری</p></div></div>