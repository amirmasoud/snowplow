---
title: >-
    شمارهٔ ۱۳۰ - و قال ایضآ یمدح الصّدر السّعید رکن الدّین صاعد
---
# شمارهٔ ۱۳۰ - و قال ایضآ یمدح الصّدر السّعید رکن الدّین صاعد

<div class="b" id="bn1"><div class="m1"><p>خفتۀ بیدار بودم دوش کز دارالسلام</p></div>
<div class="m2"><p>مسرع باد صبا آورد سوی من پیام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کای ز ضجرت کرده دایم روی در دیوار غم</p></div>
<div class="m2"><p>خیز کآمد گاه آن کز بخت باشی شادکام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند باشی از طرب تنها نشسته چون الف</p></div>
<div class="m2"><p>چند باشی زیر بار غم خمیده همچو لام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز نقد خوشدلیها کیسۀ طبعت تهیست</p></div>
<div class="m2"><p>خیز و بستان مایه یی از طبع نا اهلان بوام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کارهایی همچو جال افتاده دور از یکدگر</p></div>
<div class="m2"><p>دست در هم داد چون گوی انگل اکنون ز انتظام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانۀ دل پاک کن از گردانده وانگهی</p></div>
<div class="m2"><p>چشم شو بهر تماشا جمله تن مانند دام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتح باب دولتست امروز زیرا داده اند</p></div>
<div class="m2"><p>در سرای خاص سلطان شریعت بار عام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مطلع خورشید شد بار دگر برج شرف</p></div>
<div class="m2"><p>جلوگاه کعبه شد با ردگر بیت الحرام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل که چون سنگ سیه بد، یافت چون زمزم صفا</p></div>
<div class="m2"><p>تا که رکن شرع را در کعبه می بیند مقام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل با این خانه دید ار بیت معمور فلک</p></div>
<div class="m2"><p>هر زمان در حیرت افتد کین کدامست آن کدام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ربع مسکون از جوار آن همی یابد خطر</p></div>
<div class="m2"><p>سقف مرفوع از ستون او همی گیرد قوام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مهرومه را از برای خشت بامش ساختند</p></div>
<div class="m2"><p>این یکی از زرّ پخته وان یکی از سیم خام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بوده از شکل هلالش دوش گردون ناوه کش</p></div>
<div class="m2"><p>وافتابش روز و شب اندر گل اندایی بام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست رضوان ساحت فردوس گویی آب زد</p></div>
<div class="m2"><p>بس که از شرم نهادش خوی کند دارالسّلام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صبح از این معنی نماید هر نفس دست سپید</p></div>
<div class="m2"><p>تا بیفزود بدان صحن سرایش چون رخام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد شفق شنگرف و گردون کاسه های لاژورد</p></div>
<div class="m2"><p>مهر و ماهش شمسه و نقّاش چرخ خویش کام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از خواص این سرای آنست کآهختست تیغ</p></div>
<div class="m2"><p>بر در او حاجب الشّمس از پی دفع عوام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لطف و عنف خواجه دروی داد بار از بهر آن</p></div>
<div class="m2"><p>هم هوایش راست صحّت هم سمومش را سقام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاد باش ای هفت اجرام سماوی بر درت</p></div>
<div class="m2"><p>همچو پروین بر هم افتاده ز فرط ازدحام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خسرو سیّارگان لبّیک زد، چون قدر تو</p></div>
<div class="m2"><p>حلقۀ گردون گرفت و بانگ در زد کای غلام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از تواضع حلم تو همچون زمین سهل الفیاد</p></div>
<div class="m2"><p>وز ترفّع قدر تو همچون فلک صعب المرام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از لباس مستعار روز و شب ذاتت کنون</p></div>
<div class="m2"><p>بر حقست ار عار می دارد ز فرط احتشام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آسمان کو همچون در، حلقه بگوش این درست</p></div>
<div class="m2"><p>بندگیت را ز تحت الفرط کردست التزام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نطفه یی از صلب جودت زادۀ دریا و کان</p></div>
<div class="m2"><p>رشحه یی از بحر طبعت مایۀ فیض غمام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رخنه یی کز تیغ قهرت در دل خصم اوفتاد</p></div>
<div class="m2"><p>هم بنوک ناوک قهرت پذیرد التیام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پایمال نیستی گردد فلک همچون رکاب</p></div>
<div class="m2"><p>گر بتابی یکدم از کارش عنان اهتمام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پرتوی از رای تو گلگونۀ رخسار صبح</p></div>
<div class="m2"><p>گردی از میدان قهرت وسمۀ گیسوی شام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با وفاق تو نگنجد این دو رنگی در جهان</p></div>
<div class="m2"><p>با خلاف تو بیفتد سلک ایّام از نظام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با طبقهای نثار آید فلک از سیم و زر</p></div>
<div class="m2"><p>بامدادان تا کند بر خاک درگاهت سلام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صبح از این معنی ، درم ریزان بر اندازد نقاب</p></div>
<div class="m2"><p>مهر از این رو زرفشان آید پدید از راه بام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با یک اندازی کلکت تیر چرخ از دم زند</p></div>
<div class="m2"><p>همچون سوفارش زبان بیرون کشد گردون ز کام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر نکردی جاه تو تعدیل ذات مشتری</p></div>
<div class="m2"><p>هرگز او را کی بدی در محضر افلاک نام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پیش لفظ تو شکر شیرینی خود عرضه کرد</p></div>
<div class="m2"><p>عقل از این رو میکند چون پسته در لب ابتسام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دشمنت چون نار از آن رو سرخ روی آمد که شد</p></div>
<div class="m2"><p>قطره قطره خون اندامش فسرده در مسام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دست قدرت چون سراپرده بزد بر بام چرخ</p></div>
<div class="m2"><p>از مسامیر ثوابت ساخت اوتاد خیام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سحر کآید از سر کلکت بود سحر حلال</p></div>
<div class="m2"><p>بیت کان نبود مدیح نو بود بیت حرام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر نگویم مدح تو تیغ زبان در کام من</p></div>
<div class="m2"><p>باز گردد با شگونه همچو تیغ ! ندر نیام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مدح اخلاق تو کز وی عقل کلّ قاصر بود</p></div>
<div class="m2"><p>کی نماید کلک پی کرده بشرح آن قیام؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون صراحی از می مهرت تهی پهلو که کرد؟</p></div>
<div class="m2"><p>کش نگشت از دور گردون دل پر از خون همچو جام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای خداوندی که پیش نفخۀ اخلاق تو</p></div>
<div class="m2"><p>از نسیم گل، فلک چون غنچه برگیرد مشام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>روزگار دولت تو روز بازار هنر</p></div>
<div class="m2"><p>هجرت میمون تو تاریخ ایّام کرام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همچو میخ از سرزنش گردون فرو رفتی بخاک</p></div>
<div class="m2"><p>گر نکردی از تضرّع هم بحبلت اعتصام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دودمانت را گر آتش هم نفس شد باک نیست</p></div>
<div class="m2"><p>خانۀ خورشید لابد آتشی باشد مدام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چرخ وانجم در طواف خانه ات بودند وکرد</p></div>
<div class="m2"><p>آستانت را اثیر از روی تعظیم استلام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر نهاد آتش زبان در خاندان عصمتت</p></div>
<div class="m2"><p>لاجرم زان شد زبان زرنگارش قیر فام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در بهشت خانه ات آتش ازیرا راه یافت</p></div>
<div class="m2"><p>کو همی سوزد دل اعدای جاهت بر دوام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جرم اختر را ز برج محترق ناید گزند</p></div>
<div class="m2"><p>ذات گوهر را زکان کندن نکاهد احترام</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زرد و لرزان بر درت افتاد چون زنهاریان</p></div>
<div class="m2"><p>تا نخواهد خاطر و قّادت از وی انتقام</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شاید ار با آسمان پهلو زند چرخ اثیر</p></div>
<div class="m2"><p>کز سرافرازی گذارد بر چنین درگاه گام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همچو آتش اطلس زربفت پوشد آتشی</p></div>
<div class="m2"><p>هر که او بر آستانت کرد یک ساعت مقام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>من که هستم معتکف چون خاک بر درگاه تو</p></div>
<div class="m2"><p>از چه محرومم ز تشریفاتت ای صدر انام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آری آری روزه شرط اعتکاف آمد از آن</p></div>
<div class="m2"><p>دست گردون کرد بر کام من از حرمان لگام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تا که کّحال قدر از چرخ و انجم هر شبی</p></div>
<div class="m2"><p>سازد از کحل الجواهر سرمۀ چشم ظلام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>باد عمرت جاودان در دولت و بخت جوان</p></div>
<div class="m2"><p>باد کارت با نظام از دولت خواجه نظام</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>حال تو در رفعت و حال حسودت در خمول</p></div>
<div class="m2"><p>هم برین منوال بادا تا قیامت والسّلام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بر تو میمون باد این تحویل فرّخ کاوفتاد</p></div>
<div class="m2"><p>در سنۀ خمس و نمانین غرّۀ ماه صیام</p></div></div>