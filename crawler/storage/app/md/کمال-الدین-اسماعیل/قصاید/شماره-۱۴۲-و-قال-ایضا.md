---
title: >-
    شمارهٔ ۱۴۲ - و قال ایضاً
---
# شمارهٔ ۱۴۲ - و قال ایضاً

<div class="b" id="bn1"><div class="m1"><p>بعهدهای گذشته امین من آن بود</p></div>
<div class="m2"><p>که شعر خوانم بر آنکه سیم بستانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بقحط سالی افتادم از هنرمندان</p></div>
<div class="m2"><p>که گربیان کنم آنرا بشرح نتوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بیابم آنرا که شعر دریابد</p></div>
<div class="m2"><p>بدو دهم صلتی تا سخن بروخوانم</p></div></div>