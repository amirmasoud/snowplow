---
title: >-
    شمارهٔ ۱۱۷ - و قال ایضا یمدح الصّدر رکن الدّین
---
# شمارهٔ ۱۱۷ - و قال ایضا یمدح الصّدر رکن الدّین

<div class="b" id="bn1"><div class="m1"><p>هرگز کسی نداد بدینسان نشان برف</p></div>
<div class="m2"><p>گویی که لقمه ییست زمین در دهان برف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانند پنبه دانه که در پنبه تعبیه ست</p></div>
<div class="m2"><p>اجرام کوههاست نهان در میان برف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناگه فتاد لرزه بر اطراف روزگار</p></div>
<div class="m2"><p>از چه؟ ز بیم تاختن ناگهان برف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشتند ناامید همه جا نور ز جان</p></div>
<div class="m2"><p>با جان کوهسار چو پیوست جان برف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با ما سپید کاری از حد همی برد</p></div>
<div class="m2"><p>ابر سیاه کار که شد در ضمان برف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خان خرک شدست همه خان و مان ما</p></div>
<div class="m2"><p>بر یکدگر نشسته درو کاروان برف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چاه مقّنعست همه چاه خانها</p></div>
<div class="m2"><p>انباشته بجوهر سیماب سان برف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرکوه، پشم برزده گردد برستخیز</p></div>
<div class="m2"><p>کوهی زپشم برزده آنک مکان برف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین سان که سر بسینه ی گردون نهاد باز</p></div>
<div class="m2"><p>خورشید پای در ننهاد ز آستان برف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتش بدست و پای فرو مرد و برحقست</p></div>
<div class="m2"><p>مرغ شرر چگونه پر دز آشیان برف؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از روی خاک سر بعنان السّما کشید</p></div>
<div class="m2"><p>آن خنگ باد پای گسسته عنان برف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در خانقاه باغ نه صادر نه واردست</p></div>
<div class="m2"><p>تا پیر پنبه گشت حریف گران برف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از تیغ مهر و ناوک انجم خلاص یافت</p></div>
<div class="m2"><p>این ابلق زمانه زبر گستوان برف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد جویبار بالش نقره چو خفت باغ</p></div>
<div class="m2"><p>در آب رفت بستر چون پرنیان برف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صابو نیست صحن زمین لب بلب ز بس</p></div>
<div class="m2"><p>کاورد قند مصری بازرگانان برف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باشد خلاف رسم خطیبان روزگار</p></div>
<div class="m2"><p>زاغ سیه چو برفکند طیلسان برف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در بند کرد روی زمین را چو زال زر</p></div>
<div class="m2"><p>بهمن بدست لشکر گیتی ستان برف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این قرص آفتاب بنان پاره کرد خرج</p></div>
<div class="m2"><p>تا خیمه بر ولایت زد تورخان برف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سیلاب ظلم او در و دیوار می کند</p></div>
<div class="m2"><p>خود رسم عدل نیست مگر در جهان برف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ناگه فروگرفت درو بامها و پس</p></div>
<div class="m2"><p>بگرفت ریش خانه خدا ایرمان برف</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در خانه ها ز بس که فرود آمدست برف</p></div>
<div class="m2"><p>نامد به حلق خانه فرو هیچ نان برف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از نان و جامه خلق غنی گشتی اربدی</p></div>
<div class="m2"><p>ازآرد یا زپنبه تن ناتوان برف</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن کو برهنه باشد و بی برگ چون درخت</p></div>
<div class="m2"><p>کیمخت زود خشک کند در نهان برف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بی خنجر هلالی و بی تیغ آفتاب</p></div>
<div class="m2"><p>نتوان به تیرماه کشیدن کمان برف</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از بس که سر بخانه هرکس فرو برد</p></div>
<div class="m2"><p>سرد و گران و بی مزه شد میهمان برف</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرچه سپید کرد همه خان و مان ما</p></div>
<div class="m2"><p>یارب سیاه باد همه خان و مان برف</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وقتی چنین نشاط کسی را مسلّمست</p></div>
<div class="m2"><p>کاسباب عیش دارد اندر زمان برف</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هم نان و گوشت دارد و هم هیزم و شراب</p></div>
<div class="m2"><p>هم مطربی که بر زندش داستان برف</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>معشوقه یی مرکّب زا اضداد مختلف</p></div>
<div class="m2"><p>باطن بسان آتش و ظاهر بسان برف</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چشمش بروی یار بود گوش سوی چنگ</p></div>
<div class="m2"><p>در طبع او شکوفه نماید گمان برف</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از شادیش نظر نبود سوی غمگنان</p></div>
<div class="m2"><p>وزمستیش خبر نبود از عیان برف</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گلگونه ای بود بسپیداب بر زده</p></div>
<div class="m2"><p>هر جرعه یی که ریزد بر جرعه دان برف</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا رنگ روی یار نماند بدین قیاس</p></div>
<div class="m2"><p>بعضی از آن باده و بعضی از آن برف</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>می می خورد بکام و ز نخ می زند بجد</p></div>
<div class="m2"><p>در گوش خود رها نکند سوزیان برف</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آنرا که پوشش و می و خرگاه و آتشست</p></div>
<div class="m2"><p>وقت صبوح مژده دهد بر نشان برف</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وانجا که ساز عیش بدین سان میسّرست</p></div>
<div class="m2"><p>می باش گو فلان و فلان در فلان برف</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه همچو من که هر نفسش باد ز مهریر</p></div>
<div class="m2"><p>پیغامهای سرد دهد بر زبان برف</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دست تهی بزیر زنخدان کند ستون</p></div>
<div class="m2"><p>وندر هوا همی شمرد پودوتان برف</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خانه تهی ز چیز و ملا از خورندگان</p></div>
<div class="m2"><p>آبی بریق میخورد از ناودان برف</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هر لحظه دست چرخ بخروارها نمک</p></div>
<div class="m2"><p>بپراگند بدین دل ریش از امان برف</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دلتنگ و بی نوا چو بطان بر کنار آب</p></div>
<div class="m2"><p>خلقی نشسته ایم کران تا کران برف</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر قوّتم بدی ز پی قرص آفتاب</p></div>
<div class="m2"><p>بر بام چرخ رفتمی از نردبان برف</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ای منعم زمانه که گر عقل بشکند</p></div>
<div class="m2"><p>پر مغز و نعمت تو بود استخوان برف</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پشت و پناه دست قضا رکن دین آنک</p></div>
<div class="m2"><p>کز طبع نو بهار نماید خزان برف</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از کیسۀ سخای تو دزدیده کرد ابر</p></div>
<div class="m2"><p>سیمی که خرج می کند اکنون زکان برف</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اوّل ز خوان نعمت تو زلّه کرد و پس</p></div>
<div class="m2"><p>آنگه بگسترید در آفاق خوان برف</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تأثیر گفتۀ کرمت بر دهان خلق</p></div>
<div class="m2"><p>چون تیغ آفتاب بود بر میان برف</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>لطف شمایل تواگر بر جهان دمد</p></div>
<div class="m2"><p>برگ سمن پراکنده از بادبان برف</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سرمایه از وقار تو کردست اکتساب</p></div>
<div class="m2"><p>آن پیر پر مهابت آتش نشان برف</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در عهد عدل تو چو کسی سیم دزد نیست</p></div>
<div class="m2"><p>هندوی زاغ بهر چه شد پاسبان برف ؟</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هم سغبه ییست از نظر دوربین تو</p></div>
<div class="m2"><p>سودی که هست تعبیه اندرزیان برف</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مالید برف شیبت خود بر زمین بسی</p></div>
<div class="m2"><p>تا داد دست سیم کش تو امان برف</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>آب روان شود تن دشمن ز بیم تو</p></div>
<div class="m2"><p>گر بر نهند سکه بسیم روان برف</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ای آفتاب فضل! چنین روز یاد کن</p></div>
<div class="m2"><p>زان بینوا که هست کنون میزبان برف</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خورشید جودت ار نکند پشت گرمئی</p></div>
<div class="m2"><p>سرما کند شمار من از کشتگان برف</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>باران جودت ار نکند دست یاریی</p></div>
<div class="m2"><p>بیرون که آردم ز کف امتحان برف ؟</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چون برف در سخن ید بیضا نمودمی</p></div>
<div class="m2"><p>بیم ملالت ار نبدی در بیان برف</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کوته کنم که بس سبب پوستین بود</p></div>
<div class="m2"><p>دم سردی بدین صفت اندرزمان برف</p></div></div>