---
title: >-
    شمارهٔ ۱۸۲ - وله ایضاً
---
# شمارهٔ ۱۸۲ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>زین پس نبیند این دل من روی خوشدلی</p></div>
<div class="m2"><p>بر بسته کشت راه من از کوی خوشدلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمگین دلم که خوی گر درد و محنت است</p></div>
<div class="m2"><p>تا غم بود کجا نگرد سوی خوشدلی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی بر بماند کشت امیدم از آنکه نیست</p></div>
<div class="m2"><p>آب حیات را مدد از جوی خوشدلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مجمر ار چه سینۀ تنگم پر آتشست</p></div>
<div class="m2"><p>زین سوخته جگر ندمد بوی خوشدلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عرصۀ وجود اگر چه بسر دوم</p></div>
<div class="m2"><p>چوگان قامتم بنزد گوی خوشدلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این طرفه بین که در دل تنگم هزار غم</p></div>
<div class="m2"><p>گنجید و می نگنجد یک موی خوشدلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگرفت های های گرستن همه جهان</p></div>
<div class="m2"><p>بنشست با دو بانگ و هیاهوی خوشدلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چاووش ناله در همه آفاق بانگ زد</p></div>
<div class="m2"><p>وای دلی که هست هواجوی خوشدلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه غم شکیبد از من و نه من ز غم کنون</p></div>
<div class="m2"><p>کز سر برون شدست مرا خوی خوشدلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بس بلا و غصّه که بر یکدگر نشست</p></div>
<div class="m2"><p>در دل نماند جای تکاپوی خوشدلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیمرغ خوشدلی پس قاف عدم گریخت</p></div>
<div class="m2"><p>جز نقش نیست صورت نیکوی خوشدلی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>الّا اگر چو خوشدلی اندر عدم شود</p></div>
<div class="m2"><p>ورنه، نبیند این دل من روی خوشدلی</p></div></div>