---
title: >-
    شمارهٔ ۶۹ - ایضاً له
---
# شمارهٔ ۶۹ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>خورشید چرخ شرع که نور چراغ فضل</p></div>
<div class="m2"><p>الّا ز شمع خاطر تو مقتبس نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چشم همّت تو بمیزان اعتبار</p></div>
<div class="m2"><p>گوی زمین موازن پرّ مگس نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردی ز خاک مرکب تو باد نداشت</p></div>
<div class="m2"><p>کورا دو اسبه مردم چشمم ز پس نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان در تنم که دست نشان هوای تست</p></div>
<div class="m2"><p>بی آرزوی خدمت تو یک نفس نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمم بآب چشم تیمّم از آن کند</p></div>
<div class="m2"><p>کاینجا بخاک پای توش دسترس نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفتست التماس حضورم ز خدمتت</p></div>
<div class="m2"><p>والحق مرا زبخت جز این ملتمس نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ و ستاره در هوش خدمت تواند</p></div>
<div class="m2"><p>برمن چه ظن بری که مرا این هوس نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان باز مانده ام که ز اسباب ره مرا</p></div>
<div class="m2"><p>جز نالۀ درای و فغان جرس نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخ سوی شاه شرع نهادم پیاده لیک</p></div>
<div class="m2"><p>در پای پیل ماندم از آن کم فرس نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من بنده را که ساکن خاک درت بدم</p></div>
<div class="m2"><p>آنگه که در دیار وفا هیچکش نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر باریم تغیّر رای تو در خورست؟</p></div>
<div class="m2"><p>حرمان دست بوس تو انصاف بس نبود؟</p></div></div>