---
title: >-
    شمارهٔ ۴۲ - ایضاً له یمدح الملک العادل اصفهبد المازندران
---
# شمارهٔ ۴۲ - ایضاً له یمدح الملک العادل اصفهبد المازندران

<div class="b" id="bn1"><div class="m1"><p>سپاهان رابهریک چنددولتها جوان گردد</p></div>
<div class="m2"><p>هوایش عنبر افشاند،زمینش گلستان گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زخارستان اندوهش گل عشرت ببار آید</p></div>
<div class="m2"><p>درودیوارش از شادی بهشت جاودان گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هواهایی ز دلگیری چورای دشمنان تیره</p></div>
<div class="m2"><p>زناگاهان خوش ودلکش چوروی دوستان گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روان رفته بازآید،زبان بسته بگشاید</p></div>
<div class="m2"><p>همه دلها بیاساید،همه جان شادمان گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگویم کزچه می خیزدسپاهان راچنین دولت</p></div>
<div class="m2"><p>ازآن کآرامگاه تخت شاه کامران گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملک اصفهبدعادل،حسن،کآنجا که روی آرد</p></div>
<div class="m2"><p>سعادت بارکاب او،دواسبه هم عنان گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه بیند باهزاران دیده درعالم نظیراو</p></div>
<div class="m2"><p>سپهر سرزده هرچند درگرد جهان گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عالم بهرآن آمد فلک برسرکه پیوسته</p></div>
<div class="m2"><p>همی درگاه خسرورابگردآستان گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چواندر دست شه پیداشودگرز گران سنگش</p></div>
<div class="m2"><p>کشف کردارخصمش راسراندر تن نهان گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چوحزم اودرنگ آرد،فلکها راشودلنگر</p></div>
<div class="m2"><p>چوعزم اوشتاب آردزمین رابادبان گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان بخشی که هرساعت هزاران مفلس ازجودش</p></div>
<div class="m2"><p>خداوندزرودینارواسب وخان ومان گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی آیدبدریوزه سوی دست گهربارش</p></div>
<div class="m2"><p>سحاب ازبهرآن همواره درمازندران گردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خداوندا توآن شاهی که هرچ اندرضمیرآری</p></div>
<div class="m2"><p>ستاره همچنان آرد،زمانه همچنان گردد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز دزد حادثات ایمن نخسبد یک زمان گیتی</p></div>
<div class="m2"><p>اگرنه تیغ هندویت جهان راپاسبان گردد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیاردگشت بادمرگ گرد شاخ عمرما</p></div>
<div class="m2"><p>نسیم لطف تومارااگرپیوندجان گردد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چوقهرت تاختن آرد،فلک چون خاک ره گردد</p></div>
<div class="m2"><p>چودست توگهربارد،زمین چون آسمان گردد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چودست توگهربارد،زمین چون آسمان گردد</p></div>
<div class="m2"><p>لب معنی شودخندان،چولفظت درفشان گردد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زبان تیغ تودررزم چون درگفتگو آید</p></div>
<div class="m2"><p>همه رازدل بدخواه برصحراعیان گردد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه کارش زدولت راست چون تبرآیدآنکس کو</p></div>
<div class="m2"><p>برای خدمت خسروبقامت چون کمان گردد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بداندیشت زدم سردی خزان راماندوهردم</p></div>
<div class="m2"><p>زبادسردخودرویش،چوبرگ اندرخزان گردد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چوتیغت درمیان آید،سپاه خصم بفزاید</p></div>
<div class="m2"><p>چرا؟زیرا که هرشخصی دوپاره درزمان گردد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دل ودست تراهرگه که یادآردبداندیشت</p></div>
<div class="m2"><p>دوچشمش طیرۀ دریاورویش رشک کان گردد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سوی آبشخورآردگرک میش لنگ رابرسفت</p></div>
<div class="m2"><p>اگراضدادعالم رانهیب توشبان گردد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خیال خنجرت راسرو اگردرآب بنماید</p></div>
<div class="m2"><p>ازوبرخودچنان پیچدکه همچون خیزران گردد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چودرتاریکی گرد وغاگردد اجل گمره</p></div>
<div class="m2"><p>سوی جان بداندیشت چراغ اوسنان گردد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بجای دم زکام پردلان آتش دمد بیرون</p></div>
<div class="m2"><p>بجای خوی زاندام دلیران خون روان گردد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لباس عافیت راتیغ چون گل چاک گرداند</p></div>
<div class="m2"><p>زخون دشمنان،نیزه درخت ارغوان گردد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چوبیندخصم روی مرگ درآیینۀ تیغت</p></div>
<div class="m2"><p>خداداند که آن ساعت دل اوبرچه سان گردد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چوشانه پنجۀ قهرتوشان برهم زند،ورچه</p></div>
<div class="m2"><p>سپاه خصم ازانبوهی چوموی دیلمان گردد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بنامیزد!بنامیزد!خنک آن پادشاهی را</p></div>
<div class="m2"><p>که اوراچون توشه زاده،پناه خاندان گردد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگرچه مملکت شدپیر بردرگاه اجدادت</p></div>
<div class="m2"><p>باقبال توهرساعت،چوبخت توجوان گردد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خداوندا ز مدح توزبان بنده عجزآرد</p></div>
<div class="m2"><p>وگرچون سوسن آزاد،سرتاسرزبان گردد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه رگهای من شدراست درآهنگ مدح تو</p></div>
<div class="m2"><p>که انعام توام هرلحظه مغزاستخوان گردد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مراواجب بودازجان،دعای دولتت گفتن</p></div>
<div class="m2"><p>بشکرمنعم اولیتر،زبان کاندردهان گردد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چوآب زندگی خوردست تیغ شه خضرآسا</p></div>
<div class="m2"><p>عجب نبود که سرسبزی اوهم جاودان گردد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تمتّع بادت ازاقبال وبرخورداری ازدولت</p></div>
<div class="m2"><p>همی تامرغ زرّین اندرین سبزآشیان گردد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زحق اومیدمیدارم که:هر چ اومیدمیداری</p></div>
<div class="m2"><p>زاسباب جهانداری،همه بهترازآن گردد</p></div></div>