---
title: >-
    شمارهٔ ۴۳ - وله ایضاً فیه
---
# شمارهٔ ۴۳ - وله ایضاً فیه

<div class="b" id="bn1"><div class="m1"><p>فلک جنابا در آرزوی حضرت تو</p></div>
<div class="m2"><p>بسی بگشت بسرآسمان عالم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنایت از قلم تست مرغک دانا</p></div>
<div class="m2"><p>عبارت از سخن تست گنج بادآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تویی که گر نبود سایۀ تو یک ذرّه</p></div>
<div class="m2"><p>سایه روی شود آفتاب سایه نورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهیب زخم تو دیدست خصم ازین قبلست</p></div>
<div class="m2"><p>که خانه خانه گریزان بود چو مهرۀ نرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لقای تو سبب امن و راحت خلقتست</p></div>
<div class="m2"><p>من این قضیّه بدانسته ام بعکس و بطرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آتش جگرست آب چشم دشمن تو</p></div>
<div class="m2"><p>چنانکه از دل گرمست صبح را دم سرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر بدو رسد الماس خاطر تیزت</p></div>
<div class="m2"><p>شود هراینه قسمت پذیر جوهر فرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کفایتت بسرکلک کارهایی کرد</p></div>
<div class="m2"><p>که تیغ رستم دستان نکرد روز نبرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا زمانه اگر پی کند بسان قلم</p></div>
<div class="m2"><p>بسر بخدمتت ارنیستم نباشم مرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وراز قبول تو باد عنایتی جهدم</p></div>
<div class="m2"><p>بخاک پای تو کز آسمان برآرم گرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو مر هم از تو بود درد پای کی دارد؟</p></div>
<div class="m2"><p>چو پرسش از تو بود غم کجا بود در خورد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندید روی بهی تا ندید روی ترا</p></div>
<div class="m2"><p>رهی که همچو بهمی بد ز درد با رخ زرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگرد پای رهی دست درد هم نرسد</p></div>
<div class="m2"><p>کنونکه پرسش تو سایه بر سرم گسترد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برفته بود سراپای من ز دست ولیک</p></div>
<div class="m2"><p>گشادگیّ دو دست تو پای بندم کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز دست پای تو درد آن قفای محکم یافت</p></div>
<div class="m2"><p>که پای بنده ز دست عنای او میخورد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ببرد درد سر خویش درد پای از من</p></div>
<div class="m2"><p>کنونکه عاطفتت پای در میان آورد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنانکه پای من از درد بر سر آمده بود</p></div>
<div class="m2"><p>بفّر دولتت از پای اندر آمد درد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نصیب خانۀ خصم تو باد بردابرد</p></div>
<div class="m2"><p>رسیل موکب جاه تو باد بردابرد</p></div></div>