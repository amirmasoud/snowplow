---
title: >-
    شمارهٔ ۱۶۸ - وله ایضاً فیه
---
# شمارهٔ ۱۶۸ - وله ایضاً فیه

<div class="b" id="bn1"><div class="m1"><p>ای ز بزرگی بدان مقام که قدرت</p></div>
<div class="m2"><p>بر سر گردون فراشتست و ساده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که تردّد کنند زی درت آنک</p></div>
<div class="m2"><p>بر فلک از کهکشان علامت جاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاجز تدبیر تست جنبش گردون</p></div>
<div class="m2"><p>ور چه بکار آورد فنون جلاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدمت تو کردنی چو طاعت ایزد</p></div>
<div class="m2"><p>مدحت تو گفتنی چو لفظ شهاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلوه گه خصم تو منصّۀ دارست</p></div>
<div class="m2"><p>گردن بندش کمند و تیغ قلاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیر فلک در هوای آتش طبیعت</p></div>
<div class="m2"><p>بر بفکندست همچو تیر کباده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش خشم تو چون زبانه برآرد</p></div>
<div class="m2"><p>شیر فلک برنهد بگاو لباده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تو سؤالیست بنده را بتفضّل</p></div>
<div class="m2"><p>زود جوابش ده از طریق افاده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بفضولی کسی ز خادم مخلص</p></div>
<div class="m2"><p>پرسد حالی چنان که باشد عاده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوید نان زیادت تو چه فرمود</p></div>
<div class="m2"><p>خواجه چو باز آمد از سفر بسعاده؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاید اگر گویمش که از پس شش ماه</p></div>
<div class="m2"><p>صرت کما کنت و العناء زیاده</p></div></div>