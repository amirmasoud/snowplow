---
title: >-
    شمارهٔ ۱۵۹ - وقال ایضاً یمدحه
---
# شمارهٔ ۱۵۹ - وقال ایضاً یمدحه

<div class="b" id="bn1"><div class="m1"><p>ای بهنگام شداید کرمت عدّت من</p></div>
<div class="m2"><p>وی بهر حال مربّی و ولی نعمت من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ زرّین بستانم زکف حاجب شمس</p></div>
<div class="m2"><p>شحنۀ هیبتت ار زانکه دهد رخصت من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوبهارست و نسیم و سحر و آب روان</p></div>
<div class="m2"><p>زان بود در خط و خلق و سخنت نزهت من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه در مدح تو محصور بود کام دلم</p></div>
<div class="m2"><p>همه بر یاد تو مقصور بود لذّت من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشکنم پنجۀ احداث چو پشت عدوت</p></div>
<div class="m2"><p>بازوی بخت تو گر هیچ دهد قوّت من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نو عروسان مدیحت بینی صف در صف</p></div>
<div class="m2"><p>گر تماشا کنی اندر تتق فکرت من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چاوش سطوتت از چند مرا دور کند</p></div>
<div class="m2"><p>صیت انعام تو هر لحظه کند دعوت من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مدّتی رفت که چون خاطرات آسوده بدست</p></div>
<div class="m2"><p>خاک درگاه تو از عارضۀ جبهت من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لطفت از روی تفقّد نه همانا گفتست</p></div>
<div class="m2"><p>که فلان کو؟ که نمی باشد در حضرت من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>او چرا نیست درین زمره چوارباب هنر ؟</p></div>
<div class="m2"><p>که همه بهره ورند از کرم و نعمت من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او گناهی نکند ور بمثل نیز کند</p></div>
<div class="m2"><p>کی دریغ آید از وعاطفت و رحمت من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکن ای خواجه و با عفو بکن مشورتی</p></div>
<div class="m2"><p>پس ازین چون شنوی از دگران تهمت من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که نباید که به لطفی که کم از هیچ نبود</p></div>
<div class="m2"><p>همه بر هیچ بود سابقۀ خدمت من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرخ را بر من بیچاره چنان چیره مکن</p></div>
<div class="m2"><p>که چو انعام تو از حد ببرد محنت من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چین ابروی تو دلگرمی چرخ ار ندهد</p></div>
<div class="m2"><p>زهره دارد که بر اندیشد از نکبت من ؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عجبست الحق از آن لطف هنر پرور تو</p></div>
<div class="m2"><p>که چنین سیر شد از خدمت بی علت من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طمعی نه که گران گردد ازآن سایۀ من</p></div>
<div class="m2"><p>کلفتی نی که تحمل نتوان زحمت من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>محض دل دوستی و مهر و هوا خواهی تست</p></div>
<div class="m2"><p>سخت با درگه تو سلسه علقت من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر بدی گفت مرا حاسد من نیک آنست</p></div>
<div class="m2"><p>که نکو داند آیین تو و عفّت من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاعری هستم قانع بسلامت مشغول</p></div>
<div class="m2"><p>که نیازرد ز من موردی در مدّت من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>احترام تو دهد خواجگی و رونق من</p></div>
<div class="m2"><p>التفات تو نهاد قاعدۀ حشمت من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه بجاه همه کس گردن من نرم شود</p></div>
<div class="m2"><p>نه بمال همه کس میل کند نهمت من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون تویی باید و هیهات! نیابم دگری</p></div>
<div class="m2"><p>که بخاک در او سر بنهد همّت من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون بود قصد رهی با دگری در خدمت</p></div>
<div class="m2"><p>چه اثر دارد و تا چند بود قدرت من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قطرۀ خوی نچکاند زرخ گلبرگی</p></div>
<div class="m2"><p>گر همه آتش سوزنده شود هیبت من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مویها بر تنم از سیخ شود چون گلبن</p></div>
<div class="m2"><p>چشم بر هم نزند نرگسی از شوکت من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جز به نیروی تو هرگز بنبرّد مویی</p></div>
<div class="m2"><p>ور همه استره گردد بمثل خلقت من</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این همه رفت چنان گیر که جرمی کردم</p></div>
<div class="m2"><p>عفو تو بیشترست آخر از زلّت من</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه فرشتست دعاگو، نه پیمبر، نه ولی</p></div>
<div class="m2"><p>از کجا آمد در خاطر تو عصمت من ؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من یکی آدمیم همچو دگر آدمیان</p></div>
<div class="m2"><p>نیک و بد هر دو سرشتست درین طینت من</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این یکی هست که اندر همه آفاق امروز</p></div>
<div class="m2"><p>دومی نیست مرا در نمط صنعت من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اینت چالاک حسودی که چنین چفته نهاد</p></div>
<div class="m2"><p>بعتاب تو و تهدید زر و خلعت من</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صاحبا! صدرا! هر چند که آمد کرمت</p></div>
<div class="m2"><p>سبب حرمت و جاه و مدد ثروت من</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اندرین حضرت از جملۀ خدمتکاران</p></div>
<div class="m2"><p>بیش باید که بود حقّ من و حرمت من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خدمت هر کس قایم بحیات آید و باز</p></div>
<div class="m2"><p>منقطع نیست بهر حال ز تو خدمت من</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من شوم معتکف خاک و در اقطار جهان</p></div>
<div class="m2"><p>می پرد مرغ ثنایت پیر مدحت من</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر چه این شعر گران سنگ چهل من بیشست</p></div>
<div class="m2"><p>هم سبک روح و لطیف آمد با نسبت من</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا جهانست درو حاکم و فرمانده باش</p></div>
<div class="m2"><p>تا بجاهت زفلک بر گذرد رتبت من</p></div></div>