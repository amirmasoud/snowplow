---
title: >-
    شمارهٔ ۱۶۱ - وقال ایضایمدحه
---
# شمارهٔ ۱۶۱ - وقال ایضایمدحه

<div class="b" id="bn1"><div class="m1"><p>گرفت پایۀ تخت خدایگان زمین</p></div>
<div class="m2"><p>قرارگاه همایون براوج علییّن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهانگشای جوانبخت اتابک عادل</p></div>
<div class="m2"><p>پناه سلغریان،شهریار روی زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مظفّرالدّین بوبکرسعدبن زنگی</p></div>
<div class="m2"><p>که روی ملک کیانست وپشت ملّت ودین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دور دولت ایّام تا که غایت وقت</p></div>
<div class="m2"><p>نبود مملکت آن طرف بدین آیین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه چنگ گرگ گراید همی بنای گلو</p></div>
<div class="m2"><p>نه میش لنگ هراسد همی زشیر عرین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان بیک ره میزان عدل شد طیّار</p></div>
<div class="m2"><p>که میل سوی کبوترنمی کندشاهین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنفخ صور مبادا مزلزل این دولت</p></div>
<div class="m2"><p>که نیک جای گرفتست درقرارمکین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهی زخنجرتیرتو ملک را آرام</p></div>
<div class="m2"><p>زهی بزیورعدل توشرع راتزیین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعاع رای تو گرسایه برچمن فکند</p></div>
<div class="m2"><p>درختها را نبودشکوفه جز پروین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبار لعل چوخاتم خمیده پشت شود</p></div>
<div class="m2"><p>چوبرق ازکف توزربرون جهدزکین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو نیزۀ تو میان گر بندد از سر دست</p></div>
<div class="m2"><p>بیک زمان بگشاید حصارهای حصین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپاه فقر کجا همچو ابر سایه فکند</p></div>
<div class="m2"><p>چو برق از کف نوزر برون جهد زکمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چوچشم ترک شودحالتنگ برمردم</p></div>
<div class="m2"><p>گهی که ابروی تودادعرض لشکرچین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهررگی زعدو ازتو میرسد زخمی</p></div>
<div class="m2"><p>چوچنگ ازان کند از سینه ناله های حزین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بآب تیغ توآیند تشنگان اجل</p></div>
<div class="m2"><p>درآن مقام که بالا گرفت آتش کین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زبس که تیغ ترا درلبست جان عدو</p></div>
<div class="m2"><p>بذوق خصم توشدتیغ رازبان شیرین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چوخامه هرکه زبان تر کند بمدحت شاه</p></div>
<div class="m2"><p>کنددهان چودهان دویت مشک آگین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زبخشش تو بجز باد نیست درکف بحر</p></div>
<div class="m2"><p>اگرچه داشت ازین پیش مایه درّثمین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زدست جود تواکنون بماند با لب خشک</p></div>
<div class="m2"><p>چوعاجزست ز دست توچون کندمسکین؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بتلخ وشور رسانیدکارخود بکنار</p></div>
<div class="m2"><p>بماند کان جگرخسته بادلی خونین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ببرده بودجگرگوشگانش راجودت</p></div>
<div class="m2"><p>بباد داد هرآن خرده یی که داشت دفین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بعهد جودتوکان کیست؟کنده یی زردوست</p></div>
<div class="m2"><p>زروی عجزشده زیرتیشۀ میتین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سخاوت توچه خواهدزجان سنگینش؟</p></div>
<div class="m2"><p>چه گردخیزدازاین خاک پای راه نشین؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چونیست برجگربحرآب،کم کن از آن</p></div>
<div class="m2"><p>چونیست دررگ کان خون تونیزبس کن ازین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهان پناها! آنی که کرد روح قدس</p></div>
<div class="m2"><p>زبان تیغ تراآیت ظفرتلقین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چوخامۀ توگهر زیر پای می سپرد</p></div>
<div class="m2"><p>بدستبوس خودآنراکه داده یی تمکین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شد از یسار نگین وارغرق در زر و سیم</p></div>
<div class="m2"><p>زنیک بختی هرکوتراست ملک یمین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگرنه خنجرتوعدل را دهد یاری</p></div>
<div class="m2"><p>وگرنه هیبت توفتنه راکند تسکین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کلاه ازسرهدهدبغصب بربایند</p></div>
<div class="m2"><p>برون کنندبغارت زپای بط نعلین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صدای نوبت عدلت باصفهان برسید</p></div>
<div class="m2"><p>چوطاس چرخ زآوای اوگرفت طنین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عروس طبع مرا ازثنای فایح شاه</p></div>
<div class="m2"><p>همه زعنبرومشک است بستر و بالین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نمی دهد بطمع زحمت خزاین شاه</p></div>
<div class="m2"><p>وگرنه دور نبودی توقّع کابین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا حقوق دعاگویی است بردولت</p></div>
<div class="m2"><p>همه اکابر این دولت آگهند و یقین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ستایش توکه درنظم بنده می آید</p></div>
<div class="m2"><p>هم ازتمامت اقبال ودولت خودبین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مسامع همه شاهان به آرزوخواهند</p></div>
<div class="m2"><p>که از زبان دعاگو شوند گوهر چین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بپای مردی عفوت بضاعتی مزجاة</p></div>
<div class="m2"><p>بدان جناب فرستاده است غثّ وثمین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بچشم گوشۀ لطف اربسوی آن نگری</p></div>
<div class="m2"><p>شونداهل معانی بمنّت تو رهین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دوبنده رابدرشاه رهنمون شده ام</p></div>
<div class="m2"><p>یکی زماء مهین ویکی زماء معین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یکی بمعنی پاک ازعطای روح قدس</p></div>
<div class="m2"><p>یکی بصورت خوب ازنژادحورالعین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یکی بزلف وخط آشوب وفتنۀ دلها</p></div>
<div class="m2"><p>یکی بچهرۀ زیبا،نگارخانۀ چین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یکی زبهرتمنّای گوش معنی جوی</p></div>
<div class="m2"><p>یکی زبهرتماشای چشم صورت بین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی گشاده میانست لیک بس دلبند</p></div>
<div class="m2"><p>یکی ببسته میان لیک بس گشاده جبین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی سپیدولیکن چوچشم ودل روشن</p></div>
<div class="m2"><p>یکی سیاه ولیکن چوعقل وجان شیرین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زبهرخدمت خاص این سپید را بپذیر</p></div>
<div class="m2"><p>برای راحت عام آن سیاه رابگزین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که تانیابت این دل شکسته میدارند</p></div>
<div class="m2"><p>بقدر وسع برآن آستان همان وهمین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اگرقبولی یابند از نوازش شاه</p></div>
<div class="m2"><p>بدیع نیست ازآن خانه لطفهای چنین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مرا گوارش احسان گرم ده چو دهی</p></div>
<div class="m2"><p>که ممتلی شده ام از بوارد تحسین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو هرکجا که زبان آوریست شمع صفت</p></div>
<div class="m2"><p>زکنه مدح توشدبالگن زعجز قرین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مراصواب نباشدبجزدعاگفتن</p></div>
<div class="m2"><p>علی الخصوص که روح الامین کندآمین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هزارسال زیادت ازآنچه معهودست</p></div>
<div class="m2"><p>بکامرانی برتخت مملکت بنشین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شاه زاده قرنتاش باش پشت قوی</p></div>
<div class="m2"><p>فلک مطیع شما و خدای یار و معین</p></div></div>