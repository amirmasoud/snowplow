---
title: >-
    شمارهٔ ۱۲۵ - وله ایضا یمدحه و یهنّیه بالزّفاف
---
# شمارهٔ ۱۲۵ - وله ایضا یمدحه و یهنّیه بالزّفاف

<div class="b" id="bn1"><div class="m1"><p>چو خیل زنگ بیار استند صفّ جدال</p></div>
<div class="m2"><p>سپاه روم هزیمت گرفت هم در حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک کلاه زر اندود برگرفت از سر</p></div>
<div class="m2"><p>جهان بسفت درافکند عنبرین سربال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگاه کردم و دیدم عروس گردون را</p></div>
<div class="m2"><p>شده چمان و خرامان بعزم استقبال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرو گذاشته بر عارض منوّر روز</p></div>
<div class="m2"><p>ذوابۀ شب تار از برای زیب و جمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فروغ داده بگلگونۀ شفق رخسار</p></div>
<div class="m2"><p>خضاب کرده کف دست را عروس مثال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بفرق سر بر تاجی نهاده از اکلیل</p></div>
<div class="m2"><p>بساق پای وی اندر زماه نو خلخال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و شاح عقد ثریّا فکنده در گردون</p></div>
<div class="m2"><p>نطاق بسته میان را ز عقدهای لآل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی دوید ز پیش آفتاب مشعله دار</p></div>
<div class="m2"><p>همی چمید ز پس عود سوز باد شمال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سماک رامح میرفت دور باش بکف</p></div>
<div class="m2"><p>شهاب ثاقب میزد میان راه دوال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنغمه زهره از پردۀ سپاهان کرد</p></div>
<div class="m2"><p>روایت غزلی مطلعش برین منوال</p></div></div>
<div class="b2" id="bn11"><p>زهی مبارک طالع خهی همایون فال</p>
<p>که روزنامۀ سعدست و منشاء اقبال</p></div>
<div class="b" id="bn12"><div class="m1"><p>شبی که منزل شادی دروست میلامیل</p></div>
<div class="m2"><p>شبی که جام سعادت در اوست مالامال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شبی که هست ملاقات عقل و روح در او</p></div>
<div class="m2"><p>شبی که زهره و خورشید را دروست وصال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بخور جانرا بر مجمر سرور بسوز</p></div>
<div class="m2"><p>بسان شکّر و عود آمده صواب و محال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو حال چرخم از ینسان مشاهدت افتاد</p></div>
<div class="m2"><p>بنزد عقل شدم بهر کشف این احوال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو باز راندم این ماجرا بنزد خرد</p></div>
<div class="m2"><p>جواب داد مرا گفت : نیست جای سؤال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>معاینه ست شب قدر عقلی و شرعی</p></div>
<div class="m2"><p>بخواه حاجت و زین پس ز دور چرخ منال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بزرگ عیدی سایه فکنددر رمضان</p></div>
<div class="m2"><p>که پیروی کندش عید غرۀ شوّال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شب است زنگی آبستن سرور و فرح</p></div>
<div class="m2"><p>نشسته بهر ولادت برین شکسته سفال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شب زفاف امام زمانه خواجۀ ماست</p></div>
<div class="m2"><p>که بهر خدست اوخم گرفت پشت هلال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زحل زگلشن نیلوفری فرود آید</p></div>
<div class="m2"><p>محفّه داری او را گرش دهند مثال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برای عزّت خود خواست آفتاب بسی</p></div>
<div class="m2"><p>که از خضاب کسوفش دهند پیک خال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدان امید که مشّاطگی کندمه چرخ</p></div>
<div class="m2"><p>بگونۀ گل گلگونه داد چندین سال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز اجتماع سلیمان شرع با بلقیس</p></div>
<div class="m2"><p>رواق صرح ممّرد شدست صفّ نعال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زمانه یابد ازین اتّصال خوب محل</p></div>
<div class="m2"><p>ستاره گیرد ازین اقتران میمون فال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو شد مصوّرم این حال بهر تهنیتش</p></div>
<div class="m2"><p>نبد گزیرم ازین چندبیت و صف الحال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کشیدم از سر اندیشه پای در دامن</p></div>
<div class="m2"><p>بمانده عاجز و حیران بدست خواب و ملال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بفرّ خواجه از املای طبع هم برفور</p></div>
<div class="m2"><p>بدیهه نظمی پرداختم چو آب زلال</p></div></div>
<div class="b2" id="bn29"><p>زهی سخای تو بر آز تنگ کرده مجال</p>
<p>زهی عطای تو بر ما فراخ کرده منال</p></div>
<div class="b" id="bn30"><div class="m1"><p>پناه سروری و پشت شرع ، رکن الدّین</p></div>
<div class="m2"><p>که هست کلک و بنانمت بیان سحر حلال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تویی که نام تو نقشست بر نگین خرد</p></div>
<div class="m2"><p>تویی که رای تو قطب است و سپهر جلال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>معالی تو برون از تصرّف اوهام</p></div>
<div class="m2"><p>مکارم تو فزون از توقّع آمال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نسیم لطف تو گر بر جهان دمد نفسی</p></div>
<div class="m2"><p>شوند قابل جانها هیاکل تمثال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سموم قهر تو حاشا اگر زبانه زند</p></div>
<div class="m2"><p>شوند نطفه دگرباره در رحم اطفال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زفیض طبع تو کردست بحر استمداد</p></div>
<div class="m2"><p>و گرنه جود تواش کرده بود استیصال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دبیر چرخ ز بدو وجود بنوشتست</p></div>
<div class="m2"><p>حسود جاه ترا حرز: ماله من وال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فلک پیاده شتابد بخوابگاه عدم</p></div>
<div class="m2"><p>اگر دهند ز دیوان هیبت تو مثال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شود ستاره بپهلو سوی درت غلطان</p></div>
<div class="m2"><p>گرش کنند ز درگاهت امرت استعجال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو شاخ صدره زجیب سپهر سربزند</p></div>
<div class="m2"><p>اگر بنام تو اندر زمین نهند نهال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کشد چو آب گریبان نامیه در خاک</p></div>
<div class="m2"><p>اگر تو گویی شاخ درخت را که مبال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به ابر کردم تشبیه دست در بارت</p></div>
<div class="m2"><p>خرد نفیر برآورد و گفت: به بسگال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کجا برابر دریای درفشان باشد ؟</p></div>
<div class="m2"><p>کسی که خیره همی بیزد آب در غربال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زهی زمانه زبأس تو گشته مستشعر</p></div>
<div class="m2"><p>خهی سپهر زجاه تو کرده استکمال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فراغتست ترا از وجود هفت و چهار</p></div>
<div class="m2"><p>که هست ذات تو خود عالمی باستقلال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نشاند عدل تو ناهید شهره را بر گاو</p></div>
<div class="m2"><p>فکند سهم تو بر کوه علّت زلزال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یتیم ماند جگرگوشۀ صدف زسخات</p></div>
<div class="m2"><p>ذلیل گشت ز الفاظ تو سلالۀ نال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هم از مآثر عدل تو بینم این که همی</p></div>
<div class="m2"><p>برآید از دل شیر سپهر قرن غزال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدانک خصم تو روزی نشست بر منبر</p></div>
<div class="m2"><p>گمان برد که عدیل تو گشت، اینت محال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خرد گواه منست اندرین که چون عیسی</p></div>
<div class="m2"><p>نشد بواسطۀ خر بر آسمان دجّال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هوای عالم مدح تو چون کنم ؟ کآنجا</p></div>
<div class="m2"><p>همی بسوزد سیمرغ فکر را پروبال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همیشه تا که سویدا بود محلّ حیات</p></div>
<div class="m2"><p>همیشه تا که دماغست مستقّر خیال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مباد ماه جلال ترا افول و محاق</p></div>
<div class="m2"><p>مباد مهر بقای ترا کسوف و زوال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خجسته بادت این اتّصال تا جاوید</p></div>
<div class="m2"><p>بکام خویش ممتّع بدین ستوده همال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زبارگاه تو مصروف باد دست فنا</p></div>
<div class="m2"><p>ز روزگار تو مکفوف باد عین کمال</p></div></div>