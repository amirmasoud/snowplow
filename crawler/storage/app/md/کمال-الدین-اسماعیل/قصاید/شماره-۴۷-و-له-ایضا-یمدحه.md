---
title: >-
    شمارهٔ ۴۷ - و له ایضا یمدحه
---
# شمارهٔ ۴۷ - و له ایضا یمدحه

<div class="b" id="bn1"><div class="m1"><p>ای خسروی که آتش تیغ تو روز کن</p></div>
<div class="m2"><p>در قلب چرخ زهرۀ مرّیخ آب کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارای ملک، شاه مظّفر پناه دین</p></div>
<div class="m2"><p>کت چرخ نام خسرو مالک رقاب کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبعت بر شحه یی سر خورشید غوطه داد</p></div>
<div class="m2"><p>تیغت به لمعه یی دل آتش کباب کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مغز تیغ تو گهر ازبیم جود تو</p></div>
<div class="m2"><p>رخسار خود بخون حسودت خضاب کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جود تراکه هست حسابش برون ز عقل</p></div>
<div class="m2"><p>نتوان بعقد از سردستش حساب کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر ثبات خیمۀ ملک ترا فلک</p></div>
<div class="m2"><p>از نیزه و کمند تو میخ و طناب کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید زخم تیغ ترا دید در مصاف</p></div>
<div class="m2"><p>حالی ز گرد خیل تو بر رخ نقاب کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردون برسم خویش غرورش همی دهد</p></div>
<div class="m2"><p>گریک دو روز خصم ترا کامیاب کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی جای اعتماد بود خاصه روز باد</p></div>
<div class="m2"><p>آن گنبدی که بر سر آبش حباب کرد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز جرعه های ساغر لطف تو در چمن</p></div>
<div class="m2"><p>دست بهار نصفی گل پر شراب کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لطف نسیم طبع تو از سنگ لاله ساخت</p></div>
<div class="m2"><p>تفّ سموم قهر تو در یا سراب کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون زلف دلربایان خطّ مسلست</p></div>
<div class="m2"><p>در پای عقل سلسله مشک ناب کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اندیشه ی ثنای گلستان خلق تو</p></div>
<div class="m2"><p>اندر مسام طبع، عرق را گلاب کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنشینید بوی خلق تو مشک خطا، ز شرم</p></div>
<div class="m2"><p>برباد داد بوی خود، الحق صواب کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>الطاف ایزدیست معانیّ ذات تو</p></div>
<div class="m2"><p>آنرا بسعی خود نتوان اکتساب کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا صبح رستخیز بماند در آن جهان</p></div>
<div class="m2"><p>هر مغز را که شربت کینت خراب کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دایم برد ز چشمۀ خورشید آب روی</p></div>
<div class="m2"><p>هر کو بخاک در گه تو انتساب کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با این سکون امن که در عهد عدل تست</p></div>
<div class="m2"><p>زلف بتان عجب که ز باد اضطراب کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای آفتاب سایه شهی کز برای تو</p></div>
<div class="m2"><p>چرخ سبک عنان زمه نو رکاب کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از شوق خدمت تو ضمیرم شب دراز</p></div>
<div class="m2"><p>صدگونه عشقبازی با آن جناب کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بهر نثار خیل خیال تو دست شوق</p></div>
<div class="m2"><p>چشم مرا خزینۀ در خوشاب کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گردون مرا خطاب خداوند می کند</p></div>
<div class="m2"><p>زانگه که شاه بنده ی خویشم خطاب کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این تربیت که کرد مرا لطف شهریار</p></div>
<div class="m2"><p>باهیچ ذرّه حقّا گر آفتاب کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مداحی ترا بدعا خواستم همی</p></div>
<div class="m2"><p>آخر خدای دعوت من مستجاب کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همتای تو خیال بخوابم همی نمود</p></div>
<div class="m2"><p>چشم رهی ز غیرت آن ترک خواب کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جودت زما سؤال تقاضا همی کند</p></div>
<div class="m2"><p>آسان بود همی سؤال چنین را جواب کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تشریف شه نه پایه ی امثال ماست لیک</p></div>
<div class="m2"><p>لطف تو هرچه کرد ز بهر ثواب کرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درج ضمیر بنده ی پر از درّ مدح تست</p></div>
<div class="m2"><p>این چند دانه حالی از آن انتخاب کرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کلک مرا از عجز سخن در زبان نماند</p></div>
<div class="m2"><p>زیرا که مسرع تو فراوان شتاب کرد</p></div></div>