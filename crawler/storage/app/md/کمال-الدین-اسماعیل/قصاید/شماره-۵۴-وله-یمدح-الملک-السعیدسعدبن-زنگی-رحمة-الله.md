---
title: >-
    شمارهٔ ۵۴ - وله یمدح الملک السّعیدسعدبن زنگی رحمة الله
---
# شمارهٔ ۵۴ - وله یمدح الملک السّعیدسعدبن زنگی رحمة الله

<div class="b" id="bn1"><div class="m1"><p>تادلم درخم آن زلف پریشان باشد</p></div>
<div class="m2"><p>چه عجب کارمن اربی سروسامان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدرآن زلف پریشان تومن دانم وبس</p></div>
<div class="m2"><p>کین کسی داندکونیز ریشان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لعل توچون سردندان کندازخنده سپید</p></div>
<div class="m2"><p>گوهرش حلقه بگوش ازبن دندان باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جزکه برخوان نکویی تود رروی زمین</p></div>
<div class="m2"><p>من ندیدم شکرستان که نمکدان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقیّ من بیدل عجبست ارنه تورا</p></div>
<div class="m2"><p>باچنان زلف ورخی دلبری آسان باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبزۀ خطّ توچون تازه وتر بر ناید؟</p></div>
<div class="m2"><p>تاکه آبشخورش ازچاه زنخدان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف تونامۀ خوبی چو مسلسل بنوشت</p></div>
<div class="m2"><p>زیبد ار برسرش ازخطّ توعنوان باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با تو ما را چه عجب گرسخن اندرجانست</p></div>
<div class="m2"><p>تا بود درلب شیرین تودرجان باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گربخندم تومپندارکه خوش دل شده ام</p></div>
<div class="m2"><p>غنچه راخنده همه ازدل ویران باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل شکسته ست هرآن پسته که لب بگشادست</p></div>
<div class="m2"><p>سرگرفتست هرآن شمع که خندان باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم خون ریزمرا گرنکنی عیب،سزد</p></div>
<div class="m2"><p>تاترا غمزۀ خون ریز بر آن سان باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اشک یاقوتی عاشق راطعنه نزند</p></div>
<div class="m2"><p>هرکه او را لب چون لعل بدخشان باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه همه کس راچوگان زسرزلف بود</p></div>
<div class="m2"><p>کس بودنیزکش ازقامت چوگان باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مشکل آنست که مارارخ وقدّت هوسست</p></div>
<div class="m2"><p>ورنه خودسرووگل اندرهمه بستان باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عاشقان رازگل وسروچه حاصل جزآنک</p></div>
<div class="m2"><p>یادگاری زرخ وقامت جانان باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تاکی ای دل زبرای لب شیرین پسران</p></div>
<div class="m2"><p>دل مجروح تودرسینه بزندان باشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برو و خاک سم اسب اتابک بکف آر</p></div>
<div class="m2"><p>که ترا آن بدل چشمۀ حیوان باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خسرو روی زمین شاه مظفّر که برزم</p></div>
<div class="m2"><p>گذر نیزۀ او بردل سندان باشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سعدبن زنگی شاهی که فرود حق اوست</p></div>
<div class="m2"><p>سعد اکبر اگرش نایب دربان باشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چشم خورشیداگرچنددقایق بینست</p></div>
<div class="m2"><p>هم ز ادراک کمالاتش حیران باشد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تامگردردل وچشم عدوش جای کند</p></div>
<div class="m2"><p>غنچۀ گل همه برصورت پیکان باشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دست خنجرچوکندزاستی حرب برون</p></div>
<div class="m2"><p>تابدامن زرۀ خصم گریبان باشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای خداوندی کز فضلۀ جودکف تست</p></div>
<div class="m2"><p>هرچه در بحر پدید آید و درکان باشد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زیردستیست ترا خنجر هندو کورا</p></div>
<div class="m2"><p>جاودان برسراعدای توفرمان باشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرچورمح توبزددشمن توسربفلک</p></div>
<div class="m2"><p>استخوانهاش هم ازبیم تولرزان باشد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرزت انصاف گرانی همی ازحدّببرد</p></div>
<div class="m2"><p>دایم اعدای تراکوفتگی زان باشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زانکه دربحرکف تست شناور پیوست</p></div>
<div class="m2"><p>خنجر تو تر و لرزنده و عریان باشد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حجّت قاطع بازوی توشمشیربس است</p></div>
<div class="m2"><p>درجهان گیری اگرکار ببرهان باشد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مهره ساییست سر گرز توکو را پیوست</p></div>
<div class="m2"><p>زبرگردن اعدای تو دکّان باشد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گندناییست حسام تووخصم ارچه دهد</p></div>
<div class="m2"><p>جان بیک دسته ازآن نیز گران جان باشد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دست بردوش فلک قدر تو دی می آمد</p></div>
<div class="m2"><p>این چه لطفست فلک نیز از آنان باشد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سبزۀ تیغ توچون خوان فنا آراید</p></div>
<div class="m2"><p>جگردشمن توسوختۀ خوان باشد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عاریت خواهدازدشمن توکاسۀ سر</p></div>
<div class="m2"><p>چون اجل راسرشمشیرتومهمان باشد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ازتوملک یدوزحاسد دولت رقبه است</p></div>
<div class="m2"><p>هرکجادعوی باتیغ سرافشان باشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اندرآن روزکه ازگرد وغاچشمۀ روز</p></div>
<div class="m2"><p>همچوجان ملک اندرتن شیطان باشد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نیزه سرتیزشود،تیغ بلرزد برخود</p></div>
<div class="m2"><p>تیردرتاب فتد،کوس درافغان باشد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شیهۀ ابرش تو درخم گردون پیچد</p></div>
<div class="m2"><p>مجری ناوک تودیدۀ کیوان باشد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خنجر شاه چوخورشیدکه برسمت آید</p></div>
<div class="m2"><p>سپرخصم چومه درشب نقصان باشد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>روز بازار فنا گرم شود و ندر وی</p></div>
<div class="m2"><p>تیغ دلّال بود،نرخ سرارزان باشد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سنگ حلم تواگرنایدش اندردندان</p></div>
<div class="m2"><p>خاک رادرحرکت سبحۀ گردان باشد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شادباش ای شه پردل که نداردپایت</p></div>
<div class="m2"><p>دشمن ارخودبمثل رستم دستان باشد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خنجرتیز زبانت چو درآید بسخن</p></div>
<div class="m2"><p>کلماتش همه برصفحۀ ابدان باشد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اندرآن لحظه زبیم تو چو کرم پیله</p></div>
<div class="m2"><p>کفن خصم گژاکندش وخفتان باشد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زهرۀ ابرزبیم کف توآب شدست</p></div>
<div class="m2"><p>گه گهی چون بچکد،قطرۀ باران باشد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خاک برداشتی ازکان وتهی شدکف بحر</p></div>
<div class="m2"><p>وانگهی جود ترا خود چه غم آن باشد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نیست پایان سخای توودرزیرفلک</p></div>
<div class="m2"><p>همه چیزی را جز عمر توپایان باشد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جمع مالست غرض این دگرانرا ازملک</p></div>
<div class="m2"><p>تویی آن شه که زملکت غرض احسان باشد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هردرم دارکه او را نبود همّت وجود</p></div>
<div class="m2"><p>اوخداوند درم نیست،نگهبان باشد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مردمی ومردمی ودانش واحسان وکرم</p></div>
<div class="m2"><p>وانچ ازین معنی آیین بزرگان باشد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در نهاد تو بحمدالله ازینها هریک</p></div>
<div class="m2"><p>بیش ازآنست که در حیّز امکان باشد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فرض عین است تراطاعت وخدمتکاری</p></div>
<div class="m2"><p>وین بود معتقد هرکه مسلمان باشد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هرکه درخدمت درگاه تو تفصیرکند</p></div>
<div class="m2"><p>ای بسا روز که ازکرده پشیمان باشد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>وارث تخت سلیمان چوتوشاهی زیبد</p></div>
<div class="m2"><p>کآصفی ازجهتش حاکم دیوان باشد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هست پیدا که زدستورگرانمایۀ تو</p></div>
<div class="m2"><p>زآنچه درپردۀ غیبست چه پنهان باشد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عنده علم بباید صفت آصف را</p></div>
<div class="m2"><p>آصفی چون کند آن خواجه که نادان باشد؟</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بنده راشاها عمریست که تا این سوداست</p></div>
<div class="m2"><p>که درآن حضرت یک روزثنا خوان باشد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هم شوم روزی برخاک جنابت چاکر</p></div>
<div class="m2"><p>دردحرمانش اگرقابل درمان باشد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چون همه خلق دعاگوی توشدپس چه زیان</p></div>
<div class="m2"><p>که ترا مادحی ازخاک سپاهان باشد؟</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>لابدش مور چو سیمرغ بباید پرورد</p></div>
<div class="m2"><p>هرکه در پادشهی تلو سلیمان باشد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا چوخورشیدفلک مایدۀ نوردهد</p></div>
<div class="m2"><p>دورونزدیک جهانش همه یکسان باشد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سایه ات بادا پاینده ودرعالم کیست</p></div>
<div class="m2"><p>آنکه پاینده تر از سایۀ یزدان باشد؟</p></div></div>