---
title: >-
    شمارهٔ ۱۷۴ - و قال ایضا یمدحه
---
# شمارهٔ ۱۷۴ - و قال ایضا یمدحه

<div class="b" id="bn1"><div class="m1"><p>ای که در شیوۀ گوهر باری</p></div>
<div class="m2"><p>ابر خواهد ز بنانت یاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قفس کرد سر خامۀ تو</p></div>
<div class="m2"><p>طوطیانرا بشکر گفتاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چه خلقست بدین زیبایی؟</p></div>
<div class="m2"><p>وین چه لطفست بدین بسیاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلم تو که کلید کرمست</p></div>
<div class="m2"><p>بر در بخل کند مسماری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا خلق تو مجمر سوزد</p></div>
<div class="m2"><p>نکند باد صبا عطّاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون کند هیبت تو دندان تیز</p></div>
<div class="m2"><p>نبود معدۀ دوزخ ناری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیستی خفته ز کار فضلا</p></div>
<div class="m2"><p>چشم بد دور ازین بیداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که آمد بحسابی در عقد</p></div>
<div class="m2"><p>تو زانگشت فرو نگذاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفست صحّت جان می بخشد</p></div>
<div class="m2"><p>گرچه چون باد صبا بیماری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور چه در تو ز تکسّر اثریست</p></div>
<div class="m2"><p>چون سر زلف بتان دلداری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کرم عام تو صدره کردست</p></div>
<div class="m2"><p>خاص احوال مرا غمخواری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد درستم که تویی چشم وجود</p></div>
<div class="m2"><p>که به بیماری مردم داری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگه تب که دگر بار مباد</p></div>
<div class="m2"><p>آن عرق نیست که می پنداری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرط جودست که چون ابر کند</p></div>
<div class="m2"><p>همه اندام تو گوهر باری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>علم الله که ز رنج تن تو</p></div>
<div class="m2"><p>شد جهان بر دل و چشمم تاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زود برخیز که می در نخورد</p></div>
<div class="m2"><p>بار تیمار مرا سر باری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیست ذات تو برنج ارزانی</p></div>
<div class="m2"><p>ای همه لطف و نکوکرداری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بتو یک ذرّه که خواهد آزار؟</p></div>
<div class="m2"><p>چون تو موری بستم نازاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ذات تو نسخت لطف ازلست</p></div>
<div class="m2"><p>این سخن را بهوس نشماری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حرف علّت اگرن کرد سقیم</p></div>
<div class="m2"><p>تا از آن هیچ بدل درتاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که قضا از پی تصحیح تو کرد</p></div>
<div class="m2"><p>قلم خود بسلامت جاری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای ترا فضل و هنر خاص الخاص</p></div>
<div class="m2"><p>وی ترا اهل هنر زنهاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اندرین عهد تن آسانی خلق</p></div>
<div class="m2"><p>کار من چیست بدین دشواری؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زانکه چون کوه فلک با من کرد</p></div>
<div class="m2"><p>سختی و تندی و ناهمواری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همچو لعلم جگری پر خونست</p></div>
<div class="m2"><p>عکسش اینک زرخم دیداری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بس که دیدم ز کریمان زفتی</p></div>
<div class="m2"><p>بس که بردم زعزیزان خواری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لاجرم می کشم از نومیدی</p></div>
<div class="m2"><p>بر سر فضل خط بیزاری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گشته بد خانۀ معنی ویران</p></div>
<div class="m2"><p>گر نکردی کرمت معماری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جانی از نو بتنم باز آورد</p></div>
<div class="m2"><p>لفظ عذب تو بشیرین کاری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کس خریدار نباشد ما را</p></div>
<div class="m2"><p>گرنه لطف تو کند سمساری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون تویی عاقلۀ اهل هنر</p></div>
<div class="m2"><p>با شدت خود غم من ناچاری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چشم دارم که از گوشه چشم</p></div>
<div class="m2"><p>بر معاشم نظری بگماری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حق گزاری ز که باشد طمعم ؟</p></div>
<div class="m2"><p>گر تو حقّ هنرم نگزاری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صد ازین عید بشادی گذران</p></div>
<div class="m2"><p>همه در نعمت و برخورداری</p></div></div>