---
title: >-
    شمارهٔ ۳۴ - و قال ایضاً یمدحه
---
# شمارهٔ ۳۴ - و قال ایضاً یمدحه

<div class="b" id="bn1"><div class="m1"><p>درین جناب همایون که تا قیامت باد</p></div>
<div class="m2"><p>بیمن معدلت صدر روزگار آباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجال لطف فراخست و من عجب دلتنگ</p></div>
<div class="m2"><p>نمود خواهم حالّی و هرچه باداباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزرگوارا ! قرب چهار ماه گذشت</p></div>
<div class="m2"><p>که بنده یک نفس از بنده غم نبود آزاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر هزار یکی از غمم نهی برکوه</p></div>
<div class="m2"><p>شود چو سوزن زردوز بیضۀ پولاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امید برتری از پایۀ خمولم نیست</p></div>
<div class="m2"><p>که نیک پست فکندست دولتم بنیاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآستان توام خرج شد خلاصۀ عمر</p></div>
<div class="m2"><p>که یک نفس زهنرهای خود نبودم شاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهال فضل و هنر را بآب دیده بسی</p></div>
<div class="m2"><p>بپروریدم و هرگز بریم باز نداد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی ستایم خود را بحضرت تو و لیک</p></div>
<div class="m2"><p>تو نیز نیک شناسی مرا ز روی نهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازین ستانه فراتر نبوده ام گاهی</p></div>
<div class="m2"><p>زعهد آنکه مرا مادر زمانه بزاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بجز بخدمت تو بنده انتما نکند</p></div>
<div class="m2"><p>هرآنکجا که پژوهش کنند زاصل و نژاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترازو آسا پیش خسان بیک جو زر</p></div>
<div class="m2"><p>نه بوسه داد زمین را و نه زبان بگشاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بزرگ و خرد بمن برنخاست هیچ حسود</p></div>
<div class="m2"><p>که روزگارش بر من مزیّتی ننهاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد از تعرّض آسیب روزگار ایمن</p></div>
<div class="m2"><p>ببارگاه تو هر مجرمی که ساخت ملاذ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زطالعست که من با برائت ساحت</p></div>
<div class="m2"><p>زکمترین کس دایم همی کشم بیداد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رفیع رای تو را با کمال حزم و ثبات</p></div>
<div class="m2"><p>چگونه کرد مزلزل عدو ؟ زهی استاد!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدان خدای که جلّاد قهر لم یزلش</p></div>
<div class="m2"><p>بخاک و خشت بدل کرد تاج و تخت قباد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بلطف او که جز از بهر رحمت عالم</p></div>
<div class="m2"><p>محمّد عربی را بخلق نفرستاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بخاک پای امینی که شرع و ملّت او</p></div>
<div class="m2"><p>شد از مکان تو آراسته بدانش و داد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که آنچه در حق من گفت مفسدی بغرض</p></div>
<div class="m2"><p>نه کردم و نه روا داشتم ، نه دارم یاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تبارک الله چندان سوابق خدمات</p></div>
<div class="m2"><p>شود بچربک و تضریب مفسدی بر باد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چرا ؟ چه بود ؟ چه کردم؟ زمن چه صادر شد؟</p></div>
<div class="m2"><p>که عفو تو نتوانست پیش آن استاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گناه گیر که کردم ، ببخش و باک مدار</p></div>
<div class="m2"><p>که هم بطبع کریمی و هم بهمّت راد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من آنگهی سپر از دشمنان بیندازم</p></div>
<div class="m2"><p>که تیغ حکم ترا کم شود مضا و نفاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا شماتت اعدا بلا همی دارد</p></div>
<div class="m2"><p>زیان مالی را ، دولت تو برجا داد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر نباشد دلگرمیی زعاطفتت</p></div>
<div class="m2"><p>تنی چگونه برآید بدشمنی هفتاد؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زخیط باطل اینها چه طرف بر بندم</p></div>
<div class="m2"><p>چو تو که سایه حقّی نمی رسی فریاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ترا سعادت باد و مرا شکیبایی</p></div>
<div class="m2"><p>که در زمانه ازین صعبتر بسی افتاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر نکو شودم کار از میامن تست</p></div>
<div class="m2"><p>و گرنه خسته دلانرا خدای صبر دهاد</p></div></div>