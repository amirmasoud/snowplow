---
title: >-
    شمارهٔ ۶ - و قال ایضاًَ یمدح الّصدر الّسعید رکن الدّین صامد و یصف الشّمس
---
# شمارهٔ ۶ - و قال ایضاًَ یمدح الّصدر الّسعید رکن الدّین صامد و یصف الشّمس

<div class="b" id="bn1"><div class="m1"><p>چیست این جرم منوّر سال و ماه اندر شتاب؟</p></div>
<div class="m2"><p>شهسواری پر دل پیروز جنگ کامیاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعلۀ او هر سحر جاروب صحن آسمان</p></div>
<div class="m2"><p>طلعت او چشمۀ انوار عالم را زهاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملکت او را ز حدّ نیمروز آید زوال</p></div>
<div class="m2"><p>دولت او را زخیل شام باشد انقلاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معدۀ مغرب ز قرص او خورد هر شام چاشت</p></div>
<div class="m2"><p>وز شفق گردون بتیغ او کند هر شب کباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاهی اندر دلو چون یوسف بود او را مقام</p></div>
<div class="m2"><p>گه ز بطن الحوت چون یونس بود او را مآب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه همی تابد به تشت آتشین صدر النّهار</p></div>
<div class="m2"><p>گه بزخم تیغ دارد عالمی را در عذاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز با تیغ آشکارا میکند قطع الّطریق</p></div>
<div class="m2"><p>شب چو دزد نقب زن زیر زمین اندر حجاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیکر او چون سپر لیک آن سپر شمشیر زن</p></div>
<div class="m2"><p>هیأت او چشمه یی و آن چشمه اندر التهاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زانک یک روز دست این چشم و چراغ روزگار</p></div>
<div class="m2"><p>از دهانش می رود چون شمع گه گاهی لعاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سر عالم همی لرزد ز مهر دل و لیک</p></div>
<div class="m2"><p>باوی از تیزی بخنجر باشدش دائم خطاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از تأمّل صورت او شاهد و شمع و لگن</p></div>
<div class="m2"><p>وز تخیّل پیکر او ساقی و جام و شراب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو زرّی سیم کش یا همچو نانی آبکش</p></div>
<div class="m2"><p>تازه روی و تیغ زن، آسوده اندر اضطراب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طرفه قرصی کو شود مهر دهان روزه دار</p></div>
<div class="m2"><p>بلعجب مهری که میسوزد جهانی را بتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میل زر بر تختۀ خاک از پی آن می زند</p></div>
<div class="m2"><p>تا که سال و ماه را روشن بود باری حساب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر بیاض صبح شکلش همچو زر در کاغذست</p></div>
<div class="m2"><p>در سواد شب شعاعش همچو تیغ اندر قراب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قرص صابونست پنداری وتشت و آب گرم</p></div>
<div class="m2"><p>تا بدان گردون فرو شوید ز زلف شب خضاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیست بر وی اعتماد بی ثباتی زانک او</p></div>
<div class="m2"><p>هر سر ماه آورد از ماه نو یا در رکاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سال و مه دامن بگستردست کوه ره نشین</p></div>
<div class="m2"><p>تا کند از فیض جودش خردۀ زر اکتساب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می کند در آمد و شد عمر ما را پای مال</p></div>
<div class="m2"><p>می رود از رفتن او زندگانی در شتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دیدۀ بی خواب دارد، پر ز میل آتشین</p></div>
<div class="m2"><p>وین عجب کز او دیده ها گردد پر آب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دشمن خوابست همچون بخت خواجه زان بتیغ</p></div>
<div class="m2"><p>خلق را بیرون کند هر بامداد از چشم خواب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تیغ ْ شاهان گر همی از خاک بردارند زر</p></div>
<div class="m2"><p>تیغ او بر خاک بازی می فشاند زرّ ناب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنکه بوسد بامدادان آستان خواجه کیست؟</p></div>
<div class="m2"><p>روشنست این: آفتابست، آفتابست، آفتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آستان رکن دین صاعد ، امام شرق و غرب</p></div>
<div class="m2"><p>سرور خورشید همکّت ، خواجۀ گردون جناب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آفتاب ار چه ز شوخی می رود در چشم شیر</p></div>
<div class="m2"><p>زرد و لرزان از نهیبش روی دارد در نقاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر مجاراتی کند با خاطر وفّاد او</p></div>
<div class="m2"><p>آفتاب گرم رو چون خر بماند در خلاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زهره دارد کاندر آید آفتاب از راه بام</p></div>
<div class="m2"><p>پاسبان قهرش ار با وی کند روزی خطاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آفتاب دولتش گر سایه بر آب افکند</p></div>
<div class="m2"><p>بر نیاید آبله اندامش از شکل حباب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آفتاب ورای او، در عقل گنجد این سخن؟</p></div>
<div class="m2"><p>یا کسی هرگز روا دارد ازین سان ارتکاب؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن نفس نگشاد هرگز جز که از راه خطا</p></div>
<div class="m2"><p>وین قدم ننهاد بیرون یکدم از صوب صواب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای سیاهّی دواتت چون سحر خورشید زای</p></div>
<div class="m2"><p>وی ایادیّ حسامت چون طمع مالک رقاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آفتاب از جام لطفت جرعه یی خوردست از آن</p></div>
<div class="m2"><p>بر در و دیوار می افتد، چو مستان خراب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ریسمان سازد همی تا بر تو بندد خویش را</p></div>
<div class="m2"><p>زان دهد همواره خیط الشّمس رغا در تاب ناب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گه بخاک اندر شدست از شرم رای تو چو میخ</p></div>
<div class="m2"><p>گه ز تاب هیبت نو تافته همچون طناب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر نه شاگردی دستت کرده بودی سال ها</p></div>
<div class="m2"><p>تیغ کوه از بخشش او کی شدی صاحب نصاب؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بادبان کشتی خور گر نه رایت بر کند</p></div>
<div class="m2"><p>رود نیل آسمان یکبارگی گردد سراب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ذرّه یی نقصان نیاید سایه را از آفتاب</p></div>
<div class="m2"><p>گر براند در جهان عدل تو رسم احتساب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر نخواهد رای تو هم در زمان زائل شود</p></div>
<div class="m2"><p>روزبانی ز آفتاب و شب روی از ماهتاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از دل و دست تو معمورست آفاق جهان</p></div>
<div class="m2"><p>کین درخشان چون خور آمد و آن در افشان چون سحاب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا ز خورشید ضمیرت در نگیرد مشعله</p></div>
<div class="m2"><p>کی شبیخون برد یارد بر سر دیوان شهاب؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خود گرفتم کآفتاب آفاق را در زر گرفت</p></div>
<div class="m2"><p>از زر او بر نشاید بست طرف از هیچ باب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جود، جود تست کز وی تا بروی و چشم خصم</p></div>
<div class="m2"><p>جمله زّر ناب بگرفتست و لؤلؤی خوشاب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سرفرازا! در ثنایت نظم شد شعری چنان</p></div>
<div class="m2"><p>کآفتابش چون عطارد ثبت کرد اندر کتاب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پشت گرمی ضمیرم ز آفتاب جاه تست</p></div>
<div class="m2"><p>ورنه طبع چون منی راکی بود این توش و تاب؟</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>حضرت خورشید شرعست ارنه دعوی کردمی</p></div>
<div class="m2"><p>شعر ازین دستست ، بسم الله، که میگوید جواب؟</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سایۀ اقبال تو پاینده می باید مدام</p></div>
<div class="m2"><p>گر نتابد آفتاب از چرخ، گو هر گز متاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بسکه بر جانت دعای خیر میگویند، خلق</p></div>
<div class="m2"><p>می بغلتد آفتاب اتدر دعای مستجاب</p></div></div>