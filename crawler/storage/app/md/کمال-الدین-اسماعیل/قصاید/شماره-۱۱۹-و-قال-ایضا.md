---
title: >-
    شمارهٔ ۱۱۹ - و قال ایضاً
---
# شمارهٔ ۱۱۹ - و قال ایضاً

<div class="b" id="bn1"><div class="m1"><p>چارند گواه خواجه اسحق</p></div>
<div class="m2"><p>هر چار بر خرد مصدّق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کانکس که بود برنگ خواجه</p></div>
<div class="m2"><p>مجبول بود ز شّر مطلق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آواز گران و روی فربه</p></div>
<div class="m2"><p>با سرخی موی و چشم ازرق</p></div></div>