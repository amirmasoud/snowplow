---
title: >-
    شمارهٔ ۱۷۸ - وله ایضا یمحدح
---
# شمارهٔ ۱۷۸ - وله ایضا یمحدح

<div class="b" id="bn1"><div class="m1"><p>ای دل چو نیست صبر ترا برقرار پای</p></div>
<div class="m2"><p>هان بر بساط عشق منه زینهار پای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهلست پایداری تو در مقام وصل</p></div>
<div class="m2"><p>چون دست برد هجر به بینی بدار پای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرگار وار سر مبر از دایره برون</p></div>
<div class="m2"><p>چون در میان نهادی پرگار وار پای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بر سر تو تیغ بود فی المثل چو کوه</p></div>
<div class="m2"><p>میدار سخت در غم آن غمگسار پای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرگار از آن بگرد سر خود همی دود</p></div>
<div class="m2"><p>کو مینهد بیکسو از پیش یار پای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دل که یافت در سر آن زلف مدخلی</p></div>
<div class="m2"><p>چون شانه بر تراشد از سر هزار پای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سروی بود که جای کند برکنار جوی</p></div>
<div class="m2"><p>گر بر نهد بدیدۀ من آن نگار پای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانا ز عشق قامت تست این که سرورا</p></div>
<div class="m2"><p>گیرد بناز دست چمن بر کنار پای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم تو ناتوان و چو یازد به تیغ دست</p></div>
<div class="m2"><p>با او کسی ندارد در این دیار پای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا همچو خط بچهرۀ تو سر برآورم</p></div>
<div class="m2"><p>از فرق سر کنم چو قلم آشکار پای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در خدمتت چو سرو بپای ایستم همه</p></div>
<div class="m2"><p>ور خود بسان گل بودم پر ز خار پای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باد صبا به پشتی گلزار روی تو</p></div>
<div class="m2"><p>اندر نهد سبک بسر لاله زار پای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بلقیس وار پای برهنه ست سرو را</p></div>
<div class="m2"><p>تا در نهد ز شرم تو در جویبار پای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درپای تافکنده یی آن زلف مشکبار</p></div>
<div class="m2"><p>بر میزنی ز ناز بمشک تتار پای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تشریف وصلت ار چه نه اندازۀ منست</p></div>
<div class="m2"><p>گه گاه رنجه کن بر من سوگوار پای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زیرا که گر چه جای گهر افسر سرست</p></div>
<div class="m2"><p>هم پی نصیب نیست بوقت نثار پای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر دست محنت تو گریبان بگیردم</p></div>
<div class="m2"><p>در دامن فراغ کشم مرد وار پای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نی نی سزای کفش چوپایست، آن سری</p></div>
<div class="m2"><p>کو باز گیرد از در صد رکبار پای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سلطان اهل فضل که خصمش همی نهد</p></div>
<div class="m2"><p>در دام حادثه ز سر اختیار پای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در روی رای او نکشد آفتاب تیغ</p></div>
<div class="m2"><p>در پیش حکم او ننهد روزگار پای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با حلم او نیارد کوه بلند سنگ</p></div>
<div class="m2"><p>با عزم او نداد باد بهار پای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اندیشه در عبارت خطّش چنان رود</p></div>
<div class="m2"><p>همچون کسی که بسته بود درنگار پای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای سروری که هر که زمین تو بوسه داد</p></div>
<div class="m2"><p>بر بام آسمان نهد از اقتدار پای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بی دستیاری قلم ناتوان تو</p></div>
<div class="m2"><p>چتر ملوک را نبود برقرار پای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون نرگسش ز دولت تو تاج برسرست</p></div>
<div class="m2"><p>آنرا که شد ز گرد درت خاکسار پای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خود را چو نعل بر رهت افکند ماه نو</p></div>
<div class="m2"><p>زان تا ببوسد اسب ترا برگذار پای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون سر ز جیب نطق بر آری تو، ناطقه</p></div>
<div class="m2"><p>در دامن سکوت کشد شرمسار پای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اطراف روم را بنگارد بنقش چین</p></div>
<div class="m2"><p>کلک تو چون برون نهد از زنگبار پای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر سر برآورد چو کدو با تو بدسگال</p></div>
<div class="m2"><p>تیغ قضا قلم کندش چون خیار پای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در وصف دست تو نتوان رفت سرسری</p></div>
<div class="m2"><p>خود چون نهند سرسری اندر بحار پای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون گل درد ز جود تو پیراهن حریر</p></div>
<div class="m2"><p>درپا چو سرو آنکه ندارد ازار پای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در گرد عزم تو نرسد برق گرم رو</p></div>
<div class="m2"><p>ور زاتشش بود بمثل چون شرار پای</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ابر از بحار دست تو مایه بکف کند</p></div>
<div class="m2"><p>آنگاه برنهد بسر کوهسار پای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>با تند باد قهر تو در عرصۀ وجود</p></div>
<div class="m2"><p>کوه بلند را نبود پایدار پای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دلگرمی پیادۀ شطرنج اگر دهی</p></div>
<div class="m2"><p>با آن پیاده نیز ندارد سوار پای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دشمن بدان هوس که گریزد سوی عدم</p></div>
<div class="m2"><p>هر شب چو شمع سازد درپا فزار پای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از بهر بخشش تو بیازید شاخ دست</p></div>
<div class="m2"><p>وز بهر حاسد تو فرو برد دار پای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خصم تو سر ندارد و دادی ز دست نیز</p></div>
<div class="m2"><p>گرمی نداشتی ز برای فرار پای</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خورشید همچو سایه نهد روی بر زمین</p></div>
<div class="m2"><p>تا بر ستانۀ تو نهد روز بار پای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در عطف دامن کرمت زد چو خاک دست</p></div>
<div class="m2"><p>در سنگ نیز آمدش از اعتقار پای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در عهد تو هرآنکه بر آرد چو سر و دست</p></div>
<div class="m2"><p>او را به تخته بند کنند استوار پای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دریا دلا! ز صدر تو محروم مانده ام</p></div>
<div class="m2"><p>زیرا که نیست عزم مرا دستیار پای</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پیریّ و ضعف بنیت و سرمای بس قوی</p></div>
<div class="m2"><p>نگذاشتند بر من مدحت نگار پای</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وقت قیام هست عصا دستگیر من</p></div>
<div class="m2"><p>بیچاره آنکه او کند از دستوار پای</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زین پیش اگر بهرزه دوی سر سبک بدم</p></div>
<div class="m2"><p>اکنون همی کشم ز سر اضطرار پای</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آنکو زند ز روی جفا پشت پای من</p></div>
<div class="m2"><p>بوسم چو دامنش بلب اعتذار پای</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر چون عنان فرو نگذاری مرا ز دست</p></div>
<div class="m2"><p>همچون رکاب بوسمت از افتخار پای</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ور دولتیم دست دهد همچو آستین</p></div>
<div class="m2"><p>چون دامنت رها نکنم از کنار پای</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از یمن همّت تو برآرم چو مور پر</p></div>
<div class="m2"><p>از فرط عجز اگر چه ندارم چو مار پای</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر چه بدست بوس تو یازد دهان من</p></div>
<div class="m2"><p>من اهل دستبوس نباشم بیار پای</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پای کرم ز کوی تفقّد مگیر باز</p></div>
<div class="m2"><p>نتوان گرفت بازخود از خاک خوار پای</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مستغنی است منصب تو از حضور ما</p></div>
<div class="m2"><p>طاوس را بجلوه نیاید بکار پای</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سرمای دی رسید کز آسیب صدمتش</p></div>
<div class="m2"><p>فارغ کند بر آتش سوزان گداز پای</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بگریزد از هوای خنک خوار خواردست</p></div>
<div class="m2"><p>خون گرید از جفای زمین زاز زار پای</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شد برگ و همچو چنگل بازست شاخ از آن</p></div>
<div class="m2"><p>کم می نهند مرغان بر شاخسار پای</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از پیر برف خرقه گرفتست از آن شدست</p></div>
<div class="m2"><p>پشمینه پوش و منزوی و برد بار پای</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بهمن روانه کرد بر اطراف خیل خویش</p></div>
<div class="m2"><p>زان بیم ز دامن او در حصار پای</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پشمینه پوش از پی آن گشت چون بهی</p></div>
<div class="m2"><p>کین باد سرد می بشکافد چو نار پای</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چون موی می شکافد پیکان ز مهربر</p></div>
<div class="m2"><p>چون سر سزد که موینه سازد شعار پای</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گردد چو روی توز کمان پشت پای آن</p></div>
<div class="m2"><p>کورا شود ز ناوک سرعا فکار پای</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چون کبک آنکه موزه ندارد هر آینه</p></div>
<div class="m2"><p>در پای میکشد چو کبوتر ازار پای</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هیزم صفت از آنکه مرا حسّ پای نیست</p></div>
<div class="m2"><p>در آتش تنور نهم خوار خوار پای</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از فتح باب ابر چنان شد گل زمین</p></div>
<div class="m2"><p>کاندر خلاب غرق شود تا زهار پای</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بر من بگرید ابر و بخندد بطنز برق</p></div>
<div class="m2"><p>چون در میان و حل نهم راهوار پای</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>آورد روزگارم در پای و پیش ازین</p></div>
<div class="m2"><p>با من نداشتی بگه کارزار پای</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کار سخن بیک ره در پای و پیش ازین</p></div>
<div class="m2"><p>کردم ردیف شعر بدین اعتبار پای</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بر روزگار دست فشانان همی روم</p></div>
<div class="m2"><p>با آنکه در گلست مرا چون چنار پای</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بی پای شعر بنده روان بود خود چو آب</p></div>
<div class="m2"><p>واکنون همی دود که شدش بی شمار پای</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کردم نثار پای تو این درّ شاهوار</p></div>
<div class="m2"><p>هان بر مزن بدین گهر شاهوار پای</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سر تا قدم در آتش فکرت بسوختم</p></div>
<div class="m2"><p>تا ماند همچو شمع ز من یادگار پای</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>عالم نماند تا بچنین شعر هر دمم</p></div>
<div class="m2"><p>بوسند زیر کان معانی گزار پای</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>در پیش تو به تیغ ببّرم سر زبان</p></div>
<div class="m2"><p>گر زانکه باز پس نهد از ذوالفقار پای</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بر موقف توقّع تشریف مولوی</p></div>
<div class="m2"><p>افگار شد امید مرا ز انتظار پای</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>خواهی که راست گردد پشت دوتای من</p></div>
<div class="m2"><p>یک دست خلعتم ده و یک سر چهار پای</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چون باد مرکبی بمن خاک پای بخش</p></div>
<div class="m2"><p>تا من بدو در آرم همچون غبار پای</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چون اشتران قافله در صحن بادیه</p></div>
<div class="m2"><p>هرگز کسی نداشت چنین برقطار پای</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ترسم که چون دراز شد این شعر هیچ کس</p></div>
<div class="m2"><p>در گوش خود رهش ندهد چون هزار پای</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>عمرت دراز باد و برین ختم شد سخن</p></div>
<div class="m2"><p>بیرون نمی نهم ز ره اختصار پای</p></div></div>