---
title: >-
    شمارهٔ ۱۵۳ - وله ایضاً
---
# شمارهٔ ۱۵۳ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>ای صبا، ای صبا، بحکم کرم</p></div>
<div class="m2"><p>بوی لطفی بمغز ما برسان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببزرگی مرا پیامی هست</p></div>
<div class="m2"><p>تو رسول منی، بیا برسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجناب بهاء ملّت و دین</p></div>
<div class="m2"><p>یا رب او را بکامها برسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و آنچه او را مرا دو مقصودست</p></div>
<div class="m2"><p>اندارنش بمنتها برسان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون رسی وقت فرصت خلوت</p></div>
<div class="m2"><p>مبلغی خدمت و دعا برسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز منش خاص بیش از اندازه</p></div>
<div class="m2"><p>خدمت و مدحت و ثنا برسان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گو فلان گفت بر توام رسمیست</p></div>
<div class="m2"><p>بکرم رسمک مرا برسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و آن دعایی که پارت آوردم</p></div>
<div class="m2"><p>اگرش وقت شد عطا برسان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور در اینش تعلّلی بینی</p></div>
<div class="m2"><p>این سخن هم بدین ادا برسان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مدحت رایگان حلالت باد</p></div>
<div class="m2"><p>عوض تحفه یا بها برسان</p></div></div>