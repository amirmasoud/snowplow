---
title: >-
    شمارهٔ ۱۳۳ - در مرثیهٔ پسر خود گوید
---
# شمارهٔ ۱۳۳ - در مرثیهٔ پسر خود گوید

<div class="b" id="bn1"><div class="m1"><p>نور دو دیدگان ز لقای تو داشتم</p></div>
<div class="m2"><p>یک سینه پر زمهر و هوای تو داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من جان و زندگی خودای جان و زندگی</p></div>
<div class="m2"><p>گر دوست داشتم ز برای تو داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر رنج و هر بلا که ز ایّام داشتم</p></div>
<div class="m2"><p>از بهر دفع رنج و بلای تو داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حقّا که گرچه خلق جهان عیب می کنند</p></div>
<div class="m2"><p>محراب روی خود کف پای تو داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا روز هر شبی بدو پا ایستاده من</p></div>
<div class="m2"><p>دو دست برخداز دعای تو داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه ز روزگار وفاکس ندیده بود</p></div>
<div class="m2"><p>از روزگار چشم وفای تو داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر بند شد دلم که کلید مرادها</p></div>
<div class="m2"><p>رخسار خوب طبع گشای تو داشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جای تو بی تو گردش گردون بمن نمود</p></div>
<div class="m2"><p>الحق نه این امید بجای تو داشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با این دل شکسته و این جان ناشکیب</p></div>
<div class="m2"><p>کی طاقت فراق لقای تو داشتم؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معذور دار، دست شریعت رها نکرد</p></div>
<div class="m2"><p>گر ماتم تو من نه سزای تو داشتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دردا و حسرتا که همه باد پاک بود</p></div>
<div class="m2"><p>امّیدها که من به لقای تو داشتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنگر چه سخت جانم و چون سنگدل که من</p></div>
<div class="m2"><p>دم میزنم هنوز و عزای تو داشتم</p></div></div>