---
title: >-
    شمارهٔ ۲ - و له یمدح المرلی رکن الدین مسعود حین انصرافه من خوارزم و یذکر ماجری
---
# شمارهٔ ۲ - و له یمدح المرلی رکن الدین مسعود حین انصرافه من خوارزم و یذکر ماجری

<div class="b" id="bn1"><div class="m1"><p>منم اینکه گشتست ناگه مرا</p></div>
<div class="m2"><p>دل و دامن از چنگ محنت رها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم اینکه از گردش روزگار</p></div>
<div class="m2"><p>شدست آرزوی جانم وفا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم اینکه در ظلمت جور و ظلم</p></div>
<div class="m2"><p>چو یونس شدم مستجاب الدّعا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم باز در پیش صدر جهان</p></div>
<div class="m2"><p>زبان برگشاده بشکر و ثنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی بینم اینو بچشم و هنوز</p></div>
<div class="m2"><p>نمی گردد از خویش باور مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابطحاء مکّة هدا الّذی</p></div>
<div class="m2"><p>اراه عیاناً و هذا انا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهی جیب تو مطلع صبح عدل</p></div>
<div class="m2"><p>زهی آستینت غلاف سخا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمهرت طر ازیده چهره صباح</p></div>
<div class="m2"><p>زقهرت بشولیده گیسو مسا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو رای تو تدبیر کلّی کند</p></div>
<div class="m2"><p>بود آفتاب و خط استوا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگوید ضمیر توالّا صواب</p></div>
<div class="m2"><p>نبندد خیال تو نقش خطا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کف آب در کلبن آتش زند</p></div>
<div class="m2"><p>کجا گشت قهر تو فرمان روا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کجا لطف تو مهربانی نمود</p></div>
<div class="m2"><p>کند دانه را تربیت آسیا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ببازار قدرت چه باشد فلک</p></div>
<div class="m2"><p>یکی اطلس کهنۀ کم بها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز آزاد مردی تو چون سوسنی</p></div>
<div class="m2"><p>که هم خوش زبانی و هم خوش لقا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدندان گوهر بخاید صدف</p></div>
<div class="m2"><p>زشرم لبانت لب خویش را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مظفّر ضمیر تو بر معظلات</p></div>
<div class="m2"><p>چو بر خیل ظلمت سپاه ضیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر بحر و کان خوانمت گاه جود</p></div>
<div class="m2"><p>چنان دان که گفتم ترا ناسزا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در ایّام عدل تو از راستی</p></div>
<div class="m2"><p>کمان نیز سرباز زد زانحنا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نهادست خوان کرم همّتت</p></div>
<div class="m2"><p>بآفاق در داده بانگ صلا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دعای تو کر کوه کر بشنود</p></div>
<div class="m2"><p>جزآمین نگوید زبان صدا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کسی کو زخاک درت سرمه کرد</p></div>
<div class="m2"><p>نیاید بچشم اندرش تو تیا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خرد سرّ غیبی کند فهم ازو</p></div>
<div class="m2"><p>چو گوید سر کلک تو لوترا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگستاخی آنگه گه گه فلک</p></div>
<div class="m2"><p>دهد بوسه سّم سمند ترا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خیالی کژ از صورت ماه نو</p></div>
<div class="m2"><p>همی گردد اندر دلش دایما</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که اندر ترفّع هلاکش کند</p></div>
<div class="m2"><p>بنعل سم اسب تو اقتدا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زهی نعت حلمت ززین الحصی</p></div>
<div class="m2"><p>زهی وصف بأست شدید القوی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی داستانست ما را دراز</p></div>
<div class="m2"><p>بری از دروغ و جدا ز افترا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از آنها که در غیبت خواجه رفت</p></div>
<div class="m2"><p>درین شهر خاصه بر اصحابنا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چه از پادشاه و چه از زیر دست</p></div>
<div class="m2"><p>چه از پیشکار و چه از پیشوا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر سمع عالی نگردد ملول</p></div>
<div class="m2"><p>مفصّل بگویم همه ز ابتدا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نخستین بتاراج بردند دست</p></div>
<div class="m2"><p>زغارت شدند اغبیا اغنیا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بخواندند جاسوا خلال الدیّار</p></div>
<div class="m2"><p>محابانبد هیچ بر اولیا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نهان خانه ها بی دیانت شدند</p></div>
<div class="m2"><p>بنا اهل کردند امانت ادا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حدیثش زده دسته سنجاب بود</p></div>
<div class="m2"><p>کرامایه بد دستۀ گندنا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کشیدند زرها و کردند پس</p></div>
<div class="m2"><p>ززّر کشیده کلاه و قبا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو راز دل عاشق از اشک شد</p></div>
<div class="m2"><p>دفاین هویدا زستر خفا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فزلزلت الارض زلرالها</p></div>
<div class="m2"><p>واخرجت الارض اثقالها</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو از غارت رخت فارغ شدند</p></div>
<div class="m2"><p>ببردند خانه باعیانها</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه قابل نقل و تحویل گشت</p></div>
<div class="m2"><p>سرای و دکان ها و خان و بنا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بسا خاندانهای پیر قدیم</p></div>
<div class="m2"><p>که بودش عصای ستون متّکا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که از اوج چرخش بیک دستبرد</p></div>
<div class="m2"><p>فکندند ناگه بتحت الثّری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنان شد پراکنده از هم که نیز</p></div>
<div class="m2"><p>نکردند با هم دو خشت التقا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو دندان پر از رخنه دیوار لیک</p></div>
<div class="m2"><p>خلالی نکرده بدو در رها</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شده خیره چون ناکسی بر طباع</p></div>
<div class="m2"><p>خلل بر خللها فنا بر فنا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اذّا دکّت الارض منشور خاک</p></div>
<div class="m2"><p>بر ایوانها نقش نطوی السمّا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>لب بام کرده زمین بوس در</p></div>
<div class="m2"><p>ستونها ز ضجرت برفته زجا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>قواعد زخانه نشینی ملول</p></div>
<div class="m2"><p>بیک ره شده در جوار جلا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زخوامی شده خشتها خر سوار</p></div>
<div class="m2"><p>بیفتاده از قالب انزوا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بتنگ آمده آجر اندر نهفت</p></div>
<div class="m2"><p>تفرّج گزیده بصحن فضا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وطن کرده بدرود خاک دمن</p></div>
<div class="m2"><p>بپشت خران رفته باروستا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مساکن چو سکّان شده منزعج</p></div>
<div class="m2"><p>که چونین همی کرد وقت اقتضا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زسودای سیم و زر اندوختن</p></div>
<div class="m2"><p>شده مغز قومی پر از کیمیا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دگرباره آن ضربهای عنیف</p></div>
<div class="m2"><p>وزان قسمت زرّ بی منتها</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تهی دست چون سر و در تخته بند</p></div>
<div class="m2"><p>درم دار چون سکّه خورده قفا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو دوک این یکی ریسمان در گلو</p></div>
<div class="m2"><p>چو چرخ آن یکی کنده بر دست وپا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یکی برکشیده رک از تن چو چنگ</p></div>
<div class="m2"><p>یکی کعب سوراخ کرده چونا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یکی کرده پیرایه از زن برون</p></div>
<div class="m2"><p>یکی کرده پیراهن از تن جدا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یکی چوب بر سرکه بفروش هین</p></div>
<div class="m2"><p>یکی در شکنجه که بشتاب ها</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کشیدند از چشم نرگس برون</p></div>
<div class="m2"><p>زری رسته کان بد بمهر خدا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بیفسرد در ناخن غنچه خون</p></div>
<div class="m2"><p>که بود از شکنجه تنش در عنا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>زن پارسا چون گل پارسی</p></div>
<div class="m2"><p>برون افتاده ز پرده سرا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بمجمع زبهر دو سه خرده زر</p></div>
<div class="m2"><p>شخوده رخان و دریده وطا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همی کرد دندان کنان زیر چوب</p></div>
<div class="m2"><p>شکوفه ز خود سیم خود را جدا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سر آزاد از آن قوم سوسن برست</p></div>
<div class="m2"><p>بزخم زبان و بطال البقا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>توانگر که بد ساخته چون رباب</p></div>
<div class="m2"><p>همه ساز و اسباب عیش از غنا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همش در جهان نام و آوازه بود</p></div>
<div class="m2"><p>همش دستگاهی بساز و نوا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هم او را خزینه همش پرده دار</p></div>
<div class="m2"><p>همش کاسه بود و همش گردنا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گه او را مغمّز و شاق چگل</p></div>
<div class="m2"><p>گهی ترجمانش نگار خطا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خرش را زابریشم افسار و تنگ</p></div>
<div class="m2"><p>سرش را کنار بتان تکیه جا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>نخستش کشیدند در چار میخ</p></div>
<div class="m2"><p>بدادند پس کوشمالش سزا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ببستند دست و زدندش بچوب</p></div>
<div class="m2"><p>که هان! تا چه داری بیاور هلا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خروشید بسیار و سودی نداشت</p></div>
<div class="m2"><p>بجز نقد موزون که می کرد ادا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ککنون خانه و دست و کاسه تهی</p></div>
<div class="m2"><p>فرا داشته پنجه همچون گدا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ضعیفی که چون سوزن تنگ عیش</p></div>
<div class="m2"><p>زدامن درازی بد اندر عنا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هم اسباب رزقش گره برگره</p></div>
<div class="m2"><p>هم ابواب دخل وی از تنگنا</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تن آهنین کرده چون ریسمان</p></div>
<div class="m2"><p>زسعی و تکاپوی بی انتها</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بدان تا دو سه خرقه آرد بهم</p></div>
<div class="m2"><p>بسر می دویدی در اطرافها</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گرفتند زارش بگیسو کشان</p></div>
<div class="m2"><p>بسفتند گوشش بدست جفا</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>کشیدندش از جامه بیرون چنان</p></div>
<div class="m2"><p>که بروی نماندند یکرشته تا</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>وزان شیون خانه ها سوز نو</p></div>
<div class="m2"><p>که بد خانه پرداز تر از وبا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مساجد شده خنق پارگین</p></div>
<div class="m2"><p>منابر شده هیزم شوربا</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>کجا اهل قبله به وی مژه</p></div>
<div class="m2"><p>همی خاک رفتندش از بوریا</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>کنون بینی آنرا بروز سپید</p></div>
<div class="m2"><p>ملا از نجاست چو کنج خلا</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>سگ مرده افتاده در موضعی</p></div>
<div class="m2"><p>که بد جای پیشانی اولیا</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بصفّ خران گشته آراسته</p></div>
<div class="m2"><p>مساجد که بد خانۀ اتقیا</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو اوتاد در سجده افتاد سقف</p></div>
<div class="m2"><p>چو ابدال گشته ستونها دوتا</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>امامان چو قندیل آویخته</p></div>
<div class="m2"><p>چو سجّاد افکنده محرابها</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>مناره همی زد کله بر زمین</p></div>
<div class="m2"><p>که باخاک کردند یکسان مرا</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بتعجیل گهواره را مادران</p></div>
<div class="m2"><p>برون برده از خانه با صد بکا</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>شده همنشین سگ کوی خویش</p></div>
<div class="m2"><p>عروسان پاکیزه با کدخدا</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>یکی زار و گریان که ، واخان و مان!</p></div>
<div class="m2"><p>یکی نوحه گر ، کآه !رسواییا!</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بسا روی پوشیده کو نامدی</p></div>
<div class="m2"><p>زخانه برون روز سور و عزا</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>کنون از سر عجز و بیچارگی</p></div>
<div class="m2"><p>گرفتست بیگانه را آشنا</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ز بی خانگی خفته در مسجدی</p></div>
<div class="m2"><p>زن پیر با دختر پارسا</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>وزان نازنینان که آواره اند</p></div>
<div class="m2"><p>در اطراف گیتی بسا و بسا</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بیاروی و خندق نگه کن ببین</p></div>
<div class="m2"><p>که چون باشگونه ست این ماجرا</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ز خندق تنم زنده در زیر خاک</p></div>
<div class="m2"><p>ز بار و سر مردگان بر هوا</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نه بر طفل رحمت نه از پیر شرم</p></div>
<div class="m2"><p>نه آزرم خلق و نه روی و ریا</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نه کس را پژوهش که این را چه جرم</p></div>
<div class="m2"><p>نه کس را دلیری که گوید : چرا؟</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>تعصّب گری نیست، انصاف کو</p></div>
<div class="m2"><p>مسلمانی و پس بدینها رضا؟</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>تعصّب چه باشد ؟ که این رسم و راه</p></div>
<div class="m2"><p>ندارند ابخازیان هم روا</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چنین رسم و آیین و پس لاف آن</p></div>
<div class="m2"><p>که هستیم اما امّت مصطفی؟</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چه تأویل براین چنینها نهند</p></div>
<div class="m2"><p>قیامت نخواهد بدن گوییا</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بلایی که ما را ز هجرت رسید</p></div>
<div class="m2"><p>بگویم که موجب چه بود اوّلا</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>هرآنکس که کفران نعمت کند</p></div>
<div class="m2"><p>بحرمان ازو می شود مبتلا</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بسی سالها بود کآسوده بود</p></div>
<div class="m2"><p>سپاهان باقبال و جاه شما</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>نه از باد گل را پراکندگی</p></div>
<div class="m2"><p>نه بر سایه از تیغ مهر اعتدا</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>نه بی خطبۀ بلبلان در چمن</p></div>
<div class="m2"><p>شدی محرم غنچه باد صبا</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>نه شمشیر کردی ز روی ادب</p></div>
<div class="m2"><p>برهنه تن خویشتن بر ملا</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ز کوتاه دستی در آن روزگار</p></div>
<div class="m2"><p>نبد جاذبه در تن کهربا</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>درو دعوی روز روشن نشد</p></div>
<div class="m2"><p>مگر کز دو صبحش بد اوّل گوا</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>نه با حاکمان نسبت قصد و میل</p></div>
<div class="m2"><p>نه بر قاضیان و صمت ارتشا</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>قلم گرچه بیمار بود و ضعیف</p></div>
<div class="m2"><p>همی از مزوّر نمود احتما</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>هرآنکس که تلبیس کردی چوشام</p></div>
<div class="m2"><p>چو صبحش به تشهیر بودی جزا</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>زر ارچه دو روییست در طبع او</p></div>
<div class="m2"><p>بکتمان شهادت نکردی ادا</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بسان ترازو شدی سنگسار</p></div>
<div class="m2"><p>بزر هرکه مایل شدی از هوا</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>ندانست کسی قدر این موهبت</p></div>
<div class="m2"><p>بنشناخت کس کنه این اعتنا</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چو شاکر نبودیم از آن لاجرم</p></div>
<div class="m2"><p>اسیر امیری شدیم از قضا</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>خرابی کن و خام چون طبع می</p></div>
<div class="m2"><p>جگر سوز و زر بر چو نرد دغا</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>همه کندن و کشتن و سوختن</p></div>
<div class="m2"><p>نه ترس از خدا و نه از کس حیا</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>بجرم یزیدی زر این مباح</p></div>
<div class="m2"><p>بوزر مخالف دم آن هبا</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>مدارس چو رسم کرم مندرس</p></div>
<div class="m2"><p>مکارم سیه رو چو دست قضا</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>درخت هنر همچو شاخ گوزن</p></div>
<div class="m2"><p>فرمانده بی برگ و نشو و نما</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>گرانمایه را کار در انحطاط</p></div>
<div class="m2"><p>فرو مایه را پایه در ارتقا</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>همه ملک موقوف و موقوف ملک</p></div>
<div class="m2"><p>همه ده کیا آن و ده بی کیا</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>چو روز قیامت گریزان شده</p></div>
<div class="m2"><p>پدر از پسر ، اقربا ز اقربا</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>نه کس را گناهی بجز زندگی</p></div>
<div class="m2"><p>نه کس را پناهی بجز اختفا</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>همه خسته و مرهم از دست دور</p></div>
<div class="m2"><p>همه غرق و بیگانه از آشنا</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>نه برگ خموشی نه یارای گفت</p></div>
<div class="m2"><p>نه پایان خوف و نه بوی رجا</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>چو یارای مسعود صاعد نبود</p></div>
<div class="m2"><p>چه گفتیم؟ بوالقاسم بوالعلا</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>زکفران نعمت مثل زد خدای</p></div>
<div class="m2"><p>بقرآن در ، از حال شهر سبا</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>یکی شهر بود آن بر آراسته</p></div>
<div class="m2"><p>خوش و ایمن ، از مال و نعمت ملا</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>دو بستان زیباش از چپ و راست</p></div>
<div class="m2"><p>پر از گونه گون ساز و برگ نوا</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>زهاب وی از کوثر و سلسبیل</p></div>
<div class="m2"><p>مریضش نسیم و درستش هوا</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>زلالش رحیق و نبانش شکر</p></div>
<div class="m2"><p>نهال وی از سدرة المنتهی</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>گل و سوسن او ز اخلاق نغز</p></div>
<div class="m2"><p>برو میوۀ او زبرّو عطا</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>لقب یافته بلدة طیّبه</p></div>
<div class="m2"><p>و ربّ غفور اندرو مقتدا</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>چو اعراض کردند از شکر حق</p></div>
<div class="m2"><p>یکی جانور کرد ایزد فرا</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>که ناگه بدندان خبث و فساد</p></div>
<div class="m2"><p>بسیل العرم دادشان بر فنا</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>دو بستانشان شد دو بستان بدل</p></div>
<div class="m2"><p>پر از حنظل تلخ و خار گیا</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>درختش همه خار چشم و جگر</p></div>
<div class="m2"><p>نباتش همه تخم جور و جفا</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>نه در چشمه آب و نه در ابر نم</p></div>
<div class="m2"><p>نه بر شاخها گل ، نه گل را روا</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>نه در زیر سایه ، نه از بر ثمر</p></div>
<div class="m2"><p>نه بوی وفا و نه رنگ صفا</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>ز نام سپاهان قیاس ار کنیم</p></div>
<div class="m2"><p>سبا خود بود نیمۀ شهر ما</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>بحمدالله آن دور جور سدوم</p></div>
<div class="m2"><p>نهان گشت در پرده انقضا</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>فکندند در بستگیها کلید</p></div>
<div class="m2"><p>نهادند بر خستگیها دوا</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>لقای تو شد بستگان را نجات</p></div>
<div class="m2"><p>حدیث تو شد خستگان را شفا</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>ز فرّ قدومت بگردون رسید</p></div>
<div class="m2"><p>ز دیوار و در : مرحبا ! مرحبا!</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>بلی مه زند طبل زیر گلیم</p></div>
<div class="m2"><p>چو خورشید تابان شود در غطا</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>سلیمان چو انگشتری گم کند</p></div>
<div class="m2"><p>شود دیو بر آدمی پادشا</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>پرستند گوساله را قوم او</p></div>
<div class="m2"><p>چو موسی بحضرت کند التجا</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>چو خورشید تابنده غایب شود</p></div>
<div class="m2"><p>شگفتی نباشد ظهور سها</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>نپاید کنون چشم بندیّ خصم</p></div>
<div class="m2"><p>چو شد دست کلک تو مشکل گشا</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>خیالات جادو بود باد پاک</p></div>
<div class="m2"><p>چو انداخت از دست موسی عصا</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>فراق تو هرچند ما را سپرد</p></div>
<div class="m2"><p>بچنگال شیرو دم اژدها</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>چو روی تو دیدیم این گفته ایم :</p></div>
<div class="m2"><p>لقد احسن الله فیماضی</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>نه مدح تو بود اینکه منظوم شد</p></div>
<div class="m2"><p>ولکن شکونا الی المشتکی</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>بغربال فکرت ببیز این سخن</p></div>
<div class="m2"><p>که یابی درو خردۀ کیمیا</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>برآرد بسی گوهر شب چراغ</p></div>
<div class="m2"><p>ازین بحر غوّاص ذهن و ذکا</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>نگردد بایطا معیب این سخن</p></div>
<div class="m2"><p>که نظمیست بر گونه گون ماجرا</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>رهی را چنان کز تو زیبد بدار</p></div>
<div class="m2"><p>چو دانی که هستش بتو انتما</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>مکدّر نگشتش بعهد دراز</p></div>
<div class="m2"><p>زباد مخالف زلال صفا</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>ترا رسم تشریف و ما را مدیح</p></div>
<div class="m2"><p>فراوان همی کرد باید قضا</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>بقیتم لشمل العلی ناظماً</p></div>
<div class="m2"><p>سجیس اللیالی بر غم العدی</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>رفیع الندی حلیف الندی</p></div>
<div class="m2"><p>رحیب الفناء مهیب السطی</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>چو خر گه زدی خصم را بر زمین</p></div>
<div class="m2"><p>چو خیمه بکش دامن کبریا</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>زدون همّتی گر زر انداخت خصم</p></div>
<div class="m2"><p>تو جز نام نیکو مکن اقتنا</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>زفرزند و جاه و جوانیّ و مال</p></div>
<div class="m2"><p>ممتّع بمان تا بیوم الجزا</p></div></div>