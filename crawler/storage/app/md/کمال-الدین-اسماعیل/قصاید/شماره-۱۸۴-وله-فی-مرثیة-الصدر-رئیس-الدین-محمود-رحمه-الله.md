---
title: >-
    شمارهٔ ۱۸۴ - وله فی مرثیة الصّدر رئیس الدّین محمود رحمه الله
---
# شمارهٔ ۱۸۴ - وله فی مرثیة الصّدر رئیس الدّین محمود رحمه الله

<div class="b" id="bn1"><div class="m1"><p>دریغا که پژمرده شد ناگهانی</p></div>
<div class="m2"><p>گل باغ دولت بروز جوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحسرت برفت از جهان رادمردی</p></div>
<div class="m2"><p>که بودش بر اقلیم دین قهرمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپیده دم روز اقبال بودش</p></div>
<div class="m2"><p>بدین تیره شب خود کرا بدگمانی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دریغا چنان کامرانی که ناگه</p></div>
<div class="m2"><p>شکستند در کام او کامرانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تابوت کردست اجل تخته بندش</p></div>
<div class="m2"><p>چو سرو سهی قامت پهلوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهالی سرافراز بد لیک گردون</p></div>
<div class="m2"><p>نداد آبش از چشمۀ زندگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز گلبرگ او چون بر آمد بنفشه</p></div>
<div class="m2"><p>ز آفت برو جست باد خزانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوقتی که آمد گل از غنچه بیرون</p></div>
<div class="m2"><p>شد اندر کفن همچو غنچه نهانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهانا ترا شرم ناید که بی او</p></div>
<div class="m2"><p>کنی عرضه بر ما گل بوستانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پیرانه سر خودجوانی کنی، پس</p></div>
<div class="m2"><p>بقهر از جوانان جوانی ستانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو کشتی بباد فنا شمع دین را</p></div>
<div class="m2"><p>چراغ گل از خار بر می دمانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبخشودی آخر بر آن سرو قامت</p></div>
<div class="m2"><p>چه سنگین دلیّ و چه نامهربانی!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه انگام سرسبزی تست، شهری</p></div>
<div class="m2"><p>سیه گشته زین ماتم ناگهانی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه رنگ آورد ارغوان، کرده خلقی</p></div>
<div class="m2"><p>ز خون جگر جامه ها ارغوانی؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لب لالۀ دل سبک چند خندد</p></div>
<div class="m2"><p>نمی ترسد آخر از این دلگرانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز باد فنا ریخت در دامن گل</p></div>
<div class="m2"><p>گلی تازه تر از گل بوستانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرو بسته او همچو غنچه دهن خشک</p></div>
<div class="m2"><p>بسوسن نه لایق بودتر زبانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خرامنده سروا! نگویی چه بودت؟</p></div>
<div class="m2"><p>که امروز گرد چمن ناچمانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو نرگس یکی دیده از خواب بگشا</p></div>
<div class="m2"><p>ز بیماری ار چند بس ناتوانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نشستست صدر جهان بار داده</p></div>
<div class="m2"><p>تو غایب چرایی؟ همانا ندانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه زی بارگاه برادر خرامی</p></div>
<div class="m2"><p>نه ما را سوی حضرت خویش خوانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه یکران آسوده را بر نشینی</p></div>
<div class="m2"><p>نه جعد بشولیده را بر نشانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بساجان که دادند دی در قدومت</p></div>
<div class="m2"><p>یکی از نهیب و دگر مژدکانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس از انتظار دراز تو الحق</p></div>
<div class="m2"><p>نه این چشم می داشتند ارمغانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نمد زینت از یک سفر ناشده خشک</p></div>
<div class="m2"><p>بدین گرمی آخر کجا می داونی؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رهی دور در پیش داری و ترسم</p></div>
<div class="m2"><p>که این نوبت اندر سفر دیرمانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو بس چابکی در سواری و لیکن</p></div>
<div class="m2"><p>چو بین بود مرکبت چون برانی؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز بالای چرخست نام تو گرچه</p></div>
<div class="m2"><p>ز زیر زمین می دهندت نشانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو آنجا مقام تو محمود آمد</p></div>
<div class="m2"><p>نگردی درین خاکدان ایرمانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بنالید ای دوستان و بگریید</p></div>
<div class="m2"><p>بر آن طلعت خوب و فرّ کیانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بخند ای بداندیش او از وفاتش</p></div>
<div class="m2"><p>ز چنگال مرگ ار برستن توانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چه شادی کنی ای بد اندیش کاخر</p></div>
<div class="m2"><p>دهد دور گردونت از این دوستکانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همیشه پی شادمانی غم آرد</p></div>
<div class="m2"><p>چنین بود تا بود گیتیّ فانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هم از صبر جوشن کنیم ار چه سستست</p></div>
<div class="m2"><p>گشاده چو شد ناوک آسمانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بحمدالله ار چه ستاره فرو شد</p></div>
<div class="m2"><p>بجایست خورشید چرخ معانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>امام جهان، رکن دین، صدر عالم</p></div>
<div class="m2"><p>سرافراز ایّام، نعمان ثانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو بر جا بود رکن، باطل نگردد</p></div>
<div class="m2"><p>ز نقصان یک خشت اصل مبانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ایا سرفرازی که این هفت گردون</p></div>
<div class="m2"><p>کند بام قدر ترا نردبانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مبینام یک روزت از جای رفته</p></div>
<div class="m2"><p>که تو قطب اقبال این خاندانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو خورشید شرعی و او ماه ملّت</p></div>
<div class="m2"><p>شده روشن از هر دو چشم امانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>میان شما خاک چون حایل آمد</p></div>
<div class="m2"><p>قمر منخسف شد، تو جاوید مانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ترا واپسین انده این باد و آنرا</p></div>
<div class="m2"><p>که شادست ازین، واپسین شادمانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نه بر وفق ذوقست این شعر لیکن</p></div>
<div class="m2"><p>مرامی نیاید ز من هم نهانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خدایا! درین ساعت از گنج رحمت</p></div>
<div class="m2"><p>هزاران لطیفه بخاکش رسانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز فرزند و جاه و جوانی و دولت</p></div>
<div class="m2"><p>تمتّع ده این خواجه را جاودانی</p></div></div>