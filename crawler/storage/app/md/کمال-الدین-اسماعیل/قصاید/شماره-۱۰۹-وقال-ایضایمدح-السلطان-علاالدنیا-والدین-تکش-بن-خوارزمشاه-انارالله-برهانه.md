---
title: >-
    شمارهٔ ۱۰۹ - وقال ایضایمدح السلطان علاءالدنیا والدین تکش بن خوارزمشاه انارالله برهانه
---
# شمارهٔ ۱۰۹ - وقال ایضایمدح السلطان علاءالدنیا والدین تکش بن خوارزمشاه انارالله برهانه

<div class="b" id="bn1"><div class="m1"><p>ای ز رایت ملک و دین در نازش و در پرورش</p></div>
<div class="m2"><p>ای شهنشاه فریدون فرّ اسکندرمنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ حکمت آفتاب گرم رو راپی کند</p></div>
<div class="m2"><p>تاب عزمت آورد خاک زمین را در روش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقتبس از شعلۀ رایت شعاع آفتاب</p></div>
<div class="m2"><p>مستعار از نفحۀ خلقت نسیم خوش دمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر آمد گوهر تیغ تو در روز نبرد</p></div>
<div class="m2"><p>بر سر آید هر کرا زان دست باشد پرورش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتاب فتح را از سایۀ چترت طلوع</p></div>
<div class="m2"><p>آب روی ملک را از آتش تیغت زهش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوسه جای اختران باشد فراوان سالها</p></div>
<div class="m2"><p>خاک راهی کان شد از نعل سمندت منتقش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو سلیمان تا ببیند رونق و آیین ملک</p></div>
<div class="m2"><p>کو فریدون تا بیاموزد ز تو داد و دهش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فیض لطفت مانعست ار نی ز تاب خشم تو</p></div>
<div class="m2"><p>همچو مه بگداختی اجزای خورشید از تبش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای عجب شمشیر خسرو از چه سبزرنگ شد</p></div>
<div class="m2"><p>چون همه ساله ز خون لعل مییابد خورش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز چترت چون بجنبد دشمنت را مرغ دل</p></div>
<div class="m2"><p>همچو مرغ نیم بسمل حالی افتد در تپش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز کوشش چون نماید قهر تو دندان کین</p></div>
<div class="m2"><p>آید آنجا خنجرت را جان به لب از بس کشش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای خداوندی که هستند از نهیب خنجرت</p></div>
<div class="m2"><p>در میان سنگ و آهن، آب و آتش مرتعش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرد بر دل خوش تطاولهای رمحت خصم لیک</p></div>
<div class="m2"><p>گه گهش سخت آید از گرز گرانت سرزنش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مدّت عمر بداندیش تو زان کوتاه شد</p></div>
<div class="m2"><p>کز نهیب تو هم آمد روزگارش بدکنش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آسمان از گرد خیلت زان همی بندد نقاب</p></div>
<div class="m2"><p>تا نگردد روی خورشید از سنانت مندخش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تیر را هرچند کش تو بیشتر در خود کشی</p></div>
<div class="m2"><p>بیشتر بینم مر او را سوی اعدایت کشش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر عیار ملک ایران غش ظلم ار هست، باش</p></div>
<div class="m2"><p>تیغ تو سرسبز بادا کش بپا لاید زغش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با فلک گفتم کجا دانی پناهی آن چنانک</p></div>
<div class="m2"><p>بخت افتاده شود در سایۀ او منتعش؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صبح صادق با لبی خندان اشارت کرد و گفت:</p></div>
<div class="m2"><p>حضرت سلطان علاءالدّین والدّنیا، تکش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سایۀ حقّست، یارب سایه اش پاینده دار</p></div>
<div class="m2"><p>زانکه فرضست از میان جان دعای دولتش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p></p></div>