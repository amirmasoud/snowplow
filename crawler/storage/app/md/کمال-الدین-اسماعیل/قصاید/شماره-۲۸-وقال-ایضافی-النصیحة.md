---
title: >-
    شمارهٔ ۲۸ - وقال ایضاًفی النصیحة
---
# شمارهٔ ۲۸ - وقال ایضاًفی النصیحة

<div class="b" id="bn1"><div class="m1"><p>ز کار آخرت آن را خبر تواند بود</p></div>
<div class="m2"><p>که زنده بر پل مرگش گذر تواند بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آرزو و هوس بر نیاید این معنی</p></div>
<div class="m2"><p>به سوز سینه و خون جگر تواند بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو روز در غم دنیا و شب غنوده به خواب</p></div>
<div class="m2"><p>ز کار آخرتت کی خبر تواند بود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصال دوست طلب میکنی بلاکش باش</p></div>
<div class="m2"><p>که خار و گل همه با یکدگر تواند بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ترک خویش بگو تا بکوی یار رسی</p></div>
<div class="m2"><p>که کارهای چنین با خطر تواند بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی به گردن مقصود دست حلقه کند</p></div>
<div class="m2"><p>که پیش تیر بلاها سپر تواند بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آب خوش نتوان یافت عقد در خوشاب</p></div>
<div class="m2"><p>که تلخ و شور مقر گهر تواند بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو نیشکر اگرت خوشدلی همی باید</p></div>
<div class="m2"><p>ز پای تا به سرت در کمر تواند بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلاه ملک طلب می کنی، قبا دربند</p></div>
<div class="m2"><p>که سرفرازی با بیم سر تواند بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حیات باقی خواهی بدان که این دولت</p></div>
<div class="m2"><p>ز چار حد طبایع به در تواند بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگرچه کار بزرگیست، هم طمع بمبر</p></div>
<div class="m2"><p>به جان بکوش، چه دانی؟ مگر تواند بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلندهمت باش ای پسر که رتبت تو</p></div>
<div class="m2"><p>چنانکه همت تست آن قدر تواند بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز حال بی خودی آن را که بهره ای باشد</p></div>
<div class="m2"><p>وجود در نظرش مختصر تواند بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو کرده جوشن غفلت هزار تو در بر</p></div>
<div class="m2"><p>چگونه تیر سخن کارگر تواند بود؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جفا به جای کسی چون کنی که در دو جهان</p></div>
<div class="m2"><p>ازو گزر نه و از جان گزر تواند بود؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ترا ز همت دون در طمع نمی گذرد</p></div>
<div class="m2"><p>که لذتی بجز از خواب و خور تواند بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بآب و سبزه قناعت مکن ز باغ بهشت</p></div>
<div class="m2"><p>که این قدر علف گاو و خر تواند بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو دور در شوی از فکر اعتقاد کنی</p></div>
<div class="m2"><p>که خوان و نان بهشت از شکر توان بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز تنگ چشمی، در خاطر تو کی گذرد</p></div>
<div class="m2"><p>که هیچ چیز به از سیم و زر تواند بود؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شکر چه باشد و زر چیست ای اسیر حواس؟</p></div>
<div class="m2"><p>ترا چنین که تویی این نظر تواند بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به چشم عقل ببین و به ذوق جان دریاب</p></div>
<div class="m2"><p>کزین لذیذتر و خوب تر تواند بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وگر تو چاشنی ای زان به نقد می خواهی</p></div>
<div class="m2"><p>دعای قطب زمانه عمر تواند بود</p></div></div>