---
title: >-
    شمارهٔ ۱۶۳ - و قال ایضاً
---
# شمارهٔ ۱۶۳ - و قال ایضاً

<div class="b" id="bn1"><div class="m1"><p>زهی رسیده بجایی که بر سپهر برین</p></div>
<div class="m2"><p>دعای جان تو گشتست ورد روح امین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسان سوزن نظّام نوک خامۀ تو</p></div>
<div class="m2"><p>همی کشد سوی هم عقدهای درّ ثمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر که لیق دویتت شود، در این سودا</p></div>
<div class="m2"><p>همی بپیچد بر خویش زلف حورالعین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باستراق حدیث تو در منافذ گوش</p></div>
<div class="m2"><p>هزار رهزن اندیشه کرده اند کمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرد چو معنی باریک و لفظ جزل تودید</p></div>
<div class="m2"><p>چه گفت؟ گفت: زهی ازدواج غثّ و سمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر آن کجا که زبان آوریست همچون شمع</p></div>
<div class="m2"><p>زکنه مدح تو شد با لگن ز عجز قرین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بنده خانه قدم رنجه کرده یی آری</p></div>
<div class="m2"><p>برای تربیت من کنی هزار چنین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بام کعبه بسوراخ مورفرق بسیست</p></div>
<div class="m2"><p>ولیک پر تو خورشید را چه آو چه این</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان شد از شرف پای تو ستانۀ من</p></div>
<div class="m2"><p>که در نیارد سر زین سپس بعلّبّین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از این تفاخر در کوی من عجب نبود</p></div>
<div class="m2"><p>که سر برآرد با فرق چرخ خاک زمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا که در ره شکر تو دست و پایی نیست</p></div>
<div class="m2"><p>بدست و پا همه تشریف دادی و تمکین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخدمت تو از آن جان خشک آوردم</p></div>
<div class="m2"><p>که در جهان بجز از جان نداشتم شیرین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صداع عذر نمی آورم چرا؟ زیرا</p></div>
<div class="m2"><p>که نیست لطف تو در حقّ من همین و همین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در آستین مراد تو باد دست قضا</p></div>
<div class="m2"><p>بر آستان بقایت سر شهور و سنین</p></div></div>