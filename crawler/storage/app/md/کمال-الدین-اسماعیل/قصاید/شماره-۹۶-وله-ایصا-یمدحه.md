---
title: >-
    شمارهٔ ۹۶ - وله ایصاً یمدحه
---
# شمارهٔ ۹۶ - وله ایصاً یمدحه

<div class="b" id="bn1"><div class="m1"><p>زهی بسیرت محمود در جهان مذکور</p></div>
<div class="m2"><p>زهی بدیدۀ تعظیم از آسمان منظور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پناه اهل معانی و افتخار عراق</p></div>
<div class="m2"><p>که باد عین کمال از جمال بخت تو دور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تویی بفیض کرم میزبان آن عالم</p></div>
<div class="m2"><p>که آفتاب شد آنجا بسفلگی مشهور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درون منظرۀ وهم تست بیش از عقل</p></div>
<div class="m2"><p>برون کنگرۀ مجدتست قصر قصور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زرشح طبعت ورتفّ خاطرت مه و مهر</p></div>
<div class="m2"><p>چو نارو آبی مرطوب گشته و محرور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بساط حضر جاه و تو سندس افلاک</p></div>
<div class="m2"><p>حریم صدر رفیع تو خانۀ معمور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صدای صیت توطی کرده طول و عرض وجود</p></div>
<div class="m2"><p>لعاب کلک تو حل کرده مشکلات امور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عروس فکر تو خاتون آن شبانست</p></div>
<div class="m2"><p>که مطبخیست درو آفتاب و مه مزدور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بپیش رای تو گر صبح کرد دم سردی</p></div>
<div class="m2"><p>برو مگیری تو کان هست نقثة المصدور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بحسن رای صواب ارعلاج دهر کنی</p></div>
<div class="m2"><p>نیاید ایچ در اطراف روزگار فتور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دهان تیر چنان بازمانده از پی چیست؟</p></div>
<div class="m2"><p>اگر نشد بجگر گوشۀ عدوت آزور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بحلق صبح درون زان شود نفسها تیغ</p></div>
<div class="m2"><p>که پیش نور ضمیر تو کرد دعوی نور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کند زمانه سجلْات چرخ را مطویّ</p></div>
<div class="m2"><p>اگر دهند زدیوان قهر تو منشور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حسود لاف زنت را از آن سرپر باد</p></div>
<div class="m2"><p>چه حاصلست بجز دست بسته چون طنبور؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر آفتاب کله گوشه بی تو بنماید</p></div>
<div class="m2"><p>سپهر برکشد از سفت او غلالۀ نور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زهی مصالح گیتی بسعی تو منظوم</p></div>
<div class="m2"><p>زهی مساعی خوب تو در جهان مشکور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنین که من ز هنرهای خویش محرومم</p></div>
<div class="m2"><p>چه فایده که بود خطّْ دانشم موفور؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو گوش بخشش کر شد چه سود صیت هنر</p></div>
<div class="m2"><p>چو غنچه کور دل آمد، چه سود لحن طیور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سزد که خوشۀ یاقوت منتظم دهیم</p></div>
<div class="m2"><p>بعرض این سخنان چو لؤ لؤ منثور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر چه دختر رز چون گلست ترا دامن</p></div>
<div class="m2"><p>ز شور بختی خادم چو غنچه شدمستور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حدیقۀ عنبی من ارچه سیرابست</p></div>
<div class="m2"><p>ولیک حاصل ان بر عصیر شد مقصور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سیه چو گشت مرا ز انتظار خانۀ چشم</p></div>
<div class="m2"><p>چو کان لعل کنم از تو خانۀ انگور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر چه زحمت بسیار میدهم هر وقت</p></div>
<div class="m2"><p>مکارم تو همانا که داردم معذور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه تا که بود کامکار بخت جوان</p></div>
<div class="m2"><p>زرای پیر تو بادا زمانه را دستور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درآستین مرادت کلید لیل و نهار</p></div>
<div class="m2"><p>برآستان بقایت سر سنین و شهور</p></div></div>