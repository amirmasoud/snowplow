---
title: >-
    شمارهٔ ۶۸ - وله ایضاً فیه
---
# شمارهٔ ۶۸ - وله ایضاً فیه

<div class="b" id="bn1"><div class="m1"><p>سرورا همّت تو برتر از آنست که عقل</p></div>
<div class="m2"><p>گرد انکامۀ نه شعبده بازش بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا گفت قدر نیست ازین برتر جای</p></div>
<div class="m2"><p>بارگاهی ز جلال تو فرازش بیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست در کارگه نطق یکی جامه که عقل</p></div>
<div class="m2"><p>نه ز القاب شریف تو طرازش بیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ سیّار گذر کرد نیارد بر چرخ</p></div>
<div class="m2"><p>که نه از خطّ رضای تو جوازش بیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتاب ار نکند پیروی سایۀ تو</p></div>
<div class="m2"><p>در تن خویش چو در سایه گدازش بیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همره صیت معالّی تو شد ماه مگر</p></div>
<div class="m2"><p>که همیشه فلک اندرتک و تازش بیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عدل تو سرزنش کلک کند ز آنکه همی</p></div>
<div class="m2"><p>با عروس تّق غیب برازش بیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زحل ار بر فلک همّت تو جای کند</p></div>
<div class="m2"><p>زآن سپس چرخ بصددولت و نازش بیند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جود هر جائیت آن شیفته کارست که عقل</p></div>
<div class="m2"><p>دایم آویخته در دامن آزش بیند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو افلاک کند دامن اطلس در خاک</p></div>
<div class="m2"><p>هر که چو من ز سخای تو نوازش بیند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وآنکه چون سیر برهنه بر جودت آید</p></div>
<div class="m2"><p>بخت در صدرۀ ده تو چو پیازش بیند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اعتقادیست رهی را که ز صدق خدعت</p></div>
<div class="m2"><p>چرخ همواره بدین سدّه نیازش بیند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جز ز مدّاحی دولتکدۀ صاعدیان</p></div>
<div class="m2"><p>چشم بر دوخته اقبال چو بازش بیند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاطرش را نبود هیچ عروس سخنی</p></div>
<div class="m2"><p>که بجز زیور مدح تو جهازش بیند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرچه پستست رهی بر گذرد از همگان</p></div>
<div class="m2"><p>اگر از تربیتت قوّت یازش بیند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رفت آن کز پی یک خردۀ زر چشم امل</p></div>
<div class="m2"><p>باز گرده دهن حرص چوگازش بیند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یا باومید عطا چشم هنر هر ساعت</p></div>
<div class="m2"><p>بهر هر نااهلی مدح طرازش بیند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یا چو خورشید پی کسب قراضات نجوم</p></div>
<div class="m2"><p>از تف سینه سپهر آتش بازش بیند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرچه در خاطر او دوش نیامد که کسی</p></div>
<div class="m2"><p>از حضیض کرۀ خاک فرازش بیند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باز نشناسدش امروز ز طاوس فلک</p></div>
<div class="m2"><p>هر که در حلقۀ تشریف تو بازش بیند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یارب اندر کنف لطف بدارش چندان</p></div>
<div class="m2"><p>ابد صد یکی از عمر درازش بیند</p></div></div>