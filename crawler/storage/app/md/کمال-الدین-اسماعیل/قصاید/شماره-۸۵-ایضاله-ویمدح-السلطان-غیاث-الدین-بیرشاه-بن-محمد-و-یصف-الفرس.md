---
title: >-
    شمارهٔ ۸۵ - ایضاًله ویمدح السّلطان غیاث الدّین بیرشاه بن محمّد و یصف الفرس
---
# شمارهٔ ۸۵ - ایضاًله ویمدح السّلطان غیاث الدّین بیرشاه بن محمّد و یصف الفرس

<div class="b" id="bn1"><div class="m1"><p>خدای داد بملک زمانه دیگر بار</p></div>
<div class="m2"><p>طراوتی نه باندازۀ قیاس وشمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفّر سایۀ رایات خسرو منصور</p></div>
<div class="m2"><p>غیاث دولت ودین کز سپهرش آیدعار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدایگان سلاطین مشرق ومغرب</p></div>
<div class="m2"><p>که دست وخنجراوهست ابرصاعقه بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلندهمّت بسیاردان اندک سال</p></div>
<div class="m2"><p>جهانگشای ممالک ستان گیتی دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پلنگ خاصیت پیل زورشیرافکن</p></div>
<div class="m2"><p>همای سایۀ طوطی حدیث باز شکار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درشت باطشۀ نرم گوی سخت کمان</p></div>
<div class="m2"><p>گران عطا وسبک حملۀ لطیف آثار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیاث ملّت و دولت، شهنشه عالم</p></div>
<div class="m2"><p>که باد تا بقیامت زملک برخوردار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بچرب دستی اقبال او مطرّا شد</p></div>
<div class="m2"><p>لباس ملکی کزوی نه پودبودونه تار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به آب تیغ و بگرزگران بشست وبکوفت</p></div>
<div class="m2"><p>ازآن سپس که بخون عدوش دادآهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهی زهیبت تو کُند ظلم را دندان</p></div>
<div class="m2"><p>زهی زخنجرتو تیز عدل را بازار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زجودتست امل را هزار دلگرمی</p></div>
<div class="m2"><p>به عفوتست گنه را هزار استظهار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برنوازش لطف تو،بخت کم ناموس</p></div>
<div class="m2"><p>بنزد مالش قهرت،زمانه نیکوکار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هوای مهرتو، تن را مفیدتر زغذا</p></div>
<div class="m2"><p>حروف نام تو زر را شگرف تر زعیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زمین ببوسدخورشید،چون توگیری جام</p></div>
<div class="m2"><p>میان ببندداقبال،چون تودادی بار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگاه لطف،جهانراوفاکنی تعلیم</p></div>
<div class="m2"><p>بگاه کینه برآری زروزگاردمار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میان طبع وستم خمشت آتشین باره</p></div>
<div class="m2"><p>میان ملک وخلل تیغت آهنین دیوار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زمهروکین تو تمییز یافتند ارنی</p></div>
<div class="m2"><p>دوشاخ بودند از یک درخت منبرو دار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ببست چاوش سهم توراه برفتنه</p></div>
<div class="m2"><p>ببردسایۀ شمشیر تو زکوه وقار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بخانه های کمان تو پی برد فکرت</p></div>
<div class="m2"><p>چومرگ نقب زند در خزینۀ اعمار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مگرکه تیر ترا نسبتست باشیطان</p></div>
<div class="m2"><p>که درمجاری خون ورگش بود رفتار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شودزگرزتوگردن شکسته چون نرگس</p></div>
<div class="m2"><p>کراز بادۀ کین تو در سرست خمار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز زیرگرز تودانی که چون جهددشمن؟</p></div>
<div class="m2"><p>بچهره زردوبتن پخچ گشته چون دینار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بخرده کاری گرز تو برسرآمده است</p></div>
<div class="m2"><p>اگرچه سخت گرانست وجلف و ناهموار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زطبع تیز نیاید قرار و این عجب است</p></div>
<div class="m2"><p>که تیغ تیز تو دادست کارملک قرار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کندرمّرد تیغت بحلقه های زره</p></div>
<div class="m2"><p>چنانکه عکس زمّرد بچشم افعی کار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خیال تیغ توگربردل عدوگذرد</p></div>
<div class="m2"><p>ندیده زخم،دونیمه شود بسان انار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زوصف تیغ توزان قاصرم که اندیشه</p></div>
<div class="m2"><p>بریده گشت چو بر تیز ناش کردگذار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کلیدخانۀ فتحست نعل مرکب تو</p></div>
<div class="m2"><p>که هرکجابرسیداو،گشاده گشت حصار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تکاوری که نداردخبرزمین زسمش</p></div>
<div class="m2"><p>که ازبرش بیکی پای رفت یا بچهار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هزاردایره برنقطه یی پدیدآرد</p></div>
<div class="m2"><p>مگرقوایمش ازآهنست چون پرگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بخوش عنانی برآب بگذردچوحباب</p></div>
<div class="m2"><p>بگرم تازی زآتش برون جهد چو شرار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بسان قطرۀ اشکی که ازمژه بدود</p></div>
<div class="m2"><p>گذرکندزبرتارموی درشب تار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سوی نشیب شتابان چوقطره درنوروز</p></div>
<div class="m2"><p>سوی بلندی تازان چوابردرآزار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فراخ گام چواندیشه،دوربین چوطمع</p></div>
<div class="m2"><p>نظرستان چونکویی خجسته پی چویسار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>رمنده همچومرادورسنده چون روزی</p></div>
<div class="m2"><p>جهنده همچونسیم وخورنده آتش وار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چوخشم آتش پای وچوصبرآهن خای</p></div>
<div class="m2"><p>چومرگ ناگه گیروچوعمرخوش رفتار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ببردباری ماندچوباشدآهسته</p></div>
<div class="m2"><p>بکامرانی ماندچومی رود رهوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برنگ آتش ودنبال وبش چو دو دسیاه</p></div>
<div class="m2"><p>بشکل لاله واطراف اوچو نور ازنار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ازآنک ازتک او باز پس فتد آهو</p></div>
<div class="m2"><p>شکار آهو بر پشت اوبود دشوار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چوگرم گشت نیاردچخید با او برق</p></div>
<div class="m2"><p>چو تندتند نتواند برو نشست غبار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چوصیت خسروگیتی نوردازآن آمد</p></div>
<div class="m2"><p>که ایمنست چوبخت توجاودان زعثار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چوروزجنگ زگردسپاه شب گردد</p></div>
<div class="m2"><p>درو زبیم بود دیدۀ سنان بیدار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو بادلیران نیزه زبان کنددرکام</p></div>
<div class="m2"><p>چو بر نهند یلان بر رخ سپر رخسار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سوادچشم گزارد بنوک تیرنظر</p></div>
<div class="m2"><p>نیام تیغ زشریان خورد روان ادرار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دل دلیران بینی میان نیزه وتیر</p></div>
<div class="m2"><p>برآمده خوش وخندان چنانکه غنچه زخار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زحلقه های زره خون پردلان جوشان</p></div>
<div class="m2"><p>چنانکه ازشکن زلف،رنگ چهرۀ یار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زرشق تیر تن مرد نیزه وربینی</p></div>
<div class="m2"><p>چوخارپشت که ماراندرآورد بکنار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مبارزان راازخوی بگل فروشده پای</p></div>
<div class="m2"><p>بمانده دست تحیّربدست بر چو چنار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فتاده بینی درموج خون چوسایه درآب</p></div>
<div class="m2"><p>زتاب حمله زبر زیرگشته اسب وسوار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زخود و جوشن بی مرد،روی دشت نبرد</p></div>
<div class="m2"><p>چوسطح آب که باشدحباب ازو دیدار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگرچوپیکان زاهن بودسردشمن</p></div>
<div class="m2"><p>دونیمه گردد از زخم تیغ چون سوفار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چنان گذارده کند نیزه برمسام زره</p></div>
<div class="m2"><p>بگاه حمله که آیدزپوست بیرون مار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خم خنجرسبزت چنان برآید خون</p></div>
<div class="m2"><p>که ظن برندکه آتش همی جهد ز خیار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چنان برآردگرزت زاستخوانها مغز</p></div>
<div class="m2"><p>که ازدرخت برآرد شکوفه باد بهار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زبان برآردتیغ تو وعدوا انگشت</p></div>
<div class="m2"><p>و لیک این همه جان خواهد آن همه زنهار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تومی خرامی آن گرزگاوسار بدست</p></div>
<div class="m2"><p>شتردلانرا بندکمندکرده مهار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کمندچه؟که ببندقبای خودهمه را</p></div>
<div class="m2"><p>همی کشند بپای علم قطارقطار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کله زدست تو برخاک میزند خورشید</p></div>
<div class="m2"><p>اجل زبیم تودرپای میکشددستار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>جهان ستانا بردعوی جهانداریت</p></div>
<div class="m2"><p>سپهر واختر وارکان همی کنند اقرار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کلاه ملک ترامی سزدکه پشت ترا</p></div>
<div class="m2"><p>بجز قبای تو هگز ندید در پیکار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>زجیب مشرق تاعطف دامن مغرب</p></div>
<div class="m2"><p>بقدّ ملک توبرکسوتیست چون طیار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خدایگانا خود جز ثنای چون توشهی</p></div>
<div class="m2"><p>حرام محض بود نظم گوهر شهوار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>قصیده ها را گر بیت نیک شه بیت است</p></div>
<div class="m2"><p>جزاین قصیده نباشد شهنشه اشعار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>درین زفاف همایون که برتومیمون باد</p></div>
<div class="m2"><p>چنانکه سایۀ چترترا بلاد و دیار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سزدکه گوهروجان رابهم برآمیزد</p></div>
<div class="m2"><p>چو بنده هرکه فرستدبحضرت تونثار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همیشه تاکه بودچشمه سارآب حیات</p></div>
<div class="m2"><p>هرآنکجا که زندمرغ کلک شه منقار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بتخت سلطنت وملک بربکام نشین</p></div>
<div class="m2"><p>هزارسال و نباشد هزار خود بسیار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بپای قدروشرف تارک سپهرسپر</p></div>
<div class="m2"><p>بدست لطف وکرم تخم نیکنامی کار</p></div></div>