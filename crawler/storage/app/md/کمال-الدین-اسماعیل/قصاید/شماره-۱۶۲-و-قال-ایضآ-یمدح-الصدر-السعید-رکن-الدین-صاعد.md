---
title: >-
    شمارهٔ ۱۶۲ - و قال ایضآ یمدح الصّدر السّعید رکن الدّین صاعد
---
# شمارهٔ ۱۶۲ - و قال ایضآ یمدح الصّدر السّعید رکن الدّین صاعد

<div class="b" id="bn1"><div class="m1"><p>برخیّ آن دو عارض و آن زلف نازنین</p></div>
<div class="m2"><p>جان من ار چه نیست بدین حال نازنین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون حلقه بر درم ز وصالش که سال و ماه</p></div>
<div class="m2"><p>در بند سیم و زر بود آن لعل چون نگین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم رخت گلست، و زین ننگ، رنگ گل</p></div>
<div class="m2"><p>می بسترد ز چهره بدان خطّ عنبرین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بس که باد و زلف سیه گر همی نشست</p></div>
<div class="m2"><p>تا لاجرم گرفت رخش رنگ همنشین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عاشقم بدان رخ چون ماه و آفتاب</p></div>
<div class="m2"><p>زنهار تا مرا نکنی سرزنش بدین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سهلست دیدن مه و خورشید و دل بجای</p></div>
<div class="m2"><p>دل را بجای دار و بیا روی او ببین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای شام طرّه های تو سر حّد نیم روز</p></div>
<div class="m2"><p>وی زنگبار زلف تو در اندرون چین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جستجوی وصل تو چون صبح میرویم</p></div>
<div class="m2"><p>زر در دهان نهاده و جان اندر آستین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بادی بعافیت بتو بر نگذرد که نه</p></div>
<div class="m2"><p>فتنه گشاید از زخم زلف بر و کمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از روشنی، حقیقت رویت چو کس ندید</p></div>
<div class="m2"><p>یافه ست گفتنم که: چنانست یا چنین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خورشید را که روی تو نپسنددش غلام</p></div>
<div class="m2"><p>چون با ضمیر صدر جهانش کنم قرین؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از حرمت لبت همه سال عقیق را</p></div>
<div class="m2"><p>در دیده مینشانم و در سیم رکن دین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاهنشه شریعت صاعد، که درگهش</p></div>
<div class="m2"><p>از جور روزگار پناهیست بس حصین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صدری که هست دولت او را فلک مطیع</p></div>
<div class="m2"><p>رادی که هست بخشش او را جهان رهین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای پرتو لقای تو نوروز عقل و جان</p></div>
<div class="m2"><p>وی ظلمت خط تو شبستان حور حین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ابر اربدان گریست که چون دست تو نشد</p></div>
<div class="m2"><p>گو خون گری که نیستی از بحر و کان گزین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ناکرده کس قیاس یسار تو بر بحار</p></div>
<div class="m2"><p>نگرفته کس شمار سخای تو بر یمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گردون بداس ماه نو انگام ارتفاع</p></div>
<div class="m2"><p>از خرمن جلال تو همواره خوشه چین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جام جهان نمای ز رای تو با فروغ</p></div>
<div class="m2"><p>طاس سپهر نام ز حلم تو با طنین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم شمّه یی ز خلق تو در بادبان گل</p></div>
<div class="m2"><p>هم جرعه یی ز لطف تو در جام یاسمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیوسته تاب مهر تو در جان آسمان</p></div>
<div class="m2"><p>افتاده وقع حلم تو در خاطر زمین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در دهر جز میان و سرین سمنبران</p></div>
<div class="m2"><p>جودت رها نکردست از غثّ واز سمین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برخواند حرز مدح تو و بر جهان دمید</p></div>
<div class="m2"><p>اوّل که برگشاد نفس صبح راستین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حزم زمین قرار تو چون خوف پس نگر</p></div>
<div class="m2"><p>رای جهان فروز تو چون عقل پیش بین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از هیبت تو تیغ شود موی بر تنش</p></div>
<div class="m2"><p>چون مهر هر کراسوی او بنگری بکین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون چین بهم فرو شکند طاق آسمان</p></div>
<div class="m2"><p>در طاق ابروان چو شکست آوری ز چین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر دف بزد حرارۀ خورشید چون بدید</p></div>
<div class="m2"><p>ناهید عکس رای تو بر چرخ چارمین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رایات فتح در صف اقبال تو قویست</p></div>
<div class="m2"><p>آیات نجح در خط پیشانیت مبین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زین پس درست مغربی چرخ نام تو</p></div>
<div class="m2"><p>بهر رواج خویش کند نقش بر جبین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با دست درفشان تو رای مری زدی</p></div>
<div class="m2"><p>گر اشک دشمن تو بدی گوهر ثمین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رعد از پی سخات ببانگ بلند گفت:</p></div>
<div class="m2"><p>احسنت! شادباش ! همین شیوه! آفرین!</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شرعست مانع، ار نی از بهر دفع شر</p></div>
<div class="m2"><p>عدلت رها نکردی پیوند را وشین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عالم بدولت تو طرب زای شد چنانک</p></div>
<div class="m2"><p>از چنگ هم نمی شنوم نالۀ حزین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر پای بند خصم شود لفظ عذب تو</p></div>
<div class="m2"><p>می دان که آن شقاوت او را بود ضمین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زیرا که هم بکوی عدم سر برآورد</p></div>
<div class="m2"><p>آن مور را که پای فروشد با نگبین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر با تو دشمن تو زند لاف سروری</p></div>
<div class="m2"><p>باشد حدیث چشمۀ حیوان و پارگین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فصل اعادی تو خزان سخن بود</p></div>
<div class="m2"><p>زیرا که اندر آن نگریزد ز پوستین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر ذروۀ مدارج قدر رفیع تو</p></div>
<div class="m2"><p>وهم گمان نمیرسد و خاطر یقین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زین بیش مایۀ سخنم نیست چون کنم؟</p></div>
<div class="m2"><p>بستم بر اسب خاموشی از اضطرار زین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ختم سخن بکردم تا ظن نییفتدت</p></div>
<div class="m2"><p>کاندازۀ مدیح تو این بود و خود همین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>لیکن ازین قدر نگزیرد که گویمت:</p></div>
<div class="m2"><p>عیدت خجسته باد و خدا حافظ و معین</p></div></div>