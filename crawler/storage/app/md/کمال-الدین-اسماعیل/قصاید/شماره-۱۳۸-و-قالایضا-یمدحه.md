---
title: >-
    شمارهٔ ۱۳۸ - و قالایضاً یمدحه
---
# شمارهٔ ۱۳۸ - و قالایضاً یمدحه

<div class="b" id="bn1"><div class="m1"><p>گهرفشانان ، صدرا ، زعشق الفاظت</p></div>
<div class="m2"><p>بسا غرور من از گوهر عدن بخورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسیم خلق تو چون در دل من آویزد</p></div>
<div class="m2"><p>به سرزنش جگر نافۀ ختن بخورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجرم آنکه بعهد تو جام می برداشت</p></div>
<div class="m2"><p>سزد که خون دل لالۀ چمن بخورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن مقام که لطف تو پرده بردارد</p></div>
<div class="m2"><p>هزار تشویر از بهر نسترن بخورم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببوی لطف بوی تو جان پروردم</p></div>
<div class="m2"><p>من این قسم ز برای گل و سمن بخورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی خورم دم لطف تو وان بجای خودست</p></div>
<div class="m2"><p>دم مسیح گر از بهر زیستن بخورم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن دیار که دیدار تست غم نبود</p></div>
<div class="m2"><p>و گر بود نبود بیش از آنکه من بخورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بآب روی تو کم ذوق زندگانی نیست</p></div>
<div class="m2"><p>زبس قفا که من از گردش زمن بخورم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بمجلسی که درو ماجرای من گویند</p></div>
<div class="m2"><p>زشرم آب شوم خاک انجمن بخورم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برفت آبم و از دست برنمی خیزد</p></div>
<div class="m2"><p>که نیم نانی با این همه محن بخورم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمرهم دگران من غریو دربندم</p></div>
<div class="m2"><p>هزار زخم بدست خودم بزن بخورم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو باز طعمه جز از دست شاه نستانم</p></div>
<div class="m2"><p>و گرزمخمصه مردار چون زغن بخورم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زننگ خواستن از خود قوت درمانم</p></div>
<div class="m2"><p>زغصّه جان بلب آرم چو شمع و تن بخورم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو زر عزیز از آنم که تازه رویم و نرم</p></div>
<div class="m2"><p>بطبع اگر چه بسی زخم دلشکن بخورم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرم زملک قناعت از آن فرو ناید</p></div>
<div class="m2"><p>که از عریش فلک خوشۀ پرن بخورم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وگر ز گرسنگی جان برآیدم چو صبح</p></div>
<div class="m2"><p>حرام بادم ارین قرص شعله زن بخورم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو راحت بدنم در شکنجۀ روحست</p></div>
<div class="m2"><p>عذاب روح دهم گر غم بدن بخورم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شو شمع جان من از آتش نیاز بسوخت</p></div>
<div class="m2"><p>مرا چه سود کند کانده لگن بخورم؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خلاقت من و انواع نامردی ها</p></div>
<div class="m2"><p>بدان کشید که زنهار باوطن بخورم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو تو مرا ندهیّ و نخواهم از دگران</p></div>
<div class="m2"><p>شوم بحکم ضرورت غم شدن بخورم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>متاع من هنرست و زمن بنیم بها</p></div>
<div class="m2"><p>نمی خری تو که بفروشم و ثمن بخورم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زمن نداری باور که حال من چونست</p></div>
<div class="m2"><p>وگر بنزد تو حاشا طلاق زن بخورم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زفرط تنگدلی گشته ام فراخ سخن</p></div>
<div class="m2"><p>مگر غمی بخوری تا غم سخن بخورم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا مدد ده و بنگر که من بتیغ زبان</p></div>
<div class="m2"><p>زحدّ مشرق تا طایف و یمن بخورم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه طالعست ؟ که یک شربت آب سرد مرا</p></div>
<div class="m2"><p>بلب نیاید تا خون دل دومن بخورم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه درد سرکه نیاورد با سرم دستار؟</p></div>
<div class="m2"><p>چه کفشها که من از بهر پیرهن بخورم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدان امید که چون مرغ دانه یی یابم</p></div>
<div class="m2"><p>بسا عذاب که چون مرغ باب زن بخورم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدین دو نان که اگر خودسنان خورم به از آن</p></div>
<div class="m2"><p>پدید نیست که سیلی چند تن بخورم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو میزبان جهانی مرا طفیلی گیر</p></div>
<div class="m2"><p>چه باشد آنچه من زار ممتحن بخورم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کنون که می نکند جور روزگار رها</p></div>
<div class="m2"><p>که من زخوان سخای تو یک دهن بخورم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>توقّع است که بر سفرۀ عنایت تو</p></div>
<div class="m2"><p>رها کنند که من نان خویشتن بخورم</p></div></div>