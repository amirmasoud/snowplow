---
title: >-
    شمارهٔ ۴۰ - وله  ایضآ یمدحه
---
# شمارهٔ ۴۰ - وله  ایضآ یمدحه

<div class="b" id="bn1"><div class="m1"><p>درّی که چرخ بر طبق اسمان نهاد</p></div>
<div class="m2"><p>بهر نثار موکب صدر جهان نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفکند چار نعل هلال آسمان دوبار</p></div>
<div class="m2"><p>تا با رکاب خواجه عنان بر عنان نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن خواجه یی که پایۀ قدرش ز مرتبت</p></div>
<div class="m2"><p>دست جلال برطرف لامکان نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون صبح باز کرد دهان را بمدح او</p></div>
<div class="m2"><p>چرخش درست مغربی اندر دهان نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیرون فکند جرم ترازو زبان ز کام</p></div>
<div class="m2"><p>از بس که بار جود برو بی کران نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گامی که برگرفت سمندش ز روی خاک</p></div>
<div class="m2"><p>بر پشت مهرۀ گذر کهکشان نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سایۀ تواضع و خورشید همّتش</p></div>
<div class="m2"><p>جرم زمین و پیکر گردون توان نهاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر خامه نظم گوهر الفاظ مشکلست</p></div>
<div class="m2"><p>زین قاعده که آن کف گوهر فشان نهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیمرغ صبح را نبود جای دم زدن</p></div>
<div class="m2"><p>آنجا که مرغ همّت او آشیان نهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست امید دوزد بر دامن غرض</p></div>
<div class="m2"><p>تیری که رأی صائب او در کمان نهاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکروزه خرج کیسۀ صراف جود اوست</p></div>
<div class="m2"><p>از مهر هر ذخیره که کان در دکان نهاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جیب و کنار عقل پر از مشک و در شود</p></div>
<div class="m2"><p>کلک سخن طراز چو اندر بنان نهاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای سروری که لفظ کرم را بنان عقل</p></div>
<div class="m2"><p>اندر زبان خامۀ تو ترجمان نهاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آثار لطف تست که از باد روح کرد</p></div>
<div class="m2"><p>اعجاز کلک تست که سحر از بیان نهاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روح القدس مگس بود آنجا که عقل را</p></div>
<div class="m2"><p>لفظ شکرفشان تو از نطق خوان نهاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باس تو با‍‍ز و بدرقه بر ماه و خور فکند</p></div>
<div class="m2"><p>جودت خراج و جزیت بر بحر و کان نهاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تیغ گهر فروش زبانرا کبود کرد</p></div>
<div class="m2"><p>از بس که بر سخایت امان الامان نهاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صفراویان آتش خشم ترا فلک</p></div>
<div class="m2"><p>از اشک چشم دشمن تو ناردان نهاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در پای او فکند فلک اطلسی که داشت</p></div>
<div class="m2"><p>قدرت چو گام در وطن اختران نهاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رای تو خواست تا که مکافات او کند</p></div>
<div class="m2"><p>تاجی ز نور بر سر چرخ کیان نهاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خصمت سبک سرآمد از آن دست روزگار</p></div>
<div class="m2"><p>برپای او ز حادثه بندی گران نهاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پنداشت لاله را که دل دشمنان تست</p></div>
<div class="m2"><p>سوسن درو زبان وقعیت از آن نهاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون آستان مقیم شود بخت بردرش</p></div>
<div class="m2"><p>هر کو چو بخت روی برین آستان نهاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در مدح جز تو چرب زبانی نمود شمع</p></div>
<div class="m2"><p>عقلش ز غیرت آتشی اندر زبان نهاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با آسمان ضمیر تو روزی کرشمه کرد</p></div>
<div class="m2"><p>زان روز آفتاب سر اندر جهان نهاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تقدیر از تواضع و لطف تو در ازل</p></div>
<div class="m2"><p>بر ساخت عنصری و از آن جسم و جان نهاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سرّی که از سپهر نهان داشتی قضا</p></div>
<div class="m2"><p>با منهیان فکر تو اندر میان نهاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در عرصۀ وجود بنای فلک نبود</p></div>
<div class="m2"><p>کاقبال رخت خویش درین خاندان نهاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قهرت ز پای خواست در آورد چرخ را</p></div>
<div class="m2"><p>لیکن و قارو حلم تو دستی بر آن نهاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در نام تو نهاد قضا روح خلق را</p></div>
<div class="m2"><p>خاصیّتی که دل را با زعفران نهاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صدرا! بدان خدای که دست ارادتش</p></div>
<div class="m2"><p>طفل وجود در رحم کن فکان نهاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ادراک صنع اورا بر بام معرفت</p></div>
<div class="m2"><p>از پایۀ حواس خرد نردبان نهاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>قهرش بیک تپانچه فلک را کبود کرد</p></div>
<div class="m2"><p>خوفش غیار بر کتف آسمان نهاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گردر نهاد دوزخ از آن سوز صدیکیست</p></div>
<div class="m2"><p>کاندیشۀ فراق تو در اصفهان نهاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یا رب چه فتنه بود که از سهم و هیبتش</p></div>
<div class="m2"><p>مریّخ تیر خود همه در دو کدان نهاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آن رفت، شکرهاست ز ایزد که مقدمت</p></div>
<div class="m2"><p>انگشت لطف بر دل پیر و جوان نهاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در ضمن آن هر آینه مدرج سعادتیست</p></div>
<div class="m2"><p>رنجی که بر تو این سفر ناگهان نهاد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در سختیست راحت و زین روی کردگار</p></div>
<div class="m2"><p>مغز لطیف تعبیه در استخوان نهاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چشم بد از تو دور که گردون زمام خویش</p></div>
<div class="m2"><p>اندر کف تو خواجۀ صاحب قران نهاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا می قدر به کالبد اندر روان نهد</p></div>
<div class="m2"><p>گوید خرد که گوهر در خاکدان نهاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جاوید زی که دور فلک وضع روزگار</p></div>
<div class="m2"><p>چونانکه رفت اشارت تو همچنان نهاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پیوسته باد چشم تو روشن ببخت آن</p></div>
<div class="m2"><p>کش عقل نام مهدی آخر زمان نهاد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یا رب تو در قماط بزرگی بپرورش</p></div>
<div class="m2"><p>کز عمر او ابد مدد جاودان نهاد</p></div></div>