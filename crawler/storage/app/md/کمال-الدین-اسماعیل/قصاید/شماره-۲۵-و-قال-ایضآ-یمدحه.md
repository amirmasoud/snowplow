---
title: >-
    شمارهٔ ۲۵ - و قال ایضآ یمدحه
---
# شمارهٔ ۲۵ - و قال ایضآ یمدحه

<div class="b" id="bn1"><div class="m1"><p>طراوتی که جهان از دم بهار گرفت</p></div>
<div class="m2"><p>شریعت از نفس صدر کامکار گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدایگان شریعت که قاضی افلاک</p></div>
<div class="m2"><p>ز روی فرّخ او فال اختیار گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحکم آنکه سر سال برستانۀ اوست</p></div>
<div class="m2"><p>کلاه خلعت سر سبزی از بهار گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا که مایه ده طبلۀ ریاحین است</p></div>
<div class="m2"><p>ز خلق خواجه نسیمی بیادگار گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قیامتیست بصحرا که زنده می گردد</p></div>
<div class="m2"><p>تنی که خاکش شش ماه در حصار گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مردگان که کفن ها بدوش برفکنند</p></div>
<div class="m2"><p>درختهای شکوفه همان شعار گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درخت پیری که موی سرش بریخته بود</p></div>
<div class="m2"><p>از آن سپس که دو تا گشت، دستوار گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دم مبارک باد صبا بدو پیوست</p></div>
<div class="m2"><p>جوان و تازه شد و دست در نگار گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کلّۀ چمن اندر بقرب یک هفته</p></div>
<div class="m2"><p>عروس گشت و بشوهر رسید و بار گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو رعد طبل بشارت بزد بیامد ابر</p></div>
<div class="m2"><p>نثار او همه از درّ شاهوار گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هوای باغ خنک بود و نرگس مسکین</p></div>
<div class="m2"><p>بخفت مست و سپیده دمش خمار گرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کجاست سیم زمستان ، که خورد زرّ خریف؟</p></div>
<div class="m2"><p>کزین دوروی زمین پایۀ یسار گرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی بخاک فروشد یکی بباد برفت</p></div>
<div class="m2"><p>خنک کسی که ازین حال اعتبار گرفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهان بریشم ساعات روز و شب باهم</p></div>
<div class="m2"><p>باخت خوش خوش و چنگ در کنار گرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو دید خسرو سیّارگان که کار جهان</p></div>
<div class="m2"><p>بجملگی همه بر رکن دین قرار گرفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برسم خدمت او از برای نوروزی</p></div>
<div class="m2"><p>بدست خود بره را گردن استوار گرفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شبانی رمۀ خواجه را بفصل ربیع</p></div>
<div class="m2"><p>ز یک دو سر بره و گاه سازگار گرفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>منجّم ار چه ز تقویم هفت سیّاره</p></div>
<div class="m2"><p>حساب نیک و بد دور روزگار گرفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو رای خواجه بدید و کمال تدبیرش</p></div>
<div class="m2"><p>مدبّران فلک هشت در شمار گرفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگاه کرد قضا در حساب هیلاجش</p></div>
<div class="m2"><p>از آنچه بود مقّدر یکی هزار گرفت</p></div></div>