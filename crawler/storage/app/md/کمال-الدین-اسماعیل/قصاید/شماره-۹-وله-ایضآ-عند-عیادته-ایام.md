---
title: >-
    شمارهٔ ۹ - وله ایضآ عند عیادته ایّام
---
# شمارهٔ ۹ - وله ایضآ عند عیادته ایّام

<div class="b" id="bn1"><div class="m1"><p>زهی دیدار تو فال سعادت</p></div>
<div class="m2"><p>ترا می زبید آیین سیادت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه اقبال تو توحید و سنّت</p></div>
<div class="m2"><p>همه افعال تو عدل و عبادت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرای شرع را از تو عمارت</p></div>
<div class="m2"><p>بنای فضل را از تو اشادت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب و روز تو مستغرق بخیرات</p></div>
<div class="m2"><p>گه و بیگاه تو علم و افادت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو اندر یافتی کار من ارنی</p></div>
<div class="m2"><p>چنان بودم چنان دور از سعادت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که جانم غوطۀ تسلیم می خورد</p></div>
<div class="m2"><p>میان عالم غیب و شهادت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روان و قالب من بی علاقت</p></div>
<div class="m2"><p>سکون و جنبش من بی ارادت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حواس از شغل آنها گشته معزول</p></div>
<div class="m2"><p>معطّل مانده در کنج بلادت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تخییلات گوناگون دماغم</p></div>
<div class="m2"><p>چو مرفوعات دیوان عمادت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سکون مستولی از اطراف بر تن</p></div>
<div class="m2"><p>ولکن اضطراب دل زیادت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حیات از صحبت جان در تبرّم</p></div>
<div class="m2"><p>قوی از یکدگر در استزادت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نفس آمد شدی میکرد گه گاه</p></div>
<div class="m2"><p>بکوی زندگی با صد نکادت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>علل بر هم زده قانون صحّت</p></div>
<div class="m2"><p>همه باطل شده اوضاع عادت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه چشم از رنگ می دید استراحت</p></div>
<div class="m2"><p>نه مغز از بوی میکرد استفادت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه هیچ اندر دهانم می نهادند</p></div>
<div class="m2"><p>ز نومیدی بجز لفظ شهادت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طبیب از کار من عاجز شد ارچه</p></div>
<div class="m2"><p>بکار آورد انواع جلادت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز یأسم کار تا آنجا رسیده</p></div>
<div class="m2"><p>که میکردند یاسین استعادت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قوی رازهره از بیم آب می گشت</p></div>
<div class="m2"><p>بوقت کار زار طبع و مادت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وجودم چشم بسته بر سر پای</p></div>
<div class="m2"><p>بر آهخته اجل تیغ ابادت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زناگه در رسید آواز راحت</p></div>
<div class="m2"><p>که دادت خواجه تشریف عیادت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از آن یک انتعاشم گشت معلوم</p></div>
<div class="m2"><p>که روز حشر چون باشد اعادت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنان دیدم که اندر عالم کون</p></div>
<div class="m2"><p>مرا آن لحظه بد وقت ولایت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دم جان بخش او جانی نوم داد</p></div>
<div class="m2"><p>که بادش عمر و دولت بر زیادت</p></div></div>