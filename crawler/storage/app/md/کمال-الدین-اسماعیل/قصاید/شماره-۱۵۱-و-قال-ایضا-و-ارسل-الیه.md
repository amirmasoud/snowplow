---
title: >-
    شمارهٔ ۱۵۱ - و قال ایضا و ارسل الیه
---
# شمارهٔ ۱۵۱ - و قال ایضا و ارسل الیه

<div class="b" id="bn1"><div class="m1"><p>سلام علیک ای بزرگ جهان</p></div>
<div class="m2"><p>سلامی ز خورشید و سایه نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلامی نه برپشت باد هوا</p></div>
<div class="m2"><p>سلامی نه بر دست گوش و زبان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلامی چو دوشیزگان بهشت</p></div>
<div class="m2"><p>کشیده تن از صحبت انس وجان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلامی که نبود بر اطراف او</p></div>
<div class="m2"><p>ز صوت و حروف تقطّع نشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلامی منزّه حواشی او</p></div>
<div class="m2"><p>ز آلایش نقش کلک و بنان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلامی که بر قصر ادراک او</p></div>
<div class="m2"><p>نیفکند فکرت کمند گمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلامی که در جلوه گاه ظهور</p></div>
<div class="m2"><p>ندارد گذر بر مضیق دهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلامی که گر در ره او نفس</p></div>
<div class="m2"><p>بجنبد، ز غیرت بتابد عنان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلامی که در خلوت عصمتش</p></div>
<div class="m2"><p>نخواهم که باشم من اندر میان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلامی نه کورا سیه کرده روی</p></div>
<div class="m2"><p>نمایند رسوا به ببینندگان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سلامی نه کورا بدست قلم</p></div>
<div class="m2"><p>برآرند در شهر گیسوکشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سلامی نوشته بخطّ خدای</p></div>
<div class="m2"><p>که او را نباشد قلم ترجمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قلم دو زبانست و کاغذ دوروی</p></div>
<div class="m2"><p>نباشند محرم درین سو زیان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سلامی که تنگ آید از موکبش</p></div>
<div class="m2"><p>فضای زمان و حدود مکان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سلامی که شوقش ز سوز نیاز</p></div>
<div class="m2"><p>رساند بسمع دل از مغز جان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سلامی که بی زحمت گفت و گوی</p></div>
<div class="m2"><p>بسمع مبارک رسد هر زمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سلامی نهان از دهان جهان</p></div>
<div class="m2"><p>سلامی روان از روان تا روان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سلامی شب قدر تا روز حشر</p></div>
<div class="m2"><p>بهندویی او ببسته میان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سلامی کزو دل برد زندگی</p></div>
<div class="m2"><p>سلامی کزو جان شود شادمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سلامی جنیبت کش باد صبح</p></div>
<div class="m2"><p>سلامی سراپردۀ گلستان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سلامی که از وی حکایت کند</p></div>
<div class="m2"><p>باواز خوش در چمن زند خوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سلامی پر از سوسنش آستین</p></div>
<div class="m2"><p>سلامی پر از عنبرش بادبان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سلامی چو اخلاق تو مشک بوی</p></div>
<div class="m2"><p>سلامی چو الفاظ تو درفشان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سلامی چو فضل تو نامنتهی</p></div>
<div class="m2"><p>سلامی چو انعام تو بی کران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سلامی چو طبع تو با اهل فضل</p></div>
<div class="m2"><p>سلامی چو خلق تو با این و آن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سلامی چو در مدح تو نظم من</p></div>
<div class="m2"><p>سلامی چو لفظ تو گاه بیان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سلامی هزاران دعاو ثنا</p></div>
<div class="m2"><p>شده در رکابش بحضرت روان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برآن طلعت و فرّه ایزدی</p></div>
<div class="m2"><p>برآن خاطر و فکرت غیب دان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برآن روی ورای و برآن عزم و حزم</p></div>
<div class="m2"><p>برآن فرّو زیب و برآن شکل و سان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر آن قد و بالا که براخمصش</p></div>
<div class="m2"><p>بود بوسه جای لب فرقدان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بران رای روشن که خورشید از او</p></div>
<div class="m2"><p>سیه روی چون سایه شد جاودان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برآن حلم ثابت که در جنب اوست</p></div>
<div class="m2"><p>سبک سارو بی سنگ کوه گران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برآن عزم قاطع که گاه نفوذ</p></div>
<div class="m2"><p>درخشیست از گوهر کن فکان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بران دست بخشنده کز فرط جود</p></div>
<div class="m2"><p>شد از دست او چون کف دست کان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بران کلک جادو که سیراب کرد</p></div>
<div class="m2"><p>به آب دهان روضه های جنان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بران طبع موزون که تعدیل یافت</p></div>
<div class="m2"><p>ز لطفش سهی سرو در بوستان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زهی عرضه داده سر کلک تو</p></div>
<div class="m2"><p>بیک نکته اندر علوم جهان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ازآن پایه بگذشته یی در کمال</p></div>
<div class="m2"><p>که مدّاح گوید چنین و چنان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کجا پای دست تو دارد سحاب ؟</p></div>
<div class="m2"><p>و گر خود کشد سر سوی آسمان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز عدل توممکن که شهپّر باز</p></div>
<div class="m2"><p>شود بچۀ کبک را سایه بان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز سهم تو زدا که بیرون نهد</p></div>
<div class="m2"><p>کژی رخت از خانه های کمان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو دندان نماید سر کلک تو</p></div>
<div class="m2"><p>شهادت بگوید زبان سنان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز صوب ایادّی تو می رسد</p></div>
<div class="m2"><p>بشهر امل کاروان کاروان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو مدح تو خوانند در خانه یی</p></div>
<div class="m2"><p>درآن خانه دولت کند آشیان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو برخاک پای تو مالند روی</p></div>
<div class="m2"><p>برآن روی آتش شود مهربان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>صبا را دو خاصیّت عیسویست</p></div>
<div class="m2"><p>چو جنبان شود زان بلند آستان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یکی آنکه زنده کند مرده را</p></div>
<div class="m2"><p>چو با لفظ تو کرده باشد قران</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دوم آنکه روشن کند چشم کور</p></div>
<div class="m2"><p>چو سازد ز خاک درت سرمه دان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ایا صدر اسلام وپشت هنر</p></div>
<div class="m2"><p>امام جهان شافعّی الزمان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تویی تو که نام هنر می بری</p></div>
<div class="m2"><p>درین باتوکس نیست همداستان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>منم از بقایای اهل هنر</p></div>
<div class="m2"><p>اگر باورت نیست رو بازدان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اگر بخت را بویی آید ز من</p></div>
<div class="m2"><p>خود اندازدم سوی آن خاکدان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بمدح تو روشن کنم جان چو شمع</p></div>
<div class="m2"><p>وگر خود نهد آتشم در زبان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کنم جای سودای تو در دماغ</p></div>
<div class="m2"><p>چو کلک ار رسد تیغ بر استخوان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>و گر آستین گیردم بخت بد</p></div>
<div class="m2"><p>تو از من درودی بدانش رسان</p></div></div>