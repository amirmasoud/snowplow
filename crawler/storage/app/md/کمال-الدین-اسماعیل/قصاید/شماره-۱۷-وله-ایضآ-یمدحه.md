---
title: >-
    شمارهٔ ۱۷ - وله ایضآ یمدحه
---
# شمارهٔ ۱۷ - وله ایضآ یمدحه

<div class="b" id="bn1"><div class="m1"><p>برتافتست بخت مرا روزگار دست</p></div>
<div class="m2"><p>زانم نمی رسد بسر زلف یار دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر بر نیاورد فلک از دست دست من</p></div>
<div class="m2"><p>با یار اگر شبی کنم اندر کنار دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرم برون زهر شکنش صد هزار دل</p></div>
<div class="m2"><p>گر در شود مرا بدو زلف نگار دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبر و جوانی و دل و جان بود در غمش</p></div>
<div class="m2"><p>شستم بآب دیده ازین هر چهار دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دمّ مار پای نهادست بیگمان</p></div>
<div class="m2"><p>هر کس که زد در آن سر زلف چو ماردست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم دست نیک میدهد از هر طرف و لیک</p></div>
<div class="m2"><p>اینم بترکه می ندهد غمگسار دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون آستین ز دست گذشتست کار من</p></div>
<div class="m2"><p>و او در نمی کشد ز چنین دستکار دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دل گرت بعافیتی دسترس بود</p></div>
<div class="m2"><p>کوته مکن ز دامن او زینهار دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سربازیست کار تو با دست بازیش</p></div>
<div class="m2"><p>چون پای او نداری رو زو بدار دست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر جه یکی ز چاه ز نخدانش مرد وار</p></div>
<div class="m2"><p>در زن بدان دو تا رسن مشکبار دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایدست رنگ کرده چه دستت! این که باز</p></div>
<div class="m2"><p>آلوده یی بخون دلم آشکار دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در خون عاشقان تو سعی ار نمی کند</p></div>
<div class="m2"><p>بهر چراست بسته کمر از سوار دست؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیکان تیر غمزۀ تو در دل منست</p></div>
<div class="m2"><p>ورنیست باورت ز من اینک بیاردست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طوطیّ عقل در هوس شکَر لبت</p></div>
<div class="m2"><p>بر سر همی زند چو مگس زاز زار دست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نامد بدست وصل تو بی زحمت فراق</p></div>
<div class="m2"><p>بر کل کسی نیابد بی زخم خار دست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لعل ترا شبی ببسودم من و هنوز</p></div>
<div class="m2"><p>می لیسم از حلاوت آن گربه واردست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از ارزوی سلسلۀ زلف تست اینک</p></div>
<div class="m2"><p>دیوانه وار گردد بر نی سوار دست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در آرزوی روی تو دارم چو اینه</p></div>
<div class="m2"><p>دایم ستون بزیر ز نخ ز انتظار دست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون در در آب جویند این مهرۀ گلین</p></div>
<div class="m2"><p>گر باز دارم از مژۀ اشکبار دست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پای از میان کار غمت آورم برون</p></div>
<div class="m2"><p>گر گیردم عنایت صدر کبار دست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سلطان شرع صاعد کانگام حلّ و عقد</p></div>
<div class="m2"><p>بر بند آسمانرا از اقتدار دست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گشتست پنج شاخ سر دست بهر آنک</p></div>
<div class="m2"><p>ز اسیب بار بخشش او شد فکار دست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در زان سبب یتیم نهادست نام خود</p></div>
<div class="m2"><p>تاوی درآورد بسرش خوار خوار دست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کردست دستیاری ظالم بعهد او</p></div>
<div class="m2"><p>در خام از آن گرفته بود بازیار دست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مستظهر است دست شریعت بذات او</p></div>
<div class="m2"><p>زان سینه می کند ز پی افتخار دست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای مانده زیر سنگ وقار تو دست کوه</p></div>
<div class="m2"><p>وی یافته شکوه تو برنه حصار دست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برداریش ز خاک و رسانیش بر فلک</p></div>
<div class="m2"><p>هر کو بدامن تو زند چون غبار دست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر جان آدمی نه بدست قضا درست</p></div>
<div class="m2"><p>از بهر چیست جای تو ای نامدار دست؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون آستین زیمن تو صاحب علم شود</p></div>
<div class="m2"><p>هر کس که بوسه داد ترا یک دوبار دست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زور آزمای خشم تو چون پای بفشرد</p></div>
<div class="m2"><p>یا زد بقهر در کمر کوهسار دست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بستست دست خصم ز امساک و مر ترا</p></div>
<div class="m2"><p>از جود مطلق است در این روزگار دست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از روی آنکه از پس پشتش فکنده یی</p></div>
<div class="m2"><p>دایم چو دشمن تو بود سوگوار دست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون بالش تو دست تو در صدر چرخ را</p></div>
<div class="m2"><p>بر هم نهد پیش درت بنده وار دست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آنجا که هست دست تو در صدر چرخ را</p></div>
<div class="m2"><p>دربان بسینه باز نهد روزبار دست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر هیبت تو باطشه را بانگ برزند</p></div>
<div class="m2"><p>چون سرو باز داردش از گیر و دار دست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>حالی بسر درآید انگشتها ز عجز</p></div>
<div class="m2"><p>از بار بخشش تو چو گیرد شمار دست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کان کیست باسخای تو تا هست دست تو</p></div>
<div class="m2"><p>خود کس نبرد نزد چنان خاکسار دست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>احرار دهر ملک یمین تواند از آنک</p></div>
<div class="m2"><p>بر همگنانت هست ز جود و یسار دست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خورشید دولتیّ و بفّر تو زر شود</p></div>
<div class="m2"><p>گر فی المثل بری بسوی خاک خوار دست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون کوزه سر نیافت بگردن براز نهیب</p></div>
<div class="m2"><p>بر سر چو زد حسود تو از اضطراب دست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>با دست دستگاهش چندانکه گرد خویش</p></div>
<div class="m2"><p>خصم تو می برآرد همچون چنار دست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مبسوط دست خصم تو چندان بود که او</p></div>
<div class="m2"><p>بهر سوال دارد بر رهگذار دست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در زر گرفت باد خزان دست شاخسار</p></div>
<div class="m2"><p>زیرا که داشت بهر تو برکردگار دست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پهلو ز تو هر آنکه تهی کرد چون چنار</p></div>
<div class="m2"><p>بر پیش و پس گرفته بود ز افتقار دست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وانکو برهنه پیش سخایت رود چو کاج</p></div>
<div class="m2"><p>حالی چو سرو جامه کند از هزار دست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر خاطرم نهادی دستی ز مکر مت</p></div>
<div class="m2"><p>ورنه بشسته بودم از این کار و بار دست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سر دستی است شعر من ایراکه می نداد</p></div>
<div class="m2"><p>ابکار فکر بر حسب اختیار دست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بهر قبول بخشش بی انتهای تو</p></div>
<div class="m2"><p>بنگر چگونه داشته ام بر قطار دست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آورده ام بدست وبر آورده ام ز دست</p></div>
<div class="m2"><p>شعری که یافت بر گهر شاهوار دست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دوشیزگان خاطر من بین که غنچه وار</p></div>
<div class="m2"><p>بر رخ گرفته اند ز تو شرمسار دست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هستم هزار دستان در باغ مدحتت</p></div>
<div class="m2"><p>کز هگمتان ببردم در این دیار دست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مرغی که در خزانش از این دست لحنهاست</p></div>
<div class="m2"><p>خود چون بود چو تازه کند نو بهار دست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بر دست از آن نهادم این شعر چون نگار</p></div>
<div class="m2"><p>کایّام عید خوب بود درنگار دست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خصم شتر دلت را قربان کند همی</p></div>
<div class="m2"><p>زین روی سعد ذابح آهخته کار دست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جاوید زی که مملکت پایدار تو</p></div>
<div class="m2"><p>در دامن قیامت زد استوار دست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هم عهد خود شدند بقای تو و ابد</p></div>
<div class="m2"><p>وانگه زدند بر هم بر این قرار دست</p></div></div>