---
title: >-
    شمارهٔ ۷ - و قال  ایضآ یمدح الصّدر المعظّم فخر الدّین
---
# شمارهٔ ۷ - و قال  ایضآ یمدح الصّدر المعظّم فخر الدّین

<div class="b" id="bn1"><div class="m1"><p>روز عیدست بده جام شراب</p></div>
<div class="m2"><p>وقت کارست ، چه داری؟ دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مغزم از بانگ دهل کوفته شد</p></div>
<div class="m2"><p>مرهمش نالۀ چنگست و رباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدّتی شد که دهان بربستم</p></div>
<div class="m2"><p>همچو غنچه زشراب و زکباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت آنست که همچون نرگس</p></div>
<div class="m2"><p>بر نداریم سر از مستی و خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار دیگر بزه اندوز شویم</p></div>
<div class="m2"><p>که نمی آید ما را ز ثواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفت آن دور که دوران فلک</p></div>
<div class="m2"><p>هرزه می داشت دلم را بعذاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این زمان گر بچخد با دل من</p></div>
<div class="m2"><p>بدو ساغر دهمش باز جواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین سپس دست من و ساغر می</p></div>
<div class="m2"><p>پس ازین کام من و باده ناب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کجا شربتی از می بینم</p></div>
<div class="m2"><p>بر سرش خیمه زنم همچو حباب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیک امشب همه اسباب جهان</p></div>
<div class="m2"><p>عکس مطلق شده است از هر باب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه دی آب نمی خورد نهان</p></div>
<div class="m2"><p>آشکارا خورد امروز شراب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و آنکه دی معتکف مسجد بود</p></div>
<div class="m2"><p>در خرابات فتادست خراب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آبگینه که پیاله ست امروز</p></div>
<div class="m2"><p>دوش قندیل بد اندر محراب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سرده بزم شرابست امروز</p></div>
<div class="m2"><p>آنکه دی بود امام اصحاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گیرو دار قدحست ای ساقی</p></div>
<div class="m2"><p>هان و هان! موسم شادی دریاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن نشاطی گهر گلگون را</p></div>
<div class="m2"><p>که فتادست ز تیری درتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خیزو در عرصۀ میدان آرش</p></div>
<div class="m2"><p>تا بگردد که چنین است صواب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پرده از دختر رز بردارید</p></div>
<div class="m2"><p>که نمی زیبدش این سترو حجاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می که در روزه ز تو فایت شد</p></div>
<div class="m2"><p>بقضا باز خور اکنون بشتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در ده آن جام می گلناری</p></div>
<div class="m2"><p>کش بود رنگ گل و بوی گلاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خاک در چشم غم انداز چو باد</p></div>
<div class="m2"><p>ز آتشی ساخته از آب نقاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عقل با این همه نا حفظی عیش</p></div>
<div class="m2"><p>در دهان آرد ازین آتش آب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بادۀ همچو زر سرخ کزو</p></div>
<div class="m2"><p>بگریزد غم دل چون سیماب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دست در هم زده کف بر سر او</p></div>
<div class="m2"><p>همچو مرجان زبر لعل مذاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از پیاله شده رخشنده چنانک</p></div>
<div class="m2"><p>آفتابی ز میان مهتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>طرب انگیز و لطیف و روشن</p></div>
<div class="m2"><p>چون رخ صاحب فرخنده جناب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صاحب عالم عادل که ببرد</p></div>
<div class="m2"><p>سخنش آب همه درّ خوشاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنکه تا دولت بیدار بدست</p></div>
<div class="m2"><p>مثل او خواجه ندیدست بخواب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نزد اوج شرفش چرخ نژند</p></div>
<div class="m2"><p>پیش فیض کرمش نیل سراب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آنکه با هیبت او نخراشد</p></div>
<div class="m2"><p>نای حلقه بره را چنگ ذیاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای شده مدحت تو ورد زبان</p></div>
<div class="m2"><p>وی شده منّت تو طوق رقاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مایۀ حلم تو در جان رقیب</p></div>
<div class="m2"><p>سرعت عزم تو در عهد شباب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چشمۀ آب کرم را اومید</p></div>
<div class="m2"><p>دیده از چاه دولت تو زهاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صاحب ار زنده شود بر در تو</p></div>
<div class="m2"><p>باشد او نیز یکی از اصحاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زیر دست تو کرم همچو عنان</p></div>
<div class="m2"><p>پای بوس تو فلک همچو رکاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پرتو رای تو دیدست از آن</p></div>
<div class="m2"><p>پشت بر مهر کند اصطرلاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همّت عالی تو دریاییست</p></div>
<div class="m2"><p>که ندیدست سپهرش پایاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تیر چرخ ار نبود مادح تو</p></div>
<div class="m2"><p>چرخ از خود کند او را پرتاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سرخ رویست حسودت زیراک</p></div>
<div class="m2"><p>بر رخ از خون جگر کرد خضاب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زحل آن روز شود مقبل نام</p></div>
<div class="m2"><p>کش کنی هندوک خویش خطاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر که چون پسته زبان بر تو گشاد</p></div>
<div class="m2"><p>سرخ روی آید همچون عنّاب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر کجا سیم دهی وقت عطا</p></div>
<div class="m2"><p>باشدش بر سر انگشت حساب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تویی آنکس که بهنگام سخا</p></div>
<div class="m2"><p>بودت در سر انگشت سحاب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>احتشام تو و لله الحمد</p></div>
<div class="m2"><p>نیست محتاج بحصر القاب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فخر دین ابن نظام الدّین بس</p></div>
<div class="m2"><p>بیش ازین شرط نباشد اطناب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه زند پهلو با دست تو بحر</p></div>
<div class="m2"><p>می نترسد که سخایت بعتاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ناگهان خاک از او برگیرد</p></div>
<div class="m2"><p>وانگهی ناید ازو آب بآب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چون بدریای ثنای تو رسد</p></div>
<div class="m2"><p>کشتی و هم فتد در غرقاب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سپری هم نشود مدحت تو</p></div>
<div class="m2"><p>ور بسازند دو صد باره کتاب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا که اسباب جهان ساخته است</p></div>
<div class="m2"><p>در جهان ساخته بادت اسباب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خیمۀ دولت و اقبال ترا</p></div>
<div class="m2"><p>در مسامیر ابد بسته طناب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رای تو در همه اندیشه مصیب</p></div>
<div class="m2"><p>خصم تو در همه احوال مصاب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عید فرخنده بشادی گذران</p></div>
<div class="m2"><p>در جهان هر چه مرادست بیاب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>لبت اندر لب جام گلگون</p></div>
<div class="m2"><p>دستت اندر کمر زلف بتاب</p></div></div>