---
title: >-
    شمارهٔ ۳۶ - وله یمدحه ببلد الری و بهنیّه بالنیروز.!
---
# شمارهٔ ۳۶ - وله یمدحه ببلد الری و بهنیّه بالنیروز.!

<div class="b" id="bn1"><div class="m1"><p>بزرگوارا روزت همیشه نوروزست</p></div>
<div class="m2"><p>چو وقت گل همه اوقات عمر تو خوش باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدامن تو هر آنکو گلی فشاند بقصد</p></div>
<div class="m2"><p>بسان گل همه عمرش زخار مفرش باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو لاله هر که برت رخ نمی نهد بر خاک</p></div>
<div class="m2"><p>گر آب صرف خورد در مزاجش آتش باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا چو سر و درین روزگار آزادیست</p></div>
<div class="m2"><p>ببندگیّ تو استاده، دست بر کش باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شاه خلق تو عرض سپاه لطف دهد</p></div>
<div class="m2"><p>سلاح دارش سوسن، گلش سپرکش باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بقصد مذهب نعمان هر آنکه سعی کند</p></div>
<div class="m2"><p>ز باد قهر تو چون لاله دل مشوّش باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسود بدرگ اگر پرده کژ دهد با تو</p></div>
<div class="m2"><p>چنان بریشم ناساز در کشاکش باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بران طویله که جاه عریض تو بکشند</p></div>
<div class="m2"><p>کمینه لاغری آن سپهر ابرش باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بقصد جان عدو چون کمان کینه کشی</p></div>
<div class="m2"><p>مسیر عزم تو پرتاب تیر آرش باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوی مصاعد رفعت که نسر واقع شد</p></div>
<div class="m2"><p>همای رایت قدر تو مرغ مرعش باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زکعبتین شب و روز در سکّرۀ چرخ</p></div>
<div class="m2"><p>چو تاج نرگس نقش مقاصدت شش باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو نیست لایق قربان جاه تو خصمت</p></div>
<div class="m2"><p>ز تیر حادثه باری دلش چو ترکش باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سلیل سلب تو یک دانۀ قلادۀ مجد</p></div>
<div class="m2"><p>که جان جانها برخی آن پری وش باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر چه دامن کوهست جای پروردشش</p></div>
<div class="m2"><p>چو لاله از دم لطف تو خرّم وکش باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برای نازکی پای سایه پروردش</p></div>
<div class="m2"><p>بساط کوه که خار است اطلس ورش باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کسی که دست سیه جز بخانه ات خواهد</p></div>
<div class="m2"><p>به پنجه های سیه خانه اش منقّش باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به باز همّت عالی اگر بپیمایی</p></div>
<div class="m2"><p>چهار طاق فلک جمله کم ز یک رش باد</p></div></div>