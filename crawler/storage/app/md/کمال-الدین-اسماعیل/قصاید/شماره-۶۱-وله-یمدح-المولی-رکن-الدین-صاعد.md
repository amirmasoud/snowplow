---
title: >-
    شمارهٔ ۶۱ - وله یمدح المولی رکن الدّین صاعد
---
# شمارهٔ ۶۱ - وله یمدح المولی رکن الدّین صاعد

<div class="b" id="bn1"><div class="m1"><p>ای که از درّ درج مدحت تو</p></div>
<div class="m2"><p>عقد بر گردن جهان بستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بارگاه ترا قضا و قدر</p></div>
<div class="m2"><p>از نهم چرخ سایبان بستند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ را بر درت به میخ نیاز</p></div>
<div class="m2"><p>همچو شفشه بر آستان بستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر عروسان نطق عقد گهر</p></div>
<div class="m2"><p>زان سر کلک درفشان بستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تف خاطرت زخیط الشّمس</p></div>
<div class="m2"><p>تب گردوه بریسمان بستند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخ چون جلوه گاه قدر تو شد</p></div>
<div class="m2"><p>تتقی از شفق برآن بستند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دو دست تو کان دو بحر آمد</p></div>
<div class="m2"><p>کان و دریا در دکان بستند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقشبندان فکر مدح ترا</p></div>
<div class="m2"><p>برفراز طراز جان بستند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مسرعان ولایت علوی</p></div>
<div class="m2"><p>در سر کلک تو عنان بستند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوشه چینان خرمن ملکوت</p></div>
<div class="m2"><p>طرف از آن کلک غیب دان بستند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پی جلوه گاه دیدارت</p></div>
<div class="m2"><p>کلّۀ سبز آسمان بستند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مهر مهر ترهر دهان که شکست</p></div>
<div class="m2"><p>میخ دندان بر آن دهان بستند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جز بمدحت کسی زبان نگشاد</p></div>
<div class="m2"><p>که نه چون پسته اش زبان بستند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>انجم از بیم آتش قهرت</p></div>
<div class="m2"><p>آب در راه کهکشان بستند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از نهیب نقابی از شب و روز</p></div>
<div class="m2"><p>بر رخ گردش زمان بستند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چرخ و انجم ز شوق حضرت تو</p></div>
<div class="m2"><p>جان کمروار بر میان بستند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دشمنانت ندانم از چه سبب</p></div>
<div class="m2"><p>کین تو در دل و روان بستند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بهر دفع خیال تیغ تو آب</p></div>
<div class="m2"><p>در حوالی دیدگان بستند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می ندانند کاخر از چه سبب</p></div>
<div class="m2"><p>بند بر پای آن جوان بستند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سرفرازا بخدمت آوردم</p></div>
<div class="m2"><p>حسب حالی ردیف آن بستند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کم از آن قطعه نیست اینکه ازو</p></div>
<div class="m2"><p>های و هویی در اصفهای بستند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سرفرازا منجّمان بدروغ</p></div>
<div class="m2"><p>تهمتی بر ستارگان بستند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اثر اندر حسود پیدا کرد</p></div>
<div class="m2"><p>آن سخن ها بر قران بستند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برد آنرا که بردنی بد باد</p></div>
<div class="m2"><p>گر ز طوفان برو گمان بستند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا که گویند بهر مقدم گل</p></div>
<div class="m2"><p>کلّه از شاخ ارغوان بستند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جاودان زی که دولت و عمرت</p></div>
<div class="m2"><p>با ابد عهد جاودان بستند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بهر قربان عید خصم ترا</p></div>
<div class="m2"><p>اندرین کنج خاکدان بستند</p></div></div>