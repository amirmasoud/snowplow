---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>یارآمد و دست من آشفته گرفت</p></div>
<div class="m2"><p>وز من گله های گفته ناگفته گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین دولت بیدار عجب ماندم نیک</p></div>
<div class="m2"><p>کو بخت بد مرا چنین خفته گرفت</p></div></div>