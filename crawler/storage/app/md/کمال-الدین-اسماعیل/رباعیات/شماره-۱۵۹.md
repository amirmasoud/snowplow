---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>نقّاش که آن صورت زیبا بنگاشت</p></div>
<div class="m2"><p>یارب چه بقد آن قد و بالا بنگاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز خط خوشت نیز چگویم کانصاف</p></div>
<div class="m2"><p>نتوان بقلم چنان خطی را بنگاشت</p></div></div>