---
title: >-
    شمارهٔ ۷۸۵
---
# شمارهٔ ۷۸۵

<div class="b" id="bn1"><div class="m1"><p>امشب که نگارم از برای مستی</p></div>
<div class="m2"><p>آمد برم از سر هوای مستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستی بر ما فکند هم مستی نیز</p></div>
<div class="m2"><p>خیزم و درافتیم بپای مستی</p></div></div>