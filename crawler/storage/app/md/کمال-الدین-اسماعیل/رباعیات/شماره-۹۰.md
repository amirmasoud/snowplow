---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>در عمر مرا با تو شبی خوش بودست</p></div>
<div class="m2"><p>زان وقت هنوز چشم من نغنودست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باری بنده را طبیبی دانا</p></div>
<div class="m2"><p>در خدمت تو شبی دگر فرمودست</p></div></div>