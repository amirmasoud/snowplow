---
title: >-
    شمارهٔ ۶۸۰
---
# شمارهٔ ۶۸۰

<div class="b" id="bn1"><div class="m1"><p>ای رسم تو در ناکس و کس پیوستن</p></div>
<div class="m2"><p>عهدی داری بعهد ها بشکستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرمت ناید بقصد جان چو منی</p></div>
<div class="m2"><p>بر خاستن و بادگری تنشتن</p></div></div>