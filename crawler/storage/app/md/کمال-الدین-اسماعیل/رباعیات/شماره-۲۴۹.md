---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>شب گر چه ز روشنی جدایی دارد</p></div>
<div class="m2"><p>او را دو طرف بروشنایی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانه ز حال دل منم ورنه دلم</p></div>
<div class="m2"><p>دیریست که با تو آشنایی دارد</p></div></div>