---
title: >-
    شمارهٔ ۶۲۱
---
# شمارهٔ ۶۲۱

<div class="b" id="bn1"><div class="m1"><p>در عشق تو گر چه شهره ام در عالم</p></div>
<div class="m2"><p>زان روی که تا نهفته ماند حالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانند گل دو رنگ بر چهرۀ زرد</p></div>
<div class="m2"><p>گلگونه یی از خون جگر می مالم</p></div></div>