---
title: >-
    شمارهٔ ۶۶۱
---
# شمارهٔ ۶۶۱

<div class="b" id="bn1"><div class="m1"><p>گر با تو بنای وصل آغاز نهیم</p></div>
<div class="m2"><p>روی از غم دل بنعمت و ناز نهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون پرگار این همه سرگردانی</p></div>
<div class="m2"><p>چندان باشد که سر بسر بازنهیم</p></div></div>