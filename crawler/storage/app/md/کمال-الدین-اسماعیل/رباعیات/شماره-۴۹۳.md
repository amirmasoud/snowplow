---
title: >-
    شمارهٔ ۴۹۳
---
# شمارهٔ ۴۹۳

<div class="b" id="bn1"><div class="m1"><p>زلف تو که نیست در درازی همتاش</p></div>
<div class="m2"><p>بگذشت ز حد سیاه کاری باماش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد سر آن که سر زمن بر تابد</p></div>
<div class="m2"><p>ور چون قد تو سیم کنم هم بالایش</p></div></div>