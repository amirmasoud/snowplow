---
title: >-
    شمارهٔ ۱۳۳
---
# شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>تا ظن نبری که در نکوئیت شکیست</p></div>
<div class="m2"><p>یا چون رخ تو ستاره یی بر فلکیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بی آبیّ و شوخی و تیغ زدن</p></div>
<div class="m2"><p>خورشید سپهر و چشم تو هر دو یکیست</p></div></div>