---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>در حضرت عشق هر که او مقبولست</p></div>
<div class="m2"><p>هر آرزویی که او کند مبذولست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دل که نه او بعاشقی مشغولست</p></div>
<div class="m2"><p>در عالم جان او ز دلی معزولست</p></div></div>