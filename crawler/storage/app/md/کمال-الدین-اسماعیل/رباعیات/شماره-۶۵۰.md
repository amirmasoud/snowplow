---
title: >-
    شمارهٔ ۶۵۰
---
# شمارهٔ ۶۵۰

<div class="b" id="bn1"><div class="m1"><p>دیشب من و صبح از غمت دم نزدیم</p></div>
<div class="m2"><p>کمتر از یکی آه بود هم نزدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودیم همه شب من و اختر تا روز</p></div>
<div class="m2"><p>درهم زده چشم و چشم برهم نزدیم</p></div></div>