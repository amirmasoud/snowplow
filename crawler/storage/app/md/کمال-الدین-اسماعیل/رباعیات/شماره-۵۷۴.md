---
title: >-
    شمارهٔ ۵۷۴
---
# شمارهٔ ۵۷۴

<div class="b" id="bn1"><div class="m1"><p>لب باز مگیر یک زمان از لب جام</p></div>
<div class="m2"><p>تا برداری کام جهان از لب جام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جام جهان چو تلخ و شیرین بهمست</p></div>
<div class="m2"><p>این از لب یار خواه و آن از لب جام</p></div></div>