---
title: >-
    شمارهٔ ۲۹۳
---
# شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>آنرا که دل از غمی مشوّش باشد</p></div>
<div class="m2"><p>باد سحرش آب بر اتش باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوشم سحری باد زنو جانی داد</p></div>
<div class="m2"><p>بیمار که جان چنین دهد خوش باشد</p></div></div>