---
title: >-
    شمارهٔ ۵۸۱
---
# شمارهٔ ۵۸۱

<div class="b" id="bn1"><div class="m1"><p>بس جور که من ز دست جانان بردم</p></div>
<div class="m2"><p>بس دست که از غصّه بدندان بردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس غصّه که آشکار و پنهان بردم</p></div>
<div class="m2"><p>تا عمر عزیز را بپایان بردم</p></div></div>