---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>یک روز شبی چو شمع برخواهم خاست</p></div>
<div class="m2"><p>ورنیز زبانم ز کسان باید خواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا با تو کنم روشن و برگویم راست</p></div>
<div class="m2"><p>چون آتش و آب سرگذشتی که مراست</p></div></div>