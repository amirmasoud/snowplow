---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>فصّاد چو بررگ تو نشتر بگماشت</p></div>
<div class="m2"><p>خون در تن ازین تغابین نگذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون دید روان از رگ تو دل پنداشت</p></div>
<div class="m2"><p>کازرده شد آن رگی که با جانم داشت</p></div></div>