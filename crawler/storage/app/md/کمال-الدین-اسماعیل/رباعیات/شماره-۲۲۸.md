---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>چون دید بر آورده غم از جانم گرد</p></div>
<div class="m2"><p>بی فایده بسیار پشیمانی خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذشت و همی گریست، می گفت بدرد</p></div>
<div class="m2"><p>خوی بد من کار چنین داند کرد</p></div></div>