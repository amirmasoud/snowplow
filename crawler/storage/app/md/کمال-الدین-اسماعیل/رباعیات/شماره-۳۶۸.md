---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>ای دل اگرت چه دیده بس رنج نمود</p></div>
<div class="m2"><p>مگذار که او از تو بود ناخشنود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا که بدین سان که تویی، دیرازود</p></div>
<div class="m2"><p>روزی گذرت بر در او خواهد بود</p></div></div>