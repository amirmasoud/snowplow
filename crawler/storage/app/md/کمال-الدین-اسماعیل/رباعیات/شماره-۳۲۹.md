---
title: >-
    شمارهٔ ۳۲۹
---
# شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>بستان دلم،ارنه غم زمن بستاند</p></div>
<div class="m2"><p>ور من ندهم بدم زمن بستاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عشق تو خون من چنین خواهد ریخت</p></div>
<div class="m2"><p>سرمایۀ گریه هم زمن بستاند</p></div></div>