---
title: >-
    شمارهٔ ۵۶۴
---
# شمارهٔ ۵۶۴

<div class="b" id="bn1"><div class="m1"><p>ای دل همه جام عاشقی نوش چو گل</p></div>
<div class="m2"><p>پیوسته لباس عاشقی پوش چو گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پون شمع زبان آتشین دارد عشق</p></div>
<div class="m2"><p>زنهار مباش پنبه در گوش چو گل</p></div></div>