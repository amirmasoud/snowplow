---
title: >-
    شمارهٔ ۶۰۶
---
# شمارهٔ ۶۰۶

<div class="b" id="bn1"><div class="m1"><p>آنشب که ز بر آتش غم سوزم</p></div>
<div class="m2"><p>خشک و تر خود چو شمع در هم سوزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنها سوزم بگوشه یی در چو سپند</p></div>
<div class="m2"><p>چون شمع میان مردمان کم سوزم</p></div></div>