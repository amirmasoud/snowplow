---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>روز آمد و بردوختم از دم لب را</p></div>
<div class="m2"><p>پرداخته کردم از روان قالب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که مرا زنده همی دارد شب</p></div>
<div class="m2"><p>شاید که چو شمع زنده دارم شب را</p></div></div>