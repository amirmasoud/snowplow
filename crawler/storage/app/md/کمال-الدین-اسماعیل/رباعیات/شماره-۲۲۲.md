---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>از عکس لبت دیده بدخشان گردد</p></div>
<div class="m2"><p>وز یاد رخت سینه گلستان گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی روی توگر آب خورم چون گلبن</p></div>
<div class="m2"><p>اندر دل من چو غنچه پیکان گردد</p></div></div>