---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>ای دل چو رسید روز اومید بشب</p></div>
<div class="m2"><p>هیچت ناید زوصل آن شیرین لب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان پیش که تیره گرددت دیده ز اشک</p></div>
<div class="m2"><p>باری دگری بروشنایی بطلب</p></div></div>