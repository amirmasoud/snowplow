---
title: >-
    شمارهٔ ۶۰۹
---
# شمارهٔ ۶۰۹

<div class="b" id="bn1"><div class="m1"><p>پیوسته ز تو با دل پر خون باشم</p></div>
<div class="m2"><p>با چهرۀ زرد و اشک گلگون باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که دوبار بینمت، حال اینست</p></div>
<div class="m2"><p>آن روز که خود نبینمت چون باشم</p></div></div>