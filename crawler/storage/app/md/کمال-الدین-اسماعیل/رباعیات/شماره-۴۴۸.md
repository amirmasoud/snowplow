---
title: >-
    شمارهٔ ۴۴۸
---
# شمارهٔ ۴۴۸

<div class="b" id="bn1"><div class="m1"><p>ای از رسرشک باران از ابر</p></div>
<div class="m2"><p>باران هنر از تو چو باران از ابر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست تو آستین سایل همه سال</p></div>
<div class="m2"><p>چون دامن خیمه روز باران از ابر</p></div></div>