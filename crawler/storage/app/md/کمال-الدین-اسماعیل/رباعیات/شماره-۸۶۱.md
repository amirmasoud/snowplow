---
title: >-
    شمارهٔ ۸۶۱
---
# شمارهٔ ۸۶۱

<div class="b" id="bn1"><div class="m1"><p>در هر عمری دمی که دمساز آیی</p></div>
<div class="m2"><p>آن هم بدوصد کرشمه و نازآیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانگه که شوی رنجه نیایی بر من</p></div>
<div class="m2"><p>چندانکه بپرسمت که کی بازآیی؟</p></div></div>