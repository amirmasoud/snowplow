---
title: >-
    شمارهٔ ۴۵۹
---
# شمارهٔ ۴۵۹

<div class="b" id="bn1"><div class="m1"><p>آمد گل و آورد به پیراهن زر</p></div>
<div class="m2"><p>بنشست به باغ و کرد خرمن زر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعنی که بشادی نتوان برد بسر</p></div>
<div class="m2"><p>یک روزه حیات جز بیک دامن زر</p></div></div>