---
title: >-
    شمارهٔ ۷۸۹
---
# شمارهٔ ۷۸۹

<div class="b" id="bn1"><div class="m1"><p>آن آب کزو بود دلم را شادی</p></div>
<div class="m2"><p>د رچشم خودش هیچ محل ننهادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آتش که ازوست بر دلم بیدادی</p></div>
<div class="m2"><p>بر منصب دست راست جایش دادی</p></div></div>