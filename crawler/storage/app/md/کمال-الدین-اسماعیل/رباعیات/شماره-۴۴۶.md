---
title: >-
    شمارهٔ ۴۴۶
---
# شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>این پیش نیاز کرده از زر دیوار</p></div>
<div class="m2"><p>خورشید حیات دشمنت بسر دیوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جود توام امید چیزیست که آن</p></div>
<div class="m2"><p>چون روی حسود روی تو بود در دیوار</p></div></div>