---
title: >-
    شمارهٔ ۲۳۸
---
# شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>گر حلقۀ زلف تو کسی زبشمارد</p></div>
<div class="m2"><p>در حال دلش بکفر ایمان آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین سر که سر زلف درازت دارد</p></div>
<div class="m2"><p>کس را بوصال روی تو نگذارد</p></div></div>