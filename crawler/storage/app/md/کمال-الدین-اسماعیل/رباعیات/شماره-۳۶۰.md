---
title: >-
    شمارهٔ ۳۶۰
---
# شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>عشّاق نه از غم جوانی گریند</p></div>
<div class="m2"><p>یا از پی مال و سوزیانی گریند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون چنگ همه ز تن درستی نالند</p></div>
<div class="m2"><p>چون شمع همه ز زندگانی گریند</p></div></div>