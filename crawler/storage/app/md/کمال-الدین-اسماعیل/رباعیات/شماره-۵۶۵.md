---
title: >-
    شمارهٔ ۵۶۵
---
# شمارهٔ ۵۶۵

<div class="b" id="bn1"><div class="m1"><p>ماییم نهفته گریه در خنده چو گل</p></div>
<div class="m2"><p>مرده بدمی و از دمی زنده چو گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را بهمه میان درافگنده چو گل</p></div>
<div class="m2"><p>و اندر همه مجمعی پراگنده چو گل</p></div></div>