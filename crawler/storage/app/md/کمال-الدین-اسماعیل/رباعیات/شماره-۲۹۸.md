---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>بسیار بدیدم و چو تو کم باشد</p></div>
<div class="m2"><p>یاری که برنج یار خرم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نازک دل و زود سیر و بد پیوندی</p></div>
<div class="m2"><p>زان وصل تو چون پیاله یک دم باشد</p></div></div>