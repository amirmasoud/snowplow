---
title: >-
    شمارهٔ ۵۷۶
---
# شمارهٔ ۵۷۶

<div class="b" id="bn1"><div class="m1"><p>شاید گر از آن روی نکو نشکیبم</p></div>
<div class="m2"><p>یا زان سر زلف مشک بو نشکیبم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناسازتر از غمش حریفی نبود</p></div>
<div class="m2"><p>روزی که نبینمش ازو نشکیبم</p></div></div>