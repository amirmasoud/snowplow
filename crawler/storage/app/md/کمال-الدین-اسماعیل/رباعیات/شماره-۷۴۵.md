---
title: >-
    شمارهٔ ۷۴۵
---
# شمارهٔ ۷۴۵

<div class="b" id="bn1"><div class="m1"><p>ای دل ره او می روی اوّل خون شو</p></div>
<div class="m2"><p>وآنگاه بیا تات بگویم چون شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین مشکل اگر برون شوی می جویی</p></div>
<div class="m2"><p>بیرون مشو از خود وز خود بیرون شو</p></div></div>