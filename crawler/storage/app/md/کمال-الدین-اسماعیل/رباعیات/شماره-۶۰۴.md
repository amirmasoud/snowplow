---
title: >-
    شمارهٔ ۶۰۴
---
# شمارهٔ ۶۰۴

<div class="b" id="bn1"><div class="m1"><p>چون کوس ز پرخاش بود آوازم</p></div>
<div class="m2"><p>چون نیزه بسر بجنگ دشمن یازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تیر به تنها شکنم قلب عدو</p></div>
<div class="m2"><p>چون تیغ برهنه بر سر او تازم</p></div></div>