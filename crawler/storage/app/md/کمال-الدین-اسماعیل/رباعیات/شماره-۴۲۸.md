---
title: >-
    شمارهٔ ۴۲۸
---
# شمارهٔ ۴۲۸

<div class="b" id="bn1"><div class="m1"><p>آنان که زوصلشان دلم می بالید</p></div>
<div class="m2"><p>جانم زفراقشان فراوان نالید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگاه دهان گورشان بی دندان</p></div>
<div class="m2"><p>چون آب بخورد و خاک در لب مالید</p></div></div>