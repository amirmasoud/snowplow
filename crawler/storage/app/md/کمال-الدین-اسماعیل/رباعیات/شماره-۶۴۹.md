---
title: >-
    شمارهٔ ۶۴۹
---
# شمارهٔ ۶۴۹

<div class="b" id="bn1"><div class="m1"><p>بی آنکه مراد خویش حاصل کردیم</p></div>
<div class="m2"><p>سرمایۀ عمر خیره باطل کردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیهوده پی هوا دل می برفتیم</p></div>
<div class="m2"><p>تا جان عزیز در سر دل کردیم</p></div></div>