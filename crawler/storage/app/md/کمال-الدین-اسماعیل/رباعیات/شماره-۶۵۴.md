---
title: >-
    شمارهٔ ۶۵۴
---
# شمارهٔ ۶۵۴

<div class="b" id="bn1"><div class="m1"><p>تا کی چو حدیث در زبانت گیریم ؟</p></div>
<div class="m2"><p>خواهیم که در میان جانت گیریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی چو کمر بگرد تو حلقه کنیم</p></div>
<div class="m2"><p>وز بهر کنار در میانت گیریم</p></div></div>