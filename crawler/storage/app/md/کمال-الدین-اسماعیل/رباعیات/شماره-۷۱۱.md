---
title: >-
    شمارهٔ ۷۱۱
---
# شمارهٔ ۷۱۱

<div class="b" id="bn1"><div class="m1"><p>هر شب ز غم تو ای غمت روز افزون</p></div>
<div class="m2"><p>از نالۀ من بناله آید گردون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر لب جیحون گذرم نیست کنون</p></div>
<div class="m2"><p>خود می گذرد بر لب چشمم جیحون</p></div></div>