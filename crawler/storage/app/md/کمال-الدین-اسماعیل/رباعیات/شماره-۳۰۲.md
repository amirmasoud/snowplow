---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>عشق تو مرا جان و روان می‌بخشد</p></div>
<div class="m2"><p>اندوه توام شادی جان می‌بخشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخشنده بود مست، از آن خسته‌دلم</p></div>
<div class="m2"><p>تا مست تو شد هردو جهان می‌بخشد</p></div></div>