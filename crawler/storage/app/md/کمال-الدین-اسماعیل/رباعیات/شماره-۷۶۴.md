---
title: >-
    شمارهٔ ۷۶۴
---
# شمارهٔ ۷۶۴

<div class="b" id="bn1"><div class="m1"><p>ای بر دل من غم ترا دلسوزه</p></div>
<div class="m2"><p>سر گشته ز لعلت فلک پیروزه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ساعته حسنت ز خدا می خواهد</p></div>
<div class="m2"><p>هم ماه دو هفته هم گل یک روزه</p></div></div>