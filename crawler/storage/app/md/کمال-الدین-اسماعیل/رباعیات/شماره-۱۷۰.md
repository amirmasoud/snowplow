---
title: >-
    شمارهٔ ۱۷۰
---
# شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>وقت سحرش چو عزم رفتن بگرفت</p></div>
<div class="m2"><p>دلرا غم جان رفته دامن بگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشکم بدوید تابگیرد راهش</p></div>
<div class="m2"><p>در وی نرسید و دامن من بگرفت</p></div></div>