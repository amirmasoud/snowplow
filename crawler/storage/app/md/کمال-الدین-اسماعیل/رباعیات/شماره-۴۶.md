---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>روزی گیرم میان تو چون کمرت</p></div>
<div class="m2"><p>یا همچو قبا تنگ در آرم ببرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور هیچ شبی چو زلفت افتم بسرت</p></div>
<div class="m2"><p>چون خط تو ناخوانده در آیم ز درت</p></div></div>