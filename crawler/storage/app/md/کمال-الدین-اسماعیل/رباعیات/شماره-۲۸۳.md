---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>باز این چشمم چه فتنه می انگیزد</p></div>
<div class="m2"><p>کز دزدی و خون هیچ نمی پرهیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاهی نظر از خوش پسری می دزدد</p></div>
<div class="m2"><p>گه خون دلی برگذری می ریزد</p></div></div>