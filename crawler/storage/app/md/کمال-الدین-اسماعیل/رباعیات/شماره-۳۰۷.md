---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>آلوده مشو که پاک می باید شد</p></div>
<div class="m2"><p>ماسای که دردناک می باید شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از باد چو آتش ار بری سر فلک</p></div>
<div class="m2"><p>چون آب بزیر خاک می باید شد</p></div></div>