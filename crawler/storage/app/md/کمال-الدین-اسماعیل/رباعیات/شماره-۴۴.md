---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>خواهی که هنروران نکودارندت</p></div>
<div class="m2"><p>با مونس روزگار بگذارندت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان تا ندهی کتاب خود را بکسان</p></div>
<div class="m2"><p>ور نیز گروهای زرین آرندت</p></div></div>