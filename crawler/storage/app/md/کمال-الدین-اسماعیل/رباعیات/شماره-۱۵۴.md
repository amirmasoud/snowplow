---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>بلبل نالان ز شاخ چون دلشده ییست</p></div>
<div class="m2"><p>گریان ز برش ابر چو محنت زده ییست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند جهان خوشست ایمه چه خوشی؟</p></div>
<div class="m2"><p>کز گریه و ناله همچو ماتم کده ییست</p></div></div>