---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>چون مردمک دیده ام ای در خوشاب</p></div>
<div class="m2"><p>با آنکه بود بخشش من گوهر ناب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دور اگرم قرص خور آید در چشم</p></div>
<div class="m2"><p>از غایت حرص بر دهان آرم آب</p></div></div>