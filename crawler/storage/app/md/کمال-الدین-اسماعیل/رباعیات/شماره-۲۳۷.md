---
title: >-
    شمارهٔ ۲۳۷
---
# شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>زلف تو که خون خلق ازو می بارد</p></div>
<div class="m2"><p>گیرم دل عاشقان همی آزارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باری کلهت بروچه دعوی داد؟</p></div>
<div class="m2"><p>کز سایه بآفتاب می نگذارد</p></div></div>