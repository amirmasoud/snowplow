---
title: >-
    شمارهٔ ۷۲۰
---
# شمارهٔ ۷۲۰

<div class="b" id="bn1"><div class="m1"><p>در دست شه آن ساغر غمکاه ببین</p></div>
<div class="m2"><p>خورشید که جان می کشد از ماه ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هندوی برهنه دیده یی در ردیا</p></div>
<div class="m2"><p>اندر کف شاه خنجر شاه ببین</p></div></div>