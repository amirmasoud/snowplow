---
title: >-
    شمارهٔ ۴۹۸
---
# شمارهٔ ۴۹۸

<div class="b" id="bn1"><div class="m1"><p>نرگس که صبا بروی درمی جهدش</p></div>
<div class="m2"><p>یعنی ز لطافتست که دم می دهدش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کآن چشم که باز کرد در کوی بقا</p></div>
<div class="m2"><p>چندان ندهد مهله که بر هم نهدش</p></div></div>