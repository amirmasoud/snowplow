---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>هر کجا که دلی هست ز غم فرسودست</p></div>
<div class="m2"><p>کس نیست که از رنج جهان آسودست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بلبل محنت زده عاشق بودست</p></div>
<div class="m2"><p>باری دل غنچه از چه خون آلودست؟</p></div></div>