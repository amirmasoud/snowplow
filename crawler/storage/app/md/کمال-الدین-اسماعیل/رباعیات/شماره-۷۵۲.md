---
title: >-
    شمارهٔ ۷۵۲
---
# شمارهٔ ۷۵۲

<div class="b" id="bn1"><div class="m1"><p>بر رهگذرت بس که کنم تر سر راه</p></div>
<div class="m2"><p>بگرفت سرشک چشم من هر سر راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آرزوی روی تو این چشم پرآب</p></div>
<div class="m2"><p>دارم چو سقایه روز و شب بر سر راه</p></div></div>