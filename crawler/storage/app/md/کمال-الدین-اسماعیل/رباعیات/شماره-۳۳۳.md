---
title: >-
    شمارهٔ ۳۳۳
---
# شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>اهل طبرستان همه چون فاخته اند</p></div>
<div class="m2"><p>کز سلّۀ بید خانه پرداخته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان همچو شکر در آب بگداخته اند</p></div>
<div class="m2"><p>کایشان چو شکر خانه زنی ساخته اند</p></div></div>