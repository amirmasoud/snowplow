---
title: >-
    شمارهٔ ۸۵۹
---
# شمارهٔ ۸۵۹

<div class="b" id="bn1"><div class="m1"><p>کی بر کنم از تو من دل ای بینایی</p></div>
<div class="m2"><p>کز عشق تو گشته ام بدین رسوایی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان روز که در جور و جفا افزایی</p></div>
<div class="m2"><p>گویی که بچشم من نکوتر آیی</p></div></div>