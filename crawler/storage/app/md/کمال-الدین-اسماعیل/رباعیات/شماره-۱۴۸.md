---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>چشم تو که کار او بازی نیست</p></div>
<div class="m2"><p>در مملکت جمالش انبازی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتند یکیست چشم تیر اندازش</p></div>
<div class="m2"><p>باری دوم او بیک اندازی نیست</p></div></div>