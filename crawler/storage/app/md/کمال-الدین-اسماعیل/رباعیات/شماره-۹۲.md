---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>از حلقۀ گوش تو دلم را خبرست</p></div>
<div class="m2"><p>کین تندی طبعت همه از بهر زرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گوش تو خود قیاس می باید کرد</p></div>
<div class="m2"><p>کانجا که زرست پاره یی نرم ترست</p></div></div>