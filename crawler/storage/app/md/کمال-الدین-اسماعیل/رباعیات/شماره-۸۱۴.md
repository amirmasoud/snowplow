---
title: >-
    شمارهٔ ۸۱۴
---
# شمارهٔ ۸۱۴

<div class="b" id="bn1"><div class="m1"><p>با دل گفتم تو باری آخر نیکی</p></div>
<div class="m2"><p>از من دوری به یار من نزدیکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گفت که با دهان و زلفش عمریست</p></div>
<div class="m2"><p>تا می سازم بتنگی و تاریکی</p></div></div>