---
title: >-
    شمارهٔ ۵۹۸
---
# شمارهٔ ۵۹۸

<div class="b" id="bn1"><div class="m1"><p>چون در تو نیارم که پیاپی نگرم</p></div>
<div class="m2"><p>دزدیده بزیر چشم تا کی نگرم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خام طمع وصل کسی می طلبم</p></div>
<div class="m2"><p>کم زهرۀ آن نیست که دوری نگرم</p></div></div>