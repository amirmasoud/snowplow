---
title: >-
    شمارهٔ ۸۵۱
---
# شمارهٔ ۸۵۱

<div class="b" id="bn1"><div class="m1"><p>دل در سر زلف تونه زان بست رهی</p></div>
<div class="m2"><p>کورا دو هزار بند بر بند نهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاهیش بزیر کلهی بنشانی</p></div>
<div class="m2"><p>گاهیش بدست شانه یی با ز دهی</p></div></div>