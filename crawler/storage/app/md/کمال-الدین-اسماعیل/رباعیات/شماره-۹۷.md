---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>گفتم که مرا بر تو ببوسی نازست</p></div>
<div class="m2"><p>گفتا که زرت چاره اراینت آزست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم: نه تو دانی که مرا زر نبود</p></div>
<div class="m2"><p>گفتا که برو ، اوام را در بازست</p></div></div>