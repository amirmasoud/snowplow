---
title: >-
    شمارهٔ ۵۸۵
---
# شمارهٔ ۵۸۵

<div class="b" id="bn1"><div class="m1"><p>از عمر عزیز خود برین خرسندم</p></div>
<div class="m2"><p>کآن روز که در غصّه بشب پیوندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دل گویم مؤده که از راه اجل</p></div>
<div class="m2"><p>یک منزل دیگر پس پشت افگندم</p></div></div>