---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>بی مرگی اگر چه آشنایی مرگست</p></div>
<div class="m2"><p>از دست مده که آن نوای مرگست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین نکنی کوشش اگر بشناسی</p></div>
<div class="m2"><p>کآسایش زندگی بلای مرگست</p></div></div>