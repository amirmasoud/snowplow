---
title: >-
    شمارهٔ ۵۵۷
---
# شمارهٔ ۵۵۷

<div class="b" id="bn1"><div class="m1"><p>شادی طلبی از غم جانان مگسل</p></div>
<div class="m2"><p>ور دل جویی ز زلف ایشان مگسل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور عیش خوشت باید و کاری بنظام</p></div>
<div class="m2"><p>تا جان داری زان لب و دندان مگسل</p></div></div>