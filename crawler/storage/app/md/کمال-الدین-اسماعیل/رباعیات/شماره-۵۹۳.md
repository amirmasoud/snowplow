---
title: >-
    شمارهٔ ۵۹۳
---
# شمارهٔ ۵۹۳

<div class="b" id="bn1"><div class="m1"><p>آن روز که بر خاطر عالی گذرم</p></div>
<div class="m2"><p>از عجب چو نرگس همه در خود نگرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کوی تو ار باد قبولی جهدم</p></div>
<div class="m2"><p>مانندۀ برگ گل ز شادی بپرم</p></div></div>