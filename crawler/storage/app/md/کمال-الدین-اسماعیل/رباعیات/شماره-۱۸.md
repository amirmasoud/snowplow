---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>دل بر تو نهم رغم بد اندیشان را</p></div>
<div class="m2"><p>وز تو نبرم ستیزۀ ایشان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور من بمثل بمیرم اندر غم تو</p></div>
<div class="m2"><p>عشق تو بمیراث دهم خویشان را</p></div></div>