---
title: >-
    شمارهٔ ۸۱۹
---
# شمارهٔ ۸۱۹

<div class="b" id="bn1"><div class="m1"><p>چون نیست حدیث وصلت از زر خالی</p></div>
<div class="m2"><p>هم نرم کنم ترا بچیزی مالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر را بفرستم که خود او چون حلقه</p></div>
<div class="m2"><p>گوشت گیرد پیش من آرد حالی</p></div></div>