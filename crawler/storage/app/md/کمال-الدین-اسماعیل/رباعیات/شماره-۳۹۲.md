---
title: >-
    شمارهٔ ۳۹۲
---
# شمارهٔ ۳۹۲

<div class="b" id="bn1"><div class="m1"><p>گر باد در آن طرّة دلخواه شود</p></div>
<div class="m2"><p>از بس خم و پیچ و تاب گمراه شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان هم ز شکستگی اندام بود</p></div>
<div class="m2"><p>کوگاه دراز و گاه کوتاه شود</p></div></div>