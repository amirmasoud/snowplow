---
title: >-
    شمارهٔ ۸۵۸
---
# شمارهٔ ۸۵۸

<div class="b" id="bn1"><div class="m1"><p>یک روز بکوی عاشقان بر نایی</p></div>
<div class="m2"><p>وز بهر تماشا نظری نگشایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چون سر زلف خویش بینی آنجا</p></div>
<div class="m2"><p>بر هر در خانه، حلقه‌ای سودایی</p></div></div>