---
title: >-
    شمارهٔ ۴۲۷
---
# شمارهٔ ۴۲۷

<div class="b" id="bn1"><div class="m1"><p>لطف تو بآشنا و بیگانه رسید</p></div>
<div class="m2"><p>زو بهره بهر دلی جداگانه رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خلوت وصل و لذت گفت و شنید</p></div>
<div class="m2"><p>ما را همه آرزو و افسانه رسید</p></div></div>