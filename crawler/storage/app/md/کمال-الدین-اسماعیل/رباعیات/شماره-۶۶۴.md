---
title: >-
    شمارهٔ ۶۶۴
---
# شمارهٔ ۶۶۴

<div class="b" id="bn1"><div class="m1"><p>با دیده دلم گفت: چو از دست تو من</p></div>
<div class="m2"><p>خون گشتم و ساختم در آتش مسکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو راه برون شوم بکن ، گفت بچشم</p></div>
<div class="m2"><p>از بهر تو بر منست راهی روشن</p></div></div>