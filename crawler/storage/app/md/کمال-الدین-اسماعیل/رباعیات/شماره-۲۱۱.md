---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>ساقی بر من چو جام روشن بنهاد</p></div>
<div class="m2"><p>جانم بهوای خدمتش تن بنهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقلم چو صراحی ار چه گرد نکش بود</p></div>
<div class="m2"><p>حالی چو پیاله دید گردن بنهاد</p></div></div>