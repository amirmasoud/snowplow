---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>خون باد دل ارشاد بپیوند تو نیست</p></div>
<div class="m2"><p>جان برخی اگر بر سر سوگند تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشمن دارم کسی که در تو نگرد</p></div>
<div class="m2"><p>من بندۀ انکسم که در بند تو نیست</p></div></div>