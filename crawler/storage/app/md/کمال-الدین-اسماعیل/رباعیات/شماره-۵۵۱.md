---
title: >-
    شمارهٔ ۵۵۱
---
# شمارهٔ ۵۵۱

<div class="b" id="bn1"><div class="m1"><p>خاک سر کوی آن بت مشکین خال</p></div>
<div class="m2"><p>می بوسیدم دوش باومید وصال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنهان ز رقیب آمد و در گوشم گفت</p></div>
<div class="m2"><p>می خور غم ما و خاک در لب می مال</p></div></div>