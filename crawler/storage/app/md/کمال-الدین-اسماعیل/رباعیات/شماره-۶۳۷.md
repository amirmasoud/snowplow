---
title: >-
    شمارهٔ ۶۳۷
---
# شمارهٔ ۶۳۷

<div class="b" id="bn1"><div class="m1"><p>می باز خورم و لیک مستی نکنم</p></div>
<div class="m2"><p>الا بقدح دراز دستی نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی غرضم زمی پرستی چه بود؟</p></div>
<div class="m2"><p>تا همچو تو خویشتن پرستی نکنم</p></div></div>