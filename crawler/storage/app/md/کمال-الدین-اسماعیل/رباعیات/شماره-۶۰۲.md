---
title: >-
    شمارهٔ ۶۰۲
---
# شمارهٔ ۶۰۲

<div class="b" id="bn1"><div class="m1"><p>د رعشق ز حیله ها که می پردازم</p></div>
<div class="m2"><p>تات از همه کس نهفته ماند رازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانند زبان شمع آنگه سوزم</p></div>
<div class="m2"><p>کز اشک بنزد خویش خندق سازم</p></div></div>