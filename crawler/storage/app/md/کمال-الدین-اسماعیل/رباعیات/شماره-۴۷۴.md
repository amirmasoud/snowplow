---
title: >-
    شمارهٔ ۴۷۴
---
# شمارهٔ ۴۷۴

<div class="b" id="bn1"><div class="m1"><p>ای دوست بیکباره ره ناز مگیر</p></div>
<div class="m2"><p>هر لحظه بهانه یی نو آغاز مگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که مقصودی از آن حاصل نیست</p></div>
<div class="m2"><p>پیغام دروغ هم ز من بازمگیر</p></div></div>