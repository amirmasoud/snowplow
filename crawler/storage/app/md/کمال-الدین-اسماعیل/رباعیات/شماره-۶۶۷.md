---
title: >-
    شمارهٔ ۶۶۷
---
# شمارهٔ ۶۶۷

<div class="b" id="bn1"><div class="m1"><p>نرگس که در انتظار گل بود چو من</p></div>
<div class="m2"><p>یک چند نهاده چشم بر طرف چمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با سرخ گلش بهم چو دیدم گفتم</p></div>
<div class="m2"><p>ای نرگس پرخمار ، چشمت روشن</p></div></div>