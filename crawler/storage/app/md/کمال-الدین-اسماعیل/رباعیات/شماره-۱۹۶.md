---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>وقتی که بتم گره بر آن زلف نهاد</p></div>
<div class="m2"><p>از سختی بند بر قفا می افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز بشکر آنکه بندش بگشاد</p></div>
<div class="m2"><p>بر روی افتاد بوسه بر پایش داد</p></div></div>