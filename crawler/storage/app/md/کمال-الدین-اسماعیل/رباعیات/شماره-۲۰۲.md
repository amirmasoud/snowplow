---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>.... عشق تو فیروز مباد</p></div>
<div class="m2"><p>جز جان من از عشق تو با سوزمباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>... ندیده بودیم روا</p></div>
<div class="m2"><p>روزی که غمت نه بینم آنروز مباد</p></div></div>