---
title: >-
    شمارهٔ ۶۸۸
---
# شمارهٔ ۶۸۸

<div class="b" id="bn1"><div class="m1"><p>در روزه چو نیست روی می نوشیدن</p></div>
<div class="m2"><p>گل را بچه کارست چنین خندیدن؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکل کاری بوقت گل اندر پیش</p></div>
<div class="m2"><p>قندیل بجای ساتکینی دیدن</p></div></div>