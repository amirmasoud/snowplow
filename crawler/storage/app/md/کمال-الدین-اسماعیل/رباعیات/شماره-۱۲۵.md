---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>بشنو سخن باد که چون دلخواهست</p></div>
<div class="m2"><p>بنشین که نه وقت آتش و خر گاهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این باد بدین خوشی ندانم زکجاست</p></div>
<div class="m2"><p>یا محمل گل رسید یا در راهست</p></div></div>