---
title: >-
    شمارهٔ ۶۴۴
---
# شمارهٔ ۶۴۴

<div class="b" id="bn1"><div class="m1"><p>سر در سر خاک آستان تو نهم</p></div>
<div class="m2"><p>دل در خم زلف دلستان تو نهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانم بلب آمدست یک بوسه بیار</p></div>
<div class="m2"><p>تا جان ببهانه در دهان تو نهم</p></div></div>