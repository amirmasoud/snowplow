---
title: >-
    شمارهٔ ۲۲۷
---
# شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>گل در مه روزه همچنان می خندد</p></div>
<div class="m2"><p>گویی که بطنز بر جهان می خندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می روشن و نو بهار و مردم هشیار</p></div>
<div class="m2"><p>گل را عجب آمدست از آن می خندد</p></div></div>