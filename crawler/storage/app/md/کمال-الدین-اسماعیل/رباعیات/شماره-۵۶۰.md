---
title: >-
    شمارهٔ ۵۶۰
---
# شمارهٔ ۵۶۰

<div class="b" id="bn1"><div class="m1"><p>هر صبحدمی زخواب برخیزد گل</p></div>
<div class="m2"><p>رنگی ز دگر گونه برآمیزد گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا در دو سه خردۀ زر آویزد گل</p></div>
<div class="m2"><p>صد وجه زخویشتن برانگیزد گل</p></div></div>