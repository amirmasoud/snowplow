---
title: >-
    شمارهٔ ۳۶۵
---
# شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>گل چو ز صبا حدیث رویت بشنود</p></div>
<div class="m2"><p>لبخند یی از سر رعونت بنمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا لاجرمش صبا چنان زد بر روی</p></div>
<div class="m2"><p>کش گشت همه لب و دهن خون آلود</p></div></div>