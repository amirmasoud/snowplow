---
title: >-
    شمارهٔ ۵۹۵
---
# شمارهٔ ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>گر آب خورم درد شود بر جگرم</p></div>
<div class="m2"><p>ور خواب کنم گرد شود در بصرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرج آن سبب راحت خود می شمرم</p></div>
<div class="m2"><p>رنجم همه زانست چو در می نگرم</p></div></div>