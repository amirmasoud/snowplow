---
title: >-
    شمارهٔ ۴۱۸
---
# شمارهٔ ۴۱۸

<div class="b" id="bn1"><div class="m1"><p>چون یادم از آن روی نکو می آید</p></div>
<div class="m2"><p>خونم بدل تیره فرو می اید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاکیزه تری ز قطرۀ اشک از انک</p></div>
<div class="m2"><p>در چشم نیایی تو و او می آید</p></div></div>