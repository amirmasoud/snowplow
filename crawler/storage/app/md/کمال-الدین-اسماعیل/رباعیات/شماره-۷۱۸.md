---
title: >-
    شمارهٔ ۷۱۸
---
# شمارهٔ ۷۱۸

<div class="b" id="bn1"><div class="m1"><p>گر رای تماشا کنی ای دل منشین</p></div>
<div class="m2"><p>بیرون جه از آن کمند زلف برچین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیرامن چشمۀ حیات لب او ی</p></div>
<div class="m2"><p>بگشای یکی دو چشم و آن سبزه ببین</p></div></div>