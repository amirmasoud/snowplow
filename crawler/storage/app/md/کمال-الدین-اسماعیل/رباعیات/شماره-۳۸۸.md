---
title: >-
    شمارهٔ ۳۸۸
---
# شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>وقتست که گل تاج سر انگشت شود</p></div>
<div class="m2"><p>و افروخته چون آتش زردشت شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون رای تو گیرد همه دل رو گیرد</p></div>
<div class="m2"><p>چون روی تو بیند همه تن پشت شود</p></div></div>