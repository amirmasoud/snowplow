---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>گر شرح دهم که بی تو کارم چونست</p></div>
<div class="m2"><p>یا حالت چشم اشک بارم چونست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کارم همه غم خوردن و بارم برجان</p></div>
<div class="m2"><p>انصاف به بین که کار و بارم چونست</p></div></div>