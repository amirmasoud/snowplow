---
title: >-
    شمارهٔ ۵۹۷
---
# شمارهٔ ۵۹۷

<div class="b" id="bn1"><div class="m1"><p>چشمی ز خیال تو پر اختر دارم</p></div>
<div class="m2"><p>دستی ز غم هجر تو بر سر دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش گشت دلم تا که خیال تو در اوست</p></div>
<div class="m2"><p>زانش همه‌ساله تنگ در بر دارم</p></div></div>