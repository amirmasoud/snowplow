---
title: >-
    شمارهٔ ۳۳۸
---
# شمارهٔ ۳۳۸

<div class="b" id="bn1"><div class="m1"><p>صاحبنظران چو شمع در بر گیرند</p></div>
<div class="m2"><p>از هیچ حساب خویش کمتر گیرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آفت بود خویشتن بشناسد</p></div>
<div class="m2"><p>حالی سر خود بدست خود برگیرند</p></div></div>