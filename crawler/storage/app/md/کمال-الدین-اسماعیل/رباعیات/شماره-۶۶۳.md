---
title: >-
    شمارهٔ ۶۶۳
---
# شمارهٔ ۶۶۳

<div class="b" id="bn1"><div class="m1"><p>ای مدح تو آورده قلم را بسخن</p></div>
<div class="m2"><p>وی ناطقه در وصف کفت بسته دهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون هر سخن آوری سخن از تو برد</p></div>
<div class="m2"><p>پس چون سخن آوری کنم پیش تو من؟</p></div></div>