---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>ترکم سوی آما جگه آمد سر مست</p></div>
<div class="m2"><p>چون غمزۀ خود تیر و کمان اندر دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر تیر که چون منش ز خود دور انداخت</p></div>
<div class="m2"><p>نالان نالان برفت و در خاک نشست</p></div></div>