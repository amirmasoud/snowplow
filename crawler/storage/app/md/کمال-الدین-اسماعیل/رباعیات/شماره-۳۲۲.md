---
title: >-
    شمارهٔ ۳۲۲
---
# شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>هر گوهر معنی که دلم کرد پسند</p></div>
<div class="m2"><p>تا ناطقه را ازو کنم عقدی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دید لبم بمهر حرمان در بند</p></div>
<div class="m2"><p>آن جمله ز راه دیده بیرون افکند</p></div></div>