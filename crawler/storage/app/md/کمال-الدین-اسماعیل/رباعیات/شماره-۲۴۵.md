---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>آن شاخ سمن که روی خندان دراد</p></div>
<div class="m2"><p>می نتواند که سیم پنهان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان غنچۀ تنگ خولب آورده بهم</p></div>
<div class="m2"><p>زر تعبیه در جامۀ خلقان دارد</p></div></div>