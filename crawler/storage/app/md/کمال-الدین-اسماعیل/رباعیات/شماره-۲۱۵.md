---
title: >-
    شمارهٔ ۲۱۵
---
# شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>چون آه دمادمم افتد</p></div>
<div class="m2"><p>سوز دل من در دل انجم افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با روی تو گر چشم مرا کار افتاد</p></div>
<div class="m2"><p>آری همه کارها بمردم افتد</p></div></div>