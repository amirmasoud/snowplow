---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>تیمار من پشت دوته باید داشت</p></div>
<div class="m2"><p>چشم از پی حاجتم بره باید داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر وقتی که جان بود بر سرپایی</p></div>
<div class="m2"><p>به زین دل دوستان نگه باید داشت</p></div></div>