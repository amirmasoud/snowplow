---
title: >-
    شمارهٔ ۵۳۴
---
# شمارهٔ ۵۳۴

<div class="b" id="bn1"><div class="m1"><p>یارب که چگونه خفت دوش اندر خاک</p></div>
<div class="m2"><p>وان سیمین وی چگونه بپذیرد خاک (؟)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>.....خدایا گنهش</p></div>
<div class="m2"><p>چون رفت بیک بارگهی اندر خاک (؟)</p></div></div>