---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>ای قبله گه ملک خم ابرویت</p></div>
<div class="m2"><p>وی سیلی ابلیس زده بازویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای هشت بهشت خانه یی از کویت</p></div>
<div class="m2"><p>وی دوزخ نیم سوختۀ هندویت</p></div></div>