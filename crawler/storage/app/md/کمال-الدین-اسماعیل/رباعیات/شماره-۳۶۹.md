---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>تیغ تو گیاهیست که مردم درود</p></div>
<div class="m2"><p>وز زخم زبان برو خطایی نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رنگ بسان برگ حنی آمد</p></div>
<div class="m2"><p>سبزست و بهر جا که رسد لعل شود</p></div></div>