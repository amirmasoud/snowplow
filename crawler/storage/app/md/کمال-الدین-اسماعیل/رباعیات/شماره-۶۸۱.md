---
title: >-
    شمارهٔ ۶۸۱
---
# شمارهٔ ۶۸۱

<div class="b" id="bn1"><div class="m1"><p>آن غنچۀ دوشیزه نگر آبستن</p></div>
<div class="m2"><p>از مهر شده بیک نظر آبستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعلی بهزار خرده زر آبستن</p></div>
<div class="m2"><p>چون پیکانی بصد سپر آبستن</p></div></div>