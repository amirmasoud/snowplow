---
title: >-
    شمارهٔ ۳۹۳
---
# شمارهٔ ۳۹۳

<div class="b" id="bn1"><div class="m1"><p>گر سوز توام یک نفس آهسته شود</p></div>
<div class="m2"><p>از دود راه نفس بسته شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیده از آن آب همی گردانم</p></div>
<div class="m2"><p>تا هر چه نه نقش تست از آن شسته شود</p></div></div>