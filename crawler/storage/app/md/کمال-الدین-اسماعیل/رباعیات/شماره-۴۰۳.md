---
title: >-
    شمارهٔ ۴۰۳
---
# شمارهٔ ۴۰۳

<div class="b" id="bn1"><div class="m1"><p>گفتی دودلی تو ، از تو کاری ناید</p></div>
<div class="m2"><p>بهتان چنین نهی تو بر من شاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نیست مرا دلی وگر نیزم هست</p></div>
<div class="m2"><p>تا صد بود از بهر غمت می باید</p></div></div>