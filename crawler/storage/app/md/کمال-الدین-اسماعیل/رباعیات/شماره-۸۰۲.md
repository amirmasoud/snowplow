---
title: >-
    شمارهٔ ۸۰۲
---
# شمارهٔ ۸۰۲

<div class="b" id="bn1"><div class="m1"><p>ای غنچه که خنده هر دم از سر گیری</p></div>
<div class="m2"><p>دل می دهدت که لب ز هم بر گیری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی نرگس شوخ دیده بی چهرۀ او</p></div>
<div class="m2"><p>چشم آب نگیردت که ساغر گیری؟</p></div></div>