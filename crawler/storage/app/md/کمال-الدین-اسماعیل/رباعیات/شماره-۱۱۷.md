---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>این خشک گیا که زرد چون روی منست</p></div>
<div class="m2"><p>ریزنده و جای جای جون موی منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و ایت طاق پل شکسته و آب روان</p></div>
<div class="m2"><p>گویی که مثال چشم و ابروی منست</p></div></div>