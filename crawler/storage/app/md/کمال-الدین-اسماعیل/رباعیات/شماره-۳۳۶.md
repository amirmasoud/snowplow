---
title: >-
    شمارهٔ ۳۳۶
---
# شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>نام کف تو چو پیش دشمن بردند</p></div>
<div class="m2"><p>گوهر ز دو چشم او بخرمن بردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز طرب از بزمگهت زر و درم</p></div>
<div class="m2"><p>نرگس بکلاه و گل بدامن بردند</p></div></div>