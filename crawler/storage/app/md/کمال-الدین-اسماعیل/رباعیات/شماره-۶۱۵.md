---
title: >-
    شمارهٔ ۶۱۵
---
# شمارهٔ ۶۱۵

<div class="b" id="bn1"><div class="m1"><p>تا کرد بروی تو نظر مردم چشم</p></div>
<div class="m2"><p>عیشی دارد بس خوش وتر مردم چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شب ز غمت هزار میخیّ مژه</p></div>
<div class="m2"><p>برخود بدرد تا بسحر مردم چشم</p></div></div>