---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>آن دل که بجز سوی غمت رهبر نیست</p></div>
<div class="m2"><p>بی روی دلفروز توام در خور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان گرچه عزیزست ولی جانب او</p></div>
<div class="m2"><p>از جانب رخسار تو نازک تر نیست</p></div></div>