---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>روی تو بدید عقل را رای برفت</p></div>
<div class="m2"><p>قدّت بخمیده و سرو از جای برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذشت صبا سحرگهی بر گلزار</p></div>
<div class="m2"><p>بویی تو شنید و زورش از پای برفت</p></div></div>