---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>آن بت که سوی دهانش رهبر سخنست</p></div>
<div class="m2"><p>نسرین بر و پسته لب و شکّر سخنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوسه ز دهان او کجا دارم چشم؟</p></div>
<div class="m2"><p>چون با لب او مرا سخن در سخنست</p></div></div>