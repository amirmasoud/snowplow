---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>گفتی که بگو حال دل غم پیوند</p></div>
<div class="m2"><p>تا چند نهان کنی ز من؟ آخر چند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من با تو چه گویم؟ که همه راز دلم</p></div>
<div class="m2"><p>خاموشی من گفت باواز بلند</p></div></div>