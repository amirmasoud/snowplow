---
title: >-
    شمارهٔ ۴۶۲
---
# شمارهٔ ۴۶۲

<div class="b" id="bn1"><div class="m1"><p>باور نکنی که خوب رویان یکسر</p></div>
<div class="m2"><p>اندر پی زر شوند، آنک بنگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیرامن گل بهر دوسه خردۀ زر</p></div>
<div class="m2"><p>صد روی نکو فتاده بر یکدیگر</p></div></div>