---
title: >-
    شمارهٔ ۸۱۵
---
# شمارهٔ ۸۱۵

<div class="b" id="bn1"><div class="m1"><p>تا یافت دلم بزلف تو نزدیکی</p></div>
<div class="m2"><p>چون خطّ تو شد بخردی و باریکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق دهن و زلف تو خوش کرد مردا</p></div>
<div class="m2"><p>اندر دل و دیده تنگی و تاریکی</p></div></div>