---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>یک شب خواهم خراب و ناپروایت</p></div>
<div class="m2"><p>تا روز من اندر برسیم آسایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خط تو گرد دهنت می گردم</p></div>
<div class="m2"><p>چون زلف تو می فتم بسر در پایت</p></div></div>