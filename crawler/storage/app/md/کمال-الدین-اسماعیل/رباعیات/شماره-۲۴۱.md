---
title: >-
    شمارهٔ ۲۴۱
---
# شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>... که طاقت فراقت دارد ؟</p></div>
<div class="m2"><p>دل کیست که برگ اشتیاقت دارد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>... روی تو که بتواند ساخت ؟</p></div>
<div class="m2"><p>با درد فراق تو که طاقت دارد؟</p></div></div>