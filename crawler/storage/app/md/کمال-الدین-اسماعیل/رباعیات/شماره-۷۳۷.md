---
title: >-
    شمارهٔ ۷۳۷
---
# شمارهٔ ۷۳۷

<div class="b" id="bn1"><div class="m1"><p>هر سال که تشریف دهی چون گل نو</p></div>
<div class="m2"><p>با باد بود برفتنت بسته گرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من لابه کنان چو بلبل اندر پی تو</p></div>
<div class="m2"><p>تو خنده زنان گوش در اکنده ورو</p></div></div>