---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>زین بیش گرم چه رای خاموشی نیست</p></div>
<div class="m2"><p>خاموشم و هیچ جای خاموشی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریادرس ای خواجه درین غم که مرا</p></div>
<div class="m2"><p>برگ سخن و نوای خاموشی نیست</p></div></div>