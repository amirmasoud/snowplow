---
title: >-
    شمارهٔ ۶۰۸
---
# شمارهٔ ۶۰۸

<div class="b" id="bn1"><div class="m1"><p>از گردش چرخ بی خرد می ترسم</p></div>
<div class="m2"><p>در هر حالی ز نیک و بد می ترسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان روی که بر کس اعتمادی بنماند</p></div>
<div class="m2"><p>از همرهی سایۀ خود می ترسم</p></div></div>