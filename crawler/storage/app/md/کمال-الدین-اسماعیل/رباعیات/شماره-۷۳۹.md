---
title: >-
    شمارهٔ ۷۳۹
---
# شمارهٔ ۷۳۹

<div class="b" id="bn1"><div class="m1"><p>آن شانه که از تو هست یک موی درو</p></div>
<div class="m2"><p>چون ارّه کشم زبان ز هر سوی درو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وآن آینه‌ای که روی در روی تو کرد</p></div>
<div class="m2"><p>چون قبله شب و روز کنم روی درو</p></div></div>