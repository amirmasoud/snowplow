---
title: >-
    شمارهٔ ۷۹۸
---
# شمارهٔ ۷۹۸

<div class="b" id="bn1"><div class="m1"><p>زلفی که چو روز من سیه می داری</p></div>
<div class="m2"><p>بر پای دلم بی گه و گه می داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی که دل من دل شیران دارد</p></div>
<div class="m2"><p>ز آتش بدو زنجیر نگه می داری</p></div></div>