---
title: >-
    شمارهٔ ۷۱۳
---
# شمارهٔ ۷۱۳

<div class="b" id="bn1"><div class="m1"><p>از خار چو آمد گل رنگین بیرون</p></div>
<div class="m2"><p>اندوه کنیم از دل غمگین بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردند نظاره را عروسان چمن</p></div>
<div class="m2"><p>سرها ز دریچه های چو بین بیرون</p></div></div>