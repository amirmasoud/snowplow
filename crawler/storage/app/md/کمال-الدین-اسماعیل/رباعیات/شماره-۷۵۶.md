---
title: >-
    شمارهٔ ۷۵۶
---
# شمارهٔ ۷۵۶

<div class="b" id="bn1"><div class="m1"><p>میآید و چهره از عرق تر کرده</p></div>
<div class="m2"><p>چو کان بکفت و اسب ز جابر کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واندر رخم زلفهای گرد آلودش</p></div>
<div class="m2"><p>شهری دل خسته خاک بر سر کرده</p></div></div>