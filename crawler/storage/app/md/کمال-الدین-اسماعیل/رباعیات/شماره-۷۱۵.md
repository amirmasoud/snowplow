---
title: >-
    شمارهٔ ۷۱۵
---
# شمارهٔ ۷۱۵

<div class="b" id="bn1"><div class="m1"><p>فرّاش چمن باد شمالست اکنون</p></div>
<div class="m2"><p>بی باده و گل عمر و بالست اکنون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خور که با جماع همه اهل خرد</p></div>
<div class="m2"><p>خون رزو مال گل حلالست اکنون</p></div></div>