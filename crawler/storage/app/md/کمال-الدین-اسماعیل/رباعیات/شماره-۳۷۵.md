---
title: >-
    شمارهٔ ۳۷۵
---
# شمارهٔ ۳۷۵

<div class="b" id="bn1"><div class="m1"><p>جایی که فراق آن دلفروز بود</p></div>
<div class="m2"><p>سنگین بود آن دل که نه پر سوز بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دیده گرت اشک نماندست رواست</p></div>
<div class="m2"><p>خون جگر از بهر چنین روز بود</p></div></div>