---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>بی روی تو شادی همه در درد دلست</p></div>
<div class="m2"><p>و احوال زمانه سر بسر درد دلست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم بهر آن صفت که باشد گویاش</p></div>
<div class="m2"><p>ما را ز میانه راه بر درد دلست</p></div></div>