---
title: >-
    شمارهٔ ۲۹۷
---
# شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>شمعم که غذای چشمم از نم باشد</p></div>
<div class="m2"><p>خنده همه گریه سور ماتم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادی و طرب بخواب بینم گه گاه</p></div>
<div class="m2"><p>وان نیز چو بیدار شوم غم باشد</p></div></div>