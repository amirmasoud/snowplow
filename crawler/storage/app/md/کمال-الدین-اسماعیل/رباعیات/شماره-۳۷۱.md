---
title: >-
    شمارهٔ ۳۷۱
---
# شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>خورشید اگر چه در جهان فرو برد</p></div>
<div class="m2"><p>ز آمد شدنش دلی پر از درد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم وقت بر آمدن دمش سرد بود</p></div>
<div class="m2"><p>هم وقت فرو شدن رخش زرد بود</p></div></div>