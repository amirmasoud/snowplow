---
title: >-
    شمارهٔ ۶۷۶
---
# شمارهٔ ۶۷۶

<div class="b" id="bn1"><div class="m1"><p>در فرقت تو چو بلبلم نوحه گران</p></div>
<div class="m2"><p>چون دیدۀ نرگس از پی جان نگران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون لاله ام از میان جان سوخته دل</p></div>
<div class="m2"><p>چون غنچه ام از درون دل جامه دران</p></div></div>