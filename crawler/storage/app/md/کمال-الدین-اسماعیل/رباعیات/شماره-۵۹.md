---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>ای ترک حصاری همه چیزت بنواست</p></div>
<div class="m2"><p>الّا یک چیز کز تو آن عین خطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک چشم تو مستور و دگر مست و خراب</p></div>
<div class="m2"><p>مستوری و مستیت بهم ناید راست</p></div></div>