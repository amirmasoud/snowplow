---
title: >-
    شمارهٔ ۶۸۵
---
# شمارهٔ ۶۸۵

<div class="b" id="bn1"><div class="m1"><p>سیر آمدم از غم دمادم خوردن</p></div>
<div class="m2"><p>وز بس غم گونه گونه در هم خوردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الحق چه نکوست عادت کم خوردن</p></div>
<div class="m2"><p>اندر همه چیز خاصه در غم خوردن</p></div></div>