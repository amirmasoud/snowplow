---
title: >-
    شمارهٔ ۸۵۳
---
# شمارهٔ ۸۵۳

<div class="b" id="bn1"><div class="m1"><p>من پیرو کهن گشته ز جان فرسایی</p></div>
<div class="m2"><p>عشق آمد و داد از توام بر نایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیرانه سر ار چه نیست جز رسوایی</p></div>
<div class="m2"><p>الحق خوشم آید این کهن پیرایی</p></div></div>