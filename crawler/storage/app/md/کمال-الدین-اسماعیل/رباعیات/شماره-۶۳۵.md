---
title: >-
    شمارهٔ ۶۳۵
---
# شمارهٔ ۶۳۵

<div class="b" id="bn1"><div class="m1"><p>اندر دل خود نقش تو پیدا نکنم</p></div>
<div class="m2"><p>بر دیده خیالت آشکارا نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود دیده و دل خاک در تست ولی</p></div>
<div class="m2"><p>من جای تو در دوزخ و دریا نکنم</p></div></div>