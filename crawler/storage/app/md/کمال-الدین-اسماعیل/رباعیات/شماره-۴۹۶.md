---
title: >-
    شمارهٔ ۴۹۶
---
# شمارهٔ ۴۹۶

<div class="b" id="bn1"><div class="m1"><p>گل را دیدم دمیده از کام آتش</p></div>
<div class="m2"><p>از آب گرفته هفت اندام آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که چه شد ؟ گفت بلائیست دراز</p></div>
<div class="m2"><p>کوتاهی عمر و پس سرانجام آتش</p></div></div>