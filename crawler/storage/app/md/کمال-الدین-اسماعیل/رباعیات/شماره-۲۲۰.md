---
title: >-
    شمارهٔ ۲۲۰
---
# شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>اشکم ز تو در خون جگر می غلتد</p></div>
<div class="m2"><p>پیش در تو بخاک در می غلتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آرزوی خاک در تو همه شب</p></div>
<div class="m2"><p>از چهرۀ من بر سر زر می غلتد</p></div></div>