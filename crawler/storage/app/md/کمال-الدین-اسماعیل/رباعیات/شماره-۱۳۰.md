---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>گر چه دل من چو شمع آتش خایست</p></div>
<div class="m2"><p>این آتش من چو آب جار افزایست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ورچند مرا زبان تن فرسایست</p></div>
<div class="m2"><p>چون شمع زبان من بتن برپایست</p></div></div>