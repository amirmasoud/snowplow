---
title: >-
    شمارهٔ ۵۵۴
---
# شمارهٔ ۵۵۴

<div class="b" id="bn1"><div class="m1"><p>آن غنچه نگر چو من گرفتار بدل</p></div>
<div class="m2"><p>پیوسته لبش بمهر و انکار بدل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آورده فراهم رخ و برخود پیچان</p></div>
<div class="m2"><p>زانست که هر دم رسدش خاربدل</p></div></div>