---
title: >-
    شمارهٔ ۳۰۴
---
# شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>دل در پی دلبر بسفر خواهد شد</p></div>
<div class="m2"><p>جان نیز برین عزم بدر خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا باز مرا ازو خبر خواهد شد</p></div>
<div class="m2"><p>بس آب جوی دیده در خواهد شد</p></div></div>