---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>آن دل که بر اتش غمت صد ره سوخت</p></div>
<div class="m2"><p>از پهلوی من همه غم و درد اندوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون گشت و همی رود ز چشمم همه روز</p></div>
<div class="m2"><p>این شب روی اندر سر زلفت آموخت</p></div></div>