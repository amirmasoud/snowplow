---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>نه یاری ازین دلگسلی هست مرا</p></div>
<div class="m2"><p>نه جز غم عشق حاصلی هست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وآنگه گویی مرا که دل خوش می دار</p></div>
<div class="m2"><p>خاموش، کدام دل؟ دلی هست مرا؟</p></div></div>