---
title: >-
    شمارهٔ ۸۰۳
---
# شمارهٔ ۸۰۳

<div class="b" id="bn1"><div class="m1"><p>چشمت بکر شمه از سر طنازی</p></div>
<div class="m2"><p>دی گفت شبی بوصل من پردازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی که درین چه دیده باشد چشمت</p></div>
<div class="m2"><p>جز آنکه همی کند بمردم بازی؟</p></div></div>