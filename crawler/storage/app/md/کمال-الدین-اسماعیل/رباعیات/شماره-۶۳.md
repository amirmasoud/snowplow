---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>در چاه ز نخدانت دل ما بنواست</p></div>
<div class="m2"><p>وان خیال سیاه تو بدین حال گواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیبیست ز نخدان تو وان خال سیاه</p></div>
<div class="m2"><p>از غایت لطف دانه در وی پیداست</p></div></div>