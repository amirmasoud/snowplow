---
title: >-
    شمارهٔ ۴۷۲
---
# شمارهٔ ۴۷۲

<div class="b" id="bn1"><div class="m1"><p>شد بر دل من زلفک هندوی تو چیر</p></div>
<div class="m2"><p>بر بودش و در زیر کله رفت دلیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می گویمت ای دوست بگو با کلهت</p></div>
<div class="m2"><p>تا هندوی دزد را نگیرد در زیر</p></div></div>