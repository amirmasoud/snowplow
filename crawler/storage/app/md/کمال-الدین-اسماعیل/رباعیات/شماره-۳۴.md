---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>کام همه ناله و خروشست امشب</p></div>
<div class="m2"><p>نه صبر پدیدست و نه هوشست امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوشم خوش بود ساعتی پنداری</p></div>
<div class="m2"><p>کفارت خوش دلی دو شست امشب</p></div></div>