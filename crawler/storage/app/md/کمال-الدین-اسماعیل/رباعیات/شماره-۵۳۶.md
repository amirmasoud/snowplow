---
title: >-
    شمارهٔ ۵۳۶
---
# شمارهٔ ۵۳۶

<div class="b" id="bn1"><div class="m1"><p>ای دانۀ دل نهاده در خوشۀ اشک</p></div>
<div class="m2"><p>در راه غمت شد سپری توشۀ اشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین چشم که بار داشت از قطرۀ اشک</p></div>
<div class="m2"><p>بر بوی تو بفکند جگر گوشۀ اشک</p></div></div>