---
title: >-
    شمارهٔ ۲۴۸
---
# شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>گر دیدۀ نرگس نه سبل می دارد</p></div>
<div class="m2"><p>بینایی او چرا خلل می دارد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بید ارنه سر خلاف دارد در باغ</p></div>
<div class="m2"><p>از بهر چه گربه در بغل می دارد</p></div></div>