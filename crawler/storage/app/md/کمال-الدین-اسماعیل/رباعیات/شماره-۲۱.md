---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ساقت که بآرزو پرستند او را</p></div>
<div class="m2"><p>همچون دل من چرا بخستند او را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زخمی که حوالت گه او بود دلم</p></div>
<div class="m2"><p>دردا گه برشته بر تو بستند او را</p></div></div>