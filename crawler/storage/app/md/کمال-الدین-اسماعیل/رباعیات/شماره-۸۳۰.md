---
title: >-
    شمارهٔ ۸۳۰
---
# شمارهٔ ۸۳۰

<div class="b" id="bn1"><div class="m1"><p>پیوسته ز بهر شهوت جسمانی</p></div>
<div class="m2"><p>این جان شریف را همی رنجانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آگاه نیی که آفت جان تواند</p></div>
<div class="m2"><p>آنها که تو در آرزوی ایشانی</p></div></div>