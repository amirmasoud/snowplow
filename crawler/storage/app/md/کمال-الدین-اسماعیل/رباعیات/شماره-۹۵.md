---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>تا تنگ دلم جای چو تو خوش پسرست</p></div>
<div class="m2"><p>الحق ز خوشی دلم چو تنگ شکرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا چو شکر ز تنگت از ناگزرست</p></div>
<div class="m2"><p>در دست من آی کز دلم تنگ ترست</p></div></div>