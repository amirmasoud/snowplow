---
title: >-
    شمارهٔ ۸۱۳
---
# شمارهٔ ۸۱۳

<div class="b" id="bn1"><div class="m1"><p>زین گونه که تو بدل ربایی فاشی</p></div>
<div class="m2"><p>عاشق خواهی ز سنگ صد بتراشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معشوقی تو بعشق کس نیست گرو</p></div>
<div class="m2"><p>خود هم تو سزد که عاشق خود باشی</p></div></div>