---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>گر بگشایم ز غصّه امشب لب را</p></div>
<div class="m2"><p>بر چرخ بسوزم از نفس کوکب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای صبح بیا تو نیز جانی می کن</p></div>
<div class="m2"><p>باشد که بهم روز کنیم این شب را</p></div></div>