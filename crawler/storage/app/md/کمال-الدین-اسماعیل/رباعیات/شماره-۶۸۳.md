---
title: >-
    شمارهٔ ۶۸۳
---
# شمارهٔ ۶۸۳

<div class="b" id="bn1"><div class="m1"><p>ای حکم ترا نهاده سرها گردن</p></div>
<div class="m2"><p>در جنبر طاعتت فلک را گردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این طرفه که دریای کفت را از تیغ</p></div>
<div class="m2"><p>آبیست بداندیش ترا تا گردن</p></div></div>