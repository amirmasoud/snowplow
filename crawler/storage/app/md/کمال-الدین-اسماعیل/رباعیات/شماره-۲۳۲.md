---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>زین سر که زبان دور باشت دارد</p></div>
<div class="m2"><p>خصمان ترا بگفت و گو نگدارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوسته ز خون دشمنان آب خورد</p></div>
<div class="m2"><p>این شاخ که مرگ ناگهان بار آرد</p></div></div>