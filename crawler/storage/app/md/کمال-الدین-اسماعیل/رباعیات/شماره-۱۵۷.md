---
title: >-
    شمارهٔ ۱۵۷
---
# شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>آمد بر من چو بر کفم زر پنداشت</p></div>
<div class="m2"><p>چون دید که زر نداشتم ره بگذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حلقۀ گوش او مرا شد معلوم</p></div>
<div class="m2"><p>کانجا که زرست گوش می باید داشت</p></div></div>