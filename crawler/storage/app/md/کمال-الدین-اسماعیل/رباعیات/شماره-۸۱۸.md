---
title: >-
    شمارهٔ ۸۱۸
---
# شمارهٔ ۸۱۸

<div class="b" id="bn1"><div class="m1"><p>ای خام که عادتت بود سنگدلی</p></div>
<div class="m2"><p>بس خوب و نکویی ، زچه آبی؟ چه گلی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرمی نکنی به بندۀ خود روزی</p></div>
<div class="m2"><p>ترسم که دعایی کنم از تنگ دلی</p></div></div>