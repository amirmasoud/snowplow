---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>گفتم که چو مست شد مرا ناز آرد</p></div>
<div class="m2"><p>گر بوسه زنم برایگان بگذارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسوس که همچو نرگس آن بینایی</p></div>
<div class="m2"><p>مست است و هنوز چشم زر می دارد</p></div></div>