---
title: >-
    شمارهٔ ۶۷۹
---
# شمارهٔ ۶۷۹

<div class="b" id="bn1"><div class="m1"><p>عمری بودم بخدمتت بسته میان</p></div>
<div class="m2"><p>در ساخته با غمت بهر سود و زیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر من چه جفاهای تو بینند عیان</p></div>
<div class="m2"><p>امروز چه گویتد ترا عالمیان؟</p></div></div>