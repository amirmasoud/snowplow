---
title: >-
    شمارهٔ ۷۷۳
---
# شمارهٔ ۷۷۳

<div class="b" id="bn1"><div class="m1"><p>گز زآنکه ترا بکشتنم باشد رای</p></div>
<div class="m2"><p>باشم سوی خویش مرگ را راهنمای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در اتش اگر رضای طبعت بینم</p></div>
<div class="m2"><p>چون شمع بسر شوم در آتش نه پای</p></div></div>