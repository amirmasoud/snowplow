---
title: >-
    شمارهٔ ۷۰۲
---
# شمارهٔ ۷۰۲

<div class="b" id="bn1"><div class="m1"><p>شمعم که شدست جان من دشمن من</p></div>
<div class="m2"><p>صد تو غم دل گرفته پیرامن من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر یاد لب تو وقت جان دادن من</p></div>
<div class="m2"><p>جان خنده زنان برون شود از تن من</p></div></div>