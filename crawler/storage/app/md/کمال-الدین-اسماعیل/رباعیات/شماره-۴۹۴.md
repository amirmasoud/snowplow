---
title: >-
    شمارهٔ ۴۹۴
---
# شمارهٔ ۴۹۴

<div class="b" id="bn1"><div class="m1"><p>در عالم هجر آن بت شکر پاش</p></div>
<div class="m2"><p>آوازۀ بی صبری من چون شد فاش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ارجاف کنی زکوی وصلش امروز</p></div>
<div class="m2"><p>در گوش دلم گفت که نومید مباش</p></div></div>