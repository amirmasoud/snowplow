---
title: >-
    شمارهٔ ۵۶۹
---
# شمارهٔ ۵۶۹

<div class="b" id="bn1"><div class="m1"><p>گر در همه عمر روزی از روی کرم</p></div>
<div class="m2"><p>گویی که چگونه بی تو با این همه غم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانم نشود هیچ ز خوبی تو کم</p></div>
<div class="m2"><p>کآخر نه برفت مردمی از عالم</p></div></div>