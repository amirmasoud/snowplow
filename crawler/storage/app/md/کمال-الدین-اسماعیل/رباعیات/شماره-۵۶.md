---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>تا هست ترا چراغ دانش در دست</p></div>
<div class="m2"><p>از پای طلب همی نباید بنشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا روشن گرددت که این جان شریف</p></div>
<div class="m2"><p>چون بگسلداز تو با که خواهد پیوست</p></div></div>