---
title: >-
    شمارهٔ ۳۴۷
---
# شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>هر کو سر و زر بیار تسلیم کند</p></div>
<div class="m2"><p>خود را ز غم فراق بی بیم کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دلبر خویش روی در روی آرد</p></div>
<div class="m2"><p>چون آینه هر که پشت بر سیم کند</p></div></div>