---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>عشق تو گرم چه غم فراوان آرد</p></div>
<div class="m2"><p>نندیشم اگر هزار چندان آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا کار غمت بسر برم مردانه</p></div>
<div class="m2"><p>با عشق تو روز من بپایان آرد</p></div></div>