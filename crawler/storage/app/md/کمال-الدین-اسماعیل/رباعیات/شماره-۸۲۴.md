---
title: >-
    شمارهٔ ۸۲۴
---
# شمارهٔ ۸۲۴

<div class="b" id="bn1"><div class="m1"><p>تنها ام و کاشکی تنی داشتمی</p></div>
<div class="m2"><p>یا زور بدی که بال بفراشتمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دوست بجای تو اگر من بدهی</p></div>
<div class="m2"><p>تنهات چنین روز بنگذاشتمی</p></div></div>