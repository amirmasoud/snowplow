---
title: >-
    شمارهٔ ۲۵۵
---
# شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>مهر تو نشان آب و گل می ببرد</p></div>
<div class="m2"><p>رخسار تو رونق چگل می ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان هندوی زلفت تو بچابک دستی</p></div>
<div class="m2"><p>ناگه زمیان چشم دل می ببرد</p></div></div>