---
title: >-
    شمارهٔ ۴۲۵
---
# شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>هنگام صبوحست حریفان خیزید</p></div>
<div class="m2"><p>وان باقی دوشین بقدح در ریزید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک لحظه ز بند نیک و بد بگریزید</p></div>
<div class="m2"><p>در بی خبریّ و بی خودی آویزید</p></div></div>