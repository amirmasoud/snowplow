---
title: >-
    شمارهٔ ۶۴۷
---
# شمارهٔ ۶۴۷

<div class="b" id="bn1"><div class="m1"><p>دور از تو اگر لب بنفس بگشایم</p></div>
<div class="m2"><p>از بی زوری با دم خود برنایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خسته تنم را کمری آرایم</p></div>
<div class="m2"><p>افتد چو قلم زلاغری در پایم</p></div></div>