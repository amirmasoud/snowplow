---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>آن بت که جهان بغمزۀ مست گرفت</p></div>
<div class="m2"><p>زان پس که دلم بزلف چون شست گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دست گرفت باز تا جان شکرد</p></div>
<div class="m2"><p>این شیوه نگر که باز بر دست گرفت</p></div></div>