---
title: >-
    شمارهٔ ۶۳۴
---
# شمارهٔ ۶۳۴

<div class="b" id="bn1"><div class="m1"><p>گرمن ز غمت حکایت آغاز کنم</p></div>
<div class="m2"><p>با خود دل خلقی بغم انباز کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون در دل من فسرده بینی ده تو</p></div>
<div class="m2"><p>چون غنچه اگر من سر دل باز کنم</p></div></div>