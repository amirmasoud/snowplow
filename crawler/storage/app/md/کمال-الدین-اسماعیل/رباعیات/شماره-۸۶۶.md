---
title: >-
    شمارهٔ ۸۶۶
---
# شمارهٔ ۸۶۶

<div class="b" id="bn1"><div class="m1"><p>ای ترک چرا چهره برافروخته‌ای</p></div>
<div class="m2"><p>خود از همه چیز کینه اندوخته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این تنگ فرو گرفتنت بر مردم</p></div>
<div class="m2"><p>مانا که ز چشم خویش آموخته‌ای</p></div></div>