---
title: >-
    شمارهٔ ۸۲۱
---
# شمارهٔ ۸۲۱

<div class="b" id="bn1"><div class="m1"><p>بگذشت بمن رشک بتان چگلی</p></div>
<div class="m2"><p>پوشیده رخ از نقاب چشم از چگلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا که ز عشق من نمردی تو هنوز؟</p></div>
<div class="m2"><p>ای سخت دل سست امید، ازچه گلی؟</p></div></div>