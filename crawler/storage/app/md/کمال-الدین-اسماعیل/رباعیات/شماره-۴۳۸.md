---
title: >-
    شمارهٔ ۴۳۸
---
# شمارهٔ ۴۳۸

<div class="b" id="bn1"><div class="m1"><p>بستند گرو با نفس مشک تتار</p></div>
<div class="m2"><p>بوی گل و باد سحر وزلف نگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستند بر غم یکدگر گوهر بار</p></div>
<div class="m2"><p>چشم من و لعل یارم و ابر بهار</p></div></div>