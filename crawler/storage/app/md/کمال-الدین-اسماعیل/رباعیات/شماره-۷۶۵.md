---
title: >-
    شمارهٔ ۷۶۵
---
# شمارهٔ ۷۶۵

<div class="b" id="bn1"><div class="m1"><p>ماییم بدست دل گرفتار همه</p></div>
<div class="m2"><p>تسبیه گسسته بسته زنّار همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پوشیده خشتها و شکم پر باده</p></div>
<div class="m2"><p>مانند قرابه پنبه دستار همه</p></div></div>