---
title: >-
    شمارهٔ ۵۵۵
---
# شمارهٔ ۵۵۵

<div class="b" id="bn1"><div class="m1"><p>با رنگ رخ تو بر سمن خندد دل</p></div>
<div class="m2"><p>جز زلف تو جای خویش نپسندد دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زلف تو دانی بچه پیوندد دل؟</p></div>
<div class="m2"><p>خود را برسن بر تو همی بندد دل</p></div></div>