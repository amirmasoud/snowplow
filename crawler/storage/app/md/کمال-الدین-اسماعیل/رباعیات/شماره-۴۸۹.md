---
title: >-
    شمارهٔ ۴۸۹
---
# شمارهٔ ۴۸۹

<div class="b" id="bn1"><div class="m1"><p>آخر خبری زین دل تنگ بپرس</p></div>
<div class="m2"><p>شرح غمم از طرۀ شبرنگ بپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا چو خوری باده بهنگام صبوح</p></div>
<div class="m2"><p>احوال دلم ز نالۀ چنگ بپرس</p></div></div>