---
title: >-
    شمارهٔ ۵۶۲
---
# شمارهٔ ۵۶۲

<div class="b" id="bn1"><div class="m1"><p>عشّاق بر آمدند پیرامن گل</p></div>
<div class="m2"><p>یکباره زدند دست در دامن گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز بس که همی کشند پیراهن گل</p></div>
<div class="m2"><p>آنک بهزار شاخ شد بر تن گل</p></div></div>