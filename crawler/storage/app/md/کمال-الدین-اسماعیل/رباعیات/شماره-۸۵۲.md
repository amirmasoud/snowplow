---
title: >-
    شمارهٔ ۸۵۲
---
# شمارهٔ ۸۵۲

<div class="b" id="bn1"><div class="m1"><p>دستی که بر او همی می ناب نهی</p></div>
<div class="m2"><p>دل میدهدت که خیره در تاب نهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انصاف بده چه چرب دست استادی</p></div>
<div class="m2"><p>کز آتش ناب مهر بر آب نهی</p></div></div>