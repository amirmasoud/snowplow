---
title: >-
    شمارهٔ ۸۴۲
---
# شمارهٔ ۸۴۲

<div class="b" id="bn1"><div class="m1"><p>هم بر سر آن نیی که ما را بینی</p></div>
<div class="m2"><p>وان حال که دیده یی یکی وا بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخیز و بنظّارۀ احوال من آی</p></div>
<div class="m2"><p>گر دل دهدت که درد دلها بینی</p></div></div>