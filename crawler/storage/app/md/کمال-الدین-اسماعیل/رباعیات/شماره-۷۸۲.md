---
title: >-
    شمارهٔ ۷۸۲
---
# شمارهٔ ۷۸۲

<div class="b" id="bn1"><div class="m1"><p>ای دل بشبی که با غمش بنشستی</p></div>
<div class="m2"><p>از ناله خروشی بجهان دربستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خامش چو پیاله با دل پر خون باش</p></div>
<div class="m2"><p>تا چند چو چنگ نالۀ سردستی؟</p></div></div>