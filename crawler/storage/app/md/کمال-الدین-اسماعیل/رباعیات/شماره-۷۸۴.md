---
title: >-
    شمارهٔ ۷۸۴
---
# شمارهٔ ۷۸۴

<div class="b" id="bn1"><div class="m1"><p>از بس که بلای دل ما می جستی</p></div>
<div class="m2"><p>از خطّ تو در کار تو آمد سستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خط که سیه باد چو روزم رویت</p></div>
<div class="m2"><p>ناگه ز کجا بروی او بررستی</p></div></div>