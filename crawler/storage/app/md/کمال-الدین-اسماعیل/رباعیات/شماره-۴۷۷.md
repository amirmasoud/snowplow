---
title: >-
    شمارهٔ ۴۷۷
---
# شمارهٔ ۴۷۷

<div class="b" id="bn1"><div class="m1"><p>کار همه دنیا بمرادت شده گیر</p></div>
<div class="m2"><p>پس عمر برفته و اجل آمده گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی بمراد خویش دستی بزنم</p></div>
<div class="m2"><p>خود نتوانی و گر توانی زده گیر</p></div></div>