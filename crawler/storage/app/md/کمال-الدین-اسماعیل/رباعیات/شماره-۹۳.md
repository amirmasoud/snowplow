---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>در بند جهان کسی که او بیشترست</p></div>
<div class="m2"><p>چون زلف تو آشفته و آسیمه سرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون چشم تو آن خوشست در عالم، کو</p></div>
<div class="m2"><p>مستست چنان که از جهان بی خبرست</p></div></div>