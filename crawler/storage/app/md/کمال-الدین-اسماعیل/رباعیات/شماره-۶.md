---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>شمعم که زمن هست اثر غم پیدا</p></div>
<div class="m2"><p>شد سوز دلم زچشم پر نم پیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآن نیست مرا سوز زماتم پیدا</p></div>
<div class="m2"><p>کم گریه و خنده نیست از هم پیدا</p></div></div>