---
title: >-
    شمارهٔ ۶۷۵
---
# شمارهٔ ۶۷۵

<div class="b" id="bn1"><div class="m1"><p>چون نیشکرم کرده ز بیداد جهان</p></div>
<div class="m2"><p>در سینۀ تنگ لفظ شیرین پنهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خاموشی چو پسته در کنج دهان</p></div>
<div class="m2"><p>زنگار گرفتست مرا تیغ زبان</p></div></div>