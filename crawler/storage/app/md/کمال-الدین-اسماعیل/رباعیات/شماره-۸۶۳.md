---
title: >-
    شمارهٔ ۸۶۳
---
# شمارهٔ ۸۶۳

<div class="b" id="bn1"><div class="m1"><p>آگاه ز حال من سر گشته نیی</p></div>
<div class="m2"><p>کز عشق چو من زیرو زبر گشته نیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن روی چو روز را مگردان از من</p></div>
<div class="m2"><p>شکرانۀ آنکه روز برگشته نیی</p></div></div>