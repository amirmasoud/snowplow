---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>جایی که در بقا فرازست آنجا</p></div>
<div class="m2"><p>رمح تو ز لاف سرفرازست آنجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آنجا که جواب مشکلی باید داد</p></div>
<div class="m2"><p>شمشیر ترا زبان درازست آنجا</p></div></div>