---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>ای عزم تو بر شکستن عهد درست</p></div>
<div class="m2"><p>ز آمد شدن تو پای اومید سست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوابی که چو آیی کنم از چشمت جای</p></div>
<div class="m2"><p>اشکی که چو می روی همه دل با تست</p></div></div>