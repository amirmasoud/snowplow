---
title: >-
    شمارهٔ ۷۸۶
---
# شمارهٔ ۷۸۶

<div class="b" id="bn1"><div class="m1"><p>مشتاق بنیستی ّ من گر هستی</p></div>
<div class="m2"><p>ما بگزینیمم نیستی بر هستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو لطف کن و در نطر دشمن و دوست</p></div>
<div class="m2"><p>روزی خوشکم بگو که خوشتر هستی؟</p></div></div>