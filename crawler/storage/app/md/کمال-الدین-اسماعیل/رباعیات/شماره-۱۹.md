---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>آنها که فلک وفا نداد ایشان را</p></div>
<div class="m2"><p>وصل من و تو بد اوفتاد ایشانرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهند مرا ز خدمتت باز برند</p></div>
<div class="m2"><p>یا رب که زبان بریده باد ایشانرا</p></div></div>