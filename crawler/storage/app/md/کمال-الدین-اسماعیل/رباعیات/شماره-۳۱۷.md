---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>بیداد جهان بسر نخواهد آمد</p></div>
<div class="m2"><p>و اندوه تو کارگر نخواهد آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد چون تو بخاک اگر فرو خواهد شد</p></div>
<div class="m2"><p>هم آب بآب بر نخواهد آمد</p></div></div>