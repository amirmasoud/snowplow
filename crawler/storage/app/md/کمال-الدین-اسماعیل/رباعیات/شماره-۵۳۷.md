---
title: >-
    شمارهٔ ۵۳۷
---
# شمارهٔ ۵۳۷

<div class="b" id="bn1"><div class="m1"><p>در شوق تو از شرح و بیان من و کلک</p></div>
<div class="m2"><p>یکباره سپید شد زبان من و کلک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه زانکه گه گهی خاموشست</p></div>
<div class="m2"><p>تیغست همه ساله میان من و کلک</p></div></div>