---
title: >-
    شمارهٔ ۸۳۴
---
# شمارهٔ ۸۳۴

<div class="b" id="bn1"><div class="m1"><p>ای دوست مرا اگر چه دشمن دانی</p></div>
<div class="m2"><p>حال دل من تو بهتر از من دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود نیست ز تو امید رحمت ورنی</p></div>
<div class="m2"><p>حال شب من چو روز روشن دانی</p></div></div>