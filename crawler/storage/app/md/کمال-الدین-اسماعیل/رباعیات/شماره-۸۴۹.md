---
title: >-
    شمارهٔ ۸۴۹
---
# شمارهٔ ۸۴۹

<div class="b" id="bn1"><div class="m1"><p>دی گفت مرا حدیث من کمتر گوی</p></div>
<div class="m2"><p>ور می گویی بیا بگوشم در گوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنمود مرا حلقة زرّین در گوش</p></div>
<div class="m2"><p>یعنی که حدیث وصل من بازر گوی</p></div></div>