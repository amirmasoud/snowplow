---
title: >-
    شمارهٔ ۷۴۳
---
# شمارهٔ ۷۴۳

<div class="b" id="bn1"><div class="m1"><p>کس نیست که جان بنده برهاند ازو</p></div>
<div class="m2"><p>یا داد من دل شده بستاند ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبحان الله که نیست سرتاپایش</p></div>
<div class="m2"><p>عیبی که دلم عنان بگرداند ازو</p></div></div>