---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>آتش که بطبع جان کداز آمده است</p></div>
<div class="m2"><p>وز روی نهاد سرفراز آمده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میخواست که بر دل منش دست بود</p></div>
<div class="m2"><p>بنگر بکدام دست باز آمده است</p></div></div>