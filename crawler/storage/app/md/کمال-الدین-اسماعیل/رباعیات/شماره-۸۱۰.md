---
title: >-
    شمارهٔ ۸۱۰
---
# شمارهٔ ۸۱۰

<div class="b" id="bn1"><div class="m1"><p>ما راست دلی که نیست خالی نفسی</p></div>
<div class="m2"><p>هر گوشۀ او ز سر صاحب هوسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آیینه ییست ور بجویند بسی</p></div>
<div class="m2"><p>جز خویشتن اندرو نبینند کسی</p></div></div>