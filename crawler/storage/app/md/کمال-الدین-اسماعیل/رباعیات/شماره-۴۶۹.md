---
title: >-
    شمارهٔ ۴۶۹
---
# شمارهٔ ۴۶۹

<div class="b" id="bn1"><div class="m1"><p>ای بحر کف تو چون امل پنهاور</p></div>
<div class="m2"><p>لطف تو میان آب و آتش داور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که کفت داروی حرمان بخشد</p></div>
<div class="m2"><p>از حال من شکسته دل یادآور</p></div></div>