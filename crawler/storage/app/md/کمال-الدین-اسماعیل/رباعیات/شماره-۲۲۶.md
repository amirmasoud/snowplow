---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>گل راز طرب همه دهان می خندد</p></div>
<div class="m2"><p>گویی ز برای چه چنان می خندد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آری همه کار کش ببرگست ز زر</p></div>
<div class="m2"><p>زان خفت ستان و بر جهان می خندد</p></div></div>