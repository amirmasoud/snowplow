---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>زابشخور وصل بهره اشکست مرا</p></div>
<div class="m2"><p>گلگونۀ رنگ چهره اشکست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون چرخ ز خورشید و ستاره شب و روز</p></div>
<div class="m2"><p>چشمی و هزار قطره اشکست مرا</p></div></div>