---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>خون دل من بتم بصد ناز خورد</p></div>
<div class="m2"><p>مانند پیاله یی کز آغاز خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانم چو پیاله بر لب آمد بامید</p></div>
<div class="m2"><p>باشد که دمی لبش بمن باز خورد</p></div></div>