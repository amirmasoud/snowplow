---
title: >-
    شمارهٔ ۶۰۵
---
# شمارهٔ ۶۰۵

<div class="b" id="bn1"><div class="m1"><p>در مدح ملک چو نظم موزون سازم</p></div>
<div class="m2"><p>هر نکته درو چو درّ مکنون سازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپذیر مرا ببندگی تا بینی</p></div>
<div class="m2"><p>در مدح تو دیوان سخن چون سازم</p></div></div>