---
title: >-
    شمارهٔ ۶۶۲
---
# شمارهٔ ۶۶۲

<div class="b" id="bn1"><div class="m1"><p>ماییم نگین خاتم جان ماییم</p></div>
<div class="m2"><p>ماییم که بیگار خرد فرماییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آتش غم چو شمع از آن فرساییم</p></div>
<div class="m2"><p>کز جسم بکاهیم و بجان افزاییم</p></div></div>