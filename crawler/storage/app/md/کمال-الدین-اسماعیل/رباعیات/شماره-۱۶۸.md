---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>اطراف چمن لالۀ دلکش بگرفت</p></div>
<div class="m2"><p>وز جنبش باد آتش خوش بگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آتش زنۀ ابر ز بس شعلۀ برق</p></div>
<div class="m2"><p>حرّافۀ گل سربسر آتش بگرفت</p></div></div>