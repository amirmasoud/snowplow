---
title: >-
    شمارهٔ ۳۳۴
---
# شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>چشم و دل من زبس که پر غم شده اند</p></div>
<div class="m2"><p>در تاب فتاده اند و پر نم شده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانگه ز برای کشتن آتش غم</p></div>
<div class="m2"><p>خون دل و آب دیده در هم شده اند</p></div></div>