---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>شاید که دلم بمن نمی پردازد</p></div>
<div class="m2"><p>کز غصه بخویشتن نمی پردازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی که چرا نیم گریزد ز غمت؟</p></div>
<div class="m2"><p>کز غم بگریختن نمی پردازد</p></div></div>