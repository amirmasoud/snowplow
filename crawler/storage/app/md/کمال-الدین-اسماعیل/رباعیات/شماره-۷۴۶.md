---
title: >-
    شمارهٔ ۷۴۶
---
# شمارهٔ ۷۴۶

<div class="b" id="bn1"><div class="m1"><p>از بند سخن لبم چو بگشاد گره</p></div>
<div class="m2"><p>از خشم در ابروانش افتاد گره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بادست حدیث من واو آب لطیف</p></div>
<div class="m2"><p>بر آب فتد ز جنبش باد گرده</p></div></div>