---
title: >-
    شمارهٔ ۸۳۳
---
# شمارهٔ ۸۳۳

<div class="b" id="bn1"><div class="m1"><p>خواهی که جهان زیروز بر گردانی</p></div>
<div class="m2"><p>تاز و تن خویش بهره ور گردانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرمت ناید این همه سرگردانی</p></div>
<div class="m2"><p>تا لقمۀ خاک چرب تر گردانی</p></div></div>