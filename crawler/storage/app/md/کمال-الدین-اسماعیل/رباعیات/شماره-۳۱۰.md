---
title: >-
    شمارهٔ ۳۱۰
---
# شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>آن شمع دراز قد که جز سر نکشد</p></div>
<div class="m2"><p>بیش از یک شب عمروی اندر نکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ده تو دارد جامه و از سر سبکی</p></div>
<div class="m2"><p>می جوشد مغزش و یکی بر نکشد</p></div></div>