---
title: >-
    شمارهٔ ۶۷۸
---
# شمارهٔ ۶۷۸

<div class="b" id="bn1"><div class="m1"><p>از باد ببین شکوفه را شبگیران</p></div>
<div class="m2"><p>لرزنده شده چو دست و پای پیران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آن سایۀ برگ بید بر روی شمر</p></div>
<div class="m2"><p>همچون ماهی بشست ماهی گیران</p></div></div>