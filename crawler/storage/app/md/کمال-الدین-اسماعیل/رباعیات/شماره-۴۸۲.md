---
title: >-
    شمارهٔ ۴۸۲
---
# شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>خرم رخ تو دژم مبادا هرگز</p></div>
<div class="m2"><p>شادان دل تو بغم مبادا هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو گر تنم شد از مویی کم</p></div>
<div class="m2"><p>مویی زسر تو کم مباداهرگز</p></div></div>