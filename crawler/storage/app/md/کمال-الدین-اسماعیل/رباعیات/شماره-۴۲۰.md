---
title: >-
    شمارهٔ ۴۲۰
---
# شمارهٔ ۴۲۰

<div class="b" id="bn1"><div class="m1"><p>چشم ز میان تو نشان هیچ ندید</p></div>
<div class="m2"><p>بیش از کمر تو در میان هیچ ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچست دهان تو و در عالم لطف</p></div>
<div class="m2"><p>هر کس که ندید آن دهان هیچ ندید</p></div></div>