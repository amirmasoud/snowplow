---
title: >-
    شمارهٔ ۵۷۳
---
# شمارهٔ ۵۷۳

<div class="b" id="bn1"><div class="m1"><p>ای شب بستان درازی از صبح بوام</p></div>
<div class="m2"><p>مگذار که باز خندد امشب لب بام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عمر خود امشبم بدام آمد مرغ</p></div>
<div class="m2"><p>گر صبح بخندد بر مد مرغ از دام</p></div></div>