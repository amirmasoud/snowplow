---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>ای غمزۀ تیز تو جگر خواره</p></div>
<div class="m2"><p>وی لعل تو طیرۀ شکر پاره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم وعدۀ تو دراز بی حاصل</p></div>
<div class="m2"><p>هم چشم ضعیف تو ستمکاره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگشته بصد هزار نومیدی</p></div>
<div class="m2"><p>از کوی تو عاشقان بیچاره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچون گل و لاله و خور پیشت</p></div>
<div class="m2"><p>بر خاک همی نهند رخساره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگذار که زلفکان هندویت</p></div>
<div class="m2"><p>ناهمواری کنند همواره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر گه که کنم حدیث وصل تو</p></div>
<div class="m2"><p>گوید غم تو که هان، دگر باره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد در دل ما بدولت عشقت</p></div>
<div class="m2"><p>غم خانه نشین و صبر آواره</p></div></div>