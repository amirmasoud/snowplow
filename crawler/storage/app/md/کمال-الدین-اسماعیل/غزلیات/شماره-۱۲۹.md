---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>نگارا چند ازین پیمان شکستن</p></div>
<div class="m2"><p>ز پیشانی دل سندان شکستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمان ابروان در هم کشیدن</p></div>
<div class="m2"><p>وزو در جان من پیکان شکستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر زلف تو ان نا تندرستت</p></div>
<div class="m2"><p>که باشد عادتش پیمان شکستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبت را رسم باشد گاه خنده</p></div>
<div class="m2"><p>گهر را کار در دندان شکستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکر را عیش شیرین تلخ کردن</p></div>
<div class="m2"><p>قدح را خنده اندر جان شکستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهانت راست عادت وقت گفتار</p></div>
<div class="m2"><p>زشکّر پستۀ خندان شکستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم زندان غم گشتست و این راست</p></div>
<div class="m2"><p>همیشه عادت زندان شکستن؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه مردی باشد اندر عهد بستن</p></div>
<div class="m2"><p>بدشواری و پس آسان شکستن؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدین سستی که پیمان تو باشد</p></div>
<div class="m2"><p>بیک ساعت دو صد بتوان شکستن</p></div></div>