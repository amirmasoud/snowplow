---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>یا رب! این بچّهٔ ترکان چه ز ما می‌خواهند؟</p></div>
<div class="m2"><p>که همیشه دل ما را به بلا می‌خواهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف پرچین ز چه بر زیر کله می‌شکنند؟</p></div>
<div class="m2"><p>گر نه مان بسته ترا ز چین قبا می‌خواهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز اسب و زره و تیغ و کمر می‌طلبند</p></div>
<div class="m2"><p>شب شراب و قدح و زیر و دوتا می‌خواهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ده منی گرز چو از دست بمی اندازند</p></div>
<div class="m2"><p>یک منی ساغر در حال فرا می‌خواهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز چون از پی بازی سوی میدان تازند</p></div>
<div class="m2"><p>گوی و چوگان ز دل و قامت ما می‌خواهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلف چون چوگان دارند و زنخدان چو گوی</p></div>
<div class="m2"><p>پس ز ما عاریت این هر دو چرا می‌خواهند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفت هوش و روانند و بلای دل و دین</p></div>
<div class="m2"><p>وآنگه ایشان را مردم به دعا می‌خواهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اصلشان چون ز خطا باشد بر اصل خطا</p></div>
<div class="m2"><p>لاجرم بوسه بها جزو خطا می‌خواهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رایگانی به تو کی بوسه دهند؟ آن قومی</p></div>
<div class="m2"><p>کز پی بچّهٔ خود شیربها می‌خواهند</p></div></div>