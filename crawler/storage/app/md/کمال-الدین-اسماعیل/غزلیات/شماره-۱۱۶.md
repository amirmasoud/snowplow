---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>عید کنون عید شد که روی تو دیدم</p></div>
<div class="m2"><p>کار کنون راست شد که در تو رسیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با چه برابر کنم چنین دو سعادت</p></div>
<div class="m2"><p>من که مه عید را بروی تو دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان و جوانی بباد دادم از یراک</p></div>
<div class="m2"><p>بوی سر زلف تو زیاد شنیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هوس آن که بر خط تو نهم سر</p></div>
<div class="m2"><p>سوی تو همچون قلم بفرق دویدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راه چو زلفت دراز بود و چو شانه</p></div>
<div class="m2"><p>پای شدم جمله و بسر ببریدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرح یکی از هزار هم نتوان داد</p></div>
<div class="m2"><p>آنچه من از دست فرقت تو کشیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در طلب آفتاب روی تو چون صبح</p></div>
<div class="m2"><p>دم نزدم من که پیرهن ندریدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دولت وصل تو یار من شد و آخر</p></div>
<div class="m2"><p>جان خود از دست هجر باز خریدم</p></div></div>