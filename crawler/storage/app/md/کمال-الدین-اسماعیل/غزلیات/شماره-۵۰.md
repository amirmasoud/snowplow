---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>دلبرم رسم خود چنین دارد</p></div>
<div class="m2"><p>که دل دوستان غمین دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پی یک حدیث دامن گیر</p></div>
<div class="m2"><p>صد جواب اندر آستین دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقة زلف او ز بلعجبی</p></div>
<div class="m2"><p>چرخ پیروزه در نگین دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف پرچین زنگی آسایش</p></div>
<div class="m2"><p>حکم بر زنگبار و چین دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نز تواضع، ز سرکشی باشد</p></div>
<div class="m2"><p>سر که پیش تو بر زمین دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشود خود به کوی او نزدیک</p></div>
<div class="m2"><p>هر که او عقل دوربین دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد کوی وی آنکسی گردد</p></div>
<div class="m2"><p>که ملالش ز عقل و دین دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نخورد هیچ غم بهستی خویش</p></div>
<div class="m2"><p>هر که دلدار نازنین دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تن مومین نپرورد چون شمع</p></div>
<div class="m2"><p>هر که او جان آتشین دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برهاند ترا ز هستی خویش</p></div>
<div class="m2"><p>عشق خود خاصیت همین دارد</p></div></div>