---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>با لب تو جان شکار زلف تست</p></div>
<div class="m2"><p>با رخ تو کار کار زلف تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمۀ خورشید سوی روی تو</p></div>
<div class="m2"><p>حلقۀ شب گوشوار زلف تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عروسیّ جمالت عقل را</p></div>
<div class="m2"><p>دست و پنجه در نگار زلف تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر ز عتبر شد کنار عارضت</p></div>
<div class="m2"><p>آن نه خطّست ، آن نثار زلف تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چه گویم؟ کز رخت روشنتر ست</p></div>
<div class="m2"><p>فتنه کاندر روزگار زلف تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد هزاران دل ربودی و هنوز</p></div>
<div class="m2"><p>شست و پنجه در شمار زلف تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برزر رخسارم از شنگرف اشک</p></div>
<div class="m2"><p>نقش شد کین دستکار زلف تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالمی عشّاق را از مرد و زن</p></div>
<div class="m2"><p>آرزو اندر کنار زلف تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا زنخدان تو چاه یوسفست</p></div>
<div class="m2"><p>جان ما زنجیر دار زلف تست</p></div></div>