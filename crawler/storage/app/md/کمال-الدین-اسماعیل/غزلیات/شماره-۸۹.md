---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>هر شبی از سر شک من، دامن خاک تر شود</p></div>
<div class="m2"><p>شاد شوم اگر ترا ، از غم من خبر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی تو تنم ز لاغری، گشت برای صفت که گر</p></div>
<div class="m2"><p>دست در آه من زند، تا بستاره برشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سحری که آورد، باد نسیم زلف تو</p></div>
<div class="m2"><p>جان بکنار لب دود، دیده برهگذر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آتش دل مرا جگر، خون شد و باز این عجب</p></div>
<div class="m2"><p>کزدم سرد هر نفس ، خون دلم جگر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک درت ز کیمیا، هست عزیزتر که چون</p></div>
<div class="m2"><p>در رخ و چشم مالمش، جمله زر و گهر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سر زلف تو دلم، نیک بدست کرد جای</p></div>
<div class="m2"><p>بد نبود گرش همی ، کار چنین بسر شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نامده در دو چشم من، خاک در تو ازمژه</p></div>
<div class="m2"><p>اشک برخ فرو دود، زود بسجده درشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لابۀ ما چو بشنود، زلف تو دل چه جان کند</p></div>
<div class="m2"><p>کور دلا که بهر صید، از پی مارگر شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون دلم همی رود، در سر دیده دم بدم</p></div>
<div class="m2"><p>عاقبتش همین بود، دل که پی نظر شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق چو رخ نمود خود، کم نبود بلاو غم</p></div>
<div class="m2"><p>کین همه عادت آن بود، کز پی یکدگر شود</p></div></div>