---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>گشت آشکاره راز دلم بر زبان اشک</p></div>
<div class="m2"><p>از چشم خلق ازآن بفتاد بسان اشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افگنده پاره پاره دلم در دهان خلق</p></div>
<div class="m2"><p>زان پاره پاره می‌نهمش در دهان اشک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بردوختست چشم من از خواب تا کشید</p></div>
<div class="m2"><p>در تار سوزن مژه از ریسمان اشک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زانکه که گشت سینۀ من منزل غمت</p></div>
<div class="m2"><p>می نگسلد ز دان من کاروان اشک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفراویت رنگ رخان در فراق او</p></div>
<div class="m2"><p>از بهر آن همی دهمش ناردان اشک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ناردانه یی که در او استخوان بود</p></div>
<div class="m2"><p>پنهان شدست شخص من اندر میان اشک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان هر زمان بروی درآید سرشک من</p></div>
<div class="m2"><p>کز دست اختیار برون شد عنان اشک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بر رخت بنفشه و گلنار بردمید</p></div>
<div class="m2"><p>می بشکفت ز نرگس من ارغوان اشک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل درمیان اشک و تواندر میان دل</p></div>
<div class="m2"><p>پیداست رنگ چهرۀ توازنهان اشک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خون دلم هدر شد از بس که هر زمان</p></div>
<div class="m2"><p>فتوی دهد بخون دل من زبان اشک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر گوشه‌ای که من بگریزم ز دست غم</p></div>
<div class="m2"><p>آرد غم تو پی به سرم بر نشان اشک</p></div></div>