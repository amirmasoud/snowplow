---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>کجایی ای به دو رُخ آفتابِ دلداری؟</p></div>
<div class="m2"><p>چگونه‌ای که نه‌ای هیچ جایْ دیداری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا و خوی فرا مردمیّ و مردم کن</p></div>
<div class="m2"><p>که هیچ حاصل ناید ز مردم‌آزاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حکایتِ غمِ دل با تو من چرا گویم؟</p></div>
<div class="m2"><p>تو خود ز حالِ من و دل فراغتی داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کارِ عشقِ تو در، هستم آن‌چنان بیدار</p></div>
<div class="m2"><p>که کارِ من همه بی‌خوابی است و غمخواری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو حالِ بنده چه دانی؟ که بگذرد شب‌ها</p></div>
<div class="m2"><p>که نرگسِ تو نبیند به خواب، بیداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آفتابِ فلک، پیشِ من عزیزتری</p></div>
<div class="m2"><p>وگرچه دایم در پرده، سایه‌کرداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا که آرزویِ آفتابِ خانگی است،</p></div>
<div class="m2"><p>چه گَرد خیزد ازین آفتابِ بازاری؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به زیرِ زلفِ تو منزل گرفت نیکویی</p></div>
<div class="m2"><p>ز چشمِ مستِ تو پرهیز کرد هشیاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شود سیاهیِ شب شُسته از رخِ عالَم</p></div>
<div class="m2"><p>گر آبِ روی تو را اشکِ من کند یاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولی چه سود؟ که هر لحظه چرخ آموزد</p></div>
<div class="m2"><p>ز عکسِ زلفتِ تو و بخت من سیه‌کاری</p></div></div>