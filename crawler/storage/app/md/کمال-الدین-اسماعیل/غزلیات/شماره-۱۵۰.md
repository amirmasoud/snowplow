---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>بار دیگر ز که می آموزی</p></div>
<div class="m2"><p>این که دلها بجفا می سوزی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می دری پرده و می سوزی دل</p></div>
<div class="m2"><p>بر غمزه زکین اندوزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طالعی بد بود آن شب که دلم</p></div>
<div class="m2"><p>بتو دادم ز پی بهرورزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا زنی در دلم آتش بادب</p></div>
<div class="m2"><p>ازده انگشت چراغ افروزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خه خه، ای دلبر درّا دوزا</p></div>
<div class="m2"><p>خوب می دّری و خوش می دوزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندکی لطف بیاموز آخر</p></div>
<div class="m2"><p>خود همه جور و جفا آموزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه خط با رخ زیبای تو کرد</p></div>
<div class="m2"><p>کینه از سینۀ من می توزی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه عشوةۀ تو دانم چیست</p></div>
<div class="m2"><p>بی وفاییم همی آموزی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر سالست، مرا از رخ تو</p></div>
<div class="m2"><p>نظری رسم بود نوروزی</p></div></div>