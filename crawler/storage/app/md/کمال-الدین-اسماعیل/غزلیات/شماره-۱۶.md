---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>رخت از ماه و لبت از شکرست</p></div>
<div class="m2"><p>آنت ازین وینت از آن خوبترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر رخت بوسه کجا شاید داد؟</p></div>
<div class="m2"><p>که نظر نیز محل نظرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه میان نیست میان تو ز لطف</p></div>
<div class="m2"><p>وین عجبتر که میان کمرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا لبت را نرسد چشم بدان</p></div>
<div class="m2"><p>در زبانها همه نام شکرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوسه یی از لب و دندان خوشت</p></div>
<div class="m2"><p>خون بهای دو جهان خوش پسرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چه ناشسته رخم می خوانی</p></div>
<div class="m2"><p>که رخم شسته بخون جگرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدکنی با من و گویم که مکن</p></div>
<div class="m2"><p>گوئیم نیکست، اینم بترست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با رخ و غمزۀ تو می سازم</p></div>
<div class="m2"><p>که گل و خارتو با یکدگرست</p></div></div>