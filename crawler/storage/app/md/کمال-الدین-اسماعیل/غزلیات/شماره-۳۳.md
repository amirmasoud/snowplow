---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>ای مرا کرده مشوّش زلفت</p></div>
<div class="m2"><p>وی ز گل ساخته مفرش زلفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که در خسته دل ما پیوست</p></div>
<div class="m2"><p>نیست خالی ز کشاکش زلفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد ز بیماری چشمت آگاه</p></div>
<div class="m2"><p>هست از آن روی بر آتش زلفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز خوش را دل من شب خوش کرد</p></div>
<div class="m2"><p>تا که او راست شب خوش زلفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نور خورشید نهان شد چو فکند</p></div>
<div class="m2"><p>سایه یی بر رخ مه وش زلفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن چه خطّست که گویی که بمشک</p></div>
<div class="m2"><p>سیم را کرد منقش زلفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم ان زلف بگیرم یک شب</p></div>
<div class="m2"><p>گشت از اندیشه مشوّش زلفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرز حال دل من می پرسی</p></div>
<div class="m2"><p>آنک آنک سوی خودکشی زلفت</p></div></div>