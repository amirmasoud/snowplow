---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>گر ترا گویم که عاشق نیستم</p></div>
<div class="m2"><p>یا ز جان یار موافق نیستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از منت باور مبادا این سخن</p></div>
<div class="m2"><p>زانک در این قول صادق نیستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقم، عاشق، بآواز بلند</p></div>
<div class="m2"><p>پس که باشم من که عاشق نیستم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو بحسن افزونی از عذرا و من</p></div>
<div class="m2"><p>در غم تو کم ز وامق نیستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق تو یکچند می کردم نهان</p></div>
<div class="m2"><p>زانکه دانستم که لایق نیستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آشکارا کردم اکنون راز خویش</p></div>
<div class="m2"><p>وندرین دعوی منافق نیستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که در عالم ترا عاشق شدند</p></div>
<div class="m2"><p>من به از چندین خلایق نیستم</p></div></div>