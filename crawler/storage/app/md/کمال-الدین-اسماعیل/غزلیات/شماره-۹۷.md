---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>لشکر نوروز بصحرا رسید</p></div>
<div class="m2"><p>موسم شادیّ و تماشا رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آمدن گل ببشارت ز پیش</p></div>
<div class="m2"><p>عید رسید اینک و زیبا رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزه شبانگاه بزد طبل کوچ</p></div>
<div class="m2"><p>بر سر این طارم مینا رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بادة نوروز و گلشن همرهند</p></div>
<div class="m2"><p>عید مبارک نه بتنها رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چمن از بس خوشی و رنگ بوی</p></div>
<div class="m2"><p>موکب گل یا برسد، یا رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باده بیاور که درین انتظار</p></div>
<div class="m2"><p>جان پیاله بلب ما رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو چو زد دست در آزادگی</p></div>
<div class="m2"><p>لاجرمش کار ببالا رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاخ شکوفه ست ثریّا وزین</p></div>
<div class="m2"><p>نعرۀ بلبل بثریّا رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاله چو من خیمه بکهسار زد</p></div>
<div class="m2"><p>تا بدلش آتش سودا رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داشت صبا بوی سر زلف یار</p></div>
<div class="m2"><p>نیم شبان دوش چو اینجا رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن همه آسایش و راحت چه بود؟</p></div>
<div class="m2"><p>کز دم آن باد بدلها رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>الحق از آنها که ز روی تم</p></div>
<div class="m2"><p>پار بروی گل رعنا رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم ازین پس نزند رای باغ</p></div>
<div class="m2"><p>شکر که امسال بما وا رسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بوی گل و نعرۀ بلبل ز باغ</p></div>
<div class="m2"><p>چون سخن من بهمه جا رسید</p></div></div>