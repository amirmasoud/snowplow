---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>تا بکف جام می توانم دید</p></div>
<div class="m2"><p>زهد و سالوس کی توانم دید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکنم یاد زهد و صومعه هیچ</p></div>
<div class="m2"><p>تا رخ ترک فی توانم دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دم از باد پیچش افتاده</p></div>
<div class="m2"><p>در تهی گاه نمی توانم دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از قدح باده چون فروغ زند</p></div>
<div class="m2"><p>در عدم نقش شی توانم دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اندرون قدح بچشم صفا</p></div>
<div class="m2"><p>راز پنهان می توانم دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر گل عارض نکو رویان</p></div>
<div class="m2"><p>شبنم از رشح خوی توانم دید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سر زلفشان دل شده را</p></div>
<div class="m2"><p>نیم شب نقش پی توانم دید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه نه، کز زهد چنگ بدرک را</p></div>
<div class="m2"><p>رگ گسته ز پی توانم دید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیشۀ بد مزاج نازک را</p></div>
<div class="m2"><p>زامثلا کرده قی توانم دید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ورصراحی نه در رکوع بود</p></div>
<div class="m2"><p>ریخته خون وی توانم بود</p></div></div>