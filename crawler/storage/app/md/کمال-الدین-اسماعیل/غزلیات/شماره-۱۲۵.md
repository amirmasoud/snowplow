---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>یاد باد آنکه حریفان همه با هم بودیم</p></div>
<div class="m2"><p>دوستانی که همه یک دل و محرم بودیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوحریفانی پاکیزه تر از قطرۀ آب</p></div>
<div class="m2"><p>بر نشسته بگل و لاله چو شبنم بودیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر یکی عالمی از فضل و هنرمندی و باز</p></div>
<div class="m2"><p>فارغ از نیک و بد گردش عالم بودیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کجا بستگیی بود کلیدش بودیم</p></div>
<div class="m2"><p>هر کجا خستگیی آمد مرهم بودیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در لطافت همه چون باد صباست عنان</p></div>
<div class="m2"><p>در وفا کوه صفت ثابت و محکم بودیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز کوشش همه هم پشت جوانان بودیم</p></div>
<div class="m2"><p>شب خلوت همه یک رویه و همدم بودیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حلقة زلف بتان رشک همی برد زما</p></div>
<div class="m2"><p>که ز دلداری در بند دل هم بودیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کجا پر هنری یا سخن آرایی بود</p></div>
<div class="m2"><p>بدل ایشان نزدیک تر از غم بودیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنچنان فارغ و آزاد بدیم از غم دل</p></div>
<div class="m2"><p>که تو گفتی که نه از عالم آدم بودیم</p></div></div>