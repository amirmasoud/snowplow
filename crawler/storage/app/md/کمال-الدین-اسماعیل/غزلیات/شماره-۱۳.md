---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>تا دم باد صبا بگشا دست</p></div>
<div class="m2"><p>گرهی از دل ما بگشا دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر فتوحی که جهانراست ز گل</p></div>
<div class="m2"><p>همه از باد هوا بگشادست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش بدان چمن را همه کار</p></div>
<div class="m2"><p>ار نفسهای صبا بگشادست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله پنداری عطّار بهار</p></div>
<div class="m2"><p>نافۀ مشک خطا بگشادست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میزبا نیست چمن خندان روی</p></div>
<div class="m2"><p>غنچه زان بند قبا بگشادست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل سوسن ز که آزرده شدست</p></div>
<div class="m2"><p>که زبانرا بجفا بگشادست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تهی دستی، بیچاره چنار</p></div>
<div class="m2"><p>دست خواهش چو گدابگشادست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا گلش خردکی زر بخشد</p></div>
<div class="m2"><p>پنجه پیشش بدعا بگشادست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابر را گر نه برو دل سوزست</p></div>
<div class="m2"><p>آبش از دیده چرا بگشادست؟</p></div></div>