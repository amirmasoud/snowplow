---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>اگر دلدار من روزی، نقاب از رخ بر اندازد</p></div>
<div class="m2"><p>بسا عاشق مه درپایش، بدست خود سر اندازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجانم در زند آتش ، چو زلفش عنبر افشاند</p></div>
<div class="m2"><p>دلم را آب گرداند، چو لعلش شکّر اندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزاران گردن افرازان سر برکرده از هر سوی</p></div>
<div class="m2"><p>که تا او از سر صیدی، کمندی اندر اندازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان تا عاشقانرا باد در دل کم جهد باری</p></div>
<div class="m2"><p>برو ز باد هر ساعت، گره بر عنبر اندازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کند مستی دلم زان می ز خونی کو همی ریزد</p></div>
<div class="m2"><p>کنم نقل لب و دندان، ز سنگی کو در اندازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمستان راست اندازی، ندارد چشم کس هرگز</p></div>
<div class="m2"><p>مگر چشمش که چون شد مست ناوک بهتر اندازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو اندازد بمن تیری کنم در سینه پنهانش</p></div>
<div class="m2"><p>بدان تا از پی آن تیر تیری دیگر اندازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی کز سوز عشق او، چو آتش یافت دل گرمی</p></div>
<div class="m2"><p>بوجد اندر سر افشاند، برقص اندر زر اندازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کند در دیدۀ عبهر تجلّی مردم دیده</p></div>
<div class="m2"><p>گر او از گوشۀ چشمی، نظر بر عبهر اندازد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر چه نیست دریای غمش را هیچ پایانی</p></div>
<div class="m2"><p>مبادا آنکه او ما را ، ازین دریا براندازد</p></div></div>