---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>دلم نخست که دل بر وفای یار نهاد</p></div>
<div class="m2"><p>به بی قراری با خویشتن قرار نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جان امید ببّرید و دل ز سر برداشت</p></div>
<div class="m2"><p>پس انگهی قدم اندر ره استوار نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگرد خویش چو پرگار می دود بر سر</p></div>
<div class="m2"><p>کنون که پای طلب در میان کا نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آن ستم که ز دلدار دید در ره عشق</p></div>
<div class="m2"><p>گناه آن همه بر بخت و روزگار نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تنگنای دلم چون بجست قطرۀ اشک</p></div>
<div class="m2"><p>ز راه دیده روان سر بکوی یار نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر آن سیه گریی کان دوزلف با من کرد</p></div>
<div class="m2"><p>برفت و یک یک بر دست آن نگار نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بیم تنگی اندر حصار سینه دلم</p></div>
<div class="m2"><p>ذخیرۀ غم و اندوه بی شمار نهاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا بخون دل خود چو تشنه دید فلک</p></div>
<div class="m2"><p>بدست گریه ام این کام در کنار نهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم از نوا در ایّام دان که نرگس تو</p></div>
<div class="m2"><p>مرا بسان گل از نوک غمزه خار نهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی که نام فراق تو بر زبان آورد</p></div>
<div class="m2"><p>چنان بود که زبان در دهان مار نهاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وصال را چه کنم زین سپس؟ که مایۀ عمر</p></div>
<div class="m2"><p>فراق او همه در راه انتظار نهاد</p></div></div>