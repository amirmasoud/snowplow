---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>مژده ایدل که یار باز آمد</p></div>
<div class="m2"><p>ترک چابک سوار باز آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزۀ او نیم مست برفت</p></div>
<div class="m2"><p>با هزاران خمار باز آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسته جانی هزار بر فتراک</p></div>
<div class="m2"><p>این زمان از شکار باز آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شماری که کردم از حسنش</p></div>
<div class="m2"><p>نه یکی، صد هزار باز آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا رب آن ساعت خجسته چه بود</p></div>
<div class="m2"><p>کز درم آن نگار باز آمد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنمرد سپاس ایزد را</p></div>
<div class="m2"><p>تا بدیدم که یار باز آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر آن آب چشم و آه سحر</p></div>
<div class="m2"><p>عاقبت هم بکار باز آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هین برون آی ای غم از دل من</p></div>
<div class="m2"><p>که مرا غمگسار باز آمد</p></div></div>