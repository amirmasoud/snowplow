---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>رخی چنان که ز خورشید و ماه نتوان کرد</p></div>
<div class="m2"><p>خطی چنان که ز مشک سیاه نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چگونه بوسه توان زد برای رخ نازک</p></div>
<div class="m2"><p>که از لطیفی در وی نگاه نتوان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پیش چهرۀ تو من ز غم دمی نزنم</p></div>
<div class="m2"><p>که پیش آیینه دانی که آه نتوان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بترک وصل تو و دل بگفتم و رفتم</p></div>
<div class="m2"><p>بهرزه عمر گرامی تباه نتوان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آب دیده در آغشته است قامت من</p></div>
<div class="m2"><p>که چوب تا نکشد نم دو تاه نتوان مرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببوسه یی که ندانم دهی تو یا ندهی</p></div>
<div class="m2"><p>همه جهان را بر خود گواه نتوان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدانک تا تو ز حالم مگر شوی آگاه</p></div>
<div class="m2"><p>ز ناله هر دم پیکی براه نتوان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه مرد عشق توام من که عشقبازی تو</p></div>
<div class="m2"><p>بجز بواسطۀ مال و جاه نتوان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حدیث وصل تو گویم، خیال تو گوید</p></div>
<div class="m2"><p>خموش باش، حدیث نگاه نتوان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو من بمرد از اندیشۀ تو وصلت را</p></div>
<div class="m2"><p>حدیث خواه توان کرد و خواه نتوان کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بجز ببدرقۀ جاه صدر فخرالدّین</p></div>
<div class="m2"><p>دلیر بر سر کوی تو راه نتوان کرد</p></div></div>