---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>منم امروز و یکی مطرب و جایی خالی</p></div>
<div class="m2"><p>شیشه یی پر ز می و صحن سرایی خالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانه یی خرد، و لیکن چون نگارستانی</p></div>
<div class="m2"><p>خوش و از زحمت هر خانه خدایی خالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرد و شطرنج بدست آید و در شیوۀ خویش</p></div>
<div class="m2"><p>راستی نیست هم از برگ و نوایی خالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیز جانا و بیا تا سه بسه بنشینیم</p></div>
<div class="m2"><p>که نباشند حریفان ز بلایی خالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوفا بر تو که تنها بخرامی زیراک</p></div>
<div class="m2"><p>نبود روی رقیبان ز جفایی خالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تو در خلوت خواهم که کنم عشرت از آنک</p></div>
<div class="m2"><p>بر ملاعیش نباشد زربایی خالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطرب، انصاف درین مجلس هم زحمت ماست</p></div>
<div class="m2"><p>لیک هم خوش نبود از دف و نایی خالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا کنم بر رخ تو همچو صراحی ز شراب</p></div>
<div class="m2"><p>مغز و اندیشه زهر رنج و عنایی خالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ادب می کنمت خدمت از آن سان که بود</p></div>
<div class="m2"><p>حرکاتم همه از چون و چرایی خالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست مال سر زلف ار نکنم گه گاهی</p></div>
<div class="m2"><p>بود از خدمت مالیدن پایی خالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیک اگر از سر مستی دهمت بوسه مرنج</p></div>
<div class="m2"><p>فعل مستان نبود خود ز خطایی خالی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور ببر در کشمت هست هم از جا بمرو</p></div>
<div class="m2"><p>بهر این کار بکار آید جایی خالی</p></div></div>