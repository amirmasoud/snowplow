---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>دمید صبح، چه خسبی چو بخت من برخیز؟</p></div>
<div class="m2"><p>بساز چنگ و برآور خروش رستاخیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببوی باده بر آمیز نکهت گل را</p></div>
<div class="m2"><p>که شب بروز بر آمیخت صبح رنگ آمیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا ز مستی دست قدح ستان بنماند</p></div>
<div class="m2"><p>بدت خویش قدح را بحلق من در ریز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجام باده فرو برسرم وگر ترسی</p></div>
<div class="m2"><p>که غرقه گردم، زلفت بست دست آویز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محیط چرخ دخانیست، چشم ازو فکن</p></div>
<div class="m2"><p>بسیط خاک غباریست، از سرش برخیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه آسمانی، با ما زمان زمان بمگرد</p></div>
<div class="m2"><p>نه روزگاری ،با ما نفس نفس مستیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدست رطل گران دادیم باوّل بار</p></div>
<div class="m2"><p>بپای مستی اگر مردی از سرم مگریز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا که گفت که چون روزگار هر ساعت</p></div>
<div class="m2"><p>بلا چون ریگ بغربال چرخ بر من بیز؟</p></div></div>