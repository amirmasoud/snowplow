---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>نفس باد صبا می جنبد</p></div>
<div class="m2"><p>غیرت مشک خطا می جنبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر و گویی سر حالت دراد</p></div>
<div class="m2"><p>می زند دست وز جا می جنبد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لالۀسوخته دل پنداری</p></div>
<div class="m2"><p>که هم از عالم ما می جنبد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاخ از رقص نمی آساید</p></div>
<div class="m2"><p>کز سر برگ و نوا می جنبد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوری اندر چمن افکند صبا</p></div>
<div class="m2"><p>خود ندانم ز کجا می جنبد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب را سلسله می جنباند</p></div>
<div class="m2"><p>باد دیوانه چو فا می جنبد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهد طفلان چمن پنداری</p></div>
<div class="m2"><p>بر انگشت هوا می جنبد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاخ دندان شکوفه که هنوز</p></div>
<div class="m2"><p>دی برآورد چرا می جنبد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهنی دارد پر آتش و آب</p></div>
<div class="m2"><p>گویی از خوف ورجا می جنبد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برگ در پیش همی تازد تیز</p></div>
<div class="m2"><p>قطره نرمک ز هوا می جنبد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاخ را هر نفسی باد صبا</p></div>
<div class="m2"><p>می زند بر سر تا می جنبد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زند خوان همچو مغان بر سر شاخ</p></div>
<div class="m2"><p>چون کند زمزمه ها می جنبد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باد نوروز چنان می جنبد</p></div>
<div class="m2"><p>که بصد حیله فرا می جنبد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عالم مرده صفت بار دگر</p></div>
<div class="m2"><p>ز اثر صنع خدا می جنبد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رگ باران حرکت می گیرد</p></div>
<div class="m2"><p>نفس باد صبا می جنبد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همچو پیریست شکوفه بر شاخ</p></div>
<div class="m2"><p>که بیاری عصا می جنبد</p></div></div>