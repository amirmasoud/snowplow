---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>آنچه عشق تو در جهان کردست</p></div>
<div class="m2"><p>بالله ار دور آسمان کردست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر تو با دلم چه کین دارد؟</p></div>
<div class="m2"><p>که دلم برد و قصد جان کردست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن نه خالت، عکس دیده ما</p></div>
<div class="m2"><p>بر رخ نازکت نشان کردست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست نام کلاه تو شب پوش</p></div>
<div class="m2"><p>زانکه زلف ترا نهان کردست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتاب از رخت سپر بفکند</p></div>
<div class="m2"><p>ورچه صد تیغ بر میان کردست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بیاموخت از تو عشوه گری</p></div>
<div class="m2"><p>سالها آسمان دران کردست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر من آن زلف پیچ پیچ آخر</p></div>
<div class="m2"><p>روی پرچین چرا چنان کردست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشوه ام داده است و بستده جان</p></div>
<div class="m2"><p>راستی را بسی زیان کردست</p></div></div>