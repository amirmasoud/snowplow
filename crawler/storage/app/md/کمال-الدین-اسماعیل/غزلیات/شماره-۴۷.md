---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>زلف تو کان همه سرها دارد</p></div>
<div class="m2"><p>گوییا هیچ سرما دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد روی تو چرا حلقه کند</p></div>
<div class="m2"><p>گر نه با ما سر سودا دارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرکشی چون نکند هندویی</p></div>
<div class="m2"><p>کان همه نعمت و کالا دارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من نگویم که چه دارد زلفت</p></div>
<div class="m2"><p>هر چه دارد همه زیبا دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرمنی مشک فراهم کردست</p></div>
<div class="m2"><p>دامنی عنبر سارا دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لب و عارض و خطّ و دهنت</p></div>
<div class="m2"><p>شکر و عنبر و دیبا دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بالش نقره و درج یاقوت</p></div>
<div class="m2"><p>رشتۀ لؤلؤ لالا دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسر تو که ندارد سلطان</p></div>
<div class="m2"><p>آنچه زلف تو بتنها دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قد من گشت دو تاتا گویند</p></div>
<div class="m2"><p>کان دو تا زلف تو همتا دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نافة مشک اگرش خود بودست</p></div>
<div class="m2"><p>جگر سوخته از ما دارد</p></div></div>