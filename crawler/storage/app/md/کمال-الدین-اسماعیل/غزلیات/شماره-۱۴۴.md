---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>خطی بر سوسن از عنبر کشیدی</p></div>
<div class="m2"><p>سر خورشید در چنبر کشیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه خطهای خوبان جهانرا</p></div>
<div class="m2"><p>بخطّ خود قلم بر سر کشیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکستی پشت سنبل را بدین خط</p></div>
<div class="m2"><p>که از ناگه برویش برکشیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنار نسترن پر سبزه کردی</p></div>
<div class="m2"><p>پر طوطی سوی شکّر کشیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر فهرست نیکوییست آن خط</p></div>
<div class="m2"><p>که بی پرگار و بی مسطر کشیدی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غبار مشک بر سوسن فشاندی</p></div>
<div class="m2"><p>طراز لاله از عنبر کشیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مه اندر خط شد از رشکت که از مشک</p></div>
<div class="m2"><p>هلالی بر کنار خور کشیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشد بر چهره هر خوبی خطی لیک</p></div>
<div class="m2"><p>تو خود از گونۀ دیگر کشیدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگرد خرمن مه آن خط سبز</p></div>
<div class="m2"><p>ز صد قوس قزح خوشتر کشیدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز زلف بس نبود آن ترک تازی</p></div>
<div class="m2"><p>که هندویی دگر را برکشیدی</p></div></div>