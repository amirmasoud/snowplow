---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>خون دل از دو دیده بدامن همی کشم</p></div>
<div class="m2"><p>باری گران نه در خور این تن همی کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخسار من چو کاه و برو دانهای اشک</p></div>
<div class="m2"><p>این کاه و دانه بین که بخرمن همی کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افتاده ام چو سایه و چالاک میدوم</p></div>
<div class="m2"><p>چون سوزنم برهنه و دامن همی کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید که چون صراحی خونم همی خورند</p></div>
<div class="m2"><p>زیرا که سر ندارم و گردن همی کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عجز همچو گل سپر از آب بفکنم</p></div>
<div class="m2"><p>وانگه ز عجب تیغ چو سوسن همی کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در می کشم بتار مژه قطره های اشک</p></div>
<div class="m2"><p>دردانه بین که در سر سوزن همی کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معذورم ارز گریه مرا صبر دل نماند</p></div>
<div class="m2"><p>از بیم سیل رخت ز مسکن همی کشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این جورها ببین که من از دوست می برم</p></div>
<div class="m2"><p>وین طعنها نگر که ز دشمن همی کشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنجی که از کشیدن آن کوه عاجزست</p></div>
<div class="m2"><p>با آنکه نیست تاب کشیدن همی کشم</p></div></div>