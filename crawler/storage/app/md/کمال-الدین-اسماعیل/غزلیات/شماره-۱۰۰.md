---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>مخور ای دل غم بسیار مخور</p></div>
<div class="m2"><p>ور خوری جز غم دلدار مخور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه غم یار عزیزست؟ آن نیز</p></div>
<div class="m2"><p>اگرت هست نگهدار مخور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار تیمار تو چون می نخورد</p></div>
<div class="m2"><p>پس تو بی فایده تیمار مخور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خه! چنین خواهمتف، احسنت،ای یار</p></div>
<div class="m2"><p>غم من اندک و بسیار مخور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من ز عشق تو زیم یا میرم</p></div>
<div class="m2"><p>تو خود البّته غم کار مخور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشت من بشکن و پیمان مشکن</p></div>
<div class="m2"><p>خون من می خور وز نهار مخور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم تو دوش لبت را می گفت</p></div>
<div class="m2"><p>با فلان باده دگر باره مخور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب تو گفت بدو خیز بخسب</p></div>
<div class="m2"><p>تو که مستی غم هشیار مخور</p></div></div>