---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>سحرگهان که دم صبح در چمن گیرد</p></div>
<div class="m2"><p>چهار سوی چمن نافۀ ختن گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسیم افتان خیزان چو مست عربده جوی</p></div>
<div class="m2"><p>بباغ در جهد و جیب نسترن گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خروش مرغان بر سرو بن چو دلشده یی</p></div>
<div class="m2"><p>که یار خود را بر پای در سخن گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشکل لاله نگر خال عنبرین بر لب</p></div>
<div class="m2"><p>چو یار خود را بر پای در سخن گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل شکفته چو معشوق شوخ کز عاشق</p></div>
<div class="m2"><p>زر قراضه در اطراف پیرهن گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیال سبزه و شبنم برو بدان ماند</p></div>
<div class="m2"><p>کسی که قبضۀ شمشیر در سفن گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درست گویی زنجیر زلف یار منست</p></div>
<div class="m2"><p>چو روی آب ز باد هوا شکن گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدیث مشک خطا پیش او خطا باشد</p></div>
<div class="m2"><p>چو باد فایده ز انفاس یاسمن گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو غنچه هر که در این وقت تنگ دل باشد</p></div>
<div class="m2"><p>دلش گشاده شود چون ره چمن گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین چنین ره وقتی بحدّ پایۀ خویش</p></div>
<div class="m2"><p>همه کسی پی دلدار خویشتن گیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببوی یار بنزدیک گل شوم، او نیز</p></div>
<div class="m2"><p>ز بی وفایی رنگ نگار من گیرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلم ز غصّۀ او قطره قطره خون گردد</p></div>
<div class="m2"><p>ز راه دیده یکایک برون شدن گیرد</p></div></div>