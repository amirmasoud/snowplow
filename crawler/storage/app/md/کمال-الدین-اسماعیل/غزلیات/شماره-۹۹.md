---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>زهی در حسرت آن چشم مخمور</p></div>
<div class="m2"><p>فتاده نرگس سرمست رنجور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن در لعل تو عقلست در جان</p></div>
<div class="m2"><p>قدح در دست تو نور علی نور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روانروا در خوشی لعل تو مایه</p></div>
<div class="m2"><p>فلک را در جفا خوی تو دستور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهار آمد، چه داری؟ خیز کاکنون</p></div>
<div class="m2"><p>نباشد مردم هشیار معذور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو غنچه هرگز او بوی دل آید</p></div>
<div class="m2"><p>نماند وقت گل او نیز مستور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک می گردد ای غافل چه باشی</p></div>
<div class="m2"><p>بدین ده روزه ملک حسن مغرور؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر شادی بخون خواری بهر حال</p></div>
<div class="m2"><p>ز خون عاشقان به خون انگور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیاد بزم خسرو جام پر کن</p></div>
<div class="m2"><p>که باد از دولت او چشم بد دور</p></div></div>