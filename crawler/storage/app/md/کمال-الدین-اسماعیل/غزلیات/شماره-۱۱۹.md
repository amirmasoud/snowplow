---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>نه دسترسی به یار دارم</p></div>
<div class="m2"><p>نه طاقت انتظار دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جور که از تو بر من آید</p></div>
<div class="m2"><p>از گردش روزگار دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در راه غمت کنم هزینه</p></div>
<div class="m2"><p>گر یک دل و گر هزار دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این خسته تنی چو موی باریک</p></div>
<div class="m2"><p>از زلف تو یادگار دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من کانده تو کشیده باشم</p></div>
<div class="m2"><p>اندوه زمانه خوار دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آب دو دیده غرقه گشتم</p></div>
<div class="m2"><p>و امید لب و کنار دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل بردی و رفتی و همین بود</p></div>
<div class="m2"><p>من با تو بسی شمار دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دشنام همی‌دهی مرا باش</p></div>
<div class="m2"><p>من با دو لب تو کار دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر دریابم شبی ز بوسش</p></div>
<div class="m2"><p>حقّا که دومه فگار دارم</p></div></div>