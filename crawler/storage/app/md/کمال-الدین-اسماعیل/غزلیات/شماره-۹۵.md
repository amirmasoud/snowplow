---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>غمت جز با دلم خوش در نیاید</p></div>
<div class="m2"><p>سرح جز با تو سرکش در نیاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشاده کی بود آن مهرۀ دل؟</p></div>
<div class="m2"><p>که با نقش تو در ششدر نیاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خط کژ طبع تو هم راست طبعست</p></div>
<div class="m2"><p>که جز با لعل تو خوش در نیاید</p></div></div>