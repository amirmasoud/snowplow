---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>ما حالی از نشاط کناری گرفته ایم</p></div>
<div class="m2"><p>در سر زجام غصّه خماری گرفته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرورده ایم دشمن جانرا بخون دل</p></div>
<div class="m2"><p>پس لاف می زنیم که یاری گرفته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندین هزار گلبن شادی درین جهان</p></div>
<div class="m2"><p>ما با غم تو دامن خاری گرفته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدم نبهره بود بمعیار مردمی</p></div>
<div class="m2"><p>از دوستی هر که عیاری گرفته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگه که دست در سر زلف بتی زدیم</p></div>
<div class="m2"><p>چون نیک بنگری دم ماری گرفته ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز درد دل ز دیده ندیدیم ازین سبب</p></div>
<div class="m2"><p>بر خون دل زدیده کناری گرفته ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کردم شمار و در غلطم از همه شمار</p></div>
<div class="m2"><p>عمر خود زهر که شماری گرفته ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آیین خوش دلی ز زمانه بر اوفتاد</p></div>
<div class="m2"><p>ما بیهده چرا پی کاری گرفته ایم؟</p></div></div>