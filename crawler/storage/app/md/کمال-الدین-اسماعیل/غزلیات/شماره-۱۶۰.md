---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>بازم لباس صبر به صد پاره کرده‌ای</p></div>
<div class="m2"><p>بازم ز کوی عافیت آواره کرده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم خجل شوی اگرت آورم بروی</p></div>
<div class="m2"><p>آن جورها که بر من بیچاره کرده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچ آسمان به خنجر مرّیخ می‌کند</p></div>
<div class="m2"><p>تو در زمین به غمزهٔ خونخواره کرده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود با دل تو لابۀ ما سودمند نیست</p></div>
<div class="m2"><p>گویی به رغم ما دلی از خاره کرده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند رستخیز به هم برزند جهان</p></div>
<div class="m2"><p>این بازیی ات خود که تو صدباره کرده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو داد و داوری؟ که کنم بر تو من درست</p></div>
<div class="m2"><p>تا بی‌سبب چرا دل من پاره کرده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی که رایگان غم من می‌خوری نه بس</p></div>
<div class="m2"><p>الحق تو این شگرفی همواره کرده‌ای</p></div></div>