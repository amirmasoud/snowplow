---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ندانم غنچه را بلبل چه گفتست</p></div>
<div class="m2"><p>که بس خونین دل و چهره کشفتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر رازی که او را با صبا بود</p></div>
<div class="m2"><p>یکایک فاش در رویش بگفتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو گویی آتش افتادست در خار</p></div>
<div class="m2"><p>ز بس گلها که از گلبن شکفتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجز در حلقۀ لاله نیایی</p></div>
<div class="m2"><p>گهر هایی که چشم ابر سفتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه فتنۀ باغست نرگس</p></div>
<div class="m2"><p>بدان شادم که آخر فتنه خفتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبا کرد آشکارا بر سر چوب</p></div>
<div class="m2"><p>هر آن خرده که گل در دل نهفتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو سر در یک قدم بنمود نرگس</p></div>
<div class="m2"><p>بنا میزد، چه زیبا طاق و جفتست</p></div></div>