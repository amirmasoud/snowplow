---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>رویی، چگونه رویی؟ رویی چو آفتاب</p></div>
<div class="m2"><p>زلفی، چگونه زلفی؟ هر حلقه یی و تابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر پرتوی ز رویت، در چشم عقل نوری</p></div>
<div class="m2"><p>هر حلقه یی ز زلفت، در حلق جان طنابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر عکس عارض تو، بر صحن عالم افتد</p></div>
<div class="m2"><p>گردد ز سایۀ او، هر ذرّه آفتابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب حیات کبود؟ خلد برین چه باشد</p></div>
<div class="m2"><p>بر روی تو نگاهی ، بر یاد تو شرابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دور چشم مستت، هست از شراب فتنه</p></div>
<div class="m2"><p>افتاده همچو نرگس ، هر گوشه یی خرابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن چشم نرگسین را، از خواب خوش برانگیز</p></div>
<div class="m2"><p>تا هر زمان نبیند، در راه فتنه خوابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر جان عاشقانت، بخشایش ار نیاید</p></div>
<div class="m2"><p>گه گاه چشم بد را، بر میفکن نقابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خشک سال هجران، هو دولت رخ تست</p></div>
<div class="m2"><p>گر هیچگونه ماندست، در چشم بنده آبی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کس که پرسد از من، احوال سوزیانم</p></div>
<div class="m2"><p>باشد سرشک خونین، حاضرترین جوابی</p></div></div>