---
title: >-
    شمارهٔ ۱ - وله ایضا فی صفتها
---
# شمارهٔ ۱ - وله ایضا فی صفتها

<div class="b" id="bn1"><div class="m1"><p>ای ز احکام همچو رویین دز</p></div>
<div class="m2"><p>دست و هم از گشادنت عاجز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طرفه معشوق و گونة عاشق</p></div>
<div class="m2"><p>از درون صامت از برون ناطق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه چون نرگسی سرافگنده</p></div>
<div class="m2"><p>گه دهان چون گل از زرا گنده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان نهادی چو غنچه لب بر هم</p></div>
<div class="m2"><p>که دلت بستۀ زرست و درم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ده زبان همچو وسنی لیکن</p></div>
<div class="m2"><p>بر تو از رازها بوند ایمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورتت در جهات شش گانه</p></div>
<div class="m2"><p>آشکارا یکی نهان خانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نتهی راز پیش بلهوسان</p></div>
<div class="m2"><p>ورچه هستت زبان به دست کسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو چنگی شکم تهی که ترا</p></div>
<div class="m2"><p>به سر انگشت شد زباننرم گویا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نرم گوییّ و سخت پیشانی</p></div>
<div class="m2"><p>ندهی تا نخست نستانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نرسانی امانت کس باز</p></div>
<div class="m2"><p>تا سرت بر نگیرت از آغاز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ترا مالش زبان ندهند</p></div>
<div class="m2"><p>راز را با تو در میان ننهند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با هر ان کو فتاد پیوندت</p></div>
<div class="m2"><p>کند از بپر خود زبان بندت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتمت بستة زر و درمی</p></div>
<div class="m2"><p>تا بدیدمت بندة شکمی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس که هر چیز درکشی بدمت</p></div>
<div class="m2"><p>سر نهادی تو در سرشکمت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از تو در خط همی شود خابن</p></div>
<div class="m2"><p>بر سرت خط همی نهد خازن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ساده بودی نخست و آخر کار</p></div>
<div class="m2"><p>گشت بر گرد لب خطت دیدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون صدف بسته از درون زیور</p></div>
<div class="m2"><p>سر تو بر لب و زبان سر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چارپایی و لیک ره نکنی</p></div>
<div class="m2"><p>چار میخت کشند واه نکنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باز کرده شکم چو آبستن</p></div>
<div class="m2"><p>بر سر پای از پی زادن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زخمها خورده بیخصومت و حرب</p></div>
<div class="m2"><p>چار دیوارتست دارالّضرب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر چه از رنج فقر بی بیمی</p></div>
<div class="m2"><p>اینچنین کوفته هم از سیمی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>طالع آنکس است نیکو حال</p></div>
<div class="m2"><p>کش بود صورت تو بیت المال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بند بر زال زر نهادستی</p></div>
<div class="m2"><p>زانک رویین تن او فتادستی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر چه با خویش و آشنا گویی</p></div>
<div class="m2"><p>همه مرموز و لوترا گویی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در زبان تو کم کسی داند</p></div>
<div class="m2"><p>ورچه اندیشه ات یکی ده باد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از تو دست دراز کوته باد</p></div>
<div class="m2"><p>سر اندیشه ات یکی ده باد</p></div></div>