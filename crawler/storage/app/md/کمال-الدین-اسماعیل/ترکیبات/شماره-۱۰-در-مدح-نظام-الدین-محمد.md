---
title: >-
    شمارهٔ ۱۰ - در مدح نظام الدّین محمّد
---
# شمارهٔ ۱۰ - در مدح نظام الدّین محمّد

<div class="b" id="bn1"><div class="m1"><p>جانا به سحر چشم جهانی ببسته‌ای</p></div>
<div class="m2"><p>زین حلقه‌های زلف که بر هم شکسته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر چه فتنه‌ای؟ که ز عشق تو در جهان</p></div>
<div class="m2"><p>برخاست رستخیز و تو فارغ نشسته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حقّا که در مشهّرۀ لعل فستقی</p></div>
<div class="m2"><p>شیرین‌تر و لطیف‌تر از مغز پسته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشکسته‌ای به سنگ جفاها دل مرا</p></div>
<div class="m2"><p>پس رفته‌ای به طنز و سر زلف بسته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حقّۀ عقیق تو یابند مرهمش</p></div>
<div class="m2"><p>آنرا که دل به ناوک مژگان بخسته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای صبر ناپدید، تو بس تنگ عرصه‌ای</p></div>
<div class="m2"><p>وی اشک بی‌قرار، تو بس سرگشته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وی یار سنگ دل که مرا طعنه می‌زنی</p></div>
<div class="m2"><p>باری ترا که نیست غم عشق، رسته‌ای</p></div></div>
<div class="b2" id="bn8"><p>زین سان که در همست و پر از بند چون زره</p>
<p>بر کار خویش و زلف تو چون افکنم گره؟</p></div>
<div class="b" id="bn9"><div class="m1"><p>هر شام کآفتاب ز گردون فرو شود</p></div>
<div class="m2"><p>جانم ز غم بفکر دگرگون فرو شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آه از برم چو عیسی سر بر فلک نهد</p></div>
<div class="m2"><p>اشک از رخم بخاک چو قارون فرو شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خونش بدل فرو شود از غصّه های من</p></div>
<div class="m2"><p>اندیشه چون بدین دل پر خون فرو شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر برنیاورید مگر از چشمسار چشم</p></div>
<div class="m2"><p>هر دل که او بدان رخ گلگون فرو شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر صبحدم که جیب لب از آه بردرم</p></div>
<div class="m2"><p>خون شفق بدامن گردون فرو شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد نا پدید خون دلم در میان اشک</p></div>
<div class="m2"><p>چون چند قطره‌ای که به جیحون فرو شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی تو هلال وار تن زرد لاغرم</p></div>
<div class="m2"><p>هر کش بدید گفت هم اکنون فروشود</p></div></div>
<div class="b2" id="bn16"><p>چون حلقه های زلف تو سردرسر آورد</p>
<p>اندیشه ها ز خاطر من سر بر آورد</p></div>
<div class="b" id="bn17"><div class="m1"><p>ای زلف هندوی تو چو ترکان دلستان</p></div>
<div class="m2"><p>جان از برای غارت دل بسته بر میان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک شب نداشت پاس دلم زلف هندوت</p></div>
<div class="m2"><p>با آنکه هندوان همه باشند پاسبان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بردیده می نشانم چون لعبتان چشم</p></div>
<div class="m2"><p>هر هندوی که دارد از نام تو نشان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رسمیست هندوان که در آتش کنند جای</p></div>
<div class="m2"><p>زان جای زلف تست مرا در دل و روان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زلف تو دل همی ببرد از میان چشم</p></div>
<div class="m2"><p>نبود شگفت دزدی چابک ز هندوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با ترک تاز طرّۀ هندوی تو مرا</p></div>
<div class="m2"><p>همواره همچو بنگه لوریست خان و مان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اقبال هندوی تو و دولت غلام تست</p></div>
<div class="m2"><p>تا هست سوی تو نظر خواجۀ جهان</p></div></div>
<div class="b2" id="bn24"><p>صدر زمانه صاحب عادل نظام دین</p>
<p>کش بوسه داد حلقۀ افلاک بر نگین</p></div>
<div class="b" id="bn25"><div class="m1"><p>ای سروری که مثل تو در روزگار نیست</p></div>
<div class="m2"><p>بارایت آفتاب جهانرا بکار نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیشی از آفتاب بقدر و شکوه و جاه</p></div>
<div class="m2"><p>جودو کرم مگیر که آن در شمار نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا هست ابر جود تو بارنده بر جهان</p></div>
<div class="m2"><p>از نیستی بدامن کس بر غبار نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر در شکم که مثل تو بودست یا نبود</p></div>
<div class="m2"><p>دانم همی یقین که درین روزگار نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در عهد تو میان بوفا استوار کرد</p></div>
<div class="m2"><p>گر چه فلک بعهد چنان استوار نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از سایۀ تو هر که جدا شد چو آفتاب</p></div>
<div class="m2"><p>یک ذرّه بر زمینش جای قرار نیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>روزی دوگر حسود ترا کارکی برفت</p></div>
<div class="m2"><p>آن از نوادرست بدان اعتبار نیست</p></div></div>
<div class="b2" id="bn32"><p>از بس که مسرفست بدادن سخای تو</p>
<p>خواهنده را ملال گرفت از عطای تو</p></div>
<div class="b" id="bn33"><div class="m1"><p>لطف تو در شمایل جان آن اثر کند</p></div>
<div class="m2"><p>کاندر مزاج غنچه نسیم سحر کند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیرون از آن که کام دل آرزو دهد</p></div>
<div class="m2"><p>جود تو در زمانه چه کار دگر کند؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر سر کند حسود تو خاک از جفای بخت</p></div>
<div class="m2"><p>هر روز کآفتاب سر از خاک برکند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از نوک خامۀ تو چکیدست بر زمین</p></div>
<div class="m2"><p>آن مایه‌ای که خاک از آن نیشکر کند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اقبال را نشیمن اصلی جناب تست</p></div>
<div class="m2"><p>جود تو بایدش که بهر جا گذر کند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آنرا بآب روی نگیرند در شمار</p></div>
<div class="m2"><p>کز آب چشم خصم تو رخساره تر کند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر کس که او زبان بثنای تو برگشاد</p></div>
<div class="m2"><p>شاید که همچو شمع زبان تاج سر کند</p></div></div>
<div class="b2" id="bn40"><p>گر چه کنند بخشش پیوست بحر و کان</p>
<p>هرگز کجا رسند در آن دست بحر و کان</p></div>
<div class="b" id="bn41"><div class="m1"><p>ای صاحب زمانه و دستور روزگار</p></div>
<div class="m2"><p>بادا همیشه خصم تو مقهور روزگار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پروانۀ ضمیر تو حاصل کند نخست</p></div>
<div class="m2"><p>پس شمع آفتاب دهد نور روزگار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جان از برای خدمت تو بست بر میان</p></div>
<div class="m2"><p>وین قدر خود چه باشد مقدور روزگار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گردون نوشته بود در القاب خاص تو</p></div>
<div class="m2"><p>مشکور از آفرینش و مشهور روزگار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بر تارک عروس بقایت کند نثار</p></div>
<div class="m2"><p>عطّار چرخ عنبر و کافور روزگار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پیوسته تاب مهر تو در جان آفتاب</p></div>
<div class="m2"><p>بنوشته دست عمر تو منشور روزگار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کوته شود ز دامن اعمار دست مرگ</p></div>
<div class="m2"><p>چرخ ارکند ز لطف تو دستور روزگار</p></div></div>
<div class="b2" id="bn48"><p>این رسم جود کز دل و دست تو دیده ایم</p>
<p>حقّا اگر ز حاتم طایی شنیده ایم</p></div>
<div class="b" id="bn49"><div class="m1"><p>ای سایه ات خجسته تر از سایۀ همای</p></div>
<div class="m2"><p>بر مطرحت ملوک بحرمت نهاده پای</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تشریف بود و تربیتی بس بجای خویش</p></div>
<div class="m2"><p>گر رنجه گشت شاه سوی این بلند جای</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>معلوم شد که سوی نکوییست رای شاه</p></div>
<div class="m2"><p>چون کرد رای آنک خرامد بدین سرای</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شاه ستارگان را جوزاست برج اوج</p></div>
<div class="m2"><p>زیرا که هست خانۀ دستور نیک رای</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>لایق بحسب حال تو بیتی شنیده ام</p></div>
<div class="m2"><p>از گفتۀ عمادی بس نغز و دلگشای</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تشریف طغرلیست وگرنه بگفتمی</p></div>
<div class="m2"><p>مصحف ز بند زر نشود مرتبت فزای</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>برخوان نعمتت چو ملو کند میهمان</p></div>
<div class="m2"><p>گنجم هر اینه بطفیلی من گدای</p></div></div>
<div class="b2" id="bn56"><p>کس در جهان نگفت و نگوید چنین سخن</p>
<p>ور گفته اند پس تو مرا تربیت مکن</p></div>
<div class="b" id="bn57"><div class="m1"><p>دولت قرین حضرت صدر زمانه باد</p></div>
<div class="m2"><p>اقبال را مقام بر این آستانه باد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هر تیر دیده دوز که از شست چرخ جست</p></div>
<div class="m2"><p>انرا ز طاق ابروی خصمت نشانه باد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مرغی که کرد بیضۀ زرّین آفتاب</p></div>
<div class="m2"><p>بر گوشۀ سرای تواش آشیانه باد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از بارگاه غیب بدرگاه حشمتت</p></div>
<div class="m2"><p>امداد کامرانی و نصرت روانه باد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ارکان ملک داده بحکم تو چشم و گوش</p></div>
<div class="m2"><p>وز تو اشارتی بسر تازیانه باد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تا گرد قطب باشد دوران فرقدان</p></div>
<div class="m2"><p>دوران آن دو گانه بر این یگانه باد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>وان کو نخواست قدر ترا برتر از فلک</p></div>
<div class="m2"><p>کارش چو کار خادم زیر از میانه باد</p></div></div>
<div class="b2" id="bn64"><p>داد مرادهای تو گیتی بداده باد</p>
<p>دست و دل و در تو بشادی گشاده باد</p></div>