---
title: >-
    شمارهٔ ۱ - فی نعت سیدالمرسلین و خاتم‌النبیین محمدالمصطفی (ص)
---
# شمارهٔ ۱ - فی نعت سیدالمرسلین و خاتم‌النبیین محمدالمصطفی (ص)

<div class="b" id="bn1"><div class="m1"><p>ای جز باحترام خدایت نبرده نام</p></div>
<div class="m2"><p>وی سلک انبیاز وجود تو بانظام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردست عقل،نور مساعی توچراغ</p></div>
<div class="m2"><p>برکام نفس،حکم مناهی تو لگام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازآتش سنان تویک شعله نورصبح</p></div>
<div class="m2"><p>وزپرچم سیاه تو یک تارزلف شام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتراک توست عروۀ وثقی که جبرئیل</p></div>
<div class="m2"><p>در وی زند ز بهر شرف دست اعتصام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرصورت تو رحمت عالم نیامدی</p></div>
<div class="m2"><p>ازحضرت خدای که دادی بما پیام؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چل روزاز آن سبب گل آدم سرشته شد</p></div>
<div class="m2"><p>تا قصردین بخشت وجودت شود تمام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای نقش کرده برصفحات وجودخویش</p></div>
<div class="m2"><p>عرش مجید نام ترا ازبرای نام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرجوش دیگ سینه چه داری که می پزند</p></div>
<div class="m2"><p>در مطبخ«ابیت»ترا گونه گون طعام؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درموکب جلال توازعجز بازماند</p></div>
<div class="m2"><p>روح القدس بمنزل الاله مقام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نزدیک توچه تحفه فرستیم ما ز دور؟</p></div>
<div class="m2"><p>دردست ما همین صلوات است والسلام</p></div></div>
<div class="b2" id="bn11"><p>عیسی ز مقدم تو با یام مژده داد</p>
<p>از یُمن آن سخن نفسش جان بمرده داد</p></div>
<div class="b" id="bn12"><div class="m1"><p>ای کرده خاک پای تو با عرش همسری</p></div>
<div class="m2"><p>ختمست بر کمال تو ختم پیمبری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در معرض ظهور نکرد از علو قدر</p></div>
<div class="m2"><p>با آفتاب سایۀ شخصت برابری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باد صبا ببست میان نصرت تو را</p></div>
<div class="m2"><p>دیدی چراغ را که دهد باد یاوری؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دریای وحی را شده غواص جبرئیل</p></div>
<div class="m2"><p>جوهر کلام حق و زبان تو جوهری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو کرده از تواضع درویشی اختیار</p></div>
<div class="m2"><p>وز همت تو یافته دریا توانگری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر عزم قاب قوسین اندر دمی لطیف</p></div>
<div class="m2"><p>چون تیر برگذشته ز افلاکِ چنبری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر راه تو نهاده فلک صد هزار چشم</p></div>
<div class="m2"><p>تا جز فرار از دیدهٔ او گام نسپری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر هفت کرده چرخ و به راه تو آمده</p></div>
<div class="m2"><p>بر آرزوی آن که در او بوکه بنگری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو برگذشته فارغ و آزاد از همه</p></div>
<div class="m2"><p>جایی که جبرئیل ندانست رهبری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بی واسطه رسیده به صندوق سر تو</p></div>
<div class="m2"><p>چندان جواهر کرم و بنده پروری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در حضرت الهی چون ما به حضرتت</p></div>
<div class="m2"><p>در بند عجز کرده زبان ثناگری</p></div></div>
<div class="b2" id="bn23"><p>برهان مُعجِز تو کلام الهیست</p>
<p>نه چون کلیم و ذوالنون از مار و ماهیست</p></div>
<div class="b" id="bn24"><div class="m1"><p>ای از فرازسدره برافراشته علم</p></div>
<div class="m2"><p>وی صورت شفای تو درسورة الم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پروازِ مرغ همت تودرفضای قرب</p></div>
<div class="m2"><p>خلوت سرای فکرت توعالم قدم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پیکان تیرازکف تو منبع زلال</p></div>
<div class="m2"><p>سنگ وکلوخ در نظرتوست جام جم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>توتیغ را بروی قلم برکشیده یی</p></div>
<div class="m2"><p>زان حکم تیغ هست روان برسرقلم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چشم وچراغ هردوجهانی وهرشبی</p></div>
<div class="m2"><p>تاروزایستاده چوشمعی بیک قدم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گسترده درسرای نبوت بساط تو</p></div>
<div class="m2"><p>آدم هنوزرخت نیاورده ازعدم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>درمعرضی که آتش قهرت زبانه زد</p></div>
<div class="m2"><p>اندردهان دریا الحق نماندنم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وآنجا که برگشاد زبان آب لطف تو</p></div>
<div class="m2"><p>آتش بکام نیستی اندرکشید دم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روحانیان در آرزوی خاک پای تو</p></div>
<div class="m2"><p>باخاکیان نشسته توازغایت کرم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نور تو پیش ازآدم وسایه پس ازرسل</p></div>
<div class="m2"><p>زانست نوروسایه زپیش و پست بهم</p></div></div>
<div class="b2" id="bn34"><p>از بیم آب روی تو در صف رستخیز</p>
<p>آتش نموده پشت و گرفته ره گریز</p></div>
<div class="b" id="bn35"><div class="m1"><p>ای با علو همت تو آسمان زمین</p></div>
<div class="m2"><p>وی گام اولین تو برچرخ هفتمین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>روح الله ار زآستی مریم آمدست</p></div>
<div class="m2"><p>صد مریمست روح ترا اندر آستین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>محبوب حق شد آنکه ترا کرد پیروی</p></div>
<div class="m2"><p>وه کزکجاست تابکجامنصبی چنین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تقدیر بر کشید بمیزان همتت</p></div>
<div class="m2"><p>وز پرپشه بود سبک مایه ترزمین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای تیردیده دوز تواز کیش«مارمیت»</p></div>
<div class="m2"><p>وی سَنجَق سپاه توخیل مسومین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ازشرح لفظ تودهن نُقل پرشکر</p></div>
<div class="m2"><p>وزیاد خُلق تونفس عقل عنبرین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عزم درست توزپی نصرت صواب</p></div>
<div class="m2"><p>برهم شکسته لشکرکفرخطاچوچین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پیروزۀ فلک بنسودی کف وجود</p></div>
<div class="m2"><p>نام محمد ار نبدی نقش آن نگین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آدم که دانه یی ز بهشتش بدر فکند</p></div>
<div class="m2"><p>ازخرمن شفاعت توهست خوشه چین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ظلمت زدای عالم جانی ازآنکه هست</p></div>
<div class="m2"><p>لفظ تو آفتاب ونفس صبح راستین</p></div></div>
<div class="b2" id="bn45"><p>تلقین ذکر کرده گفت سنگ ریزه را</p>
<p>انبار رزق کرده دلت ظل نیزه را</p></div>
<div class="b" id="bn46"><div class="m1"><p>ای گاه تربیت صفت ذات تورحیم</p></div>
<div class="m2"><p>وی گاه صفدری یزک لشکرتوبیم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>طاوس سدره درحرمت مرغ خانگی</p></div>
<div class="m2"><p>بطنان عرش کعبۀ جاه تراحریم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>صیت صداش مشرق ومغرب فروگرفت</p></div>
<div class="m2"><p>دست نبوت توچوزدطبل در گلیم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>انگشت معجز توکه تیغیست آبدار</p></div>
<div class="m2"><p>یک زخم اوکندسپرماه رادونیم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مخلوق درثنای توخود تاکجارسد؟</p></div>
<div class="m2"><p>خوانده خدای باعظمت خلق توعظیم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ازراه تربیت پدر خلق عالمی</p></div>
<div class="m2"><p>وز نازدرزبان قضانام تو یتیم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تقویم تو خدای چنان کرددر ازل</p></div>
<div class="m2"><p>کآمدچوراه حق همه چیزتومستقیم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تشریف داد ذات ترااز صفات خویش</p></div>
<div class="m2"><p>گاهی کریم وگاه رئوف وگهی رحیم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>رمزیست ازدوحرف میانینِ نام تو</p></div>
<div class="m2"><p>درهفت جاکه هست اشارت به حاومیم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بالشکر تو پای که دارد چو با شدت</p></div>
<div class="m2"><p>زراد خانه خاک و مبارز دم نسیم؟</p></div></div>
<div class="b2" id="bn56"><p>ای مرگ دشمنان تو بیماری صبا؟</p>
<p>وی کوری مخالف تو سرمۀ هبا</p></div>
<div class="b" id="bn57"><div class="m1"><p>عکسی زنور روی توخورشیدانورست</p></div>
<div class="m2"><p>رشحی زقُلزُم کرمت حوض کوثرست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اندرریاض وحی زبان تو بلبلست</p></div>
<div class="m2"><p>واندر بحار قرآن خلق تو عنبرست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نه عقل برخصایص ذات تو واقفست</p></div>
<div class="m2"><p>نه طبع دردقایق شرع تو رهبرست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بانور رهنمای توعَضبا قَلاوُزست</p></div>
<div class="m2"><p>درشرح معجزات توحَصبا سخنورست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سرگشته باشد ازبن دندان کلیدوار</p></div>
<div class="m2"><p>هرکزسرای شرع توچون قفل بردست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چون غنچه هرکه یافت زخُلق توشمه یی</p></div>
<div class="m2"><p>خندان لب ورقیق دل وخوب محضرست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هرکوزسوز دل نفسی خوش همی زند</p></div>
<div class="m2"><p>درزیردامن کرمت همچو مجمرست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>آنرا که برکشیدقبول تو،همچوتیغ</p></div>
<div class="m2"><p>گرچه برهنه است زگوهر توانگرست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>وآنراکه همچوتیر بینداخت رد تو</p></div>
<div class="m2"><p>خونین دهان وپی زده و خاک برسرست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>درقبضۀ توخنجر چون آب را چه کار؟</p></div>
<div class="m2"><p>درحلق دشمنان توخودآب خنجرست</p></div></div>
<div class="b2" id="bn67"><p>دنیا واهل دنیانزدتوهردوخوار</p>
<p>یک مشت خاک برسریک مشت خاکسار</p></div>
<div class="b" id="bn68"><div class="m1"><p>آنجاکه قدرتست فلک رامدار نیست</p></div>
<div class="m2"><p>وانجا که قهرتست زمین راقرارنیست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>هرچ آمدت بدست بدادی وبیش ازآن</p></div>
<div class="m2"><p>وین جودآنکس است کش ازفقرعارنیست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سرکان نه خاک پای تو،درد سرآورد</p></div>
<div class="m2"><p>دولت که آن نه از تو بود پایدار نیست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آنجاکه کردشرعِ توانفاذ تیغ حکم</p></div>
<div class="m2"><p>عقل برهنه راسپر ختیار نیست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گرچه شمارخلق جهان ازعطای تست</p></div>
<div class="m2"><p>درعالم عطای تورسم شمارنیست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نه انبیای مرسل ونه جبرئیل را</p></div>
<div class="m2"><p>درپرده های خلوت خاص تو بارنیست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تاتهمت جنون نهند کفرهرزه گوی</p></div>
<div class="m2"><p>انگشتِ خط نگارتو برنی سوار نیست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ای انبیا بسایۀ توکرده التجا</p></div>
<div class="m2"><p>آن کیست کش بسایۀ جاه توکارنیست؟</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تومفتخربفقروهمه نسل آدمت</p></div>
<div class="m2"><p>درسایۀ لواوبدانت افتخارنیست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دریای مدحت توزپهناوری که هست</p></div>
<div class="m2"><p>در وی شناوران سخن راگذار نیست</p></div></div>
<div class="b2" id="bn78"><p>خورده قفا زدست تو زرهای ماه روی</p>
<p>گشته ندیم خاص توفقرسیاه روی</p></div>
<div class="b" id="bn79"><div class="m1"><p>ای گفته لطف حق بخودی خودت ثنا</p></div>
<div class="m2"><p>ما از کجاو مدح وثنای تو از کجا؟</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ماخود که ایم تابثنای تو دم زنیم</p></div>
<div class="m2"><p>درمعرض لعمرک ولولاک والضحی؟</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>لطف خدای جمله کمالات خلق را</p></div>
<div class="m2"><p>یک چیز کرد و داد بدون ام مصطفی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>آدم ز کار گل بِنشُسته هنوز دست</p></div>
<div class="m2"><p>درخانۀ نبوت بودی تو کدخدا</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>آزادِ مطلقی وشعارتو بندگی</p></div>
<div class="m2"><p>سلطان هردوکون وسراپرده ات عبا</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ناداده ازحقارت اسباب کاینات</p></div>
<div class="m2"><p>اندرخورمروت خودهمتت عطا</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>هرچندانبیاهمه پیش ازتوآمدند</p></div>
<div class="m2"><p>چون پس روان همه بتوکردند اقتدا</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تشریف سایۀ تو زمین گر بیافتی</p></div>
<div class="m2"><p>درچشم آفتاب شدی خاک توتیا</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>محروم کرد روح قُدُس رازمحرمی</p></div>
<div class="m2"><p>چاوش«لودنوت»شب خلوت دنا</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بازار بعثت توبدست کمال زد</p></div>
<div class="m2"><p>مسمار نسخ بر در دکان انبیا</p></div></div>
<div class="b2" id="bn89"><p>شاگرددست تست،از آن ابر دُر فشان</p>
<p>آنجارودکه دست تواو را دهد نشان</p></div>
<div class="b" id="bn90"><div class="m1"><p>آنجاکه جای نیست،توآنجارسیده یی</p></div>
<div class="m2"><p>هرچ آن کسی ندید،تو آنرا بدیده یی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>کس را ز انبیا نرسد کآرزو کند</p></div>
<div class="m2"><p>کآنجارسدکه توبسعادت رسیده یی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بینایی ازتو دارد هردیده ورکه هست</p></div>
<div class="m2"><p>کزجمله برسرآمده چون نوردیده یی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>خودمحض رحمتی تو،خطا باشد این که من</p></div>
<div class="m2"><p>گویم برای رحمت خلق آفریده یی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ارکانِ ناگزیر سرایِ شریعتند</p></div>
<div class="m2"><p>یاران چارگانه کشان برگزیده یی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>صدیق را نواله رسانیده یی بکام</p></div>
<div class="m2"><p>ازهرطعام خوش که بخلوت چشیده یی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>فاروق راکه زهرَ گزندش نمی کند</p></div>
<div class="m2"><p>تریاکش ازعنایت خودپروریده یی</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>تا دامن قیامت در پای می کشد</p></div>
<div class="m2"><p>پیراهنی که برقد عثمان بریده یی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بیناترازعلی نبوددرجهان دین</p></div>
<div class="m2"><p>کاندر دو چشم اونفس خود دمیده یی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>زین هر دو گوشوارۀ زیبا که ازتویافت</p></div>
<div class="m2"><p>درگوشِ عرش حلقۀ منت کشیده یی</p></div></div>
<div class="b2" id="bn100"><p>ای رحمت تو دایۀ اولاد ابوالبشر</p>
<p>مارا اگرچه هیچ نیرزیم هم بخر</p></div>
<div class="b" id="bn101"><div class="m1"><p>من بنده گرچه نظم ثنای تو می کنم</p></div>
<div class="m2"><p>نظم ثنای تو نه سزای تو می کنم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>تو فارغی زمدح چومن صدهزار، لیک</p></div>
<div class="m2"><p>من خود تقربی بخدای تو م یکنم</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>خود را بزرگ می کنم اندرمیان خلق</p></div>
<div class="m2"><p>نه آنکه خدمتی ز برای تو م یکنم</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بسیارهرزه گفته ام از بهر هر کسی</p></div>
<div class="m2"><p>اکنون تدارکش بثنای تو می کنم</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>از بهر نیکنامی دنیا و آخرت</p></div>
<div class="m2"><p>نام بزرگ خویش گدای تومی کنم</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>من بس نیازمندم وخُلق توبس کریم</p></div>
<div class="m2"><p>روی طمع بسوی سخای تو می کنم</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>درمانده ام بدست غریمان مظلمه</p></div>
<div class="m2"><p>دریوزه یی زکوی عطای تو می کنم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>ناموس من مبرکه همه عمر پیش خلق</p></div>
<div class="m2"><p>دعوی بندگی و ولای تو می کنم</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>شرمندۀ گناهم وآلودۀ خطا</p></div>
<div class="m2"><p>وانگه چه آرزوی لقای تو می کنم</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>دانم که نا امید نگردم زلطف تو</p></div>
<div class="m2"><p>گراستعانتی بدعای تو می کنم</p></div></div>
<div class="b2" id="bn111"><p>شرط شفاعت تو ز ما گرکبایرست</p>
<p>بامابسی متاع ازاین جنس حاضرست</p></div>