---
title: >-
    فصل ۱۰۸
---
# فصل ۱۰۸

<div class="n" id="bn1"><p>عاشق در عشق تا بخود قائم است اخلاص و ریا هر دو وصف او را لازمست و بدان از معشوق محجوب، چون اوصاف او در او بقوت عشق باطل شوند و در خود از خود فانی بماند و ببقای محبوب باقی درین مقام لاریاءٌ وَلااخْلاصٌ لاهَلاکٌ وَلاخَلاصٌ ای برادر عاشق را رویست از آن روی برویست و کار معشوق بر خلاف اینست از آن بر سر کوی او از کشته پشته‌هاست حکیم گوید:</p></div>
<div class="b" id="bn2"><div class="m1"><p>برسر کویت بسی کشته و مرده ولی</p></div>
<div class="m2"><p>کشته نیابد قصاص مرده نیابد کفن</p></div></div>
<div class="n" id="bn3"><p>مَنْ قَتلتُه فَاَنَا دَیِتُهُ غوری عظیم دارد در بیان نتوان آورد کسی سرش نمیداند زبان درکش زبان درکش.</p></div>