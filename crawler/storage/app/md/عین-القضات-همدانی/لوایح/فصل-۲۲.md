---
title: >-
    فصل ۲۲
---
# فصل ۲۲

<div class="n" id="bn1"><p>اگر عشق شریک روحست خسارت چرا بر شریک روا می‌داری، برادر عشق مقدس است از شریک و از شبیه اما روح سر از شرکت او برمی‌آرد و از برای اثبات وحدت معشوق رقم خسارت خَسِرَ الدُّنیا وَالْآخِرَة بر روح می‌کشد با او می‌گوید که بدولت وصل آنگاه رسی که در خود برسی و به عالم اصل خود آنگاه باز شوی که با نیستی انباز شوی و بیقین بدانی:</p></div>
<div class="b" id="bn2"><div class="m1"><p>گر هر چه ترا هست همه دربازی</p></div>
<div class="m2"><p>ور هستی خود جدا کنی انبازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشد که ز خود باز رهی در تازی</p></div>
<div class="m2"><p>در پرتو نور او پناهی سازی</p></div></div>