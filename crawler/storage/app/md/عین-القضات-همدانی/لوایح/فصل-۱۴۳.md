---
title: >-
    فصل ۱۴۳
---
# فصل ۱۴۳

<div class="n" id="bn1"><p>محبت محبوب هم وصف لازمۀ وجود اوست زیرا که اعراض رابدرگاه او راه نباشد و محبت محب هم صفت لازمۀ اوست زیرا که در اوان وجود و ابداع در حقیقت وجود او تعبیه شده است و این سخن سر این معنی است که ارباب تحقیق گفته‌اند یُحِبُّونَهُ از آثار انوار یُحِبُهُم پدید آمده است:</p></div>
<div class="b" id="bn2"><div class="m1"><p>اول از او بداین حکایت عشق</p></div>
<div class="m2"><p>پس بگو عشق را بدایت نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهبر راه عشق حضرت اوست</p></div>
<div class="m2"><p>او علیمست که جز عنایت نیست</p></div></div>