---
title: >-
    فصل ۴۵
---
# فصل ۴۵

<div class="n" id="bn1"><p>عاشق چون عدم استعداد وصول در خود مشاهده کند هر آینه که فراق ابدی تصورش باید کرد و آن درد نامتناهی بود پس بدین جهت عدم خواهد و نیابد بیچاره پیوسته از درد می‌نالد و جبین بر خاک مذلت می‌مالد و می‌گوید:</p></div>
<div class="b" id="bn2"><div class="m1"><p>اندر ره عشق حاصلی باید و نیست</p></div>
<div class="m2"><p>در کوی امید ساحلی باید و نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی که بصبر کار تو نیک شود</p></div>
<div class="m2"><p>با صبر تو دانی که دلی باید و نیست</p></div></div>
<div class="n" id="bn4"><p>و آنچه مالک دینار قدّسَ اللّهُ روحَهُ گفت اَللّهُم اِذا اَدْخَلْتَنی الْجَنَّة وَ قُلْتَ رَضیتُ عَنکَ یا مالِکُ فَاجعلَنی تُراباً فَهِی الجَنَّةُ لِاَربابِها سر این معنی است.</p></div>