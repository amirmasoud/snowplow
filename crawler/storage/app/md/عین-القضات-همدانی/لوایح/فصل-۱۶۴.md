---
title: >-
    فصل ۱۶۴
---
# فصل ۱۶۴

<div class="n" id="bn1"><p>عاشق را آنچه بباید در عشق بیابد و آنچه نباید لازمۀ او بود و زهی درد بیدرمان و زهی رنج بی پایان ای عزیز عاشق را رستن از درد عشق جز بعدم نبود و در عدم بر او بسته و جان او بزخم وجود خسته چون وجود عاشق گناه کبیرۀ او بود در عشق او را تارک آن بودن بهتر و دست از آن داشتن خوشتر:</p></div>
<div class="b" id="bn2"><div class="m1"><p>اِذا قُلْتُ ما اَذْنَبْتُ قالتْ مُجیبةً</p></div>
<div class="m2"><p>وُجُودُکَ ذَنْبٌ لایُقاسٌ بِه ذَنْبٌ</p></div></div>
<div class="n" id="bn3"><p>لعمری اگر او را آن گناه نبودی که از حیز عدم قدم در ساحت وجود نهاده است این درد بی درمان را و این محنت بی پایان را با او چه کار بودی آسوده بود در قید هاویۀ عدم چون خود را در آینۀ قدم بدید شوریده شد از هاویۀ عدم قومی را بدر آورد و خود را بر ایشان عرضه کرد تا بر اوهم چواو برخود عاشق شدند پس بحجاب عزت محتجب گشت و ایشان را در درد ابدی و محنت سرمدی بگذاشت عجب رمزیست تا نبودند این بود یُحِبُّهم و نیست را هست کردن سبب همین است تا ایشان را در عالم خود می‌یافت از غیرت بوسایطشان بیرون میکرد و چون حصولشان در عالم دیگر شد در لباس قربت چشم و گوش از ایشان بسته تا او را بخود نبینند و از او چیزی بخود نشنوند و این از کمال غیرتست و مهر قهر بر بصر و سمع ایشان نهادنست و این را بواسطه دانش فهم نتوان کرد.</p></div>