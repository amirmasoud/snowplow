---
title: >-
    فصل ۱۸۲
---
# فصل ۱۸۲

<div class="n" id="bn1"><p>پادشاهی عاشقی را گفت خواهی که من باشی گفت خواهم که من نباشم یعنی چون مرا از حریت من دل گرفته است بدان عوائق که تو بدان گرفتاری کی بنده شوم ای برادر عاشق باید که آزاد بود و بغم شاد بود آرزومند و دربند بود لعمری چون آرزومند او بود:</p></div>
<div class="b" id="bn2"><div class="m1"><p>موقوف بجان اگر بمانی مانی</p></div>
<div class="m2"><p>زیرا که چو در عالم جانی جانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این نکته اگر نیک بدانی دانی</p></div>
<div class="m2"><p>هرچیز که در جستن آنی آنی</p></div></div>