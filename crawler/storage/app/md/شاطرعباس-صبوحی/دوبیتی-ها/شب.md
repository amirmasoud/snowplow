---
title: >-
    شب
---
# شب

<div class="b" id="bn1"><div class="m1"><p>دست بر زلفش زدم، شب بود، چشمش مست خواب</p></div>
<div class="m2"><p>برقع از رویش گشودم تا درآید آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش خورشید سر زد، ماه من بیدار شو</p></div>
<div class="m2"><p>گفت تا من برنخیزم، کی برآید آفتاب</p></div></div>