---
title: >-
    اشک
---
# اشک

<div class="b" id="bn1"><div class="m1"><p>چه شد که بر گل عارض گلاب می‌ریزی</p></div>
<div class="m2"><p>ستاره بر رخ این آفتاب می‌ریزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار دیده برای تو اشکریزان است</p></div>
<div class="m2"><p>چرا تو اشک به مثال حباب می‌ریزی</p></div></div>