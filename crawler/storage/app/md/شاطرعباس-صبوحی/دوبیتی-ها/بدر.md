---
title: >-
    بدر
---
# بدر

<div class="b" id="bn1"><div class="m1"><p>نموده گوشهٔ ابرو به من مهی لب بام</p></div>
<div class="m2"><p>هلال یک‌شبه دیدم به روی بدر تمام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو دیدمش به لب بام من به دل گفتم</p></div>
<div class="m2"><p>که عمر من بود این آفتاب بر لب بام</p></div></div>