---
title: >-
    دود سیه
---
# دود سیه

<div class="b" id="bn1"><div class="m1"><p>در آب پر غراب افتاده مگیر</p></div>
<div class="m2"><p>یا تودهٔ مشک ناب افتاده مگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیچیده به روی آب دود سیهی</p></div>
<div class="m2"><p>آتش به میان آب افتاده مگیر</p></div></div>