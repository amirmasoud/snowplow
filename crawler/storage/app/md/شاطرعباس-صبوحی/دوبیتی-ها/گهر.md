---
title: >-
    گهر
---
# گهر

<div class="b" id="bn1"><div class="m1"><p>هر روز گرفت خانهٔ کعبه شرف</p></div>
<div class="m2"><p>از مولد شیر حق، شهنشاه نجف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز ذات محمّدی نیامد به وجود</p></div>
<div class="m2"><p>یکتا گهری چو ذات حیدر ز صدف</p></div></div>