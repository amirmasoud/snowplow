---
title: >-
    ناله
---
# ناله

<div class="b" id="bn1"><div class="m1"><p>چشمان تو با فتنه به جنگ آمده است</p></div>
<div class="m2"><p>ابروی تو غارت فرنگ آمده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز به دل تو ناله تأثیر نکرد</p></div>
<div class="m2"><p>اینجاست که تیر ما به سنگ آمده است</p></div></div>