---
title: >-
    ستمگر
---
# ستمگر

<div class="b" id="bn1"><div class="m1"><p>هر قدر زلف تو ای سلسله مو سلسله دارد</p></div>
<div class="m2"><p>به همان قدر دلم از تو ستمگر گله دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتیم صبر نما تا ز لبم کام بگیری</p></div>
<div class="m2"><p>آخر ای سنگدل این دل چقدر حوصله دارد</p></div></div>