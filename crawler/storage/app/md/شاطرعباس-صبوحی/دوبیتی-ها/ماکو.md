---
title: >-
    ماکو
---
# ماکو

<div class="b" id="bn1"><div class="m1"><p>خیّاط پسری بود به دستش ماکو</p></div>
<div class="m2"><p>گفتم که دلی که برده‌ای از ما کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا که دل تو در کف من خون شد</p></div>
<div class="m2"><p>از او اثری اگر بخواهی ماکو</p></div></div>