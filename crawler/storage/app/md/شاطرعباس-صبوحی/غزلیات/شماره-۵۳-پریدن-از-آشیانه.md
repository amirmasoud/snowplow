---
title: >-
    شمارهٔ  ۵۳ - پریدن از آشیانه
---
# شمارهٔ  ۵۳ - پریدن از آشیانه

<div class="b" id="bn1"><div class="m1"><p>ترنج غبغب آن یوسف عزیز چو دیدم</p></div>
<div class="m2"><p>چنان شدم که به جای ترنج، دست بریدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز قهر تیغ کشیدی، به سوی من بدویدی</p></div>
<div class="m2"><p>ز من تو سر ببریدی، من از تو دل نبریدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشست یار به محفل، گذشت قافله غافل</p></div>
<div class="m2"><p>که هر چه من بدویدم، به گرد او نرسیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مپرس حالت مجنون ز سایه پرور شهری</p></div>
<div class="m2"><p>ز من بپرس که با سر، به کوی دوست دویدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا هوای پریدن نبود از پی طوبی</p></div>
<div class="m2"><p>بهشت روی تو دیدم، از آشیانه پریدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توئی که سوختی‌م از فراق و رحم نکردی</p></div>
<div class="m2"><p>منم که سوختم و ساختم، نفس نکشیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رموز غیب که یزدان بجبرئیل نگفتی</p></div>
<div class="m2"><p>من از گدای در کوی می فروش شنیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر آنچه تخم طرب کاشتم به مزرعهٔ دل</p></div>
<div class="m2"><p>ز بخت بد چو صبوحی گیاه غم درویدم</p></div></div>