---
title: >-
    شمارهٔ  ۵۴ - سرخوشم
---
# شمارهٔ  ۵۴ - سرخوشم

<div class="b" id="bn1"><div class="m1"><p>خوش می‌کشد به سوی تو این عشق سرکشم</p></div>
<div class="m2"><p>گر از جفا رقیب نسازد مشوّشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه خال دانه می‌کشدم گه کمند زلف</p></div>
<div class="m2"><p>چون صید ناتوان ز جفا در کشاکشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آب چشم و آتش دل بی تو هر زمان</p></div>
<div class="m2"><p>گاهی در آب غوطه ور و گه در آتشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر صد رهم رقیب کشد از جفا هنوز</p></div>
<div class="m2"><p>من با امید وصل تو با باده سرخوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سیل اشک و نالهٔ غم آه دردناک</p></div>
<div class="m2"><p>سوزد درون و چهرهٔ از خون منقّشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبود متاع دیگرم اندر دیار عشق</p></div>
<div class="m2"><p>ای وای اگر مدد نکند بخت سرکشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانا به وری و موی عزیزت که در جهان</p></div>
<div class="m2"><p>یکدم خیال روی تو نبود فرامشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم که ناخوشم ز غم هجر و انتظار</p></div>
<div class="m2"><p>گفتا خموش باش صبوحی که من خوشم</p></div></div>