---
title: >-
    شمارهٔ  ۱۱ - گل بادام
---
# شمارهٔ  ۱۱ - گل بادام

<div class="b" id="bn1"><div class="m1"><p>سُرخ و بیجادهٔ رخ و تازه لب از باده و مست</p></div>
<div class="m2"><p>رفته از غایت مستی گل بادام از دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مترشح غد و موزون قد و میگون لب و مست</p></div>
<div class="m2"><p>جامه گلنار و کمر زرکش و ساغر در دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طرّه‌اش شعبده‌باز و نگهش شهرآشوب</p></div>
<div class="m2"><p>چشم بیمار و دو ابروی وی بیمارپرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر زلفش که به تحریک صبا رقصی داشت</p></div>
<div class="m2"><p>هر قدم طبلهٔ مُشکی به سر توده شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیرگاه از می هوش آمد و بیمارم دید</p></div>
<div class="m2"><p>گفت افسوس که بر دیده ره خوابت هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم از دست خیال تو، بخندید و بگفت</p></div>
<div class="m2"><p>کامشب آیا هوس وصل نگارینت هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جستم از جای بصد شوق که آری آری</p></div>
<div class="m2"><p>ای مبارک شب آنکس ز هجر تو برست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرو قدّش بخرام آمد و با صد شفقت</p></div>
<div class="m2"><p>بر سر کهنه لحافی که مرا بود نشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرد تا وقت صباحم به صبوحی مشغول</p></div>
<div class="m2"><p>ز اختلاط می و معشوق شدم بی‌خود و مست</p></div></div>