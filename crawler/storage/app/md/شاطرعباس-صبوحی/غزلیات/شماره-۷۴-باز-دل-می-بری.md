---
title: >-
    شمارهٔ  ۷۴ - باز، دل می‌بری 
---
# شمارهٔ  ۷۴ - باز، دل می‌بری 

<div class="b" id="bn1"><div class="m1"><p>ای که صد سلسله دل، بسته به هر مو داری</p></div>
<div class="m2"><p>باز دل می‌بری از خَلق، عجب رو داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون عشّاق، حلال است، مگر در بر تو</p></div>
<div class="m2"><p>که به دل، عادت چنگیز و هلاکو داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گل و لاله و سرو لب جو، بیزارم</p></div>
<div class="m2"><p>تا تو بر سرو قدت روضهٔ مینو داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو پریزاده نگردی به جهان، رام کسی</p></div>
<div class="m2"><p>حالت مرغ هوا، شیوهٔ آهو داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این خط سبز بود سر زده زان شکّر لب</p></div>
<div class="m2"><p>یا که در آب بقا، سبزهٔ خودرو داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جای مستان همه در گوشهٔ محراب افتاد</p></div>
<div class="m2"><p>تا که بالای دو چشمت خم ابرو داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر صبوحی شده پا بست تو، این نیست عجب</p></div>
<div class="m2"><p>تا که صد سلسله دل، در خم گیسو داری</p></div></div>