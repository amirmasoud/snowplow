---
title: >-
    شمارهٔ  ۱۵ - نیاز
---
# شمارهٔ  ۱۵ - نیاز

<div class="b" id="bn1"><div class="m1"><p>تا چند پیش ناز تو باید نیاز کرد</p></div>
<div class="m2"><p>بر ناز خود بناز که نازت کشیدنی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمت نظر ز لُطف به عاشق نمی‌کند</p></div>
<div class="m2"><p>چون آهوی ختائی کارش رمیدنی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مشربت زلال لبست کام دل برآر</p></div>
<div class="m2"><p>سرچشمهٔ حیات زلالت چشیدنی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشدل مرا نمای بد شنامت ای حبیب</p></div>
<div class="m2"><p>چون حرف تلخ از لب شیرین شنیدنی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر نقد جان دو بوسه ز لعل تو خواستم</p></div>
<div class="m2"><p>گرچه گرانبهاست ولیکن خریدنی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانع مشو ز لعل لبت بوسه خواستم</p></div>
<div class="m2"><p>هر شکرین لبی نمکین شد چشیدنی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز من هر آنکه دست صبوحی بتو فکند</p></div>
<div class="m2"><p>قطعش نما ز دوش که دستش بریدنی است</p></div></div>