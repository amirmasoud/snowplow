---
title: >-
    شمارهٔ  ۶۶ - آیهٔ رحمت
---
# شمارهٔ  ۶۶ - آیهٔ رحمت

<div class="b" id="bn1"><div class="m1"><p>غبار نیست که بر گرد عارض ترش است این</p></div>
<div class="m2"><p>گذشته پادشه حُسن گَرد لشکرش است این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه خط غالیه سا دور عارض مهش است این</p></div>
<div class="m2"><p>همای حُسن پریده است و سایهٔ پرش است این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستاده بر سر نعشم، گرفته دست به مژگان</p></div>
<div class="m2"><p>که این قتیل نگاه منست و خنجرش است این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کتاب نیست که می‌خواند آن نگار به مکتب</p></div>
<div class="m2"><p>کند حساب شهیدان خویش و دفترش است این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان آبله دیدم به روی یار بگفتم</p></div>
<div class="m2"><p>قسم به آیهٔ رحمت که اصل جوهرش است این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار مرتبه بر قبر من گذشت و نگفتا</p></div>
<div class="m2"><p>که این شهید، شهید من است و مقبرش است این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر در آینه کرد آن نگار رو با خود گفت!</p></div>
<div class="m2"><p>خوشا به حال دل عاشقی که دلبرش است این</p></div></div>