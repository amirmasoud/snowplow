---
title: >-
    شمارهٔ  ۲۹ - دل گمشده
---
# شمارهٔ  ۲۹ - دل گمشده

<div class="b" id="bn1"><div class="m1"><p>گرهی از خم آن زلف چلیپا وا شد</p></div>
<div class="m2"><p>هرکجا بود، دل گمشده‌ای پیدا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به آهوی ختا، نسبت چشمت دادیم</p></div>
<div class="m2"><p>گنه از جانب او نیست، خطا از ما شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گندم خال تو در خُلد، ره آدم زد</p></div>
<div class="m2"><p>زلف شیطان صفتت راهزن حوا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک چشمان تو مستند و، دو شمشیر به دست</p></div>
<div class="m2"><p>از دو بد مست یکی شهر پر از غوغا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن از لعل تو، هر جا که روم می‌شنوم</p></div>
<div class="m2"><p>این چه سرّیست که در دوره ما پیدا شد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا رب! این خرمن گل چیست که از نکهت او</p></div>
<div class="m2"><p>آتشی حاصل و جانسوز من شیدا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ارنی گفت دلم بهر تماشای رُخش</p></div>
<div class="m2"><p>لن ترانی به جواب، از دو لبش گویا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌سبب رهزن میخانه صبوحی گشته</p></div>
<div class="m2"><p>رهزن دین و دلم آن صنم ترسا شد</p></div></div>