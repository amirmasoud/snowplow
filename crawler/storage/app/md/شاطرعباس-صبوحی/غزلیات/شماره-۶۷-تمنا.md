---
title: >-
    شمارهٔ  ۶۷ - تمنّا
---
# شمارهٔ  ۶۷ - تمنّا

<div class="b" id="bn1"><div class="m1"><p>زلفت از سنبل تر سر زده بر طرف چمن</p></div>
<div class="m2"><p>کاکلت بسته صف از ملک حبش لشکر چین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ختا و ختن ای خسرو خوبان جهان</p></div>
<div class="m2"><p>چون تو شوخی نبود در همهٔ چین و ماچین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب من با لب تو نرد به بوسی می‌باخت</p></div>
<div class="m2"><p>لب لشکر شکنت گفت که بردی بر چین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواستم جوهر هندو ز لبت بر چینم</p></div>
<div class="m2"><p>لب تو گفت بچین، غمزهٔ تو گفت مچین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من از این چین و مچین، واله و شیدا چه کنم</p></div>
<div class="m2"><p>سر زلف بت شکر شکنت برده ز چین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گل روش صبوحی چه تمنّا داری</p></div>
<div class="m2"><p>غنچه این لحظه تو از باغ وصالش برچین</p></div></div>