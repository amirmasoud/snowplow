---
title: >-
    شمارهٔ  ۴ - آرزو
---
# شمارهٔ  ۴ - آرزو

<div class="b" id="bn1"><div class="m1"><p>اگر روزی بدست آرم سر زلف نگارم را</p></div>
<div class="m2"><p>شمارم مو به مو شرح غم شب‌های تارم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برای جان سپردن، کوی جانان آرزو دارم</p></div>
<div class="m2"><p>که شاید با دو سیل او، برد خاک مزارم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارم حاجت فصل بهاران با گل و گلشن</p></div>
<div class="m2"><p>به باغ حسن اگر بینم نگار گلعذارم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگرد عارضش چون سبز شد خط من، به دل گفتم</p></div>
<div class="m2"><p>سیه بین روزگارم را، خزان بنگر بهارم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تمنّا داشتم عین وصالش در شب هجران</p></div>
<div class="m2"><p>صبا بوئی از آن آورد و برد از دل قرارم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان امید از احسان که در پایش فشانم جان</p></div>
<div class="m2"><p>که از شفقّت بدست آرد دل امیدوارم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مریض عشق را نبود دوائی غیر جان دادن</p></div>
<div class="m2"><p>مگر وصل تو سازد چاره، درد انتظارم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو یارم ساخت با اغیار و من جان دادم از حسرت</p></div>
<div class="m2"><p>بگویید ای برادر آن بت ناسازگارم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبوحی را سگ دربان خود خواند آن پری از مهر</p></div>
<div class="m2"><p>میان عاشقان افزوده قدر و اعتبارم را</p></div></div>