---
title: >-
    شمارهٔ  ۵۱ - حلقۀ زلف
---
# شمارهٔ  ۵۱ - حلقۀ زلف

<div class="b" id="bn1"><div class="m1"><p>تا در آن حلقهٔ زلف تو گرفتار شدم</p></div>
<div class="m2"><p>سوختم تا که من از عشق خبردار شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چه کردم که چنین از نظرت افتادم</p></div>
<div class="m2"><p>چاره‌ای کن که به لُطف تو گنهکار شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواب دیدم که سر زلف تو در دستم بود</p></div>
<div class="m2"><p>بوی عطری به مشامم زد و بیدار شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا در آن سلسلهٔ زلف تو افتادم من</p></div>
<div class="m2"><p>بی‌سبب چیست که پیش نظرت خوار شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو ای باد صبا بر سر کویش تو بگو</p></div>
<div class="m2"><p>که ز مهجوری تو دست و دل از کار شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان به لب آمد و راز تو نگفتم به کسی</p></div>
<div class="m2"><p>نقد جان دادم و عشق تو خریدار شدم</p></div></div>