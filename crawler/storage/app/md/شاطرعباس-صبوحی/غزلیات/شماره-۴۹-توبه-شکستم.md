---
title: >-
    شمارهٔ  ۴۹ - توبه شکستم
---
# شمارهٔ  ۴۹ - توبه شکستم

<div class="b" id="bn1"><div class="m1"><p>پیوسته من از شوق لب لعل تو مستم</p></div>
<div class="m2"><p>زین ذوق که دارم خبرم نیست که هستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بادیهٔ عشق تو تا پای نهادم</p></div>
<div class="m2"><p>یکباره بشد دین و دل و عقل ز دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بس که نظر بر گُل رخسار تو دارم</p></div>
<div class="m2"><p>شد شهره بهر شهر که خورشید پرستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو مهر ز من ای بت عیّار بریدی</p></div>
<div class="m2"><p>من دل به خَم طرّه طرّار تو بستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مُطرب تو بزن ساز نوایی به درستی</p></div>
<div class="m2"><p>ساقی تو بده باده که من توبه شکستم</p></div></div>