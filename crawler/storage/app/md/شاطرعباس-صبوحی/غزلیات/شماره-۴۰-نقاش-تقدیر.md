---
title: >-
    شمارهٔ  ۴۰ - نقاش تقدیر
---
# شمارهٔ  ۴۰ - نقاش تقدیر

<div class="b" id="bn1"><div class="m1"><p>سالها قد تو را خامهٔ تقدیر کشید</p></div>
<div class="m2"><p>قامتت بود قیامت که چنین دیر کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواست رخسار تو با زلف گره گیر کشد</p></div>
<div class="m2"><p>فکرها کرد که باید به چه تدبیر کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدتی چند بپیچید به خود و آخر کار</p></div>
<div class="m2"><p>ماه را از فلک آورد و به زنجیر کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جای ابروی تو نقاش، پس از آهوی چشم</p></div>
<div class="m2"><p>تا به بازیچه نگیرند دم شیر کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد چشم تو، مصوّر چو به ابرو پرداخت</p></div>
<div class="m2"><p>شد چنان مست که بر روی تو شمشیر کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل اسیر مژه‌ات از عدم آمد به وجود</p></div>
<div class="m2"><p>همچو صیدی که مصوّر به دم شیر کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش تشریف رسای کرم دوست ازل</p></div>
<div class="m2"><p>منّت از کوتهی خامهٔ تقدیر کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لاغری بین که در اندیشهٔ نقشم، نقّاش</p></div>
<div class="m2"><p>آنقدر ماند که تصویر مرا پیر کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر خرابم کنی ای عشق چنان کن، باری</p></div>
<div class="m2"><p>که نشاید دگرم منّت تعمیر کشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نتوان بهر علاج دل دیوانهٔ ما</p></div>
<div class="m2"><p>از سر زلف به دوش این همه زنجیر کشید</p></div></div>