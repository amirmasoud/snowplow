---
title: >-
    شمارهٔ  ۱۰ - نقش خود
---
# شمارهٔ  ۱۰ - نقش خود

<div class="b" id="bn1"><div class="m1"><p>مکن دریغ ز من ساقیا شراب امشب</p></div>
<div class="m2"><p>از آنکه ز آتش خود، گشته‌ام کباب امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بس‌که شعله زند در دل من، آتش شوق</p></div>
<div class="m2"><p>ز آتش دل خویشم در التهاب امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خواب دیده ام آن چشم نیم خوابش دوش</p></div>
<div class="m2"><p>گمان مبر که رود دیده ام به خواب امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دست نرگس مستش برفت دل، از کف</p></div>
<div class="m2"><p>نگر به حال دلم از ره ثواب امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد آنکه بادهٔ پنهان کشیدمی همه عمر</p></div>
<div class="m2"><p>بده به بانگ نی و نغمهٔ رباب امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس‌که نقش مخالف ز دوستان دیدم</p></div>
<div class="m2"><p>بر آن شدم که زنم نقش خود بر آب امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شود خراب چو این خانه لاجرم روزی</p></div>
<div class="m2"><p>ز سیل باده بهل تا شود خراب امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم که داشت قرار اندر آن دو زلف چو شب</p></div>
<div class="m2"><p>بود چو گوی بچوگان در اضطراب امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبوحی دل مده از دست، محکمش میدار</p></div>
<div class="m2"><p>که چشم یار، بود بر سر عتاب امشب</p></div></div>