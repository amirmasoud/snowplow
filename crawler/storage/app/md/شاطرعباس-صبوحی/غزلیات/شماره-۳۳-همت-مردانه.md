---
title: >-
    شمارهٔ  ۳۳ - همت مردانه
---
# شمارهٔ  ۳۳ - همت مردانه

<div class="b" id="bn1"><div class="m1"><p>ای خوش آنانکه قدم بر در میخانه زدند</p></div>
<div class="m2"><p>بوسه دادند لب ساقی و پیمانه زدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حقارت منگر باده کشان را، کاین قوم</p></div>
<div class="m2"><p>پشت پا بر فلک از همّت مردانه زدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون من باد حلال لب شیرین دهنان</p></div>
<div class="m2"><p>که به کار دل من، خندهٔ مستانه زدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانم آمد به لب امروز، مگر یاران دوش</p></div>
<div class="m2"><p>قدح باده به یاد لب جانانه زدند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردم از حسرت جمعی که از آن حلقهٔ زلف</p></div>
<div class="m2"><p>سر زنجیر به پای من دیوانه زدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقبت یک تن از آن قوم نیامد به کنار</p></div>
<div class="m2"><p>که به دریای غمت از پی دردانه زدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ حضرت شاهی شدم از دولت عشق</p></div>
<div class="m2"><p>که گدایان درش، افسر شاهانه زدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچکس در حرمش راه ندارد کاینجا</p></div>
<div class="m2"><p>دست محروم به هر محرم و بیگانه زدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه کاشانهٔ دل، خاص غم مهر تو نیست</p></div>
<div class="m2"><p>پس، چرا مهر تو را بر در این خانه زدند؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل گم گشتهٔ ما را نبود هیچ نشان</p></div>
<div class="m2"><p>مو به مو هر چه سر زلف تو را شانه زدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آخر از پیرهن چاک صبوحی سر زد</p></div>
<div class="m2"><p>آتشی را که نهان بر پر پروانه زدند</p></div></div>