---
title: >-
    شمارهٔ  ۳۲ - خمخانه حق
---
# شمارهٔ  ۳۲ - خمخانه حق

<div class="b" id="bn1"><div class="m1"><p>شام هجران مرا صبح نمایان آمد</p></div>
<div class="m2"><p>محنت آخر شد و اندوه به پایان آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس باد صبا باز مسیحائی کرد</p></div>
<div class="m2"><p>مگر از زلف خم در خم جانان آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکر ایزد که دگر بار، به کوری رقیب</p></div>
<div class="m2"><p>دلبرم شاد رخ و خرّم و خندان آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجبی نیست گرم از کرم پیر مغان</p></div>
<div class="m2"><p>رنج راحت شد و هم درد به درمان آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه بسیار چشیدی ستم ز هر فراق</p></div>
<div class="m2"><p>دلبر شاد به بزمت شکرستان آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقیا! ساغر لبریز از آن باده بده</p></div>
<div class="m2"><p>که ز خمخانهٔ حق، هدیه به مستان آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطرب! آغاز کن آن نغمهٔ داوودی را</p></div>
<div class="m2"><p>که ز الحان خوشش، جان به سلیمان آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مژده ای صدر نشینان صف میکده، باز</p></div>
<div class="m2"><p>که صبوحی ز حرم مست و غزل‌خوان آمد</p></div></div>