---
title: >-
    شمارهٔ  ۱ - گرفتاران
---
# شمارهٔ  ۱ - گرفتاران

<div class="b" id="bn1"><div class="m1"><p>پدر، خواهد ببرّد زلفکان چون کمندش را</p></div>
<div class="m2"><p>پسر حیران، که چون سازد گرفتاران بندش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند کوتاه، دست از زلف و از لعل شکر خندش</p></div>
<div class="m2"><p>نداند کاین دو هندو، پاسبانانند قندش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپندش خال و دودش زلف و آتش، پرتو رویش</p></div>
<div class="m2"><p>عبث بی‌دود می‌خواهی بر این آتش، سپندش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکرده هیچ ابرو خم به قطع زلف می‌ماند</p></div>
<div class="m2"><p>کمانداری که داد از دست ار پیچان کمندش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبوحی آنقدر نگذاشت آن زلف تا برجا</p></div>
<div class="m2"><p>که گیری یک شب و بوسی دو لعل نوشخندش را</p></div></div>