---
title: >-
    قند لعل
---
# قند لعل

<div class="b" id="bn1"><div class="m1"><p>ره دل را بتا زان شوخ چشم مست رهزن زن</p></div>
<div class="m2"><p>به عیاری زُلفت خویش را غافل به مخزن زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقاب پرنیان را برفکن از چهر آذرگون</p></div>
<div class="m2"><p>شرر از چشمه خورشیدوش بر مرد و بر زن زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رقیب بوالهوس در بزم از روزن نظر دارد</p></div>
<div class="m2"><p>کمان ابرو خدنگی بر دو چشمانش ز روزن زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر خواهی نما شیرین مذاق عاشقانت را</p></div>
<div class="m2"><p>ز قند لعل خود کام صبوحی را یک ارزن زن</p></div></div>