---
title: >-
    بخش ۳۲ - اردو کشی داریوش بهندوستان
---
# بخش ۳۲ - اردو کشی داریوش بهندوستان

<div class="b" id="bn1"><div class="m1"><p>سه سالیچو بگذشت از عیش و نوش</p></div>
<div class="m2"><p>بفرمود آنگه شه داریوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا جنگ هندوستان است پیش</p></div>
<div class="m2"><p>که باید به پیمایم این راه خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با سپهبدان زان بفرمود شاه</p></div>
<div class="m2"><p>ببینید سان سپه صبح گاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شد صبح و خورشید رخشان دمید</p></div>
<div class="m2"><p>پرا کنده شد نورو خور سر کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منور شد از نور رویش جهان</p></div>
<div class="m2"><p>ز خواب اندر آمد کهان و مهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صدای تییره ز ارکان جنگ</p></div>
<div class="m2"><p>بیامد بگوش دلیران جنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آمد ز جا لشگر جاودان</p></div>
<div class="m2"><p>شتابان بدرگاه شاه جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی بود تعداد شان ده هزار</p></div>
<div class="m2"><p>دلیران شیر افکن نامدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همیشه مسلح بدرگاه شاه</p></div>
<div class="m2"><p>بپا ایستاده بدند آن سپاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر یک تن از آن سپاه دلیر</p></div>
<div class="m2"><p>شدی کشته یا شد بمیدان اسیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزودی بجایش یکی شیرمرد</p></div>
<div class="m2"><p>مهیا نمودی بگاه نبرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بد این لشگر نیک مخصوص شاه</p></div>
<div class="m2"><p>مسلح مهیا بدرگاه شاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صد افسر بر ایشان بدی نامدار</p></div>
<div class="m2"><p>همه مرد جنگی گه کارزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر افسران بود مردونیه</p></div>
<div class="m2"><p>چو برگشت از جنگ مقدونیه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شمار یلان بد برون از حساب</p></div>
<div class="m2"><p>بدیدند سان جمله پا در رکاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درگنج شاهی دگر باز شد</p></div>
<div class="m2"><p>همی کار لشگر دگر ساز شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیک هفته لشگر کلاه و کمر</p></div>
<div class="m2"><p>گرفتند شمشیر و گرز و سپر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وزان پس ذخیره نمودند بار</p></div>
<div class="m2"><p>ز سیم و ز زر هر چه آید کار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو آماده شد کار لشگر همه</p></div>
<div class="m2"><p>بایران بیفتاد بس همهه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شهنشه بسرداد خود امر داد</p></div>
<div class="m2"><p>بفرمود ای مرد نیکو نژاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو این مملکت را به نیکی بدار</p></div>
<div class="m2"><p>بهندوستان من کنم کارزار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سپهدار گفتا یکی بنده ام</p></div>
<div class="m2"><p>بفرمان و رأیت سرافکنده ام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سحرگه چو برداشت بانگ خروس</p></div>
<div class="m2"><p>ز ارکان جنگ آمد آوای کوس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دلیران سر از خواب برداشتند</p></div>
<div class="m2"><p>که رخت سفر را به بر داشتند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سواران بدرگاه شاه آمدند</p></div>
<div class="m2"><p>گشاده رخ و نام خواه آمدند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بمیدان شاهی کشیدند صف</p></div>
<div class="m2"><p>سرو جان خود را نهادند کف</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو آنان همی خواندندی سرود</p></div>
<div class="m2"><p>بشاهنشه پارس دادی درود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که ما نوجوانان ایران زمین</p></div>
<div class="m2"><p>نترسیم از لشگر هند و چین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بکوشیم بهر وطن ما ز جان</p></div>
<div class="m2"><p>نخواهیم هرگز ز دشمن امان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بنام شهنشاه شه داریوش</p></div>
<div class="m2"><p>ز هندو برآیم ما چشم و گوش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دلیریم ما همچو شیران نر</p></div>
<div class="m2"><p>بگیریم ملک جهان سربسر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که ایران ما هست شاهنشهی</p></div>
<div class="m2"><p>زما نونهالان بیامد بسی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شهنشاه کشور گشا داریوش</p></div>
<div class="m2"><p>شه پارسی شاه با رأی و هوش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که او شهریار است ما بندگان</p></div>
<div class="m2"><p>بپا ایستاده کمر بر میان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدرگاه او ما نهادیم رو</p></div>
<div class="m2"><p>بپا ایستادیم با آبرو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سرو جان ما جمله در راه شاه</p></div>
<div class="m2"><p>رود،رو نه بیچیم ز آوردگاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بریزیم خون را براه وطن</p></div>
<div class="m2"><p>گر از خون نمایند ما را کفن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه نام جوی و همه نیک خواه</p></div>
<div class="m2"><p>گذشته ز جان در رکاب تو شاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دعا گوی شه نیک خواه وطن</p></div>
<div class="m2"><p>ز جان و ز سر نیست مارا سخن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بگوئیم یکسر که شه زنده باد</p></div>
<div class="m2"><p>کزین شاه ایرانیان گشت شاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شهنشه سرود دلیران شنید</p></div>
<div class="m2"><p>دلش شاد تر شد چو این سان بدید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بیامد شهنشه بکاخ بهار</p></div>
<div class="m2"><p>بدید این همه افسر نامدار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مسلح همه با لباس سفر</p></div>
<div class="m2"><p>ابا تیغ و کوپال و گرز و سپر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شه از دور دیدند و در احترام</p></div>
<div class="m2"><p>کشیدند شمشیرها از نیام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خبردار گفتند و لشگر سوار</p></div>
<div class="m2"><p>بشد جمله آماده کارزار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جنیبت کشان و جلو دار ها</p></div>
<div class="m2"><p>سواران مخصوص و سردار ها</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>درفش درخشان بدی دستشان</p></div>
<div class="m2"><p>جهانی همی بود در شستشان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همان پرچم پارس بد پیش رو</p></div>
<div class="m2"><p>همی میکشیدند با های و هو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شهنشاه بر اسب تازی سوار</p></div>
<div class="m2"><p>سواران مخصوص شه ده هزار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شمار یلان بود یکصد هزار</p></div>
<div class="m2"><p>دلیران مرد افکن نامدار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>صدای تبیره شده بر فلک</p></div>
<div class="m2"><p>سر از ابر بیرون نموده ملک</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بهندوستان چون رسید آگهی</p></div>
<div class="m2"><p>که اینک رسد فر شاهنشهی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شهنشاه ایران ابا صد هزار</p></div>
<div class="m2"><p>دلیران نام آور کارزار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هزار افسرانند چون شیر نر</p></div>
<div class="m2"><p>مسلح بشمشیر و گرز و سپر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همه جنگ جوی و همه نیک نام</p></div>
<div class="m2"><p>همه سرفراز و همه شاد کام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو بشنید آنشاه هندیان</p></div>
<div class="m2"><p>همی نامه بنوشت بر دوستان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز هرجای لشگر همی خواستند</p></div>
<div class="m2"><p>سپاهی که باید بیاراستند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو آماده شد لشگر هندیان</p></div>
<div class="m2"><p>کمر تنگ بستند اندر میان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بدان منتظر تا بیاید سپاه</p></div>
<div class="m2"><p>همه چشمها سوی کشتی شاه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از آن رو شهنشاه ایران پناه</p></div>
<div class="m2"><p>بدریای هندوستان با سپاه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>همان دیده بانان هندوستان</p></div>
<div class="m2"><p>نظاره نمودند از بوستان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کشیدند فریاد کآمد سپاه</p></div>
<div class="m2"><p>ز کشتی شده روی دریا سیاه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سپهدار هندوستان شد خبر</p></div>
<div class="m2"><p>که آمد ز دریا شه نامور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بفرمود بندید بر شاه، راه</p></div>
<div class="m2"><p>ز دریا نیاید برون آن سپاه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کشیدند صف لشگر هندوان</p></div>
<div class="m2"><p>گرفتند بر دست تیر و کمان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نمودند بر لشگر شهریار</p></div>
<div class="m2"><p>چنان تیر باران چو ابر بهار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شهنشه بفرمود با لشگران</p></div>
<div class="m2"><p>نترسید از لشگر هندوان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شما هم همی تیر باران کنید</p></div>
<div class="m2"><p>کمان را چو ابر بهاران کنید</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زبر دست بودند ایرانیان</p></div>
<div class="m2"><p>فراری شده لشگر هندوان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به پنجاب آمد فرود آن سپاه</p></div>
<div class="m2"><p>بشد دشت پنجاب یکسر سیاه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو شد روز دیگر شه داریوش</p></div>
<div class="m2"><p>یکی مرد بیدار و با عقل و هوش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بفرمود رو شهر هندوستان</p></div>
<div class="m2"><p>به پنجاب رو توی آن بوستان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بگو با شه هند از من پیام</p></div>
<div class="m2"><p>نخواهید من تیغ کین از نیام</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>برانم همه هند ویران کنم</p></div>
<div class="m2"><p>همان لشگرت را پریشان کنم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>اگر خود بیائی بدرگاه ما</p></div>
<div class="m2"><p>نیارید همراه خود از سپاه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>شما را بسی مهربانی کنم</p></div>
<div class="m2"><p>نه جنگ و نه من سرگرانی کنم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>وگرنه من و گرز و میدان هند</p></div>
<div class="m2"><p>بکوبم ز پنجاب تا رود سند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>جهانی مرا لشگر و کشور است</p></div>
<div class="m2"><p>همان شهریاری مرا در خوراست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نه دریا بمانم نه صحرا نه هند</p></div>
<div class="m2"><p>زنم آتش از هند تا رود سند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>فرستاده آمد به هندوستان</p></div>
<div class="m2"><p>بنزد شهنشاه آن بوستان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بدادی چو پیغام شه داریوش</p></div>
<div class="m2"><p>شه هند پیغام ننمود گوش</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بگفتا برو نزد آن شهریار</p></div>
<div class="m2"><p>بگو لشگرت گر بود صد هزار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>مرا لشگر هند صد لشگر است</p></div>
<div class="m2"><p>ز پروین سپاه من افزون تر است</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>مرا پیل جنگی بود ده هزار</p></div>
<div class="m2"><p>بهر پیل ده مرد زوبین گراز</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ز کشمیر و پنجاب تا رود سند</p></div>
<div class="m2"><p>ز اینجا و کلکته تا بحر هند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>سراسر همه خود سپاه منند</p></div>
<div class="m2"><p>مسلح همه در پناه منند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نیایم مگر با سپاه دلیر</p></div>
<div class="m2"><p>ابا پیل و با گرز و شمشیر و تیر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چو قاصد بیامد بر شهریار</p></div>
<div class="m2"><p>بگفت آنچه بشنید آن نامدار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>شهنشه برآشفت از این پیام</p></div>
<div class="m2"><p>بگفتا کشم تیغ کین از نیام</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نه هندی بماند نه هندوستان</p></div>
<div class="m2"><p>نه فیل و نه سروی در آن بوستان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>شهنشه بفرمود فردا بگاه</p></div>
<div class="m2"><p>بمیدان رژه برکشد این سپاه</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>شه هند خود لشگری بیشمار</p></div>
<div class="m2"><p>رده برکشیده بمیدان کار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بکوشید و در هند نام آورید</p></div>
<div class="m2"><p>سر هندیان را به دام آورید</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>سپهبد بگفتا که فرمان برم</p></div>
<div class="m2"><p>دمی من ز فرمان شه نگذرم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بهندو نمایم چنان رسته خیز</p></div>
<div class="m2"><p>که هرگز نیابند راه گریز</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بپیلانشان تیر باران کنم</p></div>
<div class="m2"><p>کمان را چو ابر بهاران کنم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>نه هندو گذارم نه پیلان شان</p></div>
<div class="m2"><p>زهندی نمانم بعالم نشان</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بگفتا که فردا باقبال شاه</p></div>
<div class="m2"><p>نه هندو گذارم نه فیل و سپاه</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>سحرگه چو شد لشکر زنگبار</p></div>
<div class="m2"><p>فراری ز خورشید از کوه و غار</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>همان نیر اعظم با شکوه</p></div>
<div class="m2"><p>پراکند نوری بصحرا و کوه</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چو روشن شود عالم از نور او</p></div>
<div class="m2"><p>بجوش آورد آدم از شور او</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>برآمد زجا لشکر نامدار</p></div>
<div class="m2"><p>برفتند مردان گه کارزار</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>رژه برکشیدند از هر طرف</p></div>
<div class="m2"><p>کمان های چاچی گرفته بکف</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>همان نیزه داران پیاده سپاه</p></div>
<div class="m2"><p>سواران براسبان همی کینه خواه</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>درفش درخشان ببالای سر</p></div>
<div class="m2"><p>ازو چشمها خیره شد سربسر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>شهنشاه خود بود قلب سپاه</p></div>
<div class="m2"><p>دلیران ز دشمن همه کینه خواه</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>از آن رو دگر لشکر هندوان</p></div>
<div class="m2"><p>مسلح بشمشیر و گرز و سنان</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>رژه بر کشیدند با های و هوی</p></div>
<div class="m2"><p>همه کینه خواه و همه کینه جو</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>ز ایرانیان نوجوانی دلیر</p></div>
<div class="m2"><p>بمیدان همی تاخت چون نره شیر</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>زدل او چنان نعره ای بر کشید</p></div>
<div class="m2"><p>کز آن زهره هندوان بردرید</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>شه هندوان گفت با لشکران</p></div>
<div class="m2"><p>بگیرید این مرد را در میان</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>براو تیر بارید همچون تگرگ</p></div>
<div class="m2"><p>که از تن بر آرد همی شاخ و برگ</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>از آنرو سپاه شه نامجو</p></div>
<div class="m2"><p>همه سوی میدان نهادند رو</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بکشتند تا شام از یکدگر</p></div>
<div class="m2"><p>نه این را شکست و نه آن را ظفر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چو یک چند روزی بشد این نبرد</p></div>
<div class="m2"><p>شهنشه ز هندو بر آورد گرد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>که هندو نهادند رو برفرار</p></div>
<div class="m2"><p>تعاقب نمودند آن شه نامدار</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>خلاصه گرفتند پنجاب و سند</p></div>
<div class="m2"><p>مسخر نمودند تا بحر هند</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>شه آنگه بفرمود سردار را</p></div>
<div class="m2"><p>که جوید همی بهره کار را</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بگیرد همه بحر های جهان</p></div>
<div class="m2"><p>ز عمان ز احمر چه اعرابیان</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>گذد کن هم از ترعه رود نیل</p></div>
<div class="m2"><p>همان آب کاوهست بررنگ نیل</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>سپردم بتو لشکر بیکران</p></div>
<div class="m2"><p>روم من سوی پارس دریا کران</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>دگر لشکران با سپهدار شاه</p></div>
<div class="m2"><p>ز دریا بدریا گزیدند راه</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>بایران خبر شد که آمد سپاه</p></div>
<div class="m2"><p>شهنشاه با فتح و نصرت ز راه</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>سپهبد بفرمود ایرانیان</p></div>
<div class="m2"><p>شهنشاه با لشکر جاودان</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>بیایند با فتح و با فرهی</p></div>
<div class="m2"><p>پذیره نمائید شاهنشهی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ببندید صد طاق نصرت بپارس</p></div>
<div class="m2"><p>که آمد شهنشاه ما بی هراس</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>همه شهرها را چراغان کنید</p></div>
<div class="m2"><p>همه مردمان شاد و خندان کنید</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>بهرجا نوازید ساز و سرود</p></div>
<div class="m2"><p>بگویند برشاه ایران درود</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>سحر گه سر از خواب برداشتند</p></div>
<div class="m2"><p>خیال پذیره شدن داشتند</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>سپهبد ابا افسران سپاه</p></div>
<div class="m2"><p>برای پذیره گزیدند راه</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>گذر برگذر طاق نصرت بپا</p></div>
<div class="m2"><p>نموده همان نیزه داران شاه</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>خیابان به بستند از نیزه دار</p></div>
<div class="m2"><p>کزان بگذرد شاه با اقتدار</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>همه پرچم سبز و سرخ و بنفش</p></div>
<div class="m2"><p>بپاهایشان بود زرینه کفش</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>سپهبد شهنشاه را از دور دید</p></div>
<div class="m2"><p>ز اسب اندر آمد بسویش دوید</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بنزد شهنشه زمین بوسه داد</p></div>
<div class="m2"><p>بگفتا شهنشاه ما زنده باد</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>همه افسران و سران سپاه</p></div>
<div class="m2"><p>زمین بوسه دادند نزدیک شاه</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>صدای تبیره بشد بر فلک</p></div>
<div class="m2"><p>که از جنگ فاتح بیامد ملک</p></div></div>