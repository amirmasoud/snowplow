---
title: >-
    بخش ۲۷ - سر کوبی سکاها
---
# بخش ۲۷ - سر کوبی سکاها

<div class="b" id="bn1"><div class="m1"><p>بسوی سکاها بشد لشکرش</p></div>
<div class="m2"><p>کزآن قوم قدری گران بدسرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باطراف قفقار و بحر سیاه</p></div>
<div class="m2"><p>که مسکن همی داشتند آن سپاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دیدند خود شاه آید بجنگ</p></div>
<div class="m2"><p>زمانی نکردند پیشش درنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه پشت برشاه وروشان بکوه</p></div>
<div class="m2"><p>پناهده برغار هر یک گروه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهنشه برایشان نیاورد خشم</p></div>
<div class="m2"><p>بفرمود ز ایشان بپوشید چشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که ایشان پراکنده در کوه و سنگ</p></div>
<div class="m2"><p>نه شهرو نه خانه نه جای درنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آنجا بایران نهادند روی</p></div>
<div class="m2"><p>ابا فتح و فیروزی و رنگ و بوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو یک چند روزی فراغت نمود</p></div>
<div class="m2"><p>بگفتا نزیبد که بیکار بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بفرمود باید که هشت ده هزار</p></div>
<div class="m2"><p>سوار دلیر پس کار زار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مقاپیش باشد سپهدارشان</p></div>
<div class="m2"><p>به نیکی گراید بهر کارشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترا کیه باید مسخر کنند</p></div>
<div class="m2"><p>بمقدونیه پس سپه برکشند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به فرمان آن شاه والاگهر</p></div>
<div class="m2"><p>سپه را بدیدند سان سر بسر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو گوئی که دریا بموج آمده است</p></div>
<div class="m2"><p>که لشکر همی فوج فوج آمده است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همان پرچم پارس در پیش رو</p></div>
<div class="m2"><p>چنان میکشیدند باهای و هو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زبس بوق و هم کوس و هم کرنا</p></div>
<div class="m2"><p>فلک خود همی کرده گم دست و پا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شهنشه سپهدار را پیش خواند</p></div>
<div class="m2"><p>سخن های شایسته با او براند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بفرمود ای افسر نامدار</p></div>
<div class="m2"><p>بسوی تراکیه شو رهسپار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیر تو بهمراه هشت ده هزار</p></div>
<div class="m2"><p>براه تراکیه با اقتدار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز زر و ز سیم و کلاه و کمر</p></div>
<div class="m2"><p>ز تیغ وزکوپال و گرز و سپر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم آذوقه و هم ذخیره ببر</p></div>
<div class="m2"><p>علوفه بر اسبان همه سر بسر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرسنه نباید شود لشگرت</p></div>
<div class="m2"><p>نباید بسختی روند از برت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همی مهربان باش تو با سپاه</p></div>
<div class="m2"><p>نیارند از تو بدشمن پناه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو خود پیش رو باش و ننمای باک</p></div>
<div class="m2"><p>دل خویش برنه به یزدان پاک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر چیره گشتی بهر کشوری</p></div>
<div class="m2"><p>بهر نامداری و هر افسری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>میازار زنها و اطفال را</p></div>
<div class="m2"><p>همان پیر مردان بیحال را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ببخشای بر جان زار و فقیر</p></div>
<div class="m2"><p>مرنجان دل زیر دست و اسیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مکش شاه و آور تو در نزد من</p></div>
<div class="m2"><p>بگویم باو من به نیکی سخن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عدالت کن و داد را پیشه کن</p></div>
<div class="m2"><p>همی از خداوند اندیشه کن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو مردونیه همره خود ببر</p></div>
<div class="m2"><p>جوان دلیر است و هم با هنر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نما افسر اورا تو بر ده هزار</p></div>
<div class="m2"><p>چو او نام جویست در کارزار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مقا پیش گفتا که فرمان برم</p></div>
<div class="m2"><p>تو شاهنشهی من یکی کهترم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زمین بوسه کرد و روان شد براه</p></div>
<div class="m2"><p>سوی منزل خویش شد شامگاه</p></div></div>