---
title: >-
    بخش ۱۴ - سر کوبی سکاها
---
# بخش ۱۴ - سر کوبی سکاها

<div class="b" id="bn1"><div class="m1"><p>دگر روز آمد سواری بماد</p></div>
<div class="m2"><p>بگفتا که ای شاه با عدل و داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سکاهای بی خانمان نژند</p></div>
<div class="m2"><p>رسانند بر اهل ایران گزند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرازیر گشته چو سیل روان</p></div>
<div class="m2"><p>نموده زن و مرد بی خانمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برس تو بفریاد ما بیکسان</p></div>
<div class="m2"><p>که آسوده گردیم از این خسان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهنشه بر آشفت از این خبر</p></div>
<div class="m2"><p>بگفت از سکاها نمانم اثر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرا پاک را این چنین گفت شاه</p></div>
<div class="m2"><p>که خواهم نمایم سکاها تباه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سکاها که هستند چون وحشیان</p></div>
<div class="m2"><p>بتنبیه ایشان به بندم میان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در گنج بگشا بده سیم و زر</p></div>
<div class="m2"><p>ز شمشیر و کوپال و گرز و کمر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو روز دگر کوچ ده سوی شرق</p></div>
<div class="m2"><p>یکی لشکری همچو طوفان و برق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرا پاک گفتا که فرمان تر است</p></div>
<div class="m2"><p>بکوشم بسازم همه کار رات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرا پاک شد افسرانرا بخواست</p></div>
<div class="m2"><p>بفرمود لشکر ببایست خواست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سوی شرق ایران گذاریم روی</p></div>
<div class="m2"><p>که شاه جوان است دیهیم جو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفتا شها لشکر آماده گشت</p></div>
<div class="m2"><p>ابر دشت خیمه پرا گنده گشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بفرمود من خود بیایم براه</p></div>
<div class="m2"><p>نباید که پی شاه باشد سپاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرا پرده شاه بیرون زدند</p></div>
<div class="m2"><p>سواران محصوص خیمه زدند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بخدمت غلامان زرین کمر</p></div>
<div class="m2"><p>قباهای زر تار کرده ببر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هزاران جلو دار زرین کلاه</p></div>
<div class="m2"><p>به پیش اوفتادند یکسر براه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هزار اسب تازی جنیبت کشان</p></div>
<div class="m2"><p>همه با لگام جواهر نشان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان پرچم پارس در پیش شاه</p></div>
<div class="m2"><p>همی میکشیدند با دستگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی چتر زرین گرفته بسر</p></div>
<div class="m2"><p>ببازو جواهر بگردن گهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپاه عازم جنگ با خاوران</p></div>
<div class="m2"><p>به پیش سپه شاه روشن روان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خبر چون بیامد سوی دشمنان</p></div>
<div class="m2"><p>آمد سپاهی چنان بیکران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپاه سکاها و هم آریان</p></div>
<div class="m2"><p>همه جنگ را تنگ بسته میان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز هرسوی بر پای شد جنگ سخت</p></div>
<div class="m2"><p>سکاها بدیدنده برگشته بخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سران شان به نزدیک شاه آمدند</p></div>
<div class="m2"><p>خمیده کمر عذر خواه آمدند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سکاها و آن شهرها سر بسر</p></div>
<div class="m2"><p>همه شد مطیع شه دادگر</p></div></div>