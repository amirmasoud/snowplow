---
title: >-
    بخش ۲۶ - کندن ترعه بفرمان داریوش
---
# بخش ۲۶ - کندن ترعه بفرمان داریوش

<div class="b" id="bn1"><div class="m1"><p>چو شد جمله ی کار آراسته</p></div>
<div class="m2"><p>بهشتی بشد مصر پر خواسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس آنگه سران سپه را بخواست</p></div>
<div class="m2"><p>چه صنعتگران و چه مزدور خواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دریای سرخ و ز دریای روم</p></div>
<div class="m2"><p>یکی ترعه سازند زین مرز و بوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر خاک را برکنی از میان</p></div>
<div class="m2"><p>ز دریا بدریا گذر میتوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو بحر بزرگی که ازهم جداست</p></div>
<div class="m2"><p>بهم راه جویند بی کم و کاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهندس بسی آمد از هر کنار</p></div>
<div class="m2"><p>بامر شهنشه ز ایران بکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکندند ترعه به تدبیر و زور</p></div>
<div class="m2"><p>ز دریا بدریا ببرد آب شور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دریای رومش که مسدود بود</p></div>
<div class="m2"><p>رهی تا بدریای سرخش گشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وزان پس بفرمود برجی بلند</p></div>
<div class="m2"><p>بنزدیک آنجا بسازند چند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نویسند بالای آن برجها</p></div>
<div class="m2"><p>ز شه یادگاری بماند بجا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که کندم من این ترعه بین دو بحر</p></div>
<div class="m2"><p>و ازینرویکی کردم این هردو نهر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منم داریوشی که از هور مزد</p></div>
<div class="m2"><p>رسیده بمن زور و تدبیر و مزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خداوند پروردگار من است</p></div>
<div class="m2"><p>همیشه سعادت کنار من است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عطا کرد بر من خدای بزرگ</p></div>
<div class="m2"><p>بسی زور و تدبیر و عزم سترگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که هستم شهی فاتح و دادگر</p></div>
<div class="m2"><p>بفرمان او بسته&shy;ام من کمر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بمن داد تدبیر و گفتار نیک</p></div>
<div class="m2"><p>تن سالم و کارو پندار نیک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در این مللک پهناور باستان</p></div>
<div class="m2"><p>شهنشاهی من شود داستان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>منم داریوشی که از مهر هور</p></div>
<div class="m2"><p>کنم چشم اهریمن از نور کور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بفرمان من گشت کشتی روان</p></div>
<div class="m2"><p>ز دریا بدریا نه رنجی میان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرفتم همه مصر و هم روم و هند</p></div>
<div class="m2"><p>سکاها و پنجاب تا رود سند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>و از آن پس گرفتیم مقد و نیا</p></div>
<div class="m2"><p>دگر مانیکا آتن و میلیا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نوشتم بماند زمن یادگار</p></div>
<div class="m2"><p>بدانند ز ایزد رسد اقتدار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو برنج شهنشاه بر پای شد</p></div>
<div class="m2"><p>همه مصر جشنی سرا پای شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپهدار لشکر همی خواست شاه</p></div>
<div class="m2"><p>بفرمود آماده گردان سپاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که در مصر باید که چندین هزار</p></div>
<div class="m2"><p>دلیران و نام آور کار زار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بمانند هر فوج با افسری</p></div>
<div class="m2"><p>که دشمن نسازد دگر خودسری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو این کرده شد ساز و برگ سفر</p></div>
<div class="m2"><p>تهیه نمودند و شد رهسپر</p></div></div>