---
title: >-
    بخش ۱۳ - فتح ماد
---
# بخش ۱۳ - فتح ماد

<div class="b" id="bn1"><div class="m1"><p>از آن پس سران سپه را بخواست</p></div>
<div class="m2"><p>بگفتا که لشکر بسازید راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گنج بگشاد کورش چنان</p></div>
<div class="m2"><p>بروی سوران، دلیران، یلان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زرو ز سیم و کلاه و کمر</p></div>
<div class="m2"><p>ز شمشیر و پولاد و گرز و سپر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زتیر و ز ترکش ز تیغ و تبر</p></div>
<div class="m2"><p>ز زوبین واز نیزه تیز سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم از پرچم و نای وهم بوق و کوس</p></div>
<div class="m2"><p>بکی لشکری ساخت هم چون عروس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بفرمود کامشب به بانگ خروس</p></div>
<div class="m2"><p>زهر سو بکوبید بر طبل و کوس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو آوای نای و تبیره بکوش</p></div>
<div class="m2"><p>بیامد سپه جملگی در خروش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهادند رو چون سوی شهر ماد</p></div>
<div class="m2"><p>بازید هاک این خبر کس بداد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که آمد سپاهی برون از حساب</p></div>
<div class="m2"><p>دریغا که ماداست اینک بخواب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر آن سپه کورش نوجوان</p></div>
<div class="m2"><p>چو بختش جوان بو ورأیش جوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو بشنید آژید هاک این خبر</p></div>
<div class="m2"><p>بگفتا که آمد زمانم بسر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتا هرا پاک آید برم</p></div>
<div class="m2"><p>به بیند چه آورده او بر سرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرا پاک آمد زمین بوسه داد</p></div>
<div class="m2"><p>بگفتا که شاها دلت شاد باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرا رنجه گشتی تو از این خبر</p></div>
<div class="m2"><p>همت لشکر و هم کلاه و کمر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسازم سپاه و پذیره شوم</p></div>
<div class="m2"><p>یقین است در جنگ چیره شوم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جوابش چنین گفت آژید هاک</p></div>
<div class="m2"><p>ببر لشکر از جنگ منمای باک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در گنج بگشا ز زر و گهر</p></div>
<div class="m2"><p>هر آنچه که بایست همره به بر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرا پاک گفتا که فرمان برم</p></div>
<div class="m2"><p>هر آن امر کردی بجا آورم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه بر گشت فرزانه از پیش شاه</p></div>
<div class="m2"><p>رژه بر کشید و روان شد براه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو نزدیک کورش رسید آن سپاه</p></div>
<div class="m2"><p>هرا پاک گفت ای دلیران شاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یک امشب در این دشت راحت کنید</p></div>
<div class="m2"><p>بچا در همه استراحت کیند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سپس کفت ای نو گلان وطن</p></div>
<div class="m2"><p>من اینک شما را بگویم سخن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شنیدم من از مؤبد مؤبدان</p></div>
<div class="m2"><p>ز اختر شناسان و هم بخردان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که کورش شود شاه بر این جهان</p></div>
<div class="m2"><p>شود سرور سروران و مهان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جوانی است با دانش و با هنر</p></div>
<div class="m2"><p>سزد گر ببندیم پیشش کمر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که آژیدهاک است بی رحم و باک</p></div>
<div class="m2"><p>نموده همه مادیک مشت خاک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چرا بهر او جانفشانی کنیم</p></div>
<div class="m2"><p>فدا کاری و جان ستانی کنیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کشد بی سبب او جوانان ما</p></div>
<div class="m2"><p>بآتش بسوزد دل و جان ما</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگفتند رأی تو را کمتریم</p></div>
<div class="m2"><p>ر رأی و ر فرمان تو نگذریم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه دیده ها پر نم و سینه چاک</p></div>
<div class="m2"><p>بگوییم تفرین آژید هاک</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همه یک دل و رأی فرمان بریم</p></div>
<div class="m2"><p>ز فرمان تو لحظه ای نگذریم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو شب شد هرا پاک خود بی سپاه</p></div>
<div class="m2"><p>پیاده بیامد به درگاه شاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه بر سرش خود و نه بر تن زره</p></div>
<div class="m2"><p>نه آلات جنگ و نه بند و گره</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیامد بکورش سلامی بداد</p></div>
<div class="m2"><p>بگفتا شها ملکت آباد باد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو فرمان دهی ماسپاه توایم</p></div>
<div class="m2"><p>ههه یکسره در پناه توایم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو بشنید کورش دلش گشت شاد</p></div>
<div class="m2"><p>سران را همه بهترین جای داد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بگفتا هرا پاک جانم ر تست</p></div>
<div class="m2"><p>همان جسم و روح و روانم ز تست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هرا پاک گفتا که ما کهتریم</p></div>
<div class="m2"><p>ز رای و ز فرمان تو نگذریم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه لشکر ماد تسلیم شد</p></div>
<div class="m2"><p>شه ماد بی چاره در بیم شد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو بشنید آژید هاک انی خبر</p></div>
<div class="m2"><p>جهان شد بچشمش جهانی دگر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بفرمود لشکر بیاراستند</p></div>
<div class="m2"><p>سلیح و سنان و سپر خواستند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز کاخ شهی رخت بیرون کشید</p></div>
<div class="m2"><p>سرا پرده را سوی هامون کشید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همان پرچم ماد بالای سر</p></div>
<div class="m2"><p>بکوبید و آمد بجنگ پسر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بکورش بگفتند آمد سپاه</p></div>
<div class="m2"><p>زمین شد ز گرد سواران سیاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بفرمود لشکر به هامون کشند</p></div>
<div class="m2"><p>همه لشکر ماد در خون کشند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بزد اسب و بیرون شد از گرد و خاک</p></div>
<div class="m2"><p>بگفتا که ای شاه آژید هاک</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مرا باتو جنگ است نی با سپاه</p></div>
<div class="m2"><p>نه باید که کشته شود بی گناه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به لشکر بفرمود آژید هاک</p></div>
<div class="m2"><p>به پیکان و پر افکنیدش بخاک</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بزد کوس و لشکر برآمد زدشت</p></div>
<div class="m2"><p>تو گفتی جهان سر بسر تیره گشت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز خاک و ز خونی که آمد بجوش</p></div>
<div class="m2"><p>هوا قیرگون شد زمین سرخ پوش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وزانسو هرا پاک چون حمله برد</p></div>
<div class="m2"><p>همه لشکر ماد را کرد خرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو کورش نیارا، میان سپاه</p></div>
<div class="m2"><p>بدیدش سیه کشته از گرد راه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بزد اسب و آمد بنزدیک او</p></div>
<div class="m2"><p>که تا در برد جان تاریک او</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بینداخت آن خسروانی کمند</p></div>
<div class="m2"><p>سرشاه ماد اندر آمد به بند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کشیدش ز اسب و بزد بر زمین</p></div>
<div class="m2"><p>بگفتا که ای شاه با آفرین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فکندی یک کودکی را بکوه</p></div>
<div class="m2"><p>که شاید زدستش نگردی ستوه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نکردی تو فکر جهان آفرین</p></div>
<div class="m2"><p>که او زنده دارد مرا در زمین</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو مادی شه خویش زا دستگیر</p></div>
<div class="m2"><p>بدیدند گشتند از جنگ سیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همه افسرانشان پیاده دوان</p></div>
<div class="m2"><p>بپابوس کورش شه نوجوان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بخاک اوفتاده امان خواستند</p></div>
<div class="m2"><p>گذشت شه نوجوان خواستند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بفرمود کورش شه دادگر</p></div>
<div class="m2"><p>مرا با شما جنگ نبود دگر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>منم با شما مهربان چون پدر</p></div>
<div class="m2"><p>بصلح و بجنگ و براه و سفر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شما یکسره در پناه منید</p></div>
<div class="m2"><p>شما افسران سپاه منید</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دگر گفت او، ای نیای سترگ</p></div>
<div class="m2"><p>تو پیری جهاندیده ای و بزرگ</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کنون باش مهمان بر دخترت</p></div>
<div class="m2"><p>منم همچو فرزند در کشورت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نیا را روانه سوی پارس کرد</p></div>
<div class="m2"><p>که آساید از رنج راه و نبرد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هرا پاک آمد سوی شهریار</p></div>
<div class="m2"><p>بگفتا که ای خسرو کامگار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>اجازت بده من روم سوی ماد</p></div>
<div class="m2"><p>دهم مژده از شاه با عدل و داد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بفرمود کورش برو شاد باش</p></div>
<div class="m2"><p>ز رنج و ز غم دیگر آزاد باش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هرا پاک با لشگر خویشتن</p></div>
<div class="m2"><p>نهادند سر جمله سوی وطن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بفرمود آیین به بندند شهر</p></div>
<div class="m2"><p>هم از شادمانی بجویند بهر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خود ولشکر و افسران سپاه</p></div>
<div class="m2"><p>بکورش پذیره شدندی براه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بفرمود از شهر تا پهن دشت</p></div>
<div class="m2"><p>ز نیزه یکی طاق سازند و هشت</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>سر راه او شمع افروختد</p></div>
<div class="m2"><p>بدشت عود و عنبر همی سوختند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>همه افسران با کله خود و پر</p></div>
<div class="m2"><p>سواره زره پوش و زرین کمر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بفر و شکوهش پذیره شدند</p></div>
<div class="m2"><p>ابابوق و کوس و تبیره شدند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو آمد به شهر آن شه دادگر</p></div>
<div class="m2"><p>نمودند شادی همه سر بسر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بیامد به تخت شهی بر نشست</p></div>
<div class="m2"><p>بفرمود کی مردم حق پرست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>امیدم چنان است از کردگار</p></div>
<div class="m2"><p>که باشم شهی عادل و رستگار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>همه سر نهادند روی زمین</p></div>
<div class="m2"><p>بشادی بر او خواندند آفرین</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>پس آنگه بفرمود هر پاک را</p></div>
<div class="m2"><p>که آرد شبان گهر پاک را</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بفرمود کورش به مرد شبان</p></div>
<div class="m2"><p>که بودی تو بامن بسی مهربان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نکردم فراموش رنج شما</p></div>
<div class="m2"><p>دهم در عوض پاس گنج شما</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بفرمود کورش بیارند زر</p></div>
<div class="m2"><p>شبان را بریزند زر تا کمر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بفرمود دیگر شبانی مکن</p></div>
<div class="m2"><p>دگر گله را پاسبانی مکن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>برو شاد خوش باش با دایه ام</p></div>
<div class="m2"><p>همیشه بمانید در سایه ام</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بد و قریه ای داد آباد و پاک</p></div>
<div class="m2"><p>پر از چشمه آب پر باغ و تاک</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بفرمود هر مان یک بدره زر</p></div>
<div class="m2"><p>بیا توز گنجور بستان دگر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>شبان بادلی خوش زمین بوسه داد</p></div>
<div class="m2"><p>دعا گوی شه گشت هر بامداد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چو شد کارها جمله آراسته</p></div>
<div class="m2"><p>همه مملکت گشت پیراسته</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>شهنشاه کوروش به یزدان پاک</p></div>
<div class="m2"><p>چنین گفت : کای داور آب و خاک</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>خدایا منم کمترین بنده ات</p></div>
<div class="m2"><p>یکی طفل زار و پرستنده ات</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ز بند بلایم تو کردی رها</p></div>
<div class="m2"><p>رهانیدیم از دم اژدها</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>شبانرا تو گفتی که پروردیم</p></div>
<div class="m2"><p>زنش شیر از مهر جان دادیم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>تو فرمان بدادی به آژید هاک</p></div>
<div class="m2"><p>که از کینه من دلش گشت پاک</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>همه از تو دارم نه از این و آن</p></div>
<div class="m2"><p>کنم شکر صدها هزاران چنان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>خدایا تو بپذیر سوگند من</p></div>
<div class="m2"><p>کنم مهربانی باهل زمن</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>زمین بوسه داد و بنالید سخت</p></div>
<div class="m2"><p>خدایا ز تو یافتم تاج و تخت</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>خدایا امیدم بدرگاه توست</p></div>
<div class="m2"><p>که باشم شهی عادل و تن درست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>زمن دور کن کید اهریمنان</p></div>
<div class="m2"><p>نگردم ر کس بد دل و بد گمان</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>پس آنگه بیامد سر تخت عاج</p></div>
<div class="m2"><p>بسر برنهاد آن برازنده تاج</p></div></div>