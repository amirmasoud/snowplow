---
title: >-
    شمارهٔ  ۱۰
---
# شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>زندگی خوب است اما گوشهٔ گلزارکی</p></div>
<div class="m2"><p>طبخکی دلدارکی در ضمن خدمتگارکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌کنم اظهارکی من با شما هموارکی</p></div>
<div class="m2"><p>در جهان ای یارکان البتّه باید یارکی</p></div></div>
<div class="b2" id="bn3"><p>نازک و خوبک لطف دلبرک دلدارکی</p></div>
<div class="b" id="bn4"><div class="m1"><p>گل‌رخک گرم‌اختلاطک جانیک مه‌پیکرک</p></div>
<div class="m2"><p>جاهلک لیلی‌وشک خوش‌نغمگک لب‌شکّرک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش‌ملنگک محرمک زنّارزلفک کافرک</p></div>
<div class="m2"><p>شاهدک شنگ و ملیحک چابکک سیمین‌برک</p></div></div>
<div class="b2" id="bn6"><p>گل‌لبک غنچه‌دهانک سادگک خوش‌باورک</p></div>
<div class="b" id="bn7"><div class="m1"><p>لاابالیئک حریفک سبزگک هم‌صحبتک</p></div>
<div class="m2"><p>شوخ‌چشمک کم‌رقیبک پرفنک بافرصتک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درددانک جلوه‌دارک کاردانک آفتک</p></div>
<div class="m2"><p>عارفک رندک ندیمک ماهرویک لعبتک</p></div></div>
<div class="b2" id="bn9"><p>ترککی شوخک ظریفک دلبرک دلدارکی</p></div>
<div class="b" id="bn10"><div class="m1"><p>گل‌لبک غنچه‌دهانک سادگک خوش‌باورک</p></div>
<div class="m2"><p>خوش‌خرامک درفشانک خوش‌زبانک ماهرک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چاره‌سازک جان‌گدازک پاک‌بازک ساحرک</p></div>
<div class="m2"><p>ماهرویک محرمک خوش‌اوّلک خوش‌آخرک</p></div></div>
<div class="b2" id="bn12"><p>عنبرین‌‌خالک مهک مشکین‌خطک گلزارکی</p></div>
<div class="b" id="bn13"><div class="m1"><p>دست‌وپا‌دارک محیلک غمزه‌دانک قابلک</p></div>
<div class="m2"><p>نردبازک خوش‌قمارک بردبارک عاقلک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل‌نشینک مه‌جبینک بی‌قرینک جاهلک</p></div>
<div class="m2"><p>ساقیک شکّرلبک پسته‌دهانک خوشگلک</p></div></div>
<div class="b2" id="bn15"><p>مطربک جانک لطیفک تیزفهمک یارکی</p></div>
<div class="b" id="bn16"><div class="m1"><p>سرگرانک هم‌زبانک خوش‌بیانک ماهرک</p></div>
<div class="m2"><p>شعرفهمک شعردانک شعرخوانک شاعرک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باب قصاب لذیذک چرب‌و‌نرمک پیکرک</p></div>
<div class="m2"><p>دنبه‌دارک فربهک سرخ و سفیدک نادرک</p></div></div>
<div class="b2" id="bn18"><p>نازنینک خوش‌ادائک مهوشک هشیارکی</p></div>