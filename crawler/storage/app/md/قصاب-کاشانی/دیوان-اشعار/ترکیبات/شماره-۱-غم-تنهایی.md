---
title: >-
    شمارهٔ  ۱ - غم تنهایی
---
# شمارهٔ  ۱ - غم تنهایی

<div class="b" id="bn1"><div class="m1"><p>ای بر قد و بالایت از خوبی و رعنایی</p></div>
<div class="m2"><p>گردیده نگه حیران در چشم تماشایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازآ که کشید آخر عشق تو به رسوایی</p></div>
<div class="m2"><p>ای پادشه خوبان داد از غم تنهایی</p></div></div>
<div class="b2" id="bn3"><p>دل بی تو به جان آمد وقت است که بازآیی</p></div>
<div class="b" id="bn4"><div class="m1"><p>اول گل رخسارت سرگرم فغانم کرد</p></div>
<div class="m2"><p>وآنگه خم ابرویت قصد دل و جانم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق آمد و در آخر رسوای جهانم کرد</p></div>
<div class="m2"><p>مشتاقی و مهجوری دور از تو چنانم کرد</p></div></div>
<div class="b2" id="bn6"><p>کز دست بخواهد شد دامان شکیبایی</p></div>
<div class="b" id="bn7"><div class="m1"><p>ای ذکر توام مسطر در دفتر ناکامی</p></div>
<div class="m2"><p>ای نام توام رهبر در شش‌در ناکامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از زخم توام مرهم در پیکر ناکامی</p></div>
<div class="m2"><p>ای درد توام درمان در بستر ناکامی</p></div></div>
<div class="b2" id="bn9"><p>وی یاد توام مونس در گوشه تنهایی</p></div>
<div class="b" id="bn10"><div class="m1"><p>عمری است در این وادی سرگشته دلداریم</p></div>
<div class="m2"><p>هرکس طلبی دارد ما طالب دیداریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دست غم هجران دیری است گرفتاریم</p></div>
<div class="m2"><p>در دایره قسمت ما نقطه پرگاریم</p></div></div>
<div class="b2" id="bn12"><p>لطف آنچه تو اندیشی حکم آنچه تو فرمایی</p></div>
<div class="b" id="bn13"><div class="m1"><p>روزی به خیال او دل، شاد همی‌کردم</p></div>
<div class="m2"><p>در گوشه تنهایی فریاد همی‌کردم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر کشتن خویش از غم امداد همی‌کردم</p></div>
<div class="m2"><p>دیشب گله زلفش با باد همی‌کردم</p></div></div>
<div class="b2" id="bn15"><p>گفتا غلطی بگذر زین فکرت سودایی</p></div>
<div class="b" id="bn16"><div class="m1"><p>جز تیر توام در دل پروای خدنگی نیست</p></div>
<div class="m2"><p>جز کوی توام بر سر سودای فرنگی نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر سینه پرداغم جز عشق تو رنگی نیست</p></div>
<div class="m2"><p>ساقی چمن گل را بی روی تو رنگی نیست</p></div></div>
<div class="b2" id="bn18"><p>شمشاد خرامان کن تا باغ بیارایی</p></div>
<div class="b" id="bn19"><div class="m1"><p>در دل ز هجوم اشگ خوناب نمی‌ماند</p></div>
<div class="m2"><p>فردا است که در چشمم سیلاب نمی‌ماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غارت‌زده دل را اسباب نمی‌ماند</p></div>
<div class="m2"><p>دائم گل این بستان شاداب نمی‌ماند</p></div></div>
<div class="b2" id="bn21"><p>دریاب ضعیفان را در وقت توانایی</p></div>
<div class="b" id="bn22"><div class="m1"><p>دیشب خبر وصلی از پیش نگار آمد</p></div>
<div class="m2"><p>بگذشت خزان هجر ایام بهار آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قصاب گل عشقت می خور که به‌بار آمد</p></div>
<div class="m2"><p>حافظ به شب هجران بوی خوش یار آمد</p></div></div>
<div class="b2" id="bn24"><p>شادیت مبارک باد ای عاشق شیدایی</p></div>