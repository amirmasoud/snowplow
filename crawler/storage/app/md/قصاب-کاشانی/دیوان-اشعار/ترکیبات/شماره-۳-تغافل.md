---
title: >-
    شمارهٔ  ۳ - تغافل
---
# شمارهٔ  ۳ - تغافل

<div class="b" id="bn1"><div class="m1"><p>بت من به حسن و خوبی به خدا که تا نداری</p></div>
<div class="m2"><p>به دلی نظر نکردم که در او تو جا نداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تو چون کنم که یک جو غم بینوا نداری</p></div>
<div class="m2"><p>به چه دل دهم تسلی که سری به ما نداری</p></div></div>
<div class="b2" id="bn3"><p>به تو با چه رو بگویم که چرا وفا نداری</p></div>
<div class="b" id="bn4"><div class="m1"><p>سر فتنه چون گشاید ز پس نقاب چشمت</p></div>
<div class="m2"><p>به یکی کرشمه سازد دو جهان خراب چشمت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو به قصد عاشق آید به سر عتاب چشمت</p></div>
<div class="m2"><p>چو شوم ز دور پیدا زره حجاب چشمت</p></div></div>
<div class="b2" id="bn6"><p>زده بر در تغافل تو مگر حیا نداری</p></div>
<div class="b" id="bn7"><div class="m1"><p>گهیم به خون نشاندی ز ره ستیزه رنگی</p></div>
<div class="m2"><p>گهیم هلاک کردی ز مصیبت دورنگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ننموده رخ ربودی دل ما به تیزچنگی</p></div>
<div class="m2"><p>به من شکسته‌خاطر چه نکردی ای فرنگی</p></div></div>
<div class="b2" id="bn9"><p>تو به ما بگو که شرمی مگر از خدا نداری</p></div>
<div class="b" id="bn10"><div class="m1"><p>همه پای و سر زبانی ز حکایت نهانی</p></div>
<div class="m2"><p>ز کلام درفشانی و ز ناز سرگرانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به روش سبک عنانی و به رمز نکته‌دانی</p></div>
<div class="m2"><p>ز نگاه جان‌ستانی به طریق مهربانی</p></div></div>
<div class="b2" id="bn12"><p>همه چیز داری اما نظری به ما نداری</p></div>
<div class="b" id="bn13"><div class="m1"><p>ز لب و بیاض گردن همه شیشه و پیاله</p></div>
<div class="m2"><p>زد و چشم و سیب غبغب مزه با می دوساله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز خط عرق‌فشانت به بنفشه ریخت ژاله</p></div>
<div class="m2"><p>به چمن سرای عشق از گل سرخ تا به لاله</p></div></div>
<div class="b2" id="bn15"><p>همه رنگ داری اما گل مدّعا نداری</p></div>
<div class="b" id="bn16"><div class="m1"><p>به میان خوب‌رویان چو تو کج‌کلاه حسنی</p></div>
<div class="m2"><p>زده صف ز خیل مژگان سروسر سپاه حسنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بنمای رخ به عاشق که در اوج ماه حسنی</p></div>
<div class="m2"><p>بشنو هر آنچه گفتم چو تو پادشاه حسنی</p></div></div>
<div class="b2" id="bn18"><p>نظری چرا ز احسان به من گدا نداری</p></div>
<div class="b" id="bn19"><div class="m1"><p>صنما دو تیغ ابرو ز چه آب داده بودی</p></div>
<div class="m2"><p>دگر از برای قتلم چه خطاب داده بودی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سخن مرا جوابش شکراب داده بودی</p></div>
<div class="m2"><p>نشنیده حرف قصاب و جواب داده بودی</p></div></div>
<div class="b2" id="bn21"><p>تو که مدتی است اصلا خبری ز ما نداری</p></div>