---
title: >-
    شمارهٔ  ۷ - دامان رقیب
---
# شمارهٔ  ۷ - دامان رقیب

<div class="b" id="bn1"><div class="m1"><p>باز چون سرو قد افراخته‌ای یعنی چه</p></div>
<div class="m2"><p>ریشه بر هر جگر انداخته‌ای یعنی چه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهره چون لاله ز می ساخته‌ای یعنی چه</p></div>
<div class="m2"><p>ماه من پرده برانداخته‌ای یعنی چه</p></div></div>
<div class="b2" id="bn3"><p>مست از خانه برون تاخته‌ای یعنی چه</p></div>
<div class="b" id="bn4"><div class="m1"><p>دلت از راه برون رفته ز افسان رقیب</p></div>
<div class="m2"><p>زده‌ای دست ندانسته به دامان رقیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدی از رغم من غم‌زده مهمان رقیب</p></div>
<div class="m2"><p>زلف در دست صبا گوش به فرمان رقیب</p></div></div>
<div class="b2" id="bn6"><p>این‌چنین با همه درساخته‌ای یعنی چه</p></div>
<div class="b" id="bn7"><div class="m1"><p>همنشین تا تو بدین پاره قبایان شده‌ای</p></div>
<div class="m2"><p>چون مه نو سر انگشت نمایان شده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نی غلط پیرو این بی‌سر‌وپایان شده‌ای</p></div>
<div class="m2"><p>شاه خوبانی و منظور گدایان شده‌ای</p></div></div>
<div class="b2" id="bn9"><p>قدر این طایفه نشناخته‌ای یعنی چه</p></div>
<div class="b" id="bn10"><div class="m1"><p>گاه پیغامی از آن نرگس مستم دادی</p></div>
<div class="m2"><p>گاه بر خاک ره خویش نشستم دادی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این‌قدر هست گناهم که شکستم دادی</p></div>
<div class="m2"><p>نه سر زلف خود اول تو به دستم دادی</p></div></div>
<div class="b2" id="bn12"><p>بازم از پای درانداخته‌ای یعنی چه</p></div>
<div class="b" id="bn13"><div class="m1"><p>گفت ابروی تو با دل سخنی چند نهان</p></div>
<div class="m2"><p>جلوه‌ات آمد و کرد آن سخنان جمله عیان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لبت از موج تبسم نمکی ریخت در آن</p></div>
<div class="m2"><p>سخنت رمز دهان گفت و کمر سرّ میان</p></div></div>
<div class="b2" id="bn15"><p>زین میان تیغ به ما آخته‌ای یعنی چه</p></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه از خون جگر شستن دل کرد قبول</p></div>
<div class="m2"><p>می‌توان گفت که شد در ره عشقت مقبول</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ما نکردیم به جز درد و غمت هیچ وصول</p></div>
<div class="m2"><p>هرکس از مهره مهر تو به نقشی مشغول</p></div></div>
<div class="b2" id="bn18"><p>عاقبت با همه کج باخته‌ای یعنی چه</p></div>
<div class="b" id="bn19"><div class="m1"><p>دوش آمد به صدا آن لب شیرین‌گفتار</p></div>
<div class="m2"><p>بارها کرد به قصاب همین را تکرار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که در این خانه ره غیر بود یا اغیار</p></div>
<div class="m2"><p>حافظا در دل تنگت چو فرود آید یار</p></div></div>
<div class="b2" id="bn21"><p>خانه از غیر نپرداخته‌ای یعنی چه</p></div>