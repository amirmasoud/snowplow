---
title: >-
    شمارهٔ  ۳۰۱
---
# شمارهٔ  ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>ای مهر دل‌فروز گل روی کیستی</p></div>
<div class="m2"><p>وی ماه نو نمونه ابروی کیستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رم می‌کنی ز سایه مژگان خویشتن</p></div>
<div class="m2"><p>ای از حرم برآمده آهوی کیستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عطر عبیر گرد جهان را گرفته است</p></div>
<div class="m2"><p>باز ای صبا روان ز سر کوی کیستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا صبح همچو شمع ز حیرت گداختم</p></div>
<div class="m2"><p>ای شام تیره حلقه‌ای از موی کیستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خود خبر نباشدم ای دل ز من مپرس</p></div>
<div class="m2"><p>مجروح تیر غمزه ابروی کیستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر قصاص پرورشت می‌دهد شبان</p></div>
<div class="m2"><p>قصاب گوسفند سر کوی کیستی</p></div></div>