---
title: >-
    شمارهٔ  ۳۰۴
---
# شمارهٔ  ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>در کعبه و بتخانه ز حسن تو صنم های</p></div>
<div class="m2"><p>عشق آمده و ریخته دل بر سر هم های</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند توان ریخت سرشک از مژه بر دل</p></div>
<div class="m2"><p>فردا است که ویران شده این خانه ز نم های</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خویشتن از شوق کنم پاره کفن را</p></div>
<div class="m2"><p>گر بر سر خاکم نهی از لطف قدم های</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سبحه بگسسته فرو ریخته صد دل</p></div>
<div class="m2"><p>تا زلف تو را شانه جدا کرد ز هم های</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین عمر تماشای تو چون سیر توان کرد</p></div>
<div class="m2"><p>فریاد از این خرج پر و مایه کم های</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آشفته‌تر از باد گذشتیم و نکردیم</p></div>
<div class="m2"><p>در کوی تو خاکی به سر خویش ز غم های</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دیده نگه بر خم ابروش کن ای دل</p></div>
<div class="m2"><p>زنهار مپرهیز از این تیغ دو دم های</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخسار تو آسان نتوان دید از اندام</p></div>
<div class="m2"><p>گردیده حیا پرده فانوس حرم های</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصاب بود نامه قتل تو حذر کن</p></div>
<div class="m2"><p>ز آن خط که لبش کرده دگر تازه رقم های</p></div></div>