---
title: >-
    شمارهٔ  ۱۰
---
# شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>اگر آن شمع بزم دل رود مستانه در صحرا</p></div>
<div class="m2"><p>نیاید در نظر غیر از پر پروانه در صحرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نماید هرکجا رخ نه فلک آیینه می‌گردد</p></div>
<div class="m2"><p>ز صیقل کاری خاکستر پروانه در صحرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز وحشت تنگنای شهر زندان است بر عاشق</p></div>
<div class="m2"><p>به وسعت داد عشرت می‌دهد دیوانه در صحرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سیل اشگ میسازم خراب این عالم دل را</p></div>
<div class="m2"><p>برای خویش پیدا می‌کنم ویرانه در صحرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تلاش رزق لازم نیست کایزد کرده در اول</p></div>
<div class="m2"><p>مهیا بهر ما روزی ز آب و دانه در صحرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شده بهر هزاران هر طرف از غنچه‌های گل</p></div>
<div class="m2"><p>عیان در بوته هر خار صد خمخانه در صحرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلیل راه عاشق را چو خاموشی نمی‌باشد</p></div>
<div class="m2"><p>مگو قصاب بی‌جا این قدر افسانه صحرا</p></div></div>