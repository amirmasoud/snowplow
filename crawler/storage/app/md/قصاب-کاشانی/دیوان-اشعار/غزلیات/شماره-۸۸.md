---
title: >-
    شمارهٔ  ۸۸
---
# شمارهٔ  ۸۸

<div class="b" id="bn1"><div class="m1"><p>ای خطّت از قلمرو خوبی ستانده باج</p></div>
<div class="m2"><p>بگرفته نرگست ز غزال ختن خراج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نقره‌ای که سکه کند رایجش به دهر</p></div>
<div class="m2"><p>خط داده تازه حسن جمال تو را رواج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخمی که از نگاه تو آید به جان همان</p></div>
<div class="m2"><p>مژگانت از خدنگ دگر می‌کند علاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خال است کرده جای در اطراف عارضت</p></div>
<div class="m2"><p>یا شاه زنگ تکیه زده بر سریر عاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پا را شمرده نه چو شکستی دل مرا</p></div>
<div class="m2"><p>بگذر به احتیاط از این ریزه زجاج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آب و هوای باغ گل و شمع را چه سود</p></div>
<div class="m2"><p>دل را به اشک و آه مگر بشکند مزاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گفتگو ببند زبان در جهان که نیست</p></div>
<div class="m2"><p>قصاب سنگ تفرقه‌ای بدتر از لجاج</p></div></div>