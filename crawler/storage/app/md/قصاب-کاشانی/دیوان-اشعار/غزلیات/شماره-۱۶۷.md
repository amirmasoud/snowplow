---
title: >-
    شمارهٔ  ۱۶۷
---
# شمارهٔ  ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>ای کعبه ارباب وفا کوی رفوگر</p></div>
<div class="m2"><p>محراب دعا گوشه ابروی رفوگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون قطره شبنم که نشیند به رخ گل</p></div>
<div class="m2"><p>دیدم عرق‌آلوده گل روی رفوگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این رهزن صد قافله یا هاله ماه است</p></div>
<div class="m2"><p>یا سرزده خط از رخ نیکوی رفوگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بهر رفوکاری‌اش افتاده شب و روز</p></div>
<div class="m2"><p>صد پاره دل بر سر زانوی رفوگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بند قبا جانب گلزار گشاید</p></div>
<div class="m2"><p>گلزار معطر شود از بوی رفوگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیرون ز کفم شد دل و دین آخر و گشتم</p></div>
<div class="m2"><p>قصاب اسیر قد دلجوی رفوگر</p></div></div>