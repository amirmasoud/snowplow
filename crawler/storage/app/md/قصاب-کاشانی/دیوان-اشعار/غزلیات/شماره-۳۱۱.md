---
title: >-
    شمارهٔ  ۳۱۱
---
# شمارهٔ  ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>بشنوی گر ز من غمزده پندی مردی</p></div>
<div class="m2"><p>دل به این دهر ستم‌پیشه نبندی مردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خنده و شوخی بیجای گل از بی‌دردی‌ست</p></div>
<div class="m2"><p>گر از این عشرت ده روزه نخندی مردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بید بر خویشتن از بیم بریدن لرزد</p></div>
<div class="m2"><p>اگر از حادثه دهر نچندی مردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کندن صورت شیرین بود آسان در کوه</p></div>
<div class="m2"><p>اگر از جان دل ماتم‌زده کندی مردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش ما کردن تحصیل دوا، نامردی‌ست</p></div>
<div class="m2"><p>چو شدی عاشق اگر درد پسندی مردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچه بر خود مپسندی ز بدی‌ها قصاب</p></div>
<div class="m2"><p>آن بدی را به کسی گر نپسندی مردی</p></div></div>