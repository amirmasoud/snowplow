---
title: >-
    شمارهٔ  ۱۶۶
---
# شمارهٔ  ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>شده‌ست از بس که روز از ماه و ماه از سال رسواتر</p></div>
<div class="m2"><p>مرا هر لحظه گردد صورت احوال رسواتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن دل پایبند شاهد دنیا که در محفل</p></div>
<div class="m2"><p>کند معشوقه بدشکل را خلخال رسواتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم صیدگاه کیست این صحرای پر وحشت</p></div>
<div class="m2"><p>که از صید گرفتار است فارغ‌بال رسواتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلندرمشربان را پرده‌پوشی کی بود لازم</p></div>
<div class="m2"><p>در این ره کوچک ابدال است از ابدال رسواتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بی‌تابی ره سیلاب را کی‌ می‌توان بستن</p></div>
<div class="m2"><p>کند نوکیسه را هر دم غرور مال رسواتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گذشت از حد تو را رسوایی ای قصاب می‌ترسم</p></div>
<div class="m2"><p>که سازد روز حشرت نامه اعمال رسواتر</p></div></div>