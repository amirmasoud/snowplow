---
title: >-
    شمارهٔ  ۲۴۴
---
# شمارهٔ  ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>سوختم مهر یار را نازم</p></div>
<div class="m2"><p>گرمی آن نگار را نازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار راهش ز گریه‌ام شد سبز</p></div>
<div class="m2"><p>فیض این نوبهار را نازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا قیامت کشیده وعده وصل</p></div>
<div class="m2"><p>طاقت انتظار را نازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بردن نرد عشق جان‌بازی است</p></div>
<div class="m2"><p>دو شش این قمار را نازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم آفاق را به من دادند</p></div>
<div class="m2"><p>رتبه و اعتبار را نازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتی‌ام شد ز دیده طوفانی</p></div>
<div class="m2"><p>دیده اشکبار را نازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرده ما را ز خود پریشان‌تر</p></div>
<div class="m2"><p>سر زلف نگار را نازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سنگ زیرین آسیا شده‌ام</p></div>
<div class="m2"><p>گردش روزگار را نازم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز پدر دارم ارث صحرا را</p></div>
<div class="m2"><p>خانه بی‌حصار را نازم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خون دل شد حواله قصاب</p></div>
<div class="m2"><p>تا به حشر این قرار را نازم</p></div></div>