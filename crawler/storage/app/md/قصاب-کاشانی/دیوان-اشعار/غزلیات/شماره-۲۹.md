---
title: >-
    شمارهٔ  ۲۹
---
# شمارهٔ  ۲۹

<div class="b" id="bn1"><div class="m1"><p>مگر آن آتشین‌خو آگه از بخت من است امشب</p></div>
<div class="m2"><p>که همچون شمع مغز استخوانم روشن است امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گلشن جان من دی تا از استغنا گذر کردی</p></div>
<div class="m2"><p>ز مژگان تو گل را خار در پیراهن است امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جان افشانی من حسن او را شعله افزون شد</p></div>
<div class="m2"><p>مگر بر آتش گل بال بلبل دامن است امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دستارم بنفشه مشت خاکستر بود امشب</p></div>
<div class="m2"><p>نگاهی کن که گلشن بی تو بر من گلخن است امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو شمع مجلس‌افروزی و من پروانه محفل</p></div>
<div class="m2"><p>نشستن ازتو، بر گرد تو گشتن از من است امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی‌دانم نگه چون رفت بیرون گریه چون آمد</p></div>
<div class="m2"><p>ز بس چشمم به رخسار تو محو دیدن است امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز داغ دوری‌ات صد رنگ گل در آستین دارم</p></div>
<div class="m2"><p>بیا گلچین که از داغ تو دستم گلشن است امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دید آن سبز گندمگون دلم را گفت زیر لب</p></div>
<div class="m2"><p>که یک مور ضعیفی در کنار خرمن است امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمانده در تنم جایی کزو قصاب ناید خون</p></div>
<div class="m2"><p>مرا خون در جگر چون آب در پرویزن است امشب</p></div></div>