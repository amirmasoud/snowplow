---
title: >-
    شمارهٔ  ۱۰۲
---
# شمارهٔ  ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>بیرون خیالت از دل غافل نمی‌رود</p></div>
<div class="m2"><p>ور می‌رود به سوی تو بی دل نمی‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز مرا هوای سر کویت ای نگار</p></div>
<div class="m2"><p>از سر برون ز دوری منزل نمی‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحری است عشق او که ز باد مخالفش</p></div>
<div class="m2"><p>تا نشکند قراب به ساحل نمی‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آید چو یار تا نکند کار عشق را</p></div>
<div class="m2"><p>بر من هزار مرتبه مشکل نمی‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصاب تیر غمزه چو خوردی قرار گیر</p></div>
<div class="m2"><p>کس زخم‌دار از پی قاتل نمی‌رود</p></div></div>