---
title: >-
    شمارهٔ  ۵۸
---
# شمارهٔ  ۵۸

<div class="b" id="bn1"><div class="m1"><p>روزی که کرد گردون غم‌های یار قسمت</p></div>
<div class="m2"><p>ما را رساند بر دل زان غم هزار قسمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادند جا غمت را پنهان درون دل‌ها</p></div>
<div class="m2"><p>داغ تو را نمودند در لاله‌زار قسمت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عارض و خط تو گلزار رنگ و بو را</p></div>
<div class="m2"><p>بگرفت تا نماید در هر بها‌ر قسمت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی که حسنت از رخ برقع گشود دل را</p></div>
<div class="m2"><p>کردند نوری از آن آیینه‌زار قسمت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا زلف سرکش او آشفته بود کردند</p></div>
<div class="m2"><p>تاری از آن میان بر شب‌های تار قسمت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کشتگان نازش ساکت‌ شوند کردند</p></div>
<div class="m2"><p>گردی ز کوی او را بر هر مزار قسمت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از هر کجا گذشتی خاک از دو دیده مردم</p></div>
<div class="m2"><p>چون توتیا نمودند زان رهگذار قسمت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا هر دلی که باشد بی‌بهره زان نماند</p></div>
<div class="m2"><p>درد تو را نمودند چندین هزار قسمت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تقصیر گلستان چیست کردند روز اول</p></div>
<div class="m2"><p>افغان به عندلیبان گل را به خار قسمت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دام چین زلفت از بس نمانده جایی</p></div>
<div class="m2"><p>یک لحظه می‌نمایند چندین شکار قسمت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از خوان دهر قصاب جز خون دل نخوردیم</p></div>
<div class="m2"><p>تقصیر ما چه باشد این کرده یار قسمت</p></div></div>