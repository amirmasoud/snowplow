---
title: >-
    شمارهٔ  ۴۱
---
# شمارهٔ  ۴۱

<div class="b" id="bn1"><div class="m1"><p>بس ‌که بر جانم ز مژگانت خدنگ افتاده است</p></div>
<div class="m2"><p>وسعتی خواهم که بر دل کار تنگ افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو با این آب و رنگ آهنگ گلشن کرده‌ای</p></div>
<div class="m2"><p>گل ز شرم عارضت از آب و رنگ افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عطر سنبل بلبلان را گرم افغان کرده است</p></div>
<div class="m2"><p>تار زلفت تا گلستان را به چنگ افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک دل مجروح با چندین غم او چون کند</p></div>
<div class="m2"><p>میهمان بسیار و ما را خانه تنگ افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی توان زین بحر کام از هر صدف حاصل نمود</p></div>
<div class="m2"><p>گوهر مقصود در کام نهنگ افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دل پرخونم از آسیب گردون نشکند</p></div>
<div class="m2"><p>من که دائم شیشه‌ام در راه سنگ افتاده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از غبار کینه پیدا نیست در دل عکس دوست</p></div>
<div class="m2"><p>حیف کاین آیینه بی‌حاصل به زنگ افتاده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا قیامت زنده در گور است مانند نگین</p></div>
<div class="m2"><p>هر که در دنیا به قید نام و ننگ افتاده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرد تا عزم رخش قصاب اثر از دل نشد</p></div>
<div class="m2"><p>می‌توان دانست در قید فرنگ افتاده است</p></div></div>