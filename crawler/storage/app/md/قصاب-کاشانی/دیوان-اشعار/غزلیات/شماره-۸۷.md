---
title: >-
    شمارهٔ  ۸۷
---
# شمارهٔ  ۸۷

<div class="b" id="bn1"><div class="m1"><p>از کج‌نهاد سرنزند جز بیان کج</p></div>
<div class="m2"><p>کج می‌رود خدنگ برون از کمان کج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نتوان خموش ساخت زبان اهل کذب را</p></div>
<div class="m2"><p>دشوار بسته در شود از آستان کج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظاهر چو شد هوس نشود کار عشق راست</p></div>
<div class="m2"><p>رهرو به منزلی نرسد از نشان کج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی عنان به دست هوا و هوس دهی</p></div>
<div class="m2"><p>بیرون ز راه افتی از این همرهان کج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زیر آسمان برو ای دل که بیش از این</p></div>
<div class="m2"><p>نتوان نشست در پس این سایبان کج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد سرنگون ز سنگ حوادث چو شهپرم</p></div>
<div class="m2"><p>قصاب اوفتادم از این آشیان کج</p></div></div>