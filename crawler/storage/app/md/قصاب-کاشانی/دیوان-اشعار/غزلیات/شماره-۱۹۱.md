---
title: >-
    شمارهٔ  ۱۹۱
---
# شمارهٔ  ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>منم فتاده به راه تو خاکسار مشخّص</p></div>
<div class="m2"><p>به روی آینه گیتی‌ام غبار مشخّص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هرکجا که روم ز آه و اشک بادم و باران</p></div>
<div class="m2"><p>به دور سبزه خط توام غبار مشخّص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز پای تا به سرم نیست عضو بی گل داغت</p></div>
<div class="m2"><p>از این چمن منم امروز لاله‌زار مشخّص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه حاصلی نه نمودی نه سایه‌ای و نه سودی</p></div>
<div class="m2"><p>به گرد این چمنم کرده‌ای حصار مشخّص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فراق روی توام کرده است پرده قانون</p></div>
<div class="m2"><p>خیال زلف توام ساخته است تار مشخّص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شده است تا تن قصاب پایمال حوادث</p></div>
<div class="m2"><p>به چشم خلق بود خاک رهگذار مشخّص</p></div></div>