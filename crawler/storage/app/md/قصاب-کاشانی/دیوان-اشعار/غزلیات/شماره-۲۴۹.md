---
title: >-
    شمارهٔ  ۲۴۹
---
# شمارهٔ  ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>کجاست دیده که رو سوی یار خویش کنم</p></div>
<div class="m2"><p>علاج درد دل بی‌قرار خویش کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خاک کوی بتان بو غم نمی‌آید</p></div>
<div class="m2"><p>مگر همان به سر خود غبار خویش کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو کرم پیله به خود در تنم شب هجران</p></div>
<div class="m2"><p>به حالتی که غمش را حصار خویش کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ابر جمله کریمان نظر فکنده و من</p></div>
<div class="m2"><p>نگه به چشم تر اشگبار خویش کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هجر اگر کشیم دل نمی‌کند باور</p></div>
<div class="m2"><p>دروغ وصل تو تا کی به کار خویش کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط غلامی او خطّ سرنوشت من است</p></div>
<div class="m2"><p>همین بس است که لوح مزار خویش کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار حیف که در این چمن رسید خزان</p></div>
<div class="m2"><p>امان نداد که فکر بهار خویش کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طمع به هیچ ندارم در این جهان قصاب</p></div>
<div class="m2"><p>سوای جان که فدای نگار خویش کنم</p></div></div>