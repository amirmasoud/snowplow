---
title: >-
    شمارهٔ  ۱۸۴
---
# شمارهٔ  ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>به پاسبانی یک قطره آب گوهر خویش</p></div>
<div class="m2"><p>به هر دو دست نگهدار چون صدف سر خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش آن زمان که به قصد هوای کشور خویش</p></div>
<div class="m2"><p>گهی برون ز شکاف قفس کنم سر خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشد نشاط نصیبم چو صید دام شدم</p></div>
<div class="m2"><p>مگر به وقت طپیدن به هم زنم پر خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی بداد ز دستم گهی بدرد از پا</p></div>
<div class="m2"><p>چه‌ها نمی‌کشم از دست نفس کافر خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رود چو خامه سرش یک قلم به باد فنا</p></div>
<div class="m2"><p>قدم هرآنکه گذارد برون ز مسطر خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب مبین که سر ما به آسمان ساید</p></div>
<div class="m2"><p>از آنکه داغ تو را کرده‌ایم افسر خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو قدر دل چه شناسی که در محیط، صدف</p></div>
<div class="m2"><p>چه احتمال که داند بهای گوهر خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز جور دهر به تنگ آمدم بسی قصاب</p></div>
<div class="m2"><p>بریم درددل خویش را به داور خویش</p></div></div>