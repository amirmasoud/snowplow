---
title: >-
    شمارهٔ  ۲۴۰
---
# شمارهٔ  ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>در هر دو جهان عاقل و فرزانه نباشم</p></div>
<div class="m2"><p>گر آنکه گرفتار تو جانانه نباشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را ز فغان می‌کنم از درد تو خالی</p></div>
<div class="m2"><p>فریاد از آن روز که دیوانه نباشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گرد سرت گردم و جان‌باز بسوزم</p></div>
<div class="m2"><p>این‌‌ها نکنم پیش تو پروانه نباشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خود نروم شام فراق تو که ترسم</p></div>
<div class="m2"><p>مهرت در دل کوبد و در خانه نباشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارم به دل از داغ تو صد گنج نهان بیش</p></div>
<div class="m2"><p>از سیل حوادث ز چه ویرانه نباشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردان همه جان در ره جانانه سپردند</p></div>
<div class="m2"><p>چون سر نسپارم ز چه مردانه نباشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سردار شده عشق و گر امروز شبیخون</p></div>
<div class="m2"><p>بر لشگر زلفت نزنم شانه نباشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درس دل و جان باختن از عشق گرفتم</p></div>
<div class="m2"><p>ناگوش بر آواز هر افسانه نباشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصاب در اول به غمش دست اخوت</p></div>
<div class="m2"><p>دادم که در این مرحله بیگانه نباشم</p></div></div>