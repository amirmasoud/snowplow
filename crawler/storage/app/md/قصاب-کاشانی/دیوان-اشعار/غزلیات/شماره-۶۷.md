---
title: >-
    شمارهٔ  ۶۷
---
# شمارهٔ  ۶۷

<div class="b" id="bn1"><div class="m1"><p>یاد تو مونس شب تار دل من است</p></div>
<div class="m2"><p>داغت گل همیشه بهار دل من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخی که نه رواق فلک زیر بال او است</p></div>
<div class="m2"><p>در صیدگاه عشق شکار دل من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشتن شهید تیغ تو نقشی است بر مراد</p></div>
<div class="m2"><p>جان باختن به خصم قمار دل من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر تار زلف مرغ دلم را شد آشیان</p></div>
<div class="m2"><p>هر حلقه‌ای ز دام حصار دل من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیدا است از صفای رخش عکس راز من</p></div>
<div class="m2"><p>رخسار یار آینه‌دار دل من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندین هزار آرزو اینجا است در شنا</p></div>
<div class="m2"><p>گویی میان بحر کنار دل من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون قد کشید آه مرا خرم است باغ</p></div>
<div class="m2"><p>چون چشمه کرد داغ بهار دل من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قصاب طی مرحله‌ام تا تپیدن است</p></div>
<div class="m2"><p>خون خوردن مدام مدار دل من است</p></div></div>