---
title: >-
    شمارهٔ  ۱۳۱
---
# شمارهٔ  ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>خورشید تف از عارض تابان تو دارد</p></div>
<div class="m2"><p>مه روشنی از شمع شبستان تو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمکین و سرافرازی و رعنایی و خوبی</p></div>
<div class="m2"><p>سرو سهی از قدّ خرامان تو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود را ز حشم کم ز سلیمان نشمارد</p></div>
<div class="m2"><p>آن مور که ره بر شکرستان تو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفاق ز رخ کرده منوّر گل خورشید</p></div>
<div class="m2"><p>پیداست که رنگی ز گلستان تو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنگام تبسّم ز غزل‌خوانی بلبل</p></div>
<div class="m2"><p>رمزی است که گل از لب خندان تو دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر طعنه زند بر چمن خلد عجب نیست</p></div>
<div class="m2"><p>آن دل که گل غنچه پیکان تو دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برکش ز میان تیغ و به قصاب نظر کن</p></div>
<div class="m2"><p>چون گردن تسلیم به فرمان تو دارد</p></div></div>