---
title: >-
    شمارهٔ  ۱۹۴
---
# شمارهٔ  ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>می‌دهد هر ساعتی چشمش شرابم ز اعتراض</p></div>
<div class="m2"><p>می‌نماید آتش لعلش کبابم ز اعتراض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست حرفش بیش از این کز پیش من کم کن گذر</p></div>
<div class="m2"><p>زیر لب گاهی که می‌گوید جوابم ز اعتراض</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روبرو هرگه که برخوردم به آن دریای حسن</p></div>
<div class="m2"><p>همچو ماهی در خوی خجلت بر آبم ز اعتراض</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو موم نحل کز خورشید می‌پاشد ز هم</p></div>
<div class="m2"><p>پیش رخسار تو چون آیم خرابم ز اعتراض</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست از جان شسته در پیشش گریزم هر نفس</p></div>
<div class="m2"><p>بر سر دریای بی‌تابی حبابم ز اعتراض</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی مرا بیدار سازد خوف روز رستخیز</p></div>
<div class="m2"><p>چون کند افسون چشم او به‌‌ خوابم ز اعتراض</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌برد قصاب بی‌هوشی به راه گلشنم</p></div>
<div class="m2"><p>چون زند آن دل‌ربا بر رخ گلابم ز اعتراض</p></div></div>