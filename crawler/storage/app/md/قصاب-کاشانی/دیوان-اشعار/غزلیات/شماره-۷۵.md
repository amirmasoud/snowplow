---
title: >-
    شمارهٔ  ۷۵
---
# شمارهٔ  ۷۵

<div class="b" id="bn1"><div class="m1"><p>ماییم که جز اشک ندامت بر ما نیست</p></div>
<div class="m2"><p>کوثر به گوارایی چشم تر ما نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محویم به گلزار تو چون بلبل تصویر</p></div>
<div class="m2"><p>شادیم که پرواز نصیب پر ما نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما بهره نداریم ز آرام چو سیماب</p></div>
<div class="m2"><p>این جنس متاعی است که در کشور ما نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جامه که بر قامت ما دوخته ایام</p></div>
<div class="m2"><p>تا چاک نگردیده چو گل، در بر ما نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتح صف عشاق بود وقت شهادت</p></div>
<div class="m2"><p>هرکس که زجان نگذرد از لشکر ما نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سوختن ارشاد ز پروانه گرفتیم</p></div>
<div class="m2"><p>هرگز اثری ز آتش و خاکستر ما نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین مرتبه بهتر چه که در گلشن ایام</p></div>
<div class="m2"><p>جز لاله داغ تو گلی بر سر ما نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگز به رخ دل در عیشی نگشادیم</p></div>
<div class="m2"><p>این باب حسابی است که در دفتر ما نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه بیخودِ گفتار و گهی مست نگاهیم</p></div>
<div class="m2"><p>کی باده شوقی است که در ساغر ما نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قصاب چو چین جمله اسیریم بر آن زلف</p></div>
<div class="m2"><p>بیرون شو از این سلسه در خاطر ما نیست</p></div></div>