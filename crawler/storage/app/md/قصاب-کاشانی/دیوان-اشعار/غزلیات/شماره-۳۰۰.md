---
title: >-
    شمارهٔ  ۳۰۰
---
# شمارهٔ  ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>هرگز به کام دل ننشستیم رو‌به‌روی</p></div>
<div class="m2"><p>یک لحظه با وصال تو ای ترک تند خوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خضر آن‌قدر که داشت غم آب زندگی</p></div>
<div class="m2"><p>داریم ما به شربت تیغ تو آرزوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر چشم من خرام و زمانی قرار گیر</p></div>
<div class="m2"><p>تا سروت آبخور شود از این کنار جوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون طفل کند فهم ز تکرار درس عشق</p></div>
<div class="m2"><p>دائم فتاده پیش توام گریه در گلوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تیغ کینه چون رسد آن شوخ از غضب</p></div>
<div class="m2"><p>قصاب جان فدا کن و از وی متاب روی</p></div></div>