---
title: >-
    شمارهٔ  ۲۳۳
---
# شمارهٔ  ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>تا خون خویش در ره جانانه ریختیم</p></div>
<div class="m2"><p>آبی به پای آن بت فرزانه ریختیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردیم ظرف دیده لبالب ز خون دل</p></div>
<div class="m2"><p>این باده را ز ظرف به پیمانه ریختیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دادیم جای مهر تو را در زمین دل</p></div>
<div class="m2"><p>تخمی به خاک این ده ویرانه ریختیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندوه و داغ و درد و غم و آتش فراق</p></div>
<div class="m2"><p>کردیم جمع و در دل دیوانه ریختیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصاب آب اشک ندامت نداشت سود</p></div>
<div class="m2"><p>چندان که پیش محرم و بیگانه ریختیم</p></div></div>