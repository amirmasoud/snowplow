---
title: >-
    شمارهٔ  ۲۹۵
---
# شمارهٔ  ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>آنکه رخ بنمود و روشن ساخت جان در تن تویی</p></div>
<div class="m2"><p>آنکه آتش زد به من چون برق در خرمن تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه اندر یک تبسم کرد جان در تن تویی</p></div>
<div class="m2"><p>آنکه جان بگرفت از یک زهر چشم از من تویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شعله‌های آه جان‌سوز از که می‌پرسی که چیست</p></div>
<div class="m2"><p>آنکه زد بر آتش بیچارگان دامن تویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای نسیم کوی یار این سرگرانی تا به کی</p></div>
<div class="m2"><p>می‌رساند آنکه بر ما بوی پیراهن تویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلوه کن در باغ تا گیرند گل‌ها از تو رنگ</p></div>
<div class="m2"><p>چون جمال‌آرا و زینت‌بخش این گلشن تویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌رساند آنکه در عالم برای پرورش</p></div>
<div class="m2"><p>مور اعمی را ز احسان بر سر خرمن تویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حریم عاشقان دوست جای غیر نیست</p></div>
<div class="m2"><p>آنکه چون مهر تو در دل می‌کند مسکن تویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم آمرزش به درگاه تو دارم روز و شب</p></div>
<div class="m2"><p>می‌تواند آنکه بخشاید گناه من تویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌توانی پرتوی قصاب را در دل فکند</p></div>
<div class="m2"><p>آنکه شمع مهر و مه را می‌کند روشن تویی</p></div></div>