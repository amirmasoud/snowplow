---
title: >-
    شمارهٔ  ۱۲۲
---
# شمارهٔ  ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>چون خونم از دو دیده گریان روان شود</p></div>
<div class="m2"><p>مژگان ز اشک شاخ گل ارغوان شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن کند چراغ رخش نور آفتاب</p></div>
<div class="m2"><p>روشن چو شمع محفل روحانیان شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سربرنگیرم از ره کنعان به راه تو</p></div>
<div class="m2"><p>تا خاک من غبار ره کاروان شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردیم با وجود که یک جو نداشت مهر</p></div>
<div class="m2"><p>فریاد از آن زمان که به ما مهربان شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بس که گفتم و نشنیدم جواب از او</p></div>
<div class="m2"><p>نزدیک شد دمی که زبان استخوان شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر وصل نارسیده مرا شرم آب کرد</p></div>
<div class="m2"><p>قصاب چیست چاره چو آتش عیان شود</p></div></div>