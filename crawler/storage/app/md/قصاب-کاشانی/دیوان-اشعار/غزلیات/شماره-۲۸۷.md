---
title: >-
    شمارهٔ  ۲۸۷
---
# شمارهٔ  ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>ترک سر ناگفته دل بر مهر جانان بسته‌ای</p></div>
<div class="m2"><p>نیستی عاشق چرا بر خویش بهتان بسته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سعی کن ای دیده تا پیدا کنی سرچشمه‌ای</p></div>
<div class="m2"><p>چون صدف دل را چرا بر ابر نیسان بسته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ‌کس از سحر چشمت سر نمی‌آرد برون</p></div>
<div class="m2"><p>از نگاهی راه بر گبر و مسلمان بسته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منزل جمعیت آسایش دل‌ها است این</p></div>
<div class="m2"><p>چیست این تهمت که بر زلف پریشان بسته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در لبت موج تبسم بخیه دل‌های ما است</p></div>
<div class="m2"><p>خون چندین زخم از گرد نمکدان بسته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صید دام افتاده را صیاد بندد بال و پر</p></div>
<div class="m2"><p>حیرتی دارم که چونم رشته بر جان بسته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی فراموشت کنم ای جان گره بگشا ز زلف</p></div>
<div class="m2"><p>از چه‌ام این رشته بر انگشت نسیان بسته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوسفند تو است قصاب از نظر نندازیش</p></div>
<div class="m2"><p>تربیت کن بهترش چون خویش قربان بسته‌ای</p></div></div>