---
title: >-
    شمارهٔ  ۲۰۷
---
# شمارهٔ  ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>ز دیدن تو شود دیده را حیا مانع</p></div>
<div class="m2"><p>همیشه هست کسی در میان ما مانع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان ما و تو قطع امید ممکن نیست</p></div>
<div class="m2"><p>تو را جفا و مرا می‌شود وفا مانع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن سبب نرسد تیر آه ما به هدف</p></div>
<div class="m2"><p>که هست در حد ره سد مدّعا مانع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در این محیط ندیدیم روی ساحل را</p></div>
<div class="m2"><p>چرا که کشتی مارا است ناخدا مانع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر رسم به وصالش مرا شود قصاب</p></div>
<div class="m2"><p>به گاه دیدن او سایه از قفا مانع</p></div></div>