---
title: >-
    شمارهٔ  ۱۳۷
---
# شمارهٔ  ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>درشتی از مزاج سخت‌طینت کم نمی‌گردد</p></div>
<div class="m2"><p>کنی چندان که نرم الماس را مرهم نمی‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تن زینت‌پرستان را گر از هستی نمی‌باشد</p></div>
<div class="m2"><p>سفالین کوزه زردار جام جم نمی‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز اسباب جهان بگذر که گر عیسی است در گیتی</p></div>
<div class="m2"><p>به بند سوزنی تا هست صاحب‌دل نمی‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غیر از زور بازوی قناعت در همه عالم</p></div>
<div class="m2"><p>کس دیگر حریف خواهش آدم نمی‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه طرح میهمانی با جهان می‌افکنی؟ غافل</p></div>
<div class="m2"><p>سپهر بی‌مروت با کسی همدم نمی‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی می‌کرد خونم در دل و می‌گفت زاستغنا</p></div>
<div class="m2"><p>نه بیند این زمین تا شبنمی خرم نمی‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر چیزی است شرط آدمیت در جهان ورنه</p></div>
<div class="m2"><p>کسی از چشم و گوش و دست و پا آدم نمی‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان عاشق و معشوق سرّی هست پنهانی</p></div>
<div class="m2"><p>بدین سرّ آنکه از سر نگذرد محرم نمی‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گردش‌های چرخ واژگون قصاب دانستم</p></div>
<div class="m2"><p>که هرگز بر مراد هیچ‌کس یک دم نمی‌گردد</p></div></div>