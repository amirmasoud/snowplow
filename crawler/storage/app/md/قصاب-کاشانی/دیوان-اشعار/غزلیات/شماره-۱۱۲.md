---
title: >-
    شمارهٔ  ۱۱۲
---
# شمارهٔ  ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>دلم شبی که به شکّر لبی خطاب ندارد</p></div>
<div class="m2"><p>چو کودکی است که از بهر شیر خواب ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب وصال مکن منعم از دو دیده بی‌نم</p></div>
<div class="m2"><p>عجب مدان گل تصویر اگر گلاب ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دل چو مهر رخت نیست داغ نقش نه بندد</p></div>
<div class="m2"><p>نروید از چمنی گل که آفتاب ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آب و تاب رخت را به آفتاب نسنجم</p></div>
<div class="m2"><p>که آفتاب اگر تاب دارد آب ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خاطری که دو مصرع ز ابروی تو نباشد</p></div>
<div class="m2"><p>صحیفه‌ای است که یک بیت انتخاب ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دل ز خون نشود پر نمی‌رسد به وصالی</p></div>
<div class="m2"><p>به بزم جا نکند شیشه تا شراب ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دل مپرس که عشقت ز داغ‌های نهانی</p></div>
<div class="m2"><p>چه گنج‌ها که در این خانه خراب ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تمام خون دل است اینکه دیده ریخت به راهت</p></div>
<div class="m2"><p>غمین مباش ز سودای ما که آب ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به قصد کشتن قصاب اضطراب چه داری</p></div>
<div class="m2"><p>شهید تیغ تو خواهد شدن شتاب ندارد</p></div></div>