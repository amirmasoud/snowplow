---
title: >-
    شمارهٔ  ۲۸۲
---
# شمارهٔ  ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>شد از فراق توام قامت کشیده خمیده</p></div>
<div class="m2"><p>هزار خار ملامت به پای دیده خلیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب گذشته به یاد رخ تو مردم چشمم</p></div>
<div class="m2"><p>هزار قطره خون خورده تا سفیده دمیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار بار دلم می‌زند به گرد سرت پر</p></div>
<div class="m2"><p>ندیده است کسی صید پر بریده پریده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شوره‌زار بود بیشتر چراگه آهو</p></div>
<div class="m2"><p>به حیرتم که غزالم چرا ز دیده رمیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عکس نیک و بد آیینه را ملال نباشد</p></div>
<div class="m2"><p>یکی است در بر روشن‌دلان ندیده و دیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون خرام که کرّوبیان عالم بالا</p></div>
<div class="m2"><p>کشند خاک درت را ز شوق دیده به دیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شوق دیدن رویت ز دیده تا سر کویت</p></div>
<div class="m2"><p>چه چاره مدّ نگاهم به سر دویده ندیده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رساند ریشه به خون رفته‌رفته نخل سرشگم</p></div>
<div class="m2"><p>از این نهال ثمر تا بهم رسیده رسیده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به یاد دوست به سر بر وصال اگر ندهد رو</p></div>
<div class="m2"><p>یکی است در بر بلبل گل نچیده و چیده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه باک از دل قصاب کز فشار حوادث</p></div>
<div class="m2"><p>هزار مرتبه خون گشته و ز دیده چکیده</p></div></div>