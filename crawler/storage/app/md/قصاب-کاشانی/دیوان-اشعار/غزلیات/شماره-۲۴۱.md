---
title: >-
    شمارهٔ  ۲۴۱
---
# شمارهٔ  ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>بر میان تاری ز زلف یار می‌خواهد دلم</p></div>
<div class="m2"><p>سبحه را افکنده و زنّار می‌خواهد دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نشانی هست از غم‌خانه زندان دوست</p></div>
<div class="m2"><p>کافرم گر جانب گلزار می‌خواهد دلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نشینی در میان چون نقطه بر گرد سرت</p></div>
<div class="m2"><p>یک قدم رفتار چون پرگار می‌خواهد دلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روشن آن مجلس که از حسن تو باشد تا به صبح</p></div>
<div class="m2"><p>چون کواکب دیده بیدار می‌خواهد دلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه فلک را باده شوقت به چرخ آورده است</p></div>
<div class="m2"><p>هم از این می باده سرشار می‌خواهد دلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در فراقت جان من عمری است تاب آورده‌ایم</p></div>
<div class="m2"><p>یک نفس در طاقت دیدار می‌خواهد دلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن‌قدر قصاب می‌دانم که از کون و مکان</p></div>
<div class="m2"><p>عشق را می‌خواهد و بسیار می‌خواهد دلم</p></div></div>