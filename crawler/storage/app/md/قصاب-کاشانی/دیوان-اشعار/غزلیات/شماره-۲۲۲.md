---
title: >-
    شمارهٔ  ۲۲۲
---
# شمارهٔ  ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>عالم همه باطل غم عالم همه باطل</p></div>
<div class="m2"><p>در جای چنین کوشش آدم همه باطل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این دیر همان کهنه بنایی است که در وی</p></div>
<div class="m2"><p>شد تاج کی و سلطنت جم همه باطل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این عرصه همان دخمه‌زمین است که گرداند</p></div>
<div class="m2"><p>دست کی و سرپنجه رستم همه باطل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با عیش و الم ساز که خواهد شدن آخر</p></div>
<div class="m2"><p>فردا به تو نوروز و محرم همه باطل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمگین مشو از سفره افلاک که کردند</p></div>
<div class="m2"><p>بیش است اگر رزق تو گر کم همه باطل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصاب ز دوران مشو آزرده که باشد</p></div>
<div class="m2"><p>از آمده و رفته جز این دم همه باطل</p></div></div>