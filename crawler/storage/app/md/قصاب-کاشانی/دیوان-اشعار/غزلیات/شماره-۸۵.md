---
title: >-
    شمارهٔ  ۸۵
---
# شمارهٔ  ۸۵

<div class="b" id="bn1"><div class="m1"><p>همرهان بر سینه بی دلدار سنگ ما عبث</p></div>
<div class="m2"><p>می‌کند آه و فغان بسیار زنگ ما عبث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما حریف حیله‌بازی‌های گردون نیستیم</p></div>
<div class="m2"><p>هست با این خصم بی زنهار جنگ ما عبث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهر دارد در این هر سنگ دزدی در کمین</p></div>
<div class="m2"><p>شد در این منزلگه خونخوار لنگ ما عبث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گلم بار تعلق بر قفا افکنده است</p></div>
<div class="m2"><p>مانده بی روی تو بر رخسار رنگ ما عبث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست یک کف از زمین دهر بی پست و بلند</p></div>
<div class="m2"><p>می‌دود زین راه ناهموار خنگ ما عبث</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما به ابروی بتی قصاب دل را داده‌ایم</p></div>
<div class="m2"><p>بسته بر این تیغ جوهردار زنگ ما عبث</p></div></div>