---
title: >-
    شمارهٔ  ۲۵۱
---
# شمارهٔ  ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>تا چون نگه توان شد دور از میان مردم</p></div>
<div class="m2"><p>زنهار جا نسازی در خانمان مردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرواز گیر ای دل تا کی چو مرغ تصویر</p></div>
<div class="m2"><p>حیران توان نشستن در آشیان مردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم از زنخ بپوشان بردار دست از زلف</p></div>
<div class="m2"><p>نتوان به چاه رفتن با ریسمان مردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از حرص همچون کرکس تا کی در این بیابان</p></div>
<div class="m2"><p>چشم طمع توان داشت بر استخوان مردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکت به دیده‌ ای نفس شرمی بیار تا چند</p></div>
<div class="m2"><p>پر چون مگس توان زد بر گرد خوان مردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصاب همچو طوطی آیینه در برابر</p></div>
<div class="m2"><p>تا چند می‌توان زد حرف از دهان مردم</p></div></div>