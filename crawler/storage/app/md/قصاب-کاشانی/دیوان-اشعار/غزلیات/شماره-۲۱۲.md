---
title: >-
    شمارهٔ  ۲۱۲
---
# شمارهٔ  ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>دیده تا چند به دیوار تو یا شاه نجف</p></div>
<div class="m2"><p>مردم از حسرت دیدار تو یا شاه نجف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین گلستان گل عیشم رسد آن دم که فتد</p></div>
<div class="m2"><p>نظرم بر گل رخسار تو یا شاه نجف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روضه خلد برین خوار بود در نظرش</p></div>
<div class="m2"><p>هر که گل چید ز گلزار تو یا شاه نجف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست حرفی که ز سرّ دو جهان آگاه است</p></div>
<div class="m2"><p>هرکه شد آگه از اسرار تو یا شاه نجف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقد و جنس دو جهان را به فدای تو نمود</p></div>
<div class="m2"><p>هرکه گردید خریدار تو یا شاه نجف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی آیی به سرم چون رودم از تن جان</p></div>
<div class="m2"><p>نیست انکار در اقرار تو یا شاه نجف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیمی از تاب صف حشر ندارد قصاب</p></div>
<div class="m2"><p>هست در سایه دیوار تو یا شاه نجف</p></div></div>