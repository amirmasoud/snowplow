---
title: >-
    شمارهٔ  ۲۱۱
---
# شمارهٔ  ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>روز و شب در بزم دوران بی‌قرارم همچو دف</p></div>
<div class="m2"><p>بس‌که سیلی‌خوار دست روزگارم همچو دف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون که می‌سازد به گرم و سرد طبعم روزگار</p></div>
<div class="m2"><p>زآب و آتش می‌رسد گاهی مدارم همچو دف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی از این بی‌خانمانی منت از دونان کشم</p></div>
<div class="m2"><p>من که از پهلوی خود باشد حصارم همچو دف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زینتی جز پوست بر بالای من زیبنده نیست</p></div>
<div class="m2"><p>با غم دیبای اطلس نیست کارم همچو دف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشته‌ام پنهان به زیر خرقه عریان تنی</p></div>
<div class="m2"><p>کی دگر از دهر چشم فتنه دارم همچو دف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بساط نغمه‌سنجان حلقه‌ها دارم به گوش</p></div>
<div class="m2"><p>پای تا سر در محبت داغ‌دارم همچو دف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا زچشم بد به هنگام حجاب ایمن بود</p></div>
<div class="m2"><p>پیش روی یار دائم بی‌قرارم همچو دف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا صدای یار نگذارد ز مجلس پا برون</p></div>
<div class="m2"><p>بهر حفظ ناله‌اش پرگاروارم همچو دف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌برندم گرچه قصاب این زمان بر روی دست</p></div>
<div class="m2"><p>پیش جانان غیر افغان نیست کارم همچو دف</p></div></div>