---
title: >-
    شمارهٔ  ۱۹۷
---
# شمارهٔ  ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>یار هجران وفا کند به غلط</p></div>
<div class="m2"><p>درد ما را دوا کند به غلط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست روی لبش به جانب غیر</p></div>
<div class="m2"><p>نظری چون به ما کند به غلط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانه‌ام از برای حفظ بدن</p></div>
<div class="m2"><p>تکیه بر آسیا کند به غلط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم از موج صد خطر دارد</p></div>
<div class="m2"><p>در سراب آشنا کند به غلط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عنایات، حاجت قصاب</p></div>
<div class="m2"><p>چه شود گر روا کند به کند</p></div></div>