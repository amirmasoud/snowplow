---
title: >-
    شمارهٔ  ۸۰
---
# شمارهٔ  ۸۰

<div class="b" id="bn1"><div class="m1"><p>سیر دنیا یک قدم نشو و نمایی بیش نیست</p></div>
<div class="m2"><p>همچو گل در صحبت باد صبایی بیش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد حسن او به دست ما چو آید می‌رود</p></div>
<div class="m2"><p>وصل عاشق را به کف رنگ حنایی بیش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده باطن دو عالم را تماشا می‌کند</p></div>
<div class="m2"><p>چشم ظاهربین چراغ پیش‌ پایی بیش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق از یک دیدن قاتل تسلی می‌شود</p></div>
<div class="m2"><p>کشته او را نگاهش خون‌بهایی بیش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقصد رندان ز عالم واله هم گشتن است</p></div>
<div class="m2"><p>ورنه این آیینه گیتی نمایی بیش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاد راهی گیر ای قصاب آمد وقت کوچ</p></div>
<div class="m2"><p>کاین همه معموره یک مهمان‌سرایی بیش نیست</p></div></div>