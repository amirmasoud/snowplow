---
title: >-
    شمارهٔ  ۲۳۵
---
# شمارهٔ  ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>در عشق عاقبت به بلا مبتلا شدیم</p></div>
<div class="m2"><p>تا پایبند آن سر زلف دوتا شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا آمدیم بر سر سودای دوستی</p></div>
<div class="m2"><p>دادیم نقد دل به تو و مبتلا شدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیگانگی ز مردم عالم ضرر نداشت</p></div>
<div class="m2"><p>از خود برآمدیم و به خلق آشنا شدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گیریم تا ز سفره افلاک توشه‌ای</p></div>
<div class="m2"><p>چون دانه خرد در دل این آسیا شدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دادیم صبر و هوش و گرفتیم داغ هجر</p></div>
<div class="m2"><p>تا همنشین به آن صنم بی‌وفا شدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ما کنند خلق تماشای عالمی</p></div>
<div class="m2"><p>در عشق همچو آینه گیتی‌نما شدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بردیم ره به عیش و گرفتیم کام دل</p></div>
<div class="m2"><p>قصاب چون‌که ما و تو بی‌مدعا شدیم</p></div></div>