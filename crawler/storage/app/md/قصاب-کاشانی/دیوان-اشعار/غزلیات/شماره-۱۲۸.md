---
title: >-
    شمارهٔ  ۱۲۸
---
# شمارهٔ  ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>مگر تیر جفای یار پر در بسترم دارد</p></div>
<div class="m2"><p>که امشب خواب راحت راه بر چشم ترم دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محبت این‌قدر دارد به قتلم کز پس مردن</p></div>
<div class="m2"><p>به جای خشت تیغش دست در زیر سرم دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا سوزاند و دست از دامن من دل برنمی‌دارد</p></div>
<div class="m2"><p>هنوز آن برق جولان کار با خاکسترم دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مراد دل بود پیمانه‌ای از گردش چشمش</p></div>
<div class="m2"><p>وگرنه این‌قدر خونی که خواهد ساغرم دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به قربان تو، بی‌کس نیستم در کنج تنهایی</p></div>
<div class="m2"><p>همان تیغ تو گاهی راه پایی بر سرم دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خار آشیان آن رشک طاووس از ره شوخی</p></div>
<div class="m2"><p>گهی در زیر پا گاهی به زیر شهپرم دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا چون سوختی بوی عبیر از کلبه‌ام بشنو</p></div>
<div class="m2"><p>همان خال تو دود عنبرین در مجمرم دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو باقی دار دیوانم به دور خطّ او قصاب</p></div>
<div class="m2"><p>که حسن کافرستانش حساب دفترم دارد</p></div></div>