---
title: >-
    شمارهٔ  ۲۲۵
---
# شمارهٔ  ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>ز حرف مدعی از الفت دلدار می‌ترسم</p></div>
<div class="m2"><p>چمن پرورده‌ام از دوری گلزار می‌ترسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارم قوّت دیدار ابروی کمانش را</p></div>
<div class="m2"><p>ز تیر غمزه آن ترک بی‌زنهار می‌ترسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان ما و بلبل در چمن فرقی که هست این است</p></div>
<div class="m2"><p>که او از خار و من از آن گل بی‌خار می‌ترسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به من تا گشت ظاهر کز وفا دوراند مهرویان</p></div>
<div class="m2"><p>همه از هجر و من از روز وصل یار می‌ترسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه شب می‌پرم پروانه‌سان بر گرد بالایش</p></div>
<div class="m2"><p>ولی از آرزوی حسرت دیدار می‌ترسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی‌گیرم ز لعلش بوسه‌ای قصاب تا دانی</p></div>
<div class="m2"><p>گیاه خشکم از آن آتش رخسار می‌ترسم</p></div></div>