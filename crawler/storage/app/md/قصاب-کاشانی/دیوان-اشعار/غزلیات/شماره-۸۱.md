---
title: >-
    شمارهٔ  ۸۱
---
# شمارهٔ  ۸۱

<div class="b" id="bn1"><div class="m1"><p>خال عنبربو جدا و خط مشکین‌مو جداست</p></div>
<div class="m2"><p>آنچه خوبان را بود در کار با آن دل‌رباست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرگس شهلا است یا چشم است یا آهوی چین</p></div>
<div class="m2"><p>زینت حسن است یا خال است یا مشک ختاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله سیراب یا ابر است یا قرص قمر</p></div>
<div class="m2"><p>هاله ماه است یا خطّ است یا دام بلاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دُرجِ مروارید یا لعل است یا عناب لب</p></div>
<div class="m2"><p>شربت قند است یا شهد است یا آب بقاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این قد و بالاست یا سرو است یا آشوب دهر</p></div>
<div class="m2"><p>جلوه قد است یا شمشاد یا صُنعِ خداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خنجر فولاد یا الماس یا نظاره است</p></div>
<div class="m2"><p>این به‌ دل ‌جا کرده مژگان است یا تیر قضاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌رسد با تیغ خون‌آلود آن شوخ از غضب</p></div>
<div class="m2"><p>عید قربان است ای قصاب یا روز جزاست</p></div></div>