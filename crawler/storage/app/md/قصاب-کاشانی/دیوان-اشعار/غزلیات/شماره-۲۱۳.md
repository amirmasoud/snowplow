---
title: >-
    شمارهٔ  ۲۱۳
---
# شمارهٔ  ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>مقصد عاشق همین یار است دنیا بر طرف</p></div>
<div class="m2"><p>زندگی بی یار دشوار است عقبا بر طرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌برد تا بندر صورت دل سرگشته را</p></div>
<div class="m2"><p>کار ما با چشم خون‌بار است دریا بر طرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگرفت از روی چون گل پرده بلبل شد خموش</p></div>
<div class="m2"><p>غبن، حسن فیض گلزار است غوغا بر طرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینقدرها جان من کردن ستم بر عاشقان</p></div>
<div class="m2"><p>نقص خوبی‌های دلدار است از ما بر طرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بپرسند از تو روز حشر خون‌دار تو کیست</p></div>
<div class="m2"><p>مصلحت قصاب انکار است دعوا بر طرف</p></div></div>