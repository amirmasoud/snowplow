---
title: >-
    شمارهٔ  ۳۴
---
# شمارهٔ  ۳۴

<div class="b" id="bn1"><div class="m1"><p>هر کجا عکس جمال یار می‌افتد در آب</p></div>
<div class="m2"><p>گویی از گلشن گل بی‌خار می‌افتد در آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز می‌دارد ز رفتن بحر را حیرت مگر</p></div>
<div class="m2"><p>سایه آن سرو خوش‌رفتار می‌افتد در آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک نگاهی کن که می‌گردد صدف را آب دل</p></div>
<div class="m2"><p>عکس این بادام تا از بار می‌افتد در آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوش دریا ماهیان‌ را سوخت از حسرت مگر</p></div>
<div class="m2"><p>پرتویی زآن آتشین رخسار می‌افتد در آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشه مرجان چو بید واژگون آشفته شد</p></div>
<div class="m2"><p>سایه زلفش ز بس بسیار می‌افتد در آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابر تا بگذشت از دریای آن گلزار حسن</p></div>
<div class="m2"><p>قطره چون گلگوشه دستار می‌افتد در آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دید تا قصاب چشمش را دلش در خون نشست</p></div>
<div class="m2"><p>شد چو طوفان، ناخدا ناچار می‌افتد در آب</p></div></div>