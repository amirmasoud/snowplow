---
title: >-
    شمارهٔ  ۱۹
---
# شمارهٔ  ۱۹

<div class="b" id="bn1"><div class="m1"><p>عشقت چو شمع سوخت سراپا تن مرا</p></div>
<div class="m2"><p>چون موم و رشته پیرهن و دامن مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوز درون گداخته از بس که جان من</p></div>
<div class="m2"><p>با هم شمرده تن نخ پیراهن مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من عندلیب گلشن تصویر گشته‌ام</p></div>
<div class="m2"><p>در کار نیست آب و هوا گلشن مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موری به کام دانه‌ای از حاصلم برد</p></div>
<div class="m2"><p>کو برق تا به باد دهد خرمن مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون آتشی که میل به خاشاک می‌کند</p></div>
<div class="m2"><p>عشق تو می‌کشد سوی خود دامن مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دیده باد‌دستی بی‌صرفه درگذار</p></div>
<div class="m2"><p>خالی مکن ز خون جگر معدن مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر از هما که طعمه شدش استخوان من</p></div>
<div class="m2"><p>پیدا نکرده است کسی مسکن مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من صیدم و رضا به قضای تو داده‌ام</p></div>
<div class="m2"><p>بیرون ز طوق خویش مکن گردن مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در واجبات عشق همین بس کز آب تیغ</p></div>
<div class="m2"><p>تعلیم داده دست ز جان شستن مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیدم تو را و دست و نگاهم ز کار رفت</p></div>
<div class="m2"><p>محروم ساخت وصل تو گل‌چیدن مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غیر از زبان که محرم غم‌خانه دل است</p></div>
<div class="m2"><p>قصاب پی نبرد کسی مخزن مرا</p></div></div>