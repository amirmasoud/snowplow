---
title: >-
    شمارهٔ  ۸۲
---
# شمارهٔ  ۸۲

<div class="b" id="bn1"><div class="m1"><p>تن چه باشد چاردیوار بنای عاریت</p></div>
<div class="m2"><p>پر مکن قصد اقامت در سرای عاریت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذر از این باغ بی‌‌رنگ تعلق چون نسیم</p></div>
<div class="m2"><p>دست و پا مگذار چون گل در حنای عاریت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک قدم منصور بالاتر نرفت از پای دار</p></div>
<div class="m2"><p>بیش از این کی می‌توان رفتن به پای عاریت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در قفا دارد کدورت آشنایی‌های خلق</p></div>
<div class="m2"><p>پر مشو حیران در این آیینه‌های عاریت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زبان جاده خاموشی گزین از هر سؤال</p></div>
<div class="m2"><p>درمرو از جای چون کوه از صدای عاریت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دل صد چاک از دنبال محمل چون جرس</p></div>
<div class="m2"><p>می‌کنم افغان و می‌آیم به پای عاریت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جهان یک‌ دم نمی‌گیرند از وحشت قرار</p></div>
<div class="m2"><p>دلنشین اهل همت نیست جای عاریت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوب گفتند اهل دل قصاب این درگشته را</p></div>
<div class="m2"><p>یک ده ویران و چندین کدخدای عاریت</p></div></div>