---
title: >-
    شمارهٔ  ۳۱۳
---
# شمارهٔ  ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>در زمان ما نمی‌بارد سحاب زندگی</p></div>
<div class="m2"><p>خشک گردیده‌ست در سرچشمه آب زندگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جفای بی‌حد ایام و گردش‌های دهر</p></div>
<div class="m2"><p>گشته کوته رشته عمرم ز تاب زندگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست آسایش به بزم دهر، در مینای تن</p></div>
<div class="m2"><p>هست باقی قطره‌ای تا از شراب زندگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با دو چشم خون‌چکان عمری است اندر آتشم</p></div>
<div class="m2"><p>نیست کس در عشق بیش از من کباب زندگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌رود با آنکه در یک روز و شب صدساله راه</p></div>
<div class="m2"><p>اضطراب ما بود بیش از شتاب زندگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر شرح نامرادی معنی دیگر نداشت</p></div>
<div class="m2"><p>درس هر فصلی که خواندیم از کتاب زندگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی توان آمد برون از زیر بار یک نفس</p></div>
<div class="m2"><p>وای اگر در حشر پرسندت حساب زندگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرح نتوان کرد پیش کس ز جور چرخ پیر</p></div>
<div class="m2"><p>آنچه ما دیدیم ز ایام شباب زندگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌توان قصاب گفتش در جهان رویین‌تن است</p></div>
<div class="m2"><p>هرکه می‌آرد در این ایام تاب زندگی</p></div></div>