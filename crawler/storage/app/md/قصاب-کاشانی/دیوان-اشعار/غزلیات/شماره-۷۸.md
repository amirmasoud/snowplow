---
title: >-
    شمارهٔ  ۷۸
---
# شمارهٔ  ۷۸

<div class="b" id="bn1"><div class="m1"><p>باد رنجور آن تنی کز درد او بیمار نیست</p></div>
<div class="m2"><p>خاک بر چشمی که با یاد رخش بیدار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌نصیب آن دل که زخم از تیر مژگانی نخورد</p></div>
<div class="m2"><p>وای بر مرگی که خود از حسرت دیدار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نگردم کشته در کوی تو با چند آرزو</p></div>
<div class="m2"><p>بر نمی‌گردم دگر این‌بار چون هر بار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پا ز فرمان قضا بیرون نهادن مشکل است</p></div>
<div class="m2"><p>هیچ‌کس را ره برون زین حلقه پرگار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نغمه‌سنجان حقیقت مست حیرت خفته‌اند</p></div>
<div class="m2"><p>در بساط عشق گویا هیچ‌کس هشیار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر گریزان نیستم از سنگ طبع ناکسان</p></div>
<div class="m2"><p>در جهان قصاب ما را شیشه‌ای در بار نیست</p></div></div>