---
title: >-
    شمارهٔ  ۴۲
---
# شمارهٔ  ۴۲

<div class="b" id="bn1"><div class="m1"><p>برده دل از من پری‌رویی نمی‌گویم که کیست</p></div>
<div class="m2"><p>شوخ چشمی طفل بدخویی نمی‌گویم که کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داده از زهر آب، بی رحمی، فرنگی زاده‌ای</p></div>
<div class="m2"><p>بهر قتلم تیغ ابرویی نمی‌گویم که کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو خال گوشه چشمش دلم افتاده است</p></div>
<div class="m2"><p>در قفای طرفه آهویی نمی گویم که کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به صبح امشب دماغم را پریشان کرده بود</p></div>
<div class="m2"><p>عطر زلف عنبرین بویی نمی‌گویم که کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد آن سبز ملیح کافر بیدادگر</p></div>
<div class="m2"><p>کنج لعلش خال هندویی نمی‌گویم که کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقبت قصاب قربان خواهدت کردن کسی</p></div>
<div class="m2"><p>عید قربان در سر کویی نمی‌گویم که کیست</p></div></div>