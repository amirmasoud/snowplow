---
title: >-
    شمارهٔ  ۳۰۷
---
# شمارهٔ  ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>بینا نیم آن لحظه که با ما تو نباشی</p></div>
<div class="m2"><p>بی دیده‌ام آن روز که پیدا تو نباشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای پادشه کون و مکان در دو جهان نیست</p></div>
<div class="m2"><p>یک سر که در آن مایه سودا تو نباشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ارض و سما کرده بسی سیر و ندیدیم</p></div>
<div class="m2"><p>یک ذرّه کز آن ذرّه هویدا تو نباشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کعبه و بتخانه به هر جا که گذشتیم</p></div>
<div class="m2"><p>جایی نرسیدیم که آنجا تو نباشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ویران شود این خانه ز سیلاب حوادث</p></div>
<div class="m2"><p>ای وای اگر مونس دل‌ها تو نباشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی بانگ تو دیگر نگشایم در دل را</p></div>
<div class="m2"><p>هر چند که گویند مبادا تو نباشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دوست در این بحر خطرناک چه سازم</p></div>
<div class="m2"><p>آن لحظه که فریادرس ما تو نباشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهدی کن و از گریه گلابی به کف آور</p></div>
<div class="m2"><p>ای دیده مثال گل زیبا تو نباشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصاب رفیقی چو غمش در دو جهان نیست</p></div>
<div class="m2"><p>جهدی که در این بادیه تنها تو نباشی</p></div></div>