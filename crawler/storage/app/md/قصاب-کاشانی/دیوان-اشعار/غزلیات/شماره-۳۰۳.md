---
title: >-
    شمارهٔ  ۳۰۳
---
# شمارهٔ  ۳۰۳

<div class="b" id="bn1"><div class="m1"><p>نفس در سینه‌ام چون ناله تار است پنداری</p></div>
<div class="m2"><p>بساط عشرتم گرم از دل زار است پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برافکن پرده از رخسار و بنما ماه تابان را</p></div>
<div class="m2"><p>که بی روی تو روزم چون شب تار است پنداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفتم چون سر زلف تو از کف رفت ایمانم</p></div>
<div class="m2"><p>به دستم هر سر موی تو زنّار است پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تعلیم فلاطون خاطرم راضی نمی‌گردد</p></div>
<div class="m2"><p>دلم در کج‌‌مزاجی طفل بیمار است پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر جا می‌روم دست از دل من برنمی‌دارد</p></div>
<div class="m2"><p>در این محنت نصیبی‌ها غمم یار است پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جای سبزه ز آب دیده ما لاله می‌روید</p></div>
<div class="m2"><p>مدار کشت ما با چشم خون‌بار است پنداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برو قصاب زین دردی که ما داریم تا محشر</p></div>
<div class="m2"><p>تو را آه و مرا این ناله در کار است پنداری</p></div></div>