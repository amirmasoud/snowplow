---
title: >-
    شمارهٔ  ۱۹۰
---
# شمارهٔ  ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>می‌کند بی‌گنه‌ام هر نفس آن یار قصاص</p></div>
<div class="m2"><p>شکر لله که دلم دید ز دیدار قصاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسته را نیست توانایی آزار کسی</p></div>
<div class="m2"><p>می‌کند چشم توام تا شده بیمار قصاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوار اگر جور و جفا نیست، عجب حیرانم</p></div>
<div class="m2"><p>که چرا می‌کندم آن گل بی‌خار قصاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راست‌رو باش که از روز بد ایمن باشی</p></div>
<div class="m2"><p>کج‌روی‌ها است که می‌بیند از آن مار قصاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل پرکینه ز سوهان بدی‌‌ها است برنج</p></div>
<div class="m2"><p>نیست دور ار کشد آیینه ز زنگار قصاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایمن از سنگ حوادث بود افتاده به راه</p></div>
<div class="m2"><p>می‌کشد، ماند هر آن میوه که در بار قصاص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکوه از گردش ایام چه داری قصاب</p></div>
<div class="m2"><p>می‌کشد در همه جا طالب دیدار قصاص</p></div></div>