---
title: >-
    شمارهٔ  ۱۹۶
---
# شمارهٔ  ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>دل به دهر حیله‌گر دادم غلط کردم غلط</p></div>
<div class="m2"><p>رفت ذوق هستی از یادم غلط کردم غلط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرمن عمری که جمع آورده بودم سال‌ها</p></div>
<div class="m2"><p>داد دهر سفله بربادم غلط کردم غلط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آرزوی کوی شیرین کوه عصیان روزگار</p></div>
<div class="m2"><p>بست بر گردن چو فرهادم غلط کردم غلط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یادگار از من به غیر از معصیت چیزی نماند</p></div>
<div class="m2"><p>نفس کافر بود استادم غلط کردم غلط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چمن عمری ندانستم که اصل جلوه چیست</p></div>
<div class="m2"><p>من همان در فکر شمشادم غلط کردم غلط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاری از من برنیامد تا به کار آید مرا</p></div>
<div class="m2"><p>بود چون در دل غمت شادم غلط کردم غلط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با هزاران ماجرا دارد همان طول امل</p></div>
<div class="m2"><p>زین جهان قصاب دل‌شادم غلط کردم غلط</p></div></div>