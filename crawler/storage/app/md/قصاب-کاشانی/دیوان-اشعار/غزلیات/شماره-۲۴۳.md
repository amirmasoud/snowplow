---
title: >-
    شمارهٔ  ۲۴۳
---
# شمارهٔ  ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>شبی ای مه به پابوست رسیدن آرزو دارم</p></div>
<div class="m2"><p>دو بیت از لعل جان‌بخشت شنیدن آرزو دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشینی در بساط دل چو شمع و من چو پروانه</p></div>
<div class="m2"><p>تو را یکسر به گرد سر پریدن آرزو دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرت گردم کمان تارت از بس چاشنی دارد</p></div>
<div class="m2"><p>لب زخم خدنگت را مکیدن آرزو دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلستان است سر تا پایت ای غارتگر دل‌ها</p></div>
<div class="m2"><p>گل وصلی از این گلزار چیدن آرزو دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من لب تشنه قربانت شوم در وادی هجران</p></div>
<div class="m2"><p>ز تیغت شربت آبی چشیدن آرزو دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شمع زنده در بزم وصالت تا سحر سوزان</p></div>
<div class="m2"><p>سر خود را به پای خویش دیدن آرزو دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه می‌خواهد دگر قصاب یک شب در سر کویش</p></div>
<div class="m2"><p>تپیدن جان بدون آرمیدن آرزو دارم</p></div></div>