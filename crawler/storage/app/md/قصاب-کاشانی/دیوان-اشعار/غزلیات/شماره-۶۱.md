---
title: >-
    شمارهٔ  ۶۱
---
# شمارهٔ  ۶۱

<div class="b" id="bn1"><div class="m1"><p>می‌کنم طوفی نمی‌دانم که طوف کوی کیست</p></div>
<div class="m2"><p>هست محرابی نمی‌دانم خم ابروی کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهسواری گردنم را در کمند آورده است</p></div>
<div class="m2"><p>می‌کشد هرسو نمی‌دانم سر گیسوی کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شعله‌ای در جان نهان دارم ز حسن سرکشی</p></div>
<div class="m2"><p>حیرتی دارم که این آتش ز عکس روی کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست یک دل کز خدنگ غمزه خون‌آلود نیست</p></div>
<div class="m2"><p>این کمان ناز حیرانم که در بازوی کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوی مشگی زین گلستان بر مشامم می‌رسد</p></div>
<div class="m2"><p>باز باد آشفته‌ساز زلف عنبربوی کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم ز مجنون می‌گریزد هم ز لیلی می‌رمد</p></div>
<div class="m2"><p>من نمی‌دانم که آن وحشی‌نگه آهوی کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوختی قصاب عمری شد، ندانستی چه سود</p></div>
<div class="m2"><p>کاین همه گرمی ز تاب قهر آتش‌خوی کیست</p></div></div>