---
title: >-
    شمارهٔ  ۱۴۲
---
# شمارهٔ  ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>بیا که بی گل رویت دلم قرار ندارد</p></div>
<div class="m2"><p>کسی به غیر تو در خاطرم گذار ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ماه یک‌شبه زرد و ضعیف در نظر آید</p></div>
<div class="m2"><p>چو هاله هرکه تو را تنگ در کنار ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار حیف که دیرآشنا و سست‌وفایی</p></div>
<div class="m2"><p>وگرنه چون تو گلی باغ روزگار ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خلف وعده‌ات ای مه چنین به من شده ظاهر</p></div>
<div class="m2"><p>که وعده‌های تو چون عمر اعتبار ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کدام وقت که قصاب تا به صبح شب هجر</p></div>
<div class="m2"><p>چو شمع دیده سوزان و اشگبار ندارد</p></div></div>