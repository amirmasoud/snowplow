---
title: >-
    شمارهٔ  ۴۴
---
# شمارهٔ  ۴۴

<div class="b" id="bn1"><div class="m1"><p>عالمی را سوختی از جلوه ای رعنا بس است</p></div>
<div class="m2"><p>بردی از حد ناز ای بی‌رحم استغنا بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز انتظار ضربت تیغ تو مردن تا به کی</p></div>
<div class="m2"><p>چند ای قاتل کنی امروز را فردا بس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگری را در میان زنّار ای بدخود مبند</p></div>
<div class="m2"><p>چون مرا کردی در این بتخانه پابرجا بس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیگری را در گرفتاری شریک ما مکن</p></div>
<div class="m2"><p>مدعا گر شهرت حسن است یک رسوا بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خنجر دیگر برای قتل من در کار نیست</p></div>
<div class="m2"><p>تیغ ابروی تو بر جان من شیدا بس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم بر خُم‌خانه گردون ندارم در خمار</p></div>
<div class="m2"><p>بهر درد سر مرا دُرد تو در مینا بس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بحر بی‌پایان ما را نیست امید کنار</p></div>
<div class="m2"><p>دست و پا تا چند ای قصاب در دریا بس است</p></div></div>