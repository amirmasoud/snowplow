---
title: >-
    شمارهٔ  ۲۷۴
---
# شمارهٔ  ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>مرا که نیست دمی روح در بدن بی تو</p></div>
<div class="m2"><p>حرام باد دگر زندگی به من بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جستجوی تو بعد از وفات خواهم گشت</p></div>
<div class="m2"><p>به گرد خویش چو فانوس در کفن بی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجاست جلوه قدت که سرو در چشمم</p></div>
<div class="m2"><p>بود چو دود که برخیزد از چمن بی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دست کوته و بی‌حاصلی چه سازم پس</p></div>
<div class="m2"><p>سزد اگر زنم آتش به خویشتن بی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شعله سوز دیارم ز غربت افزون است</p></div>
<div class="m2"><p>چو شمع چند توان سوخت در وطن بی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پای تا به سرم هم چو شمع زآتش عشق</p></div>
<div class="m2"><p>بسوخت ریشه جان و گداخت تن بی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم تو داری و من دارم از ازل غم تو</p></div>
<div class="m2"><p>ز دست عشق مرا قفل بر دهن بی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مباد گفته قصاب بعد از این رنگین</p></div>
<div class="m2"><p>اگر رود به زبانش دمی سخن بی تو</p></div></div>