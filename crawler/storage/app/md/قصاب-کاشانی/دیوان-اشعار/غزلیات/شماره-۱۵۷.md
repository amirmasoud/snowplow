---
title: >-
    شمارهٔ  ۱۵۷
---
# شمارهٔ  ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>صبح وصال را اثر آمد هزار شکر</p></div>
<div class="m2"><p>شام فراق را سحر آمد هزار شکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبر رسید خرم و می خورد و بوسه داد</p></div>
<div class="m2"><p>نخل مراد بارور آمد هزار شکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون آفتاب از ره روزن به روی ماه</p></div>
<div class="m2"><p>ناخوانده‌ام به کلبه درآمد هزار شکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌کردم از سپهر سراغ هلال عید</p></div>
<div class="m2"><p>ابروی یار در نظر آمد هزار شکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چار موج دجله اضداد کشتی‌ام</p></div>
<div class="m2"><p>بیرون ز ورطه خطر آمد هزار شکر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طفل سرشگ تا سر مژگان ز دل دوید</p></div>
<div class="m2"><p>این نورسیده با جگر آمد هزار شکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قصاب داغ زلف سیاهی به دل رسید</p></div>
<div class="m2"><p>امشب عزیزم از سفر آمد هزار شکر</p></div></div>