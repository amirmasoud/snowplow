---
title: >-
    شمارهٔ  ۱۲۴
---
# شمارهٔ  ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>لب‌تشنه نیاز چو بی‌تاب می‌شود</p></div>
<div class="m2"><p>از آب تیغ ناز تو سیراب می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق در این محیط خطرناک چون حباب</p></div>
<div class="m2"><p>در یک نفس ز شوق تو نایاب می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن کس که بر کشاکش این بحر تن نهاد</p></div>
<div class="m2"><p>چون موج طوق گردن گرداب می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دورم به هرکجا که نشینم به یاد او</p></div>
<div class="m2"><p>از سیل گریه دجله خوناب می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدن رخت دوباره میسر نمی‌شود</p></div>
<div class="m2"><p>زیرا که هر که دید تو را آب می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بزم خاص باده‌پرستان شوق تو</p></div>
<div class="m2"><p>دوری که هست قسمت قصاب می‌شود</p></div></div>