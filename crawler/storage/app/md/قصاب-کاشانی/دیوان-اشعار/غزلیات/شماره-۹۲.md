---
title: >-
    شمارهٔ  ۹۲
---
# شمارهٔ  ۹۲

<div class="b" id="bn1"><div class="m1"><p>رفت شب تا پرده از رخسار بگشاید صباح</p></div>
<div class="m2"><p>بر رخ بلبل در گلزار بگشاید صباح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر دهد تا پرده شب حق و باطل را به هم</p></div>
<div class="m2"><p>عقده را از سبحه و زنار بگشاید صباح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنگ شب را مهر بردارد ز مرآت سپهر</p></div>
<div class="m2"><p>پرده شرم از رخ اسرار بگشاید صباح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مکافاتش دو در بر روی می‌بندد فلک</p></div>
<div class="m2"><p>بر رخ هرکس دری یک ‌بار بگشاید صباح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌دهد در یک نفس ایام بر باد فنا</p></div>
<div class="m2"><p>غنچه‌ای را چون به صد آزار بگشاید صباح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچه از خجلت نیارد از گریبان سر برون</p></div>
<div class="m2"><p>در چمن بند قبا چون یار بگشاید صباح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌نشیند تا به شب قصاب در خون جگر</p></div>
<div class="m2"><p>چون نظر بر تارک دلدار بگشاید صباح</p></div></div>