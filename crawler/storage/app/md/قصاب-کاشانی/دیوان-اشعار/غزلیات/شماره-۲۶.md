---
title: >-
    شمارهٔ  ۲۶
---
# شمارهٔ  ۲۶

<div class="b" id="bn1"><div class="m1"><p>ای خرم از نسیم وصالت بهارها</p></div>
<div class="m2"><p>گلچین بوستان رخت گلعذارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشجار باغ را به نظر گر درآوری</p></div>
<div class="m2"><p>نرگس به جای برگ بروید زخارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنمای رخ به ما که ز حد رفت کار دل</p></div>
<div class="m2"><p>گیرد قرار تا دل ما بی‌قرارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل کارخانه‌ای است رگ و ریشه پود و تار</p></div>
<div class="m2"><p>تا دیده‌ای گسسته ز هم پود و تارها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطرب به نغمه کوش که یک‌دم غنیمت است</p></div>
<div class="m2"><p>تا پرده بر نخاسته از روی کارها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آب رفت نوح و تو در خواب غفلتی</p></div>
<div class="m2"><p>بستند همرهان همه بر ناقه بارها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عالم تمام یک شب و یک روز بیش نیست</p></div>
<div class="m2"><p>قصاب چند سیر کنی در دیارها</p></div></div>