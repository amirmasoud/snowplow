---
title: >-
    شمارهٔ  ۱۳۶
---
# شمارهٔ  ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>هرکجا آیینه رخسار او پیدا شود</p></div>
<div class="m2"><p>طوطی تصویر از شوق رخش گویا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادن جان و گرفتن داغ او نقش من است</p></div>
<div class="m2"><p>دیده کج‌بین اگر بگذارد این سودا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته بودی می‌کنم امشب تو را قربان خویش</p></div>
<div class="m2"><p>این شب وصل است، می‌ترسم دگر فردا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکل بسیار در پیش ره است ای همرهان</p></div>
<div class="m2"><p>کو غم او تا در این ره حل مشکل‌ها شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذرد گر بر مزار کشتگان ناز خویش</p></div>
<div class="m2"><p>لوح بر خاک شهیدانش ید بیضا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌توان از الفت دریادلان گشتن بزرگ</p></div>
<div class="m2"><p>قطره باران چو بر دریا رسد دریا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سینه را خواهم ز پیش دل گذارم بر کنار</p></div>
<div class="m2"><p>از قفس دیوار چون برداشتی صحرا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در گلستان چون نماید آن گل رخسار را</p></div>
<div class="m2"><p>تا به رویش دیده نرگس واکند شهلا شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر به رویت بسته شد قصاب این در، باک نیست</p></div>
<div class="m2"><p>سر بنه بر آستان دوست تا در واشود</p></div></div>