---
title: >-
    شمارهٔ  ۱۵۵
---
# شمارهٔ  ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>پس از دیدار قاصد چون فتادم دیده بر کاغذ</p></div>
<div class="m2"><p>شد افشان بس که اشک حسرتم پاشیده بر کاغذ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تنگی‌های جا دارد درون سینه از شوقش</p></div>
<div class="m2"><p>نفس را بر دلم چون رشته پیچیده بر کاغذ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کند تا در جواب نامه‌اش حرف وفا پیدا</p></div>
<div class="m2"><p>تهی شد از نگه بس دیده‌ام گردیده بر کاغذ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کبوتر را به پر چسبیده مکتوبش ز شیرینی</p></div>
<div class="m2"><p>ز شوخی بس لبش بر حال من خندیده بر کاغذ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شرح نامه‌ام حرف محبت در میان گم شد</p></div>
<div class="m2"><p>نگاه آن دل‌ربا می‌کرد تا دزدیده بر کاغذ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شوقش قاصد آتش هر قدم در زیر پا دارد</p></div>
<div class="m2"><p>تو پنداری سپند از خال او پاشیده بر کاغذ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نباشد دور اگر چون برگ گل از یکدگر پاشد</p></div>
<div class="m2"><p>به رنگ غنچه از بس نام او بالیده بر کاغذ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جواب نامهٔ آن شوخ را قصاب خونین دل</p></div>
<div class="m2"><p>حنایی کرده از بس روی خود مالیده بر کاغذ</p></div></div>