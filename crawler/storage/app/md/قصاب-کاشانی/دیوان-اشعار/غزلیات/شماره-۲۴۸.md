---
title: >-
    شمارهٔ  ۲۴۸
---
# شمارهٔ  ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>کارم ز عقل راست نشد بر جنون زدم</p></div>
<div class="m2"><p>سنگی به شیشه فلک واژگون زدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگین نشد ز گریه مردانه چهره‌ام</p></div>
<div class="m2"><p>پیمانه را ز میکده دل به خون زدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنیاد هستی‌ام ز نگاهی به باد رفت</p></div>
<div class="m2"><p>تا چون حباب خیمه به دریای خون زدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برخواستم ز آتش شوق تو چون سپند</p></div>
<div class="m2"><p>گرم آن‌چنان که نعره ندانم که چون زدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصاب برنخاست صدا از یک آشنا</p></div>
<div class="m2"><p>چندان که حلقه بر در دنیا دون زدم</p></div></div>