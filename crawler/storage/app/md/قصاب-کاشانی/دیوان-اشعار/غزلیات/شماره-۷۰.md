---
title: >-
    شمارهٔ  ۷۰
---
# شمارهٔ  ۷۰

<div class="b" id="bn1"><div class="m1"><p>آن کس که بار عشق به آهی کشیده است</p></div>
<div class="m2"><p>باشد چنانچه کوه به کاهی کشیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون صید زخم‌خورده من دل‌شکسته را</p></div>
<div class="m2"><p>چشم تو بر زمین به نگاهی کشیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر شکست دل مژه‌ها بسته‌اند صف</p></div>
<div class="m2"><p>یا پادشاه غمزه سپاهی کشیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آفتاب عارضت آمد بریر زلف</p></div>
<div class="m2"><p>دل خویش را به طرفه پناهی کشیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی تو را گرفته خط سبز در میان</p></div>
<div class="m2"><p>یا هاله گرد عارض ماهی کشیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصاب دهر سفله تو را بهر پایمال</p></div>
<div class="m2"><p>چون خار جاده بر سر راهی کشیده است</p></div></div>