---
title: >-
    شمارهٔ  ۲۰۹
---
# شمارهٔ  ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>تنها نه دل لاله کباب است در این باغ</p></div>
<div class="m2"><p>مرغ سحری در تاب و تاب است دراین باغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر برگ گلی دست حنابسته ساقی اس</p></div>
<div class="m2"><p>هر گل قدحی پر ز شراب است در این باغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جنبش اشجار چمن موجه دریا است</p></div>
<div class="m2"><p>هر غنچه سربسته حباب است در این باغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سرو خدنگی است که رو کرده به افلاک</p></div>
<div class="m2"><p>هر ریشه او بال عقاب است در این باغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر برگ گل و سبزه که بر خاک فتاده است</p></div>
<div class="m2"><p>از باد صبا مست و خراب است در این باغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستانه گل و لاله و شمشاد برقصند</p></div>
<div class="m2"><p>اطراف چمن عالم آب است در این باغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر روی عروسان گل و لاله ز شبنم</p></div>
<div class="m2"><p>هر قطره که بینی تو گلاب است در این باغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قصاب در این غلغله هشیار کسی نیست</p></div>
<div class="m2"><p>خندیدن تو کفر و غذاب است در این باغ</p></div></div>