---
title: >-
    شمارهٔ  ۱۶۹
---
# شمارهٔ  ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>تار قانون جهان ساز نگردد هرگز</p></div>
<div class="m2"><p>به کس این ساز هم‌آواز نگردد هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای بیرون منه از خویش که مرغ تصویر</p></div>
<div class="m2"><p>زخمی چنگل شهباز نگردد هرگز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رمز خون‌ریزی مژگان تو دل داند و بس</p></div>
<div class="m2"><p>دیگری آگه از این راز نگردد هرگز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست عمری که به قربان سرت می‌گردم</p></div>
<div class="m2"><p>مرغ دل مانده ز پرواز نگردد هرگز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به توکل فکن این کار که با ناخن سعی</p></div>
<div class="m2"><p>گره افتد چو به دل باز نگردد هرگز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پاک طینت نکشد زحمت سوهان قصاب</p></div>
<div class="m2"><p>مهر محتاج بپرداز نگردد هرگز</p></div></div>