---
title: >-
    شمارهٔ  ۳۱۴
---
# شمارهٔ  ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>بر رخ هر آرزو در بند تا محرم شوی</p></div>
<div class="m2"><p>دیده پوش از سیر باغ خلد تا آدم شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌کند از ترک لذت موم جا در چشم داغ</p></div>
<div class="m2"><p>دل ز وصل انگبین بردار تا مرهم شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زد حباب از خودنمایی خیمه از دریا برون</p></div>
<div class="m2"><p>چون صدف پستی گزین تا با گهر همدم شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آرزوی مور نه بر دیده انگشت قبول</p></div>
<div class="m2"><p>تا توانی چون سلیمان صاحب خاتم شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سربلندی بایدت، افتاده‌ای را دست گیر</p></div>
<div class="m2"><p>مرده‌ای احیا نما تا عیسی مریم شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بندبند استخوانم همچو نی دارد فغان</p></div>
<div class="m2"><p>خواهی‌ام کردن نوازش گر به من همدم شوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشته آن غمزه‌ام قصاب چون، خاک تو را</p></div>
<div class="m2"><p>جام اگر سازند جا دارد که جام جم شوی</p></div></div>