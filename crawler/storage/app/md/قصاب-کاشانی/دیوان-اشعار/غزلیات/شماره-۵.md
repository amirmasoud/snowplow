---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>نهاده حسن تو بنیاد دلربایی را</p></div>
<div class="m2"><p>گرفته گل ز رخت بوی بی‌وفایی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی نیافته قدر برهنه پایی را</p></div>
<div class="m2"><p>خراج نیست در این ملک بی‌نوایی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسا نمی‌شود از سعی خامه تقدیر</p></div>
<div class="m2"><p>به قد آن که بریدند نارسایی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خون دل ثمری جلوه ده در این گلزار</p></div>
<div class="m2"><p>چو سرو چند کنی پیشه خودنمایی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز زهد خشک به تنگ آمدم شراب کجاست</p></div>
<div class="m2"><p>که تا به آب دهم خرقهٔ ریایی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رضا به عشرت عالم نمی‌شود قصاب</p></div>
<div class="m2"><p>کسی که یافت چو من لذت جدایی را</p></div></div>