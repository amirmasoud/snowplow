---
title: >-
    شمارهٔ  ۲۰۵
---
# شمارهٔ  ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>ای قدّ تو چون معنی برجسته مصرع</p></div>
<div class="m2"><p>ابروی تو دیوان قضا را شده مطلع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخشیده صدف را کف نیسان تو هرگز</p></div>
<div class="m2"><p>گردانده چمن را نم فیض تو مخلّع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بهر تو شد دفتر ایام مرتب</p></div>
<div class="m2"><p>در شأن تو هرجای کتابی است مسجّع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مهر بود خشت حریم تو نمودار</p></div>
<div class="m2"><p>چون عرش بود شمسه ایوان تو ارفع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیچید سر اگر رشته ایام ز امرت</p></div>
<div class="m2"><p>از تیغ هلاکش کند افلاک مقطّع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از روی ادب تا نکشد پا به حریمت</p></div>
<div class="m2"><p>خورشید نشیند سر کوی تو مربّع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بهر یدک تا کشد از پیش تو دوران</p></div>
<div class="m2"><p>افکند فلک غاشیه بر زین مرصّع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرجا که رود منقبتی از شه مردان</p></div>
<div class="m2"><p>قصاب در آنجا تو ز جان باش مسمّع</p></div></div>