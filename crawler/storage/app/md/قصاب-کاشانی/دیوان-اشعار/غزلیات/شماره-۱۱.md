---
title: >-
    شمارهٔ  ۱۱
---
# شمارهٔ  ۱۱

<div class="b" id="bn1"><div class="m1"><p>می‌رساند از ره ظلمت به منزل مور را</p></div>
<div class="m2"><p>آنکه پنهان در دل هر ذره دارد نور را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وادی عشق است و اول ترک هستی گفته‌ام</p></div>
<div class="m2"><p>کرده‌ام بر خویشتن نزدیک راه دور را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نگردد از رفوکاری جراحت‌های دل</p></div>
<div class="m2"><p>بخیه بی‌نفع است زخم کاری ناسور را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهر چشمش را هجوم گریه‌ام در کار بود</p></div>
<div class="m2"><p>تلخی بادام او می‌خواست آب شور را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کج‌روان را راستی باید که گردد دستگیر</p></div>
<div class="m2"><p>می‌شود رهبر عصا در وقت رفتن کور را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظالمان را آتشی جز حرص نتواند گداخت</p></div>
<div class="m2"><p>شعله‌ای باید که سوزد خانه زنبور را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با سفال خویش هر کس می‌تواند ساختن</p></div>
<div class="m2"><p>می‌زند بر فرق قیصر کاسه فغفور را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پنجه مژگان او قصاب چون بربود دل</p></div>
<div class="m2"><p>همچو شهبازیست کز جا بر کند عصفور را</p></div></div>