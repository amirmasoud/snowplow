---
title: >-
    شمارهٔ  ۲۶۲
---
# شمارهٔ  ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>داغ دل را کرد از چاک آشکارا پیرهن</p></div>
<div class="m2"><p>عاقبت در عشق ما را ساخت رسوا پیرهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنگ در آغوشش آوردن نصیب ما نشد</p></div>
<div class="m2"><p>نامسلمان شانه، کافر سرمه، ترسا پیرهن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگذر از حق بی تو هر شب تا سحر در سوختن</p></div>
<div class="m2"><p>می‌کند امدادها چون شمع با ما پیرهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشئه کیفیت معنی ز لفظ نازک است</p></div>
<div class="m2"><p>باده را پوشند می‌خواران ز مینا پیرهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صدق پیش آور که از اعجاز خوبان دور نیست</p></div>
<div class="m2"><p>دیده یعقوب را گر ساخت بینا پیرهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سینه شد قصاب چون گل چاک‌چاک از دست صبر</p></div>
<div class="m2"><p>بر رخ دل عاقبت بگشود درها پیرهن</p></div></div>