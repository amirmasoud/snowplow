---
title: >-
    شمارهٔ  ۱۷
---
# شمارهٔ  ۱۷

<div class="b" id="bn1"><div class="m1"><p>پیچ و تاب زلف مشکینت شده زنجیرها</p></div>
<div class="m2"><p>بند بر پا مانده در زنجیر زلفت شیرها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردم افزون شد نمی‌دانم ز عشقت چون کنم</p></div>
<div class="m2"><p>با وجود آن‌که کردم در غمت تدبیرها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون خلاصی کس تواند یافتن از کوی او</p></div>
<div class="m2"><p>نقش پای مور را بر پا نهد زنجیرها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نیفتد پرتو حسن تو چون ظاهر شود</p></div>
<div class="m2"><p>دستکار صنعت نقاش بر تصویرها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخت بد قصاب او را دور می‌سازد ز تو</p></div>
<div class="m2"><p>گرچه بسیار از محبت دیده‌ام تأثیرها</p></div></div>