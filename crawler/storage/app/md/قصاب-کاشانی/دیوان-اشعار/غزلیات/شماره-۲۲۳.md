---
title: >-
    شمارهٔ  ۲۲۳
---
# شمارهٔ  ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>برنمی‌آید به کار کس ثبات بی‌محل</p></div>
<div class="m2"><p>مرگ به زندانیان را از حیات بی‌محل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبع اگر آزاده‌دل را نیست صرف لعل یار</p></div>
<div class="m2"><p>کم ز صندل نیست در لذت نبات بی‌محل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زود پیدا گشت خط و کشت ما را ز اضطراب</p></div>
<div class="m2"><p>گشت این معموره ویران از برات بی‌محل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانب اغیار رو کردن ز خوبان بدنما است</p></div>
<div class="m2"><p>نیست کمتر از گنه دادن ز کوه بی‌محل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مفلس عاصی است ز ارباب جهان قهارتر</p></div>
<div class="m2"><p>ظلم بسیار است اکثر دردهات بی‌محل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس را سرکش نمودن از ملامت خوب نیست</p></div>
<div class="m2"><p>کم ز کشتن نیست قصاب این نجات بی‌محل</p></div></div>