---
title: >-
    شمارهٔ  ۲۱۴
---
# شمارهٔ  ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>گر نخواهی سازدت در بزم بی‌مقدار حرف</p></div>
<div class="m2"><p>تا توان خاموش گردیدن مزن بسیار حرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرف بی‌جا گر دعا باشد چو سنگ تفرقه است</p></div>
<div class="m2"><p>گل به‌در زد بس‌که بلبل زد در این گلزار حرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حرف حق خوب است اما صرفه در گفتار نیست</p></div>
<div class="m2"><p>کرد آخر در جهان منصور را بر دار حرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر دفع خرج دل‌ها را مرنجان چون برات</p></div>
<div class="m2"><p>تا توان بر خویشتن پیچید چون طومار حرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت سر برباد از بسیار گفتن خامه را</p></div>
<div class="m2"><p>رحم اگر بر خویشتن داری مزن بسیار حرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شوی ایمن ز جور کوهکن هموار باش</p></div>
<div class="m2"><p>از زبان تیشه کس نشنید ناهموار حرف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بگویم حرف دل می‌رنجد از من چشم یار</p></div>
<div class="m2"><p>کی توان گفتن به پیش مردم بیمار حرف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر همه قند مکرر بود حرفت خوب نیست</p></div>
<div class="m2"><p>پر مکن قصاب نزد اهل دل تکرار حرف</p></div></div>