---
title: >-
    مناجات شمارهٔ ۱۵۰
---
# مناجات شمارهٔ ۱۵۰

<div class="n" id="bn1"><p>الهی ای سزاوار ثنای خویش، ای شکر کنندهٔ عطای خویش ای شیرین نمایندهٔ بلای خویش، بنده به ذات خود از ثنای تو عاجز و بعقل خود از شناخت منّت تو عاجز و به توان خود از سزای تو عاجز.</p></div>