---
title: >-
    مناجات شمارهٔ ۶۸
---
# مناجات شمارهٔ ۶۸

<div class="n" id="bn1"><p>الهی دانایی ده که در راه نیفتیم و بینایی ده که در چاه نیفتیم.</p></div>