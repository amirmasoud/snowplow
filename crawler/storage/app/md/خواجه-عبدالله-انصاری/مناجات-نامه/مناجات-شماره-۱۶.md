---
title: >-
    مناجات شمارهٔ ۱۶
---
# مناجات شمارهٔ ۱۶

<div class="n" id="bn1"><p>الهی تو دوستان را بدشمنان می نمایی، درویشان را غم و اندوه دهی، بیمار کنی و خود بیمارستان کنی، درمانده کنی و خود درمان کنی، از خاک آدم کنی و با وی چندان احسان کنی، سعداتش بر سر دیوان کنی و بفردوس او را میهمان کنی، مجلسش روضهٔ رضوان کنی، نا خوردن گندم با وی پیمان کنی، و خوردن آن در علم غیب پنهان کنی، آنکه او را زندان کنی و سالها گریان کنی، جباری تو کار جباران کنی، خداوند تو کار خداوندان کنی، تو عتاب و جنگ همه با دوستان کنی.</p></div>