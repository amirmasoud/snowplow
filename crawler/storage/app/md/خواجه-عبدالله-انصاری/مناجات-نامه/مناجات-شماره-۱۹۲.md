---
title: >-
    مناجات شمارهٔ ۱۹۲
---
# مناجات شمارهٔ ۱۹۲

<div class="n" id="bn1"><p>الهی آنچه نا خواسته یافتنی است، خواهندهٔ آن کیست ؟ و آنچه از پاداش بر تر است پرسش در جنب آن چیست ؟ پس هر چه از باران منّت است بهار آن دمی است و دانش و کوشش محنت آدمی است.</p></div>