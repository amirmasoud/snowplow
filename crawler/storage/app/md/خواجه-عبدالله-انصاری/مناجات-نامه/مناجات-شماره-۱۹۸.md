---
title: >-
    مناجات شمارهٔ ۱۹۸
---
# مناجات شمارهٔ ۱۹۸

<div class="n" id="bn1"><p>الهی تو دوستان خود را به لطف پیدا گشتی تا قومی را بشراب انس مست کردی، قومی را به دریای دهشت غرق کردی، ندا از نزدیک شنوانیدی و نشان از دور دادی ف رهی را باز خواندی و آنگاه خود نهان گشتی از وراء پرده خود را عرضه کردی و به نشان بزرگی خود را جلوه نموده تا آن جوانمردانرا در وادی دهشت گم کردی، و ایشان را در بیتابی و بی توانی سر گردان کردی ف داور آن داد خواهان تویی و داد ده آن فریاد کنان تویی و دیت آن کشتگان تویی، تا آن گُم شده کی به راه آید و آن غرق شده کُجا به کران افتد، و آن جانهای خسته کُجا بیاسایند، و این قصهٔ نهانی را کی جواب آید و شب انتظار آنان را کی بامداد آید ؟</p></div>
<div class="b" id="bn2"><div class="m1"><p>یار از غم من خبر ندارد گویی</p></div>
<div class="m2"><p>یا خواب به من گُذر ندارد گویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاریک تر است هر زمانی شب من</p></div>
<div class="m2"><p>یارب شب من سحر ندارد گویی</p></div></div>