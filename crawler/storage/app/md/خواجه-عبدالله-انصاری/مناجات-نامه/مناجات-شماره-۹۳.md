---
title: >-
    مناجات شمارهٔ ۹۳
---
# مناجات شمارهٔ ۹۳

<div class="n" id="bn1"><p>الهی بحرمت ذاتی که تو آنی، بحرمت صفاتی که چنانی و بحرمت نامی که تتو دانی بفریاد رس که میتوانی.</p></div>