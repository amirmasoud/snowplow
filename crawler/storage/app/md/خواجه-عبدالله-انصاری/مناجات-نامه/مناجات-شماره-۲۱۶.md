---
title: >-
    مناجات شمارهٔ ۲۱۶
---
# مناجات شمارهٔ ۲۱۶

<div class="n" id="bn1"><p>الهی بقدر تو نادانم و سزای تو را ناتوانم در بیچارگی خود سرگردانم، روز بروز بر زیانم، چون منی چون بود چنانم، و از نگریستن در تاریکی بفغانم که بر هیچ چیز هست ما ندانند چشم بر روزی دارم که تو بمانی و من نمانم پس چون من کیست که آنروز به بینم پس ور به بینم فدایی آنم.</p></div>