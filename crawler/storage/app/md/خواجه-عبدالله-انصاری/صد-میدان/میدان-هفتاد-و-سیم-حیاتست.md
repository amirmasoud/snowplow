---
title: >-
    میدان هفتاد و سیم حیاتست
---
# میدان هفتاد و سیم حیاتست

<div class="n" id="bn1"><p>از میدان بصیرت میدان حیات زاید. قوله تعالی: «او من کان میتا فاحییناه». زندگانی دل سه چیزست. و هر دل که در آن ازین سه چیز چیزی در وی نیست مردارست: یکی زندگانی بیم است با علم، و دیگر زندگانی امید با علم، سیم زندگانی دوستی با علم. زندگانی بیم دامن مرد پاک دارد، و چشم وی بیدار، و راه وی راست. و زندگانی امید مرکب ببردارد، و زادش تمام، و راه نزدیک. و زندگانی دوستی قدر مرد بزرگ دارد، و سروی آزاد، و دل وی شاد. بیم بی‌علم بیم خارجیانست، و امید بی‌علم امید مرجیانست، و دوستی بی‌علم دوستی اباحتیانست. و آن علم علم حدست و شرط در بیم و امید و در دوستی.</p></div>