---
title: >-
    میدان چهل و ششم مواصلت است
---
# میدان چهل و ششم مواصلت است

<div class="n" id="bn1"><p>از میدان رغبت میدان مواصلت زاید. قوله تعالی: «واسجد واقترب». مواصلت سه چیزست: مواصلت عذر با پذیرفتکاری، و مواصلت جهد و از ملک یاری، و مواصلت دوست‌داری و از مولی پسندکاری. نشان مواصلت عذر ظاهر گشتن برکات در فعلست، و آرامش در خوی، و خوشی در دل. و نشان مواصلت جهد و توفیق ظاهر گشتن برکاتست بر اسرار، و قبول دلها، و استجابت دعاها. و نشان مواصلت دوست‌داری و از مولی پسندکاری ظاهر گشتن برکاتست بر انفاس، و بزرگی همت، و گشاد حکمت.</p></div>