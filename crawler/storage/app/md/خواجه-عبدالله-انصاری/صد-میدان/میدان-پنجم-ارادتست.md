---
title: >-
    میدان پنجم ارادتست
---
# میدان پنجم ارادتست

<div class="n" id="bn1"><p>از میدان فتوت میدان ارادت زاید. ارادت خواست است و مراد در راه بردن. قوله تعالی: «قل کل یعمل علی شاکلته». جمله ارادت سه است: اول ارادت دنیایی محض است، و دیگر ارادت آخرت محض، سیم ارادت حق محض، اما ارادت دنیایی محض آنست که قوله تعالی: « منکم من یرید الدنیا»، «تریدون عرض الدنیا»، «من کان یرید حرث الدنیا»، «من کان یرید العاجلة»، «من کان یرید الحیوة الدنیا و زینتها»، «ان کنتن تردن الحیوة الدنیا و زینتها». نشان آن سه چیزست: یکی در زیارت دنیا بنقصان دین راضی بودن، و دیگر از درویشان مسلمان اعراض کردن، دیگر حاجتهای خود را بمولی بحاجتهای دنیا افگندن. و ارادت آخرت محض آنست که گفت قوله تعالی: «من أراد الاخرة»، «من کان یرید حرث الاخرة». و نشان آن سه چیز سر: یکی درسلامت دین بنقصان دنیا راضی بودن، و دیگر مؤانست با درویشان داشتن، سیم حاجتهای خود بمولی به آخرت افگندن. و ارادت حق محض آنست که گفت قوله تعالی: «ان کنتن تردن اللّه و رسوله». و نشان آن سه چیزست: اول پای بر هر دو جهان نهادن، و از خلق آزادگشتن، و از خود باز رستن.</p></div>