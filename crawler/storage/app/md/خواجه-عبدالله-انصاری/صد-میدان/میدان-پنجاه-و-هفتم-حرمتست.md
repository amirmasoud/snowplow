---
title: >-
    میدان پنجاه و هفتم حرمتست
---
# میدان پنجاه و هفتم حرمتست

<div class="n" id="bn1"><p>از میدان تمکن میدان حرمت زاید. قوله تعالی: «ما لکم ترجون للّه وقار». حرمت آزرم داشتنست. آن سه قسم است: احترام خدمت را، و احترام ذکر را، و احترام سر را. احترام خدمت را سه نشانست: عین آنرا در دین شکوه داری، و بهدایت آن از مولی شادی کنی، و نفس خود را در آن بتقصیر متهم کنی و معیوب بینی. اما احترام ذکر را سه نشانست: سخن هزل را در ذکر حق نیامیزی، و در غیبت دل مولی را یاد نکنی، و بی وی خود را خوانی وی را خود باشی. اما احترام سر را سه نشانست: اگر می‌ترسی مهر نبری، و اگر امید داری خود حق نه بینی، و اگر گستاخی کنی تعظیم نگاه داری.</p></div>