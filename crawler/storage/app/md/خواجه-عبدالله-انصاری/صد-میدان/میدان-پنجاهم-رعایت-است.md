---
title: >-
    میدان پنجاهم رعایت است
---
# میدان پنجاهم رعایت است

<div class="n" id="bn1"><p>از میدان همت میدان رعایت زاید. قوله تعالی: «فما رعوها حق رعایتها». رعایت براستا کردنست و کوشیدن. رعایت اهل حق سه چیزست: همت را، و وقت را، و سر را. رعایت همت آنست که یقین را باظن بدل نکنی، و اهام را در وسواس نیامیزی، و فراست از تمیز جدا کنی. و رعایت وقت آنست که از علایق ننگ داری، و خویش را از اسباب دریغ داری، و هر نفسی بحق بند کنی. و رعایت سر آنست که خویشتن را بدست امانی ندهی، و در زرق خویش میانجی نه پیچی، و از حق طرفة‌العینی بخود باز نیآیی.</p></div>