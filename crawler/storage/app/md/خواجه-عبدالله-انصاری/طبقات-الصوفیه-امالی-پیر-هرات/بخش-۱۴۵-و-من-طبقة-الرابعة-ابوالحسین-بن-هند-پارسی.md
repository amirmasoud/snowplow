---
title: >-
    بخش ۱۴۵ - و من طبقة الرابعة ابوالحسین بن هند پارسی
---
# بخش ۱۴۵ - و من طبقة الرابعة ابوالحسین بن هند پارسی

<div class="n" id="bn1"><p>نام وی علی بن هندالقرشی از مهینان مشایخ پارس است و علماء ایشان، صحبت کرده با جعفر حداد و مه ازو چون عمرو و عثمان مکی و جنید و آن طبقه و او را احوالهاست قوی، و مقامات پاک داشته. او گفت: لیس حکم ما و صقنا حکم ما نزلنا هم وی گفت: که دلها اوعیه و ظروفست، و هر وعا و ظرف شایسه بود نوعی را از برادشتگان، اما دلهای دوست او بیرایهاء معرفت است، و دلهاء عارفان بیرایهاء محبت‌اند ودلهاء محبان بیرایهاء و آوندهاء شوق‌اند. و قلوب مشتاقان، اوعیهٔ انس‌اند. و هر حال ازاین احوال را ادابست، هر که آن ادبها بکار ندارد در وقتها، هلاک شود از انجا که نجات بیوسد.</p></div>
<div class="n" id="bn2"><p>شیخ الاسلام گفت: که کنیت ابوالادیان ابوالحسن است نام علی ابوالادیان است، کنیت کردند که در همه کیشها مناظره کردی و ایشان را بشکستی. حکایت مناظره کردن وی با جهود، و در آتش شدن و آتش بپای همه خاکستر کردند القصه.</p></div>
<div class="n" id="bn3"><p>شیخ الاسلام گفت: هر گه وی به حج خواستی رفت از خانهٔ خود لبیک زدی وزانجا احرام گرفتی. وقتی از حج باز آمد و زود لبیک بزد ویرا گفتند: سردی مکن اکنون باز آمدی، می لبیک زنی. گفت: این بار لبیک نه حج را می‌زنم، لبیک او را می‌زنم، سر یک هفته را برفت از دنیا.</p></div>
<div class="n" id="bn4"><p>گویند ابوالادیان بصریست. در ایام جنید٭ بوده و بابوسعید خراز صحبت کرده، عالم بوده، و صاحب لسان.</p></div>