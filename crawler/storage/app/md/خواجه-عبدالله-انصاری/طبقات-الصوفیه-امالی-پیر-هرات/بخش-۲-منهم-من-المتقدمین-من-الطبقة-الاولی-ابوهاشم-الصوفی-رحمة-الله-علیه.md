---
title: >-
    بخش ۲ - منهم من المتقدمین من الطبقة الاولی ابوهاشم الصوفی رحمة اللّه علیه
---
# بخش ۲ - منهم من المتقدمین من الطبقة الاولی ابوهاشم الصوفی رحمة اللّه علیه

<div class="n" id="bn1"><p>بکنیت معروفست. از شیخ الاسلام شنودم کرم اللّه وجهه کی گفت: اول کسی که او را صوفی گفتند بوهاشم صوفی است شیخ بوده بشام و باصل کوفیست و بکنیت معروفست، در ایام سفین ثوری بوده، و سفین ثوری گوید: لولا ابوهاشم الصوفی ما عرفت را دیدم. و مات سفین الثوری بالبصره سنه احدی و ستین و مائه، و ابراهیم بن ادهم ایضاً رحمت اللّه علیه.</p></div>
<div class="n" id="bn2"><p>و پیش از وی بزرگان بودند در زهد و ورع و معاملت نیکو در طریق توکل و طریق محبت. لکن این نام صوفی نخست ویرا گفته‌اند. و در قدیم طریق تصوف تنگ‌تر بوده است و بسط نشده بود، که روزگار نازکتر بود، و در سخن صاین‌تر بودند، کی ایشان در عاملت می‌کوشیدند، نه در بسیاری مقال و سخن. که ایشان متمکنان بودند لیکن در آخر تر در متأخران ولایت ظاهرتر گشت و سخن و دعوی عریض‌تر، کی مغلوبتر بودند، بی‌طاقت گشتند، و مضطر، در سکر و قلق و غلیان آنچه یافتند، سخن ظاهر کردند، و وغستن این طریق در طبقه ثانی بیشتر بود.</p></div>
<div class="n" id="bn3"><p>شیخ الاسلام گفت: کی منصور عمارد مشقی گوید، کی بوهشم صوفی بیمار بود بیاری مرگ. من در وی شدم، ویرا گفتم: خود را چون می‌یابی؟ گفت: بلاءِ عظیم می‌بینم، اما هوی مه از بلی است یعنی مهر. مه از بلاست یعنی بلا بزرگست اما در جنب مهر حقیر است. شیخ الاسلام گفت کرم اللّه وجهه: اربقدر هوی بلاستی هوا نیستی</p></div>
<div class="n" id="bn4"><p>وانشد نا الامام لبعضهم</p></div>
<div class="b" id="bn5"><div class="m1"><p>سلام رایح غادی</p></div>
<div class="m2"><p>علی ساکنة الوادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علی من حبه فرض</p></div>
<div class="m2"><p>علی الحاضر والبادی</p></div></div>
<div class="n" id="bn7"><p>انشدنا لبعضهم</p></div>
<div class="b" id="bn8"><div class="m1"><p>بقلبی شیٔ لست احسن وصفه</p></div>
<div class="m2"><p>علی «انه» ما کان فهو شدید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تمربه الایام لتلحب ذیلها</p></div>
<div class="m2"><p>فتبلی به الایام و هو جدید</p></div></div>
<div class="n" id="bn10"><p>لغیره</p></div>
<div class="b" id="bn11"><div class="m1"><p>و ما شرنی انی خلق من الهوی</p></div>
<div class="m2"><p>و لو ان لی ما بین شرق الی غرب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فان کان هذا الحب ذنبی الیکم</p></div>
<div class="m2"><p>فلا غفر الرحمن ذلک من ذنب</p></div></div>
<div class="n" id="bn13"><p>انشدناه ایضاً لغیره</p></div>
<div class="b" id="bn14"><div class="m1"><p>جورالهوی احسن من عدله</p></div>
<div class="m2"><p>و بخله اظرف من بذله</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لو سمح العشق لا صحا به</p></div>
<div class="m2"><p>لمات کل الخلق من اجله</p></div></div>
<div class="n" id="bn16"><p>آخر</p></div>
<div class="b" id="bn17"><div class="m1"><p>قد ذقت حلواً و ذقت مراً</p></div>
<div class="m2"><p>کلا هما فی الهوی یطیب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من کان فی حبه مربئاً</p></div>
<div class="m2"><p>فاننی فیه لا اریب</p></div></div>
<div class="n" id="bn19"><p>وقتی بوهشم، شریک نخعی دید کی‌بیرون آمد از سرای یحیی خالد گفت: اعوذ باللّه من علم لا ینفع قضا، کی وی قاضی بود، و جز ازو هم آرند این حکایت. و هم بوهشم گفت: لقع الجبال بالا برایسر من اخراج الکبر من القولوب. بسوزن کوه کندن آسانتر، از کبر از دل بیرون کردن.</p></div>
<div class="n" id="bn20"><p>شیخ سیروانی٭ گفته است: آخر ما یخرج من قلوب الصدیقین حب الریاسه. و شیخ الاسلام املا کرد بر ما از محمد بن الجنید کی گفت: الشهرة فتنة و کل یتمناها والخمول راحة و قل من یرضیحا. و این پس از وی گفته‌اند من المتأخرین.</p></div>
<div class="n" id="bn21"><p>شیخ الاسلام گفته قدس اللّه روحه: کی شیخ بوجعفر مرا گفت بدامغان نام وی محمد قصاب دامغانی، شاگرد شیخ بوالعباس قصاب آملی٭ رحمهم اللّه گفت، از با محمد طینی شنیدم: کی پیشین خانقاه صوفیان کی این طایفه را کرد ندا آنست کی برملهٔ شام کردند. سبب آن بود کی امیری بود ترسا، یک روزی بشکل رفته بود. در راه دو تن را دید ازین طایفه کی فراهم رسیدند، دست در آغوش یکدیگر کردند، پس آنجا فرو نشستند آنچه داشتند از خوردنی فرا پیش نهادند و بخوردند و برفتند. آن امیر ترسا یکی را از ایشان فرا خواند، کی آنچه دیده بود، ویرا خوش آمده بود، و آن الفت ایشان پرسید از وی که او کی بود؟ گفت ندانم. گفت ترا چه بود؟ هیچ چیز. گفت از کجا بود؟ گفت ندانم.</p></div>
<div class="n" id="bn22"><p>امیر گفت: پس این الفت چه بود کی شما را با یکدیگر بود؟ آن درویش گفت: که آن ما را طریقتست. گفت: شما را جای هست کی آنجا فراهم آیند؟ گفت نه. گفتن: من شما را جای کنم تا با یکدیگر آنجا فراهم آئید آن خانقاه رمله بکرد انشدنا الامام:</p></div>
<div class="b" id="bn23"><div class="m1"><p>خیردار حل فیها خیر اربابها الدیار</p></div>
<div class="m2"><p>و قدیماً وفق اللّه خیار الخیار</p></div></div>
<div class="n" id="bn24"><p>٭٭٭</p></div>
<div class="b" id="bn25"><div class="m1"><p>هی المعالم و الاطلال والدار</p></div>
<div class="m2"><p>دار علیها من الاحباب آثار</p></div></div>
<div class="n" id="bn26"><p>٭٭٭</p></div>
<div class="b" id="bn27"><div class="m1"><p>واحبها و احب منزلها الذی</p></div>
<div class="m2"><p>حلت به واحب اهل المنزل</p></div></div>