---
title: >-
    بخش ۱۲۶ - و من طبقة الثالثه ابوالحسین الوراق
---
# بخش ۱۲۶ - و من طبقة الثالثه ابوالحسین الوراق

<div class="n" id="bn1"><p>نام وی محمد بن سعد از مهینان مشایخ نشاپورست، از قدیمان اصحاب با عثمان٭ عالم بوده بعلم ظاهر و سخن گوی بدقایق علوم و معاملات و عیوب افعال. مات قبل العشرین و ثلثمائه. وی گفته: کرم آن بود در عفو که یاد نکنی جفاء یار خود، پس آنک عفو کردی. و هم وی گفت: که حیاة دل در یاد حی الذی لا یموتست، و عیش گوارنده زندگانیست باللّه تعالی جز ازونه.</p></div>
<div class="n" id="bn2"><p>و هم وی گفته: که علامت محبت اللّه متابعت دوست اوست رسول او .</p></div>