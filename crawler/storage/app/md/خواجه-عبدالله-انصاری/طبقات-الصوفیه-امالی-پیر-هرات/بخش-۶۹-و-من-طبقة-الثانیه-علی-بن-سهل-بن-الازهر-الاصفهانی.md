---
title: >-
    بخش ۶۹ - و من طبقة الثانیه علی بن سهل بن الازهر الاصفهانی
---
# بخش ۶۹ - و من طبقة الثانیه علی بن سهل بن الازهر الاصفهانی

<div class="n" id="bn1"><p>کنیه ابوالحسن، مردی بزرگ بوده، و از قدیمان مشایخ سپاهان، شاگرد محمد یوسف بنا٭ بود، از اقران جنید بوده، او را بوی مکاتبت و رسالت بود، با ابن معدان و بابوتراب نخشبی٭ صحبت کرده بود. عمر و عثمان مکی٭ را که می سی هزار درم وام برآمد بمکه، وی همه آنبداد بی‌‌آگاهی وی سفینه بمکه فرستاد القصه: علی سهل گوید: نه حلالست بسوی ما، کی این طایفه را درویشان خوانند، که ایشان توانگرین خلق‌اند.</p></div>
<div class="n" id="bn2"><p>شیخ الاسلام گفت: کی پادشاه کی جامهاءٍ نیکو فرا دنیا داران داد فرحامه فرا درویشان داد. و طعام پاکیزه فرا ایشان داد، مزه فرا ایشان داد. للمرشدی: یعیری قومی علی الملبس الدون الابیات.</p></div>
<div class="n" id="bn3"><p>علی سهل گفت: اعاذ نا اللّه و ایاکم من غرور حسن الاعمال مع فساد بواطن الاسرار. و هم وی گفت: التصوف التبری عن من دونه والتخلی عن من سواه و پرسیدند از وی از حقیقت توحید، گفت: بعید من الظنون قریب فی الحقایق.</p></div>
<div class="n" id="bn4"><p>و انشد لبعضهم:</p></div>
<div class="b" id="bn5"><div class="m1"><p>فقلت لاصحابی هی الشمس ضوئها</p></div>
<div class="m2"><p>قریب و لکن فی تناولها بعد</p></div></div>
<div class="n" id="bn6"><p>شیخ الاسلام گفت: که فرا علی سهل گفتند: که یاد داری روز بلی؟ گفت: چون ندارم، گویی که دی بود.</p></div>
<div class="n" id="bn7"><p>شیخ الاسلام گفت: که درین نقص است، صوفی رادی و فردا چه بود، آن روز هنوز شب نیامد، صوفی دران روز است، صوفی در وقتست او ابن الوقت، و او ابن الازل است. تو از پدر زادی و عارف از وقت، تو در خانه نشستی و عارف در وقت. تو در مرکب سواری و وی بر وقت. تو بندهٔ وقتی و عارف اشمنده «آشامنده» وقت. وقت جام اوست و تو اشمندهٔ «آشامنده» وقت. عارف و صوفی را دی و فردا نبود. او بوقت قایمست و بر وقت. موقوفست. صوفی در ازل خود بنشیده، و ان صوفی وقت او ایذ و نسبت او موجود او ایذ و صورت او حال او ایذ.</p></div>
<div class="n" id="bn8"><p>شیخ الاسلام گفت: که سهل علی یار او ایذ، کی در سرای عبداللّه مبارک٭ شد گفت: این کنیزکان مطرب چرا بر بام کردهٔ آراسته، چرا فرو نخوانی؟ ابن المبارک گفت: چنین کنم. چون بیرون شد. ابن المبارک گفت: که ویرا بگوشید و دریاوید که وی اکنون برود از دنیا، کی آنک او دید بر بام من حوران بوده‌اند، پذیرهٔ وی فرستادند از بهشت، کی بر بام من هیچ کنیزک نبود و وی دروغ نگوید، بگوشید زود. چون بیرون رفت از سرای، در حال جان بداد.</p></div>
<div class="n" id="bn9"><p>سهل علی مروزی پرسیدند: کی از نواختهاء اللّه کی بنده بدان بنوازد، کدام مه است؟ گفت فراغت. مصطفی گفت : نعمتان مغبون فیهما کثیر من الماس: الصحة والفراغ و این سهل علی گوید: الفراغ بلاء من البلایا. و آن چنان است، کی شیخ الاسلام گفت: کی کسی را تقوی برو نه غالب بود، ویرا شغل به از فراغ باشد تا از فراغت ویرا بلا نخیزد. چنانک گفته‌اند لقد جلب الفراغ علیک شغلا٭ و اسباب البلا من الفراغ</p></div>
<div class="n" id="bn10"><p>اما او که متقی بود، و ورع و دل دارد، ویرا فراغت ملک باشد بی‌بها و فراغت دل، خانهٔ صحبت حق باشد و درویشی دوکان این کار. ابن جریج گوید: هر که از راه طریق عزم نیست، او را ور زیادت روی نیست.</p></div>