---
title: >-
    بخش ۳۴ - و من طبقه الثانیه ایضا من المتقدمین شیخ ابوحمزه
---
# بخش ۳۴ - و من طبقه الثانیه ایضا من المتقدمین شیخ ابوحمزه

<div class="n" id="bn1"><p>البغدادی الیزاز، و هو من اقران سری السقطی ٭ نام وی محمد بن ابراهیم است. شیخ الاسلام گفت کی وی ببغداد بود، و همه بزرگان اندکی از وی روایت کنند، علماً حدیث، چون محمد بن سلام الجمحی و خیر نساج و بوبکر کتانی ٭ ایشان.</p></div>
<div class="n" id="bn2"><p>وی صحبت کرده بود باسری سقطی ٭ و از قرینان اوست و نسبت باحسن مسوحی ٭ کند و از رفیقان بوتراب نخشبی ٭ بوده در سفر. و گویند که از فرزندان عیسی بن ابان بوده، و با بشر حافی ٭ صحبت کرده و پیش از جنید و بوحمزه خراسانی ٭ برفته در سنه تسع و ثمانین و مائتین پس بوسعید خراز ٭ از استادان جنید بوده و عالم بقرأت و طریقت او نزدیک بوده از طریقت بشر حافی.</p></div>
<div class="n" id="bn3"><p>شیخ الاسلام گفت روزی از احمد بن حنبل چیزی پرسیدند و سخن رفتی در مجلس، احمد بوی اضافت کردی روزی چیزی پرسیدند احمد گفت شیخ بوحمزه را: اجب یا صوفی! بگوی یا صوفی! شیخ الاسلام گفت، کی شیخ بوعبداللّه با کوی ٭ گفت، که محمد بن احمد آملی گفت، کی بوبکر کتانی گفت، که شیخ بوحمزهٔ بغدادی گفت: لولا الغفلة لمات الصدیقون من روح ذکر اللّه و قربه. شیخ الاسلام گفت: که از یافت تو بر اندیشم از علم خود گریزم، بر زهرهٔ خود بترسم، در غفلت آویزم. و گفت: وقت بود که کسی مراد هزل و غفلت یکساعت مشغول دارد طمع دارم که از همه جرمها آزادی یابد از بارکی برمن بود، تا طرفی بر آسایم.</p></div>
<div class="n" id="bn4"><p>فرا شیخ بوعبداللّه خفیف گفتند: کی چرا عبدالرحیم اصطخری با سگبانان بدشت می‌رود؟ گفت: تا ازان بار وجود، کی بروست دم زند. شیخ الاسلام گفت: که لذت و خوشی در طلب است، دریافت خوشی نیست، دریافت صدمت است، کی ترا فرو می‌شکند. وله:</p></div>
<div class="b" id="bn5"><div class="m1"><p>وجدانکم فوق السرور</p></div>
<div class="m2"><p>و فقدکم فوق الحزن</p></div></div>
<div class="c"><p>و فی کلامه: ار الهی کسی طاقت تو نیارد، شگفت نیست، عتاب بازگیر، که ایذر جز از گفت نیست، یکی از طلب می‌نماید و یکی از یافت. ای جان آسای مولی! حق رهی از تو بنمی‌ماند، آه مادر تو می‌درماند. حمال از شکر کشیدن همچنان بماند که از شکار کشیدن. شادی بی‌اندازه از اندوه کشنده تراست، نه از دوست ملالست و نه درد است عیب ازین سوست که آهن سردست.</p></div>
<div class="n" id="bn6"><p>وانشد</p></div>
<div class="b" id="bn7"><div class="m1"><p>حملتم جبال الحب فوقی واننی</p></div>
<div class="m2"><p>لاعجزعن حمل القمیص واضعف</p></div></div>