---
title: >-
    بخش ۱۵۰ - و من طبقة الخامسة ایضاً ابوالحسن الصوفی البوشنجی
---
# بخش ۱۵۰ - و من طبقة الخامسة ایضاً ابوالحسن الصوفی البوشنجی

<div class="n" id="bn1"><p>نام وی علی بن محمد احمد بن سهل. از یگانگان جوانمردان خراسان بود باعثمان حیری دیده و بعراق بابوالعباس عطا و جریری صحبت کرده، و بشام طاهر مقدسی و بوعمر دمشقی صحبت کرده، و با شبلی مسایل گفته و عالم بوده بعلوم و معاملات نیکو، و نیکو طریقت در فتوت و تجرید وکوشنده درویشی را.</p></div>
<div class="n" id="bn2"><p>لکن شیخ الاسلام گفت: در کار وی نه دور فابوده مگر در عقیده ویرا خللی بوده یا خطائی در سخن واللّه اعلم. در سنة ثمان و اربعین و ثلثمائه برفته و از پوشنگ بوده و به نشاپور نشسته و جایگاه آنجا داشته،و طریقت صوفیان، نیکو دانسته، و سفرهاء نیکو کرده او ایذ که عهد کرده بود: که هر که مرا احتلام افتد، چیزی بدهم بدرویش. که آن از خلل افتد در لقمه یا اندیشهٔ نه راست. وقتی در بادیه بود، ویرا احتلام افتاد و تنها بود، ازار پای بیرون کرده و بر مغیلان افگند تا هر که فرا رسید فرا گیرد، وفا کردن عهد را.</p></div>
<div class="n" id="bn3"><p>ویرا پرسیدند از تصوف: گفت: اسم و لا حقیقة و قد کان قبل حقیقة ولا اسم. بوعثمان مغربی گوید: پرسیدند ویرا: که ظریف که بود؟ گفت: الخفیف فی ذاته و اخلاقه و افعاله و شما یله من عیر تکلف و سئل عنه ما المروة؟ قال حسن السر و قال من ذل فی نفسه رفع اللّه قدره، و من عزفی نفسه اذله اللّه فی اعین عباده</p></div>