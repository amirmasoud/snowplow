---
title: >-
    بخش ۷۶ - و من طبقة الثانیه ایضاً ابوعبداللّه السجزی
---
# بخش ۷۶ - و من طبقة الثانیه ایضاً ابوعبداللّه السجزی

<div class="n" id="bn1"><p>از مهینان مشایخ خراسان و فتیان ایشان با حفص حداد صحبت کرده و بادیه بریده بر توکل. وی گفته: که علامات اولیا سه چیز است: تواضع از بزرگ منشی و بزرگی حال، و زهد اندر اندک و انصاف از قوت. هم وی گفته است: هر واعظی که توانگر از مجلس وی نه درویش برخیزد، و درویش نه توانگر. وی نه واعظ است.</p></div>