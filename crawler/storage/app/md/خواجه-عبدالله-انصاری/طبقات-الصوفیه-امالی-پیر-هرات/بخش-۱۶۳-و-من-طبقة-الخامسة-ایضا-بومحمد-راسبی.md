---
title: >-
    بخش ۱۶۳ - و من طبقة الخامسة ایضاً بومحمد راسبی
---
# بخش ۱۶۳ - و من طبقة الخامسة ایضاً بومحمد راسبی

<div class="n" id="bn1"><p>نام وی عبداللّه بن محمد الراسبی بغدادیست از اجلهٔ مشایخ ایشان، صحبت کرده بابوالعباس. عطا و جریری٭ بشام، پس با بغداد آمد و آن جا برفته از دنیا در سنهٔ سبع و ستین و ثلثمائه.</p></div>
<div class="c"><p>قال السلمی: سمعت ابا محمد الراسبی یقول: القلب اذا امتحن بالتقوی نزع عنه حب الدنیا و حب الشهوات و اوقف علی المغیبات. و علی سعید ثقری گوید: که نزدیک بو محمد راسبی بودم، بنزدیک وی ذکر محبت می‌رفت وی گفت: المحبة اذا ظهرت افتضح فیها المحب، و اذاکتمت قتل المحب کیدا و انشدناه علی اثر ذلک:</p></div>
<div class="b" id="bn2"><div class="m1"><p>ولقد افارقه باظهار الهوی</p></div>
<div class="m2"><p>عمداً لیستر سره اعلانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و لربماکتم الهوی کشف اظهاره</p></div>
<div class="m2"><p>و لربما فضح الهوی کتمانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عن الحبیب لدی الحبیب بلاغة</p></div>
<div class="m2"><p>و لربما قتل البیغ لسانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کم قد راینا قاهراً سلطانه</p></div>
<div class="m2"><p>للناس کل محبه سلطانه</p></div></div>
<div class="n" id="bn6"><p>و قال ابومحمد: اعظم حجاب بینک و بین الحق اشتغالک بتدبیر نفسک او اعتمادک علی عاجز مثلک فی اسبابک. و هم وی گفت: الهموم عقوبات الذنوب. و قال: لا یکون الصوفی صوفیاً حتی لا تقله ارض و لا تظله سماء و لایکون له قبول عندالخلق و یکون مرجعه فی کل الاحوال الی الحق.</p></div>