---
title: >-
    بخش ۱۵۲ - در بیان آنکه این عالم ذره‌ایست از آن عالم. زیرا این محدود است و آن نامحدود. و آن عالمها همه انوار و آثار حق‌اند و قایم بحق‌اند و از انوار او زنده‌اند. چون از این عالم محدود بگذری آن عالم نامحدود را که در جوار حق است ببینی واللّه العالم
---
# بخش ۱۵۲ - در بیان آنکه این عالم ذره‌ایست از آن عالم. زیرا این محدود است و آن نامحدود. و آن عالمها همه انوار و آثار حق‌اند و قایم بحق‌اند و از انوار او زنده‌اند. چون از این عالم محدود بگذری آن عالم نامحدود را که در جوار حق است ببینی واللّه العالم

<div class="b" id="bn1"><div class="m1"><p>ذره ‌ ای نیست آسمان و زمین</p></div>
<div class="m2"><p>پیش آن آفتاب عل ّ یین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تو با ما ز جان و دل یاری</p></div>
<div class="m2"><p>چشم بگشا اگر بصر داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ببینی که صد هزار جهان</p></div>
<div class="m2"><p>که ندارند اول و پایان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد آن خور چو ذره گردان ‌ اند</p></div>
<div class="m2"><p>همه دایم بروش حیران ‌ اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این جهان سایه ‌ ای از آن طوبی است</p></div>
<div class="m2"><p>یا چو برگی ز گلشن عقبی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این جهان پرتوی است از تابش</p></div>
<div class="m2"><p>بلکه ز انجاست جمله اسبابش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این جهان از برای دوران است</p></div>
<div class="m2"><p>هرکه اینجا خوش است حیوان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وانکه انسان بود کند نقلان</p></div>
<div class="m2"><p>از جهان بدن بعالم جان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان فانی کند بحق ایثار</p></div>
<div class="m2"><p>تا بجای یکی برد دو هزار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عوض تن ز حق برد جانها</p></div>
<div class="m2"><p>عوض یک قراضه ‌ ای کانها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عوض خانه ‌ ای برد شهری</p></div>
<div class="m2"><p>عوض قطره ‌ ای برد نهری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اینچنین سود اگر ببازرگان</p></div>
<div class="m2"><p>برسیدی یقین شدی سلطان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از چنین سود چون گریزانی</p></div>
<div class="m2"><p>مگر این سود را نمیدانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر شدی جان تو از این آگاه</p></div>
<div class="m2"><p>ترک را دیده ‌ ای در این خرگاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تن تو خرگه است و در وی جان</p></div>
<div class="m2"><p>هست ترکی چو مه در آن پنهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر ببینی ورا در این خرگاه</p></div>
<div class="m2"><p>از سر دل چو ما شوی آگاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گنج در تست جوی در خود آن</p></div>
<div class="m2"><p>چون توئی جمله آشکار و نهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست چیزی ز تو برون میدان</p></div>
<div class="m2"><p>فاش کردند راز را مردان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عقل را ترک گوی و شو مجنون</p></div>
<div class="m2"><p>چون حجاب است رو سوی بیچون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفتگو چون حجاب راه تواست</p></div>
<div class="m2"><p>همچو ابری بپیش ماه نو است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست باید شدن از این هستی</p></div>
<div class="m2"><p>تا که بی می رسی در آن مستی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>محو باید شدن ز جسم و ز جان</p></div>
<div class="m2"><p>تا که گردی مقارن جانان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>توئی تو حجاب راه تو است</p></div>
<div class="m2"><p>گیر یک را و هل هر آنچه دواست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رنجها جمله از دوی و سوی است</p></div>
<div class="m2"><p>چون دوئی رفت راه عشق سوی است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نقش چون رفت آید آن معنی</p></div>
<div class="m2"><p>بی تو دایم بود روان معنی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در تن ای جان دگر نمیگنجم</p></div>
<div class="m2"><p>زانکه اندر نهان دو صد گنجم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گنج من بیحد است و بی پایان</p></div>
<div class="m2"><p>تن من همچو خاک بر سر آن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر تو بی من جمال من بینی</p></div>
<div class="m2"><p>قبله سازی مر او بگزینی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ننگری در مشایخ دیگر</p></div>
<div class="m2"><p>نشناسی بغیر من سرور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>لیک چه سود کز تو پنهانم</p></div>
<div class="m2"><p>سر بسر تو تنی و من جانم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نور جانم گرفت عالم را</p></div>
<div class="m2"><p>کرد روشن روان آدم را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بعد ازین هر که ماند او اغیار</p></div>
<div class="m2"><p>کافرش دان تو مؤمنش مشمار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جنس مردم نباشد آن حیوان</p></div>
<div class="m2"><p>سگ بو د در لباس آدمیان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جان او باشد از بخار و زخون</p></div>
<div class="m2"><p>قایم از چار عنصر آن ملعون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همچو کرمی بود که رست از خاک</p></div>
<div class="m2"><p>کی کند میل جانب افلاک</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>میل با ما کسی کند اینجا</p></div>
<div class="m2"><p>کش بود از ازل عطای خدا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جان او بی تن از شراب است</p></div>
<div class="m2"><p>خورده باشد وزان بود سرمست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جان او را بما بود خویشی</p></div>
<div class="m2"><p>زان کند میل سوی درویشی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خویشی جانها بود زانجا</p></div>
<div class="m2"><p>نیست فانی چو خویشی تنها</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چند روز است خویشی ابدان</p></div>
<div class="m2"><p>خویشی جانهاست جاویدان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عقل جز وی کجا رسد درما</p></div>
<div class="m2"><p>عقل کل چونکه خیره گشت اینجا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آنچه کس را نداده است خدا</p></div>
<div class="m2"><p>همه محصول ماست ای دانا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زانکه سلطان ما چنین فرمود</p></div>
<div class="m2"><p>چونکه در جلوه خویش را بنمود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که منم روح و زبدۀ آدم</p></div>
<div class="m2"><p>جوی از آدم چو اولیا آن دم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز اولیا نور نور نورم من</p></div>
<div class="m2"><p>از نظرها نهان و دورم من</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بقامات من کسی نرسد</p></div>
<div class="m2"><p>سر سلطان بهر خسی نرسد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شده حیران بروی ما عیسی</p></div>
<div class="m2"><p>جسته بر طور وصل ما موسی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هیچ موسی ز خضر شد آگاه</p></div>
<div class="m2"><p>گرچه بود او نبی و خاص آله</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در همه کارهای خضر انکار</p></div>
<div class="m2"><p>مینمود آن پیمبر مختار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زانکه از سر او نبود آگاه</p></div>
<div class="m2"><p>بود از او خفیه حالت آن شاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همچو او خضر عاشق رخ ماست</p></div>
<div class="m2"><p>مانده حیران نور فرخ ماست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>لیک سری خدای کرد پدید</p></div>
<div class="m2"><p>کو ز ما صد هزار چندان دید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که از او دیده بد کلیم کریم</p></div>
<div class="m2"><p>کرد اقرار و شد بعشق ندیم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کژی ما چو راستی او را</p></div>
<div class="m2"><p>برد تا صدر جنت المأوی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>قوت آن دان که هر چه بنمائی</p></div>
<div class="m2"><p>طالب خویش را بیفزائی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>برد از دردهای تو درمان</p></div>
<div class="m2"><p>هم پذیرد ز کفرها ایمان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کی کند سرکشی زر ستم شیر</p></div>
<div class="m2"><p>بر همه گرچه شیر باشد چیر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر ما شیر کم ز روباه است</p></div>
<div class="m2"><p>گرچه پیش وحوش خود شاه است</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همه شاهان گدای درگه ما</p></div>
<div class="m2"><p>برده هر یک ز ما هزار عطا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نیست دعوی گشای چشم و ببین</p></div>
<div class="m2"><p>همچو مردان ورای چرخ و زمین</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>در جهانی که جانهای شریف</p></div>
<div class="m2"><p>خوش بهمدیگرند یار و حریف</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>جان ما را چو قرص خور تابان</p></div>
<div class="m2"><p>بنگر آشکار نور افشان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همچنانکه ز نور خور عالم</p></div>
<div class="m2"><p>روشن است و بدید شه ز حشم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مینماید بدو سیاه و سپید</p></div>
<div class="m2"><p>فرق از او میشود چنار از بید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>آفتاب سپهر عالم جان</p></div>
<div class="m2"><p>دان که مائیم اندر این دوران</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گشت ازما جدا ولی ز عدو</p></div>
<div class="m2"><p>شبه از گوهر و بد از نیکو</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گرچه نورانی اند آن ارواح</p></div>
<div class="m2"><p>پیش این گوهرند کم ز اشباح</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بر این لطف چون تن ‌ اند کثیف</p></div>
<div class="m2"><p>همچنانکه خسیس پیش شریف</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>عقلهائی که رشگ املاک اند</p></div>
<div class="m2"><p>پیش این بحر پاک خاشاک اند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چون شهان حقیقتی در ما</p></div>
<div class="m2"><p>نرسیده ‌ اند و مانده ‌ اند جدا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>جمع کوران جمال حسن مرا</p></div>
<div class="m2"><p>گر نبینند دان که هست روا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>حق چو ما را بواصلان ننمود</p></div>
<div class="m2"><p>چون نماید بدین گروه حسود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>طمع خام جمع کوران بین</p></div>
<div class="m2"><p>که ندارند بوی صدق و یقین</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همه در چاه هست خود محبوس</p></div>
<div class="m2"><p>همه گشته ز جرمها منکوس</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>کی ببینند این خسان ما را</p></div>
<div class="m2"><p>چون ندیدند آن حسان ما را</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هر که ما را بدید او از ماست</p></div>
<div class="m2"><p>موج دریا یقین که از دریاست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اینچنین رمز اشارت است بدان</p></div>
<div class="m2"><p>صد هزاران بشارت است بدان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>که مریدم ز بحر من موجی است</p></div>
<div class="m2"><p>گرچه پ ست و گرچه در اوجی است</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>پس چو ما روز و شب بهم بودیم</p></div>
<div class="m2"><p>هرچه گفت او بصدق بشنودیم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ره بریدیم خوش ز گفتارش</p></div>
<div class="m2"><p>بسوی منزل پر انوارش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>دردتن گشته است صاف از وی</p></div>
<div class="m2"><p>زانکه مستیم دائما بی می</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>پس چرا لافها از او نزنیم</p></div>
<div class="m2"><p>هرچه خواهیم ما چرا نکنیم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>میرسد گر کنیم ما شاهی</p></div>
<div class="m2"><p>زیر و بالا ز ماه تا ماهی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>زانکه بنده شه است در تحقیق</p></div>
<div class="m2"><p>عین شه شد چو رست از تفریق</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>شخص اگرچه ز دست و پا و سر است</p></div>
<div class="m2"><p>دو سه مشمر ورا که یک بشر است</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>همچنین موجهای دریا بار</p></div>
<div class="m2"><p>گرچه باشند در عدد بسیار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>سر بر آورده هر طرف چپ و راست</p></div>
<div class="m2"><p>همه را یک ببین چو از یم خاست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>موجها همچو دستهای یم ‌ اند</p></div>
<div class="m2"><p>عین بحراند نی فزون نه کم ‌ اند</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>در عددشان مکن ز جهل نظر</p></div>
<div class="m2"><p>بین احد را و از عدد بگذر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نقش را ترک کن بمعنی رو</p></div>
<div class="m2"><p>از چئی در نقوش مرده گرو</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>صد نفر گر بهم رفیق شوند</p></div>
<div class="m2"><p>در ره حق همه بعشق روند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>همدگر را مدد کنند از جان</p></div>
<div class="m2"><p>از سر صدق و عشق روز و عیان</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>یک بوند آنهمه چو واجوئی</p></div>
<div class="m2"><p>رو بمعنی اگر از این گوئی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>هر که بگذشت نقش عالم را</p></div>
<div class="m2"><p>جست از جسم آدم آن دم را</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بر صور پشت کرد بی دعوی</p></div>
<div class="m2"><p>روی آورد جانب معنی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>فرش را از برای عرش گذاشت</p></div>
<div class="m2"><p>پرده ‌ ها را ز پیش خود برداشت</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>دوخت از غیر چشم خود چون باز</p></div>
<div class="m2"><p>تا که بر روی شه گشاید باز</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>جان و دل را ز آب و گل برکند</p></div>
<div class="m2"><p>خویش را در جهان جان افکند</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>روح را کرد همره ارواح</p></div>
<div class="m2"><p>رست از زحمت مساو صباح</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>رفت آنجا که مرگ راره نیست</p></div>
<div class="m2"><p>سوی آن چرخ کش خور و مه نیست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بلکه هم چرخ و هم خور و ماه است</p></div>
<div class="m2"><p>همه شه و هم امیر و اسپاه اوست</p></div></div>
<div class="b" id="bn102"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>