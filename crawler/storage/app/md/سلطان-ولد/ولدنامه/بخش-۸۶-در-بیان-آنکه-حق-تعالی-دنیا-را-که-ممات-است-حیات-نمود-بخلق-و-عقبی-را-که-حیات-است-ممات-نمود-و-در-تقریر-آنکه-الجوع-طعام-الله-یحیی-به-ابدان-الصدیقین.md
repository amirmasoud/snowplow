---
title: >-
    بخش ۸۶ - در بیان آنکه حق تعالی دنیا را که ممات است حیات نمود بخلق و عقبی را که حیات است ممات نمود. و در تقریر آنکه الجوع طعام اللّه یحیی به ابدان الصدیقین.
---
# بخش ۸۶ - در بیان آنکه حق تعالی دنیا را که ممات است حیات نمود بخلق و عقبی را که حیات است ممات نمود. و در تقریر آنکه الجوع طعام اللّه یحیی به ابدان الصدیقین.

<div class="b" id="bn1"><div class="m1"><p>بسط در قبض جوی ای جویا</p></div>
<div class="m2"><p>زندگی درگذار و مرگ و فنا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه مرگ است زندگیت نمود</p></div>
<div class="m2"><p>دمبدم رغبتت در آن افزود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وانچه آن زندگی جاوید است</p></div>
<div class="m2"><p>نفس تو زان نفور و نومید است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این جهان آخرش فنا و هباست</p></div>
<div class="m2"><p>وان جهان اصل زندگی و بقاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده ای ترک آن ز نادانی</p></div>
<div class="m2"><p>اندرین مانده ای ز بیجانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعل بین و اژگون میفت از اسب</p></div>
<div class="m2"><p>عکس آن را گزین گذر از کسب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گنج در رنج جو نه در راحت</p></div>
<div class="m2"><p>فسحت از سینه جونه از ساحت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بطلب هم طعام را در جوع</p></div>
<div class="m2"><p>باده و نقل و جام را در جوع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیر از جوع شو نه از بریان</p></div>
<div class="m2"><p>جهد کن تا ز غم شوی شادان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جوی در نیستی تو هستی را</p></div>
<div class="m2"><p>هم بجویی شراب مستی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خصم دین را بکش بخنجرلا</p></div>
<div class="m2"><p>تا رسی بی حجاب در الا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در رهش شو فنا که مانی تو</p></div>
<div class="m2"><p>گرد نادان که تا بدانی تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>توی تست اندک از بسیار</p></div>
<div class="m2"><p>بی توئی خود ترا کجاست کنار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>توی تست خ ارو دلبر گل</p></div>
<div class="m2"><p>توی تست جزو و دلبر کل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>توئی تست کف بر آن دریا</p></div>
<div class="m2"><p>نیست شو بازرو در آن دریا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از نبی راجعون شنو ای ای یار</p></div>
<div class="m2"><p>کفک بگذار و رو بدریا آر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کفک دریا یقین که از دریاست</p></div>
<div class="m2"><p>نقش جا بیگمان هم از بیجاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خنک آن صورتی که معنی شد</p></div>
<div class="m2"><p>بازگشت آنچنان که اول بد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فرع بود و باصل خود پیوست</p></div>
<div class="m2"><p>شاه گشت و ز بندگی وارست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دید خود را چنانکه اول بود</p></div>
<div class="m2"><p>گرچه اندر فراق احول بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جوهر عشق گشته بود عرض</p></div>
<div class="m2"><p>چشم او کور کرده بود غرض</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از غرض میشود هنر پنهان</p></div>
<div class="m2"><p>نی که یوسف نهان شد از اخوان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خو ب یش گشت از غرض مستور</p></div>
<div class="m2"><p>گرچه اندر جمال بد مشهور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنچنان حسنشان چو گرگ نمود</p></div>
<div class="m2"><p>زانکه هر یک پر از غرضها بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ذات قاضی چو گشت رشوت خوار</p></div>
<div class="m2"><p>پیش او هر عزیز باشد خوار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت ظالم نم ایدش چو شکر</p></div>
<div class="m2"><p>گفت مظلوم هرچه ناخوشتر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همچو حق عادل است قاضی راست</p></div>
<div class="m2"><p>آن چنان ذات بر فزود و نکاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مصطفی گفت عدل یک ساعت</p></div>
<div class="m2"><p>بهتر از شصت سالۀ طاعت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عدل گستر در این جهان امروز</p></div>
<div class="m2"><p>تا که فردا شوی شه پیروز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گردد از عدل اینجهان معمور</p></div>
<div class="m2"><p>هم شود جان در آن جهان مسرور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عدل تخم گزین بود میکار</p></div>
<div class="m2"><p>تا برش بدروی در آخر کار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خنک آن جان که تخم عدل بکاشت</p></div>
<div class="m2"><p>در جنان صد چنان عوض برداشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صدر جنت شود ورا مسکن</p></div>
<div class="m2"><p>نی ز خوف سقر در آن مأمن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عمر اندر جنان شود بیحد</p></div>
<div class="m2"><p>آنچنان عمر را نباشد عد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هست انواع طاعت اندر راه</p></div>
<div class="m2"><p>هر یکی را عوض رسد ز آله</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عدل را چونکه قدر بد افزون</p></div>
<div class="m2"><p>لاجرم اجر عدل شد افزون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرتبۀ عادلان چو هست اعلا</p></div>
<div class="m2"><p>پس برو در جهان تو عدل افزا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عدل خلق خداست در انسان</p></div>
<div class="m2"><p>ظلم باشد ز شیمت شیطان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون پری از صفات حق ای یار</p></div>
<div class="m2"><p>خویشتن را مگیر از اغیار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یار او خود توئی چه مینالی</p></div>
<div class="m2"><p>گنج اور ا همیشه حمالی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گذر از تن چو اندر او جانی</p></div>
<div class="m2"><p>زرجان را تو بوته و کانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چشمه را آب دان مخوانش خاک</p></div>
<div class="m2"><p>گرچه زاید ز خاک هست آن پاک</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رو بهل درد و گیر صافی را</p></div>
<div class="m2"><p>بهر وافی گذار جافی را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در اگر در حدث فتد ناگاه</p></div>
<div class="m2"><p>کی هلد در حدث ورا آگاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دست را در حدث کند بی او</p></div>
<div class="m2"><p>جویدش اندران حدث هر سو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آدمی کمتر از حدث نبود</p></div>
<div class="m2"><p>در ز نعت خدای به نشود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در دل او درآ در او کن جای</p></div>
<div class="m2"><p>مرم از صورتش بمعنی آی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>