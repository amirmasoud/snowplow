---
title: >-
    بخش ۶۷ - در بیان مصاحبت کردن چلبی حسام الدین قدس اللّه سره مدت ده سال تنگاتنگ با حضرت مولانا قدسنا اللّه بسره العزیز و یاران و اصحاب از حضرت هر دو بیحسدی مستفید شدن و بعد از آن نقل فرمودن حضرت مولانا قدسنا اللّه بسره العزیز.
---
# بخش ۶۷ - در بیان مصاحبت کردن چلبی حسام الدین قدس اللّه سره مدت ده سال تنگاتنگ با حضرت مولانا قدسنا اللّه بسره العزیز و یاران و اصحاب از حضرت هر دو بیحسدی مستفید شدن و بعد از آن نقل فرمودن حضرت مولانا قدسنا اللّه بسره العزیز.

<div class="b" id="bn1"><div class="m1"><p>بود با شیخ در زمانۀ شیخ</p></div>
<div class="m2"><p>همدل و همنشین بخانۀ شیخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صفا و وفا بهم همدم</p></div>
<div class="m2"><p>همه اصحاب شادمان بیغم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخشش هر دو بر همه شامل</p></div>
<div class="m2"><p>همه از ه ر دو عالم و عامل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه در باغ عشق چون اشجار</p></div>
<div class="m2"><p>شیخ و نایب در آن چو باد بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنده از آبشان نهال همه</p></div>
<div class="m2"><p>گشته خوب از وصال حال همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر یکی را بقدر خود ادرار</p></div>
<div class="m2"><p>دائماً میرسید بی آزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داده هر یک درخت شکل دگر</p></div>
<div class="m2"><p>میوه های لذیذتر ز شکر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک از آن تاب داده بر خرما</p></div>
<div class="m2"><p>یک بداده انار جان افزا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در عروج از بروج همچو ملک</p></div>
<div class="m2"><p>کرده هر یک گذر ز هفت فلک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش بهم بوده مدت ده سال</p></div>
<div class="m2"><p>پاک و صافی مثال آب زلال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بعد از آن نقل کرد مولانا</p></div>
<div class="m2"><p>زین جهان کثیف پر زعنا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پنجم ماه در جماد آخر</p></div>
<div class="m2"><p>بود تقلان آن شه فاخر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سال هفتاد و دو بده بعدد</p></div>
<div class="m2"><p>ششصد از عهد هجرت احمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چشم زخمی چنین رسید بخلق</p></div>
<div class="m2"><p>سوخت جانها ز صدمت آن برق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لرزه افتاد در زمین آن دم</p></div>
<div class="m2"><p>گشت نالان فلک در آن ماتم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مردم شهر از صغیر و کبیر</p></div>
<div class="m2"><p>همه اندر فغان و آه و نفیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دیهیان هم ز رومی و اتراک</p></div>
<div class="m2"><p>کرده ازدرد او گریبان چاک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بجنازه شده همه حاضر</p></div>
<div class="m2"><p>از سر مهر و عشق نز پی بر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اهل هر مذهبی بر او صادق</p></div>
<div class="m2"><p>قوم هر ملتی بر او عاشق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کرده او را مسیحیان معبود</p></div>
<div class="m2"><p>دیده او را جهود خوب چو هود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عیسوی گفته اوست عیسی ما</p></div>
<div class="m2"><p>موسوی گفته اوست موسی ما</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مؤمنش خوانده سرو نور رسول</p></div>
<div class="m2"><p>گفته هست او عظیم بحر نغول</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه کرده ز غم گریبان چاک</p></div>
<div class="m2"><p>همه از سوز کرده بر سر خاک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن فغان و خروش کانجا بود</p></div>
<div class="m2"><p>کس ندیده است زیر چرخ کبود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همچنان این کشید تا چل روز</p></div>
<div class="m2"><p>هیچ ساکن نشد دمی تف و سوز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بعد چل روز سوی خانه شدند</p></div>
<div class="m2"><p>همه مشغول این فسانه شدند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روز و شب بود گفتشان همه این</p></div>
<div class="m2"><p>که شد آن گنج زیر خاک دفین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ذکر احوال و زندگانی او</p></div>
<div class="m2"><p>ذکر اقوال و در فشانی او</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ذکر خلق لطیف بی مثلش</p></div>
<div class="m2"><p>ذکر خلق شریف بی مثلش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ذکر عشق خدا و تجریدش</p></div>
<div class="m2"><p>ذکر مستی و صدق و توحیدش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ذکر تنزیه او از این دنیا</p></div>
<div class="m2"><p>کلی رغبتش سوی عقبی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ذکر و ورد و نماز او همه شب</p></div>
<div class="m2"><p>ذکر تخصیص او بحضرت رب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ذکر لطف و تواضع و کرمش</p></div>
<div class="m2"><p>ذکر حال و سماع چون ارمش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ذکر تذکیر و وعظ و گرمی او</p></div>
<div class="m2"><p>ذکر مهر و وفا و نرمی او</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ذکر اسرار و لطف انوارش</p></div>
<div class="m2"><p>ذکر آن کشف ها ز دیدارش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ذکر تقوی و حلم و رحمت او</p></div>
<div class="m2"><p>ذکر فتوی و علم و حکمت او</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ذکر هر نوع از کرامت او</p></div>
<div class="m2"><p>در ره صدق استقامت او</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه در هر صفت ورا خوانند</p></div>
<div class="m2"><p>زانکه او را شفیع خود دانند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه نامش برند در سوگند</p></div>
<div class="m2"><p>همه از نام او رهند از بند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا نیارند نام او بزبان</p></div>
<div class="m2"><p>هیچ باور نگردد آن پیمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زانکه آن نام بهترین قسم است</p></div>
<div class="m2"><p>نقض آن پیششان بترزسم است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر بگویم از این نسق شب و روز</p></div>
<div class="m2"><p>دل عش ا ق خون شود از سوز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دل چون کوه که شود زین غم</p></div>
<div class="m2"><p>آن به آید کزیم ببندم دم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سوی قصه روم که از غصه</p></div>
<div class="m2"><p>برهند و برند از آن حصه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>