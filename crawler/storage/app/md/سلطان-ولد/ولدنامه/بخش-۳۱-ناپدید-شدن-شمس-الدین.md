---
title: >-
    بخش ۳۱ - ناپدید شدن شمس الدین
---
# بخش ۳۱ - ناپدید شدن شمس الدین

<div class="b" id="bn1"><div class="m1"><p>ناگهان گ م شد از میان همه</p></div>
<div class="m2"><p>تا رهد از دل اندهان همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک دو روز او چو گشت ناپیدا</p></div>
<div class="m2"><p>کرد افغان ز درد مولانا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعد از آن چون ورا بجد جستند</p></div>
<div class="m2"><p>سوی هر کوی و هر سرا جستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ ازوی ک سی نداد خبر</p></div>
<div class="m2"><p>نی بکس بو رسید از او نه اثر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ش یخ گشت از فراق او مجنون</p></div>
<div class="m2"><p>بی سر و پاز عشق چون ذوالنون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیخ مفتی ز عشق شاعر شد</p></div>
<div class="m2"><p>گشت خمار اگرچه زاهد بد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی ز خمری که او بود زانگور</p></div>
<div class="m2"><p>جان نوری نخورد جز می نور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>