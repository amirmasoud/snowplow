---
title: >-
    بخش ۴۲ - در بیان آنکه مولانا قدسنا اللّه بسره العزیز چون بولد عنایت داشت پیوسته بتعظیم اولیاء ترغیبش دادی
---
# بخش ۴۲ - در بیان آنکه مولانا قدسنا اللّه بسره العزیز چون بولد عنایت داشت پیوسته بتعظیم اولیاء ترغیبش دادی

<div class="b" id="bn1"><div class="m1"><p>پس ولد را بخواند مولانا</p></div>
<div class="m2"><p>گفت دریاب چون توئی دانا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر نهاد و سؤال کرد از او</p></div>
<div class="m2"><p>چیست مقصود از این ببنده بگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت بنگر رخ صلاح الدین</p></div>
<div class="m2"><p>که چه ذات است آن شه حق بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقتدای جهان جان است او</p></div>
<div class="m2"><p>ملک ملک لامکان است او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم آری ولیک چون تو کسی</p></div>
<div class="m2"><p>بیند او را نه هر حقیر و خسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت با من که شمس دین این است</p></div>
<div class="m2"><p>آن شه بی یراق و زین اینست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش من همان همی بینم</p></div>
<div class="m2"><p>غیر آن بحر جان نمی ‌ بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دل و جان کمین غلام ویم</p></div>
<div class="m2"><p>مست و بیخویشتن ز جام ویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچه فرمائیم کنم من آن</p></div>
<div class="m2"><p>هستم از جان مطیعت ای سلطان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت از این بس صلاح دین را گیر</p></div>
<div class="m2"><p>آن شهنشاه راستین را گیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نظرش کیمیاست بر تو فتد</p></div>
<div class="m2"><p>رحمت کبریاست بر تو فتد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بحر او قطره را گهر سازد</p></div>
<div class="m2"><p>زر کند خاک را چو بگدازد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل پژمرده را کند زنده</p></div>
<div class="m2"><p>بخشدت جان پاک پاینده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برهاند ترا ز مرگ و فنا</p></div>
<div class="m2"><p>برساند بتخت ملک بقا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کندت بر علوم سر دانا</p></div>
<div class="m2"><p>جمله اسرار از او شود پیدا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر زمینی تو آسمان گردی</p></div>
<div class="m2"><p>همچو جان سوی لامکان گردی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتمش من قبول کردم این</p></div>
<div class="m2"><p>که شوم بندۀ صلاح الد ّ ین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بکشم، خاک پاش در دیده</p></div>
<div class="m2"><p>تا از آن نور حق شود دیده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رو نهاده بوی بصدق و نیاز</p></div>
<div class="m2"><p>بندۀ او شدم بعشق و نیاز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کرد بر من نظر چو دید مرا</p></div>
<div class="m2"><p>هستم او را غلام در دو سرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مست گشتم نه از می انگور</p></div>
<div class="m2"><p>غرق شد جان و جسمم اندر نور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نی چنین غرق کو بود نقصان</p></div>
<div class="m2"><p>بل کمالی که نیست بر تر از آن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جان من بود قطره دریا شد</p></div>
<div class="m2"><p>دلم از پست سوی بالا شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فکرها در زمان مصور گشت</p></div>
<div class="m2"><p>روح صافی بشکل پیکر گشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>انبیا را بدید پیش نظر</p></div>
<div class="m2"><p>باسر و دست و پا چو نقش بشر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفته با هر یکی سخن بیدار</p></div>
<div class="m2"><p>با زبان و بصورت از اسرار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خلق دیگر مگر که اندر خواب</p></div>
<div class="m2"><p>زین ببینند اندکی چو سراب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>