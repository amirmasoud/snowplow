---
title: >-
    بخش ۷۹ - در تفسیر این آیت که فمنکم کافر و منکم مؤمن هم کفر و ایمان در تو مضمر است، و هم زمینی و هم آسمانی تا آخر الامر کدام صفت غالب شود که الحکم للغالب
---
# بخش ۷۹ - در تفسیر این آیت که فمنکم کافر و منکم مؤمن هم کفر و ایمان در تو مضمر است، و هم زمینی و هم آسمانی تا آخر الامر کدام صفت غالب شود که الحکم للغالب

<div class="b" id="bn1"><div class="m1"><p>در تو جمع است کفرو هم ایمان</p></div>
<div class="m2"><p>گشت مضمر فرشته هم شیطان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نبی گفت تا شوی موقن</p></div>
<div class="m2"><p>که توئی کافر و توئی مؤمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنگر زین دو کیست عالیتر</p></div>
<div class="m2"><p>سر که افزونتر است یا شکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کدامین که بر تو شد غالب</p></div>
<div class="m2"><p>از شمار و ل ی تو ای طالب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر میزان همین بود میدان</p></div>
<div class="m2"><p>نقد در خود ببین بنسیه ممان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غالب اندر درم چو نقره بود</p></div>
<div class="m2"><p>در شمار درم روانه بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور بود غالب درم مس بد</p></div>
<div class="m2"><p>پیش صراف خوار باشد و رد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس یقین شد که حکم غالب راست</p></div>
<div class="m2"><p>زانکه مغلوب از شمار فناست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبود این حدیث را آخر</p></div>
<div class="m2"><p>شرح کن تا چه کرد آن فاخر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>