---
title: >-
    بخش ۶۲ - در بیان آنکه چون ولد از قیل و قال عقلی و نقلی بگذشت جان او چون دریا بجوش آمد و امواج سخن از دل او جوشیدن گرفت
---
# بخش ۶۲ - در بیان آنکه چون ولد از قیل و قال عقلی و نقلی بگذشت جان او چون دریا بجوش آمد و امواج سخن از دل او جوشیدن گرفت

<div class="b" id="bn1"><div class="m1"><p>لب ببستم ز گفتگوی تمام</p></div>
<div class="m2"><p>پشت کردم بسوی فضل و کلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش آن بحر علم گوش شدم</p></div>
<div class="m2"><p>پس چو دریا از او بجوش شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونکه گشتم مقیم در خمشی</p></div>
<div class="m2"><p>از درون رو نمود بحر خوشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در وصلش درون آن دریا</p></div>
<div class="m2"><p>گشت رخشان چو آفتاب سما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه بود تاب آفتاب سما</p></div>
<div class="m2"><p>که بود شبه آن فروغ و ضیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش آن نور این بود ناری</p></div>
<div class="m2"><p>فرق میکن اگر نه اغیاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنهمه نور و این سراسر نار</p></div>
<div class="m2"><p>آن چو گلزار و این سراسر خار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن بود جان و این بود قالب</p></div>
<div class="m2"><p>آن بود روز و این بود چون شب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن چو دریا و این چو یک قطره</p></div>
<div class="m2"><p>آن چو خورشید و این چو یک ذره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بحر جان آسمان و آن در خور</p></div>
<div class="m2"><p>ت افته بی حجاب زیر و زبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نور او پر شده در آن دریا</p></div>
<div class="m2"><p>مثل آفتاب در صحرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر هارا نموده بس روشن</p></div>
<div class="m2"><p>خار هر روح گشته زو گلشن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باغها اندر او برون ز شمار</p></div>
<div class="m2"><p>میوه هاشان عزیز و با مقدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر سوئی حوریان فزون از ریگ</p></div>
<div class="m2"><p>آشها پخته دائما بی دیگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قصر های بلند هر طرفی</p></div>
<div class="m2"><p>هر خسی یافته در او شرفی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چار جویش روانه همچون تیر</p></div>
<div class="m2"><p>از می و آب و انگبین و ز شیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مطربانش بصد هزار الحان</p></div>
<div class="m2"><p>درسرود و رباب و چنگ زنان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاخ و برگ ثمارشان زنده</p></div>
<div class="m2"><p>همچو گل هر نبات در خنده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاخ با میوه در سلام و کلام</p></div>
<div class="m2"><p>سرو بابید در رکوع و قیام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زنده زانند آن حبوب و کروم</p></div>
<div class="m2"><p>که بری اند از خصوص و عموم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه ز اعمال نیک هست شدند</p></div>
<div class="m2"><p>خشت هر قصر را ز ذکر زدند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ذکر و ورد و نماز زندگی است</p></div>
<div class="m2"><p>صدق و سوز و نیاز زندگی است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زانکه از زندگی است بنیادش</p></div>
<div class="m2"><p>عمل زنده کرد آبادش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بس بود زنده هم تر و خشگش</p></div>
<div class="m2"><p>نطق زاید ز عود و از مشگش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بخلاف عمارت دنیا</p></div>
<div class="m2"><p>که جماد است اصل آن ز بنا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سنگ و خشتش جماد و بیجان ‌ اند</p></div>
<div class="m2"><p>نیک و بد را از آن نمیدانند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لاجرم این جهان بود مرده</p></div>
<div class="m2"><p>هر که ماند این طرف شد افسرده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صورت از بهر ماندن نامد</p></div>
<div class="m2"><p>دل بصورت چگونه آرامد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خیمۀ چرخ را اگر چه زدند</p></div>
<div class="m2"><p>بی ستون بر هوا عظیم بلند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چونکه نقش است صورت آخر کار</p></div>
<div class="m2"><p>نیست گردد نماندش آثار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گذر از نقش و جوی معنی را</p></div>
<div class="m2"><p>اصل گهر و گذار دعوی را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عمل تو بهشت تست بدان</p></div>
<div class="m2"><p>از جنان تو رسته است جنان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از صفا و وفا و صدق دلت</p></div>
<div class="m2"><p>رود اندر بهشت آب و گلت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آب و گل از عمل شود صافی</p></div>
<div class="m2"><p>همچو نادان ز عاقلی کافی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آب و گل را کنند صاف چو دل</p></div>
<div class="m2"><p>نی منی شد نگار خوب چگل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نی که شد دانه زیر خاک درخت</p></div>
<div class="m2"><p>میوه و برگ داد و شد پر رخت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هیچ ماند درخت با دانه</p></div>
<div class="m2"><p>یا که نطفه بمرد مردانه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دانه کی داشت شاخ و برگ و نوا</p></div>
<div class="m2"><p>شد هزار ار چه بود یک تنها</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>صد هزاران چنین درین صحرا</p></div>
<div class="m2"><p>کرد حق بر تو روشن و پیدا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تخم ریحان وسوسن و نسرین</p></div>
<div class="m2"><p>تخم کاهو و شلجم و یقطین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هیچ مانند با نبات بگو</p></div>
<div class="m2"><p>از که بردند آن صلات بگو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هم از آن کس رسد عطای تو نیز</p></div>
<div class="m2"><p>چیز گردی اگر شوی ناچیز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نیست این را کران بگو کان شاه</p></div>
<div class="m2"><p>چون نمودی بیار رهرو راه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>