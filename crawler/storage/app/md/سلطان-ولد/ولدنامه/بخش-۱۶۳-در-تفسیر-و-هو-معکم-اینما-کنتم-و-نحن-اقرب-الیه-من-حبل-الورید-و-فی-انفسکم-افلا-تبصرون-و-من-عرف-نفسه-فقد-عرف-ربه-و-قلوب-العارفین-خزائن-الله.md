---
title: >-
    بخش ۱۶۳ - در تفسیر و هو معکم اینما کنتم و نحن اقرب الیه من حبل الورید و فی انفسکم افلا تبصرون و من عرف نفسه فقد عرف ربه و قلوب العارفین خزائن اللّه.
---
# بخش ۱۶۳ - در تفسیر و هو معکم اینما کنتم و نحن اقرب الیه من حبل الورید و فی انفسکم افلا تبصرون و من عرف نفسه فقد عرف ربه و قلوب العارفین خزائن اللّه.

<div class="b" id="bn1"><div class="m1"><p>جوی در خود ورا چو جویانی</p></div>
<div class="m2"><p>خیره هر سوی از چه پویانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب لطفش ز جسم خاکی تو</p></div>
<div class="m2"><p>میکند جوش بهر پاکی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خودش جو چو از تو میروید</p></div>
<div class="m2"><p>بلکه خود او ترا همیجوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با تو است او مجوی جای دگر</p></div>
<div class="m2"><p>چشم بگشا و در خودت بنگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوی شیر اندرون تست روان</p></div>
<div class="m2"><p>شیرجوئی تو گاه از این گه از آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست با بحر متصل خم تو</p></div>
<div class="m2"><p>چند جوئی تو آب از هر جو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سله نان نهاده بر سر تو</p></div>
<div class="m2"><p>پارۀ نان طلب کنی هر سو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سر خویش پیچ اگر نه خری</p></div>
<div class="m2"><p>گرد خود گرد اگر نه خیره سری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لابه ها میکنی سرار و جهار</p></div>
<div class="m2"><p>کای خدا رو نما بمن یکبار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میدهد او جواب کای نادان</p></div>
<div class="m2"><p>از تو هرگز جدا نیم چون جان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر جدا گشتمی ز تو یکدم</p></div>
<div class="m2"><p>کی بماندی تن تو زنده بدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دمبدم از دم منی نالان</p></div>
<div class="m2"><p>چشم بگشا اگر نئی نادان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خود منم بر تو دائما بر کار</p></div>
<div class="m2"><p>از چه خفتی نمیشوی بیدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نی که فرمود ایزد ای جویان</p></div>
<div class="m2"><p>با توام دایم آشکار و نهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در درون و برون مرا مبین</p></div>
<div class="m2"><p>تا شود دیدن منت آئین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بلکه من چون بهارم و تو درخت</p></div>
<div class="m2"><p>از منستت حیات و زینت و رخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پری از من چو جام از باده</p></div>
<div class="m2"><p>از منی تو روانه در جاده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در کف من چو لعبتی دایم</p></div>
<div class="m2"><p>از منی قاعد از منی قایم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>راز و ناز و نیاز تو ز من است</p></div>
<div class="m2"><p>جنبش از جان بود چه گر ز تن است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روز و شب با توام نمی بینی</p></div>
<div class="m2"><p>گه ز من شاد و گاه غمگین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گاه کفری ز من گهی دینی</p></div>
<div class="m2"><p>گاه مهری ز من گهی کیلی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از منی زنده چون زیم ماهی</p></div>
<div class="m2"><p>چون نداری از این سر آگاهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زندگیئی که دادمت می بین</p></div>
<div class="m2"><p>کان منم وز من است در تو یقین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چشمها را گشا و منشین کور</p></div>
<div class="m2"><p>زاب شیرین بخور مخور از شور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چشم را دارد آب شور زیان</p></div>
<div class="m2"><p>زاب شیرین عشق شوریان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا شود چشم جان تو روشن</p></div>
<div class="m2"><p>تا شود نار بر تو چون گلشن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر طرف من ترا همیرانم</p></div>
<div class="m2"><p>نیست از مرکبت جدا رانم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اسب تو زیر ران و تو هر سو</p></div>
<div class="m2"><p>میدوی هر طرف که اسبم کو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفتن کو حجاب گشت ترا</p></div>
<div class="m2"><p>ورنه همچون خور است حق پیدا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در گذر از خیال ای رهرو</p></div>
<div class="m2"><p>تا رسی در وصال ای رهرو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>غیر اندیشه پردۀ ره نیست</p></div>
<div class="m2"><p>پرده افزاید آن که آگه نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر بمردان عشق بنشینی</p></div>
<div class="m2"><p>همچو جان اندرون خود بینی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گرچه با هر کسی نشینی تو</p></div>
<div class="m2"><p>بعد از آن غیر حق نبینی تو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در خس و کس ورا عیان بینی</p></div>
<div class="m2"><p>گاه پیدا و گه نهان بینی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هیچ چیزی نماندت مشکل</p></div>
<div class="m2"><p>چون که پیش از اجل شوی بسمل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر کجا رو نهی ورا بینی</p></div>
<div class="m2"><p>سر ها جمله بیخطا بینی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در که و کوه چون کنی تو نگاه</p></div>
<div class="m2"><p>پر شود چشم و سینه ‌ ات ز آله</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بحر رحمت شوی در این عالم</p></div>
<div class="m2"><p>ملکت سر نهند چون آدم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه ارواح را امیر شوی</p></div>
<div class="m2"><p>حاکم و نایب و وزیر شوی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جمله پیشت ز دل سجود کنند</p></div>
<div class="m2"><p>تا ز کان تو گنج عشق کنند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همه را علم و رتبت افزاید</p></div>
<div class="m2"><p>بسته هاشان تمام بگشاید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همه از تو برند درس و سبق</p></div>
<div class="m2"><p>همه خوانند بیحروف ورق</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>علم اسما چو شد ترا معلوم</p></div>
<div class="m2"><p>همه را هم ز تو شود مفهوم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>قدر تو بود از فلک افزون</p></div>
<div class="m2"><p>زان سجودت همیکنند اکنون</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون که پیشی بدانش و تقوی</p></div>
<div class="m2"><p>ملک و روح را دهی فتوی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>رهبر و رهنمای حق باشی</p></div>
<div class="m2"><p>بر همه درهای جان پاشی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اندر این باب یک حکایت خوب</p></div>
<div class="m2"><p>بشنو از من بصدق ای محبوب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از پدر مانده بود شخصی را</p></div>
<div class="m2"><p>زر و املاک و گونه گون کالا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همه را پاک خورد و مفلس ماند</p></div>
<div class="m2"><p>گریه میکرد و اشگها میراند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از خدا با هزار نوحه و سوز</p></div>
<div class="m2"><p>گنج بیرنج خواستی شب و روز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گفت در خواب هاتفی او را</p></div>
<div class="m2"><p>که سوی مصر تاز ای جویا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>داد او را نشان کوی و مقام</p></div>
<div class="m2"><p>گفت آن جایگه رسی تو بکام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پس ز بغداد سوی مصر روان</p></div>
<div class="m2"><p>گشت او بر امید گنج نهان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مفلس و بینوا بمصر رسید</p></div>
<div class="m2"><p>کس بنانی ورا نمیپرسید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شرم مانع همیشدش از خواست</p></div>
<div class="m2"><p>در مجاعت وجود را میکاست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چون که از حد گذشت گرسنگیش</p></div>
<div class="m2"><p>گفت تا چند باشد این تشویش</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همچو شبکوک شب روم بیرون</p></div>
<div class="m2"><p>بو که چیزی دهد مرا بیچون</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پای از خانه چونکه پیش نهاد</p></div>
<div class="m2"><p>ناگهانی عسس بر او افتاد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بگرفتش بزخم چوب که هان</p></div>
<div class="m2"><p>چه کسی زود گو مدار نهان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گفت بهر خدا دمی بگذار</p></div>
<div class="m2"><p>تا کنم واقفت از این اسرار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چون که بگذاشت حال خویش بگفت</p></div>
<div class="m2"><p>باعسس یک بیک نداشت نهفت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گفت او را عسس که خریده ‌ ای</p></div>
<div class="m2"><p>طالب این از آن سبب شده ‌ ای</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>دیده ‌ ام من هزار خواب چنین</p></div>
<div class="m2"><p>که ببغداد هست گنج دفین</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>در فلان کوی و در فلان خانه</p></div>
<div class="m2"><p>نفتادم بدام از آن دانه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تو عظیم احمقی که چندین ره</p></div>
<div class="m2"><p>بر یکی خواب کوفتی ز بله</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از عسس چون نشان گنج شنید</p></div>
<div class="m2"><p>گشت بر وی مقام گنج پدید</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گفت در خانۀ من است آن گنج</p></div>
<div class="m2"><p>احمقانه چه میکشم این رنج</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>باز از مصر رفت تا بغداد</p></div>
<div class="m2"><p>گنج در خانه یافت شد دلشاد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گرچه بیفایده بد آن سفرش</p></div>
<div class="m2"><p>ظاهرا لیک نیک بین اثرش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>که چسان فایده ‌ اش رسید ز سیر</p></div>
<div class="m2"><p>یافت در عین شر هزاران خیر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گر نکردی ز شهر خویش سفر</p></div>
<div class="m2"><p>کی شنیدی ز گنج خویش خبر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>رنجها در سفر اگرچه کشید</p></div>
<div class="m2"><p>عاقبت بین که چون بکام رسید</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چون پی رنج بیگمان گنج است</p></div>
<div class="m2"><p>آن طرف تاز کاندر آن رنج است</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نی که از رنجهای صوم و نیاز</p></div>
<div class="m2"><p>از حج و از زکوة و ذکر و نماز</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>میرسد آدمی بگنج درون</p></div>
<div class="m2"><p>عملش گرچه مینمود برون</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>همچو آن شخص کاو ز خانه و شهر</p></div>
<div class="m2"><p>تا نیامد برون نبرد آن بهر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>زان سفر گنج یافت در خانه</p></div>
<div class="m2"><p>گشت فارغ ز خویش و بیگانه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گنج تونیز هم بخانۀ تست</p></div>
<div class="m2"><p>لیک در جستنش نگشتی چست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>خیره سر روی هر طرف آری</p></div>
<div class="m2"><p>چشم با خویشتن نمیداری</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تن تو خانه گنج نور خدا</p></div>
<div class="m2"><p>نور در خویش جوی نی هر جا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>هست نزدیکتر بتویزدان</p></div>
<div class="m2"><p>از رگ گردنت یقین میدان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نحن اقرب الیه در قرآن</p></div>
<div class="m2"><p>زین بفرموده است الرحمن</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>آنچه نزدیکتر ز تست بتو</p></div>
<div class="m2"><p>غافلی زان نمیبری خود بو</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>وانچه دور است از تو میدانی</p></div>
<div class="m2"><p>همچو لوح نبشته میخوانی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>کارهای تو جمله معکوس است</p></div>
<div class="m2"><p>زان سرت زیر قهر منکوس است</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>آنچه پیدا تر است از خورشید</p></div>
<div class="m2"><p>گشت پوشیده پرتو ای نومید</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>وانچه پنهان تر است از عنقا</p></div>
<div class="m2"><p>همچو صعوه است پیش تو پیدا</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>از خودی خودی چو خر نادان</p></div>
<div class="m2"><p>که کدا می فرشته یا حیوان</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>وز هر آن چیز کز تو دور است آن</p></div>
<div class="m2"><p>واقفی نیک و گشته ‌ ای همه دان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>آن چه سودت نکرد دانستی</p></div>
<div class="m2"><p>ضبط هر علم را توانستی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>وانچه سودت در آن ولابد است</p></div>
<div class="m2"><p>بخت و دولت ترا ازان و داست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>اجنبیئی از آن و بیخبری</p></div>
<div class="m2"><p>عمر را در فشار میسپری</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>علم جان را که تن از آن زنده است</p></div>
<div class="m2"><p>دائماً نغز و خوب و رخشنده است</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>باید اول شناختن آن را</p></div>
<div class="m2"><p>که چرا آفرید حق جان را</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>از کجا آمد و کجا رود او</p></div>
<div class="m2"><p>واخر کار تا چسان شود او</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>هست مقبول حضرت یزدان</p></div>
<div class="m2"><p>یا که مردود اوست در دو جهان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>رو سپید است یا چو قیر سیاه</p></div>
<div class="m2"><p>یا پر است از صواب یا ز گناه</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>هست فانی و یا بود باقی</p></div>
<div class="m2"><p>هست واقی و یا بود عاقی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>مرگ و حشر و صراط و حور و جنان</p></div>
<div class="m2"><p>هم بداند که چیستند و چسان</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>شخص را واجب است دانش این</p></div>
<div class="m2"><p>کز چنین علم میفزاید دین</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>نی ز فکر و قیاس و نقل و خبر</p></div>
<div class="m2"><p>بل ز عین و عیان و کشف ونظر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>اینچنین علم جست هر عاقل</p></div>
<div class="m2"><p>غیر این را نجست جز غافل</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>غیر این علم گمرهی است یقین</p></div>
<div class="m2"><p>نیستش حاصلی بجز تزیین</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چند روزه است دانش ظاهر</p></div>
<div class="m2"><p>هیچ جانی از آن نشد طاهر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>همچو کالا وزر شود فانی</p></div>
<div class="m2"><p>هر علومی که نیست آن جانی</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>در علوم زیادتی چستی</p></div>
<div class="m2"><p>واندر آنچه که بایدت سستی</p></div></div>
<div class="b" id="bn107"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>