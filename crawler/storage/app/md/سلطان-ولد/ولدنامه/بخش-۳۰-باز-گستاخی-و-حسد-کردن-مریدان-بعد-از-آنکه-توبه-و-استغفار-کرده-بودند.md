---
title: >-
    بخش ۳۰ - باز گستاخی و حسد کردن مریدان بعد از آنکه توبه و استغفار کرده بودند
---
# بخش ۳۰ - باز گستاخی و حسد کردن مریدان بعد از آنکه توبه و استغفار کرده بودند

<div class="b" id="bn1"><div class="m1"><p>باز شیطان بصورتی دیگر</p></div>
<div class="m2"><p>زد در ایشان کدورتی دیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد چندین صفا و کشف عطا</p></div>
<div class="m2"><p>بعد چندین عروج سوی علا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکر شیطان ببین که چونشان باز</p></div>
<div class="m2"><p>کرد بیزار از نمازو نیاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخت اعمال جمله را دزدید</p></div>
<div class="m2"><p>هر یکی زاعتقاد بر گردید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازگشتند همچو اول بار</p></div>
<div class="m2"><p>می و مستی گذشت و ماند خمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روشنی شد بدل بتاریکی</p></div>
<div class="m2"><p>صحت تن برنج باریکی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم زخمی رسید از غیرت</p></div>
<div class="m2"><p>تا شود جمله خلق را عبرت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا که خایف بوند در ره دین</p></div>
<div class="m2"><p>نشوند ایمن از ابلیس لعین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تکیه بر زهد و بر عمل نکنند</p></div>
<div class="m2"><p>شادمانی بهر امل نکنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه گردند از عمل دریا</p></div>
<div class="m2"><p>جمله باشند خایف و جویا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاجزانه روند این ره را</p></div>
<div class="m2"><p>نهلند از کف خود آگه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هیچ بی پیشوا قدم ننهند</p></div>
<div class="m2"><p>دامنش را ز دست خود ندهند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرچه آن خمرشان کند مسرور</p></div>
<div class="m2"><p>نشوند از بله بدان مغرور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حال آن جمع یادشان آید</p></div>
<div class="m2"><p>ترسشان هر نفس بیفزاید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر رسدشان ز حق هزار عطا</p></div>
<div class="m2"><p>نشوند ایمن از کمین قضا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قوت و زور زارشان دارد</p></div>
<div class="m2"><p>در عبادت بکارشان دارد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در تنعم کنند مسکینی</p></div>
<div class="m2"><p>گاه شادی و عیش غمگینی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زان چنان چشم زخم روز و شبان</p></div>
<div class="m2"><p>ترس ترسان بوند ناله کنان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هله ای زاهدان شب بیدار</p></div>
<div class="m2"><p>هله ای عالمان خوش رفتار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هل ای رهروان ز پیر و فتی</p></div>
<div class="m2"><p>هله ای صادقان بی همتا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هله ای بندگان آن حضرت</p></div>
<div class="m2"><p>هله ای طالبان آن دولت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هله آنها که که از جهان رستید</p></div>
<div class="m2"><p>از چنین دام بی امان جستید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هله آنها که پاک بازانید</p></div>
<div class="m2"><p>هر یکی در شکار بازانید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هله آنها که فارغ از خلقید</p></div>
<div class="m2"><p>شده قانع بکهنۀ دلقید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هله آنها که بی خورش سیرید</p></div>
<div class="m2"><p>در چنین بیشه هر یکی شیرید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هله آنها که بر شما آتش</p></div>
<div class="m2"><p>همچو گل شد لطیف و تازه و خوش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هله آنها که بر شما طوفان</p></div>
<div class="m2"><p>گشت چون جسر تا روید بر آن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هله آنها که بر هوا رفتید</p></div>
<div class="m2"><p>سبک ار چه بتن قوی زفتید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ترس ترسان روید این ره را</p></div>
<div class="m2"><p>تا ببینید روی آن شه را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دشمن جانتان چو شیطان است</p></div>
<div class="m2"><p>نبود ایمن آنکه ان س ان است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دشمن خرد نیست زو ترسید</p></div>
<div class="m2"><p>مکر او را ز رهروان پرسید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صدهزاران هزار چون ما را</p></div>
<div class="m2"><p>قصد کرد از برای یغما را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همچو آدم که اصل و بابا بود</p></div>
<div class="m2"><p>جد هر مؤمنی و ترسا بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>انبیا و اولیا ز پشت وی اند</p></div>
<div class="m2"><p>گرچه از مصر و از عراق وری اند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مقتدا و خلیفۀ یزدان</p></div>
<div class="m2"><p>هر فرشته اش سجود کرده ز جان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>با چنین آدم علیم صفی</p></div>
<div class="m2"><p>با چنین پیشوا و یار وفی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مکرها کرد و عاقبت او را</p></div>
<div class="m2"><p>کرد بیرون ز جنة المأوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از کمین نقل نقل کرد از عهد</p></div>
<div class="m2"><p>گندمی را نمود بیش از شهد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دام ر ا زیر دانه پنهان کرد</p></div>
<div class="m2"><p>تا ورا صید همچو مرغان کرد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>با تو مسکین که کم ز عصفوری</p></div>
<div class="m2"><p>چه کند فکر کن چه مغروری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دشمن آدم است بچگانش</p></div>
<div class="m2"><p>کو کسی کو نشد پریانش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>باز چون شمس دین بدانس ت این</p></div>
<div class="m2"><p>که شدند آن گروه پ ر از کین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آن محبت برفت از دلشان</p></div>
<div class="m2"><p>باز شد دل زبون آن گلشان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عقلشان شد اسیر نفس و هوی</p></div>
<div class="m2"><p>مؤمنان گشته از هوا ترسا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ن فسهای خبیث جوشیدند</p></div>
<div class="m2"><p>باز در قلع شاه کوشیدند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گفت شه با ولد که دیدی باز</p></div>
<div class="m2"><p>چون شدند از شقا همه دمساز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که مرا از حضور مولانا</p></div>
<div class="m2"><p>که چو او نیست هادی و دانا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فکنندم جدا و دور کنند</p></div>
<div class="m2"><p>بعد من جملگان سرور کنند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خواهم این بار آنچنان رفتن</p></div>
<div class="m2"><p>که نداند کسی کجایم من</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همه گردند در طلب عاجز</p></div>
<div class="m2"><p>ندهد کس نشان ز من هرگز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سالها بگذرد چنین بسیار</p></div>
<div class="m2"><p>کس نیاید ز گرد من آثار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چون کشانم دراز گویند این</p></div>
<div class="m2"><p>که ورا دشمنی بکشت یقین</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چند بار این سخن مکرر کرد</p></div>
<div class="m2"><p>بهر تأکید را مقرر کرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>