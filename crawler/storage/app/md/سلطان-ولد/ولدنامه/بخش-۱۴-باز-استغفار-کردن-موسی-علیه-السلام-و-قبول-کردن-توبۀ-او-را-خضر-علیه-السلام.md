---
title: >-
    بخش ۱۴ - باز استغفار کردن موسی علیه السلام و قبول کردن توبۀ او را خضر علیه السلام
---
# بخش ۱۴ - باز استغفار کردن موسی علیه السلام و قبول کردن توبۀ او را خضر علیه السلام

<div class="b" id="bn1"><div class="m1"><p>باز با همدگر رفیق شدند</p></div>
<div class="m2"><p>باز از جان و دل شفیق شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا رسیدند در جزیره بحر</p></div>
<div class="m2"><p>بر عمارت بزرگ همچون شهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندر آن جای یک پسر دیدند</p></div>
<div class="m2"><p>روی او خوب چون قمر دیدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیره ماندند هر دو در رخ او</p></div>
<div class="m2"><p>در حدیث و سؤال و پاسخ او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواند او را خضر بسوئی برد</p></div>
<div class="m2"><p>از پس کوه پیش جوئی برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیر بنهاد و حلق او ببرید</p></div>
<div class="m2"><p>مرغ جان پسر ز تن بپرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون کلیم این بدید گفتش های</p></div>
<div class="m2"><p>بازگو چیست این برای خدای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طفل معصوم را بکشتی زار</p></div>
<div class="m2"><p>کی روا دارد این بگو زنهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت من هی بگفتمت ز آغاز</p></div>
<div class="m2"><p>که نخواهی تو فهم کرد این راز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زانکه در ظاهری فرو مانده</p></div>
<div class="m2"><p>گرچه حقت کلیم خود خوانده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرچه بینی ز من تو تا صد سال</p></div>
<div class="m2"><p>کرد خواهی بر آن ز عجز سؤال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت عفوم کن این دوم بار است</p></div>
<div class="m2"><p>بحق حق که با تو او یار است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرد زاری بپیش او موسی</p></div>
<div class="m2"><p>که ببخش این گناه را تاسه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چونکه سنت سه بار آمده است</p></div>
<div class="m2"><p>تا ب س ه در شمار نامده است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر کنم باز اینچنین جرمی</p></div>
<div class="m2"><p>نبود جز فراق تو غرمی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بعد از آن عذر را مجال مده</p></div>
<div class="m2"><p>هجر بگزین دگر وصال مده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت میگفتمت نمیشنوی</p></div>
<div class="m2"><p>زانکه در شرع را سخی و قوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر تو ظاهر چو غالبست از آن</p></div>
<div class="m2"><p>این لجاجت چنین قویست بدان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر بدی مر ترا بمعنی راه</p></div>
<div class="m2"><p>گفت من کی بدی بر تو تباه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس ز اول که گفتمت که برو</p></div>
<div class="m2"><p>همره من مشو ز من بشنو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیروی آن بدی نه این که بمن</p></div>
<div class="m2"><p>می ‌ روی هر طرف بظاهر تن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیروی آن بدست در معنی</p></div>
<div class="m2"><p>غیر این گمرهی است هم دعوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>