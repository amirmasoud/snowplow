---
title: >-
    بخش ۷۴ - رجوع کردن بدان قصه که ولد را چلبی حسام الدین قدسنا اللّه بسرالعزیز در خواب نموده بود
---
# بخش ۷۴ - رجوع کردن بدان قصه که ولد را چلبی حسام الدین قدسنا اللّه بسرالعزیز در خواب نموده بود

<div class="b" id="bn1"><div class="m1"><p>هست مردی در این جهان پنهان</p></div>
<div class="m2"><p>مثل نقره و زر اندر کان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظاهرش خاک و باطنش زر پاک</p></div>
<div class="m2"><p>تن او سست و جان او چالاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذات او نور آسمان و زمین</p></div>
<div class="m2"><p>گر ترا هست نور چشم ببین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوچه شکل است و چه بدیع نگار</p></div>
<div class="m2"><p>بی نظیر است در میان کبار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس ندید اندر آب و گل چو وئی</p></div>
<div class="m2"><p>دل و جان مثل او نیافت حئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست مانندش اندر این دوران</p></div>
<div class="m2"><p>در زمان و زمین و کون و مکان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه عالم چو جسم و او چون جان</p></div>
<div class="m2"><p>همه عالم قراضه او چون کان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصف او کرده بد بمن در خواب</p></div>
<div class="m2"><p>ش ه حسام الحق لطیف جواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچنان است بلکه صد چندان</p></div>
<div class="m2"><p>نتوان کرد شرح او بزبان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گشته ام کمترین غلام درش</p></div>
<div class="m2"><p>تا شدم هست میخورم ز برش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیش از این آنچه خورده بودم من</p></div>
<div class="m2"><p>بیشمار است ناید آن بسخن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اینقدر کان بفهم می آید</p></div>
<div class="m2"><p>گفتنش پیش عاقلان شاید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گویم ار بشنوی بصدق زمن</p></div>
<div class="m2"><p>چند حرفی ز سر گذشت ز من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون که زائیدم از تن مادر</p></div>
<div class="m2"><p>شیر شد بعد خونم اندر خور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پاره ای چون بزرگتر گشتم</p></div>
<div class="m2"><p>لوت خوردم ز شیر بگذشتم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بعد از آن از برنج و شهد و شکر</p></div>
<div class="m2"><p>شد غذا میوه ها ز خشک و ز تر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون ز خوردن گذشتم اندر جوع</p></div>
<div class="m2"><p>حکمت از من برست چون ینبوع</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بی دهانی طعام ها خوردم</p></div>
<div class="m2"><p>بی کف از وی نواله ها بردم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بشریت برفت و دل چو ملک</p></div>
<div class="m2"><p>گشت پران ورای هفت فلک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چونکه از خود گذشتم آخر کار</p></div>
<div class="m2"><p>بحر گشتم مرا مجوی کنار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست این را نهایت و پایان</p></div>
<div class="m2"><p>کو درون و کجا بیان و زبان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>میروم من گهی چپ و گه راست</p></div>
<div class="m2"><p>دم مزن کاین نفس ز حق برخاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رو مکن اعتراض بر مسکین</p></div>
<div class="m2"><p>گرچه زفتی و خوب و با تمکین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در شکستش مرو عجب چیز است</p></div>
<div class="m2"><p>فصل او بی بهار و پائیز است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نی ز نار است نور آن سرور</p></div>
<div class="m2"><p>نبود آن طرف شه و چاکر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>غیر او شیخ و اوستاد مجو</p></div>
<div class="m2"><p>زانکه نبود در این جهان چون او</p></div></div>
<div class="b" id="bn27"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>