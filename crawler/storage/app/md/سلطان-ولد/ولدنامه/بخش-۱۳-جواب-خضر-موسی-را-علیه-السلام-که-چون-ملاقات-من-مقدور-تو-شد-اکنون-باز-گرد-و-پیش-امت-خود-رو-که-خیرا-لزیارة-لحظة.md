---
title: >-
    بخش ۱۳ - جواب خضر موسی را علیه السلام که چون ملاقات من مقدور تو شد اکنون باز گرد و پیش امت خود رو که خیرا لزیارة لحظة
---
# بخش ۱۳ - جواب خضر موسی را علیه السلام که چون ملاقات من مقدور تو شد اکنون باز گرد و پیش امت خود رو که خیرا لزیارة لحظة

<div class="b" id="bn1"><div class="m1"><p>گفت ای موسی کلیم بدان</p></div>
<div class="m2"><p>که بمن کرد همرهمی نتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که زنم نعل باژگونه بسی</p></div>
<div class="m2"><p>نکته ام را نکرد فهم کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صحبتم مشکل و قوی صعب است</p></div>
<div class="m2"><p>آب دریا م تا حد کعب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای همراهیم کجا داری</p></div>
<div class="m2"><p>چون تو بی من رهی جدا داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت باشد که حق دهد یاری</p></div>
<div class="m2"><p>بخشدم عقل و فهم و هشیاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چنین خواب غفلت تاری</p></div>
<div class="m2"><p>رسدم از خدای بیداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ورا دید راغب و صادق</p></div>
<div class="m2"><p>مست او شد و واله و عاشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کردش از دل قبول در صحبت</p></div>
<div class="m2"><p>که بود بس حمول در صحبت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نرمد از هر آنچه رو ب ی ند</p></div>
<div class="m2"><p>نیک و بد را همه نکو بیند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کفرهای ورا شمارد دین</p></div>
<div class="m2"><p>نشود از جفای او غمگین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زهر را از کفش چو شهد خورد</p></div>
<div class="m2"><p>سنگ او را بجای لعل خرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون بهم در سفر رفیق شدند</p></div>
<div class="m2"><p>همدگر را ز جان شفیق شدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چند روزی بهم همیرفتند</p></div>
<div class="m2"><p>در جان را بگفت میسفتند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر طرف چون بسی بگردیدند</p></div>
<div class="m2"><p>بر لب بحر کشتئی دیدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که نبد در جهان چنین کشتی</p></div>
<div class="m2"><p>خلق را بود بستر وپشتی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همچو شهری فراخ بود و بزرگ</p></div>
<div class="m2"><p>بادبانی بر او بلند و سترگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ناگهان خضر سوی کشتی رفت</p></div>
<div class="m2"><p>تبری در کفش بصورت زفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زد بر آن بادبان و کشتی او</p></div>
<div class="m2"><p>از پی خدمت آن گزیدۀ هو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در شکست آن درست کشتی را</p></div>
<div class="m2"><p>تا کند دفع ظلم و زشتی را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شد معطل ز کار آن کشتی</p></div>
<div class="m2"><p>ماند بی رخت و بار آن کشتی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت با وی کلیم این چون است</p></div>
<div class="m2"><p>این ز عقل و ز شرع بیرون است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مؤمنان را بد این پناه حصین</p></div>
<div class="m2"><p>از چه رو کردیش خراب چنین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هیچ این را روا ندارد حق</p></div>
<div class="m2"><p>اندر این کار بر تو گیرد دق</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفت او را نگفتمت پیشین</p></div>
<div class="m2"><p>که ترا صبر نبود و تمکین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من نگفتم ترا از او ّ ل کار</p></div>
<div class="m2"><p>که نداری تو پای من هشدار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کار من بد نما ولی نیکوست</p></div>
<div class="m2"><p>همچو آن زشت رو که نیکو خوست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من بر آتش اگرچه ب نشینم</p></div>
<div class="m2"><p>هر دم از وی گل و سمن چینم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من ز مرده برون کنم زنده</p></div>
<div class="m2"><p>کنم از عین گریه صد خنده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آرم ابلیس را ز عرش بفرق</p></div>
<div class="m2"><p>برم ادریس را ز فرش بعرش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفت ای شاه من خطا کردم</p></div>
<div class="m2"><p>این ز نسیان نه از رضا کردم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گذران از من این یکی کرت</p></div>
<div class="m2"><p>عفو فرما ز لطف این زل ّ ت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفت میدان کزین نخواهی گشت</p></div>
<div class="m2"><p>خاک خاک است اگر باب آ غ شت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لیک این حال بر تو پوشیده است</p></div>
<div class="m2"><p>سرت از بند من بگردیده است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هم شود آخرت یقین پید ا</p></div>
<div class="m2"><p>کانچه گفتم نبود سهو و خطا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اینت گفتم خداست شاهد حال</p></div>
<div class="m2"><p>هرکه گژ گیردش بود او ضال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون ش نید از خضر کلیم این را</p></div>
<div class="m2"><p>پیش آورد آن زمان لین را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کرد زاری و گفت بهر خدا</p></div>
<div class="m2"><p>لابه ام را پذیر و بخش خطا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر کنم بار دیگر این حرکت</p></div>
<div class="m2"><p>مشنو از من بهانه یا حجت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>