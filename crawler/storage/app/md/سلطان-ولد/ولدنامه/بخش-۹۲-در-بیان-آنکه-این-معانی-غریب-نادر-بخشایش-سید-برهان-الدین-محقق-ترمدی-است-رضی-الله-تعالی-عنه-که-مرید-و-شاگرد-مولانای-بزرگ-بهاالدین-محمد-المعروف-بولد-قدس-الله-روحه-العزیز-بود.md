---
title: >-
    بخش ۹۲ - در بیان آنکه این معانی غریب نادر بخشایش سید برهان الدین محقق ترمدی است رضی اللّه تعالی عنه که مرید و شاگرد مولانای بزرگ بهاءالدین محمد المعروف بولد قدس اللّه روحه العزیز بود.
---
# بخش ۹۲ - در بیان آنکه این معانی غریب نادر بخشایش سید برهان الدین محقق ترمدی است رضی اللّه تعالی عنه که مرید و شاگرد مولانای بزرگ بهاءالدین محمد المعروف بولد قدس اللّه روحه العزیز بود.

<div class="b" id="bn1"><div class="m1"><p>این معانی و این غریب بیان</p></div>
<div class="m2"><p>داد برهان دین محقق دان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت در گوشم آن گزیدۀ حق</p></div>
<div class="m2"><p>سبق برده ز سابقان بسبق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکته ‌ هائی که کس نگفت آنرا</p></div>
<div class="m2"><p>کرد پیدا نمود برهان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان او بود معدن اسرار</p></div>
<div class="m2"><p>همچو خورشید چشمۀ انوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اولیا کس چو او نگفت سخن</p></div>
<div class="m2"><p>فرد بود او بعشق و علم لدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخنش را هر آنکه بشنودی</p></div>
<div class="m2"><p>دایم او را بصدق بستودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مست گشتی و واله و حیران</p></div>
<div class="m2"><p>خانۀ هوش او شدی ویران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که دیدی رخ ورا از دور</p></div>
<div class="m2"><p>نشدی پیش چشم او مستور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی معرف شدی بر او پیدا</p></div>
<div class="m2"><p>که ندارد در این جهان همتا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز اولیای خداست بی شک و ریب</p></div>
<div class="m2"><p>همه را رهبر است اندر غیب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود پیدا میانۀ خلقان</p></div>
<div class="m2"><p>همچو از اختران مه تابان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ماه از اختران نه ممتاز است</p></div>
<div class="m2"><p>کی بگوید که صعوه چون باز است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طفل شش ساله را شدی معلوم</p></div>
<div class="m2"><p>که نظیرش نیامد اندر روم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زبدۀ اولیای یزدان بود</p></div>
<div class="m2"><p>همچو حق آشکار و پنهان بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرچه جمله ورا غلام بدند</p></div>
<div class="m2"><p>لیک در فهم ناتمام بدند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر کسی قدر خویش دانستش</p></div>
<div class="m2"><p>آن قدر دید کو توانستش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زانکه احوال او چنان کان هست</p></div>
<div class="m2"><p>جز خدا هیچکس ندانسته است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آشکار و نهان از این روی است</p></div>
<div class="m2"><p>که نهان بحرو در عیان جوی است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گشت پیدا بما ز روی کرم</p></div>
<div class="m2"><p>از عطاهاش شد نم مایم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لیک او را هزار بحردگر</p></div>
<div class="m2"><p>هست پنهان ز چشمهای بشر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>