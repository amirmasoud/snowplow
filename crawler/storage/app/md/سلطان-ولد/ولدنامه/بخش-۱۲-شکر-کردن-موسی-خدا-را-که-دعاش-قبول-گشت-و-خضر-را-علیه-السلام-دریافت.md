---
title: >-
    بخش ۱۲ - شکر کردن موسی خدا را که دعاش قبول گشت و خضر را علیه السلام دریافت
---
# بخش ۱۲ - شکر کردن موسی خدا را که دعاش قبول گشت و خضر را علیه السلام دریافت

<div class="b" id="bn1"><div class="m1"><p>بر زمین سر نهاد و شکر خدا</p></div>
<div class="m2"><p>کرد از جان و دل بصدق و صفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان ملاقات شد قوی شادان</p></div>
<div class="m2"><p>رفت پیش خضر سجود کنان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست بوس خضر چو کرد بگفت</p></div>
<div class="m2"><p>حمد او گاه فاش و گاه نهفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد از آن خضر مر ورا بنواخت</p></div>
<div class="m2"><p>با وی از لطف یک ن فس پرداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت چونی ز رنجهای سفر</p></div>
<div class="m2"><p>گفت چون بهر تست نیست ضرر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنج بهر تو است گنج و گهر</p></div>
<div class="m2"><p>زهر از دست تست به ز شکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ز موسی چنین ارادت دید</p></div>
<div class="m2"><p>وان چنان لفظ های خوب شنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس زبان را بلطف و مهر گشود</p></div>
<div class="m2"><p>دل او را چو آینه بزدود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنچه میجست از خدای ودود</p></div>
<div class="m2"><p>در سخن جمله را ب وی بنمود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صد چنان شد که بد ز صحبت او</p></div>
<div class="m2"><p>دل بسته ‌ اش روانه گشت چو جو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو چه باشد که شد یکی دریا</p></div>
<div class="m2"><p>در صدف گشت در ّ بی همتا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در چی و بحر چی چه گفتم من</p></div>
<div class="m2"><p>آنچه او شد مجو ز راه سخن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون رسید از خضر بموسی این</p></div>
<div class="m2"><p>پس بگفتش بلطف آن ره بین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هله بر خیز سوی امت شو</p></div>
<div class="m2"><p>بی توقف بشهر خویش برو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خلق گمراه را براه آور</p></div>
<div class="m2"><p>همه را رو سوی اله آور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برهان جمله را ز نار جحیم</p></div>
<div class="m2"><p>که و مه را رسان بصدر نعیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا عوض از حق ت ثواب رسد</p></div>
<div class="m2"><p>اجر بیحد و بیحساب رسد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت موسی بوی که ای سلطان</p></div>
<div class="m2"><p>زین چنین حضرتی مرا تو مران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روی خوبت ندیده بودم من</p></div>
<div class="m2"><p>بشهانت گزیده بودم من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شب ز شوقت دم ی نمیخفتم</p></div>
<div class="m2"><p>درد دل را بکس نمی گفتم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ناچشیده میت خراب بدم</p></div>
<div class="m2"><p>مست بی جام و بی شراب بدم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بوی نان خوش مرا بنان آورد</p></div>
<div class="m2"><p>خورد نان سوی ملک جان آورد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در تمنات می سپردم جان</p></div>
<div class="m2"><p>بعد این وصل چون کشم هجران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چونکه افتاد بر رخت نظرم</p></div>
<div class="m2"><p>عمر بی تو بسر چگونه برم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بخدائی که اوست مطلوبت</p></div>
<div class="m2"><p>که شدم عاشق رخ خوبت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نکنم دور از این جناب رفیع</p></div>
<div class="m2"><p>مبر این شیر را ز طفل رضیع</p></div></div>
<div class="b" id="bn27"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>