---
title: >-
    بخش ۵۲ - در بیان آنکه چون مولانا و شیخ صلاح الدین قدسنا اللّه بسرهما العزیز از مریدان منکر روی گردانیدند و ایشان زیان های آن را در خود مشاهده کردند و دیدند که کلی محروم خواهند شدن بردر ایشان بفغان آمدند و توبه و استغفار پیش آوردند
---
# بخش ۵۲ - در بیان آنکه چون مولانا و شیخ صلاح الدین قدسنا اللّه بسرهما العزیز از مریدان منکر روی گردانیدند و ایشان زیان های آن را در خود مشاهده کردند و دیدند که کلی محروم خواهند شدن بردر ایشان بفغان آمدند و توبه و استغفار پیش آوردند

<div class="b" id="bn1"><div class="m1"><p>گفته از صدق ما غلامانیم</p></div>
<div class="m2"><p>شاه خود را بعشق جویانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقانیم سوی دوست رویم</p></div>
<div class="m2"><p>آن اوئیم پس برکه رویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لابه ها کرده زین نسق شب و روز</p></div>
<div class="m2"><p>با دو چشم پر آب از سرسوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریۀ زارشان چو رفت از حد</p></div>
<div class="m2"><p>بانگ و افغانشان گذشت از ع د ّ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشک چشمانشان چو جیحون شد</p></div>
<div class="m2"><p>جانهاشان ز هجر پر خون شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونکه دشمن بجانشان نگریست</p></div>
<div class="m2"><p>کرد رحمت بر آن گروه و گریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنگ چون موم شد ز آتششان</p></div>
<div class="m2"><p>بلکه بگداخت شد چو آب روان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شنیدند هر دو زاری را</p></div>
<div class="m2"><p>ساز کردند چنگ یاری را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در گشادند و راهشان دادند</p></div>
<div class="m2"><p>قفل های ببسته بگشادند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>توبه هاشان قبول شد آن دم</p></div>
<div class="m2"><p>شاد گشتند و رفت از ایشان غم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز خوش پر و بال بگشادند</p></div>
<div class="m2"><p>باز از نو ز مادران زادند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باز از نو جهان جان دیدند</p></div>
<div class="m2"><p>خویش را بی جسد روان دیدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حکمت از سینه شان بجوش آمد</p></div>
<div class="m2"><p>عوض جهل عقل و هوش آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه ظلم ت بدند نور شدند</p></div>
<div class="m2"><p>همه ماتم بدند سور شدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خار انکارشان شده گلشن</p></div>
<div class="m2"><p>شب تاریکشان چو مه روشن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه گشتند صافی و چالاک</p></div>
<div class="m2"><p>چون ملک رفته جمله بر افلاک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه را گشت چشمها بینا</p></div>
<div class="m2"><p>همه عالم شدند بر اسما</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باز مقبول آن دو شاه شدند</p></div>
<div class="m2"><p>باز ایمن در آن پناه شدند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سر آن رشته را که گم شده بود</p></div>
<div class="m2"><p>یافتند و زیانشان شد سود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بندۀ شه صلاح دین گشتند</p></div>
<div class="m2"><p>باز عشق ورا رهین گشتند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شیخ شد باز از همه خشنود</p></div>
<div class="m2"><p>باز از نو گناهشان بخشود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دادشان از کرم عطائی نو</p></div>
<div class="m2"><p>از رخ خوب خود لقائی نو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عمرده روزشان هزاران شد</p></div>
<div class="m2"><p>بلکه خود بیشمار و پایان شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کفرشان را ز لطف کردایمان</p></div>
<div class="m2"><p>جان جمله رسید در جانان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جانشان از بلای هجر رهید</p></div>
<div class="m2"><p>باز هر عاشقی بوصل رسید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>درد از درد دوست صاف شود</p></div>
<div class="m2"><p>مس دون زرکی از گزاف شود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کیمیا هر کسی نداند ساخت</p></div>
<div class="m2"><p>علم عشق کم کسی افراخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کیمیا چیست سر فدا کردن</p></div>
<div class="m2"><p>دائماً رو بمرگ آوردن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کیمیا دان که کشتن نفس است</p></div>
<div class="m2"><p>هرکه کشتش ز حبس هستی رست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کیمیا مردن است چون مردی</p></div>
<div class="m2"><p>صاف نوشی شراب بی دردی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرده شو زیر پای مرد خدا</p></div>
<div class="m2"><p>تا شوی زنده وروی بالا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نظرش هست کیمیای جلال</p></div>
<div class="m2"><p>مس تو زر شود از او در حال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سوی ب ی سوی کم سواری تاخت</p></div>
<div class="m2"><p>در ره عشق نادری سر باخت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر که سر باخت او شود سرور</p></div>
<div class="m2"><p>زنده باشد همیشه بی پیکر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سر بی سر سزای افسار است</p></div>
<div class="m2"><p>سر بی سر شه و جهاندار است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>