---
title: >-
    بخش ۳۵ - رفتن مولانا بجانب شام در جستجوی شمس الدین
---
# بخش ۳۵ - رفتن مولانا بجانب شام در جستجوی شمس الدین

<div class="b" id="bn1"><div class="m1"><p>کرد آهنگ و رفت جانب شام</p></div>
<div class="m2"><p>در پیش شد روانه پخته و خام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون رسید اندر آن سفر بدمشق</p></div>
<div class="m2"><p>خلق را سوخت او ز آتش عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه را کرد شیفته و مفتون</p></div>
<div class="m2"><p>همه رفتند از خودی بیرون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه گشتند عاشقش از جان</p></div>
<div class="m2"><p>دیده در درد او دو صد درمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانمان را فدای او کردند</p></div>
<div class="m2"><p>امرش از دل بجای آوردند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه از جان مرید و بنده شدند</p></div>
<div class="m2"><p>همچو سایه پیش فکنده شدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طالبش گشته طفل و پیر و جوان</p></div>
<div class="m2"><p>همه او را گزیده از دل و جان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شامیان هم شدند والۀ او</p></div>
<div class="m2"><p>کین چنین فاضل پیمبر خو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از چه گشته است عاشق و مجنون</p></div>
<div class="m2"><p>کاندر او مدرج است صد ذوالنون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عالم و ع ا می و غنی و فقیر</p></div>
<div class="m2"><p>مانده خیره در آن فغان و نفیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفته چه شیخ و چه مرید است این</p></div>
<div class="m2"><p>که نبدشان بهیچ قرن قرین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا جهان شد ز عهد آدم کس</p></div>
<div class="m2"><p>نشنید این چنین هوی و هوس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دیده بر روی او هزار اثر</p></div>
<div class="m2"><p>هر کرا بود در درون گوهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر دم از وی کرامتی همگان</p></div>
<div class="m2"><p>دیده مانند آفتاب عیان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سر ماضی و حال و مستقبل</p></div>
<div class="m2"><p>گفته با جمله بی خطا و زلل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه گفتند خود عجب اینست</p></div>
<div class="m2"><p>این چنین دیده کو خدا بین ست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مثلش اندر دهور نشنیدیم</p></div>
<div class="m2"><p>نی چو او در زمانه هم دیدیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کی بود در جهان از او بهتر</p></div>
<div class="m2"><p>در بزرگی و عز از او مهتر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که شده است اینچنین وراجویان</p></div>
<div class="m2"><p>هر طرف گشته خیره سر پویان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شمس تبریز خود چه شخص بود</p></div>
<div class="m2"><p>تا پیش این چنین یگانه رود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای عجب شیخ از او چه میجوید</p></div>
<div class="m2"><p>که پیش هر طرف همی پوید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این چه سراست ای خدا بنما</p></div>
<div class="m2"><p>بی حجابی بما چو خور پیدا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خود ندانسته این که فوقی نیست</p></div>
<div class="m2"><p>جز بخود با کسیش شوقی نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اندر او خویش را همی بیند</p></div>
<div class="m2"><p>غیر را عقل هیچ نگزیند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عقل گوید که طالب عقلم</p></div>
<div class="m2"><p>دایم از عاقلان بود نقلم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جنس آن دان که عین آن باشد</p></div>
<div class="m2"><p>کی شکر جنس ناردان باشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دومبین در میان که هر دو یکیم</p></div>
<div class="m2"><p>دردوشکی است ما بری ز شکیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ما غریبیم و هم غریب رویم</p></div>
<div class="m2"><p>اندر آخر سو بر حبیب رویم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیشکی جفت باز باز شود</p></div>
<div class="m2"><p>هم یقین سوی زاغ زاغ رود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو مرا غیر شمس دین مشمر</p></div>
<div class="m2"><p>روح ما یک بود گذر ز صور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چار و پنج است و هفت یک قالب</p></div>
<div class="m2"><p>یک ز جان گشت چون جهان از رب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خاک قالب بد اول افکنده</p></div>
<div class="m2"><p>در زمین هر طرف پراکنده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن پراکندگی ز جان شد یک</p></div>
<div class="m2"><p>اندر این نیست هیچکس را شک</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>باز چون روح شد جدا از تن</p></div>
<div class="m2"><p>تن همان خاک گشت ای پر فن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شد پراکنده باز آن اجزا</p></div>
<div class="m2"><p>همچو او ّ ل که بود در مبدأ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چشم و گوش و سرودو دست و دو پا</p></div>
<div class="m2"><p>یک ز جان گشته اند چشم گشا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ورنه چون جان رود ز تن بیرون</p></div>
<div class="m2"><p>گردد از همدگر جدا تن دون</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>متفرق شوند هر سوئی</p></div>
<div class="m2"><p>پ ا رود جانبی و سر سوئی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یک شود کوزه یک شود دستی</p></div>
<div class="m2"><p>نیست گردند جمله زان هستی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همچنین ذره های ارض و سما</p></div>
<div class="m2"><p>از خور و ماه و از که و دریا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شده مجموع از یکی جان اند</p></div>
<div class="m2"><p>همه زو زنده اند و جنبان اند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همچو یک شخص گیر عالم را</p></div>
<div class="m2"><p>که بروحی است قائم و بر پا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون رود در قیامت ازوی جان</p></div>
<div class="m2"><p>آسمان و زمین شود ویران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ماه و استارگان فرو ریزند</p></div>
<div class="m2"><p>زیر و بالا بهم بیامیزند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نی جهان ماند و نه ارض و سما</p></div>
<div class="m2"><p>همه گردند لابجز الا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جان چو اعداد را کند یکتن</p></div>
<div class="m2"><p>چون بود جان دو چیز گوی بمن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گو نه هر اسب اسب را جوید</p></div>
<div class="m2"><p>سوی اشتر چرا نمیپوید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این سخن هست روشن و پیدا</p></div>
<div class="m2"><p>پیش آن کس که او بود دانا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جستن از نسبتست و جنسیت</p></div>
<div class="m2"><p>هر کسی را جداست ماهیت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سر این بیکران و بیحد ّ است</p></div>
<div class="m2"><p>ناید اندر شمار بی عد ّ است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زین معانی گذر کن ای سرمست</p></div>
<div class="m2"><p>قصه را گو که تا کجا پیوست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>