---
title: >-
    بخش ۲۹ - استغفار حسودان از کرده های خویش
---
# بخش ۲۹ - استغفار حسودان از کرده های خویش

<div class="b" id="bn1"><div class="m1"><p>وان جماعت که منکران بودند</p></div>
<div class="m2"><p>منکر قطب آسمان بودند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمله شان جان فشان باستغفار</p></div>
<div class="m2"><p>سر نهادند کای خدیو کبار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توبه کاریم از آنچه ما کردیم</p></div>
<div class="m2"><p>از سر صدق روی آوردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر یکی بر درش شده ساجد</p></div>
<div class="m2"><p>اشک ریزان ز عشق او واجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردشان شه قبول چون دید این</p></div>
<div class="m2"><p>دادشان از نوازش او تمکین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد از آن جمله از وضیع و شریف</p></div>
<div class="m2"><p>حلقه شستند گرد شاه لطیف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پهلوی شه نشسته مولانا</p></div>
<div class="m2"><p>چون دو خور که زنند سر ز سما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمس تبریز در سخن آمد</p></div>
<div class="m2"><p>زنده شد آنکه فهم کن آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر یکی زان سخن بعشق پ رید</p></div>
<div class="m2"><p>هر یکی از خودی تمام برید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بعد از آن هر یکی سماعی داد</p></div>
<div class="m2"><p>هر یکی خوان معتبر بنهاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر یکی قدر وسع و طاقت خویش</p></div>
<div class="m2"><p>از امیر و توانگر و درویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخشش آورد و میهمانی کرد</p></div>
<div class="m2"><p>تا شود یار مهربانی کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مدتی اینچنین گذشت زمان</p></div>
<div class="m2"><p>در حضور شهان هر دو جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه چون جام وان دو شه چون راح</p></div>
<div class="m2"><p>همه چون لیل و آن دو شه چو صباح</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن دو شه چون بهار و ایشان دشت</p></div>
<div class="m2"><p>همه را تازه گشته زیشان ک شت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاخ و برگ درونشان پر بار</p></div>
<div class="m2"><p>رسته بیخار هر طرف گلزار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دیده بی پرده ای همه دیدار</p></div>
<div class="m2"><p>همه گشته در آن جهان برکار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در چنین عیش و در چنین وصلت</p></div>
<div class="m2"><p>همه پر نور و غرق در رحمت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p></p></div>
<div class="m2"><p></p></div></div>