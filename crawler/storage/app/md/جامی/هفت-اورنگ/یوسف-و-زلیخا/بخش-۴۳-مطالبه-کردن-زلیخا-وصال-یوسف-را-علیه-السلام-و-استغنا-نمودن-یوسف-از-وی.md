---
title: >-
    بخش ۴۳ - مطالبه کردن زلیخا وصال یوسف را علیه السلام و استغنا نمودن یوسف از وی
---
# بخش ۴۳ - مطالبه کردن زلیخا وصال یوسف را علیه السلام و استغنا نمودن یوسف از وی

<div class="b" id="bn1"><div class="m1"><p>چو بندد بیدلی دل در نگاری</p></div>
<div class="m2"><p>نگیرد کار او هرگز قراری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نبود به کف نقد وصالش</p></div>
<div class="m2"><p>به نسیه عشق بازد با خیالش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی خونش بود از دل چکیده</p></div>
<div class="m2"><p>که افتد کار وی از دل به دیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو یابد بهره چشم اشکبارش</p></div>
<div class="m2"><p>فتد اندیشه بوس و کنارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر بوس و کنارش هم دهد دست</p></div>
<div class="m2"><p>ز بیم هجر باشد رنجه پیوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امید کامرانی نیست در عشق</p></div>
<div class="m2"><p>صفای زندگانی نیست در عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود آغاز آن خون خوردن و بس</p></div>
<div class="m2"><p>بود انجامش از خود مردن و بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به راحت کی بود آن کس سزاوار</p></div>
<div class="m2"><p>که خون خوردن بود یا مردنش کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلیخا بود یوسف را ندیده</p></div>
<div class="m2"><p>به خوابی و خیالی آرمیده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به جز دیدارش از هر جست و جویی</p></div>
<div class="m2"><p>نمی دانست خود را آرزویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو دید از دیدن او بهره مندی</p></div>
<div class="m2"><p>ز دیدن خواست طبع او بلندی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به آن آورد روی جست و جو را</p></div>
<div class="m2"><p>که آرد در کنار آن آرزو را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز لعل او به بوسه کام گیرد</p></div>
<div class="m2"><p>ز سروش با کنار آرام گیرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بلی نظارگی کاید سوی باغ</p></div>
<div class="m2"><p>ز شوق گل چو لاله سینه پر داغ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نخست از روی گل دیدن شود مست</p></div>
<div class="m2"><p>ز گل دیدن به گل چیدن برد دست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زلیخا وصل را می جست چاره</p></div>
<div class="m2"><p>ولی می کرد ازان یوسف کناره</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زلیخا بود خون از دیده ریزان</p></div>
<div class="m2"><p>ولی می بود ازو یوسف گریزان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زلیخا داشت بس جانسوز داغی</p></div>
<div class="m2"><p>ولی می داشت زان یوسف فراغی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زلیخا رخ بدان فرخ لقا داشت</p></div>
<div class="m2"><p>ولی یوسف نظر بر پشت پا داشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زلیخا بهر یک دیدن همی سوخت</p></div>
<div class="m2"><p>ولی یوسف ز دیدن دیده می دوخت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز بیم فتنه روی او نمی دید</p></div>
<div class="m2"><p>به چشم فتنه جوی او نمی دید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نیارد عاشق آن دیدار در چشم</p></div>
<div class="m2"><p>که با یارش نیفتد چشم بر چشم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز عاشق دمبدم اشکی و آهی</p></div>
<div class="m2"><p>نباشد جو به امید نگاهی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو یار از حال عاشق دیده پوشد</p></div>
<div class="m2"><p>سزد کش خون دل از دیده جوشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زلیخا را چو این غم بر سر آمد</p></div>
<div class="m2"><p>به اندک فرصتی از پا درآمد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برآمد در خزان محنت و درد</p></div>
<div class="m2"><p>گل سرخش به رنگ لاله زرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به دل ز اندوه بودش بار انبوه</p></div>
<div class="m2"><p>سهی سروش خمید از بار اندوه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برفت از لعل لب آبی که بودش</p></div>
<div class="m2"><p>نشست از شمع رخ تابی که بودش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نکردی شانه موی عنبرین بوی</p></div>
<div class="m2"><p>جز این پنجه که می کندی به آن موی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به سوی آینه کم رو گشادی</p></div>
<div class="m2"><p>مگر زانو که بر وی رو نهادی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز بس کز دل فشاندی خون تازه</p></div>
<div class="m2"><p>نگشتی چهره اش محتاج غازه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه عالم به چشمش چون سیه بود</p></div>
<div class="m2"><p>به چشمش سرمه را کی جایگه بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز سرمه زان سیه چشمی نمی جست</p></div>
<div class="m2"><p>که اشک از نرگس او سرمه می شست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زلیخا را چو شد زین غم جگر ریش</p></div>
<div class="m2"><p>زبان سرزنش بگشاد بر خویش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که ای کارت به رسوایی کشیده</p></div>
<div class="m2"><p>ز سودای غلام زر خریده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو شاهی بر سریر سرفرازی</p></div>
<div class="m2"><p>چرا با بنده خود عشق بازی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به معشوقی چو خود شاهی طلب دار</p></div>
<div class="m2"><p>که شاهی را بود شاهی سزاوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عجب تر آنکه از عجبی که دارد</p></div>
<div class="m2"><p>به وصل چون تویی سر در نیارد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زنان مصر اگر دانند حالت</p></div>
<div class="m2"><p>رسانند از ملامت صد ملامت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همی گفت این و لیکن آن یگانه</p></div>
<div class="m2"><p>نه زانسان در دل او داشت خانه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کش از خاطر توانستی برون کرد</p></div>
<div class="m2"><p>بدین افسانه دردش را فسون کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بلی چون دلبری با جان درآمیخت</p></div>
<div class="m2"><p>نیارد جان ازو پیوند بگسیخت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برد پیوند جان از تن به یک دم</p></div>
<div class="m2"><p>ولی با او بود جاوید محکم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چه خوش گفت آن به داغ عشق رنجور</p></div>
<div class="m2"><p>که بوی از مشک و رنگ از گل شود دور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ولی بیرون بود ز امکان عاشق</p></div>
<div class="m2"><p>که گوید ترک جانان جان عاشق</p></div></div>