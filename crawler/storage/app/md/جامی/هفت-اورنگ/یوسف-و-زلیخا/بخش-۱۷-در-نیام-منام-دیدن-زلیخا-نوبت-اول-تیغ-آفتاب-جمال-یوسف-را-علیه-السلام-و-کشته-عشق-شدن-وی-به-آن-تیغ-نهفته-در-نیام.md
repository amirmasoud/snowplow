---
title: >-
    بخش ۱۷ - در نیام منام دیدن زلیخا نوبت اول تیغ آفتاب جمال یوسف را علیه السلام و کشته عشق شدن وی به آن تیغ نهفته در نیام
---
# بخش ۱۷ - در نیام منام دیدن زلیخا نوبت اول تیغ آفتاب جمال یوسف را علیه السلام و کشته عشق شدن وی به آن تیغ نهفته در نیام

<div class="b" id="bn1"><div class="m1"><p>ز کنگردار کاخ شهریاری</p></div>
<div class="m2"><p>چو حارس دیده شکل کوکناری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بیداری نمانده دیگرش تاب</p></div>
<div class="m2"><p>خواص کوکنارش کرده در خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستاره از دهل کوبی دهل کوب</p></div>
<div class="m2"><p>هجوم خواب دستش بسته بر چوب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکرده مؤذن از گلبانگ یا حی</p></div>
<div class="m2"><p>فراش غفلت شب مردگان طی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلیخا آن به لبها شکر ناب</p></div>
<div class="m2"><p>شده بر نرگسش شیرین شکر خواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرش سوده به بالین جعد سنبل</p></div>
<div class="m2"><p>تنش داده به بستر خرمن گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بالین سنبلش در هم شکسته</p></div>
<div class="m2"><p>به گل تار حریرش نقش بسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خوابش چشم صورت بین غنوده</p></div>
<div class="m2"><p>ولی چشم دگر از دل گشوده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آمد از درش ناگه جوانی</p></div>
<div class="m2"><p>چه می گویم جوانی نی که جانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همایون پیکری از عالم نور</p></div>
<div class="m2"><p>به باغ خلد کرده غارت حور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ربوده سر به سر حسن و جمالش</p></div>
<div class="m2"><p>گرفته یک به یک غنج و دلالش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کشیده قامتی چون تازه شمشاد</p></div>
<div class="m2"><p>به آزادی غلامش سرو آزاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بر آویخته زلفی چو زنجیر</p></div>
<div class="m2"><p>خرد را بسته دست و پای تدبیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فروزان لمعه نور از جبینش</p></div>
<div class="m2"><p>مه و خورشید را رو بر زمینش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مقوس ابرویش محراب پاکان</p></div>
<div class="m2"><p>معنبر سایه بان بر خوابناکان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رخش ماهی ز اوج برج فردوس</p></div>
<div class="m2"><p>ز ابرو کرده آن مه خانه در قوس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مکحل نرگسش از سرمه ناز</p></div>
<div class="m2"><p>ز مژگان بر جگرها ناوک انداز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دو لعلش از تبسم در شکر ریز</p></div>
<div class="m2"><p>دهانش در تکلم شکر آمیز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بریق درش از لعل درافشان</p></div>
<div class="m2"><p>چو از گلگون شفق برق درخشان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به خنده از ثریا نور می ریخت</p></div>
<div class="m2"><p>نمک از پسته پرشور می ریخت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ذقن چون سیبی از غبغب مطوق</p></div>
<div class="m2"><p>ز سیب آویخته آبی معلق</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به گل خال رخش از مشک داغی</p></div>
<div class="m2"><p>گرفته آشیان زاغی به باغی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز سیمش ساعد و بازو توانگر</p></div>
<div class="m2"><p>ز بی سیمی میان چون موی لاغر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زلیخا چون به رویش دیده بگشاد</p></div>
<div class="m2"><p>به یک دیدارش افتاد آنچه افتاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جمالی دید از حد بشر دور</p></div>
<div class="m2"><p>ندیده از پری نشنیده از حور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز حسن صورت و لطف شمایل</p></div>
<div class="m2"><p>اسیرش شد به یک دل نی به صد دل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرفت از قامتش در دل خیالی</p></div>
<div class="m2"><p>نشاند از دوستی در جان نهالی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز رویش آتشی در سینه افروخت</p></div>
<div class="m2"><p>وز آن آتش متاع صبر و دین سوخت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وز آن عنبر فشان گیسوی دلبند</p></div>
<div class="m2"><p>به هر مو رشته جان کرد پیوند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز طاق ابرویش با ناله شد جفت</p></div>
<div class="m2"><p>ز خواب آلوده چشمش غرق خون گفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دل تنگ از لبش تنگ شکر ساخت</p></div>
<div class="m2"><p>ز دندانش مژه عقد گهر ساخت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز سیمین ساعدش شست از خرد دست</p></div>
<div class="m2"><p>میانش را کمر در بندگی بست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به رویش دید مشکین خال دلکش</p></div>
<div class="m2"><p>نشست از وی سپند آسا بر آتش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز سیب غبغبش آسیب جان دید</p></div>
<div class="m2"><p>بدانسان سیبی آسان کی توان چید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بنامیزد چه زیبا صورتی بود</p></div>
<div class="m2"><p>که صورت کاست و اندر معنی افزود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زلیخا از زلیخایی رمیده</p></div>
<div class="m2"><p>ازان صورت به معنی آرمیده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ازان معنی اگر آگاه بودی</p></div>
<div class="m2"><p>یکی از واصلان راه بودی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ولی چون بود در صورت گرفتار</p></div>
<div class="m2"><p>نشد در اول از معنی خبردار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه در بند پنداریم مانده</p></div>
<div class="m2"><p>به صورتها گرفتاریم مانده</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز صورت گر نه معنی رو نماید</p></div>
<div class="m2"><p>کجا یک دل سوی صورت گراید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یقین داند که در کوزه نمی هست</p></div>
<div class="m2"><p>ازان در گردن آرد تشنه اش دست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو سازد غرقه دریای زلالش</p></div>
<div class="m2"><p>نیاید یاد نم دیده سفالش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شبی خوش همچو صبح زندگانی</p></div>
<div class="m2"><p>نشاط افزا چو ایام جوانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز جنبش مرغ و ماهی آرمیده</p></div>
<div class="m2"><p>حوادث پای در دامن کشیده</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>درین بستانسرای پر نظاره</p></div>
<div class="m2"><p>نمانده باز جز چشم ستاره</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ربوده دزد شب هوش عسس را</p></div>
<div class="m2"><p>زبان بسته جرس جنبان جرس را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سگان را طوق گشته حلقه دم</p></div>
<div class="m2"><p>در آن حلقه ره فریادشان گم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز شهپر مرغ شب خنجر کشیده</p></div>
<div class="m2"><p>ز بانگ صبح نای خود بریده</p></div></div>