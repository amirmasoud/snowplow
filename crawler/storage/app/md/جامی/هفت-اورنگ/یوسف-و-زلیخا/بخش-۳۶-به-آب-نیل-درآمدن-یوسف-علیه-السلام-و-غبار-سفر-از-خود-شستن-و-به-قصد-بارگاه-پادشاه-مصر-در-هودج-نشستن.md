---
title: >-
    بخش ۳۶ - به آب نیل درآمدن یوسف علیه السلام و غبار سفر از خود شستن و به قصد بارگاه پادشاه مصر در هودج نشستن
---
# بخش ۳۶ - به آب نیل درآمدن یوسف علیه السلام و غبار سفر از خود شستن و به قصد بارگاه پادشاه مصر در هودج نشستن

<div class="b" id="bn1"><div class="m1"><p>به چارم روز موعود یوسف خور</p></div>
<div class="m2"><p>چو زد از ساحل نیل فلک سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یوسف گفت مالک کای دلارای</p></div>
<div class="m2"><p>تو همچون خود کنار نیل کن جای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خود کن گرد ره را شست و شویی</p></div>
<div class="m2"><p>ز خاکت نیل را ده آبرویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حکم مالک آن خورشید تابان</p></div>
<div class="m2"><p>به سوی نیل شد حالی تابان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زیر پیرهن برد از برون دست</p></div>
<div class="m2"><p>سمن را پرده نیلوفری بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلاه زرفشان از فرق بنهاد</p></div>
<div class="m2"><p>ز زرین بیضه خود زاغ شب زاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشید آنگه چنان پیراهن از فرق</p></div>
<div class="m2"><p>که جیبش غرب مه شد دامنش شرق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمود آن دوش و بر از عطف دامن</p></div>
<div class="m2"><p>چنان کز دور گردون صبح روشن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازار نیلگون برخاست فریاد</p></div>
<div class="m2"><p>چو سیمین سروری آمد بر لب نیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز چرخ نیلگون برخاست فریاد</p></div>
<div class="m2"><p>که شد نیل از قدوم آن مه آباد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به جای نیل من بودی چه بودی</p></div>
<div class="m2"><p>ز پا بوسش من آسودی چه بودی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر آن شد خور که خود را افکند پیش</p></div>
<div class="m2"><p>به رود نیل ریزد چشمه خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نبیند چشمه خود چون سزایش</p></div>
<div class="m2"><p>طفیل نیل شوید دست و پایش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به دریا پا نهاد از سوی ساحل</p></div>
<div class="m2"><p>چو مه در برج آبی ساخت منزل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به طلعت بود خورشید جهانتاب</p></div>
<div class="m2"><p>چو نیلوفر فرو رفت اندر آن آب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تنش در آب چون عریان درآمد</p></div>
<div class="m2"><p>به تن آب روان را جان درآمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گشاد از هم مسلسل گیسوان را</p></div>
<div class="m2"><p>به رخ زنجیر بست آب روان را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مهیا ساخت بهر صید خواهی</p></div>
<div class="m2"><p>معنبر دامی از مه تا به ماهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گهی می ریخت آب از دست بر سر</p></div>
<div class="m2"><p>ز پروین ماه را می بست زیور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گهی می داد از کف مالش گل</p></div>
<div class="m2"><p>ز پنجه شانه می زد شاخ سنبل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو گرد از روی و چرک از تن فرو شست</p></div>
<div class="m2"><p>چو سروی از کنار نیل بر رست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز مفرش دار مالک پیرهن خواست</p></div>
<div class="m2"><p>به جلباب سمن گل را بیاراست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کشید آنگه به بر دیبای زرکش</p></div>
<div class="m2"><p>به چندین نقش های خوش منقش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به زرین تاج مه را قدر بشکست</p></div>
<div class="m2"><p>کمربند مرصع بر میان بست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فرو آویخت زلفین دلاویز</p></div>
<div class="m2"><p>هوای مصر ازان شد عنبر آمیز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدان خوبیش در هودج نشاندند</p></div>
<div class="m2"><p>به قصد قصر شه مرکب براندند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نمود از قصر بیرون تختگاهی</p></div>
<div class="m2"><p>که شاه آنجا کشیدی رخت گاهی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به پیشش خیل خوبان صف کشیده</p></div>
<div class="m2"><p>پی دیدار یوسف آرمیده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فراز تخت هودج را نهادند</p></div>
<div class="m2"><p>جهانی چشم بر هودج ستادند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قضا را بود ز ابر تیره آن روز</p></div>
<div class="m2"><p>نهفته آفتاب عالم افروز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به یوسف گفت مالک کای دلارام</p></div>
<div class="m2"><p>ز هودج نه به سوی تختگه گام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو خورشیدی ز عارض پرده بگشای</p></div>
<div class="m2"><p>ز نور خویش عالم را بیارای</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو یوسف برج هودج را بپرداخت</p></div>
<div class="m2"><p>چو خور بر چشم مردم پرتو انداخت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گمان شد ناظران را کافتاب است</p></div>
<div class="m2"><p>که طالع گشته از نیلی سحاب است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نظر کردند در مهر جهانتاب</p></div>
<div class="m2"><p>بدانستند کز وی نیست آن تاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هنوز او در پس ابر است مستور</p></div>
<div class="m2"><p>ز روی یوسف است آن تابش نور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز حیرت کف زنان اهل نظاره</p></div>
<div class="m2"><p>فغان برداشتند از هر کناره</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که یا رب کیست این فرخنده اختر</p></div>
<div class="m2"><p>که هم ماه است ازو شرمنده هم خور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بتان مصر سر در پیش ماندند</p></div>
<div class="m2"><p>ز لوحش حرف نسخ خویش خواندند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بلی هر جا شود مهر آشکارا</p></div>
<div class="m2"><p>سها را جز نهان بودن چه یارا</p></div></div>