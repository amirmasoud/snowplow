---
title: >-
    بخش ۶۰ - رفتن زلیخا در روز به بام قصر خویش و از آنجا نظاره بام زندان کردن و بر مفارقت یوسف ناله و زاری برداشتن
---
# بخش ۶۰ - رفتن زلیخا در روز به بام قصر خویش و از آنجا نظاره بام زندان کردن و بر مفارقت یوسف ناله و زاری برداشتن

<div class="b" id="bn1"><div class="m1"><p>شب آمد عاشقان را پرده راز</p></div>
<div class="m2"><p>شب آمد بی دلان را غصه پرداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توان بس کار در شبگیر کردن</p></div>
<div class="m2"><p>که روزش کم توان تدبیر کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلیخا چون غم شب بگذرانید</p></div>
<div class="m2"><p>نه غم بل ماتم شب بگذرانید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلا و محنت روز آمدش پیش</p></div>
<div class="m2"><p>صد اندوه جگرسوز آمدش پیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه روی آنکه در زندان کند روی</p></div>
<div class="m2"><p>نه صبر آنکه بی زندان کند خوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز نعمت های خوش هر لحظه چیزی</p></div>
<div class="m2"><p>نهادی بر کف محرم کنیزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرستادی به زندان سوی یوسف</p></div>
<div class="m2"><p>که تا دیدی به جایش روی یوسف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو آن محرم ز زندان آمدی باز</p></div>
<div class="m2"><p>بدو صد عشقبازی کردی آغاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی رو بر کف پایش نهادی</p></div>
<div class="m2"><p>گهی صد بوسه اش بر چشم دادی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که این چشمیست کان رخسار دیده ست</p></div>
<div class="m2"><p>که آن پاییست کانجاها رسیده ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر چشمش نیارم بوسه دادن</p></div>
<div class="m2"><p>و یا رو بر کف پایش نهادن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ببوسم باری آن چشمی که گاهی</p></div>
<div class="m2"><p>کند در روی زیبایش نگاهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نهم رو بر کف آن پای باری</p></div>
<div class="m2"><p>که وقتی می کند سویش گذاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بپرسیدی ازان پس حال او را</p></div>
<div class="m2"><p>جمال روی فرخ فال او را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که رویش را نفرسوده گزندی</p></div>
<div class="m2"><p>به کار او نیفتاده ست بندی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گلش را از هوا پژمردگی نیست</p></div>
<div class="m2"><p>تنش را زان زمین آزردگی نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز نعمت ها که بردی خورد یا نی</p></div>
<div class="m2"><p>ازین دلداده یاد آورد یا نی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس از پرسش نمودن های بسیار</p></div>
<div class="m2"><p>ز جا برخاستی با چشم خونبار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به بام کاخ در یک غرفه بودش</p></div>
<div class="m2"><p>کز آنجا بام زندان می نمودش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در آن غرفه شدی تنها نشستی</p></div>
<div class="m2"><p>در غرفه به روی خلق بستی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدیده در به مژگان لعل سفتی</p></div>
<div class="m2"><p>سوی زندان نظر کردی و گفتی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کیم تا روی گلفامش ببینم</p></div>
<div class="m2"><p>پس این کز بام خود بامش ببینم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیم شایسته دیدار دیدن</p></div>
<div class="m2"><p>خوشم با آن در و دیوار دیدن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به هر جا ماه من منزل نشین است</p></div>
<div class="m2"><p>نه خانه روضه خلد برین است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز دولت سقف او سرمایه دارد</p></div>
<div class="m2"><p>که خورشیدی چنان در سایه دارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرا دیوارش از غم پشت بشکست</p></div>
<div class="m2"><p>که پشت آن مه بر او بنهاده بنشست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سعادت سرفراز آید ازان در</p></div>
<div class="m2"><p>که سرو من فرود آرد به آن سر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه دولتمند باشد آستانی</p></div>
<div class="m2"><p>که بوسد پای آنسان دلستانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خوش آن کز تیغ مهرش آشکاره</p></div>
<div class="m2"><p>تنم چون ذره کرده پاره پاره</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در افتم سرنگون از روزن او</p></div>
<div class="m2"><p>به پیش آفتاب روشن او</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هزاران رشک دارم بر زمینی</p></div>
<div class="m2"><p>که بخرامد بدانسان نازنینی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شود از گرد دامانش معطر</p></div>
<div class="m2"><p>ز موی عنبرافشانش معنبر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سخن کوتاه تا شب کارش این بود</p></div>
<div class="m2"><p>گرفتاریش آن گفتارش این بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>درین گفتار جانش بر لب آمد</p></div>
<div class="m2"><p>درین اندوه روزش تا شب آمد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو آمد شب دگر شد حیله اندیش</p></div>
<div class="m2"><p>که گیرد پیش آیین شب پیش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شبش این بود و روزان تا بدان روز</p></div>
<div class="m2"><p>که زندان بود جای آن دل افروز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به شب زندان شدن را چاره کردی</p></div>
<div class="m2"><p>به روز از غرفه اش نظاره کردی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نبودی هیچگه خالی ازین کار</p></div>
<div class="m2"><p>گهی دیوار دیدی گاه دیدار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنان یوسف به خاطر خانه کردش</p></div>
<div class="m2"><p>که از جان و جهان بیگانه کردش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز بس در یاد او گم کرد خود را</p></div>
<div class="m2"><p>بشست از لوح خاطر نیک و بد را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کنیزان گر چه می دادندش آواز</p></div>
<div class="m2"><p>نمی آمد به حال خویشتن باز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بگفتی با کنیزان گاه و بیگاه</p></div>
<div class="m2"><p>که من هرگز نباشم از خود آگاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به گفتار از من آگاهی مجویید</p></div>
<div class="m2"><p>بجنبانیدم اول پس بگویید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز جنبانیدن اول با خود آیم</p></div>
<div class="m2"><p>وزان پس گوش بشنیدن گشایم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دل من هست با زندانی من</p></div>
<div class="m2"><p>از آنست این همه حیرانی من</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به خاطر هر که را آن ماه گردد</p></div>
<div class="m2"><p>کجا از دیگری آگاه گردد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بگشت از حال خود روزی مزاجش</p></div>
<div class="m2"><p>به زخم نشتر افتاد احتیاجش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز خونش بر زمین در دیده کس</p></div>
<div class="m2"><p>نیامد غیر یوسف یوسف و بس</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به کلک نشتر استاد سبکدست</p></div>
<div class="m2"><p>به لوح خاک نقش این حرف را بست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چنان از دوست پر بودش رگ و پوست</p></div>
<div class="m2"><p>که بیرون نامدش از پوست جز دوست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خوش آن کس کو رهایی یابد از خویش</p></div>
<div class="m2"><p>نسیم آشنایی یابد از خویش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کند در دل چنان جا دلبری را</p></div>
<div class="m2"><p>که گنجایی نماند دیگری را</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>درآید همچو جانش در رگ و پی</p></div>
<div class="m2"><p>نبیند یک سر مو خالی از وی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نه بویی باشدش از خود نه رنگی</p></div>
<div class="m2"><p>نه صلحی باشدش با کس نه جنگی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نه دل در تاج و نه در تخت بندد</p></div>
<div class="m2"><p>ز کوی او هوس ها رخت بندد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اگر گوید سخن با یار گوید</p></div>
<div class="m2"><p>وگر جوید مراد از یار جوید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نیارد خویشتن را در شماری</p></div>
<div class="m2"><p>نگیرد پیش غیر از عشق کاری</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>رخ اندر پختگی آرد ز خامی</p></div>
<div class="m2"><p>ز بود خود برون آید تمامی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تو هم جامی تمام از خود برون آی</p></div>
<div class="m2"><p>به دولتخانه سرمد درون آی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو دانم راه دولتخانه دانی</p></div>
<div class="m2"><p>نه از دولت بود چندین گرانی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بر این دام گران جانان قدم نه</p></div>
<div class="m2"><p>قدم در دولت آباد عدم نه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نبودی و زیانی زان نبودت</p></div>
<div class="m2"><p>مباش امروز هم کین است سودت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مجوی اندر خودی بهبود خود را</p></div>
<div class="m2"><p>کزین سودا نیابی سود خود را</p></div></div>