---
title: >-
    بخش ۳۸ - به معرض بیع درآوردن مالک یوسف را علیه السلام و خریدن زلیخا وی را به اضعاف آنچه دیگران می خریدند
---
# بخش ۳۸ - به معرض بیع درآوردن مالک یوسف را علیه السلام و خریدن زلیخا وی را به اضعاف آنچه دیگران می خریدند

<div class="b" id="bn1"><div class="m1"><p>چه خوش وقتی و خرم روزگاری</p></div>
<div class="m2"><p>که یاری بر خورد از وصل یاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برافروزد چراغ آشنایی</p></div>
<div class="m2"><p>رهایی یابد از داغ جدایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو یوسف شد به خوبی گرم بازار</p></div>
<div class="m2"><p>شدندش مصریان یکسر خریدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر چیزی که هر کس دسترس داشت</p></div>
<div class="m2"><p>در آن بازار بیع او هوس داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شنیدم کز غمش زالی برآشفت</p></div>
<div class="m2"><p>تنیده ریسمانی چند می گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همین بس گر چه بس کاسد قماشم</p></div>
<div class="m2"><p>که در سلک خریدارانش باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منادی بانگ می زد از چپ و راست</p></div>
<div class="m2"><p>که می خواهد غلامی بی کم و کاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخ او مطلع صبح صباحت</p></div>
<div class="m2"><p>لب او گوهر کان ملاحت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سیمای صلاحش چهره پر نور</p></div>
<div class="m2"><p>به اخلاق کرامش سینه معمور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیارد بر زبان جز راستی هیچ</p></div>
<div class="m2"><p>نباشد در کلام او خم و پیچ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی شد زان میانه اول کار</p></div>
<div class="m2"><p>به یک بدره زر سرخش خریدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازان بدره که چون خواهی شمارش</p></div>
<div class="m2"><p>بیابی از درست زر هزارش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خریداران دیگر رخش راندند</p></div>
<div class="m2"><p>به منزلگاه صد بدره رساندند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر آن افزود دولتمند دیگر</p></div>
<div class="m2"><p>به قدر وزن یوسف مشک اذفر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر آن دانای دیگر ساخت افزون</p></div>
<div class="m2"><p>به وزنش لعل ناب و در مکنون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدین قانون ترقی می نمودند</p></div>
<div class="m2"><p>ز انواع نفایس می فزودند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زلیخا گشت ازین معنی خبردار</p></div>
<div class="m2"><p>مضاعف ساخت آنها را به یک بار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خریداران دیگر لب ببستند</p></div>
<div class="m2"><p>پس زانوی نومیدی نشستند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عزیز مصر را گفت ای نکو رای</p></div>
<div class="m2"><p>برو بر مالک این قیمت بپیمای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگفتا آنچه من دارم دفینه</p></div>
<div class="m2"><p>ز مشک و گوهر و زر در خزینه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به یک نیمه بهایش برنیاید</p></div>
<div class="m2"><p>ادای آن تمام از من کی آید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زلیخا داشت درجی پر ز گوهر</p></div>
<div class="m2"><p>نه درجی بلکه برجی پر ز اختر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بهای هر گهر زان درج مکنون</p></div>
<div class="m2"><p>خراج مصر بودی بلکه افزون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگفتا کین گهرها در بهایش</p></div>
<div class="m2"><p>بده ای گوهر جانم فدایش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عزیز آورد باز از نو بهانه</p></div>
<div class="m2"><p>که دارد میل آن شاه زمانه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که در خیل وی این پاکیزه دامان</p></div>
<div class="m2"><p>بود سر دفتر دیگر غلامان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگفتا رو سوی شاه جهاندار</p></div>
<div class="m2"><p>حق خدمتگزاری را بجا آر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگو بر دل جز این بندی ندارم</p></div>
<div class="m2"><p>که پیش دیده فرزندی ندارم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سرافرازی فزا زین احترامم</p></div>
<div class="m2"><p>که آید زیر فرمان این غلامم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به برجم اختر تابنده باشد</p></div>
<div class="m2"><p>مرا فرزند و شه را بنده باشد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو شاه این نکته سنجیده بشنید</p></div>
<div class="m2"><p>ز بذل التماسش سر نپیچید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اجازت داد تا حالی خریدش</p></div>
<div class="m2"><p>ز مهر دل به فرزندی گزیدش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به سوی خانه بردش خرم و شاد</p></div>
<div class="m2"><p>زلیخا شد ز بند محنت آزاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به مژگان گوهر شادی همی سفت</p></div>
<div class="m2"><p>دو چشم خود همی مالید و می گفت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به بیداریست یا رب یا به خواب است</p></div>
<div class="m2"><p>که جان من ز جانان کامیاب است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به شبهای سیه کی بود امیدم</p></div>
<div class="m2"><p>که گردد روزی این روز سفیدم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شبم را صبح فیروزی برآمد</p></div>
<div class="m2"><p>غم و رنج شباروزی سرآمد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شدم با نازنین خویش همراز</p></div>
<div class="m2"><p>سزد اکنون که بر گردون کنم ناز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>درین محنتسرا بی غم چو من کیست</p></div>
<div class="m2"><p>پس از پژمردگی خرم چو من کیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چه بودم ماهیی در ماتم آب</p></div>
<div class="m2"><p>طپان بر ریگ تفسان از غم آب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>درآمد سیلی از ابر کرامت</p></div>
<div class="m2"><p>به دریا برد ازان ریگم سلامت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که بودم گمره در ظلمت شب</p></div>
<div class="m2"><p>رسیده جان ز گمراهیم بر لب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برآمد از افق رخشنده ماهی</p></div>
<div class="m2"><p>به کوی دولتم بنمود راهی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که بودم خفته ای بر بستر مرگ</p></div>
<div class="m2"><p>خلیده در رگ جان نشتر مرگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>درآمد ناگهان خضر از در من</p></div>
<div class="m2"><p>به آب زندگی شد یاور من</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بحمدالله که دولت یاریم کرد</p></div>
<div class="m2"><p>زمانه ترک جان آزاریم کرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هزاران جان فدای آن نکوکار</p></div>
<div class="m2"><p>که آورد اینچنین نقدی به بازار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چه غم گر حقه گوهر شکستم</p></div>
<div class="m2"><p>چو آمد معدن گوهر به دستم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به پیش نقد جان گوهر چه باشد</p></div>
<div class="m2"><p>طفیل دوست باشد هر چه باشد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جمادی چند دادم جان خریدم</p></div>
<div class="m2"><p>بنامیزد عجب ارزان خریدم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کی از نقد خود آن کس بهره بیند</p></div>
<div class="m2"><p>که عیسی بدهد و خر مهره چیند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اگر خرمهره را بدرود کردم</p></div>
<div class="m2"><p>چو عیسی آن من شد سود کردم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به شعر فکرت این اسرار می بیخت</p></div>
<div class="m2"><p>سرشک از چشم گوهربار می ریخت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گهی در روی یوسف لال می بود</p></div>
<div class="m2"><p>ز داغ هجر فارغ بال می بود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گه از هجر گذشته یاد می کرد</p></div>
<div class="m2"><p>به وصلش خاطر خود شاد می کرد</p></div></div>