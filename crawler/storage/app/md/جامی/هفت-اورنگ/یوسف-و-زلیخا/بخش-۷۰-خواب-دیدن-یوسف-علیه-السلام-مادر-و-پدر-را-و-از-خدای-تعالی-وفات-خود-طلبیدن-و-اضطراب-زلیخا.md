---
title: >-
    بخش ۷۰ - خواب دیدن یوسف علیه السلام مادر و پدر را و از خدای تعالی وفات خود طلبیدن و اضطراب زلیخا
---
# بخش ۷۰ - خواب دیدن یوسف علیه السلام مادر و پدر را و از خدای تعالی وفات خود طلبیدن و اضطراب زلیخا

<div class="b" id="bn1"><div class="m1"><p>زهی حسرت که ناگه نیکبختی</p></div>
<div class="m2"><p>کشد تا پیشگاه وصل رختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشیده شاهد دولت در آغوش</p></div>
<div class="m2"><p>کند اندوه هجران را فراموش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندیده خاطرش از غم غباری</p></div>
<div class="m2"><p>به شادی بگذراند روزگاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ناگه باد ادباری برآید</p></div>
<div class="m2"><p>سموم هجر را کاری برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درآید در ریاض وصل گستاخ</p></div>
<div class="m2"><p>درخت آرزو را بشکند شاخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلیخا چون ز یوسف کام دل یافت</p></div>
<div class="m2"><p>به وصل دایمش آرام دل یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دل خرم به خاطر شاد می زیست</p></div>
<div class="m2"><p>ز غم های جهان آزاد می زیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تمادی یافت ایام وصالش</p></div>
<div class="m2"><p>در آن دولت ز چل بگذشت سالش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیاپی داد آن نخل برومند</p></div>
<div class="m2"><p>بر فرزند بل فرزند فرزند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرادی از جهان در دل نبودش</p></div>
<div class="m2"><p>که بر خوان امل حاصل نبودش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شبی بنهاده سر یوسف به محراب</p></div>
<div class="m2"><p>ره بیداریش زد رهزن خواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پدر را دید با مادر نشسته</p></div>
<div class="m2"><p>به رخ چون خور نقاب نور بسته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندا کردند کای فرزند دریاب</p></div>
<div class="m2"><p>کشید ایام دوری دیر بشتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز ناخواهی بر آب و گل رقم نه</p></div>
<div class="m2"><p>به نزهتگاه جان و دل قدم نه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو یوسف یافت بیداری ازان خواب</p></div>
<div class="m2"><p>به پهلوی زلیخا شد ز محراب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حدیث خواب را با وی بیان کرد</p></div>
<div class="m2"><p>وز آن مقصود را بر وی عیان کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز خوابش با خیال دوری افکند</p></div>
<div class="m2"><p>به جانش آتش مهجوری افکند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ولی یوسف ز طور خود برون شد</p></div>
<div class="m2"><p>به اقلیم بقا شوقش فزون شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قدم زین تنگنای آز برداشت</p></div>
<div class="m2"><p>ره فسحتسرای راز برداشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>متاع انس ازین دیر فنا برد</p></div>
<div class="m2"><p>به محراب بقا دست دعا برد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که ای حاجت روای مستمندان</p></div>
<div class="m2"><p>به سر افسر نه تارک بلندان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به فرقم تاج اقبالی نهادی</p></div>
<div class="m2"><p>که هرگز هیچ مقبل را ندادی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دلم زین کشور فانی گرفته ست</p></div>
<div class="m2"><p>ز تدبیر جهانبانی گرفته ست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا فارغ ز من راهی به خود ده</p></div>
<div class="m2"><p>مثال شاهی ملک ابد ده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نکوکاران که راه دین گرفتند</p></div>
<div class="m2"><p>به قرب و منزلت پیشین گرفتند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برون آر از شمار واپسانم</p></div>
<div class="m2"><p>به فر قربت ایشان رسانم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زلیخا چون شنید این رازداری</p></div>
<div class="m2"><p>به دل زخمی رسیدش سخت کاری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یقین دانست کز وی آن دعا را</p></div>
<div class="m2"><p>اثر گردد به زودی آشکارا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیاید از کمان او خدنگی</p></div>
<div class="m2"><p>که در تأثیر آن افتد درنگی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قدم در کلبه ای زد تیره و تنگ</p></div>
<div class="m2"><p>گشاد از یکدگر گیسوی شبرنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همی کرد از غم دوری به سر خاک</p></div>
<div class="m2"><p>همی مالید پر خون چهره بر خاک</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز شادی طاق و با اندوه و غم جفت</p></div>
<div class="m2"><p>ز دیده اشک می بارید و می گفت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که ای درمان درد دردناکان</p></div>
<div class="m2"><p>به مرهم خرقه دوز سینه چاکان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مراد خاطر هر نامرادی</p></div>
<div class="m2"><p>گشاد ششدر هر بی گشادی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مفاتیح آور درهای بسته</p></div>
<div class="m2"><p>جبایر بند دلهای شکسته</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خلاصی بخش مهجوران ز اندوه</p></div>
<div class="m2"><p>سبک سازنده غم های چون کوه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گرفتار دل افگار خویشم</p></div>
<div class="m2"><p>عجب حیران شده در کار خویشم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ندارم طاقت هجران یوسف</p></div>
<div class="m2"><p>ز تن کش جان من با جان یوسف</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نخواهم بی جمالش زندگی را</p></div>
<div class="m2"><p>به ملک زندگی پایندگی را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نهال عمر بی برگ است بی او</p></div>
<div class="m2"><p>حیات جاودان مرگ است بی او</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به قانون وفا نیکو نباشد</p></div>
<div class="m2"><p>که من باشم به گیتی او نباشد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر با من نسازی همره او را</p></div>
<div class="m2"><p>مرا بیرون بر اول آنگه او را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نمی خواهم کزو یکسو نشینم</p></div>
<div class="m2"><p>جهان را بی جمال او ببینم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به سر برد اینچنین در گریه و سوز</p></div>
<div class="m2"><p>نه شب را گفت شب نی روز را روز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بلی هر کس ز غم دارد دلی تنگ</p></div>
<div class="m2"><p>شب و روزش نماید هر دو یکرنگ</p></div></div>