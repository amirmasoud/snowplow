---
title: >-
    بخش ۱۰ - در تمدح سلطانی که به موجب مدح السلطان یستنزل الامان مدحت او طیب زندگانی را ضمان است و مادح او از فوت امانی در امان
---
# بخش ۱۰ - در تمدح سلطانی که به موجب مدح السلطان یستنزل الامان مدحت او طیب زندگانی را ضمان است و مادح او از فوت امانی در امان

<div class="b" id="bn1"><div class="m1"><p>جهان یکسر چه ارواح و چه اجسام</p></div>
<div class="m2"><p>بود شخص معین عالمش نام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود انسان درین شخص معین</p></div>
<div class="m2"><p>چو عین باصره در چشم روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین عین آن که چون انسان عین است</p></div>
<div class="m2"><p>جهان مردمی سلطان حسین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زیر این خمیده طاق مینا</p></div>
<div class="m2"><p>دو چشم آدمیت زوست بینا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشا چشمی که بینایی ازو یافت</p></div>
<div class="m2"><p>به بینایی توانایی ازو یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک صد چشم دارد بر ره او</p></div>
<div class="m2"><p>که چشم خود کند منزلگه او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز روی اوست روشن چشم عالم</p></div>
<div class="m2"><p>به بوی اوست گلشن خاک آدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حسن خلق و لطف خلق بی قیل</p></div>
<div class="m2"><p>بود یوسف درین مصر فلک نیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در اصلابش کرم رسمی قدیم است</p></div>
<div class="m2"><p>کریم ابن الکریم ابن الکریم است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سزد گر از کمال خوبی او</p></div>
<div class="m2"><p>کند پیر فلک یعقوبی او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز کف بحر نوال آورده در مشت</p></div>
<div class="m2"><p>کشیده جویباری از هر انگشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو صد کشت امل در هر دیاری</p></div>
<div class="m2"><p>شده سرسبز از هر جویباری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز دستش کابر و یم هستند ازان کم</p></div>
<div class="m2"><p>خروشان باشد ابر و کف زنان یم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نموده لمعه ای از زرفشان تیغ</p></div>
<div class="m2"><p>نهفته تیغ خود خورشید در میغ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو گشته برق تیغش پرتوافکن</p></div>
<div class="m2"><p>جهان را کرده چون خورشید روشن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دو دم یک برق را گر چه بقا نیست</p></div>
<div class="m2"><p>بقا از تیغ او یکدم جدا نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بقای او فنای تیرگی هاست</p></div>
<div class="m2"><p>نیاید روشنی با تیرگی راست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز عدل او به وقت خواب شبگیر</p></div>
<div class="m2"><p>کند نطع از پلنگ خفته نخچیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز شبگردی چو یابد گرگ مالش</p></div>
<div class="m2"><p>نهد از دنبه میشش گرد بالش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پی جذب محبت چنگل باز</p></div>
<div class="m2"><p>شود قلاب مرغ تیز پرواز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درخت بیشه ای پر شاخ و پیوند</p></div>
<div class="m2"><p>اگر شاخ گوزنی را کند بند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کند شیر ژیان مشکل گشایی</p></div>
<div class="m2"><p>به پنجه بخشد از بندش رهایی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کمینگاه بد اندیشان بی باک</p></div>
<div class="m2"><p>بود ز اندیشه ناایمنی پاک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر یک تن بود چون مهر انور</p></div>
<div class="m2"><p>ز مشرق تا به مغرب طشتی از زر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نیارد هیچ عور از درع پرهیز</p></div>
<div class="m2"><p>که در طشت زر او بنگرد تیز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو صبح آنجا که لطف او بخندد</p></div>
<div class="m2"><p>چو ظلمت ظلم از آنجا رخت بندد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو برق آنجا که قهرش بر فروزد</p></div>
<div class="m2"><p>به یک شعله جهانی را بسوزد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خداوندا به پیران جوانبخت</p></div>
<div class="m2"><p>که تا هست آسمان چتر و زمین تخت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به زیر پای تخت شاهیش باد</p></div>
<div class="m2"><p>به تارک چتر ظل اللهیش باد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فلک با چتر او در چاپلوسی</p></div>
<div class="m2"><p>زمین با تخت او در خاکبوسی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خراب آباد عالم باد معمور</p></div>
<div class="m2"><p>به اولاد کرامش تا دم صور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به تخصیص آن که چرخ آمد مطیعش</p></div>
<div class="m2"><p>زمان را تاج سر نام بدیعش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زمانش آن عجم از وی مشرف</p></div>
<div class="m2"><p>به تعریف عرب بادا معرف</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جهان را تا بلندی هست و پستی</p></div>
<div class="m2"><p>مباد این نام پاک از لوح هستی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دگر شهزاده کز بخت مظفر</p></div>
<div class="m2"><p>به طفلی شد طفیلش تخت و افسر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خرد چون دید جاه و احترامش</p></div>
<div class="m2"><p>همی کرد آرزو نقشی ز نامش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>درین میدان که بادا خالی از درد</p></div>
<div class="m2"><p>فلک طاس تهی را پر فرح کرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز بزمش خور یکی زرین قدح باد</p></div>
<div class="m2"><p>دلش چون نام دایم پر فرح باد</p></div></div>