---
title: >-
    بخش ۷۱ - وفات یافتن یوسف علیه السلام و هلاک شدن زلیخا از الم مفارقت وی
---
# بخش ۷۱ - وفات یافتن یوسف علیه السلام و هلاک شدن زلیخا از الم مفارقت وی

<div class="b" id="bn1"><div class="m1"><p>به دیگر روز یوسف بامدادان</p></div>
<div class="m2"><p>که شد دلها ز فیض صبح شادان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بر کرده لباس شهریاری</p></div>
<div class="m2"><p>برون آمد به آهنگ سواری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو پا در یک رکاب آورد جبریل</p></div>
<div class="m2"><p>بدو گفتا مکن زین بیش تعجیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امان نبود ز چرخ عمر فرسای</p></div>
<div class="m2"><p>که ساید بر رکاب دیگرت پای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنان بگسل ز آمال و امانی</p></div>
<div class="m2"><p>بکش پا از رکاب زندگانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو یوسف این بشارت کرد ازو گوش</p></div>
<div class="m2"><p>زشادی شد بر او هستی فراموش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شاهی دامن همت برافشاند</p></div>
<div class="m2"><p>یکی از وارثان ملک را خواند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جای خود شه آن مرز کردش</p></div>
<div class="m2"><p>به خصلت های نیک اندرز کردش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر گفتا زلیخا را بخوانید</p></div>
<div class="m2"><p>به میعاد وداع من رسانید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتند او به دست غم زبون است</p></div>
<div class="m2"><p>فتاده در میان خاک و خون است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندارد طاقت این بار جانش</p></div>
<div class="m2"><p>به کار خویش بگذار آنچنانش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتا ترسم این داغ غرامت</p></div>
<div class="m2"><p>بماند بر دل او تا قیامت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفتند ایزدش خرسند داراد</p></div>
<div class="m2"><p>به خرسندی قوی پیوند داراد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به کف جبریل حاضر داشت سیبی</p></div>
<div class="m2"><p>که باغ خلد ازان می داشت زیبی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو یوسف را به دست آن سیب بنهاد</p></div>
<div class="m2"><p>روان آن سیب را بویید و جان داد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بلی زان نکهت باغ بقا یافت</p></div>
<div class="m2"><p>ازان نکهت به سوی باغ بشتافت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو یوسف را ازان بو جان برآمد</p></div>
<div class="m2"><p>ز جان حاضران افغان برآمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز بس بالا گرفت آواز فریاد</p></div>
<div class="m2"><p>صدا در گنبد فیروزه افتاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زلیخا گفت کین شور و فغان چیست</p></div>
<div class="m2"><p>پر از غوغا زمین و آسمان چیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدو گفتند کان شاه جوانبخت</p></div>
<div class="m2"><p>به سوی تخته رو کرد از سر تخت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وداع کلبه تنگ جهان کرد</p></div>
<div class="m2"><p>وطن بر اوج کاخ لا مکان کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو بشنید این سخن از خویشتن رفت</p></div>
<div class="m2"><p>فروغ نیر هوشش ز تن رفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز هول این حدیث آن سرو چالاک</p></div>
<div class="m2"><p>سه روز افتاد همچون سایه بر خاک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو چارم روز شد زان خواب بیدار</p></div>
<div class="m2"><p>سماع آن ز خود بردش دگر بار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سه بار اینسان سه روز از خود همی رفت</p></div>
<div class="m2"><p>به داغ سینه سوز از خود همی رفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چهارم روز چون آمد به خود باز</p></div>
<div class="m2"><p>ز یوسف کرد اول پرسش آغاز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه از وی بر سر بستر نشان یافت</p></div>
<div class="m2"><p>نه تابوتش به آن عالم روان یافت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جز این از وی خبر بازش ندادند</p></div>
<div class="m2"><p>که همچون گنج در خاکش نهادند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نخست از دور چرخ ناموافق</p></div>
<div class="m2"><p>گریبان چاک زد چون صبح صادق</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر آن آتش که بر دل داشت پنهان</p></div>
<div class="m2"><p>رهی بگشاد از چاک گریبان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ولی زان راه در جانش به هر دم</p></div>
<div class="m2"><p>فزون گشت آتش سوزنده نی کم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به ناخن رخنه ها در روی می کند</p></div>
<div class="m2"><p>برای چشمه خور جوی می کند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به هر جویی کز آن چشمه روان کرد</p></div>
<div class="m2"><p>سمن را جلوه گاه ارغوان کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شد از ناخن به رخ گلگون خط افکن</p></div>
<div class="m2"><p>چو عرق ناخنه در چشم روشن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به سینه از تغابن سنگ می زد</p></div>
<div class="m2"><p>طپانچه بر رخ گلرنگ می زد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز سیم آنجا عقیق تر همی رست</p></div>
<div class="m2"><p>وز این بر لاله نیلوفر همی رست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به سوی فرق نازک برد پنجه</p></div>
<div class="m2"><p>ز زور پنجه آن را ساخت رنجه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز ریحان سرو بستان را سبک کرد</p></div>
<div class="m2"><p>به چیدن سنبلستان را تنک کرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز دل نوحه ز جان فریاد برداشت</p></div>
<div class="m2"><p>فغان از سینه ناشاد برداشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که یوسف کو و تخت آرایی او</p></div>
<div class="m2"><p>به محتاجان کرم فرمایی او</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو عزمش کرد زین بر بارگی تنگ</p></div>
<div class="m2"><p>به ملک جاودانی داشت آهنگ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز بس بود اندرین رفتن شتابش</p></div>
<div class="m2"><p>نکردم پایبوسی چون رکابش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ازین کاخ غم افزا چون برون رفت</p></div>
<div class="m2"><p>نبودم در حضور او که چون رفت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سرش بنهاده بر بالین ندیدم</p></div>
<div class="m2"><p>خویش از صفحه نسرین نچیدم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو آمد بر تن آن زخم درشتش</p></div>
<div class="m2"><p>نکردم سینه پشتیبان پشتش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو سوی تخته برد از تختگه رخت</p></div>
<div class="m2"><p>همایون بخت شد زو تخته چون تخت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گلاب از چشم اشک افشان نجستم</p></div>
<div class="m2"><p>به آن روشن گلاب او را نشستم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کفن چون بر تن او راست کردند</p></div>
<div class="m2"><p>به تکفینش نشست و خاست کردند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نکردم رشته اندوزی فن خویش</p></div>
<div class="m2"><p>که تا دوزم بر او لاغر تن خویش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو از غم خارها در دل شکستند</p></div>
<div class="m2"><p>وز این سر منزلش محمل ببستند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زبان پر از نوای بینوایی</p></div>
<div class="m2"><p>نکردم محمل او را درایی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو جای خواب در خاکش گشادند</p></div>
<div class="m2"><p>چو در پاک در خاکش نهادند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زمین زیر بر و دوشش نرفتم</p></div>
<div class="m2"><p>به کام دل به آغوشش نخفتم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دریغا زین زیانکاری دریغا</p></div>
<div class="m2"><p>دریغا زین جگرخواری دریغا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بیا ای کام جان محرومیم بین</p></div>
<div class="m2"><p>ز ظلم آسمان مظلومیم بین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بریدی از من و یادم نکردی</p></div>
<div class="m2"><p>به دیداری ز خود شادم نکردی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>وفادارا وفاداری نه این بود</p></div>
<div class="m2"><p>به یاران شیوه یاری نه این بود</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مرا از دل برون افکندی و رفت</p></div>
<div class="m2"><p>میان خاک و خون افکندی و رفت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عجب خاری شکستی در دل من</p></div>
<div class="m2"><p>که بیرون ناید الا از گل من</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نه جایی راه رفتن کرده ای ساز</p></div>
<div class="m2"><p>کز آنجا هیچگه آید کسی باز</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>همان بهتر کز اینجا پر گشایم</p></div>
<div class="m2"><p>به یک پرواز کردن سویت آیم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بگفت این و عماری دار را خواست</p></div>
<div class="m2"><p>به روی خود عماری را بیاراست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به یک جنبش ازان اندوه خانه</p></div>
<div class="m2"><p>به رحلتگاه یوسف شد روانه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ندید آنجا نشان زان گوهر پاک</p></div>
<div class="m2"><p>به جز خرپشته ای از خاک نمناک</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بر آن خرپشته آن خورشید پایه</p></div>
<div class="m2"><p>به خاک انداخت خود را همچو سایه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز رخسار چو خور در زر گرفتش</p></div>
<div class="m2"><p>ز اشک لعل در گوهر گرفتش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گهی فرقش همی بوسید و گه پای</p></div>
<div class="m2"><p>فغان می زد ز دل کای وای من وای</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تو زیر گل چو بیخ گل نهفته</p></div>
<div class="m2"><p>به بالا من چو شاخ گل شگفته</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تو زیر خاک منزل کرده چون گنج</p></div>
<div class="m2"><p>به روی خاک من ابر گهرسنج</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>فرو رفته تو همچون آب در خاک</p></div>
<div class="m2"><p>به بیرون مانده من چون خار و خاشاک</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>خیالت موج خون بر خاک من زد</p></div>
<div class="m2"><p>فراقت شعله در خاشاک من زد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زدی آتش به خاشاک وجودم</p></div>
<div class="m2"><p>ازان پیچان رود بر چرخ دودم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به دود من کسی نگشاده دیده</p></div>
<div class="m2"><p>که نی از دیدگان آبش چکیده</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همی نالید و هر دم سینه چاک</p></div>
<div class="m2"><p>به صد حسرت همی مالید بر خاک</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چو درد و حسرتش از حد برون شد</p></div>
<div class="m2"><p>به رسم خاکبوسی سرنگون شد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به چشمان خود انگشتان درآورد</p></div>
<div class="m2"><p>دو نرگس را ز نرگسدان برآورد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به خاک وی فکند از کاسه سر</p></div>
<div class="m2"><p>که نرگس کاشتن در خاک بهتر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو باشد از گل رویت جدا چشم</p></div>
<div class="m2"><p>چه کار آید درین بستان مرا چشم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بود رسم مصیبت بین مبهوت</p></div>
<div class="m2"><p>سیه بادام افشاندن به تابوت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو آن مسکین ز تابوتش جدا ماند</p></div>
<div class="m2"><p>دو بادام سیه بر خاکش افشاند</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به خاکش روی خون آلود بنهاد</p></div>
<div class="m2"><p>به مسکینی زمین بوسید و جان داد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>خوش آن عاشق که چون جانش برآید</p></div>
<div class="m2"><p>به بوی وصل جانانش برآید</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>حریفان حال او را چون بدیدند</p></div>
<div class="m2"><p>فغان و ناله بر گردون کشیدند</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>هر آن نوحه که بهر یوسف او کرد</p></div>
<div class="m2"><p>همی کردند بر وی با دو صد درد</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>همی کردند نوحه نوحه گر را</p></div>
<div class="m2"><p>به سان نوحه گر آن سیمبر را</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو ساز نوحه را آهنگ شد پست</p></div>
<div class="m2"><p>نوردیدند بهر شستنش دست</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بشستندش ز دیده اشکباران</p></div>
<div class="m2"><p>چو برگ گل ز باران بهاران</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به سان غنچه کز شاخ سمن رست</p></div>
<div class="m2"><p>بر او کردند زنگاری کفن چست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز گرد فرقتش رخ پاک کردند</p></div>
<div class="m2"><p>به جنب یوسفش در خاک کردند</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ندیده هرگز این دولت کس از مرگ</p></div>
<div class="m2"><p>که یابد صحبت جانان پس از مرگ</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ولی دانای این شیرین حکایت</p></div>
<div class="m2"><p>که دارد از کهن پیران روایت</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چنین گوید که با هر جانب از نیل</p></div>
<div class="m2"><p>که جسم پاک یوسف یافت تحویل</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به دیگر جانبش قحط و وبا خاست</p></div>
<div class="m2"><p>به جای نعمت انواع بلا خاست</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بر این آخر قرار کار دادند</p></div>
<div class="m2"><p>که در تابوتی از سنگش نهادند</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>شکاف سنگ قیر اندای کردند</p></div>
<div class="m2"><p>میان قعر نیلش جای کردند</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ببین حیله که چرخ بی وفا کرد</p></div>
<div class="m2"><p>که بعد از مرگش از یوسف جدا کرد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>نمی دانم که با ایشان چه کین داشت</p></div>
<div class="m2"><p>که زیر خاکشان آسوده نگذاشت</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>یکی شد غرق بحر آشنایی</p></div>
<div class="m2"><p>یکی لب تشنه در بر جدایی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چه خوش گفت آن قدم فرسوده در عشق</p></div>
<div class="m2"><p>ز هر سود و زیان آسوده در عشق</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>که عشق آنجا که باشد گرم بازار</p></div>
<div class="m2"><p>ندارد هیچ با آسودگی کار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>کفن بر عاشق از وی چاک باشد</p></div>
<div class="m2"><p>اگر خود خفته زیر خاک باشد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>خوش آن عاشق که در هجران چنین مرد</p></div>
<div class="m2"><p>به خلوتگاه جانان جان چنین برد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>نگوید کس که مردی در کفن رفت</p></div>
<div class="m2"><p>بدین مردانگی کان شیرزن رفت</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>نخست از غیر جانان دیده بر کند</p></div>
<div class="m2"><p>وز آن پس نقد جان بر خاکش افکند</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>هزاران فیض بر جان و تنش باد</p></div>
<div class="m2"><p>به جانان دیده جان روشنش باد</p></div></div>