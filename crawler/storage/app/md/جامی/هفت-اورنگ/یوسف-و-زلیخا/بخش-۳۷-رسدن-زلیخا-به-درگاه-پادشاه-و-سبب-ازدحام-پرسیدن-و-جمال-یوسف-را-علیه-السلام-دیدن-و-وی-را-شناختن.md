---
title: >-
    بخش ۳۷ - رسدن زلیخا به درگاه پادشاه و سبب ازدحام پرسیدن و جمال یوسف را علیه السلام دیدن و وی را شناختن
---
# بخش ۳۷ - رسدن زلیخا به درگاه پادشاه و سبب ازدحام پرسیدن و جمال یوسف را علیه السلام دیدن و وی را شناختن

<div class="b" id="bn1"><div class="m1"><p>زلیخا بود ازین صورت تهی دل</p></div>
<div class="m2"><p>کزو تا یوسف آمد یک دو منزل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولی جانش ازان معنی خبر داشت</p></div>
<div class="m2"><p>ز داغ شوق سوزی در جگر داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی دانست کان شوق از کجا خاست</p></div>
<div class="m2"><p>به حیلت سازیش تسکین همی خواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به صحرا شد برون تا زان بهانه</p></div>
<div class="m2"><p>ز دل بیرون دهد اندوه خانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سختی چند روز آنجا به سر برد</p></div>
<div class="m2"><p>بر آن محنت بسی دندان بیفشرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفت اسباب عیش و خرمی پیش</p></div>
<div class="m2"><p>ولی هر لحظه شد اندوه او بیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو در صحرا به خرمن سیلش افتاد</p></div>
<div class="m2"><p>دگر باره به خانه میلش افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پشت بارگی هودج نشین شد</p></div>
<div class="m2"><p>به منزلگاه خود رحلت گزین شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه روی در منزلگهش بود</p></div>
<div class="m2"><p>گذر بر ساحت قصر شهش بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دید آن انجمن گفت این چه غوغاست</p></div>
<div class="m2"><p>که گویی رستخیز از مصر برخاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی گفت این پی فرخنده نامیست</p></div>
<div class="m2"><p>بساط عرض کنعانی غلامیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غلامی نی که رخشان آفتابی</p></div>
<div class="m2"><p>به دارالملک خوبی کامیابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زلیخا دامن هودج برانداخت</p></div>
<div class="m2"><p>چو چشمش بر غلام افتاد بشناخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برآمد از دلش بی خواست فریاد</p></div>
<div class="m2"><p>ز فریادی که زد بی خود بیفتاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روان هودج کشان هودج براندند</p></div>
<div class="m2"><p>به خلوتخانه خاصش رساندند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو شد منزلگهش آن خلوت راز</p></div>
<div class="m2"><p>ز حال بی خودی آمد به خود باز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ازو پرسید دایه کای دلفروز</p></div>
<div class="m2"><p>چرا کردی فغان از جان پر سوز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لب شیرین به افغان چون گشادی</p></div>
<div class="m2"><p>بدان تلخی چرا بی خود فتادی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفت ای مهربان مادر چه گویم</p></div>
<div class="m2"><p>که گردد آفت من هر چه گویم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در آن مجمع غلامی را که دیدی</p></div>
<div class="m2"><p>ز اهل مصر وصف او شنیدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز عالم قبله گاه جان من اوست</p></div>
<div class="m2"><p>فدایش جان من جانان من اوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به خوابم روی زیبا وی نموده ست</p></div>
<div class="m2"><p>شکیب از جان شیدا وی ربوده ست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به تن در تب به دل در تاب ازویم</p></div>
<div class="m2"><p>ز دیده غرق خون ناب ازویم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درین کشور ز سودایش فتادم</p></div>
<div class="m2"><p>بدین شهر از تمنایش فتادم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز خان و مان مرا آواره او ساخت</p></div>
<div class="m2"><p>درین آوارگی بیچاره او ساخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به هر محنت که دیدی چند سالم</p></div>
<div class="m2"><p>که بود از راحت گیتی ملالم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه از آرزوی روی او بود</p></div>
<div class="m2"><p>ز شوق قامت دلجوی او بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز کوه افزون بود بار من امروز</p></div>
<div class="m2"><p>ندانم چون شود کار من امروز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مه من شاه ایوان که گردد</p></div>
<div class="m2"><p>به رخ شمع شبستان که گردد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کدامین دیده گردد روشن از وی</p></div>
<div class="m2"><p>کدامین خانه گردد گلشن از وی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که یابد از لب جانبخش او کام</p></div>
<div class="m2"><p>که گیرد در پناه سروش آرام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کمند جعد مشکینش که بافد</p></div>
<div class="m2"><p>ز وصل نخل سیمینش که لافد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که بازد حاصل خود در بهایش</p></div>
<div class="m2"><p>که سازد کحل دیده خاک پایش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرا به گردد از وی حال یا نی</p></div>
<div class="m2"><p>رسد دستم بدین اقبال یا نی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو دایه آتش او دید کز چیست</p></div>
<div class="m2"><p>چو شمع از آتش او زار بگریست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگفت ای شمع سوز خود نهان دار</p></div>
<div class="m2"><p>غم شب رنج روز خود نهان دار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>صبوری پیشه کردی روزگاری</p></div>
<div class="m2"><p>مکن جز صبر نیز امروز کاری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بود کز صبر امیدت برآید</p></div>
<div class="m2"><p>ز ابر تیره خورشیدت برآید</p></div></div>