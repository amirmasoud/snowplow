---
title: >-
    بخش ۱۱ - در بیان آنکه هر یک از جمال و عشق مرغیست از آشیان وحدت پریده و بر شاخسار مظار کثرت آرمیده اگر نوای عزت معشوقیست از آنجاست و اگر ناله محنت عاشقیست هم از آنجاست
---
# بخش ۱۱ - در بیان آنکه هر یک از جمال و عشق مرغیست از آشیان وحدت پریده و بر شاخسار مظار کثرت آرمیده اگر نوای عزت معشوقیست از آنجاست و اگر ناله محنت عاشقیست هم از آنجاست

<div class="b" id="bn1"><div class="m1"><p>در آن خلوت که هستی بی نشان بود</p></div>
<div class="m2"><p>به کنج نیستی عالم نهان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وجودی بود از نقش دویی دور</p></div>
<div class="m2"><p>ز گفت و گوی مایی و تویی دور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمال مطلق از قید مظاهر</p></div>
<div class="m2"><p>به نور خویش هم بر خویش ظاهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلارا شاهدی در حجله غیب</p></div>
<div class="m2"><p>مبرا دامنش از تهمت عیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه با آیینه رویش در میانه</p></div>
<div class="m2"><p>نه زلفش را کشیده دست شانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبا از طره اش نگسسه تاری</p></div>
<div class="m2"><p>ندیده چشمش از سرمه غباری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگشته با گلش همسایه سنبل</p></div>
<div class="m2"><p>نبسته سبزه ای پیرایه بر گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخش ساده ز هر خطی و خالی</p></div>
<div class="m2"><p>ندیده هیچ چشمی زو خیالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوای دلبری با خویش می ساخت</p></div>
<div class="m2"><p>قمار عاشقی با خویش می باخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولی زانجا که حکم خوبروییست</p></div>
<div class="m2"><p>ز پرده خوبرو در تنگ خوییست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نکو رو تاب مستوری ندارد</p></div>
<div class="m2"><p>ببندی در ز روزن سر برآرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نظر کن لاله را در کوهساران</p></div>
<div class="m2"><p>که چون خرم شود فصل بهاران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کند شق شقه گلریز خارا</p></div>
<div class="m2"><p>جمال خود کند زان آشکارا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو را چون معنیی در خاطر افتد</p></div>
<div class="m2"><p>که در سلک معانی نادر افتد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیاری از خیال آن گذشتند</p></div>
<div class="m2"><p>دهی بیرون به گفتن یا نوشتن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو هر جا هست حسن اینش تقاضاست</p></div>
<div class="m2"><p>نخست این جنبش از حسن ازل خاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برون زد خیمه ز اقلیم تقدس</p></div>
<div class="m2"><p>تجلی کرد بر آفاق و انفس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز هر آیینه ای بنمود رویی</p></div>
<div class="m2"><p>به هر جا خاست از وی گفت و گویی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ازو یک لمعه بر ملک و ملک تافت</p></div>
<div class="m2"><p>ملک سرگشته خود را چون فلک یافت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه سبوحیان سبوح جویان</p></div>
<div class="m2"><p>شدند از بیخودی سبوح گویان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز غواصان این بحر فلک فلک</p></div>
<div class="m2"><p>برآمد غلغل سبحان ذی الملک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ازان لمعه فروغی بر گل افتاد</p></div>
<div class="m2"><p>ز گل شوری به جان بلبل افتاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رخ خود شمع ازان آتش برافروخت</p></div>
<div class="m2"><p>به هر کاشانه صد پروانه را سوخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز نورش تافت بر خورشید یک تاب</p></div>
<div class="m2"><p>برون آورد نیلوفر سر از آب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز رویش روی خویش آراست لیلی</p></div>
<div class="m2"><p>به هر مویش ز مجنون خاست میلی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لب شیرین به شکر ریز بگشاد</p></div>
<div class="m2"><p>دل از پرویز برد و جان ز فرهاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سر از جیب مه کنعان برآورد</p></div>
<div class="m2"><p>زلیخا را دمار از جان برآورد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جمال اوست هر جا جلوه کرده</p></div>
<div class="m2"><p>ز معشوقان عالم بسته پرده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به هر پرده که بینی پردگی اوست</p></div>
<div class="m2"><p>قضا جنبان هر دل بردگی اوست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به عشق اوست دل را زندگانی</p></div>
<div class="m2"><p>به عشق اوست جان را کامرانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دلی کو عاشق خوبان دلجوست</p></div>
<div class="m2"><p>اگر داند وگر نی عاشق اوست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هلا تا نغلطی ناگه نگویی</p></div>
<div class="m2"><p>که از ما عاشقی وز وی نکویی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که همچون نیکویی عشق ستوده</p></div>
<div class="m2"><p>ازو سر برزده در تو نموده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تویی آیینه او آیینه آرا</p></div>
<div class="m2"><p>تویی پوشیده و او آشکارا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو نیکو بنگری آیینه هم اوست</p></div>
<div class="m2"><p>نه تنها گنج او گنجینه هم اوست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من و تو در میان کاری نداریم</p></div>
<div class="m2"><p>بجز بیهوده پنداری نداریم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خمش کین قصه پایانی ندارد</p></div>
<div class="m2"><p>زبانی و زبان دانی ندارد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همان بهتر که هم در عشق پیچیم</p></div>
<div class="m2"><p>که بی این گفت و گو هیچیم هیچیم</p></div></div>