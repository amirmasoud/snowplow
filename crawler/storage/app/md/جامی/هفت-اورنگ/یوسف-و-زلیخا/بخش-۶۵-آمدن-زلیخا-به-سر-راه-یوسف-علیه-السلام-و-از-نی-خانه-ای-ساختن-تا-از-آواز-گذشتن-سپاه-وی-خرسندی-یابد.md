---
title: >-
    بخش ۶۵ - آمدن زلیخا به سر راه یوسف علیه السلام و از نی خانه ای ساختن تا از آواز گذشتن سپاه وی خرسندی یابد
---
# بخش ۶۵ - آمدن زلیخا به سر راه یوسف علیه السلام و از نی خانه ای ساختن تا از آواز گذشتن سپاه وی خرسندی یابد

<div class="b" id="bn1"><div class="m1"><p>زلیخا را ز تنهایی چو جان کاست</p></div>
<div class="m2"><p>به راه یوسف از نی خانه ای خواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو کردند نی بستی حواله</p></div>
<div class="m2"><p>چو موسیقار پر فریاد و ناله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو کردی از جدایی ناله آغاز</p></div>
<div class="m2"><p>جدا برخاستی از هر نی آواز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو از هجر آتش اندر وی گرفتی</p></div>
<div class="m2"><p>ز آهش شعله در هر نی گرفتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن نی بست بود افتاده خسته</p></div>
<div class="m2"><p>چو صیدی تیرها گردش نشسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولی از ذوق عشقش چون اثر بود</p></div>
<div class="m2"><p>بر او هر تیرگویی نیشکر بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آخر داشت یوسف دیوزادی</p></div>
<div class="m2"><p>سپهر اندازه ای گردون نهادی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تکاور ابلقی چون چرخ فیروز</p></div>
<div class="m2"><p>ز شب بسته هزاران وصله بر روز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نور و ظلمت اندر وی نشانه</p></div>
<div class="m2"><p>برابر چون شب و روز زمانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گره بر خوشه چرخ از دم او</p></div>
<div class="m2"><p>شکن در کاسه بدر از سم او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هر سمش هلالی بسته از زر</p></div>
<div class="m2"><p>ز سیم اختر رخشان مسمر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به زخم سم چو سنگ خاره خستی</p></div>
<div class="m2"><p>ز هر ماه نوش سیاره جستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر نعلش پریدی در تک و دو</p></div>
<div class="m2"><p>به چرخ اندر نشستی چون مه نو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گذشتی در شکارستان نخجیر</p></div>
<div class="m2"><p>پران از پهلوی نخجیر چون تیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرش میدان شدی از غرب تا شرق</p></div>
<div class="m2"><p>به یک جستن پریدی گرم چون برق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر گردش نه پا زو پس کشیدی</p></div>
<div class="m2"><p>به گردش باد صرصر کی رسیدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به راه ار چه شدی پر قطره از خوی</p></div>
<div class="m2"><p>ندیدی هیچ کس یک قطره از وی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به خوش رفتن در آن خوی بودیش میل</p></div>
<div class="m2"><p>چو آن گرد آمده از قطره ها سیل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو گنجی بود از گوهر روانه</p></div>
<div class="m2"><p>بری ز آسیب مار تازیانه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر آخر گر شدی رام و فروتن</p></div>
<div class="m2"><p>گرفتی خدمتش گردون به گردن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدادیش ار درآوردی به آن سر</p></div>
<div class="m2"><p>به سطل ماه آب از چشمه خور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مهیا ساختی در هر شبانگاه</p></div>
<div class="m2"><p>جوش از سنبله وز کهکشان کاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز شعر چشمه دار شب مه و سال</p></div>
<div class="m2"><p>پی جو کردیش آماده غربال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز سدره سبحه خوان مرغان گزیدی</p></div>
<div class="m2"><p>که تا سنگ از جوش چون دانه چیدی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دو پیکر بود از زینش مثالی</p></div>
<div class="m2"><p>رکاب از هر طرف تابان هلالی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو یوسف در هلالش پای کردی</p></div>
<div class="m2"><p>چو ماه اندر دو پیکر جای کردی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کشیدی زیر ران او صهیلی</p></div>
<div class="m2"><p>که رفتی هر طرف اضعاف میلی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به هر جا هر که بشنیدی صهیلش</p></div>
<div class="m2"><p>نبودی حاجت کوس رحیلش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شتابان سوی آن شاه آمدندی</p></div>
<div class="m2"><p>چو سیاره پی ماه آمدندی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زلیخا نیز چون آن را شنیدی</p></div>
<div class="m2"><p>ازان نی بست خود بیرون خزیدی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به حسرت بر سر راهش نشستی</p></div>
<div class="m2"><p>خروشان بر گذرگاهش نشستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو بی یوسف رسیدی خیلی از راه</p></div>
<div class="m2"><p>به طنزش کودکان کردندی آگاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که اینک در رسید از راه یوسف</p></div>
<div class="m2"><p>به رویی رشک مهر و ماه یوسف</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زلیخا گفتی از یوسف در اینان</p></div>
<div class="m2"><p>نمی یابم نشان ای نازنینان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به دل زین طنز مپسندید داغم</p></div>
<div class="m2"><p>که ناید بوی یوسف در دماغم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به هر منزل که آن دلدار گردد</p></div>
<div class="m2"><p>جهان پر نافه تاتار گردد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به هر محمل که آن جانان نشیند</p></div>
<div class="m2"><p>شمیمش در مشام جان نشیند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو یوسف در رسیدی با گروهی</p></div>
<div class="m2"><p>کز ایشان در دل افتادی شکوهی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بگفتندی که از یوسف خبر نیست</p></div>
<div class="m2"><p>درین قوم از قدوم او اثر نیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بگفتی در فریب من مکوشید</p></div>
<div class="m2"><p>قدوم دوست را از من مپوشید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بتی کش شاه ملک جان توان داشت</p></div>
<div class="m2"><p>قدومش را کجا پنهان توان داشت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نسیمش باغ جان را تازه سازد</p></div>
<div class="m2"><p>نه تنها جان جهان را تازه سازد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو جان را تازگی همراه گردد</p></div>
<div class="m2"><p>ازان جان تازه کن آگاه گردد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو کردی گوش آن حیران مهجور</p></div>
<div class="m2"><p>ز چاووشان صدای دور شو دور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زدی افغان که من عمریست دورم</p></div>
<div class="m2"><p>به صد محنت درین دوری صبورم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نباشد بیش ازینم تاب دوری</p></div>
<div class="m2"><p>نجویم دوری الا از صبوری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز جانان تا به کی مهجور باشم</p></div>
<div class="m2"><p>همان بهتر که از خود دور باشم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بگفتی این و بیهوش اوفتادی</p></div>
<div class="m2"><p>ز خود کرده فراموش اوفتادی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز جام بیخودی از دست رفتی</p></div>
<div class="m2"><p>چنان بیخود به آن نی بست رفتی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در آن نیها چو دم از جان ناشاد</p></div>
<div class="m2"><p>دمیدی خاستی افغان و فریاد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدین دستور بودی روزگاری</p></div>
<div class="m2"><p>نبودی غیر ازینش کار و باری</p></div></div>