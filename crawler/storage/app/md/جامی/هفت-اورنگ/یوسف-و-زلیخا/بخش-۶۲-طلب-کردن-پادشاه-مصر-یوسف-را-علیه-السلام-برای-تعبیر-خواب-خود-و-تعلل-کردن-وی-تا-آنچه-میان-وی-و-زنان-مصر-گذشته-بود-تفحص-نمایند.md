---
title: >-
    بخش ۶۲ - طلب کردن پادشاه مصر یوسف را علیه السلام برای تعبیر خواب خود و تعلل کردن وی تا آنچه میان وی و زنان مصر گذشته بود تفحص نمایند
---
# بخش ۶۲ - طلب کردن پادشاه مصر یوسف را علیه السلام برای تعبیر خواب خود و تعلل کردن وی تا آنچه میان وی و زنان مصر گذشته بود تفحص نمایند

<div class="b" id="bn1"><div class="m1"><p>چو باشد خوشه خشک و گاو لاغر</p></div>
<div class="m2"><p>بود از سال تنگت قصه آور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخستین سال های هفتگانه</p></div>
<div class="m2"><p>بود باران و آب و کشت و دانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه عالم ز نعمت پر برآید</p></div>
<div class="m2"><p>وز آن پس هفت سال دیگر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که نعمت های پیشین خورده گردد</p></div>
<div class="m2"><p>ز تنگی جان خلق آزرده گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبارد زآسمان ابر عطایی</p></div>
<div class="m2"><p>نروید از زمین شاخ گیایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز عشرت مالداران ست دارند</p></div>
<div class="m2"><p>ز تنگی تنگدستان جان سپارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان نان گم شود بر خوان دوران</p></div>
<div class="m2"><p>که گوید آدمی نان و دهد جان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوانمرد این سخن بشنید و برگشت</p></div>
<div class="m2"><p>حریف بزم شاه دادگر گشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حدیث یوسف و تعبیر او گفت</p></div>
<div class="m2"><p>دل شاه از دمش چون غنچه بشگفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتا خیز و یوسف را بیاور</p></div>
<div class="m2"><p>کزو به گرددم این نکته باور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخن کز دوست آری شکر است آن</p></div>
<div class="m2"><p>ولی گر خود بگوید خوشتر است آن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو از دلبر سخن شاید شنیدن</p></div>
<div class="m2"><p>چرا از هر دهن باید شنیدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دگر باره به زندان شد روانه</p></div>
<div class="m2"><p>ببرد این مژده سوی آن یگانه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که ای سرو ریاض قدس بخرام</p></div>
<div class="m2"><p>سوی بستانسرای شاه نه گام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خرام آنسو بدین روی دلارا</p></div>
<div class="m2"><p>بیارا زین گل آن بستانسرا را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگفتا من چه آیم سوی شاهی</p></div>
<div class="m2"><p>که چون من بی کسی را بی گناهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به زندان سالها محبوس کرده ست</p></div>
<div class="m2"><p>ز آثار کرم مأیوس کرده ست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر خواهد ز من بیرون نهم پای</p></div>
<div class="m2"><p>ازین غمخانه اول گو بفرمای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که آنانی که چون رویم بدیدند</p></div>
<div class="m2"><p>ز حیرت در رخم کفها بریدند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به یکجا چون ثریا با هم آیند</p></div>
<div class="m2"><p>نقاب از کار من روشن گشایند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که جرم من چه بود از من چه دیدند</p></div>
<div class="m2"><p>چرا رختم سوی زندان کشیدند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بود کین سر شود بر شاه روشن</p></div>
<div class="m2"><p>که پاک است از خیانت دامن من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرا پیشه گناه اندیشگی نیست</p></div>
<div class="m2"><p>در اندیشه خیانت پیشگی نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در آن خانه خیانت نامد از من</p></div>
<div class="m2"><p>به جز صدق و امانت نامد از من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرا به گر زنم نقب خزاین</p></div>
<div class="m2"><p>که باشم در فراش خانه خاین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جوانمرد این سخن چون گفت با شاه</p></div>
<div class="m2"><p>زنان مصر را کردند آگاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که پیش شاه یکسر جمع گشتند</p></div>
<div class="m2"><p>همه پروانه آن شمع گشتند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو ره کردند در بزم شه آن جمع</p></div>
<div class="m2"><p>زبان آتشین بگشاد چون شمع</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کزان شمع حریم جان چه دیدید</p></div>
<div class="m2"><p>که بر وی تیغ بدنامی کشیدید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز رویش در بهار و باغ بودید</p></div>
<div class="m2"><p>چرا ره سوی زندانش نمودید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بتی کازار باشد بر تنش گل</p></div>
<div class="m2"><p>کی از دانا سزد بر گردنش غل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گلی کش نیست تاب باد شبگیر</p></div>
<div class="m2"><p>به پایش چون نهد جز آب زنجیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زنان گفتند کای شاه جوان بخت</p></div>
<div class="m2"><p>به تو فرخنده فر هم تاج و هم تخت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز یوسف ما به جز پاکی ندیدیم</p></div>
<div class="m2"><p>به جز عز و شرفناکی ندیدیم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نباشد در صدف گوهر چنان پاک</p></div>
<div class="m2"><p>که بوده از تهمت آن جان و جهان پاک</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زلیخا نیز بود آنجا نشسته</p></div>
<div class="m2"><p>زبان از کذب و جان از کید رسته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز دستان های پنهان زیر پرده</p></div>
<div class="m2"><p>ریاضت های عشقش پاک کرده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فروغ راستیش از جان علم زد</p></div>
<div class="m2"><p>چو صبح راستین از صدق دم زد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به جرم خویش کرد اقرار مطلق</p></div>
<div class="m2"><p>برآمد زو صدای حصحص الحق</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بگفتا نیست یوسف را گناهی</p></div>
<div class="m2"><p>منم در عشق او گم کرده راهی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نخست او را به وصل خویش خواندم</p></div>
<div class="m2"><p>چو کام من نداد از پیش راندم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به زندان از ستم های من افتاد</p></div>
<div class="m2"><p>در آن غم ها ز غم های من افتاد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>غم من چون گذشت از حد و غایت</p></div>
<div class="m2"><p>به حالش کرد حال من سرایت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جفایی گر رسد او را ز جافی</p></div>
<div class="m2"><p>کنون واجب بود آن را تلافی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر احسان کاید از شاه نکوکار</p></div>
<div class="m2"><p>به صد چندان بود یوسف سزاوار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو شاه این نکته سنجیده بشنید</p></div>
<div class="m2"><p>چو گل بشگفت و چون غنچه بخندید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اشارت کرد کز زندانش آرند</p></div>
<div class="m2"><p>بدان خرم سرابستانش آرند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز باغ لطف گلبرگیست خندان</p></div>
<div class="m2"><p>گل خندان به بستان به که زندان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به ملک جان بود شاه نکوبخت</p></div>
<div class="m2"><p>مقام شه نشاید جز سر تخت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بسا قفلا که ناپیدا کلید است</p></div>
<div class="m2"><p>برد او راه گشایش ناپدید است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بود چون کار دانا پیچ در پیچ</p></div>
<div class="m2"><p>به پیشش کوشش فکر و نظر هیچ</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز ناگه دست صنعی در میان نه</p></div>
<div class="m2"><p>به فتحش هیچ صانع را گمان نه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پدید آید ز غیب آن را گشادی</p></div>
<div class="m2"><p>ودیعت در گشادش هر مرادی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو یوسف دل ز حیلت های خود کند</p></div>
<div class="m2"><p>برید از رشته تدبیر پیوند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به جز ایزد نماند او را پناهی</p></div>
<div class="m2"><p>که باشد در نوایب تکیه گاهی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز پندار خودی و بخردی رست</p></div>
<div class="m2"><p>گرفتش فیض فضل ایزدی دست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شبی سلطان مصر آن شاه بیدار</p></div>
<div class="m2"><p>به خوابش هفت گاو آمد پدیدار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همه بسیار خوب و سخت فربه</p></div>
<div class="m2"><p>به خوبی و خوشی از یکدگر به</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>وز آن پس هفت دیگر در برابر</p></div>
<div class="m2"><p>پدید آمد سراسر خشک و لاغر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در آن هفت نخستین روی کردند</p></div>
<div class="m2"><p>به سان سبزه آن را پاک خوردند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بدینسان سبز و خرم هفت خوشه</p></div>
<div class="m2"><p>که دل زان قوت بردی دیده توشه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>برآمد از عقب هفت دگر خشک</p></div>
<div class="m2"><p>بر آن پیچید و کردش سر به سر خشک</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو سلطان بامداد از خواب برخاست</p></div>
<div class="m2"><p>ز هر بیدار دل تعبیر آن خواست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همه گفتند کین خوابی محال است</p></div>
<div class="m2"><p>فراهم کرده وهم و خیال است</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به حکم عقل تعبیری ندارد</p></div>
<div class="m2"><p>به جز اعراض تدبیری ندارد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>جوانمردی که از یوسف خبر داشت</p></div>
<div class="m2"><p>ز روی کار یوسف پرده برداشت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>که در زندان همایون فر جوانیست</p></div>
<div class="m2"><p>که در حل دقایق خرده دانیست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بود بیدار در تعبیر هر خواب</p></div>
<div class="m2"><p>دلش از غوص این دریا گهریاب</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اگر گویی بر او بگشایم این راز</p></div>
<div class="m2"><p>و زو تعبیر خوابت آورم باز</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بگفتا اذن خواهی چیست از من</p></div>
<div class="m2"><p>چه بهتر کور را از چشم روشن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>مرا چشم خرد زان لحظه کور است</p></div>
<div class="m2"><p>که از دانستن این راز دور است</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>روان شد جانب زندان جوانمرد</p></div>
<div class="m2"><p>به یوسف حال خواب شه بیان کرد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بگفتا گاو و خوشه هر دو سالند</p></div>
<div class="m2"><p>به اوصاف خودش وصاف حالند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو باشد خوشه سبز و گاو فربه</p></div>
<div class="m2"><p>بود از خوبی سالت خبره ده</p></div></div>