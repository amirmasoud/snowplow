---
title: >-
    بخش ۶۱ - در شرح احسان های یوسف علیه السلام با اهل زندان و تعبیر کردن وی خواب مقربان پادشاه مصر را و وصیت کردن وی مر یکی ازیشان را که وی را پیش پادشاه یاد کند
---
# بخش ۶۱ - در شرح احسان های یوسف علیه السلام با اهل زندان و تعبیر کردن وی خواب مقربان پادشاه مصر را و وصیت کردن وی مر یکی ازیشان را که وی را پیش پادشاه یاد کند

<div class="b" id="bn1"><div class="m1"><p>ز مادر هر که دولتمند زاید</p></div>
<div class="m2"><p>فروغ دولتش ظلمت زداید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خارستان رود گلزار گردد</p></div>
<div class="m2"><p>گل از وی نافه تاتار گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو ابر ار بگذرد بر تشنه کشتی</p></div>
<div class="m2"><p>شود از مقدمش خرم بهشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو باد ار در رود در تازه باغی</p></div>
<div class="m2"><p>فروزد از رخ هر گل چراغی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زندان گر درآید خرم و شاد</p></div>
<div class="m2"><p>کند زندانیان را از غم آزاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو زندان بر گرفتاران زندان</p></div>
<div class="m2"><p>شد از دیدار یوسف باغ خندان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه از مقدم او شاد گشتند</p></div>
<div class="m2"><p>ز بند درد و رنج آزاد گشتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گردن غلشان شد طوق اقبال</p></div>
<div class="m2"><p>به پا زنجیرشان فرخنده خلخال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر زندانیی بیمار گشتی</p></div>
<div class="m2"><p>اسیر محنت و تیمار گشتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمر بستی پی بیمارداریش</p></div>
<div class="m2"><p>خلاصی دادی از تیمارخواریش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وگر جا بر گرفتاری شدی تنگ</p></div>
<div class="m2"><p>سوی تدبیر کارش کردی آهنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گشاده رو شدی او را رضا جوی</p></div>
<div class="m2"><p>ز تنگی در گشاد آوردیش روی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وگر بر مفلسی عشرت شدی تلخ</p></div>
<div class="m2"><p>ز ناداری نمودی غره اش سلخ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز زرداران کلید زر گرفتی</p></div>
<div class="m2"><p>ز عیشش قفل تنگی بر گرفتی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگر خوابی بدیدی نیکبختی</p></div>
<div class="m2"><p>به گرداب خیال افتاده رختی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شنیدی از لبش تعبیر آن خواب</p></div>
<div class="m2"><p>به خشکی آمدی رختش ز گرداب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دو کس از محرمان شاه آن بوم</p></div>
<div class="m2"><p>ز خلوتگاه قربش مانده محروم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به زندان همدمش بودند و همراز</p></div>
<div class="m2"><p>در آن ماتمکده با وی هم آواز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به یک شب هر یکی دیدند خوابی</p></div>
<div class="m2"><p>کزان در جانشان افتاد تابی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی را مژده ده خواب از نجاتش</p></div>
<div class="m2"><p>یکی را مخبر از قطع حیاتش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ولی تعبیر آن زیشان نهان بود</p></div>
<div class="m2"><p>وز آن بر جانشان بار گران بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به یوسف خواب های خود بگفتند</p></div>
<div class="m2"><p>جواب خواب های خود شنفتند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی را گوشمال از دار دادند</p></div>
<div class="m2"><p>یکی را بر در شه بار دادند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جوانمردی که سوی شاه می رفت</p></div>
<div class="m2"><p>به مسندگاه عز و جاه می رفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو رو سوی شه مسندنشین کرد</p></div>
<div class="m2"><p>به وی یوسف وصیت اینچنین کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که چون در صحبت شه باریابی</p></div>
<div class="m2"><p>به پیشش فرصت گفتار یابی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا در مجلسش یاد آوری زود</p></div>
<div class="m2"><p>کزان یادآوری وافر بری سود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگویی هست در زندان غریبی</p></div>
<div class="m2"><p>ز عدل شاه دوران بی نصیبی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنینش بی گنه مپسند رنجور</p></div>
<div class="m2"><p>که هست این از طریق معدلت دور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو خورد آن بهره مند از دولت و جاه</p></div>
<div class="m2"><p>می از قرابه قرب شهنشاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چنان رفت آن وصیت از خیالش</p></div>
<div class="m2"><p>که بر خاطر نیامد چند سالش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نهال وعده اش مأیوسی آورد</p></div>
<div class="m2"><p>به زندان بلا محبوسی آورد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بلی آن را که ایزد برگزیند</p></div>
<div class="m2"><p>به صدر عز معشوقی نشیند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ره اسباب بر رویش ببندد</p></div>
<div class="m2"><p>رهین این و آنش کم پسندد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نتابد جز سوی خود روی او را</p></div>
<div class="m2"><p>ز هر کس بگسلاند خوی او را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به دست غیر تاراجش نخواهد</p></div>
<div class="m2"><p>به غیر خویش محتاجش نخواهد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نخواهد دست او در دامن کس</p></div>
<div class="m2"><p>اسیر دام خویشش خواهد و بس</p></div></div>