---
title: >-
    بخش ۱۱ - در سبب نظم کتاب و باعث عرض این خطاب
---
# بخش ۱۱ - در سبب نظم کتاب و باعث عرض این خطاب

<div class="b" id="bn1"><div class="m1"><p>ضعف پیری قوت طبعم شکست</p></div>
<div class="m2"><p>راه فکرت بر ضمیر من ببست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دلم فهم سخندانی نماند</p></div>
<div class="m2"><p>بر لبم حرف سخنرانی نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به که سر در جیب خاموشی کشم</p></div>
<div class="m2"><p>پا به دامان فراموشی کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسبتی دارد به حال من قوی</p></div>
<div class="m2"><p>این دو بیت از مثنوی مولوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>«کیف یأتی النظم لی و القافیه</p></div>
<div class="m2"><p>بعد ما ضاعت اصول العافیه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قافیه اندیشم و دلدار من</p></div>
<div class="m2"><p>گویدم مندیش جز دیدار من »</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست دلدار آن که دلها دار اوست</p></div>
<div class="m2"><p>جمله جانها مخزن اسرار اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارد او از خانه خود آگهی</p></div>
<div class="m2"><p>به که داری خانه او را تهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا چو بیند دور از او بیگانه را</p></div>
<div class="m2"><p>جلوه گاه خود کند آن خانه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که را باشد ز دانش بهره مند</p></div>
<div class="m2"><p>غیر ازین معنی کجا افتد پسند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیک شاهان نیز او را سایه اند</p></div>
<div class="m2"><p>از صفات و ذات او پر مایه اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ذکر ایشان در حقیقت ذکر اوست</p></div>
<div class="m2"><p>فکر در اوصاف ایشان فکر اوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لاجرم با دعوی تقصیر من</p></div>
<div class="m2"><p>مدحت شه شد گریبانگیر من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیک مدحش را درین دیرینه کاخ</p></div>
<div class="m2"><p>بود دربایست میدان فراخ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می کنم میدان آن زین مثنوی</p></div>
<div class="m2"><p>می دهم آیین مدحش را نوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور نه بودم مثنوی ها ساخته</p></div>
<div class="m2"><p>خاطر از امثالشان پرداخته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خاصه نظم این کتاب از بهر اوست</p></div>
<div class="m2"><p>مظهر آیات لطف و قهر اوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا چو تقریبی شود انگیخته</p></div>
<div class="m2"><p>باشم اندر ذکر او آویخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در ثنایش نغز گفتاری کنم</p></div>
<div class="m2"><p>در دعایش ناله و زاری کنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون ندارم دامن قربی به دست</p></div>
<div class="m2"><p>بایدم در گفت و گوی او نشست</p></div></div>