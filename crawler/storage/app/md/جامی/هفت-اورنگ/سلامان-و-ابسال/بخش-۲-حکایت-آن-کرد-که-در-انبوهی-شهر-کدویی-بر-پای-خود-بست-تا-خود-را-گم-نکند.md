---
title: >-
    بخش ۲ - حکایت آن کرد که در انبوهی شهر کدویی بر پای خود بست تا خود را گم نکند
---
# بخش ۲ - حکایت آن کرد که در انبوهی شهر کدویی بر پای خود بست تا خود را گم نکند

<div class="b" id="bn1"><div class="m1"><p>کردی از آشوب گردش های دهر</p></div>
<div class="m2"><p>کرد از صحرا و کوه آهنگ شهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دید شهری پر فغان و پر خروش</p></div>
<div class="m2"><p>آمده ز انبوهی مردم به جوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی قراران جهان در هر مقر</p></div>
<div class="m2"><p>در تک و پو بر خلاف یکدگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن یکی را از برون عزم درون</p></div>
<div class="m2"><p>وان دگر را از درون میل برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن یکی را از یمین رو در شمال</p></div>
<div class="m2"><p>وان دگر سوی یمین جنبش سگال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد مسکین چون بدید آن کار و بار</p></div>
<div class="m2"><p>از میانه کرد جا بر یک کنار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت اگر جا بر صف مردم کنم</p></div>
<div class="m2"><p>جای آن دارد که خود را گم کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک نشانه بهر خود ناکرده ساز</p></div>
<div class="m2"><p>خویشتن را چون توانم یافت باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اتفاقا یک کدو بودش به دست</p></div>
<div class="m2"><p>آن کدو بهر نشان بر پای بست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا چو خود را گم کند در شهر و کو</p></div>
<div class="m2"><p>بازیابد چون ببیند آن کدو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زیرکی آن راز را دانست و زود</p></div>
<div class="m2"><p>در پیش افتاد تا جایی غنود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن کدو را حالی از وی باز کرد</p></div>
<div class="m2"><p>بر تن خود بست و خواب آغاز کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرد چون بیدار شد دید آن کدو</p></div>
<div class="m2"><p>بسته بر پای کسی پهلوی او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بانگ بر وی زد که خیز ای سست کیش</p></div>
<div class="m2"><p>کز تو حیران مانده ام در کار خویش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این منم یا تو نمی دانم درست</p></div>
<div class="m2"><p>گر منم چون این کدو بر پای توست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور تویی این من کجایم کیستم</p></div>
<div class="m2"><p>در شماری می نیایم چیستم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای خدا آن کرد بی سرمایه ام</p></div>
<div class="m2"><p>از همه کردان فروتر پایه ام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ده ز فضلت رونقی این کرد را</p></div>
<div class="m2"><p>کن ز لطفت راوقی این درد را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا زهر آلایشی صافی شوم</p></div>
<div class="m2"><p>اهل دل را شربتی شافی شوم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جامی آسا یک به یک را شادکام</p></div>
<div class="m2"><p>خم خم ار نبود رسانم جام جام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ور به من این مکرمت باشد بدیع</p></div>
<div class="m2"><p>خواجه کونین را آرم شفیع</p></div></div>