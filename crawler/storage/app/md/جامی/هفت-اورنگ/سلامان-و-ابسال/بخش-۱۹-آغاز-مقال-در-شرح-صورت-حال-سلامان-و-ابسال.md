---
title: >-
    بخش ۱۹ - آغاز مقال در شرح صورت حال سلامان و ابسال
---
# بخش ۱۹ - آغاز مقال در شرح صورت حال سلامان و ابسال

<div class="b" id="bn1"><div class="m1"><p>شهریاری بود در یونان زمین</p></div>
<div class="m2"><p>چون سکندر صاحب تاج و نگین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود در عهدش یکی حکمت شناس</p></div>
<div class="m2"><p>کاخ حکمت را قوی کرده اساس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اهل حکمت یک به یک شاگرد او</p></div>
<div class="m2"><p>حلقه بسته جمله گرداگرد او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه چون دانست قدرش را شریف</p></div>
<div class="m2"><p>ساختش در خلوت و صحبت حریف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز به تدبیرش نرفتی نیم گام</p></div>
<div class="m2"><p>جز به تلقینش نجستی هیچ کام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جهانگیری ز بس تدبیر کرد</p></div>
<div class="m2"><p>قاف تا قافش همه تسخیر کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلق را از عدل و جودش ساخت کار</p></div>
<div class="m2"><p>شد بدان بنیاد ملکش استوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاه چون نبود به نفس خود حکیم</p></div>
<div class="m2"><p>تا حکیمی نبودش یار و ندیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصر ملکش را بود بنیاد سست</p></div>
<div class="m2"><p>کم فتد قانون حکم او درست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خالی از نعت و نشان عدل و ظلم</p></div>
<div class="m2"><p>فرق نتواند میان عدل و ظلم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ظلم را بندد به جای عدل کار</p></div>
<div class="m2"><p>عدل را داند به سان ظلم عار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عالم از بیداد او گردد خراب</p></div>
<div class="m2"><p>چشمه سار ملک و دین از وی سراب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نکته ای خوش گفته است آن دوربین</p></div>
<div class="m2"><p>عدل دارد ملک را قایم نه دین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کفر کیشی کو به عدل آید فره</p></div>
<div class="m2"><p>ملک را از ظالم دیندار به</p></div></div>