---
title: >-
    بخش ۵۲ - نصیحت کردن حکیم سلامان را
---
# بخش ۵۲ - نصیحت کردن حکیم سلامان را

<div class="b" id="bn1"><div class="m1"><p>چون شه از پند سلامان شد خموش</p></div>
<div class="m2"><p>شد حکیم اندر نصیحت سخت کوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت کای نوباوه باغ کهن</p></div>
<div class="m2"><p>آخرین نقش بدیع کلک کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حرفخوان دفتر هفت و چهار</p></div>
<div class="m2"><p>خط شناس صفحه لیل و نهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خازن گنجینه آدم تویی</p></div>
<div class="m2"><p>نسخه مجموعه عالم تویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدر خود بشناس و مشمر سرسری</p></div>
<div class="m2"><p>خویش را کز هر چه گویم برتری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن که دست قدرتش خاکت سرشت</p></div>
<div class="m2"><p>حرف حکمت در دل پاکت نوشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاک کن از نقش صورت سینه را</p></div>
<div class="m2"><p>روی در معنی کن آن آیینه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا شود گنج معانی سینه ات</p></div>
<div class="m2"><p>غرق نور معرفت آیینه ات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم خویش از طلعت شاهد بپوش</p></div>
<div class="m2"><p>بیش ازین در صحبت شاهد مکوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چیست شاهد صورتی پر عار و عیب</p></div>
<div class="m2"><p>از هوس نی دامنش پاک و نه جیب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر چنین آلودگی مفتون مشو</p></div>
<div class="m2"><p>وز حریم عافیت بیرون مشو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نطفه در تن مایه بخش جان توست</p></div>
<div class="m2"><p>قوت اعضا قوت ارکان توست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای ز شهوت با تن و جان در ستیز</p></div>
<div class="m2"><p>گوش دارش خواهی و خواهی بریز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بودی از آغاز عالی مرتبه</p></div>
<div class="m2"><p>بر فراز چرخ بودت کوکبه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شهوت نفست به زیر انداخته</p></div>
<div class="m2"><p>در حضیض خاک بندت ساخته</p></div></div>