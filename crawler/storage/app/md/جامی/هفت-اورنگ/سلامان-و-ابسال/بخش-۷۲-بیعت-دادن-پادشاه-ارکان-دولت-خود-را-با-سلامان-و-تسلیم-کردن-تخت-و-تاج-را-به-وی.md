---
title: >-
    بخش ۷۲ - بیعت دادن پادشاه ارکان دولت خود را با سلامان و تسلیم کردن تخت و تاج را به وی
---
# بخش ۷۲ - بیعت دادن پادشاه ارکان دولت خود را با سلامان و تسلیم کردن تخت و تاج را به وی

<div class="b" id="bn1"><div class="m1"><p>افسر شاهی چه خوش سرمایه است</p></div>
<div class="m2"><p>تخت سلطانی چه عالی پایه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سری لایق به آن سرمایه نیست</p></div>
<div class="m2"><p>هر قدم شایسته این پایه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ سا پایی سزد این پایه را</p></div>
<div class="m2"><p>عرش سا فرقی شد این سرمایه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سلامان از غم ابسال رست</p></div>
<div class="m2"><p>دل به معشوق همایون فال بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامنش ز آلودگی ها پاک شد</p></div>
<div class="m2"><p>همتش را روی در افلاک شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تارک او گشت در خور تاج را</p></div>
<div class="m2"><p>پای او تخت فلک معراج را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه یونان شهریاران را بخواند</p></div>
<div class="m2"><p>سرکشان و تاجداران را بخواند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جشنی آنسان ساخت کز شاهنشهان</p></div>
<div class="m2"><p>نیست در طی تواریخ جهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود هر لشکر کش و هر لشکری</p></div>
<div class="m2"><p>حاضر آن جشن از هر کشوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان همه لشکرکش و لشکر که بود</p></div>
<div class="m2"><p>با سلامان کرد بیعت هر که بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جمله دل از سروری برداشتند</p></div>
<div class="m2"><p>سر به طوق بندگی افراشتند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شه مرصع افسرش بر سر نهاد</p></div>
<div class="m2"><p>تخت ملکش زیر پای از زر نهاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هفت کشور را به وی تسلیم کرد</p></div>
<div class="m2"><p>رسم لشکر داریش تعلیم کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کرد انشا در چنان هنگامه ای</p></div>
<div class="m2"><p>از برای وی وصیتنامه ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر سر جمع آشکارا نی نهفت</p></div>
<div class="m2"><p>صد گهر ز الماس فکرت سفت و گفت</p></div></div>