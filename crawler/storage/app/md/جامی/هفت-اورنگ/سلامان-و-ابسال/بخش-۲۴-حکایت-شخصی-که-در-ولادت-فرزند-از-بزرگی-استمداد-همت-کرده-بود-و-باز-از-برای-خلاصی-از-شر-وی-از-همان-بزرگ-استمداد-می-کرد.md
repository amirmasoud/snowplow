---
title: >-
    بخش ۲۴ - حکایت شخصی که در ولادت فرزند از بزرگی استمداد همت کرده بود و باز از برای خلاصی از شر وی از همان بزرگ استمداد می کرد
---
# بخش ۲۴ - حکایت شخصی که در ولادت فرزند از بزرگی استمداد همت کرده بود و باز از برای خلاصی از شر وی از همان بزرگ استمداد می کرد

<div class="b" id="bn1"><div class="m1"><p>پیش شیخی رفت آن مرد فضول</p></div>
<div class="m2"><p>بهر بی فرزندیش خاطر ملول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت با من دار شیخا همتی</p></div>
<div class="m2"><p>تا ببخشد کردگارم دولتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تازه سروی روید از آب و گلم</p></div>
<div class="m2"><p>کز وجود او بیاساید دلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یعنی آید در کنارم یک پسر</p></div>
<div class="m2"><p>کز جمال او شود روشن بصر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیخ گفتا خویش را رنجه مدار</p></div>
<div class="m2"><p>واگذار این کار را با کردگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هر آن کاری که آری روی و رای</p></div>
<div class="m2"><p>مصلحت را از تو به داند خدای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت شیخا من بدین مقصود اسیر</p></div>
<div class="m2"><p>مانده ام از من عنایت وامگیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دعا شو قاصد بهبود من</p></div>
<div class="m2"><p>تا به زودی رو دهد مقصود من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیخ حالی در دعا برداشت دست</p></div>
<div class="m2"><p>بر نشان افتاد تیر او ز شست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک پسر چون آهوی چین مشکبار</p></div>
<div class="m2"><p>از شکارستان غیبش شد شکار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نهال شهوت و شاخ هوا</p></div>
<div class="m2"><p>یافت در آب و گلش نشو و نما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با حریفان باده نوشیدن گرفت</p></div>
<div class="m2"><p>در پی هر کام کوشیدن گرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مست شد جا بر کنار بام کرد</p></div>
<div class="m2"><p>دختر همسایه را بدنام کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شوهر دختر ز پیش او گریخت</p></div>
<div class="m2"><p>ورنه خونش را به خنجر خواست ریخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شحنه را دادند ازین صورت خبر</p></div>
<div class="m2"><p>بدره های زر طمع کرد از پدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روز و شب این بود کار و بار او</p></div>
<div class="m2"><p>فاش شد در شهر و کو کردار او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نی نصیحت را اثر بودی در او</p></div>
<div class="m2"><p>نی سیاست کارگر بودی در او</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون پدر زین کار و بار آمد به تنگ</p></div>
<div class="m2"><p>باز زد در دامن آن شیخ چنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که ندارم غیر تو فریادرس</p></div>
<div class="m2"><p>رحم کن بر من به فریادم برس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کن دعای دیگر اندر کار او</p></div>
<div class="m2"><p>وز سر من دور کن آزار او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شیخ گفت آن روز من گفتم تو را</p></div>
<div class="m2"><p>که مکن الحاح و بگذر زین دعا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عفو می خواه از خدا و عافیت</p></div>
<div class="m2"><p>کین بود در هر دو عالم کافیت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون ببندی بار رحلت زین دیار</p></div>
<div class="m2"><p>نی پسر نی دخترت آید به کار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بنده ای در بندگی بی بند باش</p></div>
<div class="m2"><p>هر چه می آید بدان خرسند باش</p></div></div>