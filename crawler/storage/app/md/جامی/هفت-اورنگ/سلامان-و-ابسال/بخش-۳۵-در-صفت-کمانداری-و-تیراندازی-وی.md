---
title: >-
    بخش ۳۵ - در صفت کمانداری و تیراندازی وی
---
# بخش ۳۵ - در صفت کمانداری و تیراندازی وی

<div class="b" id="bn1"><div class="m1"><p>شه چو گشتی بعد چوگان باختن</p></div>
<div class="m2"><p>چون کمان مایل به تیر انداختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کمانداران خاص اندر زمان</p></div>
<div class="m2"><p>خواستی ناکرده زه چاچی کمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی مدد آن را به زه آراستی</p></div>
<div class="m2"><p>بانگ زه از گوشه ها برخاستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست مالیدی بر آن چالاک و چست</p></div>
<div class="m2"><p>تا بن گوشش کشیدی از نخست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه بنهادی سه پر مرغی بر آن</p></div>
<div class="m2"><p>رهسپر کردی به هنجار نشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نشان بودی ازین فیروزه سفر</p></div>
<div class="m2"><p>نقطه ای بی شک شدی آن نقطه صفر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور گشادی تیر پرتابی زشست</p></div>
<div class="m2"><p>بودیش خط افق جای نشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نه مانع سختی گردون شدی</p></div>
<div class="m2"><p>از خط دور افق بیرون شدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سر تیرش نرستی از خطر</p></div>
<div class="m2"><p>گاه صید آهو به پا، تیهو به پر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پی سوی مقصود بردی راست پا</p></div>
<div class="m2"><p>همچو طبع راست محفوظ از خطا</p></div></div>