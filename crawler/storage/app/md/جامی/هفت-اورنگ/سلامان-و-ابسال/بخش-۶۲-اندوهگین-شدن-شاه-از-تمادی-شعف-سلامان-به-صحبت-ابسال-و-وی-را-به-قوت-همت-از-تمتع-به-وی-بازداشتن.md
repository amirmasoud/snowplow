---
title: >-
    بخش ۶۲ - اندوهگین شدن شاه از تمادی شعف سلامان به صحبت ابسال و وی را به قوت همت از تمتع به وی بازداشتن
---
# بخش ۶۲ - اندوهگین شدن شاه از تمادی شعف سلامان به صحبت ابسال و وی را به قوت همت از تمتع به وی بازداشتن

<div class="b" id="bn1"><div class="m1"><p>شاه یونان چون سلامان را بدید</p></div>
<div class="m2"><p>کو به ابسال و وصالش آرمید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر رفت و زین خسارت بس نکرد</p></div>
<div class="m2"><p>وز ضلالت روی دل واپس نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماند خالی زافسر شاهی سرش</p></div>
<div class="m2"><p>تا که گردد سربلند از افسرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تخت را افکند در پا بخت او</p></div>
<div class="m2"><p>تا کف پای که بوسد تخت او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در درون افتاد ازین غم آتشش</p></div>
<div class="m2"><p>وقت شد زین حال ناخوش ناخوشش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سلامان قوت همت گذاشت</p></div>
<div class="m2"><p>تا ز ابسالش بکلی باز داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لحظه لحظه جانب او می شتافت</p></div>
<div class="m2"><p>لیک نتوانستی از وی بهره یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی او می دید و جانش می طپید</p></div>
<div class="m2"><p>لیک با وصلش نیارستی رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین تغابن در ره سخت اوفتاد</p></div>
<div class="m2"><p>خر بمرد و بر زمین رخت اوفتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرد مفلس را ازین بدتر چه غم</p></div>
<div class="m2"><p>گنج در پهلو و کیسه بی درم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تشنه را زین سختر چه بود عذاب</p></div>
<div class="m2"><p>چشمه پیش چشم و لب محروم از آب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اهل دوزخ را چه محنت زین بتر</p></div>
<div class="m2"><p>آتش اندر جان و جنت در نظر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر سلامان چون شد این محنت دراز</p></div>
<div class="m2"><p>شد در راحت به روی وی فراز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد بر او روشن که آن هست از پدر</p></div>
<div class="m2"><p>تا مگر زان ورطه اش آرد بدر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترس ترسان در پدر آورد روی</p></div>
<div class="m2"><p>توبه کار و عذر خواه و عفو جوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آری آن مرغی که باشد نیکبخت</p></div>
<div class="m2"><p>آخر آرد سوی اصل خویش رخت</p></div></div>