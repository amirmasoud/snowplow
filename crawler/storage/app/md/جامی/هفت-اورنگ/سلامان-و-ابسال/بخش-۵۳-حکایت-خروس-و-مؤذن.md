---
title: >-
    بخش ۵۳ - حکایت خروس و مؤذن
---
# بخش ۵۳ - حکایت خروس و مؤذن

<div class="b" id="bn1"><div class="m1"><p>با خروس آن تاجدار سرفراز</p></div>
<div class="m2"><p>آن مؤذن گفت در وقت نماز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ دانا وقت نشناسد چو تو</p></div>
<div class="m2"><p>وز فوات وقت نهراسد چو تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با چنین دانایی ای دستانسرای</p></div>
<div class="m2"><p>کنگر عرشت همی بایست جای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماکیانی چند را کرده گله</p></div>
<div class="m2"><p>چند گردی در ته هر مزبله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت بود اول مرا پایه بلند</p></div>
<div class="m2"><p>شهوت نفسم بدین پستی فکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز نفس و شهوتش بگذشتمی</p></div>
<div class="m2"><p>در ته هر مزبله کی گشتمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ریاض قدس محرم بودمی</p></div>
<div class="m2"><p>با خروس عرش همدم بودمی</p></div></div>