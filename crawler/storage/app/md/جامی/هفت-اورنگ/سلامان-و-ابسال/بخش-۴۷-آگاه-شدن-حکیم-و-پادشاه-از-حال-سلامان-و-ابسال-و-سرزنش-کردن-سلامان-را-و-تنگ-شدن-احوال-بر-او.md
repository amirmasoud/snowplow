---
title: >-
    بخش ۴۷ - آگاه شدن حکیم و پادشاه از حال سلامان و ابسال و سرزنش کردن سلامان را و تنگ شدن احوال بر او
---
# بخش ۴۷ - آگاه شدن حکیم و پادشاه از حال سلامان و ابسال و سرزنش کردن سلامان را و تنگ شدن احوال بر او

<div class="b" id="bn1"><div class="m1"><p>چون سلامان شد حریف ابسال را</p></div>
<div class="m2"><p>صرف وصلش کرد ماه و سال را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز ماند از خدمت شاه و حکیم</p></div>
<div class="m2"><p>هر دو را شد دل ز هجر او دو نیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ز حال او خبر جستند باز</p></div>
<div class="m2"><p>محرمان کردندشان دانای راز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر پرسش پیش خویشش خواندند</p></div>
<div class="m2"><p>با وی از هر کجا حکایت راندند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکته ها گفتند از نو و کهن</p></div>
<div class="m2"><p>تا به مقصود از طلب آمد سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد یقین کان قصه از وی راست بود</p></div>
<div class="m2"><p>داستانی بی کم و بی کاست بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر یک اندر کار وی رایی زدند</p></div>
<div class="m2"><p>در خلاصش دستی و پایی زدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر نصیحت یافت کار آخر قرار</p></div>
<div class="m2"><p>کز نصیحت نیست بهتر هیچ کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نصیحت ناقصان کامل شوند</p></div>
<div class="m2"><p>وز نصیحت مدبران مقبل شوند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از نصیحت زنده گردد هر دلی</p></div>
<div class="m2"><p>وز نصیحت حل شود هر مشکلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناصحان پیغمبرانند از نخست</p></div>
<div class="m2"><p>گشته کار عقل و دین زیشان درست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که از پیغمبری دم زد بر او</p></div>
<div class="m2"><p>جز نصیحت ز آسمان نامد فرو</p></div></div>