---
title: >-
    بخش ۳۱ - قیام نمودن ابسال به دایگی سلامان و دامن بر زدن در پرورش آن پاکیزه دامان
---
# بخش ۳۱ - قیام نمودن ابسال به دایگی سلامان و دامن بر زدن در پرورش آن پاکیزه دامان

<div class="b" id="bn1"><div class="m1"><p>شاه چون دایه گرفت ابسال را</p></div>
<div class="m2"><p>تا سلامان همایون فال را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آورد در دامن احسان خویش</p></div>
<div class="m2"><p>پرورد از رشحه پستان خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم او چون بر سلامان اوفتاد</p></div>
<div class="m2"><p>زان نظر چاکش به دامان اوفتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد به جان مشعوف لطف گوهرش</p></div>
<div class="m2"><p>همچو گوهر بست در مهد زرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تماشای رخ آن دل فروز</p></div>
<div class="m2"><p>رفت ازو خواب شب و آرام روز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز تا شب جد او و جهد او</p></div>
<div class="m2"><p>بود در بست و گشاد مهد او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه تنش را شستی از مشک و گلاب</p></div>
<div class="m2"><p>گه گرفتی شکرش در شهد ناب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر آن مه بس که در جانش نشست</p></div>
<div class="m2"><p>چشم مهر از هر که غیر او ببست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر میسر گشتیش بی هیچ شک</p></div>
<div class="m2"><p>کردیش جا در بصر چون مردمک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بعد چندی چون ز شیرش باز کرد</p></div>
<div class="m2"><p>نوع دیگر کار و بار آغاز کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت خفتن راست کردی بسترش</p></div>
<div class="m2"><p>سوختی چون شمع بالای سرش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بامداد از خواب چون برخاستی</p></div>
<div class="m2"><p>همچو زرین لعبتش آراستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرمه کردی نرگس شهلای او</p></div>
<div class="m2"><p>چست بستی جامه بر بالای او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کج نهادی بر سرش زرین کلاه</p></div>
<div class="m2"><p>وز برش آویختی زلف سیاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با مرصع بندهای لعل و زر</p></div>
<div class="m2"><p>بر میان نازکش بستی کمر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کردی اینسان خدمتش بیگاه و گه</p></div>
<div class="m2"><p>تا شدش سال جوانی چارده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چارده بودش به خوبی ماه رو</p></div>
<div class="m2"><p>سال او شد چارده چون ماه او</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پایه حسنش بسی بالا گرفت</p></div>
<div class="m2"><p>در همه دلها هوایش جا گرفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شد یکی صد حسن او وان صد هزار</p></div>
<div class="m2"><p>صد هزاران دل ز عشقش بی قرار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با قد چون نیزه بود آن دلپسند</p></div>
<div class="m2"><p>آفتابی گشته یک نیزه بلند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیزه واری قد او چون سر کشید</p></div>
<div class="m2"><p>بر دل هر کس ازو زخمی رسید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زان بلندی هر کجا افکند تاب</p></div>
<div class="m2"><p>سوخت جان عالمی زان آفتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جبهه اش بدر و از او نیمی نهان</p></div>
<div class="m2"><p>با هلال منخسف کرده قران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بینی اش زیر هلال منخسف</p></div>
<div class="m2"><p>در میان ماه کافوری الف</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چشم مستش آهوی مردم شکار</p></div>
<div class="m2"><p>جلوه گاهش در میان لاله زار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ملک خوبی را به رخها شاه بود</p></div>
<div class="m2"><p>شوکت شاهی به او همراه بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خانم شاهیش لعل آتشین</p></div>
<div class="m2"><p>گنج در و گوهرش زیر نگین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تازه سیبش میوه باغ بهشت</p></div>
<div class="m2"><p>آفرین بر دست آن کین میوه کشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چشمه سار لطف سیب غبغبش</p></div>
<div class="m2"><p>تشنگان را آمده جان بر لبش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گردن او سرفراز مهوشان</p></div>
<div class="m2"><p>در کمندش گردن گردنکشان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پاکبازان از پی دفع گزند</p></div>
<div class="m2"><p>از دعا بر بازویش تعویذبند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پست ازو قدر همه زورآوران</p></div>
<div class="m2"><p>زیر دستش ساعد سیمینبران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ساعدش را از یسار و از یمین</p></div>
<div class="m2"><p>جان فشانان نقد جان در آستین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پنجه اش داده شکست سیم ناب</p></div>
<div class="m2"><p>دست هر پولاد بازو داده تاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نقد راحت از دو کف در مشت او</p></div>
<div class="m2"><p>حسن خاتم ختم بر انگشت او</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر چه در وصف جمالش گفته شد</p></div>
<div class="m2"><p>گوهری از بحر صورت سفته شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گوش جان را کن به سوی من گرو</p></div>
<div class="m2"><p>شمه ای دیگر ز احوالش شنو</p></div></div>