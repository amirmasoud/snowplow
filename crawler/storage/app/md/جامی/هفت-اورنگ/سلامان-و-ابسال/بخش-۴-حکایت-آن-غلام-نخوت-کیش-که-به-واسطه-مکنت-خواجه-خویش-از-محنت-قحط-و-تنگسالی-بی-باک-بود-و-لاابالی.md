---
title: >-
    بخش ۴ - حکایت آن غلام نخوت کیش که به واسطه مکنت خواجه خویش از محنت قحط و تنگسالی بی باک بود و لاابالی
---
# بخش ۴ - حکایت آن غلام نخوت کیش که به واسطه مکنت خواجه خویش از محنت قحط و تنگسالی بی باک بود و لاابالی

<div class="b" id="bn1"><div class="m1"><p>در دیار مصر قحطی خاست سخت</p></div>
<div class="m2"><p>کز فزع هر کس به نیل انداخت رخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون به سوی نان رهی نشناختند</p></div>
<div class="m2"><p>رخت هستی را در آب انداختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود جانی قیمت هر تای نان</p></div>
<div class="m2"><p>نان همی گفتند و می دادند جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخردی زیبا غلامی را بدید</p></div>
<div class="m2"><p>کو به فخر و ناز دامن می کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طلعتی چون قرص خور آراسته</p></div>
<div class="m2"><p>نی ز کمخواری مه آسا کاسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تازه روی و خنده ناک و شادکام</p></div>
<div class="m2"><p>هر طرف چون شاخ خرم در خرام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخردش گفت ای غلام از فخر و ناز</p></div>
<div class="m2"><p>چند باشی سرکش و گردن فراز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از غم نان عالمی خوار و دژم</p></div>
<div class="m2"><p>تو چرایی اینچنین فارغ ز غم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت بر سر خواجه ای دارم کریم</p></div>
<div class="m2"><p>هستم از انعام او غرق نعیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوان پر از نان خانه اش پر گندم است</p></div>
<div class="m2"><p>نام قحط از خان و مان او گم است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نباشم خرم و شاد اینچنین</p></div>
<div class="m2"><p>وز گزند قحط آزاد اینچنین</p></div></div>