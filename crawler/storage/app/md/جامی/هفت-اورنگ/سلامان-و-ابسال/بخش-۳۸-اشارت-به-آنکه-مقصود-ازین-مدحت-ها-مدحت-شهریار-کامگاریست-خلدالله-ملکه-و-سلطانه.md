---
title: >-
    بخش ۳۸ - اشارت به آنکه مقصود ازین مدحت ها مدحت شهریار کامگاریست خلدالله ملکه و سلطانه
---
# بخش ۳۸ - اشارت به آنکه مقصود ازین مدحت ها مدحت شهریار کامگاریست خلدالله ملکه و سلطانه

<div class="b" id="bn1"><div class="m1"><p>شب خرد آن ناصح شیرین خطاب</p></div>
<div class="m2"><p>کرد مشفق وار آواز عتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت جامی فکرت بیهوده چند</p></div>
<div class="m2"><p>سودن این کلک نافرسوده چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که بر ملک بقا فیروز نیست</p></div>
<div class="m2"><p>دی به فرض ار بوده است امروز نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گم مکن سر رشته مقصود را</p></div>
<div class="m2"><p>مدح کم گو شاه ناموجود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم ای سرچشمه دانشوری</p></div>
<div class="m2"><p>بر تو ختم اندیشه نطق آوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصد من زین مدح شاه دیگر است</p></div>
<div class="m2"><p>کافسر اقبالش اکنون بر سر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هفت کشور سخره فرمان اوست</p></div>
<div class="m2"><p>هفت دریا رشحه احسان اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصف خاصان به ز عام اندر نهفت</p></div>
<div class="m2"><p>باد صافی وقت آن عارف که گفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«خوشتر آن باشد که وصف دلبران</p></div>
<div class="m2"><p>گفته آید در لباس دیگران »</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر کس آری محرم این راز نیست</p></div>
<div class="m2"><p>بر رخ هر محرم این در باز نیست</p></div></div>