---
title: >-
    بخش ۷۰ - شنیدن پادشاه حال سلامان را و عاجز ماندن از تدبیر کار او و در تدبیر آن به حکیم رجوع کردن
---
# بخش ۷۰ - شنیدن پادشاه حال سلامان را و عاجز ماندن از تدبیر کار او و در تدبیر آن به حکیم رجوع کردن

<div class="b" id="bn1"><div class="m1"><p>چون سلامان ماند از ابسال اینچنین</p></div>
<div class="m2"><p>بود در روز و شبش حال اینچنین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محرمان آن پیش شه گفتند باز</p></div>
<div class="m2"><p>جان او افتاد ازان غم در گداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داشت با ابسال صد اندوه بیش</p></div>
<div class="m2"><p>آمدش بی او غمی چون کوه پیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با ویش غم بود و بی وی نیز هم</p></div>
<div class="m2"><p>از ضمیر او نشد ناچیز غم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنبد گردون عجب غمخانه ایست</p></div>
<div class="m2"><p>بی غمی در وی دروغ افسانه ایست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون گل آدم سرشتند از نخست</p></div>
<div class="m2"><p>شد به قدش خلعت صورت درست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ریخت بالای وی از سر تا قدم</p></div>
<div class="m2"><p>چل صباح ابر بلا باران غم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون چهل بگذشت روزی تا به شب</p></div>
<div class="m2"><p>بر سرش بارید باران طرب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاجرم از غم کس آزادی نیافت</p></div>
<div class="m2"><p>جز پس از چل غم یکی شادی نیافت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بود باران شادی ختم کار</p></div>
<div class="m2"><p>گیرد آخر کار بر شادی قرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیک داند آن که دانش پرور است</p></div>
<div class="m2"><p>کین قرار اندر سرای دیگر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شه سلامان را در آن ماتم چو دید</p></div>
<div class="m2"><p>بر دلش صد زخم رنج و غم رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چاره آن کار نتوانست هیچ</p></div>
<div class="m2"><p>بر رگ جان اوفتادش تاب و پیچ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کرد عرض رای آن دانا حکیم</p></div>
<div class="m2"><p>کای جهان را قبله امید و بیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر کجا درمانده ای را مشکلیست</p></div>
<div class="m2"><p>حل آن ز اندیشه روشندلیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در جهان امروز روشندل تویی</p></div>
<div class="m2"><p>بند سای قفل هر مشکل تویی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سوخت ابسال و سلامان از غمش</p></div>
<div class="m2"><p>کرده وقت خویش وقف ماتمش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نمی توان ابسال را آورد باز</p></div>
<div class="m2"><p>نی سلامان را توان شد چاره ساز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفتم اینک مشکل خود پیش تو</p></div>
<div class="m2"><p>چاره جوی از عقل دور اندیش تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رحمتی فرما که بس درمانده ام</p></div>
<div class="m2"><p>در کف صد غصه مضطر مانده ام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>داد آن دانا حکیم او را جواب</p></div>
<div class="m2"><p>کای نگشته رایت از راه صواب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر سلامان نشکند پیمان من</p></div>
<div class="m2"><p>وآید اندر ربقه فرمان من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زود باز آرم به وی ابسال را</p></div>
<div class="m2"><p>کشف گردانم ز وی این حال را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چند روزی چاره حالش کنم</p></div>
<div class="m2"><p>جاودان دمساز ابسالش کنم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از حکیم این را سلامان چون شنید</p></div>
<div class="m2"><p>زیر فرمان وی از جان آرمید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خار و خاشاک درش رفتن گرفت</p></div>
<div class="m2"><p>هر چه گفت از جان پذیرفتن گرفت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خوش بود خاک در کامل شدن</p></div>
<div class="m2"><p>بنده فرمان صاحبدل شدن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بشنو این نکته که دانا گفته است</p></div>
<div class="m2"><p>گوهری بس خوب و زیبا سفته است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باش دانا بی لجاج و بی ستیز</p></div>
<div class="m2"><p>یا که اندر سایه دانا گریز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رخنه کز نادانی افتد در مزاج</p></div>
<div class="m2"><p>یابد از دانا و دانایی علاج</p></div></div>