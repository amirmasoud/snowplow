---
title: >-
    بخش ۴۰ - به کمال رسیدن اسباب جمال سلامان و ظاهر شدن عشق ابسال بر وی و حیله نمودن تا وی را نیز گرفتار خود گرداند
---
# بخش ۴۰ - به کمال رسیدن اسباب جمال سلامان و ظاهر شدن عشق ابسال بر وی و حیله نمودن تا وی را نیز گرفتار خود گرداند

<div class="b" id="bn1"><div class="m1"><p>چون سلامان را شد اسباب جمال</p></div>
<div class="m2"><p>از بلاغت جمع در حد کمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو نازش تازگی را سر گرفت</p></div>
<div class="m2"><p>باغ لطفش رونق دیگر گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نارسیده میوه ای بود از نخست</p></div>
<div class="m2"><p>چون رسیدن شد بر آن میوه درست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاطر ابسال چیدن خواستش</p></div>
<div class="m2"><p>وز پی چیدن چشیدن خواستش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک بود آن میوه بر شاخ بلند</p></div>
<div class="m2"><p>بود کوتاه آرزو را زان کمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهدی پر عشوه بود ابسال نیز</p></div>
<div class="m2"><p>کم نه ز اسباب جمالش هیچ چیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با سلامان عرض خوبی ساز کرد</p></div>
<div class="m2"><p>شیوه جولانگری آغاز کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه بر رسم نغوله پیش سر</p></div>
<div class="m2"><p>بافتی زنجیره ای از مشک تر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بدان زنجیره دانا پسند</p></div>
<div class="m2"><p>ساختی پای دل شهزاده بند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گاه مشکین موی را بشکافتی</p></div>
<div class="m2"><p>فرق کرده زان دو گیسو تافتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یعنی از وی کام دل نایافتن</p></div>
<div class="m2"><p>تا کیم خواهد بدینسان تافتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گه نهادی چون بتان دل فروز</p></div>
<div class="m2"><p>بر کمان ابروان از وسمه توز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا ز جان او به زنگاری کمان</p></div>
<div class="m2"><p>صید کردی مایه امن و امان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چشم خود را کردی از سرمه سیاه</p></div>
<div class="m2"><p>تاش بردی زان سیه کاری ز راه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برگ گل را دادی از گلگونه زیب</p></div>
<div class="m2"><p>تا بدان رنگش ز دل بردی شکیب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دانه مشکین نهادی بر عذار</p></div>
<div class="m2"><p>تا بدان مرغ دلش کردی شکار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گه گشادی بند از تنگ شکر</p></div>
<div class="m2"><p>گه شکستی مهر بر درج گهر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا چو شکر بر دلش شیرین شدی</p></div>
<div class="m2"><p>وز لب گویاش گوهر چین شدی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گه نمودی از گریبان گوی زر</p></div>
<div class="m2"><p>زیر آن طوق مرصع از کمر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا کشیدی با همه فرخندگی</p></div>
<div class="m2"><p>گردنش را زیر طوق بندگی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گه به کاری دست سیمین در زدی</p></div>
<div class="m2"><p>زان بهانه آستین را بر زدی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا نگارین ساعد او آشکار</p></div>
<div class="m2"><p>دیدی و کردی به خون چهره نگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گه ز بهر خدمتی کردی قیام</p></div>
<div class="m2"><p>سخت تر برداشتی از جای گام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا ز بانگ جنبش خلخال او</p></div>
<div class="m2"><p>تاجور فرقش شدی پامال او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بودی القصه به صد مکر و حیل</p></div>
<div class="m2"><p>جلوه گر در چشم او در هر محل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صبح و شامش روی در خود داشتی</p></div>
<div class="m2"><p>یکدمش غافل ز خود نگذاشتی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زانکه می دانست کز راه نظر</p></div>
<div class="m2"><p>عشق دارد در دل عاشق اثر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جز به دیدار بتان دلپذیر</p></div>
<div class="m2"><p>عشق در دلها نگردد جایگیر</p></div></div>