---
title: >-
    بخش ۴۸ - داستان رسیدن اسکندر به شهری که همه مردم پاکیزه روزگار بودند و سؤال و جواب ایشان
---
# بخش ۴۸ - داستان رسیدن اسکندر به شهری که همه مردم پاکیزه روزگار بودند و سؤال و جواب ایشان

<div class="b" id="bn1"><div class="m1"><p>سکندر چو می گشت گرد جهان</p></div>
<div class="m2"><p>خبر پرس هر آشکار و نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در اثنای رفتن به شهری رسید</p></div>
<div class="m2"><p>در آن شهر قومی پسندیده دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز گفتار بیهوده لبها خموش</p></div>
<div class="m2"><p>فروبسته از ناسزا چشم و گوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نجسته به بد هرگز آزار هم</p></div>
<div class="m2"><p>به هر کار نیکو مددگار هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه زیشان توانگر کسی نی فقیر</p></div>
<div class="m2"><p>بر ایشان نه سلطان کسی نی امیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برابر به هم قسمت مالشان</p></div>
<div class="m2"><p>موافق به هم صورت حالشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه از محنت قحطشان سال تنگ</p></div>
<div class="m2"><p>نه بر صفحه صلحشان حرف جنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز یک خانه هر یک شده بهره مند</p></div>
<div class="m2"><p>نه در بر در خانه هاشان نه بند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر در فرو برده گوری مغاک</p></div>
<div class="m2"><p>که بیننده را زان شدی سینه چاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سکندر چو شد واقف طورشان</p></div>
<div class="m2"><p>شد از گفت و گو طالب غورشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفتا ز اول که در وقت زیست</p></div>
<div class="m2"><p>فرو بردن گور از بهر چیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتند از بهر آن کنده ایم</p></div>
<div class="m2"><p>که تا در فضای جهان زنده ایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نبندد لب خود ز ارشاد ما</p></div>
<div class="m2"><p>دهد هر دم از مردگی یاد ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گشاده بدین نکته دایم دهان</p></div>
<div class="m2"><p>که ما و توییم آن دهان را زبان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز هر کام برکنده دندان در او</p></div>
<div class="m2"><p>زبان وار افتیم عریان در او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زبان وارمان چون به زندان کنند</p></div>
<div class="m2"><p>ز دندانه خشت دندان کنند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دگر گفت چون خانه ها بی در است</p></div>
<div class="m2"><p>در باز مر دزد را رهبر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفتند در شهر ما نیست دزد</p></div>
<div class="m2"><p>که از کسب دزدی خورد دستمزد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه مردم صادقند و امین</p></div>
<div class="m2"><p>چو خاکند امینان روی زمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به خاک ار سپاری یکی دانه جو</p></div>
<div class="m2"><p>دهد هفتصدت باز وقت درو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دگر گفت چون بهر مال و متاع</p></div>
<div class="m2"><p>میان شما نیست جنگ و نزاع</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگفتند ما بنده صانعیم</p></div>
<div class="m2"><p>به قوت و لباسی ز وی قانعیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رسد بی نزاع آنچه باشد کفاف</p></div>
<div class="m2"><p>ازان در غلاف است تیغ خلاف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دگر گفت چون شاه فرمانروای</p></div>
<div class="m2"><p>درین شهر بی شور نگرفته جای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پی دفع ظلم است گفتند شاه</p></div>
<div class="m2"><p>ز ظلم این ولایت بود در پناه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زر عدل از ظلم گیرد عیار</p></div>
<div class="m2"><p>چو ظالم نباشد به عادل چه کار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دگر گفت چون در دیار شما</p></div>
<div class="m2"><p>غنی نیست کس در شمار شما</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگفتند ناید در طبع کریم</p></div>
<div class="m2"><p>حریصی نمودن پی زر و سیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نسازد درین تنگنای مجاز</p></div>
<div class="m2"><p>زر و سیم را جمع جز حرص و آز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دگر گفت چون از صروف زمان</p></div>
<div class="m2"><p>ز محرومی قحط دارید امان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگفتند بیگاه و گاهی که هست</p></div>
<div class="m2"><p>در آمرزشیم از گناهی که هست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شود آدمی را درین دیولاخ</p></div>
<div class="m2"><p>ز آمرزش اسباب روزی فراخ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دگر گفت کین شیوه خاص شماست</p></div>
<div class="m2"><p>که سرمایه بخش خلاص شماست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>و یا از پدر بر پدر آمده ست</p></div>
<div class="m2"><p>گهروار از کان بدر آمده ست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بگفتند کین خاصه از ما نخاست</p></div>
<div class="m2"><p>ابا عن جد این کشته میراث ماست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نداریم از نخل کاری خبر</p></div>
<div class="m2"><p>ز نخل پدر چیده ایم این ثمر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سکندر چو پرداخت از گفت و گوی</p></div>
<div class="m2"><p>به آهنگ برگشتن آورد روی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به دکانچه درزیی برگذشت</p></div>
<div class="m2"><p>که چشم از فروغ ویش خیره گشت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به مقراض تجرید ببریده دل</p></div>
<div class="m2"><p>ز پیوند این عالم آب و گل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فرو برده سر همچو سوزن به کار</p></div>
<div class="m2"><p>گذشته ز دراعه عیب و عار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو رشته سر از جاهلان تافته</p></div>
<div class="m2"><p>سر رشته معرفت یافته</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سکندر بدو گفت کای خیره سر</p></div>
<div class="m2"><p>چو آمد به گوش تو از ما خبر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو رشته سر از ما چرا تافتی</p></div>
<div class="m2"><p>چو سوزن به سر تیز نشتافتی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بگفتا که من مرد آزاده ام</p></div>
<div class="m2"><p>به راه هوس پای ننهاده ام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نیاید خوشم فر و اقبال تو</p></div>
<div class="m2"><p>چه سازم سر خویش پامال تو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ندارم طمع گنج سیم و زرت</p></div>
<div class="m2"><p>چو مار از چه حلقه زنم بر درت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ازین پیش در شهر ما یک دو کس</p></div>
<div class="m2"><p>بپریدشان مرغ جان از قفس</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برید آن امید خود از تاج و تخت</p></div>
<div class="m2"><p>کشید این ز بیغوله فقر رخت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کفن بر تن آن ز خز و حریر</p></div>
<div class="m2"><p>بر این از کهن دلق دل ناپذیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ازین بی وفا کاخ ناپایدار</p></div>
<div class="m2"><p>نهادندشان در یکی کنج غار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر ایشان چو بگذشت یکچند روز</p></div>
<div class="m2"><p>گذشتم بر آن غار با درد و سوز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز هم دیدم آن هر دو را ریخته</p></div>
<div class="m2"><p>به هم استخوان ها در آمیخته</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نشد روشنم بعد صد اهتمام</p></div>
<div class="m2"><p>که آن یک کدام است و این یک کدام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هوای جهان بر دلم سرد شد</p></div>
<div class="m2"><p>ز پیوند آن خاطرم فرد شد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدو گفت شه کای به دانشوری</p></div>
<div class="m2"><p>تو را از همه پایه برتری</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز هر کار می بینم آگه تو را</p></div>
<div class="m2"><p>بیا تا بر اینان کنم شه تو را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بگفتا که شاها من آن درزیم</p></div>
<div class="m2"><p>که باشد پی خود عمل ورزیم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پی خویش دلق بقا دوختن</p></div>
<div class="m2"><p>به از اطلس فانی اندوختن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نمی خواهم این خلعت مستعار</p></div>
<div class="m2"><p>به عور دگر کن عطا این شعار</p></div></div>