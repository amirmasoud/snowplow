---
title: >-
    بخش ۱۴ - معارضه حکیم و لئیمی که صورت این چون سیرت آن آراسته بود و صورت آن چو سیرت این ناپیراسته
---
# بخش ۱۴ - معارضه حکیم و لئیمی که صورت این چون سیرت آن آراسته بود و صورت آن چو سیرت این ناپیراسته

<div class="b" id="bn1"><div class="m1"><p>حکیمی نه بر صورت دلپسند</p></div>
<div class="m2"><p>ز سرمایه حسن نابهره مند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حد تناسب برون پیکرش</p></div>
<div class="m2"><p>به هم ناملایم ز پا تا سرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدی راست چون همت سفله پست</p></div>
<div class="m2"><p>رخی همچو زلف بتان پر شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آسیب لنگیش پا پر خلل</p></div>
<div class="m2"><p>ز نیروی گیراییش دست شل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز قوت تهی حقه مشت او</p></div>
<div class="m2"><p>به فرمان او نی یک انگشت او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فضولی بدو گفت دور از قبول</p></div>
<div class="m2"><p>که ای طبع دانا ز شکلت ملول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین شکل ناخوش ز حکمت ملاف</p></div>
<div class="m2"><p>ندیده کس از تیره گل آب صاف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر آن میوه کش نیست خوش رنگ و بوی</p></div>
<div class="m2"><p>ز شیرینی طعم او دست شوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به چشم عنایت مشو ناظرش</p></div>
<div class="m2"><p>که عنوان باطن بود ظاهرش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخندید از آن هرزه گویی حکیم</p></div>
<div class="m2"><p>بدو گفت کای هرزه گوی سلیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز من این هنر بس که جان کاستم</p></div>
<div class="m2"><p>به نقش حقایق دل آراستم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مصیقل شد آیینه سان سینه ام</p></div>
<div class="m2"><p>دو عالم مصور در آیینه ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز من یافت اجناس عالم نوی</p></div>
<div class="m2"><p>شدم عالمی نو ولی معنوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به تکمیل معنی که مقدور بود</p></div>
<div class="m2"><p>قصور تکاسل ز من دور بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو تحسین صورت به تدبیر من</p></div>
<div class="m2"><p>نیامد مزن طعن تقصیر من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به صنع از تو گر طعنه ای راجع است</p></div>
<div class="m2"><p>به تحقیق آن طعنه بر صانع است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به این طعنه کم ده زبان را گشاد</p></div>
<div class="m2"><p>مده خرمن دین و دانش به باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیا ساقی آن باده عیب شوی</p></div>
<div class="m2"><p>که از خم فتاده به دست سبوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بده تا دمی عیب شویی کنم</p></div>
<div class="m2"><p>درون فارغ از عیب جویی کنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیا مطرب و پرده ای خوش بساز</p></div>
<div class="m2"><p>وز آن پرده کن چشم عیبم فراز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که تا گردم از عیبجویی خموش</p></div>
<div class="m2"><p>شوم بر سر عیب ها پرده پوش</p></div></div>