---
title: >-
    بخش ۱۹ - خردنامه ارسطاطالیس
---
# بخش ۱۹ - خردنامه ارسطاطالیس

<div class="b" id="bn1"><div class="m1"><p>دبیر خردمند دانش پژوه</p></div>
<div class="m2"><p>نویسنده قصه هر گروه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوشت از سکندر شه نامدار</p></div>
<div class="m2"><p>که چون سلطنت یافت بر وی قرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو نور خرد بودش اندر سرشت</p></div>
<div class="m2"><p>خردنامه های حکیمان نوشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هر حرف حکمت که شد بهره یاب</p></div>
<div class="m2"><p>نوشتش به حل یافته زر ناب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلی نقد بحر خرد گوهر است</p></div>
<div class="m2"><p>به زر نظم سلک گهر خوشتر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر لحظه کردی در آنجا نظر</p></div>
<div class="m2"><p>شدی از سوادش مکحل بصر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفتی به دستور آن کار پیش</p></div>
<div class="m2"><p>به آن راست کردی همه کار خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نخست از ارسطو کش استاد بود</p></div>
<div class="m2"><p>به شاگردی او دلش شاد بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خردنامه ای نغز عنوان گرفت</p></div>
<div class="m2"><p>که مغز از قبول دل و جان گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز نام خدایش سر آغاز کرد</p></div>
<div class="m2"><p>وز آن پس نوای دعا ساز کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که شاها دلت چشمه راز باد</p></div>
<div class="m2"><p>به روی تو چشم رضا باز باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زبانی که باشد به فرمان گرو</p></div>
<div class="m2"><p>نباشد به از گوش فرمان شنو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فضیلت بود در قبول سخن</p></div>
<div class="m2"><p>نه اندر فضولی کن یا مکن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز سوسن گل باغ ازان بهتر است</p></div>
<div class="m2"><p>که این جمله گوش آن زبان آور است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدای آنچه با بندگان می کند</p></div>
<div class="m2"><p>ازیشان توقع همان می کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کند لطف تا لطف خویی کنند</p></div>
<div class="m2"><p>کند نیکویی تا نکویی کنند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بپرورد در لجه جودشان</p></div>
<div class="m2"><p>به جودی که پرورد فرمودشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گناه همه از نم عفو شست</p></div>
<div class="m2"><p>به جرم کسان از همه عفو جست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ازان با همه زد دم از راستی</p></div>
<div class="m2"><p>که تابد عنانشان ز کم کاستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به هر کس ز داد و ستد ره گشاد</p></div>
<div class="m2"><p>نمی خواهد از وی به جز آنچه داد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>میفکن به کار رعیت گره</p></div>
<div class="m2"><p>خدا آنچه دادت به ایشان بده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ترحم کن و عفو و بخشش نمای</p></div>
<div class="m2"><p>که اینها رسیدت ز فضل خدای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهان کوه و فعل تو آمد ندا</p></div>
<div class="m2"><p>جزای تو بر فعل باشد صدا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ازین کوه کز فعل تو پر نداست</p></div>
<div class="m2"><p>صدا جز به وفق ندا برنخاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به کوه آنچه گویی جز آن نشنوی</p></div>
<div class="m2"><p>به خاک آنچه کاری جز آن ندروی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نهالی که کاری درین تیره خاک</p></div>
<div class="m2"><p>چنان کار کز وایه طبع پاک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دهد نام نیکوت امروز بار</p></div>
<div class="m2"><p>به فردات خشنودی کردگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر واگذاری به او کار خویش</p></div>
<div class="m2"><p>نیاید تو را هیچ دشوار پیش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز کار تو دشمن هراسان شود</p></div>
<div class="m2"><p>همه کارها بر تو آسان شود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وگر جز بدو افکنی کار را</p></div>
<div class="m2"><p>نشانه شوی تیر ادبار را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بماند تو را کار ناساخته</p></div>
<div class="m2"><p>دل از نقد اقبال پرداخته</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نیاورده روی دل اندر صلاح</p></div>
<div class="m2"><p>ز تو قصد اصلاح نبود مباح</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز گم کرده ره رهنمایی که یافت</p></div>
<div class="m2"><p>ز دود سیه روشنایی که یافت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز سرچشمه چون تلخ و شور آید آب</p></div>
<div class="m2"><p>ز لب تشنگان کی برد تف و تاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر اصلاح خلق جهان بایدت</p></div>
<div class="m2"><p>دل از هر بدی بر کران بایدت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نشسته ز خود حرف عیب از نخست</p></div>
<div class="m2"><p>ز تو عیب شویی نیاید درست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو ناپاک آید به تو آب جوی</p></div>
<div class="m2"><p>مجو پاکی جامه از شست و شوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مشو غره حسن گفتار خویش</p></div>
<div class="m2"><p>نکو کن چو گفتار کردار خویش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو کردار ناصح بود ناپسند</p></div>
<div class="m2"><p>نصیحت کی افتد ز وی سودند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خرد عیب آن بی خرد می کند</p></div>
<div class="m2"><p>که منع کس از کار خود می کند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نشد مانع طفل قول پدر</p></div>
<div class="m2"><p>که خود خورد حلوا و گفتش مخور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پی زجر نادان بی باک کیش</p></div>
<div class="m2"><p>بود قوت فعل از قول بیش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ودیعت نهادت فلک در سرشت</p></div>
<div class="m2"><p>بسی خوی نیک و بسی خوی زشت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هلاک تو در خوی زشت است لیک</p></div>
<div class="m2"><p>نجات تو بخشد ازان خوی نیک</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو غالب شود خوی بد بر مزاج</p></div>
<div class="m2"><p>نباشد به جز خوی نیکش علاج</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بزن شیشه خشم را سنگ حلم</p></div>
<div class="m2"><p>بشو ظلمت جهل را زآب علم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به فکرت ز دل زنگ نسیان ببر</p></div>
<div class="m2"><p>به شکر از درون داغ کفران ببر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو باری ز گردونت آید به دوش</p></div>
<div class="m2"><p>در افکندن آن مشو حیله کوش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به پشت تحمل کش آن بار را</p></div>
<div class="m2"><p>مکن حیله گر نفس مکار را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مبادا شود سخت تر کار تو</p></div>
<div class="m2"><p>به پشت تو گردد فزون بار تو</p></div></div>