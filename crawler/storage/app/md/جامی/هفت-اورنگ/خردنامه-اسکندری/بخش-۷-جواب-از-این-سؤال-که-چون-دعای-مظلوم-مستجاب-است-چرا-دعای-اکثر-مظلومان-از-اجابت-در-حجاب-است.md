---
title: >-
    بخش ۷ - جواب از این سؤال که چون دعای مظلوم مستجاب است چرا دعای اکثر مظلومان از اجابت در حجاب است
---
# بخش ۷ - جواب از این سؤال که چون دعای مظلوم مستجاب است چرا دعای اکثر مظلومان از اجابت در حجاب است

<div class="b" id="bn1"><div class="m1"><p>شنیدم که این نکته را ساده ای</p></div>
<div class="m2"><p>بپرسید روزی ز آزاده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بسیار مظلوم را دیده ایم</p></div>
<div class="m2"><p>فراوان دعاهاش بشنیده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی خصم را بسته غم نکرد</p></div>
<div class="m2"><p>سر مویی از فرق او کم نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگفت آن که سنگ از دمش موم نیست</p></div>
<div class="m2"><p>اگر زیر تیغ است مظلوم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستمکش اگر نی ستمگر بود</p></div>
<div class="m2"><p>قبول دعایش مقرر بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وگر شغل او هم ستم پیشگیست</p></div>
<div class="m2"><p>دعای وی از کوته اندیشگیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو باشد دلش را سوی ظلم رو</p></div>
<div class="m2"><p>نیاید دعایش فرو جز به رو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین ظلمت آباد پر گفت و گوی</p></div>
<div class="m2"><p>بسی ظالمانند مظلوم روی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غلام از ستم چوب بر خر شکست</p></div>
<div class="m2"><p>به پاداش آن خواجه اش سر شکست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زد انبان آن بیوه را رخنه موش</p></div>
<div class="m2"><p>برآورد گربه ز جانش خروش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر آن مور گنجشک هم زور کرد</p></div>
<div class="m2"><p>ازو دیگری معده معمور کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیابد امان دیگری نیز هم</p></div>
<div class="m2"><p>ز چیزی شود پست و ناچیز هم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی رو چنین تا رسد سلسله</p></div>
<div class="m2"><p>به جایی کز آنجا نشاید گله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آنجا همه عدل مطلق بود</p></div>
<div class="m2"><p>حق محض و خیر محقق بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو آنجا رسیدی خموشی به است</p></div>
<div class="m2"><p>ز هر گفت و گو تیز هوشی به است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیا ساقیا برگ عشرت بساز</p></div>
<div class="m2"><p>مکن در به روی حریفان فراز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که از دولت شه نه کاووس و کی</p></div>
<div class="m2"><p>بگیریم جام و بنوشیم می</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیا مطربا مرحبایی بزن</p></div>
<div class="m2"><p>دعایی بگوی و نوایی بزن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که طبع شه از هر غم آزاد باد</p></div>
<div class="m2"><p>به عدلش همه عالم آباد باد</p></div></div>