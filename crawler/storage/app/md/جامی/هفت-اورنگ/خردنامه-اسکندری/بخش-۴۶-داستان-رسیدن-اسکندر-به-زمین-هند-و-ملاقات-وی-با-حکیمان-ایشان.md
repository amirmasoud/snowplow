---
title: >-
    بخش ۴۶ - داستان رسیدن اسکندر به زمین هند و ملاقات وی با حکیمان ایشان
---
# بخش ۴۶ - داستان رسیدن اسکندر به زمین هند و ملاقات وی با حکیمان ایشان

<div class="b" id="bn1"><div class="m1"><p>سکندر چو بر هند لشکر کشید</p></div>
<div class="m2"><p>خردمندی برهمانان شنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گروهی خدادان و حکمت شناس</p></div>
<div class="m2"><p>بریده ز گیتی امید و هراس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیامد ازیشان کسی سوی او</p></div>
<div class="m2"><p>ز تقصیرشان گرم شد خوی او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برانگیخت لشکر بی قهرشان</p></div>
<div class="m2"><p>شتابان رخ آورد در شهرشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو زان برهمانان خبر یافتند</p></div>
<div class="m2"><p>به تدبیر آن کار بشتافتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسیدند پیشش در اثنای راه</p></div>
<div class="m2"><p>به عرضش رساندند کای پادشاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گروهی فقیریم حکمت پژوه</p></div>
<div class="m2"><p>چه تابی رخ مرحمت زین گروه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه ما را سر صلح نی تاب جنگ</p></div>
<div class="m2"><p>درین کار به گر نمایی درنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو موریم پیشت تواضع نمای</p></div>
<div class="m2"><p>چه مالی صف مور را زیر پای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نداریم جز گنج حکمت متاع</p></div>
<div class="m2"><p>نشاید ز کس بر سر آن نزاع</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر گنج حکمت همی بایدت</p></div>
<div class="m2"><p>به جز گنج کاوی نمی شایدت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود کاوش گنج طاعتوری</p></div>
<div class="m2"><p>نه کشور گشایی و غارتگری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میازار ما را که آزرده ایم</p></div>
<div class="m2"><p>مکش تیغ بر ما که ما مرده ایم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سکندر چو بشنید این عرض حال</p></div>
<div class="m2"><p>ز لشکر کشیدن کشید انفعال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فزون دید ازان سویشان میل خویش</p></div>
<div class="m2"><p>تنی چند بگزید از خیل خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به آن چند تن راه جان برگرفت</p></div>
<div class="m2"><p>دل از ملک و مال جهان گرفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زر و زینت خویش یک سو نهاد</p></div>
<div class="m2"><p>به آن قوم بی پا و سر رو نهاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس از قطع هامون به کوهی رسید</p></div>
<div class="m2"><p>در او کنده هر سو بسی غار دید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گروهی نشسته در آن غارها</p></div>
<div class="m2"><p>فرو شسته دست از همه کارها</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ردا و ازار از گیا بافته</p></div>
<div class="m2"><p>عمامه به فرق از گیا تافته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زن و بچه فقر پروردشان</p></div>
<div class="m2"><p>گیاچین به هامون پی خوردشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گشادند با هم زبان خطاب</p></div>
<div class="m2"><p>بسی شد ز هر سو سؤال و جواب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بسا رمز حکمت که پرداختند</p></div>
<div class="m2"><p>بسا سر مشکل که حل ساختند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو آمد به سر مجلس گفت وگوی</p></div>
<div class="m2"><p>سکندر در آن حاضران کرد روی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که هر چه از جهان احتیاج شماست</p></div>
<div class="m2"><p>بخواهید از من که یکسر رواست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگفتند ما را درین خاکدان</p></div>
<div class="m2"><p>نباید به جز هستی جاودان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرادی کزان برتر امید نیست</p></div>
<div class="m2"><p>به جز زندگانی جاوید نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگفتا که این نیست مقدور من</p></div>
<div class="m2"><p>وز این حرف خالیست منشور من</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کسی کو نیارد که در عمر خویش</p></div>
<div class="m2"><p>کند لحظه ای بلکه کم نیز بیش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه سان بخشش زندگانی کند</p></div>
<div class="m2"><p>بقای کسی جاودانی کند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگفتند چون دانی این راز را</p></div>
<div class="m2"><p>چرا بنده ای شهوت و آز را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پی ملک تا چند خون ریختن</p></div>
<div class="m2"><p>به هر کشوری لشکر انگیختن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گرفتم که گیتی همه آن توست</p></div>
<div class="m2"><p>جهان سر به سر زیر فرمان توست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شده بر تو دور زمان گنج سنج</p></div>
<div class="m2"><p>نمانده ست بر تو نهان هیچ گنج</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه حاصل چو می باید آخر گذاشت</p></div>
<div class="m2"><p>به دل تخم اندوه جاوید کاشت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگفتا من این نی به خود می کنم</p></div>
<div class="m2"><p>نه تنها به حکم خرد می کنم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرا ایزد این منزلت داده است</p></div>
<div class="m2"><p>به خلق جهانم فرستاده است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که تا دین او را کنم آشکار</p></div>
<div class="m2"><p>برآرم ز جان مخالف دمار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دهم قدر بتخانه ها را شکست</p></div>
<div class="m2"><p>کنم هر که را هست یزدان پرست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من آن موج جنبش نهادم ز باد</p></div>
<div class="m2"><p>که یکدم ز جنبش نیارم ستاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز باد اذن آرام گر دیدمی</p></div>
<div class="m2"><p>سر مویی از جا نجنبیدمی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ولی چون نه پیش من است اختیار</p></div>
<div class="m2"><p>نیارم گرفتن به یک جا قرار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اسیرم درین جنبش نو به نو</p></div>
<div class="m2"><p>روم تا مرا گوید ایزد برو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز دست اجل چون شوم پای بست</p></div>
<div class="m2"><p>کشم پای ازین جنبش دور دست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>روم عور ازین دیر از خیر دور</p></div>
<div class="m2"><p>چنان کامده ستم ز آغاز عور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ولی نبودم زین تن عور باک</p></div>
<div class="m2"><p>چو در ستر حکمت بود جان پاک</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دلا از لباس بدن عور باش</p></div>
<div class="m2"><p>ز آلایش ما و من دور باش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو جان تو گنج و طلسم است جسم</p></div>
<div class="m2"><p>بر این گنج پر مایه بشکن طلسم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ولی باشد آنگاه جان تو گنج</p></div>
<div class="m2"><p>که چون بگذرد زین سرای سپنج</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بود همره او گهرهای راز</p></div>
<div class="m2"><p>کزان تا ابد باشدش برگ ساز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدان جاودان شاد و خرم بود</p></div>
<div class="m2"><p>به هر جاکه باشد مکرم بود</p></div></div>