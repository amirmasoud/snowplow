---
title: >-
    بخش ۳۳ - داستان جهانگیری اسکندر و عمارت شهرها و اختراع کارهای وی بر سبیل اجمال
---
# بخش ۳۳ - داستان جهانگیری اسکندر و عمارت شهرها و اختراع کارهای وی بر سبیل اجمال

<div class="b" id="bn1"><div class="m1"><p>گهرسنج این گنج گوهرفشان</p></div>
<div class="m2"><p>چنین می دهد از سکندرنشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون این خردنامه ها را نوشت</p></div>
<div class="m2"><p>به دل تخم اقبال جاوید کشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ملک عدالت علم برکشید</p></div>
<div class="m2"><p>به حرف ضلالت قلم درکشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کشور ستانی عنان تاب داد</p></div>
<div class="m2"><p>ز کشور ستانان سنان آب داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخستین چو خور سوی مغرب شتافت</p></div>
<div class="m2"><p>فروغ جمالش بر آن ملک تافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کف تیغ آتشفشان صبح وار</p></div>
<div class="m2"><p>سپه تاخت بر لشکر زنگبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زدود از پی رستن از ننگشان</p></div>
<div class="m2"><p>ز آیینه مصریان زنگشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وز آنجا سپه سوی دارا کشید</p></div>
<div class="m2"><p>و زو کین خود بی مدارا کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لباس بقا بر تنش چاک کرد</p></div>
<div class="m2"><p>ز ظلمات ظلمش جهان پاک کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز آن پس به تأیید عز و جلال</p></div>
<div class="m2"><p>سراپرده زد بر بلاد شمال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمالش چو در سلک ملک یمین</p></div>
<div class="m2"><p>درآمد علم زد به مشرق زمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به مشرق زمین مطلع نور شد</p></div>
<div class="m2"><p>وز آن ناحیت تیرگی دور شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ولی چون خور آنجا نه دیر آرمید</p></div>
<div class="m2"><p>جنیبت به حد جنوبی کشید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وز آنجا به مغرب زمین بازگشت</p></div>
<div class="m2"><p>سرانجام کارش چو آغاز گشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در آخر نهاد اندرین تنگنای</p></div>
<div class="m2"><p>چو پرگار بر اولین نقطه پای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد این چار دیوار با چار حد</p></div>
<div class="m2"><p>به ملکیت دولتش نامزد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به محدود آورد روی از حدود</p></div>
<div class="m2"><p>فرو ریخت باران احسان و جود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز سر حد چین تا در روم و روس</p></div>
<div class="m2"><p>جهان را رهاند از دریغ و فسوس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گهی آخت بر هند شمشیر عزم</p></div>
<div class="m2"><p>گهی ساخت بر دشت خوارزم رزم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گه از نور آهنگ ظلمات کرد</p></div>
<div class="m2"><p>بدو نور ظلمت مباهات کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صنمخانه ها را ز بنیاد کند</p></div>
<div class="m2"><p>به زردشت و زردشتی آتش فکند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز هر دین به جز دین یزدان پاک</p></div>
<div class="m2"><p>فرو شست یکباری لوح خاک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بنا کرد بس شهرها در جهات</p></div>
<div class="m2"><p>به سان سمرقند و مرو و هرات</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پی بستن سد به مشرق نشست</p></div>
<div class="m2"><p>در فتنه بر روی یأجوج بست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو طی کرد یکسر بساط بسیط</p></div>
<div class="m2"><p>ز خشکی درآمد به اخضر محیط</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تهی گشته از خویش بر روی آب</p></div>
<div class="m2"><p>همی رفت گنبد زنان چون حباب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو گویی مگر گوهرافشان قلم</p></div>
<div class="m2"><p>به لوح زمرد همی زد قلم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو ملک جهان یافت بر وی قرار</p></div>
<div class="m2"><p>چه نادر اثرها که گشت آشکار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زر و سیم نقش روایی گرفت</p></div>
<div class="m2"><p>که با سکه اش آشنایی گرفت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به آهن چو ره یافت زو روشنی</p></div>
<div class="m2"><p>به آیینگی آمد از آهنی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ازو زرگران زرگری یافتند</p></div>
<div class="m2"><p>و زو سیم و زر زیوری یافتند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به هر ره که زد کوس بهر رحیل</p></div>
<div class="m2"><p>ازو گشت پیموده فرسنگ و میل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ازو نوبتی نوبت آغاز کرد</p></div>
<div class="m2"><p>ز نام وی این زمزمه ساز کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به لفظ دری هر چه بر عقل تافت</p></div>
<div class="m2"><p>به یونانی الفاظ ازو نقل یافت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بسی از حکیمان و دانشوران</p></div>
<div class="m2"><p>نه تنها حکیمان که پیغمبران</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در آن خوش سفر همدمش بوده اند</p></div>
<div class="m2"><p>به تدبیر ره محرمش بوده اند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یکی زان حکیمان بلیناس بود</p></div>
<div class="m2"><p>ز پیغمبران خضر و الیاس بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو پیش آمدی مشکلی در رهش</p></div>
<div class="m2"><p>برون از وقوف دل آگهش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز هر یک در آن خواستی یاوری</p></div>
<div class="m2"><p>به فکرت گزاری و حیلتگری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به خود هم دل حکمت اندیش داشت</p></div>
<div class="m2"><p>که حکمتوری از همه بیش داشت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو از دیگران کار نگشادیش</p></div>
<div class="m2"><p>گشادی ز تدبیر خود دادیش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بلی حکمت آن به که زاید ز دل</p></div>
<div class="m2"><p>زهاب درایت گشاید ز دل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زمین دل مرد را در سرشت</p></div>
<div class="m2"><p>بود از حکیم ازل دست کشت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نه تاراج مرگش تواند ربود</p></div>
<div class="m2"><p>نه تیغ هلاکش تواند زدود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز دستش درین دیر دیرینه پای</p></div>
<div class="m2"><p>رود هر چه هست آن بماند به جای</p></div></div>