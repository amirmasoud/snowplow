---
title: >-
    بخش ۷۷ - حکایت عمر گذرانیدن دیوانه بلخی از گریه بسیار به شوری و تلخی
---
# بخش ۷۷ - حکایت عمر گذرانیدن دیوانه بلخی از گریه بسیار به شوری و تلخی

<div class="b" id="bn1"><div class="m1"><p>سراسیمه ای خانه در بلخ داشت</p></div>
<div class="m2"><p>که بر مردگان گریه تلخ داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن شهر بی گریه کم زیستی</p></div>
<div class="m2"><p>به خون بهر هر مرده بگریستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر حلقه غم که پرداختی</p></div>
<div class="m2"><p>از اشک چو لعلش نگین ساختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نصیحتگری گفت با او نهفت</p></div>
<div class="m2"><p>که ای هر کس از حال تو در شگفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را این همه گریه زار چیست</p></div>
<div class="m2"><p>نه مزدوری این گونه بیگار چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مریز اشک خود را به هر خاک کوی</p></div>
<div class="m2"><p>که این آب چشم است نی آب جوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخندید دیوانه کای بیخرد</p></div>
<div class="m2"><p>که شاخ قبولت بود بیخ رد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من این گریه از بهر خود می کنم</p></div>
<div class="m2"><p>نه از مرگ هر نیک و بد می کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به مردن هر آن زنده کز پا فتاد</p></div>
<div class="m2"><p>ازان مردن خویشم آمد به یاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز غم آتش افتاد در جان من</p></div>
<div class="m2"><p>شد از دود پر چشم گریان من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازان آتشم دود خیزد ز چشم</p></div>
<div class="m2"><p>وز آن دودم این آب ریزد ز چشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهی مرد نادان که از مرگ خویش</p></div>
<div class="m2"><p>نگردد جگرپاره و سینه ریش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نگرید ز درد دل خود به خون</p></div>
<div class="m2"><p>غم دل به آن گریه ندهد برون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیا ساقیا تا جگر خون کنیم</p></div>
<div class="m2"><p>وز این می قدح را جگرگون کنیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که غمدیده را آه و زاری به است</p></div>
<div class="m2"><p>جگرخواری از میگساری به است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیا مطربا کز طرب بگذریم</p></div>
<div class="m2"><p>ز چنگ طرب تارها بردریم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز چنگ اجل چون نشاید گریخت</p></div>
<div class="m2"><p>ز چنگ رب تار باید گسیخت</p></div></div>