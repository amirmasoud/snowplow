---
title: >-
    بخش ۴۵ - داستان نکته های حکمت راندن شاگردان ارسطو و خبر یافتن اسکندر از آن و عقدهای گوهر بر ایشان نثار کردن
---
# بخش ۴۵ - داستان نکته های حکمت راندن شاگردان ارسطو و خبر یافتن اسکندر از آن و عقدهای گوهر بر ایشان نثار کردن

<div class="b" id="bn1"><div class="m1"><p>ارسطو که در حکمت استاد بود</p></div>
<div class="m2"><p>و زو کشور حکمت آباد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی طالبان بود دور از حرم</p></div>
<div class="m2"><p>یکی خانه اش نام بیت الحکم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان خانه هر گه برون آمدی</p></div>
<div class="m2"><p>ز هر سو دو صد ذوالفنون آمدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شاگردیش صف کشیدی همه</p></div>
<div class="m2"><p>می صرف حکمت چشیدی همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی روز نامد برون تا به دیر</p></div>
<div class="m2"><p>شد از انتظارش دل جمله سیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیایید گفتند تا یک به یک</p></div>
<div class="m2"><p>زنیم از سخن نقد خود بر محک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو سه نکته از حکمت آریم پیش</p></div>
<div class="m2"><p>نماییم ازان حاصل کار خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی گفت کای گم به راه هوس</p></div>
<div class="m2"><p>همین گمرهیت اندرین راه بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که نبود امید تو در هیچ کار</p></div>
<div class="m2"><p>به فضل خداوندگار استوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به کار آر علمی که آموختی</p></div>
<div class="m2"><p>مکش مشعلی را که افروختی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو دانش به سوی کنش رهبر است</p></div>
<div class="m2"><p>کنش مایه دانش دیگر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بکش بر جهان عطف دامان ناز</p></div>
<div class="m2"><p>که پیش تو افتد به خاک نیاز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود این جهان زاغ مردارخوار</p></div>
<div class="m2"><p>جهان دگر رشک باغ بهار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به تن مایه قوت این زاغ باش</p></div>
<div class="m2"><p>به جان طایر شاخ آن باغ باش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دوم گفت گیتی یکی گلشن است</p></div>
<div class="m2"><p>خدا جوی را دیده روشن است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خدا را به او بین و او را مبین</p></div>
<div class="m2"><p>به بی رنگ شو رنگ و بو را مبین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود خانه دل حریم خدای</p></div>
<div class="m2"><p>مکن جز خدا را در آن خانه جای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه لایق به قانون فرزانگی</p></div>
<div class="m2"><p>که با حق کند خلق همخانگی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سیم گفت کین چند روز حیات</p></div>
<div class="m2"><p>بود نقد گنجینه کاینات</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خوش آن کس که راه خرد را گزید</p></div>
<div class="m2"><p>بداد آن و عمر ابد را خرید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چهارم بدین نکته لب را گشود</p></div>
<div class="m2"><p>که آینده آید چه دیر و چه زود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خوش آن کس که آب رخ خود نریخت</p></div>
<div class="m2"><p>به نیکش رخ آورد و از بد گریخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گذشته چو مرغیست جسته ز دام</p></div>
<div class="m2"><p>ازو نیست در دست تو غیر نام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برایش نه غمگین و نی شاد باش</p></div>
<div class="m2"><p>به کلی ز فکر وی آزاد باش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز جان و دل پنجم این نکته خاست</p></div>
<div class="m2"><p>که هر کس به حق راست با خلق راست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو با حق کند بنده ناراستی</p></div>
<div class="m2"><p>نیاید ازو هیچ جا راستی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مساق سخن چون بدینجا رسید</p></div>
<div class="m2"><p>ز در ناگه آن پیر دانا رسید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگفتا که در وقت این انتظار</p></div>
<div class="m2"><p>کدامین سخن بودتان اختیار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگفتند آنها که بگذشته بود</p></div>
<div class="m2"><p>نوابخش گوش و زبان گشته بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو پیر آنچه گفتند با او شنفت</p></div>
<div class="m2"><p>چو غنچه بخندید و چون گل شگفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به گوش سکندر رسید این خبر</p></div>
<div class="m2"><p>بفرمود تا عقدهای گهر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ببردند و زان رشته بگسیختند</p></div>
<div class="m2"><p>به فرق فلک سایشان ریختند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ازیشان کسی سر به بالا نکرد</p></div>
<div class="m2"><p>نظر در گهرهای والا نکرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ارسطو به تحسینشان لب گشاد</p></div>
<div class="m2"><p>که این عقل و دین از جهان گم مباد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر آن چند دعوی که پرداختید</p></div>
<div class="m2"><p>ز همت بلندی گوا ساختید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به هر کار کاینجا رساندید رخت</p></div>
<div class="m2"><p>بگیرید دامان آن کار سخت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به آن صید اقبال دیگر کنید</p></div>
<div class="m2"><p>رخ همت از به به بهتر کنید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بیا ساقیا می روانتر بده</p></div>
<div class="m2"><p>سبک باش و جام گرانتر بده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به کف باده در ساغر زر درآی</p></div>
<div class="m2"><p>چو به داری از به به بهتر گرای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بیا مطربا بر یکی پرده ایست</p></div>
<div class="m2"><p>مکن کین عجب جانفزا پرده است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به هر پرده رازی بود دلنواز</p></div>
<div class="m2"><p>که آن را ندانند جز اهل راز</p></div></div>