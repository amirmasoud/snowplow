---
title: >-
    بخش ۴۰ - حکایت شخصی که زمین خرید و در آنجا گنجی یافت، برنداشت که از آن فروشنده است و فروشنده قبول نکرد که من زمین را و هر چه در وی بوده فروخته ام
---
# بخش ۴۰ - حکایت شخصی که زمین خرید و در آنجا گنجی یافت، برنداشت که از آن فروشنده است و فروشنده قبول نکرد که من زمین را و هر چه در وی بوده فروخته ام

<div class="b" id="bn1"><div class="m1"><p>شنیدم که در عهد نوشیروان</p></div>
<div class="m2"><p>که گیتی چو تن بود و عدلش روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان عدل در مغز جان ها نشست</p></div>
<div class="m2"><p>که هنگامه ظالمان برشکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فقیری در این عرصه جایی نداشت</p></div>
<div class="m2"><p>سزای نشستن سرایی نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برای عمارت زمین خرید</p></div>
<div class="m2"><p>که در کندنش گنجی آمد پدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کلندش شد اندر کف رنجبر</p></div>
<div class="m2"><p>به صورت کلید در گنج زر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روانی به سوی فروشنده رفت</p></div>
<div class="m2"><p>پی رد آن گنج کوشنده رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفت آن زمین را چو بشکافتم</p></div>
<div class="m2"><p>پر از سیم و زر مخزنی یافتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا گنج خود را پذیرنده شو</p></div>
<div class="m2"><p>ز سیم و زرش بهره گیرنده شو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفتا من آن را چو بفروختم</p></div>
<div class="m2"><p>ز سیم و زرش کیسه افروختم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تصرف در آن نیست از من درست</p></div>
<div class="m2"><p>در او هر چه یابی همه حق توست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه بایع گرفت آن و نی مشتری</p></div>
<div class="m2"><p>به داور رساندند این داوری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بپرسید ازیشان که ای بخردان</p></div>
<div class="m2"><p>به لشکرگه عدل اسپهبدان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدا هیچ فرزندتان داده است</p></div>
<div class="m2"><p>و یا لوح ازین نقشتان ساده است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی گفت دارم بلی دختری</p></div>
<div class="m2"><p>ز حال پسر زد نفس دیگری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هم هر دو را بست عقد نکاح</p></div>
<div class="m2"><p>وز آن گنجشان کرد خوردن مباح</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که فرزند ازان چون شود بهره ور</p></div>
<div class="m2"><p>رسد راحت آن به جان پدر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر آن قصه بودی درین روزگار</p></div>
<div class="m2"><p>برآوردی از گنج هر یک دمار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شدی بایع و مشتری در سرش</p></div>
<div class="m2"><p>ببردی به عنف از میان داورش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیا ساقیا در ده آن جام عدل</p></div>
<div class="m2"><p>که فیروزی آمد سرانجام عدل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بکش بازوی مکنت از جور دور</p></div>
<div class="m2"><p>که چندان بقا نیست در دور جور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیا مطربا پرده معتدل</p></div>
<div class="m2"><p>که آرام جان بخشد و انس دل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزن تا ز آشفته حالی رهیم</p></div>
<div class="m2"><p>ز تشویش بی اعتدالی رهیم</p></div></div>