---
title: >-
    بخش ۳۵ - خردنامه اسکندر
---
# بخش ۳۵ - خردنامه اسکندر

<div class="b" id="bn1"><div class="m1"><p>سکندر که گنجینه راز بود</p></div>
<div class="m2"><p>در گنج حکمت بدو باز بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حکمت بسی گوهر شب فروز</p></div>
<div class="m2"><p>کزو مانده پیداست بر روی روز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا گوش را قاید هوش کن</p></div>
<div class="m2"><p>وز آن گوهر آویزه گوش کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو داری دل و هوش حکمت گرو</p></div>
<div class="m2"><p>بکش پنبه از گوش حکمت شنو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ارسطو کش استاد تعلیم بود</p></div>
<div class="m2"><p>بدو نقد خود کرده تسلیم بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو گفت روزی که ای خرده جوی</p></div>
<div class="m2"><p>به دانش ز اقران خود برده گوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو ملک جهانت مسلم شود</p></div>
<div class="m2"><p>در آن پایه پای تو محکم شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه باشد به پیش تو مقدار من</p></div>
<div class="m2"><p>چه رونق پذیرد ز تو کار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفتا که باشد تو را برتری</p></div>
<div class="m2"><p>بر من به مقدار فرمانبری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به طاعت تو را تا قدم پیشتر</p></div>
<div class="m2"><p>بود قدر تو پیش من بیشتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ارسطو چو از وی شنید این جواب</p></div>
<div class="m2"><p>به معیار حکمت نمودش صواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتا شد اکنون یقینم درست</p></div>
<div class="m2"><p>که این جامه بر قامت توست چست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به تاج کیانی شوی سربلند</p></div>
<div class="m2"><p>ز تخت جم و ملک او بهره مند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی بود دایم به فرهنگ و رای</p></div>
<div class="m2"><p>به تعظیم استاد کوشش نمای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کسی گفت چونی چنین رنجبر</p></div>
<div class="m2"><p>به تعظیم استاد بیش از پدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگفتا زد این نقش آب و گلم</p></div>
<div class="m2"><p>وز آن تربیت یافت جان و دلم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ازین شد تن من پذیرای جان</p></div>
<div class="m2"><p>وز آن آمدم زنده جاودان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازین یافتم یک دو روزه وجود</p></div>
<div class="m2"><p>وز آن یک شدم بحر افضال و جود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ازین بهر گفتن زبان ور شدم</p></div>
<div class="m2"><p>وز آن در سخن کان گوهر شدم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز شهوت شد این یک زمان کامیاب</p></div>
<div class="m2"><p>پی تخم من ریخت یک قطره آب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز فکرت شد آن سالها سحر کار</p></div>
<div class="m2"><p>که در علم و حکمت شدم نامدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ازین پا گشادم ز قید عدم</p></div>
<div class="m2"><p>وز آن رو نهادم به ملک قدم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی روز بر تخت شاهی بسی</p></div>
<div class="m2"><p>به سر برد و بیگانه نامد کسی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگفتا که امروز را کز درم</p></div>
<div class="m2"><p>نیامد کس از عمر خود نشمرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در آن روز شه را چه آسایش است</p></div>
<div class="m2"><p>که از وی نه بخشش نه بخشایش است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نریزد به دامان خواهنده سیم</p></div>
<div class="m2"><p>نشوید ز جان پناهنده بیم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عنایت نبیند نکوکار ازو</p></div>
<div class="m2"><p>سیاست نبیند دل آزار ازو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه خوش گفت روزی که قول حکیم</p></div>
<div class="m2"><p>بود آینه پیش مرد کریم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که بیند در او سیرت و خوی را</p></div>
<div class="m2"><p>بدانسان که در آینه روی را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خرد را اثر در دل عاقلان</p></div>
<div class="m2"><p>فزون باشد از تیغ بر جاهلان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بماند مدام آن اثر در ضمیر</p></div>
<div class="m2"><p>شود این به یکچند درمان پذیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کمان اجل گر خدنگ افکن است</p></div>
<div class="m2"><p>میازار کآزار آن بر تن است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو سالم زید مرغ شیرین نفس</p></div>
<div class="m2"><p>چه غم گر شکستی رسد بر قفس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو مجرم شود از گنه عذرخواه</p></div>
<div class="m2"><p>گنه دان تغافل ز عذر گناه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بترس از عقاب شدیدالعقاب</p></div>
<div class="m2"><p>مکن در عقوبتگرایی شتاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>توان زندگان را فکندن ز پای</p></div>
<div class="m2"><p>ولی کشته هرگز نخیزد ز جای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فراوان همی بخش و کم می شمار</p></div>
<div class="m2"><p>ز منت نهادن همی کن کنار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همی گیر کم لیک می بین بسی</p></div>
<div class="m2"><p>کزین شکر پیوند گردد کسی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو دارا به آن رای و فرهنگ خویش</p></div>
<div class="m2"><p>شد آزرده تیغ سرهنگ خویش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ازان زخم در خاک و خون اوفتاد</p></div>
<div class="m2"><p>ز ملک سلامت برون اوفتاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پس پرده پوش یکی طرفه دخت</p></div>
<div class="m2"><p>ز پاکیزگی میوه سایه پخت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>وصیت چنین کرد کان در پاک</p></div>
<div class="m2"><p>ز فر سکندر شود تابناک</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نگردد جز او هیچ کس جفت او</p></div>
<div class="m2"><p>گشاینده درج ناسفت او</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سکندر چو کرد آن وصیت قبول</p></div>
<div class="m2"><p>ولی از قبول وصیت ملول</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدو گفت کس کین ملالت ز چیست</p></div>
<div class="m2"><p>ازو بهترت در جهان جفت کیست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بگفتا ازان باشد اندیشه ام</p></div>
<div class="m2"><p>که بر پا زند عشق او تیشه ام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز سودای عشقش در افتم ز پای</p></div>
<div class="m2"><p>شود بر سرم شاه فرمانروای</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نیارم ز کس کردن آن را نهان</p></div>
<div class="m2"><p>بگویند فرزانگان جهان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سکندر ز دارا جهان را گرفت</p></div>
<div class="m2"><p>ولی دخترش از وی آن را گرفت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زبون ساز مردان صاحب نگین</p></div>
<div class="m2"><p>زبون شد زنی را نه عقل و نه دین</p></div></div>