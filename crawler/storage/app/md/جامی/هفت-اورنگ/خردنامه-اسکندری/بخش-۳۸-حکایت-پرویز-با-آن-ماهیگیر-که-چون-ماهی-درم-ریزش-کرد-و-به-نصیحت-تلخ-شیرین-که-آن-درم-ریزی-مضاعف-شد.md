---
title: >-
    بخش ۳۸ - حکایت پرویز با آن ماهیگیر که چون ماهی درم ریزش کرد و به نصیحت تلخ شیرین که آن درم ریزی مضاعف شد
---
# بخش ۳۸ - حکایت پرویز با آن ماهیگیر که چون ماهی درم ریزش کرد و به نصیحت تلخ شیرین که آن درم ریزی مضاعف شد

<div class="b" id="bn1"><div class="m1"><p>یکی روز پرویز و شیرین به هم</p></div>
<div class="m2"><p>نشسته چو خورشید و پروین به هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ناگه به رسم هواخواهیی</p></div>
<div class="m2"><p>برآورد دریایی ماهیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه ماهی که زیبا طلسمی ز سیم</p></div>
<div class="m2"><p>نموداری از صنع دانا حکیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تر و تازه چون ساعد نیکوان</p></div>
<div class="m2"><p>ربوده دل از دست پیر و جوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو روز جزا ممسک بی کرم</p></div>
<div class="m2"><p>همه پشت و پهلوی او پر درم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش آمد بسی طبع پرویز را</p></div>
<div class="m2"><p>بیفشاند دست گهر ریز را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که تا خازنش راه احسان سپرد</p></div>
<div class="m2"><p>هزاران درم در کنارش شمرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو شیرین بدید آن کرم گستری</p></div>
<div class="m2"><p>بدو گفت کای قبله سروری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ماهی فروشی بدینسان عطا</p></div>
<div class="m2"><p>بود پیش ارباب احسان خطا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هر کس که بخشش کنی اینقدر</p></div>
<div class="m2"><p>کجا آیدش اینقدر در نظر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگوید که این نرخ یک ماهی است</p></div>
<div class="m2"><p>چه لایق به جود شهنشاهی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وگر کم از آنش دهی گوید آه</p></div>
<div class="m2"><p>کم از نرخ یک ماهیم داده شاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شهش گفت اکنون چه درمان کنم</p></div>
<div class="m2"><p>که رد درمهاش فرمان کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفتا بپرسش که ای خودپرست</p></div>
<div class="m2"><p>شکار تو ماده ست یا خود نر است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هر یک که گوید ازین دو جواب</p></div>
<div class="m2"><p>بگو نیست خوردن از آنم صواب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیا فسخ این بیع را ساز ده</p></div>
<div class="m2"><p>درم های سنجیده را باز ده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بشنید ماهی فروش این سؤال</p></div>
<div class="m2"><p>بدانست از زیرکی سر حال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفتا برون زین دو معنی ست این</p></div>
<div class="m2"><p>نه نر است و نی ماده خنثی ست این</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بخندید پرویز و دادش مثال</p></div>
<div class="m2"><p>که گردد مضاعف بر او آن نوال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یک انبان درم شد گرفتش به پشت</p></div>
<div class="m2"><p>پی نرمی روزگار درشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو برداشت از بهر رفتن قدم</p></div>
<div class="m2"><p>فتادش ز انبان فرو یک درم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فکند از سر دوش انبان و زود</p></div>
<div class="m2"><p>نهاد آن درم را به جایی که بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به شه گفت شیرین ببین کان لئیم</p></div>
<div class="m2"><p>چها می کند بهر یک قطعه سیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو شد ظاهر این بخل پنهان ازو</p></div>
<div class="m2"><p>سزد گر ستانیم انبان ازو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سوی خویش پرویز از ره بخواند</p></div>
<div class="m2"><p>وز آن بخل ورزی بدو قصه راند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زمین را ببوسید کای شهریار</p></div>
<div class="m2"><p>ز نام تو بود آن درم سکه دار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرفتم که ناگه یکی تیره رای</p></div>
<div class="m2"><p>نساید بر آن بی ادب وار پای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو بشنید حسن ادب داریش</p></div>
<div class="m2"><p>نکوکاری و نغز گفتاریش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دگر باره رسم کرم فاش کرد</p></div>
<div class="m2"><p>ز گنج نوالش درم پاش کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وز آن پس بگفتا که کارآگهان</p></div>
<div class="m2"><p>منادی کنند این سخن در جهان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که باشد به فرموده زن عمل</p></div>
<div class="m2"><p>زیان بر زیان و خلل بر خلل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز گفتار ایشان ببندید گوش</p></div>
<div class="m2"><p>مباشید از زن نصیحت نیوش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بیا ساقی و جام مردانه ده</p></div>
<div class="m2"><p>بزن جام بر سنگ و پیمانه ده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زن آمد جهان سخره زن مباش</p></div>
<div class="m2"><p>برای زن اینسان فروتن مباش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بیا مطرب و زیر و بم ساز جفت</p></div>
<div class="m2"><p>بزن آشکار این نوای نهفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که بر بخرد این نکته روشن بود</p></div>
<div class="m2"><p>که مأمور زن کمتر از زن بود</p></div></div>