---
title: >-
    بخش ۵۵ - حکایت آن حکیم که با زن گفت هر چه نفقه کردی بهره تو آن است و آنچه برای خود گذاشتی نصیب دیگران است
---
# بخش ۵۵ - حکایت آن حکیم که با زن گفت هر چه نفقه کردی بهره تو آن است و آنچه برای خود گذاشتی نصیب دیگران است

<div class="b" id="bn1"><div class="m1"><p>شنیدم که فرزانه مردی حکیم</p></div>
<div class="m2"><p>به زن داد روزی یکی کیسه سیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس از چند روزش بپرسد حال</p></div>
<div class="m2"><p>وز آن کیسه سیم کردش سؤال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگفتا به دست من آن کیسه سیم</p></div>
<div class="m2"><p>چو آمد چو زر کردم آن را دو نیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی صرف کردم به هر سینه ریش</p></div>
<div class="m2"><p>یکی کردمش صرفه از بهر خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حکیم آن حکایت چو از وی شنفت</p></div>
<div class="m2"><p>بگفت ای نه دانا به راز نهفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود بهره ات آن که کردی نثار</p></div>
<div class="m2"><p>نه آن کش ز گنجینه کردی حصار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گنجینه نقدی که مخزون بود</p></div>
<div class="m2"><p>که داند که انجام آن چون بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیارد برون کس ازین سر سری</p></div>
<div class="m2"><p>که آن بهره توست یا دیگری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا ساقیا باده در جام کن</p></div>
<div class="m2"><p>به رندان لب تشنه انعام کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هر کس که یک جرعه خواهی فشاند</p></div>
<div class="m2"><p>نخواهد جز آن از جهان با تو ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیا مطربا پرده ای ساز لیک</p></div>
<div class="m2"><p>به هنجار نیکو و گفتار نیک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به گیتی مزن جز به نیکی نفس</p></div>
<div class="m2"><p>که اینست آیین نیکان و بس</p></div></div>