---
title: >-
    بخش ۷۵ - جواب نوشتن مادر اسکندر نامه ارسطو را
---
# بخش ۷۵ - جواب نوشتن مادر اسکندر نامه ارسطو را

<div class="b" id="bn1"><div class="m1"><p>چو سرچشمه فیض اسکندری</p></div>
<div class="m2"><p>کزو بود همچون صدف گوهری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن کاغذی کز ارسطو رسید</p></div>
<div class="m2"><p>بسی داروی صبر پیچیده دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز داروی او دفع تیمار کرد</p></div>
<div class="m2"><p>دوای دل و جان بیمار کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلی شربتی بود آن معنوی</p></div>
<div class="m2"><p>به وی از شفاخانه عیسوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز آن پس یکی نامه انگیز کرد</p></div>
<div class="m2"><p>سر نامه را عنبرآمیز کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نام حکیمی که هر نیک و بد</p></div>
<div class="m2"><p>به حکم ویست از ازل تا ابد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر بر درش مرگ اگر زندگیست</p></div>
<div class="m2"><p>سرآورده در ربقه بندگیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود حکمت او نهان در همه</p></div>
<div class="m2"><p>به حکمت بود حکمران بر همه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به حکم وی آیند خلق و روند</p></div>
<div class="m2"><p>به جز حکم او حکم کس نشنوند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سکندر که بر چرخ افسر کشید</p></div>
<div class="m2"><p>نیارست از حکم او سرکشید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به فرمان او زیست چندان که زیست</p></div>
<div class="m2"><p>چو فرمان مرگ آمدش خون گریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولی گریه اش هیچ کاری نکرد</p></div>
<div class="m2"><p>به آن آب دفع غباری نکرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا گر چه بر دل نشست آن غبار</p></div>
<div class="m2"><p>شد آن سرمه دیده اعتبار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدیدم سرانجام کار همه</p></div>
<div class="m2"><p>که بر چیست آخر قرار همه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا زین مصیبت که ناگه رسید</p></div>
<div class="m2"><p>صد اندوه بر جان آگه رسید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلم بود در صبر لیکن چو کوه</p></div>
<div class="m2"><p>نجنبید ازین ماتم پر ستوه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه امکان بود سیل انبوه را</p></div>
<div class="m2"><p>که از بیخ و بن برکند کوه را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کسی کز غم خود بود دل گران</p></div>
<div class="m2"><p>چرا گرید از ماتم دیگران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر مرگ را سازگاری کنم</p></div>
<div class="m2"><p>ازان به که بر مرده زاری کنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرا خود چنین بود حال ای حکیم</p></div>
<div class="m2"><p>که آمد خطی از تو عنبر شمیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به هر نقطه زو نکته ای دلپسند</p></div>
<div class="m2"><p>به هر حرف ازو صد فرح کرده بند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به جان اختر هوش ازان تاب یافت</p></div>
<div class="m2"><p>به دل مزرع صبر ازان آب یافت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اساس خرد دید ازان محکمی</p></div>
<div class="m2"><p>غم و محنت آورد رو در کمی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حیات ابد رشح کلک تو باد</p></div>
<div class="m2"><p>نظام ادب نظم سلک توباد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو آن نامه غم به پایان رساند</p></div>
<div class="m2"><p>نم حسرت از چشم گریان فشاند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وز آن پس یکی لحظه خندان نزیست</p></div>
<div class="m2"><p>کنم قصه کوتاه چندان نزیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه او زیست جاوید نی ما زییم</p></div>
<div class="m2"><p>کمینگاه مرگیم هر جا زییم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مکن هستی جاودانی هوس</p></div>
<div class="m2"><p>که این خاصه کردگار است و بس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیا ساقیا کان که فرزانه است</p></div>
<div class="m2"><p>زده دست در دست پیمانه است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو آرد غم مرگ بر دل شکست</p></div>
<div class="m2"><p>نگیرد کسی غیر پیمانه دست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بیا مطربا تا ز چنگ سپهر</p></div>
<div class="m2"><p>ببریم چون بخردان تار مهر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که آخر اجل تیغ خواهد کشید</p></div>
<div class="m2"><p>به ناخواست این رشته خواهد برید</p></div></div>