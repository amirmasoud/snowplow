---
title: >-
    بخش ۱۸ - حکایت پسر مهتر ده که چون با پدر مشاهده حشمت و شوکت پادشاه شهر کرد
---
# بخش ۱۸ - حکایت پسر مهتر ده که چون با پدر مشاهده حشمت و شوکت پادشاه شهر کرد

<div class="b" id="bn1"><div class="m1"><p>گفت اگر اینست رسم مهتری</p></div>
<div class="m2"><p>منصب ما نیست جز لولیگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی روستایی پسر کش پدر</p></div>
<div class="m2"><p>به ده بودی از مه دهی بهره ور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دماغی پر از نخوت و جاه داشت</p></div>
<div class="m2"><p>دلی خالی از حشمت شاه داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پدر روزی از ده کمتر تنگ کرد</p></div>
<div class="m2"><p>به رفتن سوی شهر آهنگ کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پسر نیز با او قدم زد به راه</p></div>
<div class="m2"><p>که از شهر سازد چو ده جلوه گاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو در عرصه شهر مأوا گرفت</p></div>
<div class="m2"><p>به هر کوی راه تماشا گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی بارگه دید سر بر سماک</p></div>
<div class="m2"><p>به گردون رسیده ازو قدر خاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز کیوان بسی برتر ایوان او</p></div>
<div class="m2"><p>زحل پیکران گشته دربان او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برآمد ز در نعره کره نای</p></div>
<div class="m2"><p>زمین و زمان کرد جنبش ز جای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برون آمد از در هزاران سوار</p></div>
<div class="m2"><p>قبا و کله زر و گوهر نگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وزیشان یکی افسر زر به فرق</p></div>
<div class="m2"><p>ز زر و گهر اسب و زین هر دو غرق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نقیبان به کف حربه نور پاش</p></div>
<div class="m2"><p>زده هر طرف نعره دور باش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پسر کز پدر کس نپنداشت مه</p></div>
<div class="m2"><p>ندانست ازو هیچ مهتر فره</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بپرسید ازان کش به سر افسر است</p></div>
<div class="m2"><p>بگفتند کو شاه این کشور است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرومانده حیران و آورد سر</p></div>
<div class="m2"><p>به گوش پدر کای گرامی پدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر اینست اندازه مهتری</p></div>
<div class="m2"><p>بود کار ما و تو لولیگری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیا ساقی آبی چو آذر بیار</p></div>
<div class="m2"><p>نه می بلکه کبریت احمر بیار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که بر مس ما کیمیایی کند</p></div>
<div class="m2"><p>به نقد خرد رهنمایی کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیا مطرب آغاز کن زیر و بم</p></div>
<div class="m2"><p>که کرد از دلم مرغ آرام رم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پی حلق این مرغ ناگشته رام</p></div>
<div class="m2"><p>ز ابریشم چنگ کن حلقه دام</p></div></div>