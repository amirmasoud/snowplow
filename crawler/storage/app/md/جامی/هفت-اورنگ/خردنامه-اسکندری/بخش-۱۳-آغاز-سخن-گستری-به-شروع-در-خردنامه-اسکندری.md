---
title: >-
    بخش ۱۳ - آغاز سخن گستری به شروع در خردنامه اسکندری
---
# بخش ۱۳ - آغاز سخن گستری به شروع در خردنامه اسکندری

<div class="b" id="bn1"><div class="m1"><p>شناسای تاریخ های کهن</p></div>
<div class="m2"><p>چنین رانده است از سکندر سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مشاطه دولت فیلقوس</p></div>
<div class="m2"><p>چو آراست روی زمین چون عروس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دمسازی این عروسش به بر</p></div>
<div class="m2"><p>خدا داد پیرانه سر یک پسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پسر نی که گردون صدف گوهری</p></div>
<div class="m2"><p>فروزان ز اوج شرف اختری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بخشنده نامان چرخ کبود</p></div>
<div class="m2"><p>پی نامش اسکندر آمد فرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بگذشت سال وی از هفت و هشت</p></div>
<div class="m2"><p>وزو فر شاهی فروزنده گشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پدر صاحب عهد خود ساختش</p></div>
<div class="m2"><p>به تاج کیانی سر افراختش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قوی پنجگان را بدو داد دست</p></div>
<div class="m2"><p>سران را ز جز خدمتش پای بست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بیعت گرفتش ز گردنکشان</p></div>
<div class="m2"><p>به سرچشمه علم دادش نشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرستاد پیش ارسطالسش</p></div>
<div class="m2"><p>که گردد ز نابخردی حارسش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدو داد پیغام کای فیلسوف</p></div>
<div class="m2"><p>که خورشید تو رسته است از کسوف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپهر خرد را تویی آفتاب</p></div>
<div class="m2"><p>ز فیض تو یونان زمین نوریاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز دانش شود کار گیتی به ساز</p></div>
<div class="m2"><p>ز بی دانشی کار گردد دراز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز دل سر زند سر دانش نخست</p></div>
<div class="m2"><p>که بر دست و پا کار گردد درست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر در جهان نبود آموزگار</p></div>
<div class="m2"><p>شود تیره از بی خرد روزگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تفاوت بود اهل تمییز را</p></div>
<div class="m2"><p>به هر کس ندادند هر چیز را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همان به که نادان به دانا رود</p></div>
<div class="m2"><p>که از دانشش کار بالا رود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو نادان ز دانا کند سرکشی</p></div>
<div class="m2"><p>نبیند ز دوران گیتی خوشی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر شاه دوران نباشد حکیم</p></div>
<div class="m2"><p>بود در حضیض جهالت مقیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ازو شیوه جهل خیزد همه</p></div>
<div class="m2"><p>وزو میوه ظلم ریزد همه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ازو حظ بد کامگاری بود</p></div>
<div class="m2"><p>نصیب نکو خاکساری بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سکندر که پرورده مهدم اوست</p></div>
<div class="m2"><p>بر او رنگ شاهی ولیعهدم اوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز هر نقش لوح دلش ساده است</p></div>
<div class="m2"><p>وی نقش را قابل افتاده است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به قانون اقبال داناش کن</p></div>
<div class="m2"><p>بر اسباب دولت تواناش کن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز حکمت بدانسان کنش بهره مند</p></div>
<div class="m2"><p>که سازد پس از مرگ نامم بلند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دهد گوهرش را عدالت شرف</p></div>
<div class="m2"><p>مرا گردد اندر عدالت خلف</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شود عرصه دهر آباد ازو</p></div>
<div class="m2"><p>دل و جان غمدیدگان شاد ازو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ارسطالس این نکته ها چون شنود</p></div>
<div class="m2"><p>به درس سکندر زبان را گشود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به حکمت چراغ دل افروختش</p></div>
<div class="m2"><p>ره حل هر مشکل آموختش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سکندر که طبع هنر سنج داشت</p></div>
<div class="m2"><p>به امکان درون از هنر گنج داشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نشد ضایع اندر طلب رنجهاش</p></div>
<div class="m2"><p>ز امکان به فعل آمد آن گنجهاش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به نقادی فکر روشن که بود</p></div>
<div class="m2"><p>گذشت از رفیقان به هر فن که بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به امداد استاد و همکار نیز</p></div>
<div class="m2"><p>بدانست اسرار بسیار چیز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز دل حرف نابخردی کاسته</p></div>
<div class="m2"><p>به علم طبیعی شد آراسته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کشید از جمال طبایع نقاب</p></div>
<div class="m2"><p>ز اجسام و اعراض شد بهره یاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وز آن پس ره جهل کاهی گرفت</p></div>
<div class="m2"><p>فروغ از علوم الهی گرفت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به یزدان شناسی علم برفراخت</p></div>
<div class="m2"><p>ز دانش پژوهی خدا را شناخت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شد از فسحت خاطر آگهش</p></div>
<div class="m2"><p>ریاض ریاضی تماشاگهش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز اقلیدس اقلیدش آمد به دست</p></div>
<div class="m2"><p>طلسمات گنج مجسطی شکست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کمالات وی شد ز قوت سرای</p></div>
<div class="m2"><p>به سر منزل فعل محمل گشای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نهالش درین باغ کون و فساد</p></div>
<div class="m2"><p>شکوفه برآورد و بر نیز داد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شد از گردش چرخ دیرین اساس</p></div>
<div class="m2"><p>حقایق پذیر و دقایق شناس</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بلی حکمت آنست پیش حکیم</p></div>
<div class="m2"><p>که بر راه دانش شود مستقیم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به نور دل پاک حکمت پرست</p></div>
<div class="m2"><p>برد پی به هر چیز آنسان که هست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو تحسین صورت نه مقدور اوست</p></div>
<div class="m2"><p>در آرایش باطن آورده روست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کشد خامه در دفتر آب و گل</p></div>
<div class="m2"><p>ز دانش دهد زیور جان و دل</p></div></div>