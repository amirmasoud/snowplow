---
title: >-
    بخش ۶۴ - ندبه حکیم هشتم
---
# بخش ۶۴ - ندبه حکیم هشتم

<div class="b" id="bn1"><div class="m1"><p>ز هشتم جز این نکته سر بر نزد</p></div>
<div class="m2"><p>که کس کوس ملک سکندر نزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سفرها که او کرد گرد جهان</p></div>
<div class="m2"><p>نکرده کس از خیل شاهنشهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولیکن به هر سو سفر ساز کرد</p></div>
<div class="m2"><p>ره آن به زور سپه باز کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز این یک سفر کز همه دور ماند</p></div>
<div class="m2"><p>جنیبت به منزلگه گور راند</p></div></div>