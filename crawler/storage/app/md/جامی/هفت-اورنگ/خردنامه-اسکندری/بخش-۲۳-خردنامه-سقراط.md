---
title: >-
    بخش ۲۳ - خردنامه سقراط
---
# بخش ۲۳ - خردنامه سقراط

<div class="b" id="bn1"><div class="m1"><p>زهی گنج حکمت که سقراط بود</p></div>
<div class="m2"><p>مبرا ز تفریط و افراط بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد از جودت فکر ظلمت زدای</p></div>
<div class="m2"><p>همه نور حکمت ز سر تا به پای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرانجام خلعت پرستان شناخت</p></div>
<div class="m2"><p>ز بی خلعتی خلعت خویش ساخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خمخانه چرخ پر اشتلم</p></div>
<div class="m2"><p>به خانه درون داشت یک کهنه خم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به فصل زمستان در آن سرزمین</p></div>
<div class="m2"><p>به شبها ز سرما شدی خم نشین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خورشید خیمه به گردون زدی</p></div>
<div class="m2"><p>ز تدویر خم خیمه بیرون زدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشستی ز عریان تنی بی حجاب</p></div>
<div class="m2"><p>شدی گرم در پرتو آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی روز تن عور خورشیدوار</p></div>
<div class="m2"><p>رسیدش به سر شاه آن روزگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدو گفت کای پیر دانش پذیر</p></div>
<div class="m2"><p>بدینسان چرایی ز ما گوشه گیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قدم باز می داری از راه ما</p></div>
<div class="m2"><p>نمی آوری رو به درگاه ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفتا که تنگ است بر من مجال</p></div>
<div class="m2"><p>ز شغلی که باشد مرا ماه و سال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتش که چندین تورا شغل چیست</p></div>
<div class="m2"><p>که بی آن نیاری یکی لحظه زیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفتا پی دولت زندگی</p></div>
<div class="m2"><p>همی سازم اسباب پایندگی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفتش که اسباب آن پیش ماست</p></div>
<div class="m2"><p>رساندن به حاجتوران کیش ماست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگفت ار بدانم که آن پیش توست</p></div>
<div class="m2"><p>ببندم کمر در رضای تو چست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به دست تو برگ حیات تن است</p></div>
<div class="m2"><p>که آن سد راه نجات من است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حیات دل و جان بود کام من</p></div>
<div class="m2"><p>که آن بندد از راه تو گام من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفتش به هر چیز داری نیاز</p></div>
<div class="m2"><p>بگو تا کنم از برای تو ساز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفتا نیاز من خاکسار</p></div>
<div class="m2"><p>به تو غیر ازین نیست ای شهریار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که این خلعت گرم کز عکس مهر</p></div>
<div class="m2"><p>به دوشم کشیده ست اکنون سپهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به تاراج سایه نگیری ز من</p></div>
<div class="m2"><p>به لطف این توقع پذیری ز من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گذاری که یکدم به بی پردگی</p></div>
<div class="m2"><p>برد مهر چرخ از من افسردگی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو بشنید شاه از وی این گفت و گوی</p></div>
<div class="m2"><p>شد از خاصگان بهر او جامه جوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی جامه دادند او را عطا</p></div>
<div class="m2"><p>ز مویینه چین و خز خطا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگرداند حالی ازان جامه پشت</p></div>
<div class="m2"><p>به نرمی فرو خواند حرفی درشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که کی زندگان را کشیدن نکوست</p></div>
<div class="m2"><p>ز مرده کفن یار ز مردار پوست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز سردی دی چون شوم رنج یاب</p></div>
<div class="m2"><p>شبم خم پسند است و روز آفتاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هزار آفرین بر حکیمی چنین</p></div>
<div class="m2"><p>برون پایه اش زآسمان و زمین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه بر جانش از دور افلاک درد</p></div>
<div class="m2"><p>نه بر طبعش از عالم خاک گرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>درین کار شاگرد بودش هزار</p></div>
<div class="m2"><p>فلاطون از آنها یکی در شمار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فلاطون فلاطونی از وی گرفت</p></div>
<div class="m2"><p>فلاطونی افزونی از وی گرفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به حکمت چو در ثمین سفته است</p></div>
<div class="m2"><p>به دانا فلاطون چنین گفته است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که ای رسته از تنگنای خیال</p></div>
<div class="m2"><p>زده در هوای خرد پر و بال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر آن دار همت ز آغاز کار</p></div>
<div class="m2"><p>که گردی شناسای پروردگار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدانی حق دولت بندگیش</p></div>
<div class="m2"><p>نهی پا به راه پرستندگیش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>روی راه خوشنودیش صبح و شام</p></div>
<div class="m2"><p>به کسب رضایش کنی اهتمام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز حکمت به معراج عزت برآی</p></div>
<div class="m2"><p>بنه بر سر چرخ گردنده پای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسا دست کوته ز بی مایگی</p></div>
<div class="m2"><p>که دارد ز حکمت فلک پایگی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر بودی از جهل هر سینه صاف</p></div>
<div class="m2"><p>برافتادی از خلق رسم خلاف</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ره مرد دانا یکی بیش نیست</p></div>
<div class="m2"><p>به جز طبع نادان دو اندیش نیست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نبینی درین ششدر دیولاخ</p></div>
<div class="m2"><p>ز شادی دل شش نفر را فراخ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی آن حسدور به هر کشوری</p></div>
<div class="m2"><p>که رنجش بود راحت دیگری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو حال کسی بیند از خویش به</p></div>
<div class="m2"><p>فتد بر رگ جانش از غم گره</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دوم کینه ورزی که از خلق زشت</p></div>
<div class="m2"><p>بود کینه خلقش اندر سرشت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو نتواند از کس شدن کینه کش</p></div>
<div class="m2"><p>نباشد ز کینداریش سینه خوش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سیم نو توانگر که بهر درم</p></div>
<div class="m2"><p>بود روز و شب بر دل او دو غم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یکی آنکه چون چیزی آرد به کف</p></div>
<div class="m2"><p>دوم آنکه ناگه نگردد تلف</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چهارم لئیمی که با گنج سیم</p></div>
<div class="m2"><p>بود همچو نام زرش دل دو نیم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که ناگه نیابد بدو فقر راه</p></div>
<div class="m2"><p>نگردد بدان روز عیشش تباه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بود پنجمین طالب پایه ای</p></div>
<div class="m2"><p>که در خورد آن نبودش مایه ای</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کند آرزوی مقام بلند</p></div>
<div class="m2"><p>که نتواند آنجا فکندن کمند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ششم از ادب خالی اندیشه ای</p></div>
<div class="m2"><p>که باشد حریف ادب پیشه ای</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو طبعش بود از ادب بی نصیب</p></div>
<div class="m2"><p>کشد نو به نو مالشی از ادیب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بود سیم و زر رنج دین پروران</p></div>
<div class="m2"><p>طبیبان آن رنج دانشوران</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کشد رنج را چون سوی خود طبیب</p></div>
<div class="m2"><p>کجا باشدش از مداوا نصیب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ازان کس بپرهیز و فعل و فنش</p></div>
<div class="m2"><p>که دارد دلت بی سبب دشمنش</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اگر ره نگرداند از گرگ و میش</p></div>
<div class="m2"><p>بود یاور او در آزار خویش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>زبان را چه داری به گفتن گرو</p></div>
<div class="m2"><p>ز هر سو گشا گوش حکمت شنو</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>خدا یک زبانت بداد و دو گوش</p></div>
<div class="m2"><p>که کم گوی یعنی و افزون نیوش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خموشی بود دولت ایزدی</p></div>
<div class="m2"><p>دلیل هنرمندی و بخردی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز بسیار دانان فراست گواست</p></div>
<div class="m2"><p>که بسیار گوی از کیاست جداست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سخن را کزان بسته داری نفس</p></div>
<div class="m2"><p>یکی مرغ دان پایبند قفس</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو گفتی قفس یافت بر وی شکست</p></div>
<div class="m2"><p>طمع بگسل از وی که آید به دست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مکش زیر ران مرکب حرص و آز</p></div>
<div class="m2"><p>ز گیتی به قدر کفایت بساز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به هر روز تا شب ز خوان سپهر</p></div>
<div class="m2"><p>بسنده ست یک خشت نانت چو مهر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بیفکن ز کف کاسه زر ناب</p></div>
<div class="m2"><p>کف خویش را کاسه کن بهر آب</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ز زربفت هستی مشو خودفروش</p></div>
<div class="m2"><p>کهن خرقه نیستی کش به دوش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مکش بهر معموری خانه رنج</p></div>
<div class="m2"><p>به ویرانه خود را نهان کن چو گنج</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به خود بند در خدمت خود کمر</p></div>
<div class="m2"><p>به مخدومی از کس مکش درد سر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز چوبت کف پای نعلین سای</p></div>
<div class="m2"><p>به از نعل زر بر سم بادپای</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چراغ شبت بس بود ماهتاب</p></div>
<div class="m2"><p>ادیم زمین بهر تو نطع خواب</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بدین حال با حکمت اندوزیت</p></div>
<div class="m2"><p>سلوک عمل گر شود روزیت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بری گوی دولت ز همپیشگان</p></div>
<div class="m2"><p>شوی سرور حکمت اندیشگان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>رهانی ز سود و زیان خویش را</p></div>
<div class="m2"><p>رسانی به پیشینیان خویش را</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>حذر کن ز آسیب جادو زنان</p></div>
<div class="m2"><p>به دستان سران را ز پای افکنان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به روی زمین دام مردان مرد</p></div>
<div class="m2"><p>بساط وفا و مروت نورد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ازیشان در درج حکمت به بند</p></div>
<div class="m2"><p>وزیشان نگون قدر هر سربلند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ازیشان خردمند را پایه پست</p></div>
<div class="m2"><p>وزیشان سپاه خرد را شکست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>دهد طعم شهد و شکر زهرشان</p></div>
<div class="m2"><p>مخور زهر را چون شکر بهرشان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>مشو غره حلم مرد حلیم</p></div>
<div class="m2"><p>که بر حلم عمری نشیند مقیم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>درختیست صندل خنک در مزاج</p></div>
<div class="m2"><p>پی علت گرم طبعان علاج</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به هم در شده شاخه ها زان درخت</p></div>
<div class="m2"><p>چو در اصطکاک افتد از باد سخت</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>زند آتشی شعله زان اصطکاک</p></div>
<div class="m2"><p>که ریزد ازان شاخ و برگش به خاک</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>اگر پیر باشد عوان ور جوان</p></div>
<div class="m2"><p>به هر حال نبود عوان جز عوان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>تنش گر چه از ضعف پیریست سست</p></div>
<div class="m2"><p>بود سیرت بد در او تندرست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>درونش سیاه از دل تیره خوی</p></div>
<div class="m2"><p>کیش سود دارد سفیدی موی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به سال و مه ار گرگ گردد بزرگ</p></div>
<div class="m2"><p>نیاید برون هرگز از خوی گرگ</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به پیمان مشو بند فرمان او</p></div>
<div class="m2"><p>که دام فریب است پیمان او</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>مبادا به آن دامت اندر کشد</p></div>
<div class="m2"><p>به تزویر جانت ز تن برکشد</p></div></div>