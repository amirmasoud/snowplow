---
title: >-
    بخش ۳۹ - داستان خاقان چین که تحفه حقیر به اسکندر فرستاد و به حکمتی شریفش آگاهی داد
---
# بخش ۳۹ - داستان خاقان چین که تحفه حقیر به اسکندر فرستاد و به حکمتی شریفش آگاهی داد

<div class="b" id="bn1"><div class="m1"><p>سکندر ز اقصای یونان زمین</p></div>
<div class="m2"><p>سپه راند بر قصد خاقان چین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آوازه او به خاقان رسید</p></div>
<div class="m2"><p>ز تسکین آن فتنه درمان ندید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز لشگرگه خود به درگاه او</p></div>
<div class="m2"><p>رسولی روان کرد و همراه او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنیزی فرستاد و یک تن غلام</p></div>
<div class="m2"><p>یکی دست جامه یکی خوان طعام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سکندر چو آن تحفه ها را بدید</p></div>
<div class="m2"><p>سرانگشت حیرت به دندان گزید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خود گفت کین تحفه های حقیر</p></div>
<div class="m2"><p>نمی افتد از وی مرا دلپذیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرستادن آن بدین انجمن</p></div>
<div class="m2"><p>نه لایق به وی باشد و نی به من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همانا نهان نکته ای خواسته ست</p></div>
<div class="m2"><p>که در چشمش آن را بیاراسته ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حکیمان که در لشکر خویش داشت</p></div>
<div class="m2"><p>کز ایشان دل حکمت اندیش داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خلوتگه خاص خود خواندشان</p></div>
<div class="m2"><p>به صد گونه تعظیم بنشاندشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرو خواند راز دل خویش را</p></div>
<div class="m2"><p>که تا حل کند مشکل خویش را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی زان میان گفت کز شاه چین</p></div>
<div class="m2"><p>پیامیست پوشیده سوی تو این</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که چون آدمی را مرتب بود</p></div>
<div class="m2"><p>کنیزی که همخوابه شب بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غلامی توانا به خدمتگری</p></div>
<div class="m2"><p>که در کار سختت دهد یاوری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی دست جامه به سالی تمام</p></div>
<div class="m2"><p>پی طعمه هر روز یک خوان طعام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چرا هر زمان رنج دیگر کشد</p></div>
<div class="m2"><p>به هر کشور از دور لشگر کشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نهد رو به هر ملک تاراج را</p></div>
<div class="m2"><p>رباید ز فرق شهان تاج را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرفتم که گیتی بگیرد تمام</p></div>
<div class="m2"><p>به دستش دهد ملک و ملت زمام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به کوشش برآید به چرخ بلند</p></div>
<div class="m2"><p>نخواهد شدن بیش ازین بهره مند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همان به که کوس قناعت زند</p></div>
<div class="m2"><p>در رستگاری و طاعت زند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سکندر چو از وی شنید این سخن</p></div>
<div class="m2"><p>درخت انانی شکستش ز بن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگفت آن که رو در هدایت بود</p></div>
<div class="m2"><p>نصیحت همینش کفایت بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وز آن پس به خاقان در صلح کوفت</p></div>
<div class="m2"><p>ز راهش غبار خصومت بروفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شد از خاطر صافی انصاف ده</p></div>
<div class="m2"><p>که از هر چه جوید شه انصاف به</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهان پادشاها در انصاف کوش</p></div>
<div class="m2"><p>ز جام عدالت می صاف نوش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به انصاف و عدل است گیتی به پای</p></div>
<div class="m2"><p>سپاهی چو آن نیست گیتی گشای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر ملک خواهی ره عدل پوی</p></div>
<div class="m2"><p>وگر نی ز دل این هوس را بشوی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تهی قبضه از تیر تدبیر باش</p></div>
<div class="m2"><p>به تیغ عدالت جهانگیر باش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنان زی که گر باشدت شرق جای</p></div>
<div class="m2"><p>کنندت طلب اهل غرب از خدای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه زانسان که در ری شوی جایگیر</p></div>
<div class="m2"><p>به نفرینت از روم خیزد نفیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شد از دست ظلم تو کشور خراب</p></div>
<div class="m2"><p>به ملک دگر پا مکن در رکاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به ملک خودت نیست جز ظلم خوی</p></div>
<div class="m2"><p>چه آری به اقلیم بیگانه روی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رعیت به ظلم تو چون عالمند</p></div>
<div class="m2"><p>ز ظلم تو بر یکدگر ظالمند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به عدل آر رو تا که عادل شوند</p></div>
<div class="m2"><p>همه با تو در عدل یکدل شوند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دل شه چو میل عنایت کند</p></div>
<div class="m2"><p>عنایت به مردم سرایت کند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وگر شیوه ظلم گیرد به پیش</p></div>
<div class="m2"><p>شوند اهل عالم همه ظلم کیش</p></div></div>