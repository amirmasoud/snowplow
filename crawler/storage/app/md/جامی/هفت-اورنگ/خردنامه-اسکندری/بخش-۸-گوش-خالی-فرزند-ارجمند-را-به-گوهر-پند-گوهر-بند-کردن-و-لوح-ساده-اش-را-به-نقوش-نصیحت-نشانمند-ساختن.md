---
title: >-
    بخش ۸ - گوش خالی فرزند ارجمند را به گوهر پند گوهر بند کردن و لوح ساده اش را به نقوش نصیحت نشانمند ساختن
---
# بخش ۸ - گوش خالی فرزند ارجمند را به گوهر پند گوهر بند کردن و لوح ساده اش را به نقوش نصیحت نشانمند ساختن

<div class="b" id="bn1"><div class="m1"><p>بیا ای جگر گوشه فرزند من</p></div>
<div class="m2"><p>بنه گوش بر گوهر پند من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدف وار بنشین دمی لب خموش</p></div>
<div class="m2"><p>چو گوهر فشانم به من دار گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شنو پند و دانش به آن یار کن</p></div>
<div class="m2"><p>چو دانستی آنگه به آن کار کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز گوش ار نیفتد به دل نور هوش</p></div>
<div class="m2"><p>چه سوراخ موش و چه سوراخ گوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دانش که با آن کنش یار نیست</p></div>
<div class="m2"><p>به جز ناخردمند را کار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیاید ز دل سرمه دانیت خوش</p></div>
<div class="m2"><p>چو نبود ازان دیده ات سرمه کش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزرگان که تعلیم دین کرده اند</p></div>
<div class="m2"><p>به خردان وصیت چنین کرده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که ای همچو خورشید روشن ضمیر</p></div>
<div class="m2"><p>چو صبح از صفا شیوه صدق گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر کار دل با خدا راست دار</p></div>
<div class="m2"><p>که از راستکاری شوی رستگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به طاعت چه حاصل که پشتت دوتاست</p></div>
<div class="m2"><p>چو روی دلت نیست با قبله راست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی باش روشندل و صاف رای</p></div>
<div class="m2"><p>به انصاف با بندگان خدای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به هر ناکس و کس درین کارگاه</p></div>
<div class="m2"><p>ز خود می ده انصاف و از کس مخواه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دم صبحگاهان چو گردان سپهر</p></div>
<div class="m2"><p>بر آفاق مگشای جز چشم مهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازان چرخ را برتری حاصل است</p></div>
<div class="m2"><p>که هر ذره را مهر او شامل است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو باید بزرگیت پیرانه سر</p></div>
<div class="m2"><p>به چشم بزرگی به پیران نگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همی کن به پیران بی کس کسی</p></div>
<div class="m2"><p>کزین شیوه دانم به پیری رسی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به تخصیص پیری که سرور بود</p></div>
<div class="m2"><p>به پیری به هم پیر پرور بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به خردان به چشم حقارت مبین</p></div>
<div class="m2"><p>بسا خرد صدر بزرگی نشین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بود قیمت گوهر از آب و رنگ</p></div>
<div class="m2"><p>چه غم زانکه خرد است نسبت به سنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به هر دشمنی کان برونی بود</p></div>
<div class="m2"><p>وگر دشمنیهاش خونی بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به حلم و مدارا چو کوه آی پیش</p></div>
<div class="m2"><p>ز تیغ جفایش مکش فرق خویش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به خصم درونی که آن نفس توست</p></div>
<div class="m2"><p>ز تو بردباری نباشد درست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در آزار او تیغ خونریز باش</p></div>
<div class="m2"><p>به خونریزیش دمبدم تیز باش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نصیحتگری بر دل دوستان</p></div>
<div class="m2"><p>بود چون دم صبح بر بوستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به باغ ار نباشد صبا بهره ده</p></div>
<div class="m2"><p>ز دل غنچه را کی گشاید گره</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به درویش محتاج بخشش نمای</p></div>
<div class="m2"><p>فرو بسته کارش به بخشش گشای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بود او چو لب تشنه کشت و تو میغ</p></div>
<div class="m2"><p>چرا داری از کشت باران دریغ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز نادان که اسراردان سخن</p></div>
<div class="m2"><p>نباشد بگردان عنان سخن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو گردد ازو خرمنت شعله خیز</p></div>
<div class="m2"><p>پی کشتن شعله روغن مریز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تواضع کن آن را که دانشور است</p></div>
<div class="m2"><p>به دانش ز تو قدر او برتر است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بود دانش آب و زمین بلند</p></div>
<div class="m2"><p>ز آب روان کی شود بهره مند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کی افتد به کف مرد را در ناب</p></div>
<div class="m2"><p>سر خود نبرده فرو زیر آب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زبان سوده شد زین سخن خامه را</p></div>
<div class="m2"><p>ورق شد سیه زین رقم نامه را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چه خوش گفت دانا که در خانه کس</p></div>
<div class="m2"><p>چو باشد ز گوینده یک حرف بس</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همان به که در کوی دل ره کنیم</p></div>
<div class="m2"><p>زبان را بدین حرف کوته کنیم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بیا ساقی و طرح نو درفکن</p></div>
<div class="m2"><p>گلین خشت از طارم خم بکن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>برآور به خلوتگه جست و جوی</p></div>
<div class="m2"><p>به آن خشت بر من در گفت و گوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بیا مطرب و عود را ساز ده</p></div>
<div class="m2"><p>ز تار ویم بر زبان بند نه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو او پرده سازد شوم جمله گوش</p></div>
<div class="m2"><p>نشینم ز بیهوده گویی خموش</p></div></div>