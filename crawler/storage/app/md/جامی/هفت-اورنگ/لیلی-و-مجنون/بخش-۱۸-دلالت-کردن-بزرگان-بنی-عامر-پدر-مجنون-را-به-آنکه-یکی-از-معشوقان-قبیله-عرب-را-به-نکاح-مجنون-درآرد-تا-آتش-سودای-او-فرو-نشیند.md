---
title: >-
    بخش ۱۸ - دلالت کردن بزرگان بنی عامر پدر مجنون را به آنکه یکی از معشوقان قبیله عرب را به نکاح مجنون درآرد تا آتش سودای او فرو نشیند
---
# بخش ۱۸ - دلالت کردن بزرگان بنی عامر پدر مجنون را به آنکه یکی از معشوقان قبیله عرب را به نکاح مجنون درآرد تا آتش سودای او فرو نشیند

<div class="b" id="bn1"><div class="m1"><p>چون قیس دریده جیب و دامان</p></div>
<div class="m2"><p>از پند پدر نشد به سامان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اعیان قبیله پیش آن پیر</p></div>
<div class="m2"><p>گفتند به حسن راء/ی و تدبیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کای عامری فلک عماری</p></div>
<div class="m2"><p>معموری ملک کامگاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرزند تو نور دیده ماست</p></div>
<div class="m2"><p>آرام دل رمیده ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم و دل ما به اوست روشن</p></div>
<div class="m2"><p>آب و گل ما ازوست روشن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آتش مهر او سپندیم</p></div>
<div class="m2"><p>تا چند بر آتشش پسندیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون عشق و وفاست در سرشتش</p></div>
<div class="m2"><p>این واقعه گشت سرنوشتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن را که فتد چنین بلایی</p></div>
<div class="m2"><p>گر زانکه طلب کند دوایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرط است ره سفر گرفتن</p></div>
<div class="m2"><p>یا مهر بتی دگر گرفتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خرد است و زو سفر نیاید</p></div>
<div class="m2"><p>وز عهده آن به در نیاید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن به که پریرخی بجویی</p></div>
<div class="m2"><p>مشهور جهان به خوبرویی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در عقد و نکاح وی در آری</p></div>
<div class="m2"><p>همت به صلاح او گماری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باشد گیرد بدو تسلی</p></div>
<div class="m2"><p>فارغ شود از هوای لیلی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در خدمت آن میان ببندد</p></div>
<div class="m2"><p>وز قصه این زبان ببندد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این نکته ز صاحبان تدبیر</p></div>
<div class="m2"><p>افتاد پسند خاطر پیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگشاد زبان و قیس را خواند</p></div>
<div class="m2"><p>پیش نظرش به لطف بنشاند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت ای ز تو بخت من خجسته</p></div>
<div class="m2"><p>در دیده چو مردمم نشسته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چشمم به شمایل تو بیناست</p></div>
<div class="m2"><p>وز پشتی توست پشت من راست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طبعم به تو شاد و سینه خرم</p></div>
<div class="m2"><p>حالم ز جدایی تو درهم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا چند ز خانه فرد باشی</p></div>
<div class="m2"><p>تنها رو و هرزه گرد باشی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکچند به سوی خانه باز آی</p></div>
<div class="m2"><p>چون مرغ به آشیانه باز آی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ور نیست به خانه ات قراری</p></div>
<div class="m2"><p>همخانه کنم تو را نگاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا صحبت تو به ناز دارد</p></div>
<div class="m2"><p>وز هرزه رویت باز دارد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گاهی که قدم نهی به خانه</p></div>
<div class="m2"><p>بوسد قدمت چو آستانه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور سوی برون شوی خرامان</p></div>
<div class="m2"><p>در پای تو سر نهد چو دامان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عم تو که هست نقطه غم</p></div>
<div class="m2"><p>از صفحه روزگار او کم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در پرده یکی نگار دارد</p></div>
<div class="m2"><p>کز مه به جمال عار دارد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صافی بدنی چو در مکنون</p></div>
<div class="m2"><p>از حقه تنگ وصف بیرون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همشیره شهد شکر او</p></div>
<div class="m2"><p>همخوابه ناز عبهر او</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر دم به جهان فکنده پرتو</p></div>
<div class="m2"><p>از قامت او قیامتی نو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آوازه او به هر دیاری</p></div>
<div class="m2"><p>آواره او چو تو هزاری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بیرون ز حساب عقل مالش</p></div>
<div class="m2"><p>وز مال بسی فزون جمالش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در افسر جاه همسر توست</p></div>
<div class="m2"><p>در اصل و نسب برابر توست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر دامن تو نه ننگی از وی</p></div>
<div class="m2"><p>بر شیشه تو نه سنگی از وی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حیف است چنین دو گوهر پاک</p></div>
<div class="m2"><p>ناگشته به وصل هم طربناک</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خواهم که شود قرینه تو</p></div>
<div class="m2"><p>خشنود به مهر و کینه تو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گردد به تو جلوه گر ز یک سلک</p></div>
<div class="m2"><p>نا سفته درش شود تو را ملک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باشید به هم چو جان و دل دوست</p></div>
<div class="m2"><p>بادام صفت دو مغز و یک پوست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گردید به هم رفیق و دمساز</p></div>
<div class="m2"><p>نی نیش حسود و زخم غماز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون قیس شنید این سخن را</p></div>
<div class="m2"><p>بگشاد لب شکرشکن را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هم از مژه هم ز لب گهر سفت</p></div>
<div class="m2"><p>افشاند سرشک و با پدر گفت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کای اصل وجود و گوهر من</p></div>
<div class="m2"><p>خاک قدم تو افسر من</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گل کرده توست آب و خاکم</p></div>
<div class="m2"><p>پرورده توست جان پاکم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>من عیسی مریمم درین دیر</p></div>
<div class="m2"><p>در راه مجردی سبک سیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خورشید وشم ازین و آن فرد</p></div>
<div class="m2"><p>پیوند بریده از زن و مرد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دارم دلی از جهان رمیده</p></div>
<div class="m2"><p>آن به که من بلا رسیده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا در خم این رواق باشم</p></div>
<div class="m2"><p>زآویزش جفت طاق باشم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دیوانه ام از بلند رایی</p></div>
<div class="m2"><p>دیوانه چه مرد کدخدایی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>من بار خود افکنم ز گردن</p></div>
<div class="m2"><p>در بار کسان چرا دهم تن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جز من نسزد رفیق من کس</p></div>
<div class="m2"><p>تنهایی من رفیق من بس</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بیچاره پدر چو کرد ازو گوش</p></div>
<div class="m2"><p>این طرفه جواب رفت از هوش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گفتا که ز کدخدایی تو</p></div>
<div class="m2"><p>باشد غرضم رهایی تو</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>باشد یابی به کدخدایی</p></div>
<div class="m2"><p>از لیلی و عشق او رهایی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پیوند کنی به دیگری سخت</p></div>
<div class="m2"><p>بندد غم لیلی از دلت رخت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یک کفش بود برای یک پای</p></div>
<div class="m2"><p>یک دل نشود دو دوست را جای</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ماء/وای دو خصم نیست یک باغ</p></div>
<div class="m2"><p>شهباز آید سفر کند زاغ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گفت ای پدر این چه حیله سازیست</p></div>
<div class="m2"><p>با بیدلی این چه عشوه بازیست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هیهات که بگسلم ز لیلی</p></div>
<div class="m2"><p>تا سیر شود دلم ز لیلی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>لیلی نقش و دلم نگین است</p></div>
<div class="m2"><p>لیلی تخم و دلم زمین است</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>لیلی جان است و من تن او را</p></div>
<div class="m2"><p>او طوطی و دل نشیمن او را</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا تن جان را بود نشیمن</p></div>
<div class="m2"><p>من باشم و لیلی لیلی و من</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گشتم یکسر همه جهان را</p></div>
<div class="m2"><p>دیدم یک یک جهانیان را</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هر چیز که روی در خلل داشت</p></div>
<div class="m2"><p>چون در نگریستم بدل داشت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>الا لیلی که گر نیاید</p></div>
<div class="m2"><p>چیزی دگرش بدل نشاید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بر بی بدل ار بدل گزینم</p></div>
<div class="m2"><p>جز در دل و دین خلل نبینم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چون دید پدر که حال مجنون</p></div>
<div class="m2"><p>از پند نمی شود دگرگون</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>با خاطر خوش شدش دعاگوی</p></div>
<div class="m2"><p>در کش مکش قضا رضاجوی</p></div></div>