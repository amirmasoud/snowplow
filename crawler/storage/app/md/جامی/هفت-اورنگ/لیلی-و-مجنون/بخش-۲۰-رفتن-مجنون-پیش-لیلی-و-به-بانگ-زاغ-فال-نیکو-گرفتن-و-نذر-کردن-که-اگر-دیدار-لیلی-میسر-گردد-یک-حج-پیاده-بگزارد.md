---
title: >-
    بخش ۲۰ - رفتن مجنون پیش لیلی و به بانگ زاغ فال نیکو گرفتن و نذر کردن که اگر دیدار لیلی میسر گردد یک حج پیاده بگزارد
---
# بخش ۲۰ - رفتن مجنون پیش لیلی و به بانگ زاغ فال نیکو گرفتن و نذر کردن که اگر دیدار لیلی میسر گردد یک حج پیاده بگزارد

<div class="b" id="bn1"><div class="m1"><p>چون باز سفیده دم درین باغ</p></div>
<div class="m2"><p>بنشست بر آشیانه زاغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاغان سیه ز سهم آن باز</p></div>
<div class="m2"><p>کردند ز آشیانه پرواز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد قیس چو باز صبحدم خیز</p></div>
<div class="m2"><p>مقراض دو پا به ره بری تیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون از ره حی برید لختی</p></div>
<div class="m2"><p>ناگاه پدید شد درختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبز و خرم چو نخل مینا</p></div>
<div class="m2"><p>بگشاد به آن دو چشم بینا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخشنده بصر بدید زاغی</p></div>
<div class="m2"><p>چون دود چراغی و چراغی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در تیره شبی ستاره دو</p></div>
<div class="m2"><p>با انگشتی شراره دو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عباسی خلعتی مرتب</p></div>
<div class="m2"><p>کرده ز پی خلافت شب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بانگی دو سه زد لطیف و موزون</p></div>
<div class="m2"><p>نزدیک عرب به فال میمون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مجنون زان بانگ در طرب شد</p></div>
<div class="m2"><p>رقاص نشیمن طلب شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یعنی که خوش است فالم امروز</p></div>
<div class="m2"><p>روزی گردد وصالم امروز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر من باشد حجی پیاده</p></div>
<div class="m2"><p>یک حج چه بود که صد زیاده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر بار دهد به خاطر خوش</p></div>
<div class="m2"><p>سوی خودم آن نگار مهوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون راه به خیمه گاه حی برد</p></div>
<div class="m2"><p>وز حی به حریم دوست پی برد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرموده اجازت دلخولش</p></div>
<div class="m2"><p>بنشاند به مسند قبولش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سر نامه راز برگشادند</p></div>
<div class="m2"><p>هر راز که بود شرح دادند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گاه از ستم فراق گفتند</p></div>
<div class="m2"><p>گاه از غم اشتیاق گفتند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کردند دو همنشین و همراز</p></div>
<div class="m2"><p>معشوقی و عاشقی به هم ساز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لیلی به سریر پادشاهی</p></div>
<div class="m2"><p>مجنون به نفیر دادخواهی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لیلی و سر شرف بر افلاک</p></div>
<div class="m2"><p>مجنون و رخ نیاز بر خاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لیلی و به خنده شکرافشان</p></div>
<div class="m2"><p>مجنون و زدیده گوهر افشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لیلی و ز حسن ناز بر ناز</p></div>
<div class="m2"><p>مجنون و ز عشق راز در راز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لیلی نه که شمع صبح خیزان</p></div>
<div class="m2"><p>مجنون نه که ابر فیض ریزان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لیلی نه که ماه عالم افروز</p></div>
<div class="m2"><p>مجنون نه که آتش جهانسوز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لیلی نه که لاله بر سر کوه</p></div>
<div class="m2"><p>مجنون نه که کوه رنج و اندوه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لیلی چه سخن چراغ دلها</p></div>
<div class="m2"><p>مجنون به گداز داغ دلها</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لیلی به دو زلف و مشکبیزی</p></div>
<div class="m2"><p>مجنون به دوچشم اشکریزی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لیلی گلی از گلاب شسته</p></div>
<div class="m2"><p>مجنون خسی از سراب رسته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لیلی به نشاط خودپسندی</p></div>
<div class="m2"><p>مجنون به بساط دردمندی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بردند به سر دو آرزومند</p></div>
<div class="m2"><p>با هم روزی ز دور خرسند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر راز که داشتند گفتند</p></div>
<div class="m2"><p>هر نکته که خواستند سفتند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یک درد دلی نگفته کم ماند</p></div>
<div class="m2"><p>یک غنچه ناشگفته کم ماند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون خواست وداع آن دلارای</p></div>
<div class="m2"><p>مجنون به نیاز خاست بر پای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کای کعبه رهروان مشتاق</p></div>
<div class="m2"><p>وی قبله نیکوان آفاق</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گلزار ارم حریم کویت</p></div>
<div class="m2"><p>زوار حرم مقیم کویت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گیسوی تو طوق تاجداران</p></div>
<div class="m2"><p>بر بوی تو شوق بی قراران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خلخال زر تو تاج بر سر</p></div>
<div class="m2"><p>سلسال لب تو رشک کوثر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر موی تو را ز زلف شبگون</p></div>
<div class="m2"><p>آشفته چون من هزار مجنون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بگشاده لبت به خنده کوشی</p></div>
<div class="m2"><p>بازارچه شکر فروشی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بستم چو گشاده طبع و شادان</p></div>
<div class="m2"><p>احرام در تو بامدادان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گفتم که سجود خاک این در</p></div>
<div class="m2"><p>امروزم اگر شود میسر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بر من باشد که بندم احرام</p></div>
<div class="m2"><p>زین در به طواف حج اسلام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اکنون که به کام خود رسیدم</p></div>
<div class="m2"><p>رویت به مراد خود بدیدم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فرمان تو گر بود درین کار</p></div>
<div class="m2"><p>بندم سوی حج ز منزلت بار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر عمر بود دگر بیابم</p></div>
<div class="m2"><p>با پا روم و به سر بیایم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ور گردد جیب عمر پاره</p></div>
<div class="m2"><p>ماشاء/الله کان چه چاره</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لیلی ز وی این سخن چو بشنید</p></div>
<div class="m2"><p>بر خویش چو زلف خویش پیچید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گفت ای ره صدق منهج تو</p></div>
<div class="m2"><p>تو حج منی و من حج تو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر چهره به وصل هم فروزیم</p></div>
<div class="m2"><p>زان به که به هجر هم بسوزیم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>روزی که من از تو دور باشم</p></div>
<div class="m2"><p>خود گو که چه سان صبور باشم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو شاد به شغل حج گزاری</p></div>
<div class="m2"><p>من زار به کنج سوگواری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گفتا ز عنایت خدایی</p></div>
<div class="m2"><p>خواهم که به محنت جدایی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>صابر دارد تو را مرا هم</p></div>
<div class="m2"><p>چندان که رسیم باز با هم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>این گفت و ز دیده خون روان کرد</p></div>
<div class="m2"><p>گریان گریان وداع جان کرد</p></div></div>