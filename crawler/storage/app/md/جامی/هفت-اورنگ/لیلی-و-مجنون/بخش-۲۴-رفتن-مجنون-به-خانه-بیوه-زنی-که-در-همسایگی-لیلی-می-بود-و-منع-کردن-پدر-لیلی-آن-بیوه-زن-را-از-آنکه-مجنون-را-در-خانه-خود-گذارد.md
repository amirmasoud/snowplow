---
title: >-
    بخش ۲۴ - رفتن مجنون به خانه بیوه زنی که در همسایگی لیلی می بود و منع کردن پدر لیلی آن بیوه زن را از آنکه مجنون را در خانه خود گذارد
---
# بخش ۲۴ - رفتن مجنون به خانه بیوه زنی که در همسایگی لیلی می بود و منع کردن پدر لیلی آن بیوه زن را از آنکه مجنون را در خانه خود گذارد

<div class="b" id="bn1"><div class="m1"><p>همسایه لیلی آن جمیله</p></div>
<div class="m2"><p>می بود زنی نه زان قبیله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کربت غربتش درون ریش</p></div>
<div class="m2"><p>وز محنت بیوگی غم اندیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برداشته شوهر از سرش پای</p></div>
<div class="m2"><p>وز وی دو یتیم مانده بر جای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بودند به هم غریب و مهجور</p></div>
<div class="m2"><p>هم معده گرسنه هم بدن عور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مجنون چو ز گنج وصل محروم</p></div>
<div class="m2"><p>کردی چو چغذ میل آن بوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمخانه وی مقام کردی</p></div>
<div class="m2"><p>در خدمت وی قیام کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن هر دو یتیم را چو دیدی</p></div>
<div class="m2"><p>دست شفقت به سر کشیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر سیم و زرش که دست دادی</p></div>
<div class="m2"><p>پوشیده به دستشان نهادی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون سایه یار رفتش از دست</p></div>
<div class="m2"><p>همسایه وی به جاش بنشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بادیه تشنه جان غمناک</p></div>
<div class="m2"><p>مالد لب خود به ریگ نمناک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی آب فتاده در تب و تاب</p></div>
<div class="m2"><p>جوید از ریگ تری آب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترک همه قیل قال کردی</p></div>
<div class="m2"><p>وز دلبر خود سئوال کردی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتی چون است و حال او چیست</p></div>
<div class="m2"><p>نظارگی جمال او کیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیوند وصال با که دارد</p></div>
<div class="m2"><p>آیین دلال با که دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون من دگریش هست یا نه</p></div>
<div class="m2"><p>با من نظریش هست یا نه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دام دل کیست گیسوانش</p></div>
<div class="m2"><p>محراب که طاق ابروانش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لعلش به عتاب خنده آمیز</p></div>
<div class="m2"><p>در کام که می کند شکرریز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درج گهرش به وقت گفتار</p></div>
<div class="m2"><p>بر گوش که می شود گهربار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من می سوزم ز آرزویش</p></div>
<div class="m2"><p>تا کیست نشسته پیش رویش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من می میرم ز اشتیاقش</p></div>
<div class="m2"><p>تا کیست ملازم وثاقش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با آن همه نازنینی او</p></div>
<div class="m2"><p>حاشا من و همنشینی او</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این بس که به خانه ات نشینم</p></div>
<div class="m2"><p>ربع و طللش ز دور بینیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این گفتی و بر زمین فتادی</p></div>
<div class="m2"><p>وز هر مژه سیل خون گشادی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چندان ز دو دیده اشک راندی</p></div>
<div class="m2"><p>کش تاب گریستن نماندی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از بی تابی برفتی از هوش</p></div>
<div class="m2"><p>کردی ز همه جهان فراموش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن بیوه زنش به رخ زدی آب</p></div>
<div class="m2"><p>شستیش ز دیده سرمه خواب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زان خواب چو چشمش آمدی باز</p></div>
<div class="m2"><p>رفتن کردی به جای خود ساز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>محروم ز یار روزگاری</p></div>
<div class="m2"><p>جز این تک و پو نداشت کاری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لیکن فلک ستیزه پیشه</p></div>
<div class="m2"><p>کش پیشه همین بود همیشه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یک داغ دگر به دل نهادش</p></div>
<div class="m2"><p>بر تافت زمام این مرادش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>لیلی خواهان قدم نهادند</p></div>
<div class="m2"><p>پیش پدرش زبان گشادند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زآمد شد او فسانه گفتند</p></div>
<div class="m2"><p>گرد وی ازان ستانه رفتند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کین پدرش دگر بجوشید</p></div>
<div class="m2"><p>در طعنه بیوه زن خروشید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کای سفله ناکس این چه سستی ست</p></div>
<div class="m2"><p>در کار من این چه نادرستی ست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن را که ببرد نام و ننگم</p></div>
<div class="m2"><p>بر جام شرف فکند سنگم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در خانه خود چرا دهی راه</p></div>
<div class="m2"><p>گر بار دگر درین گذرگاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گردن به رضای او درآی</p></div>
<div class="m2"><p>می دان به یقین که سر نداری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بیچاره چو آن عتاب بشنید</p></div>
<div class="m2"><p>بر خویش چو نی در آب لرزید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مجنون رمیده دل دگر بار</p></div>
<div class="m2"><p>چون از ره دور شد پدیدار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زد بانگ که ای خجسته فرزند</p></div>
<div class="m2"><p>آزار من شکسته مپسند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دیگر ره خانه ام مپیمای</p></div>
<div class="m2"><p>در ساحت خیمه ام منه پای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>لیلی به تو در مقام یاریست</p></div>
<div class="m2"><p>لیکن پدرش به کین گذاریست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>او میر قبیله من گدایم</p></div>
<div class="m2"><p>با صولت او کجا بس آیم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تنها نه ز جان خویش ترسم</p></div>
<div class="m2"><p>بر زندگی تو بیش ترسم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دیگر ز درم قدم نگه دار</p></div>
<div class="m2"><p>راندم دم راست دم نگه دار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مجنون ز حدیث او بر آشفت</p></div>
<div class="m2"><p>گریان گریان به زیر لب گفت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کای مادر مشفق این چه کار است</p></div>
<div class="m2"><p>کز مشفقیت دلم فگار است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ما هر دو غریب این دیاریم</p></div>
<div class="m2"><p>بیگانگیی ز هم نداریم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از خدمت خویش راندنم چیست</p></div>
<div class="m2"><p>خونابه ز دل چکاندنم چیست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر کس که ز غربتش نصیب است</p></div>
<div class="m2"><p>آزار غریب ازو غریب است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در نامه نسبت نسیبان</p></div>
<div class="m2"><p>خویشند به هم همه غریبان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>باشد ورق ادب دریدن</p></div>
<div class="m2"><p>خط بر ورق نسب کشیدن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در کوی تو رو به لیلی ام بود</p></div>
<div class="m2"><p>زین روی بسی تسلی ام بود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>واکنون که ز من بتافتی روی</p></div>
<div class="m2"><p>از جان و دلم تو را دعاگوی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از کوی تو رخت بستم اینک</p></div>
<div class="m2"><p>در ورطه خون نشستم اینک</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شاد آمدم و حزین برفتم</p></div>
<div class="m2"><p>با حال چنان چنین برفتم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دارم ز تو چشم آنکه گاهی</p></div>
<div class="m2"><p>کافتد سوی لیلیت نگاهی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یادآوری از غریبی من</p></div>
<div class="m2"><p>وز محنت بی نصیبی من</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گویی به زبان من دعایش</p></div>
<div class="m2"><p>خواهی ز برای من لقایش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گر بر هدف اجابت آید</p></div>
<div class="m2"><p>این عقده ز کار من گشاید</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ور نه ز فراق او بمیرم</p></div>
<div class="m2"><p>دامن به قیامتش بگیرم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>این نکته بگفت و شد شتابان</p></div>
<div class="m2"><p>وحشت زده روی در بیابان</p></div></div>