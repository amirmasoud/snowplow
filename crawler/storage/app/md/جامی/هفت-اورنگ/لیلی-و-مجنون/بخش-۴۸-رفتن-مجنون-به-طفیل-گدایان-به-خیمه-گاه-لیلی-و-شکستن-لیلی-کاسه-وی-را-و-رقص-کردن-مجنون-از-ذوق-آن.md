---
title: >-
    بخش ۴۸ - رفتن مجنون به طفیل گدایان به خیمه گاه لیلی و شکستن لیلی کاسه وی را و رقص کردن مجنون از ذوق آن
---
# بخش ۴۸ - رفتن مجنون به طفیل گدایان به خیمه گاه لیلی و شکستن لیلی کاسه وی را و رقص کردن مجنون از ذوق آن

<div class="b" id="bn1"><div class="m1"><p>شیرین سخن شکر فسانه</p></div>
<div class="m2"><p>کین قصه نهاد در میانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسانه پوست چون فرو خواند</p></div>
<div class="m2"><p>از پوست برون چنین سخن راند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کان خورده چو دف طپانچه بر پوست</p></div>
<div class="m2"><p>در ناله ز دست فرقت دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می گشت به کوه و دشت یکچند</p></div>
<div class="m2"><p>از دوست همی به پوست خرسند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون پوست نشان ز دوست می داد</p></div>
<div class="m2"><p>خود را تسکین به پوست می داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وان دم که زمانه کند ازو پوست</p></div>
<div class="m2"><p>وان نیز به کف نماندش از دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می برد به سر به کام دشمن</p></div>
<div class="m2"><p>نی دوست به بر نه پوست بر تن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی دوست که بود رفته جانی</p></div>
<div class="m2"><p>بی پوست چه بود استخوانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون یکچندی بر این برآمد</p></div>
<div class="m2"><p>دودش ز دل حزین برآمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک روز به وقت نیمروزان</p></div>
<div class="m2"><p>شد پیش شبان ز درد سوزان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون سایه به زیر پایش افتاد</p></div>
<div class="m2"><p>برداشت ز سوز سینه فریاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کای چاره گر درون ریشم</p></div>
<div class="m2"><p>روزی عجب آمده ست پیشم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در حال دلم نظاره ای کن</p></div>
<div class="m2"><p>مردم ز فراق چاره ای کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین پیش ز هجر مرده بودم</p></div>
<div class="m2"><p>جان را به اجل سپرده بودم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>انفاس توام به لطف بنواخت</p></div>
<div class="m2"><p>وز نو چو مسیح زنده ام ساخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>افکن نظری دگر به کارم</p></div>
<div class="m2"><p>کامروز همان امید دارم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگریست به درد کای جوانمرد</p></div>
<div class="m2"><p>سر تا به قدم همه غم و درد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز اندوه تو شد مرا جگرخون</p></div>
<div class="m2"><p>وز درد تو اشک من جگرگون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بختت به مراد دل رساناد</p></div>
<div class="m2"><p>بر مسند دولتت نشاناد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از هیچ مقام و هیچ جایی</p></div>
<div class="m2"><p>زین بیش نبینمت دوایی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کان نقش بدیع کلک تصویر</p></div>
<div class="m2"><p>وان شیرین تر ز شکر و شیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر اول هفت وقت شامی</p></div>
<div class="m2"><p>از شیر رمه پزد طعامی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خاصه پی طعمه گدایان</p></div>
<div class="m2"><p>از خوان سپهر بینوایان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر کس که بود در آن حوالی</p></div>
<div class="m2"><p>از سفره رزق دست خالی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آرند به آستان او روی</p></div>
<div class="m2"><p>از خوان نوال او غذا جوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مالد سر آستین خود باز</p></div>
<div class="m2"><p>قسامی آن به خود کند ساز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کفلیز به کف طعام سنجد</p></div>
<div class="m2"><p>در کاسه هر کس آنچه گنجد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دارند آندم در آن گذرگاه</p></div>
<div class="m2"><p>بیگانه و آشنا همه راه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>امشب هنگام کام بخشیست</p></div>
<div class="m2"><p>بی شامان را طعام بخشیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برخیز تو نیز کاسه بر کف</p></div>
<div class="m2"><p>خود را افکن به سلک آن صف</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باشد که طفیل هر گدایی</p></div>
<div class="m2"><p>زان مایده ات رسد نوایی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مجنون چو شنید این بشارت</p></div>
<div class="m2"><p>برخاست به موجب اشارت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بگرفت به کف شکسته جامی</p></div>
<div class="m2"><p>می زد به حریم دوست گامی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن دلشده چون رسید آنجا</p></div>
<div class="m2"><p>صد دلشده بیش دید آنجا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر دست گرفته کاسه یا جام</p></div>
<div class="m2"><p>دریوزه گرش ز خوان انعام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر کس ز کف چنان حبیبی</p></div>
<div class="m2"><p>می یافت به قدر خود نصیبی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مجنون از دور چون بدیدش</p></div>
<div class="m2"><p>عقل از سر و جان ز تن رمیدش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بی خود شد و میل خاک ره داشت</p></div>
<div class="m2"><p>خود را به حیل به پا نگه داشت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون نوبت وی رسید بی خویش</p></div>
<div class="m2"><p>آورد او نیز جام خود پیش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>لیلی وی را چو دید بشناخت</p></div>
<div class="m2"><p>کارش نه چو کار دیگران ساخت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ناداده نصیب ازان طعامش</p></div>
<div class="m2"><p>کفلیز زد و شکست جامش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مجنون چو شکست جام خود دید</p></div>
<div class="m2"><p>گویا که جهان به کام خود دید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آهنگ سماع آن شکستش</p></div>
<div class="m2"><p>چون راه سماع ساخت مستش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>می بود بر آن سرود رقاص</p></div>
<div class="m2"><p>می زد با خود ترانه خاص</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>العیش که کام شد میسر</p></div>
<div class="m2"><p>عیشی به تمام شد میسر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همچون دگران نداد کامم</p></div>
<div class="m2"><p>وز سنگ ستم شکست جامم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>با من نظریش هست تنها</p></div>
<div class="m2"><p>زان جام مرا شکست تنها</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بیهوده شکست من نجسته ست</p></div>
<div class="m2"><p>کارم زشکست او درست است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آن سنگ که زد به جام من فاش</p></div>
<div class="m2"><p>زان کاسه سرشکستیم کاش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا در صف واقعان این راز</p></div>
<div class="m2"><p>جاوید نشستمی سرافراز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گر جام مرا شکست یارم</p></div>
<div class="m2"><p>آزردگیی جز این ندارم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کان لحظه مرا که جام بشکست</p></div>
<div class="m2"><p>آزرده نگشته باشدش دست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>صد سر فدی شکست او باد</p></div>
<div class="m2"><p>جانها شده مزد دست او باد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از خنجر مهر او دلم چاک</p></div>
<div class="m2"><p>وز هر چه نه مهر او دلم پاک</p></div></div>