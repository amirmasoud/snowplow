---
title: >-
    بخش ۵۰ - خبر یافتن اعرابی از حال مجنون و به زیارت وی رفتن و چند روز با وی بودن و اشعار یاد گرفتن
---
# بخش ۵۰ - خبر یافتن اعرابی از حال مجنون و به زیارت وی رفتن و چند روز با وی بودن و اشعار یاد گرفتن

<div class="b" id="bn1"><div class="m1"><p>محمل بند عروس این راز</p></div>
<div class="m2"><p>آهنگ حدی چنین کند ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز بر عرب یکی عرابی</p></div>
<div class="m2"><p>مقبول خرد به خرده یابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عرصه عشق پاکبازی</p></div>
<div class="m2"><p>در نکته شعر سحر سازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آواز خوشش مهیج شوق</p></div>
<div class="m2"><p>چاک افکن جیب صاحب ذوق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشنید حدیث عشق مجنون</p></div>
<div class="m2"><p>صیت غزل چو در مکنون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوقش به عنان جان درآویخت</p></div>
<div class="m2"><p>طیاره بادپا برانگیخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پره بر و عرصه دشت</p></div>
<div class="m2"><p>بر عامریان چو باد بگذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با اهل قبیله گفتگو کرد</p></div>
<div class="m2"><p>وز هر نفری سراغ او کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتند که او ز خلق یکتاست</p></div>
<div class="m2"><p>انسش همه با وحوش صحراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>او نیز ز جنس وحش گشته ست</p></div>
<div class="m2"><p>وز انس به انسیان گذشته ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با گور و گوزن دارد آرام</p></div>
<div class="m2"><p>با اهل قبیله کم شود رام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیچاره عرابی آن چو بشنید</p></div>
<div class="m2"><p>از عامریان عنان بپیچید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دربست میان به گردبادی</p></div>
<div class="m2"><p>شد مرحله گرد کوه و وادی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می گشت به هر فراز و شیبی</p></div>
<div class="m2"><p>می خورد ز دام و دد نهیبی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ناگه گله ای ز آهوان دید</p></div>
<div class="m2"><p>و او را چو شبان در آن میان دید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر پای ستاده بی خم و پیچ</p></div>
<div class="m2"><p>همچون الفی و با الف هیچ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لیکن الفی که با سیاهی</p></div>
<div class="m2"><p>می زد ز سموم چاشتگاهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرده پس ستر پرده خویش</p></div>
<div class="m2"><p>مشتی دو گیاه از پس و پیش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وز سر شده موی تار تارش</p></div>
<div class="m2"><p>از شعر سیه به بر شعارش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با ضعف و سیاهیش تن زار</p></div>
<div class="m2"><p>زان شعر سیاه بود یک تار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون دید عرابیش بدان حال</p></div>
<div class="m2"><p>بر وی به سلام کرد اقبال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پشتش چو شد از سلام او خم</p></div>
<div class="m2"><p>کرد آن رمه از سلام او رم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مجنون به جفاش سنگ برداشت</p></div>
<div class="m2"><p>بی صلح نفیر جنگ برداشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کای بی خبر این چه دم زدن بود</p></div>
<div class="m2"><p>وز راه برون قدم زدن بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یاران مرا ز من رماندی</p></div>
<div class="m2"><p>وز دام وفای من جهاندی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این بی خردی ز خود جدا کن</p></div>
<div class="m2"><p>برگرد و مرا به من رها کن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو بند به نفس و من رهیده</p></div>
<div class="m2"><p>تو رام به طبع و من رمیده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو شاد به سور و من به ماتم</p></div>
<div class="m2"><p>ما را چه موافقیست با هم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با او به سخن نشد هم آواز</p></div>
<div class="m2"><p>کرد از سر درد لحنی آغاز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برخواند طرب فزا نسیبی</p></div>
<div class="m2"><p>دادش ز غذای جان نصیبی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شد وقت وی از سماع آن خوش</p></div>
<div class="m2"><p>وز همدمیش نشد عنانکش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون شیر و شکر به وی درآمیخت</p></div>
<div class="m2"><p>وز بیت و غزل بر او شکر ریخت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نامه درد خواند بر وی</p></div>
<div class="m2"><p>صد عقد گهر فشاند بر وی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وین همچو صدف شده همه گوش</p></div>
<div class="m2"><p>بر گوش بمانده دیده هوش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر در که به گوش می رسیدش</p></div>
<div class="m2"><p>در رشته حفظ می کشیدش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کارش همه روز تا شب این بود</p></div>
<div class="m2"><p>وردش همه شب مرتب این بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>روز آنچه ز وی شکار می کرد</p></div>
<div class="m2"><p>پایش به شب استوار می کرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حرفی که کشند روز در سلک</p></div>
<div class="m2"><p>تکرار شبش همی کند ملک</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>روزی دو سه چار بود با او</p></div>
<div class="m2"><p>وین گونه به کار بود با او</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شد راحله ز آب و زاد خالی</p></div>
<div class="m2"><p>زد دم ز وداع آن حوالی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از صحبت او برید پیوند</p></div>
<div class="m2"><p>بر خاطر ازو قصیده ای چند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بیتی که ز هر قصیده خواندی</p></div>
<div class="m2"><p>خون از دل مستمع چکاندی</p></div></div>