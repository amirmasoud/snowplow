---
title: >-
    بخش ۸ - آغاز سلسله جنبانی داستان عشق لیلی و مجنون که آن سر دفتر پردگیان حجله جمال و عفت بود و این سر حلقه زنجیریان عشق و محبت
---
# بخش ۸ - آغاز سلسله جنبانی داستان عشق لیلی و مجنون که آن سر دفتر پردگیان حجله جمال و عفت بود و این سر حلقه زنجیریان عشق و محبت

<div class="b" id="bn1"><div class="m1"><p>تاریخ نویس عشقبازان</p></div>
<div class="m2"><p>شیرین رقم سخن طرازان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سرور عاشقان چو دم زد</p></div>
<div class="m2"><p>بر لوح بیان چنین رقم زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کز عامریان بلند قدری</p></div>
<div class="m2"><p>بر صدر شرف خجسته بدری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقبول عرب به کار سازی</p></div>
<div class="m2"><p>محبوب عجم به دلنوازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از مال و منال بودش اسباب</p></div>
<div class="m2"><p>افزون ز عمارت گل و آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون خیمه درین بساط غبرا</p></div>
<div class="m2"><p>می بود مقیم کوه و صحرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صحرای عرب مخیم او</p></div>
<div class="m2"><p>معمور ز یمن مقدم او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرض رمه اش برون ز فرسنگ</p></div>
<div class="m2"><p>بر آهوی دشت کرده جا تنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اشتر گله هاش کوه کوهان</p></div>
<div class="m2"><p>چون کوه بلند پرشکوهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیشان گشتی گه چراخوار</p></div>
<div class="m2"><p>کوهستان ها زمین هموار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیلش گذران به هر کناره</p></div>
<div class="m2"><p>چون گله گور بی شماره</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگشاده دری به میزبانی</p></div>
<div class="m2"><p>در داده صلای میهمانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر شام به کوه و دشت تا روز</p></div>
<div class="m2"><p>آتش پی میهمانی افروز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حاجت طلبان به روی او شاد</p></div>
<div class="m2"><p>ویرانی شان به جودش آباد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دستش به ایادی جمیله</p></div>
<div class="m2"><p>انگشت نمای هر قبیله</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>داده کف او شکست خاتم</p></div>
<div class="m2"><p>بر بسته به جود دست حاتم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سادات عرب به چاپلوسی</p></div>
<div class="m2"><p>پیش در او به خاکبوسی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاهان عجم ز بختیاری</p></div>
<div class="m2"><p>با او به هوای دوستداری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از جاه هزار زیب و فر داشت</p></div>
<div class="m2"><p>وان از همه به که ده پسر داشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر یک ز نهال عمر شاخی</p></div>
<div class="m2"><p>وز شهر امل بلند کاخی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لیکن ز همه کهینه فرزند</p></div>
<div class="m2"><p>می داشت دلش به مهر خود بند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر دست بود بلی ده انگشت</p></div>
<div class="m2"><p>در قوت حمله جمله یک پشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باشد ز همه به سور و ماتم</p></div>
<div class="m2"><p>انگشت کهین سزای خاتم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آری بود او ز برج امید</p></div>
<div class="m2"><p>فرخنده مهی تمام خورشید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فرخندگی مه تمامش</p></div>
<div class="m2"><p>بیرون ز قیاس و قیس نامش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سالش که قدم به چارده داشت</p></div>
<div class="m2"><p>بر چارده مه خط سیه داشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یاقوت لبش به خوشنویسی</p></div>
<div class="m2"><p>ماهش به شعار مشک ریسی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تابان مه روشن از جبینش</p></div>
<div class="m2"><p>خورشید فتاده بر زمینش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ابروش بلای نازنینان</p></div>
<div class="m2"><p>محراب دعا پاکدینان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قدش نخلی عجب دلاویز</p></div>
<div class="m2"><p>بر خسته دلان ز لب رطب ریز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دور شکرش ز موی میمی</p></div>
<div class="m2"><p>زیر کمرش ز موی نیمی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گوی ذقنش ز سیم ساده</p></div>
<div class="m2"><p>سبزه ز درون برون نداده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سرو قد گلرخان دلجوی</p></div>
<div class="m2"><p>چوگان شده در هوای آن گوی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سر تا قدم از ادب سرشته</p></div>
<div class="m2"><p>بر دل رقم ادب نوشته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>طبعش ز سخن به موشکافی</p></div>
<div class="m2"><p>مشعوف به شعر شعربافی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون لعل لبش خموش بودی</p></div>
<div class="m2"><p>بر روزن راز گوش بودی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون غنچه تنگ او شکفتی</p></div>
<div class="m2"><p>سنجیده هزار نکته گفتی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کلکش ز سواد طره حور</p></div>
<div class="m2"><p>صد نقش زدی به لوح کافور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر حرف که بر ورق کشیدی</p></div>
<div class="m2"><p>بر نغز خطان ورق دریدی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>با طایفه ای ز خردسالان</p></div>
<div class="m2"><p>چون او همه مشکبو غزالان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همواره هوای گشت کردی</p></div>
<div class="m2"><p>طوافی کوه و دشت کردی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گه باز زدی به کوه دامان</p></div>
<div class="m2"><p>با کبک دری شدی خرامان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گه بنشستی به طرف وادی</p></div>
<div class="m2"><p>بر رود زدی نوای شادی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گه ره سوی چشمه سار جستی</p></div>
<div class="m2"><p>وز چشمه دل غبار شستی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گه رخت به مرغزار بردی</p></div>
<div class="m2"><p>وز دل غم روزگار بردی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>می زد قدمی به هر بهانه</p></div>
<div class="m2"><p>فارغ ز حوادث زمانه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نه در جگرش ز عشق تابی</p></div>
<div class="m2"><p>نه بر مژه اش ز شوق آبی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نه جامه صابری دریده</p></div>
<div class="m2"><p>نی ناله عاشقی کشیده</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شب خواب فراغتش ربودی</p></div>
<div class="m2"><p>بر بستر عافیت غنودی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>روزش در آرزو گشادی</p></div>
<div class="m2"><p>در هر تک و پوی رو نهادی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کامی که عنان کش دلش بود</p></div>
<div class="m2"><p>بر وفق مراد حاصلش بود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بینا نظر پدر به حالش</p></div>
<div class="m2"><p>خرم دل مادر از جمالش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ناگشته هنوز خاطراندیش</p></div>
<div class="m2"><p>کاخر ز فلک چه آیدش پیش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حالیست عجب که آدمیزاد</p></div>
<div class="m2"><p>آسوده زید درین غم آباد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>غافل که چه بر سرش نوشتند</p></div>
<div class="m2"><p>در آب و گلش چه تخم کشتند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شاخی کش از آب و خاک خیزد</p></div>
<div class="m2"><p>در دامن او چه میوه ریزد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شیرین گردد ازان دهانش</p></div>
<div class="m2"><p>یا تلخ شود مذاق جانش</p></div></div>