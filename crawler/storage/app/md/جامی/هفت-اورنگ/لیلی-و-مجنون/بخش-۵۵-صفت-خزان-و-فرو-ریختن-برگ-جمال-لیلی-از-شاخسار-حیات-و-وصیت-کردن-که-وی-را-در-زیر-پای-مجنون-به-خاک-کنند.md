---
title: >-
    بخش ۵۵ - صفت خزان و فرو ریختن برگ جمال لیلی از شاخسار حیات و وصیت کردن که وی را در زیر پای مجنون به خاک کنند
---
# بخش ۵۵ - صفت خزان و فرو ریختن برگ جمال لیلی از شاخسار حیات و وصیت کردن که وی را در زیر پای مجنون به خاک کنند

<div class="b" id="bn1"><div class="m1"><p>چون از نفس خزان درختان</p></div>
<div class="m2"><p>گشتند به باد داده رختان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خلعت سبز عور ماندند</p></div>
<div class="m2"><p>وز برگ و بهار دور ماندند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلزار ز هر گل و گیاهی</p></div>
<div class="m2"><p>شد رنگرزانه کارگاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنمود هزار رنگ بی قیل</p></div>
<div class="m2"><p>صباغ فلک ز یک خم نیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طاووس درخت پر بینداخت</p></div>
<div class="m2"><p>سلطان چمن سپر بینداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پنجره های لاجوردی</p></div>
<div class="m2"><p>کم شد سیهی فزود زردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بستان ز هوای سرد بفسرد</p></div>
<div class="m2"><p>تب لرزه ز رخ طراوتش برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرداب شمر در آن علیلی</p></div>
<div class="m2"><p>قاروره نمایی و دلیلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد هر شاخی ز برگ و بر پاک</p></div>
<div class="m2"><p>بر دوش درخت مار ضحاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خون خوردن انار خندان</p></div>
<div class="m2"><p>آلوده به خون نمود دندان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به گشت چو عاشقی رخش زرد</p></div>
<div class="m2"><p>از درد نشسته بر رخش گرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نارنج به شاخ پیش بینا</p></div>
<div class="m2"><p>گوی زر و صولجان مینا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عناب ز برگ زرد پیدا</p></div>
<div class="m2"><p>اشک و رخ عاشقان شیدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رز کرده گهی ز شاخ انگور</p></div>
<div class="m2"><p>عقد در ناب و ساعد حور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گاه از سر دار طارم تاک</p></div>
<div class="m2"><p>آویخته زنگیان بی باک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گه داده به دست دستبوسان</p></div>
<div class="m2"><p>رنگین انگشت نوعروسان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>امرود به شاخ خود نشسته</p></div>
<div class="m2"><p>بر دسته عود گوشه بسته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بادام به عبرت ایستاده</p></div>
<div class="m2"><p>صد چشم به هر طرف نهاده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باغی تهی از گل و شکوفه</p></div>
<div class="m2"><p>بغداد بدل شده به کوفه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بغداد به کوفگی نشانمند</p></div>
<div class="m2"><p>با کرگس و کوف گشته خرسند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در زاویه زوال یابی</p></div>
<div class="m2"><p>عالم ز خزان بدین خرابی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وان غیرت گلرخان بغداد</p></div>
<div class="m2"><p>یعنی لیلی گلی چمن زاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>افتاد به خار خار مردن</p></div>
<div class="m2"><p>تن بنهاده به جان سپردن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گریان شد کای ستوده مادر</p></div>
<div class="m2"><p>پاکیزه فراش پاک چادر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای مریم مهد مهرجویی</p></div>
<div class="m2"><p>بلقیس سبای نیکخویی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یک لحظه به مهر باش مایل</p></div>
<div class="m2"><p>کن دست به گردنم حمایل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روی شفقت بنه به رویم</p></div>
<div class="m2"><p>بگشا نظر کرم به سویم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زین پیش ز گفت و گوی مردم</p></div>
<div class="m2"><p>بر من نامد تو را ترحم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نگذاشتیم به دوست پیوند</p></div>
<div class="m2"><p>تا فرقت وی به مرگم افکند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرد او ز غم فراق و من نیز</p></div>
<div class="m2"><p>دل بنهادم به مرگ و تن نیز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>روزم بی او به شب رسیده</p></div>
<div class="m2"><p>جانم محمل به لب کشیده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>محمل چو ببندد از لبم هم</p></div>
<div class="m2"><p>بهرم فکنی بساط ماتم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بین غرقه به خون نشیمنم را</p></div>
<div class="m2"><p>وز سیل مژه بشو تنم را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از خلعت عصمتم کفن کن</p></div>
<div class="m2"><p>رنگش ز سرشک لعل من کن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زان رنگ ببخش رو سفیدیم</p></div>
<div class="m2"><p>کانست علامت شهیدیم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از آتش سینه مجمرم ساز</p></div>
<div class="m2"><p>وز دود جگر معطرم ساز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر بند عصابه نیازم</p></div>
<div class="m2"><p>زان ساز به عشق سرفرازم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر رخ داغم ز دود غم کش</p></div>
<div class="m2"><p>زان نیل سعادتم رقم کش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یاد آر حریف مقبلم را</p></div>
<div class="m2"><p>وآراسته ساز محملم را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>روی سفرم به خاک او کن</p></div>
<div class="m2"><p>جایم به مزار پاک او کن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بشکاف زمین به زیر پایش</p></div>
<div class="m2"><p>زن حفره به قبر دلگشایش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نه بر کف پای او سر من</p></div>
<div class="m2"><p>ساز از کف پایش افسر من</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا حشر که در وفاش خیزم</p></div>
<div class="m2"><p>آسوده ز خاک پاش خیزم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مادر چو شنید آرزویش</p></div>
<div class="m2"><p>از درد نهاد رو به رویش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بگریست که ای خجسته فرزند</p></div>
<div class="m2"><p>وز صحبت من گسسته پیوند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زین پیش اگر نه بر مرادت</p></div>
<div class="m2"><p>رفتم دل ازان حزین مبادت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آن روز نبود بی غباری</p></div>
<div class="m2"><p>در کار تو هیچم اختیاری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>وامروز که باشد اختیارم</p></div>
<div class="m2"><p>مقصود تو را به جان برآرم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>لیلی چو مراد خود روا دید</p></div>
<div class="m2"><p>از ذوق چو تازه گل بخندید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>رو سوی دیار یار دیرین</p></div>
<div class="m2"><p>افشاند به خنده جان شیرین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مادر می دید جانفشانیش</p></div>
<div class="m2"><p>می سوخت ز حسرت جوانیش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>می کند ز سر به پنجه های موی</p></div>
<div class="m2"><p>می کوفت به کف طپانچه بر روی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>روی از ناخن خراش می کرد</p></div>
<div class="m2"><p>ناخن ناخن تراش می کرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از آه به سینه چاک می زد</p></div>
<div class="m2"><p>بر خویش در هلاک می زد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دستی ننهاد بر دل خویش</p></div>
<div class="m2"><p>جز وقت طپانجه بر دل ریش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بر دل کف راحتش همین بود</p></div>
<div class="m2"><p>تسکین جراحتش همین بود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دل چون ز طپانچه گشتیش تنگ</p></div>
<div class="m2"><p>بر سینه به درد کوفتی سنگ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در سنگ زدن چو گرم گشتی</p></div>
<div class="m2"><p>سنگ از گرمیش نرم گشتی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چون برد به سر به گریه و سوز</p></div>
<div class="m2"><p>روزی که مباد کس بدان روز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>آهنگ به ساز رفتنش کرد</p></div>
<div class="m2"><p>ترتیب جهاز رفتنش کرد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>زان بیش که خواستی دل او</p></div>
<div class="m2"><p>آراسته ساخت محمل او</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بر محمل او چو نخل بستند</p></div>
<div class="m2"><p>از شاخ خزان ورق شکستند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یعنی که گلی بدین لطیفی</p></div>
<div class="m2"><p>شد رهزنش آفت خریفی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نگذشته هنوز نوبهارش</p></div>
<div class="m2"><p>در جان ز خزان خلید خارش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>او خفته به هودج عروسی</p></div>
<div class="m2"><p>مادر به رهش به خاکبوسی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>او رفته به دوش مهربانان</p></div>
<div class="m2"><p>مادر ز عقب سرشک رانان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>او رانده به وصل دوست محمل</p></div>
<div class="m2"><p>مادر ز فراق سنگ بر دل</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بردندش ازان قبیله بیرون</p></div>
<div class="m2"><p>یکسر به حظیره گاه مجنون</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خاکش به جوار دوست کندند</p></div>
<div class="m2"><p>در خاک چو گوهرش فکندند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پهلوی هم آن دو گوهر پاک</p></div>
<div class="m2"><p>خفتند فراز بستر خاک</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شد روضه آن دو کشته غم</p></div>
<div class="m2"><p>سر منزل عاشقان عالم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>باران کرم نثارشان باد</p></div>
<div class="m2"><p>سرسبز کن مزارشان باد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ایشان بستند رخت ازین حی</p></div>
<div class="m2"><p>ما نیز روانه ایم در پی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هر دم هوسی نشاید اینجا</p></div>
<div class="m2"><p>جاوید کسی نپاید اینجا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گردون که به عشوه جان ستانیست</p></div>
<div class="m2"><p>زه کرده به قصد ما کمانیست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>زان پیش کزین کمان کین توز</p></div>
<div class="m2"><p>بر سینه خوریم تیر دلدوز</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>آن به که به گوشه ای نشینیم</p></div>
<div class="m2"><p>زین مزرعه خوشه ای بچپینیم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>زان خوشه کنم توشه خویش</p></div>
<div class="m2"><p>گیریم ره نجات در پیش</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>از هستی خود نجات یابیم</p></div>
<div class="m2"><p>وز عمر ابد حیات یابیم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>عمری که درین حیات فانیست</p></div>
<div class="m2"><p>برقی ز سحاب زندگانیست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>در برق ورق گشاد نتوان</p></div>
<div class="m2"><p>بر نور وی اعتماد نتوان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نور ازل و ابد طلب کن</p></div>
<div class="m2"><p>آن را چو بیافتی طرب کن</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>آن نور نهفته در گل توست</p></div>
<div class="m2"><p>تابنده ز مشرق دل توست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دل را به خیال گل میارای</p></div>
<div class="m2"><p>وین روزنه را به گل میندای</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چون روزنه را به گل ببستی</p></div>
<div class="m2"><p>در ظلمت آب و گل نشستی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>شد نور تو زین حجاب مستور</p></div>
<div class="m2"><p>خود گو که چه بهره یابی از نور</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ای نور ازل در آرزویت</p></div>
<div class="m2"><p>از ظلمتیان بتاب رویت</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ظلمت که حجاب نور باشد</p></div>
<div class="m2"><p>آن به که ز دیده دور باشد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>خوش آنکه شوی ز پای تا فرق</p></div>
<div class="m2"><p>چون ذره در آفتاب خود غرق</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>هر چند نشان خویش جویی</p></div>
<div class="m2"><p>کم یابی اگر چه بیش جویی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>دلگرم شوی به آفتابی</p></div>
<div class="m2"><p>خود را همه آفتاب یابی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بی برگی تو شود همه برگ</p></div>
<div class="m2"><p>ایمن گردی ز آفت مرگ</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>جایی دل تو مقام گیرد</p></div>
<div class="m2"><p>کانجا جز مرگ کس نمیرد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>اینست حیات جاودانی</p></div>
<div class="m2"><p>رمزی گفتیم اگر بدانی</p></div></div>