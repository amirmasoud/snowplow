---
title: >-
    بخش ۳۴ - شنیدن خلیفه آوازه مجنون را در عشقبازی و شعرپردازی و طلب داشتن وی
---
# بخش ۳۴ - شنیدن خلیفه آوازه مجنون را در عشقبازی و شعرپردازی و طلب داشتن وی

<div class="b" id="bn1"><div class="m1"><p>دهقان شکوفه بند این شاخ</p></div>
<div class="m2"><p>استاد رقم نگار این کاخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این حرف نوشت بر کتابه</p></div>
<div class="m2"><p>کان خانه خراب این خرابه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شد به حدیث عشق مشهور</p></div>
<div class="m2"><p>وز مشهوران به عقل مهجور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آوازه نکته های چون در</p></div>
<div class="m2"><p>کرد انجمن زمانه را پر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگذاشت ز عقد آن ل آلی</p></div>
<div class="m2"><p>یک گوش به هیچ حلقه خالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان گوش خلیفه شد گهر بند</p></div>
<div class="m2"><p>چشمی به لقایش آرزومند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دادند خبر به والی نجد</p></div>
<div class="m2"><p>آن با خبر از حوالی نجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کان عاشق عامری نسب را</p></div>
<div class="m2"><p>مجنون لقب لبیب ادب را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشنیده ز هیچ کس بهانه</p></div>
<div class="m2"><p>سازد به دیار او روانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>والی به سران آن ولایت</p></div>
<div class="m2"><p>شد نکته گزار ازین حکایت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتند که او ز عقل دور است</p></div>
<div class="m2"><p>وز صحبت عاقلان نفور است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منزل نکند به هیچ جایی</p></div>
<div class="m2"><p>طعمه نخورد به جز گیایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گاهی که بود نشیمنش کوه</p></div>
<div class="m2"><p>صد کوه به سینه اش ز اندوه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همپنجه زور او پلنگ است</p></div>
<div class="m2"><p>ماء/وای شبش شکاف سنگ است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گاهی که به گرد دشت و وادی</p></div>
<div class="m2"><p>گردد به هزار نامرادی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با دام و دد است روز همگام</p></div>
<div class="m2"><p>با آهو و گور گشته شب رام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>درمانده به کار او خلایق</p></div>
<div class="m2"><p>دیدار خلیفه را چه لایق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرمود که چون خلیفه فرمان</p></div>
<div class="m2"><p>داده ست بدین غرض چه درمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کردند طلب به هر زمینش</p></div>
<div class="m2"><p>جستند نشان ز آن و اینش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر قله کوه یافتندش</p></div>
<div class="m2"><p>با فر و شکوه یافتندش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از موی به فرق چتر شاهی</p></div>
<div class="m2"><p>وز تن چو خلیفه در سیاهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گردش دد و دام حلقه بسته</p></div>
<div class="m2"><p>او خوش به میانشان نشسته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتند که خیز و رخت بربند</p></div>
<div class="m2"><p>فرمان خلیفه را کمر بند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفتا که ز رخت داشتم دست</p></div>
<div class="m2"><p>تا رخت به جز نبایدم بست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در کوه و کمر کمر فکندم</p></div>
<div class="m2"><p>تا بهر کسی کمر نبندم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از دود درون سیاه بختم</p></div>
<div class="m2"><p>بی رختی من بس است رختم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پشتم ز سپاه غم شکسته ست</p></div>
<div class="m2"><p>بر پشت چنین کمر که بسته ست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفتند بترس ازین دلیری</p></div>
<div class="m2"><p>مپسند در آنچه گفت دیری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفتا که طمع نکرده زیرم</p></div>
<div class="m2"><p>بر نارفتن ازان دلیرم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ناگشته طمع مهاربینی</p></div>
<div class="m2"><p>نتوان به خلیفه همنشینی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر خلق که کارها دراز است</p></div>
<div class="m2"><p>از شومی های حرص و آز است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عاشق که به ترک این دو خاص است</p></div>
<div class="m2"><p>از کشمکش جهان خلاص است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفتند مباد اگر ستیزد</p></div>
<div class="m2"><p>خونت نه به حجتی بریزد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفتا چو بریخت عشق خونم</p></div>
<div class="m2"><p>کی تیغ کسان کند زبونم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از خنجر تیز کی کشم سر</p></div>
<div class="m2"><p>بر کشته چه برگ گل چه خنجر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر زنده جفای زیر دستی</p></div>
<div class="m2"><p>باشد همه از برای هستی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هستی ز میان چو رخت بربست</p></div>
<div class="m2"><p>خنجر به تهی فتاد و بشکست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از وی به سخن چو باز ماندند</p></div>
<div class="m2"><p>ناقه به ره دگر براندند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اوبود پی بلاکشی کوه</p></div>
<div class="m2"><p>جا کرده به زیر تیغ اندوه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کردند دراز دست تدبیر</p></div>
<div class="m2"><p>بستند به پاش بند و زنجیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زانسان که زند به کوهساری</p></div>
<div class="m2"><p>بر شاخ گیاه حلقه ماری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>می خورد ز مار حلقه کرده</p></div>
<div class="m2"><p>صد زخم نهان به زیر پرده</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در پیچش مار مهره می سفت</p></div>
<div class="m2"><p>از گوهر اشک خویش و می گفت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>من بسته دام زلف یارم</p></div>
<div class="m2"><p>زنجیری جعد مشکبارم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زنجیر دگر به پای من چیست</p></div>
<div class="m2"><p>زنجیر بر بلای من کیست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زنجیر من ار برآرد آواز</p></div>
<div class="m2"><p>در مجلس عاشقان شود ساز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زنجیرکشان قید تدبیر</p></div>
<div class="m2"><p>زان زمزمه بگسلند زنجیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پایی که به یک دو گام کمتر</p></div>
<div class="m2"><p>بگذشت ز بند هفت کشور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نی نی که ز چار میخ ارکان</p></div>
<div class="m2"><p>وز ششدر تنگ این نه ایوان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هیهات که یک دو حلقه آهن</p></div>
<div class="m2"><p>لنگر شودش درین نشیمن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سیری که نه سوی یار پویند</p></div>
<div class="m2"><p>وز وی نه وصال یار جویند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گیرم که دهد به خلد راهی</p></div>
<div class="m2"><p>زان نیست عظیم تر گناهی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در مذهب آن که نکته دان است</p></div>
<div class="m2"><p>این بند گران جزای آنست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون یک دو سه هفته ناقه راندند</p></div>
<div class="m2"><p>نزدیک خلیفه اش رساندند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گرمیش به آب گرم بردند</p></div>
<div class="m2"><p>چرک از تن و مو ز سر ستردند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شد جود خلیفه مهر پرتو</p></div>
<div class="m2"><p>آراست تنش به خلعت نو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بر خوان کرامتش نشاندند</p></div>
<div class="m2"><p>عطر کرمش به سر فشاندند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مسکین چو به حال خود فرو دید</p></div>
<div class="m2"><p>خود را نه به شیوه نکو دید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دانست که شد درین دبستان</p></div>
<div class="m2"><p>سیلی خور دست خودپرستان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شد تنگ بر او فضای هستی</p></div>
<div class="m2"><p>دیوانگیش گرفت و مستی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بر خویش فرو درید جامه</p></div>
<div class="m2"><p>افکند به خاک ره عمامه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از گفت و شنید لب فرو بست</p></div>
<div class="m2"><p>در زاویه ای خموش بنشست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>فرمود خلیفه تا کثیر</p></div>
<div class="m2"><p>آن در ره اهل عقل خیر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>در مجلس خاص حاضر آمد</p></div>
<div class="m2"><p>دهشت بر آن مسافر آمد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گفتا که نخست در برابر</p></div>
<div class="m2"><p>آماده کنید کلک و دفتر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>زان کلک که شعر او نویسید</p></div>
<div class="m2"><p>سازید انگشت و شهد لیسید</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>برداشت بلند آنگه آواز</p></div>
<div class="m2"><p>کرد از دل خود نشیدی آغاز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>در وی صفت جمال لیلی</p></div>
<div class="m2"><p>بی بهرگی از وصال لیلی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بیماری قیس در فراقش</p></div>
<div class="m2"><p>خونخواری وی ز اشتیاقش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>زین گونه چو خواند چند بیتی</p></div>
<div class="m2"><p>زان یافت چراغ قیس زیتی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>کرد از رگ جان فتیله آن را</p></div>
<div class="m2"><p>بگشاد زبانه وش زبان را</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>برخواند ز سوز یک قصیده</p></div>
<div class="m2"><p>عقد عددش به صد رسیده</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هر بیت ازان چو خانه پر</p></div>
<div class="m2"><p>زاشک چو گهر سرشک چون در</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>مصرع مصرع ازان چو درها</p></div>
<div class="m2"><p>آمد شد درد را گذرها</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بودش به میان بیت ها چاک</p></div>
<div class="m2"><p>چاک افکن سینه های غمناک</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بحرش که ز موج برکند کوه</p></div>
<div class="m2"><p>گرد آمده سیل های اندوه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از قافیه هاش صد دل تنگ</p></div>
<div class="m2"><p>از تنگی خود به سینه زن سنگ</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هر حرف ز عشق داستانی</p></div>
<div class="m2"><p>هر نقطه ز خون دل نشانی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>خوناب جگر تراوش دل</p></div>
<div class="m2"><p>از چشمه حرفهاش سایل</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بر مطلعش اوفتاده تابی</p></div>
<div class="m2"><p>از روی چو لیلی آفتابی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>در مقطع او بریدن امید</p></div>
<div class="m2"><p>از طلعت آن خجسته خورشید</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>زو صاعقه ها به خرمن دل</p></div>
<div class="m2"><p>از یاد حبیب و ذکر منزل</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بگشاده زبان به شرح احوال</p></div>
<div class="m2"><p>زآثار خیام و رسم اطلال</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>از هر مژه سیل خون گشاده</p></div>
<div class="m2"><p>صد داغ به هر دلی نهاده</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>قاصد کرده ز مرغ یا باد</p></div>
<div class="m2"><p>بنوشته غم درون ناشاد</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>خاک قدمش به خون سرشته</p></div>
<div class="m2"><p>بنهاده به دستش آن نوشته</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بردن سوی دوست گر نیارد</p></div>
<div class="m2"><p>باری به سگان او سپارد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>زایام وصال در حکایت</p></div>
<div class="m2"><p>زآلام فراق در شکایت</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>گه جامه دری ز دست غماز</p></div>
<div class="m2"><p>گه نوحه گری ز بخت ناساز</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>هر کس که به آن نوا نهد گوش</p></div>
<div class="m2"><p>خون دلش از درون زند جوش</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>هر کس که بر آن رقم نهد چشم</p></div>
<div class="m2"><p>از گریه به سیل غم دهد چشم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چون قصه جان غصه پرورد</p></div>
<div class="m2"><p>زان ماتم غم به آخر آورد</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>از شعله آه آتش افروخت</p></div>
<div class="m2"><p>هر دل که نه سنگ ز آتشش سوخت</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>وز نوحه درد گریه برداشت</p></div>
<div class="m2"><p>یک چشم تهی ز گریه نگذاشت</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>رخساره چو سایه بر زمین سای</p></div>
<div class="m2"><p>افتاد ز پای بند بر پای</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چون دید خلیفه دردمندش</p></div>
<div class="m2"><p>فرمود که برکنند بندش</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>وانگه ز خزینه بند بگشاد</p></div>
<div class="m2"><p>صد بدره سیم و زر عطا داد</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>پس گفت که در دیار ما باش</p></div>
<div class="m2"><p>ساکن شده در جوار ما باش</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>در طی صحیفه عنایت</p></div>
<div class="m2"><p>خواهیم ز میر آن ولایت</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>کو همت خود به آن گمارد</p></div>
<div class="m2"><p>تا لیلی را پدر بیارد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>همسلک کنیم در و گوهر</p></div>
<div class="m2"><p>مقصود دلت شود میسر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>مجنون به وی التفات ننمود</p></div>
<div class="m2"><p>بر وعده وی ثبات ننمود</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>دامن ز عطای او بیفشاند</p></div>
<div class="m2"><p>در وادی عشق بارگی راند</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چون آهوی دام جسته می رفت</p></div>
<div class="m2"><p>وز جور زمانه رسته می رفت</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>می رفت و همی نشست و می خفت</p></div>
<div class="m2"><p>هر لحظه هزار شکر می گفت</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>کز درد سر خلیفه رستم</p></div>
<div class="m2"><p>و احرام دیار یار بستم</p></div></div>