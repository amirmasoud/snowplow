---
title: >-
    بخش ۱۶ - استفسار کردن اهل قبیله مجنون از حال وی و اطلاع یافتن بر محبت وی با لیلی
---
# بخش ۱۶ - استفسار کردن اهل قبیله مجنون از حال وی و اطلاع یافتن بر محبت وی با لیلی

<div class="b" id="bn1"><div class="m1"><p>بیاع متاع هوشیاری</p></div>
<div class="m2"><p>رقاص سماع بی قراری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ویرانه نشین کوه اندوه</p></div>
<div class="m2"><p>دیوانه سوار قله کوه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گنجور خزانه های افلاس</p></div>
<div class="m2"><p>رنجور فسانه های وسواس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسوده سایه مغیلان</p></div>
<div class="m2"><p>سرگشته وادی ذلیلان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمساز مغنیان فریاد</p></div>
<div class="m2"><p>همراز مجردان آزاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همگردن آهوان صحرا</p></div>
<div class="m2"><p>همشیون بلبلان شیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاراج رسیده شه عشق</p></div>
<div class="m2"><p>نعلین دریده ره عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سنگ افکن شیشه خانه عقل</p></div>
<div class="m2"><p>بر هم زن دام و دانه عقل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با گور و گوزن همطویله</p></div>
<div class="m2"><p>با دیو و پری ز یک قبیله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یعنی مجنون اسیر لیلی</p></div>
<div class="m2"><p>شوریده دار و گیر لیلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون از خود و قوم خود بگردید</p></div>
<div class="m2"><p>وز قاعده خرد بگردید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر بستر شب نیارمیدی</p></div>
<div class="m2"><p>چون روز شدی کسش ندیدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر رشته عهد پاره کردی</p></div>
<div class="m2"><p>وز همعهدان کناره کردی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر یاری را که دیدی از دور</p></div>
<div class="m2"><p>از یاری او رمیدی از دور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر خویشی را که آمدی پیش</p></div>
<div class="m2"><p>دور افکندی ز خویشی خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون قوم وی این صفت بدیدند</p></div>
<div class="m2"><p>در طعنه وی زبان کشیدند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کو را ز میان ما چه حال است</p></div>
<div class="m2"><p>کز قوم خودش چنین ملال است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تیغی نه به مرحمت کشیده ست</p></div>
<div class="m2"><p>وز ما صله رحم بریده ست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون هاله به گرد او نشستند</p></div>
<div class="m2"><p>پیرامن ماه حلقه بستند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دیدند دلیل و نبض جستند</p></div>
<div class="m2"><p>وز خامشیش زبان بشستند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نگشاد گره ز پرده راز</p></div>
<div class="m2"><p>وز پرده برون نداد آواز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یاری بودش در آن قبیله</p></div>
<div class="m2"><p>قایم به مساعی جمیله</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شیرین کاری سخن گزاری</p></div>
<div class="m2"><p>در پرده عشق رازداری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفتند بدو که قیس هر چند</p></div>
<div class="m2"><p>کرده ست چو نی ره نفس بند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در وی دردم دم وفایی</p></div>
<div class="m2"><p>باشد که برون دهد صدایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>افتاد پی تفحص حال</p></div>
<div class="m2"><p>روزی دو سه قیس را ز دنبال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وآخر گفتش که ای برادر</p></div>
<div class="m2"><p>دارم ز غمت دلی پر آذر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>داغ غم تو بسوخت جانم</p></div>
<div class="m2"><p>زد شعله ز مغز استخوانم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پیوند وفا بریدنت چیست</p></div>
<div class="m2"><p>وز صحبت من رمیدنت چیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زین پیش به هم حریف بودیم</p></div>
<div class="m2"><p>چون لام و الف الیف بودیم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>انصاف بده که آن کجا رفت</p></div>
<div class="m2"><p>آن قاعده چون شد و چرا رفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بنشین نفسی که راز گوییم</p></div>
<div class="m2"><p>احوال گذشته باز جوییم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یار ار نه به راز لب گشاید</p></div>
<div class="m2"><p>بوی یاری ز وی نیاید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در خلوت دوستان دمساز</p></div>
<div class="m2"><p>معماری دوستان کند راز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مجنون چو شنید این ترانه</p></div>
<div class="m2"><p>زد ناله و آه عاشقانه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفت ای دیرینه همدم من</p></div>
<div class="m2"><p>واندر هر راز محرم من</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کاریم به سر فتاده دشوار</p></div>
<div class="m2"><p>در ورطه مردنم ازان کار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کاری نه که بار رنج و اندوه</p></div>
<div class="m2"><p>صد بار فزون گران تر از کوه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این بار اگر نیفتد از پشت</p></div>
<div class="m2"><p>دانم به یقین که خواهدم کشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پرسید که آن کدام بار است</p></div>
<div class="m2"><p>وان بر دلت از کدام یار است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گفتا غم لیلی و بیفتاد</p></div>
<div class="m2"><p>از گفتن نام آن پریزاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هم چشم ز کار رفت و هم گوش</p></div>
<div class="m2"><p>هم لب ز حدیث گشته خاموش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دست از دو جهان فشاند تا دیر</p></div>
<div class="m2"><p>نی مرده نه زنده ماند تادیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آن یار چو دید حال او را</p></div>
<div class="m2"><p>در عشق و وفا کمال او را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دانست که کار و بار او چیست</p></div>
<div class="m2"><p>معشوقه کدام و یار او کیست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز آشفتگیش بسی بیاشفت</p></div>
<div class="m2"><p>وان راز نهان به دیگران گفت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مقصود وی آنکه آن غم و رنج</p></div>
<div class="m2"><p>گردد ز دواگران دواسنج</p></div></div>