---
title: >-
    بخش ۲۷ - ابا نمودن پدر لیلی از پیوند دادن وی با مجنون
---
# بخش ۲۷ - ابا نمودن پدر لیلی از پیوند دادن وی با مجنون

<div class="b" id="bn1"><div class="m1"><p>آن دور ز راه و رسم مردم</p></div>
<div class="m2"><p>ره کرده به رسم مردمی گم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن در تن او به جای دل سنگ</p></div>
<div class="m2"><p>از وی تا دل هزار فرسنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطموره نشین چاه غفلت</p></div>
<div class="m2"><p>طیاره سوار راه غفلت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تیرگی درون خود غرق</p></div>
<div class="m2"><p>در آب سیاه پای تا فرق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شارع دانش اوفتاده</p></div>
<div class="m2"><p>بر جهل جبلی ایستاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فارغ ز خیال عشقبازی</p></div>
<div class="m2"><p>آسوده ز حال جانگدازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی داغ محبتی کشیده</p></div>
<div class="m2"><p>نه جرعه محنتی چشیده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوری فکن دو همدم از هم</p></div>
<div class="m2"><p>طاقت شکن دو عاشق از غم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یعنی که کفیل کار لیلی</p></div>
<div class="m2"><p>بر هم زن روزگار لیلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چند کش از نسب پدر تو</p></div>
<div class="m2"><p>لیک از پدری رهش بدر بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رحم پدری نداشت بر وی</p></div>
<div class="m2"><p>صد محنت و غم گماشت بر وی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون خواهش آن قبیله بشنید</p></div>
<div class="m2"><p>از خواهششان عنان بپیچید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر ابروی ناگشاده چین زد</p></div>
<div class="m2"><p>صد عقده خشم بر جبین زد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن کس که به خنده دل خراشد</p></div>
<div class="m2"><p>ابرو چو گره زند چه باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت این چه خیال نادرست است</p></div>
<div class="m2"><p>چون خانه عنکبوت سست است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر این طلب از نخست بودی</p></div>
<div class="m2"><p>در کیش خرد درست بودی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>امروز که حیز زمانه</p></div>
<div class="m2"><p>پر شد ز صدای این ترانه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک گوش نماند در جهان باز</p></div>
<div class="m2"><p>خالی ز سماع این سرآواز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طفلان که به هم فسانه گویند</p></div>
<div class="m2"><p>این قصه به کنج خانه گویند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رندان که به نای و نوش کوشند</p></div>
<div class="m2"><p>پیمانه بدین خروش نوشند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ناصح که نهد اساس تعلیم</p></div>
<div class="m2"><p>از صورت حال ما کند بیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رسوایی ازین بتر چه باشد</p></div>
<div class="m2"><p>باد بتر این ز هر چه باشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حاشا که پذیرد این تلافی</p></div>
<div class="m2"><p>از پرده شعر حیله بافی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آتش که بود مفیض انوار</p></div>
<div class="m2"><p>بر کوه بلند در شب تار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پوشیدن آن به خس چه امکان</p></div>
<div class="m2"><p>ز اهل خرد این هوس چه امکان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شیشه که شود میان خاره</p></div>
<div class="m2"><p>ز افتادن سخت پاره پاره</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کی ز آب دهن درست گردد</p></div>
<div class="m2"><p>بر قاعده نخست گردد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خیزید و در طلب ببندید</p></div>
<div class="m2"><p>زین گفت و شنود لب ببندید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عاری که به گردن من آمد</p></div>
<div class="m2"><p>آلایش دامن من آمد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عاری دگرم به سر میارید</p></div>
<div class="m2"><p>من بعد مرا به من گذارید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر هرزه چرا کنم من این کار</p></div>
<div class="m2"><p>بیهوده چرا برم من این عار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن خس که به دیده خست خارم</p></div>
<div class="m2"><p>چون دیده خود بدو سپارم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زان کس که به دل نشاند تیرم</p></div>
<div class="m2"><p>چون دعوی دل دهی پذیرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>با آنکه زند خدنگ کاری</p></div>
<div class="m2"><p>مشت است و درفش کارزاری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من مشتکیم کنون ز یک مشت</p></div>
<div class="m2"><p>زان در ندهم به بار او پشت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در مذهب رهرو سبکبار</p></div>
<div class="m2"><p>باری نبود گرانتر از عار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در بار گران میفکنیدم</p></div>
<div class="m2"><p>وین پشت خمیده مشکنیدم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون عامریان نشسته خاموش</p></div>
<div class="m2"><p>برگشت ازین محالشان گوش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مهر از لب بسته برگرفتند</p></div>
<div class="m2"><p>آیین سخن ز سر گرفتند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گفتند حدیث عار تا چند</p></div>
<div class="m2"><p>زین بیهده افتخار تا چند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قیس هنری به جز هنر نیست</p></div>
<div class="m2"><p>وز دایره هنر بدر نیست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عشقی که زده ست سر ز جیبش</p></div>
<div class="m2"><p>هان تا نکنی دلیل عیبش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خود عشق چه جای قال و قیل است</p></div>
<div class="m2"><p>بر پاکی باطنش دلیل است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا دل نه ز میل طبع پاک است</p></div>
<div class="m2"><p>کی ز آتش عشق سوزناک است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در پاکی طبع نیست عاری</p></div>
<div class="m2"><p>بر چهره فخر ازان غباری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گفتی لیلی ازین فسانه</p></div>
<div class="m2"><p>رسوا گشته ست در زمانه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>رسوایی او بگو کدام است</p></div>
<div class="m2"><p>کز عاشقیش بلند نام است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گویا و گواست وجد و حالش</p></div>
<div class="m2"><p>بر دعوی عفت و جمالش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>معشوقه اگر جمیل نبود</p></div>
<div class="m2"><p>عاشق به رهش ذلیل نبود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ور هست جمیل و نیستش جیب</p></div>
<div class="m2"><p>پاکیزه ز وصله دوزی عیب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زود آتش عشق او بمیرد</p></div>
<div class="m2"><p>معشوقی او زوال گیرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آنجا که مقام افتخار است</p></div>
<div class="m2"><p>زین هر دو صفت بگو چه عار است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هر چند که قیس گفت و گو کرد</p></div>
<div class="m2"><p>دلالگی جمال او کرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دلاله اگر هزار باشد</p></div>
<div class="m2"><p>زینسان نه سخن گزار باشد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دلالگی جمال دلدار</p></div>
<div class="m2"><p>نی عیب در او و نه عار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آن کجرو کج نهاد کج دل</p></div>
<div class="m2"><p>در دایره کجیش منزل</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو این سخنان راست بشنید</p></div>
<div class="m2"><p>چون بی خبران ز راست رنجید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شد راه جواب آن بر او بند</p></div>
<div class="m2"><p>بگشاد زبان روان به سوگند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گفتا به خدایی خدایی</p></div>
<div class="m2"><p>کز وی نه تهیست هیچ جایی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>او بی جای و جهان ازو پر</p></div>
<div class="m2"><p>تنها نه جهان که جان ازو پر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هر ذره اگر چه زو تهی نیست</p></div>
<div class="m2"><p>یک ذره ازوش آگهی نیست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دیگر به پیمبران مرسل</p></div>
<div class="m2"><p>ثابت قدمان صف اول</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>دانشورزان دانش آموز</p></div>
<div class="m2"><p>بینش داران بینش افروز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>پرواز ده شکسته بالان</p></div>
<div class="m2"><p>نیرو شکن خطا سگالان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دیگر به سران کعبه مسکن</p></div>
<div class="m2"><p>از جعبه کعبه ناوک افکن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هر ناوک و صد هزار نخجیر</p></div>
<div class="m2"><p>بیرون ز شکارگاه تدبیر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>از صید حرم همه غذاخوار</p></div>
<div class="m2"><p>کوته زیشان زبان انکار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کز لیلی اگر درین تک و پوی</p></div>
<div class="m2"><p>خواهید برای قیس یک موی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>وان را دو جهان بها بیارید</p></div>
<div class="m2"><p>زان کار به جز قفا نخارید</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>یک موی وی و هزار مجنون</p></div>
<div class="m2"><p>گو دست ز وی بدار مجنون</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>مجنون که بود که داد خواهد</p></div>
<div class="m2"><p>وز لیلی من مراد خواهد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>جان دادن او بس است دادش</p></div>
<div class="m2"><p>مردن ز فراق او مرادش</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>با من دگر این سخن مگویید</p></div>
<div class="m2"><p>کام دل خویشتن مجویید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>آنان چو جواب او شنیدند</p></div>
<div class="m2"><p>وآزار عتاب او کشیدند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نومید به خانه بازگشتند</p></div>
<div class="m2"><p>با قیس حریف راز گشتند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هر قصه که گفته بود گفتند</p></div>
<div class="m2"><p>هر گل که شگفته بود گفتند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>امید وصال یار ازو رفت</p></div>
<div class="m2"><p>وآرام دل و قرار ازو رفت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>از گریه به خون و خاک می خفت</p></div>
<div class="m2"><p>وز سینه دردناک می گفت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>لیلی جان است و من تن او</p></div>
<div class="m2"><p>یارب به روان روشن او</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>کان کس که مرا ازو جدا ساخت</p></div>
<div class="m2"><p>کاری به مراد من نپرداخت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>در هر نفسیش باد مرگی</p></div>
<div class="m2"><p>وز زندگیش مباد برگی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>وان کس که دلم فگار کرده ست</p></div>
<div class="m2"><p>دورم ز دیار و یار کرده ست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>جانش چو دلم فگار بادا</p></div>
<div class="m2"><p>و آواره به هر دیار بادا</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>وان کس که ز خصلت پلنگی</p></div>
<div class="m2"><p>زد سنگ فراقم از دو رنگی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>پا میخ شکاف سنگ بادش</p></div>
<div class="m2"><p>سر در دهن نهنگ بادش</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>زو بر دل من چو دور خاتم</p></div>
<div class="m2"><p>شد تنگ فراخنای عالم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>واو کنده به تنگنای این دور</p></div>
<div class="m2"><p>رویم چو نگین به ناخن جور</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بادش ناخن جدا ز انگشت</p></div>
<div class="m2"><p>دستش کوته ز خارش پشت</p></div></div>