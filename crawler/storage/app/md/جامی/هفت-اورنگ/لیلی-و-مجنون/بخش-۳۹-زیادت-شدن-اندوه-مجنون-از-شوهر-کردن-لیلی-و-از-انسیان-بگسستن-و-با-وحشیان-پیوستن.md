---
title: >-
    بخش ۳۹ - زیادت شدن اندوه مجنون از شوهر کردن لیلی و از انسیان بگسستن و با وحشیان پیوستن
---
# بخش ۳۹ - زیادت شدن اندوه مجنون از شوهر کردن لیلی و از انسیان بگسستن و با وحشیان پیوستن

<div class="b" id="bn1"><div class="m1"><p>آن عاشق از خرد رمیده</p></div>
<div class="m2"><p>زاندیشه نیک و بد رهیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مستی عشق بود مجنون</p></div>
<div class="m2"><p>دادش به میان مستی افیون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغی ز فراق یار بودش</p></div>
<div class="m2"><p>یک داغ دگر بر آن فزودش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیکن داغی فزون ز هر داغ</p></div>
<div class="m2"><p>آشفت ز عشق داغ بر داغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واکرد ز انس ناکسان خوی</p></div>
<div class="m2"><p>وآورد به سوی وحشیان روی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کین کسان چو شست سینه</p></div>
<div class="m2"><p>با او دگری نجست کینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با وی همه وحش رام گشتند</p></div>
<div class="m2"><p>در انس به وی تمام گشتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می رفت به کوه و دشت چون شاه</p></div>
<div class="m2"><p>با او سپه وحوش همراه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنهاده به پای هر درختی</p></div>
<div class="m2"><p>بودش از ریگ و سنگ تختی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بر سر تخت خود نشستی</p></div>
<div class="m2"><p>گردش دد و دام حلقه بستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پرتو عدل شه بر ایشان</p></div>
<div class="m2"><p>بودند به هم ز صلح کیشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آهو از گرگ رم نکردی</p></div>
<div class="m2"><p>نخجیر ز شیر غم نخوردی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نخجیر به ره ز لعب سازی</p></div>
<div class="m2"><p>کردی به دم پلنگ بازی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رفتندی چون شدی ره اندیش</p></div>
<div class="m2"><p>گوران چو جنیبتش پس و پیش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بودی چو قدم زدی به هر راه</p></div>
<div class="m2"><p>جاروب کشیش کار روباه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا بنشاندی ز ره تف و تاب</p></div>
<div class="m2"><p>از اشک خودش زدی گوزن آب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بالای سرش ز چتر داری</p></div>
<div class="m2"><p>زاغان سیه به حق گزاری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور زانکه شدی گهیش میلی</p></div>
<div class="m2"><p>تا نامه کند به سوی لیلی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آهو قلمش ز ساق دادی</p></div>
<div class="m2"><p>وز جلد سرین ورق گشادی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بردیش به رسم نیکخواهی</p></div>
<div class="m2"><p>از چشم سیاه خود سیاهی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>می رفت چنین نشید خوانان</p></div>
<div class="m2"><p>از دیده سرشک لعل رانان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وآهو بچگان به خیر و خوبی</p></div>
<div class="m2"><p>پیش قدمش به پایکوبی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ناگاه به روضه ای رسیدند</p></div>
<div class="m2"><p>وز دور جماعتی بدیدند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از سبزه به زیر پا بساطی</p></div>
<div class="m2"><p>چون لاله ز جام می نشاطی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مجنون از دور ره بگرداند</p></div>
<div class="m2"><p>زیشان خطر سپه بگرداند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زان قوم یکی شناخت او را</p></div>
<div class="m2"><p>وز ساز ثنا نواخت او را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کای سرور عاشقان شیدا</p></div>
<div class="m2"><p>در روی تو نور عشق پیدا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وی خانه خراب این خرابات</p></div>
<div class="m2"><p>رسته ز قبیله و قرابات</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وی راهسپر به پای تجرید</p></div>
<div class="m2"><p>تنها رو تنگنای تفرید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وی فرق دو نیم تیغ اندوه</p></div>
<div class="m2"><p>بنشسته به زیر تیغ چون کوه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سوگند به آنکه مست اویی</p></div>
<div class="m2"><p>نی پا و نه سر ز دست اویی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سوگند به آنکه زندگانی</p></div>
<div class="m2"><p>جز دولت وصل او ندانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سوگند به لعل آبدارش</p></div>
<div class="m2"><p>سوگند به جعد تابدارش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سوگند به آهوان مستش</p></div>
<div class="m2"><p>جادومنشان می پرستش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سوگند به آن دو ابر مه پوش</p></div>
<div class="m2"><p>کش جای گرفته بر بناگوش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کز ما مگذر بدین روانی</p></div>
<div class="m2"><p>بر ما مشکن ز دل گرانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دیریست که ما شکسته ای چند</p></div>
<div class="m2"><p>هستیم به وصلت آرزومند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا گردان است دور عالم</p></div>
<div class="m2"><p>امروز رسیده ایم با هم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نبود پس ازین بریدن ما</p></div>
<div class="m2"><p>معلوم به هم رسیدن ما</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پیش آ که به هم دمی برآریم</p></div>
<div class="m2"><p>با یکدیگر غمی گذاریم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مجنون چو نیازمندیش دید</p></div>
<div class="m2"><p>وآیین رضا پسندیش دید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بگذاشت به جای خود سپه را</p></div>
<div class="m2"><p>بر مجلسیان فکنده ره را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پرسید که این چه سرزمین است</p></div>
<div class="m2"><p>کش خاک به نرخ مشک چین است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گفتند نواحی حجاز است</p></div>
<div class="m2"><p>رحلتگه هر که پاکباز است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>لیلی صد بار محمل اینجا</p></div>
<div class="m2"><p>رانده ست گرفته منزل اینجا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>با همقدمان خود درین جای</p></div>
<div class="m2"><p>مشکین دامان کشیده در پای</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>این خاک که همچو مشک خوشبوست</p></div>
<div class="m2"><p>از مشک افشانی دامن اوست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مجنون چو شنید این سخن را</p></div>
<div class="m2"><p>بر جای ندید خویشتن را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خود را به زمین چو سایه انداخت</p></div>
<div class="m2"><p>بانگی زد و این نشید پرداخت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کای همنفسان کزین دیارید</p></div>
<div class="m2"><p>وز دلبر من سخن گزارید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جان من و دل فدایتان باد</p></div>
<div class="m2"><p>سر خاک به زیر پایتان باد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اینجا نه هوای کعبه دارم</p></div>
<div class="m2"><p>نی نیت آنکه حج گزارم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مقصوم ازین طواف لیلی ست</p></div>
<div class="m2"><p>باقی همه پیش او طفیلی ست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نتوان چو به کوی او گذشتن</p></div>
<div class="m2"><p>سودی نکند به کعبه گشتن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>حج همه عمره دیدن اوست</p></div>
<div class="m2"><p>بی او حج و عمره ام نه نیکوست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تیر وصلش برون ز جعبه</p></div>
<div class="m2"><p>سرگردانیست طوف کعبه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>من تشنه او به وادی غم</p></div>
<div class="m2"><p>کی آب خورم ز چاه زمزم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>با زمزمه غم ویم شاد</p></div>
<div class="m2"><p>ناید ز زلال زمزمم یاد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>آن زمزمه بر زبان چو رانم</p></div>
<div class="m2"><p>از هر مژه زمزمی فشانم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در هر منزل که می زنم گام</p></div>
<div class="m2"><p>زان گام وصال او بود کام</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هر جا که نه روی او چراغ است</p></div>
<div class="m2"><p>گر باغ ارم بود که داغ است</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>لیلی ست ز هر سفر مرادم</p></div>
<div class="m2"><p>نی طالب سلمی و سعادم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تا با غم او شدم هم آغوش</p></div>
<div class="m2"><p>کردم ز دگر بتان فراموش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دانای منازل و مراحل</p></div>
<div class="m2"><p>زین وادی جانگداز هایل</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گاهی که شود فسانه پرداز</p></div>
<div class="m2"><p>از پرده چنین برون دهد راز</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کان طاق ز لطف و با ستم جفت</p></div>
<div class="m2"><p>از لیلی و جفت چون سخن گفت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>جوری که رود ز دوست بر من</p></div>
<div class="m2"><p>آن را مکشاد هیچ دشمن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>انداخت مرا به خردسالی</p></div>
<div class="m2"><p>در پنجه عشق لاابالی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بگذشت ز زور پنجه عشق</p></div>
<div class="m2"><p>عمرم همه در شکنجه عشق</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>امروز که نوبت وصال است</p></div>
<div class="m2"><p>جانم ز فراق در وبال است</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آن سکه به نام دیگری شد</p></div>
<div class="m2"><p>وان لقمه به کام دیگری شد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>او همدم یار و من چنین دور</p></div>
<div class="m2"><p>او واصل و من غریب و مهجور</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>این گفت و جبین به خاک مالید</p></div>
<div class="m2"><p>وز سینه چاک چاک نالید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>خوناب جگر ز دیده بگشاد</p></div>
<div class="m2"><p>چندانکه ز گریه بی خود افتاد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>شب را که ز بی خودی درآمد</p></div>
<div class="m2"><p>گردون به لباس دیگر آمد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>شد یکرنگی او دورنگی</p></div>
<div class="m2"><p>با حیله شیریش پلنگی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از حلقه همدمان برون جست</p></div>
<div class="m2"><p>با گور و گوزن خویش پیوست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>جان بی جانان رسیده بر لب</p></div>
<div class="m2"><p>شب برد به سر چنانکه هر شب</p></div></div>