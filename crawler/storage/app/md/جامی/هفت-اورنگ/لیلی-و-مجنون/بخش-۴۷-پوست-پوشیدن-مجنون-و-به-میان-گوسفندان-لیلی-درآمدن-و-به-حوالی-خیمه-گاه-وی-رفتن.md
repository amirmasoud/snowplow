---
title: >-
    بخش ۴۷ - پوست پوشیدن مجنون و به میان گوسفندان لیلی درآمدن و به حوالی خیمه گاه وی رفتن
---
# بخش ۴۷ - پوست پوشیدن مجنون و به میان گوسفندان لیلی درآمدن و به حوالی خیمه گاه وی رفتن

<div class="b" id="bn1"><div class="m1"><p>آن پوست و مغز قصه اش نغز</p></div>
<div class="m2"><p>از پوست چنین برون دهد مغز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان پوست شناس مغز دیده</p></div>
<div class="m2"><p>از پوست به مغز آن رسیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شد به دیار یار نزدیک</p></div>
<div class="m2"><p>شد کار بر او چو موی باریک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی رخصت پیش یار رفتن</p></div>
<div class="m2"><p>نی صبر ازان دیار رفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قرب دیار شوق افزود</p></div>
<div class="m2"><p>وز وصل هزار مانعش بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرگشته در آن دیار می گشت</p></div>
<div class="m2"><p>وآشفته و بی قرار می گشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کس که در آن دیار دیدی</p></div>
<div class="m2"><p>یا در راهی به او رسیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زو چاره کار خویش جستی</p></div>
<div class="m2"><p>درمان درون ریش جستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزی می گشت گرد آن دشت</p></div>
<div class="m2"><p>ناگه رمه ای ز دور بگذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد گرد رمه عبیر جیبش</p></div>
<div class="m2"><p>کامد ز عبیردان غیبش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نور شبان چو لمعه نور</p></div>
<div class="m2"><p>می تافت فروغ لیلی از دور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زانه لمعه چو یافت روشنایی</p></div>
<div class="m2"><p>افروخت چراغ آشنایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت ای ز تو در سیه گلیمی</p></div>
<div class="m2"><p>روشن شده آتش کلیمی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کوه ز مقدم تو طوری</p></div>
<div class="m2"><p>در طور ز آتش تو نوری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای وادی ایمن از تو این خاک</p></div>
<div class="m2"><p>ترسان ز عصات نیل افلاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر جا که ز کف بیفکنی چوب</p></div>
<div class="m2"><p>بر معرکه ددان فتد کوب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر چند به صورت آن عصاییست</p></div>
<div class="m2"><p>در دیده خصم اژدهاییست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بربوده به دشت از دد و دام</p></div>
<div class="m2"><p>آواز فلاخن تو آرام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر گه سنگی به زور بازو</p></div>
<div class="m2"><p>در کفه آن کنی ترازو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرگ از رمه ات ز بیم آن سنگ</p></div>
<div class="m2"><p>افتان خیزان جهد به فرسنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ور زانکه شوی ازان فلاخن</p></div>
<div class="m2"><p>بر برج فلک عروسک افکن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>افتاده ز ترس لرزه بر شیر</p></div>
<div class="m2"><p>خود را زان برج افکند زیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای کاسه تو کشیده خوانی</p></div>
<div class="m2"><p>پرورده ز شیر خود جهانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر صبح ز خوانش این کهن پیر</p></div>
<div class="m2"><p>بزغاله و بره را دهد شیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با تشنه لبی منم اسیری</p></div>
<div class="m2"><p>زین خوان کرم نخورده شیری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با تشنه لبان چو چرخ مستیز</p></div>
<div class="m2"><p>یک جرعه شیر بر لبم ریز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شیری نه که تن بپروراند</p></div>
<div class="m2"><p>شیری که غذا به جان رساند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یعنی که ز لطف و مهربانی</p></div>
<div class="m2"><p>رحمی بنما چنانکه دانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگشای به کوی لیلی ام در</p></div>
<div class="m2"><p>دزدیده به سوی لیلی ام بر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا بو که به گوشه ای نشینم</p></div>
<div class="m2"><p>پوشیده جمال او ببینم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از تو به قلاده سگم خوش</p></div>
<div class="m2"><p>چون سگ به قلاده خودم کش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>باشد که طفیلی سگانش</p></div>
<div class="m2"><p>سایم سر خود بر آستانش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یا کن ز سر وفا پسندی</p></div>
<div class="m2"><p>خاصم به لباس گوسفندی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آمد تن من گسسته جانی</p></div>
<div class="m2"><p>بی پوست و گوشت استخوانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زین گله که جان فدای آنم</p></div>
<div class="m2"><p>یک پوست بکش در استخوانم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شاید به حریم ارجمندان</p></div>
<div class="m2"><p>گنجم به طفیل گوسفندان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون گله به آن حرم درآید</p></div>
<div class="m2"><p>لیلی سوی آن نظر گشاید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>من نیز به آن نظر درآیم</p></div>
<div class="m2"><p>پنهان سوی او نظر گشایم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رویی بینم که در فراقش</p></div>
<div class="m2"><p>دل سوخته ام ز اشتیاقش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>این گفت و چو سایه بی خود افتاد</p></div>
<div class="m2"><p>چون مرده به خاک مرقد افتاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا ماهی و ماه کرد ازو راه</p></div>
<div class="m2"><p>از دیده سرشک وز جگر آه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بالای سرش شبان نشسته</p></div>
<div class="m2"><p>چشمی گریان دلی شکسته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زان بیهوشی چو با خود آمد</p></div>
<div class="m2"><p>واندوه شده یکی صد آمد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بگشاد شبان لب ترحم</p></div>
<div class="m2"><p>گفت ای شده در هوای دل گم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خوش باش که وقت دلنوازیست</p></div>
<div class="m2"><p>وامشب شب وصل و کار سازیست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آورد به سوی او یکی پوست</p></div>
<div class="m2"><p>کین پرده توست تا در دوست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>این را در پوش و شاد و خندان</p></div>
<div class="m2"><p>می رقص میان گوسنفدان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شاید کامروز همچو هر روز</p></div>
<div class="m2"><p>گرد رمه گردد آن دل افروز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>حال تو در آن میان نداند</p></div>
<div class="m2"><p>وز کف به تو راحتی رساند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مسکین مجنون چو پوست را دید</p></div>
<div class="m2"><p>سوی رمه میل دوست بشنید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>برخاست فکنده پوست در بر</p></div>
<div class="m2"><p>برساخت ز دست پای دیگر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پیوسته دلی اسیر غم داشت</p></div>
<div class="m2"><p>کاندر ره عشق پای کم داشت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>با آن پایی که داشت پیوست</p></div>
<div class="m2"><p>هر پای دگر کش آمد از دست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>با آن رمه خم ز بار غم پشت</p></div>
<div class="m2"><p>هم پای همی دوید هم پشت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>می زد به امید دست و پایی</p></div>
<div class="m2"><p>تا بو که ازان رسد به جایی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>می گفت به زیر لب که یارب</p></div>
<div class="m2"><p>این خلعت نورسیده کامشب</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از نرمی دولتم به پشت است</p></div>
<div class="m2"><p>با آن سنجاب بس درشت است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گر قصه آن رسد به قاقم</p></div>
<div class="m2"><p>در خود کشد از خجالتش دم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>با نرمی آن ز مو درشتی</p></div>
<div class="m2"><p>اقرار کند به خارپشتی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زین پوست شدم چو نافه مشکین</p></div>
<div class="m2"><p>اینجا چه سگ است آهوی چین</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ان نیست سزا به قد هر کس</p></div>
<div class="m2"><p>تا جان دارم لباسم این بس</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از شادی این لباس بر تن</p></div>
<div class="m2"><p>صد پوست نشست گوشت بر من</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>زین پوست شدم سعادت اندوز</p></div>
<div class="m2"><p>در پوست همی نگنجم امروز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>با خود بود اندرین فسانه</p></div>
<div class="m2"><p>کاورد ره آن شبان به خانه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>لیلی آمد ز خانه بیرون</p></div>
<div class="m2"><p>چون چارده مه ز دور گردون</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گردن ز حلی بلند آواز</p></div>
<div class="m2"><p>ساق از خلخال نغمه پرداز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>پر کرده ز زلف پر خم و تاب</p></div>
<div class="m2"><p>دامان جهان ز عنبر ناب</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کرد از رمه جا به یک کناره</p></div>
<div class="m2"><p>بگشاد نظر پی نظاره</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>هر زنده به نوبت از بز و میش</p></div>
<div class="m2"><p>زان گله همی گذشتش از پیش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>نوبت چو به آن رمیده افتاده</p></div>
<div class="m2"><p>از پوست به دوست دیده بگشاد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نی صبر بماند نه قرارش</p></div>
<div class="m2"><p>وز دست برفت اختیارش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بانگی زد و بی خبر بیفتاد</p></div>
<div class="m2"><p>چون سایه به رهگذر بیفتاد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>لیلی چو شنید بانگ بشناخت</p></div>
<div class="m2"><p>کان کیست نظر به سویش انداخت</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>افتاده چه دید پوستی خشک</p></div>
<div class="m2"><p>پر خون جگرش چو نافه مشک</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هم عقل ز دست داده هم هوش</p></div>
<div class="m2"><p>هم چشم ز کار مانده هم گوش</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بالین ز کنار خویش کردش</p></div>
<div class="m2"><p>وز چهره به گریه شست گردش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از خوی به گلاب عطر پرورد</p></div>
<div class="m2"><p>زان بیهوشی به هوشش آورد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>آمد چو به هوش و دیده بگشاد</p></div>
<div class="m2"><p>پیش رخ او به سجده افتاد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>کای مردم چشم چشم بازان</p></div>
<div class="m2"><p>وی قبله ناز پرنیازان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ای گلبن باغ سربلندی</p></div>
<div class="m2"><p>وی نور چراغ ارجمندی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ای عرش برین تو و زمین من</p></div>
<div class="m2"><p>هیهات که آن تو باشی این من</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>باور نکنم من فتاده</p></div>
<div class="m2"><p>کین بر سر من تویی ستاده</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>سر برده بر اوج لامکان عرش</p></div>
<div class="m2"><p>خاشاک زمین کیش سزد فرش</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دامان تو در کفم محال است</p></div>
<div class="m2"><p>گر نغلطم امشب این خیال است</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>مستان که به شب خیال بینند</p></div>
<div class="m2"><p>در خواب دو صد محال بینند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>آنجا که ز طالعم دلیل است</p></div>
<div class="m2"><p>این واقعه هم ازان قبیل است</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>خوابی که در او رخ تو بینم</p></div>
<div class="m2"><p>با تو به فراغ دل نشینم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بیداری دولت من است آن</p></div>
<div class="m2"><p>بینایی چشم روشن است آن</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>لیلی چو نیازمندیش دید</p></div>
<div class="m2"><p>وان نکته دلنواز بشنید</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>گفت ای شده میهمانم امشب</p></div>
<div class="m2"><p>آسوده به توست جانم امشب</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>این پوست بود ز دوست مانع</p></div>
<div class="m2"><p>از دوست شو به پوست قانع</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>از گردن خود بیفکن این پوست</p></div>
<div class="m2"><p>بی پوست نشین چو مغز با دوست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>تا چند سخن ز پرده گوییم</p></div>
<div class="m2"><p>رازی دو سه پوست کرده گوییم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>شب روشن بود و ماه تابان</p></div>
<div class="m2"><p>محنت به ره عدم شتابان</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>تا صبح به یکدگر نشستند</p></div>
<div class="m2"><p>یک لحظه لب از سخن نبستند</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>صد قصه به آه و ناله گفتند</p></div>
<div class="m2"><p>درد دل چند ساله گفتند</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>صد نکته هنوز بود باقی</p></div>
<div class="m2"><p>زد مرغ ترانه فراقی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>صبح از دم گرگ رایت افراشت</p></div>
<div class="m2"><p>سگ خفت و خروس نعره برداشت</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چون نعره او سماع کردند</p></div>
<div class="m2"><p>یکدیگر را وداع کردند</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>آن جانب خیمه قد ستون کرد</p></div>
<div class="m2"><p>وین دشت ز گریه لاله گون کرد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>این است بلی سپهر را کار</p></div>
<div class="m2"><p>کز بعد هزار رنج و تیمار</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>گر خسته دلی جگر فگاری</p></div>
<div class="m2"><p>یابد ره وصل پیش یاری</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ناکرده نگاه در رخش تیز</p></div>
<div class="m2"><p>دستش گیرد که زود برخیز</p></div></div>