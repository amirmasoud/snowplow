---
title: >-
    بخش ۳۶ - رسیدن مجنون در قافله لیلی به کعبه و در مناسک حج با وی عشق باختن
---
# بخش ۳۶ - رسیدن مجنون در قافله لیلی به کعبه و در مناسک حج با وی عشق باختن

<div class="b" id="bn1"><div class="m1"><p>لیلی چو به عزم خانه برخاست</p></div>
<div class="m2"><p>خانه به جمال خود بیاراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمش سوی آن رمیده افتاد</p></div>
<div class="m2"><p>خون جگرش ز دیده افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگریست که ای فراق دیده</p></div>
<div class="m2"><p>درد و غم اشتیاق دیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کشمکش فراق چونی</p></div>
<div class="m2"><p>در آتش اشتیاق چونی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بی تو چه دم زنم که چونم</p></div>
<div class="m2"><p>اینک ز دو دیده غرق خونم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزان و شبان در آرزویت</p></div>
<div class="m2"><p>تنها منم و خیال رویت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز مردم دیده کس ندارم</p></div>
<div class="m2"><p>کز دل با او دمی برآرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشحال تو در غمم که باری</p></div>
<div class="m2"><p>گفتن دانی به غمگزاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجنون به زبان بی زبانی</p></div>
<div class="m2"><p>هم زین سخنان چنانکه دانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می گفت و ز بیم ناکس و کس</p></div>
<div class="m2"><p>چشمی از پیش و چشمی از پس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غم بی حد و فرصتی چنین تنگ</p></div>
<div class="m2"><p>کردند به طوف خانه آهنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیلی به طواف خانه در گرد</p></div>
<div class="m2"><p>مجنون ز قفاش سینه پر درد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن سنگ سیاه بوسه می داد</p></div>
<div class="m2"><p>وین دل به خیال خام او شاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن برد دهان به آب زمزم</p></div>
<div class="m2"><p>وین کرد ز گریه دیده پر نم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن روی به مروه و صفا داشت</p></div>
<div class="m2"><p>وین جای به ذروه وفا داشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن در عرفات گشته واقف</p></div>
<div class="m2"><p>وین واقف او در آن مواقف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن روی به مشعر حرامش</p></div>
<div class="m2"><p>وین در غم شعر مشکفامش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن تیغ به دست در منا تیز</p></div>
<div class="m2"><p>وین بانگ زده که خون من ریز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن کرده به رمی سنگ آهنگ</p></div>
<div class="m2"><p>وین داشته سر به پیش آن سنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن کرده وداع خانه بنیاد</p></div>
<div class="m2"><p>وین کرده ز بیم هجر فریاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لیلی چو ازان وداع پرداخت</p></div>
<div class="m2"><p>مسند به درون محمل انداخت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مجنون به میانه فرصتی جست</p></div>
<div class="m2"><p>جا کرد به پیش محملش چست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر دو به وداع هم ستادند</p></div>
<div class="m2"><p>وز درد ز دیده خون گشادند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بی گفت زبان ز چشم پر خون</p></div>
<div class="m2"><p>دادند ز سینه درد بیرون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کردند وداع یکدگر را</p></div>
<div class="m2"><p>چون تن که کند وداع سر را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یک لحظه که سر رفیق تن نیست</p></div>
<div class="m2"><p>تن را امکان زیستن نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن راند به سوز و درد محمل</p></div>
<div class="m2"><p>وین ماند ز گریه پای در گل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زان شد محمل چو نافه پر مشک</p></div>
<div class="m2"><p>وین را در تن چو نافه خون خشک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون نافه ز راز پرده بگشاد</p></div>
<div class="m2"><p>وین شمه ز حال خود برون داد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کافسوس که تن بماند و جان رفت</p></div>
<div class="m2"><p>از دل صبر و ز تن توان رفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بنمود جمال خود پس از دیر</p></div>
<div class="m2"><p>زان می سوزم که زود شد سیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عمری ز قفای او دویدم</p></div>
<div class="m2"><p>تا روی وی از نقاب دیدم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ناگشته هنوز چشم من گرم</p></div>
<div class="m2"><p>پوشید و نداشت از خدا شرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن تشنه لبم که در بیابان</p></div>
<div class="m2"><p>هر سو شدم آبجو شتابان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون پی بردم به چشمه آب</p></div>
<div class="m2"><p>صبر از دل من چو آب نایاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ننشسته هنوز آتش تیز</p></div>
<div class="m2"><p>زد دشنه عرابیم که برخیز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از من تا مگر ره بسی نیست</p></div>
<div class="m2"><p>امروز به روز من کسی نیست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دل پر درد است و سینه پر سوز</p></div>
<div class="m2"><p>یا رب که مباد کس بدین روز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خوش آن کین روز هم نماند</p></div>
<div class="m2"><p>تیغ اجلم ز غم رهاند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>این گفت و جدا ز آل لیلی</p></div>
<div class="m2"><p>با همرهی خیال لیلی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>با جمعی دگر به راه زد گام</p></div>
<div class="m2"><p>نی تاب و توان نه صبر و آرام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ترسید کزان گروه بی باک</p></div>
<div class="m2"><p>در همرهیش به جان فتد چاک</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زان لیلی را رسد ملالی</p></div>
<div class="m2"><p>و او را ز ملالش انفعالی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آن کعبه روی حجازی آهنگ</p></div>
<div class="m2"><p>در بادیه فراخ دلتنگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>با یار ز وصل یار محروم</p></div>
<div class="m2"><p>غمگین و ز غمگذار محروم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چون پی به حریم خانه آورد</p></div>
<div class="m2"><p>رو در ره آن یگانه آورد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بگرفت ره طوافگاهش</p></div>
<div class="m2"><p>بنهاد سر وفا به راهش</p></div></div>