---
title: >-
    بخش ۵ - در معنی عشق صادقان و صدق عاشقان
---
# بخش ۵ - در معنی عشق صادقان و صدق عاشقان

<div class="b" id="bn1"><div class="m1"><p>چون صبح ازل ز عشق دم زد</p></div>
<div class="m2"><p>عشق آتش شوق در قلم زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لوح عدم قلم سرافراشت</p></div>
<div class="m2"><p>صد نقش بدیع پیکر انگاشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هستند افلاک زاده عشق</p></div>
<div class="m2"><p>ارکان به زمین فتاده عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی عشق نشان ز نیک و بد نیست</p></div>
<div class="m2"><p>چیزی که ز عشق نیست خود نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این سقف بلند لاجوردی</p></div>
<div class="m2"><p>روزان و شبان به گرد گردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیلوفر بوستان عشق است</p></div>
<div class="m2"><p>گوی خم صولجان عشق است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مغناطیسی که طبع سنگ است</p></div>
<div class="m2"><p>در آهن سخت کرده چنگ است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشقیست فتاده آهن آهنگ</p></div>
<div class="m2"><p>سر بر زده از درونه سنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان گیر قیاس دردمندان</p></div>
<div class="m2"><p>در جذبه عشق دلپسندان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بین سنگ که چون درین نشیمن</p></div>
<div class="m2"><p>بی سنگ شود ز شوق آهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر چند که عشق دردناک است</p></div>
<div class="m2"><p>آسایش سینه های پاک است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از محنت چرخ باژگون گرد</p></div>
<div class="m2"><p>بی دولت عشق کی رهد مرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کس ز آدمیان چه دون چه عالی</p></div>
<div class="m2"><p>از معنی عشق نیست خالی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیکن از دوست فرق تا دوست</p></div>
<div class="m2"><p>افزون باشد ز مغز تا پوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>معشوق یکی زر است و سیم است</p></div>
<div class="m2"><p>بی سیم دلش چو زر دو نیم است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>معشوق یکی رز است و باغ است</p></div>
<div class="m2"><p>زینهاش به سینه مانده باغ است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خوش آن که به مهر شاهدی جست</p></div>
<div class="m2"><p>زین دغدغه ها ضمیر خود شست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دل بست به طرفه نازنینی</p></div>
<div class="m2"><p>در مجلس انس خرده بینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دامن پاکی ز دست اغیار</p></div>
<div class="m2"><p>نی دامن چاک چون گل از خار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خوشتر ز وی آنکه چون اسیری</p></div>
<div class="m2"><p>شد بسته پیر دیده پیری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خجلت ده گل به تازه رویی</p></div>
<div class="m2"><p>رشک سمن از سفید مویی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آیینه روح ها جمالش</p></div>
<div class="m2"><p>مفتاح فتوح ها مقالش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عشقت چو ازین دو جا بخواند</p></div>
<div class="m2"><p>محمل به حقیقتت رساند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صحرای وجود را گل است این</p></div>
<div class="m2"><p>دریای مجاز را پل است این</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زین عشق کسی که بی نصیب است</p></div>
<div class="m2"><p>در انجمن جهان غریب است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>غافل ز حریم محرمیت</p></div>
<div class="m2"><p>نشنیده نسیم آدمیت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آرند که واعظی سخنور</p></div>
<div class="m2"><p>بر مجلس وعظ سایه گستر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از دفتر عشق نکته می راند</p></div>
<div class="m2"><p>و افسانه عاشقان همی خواند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خر گم شده ای بر او گذر کرد</p></div>
<div class="m2"><p>وز گمشده خودش خبر کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زد بانگ که کیست حاضر امروز</p></div>
<div class="m2"><p>کز عشق نبوده خاطر افروز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نی محنت عشق دیده هرگز</p></div>
<div class="m2"><p>نه داغ بتان کشیده هرگز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برخاست ز جای ساده مردی</p></div>
<div class="m2"><p>هرگز ز دلش نزاده دردی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کان کس منم ای ستوده دهر</p></div>
<div class="m2"><p>کز عشق نبوده هرگزم بهر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خر گم شده را بخواند کای یار</p></div>
<div class="m2"><p>اینک خر تو بیار افسار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این را ز خری کزان دژم نیست</p></div>
<div class="m2"><p>جز گوش دراز هیچ کم نیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سرمایه محرمی ز عشق است</p></div>
<div class="m2"><p>بل ک آدمی آدمی ز عشق است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر کس که نه عاشق آدمی نیست</p></div>
<div class="m2"><p>شایسته بزم محرمی نیست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جامی به کمند عشق شو بند</p></div>
<div class="m2"><p>بگسل ز همه به عشق پیوند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جز عشق مگوی هیچ و مشنو</p></div>
<div class="m2"><p>حرفی که نه عشق ازان خمش شو</p></div></div>