---
title: >-
    بخش ۲۲ - واقف شدن قبیله لیلی از عشق مجنون با وی و منع کردن وی از ملاقات با لیلی
---
# بخش ۲۲ - واقف شدن قبیله لیلی از عشق مجنون با وی و منع کردن وی از ملاقات با لیلی

<div class="b" id="bn1"><div class="m1"><p>خوش نغمه مغنی حجازی</p></div>
<div class="m2"><p>این نغمه زند به پرده سازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز کعبه چو بازگشت مجنون</p></div>
<div class="m2"><p>با شوقی از آنچه بود افزون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محمل به دیار لیلی افکند</p></div>
<div class="m2"><p>سررشته وصل یافت پیوند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد شد پیش ساخت پیشه</p></div>
<div class="m2"><p>جویان وصال او همیشه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سر زدی آفتاب خاور</p></div>
<div class="m2"><p>در راه طلب شدی تکاور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیین وفا ز سر گرفتی</p></div>
<div class="m2"><p>راه در دوست برگرفتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جامی ز می طلب لبالب</p></div>
<div class="m2"><p>بردی بر دوست روز تا شب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ظلمت شب علم کشیدی</p></div>
<div class="m2"><p>خود را به حریم غم کشیدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در کلبه خود مقام کردی</p></div>
<div class="m2"><p>آسایش شب حرام کردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چند که دوست را ندیدی</p></div>
<div class="m2"><p>با او گفتی و زو شنیدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون یکچندی بر این برآمد</p></div>
<div class="m2"><p>صد بار دل از زمین برآمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن واقعه فاش شد در افواه</p></div>
<div class="m2"><p>گشتند کسان لیلی آگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در گفتن این فسانه راز</p></div>
<div class="m2"><p>نمام زبان کشید و غماز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مشروح شد این حدیث درهم</p></div>
<div class="m2"><p>با مادر لیلی و پدر هم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک شب ز کمال مهربانی</p></div>
<div class="m2"><p>در گوشه خلوتی که دانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرزند خجسته را نشاندند</p></div>
<div class="m2"><p>بر وی ز سخن گهر فشاندند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کای مردم چشم و راحت دل</p></div>
<div class="m2"><p>کم شو نمک جراحت دل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر چند که چرخ پرده دار است</p></div>
<div class="m2"><p>در پرده دری ستیزه کار است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کم دوز پرده ای ز آغاز</p></div>
<div class="m2"><p>ک آخر نکند دریدنش ساز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر شب که ز مشک پرده بندد</p></div>
<div class="m2"><p>از پرده دری سحر بخندد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یک گل به نقاب غنچه ننهفت</p></div>
<div class="m2"><p>کز جنبش باد صبح نشکفت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یک دانه نشد به پرده خاک</p></div>
<div class="m2"><p>کان پرده نگشت عاقبت چاک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خلق از تو و قیس آنچه گویند</p></div>
<div class="m2"><p>زان قصه نه نیکی تو جویند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زین گونه حکایت پریشان</p></div>
<div class="m2"><p>رسوایی توست قصد ایشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بشنید صبا سحر ز بلبل</p></div>
<div class="m2"><p>آوازه پرده داری گل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر وی نفسی دمید و بگذشت</p></div>
<div class="m2"><p>آن پرده بر او درید و بگذشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زان پیش که این سخن شود فاش</p></div>
<div class="m2"><p>افتد سمری به دست اوباش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کوته کن ازان زبان مردم</p></div>
<div class="m2"><p>بر در ورق گمان مردم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دیوار چو سست شد ز یک نم</p></div>
<div class="m2"><p>از یک دو نم دگر شود خم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر رو ننمایدش ز معمار</p></div>
<div class="m2"><p>پشتیوانی شود نگونسار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آتش بنشان ز آستانه</p></div>
<div class="m2"><p>نابرده علم به سقف خانه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون شعله به سقف خانه گیرد</p></div>
<div class="m2"><p>صد حیله اگر کنی نمیرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بردار ز قیس عامری دل</p></div>
<div class="m2"><p>وز صحبت او امید بگسل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رفتن ز درت نه رای قیس است</p></div>
<div class="m2"><p>تو کعبه و قیس بوقبیس است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لیکن تو مکن برای او کار</p></div>
<div class="m2"><p>از پهلوی خود بیفکن این بار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یاری که ازو به دل غبار است</p></div>
<div class="m2"><p>یارش نکنی لقب که بار است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مپسند به گردن خود این بار</p></div>
<div class="m2"><p>بر دامنت این غبار مگذار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در ستر عفاف باش مستور</p></div>
<div class="m2"><p>دیگر مدهش به خانه دستور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مستور که رخ نهفته باشد</p></div>
<div class="m2"><p>چون غنچه ناشکفته باشد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آسوده بود به طرف گلزار</p></div>
<div class="m2"><p>رسوا نشود به کوی و بازار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وان دم که گشادچهره چون گل</p></div>
<div class="m2"><p>زد نعره عشق و شوق بلبل</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از طارم گلبنش شکستند</p></div>
<div class="m2"><p>با شاخ گیاه دسته بستند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گردانیدند گرد هر کوی</p></div>
<div class="m2"><p>بردند به هرزه آبش از روی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر چند که دامن تو پاک است</p></div>
<div class="m2"><p>وز طعنه حاسدت نه باک است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آلوده هر گمان چه باشی</p></div>
<div class="m2"><p>افتاده به هر زبان چه باشی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آن را که ز درد سر معاف است</p></div>
<div class="m2"><p>طبعش خالی ز انحراف است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از درد سر عصابه رستن</p></div>
<div class="m2"><p>بهتر که به سر عصابه بستن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>لیلی می کرد پندشان گوش</p></div>
<div class="m2"><p>از آتش قیس سینه پر جوش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ایشان با قیس بر سر جنگ</p></div>
<div class="m2"><p>لیلی بی قیس با دلی تنگ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ایشان بر قیس ناسزاگوی</p></div>
<div class="m2"><p>لیلی او را به جان دعا گوی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ایشان با قیس آب و آذر</p></div>
<div class="m2"><p>لیلی با او چو شیر و شکر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ایشان ز برون به پندگویی</p></div>
<div class="m2"><p>لیلی ز درون به مهرجویی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چون رو به دیار آن دل افروز</p></div>
<div class="m2"><p>شد قیس روان به رسم هر روز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>افتاد دوچار او عجوزی</p></div>
<div class="m2"><p>همچون خر پیر پشت کوزی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رویی که ز سختی و درشتی</p></div>
<div class="m2"><p>شاید صفتش به سنگپشتی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از کشمکش حوادث دهر</p></div>
<div class="m2"><p>فرقی چو کدو ز موی بی بهر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خالی سر او ز زیب معجر</p></div>
<div class="m2"><p>عاری تن او ز ستر میزر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>وامانده دو لب ولی نه خندان</p></div>
<div class="m2"><p>چون فرج دهان تهی ز زندان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چشمش چو دهان به جز یکی نه</p></div>
<div class="m2"><p>در دجالی او شکی نه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زان صورت زشت و شکل هایل</p></div>
<div class="m2"><p>فالی بدش اوفتاد در دل</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کان کس که نخست بیند این روی</p></div>
<div class="m2"><p>آخر ز خوشی کیش رسد بوی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بیچاره چو با دل پریشان</p></div>
<div class="m2"><p>شد همدم ماه مهر کیشان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>آن مه ز حدیث شب خبر گفت</p></div>
<div class="m2"><p>ناسازی مادر و پدر گفت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گفتا بنگر چه پیشم آمد</p></div>
<div class="m2"><p>بر ریش جگر چه نیشم آمد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از عشق تو داشتم دلی نیش</p></div>
<div class="m2"><p>شد زخم جداییت بر آن ریش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از یکشبه فرقت توام دل</p></div>
<div class="m2"><p>می سوخت به سان شمع محفل</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>اکنون که کشد به ماه یا سال</p></div>
<div class="m2"><p>هم خود تو بگو که چون بود حال</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>از آمدن تو صد بلایم</p></div>
<div class="m2"><p>گر زانکه رسد به تنگنایم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زان می ترسم که ناپسندی</p></div>
<div class="m2"><p>ناگه برساندت گزندی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مجنون چو شنید این سخن را</p></div>
<div class="m2"><p>زد چاک ز درد پیرهن را</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>جانی و دلی ز غصه جوشان</p></div>
<div class="m2"><p>برگشت بدین نوا خروشان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>کای دل پس از این صبور می باش</p></div>
<div class="m2"><p>وز هر چه نه صبر دور می باش</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گر رد تو کرد دوست غم نیست</p></div>
<div class="m2"><p>آن رد ز قبول غیر کم نیست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هجری که بود مرا دلبر</p></div>
<div class="m2"><p>وصل است و ز وصل نیز خوشتر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هر کس که نه بر رضای جانان</p></div>
<div class="m2"><p>دارد هوس لقای جانان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>در دعوی عشق نیست صادق</p></div>
<div class="m2"><p>نتوان لقبش نهاد عاشق</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>عاشق که بود ز خویش رسته</p></div>
<div class="m2"><p>بر خود در آرزو ببسته</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>افتاده به خاک نامرادی</p></div>
<div class="m2"><p>خالی ز غم و تهی ز شادی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>فارغ ز امید و ایمن از بیم</p></div>
<div class="m2"><p>بنهاده سری به خط تسلیم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>از محنت روزگار بی غم</p></div>
<div class="m2"><p>از هر چه رسد ز یار خرم</p></div></div>