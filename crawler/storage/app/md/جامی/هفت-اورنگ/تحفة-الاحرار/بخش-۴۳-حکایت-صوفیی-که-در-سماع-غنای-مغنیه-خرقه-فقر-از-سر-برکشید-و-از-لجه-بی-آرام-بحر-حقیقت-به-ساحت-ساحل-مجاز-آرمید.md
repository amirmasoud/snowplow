---
title: >-
    بخش ۴۳ - حکایت صوفیی که در سماع غنای مغنیه خرقه فقر از سر برکشید و از لجه بی آرام بحر حقیقت به ساحت ساحل مجاز آرمید
---
# بخش ۴۳ - حکایت صوفیی که در سماع غنای مغنیه خرقه فقر از سر برکشید و از لجه بی آرام بحر حقیقت به ساحت ساحل مجاز آرمید

<div class="b" id="bn1"><div class="m1"><p>کعبه روی از سر وجد عظیم</p></div>
<div class="m2"><p>در صف پیران حرم شد مقیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ دل او چو زدی پر و بال</p></div>
<div class="m2"><p>رستی از این دامگه پر وبال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وجد الهیش رهاندی ز خویش</p></div>
<div class="m2"><p>جذب حقش بازستادی ز خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمدی از هستی خود گشته صاف</p></div>
<div class="m2"><p>رقص کنان گرد حرم در طواف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی از آنجا که قضا ره زدش</p></div>
<div class="m2"><p>زخم بلا بر دل آگه زدش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطربه ای رونق کارش ببرد</p></div>
<div class="m2"><p>وز دل و جان صبر و قرارش ببرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذوق می عشوه و نازش چشید</p></div>
<div class="m2"><p>دل ز حقیقت به مجازش کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود همان حالت وجدش به جای</p></div>
<div class="m2"><p>لیک ازان شاهد دستانسرای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرقه به پیران حرم داد و گفت</p></div>
<div class="m2"><p>سر خود از خلق چه دارم نهفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دل من وجد الهی نماند</p></div>
<div class="m2"><p>جنبش من جز به ملاهی نماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز آتش اغیار درونم به جوش</p></div>
<div class="m2"><p>خرقه اصحاب چه دارم به دوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوش نبود بتکده دل زان نگار</p></div>
<div class="m2"><p>خلعت اسلام به بر کعبه وار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا به حقیقت نکشید آن مجاز</p></div>
<div class="m2"><p>باز نیامد به سر خرقه باز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جامی ازین قاعده دلپذیر</p></div>
<div class="m2"><p>تا بتوانی سبق صدق گیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زانکه درین مزرع مرد آزمای</p></div>
<div class="m2"><p>هیچ نیرزد جو گندم نمای</p></div></div>