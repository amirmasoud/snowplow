---
title: >-
    بخش ۵۳ - حکایت زاغی که چند روز در قفای کبکی دوید و از رفتار خود بازمانده به وی نرسید
---
# بخش ۵۳ - حکایت زاغی که چند روز در قفای کبکی دوید و از رفتار خود بازمانده به وی نرسید

<div class="b" id="bn1"><div class="m1"><p>زاغی از آنجا که فراغی گزید</p></div>
<div class="m2"><p>رخت خود از باغ به راغی کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنگ زدود آیینه باغ را</p></div>
<div class="m2"><p>خال سیه گشت رخ راغ را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دید یکی عرصه به دامان کوه</p></div>
<div class="m2"><p>عرضه ده مخزن پنهان کوه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبزه و لاله چو لب مهوشان</p></div>
<div class="m2"><p>داده ز فیروزه و لعلش نشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نادره کبکی به جمال تمام</p></div>
<div class="m2"><p>شاهد آن روضه فیروزه فام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فاخته گون صدره به برکرده تنگ</p></div>
<div class="m2"><p>دوخته بر صدره سجاف دو رنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیهو و دراج بدو عشقباز</p></div>
<div class="m2"><p>بر همه از گردن و سر سرفراز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پایچه ها برزده تا ساق پای</p></div>
<div class="m2"><p>کرده ز چستی به سر تیغ جای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سر هر سنگ زده قهقهه</p></div>
<div class="m2"><p>پی سپرش هم ره و هم بیرهه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیزرو و تیز دو و تیز گام</p></div>
<div class="m2"><p>خوش پرش و خوش روش و خوش خرام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم حرکاتش متناسب به هم</p></div>
<div class="m2"><p>هم خطواتش متقارب به هم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زاغ چو دید آن ره و رفتار را</p></div>
<div class="m2"><p>وان روش و جنبش هموار را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با دلی از دور گرفتار او</p></div>
<div class="m2"><p>رفت به شاگردی رفتار او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باز کشید از روش خویش پای</p></div>
<div class="m2"><p>وز پی او کرد به تقلید جای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر قدم او قدمی می کشید</p></div>
<div class="m2"><p>وز قلم پا رقمی می کشید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در پی‌ اش القصه در آن مرغزار</p></div>
<div class="m2"><p>رفت بر این قاعده روزی سه چار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عاقبت از خامی خود سوخته</p></div>
<div class="m2"><p>ره روی کبک نیاموخته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرد فرامش ره و رفتار خویش</p></div>
<div class="m2"><p>ماند غرامت زده از وار خویش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر کس ازین دایره تیزرو</p></div>
<div class="m2"><p>هست درین دیر به واری گرو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جامی و از وار همه سادگی</p></div>
<div class="m2"><p>تاجور مسند آزادگی</p></div></div>