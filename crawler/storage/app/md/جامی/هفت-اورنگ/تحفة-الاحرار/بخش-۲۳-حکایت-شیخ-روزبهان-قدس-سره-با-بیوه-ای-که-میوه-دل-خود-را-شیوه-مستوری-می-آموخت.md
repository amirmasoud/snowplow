---
title: >-
    بخش ۲۳ - حکایت شیخ روزبهان قدس سره با بیوه ای که میوه دل خود را شیوه مستوری می آموخت
---
# بخش ۲۳ - حکایت شیخ روزبهان قدس سره با بیوه ای که میوه دل خود را شیوه مستوری می آموخت

<div class="b" id="bn1"><div class="m1"><p>روزبهان فارس میدان عشق</p></div>
<div class="m2"><p>فارسیان را شه ایوان عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش در پرده سرایی رسید</p></div>
<div class="m2"><p>از پس آن پرده صدایی شنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کز سر مهر و شفقت مادری</p></div>
<div class="m2"><p>گفت به خورشید لقا دختری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کای به جمال از همه خوبان فزون</p></div>
<div class="m2"><p>پای منه هر دم از ایوان برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترسم از افزونی دیدار تو</p></div>
<div class="m2"><p>کم شود اندوه خریدار تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرخ متاعی که فراوان بود</p></div>
<div class="m2"><p>گر به مثل جان بود ارزان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیخ چو آن زمزمه را گوش کرد</p></div>
<div class="m2"><p>سر محبت ز دلش جوش کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بانگ برآورد که ای گنده پیر</p></div>
<div class="m2"><p>از دلت این بیخ هوس کنده گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن نه آنست که ماند نهان</p></div>
<div class="m2"><p>گر چه بود پرده جهان در جهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسن که در پرده مستوری است</p></div>
<div class="m2"><p>زخم هوس خورده منظوری است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ندرد چادر مستوریش</p></div>
<div class="m2"><p>جا نشود منظر منظوریش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جلوه که هر لحظه تقاضا کند</p></div>
<div class="m2"><p>بهره دلی دان که تماشا کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا ز غم عشق چو شیدا شود</p></div>
<div class="m2"><p>کوکبه حسن هویدا شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جامی اگر زنده بیننده ای</p></div>
<div class="m2"><p>در صف عشاق نشیننده ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرمه ز خاک قدم عشق گیر</p></div>
<div class="m2"><p>زنده به زیر علم عشق میر</p></div></div>