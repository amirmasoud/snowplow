---
title: >-
    بخش ۴۹ - حکایت درازدستی که دست وی به بریدن از قلم وزارت کوتاه نشد
---
# بخش ۴۹ - حکایت درازدستی که دست وی به بریدن از قلم وزارت کوتاه نشد

<div class="b" id="bn1"><div class="m1"><p>بود یکی شاه که در ملک و مال</p></div>
<div class="m2"><p>عهد وزیری چو رسیدی به سال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست قلم ساش جدا ساختی</p></div>
<div class="m2"><p>چون قلم از بند و برانداختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که گرفتی ز هوا دست او</p></div>
<div class="m2"><p>پایه اقبال شدی پست او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست وزارت به وی آراستی</p></div>
<div class="m2"><p>جان حسود از حسدش کاستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی ازین قاعده ناپسند</p></div>
<div class="m2"><p>ساخت جدا دست وزیری ز بند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست بریده به هوا برفکند</p></div>
<div class="m2"><p>تاش بگیرند صلا درفکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم خرد کرد فراز آن وزیر</p></div>
<div class="m2"><p>دست دگر کرد دراز آن وزیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست خود از بیخردی خود گرفت</p></div>
<div class="m2"><p>بهر وزارت ره مسند گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تجربه نگرفت ز دست نخست</p></div>
<div class="m2"><p>دست خود از دست دگر نیز شست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جامی ازان پیش که تیغ اجل</p></div>
<div class="m2"><p>دست تو کوتاه کند از عمل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست امل از همه کوتاه کن</p></div>
<div class="m2"><p>در صف کوته املان راه کن</p></div></div>