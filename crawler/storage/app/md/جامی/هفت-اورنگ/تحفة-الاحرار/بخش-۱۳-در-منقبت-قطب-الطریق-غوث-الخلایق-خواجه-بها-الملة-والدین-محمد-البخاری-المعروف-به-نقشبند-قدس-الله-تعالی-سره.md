---
title: >-
    بخش ۱۳ - در منقبت قطب الطریق غوث الخلایق خواجه بهاء الملة والدین محمد البخاری المعروف به نقشبند قدس الله تعالی سره
---
# بخش ۱۳ - در منقبت قطب الطریق غوث الخلایق خواجه بهاء الملة والدین محمد البخاری المعروف به نقشبند قدس الله تعالی سره

<div class="b" id="bn1"><div class="m1"><p>در خم این دایره نقش بند</p></div>
<div class="m2"><p>چند شوی بند به هر نقش چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش رها کن سوی بی نقش رو</p></div>
<div class="m2"><p>دیده به هر نقش چه داری گرو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش چو پرده ست و تو ز افسردگی</p></div>
<div class="m2"><p>مایل پرده شد از پردگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برفکن از پردگی این پرده را</p></div>
<div class="m2"><p>گرم کن از وی دل افسرده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رستن ازین پرده که بر جان توست</p></div>
<div class="m2"><p>بی مدد پیر نه امکان توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وان گهر پاک نه هر جا بود</p></div>
<div class="m2"><p>معدن آن خاک بخارا بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سکه که در یثرب و بطحا زدند</p></div>
<div class="m2"><p>نوبت آخر به بخارا زدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خط آن سکه نشد بهره مند</p></div>
<div class="m2"><p>جز دل بی نقش شه نقشبند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواجه بسته ز سر بندگی</p></div>
<div class="m2"><p>در صف صفوت کمر بندگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تاج بها بر سر دین او نهاد</p></div>
<div class="m2"><p>قفل هوا از در دین او گشاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قطب یقین نقطه توحید او</p></div>
<div class="m2"><p>خلعت دین خرقه تجرید او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر فنا را کس ازو به نگفت</p></div>
<div class="m2"><p>در بقا را کس ازو به نسفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اول او آخر هر منتهی</p></div>
<div class="m2"><p>زآخر او جیب تمنا تهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سایه او را قدم فرش سای</p></div>
<div class="m2"><p>پایه او را به سر عرش پای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صورت او راست به میزان شرع</p></div>
<div class="m2"><p>جان وی و زندگی از جان شرع</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حق طلبان را به نظرهای خاص</p></div>
<div class="m2"><p>داده ز اندیشه باطل خلاص</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر که بدان گنج عنایت رسید</p></div>
<div class="m2"><p>رخت بدایت به نهایت کشید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>راهنمای سفر اندر وطن</p></div>
<div class="m2"><p>خلوتی دایره انجمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کم زده بی همدمی هوش دم</p></div>
<div class="m2"><p>در نگذشته نظرش از قدم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بس که ز خود کرده به سرعت سفر</p></div>
<div class="m2"><p>باز نمانده قدمش از نظر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وقت توجه شده خم چون کمان</p></div>
<div class="m2"><p>از چله خلوتیان بر کران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بین که چه سان کرده دو صد قافله</p></div>
<div class="m2"><p>صید کمانی و کمان بی چله</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون ز نشان ها به عیان آمده</p></div>
<div class="m2"><p>محو نشانهاش نشان آمده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یافته در طی مقامات خویش</p></div>
<div class="m2"><p>بی صفتی را صفت ذات خویش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سلسله نسبت پیران او</p></div>
<div class="m2"><p>عروه وثقای اسیران او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>افکند آوازه آن سلسله</p></div>
<div class="m2"><p>در صف شیران جهان زلزله</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سفله که نامش به حقارت برد</p></div>
<div class="m2"><p>نام خود از لوح بصارت برد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دیده خفاش بود روز کور</p></div>
<div class="m2"><p>ور نه ز خورشید نبودی نفور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>طایر روحش که ازین کهنه دام</p></div>
<div class="m2"><p>سدره نشیمن شد و طوبی مقام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باد به فرخنده مقر مستقر</p></div>
<div class="m2"><p>عند ملیک صمد مقتدر</p></div></div>