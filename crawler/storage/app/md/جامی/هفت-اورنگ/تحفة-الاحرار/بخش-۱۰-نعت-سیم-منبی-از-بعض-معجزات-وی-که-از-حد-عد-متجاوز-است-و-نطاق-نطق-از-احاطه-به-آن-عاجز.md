---
title: >-
    بخش ۱۰ - نعت سیم منبی از بعض معجزات وی که از حد عد متجاوز است و نطاق نطق از احاطه به آن عاجز
---
# بخش ۱۰ - نعت سیم منبی از بعض معجزات وی که از حد عد متجاوز است و نطاق نطق از احاطه به آن عاجز

<div class="b" id="bn1"><div class="m1"><p>ای ز تو شق خرقه ماه منیر</p></div>
<div class="m2"><p>پیش تو مهر آمده فرمان پذیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصر نبوت به تو چون شد بلند</p></div>
<div class="m2"><p>کسر به مقصوره کسری فکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چتر فرازنده فرقت سحاب</p></div>
<div class="m2"><p>سایه نشین چتر تو را آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایه ندیدت به زمین هیچ کس</p></div>
<div class="m2"><p>نور بود سایه خورشید و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانت ز آلایش تن پاک بود</p></div>
<div class="m2"><p>سایه نینداخت بر این خاک تود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده تو هم ز پس و هم ز پیش</p></div>
<div class="m2"><p>دیده چو چشم همه عالم ز پیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روحی و غایب نه ز تو هیچ سوی</p></div>
<div class="m2"><p>در نظرت هست یکی پشت و روی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمعی و نور از تو رسد جمع را</p></div>
<div class="m2"><p>پشتی و رویی نبود شمع را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سنگ سیه در کف تو سبحه سنج</p></div>
<div class="m2"><p>دل سیهان را شده آن سبحه رنج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بحر کرم موج زن از مشت تو</p></div>
<div class="m2"><p>مقسم آن فرجه انگشت تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرسنه و تشنه هزاران هزار</p></div>
<div class="m2"><p>گشته ازان جرعه کش و لقمه خوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخل که بودش به زمین سخت پای</p></div>
<div class="m2"><p>جست به فرموده امرت ز جای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرد به هر سو که تو خواندی خرام</p></div>
<div class="m2"><p>ساخت به هر جا که تو گفتی مقام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر در غاری که گذار تو بود</p></div>
<div class="m2"><p>وز طلب خصم حصار تو بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پرده چرا بافت یکی جانور</p></div>
<div class="m2"><p>بیضه برای چه نهاد آن دگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا نرسد زخمی از اهل خلاف</p></div>
<div class="m2"><p>آمدت این بیضه گر آن درع باف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مایده کان نیم شبیت آمده</p></div>
<div class="m2"><p>روزیی از خوان «ابیت » آمده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>«یطعمنی » طعمه و «یسقینی » آب</p></div>
<div class="m2"><p>اینت گوارنده طعام و شراب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون لب تو لقمه ز بزغاله کرد</p></div>
<div class="m2"><p>لقمه به زیر لب تو ناله کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت که آلوده به زهرم مخور</p></div>
<div class="m2"><p>گر چه برد تلخی زهر این شکر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قبضه ریگی که فشاندی ز کف</p></div>
<div class="m2"><p>شد بصر بی بصرانش هدف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سرمه صفت نور بصر را کفیل</p></div>
<div class="m2"><p>بود که شد در نظر خصم میل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جامی عاجز که نواساز توست</p></div>
<div class="m2"><p>بسته لب از نکته اعجاز توست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر چه گهروار چو تیغ آمده ست</p></div>
<div class="m2"><p>بلکه گهربار چو میغ آمده ست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خواست به نعتت گهری تابناک</p></div>
<div class="m2"><p>ریخت ز رویش خوی خجلت به خاک</p></div></div>