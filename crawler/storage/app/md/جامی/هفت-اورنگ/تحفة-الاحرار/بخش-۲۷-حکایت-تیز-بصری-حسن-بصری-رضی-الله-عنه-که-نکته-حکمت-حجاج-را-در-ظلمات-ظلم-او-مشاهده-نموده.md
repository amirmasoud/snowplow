---
title: >-
    بخش ۲۷ - حکایت تیز بصری حسن بصری رضی الله عنه که نکته حکمت حجاج را در ظلمات ظلم او مشاهده نموده
---
# بخش ۲۷ - حکایت تیز بصری حسن بصری رضی الله عنه که نکته حکمت حجاج را در ظلمات ظلم او مشاهده نموده

<div class="b" id="bn1"><div class="m1"><p>از حسن آن بصری نافذ بصر</p></div>
<div class="m2"><p>نکته ای آرند عجب مختصر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز دل غفلت زده گر دم فشاند</p></div>
<div class="m2"><p>آن نفس پاک که حجاج راند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت فضولی که نه در بندگی</p></div>
<div class="m2"><p>کش پی آن داد خدا زندگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساعتی از عمر به پایان برد</p></div>
<div class="m2"><p>گر چه در آن ملک سلیمان برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاید اگر داغ به جانش نهند</p></div>
<div class="m2"><p>مالش محرومی از آنش دهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش وی آید المی جانگداز</p></div>
<div class="m2"><p>سوزد ازان حسرت دور و دراز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو حسن هر که بود هوشمند</p></div>
<div class="m2"><p>گوش کند از لب حجاج پند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حکمت نو یافته هر جا بود</p></div>
<div class="m2"><p>گم شده خاطر دانا بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه بیابد به رهش بی طلب</p></div>
<div class="m2"><p>گیردش از خاک به دست ادب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوهر گنجینه جان سازدش</p></div>
<div class="m2"><p>در صدف سینه نهان سازدش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جامی اگر خلق تو آمد حسن</p></div>
<div class="m2"><p>از لب هر ظالم حجاج فن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نکته حکمت که رسد گوش کن</p></div>
<div class="m2"><p>ظلم رساننده فراموش کن</p></div></div>