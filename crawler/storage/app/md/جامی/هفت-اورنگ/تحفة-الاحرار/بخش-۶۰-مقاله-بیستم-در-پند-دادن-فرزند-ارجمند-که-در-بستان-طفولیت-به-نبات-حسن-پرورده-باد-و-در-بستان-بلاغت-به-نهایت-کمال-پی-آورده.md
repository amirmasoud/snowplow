---
title: >-
    بخش ۶۰ - مقاله بیستم در پند دادن فرزند ارجمند که در بستان طفولیت به نبات حسن پرورده باد و در بستان بلاغت به نهایت کمال پی آورده
---
# بخش ۶۰ - مقاله بیستم در پند دادن فرزند ارجمند که در بستان طفولیت به نبات حسن پرورده باد و در بستان بلاغت به نهایت کمال پی آورده

<div class="b" id="bn1"><div class="m1"><p>ای شب امید مرا ماه نو</p></div>
<div class="m2"><p>دیده بختم به خیالت گرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پس سی روز برآید هلال</p></div>
<div class="m2"><p>روی نمودی تو پس از شصت سال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سال تو چار است به وقت شمار</p></div>
<div class="m2"><p>چار تو چل باد و چلت باز چار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چل تو یک چله کز علم و حال</p></div>
<div class="m2"><p>سیر کنی در درجات کمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نام تو شد یوسف مصر وفا</p></div>
<div class="m2"><p>باد لقب دولت و دین را ضیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کنم از خامه حکمت نگار</p></div>
<div class="m2"><p>بهر تو این نامه حکمت نگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه کنون نیست تو را فهم پند</p></div>
<div class="m2"><p>چون به حد فهم رسی کار بند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نشود برقع تو موی روی</p></div>
<div class="m2"><p>پا منه از خانه به بازار و کوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلسله بند قدم خویش باش</p></div>
<div class="m2"><p>حبس نشین حرم خویش باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچگه از صحبت همخانگان</p></div>
<div class="m2"><p>رخت مکش بر در بیگانگان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طلعت بیگانه نه میمون بود</p></div>
<div class="m2"><p>خاصه که سالش ز تو افزون بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور به دبستان سر و کارت دهند</p></div>
<div class="m2"><p>لوح «الف بی » به کنارت نهند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پهلوی هر سفله مشو جانشین</p></div>
<div class="m2"><p>از همه یکتا شو و تنها نشین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر چه به خود نیست کج اندام «الف »</p></div>
<div class="m2"><p>بین که چه سان کج شده در «لام الف »</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لوح خود آن دم که نهی بر کنار</p></div>
<div class="m2"><p>چون «الف » انگشت ازان بر مدار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>«دل » وش از شرم فکن سر به پیش</p></div>
<div class="m2"><p>«صاد» صفت دوز بر آن چشم خویش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خنده زنان گاه به آن گه به این</p></div>
<div class="m2"><p>رسته دندان منما همچو «سین »</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دل مکن از فکر پریشان دو نیم</p></div>
<div class="m2"><p>تنگ دهان باش ز گفتن چو «میم »</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گوش مده بیهده هر قیل و قال</p></div>
<div class="m2"><p>تا نکشی درد سر گوشمال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دار ادب درس معلم نگاه</p></div>
<div class="m2"><p>تا نشوی طبلک تعلیمگاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سیلی او گر چه فضیلت ده است</p></div>
<div class="m2"><p>گر تو به سیلی نرسانی به است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پی چو به سر منزل قرآن بری</p></div>
<div class="m2"><p>روزی هر روزه ازان خوان خوری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چند گره زن به میان رحل وار</p></div>
<div class="m2"><p>شاهد مصحف بنشان بر کنار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باش ز رخسار نکو فال او</p></div>
<div class="m2"><p>محو تماشای خط و خال او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر چه کنی زو گهر سلک خویش</p></div>
<div class="m2"><p>ساز به تکرار زبان ملک خویش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حرف نوشته به دل طفل خرد</p></div>
<div class="m2"><p>گزلک نیسان نتواند سترد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون تو حق حفظ وی آری بجای</p></div>
<div class="m2"><p>حفظ حق از جانت شود غم زدای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دست طلب ده به قلم گاه گاه</p></div>
<div class="m2"><p>شو به سوی خطه خط رو به راه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باز نشان از ره کسب کمال</p></div>
<div class="m2"><p>از نم آن نایژه گرد ملال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کوش به تحسین خط از هر نمط</p></div>
<div class="m2"><p>لیک نه چندان که شوی جمله خط</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صفر مکن بهر سه انگشت خویش</p></div>
<div class="m2"><p>از گهر هر هنری مشت خویش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شعر اگر چه هنری دیگر است</p></div>
<div class="m2"><p>شمه ای از عیب به شعر اندر است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شعر که عیبش ز میان سر زند</p></div>
<div class="m2"><p>همت پاکانش قلم در زند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ور فتدت گه گهی اندیشه اش</p></div>
<div class="m2"><p>کوش که چون من نکنی پیشه اش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر نفس آمد گهری ارجمند</p></div>
<div class="m2"><p>قیمت آن بیشتر از چون و چند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آن گهر از دست مده رایگان</p></div>
<div class="m2"><p>خاصه که در مدح فرومایگان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>محنت این کار به خود ره مده</p></div>
<div class="m2"><p>رنج کشی در طلب علم به</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تاج سر جمله هنرهاست علم</p></div>
<div class="m2"><p>قفل گشای همه درهاست علم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در طلب علم کمر چست کن</p></div>
<div class="m2"><p>دست ز اشغال دگر سست کن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>با تو پس از علم چه گویم سخن</p></div>
<div class="m2"><p>علم چو آید به تو گوید چه کن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>علم کثیر آمد و عمرت قصیر</p></div>
<div class="m2"><p>آنچه ضروریست به آن شغل گیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر چه ضروریست چو حاصل کنی</p></div>
<div class="m2"><p>به که عمارتگری دل کنی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آنست عمارتگری دل که دل</p></div>
<div class="m2"><p>واکشی از کشمکش آب و گل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پای به دامن کشی و سر به جیب</p></div>
<div class="m2"><p>تن به شهادت دهی و جان به غیب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یاد خدا پردگی هش کنی</p></div>
<div class="m2"><p>هر چه بجز اوست فرامش کنی</p></div></div>