---
title: >-
    بخش ۶۲ - ختم خطاب و خاتمه کتاب
---
# بخش ۶۲ - ختم خطاب و خاتمه کتاب

<div class="b" id="bn1"><div class="m1"><p>خامه چو بر موجب جف القلم</p></div>
<div class="m2"><p>خشک بیستاد از این خوش رقم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر دعا از لب ام الکتاب</p></div>
<div class="m2"><p>حرف سقاک اللهش آمد خطاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روح امین دست به آمین گشاد</p></div>
<div class="m2"><p>چرخ برین سبحه پروین گشاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوهر آن سبحه به پایش فشاند</p></div>
<div class="m2"><p>در قدم غالیه سایش فشاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت جزاک الله ازین فیض پاک</p></div>
<div class="m2"><p>از تو به سجاده نشینان خاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش شفانامه عیسی ست این</p></div>
<div class="m2"><p>یا رقم خامه مانی ست این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنچه ای از گلبن ناز آمده</p></div>
<div class="m2"><p>یا گلی از گلشن راز آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حرف کش دفتر فرزانگیست</p></div>
<div class="m2"><p>تازه کن مایه دیوانگیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قفل گشای در کاخ صفاست</p></div>
<div class="m2"><p>عطر فزای گل شاخ وفاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صبح طرب مطلع انوار اوست</p></div>
<div class="m2"><p>جیب ادب مخزن اسرار اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نظم کلامش نه بغایت بلند</p></div>
<div class="m2"><p>تا نشود هر کس ازان بهره مند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر معانیش نه زانسان دقیق</p></div>
<div class="m2"><p>کش نتوان یافت به فکر عمیق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لفظ خوش و معنی ظاهر در او</p></div>
<div class="m2"><p>آب زلال است و جواهر در او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از خس و خاشاک چو صافیست آب</p></div>
<div class="m2"><p>می نشود بر در و گوهر حجاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاهد اسرار وی از صوت و حرف</p></div>
<div class="m2"><p>کرده لباسی به بر خود شگرف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسته حروفش تتق مشکفام</p></div>
<div class="m2"><p>«حور مقصورات فی الخیام »</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ماشطه خامه چو آراستش</p></div>
<div class="m2"><p>از قبل من لقبی خواستش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تحفت الاحرار لقب دادمش</p></div>
<div class="m2"><p>تحفه به احرار فرستادمش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر که به دل از خردش روزنی ست</p></div>
<div class="m2"><p>در نظرش هر ورقی گلشنی ست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>راست چمن هاست در آنجا سطور</p></div>
<div class="m2"><p>پر گل شادی و نهال سرور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جوی زر جدولشان آبخورد</p></div>
<div class="m2"><p>سبزه تر گرد وی از لاژورد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گرد مجلد سوی جلدش چو میل</p></div>
<div class="m2"><p>داد ادیم از سر مهرش سهیل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زهره شد از چنگ پرآوازه اش</p></div>
<div class="m2"><p>تار بریشم ده شیرازه اش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هیکل آیات گرامیست این</p></div>
<div class="m2"><p>حرز حمایتگر جامیست این</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باش خدایا به کمال کرم</p></div>
<div class="m2"><p>حافظ او ز آفت هر کج قلم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ظلمت کلک وی ازین حرف نور</p></div>
<div class="m2"><p>دار چو انگشت بداندیش دور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون بتراشد ز سر خامه نیش</p></div>
<div class="m2"><p>سازد ازان نیش دل نامه ریش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خط وی از خطه دانش برون</p></div>
<div class="m2"><p>گشته به سر حد خطا رهنمون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون خط تقطیع نه بر اصطلاح</p></div>
<div class="m2"><p>وز حک و اصلاح نگیرد صلاح</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تیغ کند خامه سر تیز را</p></div>
<div class="m2"><p>رشته برد نظم دلاویز را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کلک وی از چوب عوان بدتر است</p></div>
<div class="m2"><p>وزن کش قافیه ویرانگر است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دیده حرفی که بود دیده باز</p></div>
<div class="m2"><p>گردد ازو وقت کتابت فراز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حرف نگارد چو به کلک هوس</p></div>
<div class="m2"><p>نقطه نه بر جای نهد چون مگس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گاه زند بر رخ عم خال غم</p></div>
<div class="m2"><p>گاه شود سیم ز دستش ستم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بس که مرید از قلمش مرتد است</p></div>
<div class="m2"><p>ضد وی آنجا که نویسد صد است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چند به لب باج حکایت دهیم</p></div>
<div class="m2"><p>شکر به تاراج شکایت دهیم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شکر که این رشته به پایان رسید</p></div>
<div class="m2"><p>بخیه این خرقه به دامان رسید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مهر نه خاتمه این خطاب</p></div>
<div class="m2"><p>شد رقم خاتم تم الکتاب</p></div></div>