---
title: >-
    بخش ۴۵ - حکایت آن عالم در چاه افتاده که دست به شاگرد خود نداد تا جزای آخرت از دست ندهد
---
# بخش ۴۵ - حکایت آن عالم در چاه افتاده که دست به شاگرد خود نداد تا جزای آخرت از دست ندهد

<div class="b" id="bn1"><div class="m1"><p>عالمی از چاه جهالت برون</p></div>
<div class="m2"><p>در رهی افتاد به چاهی درون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ مدد دست ندادش به راه</p></div>
<div class="m2"><p>ماند در آن راه چو یوسف به چاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایه صفت در تگ چاه آرمید</p></div>
<div class="m2"><p>سایه شخصی به سر چاه دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نعره برآورد که ای رهنورد</p></div>
<div class="m2"><p>از ره احسان و مروت مگرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای مروت به سر چاه نه</p></div>
<div class="m2"><p>دست به افتاده از راه ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راهرو آمد به سر چاه و گفت</p></div>
<div class="m2"><p>دست بده ای به غم و آه جفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت نخست از کرم عام خویش</p></div>
<div class="m2"><p>گو خبرم از لقب و نام خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت که شاگرد کمین توام</p></div>
<div class="m2"><p>در ره دین خاک نشین توام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت که حاشا که ازین چاه پست</p></div>
<div class="m2"><p>در زنم امروز به دست تو دست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من که به تعلیم میان بسته ام</p></div>
<div class="m2"><p>از غرض سود و زیان رسته ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوششم از روی خردمندی است</p></div>
<div class="m2"><p>خاص پی فضل خداوندی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کی به جزای دگر آلایمش</p></div>
<div class="m2"><p>وز غرض آلودگی افزایمش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در تک این چاه نشینم اسیر</p></div>
<div class="m2"><p>تا شودم بی غرضی دستگیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پایه علمم چو بلند اوفتاد</p></div>
<div class="m2"><p>هر چه جز آنم نه پسند اوفتاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همت جامی که بلندی گرفت</p></div>
<div class="m2"><p>از شرف علم پسندی گرفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>علم پسندید ز طبع بلند</p></div>
<div class="m2"><p>هر چه پسندید همانش بسند</p></div></div>