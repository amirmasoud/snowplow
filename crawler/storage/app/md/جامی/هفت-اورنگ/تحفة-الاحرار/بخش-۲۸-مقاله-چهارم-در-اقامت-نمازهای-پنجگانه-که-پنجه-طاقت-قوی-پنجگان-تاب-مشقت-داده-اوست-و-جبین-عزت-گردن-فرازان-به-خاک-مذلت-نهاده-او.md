---
title: >-
    بخش ۲۸ - مقاله چهارم در اقامت نمازهای پنجگانه که پنجه طاقت قوی پنجگان تاب مشقت داده اوست و جبین عزت گردن فرازان به خاک مذلت نهاده او
---
# بخش ۲۸ - مقاله چهارم در اقامت نمازهای پنجگانه که پنجه طاقت قوی پنجگان تاب مشقت داده اوست و جبین عزت گردن فرازان به خاک مذلت نهاده او

<div class="b" id="bn1"><div class="m1"><p>ای شده رخنه صف طاعت ز تو</p></div>
<div class="m2"><p>مانده تهی سلک جماعت ز تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنبه غفلت چو تو را بست گوش</p></div>
<div class="m2"><p>سود نکردت ز مؤذن خروش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نعره او خواب تو را کم نکرد</p></div>
<div class="m2"><p>قامت او قد تو را خم نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میل نمازت به جوانی نبود</p></div>
<div class="m2"><p>پشت دو تا گشته به پیری چه سود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پشت چو محراب خمیده تو را</p></div>
<div class="m2"><p>روی به قبله نرسیده تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پنج نماز است به از پنج گنج</p></div>
<div class="m2"><p>به که بدین پنج شوی گنج سنج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر تو پنجاه به پنج آمده</p></div>
<div class="m2"><p>طبع تو زین پنج به رنج آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پنجه خود ساز بدین پنج سخت</p></div>
<div class="m2"><p>پنجه ابلیس بدر لخت لخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نکنی پنجه بدین رنجه اش</p></div>
<div class="m2"><p>کی بودت طاقت سر پنجه اش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شیر دلی پنجه ازین پنج کن</p></div>
<div class="m2"><p>شاخ هوا را بکن از بیخ و بن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاخ هوا را نشود بیخ سست</p></div>
<div class="m2"><p>تا ندهی نم ز طهارت نخست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست بشو بهر تمسک به خیر</p></div>
<div class="m2"><p>روی ز پندار توجه به غیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از کف مساح به سر تاج نه</p></div>
<div class="m2"><p>پای چو شد شسته به معراج نه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا چو به معراج تو را ره شود</p></div>
<div class="m2"><p>دست شیاطین ز تو کوته شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وقت سیاست پی ادبارشان</p></div>
<div class="m2"><p>پایه معراج تو بس دارشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دین تو را نیست ستون جز نماز</p></div>
<div class="m2"><p>بهر قیامش چو ستون قد فراز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پشت تو آندم که ز طاعت دوتاست</p></div>
<div class="m2"><p>از پی این خیمه ستونیست راست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مسجد تو شد همه جا سنگ و خاک</p></div>
<div class="m2"><p>خاک شد از بهر تو چون آب پاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا ره طاعت بود آسان تو را</p></div>
<div class="m2"><p>زان نشود طبع هراسان تو را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لیک تو از کاهلی و جاهلی</p></div>
<div class="m2"><p>همچو خران مانده در آب و گلی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پای امل از گل طینت برآر</p></div>
<div class="m2"><p>چشم خرد بر زر و زینت مدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زینت تو بس کمر بندگی</p></div>
<div class="m2"><p>تاج تو در سجده سرافکندگی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رفته عمر تو رهین فناست</p></div>
<div class="m2"><p>دولت آینده که داند که راست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شاهد وقت تو همین ساعت است</p></div>
<div class="m2"><p>خوبترین زیور آن طاعت است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شرم تو بادا که به بالا و پست</p></div>
<div class="m2"><p>سجده طاعت بردش هر چه هست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو کنی از سجده او سرکشی</p></div>
<div class="m2"><p>به که ازین شیوه قدم در کشی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ساق ادب بر زده عرش برین</p></div>
<div class="m2"><p>بر در طاعت شده کرسی نشین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چرخ فلک خرقه ازرق به بر</p></div>
<div class="m2"><p>بسته ز جوزا پی خدمت کمر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دوخته شب تا به سحر در رکوع</p></div>
<div class="m2"><p>دیده انجم به زمین خضوع</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سبحه پروین ز کف آویخته</p></div>
<div class="m2"><p>اشک ستاره به سحر ریخته</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ماه زده بر در او کوس مهر</p></div>
<div class="m2"><p>مهر به خاک ره او سوده چهر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جنبش ارکان به سوی تحت و فوق</p></div>
<div class="m2"><p>از کشش اوست به زنجیر شوق</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کار جماد است پی حی پاک</p></div>
<div class="m2"><p>قعده طاعت به مصلای خاک</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وصف نبات است نمودن قیام</p></div>
<div class="m2"><p>بر در قیوم جهان بر دوام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هیئت حیوان به رکوع است راست</p></div>
<div class="m2"><p>دایم از آنست که پشتش دوتاست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ور نبود میل سجودش چرا</p></div>
<div class="m2"><p>سر به زمین می برد اندر چرا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خیز و تو هم برگ تعبد ساز</p></div>
<div class="m2"><p>جمع کن این چند عمل در نماز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا ز پریشانی ظاهر بری</p></div>
<div class="m2"><p>راه به جمعیت باطن بری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جمع نشینی به مقام حضور</p></div>
<div class="m2"><p>از خود و از هستی خود بی شعور</p></div></div>