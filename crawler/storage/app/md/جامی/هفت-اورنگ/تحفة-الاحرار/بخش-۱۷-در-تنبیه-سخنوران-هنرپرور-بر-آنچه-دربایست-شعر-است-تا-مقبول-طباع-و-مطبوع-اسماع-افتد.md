---
title: >-
    بخش ۱۷ - در تنبیه سخنوران هنرپرور بر آنچه دربایست شعر است تا مقبول طباع و مطبوع اسماع افتد
---
# بخش ۱۷ - در تنبیه سخنوران هنرپرور بر آنچه دربایست شعر است تا مقبول طباع و مطبوع اسماع افتد

<div class="b" id="bn1"><div class="m1"><p>قافیه سنجان چو در دل زنند</p></div>
<div class="m2"><p>در به رخ تیره دلان گل زنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی چو در قافیه سنجی کنند</p></div>
<div class="m2"><p>پشت بر این دیر سپنجی کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن بگذارند و همه جان شوند</p></div>
<div class="m2"><p>کوه ببرند و سوی کان شوند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان کنی و کان کنی آیینشان</p></div>
<div class="m2"><p>صیرفی چرخ گهر چینشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که درین کان جگری خورده ای</p></div>
<div class="m2"><p>گوهر رنگین به کف آورده ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوهر این کان همه یکرنگ نیست</p></div>
<div class="m2"><p>لؤلؤ عمان همه همسنگ نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوهر و لعل از دل کان می طلب</p></div>
<div class="m2"><p>هر چه بیابی به ازان می طلب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که به خس کرد قناعت خسیست</p></div>
<div class="m2"><p>به طلبی کن که به از به بسیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناشده از خوی بدت دل تهی</p></div>
<div class="m2"><p>کی رسد از نظم تو بوی بهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه به دل هست ز پاک و پلید</p></div>
<div class="m2"><p>در سخن آید اثر آن پلید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جیفه چو بندد دهن جوی تنگ</p></div>
<div class="m2"><p>آب روان گیرد ازو بوی و رنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون گره نافه گشاید نسیم</p></div>
<div class="m2"><p>غالیه بو گردد و عنبر شمیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نظم که نسبت به گهر باشدش</p></div>
<div class="m2"><p>به ز گهر باشد اگر باشدش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لفظ جهان گشته و معنی غریب</p></div>
<div class="m2"><p>لیک نه بیگانه ز فهم لبیب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قافیه کم یاب چو دیبای چین</p></div>
<div class="m2"><p>وزن سبک سنگ چو ماه معین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نی رقم کلک تکلف بر او</p></div>
<div class="m2"><p>نی کلف داغ تصلف بر او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یافته از صنعت و دقت جمال</p></div>
<div class="m2"><p>لیک نه بیرون ز حد اعتدال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاهد پرورده به صد عز و ناز</p></div>
<div class="m2"><p>بیش به مشاطه ندارد نیاز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر رخش از غالیه مشک سای</p></div>
<div class="m2"><p>خوب بود خال ولی یک دو جای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خال که از قاعده افزون فتد</p></div>
<div class="m2"><p>بر رخ معشوق نه موزون فتد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حال جمالش به تباهی کشد</p></div>
<div class="m2"><p>روی سفیدش به سیاهی کشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این همه گفتیم ولی زین شمار</p></div>
<div class="m2"><p>چاشنی عشق بود اصل کار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عشق که رقص فلک از نور اوست</p></div>
<div class="m2"><p>خوان سخن را نمک از شور اوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جامی اگر در سرت این شور نیست</p></div>
<div class="m2"><p>خوان سخن گر ننهی دور نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرد کرم پیشه کجا خوان نهد</p></div>
<div class="m2"><p>تا نه ز آغاز نمکدان نهد</p></div></div>