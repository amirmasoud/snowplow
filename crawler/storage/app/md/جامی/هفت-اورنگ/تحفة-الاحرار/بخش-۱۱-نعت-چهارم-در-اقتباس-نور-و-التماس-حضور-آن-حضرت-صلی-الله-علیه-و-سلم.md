---
title: >-
    بخش ۱۱ - نعت چهارم در اقتباس نور و التماس حضور آن حضرت صلی الله علیه و سلم
---
# بخش ۱۱ - نعت چهارم در اقتباس نور و التماس حضور آن حضرت صلی الله علیه و سلم

<div class="b" id="bn1"><div class="m1"><p>ای به سرا پرده یثرب به خواب</p></div>
<div class="m2"><p>خیز که شد مشرق و مغرب خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفته ز دستیم برون کن ز برد</p></div>
<div class="m2"><p>دستی و بنمای یکی دستبرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توبه ده از سرکشی ایام را</p></div>
<div class="m2"><p>باز خر از ناخوشی اسلام را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهد مسیح از فلک آور به زیر</p></div>
<div class="m2"><p>رایت مهدی به فلک زن دلیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاله دجال بنه بر خرش</p></div>
<div class="m2"><p>رو به بیابان عدم ده سرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افسر ملک از سر دونان بکش</p></div>
<div class="m2"><p>دامن دولت ز زبونان بکش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز پسان را فکن از پیشگاه</p></div>
<div class="m2"><p>داد ستم کش ز ستم کیش خواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خامه مفتی که چو انگشت آز</p></div>
<div class="m2"><p>شد ز پی لقمه ربایی دراز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست سیاست بکش و بشکنش</p></div>
<div class="m2"><p>همچو نی اندر بن ناخن زنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>واعظ پر گو که به پستیست بند</p></div>
<div class="m2"><p>پایه خود کرده ز منبر بلند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نه بزرگ است ز شرعش سخن</p></div>
<div class="m2"><p>منبر او بر سر او خرد کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صومعه را قاعده تازه نه</p></div>
<div class="m2"><p>رخت خرابات به دروازه نه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدعتیان را ره سنت نمای</p></div>
<div class="m2"><p>عزلتیان را در عزت گشای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرقه تزویر به صد پاره کن</p></div>
<div class="m2"><p>جان مزور ز تن آواره کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شعله فکن خرمن ابلیس را</p></div>
<div class="m2"><p>مهره شکن سبحه تلبیس را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گنج تو در خاک نهان دیر ماند</p></div>
<div class="m2"><p>نور تو غایب ز جهان دیر ماند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پرتو روی تو که هست آفتاب</p></div>
<div class="m2"><p>بود ازو کشور دین نور یاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برق فراقت چو جهانسوز شد</p></div>
<div class="m2"><p>مشعل یارانت شب افروز شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مشعلشان چرخ چو بی نور کرد</p></div>
<div class="m2"><p>صبح هدی را شب دیجور کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ظلمت بدعت همه عالم گرفت</p></div>
<div class="m2"><p>بلکه جهان جامه ماتم گرفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کاش فتد ز اوج عروجت رجوع</p></div>
<div class="m2"><p>باز کند نور جمالت طلوع</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دیده عالم به تو روشن شود</p></div>
<div class="m2"><p>گلخن گیتی ز تو گلشن شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دولتیان از تو علم برکشند</p></div>
<div class="m2"><p>ظلمتیان رو به عدم در کشند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جامی از آنجا که هوادار توست</p></div>
<div class="m2"><p>روی تو نادیده گرفتار توست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر لب جانبخش تو فرمان دهد</p></div>
<div class="m2"><p>بر قدمت سر نهد و جان دهد</p></div></div>