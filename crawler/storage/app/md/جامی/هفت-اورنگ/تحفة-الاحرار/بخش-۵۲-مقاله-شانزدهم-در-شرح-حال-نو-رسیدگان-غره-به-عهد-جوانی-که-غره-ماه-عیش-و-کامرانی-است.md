---
title: >-
    بخش ۵۲ - مقاله شانزدهم در شرح حال نو رسیدگان غره به عهد جوانی که غره ماه عیش و کامرانی است
---
# بخش ۵۲ - مقاله شانزدهم در شرح حال نو رسیدگان غره به عهد جوانی که غره ماه عیش و کامرانی است

<div class="b" id="bn1"><div class="m1"><p>ای شده با موی سیاه از غرور</p></div>
<div class="m2"><p>از نفر موی سفیدان نفور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ ز سفید به سیاهی منه</p></div>
<div class="m2"><p>نور الهی به ملاهی مدره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طفلی و چون شیر شده موی پیر</p></div>
<div class="m2"><p>هست عجب نفرت طفلان ز شیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاغ سیاهی تو درین بوم بیم</p></div>
<div class="m2"><p>کی هلد این باز سفیدت سلیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تکیه بر اسباب جوانی مکن</p></div>
<div class="m2"><p>هر چه توان تا بتوانی مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بازوی تو گر به مثل آهن است</p></div>
<div class="m2"><p>پوست اگر بر تن تو جوشن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست اجل موم کند آهنت</p></div>
<div class="m2"><p>تیغ قضا چاک زند جوشنت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خم نکنی بهر خدا پشت خویش</p></div>
<div class="m2"><p>سخت کمانی مکن ای سست کیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قوت بسیار تو چون کم شود</p></div>
<div class="m2"><p>گر همه تیر است قدت خم شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش که سازد فلک عشوه ده</p></div>
<div class="m2"><p>پشت تو را همچو کمان تن چو زه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باش کمان در پی طاعت وران</p></div>
<div class="m2"><p>گوشه گزین از ره تحسین گران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر تن خود راه ریاضت گشای</p></div>
<div class="m2"><p>از تن خود کم کن و در جان فزای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سالک ره خشک بدن به بود</p></div>
<div class="m2"><p>تک نزند اسب که فربه بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ناشده پشت تو ز پیران راه</p></div>
<div class="m2"><p>راست همی رو پی پیران راه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر صف دینند چو پیران امیر</p></div>
<div class="m2"><p>باش به فتراک امیران اسیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا نه از ایشان به اسیری رسی</p></div>
<div class="m2"><p>کی بود امکان که به پیری رسی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر در هر پیر کمربندیت</p></div>
<div class="m2"><p>به که به سر تاج خداوندیت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پایه آن تاج بود بس بلند</p></div>
<div class="m2"><p>کنگر آن را کمر آمد کمند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کوه که صد کان گهر یافته ست</p></div>
<div class="m2"><p>تاج بلندی ز کمر یافته ست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سرکشی کاف برون کن ز سر</p></div>
<div class="m2"><p>میم صفت بند گره بر کمر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در قدم پیر سبک سایه شو</p></div>
<div class="m2"><p>وز گهرش گنج گرانمایه شو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون تو به خدمت مددش می کنی</p></div>
<div class="m2"><p>آن مدد از بهر خودش می کنی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آب چو ریزی به کفش در وضو</p></div>
<div class="m2"><p>چهره اقبال دهی شست و شو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سنگ ز راهش چو نهی بر کران</p></div>
<div class="m2"><p>پله طاعات کنی زان گران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کفش تهی چون نهیش پیش پای</p></div>
<div class="m2"><p>بر سر افلاک شوی کفش سای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رکوه که در همرهی او بری</p></div>
<div class="m2"><p>آب ز سرچشمه حیوان خوری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خاک رهش را به مژه روب پاک</p></div>
<div class="m2"><p>تا شودت دیده جان سرمه ناک</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غاشیه دولت او کش به دوش</p></div>
<div class="m2"><p>تا شودت ستر کرم عیب پوش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا نشوی پیر چو پیران کار</p></div>
<div class="m2"><p>دست خود از دامن خدمت مدار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پایه پیری به جوانی مجوی</p></div>
<div class="m2"><p>راه ارادت به امانی مپوی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ترسمت آن پایه نگردد به ساز</p></div>
<div class="m2"><p>مانی از آداب جوانیت باز</p></div></div>