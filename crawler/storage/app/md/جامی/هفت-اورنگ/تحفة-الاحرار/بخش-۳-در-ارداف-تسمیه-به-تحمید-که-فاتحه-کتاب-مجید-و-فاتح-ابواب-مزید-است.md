---
title: >-
    بخش ۳ - در ارداف تسمیه به تحمید که فاتحه کتاب مجید و فاتح ابواب مزید است
---
# بخش ۳ - در ارداف تسمیه به تحمید که فاتحه کتاب مجید و فاتح ابواب مزید است

<div class="b" id="bn1"><div class="m1"><p>آنچه نگارد پی این خوش رقم</p></div>
<div class="m2"><p>بر سر هر نامه دبیر قلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حمد خداییست که از کلک کن</p></div>
<div class="m2"><p>بر ورق باد نویسد سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون رقم او بود این تازه حرف</p></div>
<div class="m2"><p>جز به ثنایش نتوان کرد صرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیک ثنایش ز بیان برتر است</p></div>
<div class="m2"><p>هر چه زبان گوید ازان برتر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نطق و ثنایش چه تمناست این</p></div>
<div class="m2"><p>عقل و تمناش چه سوداست این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست سخن جز گرهی چند سست</p></div>
<div class="m2"><p>طبع سخنور زده بر باد چست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ گشادی نبود در گره</p></div>
<div class="m2"><p>گر نشود کار به آن بند به</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد گره از رشته پر تاب و پیچ</p></div>
<div class="m2"><p>گر بگشایند در آن نیست هیچ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل درین عقده ز خود گشته گم</p></div>
<div class="m2"><p>کرده درین فکر سر رشته گم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رشته فکرش که سزد پر گهر</p></div>
<div class="m2"><p>پر بود اینجا ز گره سر به سر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می دهد این رشته ز سبحه نشان</p></div>
<div class="m2"><p>صد گره افتاده در او مهره سان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقل گرفته به کفش سبحه وار</p></div>
<div class="m2"><p>عاجزی خویش کند زان شمار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه نه دم می زند از عجز کیست</p></div>
<div class="m2"><p>غایت این کار بجز عجز چیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عجز به از هر دل دانا که هست</p></div>
<div class="m2"><p>بر در آن حی توانا که هست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرسله بند گهر کان جود</p></div>
<div class="m2"><p>سلسله پیوند نظام وجود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غره فروز سحر خاکیان</p></div>
<div class="m2"><p>مشعله سوز شب افلاکیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خوان کرامت نه آیندگان</p></div>
<div class="m2"><p>گنج سلامت ده پایندگان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چشمه کن قله قاف قدم</p></div>
<div class="m2"><p>نایژه پرداز شکاف قلم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روز برآرنده شبهای تار</p></div>
<div class="m2"><p>کارگزارنده مردان کار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>واهب هر مایه که سودیش هست</p></div>
<div class="m2"><p>قبله هر سر که سجودیش هست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دایره ساز سپر آفتاب</p></div>
<div class="m2"><p>تیرگر باد و زره باف آب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عیب نهان دار هنر پروران</p></div>
<div class="m2"><p>عذر پذیرنده عذر آوران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آب زن آتش سودای عقل</p></div>
<div class="m2"><p>تاب ده دست تمنای عقل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صیقلی صاف ضمیران پاک</p></div>
<div class="m2"><p>صیرفی گنج پذیران خاک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سر شکن خامه تدبیرها</p></div>
<div class="m2"><p>خامه کش نامه تقصیرها</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ایمنی وقت هراسندگان</p></div>
<div class="m2"><p>روشنی حال شناسندگان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تازه کن جان به نسیم حیات</p></div>
<div class="m2"><p>کارگر کارگه کائنات</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ساخت چو صنعش قلم از «کاف » و «نون »</p></div>
<div class="m2"><p>شد به هزاران رقمش رهنمون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سطر نخست از ورق این سواد</p></div>
<div class="m2"><p>قدس نژادان تجرد نهاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مایه ایشان ز هیولا بری</p></div>
<div class="m2"><p>پایه ایشان ز صور برتری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جیب بقاشان ز فنا سوده نی</p></div>
<div class="m2"><p>دامنشان ز آب و گل آلوده نی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جنبش ایشان به هنرهای خاص</p></div>
<div class="m2"><p>از کشش چنگ طبیعت خلاص</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ناشده اقلیم دوام و ثبات</p></div>
<div class="m2"><p>تنگ بر ایشان ز حدود و جهات</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سطر دوم نه فلک لاجورد</p></div>
<div class="m2"><p>گرد یکی نقطه همه تیز گرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کوشش ایشان به پیام سروش</p></div>
<div class="m2"><p>گردش ایشان ز سر عقل و هوش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>برده به چوگان ارادت همه</p></div>
<div class="m2"><p>گوی ز میدان سعادت همه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بلکه به رقص آمده صوفی وشند</p></div>
<div class="m2"><p>دایم ازین رقص چو صوفی خوشند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>داده به هر دور ز ادوارشان</p></div>
<div class="m2"><p>نور دگر واهب انوارشان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سطر سوم نیست بجز چار حرف</p></div>
<div class="m2"><p>درج به هر چار رموز شگرف</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هر چه بود در خم طاق سپهر</p></div>
<div class="m2"><p>جمله ازین چار نموده ست چهر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قدرتش آن را به هم آمیخته ست</p></div>
<div class="m2"><p>هر دم ازان نقش نو انگیخته ست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نقش نخستین چه بود زان جماد</p></div>
<div class="m2"><p>کز حرکت بر در او ایستاد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کوه نشسته به مقام وقار</p></div>
<div class="m2"><p>یافته در قعده طاعت قرار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کان که بود خازن گنجینه اش</p></div>
<div class="m2"><p>ساخته پر لعل و گهر سینه اش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر گهری دیده رواجی دگر</p></div>
<div class="m2"><p>گشته فروزنده تاجی دگر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نوبت ازین پس به نبات آمده</p></div>
<div class="m2"><p>چابک و شیرین حرکات آمده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر زده از روزنه خاک سر</p></div>
<div class="m2"><p>برده به یکچند بر افلاک سر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چتر برافراخته از برگ و شاخ</p></div>
<div class="m2"><p>ساخته بر سایه نشین جا فراخ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کاه فشانده ز شکوفه درم</p></div>
<div class="m2"><p>گاه ز میوه شده خوان کرم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جنبش حیوان شده بعد از نبات</p></div>
<div class="m2"><p>گشته روان در گلش آب حیات</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از ره حس برده ز مقصود بوی</p></div>
<div class="m2"><p>پویه کنان کرده به مقصود روی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>با دل خواهنده ز جا خاسته</p></div>
<div class="m2"><p>رفته به هر جا که دلش خواسته</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خاتمه این همه هست آدمی</p></div>
<div class="m2"><p>یافته زو کار جهان محکمی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اول فکر آخر کار آمده</p></div>
<div class="m2"><p>فکر کن و کارگزار آمده</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بر کفش از عقل نهاده چراغ</p></div>
<div class="m2"><p>داده ز هر شمع و چراغش فراغ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کارکنان داده به عقل از حواس</p></div>
<div class="m2"><p>گشته به هر مقصد ازان ره شناس</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>باصره را داده به بینش نوید</p></div>
<div class="m2"><p>راه نموده به سیاه و سپید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سامعه را کرده به بیرون دو در</p></div>
<div class="m2"><p>تا ز چپ و راست نیوشد خبر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ذایقه را داده به روی زبان</p></div>
<div class="m2"><p>کام ز شیرینی و شور جهان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>لامسه را نقد نهاده به مشت</p></div>
<div class="m2"><p>گنج شناسایی نرم و درشت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شامه را از گل و ریحان باغ</p></div>
<div class="m2"><p>ساخته چون غنچه معطر دماغ</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بر تنش این پنج حس ظاهرند</p></div>
<div class="m2"><p>پنج دگر کارگر اندر سرند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کارکنان خردند این همه</p></div>
<div class="m2"><p>بهر خرد نامزدند این همه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تا به مددگاری ایشان خرد</p></div>
<div class="m2"><p>پی به شناسایی مبدع برد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چست ببندد کمر بندگی</p></div>
<div class="m2"><p>بندگیی مایه صد زندگی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>زندگیی مدت آن لایزال</p></div>
<div class="m2"><p>در کنف عاطفت ذوالجلال</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>جامی اگر زنده دلی بنده باش</p></div>
<div class="m2"><p>بنده این زنده پاینده باش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بندگیش زندگی آمد تمام</p></div>
<div class="m2"><p>زندگی این باشد و بس والسلام</p></div></div>