---
title: >-
    بخش ۲۲ - مقاله اول در آفرینش عالم که آینه جمال نمای اسماء و صفات آفریننده است سبحانه و تعالی
---
# بخش ۲۲ - مقاله اول در آفرینش عالم که آینه جمال نمای اسماء و صفات آفریننده است سبحانه و تعالی

<div class="b" id="bn1"><div class="m1"><p>شاهد خلوتگه غیبت از نخست</p></div>
<div class="m2"><p>بود پی جلوه کمر کرده چست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیینه غیب نما پیش داشت</p></div>
<div class="m2"><p>جلوه نمایی همه با خویش داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناظر و منظور همو بود و بس</p></div>
<div class="m2"><p>غیر وی این عرصه نپیمود کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمله یکی بود و دویی هیچ نه</p></div>
<div class="m2"><p>دعوی مایی و تویی هیچ نه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود قلم رسته ز زخم تراش</p></div>
<div class="m2"><p>لوح هم آسوده ز رنج خراش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرش قدم بر سر کرسی نداشت</p></div>
<div class="m2"><p>عقل سر نادره پرسی نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دایره چرخ به صد دخل و خرج</p></div>
<div class="m2"><p>بود به مطموره یک نقطه درج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلک فلک ناظم انجم نبود</p></div>
<div class="m2"><p>پشت زمین حاصل مردم نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نطفه آبا به مضیق جهات</p></div>
<div class="m2"><p>بود مصون از رحم امهات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود درین مهد فرو بسته دم</p></div>
<div class="m2"><p>طفل موالید به خواب عدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیده آن شاهد نابود بین</p></div>
<div class="m2"><p>معنی معدوم چو موجود بین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر چه همی دید در اجمال ذات</p></div>
<div class="m2"><p>حسن تفاصیل شئون و صفات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواست که در آینه های دگر</p></div>
<div class="m2"><p>بر نظر خویش شود جلوه گر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در خور هر یک ز صفات قدم</p></div>
<div class="m2"><p>روی دگر جلوه دهد لاجرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روضه جانبخش جهان آفرید</p></div>
<div class="m2"><p>باغچه کون و مکان آفرید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرد ز هر شاخ و گل و برگ و خار</p></div>
<div class="m2"><p>جلوه او حسن دگر آشکار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سرو نشان از قد رعناش داد</p></div>
<div class="m2"><p>گل خبر از طلعت زیباش داد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غنچه سخن از شکرش کرد ساز</p></div>
<div class="m2"><p>قفل ز درج گهرش کرد باز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سبزه به گل غالیه تر سرشت</p></div>
<div class="m2"><p>پیش گل اوصاف خط او نوشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شد هوس طره او باد را</p></div>
<div class="m2"><p>بست گره طره شمشاد را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نرگس جماش به آن چشم مست</p></div>
<div class="m2"><p>زد ره مستان صبوحی پرست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فاخته با طوق تمنای سرو</p></div>
<div class="m2"><p>زد نفس شوق ز بالای سرو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بلبل نالنده به دیدار گل</p></div>
<div class="m2"><p>پرده گشا گشت ز اسرار گل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کبک دری پایچه ها بر زده</p></div>
<div class="m2"><p>زد به سر سبزه قدم سرزده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قمری بنهاده به شمشاد دل</p></div>
<div class="m2"><p>سوخت به داغ غم او شاد دل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرغ سحر ساخت به ناز و عتاب</p></div>
<div class="m2"><p>در نظر نرگس بسیار خواب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حسن ز هر جا که زد القصه سر</p></div>
<div class="m2"><p>عشق شد از جای دگر جلوه گر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حسن ز هر چهره که رخ بر فروخت</p></div>
<div class="m2"><p>عشق ازان شعله دلی را بسوخت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حسن به هر طره که آرام یافت</p></div>
<div class="m2"><p>عشق دلی آمده در دام یافت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حسن ز هر لب که شکر خنده کرد</p></div>
<div class="m2"><p>عشق دلی را به غمش بنده کرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حسن جز از عشق نگیرد غذی</p></div>
<div class="m2"><p>عشق هم از وی نگریزد بلی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قالب و جانند به هم حسن و عشق</p></div>
<div class="m2"><p>گوهر و کانند به هم حسن و عشق</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از ازل این هر دو به هم بوده اند</p></div>
<div class="m2"><p>جز به هم این راه نپیموده اند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هستی ما هست ز پیوندشان</p></div>
<div class="m2"><p>نیست گشاد همه جز بندشان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حسن و کس از عشق گرفتار نی</p></div>
<div class="m2"><p>جنس نفیس است و خریدار نی</p></div></div>