---
title: >-
    بخش ۲۹ - حکایت کشیدن پیکان از تیر راست رو کیش ولایت کرم الله تعالی وجهه در وقتی که از کشاکش کمان مجاهده بر نشان مشاهده افتاده بود
---
# بخش ۲۹ - حکایت کشیدن پیکان از تیر راست رو کیش ولایت کرم الله تعالی وجهه در وقتی که از کشاکش کمان مجاهده بر نشان مشاهده افتاده بود

<div class="b" id="bn1"><div class="m1"><p>شیر خدا شاه ولایت علی</p></div>
<div class="m2"><p>صیقلی شرک خفی و جلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز احد چون صف هیجا گرفت</p></div>
<div class="m2"><p>تیر مخالف به تنش جا گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنچه پیکان به گل او نهفت</p></div>
<div class="m2"><p>صد گل محنت ز گل او شکفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی عبادت سوی محراب کرد</p></div>
<div class="m2"><p>پشت به درد سر اصحاب کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خنجر الماس چو بید آختند</p></div>
<div class="m2"><p>چاک به تن چون گلش انداختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرقه به خون غنچه زنگارگون</p></div>
<div class="m2"><p>آمد ازان گلبن احسان برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل گل خونش به مصلا چکید</p></div>
<div class="m2"><p>گفت چو فارغ ز نماز آن بدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه گل چیست ته پای من</p></div>
<div class="m2"><p>ساخته گلزار مصلای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صورت حالش چو نمودند باز</p></div>
<div class="m2"><p>گفت که سوگند به دانای راز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کز الم تیغ ندارم خبر</p></div>
<div class="m2"><p>گر چه ز من نیست خبردارتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طایر من سدره نشین شد چه باک</p></div>
<div class="m2"><p>گر شودم تن چو قفس چاک چاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جامی از آلایش تن پاک شو</p></div>
<div class="m2"><p>در قدم پاکروان خاک شو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باشد ازان خاک به گردی رسی</p></div>
<div class="m2"><p>گرد شکافی و به مردی رسی</p></div></div>