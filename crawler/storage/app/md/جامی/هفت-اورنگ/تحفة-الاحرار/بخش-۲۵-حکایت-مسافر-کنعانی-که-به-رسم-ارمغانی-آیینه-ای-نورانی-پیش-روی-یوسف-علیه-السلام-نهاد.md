---
title: >-
    بخش ۲۵ - حکایت مسافر کنعانی که به رسم ارمغانی آیینه ای نورانی پیش روی یوسف علیه السلام نهاد
---
# بخش ۲۵ - حکایت مسافر کنعانی که به رسم ارمغانی آیینه ای نورانی پیش روی یوسف علیه السلام نهاد

<div class="b" id="bn1"><div class="m1"><p>یوسف کنعان چو به مصر آرمید</p></div>
<div class="m2"><p>صیت وی از مصر به کنعان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود در آن غمکده یک دوستش</p></div>
<div class="m2"><p>پر شده مغز وفا پوستش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ره به سوی مصر جمالش سپرد</p></div>
<div class="m2"><p>آینه ای بهر ره آورد برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یوسف ازو کرد نهانی سؤال</p></div>
<div class="m2"><p>کای شده محرم به حریم وصال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در طلبم رنج سفر برده ای</p></div>
<div class="m2"><p>زین سفرم تحفه چه آورده ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت به هر سو نظر انداختم</p></div>
<div class="m2"><p>هیچ متاعی چو تو نشناختم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آینه ای بهر تو کردم به دست</p></div>
<div class="m2"><p>پاک ز هر گونه غباری که هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چو به آن دیده خود واکنی</p></div>
<div class="m2"><p>طلعت زیبات تماشا کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تحفه ای افزون ز لقای تو چیست</p></div>
<div class="m2"><p>گر روی از جای به جای تو کیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست جهان را به صفای تو کس</p></div>
<div class="m2"><p>غافل ازین تیره دلانند و بس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جامی ازین تیره دلان پیش باش</p></div>
<div class="m2"><p>صیقلی آینه خویش باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا چو بتابی رخ ازین تیره جای</p></div>
<div class="m2"><p>یوسف غیب تو شود رو نمای</p></div></div>