---
title: >-
    بخش ۱ - آغاز
---
# بخش ۱ - آغاز

<div class="b" id="bn1"><div class="m1"><p>قبله همت خدای شناس</p></div>
<div class="m2"><p>هست بر نعمت خدای سپاس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاصه بر نعمتی که دیر بقاست</p></div>
<div class="m2"><p>در جهان تا جهان به جاست به جاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چیست آن نکته های هوشیاران</p></div>
<div class="m2"><p>نظم و نثر بدیع گفتاران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست شغلی پس از سپاس خدای</p></div>
<div class="m2"><p>بهتر از نعت خواجه دو سرای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن که بی پرده در نشیمن راز</p></div>
<div class="m2"><p>سخن از وی به پایه اعجاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خوش آن صافی دل انصاف جوی</p></div>
<div class="m2"><p>کش بود در شیوه انصاف روی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جهان بر هر چه اندازد نظر</p></div>
<div class="m2"><p>عیب را بگذارد و بیند هنر</p></div></div>