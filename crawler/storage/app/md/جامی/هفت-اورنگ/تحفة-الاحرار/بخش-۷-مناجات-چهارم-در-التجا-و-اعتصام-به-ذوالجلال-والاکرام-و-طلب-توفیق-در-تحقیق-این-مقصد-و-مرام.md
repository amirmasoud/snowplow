---
title: >-
    بخش ۷ - مناجات چهارم در التجا و اعتصام به ذوالجلال والاکرام و طلب توفیق در تحقیق این مقصد و مرام
---
# بخش ۷ - مناجات چهارم در التجا و اعتصام به ذوالجلال والاکرام و طلب توفیق در تحقیق این مقصد و مرام

<div class="b" id="bn1"><div class="m1"><p>ای ز کرم چاره گر کارها</p></div>
<div class="m2"><p>مرهم راحت نه آزارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشنی دیده بینندگان</p></div>
<div class="m2"><p>پردگی پرده نشینندگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقده گشاینده هر مشکلی</p></div>
<div class="m2"><p>قبله نماینده هر مقبلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توشه نه گوشه نشینان پاک</p></div>
<div class="m2"><p>خوشه ده دانه فشانان خاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازوی تأیید هنرپیشگان</p></div>
<div class="m2"><p>قبله توحید یک اندیشگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شانه زن زلف عروس بهار</p></div>
<div class="m2"><p>مرسله بند گلوی شاخسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نم لطفی که هوا ریخته</p></div>
<div class="m2"><p>عقد در از گوش گل آویخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دل محرم ز جمالت چراغ</p></div>
<div class="m2"><p>سیه محروم ز تو داغ داغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طاعت تو نغزترین پیشه ای</p></div>
<div class="m2"><p>فکرت تو مغز هر اندیشه ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پای طلب راه گذار از تو یافت</p></div>
<div class="m2"><p>دست توان قوت کار از تو یافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بلکه تویی کارگر راستین</p></div>
<div class="m2"><p>دست همه دست تو را آستین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا نکنی تو نتوانیم ما</p></div>
<div class="m2"><p>گر ندهی تو چه ستانیم ما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست درین کارگاه گیر و دار</p></div>
<div class="m2"><p>جز تو کسی کاید ازو هیچ کار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روی عبادت به تو آریم و بس</p></div>
<div class="m2"><p>چشم عنایت ز تو داریم و بس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در کف ما مشعل توفیق نه</p></div>
<div class="m2"><p>ره به نهانخانه تحقیق ده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اهل دل از نظم چو محفل نهند</p></div>
<div class="m2"><p>باده راز از قدح دل دهند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رشحی ازان باده به جامی رسان</p></div>
<div class="m2"><p>رونق نظمش به نظامی رسان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پست چو خاکست بریز از نوش</p></div>
<div class="m2"><p>جرعه ای از بزمگه خسروش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قافیه آنجا که نظامی نواست</p></div>
<div class="m2"><p>بر گذر قافیه جامی سزاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر سر خسرو که بلند افسر است</p></div>
<div class="m2"><p>از کف درویش گلی در خور است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این نفس از همت دون من است</p></div>
<div class="m2"><p>وین هوس از طبع زبون من است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ورنه از آنجا که کرم های توست</p></div>
<div class="m2"><p>کی بودم رشته امید سست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صد چو نظامی و چو خسرو هزار</p></div>
<div class="m2"><p>شایدم از جام سخن جرعه خوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر همه در شعر بلندیم بخش</p></div>
<div class="m2"><p>مرتبه شرع پسندیم بخش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پایه نظمم ز همه بگذران</p></div>
<div class="m2"><p>خاصه به نعت سر پیغمبران</p></div></div>