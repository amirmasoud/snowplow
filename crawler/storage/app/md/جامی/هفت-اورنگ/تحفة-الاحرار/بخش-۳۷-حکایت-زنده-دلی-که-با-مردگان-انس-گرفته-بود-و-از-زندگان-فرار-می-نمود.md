---
title: >-
    بخش ۳۷ - حکایت زنده دلی که با مردگان انس گرفته بود و از زندگان فرار می نمود
---
# بخش ۳۷ - حکایت زنده دلی که با مردگان انس گرفته بود و از زندگان فرار می نمود

<div class="b" id="bn1"><div class="m1"><p>زنده دلی از صف افسردگان</p></div>
<div class="m2"><p>رفت به همسایگی مردگان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پشت ملامت به عمارات کرد</p></div>
<div class="m2"><p>روی ارادت به مزارات کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حرف فنا خواند ز هر لوح خاک</p></div>
<div class="m2"><p>روح بقا جست ز هر روح پاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشتی ازین سگ منشان تیز تگ</p></div>
<div class="m2"><p>همچو تگ آهوی وحشی ز سگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کارشناسی پی تفتیش حال</p></div>
<div class="m2"><p>کرد ازو بر سر راهی سؤال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کین همه از زنده رمیدن چراست</p></div>
<div class="m2"><p>رخت سوی مرده کشیدن چراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت بلندان به مغاک اندراند</p></div>
<div class="m2"><p>پاک نهادان ته خاک اندراند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرده دلانند به روی زمین</p></div>
<div class="m2"><p>بهر چه با مرده شوم همنشین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همدمی مرده دهد مردگی</p></div>
<div class="m2"><p>صحبت افسرده دل افسردگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیر گل آنان که پراکنده اند</p></div>
<div class="m2"><p>گر چه به تن مرده به جان زنده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرده دلی بود مرا پیش ازین</p></div>
<div class="m2"><p>بسته هر چون و چرا پیش ازین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زنده شدم از نظر پاکشان</p></div>
<div class="m2"><p>آب حیاتست مرا خاکشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جامی ازین مرده دلان گوشه گیر</p></div>
<div class="m2"><p>گوش به خود دار و ز خود توشه گیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر چه درین دایره بیرون توست</p></div>
<div class="m2"><p>گام سعایت زده در خون توست</p></div></div>