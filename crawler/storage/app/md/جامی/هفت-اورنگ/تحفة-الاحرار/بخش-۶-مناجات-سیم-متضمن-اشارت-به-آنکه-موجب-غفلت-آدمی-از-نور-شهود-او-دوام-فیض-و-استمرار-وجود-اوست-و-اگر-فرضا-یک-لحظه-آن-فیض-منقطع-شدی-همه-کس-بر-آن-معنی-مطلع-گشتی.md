---
title: >-
    بخش ۶ - مناجات سیم متضمن اشارت به آنکه موجب غفلت آدمی از نور شهود او دوام فیض و استمرار وجود اوست و اگر فرضا یک لحظه آن فیض منقطع شدی همه کس بر آن معنی مطلع گشتی
---
# بخش ۶ - مناجات سیم متضمن اشارت به آنکه موجب غفلت آدمی از نور شهود او دوام فیض و استمرار وجود اوست و اگر فرضا یک لحظه آن فیض منقطع شدی همه کس بر آن معنی مطلع گشتی

<div class="b" id="bn1"><div class="m1"><p>ای ز وجود تو نمود همه</p></div>
<div class="m2"><p>جود تو سرمایه بود همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مبدع نوی و کهن ما تویی</p></div>
<div class="m2"><p>هست کن و نیست کن ما تویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارگرانند درین کارگاه</p></div>
<div class="m2"><p>ز آتش لا سوخته در لااله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست ز لا مخلصی الا تو را</p></div>
<div class="m2"><p>حکم تبارک و تعالی تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیض نوالت چو پیاپی رسد</p></div>
<div class="m2"><p>کس به شناسایی آن کی رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خم این دایره هزل و جد</p></div>
<div class="m2"><p>ضد متبین نشود جز به ضد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از عدم انوار قدم بازگیر</p></div>
<div class="m2"><p>از رقم لوح قلم بازگیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سبحه بکش از کف روحانیان</p></div>
<div class="m2"><p>رخنه فکن در صف نورانیان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سر کرسی بفکن عرش را</p></div>
<div class="m2"><p>خوان پی کرسی نهیش فرش را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پایه کرسی به زمین بر فرو</p></div>
<div class="m2"><p>گرد مذلت بنشین گو بر او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زلزله در گنبد اخضر فکن</p></div>
<div class="m2"><p>یک دو سه قاروره به هم در شکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منطقه بگشا ز میان فلک</p></div>
<div class="m2"><p>تیر بیفکن ز کمان فلک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بازگشا عقد ثریا ز هم</p></div>
<div class="m2"><p>ساز جدا پیکر جوزا ز هم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گاو چرا خورده این مرغزار</p></div>
<div class="m2"><p>شیر جهان خوار فنا را سپار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قطع کن از داس اجل خوشه اش</p></div>
<div class="m2"><p>ساز پی راه فنا توشه اش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باغ عناصر که زمینش خوش است</p></div>
<div class="m2"><p>آب گوارنده هوا دلکش است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هست گلی رسته در او آتشین</p></div>
<div class="m2"><p>غنچه آن گلشن چرخ برین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یار بر این باغ ز انجم تگرگ</p></div>
<div class="m2"><p>در هم و بر هم شکنش شاخ و برگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خاصترین میوه آن کادمیست</p></div>
<div class="m2"><p>لذتش از چاشنی محرمیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پخته و خامش همه بر خاک ریز</p></div>
<div class="m2"><p>بر سرش از باد اجل خاک بیز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا همه دانند که صانع تویی</p></div>
<div class="m2"><p>مبدع این جمله بدایع تویی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هستی و پایندگی از توست و بس</p></div>
<div class="m2"><p>مردگی و زندگی از توست و بس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جز تو کسی نیست به ملک قدم</p></div>
<div class="m2"><p>کز لمن الملک فرازد علم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جامی اگر نیست ز بخت نژند</p></div>
<div class="m2"><p>چون علم خسرویش سربلند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از علم فقر بلندیش ده</p></div>
<div class="m2"><p>زیر علم سایه پسندیش ده</p></div></div>