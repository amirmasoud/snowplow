---
title: >-
    بخش ۲۲ - عقد پنجم در بیان یکتایی و برهان بی همتایی حق سبحانه که در بیان و برهان همه زبان آوران یکسانند و همه بی زبانان یکزبان
---
# بخش ۲۲ - عقد پنجم در بیان یکتایی و برهان بی همتایی حق سبحانه که در بیان و برهان همه زبان آوران یکسانند و همه بی زبانان یکزبان

<div class="b" id="bn1"><div class="m1"><p>ای درین بتکده طبع فریب</p></div>
<div class="m2"><p>برده غوغای بتان از تو شکیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبع را بند خرد بر پا نه</p></div>
<div class="m2"><p>پای اندیشه درین غوغا نه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنگر این انجم و مهر و مه را</p></div>
<div class="m2"><p>بت ره گشته خلیل الله را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یافتندی به دلش راه قبول</p></div>
<div class="m2"><p>گرنه بشکستیشان سنگ افول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگ بر بتکده آزر زن</p></div>
<div class="m2"><p>در جهان صیت خلیلی افکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیز کن خنجر لابر سر لات</p></div>
<div class="m2"><p>ببر از لات منی را ز منات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاج عزت ز سر عزی کش</p></div>
<div class="m2"><p>رخت طاعت به در مولاکش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ثنوی اهرمن و یزدان گوی</p></div>
<div class="m2"><p>تافت از انجمن ایمان روی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عیسوی شد به سه گویی افزون</p></div>
<div class="m2"><p>خیمه از ساحت دین زد بیرون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو به صد بت چه به صد بلکه هزار</p></div>
<div class="m2"><p>بلکه بیرون ز ترازوی شمار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کرده ای روی ولی هر نفسی</p></div>
<div class="m2"><p>می پزی در ره ایمان هوسی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گاه گویی که من آن دریایم</p></div>
<div class="m2"><p>که جهان را به گهر آرایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل صدف گوهر توحیدم در</p></div>
<div class="m2"><p>گوش دهر از در توحیدم پر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گاه گویی که من آن گلزارم</p></div>
<div class="m2"><p>که دهد بر گل عرفان خارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر که یابد ز گل من بویی</p></div>
<div class="m2"><p>بوی عرفان دهد از هر مویی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به زبان می زنی این لاف ولی</p></div>
<div class="m2"><p>نیست بر موجب اینت عملی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر چه تقریر تو ترتیب کند</p></div>
<div class="m2"><p>صورت حال تو تکذیب کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر چه یابد ز مقال تو فروغ</p></div>
<div class="m2"><p>سازدش حال تو مطعون به دروغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نیست این راستی و راستروی</p></div>
<div class="m2"><p>که چنان راست که گویی نشوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>راه رو پس سخن راه بگوی</p></div>
<div class="m2"><p>آنچه خواهی بشو آنگاه بگوی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دل نکرده ز دورویی صافی</p></div>
<div class="m2"><p>چه ز یکرویی وحدت لافی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دیده بر شاهد وحدت بگشای</p></div>
<div class="m2"><p>وز دورویی و دو گویی باز آی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سهل باشد که ز ماهی تا ماه</p></div>
<div class="m2"><p>بر تو باشند درین نکته گواه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر چه قولت دم اقرار زند</p></div>
<div class="m2"><p>فعل تو نعره انکار زند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از محیط فلک و اوج سماک</p></div>
<div class="m2"><p>تا حضیض سمک و مرکز خاک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بین مرتب شده اجرام که هست</p></div>
<div class="m2"><p>وین همه جنبش و آرام که هست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شکل و ترتیب فلک بر یک حال</p></div>
<div class="m2"><p>دور سیر همه بر یک منوال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی از صورت خود ناگشته</p></div>
<div class="m2"><p>یکی از گردش خود نگذشته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>متفق وضع دوایر با هم</p></div>
<div class="m2"><p>منتظم سلک عناصر با هم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه بر یک صفت و یک آیین</p></div>
<div class="m2"><p>هیچ زیرین نشده بالایین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سال و مه روز و شب و شام و سحر</p></div>
<div class="m2"><p>یک به یک گرم رو و تیز گذر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا به آمد شد خود در گروند</p></div>
<div class="m2"><p>بر یکی قاعده آیند و روند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چار فصلی که به هر سال در است</p></div>
<div class="m2"><p>به همین رسم و روش رهسپر است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>این موالید سه گانه که جهان</p></div>
<div class="m2"><p>پر از آنهاست چه پیدا چه نهان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نوع نوعش نه کم آید نه فزون</p></div>
<div class="m2"><p>از نهانخانه ابداع برون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کارگاهی به چنین ضبط و نسق</p></div>
<div class="m2"><p>کار یک کارگزارست الحق</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کشور آباد نگردد به دو شاه</p></div>
<div class="m2"><p>بشکند از دو سپهدار سپاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از دو بانو چو شود آشفته</p></div>
<div class="m2"><p>خانه امید مدارش رفته</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رنج طفل است ادای دو ادیب</p></div>
<div class="m2"><p>مرگ رنجور دوای دو طبیب</p></div></div>