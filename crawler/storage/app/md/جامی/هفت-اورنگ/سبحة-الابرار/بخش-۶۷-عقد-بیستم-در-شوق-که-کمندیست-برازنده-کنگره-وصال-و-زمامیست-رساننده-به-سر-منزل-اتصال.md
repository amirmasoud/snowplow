---
title: >-
    بخش ۶۷ - عقد بیستم در شوق که کمندیست برازنده کنگره وصال و زمامیست رساننده به سر منزل اتصال
---
# بخش ۶۷ - عقد بیستم در شوق که کمندیست برازنده کنگره وصال و زمامیست رساننده به سر منزل اتصال

<div class="b" id="bn1"><div class="m1"><p>ای دلت را به کف شوق زمام</p></div>
<div class="m2"><p>سیر عاشق شود از شوق تمام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق اگر قاید راهت نشود</p></div>
<div class="m2"><p>کعبه وصل پناهت نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوق قلاب دل دوران است</p></div>
<div class="m2"><p>جاذب خاطر مهجوران است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق کوتاه کند راه دراز</p></div>
<div class="m2"><p>بر رخ مرد ببندد در آز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوق برقیست نشیمن افروز</p></div>
<div class="m2"><p>مانع ره شده را خرمن سوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوه هر رنج که در راه بود</p></div>
<div class="m2"><p>پیش مشتاق کم از کاه بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون زند شعله شوق از دل تاب</p></div>
<div class="m2"><p>نشود کشته به صد دریا آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چه تسکین ویت دسترس است</p></div>
<div class="m2"><p>آن نه شوق است هوا و هوس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هوس گام طلب نتوان زد</p></div>
<div class="m2"><p>خیمه در کوی طرب نتوان زد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هوس آیین هوسناک بود</p></div>
<div class="m2"><p>جان عاشق ز هوس پاک بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هوس ابریست ز باران خالی</p></div>
<div class="m2"><p>سایه اش مایه بی اقبالیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه ازو کشت امل آب خورد</p></div>
<div class="m2"><p>نه ز تن تب نه ز دل تاب برد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواجه دل بسته در اسباب جهان</p></div>
<div class="m2"><p>کشتی افکنده به گرداب جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خفته بر نطع امل مست غرور</p></div>
<div class="m2"><p>طبعش ازنفس و هوا پر شر و شور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چشمش از طلعت شاهد روشن</p></div>
<div class="m2"><p>گشته در کاخ بطالت روزن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل او پردگی پرده آز</p></div>
<div class="m2"><p>مانده در پرده ازو چهره راز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دستش از بازوی خذلان رنجه</p></div>
<div class="m2"><p>زده در دامن حرمان پنجه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پای او رهسپر کوی خطا</p></div>
<div class="m2"><p>گام پیمای پی نفس و هوا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>معده غارتگر هر پخته و خام</p></div>
<div class="m2"><p>خورده در هم چه حلال و چه حرام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گوشش از قول نصیحتگر کر</p></div>
<div class="m2"><p>رام با زمزمه رامشگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ژاژخایی هنر دندانش</p></div>
<div class="m2"><p>هزل دستور لب خندانش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شبش آبستن هر فسق و فساد</p></div>
<div class="m2"><p>روز او پرده در صدق و سداد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با چنین فعل و صفت گر ناگاه</p></div>
<div class="m2"><p>بشنود خارقی از اهل الله</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که فلان پیر جهان پیما گشت</p></div>
<div class="m2"><p>قدم خشک ز دریا بگذشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وان دگر پرده عادت بدرید</p></div>
<div class="m2"><p>کرد پرواز و چو مرغان بپرید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وان دگر کرد سوی کوه نظر</p></div>
<div class="m2"><p>کوه سنگ از نظر او شد زر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وان دگر زد به کرامت قدمی</p></div>
<div class="m2"><p>کرد طی بادیه ای را به دمی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وان دگر لشکر همت انگیخت</p></div>
<div class="m2"><p>لشکری را به دعایی خون ریخت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زین مقالات فتد در دل او</p></div>
<div class="m2"><p>کین مقامات شود حاصل او</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چند روزی ره مردان گیرد</p></div>
<div class="m2"><p>شیوه راهنوردان گیرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>لیکن آن شیوه از صدق تهی</p></div>
<div class="m2"><p>ندهد بهره بجز دل سببی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صدق باید که بود شوق فزای</p></div>
<div class="m2"><p>تا به مقصود شود راهنمای</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شوق صادق چو کشد محمل مرد</p></div>
<div class="m2"><p>کعبه وصل کند منزل مرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هیچ مانع نگذارد در راه</p></div>
<div class="m2"><p>تا در آن کعبه کند منزلگاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بلکه پندار وجود ار به مثل</p></div>
<div class="m2"><p>افکند در ره مقصود خلل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کشتی آساش به هم در شکند</p></div>
<div class="m2"><p>رخت هستیش به دریا فکند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون در آن موج ز خود شوید دست</p></div>
<div class="m2"><p>افتدش ماهی مقصود به شست</p></div></div>