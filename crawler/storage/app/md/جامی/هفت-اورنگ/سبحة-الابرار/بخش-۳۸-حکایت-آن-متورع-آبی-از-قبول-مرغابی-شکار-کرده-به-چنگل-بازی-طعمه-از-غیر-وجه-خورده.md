---
title: >-
    بخش ۳۸ - حکایت آن متورع آبی از قبول مرغابی شکار کرده به چنگل بازی طعمه از غیر وجه خورده
---
# بخش ۳۸ - حکایت آن متورع آبی از قبول مرغابی شکار کرده به چنگل بازی طعمه از غیر وجه خورده

<div class="b" id="bn1"><div class="m1"><p>خسروی عاقبت اندیشی کرد</p></div>
<div class="m2"><p>روی در قبله درویشی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با بزرگی که در آن کشور بود</p></div>
<div class="m2"><p>بر سر اهل صفا سرور بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوبتی چند به هم بنشستند</p></div>
<div class="m2"><p>عقد پیروی و مریدی بستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برد صد تحفه خدمت سوی پیر</p></div>
<div class="m2"><p>هیچ ازو پیر نشد تحفه پذیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی از بالش زین مسند ساخت</p></div>
<div class="m2"><p>قاصد صید سوی صحرا تاخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز را دیده بینا بگشاد</p></div>
<div class="m2"><p>کله از سر گره از پا بگشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد ازان باز رها کرده ز قید</p></div>
<div class="m2"><p>متعاقب دو سه مرغابی صید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صید را از خم فتراک آویخت</p></div>
<div class="m2"><p>جانب پیر جنیبت انگیخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بندگی کرد که ای خاص خدای</p></div>
<div class="m2"><p>لقمه پاک است به این روزه گشای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست ازین طعمه درین منزلگاه</p></div>
<div class="m2"><p>پنجه کسب خلایق کوتاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیر خندید که ای پاک نهاد</p></div>
<div class="m2"><p>نامت از لوح بقا پاک مباد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جره بازت که شکاری فکن است</p></div>
<div class="m2"><p>جره از جوزه هر بیوه زن است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رخشت این ره چو به پایان برده ست</p></div>
<div class="m2"><p>جو ز توزیع گدایان خورده ست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیروی بازوی باز اندازت</p></div>
<div class="m2"><p>باشد از دست ستم پردازت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چشمه کز سنگ تراود پاک است</p></div>
<div class="m2"><p>تیره از رهگذر گلناک است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که آلوده به گل رهگذرش</p></div>
<div class="m2"><p>کی ز گل پاک بود آبخورش</p></div></div>