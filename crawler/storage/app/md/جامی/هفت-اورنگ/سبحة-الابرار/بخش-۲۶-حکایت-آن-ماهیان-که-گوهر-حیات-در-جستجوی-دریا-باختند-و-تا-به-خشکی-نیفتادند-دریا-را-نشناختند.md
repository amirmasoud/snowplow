---
title: >-
    بخش ۲۶ - حکایت آن ماهیان که گوهر حیات در جستجوی دریا باختند و تا به خشکی نیفتادند دریا را نشناختند
---
# بخش ۲۶ - حکایت آن ماهیان که گوهر حیات در جستجوی دریا باختند و تا به خشکی نیفتادند دریا را نشناختند

<div class="b" id="bn1"><div class="m1"><p>داشت غوکی به لب بحر وطن</p></div>
<div class="m2"><p>دایم از بحر همی راند سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز و شب قصه دریا گفتی</p></div>
<div class="m2"><p>گوهر مدحت دریا سفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی از بحر پدید آمده ایم</p></div>
<div class="m2"><p>زو درین گفت و شنید آمده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ازو گوهر دانایی یافت</p></div>
<div class="m2"><p>تن ازو دست توانایی یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا می نگرم اوست همه</p></div>
<div class="m2"><p>هر طرف می گذرم اوست همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماهیی چند رسیدند آنجا</p></div>
<div class="m2"><p>وز وی آن قصه شنیدند آنجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق بحر از دلشان سر بر زد</p></div>
<div class="m2"><p>آتش شوق به جانشان در زد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پای تا سر همگی پای شدند</p></div>
<div class="m2"><p>در طلب مرحله پیمای شدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برگرفتند تک و پوی نیاز</p></div>
<div class="m2"><p>بحر جویان چه نشیب و چه فراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گاه در تگ چو صدف جا کردند</p></div>
<div class="m2"><p>گه چو خس رو به کنار آوردند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه نشان یافت شد از بحر به نام</p></div>
<div class="m2"><p>می نهادند به نومیدی گام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از قضا صیدگری دام نهاد</p></div>
<div class="m2"><p>راهشان بر گذر دام فتاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکسر آن جمع به دام افتادند</p></div>
<div class="m2"><p>تن به جان دادن خود در دادند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صیدگر برد سوی ساحلشان</p></div>
<div class="m2"><p>ساخت بر خشک زمین منزلشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چند تن کوشش و جنبش کردند</p></div>
<div class="m2"><p>خز خزان راه به بحر آوردند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیم مرده چو رسیدند به بحر</p></div>
<div class="m2"><p>جام مقصود کشیدند به بحر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دانش و بینششان روی نمود</p></div>
<div class="m2"><p>کانچه می داد نشان غوک چه بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زنده در بحر شهود آسودند</p></div>
<div class="m2"><p>غرقه بودند در آن تا بودند</p></div></div>