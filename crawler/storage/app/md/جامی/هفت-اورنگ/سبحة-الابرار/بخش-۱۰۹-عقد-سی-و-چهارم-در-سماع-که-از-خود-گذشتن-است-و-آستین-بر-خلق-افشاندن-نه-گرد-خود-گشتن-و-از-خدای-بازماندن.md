---
title: >-
    بخش ۱۰۹ - عقد سی و چهارم در سماع که از خود گذشتن است و آستین بر خلق افشاندن نه گرد خود گشتن و از خدای بازماندن
---
# بخش ۱۰۹ - عقد سی و چهارم در سماع که از خود گذشتن است و آستین بر خلق افشاندن نه گرد خود گشتن و از خدای بازماندن

<div class="b" id="bn1"><div class="m1"><p>ای درین خوابگه بی خبران</p></div>
<div class="m2"><p>بی خبر خفته چو کوران و کران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر برآور که درین پرده سرای</p></div>
<div class="m2"><p>می رسد بانگ سرود از همه جای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل از منبر گل نغمه نواز</p></div>
<div class="m2"><p>قمری از سرو سهی زمزمه ساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فاخته چنبر دف کرده ز طوق</p></div>
<div class="m2"><p>از نوا گشته جلاجل زن شوق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لحن قوال شده صومعه گیر</p></div>
<div class="m2"><p>نه مرید از دم او جسته نه پیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطرب از مصطبه دردکشان</p></div>
<div class="m2"><p>داده از منزل مقصود نشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باد نی بر دل مستان صبوح</p></div>
<div class="m2"><p>فتح کرده همه ابواب فتوح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عود خاموش ز یک مالش گوش</p></div>
<div class="m2"><p>کودک آساست برآورده خروش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنگ با عقل ره جنگ زده</p></div>
<div class="m2"><p>راه صد دل به یک آهنگ زده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تایب کاسته شکسته ز شراب</p></div>
<div class="m2"><p>به یکی کاسه شده مست رباب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیر راهب شده ناقوس زنان</p></div>
<div class="m2"><p>نوبتی مقرعه بر کوس زنان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بانگ برداشته مرغ سحری</p></div>
<div class="m2"><p>کرده بر خفته دلان پرده دری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مؤذن از راحت شب دل کنده</p></div>
<div class="m2"><p>کرده صد مرده به یا حی زنده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرخ در گرد ازین بانگ و نوا</p></div>
<div class="m2"><p>کوه در رقص ازین صوت و صدا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرگز از جای نمی خیزی تو</p></div>
<div class="m2"><p>الله الله چه گران چیزی تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هیچ دانی چه گران باشد فیل</p></div>
<div class="m2"><p>پشتش از پشته ارزیز ثقیل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زیر آن بار گران جان داده</p></div>
<div class="m2"><p>پشته بر پشت ز پای افتاده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر بسنجد خردش با تو به هم</p></div>
<div class="m2"><p>یابدش از پشه بسیاری کم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ساعتی ترک گران جانی کن</p></div>
<div class="m2"><p>شوق را سلسله جنبانی کن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگسل از پای خود این لنگر گل</p></div>
<div class="m2"><p>گام زن شو به سوی کشور دل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آستین بر سر عالم افشان</p></div>
<div class="m2"><p>دامن از طینت آدم افشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سنگ بر شیشه ناموس انداز</p></div>
<div class="m2"><p>چاک در خرقه سالوس انداز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر چه بند است بکش از وی پای</p></div>
<div class="m2"><p>هر چه حشو است تهی کن زان جای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نغمه جان شنو از چنگ سماع</p></div>
<div class="m2"><p>بجه از جسم به آهنگ سماع</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه ذرات جهان در رقصند</p></div>
<div class="m2"><p>رو نهاده به کمال از نقصند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو هم از نقص قدم نه به کمال</p></div>
<div class="m2"><p>دامن افشان ز سر جاه و جلال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زین سرودند بهایم هایم</p></div>
<div class="m2"><p>تو ازین گونه غنایم نایم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خواب بگذار که بی خوابی به</p></div>
<div class="m2"><p>دیده را سرمه بیخوابی ده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حیف باشد که به آن جثه شتر</p></div>
<div class="m2"><p>باشد از لذت این زمزمه پر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو بدین دبدبه انسانی</p></div>
<div class="m2"><p>زان صدا چون دبه خالی مانی</p></div></div>