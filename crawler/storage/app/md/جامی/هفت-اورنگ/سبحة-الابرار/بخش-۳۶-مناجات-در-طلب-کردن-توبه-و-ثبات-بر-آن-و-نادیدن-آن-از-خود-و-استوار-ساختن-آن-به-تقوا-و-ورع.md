---
title: >-
    بخش ۳۶ - مناجات در طلب کردن توبه و ثبات بر آن و نادیدن آن از خود و استوار ساختن آن به تقوا و ورع
---
# بخش ۳۶ - مناجات در طلب کردن توبه و ثبات بر آن و نادیدن آن از خود و استوار ساختن آن به تقوا و ورع

<div class="b" id="bn1"><div class="m1"><p>ای ز هر رو همه را روی به تو</p></div>
<div class="m2"><p>روی هر ذره ز هر سوی به تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار ما چیست گنه ورزیدن</p></div>
<div class="m2"><p>عادت تو گنه آمرزیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توبه از بنده بود سست نهاد</p></div>
<div class="m2"><p>توبه آنست کش از توست گشاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار نه بار فکن هر دو تویی</p></div>
<div class="m2"><p>توبه ده توبه شکن هر دو تویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که شد گمشده تیه گناه</p></div>
<div class="m2"><p>جز به توبه نشود روی به راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامی گمشده را بخش نجات</p></div>
<div class="m2"><p>توبه روزی کن و بر توبه ثبات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخوت توبه برون بر ز سرش</p></div>
<div class="m2"><p>دیدن توبه بپوش از نظرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش آن دیده که روشن نظر است</p></div>
<div class="m2"><p>دیدن توبه گناه دگر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می زند این همه از هستی سر</p></div>
<div class="m2"><p>کس نخورد از شجر هستی بر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ورع هر که زبردستی یافت</p></div>
<div class="m2"><p>پنجه زورور هستی تافت</p></div></div>