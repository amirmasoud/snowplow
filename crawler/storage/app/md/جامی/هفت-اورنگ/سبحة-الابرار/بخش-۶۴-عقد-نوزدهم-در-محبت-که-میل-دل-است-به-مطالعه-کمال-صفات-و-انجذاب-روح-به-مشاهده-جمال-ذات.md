---
title: >-
    بخش ۶۴ - عقد نوزدهم در محبت که میل دل است به مطالعه کمال صفات و انجذاب روح به مشاهده جمال ذات
---
# بخش ۶۴ - عقد نوزدهم در محبت که میل دل است به مطالعه کمال صفات و انجذاب روح به مشاهده جمال ذات

<div class="b" id="bn1"><div class="m1"><p>ای دلت شاه سراپرده عشق</p></div>
<div class="m2"><p>جان تو زخم بلا خورده عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق پروانه شمع ازل است</p></div>
<div class="m2"><p>داغ پروانگیش لم یزل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی قراری سپهر از عشق است</p></div>
<div class="m2"><p>گرم رفتاری مهر از عشق است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک یک جرعه ازان جام گرفت</p></div>
<div class="m2"><p>که درین دایره آرام گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بی عشق تن بی جان است</p></div>
<div class="m2"><p>جان ازو زنده جاویدان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوهر زندگی از عشق طلب</p></div>
<div class="m2"><p>گنج پایندگی از عشق طلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرده خوان هر که نه از وی زنده ست</p></div>
<div class="m2"><p>نیست دان هر چه نه زو پاینده ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق هر جا بود اکسیرگر است</p></div>
<div class="m2"><p>مس ز خاصیت اکسیر زر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گونه چون زر عشاق گواست</p></div>
<div class="m2"><p>کانچه شد گفته بود روشن و راست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق نی کار جهان ساختن است</p></div>
<div class="m2"><p>بلکه نقد دل و جان باختن است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق نی دلق بقا دوختن است</p></div>
<div class="m2"><p>بلکه با داغ فنا سوختن است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عاشق آن دان که ز خود باز رهد</p></div>
<div class="m2"><p>نغمه ترک خودی ساز دهد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه ره دولت و دنیا سپرد</p></div>
<div class="m2"><p>نه سوی نعمت عقبی نگرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قبله همت او دوست بود</p></div>
<div class="m2"><p>هر چه جز دوست همه پوست بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنچه با دوست دهد پیوندش</p></div>
<div class="m2"><p>شود از فرط محبت بندش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر دمد خار ز پیرامن او</p></div>
<div class="m2"><p>که سوی دوست کشد دامن او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود آن خار به از گلزارش</p></div>
<div class="m2"><p>عین راحت شمرد آزارش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وانچه از دوست حجابش گردد</p></div>
<div class="m2"><p>بر رخ وصل نقابش گردد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر چه خود مردمک دیده بود</p></div>
<div class="m2"><p>پیش چشمش نه پسندیده بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غم او شادی جانش باشد</p></div>
<div class="m2"><p>نام او ورد زبانش باشد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر به ذکرش گذراند همه سال</p></div>
<div class="m2"><p>ننشیند به دلش گرد ملال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گوی گردد خم چوگانش را</p></div>
<div class="m2"><p>سر نهد ضربت فرمانش را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نزند دم چو بگوید که بمیر</p></div>
<div class="m2"><p>شود از جام اجل جرعه پذیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نشود رنجه ز بد خوبی او</p></div>
<div class="m2"><p>نزید جز به رضا جویی او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ترک خشنودی اغیار کند</p></div>
<div class="m2"><p>به رضای دل او کار کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خیره ماند چو جمالش بیند</p></div>
<div class="m2"><p>لال گردد چو دلالش بیند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باشد از لذت صحبت رقصان</p></div>
<div class="m2"><p>لیک شوقش نپذیرد نقصان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر دمش حیرت دیگر زاید</p></div>
<div class="m2"><p>هر نفس شوق دگر افزاید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر چه در بحر بود کشتی وار</p></div>
<div class="m2"><p>عاقبت خشک لب آید به کنار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر نفس صد نفر از حور و پری</p></div>
<div class="m2"><p>گر کند بر نظرش جلوه گری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کم فتد جانب آنها نظرش</p></div>
<div class="m2"><p>نفرت افزون شود از هر نفرش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>غنچه سان باشدش از روز بهی</p></div>
<div class="m2"><p>دل پر از یار و ز اغیار تهی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه چو نرگس که چو بگشاید چشم</p></div>
<div class="m2"><p>بر همه خار و گلش آید چشم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گل همان در نظرش خار همان</p></div>
<div class="m2"><p>نشود بهر گل از خار رمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به رخ تازه گل و خشک گیاه</p></div>
<div class="m2"><p>نکند جز به یکی چشم نگاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نیست این قاعده عشق و وفا</p></div>
<div class="m2"><p>نیست این لازمه صدق و صفا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یا مکن بیهده از عشق خروش</p></div>
<div class="m2"><p>یا نظر زانچه نه معشوق بپوش</p></div></div>