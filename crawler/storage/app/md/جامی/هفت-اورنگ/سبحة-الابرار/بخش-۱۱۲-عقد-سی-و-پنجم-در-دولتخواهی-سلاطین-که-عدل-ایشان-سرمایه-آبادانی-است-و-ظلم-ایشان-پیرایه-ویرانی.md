---
title: >-
    بخش ۱۱۲ - عقد سی و پنجم در دولتخواهی سلاطین که عدل ایشان سرمایه آبادانی است و ظلم ایشان پیرایه ویرانی
---
# بخش ۱۱۲ - عقد سی و پنجم در دولتخواهی سلاطین که عدل ایشان سرمایه آبادانی است و ظلم ایشان پیرایه ویرانی

<div class="b" id="bn1"><div class="m1"><p>ای بلند از قدمت پایه تخت</p></div>
<div class="m2"><p>تاج را گوهر تو مایه بخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده از صبح ازل همرهیت</p></div>
<div class="m2"><p>سایه وش دولت ظل اللهیت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منصب خسرویت داده خدای</p></div>
<div class="m2"><p>کاوری قاعده عدل بجای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرش را قائمه این قاعده است</p></div>
<div class="m2"><p>شرع را فایده زین مائده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شه که از عدل نه فرخنده پی است</p></div>
<div class="m2"><p>خسروی واسطه خسر وی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نامه جاه فنا انجام است</p></div>
<div class="m2"><p>آنچه جاوید بماند نام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جم ازین بزم شد و جام نماند</p></div>
<div class="m2"><p>وز جم و جام بجز نام نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بد که بشکست ز مردن گهرش</p></div>
<div class="m2"><p>نام بد هست شکست دگرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیک اگر چه ز فنا گشته گم است</p></div>
<div class="m2"><p>نام نیکوش بقای دوم است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رشته عمر سراسر پیچ است</p></div>
<div class="m2"><p>با درازی چو شد آخر هیچ است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زیر این دایره دیر مدار</p></div>
<div class="m2"><p>مدت نوع شد افزون ز هزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیکن امروز هزاران سال است</p></div>
<div class="m2"><p>که جدا مانده ازان اقبال است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گنج شاهی که خدا داد تو را</p></div>
<div class="m2"><p>قیمت ملک بقا داد تو را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عدل یک ساعته ات را به قیاس</p></div>
<div class="m2"><p>شصت ساله عمل خیر شناس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود ده انصاف که این پایه که راست</p></div>
<div class="m2"><p>بهر سود ابد این مایه که راست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر بدین مایه زیانکار شوی</p></div>
<div class="m2"><p>وای آن روز که هشیار شوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روی در صحبت دینداران دار</p></div>
<div class="m2"><p>که خراب است ز بی دینان کار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سفلگانی که سرافراخته اند</p></div>
<div class="m2"><p>بهر دنیای تو دین باخته اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جاهلانند همه جاه طلب</p></div>
<div class="m2"><p>خویشتن را علما کرده لقب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چشمه هایند درین تیره مغاک</p></div>
<div class="m2"><p>گشته از جیفه دنیا ناپاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جستن پاکی ازین قوم خطاست</p></div>
<div class="m2"><p>ز آب ناپاک طهارت نه رواست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بیخ ظلم از دل خود پاک بکن</p></div>
<div class="m2"><p>شاخ ظالم به سیاست بشکن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بلکه آن بیخ چو برکنده شود</p></div>
<div class="m2"><p>شاخ ناچار سرافکنده شود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تیشه بر بیخ چو رانی گستاخ</p></div>
<div class="m2"><p>تازه بر جای کجا ماند شاخ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حیف باشد که در آن روز گران</p></div>
<div class="m2"><p>از تو پرسند گناه دگران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تیغ بر کس مکش از کینه وری</p></div>
<div class="m2"><p>به که باشد دلت از کینه بری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خشم و کین چشم خرد را رمد است</p></div>
<div class="m2"><p>نارمنده ز رمد بی خرد است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون کشد آتش خشم تو علم</p></div>
<div class="m2"><p>آب عفوش بزن از بحر کرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا نسوزی گهی از دشمن خویش</p></div>
<div class="m2"><p>مشو آتش فکن خرمن خویش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خشم کز غیرت دین شعله کش است</p></div>
<div class="m2"><p>روشنی جستن ازان شعله خوش است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر چه در چشم خسان شعله نماست</p></div>
<div class="m2"><p>بر لب خضروشان آب بقاست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مکن اندر کشش خلق شتاب</p></div>
<div class="m2"><p>که تأنیست درین کار صواب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر که شد سر به زمین افکنده</p></div>
<div class="m2"><p>نشود جز به قیامت زنده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وان که زنده ست خود از خوی درشت</p></div>
<div class="m2"><p>هر گهش خواهی بتوانی کشت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گوی با داد طلب نرم نه تیز</p></div>
<div class="m2"><p>عاجزان را نبود تاب ستیز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نرم باران به زراعت دهد آب</p></div>
<div class="m2"><p>چون رسد سیل شود کشت خراب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر ستمدیده ای از کشور تو</p></div>
<div class="m2"><p>دادخواهان برسد بر در تو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>با تو مظلومی خود عرض کند</p></div>
<div class="m2"><p>بر تو فریادرسی فرض کند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بین که آن ظلم ز ظالم به مثل</p></div>
<div class="m2"><p>گر رود با تو چه آری به عمل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سختی روز جزا آسان کن</p></div>
<div class="m2"><p>از برای دگران هم آن کن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>با اسیران به محنت شده بند</p></div>
<div class="m2"><p>آنچه با خود نپسندی مپسند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گوش بر قصه محتاجان دار</p></div>
<div class="m2"><p>کار حاجت طلبان زود گزار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا بود حاجت حاجتمندان</p></div>
<div class="m2"><p>نیست خوش طاعت دیگر چندان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همچو طاووس خودآرای مباش</p></div>
<div class="m2"><p>در خودآرایی خودرای مباش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>افسر فرق تو بس عز سجود</p></div>
<div class="m2"><p>زیور دست تو زر بخشی و جود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر میانت کمر طاعت بس</p></div>
<div class="m2"><p>بند کم شو به کمربندی کس</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کله از عدل و قبا پوش ز داد</p></div>
<div class="m2"><p>بر تو این نکته فراموش مباد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زانکه آبادی ملک از عدل است</p></div>
<div class="m2"><p>وز غم آزادی ملک از عدل است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا رعیت ز ملک شد نشد</p></div>
<div class="m2"><p>ملک از سعی وی آباد نشد</p></div></div>