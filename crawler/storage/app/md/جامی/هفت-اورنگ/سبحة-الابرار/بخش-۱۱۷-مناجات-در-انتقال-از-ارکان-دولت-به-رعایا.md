---
title: >-
    بخش ۱۱۷ - مناجات در انتقال از ارکان دولت به رعایا
---
# بخش ۱۱۷ - مناجات در انتقال از ارکان دولت به رعایا

<div class="b" id="bn1"><div class="m1"><p>ای به راه طلبت سعی کسی</p></div>
<div class="m2"><p>خالی از ترک هوس ها هوسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه ازین هیچ کسی ها که ز ماست</p></div>
<div class="m2"><p>بهر این بوالهوسی ها که ز ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان درین هیچ کسی چند کنیم</p></div>
<div class="m2"><p>در هر بوالهوسی چند زنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست در هیچ هوس بوی بهی</p></div>
<div class="m2"><p>دل ما را ز هوس ساز تهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلکه آن را به هوا ساز بدل</p></div>
<div class="m2"><p>به هوایی که بود عشق ازل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه هوایی که بود میل به مال</p></div>
<div class="m2"><p>یا به نیل شرف جاه و جلال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر جامی که متاعیست شگرف</p></div>
<div class="m2"><p>در هواها و هوس ها شده صرف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ازان عاریه چیزی مانده ست</p></div>
<div class="m2"><p>یا ازان گنج پشیزی مانده ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قوتش ده که هوای تو کند</p></div>
<div class="m2"><p>صرف آن بهر رضای تو کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از رضایت چو بباید نظری</p></div>
<div class="m2"><p>برساند به کسان زان اثری</p></div></div>