---
title: >-
    بخش ۱۱۴ - مناجات در انتقال از دولتخواهی ارباب سلطنت به نیکخواهی ارکان دولت
---
# بخش ۱۱۴ - مناجات در انتقال از دولتخواهی ارباب سلطنت به نیکخواهی ارکان دولت

<div class="b" id="bn1"><div class="m1"><p>ای ز عدل تو سماوات به پای</p></div>
<div class="m2"><p>نور عدلت ز زمین ظلم زدای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عدل شاهان که به هر خیر و شریست</p></div>
<div class="m2"><p>از جهانداری عدلت اثریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نام تو عدل بود کار تو عدل</p></div>
<div class="m2"><p>آشکارا شده آثار تو عدل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظلم هایی که به عالم پیداست</p></div>
<div class="m2"><p>همه عدل است ولی ظلم نماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه از توست بلی کی شاید</p></div>
<div class="m2"><p>کز تو کاری که نه عدل است آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسبت ظلم به تو نیست ادب</p></div>
<div class="m2"><p>ظلمت ماش دهد ظلم لقب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام عدلی به سر جامی ریز</p></div>
<div class="m2"><p>کش ز مستی نکند ظلم انگیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>معتدل ساز ازان جام او را</p></div>
<div class="m2"><p>به ز آغاز کن انجام او را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از همه ظلم رهایی بخشش</p></div>
<div class="m2"><p>دولت عدل نمایی بخشش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به هر سفله که ظلم اندوزد</p></div>
<div class="m2"><p>رستن از ظلمت ظلم آموزد</p></div></div>