---
title: >-
    بخش ۹ - سبب نظم جواهر آبدار سبحة الابرار که هر عقد وی از رشته آمال عقده گشاست و هر مهره از آن در گردش احوال مهر افزاست
---
# بخش ۹ - سبب نظم جواهر آبدار سبحة الابرار که هر عقد وی از رشته آمال عقده گشاست و هر مهره از آن در گردش احوال مهر افزاست

<div class="b" id="bn1"><div class="m1"><p>شب که زد تیرگی مهره گل</p></div>
<div class="m2"><p>قیرگون خیمه ز مخروطی ظل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اختر از سیم و شهاب از زر ناب</p></div>
<div class="m2"><p>ساختند از پی آن میخ و طناب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون مشبک قفسی مشکین رنگ</p></div>
<div class="m2"><p>گشت بر مرغ دلم عالم تنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر خود این تنگ قفس چاک زدم</p></div>
<div class="m2"><p>پای بر طارم افلاک زدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالمی یافتم از عالم پیش</p></div>
<div class="m2"><p>هر چه اندیشه رسد زان هم بیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل معزول ز گرد آوریش</p></div>
<div class="m2"><p>وهم عاجز ز مساحتگریش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نور بر نور چراغ حرمش</p></div>
<div class="m2"><p>فیض بر فیض سحاب کرمش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سنگ بطحاش گهردار همه</p></div>
<div class="m2"><p>ابر صحراش گهربار همه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سرم گوهر و در چندان ریخت</p></div>
<div class="m2"><p>که مرا رشته طاقت بگسیخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حیفم آمد که ازان گنج نهان</p></div>
<div class="m2"><p>نشوم بهره ور و بهره فشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوش جان را صدف در کردم</p></div>
<div class="m2"><p>جیب دل را ز گهر پر کردم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بازگشتم به قدمگاه نخست</p></div>
<div class="m2"><p>عزم بر نظم گهر کرده درست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر چه زانجا گهر و در رفتم</p></div>
<div class="m2"><p>همه ز الماس تفکر سفتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس سحرها که به شام آوردم</p></div>
<div class="m2"><p>شام ها همچو شفق خون خوردم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرسله مرسله بر هم بستم</p></div>
<div class="m2"><p>عقد بر عقد به هم پیوستم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سبحه ای شد پی ابرار تمام</p></div>
<div class="m2"><p>خواندمش سبحت الابرار به نام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قدسیان دست به آن آوردند</p></div>
<div class="m2"><p>دعوی «نحن نسبح » کردند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مهره هایش ز خرد مهره ربای</p></div>
<div class="m2"><p>عقدهایش ز فلک عقده گشای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سلک آن دایره مرکز دین</p></div>
<div class="m2"><p>رشته شمع شبستان یقین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نقد هر عقد وی از کان دگر</p></div>
<div class="m2"><p>داده آرایش دکان دگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>می رسد عد عقودش به چهل</p></div>
<div class="m2"><p>هر یک از دل گره جهل گسل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اربعینیست که درهای فتوح</p></div>
<div class="m2"><p>زو گشاده ست به خلوتگه روح</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرت این سبحه اقبال و شرف</p></div>
<div class="m2"><p>افتد از گردش ایام به کف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>طوق گردن کن و آویزه گوش</p></div>
<div class="m2"><p>به دو صد عقد در آن را مفروش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بو که چو سبحه درآیی به شمار</p></div>
<div class="m2"><p>رسدت دست به سر رشته کار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چرخ کحلی سلب ازرق پوش</p></div>
<div class="m2"><p>همچو ابنای زمان زرق فروش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سبحه عقد ثریا در دست</p></div>
<div class="m2"><p>خواست بر گوهر این سبحه شکست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفتم این رشته گوهر به کفت</p></div>
<div class="m2"><p>که بود نقد بلورین صدفت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر چه بس لامع و نورافشان است</p></div>
<div class="m2"><p>نور این سبحه دو صد چندان است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نور آن روی زمین را بگرفت</p></div>
<div class="m2"><p>نور این کشور دین را بگرفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نور آن چشم جهان روشن کرد</p></div>
<div class="m2"><p>نور این دیده جان روشن کرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر چه آن گوهر بحر کهن است</p></div>
<div class="m2"><p>این نو آیین در درج سخن است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر به صورت بود آن پایه بلند</p></div>
<div class="m2"><p>رفعت معنوی این راست پسند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر چه در سلک زمان آن پیش است</p></div>
<div class="m2"><p>چون درآری به شمار این بیش است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر چه آنجا نرسد دست کسی</p></div>
<div class="m2"><p>بهره ور گردد ازین دست بسی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر چه آن هم وطن ماه و خور است</p></div>
<div class="m2"><p>این به خورشید ازل راهبر است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گوش گردون چو شنید این سخنان</p></div>
<div class="m2"><p>شد ز ذوق سخنم چرخ زنان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گفت قد جئت بنظم سامی</p></div>
<div class="m2"><p>احسن الله جزاک ای جامی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ماه و اختر گهر سلک تو باد</p></div>
<div class="m2"><p>لوح خور پی سپر کلک تو باد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>باد تا مهره گل هست بجای</p></div>
<div class="m2"><p>سبحه نظم تو انگشت نمای</p></div></div>