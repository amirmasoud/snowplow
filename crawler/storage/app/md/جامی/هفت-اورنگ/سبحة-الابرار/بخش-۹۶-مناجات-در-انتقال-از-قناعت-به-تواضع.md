---
title: >-
    بخش ۹۶ - مناجات در انتقال از قناعت به تواضع
---
# بخش ۹۶ - مناجات در انتقال از قناعت به تواضع

<div class="b" id="bn1"><div class="m1"><p>ای به زندان غمت شاد همه</p></div>
<div class="m2"><p>بند تو بنده و آزاد همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی در قبله احسان توییم</p></div>
<div class="m2"><p>بندی و بنده فرمان توییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر ما افسر طاعت ز تو یافت</p></div>
<div class="m2"><p>دل ما عز قناعت ز تو یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حرص ما بر تو ز حد بیرون است</p></div>
<div class="m2"><p>هر چه گوییم ازان افزون است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان گرفتار صنایع نشویم</p></div>
<div class="m2"><p>کز تو جز هم به تو قانع نشویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامی از حرص و قناعت رسته</p></div>
<div class="m2"><p>در رهت محمل طاعت بسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بارش از راه به منزل برسان</p></div>
<div class="m2"><p>رختش از موج به ساحل برسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعله در خرمن پندارش زن</p></div>
<div class="m2"><p>سکه بر صفحه دینارش زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زآتش عشق شراریش بده</p></div>
<div class="m2"><p>بر در قرب قراریش بده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پشت کبرش که ندیده ست شکست</p></div>
<div class="m2"><p>به لگدکوب تواضع کن پست</p></div></div>