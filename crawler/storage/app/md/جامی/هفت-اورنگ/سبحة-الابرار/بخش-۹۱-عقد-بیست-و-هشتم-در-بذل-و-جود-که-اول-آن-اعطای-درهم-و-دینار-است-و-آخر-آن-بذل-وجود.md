---
title: >-
    بخش ۹۱ - عقد بیست و هشتم در بذل و جود که اول آن اعطای درهم و دینار است و آخر آن بذل وجود
---
# بخش ۹۱ - عقد بیست و هشتم در بذل و جود که اول آن اعطای درهم و دینار است و آخر آن بذل وجود

<div class="b" id="bn1"><div class="m1"><p>ای درم گرد تو بسیار شده</p></div>
<div class="m2"><p>دین تو در سر دینار شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنج جود است کف تو مپسند</p></div>
<div class="m2"><p>از هر انگشت بر آنجا دو سه بند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست بسته بود از مرد درشت</p></div>
<div class="m2"><p>بهر آزار درم جویان مشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشت پرز که نماید مدخل</p></div>
<div class="m2"><p>مشت پر کرده بود بر سایل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کف بی جود وی از خوی نه خوب</p></div>
<div class="m2"><p>بر گدایان ز قفا سیلی کوب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پنجه خود به مساحت بگشای</p></div>
<div class="m2"><p>بر درم جود در راحت بگشای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنچه سان خرده چه پیچی به ورق</p></div>
<div class="m2"><p>خرج کن همچو گل آن را به طبق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موجب قبض بود جمع درم</p></div>
<div class="m2"><p>مایه بسط و طرب بذل و کرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بین کفت را که به بیشی و کمی</p></div>
<div class="m2"><p>قبض و بسط از درم و بی درمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باش چون حقه که هست از زر و مال</p></div>
<div class="m2"><p>خواه پر خواه تهی بر یک حال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه چو همیان که زر و بی زریش</p></div>
<div class="m2"><p>می دهد فربهی و لاغریش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقد همیان که پر از سیم و زر است</p></div>
<div class="m2"><p>بر میان تو چو زرین کمر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر میان همچو کمر مپسند آن</p></div>
<div class="m2"><p>جز پی خدمت حاجتمندان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گنج از امساک بود خاک به سر</p></div>
<div class="m2"><p>کان ز امساک شود زیر و زبر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر چه داری ز در و گوهر ناب</p></div>
<div class="m2"><p>ریز بر خاک و برآ خوش چو سحاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بار فقر ار فکنی از یک تن</p></div>
<div class="m2"><p>بار منت منهش بر گردن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کوهی از فقر اگر آید پیش</p></div>
<div class="m2"><p>کاهی از منت ازان باشد بیش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون عطابخش خدا آمد و بس</p></div>
<div class="m2"><p>به که دانا ننهد منت کس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در کرم حیله گری بیش نیی</p></div>
<div class="m2"><p>جود را رهگذری بیش نیی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چیست چندین عظموت و جبروت</p></div>
<div class="m2"><p>پشت لب بر زدن و باد بروت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کیسه بیشتر از کان که شنید</p></div>
<div class="m2"><p>کاسه گرمتر از آش که دید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر زر و مال که بخشیده دهی</p></div>
<div class="m2"><p>باید از وجه پسندیده دهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به ستم سیم ستانی ز کسان</p></div>
<div class="m2"><p>تا کشی خوان کرم بهر خسان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نیست لایقتر ازین هیچ کرم</p></div>
<div class="m2"><p>کز کسان بازکشی دست ستم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قحبه کز کسب زنا بخشد زر</p></div>
<div class="m2"><p>بخل صد بار ز جودش بهتر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جود او دود شرارت شرر است</p></div>
<div class="m2"><p>بخل او نخل سعادت ثمر است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مالت از دزد به تاراج افتد</p></div>
<div class="m2"><p>به که نی در کف محتاج افتد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ابر باید که به صحرا بارد</p></div>
<div class="m2"><p>زان چه حاصل که به دریا بارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>می دهد سبزه و گل صحرا را</p></div>
<div class="m2"><p>می کند آبله رو دریا را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دل فاسق که به زر شاد کنی</p></div>
<div class="m2"><p>مجلس فسق وی آباد کنی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به می و نقل کنی یاوریش</p></div>
<div class="m2"><p>مطرب و شاهد و شمع آوریش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ظلم زور ز زر یافته هست</p></div>
<div class="m2"><p>ظلم را تیغ زراندود به دست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از زر و سیم بر او جود مکن</p></div>
<div class="m2"><p>ظلم را تیغ زر اندود مکن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر چه بخشی که بگیری دگری</p></div>
<div class="m2"><p>آن نه جود است که بیع است و شری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تخم تلبیس بود دانه به دام</p></div>
<div class="m2"><p>نیست بر گرسنه مرغان انعام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>صید گر دانه که می افشاند</p></div>
<div class="m2"><p>می کند حیله که جان بستاند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همتی ورز درین کاخ منیر</p></div>
<div class="m2"><p>همچو خورشید ببخش و مپذیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فیض خور نیست به هر شیب و فراز</p></div>
<div class="m2"><p>بهر نفعی که به وی گردد باز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر عطا صیت و ثنایی مطلب</p></div>
<div class="m2"><p>وز عطاخواه جزایی مطلب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ور فتد زو دو صدت گنج به چنگ</p></div>
<div class="m2"><p>باز ده ور چه کشد کار به جنگ</p></div></div>