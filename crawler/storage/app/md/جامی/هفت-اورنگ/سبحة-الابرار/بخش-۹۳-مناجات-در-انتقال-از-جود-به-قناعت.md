---
title: >-
    بخش ۹۳ - مناجات در انتقال از جود به قناعت
---
# بخش ۹۳ - مناجات در انتقال از جود به قناعت

<div class="b" id="bn1"><div class="m1"><p>ای محیط کرمت عرش صدف</p></div>
<div class="m2"><p>عرشیان در طلبت باده به کف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما که لب تشنه احسان توییم</p></div>
<div class="m2"><p>کشتی افتاده به طوفان توییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظر لطف بر این کشتی دار</p></div>
<div class="m2"><p>به سلامت برسانش به کنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیمه ما به سوی ساحل زن</p></div>
<div class="m2"><p>صدف هستی ما را بشکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده ظلمت ما را بگشای</p></div>
<div class="m2"><p>صفوت گوهر ما را بنمای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامی از هستی خود گشته ملول</p></div>
<div class="m2"><p>دارد از فضل تو امید قبول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر خوان عطایش بنشان</p></div>
<div class="m2"><p>دامن از گرد خطایش بفشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنگر اندوه وی و شادش کن</p></div>
<div class="m2"><p>بنده پیر شد آزادش کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بینشش ده که تو را بشناسد</p></div>
<div class="m2"><p>نعمتت را ز بلا بشناسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمر خدمت طاعت بخشش</p></div>
<div class="m2"><p>افسر عز قناعت بخشش</p></div></div>