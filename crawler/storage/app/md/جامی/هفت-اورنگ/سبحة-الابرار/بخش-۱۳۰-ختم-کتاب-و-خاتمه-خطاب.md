---
title: >-
    بخش ۱۳۰ - ختم کتاب و خاتمه خطاب
---
# بخش ۱۳۰ - ختم کتاب و خاتمه خطاب

<div class="b" id="bn1"><div class="m1"><p>دامت آثارک ای طرفه قلم</p></div>
<div class="m2"><p>دام دل ها زدی از مشک رقم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واسطی نسبت و شامی اثری</p></div>
<div class="m2"><p>تحفه شام سوی روم بری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقد عمر است نثار قدمت</p></div>
<div class="m2"><p>نور چشم است سواد رقمت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ جان راست صریر تو صفیر</p></div>
<div class="m2"><p>وز صفیر تو در آفاق نفیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کجا پرسمت ای قاصد دل</p></div>
<div class="m2"><p>که عجب مسرعی و مستعجل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرکب گرم عنان می رانی</p></div>
<div class="m2"><p>خوی چکان قطره زنان می رانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نامه نامفزا می آری</p></div>
<div class="m2"><p>خیر مقدم ز کجا می آری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این چه نقش است که ناگاه زدی</p></div>
<div class="m2"><p>پنجه شب به رخ ماه زدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بافتی بر قد این حور سرشت</p></div>
<div class="m2"><p>حله از طره حوران بهشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این چه حور است درین حله ناز</p></div>
<div class="m2"><p>کرده از دولت جاوید طراز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روی زیباش مه اوج شرف</p></div>
<div class="m2"><p>زلف مشکینش «من اللیل زلف »</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جبهه اش فاتحه مصحف نور</p></div>
<div class="m2"><p>بر میانش کمر «خیرالامور»</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر دو مصراع ز وی ابرویی</p></div>
<div class="m2"><p>قبله حاجت حاجت جویی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چشمش از کحل بصیرت روشن</p></div>
<div class="m2"><p>نظر لطف به عشاق افکن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طره اش پرده کش شاهد دین</p></div>
<div class="m2"><p>خال او مردمک چشم یقین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لب او مژده ده باد مسیح</p></div>
<div class="m2"><p>در فسون خوانی هر مرده فصیح</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>راستی شکل قد رعنایش</p></div>
<div class="m2"><p>صدق عکس رخ صبح آسایش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گوشش از حلقه اخلاص گران</p></div>
<div class="m2"><p>دیده عشق به رویش نگران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خرد گام زن از دنبالش</p></div>
<div class="m2"><p>بیخود از زمزمه خلخالش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جامی آمد چو به خلخال سخن</p></div>
<div class="m2"><p>از دعا گوهر خلخاش کن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یا رب این غیرت حورالعین را</p></div>
<div class="m2"><p>شاهد روضه علیین را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از دل و دیده هر دیده وری</p></div>
<div class="m2"><p>بخش توفیق قبول نظری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خاصه آن در روش فضل دلیر</p></div>
<div class="m2"><p>زان دلیریش شده نام دو شیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن یکی در ره دین شیر خدای</p></div>
<div class="m2"><p>وان دگر پنجه به هر صید گشای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چشمش از خوش قلمان روشن کن</p></div>
<div class="m2"><p>خالش از پاک دمان گلشن کن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از خط خوب کنش پاینده</p></div>
<div class="m2"><p>وز دم پاک طرب زاینده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لیک در جلوه گه عزت و جاه</p></div>
<div class="m2"><p>دارش از دست دویی پاک نگاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اول آن خامه زن سهو نویس</p></div>
<div class="m2"><p>به سر دوک قلم بیهده ریس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر خط و شعر وقوف از وی دور</p></div>
<div class="m2"><p>چشم داران حروف از وی کور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فصل و وصل کلماتش نه به جای</p></div>
<div class="m2"><p>فصل پیش نظرش وصل نمای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گه دو بیگانه به هم پیوسته</p></div>
<div class="m2"><p>گه دو همخانه ز هم بگسسته</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نقطه هایش نه به قانون حساب</p></div>
<div class="m2"><p>خارج از دایره صدق و صواب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خال رخساره زده بر کف پای</p></div>
<div class="m2"><p>شده از زیور رخ پای آرای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ور به اعراب شده راهسپر</p></div>
<div class="m2"><p>رسم خط گشته ازو زیر و زبر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گه نوشته ست کم و گاه فزون</p></div>
<div class="m2"><p>گشته موزون ز خطش ناموزون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یا برده یکی از پنج انگشت</p></div>
<div class="m2"><p>یا فزوده ششم انگشت به مشت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از قلم باد جدا انگشتش</p></div>
<div class="m2"><p>بلکه انگشت قلم در مشتش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دوم آن کس که کشد گزلک تیز</p></div>
<div class="m2"><p>بهر اصلاح نه از سهو و ستیز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بتراشد ز ورق حرف صواب</p></div>
<div class="m2"><p>زند از کلک خطا نقش بر آب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گل کند خار به جا بنشاند</p></div>
<div class="m2"><p>خار را خوبتر از گل داند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بادش آن گزلک خنجر کردار</p></div>
<div class="m2"><p>قاطع دست تصرف زین کار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حسن مقطع چو بود رسم کهن</p></div>
<div class="m2"><p>قطع کردیم بر این نکته سخن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ختم الله لنا بالحسنی</p></div>
<div class="m2"><p>و هو مولانا نعم المولی</p></div></div>