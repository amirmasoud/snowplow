---
title: >-
    بخش ۱۲۶ - مناجات در انتقال از خود به مطالعه کنندگان
---
# بخش ۱۲۶ - مناجات در انتقال از خود به مطالعه کنندگان

<div class="b" id="bn1"><div class="m1"><p>ای رهایی ده هر بیهوشی</p></div>
<div class="m2"><p>مهر بر لب نه هر خاموشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هوای تو سخن کوشی ما</p></div>
<div class="m2"><p>به تمنای تو خاموشی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تو در حرف تهی لطف شگرف</p></div>
<div class="m2"><p>لجه ژرف شود چشمه حرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور بر آفاق زنی حمله بیم</p></div>
<div class="m2"><p>قاف تا قاف شود حلقه میم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد توست اصل همه تنگی ها</p></div>
<div class="m2"><p>قرب تو مایه یکرنگی ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل جامی که بود تنگ از تو</p></div>
<div class="m2"><p>عندلیبیست غم آهنگ از تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بال پروازش ازین تنگی ده</p></div>
<div class="m2"><p>نکهتش از گل یکرنگی ده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوز از تار فنا دلق او را</p></div>
<div class="m2"><p>برهان از خود و از خلق او را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عیبش از بی هنران ساز نهان</p></div>
<div class="m2"><p>وز گمان هنرش باز رهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا ز عیب و هنر خود آزاد</p></div>
<div class="m2"><p>زید اندر کنف فضل تو شاد</p></div></div>