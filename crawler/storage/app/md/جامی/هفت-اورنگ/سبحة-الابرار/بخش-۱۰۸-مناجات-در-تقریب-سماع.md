---
title: >-
    بخش ۱۰۸ - مناجات در تقریب سماع
---
# بخش ۱۰۸ - مناجات در تقریب سماع

<div class="b" id="bn1"><div class="m1"><p>ای دل و دیده صاحبنظران</p></div>
<div class="m2"><p>از خیالت به جمال دگران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی در روی تو باشد همه را</p></div>
<div class="m2"><p>چشم دل سوی تو باشد همه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه جا پرتو رویت نگرند</p></div>
<div class="m2"><p>پا ز سر کرده به سویت گذرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هوای تو نشینند به هم</p></div>
<div class="m2"><p>به تمنای تو بینند به هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر نوایی که به جایی شنوند</p></div>
<div class="m2"><p>که ازان بوی وفایی شنوند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پای تا سر همگی گوش شوند</p></div>
<div class="m2"><p>با غمت دست در آغوش شوند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آستین بر سر جان افشانند</p></div>
<div class="m2"><p>دامن از میل جهان افشانند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنده جامی نه ازان انجمن است</p></div>
<div class="m2"><p>لیک در دامنشان دستزن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگسل دست وی از دامنشان</p></div>
<div class="m2"><p>خوشه چینی دهش از خرمنشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از نم زرق و ریا پاکش کن</p></div>
<div class="m2"><p>در ره صدق و صفا خاکش کن</p></div></div>