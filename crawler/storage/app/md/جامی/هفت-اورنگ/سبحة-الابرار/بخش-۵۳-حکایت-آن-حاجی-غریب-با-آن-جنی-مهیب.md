---
title: >-
    بخش ۵۳ - حکایت آن حاجی غریب با آن جنی مهیب
---
# بخش ۵۳ - حکایت آن حاجی غریب با آن جنی مهیب

<div class="b" id="bn1"><div class="m1"><p>رهروی روی به تنهایی کرد</p></div>
<div class="m2"><p>بهر حج بادیه پیمایی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راحله پای بیابان پیمای</p></div>
<div class="m2"><p>قافله دیو و دد جان فرسای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تف نشان جگرش موج سراب</p></div>
<div class="m2"><p>گرد شوی قدمش چشم پر آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز عصا کس نگرفته دستش</p></div>
<div class="m2"><p>غیر نعلین نه کس پابستش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی از دور یکی شخص غریب</p></div>
<div class="m2"><p>شد پدیدار به دیدار مهیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت تو آدمیی یا پریی</p></div>
<div class="m2"><p>که عجب بر سر غارتگریی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوهر ایمنی از من بردی</p></div>
<div class="m2"><p>به کف خایفیم بسپردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت نی آدمیم من پریم</p></div>
<div class="m2"><p>لیک چون آدمیان گوهریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو کیی، مؤمن واحد دانی</p></div>
<div class="m2"><p>یا نه در شرک فرس می رانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت من سوی یکی رو دارم</p></div>
<div class="m2"><p>از دو گویان جهان بیزارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت اگر زانکه خدای تو یکیست</p></div>
<div class="m2"><p>در دلت از یکی او نه شکیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شرم بادت که جز از وی ترسی</p></div>
<div class="m2"><p>پای بگذاشته از پی ترسی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون خدادان ز خدا ترسد و بس</p></div>
<div class="m2"><p>ترسد از وی همه چیز و همه کس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیک ترسد چو نترسد ز خدای</p></div>
<div class="m2"><p>هر وقت از همه کس در همه جای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترسگاری ز خدا عاقلی است</p></div>
<div class="m2"><p>لیک از غیر خدا غافلی است</p></div></div>