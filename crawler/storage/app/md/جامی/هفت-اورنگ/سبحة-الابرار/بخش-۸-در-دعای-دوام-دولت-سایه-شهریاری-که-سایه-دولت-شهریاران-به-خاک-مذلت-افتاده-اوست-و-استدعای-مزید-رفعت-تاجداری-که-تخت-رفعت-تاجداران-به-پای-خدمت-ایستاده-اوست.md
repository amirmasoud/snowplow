---
title: >-
    بخش ۸ - در دعای دوام دولت سایه شهریاری که سایه دولت شهریاران به خاک مذلت افتاده اوست و استدعای مزید رفعت تاجداری که تخت رفعت تاجداران به پای خدمت ایستاده اوست
---
# بخش ۸ - در دعای دوام دولت سایه شهریاری که سایه دولت شهریاران به خاک مذلت افتاده اوست و استدعای مزید رفعت تاجداری که تخت رفعت تاجداران به پای خدمت ایستاده اوست

<div class="b" id="bn1"><div class="m1"><p>چون نی خامه شد انگشت نمای</p></div>
<div class="m2"><p>به نواسازی توحید خدای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلگشا زمزمه دیگر ساخت</p></div>
<div class="m2"><p>پرده نعت پیمبر پرداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چو آن زمزمه کوتاه کند</p></div>
<div class="m2"><p>که ثناگستری شاه کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه والا گهر دریا کف</p></div>
<div class="m2"><p>که فلک گوهر او راست صدف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حامی بیضه گیتی ز فتن</p></div>
<div class="m2"><p>بر سر فتنه گران بیضه شکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل او صفحه ایام به تیغ</p></div>
<div class="m2"><p>کرده پاک از رقم درد و دریغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رای او رایت جمشید افراخت</p></div>
<div class="m2"><p>چتر او سایه به خورشید انداخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کفش ابریست که گوهر بارد</p></div>
<div class="m2"><p>بلکه خورشید صفت زر بارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چمن ز ابر کفش پر گردد</p></div>
<div class="m2"><p>هر گل از وی طبقی در گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور بر او زر کند از جود نثار</p></div>
<div class="m2"><p>مشت دینار شود دست چنار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیل اعداش که بی دسترسند</p></div>
<div class="m2"><p>دست در هم زده یک مشت خسند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برق قهرش چو رسد زهرآلود</p></div>
<div class="m2"><p>دودشان بگذرد از چرخ کبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کار مظلوم بود ساخته اش</p></div>
<div class="m2"><p>ظلم از آفاق برانداخته اش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیش ازین نقد بسی گنج شگرف</p></div>
<div class="m2"><p>نه به میزان کرم گشتی صرف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عدلش کنون که به عالم سمر است</p></div>
<div class="m2"><p>مانع صرف چو عدل عمر است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نامش آن گوهر تاج اورنگ است</p></div>
<div class="m2"><p>که بر او بحر کلامم تنگ است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بین ز فضل ازل این اکرامش</p></div>
<div class="m2"><p>که چو وی هست گرامی نامش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ذاتی از تاجوری یافته زین</p></div>
<div class="m2"><p>تاج سلطان بود و ذات حسین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای خرد داده جمال ابدت</p></div>
<div class="m2"><p>نام نیکو ز ازل نامزدت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سکه را خطبه لقبداری توست</p></div>
<div class="m2"><p>خطبه را سکه به نام تو درست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هست نیک و بد عالم همه پوست</p></div>
<div class="m2"><p>آنچه مغز است در او نام نکوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چشم ازین پوست سوی مغزگشای</p></div>
<div class="m2"><p>مغز نغز است سوی نغز گرای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیکنام آمده بحر و بری</p></div>
<div class="m2"><p>نامور شو به نکونامتری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جام عیشت چو شود دست آویز</p></div>
<div class="m2"><p>جرعه بر خاک تهی دستان ریز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پاکبازان که همه خاک تواند</p></div>
<div class="m2"><p>جرعه پرورد می پاک تواند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گنج نه گنج فشان هر دو تویی</p></div>
<div class="m2"><p>تاج ده تاج ستان هر دو تویی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سرمه چشم جهان خاک درت</p></div>
<div class="m2"><p>طوق جان حلقه بند کمرت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هست میدان سخن تنگ بسی</p></div>
<div class="m2"><p>چون رود راه ثنای تو کسی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حرف را کی بود آن گنجایی</p></div>
<div class="m2"><p>که شود ظرف ثناپیمایی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بحر معنی چو شود موج سگال</p></div>
<div class="m2"><p>چشمه حرف بود تنگ مجال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کوزه از بحر چو دریوزه کند</p></div>
<div class="m2"><p>بحر پیداست چه در کوزه کند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نیست چون این غرض انجام پذیر</p></div>
<div class="m2"><p>به که گردم ز دعا زمزمه گیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر سحر تا فلک صبح شکاف</p></div>
<div class="m2"><p>تیغ خورشید برآرد ز غلاف</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فرق حاسد ز تو بشکافته باد</p></div>
<div class="m2"><p>روز و شب یافته و تافته باد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یافته کام تو در باغ امل</p></div>
<div class="m2"><p>تافته جان وی از داغ اجل</p></div></div>