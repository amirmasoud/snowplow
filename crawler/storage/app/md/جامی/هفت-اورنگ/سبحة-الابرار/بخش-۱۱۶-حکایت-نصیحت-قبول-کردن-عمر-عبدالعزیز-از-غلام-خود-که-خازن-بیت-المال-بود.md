---
title: >-
    بخش ۱۱۶ - حکایت نصیحت قبول کردن عمر عبدالعزیز از غلام خود که خازن بیت المال بود
---
# بخش ۱۱۶ - حکایت نصیحت قبول کردن عمر عبدالعزیز از غلام خود که خازن بیت المال بود

<div class="b" id="bn1"><div class="m1"><p>عمر ثانی آن همچو نخست</p></div>
<div class="m2"><p>کرده در دین سبق عدل درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داشت در ستر حرم فرزندان</p></div>
<div class="m2"><p>چون پدر جمله سعادتمندان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عید شد پیش پدر جمع شدند</p></div>
<div class="m2"><p>همه پروانه آن شمع شدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک از دیده فشاندند چو شمع</p></div>
<div class="m2"><p>کای پریشانی عالم به تو جمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تن عور چو شمعیم همه</p></div>
<div class="m2"><p>بهر جامه شده جمعیم همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست از اطلس اکسون سخنی</p></div>
<div class="m2"><p>همچو فانوس کم از پیرهنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به کی سرزنش دایه کشیم</p></div>
<div class="m2"><p>سردی طعنه همسایه کشیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون عمر گریه فرزندان دید</p></div>
<div class="m2"><p>بار غم بر دلشان نپسندید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنده ای داشت عجب فرخ فال</p></div>
<div class="m2"><p>کار او خازنی بیت المال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتش آور بدر از مخزن خویش</p></div>
<div class="m2"><p>خرج یک ماهه من بی کم و بیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کار این چند جگرگوشه بساز</p></div>
<div class="m2"><p>خرجی من به دگر ماه انداز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنده گفتا که تویی ای خواجه</p></div>
<div class="m2"><p>بر سر دفتر دین دیباچه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می ندانم که تو را ضامن کیست</p></div>
<div class="m2"><p>که یکی هفته دگر خواهی زیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون خوری مال مسلمانان را</p></div>
<div class="m2"><p>گر بمیری که دهد تاوان را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عمر آن نکته نیکو چو شنفت</p></div>
<div class="m2"><p>آفرین کرد و به فرزندان گفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روی در زاویه درد کنید</p></div>
<div class="m2"><p>وین هوس بر دل خود سرد کنید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زانکه بی خون جگر پالودن</p></div>
<div class="m2"><p>نیست امکان به بهشت آسودن</p></div></div>