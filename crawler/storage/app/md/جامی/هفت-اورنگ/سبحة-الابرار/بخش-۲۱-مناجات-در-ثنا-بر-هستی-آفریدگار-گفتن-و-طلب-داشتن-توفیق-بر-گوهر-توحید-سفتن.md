---
title: >-
    بخش ۲۱ - مناجات در ثنا بر هستی آفریدگار گفتن و طلب داشتن توفیق بر گوهر توحید سفتن
---
# بخش ۲۱ - مناجات در ثنا بر هستی آفریدگار گفتن و طلب داشتن توفیق بر گوهر توحید سفتن

<div class="b" id="bn1"><div class="m1"><p>ای جهان از صفت ذات تو پر</p></div>
<div class="m2"><p>عالم از حجت اثبات تو پر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ جا نیست که غوغای تو نیست</p></div>
<div class="m2"><p>پرتو روی دلارای تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو چنین ظاهر و ما کور بصر</p></div>
<div class="m2"><p>تو چنین حاضر و ما دور نگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نور تو گر نبود ما چه کنیم</p></div>
<div class="m2"><p>چشم بینا دل دانا چه کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست از غایت کوته نظری</p></div>
<div class="m2"><p>خبر ما ز تو جز بی خبری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه جامی بود از بیخبران</p></div>
<div class="m2"><p>چه شود گر به طفیل دگران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخشی از هستی خویشش خبری</p></div>
<div class="m2"><p>بندی از طاعت خویشش کمری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دلش تخم هدایت کاری</p></div>
<div class="m2"><p>بر گلش ابر عنایت باری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهرش از مهره گل بگشایی</p></div>
<div class="m2"><p>زنگش از چهره دل بزدایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پا به کاشانه قربت نهیش</p></div>
<div class="m2"><p>می ز میخانه وحدت دهیش</p></div></div>