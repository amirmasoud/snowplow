---
title: >-
    بخش ۹۸ - حکایت پیر آزاده با جوان محتشم زاده
---
# بخش ۹۸ - حکایت پیر آزاده با جوان محتشم زاده

<div class="b" id="bn1"><div class="m1"><p>محتشم زاده از نخوت جاه</p></div>
<div class="m2"><p>می خرامید ظریفانه به راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تبختر قدمی برمی داشت</p></div>
<div class="m2"><p>وز تکبر علمی می افراشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عارفی پشت دو تا در ژنده</p></div>
<div class="m2"><p>دلی از نور الهی زنده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت کای تازه جوان تند مرو</p></div>
<div class="m2"><p>پند سنجیده پیران بشنو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این روش نیست چو خوش پیش خدای</p></div>
<div class="m2"><p>بازکش زین روش ناخوش پای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبع او از سخن پیر آشفت</p></div>
<div class="m2"><p>بانگ برداشت ز نادانی و گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کای ز گفتار تو بر من باری</p></div>
<div class="m2"><p>می شناسی که کیم گفت آری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اولت بود یکی قطره آب</p></div>
<div class="m2"><p>که ازان شستن ثوب است ثواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از شکم تا به کنار آمده ای</p></div>
<div class="m2"><p>از ره بول دو بار آمده ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>و آخرت جیفه افتاده به خاک</p></div>
<div class="m2"><p>کرده پنهان به یکی تیره مغاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر تو آن پرده به فرض ار بدرند</p></div>
<div class="m2"><p>چشم نابسته کسان کم گذرند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در میانه که سراسر خوشی است</p></div>
<div class="m2"><p>روز و شب کار تو سرگین کشی است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تنت آراسته از گوهر و در</p></div>
<div class="m2"><p>چون شکنبه شکم از سرگین پر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر به خود نیست شناساوریت</p></div>
<div class="m2"><p>لب گشادم به شناساگریت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از من این نکته فراموش مکن</p></div>
<div class="m2"><p>مدحت مدحگران گوش مکن</p></div></div>