---
title: >-
    بخش ۴۲ - مناجات در طلب مقام فقر بعد از تحقق به مقام زهد
---
# بخش ۴۲ - مناجات در طلب مقام فقر بعد از تحقق به مقام زهد

<div class="b" id="bn1"><div class="m1"><p>ای در رحمت تو بر همه باز</p></div>
<div class="m2"><p>غرقه نعمت تو شیب و فراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقبازان به تمنای تو بند</p></div>
<div class="m2"><p>زهدورزان به خیالت خرسند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نه با بت ز تو باشد نامی</p></div>
<div class="m2"><p>کس سوی بتکده ننهد گامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرنه بویی ز تو آید به دماغ</p></div>
<div class="m2"><p>کس نبوید گل خوشبوی به باغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ تو باغ دل جامی بس</p></div>
<div class="m2"><p>باشد از باغ تو بوییش هوس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بویی از باغ خودش روزی کن</p></div>
<div class="m2"><p>لذت از داغ خودش روزی کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منه از دام هواها بندش</p></div>
<div class="m2"><p>بگسل از هر هوسی پیوندش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر دلش نقش غم خویش نگار</p></div>
<div class="m2"><p>خاطرش بسته به هر نقش مدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخیه فقرزنش بر ژنده</p></div>
<div class="m2"><p>سازش از ذوق فنا دل زده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا چو سر بر زند از ژنده فقر</p></div>
<div class="m2"><p>مرده خود بود و زنده فقر</p></div></div>