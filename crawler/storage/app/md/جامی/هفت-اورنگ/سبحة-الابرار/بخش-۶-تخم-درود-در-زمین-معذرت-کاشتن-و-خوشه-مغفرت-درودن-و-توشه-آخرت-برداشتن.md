---
title: >-
    بخش ۶ - تخم درود در زمین معذرت کاشتن و خوشه مغفرت درودن و توشه آخرت برداشتن
---
# بخش ۶ - تخم درود در زمین معذرت کاشتن و خوشه مغفرت درودن و توشه آخرت برداشتن

<div class="b" id="bn1"><div class="m1"><p>فرخ آن روز که از مکمن راز</p></div>
<div class="m2"><p>بارگی راند به جولانگه ناز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علم جاه به بطحا افراخت</p></div>
<div class="m2"><p>مکه را سکه دولت نو ساخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو بی سایه اش از قدر بلند</p></div>
<div class="m2"><p>بر سر تشنه لبان سایه فکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ریگ از اکسیر قدومش زر شد</p></div>
<div class="m2"><p>بطن وادی صدف گوهر شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتاب سحر ایمان ویست</p></div>
<div class="m2"><p>نیر چاشتگه احسان ویست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشرقش مکه و مغرب یثرب</p></div>
<div class="m2"><p>پر ضیا مشرق ازو تا مغرب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد بر خوان نبوت یک شب</p></div>
<div class="m2"><p>دعوت گرسنه چشمان عرب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قرص مه را پی یک مشت لئیم</p></div>
<div class="m2"><p>به سر انگشت کرم کرد دو نیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست زین هیچ عجب تر عجبی</p></div>
<div class="m2"><p>که نسودند به آن قرص لبی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب دیگر ز قدم جان تا فرق</p></div>
<div class="m2"><p>بر درخشنده براقی چون برق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اشهبی همچو شهاب آتش پای</p></div>
<div class="m2"><p>نعل او چون مه نو گردون سای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گنبد خاک پس پشت فکن</p></div>
<div class="m2"><p>راند از آفاق برون گنبد زن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خرقه تن به سر عرش کشید</p></div>
<div class="m2"><p>خرقه را کند و به ذوالعرش رسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد از آن نور بقا دیده فروز</p></div>
<div class="m2"><p>آمد و خوابگهش گرم هنوز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود نور بصر شخص جهان</p></div>
<div class="m2"><p>چون بصر از نظر خویش نهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به یکی چشم زدن نور بصر</p></div>
<div class="m2"><p>می کند از همه افلاک گذر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آزمون را به سوی چرخ بلند</p></div>
<div class="m2"><p>چشم بگشای و همان لحظه ببند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بین که نور بصرت بی تک و تاز</p></div>
<div class="m2"><p>چون به گردون رود و آید باز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به قلم گر نرسید انگشتش</p></div>
<div class="m2"><p>بود لوح و قلم اندر مشتش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بود روحش قلم صنع ازل</p></div>
<div class="m2"><p>گر قلم نیست قلمزن چه خلل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از سواد و خط اگر دیده ببست</p></div>
<div class="m2"><p>به کمالش نرسد هیچ شکست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نور بود او و خط تیره ظلم</p></div>
<div class="m2"><p>شود نور و ظلم جمع به هم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چار یارش که ز گوهر کانند</p></div>
<div class="m2"><p>قصر دین را چو چهار ارکانند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صدق و عدل آوری و جود و حیاست</p></div>
<div class="m2"><p>که از ایشان به جهان مانده بجاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه مرضی همه راضی رفتند</p></div>
<div class="m2"><p>قرب حق را متقاضی رفتند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گشته در قرب حق اند اکنون گم</p></div>
<div class="m2"><p>رضی الله تعالی عنهم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اولین زاده قدرت قلم است</p></div>
<div class="m2"><p>که ز نوکش دو جهان یک رقم است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نه قلم بلکه یکی تازه نهال</p></div>
<div class="m2"><p>رسته از روضه اقلیم جمال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گوهر معنی خیرالبشر است</p></div>
<div class="m2"><p>که مر آن را شده تخم و ثمر است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سلک هستی چو درآید به شمار</p></div>
<div class="m2"><p>وی بود اول فکر آخر کار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صورتش گر چه ز آدم زاده</p></div>
<div class="m2"><p>معنیش اصل وجود ا فتاده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روشن است این بر هر فرزانه</p></div>
<div class="m2"><p>که ز هم زاد درخت و دانه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>قبله بنده و آزاد وی است</p></div>
<div class="m2"><p>علت غایی ایجاد وی است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از رخش نور ربایی همه را</p></div>
<div class="m2"><p>وز درش کار گشایی همه را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>طرفه نامش که به آن نامزد است</p></div>
<div class="m2"><p>کرده نعلین ز حرفین مد است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آدم اینک شرف سرمد را</p></div>
<div class="m2"><p>تاج سر کرده به بیادش مد را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گل شهر دو جهان است بلی</p></div>
<div class="m2"><p>هست شهری و گلی زو مثلی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گل که آمد عرق رخسارش</p></div>
<div class="m2"><p>نیست جز شبنمی از گلزارش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بود پیش از رقم تازه او</p></div>
<div class="m2"><p>بی صریر قلم آوازه او</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>لوح آثار قلم هیچ نداشت</p></div>
<div class="m2"><p>که به رخ حرف تمناش نگاشت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عرش را پای نه بر کرسی بود</p></div>
<div class="m2"><p>کز قدومش به خبر پرسی بود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا درآید به شتر گشته سوار</p></div>
<div class="m2"><p>بود گردون شتران کرده قطار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بودش ایام به ره بنشسته</p></div>
<div class="m2"><p>چار طاقی ز عناصر بسته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نورش از جبهه آدم بنمود</p></div>
<div class="m2"><p>سر نهادند ملایک به سجود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نوح در مهلکه طوفانی</p></div>
<div class="m2"><p>پشت ازو یافت به کشتیرانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بوی لطفش به براهیم رسید</p></div>
<div class="m2"><p>گلشن از آتش نمرود دمید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>طلعتش آتش موسی افروخت</p></div>
<div class="m2"><p>لبش احیا به مسیحا آموخت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>رفت در قافله فاقه خوشی</p></div>
<div class="m2"><p>صالح از قافله اش ناقه کشی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>رخت در زاویه فقر نهاد</p></div>
<div class="m2"><p>داد صد تخت سلیمان بر باد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>درس خوان ادب او ادریس</p></div>
<div class="m2"><p>خانه روب حرم او بلقیس</p></div></div>