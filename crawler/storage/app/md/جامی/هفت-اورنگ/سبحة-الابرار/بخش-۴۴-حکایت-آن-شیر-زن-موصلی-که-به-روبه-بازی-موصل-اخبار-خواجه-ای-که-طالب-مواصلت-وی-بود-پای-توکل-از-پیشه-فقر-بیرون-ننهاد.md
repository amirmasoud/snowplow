---
title: >-
    بخش ۴۴ - حکایت آن شیر زن موصلی که به روبه بازی موصل اخبار خواجه ای که طالب مواصلت وی بود پای توکل از پیشه فقر بیرون ننهاد
---
# بخش ۴۴ - حکایت آن شیر زن موصلی که به روبه بازی موصل اخبار خواجه ای که طالب مواصلت وی بود پای توکل از پیشه فقر بیرون ننهاد

<div class="b" id="bn1"><div class="m1"><p>بود مردانه زنی در موصل</p></div>
<div class="m2"><p>سر جانش به حقیقت واصل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو خورشید مؤنث در نام</p></div>
<div class="m2"><p>لیک در نور یقین مرد تمام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رو به محراب عبادت کرده</p></div>
<div class="m2"><p>چاک در پرده عادت کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه ره خورد به خود داده نه خفت</p></div>
<div class="m2"><p>خاطرش فرد ز همخوابی جفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مالداری ز بزرگان دیار</p></div>
<div class="m2"><p>در بزرگی نسب پاک عیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس فرستاد به وی کان سره زن</p></div>
<div class="m2"><p>در ره صدق و صفا نادره فن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آدمی فرد نشستن نه سزاست</p></div>
<div class="m2"><p>آن که از جفت مبراست خداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر نخوت مکش از همسریم</p></div>
<div class="m2"><p>تن فرو ده به زناشوهریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهرت ای رابعه ستر و جمال</p></div>
<div class="m2"><p>هر چه خواهی دهم از مال و منال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شیرزن عشوه روبه نخرید</p></div>
<div class="m2"><p>داد پیغام چو آن قصه شنید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که مرا گر به مثل بنده شوی</p></div>
<div class="m2"><p>همچو خاکم به ره افکنده شوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همگی ملک شود مال توام</p></div>
<div class="m2"><p>دست در هم دهد آمال توام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیک ازینها چو غباری خیزد</p></div>
<div class="m2"><p>وقت صافم به غبار آمیزد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حاش لله که به اینها نگرم</p></div>
<div class="m2"><p>راه اقبال بر اینها سپرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پایه فقر بود وایه من</p></div>
<div class="m2"><p>کی فتد بر دو جهان سایه من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مهر هر سفله کجا گیرم خوی</p></div>
<div class="m2"><p>سوی هر قبله کجا آرم روی</p></div></div>