---
title: >-
    بخش ۱۲۹ - مناجات در انتقال به خاتمه
---
# بخش ۱۲۹ - مناجات در انتقال به خاتمه

<div class="b" id="bn1"><div class="m1"><p>ای به لطف انجمن جان آرای</p></div>
<div class="m2"><p>تیغ مهرت چمن دل پیرای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست جودت ز ازل نخل نشان</p></div>
<div class="m2"><p>تا ابد بر سر ما نخل فشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه از خار ستم بینانیم</p></div>
<div class="m2"><p>زیر نخل تو رطب چینانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در رطب ریزیت از نحل کرم</p></div>
<div class="m2"><p>گر کشد خار ستم تیغ چه غم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کلک جامیت ز نخلت شاخی</p></div>
<div class="m2"><p>ریخته تازه رطب گستاخی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسزد زین رطب شهد آمیز</p></div>
<div class="m2"><p>کار محرور حسد جز پرهیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن زمان کش رود این کلک ز دست</p></div>
<div class="m2"><p>یابد این شاخ رطب ریز شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم دارد که به جای رطبش</p></div>
<div class="m2"><p>شهد ریزی ز شهادت به لبش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وان نفس کش ببرد عرق حیات</p></div>
<div class="m2"><p>تیغ ان اجل الله لآت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنی از همت رحمت املش</p></div>
<div class="m2"><p>ختم بر خیر کتاب اجلش</p></div></div>