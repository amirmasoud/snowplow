---
title: >-
    بخش ۵۱ - مناجات در انتقال از شکر و سپاسداری به خوف و ترسگاری
---
# بخش ۵۱ - مناجات در انتقال از شکر و سپاسداری به خوف و ترسگاری

<div class="b" id="bn1"><div class="m1"><p>ای کشیده به جهان خوان کرم</p></div>
<div class="m2"><p>حاضر خوان تو الوان نعم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نعم و شکر نعم هر دو ز توست</p></div>
<div class="m2"><p>نشود جز به تو این کار درست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکرگویان تو را جرم زبان</p></div>
<div class="m2"><p>یک نواله ست ازان خوان به دهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نواله ز نوا نیست جدا</p></div>
<div class="m2"><p>زان نواله ست جهانی به نوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه جامی بود از هیچ کسان</p></div>
<div class="m2"><p>زان نواله به نواییش رسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به آتش نکنی غور رسی</p></div>
<div class="m2"><p>به کسی کی رسد از هیچ کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جمال نعمش بینا کن</p></div>
<div class="m2"><p>به سپاس نعمش گویا کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز و شب با نعمش همدم دار</p></div>
<div class="m2"><p>به سپاس نعمش خرم دار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور کشد پا ز ره شکر ز طوف</p></div>
<div class="m2"><p>زخم بر دل زنش از خنجر خوف</p></div></div>