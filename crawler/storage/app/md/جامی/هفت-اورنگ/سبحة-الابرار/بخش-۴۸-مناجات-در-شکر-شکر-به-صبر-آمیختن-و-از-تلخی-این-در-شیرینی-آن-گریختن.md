---
title: >-
    بخش ۴۸ - مناجات در شکر شکر به صبر آمیختن و از تلخی این در شیرینی آن گریختن
---
# بخش ۴۸ - مناجات در شکر شکر به صبر آمیختن و از تلخی این در شیرینی آن گریختن

<div class="b" id="bn1"><div class="m1"><p>ای شکیبا نه دل ما از تو</p></div>
<div class="m2"><p>از همه صبر خوش الا از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبر بی تو ره بی دردان است</p></div>
<div class="m2"><p>صبر با تو روش مردان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از در قرب تو دوری مشکل</p></div>
<div class="m2"><p>وز جمال تو صبوری مشکل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبر بر قربت ازان مشکل تر</p></div>
<div class="m2"><p>رخ به خون دل ازان مشکل تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کرم مشکل ما آسان کن</p></div>
<div class="m2"><p>جای ما پیشگه احسان کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش گل زینت ظاهر ز تو یافت</p></div>
<div class="m2"><p>سر دل کشف سرایر ز تو یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزدا نقش گل از صفحه دل</p></div>
<div class="m2"><p>بنما نور دل از پرده گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کام جامی ز صبوری تلخ است</p></div>
<div class="m2"><p>عیشش از محنت دوری تلخ است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مپسند از دل غم فرجامش</p></div>
<div class="m2"><p>که به تلخی گذرد ایامش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا شود مرغ زبان آور شکر</p></div>
<div class="m2"><p>کام شیرین کنش از شکر شکر</p></div></div>