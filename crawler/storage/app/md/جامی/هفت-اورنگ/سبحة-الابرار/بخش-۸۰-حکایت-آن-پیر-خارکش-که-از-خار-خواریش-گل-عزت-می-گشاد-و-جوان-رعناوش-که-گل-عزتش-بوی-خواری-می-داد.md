---
title: >-
    بخش ۸۰ - حکایت آن پیر خارکش که از خار خواریش گل عزت می گشاد و جوان رعناوش که گل عزتش بوی خواری می داد
---
# بخش ۸۰ - حکایت آن پیر خارکش که از خار خواریش گل عزت می گشاد و جوان رعناوش که گل عزتش بوی خواری می داد

<div class="b" id="bn1"><div class="m1"><p>خارکش پیری با دلق درشت</p></div>
<div class="m2"><p>پشته خار همی برد به پشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لنگ لگان قدمی برمی داشت</p></div>
<div class="m2"><p>هر قدم دانه شکری می کاشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کای فرازنده این چرخ بلند</p></div>
<div class="m2"><p>وی نوازنده دلهای نژند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنم از جیب نظر تا دامن</p></div>
<div class="m2"><p>چه عزیزی که نکردی با من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دولت به رخم بگشادی</p></div>
<div class="m2"><p>تاج عزت به سرم بنهادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حد من نیست ثنایت گفتن</p></div>
<div class="m2"><p>گوهر شکر عطایت سفتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوجوانی به جوانی مغرور</p></div>
<div class="m2"><p>رخش پندار همی راند ز دور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آمد آن شکرگزاریش به گوش</p></div>
<div class="m2"><p>گفت کای پیر خرف گشته خموش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خار بر پشت زنی زینسان گام</p></div>
<div class="m2"><p>دولتت چیست عزیزیت کدام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمر در خارکشی باخته ای</p></div>
<div class="m2"><p>عزت از خواری نشناخته ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیر گفتا که چه عزت زین به</p></div>
<div class="m2"><p>که نیم بر در تو بالین نه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کای فلان چاشت بده یا شامم</p></div>
<div class="m2"><p>نان و آبی که خورم و آشامم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شکر گویم که مرا خوار نساخت</p></div>
<div class="m2"><p>به خسی چون تو گرفتار نساخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به ره حرص شتابنده نکرد</p></div>
<div class="m2"><p>به در شاه و گدا بنده نکرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>داد با این همه افتادگیم</p></div>
<div class="m2"><p>عز آزادی و آزادگیم</p></div></div>