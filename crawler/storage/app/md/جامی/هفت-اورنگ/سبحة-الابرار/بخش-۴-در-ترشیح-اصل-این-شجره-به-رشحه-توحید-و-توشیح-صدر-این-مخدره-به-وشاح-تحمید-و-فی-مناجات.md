---
title: >-
    بخش ۴ - در ترشیح اصل این شجره به رشحه توحید و توشیح صدر این مخدره به وشاح تحمید و فی مناجات
---
# بخش ۴ - در ترشیح اصل این شجره به رشحه توحید و توشیح صدر این مخدره به وشاح تحمید و فی مناجات

<div class="b" id="bn1"><div class="m1"><p>انما الله اله واحد</p></div>
<div class="m2"><p>فهو المنعم و هوالحامد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می نهد شکر نعمت به دهان</p></div>
<div class="m2"><p>می کند شکر گزاری به زبان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکر فضلش چو عطای دگر است</p></div>
<div class="m2"><p>باعث شکر و ثنای دگر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی شود در نظر خرده شناس</p></div>
<div class="m2"><p>منتهی سلسله شکر و سپاس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که جانی بودش در بدنی</p></div>
<div class="m2"><p>گر شود هر سر مویش دهنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باشد از هر دهنی گشته زبان</p></div>
<div class="m2"><p>هر سر موی به صد نطق و بیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابدالدهر سخن ساز کنند</p></div>
<div class="m2"><p>پرده از نوی و کهن باز کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نتوانند که آرند بجای</p></div>
<div class="m2"><p>شکر مویی ز کرم های خدای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن به تاریخ قدم از همه پیش</p></div>
<div class="m2"><p>وان به توقیع کرم از همه پیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن که بی لوح و قلم کرده رقم</p></div>
<div class="m2"><p>بر سر لوح عدم حرف قلم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشمه قاف قلم تا نگشاد</p></div>
<div class="m2"><p>موج فیض از دل دریا نگشاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه فلک با همه اختر که در اوست</p></div>
<div class="m2"><p>نه صدف با همه گوهر که در اوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه زان جنبش جود افتاده ست</p></div>
<div class="m2"><p>که به صحرای وجود افتاده ست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیلگون چرخ به پشت به خمش</p></div>
<div class="m2"><p>یک حباب است ز نیل کرمش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رنگ نیلی حباب است دلیل</p></div>
<div class="m2"><p>که پدید آمده از لجه نیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زانچه در کارگه بوقلمون</p></div>
<div class="m2"><p>از شکاف قلم آورد برون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طرفه نونیست نگون چرخ برین</p></div>
<div class="m2"><p>نقطه حلقه آن گوی زمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر که پی برده به این خوش رقم است</p></div>
<div class="m2"><p>عارف نکته «نون والقلم » است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرد راهش که رود پی زده گم</p></div>
<div class="m2"><p>رخش او راست فلک کاسه سم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اینک اینک بنگر شاهد حال</p></div>
<div class="m2"><p>میخ انجم زده و نعل هلال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا درین طبع فریبنده سرای</p></div>
<div class="m2"><p>ننهد حادثه زلزله پای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بهر سر کوبیش از سنگ جبال</p></div>
<div class="m2"><p>کرده دامان زمین مالامال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بحر جودش که فلک فلک آمد</p></div>
<div class="m2"><p>بانگ موجش «لمن الملک » آمد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گوش ماهیش چو این حرف شنید</p></div>
<div class="m2"><p>با خموشی ز سخن چاره ندید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از زبان گر چه تهی داشت دهان</p></div>
<div class="m2"><p>«لله الواحد» ش آمد به زبان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>واحد است او و ز ماهی تا ماه</p></div>
<div class="m2"><p>همه بر وحدت اویند گواه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نیست در رشته وحدت خم و پیچ</p></div>
<div class="m2"><p>همه او آمد و باقی همه هیچ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هست در دایره لیل و نهار</p></div>
<div class="m2"><p>بابی از رحمت او فصل بهار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باغ پر زیب ز صنعتوریش</p></div>
<div class="m2"><p>آب آیینه ز روشنگریش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باد ازو غالیه سایی اندوز</p></div>
<div class="m2"><p>مرغ ازو نغمه سرایی آموز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بست جیب سمن از غنچه گره</p></div>
<div class="m2"><p>بافت گرد چمن از سبزه گره</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زوست محروس به فانوس سپهر</p></div>
<div class="m2"><p>از دم حادثه شمع مه و مهر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به اولی اجنحه مرغان فصیح</p></div>
<div class="m2"><p>داده دانه پی قوت از تسبیح</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دست صنعش گل آدم چو سرشت</p></div>
<div class="m2"><p>به خلافتگریش نام نوشت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تاج تکریم نهاد از کرمش</p></div>
<div class="m2"><p>داد از «علم آدم » علمش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به سر مسند تعلیم نشست</p></div>
<div class="m2"><p>طاعنان را دهن از طعن ببست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همه را کرد ترشح ز انا</p></div>
<div class="m2"><p>رشح «سبحانک لا علم لنا»</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ساخت محراب ملایک رویش</p></div>
<div class="m2"><p>سجده بردند یکایک سویش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بجز آن آتشی دیو نژاد</p></div>
<div class="m2"><p>که به مسجودی او سر ننهاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کور دل بود به میل انا خیر</p></div>
<div class="m2"><p>دیده نگشاد به خیریت غیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون نه گردن نهی آمد فن او</p></div>
<div class="m2"><p>لعن شد طوق نه گردن او</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پشت در کینه وری محکم کرد</p></div>
<div class="m2"><p>روی در وسوسه آدم کرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دانه را در نظرش تزیین داد</p></div>
<div class="m2"><p>ره به دام خطرش تلقین داد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سوی دانه ز طمع گام نهاد</p></div>
<div class="m2"><p>دانه اش در دهن دام نهاد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گرد عصیانش به رخساره نشست</p></div>
<div class="m2"><p>پشت عهدش ز عصی خرد شکست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زلتش پرده ظلمت افراشت</p></div>
<div class="m2"><p>توبه اش بانگ «ظلمنا» برداشت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تابش مشعله «تاب علیه »</p></div>
<div class="m2"><p>ریخت انوار هدی بین یدیه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ما که در ظلمت هر مشغله ایم</p></div>
<div class="m2"><p>طالب نور ازان مشعله ایم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خیز جامی که مناجات کنیم</p></div>
<div class="m2"><p>روی در قبله حاجات کنیم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بو کز آن مشعله نوری برسد</p></div>
<div class="m2"><p>جان ز نورش به سروری برسد</p></div></div>