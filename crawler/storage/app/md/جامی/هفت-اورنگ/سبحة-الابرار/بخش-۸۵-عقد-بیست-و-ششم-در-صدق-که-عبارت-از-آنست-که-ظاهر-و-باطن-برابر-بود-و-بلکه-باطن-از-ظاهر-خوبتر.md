---
title: >-
    بخش ۸۵ - عقد بیست و ششم در صدق که عبارت از آنست که ظاهر و باطن برابر بود و بلکه باطن از ظاهر خوبتر
---
# بخش ۸۵ - عقد بیست و ششم در صدق که عبارت از آنست که ظاهر و باطن برابر بود و بلکه باطن از ظاهر خوبتر

<div class="b" id="bn1"><div class="m1"><p>ای گرو کرده زبان را به دروغ</p></div>
<div class="m2"><p>برده بهتان ز کلام تو فروغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نه شایسته هر دیده ور است</p></div>
<div class="m2"><p>که زبانت دگر و دل دگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ره صدق و صفا دوری چند</p></div>
<div class="m2"><p>دل قیری رخ کافوری چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی در قاعده احسان کن</p></div>
<div class="m2"><p>ظاهر و باطن خود یکسان کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکدل و یکجهت و یکرو باش</p></div>
<div class="m2"><p>وز دورویان جهان یکسو باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کجی خیزد هر جا خللیست</p></div>
<div class="m2"><p>راستی رستی نیکو مثلیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راست جو راست نگر راست گزین</p></div>
<div class="m2"><p>راست گو راست شنو راست نشین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیر اگر راست رود بر هدف است</p></div>
<div class="m2"><p>ور رود کج ز هدف بر طرف است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رورقم های «الف بی » بنگر</p></div>
<div class="m2"><p>که الف از همه باشد برتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رو بنه تخته ابجد به کنار</p></div>
<div class="m2"><p>که درآید الف اول به شمار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر سبب جوید حکمت طلبی</p></div>
<div class="m2"><p>نیست جز راستی آن را سببی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>راست رو است که سرور باشی</p></div>
<div class="m2"><p>در حساب از همه برتر باشی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صدق اکسیر مس هستی توست</p></div>
<div class="m2"><p>پایه افراز فرو دستی توست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اثر کذب بود هیچکسی</p></div>
<div class="m2"><p>به کسی گر رسی از صدق رسی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صبح کاذب زند از کذب نفس</p></div>
<div class="m2"><p>نور او یک دو نفس باشد و بس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صبح صادق چو بود صدق پسند</p></div>
<div class="m2"><p>علم نورش از آنست بلند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل اگر صدق پسندیت دهد</p></div>
<div class="m2"><p>بر همه خلق بلندیت دهد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگر از کذب گزیند علمی</p></div>
<div class="m2"><p>علم او بنشیند به دمی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صدق پیش آر که صدیق شوی</p></div>
<div class="m2"><p>گوهر لجه تحقیق شوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر چه صدیق نبی راست خلف</p></div>
<div class="m2"><p>باشدش بر دگر اصناف شرف</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر بر این قاعده برهان خواهی</p></div>
<div class="m2"><p>به که برهانش ز قرآن خواهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنست صدیق که دل صاف شود</p></div>
<div class="m2"><p>دعوی او همه انصاف شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وعده او به وفا انجامد</p></div>
<div class="m2"><p>دلش از غش به صفا آرامد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در درون تخم امانت فکند</p></div>
<div class="m2"><p>وز برون خار خیانت بکند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر فتد بیخ نفاق از گل او</p></div>
<div class="m2"><p>سرزند شاخ وفاق از دل او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه در او رنگ تکلف باشد</p></div>
<div class="m2"><p>نه در او بوی تصلف باشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دامن همت صدیقان گیر</p></div>
<div class="m2"><p>در ره خدمت صدیقان میر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بو که بر جان تو خالی ز قصور</p></div>
<div class="m2"><p>از صفای دلشان ریزد نور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مس قلب تو ازان زر گردد</p></div>
<div class="m2"><p>سنگ بی قدر تو گوهر گردد</p></div></div>