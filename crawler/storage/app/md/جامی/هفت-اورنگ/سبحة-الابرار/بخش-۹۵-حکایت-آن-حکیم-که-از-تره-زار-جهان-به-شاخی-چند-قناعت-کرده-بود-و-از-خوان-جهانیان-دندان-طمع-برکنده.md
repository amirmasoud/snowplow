---
title: >-
    بخش ۹۵ - حکایت آن حکیم که از تره زار جهان به شاخی چند قناعت کرده بود و از خوان جهانیان دندان طمع برکنده
---
# بخش ۹۵ - حکایت آن حکیم که از تره زار جهان به شاخی چند قناعت کرده بود و از خوان جهانیان دندان طمع برکنده

<div class="b" id="bn1"><div class="m1"><p>می شد آن خاصگی شاه به دشت</p></div>
<div class="m2"><p>بر کنار تره زاری بگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تره کاری ز قضا بر لب جوی</p></div>
<div class="m2"><p>بود ز آلودگی گل تره شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان تره هر چه همی ماند در آب</p></div>
<div class="m2"><p>طعمه می ساخت حکیمی به شتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاصگی گفت بدو کای سره مرد</p></div>
<div class="m2"><p>کس ندیدم که بدینسان تره خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تره تو که نه نان دیده نه دوغ</p></div>
<div class="m2"><p>ندهد کار تو را هیچ فروغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چو ما خدمتی شاه شوی</p></div>
<div class="m2"><p>صاحب مرتبه و جاه شوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دسته تره که بر خوان بودت</p></div>
<div class="m2"><p>پهلوی بره بریان بودت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لقمه بره که با تره خوری</p></div>
<div class="m2"><p>به ز هر تره که بی بره خوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت با خاصگی آن مرد حکیم</p></div>
<div class="m2"><p>کای ز جاه آمده در چاه مقیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چو ما راه قناعت سپری</p></div>
<div class="m2"><p>به حرمگاه قناعت گذری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باشد از خوان جهان تره بست</p></div>
<div class="m2"><p>خوردن بره نیفتد هوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کمر خدمت شاهت چو کمند</p></div>
<div class="m2"><p>نفکند گردن اقبال به بند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاه از خلعت شاهی بیرون</p></div>
<div class="m2"><p>نیست جز چون تو یکی مرد زبون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیش شمشیر سر افکنده شوی</p></div>
<div class="m2"><p>به که پیش چو خودی بنده شوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در دیاری که ز فقر آبادیست</p></div>
<div class="m2"><p>بندگی خاک ره آزادیست</p></div></div>