---
title: >-
    بخش ۸۲ - عقد بیست و پنجم در فتوت که بار خود از گردن خلق نهادن است و زیر بار خلق ایستادن
---
# بخش ۸۲ - عقد بیست و پنجم در فتوت که بار خود از گردن خلق نهادن است و زیر بار خلق ایستادن

<div class="b" id="bn1"><div class="m1"><p>ای که از طبع فرومایه خویش</p></div>
<div class="m2"><p>می زنی گام پی وایه خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاطر از وایه خود خالی کن</p></div>
<div class="m2"><p>زین هنر پایه خود عالی کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر خود گرمی جز سردی نیست</p></div>
<div class="m2"><p>سردی آیین جوانمردی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند روزی ز قوی دینان باش</p></div>
<div class="m2"><p>در پی حاجت مسکینان باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع شو شمع که خود را سوزی</p></div>
<div class="m2"><p>تا به آن بزم کسان افروزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با بد و نیک نکوکاری ورز</p></div>
<div class="m2"><p>شیه یاری و غمخواری ورز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابر شو تا که چو باران ریزی</p></div>
<div class="m2"><p>بر گل و خس همه یکسان ریزی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم بر لغزش یاران مفکن</p></div>
<div class="m2"><p>به ملامت دل یاران مشکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در گذر از گنه و از دگران</p></div>
<div class="m2"><p>چون ببینی گنهی در گذران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باش چون بحر ز آلایش پاک</p></div>
<div class="m2"><p>ببر آلایش از آلایشناک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو دیده به سوی خویش مبین</p></div>
<div class="m2"><p>خویش را از دگران بیش مبین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس عمارت که بود خانه رنج</p></div>
<div class="m2"><p>بس خرابی که شود پرده گنج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با همه باش به صلح آوریی</p></div>
<div class="m2"><p>که نگنجد به میان داوریی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همچو آن بیخته خاک از خس و خار</p></div>
<div class="m2"><p>که زند آب بر آن ابر بهار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کف پا رانبود زان دردی</p></div>
<div class="m2"><p>پس پا را نرسد زان گردی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور سوی داوریت افتد رای</p></div>
<div class="m2"><p>به که با خود کنی از بهر خدای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بت خود را بشکن خوار و ذلیل</p></div>
<div class="m2"><p>ناور شو به فتوت چو خلیل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بت تو نفس هواپرور توست</p></div>
<div class="m2"><p>که به صد گونه خطا رهبر توست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسط کن بر همه کس خوان کرم</p></div>
<div class="m2"><p>بذل کن بر همه همیان درم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر براهیمی و گر زردشتی</p></div>
<div class="m2"><p>روی در هم مکش از هم پشتی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بازکش پای ز آزار همه</p></div>
<div class="m2"><p>دست بگشای به ایثار همه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر چه بدهی به کسی باز مجوی</p></div>
<div class="m2"><p>دل ز اندیشه آن پاک بشوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنچه بخشند چه بسیار و چه کم</p></div>
<div class="m2"><p>نیست بر گشتن ازان طور کرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>طفل چون صاحب احسان گردد</p></div>
<div class="m2"><p>زود از داده پشیمان گردد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر چه خندان بدهد نتواند</p></div>
<div class="m2"><p>که دگر گریه کنان نستاند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا توانی مگشا جیب کسان</p></div>
<div class="m2"><p>منگر در هنر و عیب کسان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عیب بینی هنر چندان نیست</p></div>
<div class="m2"><p>هدف قصد هنرمندان نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر چه نامش نه پسندیده کنی</p></div>
<div class="m2"><p>بهتر آنست که نادیده کنی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دل ز اندیشه آن داری دور</p></div>
<div class="m2"><p>دیده از دیدن آن سازی کور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بو که از چون تو نکو کرداری</p></div>
<div class="m2"><p>به دل کس نرسد آزاری</p></div></div>