---
title: >-
    بخش ۹۲ - حکایت اعرابی که در معامله احسان و کرم بدره دینار و درم مهمانان به تخویف از زخم نیزه باز پس گردانید
---
# بخش ۹۲ - حکایت اعرابی که در معامله احسان و کرم بدره دینار و درم مهمانان به تخویف از زخم نیزه باز پس گردانید

<div class="b" id="bn1"><div class="m1"><p>آن عرابی به شتر قانع و شیر</p></div>
<div class="m2"><p>در یکی بادیه شد مرحله گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگهان جمعی از ارباب قبول</p></div>
<div class="m2"><p>شب در آن مرحله کردند نزول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاست مردانه به مهمانیشان</p></div>
<div class="m2"><p>شتری برد به قربانیشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز دیگر ره پیشینه سپرد</p></div>
<div class="m2"><p>بهر ایشان شتر دیگر برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عذر گفتند که باقیست هنوز</p></div>
<div class="m2"><p>چیزی از داده دوشین امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت حاشا که ز پس مانده دوش</p></div>
<div class="m2"><p>دیگ جود آیدم امروز به جوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز دیگر به کرم ورزی پشت</p></div>
<div class="m2"><p>کرد محکم شتری دیگر کشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد ازان بر شتری راکب شد</p></div>
<div class="m2"><p>بهر کاری ز میان غایب شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قوم چون خوان نوالش خوردند</p></div>
<div class="m2"><p>عزم رحلت ز دیارش کردند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست احسان و کرم بگشادند</p></div>
<div class="m2"><p>بدره زر به عیالش دادند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دور ناگشته هنوز از دیده</p></div>
<div class="m2"><p>میهمانان کرم ورزیده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آمد آن طرفه عرابی از راه</p></div>
<div class="m2"><p>دید آن بدره در آن منزلگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت کین چیست زبان بگشودند</p></div>
<div class="m2"><p>صورت حال بدو بنمودند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاست بدره به کف و نیزه به دوش</p></div>
<div class="m2"><p>وز پی قوم برآورد خروش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کای سفیهان خطا اندیشه</p></div>
<div class="m2"><p>وی لئیمان خساست پیشه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود مهمانیم از محض کرم</p></div>
<div class="m2"><p>نه چو بیع از پی دینار و درم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>داده خویش ز من بستانید</p></div>
<div class="m2"><p>پس رواحل به ره خود رانید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور نه تا جان برود از تنتان</p></div>
<div class="m2"><p>در تن از نیزه کنم روزنتان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>داده خویش گرفتند و گذشت</p></div>
<div class="m2"><p>وان عرابی ز قفاشان برگشت</p></div></div>