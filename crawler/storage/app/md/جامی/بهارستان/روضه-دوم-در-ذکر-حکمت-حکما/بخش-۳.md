---
title: >-
    بخش ۳
---
# بخش ۳

<div class="n" id="bn1"><p>یکی از حکما گفته است: چهل دفتر در حکمت نوشتم و به آن منتفع نگشتم، چهل کلمه از آن اختیار کردم، از آن نیز بهره به دست نیاوردم چهار کلمه از آن برگزیدم در آن یافتم آنچه می طلبیدم:</p></div>
<div class="n" id="bn2"><p>اول آن که زنان را چون مردان محل اعتماد نگردان، زیرا که زن اگر چه از قبیله معتمدان آید از آن قبیل نیست که معتمدی را شاید.</p></div>
<div class="b" id="bn3"><div class="m1"><p>عقل زن ناقص است و دینش نیز</p></div>
<div class="m2"><p>هرگزش کامل اعتقاد مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بد است از وی اعتبار مگیر</p></div>
<div class="m2"><p>ور نکو بر وی اعتماد مکن</p></div></div>
<div class="n" id="bn5"><p>دویم آن که به مال مغرور مشو اگر چه بسیار بود، زیرا که عاقبت پایمال حوادث روزگار شود.</p></div>
<div class="b" id="bn6"><div class="m1"><p>مغرور مشو به مال چون بی خبران</p></div>
<div class="m2"><p>زیرا که بود مال چو ابر گذران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابر گذران اگر چه گوهر بارد</p></div>
<div class="m2"><p>خاطر ننهد مرد خردمند بر آن</p></div></div>
<div class="n" id="bn8"><p>سیم آن که اسرار نهان داشتنی خود را به هیچ دوست در میان منه، زیراکه بسیار باشد که در دوستی خلل افتد و به دشمنی بدل گردد.</p></div>
<div class="b" id="bn9"><div class="m1"><p>ای پسر سری کش از دشمن نهفتن لازم است</p></div>
<div class="m2"><p>به که از افشای آن با دوستان کم دم زنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده ام بسیار از سیر سپهر کج نهاد</p></div>
<div class="m2"><p>دوستان دشمن شوند و دوستیها دشمنی</p></div></div>
<div class="n" id="bn11"><p>چهارم آن که جز علمی را فرانگیری که به ترک آن بزهمند میری از فضولی بگریز و آنچه ضروریست در آن آویز.</p></div>
<div class="b" id="bn12"><div class="m1"><p>علمی که ناگزیر تو باشد بدان گرای</p></div>
<div class="m2"><p>وان را کز آن گزیر بود جستجو مکن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وان دم که حاصل تو شود علم ناگزیر</p></div>
<div class="m2"><p>غیر از عمل به موجب آن آرزو مکن</p></div></div>