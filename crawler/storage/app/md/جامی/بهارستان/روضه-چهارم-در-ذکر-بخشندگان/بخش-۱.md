---
title: >-
    بخش ۱
---
# بخش ۱

<div class="n" id="bn1"><p>جود بخشیدن چیزیست بایستی بی ملاحظه غرضی و مطالبه عوضی، اگر چه آن غرض یا عوض ثنای جمیل یا ثواب جزیل باشد.</p></div>
<div class="b" id="bn2"><div class="m1"><p>کیست کریم آن که نه بهر جزاست</p></div>
<div class="m2"><p>هر کرمی کاید ازو در وجود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه بود بهر ثنا و ثواب</p></div>
<div class="m2"><p>بیع و شری گیر نه احسان و جود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که مقصودش از کرم آنست</p></div>
<div class="m2"><p>که برآرد به عالم آوازه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشد از مصر جود و شهر کرم</p></div>
<div class="m2"><p>خانه او برون دروازه</p></div></div>