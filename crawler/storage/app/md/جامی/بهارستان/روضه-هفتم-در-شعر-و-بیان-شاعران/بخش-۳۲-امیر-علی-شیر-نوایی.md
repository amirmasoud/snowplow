---
title: >-
    بخش ۳۲ - امیر علی شیر نوایی
---
# بخش ۳۲ - امیر علی شیر نوایی

<div class="n" id="bn1"><p>میرنوایی - رحمه الله، و صاحب دولتی که زمان ما به وجود شریف او مشرف است هر چند پایه قدر وی نظر به مراتب جاه و حشمت و قرب پادشاه صاحب شوکت و قیاس به مناقب معنوی از فضل و ادب و فضایل موهوب و مکتسب از آن بلندتر است که وی را به حسن شعر تعریف کنند و به جودت نظم توصیف.</p></div>
<div class="n" id="bn2"><p>اما چون خاطر شریفش به واسطه کسب فضیلت تواضع و کسر نفس به آن فرود آمده است که خود را در سلک این طایفه منخرط گردانیده است دیگران را حجاب تحاشی از آن معنی که وی را از طبقه ایشان دارند و از زمره ایشان شمارند مرتفع گشته، اما انصاف آن است که هر جا این طایفه باشند وی سر باشد و هرگاه نام این طبقه نویسند وی سر دفتر، چنانکه این معما به اسم شریفش منبی از این معنی است:</p></div>
<div class="b" id="bn3"><div class="m1"><p>علی سیر الافاضل سرت دهرا</p></div>
<div class="m2"><p>و احرزت الفضائل بالفواضل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و باسمک فقت اهل الفضل طرا</p></div>
<div class="m2"><p>لذا صورته فوق الافاضل</p></div></div>
<div class="n" id="bn5"><p>و چون گوهر نامش بزرگتر از آن است که هر محل از نظم صدف آن تواند بود و هر مقام از شعر شرف آن تواند یافت تخلص اشعارش به آنچه از این معمای دیگر مفهوم می گردد نامزد گشته،</p></div>
<div class="b" id="bn6"><div class="m1"><p>کنه نامش در تخلصها نیابد هیچ کس</p></div>
<div class="m2"><p>بر لب یابندگان از وی نوایی دان و بس</p></div></div>
<div class="n" id="bn7"><p>و اگر چه وی را بحسب قوت طبیعت و وسعت قابلیت هر دو نوع شعر ترکی و فارسی میسر است، اما میل طبع وی به ترکی از فارسی بیشتر است و غزلیات وی به آن زبان از ده هزار زیادت خواهد بود.</p></div>
<div class="n" id="bn8"><p>و مثنویاتی که در مقابله خمسه نظامی وقوع یافته به سی هزار است و گوهر نظم نسفته است و از جمله اشعار فارسی وی است قصیده ای که در جواب قصیده خسرو است که مسماست به دریای ابرار واقع شده و مشتمل است بر بسیاری از معانی دقیقه و خیالات لطیفه، مطلعش این است:</p></div>
<div class="b" id="bn9"><div class="m1"><p>آتشین لعلی که تاج خسروان را زیور است</p></div>
<div class="m2"><p>اخگری بهر خیال خام پختن در سر است</p></div></div>
<div class="n" id="bn10"><p>و این رباعی را در تهنیت قدوم بعضی آیندگان از سفر حجاز در رقعه ای نوشته بود:</p></div>
<div class="b" id="bn11"><div class="m1"><p>انصاف بده ای فلک مینافام</p></div>
<div class="m2"><p>تا زین دو کدام خوبتر کرد خرام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خورشید جهانتاب تو از جانب صبح</p></div>
<div class="m2"><p>یا ماه جهانگرد من از جانب شام</p></div></div>
<div class="n" id="bn13"><p>و این رباعی را در رقعه ای دیگر:</p></div>
<div class="b" id="bn14"><div class="m1"><p>این نامه نه نامه دافع درد من است</p></div>
<div class="m2"><p>آرام درون رنج پرورد من است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تسکین دل گرم و دم سرد من است</p></div>
<div class="m2"><p>یعنی خبر از ماه جهانگرد من است</p></div></div>
<div class="n" id="bn16"><p>و این رباعی دیگر را به تجدید در رقعه ای دیگر:</p></div>
<div class="b" id="bn17"><div class="m1"><p>گر در دیرم به گفتگویت باشم</p></div>
<div class="m2"><p>ور در حرمم به جستجویت باشم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در وقت حضور رو به رویت باشم</p></div>
<div class="m2"><p>در غیبت روی دل به سویت باشم</p></div></div>