---
title: >-
    بخش ۶ - عسجدی
---
# بخش ۶ - عسجدی

<div class="n" id="bn1"><p>عسجدی - رحمه الله تعالی، وی از مرو است و از جمله مادحان یمین الدوله بود و در تهنیت فتح وی مر هندوستان را قصیده ای دارد که مطلعش این است:</p></div>
<div class="b" id="bn2"><div class="m1"><p>تا شاه خورده بین سفر سومنات کرد</p></div>
<div class="m2"><p>کردار خویش را علم معجزات کرد</p></div></div>
<div class="n" id="bn3"><p>و در صفت خربوزه می گوید:</p></div>
<div class="b" id="bn4"><div class="m1"><p>آن زبرجد رنگ مشکین بوی و طعمش طعم شهد</p></div>
<div class="m2"><p>رنگ دیبا دارد او گویی و بوی عود خام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ببریدی شود هر یک ازان ده ماه نو</p></div>
<div class="m2"><p>ور نبری باشد اندر ذات خود ماه تمام</p></div></div>