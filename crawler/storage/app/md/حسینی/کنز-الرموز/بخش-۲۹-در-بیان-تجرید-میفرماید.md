---
title: >-
    بخش ۲۹ - در بیان تجرید میفرماید
---
# بخش ۲۹ - در بیان تجرید میفرماید

<div class="b" id="bn1"><div class="m1"><p>چیست تجرید از علایق پاک شو</p></div>
<div class="m2"><p>در ره آزادگان چالاک شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو مرغان بسته دانه مباش</p></div>
<div class="m2"><p>مبتلای خویش و بیگانه مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو گل خندان و بیرون شو ز پوست</p></div>
<div class="m2"><p>گر تو را معنی تجریدی ازوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در لب دریا به غواصان نگر</p></div>
<div class="m2"><p>کو ب ه تجرید آورد چندان گهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مجرد شد ز نقد و نسیه مرد</p></div>
<div class="m2"><p>او برآورد از فلک یکبار گرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کم زن ای دل گر همی خواهی کمال</p></div>
<div class="m2"><p>سر این معنیست انفق یا بلال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه در تجرید مرد فرد نیست</p></div>
<div class="m2"><p>در طریق اهل معنی مرد نیست</p></div></div>