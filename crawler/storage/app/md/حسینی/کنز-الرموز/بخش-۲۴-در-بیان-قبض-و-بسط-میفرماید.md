---
title: >-
    بخش ۲۴ - در بیان قبض و بسط میفرماید
---
# بخش ۲۴ - در بیان قبض و بسط میفرماید

<div class="b" id="bn1"><div class="m1"><p>در محبت چون زدی گام نخست</p></div>
<div class="m2"><p>قبض و بسط از گردش احوال تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر فتوحی کز بر جانان رسد</p></div>
<div class="m2"><p>بیدلان را مژده درمان رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشکفد گلها ز باغ خوشدلی</p></div>
<div class="m2"><p>روی دل گردد ز انده صیقلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ز شادی چون شود مست و خراب</p></div>
<div class="m2"><p>نفس را بوئی رساند از شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرط باشد هر که میگیرد بدست</p></div>
<div class="m2"><p>خاک را از جرعۀ سازند مست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس را از جرعه آرد در خوشی</p></div>
<div class="m2"><p>دست بردارد ز بهر سرکشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عزت عشقش کشد در پیچ و خم</p></div>
<div class="m2"><p>آن همه شادی بدل گردد بغم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قسم او گردد ز باغ روزگار</p></div>
<div class="m2"><p>هرگلی را بر جگر صد گونه خار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس گل را باشد این معنی عیان</p></div>
<div class="m2"><p>مرغ دل را برتر آمد آشیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راست پرسی این همه هستی تست</p></div>
<div class="m2"><p>این همه درد سر از مستی تست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این سر پر درد را گر آگهی</p></div>
<div class="m2"><p>در گریبان فناکش تا رهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیستی جولانگه اهل دلست</p></div>
<div class="m2"><p>شاه راه عاشقان کاملست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان عاشق دوسترا طالب شود</p></div>
<div class="m2"><p>نور حق باهستیش غالب شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت مردی کاندرین ره کاملست</p></div>
<div class="m2"><p>نیستی راهست و هستی منزلست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ره مخوفست ای غریب هر دری</p></div>
<div class="m2"><p>جهد میکن تا ازین ره بگذری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون فنا گردی فنا اندر فنا</p></div>
<div class="m2"><p>از بقای حق رسی اندر بقا</p></div></div>