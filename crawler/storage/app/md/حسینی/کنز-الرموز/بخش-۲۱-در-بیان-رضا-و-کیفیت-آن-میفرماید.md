---
title: >-
    بخش ۲۱ - در بیان رضا و کیفیت آن میفرماید
---
# بخش ۲۱ - در بیان رضا و کیفیت آن میفرماید

<div class="b" id="bn1"><div class="m1"><p>از رضا خود نیست بهتر منزلی</p></div>
<div class="m2"><p>گوی این بیامد هر دلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اختیار خود بنه باری نخست</p></div>
<div class="m2"><p>پس رضا اندر میان بربند چست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تو از علم حقیقی غافلی</p></div>
<div class="m2"><p>از چنین دار الأدب بیحاصلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نۀ فارغ زاندوه جهان</p></div>
<div class="m2"><p>کی شوی دانای این حرف نهان</p></div></div>