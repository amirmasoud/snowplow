---
title: >-
    بخش ۲۸ - در بیان تفرید میفرماید
---
# بخش ۲۸ - در بیان تفرید میفرماید

<div class="b" id="bn1"><div class="m1"><p>مرد فردازنور وحدت بهره مند</p></div>
<div class="m2"><p>نی قبول و رد خلقش پایبند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرصۀ میدان او را حال نی</p></div>
<div class="m2"><p>دید او را دیدن افعال نی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ وحدت ز آشیان حق پرد</p></div>
<div class="m2"><p>همچو برق آید بزودی بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلبل جان از قفس پران شود</p></div>
<div class="m2"><p>گه بخندد مرد و گه گریان شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه جمال دوست بردارد نقاب</p></div>
<div class="m2"><p>گه زحسن عزتش گردد حجاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جذبه حق در رباید از خودش</p></div>
<div class="m2"><p>تا ب ه علیین برآرد م س ندش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این سخن چون همدم طالب شود</p></div>
<div class="m2"><p>گاه مغلوب و گهی غالب شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه مغلوبست محبوس خودست</p></div>
<div class="m2"><p>اندرین ره مشکل او بیحد است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه غالب شد پرید از دام خویش</p></div>
<div class="m2"><p>در حریم قدس کرد آرام خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حال پستی دار ملک ابتلاست</p></div>
<div class="m2"><p>مهره ها در ششدربازی دعاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پی شوق هرکه نوشد جام او</p></div>
<div class="m2"><p>در جهان با حق بود آرام او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هیبت حسنش چو بربودت ز خویش</p></div>
<div class="m2"><p>پرده چشم تو بردار د ز پیش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرچه غیر است از میان بیرون شود</p></div>
<div class="m2"><p>پس امید از بیم مرد افزون شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مجلس پر نقش آمد این مقام</p></div>
<div class="m2"><p>عشق بازان را نشاط آمد مدام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مایه سودا از این بازار خاست</p></div>
<div class="m2"><p>پس کلیم الله از این دیدار خواست</p></div></div>