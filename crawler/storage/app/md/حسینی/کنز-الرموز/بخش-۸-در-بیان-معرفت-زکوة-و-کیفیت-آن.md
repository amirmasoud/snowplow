---
title: >-
    بخش ۸ - در بیان معرفت زکوة و کیفیت آن
---
# بخش ۸ - در بیان معرفت زکوة و کیفیت آن

<div class="b" id="bn1"><div class="m1"><p>مالها داری تو ای صاحب نصاب</p></div>
<div class="m2"><p>حق درویشان بده گردن متاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر این معنی نقد این دنیا بدان</p></div>
<div class="m2"><p>آیت مما رزقنا هم بخوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چیست دنیا با همه خشک و ترش</p></div>
<div class="m2"><p>گرهمه عقلست برخیز از سرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه دادنت برون آر و بپاش</p></div>
<div class="m2"><p>اندرین معنی کم از خاکی مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل شو و میده نسیم دلفروز</p></div>
<div class="m2"><p>همچو آتش هر کرا یابی مسوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جوانمردی برآمد نام مرد</p></div>
<div class="m2"><p>حاتم طی بین که در هیجا چه کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهل عشرت چون بهم آمیختند</p></div>
<div class="m2"><p>جرعۀ بر خاک مجلس ریختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موراگر پای ملخ برخوان نهاد</p></div>
<div class="m2"><p>آنچه بودش در بر مهمان نهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نکردی خود جوانمردی پدید</p></div>
<div class="m2"><p>در جهان نه پیر بودی نه مرید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنچه می باید مرید از جمله پیش</p></div>
<div class="m2"><p>مایه دارست از زکوة پیر خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون گدا را از توانگر میرسد</p></div>
<div class="m2"><p>امتنان را از پیمبر میرسد</p></div></div>