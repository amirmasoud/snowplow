---
title: >-
    بخش ۳۵ - در بیان تجلی ذات میفرماید
---
# بخش ۳۵ - در بیان تجلی ذات میفرماید

<div class="b" id="bn1"><div class="m1"><p>چون زدود آئینۀ صافی شد زشک</p></div>
<div class="m2"><p>رو نماید صورت انس و ملک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه وقت خویش بودش در نظر</p></div>
<div class="m2"><p>وصف حالش گشت مازاغ البصر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تو با وقتی ز کار افتاده ای</p></div>
<div class="m2"><p>وقت اگر با تو بود آزاده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت اگر با تو نماید حال تست</p></div>
<div class="m2"><p>بازیابی نقد وقت خود درست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست وقت حال را چندان درنگ</p></div>
<div class="m2"><p>زین سبب گیرد دلت صد گونه رنگ</p></div></div>