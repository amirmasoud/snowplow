---
title: >-
    بخش ۱۰ - در بیان معرفت حج
---
# بخش ۱۰ - در بیان معرفت حج

<div class="b" id="bn1"><div class="m1"><p>زین گریبان هر که سر برمی زند</p></div>
<div class="m2"><p>هر زمان صد حج اکبر میزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بیابان هوا احرام گیر</p></div>
<div class="m2"><p>پس طواف کعبه اسلام گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمان سوی تو یابد از صفا</p></div>
<div class="m2"><p>در صفای مروه خوف و رجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش اندر خرمن پندار زن</p></div>
<div class="m2"><p>آنگهی لبیک عاشق وار زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون پدید آمد حریم بارگاه</p></div>
<div class="m2"><p>نفس خود قربان کن اندر پیش شاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو مویت این طریق ای هوشمند</p></div>
<div class="m2"><p>مو بمو از خود جدا باید فکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین به پشت مرکب توفیق کن</p></div>
<div class="m2"><p>پس طواف کعبه تحقیق کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جهت بگذر که اینجا کبریاست</p></div>
<div class="m2"><p>خود بهرجائی که روآری خداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کعبه مردان نه از آب و گلست</p></div>
<div class="m2"><p>طالب دل شو که بیت الله دلست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ز معنی بایدت سرمایه ای</p></div>
<div class="m2"><p>بهتر از دانش ندانم مایه ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آشنا باید در این دریای ژرف</p></div>
<div class="m2"><p>یاد گیر این نکته حرفاً بعد حرف</p></div></div>