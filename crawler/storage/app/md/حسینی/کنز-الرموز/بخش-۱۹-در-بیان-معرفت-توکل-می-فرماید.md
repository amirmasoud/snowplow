---
title: >-
    بخش ۱۹ - در بیان معرفت توکل می‌فرماید
---
# بخش ۱۹ - در بیان معرفت توکل می‌فرماید

<div class="b" id="bn1"><div class="m1"><p>چون تو رو از غیر حق برتافتی</p></div>
<div class="m2"><p>نقد اسرار توکل یافتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این بنارا هرکه میخواهد ثبات</p></div>
<div class="m2"><p>مرده باید بود او را در حیات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پی تدبیر نفسانی مرو</p></div>
<div class="m2"><p>بی خدا هرجا که میدانی مرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز و شب سودای نیک و بد کنی</p></div>
<div class="m2"><p>خودپرستی چون حدیث خود کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزت امروز است اگر داری خبر</p></div>
<div class="m2"><p>از غم فردا مخور خون جگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو خواهی ورنه حق روزی ده است</p></div>
<div class="m2"><p>حق طلب کن یاد آن باری به است</p></div></div>