---
title: >-
    بخش ۱۲ - در بیان معرفت توحید
---
# بخش ۱۲ - در بیان معرفت توحید

<div class="b" id="bn1"><div class="m1"><p>چون مسافر گشتی اندر راه دین</p></div>
<div class="m2"><p>صدق باشد مرکب و رهبر یقین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز کن چشم خرد را پیش و پس</p></div>
<div class="m2"><p>عقل فرزانه تورا استاد بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفی کن اثبات هر موجود را</p></div>
<div class="m2"><p>تا بدانی هستی معبود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون یقین شد کافر نیند خداست</p></div>
<div class="m2"><p>ذات پاکش را مگو چون و چراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حضرت او برتر از حد و مثال</p></div>
<div class="m2"><p>درنگنجد صورت و وهم و خیال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی بدایت بوده ذات او نخست</p></div>
<div class="m2"><p>بینهایت همچنان باشد درست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصف خود کرد و بدان موصوف شد</p></div>
<div class="m2"><p>نام خود کرد و بدان معروف شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او بخود هست و همه هستی از اوست</p></div>
<div class="m2"><p>نیست آمد هرچه آمد جمله اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ذات او را نیست نقصان و زوال</p></div>
<div class="m2"><p>نی سکون و نی تحرک را مجال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کمال لایزالی کاملست</p></div>
<div class="m2"><p>بی جهت هرجا که جوئی حاصلست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دو عالم هیچکس همتاش نیست</p></div>
<div class="m2"><p>همچو عالم پستی و بالاش نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دانش عامی ندارد زین گذر</p></div>
<div class="m2"><p>اهل صورت را تمامست این قدر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رهروان کز ملک معنی آگهند</p></div>
<div class="m2"><p>کشتگان خنجر الا الله اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از دو کون آزاد و از خود بی نشان</p></div>
<div class="m2"><p>در فنای کل شده دامن کشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>محو بینند آنچه غیر حق بود</p></div>
<div class="m2"><p>نیستی شان زین سبب مطلق بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرچه باشد از نهایتها که هست</p></div>
<div class="m2"><p>جمله را در نور حق یابند و بس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از فنای خویشتن یکتا شده</p></div>
<div class="m2"><p>جمله در حق هم بحق بینا شده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون رسد آنجا همه گردد مراد</p></div>
<div class="m2"><p>دور از این معنی حلول و اتحاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هوشیار و مست و گویا و خموش</p></div>
<div class="m2"><p>گاه جمله چشم و گاهی جمله گوش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نور حق در سر او پیدا شده</p></div>
<div class="m2"><p>او ز سر خویشتن یکتا شده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرکه او از بند خود آزاد نیست</p></div>
<div class="m2"><p>دار ملک وحدتش آبادنیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سر توحید آن زمان گردد عیان</p></div>
<div class="m2"><p>کز قفس یابد رهائی مرغ جان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگذرد از گلخن طبع و حواس</p></div>
<div class="m2"><p>نی خیال و وهم بیند نی قیاس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نفس رعنا را ببرد دست و پای</p></div>
<div class="m2"><p>عقل دور اندیش را ماند بجای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر دو عالم با همه شادی و غم</p></div>
<div class="m2"><p>غرقه گرداند بدریای عدم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون بیاسود از گرامی مرکبش</p></div>
<div class="m2"><p>در بر معشوق خود باداش و بس</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا بدانی هر که رفت آنجا رسید</p></div>
<div class="m2"><p>با کسی کاو دیدۀ دارد پدید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای بسی دانا که گفت این سر گذشت</p></div>
<div class="m2"><p>سر فرو آورد و حیران درگذشت</p></div></div>