---
title: >-
    بخش ۳۰ - در بیان تلوین و تمکین میفرماید
---
# بخش ۳۰ - در بیان تلوین و تمکین میفرماید

<div class="b" id="bn1"><div class="m1"><p>چون بیارایند بزم انس را</p></div>
<div class="m2"><p>برکشند از دام صید قدس را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میدهند او را ز جام دوستی</p></div>
<div class="m2"><p>تا برون آید ز دام نیستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این قدح را هر دل بینا کشد</p></div>
<div class="m2"><p>تشنه باشد گرچه صد دریا کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق اینجا بس پریشانی کند</p></div>
<div class="m2"><p>حالتش دعوی سبحانی کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خستۀ آن خنجر خونخوار بود</p></div>
<div class="m2"><p>آنکه در کوی بلا بردار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این محل آفتست و جای بیم</p></div>
<div class="m2"><p>صد هزار اینجا به یک ساعت دو نیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دانشی در عین دانائیست این</p></div>
<div class="m2"><p>منطقی از طیر سبحانیست این</p></div></div>