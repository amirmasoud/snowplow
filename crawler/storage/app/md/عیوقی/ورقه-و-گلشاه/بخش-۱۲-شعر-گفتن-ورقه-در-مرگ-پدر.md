---
title: >-
    بخش ۱۲ - شعر گفتن ورقه در مرگ پدر
---
# بخش ۱۲ - شعر گفتن ورقه در مرگ پدر

<div class="b" id="bn1"><div class="m1"><p>دریغ ای پدر دیدهٔ شیر مرد</p></div>
<div class="m2"><p>کی رفتی ز دنیا پر از داغ و درد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین است کار سرای سپنج</p></div>
<div class="m2"><p>چنین بود خواهد جهان گرد گرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شدی کشته ناگه به دست سگی</p></div>
<div class="m2"><p>که او را نه زن خواند شاید نه مرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از و کین تو باز خواهم چنانک</p></div>
<div class="m2"><p>کی گرید برو گنبد لاژورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان کو بر آورد گرد از سرت</p></div>
<div class="m2"><p>بر آرم ز فرقش به شمشیر گرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفت این واز درد بگریست زار</p></div>
<div class="m2"><p>ز خون کرد روی زمین لاله زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بسیار بگرست و زاری نمود</p></div>
<div class="m2"><p>بگفتا کنون بانگ و زاری چه سود؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برفت او و بر بارگی برنشست</p></div>
<div class="m2"><p>سوی کینه رانداسب چون پیل مست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به حمله درآمد به سوی ربیع</p></div>
<div class="m2"><p>بگفتش ربیع: ای سوار بدیع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه مردی و پیشم چرا آمدی؟</p></div>
<div class="m2"><p>بر شیر نر به چرا آمدی!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کی بر تو مرا رحمت آید همی</p></div>
<div class="m2"><p>نخواهم کی آیدت از من غمی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سوی مرگ رفتن بسیجی همی</p></div>
<div class="m2"><p>به دستم بذره نسنجی همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سراسیمه رایی و دل خسته ای</p></div>
<div class="m2"><p>مگر با غم عشق پیوسته ای؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه مردی؟ هلا زود بر گوی نام</p></div>
<div class="m2"><p>همانا تویی ورقه ابن الهمام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کی از هجر گلشاه و مرگ پدر</p></div>
<div class="m2"><p>شدستی دلاواره آسیمه سر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر ورقه ای، حمله آور، هلا!</p></div>
<div class="m2"><p>کی برهانمت هم کنون زین بلا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کی از بهر بابک بنالی همی</p></div>
<div class="m2"><p>برین روی زاری سگالی همی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رسانم ترا هم کنون زی پدر</p></div>
<div class="m2"><p>بدین تیغ پولاد پرخاش خر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ور از بهر گلشاه دل خسته ای</p></div>
<div class="m2"><p>دل اندر غم عشق او بسته ای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم اکنون سرت را برم سوی اوی</p></div>
<div class="m2"><p>نبینی تو تا زنده ای روی اوی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه ای تو سزاوار دیدار اوی</p></div>
<div class="m2"><p>منم یار او و سزاوار اوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو بشنید ورقه ازو این سخن</p></div>
<div class="m2"><p>غم بابک و عشق آن سروبن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دگرباره اندر دلش تازه شد</p></div>
<div class="m2"><p>بنالید و دردش بی اندازه شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگفت: ای فرومایهٔ مستحل</p></div>
<div class="m2"><p>نیاورد گردون چو تو سنگ دل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نبخشودی ای شوم نامهربان</p></div>
<div class="m2"><p>بر آن پیر فرتوت دیده جهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که بر جان او بر، کمین ساختی</p></div>
<div class="m2"><p>جهان را ز نامش بپرداختی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه گفتی وراهیچ کین خواه نیست</p></div>
<div class="m2"><p>و یا سوی تو مرگ را راه نیست؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ترا گر چنین بود در دل گمان</p></div>
<div class="m2"><p>گمانت کژ آمد بسان کمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که من از پی کین او خاستم</p></div>
<div class="m2"><p>به کینش زبان را بیاراستم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کنون از عرب نام تو کم کنم</p></div>
<div class="m2"><p>نشاط و سرور تو ماتم کنم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز بی آن کی بر تو شبیخون کنم،</p></div>
<div class="m2"><p>ز خون تو این دشت گلگون کنم،</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به شمشیر جان از تنت برکنم</p></div>
<div class="m2"><p>بگرز گران گردنت بشکنم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جهان را، چو من تیغ پیدا کنم،</p></div>
<div class="m2"><p>ز خون سپاه تو دریا کنم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بنالند چون نیزه گیرم به چنگ</p></div>
<div class="m2"><p>به بیشه هزبرو به دریا نهنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ألا با من ای شیر جنگی بگرد</p></div>
<div class="m2"><p>کی خواهم ز فرقت برآورد گرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نباید مرا با تو زین بیش لاف</p></div>
<div class="m2"><p>کی جای نبردست و جای مصاف</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ربیع ابن عدنان ز گفتار اوی</p></div>
<div class="m2"><p>برآشفت و آمد به پیکار اوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بگفت: ای فرومایهٔ بی وفا</p></div>
<div class="m2"><p>چه گویی تو در روی مردان جفا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نگویی مرا تا تو اندر عرب</p></div>
<div class="m2"><p>چه کردستی از کارهای عجب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کی پیشم دلیری نمایی همی</p></div>
<div class="m2"><p>بر شیر سگرا ستایی همی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>محالست با من ترا گفت و گوی</p></div>
<div class="m2"><p>بیا تا به میدان درآریم گوی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بگفت این و مانند ابر بهار</p></div>
<div class="m2"><p>برو حمله کرد آن دلاور سوار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بر آویختند آن دو میر عرب</p></div>
<div class="m2"><p>دو فرخنده نام و دو عالی نسب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دو میر شجاع و دو پیل نبرد</p></div>
<div class="m2"><p>دو شیر صف آشوب و دو مرد مرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بهٔک جای هر دو برآویختند</p></div>
<div class="m2"><p>به نیزه همی صاعقه بیختند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سپردند هر دو به مرکب عنان</p></div>
<div class="m2"><p>به پیش آوریدند نوک سنان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بگشتند چندان بخشم و ستیز</p></div>
<div class="m2"><p>که شد نیزهٔ هر دوان ریز ریز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فگندند نیزه، کشیدند تیغ</p></div>
<div class="m2"><p>چو دو برق رخشنده از تیره میغ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز بس ضرب شمشیر زهر آب دار</p></div>
<div class="m2"><p>بشد تیغ در دستشان پاره پار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بجز قبضهٔ درقه در دستشان</p></div>
<div class="m2"><p>نماندونه کس داد ز آن سان نشان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ربیع ابن عدنان بکردار میغ</p></div>
<div class="m2"><p>فراز سر ورقه بگذارد تیغ</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز شمشیر او ورقه ابن الهمام</p></div>
<div class="m2"><p>بترسید و پیش آوریدش حسام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به شمشیر شمشیر او را گرفت</p></div>
<div class="m2"><p>به دو نیمه شد هر دو تیغ ای شگفت!</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بماندند بی نیزه و تیغ تیز</p></div>
<div class="m2"><p>کسی کرد بی تیغ و نیزه ستیز؟</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو از تیغ و نیزه ندیدند کام</p></div>
<div class="m2"><p>ربیع و همان ورقه ابن الهمام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نشدشان به جنگ اندرون رای سست</p></div>
<div class="m2"><p>بگرز گران دست بردند چست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به گرز گران آوریدند رای</p></div>
<div class="m2"><p>فشردند هر دو به پرخاش پای</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بگرز آزمودند چندان نبرد</p></div>
<div class="m2"><p>کی گل رنگ رخسارشان گشت زرد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نکردند گرز گران را یله</p></div>
<div class="m2"><p>جز آنگه که شددست پر آبله</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دگر باره شمشیرها خواستند</p></div>
<div class="m2"><p>همی جنگ نو از سر آراستند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دو تیغ و دو رمح آوریدندشان</p></div>
<div class="m2"><p>چو بی نیزه و تیغ دیدندشان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به میدان در، آن هر دو خسرونژاد</p></div>
<div class="m2"><p>به کینه بگشتند چون تند باد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ربیع ابن عدنان برآورد خشم</p></div>
<div class="m2"><p>یکی حمله کرد آن سگ شوخ چشم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سر نیزه بگذارد بر ران اوی</p></div>
<div class="m2"><p>که از درد آزرده شد جان اوی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ابر پهلوی اسپ رانش بدوخت</p></div>
<div class="m2"><p>رخ ورقه از دلد دل برفروخت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>پیاده شد از اسپ، اسپس بمرد</p></div>
<div class="m2"><p>پیاده برآن خستگی حمله برد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یکی نیزه ای زد به بازوش بر</p></div>
<div class="m2"><p>کی بردوخت بازو به پهلوش بر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ولیکن به جانش نیامد گزند</p></div>
<div class="m2"><p>ز بازوی خود نوک نیزه بکند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تن هر دو در بند غم بسته شد</p></div>
<div class="m2"><p>همین خسته گشت و همان خسته شد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>غلامش یکی بارهٔ تیز گام</p></div>
<div class="m2"><p>بیاورد زی ورقه ابن الهمام</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هم اندر زمان ورقهٔ نیک رای</p></div>
<div class="m2"><p>به اسپ تک آور درآورد پای</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بگشتند آن هر دو فرخ جوان</p></div>
<div class="m2"><p>ز هر دو چو دو سل خون شد روان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ز بس خون کی از هر دو پالوده شد</p></div>
<div class="m2"><p>ز خونشان دل خاک آلوده شد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دل هر دو از درد مستی گرفت</p></div>
<div class="m2"><p>تن هر دو از رنج سستی گرفت</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز سستی بماندند از کارزار</p></div>
<div class="m2"><p>بر آن هر دو آزاده شد کار زار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>یکی خسته بازو یکی خسته ران</p></div>
<div class="m2"><p>همان زین بترسید و هم این از آن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دو عاشق ز بهر دلارام خویش</p></div>
<div class="m2"><p>همی تیره کردند ایام خویش</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>همی بر زدند از جگر سرد باد</p></div>
<div class="m2"><p>همی کرد هریک ز گلشاهٔاد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چنان بد دل ورقه ابن الهمام</p></div>
<div class="m2"><p>ز هجران گلشاه فرخنده نام</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>کی هزمان همی مغزش آمد به جوش</p></div>
<div class="m2"><p>ولیکن بداز صبر و مردی خموش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ربیع ارچه بد عاشق زار اوی</p></div>
<div class="m2"><p>دلش ایمن آخر بد از کار اوی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>کی در حی او بود و در خان اوی</p></div>
<div class="m2"><p>دلارام گل رخ بدش جان اوی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>کی گلشاه با او وفا کرده بود</p></div>
<div class="m2"><p>تن از وی به حیله رها کرده بود</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به مکر و به چاره دلش بسته بود</p></div>
<div class="m2"><p>به شیرین زبانی ازو جسته بود</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>جهان بر دلش تنگ چون حلقه بود</p></div>
<div class="m2"><p>ولی جانش پیوستهٔ ورقه بود</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>کی باوی بهٔکجای خو کرده بود</p></div>
<div class="m2"><p>دل هر دو در عشق پرورده بود</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>هم از کودکی مهرشان بسته بود</p></div>
<div class="m2"><p>وفا در دل هر دوان رسته بود</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ربیع ابن عدنان ز بس ابلهی</p></div>
<div class="m2"><p>نبود آگه از مکر سروسهی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>کی بروی به حیلت نهادست بند</p></div>
<div class="m2"><p>به تیمار هجران و بیم گزند</p></div></div>