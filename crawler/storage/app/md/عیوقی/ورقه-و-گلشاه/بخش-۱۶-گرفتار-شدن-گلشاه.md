---
title: >-
    بخش ۱۶ - گرفتار شدن گلشاه
---
# بخش ۱۶ - گرفتار شدن گلشاه

<div class="b" id="bn1"><div class="m1"><p>چو در بند او شد درخشنده ماه</p></div>
<div class="m2"><p>نهاد از طرب روی سوی سپاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپاه بنی ضبه گشتند شاد</p></div>
<div class="m2"><p>همه حمله کردند چون تند باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفتند مر هر دو را در میان</p></div>
<div class="m2"><p>شدند از طرب شادمان و دنان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو ورقه چنان دید غم خواره شد</p></div>
<div class="m2"><p>چو سر گشته ای زار و بیچاره شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفت: ای سران و مهان عرب</p></div>
<div class="m2"><p>شجاعان و نام آوران عرب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجویید روی و بیابید رای</p></div>
<div class="m2"><p>بکوشید با من ز بهر خدای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا اندرین کار یاری کنید</p></div>
<div class="m2"><p>به جنگ اندرون پای داری کنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر یار گم بوده باز آورم</p></div>
<div class="m2"><p>دل دشمنان زیر گاز آورم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سبک باب گلشاه فرخ نسب</p></div>
<div class="m2"><p>خروشید و بدرید بر تن سلب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفت: از پی حمیت و نام و ننگ</p></div>
<div class="m2"><p>بکوشید أیا نام داران به جنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو گفت این أبا ورقه و با سپاه</p></div>
<div class="m2"><p>ز کینه بر اعدا گرفتند راه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به حمله درون زود بشتافتند</p></div>
<div class="m2"><p>مر آن قوم را جمله دریافتند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بساجان کی اندر بلا سوختند</p></div>
<div class="m2"><p>بسا دیده کز تیر بردوختند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی تا جهان جامهٔ دود رنگ</p></div>
<div class="m2"><p>نپوشید، کوته نکردند چنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو گیتی بپوشید شعر کبود</p></div>
<div class="m2"><p>دو لشکر ز هم باز گشتند زود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ربودند گلشاه دل خواه را</p></div>
<div class="m2"><p>ببستند و بردند آن ماه را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو دو لشکر از کینه گشتند باز</p></div>
<div class="m2"><p>دل ورقه شد جفت گرم و گداز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفتا که: در جنگ کشته شدن</p></div>
<div class="m2"><p>بهست از چنین زار زنده بدن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفتار با خلق بگشاد لب</p></div>
<div class="m2"><p>نگر تا چه کرد آن سوار عرب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همی بود تا شب بسی درگذشت</p></div>
<div class="m2"><p>شباهنگ بر چرخ گردان بگشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برون آمد از خیمه چون تند باد</p></div>
<div class="m2"><p>سوی لشکر دشمنان رخ نهاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ابا درقه و دشنه و تیغ تیز</p></div>
<div class="m2"><p>همی رفت تنها به خشم و ستیز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپه بد سر اندر کشیده به خواب</p></div>
<div class="m2"><p>که بس مانده بودند از رنج و تاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو آتش دل ورقه تفسیده گرم</p></div>
<div class="m2"><p>همی شد میان سپه نرم نرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی کرد با خیمها در نگاه</p></div>
<div class="m2"><p>به تیره شب اندر همی کرد راه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز دلبرش جایی نشانی ندید</p></div>
<div class="m2"><p>نه از هیچ جا زاری او شنید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو از دیدن دوست نومید گشت</p></div>
<div class="m2"><p>دل و جانش لرزنده چون بید گشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی خیمه ای دید عالی ز دور</p></div>
<div class="m2"><p>همی تافت از وی چو از ماه نور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سبک ورقه زی خیمه افگند رای</p></div>
<div class="m2"><p>باومید دیدار آن دل ربای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو از دور نزدیک خیمه رسید</p></div>
<div class="m2"><p>به درگاه خیمه درون بنگرید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به بیغولهٔ خیمه گلشاه را</p></div>
<div class="m2"><p>بدیدش مر آن دل گسل ماه را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دو گیسوش بر چوب بسته چو سنگ</p></div>
<div class="m2"><p>ببسته چنان چوب بر رشته چنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پس پشت او دستها بسته سخت</p></div>
<div class="m2"><p>غلام از بر تخت و او زیر تخت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همه مشک پر چنبرش زیر تاب</p></div>
<div class="m2"><p>همه نرگس دلبرش زیر آب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گل لعل فامش نهان زیر ابر</p></div>
<div class="m2"><p>دل مهربانش نهان زیر صبر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>غلام از بر تخت، دل پرستیز</p></div>
<div class="m2"><p>نهاده ز پیش اندرون تیغ تیز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تهی کرده بد خیمه از کهتران</p></div>
<div class="m2"><p>ز پیوستگان و هم از مهتران</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نبد جز غلام اندر آن خیمه کس</p></div>
<div class="m2"><p>هم او بود تنها و گلشاه و بس</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یکی نوبتی بر در خیمه بر</p></div>
<div class="m2"><p>ببسته به کردار مرغی بپر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نهاده بر آن تخت پیش غلام</p></div>
<div class="m2"><p>یکی قطره میزی می لعل فام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به دست اندرش ساغری پر شراب</p></div>
<div class="m2"><p>به رنگ گل سرخ و بوی گلاب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>غلام از بر تخت بد نیم مست</p></div>
<div class="m2"><p>یکی تیغ پیش و پیاله به دست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فگنده بدیدار گلشاه چشم</p></div>
<div class="m2"><p>رخی پر ز رشک و دلی پر ز خشم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به گلشاه گفتی همی هر زمان</p></div>
<div class="m2"><p>ز کین جگر: کای فلان و فلان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بکشتی تو مر بابکم را به قهر</p></div>
<div class="m2"><p>به من بر همه نوش کردی چو زهر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>برادرم را نیز کشتی به جنگ</p></div>
<div class="m2"><p>ایا سنگ دل شوخ بی نام و ننگ</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گناه ترا جمله کردم عفو</p></div>
<div class="m2"><p>نداری همی مر مرا هم کفو؟</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>من از ورقهٔ تو بچه کمترم</p></div>
<div class="m2"><p>که ما را نخواهی، نیایی برم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ببندم کنون لاجرم بسته ای</p></div>
<div class="m2"><p>دل خویش با مرگ پیوسته ای</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هم اکنون من این بادهٔ لعل فام</p></div>
<div class="m2"><p>خورم، چون بخوردم بگیرم حسام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همه کین دیرینه پیدا کنم</p></div>
<div class="m2"><p>بگیرمت با قهر و رسوا کنم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به پیش تو اندر به قهر و ستم</p></div>
<div class="m2"><p>چنان چون سزد با تو باشم بهم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سحرگاه باشد کی آیم به جنگ</p></div>
<div class="m2"><p>شوم ورقه را زنده آرم به چنگ</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به پیش تو آرمش بسته دو دست</p></div>
<div class="m2"><p>کنمش از غم و رنج و تیمار مست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پس آنگه ببرم سرش از قفا</p></div>
<div class="m2"><p>کزو وز تو دارم فراوان جفا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همی گفت چونین و بر کف شراب</p></div>
<div class="m2"><p>همی راند گلشاه از دیده آب</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>روانش پر از درد و دل پر ز پیچ</p></div>
<div class="m2"><p>سر از پیش خود بر نیاورد هیچ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نه با وی به گفتار بگشاد لب</p></div>
<div class="m2"><p>نه از کبر خاموش گشت ای عجب!</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>غلام فرومایه کان می بخورد</p></div>
<div class="m2"><p>نشست او زمانی و تیزی نکرد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>پس آنگاه از تخت برخاست زود</p></div>
<div class="m2"><p>ز کار فلک هیچ آگه نبود</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به نزدیک او رفت با خرمی</p></div>
<div class="m2"><p>بسیجید از بهر نا مردمی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بدان روی تا مهر بستاندش</p></div>
<div class="m2"><p>به ناپاکی آلوده گرداندش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به گلشاه چون دست کردش دراز</p></div>
<div class="m2"><p>دل ورقه مر کینه را کرد ساز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نماندش صبوری، نماندش قرار</p></div>
<div class="m2"><p>به خیمه درون جست عیار وار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بهٔک جستن آمد به نزد غلام</p></div>
<div class="m2"><p>برآورد و بگذارد هندی حسام</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بهٔک زخم بگسست از تن سرش</p></div>
<div class="m2"><p>کز آن زخم آگه نشد لشکرش</p></div></div>