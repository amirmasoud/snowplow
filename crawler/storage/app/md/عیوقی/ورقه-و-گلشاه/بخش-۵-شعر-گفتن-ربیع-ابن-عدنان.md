---
title: >-
    بخش ۵ - شعر گفتن ربیع ابن عدنان
---
# بخش ۵ - شعر گفتن ربیع ابن عدنان

<div class="b" id="bn1"><div class="m1"><p>ایا ماه گل چهر دل خواه من</p></div>
<div class="m2"><p>دراز از تو شد عمر کوتاه من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر وصل من در خور آید ترا</p></div>
<div class="m2"><p>نهد بخت بر مشتری گاه من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم شاه گردن کشان جهان</p></div>
<div class="m2"><p>تو شاه ظریفانی و ماه من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرم در چه غم نخواهی فگند</p></div>
<div class="m2"><p>چرا کندی اندر زنخ چاه من</p></div></div>