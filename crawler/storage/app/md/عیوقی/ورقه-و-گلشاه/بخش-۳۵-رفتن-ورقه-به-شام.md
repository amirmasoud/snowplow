---
title: >-
    بخش ۳۵ - رفتن ورقه به شام
---
# بخش ۳۵ - رفتن ورقه به شام

<div class="b" id="bn1"><div class="m1"><p>بگفت این و آمد ز پیشش برون</p></div>
<div class="m2"><p>ز دیده روان کرده دو جوی خون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفتار با خلق نگشاد لب</p></div>
<div class="m2"><p>بپوشید دستی سلیح و سلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خشم عم و بهر ننگ و نبرد</p></div>
<div class="m2"><p>نشست از بر بارهٔ ره نورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز حی بنی شیبه بنهاد روی</p></div>
<div class="m2"><p>سوی شام از بهر آن مهرجوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهی راند نرم و گهی راند گرم</p></div>
<div class="m2"><p>به رخ برچکان از مژه آب گرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلش چون دو زلف بتان تافته</p></div>
<div class="m2"><p>ز دل دار خود کام نایافته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین سان همی رفت بیگاه و گاه</p></div>
<div class="m2"><p>بسه روز پوینده ده روزه راه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو نزدیکی شام آمد فراز</p></div>
<div class="m2"><p>برو گشت کوتاه راه دراز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آن وقت گیتی دگر گشته بود</p></div>
<div class="m2"><p>همه ره زدزدان پر از کشته بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان از بد خلق ایمن نبود</p></div>
<div class="m2"><p>ابی دزد و ره دار ممکن نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو ورقه بر شهر نزدیک شد</p></div>
<div class="m2"><p>برو روز رخشنده تاریک شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز دزدان چهل مرد گم بودگان</p></div>
<div class="m2"><p>ازین راه داران بیهودگان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه از کمین گاه برخاستند</p></div>
<div class="m2"><p>به پرخاش تن را بیاراستند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه پیش ورقه برون آمدند</p></div>
<div class="m2"><p>طلب کار و جویای خون آمدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی پیشش آمد از آن چل سوار</p></div>
<div class="m2"><p>کشیدهٔکی خنجر آب دار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنین گفت مر ورقه را کای جوان</p></div>
<div class="m2"><p>مرا باد مال و ترا بادجان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرود آی ز اسپ وره خویش گیر</p></div>
<div class="m2"><p>مده جان، بده مال، پندم پذیر!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ستور و سلیحت همین جا بنه</p></div>
<div class="m2"><p>ز چنگال مرگ ار حکیمی بجه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو در پیش ورقه چنین کرد یاد</p></div>
<div class="m2"><p>به پاسخش ورقه زبان برگشاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگفت ای فرومایهٔ تیره کیش</p></div>
<div class="m2"><p>ترا بهتر از مال من جان خویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شما هر چهل ار چهل لشکرید</p></div>
<div class="m2"><p>به نزد من از کودکی کمترید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همی خواست از وی تمامی سلیح</p></div>
<div class="m2"><p>ستور و سلب با حسام و رمیح</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدیشان چنین گفت آن سرفراز</p></div>
<div class="m2"><p>که من شیر چنگم شما چون گراز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو در کینهٔازم به شمشیر چنگ</p></div>
<div class="m2"><p>چهٔک تن چه صد تن به پیشم به جنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز دزدان نیندیشم و دزد را</p></div>
<div class="m2"><p>بشایدش کشت از پی مزد را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیا گر سلب خواهی و اسب و ساز</p></div>
<div class="m2"><p>سوی کینه و جنگ ما دست یاز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که من ساختم دل به جنگ شما</p></div>
<div class="m2"><p>کنم کند در جنگ چنگی شما</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگفت این و از بهر ننگ و نبرد</p></div>
<div class="m2"><p>بر آن هر چهل مرد بر حمله کرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بهر رخم مردی بدونیم کرد</p></div>
<div class="m2"><p>چو زد تیغ با مرگ تسلیم کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چهل مرد صعلوک شمشیر زن</p></div>
<div class="m2"><p>همه کشور آرای و لشکر شکن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گشادند بر یک تن از کینه دست</p></div>
<div class="m2"><p>دمان در میان ورقه چون پیل مست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به نوک سر رمح و شمشیر تیز</p></div>
<div class="m2"><p>برآورد از آن هر چهل رستخیز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز چل مرد صعلوک سی را بکشت</p></div>
<div class="m2"><p>دگردر هزیمت نمودند پشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو با دشمنش جنگ پیوسته شد</p></div>
<div class="m2"><p>بده جای افزون تنش خسته شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز خونش دل خاک بیرنگ شد</p></div>
<div class="m2"><p>ز سستی جهان بر دلش تنگ شد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همی رفت ازو خون چکان بر زمین</p></div>
<div class="m2"><p>شده پر ز خونش نمد زین و زین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدان حال شد تا در شهر شام</p></div>
<div class="m2"><p>دلش ریش از عشق و تن از حسام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به دروازهٔ شهر دربنگرید</p></div>
<div class="m2"><p>درختی و دو چشمهٔ آب دید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سوی چشمه راند اسپ را همچو باد</p></div>
<div class="m2"><p>ز سستی کی بوداز فرس درفتاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدین حال در سایهٔ بید برگ</p></div>
<div class="m2"><p>بیفتاد و بنهاد دل را به مرگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جدا گشت ازو هوش و دور از خرد</p></div>
<div class="m2"><p>چو شخصی کزو جان ز تن برپرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ستوره ره انجام او رهبرش</p></div>
<div class="m2"><p>بپای ایستاده فراز سرش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>قضای خداوند را شاه شام</p></div>
<div class="m2"><p>که بد شوی گلشاه فرخنده نام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همی آمد از دشت نخچیرگاه</p></div>
<div class="m2"><p>ابا باز و با یوز و خیل و سپاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو از ره به نزدیک چشمه رسید</p></div>
<div class="m2"><p>یکی مرد مجروح سرگشته دید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جوانی نکو قامت و خوب روی</p></div>
<div class="m2"><p>همه روی رنگ و همه موی بوی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز سنبل دمید خطی گرد ماه</p></div>
<div class="m2"><p>فگنده بر آن خاک زار و تباه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شده غرقه در خون ز سر تا قدم</p></div>
<div class="m2"><p>بپیچید مر شاه را دل ز غم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>برو بر دل شاه کشور بسوخت</p></div>
<div class="m2"><p>همی جانش از مهر او برفروخت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جوانمردیی بود در شه ز نسل</p></div>
<div class="m2"><p>ابا نسل نیکو بود فضل و اصل</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بفرمود تا بر گرفتند زود</p></div>
<div class="m2"><p>مرو را از آن جایگه همچو دود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو برداشتندش برفتند و برد</p></div>
<div class="m2"><p>به قصر شه او را به خادم سپرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کنیزک بد او را یکی کاردان</p></div>
<div class="m2"><p>خردمند و هشیار و بسیاردان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مرو را به دست پرستار داد</p></div>
<div class="m2"><p>بدو گفتش و مال بسیار داد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بگفتا برو بر همی بر بکار</p></div>
<div class="m2"><p>دلش دور کن از غم روزگار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو مسکین تن ورقه آمد بهوش</p></div>
<div class="m2"><p>دل و دیده و مغزش آمد به جوش</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بگفت ای جوامرد فرخنده روی</p></div>
<div class="m2"><p>چه نامی تو و از کجایی بگوی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر ورقه شد در زمان شاه شام</p></div>
<div class="m2"><p>بخوشی بپرسید و کردش سلام</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نهان کرد نام خود آن سرفراز</p></div>
<div class="m2"><p>که بودش بدیدار آن بت نیاز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بدان تا کس او را نداند که کیست</p></div>
<div class="m2"><p>نداند که احوال آن شیر چیست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بگفتش که من نصر بن احمدم</p></div>
<div class="m2"><p>بحی خزاعه درون بخردم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به بازارگانی کنم قصد راه</p></div>
<div class="m2"><p>به هر شهر و هر حی و هر جایگاه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کنون چون رسیدم بدین حد و بوم</p></div>
<div class="m2"><p>به من باز خوردند دزدان شوم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به شمشیر کردند بر من کمین</p></div>
<div class="m2"><p>بخستندم ای پادشاه زمین</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به دم یک تن و راه داران بسی</p></div>
<div class="m2"><p>نبد جز خداوند یارم کسی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>زمانی به کینه برآویختم</p></div>
<div class="m2"><p>چو بسیار گشتند بگریختم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ببردند گم بودگان مال من</p></div>
<div class="m2"><p>چنین بود ایا پادشه حال من</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به نزدیک گلشاه شد شاه شام</p></div>
<div class="m2"><p>بگفت ای دلارام فرخنده نام</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بره بر یکی خسته دل یافتم</p></div>
<div class="m2"><p>مفاجا زِرَه سوی او تافتم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>جوانی نکو روی و فرخنده رای</p></div>
<div class="m2"><p>سرشته تنش زآفرین خدای</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بیاوردم آن زار دل خسته را</p></div>
<div class="m2"><p>مر آن گشته مجروح دل خسته را</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز چاهش مگر سوی گاه آوریم</p></div>
<div class="m2"><p>به درمان تنش سوی راه آوریم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سزد گر برو مهربانی کنی</p></div>
<div class="m2"><p>ورا چند گه میزبانی کنی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بدو گفت گلشاه چونین کنم</p></div>
<div class="m2"><p>من این کار را خود به آیین کنم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>کجا بر غریبان رنج آزمای</p></div>
<div class="m2"><p>ببخشود باید ز بهر خدای</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>کی گلشاه از ایزد پدیرفته بود</p></div>
<div class="m2"><p>بدانگه کجا ورقه زو رفته بود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کی هر گه کی آید غریبیش پیش</p></div>
<div class="m2"><p>مرو را بداردش چون جان خویش</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>مگر ورقه را دیده باشد براه</p></div>
<div class="m2"><p>ویا کرده باشد برویش نگاه</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>پرستنده ای بود گلشاه را</p></div>
<div class="m2"><p>که ماننده بودی مر آن ماه را</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بدو گفت گلشاه رو زی جوان</p></div>
<div class="m2"><p>ز دل باش بر جان او مهربان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>هر آنچ از تو خواهد ز بخت و سرشت</p></div>
<div class="m2"><p>ز تلخ و ز شیرین و از خوب و زشت</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نگر سر نتابی ز فرمان اوی</p></div>
<div class="m2"><p>به خدمت گری تازه کن جان اوی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>شه شام رفتش بر ورقه شاد</p></div>
<div class="m2"><p>بگفت ای جوانمرد فرخ نژاد</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>همه انده از دل ستردم ترا</p></div>
<div class="m2"><p>بدین هر دو خادم سپردم ترا</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>دو فرخ پرستار نام آورند</p></div>
<div class="m2"><p>به خدمت ترا روز و شب درخورند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>یکی چند گه باش مهمان من</p></div>
<div class="m2"><p>فدای تو باد این تن و جان من</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>که بر مستمندی و مجروح و سست</p></div>
<div class="m2"><p>از ایدر مرو تا نگردی درست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>کنیزک به پیشش به خدمت میان</p></div>
<div class="m2"><p>ببست آن پری چهرهٔ مهربان</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به هر ساعتی چند ره سوی اوی</p></div>
<div class="m2"><p>شدی و بدیدی نکو روی اوی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بگفتی که ای خستهٔ سال و ماه</p></div>
<div class="m2"><p>ز من حاجتی و آرزویی بخواه</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بدو ورقهٔ عاشق مبتلا</p></div>
<div class="m2"><p>دعا کردی و گفتی اندر دعا</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>رساناد کدبانوت را خدای</p></div>
<div class="m2"><p>بهرچ آن ورا آرزویست ورای</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>کنیزک چوزی بانوی بانوان</p></div>
<div class="m2"><p>شدی یاد کردی حدیث جوان</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>سبک باز دادیش گلشه جواب</p></div>
<div class="m2"><p>که ایزد کناد این دعا مستجاب</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>برآمد برین حال بر روز چند</p></div>
<div class="m2"><p>ابر ورقه بر سخت تر گشت بند</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بفرسود در عشق، صبرش نماند</p></div>
<div class="m2"><p>پرستار گلشاه را پیش خواند</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بگفتا بپرسم حدیثی ترا</p></div>
<div class="m2"><p>چو گفتم جوابی بده مر مرا</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>کنیزک بگفت آنچه گویی بگوی</p></div>
<div class="m2"><p>هر آنچ آرزویست از من بجوی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بگفتا که در شهر در حد شام</p></div>
<div class="m2"><p>یکی خوب رویست گلشاه نام</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>تو جایی خبر یافتتی از وی؟</p></div>
<div class="m2"><p>و یا هیچ دیدستی او را بروی؟</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>کنیزک بگفتا چه گویی همی!</p></div>
<div class="m2"><p>بدین آرزو در چه جویی همی!</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>که این قصر گلشاه را مسکنست</p></div>
<div class="m2"><p>زن شاه شامست و تاج منست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>دل ورقه در بر تپیدن گرفت</p></div>
<div class="m2"><p>سرشک از دو چشمش دویدن گرفت</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بهریک که از شاه شام او ستد</p></div>
<div class="m2"><p>یکی را بدو دادم امروز سد</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ازین روی زاری همی کرد و گفت</p></div>
<div class="m2"><p>که تا چون بود حال من در نهفت</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بگفت ای کنیزک ز بهر خدای</p></div>
<div class="m2"><p>برین خسته دل بر مشو تیره رای</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>مرا نزدت امروز یک حاجتست</p></div>
<div class="m2"><p>که جان مرا اندرین راحتست</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>کنیزک بگفتا چه حاجت؟ بگوی!</p></div>
<div class="m2"><p>چنین گفت ورقه که ای خوب روی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چه باشد که این نغز انگشتری</p></div>
<div class="m2"><p>بگیری و نزدیک گلشه بری</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>کنیزک بگفتا که ای تیره رای</p></div>
<div class="m2"><p>نداری همی هیچ شرم از خدای</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>که می بدسگالی بدین خاندان</p></div>
<div class="m2"><p>ز تو زشت تر من ندیدم جوان</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>مرا یارگی کی بود کاین سخن</p></div>
<div class="m2"><p>کنم عرضه در پیش آن سروبن</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>که او خود شب و روز از رنج و پیچ</p></div>
<div class="m2"><p>نیاساید از درد وز ناله هیچ</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>ز ورقه شب و روز یاد آورد</p></div>
<div class="m2"><p>گه و بیگه از بهر او غم خورد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>نیارد ازو یاد کردنش شوی</p></div>
<div class="m2"><p>زورقه است اورا همه گفت و گوی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>ازین نام گوید همه روز و شب</p></div>
<div class="m2"><p>نگوید جزین نام خود ای عجب</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>تو دانی که ورقه که باشد؟ بگوی</p></div>
<div class="m2"><p>به میدان درافکن هلا زود گوی</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بگفت این و از ورقه برتافت روی</p></div>
<div class="m2"><p>بگفت این کی گفتی دگر ره مگوی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>ز گفتار آن مهربان پرستار</p></div>
<div class="m2"><p>ببد ورقه غمناک و بگریست زار</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>ز شادی هم آنگه برخ برشکفت</p></div>
<div class="m2"><p>ز دلبر به دل بر حدیثان بگفت</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>به سجده درافتاد و گفت ای خدای</p></div>
<div class="m2"><p>ازین گفته روشن تو کردیم رای</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>درین کار صبری ده اکنون مرا</p></div>
<div class="m2"><p>که تا روز و شب شکر گویم ترا</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>ز عم من اکنون تو دادم بخواه</p></div>
<div class="m2"><p>که وقتست تا عمر باشد تباه</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>که بشکست عهد من آن سنگدل</p></div>
<div class="m2"><p>که گشتم از آن سنگدل تنگدل</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>نیامد بکار آن زر و سیم و مال</p></div>
<div class="m2"><p>که من آوریدم ز نزدیک خال</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ز تیمار دل دار سرگشته بود</p></div>
<div class="m2"><p>زمین ز آب چشم وی آغشته بود</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>دل آن پرستار بر وی بسوخت</p></div>
<div class="m2"><p>ز نالیدن او رخش برفروخت</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>نگفت این سخن هیچ در پیش اوی</p></div>
<div class="m2"><p>برون رفت و از وی بتابید روی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>چو سه روز ازین حال بگذشت بیش</p></div>
<div class="m2"><p>دگر ره پرستار را خواند پیش</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>ز بهر کنیزک برآمد به پای</p></div>
<div class="m2"><p>بگفت ای کنیزک ز بهر خدای</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>سخن بشنو و حاجتم کن روا</p></div>
<div class="m2"><p>رها کن رهی را ز محنت رها</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>بگفتش همه حجت تو رواست</p></div>
<div class="m2"><p>جز آن یک سخن کآن طریق خطاست</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>چنین گفت ورقهٔکی جام شیر</p></div>
<div class="m2"><p>به نزد من آر ای بت دست گیر</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>کی بر تو سخن گفتن و داوری</p></div>
<div class="m2"><p>نهفته کنم در وی انگشتری</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>چو شیر آرزو آیدش پیش بر</p></div>
<div class="m2"><p>به نزدیک کدبانوی خویش بر</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>که خورد عرب شیر و خرما بود</p></div>
<div class="m2"><p>ازین دو عرب ناشکیبا بود</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>مگر چون خورد شیر بی داوری</p></div>
<div class="m2"><p>ببیند به جام اندر انگشتری</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>همین است حاجت مرا سوی تو</p></div>
<div class="m2"><p>ایا جان من بندهٔ روی تو</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>کنیزک بگفتا چو بیند چنین</p></div>
<div class="m2"><p>چه گوید که چون اوفتادست این</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>بدو گفت ورقه کی گفتی صواب</p></div>
<div class="m2"><p>ازین خسته دل باز بشنو جواب</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو کدبانوت بیند انگشتری</p></div>
<div class="m2"><p>اگر با تو جوید ره داوری</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>چنین گوی با بانوی بانوان</p></div>
<div class="m2"><p>همانا فتادست ازین میهمان</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>که می شیر خوردست از لاغری</p></div>
<div class="m2"><p>فتادست از انگشتش انگشتری</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>برو آنچ گفتم تو فرمان بکن</p></div>
<div class="m2"><p>کزین شاد گردد دل سرو بن</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>کنیزک بدو گفت کاو مر ترا</p></div>
<div class="m2"><p>چه داند و یا تو چه دانی ورا</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>نباید که بر جانت آید گزند</p></div>
<div class="m2"><p>بخود بر ببخشای ای مستمند</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>که کدبانوم هست والامنش</p></div>
<div class="m2"><p>گریزنده از مردم بد کنش</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>ولیکن مرا بر تو ای دل کئیب</p></div>
<div class="m2"><p>همی رحمت آید کنون ای غریب</p></div></div>