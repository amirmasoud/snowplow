---
title: >-
    بخش ۹ - شعر گفتن ورقه
---
# بخش ۹ - شعر گفتن ورقه

<div class="b" id="bn1"><div class="m1"><p>بگفت ای چراغ دل و جان من</p></div>
<div class="m2"><p>بت گل رخ و جان و جانان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هجر اندرون کرد نتوان درنگ</p></div>
<div class="m2"><p>شود نرم از عشق پولاد و سنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگارم شد و شد ز هجرش مرا</p></div>
<div class="m2"><p>هم از دل نشاط و هم از روی رنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون کم قضا سوی اوره نمود</p></div>
<div class="m2"><p>نگیرم دگر در صبوری درنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جان و ز خون معادی کنم</p></div>
<div class="m2"><p>هوا تیره فام و زمین لاله رنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیارم شبیخون،‌ نسازم کمین</p></div>
<div class="m2"><p>کزین هر دو بر مرد عارست و ننگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتم گر بکام نهنگ اندرست</p></div>
<div class="m2"><p>برون آرم او را ز کام نهنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخون ربیع ابن عدنان کنون</p></div>
<div class="m2"><p>بشویم دل و جان به شمشیر جنگ</p></div></div>