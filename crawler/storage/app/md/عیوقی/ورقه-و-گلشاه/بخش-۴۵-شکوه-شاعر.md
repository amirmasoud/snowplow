---
title: >-
    بخش ۴۵ - شکوهٔ شاعر
---
# بخش ۴۵ - شکوهٔ شاعر

<div class="b" id="bn1"><div class="m1"><p>دریغا که بد مهر گردان جهان</p></div>
<div class="m2"><p>ندارد وفا با کسی جاودان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباید همی بست دل را در اوی</p></div>
<div class="m2"><p>که بس نابکارست و بس زشت روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسا مهر پیوسته و بسته دل</p></div>
<div class="m2"><p>که او کرد بی کام دل زیر گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس امّیدها را که در دل شکست</p></div>
<div class="m2"><p>بسی بندها کو گشاد و ببست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر من بگویم که با من چه کرد</p></div>
<div class="m2"><p>چه آورد پیشم ز داغ و ز درد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بماند عجب هر کس از کار من</p></div>
<div class="m2"><p>خورد تا به جاوید تیمار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا قصه زین طرفه تر اوفتاد</p></div>
<div class="m2"><p>ولیکن نیارم گذشتن به یاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر زندگانی بود، آن سمر</p></div>
<div class="m2"><p>بگویم که چون بد همه سر بسر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه کردند با من ز مکر و حیل</p></div>
<div class="m2"><p>کسانی که‌شان بود دل پر دغل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز مرد و زن و پیر و برنا به هم</p></div>
<div class="m2"><p>ز شهری و ترک و ز بیش و ز کم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپردم به یزدان من آن را تمام</p></div>
<div class="m2"><p>که یزدان کند حکم روز قیام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ستاند ز هر ناکسی داد من</p></div>
<div class="m2"><p>رسد روز محشر به فریاد من</p></div></div>