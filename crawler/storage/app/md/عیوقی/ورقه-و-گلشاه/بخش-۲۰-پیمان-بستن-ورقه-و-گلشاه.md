---
title: >-
    بخش ۲۰ - پیمان بستن ورقه و گلشاه
---
# بخش ۲۰ - پیمان بستن ورقه و گلشاه

<div class="b" id="bn1"><div class="m1"><p>بشد سوی کانه دو تا کرده پشت</p></div>
<div class="m2"><p>جگر سوخته دل گرفته به مشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنالید پیش وی از داغ و درد</p></div>
<div class="m2"><p>ببارید خون آبه بر روی زرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزاری چنین گفت: ای بنت عم</p></div>
<div class="m2"><p>قضامان جدا کرد خواهد زهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی تازیم در وفای توم</p></div>
<div class="m2"><p>اسیر تو و خاک پای توم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر از مهر من بر دلت باک نیست</p></div>
<div class="m2"><p>مرا جایگاه بهتر از خاک نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وگر با منت هست پیوند مهر</p></div>
<div class="m2"><p>مبر دل ز مهر من ای خوب چهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که من بی تو چون ماهیی بر شخم</p></div>
<div class="m2"><p>بسان گنه کار در دوزخم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفت این سخن را و بر تخت زر</p></div>
<div class="m2"><p>فروریخت از چشم لؤلوی تر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو گلشه زورقه شنید این سخن</p></div>
<div class="m2"><p>بنالید آن سر و تن سیم بن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین گفت کای نزهت کام من</p></div>
<div class="m2"><p>ز نامت مبادا جدا نام من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به مهرم دل و جانت پیوسته باد</p></div>
<div class="m2"><p>ببند وفا جان من بسته باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میان من و تو جدایی مباد</p></div>
<div class="m2"><p>ز چرخ فلک بی وفایی مباد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر از روی من می بتابی توروی</p></div>
<div class="m2"><p>مجویم،‌ وگر جویی از خاک جوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفت این سخن را و بر ارغوان</p></div>
<div class="m2"><p>ز مژگان ببارید سیل روان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرفت آنگهی دست ورقه به دست</p></div>
<div class="m2"><p>تن خود به سوگند و پیمان ببست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کی گر بی تو هرگز بوم شاد کام</p></div>
<div class="m2"><p>وگر بینم از هیچ کس جز تو کام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>و گر با شگونه شود چرخ پیر</p></div>
<div class="m2"><p>به دست بداندیش مانم اسیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنم مسکن خویشتن تیره خاک</p></div>
<div class="m2"><p>از آن پس کجا گشته باشم هلاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفت این و از هر دو ببرید هوش</p></div>
<div class="m2"><p>بهٔک ره برآمد ز هر دو خروش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ببستند پیمان و عهد و وفا</p></div>
<div class="m2"><p>کزیشان کسی پیش نارد جفا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بورقه چنین گفت گلشه کی خیر</p></div>
<div class="m2"><p>سوی مامک و بابکم شو تو نیز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به سوگند مر هر دوان بسته کن</p></div>
<div class="m2"><p>دل هر دو با عهد پیوسته کن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کی بر تو دگر کس به دل ناورند</p></div>
<div class="m2"><p>بجز راه عهد و وفا نسپرند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفت ورقه ز گفتار تو</p></div>
<div class="m2"><p>نتابم سر از مهر و دیدار تو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سوی باب گلشاه شد برده دل</p></div>
<div class="m2"><p>سراسیمه و زار و آزرده دل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>أبا هر و پیمان ببست استوار</p></div>
<div class="m2"><p>ز بهر سفر را بسیجید کار</p></div></div>