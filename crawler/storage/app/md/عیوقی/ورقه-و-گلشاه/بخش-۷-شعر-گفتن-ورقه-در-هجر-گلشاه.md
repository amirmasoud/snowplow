---
title: >-
    بخش ۷ - شعر گفتن ورقه در هجر گلشاه
---
# بخش ۷ - شعر گفتن ورقه در هجر گلشاه

<div class="b" id="bn1"><div class="m1"><p>کجا رفتی ای دل گسل یار من</p></div>
<div class="m2"><p>مگر سیر گشتی ز دیدار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نجستم بتا هرگز آزار تو</p></div>
<div class="m2"><p>چرا جستی ای دوست آزار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چگونست بی من بتا کار تو</p></div>
<div class="m2"><p>که با جان رسید از عنا کار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز من زارتر گردی اندر فراق</p></div>
<div class="m2"><p>اگر بشنوی نالهٔ زار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر تست ز نهار جان و دلم</p></div>
<div class="m2"><p>نگه دار زنهار زنهار من</p></div></div>