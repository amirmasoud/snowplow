---
title: >-
    بخش ۱ - طبق نظر محققان شانزده بیت ابتدایی توسط نسخه برداران اضافه شده و از همای و همایون خواجوی کرمانی وام گرفته شده است
---
# بخش ۱ - طبق نظر محققان شانزده بیت ابتدایی توسط نسخه برداران اضافه شده و از همای و همایون خواجوی کرمانی وام گرفته شده است

<div class="b" id="bn1"><div class="m1"><p>به نام خداوند بالا و پست</p></div>
<div class="m2"><p>که از هستی اش هست شد هر چه هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فروزندهٔ شمسهٔ خاوری</p></div>
<div class="m2"><p>برآرندهٔ طاق نیلوفری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معطر کن باد عنبر نسیم</p></div>
<div class="m2"><p>نظام آور کار در یتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه پیکر، نگارندهٔ پیکران</p></div>
<div class="m2"><p>نه اختر، بر آرندهٔ اختران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهاندار بخشندهٔ کامکار</p></div>
<div class="m2"><p>خداوند بیچون پروردگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از خاک ره برنگیری سرم</p></div>
<div class="m2"><p>روم مصطفی را شفیع آورم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلام من العالم الحاکم</p></div>
<div class="m2"><p>علی روضة المصطفی الهاشمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شفیع امم خاتم انبیا</p></div>
<div class="m2"><p>سپهر رسالت مه اصفیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلید در گنج رب جلیل</p></div>
<div class="m2"><p>امام هدی در درج خلیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شه آسمان قدر و سیاره جیش</p></div>
<div class="m2"><p>مه هاشمی آفتاب قریش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هزاران درود از جهان آفرین</p></div>
<div class="m2"><p>سوی روضهٔ سید المرسلین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>الهی چو اومیدوارم به تو</p></div>
<div class="m2"><p>برآور اومیدی که دارم به تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رهی پیشم آور که در هر قدم</p></div>
<div class="m2"><p>زنم دم به دم در رضای تو دم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در آموز شکرم چو بخشیم گنج</p></div>
<div class="m2"><p>صبوریم ده چون فرستیم رنج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز شرم گنه آب رویم مبر</p></div>
<div class="m2"><p>چو خاکم ز تقصیر من درگدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>توقع همین دارم ای کردگار</p></div>
<div class="m2"><p>که در رستخیزم کنی رستگار</p></div></div>