---
title: >-
    بخش ۳۹ - چو گفت این، بگفتش که ای رادمرد
---
# بخش ۳۹ - چو گفت این، بگفتش که ای رادمرد

<div class="b" id="bn1"><div class="m1"><p>چو گفت این، بگفتش که ای رادمرد</p></div>
<div class="m2"><p>نگر تا توانی مرا چاره کرد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین داد داننده وی را جواب</p></div>
<div class="m2"><p>که ای برده عشق از رخت رنگ و آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر از درد خواهی روان رسته کرد</p></div>
<div class="m2"><p>به نزدیک آن شو کت او بسته کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز گفتار او ورقه ازدیده خون</p></div>
<div class="m2"><p>ببارید و بر خاک شد سرنگون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو با هش بیامد از آن جایگاه</p></div>
<div class="m2"><p>براند و سبک روی دادش براه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به روزی چنان بیست ره بیش و کم</p></div>
<div class="m2"><p>گسستیش هوش و بریدیش دم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو از روی گلشاه حورا مثال</p></div>
<div class="m2"><p>ز پیش دلش صف کشیدی خیال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شدی لرز لرزان دل اندر برش</p></div>
<div class="m2"><p>ز بالا به خاک آمدی پیکرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدین حال رفتی دو منزل زمین</p></div>
<div class="m2"><p>دل اندر کف عشق آن حور عین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هوا زی ز گلشه یکی یاد کرد</p></div>
<div class="m2"><p>رخش گشت زرد و دمش گشت سرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز مرکب فروگشت، آمد بزیر</p></div>
<div class="m2"><p>بگفتا: به غم در بماندیم دیر!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی گشت بر خاک برسان مست</p></div>
<div class="m2"><p>گرفته دل خویشتن را به دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گه آمد به هوش و گهی شد ز هوش</p></div>
<div class="m2"><p>گهی پرخروش و گهی باخروش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سوی آن ره آمد ز بیجای اوی</p></div>
<div class="m2"><p>که بد معدن آن بت مهرجوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بنالید و گفت ای دلارام من</p></div>
<div class="m2"><p>ز مهرت سیه گشت ایام من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل خسته را ای گرانمایه ول</p></div>
<div class="m2"><p>سوی خاک بردم ز مهر تو دل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به پایان شد این درد و پالود رنج</p></div>
<div class="m2"><p>پس پشت کردم سرای سپنج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>روانی که در محنت افتاده بود</p></div>
<div class="m2"><p>بدان باز دادم که او داده بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا برد زین گیتی ای دوست مهر</p></div>
<div class="m2"><p>ز تو دور بادا بلای سپهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کنون کز تو گم گشت نام رهی</p></div>
<div class="m2"><p>بزی شادمان ای چو سروسهی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مبادا کس ای لعبت دلفروز</p></div>
<div class="m2"><p>چو من گم شده بخت و برگشته روز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگفت این و کردش یکی شعر یاد</p></div>
<div class="m2"><p>حدیث جهان، گفت، بادست باد!</p></div></div>