---
title: >-
    بخش ۳ - آغاز قصه
---
# بخش ۳ - آغاز قصه

<div class="b" id="bn1"><div class="m1"><p>سخن بهتر از نعمت و خواسته</p></div>
<div class="m2"><p>سخن بهتر از گنج آراسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن مر سخنگوی را مایه بس</p></div>
<div class="m2"><p>سخن بر تن مرد پیرایه بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دانا سخن بشنو و گوش کن</p></div>
<div class="m2"><p>که نامد گهر ز آسمان جز سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن مرد را سر به گردون کشد</p></div>
<div class="m2"><p>سخن کوه را سوی هامون کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن بر تو نیکو کند کار زشت</p></div>
<div class="m2"><p>سخن ره نماید به سوی بهشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفتم به شیرین سخن این سمر</p></div>
<div class="m2"><p>که کس نیست گفته ازین پیشتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین قصه ای را کس از خاص و عام</p></div>
<div class="m2"><p>نگوید بدین وزن و انشا تمام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من و حجره و توبه از شاعری</p></div>
<div class="m2"><p>گسسته شد اندر میان داوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من از بهر آن افسر سروری</p></div>
<div class="m2"><p>سخن راند خواهم به لفظ دری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن بی شک از نظم رنگین شود</p></div>
<div class="m2"><p>عروس از مشاطه به آیین شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخن را بیاراست خواهم همی</p></div>
<div class="m2"><p>جمال از خرد خواست خواهم همی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به نظم آورم سرگذشتی عجب</p></div>
<div class="m2"><p>ز اخبار تازی و کتب عرب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین خواندم این قصهٔ دلپذیر</p></div>
<div class="m2"><p>ز اخبار تازی و کتب جریر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو از مکه پیغمبر ابطحی</p></div>
<div class="m2"><p>به یثرب شد و کار دین شد قوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگسترد او در عرب دین پاک</p></div>
<div class="m2"><p>سر سرکشان اندر آمد به خاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زدود از دل کافران کافری</p></div>
<div class="m2"><p>به شمشیر و برهان پیغمبری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه حی‌های عرب سر به سر</p></div>
<div class="m2"><p>سوی داد و دین آوریدند سر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی حی بود اندران روزگار</p></div>
<div class="m2"><p>چو ارژنگ مانی به رنگ و نگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو گفتی ز بس نعمت و خواسته</p></div>
<div class="m2"><p>یکی کشوری بود آراسته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بنی شیبه بد نام آن جایگاه</p></div>
<div class="m2"><p>سپاهی درو صفدر و کینه خواه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدو در دو سالار والامنش</p></div>
<div class="m2"><p>هنرورز و بهروز و نیکو کنش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دو سالار و آن هر دو از یک گهر</p></div>
<div class="m2"><p>برادر ز یک مام وز یک پدر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مر آن هر دو سالار را بود نام</p></div>
<div class="m2"><p>یکی را هلال و یکی را همام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مهین بود بر حسن و بر چابکی</p></div>
<div class="m2"><p>برآمد ذکی از گه کودکی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مر آن را کجا نام او بد هلال</p></div>
<div class="m2"><p>یکی دختری بود حورا مثال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی سروبن بود آراسته</p></div>
<div class="m2"><p>بتی چون بهاری پر از خواسته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی گوهری بود پر نام و ننگ</p></div>
<div class="m2"><p>یکی گلبنی بود پر بوی و رنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرو را پدر نام گل شه نهاد</p></div>
<div class="m2"><p>که خورشید رخ بود و حورا نژاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو گلشاه و چون ورقهٔ تیز مهر</p></div>
<div class="m2"><p>نبود و نپرورد گردان سپهر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو دو سرو بودند در بوستان</p></div>
<div class="m2"><p>گرازان به کام و دل دوستان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یکی ماه عارض یکی لاله خد</p></div>
<div class="m2"><p>یکی سیم ساعد یکی سرو قد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به یک جای بودند هر دو به هم</p></div>
<div class="m2"><p>که این ابن عم بود و آن بنت عم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز رفت قضا وز گذشت سپهر</p></div>
<div class="m2"><p>هم از کودکیشان بپیوست مهر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دل هر دو بر یکدگر گشت گرم</p></div>
<div class="m2"><p>روانشان پر از مهر و آزرم و شرم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنان شد دل آن دو نخل ببر</p></div>
<div class="m2"><p>که نشکیفتند ایچ از یکدگر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه بی آن دل این همی کام یافت</p></div>
<div class="m2"><p>نه بی این زمانی وی آرام یافت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دل هر دو از کودکی شد تباه</p></div>
<div class="m2"><p>به درمان و حیلت نیامد به راه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو ده سال پروردشان روزگار</p></div>
<div class="m2"><p>نشاندندشان پیش آموزگار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>معلم به تعلیم شد در شتاب</p></div>
<div class="m2"><p>که تا هر دو گشتند فرهنگ یاب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر چند در عشق می سوختند</p></div>
<div class="m2"><p>بی اندازه فرهنگ آموختند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو فارغ شدندی ز تعلیم‌گر</p></div>
<div class="m2"><p>به مهر آمدندی بر یکدگر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به سوی وی این گاه نگریستی</p></div>
<div class="m2"><p>دمی بر زدی سرد و بگریستی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گه آن سوی این دیده انداختی</p></div>
<div class="m2"><p>به ناله دل از غم بپرداختی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو خالی شدی جای آموزگار</p></div>
<div class="m2"><p>دل آن دو آسیمهٔ روزگار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به شوق وصال اندر آمیختی</p></div>
<div class="m2"><p>فراق از بر هر دو بگریختی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گه این از لب آن شکرچین شدی</p></div>
<div class="m2"><p>گه آن عذرخواهندهٔ این شدی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گه از زلف این،‌ آن گشادی گره</p></div>
<div class="m2"><p>گه از جعد آن، این ربودی زره</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گه این شکر ناب آن خورد خوش</p></div>
<div class="m2"><p>گه آن زلف پُرتاب این گیر کش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو آموزگار آمدی باز جای</p></div>
<div class="m2"><p>شدندی سراسیمه و سست رای</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>برین سان همی دانش آموختند</p></div>
<div class="m2"><p>به مهر دل اندر، همی سوختند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر آن هر دو بیچاره از رنج و تاب</p></div>
<div class="m2"><p>سیه بود روز و تبه بود خواب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو شد عمر هر دو ده و پنج سال</p></div>
<div class="m2"><p>شدند از هنر آفتاب کمال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو گوهر شدند آن دو اندر صدف</p></div>
<div class="m2"><p>چو خورشید گشتند اندر شرف</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هنریاب گشتند و فرهنگ‌یاب</p></div>
<div class="m2"><p>سخنگوی گشتند و حاضرجواب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چنان گشت ورقه ز فرهنگ و رای</p></div>
<div class="m2"><p>که کُه را به نیرو بکندی ز جای</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سواری شجاع که به هنگام جنگ</p></div>
<div class="m2"><p>همی خون گرست از نهیبش پلنگ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به قوت سر پیل بر تافتی</p></div>
<div class="m2"><p>به ناوک دل شیر بشکافتی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به شمشیر پولاد بگذاشتی</p></div>
<div class="m2"><p>به نیرو که از جای برداشتی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شجاعی که اندر مصاف نبرد</p></div>
<div class="m2"><p>ز دریا برانگیختی تیره گرد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ابا این همه هیبت و دستگاه</p></div>
<div class="m2"><p>دلش بود در عشق گلشه تباه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شب و روز با مهر پیوسته بود</p></div>
<div class="m2"><p>که از کودکی باز دلخسته بود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به حی خود اندر میان عرب</p></div>
<div class="m2"><p>به بیگاه و گاه و به روز و به شب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بتی بود پر ظرف و پر حسن و زیب</p></div>
<div class="m2"><p>دو چشم از عتیب و دو زلف از نهیب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>درفشان مهی بود بر زادسرو</p></div>
<div class="m2"><p>پراگنده بر ماه خون تذرو</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>فگنده به لولو بر از لاله بند</p></div>
<div class="m2"><p>پراگنده بر سرو سیمین کمند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>پراگنده شمشاد را در عبیر</p></div>
<div class="m2"><p>نهان کرده پولاد را در حریر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سمن برگ او زیر مشکین گره</p></div>
<div class="m2"><p>گره بر گره صد هزاران زره</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز عنبر نهاده به گل بر کله</p></div>
<div class="m2"><p>ز سنبل علم بسته بر سنبله</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همه روی حسن و همه موی میم</p></div>
<div class="m2"><p>همه زلف تاب و همه جعد جیم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سیه نرگس ناوک‌انداز اوی</p></div>
<div class="m2"><p>بگسترده اندر عرب راز اوی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به حی بنی شیبه در کس نماند</p></div>
<div class="m2"><p>که او نامهٔ عشق گلشه نخواند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>شده ورقه مسکین دل سوخته</p></div>
<div class="m2"><p>به دل در ز عشق آتش افروخته</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بدان هر دو زیبابت کش خرام</p></div>
<div class="m2"><p>عجب شادمانه دل باب و مام</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز دل دادن آن دو سرو سهی</p></div>
<div class="m2"><p>ز احوالشان یافتند آگهی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چو هنگام بیداری و جای خواب</p></div>
<div class="m2"><p>ندیدند ازیشان ره ناصواب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>در هر دو مسکین نگه داشتند</p></div>
<div class="m2"><p>ز هم شان جدا کرد نگذاشتند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دل آن دو بیچارهٔ دلشده</p></div>
<div class="m2"><p>همه روز بودی چو آتشکده</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو شب مایهٔ قیرگون خواستی</p></div>
<div class="m2"><p>فلک را به گوهر بیاراستی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>از آرامگه آن دو نخل ببر</p></div>
<div class="m2"><p>برون آمدندی بر یکدگر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>گه این بر گشادی بر آن راز خویش</p></div>
<div class="m2"><p>گه آن عرضه کردی برین ناز خویش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گه عشق بر هر دو غم بیختی</p></div>
<div class="m2"><p>گه این زان و آن زین درآویختی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>دو غمشان گه عشق گشتی هزار</p></div>
<div class="m2"><p>دو لبشان گه بوسه گشتی چهار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>که در دیده شان نامدی هیچ خواب</p></div>
<div class="m2"><p>نرفتی میانشان سخن ناصواب</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو بر سر نهادی فلک تاج زر</p></div>
<div class="m2"><p>شه روم بر زنگ کردی حشر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>دو دلسوخته عاشق تیره رای</p></div>
<div class="m2"><p>شدندی به تیمار و غم باز جای</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو از شانزده سالشان برگذشت</p></div>
<div class="m2"><p>همه حال گیتی دگر گونه گشت</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>غم عشق در هر دو دل کار کرد</p></div>
<div class="m2"><p>مر آن هر دو را زار و بیمار کرد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گل لعلشان شد به رنگ زریر</p></div>
<div class="m2"><p>کُه سیمشان شد چو تار حریر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به سوی پدرشان شد این آگهی</p></div>
<div class="m2"><p>که خمیده گشت آن دو سرو سهی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>دل مام و باب ارچه کانا بود</p></div>
<div class="m2"><p>به رنج پسر ناتوانا بود</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>پسر مر ترا دشمن منکرست</p></div>
<div class="m2"><p>ولکن ز جان بر تو شیرین ترست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو از حال ایشان خبر یافتند</p></div>
<div class="m2"><p>به وصل دو دلبند بشتافتند</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>دل و جان از انده بپرداختند</p></div>
<div class="m2"><p>به هر گوشه ای بزم برساختند</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بنی شیبه یکسر بیاراستند</p></div>
<div class="m2"><p>کجا سور کردن همی خواستند</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به هر جایگه آتش افروختند</p></div>
<div class="m2"><p>بر او عود و عنبر همی سوختند</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به شادی همی گردن افراشتند</p></div>
<div class="m2"><p>کجا نعره از چرخ بگذاشتند</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>غریویدن نای و آواز چنگ</p></div>
<div class="m2"><p>همی رفت هر جایگه بی درنگ</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>برآمد خروشیدن بم و زیر</p></div>
<div class="m2"><p>ز خاک سیه سوی چرخ اثیر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>می لعل رخشنده از سبز جام</p></div>
<div class="m2"><p>چو مریخ می تافت در گاه بام</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>هنوز آلت عقد ناکرده راست</p></div>
<div class="m2"><p>که از هر سویی غلغل و نعره خاست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>برآمد ز گردون و هامون خروش</p></div>
<div class="m2"><p>مصیبت شد آن شادی و ناز و نوش</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>زمین شد پر از مرد شمشیرزن</p></div>
<div class="m2"><p>که بد پیش شمشیرشان شیر، زن</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>سپاهی همه سرکش و تیره رای</p></div>
<div class="m2"><p>همه دیو دیدار و آهن قبای</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ز بهر شبیخون و از بهر کین</p></div>
<div class="m2"><p>تو گفتی که بر رسته اند از زمین</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>همه تیغها از نیام آخته</p></div>
<div class="m2"><p>همه کینه و جنگ را ساخته</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>شب تیره و زخم شمشیر تیز</p></div>
<div class="m2"><p>ازین صعب تر چون بود رستخیز</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>به کشتن همه گردن افراشتند</p></div>
<div class="m2"><p>کسی را همی زنده نگذاشتند</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>براندند بر خاک بر سیل خون</p></div>
<div class="m2"><p>شد از خون گردان زمین لاله گون</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>نخست از بنی شیبه کس نام و ننگ</p></div>
<div class="m2"><p>که با کس نبد ساز و آلات جنگ</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>گمانی نبرد ایچ کس در جهان</p></div>
<div class="m2"><p>که بتوان بریشان زدن ناگهان</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بدین روی غافل بدند آن گروه</p></div>
<div class="m2"><p>که در کین ز کس نامدیشان ستوه</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بماندند آن شب همه ممتحن</p></div>
<div class="m2"><p>که بی ساز بودند آن انجمن</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>هزبر ارچه چیره بود روز جنگ</p></div>
<div class="m2"><p>چگونه کند جنگ بی یشک و چنگ</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چو بی ساز بودند بگریختند</p></div>
<div class="m2"><p>تهی دست ابا خصم ناویختند</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چو زیشان عدو بی کرانی بخست</p></div>
<div class="m2"><p>ز کین باز کردند کوتاه دست</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>یکایک به تاراج دادند روی</p></div>
<div class="m2"><p>پراگنده گشت آن همه گفت و گوی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>یکی کشوری بود پرخواسته</p></div>
<div class="m2"><p>به چنگ آوریدند ناخواسته</p></div></div>