---
title: >-
    بخش ۲ - در ستایش سلطان محمود رحمه الله
---
# بخش ۲ - در ستایش سلطان محمود رحمه الله

<div class="b" id="bn1"><div class="m1"><p>دل پادشاهان شه خسروان</p></div>
<div class="m2"><p>که رایش بلندست و بختش جوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزرگی کی سازد همی رای اوی</p></div>
<div class="m2"><p>برافراز هفتم فلک پای اوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همیشه چنین دولتش یار باد</p></div>
<div class="m2"><p>خدای جهانش نگه دار باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل دولتش سال و مه تازه باد</p></div>
<div class="m2"><p>بزرگی و قدرش بی اندازه باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی تا آسمان بگذرد قدر اوی</p></div>
<div class="m2"><p>نهد بخت بر مشتری صدر اوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نتابد جهان گردن از رای اوی</p></div>
<div class="m2"><p>دهند اختران بوسه بر پای اوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمین بر نتابد همی گنج اوی</p></div>
<div class="m2"><p>زمانه نیاساید از خنج اوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه جود ابر سخا گستری</p></div>
<div class="m2"><p>گه فضل دریای پر گوهری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هژبر عرین سخرهٔ رزم اوست</p></div>
<div class="m2"><p>بهشت برین چاکر بزم اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نعیمست جایی کجا رام اوست</p></div>
<div class="m2"><p>جحیمست جایی که صمصام اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهارست جایی کجا روی اوست</p></div>
<div class="m2"><p>بهشتست جایی کجا خوی اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو جودش ببارد نبارد اَمَل</p></div>
<div class="m2"><p>چو تیغش بخندد بگرید اجل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فلک پایهٔ همتش را رهیست</p></div>
<div class="m2"><p>که در طلعتش فرّ شاهان شهیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ثنا را جز او کس خریدار نیست</p></div>
<div class="m2"><p>ثنا خود جز او را سزاوار نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر حلم او کوه را سنگ نیست</p></div>
<div class="m2"><p>بر طبع او باد را رنگ نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همیشه جهان بستهٔ نام اوست</p></div>
<div class="m2"><p>کز ایام او خوشتر ایام اوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرا زامر ملک اندر آرام نیست</p></div>
<div class="m2"><p>مبارک تر از نام او نام نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه مملکت همت و سنگ اوست</p></div>
<div class="m2"><p>جهانی همه فضل و فرهنگ اوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زمانه مزین به تأثیر اوست</p></div>
<div class="m2"><p>ولایت معین به تدبیر اوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپهر برین بستهٔ چهر اوست</p></div>
<div class="m2"><p>جهان را همه رغبت مهر اوست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رخش لعل باد و دلش شاد باد</p></div>
<div class="m2"><p>همیشه جهان را جهان دار باد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه ساله دل شاد و خرم زیاد</p></div>
<div class="m2"><p>از اهوال این دهر بی غم زیاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو تازه بادا دل دوستان</p></div>
<div class="m2"><p>چو برگ گل سرخ در بوستان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی تا به محشر مبیناد رنج</p></div>
<div class="m2"><p>ز فرزند و مال و ز ملک و ز گنج</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو عیّوقیا گرت هوش است ورای</p></div>
<div class="m2"><p>به خدمت بپیوند به مدحت گرای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به دل مهر سلطان غازی بجوی</p></div>
<div class="m2"><p>به جان مدح سلطان محمود گوی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ابوالقاسم آن شاه دین و دُوَل</p></div>
<div class="m2"><p>شهنشاه عالم امیر ملل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نبیند جهان و نزاید سپهر</p></div>
<div class="m2"><p>چنو راز و فرزانه و خوب چهر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در اقبال و در فضل و در هر فنی</p></div>
<div class="m2"><p>جهانیست در زیر پیراهنی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گه جود چون ابر با بخشش است</p></div>
<div class="m2"><p>گه علم دریای پر دانش است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تن جود و آزادگی را سر است</p></div>
<div class="m2"><p>سر فضل و فرهنگ را افسر است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گه فضل آرایش عالم است</p></div>
<div class="m2"><p>به هر علم فخر بنی آدم است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گه جود با شرم و با حشمت است</p></div>
<div class="m2"><p>که با گنج و مالست و با هیبت است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خدای جهان مرو را یار باد</p></div>
<div class="m2"><p>ز هر بد خدایش نگه دار باد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نهالی کی در اول نو بهار</p></div>
<div class="m2"><p>نشانده بدی عیدت آمد ببار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به باغ طرب در به فرخنده بخت</p></div>
<div class="m2"><p>نکشتست زو طرفه تر کس درخت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>درختی کی بیخش همه دانش است</p></div>
<div class="m2"><p>درختی کی شاخش همه رامش است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>درختی کی برگش همه نزهت است</p></div>
<div class="m2"><p>درختی کی بارش همه حکمت است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گل پایدار اندرو بادرنگ</p></div>
<div class="m2"><p>که تا حشر ازو نگسلد بوی و رنگ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بهر برگ او در هزاران کشیست</p></div>
<div class="m2"><p>بهر بوی او در هزاران خوشیست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کنون کآمد این گلبن نو ببر</p></div>
<div class="m2"><p>بر شاه ازو یادگاری ببر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کی این دستهٔ گل در ایام تست</p></div>
<div class="m2"><p>بهر برگ گل بر رقم نام تست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنان کن کنون تا به روز قضا</p></div>
<div class="m2"><p>نگردد ضِرامش زمانی جدا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ازیرا که هرگز نگردد کهن</p></div>
<div class="m2"><p>گل تازه کش اصل باشد سخن</p></div></div>