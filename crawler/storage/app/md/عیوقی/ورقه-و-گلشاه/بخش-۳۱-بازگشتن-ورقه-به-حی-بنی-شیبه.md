---
title: >-
    بخش ۳۱ - بازگشتن ورقه به حی بنی شیبه
---
# بخش ۳۱ - بازگشتن ورقه به حی بنی شیبه

<div class="b" id="bn1"><div class="m1"><p>نبد ورقه را ز آن حدیث آگهی</p></div>
<div class="m2"><p>که چونست احوال سرو سهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو انگشتری و زره سوی اوی</p></div>
<div class="m2"><p>رسید از بر گلشه خوب روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بشنید پیغام او از غلام</p></div>
<div class="m2"><p>که نالنده گشتست ماه تمام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ورقه آمد زانده به جوش</p></div>
<div class="m2"><p>ز بس غم نیارست بودن خموش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنالید و بگریست از هجر دوست</p></div>
<div class="m2"><p>همی بر تنش عشق بدرید پوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون آمد از شهر آراسته</p></div>
<div class="m2"><p>ابا مال و با نعمت و خواسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شه شهریار و وزیر و سپاه</p></div>
<div class="m2"><p>هر آن کس که بودند از پیشگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابا شادی و خرمی هم قرین</p></div>
<div class="m2"><p>برفتند با وی سه منزل زمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخشنودی او را به کردش گسی</p></div>
<div class="m2"><p>وزو عدرها خواست خسرو بسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آن شاه و لشکر ز نزدش برفت</p></div>
<div class="m2"><p>چو باد صبا ورقه ره برگرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر باب و آن بی کران عز و ناز</p></div>
<div class="m2"><p>به حی بنی شیبه آمد فراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیارست پرسید کس را خبر</p></div>
<div class="m2"><p>ز گلشاه گل عارض و سیم بر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بترسید از آن آنسر سرکشان</p></div>
<div class="m2"><p>که گر از کسی باز پرسد نشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مگر زو خبر به بدی گسترند</p></div>
<div class="m2"><p>دل شاد او را به غم بسپرند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طیان بود زین روی دل در برش</p></div>
<div class="m2"><p>که تا چه خبر یابد از دلبرش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بد او پیش و مال و غلامان ز پس</p></div>
<div class="m2"><p>همی بر نیامدش زانده نفس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو از ره سوی خیمهٔ عم رسید</p></div>
<div class="m2"><p>ز دیدار عم،‌ بر دلش غم رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عمش، باب گلشاه، چون روی اوی</p></div>
<div class="m2"><p>بدیدش دوید از عنا سوی اوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برفت او به حیلت گرفتش ببر</p></div>
<div class="m2"><p>همی گفت: نخلت نیامد ببر!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو سودست زین نعمت و مال تو</p></div>
<div class="m2"><p>که نیکو نبد کار و احوال تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو سودست این گنج تو مر مرا</p></div>
<div class="m2"><p>که رنجت نیاورد اکنون برا!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بپرسید ورقه کی گلشه کجاست</p></div>
<div class="m2"><p>که بی او مرا زنده بودن خطاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به ورقه عمش گفت کای جان عم</p></div>
<div class="m2"><p>مدار ایچ انده مدار ایچ غم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که هرچ از خداوند باشد قضا</p></div>
<div class="m2"><p>قضای ورا داد باید رضا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شکیبایی و صبر کاری نکوست</p></div>
<div class="m2"><p>کسی را که تنها بماند ز دوست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهانیست این پرفسون و فریب</p></div>
<div class="m2"><p>نشیبش فراز و فرازش نشیب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ندارد برو بر خردمند مهر</p></div>
<div class="m2"><p>که شیطان به فعلست و حورا به چهر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر آید چو ضرغام مرگ از کمین</p></div>
<div class="m2"><p>زند مرد را ناگهان برزمین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیابد رهایی ازو جانور</p></div>
<div class="m2"><p>ز دیو و ملک جن و انس ای پسر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ایا ورقهٔ بخرد نیک بخت</p></div>
<div class="m2"><p>ز مرگست بر ما همه بند سخت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خداوند مزدت دهاد اندرین</p></div>
<div class="m2"><p>که رفت آن گران مایه گل در زمین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قرین تو گلشاه فرخ نژاد</p></div>
<div class="m2"><p>روان آن ستد کو بدو باز داد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو گفتار او ورقه بشنید پاک</p></div>
<div class="m2"><p>بیفتاد چون مرده بر روی خاک</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زمانی زهش رفت و آنگاه باز</p></div>
<div class="m2"><p>بهش باز آمد یل سرفراز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پراگند بر زرد گل ارغوان</p></div>
<div class="m2"><p>رخش زرد شدراست چون زعفران</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دگر باره بر زد یکی باد سرد</p></div>
<div class="m2"><p>ز تیمار وز انده و داغ و درد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بیفتاد بر جای چون مردگان</p></div>
<div class="m2"><p>سراسیمه همچون دل آزردگان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برآمدش هوش و فرورفت دم</p></div>
<div class="m2"><p>تو گفتی دلش خون شد اندرشکم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بهٔک روز و یک شب نیامد بهوش</p></div>
<div class="m2"><p>بهٔک ره برآمد ز هر کس خروش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زدند آب بر روی دل خسته مرد</p></div>
<div class="m2"><p>بجنبید و برزد یکی باد سرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نگه کرد هر سو چو دل خفتگان</p></div>
<div class="m2"><p>سراسیمه برسان آشفتگان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو از عم خود مهربانی ندید</p></div>
<div class="m2"><p>ز گلشه بدانجا نشانی ندید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بزد دست بر تن سلب را درید</p></div>
<div class="m2"><p>بنالید وز چشم خون می دوید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بنالید و بر سر پراگند خاک</p></div>
<div class="m2"><p>به خاک اندر آلود رخسار پاک</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همی گفت: یا قوم یاری کنید</p></div>
<div class="m2"><p>به من بر بگریید و زاری کنید!</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که ماندم ز دل دارو ز آرام فرد</p></div>
<div class="m2"><p>دلم جای عشق و تنم جای درد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همی گفت وزدیدگان سیل بار</p></div>
<div class="m2"><p>یکی شعر گفت از غم عشق یار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بگفتا دریغا دریغا دریغ</p></div>
<div class="m2"><p>که شد ماه تابان من زیر میغ</p></div></div>