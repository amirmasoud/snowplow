---
title: >-
    قصیدهٔ شمارهٔ ۲۵
---
# قصیدهٔ شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>ای بی خبر ز منزل و پیش آهنگ</p></div>
<div class="m2"><p>دور از تو همرهان تو صد فرسنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در راه راست، کج چه روی چندین</p></div>
<div class="m2"><p>رفتار راست کن، تو نه ای خرچنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخسار خویش را نکنی روشن</p></div>
<div class="m2"><p>ز آئینهٔ دل ار نزدائی زنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گلشنی است دل که در آن روید</p></div>
<div class="m2"><p>از گلبنی هزار گل خوش رنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هر رهی فتاده و گمراهی</p></div>
<div class="m2"><p>تا نیست رهبرت هنر و فرهنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم تو خفته است، از آن هر کس</p></div>
<div class="m2"><p>زین باغ سیب میبرد و نارنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این روبهک به نیت طاوسی</p></div>
<div class="m2"><p>افکنده دم خویش به خم رنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بازیچه‌هاست گنبد گردان را</p></div>
<div class="m2"><p>نامی شنیده‌ای تو ازین شترنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دام بسته شبرو چرخت سخت</p></div>
<div class="m2"><p>در بر گرفته اژدر دهرت تنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>انجام کار در فکند ما را</p></div>
<div class="m2"><p>سنگیم ما و چرخ چو غلماسنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خار جهان چه میشکنی در چشم</p></div>
<div class="m2"><p>بر چهره چند میفکنی آژنک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سالک بهر قدم نفتد از پا</p></div>
<div class="m2"><p>عاقل ز هر سخن نشود دلتنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو آدمی نگر که بدین رتبت</p></div>
<div class="m2"><p>بیخود ز باده است و خراب از بنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گوهر فروش کان قضا، پروین</p></div>
<div class="m2"><p>یک ره گهر فروخته، صد ره سنگ</p></div></div>