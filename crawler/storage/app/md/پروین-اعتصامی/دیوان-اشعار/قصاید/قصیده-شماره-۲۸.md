---
title: >-
    قصیدهٔ شمارهٔ ۲۸
---
# قصیدهٔ شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>نفس گفتست بسی ژاژ و بسی مبهم</p></div>
<div class="m2"><p>به کز این پس کندش نطق خرد ابکم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره پر پیچ و خم آز چو بگرفتی</p></div>
<div class="m2"><p>روی درهم مکش ار کار تو شد درهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خشک شد زمزم پاکیزهٔ جان ناگه</p></div>
<div class="m2"><p>شستشو کرد هریمن چو درین زمزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به که از مطبخ وسواس برون آئیم</p></div>
<div class="m2"><p>تا که خود را برهانیم ز دود و دم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاخ مکر است درین کنگره مینا</p></div>
<div class="m2"><p>چاه مرگ است درین سیرگه خرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بداندیش فلک چند شوی ایمن</p></div>
<div class="m2"><p>ز ستم پیشه جهان چند کشی استم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ندیدی مگر این دانهٔ دانا کش</p></div>
<div class="m2"><p>تو ندیدی مگر این دامگه محکم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وارث ملک سلیمان نتوان خواندن</p></div>
<div class="m2"><p>هر کسیرا که در انگشت بود خاتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه هر لحظه بزخم تو زند زخمی</p></div>
<div class="m2"><p>تو ازو خیره چه داری طمع مرهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فلک آنگونه به ناورد دلیر آید</p></div>
<div class="m2"><p>که نه از زال اثر ماند و نز رستم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه ببخشود بموسی خلف عمران</p></div>
<div class="m2"><p>نه وفا کرد به عیسی پسر مریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تخت جمشید حکایت کند ار پرسی</p></div>
<div class="m2"><p>که چه آمد به فریدون و چه شد بر جم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز خوشیها چه شوی خوش که درین معبر</p></div>
<div class="m2"><p>به یکی سور قرین است دو صد ماتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو به نی بین که ز هر بند چسان نالد</p></div>
<div class="m2"><p>ز زبردستی ایام بزیر و بم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>داستان گویدت از بابلیان بابل</p></div>
<div class="m2"><p>عبرت آموزدت از دیلمیان دیلم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرصتی را که بدستست، غنیمت دان</p></div>
<div class="m2"><p>بهر روزی که گذشتست چه داری غم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان گل تازه که بشکفت سحرگاهان</p></div>
<div class="m2"><p>نه سر و ساق بجا ماند، نه رنگ و شم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر صباحیست، مسائی رسدش از پی</p></div>
<div class="m2"><p>ور بهاریست، خزانی بودش توام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صبحدم اشک بچهر گل از آن بینی</p></div>
<div class="m2"><p>که شبانگه بچمن گریه کند شبنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اندرین دشت مخوف، ای برهٔ مسکین</p></div>
<div class="m2"><p>بیم جانست، چه شد کز رمه کردی رم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مخور ای کودک بی تجربه زین حلوا</p></div>
<div class="m2"><p>که شد آمیخته با روغن و شهدش سم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دست و پائی بزن ای غرقه، توانی گر</p></div>
<div class="m2"><p>تا مگر باز رهانند تو را زین یم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مشک حیفست که با دوده شود همسر</p></div>
<div class="m2"><p>کبک زشتست که با زاغ شود همدم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برو ای فاخته، با مرغ سحر بنشین</p></div>
<div class="m2"><p>برو ای گل، بصف سرو و سمن بردم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز چنار آموز، ای دوست گرانسنگی</p></div>
<div class="m2"><p>چه شوی بر صفت بید ز بادی خم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خویش و پیوند هنر باش که تا روزی</p></div>
<div class="m2"><p>نروی از پی نان بر در خال و عم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روح را سیر کن از مائدهٔ حکمت</p></div>
<div class="m2"><p>بیکی نان جوین سیر شود اشکم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جز که آموخت ترا که خواب و خور غفلت</p></div>
<div class="m2"><p>به چه کار آمدت این سفله تن ملحم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خزفست اینکه تو داریش چنو گوهر</p></div>
<div class="m2"><p>رسن است اینکه تو بینیش چو ابریشم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مار خود، هم تو خودی، مار چه افسائی</p></div>
<div class="m2"><p>بخود، ای بیخبر از خویش، فسون میدم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز تو در هر نفسی کاسته میگردد</p></div>
<div class="m2"><p>غم خود خور، چه خوری انده بیش و کم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بیم آنست که صراف قضا ناگه</p></div>
<div class="m2"><p>زر سرخ تو بگیرد به یکی درهم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کشت یک دانه کسی را ندهد خرمن</p></div>
<div class="m2"><p>بذل یک جوز کسی را نکند حاتم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به پری پر، که عقابان نکنندت سر</p></div>
<div class="m2"><p>به رهی رو، که بزرگان نکنندت ذم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جان چو کان آمد و دانش گهرش، پروین</p></div>
<div class="m2"><p>دل چو خورشید شد و ملک تنش عالم</p></div></div>