---
title: >-
    قصیدهٔ شمارهٔ ۳۷
---
# قصیدهٔ شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>گردون نرهد ز تند رفتاری</p></div>
<div class="m2"><p>گیتی ننهد ز سر سیه‌کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گرگ چه آمدست جز گرگی</p></div>
<div class="m2"><p>وز مار چه خاستست جز ماری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس بی بصری، اگر چه بینائی</p></div>
<div class="m2"><p>بس بیخبری، اگر چه هشیاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو غافلی و سپهر گردان را</p></div>
<div class="m2"><p>فارغ ز فسون و فتنه پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو گندم آسیای گردونی</p></div>
<div class="m2"><p>گر یکمن و گر هزار خرواری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معماری عقل چون نپذرفتی</p></div>
<div class="m2"><p>در ملک تو جهل کرد معماری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوداگر در شاهوارستی</p></div>
<div class="m2"><p>خر مهره چرا کنی خریداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنهار، مخواه از جهان زنهار</p></div>
<div class="m2"><p>کاین سفله بکس نداد زنهاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرگار زمانه بر تو میگردد</p></div>
<div class="m2"><p>چون نقطه تو در حصار پرگاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکچند شوی بخواب چون مستان</p></div>
<div class="m2"><p>ناگه برسد زمان بیداری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آید گه در گذشتنت ناچار</p></div>
<div class="m2"><p>خود بگذری، آنچه هست بگذاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رفتند بچابکی سبکباران</p></div>
<div class="m2"><p>زین مرحله، ای خوشا سبکباری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کردار بد تو گشت ز نگارش</p></div>
<div class="m2"><p>آیینه دل نبود زنگاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از لقمهٔ تن بکاه تا روزی</p></div>
<div class="m2"><p>بر آتش آز دیگ مگذاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بشناس زیان ز سود، تا وقتی</p></div>
<div class="m2"><p>سرمایه بدست دزد نسپاری</p></div></div>