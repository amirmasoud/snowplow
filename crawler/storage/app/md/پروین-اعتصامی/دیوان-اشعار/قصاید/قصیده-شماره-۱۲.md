---
title: >-
    قصیدهٔ شمارهٔ ۱۲
---
# قصیدهٔ شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>اگر چه در ره هستی هزار دشواریست</p></div>
<div class="m2"><p>چو پر کاه پریدن ز جا سبکساریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پات رشته فکندست روزگار و هنوز</p></div>
<div class="m2"><p>نه آگهی تو که این رشتهٔ گرفتاریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگرگ مردمی آموزی و نمیدانی</p></div>
<div class="m2"><p>که گرگ را ز ازل پیشه مردم آزاریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپرس راه ز علم، این نه جای گمراهیست</p></div>
<div class="m2"><p>بخواه چاره ز عقل، این نه روز ناچاریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهفته در پس این لاجورد گون خیمه</p></div>
<div class="m2"><p>هزار شعبده‌بازی، هزار عیاریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلام دزد مگیر و متاع دیو مخواه</p></div>
<div class="m2"><p>چرا که دوستی دشمنان ز مکاریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آن مریض که پند طبیب نپذیرد</p></div>
<div class="m2"><p>سزاش تاب و تب روزگار بیماریست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بچشم عقل ببین پرتو حقیقت را</p></div>
<div class="m2"><p>مگوی نور تجلی فسون و طراریست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر که در دل شب خون نمیکند گردون</p></div>
<div class="m2"><p>بوقت صبح چرا کوه و دشت گلناریست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگاهوار تو افعی نهفت دایهٔ دهر</p></div>
<div class="m2"><p>مبرهن است که بیزار ازین پرستاریست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپرده‌ای دل مفتون خود بمعشوقی</p></div>
<div class="m2"><p>که هر چه در دل او هست، از تو بیزاریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدار دست ز کشتی که حاصلش تلخیست</p></div>
<div class="m2"><p>بپوش روی ز آئینه‌ای که زنگاریست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخیره بار گران زمانه چند کشی</p></div>
<div class="m2"><p>ترا چه مزد بپاداش این گرانباریست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرشته زان سبب از کید دیو بیخبر است</p></div>
<div class="m2"><p>که اقتضای دل پاک، پاک انگاریست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بلند شاخهٔ این بوستان روح افزای</p></div>
<div class="m2"><p>اگر ز میوه تهی شد، ز پست دیواریست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو هیچگاه به کار نکو نمیگرویم</p></div>
<div class="m2"><p>شگفت نیست گر آئین ما سیه کاریست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برو که فکرت این سودگر معامله نیست</p></div>
<div class="m2"><p>متاع او همه از بهر گرم بازاریست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بخر ز دکهٔ عقل آنچه روح می‌طلبد</p></div>
<div class="m2"><p>هزار سود نهان اندرین خریداریست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زمانه گشت چو عطار و خون هر سگ و خوک</p></div>
<div class="m2"><p>فروخت بر همه و گفت مشک تاتاریست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گلش مبو که نه شغلیش غیر گلچینیست</p></div>
<div class="m2"><p>غمش مخور که نه کاریش غیر خونخواریست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قضا چو قصد کند، صعوه‌ای چو ثعبانی است</p></div>
<div class="m2"><p>فلک چو تیغ کشد، زخم سوزنی کاریست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کدام شمع که ایمن ز باد صبحگهی است</p></div>
<div class="m2"><p>کدام نقطه که بیرون ز خط پرگاریست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عمارت تو شد است این چنین خراب ولیک</p></div>
<div class="m2"><p>بخانهٔ دگران پیشهٔ تو معماریست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدان صفت که تو هستی دهند پاداشت</p></div>
<div class="m2"><p>سزای کار در آخر همان سزاواریست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بهل که عاقبت کار سرنگونت کند</p></div>
<div class="m2"><p>بلندئی که سرانجام آن نگونساریست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گریختن ز کژی و رمیدن از پستی</p></div>
<div class="m2"><p>نخست سنگ بنای بلند مقداریست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز روشنائی جان، شامها سحر گردد</p></div>
<div class="m2"><p>روان پاک چو خورشید و تن شب تاریست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چراغ دزد ز مخزن پدید شد، پروین</p></div>
<div class="m2"><p>زمان خواب گذشتست، وقت بیداریست</p></div></div>