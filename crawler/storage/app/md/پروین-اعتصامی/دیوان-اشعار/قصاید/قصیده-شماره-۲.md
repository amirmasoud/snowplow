---
title: >-
    قصیدهٔ شمارهٔ ۲
---
# قصیدهٔ شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>کار مده نفس تبه کار را</p></div>
<div class="m2"><p>در صف گل جا مده این خار را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشته نکودار که موش هوی</p></div>
<div class="m2"><p>خورده بسی خوشه و خروار را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ و زمین بندهٔ تدبیر تست</p></div>
<div class="m2"><p>بنده مشو درهم و دینار را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همسر پرهیز نگردد طمع</p></div>
<div class="m2"><p>با هنر انباز مکن عار را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که شدی تاجر بازار وقت</p></div>
<div class="m2"><p>بنگر و بشناس خریدار را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخ بدانست که کار تو چیست</p></div>
<div class="m2"><p>دید چو در دست تو افزار را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بار وبال است تن بی تمیز</p></div>
<div class="m2"><p>روح چرا می‌کشد این بار را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کم دهدت گیتی بسیاردان</p></div>
<div class="m2"><p>به که بسنجی کم و بسیار را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نزند راهروی را بپای</p></div>
<div class="m2"><p>به که بکوبند سر مار را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیره نوشت آنچه نوشت اهرمن</p></div>
<div class="m2"><p>پاره کن این دفتر و طومار را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیچ خردمند نپرسد ز مست</p></div>
<div class="m2"><p>مصلحت مردم هشیار را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روح گرفتار و بفکر فرار</p></div>
<div class="m2"><p>فکر همین است گرفتار را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آینهٔ تست دل تابناک</p></div>
<div class="m2"><p>بستر از این آینه زنگار را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دزد بر این خانه از آنرو گذشت</p></div>
<div class="m2"><p>تا بشناسد در و دیوار را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چرخ یکی دفتر کردارهاست</p></div>
<div class="m2"><p>پیشه مکن بیهده کردار را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دست هنر چید، نه دست هوس</p></div>
<div class="m2"><p>میوهٔ این شاخ نگونسار را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رو گهری جوی که وقت فروش</p></div>
<div class="m2"><p>خیره کند مردم بازار را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در همه جا راه تو هموار نیست</p></div>
<div class="m2"><p>مست مپوی این ره هموار را</p></div></div>