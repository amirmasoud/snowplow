---
title: >-
    قصیدهٔ شمارهٔ ۲۲
---
# قصیدهٔ شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>کارها بود در این کارگه اخضر</p></div>
<div class="m2"><p>لیک دوک تو نگردید ازین بهتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر این رشته گرفتی و ندانستی</p></div>
<div class="m2"><p>که هریمنش گرفتست سر دیگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موجها کرده مکان در لب این دریا</p></div>
<div class="m2"><p>شعله‌ها گشته نهان در دل این مجمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو ندانم به چه امید نهادستی</p></div>
<div class="m2"><p>کالهٔ خویش در این کشتی بی لنگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای غفلت چه نهی بر دم این کژدم</p></div>
<div class="m2"><p>دست شفقت چه کشی بر سر این اژدر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نگردد دگر آزردهٔ این پیکان</p></div>
<div class="m2"><p>برنخیزد دگر افتادهٔ این خنجر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در شیطان در ننگست، بر آن منشین</p></div>
<div class="m2"><p>ره عصیان ره مرگست، بر آن مگذر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشیانها به نمی‌ریخته این باران</p></div>
<div class="m2"><p>خانمانها به دمی سوخته این اخگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسیای تو شد افلاک و همی ترسم</p></div>
<div class="m2"><p>که ز گشتنش تو چون سرمه شوی آخر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میروی مست ز بیغوله و می‌آید</p></div>
<div class="m2"><p>با تو این دزد فریبندهٔ غارتگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سبک آنمرغ که ننشست بدین پستی</p></div>
<div class="m2"><p>خنک آن دیده که نغنود درین بستر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شو و بر طوطی جان شکر عرفان ده</p></div>
<div class="m2"><p>ورنه بر پرد و گردد تبه این شکر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی خبر میرود این شبرو بی پروا</p></div>
<div class="m2"><p>ناگهان میکشد این گیتی دون پرور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هوشیاری نبود در پی این مستی</p></div>
<div class="m2"><p>جهد کن تا نخوری باده از این ساغر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو چنین بیخود و فکر تو چنین باطل</p></div>
<div class="m2"><p>کور را کور نشد هیچگهی رهبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چند چون پشه ز هر دست قفا خوردن</p></div>
<div class="m2"><p>چند چون مور بهر پای فشاندن سر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همچو طاوس بگلزار حقیقت شو</p></div>
<div class="m2"><p>همچو سیمرغ سوی قاف ارادت پر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کشتهٔ حرص نیاورد بر تقوی</p></div>
<div class="m2"><p>لشکر جهل نشد بهر کسی لشکر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چند با اهرمن تیره‌دلی همره</p></div>
<div class="m2"><p>نفسی نیز ره صدق و صفا بسپر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مردم پاک شو، آنگاه بپاکان بین</p></div>
<div class="m2"><p>دیده حق بین کن و آنگاه بحق بنگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چشم را به ز حقیقت نبود پرتو</p></div>
<div class="m2"><p>روح را به ز فضیلت نبود زیور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخن از علم سماوات چه میرانی</p></div>
<div class="m2"><p>ایکه نشناخته‌ای باختر از خاور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر که آزار روا داشت، شد آزرده</p></div>
<div class="m2"><p>هر که چه کند در افتاد بچاه اندر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر نخواهی که رسد بر دلت آزاری</p></div>
<div class="m2"><p>بر دل خلق مزن بی سببی نشتر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مطلب روزی ننهاده که با کوشش</p></div>
<div class="m2"><p>نخوری قسمت کس، گر شوی اسکندر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بهر گلزار در آتش مفکن خود را</p></div>
<div class="m2"><p>که گلستان نشود بر همه کس آذر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از نکو خصلتی و بد گهری زینسان</p></div>
<div class="m2"><p>نخل پر میوه وناچیز بود عرعر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو هم ای شاخ، بری آر که خوشتر شد</p></div>
<div class="m2"><p>ز دو صد سرو، یکی شاخک بار آور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چه شدی بستهٔ این محبس بی روزن</p></div>
<div class="m2"><p>چه شدی ساکن این کنگرهٔ بی در</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سر خود گیر و از این دام گریزان شو</p></div>
<div class="m2"><p>دل خود جوی و ازین مرحله بیرون بر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نسزد تشنه همی عمر بسر بردن</p></div>
<div class="m2"><p>بامیدی که نمک زار شود کوثر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>طلب ملک سلیمان مکن از دیوان</p></div>
<div class="m2"><p>که چو طفلت بفریبند به انگشتر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زنگ خودبینی از آئینهٔ دل بزدا</p></div>
<div class="m2"><p>گر آلودگی از چهرهٔ جان بستر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ایکه پوئی ره امید شب تیره</p></div>
<div class="m2"><p>باش چون رهروی، آگاه ز جوی و جر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو رود غیبت و هنگام حضور آید</p></div>
<div class="m2"><p>تو چه داری که توان برد بدان محضر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سود و سرمایه بیک بار تبه کردی</p></div>
<div class="m2"><p>نشدی باز هم آگاه ز نفع و ضر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو تو خود صاعقهٔ خرمن خود گشتی</p></div>
<div class="m2"><p>چه همی نالی ازین تودهٔ خاکستر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نبرد هیچ بغیر از سیهی با خود</p></div>
<div class="m2"><p>هر که زانکشت فروشان طلبد عنبر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بید خرما و تبر خون ندهد میوه</p></div>
<div class="m2"><p>دیو طه و تبارک نکند از بر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خواجه آنست که آزاده بود، پروین</p></div>
<div class="m2"><p>بانو آنست که باشد هنرش زیور</p></div></div>