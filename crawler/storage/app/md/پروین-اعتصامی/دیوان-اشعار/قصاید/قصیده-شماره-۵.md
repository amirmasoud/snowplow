---
title: >-
    قصیدهٔ شمارهٔ ۵
---
# قصیدهٔ شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ای کنده سیل فتنه ز بنیادت</p></div>
<div class="m2"><p>وی داده باد حادثه بر بادت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دام روزگار چرا چونان</p></div>
<div class="m2"><p>شد پایبند، خاطر آزادت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنها نه خفتن است و تن آسانی</p></div>
<div class="m2"><p>مقصود ز آفرینش و ایجادت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس تو گمره است و همی ترسم</p></div>
<div class="m2"><p>گمره شوی، چو او کند ارشادت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل خسرو تن است، چو ویران شد</p></div>
<div class="m2"><p>ویرانه‌ای چسان کند آبادت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غافل بزیر گنبد فیروزه</p></div>
<div class="m2"><p>بگذشت سال عمر ز هفتادت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس روزگار رفت به پیروزی</p></div>
<div class="m2"><p>با تیرماه و بهمن و خردادت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر هفته و مهی که به پیش آمد</p></div>
<div class="m2"><p>بر پیشباز مرگ فرستادت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داری سفر به پیش و همی بینم</p></div>
<div class="m2"><p>بی رهنما و راحله و زادت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرد آرزو پرستی و خود بینی</p></div>
<div class="m2"><p>بیگانه از خدای، چو شدادت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا از جهان سفله نه‌ای فارغ</p></div>
<div class="m2"><p>هرگز نخواند اهل خرد رادت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این کور دل عجوزهٔ بی شفقت</p></div>
<div class="m2"><p>چون طعمه بهر گرگ اجل زادت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روزیت دوست گشت و شبی دشمن</p></div>
<div class="m2"><p>گاهی نژند کرد و گهی شادت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای بس ره امید که بربستت</p></div>
<div class="m2"><p>ای بس در فریب که بگشادت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هستی تو چون کبوتر کی مسکین</p></div>
<div class="m2"><p>بازی چنین قوی شده صیادت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پروین، نهفته دیویت آموزد</p></div>
<div class="m2"><p>دیو زمانه، گر شود استادت</p></div></div>