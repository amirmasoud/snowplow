---
title: >-
    قصیدهٔ شمارهٔ ۳۹
---
# قصیدهٔ شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>ای شده سوختهٔ آتش نفسانی</p></div>
<div class="m2"><p>سالها کرده تباهی و هوسرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دزد ایام گرفتست گریبانت</p></div>
<div class="m2"><p>بس کن ای بیخودی و سربگریبانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح رحمت نگشاید همه تاریکی</p></div>
<div class="m2"><p>یوسف مصر نگردد همه زندانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه پر خار مغیلان وتو بی موزه</p></div>
<div class="m2"><p>سفره بی توشه و شب تیره و بارانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بخود دیده چو شداد، خدابین شو</p></div>
<div class="m2"><p>جز خدا را نسزد رتبت یزدانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو سلیمان شدن آموزی اگر، دیوان</p></div>
<div class="m2"><p>نتوانند زدن لاف سلیمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بکی کودنی و مستی و خودرائی</p></div>
<div class="m2"><p>تا بکی کودکی و بازی و نادانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو درین خاک سیه زر دل افروزی</p></div>
<div class="m2"><p>تو درین دشت و چمن لالهٔ نعمانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش دیوان مبر اندوه دل و مگری</p></div>
<div class="m2"><p>که بخندند چو بینند که گریانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل آموخت بهر کارگری کاری</p></div>
<div class="m2"><p>او چو استاد شد و ما چو دبستانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خود نمیدانی و از خلق نمیپرسی</p></div>
<div class="m2"><p>فارغ از مشکل و بیگانه ز آسانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که برد بار تو امروز که مسکینی</p></div>
<div class="m2"><p>که ترا نان دهد امروز که بی نانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست تقوی بگشا، پای هوی بربند</p></div>
<div class="m2"><p>تا ببینند که از کرده پشیمانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گهریهای حقیقت گهر خود را</p></div>
<div class="m2"><p>نفروشند بدین هیچی و ارزانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیدهٔ خویش نهان بین کن و بین آنگه</p></div>
<div class="m2"><p>دامهائی که نهادند به پنهانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حیوان گشتن و تن پروری آسانست</p></div>
<div class="m2"><p>روح پرورده کن از لقمهٔ روحانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با خرد جان خود آن به که بیارائی</p></div>
<div class="m2"><p>با هنر عیب خود آن به که بپوشانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با خبر باش که بی مصلحت و قصدی</p></div>
<div class="m2"><p>آدمی را نبرد دیو به مهمانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نفس جو داد که گندم ز تو بستاند</p></div>
<div class="m2"><p>به که هرگز ندهی رشوت و نستانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دشمنانند ترا زرق و فساد، اما</p></div>
<div class="m2"><p>به گمان تو که در حلقهٔ یارانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا زبون طمعی هیچ نمیارزی</p></div>
<div class="m2"><p>تا اسیر هوسی هیچ نمیدانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خوشتر از دولت جم، دولت درویشی</p></div>
<div class="m2"><p>بهتر از قصر شهی، کلبهٔ دهقانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خانگی باشد اگر دزد، بصد تدبیر</p></div>
<div class="m2"><p>نتوان کرد از آن خانه نگهبانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برو از ماه فراگیر دل افروزی</p></div>
<div class="m2"><p>برو از مهره بیاموز درخشانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیش زاغان مفکن گوهر یکدانه</p></div>
<div class="m2"><p>پیش خربنده مبر لعل بدخشانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر که همصحبت تو دیو نبودستی</p></div>
<div class="m2"><p>ز که آموختی این شیوهٔ شیطانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صفتی جوی که گویند نکوکاری</p></div>
<div class="m2"><p>سخنی گوی که گویند سخندانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگذر از بحر و ز فرعون هوی مندیش</p></div>
<div class="m2"><p>دهر دریا و تو چون موسی عمرانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اژدهای طمع و گرگ طبیعت را</p></div>
<div class="m2"><p>گر بترسی، نتوانی که بترسانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بفکن این لاشهٔ خونین، تو نه ناهاری</p></div>
<div class="m2"><p>برکن این جامهٔ چرکین، تو نه عریانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر توانی، به دلی توش و توانی ده</p></div>
<div class="m2"><p>که مبادا رسد آنروز که نتوانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خون دل چند خوری در دل سنگ، ای لعل</p></div>
<div class="m2"><p>مشتریهاست برای گهر کانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر چه یونان وطن بس حکما بودست</p></div>
<div class="m2"><p>نیست آگاه ز حکمت همه یونانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کلبه‌ای را که نه فرشی و نه کالائیست</p></div>
<div class="m2"><p>بر درش می‌نبود حاجت دربانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زنده با گفتن پندم نتوانی کرد</p></div>
<div class="m2"><p>که تو خود نیز چو من کشتهٔ عصیانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کینه می‌ورزی و در دائرهٔ صدقی</p></div>
<div class="m2"><p>رهزنی میکنی و در ره ایمانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا کی این خام فریبی، تو نه یاجوجی</p></div>
<div class="m2"><p>چند بلعیدن مردم، تو نه ثعبانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مقصد عافیت از گمشدگان پرسی</p></div>
<div class="m2"><p>رو که بر گمشدگان خویش تو برهانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گوسفندان تو ایمن ز تو چون باشند</p></div>
<div class="m2"><p>که شبانگاه تو در مکمن گرگانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گاه از رنگرزان خم تزویری</p></div>
<div class="m2"><p>گاه بر پشت خر وسوسه پالانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تشنه خون خورد و تو خودبین به لب جوئی</p></div>
<div class="m2"><p>گرسنه مرد و تو گمره بسر خوانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دود آهست بنائی که تو میسازی</p></div>
<div class="m2"><p>چاه راهست کتابی که تو میخوانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دیده بگشای، نه اینست جهان بینی</p></div>
<div class="m2"><p>کفر بس کن، نه چنین است مسلمانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو نهالیست روان و تو کشاورزی</p></div>
<div class="m2"><p>چو جهانیست وجود و تو جهانبانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو چراغی، ز چه رو همنفس بادی</p></div>
<div class="m2"><p>تو امیدی، ز چه همخانهٔ حرمانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تو درین بزم، چو افروخته قندیلی</p></div>
<div class="m2"><p>تو درین قصر، چو آراسته ایوانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو ز خود رفته و وادی شده پر آفت</p></div>
<div class="m2"><p>تو بخواب اندر و کشتی شده طوفانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تو رسیدن نتوانی بسبکباران</p></div>
<div class="m2"><p>که برفتار نه مانندهٔ ایشانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فکر فردا نتوانی که کنی دیگر</p></div>
<div class="m2"><p>مگر امروز که در کشور امکانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>عاقبت کشتهٔ شمشیر مه و سالی</p></div>
<div class="m2"><p>آخر کار شکار دی و آبانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هوشیاری و شب و روز بمیخانه</p></div>
<div class="m2"><p>همدم درد کشان همسر مستانی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همچو برزیگر آفت زده محصولی</p></div>
<div class="m2"><p>همچو رزم آور و غارت شده خفتانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مار در لانه، ولی مور بافسونی</p></div>
<div class="m2"><p>گرد در خانه، ولی گرد بمیدانی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دل بیچاره و مسکین مخراش امروز</p></div>
<div class="m2"><p>رسد آنروز که بی ناخن و دندانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>داستانت کند این چرخ کهن، هر چند</p></div>
<div class="m2"><p>نامجوینده‌تر از رستم دستانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>روز بر مسند پاکیزهٔ انصافی</p></div>
<div class="m2"><p>شام در خلوت آلودهٔ دیوانی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دست مسکین نگرفتی و توانائی</p></div>
<div class="m2"><p>میوه‌ای گرد نکردی و به بستانی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ظاهرست این که بد افتی چو شوی بدخواه</p></div>
<div class="m2"><p>روشنست این که برنجی چو برنجانی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دیو بسیار بود در ره دل، پروین</p></div>
<div class="m2"><p>کوش تا سر ز ره راست نپیچانی</p></div></div>