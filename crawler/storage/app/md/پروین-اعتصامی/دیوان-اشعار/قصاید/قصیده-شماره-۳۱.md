---
title: >-
    قصیدهٔ شمارهٔ ۳۱
---
# قصیدهٔ شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>حاصل عمر تو افسوس شد و حرمان</p></div>
<div class="m2"><p>عیب خود را مکن ایدوست ز خود پنهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت ضایع نکند هیچ هنرپیشه</p></div>
<div class="m2"><p>جفت باطل نشود هیچ حقیقت دان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچگه نیست ره و رسم خردمندی</p></div>
<div class="m2"><p>گرسنه خفتن و در سفره نهفتن نان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهر گرگیست گرسنه، رخ از او برگیر</p></div>
<div class="m2"><p>چرخ دیویست سیه دل، دل ازو بستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پا بر این رهگذر سخت گرانتر نه</p></div>
<div class="m2"><p>اسب زین دشت خطرناک سبکتر ران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موج و طوفان و نهنگست درین دریا</p></div>
<div class="m2"><p>باید اندیشه کند زین همه کشتیبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ آگاه نیاسود درین ظلمت</p></div>
<div class="m2"><p>هیچ دیوانه نشد بستهٔ این زندان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای بسا خرمن امید که در یکدم</p></div>
<div class="m2"><p>کرد خاکسترش این صاعقهٔ سوزان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تکیه بر اختر فیروز مکن چندین</p></div>
<div class="m2"><p>ایمن از فتنهٔ ایام مشو چندان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی تو بس خواهد بودن دی و فروردین</p></div>
<div class="m2"><p>بی تو بس خواهد گشتن فلک گردان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شود جان، به چه دردیت رسد پیکر</p></div>
<div class="m2"><p>چو رود سر به چه کاریت خورد سامان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو خود ار با نگهی پاک بخود بینی</p></div>
<div class="m2"><p>یابی آن گنج که جوئیش درین ویران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو کتابیست ریا، بی ورق و بی خط</p></div>
<div class="m2"><p>چو درختیست هوی، بی بن و بی اغصان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هیچ عاقل ننهد بر کف دست آتش</p></div>
<div class="m2"><p>هیچ هشیار نساید بزبان سوهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا تو چون گوی درین کوی بسر گردی</p></div>
<div class="m2"><p>بایدت خیره جفا دیدن از این چوگان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گشت هنگام درو، کشت چه کردی هین</p></div>
<div class="m2"><p>آمد آوای جرس، توشه چه داری هان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رهرو گمشده و راهزنان در پیش</p></div>
<div class="m2"><p>شب تار و خر لنگ و ره بی پایان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بکش این نفس حقیقت کش خود بین را</p></div>
<div class="m2"><p>این نه جرمی است که خواهند ز تو تاوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به یکی دل نتوان کار تن و جان کرد</p></div>
<div class="m2"><p>به یکی دست دو طنبور زدن، نتوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خرد استاد و تو شاگرد و جهان مکتب</p></div>
<div class="m2"><p>چه رسیدت که چنین کودنی و نادان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو شدی کاهل و از کاربری گشتی</p></div>
<div class="m2"><p>نه زمستان گنهی داشت نه تابستان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بوستان بود وجود تو گه خلقت</p></div>
<div class="m2"><p>تخم کردار بدش کرد چو شورستان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو مپندار که عناب دهد علقم</p></div>
<div class="m2"><p>تو مپندار که عزت رسد از خذلان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>منشین با همه کس، کاز پی بد کاری</p></div>
<div class="m2"><p>آدمی روی توانند شدن دیوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گشت ابلیس چو غواص به بحر دل</p></div>
<div class="m2"><p>ماند بر جا شبه و رفت در غلطان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پویه آسوده نکردست کسی زین ره</p></div>
<div class="m2"><p>لقمه بی سنگ نخوردست کسی زین خوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر شوی باد بگردش نرسی هرگز</p></div>
<div class="m2"><p>طائر عمر چو از دام تو شد پران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دی شد امروز، بخیره مخور اندوهش</p></div>
<div class="m2"><p>کز پس مرده خردمند نکرد افغان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خر تو میبرد این غول بیابانی</p></div>
<div class="m2"><p>آخر کار تو میمانی و این پالان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شبرو دهر نگردد همه در یک راه</p></div>
<div class="m2"><p>گشتن چرخ نباشد همه بر یکسان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کامها تلخ شد از تلخی این حلوا</p></div>
<div class="m2"><p>عهدها سست شد از سستی این پیمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آنکه نشناخته از هم الف و با را</p></div>
<div class="m2"><p>زو چه داری طمع معرفت قرآن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پرتوی ده، تو نه‌ای دیو درون تیره</p></div>
<div class="m2"><p>کوششی کن، تو نه‌ای کالبد بی جان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به تو هرچ آن رسد از تنگی و مسکینی</p></div>
<div class="m2"><p>همه از تست، نه از کجروی دوران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نام جوئی؟ چو ملک باش نکو کردار</p></div>
<div class="m2"><p>قدر خواهی؟ چو فلک باش بلند ارکان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>برو ای قطره در آغوش صدف بنشین</p></div>
<div class="m2"><p>روی بنمای چو گشتی گهر رخشان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یاری از علم و هنر خواه، چو درمانی</p></div>
<div class="m2"><p>نه فلان با تو کند یاری و نه بهمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دانش اندوز، چه حاصل بود از دعوی</p></div>
<div class="m2"><p>معنی آموز، چه سودی رسد از عنوان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بستهٔ شوق بود از دو جهان آزاد</p></div>
<div class="m2"><p>کشتهٔ عشق بود زندهٔ جاویدان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همه زارع نبرد وقت درو خرمن</p></div>
<div class="m2"><p>همه غواص نیارد گهر از عمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زیب یابد سر و تن از ادب و دانش</p></div>
<div class="m2"><p>زنده گردد دل و جان از هنر و عرفان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عقل گنجست، نباید که برد دزدش</p></div>
<div class="m2"><p>علم نورست، نباید که شود پنهان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هستی از بهر تن آسانی اگر بودی</p></div>
<div class="m2"><p>چه بدی برتری آدمی از حیوان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر نبودی سخن طیبت و رنگ و بو</p></div>
<div class="m2"><p>خسک و خشک بدی همچو گل و ریحان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جامهٔ جان تو زیور علم آراست</p></div>
<div class="m2"><p>چه غم ار پیرهن تنت بود خلقان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سحر باز است فلک، لیک چه خواهد کرد</p></div>
<div class="m2"><p>سحر با آنکه بود چون پسر عمران</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو شدی نیک، چه پروات ز بد روزی</p></div>
<div class="m2"><p>چو شدی نوح، چه اندیشه‌ات از طوفان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برو از تیه بلا گمشده‌ای دریاب</p></div>
<div class="m2"><p>بزن آبی و ز جانی شرری بنشان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به یکی لقمه، دل گرسنه‌ای بنواز</p></div>
<div class="m2"><p>به یکی جامه، تن برهنه‌ای پوشان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بینوا مرد بحسرت ز غم نانی</p></div>
<div class="m2"><p>خواجه دلکوفته گشت از برهٔ بریان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سوخت گر در دل شب خرمن پروانه</p></div>
<div class="m2"><p>شمع هم تا بسحرگاه بود مهمان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بی هنر گر چه بتن دیبهٔ چین پوشد</p></div>
<div class="m2"><p>به پشیزی نخرندش چو شود عریان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همه یاران تو از چستی و چالاکی</p></div>
<div class="m2"><p>پرنیان باف و تو در کارگه کتان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>آنکه صراف گهر شد ننهد هرگز</p></div>
<div class="m2"><p>سنگ را با در شهوار بیک میزان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز چه، ای شاخک نورس، ندهی باری</p></div>
<div class="m2"><p>بامید ثمری کشت ترا دهقان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هیچ، آزاده نشد بندهٔ تن، پروین</p></div>
<div class="m2"><p>هیچ پاکیزه نیالود دل و دامان</p></div></div>