---
title: >-
    قصیدهٔ شمارهٔ ۲۶
---
# قصیدهٔ شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>در خانه شحنه خفته و دزدان بکوی و بام</p></div>
<div class="m2"><p>ره دیو لاخ و قافله بی مقصد و مرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عاقلی، چرا بردت توسن هوی</p></div>
<div class="m2"><p>ور مردمی، چگونه شدستی به دیو رام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس را نماند از تک این خنگ بادپای</p></div>
<div class="m2"><p>پا در رکاب و سر به تن و دست در لگام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خانه گر که هیچ نداری شگفت نیست</p></div>
<div class="m2"><p>کالات میبرند و تو خوابیده‌ای مدام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دزد آنچه برده باز نیاورده هیچگاه</p></div>
<div class="m2"><p>هرگز به اهرمن مده ایمان خویش وام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میکاهدت سپهر، چنین بی خبر مخسب</p></div>
<div class="m2"><p>میسوزدت زمانه، بدینسان مباش خام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از کار جان چرا زنی ای تیره روز تن</p></div>
<div class="m2"><p>در راه نان چرا نهی ای بی تمیز نام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بهر صید خاطر ناآزمودگان</p></div>
<div class="m2"><p>صیاد روزگار بهر سو نهاده دام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس سقف شد خراب و نگشت آسمان خراب</p></div>
<div class="m2"><p>بس عمر شد تمام و نشد روز و شب تمام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منشین گرسنه کاین هوس خام پختن است</p></div>
<div class="m2"><p>جوشیده سالها و نپختست این طعام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگشای گر که زنده‌دلی وقت پویه چشم</p></div>
<div class="m2"><p>بردار گر که کارگری بهر کار گام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در تیرگی چو شب پره تا چند میپری</p></div>
<div class="m2"><p>بشناس فرق روشنی ای دوست از ظلام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای زورمند، روز ضعیفان سیه مکن</p></div>
<div class="m2"><p>خونابه میچکد همی از دست انتقام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فتوی دهی بغصب حق پیرزن ولیک</p></div>
<div class="m2"><p>بی روزه هیچ روز نباشی مه صیام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وقت سخن مترس و بگو آنچه گفتنی است</p></div>
<div class="m2"><p>شمشیر روز معرکه زشت است در نیام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درد از طبیب خویش نهفتی، از آن سبب</p></div>
<div class="m2"><p>این زخم کهنه دیر پذیرفت التیام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از بهر حفظ گله، شبان چون بخواب رفت</p></div>
<div class="m2"><p>سگ باید ای فقیه، نه آهوی خوشخرام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چاهت چراست جای، گرت میل برتریست</p></div>
<div class="m2"><p>حرصت چراست خواجه، اگر نیستی غلام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چندی ز بار گاه سلیمان برون مرو</p></div>
<div class="m2"><p>تا دیو هیچگه نفرستد تو را پیام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عمریست رهنوردی و چون کودکان هنوز</p></div>
<div class="m2"><p>آگه نه‌ای که چاه کدام است و ره کدام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پروین، شراب معرفت از جام علم نوش</p></div>
<div class="m2"><p>ترسم که دیر گردد و خالی کنند جام</p></div></div>