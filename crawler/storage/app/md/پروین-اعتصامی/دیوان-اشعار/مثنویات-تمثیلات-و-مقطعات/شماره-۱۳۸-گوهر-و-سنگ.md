---
title: >-
    شمارهٔ  ۱۳۸ - گوهر و سنگ
---
# شمارهٔ  ۱۳۸ - گوهر و سنگ

<div class="b" id="bn1"><div class="m1"><p>شنیدستم که اندر معدنی تنگ</p></div>
<div class="m2"><p>سخن گفتند با هم، گوهر و سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین پرسید سنگ از لعل رخشان</p></div>
<div class="m2"><p>که از تاب که شد، چهرت فروزان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدین پاکیزه‌روئی، از کجائی</p></div>
<div class="m2"><p>که دادت آب و رنگ و روشنائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین تاریک جا، جز تیرگی نیست</p></div>
<div class="m2"><p>بتاریکی درون، این روشنی چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر تاب تو، بس رخشندگیهاست</p></div>
<div class="m2"><p>در این یک قطره، آب زندگیهاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بمعدن، من بسی امید راندم</p></div>
<div class="m2"><p>تو گر صد سال، من صد قرن ماندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا آن پستی دیرینه بر جاست</p></div>
<div class="m2"><p>فروغ پاکی، از چهر تو پیداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدین روشن دلی، خورشید تابان</p></div>
<div class="m2"><p>چرا با من تباهی کرد زینسان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا از تابش هر روزه، بگداخت</p></div>
<div class="m2"><p>ترا آخر، متاع گوهری ساخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر عدل است، کار چرخ گردان</p></div>
<div class="m2"><p>چرا من سنگم و تو لعل رخشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه ما را دایهٔ ایام پرورد</p></div>
<div class="m2"><p>چرا با من چنین، با تو چنان کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا نقصان، تو را افزونی آموخت</p></div>
<div class="m2"><p>ترا افروخت رخسار و مرا سوخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترا، در هر کناری خواستاریست</p></div>
<div class="m2"><p>مرا، سرکوبی از هر رهگذریست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ترا، هم رنگ و هم ار زندگی هست</p></div>
<div class="m2"><p>مرا زین هر دو چیزی نیست در دست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترا بر افسر شاهان نشانند</p></div>
<div class="m2"><p>مرا هرگز نپرسند و ندانند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود هر گوهری را با تو پیوند</p></div>
<div class="m2"><p>گه انگشتر شوی، گاهی گلوبند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من، اینسان واژگون طالع، تو فیروز</p></div>
<div class="m2"><p>تو زینسان دلفروز و من بدین روز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بنرمی گفت او را گوهر ناب</p></div>
<div class="m2"><p>جوابی خوبتر از در خوشاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کزان معنی مرا گرم است بازار</p></div>
<div class="m2"><p>که دیدم گرمی خورشید، بسیار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از آنرو، چهره‌ام را سرخ شد رنگ</p></div>
<div class="m2"><p>که بس خونابه خوردم در دل سنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از آن ره، بخت با من کرد یاری</p></div>
<div class="m2"><p>که در سختی نمودم استواری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به اختر، زنگی شب راز میگفت</p></div>
<div class="m2"><p>سپهر، آن راز با من باز میگفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ثریا کرد با من تیغ‌بازی</p></div>
<div class="m2"><p>عطارد تا سحر، افسانه‌سازی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زحل، با آنهمه خونخواری و خشم</p></div>
<div class="m2"><p>مرا میدید و خون میریخت از چشم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فلک، بر نیت من خنده میکرد</p></div>
<div class="m2"><p>مرا زین آرزو شرمنده می‌کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سهیلم رنجها میداد پنهان</p></div>
<div class="m2"><p>بفکرم رشکها میبرد کیهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نشستی ژاله‌ای، هر گه بکهسار</p></div>
<div class="m2"><p>بدوش من گرانتر میشدی بار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنانم میفشردی خاره و سنگ</p></div>
<div class="m2"><p>که خونم موج میزد در دل تنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه پیدا بود روز اینجا، نه روزن</p></div>
<div class="m2"><p>نه راه و رخنه‌ای بر کوه و برزن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدان درماندگی بودم گرفتار</p></div>
<div class="m2"><p>که باشد نقطه اندر حصن پرگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گهی گیتی، ز برفم جامه پوشید</p></div>
<div class="m2"><p>گهی سیلم، بگوش اندر خروشید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زبونیها ز خاک و آب دیدم</p></div>
<div class="m2"><p>ز مهر و ماه، منت‌ها کشیدم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جدی هر شب، بفکر بازئی چند</p></div>
<div class="m2"><p>بمن میکرد چشم اندازئی چند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ثوابت، قصه‌ها کردند تفسیر</p></div>
<div class="m2"><p>کواکب برجها دادند تغییر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دگرگون گشت بس روز و مه و سال</p></div>
<div class="m2"><p>مرا جاوید یکسان بود احوال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر چه کار بر من بود دشوار</p></div>
<div class="m2"><p>بخود دشوار می‌نشمردمی کار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه دیدم ذره‌ای از روشنائی</p></div>
<div class="m2"><p>نه با یک ذره، کردم آشنائی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه چشمم بود جز با تیرگی رام</p></div>
<div class="m2"><p>نه فرق صبح میدانستم از شام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بسی پاکان شدند آلوده دامن</p></div>
<div class="m2"><p>بسی برزیگران را سوخت خرمن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بسی برگشت، راه و رسم گردون</p></div>
<div class="m2"><p>که پا نگذاشتیم ز اندازه بیرون</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو دیدندم چنان در خط تسلیم</p></div>
<div class="m2"><p>مرا بس نکته‌ها کردند تعلیم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بگفتندم ز هر رمزی بیانی</p></div>
<div class="m2"><p>نمودندم ز هر نامی نشانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ببخشیدند چون تابی تمامم</p></div>
<div class="m2"><p>بدخشی لعل بنهادند نامم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرا در دل، نهفته پرتوی بود</p></div>
<div class="m2"><p>فروزان مهر، آن پرتو بیفزود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کمی در اصل من میبود پاکی</p></div>
<div class="m2"><p>شد آن پاکی، در آخر تابناکی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو طبعم اقتضای برتری داشت</p></div>
<div class="m2"><p>مرا آن برتری، آخر برافراشت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نه تاب و ارزش من، رایگانی است</p></div>
<div class="m2"><p>سزای رنج قرنی زندگانی است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نه هر پاکیزه روئی، پاکزاد است</p></div>
<div class="m2"><p>که نسل پاک، ز اصل پاک زاد است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نه هر کوهی، بدامن داشت معدن</p></div>
<div class="m2"><p>نه هر کان نیز دارد لعل روشن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یکی غواص، درجی گران بود</p></div>
<div class="m2"><p>پر از مشتی شبه دیدش، چو بگشود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بگو این نکته با گوهر فروشان</p></div>
<div class="m2"><p>که خون خورد و گهر شد سنگ در کان</p></div></div>