---
title: >-
    شمارهٔ  ۳ - )
---
# شمارهٔ  ۳ - )

<div class="b" id="bn1"><div class="m1"><p>ای خوشا سودای دل از دیده پنهان داشتن</p></div>
<div class="m2"><p>مبحث تحقیق را در دفتر جان داشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیبه‌ها بی کارگاه و دوک و جولا بافتن</p></div>
<div class="m2"><p>گنجها بی پاسبان و بی نگهبان داشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده فرمان خود کردن همه آفاق را</p></div>
<div class="m2"><p>دیو بستن، قدرت دست سلیمان داشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ره ویران دل، اقلیم دانش ساختن</p></div>
<div class="m2"><p>در ره سیل قضا، بنیاد و بنیان داشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده را دریا نمودن، مردمک را غوصگر</p></div>
<div class="m2"><p>اشک را مانند مروارید غلطان داشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تکلف دور گشتن، ساده و خوش زیستن</p></div>
<div class="m2"><p>ملک دهقانی خریدن، کار دهقان داشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رنجبر بودن، ولی در کشتزار خویشتن</p></div>
<div class="m2"><p>وقت حاصل خرمن خود را بدامان داشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز را با کشت و زرع و شخم آوردن بشب</p></div>
<div class="m2"><p>شامگاهان در تنور خویشتن نان داشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سربلندی خواستن در عین پستی، ذره‌وار</p></div>
<div class="m2"><p>آرزوی صحبت خورشید رخشان داشتن</p></div></div>