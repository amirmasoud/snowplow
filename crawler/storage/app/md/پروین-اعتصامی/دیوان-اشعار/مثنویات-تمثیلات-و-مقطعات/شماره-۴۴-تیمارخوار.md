---
title: >-
    شمارهٔ  ۴۴ - تیمارخوار
---
# شمارهٔ  ۴۴ - تیمارخوار

<div class="b" id="bn1"><div class="m1"><p>گفت ماهیخوار با ماهی ز دور</p></div>
<div class="m2"><p>که چه میخواهی ازین دریای شور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خردی و ضعف تو از رنج شناست</p></div>
<div class="m2"><p>این نه راه زندگی، راه فناست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندرین آب گل آلود، ای عجب</p></div>
<div class="m2"><p>تا بکی سرگشته باشی روز و شب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت آن آمد که تدبیری کنی</p></div>
<div class="m2"><p>در سرای عمر تعمیری کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما بساط از فتنه ایمن کرده‌ایم</p></div>
<div class="m2"><p>صد هزاران شمع، روشن کرده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچگه ما را غم صیاد نیست</p></div>
<div class="m2"><p>انده طوفان و سیل و باد نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بیائی در جوار ما دمی</p></div>
<div class="m2"><p>بینی از اندیشه خالی عالمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیمروزی گر شوی مهمان ما</p></div>
<div class="m2"><p>غرق گردی در یم احسان ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه تپیدن هست و نه تاب و تبی</p></div>
<div class="m2"><p>نه غم صبحی، نه پروای شبی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دامها بینم براه تو نهان</p></div>
<div class="m2"><p>رفتنت باشد همان، مردن همان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تابه‌ها و شعله‌ها در انتظار</p></div>
<div class="m2"><p>که تو یکروزی بسوزی در شرار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نمی‌خواهی در آتش سوختن</p></div>
<div class="m2"><p>بایدت اندرز ما آموختن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر سوی خشکی کنی با ما سفر</p></div>
<div class="m2"><p>بر نگردی جانب دریا دگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر ببینی آن هوا و آن نسیم</p></div>
<div class="m2"><p>بشکنی این عهد و پیوند قدیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت از ما با تو هر کس گشت دوست</p></div>
<div class="m2"><p>تو بدست دوستی، کندیش پوست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر که هر مطلوب را طالب شویم</p></div>
<div class="m2"><p>با چه نیرو بر هوی غالب شویم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چشمهٔ نور است این آب سیاه</p></div>
<div class="m2"><p>تو نکردی چون خریداران نگاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خانهٔ هر کس برای او سزاست</p></div>
<div class="m2"><p>بهر ماهی، خوشتر از دریا کجاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر بجوی و برکه لای و گل خوریم</p></div>
<div class="m2"><p>به که از جور تو خون دل خوریم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جنس ما را نسبتی با خاک نیست</p></div>
<div class="m2"><p>پیش ماهی، سیل وحشتناک نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آب و رنگ ما ز آب افزوده‌اند</p></div>
<div class="m2"><p>خلقت ما را چنین فرموده‌اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر ز سطح آب بالاتر شویم</p></div>
<div class="m2"><p>زاتش بیداد، خاکستر شویم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قرنها گشتیم اینجا فوج فوج</p></div>
<div class="m2"><p>می نترسیدیم از طوفان و موج</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لیک از بدخواه، ما را ترسهاست</p></div>
<div class="m2"><p>ترس جان، آموزگار درسهاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بسکه بدکار و جفا جو دیده‌ام</p></div>
<div class="m2"><p>از بدیهای جهان ترسیده‌ایم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بره‌گان را ترس میباید ز گرگ</p></div>
<div class="m2"><p>گردد از این درس، هر خردی بزرگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با عدوی خود، مرا خویشی نبود</p></div>
<div class="m2"><p>دعوت تو جز بداندیشی نبود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا بود پائی، چرا مانم ز راه</p></div>
<div class="m2"><p>تا بود چشمی، چرا افتم به چاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر بچنگ دام ایام اوفتم</p></div>
<div class="m2"><p>به که با دست تو در دام اوفتم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر بدیگ اندر، بسوزم زار زار</p></div>
<div class="m2"><p>بهتر است آن شعله زین گرد و غبار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو برای صید ماهی آمدی</p></div>
<div class="m2"><p>کی برای خیر خواهی آمدی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از تو نستانم نوا و برگ را</p></div>
<div class="m2"><p>گر بچشم خویش بینم مرگ را</p></div></div>