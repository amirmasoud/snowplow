---
title: >-
    شمارهٔ  ۱۵۵ - نکوهش بیجا
---
# شمارهٔ  ۱۵۵ - نکوهش بیجا

<div class="b" id="bn1"><div class="m1"><p>سیر، یک روز طعنه زد به پیاز</p></div>
<div class="m2"><p>که تو مسکین چقدر بد بوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت، از عیب خویش بی‌خبری</p></div>
<div class="m2"><p>زان ره از خلق، عیب میجوئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتن از زشتروئی دگران</p></div>
<div class="m2"><p>نشود باعث نکوروئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو گمان میکنی که شاخ گلی</p></div>
<div class="m2"><p>بصف سرو و لاله میروئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا که همبوی مشک تاتاری</p></div>
<div class="m2"><p>یا ز ازهار باغ مینوئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خویشتن، بی سبب بزرگ مکن</p></div>
<div class="m2"><p>تو هم از ساکنان این کوئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ره ما، گر کج است و ناهموار</p></div>
<div class="m2"><p>تو خود، این ره چگونه میپوئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خود، آن به که نیکتر نگری</p></div>
<div class="m2"><p>اول، آن به که عیب خود گوئی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما زبونیم و شوخ جامه و پست</p></div>
<div class="m2"><p>تو چرا شوخ تن نمیشوئی</p></div></div>