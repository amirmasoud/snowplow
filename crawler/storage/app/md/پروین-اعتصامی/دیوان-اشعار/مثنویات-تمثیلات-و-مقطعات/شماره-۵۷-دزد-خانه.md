---
title: >-
    شمارهٔ  ۵۷ - دزد خانه
---
# شمارهٔ  ۵۷ - دزد خانه

<div class="b" id="bn1"><div class="m1"><p>حکایت کرد سرهنگی به کسری</p></div>
<div class="m2"><p>که دشمن را ز پشت قلعه راندیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فراریهای چابک را گرفتیم</p></div>
<div class="m2"><p>گرفتاران مسکین را رهاندیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خون کشتگان، شمشیر شستیم</p></div>
<div class="m2"><p>بر آتشهای کین، آبی فشاندیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پای مادران کندیم خلخال</p></div>
<div class="m2"><p>سرشک از دیدهٔ طفلان چکاندیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جام فتنه، هر تلخی چشیدیم</p></div>
<div class="m2"><p>همان شربت به بدخواهان چشاندیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفت این خصم را راندیم، اما</p></div>
<div class="m2"><p>یکی زو کینه جوتر، پیش خواندیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجا با دزد بیرونی درافتیم</p></div>
<div class="m2"><p>چو دزد خانه را بالا نشاندیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازین دشمن در افکندن چه حاصل</p></div>
<div class="m2"><p>چو عمری با عدوی نفس ماندیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز غفلت، زیر بار عجب رفتیم</p></div>
<div class="m2"><p>ز جهل، این بار را با خود کشاندیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نداده ابره را از آستر فرق</p></div>
<div class="m2"><p>قبای زندگانی را دراندیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین دفتر، بهر رمزی رسیدیم</p></div>
<div class="m2"><p>نوشتیم و به اهریمن رساندیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دویدیم استخوانی را ز دنبال</p></div>
<div class="m2"><p>سگ پندار را از پی دواندیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فسون دیو را از دل نهفتیم</p></div>
<div class="m2"><p>برای گرگ، آهو پروراندیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پلنگی جای کرد اندر چراگاه</p></div>
<div class="m2"><p>همانجا گلهٔ خود را چراندیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ندانستیم فرصت را بدل نیست</p></div>
<div class="m2"><p>ز دام، این مرغ وحشی را پراندیم</p></div></div>