---
title: >-
    شمارهٔ  ۱۰۷ - قدر هستی
---
# شمارهٔ  ۱۰۷ - قدر هستی

<div class="b" id="bn1"><div class="m1"><p>سرو خندید سحر، بر گل سرخ</p></div>
<div class="m2"><p>که صفای تو به جز یکدم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بیک پایه بمانم صد سال</p></div>
<div class="m2"><p>مرگ، با هستی من توام نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که آزاد و خوش و سرسبزم</p></div>
<div class="m2"><p>پشتم از بار حوادث، خم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دولت آنست که جاوید بود</p></div>
<div class="m2"><p>خانهٔ دولت تو، محکم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت، فکر کم و بسیار مکن</p></div>
<div class="m2"><p>سرنوشت همه کس، با هم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما بدین یکدم و یک لحظه خوشیم</p></div>
<div class="m2"><p>نیست یک گل، که دمی خرم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدر این یکدم و یک لحظه بدان</p></div>
<div class="m2"><p>تا تو اندیشه کنی، آنهم نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چونکه گلزار نخواهد ماندن</p></div>
<div class="m2"><p>گل اگر نیز نماند، غم نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه غم ار همدم من نیست کسی</p></div>
<div class="m2"><p>خوشتر از باد صبا، همدم نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمر گر یک دم و گر یک نفس است</p></div>
<div class="m2"><p>تا بکاریش توان زد، کم نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما بخندیم به هستی و به مرگ</p></div>
<div class="m2"><p>هیچگه چهرهٔ ما درهم نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آشکار است ستمکاری دهر</p></div>
<div class="m2"><p>زخم بس هست، ولی مرهم نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک ره ار داد، دو صد راه گرفت</p></div>
<div class="m2"><p>چه توان کرد، فلک حاتم نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو هم از پای در آئی ناچار</p></div>
<div class="m2"><p>آبت از کوثر و از زمزم نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باید آزاده کسی را خواندن</p></div>
<div class="m2"><p>که گرفتار، درین عالم نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گل چرا خوش ننشیند، دائم</p></div>
<div class="m2"><p>ماهتاب و چمن و شبنم نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یک نفس بودن و نابود شدن</p></div>
<div class="m2"><p>در خور این غم و این ماتم نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر چه خواندیم، نگشتیم آگه</p></div>
<div class="m2"><p>درس تقدیر، به جز مبهم نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شمع خردی که نسیمش بکشد</p></div>
<div class="m2"><p>شمع این پرتگه مظلم نیست</p></div></div>