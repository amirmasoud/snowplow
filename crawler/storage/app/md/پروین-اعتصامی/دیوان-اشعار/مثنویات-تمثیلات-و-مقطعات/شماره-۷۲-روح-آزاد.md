---
title: >-
    شمارهٔ  ۷۲ - روح آزاد
---
# شمارهٔ  ۷۲ - روح آزاد

<div class="b" id="bn1"><div class="m1"><p>تو چو زری، ای روان تابناک</p></div>
<div class="m2"><p>چند باشی بستهٔ زندان خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر مواج ازل را گوهری</p></div>
<div class="m2"><p>گوهر تحقیق را سوداگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>واگذار این لاشهٔ ناچیز را</p></div>
<div class="m2"><p>در نورد این راه آفت خیز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زر کانی را چه نسبت با سفال</p></div>
<div class="m2"><p>شیر جنگی را چه خویشی با شغال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باخرد، صلحی کن و رائی بزن</p></div>
<div class="m2"><p>کژدم تن را بسر، پائی بزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ پاکی همچو تو پاکیزه نیست</p></div>
<div class="m2"><p>گوش هستی را چنین آویزه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو یکی تابنده گوهر بوده‌ای</p></div>
<div class="m2"><p>رخ چرا با تیرگی آلوده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو چراغ ملک تاریک تنی</p></div>
<div class="m2"><p>در سیاهی‌ها، چو مهر روشنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نظر پنهانی، از دل نیستی</p></div>
<div class="m2"><p>کاش میگفتی کجائی، کیستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محبس تن بشکن و پرواز کن</p></div>
<div class="m2"><p>این نخ پوسیده از پا باز کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ببینی کآنچه دید ماسواست</p></div>
<div class="m2"><p>تا بدانی خلوت پاکان جداست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا بدانی صحبت یاران خوشست</p></div>
<div class="m2"><p>گیر و دار زلف دلداران خوشست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا ببینی کعبهٔ مقصود را</p></div>
<div class="m2"><p>بر گشائی چشم خواب آلود را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا نمایندت بهنگام خرام</p></div>
<div class="m2"><p>سیرگاهی خالی از صیاد و دام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا بیاموزند اسرار حقت</p></div>
<div class="m2"><p>تا کنند از عاشقان مطلقت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا تو، پنهان از تو، چون و چندهاست</p></div>
<div class="m2"><p>عهدها، میثاقها، پیوندهاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چند در هر دام، باید گشت صید</p></div>
<div class="m2"><p>چند از هر دیو، باید دید کید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چند از هر تیغ، باید باخت سر</p></div>
<div class="m2"><p>چند از هر سنگ، باید ریخت پر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرغک اندر بیضه چون گردد پدید</p></div>
<div class="m2"><p>گوید اینجا بس فراخ است و سپید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عاقبت کان حصن سخت از هم شکست</p></div>
<div class="m2"><p>عالمی بیند همه بالا و پست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گه پرد آزاد در کهسارها</p></div>
<div class="m2"><p>گه چمد سر مست در گلزارها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گاه بر چیند ز بامی دانه‌ای</p></div>
<div class="m2"><p>سر کند خوش نغمهٔ مستانه‌ای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جست و خیز طائران بیند همی</p></div>
<div class="m2"><p>فارغ اندر سبزه بنشیند دمی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بینوائی مهره‌ای تابنده داشت</p></div>
<div class="m2"><p>کاز فروغش دیده و دل زنده داشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خیره شد فرجام زان جلوه‌گری</p></div>
<div class="m2"><p>بردش از شادی بسوی گوهری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت این لعلست، از من میخرش</p></div>
<div class="m2"><p>گفت سنگست این، چه خوانی گوهرش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رو، که این ما را نمی‌آید بکار</p></div>
<div class="m2"><p>گر متاعی خوبتر داری بیار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دکهٔ خر مهره، جای دیگر است</p></div>
<div class="m2"><p>تحفهٔ گوهر فروشان، گوهر است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برتری تنها برنگ و بوی نیست</p></div>
<div class="m2"><p>آینهٔ جان از برای روی نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا نداند دخل و خرجش چند بود</p></div>
<div class="m2"><p>هیچ بازرگان نخواهد برد سود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چشم جانرا، بی نگه دیدارهاست</p></div>
<div class="m2"><p>پای دل را، بی قدم رفتارهاست</p></div></div>