---
title: >-
    شمارهٔ  ۱۷۳ - بیت
---
# شمارهٔ  ۱۷۳ - بیت

<div class="b" id="bn1"><div class="m1"><p>دل پاکیزه، بکردار بد آلوده مکن</p></div>
<div class="m2"><p>تیرگی خواستن، از نور گریزان شدن است</p></div></div>