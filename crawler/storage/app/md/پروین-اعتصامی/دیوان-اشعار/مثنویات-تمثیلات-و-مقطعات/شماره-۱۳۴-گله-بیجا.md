---
title: >-
    شمارهٔ  ۱۳۴ - گلهٔ بیجا
---
# شمارهٔ  ۱۳۴ - گلهٔ بیجا

<div class="b" id="bn1"><div class="m1"><p>گفت گرگی با سگی، دور از رمه</p></div>
<div class="m2"><p>که سگان خویشند با گرگان، همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چه گشتستیم ما از هم بری</p></div>
<div class="m2"><p>خوی کردستیم با خیره‌سری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چه معنی، خویشی ما ننگ شد</p></div>
<div class="m2"><p>کار ما تزویر و ریو و رنگ شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگذری تو هیچگاه از کوی ما</p></div>
<div class="m2"><p>ننگری جز خشمگین، بر روی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اولین فرض است خویشاوند را</p></div>
<div class="m2"><p>که بجوید گمشده پیوند را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هفته‌ها، خون خوردم از زخم گلو</p></div>
<div class="m2"><p>نه عیادت کردی و نه جستجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماهها نالیدم از تب، زار زار</p></div>
<div class="m2"><p>هیچ دانستی چه بود آن روزگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بارها از پیری افتادم ز پا</p></div>
<div class="m2"><p>هیچ از دستم گرفتی، ای فتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزها صیاد، ناهارم گذاشت</p></div>
<div class="m2"><p>هیچ پرسیدی چه خوردم شام و چاشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این چه رفتار است، ای یار قدیم</p></div>
<div class="m2"><p>تو ظنین از ما و ما در رنج و بیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پی یک بره، از شب تا سحر</p></div>
<div class="m2"><p>بس دوانیدی مرا در جوی و جر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از برای دنبه یک گوسفند</p></div>
<div class="m2"><p>بارها ما را رسانیدی گزند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آفت گرگان شدی در شهر و ده</p></div>
<div class="m2"><p>غیر، صد راه از تو خویشاوند به</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت، این خویشان وبال گردنند</p></div>
<div class="m2"><p>دشمنان دوست، ما را دشمنند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر ز خویشان تو خوانم خویش را</p></div>
<div class="m2"><p>کشته باشم هم بز و هم میش را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ما سگ مسکین بازاری نه‌ایم</p></div>
<div class="m2"><p>کاهل از سستی و بیکاری نه‌ایم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ما بکندیم از خیانتکار، پوست</p></div>
<div class="m2"><p>خواه دشمن بود خائن، خواه دوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با سخن، خود را نمیبایست باخت</p></div>
<div class="m2"><p>خلق را از کارشان باید شناخت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>غیر، تا همراه و خیراندیش تست</p></div>
<div class="m2"><p>صد ره ار بیگانه باشد، خویش تست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خویش بد خواهی، که غیر از بد نخواست</p></div>
<div class="m2"><p>از تو بیگانه است، پس خویشی کجاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رو، که این خویشی نمی‌آید بکار</p></div>
<div class="m2"><p>گله از ده رفت، ما را واگذار</p></div></div>