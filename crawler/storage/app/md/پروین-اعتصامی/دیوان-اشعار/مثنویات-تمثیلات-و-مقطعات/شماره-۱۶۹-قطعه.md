---
title: >-
    شمارهٔ  ۱۶۹ - قطعه
---
# شمارهٔ  ۱۶۹ - قطعه

<div class="b" id="bn1"><div class="m1"><p>گر شمع را ز شعله رهائی است آرزو</p></div>
<div class="m2"><p>آتش چرا به خرمن پروانه میزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرمست، ای کبوترک ساده دل، مپر</p></div>
<div class="m2"><p>در تیه آز، راه تو را دانه میزند</p></div></div>