---
title: >-
    شمارهٔ  ۱۳۹ - لطف حق
---
# شمارهٔ  ۱۳۹ - لطف حق

<div class="b" id="bn1"><div class="m1"><p>مادر موسی، چو موسی را به نیل</p></div>
<div class="m2"><p>در فکند، از گفتهٔ رب جلیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود ز ساحل کرد با حسرت نگاه</p></div>
<div class="m2"><p>گفت کای فرزند خرد بی‌گناه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر فراموشت کند لطف خدای</p></div>
<div class="m2"><p>چون رهی زین کشتی بی ناخدای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نیارد ایزد پاکت بیاد</p></div>
<div class="m2"><p>آب خاکت را دهد ناگه بباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحی آمد کاین چه فکر باطل است</p></div>
<div class="m2"><p>رهرو ما اینک اندر منزل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پردهٔ شک را برانداز از میان</p></div>
<div class="m2"><p>تا ببینی سود کردی یا زیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما گرفتیم آنچه را انداختی</p></div>
<div class="m2"><p>دست حق را دیدی و نشناختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در تو، تنها عشق و مهر مادری است</p></div>
<div class="m2"><p>شیوهٔ ما، عدل و بنده پروری است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست بازی کار حق، خود را مباز</p></div>
<div class="m2"><p>آنچه بردیم از تو، باز آریم باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سطح آب از گاهوارش خوشتر است</p></div>
<div class="m2"><p>دایه‌اش سیلاب و موجش مادر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رودها از خود نه طغیان میکنند</p></div>
<div class="m2"><p>آنچه میگوئیم ما، آن میکنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما، بدریا حکم طوفان میدهیم</p></div>
<div class="m2"><p>ما، بسیل و موج فرمان می‌دهیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نسبت نسیان بذات حق مده</p></div>
<div class="m2"><p>بار کفر است این، بدوش خود منه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به که برگردی، بما بسپاریش</p></div>
<div class="m2"><p>کی تو از ما دوست‌تر میداریش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نقش هستی، نقشی از ایوان ماست</p></div>
<div class="m2"><p>خاک و باد و آب، سرگردان ماست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قطره‌ای کز جویباری میرود</p></div>
<div class="m2"><p>از پی انجام کاری میرود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ما بسی گم گشته، باز آورده‌ایم</p></div>
<div class="m2"><p>ما، بسی بی توشه را پرورده‌ایم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>میهمان ماست، هر کس بینواست</p></div>
<div class="m2"><p>آشنا با ماست، چون بی آشناست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ما بخوانیم، ار چه ما را رد کنند</p></div>
<div class="m2"><p>عیب پوشیها کنیم، ار بد کنند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سوزن ما دوخت، هر جا هر چه دوخت</p></div>
<div class="m2"><p>زاتش ما سوخت، هر شمعی که سوخت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کشتئی زاسیب موجی هولناک</p></div>
<div class="m2"><p>رفت وقتی سوی غرقاب هلاک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تند بادی، کرد سیرش را تباه</p></div>
<div class="m2"><p>روزگار اهل کشتی شد سیاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>طاقتی در لنگر و سکان نماند</p></div>
<div class="m2"><p>قوتی در دست کشتیبان نماند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ناخدایان را کیاست اندکی است</p></div>
<div class="m2"><p>ناخدای کشتی امکان یکی است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بندها را تار و پود، از هم گسیخت</p></div>
<div class="m2"><p>موج، از هر جا که راهی یافت ریخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر چه بود از مال و مردم، آب برد</p></div>
<div class="m2"><p>زان گروه رفته، طفلی ماند خرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>طفل مسکین، چون کبوتر پر گرفت</p></div>
<div class="m2"><p>بحر را چون دامن مادر گرفت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>موجش اول، وهله، چون طومار کرد</p></div>
<div class="m2"><p>تند باد اندیشهٔ پیکار کرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بحر را گفتم دگر طوفان مکن</p></div>
<div class="m2"><p>این بنای شوق را، ویران مکن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در میان مستمندان، فرق نیست</p></div>
<div class="m2"><p>این غریق خرد، بهر غرق نیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صخره را گفتم، مکن با او ستیز</p></div>
<div class="m2"><p>قطره را گفتم، بدان جانب مریز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>امر دادم باد را، کان شیرخوار</p></div>
<div class="m2"><p>گیرد از دریا، گذارد در کنار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سنگ را گفتم بزیرش نرم شو</p></div>
<div class="m2"><p>برف را گفتم، که آب گرم شو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صبح را گفتم، برویش خنده کن</p></div>
<div class="m2"><p>نور را گفتم، دلش را زنده کن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لاله را گفتم، که نزدیکش بروی</p></div>
<div class="m2"><p>ژاله را گفتم، که رخسارش بشوی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خار را گفتم، که خلخالش مکن</p></div>
<div class="m2"><p>مار را گفتم، که طفلک را مزن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رنج را گفتم، که صبرش اندک است</p></div>
<div class="m2"><p>اشک را گفتم، مکاهش کودک است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گرگ را گفتم، تن خردش مدر</p></div>
<div class="m2"><p>دزد را گفتم، گلوبندش مبر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بخت را گفتم، جهانداریش ده</p></div>
<div class="m2"><p>هوش را گفتم، که هشیاریش ده</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تیرگیها را نمودم روشنی</p></div>
<div class="m2"><p>ترسها را جمله کردم ایمنی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ایمنی دیدند و ناایمن شدند</p></div>
<div class="m2"><p>دوستی کردم، مرا دشمن شدند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کارها کردند، اما پست و زشت</p></div>
<div class="m2"><p>ساختند آئینه‌ها، اما ز خشت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا که خود بشناختند از راه، چاه</p></div>
<div class="m2"><p>چاهها کندند مردم را براه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>روشنیها خواستند، اما ز دود</p></div>
<div class="m2"><p>قصرها افراشتند، اما به رود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>قصه‌ها گفتند بی‌اصل و اساس</p></div>
<div class="m2"><p>دزدها بگماشتند از بهر پاس</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جامها لبریز کردند از فساد</p></div>
<div class="m2"><p>رشته‌ها رشتند در دوک عناد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>درسها خواندند، اما درس عار</p></div>
<div class="m2"><p>اسبها راندند، اما بی‌فسار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دیوها کردند دربان و وکیل</p></div>
<div class="m2"><p>در چه محضر، محضر حی جلیل</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سجده‌ها کردند بر هر سنگ و خاک</p></div>
<div class="m2"><p>در چه معبد، معبد یزدان پاک</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>رهنمون گشتند در تیه ضلال</p></div>
<div class="m2"><p>توشه‌ها بردند از وزر و وبال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از تنور خودپسندی، شد بلند</p></div>
<div class="m2"><p>شعلهٔ کردارهای ناپسند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>وارهاندیم آن غریق بی‌نوا</p></div>
<div class="m2"><p>تا رهید از مرگ، شد صید هوی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>آخر، آن نور تجلی دود شد</p></div>
<div class="m2"><p>آن یتیم بی‌گنه، نمرود شد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>رزمجوئی کرد با چون من کسی</p></div>
<div class="m2"><p>خواست یاری، از عقاب و کرکسی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کردمش با مهربانیها بزرگ</p></div>
<div class="m2"><p>شد بزرگ و تیره دلتر شد ز گرگ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>برق عجب، آتش بسی افروخته</p></div>
<div class="m2"><p>وز شراری، خانمان‌ها سوخته</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خواست تا لاف خداوندی زند</p></div>
<div class="m2"><p>برج و باروی خدا را بشکند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>رای بد زد، گشت پست و تیره رای</p></div>
<div class="m2"><p>سرکشی کرد و فکندیمش ز پای</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>پشه‌ای را حکم فرمودم که خیز</p></div>
<div class="m2"><p>خاکش اندر دیدهٔ خودبین بریز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا نماند باد عجبش در دماغ</p></div>
<div class="m2"><p>تیرگی را نام نگذارد چراغ</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ما که دشمن را چنین میپروریم</p></div>
<div class="m2"><p>دوستان را از نظر، چون میبریم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آنکه با نمرود، این احسان کند</p></div>
<div class="m2"><p>ظلم، کی با موسی عمران کند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>این سخن، پروین، نه از روی هوی ست</p></div>
<div class="m2"><p>هر کجا نوری است، ز انوار خداست</p></div></div>