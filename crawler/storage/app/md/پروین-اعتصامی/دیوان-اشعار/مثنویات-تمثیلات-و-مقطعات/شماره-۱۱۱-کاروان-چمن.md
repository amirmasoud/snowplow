---
title: >-
    شمارهٔ  ۱۱۱ - کاروان چمن
---
# شمارهٔ  ۱۱۱ - کاروان چمن

<div class="b" id="bn1"><div class="m1"><p>گفت با صید قفس، مرغ چمن</p></div>
<div class="m2"><p>که گل و میوه، خوش و تازه رس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگشای این قفس و بیرون آی</p></div>
<div class="m2"><p>که نه در باغ و نه در سبزه، کس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت، با شبرو گیتی چکنم</p></div>
<div class="m2"><p>که سحر دزد و شبانگه عسس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بسا گوشه، که میدان بلاست</p></div>
<div class="m2"><p>ای بسا دام، که در پیش و پس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گلستان جهان، یک گل نیست</p></div>
<div class="m2"><p>هر کجا مینگرم، خار و خس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو من، غافل و سرمست مپر</p></div>
<div class="m2"><p>قفس، آخر نه همین یک قفس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ پست است، بلندش مشمار</p></div>
<div class="m2"><p>اینکه دیدیش چو عنقا، مگس است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاروان است گل و لاله بباغ</p></div>
<div class="m2"><p>سبزه‌اش اسب و صبایش جرس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گرفتاری من، عبرت گیر</p></div>
<div class="m2"><p>که سرانجام هوی و هوس است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حاصل هستی بیهودهٔ ما</p></div>
<div class="m2"><p>آه سردی است که نامش نفس است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم دید این همه و گوش شنید</p></div>
<div class="m2"><p>آنچه دیدیم و شنیدیم بس است</p></div></div>