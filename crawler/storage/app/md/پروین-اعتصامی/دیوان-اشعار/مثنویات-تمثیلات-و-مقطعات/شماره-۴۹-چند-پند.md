---
title: >-
    شمارهٔ  ۴۹ - چند پند
---
# شمارهٔ  ۴۹ - چند پند

<div class="b" id="bn1"><div class="m1"><p>کسی که بر سر نرد جهان قمار نکرد</p></div>
<div class="m2"><p>سیاه روزی و بدنامی اختیار نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش آنکه از گل مسموم باغ دهر رمید</p></div>
<div class="m2"><p>برفق گر نظری کرد، جز به خار نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تیه فقر، ازان روی گشت دل حیران</p></div>
<div class="m2"><p>که هیچگه شتر آز را مهار نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نداشت دیدهٔ تحقیق، مردمی کاز دور</p></div>
<div class="m2"><p>بدید خیمهٔ اهریمن و فرار نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکار کرده بسی در دل شب، این صیاد</p></div>
<div class="m2"><p>مگو که روز گذشت و مرا شکار نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپهر پیر بسی رشتهٔ محبت و انس</p></div>
<div class="m2"><p>گرفت و بست بهم، لیک استوار نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشو چو وقت، که یک لحظه پایدار نماند</p></div>
<div class="m2"><p>مشو چو دهر، که یک عهد پایدار نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برو ز مورچه آموز بردباری و سعی</p></div>
<div class="m2"><p>که کار کرد و شکایت ز روزگار نکرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غبار گشت ز باد غرور، خرمن دل</p></div>
<div class="m2"><p>چنین معامله را باد با غبار نکرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سفینه‌ای که در آن فتنه بود کشتیبان</p></div>
<div class="m2"><p>برفت روز و شب و ره سوی کنار نکرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مباف جامهٔ روی و ریا، که جز ابلیس</p></div>
<div class="m2"><p>کس این دو رشتهٔ پوسیده پود و تار نکرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسی ز طعنهٔ پیکان روزگار رهید</p></div>
<div class="m2"><p>که گاه حملهٔ او، سستی آشکار نکرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طبیب دهر، بسی دردمند داشت ولیک</p></div>
<div class="m2"><p>طبیب وار سوی هیچ یک گذار نکرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرا وجود منزه به تیرگی پیوست</p></div>
<div class="m2"><p>چرا محافظت پنبه از شرار نکرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز خواب جهل، بس امسالها که پار شدند</p></div>
<div class="m2"><p>خوش آنکه بیهده، امسال خویش پار نکرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روا مدار پس از مدت تو گفته شود</p></div>
<div class="m2"><p>که دیر ماند فلانی و هیچ کار نکرد</p></div></div>