---
title: >-
    شمارهٔ  ۱۲۶ - گل بی عیب
---
# شمارهٔ  ۱۲۶ - گل بی عیب

<div class="b" id="bn1"><div class="m1"><p>بلبلی گفت سحر با گل سرخ</p></div>
<div class="m2"><p>کاینهمه خار بگرد تو چراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل خشبوی و نکوئی چو ترا</p></div>
<div class="m2"><p>همنشین بودن با خار خطاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که پیوند تو جوید، خوار است</p></div>
<div class="m2"><p>هر که نزدیک تو آید، رسواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاجب قصر تو، هر روز خسی است</p></div>
<div class="m2"><p>بسر کوی تو، هر شب غوغاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما تو را سیر ندیدیم دمی</p></div>
<div class="m2"><p>خار دیدیم همی از چپ و راست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقان، در همه جا ننشینند</p></div>
<div class="m2"><p>خلوت انس و وثاق تو کجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خار، گاهم سر و گه پای بخسب</p></div>
<div class="m2"><p>همنشین تو، عجب بی سر و پاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل سرخی و نپرسی که چرا</p></div>
<div class="m2"><p>خار در مهد تو، در نشو و نماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت، زیبائی گل را مستای</p></div>
<div class="m2"><p>زانکه یکره خوش و یکدم زیباست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن خوشی کز تو گریزد، چه خوشی است</p></div>
<div class="m2"><p>آن صفائی که نماند، چه صفا است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناگریز است گل از صحبت خار</p></div>
<div class="m2"><p>چمن و باغ، بفرمان قضا است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما شکفتیم که پژمرده شویم</p></div>
<div class="m2"><p>گل سرخی که دو شب ماند، گیاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاقبت، خوارتر از خار شود</p></div>
<div class="m2"><p>این گل تازه که محبوب شماست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رو، گلی جوی که همواره خوش است</p></div>
<div class="m2"><p>باغ تحقیق ازین باغ، جداست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این چنین خواستهٔ بیغش را</p></div>
<div class="m2"><p>ز دکان دگری باید خواست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ما چو رفتیم، گل دیگر هست</p></div>
<div class="m2"><p>ذات حق، بی خلل و بی همتاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه را کشتی نسیان، کشتی است</p></div>
<div class="m2"><p>همه را، راه بدریای فناست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه توان داشت جز این، چشم ز دهر</p></div>
<div class="m2"><p>چه توان کرد، فلک بی‌پرواست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز ترازوی قضا، شکوه مکن</p></div>
<div class="m2"><p>که ز وزن همه کس، خواهد کاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ره آن پوی که پیدایش ازوست</p></div>
<div class="m2"><p>لیک با اینهمه، خود ناپیداست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نتوان گفت که خار از چه دمید</p></div>
<div class="m2"><p>خار را نیز درین باغ، بهاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چرخ، با هر که نشاندت بنشین</p></div>
<div class="m2"><p>هر چه را خواجه روا دید، رواست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بنده، شایستهٔ تنهائی نیست</p></div>
<div class="m2"><p>حق تعالی و تقدس، تنهاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گهر معدن مقصود، یکی است</p></div>
<div class="m2"><p>وانچه برجاست، شبه یا میناست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خلوتی خواه، کاز اغیار تهی است</p></div>
<div class="m2"><p>دولتی جوی، که بیچون و چراست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر گلی، علت و عیبی دارد</p></div>
<div class="m2"><p>گل بی علت و بی عیب، خداست</p></div></div>