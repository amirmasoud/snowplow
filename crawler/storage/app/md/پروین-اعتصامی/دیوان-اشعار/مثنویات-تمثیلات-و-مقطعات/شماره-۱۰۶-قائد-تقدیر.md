---
title: >-
    شمارهٔ  ۱۰۶ - قائد تقدیر
---
# شمارهٔ  ۱۰۶ - قائد تقدیر

<div class="b" id="bn1"><div class="m1"><p>کرد آسیا ز آب، سحرگاه باز خواست</p></div>
<div class="m2"><p>کای خودپسند، با منت این بدسری چراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چیره‌دستی تو، مرا صبر و تاب رفت</p></div>
<div class="m2"><p>از خیره گشتن تو، مرا وزن و قدر کاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر روز، قسمتی ز تنم خاک میشود</p></div>
<div class="m2"><p>وان خاک، چون نسیم بمن بگذرد، هباست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسوده‌اند کارگران جمله، وقت شب</p></div>
<div class="m2"><p>چون من که دیده‌ای که شب و روز مبتلاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردیدن است کار من، از ابتدای کار</p></div>
<div class="m2"><p>آگه نیم کزین همه گردش، چه مدعاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرسودن من از تو بدینسان، شگفت نیست</p></div>
<div class="m2"><p>این چشمهٔ فساد، ندانستم از کجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان پیشتر که سوده شوم پاک، باز گرد</p></div>
<div class="m2"><p>شاید که بازگشت تو، این درد را دواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با این خوشی، چرا به ستم خوی کرده‌ای</p></div>
<div class="m2"><p>آلودگی، چگونه درین پاکی و صفاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دل هر آنچه از تو نهفتم، شکستگی است</p></div>
<div class="m2"><p>بر من هر آنچه از تو رسد، خواری و جفاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیهوده چند عرصه بمن تنگ میکنی</p></div>
<div class="m2"><p>بهر گذشتن تو بصحرا، هزار جاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خندید آب، کین ره و رسم از من و تو نیست</p></div>
<div class="m2"><p>ما رهرویم و قائد تقدیر، رهنماست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من از تو تیره‌روزترم، تنگدل مباش</p></div>
<div class="m2"><p>بس فتنه‌ها که با تو نه و با من آشناست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لرزیده‌ام همیشه ز هر باد و هر نسیم</p></div>
<div class="m2"><p>هرگز نگفته‌ام که سموم است یا صباست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از کوه و آفتاب، بسی لطمه خورده‌ام</p></div>
<div class="m2"><p>بر حالم، این پریشی و افتادگی گواست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همواره جود کردم و چیزی نخواستم</p></div>
<div class="m2"><p>طبعم غنی و دوستیم خالی از ریاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بس شاخه کز فتادگیم بر فراشت سر</p></div>
<div class="m2"><p>بس غنچه کز فروغ منش رونق و ضیاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز الودگی، هر آنچه رسیدست شسته‌ام</p></div>
<div class="m2"><p>گر حله یمانی و گر کهنه بوریاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از رود و دشت و دره گذشتیم هزار سال</p></div>
<div class="m2"><p>با من نگفت هیچکسی، کاین چه ماجراست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر قطره‌ام که باد پراکنده میکند</p></div>
<div class="m2"><p>آن قطره گاه در زمی و گاه در سماست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سر گشته‌ام چو گوی، ز روزی که زاده‌ام</p></div>
<div class="m2"><p>سرگشته دیده‌اید که او را نه سر، نه پاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از کار خویش، خستگیم نیست، زان سبب</p></div>
<div class="m2"><p>کاز من همیشه باغ و چمن را گل و گیاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قدر تو آن بود که کنی آرد، گندمی</p></div>
<div class="m2"><p>ور نه بکوهسار، بسی سنگ بی‌بهاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر رنج میکشیم چه غم، زانکه خلق را</p></div>
<div class="m2"><p>آسودگی و خوشدلی از آب و نان ماست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آبم من، ار بخار شوم در چمن، خوش است</p></div>
<div class="m2"><p>سنگی تو، گر که کار کنی بشکنی رواست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون کار هر کسی به سزاوار داده‌اند</p></div>
<div class="m2"><p>از کارگاه دهر، همین کارمان سزاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با عزم خویش، هیچیک این ره نمیرویم</p></div>
<div class="m2"><p>کشتی، مبرهن است که محتاج ناخداست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در زحمتیم هر دو ز سختی و رنج، لیک</p></div>
<div class="m2"><p>هر چ آن بما کنند، نه از ما، نه از شماست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از ما چه صلح خیزد و جنگ، این چه فکر تست</p></div>
<div class="m2"><p>در دست دیگریست، گر آ ب و گر آسیاست</p></div></div>