---
title: >-
    شمارهٔ  ۷۱ - روباه نفس
---
# شمارهٔ  ۷۱ - روباه نفس

<div class="b" id="bn1"><div class="m1"><p>ز قلعه، ماکیانی شد به دیوار</p></div>
<div class="m2"><p>بناگه روبهی کردش گرفتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چشمش برد، وحشت روشنائی</p></div>
<div class="m2"><p>بزد بال و پر، از بی دست و پائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز روز نیکبختی یادها کرد</p></div>
<div class="m2"><p>در آن درماندگی، فریادها کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فضای خانه و باغش هوس بود</p></div>
<div class="m2"><p>چه حاصل، خانه دور از دسترس بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیاد آورد زان اقلیم ایمن</p></div>
<div class="m2"><p>ز کاه و خوابگاه و آب و ارزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهان با خویشتن بس گفتگو کرد</p></div>
<div class="m2"><p>در آن یکدم، هزاران آرزو کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه تدبیر، احوالی زبون داشت</p></div>
<div class="m2"><p>بجای دل، ببر یکقطره خون داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیاد آورد زان آزاد گشتن</p></div>
<div class="m2"><p>ز صحرا جانب ده بازگشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمودن رهروان خرد را راه</p></div>
<div class="m2"><p>ز هر بیراهه و ره بودن آگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز دنبال نو آموزان دویدن</p></div>
<div class="m2"><p>شدن استاد درس چینه چیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گشودن پر ز بهر سایبانی</p></div>
<div class="m2"><p>نخفتن در خیال پاسبانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بکار، از کودکان پیش اوفتادن</p></div>
<div class="m2"><p>رموز کارشان تعلیم دادن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برو به لابه کرد از عجز، کایدوست</p></div>
<div class="m2"><p>ز من چیزی نیابی، جز پر و پوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منه در رهگذار چون منی دام</p></div>
<div class="m2"><p>مکن خود را برای هیچ بدنام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرفتم سینهٔ تنگم فشردی</p></div>
<div class="m2"><p>مرا کشتی و در یک لحظه خوردی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز مادر بی‌خبر شد کودکی چند</p></div>
<div class="m2"><p>تبه گردید عمر مرغکی چند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی را کودک همسایه آزرد</p></div>
<div class="m2"><p>یکی را گربه، آن یک را سگی برد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>طمع دیو است، با وی برنیائی</p></div>
<div class="m2"><p>چو خوردی، باز فردا ناشتائی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هوی و حرص و مستی، خواجه تاشند</p></div>
<div class="m2"><p>سیه کارند، در هر جا که باشند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دچار زحمتی تا صید آزی</p></div>
<div class="m2"><p>اگر زین دام رستی، بی‌نیازی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مباش اینگونه بی‌پروا و بدخواه</p></div>
<div class="m2"><p>بسا گردد شکار گرگ، روباه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه گردی هرزه در هر رهگذاری</p></div>
<div class="m2"><p>دهی هر دم گلوئی را فشاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگفت ار تیره‌دل یا هرزه گردیم</p></div>
<div class="m2"><p>درین ره هر چه فرمودند، کردیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز روز خردیم، خصلت چنین بود</p></div>
<div class="m2"><p>دلی روئین بزیر پوستین بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرم سر پنجه و دندان بود سخت</p></div>
<div class="m2"><p>مرا این مایه بود از کیسهٔ بخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در آن دفتر که نقش ما نوشتند</p></div>
<div class="m2"><p>یکی زشت و یکی زیبا نوشتند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو من روباه و صیدم ماکیانست</p></div>
<div class="m2"><p>گذشتن از چنین سودی زیانست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بسی مرغ و خروس از قریه بردم</p></div>
<div class="m2"><p>بگردنها بسی دندان فشردم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حدیث اتحاد مرغ و روباه</p></div>
<div class="m2"><p>بود چون اتفاق آتش و کاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه غم گر نیتم بد یا که نیکوست</p></div>
<div class="m2"><p>همینم اقتضای خلقت و خوست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو خود دادی بساط خویش بر باد</p></div>
<div class="m2"><p>تو افتادی که کار از دست افتاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو مرغ خانگی، روباه طرار</p></div>
<div class="m2"><p>تو خواب آلود و دزد چرخ بیدار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اسیر روبه نفس آن چنانیم</p></div>
<div class="m2"><p>که گوئی پر شکسته ماکیانیم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بهای زندگی زین بیشتر بود</p></div>
<div class="m2"><p>اگر یک دیدهٔ صاحب نظر بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>منه بردست دیو از سادگی دست</p></div>
<div class="m2"><p>کدامین دست را بگرفت و نشکست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مکن بی فکرتی تدبیر کاری</p></div>
<div class="m2"><p>که خواهد هر قماشی پود و تاری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بوقت شخم، گاوت در گرو بود</p></div>
<div class="m2"><p>چو باز آوردیش، وقت درو بود</p></div></div>