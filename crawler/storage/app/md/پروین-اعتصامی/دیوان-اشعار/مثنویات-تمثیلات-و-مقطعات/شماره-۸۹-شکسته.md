---
title: >-
    شمارهٔ  ۸۹ - شکسته
---
# شمارهٔ  ۸۹ - شکسته

<div class="b" id="bn1"><div class="m1"><p>با بنفشه، لاله گفت ای بیخبر</p></div>
<div class="m2"><p>طرف گلشن را منظم کرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از برای جلوه، گلهای چمن</p></div>
<div class="m2"><p>رنگ را با بوی توام کرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندرین بزم طرب، گوئی ترا</p></div>
<div class="m2"><p>غرق در دریای ماتم کرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از چه معنی، در شکستی بی سبب</p></div>
<div class="m2"><p>چون بخاکت ریشه محکم کرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چه، رویت در هم و پشتت خم است</p></div>
<div class="m2"><p>از چه رو، کار تو درهم کرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چه، خود را پشت سر می‌افکنی</p></div>
<div class="m2"><p>چون به یارانت مقدم کرده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زیان این قبای نیلگون</p></div>
<div class="m2"><p>در تو زشتی را مسلم کرده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت، بهر بردن بار قضا</p></div>
<div class="m2"><p>عاقلان، پشت از ازل خم کرده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عارفان، از بهر افزودن بجان</p></div>
<div class="m2"><p>از هوی و از هوس، کم کرده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یاد حق بر یاد خود بگزیده‌اند</p></div>
<div class="m2"><p>کار ابراهیم ادهم کرده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رهروان این گذرگاه، آگهند</p></div>
<div class="m2"><p>توش راه خود فراهم کرده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گله‌های معنی، از فرسنگها</p></div>
<div class="m2"><p>گرگ خود را دیده و رم کرده‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون در آخر، جمله شادیها غم است</p></div>
<div class="m2"><p>هم ز اول، خوی با غم کرده‌اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو نمیدانی که از بهر خزان</p></div>
<div class="m2"><p>باغ را شاداب و خرم کرده‌اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو نمی‌بینی چه سیلابی نهان</p></div>
<div class="m2"><p>در دل هر قطره شبنم کرده‌اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر کسی را با چراغ بینشی</p></div>
<div class="m2"><p>راهی این راه مظلم کرده‌اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از صبا گوئی تو و ما از سموم</p></div>
<div class="m2"><p>بهر ما، این شهد را سم کرده‌اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو، خوشی بینی و ما پژمردگی</p></div>
<div class="m2"><p>هر کجا، نقشی مجسم کرده‌اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ما بخود، چیزی نکردیم اختیار</p></div>
<div class="m2"><p>کارفرمایان عالم کرده‌اند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کرده‌اند ار پرسشی در کار ما</p></div>
<div class="m2"><p>خلقت و تقدیر، با هم کرده‌اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درزی و جولاههٔ ما، صنع خویش</p></div>
<div class="m2"><p>در پس این سبز طارم کرده‌اند</p></div></div>