---
title: >-
    شمارهٔ  ۴۸ - جولای خدا
---
# شمارهٔ  ۴۸ - جولای خدا

<div class="b" id="bn1"><div class="m1"><p>کاهلی در گوشه‌ای افتاد سست</p></div>
<div class="m2"><p>خسته و رنجور، اما تندرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنکبوتی دید بر در، گرم کار</p></div>
<div class="m2"><p>گوشه گیر از سرد و گرم روزگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوک همت را به کار انداخته</p></div>
<div class="m2"><p>جز ره سعی و عمل نشناخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پشت در افتاده، اما پیش بین</p></div>
<div class="m2"><p>از برای صید، دائم در کمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشته‌ها رشتی ز مو باریکتر</p></div>
<div class="m2"><p>زیر و بالا، دورتر، نزدیکتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده می ویخت پیدا و نهان</p></div>
<div class="m2"><p>ریسمان می تافت از آب دهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درس ها می داد بی نطق و کلام</p></div>
<div class="m2"><p>فکرها می‌پخت با نخ های خام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاردانان، کار زین سان می کنند</p></div>
<div class="m2"><p>تا که گویی هست، چوگان می زنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه تبه کردی، گهی آراستی</p></div>
<div class="m2"><p>گه درافتادی، گهی برخاستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کار آماده ولی افزار نه</p></div>
<div class="m2"><p>دایره صد جا ولی پرگار نه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زاویه بی حد، مثلث بی شمار</p></div>
<div class="m2"><p>این مهندس را که بود آموزگار؟!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کار کرده، صاحب کاری شده</p></div>
<div class="m2"><p>اندر آن معموره معماری شده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این چنین سوداگری را سودهاست</p></div>
<div class="m2"><p>وندرین یک تار، تار و پودهاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پای کوبان در نشیب و در فراز</p></div>
<div class="m2"><p>ساعتی جولا، زمانی بندباز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پست و بی مقدار، اما سربلند</p></div>
<div class="m2"><p>ساده و یک دل، ولی مشکل پسند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اوستاد اندر حساب رسم و خط</p></div>
<div class="m2"><p>طرح و نقشی خالی از سهو و غلط</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت کاهل کاین چه کار سرسری ست؟</p></div>
<div class="m2"><p>آسمان، زین کار کردنها بری ست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کوها کارست در این کارگاه</p></div>
<div class="m2"><p>کس نمی‌بیند ترا، ای پر کاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می تنی تاری که جاروبش کنند؟</p></div>
<div class="m2"><p>می کشی طرحی که معیوبش کنند؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هیچ گه عاقل نسازد خانه‌ای</p></div>
<div class="m2"><p>که شود از عطسه‌ای ویرانه‌ای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پایه می سازی ولی سست و خراب</p></div>
<div class="m2"><p>نقش نیکو می زنی، اما بر آب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رونقی می جوی گر ارزنده‌ای</p></div>
<div class="m2"><p>دیبه‌ای می باف گر بافنده‌ای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کس ز خلقان تو پیراهن نکرد</p></div>
<div class="m2"><p>وین نخ پوسیده در سوزن نکرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کس نخواهد دیدنت در پشت در</p></div>
<div class="m2"><p>کس نخواهد خواندنت ز اهل هنر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی سر و سامانی از دود و دمی</p></div>
<div class="m2"><p>غرق در طوفانی از آه و نمی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کس نخواهد دادنت پشم و کلاف</p></div>
<div class="m2"><p>کس نخواهد گفت کشمیری بباف</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بس زبر دست ست چرخ کینه‌توز</p></div>
<div class="m2"><p>پنبه ی خود را در این آتش مسوز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون تو نساجی، نخواهد داشت مزد</p></div>
<div class="m2"><p>دزد شد گیتی، تو نیز از وی بدزد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خسته کردی زین تنیدن پا و دست</p></div>
<div class="m2"><p>رو بخواب امروز، فردا نیز هست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا نخوردی پشت پایی از جهان</p></div>
<div class="m2"><p>خویش را زین گوشه گیری وارهان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفت آگه نیستی ز اسرار من</p></div>
<div class="m2"><p>چند خندی بر در و دیوار من؟!</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>علم ره بنمودن از حق، پا ز ما</p></div>
<div class="m2"><p>قدرت و یاری از او، یارا ز ما</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو به فکر خفتنی در این رباط</p></div>
<div class="m2"><p>فارغی زین کارگاه و زین بساط</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در تکاپوییم ما در راه دوست</p></div>
<div class="m2"><p>کارفرما او و کارآگاه اوست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر چه اندر کنج عزلت ساکنم</p></div>
<div class="m2"><p>شور و غوغایی ست اندر باطنم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دست من بر دستگاه محکمی ست</p></div>
<div class="m2"><p>هر نخ اندر چشم من ابریشمی است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کار ما گر سهل و گر دشوار بود</p></div>
<div class="m2"><p>کارگر می خواست، زیرا کار بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>صنعت ما پرده‌های ما بس است</p></div>
<div class="m2"><p>تار ما هم دیبه و هم اطلس است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ما نمی‌بافیم از بهر فروش</p></div>
<div class="m2"><p>ما نمی گوییم کاین دیبا بپوش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عیب ما زین پرده‌ها پوشیده شد</p></div>
<div class="m2"><p>پرده ی پندار تو پوسیده شد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر، درد این پرده، چرخ پرده در</p></div>
<div class="m2"><p>رخت بر بندم، روم جای دگر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر سحر ویران کنند این سقف و بام</p></div>
<div class="m2"><p>خانه ی دیگر بسازم وقت شام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر ز یک کنجم براند روزگار</p></div>
<div class="m2"><p>گوشه ی دیگر نمایم اختیار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ما که عمری پرده‌داری کرده‌ایم</p></div>
<div class="m2"><p>در حوادث، بردباری کرده‌ایم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گاه جاروبست و گه گرد و نسیم</p></div>
<div class="m2"><p>کهنه نتوان کرد این عهد قدیم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ما نمی‌ترسیم از تقدیر و بخت</p></div>
<div class="m2"><p>آگهیم از عمق این گرداب سخت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آنکه داد این دوک، ما را رایگان</p></div>
<div class="m2"><p>پنبه خواهد داد بهر ریسمان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هست بازاری دگر، ای خواجه تاش</p></div>
<div class="m2"><p>کاندر آنجا می‌شناسند این قماش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>صد خریدار و هزاران گنج زر</p></div>
<div class="m2"><p>نیست چون یک دیده ی صاحب نظر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تو ندیدی پرده ی دیوار را</p></div>
<div class="m2"><p>چون ببینی پرده ی اسرار را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خرده می‌گیری همی بر عنکبوت</p></div>
<div class="m2"><p>خود نداری هیچ جز باد بروت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ما تمام از ابتدا بافنده‌ایم</p></div>
<div class="m2"><p>حرفت ما این بود تا زنده‌ایم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سعی کردیم آنچه فرصت یافتیم</p></div>
<div class="m2"><p>بافتیم و بافتیم و بافتیم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پیشه‌ام این ست، گر کم یا زیاد</p></div>
<div class="m2"><p>من شدم شاگرد و ایام اوستاد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کار ما اینگونه شد، کار تو چیست؟</p></div>
<div class="m2"><p>بار ما خالی است، دربار تو چیست؟</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>می نهم دامی، شکاری می زنم</p></div>
<div class="m2"><p>جوله‌ام، هر لحظه تاری می‌تنم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خانه ی من از غباری چون هباست</p></div>
<div class="m2"><p>آن سرایی که تو می سازی کجاست؟</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خانه ی من ریخت از باد هوا</p></div>
<div class="m2"><p>خرمن تو سوخت از برق هوی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>من بری گشتم ز آرام و فراغ</p></div>
<div class="m2"><p>تو فکندی باد نخوت در دماغ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ما زدیم این خیمه ی سعی و عمل</p></div>
<div class="m2"><p>تا بدانی قدر وقت بی بدل</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گر که محکم بود و گر سست این بنا</p></div>
<div class="m2"><p>از برای ماست، نز بهر شما</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گر به کار خویش می‌پرداختی</p></div>
<div class="m2"><p>خانه‌ای زین آب و گل می‌ساختی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>می گرفتی گر به همت رشته‌ای</p></div>
<div class="m2"><p>داشتی در دست خود سر رشته‌ای</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>عارفان، از جهل رخ برتافتند</p></div>
<div class="m2"><p>تار و پودی چند در هم بافتند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دوختند این ریسمان ها را به هم</p></div>
<div class="m2"><p>از دراز و کوته و بسیار و کم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>رنگرز شو، تا که در خم هست رنگ</p></div>
<div class="m2"><p>برق شد فرصت، نمی داند درنگ</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گر بنایی هست باید برفراشت</p></div>
<div class="m2"><p>ای بسا امروز کان فردا نداشت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نقد امروز ار ز کف بیرون کنیم</p></div>
<div class="m2"><p>گر که فردایی نباشد، چون کنیم؟</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>عنکبوت، ای دوست، جولای خداست</p></div>
<div class="m2"><p>چرخه‌اش می گردد، اما بی صداست</p></div></div>