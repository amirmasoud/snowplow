---
title: >-
    شمارهٔ  ۱۳۷ - گوهر اشک
---
# شمارهٔ  ۱۳۷ - گوهر اشک

<div class="b" id="bn1"><div class="m1"><p>آن نشنیدید که یک قطره اشک</p></div>
<div class="m2"><p>صبحدم از چشم یتیمی چکید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برد بسی رنج نشیب و فراز</p></div>
<div class="m2"><p>گاه در افتاد و زمانی دوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه درخشید و گهی تیره ماند</p></div>
<div class="m2"><p>گاه نهان گشت و گهی شد پدید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقبت افتاد بدامان خاک</p></div>
<div class="m2"><p>سرخ نگینی بسر راه دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت، که ای، پیشه و نام تو چیست</p></div>
<div class="m2"><p>گفت مرا با تو چه گفت و شنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من گهر ناب و تو یک قطره آب</p></div>
<div class="m2"><p>من ز ازل پاک، تو پست و پلید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوست نگردند فقیر و غنی</p></div>
<div class="m2"><p>یار نباشند شقی و سعید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اشک بخندید که رخ بر متاب</p></div>
<div class="m2"><p>بی سبب، از خلق نباید رمید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داد بهر یک، هنر و پرتوی</p></div>
<div class="m2"><p>آنکه در و گوهر و اشک آفرید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من گهر روشن گنج دلم</p></div>
<div class="m2"><p>فارغم از زحمت قفل و کلید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پرده‌نشین بودم ازین پیشتر</p></div>
<div class="m2"><p>دور جهان، پرده ز کارم کشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برد مرا باد حوادث نوا</p></div>
<div class="m2"><p>داد تو را، پیک سعادت نوید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من سفر دیده ز دل کرده‌ام</p></div>
<div class="m2"><p>کس نتوانست چنین ره برید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آتش آهیم، چنین آب کرد</p></div>
<div class="m2"><p>آب شنیدید کز آتش جهید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من بنظر قطره، بمعنی یمم</p></div>
<div class="m2"><p>دیده ز موجم نتواند رهید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همنفسم گشت شبی آرزو</p></div>
<div class="m2"><p>همسفرم بود، صباحی امید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تیرگی ملک تنم، رنجه کرد</p></div>
<div class="m2"><p>رنگم از آن روی، بدینسان پرید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تاب من، از تاب تو افزونتر است</p></div>
<div class="m2"><p>گر چه تو سرخی بنظر، من سپید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چهر من از چهرهٔ جان، یافت رنگ</p></div>
<div class="m2"><p>نور من، از روشنی دل رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نکته درینجاست، که ما را فروخت</p></div>
<div class="m2"><p>گوهری دهر و شما را خرید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کاش قضایم، چو تو برمیفراشت</p></div>
<div class="m2"><p>کاش سپهرم، چو تو برمیگزید</p></div></div>