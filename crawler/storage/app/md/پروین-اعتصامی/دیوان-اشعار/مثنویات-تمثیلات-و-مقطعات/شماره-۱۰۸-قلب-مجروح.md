---
title: >-
    شمارهٔ  ۱۰۸ - قلب مجروح
---
# شمارهٔ  ۱۰۸ - قلب مجروح

<div class="b" id="bn1"><div class="m1"><p>دی، کودکی بدامن مادر گریست زار</p></div>
<div class="m2"><p>کز کودکان کوی، بمن کس نظر نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طفلی، مرا ز پهلوی خود بیگناه راند</p></div>
<div class="m2"><p>آن تیر طعنه، زخم کم از نیشتر نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اطفال را بصحبت من، از چه میل نیست</p></div>
<div class="m2"><p>کودک مگر نبود، کسی کو پدر نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز، اوستاد بدرسم نگه نکرد</p></div>
<div class="m2"><p>مانا که رنج و سعی فقیران، ثمر نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیروز، در میانهٔ بازی، ز کودکان</p></div>
<div class="m2"><p>آن شاه شد که جامهٔ خلقان ببر نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من در خیال موزه، بسی اشک ریختم</p></div>
<div class="m2"><p>این اشک و آرزو، ز چه هرگز اثر نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز من، میان این گل و باران کسی نبود</p></div>
<div class="m2"><p>کو موزه‌ای بپا و کلاهی بسر نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر، تفاوت من و طفلان شهر چیست</p></div>
<div class="m2"><p>آئین کودکی، ره و رسم دگر نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرگز درون مطبخ ما هیزمی نسوخت</p></div>
<div class="m2"><p>وین شمع، روشنائی ازین بیشتر نداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همسایگان ما بره و مرغ میخورند</p></div>
<div class="m2"><p>کس جز من و تو، قوت ز خون جگر نداشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر وصله‌های پیرهنم خنده می‌کنند</p></div>
<div class="m2"><p>دینار و درهمی، پدر من مگر نداشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خندید و گفت، آنکه بفقر تو طعنه زد</p></div>
<div class="m2"><p>از دانه‌های گوهر اشکت، خبر نداشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از زندگانی پدر خود مپرس، از آنک</p></div>
<div class="m2"><p>چیزی بغیر تیشه و داس و تبر نداشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این بوریای کهنه، بصد خون دل خرید</p></div>
<div class="m2"><p>رختش، گه آستین و گهی آستر نداشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بس رنج برد و کس نشمردش به هیچ کس</p></div>
<div class="m2"><p>گمنام زیست، آنکه ده و سیم و زر نداشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طفل فقیر را، هوس و آرزو خطاست</p></div>
<div class="m2"><p>شاخی که از تگرگ نگون گشت، بر نداشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نساج روزگار، درین پهن بارگاه</p></div>
<div class="m2"><p>از بهر ما، قماشی ازین خوبتر نداشت</p></div></div>