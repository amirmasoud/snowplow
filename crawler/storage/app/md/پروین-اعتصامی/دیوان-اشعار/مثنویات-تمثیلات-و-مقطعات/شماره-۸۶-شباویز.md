---
title: >-
    شمارهٔ  ۸۶ - شباویز
---
# شمارهٔ  ۸۶ - شباویز

<div class="b" id="bn1"><div class="m1"><p>چو رنگ از رخ روز، پرواز کرد</p></div>
<div class="m2"><p>شباویز، نالیدن آغاز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بساط سپیدی، تباهی گرفت</p></div>
<div class="m2"><p>ز مه تا بماهی، سیاهی گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ره فتنهٔ دزد عیار باز</p></div>
<div class="m2"><p>عسس خسته از گشتن و شب دراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخفته، نه مست و نه هوشیار ماند</p></div>
<div class="m2"><p>نیاسوده گر ماند، بیمار ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرستار را ناگهان خواب برد</p></div>
<div class="m2"><p>هماندم که او خفت، رنجور مرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان چون دل بت پرستان، سیاه</p></div>
<div class="m2"><p>مه از دیده پنهان و در راه، چاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخفتند مرغان باغ و قفس</p></div>
<div class="m2"><p>شباویز افسانه میگفت و بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمیکرد دیوانه دیگر خروش</p></div>
<div class="m2"><p>نمی‌آید آواز دیگر به گوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجز ریزش سیل از کوهسار</p></div>
<div class="m2"><p>بجز گریهٔ کودک شیرخوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برون آمد از کنج مطبخ، عجوز</p></div>
<div class="m2"><p>ز پیری بزحمت، ز سرما بسوز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکایت کنان، گه ز سر، گه ز پشت</p></div>
<div class="m2"><p>چراغی که در دست خود داشت کشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگسترد چون جامه از بهر خواب</p></div>
<div class="m2"><p>سبوئی شکست و فرو ریخت آب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شنیدم که کوته زمانی نخفت</p></div>
<div class="m2"><p>شکسته گرفت و پراکنده رفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنالید از نالهٔ مرغ شب</p></div>
<div class="m2"><p>که شب نیز فارغ نه‌ایم، ای عجب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ندیدیم آسایش از روزگار</p></div>
<div class="m2"><p>گهی بانگ مرغست و گه رنج کار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بنرمی چنین داد مرغش جواب</p></div>
<div class="m2"><p>که ای سالیان خفته، یکشب مخواب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به سر منزلی کاینقدر خون کنند</p></div>
<div class="m2"><p>در آن، خواب آزادگان چون کنند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من از چرخ پیرم چنین تنگدل</p></div>
<div class="m2"><p>که از ضعف پیران نگردد خجل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بهر دست فرسوده، کاری دهد</p></div>
<div class="m2"><p>بهر پشت کاهیده، باری نهد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسی رفته، گم گشت ازین راه راست</p></div>
<div class="m2"><p>بسی خفته، چون روز شد، برنخاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عسس کی شود، دزد تیره‌روان</p></div>
<div class="m2"><p>تو خود باش این گنج را پاسبان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بهرجا برافکنده‌اند این کمند</p></div>
<div class="m2"><p>چه دیوار کوته، چه بام بلند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درین دخمه، هر شب گرفتارهاست</p></div>
<div class="m2"><p>ره و رسمها، رمزها، کارهاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شب، از باغ گم شد گل و خار ماند</p></div>
<div class="m2"><p>خنک، باغبانی که بیدار ماند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بخفتن، چرا پیر گردد جوان</p></div>
<div class="m2"><p>برهزن، چرا بگرود کاروان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فلک، در نورد و تو در خوابگاه</p></div>
<div class="m2"><p>تو مدهوش و در شبروی مهر و ماه</p></div></div>