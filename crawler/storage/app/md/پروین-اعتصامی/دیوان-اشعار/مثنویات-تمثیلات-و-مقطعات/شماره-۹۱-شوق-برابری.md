---
title: >-
    شمارهٔ  ۹۱ - شوق برابری
---
# شمارهٔ  ۹۱ - شوق برابری

<div class="b" id="bn1"><div class="m1"><p>نارونی بود به هندوستان</p></div>
<div class="m2"><p>زاغچه‌ای داشت در آن آشیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاطرش از بندگی آزاد بود</p></div>
<div class="m2"><p>جایگهش ایمن و آباد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه غم آب و نه غم دانه داشت</p></div>
<div class="m2"><p>بود گدا، دولت شاهانه داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه گله‌ایش از فلک نیلفام</p></div>
<div class="m2"><p>نه غم صیاد و نه پروای دام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از همه بیگانه و از خویش نه</p></div>
<div class="m2"><p>در دل خردش، غم و تشویش نه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقبت، آن مرغک عزلت گزین</p></div>
<div class="m2"><p>گشت بسی خسته و اندوهگین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت، بهار است و همه دوستان</p></div>
<div class="m2"><p>رخت کشیدند سوی بوستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من نه بهار و نه خزان دیده‌ام</p></div>
<div class="m2"><p>خسته و فرسوده و رنجیده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند کنم خانه درین نارون</p></div>
<div class="m2"><p>چند برم حسرت باغ و چمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند در این لانه، نشیمن کنم</p></div>
<div class="m2"><p>خیزم و پرواز بگلشن کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نغمه زنم بر سر دیوار باغ</p></div>
<div class="m2"><p>خوش کنم از بوی ریاحین دماغ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همنفس قمری و بلبل شوم</p></div>
<div class="m2"><p>شانه کش گیسوی سنبل شوم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رفت به گلزار و بشاخی نشست</p></div>
<div class="m2"><p>دید خرامان دو سه طاوس مست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جمله، بسر چتر نگارین زده</p></div>
<div class="m2"><p>طعنه بصورت گری چین زده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زاغچه گردید گرفتارشان</p></div>
<div class="m2"><p>خواست شود پیرو رفتارشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باغ بکاوید و بهر سو شتافت</p></div>
<div class="m2"><p>تا دو سه دانه پر طاوس یافت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بست دو بر دم، یک دیگر بسر</p></div>
<div class="m2"><p>گفت، مرا کس نشناسد دگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گشت دمم، چون پرم آراسته</p></div>
<div class="m2"><p>کس نخریدست چنین خواسته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زیور طاوس بسر بسته‌ام</p></div>
<div class="m2"><p>از پر زیباش به پر بسته‌ام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بال بیاراست، پریدن گرفت</p></div>
<div class="m2"><p>همره طاوس، چمیدن گرفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دید چو طاوس در آن خودپسند</p></div>
<div class="m2"><p>بال و پر عاریتش را بکند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت که ای زاغ سیه روزگار</p></div>
<div class="m2"><p>پرتو، خالی است ز نقش و نگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زیور ما، روی تو نیکو نکرد</p></div>
<div class="m2"><p>ما و تو را همسر و همخو نکرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرچه پر ما، همه پیرایه بود</p></div>
<div class="m2"><p>لیک نه بهر تو فرومایه بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سیر و خرام تو، چه حاصل بباغ</p></div>
<div class="m2"><p>زاغی و طاوس نماند به زاغ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر چه کنی، هر چه ببندی به پر</p></div>
<div class="m2"><p>گاه روش، تو دگری، ما دگر</p></div></div>