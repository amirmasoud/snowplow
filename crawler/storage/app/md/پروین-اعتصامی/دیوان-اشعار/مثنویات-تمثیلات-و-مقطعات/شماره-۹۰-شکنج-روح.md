---
title: >-
    شمارهٔ  ۹۰ - شکنج روح
---
# شمارهٔ  ۹۰ - شکنج روح

<div class="b" id="bn1"><div class="m1"><p>بزندان تاریک، در بند سخت</p></div>
<div class="m2"><p>بخود گفت زندانی تیره‌بخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که شب گشت و راه نظر بسته شد</p></div>
<div class="m2"><p>برویم دگر باره، در بسته شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمین سنگ، در سنگ، دیوار سنگ</p></div>
<div class="m2"><p>فضا و دل و فرصت و کار، تنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرانجام کردار بد، نیک نیست</p></div>
<div class="m2"><p>جز این سهمگین جای تاریک نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین است فرجام خون ریختن</p></div>
<div class="m2"><p>رسد فتنه، از فتنه انگیختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن لحظه، دیگر نمیدید چشم</p></div>
<div class="m2"><p>بجز خون نبودی به چشمم، ز خشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبخشودم، از من چو زنهار خواست</p></div>
<div class="m2"><p>نبخشاید ار چرخ بر من، رواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پشیمانم از کرده، اما چه سود</p></div>
<div class="m2"><p>چو آتش برافروختم، داد دود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر دیده لختی گراید بخواب</p></div>
<div class="m2"><p>گهی دار بینم، زمانی طناب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب، این وحشت و درد و کابوس و رنج</p></div>
<div class="m2"><p>سحرگاه، آن آتش و آن شکنج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرا خیرگی با جهان میکنم</p></div>
<div class="m2"><p>حدیث عیان را نهان میکنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخستین دم، از کردهٔ پست من</p></div>
<div class="m2"><p>خبر داد، خونین شده دست من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا بازگشت، اول کار مشت</p></div>
<div class="m2"><p>همی گفت هر قطرهٔ خون، که کشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من آن تیغ آلوده، کردم بخاک</p></div>
<div class="m2"><p>پدیدار کردش خداوند پاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نهفتم من و ایزدش باز یافت</p></div>
<div class="m2"><p>چو من بافتم دام، او نیز بافت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همانا که ما را در آن تنگنای</p></div>
<div class="m2"><p>در آن لحظه میدید چشم خدای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه بر خیره، گردون تباهی کند</p></div>
<div class="m2"><p>سیاهی چو بیند، سیاهی کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کسانی که بر ما گواهی دهند</p></div>
<div class="m2"><p>سزای تباهی، تباهی دهند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پی کیفر روزگارم برند</p></div>
<div class="m2"><p>بدین پای، تا پای دارم برند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ببندند این چشم بی‌باک را</p></div>
<div class="m2"><p>که آلوده کرد این دل پاک را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدین دست، دژخیم پیشم کشد</p></div>
<div class="m2"><p>بنزدیکی دست خویشم کشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدست از قفا، دست بندم زنند</p></div>
<div class="m2"><p>کشند و بجائی بلندم زنند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدانم، در آن جایگاه بلند</p></div>
<div class="m2"><p>که بیند گزند، آنکه خواهد گزند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بجز پستی، از آن بلندی نزاد</p></div>
<div class="m2"><p>کسی را چنین سربلندی مباد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بد من که اکنون شریک من است</p></div>
<div class="m2"><p>پس از مرگ هم، مرده ریگ من است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بهر جا نهم پا، درین تیره جای</p></div>
<div class="m2"><p>فتاده است آن کشته‌ام پیش پای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز وحشت بگردانم ار سر دمی</p></div>
<div class="m2"><p>ز دنبالم آهسته آید همی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شبی، آن تن بی روان جان گرفت</p></div>
<div class="m2"><p>مرا ناگهان از گریبان گرفت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو دیدم، بلرزیدم از دیدنش</p></div>
<div class="m2"><p>عیان بود آن زخم بر گردنش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نشستم بهر سوی، با من نشست</p></div>
<div class="m2"><p>اشارت همی کرد با چشم و دست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو راه اوفتادم، براه افتاد</p></div>
<div class="m2"><p>چو باز ایستادم، بجای ایستاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در بسته را از کجا کرد باز</p></div>
<div class="m2"><p>چو رفت، از کجا باز گردید باز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سرانجام این کار دشوار چیست</p></div>
<div class="m2"><p>درین تیرگی، با منش کار چیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نگاهش، هزارم سخن گفت دوش</p></div>
<div class="m2"><p>دل آگاه شد، گر چه نشنید گوش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شبی گفت آهسته در گوش من</p></div>
<div class="m2"><p>که چو من، ترا نیز باید کفن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنین است فرجام بد کارها</p></div>
<div class="m2"><p>چو خاری بکاری، دمد خارها</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنین است مرد سیاه اندرون</p></div>
<div class="m2"><p>خطایش ره و ظلمتش رهنمون</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رفیقی چو کردار بد، پست نیست</p></div>
<div class="m2"><p>که جز در بدی، با تو همدست نیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنین است مزدوری نفس دون</p></div>
<div class="m2"><p>بریزند خونت، بریزی چو خون</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرو زین ره سخت با پای سست</p></div>
<div class="m2"><p>مکش چونکه خونرا به جز خون نشست</p></div></div>