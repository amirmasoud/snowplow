---
title: >-
    شمارهٔ  ۵۴ - خون دل
---
# شمارهٔ  ۵۴ - خون دل

<div class="b" id="bn1"><div class="m1"><p>مرغی بباغ رفت و یکی میوه کند و خورد</p></div>
<div class="m2"><p>ناگه ز دست چرخ بپایش رسید سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خونین به لانه آمد و سر زیر پر کشید</p></div>
<div class="m2"><p>غلتید چون کبوتر با باز کرده جنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگریست مرغ خرد که برخیز و سرخ کن</p></div>
<div class="m2"><p>مانند بال خویش، مرا نیز بال و چنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نالید و گفت خون دلست این نه رنگ و زیب</p></div>
<div class="m2"><p>صیاد روزگار، بمن عرصه کرد تنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر تو هم ز لانه، پی دانه بر پری</p></div>
<div class="m2"><p>از خون پر تو نیز بدینسان کنند رنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سبزه گر روی، کندت دست جور پر</p></div>
<div class="m2"><p>بر بام گر شوی، کندت سنگ فتنه لنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آهسته میوه‌ای بکن از شاخی و برو</p></div>
<div class="m2"><p>در باغ و مرغزار، مکن هیچگه درنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میدان سعی و کار، شما راست بعد ازین</p></div>
<div class="m2"><p>ما رفتگان بنوبت خود تاختیم خنگ</p></div></div>