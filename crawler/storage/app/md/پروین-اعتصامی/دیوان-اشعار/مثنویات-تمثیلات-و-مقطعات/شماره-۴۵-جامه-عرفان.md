---
title: >-
    شمارهٔ  ۴۵ - جامهٔ عرفان
---
# شمارهٔ  ۴۵ - جامهٔ عرفان

<div class="b" id="bn1"><div class="m1"><p>به درویشی، بزرگی جامه‌ای داد</p></div>
<div class="m2"><p>که این خلقان بنه، کز دوشت افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا بر خویش پیچی ژنده و دلق</p></div>
<div class="m2"><p>چو می‌بخشند کفش و جامه‌ات خلق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خود عوری، چرا بخشی قبا را</p></div>
<div class="m2"><p>چو رنجوری، چرا ریزی دوا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی را قدرت بذل و کرم بود</p></div>
<div class="m2"><p>که دیناریش در جای درم بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفت ای دوست، از صاحبدلان باش</p></div>
<div class="m2"><p>بجان پرداز و با تن سرگران باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن خاکی به پیراهن نیرزد</p></div>
<div class="m2"><p>وگر ارزد، بچشم من نیرزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ره تن را بزن، تا جان بماند</p></div>
<div class="m2"><p>ببند این دیو، تا ایمان بماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قبائی را که سر مغرور دارد</p></div>
<div class="m2"><p>تن آن بهتر که از خود دور دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آن فارغ ز رنج انقیادیم</p></div>
<div class="m2"><p>که ما را هر چه بود، از دست دادیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از آن معنی نشستم بر سر راه</p></div>
<div class="m2"><p>که تا از ره شناسان باشم آگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا اخلاص اهل راز دادند</p></div>
<div class="m2"><p>چو جانم جامهٔ ممتاز دادند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرفتیم آنچه داد اهریمن پست</p></div>
<div class="m2"><p>بدین دست و در افکندیم از آندست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شنیدیم اعتذار نفس مدهوش</p></div>
<div class="m2"><p>ازین گوش و برون کردیم از آن گوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در تاریک حرص و آز بستیم</p></div>
<div class="m2"><p>گشودند ار چه صد ره، باز بستیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه پستی ز دیو نفس زاید</p></div>
<div class="m2"><p>همه تاریکی از ملک تن آید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو جان پاک در حد کمال است</p></div>
<div class="m2"><p>کمال از تن طلب کردن وبال است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو من پروانه‌ام نور خدا را</p></div>
<div class="m2"><p>کجا با خود کشم کفش و قبا را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کسانی کاین فروغ پاک دیدند</p></div>
<div class="m2"><p>ازین تاریک جا دامن کشیدند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرانباری ز بار حرص و آز است</p></div>
<div class="m2"><p>وجود بی تکلف بی نیاز است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مکن فرمانبری اهریمنی را</p></div>
<div class="m2"><p>منه در راه برقی خرمنی را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه سود از جامهٔ آلوده‌ای چند</p></div>
<div class="m2"><p>خیال بوده و نابوده‌ای چند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کلاه و جامه چون بسیار گردد</p></div>
<div class="m2"><p>کله عجب و قبا پندار گردد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو تن رسواست، عیبش را چه پوشم</p></div>
<div class="m2"><p>چو بی پرواست، در کارش چه کوشم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شکستیمش که جان مغزست و تن پوست</p></div>
<div class="m2"><p>کسی کاین رمز داند، اوستاد اوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر هر روز، تن خواهد قبائی</p></div>
<div class="m2"><p>نماند چهرهٔ جان را صفائی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر هر لحظه سر جوید کلاهی</p></div>
<div class="m2"><p>زند طبع زبون هر لحظه راهی</p></div></div>