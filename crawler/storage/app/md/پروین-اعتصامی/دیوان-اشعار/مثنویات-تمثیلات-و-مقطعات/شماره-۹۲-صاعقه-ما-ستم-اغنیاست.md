---
title: >-
    شمارهٔ  ۹۲ - صاعقهٔ ما، ستم اغنیاست
---
# شمارهٔ  ۹۲ - صاعقهٔ ما، ستم اغنیاست

<div class="b" id="bn1"><div class="m1"><p>برزگری پند به فرزند داد</p></div>
<div class="m2"><p>کای پسر، این پیشه پس از من تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدت ما جمله به محنت گذشت</p></div>
<div class="m2"><p>نوبت خون خوردن و رنج شماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشت کن آنجا که نسیم و نمی است</p></div>
<div class="m2"><p>خرمی مزرعه، ز آب و هواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانه، چو طفلی است در آغوش خاک</p></div>
<div class="m2"><p>روز و شب، این طفل به نشو و نماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میوه دهد شاخ، چو گردد درخت</p></div>
<div class="m2"><p>این هنر دایهٔ باد صباست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دولت نوروز نپاید بسی</p></div>
<div class="m2"><p>حمله و تاراج خزان در قفاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دور کن از دامن اندیشه دست</p></div>
<div class="m2"><p>از پی مقصود برو تات پاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چه کنی کشت، همان بدروی</p></div>
<div class="m2"><p>کار بد و نیک، چو کوه و صداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سبزه بهر جای که روید، خوش است</p></div>
<div class="m2"><p>رونق باغ، از گل و برگ و گیاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راستی آموز، بسی جو فروش</p></div>
<div class="m2"><p>هست در این کوی، که گندم نماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نان خود از بازوی مردم مخواه</p></div>
<div class="m2"><p>گر که تو را بازوی زور آزماست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سعی کن، ای کودک مهد امید</p></div>
<div class="m2"><p>سعی تو بنا و سعادت بناست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تجربه میبایدت اول، نه کار</p></div>
<div class="m2"><p>صاعقه در موسم خرمن، بلاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت چنین، کای پدر نیک رای</p></div>
<div class="m2"><p>صاعقهٔ ما ستم اغنیاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیشهٔ آنان، همه آرام و خواب</p></div>
<div class="m2"><p>قسمت ما، درد و غم و ابتلاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دولت و آسایش و اقبال و جاه</p></div>
<div class="m2"><p>گر حق آنهاست، حق ما کجاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قوت، بخوناب جگر میخوریم</p></div>
<div class="m2"><p>روزی ما، در دهن اژدهاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غله نداریم و گه خرمن است</p></div>
<div class="m2"><p>هیمه نداریم و زمان شتاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حاصل ما را، دگران می‌برند</p></div>
<div class="m2"><p>زحمت ما زحمت بی مدعاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از غم باران و گل و برف و سیل</p></div>
<div class="m2"><p>قامت دهقان، بجوانی دوتاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سفرهٔ ما از خورش و نان، تهی است</p></div>
<div class="m2"><p>در ده ما، بس شکم ناشتاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گه نبود روغن و گاهی چراغ</p></div>
<div class="m2"><p>خانهٔ ما، کی همه شب روشناست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زین همه گنج و زر و ملک جهان</p></div>
<div class="m2"><p>آنچه که ما راست، همین بوریاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همچو منی، زادهٔ شاهنشهی است</p></div>
<div class="m2"><p>لیک دو صد وصله، مرا بر قباست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رنجبر، ار شاه بود وقت شام</p></div>
<div class="m2"><p>باز چو شب روز شود، بی‌نواست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خرقهٔ درویش، ز درماندگی</p></div>
<div class="m2"><p>گاه لحاف است و زمانی عباست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از چه، شهان ملک ستانی کنند</p></div>
<div class="m2"><p>از چه، بیک کلبه ترا اکتفاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پای من از چیست که بی موزه است</p></div>
<div class="m2"><p>در تن تو، جامهٔ خلقان چراست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خرمن امسالهٔ ما را، که سوخت؟</p></div>
<div class="m2"><p>از چه درین دهکده قحط و غلاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در عوض رنج و سزای عمل</p></div>
<div class="m2"><p>آنچه رعیت شنود، ناسزاست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چند شود بارکش این و آن</p></div>
<div class="m2"><p>زارع بدبخت، مگر چارپاست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کار ضعیفان ز چه بی رونق است</p></div>
<div class="m2"><p>خون فقیران ز چه رو، بی بهاست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عدل، چه افتاد که منسوخ شد</p></div>
<div class="m2"><p>رحمت و انصاف، چرا کیمیاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آنکه چو ما سوخته از آفتاب</p></div>
<div class="m2"><p>چشم و دلش را، چه فروغ و ضیاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز انده این گنبد آئینه‌گون</p></div>
<div class="m2"><p>آینهٔ خاطر ما بی صفاست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آنچه که داریم ز دهر، آرزوست</p></div>
<div class="m2"><p>آنچه که بینیم ز گردون، جفاست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پیر جهاندیده بخندید کاین</p></div>
<div class="m2"><p>قصهٔ زور است، نه کار قضاست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مردمی و عدل و مساوات نیست</p></div>
<div class="m2"><p>زان، ستم و جور و تعدی رواست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گشت حق کارگران پایمال</p></div>
<div class="m2"><p>بر صفت غله که در آسیاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هیچکسی پاس نگهدار نیست</p></div>
<div class="m2"><p>این لغت از دفتر امکان جداست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پیش که مظلوم برد داوری</p></div>
<div class="m2"><p>فکر بزرگان، همه آز و هوی ست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>انجمن آنجا که مجازی بود</p></div>
<div class="m2"><p>گفتهٔ حق را، چه ثبات و بقاست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رشوه نه ما را، که بقاضی دهیم</p></div>
<div class="m2"><p>خدمت این قوم، به روی و ریاست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نبض تهی دست نگیرد طبیب</p></div>
<div class="m2"><p>درد فقیر، ای پسرک، بی دواست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ما فقرا، از همه بیگانه‌ایم</p></div>
<div class="m2"><p>مرد غنی، با همه کس آشناست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بار خود از آب برون میکشد</p></div>
<div class="m2"><p>هر کس، اگر پیرو و گر پیشواست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مردم این محکمه، اهریمنند</p></div>
<div class="m2"><p>دولت حکام، ز غصب و رباست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>آنکه سحر، حامی شرع است و دین</p></div>
<div class="m2"><p>اشک یتیمانش، گه شب غذاست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>لاشه خورانند و به آلودگی</p></div>
<div class="m2"><p>پنجهٔ آلودهٔ ایشان گواست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خون بسی پیرزنان خورده‌است</p></div>
<div class="m2"><p>آنکه بچشم من و تو، پارساست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خوابگه آنرا که سمور و خز است</p></div>
<div class="m2"><p>کی غم سرمای زمستان ماست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هر که پشیزی بگدائی دهد</p></div>
<div class="m2"><p>در طلب و نیت عمری دعاست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تیره‌دلان را چه غم از تیرگیست</p></div>
<div class="m2"><p>بی خبران را، چه خبر از خداست</p></div></div>