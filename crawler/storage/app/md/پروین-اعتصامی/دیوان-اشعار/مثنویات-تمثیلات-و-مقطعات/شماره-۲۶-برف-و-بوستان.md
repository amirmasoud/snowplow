---
title: >-
    شمارهٔ  ۲۶ - برف و بوستان
---
# شمارهٔ  ۲۶ - برف و بوستان

<div class="b" id="bn1"><div class="m1"><p>به ماه دی، گلستان گفت با برف</p></div>
<div class="m2"><p>که ما را چند حیران میگذاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسی باریده‌ای بر گلشن و راغ</p></div>
<div class="m2"><p>چه خواهد بود گر زین پس نباری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی گلبن، کفن پوشید از تو</p></div>
<div class="m2"><p>بسی کردی بخوبان سوگواری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکستی هر چه را، دیگر نپیوست</p></div>
<div class="m2"><p>زدی هر زخم، گشت آن زخم کاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزاران غنچه نشکفته بردی</p></div>
<div class="m2"><p>نوید برگ سبزی هم نیاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گستردی بساط دشمنی را</p></div>
<div class="m2"><p>هزاران دوست را کردی فراری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفت ای دوست، مهر از کینه بشناس</p></div>
<div class="m2"><p>ز ما ناید به جز تیمارخواری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزاران راز بود اندر دل خاک</p></div>
<div class="m2"><p>چه کردستیم ما جز رازداری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهر بی توشه ساز و برگ دادم</p></div>
<div class="m2"><p>نکردم هیچگه ناسازگاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهار از دکهٔ من حله گیرد</p></div>
<div class="m2"><p>شکوفه باشد از من یادگاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من آموزم درختان کهن را</p></div>
<div class="m2"><p>گهی سرسبزی و گه میوه‌داری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا هر سال، گردون میفرستد</p></div>
<div class="m2"><p>به گلزار از پی آموزگاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چمن یکسر نگارستان شد از من</p></div>
<div class="m2"><p>چرا نقش بد از من مینگاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به گل گفتم رموز دلفریبی</p></div>
<div class="m2"><p>به بلبل، داستان دوستاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز من، گلهای نوروزی شب و روز</p></div>
<div class="m2"><p>فرا گیرند درس کامکاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو من گنجور باغ و بوستانم</p></div>
<div class="m2"><p>درین گنجینه داری هر چه داری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا با خود ودیعتهاست پنهان</p></div>
<div class="m2"><p>ز دوران بدین بی اعتباری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هزاران گنج را گشتم نگهبان</p></div>
<div class="m2"><p>بدین بی پائی و ناپایداری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دل و دامن نیالودم به پستی</p></div>
<div class="m2"><p>بری بودم ز ننگ بد شعاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپیدم زان سبب کردند در بر</p></div>
<div class="m2"><p>که باشد جامهٔ پرهیزکاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قضا بس کار بشمرد و بمن داد</p></div>
<div class="m2"><p>هزاران کار کردم گر شماری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برای خواب سرو و لاله و گل</p></div>
<div class="m2"><p>چه شبها کرده‌ام شب زنده‌داری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به خیری گفتم اندر وقت سرما</p></div>
<div class="m2"><p>که میل خواب داری؟ گفت آری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به بلبل گفتم اندر لانه بنشین</p></div>
<div class="m2"><p>که ایمن باشی از باز شکاری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو نسرین اوفتاد از پای، گفتم</p></div>
<div class="m2"><p>که باید صبر کرد و بردباری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شکستم لاله را ساغر، که دیگر</p></div>
<div class="m2"><p>ننوشد می بوقت هوشیاری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فشردم نرگس مخمور را گوش</p></div>
<div class="m2"><p>که تا بیرون کند از سر خماری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو سوسن خسته شد گفتم چه خواهی</p></div>
<div class="m2"><p>بگفت ار راست باید گفت، یاری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز برف آماده گشت آب گوارا</p></div>
<div class="m2"><p>گوارائی رسد زین ناگواری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بهار از سردی من یافت گرمی</p></div>
<div class="m2"><p>منش دادم کلاه شهریاری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه گندم داشت برزیگر، نه خرمن</p></div>
<div class="m2"><p>نمیکردیم گر ما پرده‌داری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر یکسال گردد خشک‌سالی</p></div>
<div class="m2"><p>زبونی باشد و بد روزگاری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از این پس، باغبان آید به گلشن</p></div>
<div class="m2"><p>مرا بگذشت وقت آبیاری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>روان آید به جسم، این مردگانرا</p></div>
<div class="m2"><p>ز باران و ز باد نو بهاری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>درختان، برگ و گل آرند یکسر</p></div>
<div class="m2"><p>بدل بر فربهی گردد نزاری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بچهر سرخ گل، روشن کنی چشم</p></div>
<div class="m2"><p>نه بیهوده است این چشم انتظاری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نثارم گل، ره آوردم بهار است</p></div>
<div class="m2"><p>ره‌آورد مرا هرگز نیاری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عروس هستی از من یافت زیور</p></div>
<div class="m2"><p>تو اکنون از منش کن خواستگاری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خبر ده بر خداوندان نعمت</p></div>
<div class="m2"><p>که ما کردیم این خدمتگزاری</p></div></div>