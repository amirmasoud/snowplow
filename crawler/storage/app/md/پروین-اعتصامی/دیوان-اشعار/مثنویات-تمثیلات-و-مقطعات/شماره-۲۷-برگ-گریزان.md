---
title: >-
    شمارهٔ  ۲۷ - برگ گریزان
---
# شمارهٔ  ۲۷ - برگ گریزان

<div class="b" id="bn1"><div class="m1"><p>شنیدستم که وقت برگریزان</p></div>
<div class="m2"><p>شد از باد خزان، برگی گریزان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان شاخه‌ها خود را نهان داشت</p></div>
<div class="m2"><p>رخ از تقدیر، پنهان چون توان داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخود گفتا کازین شاخ تنومند</p></div>
<div class="m2"><p>قضایم هیچگه نتواند افکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سموم فتنه کرد آهنگ تاراج</p></div>
<div class="m2"><p>ز تنها سر، ز سرها دور شد تاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قبای سرخ گل دادند بر باد</p></div>
<div class="m2"><p>ز مرغان چمن برخاست فریاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بن برکند گردون بس درختان</p></div>
<div class="m2"><p>سیه گشت اختر بس نیکبختان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یغما رفت گیتی را جوانی</p></div>
<div class="m2"><p>کرا بود این سعادت جاودانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز نرگس دل، ز نسرین سر شکستند</p></div>
<div class="m2"><p>ز قمری پا، ز بلبل پر شکستند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برفت از روی رونق بوستان را</p></div>
<div class="m2"><p>چه دولت بی گلستان باغبان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز جانسوز اخگری برخاست دودی</p></div>
<div class="m2"><p>نه تاری ماند زان دیبا، نه پودی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخود هر شاخه‌ای لرزید ناگاه</p></div>
<div class="m2"><p>فتاد آن برگ مسکین بر سر راه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن افتادن بیگه، برآشفت</p></div>
<div class="m2"><p>نهان با شاخک پژمان چنین گفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که پروردی مرا روزی در آغوش</p></div>
<div class="m2"><p>بروز سختیم کردی فراموش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشاندی شاد چون طفلان بمهدم</p></div>
<div class="m2"><p>زمانی شیردادی، گاه شهدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخاک افتادنم روزی چرا بود</p></div>
<div class="m2"><p>نه آخر دایه‌ام باد صبا بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هنوز از شکر نیکیهات شادم</p></div>
<div class="m2"><p>چرا بی موجبی دادی به بادم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هنرهای تو نیرومندیم داد</p></div>
<div class="m2"><p>ره و رسم خوشت، خورسندیم داد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گمان میکردم ای یار دلارای</p></div>
<div class="m2"><p>که از سعی تو باشم پای بر جای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چرا پژمرده گشت این چهر شاداب</p></div>
<div class="m2"><p>چه شد کز من گرفتی رونق و آب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیاد رنج روز تنگدستی</p></div>
<div class="m2"><p>خوشست از زیردستان سرپرستی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نمودی همسر خوبان با غم</p></div>
<div class="m2"><p>ز طیب گل، بیاکندی دماغم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنون بگسستیم پیوند یاری</p></div>
<div class="m2"><p>ز خورشید و ز باران بهاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دمی کاز باد فروردین شکفتم</p></div>
<div class="m2"><p>بدامان تو روزی چند خفتم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نسیمی دلکشم آهسته بنشاند</p></div>
<div class="m2"><p>مرا بر تن، حریر سبز پوشاند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من آنگه خرم و فیروز بودم</p></div>
<div class="m2"><p>نخستین مژدهٔ نوروز بودم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نویدی داد هر مرغی ز کارم</p></div>
<div class="m2"><p>گهرها کرد هر ابری نثارم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرفتم داشتم فرخنده نامی</p></div>
<div class="m2"><p>چه حاصل، زیستم صبحی و شامی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگفتا بس نماند برگ بر شاخ</p></div>
<div class="m2"><p>حوادث را بود سر پنجه گستاخ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو شاهین قضا را تیز شد چنگ</p></div>
<div class="m2"><p>نه از صلحت رسد سودی نه از جنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو ماند شبرو ایام بیدار</p></div>
<div class="m2"><p>نه مست اندر امان باشد، نه هشیار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهان را هر دم آئینی و رائی است</p></div>
<div class="m2"><p>چمن را هم سموم و هم صبائی است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ترا از شاخکی کوته فکندند</p></div>
<div class="m2"><p>ولیک از بس درختان ریشه کندند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو از تیر سپهر ار باختی رنگ</p></div>
<div class="m2"><p>مرا نیز افکند دست جهان سنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نخواهد ماند کس دائم بیک حال</p></div>
<div class="m2"><p>گل پارین نخواهد رست امسال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ندارد عهد گیتی استواری</p></div>
<div class="m2"><p>چه خواهی کرد غیر از سازگاری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ستمکاری، نخست آئین گرگست</p></div>
<div class="m2"><p>چه داند بره کوچک یا بزرگست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو همچون نقطه، درمانی درین کار</p></div>
<div class="m2"><p>که چون میگردد این فیروزه پرگار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه تنها بر تو زد گردون شبیخون</p></div>
<div class="m2"><p>مرا نیز از دل و دامن چکد خون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جهانی سوخت ز اسیب تگرگی</p></div>
<div class="m2"><p>چه غم کاز شاخکی افتاد برگی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو تیغ مهرگانی بر ستیزد</p></div>
<div class="m2"><p>ز شاخ و برگ، خون ناب ریزد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بساط باغ را بی گل صفا نیست</p></div>
<div class="m2"><p>تو برگی، برگ را چندان بها نیست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو گل یکهفته ماند و لاله یکروز</p></div>
<div class="m2"><p>نزیبد چون توئی را ناله و سوز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو آن گنجینه را گلشن شد از دست</p></div>
<div class="m2"><p>چه غم گر برگ خشکی نیست یا هست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرا از خویشتن برتر مپندار</p></div>
<div class="m2"><p>تو بشکستی، مرا بشکست بازار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کجا گردن فرازد شاخساری</p></div>
<div class="m2"><p>که بر سر نیستش برگی و باری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نماند بر بلندی هیچ خودخواه</p></div>
<div class="m2"><p>درافتد چون تو روزی بر گذرگاه</p></div></div>