---
title: >-
    شمارهٔ  ۱۷ - امید و نومیدی
---
# شمارهٔ  ۱۷ - امید و نومیدی

<div class="b" id="bn1"><div class="m1"><p>به نومیدی، سحرگه گفت امید</p></div>
<div class="m2"><p>که کس ناسازگاری چون تو نشنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هرسو دست شوقی بود بستی</p></div>
<div class="m2"><p>به هرجا خاطری دیدی شکستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشیدی بر در هر دل سپاهی</p></div>
<div class="m2"><p>ز سوزی، ناله‌ای، اشکی و آهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبونی هر چه هست و بود از تست</p></div>
<div class="m2"><p>بساط دیده اشک‌آلود از تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس است این کار بی‌تدبیر کردن</p></div>
<div class="m2"><p>جوانان را به حسرت پیر کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدین تلخی ندیدم زندگانی</p></div>
<div class="m2"><p>بدین بی‌مایگی بازارگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهی بر پای هر آزاده بندی</p></div>
<div class="m2"><p>رسانی هر وجودی را گزندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به اندوهی بسوزی خرمنی را</p></div>
<div class="m2"><p>کشی از دست مهری دامنی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غبارت چشم را تاریکی آموخت</p></div>
<div class="m2"><p>شرارت ریشهٔ اندیشه را سوخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوصد راه هوس را چاه کردی</p></div>
<div class="m2"><p>هزاران آرزو را آه کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز امواج تو ایمن، ساحلی نیست</p></div>
<div class="m2"><p>ز تاراج تو فارغ، حاصلی نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا در هر دلی، خوش جایگاهیست</p></div>
<div class="m2"><p>به سوی هر ره تاریک راهیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دهم آزردگان را مومیایی</p></div>
<div class="m2"><p>شوم در تیرگی‌ها روشنایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلی را شاد دارم با پیامی</p></div>
<div class="m2"><p>نشانم پرتوی را با ظلامی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عروس وقت را آرایش از ماست</p></div>
<div class="m2"><p>بنای عشق را پیدایش از ماست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غمی را ره ببندم با سروری</p></div>
<div class="m2"><p>سلیمانی پدید آرم ز موری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به هر آتش، گلستانی فرستم</p></div>
<div class="m2"><p>به هر سرگشته، سامانی فرستم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خوش آن رمزی که عشقی را نوید است</p></div>
<div class="m2"><p>خوش آن دل کاندران نور امید است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفت ای دوست، گردش‌های دوران</p></div>
<div class="m2"><p>شما را هم کند چون ما پریشان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرا با روشنایی نیست کاری</p></div>
<div class="m2"><p>که ماندم در سیاهی روزگاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه یکسانند نومیدی و امید</p></div>
<div class="m2"><p>جهان بگریست بر من، بر تو خندید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در آن مدت که من امید بودم</p></div>
<div class="m2"><p>بکردار تو خود را می‌ستودم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرا هم بود شادی‌ها، هوس‌ها</p></div>
<div class="m2"><p>چمن‌ها، مرغ‌ها، گل‌ها، قفس‌ها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا دل‌ سردی ایام بگداخت</p></div>
<div class="m2"><p>همان ناسازگاری، کار من ساخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چراغ شب ز باد صبحگه مرد</p></div>
<div class="m2"><p>گل دوشینه یک شب ماند و پژمرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سیاهی‌های محنت جلوه‌ام برد</p></div>
<div class="m2"><p>درشتی دیدم و گشتم چنین خرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شبانگه در دلی تنگ آرمیدم</p></div>
<div class="m2"><p>شدم اشکی و از چشمی چکیدم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ندیدم ناله‌ای بودم سحرگاه</p></div>
<div class="m2"><p>شکنجی دیدم و گشتم یکی آه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو بنشین در دلی کاز غم بود پاک</p></div>
<div class="m2"><p>خوشند آری مرا دل‌های غمناک</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو گوی از دست ما بردند فرجام</p></div>
<div class="m2"><p>چه فرق ار اسب توسن بود یا رام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گذشت امید و چون برقی درخشید</p></div>
<div class="m2"><p>هماره کی درخشد برق امید</p></div></div>