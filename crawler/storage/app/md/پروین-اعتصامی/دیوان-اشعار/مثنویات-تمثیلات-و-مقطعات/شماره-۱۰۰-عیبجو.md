---
title: >-
    شمارهٔ  ۱۰۰ - عیبجو
---
# شمارهٔ  ۱۰۰ - عیبجو

<div class="b" id="bn1"><div class="m1"><p>زاغی به طرف باغ، به طاوس طعنه زد</p></div>
<div class="m2"><p>کاین مرغ زشت روی، چه خودخواه و خودنماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این خط و خال را نتوان گفت دلکش است</p></div>
<div class="m2"><p>این زیب و رنگ را نتوان گفت دلرباست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پایش کج است و زشت، ازان کج رود براه</p></div>
<div class="m2"><p>دمش چو دم روبه و رنگش چو کهرباست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوکش، چو نوک بوم سیه‌کار، منحنی است</p></div>
<div class="m2"><p>پشت سرش برآمده و گردنش دوتاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از فرط عجب و جهل، گمان میبرد که اوست</p></div>
<div class="m2"><p>تنها پرنده‌ای که در این عرصه و فضاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این جانور نه لایق باغ است و بوستان</p></div>
<div class="m2"><p>این بی‌هنر، نه در خور این مدحت و ثناست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسم و رهیش نیست، به جز حرص و خودسری</p></div>
<div class="m2"><p>از پا فتادهٔ هوس و کشتهٔ هوی‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طاوس خنده کرد که رای تو باطل است</p></div>
<div class="m2"><p>هرگز نگفته است بداندیش، حرف راست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردم همیشه نقش خوش ما ستوده‌اند</p></div>
<div class="m2"><p>هرگز دلیل را نتوان گفت، ادعاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدگوئی تو اینهمه، از فرط بددلی است</p></div>
<div class="m2"><p>از قلب پاک، نیت آلوده بر نخاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما عیب خود، هنر نشمردیم هیچگاه</p></div>
<div class="m2"><p>در عیب خویش، ننگرد آنکس که خودستاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گاه خرام و جلوه بنزهتگه چمن</p></div>
<div class="m2"><p>چشمم ز راه شرم و تاسف، بسوی پاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما جز نصیب خویش نخوردیم، لیک زاغ</p></div>
<div class="m2"><p>دزدی کند بهر گذر و باز ناشتاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در من چه عیب دیده کسی غیر پای زشت</p></div>
<div class="m2"><p>نقص و خرابی و کژی دیگرم کجاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیرایه‌ای بعمد، نبستم ببال و پر</p></div>
<div class="m2"><p>آرایش وجود من، ای دوست، بی‌ریاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ما بهر زیب و رنگ، نکردیم گفتگو</p></div>
<div class="m2"><p>چیزی نخواستیم، فلک داد آنچه خواست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کارآگهی که آب و گل ما بهم سرشت</p></div>
<div class="m2"><p>بر من فزود، آنچه که از خلقت تو کاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در هر قبیله بیش و کم و خوب و زشت هست</p></div>
<div class="m2"><p>مرغی کلاغ لاشخور و دیگری هماست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صد سال گر بدجله بشویند زاغ را</p></div>
<div class="m2"><p>چون بنگری، همان سیه زشت بینواست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هرگز پر تو را چو پر من نمی‌کنند</p></div>
<div class="m2"><p>مرغی که چون منش پر زیباست مبتلاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آزادی تو را نگرفت از تو، هیچ کس</p></div>
<div class="m2"><p>ما را همیشه دیدهٔ صیاد در قفاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرمانده سپهر، چو حکمی نوشت و داد</p></div>
<div class="m2"><p>کس دم نمیزند که صوابست یا خطاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ما را برای مشورت، اینجا نخوانده‌اند</p></div>
<div class="m2"><p>از ما و فکر ما، فلک پیر را غناست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>احمق، کتاب دید و گمان کرد عالم است</p></div>
<div class="m2"><p>خودبین، بکشتی آمد و پنداشت ناخداست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ما زشت نیستیم، تو صاحب نظر نه‌ای</p></div>
<div class="m2"><p>این خوردگیری، از نظر کوته شماست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>طاوس را چه جرم، اگر زاغ زشت روست</p></div>
<div class="m2"><p>این رمزها به دفتر مستوفی قضاست</p></div></div>