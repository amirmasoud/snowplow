---
title: >-
    شمارهٔ  ۱۰۳ - فریاد حسرت
---
# شمارهٔ  ۱۰۳ - فریاد حسرت

<div class="b" id="bn1"><div class="m1"><p>فتاد طائری از لانه و ز درد تپید</p></div>
<div class="m2"><p>بزیر پر چو نگه کرد، دید پیکانی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفت، آنکه بدریای خون فکند مرا</p></div>
<div class="m2"><p>ندید در دل شوریده‌ام چه طوفانی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسیکه بر رگ من تیر زد، نمیدانست</p></div>
<div class="m2"><p>که قلب خرد مرا هم ورید و شریانی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ربود مرغکم از زیر پر بعنف و نگفت</p></div>
<div class="m2"><p>که مادری و پرستاری و نگهبانی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیر کردن و کشتن، تفرج و بازی است</p></div>
<div class="m2"><p>نشانه کردن مظلوم، کار آسانی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بام خرد گل اندود پست ما، پیداست</p></div>
<div class="m2"><p>که سقف خانهٔ جمعیت پریشانی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکست پنجه و منقار من، ولیک چه باک</p></div>
<div class="m2"><p>پلنگ حادثه را نیز چنگ و دندانی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفتم آنکه بپایان رسید، فرصت ما</p></div>
<div class="m2"><p>برای فرصت صیاد نیز، پایانی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فتاد پایه، چنین خانه را چه تعمیری است</p></div>
<div class="m2"><p>گداخت سینه، چنین درد را چه درمانی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چمن خوش است و جهان سبز و بوستان خرم</p></div>
<div class="m2"><p>برای طائر آزاد، جای جولانی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمانه عرصه برای ضعیف، تنگ گرفت</p></div>
<div class="m2"><p>هماره بهر توانا، فراخ میدانی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همیشه خانهٔ بیداد و جور، آباد است</p></div>
<div class="m2"><p>بساط ماست که ویران ز باد و بارانی است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نگفته ماند سخنهای من، خوشا مرغی</p></div>
<div class="m2"><p>که لانه‌اش گه سعی و عمل، دبستانی است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا هر آنکه در افکند همچو گوی بسر</p></div>
<div class="m2"><p>خبر نداشت که در دست دهر چوگانی است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز رنج بی سر و سامانی منش چه غم است</p></div>
<div class="m2"><p>همین بس است که او را سری و سامانی است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حدیث نیک و بد ما نوشته خواهد شد</p></div>
<div class="m2"><p>زمانه را سند و دفتری و دیوانی است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کسی ز درد من آگه نشد، ولیک خوشم</p></div>
<div class="m2"><p>که چند قطرهٔ خونم، بدست و دامانی است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هزار کاخ بلند، ار بنا کند صیاد</p></div>
<div class="m2"><p>بهای خار و خس آشیان ویرانی است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه لانه‌ای و چه قصری، اساس خانه یکی است</p></div>
<div class="m2"><p>بشهر کوچک خود، مور هم سلیمانی است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز دهر، گر دل تنگم فشار دید چه غم</p></div>
<div class="m2"><p>گرفته دست قضا، هر کجا گریبانی است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه برتریست ندانم بمرغ، مردم را</p></div>
<div class="m2"><p>جز اینکه دعوی باطل کند که انسانی است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درین قبیلهٔ خودخواه، هیچ شقفت نیست</p></div>
<div class="m2"><p>چو نیک درنگری، هر چه هست عنوانی است</p></div></div>