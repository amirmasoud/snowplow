---
title: >-
    شمارهٔ  ۱۴۳ - معمار نادان
---
# شمارهٔ  ۱۴۳ - معمار نادان

<div class="b" id="bn1"><div class="m1"><p>دید موری طاسک لغزنده‌ای</p></div>
<div class="m2"><p>از سر تحقیر، زد لبخنده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاین ره از بیرون همه پیچ و خم است</p></div>
<div class="m2"><p>وز درون، تاریکی و دود و دم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فصل باران است و برف و سیل و باد</p></div>
<div class="m2"><p>ناگه این دیوار خواهد اوفتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که در این خانه صاحبخانه‌ای</p></div>
<div class="m2"><p>هر که هستی، از خرد بیگانه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست، میدانم ترا انبار و توش</p></div>
<div class="m2"><p>پس چه خواهی خوردن، ای بی‌عقل و هوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از برای کار خود، پائی بزن</p></div>
<div class="m2"><p>نوبت تدبیر شد، رائی بزن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زندگانی، جز معمائی نبود</p></div>
<div class="m2"><p>وقت، غیر از خوان یغمائی نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نپیمائی ره سعی و عمل</p></div>
<div class="m2"><p>این معما را نخواهی کرد حل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کجا راهی است، ما پیموده‌ایم</p></div>
<div class="m2"><p>هر کجا توشی است، آنجا بوده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو ز اول سست کردی پایه را</p></div>
<div class="m2"><p>سود، اندک بود اندک مایه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست خالی، دوش ما از بار ما</p></div>
<div class="m2"><p>کوشش اندر دست ما، افزار ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر به سیر و گشت، می‌پرداختیم</p></div>
<div class="m2"><p>از کجا آن لانه را می‌ساختیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر که توشی گرد کرد، او چاشت خورد</p></div>
<div class="m2"><p>هر که زیرک بود، او زد دستبرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دستبردی زد زمانه هر نفس</p></div>
<div class="m2"><p>دستبردی هم تو زن، ای بوالهوس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آخر، این سرچشمه خواهد شد خراب</p></div>
<div class="m2"><p>در سبوی خویش، باید داشت آب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سرد میگردد تنور آسمان</p></div>
<div class="m2"><p>در تنور گرم، باید پخت نان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مور، تا پی داشت در پا، سرفشاند</p></div>
<div class="m2"><p>چون تو، اندر گوشهٔ عزلت نماند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مادر من، گفت در طفلی بمن</p></div>
<div class="m2"><p>رو، بکوش از بهر قوت خویشتن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کس نخواهد بعد ازین، بار تو برد</p></div>
<div class="m2"><p>جنس ما را نیست، خرد و سالخورد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بس بزرگست این وجود خرد ما</p></div>
<div class="m2"><p>وقت دارد کار و خواب و خورد ما</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خرد بودیم و بزرگی خواستیم</p></div>
<div class="m2"><p>هم در افتادیم و هم برخاستیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مور خوارش گفت، کای یار عزیز</p></div>
<div class="m2"><p>گر تو نقاشی، بیا طرحی بریز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیک دانستم که اندر دوستی</p></div>
<div class="m2"><p>همچو مغز خالص بی پوستی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یک نفس، بنای این دیوار باش</p></div>
<div class="m2"><p>در خرابیهای ما، معمار باش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این بنا را ساختیم، اما چه سود</p></div>
<div class="m2"><p>خانهٔ بی صحن و سقف و بام بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مهرهٔ تدبیر، دور انداختیم</p></div>
<div class="m2"><p>زان سبب، بردی تو و ما باختیم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کیست ما را از تو خیراندیش تر</p></div>
<div class="m2"><p>کاشکی می‌آمدی زین پیشتر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر باین ویرانه، آبادی دهی</p></div>
<div class="m2"><p>در حقیقت، داد استادی دهی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فکر ما، تعمیر این بام و فضاست</p></div>
<div class="m2"><p>هر چه پیش آید جز این، کار قضاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو طبیب حاذق و ما دردمند</p></div>
<div class="m2"><p>ما در این پستی، تو در جای بلند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا که بر می‌آیدت کاری ز دست</p></div>
<div class="m2"><p>رونقی ده، گر که بازاری شکست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مور مغرور، این حکایت چون شنید</p></div>
<div class="m2"><p>گفت، تا زود است باید رفت و دید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پای اندر ره نهاد، آمد فرود</p></div>
<div class="m2"><p>گر چه رفتن بود و برگشتن نبود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کار را دشوار دید، از کار ماند</p></div>
<div class="m2"><p>در عجب زان راه ناهموار ماند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مور طفل، اما حوادث پیر بود</p></div>
<div class="m2"><p>احتمال چاره‌جوئی دیر بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دام محکم، ضعف در حد کمال</p></div>
<div class="m2"><p>ایستادن سخت و برگشتن محال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از برای پایداری، پای نه</p></div>
<div class="m2"><p>بهر صبر و بردباری، جای نه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چونکه دید آن صید مسکین، مور خوار</p></div>
<div class="m2"><p>گفت: گر کارآگهی، اینست کار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خانهٔ ما را نمیکردی پسند</p></div>
<div class="m2"><p>بد پسند است، این وجود آزمند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو بدین طفلی، که گفت استاد شو</p></div>
<div class="m2"><p>باد افکن در سر و بر باد شو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خوب لغزیدی و گشتی سرنگون</p></div>
<div class="m2"><p>خوب خواهیمت مکید، این لحظه خون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بسکه از معماری خود، دم زدی</p></div>
<div class="m2"><p>خانهٔ تدبیر را، بر هم زدی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دام را اینگونه باید ساختن</p></div>
<div class="m2"><p>چون تو خودبین را بدام انداختن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عیب کردی، این ره لغزنده را</p></div>
<div class="m2"><p>طاس را دیدی، ندیدی بنده را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من هزاران چون تو را دادم فریب</p></div>
<div class="m2"><p>زان فریب، آگه شوی عما قریب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هیچ پرسیدی که صاحبخانه کیست</p></div>
<div class="m2"><p>هیچ گفتی در پس این پرده چیست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دیده را بستی و افتادی بچاه</p></div>
<div class="m2"><p>ره شناسا، این تو و این پرتگاه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>طاس لغزنده است، ای دل، آز تو</p></div>
<div class="m2"><p>مبتلائی، گر شود دمساز تو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زین حکایت، قصهٔ خود گوشدار</p></div>
<div class="m2"><p>تو چو موری و هوی چون مورخوار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چون شدی سرگشته در تیه نیاز</p></div>
<div class="m2"><p>با خبر باش از نشیب و از فراز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا که این روباه رنگین کرد دم</p></div>
<div class="m2"><p>بس خروس از خانه‌داران گشت گم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پا منه بیرون ز خط احتیاط</p></div>
<div class="m2"><p>تا چو طومارت، نپیچاند بساط</p></div></div>