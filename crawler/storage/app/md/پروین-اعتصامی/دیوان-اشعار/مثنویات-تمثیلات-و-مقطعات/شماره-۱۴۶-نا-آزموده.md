---
title: >-
    شمارهٔ  ۱۴۶ - نا آزموده
---
# شمارهٔ  ۱۴۶ - نا آزموده

<div class="b" id="bn1"><div class="m1"><p>قاضی بغداد، شد بیمار سخت</p></div>
<div class="m2"><p>از عدالتخانه بیرون برد رخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هفته‌ها در دام تب، چون صید ماند</p></div>
<div class="m2"><p>محضرش، خالی ز عمرو زید ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدعی، دیگر نیامد بر درش</p></div>
<div class="m2"><p>ماند گرد آلود، مهر و دفترش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دادخواه و مردم بیدادگر</p></div>
<div class="m2"><p>هر دو، رو کردند بر جای دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن دکان عجب شد بی مشتری</p></div>
<div class="m2"><p>دیگری برداشت کار داوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدتی، قاضی ز کسب و کار ماند</p></div>
<div class="m2"><p>آن متاع زرق، بی بازار ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس نمیورد دیگر نامه‌ای</p></div>
<div class="m2"><p>بره‌ای، قندی، خروسی، جامه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیمه‌شب، دیگر کسی بر در نبود</p></div>
<div class="m2"><p>صحبتی از بدره‌های زر نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از کسی، دیگر نیامد پیشکش</p></div>
<div class="m2"><p>از میان برخاست، صلح و کشمکش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مانده بود از گردش دوران، عقیم</p></div>
<div class="m2"><p>حرف قیم، دعوی طفل یتیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر نمیورد بزاز دغل</p></div>
<div class="m2"><p>طاقهٔ کشمیری، از زیر بغل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زر، دگر ننهاد مرد کم فروش</p></div>
<div class="m2"><p>زیر مسند، تا شود قاضی خموش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون همی نیروش کم شد، ضعف بیش</p></div>
<div class="m2"><p>عاقبت روزی، پسر را خواند پیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت، دکان مرا ایام بست</p></div>
<div class="m2"><p>دیگرم کاری نمی‌آید ز دست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو بمسند برنشین جای پدر</p></div>
<div class="m2"><p>هر چه من بردم، تو بعد از من ببر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر چه باشد، باز نامش مسند است</p></div>
<div class="m2"><p>گر زیانش ده بود، سودش صد است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر بدانی راه و رسم کار را</p></div>
<div class="m2"><p>گرم خواهی کرد این بازار را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سالها اندر دبستان بوده‌ای</p></div>
<div class="m2"><p>بس کتاب و بس قلم فرسوده‌ای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آگهی، از حکم و از فتوای من</p></div>
<div class="m2"><p>از سخنها و اشارتهای من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کار دیوانخانه، میدانی که چیست</p></div>
<div class="m2"><p>وانکه میبایست بارش برد، کیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو بسی در محضر من مانده‌ای</p></div>
<div class="m2"><p>هر چه در دفتر نوشتم، خوانده‌ای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خوش گذشت از صید خلق، ایام من</p></div>
<div class="m2"><p>ای پسر، دامی بنه چون دام من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حق بر آنکس ده که میدانی غنی است</p></div>
<div class="m2"><p>گر سراپا حق بود مفلس، دنی است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حرف ظالم، هر چه گوید می‌پذیر</p></div>
<div class="m2"><p>هر چه از مظلوم میخواهی بگیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گاه باید زد به میخ و گه به نعل</p></div>
<div class="m2"><p>گر سند خواهند، باید کرد جعل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در رواج کار خود، چون من بکوش</p></div>
<div class="m2"><p>هر که را پر شیرتر بینی، بدوش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت، آری، داوری نیکو کنم</p></div>
<div class="m2"><p>خدمت هر کس بقدر او کنم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صبحگاهان رفت و در محضر نشست</p></div>
<div class="m2"><p>شامگه برگشت، خون آلوده دست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفت، چون رفتم بمحضر صبحگاه</p></div>
<div class="m2"><p>روستائی زاده‌ای آمد ز راه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کرد نفرین بر کسان کدخدای</p></div>
<div class="m2"><p>که شبانگه ریختندم در سرای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خانه‌ام از جورشان ویرانه شد</p></div>
<div class="m2"><p>کودک شش ساله‌ام، دیوانه شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روغنم بردند و خرمن سوختند</p></div>
<div class="m2"><p>بره‌ام کشتند و بز بفروختند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر که این محضر برای داوری است</p></div>
<div class="m2"><p>دید باید، کاین چه ظلم و خودسری است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفتم این فکر محال از سر بنه</p></div>
<div class="m2"><p>داوری گر نیک خواهی، زر بده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفت، دیناری مرا در کار نیست</p></div>
<div class="m2"><p>گفتمش، کمتر ز صد دینار نیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من همی گفتم بده، او گفت نی</p></div>
<div class="m2"><p>او همی رفت و منش رفتم ز پی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون درشتی کرد با من، کشتمش</p></div>
<div class="m2"><p>قصه کوته گشت، رو در هم مکش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر تو میبودی به محضر، جای من</p></div>
<div class="m2"><p>همچو من، کوته نمیکردی سخن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چونکه زر میخواستی و زر نداشت</p></div>
<div class="m2"><p>گفته‌های او اثر دیگر نداشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خیره سر میخواندی و دیوانه‌اش</p></div>
<div class="m2"><p>میفرستادی به زندانخانه‌اش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو، به پنبه میبری سر، ای پدر</p></div>
<div class="m2"><p>من به تیغ این کار کردم مختصر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن چنان کردم که تو میخواستی</p></div>
<div class="m2"><p>راستی این بود و گفتم راستی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زرشناسان، چون خدا نشناختند</p></div>
<div class="m2"><p>سنگشان هر جا که رفت انداختند</p></div></div>