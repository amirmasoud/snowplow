---
title: >-
    شمارهٔ  ۹۸ - عمر گل
---
# شمارهٔ  ۹۸ - عمر گل

<div class="b" id="bn1"><div class="m1"><p>سحرگه، غنچه‌ای در طرف گلزار</p></div>
<div class="m2"><p>ز نخوت، بر گلی خندید بسیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که، ای پژمرده، روز کامرانی است</p></div>
<div class="m2"><p>بهار و باغ را فصل جوانی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشاید در چمن، دلتنگ بودن</p></div>
<div class="m2"><p>بدین رنگ و صفا، بی رنگ بودن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشاط آرد هوای مرغزاران</p></div>
<div class="m2"><p>چو نور صبحگاهی در بهاران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو نیز آمادهٔ نشو و نما باش</p></div>
<div class="m2"><p>برنگ و جلوه و خوبی، چو ما باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر ما هر دو را یک باغبان کشت</p></div>
<div class="m2"><p>چرا گشتیم ما زیبا، شما زشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیفروز از فروغ خود، چمن را</p></div>
<div class="m2"><p>مکاه، ای دوست، قدر خویشتن را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفتا، هیچ گل در طرف بستان</p></div>
<div class="m2"><p>نماند جاودان شاداب و خندان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا هم بود، روزی رنگ و بوئی</p></div>
<div class="m2"><p>صفائی، جلوه‌ای، پاکیزه‌روئی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپهر، این باغ بس کردست یغما</p></div>
<div class="m2"><p>من امروزم بدین خواری، تو فردا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو گل یک لحظه ماند، غنچه یک دم</p></div>
<div class="m2"><p>چه شادی در صف گلشن، چه ماتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا باید دگر ترک چمن گفت</p></div>
<div class="m2"><p>گل پژمرده، دیگر بار نشکفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترا خوش باد، با خوبان نشستن</p></div>
<div class="m2"><p>که ما را باید اینک رخت بستن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مزن بیهود چندین طعنه ما را</p></div>
<div class="m2"><p>ببند، ار زیرکی، دست قضا را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو خواهد چرخ یغماگر زبونت</p></div>
<div class="m2"><p>کند باد حوادث واژگونت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بهر شاخی که روید تازه برگی</p></div>
<div class="m2"><p>شود تاراج بادی یا تگرگی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گل آن خوشتر که جز روزی نماند</p></div>
<div class="m2"><p>چو ماند، هیچکش قدرش نداند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بهستی، خوش بود دامن فشاندن</p></div>
<div class="m2"><p>گلی زیبا شدن، یک لحظه ماندن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گل خوشبوی را گرم است بازار</p></div>
<div class="m2"><p>نماند رنگ و بو، چون رفت رخسار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تبه گردید فرصت خستگان را</p></div>
<div class="m2"><p>برو، هشیار کن نو رستگان را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه نامی، چون نماند از من نشانی</p></div>
<div class="m2"><p>چه جان بخشی، چو باقی نیست جانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کسی کش دایهٔ گیتی دهد شیر</p></div>
<div class="m2"><p>شود هم در زمان کودکی پیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو این پیمانه را ساقی است گردون</p></div>
<div class="m2"><p>بباید خورد، گر شهد است و گر خون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از آن دفتر که نام ما زدودند</p></div>
<div class="m2"><p>شما را صفحهٔ دیگر گشودند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ازین پژمردگی، ما را غمی نیست</p></div>
<div class="m2"><p>که گل را زندگانی جز دمی نیست</p></div></div>