---
title: >-
    شمارهٔ  ۵۱ - حقیقت و مجاز
---
# شمارهٔ  ۵۱ - حقیقت و مجاز

<div class="b" id="bn1"><div class="m1"><p>بلبلی شیفته میگفت به گل</p></div>
<div class="m2"><p>که جمال تو چراغ چمن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت، امروز که زیبا و خوشم</p></div>
<div class="m2"><p>رخ من شاهد هر انجمن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونکه فردا شد و پژمرده شدم</p></div>
<div class="m2"><p>کیست آنکس که هواخواه من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بتن، این پیرهن دلکش من</p></div>
<div class="m2"><p>چو گه شام بیائی، کفن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حرف امروز چه گوئی، فرداست</p></div>
<div class="m2"><p>که تو را بر گل دیگر وطن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه جا بوی خوش و روی نکوست</p></div>
<div class="m2"><p>همه جا سرو و گل و یاسمن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق آنست که در دل گنجد</p></div>
<div class="m2"><p>سخن است آنکه همی بر دهن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر معشوقه بمیرد عاشق</p></div>
<div class="m2"><p>کار باید، سخن است این، سخن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میشناسیم حقیقت ز مجاز</p></div>
<div class="m2"><p>چون تو، بسیار درین نارون است</p></div></div>