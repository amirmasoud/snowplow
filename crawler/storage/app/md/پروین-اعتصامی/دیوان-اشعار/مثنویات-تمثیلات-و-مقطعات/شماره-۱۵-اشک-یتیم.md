---
title: >-
    شمارهٔ  ۱۵ - اشک یتیم
---
# شمارهٔ  ۱۵ - اشک یتیم

<div class="b" id="bn1"><div class="m1"><p>روزی گذشت پادشهی از گذرگهی</p></div>
<div class="m2"><p>فریاد شوق بر سر هر کوی و بام خاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرسید زان میانه یکی کودک یتیم</p></div>
<div class="m2"><p>کاین تابناک چیست که بر تاج پادشاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن یک جواب داد چه دانیم ما که چیست</p></div>
<div class="m2"><p>پیداست آنقدر که متاعی گرانبهاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزدیک رفت پیرزنی گوژپشت و گفت</p></div>
<div class="m2"><p>این اشک دیدهٔ من و خون دل شماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را به رخت و چوب شبانی فریفته است</p></div>
<div class="m2"><p>این گرگ سالهاست که با گله آشناست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن پارسا که ده خرد و ملک، رهزن است</p></div>
<div class="m2"><p>آن پادشا که مال رعیت خورد گداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر قطرهٔ سرشک یتیمان نظاره کن</p></div>
<div class="m2"><p>تا بنگری که روشنی گوهر از کجاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پروین، به کجروان سخن از راستی چه سود</p></div>
<div class="m2"><p>کو آنچنان کسی که نرنجد ز حرف راست</p></div></div>