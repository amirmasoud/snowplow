---
title: >-
    شمارهٔ  ۶۰ - دو محضر
---
# شمارهٔ  ۶۰ - دو محضر

<div class="b" id="bn1"><div class="m1"><p>قاضی کشمر ز محضر، شامگاه</p></div>
<div class="m2"><p>رفت سوی خانه با حالی تباه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا در دید، بر دیوار زد</p></div>
<div class="m2"><p>بانگ بر دربان و خدمتکار زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کودکان را راند با سیلی و مشت</p></div>
<div class="m2"><p>گربه را با چوبدستی خست و کشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خشم هم بر کوزه، هم بر آب کرد</p></div>
<div class="m2"><p>هم قدح، هم کاسه را پرتاب کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه کم گفتند، او بسیار گفت</p></div>
<div class="m2"><p>حرفهای سخت و ناهموار گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد خشم آلوده، سوی زن نگاه</p></div>
<div class="m2"><p>گفت کز دست تو روزم شد سیاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ز سرد و گرم گیتی بی خبر</p></div>
<div class="m2"><p>من گرفتار هزاران شور و شر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو غنودی، من دویدم روز و شب</p></div>
<div class="m2"><p>کاستم من، تو فزودی، ای عجب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو شدی دمساز با پیوند و دوست</p></div>
<div class="m2"><p>چرخ، روزی صد ره از من کند پوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناگواریها مرا برد از میان</p></div>
<div class="m2"><p>تو غنودی در حریر و پرنیان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو نشستی تا بیارندت ز در</p></div>
<div class="m2"><p>ما بیاوردیم با خون جگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر چه کردم گرد، با وزر و وبال</p></div>
<div class="m2"><p>تو بپای آز کردی پایمال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>توشه بستم از حلال و از حرام</p></div>
<div class="m2"><p>هم تو خوردی گاه پخته، گاه خام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا که چشمت دید همیان زری</p></div>
<div class="m2"><p>کردی از دل، آرزوی زیوری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا یتیم از یک بمن بخشید نیم</p></div>
<div class="m2"><p>تو خریدی گوهر و در یتیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کور و عاجز بس در افکندم بچاه</p></div>
<div class="m2"><p>تا که شد هموار از بهر تو راه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از پی یک راست، گفتم صد دروغ</p></div>
<div class="m2"><p>ماست را من بردم و مظلوم دوغ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سنگها انداختم در راه‌ها</p></div>
<div class="m2"><p>اشکها آمیختم با آه‌ها</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدرهٔ زر دیدم و رفتم ز دست</p></div>
<div class="m2"><p>بی تامل روز را گفتم شب است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حق نهفتم، بافتم افسانه‌ها</p></div>
<div class="m2"><p>سوختم با تهمتی کاشانه‌ها</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این سخنها بهر تو گفتم تمام</p></div>
<div class="m2"><p>تو چه گفتی؟ آرمیدی صبح و شام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ریختم بهر تو عمری آبرو</p></div>
<div class="m2"><p>تو چه کردی از برای من، بگو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رشوت آوردم، تو مال اندوختی</p></div>
<div class="m2"><p>تیرگی کردم، تو بزم افروختی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا به مرداری بیالودم دهن</p></div>
<div class="m2"><p>تو حسابی ساختی از بهر من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خدمت محضر ز من ناید دگر</p></div>
<div class="m2"><p>هر که را خواهی، بجای من ببر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بعد ازین نه پیروم، نه پیشوا</p></div>
<div class="m2"><p>چون تو، اندر خانه خواهم کرد جا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون تو خواهم بود پاک از هر حساب</p></div>
<div class="m2"><p>جز حساب سیرو گشت و خورد و خواب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زن بلطف و خنده گفت اینکار چیست</p></div>
<div class="m2"><p>با در و دیوار، این پیکار چیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>امشب از عقل و خرد بیگانه‌ای</p></div>
<div class="m2"><p>گر نه مستی، بیگمان دیوانه‌ای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کودکان را پای بر سر میزنی</p></div>
<div class="m2"><p>مشت بر طومار و دفتر میزنی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خودپسندیدن، و بال است و گزند</p></div>
<div class="m2"><p>دیگران را کی پسندد، خودپسند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من نمیگویم که کاری داشتم</p></div>
<div class="m2"><p>یا چو تو، بر دوش، باری داشتم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>میروم فردا من از خانه برون</p></div>
<div class="m2"><p>تو بر افراز این بساط واژگون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>میروم من، یک دو روز اینجا بمان</p></div>
<div class="m2"><p>همچو من، دانستنیها را بدان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عارفان، علم و عمل پیوسته‌اند</p></div>
<div class="m2"><p>دیده‌اند اول، سپس دانسته‌اند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زن چو از خانه سحرگه رخت بست</p></div>
<div class="m2"><p>خانه دیوانخانه شد، قاضی نشست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گاه خط بنوشت و گاه افسانه خواند</p></div>
<div class="m2"><p>ماند، اما بیخبر از خانه ماند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>روزی اندر خانه سخت آشوب شد</p></div>
<div class="m2"><p>گفتگوی مشت و سنگ و چوب شد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خادم و طباخ و فراش آمدند</p></div>
<div class="m2"><p>تا توانستند، دربان را زدند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پیش قاضی آن دروغ، این راست گفت</p></div>
<div class="m2"><p>در حقیقت، هر چه هر کس خواست گفت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عیبها گفتند از هم بیشمار</p></div>
<div class="m2"><p>رازهای بسته کردند آشکار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفت دربان این خسان اهریمنند</p></div>
<div class="m2"><p>مجرمند و بی گنه رامیزنند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>باز کردم هر سه را امروز مشت</p></div>
<div class="m2"><p>برگرفتم بار دزدیشان ز پشت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بانگ زد خادم بر او کی خود پرست</p></div>
<div class="m2"><p>قفل مخزن را که دیشب میشکست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کوزهٔ روغن تو میبردی بدوش</p></div>
<div class="m2"><p>یا برای خانه یا بهر فروش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خواجه از آغاز شب در خانه بود</p></div>
<div class="m2"><p>حاجب از بهر که، در را میگشود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دایه آمد گفت طفل شیرخوار</p></div>
<div class="m2"><p>گشته رنجور و نمیگیرد قرار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گفت ناظر، دختر من دیده است</p></div>
<div class="m2"><p>مطبخی کشک و عدس دزدیده است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ناگهان، فراش همیانی گشود</p></div>
<div class="m2"><p>گفت کاین زرها میان هیمه بود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>باغبان آمد که دزد، این ناظر است</p></div>
<div class="m2"><p>غائبست از حق، اگر چه حاضر است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زر فزون میگیرد و کم میخرد</p></div>
<div class="m2"><p>آنچه دینار است و درهم، میبرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>میکند از ما به جور و ظلم، پوست</p></div>
<div class="m2"><p>خواجه مهمانست، صاحبخانه اوست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دوش، یک من هیمه را باری نوشت</p></div>
<div class="m2"><p>خوشه‌ای آورد و خرواری نوشت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از کنار در، کنیز آواز داد</p></div>
<div class="m2"><p>بعد ازین، نان را کجا باید نهاد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کودکان نان و عسل را خورده‌اند</p></div>
<div class="m2"><p>سفره‌اش را نیز با خود برده‌اند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دید قاضی، خانه پرشور و شر است</p></div>
<div class="m2"><p>محضر است، اما دگرگون محضر است</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کار قاضی جز خط و دفتر نبود</p></div>
<div class="m2"><p>آشنا با این چنین محضر نبود</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>او چه میدانست آشوب از کجاست</p></div>
<div class="m2"><p>وین کم و افزون، که افزود و که کاست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چون امین نشناخت از دزد و دغل</p></div>
<div class="m2"><p>دفتر خود را نهاد اندر بغل</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گفت زین جنگ و جدل، سر خیره گشت</p></div>
<div class="m2"><p>بایدم رفتن، گه محضر گذشت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چون ز جا برخاست، زن در را گشود</p></div>
<div class="m2"><p>گفت دیدی آنچه گفتم راست بود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تو، به محضر داوری کردی هزار</p></div>
<div class="m2"><p>لیک اندر خانه درماندی ز کار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر چه ترساندی خلایق را بسی</p></div>
<div class="m2"><p>از تو خانه نمیترسد کسی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تو بسی گفتی ز کار خویشتن</p></div>
<div class="m2"><p>من نگفتم هیچ و دیدی کار من</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا تو اندر خانه دیدی گیر و دار</p></div>
<div class="m2"><p>چند روزی ماندی و کردی فرار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>من کنم صد شعله در یکدم خموش</p></div>
<div class="m2"><p>گاه دستم، گاه چشمم، گاه گوش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هر که بینی رشته‌ای دارد بدست</p></div>
<div class="m2"><p>هر کجا راهی است، رهپوئیش هست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تو چه میدانی که دزد خانه کیست</p></div>
<div class="m2"><p>زین حکایت حق کدام، افسانه چیست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زن، بدام افکند دزد خانه را</p></div>
<div class="m2"><p>از حقیقت دور کرد افسانه را</p></div></div>