---
title: >-
    شمارهٔ  ۱۴۵ - مور و مار
---
# شمارهٔ  ۱۴۵ - مور و مار

<div class="b" id="bn1"><div class="m1"><p>با مور گفت مار، سحرگه بمرغزار</p></div>
<div class="m2"><p>کاز ضعف و بیخودی، تو چنین خردی و نزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون تو، ناتوان نشنیدم بهیچ جا</p></div>
<div class="m2"><p>هر چند دیده‌ام چو تو جنبندگان هزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غافل چرا روی، که کشندت چو غافلان</p></div>
<div class="m2"><p>پشت از چه خم کنی، که نهندت به پشت بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر بر فراز، تا نزنندت بسر قفا</p></div>
<div class="m2"><p>تن نیک‌دار، تا ندهندت به تن فشار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خود مرو، ز دیدن هر دست زورمند</p></div>
<div class="m2"><p>جان عزیز، خیره بهر پا مکن نثار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار بزرگ هستی خود را مگیر خرد</p></div>
<div class="m2"><p>آگه چو زین شمار نه‌ای، پند گوشدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سست کاری، اینهمه سختی کشی و رنج</p></div>
<div class="m2"><p>بی موجبی کسی نشد، ایدوست، چون تو خوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن را که پای ظلم نهد بر سرت، بزن</p></div>
<div class="m2"><p>چالاک باش همچو من، اندر زمان کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خویشتن دفاع کن، ارزانکه زنده‌ای</p></div>
<div class="m2"><p>از من، ببین چگونه کند هر کسی فرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ننگ است با دو چشم به چه سرنگون شدن</p></div>
<div class="m2"><p>مرگ است زندگانی بی قدر و اعتبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من، جسم زورمند بسی سرد کرده‌ام</p></div>
<div class="m2"><p>هرگز نداده‌ام به بداندیش زینهار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرگشته چون تو، بر سر هر ره نگشته‌ام</p></div>
<div class="m2"><p>گاهی به سبزه خفته‌ام آسوده، گه به غار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از بهر نیم دانه، تو عمری تلف کنی</p></div>
<div class="m2"><p>من صبح موش صید کنم، شام سوسمار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همواره در گذرگه خلقی، تو تیره‌روز</p></div>
<div class="m2"><p>هر روز پایمالی و هر لحظه بی‌قرار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خندید مور و گفت، چنین است رسم و راه</p></div>
<div class="m2"><p>از رنج و سعی خویش، مرا نیست هیچ عار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آسوده آنکه در پی گنجی کشید رنج</p></div>
<div class="m2"><p>شاد آنکه چون منش، قدمی بود استوار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیهش چه خوانیم، که ندیدست هیچ کس</p></div>
<div class="m2"><p>مانند مور، عاقبت اندیش و هوشیار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من، دانه‌ای به لانه کشم با هزار سعی</p></div>
<div class="m2"><p>از پا دراوفتم به ره اندر، هزار بار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از کار سخت خود نکنم هیچ شکوه، زانک</p></div>
<div class="m2"><p>ناکرده کار، می‌نتوان زیست کامکار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غافل توئی، که بد کنی و بی‌خبر روی</p></div>
<div class="m2"><p>در رهگذر من نبود دام و گیر و دار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من، تن بخاک میکشم و بار میبرم</p></div>
<div class="m2"><p>از مور، بیش ازین چه توان داشت انتظار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کوشم بزندگی و ننالم بگاه مرگ</p></div>
<div class="m2"><p>زین زندگی و مرگ که بودست شرمسار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جز سعی، نیست مورچگان را وظیفه‌ای</p></div>
<div class="m2"><p>با فکر سیر و خفتن خوش، مور را چه کار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شادم که نیست نیروی آزار کردنم</p></div>
<div class="m2"><p>در زحمت است، آنکه تو هستیش در جوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جز بددلی و فکرت پستت، چه خصلتی است</p></div>
<div class="m2"><p>از مردم زمانه، ترا کیست دوستدار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ایمن مشو ز فتنه، چو خود فتنه میکنی</p></div>
<div class="m2"><p>گر چیره‌ای تو، چیره‌تر است از تو روزگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>افسونگر زمانه، ترا هم کندن فسون</p></div>
<div class="m2"><p>صیاد چرخ پیر، ترا هم کند شکار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای بی‌خبر، قبیلهٔ ما بس هنرورند</p></div>
<div class="m2"><p>هرگز نبوده‌است هنرمند، خاکسار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مورم، کسی مرا نکشد هیچگه بعمد</p></div>
<div class="m2"><p>ماری تو، هر کجاست بکوبند مغز مار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با بد، به جز بدی نکند چرخ نیلگون</p></div>
<div class="m2"><p>از خار، هیچ میوه نچیدند غیر خار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جز نام نیک و زشت، نماند ز کارها</p></div>
<div class="m2"><p>جز نیکوئی مکن، که جهان نیست پایدار</p></div></div>