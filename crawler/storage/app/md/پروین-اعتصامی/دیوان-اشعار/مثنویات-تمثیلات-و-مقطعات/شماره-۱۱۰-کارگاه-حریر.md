---
title: >-
    شمارهٔ  ۱۱۰ - کارگاه حریر
---
# شمارهٔ  ۱۱۰ - کارگاه حریر

<div class="b" id="bn1"><div class="m1"><p>به کرم پیله شنیدم که طعنه زد حلزون</p></div>
<div class="m2"><p>که کار کردن بیمزد، عمر باختن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی هلاک خود، ای بیخبر، چه میکوشی</p></div>
<div class="m2"><p>هر آنچه ریشته‌ای، عاقبت ترا کفن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدست جهل، به بنیاد خویش تیشه زدن</p></div>
<div class="m2"><p>دو چشم بستن و در چاه سرنگون شدن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو ما، برو در و دیوار خانه محکم کن</p></div>
<div class="m2"><p>مگرد ایمن و فارغ، زمانه راهزن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفت، قدر کسی را نکاست سعی و عمل</p></div>
<div class="m2"><p>خیال پرورش تن، ز قدر کاستن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخدمت دگران دل چگونه خواهد داد</p></div>
<div class="m2"><p>کسی که همچو تو، دائم بفکر خویشتن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدیگ حادثه، روزی گرم بجوشانند</p></div>
<div class="m2"><p>شگفت نیست، که مرگ از قفای زیستن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بروز مرگم، اگر پیله گور گشت و کفن</p></div>
<div class="m2"><p>بوقت زندگیم، خوابگاه و پیرهن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا بخیره نخوانند کرم ابریشم</p></div>
<div class="m2"><p>بهر بساط که ابریشمی است، کار من است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز جانفشانی و خون خوردن قبیلهٔ ماست</p></div>
<div class="m2"><p>پرند و دیبهٔ گلرنگ، هر کرا بتن است</p></div></div>