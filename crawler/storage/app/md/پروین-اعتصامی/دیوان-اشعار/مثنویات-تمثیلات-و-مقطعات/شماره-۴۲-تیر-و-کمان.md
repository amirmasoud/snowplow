---
title: >-
    شمارهٔ  ۴۲ - تیر و کمان
---
# شمارهٔ  ۴۲ - تیر و کمان

<div class="b" id="bn1"><div class="m1"><p>گفت تیری با کمان، روز نبرد</p></div>
<div class="m2"><p>کاین ستمکاری تو کردی، کس نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیرها بودت قرین، ای بوالهوس</p></div>
<div class="m2"><p>در فکندی جمله را در یک نفس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما ز بیداد تو سرگردان شدیم</p></div>
<div class="m2"><p>همچو کاه اندر هوا رقصان شدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش بکار دوستان پرداختی</p></div>
<div class="m2"><p>بر گرفتی یک یک و انداختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من دمی چند است کاینجا مانده‌ام</p></div>
<div class="m2"><p>دیگران رفتند و تنها مانده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیم آن دارم کازین جور و عناد</p></div>
<div class="m2"><p>بر من افتد آنچه بر آنان فتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترسم آخر بگذرد بر جان من</p></div>
<div class="m2"><p>آنچه بگذشتست بر یاران من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان همی لرزد دل من در نهان</p></div>
<div class="m2"><p>که در اندازی مرا هم ناگهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تو میخواهم که با من خو کنی</p></div>
<div class="m2"><p>بعد ازین کردار خود نیکو کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان گروه رفته نشماری مرا</p></div>
<div class="m2"><p>مهربان باشی، نگهداری مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به که ما با یکدگر باشیم دوست</p></div>
<div class="m2"><p>پارگی خرد است و امید رفوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکدل ار گردیم در سود و زیان</p></div>
<div class="m2"><p>این شکایت‌ها نیاید در میان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر تو از کردار بد باشی بری</p></div>
<div class="m2"><p>کس نخواهد با تو کردن بدسری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر بیک پیمان، وفا بینم ز تو</p></div>
<div class="m2"><p>یک نفس، آزرده ننشینم ز تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت با تیر از سر مهر، آن کمان</p></div>
<div class="m2"><p>در کمان، کی تیر ماند جاودان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد کمان را پیشه، تیر انداختن</p></div>
<div class="m2"><p>تیر را شد چاره با وی ساختن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تیر، یکدم در کمان دارد درنگ</p></div>
<div class="m2"><p>این نصیحت بشنو، ای تیر خدنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ما جز این یک ره، رهی نشناختیم</p></div>
<div class="m2"><p>هر که ما را تیر داد، انداختیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کیست کاز جور قضا آواره نیست</p></div>
<div class="m2"><p>تیر گشتی، از کمانت چاره نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عادت ما این بود، بر ما مگیر</p></div>
<div class="m2"><p>نه کمان آسایشی دارد، نه تیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درزی ایام را اندازه نیست</p></div>
<div class="m2"><p>جور و بد کاریش، کاری تازه نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون ترا سر گشتگی تقدیر شد</p></div>
<div class="m2"><p>بایدت رفت، ار چه رفتن دیر شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زین مکان، آخر تو هم بیرون روی</p></div>
<div class="m2"><p>کس چه میداند کجا یا چون روی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از من آن تیری که میگردد جدا</p></div>
<div class="m2"><p>من چه میدانم که رقصد در هوا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آگهم کاز بند من بیرون نشست</p></div>
<div class="m2"><p>من چه میدانم که اندر خون نشست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تیر گشتن در کمان آسمان</p></div>
<div class="m2"><p>بهر افتادن شد، این معنی بدان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این کمان را تیر، مردم گشته‌اند</p></div>
<div class="m2"><p>سر کار اینست، زان سر گشته‌اند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چرخ و انجم، هستی ما میبرند</p></div>
<div class="m2"><p>ما نمی‌بینیم و ما را میبرند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ره نمی‌پرسیم، اما میرویم</p></div>
<div class="m2"><p>تا که نیروئیست در پا، میرویم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کاش روزی زین ره دور و دراز</p></div>
<div class="m2"><p>باز گشتن میتوانستیم باز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کاش آن فرصت که پیش از ما شتافت</p></div>
<div class="m2"><p>میتوانستیم آنرا باز یافت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دیدهٔ دل کاشکی بیدار بود</p></div>
<div class="m2"><p>تا کمند دزد بر دیوار بود</p></div></div>