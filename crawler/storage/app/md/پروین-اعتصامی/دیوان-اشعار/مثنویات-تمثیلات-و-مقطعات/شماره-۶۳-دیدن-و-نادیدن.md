---
title: >-
    شمارهٔ  ۶۳ - دیدن و نادیدن
---
# شمارهٔ  ۶۳ - دیدن و نادیدن

<div class="b" id="bn1"><div class="m1"><p>شبی بمردمک چشم، طعنه زد مژگان</p></div>
<div class="m2"><p>که چند بی سبب از بهر خلق کوشیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه بار جفا بردن و نیاسودن</p></div>
<div class="m2"><p>همیشه رنج طلب کردن و نرنجیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نیک و زشت و گل و خار و مردم و حیوان</p></div>
<div class="m2"><p>تمام دیدن و از خویش هیچ نادیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو کارگر شده‌ای، مزد سعی و رنج تو چیست</p></div>
<div class="m2"><p>بوقت کار، ضروری است کار سنجیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بزم تیرهٔ خود، روشنی دریغ مدار</p></div>
<div class="m2"><p>که روشنست ازین بزم، رخت برچیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جواب داد که آئین کاردانان نیست</p></div>
<div class="m2"><p>بخواب جهل فزودن، ز کار کاهیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنایتی است درین رنج روز خسته شدن</p></div>
<div class="m2"><p>اشارتی است درین کار شب نخوابیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا حدیثی هوی و هوس مکن تعلیم</p></div>
<div class="m2"><p>هنروران نپسندند خود پسندیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگاهبانی ملک تن است پیشهٔ چشم</p></div>
<div class="m2"><p>چنانکه رسم و ره پاست ره نوردیدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر پی هوس و آز خویش میگشتم</p></div>
<div class="m2"><p>کنون نبود مرا دیده، جای گردیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بپای خویش نیفکنده روشنی هرگز</p></div>
<div class="m2"><p>اگر چه کار چراغ است نور بخشیدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه آگهیست، ز حکم قضا شدن دلتنگ</p></div>
<div class="m2"><p>نه مردمی است، ز دست زمانه نالیدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مگو چرا مژه گشتم من و تو مردم چشم</p></div>
<div class="m2"><p>ازین حدیث، کس آگه نشد بپرسیدن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هزار مسئله در دفتر حقیقت بود</p></div>
<div class="m2"><p>ولی دریغ، که دشوار بود فهمیدن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز دل تپیدن و از دیده روشنی خواهند</p></div>
<div class="m2"><p>ز خون دویدن و از اشک چشم، غلتیدن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز کوه و کاه گرانسنگی و سبکباری</p></div>
<div class="m2"><p>ز خاک صبر و تواضع، ز باد رقصیدن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپهر، مردم چشمم نهاد نام از آن</p></div>
<div class="m2"><p>که بود خصلتم، از خویش چشم پوشیدن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هزار قرن ندیدن ز روشنی اثری</p></div>
<div class="m2"><p>هزار مرتبه بهتر ز خویشتن دیدن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هوای نفس چو دیویست تیره دل، پروین</p></div>
<div class="m2"><p>بتر ز دیو پرستی است، خودپرستیدن</p></div></div>