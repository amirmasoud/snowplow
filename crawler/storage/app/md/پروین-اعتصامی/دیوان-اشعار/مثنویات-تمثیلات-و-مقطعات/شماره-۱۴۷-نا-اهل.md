---
title: >-
    شمارهٔ  ۱۴۷ - نا اهل
---
# شمارهٔ  ۱۴۷ - نا اهل

<div class="b" id="bn1"><div class="m1"><p>نوگلی، روزی ز شورستان دمید</p></div>
<div class="m2"><p>خار، آن گل دید و رو در هم کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز چه روئیدی به پیش پای ما</p></div>
<div class="m2"><p>تنگ کردی بی ضرورت، جای ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرخی رنگ تو، چشمم خیره کرد</p></div>
<div class="m2"><p>زشتی رویت، فضا را تیره کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسته گشت از بوی جانکاهت وجود</p></div>
<div class="m2"><p>این چه نقش است، این چه تار است، این چه پود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حجلت است، این شاخهٔ بی‌بار تو</p></div>
<div class="m2"><p>عبرت است، این برگ ناهموار تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاش بر میکند، زین مرزت کسی</p></div>
<div class="m2"><p>کاش میروئید در جایت خسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ندانم از کدامین کشوری</p></div>
<div class="m2"><p>هر که هستی، مایهٔ دردسری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما ز یک اقلیم، زان با هم خوشیم</p></div>
<div class="m2"><p>گر که در آبیم و گر در آتشیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شبنمی گر میچکد، بر روی ماست</p></div>
<div class="m2"><p>نکهتی گر میرسد، از بوی ماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون تو، بس در جوی و جر روئیده‌اند</p></div>
<div class="m2"><p>لیک ما را بیشتر بوئیده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دسته‌ها چیدند از ما صبح و شام</p></div>
<div class="m2"><p>هیچ ننهادند نزدیک تو گام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو همه عیبی و ما یکسر هنر</p></div>
<div class="m2"><p>ما سرافرازیم و تو بی پا و سر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گل بدو خندید کای بی مهر دوست</p></div>
<div class="m2"><p>زشتروئی، لیک گفتارت نکوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همنشین چون توئی بودن، خطاست</p></div>
<div class="m2"><p>راست گفتی آنچه گفتی، راست راست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گلبنی کاندر بیابانی شکفت</p></div>
<div class="m2"><p>یاوه‌ای گر خار بر روی گفت، گفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>می‌شکفتیم ار بطرف گلشنی</p></div>
<div class="m2"><p>میکشیدیم از تفاخر دامنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا میان خار و خاشاک اندریم</p></div>
<div class="m2"><p>کس نداند کز شما نیکوتریم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ما کز اول، پاک طینت بوده‌ایم</p></div>
<div class="m2"><p>از کجا دامان تو آلوده‌ایم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صبحت گل، رنجه دارد خار را!</p></div>
<div class="m2"><p>خیرگی بین، خار ناهموار را!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خار دیدستی که گل دید و رمید</p></div>
<div class="m2"><p>گل شنیدستی که شد خار و خلید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ما فرومایه نبودیم از ازل</p></div>
<div class="m2"><p>تو فرومایه، شدی ضرب‌المثل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همنشینان تو خارانند و بس</p></div>
<div class="m2"><p>گل چه ارزد پیش تو، ای بوالهوس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیش تو، غیر از گیاهی نیستیم</p></div>
<div class="m2"><p>تو چه میدانی چه‌ایم و کیستیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون کسی نا اهل را اهلی شمرد</p></div>
<div class="m2"><p>گر ز وی روزی قفائی خورد، خورد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ما که جای خویش را نشناختیم</p></div>
<div class="m2"><p>خویشتن را در بلا انداختیم</p></div></div>