---
title: >-
    شمارهٔ  ۱۶۴ - قطعه
---
# شمارهٔ  ۱۶۴ - قطعه

<div class="b" id="bn1"><div class="m1"><p>ای گل، تو ز جمعیت گلزار، چه دیدی</p></div>
<div class="m2"><p>جز سرزنش و بد سری خار، چه دیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای لعل دل افروز، تو با اینهمه پرتو</p></div>
<div class="m2"><p>جز مشتری سفله، ببازار چه دیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفتی به چمن، لیک قفس گشت نصیبت</p></div>
<div class="m2"><p>غیر از قفس، ای مرغ گرفتار، چه دیدی</p></div></div>