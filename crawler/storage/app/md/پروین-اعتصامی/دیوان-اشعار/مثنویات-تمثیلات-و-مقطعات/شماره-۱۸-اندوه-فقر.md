---
title: >-
    شمارهٔ  ۱۸ - اندوه فقر
---
# شمارهٔ  ۱۸ - اندوه فقر

<div class="b" id="bn1"><div class="m1"><p>با دوک خویش، پیرزنی گفت وقت کار</p></div>
<div class="m2"><p>کاوخ! ز پنبه ریشتنم موی شد سفید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که بر تو خم شدم و چشم دوختم</p></div>
<div class="m2"><p>کم نور گشت دیده‌ام و قامتم خمید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر آمد و گرفت سر کلبهٔ مرا</p></div>
<div class="m2"><p>بر من گریست زار که فصل شتا رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز من که دستم از همه چیز جهان تهیست</p></div>
<div class="m2"><p>هر کس که بود، برگ زمستان خود خرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی زر، کسی بکس ندهد هیزم و زغال</p></div>
<div class="m2"><p>این آرزوست گر نگری، آن یکی امید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر بست هر پرنده در آشیان خویش</p></div>
<div class="m2"><p>بگریخت هر خزنده و در گوشه‌ای خزید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نور از کجا به روزن بیچارگان فتد</p></div>
<div class="m2"><p>چون گشت آفتاب جهانتاب ناپدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از رنج پاره دوختن و زحمت رفو</p></div>
<div class="m2"><p>خونابهٔ دلم ز سر انگشتها چکید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک جای وصله در همهٔ جامه‌ام نماند</p></div>
<div class="m2"><p>زین روی وصله کردم، از آن رو ز هم درید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیروز خواستم چو بسوزن کنم نخی</p></div>
<div class="m2"><p>لرزید بند دستم و چشمم دگر ندید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من بس گرسنه خفتم و شبها مشام من</p></div>
<div class="m2"><p>بوی طعام خانهٔ همسایگان شنید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز اندوه دیر گشتن اندود بام خویش</p></div>
<div class="m2"><p>هر گه که ابر دیدم و باران، دلم طپید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرویزنست سقف من، از بس شکستگی</p></div>
<div class="m2"><p>در برف و گل چگونه تواند کس آرمید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هنگام صبح در عوض پرده، عنکبوت</p></div>
<div class="m2"><p>بر بام و سقف ریخته‌ام تارها تنید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در باغ دهر بهر تماشای غنچه‌ای</p></div>
<div class="m2"><p>بر پای من بهر قدمی خارها خلید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سیلابهای حادثه بسیار دیده‌ام</p></div>
<div class="m2"><p>سیل سرشک زان سبب از دیده‌ام دوید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دولت چه شد که چهره ز درماندگان بتافت</p></div>
<div class="m2"><p>اقبال از چه راه ز بیچارگان رمید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پروین، توانگران غم مسکین نمیخورند</p></div>
<div class="m2"><p>بیهوده‌اش مکوب که سرد است این حدید</p></div></div>