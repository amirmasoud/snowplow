---
title: >-
    شمارهٔ  ۳۲ - بی پدر
---
# شمارهٔ  ۳۲ - بی پدر

<div class="b" id="bn1"><div class="m1"><p>به سر خاک پدر، دخترکی</p></div>
<div class="m2"><p>صورت و سینه بناخن میخست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که نه پیوند و نه مادر دارم</p></div>
<div class="m2"><p>کاش روحم به پدر می‌پیوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه‌ام بهر پدر نیست که او</p></div>
<div class="m2"><p>مرد و از رنج تهیدستی رست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان کنم گریه که اندریم بخت</p></div>
<div class="m2"><p>دام بر هر طرف انداخت گسست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شصت سال آفت این دریا دید</p></div>
<div class="m2"><p>هیچ ماهیش نیفتاد به شست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پدرم مرد ز بی داروئی</p></div>
<div class="m2"><p>وندرین کوی، سه داروگر هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل مسکینم از این غم بگداخت</p></div>
<div class="m2"><p>که طبیبیش ببالین ننشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوی همسایه پی نان رفتم</p></div>
<div class="m2"><p>تا مرا دید، در خانه ببست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه دیدند که افتاده ز پای</p></div>
<div class="m2"><p>لیک روزی نگرفتندش دست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آب دادم بپدر چون نان خواست</p></div>
<div class="m2"><p>دیشب از دیدهٔ من آتش جست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم قبا داشت ثریا، هم کفش</p></div>
<div class="m2"><p>دل من بود که ایام شکست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اینهمه بخل چرا کرد، مگر</p></div>
<div class="m2"><p>من چه میخواستم از گیتی پست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیم و زر بود، خدائی گر بود</p></div>
<div class="m2"><p>آه از این آدمی دیوپرست</p></div></div>