---
title: >-
    شمارهٔ  ۸۸ - شکایت پیرزن
---
# شمارهٔ  ۸۸ - شکایت پیرزن

<div class="b" id="bn1"><div class="m1"><p>روز شکار، پیرزنی با قباد گفت</p></div>
<div class="m2"><p>کاز آتش فساد تو، جز دود و آه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی بیا به کلبهٔ ما از ره شکار</p></div>
<div class="m2"><p>تحقیق حال گوشه‌نشینان گناه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هنگام چاشت، سفرهٔ بی نان ما ببین</p></div>
<div class="m2"><p>تا بنگری که نام و نشان از رفاه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دزدم لحاف برد و شبان گاو پس نداد</p></div>
<div class="m2"><p>دیگر به کشور تو، امان و پناه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تشنگی، کدوبنم امسال خشک شد</p></div>
<div class="m2"><p>آب قنات بردی و آبی بچاه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سنگینی خراج، بما عرصه تنگ کرد</p></div>
<div class="m2"><p>گندم تراست، حاصل ما غیر کاه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دامن تو، دیده جز آلودگی ندید</p></div>
<div class="m2"><p>بر عیبهای روشن خویشت، نگاه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حکم دروغ دادی و گفتی حقیقت است</p></div>
<div class="m2"><p>کار تباه کردی و گفتی تباه نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد جور دیدم از سگ و دربان به درگهت</p></div>
<div class="m2"><p>جز سفله و بخیل، درین بارگاه نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ویرانه شد ز ظلم تو، هر مسکن و دهی</p></div>
<div class="m2"><p>یغماگر است چون تو کسی، پادشاه نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مردی در آنزمان که شدی صید گرگ آز</p></div>
<div class="m2"><p>از بهر مرده، حاجت تخت و کلاه نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکدوست از برای تو نگذاشت دشمنی</p></div>
<div class="m2"><p>یک مرد رزمجوی، ترا در سپاه نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جمعی سیاهروز سیهکاری تواند</p></div>
<div class="m2"><p>باور مکن که بهر تو روز سیاه نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مزدور خفته را ندهد مزد، هیچکس</p></div>
<div class="m2"><p>میدان همت است جهان، خوابگاه نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تقویم عمر ماست جهان، هر چه میکنیم</p></div>
<div class="m2"><p>بیرون ز دفتر کهن سال و ماه نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سختی کشی ز دهر، چو سختی دهی بخلق</p></div>
<div class="m2"><p>در کیفر فلک، غلط و اشتباه نیست</p></div></div>