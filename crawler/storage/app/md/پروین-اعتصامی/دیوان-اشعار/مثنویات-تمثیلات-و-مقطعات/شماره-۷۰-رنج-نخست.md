---
title: >-
    شمارهٔ  ۷۰ - رنج نخست
---
# شمارهٔ  ۷۰ - رنج نخست

<div class="b" id="bn1"><div class="m1"><p>خلید خار درشتی بپای طفلی خرد</p></div>
<div class="m2"><p>بهم برآمد و از پویه باز ماند و گریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفت مادرش این رنج اولین قدم است</p></div>
<div class="m2"><p>ز خار حادثه، تیه وجود خالی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هنوز نیک و بد زندگی بدفتر عمر</p></div>
<div class="m2"><p>نخوانده‌ای و بچشم تو راه و چاه، یکیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پای، چون تو در افتاده‌اند بس طفلان</p></div>
<div class="m2"><p>نیوفتاده درین سنگلاخ عبرت، کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندیده زحمت رفتار، ره نیاموزی</p></div>
<div class="m2"><p>خطا نکرده، صواب و خطا چه دانی چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلی که سخت ز هر غم تپید، شاد نماند</p></div>
<div class="m2"><p>کسیکه زود دل آزرده گشت دیر نزیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز عهد کودکی، آمادهٔ بزرگی شو</p></div>
<div class="m2"><p>حجاب ضعف چو از هم گسست، عزم قویست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بچشم آنکه درین دشت، چشم روشن بست</p></div>
<div class="m2"><p>تفاوتی نکند، گر ده است چه، یا بیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو زخم کارگر آمد، چه سر، چه سینه، چه پای</p></div>
<div class="m2"><p>چو سال عمر تبه شد، چه یک، چه صد، چه دویست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزار کوه گرت سد ره شوند، برو</p></div>
<div class="m2"><p>هزار ره گرت از پا در افکنند، بایست</p></div></div>