---
title: >-
    شمارهٔ  ۲۰ - ای گربه
---
# شمارهٔ  ۲۰ - ای گربه

<div class="b" id="bn1"><div class="m1"><p>ای گربه، ترا چه شد که ناگاه</p></div>
<div class="m2"><p>رفتی و نیامدی دگر بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس روز گذشت و هفته و ماه</p></div>
<div class="m2"><p>معلوم نشد که چون شد این کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جای تو شبانگه و سحرگاه</p></div>
<div class="m2"><p>در دامن من تهیست بسیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در راه تو کند آسمان چاه</p></div>
<div class="m2"><p>کار تو زمانه کرد دشوار</p></div></div>
<div class="b2" id="bn5"><p>پیدا نه بخانه‌ای نه بر بام</p></div>
<div class="b" id="bn6"><div class="m1"><p>ای گمشدهٔ عزیز، دانی</p></div>
<div class="m2"><p>کز یاد نمیشوی فراموش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برد آنکه ترا بمیهمانی</p></div>
<div class="m2"><p>دستیت کشید بر سر و گوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنواخت تو را بمهربانی</p></div>
<div class="m2"><p>بنشاند تو را دمی در آغوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میگویمت این سخن نهانی</p></div>
<div class="m2"><p>در خانهٔ ما ز آفت موش</p></div></div>
<div class="b2" id="bn10"><p>نه پخته بجای ماند و نه خام</p></div>
<div class="b" id="bn11"><div class="m1"><p>آن پنجهٔ تیز در شب تار</p></div>
<div class="m2"><p>کردست گهی شکار ماهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گشته است بحیله‌ای گرفتار</p></div>
<div class="m2"><p>در چنگ تو مرغ صبحگاهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>افتد گذرت بسوی انبار</p></div>
<div class="m2"><p>بانو دهدت هر آنچه خواهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در دیگ طمع، سرت دگر بار</p></div>
<div class="m2"><p>آلود بروغن و سیاهی</p></div></div>
<div class="b2" id="bn15"><p>چونی به زمان خواب و آرام</p></div>
<div class="b" id="bn16"><div class="m1"><p>آنروز تو داشتی سه فرزند</p></div>
<div class="m2"><p>از خندهٔ صبحگاه خوشتر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خفتند نژند روزکی چند</p></div>
<div class="m2"><p>در دامن گربه‌های دیگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرزند ز مادرست خرسند</p></div>
<div class="m2"><p>بیگانه کجا و مهر مادر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون عهد شد و شکست پیوند</p></div>
<div class="m2"><p>گشتند بسان دوک لاغر</p></div></div>
<div class="b2" id="bn20"><p>مردند و برون شدند زین دام</p></div>
<div class="b" id="bn21"><div class="m1"><p>از بازی خویش یاد داری</p></div>
<div class="m2"><p>بر بام، شبی که بود مهتاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گشتی چو ز دست من فراری</p></div>
<div class="m2"><p>افتاد و شکست کوزهٔ آب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ژولید، چو آب گشت جاری</p></div>
<div class="m2"><p>آن موی به از سمور و سنجاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زان آشتی و ستیزه کاری</p></div>
<div class="m2"><p>ماندی تو ز شبروی، من از خواب</p></div></div>
<div class="b2" id="bn25"><p>با آن همه توسنی شدی رام</p></div>
<div class="b" id="bn26"><div class="m1"><p>آنجا که طبیب شد بداندیش</p></div>
<div class="m2"><p>افزوده شود به دردمندی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این مار همیشه میزند نیش</p></div>
<div class="m2"><p>زنهار به زخم کس نخندی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هشدار، بسیست در پس و پیش</p></div>
<div class="m2"><p>بیغوله و پستی و بلندی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با حمله قضا نرانی از خویش</p></div>
<div class="m2"><p>با حیله ره فلک نبندی</p></div></div>
<div class="b2" id="bn30"><p>یغما گر زندگی است ایام</p></div>