---
title: >-
    شمارهٔ  ۱۰ - آشیان ویران
---
# شمارهٔ  ۱۰ - آشیان ویران

<div class="b" id="bn1"><div class="m1"><p>از ساحت پاک آشیانی</p></div>
<div class="m2"><p>مرغی بپرید سوی گلزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در فکرت توشی و توانی</p></div>
<div class="m2"><p>افتاد بسی و جست بسیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفت از چمنی به بوستانی</p></div>
<div class="m2"><p>بر هر گل و میوه سود منقار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا خفت ز خستگی زمانی</p></div>
<div class="m2"><p>یغماگر دهر گشت بیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیری بجهید از کمانی</p></div>
<div class="m2"><p>چون برق جهان ز ابر آذار</p></div></div>
<div class="b2" id="bn6"><p>گردید نژند خاطری شاد</p></div>
<div class="b" id="bn7"><div class="m1"><p>چون بال و پرش تپید در خون</p></div>
<div class="m2"><p>از یاد برون شدش پریدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افتاد ز گیرودار گردون</p></div>
<div class="m2"><p>نومید ز آشیان رسیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پر سر خویش کرد بیرون</p></div>
<div class="m2"><p>نالید ز درد سر کشیدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دانست که نیست دشت و هامون</p></div>
<div class="m2"><p>شایستهٔ فارغ آرمیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد چهرهٔ زندگی دگرگون</p></div>
<div class="m2"><p>در دیده نماند تاب دیدن</p></div></div>
<div class="b2" id="bn12"><p>مانا که دل از تپیدن افتاد</p></div>
<div class="b" id="bn13"><div class="m1"><p>مجروح ز رنج زندگی رست</p></div>
<div class="m2"><p>از قلب بریده گشت شریان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن بال و پر لطیف بشکست</p></div>
<div class="m2"><p>وان سینهٔ خرد خست پیکان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صیاد سیه دل از کمین جست</p></div>
<div class="m2"><p>تا صید ضعیف گشت بیجان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در پهلوی آن فتاده بنشست</p></div>
<div class="m2"><p>آلوده بخون مرغ دامان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بنهاد به پشتواره و بست</p></div>
<div class="m2"><p>آمد سوی خانه شامگاهان</p></div></div>
<div class="b2" id="bn18"><p>وان صید بدست کودکان داد</p></div>
<div class="b" id="bn19"><div class="m1"><p>چون صبح دمید، مرغکی خرد</p></div>
<div class="m2"><p>افتاد ز آشیانه در جر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون دانه نیافت، خون دل خورد</p></div>
<div class="m2"><p>تقدیر، پرش بکند یکسر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاهین حوادثش فرو برد</p></div>
<div class="m2"><p>نشنید حدیث مهر مادر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دور فلکش بهیچ نشمرد</p></div>
<div class="m2"><p>نفکند کسیش سایه بر سر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نادیده سپهر زندگی، مرد</p></div>
<div class="m2"><p>پرواز نکرده، سوختش پر</p></div></div>
<div class="b2" id="bn24"><p>رفت آن هوس و امید بر باد</p></div>
<div class="b" id="bn25"><div class="m1"><p>آمد شب و تیره گشت لانه</p></div>
<div class="m2"><p>وان رفته نیامد از سفر باز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کوشید فسونگر زمانه</p></div>
<div class="m2"><p>کاز پرده برون نیفتد این راز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>طفلان بخیال آب و دانه</p></div>
<div class="m2"><p>خفتند و نخاست دیگر آواز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از بامک آن بلند خانه</p></div>
<div class="m2"><p>کس روز عمل نکرد پرواز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکباره برفت از میانه</p></div>
<div class="m2"><p>آن شادی و شوق و نعمت و ناز</p></div></div>
<div class="b2" id="bn30"><p>زان گمشدگان نکرد کس یاد</p></div>
<div class="b" id="bn31"><div class="m1"><p>آن مسکن خرد پاک ایمن</p></div>
<div class="m2"><p>خالی و خراب ماند فرجام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>افتاد گلش ز سقف و روزن</p></div>
<div class="m2"><p>خار و خسکش بریخت از بام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آرامگهی نه بهر خفتن</p></div>
<div class="m2"><p>بامی نه برای سیر و آرام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر باد شد آن بنای روشن</p></div>
<div class="m2"><p>نابود شد آن نشانه و نام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از گردش روزگار توسن</p></div>
<div class="m2"><p>وز بدسری سپهر و اجرام</p></div></div>
<div class="b2" id="bn36"><p>دیگر نشد آن خرابی آباد</p></div>
<div class="b" id="bn37"><div class="m1"><p>شد ساقی چرخ پیر خرسند</p></div>
<div class="m2"><p>پردید ز خون چو ساغری را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دستی سر راه دامی افکند</p></div>
<div class="m2"><p>پیچانید به رشته‌ای سری را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جمعیت ایمنی پراکند</p></div>
<div class="m2"><p>شیرازه درید دفتری را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>با تیشهٔ ظلم ریشه‌ای کند</p></div>
<div class="m2"><p>بر بست ز فتنه‌ای دری را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خون ریخت بکام کودکی چند</p></div>
<div class="m2"><p>برچید بساط مادری را</p></div></div>
<div class="b2" id="bn42"><p>فرزند مگر نداشت صیاد؟</p></div>