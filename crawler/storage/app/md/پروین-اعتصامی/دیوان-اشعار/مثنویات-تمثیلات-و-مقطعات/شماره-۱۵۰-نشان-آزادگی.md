---
title: >-
    شمارهٔ  ۱۵۰ - نشان آزادگی
---
# شمارهٔ  ۱۵۰ - نشان آزادگی

<div class="b" id="bn1"><div class="m1"><p>به سوزنی ز ره شکوه گفت پیرهنی</p></div>
<div class="m2"><p>ببین ز جور تو، ما را چه زخمها بتن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه کار تو، سوراخ کردن دلهاست</p></div>
<div class="m2"><p>هماره فکر تو، بر پهلوئی فرو شدن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگفت، گر ره و رفتار من نداری دوست</p></div>
<div class="m2"><p>برو بگوی بدرزی که رهنمای من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وگر نه، بی‌سبب از دست من چه مینالی</p></div>
<div class="m2"><p>ندیده زحمت سوزن، کدام پیرهن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر به خار و خسی فتنه‌ای رسد در دشت</p></div>
<div class="m2"><p>گناه داس و تبر نیست، جرم خارکن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز من چگونه ترا پاره گشت پهلو و دل</p></div>
<div class="m2"><p>خود آگهی، که مرا پیشه پاره دوختن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه رنجها که برم بهر خرقه دوختنی</p></div>
<div class="m2"><p>چه وصله‌ها که ز من بر لحاف پیرزن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدان هوس که تن این و آن بیارایم</p></div>
<div class="m2"><p>مرا وظیفهٔ دیرینه، ساده زیستن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز در شکستن و خم گشتنم نیاید عار</p></div>
<div class="m2"><p>چرا که عادت من، با زمانه ساختن است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعار من، ز بس آزادگی و نیکدلی</p></div>
<div class="m2"><p>بقدر خلق فزودن، ز خویش کاستن است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همیشه دوختنم کار و خویش عریانم</p></div>
<div class="m2"><p>بغیر من، که تهی از خیال خویشتن است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی نباخته، ای دوست، دیگری نبرد</p></div>
<div class="m2"><p>جهان و کار جهان، همچو نرد باختن است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بباید آنکه شود بزم زندگی روشن</p></div>
<div class="m2"><p>نصیب شمع، مپرس از چه روی سوختن است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر آن قماش، که از سوزنی جفا نکشد</p></div>
<div class="m2"><p>عبث در آرزوی همنشینی بدن است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>میان صورت و معنی، بسی تفاوتهاست</p></div>
<div class="m2"><p>فرشته را، بتصور مگوی اهرمن است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هزار نکته ز باران و برف میگوید</p></div>
<div class="m2"><p>شکوفه‌ای که به فصل بهار، در چمن است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هم از تحمل گرما و قرنها سختی است</p></div>
<div class="m2"><p>اگر گهر به بدخش و عقیق در یمن است</p></div></div>