---
title: >-
    شمارهٔ  ۷۹ - سرود خارکن
---
# شمارهٔ  ۷۹ - سرود خارکن

<div class="b" id="bn1"><div class="m1"><p>به صحرا، سرود اینچنین خارکن</p></div>
<div class="m2"><p>که از کندن خار، کس خوار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوانی و تدبیر و نیروت هست</p></div>
<div class="m2"><p>به دست تو، این کارها کار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بیداری و هوشیاری گرای</p></div>
<div class="m2"><p>چو دیدی که بخت تو بیدار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بفروختی، از که خواهی خرید</p></div>
<div class="m2"><p>متاع جوانی به بازار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوانی، گه کار و شایستگی است</p></div>
<div class="m2"><p>گه خودپسندی و پندار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبایست بر خیره از پا فتاد</p></div>
<div class="m2"><p>چو جان خسته و جسم بیمار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همین بس که از پا نیفتاده‌ای</p></div>
<div class="m2"><p>بس افتادگان را پرستار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مپیچ از ره راست، بر راه کج</p></div>
<div class="m2"><p>چو در هست، حاجت به دیوار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بازوی خود، خواه برگ و نوا</p></div>
<div class="m2"><p>ترا برگ و توشی در انبار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همی دانه و خوشه خروار شد</p></div>
<div class="m2"><p>ز آغاز، هر خوشه خروار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قوی پنجه‌ای، تیشه محکم بزن</p></div>
<div class="m2"><p>هنرمند مردم، سبکسار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زر وقت، باید به کار آزمود</p></div>
<div class="m2"><p>کزین بهترش، هیچ معیار نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غنیمت شمر، جز حقیقت مجوی</p></div>
<div class="m2"><p>که باری است فرصت، دگر بار نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی ناله کردی، ولی بی ثمر</p></div>
<div class="m2"><p>کس این ناله‌ها را خریدار نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو شب، هستی و صبحدم نیستی است</p></div>
<div class="m2"><p>شکایت ز هستی، سزاوار نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنند از تو در کار دل، باز پرس</p></div>
<div class="m2"><p>درین خانه، کس جز تو معمار نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشد جامهٔ عجب، جان را قبا</p></div>
<div class="m2"><p>درین جامه، پود ار بود، تار نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درین دکه، سود و زیان با همند</p></div>
<div class="m2"><p>کس از هر زیانی، زیانکار نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گهی کم بدست اوفتد، گه فزون</p></div>
<div class="m2"><p>بساز، ار درم هست و دینار نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مگوی از گرفتاری خویشتن</p></div>
<div class="m2"><p>ببین کیست آنکو گرفتار نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بچشم بصیرت بخود در نگر</p></div>
<div class="m2"><p>ترا تا در آئینه، زنگار نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه کار ایام، درس است و پند</p></div>
<div class="m2"><p>دریغا که شاگرد هشیار نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ترا بار تقدیر باید کشید</p></div>
<div class="m2"><p>کسی را رهائی از این بار نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدشواری ار دل شکیبا کنی</p></div>
<div class="m2"><p>ببینی که سهل است و دشوار نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از امروز اندوه فردا مخور</p></div>
<div class="m2"><p>نهان است فردا، پدیدار نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر آلود انگشتهایت به خون</p></div>
<div class="m2"><p>شگفتی ز ایام خونخوار نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو خارند گلهای هستی تمام</p></div>
<div class="m2"><p>گل است اینکه داری بکف، خار نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز آزادگان، بردباری و سعی</p></div>
<div class="m2"><p>بیاموز، آموختن عار نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هزاران ورق کرده گیتی سیاه</p></div>
<div class="m2"><p>شکایت همین چند طومار نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو خاطر نگهدار شو خویش را</p></div>
<div class="m2"><p>که ایام، خاطر نگهدار نیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ره زندگان است، عیبش مکن</p></div>
<div class="m2"><p>گر این راه، همواره هموار نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پی کارهائی که گوید برو</p></div>
<div class="m2"><p>ترا با فلک، دست پیکار نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به جایی که بار است بر پشت مور</p></div>
<div class="m2"><p>برای تو، این بار، بسیار نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نشاید که بیکار مانیم ما</p></div>
<div class="m2"><p>چو یک قطره و ذره بیکار نیست</p></div></div>