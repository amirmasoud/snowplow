---
title: >-
    شمارهٔ  ۴۶ - جان و تن
---
# شمارهٔ  ۴۶ - جان و تن

<div class="b" id="bn1"><div class="m1"><p>کودکی در بر، قبائی سرخ داشت</p></div>
<div class="m2"><p>روزگاری زان خوشی خوش میگذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو جان نیکو نگه میداشتش</p></div>
<div class="m2"><p>بهتر از لوزینه می‌پنداشتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم ضیاع و هم عقارش می‌شمرد</p></div>
<div class="m2"><p>هر زمان گرد و غبارش می‌سترد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نظر باز حسودش می‌نهفت</p></div>
<div class="m2"><p>سرخی اش میدید و چون گل میشکفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بدامانش سرشکی میچکید</p></div>
<div class="m2"><p>طفل خرد، آن اشک روشن میمکید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نخی از آستینش میشکافت</p></div>
<div class="m2"><p>بهر چاره سوی مادر میشتافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوبت بازی بصحرا و بدشت</p></div>
<div class="m2"><p>سرگران از پیش طفلان میگذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتنه افکند آن قبا اندر میان</p></div>
<div class="m2"><p>عاریت میخواستندش کودکان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمله دلها ماند پیش او گرو</p></div>
<div class="m2"><p>دوست میدارند طفلان رخت نو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وقت رفتن، پیشوای راه بود</p></div>
<div class="m2"><p>روز مهمانی و بازی، شاه بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کودکی از باغ می‌آورد به</p></div>
<div class="m2"><p>که بیا یک لحظه با من سوی ده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیگری آهسته نزدش می‌نشست</p></div>
<div class="m2"><p>تا زند بر آن قبای سرخ دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روزی، آن رهپوی صافی اندرون</p></div>
<div class="m2"><p>وقت بازی شد ز تلی واژگون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جامه‌اش از خار و سر از سنگ خست</p></div>
<div class="m2"><p>این یکی یکسر درید، آن یک شکست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طفل مسکین، بی خبر از سر که چیست</p></div>
<div class="m2"><p>پارگیهای قبا دید و گریست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از سرش گر جه بسی خوناب ریخت</p></div>
<div class="m2"><p>او برای جامه از چشم آب ریخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر بچشم دل ببینیم ای رفیق</p></div>
<div class="m2"><p>همچو آن طفلیم ما در این طریق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جامهٔ رنگین ما آز و هوی است</p></div>
<div class="m2"><p>هر چه بر ما میرسد از آز ماست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در هوس افزون و در عقل اندکیم</p></div>
<div class="m2"><p>سالها داریم اما کودکیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جان رها کردیم و در فکر تنیم</p></div>
<div class="m2"><p>تن بمرد و در غم پیراهنیم</p></div></div>