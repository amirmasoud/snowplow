---
title: >-
    شمارهٔ  ۱۴۹ - نامه به نوشیروان
---
# شمارهٔ  ۱۴۹ - نامه به نوشیروان

<div class="b" id="bn1"><div class="m1"><p>بزرگمهر، به نوشین‌روان نوشت که خلق</p></div>
<div class="m2"><p>ز شاه، خواهش امنیت و رفاه کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهان اگر که به تعمیر مملکت کوشند</p></div>
<div class="m2"><p>چه حاجت است که تعمیر بارگاه کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا کنند کم از دسترنج مسکینان</p></div>
<div class="m2"><p>چرا به مظلمه، افزون بمال و جاه کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو کج روی تو، نپویند دیگران ره راست</p></div>
<div class="m2"><p>چو یک خطا ز تو بینند، صد گناه کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به لشکر خرد و رای و عدل و علم گرای</p></div>
<div class="m2"><p>سپاه اهرمن، اندیشه زین سپاه کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جواب نامهٔ مظلوم را، تو خویش فرست</p></div>
<div class="m2"><p>بسا بود، که دبیرانت اشتباه کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمام کار، بدست تو چون سپرد سپهر</p></div>
<div class="m2"><p>به کار خلق، چرا دیگران نگاه کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر بدفتر حکام، ننگری یک روز</p></div>
<div class="m2"><p>هزار دفتر انصاف را سیاه کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر که قاضی و مفتی شوند، سفله و دزد</p></div>
<div class="m2"><p>دروغگو و بداندیش را گواه کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسمع شه نرسانند حاسدان قوی</p></div>
<div class="m2"><p>تظلمی که ضعیفان دادخواه کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بپوش چشم ز پندار و عجب، کاین دو شریک</p></div>
<div class="m2"><p>بر آن سرند، که تا فرصتی تباه کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو جای خود نشناسی، به حیله مدعیان</p></div>
<div class="m2"><p>تو را ز اوج بلندی، به قعر چاه کنند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بترس ز اه ستمدیدگان، که در دل شب</p></div>
<div class="m2"><p>نشسته‌اند که نفرین بپادشاه کنند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آن شرار که روشن شود ز سوز دلی</p></div>
<div class="m2"><p>بیک اشاره، دو صد کوه را چو کاه کنند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سند بدست سیه روزگار ظلم، بس است</p></div>
<div class="m2"><p>صحیفه‌ای که در آن، ثبت اشک و آه کنند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو شاه جور کند، خلق در امید نجات</p></div>
<div class="m2"><p>همی حساب شب و روز و سال و ماه کنند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هزار دزد، کمین کرده‌اند بر سر راه</p></div>
<div class="m2"><p>چنان مباش که بر موکب تو راه کنند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مخسب، تا که نپیچاند آسمانت گوش</p></div>
<div class="m2"><p>چنین معامله را بهر انتباه کنند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو، کیمیای بزرگی بجوی، بی‌خبران</p></div>
<div class="m2"><p>بهل، که قصه ز خاصیت گیاه کنند</p></div></div>