---
title: >-
    شمارهٔ  ۵۹ - دکان ریا
---
# شمارهٔ  ۵۹ - دکان ریا

<div class="b" id="bn1"><div class="m1"><p>اینچنین خواندم که روزی روبهی</p></div>
<div class="m2"><p>پایبند تله گشت اندر رهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیلهٔ روباهیش از یاد رفت</p></div>
<div class="m2"><p>خانهٔ تزویر را بنیاد رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه زائین سپهر آگاه بود</p></div>
<div class="m2"><p>هر چه بود، آن شیر و این روباه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیره روزش کرد، چرخ نیل فام</p></div>
<div class="m2"><p>تا شود روشن که شاگردیست خام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با همه تردستی، از پای اوفتاد</p></div>
<div class="m2"><p>دل به رنج و تن به بدبختی نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه در نیرنگ سازی داشت دست</p></div>
<div class="m2"><p>بند نیرنگ قضایش دست بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرص، با رسوائیش همراه کرد</p></div>
<div class="m2"><p>تیغ ذلت، ناخنش کوتاه کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود روز کار و یارائی نداشت</p></div>
<div class="m2"><p>بود وقت رفتن و پائی نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آهنی سنگین، دمش را کنده بود</p></div>
<div class="m2"><p>مرگ را میدید، اما زنده بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میفشردی اشکم ناهار را</p></div>
<div class="m2"><p>می‌گزیدی حلقه و مسمار را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دام تادیب است، دام روزگار</p></div>
<div class="m2"><p>هر که شد صیاد، آخر شد شکار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما کیانها کشته بود این روبهک</p></div>
<div class="m2"><p>زان سبب شد صید روباه فلک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خیرگیها کرده بود این خودپسند</p></div>
<div class="m2"><p>خیرگی را چاره زندانست و بند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ماکیانی ساده از ده دور گشت</p></div>
<div class="m2"><p>بر سر آن تله و روبه گذشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از بلای دام و زندان بی خبر</p></div>
<div class="m2"><p>گفت زان کیست این ایوان و در</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت روبه این در و ایوان ماست</p></div>
<div class="m2"><p>پوستین دوزیم و این دکان ماست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هست ما را بهتر از هر خواسته</p></div>
<div class="m2"><p>اندرین دکان، دمی آراسته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ساده و پاکیزه و زیبا و نرم</p></div>
<div class="m2"><p>همچو خز شایان و چون سنجاب گرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می‌فروشیم این دم پر پشم را</p></div>
<div class="m2"><p>باز کن وقت خریدن، چشم را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر دم ما را خریداری کنی</p></div>
<div class="m2"><p>همچو ما، یک عمر طراری کنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر ز مهر، این دم به بندیمت به دم</p></div>
<div class="m2"><p>راه را هرگز نخواهی کرد گم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر ز رسم و راه ما آگه شوی</p></div>
<div class="m2"><p>ماکیانی بس کنی، روبه شوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر که بربندی در چون و چرا</p></div>
<div class="m2"><p>سودها بینی در این بیع و شری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باید آن دم کژت کندن ز تن</p></div>
<div class="m2"><p>وین دم نیکو بجایش دوختن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ماکیان را این مقال آمد پسند</p></div>
<div class="m2"><p>گفت: بر گو دمت ای روباه چند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت باید دید کالا را نخست</p></div>
<div class="m2"><p>ور نه، این بیع و شری ناید درست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر خریداری، در آی اندر دکان</p></div>
<div class="m2"><p>نرخ، آنگه پرس از بازارگان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ماکیان را آن فریب از راه برد</p></div>
<div class="m2"><p>راست اندر تله روباه برد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کاش میدانست روبه ناشتاست</p></div>
<div class="m2"><p>وان نه دکان است، دکان ریاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا دهن بگشود بهر چند و چون</p></div>
<div class="m2"><p>چنگ روباه از گلویش ریخت خون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن دل فارغ، ز خون آکنده شد</p></div>
<div class="m2"><p>وان سر بی باک، از تن کنده شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ره ندیده، روی بر راهی نهاد</p></div>
<div class="m2"><p>چشم بسته، پای در چاهی نهاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هیچ نگرفت و گرفتند آنچه داشت</p></div>
<div class="m2"><p>هم گذشت از کار دم، هم سر گذاشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر سر آنست نفس حیله‌ساز</p></div>
<div class="m2"><p>که کند راهی سوی راه تو باز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا در آن ره، سربپیچاند ترا</p></div>
<div class="m2"><p>وندر آن آتش بسوزاند ترا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اهرمن هرگز نخواهد بست در</p></div>
<div class="m2"><p>تا ترا میافتد از کویش گذر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در جوارت، حرص زان دکان گشود</p></div>
<div class="m2"><p>که تو بر بندی دکان خویش زود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا شوی بیدار، رفتست آنچه هست</p></div>
<div class="m2"><p>تا بدانی کیستی، رفتی ز دست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>با مسافر، دزد چون گردید دوست</p></div>
<div class="m2"><p>زاد و برگ آن مسافر زان اوست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گوهر کان هوی جز سنگ نیست</p></div>
<div class="m2"><p>آب و رنگش جز فریب و رنگ نیست</p></div></div>