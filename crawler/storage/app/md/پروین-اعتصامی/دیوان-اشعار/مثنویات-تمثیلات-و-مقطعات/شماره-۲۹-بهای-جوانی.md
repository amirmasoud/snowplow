---
title: >-
    شمارهٔ  ۲۹ - بهای جوانی
---
# شمارهٔ  ۲۹ - بهای جوانی

<div class="b" id="bn1"><div class="m1"><p>خمید نرگس پژمرده‌ای ز انده و شرم</p></div>
<div class="m2"><p>چو دید جلوهٔ گلهای بوستانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکند بر گل خودروی دیدهٔ امید</p></div>
<div class="m2"><p>نهفته گفت بدو این غم نهانی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که بر نکرده سر از خاک، در بسیط زمین</p></div>
<div class="m2"><p>شدم نشانه بلاهای آسمانی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا به سفرهٔ خالی زمانه مهمان کرد</p></div>
<div class="m2"><p>ندیده چشم کس اینگونه میهمانی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبیب باد صبا را بگوی از ره مهر</p></div>
<div class="m2"><p>که تا دوا کند این درد ناگهانی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز کاردانی دیروز من چه سود امروز</p></div>
<div class="m2"><p>چو کار نیست، چه تاثیر کاردانی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چشم خیرهٔ ایام هر چه خیره شدم</p></div>
<div class="m2"><p>ندید دیدهٔ من روی مهربانی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من از صبا و چمن بدگمان نمیگشتم</p></div>
<div class="m2"><p>زمانه در دلم افکند بدگمانی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان خوشند گل و ارغوان که پنداری</p></div>
<div class="m2"><p>خریده‌اند همه ملک شادمانی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکستم و نشد آگاه باغبان قضا</p></div>
<div class="m2"><p>نخوانده بود مگر درس باغبانی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بمن جوانی خود را بسیم و زر بفروش</p></div>
<div class="m2"><p>که زر و سیم کلید است کامرانی را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جواب داد که آئین روزگار اینست</p></div>
<div class="m2"><p>بسی بلند و پستی است زندگانی را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بکس نداد توانائی این سپهر بلند</p></div>
<div class="m2"><p>که از پیش نفرستاد ناتوانی را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هنوز تازه رسیدی و اوستاد فلک</p></div>
<div class="m2"><p>نگفته بهر تو اسرار باستانی را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در آن مکان که جوانی دمی و عمر شبی است</p></div>
<div class="m2"><p>بخیره میطلبی عمر جاودانی را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نهان هر گل و بهر سبزه‌ای دو صد معنی است</p></div>
<div class="m2"><p>بجز زمانه نداند کس این معانی را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز گنج وقت، نوائی ببر که شبرو دهر</p></div>
<div class="m2"><p>برایگان برد این گنج رایگانی را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز رنگ سرخ گل ارغوان مشو دلتنگ</p></div>
<div class="m2"><p>خزان سیه کند آن روی ارغوانی را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرانبهاست گل اندر چمن ولی مشتاب</p></div>
<div class="m2"><p>بدل کنند به ارزانی این گرانی را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زمانه بر تن ریحان و لاله و نسرین</p></div>
<div class="m2"><p>بسی دریده قباهای پرنیانی را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من و تو را ببرد دزد چرخ پیر، از آنک</p></div>
<div class="m2"><p>ز دزد خواسته بودیم پاسبانی را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چمن چگونه رهد ز آفت دی و بهمن</p></div>
<div class="m2"><p>صبا چه چاره کند باد مهرگانی را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو زر و سیم نگهدار کاندرین بازار</p></div>
<div class="m2"><p>بسیم و زر نخریده است کس جوانی را</p></div></div>