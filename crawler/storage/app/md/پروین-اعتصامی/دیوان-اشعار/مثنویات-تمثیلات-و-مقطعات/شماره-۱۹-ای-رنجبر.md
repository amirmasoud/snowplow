---
title: >-
    شمارهٔ  ۱۹ - ای رنجبر
---
# شمارهٔ  ۱۹ - ای رنجبر

<div class="b" id="bn1"><div class="m1"><p>تا بکی جان کندن اندر آفتاب ای رنجبر</p></div>
<div class="m2"><p>ریختن از بهر نان از چهر آب ای رنجبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینهمه خواری که بینی زافتاب و خاک و باد</p></div>
<div class="m2"><p>چیست مزدت جز نکوهش یا عتاب ای رنجبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حقوق پایمال خویشتن کن پرسشی</p></div>
<div class="m2"><p>چند میترسی ز هر خان و جناب ای رنجبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمله آنان را که چون زالو مکندت خون بریز</p></div>
<div class="m2"><p>وندران خون دست و پائی کن خضاب ای رنجبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیو آز و خودپرستی را بگیر و حبس کن</p></div>
<div class="m2"><p>تا شود چهر حقیقت بی حجاب ای رنجبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاکم شرعی که بهر رشوه فتوی میدهد</p></div>
<div class="m2"><p>کی دهد عرض فقیران را جواب ای رنجبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه خود را پاک میداند ز هر آلودگی</p></div>
<div class="m2"><p>میکند مردار خواری چون غراب ای رنجبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر که اطفال تو بی شامند شبها باک نیست</p></div>
<div class="m2"><p>خواجه تیهو می‌کند هر شب کباب ای رنجبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چراغت را نبخشیده‌است گردون روشنی</p></div>
<div class="m2"><p>غم مخور، میتابد امشب ماهتاب ای رنجبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خور دانش امیرانند و فرزندانشان</p></div>
<div class="m2"><p>تو چه خواهی فهم کردن از کتاب ای رنجبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مردم آنانند کز حکم و سیاست آگهند</p></div>
<div class="m2"><p>کارگر کارش غم است و اضطراب ای رنجبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که پوشد جامهٔ نیکو بزرگ و لایق اوست</p></div>
<div class="m2"><p>رو تو صدها وصله داری بر ثیاب ای رنجبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جامه‌ات شوخ است و رویت تیره رنگ از گرد و خاک</p></div>
<div class="m2"><p>از تو میبایست کردن اجتناب ای رنجبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر چه بنویسند حکام اندرین محضر رواست</p></div>
<div class="m2"><p>کس نخواهد خواستن زیشان حساب ای رنجبر</p></div></div>