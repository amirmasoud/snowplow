---
title: >-
    شمارهٔ  ۵۸ - دزد و قاضی
---
# شمارهٔ  ۵۸ - دزد و قاضی

<div class="b" id="bn1"><div class="m1"><p>برد دزدی را سوی قاضی عسس</p></div>
<div class="m2"><p>خلق بسیاری روان از پیش و پس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت قاضی کاین خطاکاری چه بود</p></div>
<div class="m2"><p>دزد گفت از مردم آزاری چه سود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت، بدکردار را بد کیفر است</p></div>
<div class="m2"><p>گفت، بدکار از منافق بهتر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت، هان بر گوی شغل خویشتن</p></div>
<div class="m2"><p>گفت، هستم همچو قاضی راهزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت، آن زرها که بردستی کجاست</p></div>
<div class="m2"><p>گفت، در همیان تلبیس شماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت، آن لعل بدخشانی چه شد</p></div>
<div class="m2"><p>گفت، می‌دانیم و می‌دانی چه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت، پیش کیست آن روشن نگین</p></div>
<div class="m2"><p>گفت، بیرون آر دست از آستین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دزدی پنهان و پیدا، کار توست</p></div>
<div class="m2"><p>مال دزدی، جمله در انبار توست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو قلم بر حکم داور می‌بری</p></div>
<div class="m2"><p>من ز دیوار و تو از در می‌بری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حد بگردن داری و حد می‌زنی</p></div>
<div class="m2"><p>گر یکی باید زدن، صد می‌زنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌زنم گر من ره خلق، ای رفیق</p></div>
<div class="m2"><p>در ره شرعی تو قطاع الطریق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می‌برم من جامهٔ درویش عور</p></div>
<div class="m2"><p>تو ربا و رشوه می‌گیری بزور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست من بستی برای یک گلیم</p></div>
<div class="m2"><p>خود گرفتی خانه از دست یتیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من ربودم موزه و طشت و نمد</p></div>
<div class="m2"><p>تو سیهدل مدرک و حکم و سند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دزد جاهل، گر یکی ابریق برد</p></div>
<div class="m2"><p>دزد عارف، دفتر تحقیق برد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دیده‌های عقل، گر بینا شوند</p></div>
<div class="m2"><p>خود فروشان زودتر رسوا شوند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دزد زر بستند و دزد دین رهید</p></div>
<div class="m2"><p>شحنه ما را دید و قاضی را ندید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من براه خود ندیدم چاه را</p></div>
<div class="m2"><p>تو بدیدی، کج نکردی راه را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می‌زدی خود، پشت پا بر راستی</p></div>
<div class="m2"><p>راستی از دیگران می‌خواستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دیگر ای گندم نمای جو فروش</p></div>
<div class="m2"><p>با ردای عجب، عیب خود مپوش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چیره‌دستان می‌ربایند آنچه هست</p></div>
<div class="m2"><p>می‌برند آنگه ز دزد کاه، دست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در دل ما حرص، آلایش فزود</p></div>
<div class="m2"><p>نیت پاکان چرا آلوده بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دزد اگر شب، گرم یغما کردنست</p></div>
<div class="m2"><p>دزدی حکام، روز روشن است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حاجت ار ما را ز راه راست برد</p></div>
<div class="m2"><p>دیو، قاضی را بهرجا خواست برد</p></div></div>