---
title: >-
    شمارهٔ  ۳۵ - پیام گل
---
# شمارهٔ  ۳۵ - پیام گل

<div class="b" id="bn1"><div class="m1"><p>به آب روان گفت گل کز تو خواهم</p></div>
<div class="m2"><p>که رازی که گویم به بلبل بگوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیام ار فرستد، پیامش بیاری</p></div>
<div class="m2"><p>بخاک ار درافتد، غبارش بشوئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگوئی که ما را بود دیده بر ره</p></div>
<div class="m2"><p>که فردا بیائی و ما را ببوئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگفتا به جوی آب رفته نیاید</p></div>
<div class="m2"><p>نیابی مرا، گر چه عمری بجوئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیامی که داری به پیک دگر ده</p></div>
<div class="m2"><p>بامید من هرگز این ره نپوئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از جوی چون بگذرم برنگردم</p></div>
<div class="m2"><p>چو پژمرده گشتی تو، دیگر نروئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بفردا چه میافکنی کار امروز</p></div>
<div class="m2"><p>بخوان آنکسی را که مشتاق اوئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بد اندیشه گیتی بناگه بدزدد</p></div>
<div class="m2"><p>ز بلبل خوشی و ز گل خوبروئی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو فردا شود، دیگرت کس نبوید</p></div>
<div class="m2"><p>که بی رنگ و بی بوی، چون خاک کوئی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل از آرزو یکنفس بود خرم</p></div>
<div class="m2"><p>تو اندر دل باغ، چون آرزوئی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو آب روان خوش کن این مرز و بگذر</p></div>
<div class="m2"><p>تو مانند آبی که اکنون به جوئی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نکو کار شو تا توانی، که دائم</p></div>
<div class="m2"><p>نمانداست در روی نیکو، نکوئی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو پاکیزه خو را شکیبی نباشد</p></div>
<div class="m2"><p>چو گردون گردان کند تندخوئی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نبیند گه سختی و تنگدستی</p></div>
<div class="m2"><p>ز یاران یکدل، کسی جز دوروئی</p></div></div>