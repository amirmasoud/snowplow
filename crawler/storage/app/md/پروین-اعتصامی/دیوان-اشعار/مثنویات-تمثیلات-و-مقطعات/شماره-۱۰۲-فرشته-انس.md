---
title: >-
    شمارهٔ  ۱۰۲ - فرشتهٔ انس
---
# شمارهٔ  ۱۰۲ - فرشتهٔ انس

<div class="b" id="bn1"><div class="m1"><p>در آن سرای که زن نیست، انس و شفقت نیست</p></div>
<div class="m2"><p>در آن وجود که دل مرده، مرده است روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهیچ مبحث و دیباچه‌ای، قضا ننوشت</p></div>
<div class="m2"><p>برای مرد کمال و برای زن نقصان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زن از نخست بود رکن خانهٔ هستی</p></div>
<div class="m2"><p>که ساخت خانهٔ بی پای بست و بی بنیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زن ار براه متاعت نمیگداخت چو شمع</p></div>
<div class="m2"><p>نمیشناخت کس این راه تیره را پایان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مهر، گر که نمیتافت زن بکوه وجود</p></div>
<div class="m2"><p>نداشت گوهری عشق، گوهر اندر کان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرشته بود زن، آن ساعتی که چهره نمود</p></div>
<div class="m2"><p>فرشته بین، که برو طعنه میزند شیطان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر فلاطن و سقراط، بوده‌اند بزرگ</p></div>
<div class="m2"><p>بزرگ بوده پرستار خردی ایشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگاهوارهٔ مادر، بکودکی بس خفت</p></div>
<div class="m2"><p>سپس بمکتب حکمت، حکیم شد لقمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه پهلوان و چه سالک، چه زاهد و چه فقیه</p></div>
<div class="m2"><p>شدند یکسره، شاگرد این دبیرستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حدیث مهر، کجا خواند طفل بی مادر</p></div>
<div class="m2"><p>نظام و امن، کجا یافت ملک بی سلطان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وظیفهٔ زن و مرد، ای حکیم، دانی چیست</p></div>
<div class="m2"><p>یکیست کشتی و آن دیگریست کشتیبان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو ناخداست خردمند و کشتیش محکم</p></div>
<div class="m2"><p>دگر چه باک ز امواج و ورطه و طوفان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بروز حادثه، اندر یم حوادث دهر</p></div>
<div class="m2"><p>امید سعی و عملهاست، هم ازین، هم ازان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همیشه دختر امروز، مادر فرداست</p></div>
<div class="m2"><p>ز مادرست میسر، بزرگی پسران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر رفوی زنان نکو نبود، نداشت</p></div>
<div class="m2"><p>بجز گسیختگی، جامهٔ نکو مردان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>توان و توش ره مرد چیست، یاری زن</p></div>
<div class="m2"><p>حطام و ثروت زن چیست، مهر فرزندان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زن نکوی، نه بانوی خانه تنها بود</p></div>
<div class="m2"><p>طبیب بود و پرستار و شحنه و دربان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بروزگار سلامت، رفیق و یار شفیق</p></div>
<div class="m2"><p>بروز سانحه، تیمارخوار و پشتیبان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بیش و کم، زن دانا نکرد روی ترش</p></div>
<div class="m2"><p>بحرف زشت، نیالود نیکمرد دهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سمند عمر، چو آغاز بدعنانی کرد</p></div>
<div class="m2"><p>گهیش مرد و زمانیش زن، گرفت عنان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه زن، چه مرد، کسی شد بزرگ و کامروا</p></div>
<div class="m2"><p>که داشت میوه‌ای از باغ علم، در دامان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به رستهٔ هنر و کارخانهٔ دانش</p></div>
<div class="m2"><p>متاعهاست، بیا تا شویم بازرگان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زنی که گوهر تعلیم و تربیت نخرید</p></div>
<div class="m2"><p>فروخت گوهر عمر عزیز را ارزان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کسیست زنده که از فضل، جامه‌ای پوشد</p></div>
<div class="m2"><p>نه آنکه هیچ نیرزد، اگر شود عریان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هزار دفتر معنی، بما سپرد فلک</p></div>
<div class="m2"><p>تمام را بدریدیم، بهر یک عنوان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خرد گشود چو مکتب، شدیم ما کودن</p></div>
<div class="m2"><p>هنر چو کرد تجلی، شدیم ما پنهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بساط اهرمن خودپرستی و سستی</p></div>
<div class="m2"><p>گر از میان نرود، رفته‌ایم ما ز میان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همیشه فرصت ما، صرف شد درین معنی</p></div>
<div class="m2"><p>که نرخ جامهٔ بهمان چه بود و کفش فلان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برای جسم، خریدیم زیور پندار</p></div>
<div class="m2"><p>برای روح، بریدیم جامهٔ خذلان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قماش دکهٔ جان را، بعجب پوساندیم</p></div>
<div class="m2"><p>بهر کنار گشودیم بهر تن، دکان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه رفعتست، فساد است این رویه، فساد</p></div>
<div class="m2"><p>نه عزتست، هوانست این عقیده، هوان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه سبزه‌ایم، که روئیم خیره در جر و جوی</p></div>
<div class="m2"><p>نه مرغکیم، که باشیم خوش بمشتی دان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو بگرویم به کرباس خود، چه غم داریم</p></div>
<div class="m2"><p>که حلهٔ حلب ارزان شدست یا که گران</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از آن حریر که بیگانه بود نساجش</p></div>
<div class="m2"><p>هزار بار برازنده‌تر بود خلقان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه حله‌ایست گرانتر ز حلیت دانش</p></div>
<div class="m2"><p>چه دیبه‌ایست نکوتر ز دیبهٔ عرفان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر آن گروهه که پیچیده شد بدوک خرد</p></div>
<div class="m2"><p>به کارخانهٔ همت، حریر گشت و کتان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه بانوست که خود را بزرگ میشمرد</p></div>
<div class="m2"><p>بگوشواره و طوق و بیارهٔ مرجان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو آب و رنگ فضیلت بچهره نیست چه سود</p></div>
<div class="m2"><p>ز رنگ جامهٔ زربفت و زیور رخشان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برای گردن و دست زن نکو، پروین</p></div>
<div class="m2"><p>سزاست گوهر دانش، نه گوهر الوان</p></div></div>