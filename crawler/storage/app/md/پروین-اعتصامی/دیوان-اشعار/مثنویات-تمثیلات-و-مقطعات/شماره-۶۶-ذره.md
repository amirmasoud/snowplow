---
title: >-
    شمارهٔ  ۶۶ - ذره
---
# شمارهٔ  ۶۶ - ذره

<div class="b" id="bn1"><div class="m1"><p>شنیده‌اید که روزی بچشمهٔ خورشید</p></div>
<div class="m2"><p>برفت ذره بشوقی فزون بمهمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرفته نیمرهی، باد سرنگونش کرد</p></div>
<div class="m2"><p>سبک قدم نشده، دید بس گرانجانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی، رونده سحابی گرفت چهرهٔ مهر</p></div>
<div class="m2"><p>گهی، هوا چو یم عشق گشت طوفانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار قطرهٔ باران چکید بر رویش</p></div>
<div class="m2"><p>جفا کشید بس، از رعد و برق نیسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار گونه بلندی، هزار پستی دید</p></div>
<div class="m2"><p>که تا رسید به آن بزمگاه نورانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمود دیر زمانی به آفتاب نگاه</p></div>
<div class="m2"><p>ملول گشت سرانجام زان هوسرانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهر دید و بلندی و پرتو و پاکی</p></div>
<div class="m2"><p>بدوخت دیدهٔ خودبین، ز فرط حیرانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سئوال کرد ز خورشید کاین چه روشنی است</p></div>
<div class="m2"><p>در این فضا، که ترا میکند نگهبانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بذره گفت فروزنده مهر، کاین رمزیست</p></div>
<div class="m2"><p>برون ز عالم تدبیر و فکر امکانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بتخت و تاج سلیمان، چکار مورچه را</p></div>
<div class="m2"><p>بس است ایمنی کشور سلیمانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من از گذشتن ابری ضعیف، تیره شوم</p></div>
<div class="m2"><p>تو از وزیدن بادی، ز کار درمانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه مقصد است، که گردد عیان ز نیمهٔ راه</p></div>
<div class="m2"><p>نه مشکل است، که آسان شود بسانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هزار سال اگر علم و حکمت آموزی</p></div>
<div class="m2"><p>هزار قرن اگر درس معرفت خوانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بپوئی ار همهٔ راههای تیره و تار</p></div>
<div class="m2"><p>بدانی ار همهٔ رازهای پنهانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر بعقل و هنر، همسر فلاطونی</p></div>
<div class="m2"><p>وگر بدانش و فضل، اوستاد لقمانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسمان حقیقت، بهیچ پر نپری</p></div>
<div class="m2"><p>به خلوت احدیت، رسید نتوانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در آنزمان که رسی عاقبت بحد کمال</p></div>
<div class="m2"><p>چو نیک در نگری در کمال نقصانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گشود گوهری عقل گر چه بس کانها</p></div>
<div class="m2"><p>نیافت هیچگه این پاک گوهر کانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ده جهان اگر ایدوست دهخدای نداشت</p></div>
<div class="m2"><p>که مینمود تحمل به رنج دهقانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بلند خیز مشو، زانکه حاصلی نبری</p></div>
<div class="m2"><p>بخز فتادن و درماندن و پشیمانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بکوی شوق، گذاری نمیکنی، پروین</p></div>
<div class="m2"><p>چو ذره نیز ره و رسم را نمیدانی</p></div></div>