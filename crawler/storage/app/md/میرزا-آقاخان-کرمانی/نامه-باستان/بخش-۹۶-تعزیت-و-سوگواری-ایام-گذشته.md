---
title: >-
    بخش ۹۶ - تعزیت و سوگواری ایام گذشته
---
# بخش ۹۶ - تعزیت و سوگواری ایام گذشته

<div class="b" id="bn1"><div class="m1"><p>کجات آن همه رسم و آیین و راه؟</p></div>
<div class="m2"><p>کجات افسر و گنج و ملک و سپاه؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجات آن همه دانش و زور و دست؟</p></div>
<div class="m2"><p>کجات آن بزرگان خسرو پرست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجات آن نبرده یلان دلیر؟</p></div>
<div class="m2"><p>که شیر ژیان آوریدند زیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجات آن سواران زرین ستام؟</p></div>
<div class="m2"><p>که دشمن بدی تیغ شان را نیام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجات آن همه مردی و زور و فر؟</p></div>
<div class="m2"><p>که گیتی همه داشتی زیر پر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجات آن بزرگی و آن دستگاه؟</p></div>
<div class="m2"><p>که سربرکشیدی زماهی به ماه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجات افسر و کاویانی درفش؟</p></div>
<div class="m2"><p>کجات آن همه تیغ های بنفش؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجات آن به رزم اندرون فرونام؟</p></div>
<div class="m2"><p>کجات آن به بزم اندرون کام و جام؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کجات آن دلیران روز نبرد؟</p></div>
<div class="m2"><p>کجات آن بزرگان بادار و برد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجات آن کمین و کمان و کمند؟</p></div>
<div class="m2"><p>که کردی همه دیو و جادو به بند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کجات آن فزونی گنج و سپاه؟</p></div>
<div class="m2"><p>کجات افسر و تخت و فرو کلاه؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کجات آن سواران و میدان و گوی؟</p></div>
<div class="m2"><p>که زآن ها به گیتی بدی گفتگوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کجات آن دلیران و مردانگی؟</p></div>
<div class="m2"><p>هش و رأی و فرهنگ و فرزانگی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کجات آن هنرهای بیش از شمار؟</p></div>
<div class="m2"><p>که علم و هنر از تو شد یادگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کجا شد دل و هوش و آئین تو؟</p></div>
<div class="m2"><p>توانائی و اختر و دین تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کجا شد به رزم آن نکوساز تو؟</p></div>
<div class="m2"><p>کجا شد به بزم آن خوش آواز تو؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کجا رفت آن جام گیتی نمای؟</p></div>
<div class="m2"><p>کجات آن همه خسرو پاک رای؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کجا رفت آن اختر کاویان؟</p></div>
<div class="m2"><p>کجا رفت اورنگ فرکیان؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که اکنون به پستی نیاز آمدت</p></div>
<div class="m2"><p>چنین اختر بد فراز آمدت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که بنشاند این شمع افروخته؟</p></div>
<div class="m2"><p>کزو شد همه دودمان سوخته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دریغ آن بلند اختر و رای تو</p></div>
<div class="m2"><p>دریغ آن سر عرش فرسای تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دریغ آن یلان و کیان جهان</p></div>
<div class="m2"><p>که بودی پناه کهان و مهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دریغ آن بزرگان والا گهر</p></div>
<div class="m2"><p>به مردی زشاهان برآورده سر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دریغ آن امیران والا به شأن</p></div>
<div class="m2"><p>کز ایشان به گیتی نمانده نشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هم اکنون از ایشان نبینم به جای</p></div>
<div class="m2"><p>به جز ناظم الدوله ی پاک رای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ابا چند تن از مهان گزین</p></div>
<div class="m2"><p>که از آسمان شان رسد آفرین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شده آدمیت از ایشان پدید</p></div>
<div class="m2"><p>همه گنج های وفا را کلید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه سال شان بخت و پیروز باد</p></div>
<div class="m2"><p>همه روزشان روز نیروز باد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نگهدارشان باد زروان پاک</p></div>
<div class="m2"><p>بود یارشان هرمز تابناک</p></div></div>