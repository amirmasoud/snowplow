---
title: >-
    بخش ۲۲ - برداشتن سیروس دل از جهان و گوشه گزیدن
---
# بخش ۲۲ - برداشتن سیروس دل از جهان و گوشه گزیدن

<div class="b" id="bn1"><div class="m1"><p>به بلخ اندر آتشکده نوبهار</p></div>
<div class="m2"><p>سروتن بشست آن شه نامدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمی شد ز پیکار و سیر از بدی</p></div>
<div class="m2"><p>بر او تافت یک خوره ایزدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پر اندیشه شد مایه ور جان شاه</p></div>
<div class="m2"><p>از آن ایزدی کاروان دستگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی گفت هر جای آباد بوم</p></div>
<div class="m2"><p>زهندوستان تا به یونان و روم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفتم به نیروی شاهنشهی</p></div>
<div class="m2"><p>مرا گشت فرمان و تخت مهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون آن به آید که من راهجوی</p></div>
<div class="m2"><p>شوم پیش یزدان پر از آبروی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روانم بر آن جای نیکان برد</p></div>
<div class="m2"><p>که این تاج و تخت کئی بگذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مبادا که آرد روانم منی</p></div>
<div class="m2"><p>بد اندیشد و کین اهریمنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بپوشید پس جامه ی نو سپید</p></div>
<div class="m2"><p>نیایش کنان رفت دل پر امید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زایوان به جای پرستش برفت</p></div>
<div class="m2"><p>دل از تخت شاهنشهی برگرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه کشور خویشتن سر به سر</p></div>
<div class="m2"><p>بدو نیمه کرد آن شه نامور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی نیمه کاوس کی را بداد</p></div>
<div class="m2"><p>دگر برته ی گرد والانژاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به کوه اندرون داشت برته مقام</p></div>
<div class="m2"><p>همی کهبدی ساختی صبح و شام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازو بود کاوس مهتر به سال</p></div>
<div class="m2"><p>نمی خواست کس را به گیتی همال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نهانی فرستاد و او را بکشت</p></div>
<div class="m2"><p>که سرتاسر کشور آرد به مشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سمردیس خواندندی او را به نام</p></div>
<div class="m2"><p>همی زنده پنداشتندش عوام</p></div></div>