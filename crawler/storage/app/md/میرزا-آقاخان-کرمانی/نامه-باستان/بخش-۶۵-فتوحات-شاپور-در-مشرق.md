---
title: >-
    بخش ۶۵ - فتوحات شاپور در مشرق
---
# بخش ۶۵ - فتوحات شاپور در مشرق

<div class="b" id="bn1"><div class="m1"><p>پس از جنگ با رومیان پادشاه</p></div>
<div class="m2"><p>به تاتار و هندوستان جست راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسی سروران را سر آورد پیش</p></div>
<div class="m2"><p>بیفزود بر وسعت ملک خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وز آن جا سوی ارمنستان شتافت</p></div>
<div class="m2"><p>که ارزاس از رأی او سر بتافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکشت آن جهان جوی را خوار و زار</p></div>
<div class="m2"><p>سر آورد بر پارتی روزگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی شارسان ساخت در بیستون</p></div>
<div class="m2"><p>که پیروز شاپور خوانی کنون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر شارسان خرم آباد بود</p></div>
<div class="m2"><p>که اهل لرستان بدو شاد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به استرخ کرد آن سوم شارسان</p></div>
<div class="m2"><p>بدو اندرون کاخ و بیمارسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر جای بنگاشته پیکرش</p></div>
<div class="m2"><p>که قیصر چو بنده ستاده برش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین نام خود را نموده است یاد</p></div>
<div class="m2"><p>که هستم شه آسمانی نژاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو شد مالیاتش به هفتاد و اند</p></div>
<div class="m2"><p>نگون گشت بختش زچرخ بلند</p></div></div>