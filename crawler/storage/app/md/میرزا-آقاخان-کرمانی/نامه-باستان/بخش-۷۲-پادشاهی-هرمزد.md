---
title: >-
    بخش ۷۲ - پادشاهی هرمزد
---
# بخش ۷۲ - پادشاهی هرمزد

<div class="b" id="bn1"><div class="m1"><p>پسر بد مر او را دو فرخنده کام</p></div>
<div class="m2"><p>که پیروز و هرمزدشان بود نام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پیروز که بود هرمزد گرد</p></div>
<div class="m2"><p>ولیکن پدر شاهی او را سپرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کز او دید نرمی و آهستگی</p></div>
<div class="m2"><p>خردمندی و شرم و شایستگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هرمزد سپرد تخت و نگین</p></div>
<div class="m2"><p>همان لشکر و گنج ایران زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمی گشت پیروز از کار شاه</p></div>
<div class="m2"><p>سوی شاه هیتالیان جست راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که قوم فتالیت خوانندشان</p></div>
<div class="m2"><p>هم از هون اسپید دانندشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چغانی شهی بد فغانیش نام</p></div>
<div class="m2"><p>جهان جوی و با لشگر و نام و کام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدو داد شمشیر زن سی هزار</p></div>
<div class="m2"><p>به ایران بیاید سوی کارزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو هرمزد روی برادر بدید</p></div>
<div class="m2"><p>دلش مهر و پیوند او برگزید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیاده شد از اسب و بردش نماز</p></div>
<div class="m2"><p>بدو تاج و تخت کئی داد باز</p></div></div>