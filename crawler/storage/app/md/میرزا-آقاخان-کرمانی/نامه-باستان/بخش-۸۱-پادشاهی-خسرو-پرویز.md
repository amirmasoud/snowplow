---
title: >-
    بخش ۸۱ - پادشاهی خسرو پرویز
---
# بخش ۸۱ - پادشاهی خسرو پرویز

<div class="b" id="bn1"><div class="m1"><p>چو بر تخت بنشست پرویز شاه</p></div>
<div class="m2"><p>بیاراست جشنی چو تابنده ماه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برویش ببخشید بسیار چیز</p></div>
<div class="m2"><p>مهان را نوازش بسی کرد نیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سران به مهمانی خویش خواند</p></div>
<div class="m2"><p>همه بندیان را ز زندان رهاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرستاده اش نزد بهرام گرد</p></div>
<div class="m2"><p>همه گونه تشریف شایسته برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی گفت گر باز آید به راه</p></div>
<div class="m2"><p>نمایمش سالار ایران سپاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپهدار ایرانش خواهم بداد</p></div>
<div class="m2"><p>نیارم ز کردار او هیچ یاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرستاده را گفت بهرام گرد</p></div>
<div class="m2"><p>که سازم به خسرو یکی دستبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که از روسبی باشد او را نژاد</p></div>
<div class="m2"><p>بر او روزگار بزرگی مباد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الان شاه چون شهریاری کند</p></div>
<div class="m2"><p>که بر کشتن شاه یاری کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منم شاه دین دار والا گهر</p></div>
<div class="m2"><p>همان دشمن مرد بیدادگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو خسرو زبهرام پاسخ شنید</p></div>
<div class="m2"><p>رخش گشت هم چون گل شنبلید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدانست کان دو دکون دراز</p></div>
<div class="m2"><p>نگردد بدین گفته از راه باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به لشگر بسی داستان ها بخواند</p></div>
<div class="m2"><p>وز آن پس سپه سوی نیزیب زاند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی خواست خسرو شبیخون کند</p></div>
<div class="m2"><p>جهان را به بهرام وارون کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر او تاخت آورد بهرام نیز</p></div>
<div class="m2"><p>ندید ایچ چاره بغیر از گریز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تنی چند بودند همراه اوی</p></div>
<div class="m2"><p>که خسرو سوی راه آورد روی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گذشت از بیابان شط فرات</p></div>
<div class="m2"><p>به سیرسیزیوم آمد از آن فلات</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پروتوس کو بد به دز کوتوال</p></div>
<div class="m2"><p>پذیرفت شه را به فرخنده فال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دگر روز خسرو به درد و فسوس</p></div>
<div class="m2"><p>یکی نامه بنوشت زی موریوس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که او بود قیصر در آن روزگار</p></div>
<div class="m2"><p>ازو خواست یاری پی کارزار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیاراست موریس یک انجمن</p></div>
<div class="m2"><p>در آن کار شد با سران رای زن</p></div></div>