---
title: >-
    بخش ۹۱ - پادشاهی فرخ زاد
---
# بخش ۹۱ - پادشاهی فرخ زاد

<div class="b" id="bn1"><div class="m1"><p>زجهرم فرخ زاد را خوانده اند</p></div>
<div class="m2"><p>ابر تخت شاهیش بنشانده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که او بود از تخم شاپور شاه</p></div>
<div class="m2"><p>به جهرم همی داشت آرامگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو یک ماه بر شاهی او گذشت</p></div>
<div class="m2"><p>به دست یکی بنده اش کشته گشت</p></div></div>