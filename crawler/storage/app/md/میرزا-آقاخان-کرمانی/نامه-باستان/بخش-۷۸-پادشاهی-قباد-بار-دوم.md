---
title: >-
    بخش ۷۸ - پادشاهی قباد بار دوم
---
# بخش ۷۸ - پادشاهی قباد بار دوم

<div class="b" id="bn1"><div class="m1"><p>چو آمد به تخت کیی بر نشست</p></div>
<div class="m2"><p>مهان جمله گشتند خسرو پرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مغان را بپرسید و بنواختشان</p></div>
<div class="m2"><p>به نزدیک خود جایگه ساختشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرستاد جاماسب را سوی دز</p></div>
<div class="m2"><p>گونشتاد را کشت مانند بز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که او کشتن شاه همی خواستی</p></div>
<div class="m2"><p>به گاهی که مجلس بیاراستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جایش یکی را برافراخت کام</p></div>
<div class="m2"><p>که آذر کود و نباد بودیش نام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآورد نام سیوسس به ماه</p></div>
<div class="m2"><p>سپاهی و کشور بدو داد شاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قباد اندر ایران چو شد کدخدا</p></div>
<div class="m2"><p>همی راند کار جهان سوخرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به شهنامه از گفته ی باستان</p></div>
<div class="m2"><p>دگرگونه راند همی داستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که در قید هیتالیان بد قباد</p></div>
<div class="m2"><p>رهانید پس سوخرایش بداد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیاورد و بر تخت بنشاندش</p></div>
<div class="m2"><p>به ایوان یکی شاه نو خواندش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه کارها با سوخرا راندی</p></div>
<div class="m2"><p>کسی را بر شاه نفشاندی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازو گشت بد دل جهان جو قباد</p></div>
<div class="m2"><p>به گفتار شاپور مهرک نژاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به بندش درآورد و زندان نمود</p></div>
<div class="m2"><p>وزان پس ورا زود بی جان نمود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازو هر کسی را دل آمد به درد</p></div>
<div class="m2"><p>برآشفت ایران و برخاست گرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دو پایش به آهن ببستند سخت</p></div>
<div class="m2"><p>نشاندند جاماسب را روی تخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپردند شه را به زرمهر شیر</p></div>
<div class="m2"><p>که بد زاده سوخرای دلیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که آن نامور کینه سوخرای</p></div>
<div class="m2"><p>بخواهد بدرد جهان کدخدای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ولیکن بی آزار زرمهر راد</p></div>
<div class="m2"><p>پرستش همی کرد پیش قباد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان بند را برگرفتش زپای</p></div>
<div class="m2"><p>به سوی فتالیت کردند رای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دگر ره به تخت کیی برنشست</p></div>
<div class="m2"><p>ورا گشت جاماسب مهتر پرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه کار آن پادشاهی خویش</p></div>
<div class="m2"><p>به زرمهر بسپرد از کم و بیش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وز آن پس بیاورد لشکر به روم</p></div>
<div class="m2"><p>جهان گشت او را چو یک مهره موم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به قیصر یکی رستخیزی نمود</p></div>
<div class="m2"><p>همان شهر آمید را برگشود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قلون را در آن دز نگهدار کرد</p></div>
<div class="m2"><p>میافارقین را یک ایلغار کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آناستاز آمد به جنگش دمان</p></div>
<div class="m2"><p>ولیکن ازو جست آخر امان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سپس سوی تاتار آهنگ کرد</p></div>
<div class="m2"><p>هم از جان ایشان برآورد گرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سه پور گزین داشت آن تاجور</p></div>
<div class="m2"><p>مگربود کااوزسش مه پسر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دوم زامس نامدار جوان</p></div>
<div class="m2"><p>سوم بود کسرای نوشیروان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی خواست آن نامور پادشاه</p></div>
<div class="m2"><p>که خسرو پس از وی نشیند به گاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به ژوستن که بد امپراطور روم</p></div>
<div class="m2"><p>یکی نامه بنوشت با مهر و موم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فرستاده بد مهبد و سوخرای</p></div>
<div class="m2"><p>پر از مهر آن نامه جان فزای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همی خواست از قیصر نامور</p></div>
<div class="m2"><p>که کسراش باشد به جای پدر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نپذیرفت ازو قیصر نامدار</p></div>
<div class="m2"><p>که ترسید خسرو شود تاج دار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیندازد او دست بر خاک روم</p></div>
<div class="m2"><p>بگیرد همه ملک آباد و بوم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زقیصر چو نومید گردید شاه</p></div>
<div class="m2"><p>یکی نامه بنوشت با سوز و آه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که هر کس ببیند خطی از قباد</p></div>
<div class="m2"><p>زمهر و دلیریم آرید یاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو خواهید کایران شود نیک بخت</p></div>
<div class="m2"><p>به کسری سپارید دیهیم و تخت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که یزدان ازین پور خشنود باد</p></div>
<div class="m2"><p>دل بدسگالش پر از دود باد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زمانه جوان گردد از بخت اوی</p></div>
<div class="m2"><p>شوند این جهان بنده تخت اوی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پس از شاه مهبود خسروپرست</p></div>
<div class="m2"><p>بیامد همان نامه شه به دست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ابر موبدان خواند در پیشگاه</p></div>
<div class="m2"><p>همه یاد کردند از فر شاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نشاندند کسری به گاه مهی</p></div>
<div class="m2"><p>ازو تازه شد فر شاهنشهی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به هشتاد شد سالیان قباد</p></div>
<div class="m2"><p>همان روز پیری بد از مرگ شاد</p></div></div>