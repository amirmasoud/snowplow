---
title: >-
    بخش ۹۰ - پادشاهی آزرمیدخت
---
# بخش ۹۰ - پادشاهی آزرمیدخت

<div class="b" id="bn1"><div class="m1"><p>زنش را که خواندند آزرمیدخت</p></div>
<div class="m2"><p>پس از وی نشاندند بر روی تخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخورد او ز شاهی خود نیز یر</p></div>
<div class="m2"><p>پس از چارمه روزش آمد بسر</p></div></div>