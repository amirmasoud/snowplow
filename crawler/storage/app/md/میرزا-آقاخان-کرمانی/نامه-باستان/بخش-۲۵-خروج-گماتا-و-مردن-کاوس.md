---
title: >-
    بخش ۲۵ - خروج گماتا و مردن کاوس
---
# بخش ۲۵ - خروج گماتا و مردن کاوس

<div class="b" id="bn1"><div class="m1"><p>به گاهی که از مصر آمد به شام</p></div>
<div class="m2"><p>مغی نامور بود جاماسب نام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ارکادرس خواست و آواز داد</p></div>
<div class="m2"><p>که من برته ام شاه خسرو نژاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان را یکی شهریار نوام</p></div>
<div class="m2"><p>سمردیس یل پور کیخسروام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر او انجمن گشت هر سو سپاه</p></div>
<div class="m2"><p>ستوهیده بودند مردم زشاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که پیچیده بد سر زدین خدای</p></div>
<div class="m2"><p>نیاوردی از داد و دانش به جای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرخ زاد را کشت کش بد وزیر</p></div>
<div class="m2"><p>که خواند همی پرگزسبش هژیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گماتا به گاه مهی برنشست</p></div>
<div class="m2"><p>گرفته یکی گرز زرین به دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو آگاهی آمد به کاوس کی</p></div>
<div class="m2"><p>بجوشید خونش به تن همچو می</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی تاخت اسب از پی کارزار</p></div>
<div class="m2"><p>به ناگه زاسب اندر افتاد خوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان کاردکش بود دایم به دست</p></div>
<div class="m2"><p>به پهلو فرو رفت و او را بخست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ابا نامداران ایران سپاه</p></div>
<div class="m2"><p>به هنگام مردن چنین گفت شاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که من برته را پیش ازین کشته ام</p></div>
<div class="m2"><p>به خاک سیاهش بیاغشته ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گماتای بی دانش بد گهر</p></div>
<div class="m2"><p>دروغ است گفتار او سر به سر</p></div></div>