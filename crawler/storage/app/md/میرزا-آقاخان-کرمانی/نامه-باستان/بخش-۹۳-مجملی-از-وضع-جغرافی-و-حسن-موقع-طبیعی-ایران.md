---
title: >-
    بخش ۹۳ - مجملی از وضع جغرافی و حسن موقع طبیعی ایران
---
# بخش ۹۳ - مجملی از وضع جغرافی و حسن موقع طبیعی ایران

<div class="b" id="bn1"><div class="m1"><p>خوشا مرز ایران عنبر نسیم</p></div>
<div class="m2"><p>که خاکش گرامی تر از زر و سیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمینش همه عنبر و مشک ناب</p></div>
<div class="m2"><p>به جوی اندرش آب در خوشاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فضایش چو مینو به رنگ و نگار</p></div>
<div class="m2"><p>به یک سو زمستان و دگر سو بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه کوهسارش چو خلد برین</p></div>
<div class="m2"><p>همه مرغزارش خوش و دل نشین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوایش موافق بهر آدمی</p></div>
<div class="m2"><p>زمینش سراسر پر از خرمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلابست در جویبارش روان</p></div>
<div class="m2"><p>همی پیر گردد زآبش جوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر سوی این ملک با آفرین</p></div>
<div class="m2"><p>یکی بوم فرخنده بینی گزین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر از فارس گویی بهشتی خوش است</p></div>
<div class="m2"><p>همه مرغ آن خرم و دلکش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوا خوش گوار و زمین پرنگار</p></div>
<div class="m2"><p>نه سرد و نه گرم و همیشه بهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو پاکان شیراز پاکی نهاد</p></div>
<div class="m2"><p>نباشد که رحمت بر آن خاک باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسی اندر آن بوم آباد نیست</p></div>
<div class="m2"><p>به کام از دل و جان خود شاد نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به یک سوی اهواز مینو سرشت</p></div>
<div class="m2"><p>که سبز است و خرم چو باغ بهشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شکرخیز خاکی نباشد چنان</p></div>
<div class="m2"><p>که زرنوش بودش یکی شارسان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دی و آذر و بهمن و فروردین</p></div>
<div class="m2"><p>همیشه پر از لاله بینی زمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر از ملک کرمان سرایم رواست</p></div>
<div class="m2"><p>که هندوستانی خوش آب و هواست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در آن مرز فرخنده ی ارجمند</p></div>
<div class="m2"><p>بهر سال زاید دو ره گوسفند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همان زابل از مصر ارزنده تر</p></div>
<div class="m2"><p>زقنوج و کشمر فروزنده تر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به نزد کسی کو بود فرهمند</p></div>
<div class="m2"><p>یکی نیل کوچک بود هیرمند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خراسان زچین و ختن خوش تر است</p></div>
<div class="m2"><p>که خاکش به مانند مشک تر است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه باغ او بوستان نعم</p></div>
<div class="m2"><p>همه راغ او گلستان ارم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صباح نشابور و شام طبس</p></div>
<div class="m2"><p>زخوبی آن مرز فرخنده بس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صفاهان چنو در جهان شهر نیست</p></div>
<div class="m2"><p>نداند کش اندر خرد بهر نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه ساله خندان لب جویبار</p></div>
<div class="m2"><p>به کوه اندران کبک و گور و شکار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نوازنده بلبل به باغ اندرون</p></div>
<div class="m2"><p>گرازنده آهو به راغ اندرون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خوشا حال آن مرغ دستان سرای</p></div>
<div class="m2"><p>که دارد در آن بوم فرخنده جای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نظنز و سور طرق و قهرود و تار</p></div>
<div class="m2"><p>بود خاکشان هم چو مشک تتار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عروس جهان ست ملک اراک</p></div>
<div class="m2"><p>که سرتاسرش مشک بیز است خاک</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درخت گل و سبزه آب روان</p></div>
<div class="m2"><p>طرب آرد از بهر پیر و جوان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هم از عهد جمشید و کاوس کی</p></div>
<div class="m2"><p>نبود است ملکی به خوبی چو ری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که البرز کوه است جای غباد</p></div>
<div class="m2"><p>مکان فریدون با فر و داد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دگر آذر آبادگان کشوریست</p></div>
<div class="m2"><p>که بر روم و شامش بسی برتری ست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خوی و خمسه و ارمی و اردبیل</p></div>
<div class="m2"><p>همه مشک بیزاست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر آیی سوی رشت و مازندران</p></div>
<div class="m2"><p>پر از سبزه بینی کران تا کران</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همه بوستانش سراسر گل است</p></div>
<div class="m2"><p>به کوه اندرون لاله و سنبل است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زهی خاک ایران که از گاه جم</p></div>
<div class="m2"><p>مکان کرامت همی بد عجم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خجسته برو بوم ایران که شیر</p></div>
<div class="m2"><p>همی پروراند گوان دلیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنو مرز با ارز آباد باد</p></div>
<div class="m2"><p>همیشه برو بومش آباد باد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرا تا چه کردم که چرخ بلند</p></div>
<div class="m2"><p>از آن خاک پاکم به غربت فکند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به روم از برای چه دارم وطن</p></div>
<div class="m2"><p>که زندان شد این ملک بر جان من</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خوشا روزگاران پیشین زمان</p></div>
<div class="m2"><p>که بودم به ایران زمین شادمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چه شد مایه هجر و آوارگی</p></div>
<div class="m2"><p>که این چاره جستم زبیچارگی</p></div></div>