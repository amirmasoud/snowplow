---
title: >-
    بخش ۳۶ - سرکشی مصریان و جنگ داراب با یونانیان
---
# بخش ۳۶ - سرکشی مصریان و جنگ داراب با یونانیان

<div class="b" id="bn1"><div class="m1"><p>سپس مصریان سر برافراشتند</p></div>
<div class="m2"><p>زستراپ شه روی برکاشتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوانی که امیرته اش بود نام</p></div>
<div class="m2"><p>سبک تیغ کین برکشید از نیام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر مصریان گشت فرمانروا</p></div>
<div class="m2"><p>ستوهید از او لشکر پادشا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس از وی پوزیریس آمد خدیو</p></div>
<div class="m2"><p>که امیرته را بود فرزند نیو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو سردار نام آور رزم ساز</p></div>
<div class="m2"><p>که تیسافرن باشد و فارناباز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یونان فرستاد دارا به جنگ</p></div>
<div class="m2"><p>که بر یونانیان کار ساز ند تنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گمانم بد او رشنواد گزین</p></div>
<div class="m2"><p>که تسخیر فرمود یونان زمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپهبد به همراه فرخ تژاو</p></div>
<div class="m2"><p>زیونان زمین بستدی باژ و ساو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی بود ستراپ در لیدیه</p></div>
<div class="m2"><p>دگر در هلسپون وایعونیه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به اسپرته گشتند هم دست و یار</p></div>
<div class="m2"><p>که بر آتنه تنگ سازند کار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دو پور پریزاد بد شاه را</p></div>
<div class="m2"><p>که مانست هر یک همی ماه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخست اردشیر آنکه مه بود به سال</p></div>
<div class="m2"><p>دگر بود سیروس نیکو جمال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پدر بد به پور مهین شادکام</p></div>
<div class="m2"><p>هواخواه کهتر پسر بود مام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پریزاد را این چنین بود رای</p></div>
<div class="m2"><p>که سیروس را سازد ایران خدای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ولی شه به آیین و رسم مهی</p></div>
<div class="m2"><p>به مهتر پسر داد عهد شهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پریزاد نومید شد زین سخن</p></div>
<div class="m2"><p>یکی تازه اندیشه افکند بن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه لیدی و یونیه سر بسر</p></div>
<div class="m2"><p>زداراب بگرفت بهر پسر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس آنگاه سیروس در لیدیا</p></div>
<div class="m2"><p>همی بود ستراپ و فرمانروا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به یونانیان گشت همدست و یار</p></div>
<div class="m2"><p>مگر خود بر ایران شود شهریار</p></div></div>