---
title: >-
    بخش ۱۰۰ - در مقام شرح حال گوید
---
# بخش ۱۰۰ - در مقام شرح حال گوید

<div class="b" id="bn1"><div class="m1"><p>تو تا باشی ای خسرو نامور</p></div>
<div class="m2"><p>مرنجان کسی را که دارد هنر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ویژه که باشد ز روشن دلی</p></div>
<div class="m2"><p>به جان دوست دار نبی و علی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی نامداری زایران منم</p></div>
<div class="m2"><p>که خو کرده در جنگ شیران تنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلم دارم و علم و فرهنگ و رای</p></div>
<div class="m2"><p>نژاد بزرگان و فر همای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گاهی که آمد تمیزم پدید</p></div>
<div class="m2"><p>روانم به دانش همی بد کلید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زگیتی نجستم به جز راستی</p></div>
<div class="m2"><p>نگشتم به گرد کم و کاستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه خیر اسلامیان خواستم</p></div>
<div class="m2"><p>دلم را به نیکی بیاراستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه خواستم تا که اسلامیان</p></div>
<div class="m2"><p>به وحدت ببندند یکسر میان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه دوستی با هم افزون کنند</p></div>
<div class="m2"><p>ز دل کین دیرینه بیرون کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مر اسلامیان را فزاید شرف</p></div>
<div class="m2"><p>نفاق و جدایی شود برطرف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در اسلام آید به فر حمید</p></div>
<div class="m2"><p>یکی اتحاد سیاسی پدید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شود ترک ایران و ایران چو ترک</p></div>
<div class="m2"><p>نماند دوئی در شهان سترک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همان نیز دانندگان عراق</p></div>
<div class="m2"><p>به سلطان اعظم کنند اتفاق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زدل ها زدایند این کینه زود</p></div>
<div class="m2"><p>نگویند سنی و شیعی که بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز آن پس بگیرند گیتی به زور</p></div>
<div class="m2"><p>زجان مخالف بر آرند شور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ابا چند آزاده مرد گزین</p></div>
<div class="m2"><p>نبشتیم بس نامه های متین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روانه نمودیم سوی عراق</p></div>
<div class="m2"><p>که برخیزد از عالم دین نفاق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به نیروی دادار جان آفرین</p></div>
<div class="m2"><p>همه بر نهادند امضا برین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به بخشید حسن اثر نامه ها</p></div>
<div class="m2"><p>که، خام و نپخته نبد خامه ها</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپاسم زیزدان پیروزگر</p></div>
<div class="m2"><p>که این نخل امید شد بارور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نوشتند زایران و هم از عراق</p></div>
<div class="m2"><p>که از دل بشستیم گرد نفاق</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه جان فدای شریعت کنیم</p></div>
<div class="m2"><p>به سلطان اسلام بیعت کنیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گذاریم قانون بیگانگی</p></div>
<div class="m2"><p>بگیریم آیین فرزانگی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ازین پس همه کفر سازیم پست</p></div>
<div class="m2"><p>بیاریم گیتی سراسر به دست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کسی از سلاطین اسلامیان</p></div>
<div class="m2"><p>ز عباسیان تا به عثمانیان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زسامانی و غزنی و دیلمی</p></div>
<div class="m2"><p>ز سلجوق و خوارزمی و فاطمی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز صدر سلف تا به گاه خلف</p></div>
<div class="m2"><p>موفق نگردید بر این شرف</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مگر اندرین عصر کامد پدید</p></div>
<div class="m2"><p>چنین طرح محکم ز رای سدید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گرت زین بد آید، گناه من است</p></div>
<div class="m2"><p>که این شیوه آیین و راه من است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برین زاده ام هم برین بگذرم</p></div>
<div class="m2"><p>وزین فخر بر چرخ ساید سرم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر شاه را بود حسی نهان</p></div>
<div class="m2"><p>مرا ساختی بی نیاز از جهان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وگر از مسلمانیش بود بهر</p></div>
<div class="m2"><p>به نیکی مرا شهره کردی به دهر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو در خون او جوهر شرک بود</p></div>
<div class="m2"><p>زتوحید اسلام خشمش فزود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پشیزی به از شهریاری چنین</p></div>
<div class="m2"><p>که نه کیش دارد نه آیین و دین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرا بیم دادی که در اردبیل</p></div>
<div class="m2"><p>تنم را به زنجیر بندی چو پیل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زکشتن نترسم که آزاده ام</p></div>
<div class="m2"><p>زمادر همی مرگ را زاده ام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کسی بی زمانه به گیتی نمرد</p></div>
<div class="m2"><p>بمرد آنکه نام بزرگی نبرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نمیرم ازین پس که من زنده ام</p></div>
<div class="m2"><p>که این طرح توحید افکنده ام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به گوش از سروشم بسی مژدهاست</p></div>
<div class="m2"><p>دلم گنج گوهر، قلم اژدهاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پس از مردنم هست پایندگی</p></div>
<div class="m2"><p>که جاوید باشد مرا زندگی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نصیب من آباد و تحسین بود</p></div>
<div class="m2"><p>تو را بهره همواره نفرین بود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پس از من بگویند نام آوران</p></div>
<div class="m2"><p>سرایند با یکدگر مهتران</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که کرمانی راد پاکی نهاد</p></div>
<div class="m2"><p>همه داد مردی و دانش بداد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پس از سیزده قرن پر اختلاف</p></div>
<div class="m2"><p>نمودار کرد او ره ائتلاف</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به توحید دعوت نمود از دویی</p></div>
<div class="m2"><p>به پیچید از کژی و جادویی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مرا آید از مشتری آفرین</p></div>
<div class="m2"><p>که بودم فداکار دین مبین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>درودم زمینو رسانند حور</p></div>
<div class="m2"><p>هم از آسمانم فشانند نور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به دوزخ بمانی تو تیره روان</p></div>
<div class="m2"><p>همت لعنت آید زپیر و جوان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نشینند و گویند مردان راد</p></div>
<div class="m2"><p>به نیکی نیارند نام تو یاد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که شه ناصرالدین بدی یار کفر</p></div>
<div class="m2"><p>از او گرم گردید بازار کفر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کسانی که توحید دین خواستند</p></div>
<div class="m2"><p>بدین مقصد قدس برخاستند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بیازرد و افسرد و از خود براند</p></div>
<div class="m2"><p>به گیتی به جز نام زشتی نخواند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو ای شه چنین راه دین سد مکن</p></div>
<div class="m2"><p>به خیره همی نام خود بد مکن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که ناگه دلم را برآری زجای</p></div>
<div class="m2"><p>همه دودمانت برآرم زپای</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بگویم سخن های ناگفتنی</p></div>
<div class="m2"><p>بسنبم گهرهای ناسفتنی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>که چون بود بیخ و تبار قجر</p></div>
<div class="m2"><p>چگونه به شام آوریدند سر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به تاتار بهر چه آمیختند</p></div>
<div class="m2"><p>زشام از برای چه بگریختند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مرا هست تاریخی اندر اروپ</p></div>
<div class="m2"><p>به قوت فزونتر زتوپ کروپ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مبادا که آن نامه افشان شود</p></div>
<div class="m2"><p>که بیخ و تبارت پریشان شود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همان به که خاموش سازی مرا</p></div>
<div class="m2"><p>زکینه فراموش سازی مرا</p></div></div>