---
title: >-
    بخش ۱۲ - سلاله ی مدی و پادشاهی کیقباد
---
# بخش ۱۲ - سلاله ی مدی و پادشاهی کیقباد

<div class="b" id="bn1"><div class="m1"><p>چنین تابگاه گو کی نژاد</p></div>
<div class="m2"><p>ورا خواند دانا مگر کیقباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ارباس کرد مدی نام داشت</p></div>
<div class="m2"><p>به البرز کوه اندر آرام داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپاهی زقوم مدی گرد کرد</p></div>
<div class="m2"><p>برآورد از شهر نینونه گرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بله زیس که او بود بابل خدای</p></div>
<div class="m2"><p>بهر کار ارباس را رهنمای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمغلوبی خصم دادش خبر</p></div>
<div class="m2"><p>ازیرا که بودی ستاره شمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو از کار نینویه پرداختند</p></div>
<div class="m2"><p>یکی سلطنت از مدی ساختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس از شاه ارباس شش تن بدی</p></div>
<div class="m2"><p>گرفتند هر یک ره ایزدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی ز آن میان بود نوذر بنام</p></div>
<div class="m2"><p>دگر گرد فرتوس با نام و کام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سه دیگر گو اشکش رزم زن</p></div>
<div class="m2"><p>چهارم بد اورند لشکر شکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پنجم کرزم آن یل شیر گیر</p></div>
<div class="m2"><p>ششم بود منجیک گرد دلیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ایران بهر گوشه برهم یکی</p></div>
<div class="m2"><p>گرفته زهر کشوری اندکی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز آترپاتن تا به کابل زمین</p></div>
<div class="m2"><p>نکرد ایچ یاد این از آن آن ازین</p></div></div>