---
title: >-
    بخش ۷۳ - پادشاهی پیروز
---
# بخش ۷۳ - پادشاهی پیروز

<div class="b" id="bn1"><div class="m1"><p>زهرمز چو پیروز دل شاد شد</p></div>
<div class="m2"><p>روانش ز اندیشه آزاد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیامد به تخت مهی بر نشست</p></div>
<div class="m2"><p>همان دست کهتر برادر به دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی خشک سالی بیامد پدید</p></div>
<div class="m2"><p>که کس در جهان روی سیری ندید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هامون درآمد شه نام دار</p></div>
<div class="m2"><p>همی خواست از دادگر زینهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو زین گونه شد شاه با آفرین</p></div>
<div class="m2"><p>بیامد یکی ابر در فرودین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی در ببارید بر خاک خشک</p></div>
<div class="m2"><p>همی آمد از بوستان بوی مشک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو پیروز ازین روز تنگی برست</p></div>
<div class="m2"><p>یکی شارسان کرد جای نشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که یادان فیروز بد نام او</p></div>
<div class="m2"><p>از آن جا برآمد همه کار او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در این روز گویند پیروز رام</p></div>
<div class="m2"><p>که پیروز آن جا بشد شادکام</p></div></div>