---
title: >-
    بخش ۱۰۴ - خاتمه و تاریخ اتمام کتاب
---
# بخش ۱۰۴ - خاتمه و تاریخ اتمام کتاب

<div class="b" id="bn1"><div class="m1"><p>چو آمد به بن این کهن داستان</p></div>
<div class="m2"><p>بنامیدمش نامه ی باستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زتاریخ هجرت ز بعد هزار</p></div>
<div class="m2"><p>یکی سیصد و سیزده برشمار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زشعبان گذشته همی روز ده</p></div>
<div class="m2"><p>مطابق به آغاز اسپندمه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که پایان شد این نامبردار گنج</p></div>
<div class="m2"><p>به یک ماه بردم درین کار رنج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپاسم زیزدان پیروزگر</p></div>
<div class="m2"><p>که این نامه ی نامی آمد به سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرض بود تاریخ، نی شاعری</p></div>
<div class="m2"><p>که طبع من از شعر باشد عری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ویژه که بودم به بند اندرون</p></div>
<div class="m2"><p>چه لطف آید از طبع بندی برون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین نامه از هر دری گفته شد</p></div>
<div class="m2"><p>گهرهای معنی بسی سفته شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زگفتار فردوسی پاکزاد</p></div>
<div class="m2"><p>بسی کرده ام اندرین نامه یاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نبد اندرین ره مرا توشه ای</p></div>
<div class="m2"><p>هم از خرمن او بدم خوشه ای</p></div></div>