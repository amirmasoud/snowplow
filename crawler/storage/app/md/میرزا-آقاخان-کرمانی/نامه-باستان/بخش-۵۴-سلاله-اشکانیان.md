---
title: >-
    بخش ۵۴ - سلاله اشکانیان
---
# بخش ۵۴ - سلاله اشکانیان

<div class="b" id="bn1"><div class="m1"><p>نخست اشک کش بد زآرش نژاد</p></div>
<div class="m2"><p>همش نام ارزاس آرند یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر نام ارزاس و آرش یکی ست</p></div>
<div class="m2"><p>تفاوت در این نامها اندکی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو برخاست این نامور</p></div>
<div class="m2"><p>سلفکی ازو گشت زیر و زبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسی جنگ ها کرد و پیروز شد</p></div>
<div class="m2"><p>سپس طعمه تیر دل دوز شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برادرش را جای خود برنهاد</p></div>
<div class="m2"><p>مگر نام او بد همی تیرداد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بربست بر کوهه ی پیل کوس</p></div>
<div class="m2"><p>شکسته شد از دست کالینی کوس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولی باز برگشت و پیروز شد</p></div>
<div class="m2"><p>ازو مرد بدخواه بد روز شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مازندران اندر آمد دلیر</p></div>
<div class="m2"><p>سلوکوس از جنگ او گشت سیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس از وی پسرش اردوان نخست</p></div>
<div class="m2"><p>سرگاه و دیهیم شاهی بجست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلوکوس را راند تا سوریا</p></div>
<div class="m2"><p>به ایران زمین گشت فرمانروا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو او مرد پورش فریباد گرد</p></div>
<div class="m2"><p>همه گوی مردی زمیدان ببرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس از وی فراهاد شد شهریار</p></div>
<div class="m2"><p>ظفر یافت بر مارو در کارزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بگذشت او مهرداد نخست</p></div>
<div class="m2"><p>سر تخت شاهی به مردی بجست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه سغد و آلام و مدیا گشود</p></div>
<div class="m2"><p>به هندوستان ایلغاری نمود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هر سو زفر خود افکند شور</p></div>
<div class="m2"><p>زاشکانیان برتر آمد به زور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهان بد ورا سالیان سی و هفت</p></div>
<div class="m2"><p>سپس کرد بدرود گیتی و رفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به جایش فراهاد شد تاجور</p></div>
<div class="m2"><p>که او میتریدات را بد پسر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی جنگ با قوم سیتا بجست</p></div>
<div class="m2"><p>درآورد کشتند او را نخست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دوم اردوان آمدش جانشین</p></div>
<div class="m2"><p>که دستور او بود و عم گزین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم او نیز در جنگ شد کشته باز</p></div>
<div class="m2"><p>پسر بد مر او را یکی سرفراز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر اورنگ شاهیش بنشاندند</p></div>
<div class="m2"><p>دوم مهردادش همی خواندند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زتاتار بگرفت کین پدر</p></div>
<div class="m2"><p>بتازید توران زمین سربسر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ابا رومیانش یکی جنگ خاست</p></div>
<div class="m2"><p>همه لشکر روم از وی بکاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شهی بود با فرو داد و سترگ</p></div>
<div class="m2"><p>سزد گر بر او نام بنهی بزرگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پس از وی منوچهر شد شهریار</p></div>
<div class="m2"><p>که خواند مناسکیرسش نامدار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فزون بود سالش زهشتاد و پنج</p></div>
<div class="m2"><p>که بر جای عم یافت او تاج و گنج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سنادرخ که بودی پس مهرداد</p></div>
<div class="m2"><p>نبودی زشاهی او هیچ شاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در سرکشی باز کرد از نخست</p></div>
<div class="m2"><p>ولیکن به فرجام طاعت بجست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنان سست شد کار ایران دیار</p></div>
<div class="m2"><p>که برخاست هر جا یکی نامدار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به یک تن نشد کار آن ملک راست</p></div>
<div class="m2"><p>زهر سو یکی نامداری بخاست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گهی خاست زارمینیه تیگران</p></div>
<div class="m2"><p>گهی باختر شد به یونان قران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گهی مهرداد از نژاد تباک</p></div>
<div class="m2"><p>زبیمش بلرزید در روم خاک</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>منوچهر با این همه شور و شر</p></div>
<div class="m2"><p>همی کرد شاهی به پیرانه سر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پس از وی سینا تروکس نامدار</p></div>
<div class="m2"><p>به جای پسر عم بشد شهریار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو او نیز بد پیر کی سالخورد</p></div>
<div class="m2"><p>پسر را به انباز و خود ببرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پس از وی فراهاد جایش بجست</p></div>
<div class="m2"><p>ابارومیان بست پیمان درست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پسر بد دلاور مر او را چهار</p></div>
<div class="m2"><p>پدر را بکشتند با زهرخوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مهین پور کش نام بد مهرداد</p></div>
<div class="m2"><p>به جای پدر تاج بر سر نهاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بسی بد ستمکار و ناپاک رای</p></div>
<div class="m2"><p>به جورش کسی را نبد هیچ پای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برادرش کش نام بودی ارود</p></div>
<div class="m2"><p>ازو شد گریزان و هجرت نمود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز بس جور و استمگری پیشه کرد</p></div>
<div class="m2"><p>دل مردم از خود پر اندیشه کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز تخت آوریدند او را فرود</p></div>
<div class="m2"><p>به شاهی بخواندند آنگه ارود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ارود آمد و تاج بر سر نهاد</p></div>
<div class="m2"><p>ز تن دور کرد آن سر مهرداد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یکی جنگ با رومیان زد درشت</p></div>
<div class="m2"><p>کراسوس را با سپاهش بکشت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از آن جایگه تا فلسطین بتاخت</p></div>
<div class="m2"><p>همه یال و بزر مهی برفراخت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ولیکن به زودی شد از رزم سیر</p></div>
<div class="m2"><p>نتابید با کاسیوس بی دلیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دگر سال پاکور کش بد پسر</p></div>
<div class="m2"><p>فرستاد با لشکری بی شمر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به شامات پاکور فرخ همال</p></div>
<div class="m2"><p>همی کرد ایلغار تا چند سال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شکسته شد آخر زو آنتی نیوس</p></div>
<div class="m2"><p>به فرجام شد کشته با صد فسوس</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فراهات کش بود پور دگر</p></div>
<div class="m2"><p>بیاورد روز پدر را بسر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همه ارمنستان به مردی گرفت</p></div>
<div class="m2"><p>رعیت ستوهیده ازو ای شگفت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>فرود آوردیدندش از تخت و گاه</p></div>
<div class="m2"><p>ولیکن سوی سیت جست او پناه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به خود قوم اسکیت را کرد یار</p></div>
<div class="m2"><p>دگر ره بر ایران شد او شهریار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فراهات پنجم که بودش پسر</p></div>
<div class="m2"><p>ورا کشت تا خود شود تاجور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ارود دوم نیز او را بکشت</p></div>
<div class="m2"><p>که تا کشور آرد به مردی به مشت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دگر ره بکشتند مردم ارود</p></div>
<div class="m2"><p>تو گفتی که او در زمانه نبود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ونونس که فرهاد را بد پسر</p></div>
<div class="m2"><p>به روما همی آوریدی بسر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اهالی به شاهی ورا خواستند</p></div>
<div class="m2"><p>سرگاه از بهرش آراستند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ولیکن شدند آخر از وی بسیر</p></div>
<div class="m2"><p>که آداب رومش بدی در ضمیر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بینداختندش زگاه شهی</p></div>
<div class="m2"><p>ازیرا که بود رومیان را رهی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سپس اردوان سیم را به تخت</p></div>
<div class="m2"><p>به شاهی نشاندند پیروز بخت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به سیتا همی بود پیوند او</p></div>
<div class="m2"><p>ارود سوم بود فرزند او</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو تاریخ ایران کنی رهنمون</p></div>
<div class="m2"><p>ارود است هاروت و بیژن و نون</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همه ارمنستان سراسر گرفت</p></div>
<div class="m2"><p>پسر را در آن جا نشاند و برفت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>وز آن سوی تیبر شهنشاه روم</p></div>
<div class="m2"><p>غمی شد زپیکار آن مرز و بوم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>فرستاد ژرمانیوس دلیر</p></div>
<div class="m2"><p>که هاروت را سازد از جنگ سیر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سپهدار روم با سپاهی فزون</p></div>
<div class="m2"><p>زارمینه کرده مرا ورا برون</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دوپور دگر داشت شاه اردوان</p></div>
<div class="m2"><p>که بودند هر دو دلیر و جوان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>مهین پور گودرز و برزین کهین</p></div>
<div class="m2"><p>کهین پور را ساخت او جانشین</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ولی گرد کوتارز نامور</p></div>
<div class="m2"><p>ز باردانس بگرفت تاج پدر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو جور و ستمکاری از حد فزود</p></div>
<div class="m2"><p>زگاه آوریدند او را فرود</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دگر بار برزین بشد تاجور</p></div>
<div class="m2"><p>ولی زود روزش بیامد بسر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دوم ره به گودرز شاهی رسید</p></div>
<div class="m2"><p>بیامد به گاه مهی آرمید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>پس از وی و نون گشت فرمانروا</p></div>
<div class="m2"><p>که بود از نژاد و تبار کیا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چو یکسال بگذشت او درگذشت</p></div>
<div class="m2"><p>پلاش نخست آمد و شاه گشت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به ارمینیه ایلغاری نمود</p></div>
<div class="m2"><p>پس آن گه در آشتی را گشود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو او مرد پورش که پاکور بود</p></div>
<div class="m2"><p>به گاه شهی شاد و مسرور بود</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>پس از وی برادرش بر شد به تخت</p></div>
<div class="m2"><p>که خسرو بدی نام آن نیک بخت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تراژان که بودی شهنشاه روم</p></div>
<div class="m2"><p>بدو جست جنگی دگر سخت شوم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو خسرو بشد سوی دار بقا</p></div>
<div class="m2"><p>پلاش دوم گشت فرمانروا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بسی جنگ با لشکر روم جست</p></div>
<div class="m2"><p>ولی عاقبت گشت در رزم سست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بزرگان زشاهیش گشتند سیر</p></div>
<div class="m2"><p>زگاه مهی ش آوریدند زیر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نشاندند مونزوس را جای او</p></div>
<div class="m2"><p>ولیکن نبودیش یارای او</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دگر باره شاهی بدو بازگشت</p></div>
<div class="m2"><p>بسی راند فرمان و پس درگذشت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>پلاش سیم کش بدی پور راد</p></div>
<div class="m2"><p>به جای پدر تاج بر سر نهاد</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>سپس گشت فیروز فرمانروا</p></div>
<div class="m2"><p>دگر نرسی نامدار کیا</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چو بنشست بهرام از اشکانیان</p></div>
<div class="m2"><p>ببخشید گنجی به ارزانیان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ورا خوانده اند اردوان بزرگ</p></div>
<div class="m2"><p>که ازمیش بگسست چنگال گرگ</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>یکی جنگ با رومیان ساز کرد</p></div>
<div class="m2"><p>که قیصر در آشتی باز کرد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>همی زیست بر تخت شاهی به ناز</p></div>
<div class="m2"><p>جهان جوی و نام آور و رزم ساز</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>که ناگه یکی گرد ساسان نژاد</p></div>
<div class="m2"><p>همه تخت و دیهیم دادش به باد</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>سرگاه اشکانیان شد تهی</p></div>
<div class="m2"><p>به ارمینیه لیک بدشان مهی</p></div></div>