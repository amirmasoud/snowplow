---
title: >-
    بخش ۳۷ - شاهنشاهی اردشیر ثانی معروف به منسومون
---
# بخش ۳۷ - شاهنشاهی اردشیر ثانی معروف به منسومون

<div class="b" id="bn1"><div class="m1"><p>پس از مرگ داراب شاه اردشیر</p></div>
<div class="m2"><p>نشست از برگاه شاهی دلیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هریدوت خوانده ورا منسومون</p></div>
<div class="m2"><p>که بختش جوان بود و رایش فزون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شهری که خوانند پارزگراد</p></div>
<div class="m2"><p>بسر خواست دیهیم شاهی نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین بود آیین کیخسروی</p></div>
<div class="m2"><p>که هرکو به شاهی رسید از نوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زتن جامه خویش کردی برون</p></div>
<div class="m2"><p>برفتی به آذر گشسب اندرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپوشیدی آن جامه تاجور</p></div>
<div class="m2"><p>که گاه شبانیش بودی به بر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخوردی زانجیر خشکیده چند</p></div>
<div class="m2"><p>مکیدی زبرگ بنه یا سپند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس آنگاه نوشیدی از بیش و کم</p></div>
<div class="m2"><p>یکی شربت از شیر و سرکه بهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برین بود آیین شاهنشهان</p></div>
<div class="m2"><p>به گاه نشستن به تخت کیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو شاه اندر آمد به آتشکده</p></div>
<div class="m2"><p>همه زند و استا به زر آژده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مغی شاه را داد ازین آگهی</p></div>
<div class="m2"><p>که سیروس چون بد سریر مهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترا کشت خواهد درین جایگه</p></div>
<div class="m2"><p>مگر خود بر ایران شود پادشه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کی نامدار اندر آمد به خشم</p></div>
<div class="m2"><p>همی آتش افروخت از هر دو چشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بفرمود کو را به زاری کشند</p></div>
<div class="m2"><p>همه پیکرش را به خون درکشند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پریزاد بشنید و آمد دوان</p></div>
<div class="m2"><p>در آغوش بگرفت پور جوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شهنشه به ناچار از او درگذشت</p></div>
<div class="m2"><p>دگر باره سیروس ستراپ گشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به لیدی کلی آرک را یاد کرد</p></div>
<div class="m2"><p>به دستور یونانیان کار کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیاراست لشکر پی کارزار</p></div>
<div class="m2"><p>سپاهش فزون بود از صد هزار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به کوناکسا آمد از ساردیز</p></div>
<div class="m2"><p>همه دل پر از کین سر پر ستیز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وزین سو کی نامور با سپاه</p></div>
<div class="m2"><p>کرازان بیامد بدان رزمگاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نهادند آوردگاهی بزرگ</p></div>
<div class="m2"><p>دو جنگی به مانند دو نره گرگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فزونی همی بود با اردشیر</p></div>
<div class="m2"><p>در آن جنگ سیروس شد دستگیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زدارش بیاویخت چون خارپشت</p></div>
<div class="m2"><p>وزان پس به باران تیرش بکشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو سیروس را بخت برگشته شد</p></div>
<div class="m2"><p>کلی آرخ نامور کشته شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در آن جنگ زنفون به همراه بود</p></div>
<div class="m2"><p>که دانشوری گرد و آگاه بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هم او بد سپهدار یونان سپه</p></div>
<div class="m2"><p>به زنهار آمد به نزدیک شه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به جان داد زنهارشان اردشیر</p></div>
<div class="m2"><p>فرستادشان با تژاو دلیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به یونانیان شرح این بازگشت</p></div>
<div class="m2"><p>بسی مایه صیت و آواز گشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گزنفون کزان راه برگشته است</p></div>
<div class="m2"><p>همی نغز تاریخ بنوشته است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زسیروس و کارش سراید همی</p></div>
<div class="m2"><p>مرآن نامور را ستاید همی</p></div></div>