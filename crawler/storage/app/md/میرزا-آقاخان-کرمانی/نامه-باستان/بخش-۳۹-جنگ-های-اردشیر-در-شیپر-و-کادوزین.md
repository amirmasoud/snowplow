---
title: >-
    بخش ۳۹ - جنگ های اردشیر در شیپر و کادوزین
---
# بخش ۳۹ - جنگ های اردشیر در شیپر و کادوزین

<div class="b" id="bn1"><div class="m1"><p>دگر شیپریان سر برافراشتند</p></div>
<div class="m2"><p>زفرمان شه روی برکاشتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شهر سالامین که بد پایتخت</p></div>
<div class="m2"><p>اراکور را سرکشی بود سخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهنشاه کشتی بسی کرد ساز</p></div>
<div class="m2"><p>به سرداری نامور تیرباز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خشکی سپهدار ارونتاس گرد</p></div>
<div class="m2"><p>که داماد شه بود و با دستبرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اراکور از مصریان خواست یار</p></div>
<div class="m2"><p>که تنگ آورد کار بر شهریار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولیکن به فرجام زورش بکاست</p></div>
<div class="m2"><p>به جان از ارونتاس زنهار خواست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس از رزم سیپروس شاه اردشیر</p></div>
<div class="m2"><p>ابر جنگ کادوزیان شد دلیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپاهی برآراست بیش از شمار</p></div>
<div class="m2"><p>پیاده فزون بد زسیصد هزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو آمد به کادوزیان آگهی</p></div>
<div class="m2"><p>زآذوقه کردند کشور تهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در اردوی شه قحطی افتاد سخت</p></div>
<div class="m2"><p>چنین شد گمانش که برگشت بخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ولیکن به تدبیر تیری بیاز</p></div>
<div class="m2"><p>ببردند کادوزیانش نیاز</p></div></div>