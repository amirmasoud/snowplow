---
title: >-
    بخش ۵۷ - پادشاهی شاپور پسر اردشیر
---
# بخش ۵۷ - پادشاهی شاپور پسر اردشیر

<div class="b" id="bn1"><div class="m1"><p>پس از مردن شاه شاپور راد</p></div>
<div class="m2"><p>به ایوان شد و تاج بر سر نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گیتی چون او شهریاری نبود</p></div>
<div class="m2"><p>گه رزم چونان سواری نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی جنگ با قیصر روم کرد</p></div>
<div class="m2"><p>که فرخنده گیتی بر او شوم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو داد گور دین شه رومیه</p></div>
<div class="m2"><p>همه بین شطین و ارمینیه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر باره چون شاه شد فیلپوس</p></div>
<div class="m2"><p>بر آراست لشگر چو چشم خروس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسی جنگ ها شد میان دو شاه</p></div>
<div class="m2"><p>بسی لشگر آمد زهر سو تباه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین تا گه والرین کبیر</p></div>
<div class="m2"><p>که شاپور در جنگ کردش اسیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو قیصر گرفتار شاپور شد</p></div>
<div class="m2"><p>همه لشکر روم بی زور شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی شارسان ساخت در شوشتر</p></div>
<div class="m2"><p>به دست اسیران رومی مگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دگر شارسان ساخت در کازرون</p></div>
<div class="m2"><p>نگارید رسم خود آن جا درون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو برزین توزی همی جست جای</p></div>
<div class="m2"><p>ابر تارک قیصرش بود پای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بتازید تا پیش دریای روم</p></div>
<div class="m2"><p>به آتش همی سوخت آباد بوم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بجستند زنهار ازو رومیان</p></div>
<div class="m2"><p>ببستند مر بندگی را میان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زدینار رومی دوره صد هزار</p></div>
<div class="m2"><p>فرستاد قیصر بر شهریار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببخشیدشان نام بردار شاه</p></div>
<div class="m2"><p>بفرمود تا باز گردد سپاه</p></div></div>