---
title: >-
    بخش ۸۴ - پادشاهی غباد معروف به شیرویه
---
# بخش ۸۴ - پادشاهی غباد معروف به شیرویه

<div class="b" id="bn1"><div class="m1"><p>غباد از بر تخت زرین نشست</p></div>
<div class="m2"><p>به هرقل یکی سخت پیمان ببست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرآن شارسانی زآباد بوم</p></div>
<div class="m2"><p>که بگرفته بد خسرو از ملک روم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به همراهی چوب دار مسیح</p></div>
<div class="m2"><p>که آورد سربار بهر مزیح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به قیصر همه باز پس داد و گفت</p></div>
<div class="m2"><p>کزین پس نباشیم با رنج جفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زایران سپه شد تهی مصر و روم</p></div>
<div class="m2"><p>به سر اختر بد همی گشت شوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وزآن پس سرآورد روز پدر</p></div>
<div class="m2"><p>که ناپاک زاده بد آن بدگهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکشت آن برادرش مردانه نام</p></div>
<div class="m2"><p>که سیرای فرخنده بودیش مام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که خسرو ورا کرده بود جانشین</p></div>
<div class="m2"><p>به زندان همی بود شیرویه زین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برادر بد او را همی بیست و چار</p></div>
<div class="m2"><p>سرآورد بر جملگی روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیامد براین کار شش ماه باز</p></div>
<div class="m2"><p>که او نیز آمد زمانش فراز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زرومی بزاد و به شومی بمرد</p></div>
<div class="m2"><p>همان تخت شاهی پسر را سپرد</p></div></div>