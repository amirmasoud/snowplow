---
title: >-
    بخش ۶۳ - پادشاهی شاپور ذوالاکتاف
---
# بخش ۶۳ - پادشاهی شاپور ذوالاکتاف

<div class="b" id="bn1"><div class="m1"><p>پس از شاه بروی درم ریختند</p></div>
<div class="m2"><p>به مشگوی تاجی بیاویختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ماهی دو بگذشت بر آن پری</p></div>
<div class="m2"><p>یکی کودکی زاد چون مشتری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهادند شاپور نامش مهان</p></div>
<div class="m2"><p>ازو خرمی یافت روی جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشاندند او را به گاه پدر</p></div>
<div class="m2"><p>به گهواره اش بسته دیهیم زر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی موبدی بود شهروی نام</p></div>
<div class="m2"><p>همی کرد دستوری او تمام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین تا برآمد بر این هفت سال</p></div>
<div class="m2"><p>برافروخت شاپور فرخنده یال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خردی سخن های شاهانه گفت</p></div>
<div class="m2"><p>که موبد بماندی ازو در شگفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی پل به بغداد بنیاد کرد</p></div>
<div class="m2"><p>که جان های مردم همه شاد کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خردی بیاراست کار سپاه</p></div>
<div class="m2"><p>بیفزود بر لشکر رزم خواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی جنگ کرد او بقوم عرب</p></div>
<div class="m2"><p>به طایر سرآورد روز طرب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر آن جا عرب یافتی ای شگفت</p></div>
<div class="m2"><p>زدو دست او دور کردی دو کتف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از این رو ذوالاکتافش آمد لقب</p></div>
<div class="m2"><p>که از مهره بگشاد کتف عرب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو خاک یمن را سراسر بتاخت</p></div>
<div class="m2"><p>یکی لشکر گشن آماده ساخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی کرد آهنگ قیصر بروم</p></div>
<div class="m2"><p>کزو باژ بستاند آباد بوم</p></div></div>