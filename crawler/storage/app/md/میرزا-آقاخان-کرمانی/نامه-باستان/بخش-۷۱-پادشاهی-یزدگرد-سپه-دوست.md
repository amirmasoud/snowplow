---
title: >-
    بخش ۷۱ - پادشاهی یزدگرد سپه دوست
---
# بخش ۷۱ - پادشاهی یزدگرد سپه دوست

<div class="b" id="bn1"><div class="m1"><p>پس از وی پسر تاج شاهی بجست</p></div>
<div class="m2"><p>که نامش بدی یزدگرد ار نخست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان جوی بر تخت زرین نشست</p></div>
<div class="m2"><p>در رنج و دست بدی را ببست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی داشت ایران زدشمن نگاه</p></div>
<div class="m2"><p>به هر سو فرستاد بی مر سپاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپه دوست خواندند او را ردان</p></div>
<div class="m2"><p>ازو شاد بودی دل موبدان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آرمن همی بود ارزاس گرد</p></div>
<div class="m2"><p>که ز اشکانیان بود و با دستبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو پور گزین داشت آن نامور</p></div>
<div class="m2"><p>ببخشید کشور بر آن هر دو بر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهین پور کش نام بد تیگران</p></div>
<div class="m2"><p>بدی بخش او بیش از دیگران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کهین کش همی بود ارزاس نام</p></div>
<div class="m2"><p>سوی قیصر روم برداشت گام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه بخش خود را به قیصر سپرد</p></div>
<div class="m2"><p>که تا بر برادر کند دستبرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وزین روی تیگران به ایران شتافت</p></div>
<div class="m2"><p>به نزدیک شاه دلیران شتافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر در کراسن بدی یزدگرد</p></div>
<div class="m2"><p>سر یاغیان آوریدی بگرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ورا آگهی آمد از شاه روم</p></div>
<div class="m2"><p>که لشگر فرستد به آباد بوم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به سرچشمه ی تیگرا و فرات</p></div>
<div class="m2"><p>نمود است بنیاد مستحکمات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شه نامور پوست بر تن بکفت</p></div>
<div class="m2"><p>سپه سوی ارمن کشیدن گرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وزآن رو اناتولیوس دلیر</p></div>
<div class="m2"><p>چو بشنید کامد جهان جوی شیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ندانست کش نیست پایاب او</p></div>
<div class="m2"><p>ندارد در آوردگه تاب او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیامد بر شاه و پوزش گرفت</p></div>
<div class="m2"><p>ازو هر دو لشگر شده در شگفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ابر آشتی شاه را ره نمود</p></div>
<div class="m2"><p>جهان جوی پذیرفت و دادش درود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>میان دو شه گشت پیمان برین</p></div>
<div class="m2"><p>که آسوده دارند کشور زکین</p></div></div>