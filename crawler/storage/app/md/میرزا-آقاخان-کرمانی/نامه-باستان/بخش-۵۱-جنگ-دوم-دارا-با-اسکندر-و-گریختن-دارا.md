---
title: >-
    بخش ۵۱ - جنگ دوم دارا با اسکندر و گریختن دارا
---
# بخش ۵۱ - جنگ دوم دارا با اسکندر و گریختن دارا

<div class="b" id="bn1"><div class="m1"><p>چو برخواند دارا بسی شد دژم</p></div>
<div class="m2"><p>به جنگ اندران رای زد بیش و کم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درم داد و روزی دهان را بخواند</p></div>
<div class="m2"><p>سپه را سوی دشت اربیل راند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هامون اگر بود شیب و فراز</p></div>
<div class="m2"><p>بفرمود کردن به گردون کراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که پیل و سواره در آن پهن دشت</p></div>
<div class="m2"><p>توانند هر جایگه پهن گشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز آن سوی اسکندر نامدار</p></div>
<div class="m2"><p>یکی لشکر آراست بیش از شمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مصر و فلسطین و هاماوران</p></div>
<div class="m2"><p>هم از لیدیا نیز نام آوران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گزین کرد و آمد به دشت نبرد</p></div>
<div class="m2"><p>به آوردگه اندر آورد کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان دو لشکر دو فرسنگ بود</p></div>
<div class="m2"><p>زمین از سواران کین تنگ بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی خواست دارا که کارزار</p></div>
<div class="m2"><p>فراگیرد آن لشکر نامدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولیکن پراکنده بودش سپاه</p></div>
<div class="m2"><p>سپاهی نه پر آرزو رزمخواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکسته دل و گشته از رزم سیر</p></div>
<div class="m2"><p>سکندر ابر نظم پالسکه چیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو سردار لشکر تنک دل بود</p></div>
<div class="m2"><p>به جنگ آوران کار مشکل بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گران مایگان زینهاری شدند</p></div>
<div class="m2"><p>به نزد سکندر به زاری شدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو دارا چنان دید برکاست روی</p></div>
<div class="m2"><p>گریزان همی رفت باهای هوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زاربیل آمد به سوی مدی</p></div>
<div class="m2"><p>ازو دور شد فره ایزدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز آن رو سکندر به بابل رسید</p></div>
<div class="m2"><p>در آن جایگه چند ماه آرمید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آن جا به شوش اندر آمد دلیر</p></div>
<div class="m2"><p>نشست از برگاه فرخ زریر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زسوزا بیامد به استرخ باز</p></div>
<div class="m2"><p>زمستان به سر برد آن جا دراز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی آتش افروخت در پرسه ویل</p></div>
<div class="m2"><p>که استرخ شد سوخته تا دو میل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ازین کار بهرش نبد جز زیان</p></div>
<div class="m2"><p>همی جست شادی یونانیان</p></div></div>