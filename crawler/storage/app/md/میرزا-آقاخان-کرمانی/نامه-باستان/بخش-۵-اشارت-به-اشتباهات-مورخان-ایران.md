---
title: >-
    بخش ۵ - اشارت به اشتباهات مورخان ایران
---
# بخش ۵ - اشارت به اشتباهات مورخان ایران

<div class="b" id="bn1"><div class="m1"><p>گهی شاه را پهلوان خوانده است</p></div>
<div class="m2"><p>گهی عصر این را بدان رانده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی کشوری را کند شاه نام</p></div>
<div class="m2"><p>گهی شخص نامیده قومی تمام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی نام جنگی شده پهلوان</p></div>
<div class="m2"><p>گهی گوید از دیو و از جاودان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی را کند شاه سالی هزار</p></div>
<div class="m2"><p>پدر را به جای پسر شهریار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریغا که تاریخ در آن زمان</p></div>
<div class="m2"><p>چو مه بود در زیر ابری نهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم آثار پیدا نبودی بسی</p></div>
<div class="m2"><p>خطوط کهن را نخواندی کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زآتور و از بابل و لیدیا</p></div>
<div class="m2"><p>زآلام و از هتن و از میدیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز یونان و از مصر و قوم فنیس</p></div>
<div class="m2"><p>نبود آگهی نزد دستان نویس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازین رو سروده سخن ناشناس</p></div>
<div class="m2"><p>یکی را نموده به دیگر قیاس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمان های شاهان همه درهم است</p></div>
<div class="m2"><p>سخن های تاریخ بس مبهم است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو صد سال شاهی کند کیقباد</p></div>
<div class="m2"><p>کس از توس و آرش نیارد بیاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فریبرز کو بود شاه مدی</p></div>
<div class="m2"><p>نتابید ازو فره ایزدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو کاوس فرمان براند بسی</p></div>
<div class="m2"><p>زاکمینیان یاد نآرد کسی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو لهراسب شاهی کند گاه دیر</p></div>
<div class="m2"><p>به یونان سفارت نماید زریر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو گشتاسب باشد یکی شهریار</p></div>
<div class="m2"><p>زشاهی فرو ماند اسفندیار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سه پنجه که شاهی کند اردشیر</p></div>
<div class="m2"><p>دوم اردشیر را نیاید بویر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو رستم زید سالیان دراز</p></div>
<div class="m2"><p>به هر جنگ او خود بود رزم ساز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو گرشسب باز او نشیند به تخت</p></div>
<div class="m2"><p>شود زال زر پهلوی نیکبخت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو سیروس خواند همی زند و است</p></div>
<div class="m2"><p>مه و سال زردشت نباشد درست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو ضحاک شاهیست سالی هزار</p></div>
<div class="m2"><p>نیایند کلدانیان در شمار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فریدون چو شاهی کند پنج صد</p></div>
<div class="m2"><p>به آبادیان سلطنت کی رسد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو افراسیاب است خاقان ترک</p></div>
<div class="m2"><p>همان شیده او راست پور بزرگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو ایلاگوی شد ز تورانیان</p></div>
<div class="m2"><p>بود رود کارن یکی پهلوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو ایراک پور فریدون بود</p></div>
<div class="m2"><p>شلمناصر و تور وارون شود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو آثور شد سرو شاه یمن</p></div>
<div class="m2"><p>بود شهر نینویه نام چمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو سگسار شد نام مازندران</p></div>
<div class="m2"><p>همان گرگساران بود اندران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو ملک دیاگوست دشت دغو</p></div>
<div class="m2"><p>ز گرسیوز آرند دختی نکو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو آرمینه هم گشت سرحد تور</p></div>
<div class="m2"><p>گرازان در آن جا برآرند شور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر ایرش همان جنگ آرش بود</p></div>
<div class="m2"><p>چو سیوز که نام سیاوش بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گروهی زره پهلوانی نبود</p></div>
<div class="m2"><p>فرود دلاور جوانی نبود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه پیران ویسه است دستور شه</p></div>
<div class="m2"><p>که او خود بود جاودانی سپه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آلانان به جز ملک آلام نیست</p></div>
<div class="m2"><p>که گوید به شهنامه نام دژیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گروهی ز تاتار باشد پشنگ</p></div>
<div class="m2"><p>که بودند به امیدیان پیش جنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گهارگهانی است هون سیاه</p></div>
<div class="m2"><p>که بودی قراخان زتوران سپاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بلاشان به جز قوم شریک نیست</p></div>
<div class="m2"><p>چو بیژن که افسانه ی بی زنی ست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پشن خود بود جنگ نورانیاس</p></div>
<div class="m2"><p>که اندر پلانه بد آن را اساس</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از این گونه باشد غلط ها بسی</p></div>
<div class="m2"><p>که آگه نبودی از این ها کسی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مگر اندرین عصر فرخنده بن</p></div>
<div class="m2"><p>که پیدا شد آن رازهای کهن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دریغ است که امروز ایرانیان</p></div>
<div class="m2"><p>ندانند تاریخ پیشینیان</p></div></div>