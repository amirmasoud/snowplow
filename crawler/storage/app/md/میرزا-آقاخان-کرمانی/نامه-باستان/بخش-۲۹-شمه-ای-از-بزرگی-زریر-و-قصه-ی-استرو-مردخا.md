---
title: >-
    بخش ۲۹ - شمه ای از بزرگی زریر و قصه ی استرو مردخا
---
# بخش ۲۹ - شمه ای از بزرگی زریر و قصه ی استرو مردخا

<div class="b" id="bn1"><div class="m1"><p>زشاهان فزون بود او را منش</p></div>
<div class="m2"><p>که بگذشت ستراپش از بیست و شش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی شارسان ساخت زرنوش نام</p></div>
<div class="m2"><p>به شوش اندر آن زیستی شاد کام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیاراست جشنی چو خرم بهار</p></div>
<div class="m2"><p>که هرگز نبیند چون او روزگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشیدند شیلانی آن جا سترگ</p></div>
<div class="m2"><p>زهر سو بخواندند خورد و بزرگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همانا در آن جشن و آن دستگاه</p></div>
<div class="m2"><p>ز بانوی و شتی برنجید شاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک ماهرو برگزید از یهود</p></div>
<div class="m2"><p>سپس بانوی بانوانش نمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که استیر خوانند نامش مگر</p></div>
<div class="m2"><p>بدی مردخایش برادر پدر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان جوی هومان که دستور بود</p></div>
<div class="m2"><p>هم از مردخا سخت رنجور بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی خواست کشتن یهودان همه</p></div>
<div class="m2"><p>که او بود چون گرگ ایشان رمه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز نیروی آن بانوی حورزاد</p></div>
<div class="m2"><p>سر خویش را داد هومان به باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازو گشت ایران سراسر غمی</p></div>
<div class="m2"><p>که شد آرز و بزم بودش همی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی خواجه بد نام او مهر داد</p></div>
<div class="m2"><p>که از شاه جانش نبد هیچ شاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به همراهی نامور اردوان</p></div>
<div class="m2"><p>بکشتند مرشاه را هردوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>توانه همی گفت با اردشیر</p></div>
<div class="m2"><p>که دارا بکشته است فرخ زریر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برفتند و او را بکشتند زار</p></div>
<div class="m2"><p>به کین خواهی نامور شهریار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دریغ آن نبرده زریر سوار</p></div>
<div class="m2"><p>همان شاهزاده که شد کشته زار</p></div></div>