---
title: >-
    بخش ۱ - نامهٔ باستان در یگانگی خداوند پاک
---
# بخش ۱ - نامهٔ باستان در یگانگی خداوند پاک

<div class="b" id="bn1"><div class="m1"><p>سر نامه بر نام زروان پاک</p></div>
<div class="m2"><p>که رخشید ازو هرمز تابناک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خداوند زاووش و کیوان پیر</p></div>
<div class="m2"><p>فروزندهٔ ماه و ناهید و تیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وز او آفرین باد بر ایزدان</p></div>
<div class="m2"><p>که هستند فرمانبرش جاودان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم امشاسپندان با زور و دست</p></div>
<div class="m2"><p>که دارند بر کوه ها را نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وهومانو آن پیک هوش و خرد</p></div>
<div class="m2"><p>کزو برتر اندیشه برنگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر ثاریوهرا سروش شهان</p></div>
<div class="m2"><p>کزاو گردد آباد ویران جهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپندار میتا شه زندگی</p></div>
<div class="m2"><p>که جان ها ازو یافت پایندگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فراواشی امشاسفند روان</p></div>
<div class="m2"><p>که آخر شتابد سوی آسمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی باد نفرین ناخوب و زشت</p></div>
<div class="m2"><p>ابر انگرومانیوس پلشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که اهریمنانند او را رهی</p></div>
<div class="m2"><p>ز نور و فروغ است جانش تهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اکومانو آن دیو تیره روان</p></div>
<div class="m2"><p>که از زهر تن‌ها کند ناتوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دگر زیری آن مایهٔ بغض و کین</p></div>
<div class="m2"><p>کز او شد پر آشوب روی زمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دگر سااورو آن بد بدکنش</p></div>
<div class="m2"><p>که آز و فریب است او را منش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همان اندریمان ناپاک رای</p></div>
<div class="m2"><p>که جور و ستم ماند از وی به جای</p></div></div>