---
title: >-
    بخش ۱۱ - احوال سلم و تور و عصر پهلوانی
---
# بخش ۱۱ - احوال سلم و تور و عصر پهلوانی

<div class="b" id="bn1"><div class="m1"><p>اگرچه پس از آن دو فرمانروا</p></div>
<div class="m2"><p>شلمنصر و آتور از نینوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که امروز خوانندشان سلم و تور</p></div>
<div class="m2"><p>بر ایران همی تاختندی ستور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی پهلوانان آن روزگار</p></div>
<div class="m2"><p>چو گرشسب و نیرم چو سام سوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همان قارن و اکمن پیلتن</p></div>
<div class="m2"><p>دگر زال و مهراب لشکر شکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو رستم که او شد سر داستان</p></div>
<div class="m2"><p>لقب کرد دستان ورا باستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زواره فرامرز و کشواد پیر</p></div>
<div class="m2"><p>چو سیرنک شاه آن یل تیز ویر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همان زو که افراخت زابلستان</p></div>
<div class="m2"><p>همه سیستان ساخت چون گلستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو تهماسب که اوزاب را بد پدر</p></div>
<div class="m2"><p>تهمتن به شهنامه خواندش مگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کریمان به جز او نبوده است کس</p></div>
<div class="m2"><p>همان سام سهم آبان است و بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرند همایون و فرخ تورک</p></div>
<div class="m2"><p>شماساس و اترد شد سبب بزرگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو گودرز کاو بد کدور لاهور</p></div>
<div class="m2"><p>چو کهرم که جهرم ازو بد مگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همان خوم بابا کش بدی نام، هوم</p></div>
<div class="m2"><p>در اهواز بودی ورا مرز و بوم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین پهلوانان با فر و زور</p></div>
<div class="m2"><p>که بر چرخ گردون رساندند شور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در ایام فترت دلیران بدند</p></div>
<div class="m2"><p>به مردی نگهدار ایران بدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گهی رزم به آتوریان ساختند</p></div>
<div class="m2"><p>گهی بر فراز آبیان تاختند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرا از زبان دری نیست تاب</p></div>
<div class="m2"><p>فراز آب را خوانده افراسیاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو جیحون که او را چو بنگاشتند</p></div>
<div class="m2"><p>مران رود را جهن پنداشتند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان شیده که اقوام شیتا بدند</p></div>
<div class="m2"><p>به قوم فراز آب همتا بدند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر ایران همی تاختند آن گروه</p></div>
<div class="m2"><p>همه مردم آمد از ایشان ستوه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دلیران ایران درین دار و گیر</p></div>
<div class="m2"><p>زهر سو به کردار درنده شیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نگه داشتند آن سریر مهی</p></div>
<div class="m2"><p>که گاه مهی بد زشاهان تهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو گرشسب کوشد به هندو دیار</p></div>
<div class="m2"><p>سمیرامس از وی بشد زخم دار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بجنگید با نامور رامسیس</p></div>
<div class="m2"><p>که خوانندش امروز سیروسریس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به «ارکوشیا» بد مگر این نبرد</p></div>
<div class="m2"><p>که هندوکشش خوانده آزاد مرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همان هاکهامانش نامور</p></div>
<div class="m2"><p>که سیروس را بود هفتم پدر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زبابل همی تاخت تا نینوا</p></div>
<div class="m2"><p>به آلامیان بود فرمانروا</p></div></div>