---
title: >-
    بخش ۵۹ - پادشاهی بهرام دوم
---
# بخش ۵۹ - پادشاهی بهرام دوم

<div class="b" id="bn1"><div class="m1"><p>ورارام پور دلارام او</p></div>
<div class="m2"><p>که بهرام بهرام بد نام او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس از وی به گاه مهی برنشست</p></div>
<div class="m2"><p>شهی بود با داد یزدان پرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه بگردید از راه داد</p></div>
<div class="m2"><p>ولی موبدان موبدش پند داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شه روم کش نام پربوس بود</p></div>
<div class="m2"><p>ابا لشگر و پیل و باکوس بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیاراست با او یکی کارزار</p></div>
<div class="m2"><p>ولیکن به ره گشته گردید زار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان جوی کاروس شد جانشین</p></div>
<div class="m2"><p>سوی تیسفون اندر آمد به کین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یک تیر شد کشته در رزمگاه</p></div>
<div class="m2"><p>پراکنده گشتند رومی سپاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیوکلس پس از وی سپهدار شد</p></div>
<div class="m2"><p>سوی روم از دشت پیکار شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ورارام با لشکری رزم ساز</p></div>
<div class="m2"><p>سوی ارمنستان بیامد فراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز آن روی قیصر سپه برکشید</p></div>
<div class="m2"><p>به جنگ ورارام لشکر کشید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو پیروز نامد یکی زین دو شاه</p></div>
<div class="m2"><p>زپیکار برگشت هر دو سپاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس از اندکی مرد بهرام گرد</p></div>
<div class="m2"><p>زگیتی به جز تخم نیکی نبرد</p></div></div>