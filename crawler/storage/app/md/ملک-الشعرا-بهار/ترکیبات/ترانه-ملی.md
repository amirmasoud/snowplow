---
title: >-
    ترانهٔ ملی
---
# ترانهٔ ملی

<div class="b" id="bn1"><div class="m1"><p>دوشینه ز رنج دهر بدخواه</p></div>
<div class="m2"><p>رفتم سوی بوستان نهانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا وارهم از خمار جانکاه</p></div>
<div class="m2"><p>در لطف و هوای بوستانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدم گل‌های نغز و دلخواه</p></div>
<div class="m2"><p>خندان ز طراوات جوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغان لطیف طبع آگاه</p></div>
<div class="m2"><p>نالان به نوای باستانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آتش روی گل شبانگاه</p></div>
<div class="m2"><p>هر یک سرگرم زندخوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من بی‌خبرانه رفتم از راه</p></div>
<div class="m2"><p>از آن نغمات آسمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خود گفتم به ناله و آه</p></div>
<div class="m2"><p>کای رانده ز عالم معانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با بال ضعیف و پر کوتاه</p></div>
<div class="m2"><p>پرواز بلند کی توانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بودم در این سخن که ناگاه</p></div>
<div class="m2"><p>مرغی به زبان بی‌زبانی</p></div></div>
<div class="b2" id="bn10"><p>این مژده به گوش من رسانید</p>
<p>کزرحمت حق مباش نومید</p></div>
<div class="b" id="bn11"><div class="m1"><p>گر از ستم سپهرکین توز</p></div>
<div class="m2"><p>یک چند بهار ما خزان شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وز کید مصاحب بدآموز</p></div>
<div class="m2"><p>چوپان بر گله سر گران شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روزی دو سه‌، آتش جهانسوز</p></div>
<div class="m2"><p>در خرمن ملک میهمان شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خون‌های شریف پاک هر روز</p></div>
<div class="m2"><p>بر خاک منازعت روان شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وان قصهٔ زشت حیرت‌اندوز</p></div>
<div class="m2"><p>سرمایهٔ عبرت جهان شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>امروز به فر بخت فیروز</p></div>
<div class="m2"><p>دل‌های فسرده شادمان شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از فر مجاهدان بهروز</p></div>
<div class="m2"><p>آن ‌را که ‌دل تو خواست‌ آن شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وز تابش مهر عالم‌افروز</p></div>
<div class="m2"><p>ایران فردوس جاودان شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شد شامش روز و روز نوروز</p></div>
<div class="m2"><p>زین بهتر نیز می‌توان شد</p></div></div>
<div class="b2" id="bn20"><p>روزی دو سه صبرکن به امید</p>
<p>از رحمت حق مباش نومید</p></div>
<div class="b" id="bn21"><div class="m1"><p>از عرصهٔ تنگ حصن بیداد</p></div>
<div class="m2"><p>انصاف برون جهاند مرکب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در معرکه داد پردلی داد</p></div>
<div class="m2"><p>آن دانا فارس مهذب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شاهین کمال‌، بال بگشاد</p></div>
<div class="m2"><p>برکند ز جغد جهل مخلب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>استاد بزرگ، لوح بنهاد</p></div>
<div class="m2"><p>شد مدرس کودکان مرتب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آمد به نیاز، پیش استاد</p></div>
<div class="m2"><p>آن طفل گریخته ز مکتب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>استاد خجسته پی در استاد</p></div>
<div class="m2"><p>تا کودک را کند مؤدب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آواز به شش جهت درافتاد</p></div>
<div class="m2"><p>از غفلت دیو و سطوت رب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای از شب هجر بوده ناشاد</p></div>
<div class="m2"><p>برخیز که رهسپار شد شب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صبح آمد و بردمید خورشید</p></div>
<div class="m2"><p>از رحمت حق مباش نومید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای سر به ره نیاز سوده</p></div>
<div class="m2"><p>با سرخوشی و امیدواری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>منشور دلاوری ربوده</p></div>
<div class="m2"><p>در عرصهٔ رزم جان‌سپاری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>با داس مقاومت دروده</p></div>
<div class="m2"><p>کشت ستم و تباهکاری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زنگار ظلام را زدوده</p></div>
<div class="m2"><p>ز آئینهٔ دین کردگاری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لب بسته و بازوان گشوده</p></div>
<div class="m2"><p>وز دین قویم، کرده یاری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وندر طلب حقوق بوده</p></div>
<div class="m2"><p>چون کوه‌، قرین بردباری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جان داده و آبرو فزوده</p></div>
<div class="m2"><p>در راه بقای کامکاری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وین گلشن تازه را نموده</p></div>
<div class="m2"><p>از خون شریف‌، آبیاری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مشتیز به دهر ناستوده</p></div>
<div class="m2"><p>کز منظرهٔ امیدواری</p></div></div>
<div class="b2" id="bn39"><p>خورشید امید باز تابید</p>
<p>از رحمت حق مباش نومید</p></div>
<div class="b" id="bn40"><div class="m1"><p>ای شیردل ای دلیر ستار</p></div>
<div class="m2"><p>سردار مجاهدان تبریز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ای بسته میان به فر دادار</p></div>
<div class="m2"><p>در حفظ حقوق عزت‌آمیز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای ناصر ملت ای سپهدار</p></div>
<div class="m2"><p>ای ازره جور کرده پرهیز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ای باقرخان راد سالار</p></div>
<div class="m2"><p>بر خرمن جور آتش‌انگیز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای صمصام ای بزرگ سردار</p></div>
<div class="m2"><p>آب دم تیغت آتش تیز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ای سید لاری ای ز پیکار</p></div>
<div class="m2"><p>کرمان بگرفته تابه نیریز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همدست شوید جمله احرار</p></div>
<div class="m2"><p>تا پای کشد عدوی خونریز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر رایت خود کنید ستوار</p></div>
<div class="m2"><p>زین معنی دلکش دلاویز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کانصاف بساط جور برچید</p></div>
<div class="m2"><p>از رحمت حق مباش نومید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ای حجت‌دین حکیم مشفق‌</p></div>
<div class="m2"><p>وی محیی دین حق محمد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ای فخر تبار و آل صادق</p></div>
<div class="m2"><p>سبط علی و سلیل احمد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ای بر تو شعار شرع لایق</p></div>
<div class="m2"><p>ای از تو اساس دین مشید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گر بر تو ز دهر ناموافق</p></div>
<div class="m2"><p>شد ظلم و جفا و جور بی‌حد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خوش باش که بخت شد موافق</p></div>
<div class="m2"><p>و اقبال برون کشید مسند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>طوس از علمای فحل مفلق</p></div>
<div class="m2"><p>گردید چو جنت مخلد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خرم شد مشهد حقایق</p></div>
<div class="m2"><p>از فر مجاهدین مشهد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>با ترکان برخلاف سابق</p></div>
<div class="m2"><p>گشتند به دوستی مقید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>در یاری دین شدند شایق</p></div>
<div class="m2"><p>زان کرد خدایشان مؤید</p></div></div>
<div class="b2" id="bn58"><p>دین یابد از این گروه تأیید</p>
<p>از رحمت حق مباش نومید</p></div>
<div class="b" id="bn59"><div class="m1"><p>صد شکر که کار یافت قوت</p></div>
<div class="m2"><p>از یاری حجهٔ خراسان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>وان قبله و پیشوای امت</p></div>
<div class="m2"><p>سرمایه حرمت خراسان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بن موسی جعفر آن که عزت</p></div>
<div class="m2"><p>افزوده به عزت خراسان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بگرفت نکو به دست قدرت</p></div>
<div class="m2"><p>سررشتهٔ قدرت خراسان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>وز همت عاقلان ملت</p></div>
<div class="m2"><p>شد نادره ملت خراسان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>وز عالم فحل با حمیت</p></div>
<div class="m2"><p>شد شهره حمیت خراسان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ترکان دلیر با فتوت</p></div>
<div class="m2"><p>کردند حمایت خراسان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نیز از علمای خوش رویت</p></div>
<div class="m2"><p>خوش گشت رویت خراسان</p></div></div>
<div class="b2" id="bn67"><p>زین بهتر نیز خواهیش دید</p>
<p>از رحمت حق مباش نومید</p></div>