---
title: >-
    انتقاد از دولت
---
# انتقاد از دولت

<div class="b" id="bn1"><div class="m1"><p>یاران روش دگر گرفتند</p></div>
<div class="m2"><p>وز ما دل و دیده برگرفتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مسلک ما شدند دلگیر</p></div>
<div class="m2"><p>پس مسلک خوبتر گرفتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سایهٔ طبع اعتدالی</p></div>
<div class="m2"><p>پیرایهٔ مختصر گرفتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر زشتی را نکو گزیدند</p></div>
<div class="m2"><p>هر نفعی را ضرر گرفتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز خارجیان ز ساده‌لوحی</p></div>
<div class="m2"><p>زهر از عوض شکرگرفتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرمان شکوه خویشتن را</p></div>
<div class="m2"><p>از دشمن کینه‌ور گرفتند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باری هرکار پرخطر را</p></div>
<div class="m2"><p>کاینان ز ره خطر گرفتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بازی بازی ز کف نهادند</p></div>
<div class="m2"><p>شوخی شوخی ز سر گرفتند</p></div></div>
<div class="b2" id="bn9"><p>غافل که به خانقاه احرار</p>
<p>سیصد گوش است پشت دیوار</p></div>