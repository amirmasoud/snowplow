---
title: >-
    نکیر و منکر
---
# نکیر و منکر

<div class="b" id="bn1"><div class="m1"><p>چون فروبردند نعشم را به گور</p></div>
<div class="m2"><p>خاک افشاندند و زان گشتند دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگهان آواز پایی سهمناک</p></div>
<div class="m2"><p>کردگوشم را خبر از راه دور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بسان خفته زان آواز پای</p></div>
<div class="m2"><p>جستم وآمد به مغزاندر شعور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه هوا ونه فضا ونه نسیم</p></div>
<div class="m2"><p>سینه تنگ وپای لنگ وجسم عور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک در آن حفرهٔ تاریک و تنگ</p></div>
<div class="m2"><p>هردو جشمم خیره شد ناگه زنور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوشه‌ای از خاک من شد چاک و زان</p></div>
<div class="m2"><p>کرد منکربارفیق‌خودظهور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوفرشته چون دوفیل خشمگین</p></div>
<div class="m2"><p>من فتاده پیش ایشان همچو مور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من به زحمت از فشارگور، لیک</p></div>
<div class="m2"><p>آن دو می کردند هر جانب عبور</p></div></div>
<div class="b2" id="bn9"><p>گور تنگ است از برای مجرمان</p>
<p>از برای مؤمنان باغی است‌، گور</p></div>
<div class="b" id="bn10"><div class="m1"><p>روی چون ازآهن تفته سپر</p></div>
<div class="m2"><p>بر جبین‌شان گشته‌رسم آیات شر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از دهانی شیروش پهن و فراخ</p></div>
<div class="m2"><p>نابها پیدا بسان شیر نر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بینیی هایل چو شاخ کرگدن</p></div>
<div class="m2"><p>جسته بالا نوک آن همچو تبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درکف هریک عمودی آتشین</p></div>
<div class="m2"><p>وافعیئی پیچیده برگرد کمر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سوی من زان چشم‌های چون تنور</p></div>
<div class="m2"><p>هر دم افشاندند صد خرمن شرر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بانگ زد برمن یکی زآنان که خیز</p></div>
<div class="m2"><p>هرچه گویم پاسخ آور مختصر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>استخوان‌هایم به پیچ و خم فتاد</p></div>
<div class="m2"><p>زان درشت آوا و بانگ زهره‌در</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خویش راکردم مهیٌای جواب</p></div>
<div class="m2"><p>تا چه پرسند ازمن آسیمه‌سر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتگو برخاست در زهدان خاک</p></div>
<div class="m2"><p>بین من وآن یک که بد نزدیکتر</p></div></div>
<div class="b2" id="bn19"><p>زیرپایش من چو گنجشکی حقیر</p>
<p>او چو کرکس از برم بگشوده پر</p></div>
<div class="b" id="bn20"><div class="m1"><p>گفت با من‌، کیستی ای مرد پیر؟</p></div>
<div class="m2"><p>گفتمش پیری به خاک اندر اسیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت‌: وقت زندگی‌، اعمال تو؟</p></div>
<div class="m2"><p>گفتمش چون دیگران پست و حقیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در جهان از من نیامد در وجود</p></div>
<div class="m2"><p>هیچ کاری عمده و امری خطیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت ازین فن‌ها و صنعت‌های دهر</p></div>
<div class="m2"><p>در کدامین بودی استاد و بصیر؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفتمش در صنعت شعر و ادب</p></div>
<div class="m2"><p>بودم استاد و ز نقاشی خبیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت گاه زندگی دینت چه بود؟</p></div>
<div class="m2"><p>گفتمش اسلام را بودم نصیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت معبود تو در گیتی که بود؟</p></div>
<div class="m2"><p>گفتمش معبود من حی قدیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت چون بگذاشتی گیتی‌، که تو</p></div>
<div class="m2"><p>بر تن و بر نفس خود بودی امیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفتم از عمرم چه‌می‌پرسی که رفت</p></div>
<div class="m2"><p>جمله با خون دل ورنج ضمیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دور، از آزادی و از اختیار</p></div>
<div class="m2"><p>جفت‌، با ناچاری و ضعف و زحیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نی هنر تا دهر را پیچم عنان</p></div>
<div class="m2"><p>نی توان تا چرخ را بندم مسیر</p></div></div>
<div class="b2" id="bn31"><p>بهتر از من پارهٔ سنگی که نیست</p>
<p>آمر و مامور وگویا و بصیر</p></div>
<div class="b" id="bn32"><div class="m1"><p>گفت کاری کرده‌ام غیر از گناه‌؟</p></div>
<div class="m2"><p>گفتم آری تکیه برلطف اله</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>من نگویم چون دگر مردم سخن</p></div>
<div class="m2"><p>آن زمان کم باز پرسند ازگناه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عامیان در بند اوهام اندرند</p></div>
<div class="m2"><p>هستشان این بستگی به از رفاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برگنه خستو شدن اولیتر است</p></div>
<div class="m2"><p>مرد دانا را زگفتار تباه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بس حدیثا کش خرد گوید، ولیک</p></div>
<div class="m2"><p>قلب بر عکسش پذیرد انتباه</p></div></div>
<div class="b2" id="bn37"><p>گندم و جوهر دو راکاهست لیک</p>
<p>فرق بسیار است بین این دو کاه</p></div>
<div class="b" id="bn38"><div class="m1"><p>من که بودم در شداید پایدار</p></div>
<div class="m2"><p>سست گشتم ناگهان بی‌اختیار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>قلب من لرزید و کی بودم گمان</p></div>
<div class="m2"><p>کاین چنین قلبی بلرزد روزگار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>لیک خود را با خود آوردم نخست</p></div>
<div class="m2"><p>تا بجا آمد دلم زان گیر و دار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خویشتن را وانمودم با دلی</p></div>
<div class="m2"><p>از یقین ثابت نه از شک بی‌قرار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گرچه آخر از سخن‌های صریح</p></div>
<div class="m2"><p>تیره کردم باز خود را روزگار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خاطر آزاد مرد نکته‌سنج</p></div>
<div class="m2"><p>کی پسندد گفتهٔ نااستوار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ناپسند آید دورویی از ادیب</p></div>
<div class="m2"><p>ناسزا باشد نفاق از هوشیار</p></div></div>
<div class="b2" id="bn45"><p>لاجرم بر من گذشت آن بد که خاست</p>
<p>از نهیبش نعره از اهل مزار</p></div>