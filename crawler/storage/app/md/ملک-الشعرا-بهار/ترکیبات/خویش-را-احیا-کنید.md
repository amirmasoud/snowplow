---
title: >-
    خویش را احیا کنید
---
# خویش را احیا کنید

<div class="b" id="bn1"><div class="m1"><p>ای سفیهان بهر خود هم اندکی غوغا کنید</p></div>
<div class="m2"><p>حال خود را دیده‌، واغوثا و واویلا کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیسه‌های خالی خود را دهید آخر تکان</p></div>
<div class="m2"><p>پس تکانی خورده دزد خویش را پیدا کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به کی با این لباس ژنده می‌ریزید اشگ</p></div>
<div class="m2"><p>با جوی غیرت لباس از اطلس و دیبا کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشته شد شاه شهیدان تا شما گیرید پند</p></div>
<div class="m2"><p>پیش ظالم پافشاری یکه و تنها کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانه‌هاتان شد خراب اما صداهاتان گرفت</p></div>
<div class="m2"><p>آخر ای خانه‌خرابان لااقل نجوا کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انتظار از مجلس و از شیخ و از ملای شهر</p></div>
<div class="m2"><p>کار بیهوده است خود را حاضر دعوا کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خودکشی باشد قمه برسرزدن‌، آن تیغ تیز</p></div>
<div class="m2"><p>بر سر دشمن زنید و خویش را احیا کنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این دکاکین کساد ای اهل تهران بسته به</p></div>
<div class="m2"><p>دکه بربندید و مشت ظالمان را واکنید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای جوانان مدارس‌، بی‌سوادان حاکمند</p></div>
<div class="m2"><p>این گروه بینوا و سفله را رسوا کنید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای رفیقان اداری‌، رفت قانون زبر پای</p></div>
<div class="m2"><p>حفظ قانون را قیامی سخت و پابرجا کنید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای دیانت‌پیشگان دین رفت و دنیا نیز رفت</p></div>
<div class="m2"><p>جشم‌پوشی بعد از این از دین و از دنیا کنید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چشم‌هاتان روشن ای مشروب خواران قدیم</p></div>
<div class="m2"><p>هم به‌ضد یکدگر هنگامه و غوغا کنید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کشور دارا لگدکوب سمند جور شد</p></div>
<div class="m2"><p>راستی فکری برای کشور دارا کنید</p></div></div>
<div class="b2" id="bn14"><p>چون که ننهادید بر قانون و بر خویش احترام</p>
<p>مستبدین از شما یک‌یک کشیدند انتقام</p></div>
<div class="b" id="bn15"><div class="m1"><p>رفته حس مردمی از مرد و زن‌، من باکیم</p></div>
<div class="m2"><p>نیست گوشی تا نیوشد این سخن‌، من با کیم؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیست سال افزون زدم داد وطن، نشنید کس</p></div>
<div class="m2"><p>تازه از نو می‌زنم داد وطن‌، من با کیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همچو بلبل گر هزار آوا برآرم‌، چون که هست</p></div>
<div class="m2"><p>گوش‌ها بر نغمهٔ زاغ و زغن‌، من با کیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هی‌علی و هی‌حسین و هی‌حسن گویم‌،‌چو نیست</p></div>
<div class="m2"><p>نی علی و نی حسین و نی حسن‌، من با کیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گاه گویم کز مشیری مؤتمن جویم علاج</p></div>
<div class="m2"><p>چون نمی‌بینم‌ مشیری مؤ تمن‌، من با کیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>می‌زنم در انجمن فریاد واوبلا ولیک</p></div>
<div class="m2"><p>پنبه دارد گوش اهل انجمن‌، من با کیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خلق ایران دسته‌ای دزدند و بی‌دین‌، دسته‌ای</p></div>
<div class="m2"><p>سینه‌زن‌، زنجیرزن‌، قداره‌زن‌، من با کیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گویم این قداره را بر گردن ظالم بزن</p></div>
<div class="m2"><p>لیک شیطان گویدش بر خود بزن‌، من باکیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گویم این زنجیر بهر قید دزدانست و او</p></div>
<div class="m2"><p>هی زند زنجیر را بر خویشتن‌، من با کیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گویم ای نادان به ظلم ظالمان گردن منه</p></div>
<div class="m2"><p>او بخارد گردن و ریش و ذقن‌، من با کیم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گویمش باید بپوشانی کفن بر دشمنان</p></div>
<div class="m2"><p>باز می‌پوشد به عاشورا کفن‌، من با کیم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گویم ای واعظ دهانت را لئیمان دوختند</p></div>
<div class="m2"><p>او همی بلعد ز بیم آب دهان‌، من با کیم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گویم ای‌ آخوند خوردند این شپش‌ها خون تو</p></div>
<div class="m2"><p>او شپش می‌جوید اندر پیرهن‌، من با کیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گوبمش دین رفت از کف‌، گوید این باشد دلیل</p></div>
<div class="m2"><p>بر ظهور مهدی صاحب زمن‌، من با کیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گویم ای کلاش‌، آخر این گدایی تا به کی</p></div>
<div class="m2"><p>گوبدم‌: چیزی به نذر پنج تن‌، من با کیم</p></div></div>
<div class="b2" id="bn30"><p>پس همان بهتر که لب بربندم از گفت و شنید</p>
<p>مستمع چون نیست باری‌، خامشی باید گزید</p></div>