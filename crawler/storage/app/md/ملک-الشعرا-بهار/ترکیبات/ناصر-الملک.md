---
title: >-
    ناصر  الملک
---
# ناصر  الملک

<div class="b" id="bn1"><div class="m1"><p>نایب شه چون زگیتی رخت بست</p></div>
<div class="m2"><p>ناصرالملک آمد و جایش نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظاهرا گفتند جمعی کم خرد</p></div>
<div class="m2"><p>بعد جاهل عالمی برجای هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتیم کاین مرد جبان</p></div>
<div class="m2"><p>پشت استقلال را خواهد شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این اروپایی‌پرست است از چه روی</p></div>
<div class="m2"><p>کام خواهید از اروپایی‌پرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این نسازد کار را محکم‌، ولی</p></div>
<div class="m2"><p>رشته‌های ملک را خواهدگسست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در اروپا پخته‌اند او را و او</p></div>
<div class="m2"><p>سخت از این پخت و پزهاگشته مست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخت مکار است و ترسو این جناب</p></div>
<div class="m2"><p>دل بر او زبن روی نتوانیم بست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابلهان گفتند خیر این‌طور نیست</p></div>
<div class="m2"><p>ناصرالملک آدمی دانشور است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکه جاهل ماند دور از آدمیست</p></div>
<div class="m2"><p>هرکه آدم شد ز قید جهل رست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما بدیشان یک مثل گفتیم نیز</p></div>
<div class="m2"><p>گرچه نشنیدند و تیر از شست جست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>« کای بسا ابلیس آدم‌رو که هست</p></div>
<div class="m2"><p>پس به هر دستی نباید داد دست‌»</p></div></div>
<div class="b2" id="bn12"><p>« گر به صورت آدمی انسان بدی‌»</p>
<p>«‌احمد و بوجهل هم یکسان بدی‌»</p></div>
<div class="b" id="bn13"><div class="m1"><p>هرکه روزی چند رفت اندر فرنگ</p></div>
<div class="m2"><p>کی شود اگه ز رسم نام وننگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وانکه درسی چند از طامات خواند</p></div>
<div class="m2"><p>کی کند در سینه‌اش دانش درنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیپلوماسی مشربان خشک مغز</p></div>
<div class="m2"><p>خود چه‌ می‌دانند جز نیرنگ و رنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>و آن همه نیرنگ‌هاشان صورتی است</p></div>
<div class="m2"><p>کز درون زشتست و از بیرون قشنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرکجا نفعی است شخصی‌، می‌پرند</p></div>
<div class="m2"><p>سوی آن چون جره باز تیز چنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سوی منصب حمله آرند این گروه</p></div>
<div class="m2"><p>چون مقیمان ترن هنگام زنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ناصرالملک از فرنگستان چه یافت</p></div>
<div class="m2"><p>جز تقلب‌های دزدان فرنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سیرتش باری همان باشدکه بود</p></div>
<div class="m2"><p>گرچه باشد صورت او رنگ رنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سخت نزدیک است شعر مولوی</p></div>
<div class="m2"><p>در صفات این چنین قوم دبنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>«‌یک شغالی رفت اندر خم رنگ</p></div>
<div class="m2"><p>اندر آن خم کرد یک ساعت درنگ</p></div></div>
<div class="b2" id="bn23"><p>«‌پس برآمد یال و دم رنگین شده‌»</p>
<p>« کاین منم طاووس علیین شده‌»</p></div>
<div class="b" id="bn24"><div class="m1"><p>ناصرالملک آن برید زشت‌پی</p></div>
<div class="m2"><p>از فرنگ آمد شتابان سوی ری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شورها انگیخت در آغاز کار</p></div>
<div class="m2"><p>با نواهای مخالف همچو نی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اکثریت گشت گردش چرخ زن</p></div>
<div class="m2"><p>چون بنات‌النعش برگرد جدی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حیلت آغازبد و ضدیت فکند</p></div>
<div class="m2"><p>در وکیلان‌، حیله‌بازی‌های وی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از بیانات پیاپی فاش کرد</p></div>
<div class="m2"><p>آن بناهایی که می‌افکند پی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باده‌ای کاندر اروپا خورده بود</p></div>
<div class="m2"><p>کرد در ایران به یک گفتار قی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیز از او در اعتدالیون فتاد</p></div>
<div class="m2"><p>آنچه در دیوانگان از شور می</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مست گشتند و سوی ما تاختند</p></div>
<div class="m2"><p>چون به سوی باغ‌، باد سرد دی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خان نایب نیز می‌بالید سخت</p></div>
<div class="m2"><p>کامدستم از اروپا سوی ری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بهر ایران علم و فضل آورده‌ام</p></div>
<div class="m2"><p>تا شوند از فضل من اموات حی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وه چه‌خوش گفت آن حکیم مولوی</p></div>
<div class="m2"><p>در صفات این گروه لابشی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>«‌آن یکی پرسید اشتر را که هی</p></div>
<div class="m2"><p>ازکجا می‌آیی ای فرخنده پی‌»</p></div></div>
<div class="b2" id="bn36"><p>« گفت از حمام گرم کوی تو»</p>
<p>« گفت خود پیداست از زانوی تو»</p></div>
<div class="b" id="bn37"><div class="m1"><p>ناصرالملک آمد و مسند ربود</p></div>
<div class="m2"><p>با وزیران پیل بازی‌ها نمود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حیله‌ها انگیخت تا خود از شمال</p></div>
<div class="m2"><p>شاه سابق با سواران رخ نمود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>(‌شستر) آن والا مشیر ارجمند</p></div>
<div class="m2"><p>بهر دفعش دست قدرت برگشود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نامداران نیز بر اسب نبرد</p></div>
<div class="m2"><p>زین فروبستند بی گفت و شنود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حمله‌های آتشین‌شان شاه را</p></div>
<div class="m2"><p>دادکش از هر طرف برسان دود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شاه خود شد مات لیکن کینه‌ها</p></div>
<div class="m2"><p>مر وزیران را ز شستر برفزود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دست در دامان این نایب زدند</p></div>
<div class="m2"><p>که بکن فکری در این هنگامه زود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خان نایب نیز انگشتی رساند</p></div>
<div class="m2"><p>تاکه از روسیه بالا شد عمود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آمد از روسیه اولتیماتومی</p></div>
<div class="m2"><p>سرخ و سبز و ازرق و زرد و کبود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ناصرالملک از طبابت‌های خویش</p></div>
<div class="m2"><p>این چنین بر خستگان بخشود سود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از دواهایش شفا نامد پدید</p></div>
<div class="m2"><p>وتن مریض از آن کسل‌تر شدکه بود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این مریض و این دوا را مولوی</p></div>
<div class="m2"><p>کرده اندر مثنوی خوش وانمود</p></div></div>
<div class="b2" id="bn49"><p>« گرقضا سرکنگبین صفرا فزود</p>
<p>روغن بادام خشکی می‌نمود»</p></div>
<div class="b" id="bn50"><div class="m1"><p>«‌آن علاج و آن طبابت‌های او»</p></div>
<div class="m2"><p>«‌ریخت یکسر از طبیبان آبرو»</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خائنان زینکار نبود ننگشان</p></div>
<div class="m2"><p>کور بادا کور چشم تنگشان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بنده و اجری خور روسند و بس</p></div>
<div class="m2"><p>از تمدن خواه تا الدنگشان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کفه‌شان بالاست در عرض دول</p></div>
<div class="m2"><p>نیست گو در ترازو سنگشان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>این وزیران کاروان غفلتند</p></div>
<div class="m2"><p>ناصرالملک است پیش‌آهنگشان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آن‌چنان قومی که این شان پیشواست</p></div>
<div class="m2"><p>چیست گویی دانش و فرهنگشان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>لاجرم این پیشوا بی‌هیچ عذر</p></div>
<div class="m2"><p>می کند تقدیم خصم اورنگشان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>وین خسان بینند و اصلاً شرم نیست</p></div>
<div class="m2"><p>نزکیومرث و نه از هوشنگشان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اندرین صلحی که کردند این گروه</p></div>
<div class="m2"><p>مولوی گفته است روی و رنگشان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>« کز خیالی صلحشان و جنگشان</p></div>
<div class="m2"><p>و از خیالی نامشان و ننگشان‌»</p></div></div>
<div class="b2" id="bn60"><p>«‌این وزیران از کهین و از مهین‌»</p>
<p>«‌لعنت‌الله علیهم اجمعین‌»</p></div>
<div class="b" id="bn61"><div class="m1"><p>ناصرالملک آن یل کار آزمود</p></div>
<div class="m2"><p>اندرین میدان میانداری نمود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گاه شد سر شاخ وگاه آمد به خاک</p></div>
<div class="m2"><p>گاه شد بالا وگاه آمد فرود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در مصالح کرد جنبش دیر دیر</p></div>
<div class="m2"><p>در مفاسد کرد کوشش زود زود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کشت ملت‌ را که خرم بود و سبز</p></div>
<div class="m2"><p>نارسیده از حیل بازی درود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>زان سپس قصد فراریدن گرفت</p></div>
<div class="m2"><p>تا نه بیند آنچه خود آورده بود</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کرد روشن آتش و خود روی تافت</p></div>
<div class="m2"><p>تا از آن ما را رود در چشم دود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کارهای ملک و رأی خو یش را</p></div>
<div class="m2"><p>جمله پیچید و به صندوقی نمود</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چون از ایران رفت آن صندوق را</p></div>
<div class="m2"><p>دست قدرت بی‌محابا برگشود</p></div></div>
<div class="b2" id="bn69"><p>«‌تا بداند مسلم و گبر و یهود»</p>
<p>« کاندرین صندوق جز لعنت نبود»</p></div>