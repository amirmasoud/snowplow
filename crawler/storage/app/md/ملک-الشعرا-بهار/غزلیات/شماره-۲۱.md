---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>شب فراق تو گویی شبان پیوسته است</p></div>
<div class="m2"><p>که زلف هرشبی اندرشب دگربسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از تمام علایق گسسته‌ام که مرا</p></div>
<div class="m2"><p>خیال ابروی او پیش چشم‌، پیوسته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه خنجر و نه کمانست ابروان کجش</p></div>
<div class="m2"><p>که در فضیلت رویش دو خط برجسته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشاط من ز خط سبز آن پسر باری</p></div>
<div class="m2"><p>چنان بود که فقیری زمردی جسته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سبز برگ خط البته آفتی نرسد</p></div>
<div class="m2"><p>به گلبنی که برو صدهزار گل رسته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دولت سر عشق تو زنده‌ام‌، ورنه</p></div>
<div class="m2"><p>هزار بار فزون مرگم از کمین جسته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مباش تند و مغاضب که نعمت دو جهان</p></div>
<div class="m2"><p>نتیجهٔ رخ خندان و طبع آهسته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز روی درد نگه کن به شعر من‌، کاین شعر</p></div>
<div class="m2"><p>تراوش دل خونین و خاطر خسته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ارادت ار طلبی معنویتی بنمای</p></div>
<div class="m2"><p>که از علایق صوری فقیر وارسته است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به سربلندی یاران نهاده گردن و باز</p></div>
<div class="m2"><p>به دستگیری ایشان ز پای ننشسته است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرفته یار ولی هیچ کام نگرفته</p></div>
<div class="m2"><p>شکسته توبه ولی هیچ عهد نشکسته است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگفته هیچ دروغ ارچه جای آن بوده</p></div>
<div class="m2"><p>نکرده هیچ بدی گرچه می‌توانسته است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>«‌بهار» گوی سعادت کسی ربوده به دهر</p></div>
<div class="m2"><p>که‌خواسته‌است‌و توانسته‌است‌و دانسته‌است</p></div></div>