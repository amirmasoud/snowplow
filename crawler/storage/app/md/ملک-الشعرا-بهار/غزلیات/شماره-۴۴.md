---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>آخر از جور تو عالم را خبر خواهیم کرد</p></div>
<div class="m2"><p>خلق را از طرّه‌ات آشفته‌تر خواهیم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او از عشق جهانسوزت مدد خواهیم خواست</p></div>
<div class="m2"><p>پس جهانی را ز شوقت پر شرر خواهیم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان اگر باید به کوی ات نقد جان خواهیم داد</p></div>
<div class="m2"><p>سر اگر باید به راهت ترک سر خواهیم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در غم عشق تو با این ناله‌های دردناک</p></div>
<div class="m2"><p>اختر بیدادگر را دادگر خواهیم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکسی کام دلی آورده درکویت به‌دست</p></div>
<div class="m2"><p>ماهم آخر در غمت خاکی به سر خواهیم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا جهانی درخور شرح غمت پیداکنیم</p></div>
<div class="m2"><p>خویش را زین عالم فانی ‌بدر خواهیم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاکه ننشیند به دامانت غبار از خاک ما</p></div>
<div class="m2"><p>روی گیتی را ز آب دیده تر خواهیم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا ز آه نیم شب‌، یا از دعا، یا از نگاه</p></div>
<div class="m2"><p>هرچه باشد در دل سختت اثر خواهیم کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لابه‌ها خواهیم کردن تا به ما رحم آوری</p></div>
<div class="m2"><p>ور به‌بی‌رحمی زدی فکر دگر خواهیم کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون‌ بهار از جان‌ شیرین‌ دست‌ برخواهیم‌ داشت</p></div>
<div class="m2"><p>پس سرکوی تو را پرشور و شر خواهیم کرد</p></div></div>