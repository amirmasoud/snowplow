---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>خوبرویان یار را در عین یاری می‌کشند</p></div>
<div class="m2"><p>دوستداران را به جرم دوستداری می‌کشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ وحشی چون نمی‌افتد به دست کودکان</p></div>
<div class="m2"><p>مرغ دست‌آموز را با زجر و خواری می‌کشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهدان دیر جوش از دوستان باوفا</p></div>
<div class="m2"><p>زود سیر آیند و ایشان را به زاری می‌کشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوستان خاص را مانند مرغ خانگی</p></div>
<div class="m2"><p>در عروسی و عزا بر رسم جاری می‌کشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر شبانان فی‌المثل گوسالهٔ پا بسته را</p></div>
<div class="m2"><p>در قبال جستن گاو فراری می‌کشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا مگر از کید بدخواهان دمی ایمن شوند</p></div>
<div class="m2"><p>نیکخواهان را ز فرط خام کاری می‌کشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر قربان بر سر راه حسودان دورو</p></div>
<div class="m2"><p>غمگساران را به جای غمسگاری می‌کشند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون وزیر و پیل و رخ از کار افتادند و شاه</p></div>
<div class="m2"><p>ماند بی‌اصحاب با یک زخم کاری می‌کشند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تجربت‌ها کرده‌ایم از کار دولت‌ها «‌بهار»</p></div>
<div class="m2"><p>گر نکشتی اختیاری‌، اضطراری می‌کشند</p></div></div>