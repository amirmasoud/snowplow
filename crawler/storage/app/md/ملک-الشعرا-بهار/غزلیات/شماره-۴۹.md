---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>سر آزادهٔ ما منت افسر نکشد</p></div>
<div class="m2"><p>تن وارستهٔ ما حسرت زیور نکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما فقیران تهی‌دست ز خود بیخبریم</p></div>
<div class="m2"><p>جز سوی حق دل ما جانب دیگر نکشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما گداییم ولی قصر غنا منزل ماست</p></div>
<div class="m2"><p>هرکه شد همدم ما منت‌قیصرنکشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خضر ماییم که خاک ره ما آب بقاست</p></div>
<div class="m2"><p>هرکه شد همره ما ناز سکندر نکشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تاکه ما راست سر رشتهٔ تسلیم به‌دست</p></div>
<div class="m2"><p>بادپای فلک از رشتهٔ ما سر نکشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پدر دهر چو در مهد صفا بیند طفل</p></div>
<div class="m2"><p>ناز او را کشد آن‌گونه که مادر نکشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشتابید سوی حق که نگردد منعم</p></div>
<div class="m2"><p>تا گدا رخت به درگاه توانگر نکشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی کند سیر گلستان صفا، ابراهیم</p></div>
<div class="m2"><p>تا ز تسلیم و رضا رخت در آذر نکشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دلی را نبود تاب غم عشق «‌بهار»</p></div>
<div class="m2"><p>تا دلاور نبود بار دلاور نکشد</p></div></div>