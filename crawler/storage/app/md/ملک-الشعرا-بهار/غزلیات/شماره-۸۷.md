---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>غمزه‌ات خونریزتر یا دیدهٔ خونبار من</p></div>
<div class="m2"><p>طره‌ات آشفته‌تر یا خاطر افکار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعل جانان سرخ‌تر یا لاله یا می یا عقیق</p></div>
<div class="m2"><p>مه نکوتر یا پری یا حور یا دلدار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کام عاشق تلخ‌تر یا صبر یا گفتار تو</p></div>
<div class="m2"><p>وصل دلبر خوبتر یا عشق یا کردار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طالع من تیره‌تر یا زلف تو یا شام هجر</p></div>
<div class="m2"><p>وصل تو دشوارتر یا کام دل یا کار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر تو موهوم‌تر یا نقطه یا سیمرغ و قاف</p></div>
<div class="m2"><p>کیمیا پوشیده‌تر یا صدق یا آثار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سنگ آهن سخت‌تر یا آن دل بی‌مهر تو</p></div>
<div class="m2"><p>نرگس تو خسته‌تر یا این دل بیمار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پشت من خمیده‌تر یا حلقه‌های زلف تو</p></div>
<div class="m2"><p>عشوهٔ تو بیشتر یا ناله‌های زار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر و مه تابنده‌تر یا چهر تو یا صبح وصل</p></div>
<div class="m2"><p>شام هجران تیره‌تر یا بخت کجرفتار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشتری فرخنده‌تر یا روی تو یا بخت شاه</p></div>
<div class="m2"><p>قامت تو راست‌تر یا سرو یا گفتار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لطف شه سازنده تر یا لعل روح ‌افزای دوست</p></div>
<div class="m2"><p>خشم شه سوزنده‌تر یا آه آتشبار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در غزل‌سازی بهار استادتر یا آن که گفت</p></div>
<div class="m2"><p>«‌روزگار آشفته‌تر یا زلف تو یا کار من‌»</p></div></div>