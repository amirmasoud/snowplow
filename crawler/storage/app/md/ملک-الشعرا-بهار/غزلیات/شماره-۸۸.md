---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>لاله خونین کفن از خاک سر آورده برون</p></div>
<div class="m2"><p>خاک مستوره فلب بشر آورده برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست این لالهٔ نوخیز، که از سینه خاک</p></div>
<div class="m2"><p>پنجه جنگ جهانی جگر آورده برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رمزی از نقش قتالست که نقاش سپهر</p></div>
<div class="m2"><p>بر سر خامه ز دود و شرر آورده برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاکه در صحنهٔ گیتی ز نشان‌های حریق</p></div>
<div class="m2"><p>ذوق صنعت اثری مختصر آورده برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منکسف ماه و براو هالهٔ خونبار محیط</p></div>
<div class="m2"><p>طرحی از فتنه دور فمر آورده برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ماتمزدهٔ مادر زاریست که مرگ</p></div>
<div class="m2"><p>از زمین همره داغ پسر آورده برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعلهٔ واقعه گوییست که از روی تلال‌</p></div>
<div class="m2"><p>دست مخبر به نشان خبر آورده برون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست خونین زمین است که از بهر دعا</p></div>
<div class="m2"><p>صلح‌جویانه زکوه وکمر آورده برون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آتشین آه فرو مردهٔ مدفون شده است</p></div>
<div class="m2"><p>که زمین از دل خود شعله‌ور آورده برون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پاره‌های کفن و سوخته‌های جگرست</p></div>
<div class="m2"><p>کزپی عبرت اهل نظر آورده برون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق‌ مدفون‌ شده‌ و آرزوی خاک‌ شده‌است</p></div>
<div class="m2"><p>کش زمین بیخته در یکدگر آورده برون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پاره‌ها زآهن سرخست که در خاور دور</p></div>
<div class="m2"><p>رفته در خاک و سر از باختر آورده برون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس که‌خون‌درشکم‌خاک‌فشرده‌است بهم</p></div>
<div class="m2"><p>لخت لختش ز مسامات سر آورده برون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>راست گو که زبان‌های وطن خواهانست</p></div>
<div class="m2"><p>که جفای فلک از پشت سرآورده برون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یا ظفرنامچهٔ لشگر سرخست که دهر</p></div>
<div class="m2"><p>بر سر نیزه به یاد ظفر آورده برون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا به تقلید شهیدان ره آزادی</p></div>
<div class="m2"><p>طوطی سبز قبا سرخ پر آورده برون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یاکه بر لوح وطن خامهٔ خونبار بهار</p></div>
<div class="m2"><p>نقشی از خون دل رنجبر آورده برون</p></div></div>