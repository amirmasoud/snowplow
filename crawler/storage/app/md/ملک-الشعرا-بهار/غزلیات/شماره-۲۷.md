---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>به کشوری که در آن ذره‌ای معارف نیست</p></div>
<div class="m2"><p>اگرکه مرگ بباردکسی مخالف نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگو به مجلس شورا چرا معارف را</p></div>
<div class="m2"><p>هنوز منزلت کمترین مصارف نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وکیل بی‌هنر از موش مرده می‌ترسد</p></div>
<div class="m2"><p>ولی ز مردن ابناء نوع خائف نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند قبیلهٔ دیگر حقوق او پامال</p></div>
<div class="m2"><p>هرآن قبیله که بر حق خویش واقف نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشاط محفل ناهید و نغمهٔ داود</p></div>
<div class="m2"><p>تمام‌یکسره جمع است حیف «‌عارف‌» نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>«‌بهار» عاطفه از ناکسان مدار طمع</p></div>
<div class="m2"><p>که در قلوب کسان ذره‌ای عواطف نیست</p></div></div>