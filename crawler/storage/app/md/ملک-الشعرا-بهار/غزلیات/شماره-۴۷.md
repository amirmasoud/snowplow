---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>مشتاقی و صبوری با هم قرین نباشد</p></div>
<div class="m2"><p>این باشد آن نباشد آن باشد این نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با انگبین لبت را سنجیده‌ام مکرر</p></div>
<div class="m2"><p>شهدی که در لب تست در انگبین نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قومی به فکر مشغول قومی بدین گرفتار</p></div>
<div class="m2"><p>غافل که آنچه‌جویند درکفر و دین نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در نکتهٔ دهانت هرکس کند گمانی</p></div>
<div class="m2"><p>تا تو سخن نگویی کس را یقین نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماه فلک ز حسنت خواهد برد نصیبی</p></div>
<div class="m2"><p>ورنه همیشه سیرش گرد زمین نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهم سایم سر ارادت بر آستانت</p></div>
<div class="m2"><p>شرمنده‌ام که چیزیم در آستین نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یابد ز دام زلفش صید دلم رهایی</p></div>
<div class="m2"><p>گر چشم صیدگیرش اندرکمین نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با ترکتاز چشمش نیکو مقاومت کرد</p></div>
<div class="m2"><p>حقاکه چون دل من حصنی حصین نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم بهار مسکین خواهدگلی ز باغت</p></div>
<div class="m2"><p>گفتا خزان رسیده است گل بعد ازین نباشد</p></div></div>