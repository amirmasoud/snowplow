---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>شمعیم و دلی مشعله‌افروز و دگر هیچ</p></div>
<div class="m2"><p>شب تا به سحر گریهٔ جانسوز و دگر هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسانه بود معنی دیدار که دادند</p></div>
<div class="m2"><p>در پرده یکی وعدهٔ مرموز و دگر هیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاجی که خدا را به حرم جست چه باشد</p></div>
<div class="m2"><p>از پارهٔ سنگی شرف‌اندوز و دگر هیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهی که شوی باخبر ازکشف و کرامات</p></div>
<div class="m2"><p>مردانگی و عشق بیاموز و دگر هیچ‌</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی که دلی را به نگاهی بنوازند</p></div>
<div class="m2"><p>از عمر حسابست همان روز و دگر هیچ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین قوم چه خواهی که بهین پیشه‌ورانش</p></div>
<div class="m2"><p>گهواره تراشند و کفن‌دوز و دگر هیچ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین مدرسه هرگز مطلب علم که اینجاست</p></div>
<div class="m2"><p>لوحی سیه و چند بدآموز و دگر هیچ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهد بَدَل عمر، بهار از همه گیتی</p></div>
<div class="m2"><p>دیدار رخ یار دل‌افروز و دگر هیچ</p></div></div>