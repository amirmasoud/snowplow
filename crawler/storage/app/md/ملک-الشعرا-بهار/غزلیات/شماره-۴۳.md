---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>ای دل به صبر کوش که هر چیز بگذرد</p></div>
<div class="m2"><p>زبن حبس هم مرنج که این نیزبگذرد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرهاد گو به تلخی غم صبر کن که زود</p></div>
<div class="m2"><p>شیرینی تعیش پرویز بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوران رادمردی و آزادگی گذشت</p></div>
<div class="m2"><p>وین دورهٔ سیاه بلاخیز بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردانه پایدار بر احداث روزگار</p></div>
<div class="m2"><p>کاین روزگار زن‌صفت حیز بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما و تو نیستیم و به خاک مزار ما</p></div>
<div class="m2"><p>بسیار این نسیم فرح‌بیز بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این است پند من که ز خوب و بد جهان</p></div>
<div class="m2"><p>نه غره شو، نه رنجه که هر چیز بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبح نشاط خندد و آید «‌بهار» عیش</p></div>
<div class="m2"><p>وین شام شوم و عصر غم‌انگیز بگذرد</p></div></div>