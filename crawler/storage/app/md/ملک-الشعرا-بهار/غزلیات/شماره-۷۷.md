---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>منم که خط غلامی دهم به نیم ‌سلام</p></div>
<div class="m2"><p>دل من است که قانع شود به یک پیغام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنون که گردش ایام را ثباتی نیست</p></div>
<div class="m2"><p>همان‌ خوش است که در عشق بگذرد ایام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من آن مقام بلند از کجا به دست آرم</p></div>
<div class="m2"><p>که عاشقانه بیایم در آن بلند مقام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من آن نی‌ام که هلال از تمام نشناسم</p></div>
<div class="m2"><p>مه‌ دو هفته هلال‌ است و عارض تو تمام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چراغ وصل بیفروز و حجره روشن کن</p></div>
<div class="m2"><p>که آفتاب جدایی رسیده بر لب بام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمم بکشت که خوبان چرا ندانستند</p></div>
<div class="m2"><p>که خدعه‌باز کدامست و عشق‌باز کدام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نام عشق که از عشق رخ نخواهم تافت</p></div>
<div class="m2"><p>اگر دچار ملامت شوم و گر بدنام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهار باشد و بس آن که در ارادت دوست</p></div>
<div class="m2"><p>کشیده طعنهٔ کفر و ملامت اسلام</p></div></div>