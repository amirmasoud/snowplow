---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>گهی با دزد افتد کار و گاهی با عسس ما را</p></div>
<div class="m2"><p>نشد کاین آسمان راحت گذارد یک ‌نفس ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عسس با دزد شد دمساز و ما با هر دو بیگانه</p></div>
<div class="m2"><p>به ‌شب ‌از دزد باشد وحشت ‌و روز از عسس‌ ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفتار جفای ناکسان گشتیم در عالم</p></div>
<div class="m2"><p>دربغا زندگانی طی شد و نشناخت کس ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بس ماندیم درگنج قفس‌، گر باغبان روزی</p></div>
<div class="m2"><p>کند ما را رها، ره نیست جز کنج قفس ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان کاروان عافیت پیدا نشد لیکن</p></div>
<div class="m2"><p>به کوه و دشت کرد آواره آوای جرس ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دست دل گریبان پاره کردیم از غمت شاید</p></div>
<div class="m2"><p>سوی دل باشد از چاک گریبان دسترس ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین تاریکی حیرت‌، به دل از عشق برقی زد</p></div>
<div class="m2"><p>مگر تا وادی ایمن کشاند این قبس ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بریدیم از شهنشاهان طمع در عین درویشی</p></div>
<div class="m2"><p>که از خوبان نباشد جز نگاهی ملتمس ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر خواهی که با صاحبدلان طرح وفا ریزی</p></div>
<div class="m2"><p>کنون درنه قدم‌، زبرا نبینی زین سپس ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خداوندی و سلطانی به یاران باد ارزانی</p></div>
<div class="m2"><p>درین بیدای ظلمانی فروغ عشق بس ما را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هوس بستیم تا ترک هوس گوییم در عالم</p></div>
<div class="m2"><p>بهار آخر به جایی می‌رساند این هوس ما را</p></div></div>