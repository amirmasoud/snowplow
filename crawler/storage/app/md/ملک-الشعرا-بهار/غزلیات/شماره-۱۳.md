---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>غم طوقی از آهن شد و برگردنم آویخت</p></div>
<div class="m2"><p>چون ژندهٔ درویش، بلا در تنم آویخت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درگردن دلدار نیاویخته‌، دستم</p></div>
<div class="m2"><p>بشکست به صد خواری و در گردنم آویخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن طفل که پروردهٔ دل بود چو اغیار</p></div>
<div class="m2"><p>افتاد ز چشم من و در دامنم آویخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدگوبی جهال به بوم و برم آشفت</p></div>
<div class="m2"><p>بیغارهٔ حساد به پیرامنم آویخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببرید طبیعت ز هواهای دلم سر</p></div>
<div class="m2"><p>وآورد ویکایک به سر برزنم آوبخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبل‌صفت آفات سخن گفتن شیرین</p></div>
<div class="m2"><p>در خانه و در لانه و درگلشنم آویخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون منطق شیرین مرا دید زمانه</p></div>
<div class="m2"><p>از طاق فلک در قفس آهنم آویخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگداخت تنم شمع‌صفت وین دل سوزان</p></div>
<div class="m2"><p>چون شعلهٔ فانوس به پیراهنم آوبخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چیزکزان بیش دلم داشت تنفر</p></div>
<div class="m2"><p>چون پردهٔ تاری به در روزنم آوبخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تاربکی افکار حریفان چو حجابی</p></div>
<div class="m2"><p>گرد آمد و درییش دل روشنم آوبخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حلاج‌ صفت‌، تا ز چه گفتم سخن حق</p></div>
<div class="m2"><p>از دار بلا این فلک ریمنم آویخت</p></div></div>