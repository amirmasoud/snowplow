---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>لب لعل تو می‌ فروشی کرد</p></div>
<div class="m2"><p>چشم مست تو باده‌نوشی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این خطاها چو دید حاجب حسن</p></div>
<div class="m2"><p>زان خط سبز پرده‌پوشی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه پراکنده گفت زلف‌، که دوش</p></div>
<div class="m2"><p>خم شد و با تو سر به گوشی کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راز دل با لبت نگفته‌، خطت</p></div>
<div class="m2"><p>سر برآورد وتیزهوشی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاقبت سست گردد اندر هجر</p></div>
<div class="m2"><p>هرکه با عشق سخت کوشی کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خار، هر سرزنش که کرد، بهار</p></div>
<div class="m2"><p>غنچهٔ تنگ‌دل خموشی کرد</p></div></div>