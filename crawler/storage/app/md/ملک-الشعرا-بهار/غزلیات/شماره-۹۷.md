---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>نهاده کشور دل باز رو به ویرانی</p></div>
<div class="m2"><p>که دیده مملکتی را بدین پریشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلا مکن گله از کس که خوار و زار شود</p></div>
<div class="m2"><p>هر آن که‌ شد چو تو سرگشته در هوسرانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تار زلف سیاه تو روز مشتاقان</p></div>
<div class="m2"><p>بود سیاه‌تر از روزگار ایرانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پاس هستی ایرانیان برآور سر</p></div>
<div class="m2"><p>ز خاک نیستی‌، ای اردشیر ساسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببین به کشور ایران و حال تیرهٔ او</p></div>
<div class="m2"><p>که پست و خوار و زبون باد جهل و نادانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهار بندهٔ حق باش و پادشاهی کن</p></div>
<div class="m2"><p>که بندگان حقیقت کنند سلطانی</p></div></div>