---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>باد بر آن دو سر طرهٔ شبرنگ افتاد</p></div>
<div class="m2"><p>حذر ای دل که میان دو سپه جنگ افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط بر آن روی نکو دست تطاول بگشود</p></div>
<div class="m2"><p>آه و صد آه که آن آینه را زنگ افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون دل شد عوض باده به کام من مست</p></div>
<div class="m2"><p>بس که در ساغرم از بام فلک سنگ افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم آید اثری در دلش از ناله و آه</p></div>
<div class="m2"><p>وه که آه از اثر و ناله ز آهنگ افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش آن قد خرامنده و آن عارض پاک</p></div>
<div class="m2"><p>گل و سرو و چمن از جلوه و از رنگ افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داغ هجر است که بینی به دل لاله رسید</p></div>
<div class="m2"><p>شور عشق است که بینی به سر چنگ افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته ی حافظ و سعدی نکند گوش‌ ، بهار</p></div>
<div class="m2"><p>هرکه را نسخه‌ای از شعر تو در چنگ افتاد</p></div></div>