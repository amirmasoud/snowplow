---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>تو اگر خامی و ما سوخته‌، توفیر بسی است</p></div>
<div class="m2"><p>شعلهٔ عشق نه گیرندهٔ هر خاروخسی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر طبیبی نکند چارهٔ این مرده‌دلان</p></div>
<div class="m2"><p>که دوای دل ما درکف عیسی‌نفسی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دل سوخته ره برد به جایی نه عجب</p></div>
<div class="m2"><p>سوی حق راهبر موسی عمران‌، قبسی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاروانی است پراکنده و سرگشته ولیک</p></div>
<div class="m2"><p>خاطر گمشدگان شاد به بانگ جرسی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طفل راگوشهٔ گهواره جهانی است فراخ</p></div>
<div class="m2"><p>همه آفاق بر همت مردان قفسی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای توانگر تو به زر شادی و دانا به ضمیر</p></div>
<div class="m2"><p>هر کسی را به جهان گذران ملتمسی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهر ما با عسس و محتسب از دزد پر است</p></div>
<div class="m2"><p>ای‌خوش آن‌ شهر که‌ در باطن‌ هر کس عسسی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سال‌ها حلقه زدم بر در این خانه «‌بهار»</p></div>
<div class="m2"><p>بود ظنم به همه عمر که در خانه کسی است</p></div></div>