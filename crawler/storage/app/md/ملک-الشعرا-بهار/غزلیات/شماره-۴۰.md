---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>ملک جهان چون سویس باغ ندارد</p></div>
<div class="m2"><p>لالهٔ باغ سویس داغ ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز دل ایرانیان خسته درین ملک</p></div>
<div class="m2"><p>یک دل غمگین کسی سراغ ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست نشاطند خلق و جز من بیمار</p></div>
<div class="m2"><p>کیست که دایم به کف ایاغ ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک دل افسرده در تمام ژنو نیست</p></div>
<div class="m2"><p>یک گل پژمرده هیچ باغ ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وادی بی‌آب و سنگلاخ نیابی</p></div>
<div class="m2"><p>غیر گلستان و باغ و راغ ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهر و ده اینجاست غرق نور ولیکن</p></div>
<div class="m2"><p>مرکز ایران به شب چراغ ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل گویا به باغ گرم سرود است</p></div>
<div class="m2"><p>لاشخور و کرکس و کلاغ ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشق آزرده از رقیب نباشد</p></div>
<div class="m2"><p>بلبلش آشفتگی ز زاغ ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از غم ایران دلم گرفته به‌نوعی</p></div>
<div class="m2"><p>کز پی درمان خود فراغ ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جای غزل گفتن بهار همین‌جاست</p></div>
<div class="m2"><p>حیف که مسکین ملک دماغ ندارد</p></div></div>