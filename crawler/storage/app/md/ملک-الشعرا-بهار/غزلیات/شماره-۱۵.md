---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>همین نه ازستم چرخ شهرآمل سوخت</p></div>
<div class="m2"><p>که‌ازعطش به‌ری امسال سبزه وگل سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجای شمع برافروخت در چمن گل سرخ</p></div>
<div class="m2"><p>بجای شهپر پروانه بال بلبل سوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به باغ‌، بید معلق زتشنگی چون شمع</p></div>
<div class="m2"><p>گرفت لرزه و ازپای تا به کاکل سوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو ای سحاب کرم قطره‌ای فشان بر خاک</p></div>
<div class="m2"><p>که چهرلاله سیه گشت وزلف سنبل سوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حال خلق تغافل بس است ای وزرا</p></div>
<div class="m2"><p>که خانمان ضعیفان ازین تغافل سوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کار ملک تعلل بس است ای امرا</p></div>
<div class="m2"><p>که شهر دلکش آمل ازبن تعلل سوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به داغ هیچ عزیزی خدا نسوزاند</p></div>
<div class="m2"><p>هرآن دلی که بر احوال شهرآمل‌سوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهارگفت توکل به حق کنید دربغ</p></div>
<div class="m2"><p>که‌برق غفلت‌ماخرمن‌توکل‌سوخت</p></div></div>