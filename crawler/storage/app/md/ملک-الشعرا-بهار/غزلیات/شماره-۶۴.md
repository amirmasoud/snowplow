---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>از ما به جز از وفا نیاید</p></div>
<div class="m2"><p>وز یار به جز جفا نیاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبر چه بلا بود که هرگز</p></div>
<div class="m2"><p>نزد من مبتلا نیاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حرزی است مرا نهان کزان حرز</p></div>
<div class="m2"><p>در خانهٔ ما بلا نیاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من کوه غم توام ولیکن</p></div>
<div class="m2"><p>زین کوه دگر صدا نیاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خانهٔ ما نیایی آری</p></div>
<div class="m2"><p>منعم بر بینوا نیاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شادان‌، خبر غمی نپرسد</p></div>
<div class="m2"><p>سلطان به سر گدا نیاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و آن را که قدم به فرش دیباست</p></div>
<div class="m2"><p>در خانه ی بوربا نیاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر ز خدا بترس اگر هیچ</p></div>
<div class="m2"><p>از روی منت حیا نیاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوبی که ز عشق دست بردار</p></div>
<div class="m2"><p>این کار ز دست ما نیاید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من زلف تو مشک چین نخوانم</p></div>
<div class="m2"><p>کز اهل ادب خطا نیاید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر ما قلبت چرا نسوزد؟</p></div>
<div class="m2"><p>بر ما رحمت چرا نیاید؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیگانه بود «‌بهار» آنجا</p></div>
<div class="m2"><p>کاوازهٔ آشنا نیاید</p></div></div>