---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>گل مقصود نچید آن که چو من خوار نشد</p></div>
<div class="m2"><p>نشد آزاد ز غم هر که گرفتار نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسف مصر نشد آن که به بازار وجود</p></div>
<div class="m2"><p>پیره زالی به کلافیش خریدار نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همره نوح نشد، همسر داود نگشت</p></div>
<div class="m2"><p>هرکه خدمتگر آهنگر و نجار نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رهش پای مکش دامنش از دست منه</p></div>
<div class="m2"><p>فکر یکبار دگر کن اگر این بار نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صنما پرده ز رخ برکش و بر قلب فکن</p></div>
<div class="m2"><p>که حجاب رخ زن حافظ اسرار نشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چهره بگشای و ز چشم بد اغیار مترس</p></div>
<div class="m2"><p>که گل آزرده دل از چشم بد خار نشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پس پردهٔ ناموس نهان شو زیرا</p></div>
<div class="m2"><p>چادر و پیچه حجاب زن بدکار نشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زن که با حسن خداداده نیاموخت هنر</p></div>
<div class="m2"><p>لایق همسری مردم هشیار نشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیو پتیاره بود گرچه بود نیکوروی</p></div>
<div class="m2"><p>زن که با نامزد خویش وفادار نشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عفت دختر دوشیزه نهالی است بهار</p></div>
<div class="m2"><p>که چو شدکنده ز جا سبز دگربار نشد</p></div></div>