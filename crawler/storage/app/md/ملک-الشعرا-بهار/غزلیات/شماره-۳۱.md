---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>در مسیل مسکنت خفتیم و چندی برگذشت</p></div>
<div class="m2"><p>سر ز جا برداشتیم اکنون که‌آب از سرگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ بر سر خورده فرهادا برآور سر ز خواب</p></div>
<div class="m2"><p>کافتاب از تیغ کوه بیستون اندرگذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اهرمن ملک سلیمان پیمبر غصب کرد</p></div>
<div class="m2"><p>دیو بر بنگاه کیکاوس نام‌آورگذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش این روز سیه‌، گشتند بالله روسفید</p></div>
<div class="m2"><p>روزهایی کز سیه‌بختی برین کشور گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست بالله سهل وآسان پیش دزد خانگی</p></div>
<div class="m2"><p>زحمت دزدی که از بام آمد و از در گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تازه گشت از فرقهٔ قزاق در دوران ما</p></div>
<div class="m2"><p>آنچه از خیل غزان در دورهٔ سنجرگذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دهان اهل دانش فرقهٔ غز خاک ریخت</p></div>
<div class="m2"><p>وای خاکم بر دهان برما ازآن بدترگذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ نگذشت از ستم بر ما ز چنگیز مغول</p></div>
<div class="m2"><p>کز رضاخان ستم کار ستم گستر گذشت</p></div></div>