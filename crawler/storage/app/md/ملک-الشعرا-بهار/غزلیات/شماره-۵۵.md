---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>آن خط سبز بین که چه زیبا نوشته‌اند</p></div>
<div class="m2"><p>گویی خط از عبیر به دیبا نوشته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در معنی لب تو ز شنگرف نقطه‌ای</p></div>
<div class="m2"><p>برگل نهاده شرح به بالا نوشته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا نسختی ز مهر گیا ثبت کرده‌اند</p></div>
<div class="m2"><p>یا سر خطی بحون دل ما نوشته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا با خط غبار به گرد عقیق تر</p></div>
<div class="m2"><p>رمزی ز زنده کردن موتی نوشته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرحی ز نوشداروی کاوس داده‌اند</p></div>
<div class="m2"><p>رازی ز معجزات مسیحا نوشته‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیات حسن مطلق و اسرار عشق پاک</p></div>
<div class="m2"><p>با لاجورد بر گل رعنا نوشته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز عشق‌، صانعی نبود در جهان «‌بهار»</p></div>
<div class="m2"><p>بیهوده گفته‌اند جز این یا نوشته‌اند</p></div></div>