---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>از دوست بریدیم به صد رنج و ندامت</p></div>
<div class="m2"><p>از دوست به‌خیر آمد و از ما به‌سلامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حالی دل مظلوم مرا غمزهٔ مستش</p></div>
<div class="m2"><p>با تیر زد و ماند قصاصش به قیامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عشق حذرکن که بود ماحصل عشق</p></div>
<div class="m2"><p>خون خوردن و جان کندن و آنگاه ملامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طی شد زجهان چشمه خضر ودم عیسی</p></div>
<div class="m2"><p>ایزد به لب لعل تو داد این دو کرامت</p></div></div>