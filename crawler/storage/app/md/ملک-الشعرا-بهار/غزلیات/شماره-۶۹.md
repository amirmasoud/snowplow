---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>نرگس غمزه‌زنش بر سر ناز است هنوز</p></div>
<div class="m2"><p>طرهٔ پرشکنش سلسله‌باز است هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقان را سپه ناز براند از در دوست</p></div>
<div class="m2"><p>بر در دوست مرا روی نیاز است هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک محمود شد از دست حوادث بر باد</p></div>
<div class="m2"><p>در دلش آتش سودای ایاز است هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کسی را سر کوی صنمی شد مقصود</p></div>
<div class="m2"><p>مقصد ساده‌دلان خاک حجاز است هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه شد عمر من از خط توکوتاه ولی</p></div>
<div class="m2"><p>دست امّید به زلف تو دراز است هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسجد حسن تو از خط شده ویران لیکن</p></div>
<div class="m2"><p>طاق ابروی تو محراب نماز است هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزی ای گل به چمن چشم گشودی از ناز</p></div>
<div class="m2"><p>چشم نرگس به تماشای تو باز است هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین تحسرکه چرا سوخت پرپروانه</p></div>
<div class="m2"><p>شمع دلسوخته در سوز وگداز است هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز شد شهپر مرغان گرفتار بهار</p></div>
<div class="m2"><p>بستگی‌هاست که در دیده ی باز است هنوز</p></div></div>