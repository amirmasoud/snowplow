---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>سیل خون‌آلود اشکم بی‌خبر گیرد تو را</p></div>
<div class="m2"><p>خون مردم‌، آخر ای بیدادگر، گیرد تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شکرلب‌، آب چشمم نیک دریابد تو را</p></div>
<div class="m2"><p>وی قصب‌پوش آتش دل زود درگیرد تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور گریزی زین دو طوفان چون پری بر آسمان</p></div>
<div class="m2"><p>بر فراز آسمان آه سحر گیرد تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باخبر کردم تو را خون ضعیفان را مریز</p></div>
<div class="m2"><p>زان که خون بی‌گناهان بی‌خبر گیرد تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفرت مردم به مانند سگ درنده است</p></div>
<div class="m2"><p>گر تو از پیشش گریزی زودتر گیرد تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کن حذر زان دم که دست عاشق دلمرده‌ای</p></div>
<div class="m2"><p>همچو قاتل در میان رهگذر گیرد تو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خدنگ غمزهٔ جانان ز تنهایی منال</p></div>
<div class="m2"><p>مرغ دل چون جوجه زیر بال و پر گیرد تو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک زیر و رو ندارد پیش عزم عاشقان</p></div>
<div class="m2"><p>هر کجا باشد بهار آخر به بر گیرد تو را</p></div></div>