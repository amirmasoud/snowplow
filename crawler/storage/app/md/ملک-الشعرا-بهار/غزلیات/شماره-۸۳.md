---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>منم که عشق بتانم نموده پیر و کهن</p></div>
<div class="m2"><p>ندانم اینکه چه افتاده عشق را با من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلی هر آنکو عشق بتانش چیره شود</p></div>
<div class="m2"><p>شگفت نیست گر آید نزار و پیر و کهن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رنج و درد چنان شد تنم که گر بینی</p></div>
<div class="m2"><p>گمان بری که سرشته ز رنج و دردم تن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا ز عشق که بر اهرمن نصیب مباد</p></div>
<div class="m2"><p>سیه‌ تر آمده گیتی ز جان اهریمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو تفته آهن‌، دل در برم از آن بگداخت</p></div>
<div class="m2"><p>که یار را به‌ بر اندر دلی است چون آهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنم بکاست از آنگه که عشق ورزبدم</p></div>
<div class="m2"><p>بلی بکاهد مردم ز عشق ورزبدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازآن زمان که مرا عشق زد به دامان دست</p></div>
<div class="m2"><p>همی فشانم خون از دو دیده بر دامن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم بیفسرد از جور یار و درد فراق</p></div>
<div class="m2"><p>تنم بفرسود از گردش دی و بهمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن ز عشق به کس گرچه می‌نگویم لیک</p></div>
<div class="m2"><p>شرار عشق پدید آیدم ز سوز سخن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هوای یارم آمیخته است با رگ و پوست</p></div>
<div class="m2"><p>چو رنگ و بوی نهفته به لاله و لادن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا رسید ز عشق آنچه ز آتش اندر عود</p></div>
<div class="m2"><p>مرا مبین‌، که همه اوست اینکه بینی من</p></div></div>