---
title: >-
    الحمدلله
---
# الحمدلله

<div class="b" id="bn1"><div class="m1"><p>می ده که طی شد دوران جانکاه</p></div>
<div class="m2"><p>آسوده شد ملک‌، الملک‌لله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد شاه نو را اقبال همراه</p></div>
<div class="m2"><p>کوس شهی کوفت بر رغم بدخواه</p></div></div>
<div class="b2" id="bn3"><p>شد صبح طالع‌، طی شد شبانگاه</p>
<p>الحمد لله‌، الحمد لله</p></div>
<div class="b" id="bn4"><div class="m1"><p>یک چند ما را غم رهنمون شد</p></div>
<div class="m2"><p>جان یارغم گشت‌،‌دل غرق‌خون شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مام وطن را رخ نیلگون شد</p></div>
<div class="m2"><p>و امروز دشمن خوار و زبون شد</p></div></div>
<div class="b2" id="bn6"><p>زبن جنبش‌ سخت‌، زبن فتح ناگاه</p>
<p>الحمد لله‌، الحمد لله</p></div>
<div class="b" id="bn7"><div class="m1"><p>چندی ز بیداد فرسوده گشتیم</p></div>
<div class="m2"><p>با خاک و با خون آلوده گشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر پی خصم پیموده گشتیم</p></div>
<div class="m2"><p>و امروز دیگر آسوده گشتیم</p></div></div>
<div class="b2" id="bn9"><p>از ظلم ظالم‌، از کید بدخواه</p>
<p>الحمد لله‌، الحمد لله</p></div>
<div class="b" id="bn10"><div class="m1"><p>آنان که ما را کشتند و بستند</p></div>
<div class="m2"><p>قلب وطن را از کینه خستند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از کج نهادی پیمان شکستند</p></div>
<div class="m2"><p>از چنگ ملت آخر نجستند</p></div></div>
<div class="b2" id="bn12"><p>از حضرت‌ شیخ تا حضرت‌ شاه</p>
<p>الحمد لله‌، الحمد لله</p></div>
<div class="b" id="bn13"><div class="m1"><p>آنان که با جور منسوب گشتند</p></div>
<div class="m2"><p>در پیکر ملک میکروب گشتند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آخر به ملت مغضوب گشتند</p></div>
<div class="m2"><p>از ساحت ملک جاروب گشتند</p></div></div>
<div class="b2" id="bn15"><p>پیران جاهل‌، شیخان گمراه</p>
<p>الحمد لله‌، الحمد لله</p></div>
<div class="b" id="bn16"><div class="m1"><p>چون کدخدا دید جور شبان را</p></div>
<div class="m2"><p>از جا برانگیخت ستارخان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سدّ ستم ساخت آن مرزبان را</p></div>
<div class="m2"><p>تاکرد رنگین تیغ وسنان را</p></div></div>
<div class="b2" id="bn18"><p>از خون دشمن وز مغز بدخواه</p>
<p>الحمد لله‌، الحمد لله</p></div>
<div class="b" id="bn19"><div class="m1"><p>پس مستبدین لختی جهیدند</p></div>
<div class="m2"><p>گفتند لختی‌، لختی شنیدند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ناگه ز هر سو شیران رسیدند</p></div>
<div class="m2"><p>آن روبهان باز دم درکشیدند</p></div></div>
<div class="b2" id="bn21"><p>شد طعمهٔ شیر بیچاره روباه</p>
<p>الحمد لله‌، الحمد لله</p></div>
<div class="b" id="bn22"><div class="m1"><p>یک سو سپهدار شد فتنه را سد</p></div>
<div class="m2"><p>یک سو یورش برد سردار اسعد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ضرغام پر دل‌، آمد ز یک حد</p></div>
<div class="m2"><p>برکف گرفتند تیغ مهند</p></div></div>
<div class="b2" id="bn24"><p>بستند بر خصم از هر طرف راه</p>
<p>الحمد لله‌، الحمد لله</p></div>
<div class="b" id="bn25"><div class="m1"><p>اقبال شد یار با بختیاری</p></div>
<div class="m2"><p>گیلانیان را حق کرد یاری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جیش عدو شد یکسر فراری</p></div>
<div class="m2"><p>درگنج غم گشت دشمن حصاری</p></div></div>
<div class="b2" id="bn27"><p>شد کار ملت بر طرز دلخواه</p>
<p>الحمد لله‌، الحمد لله</p></div>
<div class="b" id="bn28"><div class="m1"><p>بد خواه دین را سدی متین بود</p></div>
<div class="m2"><p>لیکن مر او را غم درکمین بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خاکش به سر شد پاداشش این بود</p></div>
<div class="m2"><p>دشمن که با عیش دایم قرین بود</p></div></div>
<div class="b2" id="bn30"><p>اکنون قرین است با ناله و آه</p>
<p>الحمد لله‌، الحمد لله</p></div>
<div class="b" id="bn31"><div class="m1"><p>بخت سپهدار فرخنده بادا</p></div>
<div class="m2"><p>سردار اسعد پاینده بادا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صمصام ایران برنده بادا</p></div>
<div class="m2"><p>ضرغام دین را دل زنده بادا</p></div></div>
<div class="b2" id="bn33"><p>کافتاد از ایشان بدخواه در چاه</p>
<p>الحمد لله‌، الحمد لله</p></div>
<div class="b" id="bn34"><div class="m1"><p>ستارخان را بادا ظفر یار</p></div>
<div class="m2"><p>تبریزیان را یزدان نگهدار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سالارشان را نیکو بودکار</p></div>
<div class="m2"><p>احرار را نیز دل باد بیدار</p></div></div>
<div class="b2" id="bn36"><p>تا حمله گویند با جان آگاه</p>
<p>الحمد لله‌، الحمد لله</p></div>