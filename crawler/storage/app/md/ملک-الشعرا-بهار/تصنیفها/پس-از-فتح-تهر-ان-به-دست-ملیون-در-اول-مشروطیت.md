---
title: >-
    پس از فتح تهر‌ان به دست ملیون در اول مشروطیت
---
# پس از فتح تهر‌ان به دست ملیون در اول مشروطیت

<div class="b" id="bn1"><div class="m1"><p>ای شهسواران وطن یزدان به ما یار آمده</p></div>
<div class="m2"><p>با رایت فتح و ظفر جیش سپهدار آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جیش صمصام رسید ایل ضرغام رسید</p></div>
<div class="m2"><p>لله الحمدکه کام دل ناکام رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل افکار وطن مادر زار وطن</p></div>
<div class="m2"><p>خوش‌خبر باش که غم جمله به اتمام رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکسره آزاد شدی ای وطن</p></div>
<div class="m2"><p>خرم و دلشاد شدی ای وطن</p></div></div>
<div class="b2" id="bn5"><p>از ستم آزاد شدی ای وطن</p></div>
<div class="b" id="bn6"><div class="m1"><p>زان ماه تابان وطن روشن شده جان وطن</p></div>
<div class="m2"><p>زان مهر رخشان وطن روز عدو تار آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیر گیلان یله شد جیش ما یک دله شد</p></div>
<div class="m2"><p>گرگ خونخوار وطن باز اسیر تله شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دل افکار وطن مادر زار وطن</p></div>
<div class="m2"><p>شاد شو شاد که بین تو و غم فاصله شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجلس مشروطه به پا شد دگر</p></div>
<div class="m2"><p>سلطنت‌آباد فنا شد دگر</p></div></div>
<div class="b2" id="bn10"><p>کار به کام دل ما شد دگر</p></div>