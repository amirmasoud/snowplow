---
title: >-
    مرغ سحر (در دستگاه ماهور)
---
# مرغ سحر (در دستگاه ماهور)

<div class="n" id="bn1"><p>بند اول</p></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ سحر ناله سر کن</p></div>
<div class="m2"><p>داغ مرا تازه‌تر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآه شرربار این قفس را</p></div>
<div class="m2"><p>برشکن و زبر و زبر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلبل پربسته ز کنج قفس درآ</p></div>
<div class="m2"><p>نغمهٔ آزادی نوع بشر سرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز نفسی عرصه این خاک توده را</p></div>
<div class="m2"><p>پرشرر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظلم ظالم‌، جور صیاد</p></div>
<div class="m2"><p>آشیانم داده بر باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خدا! ای فلک! ای طبیعت!</p></div>
<div class="m2"><p>شام تاریک ما را سحر کن</p></div></div>
<div class="b2" id="bn8"><p>*‌</p></div>
<div class="b" id="bn9"><div class="m1"><p>نوبهار است‌، گل به بار است</p></div>
<div class="m2"><p>ابر چشمم ژاله‌بار است</p></div></div>
<div class="b2" id="bn10"><p>این قفس چون دلم تنگ و تار است</p></div>
<div class="b" id="bn11"><div class="m1"><p>شعله فکن در قفس ای آه آتشین</p></div>
<div class="m2"><p>دست طبیعت گل عمر مرا مچین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جانب‌ عاشق نگه‌، ای‌ تازه گل‌! از این</p></div>
<div class="m2"><p>بیشتر کن</p></div></div>
<div class="b2" id="bn13"><p>مرغ بیدل‌، شرح هجران مختصر مختصر مختصر کن!</p>
<p>بند دوم</p></div>
<div class="b" id="bn14"><div class="m1"><p>عمر حقیقت به سر شد</p></div>
<div class="m2"><p>عهد و وفا بی‌سپر شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نالهٔ عاشق‌، ناز معشوق</p></div>
<div class="m2"><p>هردو دروغ و بی‌اثر شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>راستی و مهر و محبت فسانه شد</p></div>
<div class="m2"><p>قول و شرافت همگی از میانه شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از پی دزدی وطن و دین بهانه شد</p></div>
<div class="m2"><p>دیده تر شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ظلم مالک‌، جور ارباب</p></div>
<div class="m2"><p>زارع از غم گشته بیتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ساغر اغنیا پر می ناب</p></div>
<div class="m2"><p>جام ما پر ز خون جگر شد</p></div></div>
<div class="b2" id="bn20"><p>*</p></div>
<div class="b" id="bn21"><div class="m1"><p>ای دل تنگ ناله سر کن</p></div>
<div class="m2"><p>از قوی‌دستان حذر کن</p></div></div>
<div class="b2" id="bn22"><p>از مساوات صرف‌نظر کن</p></div>
<div class="b" id="bn23"><div class="m1"><p>ساقی گلچهره بده آب آتشین</p></div>
<div class="m2"><p>پردهٔ دلکش بزن ای یار دلنشین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ناله برآر از قفس ای بلبل حزین</p></div>
<div class="m2"><p>کز غم تو، سینهٔ من‌، پر شرر شد</p></div></div>
<div class="b2" id="bn25"><p>کز غم تو سینهٔ من پرشرر پرشرر پرشرر شد</p></div>