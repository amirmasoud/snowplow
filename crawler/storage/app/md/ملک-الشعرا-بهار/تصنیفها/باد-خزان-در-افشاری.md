---
title: >-
    باد خزان (در افشاری)
---
# باد خزان (در افشاری)

<div class="b" id="bn1"><div class="m1"><p>باد خزان وزان شد</p></div>
<div class="m2"><p>چهرهٔ گل خزان شد</p></div></div>
<div class="b2" id="bn2"><p>طلایه لشکر خزان از دو طرف عیان شد</p>
<p>چو ابر بهمن ز چشم من چشمهٔ خون روان شد</p></div>
<div class="b" id="bn3"><div class="m1"><p>ناله‌، بس مرغ سحر در غم آشیان زد</p></div>
<div class="m2"><p>آشیان سوخته بین مشعله در جهان زد</p></div></div>
<div class="b2" id="bn4"><p>عزیز من -‌ مشعله در جهان زد</p></div>
<div class="b" id="bn5"><div class="m1"><p>خدا خدا داد ز دست استاد</p></div>
<div class="m2"><p>که بسته رخ شاهد مه‌لقا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فغان و فریاد ز جور گردون</p></div>
<div class="m2"><p>که داده فتوای فنای ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشور خراب‌، فغان و زاری</p></div>
<div class="m2"><p>پیچه و نقاب سیاه و تاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وه چه کنم از غم بیقراری</p></div>
<div class="m2"><p>تا به کی کشیم ذلت و بیماری</p></div></div>
<div class="b2" id="bn9"><p>بیا مه من رویم از ورطهٔ جانسپاری</p></div>