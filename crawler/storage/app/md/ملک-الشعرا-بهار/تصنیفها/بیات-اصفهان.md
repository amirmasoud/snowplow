---
title: >-
    بیات اصفهان
---
# بیات اصفهان

<div class="n" id="bn1"><p>این تصنیف را بهار در منفای خود در سال ۱۳۱۲ ساخته و به به اهالی اصفهان اهدا کرده است‌.</p></div>
<div class="b" id="bn2"><div class="m1"><p>به اصفهان رو که تا بنگری بهشت ثانی</p></div>
<div class="m2"><p>به زنده‌رودش سلامی ز چشم ما رسانی</p></div></div>
<div class="b2" id="bn3"><p>ببر از وفا کنار جلفا به گل چهرگان سلام ما را</p></div>
<div class="b" id="bn4"><div class="m1"><p>شهر با شکوه</p></div>
<div class="m2"><p>قصر چلستون</p></div></div>
<div class="b2" id="bn5"><p>کن گذر به چار باغش</p></div>
<div class="b" id="bn6"><div class="m1"><p>گر شد از کفت‌</p></div>
<div class="m2"><p>یار بی‌وفا</p></div></div>
<div class="b2" id="bn7"><p>کن کنار پل سراغش</p></div>
<div class="b" id="bn8"><div class="m1"><p>بنشین درکریاس یاد شاه عباس بستان از دلبر می</p></div>
<div class="m2"><p>بستان از دست وی می پی در پی تاکی تا بتوانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز شادی در دهر کدامست</p></div>
<div class="m2"><p>غیر از می هرچیز حرام است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساعتی در جهان خرم بودن بی‌غم بودن بی‌غم بودن</p></div>
<div class="m2"><p>با بتی دلستان محرم بودن با هم بودن همدم بودن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای بت اصفهان زآن شراب جلفا ساغری در ده ما را</p></div>
<div class="m2"><p>ما غریبیم ای مه - ‌بر غریبان رحمی کن خدا را</p></div></div>