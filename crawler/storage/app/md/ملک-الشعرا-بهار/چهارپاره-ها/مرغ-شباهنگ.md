---
title: >-
    مرغ شباهنگ
---
# مرغ شباهنگ

<div class="b" id="bn1"><div class="m1"><p>برشو ای رایت روز از در شرق</p></div>
<div class="m2"><p>بشکف ای غنچهٔ صبح از بر کوه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهر را تاج زر آویز به فرق</p></div>
<div class="m2"><p>کامدم زین شب مظلم به‌ستوه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای شب موحش انده گستر</p></div>
<div class="m2"><p>اندک احسان و فراوان ستمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطلع یأس و هراسی تو مگر</p></div>
<div class="m2"><p>سحر حشر و غروب عدمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو شنیدی که منم برخی شب‌ </p></div>
<div class="m2"><p>آری اما نه چنان ابراندود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌فروغ مه و نور کوکب</p></div>
<div class="m2"><p>چون یکی زنگی انگشت‌آلود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ماه چون بیوه‌زنان پوشیده</p></div>
<div class="m2"><p>به حجاب سیه اندر، همه تن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخت پوشیده جمال از دیده</p></div>
<div class="m2"><p>تا ندانندکه پیرست آن زن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نجم ناهید نهان ساخته رو</p></div>
<div class="m2"><p>در پس ابر عبوس غمگین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مردم چشم من اندر پی او</p></div>
<div class="m2"><p>چون کسی کش به‌ چه افتاده نگین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مانده ازکار درین ظلمت عام</p></div>
<div class="m2"><p>به فلک برقلم تیرِ دبیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زانکه بر جای مرکب ز غمام</p></div>
<div class="m2"><p>دهر پرکرده دواتش از قیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مشتری بسته درین ابر سیاه</p></div>
<div class="m2"><p>سیه چهره از بیم فرجامی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>واندر امواج بخار جانکاه</p></div>
<div class="m2"><p>گم شده شعشعهٔ بهرامی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عاشقم من به شبی مینایی</p></div>
<div class="m2"><p>خوش و لیلی‌وش و هندیه‌عذار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه یکی وحشی افریقایی</p></div>
<div class="m2"><p>زشت و آشفته و مجنون کردار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>*</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عاشقم‌ من به ‌شبی ‌خامش ‌و صاف</p></div>
<div class="m2"><p>نور پیوسته سما را به سمک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همره نور سماوات شکاف</p></div>
<div class="m2"><p>به زمین تاخته آواز ملک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>*</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ماه بیرون شده از پشت سحاب</p></div>
<div class="m2"><p>گسترانیده شعاع سیمین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گاه پنهان شده در زیر نقاب</p></div>
<div class="m2"><p>گه عیان ساخته لختی ز جبین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عاشقم بر فلکی نورانی</p></div>
<div class="m2"><p>ز اختران پنجرهٔ نقره بر آن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من از آن پنجرهٔ روحانی</p></div>
<div class="m2"><p>در فضای ابدیت نگران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>*</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>‌نه هوایی کدر و گردآلود</p></div>
<div class="m2"><p>بر وی از ابر یکی خیمهٔ شوم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بسته اندر قفسی قیراندود</p></div>
<div class="m2"><p>منظره دیده ز دیدار نجوم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از تو و تیرگیت داد ای شب</p></div>
<div class="m2"><p>که دلم پاره شد از واهمه‌ات</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زین سیه کاری و بیداد ای شب</p></div>
<div class="m2"><p>به کجا برد توان مظلمه‌ات</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای شب جان‌شکر عمرگداز</p></div>
<div class="m2"><p>ای ز جور تو به هر دل اثری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ظلم کوته کندت دست دراز</p></div>
<div class="m2"><p>هر شبی را بود از پی سحری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>*</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من و دژخیم خیانت‌کردار</p></div>
<div class="m2"><p>بگذرانیم جهان گذران</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خفته او مست و من اینک بیدار</p></div>
<div class="m2"><p>بر وی از دیده نفرت نگران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>*</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شب که اندر بن این ژرف‌قباب</p></div>
<div class="m2"><p>خلق خفته‌است‌،‌خدا بیدار است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آنکه را دیده نیالود به‌ خواب</p></div>
<div class="m2"><p>دیده‌بانش کرم دادار است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>*</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تیره شد دیده و شد ختم کتاب</p></div>
<div class="m2"><p>لیک‌ نوز این‌ شب‌ غمناک بجاست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سپری گشت ز چشمانم خواب</p></div>
<div class="m2"><p>چون‌ غم‌ آید به‌ میان‌ خواب کجاست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به امیدی که مگر فجر دمید</p></div>
<div class="m2"><p>دمبدم دوخته بر شیشه نگاه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در پس شیشهٔ درگشت سپید</p></div>
<div class="m2"><p>چشم‌ بی‌خواب من و شیشه سیاه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شمع‌شد خامش‌و ساعت‌هم‌خفت</p></div>
<div class="m2"><p>دل من تفته و چشمم بیدار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شده با زحمت‌بیداری‌، جفت</p></div>
<div class="m2"><p>غم و اندیشهٔ این شهر و دیار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یک ره این پردهٔ غمناک بدر</p></div>
<div class="m2"><p>وین سیاهی ببر ای روز سپید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ورنه‌ای هیچ صباح محشر</p></div>
<div class="m2"><p>سر برآر از عدم ای صبح امید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نه شبم رام و نه روزم پیروز</p></div>
<div class="m2"><p>منزوی روز و دل اندر وا شب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چون شود شب‌ بخروشم‌ تا روز</p></div>
<div class="m2"><p>چون شود روز بنالم تا شب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>این بود حال غریبی چون من</p></div>
<div class="m2"><p>در یکی کشور بیداد سرشت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مانده بیگانه به شهر و به وطن</p></div>
<div class="m2"><p>چون مؤذن به کلیسا و کنشت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ای دریغا که جوانی بگذشت</p></div>
<div class="m2"><p>بهر آبادی این ملک خراب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همچو دهقان که برد آب ز دشت</p></div>
<div class="m2"><p>تا گل و سبزه دماند ز سراب</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یاد آرید در آن بستر ناز</p></div>
<div class="m2"><p>ای فرو خفته بهم فرزندان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زبن شبان سیه عمرگداز</p></div>
<div class="m2"><p>که سر آورده پدر در زندان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>یاد آر ای پسر خوب‌خصال</p></div>
<div class="m2"><p>کز تبه کاری این مردم دون</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پدرت گشت به خواری پامال</p></div>
<div class="m2"><p>تا تو گردی به شرافت مقرون</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شو سوی مدرسه‌ای دختر زار</p></div>
<div class="m2"><p>ای زن باهنر سیصد و بیست </p></div></div>
<div class="b" id="bn72"><div class="m1"><p>واندر آن عهد همایون یاد آر</p></div>
<div class="m2"><p>تا بدانی پدرت کشتهٔ کیست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>لیک دانم که در آن عهد و زمن</p></div>
<div class="m2"><p>این مصائب همه با یاد شماست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>جستن کین من و ملت من</p></div>
<div class="m2"><p>اندر آن روز، ورستاد شماست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>روزگاری که شما آزادان</p></div>
<div class="m2"><p>باز جویید ز دزدان کیفر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دزدزادان و ستمگرزادان</p></div>
<div class="m2"><p>غرق ننگند و شما نام‌آور</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بحرم بر، گلهٔ گرگ رده</p></div>
<div class="m2"><p>به‌صفت گرگ و به صورت چو غنم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>خورده آهوی حرم را و شده</p></div>
<div class="m2"><p>جای آهوی حرم گرگ حرم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ای جوانان غیور فردا</p></div>
<div class="m2"><p>پردل و باشرف و زبرک‌سار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>پاک سازید ز گرگان دغا</p></div>
<div class="m2"><p>حرم پاک وطن را یکبار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>آن سیه لحظه که از گرسنگی</p></div>
<div class="m2"><p>رخ اطفال وطن گردد زرد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سبزخطان و جوانان همگی</p></div>
<div class="m2"><p>بیرق فتح به کف بهر نبرد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>تو هم ای پور دل‌آزردهٔ من</p></div>
<div class="m2"><p>اندر آن روز به یاد آر این درس</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>پای نه پیش و به تن پوش کفن</p></div>
<div class="m2"><p>سر غوغا شو و از مرگ مترس</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>روزکیفر چو طبیعت خواند</p></div>
<div class="m2"><p>خائنان را پی تفریغ حساب</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>دزدزاده ز تو خط بستاند</p></div>
<div class="m2"><p>بو که تخفیف دهندش به‌ عذاب</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>پسر من‌! تو به روز کیفر</p></div>
<div class="m2"><p>ریشهٔ عاطفه از دل برکن</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>از سرکیفر دزدان مگذر</p></div>
<div class="m2"><p>تا پشیمان نشوی همچون من</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>اجر این تیره‌شبان مظلم</p></div>
<div class="m2"><p>بازگردد به تو در روز حسیب</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>راند آن روز نژاد ظالم</p></div>
<div class="m2"><p>که ز ما هر دو که ‌خورده‌است فریب</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بخ بخ ای مرغ شباهنگ ز شاخ</p></div>
<div class="m2"><p>با من دلشده دمسازی کن</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>تو هم ای دل به ره حق گستاخ</p></div>
<div class="m2"><p>با شباهنگ هم‌آوازی کن</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ای شباهنگ! از آن شاخ بلند</p></div>
<div class="m2"><p>شو یک امشب ز وفا یار بهار</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>گر بخواهی که شوم من خرسند</p></div>
<div class="m2"><p>یکدم ازگفتن حق دست مدار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>هان چه گوید بشنو، مرغ ز دور</p></div>
<div class="m2"><p>می‌دهد پاسخ من‌، حق حق حق</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>آخر از همت مردان غیور</p></div>
<div class="m2"><p>شود آباد وطن‌، حق حق حق</p></div></div>