---
title: >-
    بنای یادگار
---
# بنای یادگار

<div class="b" id="bn1"><div class="m1"><p>در دهر بزرگ یادگاری</p></div>
<div class="m2"><p>کردم ز برای خویش بنیاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنیاد بنای پایداری</p></div>
<div class="m2"><p>بی‌یاری دست من شد ایجاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>*</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خار و خس روزگار ناساز</p></div>
<div class="m2"><p>سد کردن راه او نیارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بانی خود ز فرط اعزاز</p></div>
<div class="m2"><p>سر پیش کسی فرو نیارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آسیب زمانه برکنار است</p></div>
<div class="m2"><p>کز خاک منست دیر پاتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ستوار و بلند و پایدار است</p></div>
<div class="m2"><p>مانند منارهٔ سکندر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بربط من شدست پنهان</p></div>
<div class="m2"><p>این روح لطیف لایزالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از مردن تن نیم هراسان</p></div>
<div class="m2"><p>کاز من نشود زمانه خالی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در عرصهٔ پهن‌دشت سقلاب</p></div>
<div class="m2"><p>ز آوازه‌ام افتد انقلابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وز جلوه به جلوه گاه مهتاب</p></div>
<div class="m2"><p>مشهور شوم چو آفتابی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا زنده بود یکی در این بوم</p></div>
<div class="m2"><p>تا زنده بود کمیت نامم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا هست سخن به دهر معلوم</p></div>
<div class="m2"><p>معلوم جهان بود کلامم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر هموطن سرودخوانی</p></div>
<div class="m2"><p>گویاست به یاد من زبانش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>افتد سخنم به هر زبانی</p></div>
<div class="m2"><p>آزادی و عشق ترجمانش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>*</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با بربط خود به جنبش آرم</p></div>
<div class="m2"><p>هر شش جهت و چهارسو را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>واندر دل خلق زنده دارم</p></div>
<div class="m2"><p>اخلاق و عواطف نکو را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در ساحت این زمانه تار</p></div>
<div class="m2"><p>رحم از دل من فکند سایه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حریت و انقلاب افکار</p></div>
<div class="m2"><p>از گفتهٔ من گرفت مایه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای‌ طبع سخن‌سرای من‌،‌ خیز</p></div>
<div class="m2"><p>تا در ره حق شوی سخن‌ساز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اندیشه مکن ز خنجر تیز</p></div>
<div class="m2"><p>مغرور مشو به تاج اعزاز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا بی‌خردان به آزمایش</p></div>
<div class="m2"><p>مستیز و ره وقار بگزین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فارغ ز نکوهش و ستایش</p></div>
<div class="m2"><p>خونسرد به‌آفرین و نفرین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>*</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر بربط خود بناز بنشین‌</p></div>
<div class="m2"><p>کن با پر و بال نغمه پرواز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وز خاک برآ به اوج پروبن</p></div>
<div class="m2"><p>پرکن همهٔ فضا از آواز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا اختر نحس نامرادی</p></div>
<div class="m2"><p>این پنبه زگوش خود برآرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وز چشم فلک ز فرط شادی</p></div>
<div class="m2"><p>اختر عوض سرشگ بارد</p></div></div>