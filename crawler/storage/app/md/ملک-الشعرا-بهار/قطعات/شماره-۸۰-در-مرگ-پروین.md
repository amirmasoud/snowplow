---
title: >-
    شمارهٔ ۸۰ - در مرگ پروین
---
# شمارهٔ ۸۰ - در مرگ پروین

<div class="b" id="bn1"><div class="m1"><p>نهفته روی به برگ اندرون گلی محجوب</p></div>
<div class="m2"><p>ز باغبان طبیعت ملول و غمگین بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زتاب و جلوه اگر چند مانده بود جدا</p></div>
<div class="m2"><p>ولی ز نکهت او باغ عنبرآگین بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز اوستادی خورشید و دایگانی ماه</p></div>
<div class="m2"><p>جدا به سایهٔ اشجار، فرد و مسکین بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه با تحیت نوری ز خواب برمی‌خاست</p></div>
<div class="m2"><p>نه به افسانهٔ مرغی سرش به بالین بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فسرده عارض بی‌رنگ او به سایه‌، ولیک</p></div>
<div class="m2"><p>فروغ شهرت او رونق بساتین بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمال ظاهر او پرورش گر ازهار</p></div>
<div class="m2"><p>جمال باطنش آرایش ریاحین بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جای چهره‌فروزی به بوستان وجود</p></div>
<div class="m2"><p>نصیب او ز طبیعت وقار و تمکین بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چگونه ‌چهره‌ فروزد تنی که سوزی داشت</p></div>
<div class="m2"><p>چگونه جلوه فروشد دلی که خونین بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز ازدحام هواها مصون که برگردش</p></div>
<div class="m2"><p>ز دور باش حقیقت مدام پرچین بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه غم که بر سر باغ مجاز جلوه نکرد</p></div>
<div class="m2"><p>گلی که از نفسش طبع دهر مشکین بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به‌ خسروان‌ سخن ناز اگر فروخت رواست</p></div>
<div class="m2"><p>شکر لبی که خداوند طبع شیرین بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسی که عقد سخن را به لطف داد نظام</p></div>
<div class="m2"><p>ز جمع پردگیان بی‌خلاف‌، پروین بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جلیس بیت‌ حزن‌ شد چو یوسفش کم گشت</p></div>
<div class="m2"><p>غم فراق پدر هرچه بود سنگین بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به نوبهار حیات از خزان مرگ، به‌ باد</p></div>
<div class="m2"><p>شد آن گلی که نه در انتظار گلچین بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگرچه آرزوی زندگی ببرد به گور</p></div>
<div class="m2"><p>ولی به زندگی امیدوار و خوش‌بین بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگرچه‌ حجلهٔ‌ رنگین‌ به کام خویش نساخت</p></div>
<div class="m2"><p>ولی ز شعر خوشش روی دهر رنگین بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ندیده کام جوانی جوانه مرگش کرد</p></div>
<div class="m2"><p>سپهر پیرکه با اهل معنی اش کین بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شگفت‌ و عطر برافشاند و خنده کرد و بریخت</p></div>
<div class="m2"><p>نتیجهٔ گل افسرده عاقبت این بود</p></div></div>