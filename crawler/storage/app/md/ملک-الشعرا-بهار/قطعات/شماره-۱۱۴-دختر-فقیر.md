---
title: >-
    شمارهٔ ۱۱۴ - دختر فقیر
---
# شمارهٔ ۱۱۴ - دختر فقیر

<div class="b" id="bn1"><div class="m1"><p>دختری خرد بدیدم به گدایی مشغول</p></div>
<div class="m2"><p>کرده در جامهٔ صدپاره نهان پیکر خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود مکشوف به تاراجگه دزد نگاه</p></div>
<div class="m2"><p>گرچه در ژنده نهان ساخته بد گوهر خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ورچه ز اهل دل و دین رحم طمع داشت ولی</p></div>
<div class="m2"><p>بود خصم دل و دین از نگه کافر خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حبه‌ای سیم بدو دادم و بگذشتم و سوخت</p></div>
<div class="m2"><p>برق چشم تر او خرمنم از آذر خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شامگاهان به یکی بیشه شدم بر لب رود</p></div>
<div class="m2"><p>ناگهان دیدمش آنجا به سر معبر خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با لبی خنده‌زنان می‌شد و می‌خواند سرود</p></div>
<div class="m2"><p>به خلاف لب خشکیده و چشم تر خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم ای شوخ نبودی تو که یک‌ ساعت پیش</p></div>
<div class="m2"><p>سوختی خرمن اهل نظر از منظر خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای ترشرو چه شد آن گریه تلخت که چنین</p></div>
<div class="m2"><p>خنده را، کان نمک ساخته از شکر خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت دارم پدری عاجز و مامی بیمار</p></div>
<div class="m2"><p>که نیارند بپا خاستن از بستر خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست این خنده‌ام از بهر دل خود لیکن</p></div>
<div class="m2"><p>گریه‌ام بود برای پدر و مادر خویش</p></div></div>