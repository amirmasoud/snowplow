---
title: >-
    شمارهٔ ۱۹۰ - نیکنامی
---
# شمارهٔ ۱۹۰ - نیکنامی

<div class="b" id="bn1"><div class="m1"><p>چون برکه‌های دشت عرب دان تو حال خلق</p></div>
<div class="m2"><p>گاهی ز آب پر شود و نوبتی تهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این برکهٔ حیات مسلم تهی شود</p></div>
<div class="m2"><p>از آب زندگانی و از فر و فرهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیر است و زود مرگ نباشد از آن گریز</p></div>
<div class="m2"><p>فرخنده نیکنامی و خوشبخت آگهی</p></div></div>