---
title: >-
    شمارهٔ ۳۹ - برف
---
# شمارهٔ ۳۹ - برف

<div class="b" id="bn1"><div class="m1"><p>ابری به خروش آمد چون قلزم مواج</p></div>
<div class="m2"><p>بر روی زمین بیخت هزاران ورق عاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویا فلک امروز بریزد به سر خلق</p></div>
<div class="m2"><p>پس ماندهٔ آن شیر برنج شب معراج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلاج شدست ابر و زند برف چو پنبه</p></div>
<div class="m2"><p>لرزان من ازین حادثه چون خایهٔ حلاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویی که یکی سید، مندیل عوض کرد</p></div>
<div class="m2"><p>زان برف فراوان که نشسته به سرکاج</p></div></div>