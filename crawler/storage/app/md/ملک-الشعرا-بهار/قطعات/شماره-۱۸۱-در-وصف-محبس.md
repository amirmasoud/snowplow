---
title: >-
    شمارهٔ ۱۸۱ - در وصف محبس
---
# شمارهٔ ۱۸۱ - در وصف محبس

<div class="b" id="bn1"><div class="m1"><p>سهمگین‌ سمجی چو تاری مسکنی</p></div>
<div class="m2"><p>بسته برروبش دری چون آهنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاسبانانی در آنجا صف زده</p></div>
<div class="m2"><p>هریکی از خشم چون اهریمنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیست گویی اندربن در بسته سمج</p></div>
<div class="m2"><p>رستمی آنجاست یا روبین‌تنی</p></div></div>