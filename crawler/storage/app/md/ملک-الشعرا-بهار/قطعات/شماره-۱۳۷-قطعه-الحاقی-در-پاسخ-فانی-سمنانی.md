---
title: >-
    شمارهٔ ۱۳۷ - قطعهٔ الحاقی در پاسخ فانی سمنانی
---
# شمارهٔ ۱۳۷ - قطعهٔ الحاقی در پاسخ فانی سمنانی

<div class="b" id="bn1"><div class="m1"><p>فانی‌، کز زادن چنو سخن آرای</p></div>
<div class="m2"><p>مادر ایام شد عقیم و سترون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشا زبن چامهٔ بدیع که باشد</p></div>
<div class="m2"><p>باغی پریاسمین و خیری و سوسن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر ورقی راکزو دو بیت نگاری</p></div>
<div class="m2"><p>گردد بیغارهٔ پرند ملوّن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدم ازبن یک قصیده پاکی طبعش</p></div>
<div class="m2"><p>دید توان نور آفتاب ز روزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک من و فانی‌ایم بندهٔ ناصر</p></div>
<div class="m2"><p>آنکه سروده است این چکامهٔ متقن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>«‌دیر بماندم در این سرای کهن من‌»</p></div>
<div class="m2"><p>«‌تاکهنم کرد صحبت دی و بهمن‌»</p></div></div>