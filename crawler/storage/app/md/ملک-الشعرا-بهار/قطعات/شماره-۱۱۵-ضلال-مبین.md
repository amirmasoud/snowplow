---
title: >-
    شمارهٔ ۱۱۵ - ضلال مبین
---
# شمارهٔ ۱۱۵ - ضلال مبین

<div class="b" id="bn1"><div class="m1"><p>دیدم به بصره دخترکی اعجمی نسب</p></div>
<div class="m2"><p>روشن نموده شهر به نور جمال خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌خواند درس قرآن در پیش شیخ شهر</p></div>
<div class="m2"><p>وز شیخ دل ربوده به غنج و دلال خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌داد شیخ‌، درس ضلال مبین بدو</p></div>
<div class="m2"><p>و آهنگ ضاد رفته به اوج کمال خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دختر نداشت طاقت گفتار حرف ضاد</p></div>
<div class="m2"><p>با آن دهان کوچک غنچه مثال خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌داد شیخ را به «‌دلال مبین‌» جواب</p></div>
<div class="m2"><p>وان شیخ می‌نمود مکرر مقال خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم به شیخ راه ضلال این‌قدر مپوی</p></div>
<div class="m2"><p>کاین شوخ منصرف نشود از خیال خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهتر همان بودکه بمانید هر دوان</p></div>
<div class="m2"><p>او در دلال خویش و تو اندر ضلال خویش</p></div></div>