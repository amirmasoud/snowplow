---
title: >-
    شمارهٔ ۵۶ - در مرثیهٔ عشقی
---
# شمارهٔ ۵۶ - در مرثیهٔ عشقی

<div class="b" id="bn1"><div class="m1"><p>وه که عشقی در صباح زندگی</p></div>
<div class="m2"><p>از خدنگ دشمن شبرو بمرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرتوی بود ازفروغ آرزو</p></div>
<div class="m2"><p>آن فروغ افسرد وآن پرتو بمرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاعری نوبود وشعرش نیزنو</p></div>
<div class="m2"><p>شاعر نو رفت و شعر نو بمرد</p></div></div>