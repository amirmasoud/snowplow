---
title: >-
    شمارهٔ ۱۵۰ - شکایت از بچه‌ها
---
# شمارهٔ ۱۵۰ - شکایت از بچه‌ها

<div class="b" id="bn1"><div class="m1"><p>فکر مرا سخت مشون کند</p></div>
<div class="m2"><p>نعره این دخترک بی‌سکون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مال نه وگشته ز بخت سیاه</p></div>
<div class="m2"><p>خانه لبالب ز بنات و بنون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبر مرا بردند از قال و قیل</p></div>
<div class="m2"><p>مغز مرا خوردند از چند و چون</p></div></div>