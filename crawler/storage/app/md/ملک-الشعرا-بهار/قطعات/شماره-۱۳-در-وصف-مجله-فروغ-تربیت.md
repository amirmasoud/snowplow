---
title: >-
    شمارهٔ ۱۳ - در وصف مجلهٔ فروغ تربیت
---
# شمارهٔ ۱۳ - در وصف مجلهٔ فروغ تربیت

<div class="b" id="bn1"><div class="m1"><p>به باغ در، به مه دی خمیده خاربنی</p></div>
<div class="m2"><p>به پیشم آمدگفتم درین چه خاصیت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه تیر قامت او را ز غنچه پیکانست</p></div>
<div class="m2"><p>نه صدر حشمت او را ز برگ حاشیت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسان تیغی کان‌را نه قبضه و نه نیام</p></div>
<div class="m2"><p>بسان شعری کان را نه وزن و قافیت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان برف یکی خاربن تو گفتی راست</p></div>
<div class="m2"><p>میانهٔ دل پاک‌، ازکژی یکی نیت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوای او به دل اندر غم آورد، گویی</p></div>
<div class="m2"><p>ز طبع خسته یکی پر ملال مرثیت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نوبهاران زان پس بدیدمش خوش و خوب</p></div>
<div class="m2"><p>چو توبه‌ای خوش کاندر قفای معصیت است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکفته سرخ گلی بر فرازآن گفتی</p></div>
<div class="m2"><p>فراز قصر سعادت درفش عافیت است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شگفتم آمد زان حال و فکرتم جنبید</p></div>
<div class="m2"><p>بلی شگفتی آغاز فکر و تزکیت است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگاه کردم هر سو و راز آن جستم</p></div>
<div class="m2"><p>که آن‌چه خاصیتی بود و این چه کیفیت است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسیط خاک بنگشود راز من آری</p></div>
<div class="m2"><p>بسیط خاک چراگاه راز و تعمیت است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برآسمان نگرستم وزآفتاب بلند</p></div>
<div class="m2"><p>سئوال کردم‌، گفت این فروغ تربیت است</p></div></div>