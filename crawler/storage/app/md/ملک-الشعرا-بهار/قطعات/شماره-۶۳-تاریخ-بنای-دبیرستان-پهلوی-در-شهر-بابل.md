---
title: >-
    شمارهٔ ۶۳ - تاریخ بنای دبیرستان پهلوی در شهر بابل
---
# شمارهٔ ۶۳ - تاریخ بنای دبیرستان پهلوی در شهر بابل

<div class="b" id="bn1"><div class="m1"><p>در زمان پهلوی شاهنشه ایران‌، کزو</p></div>
<div class="m2"><p>کشور ایران ز قید هرج و مرج آزاد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکسی آشفته بود از شفقتش آسوده گشت</p></div>
<div class="m2"><p>هرکجا ویرانه بود از همتش آباد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دل افسرده از او شعلهٔ شادی کشید</p></div>
<div class="m2"><p>هر دژ مخروبه از او غیرت نوشاد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عزت عصر قدیم و ذلت عهد اخیر</p></div>
<div class="m2"><p>آن‌ یکی با یاد آمد وین یکی از یاد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی کند جیش حوادث رخنه دراین مرز و بوم</p></div>
<div class="m2"><p>زان که ایران را حصار از آهن و پولاد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز نفاذ امر خسرو، کوهکن را در عمل</p></div>
<div class="m2"><p>مته شیرین‌کارتر از تیشهٔ فرهاد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بساط جم برفتی بر سر باد و هوا</p></div>
<div class="m2"><p>زین شه جم رتبه خاک کوه‌ها بر باد شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیگران‌ در جهل کوشیدند و شه کوشد به‌ علم</p></div>
<div class="m2"><p>زان که داند که‌ ارتقای کشور از استاد شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی به بابل بلکه در هر نقطهٔ کشور ز علم</p></div>
<div class="m2"><p>ریخته بنیادها زین شهریار راد شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر مراد شاه‌، فارغ چون که دستور علوم</p></div>
<div class="m2"><p>زبن دبیرستان‌.خوش‌بنیاد محکم لاد شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کلک مشکین بهار از بهر تاریخش نوشت</p></div>
<div class="m2"><p>این دبیرستان به یمن پهلوی بنیاد شد</p></div></div>