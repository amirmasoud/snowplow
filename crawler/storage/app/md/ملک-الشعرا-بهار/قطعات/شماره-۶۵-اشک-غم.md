---
title: >-
    شمارهٔ ۶۵ - اشک غم
---
# شمارهٔ ۶۵ - اشک غم

<div class="b" id="bn1"><div class="m1"><p>حسین دانش آن سرخیل ابرار</p></div>
<div class="m2"><p>که در عالم به دانایی علم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درختی سایه گستر بود افسوس</p></div>
<div class="m2"><p>که پیش تندباد مرگ خم شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بنیان ادب رکنی فرو ریخت</p></div>
<div class="m2"><p>ز بستان هنر نخلی قلم شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل روشندلان از فرقت وی</p></div>
<div class="m2"><p>قرین حرقت و رنج و الم شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نپنداری که دانش از میان رفت</p></div>
<div class="m2"><p>وجودش سوی اقلیم عدم شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی آب از یم ایجاد برخاست</p></div>
<div class="m2"><p>تکاپو کرد و آخر سوی یم شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وجودش‌ با وجود گل قرین کشت</p></div>
<div class="m2"><p>مزاجش با مزاج دهر ضم شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم سوزد به حال اهل تحقیق</p></div>
<div class="m2"><p>که فردی کامل از آن جمع کم شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به‌مرگ او «‌بهار» اشگ غم افشاند</p></div>
<div class="m2"><p>همان بر تربت پاکش رقم شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیا تا اشگ غم بر وی فشانیم</p></div>
<div class="m2"><p>که تاریخ وفاتش «‌اشک غم‌» شد</p></div></div>