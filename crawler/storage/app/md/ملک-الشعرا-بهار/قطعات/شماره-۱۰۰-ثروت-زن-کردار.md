---
title: >-
    شمارهٔ ۱۰۰ - ثروت - زن - کردار 
---
# شمارهٔ ۱۰۰ - ثروت - زن - کردار 

<div class="b" id="bn1"><div class="m1"><p>داشت‌ شخصی ‌از همه‌ عالم‌ سه‌ دوست</p></div>
<div class="m2"><p>هرسه با او جور و او با هر سه جور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اولین‌، آن ثروتی کز روی سعی</p></div>
<div class="m2"><p>کرده حاصل در سنین و در شهور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دومین‌، حوری‌وشی کاو را نبود</p></div>
<div class="m2"><p>یک سر مو در دلارایی قصور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سومین‌، مجموع خوبی‌ها که او</p></div>
<div class="m2"><p>کرده با مردم به‌تدریج و مرور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زمان احتضارش دررسید</p></div>
<div class="m2"><p>خواجه داد آن هر سه را اذن حضور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد با ثروت وداعی سوزناک</p></div>
<div class="m2"><p>گفت کای سرمایهٔ عیش و سرور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پس مرگم چه خواهی کرد؟ گفت‌:</p></div>
<div class="m2"><p>چون تو بگذشتی اپن دارالغرور،</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر مزارت شمع‌ها روشن کنم</p></div>
<div class="m2"><p>تا شود روحت سراسر غرق نور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت با محبوبه کای آرام جان</p></div>
<div class="m2"><p>بعد مرگم باش آرام و صبور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت بر قبرت چنان شیون کنم</p></div>
<div class="m2"><p>کزلحد جستن کنند اهل قبور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت آخر بار با کردار خویش</p></div>
<div class="m2"><p>کای به خوبی غیرت غلمان وحور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو پس ‌از مرگم چه‌خواهی کرد؟ گفت‌:</p></div>
<div class="m2"><p>من نخواهم شد ز نزدیک تو دور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون که دمساز تو بودم روز و شب</p></div>
<div class="m2"><p>با تو خواهم بود تا یوم النشور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>محتضر جان ‌داد و دادند آن سه دوست</p></div>
<div class="m2"><p>نعش او را سوی قبرستان عبور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن یکی شمعی نهاد از روی کوه</p></div>
<div class="m2"><p>وان دگر اشکی فشاند از روی زور!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ثروت و زن هر دو برگشتند، لیک</p></div>
<div class="m2"><p>رفت خوبی‌های او با او به گور!</p></div></div>