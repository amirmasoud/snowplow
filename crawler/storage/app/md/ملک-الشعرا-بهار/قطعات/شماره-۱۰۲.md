---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>غذای میر ندیدم ولی به گاه غذا</p></div>
<div class="m2"><p>بر اوگذشتم و دیدم که چاکران امیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمان گروهه به کف گرد سفره‌خانه او</p></div>
<div class="m2"><p>کمین گشاده مگس ها همی زنند به تیر</p></div></div>