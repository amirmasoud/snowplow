---
title: >-
    شمارهٔ ۱۴۵ - دین و وطن
---
# شمارهٔ ۱۴۵ - دین و وطن

<div class="b" id="bn1"><div class="m1"><p>زمانه کرد چو در بر شعار دین و وطن</p></div>
<div class="m2"><p>شدند مردم مسکین شکار دین و وطن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به میر و کاهن روز نخست لعنت باد</p></div>
<div class="m2"><p>کز آن دوگشت بپا یادگار دین و وطن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز پیش گرسنگان بهر پاس عزت خویش</p></div>
<div class="m2"><p>گریختند به پشت حصار دین و وطن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر اختلاف خلایق بنای دولت خویش</p></div>
<div class="m2"><p>نهاد هرکه نمود ابتکار دین و وطن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خیر محض نباشد جهان ولیک افتاد</p></div>
<div class="m2"><p>بشر به دام شرور از شرار دین و وطن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدا نیای بشر بود و خاک مادر او</p></div>
<div class="m2"><p>فغان ز قیم نااستوار دین و وطن</p></div></div>