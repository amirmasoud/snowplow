---
title: >-
    شمارهٔ ۱۲۳ - قطعه‌ (خطاب به استاد جلال همایی)
---
# شمارهٔ ۱۲۳ - قطعه‌ (خطاب به استاد جلال همایی)

<div class="b" id="bn1"><div class="m1"><p>به گلگشت جنان گل می‌فرستم</p></div>
<div class="m2"><p>به رضوان شاخ سنبل می‌فرستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هندستان فضل و خلر علم</p></div>
<div class="m2"><p>می و موز قرنفل می‌فرستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستاک نرگس وشاخ بنفشه</p></div>
<div class="m2"><p>به ساری و به آمل می‌فرستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیث خوش به قمری می‌سرایم</p></div>
<div class="m2"><p>سرود خوش به بلبل می‌فرستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به امریکی تمول می‌فروشم</p></div>
<div class="m2"><p>به پاریسی تجمل می‌فرستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به قابوس و به صابی از رعونت</p></div>
<div class="m2"><p>خط و شعر و ترسل می‌فرستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خودبینی و رعنایی و شوخی است</p></div>
<div class="m2"><p>که جزوی را سوی کل می‌فرستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جلفای صفاهان از سر جهل</p></div>
<div class="m2"><p>شراب صافی و مل می‌فرستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به تبت مشک اذفر می‌گشایم</p></div>
<div class="m2"><p>به ماچین تار کاکل می‌فرستم</p></div></div>