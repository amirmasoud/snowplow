---
title: >-
    شمارهٔ ۱۶ - فتنه بیدار شد
---
# شمارهٔ ۱۶ - فتنه بیدار شد

<div class="b" id="bn1"><div class="m1"><p>یارم از خوابگاه من برخاست</p></div>
<div class="m2"><p>فتنه بیدار شد که زن برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت من سر ز خوابگه برکرد</p></div>
<div class="m2"><p>وز چمن شاخ یاسمن برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش زلف سیاهش آهوی چین</p></div>
<div class="m2"><p>از سر نافهٔ ختن برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان که بسپرد دل بدان سر زلف</p></div>
<div class="m2"><p>از سر جان بستن برخاست</p></div></div>