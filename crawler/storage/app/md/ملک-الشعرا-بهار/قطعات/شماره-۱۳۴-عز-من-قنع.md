---
title: >-
    شمارهٔ ۱۳۴ - عز من قنع
---
# شمارهٔ ۱۳۴ - عز من قنع

<div class="b" id="bn1"><div class="m1"><p>گفتند فروتن شو تا زر به کف آری</p></div>
<div class="m2"><p>زرگرد شود چون که شود مرد فروتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که فروتن نشود مرد جوانمرد</p></div>
<div class="m2"><p>ننهد ز پی مال به بدنامی گردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان مال عزیز است کزان عزت زاید</p></div>
<div class="m2"><p>عزت را با ذلت حاصل نکنم من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزدیک فقیرانم خوشخوار چو حلوا</p></div>
<div class="m2"><p>نزدیک امیرانم دشخوار چو آهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دوست ندارند مرا دولتمندان</p></div>
<div class="m2"><p>بهتر که تهیدستان دارندم دشمن</p></div></div>