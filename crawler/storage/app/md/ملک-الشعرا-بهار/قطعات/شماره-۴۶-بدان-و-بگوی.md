---
title: >-
    شمارهٔ ۴۶ - بدان و بگوی
---
# شمارهٔ ۴۶ - بدان و بگوی

<div class="b" id="bn1"><div class="m1"><p>سخن چوگویی سنجیده گوی در مجلس</p></div>
<div class="m2"><p>که از کلام نسنجیده خوار گردد مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درست گوی و ادب ورز و بر گزافه مرو</p></div>
<div class="m2"><p>صریح باش و به‌ جد کوش و گرد هزل مگرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسا سخن که ازو خاست بحث و جنگ و قتال</p></div>
<div class="m2"><p>بسا عمل که از او زاد رشگ و کین و نبرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر آنچه گویی دانی‌، بری فراوان سود</p></div>
<div class="m2"><p>ور آنچه دانی گویی‌، کشی فراوان درد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه هرکه هرچه توانست کفت‌، بایدگفت‌!</p></div>
<div class="m2"><p>نه هرکه هرچه توانست کرد، بایدکرد!</p></div></div>