---
title: >-
    شمارهٔ ۳۷ - پروانه
---
# شمارهٔ ۳۷ - پروانه

<div class="b" id="bn1"><div class="m1"><p>آن شمع دل‌افروز من از خانهٔ من رفت</p></div>
<div class="m2"><p>پروای گلم نیست که پروانهٔ من رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم‌ صدف‌آسا کف‌ خالی و لب خشک</p></div>
<div class="m2"><p>تا ازکفم آن گوهر یکدانهٔ من رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون باغ خزان دیده ز پیرایه فتادم</p></div>
<div class="m2"><p>زبن شاخه پرگل که زگلخانهٔ من رفت</p></div></div>