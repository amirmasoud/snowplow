---
title: >-
    شمارهٔ ۱۴۴ - وعدهٔ مادر
---
# شمارهٔ ۱۴۴ - وعدهٔ مادر

<div class="b" id="bn1"><div class="m1"><p>شنیده‌ام پسری را جنایتی افتاد</p></div>
<div class="m2"><p>از اتفاق که شرحش نمی‌توان دادن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قضات محکمه دادند حکم قتلش را</p></div>
<div class="m2"><p>که رسم نیست به بیچارگان امان دادن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به‌دست و پای درافتاد مادرش که مگر</p></div>
<div class="m2"><p>توان نجاتش از آن مرگ ناگهان دادن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود علاقهٔ مادر به حالت فرزند</p></div>
<div class="m2"><p>حکایتی که محال است شرح آن دادن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن که بود مقصر جوان و دشوار است</p></div>
<div class="m2"><p>رضا به فاجعهٔ مرگ نوجوان دادن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به صورتش دم تیغ آشنا نگشته جفاست</p></div>
<div class="m2"><p>گلوش را به دم تیغ خونفشان دادن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهار زندگیش ناشکفته حیف بود</p></div>
<div class="m2"><p>گلش به دست جفاکاری خزان دادن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی دربغ که قانون حرام می‌دانست</p></div>
<div class="m2"><p>چنان شکار حلالی به رایگان دادن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود شکستن قانون گناه و نیست گناه</p></div>
<div class="m2"><p>عزیز جانی در دست جان‌ستان دادن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فقیر بود زن و ناله‌اش نداشت اثر</p></div>
<div class="m2"><p>کجا به ناله توان سنگ را تکان دادن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه رسوم و قوانین نوشته بر فقراست</p></div>
<div class="m2"><p>بجز مراتب احسان و رسم نان دادن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وسیله‌ای به ضمیر زن فقیرگذشت</p></div>
<div class="m2"><p>که باید آن را یاد جهانیان دادن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرفت رخصت و در حبسگه پسر را دید</p></div>
<div class="m2"><p>چه مشکل است تسلی در آن مکان دادن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفت غم مخور ای نور دیده کاسانست</p></div>
<div class="m2"><p>ترا نجات ازین بحر بیکران دادن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به رهن داده‌ام اسباب خانه را امروز</p></div>
<div class="m2"><p>که لازمست تعارف به این و آن دادن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز پای دار به آن غرفه بلند نگر</p></div>
<div class="m2"><p>مرا ببینی آنجا به امتحان دادن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرم سپیدبود رخت مطمئن گشتن</p></div>
<div class="m2"><p>وگر سیاه‌، به چنگ اجل عنان دادن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شبی گذاشت‌پسر در امید وگفت رواست</p></div>
<div class="m2"><p>زمام کار به اشخاص کاردان دادن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صباح مرگ یکی دار دید و میدانی</p></div>
<div class="m2"><p>پر ازدحام‌، چو لشکر به وقت سان دادن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به غرفه مادر خود دید در لباس سفید</p></div>
<div class="m2"><p>دلش قوی شد از آن عهد و آن زبان دادن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشاط کرد و بشد شادمانه تا در مرگ</p></div>
<div class="m2"><p>چو داد باید جان‌، به که شادمان دادن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فتاد رشتهٔ دارش به گردن و جان داد</p></div>
<div class="m2"><p>به‌رغم مادر و آن وعدهٔ نهان دادن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی بگفت به آن داغدیده مادر زار</p></div>
<div class="m2"><p>به وقت تسلیت وتعزیت نشان دادن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چرا تو وعدهٔ آزادی پسر دادی</p></div>
<div class="m2"><p>مگر نبود خطا وعده‌ای چنان دادن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جواب داد چو نومیدگشتم این گفتم</p></div>
<div class="m2"><p>که بچه‌ام نخورد غم به‌وقت جان دادن</p></div></div>