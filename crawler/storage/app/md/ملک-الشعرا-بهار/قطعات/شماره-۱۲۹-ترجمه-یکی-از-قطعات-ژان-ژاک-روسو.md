---
title: >-
    شمارهٔ ۱۲۹ - ترجمه یکی از قطعات ژان ژاک روسو
---
# شمارهٔ ۱۲۹ - ترجمه یکی از قطعات ژان ژاک روسو

<div class="b" id="bn1"><div class="m1"><p>چون سرابند سفلگان از دور</p></div>
<div class="m2"><p>که نمایند بحرهای علوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچه نزدیک‌تر شوی سویشان</p></div>
<div class="m2"><p>لاجرم بیشتر شوی محروم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رادمردان ز دور همچون کوه</p></div>
<div class="m2"><p>ناپدیدند و قدرشان مکتوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سویشان هرچه می‌شوی نزدیک</p></div>
<div class="m2"><p>قدرشان بیشتر شود معلوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نجومت به چشم خرد آیند</p></div>
<div class="m2"><p>گنه از چشم تو است نی ز نجوم</p></div></div>