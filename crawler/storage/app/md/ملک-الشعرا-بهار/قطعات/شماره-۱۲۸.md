---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>ز خوبرویان بر من همی گذشت ستم</p></div>
<div class="m2"><p>از آن زمان که پدر برد درد بستانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کام من شد از آن روزگار، تلخی عشق</p></div>
<div class="m2"><p>که برد مادر در کام تلخ پستانم</p></div></div>