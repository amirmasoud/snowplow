---
title: >-
    شمارهٔ ۸۵ - فیض شمال‌
---
# شمارهٔ ۸۵ - فیض شمال‌

<div class="b" id="bn1"><div class="m1"><p>ز البرز بزرگ در شمال ری</p></div>
<div class="m2"><p>هر شب دم دلکش شمال آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از باد شمال مشکبو هر دم</p></div>
<div class="m2"><p>جان‌ رقصد و دل‌ به‌ وجد و حال‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وز عطر خوش گل و ریاحینش</p></div>
<div class="m2"><p>آفات سموم را زوال آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برفش بگدازد و به شهر اندر</p></div>
<div class="m2"><p>بس چشمه دلکش زلال آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امشب ز نسیم‌، سخت خشنودم</p></div>
<div class="m2"><p>کز سوی شمال بی‌ملال آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جنبد به جنوب از شمال آسان</p></div>
<div class="m2"><p>و آزاد به بزم اهل حال آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در محفل ما هوای جانبخشش</p></div>
<div class="m2"><p>با روح به فعل وانفعال آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همراه شمال جانفزا زی ما</p></div>
<div class="m2"><p>پیوسته قوافل کمال آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من رشک برم بدو چو از شوخی</p></div>
<div class="m2"><p>با طرهٔ یار در جدال آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گاهی صف چپ ازو برآشوبد</p></div>
<div class="m2"><p>گه درصف راست اختلال آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آشوب فتد به زلف یار اما</p></div>
<div class="m2"><p>این فتنه مؤید جمال آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باری نکنم نهان که سوی ما</p></div>
<div class="m2"><p>هر فیض که آید ازشمال آید</p></div></div>