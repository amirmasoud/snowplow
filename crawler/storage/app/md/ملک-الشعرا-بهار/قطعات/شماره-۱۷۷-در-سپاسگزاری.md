---
title: >-
    شمارهٔ ۱۷۷ - در سپاسگزاری
---
# شمارهٔ ۱۷۷ - در سپاسگزاری

<div class="b" id="bn1"><div class="m1"><p>ابوسعید که اوراست اختر مسعود</p></div>
<div class="m2"><p>به اوج عزت چون شمس تابناک و جلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسین اسم و حسن رسم آن که طینت او</p></div>
<div class="m2"><p>خود از ازل بسرشته است با ولای علی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جان اوست مرکب سعادت ابدی</p></div>
<div class="m2"><p>به ذات اوست مخمر شرافت ازلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدایگانا ای آن که در جمال وکمال</p></div>
<div class="m2"><p>به‌ عصر خویش کنون بی‌شبیه و بی‌بدلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فروغ دیدهٔ ملکی و دودهٔ شرفی</p></div>
<div class="m2"><p>چراغ دیدهٔ مجدی و دیدهٔ دولی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شنیده‌ام یکی از شاعران ستوده تو را</p></div>
<div class="m2"><p>که کارها همه را می کنی تو زبر جلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هماره کم‌محلی بایدت بدین اشخاص</p></div>
<div class="m2"><p>که نیست چارهٔ ایشان بغیر کم‌ محلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدایگانا از من بگو به آن شاعر</p></div>
<div class="m2"><p>که گفته بود: مر او را نه قیمی نه ولی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا ولی است ولی خدا و حجت عصر</p></div>
<div class="m2"><p>مراست قیم و قیوم‌، رب لم‌یزلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بعد لطف خدا و ائمهٔ اطهار</p></div>
<div class="m2"><p>مرا تو قبلهٔ امید و کعبهٔ املی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بلی اگر نظری باید از امام مرا</p></div>
<div class="m2"><p>به تو کنند حوالت که خالی از خللی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به‌حق خالق یکتا هرآن که خصم تو شد</p></div>
<div class="m2"><p>دو تا شود قدش از ضرب ذوالفقار علی</p></div></div>