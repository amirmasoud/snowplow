---
title: >-
    شمارهٔ ۱۴۱ - تاریخ موزه
---
# شمارهٔ ۱۴۱ - تاریخ موزه

<div class="b" id="bn1"><div class="m1"><p>در عهد شهنشاه جوانبخت رضا شاه</p></div>
<div class="m2"><p>کاز وی شده این کشور دیرینه گلستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخل فتن از پای درافتاد چو برخاست</p></div>
<div class="m2"><p>این شاه جوانبخت به پیرایش بستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون امن شد ایران به‌ره علم کمر بست</p></div>
<div class="m2"><p>دانشگه و دانشکده بگشود و دبستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانگاه بفرمود که دستور معارف</p></div>
<div class="m2"><p>ریزد ز پی موزه چنین نادره بنیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پهلوی و حکمت او هیچ عجب نیست</p></div>
<div class="m2"><p>کین کشور فرخنده شود روضهٔ رضوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>احسنت زهی موزه کز ایوان بلندش</p></div>
<div class="m2"><p>گشتست پر از ریگ حسد موزهٔ کیوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این موزه نماینده اعصار و قرونست</p></div>
<div class="m2"><p>ممتاز از این رو شد از امثال و ز اقران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گنجینهٔ ذوق است و هنرنامهٔ تاریخ</p></div>
<div class="m2"><p>آیینهٔ علمست و نمایندهٔ عرفان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهند ازین موزه به دریوزه تحف‌ها</p></div>
<div class="m2"><p>شاهان پی آرایش کاشانه و ایوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>القصه چو بنیاد شد این موزهٔ عالی</p></div>
<div class="m2"><p>کاز فرّ شه آباد بماناد به دوران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنوشت «‌بهار» از پی تاریخ بنایش</p></div>
<div class="m2"><p>«‌این موزهٔ عالی شود آرایش ایران‌»</p></div></div>