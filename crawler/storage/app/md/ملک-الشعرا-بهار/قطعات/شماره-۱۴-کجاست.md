---
title: >-
    شمارهٔ ۱۴ - کجاست‌؟
---
# شمارهٔ ۱۴ - کجاست‌؟

<div class="b" id="bn1"><div class="m1"><p>خارندگلبنان‌، چمنا پس گلت کجاست</p></div>
<div class="m2"><p>پر شد ز زاغ صحن چمن بلبلت کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>استنبلا! خلافت اسلامیت چه شد</p></div>
<div class="m2"><p>عثمانیا! جلالت استنبلت کجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون شد قلوب خلق زتهران وانقره</p></div>
<div class="m2"><p>ای شرع پاک مصطفوی‌!کابلت کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زد لطمه فیل هند به قرآن‌، محمدا!</p></div>
<div class="m2"><p>محمود شیر پرکنه زاولت کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایران خراب‌شد ز غزان‌،‌سنجرت چه‌شد</p></div>
<div class="m2"><p>تهران زکفر محو شد ار طغرلت کجاست</p></div></div>