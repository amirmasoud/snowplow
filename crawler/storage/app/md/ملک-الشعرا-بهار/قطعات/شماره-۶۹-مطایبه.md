---
title: >-
    شمارهٔ ۶۹ - مطایبه
---
# شمارهٔ ۶۹ - مطایبه

<div class="b" id="bn1"><div class="m1"><p>بهر بهار بازو وکون وکفل نماند</p></div>
<div class="m2"><p>کزسیل‌ «‌استرپ تومیسین» ‌دشت و تل نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم که خواستند رهی را عمل کنند</p></div>
<div class="m2"><p>باقی تنی بجا ز برای عمل نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باید خرید هرکرمی بیست سی فرانک</p></div>
<div class="m2"><p>بایع فرنگی است ر مجال جدل نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پولی که بود خرج عروسی سینه شد</p></div>
<div class="m2"><p>چیزی پی هزینه ماه عسل نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دولت فقیر و ما همه از او فقیرتر</p></div>
<div class="m2"><p>نقدی بجا ز غارت دزد و دغل نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکس برای خ‌بش کلاهی تهیه دید</p></div>
<div class="m2"><p>بهر حقیر جز سر سخت کچل نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاران به ملک و مال رسیدند و بهر ما</p></div>
<div class="m2"><p>جز زخم سینه حاصل سعی و عمل نماند</p></div></div>