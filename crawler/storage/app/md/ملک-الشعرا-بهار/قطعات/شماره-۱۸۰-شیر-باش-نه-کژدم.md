---
title: >-
    شمارهٔ ۱۸۰ - شیر باش نه کژدم
---
# شمارهٔ ۱۸۰ - شیر باش نه کژدم

<div class="b" id="bn1"><div class="m1"><p>تندی مکن که رشتهٔ چل ساله دوستی</p></div>
<div class="m2"><p>در حال بگسلد چو شود تند آدمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هموار و نرم باش که شیر درنده را</p></div>
<div class="m2"><p>زیر قلاده برد توان با ملایمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد اراده باش که دیوار آهنین</p></div>
<div class="m2"><p>چون نیم جو اراده‌، نباشد به محکمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رمز است‌ هرچه ‌هست و ‌حقیقت‌ جز این ‌دو نیست</p></div>
<div class="m2"><p>ای نور چشم این دو بود عین مردمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا راه خیر خلق سپردن به حسن خلق</p></div>
<div class="m2"><p>یا راه خیر خویش سپردن به خُرّمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور زان که همت تو به آزار مردمست</p></div>
<div class="m2"><p>شیری به هر طریق نکوتر ز کژدمی</p></div></div>