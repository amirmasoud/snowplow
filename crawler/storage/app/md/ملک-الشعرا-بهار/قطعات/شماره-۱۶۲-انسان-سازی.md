---
title: >-
    شمارهٔ ۱۶۲ - انسان سازی
---
# شمارهٔ ۱۶۲ - انسان سازی

<div class="b" id="bn1"><div class="m1"><p>مرا درست به یاد اندرست عهد صبی</p></div>
<div class="m2"><p>به روزگار لطیف تفرج و بازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتاد پارهٔ مومی ز دامن دایه</p></div>
<div class="m2"><p>من آن ربودم و جستم چو آهو از تازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو سنگ بودم درآغاز و نرم گشت آخر</p></div>
<div class="m2"><p>گهی ز فرط فشردن گهی ز دمسازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از او بساختم امثال مار و موش و وزغ</p></div>
<div class="m2"><p>به‌حجره چیدمشان چون بساط خرازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پدر درآمد و دید آن صنایع از فرزند</p></div>
<div class="m2"><p>بگفت زه‌! که درین پیشه فرد ممتازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نصیحتی است مگر بشنوی وگیری یاد</p></div>
<div class="m2"><p>کازین سپس بجزاز نیکویی نیاغازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دست‌از تو و موم‌از تو و خیال‌از تست</p></div>
<div class="m2"><p>به جای پیکر انسان چرا وزغ سازی‌؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایاکسی که زمام امور درکف تواست</p></div>
<div class="m2"><p>به حال خلق سزد بیش از این بپردازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسان شیشهٔ عکسند مردم ایران</p></div>
<div class="m2"><p>که هر نگارکه خواهی بر آن بیندازی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو موم تابع دست تواند کایشان را</p></div>
<div class="m2"><p>به ذوق خویش بسازی و باز بگذاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو مار و موش بسازی‌زخلق‌وگیری خشم</p></div>
<div class="m2"><p>که‌موش و مار شد این خلق اینت ناسازی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو پاکباش و ازبن موم شکل پاکان ساز</p></div>
<div class="m2"><p>که با تو از سر پاکی کنند انبازی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندانی از چه به گرد بساط عالی تواست</p></div>
<div class="m2"><p>فریب و دزدی و جبن و فساد و غمازی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرا نشسته گروهی مخنث و بیدین</p></div>
<div class="m2"><p>به جای مردم دیندار صفدر و غازی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چرا بزرگ‌ترین چاکران توگیرند</p></div>
<div class="m2"><p>طریق کید و نفاق و فسوس و طنازی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چرا ستند امیران و خواجگان درت</p></div>
<div class="m2"><p>ازین حریص گدایان پست یک غازی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مثل بودکه چو شد مرد خانه دنبک‌زن</p></div>
<div class="m2"><p>زکودکان نه عجب گرکنند پابازی </p></div></div>