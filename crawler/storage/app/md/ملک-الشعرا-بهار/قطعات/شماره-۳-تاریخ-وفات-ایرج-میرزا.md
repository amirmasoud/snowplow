---
title: >-
    شمارهٔ ۳ - تاریخ وفات ایرج میرزا
---
# شمارهٔ ۳ - تاریخ وفات ایرج میرزا

<div class="b" id="bn1"><div class="m1"><p>سکته کرد و مرد ایرج میرزا</p></div>
<div class="m2"><p>قلب ما افسرد ایرج میرزا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود مانند می صاف طهور</p></div>
<div class="m2"><p>خالی از هر درد ایرج میرزا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سعدی ای ‌نو بود و چون سعدی‌ به ‌دهر</p></div>
<div class="m2"><p>شعر نو آورد ایرج میرزا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دل یاران به اشعار لطیف</p></div>
<div class="m2"><p>زنگ غم بسترد ایرج میرزا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دائما در شادی یاران خویش</p></div>
<div class="m2"><p>پای می‌افشرد ایرج میرزا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برخلاف آخر ز مرگ خویشتن</p></div>
<div class="m2"><p>خلق را آزرد ایرج میرزا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دریغا کانچه را آورده بود</p></div>
<div class="m2"><p>رفت و با خود برد ایرج میرزا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گورکن فضل و ادب را گل گرفت</p></div>
<div class="m2"><p>چون به گل بسپرد ایرج میرزا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سکته کرد و از پس پنجاه و پنج</p></div>
<div class="m2"><p>لحظه‌ای نشمرد ایرج میرزا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرد آسان لیک مشکل کردکار</p></div>
<div class="m2"><p>بر بزرگ و خرد ایرج میرزا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت بهر سال تاریخش بهار:</p></div>
<div class="m2"><p>وه چه راحت مرد ایرج میرزا</p></div></div>