---
title: >-
    شمارهٔ ۶۶ - بهار و تیمورتاش‌
---
# شمارهٔ ۶۶ - بهار و تیمورتاش‌

<div class="b" id="bn1"><div class="m1"><p>صدر اعظم حضرت تیمورتاش</p></div>
<div class="m2"><p>بشنود یک نکته از این مستمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق‌صحبت‌هست‌حقی‌معتبر</p></div>
<div class="m2"><p>بود می‌باید بدین حق پای‌بند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده‌را با خواجه حق صحبت است</p></div>
<div class="m2"><p>صحبتی دیرینه و بی‌زرق و فند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوست در سختی بباید پایمرد</p></div>
<div class="m2"><p>واندر این معنی روایاتی است چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود تو دانی بوده‌ام در این دو سال</p></div>
<div class="m2"><p>پایکوب انزوا و حبس و بند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه به چنگ شحنگانی دیوخوی</p></div>
<div class="m2"><p>گه اسیر ناکسانی خودپسند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جاهلان خشنود و من مانده غمی</p></div>
<div class="m2"><p>ناکسان برکار و من مانده نژند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ورنه بر هنجار بودم پیش از این</p></div>
<div class="m2"><p>یافتم زین انزوا و بند پند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فکر من دعوی آزادی گذاشت</p></div>
<div class="m2"><p>کلک‌ من شمشیر حریت فکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مردی و آزادگی در طبع من</p></div>
<div class="m2"><p>چون‌ زنان افکند بر رخ روی بند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرگ و پیری همچو گرک گرسنه</p></div>
<div class="m2"><p>می‌زند هر دم به رویم زهرخند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>محنت و تیمار مشتی کودکان</p></div>
<div class="m2"><p>بر دلم پیکان زهرآگین فکند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روزگارم دست استغنا ببست</p></div>
<div class="m2"><p>آسمانم ریشهٔ مردی بکند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قصه کوته‌، بین چه گوید بنت کعب</p></div>
<div class="m2"><p>قطعه‌ای چون همت صوفی بلند:</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>«‌عاشقی خواهی که تا پایان بری</p></div>
<div class="m2"><p>بس که بپسندید باید ناپسند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زشت باید دید و انگارید خوب</p></div>
<div class="m2"><p>زهر باید خورد و انگارید قند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>توسنی کردم ندانستم همی</p></div>
<div class="m2"><p>کز کشیدن سخت‌تر گردد کمند»</p></div></div>