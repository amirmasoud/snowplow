---
title: >-
    شمارهٔ ۸۹ - تسلیت
---
# شمارهٔ ۸۹ - تسلیت

<div class="b" id="bn1"><div class="m1"><p>خواجهٔ فرخ سیر محمد دانش</p></div>
<div class="m2"><p>ای که سخن گستری و دانش‌پرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نثر تو چون بر صحیفه خامهٔ بهزاد</p></div>
<div class="m2"><p>نظم تو چون در قنینه بادهٔ خلر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تو ندیدم سخنوری به ‌فصاحت</p></div>
<div class="m2"><p>دیده و سنجیده‌ام هزار سخنور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همسر رنجم از آن که خاطر پاکت</p></div>
<div class="m2"><p>رنجه شد از مرگ ناگهانی همسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود معزای آن کریمهٔ مغفور</p></div>
<div class="m2"><p>پر ز خلایق بسان عرصهٔ محشر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه و دریغا که من در آن شب و آن روز</p></div>
<div class="m2"><p>بودم رنجور و اوفتاده به بستر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاش که سر برنکردمی و ندیدی</p></div>
<div class="m2"><p>خاطر آن خواجه را ملول و مکدر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیدن یاران خوشست لیک به‌ شادی</p></div>
<div class="m2"><p>نه بغمان کرده هر دو گونه معصفر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار قضا بود شاد زی و مخور غم</p></div>
<div class="m2"><p>هل که بداندیش تو بود به‌ غم اندر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بزم بیارای از دو آتش سوزان</p></div>
<div class="m2"><p>ایدون کاندر رسید لشکر آذر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن‌ یک در مرزغن چو گونهٔ معشوق</p></div>
<div class="m2"><p>وان دگر اندر بلور چون لب دلبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زخمه شهنازی و نوای قمر خواه</p></div>
<div class="m2"><p>وان سخنان کز بهار دارند از بر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گاه بنوشند و گاه پای بکوبند</p></div>
<div class="m2"><p>گاه ز بوسه دهند قند مکرر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت‌ حکیمی‌ جهان‌ سراسر وهم‌ است</p></div>
<div class="m2"><p>گفت آن دیگر که بودنی است سراسر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر همگی بودنی‌است غم نکند سود</p></div>
<div class="m2"><p>ور همه وهم‌است‌باز شادی خوش‌تر</p></div></div>