---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>دل موری میازار ار چه خرد است</p></div>
<div class="m2"><p>که خردک نالشی سازد تو را خرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوانمرگی است قسم مردم آزار</p></div>
<div class="m2"><p>اگر کنت ‌است اگر دوک است اگر لرد</p></div></div>