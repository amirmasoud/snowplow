---
title: >-
    شمارهٔ ۹۵ - مشت پس از جنگ
---
# شمارهٔ ۹۵ - مشت پس از جنگ

<div class="b" id="bn1"><div class="m1"><p>چون خصم قوی گشت از او دست نگهدار</p></div>
<div class="m2"><p>و آزرده مکن مشت گرامی به حجر بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذار که پیش آیدش از بخت فتوری</p></div>
<div class="m2"><p>آنگه‌ بکنش‌ پوست به‌ یک‌ لمح بصر بر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان پیش که بدخواه به تو چاشت گذارد</p></div>
<div class="m2"><p>بگذار بر او شام و ممان تا به سحر بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند که نادان را عقل از عقب آید</p></div>
<div class="m2"><p>آنگه که فرو ماند مسکین به خطر بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر مردم احمق چو رود سالی گوید</p></div>
<div class="m2"><p>من پار بدم احمق و ماندم به ضرر بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وین طرفه که هرسال نو این گفته شود نو</p></div>
<div class="m2"><p>تا بگذردش عمر به بوک و به مگر بر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرصت‌ مده‌ از دست‌ و نگه کن که‌ چه‌ خوش گفت</p></div>
<div class="m2"><p>آن مشت‌زن پیر به فرزانه پسر بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشتی که پس از جنگ فرا یاد تو آید</p></div>
<div class="m2"><p>باید زدن آن مشت ز تشویر بسر بر</p></div></div>