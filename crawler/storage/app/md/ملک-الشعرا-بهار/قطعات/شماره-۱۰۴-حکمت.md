---
title: >-
    شمارهٔ ۱۰۴ - حکمت
---
# شمارهٔ ۱۰۴ - حکمت

<div class="b" id="bn1"><div class="m1"><p>خواجه برفت و خفت به خاک وتو ز ابلهی</p></div>
<div class="m2"><p>در ماتمش به ناله و آه اندری هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزدود خاک تیره از او آب و رنگ و تو</p></div>
<div class="m2"><p>در جامه کبود و سیاه اندری هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگری برآن که رخت به منزل کشید و رفت</p></div>
<div class="m2"><p>بر خویشتن گری که به راه اندری هنوز</p></div></div>