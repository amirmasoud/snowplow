---
title: >-
    شمارهٔ ۷۰ - این هم نماند
---
# شمارهٔ ۷۰ - این هم نماند

<div class="b" id="bn1"><div class="m1"><p>نماند درد و درمان هم نماند</p></div>
<div class="m2"><p>نماند وصل و هجران هم نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهارا غم مخورکاندر زمانه</p></div>
<div class="m2"><p>نماند عیش و خذلان هم نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تهران در منال ازیاد استخر</p></div>
<div class="m2"><p>که رفت استخر وتهران هم نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود ایران بسی آباد و ویران</p></div>
<div class="m2"><p>همان آباد و وبران هم نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نپاید چین و ژاپون هم نپاید</p></div>
<div class="m2"><p>نماند روس و آلمان هم نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نماند انگلیسی خردمند</p></div>
<div class="m2"><p>همان هندوی نادان هم نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بمیرد مرغ و ماهی هم بمیرد</p></div>
<div class="m2"><p>نماند وحش و انسان هم نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرچه دیر ماند نام نیکو</p></div>
<div class="m2"><p>سرانجام ای پسر آن هم نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بتوفد پردهٔ این نجم ساکن</p></div>
<div class="m2"><p>زمین گرد گردان هم نماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر این افراشته سقف مرصع</p></div>
<div class="m2"><p>قنادیل فروزان هم نماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بجز یک‌ذات کاصل کاینات است</p></div>
<div class="m2"><p>صور و اسماء و اعیان هم نماند</p></div></div>
<div class="b2" id="bn12"><p>بد و خوب‌ جهان اندر زوال است</p>
<p>پس‌ این‌ جنگ و جدال ما خیال ‌است</p></div>