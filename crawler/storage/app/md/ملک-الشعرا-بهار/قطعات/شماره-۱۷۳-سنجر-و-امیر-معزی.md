---
title: >-
    شمارهٔ ۱۷۳ - سنجر و امیر معزی
---
# شمارهٔ ۱۷۳ - سنجر و امیر معزی

<div class="b" id="bn1"><div class="m1"><p>شنیده‌ای تو که سنجر به عمد یا به خطا</p></div>
<div class="m2"><p>بزد به سینه سر خیل شاعران تیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلی معزی کش بود در جگر پیکان</p></div>
<div class="m2"><p>نبست لب ز ثنا گرچه بود دلگیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نماند شاعر از آن زخم تا به سال دگر</p></div>
<div class="m2"><p>ولی بماند به سنجر بزرگ تشویری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*‌‌</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانه نیز مرا زد به سینه چندین تیر</p></div>
<div class="m2"><p>که نیست بهر علاجش به دست تدبیری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمانه گویم و اهل زمانه را خواهم</p></div>
<div class="m2"><p>زمانه را نبود قدرتی وتاثیری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>...............................</p></div>
<div class="m2"><p>...............................</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گذشت عهد جوانیم زیرپنجهٔ شاه</p></div>
<div class="m2"><p>چو زیر پنجهٔ شیری ضعیف نخجیری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>...............................</p></div>
<div class="m2"><p>...............................</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بجای آنکه نهد زخم کهنه را مرهم</p></div>
<div class="m2"><p>ز زخم‌های نو انگیخت خشم و تکدیری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به بینوایی و حرمان من نشد خرسند</p></div>
<div class="m2"><p>ولیک نوع ستم‌هاش یافت توفیری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز درد و رنج کمان شد قدم بسان کسی</p></div>
<div class="m2"><p>که بسته آرزوی خوبش بر پر تیری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گماشت بر من و بر عرض‌ من‌ سفیهی چند</p></div>
<div class="m2"><p>ازین دروغ‌زنی‌، فاسقی‌، زبونگیری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نبشته این پی رسواییم مقالاتی</p></div>
<div class="m2"><p>کشیده آن پی بدنامیم تصاویری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرفته سیم و زر از... و کرده هجو بهار</p></div>
<div class="m2"><p>هجای گنده‌تر از گندنایی و سیری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>علو قدر مرا اینت برترین برهان</p></div>
<div class="m2"><p>که‌... راست ز من وحشتی و تشویری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>...............................</p></div>
<div class="m2"><p>...............................</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر چه‌ زخم زبان مولم است‌، لیک خوشم</p></div>
<div class="m2"><p>از آنکه نیست د‌رین ترهات تاثیری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا که دامان از آفتاب پاک‌تر است</p></div>
<div class="m2"><p>سیاه رو نکند تهمتی و تکفیری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کسی که شصت بهارش گذشت کج نکند</p></div>
<div class="m2"><p>رهش‌، نه سردی مهری نه گرمی تیری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز عهد باز نگردم ز خوف دشنامی</p></div>
<div class="m2"><p>ز گفته دست نشویم به سوء تعبیری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به ترک دوست نگویم به هیچ تهدیدی</p></div>
<div class="m2"><p>به راه غیر نپویم به هیچ تحذیری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به پیشگاه جلال خدا معاذالله</p></div>
<div class="m2"><p>نکرده‌ام گنهی کآوردم معاذیری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه زبن حسودان در آرزوی تحسینی</p></div>
<div class="m2"><p>نه زین عوانان در انتظار تقدیری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به‌رغم سنت دیرین و راه و رسم مهان</p></div>
<div class="m2"><p>نداشت‌..... حرمت چو من پیری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>.. ... .. ... ...... .. .. ...... ..</p></div>
<div class="m2"><p>..............................</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر آنچه پند بدادم نداشت آثاری</p></div>
<div class="m2"><p>هرآنچه موعظه کردم نکرد تاثیری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بسی حقایق گفتم‌، ولیک در بر...</p></div>
<div class="m2"><p>نبود یکسره جز حیلتی و تزویری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه‌راست گفت و نه گفتار بنده داشت به‌راست</p></div>
<div class="m2"><p>جز آن که شد تلف از عمر ما مقادیری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به ماه بهمن گفتم یکی قصیده که بود</p></div>
<div class="m2"><p>ز راه و رسم بزرگی‌، بزرگ تصویری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر آن سخن‌ها بر سنگ خاره گشتی نقش</p></div>
<div class="m2"><p>شدی ز سنگ عیان پاسخی و تقریری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گمانم آن که برنجید نیز از آن سخنان</p></div>
<div class="m2"><p>چو بود منتظر مدحتی ز نحریری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی نگفت بهار است شهره در فن خویش</p></div>
<div class="m2"><p>چنان که هست بهرکشوری مشاهیری</p></div></div>