---
title: >-
    شمارهٔ ۹ - قدرت روح
---
# شمارهٔ ۹ - قدرت روح

<div class="b" id="bn1"><div class="m1"><p>رفیقی داشتم بل اوستادی</p></div>
<div class="m2"><p>که‌صرف ‌صحبتش می گشت اوقات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علوم روح را تدریس می کرد</p></div>
<div class="m2"><p>برین سرگشتهٔ جهل و خرافات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهم دادیم قولی صادقانه</p></div>
<div class="m2"><p>که از ما هرکه گردد زودتر مات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب هفتم رفیق خویشتن را</p></div>
<div class="m2"><p>کند در عالم رویا ملاقات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگوید شمه‌ای از عالم روح</p></div>
<div class="m2"><p>ز راه و رسم پاداش و مجازات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قضا را دوست پیشی جست از من</p></div>
<div class="m2"><p>به مینو رخت بربست از خرابات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب هفتم به خواب من درآمد</p></div>
<div class="m2"><p>گرفتم دستش از روی مصافات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفتم چیست آنجا حال وما را</p></div>
<div class="m2"><p>چه بایست از عبادات و ریاضات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفت اینجا بود روح عوالم</p></div>
<div class="m2"><p>نه شیادی بکار آید نه طامات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حجاب‌صورت‌اینجا برگرفته است</p></div>
<div class="m2"><p>نباشد چشم‌پوشی و مماشات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیاید احتیالات از ریاکار</p></div>
<div class="m2"><p>نگیرد بر جوانمرد اتهامات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نشاید سفله‌ای را خواند حاتم</p></div>
<div class="m2"><p>نشاید احمقی را خواند سقرات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صفات اینجا تبرز جسته در روح</p></div>
<div class="m2"><p>عیوب اینجا تجسم جسته بالذات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان کانجا مساواتی نباشد</p></div>
<div class="m2"><p>در اینجا هم نمی‌باشد مساوات</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تفاوت‌های هول‌انگیز ارواح</p></div>
<div class="m2"><p>کند بیننده را در هر نظر مات</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود جان یکی ردف خراطین</p></div>
<div class="m2"><p>بود روح یکی جفت سموات</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>توانایی روح اینجا بکار است</p></div>
<div class="m2"><p>شود این برتری تنها مراعات</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو روحی مقتدر آید شتابند</p></div>
<div class="m2"><p>به استقبال وی ارواح اموات</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به اوج لامکانش برنشانند</p></div>
<div class="m2"><p>به‌سر بر تاجی از فخر و مباهات</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مکان و مدت اینجا بالاراده است</p></div>
<div class="m2"><p>نه‌ میعادی‌ است ‌محسوس و نه میقات</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگفتم قدرت روح از چه خیزد</p></div>
<div class="m2"><p>بفرما تاکنم جبران مافات</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جوابم گفت یک جو رحم و انصاف</p></div>
<div class="m2"><p>به است از سال‌ها ذکر و مناجات</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>محبت کن‌، مروت کن‌، کرم کن</p></div>
<div class="m2"><p>به انسان و به حیوان و نباتات</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چراکاین هر سه ذی روحند بی‌شک</p></div>
<div class="m2"><p>فرستد روحشان سوی تو سوقات</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو بر افتاده‌ای رحمی نمایی</p></div>
<div class="m2"><p>سروری در نهادت گردد اثبات</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همانا آن‌ خوشی‌ سوقات روح است</p></div>
<div class="m2"><p>که بخشندت به عنوان مکافات</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدی را همچنان پاداش باشد</p></div>
<div class="m2"><p>که از امروز نگذارد به فردات</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ترحم کن به مخلوق خداوند</p></div>
<div class="m2"><p>که ‌قوت روح رحم است و مواسات</p></div></div>