---
title: >-
    شمارهٔ ۸۳ - پند پدر
---
# شمارهٔ ۸۳ - پند پدر

<div class="b" id="bn1"><div class="m1"><p>آن که کمتر شنید پند پدر</p></div>
<div class="m2"><p>روزگارش زیاده پند دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان که را روزگار پند نداد</p></div>
<div class="m2"><p>تیغ زهر آبداده پند دهد</p></div></div>