---
title: >-
    شمارهٔ ۱۳۱ - در پیشگاه آستان قدس رضوی
---
# شمارهٔ ۱۳۱ - در پیشگاه آستان قدس رضوی

<div class="b" id="bn1"><div class="m1"><p>تبارک الله از این فرخ آستان که بود</p></div>
<div class="m2"><p>به پاس درگه او آسمان همیشه مقیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حریم زادهٔ موسی که چون دم عیسی</p></div>
<div class="m2"><p>روان فزاید خاک درش به عظم رمیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشم زایر این آستان بود روشن</p></div>
<div class="m2"><p>هرآنچه گشت به سینا نهان ز چشم کلیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به است فرش ره او ز مرغزار بهشت</p></div>
<div class="m2"><p>چنان که خاک در او زکوثر و تسنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چراست پشت سپهر این چنین خمیده و گوژ</p></div>
<div class="m2"><p>اگر ندارد پیش درش سر تعظیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهی بر آن که نهد روی دل بر این درگاه</p></div>
<div class="m2"><p>برای صافی و دین درست و قلب سلیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان که خادم این در، بهار مدح‌سرای</p></div>
<div class="m2"><p>که هست بندهٔ دیرین و خاکسار قدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کمینه چاکر این آستان که از ره عجز</p></div>
<div class="m2"><p>نهاده است به کوی رضا سر تسلیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر ستاند روزی ز خاک این درگاه</p></div>
<div class="m2"><p>دوای جان علیل و شفای قلب سقیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز پاک یزدان بادا دمی هزار درود</p></div>
<div class="m2"><p>بر این‌ حریم و خداوند این‌ خجسته حریم</p></div></div>