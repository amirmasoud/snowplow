---
title: >-
    فقرۀ ۹۹
---
# فقرۀ ۹۹

<div class="n" id="bn1"><p>شب‌خیز باش که کار روا باشی.</p></div>
<div class="b" id="bn2"><div class="m1"><p>به تاریکی از خواب بیدار شو</p></div>
<div class="m2"><p>به نام خدا بر سر کار شو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که شب‌خیز را کار باشد روا</p></div>
<div class="m2"><p>فزون خواب مردم شود بینوا</p></div></div>