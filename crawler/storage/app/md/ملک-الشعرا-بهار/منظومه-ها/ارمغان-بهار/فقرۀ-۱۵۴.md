---
title: >-
    فقرۀ ۱۵۴
---
# فقرۀ ۱۵۴

<div class="n" id="bn1"><p>و ترا گویم ای پسر که خرد به مردم بهترین دهشیاری (‌یعنی بهترین بخش و توفیق‌) است‌.</p></div>
<div class="b" id="bn2"><div class="m1"><p>ترا گویم ای پور فرخنده‌پی</p></div>
<div class="m2"><p>خرد جوی تا کام یابی ز وی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که مردم دهشیار را در جهان</p></div>
<div class="m2"><p>خرد از دهش‌ها به اندر نهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که خود زان خردکامکاری کند</p></div>
<div class="m2"><p>به دیگر کسان نیز یاری کند</p></div></div>