---
title: >-
    فقرۀ ۱۰۲
---
# فقرۀ ۱۰۲

<div class="n" id="bn1"><p>به یزدان آفرین کن و دل به رامش دار کت از یزدان فزایش به نیکویی رسد.</p></div>
<div class="b" id="bn2"><div class="m1"><p>به یزدان نخست آفرین بر شمار</p></div>
<div class="m2"><p>پس آنگاه دل را به رامش سپار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کت افزایش آید ز یزدان پاک</p></div>
<div class="m2"><p>ز رامش نگردد دلت دردناک</p></div></div>