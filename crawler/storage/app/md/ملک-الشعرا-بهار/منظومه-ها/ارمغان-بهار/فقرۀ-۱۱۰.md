---
title: >-
    فقرۀ ۱۱۰
---
# فقرۀ ۱۱۰

<div class="n" id="bn1"><p>نیک مرد آساید و بد مرد بیش و اندوه گران بود.</p></div>
<div class="b" id="bn2"><div class="m1"><p>نکو مرد آساید اندر جهان</p></div>
<div class="m2"><p>برد بدکنش مرد رنج گران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکویی بود جوشن نیکمرد</p></div>
<div class="m2"><p>به گرد بدی تا توانی مگرد</p></div></div>