---
title: >-
    فقرۀ ۹۶
---
# فقرۀ ۹۶

<div class="n" id="bn1"><p>سخن دوآیینه (‌به دورویی و تذبذب‌) مگوی.</p></div>
<div class="b" id="bn2"><div class="m1"><p>سخن هیچگه بر دو آیین مگوی</p></div>
<div class="m2"><p>که نزد مهال ریزدت آبروی</p></div></div>