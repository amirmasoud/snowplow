---
title: >-
    فقرۀ ۱
---
# فقرۀ ۱

<div class="n" id="bn1"><p>این‌پیدا (‌است‌) که‌آذرپاد را فرزند تنی‌زاد نبود، و از آن پس آیستان «‌نیاز و دعا» به یزدان کرد، دیر برنیامد که اذرپاد را فرزندی ببود، هرآینه درست خیمی زرتشت سپینمان را زرتشت نام نهاد، وکفت که برخیز پسرم تا(‌ت‌) فرهنگ برآموزم‌.</p></div>
<div class="b" id="bn2"><div class="m1"><p>شنیدم که دانا نبودش پسر</p></div>
<div class="m2"><p>بنالید زی داور دادگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به‌زودی یکی خوب فرزند یافت</p></div>
<div class="m2"><p>یکی خوب فرزند دلبند یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفرمود «‌زرتشت‌» نامش پدر</p></div>
<div class="m2"><p>مگرخیم زرتشت گیرد پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو هنگام فرهنگش آمد فراز</p></div>
<div class="m2"><p>بدین گونه فرهنگ او کرد ساز</p></div></div>