---
title: >-
    اینک منظومهٔ سی ‌روزهٔ آذ‌ر پاد مارسپندان
---
# اینک منظومهٔ سی ‌روزهٔ آذ‌ر پاد مارسپندان

<div class="b" id="bn1"><div class="m1"><p>بود ماه سی روزتا بنگری</p></div>
<div class="m2"><p>به هر روز کاری بجای آوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سزد گر به «‌هرمزد» باشی خُرم</p></div>
<div class="m2"><p>خوری می به آیین جمشید جم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به «‌بهمن‌» کنی جامه‌ها نوبرشت</p></div>
<div class="m2"><p>پرستش کنی روز «‌اردی‌بهشت‌»</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به «‌شهریور» اندر شوی شادخوار</p></div>
<div class="m2"><p>کنی در «‌سپندارمذ» کشت کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به «‌خورداد» جوی نوین کن روان</p></div>
<div class="m2"><p>به «‌مرداد» بیخ نو اندر نشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به «‌دی‌بآذر» اندر سر و تن بشوی</p></div>
<div class="m2"><p>بپیرای ناخن‌، بیارای موی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به «‌آذر» مپز نان که دارد گناه</p></div>
<div class="m2"><p>بدین روز نیکست رفتن به راه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به «‌آبان‌» بپرهیز از آب ای جوان</p></div>
<div class="m2"><p>میالای و مازار آب روان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به «‌خور روز» کودک به استاد ده</p></div>
<div class="m2"><p>که گردد دبیری خردمند و به</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخور باده با دوستان‌، روز «‌ماه‌»</p></div>
<div class="m2"><p>ز ماه خدای آمد کار خواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بفرمای برکودکان روز «‌تیر»</p></div>
<div class="m2"><p>نبرد و سواری و پرتاب تیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به « گوش‌» اندرون گاوساله به مرز</p></div>
<div class="m2"><p>ببند و بیاموز برگاو، ورز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بپیرای ناخن چو شد «‌دی‌بمهر»</p></div>
<div class="m2"><p>سر و تن بشوی و بیارای چهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جدا کن ز شاخ رز انگوررا</p></div>
<div class="m2"><p>بچرخشت افکن می سور را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر مستمندی زکس «‌مهر» روز</p></div>
<div class="m2"><p>شو اندر بر مهر گیتی‌ فروز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فشان اشک و زو دادخواهی نمای</p></div>
<div class="m2"><p>که داد تو گیرد ز دشمن خدای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به‌روز «‌سروش‌»‌ازخجسته سروش</p></div>
<div class="m2"><p>روان را و تن را توان خواه و توش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از او خواه آزادی کام خویش</p></div>
<div class="m2"><p>وزو جوی آیفت فرجام خویش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به «‌رشن‌» اندرون کار سنگین بنه</p></div>
<div class="m2"><p>روان را ز یاد خدا توشه ده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مخور هیچ سوگند در «‌فرودین‌»</p></div>
<div class="m2"><p>که زشتست ویژه به روزی چنین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ستای اندربن روز فروهر را</p></div>
<div class="m2"><p>که فرورد از او یافت این بهر را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نیایش کن امروز بر فروهر</p></div>
<div class="m2"><p>که پاکان شوند از تو خشنودتر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پی خانه افکن به «‌بهرام‌» روز</p></div>
<div class="m2"><p>سوی رزم شو گر تویی رزم‌توز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که پیروز بازآیی از کارزار</p></div>
<div class="m2"><p>همت کاخ و ایوان بود پایدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زن ار برد خواهی‌، ببر روز «‌رام‌»</p></div>
<div class="m2"><p>که‌ رامش‌ خوشست‌ اندرین‌ روز و کام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وگر باشدت کار با داوران</p></div>
<div class="m2"><p>درین روز رو تا شوی کامران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سزد روز «‌باد» ار درنگی شوی</p></div>
<div class="m2"><p>نپیوندی امروز کار از نوی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو روز نیایش بود «‌دی‌بدین‌»</p></div>
<div class="m2"><p>سر وتن بشو، ناخن و مو بچین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زن نو ببر جامهٔ نو بپوش</p></div>
<div class="m2"><p>دل از یاد یزدان پر و لب خموش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بود روز «دین‌» مرگ خرفستران</p></div>
<div class="m2"><p>بکش هرچه خرفسترست اندر آن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که خرفستران یار اهریمن‌اند</p></div>
<div class="m2"><p>دد و دام و با مردمان دشمن‌اند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به بازار شو روز «‌ارد» ای پسر</p></div>
<div class="m2"><p>نوا نو بخر چیز و با خانه بر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در «‌اشتاد» روز اسب و گاو و ستور</p></div>
<div class="m2"><p>به گشن افگنی مایه گیرند و زور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ره دورگیر «‌آسمان‌» روز، پیش</p></div>
<div class="m2"><p>که بازآیی آسان سوی خان خویش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گرت خوردن دارو افتد بسر</p></div>
<div class="m2"><p>به «‌زمیاد» روز ایچ دارو مخور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زن تازه در «‌ماراسفند» گیر</p></div>
<div class="m2"><p>که فرزند نیک آید و تیزویر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>درین روز جامه بیفزای بر</p></div>
<div class="m2"><p>بدوز و بپوش و بیارای بر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>«‌انیران‌» بود نیک زن خواستن</p></div>
<div class="m2"><p>همان ناخن و موی پیراستن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زنی کاندربن روز گیری به ‌بر</p></div>
<div class="m2"><p>شود کودکش در جهان نامور</p></div></div>
<div class="n" id="bn40"><p>انوشه روان باد آذرپاد مارسپندان‌، که این اندرز کرد و نیز این فرمان داد.</p></div>
<div class="b" id="bn41"><div class="m1"><p>انوشه روان باد آن مرد راد</p></div>
<div class="m2"><p>که این گفت‌ها گفت و این پند داد</p></div></div>
<div class="n" id="bn42"><p>پایان</p></div>