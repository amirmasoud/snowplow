---
title: >-
    فقرۀ ۶۰
---
# فقرۀ ۶۰

<div class="n" id="bn1"><p>راستگوی مرد، پیامبرکن‌.</p></div>
<div class="b" id="bn2"><div class="m1"><p>بجو راستگو مرد، پیغامبر</p></div>
<div class="m2"><p>کجا راست آید پیامت بسر</p></div></div>