---
title: >-
    فقرۀ ۱۶
---
# فقرۀ ۱۶

<div class="n" id="bn1"><p>به هیچ کس افسوس (‌استهزاء‌) مکن.</p></div>
<div class="b" id="bn2"><div class="m1"><p>مکن هیچ افسوس با مردمان</p></div>
<div class="m2"><p>کز افسوسیانند مردم رمان</p></div></div>