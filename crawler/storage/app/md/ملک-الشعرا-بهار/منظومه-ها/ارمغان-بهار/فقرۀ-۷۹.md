---
title: >-
    فقرۀ ۷۹
---
# فقرۀ ۷۹

<div class="n" id="bn1"><p>خوش بهر دین‌دوست باش که اهرو (‌اشو  مقدس‌) باشی</p></div>
<div class="b" id="bn2"><div class="m1"><p>ز دین دوستی آسمانی شوی</p></div>
<div class="m2"><p>ز داد و دهش جاودانی شوی</p></div></div>