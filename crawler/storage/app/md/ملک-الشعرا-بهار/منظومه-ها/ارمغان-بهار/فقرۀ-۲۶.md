---
title: >-
    فقرۀ ۲۶
---
# فقرۀ ۲۶

<div class="n" id="bn1"><p>از سیزک (‌خبرچین‌) و دروغ مرد سخن مشنو.</p></div>
<div class="b" id="bn2"><div class="m1"><p>مکن گوش هرگز به مرد دروغ</p></div>
<div class="m2"><p>که در گفته‌هایش نبینی فروغ</p></div></div>