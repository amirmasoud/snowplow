---
title: >-
    فقرۀ ۸۷
---
# فقرۀ ۸۷

<div class="n" id="bn1"><p>چون به‌ انجمن خواهی نشست نزدیک‌ مردم دژآگاه منشین که تو نیز دژآگاه نباشی.</p></div>
<div class="b" id="bn2"><div class="m1"><p>به هر انجمن پاک و پدرام باش</p></div>
<div class="m2"><p>پژوهنده و چست و آرام باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خواهی نشستن پژوهنده شو</p></div>
<div class="m2"><p>به نزدیک مردان داننده شو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سوی دژ آگاه مردم مرو</p></div>
<div class="m2"><p>بپرهیز و همدوش نادان مشو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مبادا چو بینند آنجا ترا</p></div>
<div class="m2"><p>شمارند همباز آنها ترا</p></div></div>