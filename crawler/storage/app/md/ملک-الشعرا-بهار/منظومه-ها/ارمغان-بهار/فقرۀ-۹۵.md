---
title: >-
    فقرۀ ۹۵
---
# فقرۀ ۹۵

<div class="n" id="bn1"><p>شرم و ننگ بدرا، روان خویش به دوزخ مسپار.</p></div>
<div class="b" id="bn2"><div class="m1"><p>مکن شرم بیجا و بیجا درنگ</p></div>
<div class="m2"><p>به دوزخ مرو از پی نام و ننگ</p></div></div>