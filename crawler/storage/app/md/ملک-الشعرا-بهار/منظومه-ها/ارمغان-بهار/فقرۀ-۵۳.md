---
title: >-
    فقرۀ ۵۳
---
# فقرۀ ۵۳

<div class="n" id="bn1"><p>به مرد بی‌آیین هرآینه وام مده.</p></div>
<div class="b" id="bn2"><div class="m1"><p>به مرد بدآیین مده وام هیچ</p></div>
<div class="m2"><p>وگر وام خواهد ازو رخ بپیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه وام دادن ره داد پوی</p></div>
<div class="m2"><p>به آیین بده وام و بیشی مجوی</p></div></div>