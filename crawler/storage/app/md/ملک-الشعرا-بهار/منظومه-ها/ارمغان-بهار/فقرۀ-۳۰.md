---
title: >-
    فقرۀ ۳۰
---
# فقرۀ ۳۰

<div class="n" id="bn1"><p>گاه را مکوش‌.</p></div>
<div class="b" id="bn2"><div class="m1"><p>به بیگاه کوشش مکن بهر جاه</p></div>
<div class="m2"><p>که جاه است بسته به هنگام و گاه</p></div></div>