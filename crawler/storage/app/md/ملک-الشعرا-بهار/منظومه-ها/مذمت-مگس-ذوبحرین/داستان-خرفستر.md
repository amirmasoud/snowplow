---
title: >-
    داستان «‌خرفستر»
---
# داستان «‌خرفستر»

<div class="b" id="bn1"><div class="m1"><p>بشنوی ار گفتهٔ پیر مغان</p></div>
<div class="m2"><p>گیری ازین دیو چه آه و فغان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلقتش از دیو شد این‌ شوم ذات</p></div>
<div class="m2"><p>کشتن وی زان بود از واجبات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مؤبدی این قصهٔ خرفستران</p></div>
<div class="m2"><p>گوید و بس نکتهٔ حکمت در آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیک و مله کژدم و مار و مگس</p></div>
<div class="m2"><p>اشپش و زنبور و از این جنس بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساخته ز اندیشهٔ اهریمن‌اند</p></div>
<div class="m2"><p>مایهٔ آزردن مرد و زن‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز پی اجر من و تو در شمار</p></div>
<div class="m2"><p>داد بر این طایفه جان‌، کردگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وین مگس آمد سر اهریمنان</p></div>
<div class="m2"><p>خلقی از او بر سر و سینه‌زنان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عافیت از هیبت او در گریز</p></div>
<div class="m2"><p>شیر نر از صدمت او اشگریز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاجز از او آدمی و چارپا</p></div>
<div class="m2"><p>تیره از او مسکن و صحن سرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر بشر از زلزله فتاک‌تر</p></div>
<div class="m2"><p>وز سگ و گرک گله بی‌باک‌تر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون سگ دیوانه و چون گرگ و مار</p></div>
<div class="m2"><p>کشتن او فرض بر اهل دیار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وز سگ دیوانه و از مار و گرگ</p></div>
<div class="m2"><p>زحمتش افزون‌تر و هولش بزرگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در همه عمری سگ دیوانه‌ای</p></div>
<div class="m2"><p>بینی و ماری شده از لانه‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وین بتر از عقرب و مار و رطیل</p></div>
<div class="m2"><p>حمله‌ور آید سوی ما، خیل‌خیل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیشهٔ او کشتن اولاد ما است</p></div>
<div class="m2"><p>کشتن او فرض بر افراد ماست</p></div></div>