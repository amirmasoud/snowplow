---
title: >-
    دیدار سواری ز پیر زال در بیشه
---
# دیدار سواری ز پیر زال در بیشه

<div class="b" id="bn1"><div class="m1"><p>شیرمردی ز سواران دلیر</p></div>
<div class="m2"><p>که ‌بدی پیشهٔ او کشتن شیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پدر اندر پدرش گُرد و سوار</p></div>
<div class="m2"><p>همه دهقان‌منش وشیر شکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جعبه پر تیر و بزه کرده کمان</p></div>
<div class="m2"><p>به کمر خنجر و در مشت سنان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گام برداشت در آن بیشه خموش</p></div>
<div class="m2"><p>کامدش زمزمه‌ای نرم به گوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی‌بنهاد بدان‌صوت خفیف</p></div>
<div class="m2"><p>ناگهان پیر زنی دید نحیف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی آورده به درگاه خدا</p></div>
<div class="m2"><p>کند از مهر به فرزند دعا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت زالا به چه کار آمده‌ای‌؟</p></div>
<div class="m2"><p>اندرین بیشه مگرگم شده‌ای‌؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من بدین نیزه و این تیر و کمان</p></div>
<div class="m2"><p>اندرین بیشه نباشم به امان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازکجایی‌؟ زکجا آمده‌ای‌؟</p></div>
<div class="m2"><p>شب درین بیشه چرا آمده‌ای‌؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاندرین بیشه بغیر از من و شیر</p></div>
<div class="m2"><p>شب کسی پا ننهاده است‌، دلیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیرزن قصهٔ خود بازنمود</p></div>
<div class="m2"><p>شکوه از بخت بد آغاز نمود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پهلوان گفت بدان پیر عجوز</p></div>
<div class="m2"><p>که تو با این همه آزار هنوز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می کنی باز به درگا‌ه خدا</p></div>
<div class="m2"><p>به چنان ظالم غدار دعا؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ییر زن گفت بدوکای سره‌مرد</p></div>
<div class="m2"><p>گرد کار من و فرزند مگرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر میان من و او شد شکرآب</p></div>
<div class="m2"><p>تو مزن دست و مشوران دگر آب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که جوان است و جوان نادانست</p></div>
<div class="m2"><p>رنج او بر دل من آسان است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طالب شادی او بودم من</p></div>
<div class="m2"><p>پی دامادی او بودم من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون که داماد شد و یار گرفت</p></div>
<div class="m2"><p>چه زبانگر ز من آزارگرفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به‌خطایی که نبوده است به چیز</p></div>
<div class="m2"><p>نکشم دست ز فرزند عزیز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرچه دارم جگر از جورش ریش</p></div>
<div class="m2"><p>بد نخواهم به‌جگرگوشهٔ خویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرچه ناخن زنم اندر دل تنگ</p></div>
<div class="m2"><p>بجز این پرده ندارد آهنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پهلوان گفت به خویش ازسر درد</p></div>
<div class="m2"><p>لاف مردی چه زنی‌؟ اینک مرد!</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شیرمردان ز تو بودند فکار</p></div>
<div class="m2"><p>اینکت پیر زنی کرد شکار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نره شیر است و یا پیرزنست</p></div>
<div class="m2"><p>پیرزن نیست که این شیرزنست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باچنین‌قلب‌وچنین‌لطف‌وگذشت</p></div>
<div class="m2"><p>می‌توان‌بر دوجهان سلطان گشت</p></div></div>