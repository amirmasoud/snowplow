---
title: >-
    عروسی
---
# عروسی

<div class="b" id="bn1"><div class="m1"><p>خواستگار آمد و با رنج دراز</p></div>
<div class="m2"><p>خوانده‌ شد خطبه ‌و شد عقد فراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیمه کشت ازگل روبش گلشن</p></div>
<div class="m2"><p>ناقه کشتند و شد آتش روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان عروسی و از آن دامادی</p></div>
<div class="m2"><p>مادرش کرد فراوان شادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیک از آغاز، عروس بدخوی</p></div>
<div class="m2"><p>سر گران داشت بدان مادرشوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زال خندان به تماشای عروس</p></div>
<div class="m2"><p>آن جفاپیشه رخ از قهر عبوس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زال اگر رفتی و شیر آوردی</p></div>
<div class="m2"><p>دختر از قهر بر آن تف کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زال اگر آب کشیدی ز غدیر</p></div>
<div class="m2"><p>دختر آن آب فشاندی به کوبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زال نان پختی و خوان بنهادی</p></div>
<div class="m2"><p>دختر آن نان به ستوران دادی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پسر آوردی اگر صید ز راه</p></div>
<div class="m2"><p>متعفن شدی اندر خرگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان که گر زال زدی دست بر او</p></div>
<div class="m2"><p>دختر آن لقمه نبردی به گلو</p></div></div>