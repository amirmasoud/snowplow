---
title: >-
    بانگ هاتف
---
# بانگ هاتف

<div class="b" id="bn1"><div class="m1"><p>هاتفی گفت که ابرام بنه</p></div>
<div class="m2"><p>مادر است این‌، دلش آزار مده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چنین دل نبود با همه کس</p></div>
<div class="m2"><p>کاین دل مادر کان باشد و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بود هیچ دلی عرش خدا</p></div>
<div class="m2"><p>بود آن دل‌، دل مادر تنها</p></div></div>