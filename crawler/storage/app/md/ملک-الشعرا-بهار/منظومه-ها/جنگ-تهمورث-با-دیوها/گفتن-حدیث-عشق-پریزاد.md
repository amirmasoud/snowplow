---
title: >-
    گفتن حدیث عشق پریزاد
---
# گفتن حدیث عشق پریزاد

<div class="b" id="bn1"><div class="m1"><p>از پری بانو، رسولی ارجمند</p></div>
<div class="m2"><p>زی تو آید، ای شهنشاه بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیو زادی‌، گربزی‌، خودکامه‌ای</p></div>
<div class="m2"><p>هدیه‌ها آرد برت با نامه‌ای</p></div></div>
<div class="b2" id="bn3"><p>تا رهاند شه ز بند</p></div>
<div class="b" id="bn4"><div class="m1"><p>لیک بانو گویدت‌: بیدار باش</p></div>
<div class="m2"><p>من درین کارم تو هم برکار باش‌</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بند خود مگسل زپای شوی من</p></div>
<div class="m2"><p>تا مگرآن شوی ناخوش‌روی من</p></div></div>
<div class="b2" id="bn6"><p>گیرد از بند تو پند</p></div>
<div class="b" id="bn7"><div class="m1"><p>صرصرسوزان سموم قهر اوست</p></div>
<div class="m2"><p>آب دریا ناگوار از زهر اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وز دم سردش به صحرای شمال</p></div>
<div class="m2"><p>زندگانی شد ز برف و یخ وبال</p></div></div>
<div class="b2" id="bn9"><p>بس که کرد افسون و فند</p></div>
<div class="b" id="bn10"><div class="m1"><p>دشمن اردیبهشت و بهمن است</p></div>
<div class="m2"><p>خصم‌ هرمزد است‌ و خود اهریمن‌ است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از حسد اوکشت گاو ایوداد</p></div>
<div class="m2"><p>خورد از بیداد، کیومرث راد</p></div></div>
<div class="b2" id="bn12"><p>در زمین نکبت فکند</p></div>
<div class="b" id="bn13"><div class="m1"><p>کژدم و موش و وزغ، زنبور و گرگ</p></div>
<div class="m2"><p>موریانه‌، و اژدر و مار بزرگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اشپش‌ و ساس‌ و جُراد و کیک ‌و سِن</p></div>
<div class="m2"><p>پشه و مور و مگس‌، کرم عفن</p></div></div>
<div class="b2" id="bn15"><p>ساخت از بهرگزند</p></div>
<div class="b" id="bn16"><div class="m1"><p>پیش یزدان خودسری‌ها کرد او</p></div>
<div class="m2"><p>در جهان پتیاره‌ها آورد او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با جلال کبریایی دشمن است</p></div>
<div class="m2"><p>وز ازل با روشنایی دشمن است</p></div></div>
<div class="b2" id="bn18"><p>هست تاربکی پسند</p></div>
<div class="b" id="bn19"><div class="m1"><p>روز و شب دیو دروغش هم‌نشین</p></div>
<div class="m2"><p>همدمش دیو فریب و آز و کین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دیو جبن و کاهلی همراز او</p></div>
<div class="m2"><p>خواب و سستی روز و شب انباز او</p></div></div>
<div class="b2" id="bn21"><p>دشمن امشاسفند</p></div>
<div class="b" id="bn22"><div class="m1"><p>علم‌ و دستان و فسون و مکر و فن</p></div>
<div class="m2"><p>حکمت و استادی و دیگر سنن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کیمیا و هندسه‌، نقش و نگار</p></div>
<div class="m2"><p>انتظامات و حقوق بیشمار</p></div></div>
<div class="b2" id="bn24"><p>وین بناهای بلند</p></div>
<div class="b" id="bn25"><div class="m1"><p>جمله او آورد و او تدبیر کرد</p></div>
<div class="m2"><p>تا جوانان را ز محنت پیر کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کینه‌ و خودخواهی و فخر و غرور</p></div>
<div class="m2"><p>عجب و کبر و کشورآرایی و زور</p></div></div>
<div class="b2" id="bn27"><p>خنجر و تیر و کمند</p></div>
<div class="b" id="bn28"><div class="m1"><p>دشمن‌ سلم‌ و خضوع‌ و سادگی‌ است</p></div>
<div class="m2"><p>خصم بی‌آزاری و افتادگی است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دشمن‌ بی‌قیدی‌ و خرسندی است</p></div>
<div class="m2"><p>عاشق هوش و دها و رندی است</p></div></div>
<div class="b2" id="bn30"><p>مایل ترفند و فند</p></div>
<div class="b" id="bn31"><div class="m1"><p>فکر آزادی و عیاشی از اوست</p></div>
<div class="m2"><p>علم طراری و قلاشی از اوست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کینه‌توزی بازی پیوست اوست</p></div>
<div class="m2"><p>وین‌ ورق‌ همواره اندر دست اوست</p></div></div>
<div class="b2" id="bn33"><p>چون‌ حریص‌ آزمند</p></div>
<div class="b" id="bn34"><div class="m1"><p>ملک ایران ویژه از او شد خراب</p></div>
<div class="m2"><p>شد ز زهرش‌ بوستان‌هاتان‌ سراب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شد چراگاهان به پایش پی سپر</p></div>
<div class="m2"><p>راغ‌ها گشت از دمش زیر و زبر</p></div></div>
<div class="b2" id="bn36"><p>باغ‌ها از بیخ کند</p></div>
<div class="b" id="bn37"><div class="m1"><p>پیش از این اندر زمین‌، جن و پری</p></div>
<div class="m2"><p>با ملایک داشتندی همسری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>لطف حق ما را چراغ راه بود</p></div>
<div class="m2"><p>فقر و آسایش به ما همراه بود</p></div></div>
<div class="b2" id="bn39"><p>بی‌خبر از چون و چند</p></div>
<div class="b" id="bn40"><div class="m1"><p>فارغ از عجب و غرور و کبریا</p></div>
<div class="m2"><p>غافل از آزادی و کید و ریا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از جمال و زیب و زینت بی‌خبر</p></div>
<div class="m2"><p>دل تهی از حرص و غم‌های دگر</p></div></div>
<div class="b2" id="bn42"><p>چون به صحرا گوسفند</p></div>
<div class="b" id="bn43"><div class="m1"><p>اهرمن آورد بحث و ذوق و حال</p></div>
<div class="m2"><p>خط و شعر و منطق و علم‌الجمال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>علم کسب ثروت و فرماندهی</p></div>
<div class="m2"><p>شد به علم عشق‌بازی منتهی</p></div></div>
<div class="b2" id="bn45"><p>در جهان آتش فکند</p></div>
<div class="b" id="bn46"><div class="m1"><p>نورخورشید از سما او کرد دور</p></div>
<div class="m2"><p>نیمروزان شد از او تاری چو گور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همچنین‌ در باختر نیرنگ ساخت</p></div>
<div class="m2"><p>کوه‌ها از برف‌ و یخ‌ چون‌ سنگ ساخت</p></div></div>
<div class="b2" id="bn48"><p>بیخ آبادی بکند</p></div>
<div class="b" id="bn49"><div class="m1"><p>با زنان او گفت کآرایش کنید</p></div>
<div class="m2"><p>خوبش را در چشم مردان افکنید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مرد را او نطق و ذوق شعر داد</p></div>
<div class="m2"><p>در پیام و لابه‌اش کرد اوستاد</p></div></div>
<div class="b2" id="bn51"><p>تاکشد زن را به بند</p></div>
<div class="b" id="bn52"><div class="m1"><p>من ز اهریمن شدم زآن رو نفور</p></div>
<div class="m2"><p>بر تو دل بربسته‌ام از راه دور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>لیکن این دیوان که نزدیک منند</p></div>
<div class="m2"><p>جملگی بر سیرت اهریمنند</p></div></div>
<div class="b2" id="bn54"><p>کردشان باید نژند</p></div>
<div class="b" id="bn55"><div class="m1"><p>این دبیر من یکی پتیاره است</p></div>
<div class="m2"><p>صاحب‌ مکر و فریب‌ و چاره است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کوش تا او را فریبی در سخن</p></div>
<div class="m2"><p>و این‌ چنین پاسخ فرستی پیش من</p></div></div>
<div class="b2" id="bn57"><p>ای خدیو دیوبند!</p></div>