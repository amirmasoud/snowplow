---
title: >-
    رهنمون‌
---
# رهنمون‌

<div class="b" id="bn1"><div class="m1"><p>بود با اهریمنان دانش‌فزون</p></div>
<div class="m2"><p>پختن و معماری و رمی و فسون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط و رسم وپوشش و بافندگی</p></div>
<div class="m2"><p>پای کوبی‌، می کشی‌، خوانندگی</p></div></div>
<div class="b2" id="bn3"><p>با دگر علم و فنون</p></div>
<div class="b" id="bn4"><div class="m1"><p>چهر آنان سر بسر بی‌موی بود</p></div>
<div class="m2"><p>نسل‌ زیبا روی ناخوش خوی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرد و زن زیبا رخ و سیمینه‌تن</p></div>
<div class="m2"><p>زن چو مردان‌ساده‌ومردان‌چو زن</p></div></div>
<div class="b2" id="bn6"><p>جمله با مکر و فسون</p></div>
<div class="b" id="bn7"><div class="m1"><p>اصلشان‌افراشته‌، لیکن دیوخوی</p></div>
<div class="m2"><p>بیوفا طبع و هوسران و دوروی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تندحس و زود رنج و گرم‌جوش</p></div>
<div class="m2"><p>بی‌تفکر، کم‌خرد، بسیارهوش</p></div></div>
<div class="b2" id="bn9"><p>صبرکم، شهوت فزون</p></div>
<div class="b" id="bn10"><div class="m1"><p>حیله و حرص و دروغ و آز و کین</p></div>
<div class="m2"><p>مستی‌و شب گردی و قتل وکمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>احتکار و ارتشاء و اختلاس</p></div>
<div class="m2"><p>جنگ و دعوی‌ داری‌ و جبن‌ و هران</p></div></div>
<div class="b2" id="bn12"><p>رندی و رشک و جنون</p></div>
<div class="b" id="bn13"><div class="m1"><p>آدمیزادان فقیر و بردبار</p></div>
<div class="m2"><p>مهربان و ساده‌لوح و راستکار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرد و زن سرگرم کار وکسب نان</p></div>
<div class="m2"><p>روز در صحرا و شب در آشیان</p></div></div>
<div class="b2" id="bn15"><p>خوش دل و صافی‌ درون</p></div>
<div class="b" id="bn16"><div class="m1"><p>شغل آنان ورزش و برزیگری</p></div>
<div class="m2"><p>گاوداری و مواشی‌ پروری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خانه‌ هاشان‌ خیمه و غار و درخت</p></div>
<div class="m2"><p>کرده از چرم ددان انبان و رخت</p></div></div>
<div class="b2" id="bn18"><p>حربه‌شان سنگ و ستون</p></div>
<div class="b" id="bn19"><div class="m1"><p>جمله با هم‌، هم‌تبار و هم‌بنه</p></div>
<div class="m2"><p>یکدل و یکروی همچون آینه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در خورش انباز و درکوشش‌ رفیق</p></div>
<div class="m2"><p>پیر و برنا همدم ویار و شفیق</p></div></div>
<div class="b2" id="bn21"><p>از درون و از برون</p></div>
<div class="b" id="bn22"><div class="m1"><p> کرده بر هردوره پیری مهتری</p></div>
<div class="m2"><p>جسته خواهر با برادر همسری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر پسر کاو مهتر ابنا بُدی</p></div>
<div class="m2"><p>جانشین و وارث بابا شدی</p></div></div>
<div class="b2" id="bn24"><p>چون پدر گشتی نگون</p></div>
<div class="b" id="bn25"><div class="m1"><p>مهتی‌بن فرزند پیر اولین</p></div>
<div class="m2"><p>پادشا بودی بر اقوام کهین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مان و ویس و زند زیردست او</p></div>
<div class="m2"><p>جمله دهیو بسته و پابست او</p></div></div>
<div class="b2" id="bn27"><p>پیش دهیوپد زبون</p></div>
<div class="b" id="bn28"><div class="m1"><p>کوچکان محکوم پیر خانمان</p></div>
<div class="m2"><p>خانمان‌ها زیر حکم خاندان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خاندان‌ها تابع زندو بدند</p></div>
<div class="m2"><p>زندوان فرمانبر دهیو بدند</p></div></div>
<div class="b2" id="bn30"><p>شد به دهیو رهنمون</p></div>
<div class="b" id="bn31"><div class="m1"><p>ز انقلابات طبیعت‌، خیل خیل</p></div>
<div class="m2"><p>رعد و برق‌ و لرزه و طوفان‌ و سیل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قوم را گه بیم و گه امید بود</p></div>
<div class="m2"><p>تکیه‌گهشان آتش و خورشید بود</p></div></div>
<div class="b2" id="bn33"><p>وین سپهر نیلگون</p></div>
<div class="b" id="bn34"><div class="m1"><p>لشکری مرد و زن و برنا و پیر</p></div>
<div class="m2"><p>بر زمین هندوان شد جایگیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جنگ‌خونین هر طرف بالاگرفت</p></div>
<div class="m2"><p>سنگ‌ها درکاسهٔ سر جا گرفت</p></div></div>
<div class="b2" id="bn36"><p>ریخت از هرکاسه خون</p></div>