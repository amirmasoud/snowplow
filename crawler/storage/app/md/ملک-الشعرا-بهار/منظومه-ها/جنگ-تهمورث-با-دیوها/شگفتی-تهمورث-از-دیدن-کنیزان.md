---
title: >-
    شگفتی تهمورث از دیدن کنیزان
---
# شگفتی تهمورث از دیدن کنیزان

<div class="b" id="bn1"><div class="m1"><p>دید تهمورث چو بر آن دوکنیز</p></div>
<div class="m2"><p>گفت با شیدسپ کای پیر عزیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این دو دختر را جمالی بیمرست</p></div>
<div class="m2"><p>یا پری خود ز آدمی زیباتر است</p></div></div>
<div class="b2" id="bn3"><p>همچو من بنگر تو نیز</p></div>
<div class="b" id="bn4"><div class="m1"><p>گفت شیدسپ ای جهان را روشنی</p></div>
<div class="m2"><p>دور باش از فکرت اهریمنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این نگار و نقش دیو رهزن است</p></div>
<div class="m2"><p>و آب و رنگ خامهٔ اهریمن است</p></div></div>
<div class="b2" id="bn6"><p>در حقیقت نیست چیز</p></div>
<div class="b" id="bn7"><div class="m1"><p>نقش بیرون از فرشته یادگار</p></div>
<div class="m2"><p>وز درون دیوند و دیوی نابکار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این نکورویان تمامی از برون</p></div>
<div class="m2"><p>راست‌بالایند و زیبا، وز درون</p></div></div>
<div class="b2" id="bn9"><p>کج‌خیال و بی‌تمیز</p></div>
<div class="b" id="bn10"><div class="m1"><p>بانوان ما رفیق شوهرند</p></div>
<div class="m2"><p>عاشق و یار و شفیق شوهرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه لطفی نیست در دیدارشان</p></div>
<div class="m2"><p>بر سر لطف است و خوبی کارشان</p></div></div>
<div class="b2" id="bn12"><p>نزدشان شوهر عزیز</p></div>
<div class="b" id="bn13"><div class="m1"><p>وین پریرویان پریزادند و بس</p></div>
<div class="m2"><p>وز جمال ‌و حسن‌ خود شادند و بس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نزد ایشان پارسایی هیچ نیست</p></div>
<div class="m2"><p>کارشان ‌جز خودنمایی‌ هیچ نیست</p></div></div>
<div class="b2" id="bn15"><p>با دو زلف مشکبیز</p></div>
<div class="b" id="bn16"><div class="m1"><p>زین دو دلبر بهترند آن دو هیون</p></div>
<div class="m2"><p>زان که خوبند از برون و از درون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اسب‌ خوب ‌از جنگ ‌بیرونت کشد</p></div>
<div class="m2"><p>جفت ‌بد بر تخت در خونت کشد</p></div></div>
<div class="b2" id="bn18"><p>با سر شمشیر تیز</p></div>
<div class="b" id="bn19"><div class="m1"><p>من اگر بودم به جای پادشاه</p></div>
<div class="m2"><p>این دو زن را راندمی زین بارگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاه گفت این زفت‌رویی خود مباد</p></div>
<div class="m2"><p>کآدمیزاد از زن و اسب است شاد</p></div></div>
<div class="b2" id="bn21"><p>زن سپید و اسب دیز </p></div>
<div class="b" id="bn22"><div class="m1"><p>این زمان آمد دوان از کوهسار</p></div>
<div class="m2"><p>بانوی ایران اناهیت از شکار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیمه‌تن پوشیده در چرم پلنگ</p></div>
<div class="m2"><p>ساق‌و زانو، کتف‌و باز و لعل‌رنگ</p></div></div>
<div class="b2" id="bn24"><p>چون گوزنی گورخیز</p></div>
<div class="b" id="bn25"><div class="m1"><p>گردنی کوته‌، رخی ناگوشتمند</p></div>
<div class="m2"><p>بینی‌ای چون بینی آهو بلند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خوشه‌خوشه موی سر مالان به پشت</p></div>
<div class="m2"><p>چشم‌ها کوچک،‌ لب زیرین درشت</p></div></div>
<div class="b2" id="bn27"><p>نیزهٔ بر کف قطره‌ریز</p></div>
<div class="b" id="bn28"><div class="m1"><p>آمد و دید آن دو اسب و آن دو زن</p></div>
<div class="m2"><p>شاه با شیدسپ مشغول سخن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گوید این یک:‌ زن بران‌،‌ مرکب بدار</p></div>
<div class="m2"><p>گوید آن یک: درخورند این‌هرچهار</p></div></div>
<div class="b2" id="bn30"><p>این دو اسب و دو کنیز</p></div>
<div class="b" id="bn31"><div class="m1"><p>رفت نزدیک کنیزان چگل</p></div>
<div class="m2"><p>آن فرشته طلعتان دیو دل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون گل‌سوری‌لطیف و تازه‌روی</p></div>
<div class="m2"><p>چون‌سمن‌پاک و چو نسرین مشکبوی</p></div></div>
<div class="b2" id="bn33"><p>چون گهر نغز و تمیز</p></div>
<div class="b" id="bn34"><div class="m1"><p>آن‌دواز بیمش بلرزیدند سخت</p></div>
<div class="m2"><p>چون‌زطوفانی‌قوی‌، شاخ درخت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لیک‌ناهید از عطوفت خندخند</p></div>
<div class="m2"><p>گفت کاین دو خوبرو زان منند</p></div></div>
<div class="b2" id="bn36"><p>زآن شه دیگرجهیز</p></div>
<div class="b" id="bn37"><div class="m1"><p>با دو بازو هر دو را در برگرفت </p></div>
<div class="m2"><p>بوسه‌ای از لعل هریک برگرفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>...</p></div>
<div class="m2"><p>...</p></div></div>
<div class="b2" id="bn39"><p>...</p></div>