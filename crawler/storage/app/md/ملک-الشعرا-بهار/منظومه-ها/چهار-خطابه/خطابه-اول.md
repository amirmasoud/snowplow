---
title: >-
    خطابهٔ اول
---
# خطابهٔ اول

<div class="b" id="bn1"><div class="m1"><p>شاه جهان‌، پهلوی نامدار</p></div>
<div class="m2"><p>ای ز سلاطین کیان یادگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خنجر بران تو روز هنر</p></div>
<div class="m2"><p>هست کلید در فتح و ظفر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیغ کجت چون زپی نظم خاست</p></div>
<div class="m2"><p>هر کجیئی بود بدو گشت راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توپ ‌تو بر خصم ‌ز دوزخ دریست</p></div>
<div class="m2"><p>قبر برایش درک دیگری است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی نکوی تو در جنت است</p></div>
<div class="m2"><p>هرکه تو را دید ز غم راحت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخت تو باشد علم کاویان</p></div>
<div class="m2"><p>ملک تو ماننده ملک کیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون پی آن بخت همایون شدی</p></div>
<div class="m2"><p>کاوه بدی باز فریدون شدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ کس از بهر تو کاری نکرد</p></div>
<div class="m2"><p>هیچ عددسنج‌، شماری نکرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچه ‌شد از همت‌ و هوش ‌تو شد</p></div>
<div class="m2"><p>تاکه جهان حلقه به کوش تو شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه برایت قدمی می‌نهاد</p></div>
<div class="m2"><p>ازکف مشتت درمی می‌گشاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کس به ‌تو خدمت ننموده بسی</p></div>
<div class="m2"><p>منت بیجا مکش از هرکسی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیز کسی با تو نکرده بدی</p></div>
<div class="m2"><p>بد نسزد با فره ایزدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تاج بنه‌، بخش سماوی‌ست این</p></div>
<div class="m2"><p>شکر بکن‌، کار خداییست این</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نسخهٔ این فال که در دست تست</p></div>
<div class="m2"><p>درکف بسیارکسان بد نخست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هیچ کس‌ آن‌ نسخه‌ نیارست خواند</p></div>
<div class="m2"><p>ور قدری خواند نیارست راند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو همه را خواندی و پرداختی</p></div>
<div class="m2"><p>کار به آیین خرد ساختی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همت تو پیشرو کار شد</p></div>
<div class="m2"><p>بخت‌، مددکار و خدا یار شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>علم و عمل را بهم انداختی</p></div>
<div class="m2"><p>ولوله در ملک جم انداختی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گردن دولت به کمند تو بود</p></div>
<div class="m2"><p>این همه از بخت بلند تو بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاه شدی کسوت شاهی بپوش</p></div>
<div class="m2"><p>چشم زتنکیل وتباهی بپوش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاه ببخشد ز رعیت گناه</p></div>
<div class="m2"><p>زان که شه از او بود و او ز شاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دشمنی ‌شه به کسی درخور است</p></div>
<div class="m2"><p>کش هوس پادشهی در سر است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرکه ندارد هوسی این‌چنین</p></div>
<div class="m2"><p>تابع شاه است به روی زمین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تابع شه هرچه بود پرگناه</p></div>
<div class="m2"><p>هرچه بود مجرم و نامه‌سیاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حالت فرزندی شه دارد او</p></div>
<div class="m2"><p>سهل بود هرچه گنه دارد او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بهر سلاطین اروپا حقی است</p></div>
<div class="m2"><p>زان‌حقشان‌منزلت و رونقی است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حق شهانست که گر مجرمی</p></div>
<div class="m2"><p>مستحق عفو نماید همی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاه به کشتن نگذارد ورا</p></div>
<div class="m2"><p>وزکف دژخیم برآرد ورا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همچو حقی بهر شهان پربهاست</p></div>
<div class="m2"><p>کاین پی محبوبیت پادشاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پادشها! خلق به دام تواند</p></div>
<div class="m2"><p>جمله ستایندهٔ نام تواند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>درپی محبوبیت خویش بامن</p></div>
<div class="m2"><p>شاه شدی حامی درویش باش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پادشهی هست در اول به‌زور</p></div>
<div class="m2"><p>چون به کف آید ندهد زور نور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رافت‌وبخشایش‌واحسان‌خوشست</p></div>
<div class="m2"><p>آنچه‌پسندهمه‌است‌آن‌خوشست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هرچه درتن ملک تباهی رود</p></div>
<div class="m2"><p>برسرآن سکهٔ شاهی رود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون به‌خدا دست برآردکسی</p></div>
<div class="m2"><p>جز توبه مردم نشماردکسی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هرکه ببالد ز تو بالیده است</p></div>
<div class="m2"><p>هرکه بنالد ز تو نالیده است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گرکه ببالیم ز اعمال تو</p></div>
<div class="m2"><p>به که بنالیم ز عمال تو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>قدرت صد لشکر شمشیرزن</p></div>
<div class="m2"><p>کم بود از نالهٔ یک پیرزن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نالهٔ مظلوم صدای خداست</p></div>
<div class="m2"><p>توپ‌شهان‌پیش خدا بی‌صداست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>قدرت و جاه تو شها در زمن</p></div>
<div class="m2"><p>کم نشود از من و صد همچو من</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ور شود از خشم تو موری تباه</p></div>
<div class="m2"><p>لکهٔ ظلمی است به دامان شاه</p></div></div>