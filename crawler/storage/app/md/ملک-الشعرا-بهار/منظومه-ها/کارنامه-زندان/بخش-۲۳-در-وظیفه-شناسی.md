---
title: >-
    بخش ۲۳ - در وظیفه‌شناسی
---
# بخش ۲۳ - در وظیفه‌شناسی

<div class="b" id="bn1"><div class="m1"><p>رسم مکتب بود که استادت</p></div>
<div class="m2"><p>پیش بنهد یکی وٍرٍستادت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوید این را بخوان و حاضر کن</p></div>
<div class="m2"><p>کزتو پرسم همی سخن به سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نخوانی و سهل‌انگاری</p></div>
<div class="m2"><p>وان ورستاد یاوه پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشمالت دهند استادان</p></div>
<div class="m2"><p>خوارگردی به نزد همزادان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این ورستاد کودکان پند است</p></div>
<div class="m2"><p>بنگرد هرکه او خردمند است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این ورستاد را که داد استاد</p></div>
<div class="m2"><p>نام آن را عرب وظیفه نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون که گشتی کلان و مرد شدی</p></div>
<div class="m2"><p>سوی امید ره‌نورد شدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شود آن گه زمانه استادت</p></div>
<div class="m2"><p>پیش بنهد دگر ورستادت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوید اینت وظیفهٔ کارست</p></div>
<div class="m2"><p>کارکن گرچه کار دشوار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر به مکتب وظیفه خواندستی</p></div>
<div class="m2"><p>وان ورستاد را نماندستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این ورستاد مر تراست روان</p></div>
<div class="m2"><p>کار فرمودنش بسی آسان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با تو گیتی شریک کار شود</p></div>
<div class="m2"><p>در ادای وظیفه یار شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون‌ روان‌ با شدت وظیفه خوبش</p></div>
<div class="m2"><p>کارهایت روان شود از پیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ژنده‌پوش کسی نخواهی شد</p></div>
<div class="m2"><p>بار دوش کسی نخواهی شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دانشی را که کرده‌ای تحصیل</p></div>
<div class="m2"><p>پیش رویت کند گشاده سبیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور به مکتب وظیفه نشناسی</p></div>
<div class="m2"><p>اوستاد و خلیفه نشناسی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون شود روزگار استادت</p></div>
<div class="m2"><p>خواند باید ز سر ورستادت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواندنش گرچه هست‌ بس مشگل</p></div>
<div class="m2"><p>داد باید به کار باری دل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرچه بینی عذاب و رنج بسی</p></div>
<div class="m2"><p>عاقبت می‌شوی تو نیز کسی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جای گیری ز جرگهٔ نسناس</p></div>
<div class="m2"><p>در صف مردم وظیفه‌شناس</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وگر این بار هم شوی کاهل</p></div>
<div class="m2"><p>نیبشی جز منافق و جاهل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کار دشخوار گرددت پس از آن</p></div>
<div class="m2"><p>وز تو دشخوار، کار اهل جهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر صباحی وظیفه‌ایت نهند</p></div>
<div class="m2"><p>هر دمی درس تازه‌ایت دهند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یاد نگرفته نکته‌ای زین یک</p></div>
<div class="m2"><p>می‌دهد درس دیگریت فلک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه ز استاد جسته تربیتی</p></div>
<div class="m2"><p>نز پدر برگرفته تجربتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه وظیفه شناخته نه عمل</p></div>
<div class="m2"><p>گول و نادان و مست و لایعقل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>افتی اندر شکنجه‌های زمان</p></div>
<div class="m2"><p>مرد بی‌درد و درد بی‌درمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روزگارت چنان بمالد گوش</p></div>
<div class="m2"><p>که زند مغز استخوانت جوش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شوی از کوب آسمان شیدا</p></div>
<div class="m2"><p>درد پیدا و زخم ناپیدا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون ندانی به زخم‌ها مرهم</p></div>
<div class="m2"><p>خو ی گیری به دردها کم کم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شو ی از دانش و شرف مفلس</p></div>
<div class="m2"><p>قسی‌القلب و بی رگ و بی‌حس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حس چو از آستانه برخیزد</p></div>
<div class="m2"><p>شرم و درد از میانه برخیزد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>درد چون رفت‌، شرم هم برود</p></div>
<div class="m2"><p>غیرت و خون گرم هم برود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شرم چون رفت‌، رفت عفت هم</p></div>
<div class="m2"><p>تقو ی و مردی و فتوت هم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرد بی‌شرم‌، بی‌ عفاف شود</p></div>
<div class="m2"><p>حیله سازد، دروغ‌باف شود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دشمن جان بخردان گردد</p></div>
<div class="m2"><p>مایهٔ ننگ خاندان گردد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خیزد این خوی‌های نسناسی</p></div>
<div class="m2"><p>ربشه‌اش از وظیفه‌نشناسی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>لاابالی شود به نیک وبه بد</p></div>
<div class="m2"><p>در جهان هیچ ننگرد جز خود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بخت از ملتی چو برگردد</p></div>
<div class="m2"><p>در وی این فرقه نامور گردد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شود از بخت بد درین صحنه</p></div>
<div class="m2"><p>محتسب دزد و راهزن شحنه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون برافتاد رسم خیر و صلاح</p></div>
<div class="m2"><p>شود البته خون خلق مباح</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زشت‌نامان چو نامدار شوند</p></div>
<div class="m2"><p>اهل ناموس و نام‌خوار شوند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>لاجرم جای آبرومندان</p></div>
<div class="m2"><p>یا ته خانه است یا زندان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اهل تقوی اگر امیر شوند</p></div>
<div class="m2"><p>جانیان جمله دستگیر شوند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همچنین چون امیر شد جانی</p></div>
<div class="m2"><p>اهل تقوی شوند زندانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زان که یزدان در اولین ترکیب</p></div>
<div class="m2"><p>ریخت طرح تناسب و ترتیب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>متناسب گرفت کار جهان</p></div>
<div class="m2"><p>تا توان داشتن شمار جهان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کیست دانش‌پژوه صاحب‌جاه</p></div>
<div class="m2"><p>آن که باشد ز خویشتن آگاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هرکه بر نفس خو‌یش چیر بود</p></div>
<div class="m2"><p>به حقیقت که او دلیر بود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وان که او دیو آز کرد به بند</p></div>
<div class="m2"><p>او بود بی‌نیاز و دولتمند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>آن کسی خوبش را به‌نام کند</p></div>
<div class="m2"><p>که به نفع بشر قیام کند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هرکه پیرامن خطرگردد</p></div>
<div class="m2"><p>در جهان سخت مشتهر گردد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>لیک هر شهرتی نکو نبود</p></div>
<div class="m2"><p>عرق ذلت آبرو نبود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>آن که افشاند بول در زمزم</p></div>
<div class="m2"><p>گشت نامی ولی نه چون خاتم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نیست شهرت دلیل دانایی</p></div>
<div class="m2"><p>نه تنومندی از توانایی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>رادمرد حکیم نیکوکار</p></div>
<div class="m2"><p>جوید از مردم زمانه کنار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زان که یک همنشین باتقوی</p></div>
<div class="m2"><p>به ز سیصد مرید بی‌معنی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آن که گنجی در آستین دارد</p></div>
<div class="m2"><p>کی سر برگ همنشین دارد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مرد عارف ز صیت بگریزد</p></div>
<div class="m2"><p>عاقل از ابلهان بپرهیزد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اکثر خلق گول و بی‌عقلند</p></div>
<div class="m2"><p>دشمن علم و عاشق نقلند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>آن که نقلش فتاد در افواه</p></div>
<div class="m2"><p>گرچه خضر است می‌شودگمراه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دوستداران و دشمنان جهول</p></div>
<div class="m2"><p>به قبول و به رد او مشغول</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نقلش اندر جهان سمر گردد</p></div>
<div class="m2"><p>پند و اندرز او هدر گردد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زیرکان زیر زیر، کار کنند</p></div>
<div class="m2"><p>کاسبان کسب اشتهار کنند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سخنی پخته و درست و تمام</p></div>
<div class="m2"><p>بهتر از صدهزار گفتهٔ خام</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گر کسی جهلی از دلی روبد</p></div>
<div class="m2"><p>به که صد شهر را برآشوبد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گر کنی تربیت جوانی را</p></div>
<div class="m2"><p>به که پر زر کنی جهانی را</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ور نهی پند نامه‌ای محکم</p></div>
<div class="m2"><p>پس مرگ خود اندر‌بن عالم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به که خلقان زتو برآشوبند</p></div>
<div class="m2"><p>بر سر و مغز یکدگرکوبند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>آن‌یکی خواندت خدای بشر</p></div>
<div class="m2"><p>وان دگر داندت بلای بشر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>عارفی چینی از طریق فسوس</p></div>
<div class="m2"><p>گفته بود این سخن به کنفسیوس</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گفته‌هایش به نظم سنجیدم</p></div>
<div class="m2"><p>زان که عین حقیقتش دیدم</p></div></div>