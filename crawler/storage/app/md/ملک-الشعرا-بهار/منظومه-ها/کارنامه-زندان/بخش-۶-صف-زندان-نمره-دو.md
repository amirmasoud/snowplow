---
title: >-
    بخش ۶ - صف زندان نمرهٔ دو
---
# بخش ۶ - صف زندان نمرهٔ دو

<div class="b" id="bn1"><div class="m1"><p>پس ره نمرهٔ دو پیمودم</p></div>
<div class="m2"><p>زان که خود راه را بلد بودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایستادم به پیش آن درگاه</p></div>
<div class="m2"><p>چه دری‌، لا اله الا الله‌!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دخمه‌ای تنگ و سوبه‌سوی ‌و نمور</p></div>
<div class="m2"><p>واندر آن دخمه چند زنده‌به‌گور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر یکی در کریچه‌ای دلتنگ</p></div>
<div class="m2"><p>بسته بر رویشان دری چون سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داشت دهلیزی و بر آن دهلیز</p></div>
<div class="m2"><p>بود بسته دری ز آهن نیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به درون رفتم از همان در، من</p></div>
<div class="m2"><p>که بدم رفته بار دیگر، من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد برگشتم از یکی رهرو</p></div>
<div class="m2"><p>پیش سمجی که بود مسکن نو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر در نمرهٔ یک استادم</p></div>
<div class="m2"><p>وان قلاوور را فرستادم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بگوید ز خانه‌ام باری</p></div>
<div class="m2"><p>بستر آرند و فرش و ناهاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس نگه کردم اندر آن دالان</p></div>
<div class="m2"><p>دیدم آنجا گروهی از یاران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هریک استاده گوشه‌ای خسته</p></div>
<div class="m2"><p>چند تن در به رویشان بسته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میر مخصوص کلهر و خسرو</p></div>
<div class="m2"><p>چندی از دوستان کهنه و نو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شده هر یک به دیگری مأنوس</p></div>
<div class="m2"><p>پنج شش سال هر یکی محبوس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میرکلهر نمود از سختی</p></div>
<div class="m2"><p>ناله‌، وز روزگار بدبختی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت شش سال بودم اندر بند</p></div>
<div class="m2"><p>چار دیگر بر او برافزودند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون شود مرد لشگری قاضی</p></div>
<div class="m2"><p>شود انسان ز قاضیان راضی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کلبهٔ عهد پیش را دیدم</p></div>
<div class="m2"><p>خوردم آنجا ناهار و خوابیدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ظاهراً تازه همتی کردند</p></div>
<div class="m2"><p>وان قفس را مرمتی کردند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پاک و بی گرد و آب و جارو بود</p></div>
<div class="m2"><p>مبرزش نیز پاک و بی‌بو بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هان و هان تا مگر نپنداری</p></div>
<div class="m2"><p>که اطاقیست خوب و گچ‌کاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عرض و طولش چو تنگنای عدم</p></div>
<div class="m2"><p>سه قدم طول بود در دو قدم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بهتر از زنده در چنین مرقد</p></div>
<div class="m2"><p>آن که مرده است و خفته زیر لحد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نبود کار مرده جنبیدن</p></div>
<div class="m2"><p>نیست محتاج خوردن و ریدن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هست‌، تا هست آدمی زنده</p></div>
<div class="m2"><p>گاه جنبنده گاه ریزنده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عادت آدمی است آمیزش</p></div>
<div class="m2"><p>خور و خفتار و جنبش و خیزش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این همه در یکی کریچهٔ تنگ</p></div>
<div class="m2"><p>گفتنش نیز هست مایهٔ ننگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با بشر هیچ کس نکرده چنین</p></div>
<div class="m2"><p>حیوان نیز نیست درخور این</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بود اندر زمانه‌های قدیم</p></div>
<div class="m2"><p>گاه‌گاهی چنین عذاب الیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لیک در دورهٔ تمدن و دین</p></div>
<div class="m2"><p>با بشر کس نکرده است چنین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تازه این جایگاه احرار است</p></div>
<div class="m2"><p>وای از آنجا که جای اشرار است</p></div></div>