---
title: >-
    بخش ۳۵ - حکمت
---
# بخش ۳۵ - حکمت

<div class="b" id="bn1"><div class="m1"><p>پیش زن مدح دیگران مکنید</p></div>
<div class="m2"><p>خوبی غیر را بیان مکنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان که جنس لطیف بیباکست</p></div>
<div class="m2"><p>هم حسود است و هم هوسناک ست‌</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حُسن زن گر شنید رشگ برد</p></div>
<div class="m2"><p>حسن مرد ار شنید دل سپرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار دیگر چو آمدیم اینجا</p></div>
<div class="m2"><p>هم‌ره هم‌، قدم زدیم اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازگشتیم سوی خانهٔ خوبش</p></div>
<div class="m2"><p>دیدمش سر فکنده اندر پیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الغرض چند دردسر دهمت</p></div>
<div class="m2"><p>آخر کار را خبر دهمت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد مسلم که جفت احمق من</p></div>
<div class="m2"><p>ظن بد برده است در حق من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مرا عاشق تو یافته است</p></div>
<div class="m2"><p>در بر شوهرت شتافته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من ندانم چه گفته است آنجا</p></div>
<div class="m2"><p>یا چه از وی شنفته است آنجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لیک دانم شدند عاشق هم</p></div>
<div class="m2"><p>هر دو از جان و دل موافق هم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شوهرت چو سفر نمود ز شهر</p></div>
<div class="m2"><p>زن من هم نمود از من قهر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چند روزی نیافتم اثرش</p></div>
<div class="m2"><p>ناگهان آمد از سفر خبرش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد محقق که آن زن بی‌باک</p></div>
<div class="m2"><p>همره ‌شوهرت زده است به چاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه غمم از برای خود تنهاست</p></div>
<div class="m2"><p>بیشتر غصه‌ام برای شماست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شوهرت را وفا و وجدان نیست</p></div>
<div class="m2"><p>بلکه درنده‌ایست انسان نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون منی را چه باک گر آزرد</p></div>
<div class="m2"><p>چون تویی را چرا ز خاطر برد؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرکه در خانه‌اش فرشته‌ایست</p></div>
<div class="m2"><p>گر دهد دل به‌ دیو، مردم نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زن که از شوهری چو من دل کند</p></div>
<div class="m2"><p>کی شود شوهر ترا دلبند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وان که ‌ازچون تو خانمی ‌شد سیر</p></div>
<div class="m2"><p>دل به یاری دگر نبندد دیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وه که دینی نماند و وجدانی</p></div>
<div class="m2"><p>نه مسلمانی و نه انسانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بس که از این دروغ‌ها پرداخت</p></div>
<div class="m2"><p>زن بیچاره را ز پای انداخت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زن ز پای اوفتاد و رفت از هوش</p></div>
<div class="m2"><p>مرد پتیاره برکشید خروش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آبش افشاند و برکشید لباس</p></div>
<div class="m2"><p>سینه مالید و زد فراوان لاس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون زن آمد به‌ هوش و آه کشید</p></div>
<div class="m2"><p>خویش را در کنار فاسق دید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خواست زن تا به شوی نامه کند</p></div>
<div class="m2"><p>آتش دل به نوک خامه کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرد کفتش چه می کنی ؟هشدار!</p></div>
<div class="m2"><p>قسم خویشتن فرا یاد آر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه ثمر زین شکایت آرایی</p></div>
<div class="m2"><p>بجز از افتضاح و رسوایی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کاین دو تن بر من و تو می‌خندند</p></div>
<div class="m2"><p>چون رسد نامه‌، شیشکی بندند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیست ما را، به‌عین سرپوشی</p></div>
<div class="m2"><p>چاره‌ای غیر صبر و خاموشی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چند روزی ‌از این حدیث گذشت</p></div>
<div class="m2"><p>خانم از رنج و غصه ناخوش گشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هیچ ننوشت بهر شوهر خویش</p></div>
<div class="m2"><p>که از او داشت دست بر سر خویش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گرچه شویش نوشت نامه بسی</p></div>
<div class="m2"><p>لیک از آنها خبر نیافت کسی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زان که آن نامه‌ها به ‌نام و نشان</p></div>
<div class="m2"><p>داشت عنوان مرد بی‌وجدان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرد بی‌ دین نهفت آنها را</p></div>
<div class="m2"><p>تا ز هم بگسلد روان‌ها را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>و آنچه از بهر زن حوالت کرد</p></div>
<div class="m2"><p>مرد بی‌دین به‌خورد و حالت کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شد چو دیگ و چراغ زن بی‌زیت</p></div>
<div class="m2"><p>به گروگان نهاد اثاث البیت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ربح سنگین و خلق بی‌انصاف</p></div>
<div class="m2"><p>خانه گشت از اثاث منزل صاف</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرد بی‌دین بیامدی همه روز</p></div>
<div class="m2"><p>چهره‌ غمگین‌ چو مردم دلسوز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ندبه کردی و حسب حالی چند</p></div>
<div class="m2"><p>وام دادی به زن ریالی چند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون نیامد خبر ز جانب شو</p></div>
<div class="m2"><p>زن یقین کرد گفتهٔ یارو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پس زمستان رسید و برف افتاد</p></div>
<div class="m2"><p>ماهرو در غمی شگرف افتاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چون که‌از شوی‌خو وکالت‌داشت</p></div>
<div class="m2"><p>دل به اندیشهٔ طلاق گماشت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نفقه و کسوه را بهانه نهاد</p></div>
<div class="m2"><p>با جوان راز در میانه نهاد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وآن جوان دو روی کاغذ ساز</p></div>
<div class="m2"><p>ساخته بود کاغذی ز آغاز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون که بشنید از زن آن گفتار</p></div>
<div class="m2"><p>گفت شرمنده‌ام از این رفتار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زآن که شوی فسادکامهٔ تو</p></div>
<div class="m2"><p>کرد صادر طلاقنامهٔ تو</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زن دلریش بی‌نوای فقیر</p></div>
<div class="m2"><p>گشت هم‌شادمان و هم دلگیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از ره رشگ و حقد و بدحالی</p></div>
<div class="m2"><p>دلش از مهر شوی شد خالی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پس به محضر شدند آن دو به‌ هم</p></div>
<div class="m2"><p>زن‌و شوهر شدند آن‌دو به‌هم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بعد چندی شنید بدکردار</p></div>
<div class="m2"><p>کاینک آید ز راه‌، شوهر یار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>آید و خانه را تهی بیند</p></div>
<div class="m2"><p>تهی از ماه خرگهی بیند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پس اندک تجسس و تفتیش</p></div>
<div class="m2"><p>می‌برد پی به قصهٔ زن خویش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ماجرائی بزرگ خواهد دید</p></div>
<div class="m2"><p>دنبه را نزدگرگ خواهد دید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نکند لاجرم شکیبایی</p></div>
<div class="m2"><p>می کند افتضاح و رسوائی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تندبادی به مغز او بوزبد</p></div>
<div class="m2"><p>لحظه‌ای فکر کرد و چاره گزید</p></div></div>