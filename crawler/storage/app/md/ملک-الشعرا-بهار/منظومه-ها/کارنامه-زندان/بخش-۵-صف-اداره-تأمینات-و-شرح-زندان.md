---
title: >-
    بخش ۵ - صف ادارهٔ تأمینات و شرح زندان
---
# بخش ۵ - صف ادارهٔ تأمینات و شرح زندان

<div class="b" id="bn1"><div class="m1"><p>مثل مردمان خطا نشود</p></div>
<div class="m2"><p>که دویی نیست کان سه‌تا نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با من این حبس گاه راکار است</p></div>
<div class="m2"><p>حبس این بنده سومین‌بار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بارهای دگر بدون درنگ</p></div>
<div class="m2"><p>می گذشتم ز ره به محبس تنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون رسیدم ز ره ولیک این بار</p></div>
<div class="m2"><p>برد فخرائیم به شعبهٔ چار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نشستم درآن کریچهٔ سرد</p></div>
<div class="m2"><p>کمر من گرفت از نو درد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیرگاهی نشستم آنجا من</p></div>
<div class="m2"><p>کس نفرمود صحبتی با من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پس یک‌ دو ساعت‌، آمد پیش</p></div>
<div class="m2"><p>فربهی سبز رنگ وکافرکیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صورتی گرد و چهره‌ای مغرور</p></div>
<div class="m2"><p>دست و پایی ز ذوق و صنعت دور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لیک درکار خویش زبر و زرنگ</p></div>
<div class="m2"><p>به فسون روبه و به کبرپلنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داد دست و نشست و خامه کشید</p></div>
<div class="m2"><p>جا ونام و نشان من پرسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس بزد بانگ و آمد از بیرون</p></div>
<div class="m2"><p>یکی از آن سه مرد راهنمون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اول رنج و زحمت است اینجا</p></div>
<div class="m2"><p>فتح باب مشقت است اینجا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنده با آن عوان روانه شدیم</p></div>
<div class="m2"><p>یک‌دوساعت‌به یک‌دو خانه شدیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شرح آن دخمه‌ها از اسرار است</p></div>
<div class="m2"><p>فکرکاهست و خاطرآزار است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در یکی زان دوکلبهٔ احزان</p></div>
<div class="m2"><p>مردمی دیدم از الم لرزان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حاج‌سیاح قمی ‌پرخور</p></div>
<div class="m2"><p>بود آن جای بسته برآخور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شکم گنده پیش آورده</p></div>
<div class="m2"><p>گنده‌بویی به ریش آورده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گشته چرک و سیاه‌مولویش</p></div>
<div class="m2"><p>بر زبان بود مدح پهلویش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شعر می‌خواند و پف پف می کرد</p></div>
<div class="m2"><p>بر سر و ریش خلق‌ تف می‌کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مدح می‌خواند شاه ایران را</p></div>
<div class="m2"><p>حامی فرقهٔ فقیران را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا مگر زودتر رها گردد</p></div>
<div class="m2"><p>باز مبل اطاق‌ها گردد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سر و ریشی صفا دهد از نو</p></div>
<div class="m2"><p>شکم گنده را دهد به جلو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بنشیند به مجلس اعیان</p></div>
<div class="m2"><p>بدهد حکم چایی و قلیان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نیزه را محرمانه بند کند</p></div>
<div class="m2"><p>چند غازی مگر بلند کند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرچه در شهر ری سرایی نیست</p></div>
<div class="m2"><p>محضری‌، منظری‌، لقایی نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>محفل و مجلسی اگر باقی است</p></div>
<div class="m2"><p>هست در این محل و الا نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قصرها را ببست دولت در</p></div>
<div class="m2"><p>تا که شد باز باب «‌قصر قجر»</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ساعتی هم دریچه گذشت</p></div>
<div class="m2"><p>تا همه چیز ثبت دفتر گشت</p></div></div>