---
title: >-
    بخش ۳۸ - داستان مرد حکیم
---
# بخش ۳۸ - داستان مرد حکیم

<div class="b" id="bn1"><div class="m1"><p>داشت همسایه‌ای به حبس مقیم</p></div>
<div class="m2"><p>پیرمردی بزرگوار و حکیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیرشد با جوان رفیق شفیق</p></div>
<div class="m2"><p>که‌ به حبس اندرون خوشست رفیق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود ساباطی اندران رسته</p></div>
<div class="m2"><p>از دو سو سمج‌های در بسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود هر لانه جای محبوسی</p></div>
<div class="m2"><p>هر اطاقی سرای مایوسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک دو ساعت ز روز بهر نشاط</p></div>
<div class="m2"><p>گرد گشتندی اندر آن ساباط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون که بودند هر دو هم‌آخور</p></div>
<div class="m2"><p>زود با یکدگر اُ‌قر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیر پرسید شرح حال جوان</p></div>
<div class="m2"><p>کرد او شرح حال خویش بیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت بنگر به بیگناهی من</p></div>
<div class="m2"><p>جرم ناکرده روسیاهی من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیر گفتش که بیگناه نه‌ای</p></div>
<div class="m2"><p>جرم ناکرده روسیاه نه‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اولین جرمت آن که بی‌سببی</p></div>
<div class="m2"><p>بی‌دل‌آزایئی و بی‌غضبی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست شستی ز دوستان قدیم</p></div>
<div class="m2"><p>سر سپردی به نورسیده ندیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دومین جرمت آن که بی‌دینی</p></div>
<div class="m2"><p>یار بگرفتی و بد آئینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکه آیین و دین نداند چیست</p></div>
<div class="m2"><p>حق صحبت یقین نداند چیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سه دگر جرمت آن که آن کس را</p></div>
<div class="m2"><p>آن رفیق جدید نورس را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>راه دادی به خانه در بر خویش</p></div>
<div class="m2"><p>آشنا ساختی به همسرخویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برهمن را بر صنم بردی</p></div>
<div class="m2"><p>کرک را همسر غنم کردی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چارمین‌، در مسافرت زن را</p></div>
<div class="m2"><p>بنهادی به خانهٔ تنها</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پنجمین آن که کارخانهٔ خویش</p></div>
<div class="m2"><p>بسپردی به مرد زشت اندیش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بنهادی ز جهل بی‌اکراه</p></div>
<div class="m2"><p>دنبه را در برابر روباه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن که وجدان بدیل دین دارد</p></div>
<div class="m2"><p>عشق را کی حرام انگارد؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون شود عاشق زنی زیبا</p></div>
<div class="m2"><p>کی نماید ز شرح عشق ابا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون که اظهار عشق خویش نمود</p></div>
<div class="m2"><p>لابه و لاف‌ها بر آن افزود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل زن را ز جای برباید</p></div>
<div class="m2"><p>عاقبت بر مراد چیر آید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زن و مردی قرین یکدیگر</p></div>
<div class="m2"><p>خانه خالی و شوی رفته سفر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قصهٔ عشق و عاشقی به میان</p></div>
<div class="m2"><p>در میانه چه می کند وجدان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هست وجدان ترازویی موزون</p></div>
<div class="m2"><p>به نهانخانهٔ خیال درون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پارسنگش دل هوسناک است</p></div>
<div class="m2"><p>نیز شاهینش نفس بیباک است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هرچه می‌خواهد اندر آن خانه</p></div>
<div class="m2"><p>سنجد از بهرخویش و بیگانه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ور ملامت کندش نفس شریف</p></div>
<div class="m2"><p>نفس اماره‌اش دهد تسویف</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شهوت و کین و حرص خودکامه</p></div>
<div class="m2"><p>غااب آید به نفس لوامه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زآن که بی‌شبهه مرد وجدانی</p></div>
<div class="m2"><p>هست همدستشان به پنهانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر حکیمی و گر خردمندی</p></div>
<div class="m2"><p>نگراید سوی خطا چندی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا نگویی مطیع وجدان است</p></div>
<div class="m2"><p>کاو مطیع اصول عرفانست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا بود اصل زندگی زر و زور</p></div>
<div class="m2"><p>تابود زن‌ضعیف‌ومرد غیور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا بود خانواده و زیور</p></div>
<div class="m2"><p>صد هزاران تجملات دگر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هست واجب معاد و برزخ هم</p></div>
<div class="m2"><p>هست لازم بهشت و دوزخ هم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دین و ایمان و عفت است ضرور</p></div>
<div class="m2"><p>شرم‌و تقوی‌و غیرت‌است ضرور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ور زر و زیور از میانه برفت</p></div>
<div class="m2"><p>نظم نوآمد و بهانه برفت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دین و وجدان یکان یکان برود</p></div>
<div class="m2"><p>وین خرافات از میان برود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>لیک تا زر بود مرام جهان</p></div>
<div class="m2"><p>زرپرستی بود نظام جهان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چانه بیهوده می‌زند وجدان</p></div>
<div class="m2"><p>هیچ کاری نمی‌کند وجدان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کی فرو چه رود پسندیده</p></div>
<div class="m2"><p>با چنین ریسمان پوسیده</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ور یکی شد، هزار می‌نشود</p></div>
<div class="m2"><p>به یکی گل بهار می‌نشود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زان که خوی بهیمه در کار است</p></div>
<div class="m2"><p>خود فروشنده‌ خود خریدار است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بشنو این نکته را و دار بهٔاد</p></div>
<div class="m2"><p>ور ز من نشنوی شنو ز استاد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>« کانچه را نام کرده‌ای وجدان</p></div>
<div class="m2"><p>چیست جز باد کرده در انبان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نیک بنگر بدو که بی کم و بیش</p></div>
<div class="m2"><p>چون ‌هریسه است‌ و آبدیده ‌سریش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چون کشی‌، ریش احمق است دراز</p></div>
<div class="m2"><p>ور رها شد درازیش به دو قاز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شیر بر غرم‌ چون برد دندان</p></div>
<div class="m2"><p>هیچ دانی چه گوبدش وجدان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گوبد ای شاه دد هماره بزی</p></div>
<div class="m2"><p>نوش خور نوش ‌و شادخواره بزی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زان که زبن غرم گول اشتر دل</p></div>
<div class="m2"><p>چون کنی طعمه‌ای شه عادل</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>عمل هضم دد! به معدهٔ میر</p></div>
<div class="m2"><p>شیر سازی کند از این نخجیر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کار صید از تو نز ره‌بازبست</p></div>
<div class="m2"><p>بلکه از دام‌، شاه ددسازیست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زن جولا چو برکشد بکتاش</p></div>
<div class="m2"><p>باز وجدان بدو زند شاباش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گویدش این نگار جانانه</p></div>
<div class="m2"><p>اندر آن تنگ و تار وبرانه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نه خورش داشتی نه جامهٔ گرم</p></div>
<div class="m2"><p>شوی نیز از رخش ببردی شرم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هر دو رستند ازین جوانمردی</p></div>
<div class="m2"><p>این یک از درد و آن ز بی دردی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آری این اوستا به هر نیرنگ</p></div>
<div class="m2"><p>از یکی خم برآورده ده رنگ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زرد ازو جوی و زعفرانی بین</p></div>
<div class="m2"><p>سرخ ازو خواه و ارغوانی بین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دهدت زین خم ار کند آهنگ</p></div>
<div class="m2"><p>نیست بالاتر از سیاهی رنگ‌»</p></div></div>