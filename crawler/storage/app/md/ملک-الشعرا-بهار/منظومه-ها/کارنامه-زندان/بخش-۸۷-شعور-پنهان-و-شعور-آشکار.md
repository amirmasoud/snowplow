---
title: >-
    بخش ۸۷ - شعور پنهان و شعور آشکار
---
# بخش ۸۷ - شعور پنهان و شعور آشکار

<div class="b" id="bn1"><div class="m1"><p>دو شعور است در نهاد بشر</p></div>
<div class="m2"><p>آن غریزی و این به علم و خبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نهانست و این دگر پیدا</p></div>
<div class="m2"><p>و آن نهانی بود به امر خدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن شعوری که از برون در است</p></div>
<div class="m2"><p>پاسدار تمدن بشر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و آن کجا ناپدید و پنهانیست</p></div>
<div class="m2"><p>پاسبان نژاد انسانیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دین و آیین و دانش و فرهنگ</p></div>
<div class="m2"><p>از شعور برون پذیرد رنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک‌ جان‌ها از این‌ شمار جداست</p></div>
<div class="m2"><p>کار جان با شعور ناپیداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچه را روح و نفس و دل خوانی</p></div>
<div class="m2"><p>هست جای شعور پنهانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مغز جای شعور مکتسب است</p></div>
<div class="m2"><p>لیک دل جایگاه فیض رب است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست‌ پُر زین شعور، قلب زنان</p></div>
<div class="m2"><p>چون شنیدی کشیده دار عنان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با زنی بشکامه گفتم من</p></div>
<div class="m2"><p>کاز چه با خویشتن شدی دشمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شوهری داشتی و سامانی</p></div>
<div class="m2"><p>آبروبی و لقمهٔ نانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کودکانی ز قند شرین‌تر</p></div>
<div class="m2"><p>گونه‌هاشان ز لاله رنگین‌تر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از چه رو پشت پا زدی همه را</p></div>
<div class="m2"><p>به‌قضا و بلا زدی همه را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دادی از دست کودکان عزیز</p></div>
<div class="m2"><p>آبرو ربختی ز شوهر نیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دادی از روی شهوت و بیداد</p></div>
<div class="m2"><p>آبروی قبیله‌ای بر باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شده‌ای هرکسی و هر جایی</p></div>
<div class="m2"><p>روز و شب گرم صورت‌ آرایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زود ازین ره تکیده خواهی شد</p></div>
<div class="m2"><p>چون انار مکید خواهی شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون شدی پیر، دورت اندازند</p></div>
<div class="m2"><p>زنده زنده به گورت اندازند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خویش‌ را جفت غم چراکردی‌؟‌!</p></div>
<div class="m2"><p>بر تن خود ستم چرا کردی‌؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پاسخم داد زن‌: که گفتی راست</p></div>
<div class="m2"><p>لیکن آخر دلم چنین می‌خواست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست زن را به کار سر، سروکار</p></div>
<div class="m2"><p>کار او با دل است و این سره کار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عقل را مغز می‌دهد یاری</p></div>
<div class="m2"><p>عشق را دل کند هواداری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرکه با عشق طرح الفت ریخت</p></div>
<div class="m2"><p>رشتهٔ ارتباط عقل گسیخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زن و عشق و دل وشعور نهان</p></div>
<div class="m2"><p>مرد و عقل و نظام کار جهان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من ندانم پی صلاح بشر</p></div>
<div class="m2"><p>زبن دو مذهب کدام اولی‌تر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر دل و مغز هر دو یار شدی</p></div>
<div class="m2"><p>عقل با عشق سازگار شدی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جای بر هیچ کس نگشتی ننگ</p></div>
<div class="m2"><p>آشتی آمدی و رفتی جنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مام نگریستی به کشته پسر</p></div>
<div class="m2"><p>کس نخفتی گرسنه بر بستر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نشدی دربدر زن بیوه</p></div>
<div class="m2"><p>شده از مرگ شوی کالیوه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>و آن تفنگی که می‌زند بدو میل</p></div>
<div class="m2"><p>چوب و آهنش یوغ گشتی و بیل</p></div></div>