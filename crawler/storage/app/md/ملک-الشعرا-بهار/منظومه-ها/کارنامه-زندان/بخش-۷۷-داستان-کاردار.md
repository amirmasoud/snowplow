---
title: >-
    بخش ۷۷ - داستان کاردار
---
# بخش ۷۷ - داستان کاردار

<div class="b" id="bn1"><div class="m1"><p>کارداری براند گرم به‌دشت</p></div>
<div class="m2"><p>شامگاهان به قریه‌ای بگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لقمه‌ای‌خورد و جرعه‌ای‌پیوست</p></div>
<div class="m2"><p>دیده بر هم نهاد خسته و مست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاکه‌از باغ خاست بانگ خروس</p></div>
<div class="m2"><p>خواجه‌برجست خشمناک‌و عبوس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت کاین مرغ بلهوس شومست</p></div>
<div class="m2"><p>یاوه گوی و فراخ حلقومست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داد فرمان به مهتر و پاکار</p></div>
<div class="m2"><p>کز خروسان برآورند دمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه آنجا خروس بدکشتند</p></div>
<div class="m2"><p>خاک با خونشان بیاغشتند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیمشب خواجه چون‌به‌بستر خفت</p></div>
<div class="m2"><p>با ندیمی از آن خویش بگفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون‌بخواند خروس صبح ای یار</p></div>
<div class="m2"><p>خیز و ما را ز خواب کن بیدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتش ای خواجه اندربن ماوی</p></div>
<div class="m2"><p>صبح خوانی دگر نماند بجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر بریدی خروسکان را باز</p></div>
<div class="m2"><p>مرغ سرکنده کی کند آواز</p></div></div>