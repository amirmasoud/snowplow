---
title: >-
    بخش ۷۱
---
# بخش ۷۱

<div class="b" id="bn1"><div class="m1"><p>آمد اردیبهشت‌، ای ساقی</p></div>
<div class="m2"><p>اصفهان شد بهشت‌، ای ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن بهشتی که گم شد از دنیا</p></div>
<div class="m2"><p>هر به سالی مهی‌، شود پیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وان مه اردیبهشت باشد و بس</p></div>
<div class="m2"><p>اصفهان چون بهشت باشد و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنت عدن و روضهٔ رضوان</p></div>
<div class="m2"><p>هست اردیبهشت اصفاهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتابی لطیف و هر روزه</p></div>
<div class="m2"><p>آسمانی چو طشت فیروزه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طاق و ایوان و گنبد و کاشی</p></div>
<div class="m2"><p>شهر را کرده پر ز نقاشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقشه‌ها هرچه خوب و دلکش‌تر</p></div>
<div class="m2"><p>نقش اردیبهشت از آن خوشتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل شب بوش پُر پَر و پرپشت</p></div>
<div class="m2"><p>یاسش انبوه و اطلسیش درشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز هم صحبت از گل آمد پیش</p></div>
<div class="m2"><p>و اوفتادم به یاد گلشن خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شده‌ام این سفر من از جان سیر</p></div>
<div class="m2"><p>لیک کی گردم از صفاهان سیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست از جان همی توان شستن</p></div>
<div class="m2"><p>وز صفاهان نمی‌توان شستن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گل آسایشم به بار آمد</p></div>
<div class="m2"><p>تلگرافی ز شهریار آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خرقهٔ ژنده‌ام رفو کردند</p></div>
<div class="m2"><p>جرم ناکرده‌ام عفو کردند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قاسم صور شرح حال مرا</p></div>
<div class="m2"><p>کرد انهی به هیئت وزرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شد فروغی شفیعم از سر مهر</p></div>
<div class="m2"><p>سود برآستان خسروچهر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در نهان با «‌شکوه‌» شد همدست</p></div>
<div class="m2"><p>بر خسرو شفاعتی پیوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نامهٔ من به پیشگاه رسید</p></div>
<div class="m2"><p>شرح حالم به عرض شاه رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواستندم ز شهر اصفاهان</p></div>
<div class="m2"><p>این چنین است عادت شاهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرچه دولت رضای من می‌جست</p></div>
<div class="m2"><p>التزامی ز من گرفت نخست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که به ری انزوا کنم پیشه</p></div>
<div class="m2"><p>نکنم در سیاست اندیشه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آنچه گفتند سربسر دادم</p></div>
<div class="m2"><p>مهر و امضای خویش بنهادم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ره تهران گرفتم اندر پیش</p></div>
<div class="m2"><p>تا شوم منزوی به خانهٔ خویش</p></div></div>