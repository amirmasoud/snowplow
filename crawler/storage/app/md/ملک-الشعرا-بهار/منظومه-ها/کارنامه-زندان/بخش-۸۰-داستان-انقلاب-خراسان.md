---
title: >-
    بخش ۸۰ - داستان انقلاب خراسان
---
# بخش ۸۰ - داستان انقلاب خراسان

<div class="b" id="bn1"><div class="m1"><p>و آن قضایا که در خراسان بود</p></div>
<div class="m2"><p>هم ز جهل پلیس نادان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که به یک روز پاسبان بلد</p></div>
<div class="m2"><p>راند قانون به مردم مشهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرد نسخ کله بهانهٔ خوبش</p></div>
<div class="m2"><p>شد به‌دنبال آب و دانهٔ خوبش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گمانش که کاری آسانست</p></div>
<div class="m2"><p>لیک غافل که این خراسانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردمانی به کار دین پابست</p></div>
<div class="m2"><p>همه پابند آن شعارکه هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلق کم‌مایه و کلاه گران</p></div>
<div class="m2"><p>شدت پاسبان مزید بر آن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسته‌ها بسته گشت و غوغا شد</p></div>
<div class="m2"><p>انقلابی عظیم برپا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرد گشتند خلق در مسجد</p></div>
<div class="m2"><p>بهر پاس شعار خویش بجد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تلگرافی به شه فرستادند</p></div>
<div class="m2"><p>کس بدان پیشگه فرستادند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شه ندانست عیب کار کجاست</p></div>
<div class="m2"><p>چون‌ ندانست‌، گم‌شد از ره‌ راست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>داد فرمان که قتل‌عام کنند</p></div>
<div class="m2"><p>کارشان در شبی تمام کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لشگری گردشان گرفت به‌ شب</p></div>
<div class="m2"><p>برشد ازآن حظیره بانگ و جلب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پاسبان و سپاهی از هر سوی</p></div>
<div class="m2"><p>با فقیران شدند روباروی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگرفتندشان به تیغ و به تیر</p></div>
<div class="m2"><p>به فلک برشد آه وبانگ نفیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صحن‌مسجد، به‌خون‌شد آغشته</p></div>
<div class="m2"><p>نیمه‌ای خسته نیمه‌ای کشته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همگی را سحر برون بردند</p></div>
<div class="m2"><p>مرده و زنده خاکشان کردند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>محشری بیگنه هلاک شدند</p></div>
<div class="m2"><p>خاک بودند و باز خاک شدند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن جنایت که ناگهانی بود</p></div>
<div class="m2"><p>همه تقصیر شهربانی بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این اداها که عین گمراهیست</p></div>
<div class="m2"><p>یادگار اصول درگاهیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کاو نه تعلیم پاسبانی داشت</p></div>
<div class="m2"><p>خوی دژخیمی و عوانی داشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود هتاک و ناکس و نامرد</p></div>
<div class="m2"><p>دیگران را به خوی خود پرورد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هست‌دیری کزین اداره جداست</p></div>
<div class="m2"><p>لیک آثار او هنوز بجاست</p></div></div>