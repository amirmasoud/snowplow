---
title: >-
    بخش ۸۲ - گفتار دهم در صفت زن گوید
---
# بخش ۸۲ - گفتار دهم در صفت زن گوید

<div class="b" id="bn1"><div class="m1"><p>چادر و روی‌بند خوب نبود</p></div>
<div class="m2"><p>زن چنان مستمند خوب نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهل اسباب عافیت نشود</p></div>
<div class="m2"><p>زن روبسته‌تربیت نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار زن برتر است از این اسباب</p></div>
<div class="m2"><p>هست‌یکسان‌حجاب‌ و رفع حجاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که اصلاح کار زن خواهی</p></div>
<div class="m2"><p>بی‌سبب عمر خوبشتن کاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زن از اول چنین که بینی بود</p></div>
<div class="m2"><p>هیچ تدبیر، چاره‌اش ننمود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر قوانین ما همین باشد</p></div>
<div class="m2"><p>ابدالدهر زن چنین باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زن به قید حواس خمس دراست</p></div>
<div class="m2"><p>زن نمودار سادهٔ بشر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زن کتاب طبیعت ساده است</p></div>
<div class="m2"><p>زن ز دستور حکمت آزاده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زن اگر جاهلست اگر داناست</p></div>
<div class="m2"><p>خودپسند است‌و خویشتن‌آراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کار او با جمال و زیباییست</p></div>
<div class="m2"><p>هنر و پیشه‌اش خودآراییست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نخواهی که بستن بنماید</p></div>
<div class="m2"><p>به سرتوکه بیش بنماید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باید آزاد سازیش ز قفس</p></div>
<div class="m2"><p>تا فرود آید از هوا و هوس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو مپندار خوی منکر زن</p></div>
<div class="m2"><p>رود ازبیم دوزخ از سر زن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زن به مردان دلیر باشد و چیر</p></div>
<div class="m2"><p>بر خدا نیز هست چیر و دلیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لابه وآه و اشگ و زاری او</p></div>
<div class="m2"><p>هست هرجا سلاح کاری او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کار با این سلاح برنده</p></div>
<div class="m2"><p>می کند با خدا و با بنده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زن خدا را ز جنس نر داند</p></div>
<div class="m2"><p>در دلش لابه را اثر داند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زن که با شوی خودوفا نکند</p></div>
<div class="m2"><p>از خدا نیز هم حیا نکند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>علم‌های خیالی و نقلی</p></div>
<div class="m2"><p>دوست دارد، نه فکری و عقلی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زن دانا اگر بود مغرور</p></div>
<div class="m2"><p>شاید ار باشد از خیانت دور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دگر آن زن که آزموده بود</p></div>
<div class="m2"><p>داستان‌ها بسی شنوده بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سوم‌آن‌زن که‌هست‌شوهردوست</p></div>
<div class="m2"><p>شوهرش نیز دل‌سپردهٔ اوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون‌ازین‌بگذری‌به‌دست‌قضاست</p></div>
<div class="m2"><p>یند و اندرز و قیل‌و قال‌، هباست</p></div></div>