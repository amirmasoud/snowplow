---
title: >-
    بخش ۳۷ - غزل در بیان مذهب نوخاستگان
---
# بخش ۳۷ - غزل در بیان مذهب نوخاستگان

<div class="b" id="bn1"><div class="m1"><p>مرد باید که دل دژم نکند</p></div>
<div class="m2"><p>زندگی صرف رنج و غم نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کم و کیف کارهای جهان</p></div>
<div class="m2"><p>یکسر مو ز کیف کم نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره نفع خود کند خدمت</p></div>
<div class="m2"><p>خدمت خلق یک قلم نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور قسم خورد و توبه کرد ز می</p></div>
<div class="m2"><p>تکیه بر توبه و قسم نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ستم کرد بر کسی، چه زیان</p></div>
<div class="m2"><p>بر خود و عشق خود ستم نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز به پیش صراحی و ساقی</p></div>
<div class="m2"><p>پیش کس پشت خویش خم نکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زندگی حرب‌ و حرب ‌هم خدعه‌است</p></div>
<div class="m2"><p>مرد دانا ز خدعه رم نکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حرف جزء هواست‌، مرد قوی</p></div>
<div class="m2"><p>اعتنایی به مدح و ذم نکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلق گر کند نیم و نیم غنم</p></div>
<div class="m2"><p>گرگ دلسوزی از غنم نکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وقت راز و نیاز، قبلهٔ خویش</p></div>
<div class="m2"><p>جز یکی نازنین‌صنم نکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا توان بود خوش‌، جفا نکشد</p></div>
<div class="m2"><p>تا توان گفت لا، نعم نکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز به شکرلبان درم ندهد</p></div>
<div class="m2"><p>جز به مه‌طلعتان کرم نکند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با رفیقی کزو امیدی نیست</p></div>
<div class="m2"><p>نه رفاقت که یاد هم نکند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقلا گفته‌اند پیش از ما</p></div>
<div class="m2"><p>نم شود هرکسی که نم نکند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن سفرکرده چون ز راه رسید</p></div>
<div class="m2"><p>قصهٔ او به سمع شاه رسید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چند جاسوسش از پس افکندند</p></div>
<div class="m2"><p>بی‌درنگش به محبس افکندند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس شش مه سؤال و استنطاق</p></div>
<div class="m2"><p>نیمه‌جان‌، نیمه کور و نیمه‌چلاق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آخر کارش به ضرب و شتم کشید</p></div>
<div class="m2"><p>پس به دیوانسرای حرب کشید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شد به دیوان حرب مظلمه‌اش</p></div>
<div class="m2"><p>کرد آن محکمه محاکمه‌اش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون نبد مدرکی جز آن مکتوب</p></div>
<div class="m2"><p>اختر هستیش نکرد غروب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لیک شد خلع از شئون نظام</p></div>
<div class="m2"><p>بعد از آن حبس شد سه‌سال تمام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون به محبس نشست بیچاره</p></div>
<div class="m2"><p>گشت جویا ز جفت آواره</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>داد پیغام تا مگر یارش</p></div>
<div class="m2"><p>آید آنجا ز بهر دیدارش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رهن بنهد ز خانه اسبابی</p></div>
<div class="m2"><p>بهر او نانی آرد و آبی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رفت مردی و ماجرا پرسید</p></div>
<div class="m2"><p>خانه را از نگار خالی دید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گشت لختی از این‌ور و آن‌ور</p></div>
<div class="m2"><p>کرد پرسش از این در و آن در</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عاقبت قصه را به دست آورد</p></div>
<div class="m2"><p>بهر بیچاره سر شکست آورد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرد باور نکرد مطلب را</p></div>
<div class="m2"><p>وان حکایات نامرتب را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بستن را چنین تسلی داد</p></div>
<div class="m2"><p>وین‌چنین نزد خویش فتوی داد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کاین سخن‌ها همه گزاف بود</p></div>
<div class="m2"><p>کاهل از کارها معاف بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یا نرفت از پی رسالت من</p></div>
<div class="m2"><p>یا ندادند رخصت رفتن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خفت بر ژنده بالش و بستر</p></div>
<div class="m2"><p>ساخت با نان و آش قصر قجر</p></div></div>