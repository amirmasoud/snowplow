---
title: >-
    بخش ۹ - تمثیل
---
# بخش ۹ - تمثیل

<div class="b" id="bn1"><div class="m1"><p>گشت مردی شریک پرخواری</p></div>
<div class="m2"><p>کرد تقسیم توشه را باری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت یک چیز ازین دوگانه بخواه</p></div>
<div class="m2"><p>خربزه یا که هندوانه بخواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت من هر دوانه می‌خواهم</p></div>
<div class="m2"><p>خربزه‌، هندوانه می‌خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برد مشروطه داغ و چوب و فلک</p></div>
<div class="m2"><p>جای آن ساخت حبس نمره یک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شحنهٔ شهر هر دو وانه گرفت</p></div>
<div class="m2"><p>خربزه داشت هندوانه گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد من‌باب دبه و لنجه</p></div>
<div class="m2"><p>حبس تاریک جفت اشکنجه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست‌بند و شکنجه‌های دگر</p></div>
<div class="m2"><p>تاز‌بانه ز جملگی بدتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاهگاهی هم از پی تحقیق</p></div>
<div class="m2"><p>آب جوشیده می‌شود تزریق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن شنیدم که «‌هایم‌» بدبخت </p></div>
<div class="m2"><p>مبتلا شد بدین شکنجهٔ سخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا گروهی ز عارف و عامی</p></div>
<div class="m2"><p>یار خود سازد، اینت بدنامی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و آن یهودی ز تهمت دگران</p></div>
<div class="m2"><p>بست لب با چنین عذاب گران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وان که او را شکنجه می‌فرمود</p></div>
<div class="m2"><p>مسلمی بود شوم‌تر ز جهود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود تشنه‌، به خون ایرانی</p></div>
<div class="m2"><p>شحنه با دعوی مسلمانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود «‌هایم‌» بدان دلآگاهی</p></div>
<div class="m2"><p>بهتر از صدهزار «‌درگاهی‌»</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کاو به‌ ناحق نبرد نام کسی</p></div>
<div class="m2"><p>وین به خلق افترا ببست بسی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برد از آغاز آن جهول ظلوم</p></div>
<div class="m2"><p>دست در خون عشقی مظلوم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بعد از آن تا زند مؤسس را</p></div>
<div class="m2"><p>زد به تیر بلا‌ «‌مدرس‌» را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شحنهٔ شهر چون که شد فتاک</p></div>
<div class="m2"><p>دگران را ز قتل و فتک چه باک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دارم افسانه‌ای ز «‌درگاهی‌»</p></div>
<div class="m2"><p>شاهکاریست بشنو ار خواهی</p></div></div>