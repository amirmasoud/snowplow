---
title: >-
    بخش ۵۴ - آخر سال
---
# بخش ۵۴ - آخر سال

<div class="b" id="bn1"><div class="m1"><p>ماه اسفند نیز شد گذری</p></div>
<div class="m2"><p>گشت سالی ز عمر ما سپری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت سالی که جز وبال نبود</p></div>
<div class="m2"><p>عمر بود این که رفت‌، سال نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پنج‌ مه‌ زان به‌ حبس و خون‌ جگری</p></div>
<div class="m2"><p>هفت ماه دگر به دربه‌دری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چهل و نه گذشت با اکراه</p></div>
<div class="m2"><p>بر سرم پنجه می‌زند پنجاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر ز پنجه گرفته‌ام به دو دست</p></div>
<div class="m2"><p>کاهنین پنجه‌اش تا شست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرسد عمر من به شصت یقین</p></div>
<div class="m2"><p>زیر این پنجه‌های پولادین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چل و پنجاه‌، آهنین دستند</p></div>
<div class="m2"><p>در گلوها چو پنجه و شستند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمر اکنون ز سال پنجاهم</p></div>
<div class="m2"><p>دامی افکنده بر سر راهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر سلامت رهم ز پنجهٔ دام</p></div>
<div class="m2"><p>چون کشم‌سر ز شصت‌خون‌آشام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با چنین دست کز غمم بسر است</p></div>
<div class="m2"><p>راه پنجاه نیز پر خطر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در شگفتم که با چنین غم و درد</p></div>
<div class="m2"><p>سال دیگر چکار خواهم کرد!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آخر سال را خدا داناست</p></div>
<div class="m2"><p>سال نیکو از اولش پیداست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شب ‌عید است ‌و من ‌غریب‌ و اسیر</p></div>
<div class="m2"><p>بسته تقدیر پنجهٔ تدبیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قرض بالای قرض خوابیده</p></div>
<div class="m2"><p>خانه‌ام چون دلم خرابیده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سال پارینه هم در اول سال</p></div>
<div class="m2"><p>قرض من بود شش هزار ربال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سال بگذشت و تازه شد نوروز</p></div>
<div class="m2"><p>سی‌هزار است قرض من امروز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>می‌رسد نامهٔ وکیل از ری</p></div>
<div class="m2"><p>که بود بی‌نتیجه کوشش وی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که خریدار خانه نایابست</p></div>
<div class="m2"><p>زان که زر در زمانه نایابست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سیم و زر گشته در خزینه نهان</p></div>
<div class="m2"><p>وآن خزینه نهان ز چشم جهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یا شود خرج راه‌آهن شاه</p></div>
<div class="m2"><p>یا بدر می‌رود ز دیگر راه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آنچه دولت ستاند از مردم</p></div>
<div class="m2"><p>هشت عشر از میانه گرددگم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه نادار، خلق و دارا هیچ</p></div>
<div class="m2"><p>همه جا عرضه و تقاضا هیچ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>غیر عمال دولت و تجار</p></div>
<div class="m2"><p>باقی خلق در شکنجه دچار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تاجران هم ازین کساد فره</p></div>
<div class="m2"><p>پس هم می‌زنند یک‌ یک زه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جز دو سه لات روزنامه‌نگار</p></div>
<div class="m2"><p>کز چپ و راست داخلند به کار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>راست چون شاعران عهد قدیم</p></div>
<div class="m2"><p>لب پر از مدح و سرپر از تعظیم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باقی خلق لند لند کنند</p></div>
<div class="m2"><p>گاهی آهسته گاه تند کنند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دسته دسته به شیوهٔ قاچاق</p></div>
<div class="m2"><p>می گریزند سوی هند و عراق</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وان که چون‌منش‌پای رفتن نیست</p></div>
<div class="m2"><p>کرد باید به رنج و زحمت زیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بلدی خانه‌اش کند حرٍّاج</p></div>
<div class="m2"><p>عوض مالیات خانه و باج</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کودکانش ز درس وامانند</p></div>
<div class="m2"><p>همه از تربیت جدا مانند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من در اینجاگرسنه و بیکار</p></div>
<div class="m2"><p>گرد من ده دوازده نان‌خوار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مورد قهر و خانه بر بادم</p></div>
<div class="m2"><p>رفته علم و ادب هم از یادم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>طرفه‌عهدیست کز سیاست‌و زور</p></div>
<div class="m2"><p>کور، شد چشم‌دار و بیناکور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زد به ذوق و ادب معارف جار</p></div>
<div class="m2"><p>شد فلان‌، اوستاد و مرد بهار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نیستم من دریغ مرد هجا</p></div>
<div class="m2"><p>گرچه باشد هجا به وقت‌، بجا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مفت خواهند جست از دستم</p></div>
<div class="m2"><p>که بدین تیر نگرود شستم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هجو اینان وظیفهٔ عالیست</p></div>
<div class="m2"><p>جای یغمای جندقی خالیست</p></div></div>