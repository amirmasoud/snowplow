---
title: >-
    بخش ۵۹ - حکایت جود و بخشش محمود
---
# بخش ۵۹ - حکایت جود و بخشش محمود

<div class="b" id="bn1"><div class="m1"><p>بود محمود زابلستانی</p></div>
<div class="m2"><p>بنده زادی چنان که می‌دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پدرش را کس از بدی اندام</p></div>
<div class="m2"><p>نخرید آن زمان که بود غلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشت محمود هم‌نشان پدر</p></div>
<div class="m2"><p>زرد روی و دراز و بدمنظر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون که شد صیت او بلندآهنک</p></div>
<div class="m2"><p>وز خراسان گرفت تا لب گنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خویشتن را یکی درآینه دید</p></div>
<div class="m2"><p>زشتی خویش را معاینه دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت روزی وزیر دانا را</p></div>
<div class="m2"><p>که بد آمد ز روی ما، ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زردرویی به روی ما بد کرد</p></div>
<div class="m2"><p>نتوان لیک شکوه از خود کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پادشه را صباحتی باید</p></div>
<div class="m2"><p>که بدو مهر خلق بگراید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دربغا کزین دژم رویم</p></div>
<div class="m2"><p>نکشد مهر مردمان سویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت با او وزیر روشن‌رای</p></div>
<div class="m2"><p>باد پاینده عمر بارخدای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چاره این دمامت آسان‌ست</p></div>
<div class="m2"><p>خود علاجش به دست سلطانست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش این رنگ و پیش این رخسار</p></div>
<div class="m2"><p>پرده برکش ز دست گوهربار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گنجت آکنده است و دخل فراخ</p></div>
<div class="m2"><p>کشورت پهن و لشگرت گستاخ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خویشتن را به گنج نامی کن</p></div>
<div class="m2"><p>در بر مردمان گرامی کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با زر سرخ سرخ‌رو گردی</p></div>
<div class="m2"><p>زر نکو بخش تا نکو گردی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از کرم خلق درپذیرندت</p></div>
<div class="m2"><p>رو کرم کن که دوست گیرندت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پادشه گفته وزیر شنید</p></div>
<div class="m2"><p>جود و احسان بکرد و شد جاوید</p></div></div>