---
title: >-
    بخش ۵۰ - داستان  مسافرت  به  یزد
---
# بخش ۵۰ - داستان  مسافرت  به  یزد

<div class="b" id="bn1"><div class="m1"><p>هفته‌ای بود کاندر آن خانه</p></div>
<div class="m2"><p>کرده بودیم گرم‌، کاشانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطلع مهر و ختم تابستان</p></div>
<div class="m2"><p>کودکان رفته در دبیرستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من به عزلت درون خانه مقیم</p></div>
<div class="m2"><p>منزوی‌وار با کتاب ندیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناگه آمد به گوش کوبه ی در</p></div>
<div class="m2"><p>خادم آمد به حالت منکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت باشد پلیس تامینات</p></div>
<div class="m2"><p>بر محمد و آل او صلوات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زن بیچاره‌ام چو این بشنید</p></div>
<div class="m2"><p>رنگ ته مانده‌اش ز روی پرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کردمش خامش و گشادم در</p></div>
<div class="m2"><p>کرد مردی سلام و داد خبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت امر آمده است از تهران</p></div>
<div class="m2"><p>که شوی سوی یزد از اصفاهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست ماشین یزد آماده</p></div>
<div class="m2"><p>بایدت رفت یکه و ساده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم آیم بر رئیس اکنون</p></div>
<div class="m2"><p>تا که آگاه گردم از چه و چون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تاختم گرم بر سرای پلیس</p></div>
<div class="m2"><p>دیدم آنجا نشسته است رئیس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حال پرسیدم و بداد جواب</p></div>
<div class="m2"><p>گشتم از آن جواب در تب و تاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت باید برون شتابی تفت</p></div>
<div class="m2"><p>گفتم اصلا نمی‌توانم رفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کی به رفتن مجال دارم من</p></div>
<div class="m2"><p>که نه مال و نه حال دارم من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود گرفتم که مال باشد و حال</p></div>
<div class="m2"><p>چه کنم با گروه اهل و عیال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتم و آمدم به مسکن خویش</p></div>
<div class="m2"><p>به تسلای خاطر زن خویش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عرض کردم‌ به ‌صد خضوع و خلوص</p></div>
<div class="m2"><p>تلگرافی به دفتر مخصوص</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شمه‌ای ز ابتلا و بد حالی</p></div>
<div class="m2"><p>رفتن یزد و کیسهٔ خالی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لطف‌ شه گشت‌ سوی من معطوف</p></div>
<div class="m2"><p>رفتن یزد بنده شد موقوف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از قراری که دوستان گفتند</p></div>
<div class="m2"><p>هم به ری هم به اصفهان گفتند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بوده «‌مکرم‌»‌» در این عمل مبرم</p></div>
<div class="m2"><p>... بادا به ریش این مکرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وان که بندد به قول مکرم کار</p></div>
<div class="m2"><p>او ز مکرم بتر بود صد بار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پس شش مه ز فرط بد روزی</p></div>
<div class="m2"><p>از فشار معیشت و روزی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تلگرافی دگر نمودم عرض</p></div>
<div class="m2"><p>به شهنشه ز فقر و شدت قرض</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یا نخواند آن شه همایون‌فر</p></div>
<div class="m2"><p>یا که خواند و در او نکرد اثر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس نوشتم عریضهٔ مکتوب</p></div>
<div class="m2"><p>با عبارات موجز و خط خوب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که اگر ماند بایدم اینجا</p></div>
<div class="m2"><p>شود آخر حقوق بنده ادا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این عبارات نیز سود نکرد</p></div>
<div class="m2"><p>نه همین شعله بلکه دود نکرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به فروغی کتابتی کردم</p></div>
<div class="m2"><p>وز قضایا شکایتی کردم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بنوشتم نظیر این ابیات</p></div>
<div class="m2"><p>از برای رئیس تشکیلات‌:</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کار نظم مصالح جمهور</p></div>
<div class="m2"><p>هست مشکل‌ تر از تمام امور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جز به بخت جوان و دانش پیر</p></div>
<div class="m2"><p>جد و جهد و درایت و تدبیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گاه آهن‌دلی و خون‌سردی</p></div>
<div class="m2"><p>گاه نرمی و کدخدا مردی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گه لبی پر ز خنده چون شکر</p></div>
<div class="m2"><p>گه نگاهی برنده چون خنجر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>صبح پیش از اذان سحر خیزی</p></div>
<div class="m2"><p>روز تا نصف شب عرق ریزی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خواندن دوسیه‌، دیدن اخبار</p></div>
<div class="m2"><p>عرض کردن به شه نتیجهٔ کار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مفسدان را مدام پاییدن</p></div>
<div class="m2"><p>خلق را روز و شام پاییدن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دسته‌دسته جماعت مشکوک</p></div>
<div class="m2"><p>متفرق میان شهر و بلوک</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه را داشتن به زیر نظر</p></div>
<div class="m2"><p>خواه در خانه خواه در معبر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سر ز سر عدو درآوردن</p></div>
<div class="m2"><p>رازشان جمله منکشف کردن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پشت هر سرقتی پی افشردن</p></div>
<div class="m2"><p>از عمل پی به عاملش بردن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>راهزن را گرفتن اندر راه</p></div>
<div class="m2"><p>گرچه خود را نهان کند در چاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خط انگشت دزد را دیدن</p></div>
<div class="m2"><p>حال جانی ز چشم فهمیدن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شهرها را به نظم آوردن</p></div>
<div class="m2"><p>خلق را رو به تربیت بردن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سخت مالیدن و ادب کردن</p></div>
<div class="m2"><p>زبن یکی گوش زان یکی گردن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دیدن جمله خلق با یک چشم</p></div>
<div class="m2"><p>لیک گاهی به خنده گاه به خشم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به یکی دست‌، لالهٔ رنگین</p></div>
<div class="m2"><p>به دگر دست‌، دهرهٔ سنگین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خلق را داشتن به بیم و امید</p></div>
<div class="m2"><p>گاه با لطف وگاه با تهدید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یکی از صد هزارها کار است</p></div>
<div class="m2"><p>که به هریک هزار اسرار است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>غیر ازین رنج ‌های پنهانی</p></div>
<div class="m2"><p>که ندانم من و تو خود دانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>من که دیرینه خادم وطنم</p></div>
<div class="m2"><p>پادشاه ممالک سخنم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گرچه در نظم و نثر سلطانم</p></div>
<div class="m2"><p>تابع پادشاه ایرانم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>با اجانب نبوده‌ام دمساز</p></div>
<div class="m2"><p>با اجامر نگشته‌ام همراز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون خود از خادمان ایرانم</p></div>
<div class="m2"><p>قدر خُدّام ملک می ‌دانم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>داغ‌های کهن به دل دارم</p></div>
<div class="m2"><p>در وطن حق آب و گل دارم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کرده‌ام من به خلق خدمت‌ها</p></div>
<div class="m2"><p>دیده‌ام خواری و مشقت‌ها</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>باغ معنی است آبخوردهٔ من</p></div>
<div class="m2"><p>نظم و نثر است زنده کردهٔ من</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چاپلوسانه نیست رفتارم</p></div>
<div class="m2"><p>زان که دل با زبان یکی دارم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بعد سی سال خدمت دایم</p></div>
<div class="m2"><p>چار دوره وکالت دایم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هست دارائیم کتابی چند</p></div>
<div class="m2"><p>خانه و باغ و پنج شش فرزند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>در زمانی که بود روز شلنگ</p></div>
<div class="m2"><p>می‌ شلنگید هر عصازن لنگ</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بنده بودم به‌جای خویش مقیم</p></div>
<div class="m2"><p>پا نکردم درازتر زگلیم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نه نمازم سوی سفارت بود</p></div>
<div class="m2"><p>نه نیازم سوی وزارت بود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نه به شغل اداری‌ام میلی</p></div>
<div class="m2"><p>نه ز انبار دولتم کیلی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بنشستم در آشیانهٔ خویش</p></div>
<div class="m2"><p>گم شدم در کتابخانهٔ خویش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>در دل خانه مختفی گشتم</p></div>
<div class="m2"><p>غرق کار معارفی گشتم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>پنج شش سال منزوی بودم</p></div>
<div class="m2"><p>گوش بر حکم پهلوی بودم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چون که نو گشت سال پارینه</p></div>
<div class="m2"><p>چرخ‌، نو کرد کین دیرینه</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دستلافی طراز جیبم شد</p></div>
<div class="m2"><p>عیدی کاملی نصیبم شد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>خورده بودم برای کسب هنر</p></div>
<div class="m2"><p>چهل و پنج سال خون جگر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>در صفاهان ز فرط رنج و ملال</p></div>
<div class="m2"><p>همه از یاد من برفت امسال</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>لیک ازین حال شکوه سر نکنم</p></div>
<div class="m2"><p>شکر شه کافرم اگر نکنم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>همه دانند بیگناهم من</p></div>
<div class="m2"><p>خود گرفتم که روسیاهم من</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>لیک پنهان نمی کنم غم خویش</p></div>
<div class="m2"><p>تات سازم شریک ماتم خویش</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>تو امیری و صاحب جاهی</p></div>
<div class="m2"><p>چاکر صادق شهنشاهی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>زین ستمدیدگان حمایت کن</p></div>
<div class="m2"><p>حال ما را به شه حکایت کن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>هرچه بودم به شهر ری موجود</p></div>
<div class="m2"><p>رهن شد در بر رحیم جهود</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>باورت نیست از محله بپرس</p></div>
<div class="m2"><p>زان رحیم رجیم دله بپرس</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بختم از خشم شاه برگشته</p></div>
<div class="m2"><p>دستم از پا درازتر گشته</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>یا اجازت دهد شه آفاق</p></div>
<div class="m2"><p>که رَوَم من به سوی هند و عراق</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>یا ازین ابتلا رها کندم</p></div>
<div class="m2"><p>خط آزادگی عطا کندم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تا به کسب معاش پردازم</p></div>
<div class="m2"><p>دفتر و نامه منتشر سازم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>هم درین ماه قطعه‌ ای گفتم</p></div>
<div class="m2"><p>وندر آن دُرّ معرفت سفتم</p></div></div>