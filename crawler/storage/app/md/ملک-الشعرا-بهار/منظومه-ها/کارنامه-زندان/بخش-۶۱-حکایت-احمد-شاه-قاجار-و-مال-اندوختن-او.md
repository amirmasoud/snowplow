---
title: >-
    بخش ۶۱ - حکایت احمد شاه قاجار و مال اندوختن او
---
# بخش ۶۱ - حکایت احمد شاه قاجار و مال اندوختن او

<div class="b" id="bn1"><div class="m1"><p>این چنین بود احمد قاجار</p></div>
<div class="m2"><p>شاه مشروطه بود و کم آزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه زر ماهیانه بگرفتی</p></div>
<div class="m2"><p>مبلغی زان به گنج بنهفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دادمش من به نوبهار بسی</p></div>
<div class="m2"><p>پند چون دُرّ شاهوار بسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم آن شه که تنگ‌چشم بود</p></div>
<div class="m2"><p>دل مردم ازو به خشم بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دل خلق شد به خشم از شاه</p></div>
<div class="m2"><p>زودش از گاه افکنند به چاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون رعیت ز شه شود رنجه</p></div>
<div class="m2"><p>گرددش سست زور سرپنجه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا رعایا شوند بروی چیر</p></div>
<div class="m2"><p>یا سپاهی بر او شوند دلیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیل زوری و تیز چنگالی</p></div>
<div class="m2"><p>نکند چارهٔ بد اقبالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشنید و ملول گشت از من</p></div>
<div class="m2"><p>دید بر من به دیدهٔ دشمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من از آن روز دم فرو بستم</p></div>
<div class="m2"><p>دیرگه لب ز گفتگو بستم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلق از او یک به یک نفور شدند</p></div>
<div class="m2"><p>دور بودند، باز دور شدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روز می‌جست خصم فرزانه</p></div>
<div class="m2"><p>تاکند بازیئی درین خانه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دید چون خلق را ز شاه بری</p></div>
<div class="m2"><p>بازیئی کرد بهر شاه بری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رخ نهان کرد و اسب تازی کرد</p></div>
<div class="m2"><p>با شه آغاز پیل‌بازی کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زد وزیران شاه را به زمین</p></div>
<div class="m2"><p>ساخت از خود پیادهٔ فرزین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مات شد شاه ما در اول دست</p></div>
<div class="m2"><p>و آن پیاده به‌جای شاه نشست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاه ما بد ضعیف و سست‌نهاد</p></div>
<div class="m2"><p>ما پراکنده و حریف استاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دل‌ شه بود خوش به سیم و زرش</p></div>
<div class="m2"><p>وز رعیت نداشت دل خبرش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت در غرب اگر کلم کارم</p></div>
<div class="m2"><p>به که در شرق تاج بگذارم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لاجرم رفت خاسر و مغلوب</p></div>
<div class="m2"><p>اخترش هم به غرب کرد غروب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شه چو از هر جهت تمام آید</p></div>
<div class="m2"><p>امر او را زمانه رام آید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ور بود شاه ناقص از منشی</p></div>
<div class="m2"><p>یا فزون باشد اندرو روشی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شاهیش هر چه استوار بود</p></div>
<div class="m2"><p>از همان راه رخنه‌دار شود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شه گرش سوء ظن مدام بود</p></div>
<div class="m2"><p>زندگانی بر او حرام بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وگرش حسن ظن تمام افتد</p></div>
<div class="m2"><p>از ره دانه‌ای به دام افتد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جود بیحد، کند به فقر دچار</p></div>
<div class="m2"><p>بخل و امساک‌، خواری آرد بار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کبر و نخوت عدو کند ایجاد</p></div>
<div class="m2"><p>وز تواضع جری شوند آحاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لهو دایم ثقیل سازد خون</p></div>
<div class="m2"><p>ثقل پیوسته می کشد به جنون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عفو و اغماض چون ز حد گذرد</p></div>
<div class="m2"><p>جرم افراد از عدد گذرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پادشه کاو به خلق کین دارد</p></div>
<div class="m2"><p>خویش را روز و شب غمین دارد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کینه و قهر چون شود افزون</p></div>
<div class="m2"><p>رود امید از میانه برون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر کسی کرد یک خطا ناگاه</p></div>
<div class="m2"><p>چون ندارد امید عفو ز شاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صد خطا می‌کند فزون ز نخست</p></div>
<div class="m2"><p>خون کند هر که دست از جان شست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شه قهار و خسرو خونریز</p></div>
<div class="m2"><p>رود آنجا که نادر و پرویز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کینه‌جویی ز شه روا نبود</p></div>
<div class="m2"><p>کینه‌جو به که پادشاه نبود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هرکرا نیست قصد پادشهی</p></div>
<div class="m2"><p>سزدش گر نوید عفو دهی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خسروانی که عاقبت سنجند</p></div>
<div class="m2"><p>از نصیحتگران نمی‌رنجند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چیره چون بیم برامید آید</p></div>
<div class="m2"><p>آن زمان دشمنی پدید آید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وگر امید چیره گشت به بیم</p></div>
<div class="m2"><p>خواجه گردد به بندگان تسلیم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>داشت باید به مکر و فن جاوبد</p></div>
<div class="m2"><p>خلق را در میان بیم و امید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>لطف کن آن که را به تو قهر است</p></div>
<div class="m2"><p>قهر زهر است‌ولطف‌پازهر است</p></div></div>