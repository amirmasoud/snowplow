---
title: >-
    بخش ۵۸ - حکایت پیشوای سمرقند
---
# بخش ۵۸ - حکایت پیشوای سمرقند

<div class="b" id="bn1"><div class="m1"><p>در سمرقند پیشوایی بود</p></div>
<div class="m2"><p>خلق را حجت خدایی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وندرآن شهر بود سرهنگی</p></div>
<div class="m2"><p>شحنه‌ای‌، ظالمی‌، قوی‌چنگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ستم خلق پیشه‌ور افشرد</p></div>
<div class="m2"><p>پیشه‌ور شکوه پیشوا را برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت شیخا برس به احوالم</p></div>
<div class="m2"><p>زبن ستم کاره واستان مالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیشوا بس نبود با سرهنگ</p></div>
<div class="m2"><p>گفت با دادخواه از دل تنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبرکن تا خدا کند کاری</p></div>
<div class="m2"><p>مر مرا دردسر مده باری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت با اشک تفته و دم سرد</p></div>
<div class="m2"><p>چون تویی سر، کجا بریم این‌درد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر نه‌تنها به‌تاج‌درخرداست</p></div>
<div class="m2"><p>گاهگاهی هم از در درد است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکرا بر سران سری باید</p></div>
<div class="m2"><p>در سرش درد سروری باید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مهتری سر بسر خطر باشد</p></div>
<div class="m2"><p>غم و تیمار و دردسر باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شیخ گنجی هزینه کرد بزرگ</p></div>
<div class="m2"><p>تا مر آن گله را رهاند ز گرگ</p></div></div>