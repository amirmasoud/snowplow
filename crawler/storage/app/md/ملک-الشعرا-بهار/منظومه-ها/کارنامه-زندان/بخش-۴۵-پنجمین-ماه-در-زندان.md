---
title: >-
    بخش ۴۵ - پنجمین ماه در زندان
---
# بخش ۴۵ - پنجمین ماه در زندان

<div class="b" id="bn1"><div class="m1"><p>تیر و مرداد هم به بنده گذشت</p></div>
<div class="m2"><p>مدت حبس من تمام نگشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب شد برف قلهٔ توچال</p></div>
<div class="m2"><p>یخ فراوان نماند در یخچال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنکی‌های قوم لیک بجاست</p></div>
<div class="m2"><p>وز دل سردشان عناد نکاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد هوا گرم و گرم شد محبس</p></div>
<div class="m2"><p>پخته گشتند مرغ‌ها به قفس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دم به دم محبسی به حبس رود</p></div>
<div class="m2"><p>لیک محبس فراخ‌تر نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حبسگاه موقتی تنگ است</p></div>
<div class="m2"><p>همه‌جا بین حبسیان جنگ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در اطاقی که پنج شش گز نیست</p></div>
<div class="m2"><p>شصت‌ و نه‌ محبسی نماید زیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه عریان ز شدت تب و تاب</p></div>
<div class="m2"><p>گرد هم درتنیده چون گرداب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیر هفتاد ساله در ناله</p></div>
<div class="m2"><p>همدمش طفل یازده ساله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن‌یکی دزد و آن دگر جاسوس</p></div>
<div class="m2"><p>وآن دگر، پار بوده نوکر روس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن یکی کرده با زنش دعوا</p></div>
<div class="m2"><p>آن دگر قرض خود نکرده ادا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن یکی هست مفلس ومفلوک</p></div>
<div class="m2"><p>سند تابعیتش مشکوک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دگری گفته من طلا دارم</p></div>
<div class="m2"><p>در دل خاک گنج‌ها دارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیک در پای میز استنطاق</p></div>
<div class="m2"><p>کرده حاشا ز فرط جهل و نفاق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نک دو سال‌ است کاندرین دهلیز</p></div>
<div class="m2"><p>می کند جان و می‌خورد مهمیز</p></div></div>