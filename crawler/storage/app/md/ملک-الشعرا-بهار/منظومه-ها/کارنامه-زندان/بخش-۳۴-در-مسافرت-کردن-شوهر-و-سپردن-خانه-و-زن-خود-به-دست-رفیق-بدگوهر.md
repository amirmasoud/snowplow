---
title: >-
    بخش ۳۴ - در مسافرت کردن شوهر و سپردن خانه و زن خود به‌دست رفیق بدگوهر
---
# بخش ۳۴ - در مسافرت کردن شوهر و سپردن خانه و زن خود به‌دست رفیق بدگوهر

<div class="b" id="bn1"><div class="m1"><p>دیرگاهی بر این وَتیره گذشت</p></div>
<div class="m2"><p>روز رخشان و شام تیره گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشت ناگاه شوی زن سفری</p></div>
<div class="m2"><p>گفت زن‌: بایدت مرا ببری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت‌مرد: ‌این‌سفرنه‌دلخواه است</p></div>
<div class="m2"><p>که رئیس اداره همراه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم به پاس اثاثهٔ خانه</p></div>
<div class="m2"><p>بایدت ماند، پُر مزن چانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قضا نیستی تو هم تنها</p></div>
<div class="m2"><p>پیشت آید رفیق تازهٔ ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کند با تو خانمش یاری</p></div>
<div class="m2"><p>او خود از توکند نگهداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست به از رفیق وجدان کیش</p></div>
<div class="m2"><p>که سپردن بدو توان زن خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که فاسد شدست خوی بشر</p></div>
<div class="m2"><p>نه برادر بود امین نه پسر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جهان اعتماد و اطمینان</p></div>
<div class="m2"><p>نیست الا به مرد با وجدان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دین و ایمان همه خرافاتست</p></div>
<div class="m2"><p>مایهٔ کین و اختلافاتست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس فقیها که دام شرعی ساخت</p></div>
<div class="m2"><p>مومنان را میان دام انداخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شیخ دیگرکلاه شرغی دوخت</p></div>
<div class="m2"><p>تا قبای ترا به غیر فروخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زوجه خلق را دهند طلاق</p></div>
<div class="m2"><p>بهر غیری کنند عقد و صداق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیک مردی که اهل وجدانست</p></div>
<div class="m2"><p>دلش از فعل بد هراسانست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>او بدی را به چشم بد بیند</p></div>
<div class="m2"><p>شرر تو زیان خود بیند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست در نیکیش امید بهشت</p></div>
<div class="m2"><p>زان که باشد به طبع نیک سرشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وز بدی دوزخش نترساند</p></div>
<div class="m2"><p>که بدی را به طبع بد داند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نکند بدکه بد به طبع بد است</p></div>
<div class="m2"><p>نیک باشد که نیکی از خرد است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نیک و بد را شناسد از وجدان</p></div>
<div class="m2"><p>هست وجدان برابرش میزان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زن اگر چند نرم‌تر شده بود</p></div>
<div class="m2"><p>ز ابتلائی دلش خبر شده بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیمی افتاده بود در دل او</p></div>
<div class="m2"><p>نگران بود ازین سفر دل او</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لیک شوهر شکیب فرمودش</p></div>
<div class="m2"><p>بوسه‌ها داد و کرد بدرودش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دست وجدان‌فروش را بفشرد</p></div>
<div class="m2"><p>رفت و آن دنبه را به گرگ سپرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رفت شوی و رفیق کج‌بنیاد</p></div>
<div class="m2"><p>به فریب زن رفیق ستاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>روزی آمد به نزد آن دلبر</p></div>
<div class="m2"><p>ساخته از دروغ مژگان تر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت‌ زن‌:‌ چیست‌؟ گفت چیزی‌ نیست</p></div>
<div class="m2"><p>آن که در دل غمی ندارد کیست‌؟‌!</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دگرین روز هم بدین منوال</p></div>
<div class="m2"><p>شد به نزدیک آن بدیع جمال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چشم‌ها سرخ و مژه اشک‌آلود</p></div>
<div class="m2"><p>گونه‌های زرد و پای چشم کبود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زن ز نازک‌دلی به تنگ آمد</p></div>
<div class="m2"><p>پای خود داریش به سنگ آمد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قسمش داد و گفت‌: دردت چیست‌؟</p></div>
<div class="m2"><p>چشم سرخ‌ و رخان زردت چیست‌؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفت اندر فشار وجدانم</p></div>
<div class="m2"><p>راز کس فاش کرد نتوانم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سومین روز ساخت آن مکار</p></div>
<div class="m2"><p>خویش را زرد و لاغر و بیمار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بود بازیگری نمایش باز</p></div>
<div class="m2"><p>لاجرم کرد این نمایش‌، ساز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رفت‌ و خود را بدین ضعیفه نمود</p></div>
<div class="m2"><p>صد هزاران غمش به غم افزود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کفت زن چند ازبن نهفتن راز</p></div>
<div class="m2"><p>چیست این روی زرد و گرم و گداز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خانمت‌ در کجاست کاین‌ دو سه‌ روز</p></div>
<div class="m2"><p>اندرین جا نشد جمال‌افروز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>این چه حالی و این چه ترکیبی است</p></div>
<div class="m2"><p>این چه وضعی و این چه ترتیبی است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جای غمخواری از من دلریش</p></div>
<div class="m2"><p>بر غم من فزوده‌ای غم خویش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گرچه چیزی ز تو نفهمیدم</p></div>
<div class="m2"><p>به خدا کز تو سخت رنجیدم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون شد آن ریوساز حیلت کر</p></div>
<div class="m2"><p>در دل خویشتن سوار به خر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>صیدش اندرکنار دانه رسید</p></div>
<div class="m2"><p>تیری افکند و بر نشانه رسید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفت اکنون که فحش خواهی داد</p></div>
<div class="m2"><p>گویم این راز هرچه بادا باد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پای رنجش چو در میان آمد</p></div>
<div class="m2"><p>راز پوشیده بر زبان آمد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>داد سوگند مرد حیلت‌ساز</p></div>
<div class="m2"><p>که زن آن راز را نگو باز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گفت بار نخست کاینجا من</p></div>
<div class="m2"><p>آمدم میهمان به همره زن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وز تو آن حجب و شرم را دیدم</p></div>
<div class="m2"><p>در دل خود بسی پسندیدم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون که بیرون شدیم ازین خانه</p></div>
<div class="m2"><p>شرح دادم ز بهر جانانه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گفتمش پند گیر ازین خانم</p></div>
<div class="m2"><p>عقل و دانش‌پذیر ازین خانم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که به چندین عفاف وسنگینی</p></div>
<div class="m2"><p>داشت زیبندگی و رنگینی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زن‌چو این سرزنش ز من بشنید</p></div>
<div class="m2"><p>بی محابا به روی من بدوید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گفت محو جمال او شده‌ای</p></div>
<div class="m2"><p>عاشق خط و خال او شده‌ای</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خوردم از بهر او قسم بسیار</p></div>
<div class="m2"><p>تاکه قانع شد وگرفت قرار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>لیک در قلبش این ملال بماند</p></div>
<div class="m2"><p>گفتگو طی شد و خیال بماند</p></div></div>