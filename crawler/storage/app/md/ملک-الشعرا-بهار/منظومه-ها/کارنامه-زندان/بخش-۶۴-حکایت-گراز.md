---
title: >-
    بخش ۶۴ - حکایت گراز
---
# بخش ۶۴ - حکایت گراز

<div class="b" id="bn1"><div class="m1"><p>در خیابان باغ‌، فصل بهار</p></div>
<div class="m2"><p>می‌چمید آن گراز پست شعار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبلی چند از قفای گراز</p></div>
<div class="m2"><p>بر سر شاخ گل مدیح طراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه به بحر طویل و گاه خفیف</p></div>
<div class="m2"><p>می‌سرودند شعرهای لطیف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در قفای گراز خودکامه</p></div>
<div class="m2"><p>این چکامه سرودی آن چامه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن یکی نغمهٔ مغانی داشت</p></div>
<div class="m2"><p>وان دگر لحن خسروانی داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغکان گه به شاخه گاه به ساق</p></div>
<div class="m2"><p>مترنم به شیوهٔ عشاق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه ز گلبن به خاک جستندی</p></div>
<div class="m2"><p>که به زیر ستاک جستندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوک نادان به عادت جهال</p></div>
<div class="m2"><p>شده سرخوش به نغمهٔ قوال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دُم به تحسینشان بجنباندی</p></div>
<div class="m2"><p>گوش وا کردی و بخواباندی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیز گاهی سری تکان دادی</p></div>
<div class="m2"><p>خبرگی‌های خود نشان دادی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرغکان لیک فارغ از آن راز</p></div>
<div class="m2"><p>بی نیاز از قبول و ردّ گراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان به دنبال او روان بودند</p></div>
<div class="m2"><p>که فقیران گرسنگان بودند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او دریدی به گاز خویش زمین</p></div>
<div class="m2"><p>تا خورد بیخ لاله و نسرین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>و آمدی زان شیارهاش پدید</p></div>
<div class="m2"><p>کرم‌هایی لطیف‌، زرد و سفید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بلبلان رزق خویش می‌خوردند</p></div>
<div class="m2"><p>همه بر خوک چاشت می‌کردند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جاهلانی که گشته‌اند عزیز</p></div>
<div class="m2"><p>نه به‌حق بل به نیش و ناخن تیز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیششان مرغکان ترانه کنند</p></div>
<div class="m2"><p>تا که تدبیر آب و دانه کنند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خوک نادان به لاله‌زار اندر</p></div>
<div class="m2"><p>مرزها را نموده زیر و زبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لقمه‌هایی کلان برانگیزد</p></div>
<div class="m2"><p>خرده‌هایی از آن فرو ریزد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرغکان خرده‌هاش چینه کنند</p></div>
<div class="m2"><p>وز پی کودکان هزینه کنند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نغمه‌خوانان به بوی چینه چمان</p></div>
<div class="m2"><p>نغمه‌هاشان مدیح محتشمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حمقا آن به ریش می‌گیرند</p></div>
<div class="m2"><p>وز کرامات خویش می‌گیرند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لیک غافل که جز چرندی نیست</p></div>
<div class="m2"><p>غیر افسوس و ریشخندی نیست</p></div></div>