---
title: >-
    بخش ۸۸ - خاتمهٔ کتاب شهید بلخی
---
# بخش ۸۸ - خاتمهٔ کتاب شهید بلخی

<div class="b" id="bn1"><div class="m1"><p>بود روزی شهید بنشسته</p></div>
<div class="m2"><p>درکتبخانه در به رخ بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسخها چیده از یمین و یسار</p></div>
<div class="m2"><p>بود سرگرم سیر آن گلزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناگه آمد ز در، گرانجانی</p></div>
<div class="m2"><p>خشک‌ مغزی‌، عظیم نادانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت با شیخ‌، کای ستوده لقا</p></div>
<div class="m2"><p>از چه ایدر نشسته‌ای تنها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیخ برداشت از مطالعه سر</p></div>
<div class="m2"><p>وز شکرخنده ربخت گنج گهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کفت آری چو خواجه پیدا شد</p></div>
<div class="m2"><p>بنده تنها نبود و تنها شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکرا نور معرفت به سرست</p></div>
<div class="m2"><p>گرچه‌تنهاست‌یک‌جهان‌بشرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وان که را مغز بی‌فروغ و بهاست</p></div>
<div class="m2"><p>در میان هزارکس تنهاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ثمر عمر، عقل و تجربت است</p></div>
<div class="m2"><p>تجربت بیخ علم و معرفت است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این همه علم‌ها که مشتهرند</p></div>
<div class="m2"><p>حاصل زندگانی بشرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در کتب حرف‌ها که انبارست</p></div>
<div class="m2"><p>جوهر و مایه‌های اعمارست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عمرها را اگر عیارستی</p></div>
<div class="m2"><p>صفحهٔ علم پیلوارستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکتابی کش از خرد بهریست</p></div>
<div class="m2"><p>نقد عمری و حاصل دهریست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر نادان کتاب‌، کانایی است</p></div>
<div class="m2"><p>زی حردمند، جان دانایی است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیش او عقده بر زبان دارد</p></div>
<div class="m2"><p>پیش این زنده است و جان دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرکرا با کتاب کار افتاد</p></div>
<div class="m2"><p>عمرش‌از شصت تا هزار افتاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وان که‌ در خلوتش کتب‌ خوانیست</p></div>
<div class="m2"><p>خاطرش فارغ از پریشانی است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکه شد باکتاب یار و ندیم</p></div>
<div class="m2"><p>یاد نارد ز دوستان قدیم</p></div></div>