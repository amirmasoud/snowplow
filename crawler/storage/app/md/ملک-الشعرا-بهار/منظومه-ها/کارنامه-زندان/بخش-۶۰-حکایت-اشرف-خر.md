---
title: >-
    بخش ۶۰ - حکایت اشرف خر
---
# بخش ۶۰ - حکایت اشرف خر

<div class="b" id="bn1"><div class="m1"><p>خود شنیدی حدیث اشرف خر</p></div>
<div class="m2"><p>که مثل شد به گرد کردن زر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود تاتار زاده‌ای نادان</p></div>
<div class="m2"><p>پور تیمورتاش بن چوپان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه کیاست نه مردمی نه شرف</p></div>
<div class="m2"><p>نام خود ساخته ملک اشرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس مرگ حسن برادر خویش</p></div>
<div class="m2"><p>پادشه گشت اشرف بد کیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آذر آبادگان مسخر داشت</p></div>
<div class="m2"><p>در دل اندیشه‌های بیمر داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دنائت به گنج شد طالب</p></div>
<div class="m2"><p>طمع زربه طبع شد غالب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ستم‌، کار خلق یکسره کرد</p></div>
<div class="m2"><p>هرکجا بود زر مصادره کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه زر داشت زار شدکارش</p></div>
<div class="m2"><p>گشت رخ زرد همچو دینارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن که زرخرده‌ای به دامان داشت</p></div>
<div class="m2"><p>روی مانند غنچه پنهان داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وانکه دانگی بدست آوردی</p></div>
<div class="m2"><p>همچو گل در شکم نهان کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان فقیران کسی که دیده شدی</p></div>
<div class="m2"><p>شکمش همچو گل دریده شدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو گل هر که در میان زر داشت</p></div>
<div class="m2"><p>شکمش بردرید و زر برداشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در درازای ده دوازده سال</p></div>
<div class="m2"><p>گنج‌ها آکنید از زر و مال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زر پیاپی بدست آوردی</p></div>
<div class="m2"><p>نه به کس دادی و نه خود خوردی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ظلم اشرف ز حد و مر بگذشت</p></div>
<div class="m2"><p>عملش در زمانه شایع گشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه همسایگان شدند آگاه</p></div>
<div class="m2"><p>که رعیت بری بود از شاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>میر قپچاق بود جانی بیک</p></div>
<div class="m2"><p>تاجداری بزرگ و خانی نیک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حمله‌ور شد به آذرآبادان</p></div>
<div class="m2"><p>گشت غالب بر اشرف نادان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شد گرفتار و کشته شد اشرف</p></div>
<div class="m2"><p>جانش از تن برفت و گنج از کف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شد به وزر و وبال آغشته</p></div>
<div class="m2"><p>هم به بخل و خری مثل گشته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هفتصد بد به سال و پنجه و هشت</p></div>
<div class="m2"><p>کاشرف خر اسیر ترکان گشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مثلی گشت کار اشرف خر</p></div>
<div class="m2"><p>او مظالم ببرد و ترکان زر</p></div></div>