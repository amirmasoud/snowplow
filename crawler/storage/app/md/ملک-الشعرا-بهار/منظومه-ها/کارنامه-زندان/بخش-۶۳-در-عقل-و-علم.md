---
title: >-
    بخش ۶۳ - در عقل و علم
---
# بخش ۶۳ - در عقل و علم

<div class="b" id="bn1"><div class="m1"><p>خرد و داد و راستی کرم است</p></div>
<div class="m2"><p>کژی و جهل وکاستی ستم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقل ار هیچ علم ناموزد</p></div>
<div class="m2"><p>وز ادب نکته‌ای نیندوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان راه راست می‌پوبد</p></div>
<div class="m2"><p>وز کژی‌ها کرانه می‌جوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ورشود پادشاه کشور خویش</p></div>
<div class="m2"><p>بالد از عقل‌ عدل گستر خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که با خلق نیکخوی بود</p></div>
<div class="m2"><p>نگسلد رشته گرچه موی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرکشد خلق رشته‌، بگذارد</p></div>
<div class="m2"><p>ور رها کرد او نگهدارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وآن که‌ را عقل‌ و عدل نیست به‌طبع</p></div>
<div class="m2"><p>الکن است ار کند قرائت سبع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علم او گر بزی و ریمنی است</p></div>
<div class="m2"><p>گر به ‌خورشید سرکشد دنی ‌است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اصل‌ هایی ز علم بگزیند</p></div>
<div class="m2"><p>که به وفق خیال خود بیند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وان که باشد میانهٔ بد و نیک</p></div>
<div class="m2"><p>در دلش دیو را فرشته شریک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>علمش از دست دیو برهاند</p></div>
<div class="m2"><p>در سرشتش فرشتگی ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وان که باشد فرشته از اول</p></div>
<div class="m2"><p>از ملک بگذرد به علم و عمل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وای از آن دیو طبع بدکردار</p></div>
<div class="m2"><p>که نباشد ز علم برخوردار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جاهل‌ و پست و بی کتاب و شرور</p></div>
<div class="m2"><p>ظالم و دون و طامع و مغرور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مفرط و پر طمع به شهوت و کین</p></div>
<div class="m2"><p>نه شرف نی خرد نه داد و نه دین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از حیا و وقار و مردی و قول</p></div>
<div class="m2"><p>متنفر چو دیو از لاحول</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرده پندار و عجب بی‌درمان</p></div>
<div class="m2"><p>سر او را چوگنبد هرمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رفته درگوش او مبالغه‌ای</p></div>
<div class="m2"><p>کرده باورکه هست نابغه‌ای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ویژه کاین آسمان ز مکاری</p></div>
<div class="m2"><p>چندگاهی با وی کند یاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>و ابلهی چند از طریق خری</p></div>
<div class="m2"><p>یا ز راه فریب و حیله گری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وارث کورش و جمش خوانند</p></div>
<div class="m2"><p>داربوش معظمش خوانند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بی‌نصیب آید از مسلمانی</p></div>
<div class="m2"><p>خویش راگم کند ز نادانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نشود ابن سعد سالارش</p></div>
<div class="m2"><p>نیز شمر لعین جلودارش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سیل‌سان بردَوَد به پست و بلند</p></div>
<div class="m2"><p>نشود هیچ چیز پیشش بند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بشکند بند و بگسلد افسار</p></div>
<div class="m2"><p>جفته کوبد به گنبد دوار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اصل‌های کهن براندازد</p></div>
<div class="m2"><p>رسم‌هایی نوین عیان سازد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عالمان را ز خود نفورکند</p></div>
<div class="m2"><p>عاقلان را ز خویش دور کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دوستانش نخست پند دهند</p></div>
<div class="m2"><p>بخردان پند سودمند دهند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>او از آن پندها به خشم آید</p></div>
<div class="m2"><p>دوستان را نکال فرماید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیکمردان و کاردانان را</p></div>
<div class="m2"><p>کشد و برکشد عوانان را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مردم از بیم جان سکوت کنند</p></div>
<div class="m2"><p>مگسان مدح عنکبوت کنند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هرچه کارآگهان زبون گردند</p></div>
<div class="m2"><p>گرد او ناکسان فزون گردند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کار افتد به دست عامی چند</p></div>
<div class="m2"><p>نان شودپخته بهر خامی چند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چرخ چون برکشد اراذل را</p></div>
<div class="m2"><p>گاو مرگی فتد افاضل را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چاپلوسان سویش هجوم کنند</p></div>
<div class="m2"><p>عارفان ترک مرز و بوم کنند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فسخ گردد اصول آزادی</p></div>
<div class="m2"><p>طی شود رسم مردی و رادی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نر گدایان به جان خلق افتند</p></div>
<div class="m2"><p>قوم در حلق و جلق و دلق افتند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اندک اندک شوند خلق فقیر</p></div>
<div class="m2"><p>پر شود گنج پادشاه و وزیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جیب یک شهر می‌شود خالی</p></div>
<div class="m2"><p>تاکه قصری بنا شود عالی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خلق گردند مشرف و جاسوس</p></div>
<div class="m2"><p>ازشرف‌دست‌شسته‌وز ناموس</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تهمت و کذب و کید و غمازی</p></div>
<div class="m2"><p>حسد و شهوت و دغل‌بازی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>این همه عادت عموم شود</p></div>
<div class="m2"><p>کارفرمای مرز وبوم شود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زیردستان به شه نگاه کنند</p></div>
<div class="m2"><p>خلق تقلید پادشاه کنند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کس به فضل و کمال رو نکند</p></div>
<div class="m2"><p>گل جود و نوال بونکند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون شود شورشی به برکه پدید</p></div>
<div class="m2"><p>بر سر آیند جرم‌های پلید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هرچه باشد به قعر آب‌، لجن</p></div>
<div class="m2"><p>بر سر آبدان کند مسکن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>می‌شود تیره سطح صافی آب</p></div>
<div class="m2"><p>آبدانی بدل شود به خلاب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سازد این انقلاب ادباری</p></div>
<div class="m2"><p>این عمل را به مملکت جاری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اهل کشور به مدت دو سه بال</p></div>
<div class="m2"><p>می‌روند آن‌چنان به راه ضلال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که به صد سال عدل و دینداری</p></div>
<div class="m2"><p>نتوانیش باز جای آری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دیده‌ای لکه‌ای که در یک‌دم</p></div>
<div class="m2"><p>بر حریری چکد ز نوک قلم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>صد ره ار شویی و کنیش درست</p></div>
<div class="m2"><p>برنگردد دگر به حال نخست</p></div></div>