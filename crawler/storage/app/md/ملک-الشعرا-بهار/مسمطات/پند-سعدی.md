---
title: >-
    پند سعدی
---
# پند سعدی

<div class="b" id="bn1"><div class="m1"><p>پادشاها ز ستبداد چه داری مقصود</p></div>
<div class="m2"><p>که از این کار جز ادبار نگردد مشهود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جودکن در ره مشروطه که گردی مسجود</p></div>
<div class="m2"><p>«‌شرف مرد به ‌جود است و کرامت به‌ سجود»</p></div></div>
<div class="b2" id="bn3"><p>«‌هر که این هر دو ندارد عدمش به ز وجود»</p></div>
<div class="b" id="bn4"><div class="m1"><p>ملکا جور مکن پیشه و مشکن پیمان</p></div>
<div class="m2"><p>که مکافات خدائیت بگیرد دامان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک بر سر کندت حادثهٔ دور زمان</p></div>
<div class="m2"><p>«‌خاک مصر طرب‌انگیز نبینی که همان‌»</p></div></div>
<div class="b2" id="bn6"><p>«‌خاک مصر است ولی بر سر فرعون و جنود»</p></div>
<div class="b" id="bn7"><div class="m1"><p>ملکا خودسری و جور تو ایران سوز است</p></div>
<div class="m2"><p>به مکافات تو امروز وطن فیروز است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تابش نور مکافات نه از امروز است</p></div>
<div class="m2"><p>«‌این‌همان چشمهٔ خورشیدجهان‌افروز است‌»</p></div></div>
<div class="b2" id="bn9"><p>« که همی تافت بر آرامگه عاد و ثمود»</p></div>
<div class="b" id="bn10"><div class="m1"><p>بیش از این شاها بر ربشهٔ خود تیشه مزن</p></div>
<div class="m2"><p>خون ملت را در ورطهٔ ذلت مفکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیخ خود را به‌ هوا و هوس نفس مکن</p></div>
<div class="m2"><p>«‌قیمت خود به ملاهی و مناهی مشکن‌»</p></div></div>
<div class="b2" id="bn12"><p>« گرت ایمان درست است به روز موعود»</p></div>
<div class="b" id="bn13"><div class="m1"><p>کشت ملت راکردی ز ستم پاک درو</p></div>
<div class="m2"><p>شدکهن قصهٔ چنگیز ز بیداد تو نو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به جهان دل ز چه‌بندی پس‌ازین گفت و شنو</p></div>
<div class="m2"><p>«‌ای که در نعمت و نازی به جهان غره مشو»</p></div></div>
<div class="b2" id="bn15"><p>« که محالست درین مرحله امکان خلود»</p></div>
<div class="b" id="bn16"><div class="m1"><p>بگذر از خطه تبریز و مقام شهداش</p></div>
<div class="m2"><p>بشنوآن قصهٔ جانسوز و دل از غم بخراش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندر آن خطه پس از آن کشش و آن پرخاش</p></div>
<div class="m2"><p>«‌خاک راهی که بر آن می گذری ساکن باش‌»</p></div></div>
<div class="b2" id="bn18"><p>« که‌ عیو‌‌ن است‌ و جفون‌ است‌ و خدود است و قدود»</p></div>
<div class="b" id="bn19"><div class="m1"><p>شاه یک دل نشد وکار هباگشت و هدر</p></div>
<div class="m2"><p>ملت خسته‌، درین مرحله کن فکر دگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پای امید منه بر در شاه خود سر</p></div>
<div class="m2"><p>«‌دست حاجت چو بری ییش خداوندی بر»</p></div></div>
<div class="b2" id="bn21"><p>« که کریم است‌و رحیم‌است و غفوراست و ودود»</p></div>
<div class="b" id="bn22"><div class="m1"><p>شاه خود کیست بدین کبر و انانیت او</p></div>
<div class="m2"><p>تا نکو باشد دربارهٔ ما نیت او</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ما پرستندهٔ حقیم و الوهیت او</p></div>
<div class="m2"><p>« کز ثری تا به ثریا به عبودیت او»</p></div></div>
<div class="b2" id="bn24"><p>«‌همه در ذکر و مناجات و قیامند و قعود»</p></div>
<div class="b" id="bn25"><div class="m1"><p>سر زند کوکب مشروطه ز گردون کمال</p></div>
<div class="m2"><p>به سر آید شب هجران و دمد صبح وصال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کار نیکو شود از فرّ خدای متعال</p></div>
<div class="m2"><p>«‌ای که در شدت فقری و پریشانی حال‌»</p></div></div>
<div class="b2" id="bn27"><p>«‌صبرکن کاین دوسه روزی به‌سر آید معدود»</p></div>
<div class="b" id="bn28"><div class="m1"><p>جز خطاکاری ازین شاه نمی باید خواست</p></div>
<div class="m2"><p>کانچه ما در او بینیم سراسر به خطاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مدهش پند که بر بدمنشان پند هباست</p></div>
<div class="m2"><p>«‌پند سعدی که کلید در گنج سعداست‌»</p></div></div>
<div class="b2" id="bn30"><p>«‌نتواند که به‌جا آورد الا مسعود»</p></div>