---
title: >-
    در منقبت حضرت حجة (‌ع‌)
---
# در منقبت حضرت حجة (‌ع‌)

<div class="b" id="bn1"><div class="m1"><p>مژده که روی خدا ز پرده برآمد</p></div>
<div class="m2"><p>آیت داور به خلق جلوه گر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌خبران را ز فیض کل خبر آمد</p></div>
<div class="m2"><p>مظهرکل در لباس جزء درآمد</p></div></div>
<div class="b2" id="bn3"><p>معنی واجب گرفت صورت امکان</p></div>
<div class="b" id="bn4"><div class="m1"><p>شعشعه گسترد جلوهٔ صمدانی</p></div>
<div class="m2"><p>گشت عیان سرّ صادرات نهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طاق طلب را قویم گشت مبانی</p></div>
<div class="m2"><p>شاهد غیبی رسید و داد نشانی</p></div></div>
<div class="b2" id="bn6"><p>از لمعات جمال قادر سبحان</p></div>
<div class="b" id="bn7"><div class="m1"><p>از فلک کون تافت اختر تجرید</p></div>
<div class="m2"><p>نفس احد سرزد ازهیولی توحید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لم‌یلد امروز یافت کسوت تولید</p></div>
<div class="m2"><p>آنکه بدو زنده گشت هر سه موالید</p></div></div>
<div class="b2" id="bn9"><p>وآنکه بدو تازه گشت چار خشیجان</p></div>
<div class="b" id="bn10"><div class="m1"><p>عقل نخستین بزرگ صادر اول</p></div>
<div class="m2"><p>کالبد مستنیر و جان ممثل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راه بدی را یکی فروخته مشعل</p></div>
<div class="m2"><p>هادی و مهدی سمی احمد مرسل</p></div></div>
<div class="b2" id="bn12"><p>حجة غایب ولی ایزد منان</p></div>
<div class="b" id="bn13"><div class="m1"><p>قاعده ‌پرداز کارگاه الهی</p></div>
<div class="m2"><p>راز جهان را دلش خبیرکماهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جاهش برتر ز حد لایتناهی</p></div>
<div class="m2"><p>فکربه کنه جلال و قدرش واهی</p></div></div>
<div class="b2" id="bn15"><p>عقل به قرب کمال و جاهش حیران</p></div>
<div class="b" id="bn16"><div class="m1"><p>شاهد غیبی و دلبر ازلی اوست</p></div>
<div class="m2"><p>پرده‌نشین حریم لم‌یزلی اوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باری سر خفی و نور جلی اوست</p></div>
<div class="m2"><p>مرشد و مولا و پیشوا و ولی اوست</p></div></div>
<div class="b2" id="bn18"><p>خواهش پیدا شمار و خواهش پنهان</p></div>
<div class="b" id="bn19"><div class="m1"><p>ای قمر تابناک برج امامت</p></div>
<div class="m2"><p>وی گهر آبدار درج کرامت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای به قد و قامت تو شور قیامت</p></div>
<div class="m2"><p>خیز و برافراز یک ره آن قد و قامت</p></div></div>
<div class="b2" id="bn21"><p>خیز و برافروز یک‌ره آن رخ رخشان</p></div>
<div class="b" id="bn22"><div class="m1"><p>غیر تو ای کنز مخفی احدیت</p></div>
<div class="m2"><p>کیست که پیدا کند کنوز هویت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از تو عیان است جلوه صمدیت</p></div>
<div class="m2"><p>هیچ تو را با خدای نیست دوئیت</p></div></div>
<div class="b2" id="bn24"><p>ذات تو با ذات هواست یکسر و یکسان</p></div>
<div class="b" id="bn25"><div class="m1"><p>خیز و عیان کن به خلق جلوهٔ دادار</p></div>
<div class="m2"><p>خیز که حق خفت و گشت باطل بیدار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر نکنی پای در رکاب ظفر یار</p></div>
<div class="m2"><p>منتظرانت زنند ای شه ابرار</p></div></div>
<div class="b2" id="bn27"><p>دست به دامان شهریار خراسان</p></div>
<div class="b" id="bn28"><div class="m1"><p>زادهٔ موسی که طور اوست حریمش</p></div>
<div class="m2"><p>عیسی گردون‌نشین غلام قدیمش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر دو جهان ریزه‌خوار کف کریمش</p></div>
<div class="m2"><p>آن که به فرمان واجب‌التعظیمش</p></div></div>
<div class="b2" id="bn30"><p>بر جهد از نقش پرده ضیغم غژمان</p></div>
<div class="b" id="bn31"><div class="m1"><p>خرگه ناسوت هست پایهٔ پستش</p></div>
<div class="m2"><p>مسند لاهوت جایگاه نشستش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عقل خردمند گشته واله و مستش</p></div>
<div class="m2"><p>غیرخدایش‌ مخوان که ‌هست ‌شکستش</p></div></div>
<div class="b2" id="bn33"><p>بخ‌بخ از این عز و این جلالت و این شأن</p></div>
<div class="b" id="bn34"><div class="m1"><p>ذاتش آئینهٔ خدای نما شد</p></div>
<div class="m2"><p>گرچه خدا نیست کی جدا ز خدا شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>درگه او زیب بخش عرش علا شد</p></div>
<div class="m2"><p>هر که به درگاه او ز روی صفا شد</p></div></div>
<div class="b2" id="bn36"><p>ز اهل صفا شد بسان خواجه دوران</p></div>