---
title: >-
    کهنه شش هزار ساله
---
# کهنه شش هزار ساله

<div class="b" id="bn1"><div class="m1"><p>ای گلبن زرد نیم مرده</p></div>
<div class="m2"><p>وی باغچهٔ خزان رسیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بلبل داغ دل شمرده</p></div>
<div class="m2"><p>وی لالهٔ زار داغ دیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای سبزهٔ چهرهٔ زردکرده</p></div>
<div class="m2"><p>صد تیرگی از خزان کشیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وی کام دل از چمن نبرده</p></div>
<div class="m2"><p>وی طعنه ز باغبان شنیده</p></div></div>
<div class="b2" id="bn5"><p>برخیز که فصل نوبهار است</p></div>
<div class="b" id="bn6"><div class="m1"><p>ای کودک عهد پهلوانی</p></div>
<div class="m2"><p>وی بچهٔ روزگار سیروس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای کام گرفته از جوانی</p></div>
<div class="m2"><p>در عهد سپندیار و کاوس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای رسته به فر خسروانی</p></div>
<div class="m2"><p>از چنگ ‌صد انقلاب منحوس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هان عهد تجدد است‌، دانی</p></div>
<div class="m2"><p>کز حلقه و بند عهد مطموس</p></div></div>
<div class="b2" id="bn10"><p>هنگام شکستن و فرار است</p></div>
<div class="b" id="bn11"><div class="m1"><p>گویندکه ‌نو شده‌است‌،‌هی‌هی</p></div>
<div class="m2"><p>این کهنهٔ شش هزار سال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کی پیر، که کرده عمرها طی</p></div>
<div class="m2"><p>گردد به دو ساعت استحاله</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تجدید قوا کنید در وی</p></div>
<div class="m2"><p>تارنج هرم‌ شود ازاله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اصلاح کنید عهدش از پی</p></div>
<div class="m2"><p>تا نوگردد که لامحاله</p></div></div>
<div class="b2" id="bn15"><p>این کهنه به‌دوش دهر بار است</p></div>
<div class="b" id="bn16"><div class="m1"><p>هر چیز که پیر شد بگندد</p></div>
<div class="m2"><p>وآن پیر که گنده شد بمیرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زبور به عجوز برنبندد</p></div>
<div class="m2"><p>تدبیر به پیر در نگیرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وبرانه‌، نگار کی پسندد</p></div>
<div class="m2"><p>افتاده‌، قرار کی پذیرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خواهید گر این کسل بخندد</p></div>
<div class="m2"><p>خواهید گر این کهن نمیرد</p></div></div>
<div class="b2" id="bn20"><p>درمان و علاجش آشکار است</p></div>
<div class="b" id="bn21"><div class="m1"><p>بایست نخست کردش احیا</p></div>
<div class="m2"><p>ز اصلاح مزاجی و اداری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>و آنگاه به پای داشت او را</p></div>
<div class="m2"><p>با تقویت درستکاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وز برق تجددش سراپا</p></div>
<div class="m2"><p>نو کرد به فرّ کردگاری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تجدید فنون و علم و انشا</p></div>
<div class="m2"><p>اصلاح عقیدتی و کاری</p></div></div>
<div class="b2" id="bn25"><p>نو کردن کهنه زین قرار است</p></div>