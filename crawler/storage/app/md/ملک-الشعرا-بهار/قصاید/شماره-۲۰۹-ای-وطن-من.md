---
title: >-
    شمارهٔ ۲۰۹ - ای وطن من
---
# شمارهٔ ۲۰۹ - ای وطن من

<div class="b" id="bn1"><div class="m1"><p>ای خطهٔ ایران مهین‌، ای وطن من</p></div>
<div class="m2"><p>ای گشته به مهر تو عجین جان و تن من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای عاصمهٔ دنیی آباد که شد باز</p></div>
<div class="m2"><p>آشفته کنارت چو دل پر حزن من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور از تو گل و لاله و سرو و سمنم نیست</p></div>
<div class="m2"><p>ای باغ گل و لاله و سرو و سمن من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس خار مصیبت که خلد دل را بر پای</p></div>
<div class="m2"><p>بی روی تو، ای تازه شکفته چمن من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بار خدای من گر بی‌تو زیم باز</p></div>
<div class="m2"><p>افرشتهٔ من گردد چون اهرمن من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا هست کنار تو پر از لشکر دشمن</p></div>
<div class="m2"><p>هرگز نشود خالی از دل محن من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از رنج تو لاغر شده‌ام چونان کاز من</p></div>
<div class="m2"><p>تا بر نشود ناله نبینی بدن من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دردا و دریغاکه چنان گشتی بی‌برک</p></div>
<div class="m2"><p>کاز بافتهء خویش نداری کفن من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسیار سخن گفتم در تعزیت تو</p></div>
<div class="m2"><p>آوخ که نگریاند کس را سخن من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وانگاه نیوشند سخن‌های مرا خلق</p></div>
<div class="m2"><p>کز خون من آغشته شود پیرهن من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و امروز همی‌گویم با محنت بسیار</p></div>
<div class="m2"><p>دردا و دریغا وطن من‌، وطن من</p></div></div>