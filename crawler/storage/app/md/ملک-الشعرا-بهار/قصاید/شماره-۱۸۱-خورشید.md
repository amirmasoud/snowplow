---
title: >-
    شمارهٔ ۱۸۱ - خورشید
---
# شمارهٔ ۱۸۱ - خورشید

<div class="b" id="bn1"><div class="m1"><p>الا یا قیرگون گوهر درون بسّدین خرمن</p></div>
<div class="m2"><p>ز جرم تیره‌ات پیکر، ز نور پاک پیراهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جدال و جنگ در باطن‌، سحرت و صلح در ظاهر</p></div>
<div class="m2"><p>جدال و جنگ تو پنهان‌، سکون و صلح تو معلن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملهب‌، چون ز سیماب گدازیده یکی دوزخ</p></div>
<div class="m2"><p>مشعشع چون ز الماس تراشیده یکی معدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی معدن که آن معدن بود بر آسمان پیدا</p></div>
<div class="m2"><p>یکی دوزخ‌، که آن دوزخ بود زیر فلک آون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آب اندر چنان تابی که سیمینه یکی مجمر</p></div>
<div class="m2"><p>به میغ اندر بدان مانی که زرّینه یکی هاون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسان چاه ویلت‌، ژرف منفذها، به پیرامون</p></div>
<div class="m2"><p>چو دریای سعیرت موج‌ها زاتش به پیرامن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پیرامون ز منفذها، کلف‌های سیه ظاهر</p></div>
<div class="m2"><p>به پیرامن ز آتش‌ها، شررهای قوی روشن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تویی آن زال جادوگر که از جادوگری داری</p></div>
<div class="m2"><p>به زیر هوش کیخسرو، نهفته جان اهریمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میان صبح نیلی‌فام چون پیدا شوی‌، گویی</p></div>
<div class="m2"><p>کسی با جامهٔ نیلی بر آتشدان زند دامن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به‌ هنگام‌ غروب اندر شفق چون در شوی‌، بندی</p></div>
<div class="m2"><p>طراز ارغوانی رنگ بر ذیل خزاد کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تناسانی و استغناست احسان تو بر مردم</p></div>
<div class="m2"><p>زهی ‌آن جرم مستغنی فری‌ آن چهر مستحسن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به یکجا زمهریر از نور رخسارت شده مینو</p></div>
<div class="m2"><p>به یکجا گلشن از تابنده دیدارت شده گلخن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همانا کیفر و مهر خداوندی که هستی تو</p></div>
<div class="m2"><p>به یکجا گرم بادافره به یکجا گرم پاداشن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گشاده باغ را بندی به رخ بر، زمردین برقع</p></div>
<div class="m2"><p>تناور نخل را پوشی به تن بر، آهنین جوشن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به باغ اندر به عیاری نمایی لاله از زمرد</p></div>
<div class="m2"><p>به نخل اندر ز جادویی گشایی شکر از آهن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز تو سبزه شود پیدا، ز تو میوه شود پخته</p></div>
<div class="m2"><p>ز تو گل جنبد از گلبن ز تو مل جوشد اندر دن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همانا بینم آن روزی که بودی جزو خورشیدی</p></div>
<div class="m2"><p>خروشان و شتابان و شررانگیز و نورافکن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز فرط کوشش و گردش بزاد او هر زمان طفلی</p></div>
<div class="m2"><p>که بود آن اختر والا به نور پاک آبستن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو زان طفلان یکی بودی جدا گشته از آن اختر</p></div>
<div class="m2"><p>چنان سنگ فلاخن از کف مرد فلاخن ‌زن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به دور افتادی از مادر ولی آهنگ او داری</p></div>
<div class="m2"><p>ازیرا سوی او پویی به گاه رفتن و گشتن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی ذروه است اندر کهکشان میدان مام تو</p></div>
<div class="m2"><p>تو پویی سوی آن ذروه چو ذرّه زی کُه قارن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو از مادر جدا ماندی فنون مادری خواندی</p></div>
<div class="m2"><p>بزادی کودکانی چند زیباروی و سیمین‌تن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزادی کودکان یک‌یک پس افکندی به صحراشان</p></div>
<div class="m2"><p>ولی آنان همی گردند مادر را به پیرامن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اصول مادری زین‌جا به گیتی گشت پابرجا</p></div>
<div class="m2"><p>که نشکیبد ز مادر هرچه کودک ابله و کودن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو چون بر تودهٔ آرین شدی بی‌مهر و کم‌ تابش</p></div>
<div class="m2"><p>ز ایران ویژه هجرت کرد زی تو تودهٔ آرین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به هندستان و ایران قوم آرین جست وصل تو</p></div>
<div class="m2"><p>که بود از هجر تو روزش شب و سالش دی و بهمن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به هر جا رفت آریانی ترا پرسید چون یزدان</p></div>
<div class="m2"><p>چه در هند و چه در ایران چه در روم و چه در آتن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به تو آباد شد بلخ و به تو آباد شد تبت</p></div>
<div class="m2"><p>ز تو بنیاد شد شوش و ز تو بنیاد شد دکهن‌</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از آن شد مهر نام تو که بودت مهر بر ایران</p></div>
<div class="m2"><p>وزان‌ خواندند خورشیدت که بودی واهب ذوالمن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>الا ای مهربان مادر، فره‌ور، شید روشنگر</p></div>
<div class="m2"><p>یکی ز انوار عز و فر به فرزندانت بپراکن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از آن اسپهبدی فره‌ اا که کورش یافت زان بهره</p></div>
<div class="m2"><p>به فرزندانت کن همره که گردد جانشان روشن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نم باران فراهم کن زمین از سبزه خرم کن</p></div>
<div class="m2"><p>ز تاب نور خود کم کن ز فر و زور خود بشکن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شعاع جاودانی را که داری در درون‌، سرده</p></div>
<div class="m2"><p>فروغ آخشیجی را که داری از برون بفکن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به ایران زیور اندرکش ز خاک تیره گوهرکش</p></div>
<div class="m2"><p>سر روشندلان برکش‌، بن اهریمنان برکن</p></div></div>