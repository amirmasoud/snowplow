---
title: >-
    شمارهٔ ۲۷۵ - به مناسبت پیوند مصر و ایران
---
# شمارهٔ ۲۷۵ - به مناسبت پیوند مصر و ایران

<div class="b" id="bn1"><div class="m1"><p>ای لطف خوشت صیقل آئینهٔ شاهی</p></div>
<div class="m2"><p>روشن دل تو آینهٔ لطف الهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم متغیر، صفتت نامتغیر</p></div>
<div class="m2"><p>دنیا متناهی‌، هنرت نامتناهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پروردهٔ آن گوهر پاکی که ز اضداد</p></div>
<div class="m2"><p>بر پایهٔ جاهش نرسد دست تباهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر روی مه و مهر کلف‌هاست ولی نیست</p></div>
<div class="m2"><p>بر صفحهٔ ادراک تو یک نقطه سیاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمشیر کجت واسطهٔ راست شعاری</p></div>
<div class="m2"><p>اخلاق خوشت قاعدهٔ ملک پناهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خسرو شیرین که بود پاک و منزه</p></div>
<div class="m2"><p>لوح دلت از نقش ملاذی و مناهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبن وصلت فرخنده که فرمود شهنشاه</p></div>
<div class="m2"><p>شد هلهله و غلغله تا ماه ز ماهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد یوسف ما را ملک مصر خریدار</p></div>
<div class="m2"><p>نک بانوی مصر است بر این گفته گواهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقد دل ابناء وطن خواستهٔ تست</p></div>
<div class="m2"><p>بردار ازین خواسته هر قدر که خواهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواندم خط بخت از رخت آن ‌روز که بودی</p></div>
<div class="m2"><p>چون غنچهٔ نوخاسته بر گلبن شاهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فالی زدم آن روز به دیدار تو و امروز</p></div>
<div class="m2"><p>هستم به عیان گشتن آن فال مباهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرچندکه از خدمت درگاه تو دورم</p></div>
<div class="m2"><p>هستم ز دل و جان به ره عشق تو راهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگشا به تفقد در معمورهٔ دل‌ها</p></div>
<div class="m2"><p>کاین ملک نگیرند به نیروی سپاهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شو خواستهٔ خلق و دل از خواسته بردار</p></div>
<div class="m2"><p>خواهنده فزاید چو تو از خواسته کاهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون خاطرت آئینهٔ غیبی است یقینست</p></div>
<div class="m2"><p>ز احوال (‌بهار) آگهی ای شاه کماهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرکس به ‌ازل قسمت خود دید و پذیرفت</p></div>
<div class="m2"><p>گل افسر یاقوتی و ما چهرهٔ کاهی</p></div></div>