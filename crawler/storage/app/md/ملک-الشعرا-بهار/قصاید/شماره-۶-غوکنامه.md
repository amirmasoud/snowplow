---
title: >-
    شمارهٔ ۶ - غوکنامه
---
# شمارهٔ ۶ - غوکنامه

<div class="b" id="bn1"><div class="m1"><p>بس کن از این مکابره ای غوک ژاژخا</p></div>
<div class="m2"><p>خامش‌، گرت هزار عروسیست‌، ور عزا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دیو زشتروی‌، رخ زشت را بشوی</p></div>
<div class="m2"><p>ورنه در آب جوی مزن بیش دست و پا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن غوک سبزپوش برآن برگ پیلگوش</p></div>
<div class="m2"><p>جسته کمین خموش و دو دیده سوی سما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون زاهدی عنود، به سجادهٔ کبود</p></div>
<div class="m2"><p>برکرده از سجود، سر و روی با خدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بگذرد ز پیشش‌، پروانه‌ای ضعیف</p></div>
<div class="m2"><p>بر وی کمین گشاید، آن زاهد دغا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌رنج گیر و دار بیوباردش به قهر</p></div>
<div class="m2"><p>چونان که آدمی را اوبارد اژدها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غوک کبود چهر، شده خیره بر سپهر</p></div>
<div class="m2"><p>خواهد مگر ز مهر، فلک دوزدش قبا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>«‌ای‌ غوک جنگلوک چو پژمرده‌ برگ کوک(‌)»</p></div>
<div class="m2"><p>«‌خواهی که‌چون چگوک بپری سوی‌هوا»</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زادی ز مام خود، به یکی رودهٔ دراز</p></div>
<div class="m2"><p>بک بچگان رده شده در آن درازنا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا باغبان بجنبید، آن روده درگسست</p></div>
<div class="m2"><p>شد خوزهٔ غدیر زکفلیزوان(‌) ملا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان‌روده برشدی سر وتن گرد و دم نحیف</p></div>
<div class="m2"><p>چون کرمکان بکردی در برکه آشنا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون یافتی کمالی آن پوست بفکنی</p></div>
<div class="m2"><p>خردک همی برآید برتنت دست وپا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از برکه اندر آیی نرمک میان خوید</p></div>
<div class="m2"><p>وزکوچکی ز خوید نداند کسی ترا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آفتاب و باد نگهداردت گیاه</p></div>
<div class="m2"><p>وزکرمکان خرد به پیش آیدت غذا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آموزگار، دهر و پرستارت آفتاب</p></div>
<div class="m2"><p>استارگانت یار و شب و روزت اقربا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در هر زمین و آب که آنجا چراکنی</p></div>
<div class="m2"><p>همرنگ آن زمین فتدت رنگ بر قبا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در خاک تیره‌، تیره و در خاک زرد، زرد</p></div>
<div class="m2"><p>در جای سبزه سبز و به جای سیه سیا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این جادویی از آنت بیاموخت روزگا‌ر</p></div>
<div class="m2"><p>کز شرّ دشمنان منافق شوی رها</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دیری بنگذرد که جوان و کلان شوی</p></div>
<div class="m2"><p>در جست و خیزآیی ودر نشو و در نما</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همزادگانت بیشترین از میان روند</p></div>
<div class="m2"><p>تو لیک چیره‌آیی درکوشش بقا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گیرد فروغ‌، چشمت وگیرد نگار، جلد</p></div>
<div class="m2"><p>گردد قوی رگ وپی وگردد فزون دها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زانپس مراد و بویهٔ جفت آیدت بلی</p></div>
<div class="m2"><p>اشکم چوگشت سیر، دگرگون شود هوا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر شب سرود نرم سرایی به یاد جفت</p></div>
<div class="m2"><p>تا بشنوی ز سویی، آواز آشنا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون مه شدی به حلقه غوکان درون شوی</p></div>
<div class="m2"><p>شب چون درافکند به سرآن قیرگون ردا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ازگوشه‌ای درآیی و رانی تحیتی</p></div>
<div class="m2"><p>وز جمع مهتران شنوی بانگ مرحبا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لختی خموش مانی و بینی که بردمید</p></div>
<div class="m2"><p>از هر طرف سری و ز هرسر یکی نوا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن یک به خصم حاضرگوید: برو، برو</p></div>
<div class="m2"><p>این یک به یار غایت گوید: بیا، بیا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شرم آیدت نخست چو بینی که آن گروه</p></div>
<div class="m2"><p>یکباره کارشان تو بگایست و من بگا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زان پس حسد بری چو به بینی که غوک نر</p></div>
<div class="m2"><p>بر غوک ماده جست و بپیچید و شد جدا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رفتار دوستان به تو باری اثرکند</p></div>
<div class="m2"><p>آری مؤثر است محیط جهان به ما</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تدبیرها کنی و به خود شکل‌ها دهی</p></div>
<div class="m2"><p>تاآیدت به چنگ‌ یکی غوک خوش لقا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>می‌بینمت که از همگان گوی برده‌ای</p></div>
<div class="m2"><p>کایدون رسد به گوش‌، غریوت چو کرنا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چشمی فراخ داری و حلقی فراخ‌تر</p></div>
<div class="m2"><p>رانی بسی ستبر و بری همچو متکا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون دشمنی به بینی اندر طپی به آب</p></div>
<div class="m2"><p>کرده بکش دو دست و روان کرده پای‌ها</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از تک چو بر سر آیی و سربرکنی ز آب</p></div>
<div class="m2"><p>گیری به دست ساحل و پاها کنی رها</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دست از پی گرفتن و پای از پی شدن</p></div>
<div class="m2"><p>این خوی آدمیست تو چون کردی اقتدا؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نی نی که اقتدا به تو کردست آدمی</p></div>
<div class="m2"><p>کز تو گذشته است در ادوار ارتقا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در قعرآب حبس نفس می کنی‌، ولیک</p></div>
<div class="m2"><p>گر دیر بر سر آیی‌، لاشک شوی فنا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از بام تا به شام‌، تو و همگنان تو</p></div>
<div class="m2"><p>هستید مست عربده و کینه و مرا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من خسته در حظیرهٔ گرم اندرون بتاب</p></div>
<div class="m2"><p>خوابم ز سر پریده از آن حرب و ماجرا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>این خانه نیست مصر و من از قبطیان نیم</p></div>
<div class="m2"><p>موسی دعا نکرد، چرا خاست این بلا؟</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فرمود بوعلی که چو غوکان فزون شوند</p></div>
<div class="m2"><p>بگریز از آنکه آید اندر پیش وبا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اکنون فزون شد ستید اندر سرای من</p></div>
<div class="m2"><p>وز من ربود خواهید این باغ واین سرا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اندر حدیث‌، کشتن تو نارواست‌، لیک</p></div>
<div class="m2"><p>یک ره بر آن سرم که کنم کار ناروا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آن برکه را تهی کنم از آب و افکنم</p></div>
<div class="m2"><p>چندین هزار غوک لعین را به زبرپا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کاین برکه جایگاه فسادست و نام اوست</p></div>
<div class="m2"><p>بنگاه فسق و جای زنا، مرکز شقا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دار فریب و خانه جور و سرای کفر</p></div>
<div class="m2"><p>بنگاه جهل و حوزهٔ کذب و در ریا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در زندگیت هرگز دردی دوا نشد</p></div>
<div class="m2"><p>لیکن ز کشتهٔ تو شود دردها دوا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آوخ که مرغ و بره اجازت نمی‌دهند</p></div>
<div class="m2"><p>ورنه که گردنت شدی از گرد ران جدا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بیتی ز اوستاد لبیبی‌، بدین نمط</p></div>
<div class="m2"><p>برخواندم و نبشت و بدان کرد اقتفا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>آن بیت را من ایدون پیوند ساختم</p></div>
<div class="m2"><p>دریابد آن که دارد در پارسی ذکا</p></div></div>