---
title: >-
    شمارهٔ ۶۰ - عدل و داد
---
# شمارهٔ ۶۰ - عدل و داد

<div class="b" id="bn1"><div class="m1"><p>باد خراسان همیشه خرم و آباد</p></div>
<div class="m2"><p>دشت و دیارش ز ظلم و جور تهی باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشت و دیار ار ز ظلم و جور تهی گشت</p></div>
<div class="m2"><p>ملک بماند همیشه خرم و آباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک یکی خانه‌ایست بنیادش عدل</p></div>
<div class="m2"><p>خانه نپاید اگر نباشد بنیاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داد و دهش گر بنا نهند به کشور</p></div>
<div class="m2"><p>به که حصاری کنند ز آهن و پولاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خصم ببستند و شهر و ملک گشودند</p></div>
<div class="m2"><p>شاهان از فر و نیروی دهش و داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و آنکو باد جفا و جور به‌سر داشت</p></div>
<div class="m2"><p>سرش به خاک اندرست و ملکش بر باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر خداوند را که داد و دهش را</p></div>
<div class="m2"><p>طرفه بنائی نهاد پادشه راد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسرو گیتی‌ستان مظفر دین شاه</p></div>
<div class="m2"><p>آنکه ز عدلش بنای ظلم برافتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داده خدایش خدایگانی و شاهی</p></div>
<div class="m2"><p>باز نگیرد خدای آنچه به کس داد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک عروسی است عدل و دادش کابین</p></div>
<div class="m2"><p>در ده کابین و شو مر او را داماد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طایر دولت که هرکسش نتوان بست</p></div>
<div class="m2"><p>بال و پر خوبش جز به سوی تو نگشاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مسند شرع و سریر حکم تو داری</p></div>
<div class="m2"><p>خصم تو دارد غریو و ناله و فریاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اینک بنگر بهار را که شدش طبع</p></div>
<div class="m2"><p>شیفته بر مدح تو چو کاه به بیجاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داند کش طبع را چه پایه و مایه است</p></div>
<div class="m2"><p>آنکه بداند شناخت شاهین از خاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود درین آستان پدرش صبوری</p></div>
<div class="m2"><p>چندی مدحت‌سرای و داد سخن داد</p></div></div>