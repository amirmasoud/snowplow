---
title: >-
    شمارهٔ ۱۸۶ - بهار اصفهان
---
# شمارهٔ ۱۸۶ - بهار اصفهان

<div class="b" id="bn1"><div class="m1"><p>نوبهار است و بود پرگل و شاداب چمن</p></div>
<div class="m2"><p>همه گل‌ها بشکفتند به غیر ازگل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به چند ای گل نازک ز چمن دلگیری</p></div>
<div class="m2"><p>خیز و با من قدمی نه به تماشای چمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبحدم بر رخ گل آب زند ابر بهار</p></div>
<div class="m2"><p>تو دگر برگل روی از مژگان آب مزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شادی دشمن و آزار دل دوست مخواه</p></div>
<div class="m2"><p>زان که چون گریه کند دوست‌، بخندد دشمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل قوی دارکه ما نیز خدایی داریم</p></div>
<div class="m2"><p>کز دل خار دماندگل صد برگ و سمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیف باشد دل آزاده به نوروز غمین</p></div>
<div class="m2"><p>این من امروز شنیدم ز زبان سوسن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هفت شین ساز مکن جان من اندر شب عید</p></div>
<div class="m2"><p>شکوه و شین وشغب شهقه وشور وشیون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هفت سین سازکن از سبزه و از سنبل و سیب</p></div>
<div class="m2"><p>سنجد وساز و سرود و سمنو سلوی و من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باشد این هفت به همراه تو و در بر تو</p></div>
<div class="m2"><p>از قد و چهره و خال و لب وگیسو و ذقن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هفت سین را به یکی سفرهٔ دلخواه بنه</p></div>
<div class="m2"><p>هفت شین را به در خانهٔ بدخواه فکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبح عید است برون کن ز دل این تاربکی</p></div>
<div class="m2"><p>کآخر این شام سیه‌، خانه نماید روشن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رسم نوروز به جای آور و از یزدان خواه</p></div>
<div class="m2"><p>کآورد حالت ما باز به حالی احسن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وگر از حسرت ری اشک فشانی تو چنین</p></div>
<div class="m2"><p>اصفهان هم نه چنان است که بردستی ظن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ری اگر نیست کم از باغ جنان یک گندم</p></div>
<div class="m2"><p>اصفهان نیزکم از ری نبود یک ارزن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پل خواجوش ز خاطر ستردگرد ملال</p></div>
<div class="m2"><p>شارع پهلوی از دل ببرد زنگ محن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ماربینش که بود نسخه‌ای از جنت عدن</p></div>
<div class="m2"><p>ازگل لعل بود رشگ یواقیت عدن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زنده‌رود از اثر مستی باران گذرد</p></div>
<div class="m2"><p>سرخوش و عربده‌جو رقص کن و دستک‌زن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیشه‌ها بر دو لب رود، چو خط لب یار</p></div>
<div class="m2"><p>ذوق را راه گذر گیرد و دل را دامن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چار باغش که نشانی ز ملوک صفوی است</p></div>
<div class="m2"><p>می‌دهد روز و شبان یاد از آن عهدکهن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دیو مانند، رده بسته درختان ز دو سو</p></div>
<div class="m2"><p>چون دلیران یل پیل‌تن شیر اوژن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وان مساجدکه برد دل ز برون و ز درون</p></div>
<div class="m2"><p>طاق بیچاده سلب گنبد پیروزه بدن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن‌یکی همچو یکی کاسهٔ مینای نگون</p></div>
<div class="m2"><p>وآن دگر چون تل فیروزه فراز معدن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سر ایوان نگارین ز بر طاق کبود</p></div>
<div class="m2"><p>وآن دوگلدسته کشیده ز دو جانب گردن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گویی افریدون بنشسته بر اورنگ شهی</p></div>
<div class="m2"><p>کاوه استاده به دستیش و به دستی قارن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نوعروسی است به هفتاد قلم کرده نگار</p></div>
<div class="m2"><p>طاق هر قصرکه بینی به سر هر برزن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از صفاهان ز چه رو سخت نفوری کاین شهر</p></div>
<div class="m2"><p>بنگه محتشمان است وکریمان زمن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اندربن شهر حکیمان و ادیبان بودند</p></div>
<div class="m2"><p>همگی صاحب رای و همگی صاحب فن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نوز دجال از این شهر نکرده است خروج</p></div>
<div class="m2"><p>کش نفوری تو چو افریشته از اهریمن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو به دجال و فتن‌های نهانش منگر</p></div>
<div class="m2"><p>کاوه را بین که برون آمد و زد بیخ فتن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ور دلت بستهٔ یاران دیارست‌، بخواه</p></div>
<div class="m2"><p>آمد کار خود از بارخدای ذوالمن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن خدایی که ز پیراهن فرزند عزیز</p></div>
<div class="m2"><p>ساخت دربیت حزن‌، چشم پدر را روشن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چشم روشن کندت از رخ یاران دیار</p></div>
<div class="m2"><p>راست چون دیدهٔ اسرائیل از پیراهن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن خدایی که به ایران ملکی قادر داد</p></div>
<div class="m2"><p>قادر است او که ترا باز برد سوی وطن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پهلوی خسرو جمجماه که ایران شد ازو</p></div>
<div class="m2"><p>خرم و تازه چو از ابر بهاری گلشن</p></div></div>