---
title: >-
    شمارهٔ ۱۸۹ - اندرز به حاکم قوچان
---
# شمارهٔ ۱۸۹ - اندرز به حاکم قوچان

<div class="b" id="bn1"><div class="m1"><p>خرم و آباد باد مرز خبوشان</p></div>
<div class="m2"><p>هیچ دلی از ستم مباد خروشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه خبوشانیان خروشان بودند</p></div>
<div class="m2"><p>بینی زین پس خموش اهل خبوشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردی باید ستوده‌خوی کزین پس</p></div>
<div class="m2"><p>برنخروشند این گروه خموشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نخروشند این گروه بباید</p></div>
<div class="m2"><p>آنچه پسندد به خود، پسندد به ایشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمع کندشان ز مردمی به‌بر خوبش</p></div>
<div class="m2"><p>کاینان را حال بوده سخت پریشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهل خراسان و جز خراسان دانند</p></div>
<div class="m2"><p>جمله که چونست حال مردم قوچان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظلمی زبن پیش رفته است بر آنها</p></div>
<div class="m2"><p>او کند آن ظلم را ازین پس جبران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملک بیاراید و به عدل گراید</p></div>
<div class="m2"><p>تا شود آباد آنچه زو شده ویران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بندد پای از عدوی خانگی آنگاه</p></div>
<div class="m2"><p>گیرد دست از یتیم بی‌سر و سامان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاری کاسان بود نگیرد دشوار</p></div>
<div class="m2"><p>تا بس دشوار کار، گردد آسان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زینسان باید ستوده مرد هنرمند</p></div>
<div class="m2"><p>آری مرد است آنکه باشد زینسان</p></div></div>