---
title: >-
    شمارهٔ ۷ - پاسخ به شعاع‌الملک
---
# شمارهٔ ۷ - پاسخ به شعاع‌الملک

<div class="b" id="bn1"><div class="m1"><p>تا تاختند بی‌هنران در مصاف‌ها</p></div>
<div class="m2"><p>زد زنگ‌، تیغ‌های هنر در غلاف‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناچار تن زند ز مصاف مخنثان</p></div>
<div class="m2"><p>آن کس که‌برشکست به‌مردی ‌مصاف‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا لافزن نمود زبان هنر دراز</p></div>
<div class="m2"><p>ی‌ت‌باره کرد خوی‌، زبان‌ها به لاف‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تیره دُرددن به می صاف چیره گشت</p></div>
<div class="m2"><p>ماندند دُردها و رمیدند صاف‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرورده شد به طرد حقایق دماغ‌ها</p></div>
<div class="m2"><p>گسترده شد بگرد طبایع گزاف‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر باد رفت قاعدهٔ اجتماع‌ها</p></div>
<div class="m2"><p>وز هم گسست رابطهٔ ائتلاف‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم ز طوف کعبهٔ عزت کرانه کرد</p></div>
<div class="m2"><p>بگرفت گرد خانهٔ عزّی طواف‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردی به خاک خفت ازین بی‌حمیتان</p></div>
<div class="m2"><p>عفت به باد رفت از این بی‌عفاف‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفتند خواجگان کریم و نماند نام</p></div>
<div class="m2"><p>زان اصطناع‌ها و از آن انتصاف‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آئین دیرباز دگرگونه گشت و شد</p></div>
<div class="m2"><p>وارون طواف‌ها و دگرگون مطاف‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نامردی زمانه نگر کزین صطبل</p></div>
<div class="m2"><p>بر قصرها شدند فراجاف جاف‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آبستنان حرص چمان پیش صف بار</p></div>
<div class="m2"><p>وز بار حرصشان به زمین سوده‌ناف‌ها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن یک امیر لشکر و این یک‌ وزیر جنگ</p></div>
<div class="m2"><p>لعنت برین مضاف‌الیه و مضاف‌ها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آزاد جاهلان وگشاده زبان‌، خران</p></div>
<div class="m2"><p>بسته مدرسان و فقیهان به خواف‌ها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>.............. ..............</p></div>
<div class="m2"><p>............................</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اکنون ‌لحاف و بستر‌، سنجاب و خز کنند</p></div>
<div class="m2"><p>آنان که پاره بد به کتفشان لحاف‌ها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وقتست تا میان چمن عاملان دی</p></div>
<div class="m2"><p>بر گلبنان کنند ز نو اعتساف‌ها</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا زاغ‌ها به باغ گشادند حنجره</p></div>
<div class="m2"><p>بستند نای‌، زمزمه خوان زندباف‌ها</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خفاش ها شدند از اشکفت‌ها برون</p></div>
<div class="m2"><p>طاووس‌ها شدند نهان در شکاف‌ها</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شهبوف‌ها شدند مهاجم به قصرها</p></div>
<div class="m2"><p>سیمرغ‌ها شدندگریزان به قاف‌ها</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>...........................</p></div>
<div class="m2"><p>....... ... .... ..............</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کل را وفاق پیشه بدو بید را خلاف(‌)</p></div>
<div class="m2"><p>پنهان وفاق‌ها شد و عریان خلاف‌ها</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زودا که بوستان فضیلت خزان شود</p></div>
<div class="m2"><p>زبن انقلاب کشور و این اختلاف‌ها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کندیم و کافتیم و بهر سو شتافتیم</p></div>
<div class="m2"><p>دیدیم سبع‌های سمان و عجاف‌ها‌</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>الا سه چار یار پراکنده گرد دهر</p></div>
<div class="m2"><p>چیزی نیافتیم از آن کند و کاف‌ها</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هان ای‌ شعاع ملک ز یاران‌ یکی تویی</p></div>
<div class="m2"><p>نزد من از بزرگترین اکتشاف‌ها</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پیوسته فحل طبع تو با بکر فکر نغز</p></div>
<div class="m2"><p>دارد درون حجلهٔ دانش زفاف‌ها</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر چامهٔ بدیع تو صدآفرین که داشت</p></div>
<div class="m2"><p>خنگ هنر بهر نقطش انعطاف‌ها</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن حله بود بافته از تار و پود فضل</p></div>
<div class="m2"><p>کانسان نبافتند دگر حله‌باف‌ها</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از کوثر معانی شیرین و لفظ عذب</p></div>
<div class="m2"><p>کرده است ساقی هنرت اغتراف‌ها</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قندیست پارسی که‌شکرپاسخان ری</p></div>
<div class="m2"><p>در پر حلاوتیش کنند اعتراف‌ها</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا نیست درکریمی یزدان مخالفت</p></div>
<div class="m2"><p>تا هست در قدیمی گیهان خلاف‌ها</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حی قدیمت ازکرم و بخشش عمیم</p></div>
<div class="m2"><p>اندرکنیف لطف کناد اکتناف‌ها</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بندد به کارنامهٔ فضلت طرازها</p></div>
<div class="m2"><p>بخشد زکارخانهٔ فیضت کفاف‌ها</p></div></div>