---
title: >-
    شمارهٔ ۹۱ - گناه آدم و حوا
---
# شمارهٔ ۹۱ - گناه آدم و حوا

<div class="b" id="bn1"><div class="m1"><p>صبح چون شاه فلک بر تختگه مأوی کند</p></div>
<div class="m2"><p>حاجب مشرق حجاب نیلگون بالا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر دفع جادویی‌های شب فرعون کیش</p></div>
<div class="m2"><p>موسی صبح از بغل بیرون ید بیضا کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود مگر زرتشت با فّر فروغ اورمزد</p></div>
<div class="m2"><p>چارهٔ پتیارهٔ اهریمن شیدا کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاور هران دلاور در دل ابر سیاه</p></div>
<div class="m2"><p>با مشعشع رمح، قصد جان اژدرها کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روشنانش را برون ریزد سپهر از آستین</p></div>
<div class="m2"><p>چون که زان فرزانگان روشن‌تری پیدا کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون سترون بانویی کز شرم درپوشد پلاس</p></div>
<div class="m2"><p>باز چون فرزند زاید جامه از دیبا کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی خطا گفتم که شب دارد بسی فرزند خرد</p></div>
<div class="m2"><p>چون فزون شد بچه‌، دل آشفته و در واکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبح‌ خوش‌ خندد که ‌یک فرزند دارد، لیک شب</p></div>
<div class="m2"><p>در غم طفلان‌، چو من پیوسته واوبلا کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب سیه‌شد زان که چون من کودکان دارد بسی</p></div>
<div class="m2"><p>همچو من آخر سر خویش اندرین سوداکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>*</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبح چون بنشینم وخواهم نویسم چیزکی</p></div>
<div class="m2"><p>در دود پروانه وز من خواهش قاقاکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وان دو ماهه مهرداد اندر کنار مادرش</p></div>
<div class="m2"><p>دم بدم عرعر نماید، متصل هرا کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دختر شش ساله‌ام کاو را ملک دختست نام</p></div>
<div class="m2"><p>بر در صندوقخانه محشری برپاکند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ظهر چون شد خرسواران در رسند از مدرسه</p></div>
<div class="m2"><p>خانه از آشوبشان زلزال‌ها پیدا کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>محشر خر راست گردد زان گروه کره‌خر</p></div>
<div class="m2"><p>آن‌یکی جفتک زند وین نعره‌، وآن آواکند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گه ملک هوشنگ از مامک رباید خوردنی</p></div>
<div class="m2"><p>گاه مامک با ملک‌دخت از حسد دعواکند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نعرهٔ خاتون پی تسکین آنان بیشتر</p></div>
<div class="m2"><p>مرمرا کالیوه و آسیمه و شیدا کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرتنی ز آنان به سالی ثروتی بدهد به‌باد</p></div>
<div class="m2"><p>هرکی ز ایشان به ماهی خانه‌ای یغما کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هریک اندر هفته جفتی کفش را ساید به‌پای</p></div>
<div class="m2"><p>هریک اندر ماه دستی جامه از سر واکند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هرچه از خاتون بجا ماند خورند این کرّگان</p></div>
<div class="m2"><p>خادمات و خادمین راکیست کاستقصاکند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کودکان دایم کلان گردند و بابا پیر و زار</p></div>
<div class="m2"><p>چون که کودک شدکلان کی رحم بر باباکند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ازکلاه‌ و کفش و کسوت‌، کاغذ و کلک و کتاب</p></div>
<div class="m2"><p>نیست کافی گر دوصدکاف دگر انشاکند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گوش شیطان کر، که بانو هست حسناء ولود</p></div>
<div class="m2"><p>همچو من سوداویئی چون منع آن حسناکند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گشته ملزم تا به هر سالی بزاید کودکی</p></div>
<div class="m2"><p>وز برای خیل شه فوجی جوان برپاکند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گویم آخر نان این قوم ازکجاگردد روان</p></div>
<div class="m2"><p>گوید آن کاو داد دندان‌، نان همو اعطاکند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>طرفه اصلی در توکل دارد این خاتون بهٔاد</p></div>
<div class="m2"><p>آن دل و آن زهره کوکاین اصل را حاشاکند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>راستی دانایی هر چیز بیش از آدمی است</p></div>
<div class="m2"><p>کیست آن کوچند و چون با مردم دانا کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>*</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چیست‌باری‌فایدت جز حسرت و تیمار و غم</p></div>
<div class="m2"><p>گر جهان را همت آبا پر از ابنا کند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا درنگی افتد اندر این موالید دورنگ</p></div>
<div class="m2"><p>چار مام ای کاش پشت خود به هفت آبا کند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این گناه از آدم و حوا پدید آمد نخست</p></div>
<div class="m2"><p>کیست کاینک داوری با آدم و حوا کند</p></div></div>