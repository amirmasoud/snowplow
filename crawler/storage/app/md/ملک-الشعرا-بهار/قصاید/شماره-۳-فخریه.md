---
title: >-
    شمارهٔ ۳ - فخریه
---
# شمارهٔ ۳ - فخریه

<div class="b" id="bn1"><div class="m1"><p>دگر باره خیاط باد صبا</p></div>
<div class="m2"><p>بر اندام گل دوخت رنگین قبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسی حله آورد و ببرید و دوخت</p></div>
<div class="m2"><p>به نوروز، خیاط باد صبا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی را به بر ارغوانی سلب</p></div>
<div class="m2"><p>یکی را به تن خسروانی ردا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز اصحاب بستان که یکسر بدند</p></div>
<div class="m2"><p>برهنه تن و مفلس و بینوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دست یکی بست زیبا نگار</p></div>
<div class="m2"><p>به پای یکی بست رنگین حنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیاراست بر پیکر سروبن</p></div>
<div class="m2"><p>یکی سبزکسوت زسرتا به پا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برافکند بر دوش بید نگون</p></div>
<div class="m2"><p>ز پیروزه درّاعه‌ای پربها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسی ساخت بازبچه و پخش کرد</p></div>
<div class="m2"><p>به اطفال باغ ازگل و ازگیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دست یکی پیکری خوب‌چهر</p></div>
<div class="m2"><p>به چنگ یکی لعبتی خوش‌لقا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی‌بسته شکلی‌به‌رخ بلعجب</p></div>
<div class="m2"><p>یکی هشته تاجی به سر خوشنما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی را به بر طرفه‌ای مشگ‌بیز</p></div>
<div class="m2"><p>یکی را به کف حلقه‌ای عطرسا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس آنگه بسی عقد گوهر ز هم</p></div>
<div class="m2"><p>گسست و پراکندشان بر هوا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درخت شکوفه ده انگشت خویش</p></div>
<div class="m2"><p>فراپیش کرد و ربود آن عطا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیه ابر توفنده کز جیش دی</p></div>
<div class="m2"><p>جدا مانده در کوه جفت عنا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر آن شد که آید به یغمای باغ</p></div>
<div class="m2"><p>بتاراجد آن ایزدی حله‌ها</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برآمد خروشنده از کوهسار</p></div>
<div class="m2"><p>بپیچید از خشم چون اژدها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که ناگاه باد صبا دررسید</p></div>
<div class="m2"><p>زدش چند سیلی همی برقفا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بنالید از آن درد ابر سیاه</p></div>
<div class="m2"><p>شد آفاق از ناله‌اش پرصدا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو گفتی سیه بنده‌ای کرده جرم</p></div>
<div class="m2"><p>دهد خواجه اکنون مر او را جزا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ببارد ز مژگان سرشک آنچنان</p></div>
<div class="m2"><p>کزان تر شود باغ و صحن سرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گه از خشم دندان نماید همی</p></div>
<div class="m2"><p>بتابد ز دندانش نور و ضیا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ببالد چمن زان خروش و غریو</p></div>
<div class="m2"><p>بخندد سمن زان فغان و بکا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جنان کز خروشیدن کوس رزم</p></div>
<div class="m2"><p>بخندد همی لشکر پادشا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نگه کن به ایران ز ده سال پیش</p></div>
<div class="m2"><p>ز آشوب و غوغا و قحط و غلا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خزینه تهی‌تر ز مغز وزیر</p></div>
<div class="m2"><p>ذخیره تهی‌تر از آن هر دوتا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ادارات‌، ویرانه و بی‌حقوق</p></div>
<div class="m2"><p>سپاهی‌، برهنه تن و بینوا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سر ماه‌، دولت به دریوزگی</p></div>
<div class="m2"><p>شده بر در اجنبی چون گدا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روان هر طرف جیش بیگانگان</p></div>
<div class="m2"><p>به یغمای این ملک داده صلا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به هرگوشه‌ای ظالمی مقتدر</p></div>
<div class="m2"><p>به هر دسته‌ای مفسدی مقتدا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شنیده خردمند هر بامداد</p></div>
<div class="m2"><p>ز نابخردان تهمت و ناسزا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز مردم کشان خون مردم هدر</p></div>
<div class="m2"><p>ز غارتگران مال ملت هبا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شده ملک گیلان و مازندران</p></div>
<div class="m2"><p>به تاراج بیگانه و آشنا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به هر برزن وکوی گرد آمده</p></div>
<div class="m2"><p>پی مفسدت لشگری زاشقیا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به‌شهر ری اندر به هریک دو ماه</p></div>
<div class="m2"><p>شده چند بیچاره فرمانروا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وطن‌دوستان‌سر ز خجلت به زیر</p></div>
<div class="m2"><p>ولی سفگان گرم چون و چرا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>درین حالت زار ناگه ز غیب</p></div>
<div class="m2"><p>برآمد یکی دست زورآزما</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نجنبید از هیبتش آب از آب</p></div>
<div class="m2"><p>لهیب فتن سرد شد جابجا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو بودی که‌ در جنگ‌ خونین ‌رشت</p></div>
<div class="m2"><p>سپر ساختی تن به تیر بلا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو بودی که کردی به رزم جنوب</p></div>
<div class="m2"><p>به دربا و صحرا تن خود فدا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو بودی که گرگان ز نیروی تو</p></div>
<div class="m2"><p>تهی شد ز یک گله گرگ دغا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>توبودی کز آن پست و تیره مغاک</p></div>
<div class="m2"><p>رساندی وطن را به اوج علا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همیدون به شرح هنرهای تو</p></div>
<div class="m2"><p>زبان رهی قاصر است از ثنا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مگر وام خواهم ز تیمورتاش</p></div>
<div class="m2"><p>زبانی فصیح و بیانی رسا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هم ازکلک او مایه خواهم همی</p></div>
<div class="m2"><p>مگر کلک او مایه بخشد مرا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پس آنگه زصد دفتر مدح تو</p></div>
<div class="m2"><p>توانم مگر کرد سطری ادا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دریغا جدا ماندم از مهر شاه</p></div>
<div class="m2"><p>ز بس گفت دشمن بدم در قفا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو من نیکخواهی کم آید به‌دست</p></div>
<div class="m2"><p>سخن گستر و ثابت و باوفا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نروبیده اندر دلش بیخ آز</p></div>
<div class="m2"><p>نخشکیده در چشمش آب حیا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>وطنخواه و بیدار و باتجربت</p></div>
<div class="m2"><p>نوبسنده و ناطق و پارسا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به کار سیاست صدیق و دلیر</p></div>
<div class="m2"><p>گریزان ز زرق و فریب و ربا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>برون‌ ز اختصاصی که دارم به شعر</p></div>
<div class="m2"><p>ببستم زهر علم طرفی جدا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز اصل لغات و ز اصل خطوط</p></div>
<div class="m2"><p>ز اصل ملل کامدند ازکجا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز پیدایش خاک و استارگان</p></div>
<div class="m2"><p>ز حیوان و انسان و آب و گیا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زگفتار داروبن و سر حیواه</p></div>
<div class="m2"><p>ز تبدیل و از نشو و از ارتقا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز تصنیف‌ الحان‌ و از صرف و نحو</p></div>
<div class="m2"><p>ز تشریح و تاریخ و جغرافیا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فزون زین هنرها که از هر یکش</p></div>
<div class="m2"><p>مرا خاست خصمی پلید و دغا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرا این هنرها ز درگاه تو</p></div>
<div class="m2"><p>جداساخت‌ای شاه کشورگشا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چه غم گر بمیرم به کام حسود</p></div>
<div class="m2"><p>که ماند پس از من ز من شعرها</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همه پخته مانند سیم رده</p></div>
<div class="m2"><p>همه سخته مانند زر طلا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گر از شعر شاید که پوشش کنند</p></div>
<div class="m2"><p>بپوشد زمانه ز شعرم کسا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>حسودان ما هم بمیرند نیز</p></div>
<div class="m2"><p>منزه شود دستگاه قضا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>قضاوت ز روی عدالت شود</p></div>
<div class="m2"><p>نه از روی بیداد وبخل و جفا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سخن‌های‌ ما خود ز دل‌ خاسته‌ است</p></div>
<div class="m2"><p>در آن نیست یک‌ذره ریو و ریا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به نیک و بدکار ما پی برند</p></div>
<div class="m2"><p>پس از ما، چو خوانند اشعار ما</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بر آنم که شعرم نگوید دروغ</p></div>
<div class="m2"><p>وگر چندگوبد سخن در قفا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بوبژه که در شعرم اغراق نیست</p></div>
<div class="m2"><p>صریح است و پاکیزه و جانفزا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به لفظ ار به کس اقتفا کرده‌ام</p></div>
<div class="m2"><p>به معنی نکردم به کس اقتفا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تنحل نکردم به شعر اندرون</p></div>
<div class="m2"><p>نسازد به دریوزه اهل غنا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تتبع بسی کرده‌ام لاجرم</p></div>
<div class="m2"><p>توارد اگر شد تفضل نما</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بلای توارد بلایی است صعب</p></div>
<div class="m2"><p>به یزدان گریزم من از این بلا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ببین دفتر فرخی و سروش</p></div>
<div class="m2"><p>که مصراع‌ها نیست از هم جدا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>من اینسان توارد ندارم به شعر</p></div>
<div class="m2"><p>که نبود مرا حافظه بی‌وفا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مرا عیب کردند در سبک نظم</p></div>
<div class="m2"><p>که این باستانی سخن تاکجا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همم عیب کردند درکار نثر</p></div>
<div class="m2"><p>که این شیوه ی تازه باری چرا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ندانند کان باستانی سخن</p></div>
<div class="m2"><p>کلیدی‌است‌درفضل ،‌مشگل گشا</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>زبان را نگه دارد از انحطاط</p></div>
<div class="m2"><p>سخن را نگه دارد از انحنا</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ولی نثر پیشین چنان ابتر است</p></div>
<div class="m2"><p>که مقصود را کرد نتوان ادا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>همان‌نظم،‌ خاص ‌است و نثر است عام</p></div>
<div class="m2"><p>نداند کس ار شعر، باشد روا</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ولی نثر را گر ندانند خلق</p></div>
<div class="m2"><p>ابا معرفت کی شوند آشنا</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در ایران به تازی نبشتند نثر</p></div>
<div class="m2"><p>که در نثر تازی فراخ است جا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به نثر اعتنایی نبوده است پیش</p></div>
<div class="m2"><p>که بوده است افزون به شعر اعتنا</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بود سخت‌، بنیان نظم دری</p></div>
<div class="m2"><p>ز آرایش و لون و برگ و نوا</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ولی نثر تازی ز نثر دگر</p></div>
<div class="m2"><p>بسی بیش دارد جمال و بها</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بجز چند دفتر ز پیشینیان</p></div>
<div class="m2"><p>که تقلید از آنان بود نابجا</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نشان ده اگر هست نثری تمام</p></div>
<div class="m2"><p>که ‌بر جای پایش توان هشت پا</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ازبرا به نثر نوبن تاختم</p></div>
<div class="m2"><p>کز آن حاجت قوم گردد روا</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گر این طرز تحریر بودی گزاف</p></div>
<div class="m2"><p>نراندی بر آن هرکسی مرحبا</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>نکردی به هر مغز چون مل اثر</p></div>
<div class="m2"><p>ندادی به هر بزم چون گل صفا</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>هرآن چیز کان را پسندند خلق</p></div>
<div class="m2"><p>سراسر صوابست و جز آن، خطا</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>دربغا که‌ خیره است ‌چشم حسود</p></div>
<div class="m2"><p>نبیند به جز عیب خلق خدا</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>گرت صد هنر باشد و عیب یک</p></div>
<div class="m2"><p>صدت عیب گیرد حسود دغا</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>حسودان به پیغمبر هاشمی</p></div>
<div class="m2"><p>ببستند از اینگونه بس افترا</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>که شعر است‌قرآن و بی‌معنیست</p></div>
<div class="m2"><p>الف لام میم و الف لام را</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چو گرک حسد مصطفی را گزید</p></div>
<div class="m2"><p>تو گوبی که آهو نگیرد مرا؟‌!</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>الا تا گلستان به فصل بهار</p></div>
<div class="m2"><p>چو روی نکو‌بان شود دل گشا</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>سرت سبز باد و تنت زورمند</p></div>
<div class="m2"><p>تو را دولت و دولتت را بقا</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>وطن باد در سایهٔ عدل تو</p></div>
<div class="m2"><p>برومند و بالنده و باصفا</p></div></div>