---
title: >-
    شمارهٔ ۱۰۲ - پیام ایران
---
# شمارهٔ ۱۰۲ - پیام ایران

<div class="b" id="bn1"><div class="m1"><p>به هوش باشکه ایران تو را پیام دهد</p></div>
<div class="m2"><p>ترا پیام به صد عز و احترام دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا چه گوید: گوید که خیر بینی اگر</p></div>
<div class="m2"><p>به کار بندی پندی که باب و مام دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسیم صبح که بر سرزمین ما گذرد</p></div>
<div class="m2"><p>ز خاک پاک نیاکان‌، ترا سلام دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز استخوان نیاکانت برگذشته بود</p></div>
<div class="m2"><p>دم بهار که ازگل به گل پیام دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یاد عشرت اجداد تست هر نوروز</p></div>
<div class="m2"><p>که گل به طرف گلستان صلای عام دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو پای‌بند زمینی و رشته‌ایست نهان</p></div>
<div class="m2"><p>که با گذشته تو را ارتباط تام دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گذشته، پایه و بنیان حال و آینده است</p></div>
<div class="m2"><p>سوابق است که هر شغل را نظام دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کارنامهٔ پیشینیان نگر، بد و خوب</p></div>
<div class="m2"><p>که تلخ کامیت آرد پدید وکام دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز درس حکمت و آداب رفتگان مگسل</p></div>
<div class="m2"><p>که این گسستگیت خواری مدام دهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی که از پدران‌ ننگ‌ داشت‌ ناخلف است</p></div>
<div class="m2"><p>که مرد را شرف باب و مام‌، نام دهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگویمت که به ستخوان خاک‌خورده بناز</p></div>
<div class="m2"><p>عظام بالیه کی رتبت عصام دهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به‌علم خویش بکن تکیه و به عزم درست</p></div>
<div class="m2"><p>که علم و عزم‌، ترا عزت و مقام دهد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ولی ز سنت دیرین متاب رخ زیراک</p></div>
<div class="m2"><p>به ملک‌، سنت دیرینه احتشام دهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز درس پارسی و تازی احتراز مکن</p></div>
<div class="m2"><p>که این دو قوت ملی علی‌الدوام دهد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شعائر پدران و معارف اجداد</p></div>
<div class="m2"><p>حیات و قدرت اقوام را قوام دهد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مباش غره به تقلید غربیان‌، که به شرق</p></div>
<div class="m2"><p>اگر دهد، هنر شرقی احترام دهد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو شرقیئی و به شرق اندرون کمالاتی است</p></div>
<div class="m2"><p>ولی چه سودکه غربت فریب تام دهد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به‌هر صفت که برآیی برآی و شرقی باش</p></div>
<div class="m2"><p>وگرنه دیو به صد قسمت انقسام دهد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز غرب علم فراگیر و ده به معدهٔ شرق</p></div>
<div class="m2"><p>که فعل هاضمه‌اش با تن انضمام دهد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به راه تست بسی دام‌های دانه‌نمای</p></div>
<div class="m2"><p>کجاست مرد که از دانه فرق دام دهد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز دام و دانه اگر نگذری محالست این</p></div>
<div class="m2"><p>که روزگار ترا فرصت قیام دهد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیام مام جگرخسته را ز جان بشنو</p></div>
<div class="m2"><p>که پند و موعظه‌ات با صد اهتمام دهد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو چشم‌ مام‌ وطن ز آفتاب و مه‌ سوی ماست</p></div>
<div class="m2"><p>وزین دو دیده به ما کسوت و طعام دهد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز چشم مام وطن خون چکد بر این آفاق</p></div>
<div class="m2"><p>که سرخی شفقش جلوه صبح و شام دهد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به ما خطاب کند با دو دیدهٔ خونبار</p></div>
<div class="m2"><p>که کیست آنکه به‌ من ‌خون‌ خویش‌ وام دهد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به روی سینه بپرورده‌ام جوانان را</p></div>
<div class="m2"><p>که داد من ز شما نوخطان‌، کدام دهد؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس از زمانهٔ خسرو شد چو بیوه‌زنی</p></div>
<div class="m2"><p>که هر کسیش نویدی گزاف و خام دهد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه کودکان که بزادم دلیر و دانشمند</p></div>
<div class="m2"><p>یکی نماند که ملک من انتظام دهد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر یکی به ره راست رفت‌، از پی او</p></div>
<div class="m2"><p>کسی نیامد کان راه را دوام دهد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز چنگ ظلم و ستبداد کس نرست که او</p></div>
<div class="m2"><p>قراری از پی آسایش انام دهد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کنون ‌امید من ‌ای ‌نو خطان‌ به ‌سعی شماست</p></div>
<div class="m2"><p>مگر که سعی شما داد من تمام دهد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز چاک سینهٔ بشکافته به خنجر جهل</p></div>
<div class="m2"><p>دل شکسته‌ام آوای انتقام دهد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>الاکجاست جوانی ز نوخطان وطن</p></div>
<div class="m2"><p>که در حمایت من وعدهٔ کرام دهد؟</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کجاست‌آنکه‌به‌داروی عقل و مرهم عدل</p></div>
<div class="m2"><p>جراحت دل خونینم التیام دهد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کنام شیران وبران شده است‌، بچهٔ شیر</p></div>
<div class="m2"><p>کجاست کآمده آرایش کنام دهد؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز چنگ بی‌هنران برکشد زمام امور</p></div>
<div class="m2"><p>به دست مردم صاحب هنر، زمام دهد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کجاست آنکه جوانمردی و فضیلت را</p></div>
<div class="m2"><p>به یاد مردم درماندهٔ عوام دهد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کجاست مرد جوانمرد و خواستار شرف</p></div>
<div class="m2"><p>که‌ سود خویش‌ زکف بهر سود عام‌ دهد؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کجاست‌ مرد، که شمشیر دادخواهی را</p></div>
<div class="m2"><p>ز قلب ظالم بیدادگر نیام دهد؟</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کجاست حزبی از آزادگان که چون پدران</p></div>
<div class="m2"><p>ز خصم‌،‌ جان‌ بستاند به‌ دوست‌،‌ جام‌ دهد؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وطن به چنگ لئام است‌، کو خردمندی</p></div>
<div class="m2"><p>که درس فضل و شرافت‌، بدین لئام دهد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به جهد، پایهٔ حزبی شریف و پاک نهد</p></div>
<div class="m2"><p>به مشت‌، پاسخ مشتی فضول و خام دهد</p></div></div>