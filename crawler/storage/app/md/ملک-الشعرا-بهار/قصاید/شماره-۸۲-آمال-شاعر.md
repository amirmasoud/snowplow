---
title: >-
    شمارهٔ ۸۲ - آمال شاعر
---
# شمارهٔ ۸۲ - آمال شاعر

<div class="b" id="bn1"><div class="m1"><p>فروردین آمد، سپس بهمن و اسفند</p></div>
<div class="m2"><p>ای ماه بدین مژده بر آذر فکن اسپند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ورگویی ما آذر و اسپند نداربم</p></div>
<div class="m2"><p>آن خال سیه چیست برآن چهرهٔ دلبند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم‌نیست گر این‌خانه تهی‌از همه کالاست</p></div>
<div class="m2"><p>عشق است و وفا نادره کالای خردمند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جا که تویی از رخ زیبای تو مشکو</p></div>
<div class="m2"><p>لعبتکدهٔ چین بود و سغد سمرقند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچند گرفتارم، آزادم آزاد</p></div>
<div class="m2"><p>هرچند تهیدستم‌، خرسندم خرسند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بربسته‌ام از هرچه به جز چهر تو، دیده</p></div>
<div class="m2"><p>بگسسته‌ام از هرچه به جز مهر تو، پیوند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای روی تو چونان که کنی تعبیه در باغ</p></div>
<div class="m2"><p>یک دسته گل سوری برسروبرومند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز یاد تو از نای من آواز نیاید</p></div>
<div class="m2"><p>هرچند نمایند جدا بند من از بند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بر ستخوان‌بندم‌،‌چون‌نی‌مگراز ضعف</p></div>
<div class="m2"><p>یاد تو ز هر بند من آرد شکر و قند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما پار ز فروردین جز بند ندیدیم</p></div>
<div class="m2"><p>وان بند بپایید به ما تا مه اسفند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر پار زبون گشتیم از دمدمهٔ دیو</p></div>
<div class="m2"><p>امسال بیاساییم از لطف خداوند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برخیز و به بستان گذر امروزکه بستان</p></div>
<div class="m2"><p>از لاله و نسرین به بهشتست همانند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درکوه تو گفتی که یکی زلزله افتاد</p></div>
<div class="m2"><p>وآنگه ز دل خاک به صحرا بپراکند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صد کان پر از گوهر و صد گنج پر از زر</p></div>
<div class="m2"><p>صد مخزن پیروزه وصد معدن یاکند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صحرا ز گل لعل چو رامشگه پروبز</p></div>
<div class="m2"><p>بستان ز گل سرخ چو آتشگه ریوند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بلبل چو مغان خرده اوستا کند از بر</p></div>
<div class="m2"><p>مرغان دگر زندکنند از بر و پا زند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یک مرغ نیایشگر مهر آمد و فرورد</p></div>
<div class="m2"><p>یک مرغ ستایشگر ارد آمد وپارند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرورد ز مینو به جهان آمد و آورد</p></div>
<div class="m2"><p>همراه‌، گل سرخ بسی فره و اورند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برگیر می لعل از آن پیش که در باغ</p></div>
<div class="m2"><p>برلعل لب غنچه نهد صبح، شکرخند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صبح ‌است‌ و گلان‌ دیده گمارند به‌ خورشید</p></div>
<div class="m2"><p>چون سوی بت نوش‌لبی‌، شیفته‌ای چند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ما نیز نیایش‌، بر خورشید گزاربم</p></div>
<div class="m2"><p>خوشا که نیایش بر خورشید گزارند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنگه که برون آید و از اوج بتابد</p></div>
<div class="m2"><p>و آنگاه که پنهان شود اندر پس الوند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زرین شود از تافتنش سینهٔ البرز</p></div>
<div class="m2"><p>چون غیبهٔ زر از بر خفتان و قراگند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون خیمهٔ زربفت شود باز چو تابد</p></div>
<div class="m2"><p>مهر از شفق مغرب بر کوه دماوند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یا چون رخ ضحاک بدانگه که فریدون</p></div>
<div class="m2"><p>بنمود رخ خویش بدان جادوی دروند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شد کشور ایران چو یکی باغ شکفته</p></div>
<div class="m2"><p>از ساحل جیحون همه تا ساحل اروند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرغان سخن پارسی آغاز نهادند</p></div>
<div class="m2"><p>از بندر شاهی همه تا بارهٔ دربند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرمزد چنین ملک گرانمایه به ما داد</p></div>
<div class="m2"><p>زردشت بیاراستش از حکمت و از پند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر فر کیان باز به ما روی نماید</p></div>
<div class="m2"><p>بیرون رود از کشور ما خواری و آفند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وز نیروی هرمزد، درآید به کف ما</p></div>
<div class="m2"><p>آنچ از کف ما رفت به جادویی و ترفند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آباد شود بار دگر کشور دارا</p></div>
<div class="m2"><p>و آراسته گردند و باندام و خوش آیند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن طاق که شد ساخته بر ساحل دجله</p></div>
<div class="m2"><p>و آن کاخ که شد سوخته در دامن سیوند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر شهر شود کشور و هر قریه شود شهر</p></div>
<div class="m2"><p>هر سنگ شود گوهر و هر زهر شود قند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دیگر دُر غلطان رسد از خطهٔ بحرین</p></div>
<div class="m2"><p>دیگر زر رویان رسد از کوه سگاوند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از چهرهٔ کان‌ها فتد آن پردهٔ اهمال</p></div>
<div class="m2"><p>چون پردهٔ خجلت ز عذار بت دلبند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بانگ ره آهن ز چپ و راست برآید</p></div>
<div class="m2"><p>چون نعرهٔ دیوان برون تاخته از بند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>صد قافله داخل شود از رهگذر روم</p></div>
<div class="m2"><p>صد قافله بیرون رود از رهگذر هند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بندر شود از کشتی چون بیشهٔ انبوه</p></div>
<div class="m2"><p>هر کشتی غرنده‌، چو شیر نر ارغند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از علم و صناعت شود این دوره گرامی</p></div>
<div class="m2"><p>وز مال و بضاعت شود این خطه گرامند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بار دگر افتد به سر این قو‌م کهن را</p></div>
<div class="m2"><p>آن فخر کز اجداد قدیم است پس افکند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن دیو کجا کارش پیوسته دروغست</p></div>
<div class="m2"><p>از مرز کیان برگسلد بویه و پیوند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دوران جوانمردی و آزادی و رادی</p></div>
<div class="m2"><p>با دید شود چون شود این ملک برومند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ورزنده شود مردم و ورزیده شود خاک</p></div>
<div class="m2"><p>از کوه گشاید ره و بر رود نهد بند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پیشه‌ور و صنعتگر و دهقان و کدیور</p></div>
<div class="m2"><p>ورزشگر و جنگاور و کوشا و قوی زند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پاکیزه و رخشنده شود نفس به تعلیم</p></div>
<div class="m2"><p>چونان که گوارنده شود آب در آوند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گردد ز نکوکاری و دانایی و پاکی</p></div>
<div class="m2"><p>عمرکم ایرانی افزون ز صد و اند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر کار شود مردم دانشور پرکار</p></div>
<div class="m2"><p>نابود شود این گره لافزن رند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ور زان که نمانم من و آن روز نبینم</p></div>
<div class="m2"><p>این چامه بماناد بدین طرفه پساوند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آن کس که دلش بستهٔ جاهست و زر و مال</p></div>
<div class="m2"><p>از دیده خود بیند، بر خلق خداوند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چون گنده‌دهان کز خرد و فهم ‌به ‌دور است</p></div>
<div class="m2"><p>گویدکه مگر کام همه خلق کندگند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آن کس که دلش بستهٔ فکریست چه داند</p></div>
<div class="m2"><p>فکر دگری چون و خیال دگری چند؟</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>این خواندن افکار بود کار حکیمان</p></div>
<div class="m2"><p>بقال‌، گزر داند و جزار جگربند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شیبانی اگر خواندی این چامه نگفتی</p></div>
<div class="m2"><p>«‌زردشت گر آتش را بستاید در زند»</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>این شعر به آیین لبیبی است که فرمود</p></div>
<div class="m2"><p>« گویند نخستین سخن از نامهٔ پازند»</p></div></div>