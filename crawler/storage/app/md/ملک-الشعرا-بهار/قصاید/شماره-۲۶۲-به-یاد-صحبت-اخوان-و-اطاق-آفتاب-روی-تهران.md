---
title: >-
    شمارهٔ ۲۶۲ - به یاد صحبت اخوان و اطاق آفتاب روی تهران
---
# شمارهٔ ۲۶۲ - به یاد صحبت اخوان و اطاق آفتاب روی تهران

<div class="b" id="bn1"><div class="m1"><p>روزگار آشفتگی دارد بسر، کو همدمی</p></div>
<div class="m2"><p>تا ز فیض صحبتش خاطر بیاساید دمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش و ابر و دم و دودست پیدا در افق</p></div>
<div class="m2"><p>کو مقامی امن و جایی محرم و دود و دمی‌؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خدا خواهم اطاق قبلی و یاری سه چار</p></div>
<div class="m2"><p>خادمی محرم که خواهد عذر هر نامحرمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بست مشک‌آلوده جوشان از بر شاخ کهور</p></div>
<div class="m2"><p>به که از کین بر گلوی نیزه بندی پرچمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق را زین‌سو مشرف کن گرت هست آرزو</p></div>
<div class="m2"><p>بی‌تغیر عالمی و بی‌تبدل آدمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هجر فرهادش به دل هر لحظه خنجر می‌زند</p></div>
<div class="m2"><p>خود گرفتم شد بهار از حفظ صحت رستمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جای موسی خالی است وآن عصای موسوی</p></div>
<div class="m2"><p>تا که فرعون کسالت را ببلعد در دمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موسیا ز انوار یزدان یک قبس ما را فرست</p></div>
<div class="m2"><p>چون‌ اناالحق زان همایون شعله بشنیدی همی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شبان وادی ایمن چو گشتی بهره‌مند</p></div>
<div class="m2"><p>زان درخت شعله‌ور فکر برادر کن کمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون سحرگاهان نهادی سر به محراب نماز</p></div>
<div class="m2"><p>بهر قلب ما فرست از دود آهی مرهمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یاد لطف صحبت اخوان درخشد در دلم</p></div>
<div class="m2"><p>چون چراغ روشنی در جایگاه مظلمی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس که خوردم چایی دم ناکشیده در سویس</p></div>
<div class="m2"><p>آبم افتد در دهان از یاد چای پر دمی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وز غم نادیدن همصحبتان محترم</p></div>
<div class="m2"><p>مردمان چشم من بستند حلقهٔ ماتمی</p></div></div>