---
title: >-
    شمارهٔ ۲۴۵ - تغزل
---
# شمارهٔ ۲۴۵ - تغزل

<div class="b" id="bn1"><div class="m1"><p>پیمان‌شکن نگار من آن ترک لشکری</p></div>
<div class="m2"><p>بگرفت خوی لشکری و شد ز من بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من دل به لشکری ز چه دادم به خیر خیر</p></div>
<div class="m2"><p>هشیار مرد، دل نسپارد به لشکری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او خود سپاهی است و رود، چون رود سپاه</p></div>
<div class="m2"><p>گوید مرا که بنشین وز هجر خون گری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دم ز هجر آن رخ چون سیم نابسود</p></div>
<div class="m2"><p>یاقوت سوده بارم بر زر جعفری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه گرم ببینی خسته ز درد هجر</p></div>
<div class="m2"><p>براین تن پرآفت من رحمت آوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بود صف شکست و کمند و کمان گرفت</p></div>
<div class="m2"><p>ایدون گمان کند که چنین است دلبری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قلب سپاه را نشناسد ز قلب دوست</p></div>
<div class="m2"><p>قلب تو بر درد چو به جولانش بگذری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیوسته چون پری است نهفته ز چشم من</p></div>
<div class="m2"><p>گر نه پری است از چه نهانست چون پری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق این‌چنین نخواهم چون نیست درخورم</p></div>
<div class="m2"><p>ای عشق مر مرا تو بدینسان نه در خوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق بتی گزینم‌، دلخواه و سازگار</p></div>
<div class="m2"><p>چون دیگران نداشته رسم ستمگری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با گیسوی شکسته‌تر از پشت بیدلان</p></div>
<div class="m2"><p>با چهرهٔ شکسته‌تر از لالهٔ طری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کر بنگری بر آن رخ و بالای او درست</p></div>
<div class="m2"><p>بینی مه چهارده بر سرو کشمری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دیبای ششتری است بناگوش و روی او</p></div>
<div class="m2"><p>مشک سیه دمیده ز دیبای ششتری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از روی اوست خوبی و نیکی ستوده فال</p></div>
<div class="m2"><p>چون از ولی داور، آئین داوری</p></div></div>