---
title: >-
    شمارهٔ ۲۷۲ - آزرم
---
# شمارهٔ ۲۷۲ - آزرم

<div class="b" id="bn1"><div class="m1"><p>ای برادر، تا توانی گیر با آزرم‌ خوی</p></div>
<div class="m2"><p>مرد بی‌آزرم باشد چون زن بسیار شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیرت و صدق و امانت‌، کاین سه اصل مردمیست</p></div>
<div class="m2"><p>اصلشان ز آزرم خیزد، گیر با آزرم‌خوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه در پیش کسان آزرم خود بر خاک ریخت</p></div>
<div class="m2"><p>غیرت و صدق و امانت خوار باشد پیش اوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانکه کشت عصمتش سیراب گشت از آب خلق</p></div>
<div class="m2"><p>روی ازو برتاب‌، کاندر وی نیابی آبروی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رادی و مردی‌، صفات ثابت آمیغی‌اند</p></div>
<div class="m2"><p>رادی از ناکس مخواه و مردی از غرزن مجوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه گردد گرد کژی‌، ای پسر گردش مگرد</p></div>
<div class="m2"><p>هرکه پوید سوی پستی‌، یا بنی سویش مپوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بمیری‌، پای خود بر خاک نامردان منه</p></div>
<div class="m2"><p>ور بسوزی‌، دست‌خویش از آب ناپاکان مشوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>معنی صدق و وفا و شرم در آزادیست</p></div>
<div class="m2"><p>ای ‌«‌بهار» آزاد باش و هرچه می‌خواهی بگوی</p></div></div>