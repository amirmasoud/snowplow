---
title: >-
    شمارهٔ ۱۶۱ - رستم نامه
---
# شمارهٔ ۱۶۱ - رستم نامه

<div class="b" id="bn1"><div class="m1"><p>شنیده‌ام که یلی بود پهلوان رستم</p></div>
<div class="m2"><p>کشیده سر ز مهابت بر آسمان رستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستبر بازو و لاغر میان و سینه فراخ</p></div>
<div class="m2"><p>دو شاخ ریش فرو هشته تا میان رستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیاش سام و پدر زال و مام رودابه</p></div>
<div class="m2"><p>ز تخم گرشاسب مانده در جهان رستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کودکی سرپیل سپیدکفته به گرز</p></div>
<div class="m2"><p>سپس به دیو سپید آخته سنان رستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بریده کله اکوان دیو و هشته به ترک</p></div>
<div class="m2"><p>به جای مغفر پولاد زرنشان رستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریده چرم ز ببر بیان وکرده به‌بر</p></div>
<div class="m2"><p>به جای جوشن و خفتان پرنیان رستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بود یافته ز اخلاط معتدل ترکیب</p></div>
<div class="m2"><p>بماندی ار نشدی کشته رایگان رستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شنیدم آنکه به چاه شغاد در کابل</p></div>
<div class="m2"><p>نمرد و بیرون آمد ازآن میان رستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شرم کشتن اسفندیار و شنعت آن</p></div>
<div class="m2"><p>نهاد سر به بیابان هندوان رستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پی معالجت زخم و دوری از ایران</p></div>
<div class="m2"><p>به جنگلی شد و بود اندر آن مکان رستم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گزید کیش زراتشت و توبه کرد و نشست</p></div>
<div class="m2"><p>به پیش آتش و گردید زند خوان رستم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو یافت آگهی از پهلوی که در ایران</p></div>
<div class="m2"><p>گزیده مسند دارا و اردوان رستم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نفوذ ترک‌ و عرب کم‌شده است‌ و مردم پارس</p></div>
<div class="m2"><p>نهاده نام خود این کیقباد و آن رستم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کشید رخت به زابل‌زمین ز خطهٔ هند</p></div>
<div class="m2"><p>به کوه‌ خواجه درون شد چو کهبدان رستم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به‌شهر طاق‌ سپس قلعه‌ای و ارگی ساخت</p></div>
<div class="m2"><p>که دورتر بود از راه کاروان رستم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خرید مزرعه‌ای در جوار طاق و نشست</p></div>
<div class="m2"><p>درون مزرعه خرسند و کامران رستم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به یاد آتش کرکوی‌، آتشی افروخت</p></div>
<div class="m2"><p>نهاد نامش کرکوی رستمان رستم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نهفته داشت زر و سیم و گوهر و کالا</p></div>
<div class="m2"><p>از آن زمانه کجا بوده مرزبان رستم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گشادگنج و نشست ازپی عبادت حق</p></div>
<div class="m2"><p>ز مهر ایران سرشار و شادمان رستم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خطا نکرده به تدبیر ملک دست از پای</p></div>
<div class="m2"><p>گذشته از سر دیهیم زرنشان رستم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نکرده خودسری و ساخته به لقمهٔ نان</p></div>
<div class="m2"><p>ز جمع حاصل املاک سیستان رستم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گذشته از سر دعوی سند و بست و فراه</p></div>
<div class="m2"><p>نهفته روی ز مخلوق بدنشان رستم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز ناگه آمد بهر ممیزی سوی طاق</p></div>
<div class="m2"><p>یکی جوان و ببردش به میهمان رستم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی جوانک ازین لاله زاریان که بود</p></div>
<div class="m2"><p>به‌ زر حریص‌ چو بر جنگ هفتخوان رستم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به پای چکمه و پیراهنی و پالتوی</p></div>
<div class="m2"><p>بدان غرور که گفتی بود جوان رستم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به طرز مردم ری گرم شد به نطق و بیان</p></div>
<div class="m2"><p>که درنیافت یکی گفته زان میان رستم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز جیب قوطی سیگار چون برون آورد</p></div>
<div class="m2"><p>شگفت ماند از آن مخزن دخان رستم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو زد به آتش سیگار را و برد به‌لب</p></div>
<div class="m2"><p>ز حیرت آورد انگشت بر دهان رستم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پذیره گشت ورا در سرای بیرونی</p></div>
<div class="m2"><p>نهاد در بر او خوان پرزنان رستم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو خواست منقلی از بهر فور، کرد بدل</p></div>
<div class="m2"><p>یکی ز مغبچگان مرد را گمان رستم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شکفته گشت‌ و یکی مجمرش نهاد به‌ پیش</p></div>
<div class="m2"><p>سرود خواند به آیین مسمغان رستم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جوان کشید چو از جامدان برون وافور</p></div>
<div class="m2"><p>به یادش آمد ازگرزهٔ گران رستم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خیال کرد که فور از نژادهٔ کرز است</p></div>
<div class="m2"><p>ازین خیال دلش گشت شادمان رستم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ولی چو فور به تدخین نهاد بر آتش</p></div>
<div class="m2"><p>بجست ناگه از جا سپندسان رستم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بگفت هی پسرآتش کسی به کرز نکوفت</p></div>
<div class="m2"><p>مکن وگرنه شود دشمنت به جان رستم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سپس چوبستی بربست و دود بیرون داد</p></div>
<div class="m2"><p>ز دودو حیرت شد گیج در زمان رستم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گرفت بینی و سرفید و بهر قی کردن</p></div>
<div class="m2"><p>به باغ تاخت ز مشکوی پر دخان رستم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به چاکرانش چنین کفت‌: گر ز من پرسید</p></div>
<div class="m2"><p>بدو بگویید افتاده ناتوان رستم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جوان از آن روش پهلوان کمی واخورد</p></div>
<div class="m2"><p>که درگذاشت ره و رسم میزبان رستم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هزارها متلک بار پیرمرد نمود</p></div>
<div class="m2"><p>که بازگشت به‌ناچار سوی خوان رستم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به‌شرم گفت‌: الا ثسپوهر خوشمت هی</p></div>
<div class="m2"><p>پذت ‌هزینه گرت گنج و مهن و مان رستم </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هنوز چیزی‌ناخورده‌ خواست جام شراب</p></div>
<div class="m2"><p>جوان و گشت ازین کرده بدگمان رستم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خیال کردکه مهمان غذا برون خوردست</p></div>
<div class="m2"><p>وزین خیال برآشفت بیکران رستم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>معاشران عجم می پس از غذا نوشند</p></div>
<div class="m2"><p>چو پیش ‌خورد جوان طیره گشت ‌از آن رستم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جوان‌ چو خورد می کهنه شد بخوان و بکرد</p></div>
<div class="m2"><p>دعای زمزمه آغاز، پیش خوان رستم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ولیک مهمان خامش نماند و صحبت کرد</p></div>
<div class="m2"><p>میان زمزم و زد مهر بر دهان رستم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو خوان گذارده شد و آب دست آوردند</p></div>
<div class="m2"><p>نهاد بزم به آیین خسروان رستم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نبید و نقل و بخور و ترنج و سیب و انار</p></div>
<div class="m2"><p>به خوانچه‌ای بنهادند و شد چران رستم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو گرم گشت سرش گفت هان فراز آرید</p></div>
<div class="m2"><p>یکی مغنی با زخمهٔ روان‌، رستم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز در درآمد و کرنش نمود زابلئی</p></div>
<div class="m2"><p>چگور برکف و گفتش بزن بخوان رستم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نواخت زخمهٔ سگزی به پهلوی ابیات</p></div>
<div class="m2"><p>شد از نشاط ‌سرشگش به ‌رخ ‌چکان رستم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سه جام خورد و نکرد اعتنا به مرد جوان</p></div>
<div class="m2"><p>از آنکه بد ز اداهاش سرگران رستم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جوان برفت و بیامد به کف یکی ویولن</p></div>
<div class="m2"><p>نمود کوک و نکرد اعتنا بدان رستم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ولی چو کرد به سیم آرشی کمان را جفت</p></div>
<div class="m2"><p>چو تیر راست شد از فرط امتنان رستم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو رند بود جوان‌، ساخت پردهٔ بیداد</p></div>
<div class="m2"><p>ز فرط شادی مستانه شد چمان رستم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بخواند قصهٔ اسفندیار رویین‌تن</p></div>
<div class="m2"><p>که درنبرد بکشتش به رایگان رستم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گریست رستم و شد داغ خاطرش تازه</p></div>
<div class="m2"><p>بگفت کاش نبودی در این جهان رستم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>من آن گنه بنکردم که کرد آن گشتاسب</p></div>
<div class="m2"><p>وگرنه بود به جان بندهٔ بغان رستم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گسیل کرد به کاری گزافه پور جوان</p></div>
<div class="m2"><p>بشد جوان و شد از مرگ او نوان رستم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زکردهٔ دگران کو، که کارنامهٔ خویش</p></div>
<div class="m2"><p>بسا شنوده ز دوران باستان رستم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جوان درست نفهمیدکاو چه گفت ولی</p></div>
<div class="m2"><p>به‌طبع‌شد سر تصنیف‌و رست‌از آن‌رستم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چوگشت صبح فرستاد چند سکهٔ زر</p></div>
<div class="m2"><p>به رسم هدیه به نزدیک میهمان رستم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نهاد خنجر زربن نیام دسته نشان</p></div>
<div class="m2"><p>به روی هدیه به عنوان ارمغان رستم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جوان چو آنهمه زربنه دید، کرد طمع</p></div>
<div class="m2"><p>که دور سازد ازآن کاخ وگنج وکان رستم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پس از دو هفته به رستم بداد دست وداع</p></div>
<div class="m2"><p>فشرد دست وی و ساختش روان رستم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>جوان‌ چو شد به ‌خراسان گزارشی‌ برداشت</p></div>
<div class="m2"><p>که‌هست‌سرکش‌و خودکام‌و بدزبان رستم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به فکر تجزیهٔ سیستان فتاده‌، از آن</p></div>
<div class="m2"><p>تفنگ و توپ کند جمع در نهان رستم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>من اهل قلعهٔ او را نهان فریفته‌ام</p></div>
<div class="m2"><p>که وقت جنگ بگیرند ناگهان رستم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اگر به بنده ز لشکر دهند گردانی</p></div>
<div class="m2"><p>شود دلیری و گردش بی نشان رستم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سپهبدان خراسان فسون او خوردند</p></div>
<div class="m2"><p>که شد ز همت او نیتش عیان رستم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>جوان کشید سوی مرز سیستان جیشی</p></div>
<div class="m2"><p>به قصد طاق که بود اندر آن مکان رستم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>سبه پراند په‌صحرای رپک وثاخث به دز</p></div>
<div class="m2"><p>که جفت سازد با اندوه و غمان رستم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سپه رسید بر طاق و دیده‌بان از ارگ</p></div>
<div class="m2"><p>بدید و گشت خبردار در زمان رستم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گمان نمود ز توران سپاهی آمده است</p></div>
<div class="m2"><p>که بود از ایران پیوسته در امان رستم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بگفت ببر بیان آورند و تیر وکمان</p></div>
<div class="m2"><p>کشید موزه و آویخت تیردان رستم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بدید ببر بیان کرم خورده و ضایع</p></div>
<div class="m2"><p>هم اوفتاده خم از پشت درکمان رستم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>فکند چاچی و خفتان وگرز را برداشت</p></div>
<div class="m2"><p>نهاد زین زبر رخش ناتوان رستم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز قلعه تاخت برون با سه چار نوکر پیر</p></div>
<div class="m2"><p>براند جانب گردان سبک‌عنان رستم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>فکند حمله و زد نعره‌ای بلند و به گرز</p></div>
<div class="m2"><p>بکوفت از دو سه سرباز، استخوان رستم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بر اوگلوله ببارید از دو سو چو تگرگ</p></div>
<div class="m2"><p>فتاد رخش و بغم گشت توامان رستم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>پیاده ماند و نگه کرد و دید سلطان را</p></div>
<div class="m2"><p>شتافت جانب سلطان دوان دوان رستم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ز هیبتش‌دو سه گز پس‌نشست مرد جوان</p></div>
<div class="m2"><p>ز بیم کش برساند مگر زیان رستم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به شک فتاد تهمتن که خود مگر بزه کرد</p></div>
<div class="m2"><p>از انکه رفت به پیکار دوستان رستم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ز یک‌طرف‌رهیانش به جنگ کشته شدند</p></div>
<div class="m2"><p>اپن دو فکر شد از دیده خونفشان رستم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>جوان چو دید که رستم گریست گشت دلیر</p></div>
<div class="m2"><p>بگفت زنده بگیرید هان و هان رستم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بریختند به گردش پیادگان سپاه</p></div>
<div class="m2"><p>چو خیل مورکه گیرند در میان رستم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نخواست‌تاکشد آن‌قوم را به مشت و لگد</p></div>
<div class="m2"><p>فکند با لم کشتی یگان دوگان رستم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>دوان‌دوان‌به‌سوی‌قلعه‌شد ز عرصهٔ جنگ</p></div>
<div class="m2"><p>ز خشم‌، دل شده در سینه‌اش طپان رستم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>جوان‌چو دیدکه‌رستم‌بجست‌، گفت دهید </p></div>
<div class="m2"><p>بگشت ناگه صد تیر را نشان رستم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بجست در بن‌چاهی که‌داشت ره به‌حصار</p></div>
<div class="m2"><p>ز راه نقب سوی قلعه شد دوان رستم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>کشیدتخته پل ودرببست وشد محصور</p></div>
<div class="m2"><p>حصار داد به آیین جنگیان رستم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>هجوم بردند از هر طرف به قلعه وگشت</p></div>
<div class="m2"><p>طپان ز بیم اسارت نه بیم جان رستم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به یادش آمد ناگه ز وعدهٔ سیمرغ</p></div>
<div class="m2"><p>طلب نمود مر او را از آشیان رستم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تریز جبه به خنجر درید و آخت برون</p></div>
<div class="m2"><p>پری که داشت نهان در میان جان رستم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>فسون بخواند وبزد سنگی از برآهن</p></div>
<div class="m2"><p>بجست برقی و شد سخت شادمان رستم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>پس از دو ساعت اندر افق سیاهی دید</p></div>
<div class="m2"><p>که می‌درآمد از اقصای زاهدان رستم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>نگاه کرد به بالا و دید پران است</p></div>
<div class="m2"><p>سطبر مرغی رویینه استخوان رستم</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>فرو نشست خروشان درون میدانی</p></div>
<div class="m2"><p>که اسپریس نوین کرد نام آن رستم</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>زپشت مرغ فرو جست لاغر اندامی</p></div>
<div class="m2"><p>که دیده بودش در هند یک زمان رستم</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>به هندوی سخنی چندگفت ورستم را</p></div>
<div class="m2"><p>سوارکرد و شد از دیده‌ها نهان رستم</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>محاصران در دز بستدند و نعره زدند</p></div>
<div class="m2"><p>وزین طرف به‌سوی هند شد پران رستم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ببرد همره خودگنج و مال و پیمان کرد</p></div>
<div class="m2"><p>کزین سپس نکند رای امتحان رستم</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>وگر دوباره بیفتد به یاد ملک کیان</p></div>
<div class="m2"><p>کمست در بر مردان‌، ز ماکیان رستم‌!</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>دروغ و حقه وافور و جعبهٔ سیگار</p></div>
<div class="m2"><p>چسان نهد به بر فره کیان رستم‌؟</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>زبان پارسی باستان چگونه نهد</p></div>
<div class="m2"><p>بر تلفظ طهران و اصفهان رستم‌؟</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>فراخنای لب هیرمند وگود زره</p></div>
<div class="m2"><p>کجا نهد ببرکند و سولقان رستم‌؟</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چه جای مقبرهٔ مجلسی و مسجد شیخ‌؟</p></div>
<div class="m2"><p>که نیست درهوس طوس وطابران رستم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>به‌لون‌ظاهرشان کی‌خورد فریب چو یافت</p></div>
<div class="m2"><p>خبر ز باطن این قوم بد نهان رستم</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>خیانتی که به دارا نموده‌اند این قوم</p></div>
<div class="m2"><p>به یاد دارد از عهد باستان رستم</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>همش به یاد بود آنچه رفت ازین مردم</p></div>
<div class="m2"><p>به تاج وتخت شهنشاه اردوان رستم</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>کجا ز یاد برد آنچه زین جفاکاران</p></div>
<div class="m2"><p>برفت بر سر پرویز‌ و خاندان رستم</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>همی بگرید از آن غدر ماهوی سوری</p></div>
<div class="m2"><p>به یزدگرد، به صحرای خاوران رستم</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>ز بیم جست و به سوی قفا ندید، چو دید</p></div>
<div class="m2"><p>که گرگ برگله گشته است پاسبان رستم</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>براستان که برون زاستانه‌اند، گریست</p></div>
<div class="m2"><p>چو دیدکج‌منشان را بر آستان رستم</p></div></div>