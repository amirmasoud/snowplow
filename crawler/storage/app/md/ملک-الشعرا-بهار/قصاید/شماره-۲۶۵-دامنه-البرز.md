---
title: >-
    شمارهٔ ۲۶۵ - دامنه البرز
---
# شمارهٔ ۲۶۵ - دامنه البرز

<div class="b" id="bn1"><div class="m1"><p>باد صبح ازکوهسار آید همی</p></div>
<div class="m2"><p>یاد یار غمگسار آید همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارگوبی سوی شهر آید زکوه</p></div>
<div class="m2"><p>دوست گویی از شکار آید همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بامدادان در هوای گرم ری</p></div>
<div class="m2"><p>بوی لطف نوبهارآید همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلهٔ البرز در چشمان من</p></div>
<div class="m2"><p>چون یکی زیبانگار آید همی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر فراز فرق‌، سیمین چادرش</p></div>
<div class="m2"><p>لعبتی سیمین‌ عذار آید همی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز چون تابد بر او زرین فروغ</p></div>
<div class="m2"><p>چون درخشی زرنگار آید همی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در نشیبش سبز وادی‌ها ز دور</p></div>
<div class="m2"><p>دیده را شادی گوار آید همی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راست گویی سوی دشت از کوهسار</p></div>
<div class="m2"><p>لشگری نیزه گذار آید همی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیل ‌در خیل‌ و درفش‌ اندر درفش</p></div>
<div class="m2"><p>این پیاده وآن سوار آید همی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کوشک‌ها هر جای محصور از درخت</p></div>
<div class="m2"><p>چون حصاری استوار آید همی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گردشان اشجار چون جیشی که ‌تنگ</p></div>
<div class="m2"><p>گرد برگرد حصار آید همی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا تناور کشتیئی در سخت موج</p></div>
<div class="m2"><p>کش‌پس‌ازکوشش قرار آید همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آبشار ازگوشهٔ وادی به چشم</p></div>
<div class="m2"><p>چون‌یکی سیمینه تار آید همی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور برو نزدیک تازی در نظرت</p></div>
<div class="m2"><p>همچو پولادین منار آید همی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گویی از بالا خروشان زی نشیب</p></div>
<div class="m2"><p>اژدهایی دیوسار آید همی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آسمان باشد به لون و در صفت</p></div>
<div class="m2"><p>آسمانی آبدار آید همی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرنه چرخست‌ از چه‌اش قوس قزح</p></div>
<div class="m2"><p>از گریبان آشکار آید همی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وز چه ‌همچون کهکشان ‌در وی‌ پدید</p></div>
<div class="m2"><p>اختران بی‌شمار آید همی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سبزه اندر سبزه یا بی‌پر نسیم</p></div>
<div class="m2"><p>گرت زی بالاگذار آید همی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چشمه اندر چشمه بینی پرفروغ</p></div>
<div class="m2"><p>چونت ره زی جویبار آید همی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون ز بالا بنگری سوی نشیب</p></div>
<div class="m2"><p>در سر از هولت دوار آید همی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بوی‌های تازه همراه صبا</p></div>
<div class="m2"><p>ازکران مرغزار آید همی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بانگ کبک آید ز بالای کمر</p></div>
<div class="m2"><p>بانگ قمری از چنار آید همی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر سحرگاهان خروشان جغد زار</p></div>
<div class="m2"><p>روح را انده گسار آید همی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در میان دشت ز انبوه هوام</p></div>
<div class="m2"><p>زیر زیر و زار زار آید همی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جرد گوبی نزد مام زنجره</p></div>
<div class="m2"><p>از برای خواستار آید همی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر زمان بادی قوی با تود بن</p></div>
<div class="m2"><p>در جدال و گیر و دار آید همی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون بر ایوب نبی زرین ملخ</p></div>
<div class="m2"><p>تودها بر ما نثار آید همی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مارچوبه در بن سنگ سیاه</p></div>
<div class="m2"><p>چون یکی خمیده مار آید همی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شاخ‌ آلوی سیاه اندر مثل</p></div>
<div class="m2"><p>همچو ماری بالدار آید همی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سیب زرد و اندر او رگ‌های سرخ</p></div>
<div class="m2"><p>همچو روی باده‌خوار آید همی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شاخ امرود خمیده پیش باد</p></div>
<div class="m2"><p>خاضع و پوزش گزار آید همی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گرچه بسیارند یارانت ولیک</p></div>
<div class="m2"><p>یار کمتر چون بهار آید همی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>صادق و مردانه و بیدار مغز</p></div>
<div class="m2"><p>یار باید تا به کار آید همی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یار آن باشد که روز بستگی</p></div>
<div class="m2"><p>بهر یاران بی‌قرار آید همی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ورنه هنگام گشایش مرد را</p></div>
<div class="m2"><p>هر دمی هفتاد یار آید همی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در فزون یاری چو تخمی کاشتی</p></div>
<div class="m2"><p>روز بی‌یاری به بار آید همی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وه که ایدون دوستاری شد گزاف</p></div>
<div class="m2"><p>چشم ازین غم اشکبار آید همی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آنکه اندر راه او دادی دو چشم</p></div>
<div class="m2"><p>گر تو را سویش گذار آید همی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>راست پنداری که از دیدار تو</p></div>
<div class="m2"><p>در دو چشمش ‌نوک ‌خار آید همی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا زرت‌ در دست ‌و زورت‌ در تن ‌است</p></div>
<div class="m2"><p>آسمانت دوستار آید همی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون‌ زرت ‌بگسست‌ زورت هم نماند</p></div>
<div class="m2"><p>دوستان را از تو عار آید همی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>برخی آنم که در درماندگی</p></div>
<div class="m2"><p>دوستان را دستیار آید همی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من بر آن عهدم که با تو عهد من</p></div>
<div class="m2"><p>تا قیامت استوار آید همی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر دهم جان در رهت اندر دلم</p></div>
<div class="m2"><p>حاش لله گر غبار آید همی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>و در استغنا زنم عیبم مکن</p></div>
<div class="m2"><p>کافتخار از افتقار آید همی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>من ز بهر نام بگذشتم ز نان</p></div>
<div class="m2"><p>کاحتشام از اشتهار آید همی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرد را ندهد ز یک منظور بیش</p></div>
<div class="m2"><p>آنکه نامش روزگار آید همی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کامیابی نیست جز در یک امل</p></div>
<div class="m2"><p>باید این بر زرنگار آید همی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چاپلوس وکربز و دورو نیم</p></div>
<div class="m2"><p>زین عیوبم انضجار آید همی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یار صد روی و مزور مرد را</p></div>
<div class="m2"><p>هر به روزی صدهزار آید همی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>لیک از ین یاران به روز بی‌کسی</p></div>
<div class="m2"><p>ناکسم گر هیچ کار آید همی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فخرم این بس کز زبان و کلک من</p></div>
<div class="m2"><p>نکته‌های شاهوار آید همی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دوستداران را به نطق و نظم و نثر</p></div>
<div class="m2"><p>فکرتم خدمتگزار آید همی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بد سگالان را به او بار روان</p></div>
<div class="m2"><p>خامهٔ من اژدهار آید همی</p></div></div>