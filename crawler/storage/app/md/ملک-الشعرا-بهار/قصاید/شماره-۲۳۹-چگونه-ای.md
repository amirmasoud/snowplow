---
title: >-
    شمارهٔ ۲۳۹ - چگونه‌ای؟!
---
# شمارهٔ ۲۳۹ - چگونه‌ای؟!

<div class="b" id="bn1"><div class="m1"><p>هان ای فراخ عرصهٔ تهران چگونه‌ای</p></div>
<div class="m2"><p>زیر درفش قائد ایران‌، چگونه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گرگ پیر، بهر مکافات خون خلق</p></div>
<div class="m2"><p>در زیر چنگ ضیغم غژمان چگونه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای منبع شرارت و ای مرکز فساد</p></div>
<div class="m2"><p>آرام و برده سر به گریبان‌، چگونه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای برده احترام بزرگان و قائدان</p></div>
<div class="m2"><p>هان ییش قائدان و بزرگان چگونه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان افترا و غیبت و غوغا و سرکشی</p></div>
<div class="m2"><p>لب بسته پاکشیده به دامان چگونه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دادی به باد عرض بسی مردم شریف</p></div>
<div class="m2"><p>زان کرده‌های زشت‌، پشیمان چگونه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود این گنه ز جمع قلیل و تو بی گناه</p></div>
<div class="m2"><p>ای بی گنه‌، معاقب گیهان چگونه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تلخی نصیحت یاران شدی ملول</p></div>
<div class="m2"><p>با تلخی نصیحت دوران چگونه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بودستی از نخست کج و هان به تیغ شاه</p></div>
<div class="m2"><p>ای کج خرام‌، راست بدین‌سان چگونه‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون راست‌رو شدی‌، شهت از خاک برگرفت</p></div>
<div class="m2"><p>ای گوی خوش، درین خم چوگان چگونه ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بودی بسان ‌دوزخ و گشتی بسان خلد</p></div>
<div class="m2"><p>ای خلد پر ز حور و ز غلمان چگونه‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز آب‌ عطای شاه‌ چو رضوان شدی به ‌روی</p></div>
<div class="m2"><p>ای آب روی روضهٔ رضوان چگونه‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تفسیق کردی آن که کلاهی نهاد کج</p></div>
<div class="m2"><p>با کج کلاهکان غزلخوان چگونه‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تکفیر کردی آن که سخن گفت ‌از حجاب</p></div>
<div class="m2"><p>هان با زنان موی پریشان چگونه‌ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بودی به ضدّ مدرسهٔ تازه‌، وین زمان</p></div>
<div class="m2"><p>با صدهزار طفل دبستان چگونه‌ای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای عاشق حکومت ملی‌، جهان گرفت</p></div>
<div class="m2"><p>فاشیست روم و نازی آلمان چگونه‌ای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کردی پی عوارض جزئی فسادها</p></div>
<div class="m2"><p>با این عوارضات فراوان چگونه‌ای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای بانگ‌زن چو جغدان بر منبر ریا</p></div>
<div class="m2"><p>هان از پس ترازوی دکان چگونه‌ای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسیار گفتمت که به یاران جفا مکن</p></div>
<div class="m2"><p>کردی و دیدی آفت خذلان چگونه‌ای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کردی فدای شهرت کاذب‌، شئون ملک</p></div>
<div class="m2"><p>ای دم بریده لیدر ذی‌شأن چگونه‌ای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بنگر به نوبهار که این روزهای سخت</p></div>
<div class="m2"><p>دیدست و گفته عاقبت آن‌، چگونه‌ای</p></div></div>