---
title: >-
    شمارهٔ ۸۱ - نکوهش چرخ (راستگوی شادزی)
---
# شمارهٔ ۸۱ - نکوهش چرخ (راستگوی شادزی)

<div class="b" id="bn1"><div class="m1"><p>ویحک ای افراشته چرخ بلند</p></div>
<div class="m2"><p>چند داری مر مرا زار و نژند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خستن روشن‌ضمیران تا به کی</p></div>
<div class="m2"><p>کشتن آزادمردان تا به چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به کی در خون مارانی غراب</p></div>
<div class="m2"><p>تا به کی برنعش ما تازی سمند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند هر جا یاوه‌، پویان چون نسیم</p></div>
<div class="m2"><p>چند هر سو خیره‌، تازان چون نوند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی بغی زین نظام ناروا</p></div>
<div class="m2"><p>کی بمانی زین طریق ناپسند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی شود آمیخته در جام دهر</p></div>
<div class="m2"><p>سعد و نحست‌ چون به‌ ساغر زهر و قند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی شود بشکسته این طاق نفاق</p></div>
<div class="m2"><p>کی شود بگسسته این دام گزند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی نجوم ازهرطرف برهم خورند</p></div>
<div class="m2"><p>پس فرو ریزند ازین طاق بلند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی فرو مانند هفت اختر ز سیر</p></div>
<div class="m2"><p>وان ثوابت بگسلند این پای‌بند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی جهند ازکهکشان‌ها اختران</p></div>
<div class="m2"><p>نیم‌سوزان همچو از مجمر سپند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کی زمان نابود گردد چون مکان</p></div>
<div class="m2"><p>بگسلد مر جاذبیت را کمند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چند از این افسانهٔ بی‌پا و سر</p></div>
<div class="m2"><p>چند از این بازبچه ناسودمند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت اصل آدمیزاد ازگیا ست</p></div>
<div class="m2"><p>زردهشت پیر، در استاد و زند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن گیا اکنون درختی شدکه هست</p></div>
<div class="m2"><p>برگ و بارش نیزه و گرز وکمند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود خوراک گوسپندان بود وکرد</p></div>
<div class="m2"><p>نوع خود را پاره همچون گوسپند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر زمان رنگی دگر پیدا کنی</p></div>
<div class="m2"><p>روز و شب سازی بدین نیرنگ و فند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گه کنی زاکسون‌، پرند نیلگون </p></div>
<div class="m2"><p>گاه وشی سازی از نیلی پرند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وین دورنگی را زمان خوانیم ما</p></div>
<div class="m2"><p>از دمادم گشتنش نگرفته پند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سست‌ پی‌ چون ‌باد و پرّان‌ چون‌ درخش</p></div>
<div class="m2"><p>تیزپرچون تیروبران چون فرند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وهم‌رنگ آموده را خوانیم عمر</p></div>
<div class="m2"><p>غره زبن‌مشتی فسون وریشخند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سربسر وهم است و پندار و غرور</p></div>
<div class="m2"><p>گر دو روز است‌آن‌وگر صدسال و اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چند بایست این فریب و رنگ و ریو</p></div>
<div class="m2"><p>چند بایست این فسون و مکر و فند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چند باید چون ستوران روز و شب</p></div>
<div class="m2"><p>جان شیرین صرف سکبا و پژند </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چند باید تن قوی و جان ضعیف</p></div>
<div class="m2"><p>خادمان فربی و سلطان دردمند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تن چه ورزی‌، جان به ورزش برگمار</p></div>
<div class="m2"><p>سوزن بشکسته مگزی بر کلند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر سپهر آتش فرو ریزد مجوش</p></div>
<div class="m2"><p>ور زمانه رو ترش سازد بخند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پر مجوش ‌ار سخت ‌خام‌ است این جهان</p></div>
<div class="m2"><p>پخته گردد چون گذارد روز چند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگذر از آبادی این کهنه دیر</p></div>
<div class="m2"><p>بگذر از معماری این کندمند </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جامهٔ کوته سزد کوتاه را</p></div>
<div class="m2"><p>نو کند جامه چو کوته شد بلند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو به راهش بر گل و ریحان نشان</p></div>
<div class="m2"><p>گر رفیقی پیش راهت چاه کند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو نکو می‌باش و بپذیر این مثل</p></div>
<div class="m2"><p>چاه‌کن خود را به چاه اندر فکند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برکسی مپسند کز تو آن رسد</p></div>
<div class="m2"><p>کت نیاید خویشتن را آن پسند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>راست گوی و نیک بین و شاد زی</p></div>
<div class="m2"><p>گوش دار و یادگیر و کار بند</p></div></div>