---
title: >-
    شمارهٔ ۱۵۴ - هرج و مرج
---
# شمارهٔ ۱۵۴ - هرج و مرج

<div class="b" id="bn1"><div class="m1"><p>بزم طرب ساز و کن فراز در غم</p></div>
<div class="m2"><p>سادهٔ زیبا بخواه و بادهٔ درغم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون سیاووش ریز در کف موسی</p></div>
<div class="m2"><p>قبلهٔ زردشت زن به خیمهٔ رستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطرب‌! چون ساختی نوای همایون</p></div>
<div class="m2"><p>گاه ره زیر ساز و گاه ره بم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده همی ده مرا و بوسه همی ده</p></div>
<div class="m2"><p>بوسه مؤخر خوش است و باده مقدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی‌، روی تو زیر زلف چه باشد</p></div>
<div class="m2"><p>روزی روشن نهفته در شب مظلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویی پشت من است زلف تو آری</p></div>
<div class="m2"><p>ورنه چرا شد چنین شکسته و درهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنها پشت من از تو خم نگرفته است</p></div>
<div class="m2"><p>پشت جهانی است زیر بار غمت خم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز غم رویت‌، مراست غم‌ها در دل</p></div>
<div class="m2"><p>خوش به مثل گفته‌اند یک دل و صد غم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد غم زلفت مرا زیاد چو دیدم</p></div>
<div class="m2"><p>ملک پریشان و کار ملک مقصم‌</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملکی کز دیرباز عهد کیومرث</p></div>
<div class="m2"><p>بود چو باغ ارم شکفته و خرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیده شکوه سیامک و فر هوشنگ</p></div>
<div class="m2"><p>شوکت طهمورث و شهنشهی جم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فر فریدون و دادخواهی کاوه</p></div>
<div class="m2"><p>رای منوچهر و کینه‌توزی نیرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>داوری کیقباد و حشمت کاووس</p></div>
<div class="m2"><p>فره کیخسرو و شجاعت رستم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وان ملکان گذشته کز دهش و داد</p></div>
<div class="m2"><p>بد همه را ملکت زمانه مسلم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وان وزرای بزرگ کاندر هر کار</p></div>
<div class="m2"><p>بودند از کردگار گیتی ملهم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وانهمه جنگ‌ آوران نیو که بودند</p></div>
<div class="m2"><p>جمله به نیروی‌ پیل‌ و حمله ضیغم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>و آن علم کاویان که در همه هنگام</p></div>
<div class="m2"><p>نصرت و اقبال بسته داشت به پرچم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ملکی چونین که بُد عروس زمانه</p></div>
<div class="m2"><p>شد رخش اکنون به داغ فتنه موسم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکسو در خاک خفت شاه مظفر</p></div>
<div class="m2"><p>یکسو در خون طپید اتابک اعظم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جاهل‌، دانا شدست و دانا، جاهل</p></div>
<div class="m2"><p>شیخ‌، مکلا شدست و میر، معمم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیخردان برگرفته حربهٔ تزویر</p></div>
<div class="m2"><p>گشته‌ به‌ خونریزی‌ ملوک‌ مصمم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مجلس کنکاش گشته ز انبه جهال</p></div>
<div class="m2"><p>چون گه انبوه حاج‌، چشمهٔ زمزم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملک خراسان که بود مخیم ابطال</p></div>
<div class="m2"><p>گشته کنون خیل ابلهان را مخیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یاوه‌سرایان به گرد هم شده انبوه</p></div>
<div class="m2"><p>هر یک را بر زبان کلامی مبهم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>انجمن معدلت ز کار معطل</p></div>
<div class="m2"><p>قومی در وی نشسته بسته ره فم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قومی دیگر به خیره هر سو پویا</p></div>
<div class="m2"><p>تا ز چه ره پر کنند مشرب و مطعم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گوید آن‌ یک که روزنامه حرامست</p></div>
<div class="m2"><p>گرش بخوانی شوی دچار جهنم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حاشیه این بر نظامنامه نویسد</p></div>
<div class="m2"><p>یکسو ان لایجوز و یکسو ان لم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بینم این جمله را و خون شودم دل</p></div>
<div class="m2"><p>زانکه نیارم کشید از این سخنان دم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حشمت مرد آشکار گردد در کار</p></div>
<div class="m2"><p>نیکی مشک آشکار گردد در شم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>داند کاین دوره عدل باید از یراک</p></div>
<div class="m2"><p>گلشن دولت ز عدل گردد خرم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عدل به کارست و داوری به‌شمارست</p></div>
<div class="m2"><p>صدق پسند است و راستی است مسلم</p></div></div>