---
title: >-
    شمارهٔ ۱۲۶ - تغزل و بهاریه
---
# شمارهٔ ۱۲۶ - تغزل و بهاریه

<div class="b" id="bn1"><div class="m1"><p>گشاده روی بهار، ای گشاده روی بهار</p></div>
<div class="m2"><p>شراب سرخ بخواه و نبید سرخ بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار آمد و سنبل برآمد از لب جوی</p></div>
<div class="m2"><p>به بوی زلف تو ای شمسهٔ بتان بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توازبهار فزونی بتا، به رنگ و به بوی</p></div>
<div class="m2"><p>خود اینکه گفتم از من بسی شگفت مدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهارگیتی روزی دو بیش خرم نیست</p></div>
<div class="m2"><p>بهار روی تو را خرمی بود هموار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جزتو ای به دوزخ رشگ لعبتان چگل</p></div>
<div class="m2"><p>ندیده هیچ کس اندر به هیچ شهر و دیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهار چنگ‌سرای و بهار رودنواز</p></div>
<div class="m2"><p>بهار ساغرگیر و بهار باده گسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهار نبود رشگ نگارخانهٔ چین</p></div>
<div class="m2"><p>بهار نبود بی‌غارهٔ بت فرخار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکن شتاب و به سیر بهار و باغ مرو</p></div>
<div class="m2"><p>وگرکه رفتن خواهی مرا چنین مگذار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بپوش روی و حذرکن که بهر سیر، تو را</p></div>
<div class="m2"><p>ستاده‌اند بسی مرد و زن به راه گذار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکن جدا ز رخ خود کنار و دیدهٔ من</p></div>
<div class="m2"><p>گرم نخواهی رنگین به خون دیده کنار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو نوبهاری و ابر تو دیدگان منست</p></div>
<div class="m2"><p>همی ببارد ابر آن کجاکه بود بهار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز رنج دوری روی تو ای بدیع صنم</p></div>
<div class="m2"><p>ز درد وسختی هجر تو ای ستوده نگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جنان بگریم از دیده گان به دامن دشت</p></div>
<div class="m2"><p>که سیل اشک فرستم همی به دریا بار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی بگریم زار و مرا نگویدکس</p></div>
<div class="m2"><p>که کریه بشکن و ز دیدکان سرشک مبار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنین نگریم‌، نی نی خطاست گریه چنین</p></div>
<div class="m2"><p>به عهد خواجه نه نیکوست گریهٔ بسیار</p></div></div>