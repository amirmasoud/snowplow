---
title: >-
    شمارهٔ ۲۱۸ - تشبیب
---
# شمارهٔ ۲۱۸ - تشبیب

<div class="b" id="bn1"><div class="m1"><p>در باغ تولیت دوش بودم روان به هرسو</p></div>
<div class="m2"><p>آشفته و نظرباز، دیوانه و غزل‌گو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدم به شوخی آنجا افکنده شور و غوغا</p></div>
<div class="m2"><p>عاشق‌کشانِ زیبا گلچهرگانِ مه‌رو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قومی به‌ عشوه ماهر جمعی به چهره باهر</p></div>
<div class="m2"><p>با زلفکان ساحر با چشمکان جادو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کاخ ناز محروس با هم ز مهر مانوس</p></div>
<div class="m2"><p>چون جوجکان طاوس چون بچگان آهو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دلبری زبر دست منشور ناز بر دست</p></div>
<div class="m2"><p>بنهاده دست بردست بنشسته روی با رو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با هم ز لعل گویا گویان به شور و غوغا</p></div>
<div class="m2"><p>صحبت بکام آقا عصرت بخیرمسیو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناگه شد آشکارا مه پیکری دل‌ آرا</p></div>
<div class="m2"><p>در من نماند یارا پیش جمال یارو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرجان اشک سفتم راهش به مژه رفتم</p></div>
<div class="m2"><p>رفتم فراز وگفتم دیوانه‌وار، یاهو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت این روش نیاید برگرد کاین نشاید</p></div>
<div class="m2"><p>درویش را نباید پیش ملک هیاهو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم رخ نکویت بازم کشید سویت</p></div>
<div class="m2"><p>شد ز آفتاب روبت این ذره در تکاپو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرسو که آفتابی‌ است ذرات را شتابی است</p></div>
<div class="m2"><p>مقهور احتسابی‌است این کارگاه نه تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرچ آن که در جهانند عشاق مهربانند</p></div>
<div class="m2"><p>زی نیکویی دوانند تا خود شوند نیکو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هستی به چرب‌دستی در حالتست و مستی</p></div>
<div class="m2"><p>عشقست کنه هستی حسن است غایت او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زان حسن نغز و والاکرده سرایت اینجا</p></div>
<div class="m2"><p>جزیی به کل اشیاء با صدهزار آهو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخشی به زلف سنبل شطری به صفحهٔ گل</p></div>
<div class="m2"><p>لختی به نای بلبل برخی به تاج پوپو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در کوه و دشت و کهسار اندر میان صد خار</p></div>
<div class="m2"><p>هرسو گلی است ناچار افتد نظر بدان سو</p></div></div>