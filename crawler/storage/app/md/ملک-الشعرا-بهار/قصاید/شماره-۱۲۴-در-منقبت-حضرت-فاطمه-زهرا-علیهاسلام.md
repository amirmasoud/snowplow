---
title: >-
    شمارهٔ ۱۲۴ - در منقبت حضرت فاطمه‌زهرا علیهاسلام
---
# شمارهٔ ۱۲۴ - در منقبت حضرت فاطمه‌زهرا علیهاسلام

<div class="b" id="bn1"><div class="m1"><p>ای زده زنار بر، ز مشک به رخسار</p></div>
<div class="m2"><p>جز تو که بر مه ز مشگ برزده زنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف نگونسارکرده‌ای و ندانی</p></div>
<div class="m2"><p>کو دل خلقی ز خویش کرده نگون‌سار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی تو تابنده ماه بر زبر سرو</p></div>
<div class="m2"><p>موی تو تابیده مشگ از برگلنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم تو ترکی وکشوربش مسخر</p></div>
<div class="m2"><p>زلف تو دامی و عالمیش گرفتار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخت به پایان کار خویش بنالد</p></div>
<div class="m2"><p>آن که بر آن زلفش اوفتاده سر ویار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ریجان داری دمیده برگل نسرین</p></div>
<div class="m2"><p>مرجان داری نهاده بر در شهوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفت جانی از آن دو غمزهٔ دلدوز</p></div>
<div class="m2"><p>فتنهٔ شهری ازآن دو طرهٔ طرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتنه شدستم به لاله و سمن از آنک</p></div>
<div class="m2"><p>چهر توباغی است لاله زار وسمن زار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لعل شکر بار داری و نه بدیع است</p></div>
<div class="m2"><p>گرچه نماید بدیع لعل شکربار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان لب شیرین تو بدیع نماید</p></div>
<div class="m2"><p>این همه ناخوش کلام و تلخی گفتار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ختم بود بر تو دلربایی‌، چونانک</p></div>
<div class="m2"><p>نیکی و پاکی بدخت احمد مختار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهرا آن اختر سپهر رسالت</p></div>
<div class="m2"><p>کاو را فرمانبرند ثابت و سیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فاطمه‌، فرخنده مام یازده سرور</p></div>
<div class="m2"><p>آن بدوگیتی پدرش سید و سالار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پرده‌نشین حریم احمد مرسل</p></div>
<div class="m2"><p>صدر گزین بساط ایزد دادار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عرفان عقد است و اوست واسطهٔ عقد</p></div>
<div class="m2"><p>ایمان پرگار و اوست نقطهٔ پرگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر همه اسرار حق ضمیرش آگاه</p></div>
<div class="m2"><p>عنوان از نام او به نامهٔ اسرار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از پی تعظیم نام نامی زهراست</p></div>
<div class="m2"><p>این که خمیده است پشت گنبد دوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر فلک ایزدی است نجمی روشن</p></div>
<div class="m2"><p>در چمن احمدیست نخلی پربار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بار ولایش به دوش گیر و میندیش</p></div>
<div class="m2"><p>ای شده دوش تو از گناه گرانبار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قدر وی از جمله کاینات فزونست</p></div>
<div class="m2"><p>نی نی‌، کاو راست زبن فزونتر مقدار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چندش مقدار باید آنکه جهانش</p></div>
<div class="m2"><p>چون رهیان ایستاده فرمانبردار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>راستی ار بنگری جز این گهر پاک</p></div>
<div class="m2"><p>از دو جهانش غرض نبود جهاندار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عصمت‌، چرخست و اوست‌اختر روشن</p></div>
<div class="m2"><p>عفت‌، بحر است و اوست گوهرشهوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آدم و حوا دو بنده‌ایش به درک‌ه</p></div>
<div class="m2"><p>مریم و عیسی دو چاکریش به دربار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کوس کمالش گذشته از همه گیتی</p></div>
<div class="m2"><p>صیت جلالش رسیده در همه اقطار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فرو شکوه و جلال و حشمت اورا</p></div>
<div class="m2"><p>گر بندانی به‌بین به نامه و اخبار</p></div></div>