---
title: >-
    شمارهٔ ۱۵۲ - پایتخت گل
---
# شمارهٔ ۱۵۲ - پایتخت گل

<div class="b" id="bn1"><div class="m1"><p>در پایتخت ما بگشادند بخت گِل</p></div>
<div class="m2"><p>شد پایتخت ما به صفت پای تخت گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشگلتر از شوارع ری نیست کاندروست</p></div>
<div class="m2"><p>صدگونه شکل هندسی از لخت لخت گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر گه ستور گام نهد از پی عبور</p></div>
<div class="m2"><p>بر گرد گام‌هاش بروید درخت گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تختی از بلور نهی برکنار راه</p></div>
<div class="m2"><p>در نیم لحظه‌اش نشناسی ز تخت گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بخت گل گره خورد از سعی نیم‌شب</p></div>
<div class="m2"><p>عمال نیمروز گشایند بخت گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک رخت پاک باز نماند به شهر ری</p></div>
<div class="m2"><p>گر آفتاب و باد نه‌بندند رخت گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر قصه موجز آمد عیبم مکن ازآنک</p></div>
<div class="m2"><p>سخت است رد شدن ز قوافی سخت گل</p></div></div>