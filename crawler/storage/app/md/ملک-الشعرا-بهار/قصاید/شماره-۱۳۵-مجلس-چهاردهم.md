---
title: >-
    شمارهٔ ۱۳۵ - مجلس چهاردهم
---
# شمارهٔ ۱۳۵ - مجلس چهاردهم

<div class="b" id="bn1"><div class="m1"><p>به بهارستان افتاد مرا دوش عبور</p></div>
<div class="m2"><p>جنتی دیدم بی‌حور و سراپای قصور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حوریان کرده رخ از فترت ایام دژم</p></div>
<div class="m2"><p>قصرها یافته از فرقت احباب فتور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سربسر یافته تبدیل به آیات عذاب</p></div>
<div class="m2"><p>آن کجا بوده سراپای پر از آیت نور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساحتی کایتی از روز سعادت بودی</p></div>
<div class="m2"><p>گشته تاربک‌تر از تیره‌شبان دیجور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیر هرگلبن او جمع‌، هزاران عقرب</p></div>
<div class="m2"><p>دور هر نوگل او گرد، هزاران زنبور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبلش نوحه گر از فرقت مردان شریف</p></div>
<div class="m2"><p>قمریش مویه گر از مرگ وکیلان غیور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آید آواز سلیمان ‌ ولی از ملک عدم</p></div>
<div class="m2"><p>می‌رسد بانگ مدرس ولی از عالم گور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فاخته کوکو گویان که کجا رفت بهار</p></div>
<div class="m2"><p>ورشان‌ مویان مویان که کجا شد تیمور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر سحرگاه بروبد ره و بیراه‌، نسیم</p></div>
<div class="m2"><p>به امیدی که کند مؤتمن‌الملک عبور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جای کیخسرو بگرفته فلان گبر، به زر</p></div>
<div class="m2"><p>جای مستوفی‌ بنشسته فلان رند، به زور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بددلی جای دلیری و طمع جای گذشت</p></div>
<div class="m2"><p>سفلگی جای جوانمردی و غم جای سرور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه پستی و دنائت همه نادانی و جهل</p></div>
<div class="m2"><p>همه تزویر و تقلب همه تقصیر و قصور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روزها پرسه‌زنان در طلب روزی و شب</p></div>
<div class="m2"><p>دست بر گنجفه و گوش به تار و طنبور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه با اجنبیان یار و زکشور بیزار</p></div>
<div class="m2"><p>همه با سید ضیا جور و به ملت ناجور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بجز از نفع ندارند ز هستی مقصود</p></div>
<div class="m2"><p>بجز از پول ندارند به گیتی منظور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز دو من قند و شکر لذت ایشان حاصل</p></div>
<div class="m2"><p>به دو تا چرخ رزین همت ایشان مقصور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>راست چون قحط و غلا در دل مردم مغضوب</p></div>
<div class="m2"><p>همچو طاعون و وبا در بر ملت منفور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه مخلوق سهیلی ز چه‌؟ از دزدی رای</p></div>
<div class="m2"><p>همه مطرود خلایق ز چه‌؟ از نقص شعور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شهر مخروبه و مخروبهٔ ایشان آباد</p></div>
<div class="m2"><p>ملک وبرانه و وبرانهٔ ایشان معمور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باد افکنده به بینی ز چه‌؟ از غایت عجب</p></div>
<div class="m2"><p>زنخ افشرده به غبغب ز چه‌؟ از فرط غرور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یاوهٔ تیه ضلالند و ز غفلت شمرند</p></div>
<div class="m2"><p>خویشرا موسی و این کاخ‌، تجلی گه طور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه را گردن فربی همه را گنده شکم</p></div>
<div class="m2"><p>این یکی مستحق چاقو و آن یک ساطور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرقه‌ای چون امل خویش طویلند و دراز</p></div>
<div class="m2"><p>جرگه‌ای چون طمع خق‌ بش کلفتند و قطور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وطن از پنجهٔ یک‌... اگر جست‌، چه شد؟</p></div>
<div class="m2"><p>که درافتاد به چنگال گروهی مزدور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در بر شاه و رعیت همه کافر نعمت</p></div>
<div class="m2"><p>پیش سرنیزهٔ دشمن همگی عبد شکور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر یکی گوید با خویش که‌، گر تنها من</p></div>
<div class="m2"><p>کیسه پر سازم از این معرکه باشم معذور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ازقضا جمله وکیلان همه این می‌گویند</p></div>
<div class="m2"><p>خاصه آنان که بر این قوم رؤسند و صدور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لاجرم جمله دوانند پی پختن نان</p></div>
<div class="m2"><p>چشم‌ها بسته و بگشوده دهان‌ها چوتنور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حیرتم من که چرا گشت سهیلی پامال</p></div>
<div class="m2"><p>زان که در پختن نان بود به‌غایت مشهور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مجلس چاردهم مجلس نان پختن بود</p></div>
<div class="m2"><p>حیف شد سعی سهیلی که نیامد مشکور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مثل است این که چهی گر به رهی حفر شود</p></div>
<div class="m2"><p>زودتر از همه حاضر قند اندر محفور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آه ازبن مجلس و دولت که توگویی از غیب</p></div>
<div class="m2"><p>همه هستند به وبرانی کشور مامور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به خدایی که بود هستی مطلق کاین ملک</p></div>
<div class="m2"><p>با دوتن مرد خردمند شود دار سرور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لیکن افسوس که این جمع منافق نهلند</p></div>
<div class="m2"><p>که کند صورتی از پرده اصلاح ظهور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه مرعوب اجانب همه مغلوب طمع</p></div>
<div class="m2"><p>همه دلال اعادی همه حمال شرور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کار بیرون شده از چاره ندانم بالله</p></div>
<div class="m2"><p>تا چه حد مردم این ملک حلیمند وصبور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>این وکیلان که به فرمان ... بودند</p></div>
<div class="m2"><p>همه ازعهد ششم جالس این مجلس سور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اینک از غفلت ما، ماه شب چارده‌اند</p></div>
<div class="m2"><p>تاکی افتد به محاق این مه منحوس شرور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این قصیده اگر از ری به خراسان افتد</p></div>
<div class="m2"><p>اوستادان به رهی طعنه زنند از ره دور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آری از ری به خراسان نبرد زبرک شعر</p></div>
<div class="m2"><p>راست چون زیره به کرمان و به تبریز انگور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آن خراسان که درو بوده صبوری و حبیب </p></div>
<div class="m2"><p>این‌یک از پشت شهید آن‌دگر از نسل صبور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن خراسان که درو بوده ادیب‌الادبا</p></div>
<div class="m2"><p>ثانی اثنین رضی‌الدین در نیشابور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ای خراسان تو به هر فترت و هر حادثه‌ای</p></div>
<div class="m2"><p>سپر ایران بودی به سنین و به شهور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جیش یونان را راندی توبه تیغ از ایران</p></div>
<div class="m2"><p>خیل مروان را کردی تو به مردی مقهور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سربداران دلیر تو از ایران کندند</p></div>
<div class="m2"><p>ربشهٔ دولت منحوس طغاخان تیمور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آل‌طاهر ز تو دادند به بغداد جواب</p></div>
<div class="m2"><p>آل لیث از تو گرفتند به شاهی منشور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آل‌سامان ز تو و دولت غزنی ز تو بود</p></div>
<div class="m2"><p>وز تو شهنامه بر اوراق ابد شد مسطور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نادر از نادره اقلیم تو برخاست که کرد</p></div>
<div class="m2"><p>خاک ایران را خالی ز سه خصم مغرور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ای خراسان ز چه بنشسته و ساکت نگری</p></div>
<div class="m2"><p>تا به نام تو دوانند به هر گوشه ستور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زبن وکیلان که تو منشور وکالت دادی</p></div>
<div class="m2"><p>نام دیرین تو شد پست الی یوم نشور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به جز از یک دو سه تن قادر و عاجز، باقی</p></div>
<div class="m2"><p>زان کسانند که شاید سرشان از تن دور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همه بی‌فضل و فضیلت همه بی‌علم و سواد</p></div>
<div class="m2"><p>همه قلاش وبخوبر، همه الدنگ وشرور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همگی از اثر بی‌رگی و بی‌حسی</p></div>
<div class="m2"><p>برده در عهد رضاشاه حظوظ موفور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بهر بی‌دردی و دلسردی و بی‌حسی خلق</p></div>
<div class="m2"><p>هست هریک را خاصیت صد من کافور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عجبم تا ز چه این سرد مزاجان خنک</p></div>
<div class="m2"><p>گشته مبعوث چنان قوم غیور محرور</p></div></div>