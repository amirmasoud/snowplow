---
title: >-
    شمارهٔ ۲۳۱ - گرسنه
---
# شمارهٔ ۲۳۱ - گرسنه

<div class="b" id="bn1"><div class="m1"><p>شاها تا کی بود بهار گرسنه</p></div>
<div class="m2"><p>خائن سیر و درستکار گرسنه‌؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرمگس و عنکبوت و پشه و زنبور</p></div>
<div class="m2"><p>آن همه سیرند و نوبهار گرسنه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه کند سفلگی شعار، بود سیر</p></div>
<div class="m2"><p>وانکه کند راستی شعار، گرسنه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سکهٔ قلب خراب سیر ولیکن</p></div>
<div class="m2"><p>شمش زر کامل‌العیار گرسنه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دشتی و زوار و شیروانی سیرند</p></div>
<div class="m2"><p>لیک تقی‌زاده و بهار گرسنه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوشش و ایران غنی و سیر ولیکن</p></div>
<div class="m2"><p>صد چو خلیلی به هر کنار گرسنه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک نفر از پرخوری کند قی و پیشش</p></div>
<div class="m2"><p>ضعف نموده است صد هزار گرسنه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دزد وطن هست سیر و آن که همه عمر</p></div>
<div class="m2"><p>بهر وطن بوده جان نثار گرسنه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن که بود چاپلوس و جاهل و بی‌دین</p></div>
<div class="m2"><p>هیچ نماند به روزگار گرسنه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وان که تملق نگفت و در همه حالی</p></div>
<div class="m2"><p>مسلک خود کرد آشکار، گرسنه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دشمن ایران به یک قرار بود سیر</p></div>
<div class="m2"><p>ملت ایران به یک قرار گرسنه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وای به باغی که جغد و زاغ در آن سیر</p></div>
<div class="m2"><p>لیک بود قمری و هزار گرسنه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیران مستوجب عنایت شاهند</p></div>
<div class="m2"><p>لیکن مستوجب فشار، گرسنه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هیچ ندیدم خدای را که گذارد</p></div>
<div class="m2"><p>عبد ضعیف گناهکار گرسنه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرسنگی لازم است لیک روا نیست</p></div>
<div class="m2"><p>بیشتر از حد انتظارگرسنه</p></div></div>