---
title: >-
    شمارهٔ ۱۴۱ - تغزل
---
# شمارهٔ ۱۴۱ - تغزل

<div class="b" id="bn1"><div class="m1"><p>بربوده دلم چشم پر فنش</p></div>
<div class="m2"><p>وان عارض چون ماه روشنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسرینش رخ و سوسنش دو زلف</p></div>
<div class="m2"><p>من بندهٔ نسرین و سوسنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقی است دگرگونه با ویم</p></div>
<div class="m2"><p>مهریست دگرگونه با منش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاطر شده مفتون عارضش</p></div>
<div class="m2"><p>دیده شده حیران دیدنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مانا ملک‌العرش از نخست</p></div>
<div class="m2"><p>آمیخته با نیکوئی تنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حورای جنان بوده مادرش</p></div>
<div class="m2"><p>فردوس برین بوده مسکنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امروز بدیدم به رهگذار</p></div>
<div class="m2"><p>خون دل خلقی بگردنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برخاسته دامن کشان به راه</p></div>
<div class="m2"><p>و آویخته قومی به دامنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افتاده بسی جان و دل به خاک</p></div>
<div class="m2"><p>پیش مژهٔ ناوک افکنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز انبوه دل از دست رفتگان</p></div>
<div class="m2"><p>چون کعبه شده کوی و برزنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من نیز فرا رفته تا مگر</p></div>
<div class="m2"><p>یک خوشه ربایم ز خرمنش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دیده فشاندم بسی سرشک</p></div>
<div class="m2"><p>پیش دل چون روی و آهنش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگذشت و به من بر نظر نکرد</p></div>
<div class="m2"><p>تا بود چنین بود دیدنش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شیون کند از دست او دلم</p></div>
<div class="m2"><p>با آنکه نه نیکوست شیونش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شیون چه کند بنده‌ای که هست</p></div>
<div class="m2"><p>درگاه خداوند مأمنش</p></div></div>