---
title: >-
    شمارهٔ ۸۹ - صیقل‌ عشق
---
# شمارهٔ ۸۹ - صیقل‌ عشق

<div class="b" id="bn1"><div class="m1"><p>گلعذاران جهان بسیارند</p></div>
<div class="m2"><p>لیک پیش گل رویت خارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل نگهدار که خوبان دل را</p></div>
<div class="m2"><p>چون گرفتند نگه می‌دارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مده آزار دل من که بتان</p></div>
<div class="m2"><p>دل عشاق نمی‌آزارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر شنیدی که نکویان جهان</p></div>
<div class="m2"><p>بی‌وفایند و شقاوت کارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرو از راه که آن بی‌ادبان</p></div>
<div class="m2"><p>همه بازاری و سردم دارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو نجیبی و نکویان نجیب</p></div>
<div class="m2"><p>همه با رحم و نکوکردارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لاله رویند ولیکن هرگز</p></div>
<div class="m2"><p>داغ محنت به دلی نگذارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه‌خوش‌صورت و خوشن‌برخوردند</p></div>
<div class="m2"><p>همه خوش سیرت و خوش رفتارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهر عشاق حقیقی نورند</p></div>
<div class="m2"><p>بهر عشاق دروغی نارند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نرمند از ادبا و احرار</p></div>
<div class="m2"><p>یار اهل ادب و احرارند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دامن با ادبان را گنجند</p></div>
<div class="m2"><p>گردن بی‌ادبان را مارند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه عاشق طلب و دلجویند</p></div>
<div class="m2"><p>همه شکر لب و شیرین کارند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهرشان عاشقی ار یافت نشد</p></div>
<div class="m2"><p>همت اندر طلبش بگمارند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاشق‌ از آهن و از چوب کنند</p></div>
<div class="m2"><p>که هم آهنگر و هم نجارند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جمله هم عاشق و هم معشوقند</p></div>
<div class="m2"><p>جمله هم ثابت و هم سیارند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دعوی بلهوسان را در عشق</p></div>
<div class="m2"><p>نپذیرند که بس عیارند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قدر صافی گهران را از دور</p></div>
<div class="m2"><p>بشناسند که بس هشیارند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نستانند دل از یکتن بیش</p></div>
<div class="m2"><p>نیز دل جز به یکی نسپارند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>امتحان‌های دلاوبز کنند</p></div>
<div class="m2"><p>تا به عشق کسی ایمان آرند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون مسلم شد و تردید نماند</p></div>
<div class="m2"><p>شرم و حشمت ز میان بردارند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در برش ساعتکی بنشینند</p></div>
<div class="m2"><p>همرهش بادگکی بگمارند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گاه گاهی ز پی صیقل عشق</p></div>
<div class="m2"><p>بوسه‌ای چند بر او بشمارند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بوسه در عشق مباح است آری</p></div>
<div class="m2"><p>بوسه را صیقل عشق انگارند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر چه عشاق نخسبند به شب</p></div>
<div class="m2"><p>مهوشان نیز براین هنجارند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دلبرانی که خداوند دلند</p></div>
<div class="m2"><p>در غم عاشق خود بیدارند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاهدی کاو غم عاشق نخورند</p></div>
<div class="m2"><p>مردمان جانورش پندارند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عشق معشوق نهانست ولیک</p></div>
<div class="m2"><p>حکما واقف از این اسرارند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شرط انیت خوبان اینست</p></div>
<div class="m2"><p>غیر از این مابقی از اغیارند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شاهدانی که چنانند و چنین</p></div>
<div class="m2"><p>مردم چشم اولی الابصارند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عشق در ساحت جان گلزاریست</p></div>
<div class="m2"><p>خوبرویان گل این گلزارند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نتوان داشت امید یاری</p></div>
<div class="m2"><p>زان رفیقان‌، که به شهوت یارند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مردمی زین شهوانی عشاق</p></div>
<div class="m2"><p>نتوان خواست‌، که مردم خوارند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دلشان ازگهر عشق تهی است</p></div>
<div class="m2"><p>همه از شهوت و حرص انبارند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کاهل و بی‌مزه و بی‌ادبند</p></div>
<div class="m2"><p>لوس و بی‌معنی و چربک سارند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به حقیقت همه گولند و سفیه</p></div>
<div class="m2"><p>لیک در گول‌زدن مکارند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نیز آن فرقه که دورند از عشق</p></div>
<div class="m2"><p>نقش حمام و گچ دیوارند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گلرخانی که نفورند از عشق</p></div>
<div class="m2"><p>گویی از خلقت خود بیزارند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>قتل عاشق برشان هست مباح</p></div>
<div class="m2"><p>این بتان افعی جان اوبارند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون به افراط شتابند از جهل</p></div>
<div class="m2"><p>شمع هر جمع و گل هر خارند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون به تفریط گرایند از عجب</p></div>
<div class="m2"><p>عشق را معصیتی انگارند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هست نزدیک خرد هر دو گزاف</p></div>
<div class="m2"><p>کاین دو در وزن به یک مقدارند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>روش مردم نادان است این</p></div>
<div class="m2"><p>که نه از فلسفه برخوردارند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>عقلا معتدلند اندر طبع</p></div>
<div class="m2"><p>وز گزاف جهلا فرارند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اولین شرط نجابت عقل است</p></div>
<div class="m2"><p>عقلا بیشتری ز اخیارند</p></div></div>