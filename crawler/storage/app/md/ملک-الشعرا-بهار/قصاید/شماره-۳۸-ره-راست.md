---
title: >-
    شمارهٔ ۳۸ - ره راست
---
# شمارهٔ ۳۸ - ره راست

<div class="b" id="bn1"><div class="m1"><p>تا شدم خویگر به رفتن راست</p></div>
<div class="m2"><p>چرخ کجرو به کشتنم برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راست نتوان سوی بلندی رفت</p></div>
<div class="m2"><p>راستی مانع ترقی ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوهرو بین که پشت خم دارد</p></div>
<div class="m2"><p>گه ز چپ می‌رود گهی از راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذروهٔ عز دنیوی کوهی است</p></div>
<div class="m2"><p>که همه نعمت اندر آن بالاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاد راهش دروغ و گربزی است</p></div>
<div class="m2"><p>نردبانش فریب و مکر و دهاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باید ار قصد برشدن داری</p></div>
<div class="m2"><p>هر زمان اوفتاد و برپا خاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست فرقی میان دشمن و دوست</p></div>
<div class="m2"><p>کاندر آن ره خروش وانفساست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر آن ره دو تن ز پهلوی هم</p></div>
<div class="m2"><p>نگذرد بس که راه کم‌پهناست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کس در این راه پر خطر از کس</p></div>
<div class="m2"><p>دستگیری نمی کند که خطاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوستان پای دوستان گیرند</p></div>
<div class="m2"><p>از پی پاس جان خویش و رواست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سنگ‌ها پیش پایت اندازد</p></div>
<div class="m2"><p>آنکه بالاتر از تو ره‌پیماست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر قدم زین مشاجرات مخوف</p></div>
<div class="m2"><p>طرفه جنگ و کشاکشی برپاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکه برگشت یا که عجز آورد</p></div>
<div class="m2"><p>در تک درهٔ عمیقش جاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این بود حال کوه‌پیمایان</p></div>
<div class="m2"><p>طرفه کوهی که مقصد عظماست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پا پر از آبله است و خون‌، زیراک</p></div>
<div class="m2"><p>ساق در خار و کام بر خاراست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زبر و بالای این گریوه و کوه</p></div>
<div class="m2"><p>از انین و نفیر، پر ز صداست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون به بالا رسند با این رنج</p></div>
<div class="m2"><p>آن مکان تازه اول دعواست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کان مکان نیست جای یک تن بیش</p></div>
<div class="m2"><p>وز همه سو نشیب هول و بلاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کسی آنجای را به چنگ آرد</p></div>
<div class="m2"><p>که به اسباب و بخت‌، کامرواست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا یکی با غنا شود مقرون</p></div>
<div class="m2"><p>صدهزار آدمی قرین عناست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جایگاهی خوشست لیک دربغ</p></div>
<div class="m2"><p>که بدین جا هجوم این غوغاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه آنجای را طمع دارند</p></div>
<div class="m2"><p>مقصد جملگی همان یکجاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرکه او بر در نیاز نشست</p></div>
<div class="m2"><p>از سر امن و عافیت برخاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به حقیقت غنی، کسی باشد</p></div>
<div class="m2"><p>کش ازاین رفت‌وآمد استغناست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جاه حاصل شده ز خون جگر</p></div>
<div class="m2"><p>بازی کودکانهٔ سفهاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دولتی پر ز بیم و باک و هلاک</p></div>
<div class="m2"><p>نیست دولت که کام اژدرهاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کوه‌پیما نه‌ایم و خرسندیم</p></div>
<div class="m2"><p>گرچه رهوار ما جهان پیماست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ما جهان را به راستی سپریم</p></div>
<div class="m2"><p>کس ندیدم که گم شد از ره راست</p></div></div>