---
title: >-
    شمارهٔ ۲۰۲ - صفاهان
---
# شمارهٔ ۲۰۲ - صفاهان

<div class="b" id="bn1"><div class="m1"><p>ای رخ میمونت آفتاب صفاهان</p></div>
<div class="m2"><p>وی به وجود تو آب و تاب صفاهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز شد از قید ظلم‌، گردن مظلوم</p></div>
<div class="m2"><p>تا تو شدی مالک الرقاب صفاهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرد صفاهان ز عدل پرسش و حق داد</p></div>
<div class="m2"><p>ز آیه لاتقنطوا، جواب صفاهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دید صفاهان همی به طالع بیدار</p></div>
<div class="m2"><p>آنچه نیامد همی به خواب صفاهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقدم آبادی آفرین «‌نصیری‌»</p></div>
<div class="m2"><p>گشت نصیر دل خراب صفاهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همت سردار جنگ و غیرت احرار</p></div>
<div class="m2"><p>بر رخ اشرار بست باب صفاهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر رجب دزد، راه بسته و بگشاد</p></div>
<div class="m2"><p>راه ذهاب و ره ایاب صفاهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سر جعفر قلی کشید سپاهی</p></div>
<div class="m2"><p>کشن و غریونده چون سحاب صفاهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لشکر دزدان غمی شدند و بجستند</p></div>
<div class="m2"><p>چون ز نسیم خزان‌، ذباب صفاهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از اثر خون خاک خوردهٔ اشرار</p></div>
<div class="m2"><p>رنگ طبرخون گرفت، آفتاب صفاهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبن سپس از بسکه خون دزد بریزد</p></div>
<div class="m2"><p>نکهت خون آید از گلاب صفاهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وز اثر این سیاست‌، از پس زردی</p></div>
<div class="m2"><p>سرخ شود رنگ شیخ و شاب صفاهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای هنری میر بختیار، که شد یار</p></div>
<div class="m2"><p>فر تو با بخت کامیاب صفاهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مردم ایران ز شرق و غرب ببردند</p></div>
<div class="m2"><p>رشگ‌، بر این حسن انتخاب صفاهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رو که خوش از عهدهٔ حساب بر آیی</p></div>
<div class="m2"><p>چون ز تو خواهد خدا حساب صفاهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رو که اثرهای مستطاب نماید</p></div>
<div class="m2"><p>در تو دعاهای مستجاب صفاهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نعمت دنیات هست‌، کوش که یابی</p></div>
<div class="m2"><p>عاقبتی نیک از احتساب صفاهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عاقبت نیک‌، غیر نام نکو چیست‌؟</p></div>
<div class="m2"><p>نام نکو جوی از جناب صفاهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا به تو منسوب گشت‌، فخر نمایند</p></div>
<div class="m2"><p>اهل صفاهان ز انتساب صفاهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روی بهار از فراق روی تو گشته است</p></div>
<div class="m2"><p>زردتر از آبی خوشاب صفاهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لیک به حکم حکیم و لطف تو شاید</p></div>
<div class="m2"><p>سرخ شود رویش از شراب صفاهان</p></div></div>