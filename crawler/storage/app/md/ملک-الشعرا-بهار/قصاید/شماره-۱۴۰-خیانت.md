---
title: >-
    شمارهٔ ۱۴۰ - خیانت
---
# شمارهٔ ۱۴۰ - خیانت

<div class="b" id="bn1"><div class="m1"><p>آن را که نگون است رایتش</p></div>
<div class="m2"><p>من هیچ نخواهم حمایتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آن دیو که این کار خواسته است</p></div>
<div class="m2"><p>دیوانه بخوانند، ملتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این کشور تحت‌الحمایه نیست</p></div>
<div class="m2"><p>هم نیز برنجد زصحبتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملکی که ز جیحون و هیرمند</p></div>
<div class="m2"><p>تا دجله برآید مساحتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کس بنخواهد حمایتی</p></div>
<div class="m2"><p>وین گفته نگنجد به غیرتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن کس که به‌ ما داده یادداشت</p></div>
<div class="m2"><p>وان صاحب او، چیست نیتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌جنگ بخواهد جهان گرفت؟</p></div>
<div class="m2"><p>صعبا و غریبا حکایتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امروز که هر ملت نژند</p></div>
<div class="m2"><p>در سایهٔ تیغ است حرکتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌قیمت خون‌بندگی خطاست</p></div>
<div class="m2"><p>وین بنده گرانست قیمتش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویند سپهدار داده خط</p></div>
<div class="m2"><p>لعنت به خطوط پر مخافتش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر داده خطی این‌چنین خطاست</p></div>
<div class="m2"><p>کاین ملک بری بوده ذمتش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی‌رأی شه و رأی مجلسین</p></div>
<div class="m2"><p>ملت نشناسد به صحتش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لعنت به وزیری چنین که هست</p></div>
<div class="m2"><p>بر خیر بداندیش‌، همتش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با آن که فزون دارد احترام</p></div>
<div class="m2"><p>با آن که فزونست ثروتش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قوم و وطن خود کند ذلیل</p></div>
<div class="m2"><p>وانگاه بخندد به ذلتش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بخشد وطن خود به رایگان</p></div>
<div class="m2"><p>وانگاه گریزد ز خشیتش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زودا و قریباکه در رسد</p></div>
<div class="m2"><p>خائن به سزای خیانتش</p></div></div>