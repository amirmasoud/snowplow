---
title: >-
    شمارهٔ ۲۲۲ - جغد جنگ
---
# شمارهٔ ۲۲۲ - جغد جنگ

<div class="b" id="bn1"><div class="m1"><p>فغان ز جغد جنگ ومرغوای او</p></div>
<div class="m2"><p>که تا ابد بریده باد نای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بریده باد نای او و تا ابد</p></div>
<div class="m2"><p>گسسته وشکسته پر وپای او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز من بریده یار آشنای من</p></div>
<div class="m2"><p>کزو بریده باد آشنای او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه باشد از بلای جنگ صعب‌تر</p></div>
<div class="m2"><p>که کس امان نیابد از بلای او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شراب او ز خون مرد رنجبر</p></div>
<div class="m2"><p>وز استخوان کارگر غذای او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی‌زند صلای‌مرک‌ونیست کس</p></div>
<div class="m2"><p>که جان برد ز صدمت صلای او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی‌دهد ندای خوف و می‌رسد</p></div>
<div class="m2"><p>به هر دلی مهابت ندای او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی تند چو دیوپای در جهان</p></div>
<div class="m2"><p>به هر طرف کشیده تارهای او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خیل مور، گرد پاره شکر</p></div>
<div class="m2"><p>فتد به جان آدمی عنای او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هر زمین که باد جنگ بر وزد</p></div>
<div class="m2"><p>به حلق‌هاگره شود هوای او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آن زمان که نای حرب دردمد</p></div>
<div class="m2"><p>زمانه بی‌نوا شود ز نای او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به گوش‌ها خروش تندر اوفتد</p></div>
<div class="m2"><p>ز بانگ توپ و غرش و هرای او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان شود چو آسیا و دم به دم</p></div>
<div class="m2"><p>به خون تازه گردد آسیای او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رونده تانک‌، همچو کوه آتشین</p></div>
<div class="m2"><p>هزار گوش کر کند صدای او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی خزد چو اژدها و درچکد</p></div>
<div class="m2"><p>به هر دلی شرنگ جانگزای او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو پر بگسترد عقاب آهنین</p></div>
<div class="m2"><p>شکار اوست شهر و روستای او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هزار بیضه هر دمی فرو هلد</p></div>
<div class="m2"><p>اجل دوان چو جوجه از قفای او</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کلنگ سان دژ پرنده بنگری</p></div>
<div class="m2"><p>به هندسی صفوف خوشنمای او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو پاره پاره ابرکافکند همی</p></div>
<div class="m2"><p>تگرگ مرگ، ابر مرگزای او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به هرکرانه دستگاهی آتشین</p></div>
<div class="m2"><p>جحیمی آفریده در فضای او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز دود و آتش و حریق و زلزله</p></div>
<div class="m2"><p>ز اشک وآه و بانگ های های او</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به رزمگه «‌خدای جنگ» بگذرد</p></div>
<div class="m2"><p>چو چشم شیر، لعلگون قبای او</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>امل‌، جهان ز قعقع سلاح وی</p></div>
<div class="m2"><p>اجل‌، دوان به سایهٔ لوای او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نهان بگرد، مغفر و کلاه وی</p></div>
<div class="m2"><p>به خون کشیده موزه و ردای او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به هر زمین که بگذرد بگسترد</p></div>
<div class="m2"><p>نهیب مرگ و درد، ویل و وای او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دو چشم‌ و کوش‌ دهرکور و کر شود</p></div>
<div class="m2"><p>چو برشود نفیر کرنای او</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهانخوران گنجبر به جنگ بر</p></div>
<div class="m2"><p>مسلطند و رنج و ابتلای او</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بقای غول جنگ هست درد ما</p></div>
<div class="m2"><p>فنای جنگبارگان دوای او </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زغول جنگ وجنگبارگی بتر</p></div>
<div class="m2"><p>سرشت جنگباره و بقای او</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>الا حذر ز جنگ و جنگبارگی</p></div>
<div class="m2"><p>که آهریمن است مقتدای او</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نبینی آنکه ساختند از اتم</p></div>
<div class="m2"><p>تمامتر سلیحی اذکیای او؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نهیبش ار به کوه خاره بگذرد</p></div>
<div class="m2"><p>شود دوپاره کوه از التقای او</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تف سموم او به دشت و درکند</p></div>
<div class="m2"><p>ز جانور تفیده تاگیای او</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شود چو شهر لوط‌، شهره بقعتی</p></div>
<div class="m2"><p>کز این سلاح داده شد جزای او</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نماند ایچ جانور به جای بر</p></div>
<div class="m2"><p>نه کاخ وکوخ و مردم و سرای او</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به ‌ژاپن ‌اندرون ‌یکی دو بمب از آن</p></div>
<div class="m2"><p>فتاد وگشت باژگون بنای او</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو گفتی آنکه دوزخ اندرو دهان</p></div>
<div class="m2"><p>گشاد و دم برون زد اژدهای او</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سپس به‌دم فروکشید سر بسر</p></div>
<div class="m2"><p>ز خلق‌و وحش‌و طیر و چارپای او</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شد آدمی بسان مرغ بابزن</p></div>
<div class="m2"><p>فرسپ خانه گشت کردنای او</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بود یقین که زی خراب ره برد</p></div>
<div class="m2"><p>کسی که شد غراب رهنمای او</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به خاک مشرق از چه روزنند ره</p></div>
<div class="m2"><p>جهانخوران غرب و اولیای او</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گرفتم آنکه دیگ شد گشاده‌سر</p></div>
<div class="m2"><p>کجاست شرم گربه و حیای او</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کسی که در دلش به جز هوای زر</p></div>
<div class="m2"><p>نیافریده بویه ای خدای او</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>رفاه و ایمنی طمع مدار هان</p></div>
<div class="m2"><p>زکشوری که کشت مبتلای او</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به‌خویشتن‌هوان و خواری افکند</p></div>
<div class="m2"><p>کسی که در دل افکند هوای او</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نهند منت نداده بر سرت</p></div>
<div class="m2"><p>وگر دهند چیست ماجرای او</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بنان ارزنت بساز و کن حذر</p></div>
<div class="m2"><p>زگندم و جو و مس و طلای او</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بسان کَه کِه سوی کَهرُبا رود</p></div>
<div class="m2"><p>رود زر تو سوی کیمیای او</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نه دوستیش خواهم و نه دشمنی</p></div>
<div class="m2"><p>نه ترسم از غرور وکبریای او</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همه فریب و حیلتست و رهزنی</p></div>
<div class="m2"><p>مخور فریب جاه و اعتلای او</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>غنای اوست اشگ چشم رنجبر</p></div>
<div class="m2"><p>مبین به چشم ساده در غنای او</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>عطاش را نخواهم و لقاش را</p></div>
<div class="m2"><p>که شومتر لقایش از عطای او</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>لقای او پلید چون عطای وی</p></div>
<div class="m2"><p>عطای وی کریه چون لقای او</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کجاست روزگار صلح و ایمنی</p></div>
<div class="m2"><p>شکفته مرز و باغ دلگشای او</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کجاست عهد راستی و مردمی</p></div>
<div class="m2"><p>فروغ عشق و تابش ضیای او</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کجاست دور یاری و برابری</p></div>
<div class="m2"><p>حیات جاودانی و صفای او</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فنای‌ جنگ خواهم از خدا که شد</p></div>
<div class="m2"><p>بقای خلق بسته در فنای او</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زهی کبوتر سپید آشتی</p></div>
<div class="m2"><p>که دل برد سرود جانفزای او</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>رسید وقت آنکه جغد جنگ را</p></div>
<div class="m2"><p>جدا کنند سر به پیش پای او</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بهار طبع من شگفته شد، چو من</p></div>
<div class="m2"><p>مدیح صلح گفتم و ثنای او</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>برین چکامه آفرین کند کسی</p></div>
<div class="m2"><p>که پارسی شناسد و بهای او</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدین قصیده برگذشت شعر من</p></div>
<div class="m2"><p>ز بن درید و از اماصحای او</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شد اقتدا به اوستاد دامغان</p></div>
<div class="m2"><p>«‌فغان از این غراب بین و وای او»</p></div></div>