---
title: >-
    شمارهٔ ۱۶۷ - بث الشکوی
---
# شمارهٔ ۱۶۷ - بث الشکوی

<div class="b" id="bn1"><div class="m1"><p>تا بر زبر ری است جولانم</p></div>
<div class="m2"><p>فرسوده و مستمند و نالانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزلست مگر سطور اوراقم</p></div>
<div class="m2"><p>یاوه است مگر دلیل و برهانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا خود مردی ضعیف تدبیرم</p></div>
<div class="m2"><p>یا خود شخصی نحیف ارکانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا همچو گروه سفلگان هر روز</p></div>
<div class="m2"><p>از بهر دونان به کاخ دونانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیمانه کش رواق دستورم‌؟</p></div>
<div class="m2"><p>دریوزه گر سرای سلطانم‌؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینها همه نیست پس چرا در ری</p></div>
<div class="m2"><p>سیلی‌خور هر سفیه و نادانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جرمی است‌ مرا قوی که‌ در این ملک</p></div>
<div class="m2"><p>مردم دگرند و من دگرسانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کید مخنثان‌، نیم ایمن</p></div>
<div class="m2"><p>زیراک مخنثی نمی‌دانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه خیل عوام را سپهدارم</p></div>
<div class="m2"><p>نه خوان خواص را نمکدانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سیرت رادمردمان‌، زین‌روی</p></div>
<div class="m2"><p>در خانه‌‌‌ٔ خویشتن به زندانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک روز کند وزیر تبعیدم</p></div>
<div class="m2"><p>یک روز زند سفیه بهتانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دشنام خورم ز مردم نادان</p></div>
<div class="m2"><p>زیراک هنرور و سخندانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زیرا به سخن یگانهٔ دهرم</p></div>
<div class="m2"><p>زیرا به هنر فرید دورانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زیراک به نقش‌بندی معنی</p></div>
<div class="m2"><p>سیلابهٔ روح بر ورق رانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زیرا پس‌چند قرن چون خورشید</p></div>
<div class="m2"><p>بیرون شده از میان اقرانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زیرا به خطابه و به نظم و نثر</p></div>
<div class="m2"><p>خورشید فروغ‌بخش ایرانم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زیرا به لطایف و شداید نیز</p></div>
<div class="m2"><p>مطبوع رواق و مرد میدانم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اینست گناه من‌، که در هر گام</p></div>
<div class="m2"><p>ناکام چو پور سعد سلمانم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پنهانم از این گروه، خودگویی</p></div>
<div class="m2"><p>من ناصرم و ری است یمکانم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با دزدان چون زیم‌، که‌نه دزدم</p></div>
<div class="m2"><p>باکشخان چون بوم، نه کشخانم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه مرد فریب و سخره و زرقم</p></div>
<div class="m2"><p>نه مرد ریا و کید و دستانم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون آتش‌، روشن است گفتارم</p></div>
<div class="m2"><p>چون آب‌، منزه است دامانم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر فاحشه نیست پایهٔ فضلم</p></div>
<div class="m2"><p>وز مسخره نیست پارهٔ نانم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از مغز سر است توشهٔ جسمم</p></div>
<div class="m2"><p>وز رنج تن است راحت جانم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بس خامه‌ط‌رازی‌، ای عجب گشتست</p></div>
<div class="m2"><p>انگشتان چون سطبر سوهانم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بس راهنوردی‌، ای دریغا هست</p></div>
<div class="m2"><p>دو پاشنه چون دو سخت سندانم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه دیر غنوده‌اند افکارم</p></div>
<div class="m2"><p>نه سیر بخفته‌اند چشمانم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زینگونه گذشت سالیان بر هفت</p></div>
<div class="m2"><p>کاندر تعب است هفت ارکانم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گه خسرو هند سوده چنگالم</p></div>
<div class="m2"><p>گه قیصر روس کنده دندانم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از نقمت دشمنان آزادی</p></div>
<div class="m2"><p>گه در ری و گاه در خراسانم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>و امروز عمید ملک شاهنشاه</p></div>
<div class="m2"><p>بسته است زبان گوهرافشانم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فرخ حسن‌ بن‌ یوسف آنک از قهر</p></div>
<div class="m2"><p>افکنده نگون به جاه کنعانم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا کام معاندان روا سازد</p></div>
<div class="m2"><p>بسپرده به کام گرگ حرمانم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وین رنج عظیم‌تر که در صورت</p></div>
<div class="m2"><p>اندر شمر فلان و بهمانم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ناکرده گنه معاقبم‌، گویی</p></div>
<div class="m2"><p>سبابهٔ مردم پشیمانم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عمری به هوای وصلت قانون</p></div>
<div class="m2"><p>از چرخ برین گذشت افغانم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در عرصهٔ گیر و دار آزادی</p></div>
<div class="m2"><p>فرسود به تن‌، درشت خفتانم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تیغ حدثان گسست پیوندم</p></div>
<div class="m2"><p>پیکان بلا بسفت ستخوانم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گفتم که مگر به نیروی قانون</p></div>
<div class="m2"><p>آزادی را به تخت بنشانم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>و امروز چنان شدم که برکاغذ</p></div>
<div class="m2"><p>آزاد نهاد خامه نتوانم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ای آزادی‌، خجسته آزادی‌!</p></div>
<div class="m2"><p>از وصل تو روی برنگردانم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا آنکه مرا به نزد خود خوانی</p></div>
<div class="m2"><p>یا آنکه‌ ترا به نزد خود خوانم</p></div></div>