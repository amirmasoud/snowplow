---
title: >-
    شمارهٔ ۱۷ - غدیریه
---
# شمارهٔ ۱۷ - غدیریه

<div class="b" id="bn1"><div class="m1"><p>ای که در هر نیکوئی آراسته یزدان تو را</p></div>
<div class="m2"><p>جمله داری خود، چه گویم این تو را یا آن تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده یزدانت همی انباز با حور بهشت</p></div>
<div class="m2"><p>وانچه‌بخشد حور را بخشیده صدچندان تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درکنار خویشتن پرورده رضوانت به ناز</p></div>
<div class="m2"><p>تاکند فرمانروا بر حور و بر غلمان تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف طرار تو زان‌پس حیله‌ها انگیخته است</p></div>
<div class="m2"><p>تا به افسون و حیل دزدیده از رضوان تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نیابد مر تو را بار دگر رضوان خلد</p></div>
<div class="m2"><p>هردم اندر بند و چین خود کند پنهان تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با همه کوشش نیابد مر تو را رضوان و من</p></div>
<div class="m2"><p>یافتم از فر مدح حجهٔ یزدان تو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیر یزدان بوالحسن آنکس چو بنگاری مدیح</p></div>
<div class="m2"><p>نه فلک گردد طراز دفتر و دیوان تو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای مهین سلطان ملک هستی ای کاندر غدیر</p></div>
<div class="m2"><p>کرده حق برهر دوگیتی سید و سلطان تو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مر تو را تشریف امکان داد یزدان از ازل</p></div>
<div class="m2"><p>تاکند زیب و طراز عالم امکان تو را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ذات تو قائم به یزدان‌، ذات ما قائم به تو است</p></div>
<div class="m2"><p>جلوهٔ ذاتند عقل و نفس و جسم و جان تو را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مر مرا باید زبانی دیگر و طبعی دگر</p></div>
<div class="m2"><p>تاشوم چونانکه شایسته است مدحت‌خوان تو را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با زبانی این‌چنین و با بیانی این‌چنین</p></div>
<div class="m2"><p>خودکجا شاید سرودن مدحتی شایان تو را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مدحتی شایان ببایدگفت آنکس راکه او</p></div>
<div class="m2"><p>چون ملک گردن نهد بر حکم و برفرمان تورا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاه رکن‌الدوله کش روز و شبان گویند خلق</p></div>
<div class="m2"><p>کای ملک بادا به گیتی عمر جاویدان تو را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چهره‌ها خرم نمودی چهره خرم‌تر تو را</p></div>
<div class="m2"><p>خانه‌ها آباد کردی خانه آبادان تو را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حیله و تزویر هر نادان نگیرد در ملوک</p></div>
<div class="m2"><p>از چه گیرد حیله و تزویر هر نادان تو را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یاوه و هذیان روا نبود بر دانش پژوه</p></div>
<div class="m2"><p>به که آید ناروا هر یاوه و هذیان تو را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نک زدم از راستی در دامنت دست امید</p></div>
<div class="m2"><p>فرخ آن کز راستی زد دست در دامان تو را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خواهش یزدان پذیر و داد مظلومان بگیر</p></div>
<div class="m2"><p>زانکه بهر داد، داد این برتری یزدان تو را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نک به‌فرمانت چنان گفتم که خودگفتم ز پیش</p></div>
<div class="m2"><p>«‌عید قربان آمد ای جان جهان قربان تو را»</p></div></div>