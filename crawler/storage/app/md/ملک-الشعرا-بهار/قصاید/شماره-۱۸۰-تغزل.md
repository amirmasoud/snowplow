---
title: >-
    شمارهٔ ۱۸۰ - تغزل
---
# شمارهٔ ۱۸۰ - تغزل

<div class="b" id="bn1"><div class="m1"><p>ای حلقهٔ زلف تو پر شکن</p></div>
<div class="m2"><p>وی نرگس مست توصف شکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یک شکن طرهٔ دوتات</p></div>
<div class="m2"><p>بر جان و دل من دو صد شکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای زلف تو سررشتهٔ بلا</p></div>
<div class="m2"><p>وی چشم تو سر منشاء فتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای نور تو را شمس مکتسب</p></div>
<div class="m2"><p>وی لعل‌ تو را شهد مرتهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای چشم تو چون آهوی ختا</p></div>
<div class="m2"><p>وی خال تو چون نافهٔ ختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای جعد تو یک باغ ضیمران</p></div>
<div class="m2"><p>وی چهر تو یک راغ نسترن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه از رخ تو یافته بها</p></div>
<div class="m2"><p>مشک از خط تو یافته ثمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشمان تو اندر پناه زلف</p></div>
<div class="m2"><p>چون در دل شب دزد راهزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر غمزهٔ تو ناوکی به دل</p></div>
<div class="m2"><p>هر مژهٔ تو خنجری به تن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صد یوسف دل کرده‌ای اسیر</p></div>
<div class="m2"><p>وافکنده‌ای اندر چه ذقن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان ناوک مژگان دل گداز</p></div>
<div class="m2"><p>گردیده مرا دل چو پروزن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگشای به جای من ای نگار</p></div>
<div class="m2"><p>از پای دل آن زلف چون رسن</p></div></div>