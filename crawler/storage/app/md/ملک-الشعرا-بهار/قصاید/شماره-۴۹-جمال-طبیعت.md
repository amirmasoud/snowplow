---
title: >-
    شمارهٔ ۴۹ - جمال طبیعت
---
# شمارهٔ ۴۹ - جمال طبیعت

<div class="b" id="bn1"><div class="m1"><p>جهان جز که نقش جهاندار نیست</p></div>
<div class="m2"><p>جهان را نکوهش سزاوار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سراسر جمال است و فر و شکوه</p></div>
<div class="m2"><p>بر آن هیچ آهو پدیدار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان را جهاندار بنگاشته است</p></div>
<div class="m2"><p>به نقشی کزان خوب‌تر کار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بیغاره رانی همی بر جهان</p></div>
<div class="m2"><p>چنان‌دان که جز برجهاندار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان راست مانند زیبا بتی است</p></div>
<div class="m2"><p>که چونان به‌مشکوی فرخار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو مفریب از او گرت هوشست یار</p></div>
<div class="m2"><p>فریب از در مرد هشیار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>متاب از بتی کان فریبنده است</p></div>
<div class="m2"><p>که بت را فریبندگی عار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنندهٔ کل ار خارش انگشت خست</p></div>
<div class="m2"><p>گنه بر چننده است بر خار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان عدل آمد بنای جهان</p></div>
<div class="m2"><p>کز آن عدل‌تر نقش پرگار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین نقش پرگار کژی مجوی</p></div>
<div class="m2"><p>اگر دیو را با دلت کار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سراسر فروغست و رخشندگی</p></div>
<div class="m2"><p>سیاهی درو جز به مقدار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگه کن بر این چتر افراشته</p></div>
<div class="m2"><p>که زر تاروار است و زرتار نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز زر الهی بر آن تارهاست</p></div>
<div class="m2"><p>ز زر هریوه برآن تار نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به یک ره دو پیکر پذیرد چنانک</p></div>
<div class="m2"><p>نگونسار هست و نگونسار نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گهی قیرگون گاه پیروزه گون</p></div>
<div class="m2"><p>گهی تارگونه است وگه تار نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گهش بر جبین خط گلنار هست</p></div>
<div class="m2"><p>گهش بر جبین خط گلنار نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو دیبای کحلی کزان خوب‌تر</p></div>
<div class="m2"><p>یکی دیبه در هیچ بازار نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بود دیبهٔ خسروانی‌، شگرف</p></div>
<div class="m2"><p>ولی چون سپهر ایزدی‌وار نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نگه کن برآن کوهسارکبود</p></div>
<div class="m2"><p>کش از ابر، یک نیمه دیدار نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی موسم گل برآن برگذر</p></div>
<div class="m2"><p>ز دیدن گرت دیده بیزار نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گذر کن برآن بام افراشته</p></div>
<div class="m2"><p>که از برف لختی سبکبار نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برآورده قصریست کاندازه‌اش</p></div>
<div class="m2"><p>در اندیشهٔ هیچ معمار نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برآن سبزه وگل بچم شادمان</p></div>
<div class="m2"><p>کرت جان رمنده زگلزار نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به‌بینی‌، گرت نیستی خارخار</p></div>
<div class="m2"><p>که‌صدکونه گل‌هست ویک‌خارنیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نگه کن بر آن جویبار روان</p></div>
<div class="m2"><p>که گویی به جزاشگ کهسار نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بود رخت درس‌ با و دلبنده کوه</p></div>
<div class="m2"><p>دلش لیک دربند دلدار نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نیاساید الا در آغوش مام</p></div>
<div class="m2"><p>ازبرا به جز رفتنش کار نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درختان بر او در تنیده بهم</p></div>
<div class="m2"><p>چنان کش به ره جای رفتار نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ازینسو بدانسو گریزد از آنک</p></div>
<div class="m2"><p>دلش ایمن ازدزد و طرار نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نگه کن بدان آفتاب بلند</p></div>
<div class="m2"><p>که طراروار است و طرار نیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نماید گذر بر در و بام خلق</p></div>
<div class="m2"><p>ولی ز اندرون‌ها خبردار نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نگه کن بدان تازه گل در بهار</p></div>
<div class="m2"><p>که خرم چنو گونهٔ یار نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نگه کن بدان مرغک بذله گوی</p></div>
<div class="m2"><p>که جز برگلش نالهٔ زار نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نگه کن بدان میوه اندر درخت</p></div>
<div class="m2"><p>که رخشان چنو در شهوار نیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نگه کن بدان دختر خردسال</p></div>
<div class="m2"><p>که نوزش به دل عشق را بارنیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نگه کن بدان پور پاکیزه چهر</p></div>
<div class="m2"><p>که در دام محنت گرفتار نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نگه کن بدان بی گنه کودکان</p></div>
<div class="m2"><p>که شان جز محبت پرستار نیست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نگه کن بدان مادر و آپن پدر</p></div>
<div class="m2"><p>که در سینه‌شان کینه انبار نیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جهان این کسانند و این است دهر</p></div>
<div class="m2"><p>جهان آن سیه روی غدار نیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو زبن نقش‌ها می چه رنجه شوی</p></div>
<div class="m2"><p>اگر دلت رنجه ز دادار نیست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ور از نقش دادار گشتی دژم</p></div>
<div class="m2"><p>تو را تن به جز نقش دیوار نیست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگرگویی این نقش‌ها ابتر است</p></div>
<div class="m2"><p>مرا بر حدیث تو اقرار نیست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به نقش نگارندهٔ چیره‌دست</p></div>
<div class="m2"><p>کس ار خرده گیرد بهنجار نیست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وگرگویی این‌نقش‌ها خود شده‌است</p></div>
<div class="m2"><p>کجا ز آفریننده آثار نیست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پس آن بد که بینی هم از چشم تست</p></div>
<div class="m2"><p>کت آئینه ناخورده زنگار نیست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از این در سخن هرچه ستوار و نغز</p></div>
<div class="m2"><p>به نزدیک داننده ستوار نیست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گرت بد رسد جمله ازخود رسد</p></div>
<div class="m2"><p>در آن بد زمانه گنهکار نیست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>توگوبی فسانه است کار جهان</p></div>
<div class="m2"><p>همیدون مرا با تو پیکار نیست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کدامین فسانه است کان پیش تو</p></div>
<div class="m2"><p>به یک‌بار خواندن سزاوار نیست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زتکرارهایش چه رنجی همی</p></div>
<div class="m2"><p>که عیب فسانه زتکرار نیست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو را گر مکرر، مرا تازه است</p></div>
<div class="m2"><p>جهان را به نزد تو زنهار نیست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دو بایست عمر از پی تجربت</p></div>
<div class="m2"><p>نگر کاین سخن محض پندار نیست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گرین خود درستست‌، صدساله عمر</p></div>
<div class="m2"><p>بر مرد فرزانه بسیار نیست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ور اندرزگیرد کس ازکار دهر</p></div>
<div class="m2"><p>ز تکرار اندرزش آزار نیست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بنالی همی از بلای جهان</p></div>
<div class="m2"><p>بلای جهان صعب و دشخوار نیست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بلای جهان آینهٔ مهر اوست</p></div>
<div class="m2"><p>که بی‌رنج‌، رامش نمودار نیست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>حکیمان پیشین چنین گفته‌اند</p></div>
<div class="m2"><p>که لذت جز از دفع تیمار نیست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گر آزاد مردی بلاجوی از آنک</p></div>
<div class="m2"><p>بلا جز که درخورد احرار نیست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کی آسایش و رامش جان برد</p></div>
<div class="m2"><p>کسی کاز بلا جانش افکار نیست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کجا هرگز ازگونه گونه خورش</p></div>
<div class="m2"><p>برد لذت آن کس که ناهار نیست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>زگیتی به واقع دل آن کس کند</p></div>
<div class="m2"><p>که این گیتی اندر برش خوار نیست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اگر برکنی دل ز ناخواسته</p></div>
<div class="m2"><p>تو را سوی من جاه و مقدار نیست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یکی شارسانی است‌، دیگر سرای</p></div>
<div class="m2"><p>کجا جای کشت و ده و دار نیست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همه نعمتی هستش الا در او</p></div>
<div class="m2"><p>کشاورز و درزی و نجار نیست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ازیدر بسازند و آنجا برند</p></div>
<div class="m2"><p>که آنجای مزدور و بیگار نیست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همه کشته و داشتهٔ خود خورند</p></div>
<div class="m2"><p>فرختار نی و خریدار نیست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>در آن شارسان مدبر افتد کسی</p></div>
<div class="m2"><p>کش این جا جز اعراض و ادبار نیست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>جهان را نبایست کردن یله</p></div>
<div class="m2"><p>که مرزوی گل جای مردار نیست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ببایست ورزید و برداشت بهر</p></div>
<div class="m2"><p>بسوزند نخلی که بربار نیست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>من اکنون برآنم که گفت آن حکیم</p></div>
<div class="m2"><p>که ناشاستی اندرین دار نیست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>همه هرچه هست آن چنان بایدی</p></div>
<div class="m2"><p>به گیتی نبایسته ناچار نیست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زمانه یکی تیز تک بارگیست</p></div>
<div class="m2"><p>سوارش جز از مرد هموار نیست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>کسی کاو یله سازد آن بارگی</p></div>
<div class="m2"><p>چنان دان که چیزیش در بار نیست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گنه کاره است آن کش از دسترنج</p></div>
<div class="m2"><p>به لب نان و در کیسه دینار نیست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به نان کسان دوختن چشم آز</p></div>
<div class="m2"><p>گناهی است کان را ستغفار نیست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>نه از دسترنج است نان کسان</p></div>
<div class="m2"><p>کشان پیشه جز جور و کشتار نیست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>که از حلق این ناکسان فاصله</p></div>
<div class="m2"><p>فزون‌تر ز یک حلقه تادار نیست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هم از گور این دیو طبعان‌، طریق</p></div>
<div class="m2"><p>فزون از به دستی سوی نار نیست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>همانا گنهکارتر در جهان</p></div>
<div class="m2"><p>کس از مردم مردم‌آزار نیست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>از آن گفتم این را که گفت آن ادیب</p></div>
<div class="m2"><p>«‌یکی گل درین نغز گلزار نیست‌»</p></div></div>