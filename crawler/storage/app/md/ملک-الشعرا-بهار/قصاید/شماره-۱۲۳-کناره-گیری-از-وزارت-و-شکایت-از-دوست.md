---
title: >-
    شمارهٔ ۱۲۳ - کناره گیری از وزارت و شکایت از دوست
---
# شمارهٔ ۱۲۳ - کناره گیری از وزارت و شکایت از دوست

<div class="b" id="bn1"><div class="m1"><p>حدیث عهد و وفا شد فسانه درکشور</p></div>
<div class="m2"><p>زکس درستی عهد و وفا مجوی دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کارنامهٔ من بین و نیک عبرت گیر</p></div>
<div class="m2"><p>که کارنامهٔ احرار هست پر ز عبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من آن کسم که چهل ساله خدمتم باشد</p></div>
<div class="m2"><p>بر آسمان وطن ز آفتاب روشن‌تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نظم و نثرکس ار پایه‌ور شدی بودی</p></div>
<div class="m2"><p>مرا به کنگرهٔ تاج آفتاب‌، مقر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهای خدمت و سعی خود ار بخواستمی</p></div>
<div class="m2"><p>به کوه زربن بایستمی نمود گذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان و نعمت او پیش چشم همت من</p></div>
<div class="m2"><p>چنان بودکه پشیزی به چشم ملیون‌ور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه چشم دارم ازبن مردمان کوته‌بین</p></div>
<div class="m2"><p>نه بیم دارم ازین روزگار مردشگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به زبر منت کس یک نفس بسرنبرد</p></div>
<div class="m2"><p>کسی که شصت خزان و بهار برده بسر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ولی دربغ که خوردم ز فرط ساده‌دلی</p></div>
<div class="m2"><p>فریب دوستی اندر سیاست کشور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بس که بودم نومید از اولیای امور</p></div>
<div class="m2"><p>که جز عوام‌فریبی نداشتند هنر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی نگفته صریح ویکی نرفته صحیح</p></div>
<div class="m2"><p>یکی نداده مصاف و یکی نکرده خطر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر آن شدم که ز صاحبدلان بدست آرم</p></div>
<div class="m2"><p>بزرگمردی بسیارکار و نام‌آور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که با هجوم مخالف مقاومت گیرد</p></div>
<div class="m2"><p>نیفکند بر هر حمله در مصاف‌،‌سپر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به خواجه‌ از سر صدق و خلوص دل بستم</p></div>
<div class="m2"><p>ز پیش آنکه رضا شه به سر نهد افسر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من و مدرس و تیمور و داور و فیروز</p></div>
<div class="m2"><p>زبهر خواجه به مجلس بساختیم محشر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به هفت نامه نوشتم مقالتی هرشب</p></div>
<div class="m2"><p>چنان که از پس آنم نه خواب ماند و نه خور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسا شباکه نشستم ز شام تا گه بام</p></div>
<div class="m2"><p>به نوک خامه نمودم ز خواجه دفع خطر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه ماه و هفته‌، گه بگذشت سالیان کامروز</p></div>
<div class="m2"><p>به نام خواجه نمودم هزار گونه اثر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز خواجه نفع نبردم به عمر خویش ولیک</p></div>
<div class="m2"><p>ز دشمنانش بدیدم هزار گونه ضرر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه دشمنان همه افسون‌طراز و هرزه‌درای</p></div>
<div class="m2"><p>چه دشمنان همه نیرنگ‌ساز و حیلت گر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه‌به نفس‌خبیث‌وهمه‌به‌طبع‌شریر</p></div>
<div class="m2"><p>همه به کذب مثال و همه به لؤم سمر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به راه خواجه ز جان عزیز شستم دست</p></div>
<div class="m2"><p>اگرچه نیست ز جان در جهان گرامی‌تر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همین نه روز عمل در وفا فشردم پای</p></div>
<div class="m2"><p>که هم به عزل نپیچیدم از ولایش سر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قوام‌، خانه‌نشین بود و منعزل آن روز</p></div>
<div class="m2"><p>که بانگ سیلی من کرد گوش خصمش کر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قوام بود به زندان و دشمنش برکار</p></div>
<div class="m2"><p>که بست از پی آزادیش بهار کمر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سپس که سوی سفرشد ز بنده یاد نکرد</p></div>
<div class="m2"><p>که چون همی گذرد روزگار ما ایدر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به جرم دوستی خواجه نفی و طرد شدم</p></div>
<div class="m2"><p>ازآن سپس که به هرلحظه بود جان به خطر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو خواجه آمد و آن آب از آسیاب افتاد</p></div>
<div class="m2"><p>به کوی مهدیه آمد فرود و جست مقر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شدم به دیدن و دادم نشان و نام ولی</p></div>
<div class="m2"><p>به رسم عرف نیامد زخواجه هیچ خبر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه نوبتی تلفون زد نه بازدید آمد</p></div>
<div class="m2"><p>نه یاد کرد که سویش روم به وقت دگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به خودگفتم کز بیم شاه پهلوی است</p></div>
<div class="m2"><p>که خواجه می‌نکند یاد از این ستایشگر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بلی چو خواجه بدیدست حبس و نفی مرا</p></div>
<div class="m2"><p>ز بیم شاه بشسته‌ست نامم از دفتر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گذشت بر من و بر خواجه قرب هجده سال</p></div>
<div class="m2"><p>که هر دو بودیم اندر مظان خوف و خطر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو شاه رفت شدم معتکف به درگه او</p></div>
<div class="m2"><p>میان ببسته و بازو گشاده شام و سحر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به رزم دشمن او با گروهی از یاران</p></div>
<div class="m2"><p>ز خامه پیکان آوردم از زبان خنجر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نوشته‌های من اندر ثنای حضرت او</p></div>
<div class="m2"><p>چو بشمری بود از صد مقالت افزون‌تر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یکی کتاب نبشتم که گر نکو نگری</p></div>
<div class="m2"><p>همه محامد این خواجه است سرتاسر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر از شکست دل ما برآمدی آواز</p></div>
<div class="m2"><p>شدی ز چشم تو خواب غرور و عشوه بدر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کسی نبودکه تاربخ رفته یاد آرد</p></div>
<div class="m2"><p>شد ازگذشتهٔ مقالات بنده یادآور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز فر خامه و سحر بنان و کوشش من</p></div>
<div class="m2"><p>به خواجه روی نهادند دوستان دگر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو یافت مسند دولت ز خواجه زیب و جمال</p></div>
<div class="m2"><p>ز دوستان به بهارش نیوفتاد نظر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به شعر بنده یکی نخل سایه گستر شد</p></div>
<div class="m2"><p>ولی دریغ که از بهر من نداشت ثمر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز خواجه دل نگرفتم تو این شگفتی بین</p></div>
<div class="m2"><p>که بود آتش مهرش مرا به جان اندر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدیم همدم روز و شبش من و یاران</p></div>
<div class="m2"><p>به فتنه‌ای که علم گشت در مه آذر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سپس که خانه‌نشین شد به قصدش از هر سو</p></div>
<div class="m2"><p>بساختند حسودان و دشمنان لشگر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بزرگ سنگری اندر حریم حرمت او</p></div>
<div class="m2"><p>بساختم من و بنشستم اندر آن سنگر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>درتن حوادث ازو هیچم انتظار نبود</p></div>
<div class="m2"><p>نه پایمردی کار و نه دستگیری زر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بسا شبا که شکایت نمود و نومیدی</p></div>
<div class="m2"><p>کش از امید گشودم به رخ هزاران در</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چه نقش‌هاکه نشان دادم از طریق صواب</p></div>
<div class="m2"><p>چه رازهاکه عیان کردم از مزاج بشر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همه شنید و پسندید و کاربست و رسید</p></div>
<div class="m2"><p>بدان مقام که جز وی نبودکس درخور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در‌بغ و درد که هم خواجه اندرین نوبت</p></div>
<div class="m2"><p>به‌ رغم من به دگر قوم گشت مستظهر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مرا به شغل وزارت بخواند خواجه ولی</p></div>
<div class="m2"><p>به صورتی که از آنم فتاد خون به جگر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>صریح گفت که شه را وزارت تو بد است</p></div>
<div class="m2"><p>از آن وزیر نگشتی و ماندی از پس در</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو نزد شاه برای معرفی رفتیم</p></div>
<div class="m2"><p>بکرد درحق من خواجه ضنتی بیمر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نداشت حرمت پیری نداشت حرمت نام</p></div>
<div class="m2"><p>ندید قدر شرافت ندید قدر هنر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زمام کار جهان را به سفله‌ای بسپرد</p></div>
<div class="m2"><p>که کس بدو نسپردی زمام استر و خر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سفیه و غره و نااعتماد و جاه‌طلب</p></div>
<div class="m2"><p>حسود و سفله و نیرنگ‌ساز و افسونگر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر آن شد از سر نامردمی که یاران را</p></div>
<div class="m2"><p>ز گرد خواجه کند دور از ایمن و ایسر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سپس چو گشت موفق به خواجه یازد دست</p></div>
<div class="m2"><p>شود به بازی بیگانه در جهان سرور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو میل خواجه بدو بود بنده تاب نداشت</p></div>
<div class="m2"><p>کناره جست و به عزلت فتاد در بستر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گذشت شش مه و از بنده خواجه یاد نکرد</p></div>
<div class="m2"><p>دربغ از آن ‌همه سودا که پختم اندر سر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خدا نخواست که ایران شود ز خواجه تهی</p></div>
<div class="m2"><p>وگرنه سفله بسنجیده بود این منکر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بر آن شدم که از ایران برون روم چندی</p></div>
<div class="m2"><p>مگرکه این تن رنجور توشه یابد و فر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به نزد خواجه شدم رخصتم نداد و جواز</p></div>
<div class="m2"><p>بگفت باش و به مجلس شو و مساز سفر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به امر خواجه بماندم ولی ازین ماندن</p></div>
<div class="m2"><p>همی تو گویی افتاده‌ام به قعر سقر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>فتاده‌ام به مغاکی درون کژدم و مار</p></div>
<div class="m2"><p>نه راه چاره همی بینم و نه راه مفر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>جهانیان را هرگز نرفته است از یاد</p></div>
<div class="m2"><p>که بیست سال بدم خواجه را حمایتگر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>وپر خواجه بدم هم وکیل حزب وبم‌</p></div>
<div class="m2"><p>وزین دو رتبه چه بالاتر است و والاتر؟</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ولی ندانند اینان که بین خواجه و من</p></div>
<div class="m2"><p>فتاده فاصله‌ای سخت بی‌حد و بیمر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گرفته‌اند گروهی حریم حضرت او</p></div>
<div class="m2"><p>که ره نیابد از آنجا نسیم جان‌ پرور</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>همه به طبع لئیم وهمه به نفس خبیث</p></div>
<div class="m2"><p>همه به معنی ابله همه به جنس ابتر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هم از فضیلت دور و هم از شرافت عور</p></div>
<div class="m2"><p>هم از دقایق کور و هم از حقایق کر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دروغگوی چو شیطان‌، دسیسه کار چو دیو</p></div>
<div class="m2"><p>فراخ‌روده چو یابو، چموش چون استر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>معاونند و وزبر و کمیته‌ساز و وکیل</p></div>
<div class="m2"><p>گهی ز توبره تناول کنند و گه ز آخور</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>کسان ز فرط گرانی و قلت مرسوم</p></div>
<div class="m2"><p>کنند ناله و اینان به عیش و عشرت در</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>من این میانه نگه می کنم بر این عظما</p></div>
<div class="m2"><p>چو اشتری که بود نعلبندش اندر بر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>عجب‌تر آنکه بدین حال و روز و این پک و پوز</p></div>
<div class="m2"><p>به نزد خواجه بد ما همی کنند از بر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گمان برند که ما فرصتی همی جوییم</p></div>
<div class="m2"><p>که جای ایشان گیریم وطی شود چرچر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>دربغ از آنکه ندانند کاشیان عقاب</p></div>
<div class="m2"><p>به کوهسار بلند است نی در آخور خر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>دربغ از آنکه ندانندکه افتخار همای</p></div>
<div class="m2"><p>به خردکردن ستخوان بود نه قند وشکر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>دربغ از آنکه ندانند کاین سیهکاری</p></div>
<div class="m2"><p>به هیچ روی نیابد خلاص ازکیفر </p></div></div>