---
title: >-
    شمارهٔ ۹۹ - سرگذشت شاعر
---
# شمارهٔ ۹۹ - سرگذشت شاعر

<div class="b" id="bn1"><div class="m1"><p>یاد باد آن عهد کم بندی به پای اندر نبود</p></div>
<div class="m2"><p>جز می اندر دست و غیر از عشقم اندر سر نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوبتر از من جوانی خوش کلام و خوش‌خرام</p></div>
<div class="m2"><p>در میان شاعران شرق‌، سرتاسر نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درسخن‌های دری چابک تر و بهتر ز من</p></div>
<div class="m2"><p>در همه مرز خراسان‌، یک سخن گستر نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سال عمر دوستان از پانزده تا شانزده</p></div>
<div class="m2"><p>سال عمر بنده نیز از بیست افزونتر نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیست ساله شاعری‌، با چشم‌های پرفروغ</p></div>
<div class="m2"><p>جز من اندر خاوران معروف و نام آور نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خانه‌ای شخصی و مبلی ساده و قدری کتاب</p></div>
<div class="m2"><p>آمد و رفتی و ترتیبی کز آن خوشتر نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مادرم تدبیر منزل را نکو می‌داشت پاس</p></div>
<div class="m2"><p>پاسداری در جهانم‌، بهتر از مادر نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر آن دوران نبود اندر دواوین عجم</p></div>
<div class="m2"><p>ز اوستادی شعر خوبی کان مرا از بر نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعر می‌‎گفتیم و می گشتیم و می‌بودیم خوش</p></div>
<div class="m2"><p>بزم ما گه‌گاه بی مَه‌روی و خنیاگر نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حال ما با حال حاضر فرق وافر داشت زانک</p></div>
<div class="m2"><p>صافی افکار را، درد نفاق اندر نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دشمنی‌ها این‌چنین پر حدت و وحشت نبود</p></div>
<div class="m2"><p>دوستی‌ها نیز از اینسان ناقص و ابتر نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اولا عرض فکل‌ها، اینقدر وسعت نداشت</p></div>
<div class="m2"><p>ثانیا فکر جوانان‌، اینقدر لاغر نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر جوانی باکسی پیوند می کرد از وفا</p></div>
<div class="m2"><p>زلف او هر روز در چنگ کسی دیگر نبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تهمت و توهین و هوکردن نبود اینقدر باب</p></div>
<div class="m2"><p>ور کسی می‌‎گفت زشتی‌، خلق را باور نبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>علتش آن بود کز اخلاق ناپاکان ری</p></div>
<div class="m2"><p>ملت پاک خراسان هیچ مستحضر نبود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زین فلک بندان لوس کون نشوی نادرست</p></div>
<div class="m2"><p>یک تن از تهران به مرز خاوران رهبر نبود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بی‌وفایی و دورویی و نفاق و ناکسی</p></div>
<div class="m2"><p>در لباس عقل و دانش‌، زیب هر پیکر نبود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عشوه و تفتین و غمازی و شوخی‌های زشت</p></div>
<div class="m2"><p>در لوای شوخ‌چشمی نقل هر محضر نبود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بود نوکر باب کمتر، حشر او محدودتر</p></div>
<div class="m2"><p>وز جوانان اداری هر طرف محشر نبود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگذریم از این سخن وین خود طبیعی بود نیز</p></div>
<div class="m2"><p>کاین رسن را فرصت بگذشتن از چنبر نبود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شور و شری ناگه اندر طوس زاد از انقلاب</p></div>
<div class="m2"><p>فکرت من نیز بی‌رغبت به شور و شر نبود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در صف طلاب بودم‌، در صف کتاب نیز</p></div>
<div class="m2"><p>در صف احرار هم‌چون من یکی صفدر نبود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در سیاست اوفتادم آخر از اوج علا</p></div>
<div class="m2"><p>وین همی‌دانم به خوبی کان مرا درخور نبود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روزنامه گر شدم‌، با سائسان همسر شدم</p></div>
<div class="m2"><p>واندر آن دوران کسم زین سائسان همسر نبود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرچه بود از کفر کافر ماجرایی ‌طبع دور</p></div>
<div class="m2"><p>گام‌های انقلابی لیک‌، بی کیفر نبود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در هزار و سیصد و سی، روسیان روسبی</p></div>
<div class="m2"><p>طرد کردندم به ری‌، زیرا کسم یاور نبود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رهزنان پارسی‌، در کوهسار لاسگرد</p></div>
<div class="m2"><p>رخت من بردند و خرسندم که هیچم زر نبود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سوی ری راندم به خواری از دربند خوار</p></div>
<div class="m2"><p>کشوری دیدم که جز لعنت در آن کشور نبود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مردمی دیدم یکایک از گدا تا شاه‌، زن</p></div>
<div class="m2"><p>منتها همچون زنان بر فرقشان معجر نبود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>معشری دیدم سراسر از جوان تا پیر دزد</p></div>
<div class="m2"><p>لیک چون دزدان لباس ژنده‌شان در بر نبود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هشت مه ماندم به ری پس بازگشتم زی وطن</p></div>
<div class="m2"><p>کم توان فرقت یاران دانشور نبود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روزگاری دیر خوش بودیم با یاران خویش</p></div>
<div class="m2"><p>کاسمان را کینهٔ دیرینه‌، اندر سر نبود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نوبهاری ساختم ز اندیشه‌های تابناک</p></div>
<div class="m2"><p>کاندر آن جز لاله و نسرین و سیسنبر نبود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>درخور اخلاق امت‌، درخور اصلاح قوم</p></div>
<div class="m2"><p>لیک تنها درخور یک مشت حیلتگر نبود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از خدا بیگانه‌ام خواندند اندر مرز طوس</p></div>
<div class="m2"><p>از خدا بیگانگان‌، اما به پیغمبر نبود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سخت آقایان هوم کردند، آری سخت هو</p></div>
<div class="m2"><p>کان‌چنان هو، هوچیان را ثبت در دفتر نبود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هو شدم اما ز میدان درنرفتم مردوار</p></div>
<div class="m2"><p>لیک یاران را سر برگ من مضطر نبود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زین سبب در هم شکست از جور روس و انگلیس</p></div>
<div class="m2"><p>شکرین کلکی که چون او هیچ نی‌شکر نبود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این‌چنین کید از رفیقان دوروی آمد پدید</p></div>
<div class="m2"><p>شکوه‌ام از کید چرخ و خصم بد اختر نبود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دوستان دور، قدر خدمتم بشناختند</p></div>
<div class="m2"><p>زان که عمر خدمتم را ساعت آخر نبود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رای دادند از دره گز وزکلات و از سرخس</p></div>
<div class="m2"><p>تا شوم زی ری که چون منشان یکی غمخور نبود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در هزار و سیصد و سی و دو زی کنگاشگاه </p></div>
<div class="m2"><p>ره گرفتم پیش و جز خضر رهم رهبر نبود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دشمنان رو به‌ آیین غدرها کردند، لیک</p></div>
<div class="m2"><p>غدر آنان درخور تنکیل شیر نر نبود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>محضری کردند در تکفیر من زی کاخ عدل</p></div>
<div class="m2"><p>لیک تاثیری از آن محضر، در آن محضر نبود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از پس یک سال و اندی رنج‌، کاندر ملک ری</p></div>
<div class="m2"><p>قسمت اوفر مرا، جز نقمت اوفر نبود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>لشگر روس از در قزوین به ری راندند و من</p></div>
<div class="m2"><p>سوی قم راندم از آن کم تاب آن لشکر نبود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اندرآن پرخاشگه بشکست دستم از دو جای</p></div>
<div class="m2"><p>وین شکست آخر بلای این تن لاغر نبود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دولت وقتم سوی ری خواند و اندر دار ملک</p></div>
<div class="m2"><p>پهلویم یک‌چند جز بر پهلوی بستر نبود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>با چنان حالت نیاسودم ز دست دشمنان</p></div>
<div class="m2"><p>جرمم این کم جز هوای دوستان در سر نبود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مهتر ملکم به امر انگلستان بند کرد</p></div>
<div class="m2"><p>از سپهسالار دون‌همت جز این درخور نبود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سوی سمنانم فرستادند، در تحت نظر</p></div>
<div class="m2"><p>در نظر چیزیم ناخوش‌تر ازین منظر نبود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زان مکان یرلیغ دشمن در خراسانم فکند</p></div>
<div class="m2"><p>آستان بوسیدم آنجا کآسمان را فر نبود</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سوی بجنوردم فکند آنگاه‌، یرلیغ دگر</p></div>
<div class="m2"><p>کش به جز آزار من فکری به مغز اندر نبود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مرده بودم بی گنه در خطهٔ بجنورد اگر</p></div>
<div class="m2"><p>مهر سردار معزز، حصن این چاکر نبود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گر بمنفی جانب فردوس می‌رفتم ز طوس</p></div>
<div class="m2"><p>در نظر فردوسم از بجنورد، نیکوتر نبود</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مردم بجنورد از آن پس هم وکیلم ساختند</p></div>
<div class="m2"><p>در جهان آری به جز نوش از پس نشتر نبود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گرچه جانم زین چهارم مجلس از محنت گداخت</p></div>
<div class="m2"><p>زان که یک جو همگنان را دانش اندر سر نبود</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>جز فساد و خبث طینت‌، در جماعات اقل</p></div>
<div class="m2"><p>جز غرور و خبط و غفلت‌، در صف اکثر نبود</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>راستی جز چند تن معدود دانشمند و راد</p></div>
<div class="m2"><p>اندر آن مجلس تو گفتی یک خردپرور نبود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اختر بختم کنون زین اقتران نحس جست</p></div>
<div class="m2"><p>کاش بر این گنبد پست این بلند اختر نبود</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نک بر آن عزمم که از ری بازگردم زی وطن</p></div>
<div class="m2"><p>کاندرین میخانه‌ام‌، جز زهر در ساغر نبود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>استخوانم خرد شد در آرزوی معدلت</p></div>
<div class="m2"><p>کاشکی ز اول همای آرزو را پر نبود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در امید نوگل اصلاح‌، صوتم پست گشت</p></div>
<div class="m2"><p>کاش هرگز بلبل امید را حنجر نبود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>لفظ دلبر راندم اما خلق را دل برنتافت</p></div>
<div class="m2"><p>شعر نیکو گفتم اما قوم را مشعر نبود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در محافل پا نهادم غیرگرک وگوسپند</p></div>
<div class="m2"><p>در مجامع سر زدم جز اسب و جز استر نبود</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دسته دسته گوسپندان دیدم و سردسته گرگ</p></div>
<div class="m2"><p>گرگ خونشان خورد و مسکین گله را باور نبود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>افعیانی آدمی‌وش‌، مردمی افعی‌پرست</p></div>
<div class="m2"><p>وه که اندر دست من گرزی کران‌پیکر نبود</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زهر اغفال است در دندان ماران ریا</p></div>
<div class="m2"><p>چون گزد گویند جز بوسیدنی دیگر نبود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>هر که رخ برتافت از این بوسه‌های زهردار</p></div>
<div class="m2"><p>نامش غیر از خائن و وصفش به جز کافر نبود</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کوفتم سر زافعیان‌، نیز از میانشان بردمی</p></div>
<div class="m2"><p>جهل این افعی‌پرستان مانع من گر نبود</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>کشور دارا نبد هرگز چنین بی‌پاسبان</p></div>
<div class="m2"><p>خانهٔ نوشیروان‌، هرگز چنین بی‌در نبود</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>شیر و خورشید ای دریغ ار جنبشی می کرد از آنک</p></div>
<div class="m2"><p>خرس و روبه را گذاری بر یک آبشخور نبود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>زود درسازند خصمان وین مثل روشن شود</p></div>
<div class="m2"><p>گر عروسی کرد سگ جز بهر مرگ خر نبود</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>این قصیده در جواب فرخ است آنجا که گفت </p></div>
<div class="m2"><p>دوش ما را بود بزمی خوش‌، کز آن خوشتر نبود</p></div></div>