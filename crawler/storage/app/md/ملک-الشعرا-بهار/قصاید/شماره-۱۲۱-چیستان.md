---
title: >-
    شمارهٔ ۱۲۱ - چیستان
---
# شمارهٔ ۱۲۱ - چیستان

<div class="b" id="bn1"><div class="m1"><p>چیست آن جنبدهٔ والاگهر</p></div>
<div class="m2"><p>گوهرش از آب و آتش جسته فر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زادهٔ خورشید و هم‌پیمان خاک</p></div>
<div class="m2"><p>گاه چون مریخ و گاهی چون قمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمان رنگی پذیرد در جهان</p></div>
<div class="m2"><p>گه سیه‌، گه سرخ‌، گه رنگ دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانورکردار، جنبانست و هست</p></div>
<div class="m2"><p>اندر او جان‌ها و خود ناجانور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار گیرنده به مانند ستور</p></div>
<div class="m2"><p>راه جوینده بمانند بشر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راه را از چاه بشناسد از آنک</p></div>
<div class="m2"><p>همچو مردم صاحب‌ مغزست و سر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دویدن چون دگر جنبندگان</p></div>
<div class="m2"><p>در قفای خویش نگذارد اثر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست فربه لیک چون ساکن شود</p></div>
<div class="m2"><p>مهره‌های پشتش آید در شمر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک زمان اندر دوره پویان بود</p></div>
<div class="m2"><p>وان دو یکسانست او را در نظر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرده از گردون گردان عاریت</p></div>
<div class="m2"><p>پای‌ها، وز نسر طایر بال و پر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست او را چشم چون مردم ولیک</p></div>
<div class="m2"><p>صد جهان بین است او را بیشتر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچنانش گوش‌ها باشد ولی</p></div>
<div class="m2"><p>این شگفتی بین که باشد کور و گر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کور ره جویست کٌر خوش نیوش</p></div>
<div class="m2"><p>گنگ غرنده است و لنگ راه بر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با ستور و گاو و خر دشمن و لیک</p></div>
<div class="m2"><p>شاد ازو جان ستور و گاو و خر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هست چون ابری سیه با رعد و برق</p></div>
<div class="m2"><p>لیک از او هرگز نمی بارد مطر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جنگیئی باشد که او را گاه رزم</p></div>
<div class="m2"><p>جوشن از چوبست و از آهن سپر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گاهگاهی زیر چوبین جوشنش</p></div>
<div class="m2"><p>می کند خفتانی از دیبا به بر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پای‌ها دارد ولی افعی مثال</p></div>
<div class="m2"><p>سینه مالان‌، پیچد اندر بوم و بر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هست همچون اژدها مردم ربای</p></div>
<div class="m2"><p>اژدری مردم‌خور و هامون سپر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هرچه پیش آید بیوبارد همی</p></div>
<div class="m2"><p>زادمی و اشتر و اسب و ستر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وین شگفتی بین کزین بلعیدنش</p></div>
<div class="m2"><p>مردم و حیوان نمی‌بیند ضرر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لیک اگر بلعیده‌ها دورافکند</p></div>
<div class="m2"><p>جان شیرینشان شود از تن بدر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گه شود زاو کشوری خرم بهشت</p></div>
<div class="m2"><p>گه شود ز او ملکتی زیر و زبر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گه بشیر دولتست و جاه و مال</p></div>
<div class="m2"><p>گه نذیر غارتست و شور و شر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر فزونش طعمه باشد هست رام</p></div>
<div class="m2"><p>ور کمش باشد خورش زاید خطر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تربیت کردنش دشوار است و سخت</p></div>
<div class="m2"><p>واندر آن گنجینه‌ها گردد هدر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زین زیانکاری که باشد اندر او</p></div>
<div class="m2"><p>پادشاهان را بود از وی حذر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر به کار آید بود بس جانفزای</p></div>
<div class="m2"><p>ور ز کار افتد شود بس جان شکر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آوخ از این غول شکل دیو فعل</p></div>
<div class="m2"><p>آوخ از این پیل زور دد سیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هان‌وهان‌ماریست‌بس خوش‌خط وخال</p></div>
<div class="m2"><p>جانب وی دست بی‌افسون مبر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گنج ایران شد هزینه اندر او</p></div>
<div class="m2"><p>باز ناپیداست پای او ز سر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بو که گردد رام عزم شهریار</p></div>
<div class="m2"><p>این هیون بدلگام خاره در</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گنجهایی را کز ایران خورده است</p></div>
<div class="m2"><p>قی کند این اژدهای گنج‌خور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پیش از آن کش افکند از بیخ و بن</p></div>
<div class="m2"><p>سیل اشگ و دود آه رنجبر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خورده ملیون‌ها، به ما واپس دهد</p></div>
<div class="m2"><p>کیسه‌ها را پر کند از سیم و زر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا که گردد صرف بند هیرمند</p></div>
<div class="m2"><p>یا که گردد خرج سدّ شوشتر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا که گردد صرف کاری کاندران</p></div>
<div class="m2"><p>خیر ملت باشد و نفع بشر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>لختی از آن صرف ایجاد قنات</p></div>
<div class="m2"><p>بخشی از آن خرج تسطیح ممر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>قسمتی زان وقف بر طبع کتاب</p></div>
<div class="m2"><p>پاره‌ای زان بخش بر اهل هنر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نیمه‌ای خرج سلیح و ساز جنگ</p></div>
<div class="m2"><p>بهره‌ای خرج سپاه نامور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ور شهنشه ریزدی آن را به دور</p></div>
<div class="m2"><p>تا ربودندیش خلق از رهگذر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به که تقدیم فرنگستان شود</p></div>
<div class="m2"><p>آنچه گرد آمد به صد خون جگر</p></div></div>