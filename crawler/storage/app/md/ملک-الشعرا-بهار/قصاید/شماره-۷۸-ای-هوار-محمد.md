---
title: >-
    شمارهٔ ۷۸ - ای هوار محمد!
---
# شمارهٔ ۷۸ - ای هوار محمد!

<div class="b" id="bn1"><div class="m1"><p>دارد سرهنگ شهریار محمد</p></div>
<div class="m2"><p>لطف به‌ من‌، چون ‌به ‌یار غار محمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما و محمد دو دوستار قدیمیم</p></div>
<div class="m2"><p>کی رود از یاد دوستار، محمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرد جان و نثار شاه نماید</p></div>
<div class="m2"><p>گوید اگر شه که جان بیار محمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه به وی اعتمادکامل دارد</p></div>
<div class="m2"><p>چون به خداوند ذوالفقار، محمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون او یک رند کهنه کار ندیدم</p></div>
<div class="m2"><p>دیده‌ام اندر جهان هزار محمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست‌ به خط و روال خوبش درپن شهر</p></div>
<div class="m2"><p>نابغه‌ای کامل‌العیار محمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نغز حدیثی بگویمش که پس از من</p></div>
<div class="m2"><p>داردش از من به یادگار محمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منکر گشتند مکیان‌، چو در آن شهر</p></div>
<div class="m2"><p>دعوت خود کرد آشکار محمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفت ز مکه سوی مدینه و آورد</p></div>
<div class="m2"><p>بر سرشان جیش بیشمار محمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرد بسی‌ جنگ و کشته گشت بسی خلق</p></div>
<div class="m2"><p>هم به احد گشت زخمدار محمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاقبت‌الامر تاخت بر سر مکه</p></div>
<div class="m2"><p>از پس یک رشته کارزار محمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون که به زنهارش آمدند حریفان</p></div>
<div class="m2"><p>داد بدان جمله زبنهار محمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کین کشی و انتقام کار ضعیف است</p></div>
<div class="m2"><p>سخت قوی بود و بردبار محمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سابقه ننهاد بهرشان پس تسلیم</p></div>
<div class="m2"><p>با همهٔ فر و اقتدار محمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیز نه پاپوش دوخت نه کلکی کرد</p></div>
<div class="m2"><p>چون به کف آورد اختیار محمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باری اندر مثل مناقشتی نیست</p></div>
<div class="m2"><p>فرض که ری مکه‌، شهریار محمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بنده قسم خورده‌ام به دوستی شاه</p></div>
<div class="m2"><p>خلف قسم را کشد دمار محمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاهد رفتار این دو سال اخیرم</p></div>
<div class="m2"><p>بوده یکایک درین دیار محمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باز چرا بر سرم کلاه گذارند</p></div>
<div class="m2"><p>مُردم ازبن مَردم‌، ای هوار محمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر کلهم بی‌لبه است گو بشمارد</p></div>
<div class="m2"><p>اهل وطن را گناه کار محمد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ور نشدستم مرید احمد و محمود</p></div>
<div class="m2"><p>بهر چه دارد ز من نقار محمد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غیر شه پهلوی و غیر تمرتاش</p></div>
<div class="m2"><p>نیست به قول کس اعتبار محمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زمزمهٔ ‌این‌ دو روزه چیست‌، اگر نیست</p></div>
<div class="m2"><p>مطلع از اصل کوک کار محمد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مدعی بنده کیست تا به جوابش</p></div>
<div class="m2"><p>باز کنم تکمهٔ ازار محمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مردهٔ چل ساله را به او بنمایم</p></div>
<div class="m2"><p>تا شود از بنده شرمسار محمد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بود کس ار مدعی بغیر «‌سهیلی‌»</p></div>
<div class="m2"><p>گو بزند بنده را به دار محمد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>الغرض ای مشفق قدیمی بنده</p></div>
<div class="m2"><p>شحنهٔ راد بزرگوار محمد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در حق من بدگمان مباش به مولا</p></div>
<div class="m2"><p>اصل ندارد هر اشتهار محمد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وآنچه تو دیدی ز قابلیت اعصاب</p></div>
<div class="m2"><p>رفت به تاراج روزگار محمد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پیری و آلودگیم عنین کرده است</p></div>
<div class="m2"><p>دهر نماند به یک قرار محمد</p></div></div>