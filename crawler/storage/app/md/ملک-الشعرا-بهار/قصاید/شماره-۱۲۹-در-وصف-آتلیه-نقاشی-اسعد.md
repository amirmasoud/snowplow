---
title: >-
    شمارهٔ ۱۲۹ - در وصف آتلیه نقاشی اسعد
---
# شمارهٔ ۱۲۹ - در وصف آتلیه نقاشی اسعد

<div class="b" id="bn1"><div class="m1"><p>حبذا از این نگارستان پر نقش و نگار</p></div>
<div class="m2"><p>خوش‌تر از بتخانهٔ چین و سرای نوبهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفحه ‌اندر صفحه خرم ‌چون بهشت ‌اندر بهشت</p></div>
<div class="m2"><p>پرده اندر پرده رنگین چون بهار اندر بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش‌های روم و یونان پیش نقشش ناتمام</p></div>
<div class="m2"><p>طرح‌های چین و تبت ییش طرحش نابکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حرکت از هر گوشه پیدا، صنعت از هرسو پدید</p></div>
<div class="m2"><p>فکر هر جانب نمایان‌، ذوق هر جا آشکار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌دود از هر طرف در این گلستان سیل روح</p></div>
<div class="m2"><p>راست همچون جدول باران به روز ژاله بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوستان بینی و گو می‌وزد این دم نسیم</p></div>
<div class="m2"><p>کاروان بینی و گوبی می‌نهد این لحظه بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویی اکنون می‌پرد از نزد ما نقش تذور</p></div>
<div class="m2"><p>گویی اینک می‌دود بر روی ما شکل سوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جنگلی بینی که شبنم می‌چکد از برک گل</p></div>
<div class="m2"><p>وز نسیم نرم حرکت می کند برگ چنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوسن بری ز شرم سوسن او روی زرد</p></div>
<div class="m2"><p>لالهٔ دشتی ز رشگ لالهٔ او داغدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساق گل بینی و خواهی تا کنی از لطف بوی</p></div>
<div class="m2"><p>لیک ‌از آن ترسی که بر دستت خلد ز آن ساق خار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوزن ‌عیسی ‌بود با رشتهٔ ‌مریم ‌قرین</p></div>
<div class="m2"><p>کاین روانبخشی روان کرده‌است بر هر پود و تار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کی شدی ارژنگ مانی همچو عنقا بی‌نشان</p></div>
<div class="m2"><p>گر ز سوزن کرد «‌اسعد» داشتی یک رشته کار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نور چشم ایلخان اسعد محمد آن که هست</p></div>
<div class="m2"><p>بختیاری را شرف زین خاندان بختیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اسعدا وصف نگارستان زیبای ترا</p></div>
<div class="m2"><p>خامهٔ من لوحه‌ای آراست بهر یادگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سوزن من خامه است و رشته‌اش فکر بلند</p></div>
<div class="m2"><p>نقش سوزن کرد من وصف نگارستان یار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر بخواند احمدی قاضی القضاه این چامه را</p></div>
<div class="m2"><p>آفرین راند به طبع صورت‌انگیز بهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در میان بنده و اسعد همو شاید حکم</p></div>
<div class="m2"><p>زان که هست اندر قضاوت دادبان و دادیار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن که گر برخوان جودش نه ‌فلک سغدو شود </p></div>
<div class="m2"><p>گزلک عزمش کند در یک دم او را چاپار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شکوهٔ اسعد به هرمز بردم آری گفته‌اند</p></div>
<div class="m2"><p>شکوهٔ یاران به یاران کرد باید آشکار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شکوه‌ای گر از تو هست اندر دل پردرد من</p></div>
<div class="m2"><p>احمدی آن شکوه را خواهد نمودن برکنار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ورتو با جمشید هستی در نزاع مرده ریک</p></div>
<div class="m2"><p>از چه با من رفت فعل مرده شو با مرده‌خوار؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>داده و بخشیده خود باز نستاند کریم</p></div>
<div class="m2"><p>این بود رسم بزرگان‌، این بودی خوی کبار</p></div></div>