---
title: >-
    شمارهٔ ۱۱۱ - عدل مظفر
---
# شمارهٔ ۱۱۱ - عدل مظفر

<div class="b" id="bn1"><div class="m1"><p>کشور ایران ز عدل شاه مظفر</p></div>
<div class="m2"><p>رونقی از نوگرفت و زینتی از سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عدل ملک ملک را فزود و بیاراست</p></div>
<div class="m2"><p>روزافزون باد عدل شاه مظفر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پادشاه دادگر مظفر دین شاه</p></div>
<div class="m2"><p>خسرو روشن‌دل عدالت‌گستر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد به‌نام ایزد این ملک سره کاری</p></div>
<div class="m2"><p>تا سره گردید کار کشور و لشکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انجمن عدل را به ملک بیاراست</p></div>
<div class="m2"><p>دست ستم را ببست وپای ستمگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجلسی آراست کاندرو ز همه ملک</p></div>
<div class="m2"><p>انجمن آیند بخردان هنرور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواست به هم اتحاد دولت و ملت</p></div>
<div class="m2"><p>تا بنمایند خیر ملک وی از شر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشور آباد شد به نیروی ملت</p></div>
<div class="m2"><p>ملت منصور شد به یاری کشور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاری داور به عدل شاه قرین شد</p></div>
<div class="m2"><p>دولت و ملت از آن شدند توانگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوئی ناید همی ز دست تهی کار</p></div>
<div class="m2"><p>آری در این سخن به خردی منگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مردی کز نیروی دو دست برومند</p></div>
<div class="m2"><p>بازگشاید هزار سد سکندر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان دو یکی را اگر ببندی بر پشت</p></div>
<div class="m2"><p>مرد به یک دست عاجز آید و مضطر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دولت و ملت دودست و بازوی‌ شاهند</p></div>
<div class="m2"><p>شاه مر این هر دو را گرامی پیکر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یک به دگر کارها همی بگشایند</p></div>
<div class="m2"><p>گر نشکیبد یکی ز یاری دیگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دولت و ملت چو هر دو دست به‌هم داد</p></div>
<div class="m2"><p>پای به دامن کشد عدوی سبکسر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دولت و دین هر دو توأمند ولیکن</p></div>
<div class="m2"><p>این دو پسر راست عدل و قانون مادر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مادر باید که پرورد پسر خویش</p></div>
<div class="m2"><p>قانون باید که ملک یابد زیور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ملک تبه گردد از تطاول سلطان</p></div>
<div class="m2"><p>دهکده ویران شود ز جور کدیور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ملکی کاو راست عدل و قانون در دست</p></div>
<div class="m2"><p>سر بفرازد همی به برج دوپیکر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>راست چنان چون بزرگ کشور ایران</p></div>
<div class="m2"><p>کاین همه دارد ز فر شاه فلک‌فر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست‌ شگفتی گر این‌چنین بود این ملک</p></div>
<div class="m2"><p>دست به دندان مخای و بیهده مگذر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بنگرکاین ملک باستانی از آغاز</p></div>
<div class="m2"><p>جایگه عدل و داد بود و نه زیدر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملک کیومرث بود و کشور جمشید</p></div>
<div class="m2"><p>جای منوچهر بود و بنگه نوذر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این بود آن کشوری که داد به کاوس</p></div>
<div class="m2"><p>طوق و نگین و سریر و یاره و افسر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>طوس سپهبد درو فراشته رایت</p></div>
<div class="m2"><p>رستم دستان در او گماشته لشکر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نامهٔ هریک بخوان و کردهٔ هریک</p></div>
<div class="m2"><p>وین سخنان مرا به بازی مشمر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زاد پیمبر به گاه دولت کسری</p></div>
<div class="m2"><p>فخر همی کرد ازین قضیه پیمبر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت بزادم به عهد خسرو عادل</p></div>
<div class="m2"><p>بنگر کاین گفته خود چه دارد در بر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مدحت نوشیروان نگفته بدین قول</p></div>
<div class="m2"><p>بلکه نبی عدل راست مدحت گستر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا که شوند این ملوک دولت اسلام</p></div>
<div class="m2"><p>زبن سخن او به عدل‌، قاصد و رهبر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شکر خداوند را که خسرو ایران</p></div>
<div class="m2"><p>نیک نیوشید این کلام مشهر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>منظری از عدل بس بلند برافراشت</p></div>
<div class="m2"><p>ظلم درافتاد از آن فراشته منظر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عدل انوشیروان اگر نشنودی</p></div>
<div class="m2"><p>روروبکره ببین به نامه ودفتر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وانگه بنگر به عدل این ملک راد</p></div>
<div class="m2"><p>عدل انوشیروان به یاد میاور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>احسنت ای پادشاه مملکت‌آرای</p></div>
<div class="m2"><p>احسنت ای خسرو رعیت‌پرور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو غم مردم همی خوری به شب و روز</p></div>
<div class="m2"><p>غمخور توکیست‌؟ پادشاه گروگر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ملک تو شاها یکی عروس نکوروست</p></div>
<div class="m2"><p>کاو را جز عدل و داد نبود شوهر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکچند این خوبرو عروس نوآئین</p></div>
<div class="m2"><p>داشت به سربریکی پلاسین معجر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عدل تو با دیبه و پرند ملون</p></div>
<div class="m2"><p>آمد و برداشت این پلاس مقیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>لیک دریغا که روزگار بنگذاشت</p></div>
<div class="m2"><p>کزتو رسد ملک را طرازی دیگر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر سر و بر افسر تو خاک فرو بیخت</p></div>
<div class="m2"><p>این فلک باژگون که خاکش بر سر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مویه کند بر تو خسروانی دیهیم</p></div>
<div class="m2"><p>ناله کند بر تو شهریاری افسر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اخترت از آسمان ملک برون شد</p></div>
<div class="m2"><p>از ستم آسمان و کینهٔ اختر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بودی یک‌چندگاه غمخور این خلق</p></div>
<div class="m2"><p>رفتی و زینان یکی نبردی غمخور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بر تو مقدر بد این قضا ز خداوند</p></div>
<div class="m2"><p>کس نچخیده است با قضای مقدر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ملک بماندی و زی بهشت براندی</p></div>
<div class="m2"><p>ملک چرا ماندی ای بهشتی منظر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کاخی از عدل برنهادی و آنگاه</p></div>
<div class="m2"><p>تفت براندی ازین کهن شده معبر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>قومی بینم به سوکواری‌ات ای شاه</p></div>
<div class="m2"><p>جامه ز غم کرده چاک و دیده ز خون تر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>رفتی و پور تو شد برین گره خلق</p></div>
<div class="m2"><p>بارخدای و امیر و سید و سرور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ماه اگر شد نهان عیان شد خورشید</p></div>
<div class="m2"><p>دریا گر شد فرو برآمد گوهر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شاها اینک توئی نشسته بر اورنگ</p></div>
<div class="m2"><p>بر اثر آن خدایگان مظفر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>داد همی ده که دادگر ملکان را</p></div>
<div class="m2"><p>ایزد پاداش داد خواهد بی مر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>یاور شو خلق را به داد، به دنیا</p></div>
<div class="m2"><p>کرت به عقبی خدای باید یاور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>محضرکنکاش محضری‌ست همایون</p></div>
<div class="m2"><p>فر و بهی جوی ازین همایون محضر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ملک پدر را ز عدل و دادکن آباد</p></div>
<div class="m2"><p>ای به تو ملک پدر پسنده و درخور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شاها دانی که ملک ایران زین پیش</p></div>
<div class="m2"><p>بود چوآراسته یکی شجرتر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بود به گردش ز عدل کنده یکی جوی</p></div>
<div class="m2"><p>آبی دروی روان به طعم چو شکر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>زان پس چیدند ازو بسی بر امید</p></div>
<div class="m2"><p>بردهد آری چو شد درخت تناور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شاخه کشید این درخت تا گه کسری</p></div>
<div class="m2"><p>وانگاه از چرخ خواست کردن سر بر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زان پس گه گاهی این درخت برومند</p></div>
<div class="m2"><p>خسته همی شد زتیشهٔ فتن وشر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تاکه درین زشت روزگا‌ر ستردند</p></div>
<div class="m2"><p>جور و ستبداد، شاخ و برگش یکسر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چندان کز آنکشن درخت به‌جا ماند</p></div>
<div class="m2"><p>شاخی فرسوده وشکسته و لاغر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>وآنگه آسیب تندباد حوادث</p></div>
<div class="m2"><p>خواست فکندنش ناگهان ز بن اندر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کامد فرخنده باغبانی پیروز</p></div>
<div class="m2"><p>ناگه و آورد آب رفته به فرغر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>آبی انگیخته ز چشمه حیوان</p></div>
<div class="m2"><p>آبی آمیخته به شربت کوثر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>آبی بر باد داده خرمن بیداد</p></div>
<div class="m2"><p>آبی آتش زده به کشت ستمگر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آبی عدلش به‌نام خوانده خردمند</p></div>
<div class="m2"><p>آبی آزادیش ستوده هشیور</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>آبی از رهگذار دانش وبینش</p></div>
<div class="m2"><p>برد سوی آن درخت دهقان‌پرور</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آب‌روان کرد و خود برفت‌و از این نخل</p></div>
<div class="m2"><p>شاخی و برگی دمید ناقص و ابتر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>آب ازو برمگیر گرش بباید</p></div>
<div class="m2"><p>شاخ برومند و برگ خرم و اخضر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آب همی ده به کشور ازکرم و داد</p></div>
<div class="m2"><p>وآتش برزن به دشمن از دم خنجر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>جانب خاور هم ازکرم نظری کن</p></div>
<div class="m2"><p>ای ز تو فر و بهای خسرو خاور</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نشگفت ار به شوند از نظرتو</p></div>
<div class="m2"><p>کز نظر آفتاب سنگ شود زر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>سوی خبوشان یکی ببین که نیوشی</p></div>
<div class="m2"><p>نالهٔ چندین هزار مادر و دختر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بنگر تا مستمند وگریان بینی</p></div>
<div class="m2"><p>شوهر و زن را به فرقت زن و شوهر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گفت حکیم این گره نهال خدایند</p></div>
<div class="m2"><p>واستم استمگران چو بادی صرصر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>تا به‌هم اندر نیوفتند و نخوشند</p></div>
<div class="m2"><p>یک نظر ای باغبان بر ایشان بگمر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بگمر چندی نظر بر ایشان و آنگاه</p></div>
<div class="m2"><p>میوهٔ شیرین چن وشکوفهٔ احمر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ملک درختیست نغز و ربشه او عدل</p></div>
<div class="m2"><p>ربشه قوی دارکز درخت چنی بر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>شاه کجا سوی عدل و دادگراید</p></div>
<div class="m2"><p>بازگراید بدو عنایت داور</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گوید الملک لایدوم مع الظلم</p></div>
<div class="m2"><p>آنکه خدایش بسی ستوده ز هر در</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>قول پیمبر به کار بند و میازار</p></div>
<div class="m2"><p>خاطرمورضعیف وپشهٔ لاغر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>عدل و سخا وتوان و دانش بگزین</p></div>
<div class="m2"><p>تاکه جهانت شود دورویه مسخر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>گفتم مدح توبا طریقی مطبوع</p></div>
<div class="m2"><p>مرهمه را نیست این طربقه میسر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>گرچه هم اندر غزل توانم گفتن</p></div>
<div class="m2"><p>غمزهٔ مردم فریب وچشم فسونگر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>لیک نگونم بویژه اکنون کز شعر</p></div>
<div class="m2"><p>حکمت جویند نی گزاف وکر و فر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نشکفت ار حکمت آید از سخن من</p></div>
<div class="m2"><p>کزسنگ آید همه زلال مقطر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>اکنون ز امر خدایگان خراسان</p></div>
<div class="m2"><p>راست بود محضری بدین بلد اندر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>محضری آراسته ز عدل که پیشش</p></div>
<div class="m2"><p>سطح سپهر محدب است مقعر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چون‌فلک‌است‌این‌خجسته‌مجلس عالی</p></div>
<div class="m2"><p>دانشمندان در او فروزان اختر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>دیر نمانده است کز خراسان شاها</p></div>
<div class="m2"><p>سوی ری آیند بخردان هشیور</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>وزپی اصلاح ملک وفره خسرو</p></div>
<div class="m2"><p>دانشمندان کنند آنجا محضر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ای ملک راد شادمانه همی زی</p></div>
<div class="m2"><p>وی عدوی شاه رنج و درد همی بر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>تاکه بود عدل برگزبده‌تر از ظلم</p></div>
<div class="m2"><p>تاکه بود نفع خوشگواراتر از ضر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ملک تو آباد باد و جان تو خرسند</p></div>
<div class="m2"><p>جسم تو بی‌رنج باد و عیش تو بی‌مر</p></div></div>