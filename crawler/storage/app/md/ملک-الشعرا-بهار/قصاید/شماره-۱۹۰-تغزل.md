---
title: >-
    شمارهٔ ۱۹۰ - تغزل
---
# شمارهٔ ۱۹۰ - تغزل

<div class="b" id="bn1"><div class="m1"><p>گه فریضهٔ شام آن چراغ ترکستان</p></div>
<div class="m2"><p>کنار من ز رخ خویش کرد لالستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی شکار دل و جان به غمزه و ابرو</p></div>
<div class="m2"><p>کهی گشاده کمین وگهی گشوده کمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چرخ‌، برجیس از ماه روی او خیره</p></div>
<div class="m2"><p>به‌باغ‌، نرگس در چشم‌مست اوحیران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به‌زیر لعل لب اندر دو رشته دندانش</p></div>
<div class="m2"><p>چنان دورشتهٔ لولو به حقه مرجان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زلف خم شده‌، دامی ولیک دام بلا</p></div>
<div class="m2"><p>به‌ قد برشده‌، سروی ولیک سروروان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسان به‌ترکستانش دهند نسبت و من</p></div>
<div class="m2"><p>برآن سرم که کشم‌قبله سوی ترکستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخنش‌چیست‌عیان‌ودهانش‌چیست‌خبر</p></div>
<div class="m2"><p>کمرش چیست‌یقین ومیانش‌چیست گمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر سخن نسراید، پدید نیست دهن</p></div>
<div class="m2"><p>وگرکمر نگشاید، پدید نیست میان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو چشم سحرنمایش به‌ غمزه‌ غارت‌ دل</p></div>
<div class="m2"><p>دولعل روح‌فزایش به‌خنده راحت جان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آب وتابش بی‌آب‌، لاله ونسرین</p></div>
<div class="m2"><p>ز زلف وخطش بی‌تاب‌، سنبل وریحان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زتیره زلفش‌، روشن رخش چنان تابید</p></div>
<div class="m2"><p>که ماه در شب میلاد حجهٔ یزدان</p></div></div>