---
title: >-
    شمارهٔ ۱۸۵ - فتح دهلی
---
# شمارهٔ ۱۸۵ - فتح دهلی

<div class="b" id="bn1"><div class="m1"><p>دو چیز است شایسته نزدیک من</p></div>
<div class="m2"><p>رفیق جوان و رحیق کهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفیق جوان غم زداید ز دل</p></div>
<div class="m2"><p>رحیق کهن روح بخشد به‌ تن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفیقی به شایستگی مشتهر</p></div>
<div class="m2"><p>رحیقی به بایستگی ممتحن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوانی نه بر دامنش گرد ننگ</p></div>
<div class="m2"><p>شرابی نه در صافیش درد دن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهاده بطی باده در پیش روی</p></div>
<div class="m2"><p>کشیده یکی مرغ بر بابزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخور باده اکنون که گشت سپهر</p></div>
<div class="m2"><p>نزاید جز از انقلاب و فتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخوان شعر و اخبار کشور مخوان</p></div>
<div class="m2"><p>بزن چنگ و لاف سیاست مزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگه کن کز انفاس اردیبهشت</p></div>
<div class="m2"><p>ببالیده در باغ‌، سرو و سمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آن تند باران دوشینه بار</p></div>
<div class="m2"><p>بهشتی شد امروز طرف چمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ویژه که رخشنده مهر سپهر</p></div>
<div class="m2"><p>به میغی تنک درکشیده است تن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان کز پس توری آبگون</p></div>
<div class="m2"><p>نماید تن خویش معشوق من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به تن‌، کوه خارا کفن کرده بود</p></div>
<div class="m2"><p>ازآن بهمنی تند برف کشن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنون زنده شد زآسمانی فروغ</p></div>
<div class="m2"><p>یکی نیمه تن برکشید ازکفن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرو ریزد اردیبهشتی نسیم</p></div>
<div class="m2"><p>به باغ و براغ و به دشت و دمن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به باغ و به راغ آستین‌های گل</p></div>
<div class="m2"><p>به دشت و دمن عقدهای پرن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به شاخ گل نو، درآویخت باد</p></div>
<div class="m2"><p>بدریدش آن ایزدی پیرهن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برهنه شد و شرمش اندر گرفت</p></div>
<div class="m2"><p>رخش سرخ شد برسرانجمن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خزیده در آغوش سرو بلند</p></div>
<div class="m2"><p>به شوخی‌، ستاک گل نسترن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو دوشیزه‌ای سرخ کرده رخان</p></div>
<div class="m2"><p>به پیچیده بر عاشق خویشتن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر آن شمعدانی نگر کش بود</p></div>
<div class="m2"><p>زپیروزه شمع و ز مرجان لگن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>میان لگن شمع مانده خموش</p></div>
<div class="m2"><p>لگن تافته چون سهیل یمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بپوشد همی کوهسار کبود</p></div>
<div class="m2"><p>به ابر سیه شامگاهان بدن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر او بروزد شهریاری هبوب</p></div>
<div class="m2"><p>خروشان شود ابر ژاله فکن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بجنبد همی کهربایی درخش</p></div>
<div class="m2"><p>بغرد همی تندر بانگ‌زن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو گویی خروش زمانه است این</p></div>
<div class="m2"><p>ز جنبیدن تیغ شاه زمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهاندار نادر شه تیزچنگ</p></div>
<div class="m2"><p>خدیو عدو بند لشکرشکن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به کردارهای گزین مشتهر</p></div>
<div class="m2"><p>به پیکارهای قوی مفتتن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نه پهلوی او سیر دیده دواج</p></div>
<div class="m2"><p>نه چشمان او سیر دیده وسن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چولشکربخسبید خسبد ملک</p></div>
<div class="m2"><p>نهاده تبرزین به زیر ذقن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زگردان جز اوکیست کاندر وغا</p></div>
<div class="m2"><p>برد حمله باگرزهٔ‌پنج من</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز شاهان جز او کیست کز موزه‌اش</p></div>
<div class="m2"><p>دمد جو، ز ناسودن و تاختن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به رکضت بود پیش تاز سپاه</p></div>
<div class="m2"><p>به فترت رود پیش باز فتن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو دریا، دلی در برش مختفی</p></div>
<div class="m2"><p>جهان‌جوی عزمی درو مختزن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در آن تیره عهدی کز افغان و روم</p></div>
<div class="m2"><p>در ایران فغان خاست از مرد و زن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ببرد از ارس تا به مازندران</p></div>
<div class="m2"><p>سپاه «‌اورس‌» چون یکی راهزن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خراسان ز محمود شد تار و مار</p></div>
<div class="m2"><p>گشن لشگری کرد او انجمن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز یک سو به کف کرده توران سپاه</p></div>
<div class="m2"><p>ز آمویه تا رودبار تجن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شده پادشه کشته در اصفهان</p></div>
<div class="m2"><p>شه نو به درد و بلا مفترن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در این ساعت ازکوهسارکلات</p></div>
<div class="m2"><p>برآمد یکی نعرهٔ کوهکن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فرشته فرود آمد از آسمان</p></div>
<div class="m2"><p>گرفته عنان یکی پیلتن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پس پشت او لشگری شیردل</p></div>
<div class="m2"><p>همه آهنین چنگ و روئینه تن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فرشته عنانش رهاکرد وگفت</p></div>
<div class="m2"><p>به نام ایزد ای نادر ممتحن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برو کت نه‌بینیم هرگز حزین</p></div>
<div class="m2"><p>بچم کت مبیناد هرگز حزن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به یک رکضت اینک خراسان بگیر</p></div>
<div class="m2"><p>سپس بر سپاه سپاهان بزن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به ترکان یکی حمله آورگران</p></div>
<div class="m2"><p>به خونخواهی رزمگاه پشن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ترا گفت یزدان که بستان خراج</p></div>
<div class="m2"><p>ز شام و حلب تاختا و ختن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نه پیچید صاحبقران بزرگ</p></div>
<div class="m2"><p>ز پیمان افرشتهٔ مؤتمن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بکوشید و پیکارها کرد صعب</p></div>
<div class="m2"><p>ز بیگانگان کرد صافی وطن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به دنبال افغان سوی قندهار</p></div>
<div class="m2"><p>شد و کرد بنگاهشان مرغزن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شنید آن که دارای دهلی کند</p></div>
<div class="m2"><p>از افغان حمایت به سر و علن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از این‌رو پی دفع آنان کشید</p></div>
<div class="m2"><p>به غزنین و کابل سپاهی کشن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به دهلی بریدی فرستاد و راند</p></div>
<div class="m2"><p>سخن زان گروه گسسته رسن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که اینان گروهی خیانتگرند</p></div>
<div class="m2"><p>ستم کرده بر خاندانی کهن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همه خونی و دزد و بی‌دولتند</p></div>
<div class="m2"><p>ندارند چندان بها و ثمن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>که دارای دهلی دهدشان به مهر</p></div>
<div class="m2"><p>پناه و، نگهدارد از خشم من</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بدان نامه‌ها پاسخی شاه هند</p></div>
<div class="m2"><p>نداد و برافزود بر سوء ظن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به ره بر بکشتند ده تن رسول</p></div>
<div class="m2"><p>به دهلی ببستند هم چند تن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ندیدند فرجام آن کار زشت</p></div>
<div class="m2"><p>کشان چشم بربسته بود اهرمن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>توگفتی بنازند از آن تنگ سخت</p></div>
<div class="m2"><p>که خیبر بود نامش اندر زمن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ندانست کان چنگ خیبر گشای</p></div>
<div class="m2"><p>کند تنگ خیبر تلال و دمن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شهنشه سوی تنگ خیبر کشید</p></div>
<div class="m2"><p>به راهی کزان دیو جستی به فن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دوکوه از دو سو سرکشیده به میغ</p></div>
<div class="m2"><p>میان رو دو راه از لب آبگن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>رهی چون ره مورچه بر درخت</p></div>
<div class="m2"><p>نشیب و فرازش شکن در شکن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به تنگ اندرون صوبه‌داران هند</p></div>
<div class="m2"><p>کمین کرده با لشکری تیغ‌زن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز افغان و هندی و پیشاوری</p></div>
<div class="m2"><p>تنیده بهر گوشه‌، چون کارتن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همه نیزه‌دار و گروهه گذار</p></div>
<div class="m2"><p>همه ناوک‌انداز و زوبین‌فکن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شهنشه بغرید و افکند رخش</p></div>
<div class="m2"><p>چو در رزم هاماوران‌، تهمتن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>فرو ریختند از تف قهر شاه</p></div>
<div class="m2"><p>چو پوسیده کاخ از تف بومهن</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو شاهنشه از تنگ خیبر گذشت</p></div>
<div class="m2"><p>ز دهلی عزا خانه شد تا دکن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به خیبر عزا خاست چون کنده شد</p></div>
<div class="m2"><p>در خیبر از بازوی بوالحسن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سپه را به پیشاور اندر گذاشت</p></div>
<div class="m2"><p>ازو کشته پنجاب بیت‌الحزن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به لاهور در، صوبه داری که بود</p></div>
<div class="m2"><p>به برگشتگی طالعش مرتهن</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دمان بر لب آب «‌زاوی‌» گرفت</p></div>
<div class="m2"><p>سر ره بر آن سیل بنیاد کن</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز یک حملهٔ لشگر شهریار</p></div>
<div class="m2"><p>بجست و امان خواست چون بیوه‌زن</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز پنجاب خسرو به دهلی شتافت</p></div>
<div class="m2"><p>مدد خواسته ز ایزد ذوالمنن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به خسرو ز دهلی رسید آگهی</p></div>
<div class="m2"><p>که دشمن بود جفت رنج و محن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز دهلی سپه برکشید و نشست</p></div>
<div class="m2"><p>به «کرنال‌» چون اشتر اندر عطن</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بگرد اندرش مرد سیصدهزار</p></div>
<div class="m2"><p>ز ترکان و از مردم برهمن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بگرد سپه توپ‌های بزرگ</p></div>
<div class="m2"><p>رده بسته چون باره‌ای از چدن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ازبن مژده خسرو چنان راند تفت</p></div>
<div class="m2"><p>که تازد سوی حجله زیبا ختن</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سپیده سپه برگرفت و رسید</p></div>
<div class="m2"><p>نماز دگر بر سر انجمن</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ز لشگر جهان دید یکسر سیاه</p></div>
<div class="m2"><p>به پولاد آکنده دشت و دمن</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>زیک سو صف پیل جنگی چنانک</p></div>
<div class="m2"><p>تناور درختی ز آهن غصن</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ز یک‌سو صف توپ کهسارکوب</p></div>
<div class="m2"><p>به اوبار جان برگشاده دهن</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نیاسوده از ره برانگیخت اسب</p></div>
<div class="m2"><p>به چنگ اندرش گرز خارا شکن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بجوشید هندی چو مور و ملخ</p></div>
<div class="m2"><p>برآورد آوا چو زاغ و زغن</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ولی شه فرو خورد و کردش خموش</p></div>
<div class="m2"><p>توگفتی چراغی است بر بادخن</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به یک ساعت از خون هندی سپاه</p></div>
<div class="m2"><p>زمین لعل شد چوعقیق یمن</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>پس از ساعتی جنگ زنهار خواست</p></div>
<div class="m2"><p>محمد شه از خسرو ممتحن</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>شهش داد زنهار و بنواختش</p></div>
<div class="m2"><p>پذیره شدش در بر خویشتن</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>پس از جنگ «کرنال‌» شد با سپاه</p></div>
<div class="m2"><p>به دهلی‌، شهنشاه والا سنن</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>به دهلی شبانگه عیان گشت عذر</p></div>
<div class="m2"><p>ز ترکان و از پیروان وثن</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بکشتند برخی از ایران سپاه</p></div>
<div class="m2"><p>که اندر سراها گزیده وطن</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>دگر روز از هیبت قهر شاه</p></div>
<div class="m2"><p>بسا سر که دور اوفتاد از بدن</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>نگه کن کزین پس شهنشه چه کرد</p></div>
<div class="m2"><p>ز مردی بر آن شاه دور از فطن</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بدو کشور و تاج بخشید و خویش</p></div>
<div class="m2"><p>به ایران گرایید بی‌لا ولن</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>فری آن تن سخت و عزم درست</p></div>
<div class="m2"><p>فری آن دل پاک و خوی حسن</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>شها چون تو شاهی جوان‌بخت و راد</p></div>
<div class="m2"><p>ندید و نه‌بیند جهان کهن</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>گوارنده بادت هدایای هند</p></div>
<div class="m2"><p>ز تخت و ز تاج و ز تیغ و مجن</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ز یاقوت رخشان و الماس پاک</p></div>
<div class="m2"><p>ز لولوی عمان و در عدن</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>همان دیبه و گوهر و زر و سیم</p></div>
<div class="m2"><p>به تخت و به تنگ و به رطل‌ و به من</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>به ایران‌ زمین رحمت آورکه هست</p></div>
<div class="m2"><p>ز تو زنده چون شیرخوار از لبن</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>همان کس که در وقعت اصفهان</p></div>
<div class="m2"><p>شمیدی به پیش عدو چون شمن</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>کنون در رکاب تو از فر تو</p></div>
<div class="m2"><p>درد چرم بر پیل و بر کرگدن</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ستودمت نادیده بعد از دو قرن</p></div>
<div class="m2"><p>چو مر مصطفی را اویس‌ بس قرن</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ز بیداد گردون و جور جهان</p></div>
<div class="m2"><p>دلم گشته چون چشمهٔ پر وزن</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>نگفت و نگوبد کس از شاعران</p></div>
<div class="m2"><p>بهنجار این پهلوانی سخن</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>الا تا به نیسان نشید هزار</p></div>
<div class="m2"><p>به گوش آید از شاخهٔ نارون</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>قدت باد یازان چو سرو سهی</p></div>
<div class="m2"><p>رخت باد خرم چوبرک سمن</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بکوش و بتاز و بگیر و ببخش</p></div>
<div class="m2"><p>بپای و ببال و بنوش و بدن</p></div></div>