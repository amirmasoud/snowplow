---
title: >-
    شمارهٔ ۲۸ - تابستان
---
# شمارهٔ ۲۸ - تابستان

<div class="b" id="bn1"><div class="m1"><p>ای آفتاب مشکو زی باغ کن شتاب</p></div>
<div class="m2"><p>کز پشت شیر تافت دگرباره آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرداد ماه باغ به بار است گونه گون</p></div>
<div class="m2"><p>از بسد و زبرجد و لولوی دیریاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم شاخ راز میوه دگرگونه گشت چهر</p></div>
<div class="m2"><p>هم باغ را به جلوه دگرگونه شد ثئیاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنگر بدان گلابی آویخته ز شاخ</p></div>
<div class="m2"><p>چون بیضه‌های زرین پر شکر و گلاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیب سپید و سرخ به شاخ درخت بر</p></div>
<div class="m2"><p>گویی ز چلچراغ فروزان بود حباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا کاویان درفش است از باد مضطرب</p></div>
<div class="m2"><p>وان گونه گون گهرها تابان از اضطراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انگور لعل بینی از تاک سرنگون</p></div>
<div class="m2"><p>وان‌غژم‌هاش یک‌به‌دگر فربی‌ و خوشاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پستان مادریست فراوان سر اندرو</p></div>
<div class="m2"><p>و انباشته همه سرپستان به شهد ناب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک خوشه زردگونه به رنگ پر تذرو</p></div>
<div class="m2"><p>دیگر سیاه گونه به‌سان پرغراب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک رز چو اژدهایی پیچیده بر درخت</p></div>
<div class="m2"><p>یک رز چو پارسایی خمیده بر تراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک‌رزکشیده همچو طنابی و دست طبع</p></div>
<div class="m2"><p>دیبای رنگ رنگ فروهشته برطناب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک ‌رز نشسته ‌همچو یکی ‌زاهدی که ‌دست</p></div>
<div class="m2"><p>برداردی ز بهر دعاهای مستجاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وانک ز دست و گردنش آویخته بسی</p></div>
<div class="m2"><p>سبحهٔ رخام ودانه به‌هر سبحه بی‌حساب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باغست نار نمرود آنگه کجا رسید</p></div>
<div class="m2"><p>از بهر پور آزرش آن ایزدی خطاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن شعله‌ها بمرد و بیفسرد لیک نور</p></div>
<div class="m2"><p>اخگر بسی به شاخ درختان بود بتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روی شلیل شد به مثل چون رخ خلیل</p></div>
<div class="m2"><p>نیمی ز هول زرد و دگر سرخ از التهاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آلوی زرد چون رخ در باخته قمار</p></div>
<div class="m2"><p>شفرنگ سرخ چون رخ دریافته شراب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شفتالوی رسیده بناگوش کود کیست</p></div>
<div class="m2"><p>وان زردمو یکانش به صندل شده خضاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از خربزه است باغتره‌ پر عبیر تر</p></div>
<div class="m2"><p>وز هندوانه مشکو پربوی مشکناب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پالیز از آن یکی شده پرکوزه‌های شهد</p></div>
<div class="m2"><p>بستان ازین یکی شده پر زمردین قباب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زان کوزه‌های شهد برآید هلال چار</p></div>
<div class="m2"><p>زین زمردین قباب برآید دو آفتاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باید زدن به دامن کهسار خیمه زانک</p></div>
<div class="m2"><p>شد شهر ری چو کورهٔ آهنگران بتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زنبق ز صفر یافت چهل پایه ارتفاع</p></div>
<div class="m2"><p>گرما شناس را بین گر داری ارتیاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گنجشک ازین درخت نپرد بدان درخت</p></div>
<div class="m2"><p>کز تاب مهر گردد بی‌بابزن کباب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ماهی فرا نیاید از قعر آبدان</p></div>
<div class="m2"><p>کز نور آفتاب درافتد به تف و تاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تفتیده شد منازل چون منزل سقر</p></div>
<div class="m2"><p>خوشیده شد جداول چون جدول کتاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پالاونی‌ است گویی این ابر نیم‌شب</p></div>
<div class="m2"><p>کز وی همی بپالایند اخگر مذاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بایست تختخواب نهادن به طرف جوی</p></div>
<div class="m2"><p>وان کلهٔ‌ نگاربن بستن به تختخواب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یک‌سو نسیم صحرا یکسو هوای کوه</p></div>
<div class="m2"><p>یک‌سو نوای فاخته یک سو غریو آب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آوازهٔ هوام شبانگاه مر مرا</p></div>
<div class="m2"><p>آید به گوش خوبتر از بربط و رباب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وبژه که خفته سرخوش نزدیک آبشار</p></div>
<div class="m2"><p>پهلوی ماهرویی در نور ماهتاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اینست شرط عقل ولیکن بهار را</p></div>
<div class="m2"><p>این‌حال ییش چشم نیاید مگر به‌خواب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هم نیست خواب از آنکه درین سمج دوزخی</p></div>
<div class="m2"><p>بیدار بود بایدم از شدت عذاب</p></div></div>