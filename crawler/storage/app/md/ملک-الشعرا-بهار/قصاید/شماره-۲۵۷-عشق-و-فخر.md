---
title: >-
    شمارهٔ ۲۵۷ - عشق و فخر
---
# شمارهٔ ۲۵۷ - عشق و فخر

<div class="b" id="bn1"><div class="m1"><p>تا به چند اندر پی عشق مجازی‌؟</p></div>
<div class="m2"><p>چند با یار مجازی عشق‌بازی‌؟‌!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند گردی گرد اسرار حقیقت</p></div>
<div class="m2"><p>ای ندانسته حقیقی از مجازی‌؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برق عشقست این چه پوشیدش به خرمن</p></div>
<div class="m2"><p>خفته‌مار است‌این‌چه گیریدش به بازی‌؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پاکبازی کن چو راه عشق پوئی</p></div>
<div class="m2"><p>عشق‌بازی را بباید پاکبازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اینکه بینی در همه گیتی سمر شد</p></div>
<div class="m2"><p>عشق محمودی است نی حسن ایازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خم ابروی دل رخ نه که نبود</p></div>
<div class="m2"><p>هر خم ابروی محرابی نمازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برکش ازگردن‌فرازی سر، که ناگه</p></div>
<div class="m2"><p>سرنگونی بینی ازگردن‌فرازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ره تجرید زی لاهوتیان شو</p></div>
<div class="m2"><p>کاید از ناسوتیانت بی‌نیازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ره عشق و طلب بی‌خویشتن شو</p></div>
<div class="m2"><p>تا نشیبی را ندانی از فرازی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بهار از شاهد معنی سخن گو</p></div>
<div class="m2"><p>نز بت نوشادی و ترک طرازی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاهباز ساعد سلطان عشقم</p></div>
<div class="m2"><p>چون کنم با هر تذرو وکبک بازی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در دبستان ازل بنهادم ازکف</p></div>
<div class="m2"><p>دفتر نیرنگ و درس حیله سازی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زبن کلام پارسی گویند بر من</p></div>
<div class="m2"><p>آنچه گفتند اندر آن کفتار تازی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عیب دیبا گوید آن مردک ولیکن</p></div>
<div class="m2"><p>عیب خود بیند گه دیبا طرازی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه نازد بر ستوری ژنده پالان</p></div>
<div class="m2"><p>چون کند با حملهٔ مردان غازی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خصم من خرد است وآری خرد دارد</p></div>
<div class="m2"><p>صعوه را اندیشهٔ چنگال بازی</p></div></div>