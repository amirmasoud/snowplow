---
title: >-
    شمارهٔ ۴۵ - که ورزندگی مایهٔ زندگی‌ است
---
# شمارهٔ ۴۵ - که ورزندگی مایهٔ زندگی‌ است

<div class="b" id="bn1"><div class="m1"><p>تن زنده والا به ورزندگی است</p></div>
<div class="m2"><p>که ورزندگی مایهٔ زندگی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ورزش گرای وسرافراز باش</p></div>
<div class="m2"><p>که فرجام سستی سرافکندگی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سختی دهد مرد آزاده تن</p></div>
<div class="m2"><p>که پایان تن‌پروری بندگی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلی بایدت روشن و تن‌درست</p></div>
<div class="m2"><p>اگر جانت جویای فرخندگی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی کاو توانا شد و تندرست</p></div>
<div class="m2"><p>خرد را به مغزش فرو زندگی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنر جوی تا کام‌یابی و ناز</p></div>
<div class="m2"><p>که جویندگی راه یابندگی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ورزش میاسای و کوشنده باش</p></div>
<div class="m2"><p>که بنیاد گیتی به کوشندگی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درخشیدن این بلند آفتاب</p></div>
<div class="m2"><p>ز بسیار کوشی و گردندگی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیاکانت را ورزش آن مایه داد</p></div>
<div class="m2"><p>که شهنامه زایشان به تابندگی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو نیز از نیاکان بیاموزکار</p></div>
<div class="m2"><p>اگر در سرت شور سرزندگی است</p></div></div>