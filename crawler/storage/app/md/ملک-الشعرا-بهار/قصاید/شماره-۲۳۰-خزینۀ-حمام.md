---
title: >-
    شمارهٔ ۲۳۰ - خزینۀ حمام
---
# شمارهٔ ۲۳۰ - خزینۀ حمام

<div class="b" id="bn1"><div class="m1"><p>افتاد به حمام‌، رهم سوی خزینه</p></div>
<div class="m2"><p>ترکید کدوی سرم از بوی خزینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من توی خزینه نروم هیچ و ز بیرون</p></div>
<div class="m2"><p>مبهوت‌ شوم‌ چون نگرم‌ سوی‌ خزینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کاسهٔ «‌بزقرمهٔ‌» پرقرمهٔ کم‌آب</p></div>
<div class="m2"><p>پر آدم و کم‌آب بود توی خزبنه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه آبی و گه سبز شود چون پرطاوس</p></div>
<div class="m2"><p>آن موج لطیفی که بود روی خزینه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر کودک بی‌مو ز خزینه بدر آید</p></div>
<div class="m2"><p>پرپشم شود پیکرش از موی خزینه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موی بدن و چرک و حنا و کف صابون</p></div>
<div class="m2"><p>آبیست که جاری بود از جوی خزینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون جمجمهٔ مردهٔ سی روزه دهد بوی</p></div>
<div class="m2"><p>آن خوی که چکد از خم ابروی خزینه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرگین گرو از عطر برد، گر بگشاید</p></div>
<div class="m2"><p>عطار سپس دکه به پهلوی خزینه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با جبههٔ پرچین و لب عربده‌جویش</p></div>
<div class="m2"><p>گرم و تر و چسبنده بود خوی خزینه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از لای کش احوال دل خستهٔ او پرس</p></div>
<div class="m2"><p>چون رنگ طبیعی پرد از روی خزینه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیکر شودش زرد به رنگ مگس نحل</p></div>
<div class="m2"><p>هرکس که برون رفت ز کندوی خزینه</p></div></div>