---
title: >-
    شمارهٔ ۱۲۷ - مرگ تزار
---
# شمارهٔ ۱۲۷ - مرگ تزار

<div class="b" id="bn1"><div class="m1"><p>خمش مباش کنون کامد ای بهار، بهار</p></div>
<div class="m2"><p>سخن زلعبت چین وبت بهار، به آر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بی‌حقیقتی چرخ و بیوفایی دهر</p></div>
<div class="m2"><p>هزاردستان زد در میان باغ‌، هزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه گفت‌؟ گفت جهان رهزنی حرام‌خورست</p></div>
<div class="m2"><p>تو سر به عشوهٔ دهر حرام‌خوار، مخار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمانه کشت ترا نارسیده می‌درود</p></div>
<div class="m2"><p>مکار تخم امل‌، در زمین این مکار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز لعب دور قمر روشنی مدار طمع</p></div>
<div class="m2"><p>که بر محک‌، سیه آمد عیار این عیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه رزم‌هاکه بود پرقتال ازبن قتال</p></div>
<div class="m2"><p>چه قلب‌ها که بود داغدار ازین غدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سال‌ها دهد و بازگیرد اندر دم</p></div>
<div class="m2"><p>نهان به پرورد و سازد آشکار، شکار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشان عاطفت از دهر کینه‌جوی‌، مجوی</p></div>
<div class="m2"><p>امید راستی از چرخ کجمدار، مدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بحر جان اوبارش کسی ار خلاصی جست</p></div>
<div class="m2"><p>نهنگ بر سر او بارد ابر جان اوبار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه شه شناسدگیتی ونی وزیر، تو شو</p></div>
<div class="m2"><p>ز هر در آر پیاده‌، ز هر سو آر، سوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به کار دولت نتوان گزافه کاری کرد</p></div>
<div class="m2"><p>که از دولت شود آخرگزافه کار، فکار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز رزم خوار شمردن‌، ترا رسدکه رسید</p></div>
<div class="m2"><p>ز خصم بر شه خوارزم و والی اترار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مبین به مردم‌خوار و زبون‌، به خواری ازآنک</p></div>
<div class="m2"><p>به کینه مردم‌خوارند، گرگ مردم‌خوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مبین تو زار و زبون مردمان غوغا را</p></div>
<div class="m2"><p>که رزمجو‌یی غوغا بکشت زار، تزار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نقاط مسکو و پطر، از تزار برگشتند</p></div>
<div class="m2"><p>دو نقطه چون که یکی کشت شد تزار نزار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به باد، اصل و تبار و قتیل‌، نسل و نتاج</p></div>
<div class="m2"><p>نه‌تاج ماند و نه‌تخت و نه صفه ماند و نه بار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دو مار بودند آری تزار و فرزندش</p></div>
<div class="m2"><p>زمانه بین که برآورد از این دو مار، دمار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سفیه محتسبانی کجا ز جهل و خری</p></div>
<div class="m2"><p>خرند بی‌سبب‌، آزار مردم بازار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو جار دانش و داد آن زمان زنی که شوی</p></div>
<div class="m2"><p>امین خرمن فلاح و دفتر تجار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زکارهای عموم آنچه را نخواست عوام</p></div>
<div class="m2"><p>به فتوی خرد آن کار، ناصواب انگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کسی که دشمنی عامه را خرید به عمد</p></div>
<div class="m2"><p>قماش عار و لباس عوار کرد شعار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نکرد بایدکاری‌، که مردم عامه</p></div>
<div class="m2"><p>رهاکند پی کار و دود سوی پیکار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل رعیت گنجست و جهل مار وبست</p></div>
<div class="m2"><p>توگنج خواهی‌، همت به مرگ مارگمار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به مذهب و ذهب او مدار کار، ولیک</p></div>
<div class="m2"><p>درون مدرسه‌اش با کتاب و کار، بکار</p></div></div>