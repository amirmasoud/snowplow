---
title: >-
    شمارهٔ ۱۸۲ - فوج آهن
---
# شمارهٔ ۱۸۲ - فوج آهن

<div class="b" id="bn1"><div class="m1"><p>چون بدرید صبح پیراهن</p></div>
<div class="m2"><p>جلوه گر گشت فوجی از آهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپهی کز نهیب نیزهٔ او</p></div>
<div class="m2"><p>بردرد چرخ پیر پیراهن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لشگری کانعطاف خنجر وی</p></div>
<div class="m2"><p>بگسلاند ز کهکشان جوشن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون‌ برآید غریو، ‌روز نبرد</p></div>
<div class="m2"><p>فوج‌ آهن ‌به جنبش آرد تن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آهنین قلعه‌ای بود جنبان</p></div>
<div class="m2"><p>نه بر او در پدید و نی روزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیر بارد چنان که بر پرد</p></div>
<div class="m2"><p>آهن ذوب گشته از معدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بمب کوبد، چنان که درغلطد</p></div>
<div class="m2"><p>سنگ خارا ز قله در دامن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میغی از تیغ برکشد که از آن</p></div>
<div class="m2"><p>مرگ بارد به تارک دشمن</p></div></div>