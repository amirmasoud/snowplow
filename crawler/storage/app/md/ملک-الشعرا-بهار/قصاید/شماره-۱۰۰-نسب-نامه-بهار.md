---
title: >-
    شمارهٔ ۱۰۰ - نسب‌نامهٔ بهار
---
# شمارهٔ ۱۰۰ - نسب‌نامهٔ بهار

<div class="b" id="bn1"><div class="m1"><p>قطعه‌ای قلم پرتو بیضایی بود</p></div>
<div class="m2"><p>پرتو معنی و لفظش ید بیضایی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حب و بغض از پدران ارث به فرزند رسد</p></div>
<div class="m2"><p>مهر پرتو به من اجدادی و آبایی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچنین بود ز میراث نیاکان بی‌شک</p></div>
<div class="m2"><p>آن محبت که ز من در دل بیضایی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راست گویی که میان پدران من و او</p></div>
<div class="m2"><p>متصل سلسلهٔ انس و شناسایی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوستی بی‌سبب آن روز عجب بود بلی</p></div>
<div class="m2"><p>دور دوران تبهکاری و خودرایی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ویژه بین دو سخنگوی که از روز نخست</p></div>
<div class="m2"><p>کار هم‌چشمی این قوم تماشایی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با چنین حال‌، رهی را پدرت دوست گرفت</p></div>
<div class="m2"><p>که دلش پاک ز لوث منی و مایی بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من هم او را ز گروه شعرا بگزیدم</p></div>
<div class="m2"><p>که چون من نیز وی از مردم دریایی بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود او نیز چو من در وطن خویش غریب</p></div>
<div class="m2"><p>با غریبانش از آن شفقت و مولایی بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود او معتقد دلشدگان شیدا</p></div>
<div class="m2"><p>که خداوند دلی واله و شیدایی بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر به کنج قفس افتاد عجب نیست که او</p></div>
<div class="m2"><p>عندلیب‌آسا محکوم خوش‌آوایی بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود مسعود زمان آن که به شومی ادب</p></div>
<div class="m2"><p>گه‌ (‌مرنجی‌) و گهی‌(‌سوبی‌) و گه‌ (‌نایی‌) بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرتوا رحمت حق بر پدری کز پس او</p></div>
<div class="m2"><p>چون تو فرزند خلف در شرف افزایی بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفتی از نسب کاشان چه‌زنی تن که پدرت</p></div>
<div class="m2"><p>بود ازبن شهر که مشهور به گویایی بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>راست گفتی و من از راست نرنجم لیکن</p></div>
<div class="m2"><p>چه توان کرد که در طوسم پیدایی بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طوس و کاشان به قیاس نسب دودهٔ ما</p></div>
<div class="m2"><p>نسب صورت با جسم هیولایی بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مولدم طوس ولیکن گهر از کاشان است</p></div>
<div class="m2"><p>نغمه آمد ز نی اما هنر از نایی بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جد من هست صبور آن که به کاشان او را</p></div>
<div class="m2"><p>با عم خوبش صبا دعوی همتایی بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می‌رسد از پس سی پشت به آل برمک</p></div>
<div class="m2"><p>وین نسب آن ‌روز اسباب‌ خودآرایی بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نایب‌السلطنه را بود دبیر مخصوص</p></div>
<div class="m2"><p>زان که شیرین خط او شهره به زیبایی بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با چنین حال شد اندر صف پیکار و جهاد</p></div>
<div class="m2"><p>که وطن دستخوش دشمن یغمایی بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در صف رزم شد از غیرت اسلام شهید</p></div>
<div class="m2"><p>زانکه با طبع غیور و سر سودایی بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سومین جد من از کاشان بشتافت به فین</p></div>
<div class="m2"><p>زانکه بی‌بهره از آلایش دنیایی بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دومین جد من آمد به خراسان از کاش</p></div>
<div class="m2"><p>کاندر این مرحله‌اش بویهٔ عقبایی بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کار دنیاش به سامان شد از آن روی که او</p></div>
<div class="m2"><p>صاحب کارگه مخمل و دارایی بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پسرانش همه صنعتگر و فرزند کهین</p></div>
<div class="m2"><p>کاظمش نام و به دل طالب دانایی بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به تقاضای نسب گشت صبوربش لقب</p></div>
<div class="m2"><p>طوطیئی گشت که شهره به‌ شکرخایی بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شیوهٔ شاعریش کرد (‌خجسته‌) تلقین</p></div>
<div class="m2"><p>آنکه‌شعرن به‌جهال شهره به‌شیوایی بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شد رئیس‌الشعرا پس ملکی یافت به ‌شعر</p></div>
<div class="m2"><p>وز شهش را تبه هم ز اول برنایی بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باد آباد مهین خطهٔ کاشان که مدام</p></div>
<div class="m2"><p>مهد هوش و خرد و صنعت و بینایی بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هرکه برخاست بهر پیشه ز شهر کاشان</p></div>
<div class="m2"><p>در فن خویشتنش فرط توانایی بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>معنی کاش جمیلست و ظریفست و ازو است</p></div>
<div class="m2"><p>لغت کشی‌، کش معنی رعنایی بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کش و کشمیر و دگر کاشمر و کاشغر است</p></div>
<div class="m2"><p>جای‌هایی که عبادتگه بودایی بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لفظ کاشانه وکاشان به لغت‌های قدیم</p></div>
<div class="m2"><p>معبد و جایگه جشن و دل آسایی بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آجر وکاسهٔ رنگین راکاشی خواندند</p></div>
<div class="m2"><p>وین هم از نقش خوش و لون تماشایی بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>لغت کاسه وکاسست هم ازکاشه وکاش</p></div>
<div class="m2"><p>زانکه پرنقش گل و بوتهٔ مینایی بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در تمنای خوشی نیز بگویند: ای کاش</p></div>
<div class="m2"><p>در فراق توام آرام و شکیبایی بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>صنعت کاشی از اینجا به دگر جای رسید</p></div>
<div class="m2"><p>کاین هنر وبژه این شهر به تنهایی بود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فرش زیباش کنون شهرهٔ دهر است چنانک</p></div>
<div class="m2"><p>زری و مخمل او شاهد هرجایی بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وز ولای علیش فخر فزونست بلی</p></div>
<div class="m2"><p>مردم کاشان پیوسته توّلایی بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>صورت و صوت نکو را هم از ایام قدیم</p></div>
<div class="m2"><p>با نبی‌القاسان همدوشی و دربائی بود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مردمش را ز هوای خوش و انفاس لطیف</p></div>
<div class="m2"><p>صوت داودی و الحان نکیسایی بود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گویی‌این نغمهٔ ‌خوش باعث ‌تعدیل وی ‌است</p></div>
<div class="m2"><p>ورنه آن لهجهٔ بد، مایهٔ رسوایی بود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یا خود این لهجهٔ ‌ناخوش سپر چشم بد است</p></div>
<div class="m2"><p>پیش شهری که پر از خوبی و زیبایی بود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>صد هنر دارد و یک عیب‌، من این زان گفتم</p></div>
<div class="m2"><p>تا نگویند که قصدم هنرآرایی بود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گفت این چامهٔ جانبخش به نوروز بهار</p></div>
<div class="m2"><p>گرچه افسرده دل از عزلت و تنهایی بود</p></div></div>