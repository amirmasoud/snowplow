---
title: >-
    شمارهٔ ۲۱۵ - شیراز
---
# شمارهٔ ۲۱۵ - شیراز

<div class="b" id="bn1"><div class="m1"><p>شد پارس یکی حلقهٔ گزین</p></div>
<div class="m2"><p>شیراز بر آن حلقه چون نگین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر حلقهٔ انگشترین پارس</p></div>
<div class="m2"><p>شیراز بود گوهری ثمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سبزهٔ شاداب و سرخ گل</p></div>
<div class="m2"><p>گه یاقوتین‌، گه زمردین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز به یک انگشتری که دید</p></div>
<div class="m2"><p>یاقوت و زمرد به هم قرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چین و شکنج گلشن به باغ</p></div>
<div class="m2"><p>چون برگذرد باد فرودین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد چین و شکنج افکند نسیم</p></div>
<div class="m2"><p>از رشک‌، برابر وی مشک چین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زی بقعت کوهی یکی برای</p></div>
<div class="m2"><p>کان بقعه بهبشی بود برین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر ساحت بردی ببر سلام‌</p></div>
<div class="m2"><p>کآن برد و سلامیست دلنشین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز چشمهٔ زنگیش نوش کن</p></div>
<div class="m2"><p>تا شکر نوشی و انگبین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگذر سحری زی سه آسیا</p></div>
<div class="m2"><p>زآنجا به کُه خور بر آببین‌</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کز عکس گل و لاله و سمن</p></div>
<div class="m2"><p>شرمنده برآید خور از زمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر مسجد ویران عمرولیث</p></div>
<div class="m2"><p>رخ‌سای که پیریست بافرین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رخ سای بر آن فرخ آستان</p></div>
<div class="m2"><p>بزدای ازو گرد باستین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قرآن‌کده‌اش را درون صحن</p></div>
<div class="m2"><p>با دیدهٔ قرآن‌شناس بین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر مرقد سعدی بسای چهر</p></div>
<div class="m2"><p>بر تربت خواجو بنه جبین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کن یاد سرکوی شاه شیخ</p></div>
<div class="m2"><p>از بهر دل حافظ غمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شو تربت خونینش را بجوی</p></div>
<div class="m2"><p>وآن خاروخس از تربتش بچین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن دولت مستعجلش نگر</p></div>
<div class="m2"><p>بر تربت وبرانه‌اش نشین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جو تربت منصور شاه را</p></div>
<div class="m2"><p>مقتول سمرقندی لعین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر تربت او یافتی‌، بروب</p></div>
<div class="m2"><p>خاک رهش از زلف حور عین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر مدفن شه شیخ برنویس</p></div>
<div class="m2"><p>کاین مدفن شاهیست راستین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر تربت منصور بر نگار</p></div>
<div class="m2"><p>کاین تربت شیریست‌ خشمگین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زانجا به سوی حافظیه شو</p></div>
<div class="m2"><p>زان خطه کفی خاک برگزین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>و آن خاک به‌ سر کن که ای دریغ</p></div>
<div class="m2"><p>کو حافظ و آن طبع دلنشین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زی تربت خواجو برای هان</p></div>
<div class="m2"><p>بر مدفن اهلی بنال هین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر مرقد بسحاق کن گذر</p></div>
<div class="m2"><p>ربزهٔ هنر از خاک او بچین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر خواجهٔ «‌داهدار» ده درود</p></div>
<div class="m2"><p>همت طلب از خاک آن زمین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در مسجد بردی ز مکتبی‌</p></div>
<div class="m2"><p>بنیوش سخن‌های نازنین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جو توشهٔ راه از شه چراغ</p></div>
<div class="m2"><p>کانجا دو جهان بنگری دفین</p></div></div>