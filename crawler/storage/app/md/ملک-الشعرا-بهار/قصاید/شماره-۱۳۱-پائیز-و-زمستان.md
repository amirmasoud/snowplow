---
title: >-
    شمارهٔ ۱۳۱ - پائیز و زمستان
---
# شمارهٔ ۱۳۱ - پائیز و زمستان

<div class="b" id="bn1"><div class="m1"><p>روان شد لشگر آبان به طرف جوببار اندر</p></div>
<div class="m2"><p>نهاده سیمگون رایت به کتف کوهسار اندر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهان شد دامن البرز در میغ و بخار اندر</p></div>
<div class="m2"><p>تو گویی گرد کُه بستند پولادین حصار اندر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بر بستان کفن پوشید برف تندبار اندر</p></div>
<div class="m2"><p>درخت سرو بر تن کرد رخت سوگوار اندر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درختان لرز لرزان در میان جویبار اندر</p></div>
<div class="m2"><p>به پای هر درختی برگ‌ها گشته نثار اندر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خزانی برگ، هرسو توده چون زر عیار اندر</p></div>
<div class="m2"><p>دمنده باد، همچون صیرفی وقت شمار اندر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بتابد خور ز بالا بر زمین زرد و نزار اندر</p></div>
<div class="m2"><p>چو زربن مغفر جنگی بهیجا از غبار اندر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر جویی یکی آیینه بنهاده به کار اندر</p></div>
<div class="m2"><p>غرابان بر سر آیینه چون آیینه‌دار اندر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شود باد خنک هر شب به بستان گرم کار اندر</p></div>
<div class="m2"><p>به جسم آبدان پوشد سلیحی آبدار اندر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فسرده غنچه‌ها گشته نگون بر شاخسار اندر</p></div>
<div class="m2"><p>توگویی لعبتان گشتند آویزه به دار اندر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن وادی که خوید آمد به زانوی سوار اندر</p></div>
<div class="m2"><p>کنون‌ جز خشک ‌خاری نیست فرش‌ رهگذار اندر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هرجا لشگر زاغان فرود آرند بار اندر</p></div>
<div class="m2"><p>فروبندد جلب‌شان بند بر پای هزار اندر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به باغ آیند زاغان شام گاهان صد هزار اندر</p></div>
<div class="m2"><p>درافکنده با بر تیره بانگ غارغار اندر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرود آیند ناگاهان به بالای چنار اندر</p></div>
<div class="m2"><p>چنار بی‌بر از ایشان ز نو آید به بار اندر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر هر شاخ پنداری بیندوده بقار اندر</p></div>
<div class="m2"><p>ز هیبت‌شان به باغ‌، از باغ بگریزد هزار اندر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چمد رنگ از کمرگاهان به شیب رودبار اندر</p></div>
<div class="m2"><p>پرد کبک دری از تیغه سوی آبشار اندر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پلنگ از قله زی دامن شود بهر شکار اندر</p></div>
<div class="m2"><p>شبان مر گوسپندان را کند پنهان بغار اندر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به برف افتد نشان پای گرگان بی‌شمار اندر</p></div>
<div class="m2"><p>به‌بازی جسته درهم پنج‌ پنج و چار چار اندر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بوادی‌ها درون خرگوشکان جسته قرار اندر</p></div>
<div class="m2"><p>نهفته تن به زبر خاربن عیاروار اندر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نهاده برکتف دو گوش و خفته زیر خار اندر</p></div>
<div class="m2"><p>گشاده چشم‌ها همچون دو لعل شاهوار اندر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به سویش ره برد تازی چو عاشق سوی یار اندر</p></div>
<div class="m2"><p>ز خفتنگه برآهنجدش و افتدگیر و دار اندر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدا بیچاره مسکینی به بدخواهان دچار اندر</p></div>
<div class="m2"><p>به قصدش مرگ بگشاده کمین از هرکنار اندر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدین معنی یکی بنگر به احوال دیار اندر</p></div>
<div class="m2"><p>درافتاده به چنگ دشمنانی دیوسار اندر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو گویی مرگ بگشاده به ایرانشهر، بار اندر</p></div>
<div class="m2"><p>به جان کشور افتاده گروهی گرگوار اندر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به دلشان هیچ ناجسته وفا و مهر بار اندر</p></div>
<div class="m2"><p>توگویی کینهٔ دیرین به دل دارند بار اندر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دربغا کشور ایران بدین احوال زار اندر</p></div>
<div class="m2"><p>دریغا آن دلیری‌ها به چندین روزگار اندر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه شد رستم که هرساعت به‌دشت کارزار اندر</p></div>
<div class="m2"><p>گرامی جان سپر کردی به پیش شهریار اندر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برگودرز یل هفتاد پور نامدار اندر</p></div>
<div class="m2"><p>به میدان داده جان هریک به عز و افتخار اندر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ببین زی داریوش آن خسرو با اقتدار اندر</p></div>
<div class="m2"><p>به نقش بیستونش بین و آن والا شعار اندر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به ‌بیم است ‌از دروغی‌، چون به ‌شهری گرگ ‌هار اندر</p></div>
<div class="m2"><p>بخواهد کز دروغ ایران بماند برکنار اندر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کنون گر بیند ایران را بدین ایام تار اندر</p></div>
<div class="m2"><p>چکد خونابه‌اش از مژگان اشکبار اندر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدین کشور نه‌بینی جز گروهی نابکار اندر</p></div>
<div class="m2"><p>کزیشان جز دروغ و ملعنت ناید به بار اندر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>امید راستگویی نیست یاری را بیار اندر</p></div>
<div class="m2"><p>ز درویش و توانگر تا به شاه و شهریار اندر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شده گویی به ایرانشهر با عز و فخار اندر</p></div>
<div class="m2"><p>تبار اهرمن چیره به یزدانی تبار اندر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز بی‌برگی درافتاده به حال احتضار اندر</p></div>
<div class="m2"><p>جهانخواران به گرد او چو جوقی لاشخوار اندر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به ختم احتضار او نشسته به انتظار اندر</p></div>
<div class="m2"><p>کجا افتند در وی از یمین و از یسار اندر</p></div></div>