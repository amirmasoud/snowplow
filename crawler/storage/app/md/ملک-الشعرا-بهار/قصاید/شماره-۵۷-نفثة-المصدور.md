---
title: >-
    شمارهٔ ۵۷ - نفثة المصدور
---
# شمارهٔ ۵۷ - نفثة المصدور

<div class="b" id="bn1"><div class="m1"><p>فریاد ازین بئس‌المقر وین برزن پر دیو و دد</p></div>
<div class="m2"><p>این مهتران بی‌هنر وین خواجگان بی‌خرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهری برون پر هلهله وز اندرون چون مزبله</p></div>
<div class="m2"><p>افعی نهفته در سله کفچه فشرده در سبد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قومی به فطرت متکی نی احمدی نی مزدکی</p></div>
<div class="m2"><p>سر تافته در گبر کی از مسمغان و هیربد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتند دانایان مه‌، مه زاید از مه‌، که ز که</p></div>
<div class="m2"><p>از مردم به کار به‌، وز کشور بد، کار بد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی زین سراب خشم و کین شهد آید و ماء معین</p></div>
<div class="m2"><p>کندوش خالی ز انگبین و آکنده چاهش زانگژد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پیرهنشان اهرمن گشته نهان همباز تن</p></div>
<div class="m2"><p>زنده به دیو است این بدن این دیو هرگز کی مرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر خواجه محض آزمون چو‌ن اشتر مست حرون</p></div>
<div class="m2"><p>بر مردم خوار زبون نهمار پراند لگد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بستند مردم را زبان تا کس نداند رازشان</p></div>
<div class="m2"><p>چون شبروی کاندر نهان بر پای درپیچد نمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر یک به تاراج وطن دامن زده چون تهمتن</p></div>
<div class="m2"><p>وز دوکدان پیرزن برداشته سهم و رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در تیره‌جانشان اژدها در مغز سرشان دیو پا</p></div>
<div class="m2"><p>عفریتشان زیر قبا، ابلیسشان در کالبد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مست رعونت هر یکی سوده به کیوان تارکی</p></div>
<div class="m2"><p>وز کبر همچون بابکی بنشسته در ایوان بد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل گشته قیراندودشان اندرز ندهد سودشان</p></div>
<div class="m2"><p>وز تیغ زهرآلودشان خسته هزار اندرزپد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مردم از این مشتی گدا از مردم مانده جدا</p></div>
<div class="m2"><p>کو شعلهٔ قهر خدا کو آتش خشم یزد </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کو رادمردی بی‌نشان درکف پرندی خونفشان</p></div>
<div class="m2"><p>کاستاند از مردم کشان داد جوانمردان رد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برپا کند ناوردها دارو نهد بر دردها</p></div>
<div class="m2"><p>بر رغم این نامردها گردد به مردی نامزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کو آن مسیح پاک‌جان کاین گفته راند بر زبان‌:</p></div>
<div class="m2"><p>بر دیگری مپسند هان آن را که نپسندی به خود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دردی که زاد از استمی دارم ز شورش مرهمی</p></div>
<div class="m2"><p>آتشزنه گردد همی اطلس کجا کردید پد </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با جور و ظلم‌و خشم‌وکین‌شورش‌ پدید آید یقین</p></div>
<div class="m2"><p>با دی مه ‌آید پوستین با تیر ماه ‌آید شمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دردا که از شورشگر‌ی هستم به طبع اندر بری</p></div>
<div class="m2"><p>با پیری و با شاعری شورش‌پژوهی کی سزد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در ری بماندم پا به گل از سال سی تا سال چل</p></div>
<div class="m2"><p>زین یازده سالم به دل شد خون هشتاد و نود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دور از خراسان گزین در ری شدم عزلت گزین</p></div>
<div class="m2"><p>مویان چو چنگ رامتین نالان چو رود باربد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دارم به دل رنجی گران از یاد زشگ و عنبران</p></div>
<div class="m2"><p>صحرای طوس و طابران الگای پاز و فارمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن رودباران نزه از قلهک و طج‌رشت به</p></div>
<div class="m2"><p>نوغان درو شاهانه ده مایان و کنک و ترغبد </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در گرمسیرش راغ‌ها در آبدان‌ها ماغ‌ها</p></div>
<div class="m2"><p>در کاخ‌ها و باغ‌ها هم بادغد هم آبغد </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>طبع «‌وثوق‌» پاک‌ جان آن خواجهٔ بسیاردان</p></div>
<div class="m2"><p>شاید که این راز نهان بهتر نماید گوشزد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>الماس رومی برکند پولاد هندی سرکند</p></div>
<div class="m2"><p>بر صفحهٔ دفتر کند پیدا یکی کان بسد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>«‌بگذشت در حسرت مرا بس ماه‌ها و سال‌ها»</p></div>
<div class="m2"><p>تا مصرعی گویم کجا با مصرعش پهلو زند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کی هست یک را در نظر مانند صد قدر و خطر</p></div>
<div class="m2"><p>گرچه یک است اندر شمر همتا و همبالای صد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ظنم خطا شد راستی در طبعم آمد کاستی</p></div>
<div class="m2"><p>بگرفتم از شرم آستی در پیش رخسار خرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای‌خواجه ‌ز آصف ‌مشربی مستوه ازین مشتی غبی</p></div>
<div class="m2"><p>کی پور داود نبی‌، آید ستوه از دیو و دد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>غم نیست گر خصم از ریا گیرد خطا بر اتقیا</p></div>
<div class="m2"><p>گیرند زندیقان خطا بر قل هو الله احد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفتم علی‌رغم عدو در اقتفای شعر تو</p></div>
<div class="m2"><p>در یکهزار و سیصد و چار اندر اسفندار مد</p></div></div>