---
title: >-
    شمارهٔ ۵۵ - پیام شاعر
---
# شمارهٔ ۵۵ - پیام شاعر

<div class="b" id="bn1"><div class="m1"><p>طبع بلند مرا کیست که فرمان برد</p></div>
<div class="m2"><p>ز من پیامی بدان مردک کشخان برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوید یکچند باز جانب یزدان‌شناس</p></div>
<div class="m2"><p>بترس اگر داوریت کس بر یزدان برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توئی که از دیرگاه رای خطاکار تو</p></div>
<div class="m2"><p>آب نکوکاری از روی نیاکان برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نام سخن بردنت بالله ماند بدانک</p></div>
<div class="m2"><p>مردک شلغم‌فروش مشک به دکان برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرتو و چون تو زنند لاف خرد بعد از این</p></div>
<div class="m2"><p>کوت کش از هر کنار خرد به دامان برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه نداند ز جهل بوجهل از مصطفی</p></div>
<div class="m2"><p>گمرهم ار زانکه او ره سوی قرآن برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن کو بن‌سعد را بیش ز سلمان شمرد</p></div>
<div class="m2"><p>کافرم ار او به خویش نام مسلمان برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه نداند شناخت قیمت خرد از بزرگ</p></div>
<div class="m2"><p>چون به بر نام خویش نام بزرگان برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه ز بی‌دانشی نظم نداند ز نثر</p></div>
<div class="m2"><p>بهر چه نزدیک خلق عیب سخندان برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طیره شوی زانکه من مدح نگویم ترا</p></div>
<div class="m2"><p>هدیهٔ ایزد کسی در بر شیطان برد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز ابلهی گفته‌ای شعر نگوید بهار</p></div>
<div class="m2"><p>وین سخنان را بدو فلان و بهمان برد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به که ز بهر سخن برنگشاید زبان</p></div>
<div class="m2"><p>گر نتواند که مرد سخن به پایان برد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من ز دگر شاعران شعرگروگان برم</p></div>
<div class="m2"><p>اگر ز سرگین‌، عبیربوی گروگان برد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه تو گویی سخن گوید بر نام من</p></div>
<div class="m2"><p>کیست که کس نام او در بر اقران برد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کس را تعلیم شعر چون دهد آن کو ز جهل</p></div>
<div class="m2"><p>هنوز باید پدرش سوی دبستان برد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه هرکه گوید سخن نامش سخندان شود</p></div>
<div class="m2"><p>نه هرکه شد سوی بحر گوهر غلطان برد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هنوز در ملک شعر نامده قحط الرجال</p></div>
<div class="m2"><p>که خنثیئی روز جنگ رایت سلطان بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خود غلط است اینکه او شعر فرستد مرا</p></div>
<div class="m2"><p>خود غلطست اینکه کس قطره به عمان برد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خود چه کسنداین گروه وین‌سخنانشان که‌مرد</p></div>
<div class="m2"><p>در بر اهل سخن نامی از ایشان برد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کیست کز اینان مرا شعر فرستد به وام</p></div>
<div class="m2"><p>کیست که شمع و چراغ زی مه تابان برد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خرمن دانش مراست و آن دگران خوشه‌چین</p></div>
<div class="m2"><p>خوشه به خرمن کسی به تحفه نتوان برد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ذره بر آفتاب مردم جاهل نهد</p></div>
<div class="m2"><p>قطره سوی ژرف بحر کودک نادان برد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>طبع من از شاعران شعر کند عاریت</p></div>
<div class="m2"><p>لعل کس ار عاریت سوی بدخشان برد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فضل من از این گروه روشن گردد به خلق</p></div>
<div class="m2"><p>تیره بود گرنه تیغ محنت سوهان برد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کس نبرد فضل من زین سخنان گزاف</p></div>
<div class="m2"><p>دیو به افسون کجا ملک سلیمان برد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس از صبوری کنون منم که از طبع من</p></div>
<div class="m2"><p>قاعدهٔ نظم و نثر روان حسان برد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بخرد سالی مراست ترانه‌های بدیع</p></div>
<div class="m2"><p>که سالخورد اندرو دست به دندان برد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیخردی کان سخن گفت‌، بباید کنون</p></div>
<div class="m2"><p>دعوی خود را به خلق حجهٔ و برهان برد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ورنه چنانش کنم خستهٔ پیکان هجو</p></div>
<div class="m2"><p>که تا ابد بهر خویش دارو و درمان برد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یک ره از دامنش دست نگیرم فراز</p></div>
<div class="m2"><p>گرچه ز حشمت بساط برنهم ایوان برد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ناوک هجو من است بیلک خفتان گذار</p></div>
<div class="m2"><p>خصم چه سود ار به تن محنت خفتان برد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نشان دهم روز هجو او را روئی که او</p></div>
<div class="m2"><p>نیم‌شب از طوس رخت سوی صفاهان برد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>داوری ما و او باز دهد گر کسی</p></div>
<div class="m2"><p>قصه ما را سوی میر خراسان برد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دامن اگر برزند رای فروزان او</p></div>
<div class="m2"><p>اختر تابان چرخ سر به گریبان برد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نیزه بروید چو موی بر تن شیر دژم</p></div>
<div class="m2"><p>یکره گر خشم او ره به نیستان برد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تاکه جهانست باد شاد و خوش اندر جهان</p></div>
<div class="m2"><p>تاکه جهانش ز جان طاعت و فرمان برد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گفتم از آنسان که گفت شمع سپاهان جمال</p></div>
<div class="m2"><p>« کیست که پیغام من به شهر شروان برد»</p></div></div>