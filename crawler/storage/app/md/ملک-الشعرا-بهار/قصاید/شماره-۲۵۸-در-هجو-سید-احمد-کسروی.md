---
title: >-
    شمارهٔ ۲۵۸ - در هجو سید احمد کسروی
---
# شمارهٔ ۲۵۸ - در هجو سید احمد کسروی

<div class="b" id="bn1"><div class="m1"><p>کسروی تا راند درکشور سمند پارسی</p></div>
<div class="m2"><p>گشت مشکل فکرت مشکل‌پسند پارسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هفت اختر را ستارهٔ هفت گردان نام داد</p></div>
<div class="m2"><p>زان که‌خود بیگانه‌بود از چون‌وچند پارسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فکرت کوتاه و ذوق ناقصش راکی سزد</p></div>
<div class="m2"><p>وسعت میدان و آهنگ بلند پارسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یافت مضمون از منجم‌باشی ترک و سپس</p></div>
<div class="m2"><p>چند دفتر زد به قالب در روند پارسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پارسی گویان تبریز ار به ما بخشند عمر</p></div>
<div class="m2"><p>باشد این تبریزی نادان گزند پارسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ازبن بنای ناشی طرز معماری خرند</p></div>
<div class="m2"><p>خشت زیبایی درافتد از خرند پارسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترکتازی‌ها کند اکنون سوی پازند و زند</p></div>
<div class="m2"><p>وای بر مظلومی پازند و زند پارسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لفظ و معنی را خناق افتد کجا این ترک خام</p></div>
<div class="m2"><p>افکند برگردن معنی کمند پارسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طوطی شکرشکن بربست لب کز ناگهان</p></div>
<div class="m2"><p>تاختند این خرمگس‌ها سوی قند پارسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اعجمی‌ ترکان به‌ جای قاف چون گفتندگاف</p></div>
<div class="m2"><p>گشت قند پارسی یک‌باره گند پارسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوی گرفت از شرمساری چهر قطران و همام</p></div>
<div class="m2"><p>تا که گشت این ننگ تبریز آزمند پارسی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خطهٔ تبریز راگویندگان بودست و هست</p></div>
<div class="m2"><p>هر یکی گوینده لعل نوشخند پارسی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس چه شد کاین احمدک زان خطهٔ مینونشان</p></div>
<div class="m2"><p>احمداگو شد به گفتار چرند پارسی</p></div></div>