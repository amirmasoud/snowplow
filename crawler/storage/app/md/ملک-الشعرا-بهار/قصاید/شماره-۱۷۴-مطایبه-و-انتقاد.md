---
title: >-
    شمارهٔ ۱۷۴ - مطایبه و انتقاد
---
# شمارهٔ ۱۷۴ - مطایبه و انتقاد

<div class="b" id="bn1"><div class="m1"><p>یاد روزی کز برای دخل میدان ساختیم</p></div>
<div class="m2"><p>از دغل سرمایه و از تزویر دکان ساختیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه با شه‌، گاه با دستور، گه با این و آن</p></div>
<div class="m2"><p>ساختیم و ملک را میدان جولان ساختیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون که خر بازار بود آن عهد، در پالان شاه</p></div>
<div class="m2"><p>کرده پیزرها و بهر خویش پالان ساختیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با اتابک ساختیم و تاختیم از هر طرف</p></div>
<div class="m2"><p>خانمان خلق را تاراج و تالان ساختیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه ز بهر دانه‌پاشی شام دادیم و ناهار</p></div>
<div class="m2"><p>گه پی حاکم‌تراشی سنگ و سوهان ساختیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکجا بد تاجری با مایه و با اعتبار</p></div>
<div class="m2"><p>هوهو افکندیم و او را لات و عریان ساختیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاجران را ورشکستیم و پی املاکشان</p></div>
<div class="m2"><p>تیز از هر جانبی چنگال و دندان ساختیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از برای خود مهیا رشتهٔ املاک چند</p></div>
<div class="m2"><p>با بهای اندک و فکر فراوان ساختیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون عموم خلق را کردیم خر، بی‌دردسر</p></div>
<div class="m2"><p>خود عمومی شرکتی در ملک عنوان ساختیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن‌یک از ترس آن‌یک از جهل آن دگرها از طمع</p></div>
<div class="m2"><p>پول‌ها دادند و ما طالار و ایوان ساختیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس ز صاحب‌ثروتان روس بگرفتیم پول</p></div>
<div class="m2"><p>راهی اندر آستارا پر ز نقصان ساختیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون که شد اعضای شرکت را بنای بازدید</p></div>
<div class="m2"><p>ما به‌ روسان اصل شرکت‌ را گروگان‌ ساختیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون ز غیرت روس راکردیم داخل در عموم</p></div>
<div class="m2"><p>در پناه او ز غم خود را تن‌ آسان ساختیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نفی گشتن‌، حبس‌ دیدن‌، بد شنیدن را به‌ خویش</p></div>
<div class="m2"><p>بهر بلع مال شرکت سهل و آسان ساختیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مال مردم‌ خوردن از اسلام باشد دور و ما</p></div>
<div class="m2"><p>مال مردم خورده تا خود را مسلمان ساختیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواندن اسناد شرکت رفتمان از یاد، لیک</p></div>
<div class="m2"><p>از نماز و ذکر، جن را مات و حیران ساختیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لایق ریش سفید ما کزین نامردمی</p></div>
<div class="m2"><p>ملک خود را ریشخند خلق دوران ساختیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دولت مشروطه چون املاک ما توقیف کرد</p></div>
<div class="m2"><p>ما برایش دفتری از کذب و بهتان ساختیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ورنه‌ این ایران همان‌ باشد که‌ ما خود از نخست</p></div>
<div class="m2"><p>زین تقلب‌ها بنایش پاک ویران ساختیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>می کند صاحب‌ سند ده پنج پول خود طلب</p></div>
<div class="m2"><p>گوئیا ما از برایش زر در انبان ساختیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مبلغی دستی به ما باید دهد صاحب سند</p></div>
<div class="m2"><p>زانکه‌ ما موضوع شرکت را دگرسان ساختیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>... شرکت گشت از ... تقلب چاک و ما</p></div>
<div class="m2"><p>خویش را چون خایهٔ حلاج لرزان ساختیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خشتک ما را اگرگیتی برون آرد رواست</p></div>
<div class="m2"><p>زانکه الحق بهر فاطی خوب تنبان ساختیم</p></div></div>