---
title: >-
    شمارهٔ ۱۱۰ - پاسخ فرخ
---
# شمارهٔ ۱۱۰ - پاسخ فرخ

<div class="b" id="bn1"><div class="m1"><p>شکر خداکه دوره غربت بسر رسید</p></div>
<div class="m2"><p>رنج سفرگذشت و نعیم حضر رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که رخت‌بستم‌از ایران سوی فرنگ</p></div>
<div class="m2"><p>پنداشتم که عهد عقوبت بسر رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم زمان خرقه تهی کردنست‌، خیز</p></div>
<div class="m2"><p>رخت سفر ببند که وقت سفر رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینک خدنگ حادثه از سینه برگذشت</p></div>
<div class="m2"><p>وآسیب زخم آن به میان جگر رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست از جهان بشوی و جهانی دگر بجوی</p></div>
<div class="m2"><p>شاد آنکه زبن جهان به جهان دگررسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیکن قضا نبود، تو گفتی در این جهان</p></div>
<div class="m2"><p>سهم بلابه بنده فزون زبن قدررسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرمان بازگشت به روح رمیده رفت</p></div>
<div class="m2"><p>پروانهٔ بقا به تن محتضررسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دستوری خلاصم از این زندگی نداد</p></div>
<div class="m2"><p>آن کس که جان ازو به تن جانور رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان به لب رسیده سوی سینه بازگشت</p></div>
<div class="m2"><p>در چشم وگوش مژدهٔ سمع و بصررسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد منقطع هزینه دورعلاج من</p></div>
<div class="m2"><p>زبن صرفه‌جوبی سره‌دولت به‌زر رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بویحیی ار برفت حکیمی به‌جای ماند</p></div>
<div class="m2"><p>وآی ارگدا به دولت و اقبال و فر رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بالجمله رفت سالی و شش ماه بر فزون</p></div>
<div class="m2"><p>کاندر سویش، لطف حقم راهبر رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسیار صبرکردم و بسیار بردم رنج</p></div>
<div class="m2"><p>تا درپناه صبر، نوید ظفر رسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بشتافتم به خانه و در بستر اوفتاد</p></div>
<div class="m2"><p>کزرنج ره براین تن نالان ضرر رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک مه فزون بودکه هم اغوش بسترم</p></div>
<div class="m2"><p>وامروز به شدم که ز «‌فرخ‌» خبر رسید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>محمود اوستاد سخن آن که صیت او</p></div>
<div class="m2"><p>از خاوران گذشته سوی باختر رسید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روح جواهری به جنان شادباد ازآنک</p></div>
<div class="m2"><p>او را پسر چو فرخ فرخ سیر رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاد این پسرکه پرورش از آن پدرگرفت</p></div>
<div class="m2"><p>شاد آن پدرکه از عقبش این پسر رسید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دانشوران ز فضل و هنر بهره می‌برند</p></div>
<div class="m2"><p>وز او هزار بهره به فضل و هنر رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کرد از بهار دعوت‌، فرخ به شهر خویش</p></div>
<div class="m2"><p>در تیر مه که تیل میان سرخ دررسید </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آباد باد خاک خراسان که هر مهی</p></div>
<div class="m2"><p>نعمت در او ز ماه دگر بیشتر رسید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سرسبز باد تیل میان سرخ او، کز آن</p></div>
<div class="m2"><p>خجلت به زعفران و گلاب و شکر رسید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نالانم ای رفیق و هراسانم از سفر</p></div>
<div class="m2"><p>خاصه که ناتوانیم از این سفر رسید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ارجو که تندرست ببینم رخ ترا</p></div>
<div class="m2"><p>کز روی فرخ توام اقبال و فر رسید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفتم جواب چامهٔ «‌فرخ‌» که گفته است</p></div>
<div class="m2"><p>«‌از دستگاه رادیو دوش این خبر رسید»</p></div></div>