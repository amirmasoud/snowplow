---
title: >-
    شمارهٔ ۲۵۴ - نثار به پیشاهنگان
---
# شمارهٔ ۲۵۴ - نثار به پیشاهنگان

<div class="b" id="bn1"><div class="m1"><p>ای قد تو چون سرو جویباری</p></div>
<div class="m2"><p>وی عارض تو چون گل بهاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای لعل تو چون خاتم بدخشی</p></div>
<div class="m2"><p>وی زلف تو چون نافهٔ تتاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامان تو مانندهٔ دل من</p></div>
<div class="m2"><p>پاکیزه‌تر از برف کوهساری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخسار تو مانند خاطر من</p></div>
<div class="m2"><p>تابنده‌تر از ماه ده چهاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای فتنهٔ مشکو به دلفریبی</p></div>
<div class="m2"><p>وی آفت میدان به جان شکاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برداشته در بزمگه به صحبت</p></div>
<div class="m2"><p>زنگ ازدل یاران به‌شادخواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برتافته در رزمگه ز غیرت</p></div>
<div class="m2"><p>سر پنجهٔ گردان کارزاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر لبت‌ اندر، شرنگ‌ و شهد است</p></div>
<div class="m2"><p>گاه سخط و گاه بردباری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیر نگهت دوزخ و بهشت است</p></div>
<div class="m2"><p>هنگام درشتی و وقت یاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسن تو به شورشگری نهاده</p></div>
<div class="m2"><p>در ملک دل آئین سربداری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وان خوی پلنگینت ایستاده</p></div>
<div class="m2"><p>پیرامن حسنت به پاسداری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای زادهٔ ایران بدان که این ملک</p></div>
<div class="m2"><p>دارد به تو چشم امیدواری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواهدکه ببالی به باغ کشور</p></div>
<div class="m2"><p>آزاده ‌تر از سرو جویباری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وانگاه بپویی به بزم دشمن</p></div>
<div class="m2"><p>پیروزتر از شیر مرغزاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باشد نگران تا به جای او تو</p></div>
<div class="m2"><p>نیکی چه کنی‌، حق چسان گزاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>راه خطر خود چگونه پوبی</p></div>
<div class="m2"><p>پاس شرف خود چگونه داری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زنهار نه پویی رهی کت آید</p></div>
<div class="m2"><p>فرجام‌، زبونی و شرمساری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ننگ بشر و آفت جوانی است</p></div>
<div class="m2"><p>زنبارگی و فسق و میگساری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وان کاهلی و سستی و بطالت</p></div>
<div class="m2"><p>فقر آورد و نیستی و زاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بودند نیاکان تو سواران</p></div>
<div class="m2"><p>شهره به دلیری و شهسواری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زنهار نجسته ز خصم‌، لیکن</p></div>
<div class="m2"><p>بخشوده به خصمان زینهاری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آوخ که ز جهل مغان فتادند</p></div>
<div class="m2"><p>از شاهنشاهی و شهریاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مهرعلی و یازده سلیلش</p></div>
<div class="m2"><p>بنمود ترا راه رستگاری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هرچند که از دشمنان کشیدی</p></div>
<div class="m2"><p>زندازه برون ای نگار، خواری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دین را مکن آلودهٔ تعصب</p></div>
<div class="m2"><p>کاسلام از آلایش است عاری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بی‌دین‌، فسرد مردم زمانه</p></div>
<div class="m2"><p>بی ‌دینی را نیست استواری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>و آن فلسفه و اصل‌های در وین</p></div>
<div class="m2"><p>علم است نه آئین ملک داری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آیین زراتشت رفت بر باد</p></div>
<div class="m2"><p>وان فره و تایید کردگاری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وان کیش که مانی نهاد، گم شد</p></div>
<div class="m2"><p>هم نیز خود او کشته شد به زاری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن بدعت کاورد (‌ارد و یراف‌)</p></div>
<div class="m2"><p>بنشست ز قرآن به سوگواری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رخ‌، گفت بشو باگمیز چون‌مهر</p></div>
<div class="m2"><p>سر بر زند از نیلگوی عماری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فرمود نبی جای بول گاوان</p></div>
<div class="m2"><p>دست وسر و پا شو به آب جاری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باز است در اجتهاد تا تو</p></div>
<div class="m2"><p>نا مقتضی از مقتضی برآری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وان کینهٔ دیرین جدا ز دین است</p></div>
<div class="m2"><p>در دین نسزد کین و دوستداری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>با قوت دین خاک دشمنان را</p></div>
<div class="m2"><p>بسپر به سم رخش نامداری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وز کتف مهانشان دوال برکش</p></div>
<div class="m2"><p>تا کینهٔ دیرینه برگزاری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>لیک این عصبیت میار در دین</p></div>
<div class="m2"><p>گر بر خردت جهل نیست طاری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تقلید فرنگان کنی بهرکار</p></div>
<div class="m2"><p>جز کار خرد، اینت نابکاری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دارند فرنگان ز روم و یونان</p></div>
<div class="m2"><p>رشک و عصبیت به یادگاری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>با مشرقیان ویژه با من و تو</p></div>
<div class="m2"><p>جوینده ره و رسم بد شعاری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عیسی و حواریش بوده بودند</p></div>
<div class="m2"><p>از مردم سامی نه قوم آری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از شرق برون آمدند و گردید</p></div>
<div class="m2"><p>ترسایی از پدر به غرب ساری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با این همه این غربیان نمایند</p></div>
<div class="m2"><p>فخر از قبل عیسی و حواری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بنگر که بدین اندر از من و تو</p></div>
<div class="m2"><p>دارند فزون جد و پافشاری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خواهی اگر این ملک باز بیند</p></div>
<div class="m2"><p>آن فر و شکوه و بزرگواری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بزدای ز دین زنگ‌های دیرین</p></div>
<div class="m2"><p>زان پیش که‌ شد روز ملک تاری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>با نیروی دانش برون کن از دین</p></div>
<div class="m2"><p>این خرخری و جهل و زشتکاری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ایمان و شرافت به مردم آموز</p></div>
<div class="m2"><p>تا طاعت بینی و جان سپاری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بیخ می و مستی ز ملک برکن</p></div>
<div class="m2"><p>بنشان بن مردی و هوشیاری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در پاس تن و عرض و مال مردم</p></div>
<div class="m2"><p>با داد و دهش کوش و پاسداری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وان‌ شمع که شد غرب از آن منور</p></div>
<div class="m2"><p>بگمار در ایوان کامکاری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چون فقر و غنا هر دو شد ز گیتی</p></div>
<div class="m2"><p>و آمد هنر و علم و شادخواری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پس معجزه‌ها بین برغم آنکو</p></div>
<div class="m2"><p>گوید عظمت نیست اختیاری</p></div></div>