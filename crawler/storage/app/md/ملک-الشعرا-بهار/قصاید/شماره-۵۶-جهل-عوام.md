---
title: >-
    شمارهٔ ۵۶ - جهل عوام
---
# شمارهٔ ۵۶ - جهل عوام

<div class="b" id="bn1"><div class="m1"><p>این عامیان که در نظر ما مصورند</p></div>
<div class="m2"><p>هر روز دام کینه به ما بر بگسترند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما پاسدار دین و کتاب پیمبریم</p></div>
<div class="m2"><p>وبنان عدوی دین و کتاب پیمبرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دین نیست اینکه بینی در دست این گروه</p></div>
<div class="m2"><p>کاین مفسده ‌است و این ‌دنیان مفسدت گرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وین رسم پاک نیست که دارند این عوام</p></div>
<div class="m2"><p>کاین‌ بدعت ‌است و این ‌سف‌ها بدعت آورند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ایزد و نبی نشناسند جز دو حرف</p></div>
<div class="m2"><p>کاینان اسیر گفته ی بابند و مادرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کویی چرایکی است‌خدا، یارسول کیست</p></div>
<div class="m2"><p>اینان ز مادر و ز پدر حجت آورند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افلاک در نبشته کمال پیمبری</p></div>
<div class="m2"><p>وینان به کارنامه ی شق‌القمر درند</p></div></div>