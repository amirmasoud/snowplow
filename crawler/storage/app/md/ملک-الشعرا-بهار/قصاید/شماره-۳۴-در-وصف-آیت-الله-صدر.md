---
title: >-
    شمارهٔ ۳۴ - در وصف آیت‌الله صدر
---
# شمارهٔ ۳۴ - در وصف آیت‌الله صدر

<div class="b" id="bn1"><div class="m1"><p>رسول گفت گرت دیدن خدای هواست</p></div>
<div class="m2"><p>باولیای خدا بین که شان جمال خداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم اولیا راگر زانکه دید خواهی روی</p></div>
<div class="m2"><p>ببین سوی علمای شریعت از ره راست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدین دلیل و بدین حجُِ و بدین برهان</p></div>
<div class="m2"><p>درست مظهر روی خدا، رخ علماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ویژه آنکه به جز گفتهٔ خدای‌، نگفت</p></div>
<div class="m2"><p>به خاصه آنکه به جز خواهش خدای‌، نخواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسان حضرت صدرالانام‌، اسمعیل</p></div>
<div class="m2"><p>که عکس چهره‌اش آئینهٔ خدای‌نماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشاده دست وگشاده دل وگشاده جبین</p></div>
<div class="m2"><p>ستوده خوی و ستوده رخ و ستوده لقاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جز رضای خدا چون نخواست چیزدگر</p></div>
<div class="m2"><p>خدای نیز بدادش هرآنچه خود می‌خواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جلال داد و شرف داد و علم داد و عمل</p></div>
<div class="m2"><p>بدین فضایلش از پای تا به سر آراست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مداد تیره‌اش از بهر سرخ‌ روبی دین</p></div>
<div class="m2"><p>به جاه و رتبه چو خون مطهر شهداست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به راه دین مبین نامه‌اش سخن گستر</p></div>
<div class="m2"><p>به کار شرع متین خامه‌اش زبان‌آراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جلالت نسب از نام نامیش ظاهر</p></div>
<div class="m2"><p>سیادت و شرف از عکس چهره‌اش پیداست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خجسته عکس بدیعی که از تجلی او</p></div>
<div class="m2"><p>سراسر آینه دهرپرفروغ و ضیاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جمال آیت حق جلوه کرد و ز هر سوی</p></div>
<div class="m2"><p>به بندگیش کمر بسته خلقی از چپ و راست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نمازگاهش چون آسمان و او چون بدر</p></div>
<div class="m2"><p>موالیانش صف بسته چو نجوم سماست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهار مدح‌سرا در مدیح حضرت اوی</p></div>
<div class="m2"><p>به امر زادهٔ احمد چکامه‌ای آراست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به جای باد دوام و بقای عزت او</p></div>
<div class="m2"><p>همیشه تا مه و خورشید را دوام و بقاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دعای اهل دعا باد حافظ تن او</p></div>
<div class="m2"><p>همیشه عکسش تا قبله گاه اهل دعاست</p></div></div>