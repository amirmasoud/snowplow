---
title: >-
    شمارهٔ ۴۳ - غم
---
# شمارهٔ ۴۳ - غم

<div class="b" id="bn1"><div class="m1"><p>گویی علامت بشر اندر جهان‌، غم است</p></div>
<div class="m2"><p>آن کس که غم نداشت نه فرزند آدم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاعر پیمبری است خداوند او شعور</p></div>
<div class="m2"><p>کاو از خدای خویش همه روزه ملهم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هستم فدای طرفه خدایی که بر قلوب</p></div>
<div class="m2"><p>الهام‌ها فرستد و جبریل او غم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر گردن حیات بپیچیده عقل و عشق</p></div>
<div class="m2"><p>این هر دو مار با همه کس یار و همدم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماریست عقل‌، یک‌دم و چندین هزار سر</p></div>
<div class="m2"><p>یعنی که عقل با غم بسیار توام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماریست عشق‌، یکسر و چندین هزار دم</p></div>
<div class="m2"><p>یعنی غمی که بر همه غم‌ها مقدم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر زنده‌ای که نیست گرفتار این دوبند</p></div>
<div class="m2"><p>او آدمی نه‌، بل حیوان مسلم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزدیک من حیات به جز رنج و درد نیست</p></div>
<div class="m2"><p>رنج است و غصه زندگی ار بیش و ار کم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیدم به عمق جنگل هندوستان‌، بهار</p></div>
<div class="m2"><p>جوکی گرفته ماتم و بوزینه خرم است</p></div></div>