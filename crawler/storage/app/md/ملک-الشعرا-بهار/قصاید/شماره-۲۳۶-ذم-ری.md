---
title: >-
    شمارهٔ ۲۳۶ - ذم ری
---
# شمارهٔ ۲۳۶ - ذم ری

<div class="b" id="bn1"><div class="m1"><p>اجل پیام فرستاد سوی کشور ری</p></div>
<div class="m2"><p>که گشت روز تو کوتاه و روزگار تو طی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بریخت خون سلیل رسول‌، زاده سعد</p></div>
<div class="m2"><p>به یاد میری تهران و حکم‌داری ری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن زمانه به نفرین خاندان رسول</p></div>
<div class="m2"><p>دچار گشته‌ای ای خاک تودهٔ لاشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرنگ قهر اجانب چشیده دم در دم</p></div>
<div class="m2"><p>پیام سخت حوادث شنیده پی در پی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسا سلالهٔ شاهنشهان که حشمتشان</p></div>
<div class="m2"><p>گذشته بد ز سر تاج خانوادهٔ کی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که درتو جای گزیدند و خوار و زار شدند</p></div>
<div class="m2"><p>چو آل بوبه که شد در تو دور آنان طی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسا بزرگان کاندر تو زار کشته شدند</p></div>
<div class="m2"><p>و یا زبیم گرفتند ره به دیگر حی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن قبل که تو شومی و شومی از در تو</p></div>
<div class="m2"><p>به ملک در شود انسان که باده در رگ و پی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هماره بنگه اوباش و جایگاه رنود</p></div>
<div class="m2"><p>همیشه مهد خرافات و گاهوارهٔ غی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه فقیر به علم و همه علیم به زرق</p></div>
<div class="m2"><p>همه ضعیف به عقل و همه دلیر به می</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به تنگدستی‌، پابند زینت سرو بر</p></div>
<div class="m2"><p>به بی‌نوایی‌، گرم نوازش دف و نی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ایا به داخلیان گفته الجناح‌علیک</p></div>
<div class="m2"><p>ولی به خارجیان گفته الجناح علی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همین نه‌تنها در جنگ شیعی و سنی</p></div>
<div class="m2"><p>بسوختی چو ز تف شراره تودهٔ نی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که جنگ شافعی و مالکیت هم پس از آن</p></div>
<div class="m2"><p>چنان فشرد که در تو نماند شخصی حی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به یاد دار که با خاک ره شدی یکسان</p></div>
<div class="m2"><p>ز نعل لشگر تاتار و برق خنجر وی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به یاد دار کز آشوب توپ استبداد</p></div>
<div class="m2"><p>شد آفتاب تو تاریک و نوبهار تو دی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به یاد دارکه دادی تو هفده شهر به روس</p></div>
<div class="m2"><p>ز ملک ایران‌، آن گه که طفل بودی هی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به یاد دار که بودی عبید اجنبیان</p></div>
<div class="m2"><p>از آن زمان که نبشتند بر تو هذالری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>علاج ایران نبود جز اینکه صاعقه‌ایت</p></div>
<div class="m2"><p>به شعله محو کند، کاخر الدوا الکی</p></div></div>