---
title: >-
    شمارهٔ ۲۴۴ - پیام به وزیر خارجه انگلستان
---
# شمارهٔ ۲۴۴ - پیام به وزیر خارجه انگلستان

<div class="b" id="bn1"><div class="m1"><p>سوی لندن گذر ای پاک نسیم سحری</p></div>
<div class="m2"><p>سخن از من بر گو به سر ادوارد گری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کای خردمند وزیری که نپرورده جهان</p></div>
<div class="m2"><p>چون تو دستور خردمند و وزیر هنری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقشهٔ پطر بر فکر تو نقشی بر آب</p></div>
<div class="m2"><p>رأی بیزمارک بر رای تو رائی سپری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز تولون جیش ناپلیون نگذشتی گر بود</p></div>
<div class="m2"><p>بر فراز هرمان نام تو در جلوه گری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داشتی پاربس ار عهد تو درکف‌، نشدی</p></div>
<div class="m2"><p>سوی الزاس ولرن لشکر آلمان سفری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انگلیس ار ز تو می‌خواست در آمریک مدد</p></div>
<div class="m2"><p>بسته می‌شد به واشنگتون ره پرخاشجری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با کماندار چف گر فرتو بودی همراه</p></div>
<div class="m2"><p>به دیویت بسته شدی سخت ره حمله‌وری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور به منچوری پلتیک تو بد رهبر روس</p></div>
<div class="m2"><p>نشد از ژاپون جیش کرپاتکین کمری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود اگر فکر تو با عائلهٔ مانچو یار</p></div>
<div class="m2"><p>انقلابیون درکار نگشتند جری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور بدی رای تو دایر به حیات ایران</p></div>
<div class="m2"><p>این همه ناله نمی‌ماند بدین بی‌اثری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مثل است این که چو بر مرد شود تیره جهان</p></div>
<div class="m2"><p>آن کند کش نه به کار آید از کارگری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو بدین دانش‌، افسوس که چون بی‌خردان</p></div>
<div class="m2"><p>کردی آن‌کار که جز افسون ازوی نبری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برگشودی در صد ساله فروبستهٔ هند</p></div>
<div class="m2"><p>بر رخ روس و نترسیدی ازین دربدری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بچهٔ گرک در آغوش بپروردی و نیست</p></div>
<div class="m2"><p>این مماشات جز از بیخودی و بی‌خبری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی‌خودانه به تمنای زبر دست حریف</p></div>
<div class="m2"><p>در نهادی سر تسلیم‌، زهی خیره‌سری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اندر آن عهد که با روس ببستی زین پیش</p></div>
<div class="m2"><p>غبن‌ها بود و ندیدی تو زکوته نظری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو خود از تبت و ایران و ز افغانستان</p></div>
<div class="m2"><p>ساختی پیش ره خصم بنائی سه دری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از در موصل بگشودی ره تا زابل</p></div>
<div class="m2"><p>وز ره تبت تسلیم شدی تا به هری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زین سپس بهر نگهداری این هر سه طریق</p></div>
<div class="m2"><p>چند ملیون سپهی باید بحری و بری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیش از فایدت هند اگر صرف شود</p></div>
<div class="m2"><p>عاقبت فایدتی نیست به جز خون‌جگری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>انگلیس آن ضرری را که ازین پیمان برد</p></div>
<div class="m2"><p>تو ندانستی و داند بدوی و حضری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه همین زیرپی روس شود ایران پست</p></div>
<div class="m2"><p>بلکه افغانی ویران شود وکاشغری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور همی گوئی روس از سر پیمان نرود</p></div>
<div class="m2"><p>رو به تاربخ نگر تا که عجایب نگری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در بر نفع سیاسی نکند پیمان کار</p></div>
<div class="m2"><p>این نه من گویم کاین هست ز طبع بشری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خاصه چون روس که او شیفته باشد بر هند</p></div>
<div class="m2"><p>همچو شاهین که بود شیفته برکبک دری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ورنه روس از پس یک حجت واهی ‌ز چه روی</p></div>
<div class="m2"><p>راند قزاق و نهاد افسر بیدادگری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در خراسان که مهین ره‌رو هند است چرا</p></div>
<div class="m2"><p>کرد این مایه قشون بی‌سببی راهبری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فتنه‌ها از چه بپاکرد و چرا آخرکار</p></div>
<div class="m2"><p>کرد نستوده چنان کار بدان مشتهری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سپه روس زتبریزکنون تا به سرخس</p></div>
<div class="m2"><p>بیش از بیست هزارند چو نیکو شمری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هله کزمشرق ما امن بود تا به شمال</p></div>
<div class="m2"><p>سپه روس چرا مانده بدین بی‌ثمری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گرچه خود بی‌ثمری نیست که این جیش گزین</p></div>
<div class="m2"><p>سفری کردن خواهند به صد ناموری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سفر ایشان هند است و تمناشان هند</p></div>
<div class="m2"><p>هند خواهند بلی‌، نرم‌تنان خزری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وبژه گر پای بیفشاری تا از خط روس</p></div>
<div class="m2"><p>خط آهن به سوی هندکند ره سپری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به عدو خط ترن ره را نزدیک کند</p></div>
<div class="m2"><p>تا تو دیگر نروی راه بدین پرخطری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سد بس معتبری ایران بد بر ره هند</p></div>
<div class="m2"><p>وه که برداشته شد سد بدان معتبری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>باد نفرین به لجاجت که لجاجت برداشت</p></div>
<div class="m2"><p>پرده ازکار و فروبست رخ پرهنری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به لجاج و به غرض کردی کاری که بدو</p></div>
<div class="m2"><p>طعنه راند عرب دشتی وترک تتری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حیف ازآن خاطر دانای تو وان رای رزین</p></div>
<div class="m2"><p>که در این مسئله زد بیهده خود را به کری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زهی آن خاطر دانای رزین تو زهی</p></div>
<div class="m2"><p>فری آن فکر توانای متین تو فری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نام نیکو به ازین چیست که گویند به دهر</p></div>
<div class="m2"><p>هند و ایران شده ویران ز سر ادوارد کری</p></div></div>