---
title: >-
    شمارهٔ ۶۴ - ابر و باد
---
# شمارهٔ ۶۴ - ابر و باد

<div class="b" id="bn1"><div class="m1"><p>بود مر ابر را اندر کمین باد</p></div>
<div class="m2"><p>برد مر ابر را زین سرزمین باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ابر آید نباریده به صحرا</p></div>
<div class="m2"><p>وز بادی‌، که دیدست این‌چنین باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگرید ابر ازین پس زانکه هر روز</p></div>
<div class="m2"><p>گشاید ابر را چین از جبین باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو ابر اندر هوا بشتافت‌، دانیم</p></div>
<div class="m2"><p>که باشد در قفای او یقین باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک پیوسته در یک آستینش</p></div>
<div class="m2"><p>بود ابر و به دیگر آستین باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفورم من ز باده زآنکه باشد</p></div>
<div class="m2"><p>به لفظش تا به حرف سومین باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غمی گشتیم از این باد و از این ابر</p></div>
<div class="m2"><p>دو صد لعنت بر این ابر و بر این باد</p></div></div>