---
title: >-
    شمارهٔ ۱۹۴ - سردسیر درکه
---
# شمارهٔ ۱۹۴ - سردسیر درکه

<div class="b" id="bn1"><div class="m1"><p>چون اوج گرفت مهر از سرطان</p></div>
<div class="m2"><p>بگشاد تموز چون شیر دهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد خشک به‌ دشت‌ آن‌ سبزهٔ خرد</p></div>
<div class="m2"><p>شد پست به کوه آن برف کلان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد توت سپید و انگور رسید</p></div>
<div class="m2"><p>وان توت سیاه آمد به دکان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد گرم هوا شد تفته زمین</p></div>
<div class="m2"><p>زبن بیش به شهر ماندن نتوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امسال مراست رای درکه</p></div>
<div class="m2"><p>کانجا ز فضول خالی است مکان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با چند رفیق همراز و شفیق</p></div>
<div class="m2"><p>هم نادره‌سنج هم قاعده‌دان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طی شد مه تیر شد نامیه پیر</p></div>
<div class="m2"><p>لیکن درکه است سرسبز و جوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جایی است نزه باغی است فره</p></div>
<div class="m2"><p>کوهی است بلند آبی است روان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین خطه بهار بیرون نرود</p></div>
<div class="m2"><p>چه فصل تموز چه فصل خزان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویی که همی این ناحیه را</p></div>
<div class="m2"><p>بگزیده بهار از جمله جهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من هم نروم زینجا که نرفت</p></div>
<div class="m2"><p>ادربس نبی از باغ جنان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از لطف هواش گویی که کسی</p></div>
<div class="m2"><p>پاشیده به خاک آب حیوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سبز است هنوز خوشه به قصیل</p></div>
<div class="m2"><p>وز گندم شهر ما ساخته نان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم توت سیاه هم توت سپید</p></div>
<div class="m2"><p>پیداست هنوز بر تود بنان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن توت سپید بر شاخ درخت</p></div>
<div class="m2"><p>چون خیل نجوم برکاهکشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وان توت سیاه در پیش نظر</p></div>
<div class="m2"><p>چون غالیه‌ها در غالیه‌دان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>انبوه درخت هنگام نسیم</p></div>
<div class="m2"><p>چون نیزه‌وران هنگام طعان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باغ از برباغ بر رفته چنانک</p></div>
<div class="m2"><p>از زمردسبز، کان از برکان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دیدم شب دوش کافروخته شمع</p></div>
<div class="m2"><p>می‌سوخت ولی خشکش مژگان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفتم ز چه رو حیران شده‌ای</p></div>
<div class="m2"><p>رقصی بنمای اشکی بفشان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتا که چنان مستم ز هوا</p></div>
<div class="m2"><p>کم بی‌خبر است قالب ز روان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زبنجا بسوی سرچشمهٔ رود</p></div>
<div class="m2"><p>صعب ‌است ‌مسیر،‌ هول‌است مکان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از ریزش کوه غلطیده به زیر</p></div>
<div class="m2"><p>احجار عظیم همچون هرمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جوزات فتد در زیر قدم</p></div>
<div class="m2"><p>چون برگذری از دوکمران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن‌هفت غدیر چون هفت صدف</p></div>
<div class="m2"><p>بسد به کنار گوهر به میان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کارا ز فراز ریزد به نشیب</p></div>
<div class="m2"><p>آرام و خموش لرزان و نوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون پش سییدکش شانه زنند</p></div>
<div class="m2"><p>از زبر زنخ تا پیش دو ران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بنگرکه چسان ببرید و شکافت</p></div>
<div class="m2"><p>کارای حقیر خارای کلان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زبن تنگ‌دره چون برگذری</p></div>
<div class="m2"><p>زی تنگ بند راهی است نهان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با دید شود اندر سر راه</p></div>
<div class="m2"><p>کانی چو شبه بی‌حد کران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خطی سیه از دو سوی دره</p></div>
<div class="m2"><p>پیوسته بهم همچون دوکمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>این کشور ماست کان زر و نیست</p></div>
<div class="m2"><p>مردی که کشد این نقد ز کان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>*</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن غرش آب کز سنگ سیاه</p></div>
<div class="m2"><p>ریزد به نشیب جوشان و دمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گوبی که مگر هم‌نعره شدند</p></div>
<div class="m2"><p>در بیشه‌‌‌‌‌ٔ ‌تنگ شیران ژیان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یا از برکوه غلطند به زبر</p></div>
<div class="m2"><p>با غرش رعد صد سنگ گران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در هر قدس تا منبع رود</p></div>
<div class="m2"><p>صد چشمهٔ عذب دارد جریان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زنجیر قلل پیوسته به هم</p></div>
<div class="m2"><p>والبرز عظیم پیدا ز کران</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پیچیده بر او چون شارهٔ سبز</p></div>
<div class="m2"><p>انبوه درخت از دیر زمان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>البرز شدست گوبی علوی</p></div>
<div class="m2"><p>کز شارهٔ سبز بربسته میان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آن پارهٔ برف بر تیغهٔ کوه</p></div>
<div class="m2"><p>چون سیم سپید بر جزع یمان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>*</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برگشتم از آن کافتاد مرا</p></div>
<div class="m2"><p>از رفعت جای در سر دوران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ناگه بدمید ماه از برکوه</p></div>
<div class="m2"><p>کاهیده ز نور یک نیمهٔ آن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چونان که به رقص پوشیده شود</p></div>
<div class="m2"><p>یک نیمه ز زلف رخسار بتان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بی‌رود و سرود بی‌جام شراب</p></div>
<div class="m2"><p>منزلگه ماست چون کورستان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یارب بفرست یارب بفرست</p></div>
<div class="m2"><p>مولی برسان مولی برسان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زان شیشهٔ می زان تیشهٔ غم</p></div>
<div class="m2"><p>زان بیشه‌ٔ حال زان ریشهٔ جان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ای چرخ مرا بی‌باده مخواه</p></div>
<div class="m2"><p>وای دوست مرا بی‌بوسه ممان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نی‌نی نه رواست‌می بهر چراست</p></div>
<div class="m2"><p>می بیخ هواست می اصل هوان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>می خانه کن‌است‌دانش‌فکن‌است</p></div>
<div class="m2"><p>آسیب تن است وآزار روان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خنیاگر ماست این بلبل مست</p></div>
<div class="m2"><p>نوشین می ماست این آب روان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از جلوهٔ کوه شو مست که هست</p></div>
<div class="m2"><p>هر منظره‌اش فردوس‌نشان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بنگرکه چسان شد مست هزار</p></div>
<div class="m2"><p>بی‌نشاهٔ می بی کیف دخان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گر از ره طبع سرمست شوی</p></div>
<div class="m2"><p>ز آسیب خمار نفتی به زیان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>این پند من است هرچند بود</p></div>
<div class="m2"><p>مشکل به عمل آسان به زبان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گر ز امر منش سر بر نزدی</p></div>
<div class="m2"><p>مردم نشدی مقهور غمان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>غم چیره به‌خلق زان شد که نمود</p></div>
<div class="m2"><p>بهمان ز سفه تقلید فلان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اقلیم و هوا پوشاک و غذا</p></div>
<div class="m2"><p>اصلی‌ست درست درسی‌ست روان‌</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بیمار شوی گر از ره جهل</p></div>
<div class="m2"><p>در جده خوری قوت همدان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>وآن را که به طبع رد کرد منش</p></div>
<div class="m2"><p>گر قصد کنی بد بینی از آن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>پر فتنه مشو بر صنع بشر</p></div>
<div class="m2"><p>کاین گفت چنین و آن کرد چنان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کز صنع بشر بازست و دراز</p></div>
<div class="m2"><p>بر فرق زمین دست حدثان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دردا که بشر شد سخرهٔ نفس</p></div>
<div class="m2"><p>وز علم نهاد دامی به جهان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ازطبع و منش برگشت و فتاد</p></div>
<div class="m2"><p>از راه یقین در بند کمان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>شد علم فزون لیکن بنکاست</p></div>
<div class="m2"><p>نز بخل بخیل نز جبن جبان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>جنگی که پریر گیتی بگرفت</p></div>
<div class="m2"><p>ناداده کسی در دهر نشان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نه برده چنو این پشت زمین</p></div>
<div class="m2"><p>نه دیده چنو این چرخ کیان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آن خون که بریخت این نیمهٔ قرن</p></div>
<div class="m2"><p>هرگز بنریخت در چند قران</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ایراک ز علم ثروت طلبند</p></div>
<div class="m2"><p>نه لذت روح نه رامش جان</p></div></div>