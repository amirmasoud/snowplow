---
title: >-
    شمارهٔ ۶۶ - دختر گدا
---
# شمارهٔ ۶۶ - دختر گدا

<div class="b" id="bn1"><div class="m1"><p>گویند سیم و زر به گدایان خدا نداد</p></div>
<div class="m2"><p>جان پدر بگوی بدانم چرا نداد؟‌!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پیش ما گذشت خدا و نداد چیز</p></div>
<div class="m2"><p>دیشب‌، که نان نسیه به ما نانوا نداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان پدر بگوی بدانم خدا نبود</p></div>
<div class="m2"><p>آن شخص خوش‌لباس که چیزی به ما نداد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر او خدا نبود چرا اعتنا نکرد</p></div>
<div class="m2"><p>بر ما و هیچ چیز به طفل گدا نداد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شخصی خیال که چیزی دهد ولی</p></div>
<div class="m2"><p>آژان میان فتاد و ردم کرد و جا نداد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که مرده مادر و بابام ناخوش است</p></div>
<div class="m2"><p>کس شاهی ای برای غذا و دوا نداد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همسایه روضه خواند و غذا داد، پس چرا</p></div>
<div class="m2"><p>بیرون در به جمع فقیران غذا نداد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیدم کلاهی ای زدم در تو را براند</p></div>
<div class="m2"><p>وز دوری خورش به تو یک لوبیا نداد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دایم به قهوه‌خانه سماور صدا دهد</p></div>
<div class="m2"><p>یکبار هم سماور بابا صدا نداد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بقال بی‌مروت از آن میوه‌ها به من</p></div>
<div class="m2"><p>یک آلوی کفک زدهٔ کم‌بها نداد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نزدیک نانوا سر پا بودم و کسی</p></div>
<div class="m2"><p>یک لقمه نان به دست من ناشتا نداد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مردی گرفت لپ مرا و فشرد و رفت</p></div>
<div class="m2"><p>چیزی ولی به دست من بینوا نداد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از این همه درخت که باشد میان شهر</p></div>
<div class="m2"><p>یک شاخه نیز منقل ما را جلا نداد</p></div></div>