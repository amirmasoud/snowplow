---
title: >-
    شمارهٔ ۸۳ - خیال خام
---
# شمارهٔ ۸۳ - خیال خام

<div class="b" id="bn1"><div class="m1"><p>کسان که شور به ترک سلاح عام کنند</p></div>
<div class="m2"><p>خدنگ غمزهٔ خونریز را چه نام کنند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسلمست که جنگ از جهان نخواهد رفت</p></div>
<div class="m2"><p>ز روی وهم گروهی خیال خام کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گمان مبر که برای نمونه مدعیان</p></div>
<div class="m2"><p>به صلح دادن ژاپون و چین قیام کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به موی تو که همین صلح ‌پیشگان فردا</p></div>
<div class="m2"><p>ز بهر قسمت چین شور و ازدحام کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز راز مهر و محبت اگر شوند آگاه</p></div>
<div class="m2"><p>مبارزان جهان تیغ در نیام کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تمدنی که اساسش ز حلق و جلق بپاست</p></div>
<div class="m2"><p>به‌ صلح و سلم چسان مردمش دوام کنند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سه چار دولت گیهان مدار هم پیمان</p></div>
<div class="m2"><p>پی موازنه این گفتگو مدام کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنوز اول صلح است و غاصبان در هند</p></div>
<div class="m2"><p>به شهر و دهکده هر روز قتل عام کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هنوز اول درد است و می کشان در چین</p></div>
<div class="m2"><p>کشیده لشگر و تدبیر انقسام کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پی ربودن و تقسیم سرزمین حبش</p></div>
<div class="m2"><p>هزار شعبده پیدا به صبح و شام کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیالشان همه این است کاین سعادت را</p></div>
<div class="m2"><p>به خود حلال و به دیگر کسان حرام کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نعوذبالله اگر مردم ستمدیده</p></div>
<div class="m2"><p>فریب خورده بر این معنی احترام کنند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندای صلح به عالم فکنده‌اند اول</p></div>
<div class="m2"><p>که معده پاک ز هضم عراق و شام کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خبر دهند به خوبان که تیغ ابرو را</p></div>
<div class="m2"><p>سپس به واسطهٔ وسمه در نیام کنند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صفوف سرکش مژگان و چشم فرمانده</p></div>
<div class="m2"><p>به پشت عینک دودی سپس مقام کنند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کمند زلف که شد پیش از این بریده سرش</p></div>
<div class="m2"><p>به دست شانه از آشفتگیش رام کنند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیاض گردن موزون و ساعد سیمین</p></div>
<div class="m2"><p>نهفته در مد خاص از نگاه عام کنند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لبان لعل و زنخدان و خال و عارض را</p></div>
<div class="m2"><p>نهفته زیر یکی قیرگون پنام کنند</p></div></div>