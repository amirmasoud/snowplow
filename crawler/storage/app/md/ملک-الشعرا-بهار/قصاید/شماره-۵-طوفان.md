---
title: >-
    شمارهٔ ۵ - طوفان
---
# شمارهٔ ۵ - طوفان

<div class="b" id="bn1"><div class="m1"><p>سحابی ‌قیرگون برشد ز دریا</p></div>
<div class="m2"><p>که قیراندود شد زو روی دنیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلیج فارس گفتی کز مغاکی</p></div>
<div class="m2"><p>به دوزخ رخنه کرد و ریخت آن‌جا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بناگه چون بخاری تیره و تار</p></div>
<div class="m2"><p>از آن چاه سیه سر زد به بالا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علم زد بر فراز بام اهواز</p></div>
<div class="m2"><p>خروشان قلزمی جوشان‌ و دروا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهنگان در چه دوزخ فتادند</p></div>
<div class="m2"><p>وز ایشان رعدسان برخاست هرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزاران اژدهای کوه پیکر</p></div>
<div class="m2"><p>به گردون تاختند از سطح غبرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجست از کام آنان آتش و دود</p></div>
<div class="m2"><p>وزان شد روشن و تاربک صحرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزیمت شد سپهر از هول و افتاد</p></div>
<div class="m2"><p>ز جیبش‌ مهرهٔ‌ خورشید رخشا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو گفتی کز نهان اهریمن زشت</p></div>
<div class="m2"><p>شبیخون زد به یزدان توانا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برون پرید روز از روزن مهر</p></div>
<div class="m2"><p>نهان شد در پس دیوار فردا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب تاری درآمد لرز لرزان</p></div>
<div class="m2"><p>چو کور بی‌عصا در سخت سرما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز برق اورا به کف شمعی که‌هردم</p></div>
<div class="m2"><p>فرو مرد از نهیب باد نکبا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خلیج فارس ناگه گشت غربال</p></div>
<div class="m2"><p>ز بالا بر سر آن تیره بیدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طبیعت خنده زد چون خندهٔ شیر</p></div>
<div class="m2"><p>زمانه نعره زد چون غول کانا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمین پنهان شد اندر موج باران</p></div>
<div class="m2"><p>که از هر سو درآمد بی‌محابا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خط آهن میان موج گفتی</p></div>
<div class="m2"><p>ره موسی است اندر قعر دریا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خروشان و شتابان رود کارون</p></div>
<div class="m2"><p>درافزوده به بالا و به پهنا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رخ سرخش غبارآلود و تیره</p></div>
<div class="m2"><p>چو روی مرد جنگی روز هیجا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز هر سو موج‌ها انگیخت چون کوه</p></div>
<div class="m2"><p>که شدکوه از نهیبش زیر و بالا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به تیغ موج‌هایش کف نشسته</p></div>
<div class="m2"><p>چو برف دیمهی بر کوه خارا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نفس در سینه‌ها پیچیده از بیم</p></div>
<div class="m2"><p>که ناگه چتر خسرو شد هویدا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو کارون دید شه را تیزتر شد</p></div>
<div class="m2"><p>چو مستی کش زنی سیلی بعمدا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهاز آتشین بر سطح کارون</p></div>
<div class="m2"><p>به ‌رقص افتاد چون می‌ خورده برنا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>و یا مانندهٔ نر اشتری مست</p></div>
<div class="m2"><p>کز آهنگ حدی برخیزد از جا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شهنشه بر سر کارون قدم سود</p></div>
<div class="m2"><p>بخفت آن شرزه شیر ناشکیبا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بلی دیوانه چون زنجیر بیند</p></div>
<div class="m2"><p>فرامش گرددش آشوب و غوغا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>می حب‌الوطن خوردست خسرو</p></div>
<div class="m2"><p>کی از دیوانه دارد مست پروا؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پس‌ از شه میر خوزستان گمان برد</p></div>
<div class="m2"><p>که کارون خفت ‌و برگشت از معادا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز شه شد دور و ناگاهان فروماند</p></div>
<div class="m2"><p>در آن غرقاب هول‌انگیز، تنها</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فروبلعیدش آن گود دژآهنگ</p></div>
<div class="m2"><p>چو پشه کافتد اندر کام عنقا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ولیک از بیم شه بیرونش افکند</p></div>
<div class="m2"><p>وز آن کرداب ژرفش کرد پیدا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کشیدندش برون از چنگ کارون</p></div>
<div class="m2"><p>چو بودش بر شه گیتی تولا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ازین غفلت به خود پیچیدکارون</p></div>
<div class="m2"><p>وزین خجلت گرفتش خوی‌ سراپا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پذیرفتار شد کاندر ولایت</p></div>
<div class="m2"><p>نیازد زین سپس دست تعدا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نهنگانش نیازارند مردم</p></div>
<div class="m2"><p>نه طوفانش بیو بارد رعایا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>برو سدها ببندد شاه گیتی</p></div>
<div class="m2"><p>وزو جرها گشاید شاه دنیا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نهد گردن به‌بند شهریاری</p></div>
<div class="m2"><p>نماید خاک خوزستان مصفا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بود چونان که بد در عهد شاپور</p></div>
<div class="m2"><p>شود چونان که شد در عهد دارا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بروباند ز اهواز اصل شکر</p></div>
<div class="m2"><p>پدید آرد ز ششتر نسج دیبا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به پیوندد ز فیضش قصر در قصر</p></div>
<div class="m2"><p>ز بند شوشتر تا خور موسی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کند برطرف بهمن شیر و حفار</p></div>
<div class="m2"><p>هزاران قریهٔ آباد انشا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شهنشه عذر کارون درپذیرفت</p></div>
<div class="m2"><p>بدان پذرفت‌کاری‌های زیبا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بود هرچند جرم بندگان بیش</p></div>
<div class="m2"><p>گذشت شاه افزونست از آنها</p></div></div>