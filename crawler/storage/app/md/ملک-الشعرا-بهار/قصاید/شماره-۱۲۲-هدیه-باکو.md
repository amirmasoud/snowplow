---
title: >-
    شمارهٔ ۱۲۲ - هدیه باکو
---
# شمارهٔ ۱۲۲ - هدیه باکو

<div class="b" id="bn1"><div class="m1"><p>روزآدینه ببستیم زری رخت سفر</p></div>
<div class="m2"><p>بسپردیم ره دیلم و دریای خزر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بساطی بنشستیم سلیمان کردار</p></div>
<div class="m2"><p>که صبا خادم او بود و شمالش چاکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یکی پرش از دشت رسیدیم به کوه</p></div>
<div class="m2"><p>به دگر پرش از بحر گذشتیم به بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رهبرما به سوی قاف یکی هدهد بود</p></div>
<div class="m2"><p>هدهدی‌غران چون شیر و دمان چون صرصر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود سیمرغ‌وشی بانگ‌زن و رویین‌تن</p></div>
<div class="m2"><p>مرغ رویین که شنیده است بدین قوت و فر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیلتن مرغ فرو خورد مرا با یاران</p></div>
<div class="m2"><p>تا بباکویه فرو ریزدمان از ژاغر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیمی‌از هشت‌چو بگذشت به‌ساعت‌، برخاست</p></div>
<div class="m2"><p>مرغ رویینه‌تن از جای چو دیوی منکر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرغ دیده است کسی دیو تن و دیو غریو؟</p></div>
<div class="m2"><p>دیو دیده است کسی مرغ‌وش و مرغ‌سیر؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دم کشیده به زمین‌، چشم گشاده به سما</p></div>
<div class="m2"><p>وز دو سو بی‌حرکت‌، پهن دو رویین شهپر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرده گفتی دو ملخ‌ صید و گرفته به دهان</p></div>
<div class="m2"><p>وآن‌دو صید از دو طرف‌سخت به‌قوت زده پر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نگیرد کس از او صید وی از جای بجست</p></div>
<div class="m2"><p>همچو سیمرغ که گیرد به‌سوی قاف گذر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جست چون برق و گذر کرد ز بالای سحاب</p></div>
<div class="m2"><p>بانگ دو پرهٔ او همچو خروش تندر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>داشت دومغز و به هر مغز یکی کارشناس</p></div>
<div class="m2"><p>چشم بر عقربک و دست به سکان اندر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما چو یونس به درون شکم حوت ولیک</p></div>
<div class="m2"><p>اوبه دریا در و ما در دل جو راهسپر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خطهٔ ری به پس پشت نهادیم و شدیم</p></div>
<div class="m2"><p>از فضای کرج و ساحت قزوین برتر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برف بر تیغهٔ البرز و بر او ابر سپید</p></div>
<div class="m2"><p>کوه بی‌جنبش و ابر از بر او بازیگر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برنشستند تو گفتی به یکی ببر سطبر</p></div>
<div class="m2"><p>نوعروسانی بنهفته به کتان پیکر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ما گذشتیم ز بالا و گذشتند ز زیر</p></div>
<div class="m2"><p>کاروان‌ها بسی از ابر، به کوه و به کمر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سایه و روشن چون رقعهٔ شطرنج شدی</p></div>
<div class="m2"><p>سطح هر دامنه کش ابرگذشتی ز زبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ما بر این رقعهٔ شطرنج مقامر بودیم</p></div>
<div class="m2"><p>خصم طوفان بد وما بروی جستیم ظفر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که‌سوی‌چپ متمایل شدی وگه سوی راست</p></div>
<div class="m2"><p>گه فروخفتی‌ و گه جستی چون ضیغم‌نر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عاقبت مرکب ما بیحد و مر اوج گرفت</p></div>
<div class="m2"><p>تا برون راند از آن ورطهٔ پرخوف وخطر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>الموت از شکم میغ نمایان‌، چونانک</p></div>
<div class="m2"><p>ملحدی روی به مندیل بپوشد ز نظر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سرخ‌رود از دره‌ای ژرف سراسیمه دوان</p></div>
<div class="m2"><p>شاهرود از طرفی قطره‌زن و خوی گستر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>راست چون عاشق و معشوق جدا مانده ز هم</p></div>
<div class="m2"><p>وز دو سوگشته دوان در طلب یکدیگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دریکی بستر، این هر دو بهم پیوستند</p></div>
<div class="m2"><p>زاد از آن فرخ پیوند یکی خوب پسر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پسری خوب کجا رود سپیدش خوانی</p></div>
<div class="m2"><p>زاد ازآن وصلت و غلتید به خونین بستر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رودبار نزه از زیر تو گفتی که بود</p></div>
<div class="m2"><p>دیبهی سبز و در او نقش ز انواع شجر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رحمت‌آباد به تن مخمل زنگاری داشت</p></div>
<div class="m2"><p>زیر دامانش نهان وادی وکوه و کردر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برگذشتیم زکهسار و رسیدیم به دشت</p></div>
<div class="m2"><p>خطهٔ رشت به چشم آمد و دریا به نظر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خطه رشت مگر فرش بهارستان بود</p></div>
<div class="m2"><p>اندرو نقش‌، ز هر لون و زهر نوع‌، گهر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از پس پشت یکی سلسله کهسار کبود</p></div>
<div class="m2"><p>پیش رو دشتی هموار ز فیروزهٔ تر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از بر گیلان راندیم به دریا و که دید</p></div>
<div class="m2"><p>سفر دربا بی گفت و شنود بندر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پرتو مهر درخشنده بر امواج کبود</p></div>
<div class="m2"><p>بافتی ماهی سیمینه به نیلی میزر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرکب آرام و هوا روشن و دریا خاموش</p></div>
<div class="m2"><p>خلوتی بود و سکوتی ز خرد گوباتر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ما خروشان و دمان در دل آن خاموشی</p></div>
<div class="m2"><p>چون به ملک ابدیت وزش وهم بشر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یازده ساعت از آن روز چو بگذشت فتاد</p></div>
<div class="m2"><p>راه ما بر سر خاکی که بودکان هنر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>«‌آبشوران‌» کهن کز مدد پیر مغان‌</p></div>
<div class="m2"><p>دارد اندر دل او آتش جاوید مقرّ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خاک باکو وطن و مامن دینداران بود</p></div>
<div class="m2"><p>اندر آن عهدکه بر شرق گذشت اسکندر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شهر باکو، نه که دردانه تاج مشرق</p></div>
<div class="m2"><p>خاک باکو، نه که دروازهٔ صلح خاور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تکیه گاه سپه سرخ که همواره بود</p></div>
<div class="m2"><p>زرد از رشک طلای سیهش چهرهٔ زر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خاک باکویه عزیز است و گرامی‌ بر ما</p></div>
<div class="m2"><p>که ز یک نسل و تباپم و زیک اصل و گهر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بیشه‌ای دیدیم آنجا ز مجانیق بلند</p></div>
<div class="m2"><p>وز عمارات قوی‌ییکر و عالی‌منظر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بیشه‌ای حاصل او نفت سیاه و زر سرخ</p></div>
<div class="m2"><p>خطه‌ای مردم او شیردل و نام‌آور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>قصر در قصر برآورده چه درکوه و چه دشت</p></div>
<div class="m2"><p>چاه در چاه فرو برده چه در بحر و چه بر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خاک او صنعت و آبش هنر و بذرش کار</p></div>
<div class="m2"><p>شجرش‌ علم و شکوفه شرف و میوه ظفر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>صبح برجسته ز جاکارگران از پی کار</p></div>
<div class="m2"><p>زیر پا واگن برقی و توکل در سر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مرد دهقان ز سرشوق برد آب به دشت</p></div>
<div class="m2"><p>که شریک است در آن مزرعهٔ جان‌پرور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>باغبان تاک نشاند ز سر رغبت و شوق</p></div>
<div class="m2"><p>خوکند باغ وکشد زحمت و برگیرد بر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کارگر کارکند روز و چو خور چهره نهفت</p></div>
<div class="m2"><p>بنمایش رود و جامه کند نو در بر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هیچ مرد و زن بیکار نیابند آنجای</p></div>
<div class="m2"><p>جز نقوشی که نگارند به دیوار و به در</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نه گدا دیدیم آنجای و نه درونش و نه دزد</p></div>
<div class="m2"><p>نه فریبندهٔ دختر نه ربایندهٔ زر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زن و مرد و بچه و پیر و جوان از سر شوق</p></div>
<div class="m2"><p>شغل خود را همگی روز و شبان بسته کمر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اندر آن مملکت از دربدری نیست نشان</p></div>
<div class="m2"><p>اندر آن ناحیت از گرسنگی نیست خبر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دربدر نیست کس آنجا به جز از باد صبا</p></div>
<div class="m2"><p>گرسنه نیست کس آنجا به جز از مرغ سحر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یا تناسانی کاهل که بود دشمن کار</p></div>
<div class="m2"><p>یا دغلبازی گر بز که بود مایهٔ شر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مزد بخشند به میزان توانایی و زور</p></div>
<div class="m2"><p>وان که بیمار و ضعیف است پزشکش یاور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>برتر از مزد درتن ملک مکان یابد و جاه</p></div>
<div class="m2"><p>هر هنرییشه و هر عالم و هر دانشور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مزد هر مرد به میزان شعور است و خرد</p></div>
<div class="m2"><p>شغل هرشخص به اندازهٔ هوش است و فکر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ابتکار آنجا بیقدر نماند زبراک</p></div>
<div class="m2"><p>صلتی باشد هرفکر نوی را درخور</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>اندر آن ملک بود ارزش هر چیز پدید</p></div>
<div class="m2"><p>ارزش کار فزون‌، ارزش فکر افزونتر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شاعران دیدم آنجا و هنرمندانی</p></div>
<div class="m2"><p>که نبدشان شمر خواستهٔ خو ز بر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مادران را گه زادن رسد از مهر، پزشک</p></div>
<div class="m2"><p>خواهد آن مام پسر زاید و خواهد دختر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کودک اندرکنف لطف پرستارانست</p></div>
<div class="m2"><p>تا رسد مادرش ازکار و بگیرد در بر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کودکستان پس از آن جایگه طفلانست</p></div>
<div class="m2"><p>چون که شد طفل کلان مدرسه آید به اثر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>طفل هست از شکم مادر خود تا دم مرگ</p></div>
<div class="m2"><p>به چنین قاعده و نظم قوی مستظهر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چون رودکار به اندازه و نظم آید پیش</p></div>
<div class="m2"><p>نز حسد یابی آثارو نه از بخل خبر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>حسد و بخل و نفاق و غرض و دزدی و مکر</p></div>
<div class="m2"><p>ز اختلاف طبقاتست و نظام ابتر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آن‌یکی غره به مالست ویکی خسته زفقر</p></div>
<div class="m2"><p>آن یکی شاد به نفع است و یکی رنجه ز ضر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ای بسا دانا کز ساده‌دلی مانده سفیل</p></div>
<div class="m2"><p>وی بسا نادان کز حیله گری نام‌آور</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>حیلت‌اندوز و رباکارکشد جام مراد</p></div>
<div class="m2"><p>خوبشتن‌دار و هنرمند خورد خون جگر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زبنت مرد به علم و هنر و پاکدلی است</p></div>
<div class="m2"><p>هست مکار و فسونساز عدوی کشور</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>اندر آن خطه که با حیلت و دستان و فریب</p></div>
<div class="m2"><p>مال گرد آید و جاه و شرف و قدر و خطر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>مرد بی‌حیلت و آزاده در او خوار شود</p></div>
<div class="m2"><p>واهل خیرات نسازند در آن ملک مقر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نظم چون گشت خطا، مرد تبه کار دنی</p></div>
<div class="m2"><p>هست‌ پیوسته به عز و به شرف مستبشر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>لاجرم خلق درافتند به جنگ طبقات</p></div>
<div class="m2"><p>زان میان جنگ جهانی بگشاید منظر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>طمع و حرص و حسد را تو یکی مزرعه دان</p></div>
<div class="m2"><p>کاندرو کینه بکارند و دهد جنگ ثمر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>عدل باید، که ستمکار شود مانده زکار</p></div>
<div class="m2"><p>نظم باید، که طمع‌ورز شود رانده ز در</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>این‌چنین قاعده و نظم‌، من اندر باکو</p></div>
<div class="m2"><p>دیدم و یافتم از گمشدهٔ خویش اثر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>وز چنین نظم قوی بود که از لشکر سرخ</p></div>
<div class="m2"><p>شد هزیمت سپه نازی و جیش محور</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>آفرین گفتم بر باکو و آذربیجان</p></div>
<div class="m2"><p>هم بر آن کس که شد این نظم قوی را رهبر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>این همان خاک عزیز است که اندر طلبش</p></div>
<div class="m2"><p>هیتلر از جمله اروپا بهم آورد حشر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>راند از اسپانی و ایتالی و بالکان و فرنگ</p></div>
<div class="m2"><p>لشکری بیحد و افروخت به روسیه شرر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>لشکر سرخ بدان سیل خروشان ره داد</p></div>
<div class="m2"><p>تا درآیند و درافتند به دام کیفر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>مردم شوروی از هر طرفی همچون سیل</p></div>
<div class="m2"><p>برسیدند و براندند به خیل و به نفر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بزدند آن سپه بی حد و راندند از پیش</p></div>
<div class="m2"><p>تا شکست از دد نازی کمر وگردن و سر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>از در بالکان وز مرز لهستان و پروس</p></div>
<div class="m2"><p>تا در برلین لشکرنگسست ازلشکر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>هیچ شک نیست که در آرزوی خوردن نفت</p></div>
<div class="m2"><p>نو ز لب تشنه بود مهتر نازی به سقر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>اگر این نظم شود در همه عالم جاری</p></div>
<div class="m2"><p>نه تنی فربه بینی نه وجودی لاغر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نه یکی منعم بر خیل فقیران سالار</p></div>
<div class="m2"><p>نه یکی نادان بر مردم دانا سرور</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>پنج سال افزون بر بیست گذشته است اکنون</p></div>
<div class="m2"><p>کابر استقلال افشانده برین خاک مطر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>شد بدین شادی آراسته جشنی و شدند</p></div>
<div class="m2"><p>در وی از هر طرفی گرد بسی نام‌آور</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ما هم از ری سوی همسایه درود آوردیم</p></div>
<div class="m2"><p>که ز همسایه سخن گفت بسی پیغمبر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>آذر آبادان همسایهٔ پر مایهٔ ماست</p></div>
<div class="m2"><p>غیر هم‌خونی و هم کیشی و احوال دگر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>من برآنم که ز همسایگی روس بزرگ</p></div>
<div class="m2"><p>برد این ملک در آینده حظوظوافر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>تا نگویی که ز همسایگی روس مرا</p></div>
<div class="m2"><p>دین و فرهنگ هبا گردد و آداب هدر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>دین و آیین تو وابستهٔ اهلیت تو است</p></div>
<div class="m2"><p>نبود دوستی شوروی الزام‌آور</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>گر تو نااهل شدی چیست گناه دگران</p></div>
<div class="m2"><p>در چنار کهن از خویش درافتد آذر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>روس همسایهٔ مستغنی و قادر خواهد</p></div>
<div class="m2"><p>نه که همسایهٔ نالان و ضعیف و مضطر</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>نیمهٔ دوم اردی است به باکو و هنوز</p></div>
<div class="m2"><p>ننموده است گل سرخ سر از غنچه بدر</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>لیک ما تازه گل سرخ فراوان دیدیم</p></div>
<div class="m2"><p>وبژه روز رژه بر ساحل دریای خزر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بگذشتند ز پیش رخ ما بیست‌هزار</p></div>
<div class="m2"><p>لعبتانی ز گل و سرو چمن زیباتر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>دخترانی همه بر لاله فروهشته کمند</p></div>
<div class="m2"><p>پسرانی همه بر سرو نشانیده قمر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>دختران سروقد و لاله‌رخ و سیم‌اندام</p></div>
<div class="m2"><p>پسران شیردل و تهمتن و کندآور</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>به گه بزم‌، فرشته گه رزم‌، اهریمن</p></div>
<div class="m2"><p>سرو در زیرکله‌، ببر به زبر مغفر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>*‌</p></div>
<div class="m2"><p>*‌</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>من زبان وطن خویشم و دانم به یقین</p></div>
<div class="m2"><p>با زبانست دل مردم ایران همسر</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>آنچه آرم به زبان راز دل ایرانست</p></div>
<div class="m2"><p>بو که اندر دل یاران کند این راز اثر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>کی فراموش کند شوروی نیک‌نهاد</p></div>
<div class="m2"><p>که شد ایران پل پیروزی او سرتاسر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>گشت ما را ستخوان خرد که سالی سه‌چهار</p></div>
<div class="m2"><p>چرخ ییروزی بر سینهٔ ما داشت گذر</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>اینک از دوستی متفقین آن خواهیم</p></div>
<div class="m2"><p>که بخواهد پسر خسته و نالان ز پدر</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>باشد این هدیهٔ باکو اثرکلک بهار</p></div>
<div class="m2"><p>یادگاری که بماند به جهان تا محشر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>هست از آنگونه که استاد ابیوردی گفت</p></div>
<div class="m2"><p>«‌به سمرقند اگر بگذری ای باد سحر»</p></div></div>