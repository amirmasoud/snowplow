---
title: >-
    شمارهٔ ۲۴۰ - شجاعت ادبی
---
# شمارهٔ ۲۴۰ - شجاعت ادبی

<div class="b" id="bn1"><div class="m1"><p>مردن اندر شجاعت ادبی</p></div>
<div class="m2"><p>بهتر از چاپلوسی و جلبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من برآنم که نیست زیرسپهر</p></div>
<div class="m2"><p>صفتی چون شجاعت ادبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نجبای جهان شجاعانند</p></div>
<div class="m2"><p>به شجاعت در است منتجبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راست باش و مدار باک از کس</p></div>
<div class="m2"><p>این بود خوی مردم عصبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخت‌رویی زگربزی بهتر</p></div>
<div class="m2"><p>احمدی خوبتر ز بولهبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم بردار از آن کسان که سخن</p></div>
<div class="m2"><p>بیخ گوشی کنند و زبر لبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخنی راستا به مذهب من</p></div>
<div class="m2"><p>به ز سیصد نماز نیم‌شبی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفته‌ای عامیانه لیک صریح</p></div>
<div class="m2"><p>به ز هفتاد خطبهٔ عربی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طفل گستاخ نزد من باشد</p></div>
<div class="m2"><p>پیر و، آن پیرچربه گوی‌، صبی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در جهانند بخردان و ردان</p></div>
<div class="m2"><p>کمتر و بیشتر جبان و غبی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو از آن مردمان کمتر باش</p></div>
<div class="m2"><p>این بود معنی فزون‌طلبی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یار اهریمنند مکر و دروغ</p></div>
<div class="m2"><p>این‌چنین گفت زردهشت نبی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ازحَسَب مرد را شرف خیزد</p></div>
<div class="m2"><p>چیست فخر شرافتِ نَسَبی‌؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هان توگستاخی و شجاعت را</p></div>
<div class="m2"><p>هرزه‌لایی مگیر و بی‌ادبی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باادب‌باش‌و راست‌باش و صریح</p></div>
<div class="m2"><p>ره حق جوی ازآنچه می‌طلبی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مگزین مذهب از برای ذهب</p></div>
<div class="m2"><p>این بود فخرِ دوره ی ذهبی</p></div></div>