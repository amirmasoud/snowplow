---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>چشم فلک است بر ستمگر نگران</p></div>
<div class="m2"><p>بیدار شود ظالم ازین خواب گران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازکار نمانده این جهان گذران</p></div>
<div class="m2"><p>بر ما بگذشت و بگذرد بر دگران</p></div></div>