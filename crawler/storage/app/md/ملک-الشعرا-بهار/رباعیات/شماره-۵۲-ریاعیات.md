---
title: >-
    شمارهٔ ۵۲ - ریاعیات
---
# شمارهٔ ۵۲ - ریاعیات

<div class="b" id="bn1"><div class="m1"><p>تن چیست‌؟ مرکبی ز چندین معدن</p></div>
<div class="m2"><p>پرگشته ز میراث نیاکان کهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محکوم محیط و انقلابات زمن</p></div>
<div class="m2"><p>تن گر گنهی کند چه بحثی است به من‌؟</p></div></div>