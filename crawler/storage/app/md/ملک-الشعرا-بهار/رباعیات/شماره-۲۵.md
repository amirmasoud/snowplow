---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>امشب ز فراق دوست خوابم نبرد</p></div>
<div class="m2"><p>هم دل به سوی شمع و کتابم نبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که دو دیده آب حسرت بارد</p></div>
<div class="m2"><p>بیدار نشسته‌ام که آبم نبرد</p></div></div>