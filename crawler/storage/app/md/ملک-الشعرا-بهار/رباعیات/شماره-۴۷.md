---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>دیشب من و پروانه سخن می‌گفتیم</p></div>
<div class="m2"><p>گاه از گل و گه ز شمع‌، می‌آشفتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد صبح نه پروانه به جا ماند و نه من</p></div>
<div class="m2"><p>گل نیز پر افشاند که ما هم رفتیم</p></div></div>