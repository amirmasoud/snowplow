---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>آن کس که رموز غیب داند، نه تویی</p></div>
<div class="m2"><p>وان کو خط نابوده بخواند، نه تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندیشهٔ عاقبت مکن کز پس مرگ</p></div>
<div class="m2"><p>چیزی هم اگر از تو بماند، نه تویی</p></div></div>