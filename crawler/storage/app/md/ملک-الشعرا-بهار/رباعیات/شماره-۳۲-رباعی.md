---
title: >-
    شمارهٔ ۳۲ - رباعی
---
# شمارهٔ ۳۲ - رباعی

<div class="b" id="bn1"><div class="m1"><p>شد نیمی عمر در خرافات هدر</p></div>
<div class="m2"><p>وندر حیرت گذشت یک نیم دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و امروز به چنگ لاالهیم اندر</p></div>
<div class="m2"><p>ز الله مگر به مرگ یابیم خبر</p></div></div>