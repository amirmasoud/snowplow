---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ارباب که صنعت وجاهت فن اوست</p></div>
<div class="m2"><p>خون فقرا تمام بر گردن اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاوس بهشت است به صورت لیکن</p></div>
<div class="m2"><p>ابلیس نهفته زیر پیراهن اوست</p></div></div>