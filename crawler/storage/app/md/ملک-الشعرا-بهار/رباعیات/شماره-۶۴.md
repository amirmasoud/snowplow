---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>زان نرگس نیم مست مستم کردی</p></div>
<div class="m2"><p>زان قامت افراشته پستم کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویندکه بت همی شکست ابراهیم</p></div>
<div class="m2"><p>ای ابراهیم بت‌پرستم کردی</p></div></div>