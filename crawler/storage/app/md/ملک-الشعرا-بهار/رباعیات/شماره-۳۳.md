---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>گر مانده و ناتوان و گر خسته و زار</p></div>
<div class="m2"><p>ما وز طلبش دست کشیدن‌، زنهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افتان خیزان رسیم تا منزل دوست</p></div>
<div class="m2"><p>پرسان پرسان روبم تا خیمهٔ یار</p></div></div>