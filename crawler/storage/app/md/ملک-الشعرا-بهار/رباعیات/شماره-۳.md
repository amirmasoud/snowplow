---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>آزادی ماست اصل آبادی ما</p></div>
<div class="m2"><p>این است نتیجهٔ خدادادی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آزاد بزی ولی نگر تا نشود</p></div>
<div class="m2"><p>آزادی تو رهزن آزادی ما</p></div></div>