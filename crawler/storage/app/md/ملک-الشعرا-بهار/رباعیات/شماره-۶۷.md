---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>ای مادر اگر دسترسی داشتمی</p></div>
<div class="m2"><p>سنگ سیه ازگور تو برداشتمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را گل و خاک تیره پنداشتمی</p></div>
<div class="m2"><p>تنهات به زبر خاک نگذاشتمی</p></div></div>