---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>از خصم کشیدن به وفا مور و جفا</p></div>
<div class="m2"><p>برهان نزاکت است و دستور صفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کشور ما اصل نزاکت این است</p></div>
<div class="m2"><p>واوبلا وامصیبتا وا اسفا</p></div></div>