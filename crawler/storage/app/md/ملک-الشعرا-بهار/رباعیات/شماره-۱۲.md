---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>آیین جهان طبل جفا کوفتن است</p></div>
<div class="m2"><p>خایسک بلا بر سر ما کوفتن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این کشتن و این کشته شدن مردان راست</p></div>
<div class="m2"><p>کانجا که زنست رقص و پا کوفتن است</p></div></div>