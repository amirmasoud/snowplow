---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>آمادهٔ جنگ باش کاین چرخ حرون</p></div>
<div class="m2"><p>با نرم‌دلی با تو نگردد مقرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز با جنگ آماده نمی‌گردد صلح</p></div>
<div class="m2"><p>جزبا خون پاکیزه نمی گردد خون</p></div></div>