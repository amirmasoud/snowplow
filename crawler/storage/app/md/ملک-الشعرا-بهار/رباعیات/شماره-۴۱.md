---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>ای برده گل رازقی از روی تو رشک</p></div>
<div class="m2"><p>در دیدهٔ مه ز دود سیگار تو اشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که چو لاله داغدار است دلم</p></div>
<div class="m2"><p>گفتی که دهم کام دلت یعنی کشک</p></div></div>