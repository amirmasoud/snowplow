---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>گر زیر فلک فکر من آزاد نبود</p></div>
<div class="m2"><p>در حنجره‌ام این‌همه فریاد نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسعود گر اندیشهٔ آزاد نداشت</p></div>
<div class="m2"><p>از قلعهٔ نای خلق را یاد نبود</p></div></div>