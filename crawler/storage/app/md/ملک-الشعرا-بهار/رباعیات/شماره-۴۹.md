---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>ما درس صداقت و صفا می‌خوانیم</p></div>
<div class="m2"><p>آیین محبت و وفا می‌دانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبن بی‌هنران سفله ای دل مخروش</p></div>
<div class="m2"><p>کانها همه می‌روند و ما می‌مانیم</p></div></div>