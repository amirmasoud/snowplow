---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>سردار به شه گفت سپاهی از من</p></div>
<div class="m2"><p>امضای اوامر و نواهی از من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزل‌از من و نصب از من و دربار ازتو</p></div>
<div class="m2"><p>تخت ازتو و تاج از تو و شاهی از من</p></div></div>