---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>بر درگه خود پلنگ دربان کردن</p></div>
<div class="m2"><p>بر گلهٔ خویش گرگ چوپان کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سگ در بغل و مار به دامان کردن</p></div>
<div class="m2"><p>بهتر که جوی به سفله احسان کردن</p></div></div>