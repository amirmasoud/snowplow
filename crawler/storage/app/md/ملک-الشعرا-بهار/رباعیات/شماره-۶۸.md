---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>ای روح روان که فارغ از این بدنی</p></div>
<div class="m2"><p>جویای عزیزکردهٔ خویشتنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خفته به خاک‌، من تو هستم تو منی</p></div>
<div class="m2"><p>من فرزندم تو مادر ممتحنی</p></div></div>