---
title: >-
    شمارهٔ ۹ - غزل
---
# شمارهٔ ۹ - غزل

<div class="b" id="bn1"><div class="m1"><p>روی تو دیدم ز عمر دست کشیدم</p></div>
<div class="m2"><p>چشم مو کاش کور مرفت که تور نمدیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بچه آهوی چین بروکه مو امروز</p></div>
<div class="m2"><p>هرچه دویدم ردت‌، بذت نرسیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابرو و چشمای تو چار آس و تو شاهی</p></div>
<div class="m2"><p>دست خلی چار آس جورته دیدم</p></div></div>