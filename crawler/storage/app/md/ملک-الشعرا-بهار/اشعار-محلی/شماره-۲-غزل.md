---
title: >-
    شمارهٔ ۲ - غزل
---
# شمارهٔ ۲ - غزل

<div class="b" id="bn1"><div class="m1"><p>یقین درم اثر امشو به های های مو نیست</p></div>
<div class="m2"><p>که یار مسته و گوشش بگریه‌های مو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدا خدا چه ثمر ای موذنا کامشو</p></div>
<div class="m2"><p>خدا خدای شمایه خدا خدای مو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمود خو‌نمه پامال و خونبها مه نداد</p></div>
<div class="m2"><p>زدم چو بر دمنش دست‌، گفت پای مو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بریز خونمه با دست نازنین خودت</p></div>
<div class="m2"><p>چره که بیتر ازی هیچه خونبهای مو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهار اگر شو صدبار بمیرم از غم دوست</p></div>
<div class="m2"><p>بجرم عشق و محبت‌، هنوز جزای مو نیست</p></div></div>