---
title: >-
    شمارهٔ ۳ - غزل
---
# شمارهٔ ۳ - غزل

<div class="b" id="bn1"><div class="m1"><p>گفتی که ممیر وخته مو لبیکمه گفتم</p></div>
<div class="m2"><p>هی هی بخدا خوب تو گفتی مو شنفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شیر نر عشق‌، تقلای مو پوچه</p></div>
<div class="m2"><p>ای بوده مقدر که بچنگال توبفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا زور دری تیز بزن بازوی صیاد</p></div>
<div class="m2"><p>مو کِفتَرِ جُون سَختُم و آسُون نَمِیُفتُم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که بپایت نخلد خار و مو امشو</p></div>
<div class="m2"><p>با جاروی مژگون سر راه تو ره رُفتُم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیشو بخیال صدف سینهٔ صافت</p></div>
<div class="m2"><p>تا وقت سحر مُروِرِیِ اشک مُسُفتُم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همدوش بهارُم مو که هم‌جفتُمُ هم‌طاق</p></div>
<div class="m2"><p>در بی‌طَقَتی طاقُم و با یاد تو جُفتُم</p></div></div>