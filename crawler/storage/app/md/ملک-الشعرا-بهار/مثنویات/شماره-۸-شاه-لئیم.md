---
title: >-
    شمارهٔ ۸ - شاه لئیم
---
# شمارهٔ ۸ - شاه لئیم

<div class="b" id="bn1"><div class="m1"><p>پادشهی بود به عهد قدیم</p></div>
<div class="m2"><p>شیفتهٔ خوردنی و زر و سیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاغر پنداری و فربه بری</p></div>
<div class="m2"><p>ای عجب آکنده بر لاغری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فربهی مرد به مغز سر است</p></div>
<div class="m2"><p>فربه بی‌مغز بلی لاغر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فربه بی‌مغزکدویی است خوار</p></div>
<div class="m2"><p>لاغر پر مایه دُر شاهوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دو جهان سیم و زر او را و بس</p></div>
<div class="m2"><p>وز ملکی تاج سر او را و بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فسحت ملکیش ز اندازه بیش</p></div>
<div class="m2"><p>با نظری تنگ‌تر از مشت خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاصل مردم شده هر سو به‌باد</p></div>
<div class="m2"><p>لیک شه از مزرعه خویش شاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مملکت از جور وزیران تباه</p></div>
<div class="m2"><p>لیک نکو حاصل مزروع شاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زارع گرینده بر احوال خویش</p></div>
<div class="m2"><p>شاه‌ خوش از حاصل امسال خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیم و زر آورده بهم چون جهود</p></div>
<div class="m2"><p>داده پس آنگاه به تربیح و سود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نی غم خلق و نه غم مملکت</p></div>
<div class="m2"><p>بی‌خبر از بیش و کم مملکت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سود خور و زر طلب‌و چشم‌تنگ</p></div>
<div class="m2"><p>بی‌عظمت‌چون ‌نم‌خون‌ روز جنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه ز پی صلح‌، وزیری هژیر</p></div>
<div class="m2"><p>نه ز پی جنگ‌، سواری دلیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کف لئیمش نشد ازحرص و آز</p></div>
<div class="m2"><p>جز ز پی زر ستدن هیچ باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسته جز از زر ز دو گیتی نظر</p></div>
<div class="m2"><p>زر و دگر زر و دگر باز زر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پرطمع و کوردل و تیره‌جان</p></div>
<div class="m2"><p>پادشه و زارع و بازارگان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون که تجارت کند و زرع‌، شاه</p></div>
<div class="m2"><p>تاجر و زارع به که جوید پناه‌؟‌!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاه بکوشد ز پی سیم و زر</p></div>
<div class="m2"><p>لیک کند بخش بر اهل هنر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گه به سپه‌،‌ تا به رهش سر دهند</p></div>
<div class="m2"><p>گه به رعیت‌، که بدو زر دهند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شه که‌به یک‌دست دهد سیم و زر</p></div>
<div class="m2"><p>باز ستاندش به دست دگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بندهٔ دینار و عبید دِرم</p></div>
<div class="m2"><p>شاه رعیت نبود لاجرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گنج برآورد و سپه کرد پست</p></div>
<div class="m2"><p>بی‌سپهی نظم ولایت شکست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملک برآشفت و سیه گشت روز</p></div>
<div class="m2"><p>گشت نهان اختر گیتی‌فروز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خلق شتابان سوی درگه به خشم</p></div>
<div class="m2"><p>رخت فروبسته شه تنگ‌چشم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با دو سه فراش جگر سوخته</p></div>
<div class="m2"><p>با دو سه صندوق زر اندوخته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برکتف هر یک صندوق زر</p></div>
<div class="m2"><p>خم شده از بار گرانشان کمر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یک‌تن از آن سه ز تعب شد فکار</p></div>
<div class="m2"><p>آه برآورد و بیفکند بار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت به صندوق که ای گنج زر</p></div>
<div class="m2"><p>حاصل خون جگر رنجبر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خلق رسیدند و برآشفت کوی</p></div>
<div class="m2"><p>هان به خداوند خود از من بگوی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کانچه در اینجاست اگر شهریار</p></div>
<div class="m2"><p>بخش نمودی به سلاح و سوار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حالتش امروز به از این بُدی</p></div>
<div class="m2"><p>خفت و خواریش نه چندین ‌بُدی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گنج که سرمایهٔ سالاری است</p></div>
<div class="m2"><p>چون‌ نشود خرج‌، گرانباری است</p></div></div>