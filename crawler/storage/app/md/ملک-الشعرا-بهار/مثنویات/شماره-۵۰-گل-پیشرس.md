---
title: >-
    شمارهٔ ۵۰ - گل پیشرس
---
# شمارهٔ ۵۰ - گل پیشرس

<div class="b" id="bn1"><div class="m1"><p>به ماه سفندار یک ‌سال شید</p></div>
<div class="m2"><p>بتابید بر یاسمین سپید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشسته هنوز از ستم دست‌، دی</p></div>
<div class="m2"><p>ز ابرو برافشاند خورشیدخوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گره شد گلوگاه باد شمال</p></div>
<div class="m2"><p>هوای دژم را نکو گشت حال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به‌صد رنگ‌، سیمرغ زربن کلاه</p></div>
<div class="m2"><p>بزد تیر در چشم اسفند ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گدازید برف و بتابید شید</p></div>
<div class="m2"><p>بجوشید سبزه، بجنبید بید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو ده روز از آن پیش کاید بهار</p></div>
<div class="m2"><p>فریبنده خورشید شد گرم کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دستان خورشید و زرق سپهر</p></div>
<div class="m2"><p>بهاری پدیدار شد خوبچهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزد برگک تر سر از شاخ خشک</p></div>
<div class="m2"><p>پر از مشک شد زلفک بیدمشگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو سه ‌روز شب گشت ‌و شب ‌روز شد</p></div>
<div class="m2"><p>گل پیشرس گلشن‌افروز شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگار بهار و عروس چمن</p></div>
<div class="m2"><p>گل یاسمین زیور انجمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ‌یک ماه از آن پیش کایّام اوست</p></div>
<div class="m2"><p>برآمد ز مغز و برون شد ز پوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخندید بر چهر خورشید، روز</p></div>
<div class="m2"><p>به شب خفت پیش مه دلفروز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندانست کایدون نه هنگام اوست</p></div>
<div class="m2"><p>که برجای می زهر در کام اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به‌ ناگه طبیعت برآمد ز خواب</p></div>
<div class="m2"><p>فروخفت ‌خورشید و برشد سحاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بغرید باد از برکوهسار</p></div>
<div class="m2"><p>بیفتاد ناژو و خم شد چنار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمانه خنک طبعی آغاز کرد</p></div>
<div class="m2"><p>طبیعت به سردی سخن ساز کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیفتاد برف و بیفسرد جوی</p></div>
<div class="m2"><p>سیه زاغ در باغ شد بذله‌گوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سراسر بیفسرد و پژمرد باغ</p></div>
<div class="m2"><p>همان پیشرس گوهر شبچراغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شکرخند نازش به کنج لبان</p></div>
<div class="m2"><p>بیفسرد و دشنامش اندر زبان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنین است پاداش زود آمدن</p></div>
<div class="m2"><p>به امّید باطل فرود آمدن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من آن پیشرس غنچهٔ تازه‌ام</p></div>
<div class="m2"><p>که هرجا رسیده است آوازه‌ام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من آن نوگل برگ جان خورده‌ام</p></div>
<div class="m2"><p>به غفلت فریب جهان خورده‌ام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سبک راه صد ساله پیموده‌ام</p></div>
<div class="m2"><p>به بیگاه رخساره بنموده‌ام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به خون گرمی روزبشکفته‌ام</p></div>
<div class="m2"><p>ز دم‌سردی‌شب‌به‌خون خفته‌ام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز بی‌آبی عرف پژمرده‌ام</p></div>
<div class="m2"><p>ز سرمای عادات افسرده‌ام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نبوده در ایام یک روز شاد‌</p></div>
<div class="m2"><p>نخندیده در باغ یک بامداد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا دیر شد روز و بگذشت کار</p></div>
<div class="m2"><p>تو روز جوانی غنیمت شمار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بهار جوانیت سرسبز باد</p></div>
<div class="m2"><p>دلت خرم و خاطرت نغز باد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی باش خندان درین بوستان</p></div>
<div class="m2"><p>ز تو شاد و خرم دل دوستان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که من زین جهان چشم برداشتم</p></div>
<div class="m2"><p>لبان بستم و مژه برکاشتم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بهار مرا کرد گیتی خزان</p></div>
<div class="m2"><p>بهار منا نوبت تو است هان</p></div></div>