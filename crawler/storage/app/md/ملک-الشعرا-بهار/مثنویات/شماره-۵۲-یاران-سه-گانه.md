---
title: >-
    شمارهٔ ۵۲ - یاران سه گانه‌:‌
---
# شمارهٔ ۵۲ - یاران سه گانه‌:‌

<div class="b" id="bn1"><div class="m1"><p>یکی از بزرگان سه تن داشت یار</p></div>
<div class="m2"><p>به تیمار آن هر سه دائم دچار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر ناب و دیگر زنی سیم‌تن</p></div>
<div class="m2"><p>سه دیگر نکوکاری خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بگرفت مرگش گریبان که خیز</p></div>
<div class="m2"><p>خبر یافتند آن سه یار عزیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بالین آن نیک‌مرد آمدند</p></div>
<div class="m2"><p>دل‌افسرده و روی‌زرد آمدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شد خواجه‌ با آن‌ سه‌ تن‌ روبروی</p></div>
<div class="m2"><p>به یار نخستین چنین گفت اوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخت سرخ باد و تنت دیر پای</p></div>
<div class="m2"><p>که بر من اجل دوخت زرین‌ قبای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زرش گفت‌: بودی نگهدار من</p></div>
<div class="m2"><p>بسی داشتی رنج و تیمار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مرگت یکی شمع روشن کنم</p></div>
<div class="m2"><p>ستودانت را رشگ گلشن کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زر از وی جدا گشت و آمد زنش</p></div>
<div class="m2"><p>چو زر گشته از رنج‌، سیمین تنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دریده گریبان ز تیمار شوی</p></div>
<div class="m2"><p>خراشیده روی و پریشیده موی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوم یار را خواجه بدرود گفت</p></div>
<div class="m2"><p>سرشکش به مژگان بپالود جفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به سوگ توگفتا؛ من مستمند</p></div>
<div class="m2"><p>کنم موی کوتاه و مویه بلند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شتابم خروشان سوی گور تو</p></div>
<div class="m2"><p>بگریم برآن گور پر نور تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس از آن دو، یار سوم رفت پیش</p></div>
<div class="m2"><p>نه‌ عارض‌ شخوده‌،‌ نه گیسو پریش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه رخساره زرد و نه لرزان تنش</p></div>
<div class="m2"><p>نه چاک از غم دوست ییراهنش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پذیره شدش با دلی پر ز مهر</p></div>
<div class="m2"><p>به مانند افرشته‌ای خوب‌چهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو خواجه گفت‌:‌ ای‌ «‌نکویی» دریغ‌</p></div>
<div class="m2"><p>که مرگ آمد و نیست جای کریغ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز تو دور خواهم‌ شدن‌ چاره‌ چیست‌</p></div>
<div class="m2"><p>ز درد جدایی بباید گریست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نکوکاری انگشت بر لب نهاد</p></div>
<div class="m2"><p>که این خود بنپذیرم از اوستاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو در زندگی با تو بودم بسی</p></div>
<div class="m2"><p>پس از مرگ جز تو نخواهم کسی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به هرجا روی با تو من همرهم</p></div>
<div class="m2"><p>ندیمی نکوخواه وکار آگهم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درین گفتگو خواجه پیر خفت</p></div>
<div class="m2"><p>زر و زن‌ چو او خفت گشتند جفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سوی گور با برگ و ساز آمدند</p></div>
<div class="m2"><p>به گورش نهفتند و باز آمدند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی شمع بنهاد و دیگر گریست</p></div>
<div class="m2"><p>پس ‌آن‌ هر دو رفتند و کردار زیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ازو دوستان جمله گشتند دور</p></div>
<div class="m2"><p>جز آن‌ دوست کاو ماند با وی‌ به گور</p></div></div>