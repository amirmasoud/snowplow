---
title: >-
    شمارهٔ ۲۰ - نامهٔ منظوم
---
# شمارهٔ ۲۰ - نامهٔ منظوم

<div class="b" id="bn1"><div class="m1"><p>به قربان حضور شاهزاده</p></div>
<div class="m2"><p>که آیین وفا ازکف نهاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر نام‌آوران‌، اعزاز سلطان</p></div>
<div class="m2"><p>مهین چشم و چراغ آل خاقان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که رای ری نمود ازکشور طوس</p></div>
<div class="m2"><p>مرا بنهاد با یک شهر افسوس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون با شاهدان ری قرین است</p></div>
<div class="m2"><p>دلش فارغ ز یاد آن و این است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ری ارچه ‌جای غلمان ‌است‌ و حور است</p></div>
<div class="m2"><p>بهشت ‌ار خوانیش عین قصور است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهشتش کی‌توان ‌خواندن که‌ زشتست‌</p></div>
<div class="m2"><p>که هرکویش یکی خرّم بهشتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشا شهزاده و بوم و بر ری</p></div>
<div class="m2"><p>خوشا آزادی و آسایش وی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشا آن شاهدان سیم غبغب</p></div>
<div class="m2"><p>خوشا آن زود ره بردن به مطلب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر باشد مرا پری و بالی</p></div>
<div class="m2"><p>به‌سوی ری پرم بی‌قیل و قالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگر باشد مرا تاب و توانی</p></div>
<div class="m2"><p>به طوس اندر نمانم یک زمانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شوم‌، گیرم ره ملک ری از پیش</p></div>
<div class="m2"><p>مگر جویم در او کام دل خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگیرم دامن شهزادهٔ راد</p></div>
<div class="m2"><p>برآرم از جفای دهر فریاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر او سازم بیان بنهفتنی‌ها</p></div>
<div class="m2"><p>بدو گویم بسی ناگفتنی‌ها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حکایت‌ها کنم از بی‌وفاییش</p></div>
<div class="m2"><p>وزین بیگانگی و آن آشناییش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنون‌ شاها کجایی حال‌ چونست‌؟</p></div>
<div class="m2"><p>که ‌از هجر تو دل‌ها غرق‌ خونست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شدی با مهوشان ری هم‌آغوش</p></div>
<div class="m2"><p>ز مشتاقان خود کردی فراموش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه دلداری چنین باشد نه یاری</p></div>
<div class="m2"><p>که یاران را به‌ یاد اندر نیاری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو یارا با همه یاران به کینی‌؟‌!</p></div>
<div class="m2"><p>و یا خود با من‌ تنها چنینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>امید است آن‌که زین پس رام گردی</p></div>
<div class="m2"><p>بساط بی‌وفایی درنوردی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که تا زین بندگان آید سلامی</p></div>
<div class="m2"><p>فرستی هر سلامی را پیامی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو با خوبان آن کشور نشینی</p></div>
<div class="m2"><p>به یاد ما نمایی بوسه‌چینی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نپرهیزی ز چشم فتنه‌جویان</p></div>
<div class="m2"><p>درآویزی به زلف خوبرویان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو و آن لعبتان ماهزاده</p></div>
<div class="m2"><p>من و این مدرسه‌، وین شاهزاده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>غرض ‌خوش‌ باش و خرم با‌ش جاوید</p></div>
<div class="m2"><p>نشاط اندوز و بی‌غم باش جاوید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گهی شطرنج و گاهی آس میزن</p></div>
<div class="m2"><p>گهی پیمانه‌، گاهی لاس میزن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو با خوبان نشینی یاد ما کن</p></div>
<div class="m2"><p>همیشه با وفاداران وفا کن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کنون کاندر سرای مستشارم</p></div>
<div class="m2"><p>به یادت این بدیهه می‌نگارم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مگر روزی ز ما یادی نمایی</p></div>
<div class="m2"><p>تو نیز از هجر فریادی نمایی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سخن تا چند بافم زین زیاده</p></div>
<div class="m2"><p>زیادت باد عمر شاهزاده</p></div></div>