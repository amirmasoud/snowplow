---
title: >-
    شمارهٔ ۴۸ - خانهٔ آهن
---
# شمارهٔ ۴۸ - خانهٔ آهن

<div class="b" id="bn1"><div class="m1"><p>یکی پادشا خانه‌زآهن بساخت</p></div>
<div class="m2"><p>شبی آتش افتاد و آهن گداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پژوهش گرفتندکآن از چه بود</p></div>
<div class="m2"><p>شراری چنین بی‌امان از چه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس از جهد بسیار بردند راه</p></div>
<div class="m2"><p>به دود دل عاجزی بی گناه</p></div></div>