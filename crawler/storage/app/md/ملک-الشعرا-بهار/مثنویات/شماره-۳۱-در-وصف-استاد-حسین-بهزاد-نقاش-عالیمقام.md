---
title: >-
    شمارهٔ ۳۱ - در وصف استاد حسین بهزاد نقاش عالیمقام
---
# شمارهٔ ۳۱ - در وصف استاد حسین بهزاد نقاش عالیمقام

<div class="b" id="bn1"><div class="m1"><p>خداوند هنر، استاد بهزاد</p></div>
<div class="m2"><p>که نقش از خامهٔ بهزاد به زاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسین رادکِش بهزاد نام است</p></div>
<div class="m2"><p>کمال‌الدین بهزادش غلام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بود او نخست‌،‌ این هست اول</p></div>
<div class="m2"><p>اگر بود او کمال‌، این هست اکمل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رنگ‌آمیزی‌ از خورشید ییش‌ است</p></div>
<div class="m2"><p>به‌معنی‌آفتاب‌عصر خویش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به صورت شادی و غم می‌نماید</p></div>
<div class="m2"><p>غم و شادی مجسم می‌نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سحرانگیزی کلک گهرخیز</p></div>
<div class="m2"><p>به نقش جان دهد رنگ دلاویز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خداوندنگارین‌خامه‌«‌مانی‌»‌است</p></div>
<div class="m2"><p>ولیکن بندهٔ بهزاد ما نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>«‌منوهر» پیش این استاد، باری</p></div>
<div class="m2"><p>خجل گردد به طرح ریزه‌کاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز رشک کلک مویین سیه‌روش</p></div>
<div class="m2"><p>رضای اصفهانی شد سیه‌پوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز صنع خامهٔ چینی نمودش</p></div>
<div class="m2"><p>فرستد فرخ چینی درودش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پیش ریزه‌کاری‌های نغزش</p></div>
<div class="m2"><p>کمال‌الملک شد آشفته مغزش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رفائیل ار به عصرش زنده گردد</p></div>
<div class="m2"><p>بر ِ آن کلک قادر بنده گردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من ارچه در سخن هستم مسلم</p></div>
<div class="m2"><p>به وصفش عاجزم والله اعلم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهار اندر سخن گر داد دادست</p></div>
<div class="m2"><p>کلامش از دل بهزاد زادست</p></div></div>