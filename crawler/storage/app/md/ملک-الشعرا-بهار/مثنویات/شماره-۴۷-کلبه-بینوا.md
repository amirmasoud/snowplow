---
title: >-
    شمارهٔ ۴۷ - کلبه بینوا!
---
# شمارهٔ ۴۷ - کلبه بینوا!

<div class="b" id="bn1"><div class="m1"><p>به زیر درختان بی‌برگ و بر</p></div>
<div class="m2"><p>به زانو نهاده یکی کلبه سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کهن کلبه‌ای چفته و گوژپشت</p></div>
<div class="m2"><p>نمایندهٔ روزگار درشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شده پشتش از بار ییری دوتا</p></div>
<div class="m2"><p>ستون زیر سقفش به جای عصا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به‌‌بر کرده از صنعت کار تن</p></div>
<div class="m2"><p>یکی زشت خاکستری پیرهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرو برده دست دی و بهمنش</p></div>
<div class="m2"><p>در آهار یخ کهنه پیراهنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دیواره‌اش خاک‌ها ریخته</p></div>
<div class="m2"><p>یکی خاکدان گردش انگیخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دربچه به لب بسته قفل سکوت</p></div>
<div class="m2"><p>بر آن قفل مهری زده عنکبوت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درش رسم خاموشی آموخته</p></div>
<div class="m2"><p>دو لب چفت بر یکدگر دوخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو پیر اشتری لفچه آویخته</p></div>
<div class="m2"><p>وز اندام او موی‌ها ریخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فکنده برآن اشتر پشت ریش</p></div>
<div class="m2"><p>خرابی همه بار سنگین خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوی حفرهٔ نیستی خم شده</p></div>
<div class="m2"><p>به قربانگه مرگ زانو زده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو گویی که هست آن نهفته مغاک</p></div>
<div class="m2"><p>یکی کهنه کوری دمیده ز خاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز دهلیز آن جایگاه ندم</p></div>
<div class="m2"><p>بود یک قدم تا سرای عدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گیاهان دشتی به فصل بهار</p></div>
<div class="m2"><p>دمیده فراوان در آن رهگذار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز تاریکی سینه‌اش نرم‌نرم</p></div>
<div class="m2"><p>برآید همی میغگون آه گرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دمی خیزد از روزنش هر زمان</p></div>
<div class="m2"><p>چو در سخت‌سرما،‌ بخار از دهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آن کلبه‌، پیچیده دودی سفید</p></div>
<div class="m2"><p>برآید به مانند پیچ کلید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رود تا گشاید در آن داوری</p></div>
<div class="m2"><p>ز گوش سموات قفل کری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به مانند دود دل مستمند</p></div>
<div class="m2"><p>که گیرد گذر بر سپهر بلند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سبک روح پیکی از آن گور پست</p></div>
<div class="m2"><p>شتابد سوی کبریایی نشست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کزان روح مطرود کلبه‌نشین</p></div>
<div class="m2"><p>به یزدان پیامی برد آتشین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو گوید ای داور هور و ماه</p></div>
<div class="m2"><p>رهانندهٔ گرفه اا کار از گناه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در ین کلبه روحی فکار اندر است</p></div>
<div class="m2"><p>زنی رانده از روزگار اندر است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پریشیده از بیکسی موی او</p></div>
<div class="m2"><p>دو نوزاد خفته به زانوی او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز دو نرگسش ژاله بارد همی</p></div>
<div class="m2"><p>دو دستش به رخ لاله کارد همی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نخستین شکم تو أمان زاده است</p></div>
<div class="m2"><p>نرینه دو آرام جان زاده است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پدر مادرش هر دوان رفته‌اند</p></div>
<div class="m2"><p>در آن تل نزدیک ده خفته‌اند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جوانی که شوی عزیز وبست</p></div>
<div class="m2"><p>به زندان درون اشگ ریز وی است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو خرمن به مرداد مه گردگشت</p></div>
<div class="m2"><p>یکی عامل از شهر آمد به‌دشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به‌تندی برافزود و ز آزرم کاست</p></div>
<div class="m2"><p>خراج نود ساله زان بوم خواست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دواج نوین جست وگستردنی</p></div>
<div class="m2"><p>ز مرغ و بره گونه گون‌خوردنی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یخ و آب‌ لیموی شیراز خواست</p></div>
<div class="m2"><p>می و رود ویارخوشآواز خواست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کشاورز مسکین شگفت آمدش</p></div>
<div class="m2"><p>بخندید و خوش‌داستانی‌زدش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که در خانهٔ خرس انگور و سیب</p></div>
<div class="m2"><p>نیابی‌، مده خویشتن را فریب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جوین کاک‌وکشکینه‌وشیر و ماست</p></div>
<div class="m2"><p>درین ده خوراک گوارای ماست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو مهمان ناخوانده آید به من</p></div>
<div class="m2"><p>بود خرجش از مطبخ خویشتن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که گر گوسپندیست‌،‌سرمایه‌راست</p></div>
<div class="m2"><p>وگر ماکیانی بود، خایه‌راست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رود گندم و روغن و سیب و به</p></div>
<div class="m2"><p>به خرج خراج و خداوند ده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر این بیزبانان شبانی کنیم</p></div>
<div class="m2"><p>ز محصولشان زندگانی کنیم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شکالی اگر ماکیانی برد</p></div>
<div class="m2"><p>چنانست کز ما جوانی برد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دگر این که ما بی‌خبر بوده‌ایم</p></div>
<div class="m2"><p>نزول تو از پیش نشنوده‌ایم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مگر چون تو مهمان والانژاد</p></div>
<div class="m2"><p>که بر دیدگان بایدت جای داد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>عروسی نوست اندرین سرزمین</p></div>
<div class="m2"><p>که بسترش پاکست و بالش نوین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جوانیست شوهرش پاکیزه‌روی</p></div>
<div class="m2"><p>بفرمای و بنشین به مشکوی اوی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز هر چیزکاینجا فراهم شود</p></div>
<div class="m2"><p>بیاریم تا دلت خرم شود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به پیشش یکی خوان نهادندگرم</p></div>
<div class="m2"><p>در او بره و مرغ و نان‌های نرم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بداندیش‌زآنان‌می‌وجام‌خواست</p></div>
<div class="m2"><p>چو می درنیامد به ‌دشنام‌ خواست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بزد پای بر خوانچهٔ خوردنی</p></div>
<div class="m2"><p>بیالود از آن فرش و گستردنی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بغرید بر میزبانان چو دیو</p></div>
<div class="m2"><p>برآورد از آن بوم و برزن غریو</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گریبان داماد را بردرید</p></div>
<div class="m2"><p>زن تازه را چادر از سر کشید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جوانمرد را تاب خواری نماند</p></div>
<div class="m2"><p>زدش سیلیئی چند و از در براند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بد اندیش از آن بوم‌ برگشت تفت</p></div>
<div class="m2"><p>پی چاره‌جویی سوی شهر رفت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کمان جفا را بزه کرد راست</p></div>
<div class="m2"><p>بزد تیر بر قلب هر کس که خواست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به نزد رئیس اداره دوید</p></div>
<div class="m2"><p>ز مژگانش اشگ دروغین چکید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدو گفت چون در فلان بوم پای</p></div>
<div class="m2"><p>نهادم که فرمانت آرم بجای</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جوانی به پیکارم آمد چو گرک</p></div>
<div class="m2"><p>بر او گرد گشتند خرد و بزرگ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سقط گفت بر شهر و بر شهریار</p></div>
<div class="m2"><p>به میر و وزیر و سران دیار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مرا راند از آن‌ ده ‌به ‌چوب ‌و به ‌سنگ</p></div>
<div class="m2"><p>هم ‌اندر نهان داشت حاضر تفنگ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>من از بیم غوغا و خون‌ ریختن</p></div>
<div class="m2"><p>برون تاختم گرم از آن انجمن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>برآنم که در چاره چستی کنی</p></div>
<div class="m2"><p>عدو سخت گردد،‌ چو سستی کنی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>رئیس ‌از فسونش ‌چنان ‌خیره گشت‌</p></div>
<div class="m2"><p>که چشم جهان‌بین او تیره گشت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز لشکر بدو داد ده نامدار</p></div>
<div class="m2"><p>همه از در کوشش و کارزار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>برفتند بر عزم کین توختن</p></div>
<div class="m2"><p>بر آن بوم و بر آتش افروختن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شد آن ناجوانمرد شهوت‌پرست</p></div>
<div class="m2"><p>بدان‌ده که دوشینه‌بودش نشست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>درآمد ز ره چون یل اسفندیار</p></div>
<div class="m2"><p>تفنگی به‌دست از پی کارزار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>پس و پشت او ده سوار هژیر</p></div>
<div class="m2"><p>همه گرد و پیل‌افکن و شیرگیر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بر آن بیگناهان شبیخون زدند</p></div>
<div class="m2"><p>زن و مرد وکودک به‌هامون زدند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>جوانمرد داماد در خانه بود</p></div>
<div class="m2"><p>غنوده به نزدیک جانانه بود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گرفته سرزلف دلبر به چنگ</p></div>
<div class="m2"><p>که‌ازکوی‌برخاست‌غوغای جنگ</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>یورش برد بدخواه بر خانه‌اش</p></div>
<div class="m2"><p>شکستش درو شد به کاشانه‌اش</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>جوان جست آسیمه از خوابگاه</p></div>
<div class="m2"><p>بر آن دستهٔ شوم بربست راه</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>یکی‌مشت زد بر سرکینه‌جوی</p></div>
<div class="m2"><p>که افتاد ناکس ز بالا به روی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گرفتش کمربند و برداشت خوار</p></div>
<div class="m2"><p>سپر کردش اندر به راه سوار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>عروس از پس پشت او بیدرنگ</p></div>
<div class="m2"><p>روان کرده‌ بر دشمنان‌چوب و سنگ</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>کمرگاه کوهی‌بر آن کوچه بود</p></div>
<div class="m2"><p>به کوه اندر آمد جوانمرد زود</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>عروس از پیش جست در کوهسار</p></div>
<div class="m2"><p>بداندیش افتاده در کوچه خوار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سواران به یغما گشودند دست</p></div>
<div class="m2"><p>ز یغمای آنان جوانمرد رست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>زن آبستن و مرد خسته ز جنگ</p></div>
<div class="m2"><p>خدا را چه سازند درکوه و سنگ</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ز بالا ره سخت و دشوار کوه</p></div>
<div class="m2"><p>به زبر اندرون گیرودار گروه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>برفتند آن شب‌همی تا سحر</p></div>
<div class="m2"><p>سحرگه به سنگی نهادند سر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چوخورشید سر برزد از کوهسار</p></div>
<div class="m2"><p>از آن کوه جستند راه فرار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به زیر درختان بی‌برگ و بار</p></div>
<div class="m2"><p>کهن کلبه‌ای بود نااستوار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>جوانمرد آن کلبه را رُفت پاک</p></div>
<div class="m2"><p>فرو رفت تا سر در آن ‌تل خاک</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به زن درد آبستنی چیره شد</p></div>
<div class="m2"><p>جوانمرد از آن ماجرا خیره شد</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>برآشفت وگفت ای بت نازنین</p></div>
<div class="m2"><p>روم تا پزشگیت آرم گزین</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>فرود آمد ازکوه دیوانه‌وار</p></div>
<div class="m2"><p>مگر خواهد از دشمنان‌ زینهار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ز درّه بپیچید و شد سوی راه</p></div>
<div class="m2"><p>ز جان شسته دست‌ و دلی بیگناه</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ندانست کاین‌دیوکش‌زدبه‌مشت</p></div>
<div class="m2"><p>هم‌اندر زمانش‌بدان مشت کشت</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>سواران چو دیدند آن کشته را</p></div>
<div class="m2"><p>مران بدرک بخت برگشته را</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به کاخ جوان آتش افزوختند</p></div>
<div class="m2"><p>همه خانه‌اش سر بسر سوختند</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>هم از کدخدایان و مردان ده</p></div>
<div class="m2"><p>ببردند از بهر آن خون زده</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو مستان بر آن برزن آشوفتند</p></div>
<div class="m2"><p>همه روستا سر بسر روفتند</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>خر و گاو بردند و هم گوسفند</p></div>
<div class="m2"><p>ستوران باری و اسب نوند</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>دواندندشان پیش مرکب به قهر</p></div>
<div class="m2"><p>پیاده ببردند تازان به شهر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>جوان ساده‌دل بود و هم بیخبر</p></div>
<div class="m2"><p>و دیگر که جفتش به خون هشته سر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>دوان تاخت ازکوه زی بوم رُست</p></div>
<div class="m2"><p>که مامایی آرد پی جفت چست</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چودیدندنش آن مردم دون همی</p></div>
<div class="m2"><p>که بودند ترسان از آن‌خون همی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>جوان راگرفتند و بستند دست</p></div>
<div class="m2"><p>به‌خواری به کنجی فکندند پست</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>جوان‌ چون‌ شنید آن که‌ خون‌ ریخته‌ است</p></div>
<div class="m2"><p>چنان‌صعب‌شوری‌برانگیخته است</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>فرو ماند بیچاره در کار خویش</p></div>
<div class="m2"><p>دلی پر ز سوز از غم یار خویش</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بترسید کان راز گوید همی</p></div>
<div class="m2"><p>که دشمن به زن راه جوبد همی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>درین بود کامد ز ره دسته‌ای</p></div>
<div class="m2"><p>به کین جستن دهِ‌میان بسته‌ای</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>گرفتند از آن مرد خونی سراغ</p></div>
<div class="m2"><p>به کف‌بر ز دشنام‌و خشیت چراغ</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چو دیدند بسته‌ ز کین‌ دست‌ و پاش</p></div>
<div class="m2"><p>گرفتند و بردند و شد قصه فاش</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>به شهر اندر افتاد از اینسان خبر</p></div>
<div class="m2"><p>که خونی‌جوانی کشیده است سر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>به شه کرده ‌طغیان ‌و عاصی شده‌ است</p></div>
<div class="m2"><p>فراوان ره کاروانان زده است</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بکشته است تحصیلداری هژیر</p></div>
<div class="m2"><p>بپا کرده در روستا داروگیر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>سواران شه جنگ‌ها کرده‌اند</p></div>
<div class="m2"><p>که وی را به بند اندر آورده‌اند</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>نبشتند در نامه‌ها، چامه‌ها</p></div>
<div class="m2"><p>بفرسود از آن چامه‌ها، خامه‌ها</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ره داورستان پر انبوه گشت</p></div>
<div class="m2"><p>چو خونی ‌سوی‌ داورستان گذشت</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>در آن داوری قصه معلوم شد</p></div>
<div class="m2"><p>در آن‌ خون جوانمرد محکوم شد</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>به زندان درافتاد از آن داوری</p></div>
<div class="m2"><p>چنین کارها کی بود سرسری</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>برآمد ز هرکوی وبرزن غریو</p></div>
<div class="m2"><p>که باید بریدن سر نرّه دیو</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چنین دیو و عفریت مردم‌شکار</p></div>
<div class="m2"><p>گروه بشر را نیاید به کار</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>کسی‌را که‌خون‌ربختن‌پیشه است</p></div>
<div class="m2"><p>دل مردم از وی پر اندیشه است</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>به داد و به دین بایدش زد به دار</p></div>
<div class="m2"><p>و گرنه شود شیر مردم‌شکار</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>سر مرد خونخواره در خاک به</p></div>
<div class="m2"><p>ز ناپاک مردم‌، جهان پاک به</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>قصاص ‌ارچه ‌خون‌ را به‌ خو‌ن شسنن‌است</p></div>
<div class="m2"><p>و لیکن ‌به‌ صد حکمت ‌آبستن است</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>به بادافره خون‌، بریده سری</p></div>
<div class="m2"><p>بود مایهٔ عبرت دیگری</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>حکیمی در آن شهر پر داد و دین</p></div>
<div class="m2"><p>ز بی‌دینی و فقر، گوشه‌نشین</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>سوی نامه‌داران یکی نامه کرد</p></div>
<div class="m2"><p>درفشی نوین بر سر خامه کرد</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>نبشت اندر آن نامهٔ دادخواه</p></div>
<div class="m2"><p>که ای نامه‌داران بادستگاه</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>قلمتان به کف دشنه بینم همی</p></div>
<div class="m2"><p>زبانتان به خون تشنه بینم همی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>نه کاری بود سهل خون ربختن</p></div>
<div class="m2"><p>روان کسی از تن انگیختن</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>فزون از شمر سال بگذشته است</p></div>
<div class="m2"><p>کجا جانور آدمی گشته است</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>فزون از شمر مرد رفته ز دهر</p></div>
<div class="m2"><p>که در دهرش از زن نبوده است بهر</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>فزون از شمر نطفه رفته ز هم</p></div>
<div class="m2"><p>که زهدان یکی را کشیده بدم</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>خبه کرده زهدان فزون از شمر</p></div>
<div class="m2"><p>که یک تن ز زهدان برآورده سر</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>فزون از شمرد مرده کودک همی</p></div>
<div class="m2"><p>کز آنان یکی کشته ریدک‌ همی</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>فزون از شمر مرده ریدک ز درد</p></div>
<div class="m2"><p>کز آنان یکی مانده و گشته مرد</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>یکی مرد، سرمایه ی عالم است</p></div>
<div class="m2"><p>به‌نزد یکی مرد، عالم کم است</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>به ویژه چنین نوجوان هژیر</p></div>
<div class="m2"><p>کشاورز و محنت کش وتیز ویر</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ز گیتی یکی گوشه کرده پسند</p></div>
<div class="m2"><p>زنی و دو تا گاو و ده گوسپند</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>مه و سال در آفتاب و دمه</p></div>
<div class="m2"><p>گهی پشت گاو و گهی با رمه</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>شده تازه از کوشش جانتان</p></div>
<div class="m2"><p>فراهم ازو روغن و نانتان</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>همان پنبه و پشم و مرغ و بره</p></div>
<div class="m2"><p>ز بهر شما ساخته یکسره</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>خورش کرده خود نان کاک جوین</p></div>
<div class="m2"><p>فرستاده بهر شما انگبین</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>نه دریوزه کار و نه تاراج گر</p></div>
<div class="m2"><p>سخی‌طبع و روشندل و رنجبر</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>عوانی فرستید در خانه‌اش</p></div>
<div class="m2"><p>که ویران کند بوم و کاشانه‌اش</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>ز یکسوی محصولش آفت زده</p></div>
<div class="m2"><p>محصل ز سوی دگر آمده</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>از او بره و مرغ و می خواسته</p></div>
<div class="m2"><p>فراشی ز دیبای پیراسته</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>فرود آمده در سرایش به زور</p></div>
<div class="m2"><p>به همسرش بردوخته چشم شور</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>پس آن که سواران ببرده ز شهر</p></div>
<div class="m2"><p>که ‌زی‌شهرش ‌آرند از آن ده به قهر</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>سواران بده ربخته نیمشب</p></div>
<div class="m2"><p>در انداخته ‌جنگ و جوش و جلب</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>عوان فرومایه بشکسته در</p></div>
<div class="m2"><p>به‌خانه به طمع زنش برده سر</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>پس آنگه ز یک‌ مشت مرد دلیر</p></div>
<div class="m2"><p>عوان زبون‌، گشته از عمر سیر</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>کشندهٔ عوان نیست مرد جوان</p></div>
<div class="m2"><p>جوان بیگناهست و جانی عوان</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>گر او را به‌حجت زبان چیر نیست</p></div>
<div class="m2"><p>چرا مر شما را دل آژیر نیست</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>کشاورز، اندام و دهیو، بدن</p></div>
<div class="m2"><p>مبرید اندام دهیو ز تن</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>به ار صد عوان کشته آید به تیغ</p></div>
<div class="m2"><p>که یک مرد دهقان بگیرد گریغ</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>قصاص ار ز آدم کشی کاستی</p></div>
<div class="m2"><p>ز آدم کشان نام برخاستی</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>گنه کاره را نیست کشتن هنر</p></div>
<div class="m2"><p>گنه را ببایست کشت ای پسر</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>برآهنج‌ تخم گنه را ز دهر</p></div>
<div class="m2"><p>بر آن تخم بپراکن از علم‌، زهر</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>چو تخم گنه شد برون از نهاد</p></div>
<div class="m2"><p>شود دیو خونخواره‌، مردم‌نژاد</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>هم‌آن‌را که‌ خون ریختن گشته خوی</p></div>
<div class="m2"><p>نگر تا چه رفته است درکار اوی</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>کسی سرسری خون نریزد همی</p></div>
<div class="m2"><p>به رغبت به کین برنخیزد همی</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>به مغز اندرش هست بیماریئی</p></div>
<div class="m2"><p>و یا در دلش کینهٔ کاریئی</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>ز مستی‌، گه و گه ز دیوانگیست</p></div>
<div class="m2"><p>کجا مست‌ و دیوانه‌ را هوش نیست</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>گهی بهر زرّست وگه بهر زن</p></div>
<div class="m2"><p>تو بیخ زن و زر ز گیتی بزن</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>چو زین‌ها گذشتی‌ سبب‌ها ست راست</p></div>
<div class="m2"><p>نگه کن که اصل سبب‌ها کجاست</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>به هر معنی از این معانی که بود</p></div>
<div class="m2"><p>نبایست خونریز را کشت زود</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>اگر هست بیمار، مدهوش ساز</p></div>
<div class="m2"><p>دماغش بدست آر و داروش ساز</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>چو تخم جنایت نباشد به شهر</p></div>
<div class="m2"><p>برد مرد جانی ز درمانت بهر</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>وگرنه به زندان به کارش گمار</p></div>
<div class="m2"><p>برو توشه از مزدکارش شمار</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>وگر کینی اندر دلش کرده جای</p></div>
<div class="m2"><p>به پند و نصیحت دلش برگرای</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>ور از آب مستی است آگاه نیست</p></div>
<div class="m2"><p>بجز منع می در جهان راه نیست</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>تو بیخ می از انجمن برفکن</p></div>
<div class="m2"><p>که مستان نجوشند در انجمن</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>مر آن مست را دار سختش به‌بند</p></div>
<div class="m2"><p>که بر مست و دیوانه‌بند است پند</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>وگر کاری افتاده زین‌ها برون</p></div>
<div class="m2"><p>کشندنه‌جانی است‌نی‌مست‌و دون</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>نیش کین دیرین نیش طمع زر</p></div>
<div class="m2"><p>نه جویای شهرت نه پرخاشخر</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>چنان‌دان که هرگزگناهیش نیست</p></div>
<div class="m2"><p>نگه کن که اینجاگنه کار نیست</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>بسا اوفتد کارها این‌چنین</p></div>
<div class="m2"><p>که خیره شود مرد با داد و دین</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>ببایست جستن سبب را ز بن</p></div>
<div class="m2"><p>از آن پیش کان کار گردد کهن</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>من اکنون بر آنم که مرد عوان</p></div>
<div class="m2"><p>گنه را سبب شد نه مرد جوان</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>به من بردو چشمش دهند آگهی</p></div>
<div class="m2"><p>که مغز از جنایتش باشد تهی</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>نه‌بوده‌است کین گستری‌پیشه‌اش‌</p></div>
<div class="m2"><p>نه بر رهزنی بوده اندیشه‌اش</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>نه‌ مِی‌خورده‌ هرگز،‌نه‌ دیوانه‌ است</p></div>
<div class="m2"><p>جوانی نکوروی و فرزانه است</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>ولیکن عوان بداندیش زشت</p></div>
<div class="m2"><p>پدیداست‌تاخودچه‌داردسرشت</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>به ده رفته و آتش افروخته</p></div>
<div class="m2"><p>بر و بوم بیچارگان سوخته</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>شکسته اوانی به کردار خوک</p></div>
<div class="m2"><p>دونده به قصد زن نو بیوک‌١</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>لتی‌خورده از شوی و رفته به قهر</p></div>
<div class="m2"><p>سواران بیاورده از سوی شهر</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>سواران دویده به کردار دیو</p></div>
<div class="m2"><p>برآورده زان بوم و برزن غریو</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>به کین توختن دردویده عوان</p></div>
<div class="m2"><p>دژ آهنگ سوی سرای جوان</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>گرفته گریبان‌، کش از پیش زن</p></div>
<div class="m2"><p>کشد بیگنه بر سر انجمن</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>جوانش‌زده مشت و رانده ز پیش</p></div>
<div class="m2"><p>سپرکرده او را پی جان خویش</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>گر ایدون نمی‌کرد بیمار بود</p></div>
<div class="m2"><p>به نزدیک دانا گنه کار بود</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>نگرکاین سبب‌ها که گفتم تمام</p></div>
<div class="m2"><p>به مرد جوان بسته باشد کدام</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>سبب‌هاهمه‌زان عوان بوده است</p></div>
<div class="m2"><p>نتاجش‌به‌دست جوان بوده است</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>یکی روزنامه نبشت این مقال</p></div>
<div class="m2"><p>به شهر اندر افتاد از آن قیل و قال</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>وکیل جوان در دگر داوری</p></div>
<div class="m2"><p>همیدون شد اندر سخن گستری</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>به پرسش برفتند مردان راست</p></div>
<div class="m2"><p>شنیدند کان گفته‌ها پابجاست</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>نگه کرد قاضی در آن داوری</p></div>
<div class="m2"><p>در آن راه و رسم سخن کستری</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>چنین گفت کاین گفته‌ها باطلست</p></div>
<div class="m2"><p>اگر خوب اگر بد جوان قاتلست</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>به فرمان دین و به حکم جزا</p></div>
<div class="m2"><p>ببایست دادن به قاتل سزا</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>تنش باید از دار آویخته</p></div>
<div class="m2"><p>روانش سوی دوزخ انگیخته</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>گرفتم که قاضیش بخشد خلاص</p></div>
<div class="m2"><p>چه‌بایست کردن به دعوای خاص</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>خصوصی‌بر او مدعی خاستست</p></div>
<div class="m2"><p>دیت‌ رد نموده‌ است‌ و کین‌خواستست</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>ز مرگش همانا نباشدگزیر</p></div>
<div class="m2"><p>که عبرت پذیرند برنا و پیر</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>رقم کرد قاضی به مرگ جوان</p></div>
<div class="m2"><p>نمودند سوی تمیزش روان</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>در آن حوزه هم حکم ابرام یافت</p></div>
<div class="m2"><p>جوان را زمان یک سر انجام یافت</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>چو محکوم‌شد مرگ‌راساخت مرد</p></div>
<div class="m2"><p>ولی دل ز اندیشهٔ زن به‌درد</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>هم آنگه حکیمی که آن نامه کرد</p></div>
<div class="m2"><p>به پشتیش در نامه هنگامه کرد</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>بیامدکه بیند جوان را به بند</p></div>
<div class="m2"><p>از آن پیش کش حلق گیرد کمند</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>بدادش بسی پند و دل‌شاد کرد</p></div>
<div class="m2"><p>ز همٌ و غم مرگش آزاد کرد</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>بگفتش که ای‌دوست‌مردن‌دمیست</p></div>
<div class="m2"><p>به‌چنگ‌ اجل‌ جان‌سپردن دمی ست</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>غم مرگ از مرگ ناخوشترست</p></div>
<div class="m2"><p>مخور غم که‌ یک‌تن‌ ز مردن نرست</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>اگر پادشاهست‌، اگر بینوا</p></div>
<div class="m2"><p>سرانجام او مرگ باشد روا</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>دو روزی اگر دیر یا زود شد</p></div>
<div class="m2"><p>چو بینی همه بوده نابود شد</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>بمیر ای پسر در جهان بیگناه</p></div>
<div class="m2"><p>بر این بیگناهیت عالم گوا</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>ز داد و ز دین بر تو رفت این ستم</p></div>
<div class="m2"><p>که این داد و دین از جهان باد کم</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>کنون‌ هرچه‌ خواهی‌ ازین‌ دوست‌ خواه</p></div>
<div class="m2"><p>بجز جان که شد برخی دادگاه</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>ترا جان شکاری بودکنده پر</p></div>
<div class="m2"><p>به چنگ قوانین مردم شکر</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>ولیکن گرت پویه ای در دلست</p></div>
<div class="m2"><p>به‌ من گوی‌ اگر چند بس مشکلست</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>جوان گفت بُد مر مرا زن یکی</p></div>
<div class="m2"><p>مگر زاده باشد کنون کودکی</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>به هنگام زادن به تیمار اوی</p></div>
<div class="m2"><p>دویدم که ماما کنم جستجوی</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>فتادم به چنگال مردم کشان</p></div>
<div class="m2"><p>از آن‌ پس ندارم ز همسر نشان</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>خبر گیر باری ز دلبند من</p></div>
<div class="m2"><p>نگهدار او باش و فرزند من</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>یکی باغ دارم یکی خانه نیز</p></div>
<div class="m2"><p>دوتا گاو و دیگر فرومایه چیز</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>اگر مانده باشند اینجا بجای</p></div>
<div class="m2"><p>به فرزند و زن بخش بهر خدای</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>یکی دار کردند در اسپریس</p></div>
<div class="m2"><p>به گردش جوانان پتیاره ریس</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>جوان را کشیدند بسته دو دست</p></div>
<div class="m2"><p>غریوان و غران به کردار مست</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>ز مرگ جوان مرد و زن سوگوار</p></div>
<div class="m2"><p>تنیده همه گرد بر گرد دار</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>یکی قاضی آمد به کف تیغ مرگ</p></div>
<div class="m2"><p>به مجرم فرو خواند یرلیغ مرگ</p></div></div>
<div class="b" id="bn224"><div class="m1"><p>هم آن گه به دارش درآویختند</p></div>
<div class="m2"><p>تماشاییان در هم آمیختند</p></div></div>
<div class="b" id="bn225"><div class="m1"><p>زمانی بپیچید و پس گشت سرد</p></div>
<div class="m2"><p>به یک‌دم گل سرخ او گشت زرد</p></div></div>
<div class="b" id="bn226"><div class="m1"><p>زبانش برون جست ازکنج لب</p></div>
<div class="m2"><p>به دندان فشرده زبان از غضب</p></div></div>
<div class="b" id="bn227"><div class="m1"><p>رخان کرده آماس و لب‌ها سیاه</p></div>
<div class="m2"><p>فکنده به گیتی ز حسرت نگاه</p></div></div>
<div class="b" id="bn228"><div class="m1"><p>یکی باد آمد هم اندر زمان</p></div>
<div class="m2"><p>بگرداندش اندر سر ریسمان</p></div></div>
<div class="b" id="bn229"><div class="m1"><p>توگفتی که شاهد پذیرد همی</p></div>
<div class="m2"><p>گواهی بر آن کشته گیرد همی</p></div></div>
<div class="b" id="bn230"><div class="m1"><p>توگفتی که گوید نسیم صبا</p></div>
<div class="m2"><p>که ای کشتهٔ بیگنه مرحبا!</p></div></div>
<div class="b" id="bn231"><div class="m1"><p>ز دین بود اگر قاضی این داد داد</p></div>
<div class="m2"><p>که لعنت برین دین و این داد باد</p></div></div>
<div class="b" id="bn232"><div class="m1"><p>گرین‌ داد و دین‌ است‌ پس کفر چیست‌؟</p></div>
<div class="m2"><p>بر این داد ودین بربباید گریست</p></div></div>
<div class="b" id="bn233"><div class="m1"><p>به جان بشر دست یازیدنا</p></div>
<div class="m2"><p>بود با خداوند جنگیدنا</p></div></div>
<div class="b" id="bn234"><div class="m1"><p>هر آن‌ دین که باشد بنایش به خون</p></div>
<div class="m2"><p>بد است‌ ار شریفست‌ اگر هست دون</p></div></div>
<div class="b" id="bn235"><div class="m1"><p>ازآن شب که شد بسته مرد فقیر</p></div>
<div class="m2"><p>برآمد چهل روز و مسکین اسیر</p></div></div>
<div class="b" id="bn236"><div class="m1"><p>زن تازه زای اندر آن خاکدان</p></div>
<div class="m2"><p>نشسته به امید مرد جوان</p></div></div>
<div class="b" id="bn237"><div class="m1"><p>دوکودک بزاد اندر آن تنگنا</p></div>
<div class="m2"><p>به چادر بپوشیدشان‌، بینوا</p></div></div>
<div class="b" id="bn238"><div class="m1"><p>چو شد روز،‌ مردی‌ شبان دررسید</p></div>
<div class="m2"><p>کجاگو سیندانش آنجا‌ چرید</p></div></div>
<div class="b" id="bn239"><div class="m1"><p>هم آن کلبه خود بود جای شبان</p></div>
<div class="m2"><p>. ر * ب . *‌ر-</p></div></div>
<div class="b" id="bn240"><div class="m1"><p>زن دربدر را بدید و شناخت</p></div>
<div class="m2"><p>در آنجا غنودی به روز و شبان</p></div></div>
<div class="b" id="bn241"><div class="m1"><p>برافروخت آتش‌، بکرد آب گرم</p></div>
<div class="m2"><p>ز پشمینه‌اش جای آرام ساخت</p></div></div>
<div class="b" id="bn242"><div class="m1"><p>به‌شیر و به‌سرشیر،‌زن را نواخت</p></div>
<div class="m2"><p>بشست آن دو نوزاد را نرم نرم</p></div></div>
<div class="b" id="bn243"><div class="m1"><p>چو شب اندر آمد فروبست در</p></div>
<div class="m2"><p>دلش‌را به آواز خوش گرم ساخت</p></div></div>
<div class="b" id="bn244"><div class="m1"><p>همی گشت تا روز آنجا شبان</p></div>
<div class="m2"><p>برون ماند و تا روز ننهاد سر</p></div></div>
<div class="b" id="bn245"><div class="m1"><p>لع‌</p></div>
<div class="m2"><p>بسان یکی نامور پاسبان</p></div></div>
<div class="b" id="bn246"><div class="m1"><p>سحر چون بیاراست خورشید زرد</p></div>
<div class="m2"><p>به تیریژ زر چادر لاجورد</p></div></div>
<div class="b" id="bn247"><div class="m1"><p>شبان اندر آمد به صحرا زکوه</p></div>
<div class="m2"><p>که جوید نشان جوان از گروه</p></div></div>
<div class="b" id="bn248"><div class="m1"><p>شبانی نیاموخته رسم و راه</p></div>
<div class="m2"><p>ندانسته هرگز ثواب از گناه</p></div></div>
<div class="b" id="bn249"><div class="m1"><p>ز خردی به کوه و بیابان شده</p></div>
<div class="m2"><p>ابا گله هر سو شتابان شده</p></div></div>
<div class="b" id="bn250"><div class="m1"><p>نه کرده دبیری‌، نه خوانده کتاب</p></div>
<div class="m2"><p>نه آمخته راه خطا از صواب</p></div></div>
<div class="b" id="bn251"><div class="m1"><p>چنین خوی نیک ازکه آموخته</p></div>
<div class="m2"><p>کجا زین خردمندی اندوخته‌؟</p></div></div>
<div class="b" id="bn252"><div class="m1"><p>توگویی طبیعت بُدش اوستاد</p></div>
<div class="m2"><p>دهد این منش‌های نیکوش یاد</p></div></div>
<div class="b" id="bn253"><div class="m1"><p>ولی من بر آنم که استاد اوی</p></div>
<div class="m2"><p>بود دوری از مردم زشت‌خوی</p></div></div>
<div class="b" id="bn254"><div class="m1"><p>چو با مردمان کم‌نشسته‌است و خاست</p></div>
<div class="m2"><p>نیامخته خویی که مخلوق راست</p></div></div>
<div class="b" id="bn255"><div class="m1"><p>خیابان‌ندیده‌است‌و غوغای شهر</p></div>
<div class="m2"><p>ز سور و ز ماتم نبرده است بهر</p></div></div>
<div class="b" id="bn256"><div class="m1"><p>به‌ عقل غریزیش کم‌خورده دست</p></div>
<div class="m2"><p>نه کردست‌مستی‌،‌نه‌دیدست‌مست</p></div></div>
<div class="b" id="bn257"><div class="m1"><p>نخوردست جز شیر و کاک جوین</p></div>
<div class="m2"><p>نه سرکه مزیده نه سرکنگبین</p></div></div>
<div class="b" id="bn258"><div class="m1"><p>نه شب دیده نور فروزان چراغ</p></div>
<div class="m2"><p>نه روز از دلارام جسته سراغ</p></div></div>
<div class="b" id="bn259"><div class="m1"><p>چمیده به روز از بر مرغزار</p></div>
<div class="m2"><p>به‌شب‌خفته در دامن کوهسار</p></div></div>
<div class="b" id="bn260"><div class="m1"><p>از آزادی و سادگی بهره‌ور</p></div>
<div class="m2"><p>برومند وآزاده و نیک‌فر</p></div></div>
<div class="b" id="bn261"><div class="m1"><p>شبان گله را با سگ و زن سپرد</p></div>
<div class="m2"><p>سوی بوم و بر، پای رفتن فشرد</p></div></div>
<div class="b" id="bn262"><div class="m1"><p>در آن دِه درآمد که جوید نشان</p></div>
<div class="m2"><p>دهی دید چون مغز مردم کشان</p></div></div>
<div class="b" id="bn263"><div class="m1"><p>شبان هفته‌ای بود رفته ز ده</p></div>
<div class="m2"><p>بنشنیده آن کار کرد فره</p></div></div>
<div class="b" id="bn264"><div class="m1"><p>بگفتندش آن رفته کار شگرف</p></div>
<div class="m2"><p>فزودند بر آن بسی نیز حرف</p></div></div>
<div class="b" id="bn265"><div class="m1"><p>همه ناروا شهرت شهریان</p></div>
<div class="m2"><p>که دادند نسبت به مرد جوان</p></div></div>
<div class="b" id="bn266"><div class="m1"><p>ز شهر اندر آمد به کردار باد</p></div>
<div class="m2"><p>در آن ده پراکند و باور فتاد</p></div></div>
<div class="b" id="bn267"><div class="m1"><p>چنین است آیین خیل عوام</p></div>
<div class="m2"><p>پذیرای هر شهره گفتار خام</p></div></div>
<div class="b" id="bn268"><div class="m1"><p>به چشم ار ببینند چیزی درست</p></div>
<div class="m2"><p>نیارند دانستنش از نخست</p></div></div>
<div class="b" id="bn269"><div class="m1"><p>به دیده ز چیزی نگیرند بهر</p></div>
<div class="m2"><p>جزان را که گردد نیوشه به شهر</p></div></div>
<div class="b" id="bn270"><div class="m1"><p>نیوشه‌خو دار چه ‌محال ‌و خطاست‌</p></div>
<div class="m2"><p>پذیرند و دارند آن را به‌راست</p></div></div>
<div class="b" id="bn271"><div class="m1"><p>نیوشیده بر دیده و سر نهند</p></div>
<div class="m2"><p>ز دیده نیوشنده برتر نهند</p></div></div>
<div class="b" id="bn272"><div class="m1"><p>شبان‌سهم‌برداشت‌زان کار خفت</p></div>
<div class="m2"><p>بلرزند بر خود ز بیم گرفت</p></div></div>
<div class="b" id="bn273"><div class="m1"><p>به نزدیک زن رفت لرزنده تن</p></div>
<div class="m2"><p>ز لرزبدنش لرزه برداشت‌، زن</p></div></div>
<div class="b" id="bn274"><div class="m1"><p>بلرزند پستان مامک ز بیم</p></div>
<div class="m2"><p>در افغان شدند آن دو طفل یتیم</p></div></div>
<div class="b" id="bn275"><div class="m1"><p>خروشی‌درآن کلبه‌برخاست‌سخت</p></div>
<div class="m2"><p>که‌شد کوه ‌از اندوهشان لخت لخت</p></div></div>