---
title: >-
    شمارهٔ ۷۴ - رنج و گنج
---
# شمارهٔ ۷۴ - رنج و گنج

<div class="b" id="bn1"><div class="m1"><p>بروکار می کن مگو چیست کار</p></div>
<div class="m2"><p>که سرمایهٔ جاودانی است کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگر تاکه دهقان دانا چه گفت</p></div>
<div class="m2"><p>به ‌فرزندگان‌، چون‌ همی‌خواست خفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که میراث خود را بدارید دوست</p></div>
<div class="m2"><p>که گنجی ز پیشینیان‌ اندر اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من آن را ندانستم اندرکجاست</p></div>
<div class="m2"><p>پژوهیدن و یافتن با شماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چوشد مهرمه کشتگه‌برکنید</p></div>
<div class="m2"><p>همه جای آن زیر و بالا کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمانید ناکنده جایی ز باغ</p></div>
<div class="m2"><p>بگیرید از آن گنج هرجا سراغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پدر مرد و پوران به امید گنج</p></div>
<div class="m2"><p>به کاویدن دشت بردند رنج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گاوآهن و بیل کندند زود</p></div>
<div class="m2"><p>هم‌اینجا، هم‌آنجا و هرجا که بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قضا را در آن‌ سال‌ از آن خوب شخم</p></div>
<div class="m2"><p>ز هرتخم برخاست هفتاد تخم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشد گنج پیدا ولی رنجشان</p></div>
<div class="m2"><p>چنان چون پدرگفت شد گنجشان</p></div></div>