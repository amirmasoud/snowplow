---
title: >-
    شمارهٔ ۴۹ - انسان و جهان بزرگ
---
# شمارهٔ ۴۹ - انسان و جهان بزرگ

<div class="b" id="bn1"><div class="m1"><p>به نام برازندهٔ نام‌ها</p></div>
<div class="m2"><p>کز آغازها داند انجام‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خداوند عرش و خداوند فرش</p></div>
<div class="m2"><p>گرایندهٔ هر دو گیتی به عرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروزندهٔ عقل و جان و سخن</p></div>
<div class="m2"><p>برازندهٔ این جهان کهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دور اندربن پهنهٔ بیکران</p></div>
<div class="m2"><p>چو بینی بر این تافته اختران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو گویی که آنان به یکجا درند</p></div>
<div class="m2"><p>همه زآسمان بر زمین بنگرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی دان که هر اختری بی گمان</p></div>
<div class="m2"><p>زمین است و آن دیگران آسمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز هر اختری به آسمان بنگری</p></div>
<div class="m2"><p>همین پهنهٔ بیکران بنگری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین حقه هر اختری مهره‌ایست</p></div>
<div class="m2"><p>ز بازی به هر مهره‌ای بهره‌ایست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درون یکی حقهٔ لاجورد</p></div>
<div class="m2"><p>شتابان بسی مهرهٔ گِرد گرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز چاکی پنجهٔ مهره‌باز</p></div>
<div class="m2"><p>یکی در نشیب و یکی در فراز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در این ‌پهنه ‌آشوب ما و تو چیست</p></div>
<div class="m2"><p>که ما و تویی اندرین پهنه نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسیط زمین با همه آب و تاب</p></div>
<div class="m2"><p>بود جزئی از پیکر آفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همو هست از ذره‌ای پست‌تر</p></div>
<div class="m2"><p>بر پیکر آفتابی دگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همان آفتاب دگر بی‌گمان</p></div>
<div class="m2"><p>بود جزئی از پیکر آسمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود آسمان پرتوی بی‌قرار</p></div>
<div class="m2"><p>ز اندیشهٔ ذات پروردگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به گیتی درون ما و تو چیستیم</p></div>
<div class="m2"><p>اگر هستی اینست ما نیستیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زمانه کز اومان سراسر گله ‌است</p></div>
<div class="m2"><p>وز اخترش ‌در هر دلی ولوله است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فروزندهٔ مهر و ماه است و بس</p></div>
<div class="m2"><p>کمین بندهٔ پادشاهست و بس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من و تو چو کرمیم و همچون گیاه</p></div>
<div class="m2"><p>به بستانسرای یکی پادشاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر این گیا مرد و آن کرم زیست</p></div>
<div class="m2"><p>به بستانسرای ملک جرم نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بکوش ای گیا تا درختی شوی</p></div>
<div class="m2"><p>به باغ امل نیک‌بختی شوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که بر تو بسوزد دل باغبان</p></div>
<div class="m2"><p>به چشم اندر آییش روز و شبان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نگر تا چه گفته ‌است استاد طوس</p></div>
<div class="m2"><p>بدانجاکه از مرگش آید فسوس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>«‌یکی مرغ بر کوه بنشست و خاست‌»</p></div>
<div class="m2"><p>«‌نه‌افزود برکوه ‌و نز وی بکاست‌»</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>«‌من آن مرغم و این جهان کوه من‌»</p></div>
<div class="m2"><p>«‌چو مردم جهان را چه اندوه من‌»</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سخن کرده کوته وگرنه جهان</p></div>
<div class="m2"><p>نه کوهست و مردم نه ‌مرغی بر آن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به کوهی که‌خورشیدازآن‌د‌ره‌ایست‌</p></div>
<div class="m2"><p>بسیط زمین کمتر از ذره‌ایست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من و تو برین ذره باری که‌ایم</p></div>
<div class="m2"><p>درین کبریا و منی بر چه‌ایم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بزرگی چنانست و خردی چنین</p></div>
<div class="m2"><p>بزرگست ذات جهان‌آفرین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برو سعی کن تا چو گل در بهار</p></div>
<div class="m2"><p>بخندی به رخسارهٔ روزگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مشو بی‌بها ژاژ و بی‌برگ خس</p></div>
<div class="m2"><p>که در بوستان‌ها نیایی به کس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>میاموز آیین ناپاک خار</p></div>
<div class="m2"><p>که جز سوختن را نیایی به کار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>‌بدین‌خردی ای کودک پوی‌پوی </p></div>
<div class="m2"><p>چه خیزی که ناگه درافتی به‌روی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیندیش و آهنگ بیشی مکن</p></div>
<div class="m2"><p>جوانی بباید تو پیشی مکن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز بیشی و پیشی دلت خون شود</p></div>
<div class="m2"><p>دو چشمانت مانند جیحون شود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>طلایه کند پیشرو را سراغ</p></div>
<div class="m2"><p>خورد میوهٔ پیشرس را کلاغ</p></div></div>