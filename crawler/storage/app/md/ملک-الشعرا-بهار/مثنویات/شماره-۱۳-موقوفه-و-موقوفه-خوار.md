---
title: >-
    شمارهٔ ۱۳ - موقوفه و موقوفه‌خوار
---
# شمارهٔ ۱۳ - موقوفه و موقوفه‌خوار

<div class="b" id="bn1"><div class="m1"><p>گفتا موقوفه به موقوفه‌خوار</p></div>
<div class="m2"><p>کای تو سزای غضب کردگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل خودکامهٔ تو شیوه‌زن</p></div>
<div class="m2"><p>ای زتو خون در جگر بیوه‌زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دهنت باز به غیبت‌گری</p></div>
<div class="m2"><p>ای دلت انباز به حیلت‌وری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلقت تو شنعت بیچارگان</p></div>
<div class="m2"><p>صنعت تو صنعت بیکارگان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی تو رویی که ندیدنش به</p></div>
<div class="m2"><p>دست تو دستی که بریدنش به</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چیست گناهم که مرا می‌خوری</p></div>
<div class="m2"><p>چون سبعانم ز چه رو می ‌دری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلق مرا بهر تو ناکرده‌اند</p></div>
<div class="m2"><p>کز پی خیرات بنا کرده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ندهی گوش بر آوای من</p></div>
<div class="m2"><p>از چه نهی سلسله برپای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آه من اندر تو اثرها کند</p></div>
<div class="m2"><p>مشت تو را روز جزا وا کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت حریف دغل از روی کین</p></div>
<div class="m2"><p>کای بت پوشیدهٔ خلوت‌نشین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خامشی‌آموز و زبان‌بسته باش</p></div>
<div class="m2"><p>تند مرو اندکی آهسته باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جان منی گرچه کنیز منی</p></div>
<div class="m2"><p>همسر و ناموس عزیز منی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قامت رعنای تو نادیده به</p></div>
<div class="m2"><p>ماه رخ خوب تو پوشیده به</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روزی ‌من ‌بر تو حوالت ‌شد ست</p></div>
<div class="m2"><p>معده من از تو مرمت شدست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر نخورم من دگری می‌خورد</p></div>
<div class="m2"><p>ور نبرم من دگری می‌برد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست به جز مفت‌خوری کار من</p></div>
<div class="m2"><p>کارگری نیست سزاوار من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جز تو مرا نیست امیدی دگر</p></div>
<div class="m2"><p>بی‌هنرم بی‌هنرم بی‌هنر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جز تو ندارم خبر از نیک و بد</p></div>
<div class="m2"><p>بی‌خردم بی‌خردم بی‌خرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شغل من این بوده پدر بر پدر</p></div>
<div class="m2"><p>نیز چنین است پسر بر پسر</p></div></div>