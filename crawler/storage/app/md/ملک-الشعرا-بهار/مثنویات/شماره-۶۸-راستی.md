---
title: >-
    شمارهٔ ۶۸ - راستی
---
# شمارهٔ ۶۸ - راستی

<div class="b" id="bn1"><div class="m1"><p>شنیدم که شاهنشهی نقش بست</p></div>
<div class="m2"><p>ابر خاتم خویشتن‌: «‌راست رست‌»</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین باغ تا راستی‌، رسته‌ای</p></div>
<div class="m2"><p>وگر شاخ ناراستی خسته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگو راست‌، ور بیم جان داردت</p></div>
<div class="m2"><p>که خود راستی در امان داردت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی روز در مکه غوغا بخاست</p></div>
<div class="m2"><p>به کین محمد که می‌‎گفت راست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یاری رسیدش یکی رادمرد</p></div>
<div class="m2"><p>به‌ چیزبش پیچید و بر دوش کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ره در رسیدند غوغاییان</p></div>
<div class="m2"><p>گرفتند آن مرد را در میان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفنند کاین‌ چیست‌؟ گفت‌ این‌ نبی‌ است</p></div>
<div class="m2"><p>در این پشتواره جز او هیچ نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گزندآوران بی گزندان شدند</p></div>
<div class="m2"><p>به‌شوخی گرفتند و خندان شدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز راهش گذشتند و بگذشت پیر</p></div>
<div class="m2"><p>وز آن راستگویی برست آن امیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگر تا پیمبر چه گفت از خرد:</p></div>
<div class="m2"><p>«‌بگو راست هرچند مرگ آورد»</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شنو تا بدانی که این راز چیست</p></div>
<div class="m2"><p>که گر نشنوی بر تو باید گریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگوی‌ آنچه داری‌ به‌ دل راست‌ راست</p></div>
<div class="m2"><p>که هر راست را بازگفتن خطاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسا راست کآشوب‌ها راست کرد</p></div>
<div class="m2"><p>وزآن گفته‌ خصم‌ آنچه‌ می‌خواست کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه هر راست را بایدت گفت تیز</p></div>
<div class="m2"><p>نگر تا نگویی به جز راست چیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کجا فتنه خیزد زگفتار راست</p></div>
<div class="m2"><p>خموشی گزیدن‌ در آنجا رواست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گروهی دروغی روا داشتند</p></div>
<div class="m2"><p>به یک‌جای و آن خیر پنداشتند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دروغی کجا سود آید از آن</p></div>
<div class="m2"><p>به از راست کآشوب زاید از آن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>منت راست گویم که چونین دروغ</p></div>
<div class="m2"><p>وگر سود بخشد ندارد فروغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز خوبی زبان خاستن بودنی است</p></div>
<div class="m2"><p>ولی در بدی هیچگه سود نیست</p></div></div>