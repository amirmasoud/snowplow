---
title: >-
    شمارهٔ ۲۲ - بچهٔ ترس
---
# شمارهٔ ۲۲ - بچهٔ ترس

<div class="b" id="bn1"><div class="m1"><p>یکی زیبا خروسی بود جنگی</p></div>
<div class="m2"><p>به مانند عقاب از تیز چنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشاده سینه و گردن کشیده</p></div>
<div class="m2"><p>برای جنگ و پرخاش آفریده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهاده تاجی از یاقوت بر ترک</p></div>
<div class="m2"><p>فروهشته‌ دو غبغب چون دوگلبرگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو چشمانش‌ چو دو مشعل فروزان</p></div>
<div class="m2"><p>نگاهی خرمن بدخواه‌، سوزان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به مانند یکی میش ازکلانی</p></div>
<div class="m2"><p>شترمرغش نوشته‌: عبد فانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خروشش‌ چون‌ خروش پهلوانان</p></div>
<div class="m2"><p>به هنگام نوا، عُزال‌خوانان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز نوک ناخنش تا زیر منقار</p></div>
<div class="m2"><p>به یک گز می‌رسیدی گاه رفتار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان هر دو بالش نیم گز بود</p></div>
<div class="m2"><p>غریو غدغدش بانگ رجز بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو پایش چون دو ساق گاو، محکم</p></div>
<div class="m2"><p>دو خارش چون دورمح آهنین‌دم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زپهنای بر و قد رسایش</p></div>
<div class="m2"><p>گذشتی مرغی از بین دو پایش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میان رانی فراخ و سفته‌ای تنگ</p></div>
<div class="m2"><p>بر آن سفته شلالی زعفران‌رنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گه رفتن منظم پا نهاده</p></div>
<div class="m2"><p>چو وقت مشق‌، سرهنگ پیاده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز منقارش نمایم راستی یاد</p></div>
<div class="m2"><p>چو دوپیکان خمّیده ز پولاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به عزم رزم چون افراختی یال</p></div>
<div class="m2"><p>ز بیم جان فکندی باز پیخال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز میدانش اگر سیمرغ بودی</p></div>
<div class="m2"><p>به‌ضرب یک لگد بیرون نمودی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خروسان محل از هیبتش باز</p></div>
<div class="m2"><p>کشیدندی سحر آهسته آواز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی روز از قضا در طرف باغی</p></div>
<div class="m2"><p>پرید از نزد او لاغرکلاغی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خروس ازبیم کرد آن گونه فریاد</p></div>
<div class="m2"><p>که اندر خیل مرغان شورش افتاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز نزدیک کلاغ آنسان بدر رفت</p></div>
<div class="m2"><p>که گفتی نوک تیرش در جگر رفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برفت ازکف وقار و طمطراقش</p></div>
<div class="m2"><p>پر و بالش بهم پیچید و ساقش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>طپان شد قلبش از تشویش در بر</p></div>
<div class="m2"><p>دهانش بازماند و چشم‌، اعور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس از لختی که فارغ شد خیالش</p></div>
<div class="m2"><p>یکی از محرمان پرسید حالش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که ای گردن‌فراز آهنین‌پی</p></div>
<div class="m2"><p>که‌بود اوکاین‌چنین‌ترسیدی‌از وی‌؟‌!!</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به پاسخ گفت کای فرزانه دلبر</p></div>
<div class="m2"><p>نبود او جزکلاغی زشت و لاغر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جوابش گفت‌: باشد صعب حالی</p></div>
<div class="m2"><p>که ترسد شرزه شیری از شغالی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خروس پهلوان با ماکیان گفت‌:</p></div>
<div class="m2"><p>کس از یار موافق راز ننهفت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من‌آن‌روزی که بودم جوجه‌ای خرد</p></div>
<div class="m2"><p>کلاغ از پیش رویم جوجه‌ای برد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بجست وکرد مسکن برسرشاخ</p></div>
<div class="m2"><p>بخورد آن جوجه را گستاخ گستاخ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنانم وحشتش بنشست در دل</p></div>
<div class="m2"><p>که آن وحشت هنوز هست در دل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز عهدکودکی تا این زمانه</p></div>
<div class="m2"><p>اگر پرد کلاغی زآشیانه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همان وحشت شود نو در دل من</p></div>
<div class="m2"><p>که آکنده است در آب و گل من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فراوان در شجاعت خوانده درسم</p></div>
<div class="m2"><p>ولی از این کلاغان بچه ترسم</p></div></div>