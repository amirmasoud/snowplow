---
title: >-
    شمارهٔ ۸۰ - از بدی بپرهیز
---
# شمارهٔ ۸۰ - از بدی بپرهیز

<div class="b" id="bn1"><div class="m1"><p>گذشته گذشته است و آینده نیست</p></div>
<div class="m2"><p>میان دو نابود، پاینده چیست‌؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشته اگر خوب اگر بد، گذشت</p></div>
<div class="m2"><p>وز آینده کس نیز واقف نگشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذشته به چنگ تو ناید دگر</p></div>
<div class="m2"><p>وز آینده‌ات نیز نبود خبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمی کاندر آن دعوی هست تست</p></div>
<div class="m2"><p>همانست کاین‌ لحظه‌ در دست تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو در دست تست ای برادر زمان</p></div>
<div class="m2"><p>زمان را به اندوه و غفلت ممان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین یکدم ار بد کنی یا که زشت</p></div>
<div class="m2"><p>زمانه به‌نام تو خواهد نوشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مبادا در این یک‌زمان بد کنی</p></div>
<div class="m2"><p>که گر بد کنی در حق خود کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مرد خدا نیست زشتی سزای</p></div>
<div class="m2"><p>که مرد ار ببخشد نبخشد خدای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بپرهیز از آزردن نیکمرد</p></div>
<div class="m2"><p>که با نیکمردان کسی بد نکرد</p></div></div>