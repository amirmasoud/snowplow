---
title: >-
    شمارهٔ ۴۵ - انسان و جنگ
---
# شمارهٔ ۴۵ - انسان و جنگ

<div class="b" id="bn1"><div class="m1"><p>شبی لب فروبسته بودم ز حرف</p></div>
<div class="m2"><p>خرد غرق اندیشه‌های شگرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درآمد بت مهربانم به بر</p></div>
<div class="m2"><p>خرامنده بر سان طاوس نر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه مهر و خوش‌خویی و نیکویی</p></div>
<div class="m2"><p>بدیع‌ است‌ با نیکویی خوش‌خویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دست اندرش نامه‌ای از فرنگ</p></div>
<div class="m2"><p>سخن‌ها درو بر ز پیکار و جنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که قیصر به دریا سپه رانده است</p></div>
<div class="m2"><p>به‌آب‌اندرون آتش افشانده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوین مرزیان زین برآشفته‌اند</p></div>
<div class="m2"><p>به بیغاره بر چیزهاگفته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازین پس به دریاست جنگی بزرگ</p></div>
<div class="m2"><p>میان عقاب و نهنگ سترگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببینیم تا بال و پر عقاب</p></div>
<div class="m2"><p>بریزد درین پهن دربای آب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و یا گرده‌گاه دلاور نهنگ</p></div>
<div class="m2"><p>زمانه بدرد به روئینه چنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برآشفت و گفت این چه دیوانگیست</p></div>
<div class="m2"><p>نه‌خون ریختن رسم فرزانگیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گروهی که در کینه پیچیده‌اند</p></div>
<div class="m2"><p>چه از مهربانی زیان دیده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی بنگر از دیده دوربین</p></div>
<div class="m2"><p>به‌پایان این رزم و پرخاش وکین‌!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدوگفتم ای ازدر آشتی</p></div>
<div class="m2"><p>تو ز اندیشه‌ام بند برداشتی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کس این جنگ را دیر برنشمرد</p></div>
<div class="m2"><p>ز خرداد و از تیر برنگذرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگر بگذرد، نیز پایانش هست</p></div>
<div class="m2"><p>جهان شست‌ خواهد ز خونابه‌ دست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بشوید جهان دست‌، لیک آدمی</p></div>
<div class="m2"><p>همی‌ تا بود جنگ جوید همی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که مردم به جنگ اندر آماده‌اند</p></div>
<div class="m2"><p>ز مادر همه جنگ را زاده‌اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رود جنگ آنگه زگیتی به در</p></div>
<div class="m2"><p>که نه ماده برجای ماند، نه نر</p></div></div>