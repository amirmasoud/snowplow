---
title: >-
    شمارهٔ ۸۱ - تود و بید
---
# شمارهٔ ۸۱ - تود و بید

<div class="b" id="bn1"><div class="m1"><p>جهانست چون جنگلی بیکران</p></div>
<div class="m2"><p>فراوان درخت و گیاه اندر آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی از در میوه اندوختن</p></div>
<div class="m2"><p>درختی دگر از در سوختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو تابید از برج خرچنگ شید</p></div>
<div class="m2"><p>بخندید بر بارور تود، بید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که این کوشش بی کران تا به کی‌؟</p></div>
<div class="m2"><p>خمیده ز بار گران تا به کی‌؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فروهشته برگردنت پالهنگ</p></div>
<div class="m2"><p>شکسته سر و دستت از چوب و سنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرو ریخته برگ و بارت بهم</p></div>
<div class="m2"><p>به زیر لگد پشت کرده بخم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یاد که در این سرای سپنج</p></div>
<div class="m2"><p>کشی بار این‌ درد و اندوه‌ و رنج‌‌؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوری غم به یاد دل شاد که‌؟</p></div>
<div class="m2"><p>به عشق که‌؟ بهر که‌؟ بر یاد که‌؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی کز برای تو تب کرد راست</p></div>
<div class="m2"><p>اگر از برایش بمیری رواست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی کز فراق تو لب می گزد</p></div>
<div class="m2"><p>گر افغان کنی در غمش می‌سزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و دیگرکه دنیا دمی بیش نیست</p></div>
<div class="m2"><p>در آن‌دم کس‌ ار غم‌ خورد ز ابلهیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو ای بارور تود فرخ سرشت</p></div>
<div class="m2"><p>چه‌ خوش کرده‌ای اندرین کار زشت‌؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نگه کن به من کاندرین جای خوب</p></div>
<div class="m2"><p>نه‌ رنجست‌ و انده‌ نه‌ سنگست‌ و چوب</p></div></div>