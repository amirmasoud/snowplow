---
title: >-
    شمارهٔ ۵۶ - اتق من شر من احسنت الیه
---
# شمارهٔ ۵۶ - اتق من شر من احسنت الیه

<div class="b" id="bn1"><div class="m1"><p>یکی مرد خودخواه مغرور دون</p></div>
<div class="m2"><p>درافتاد روزی به تنگی درون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن تنگی و بستی آه کرد</p></div>
<div class="m2"><p>رفیقی بر او رنج کوتاه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهاندش ز بیکاری و کار داد</p></div>
<div class="m2"><p>فراوان درم داد و دینار داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همش‌ نیکویی کرد و احسان نمود</p></div>
<div class="m2"><p>به نامرد نیکی و احسان چه سود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان کار آن سفله بالا گرفت</p></div>
<div class="m2"><p>که بر جایگاه گزین جاگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خودخواه‌ از آن‌ حالت‌ زار رست</p></div>
<div class="m2"><p>میان را به کین نکوکار بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمانه یکی بازی آورد راست</p></div>
<div class="m2"><p>که مرد نکوکار ازو کار خواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آغاز بیگانگی‌ها نمود</p></div>
<div class="m2"><p>چو داد آشناییش رخ وانمود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخندید چون زاری مرد دید</p></div>
<div class="m2"><p>رخش سرخ شد چون رخش زرد دید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان لعب‌ها با جوانمرد باخت</p></div>
<div class="m2"><p>که ‌سوز درون ‌استخوانش گداخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان خوار کردش بر انجمن</p></div>
<div class="m2"><p>کزان کار بگذشت و از خویشتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بداندیش از آن شیوه سرمست شد</p></div>
<div class="m2"><p>که پیشش ولی نعم پست شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی گفتش از آشنایان پار</p></div>
<div class="m2"><p>که این شوخ‌چشمی چه بود ای نگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدو گفت یک‌ روز من پیش اوی</p></div>
<div class="m2"><p>بدان حال رفتم که زن پیش شوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مراگرچه از مهربانی نواخت</p></div>
<div class="m2"><p>ولی مهر او استخوانم گداخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا خندهٔ گرم او سرد کرد</p></div>
<div class="m2"><p>دوایش دلم را پر از درد کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خودخواهی‌ام ضربتی خورد سخت</p></div>
<div class="m2"><p>بنالیدم از نابکاری بخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که چون من کسی نزد چون او کسی</p></div>
<div class="m2"><p>به حاجت رود، ننگ باشد بسی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا گر همی راند با ضجرتی</p></div>
<div class="m2"><p>از آن به که بنواخت بی‌منتی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرم دور می کرد بودم به‌آن</p></div>
<div class="m2"><p>که کامم روا کرد و منت نهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیارستم این غم ز دل بردنا</p></div>
<div class="m2"><p>چنان ناخوشی را فرو خوردنا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شد احسان او لجهٔ بی‌کران</p></div>
<div class="m2"><p>که‌ خودخواهیم غرق گشت اندر آن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پی رستن از آن غریونده زو</p></div>
<div class="m2"><p>زدم پنجه بر آن که بد پیشرو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فرو کردم او را و خود برشدم</p></div>
<div class="m2"><p>وز آن لجهٔ ژرف برتر شدم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نکوکاری او مرا خوار کرد</p></div>
<div class="m2"><p>نکو کرد و در معنی آزار کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز خواهشگری تلخ شد کام من</p></div>
<div class="m2"><p>وز احسان او تیره فرجام من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو آمد مرا نوبت چیرگی</p></div>
<div class="m2"><p>برستم از آن تلخی و تیرگی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنان چاره کردم که دیدی تو نیز</p></div>
<div class="m2"><p>پی پاس ناموس نفس عزیز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بزرگان که نام نکو برده‌اند</p></div>
<div class="m2"><p>بجای بدی نیکوبی کرده‌اند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بزرگان ما! بخردی می‌کنند</p></div>
<div class="m2"><p>بجای نکویی بدی می کنند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کسی کش بدی کرده‌ای‌، زینهار!</p></div>
<div class="m2"><p>از او هیچ گه چشم نیکی مدار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مشو ایمن ازکین و پاداشنش</p></div>
<div class="m2"><p>فزون زان بدی نیکویی‌ها کنش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نکویی کن و مهربانی و داد</p></div>
<div class="m2"><p>بود کان بدی‌ها نیارد بهٔاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو تخم بدی درنشیند به دل</p></div>
<div class="m2"><p>بروید ز دل همچو گندم ز گل</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز هر دانه‌ای هفت خوشه جهد</p></div>
<div class="m2"><p>ز هر خوشه صد تخم بیرون دهد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>توگر با شریفی بدی کرده‌ای</p></div>
<div class="m2"><p>چنان دان که نابخردی کرده‌ای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شریف از شرافت ببخشایدت</p></div>
<div class="m2"><p>ولی آن بدی خوی به‌جنگ آیدت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسی با توپنجه به پنجه شود</p></div>
<div class="m2"><p>به صدگونه زو دلت رنجه شود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مگیر از فرومایگان دوستان</p></div>
<div class="m2"><p>که حنظل نکارند در بوستان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فرومایه بیگانه بهترکه دوست</p></div>
<div class="m2"><p>که دوری ز زنبور و کژدم نکوست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بهارا بترس از فرومایه مرد</p></div>
<div class="m2"><p>تو خود گر کسی گرد ناکس مگرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که مهر فرومایگان دشمنی است</p></div>
<div class="m2"><p>نگر تا که خشم فرومایه چیست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فرومایگان بی‌هنر مردمند</p></div>
<div class="m2"><p>که بی‌دانشند و به غفلت گمند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پدر بی‌هنر، مادر از وی بَتَر</p></div>
<div class="m2"><p>نه برخی زمادر، نه بهر از پدر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نه از درس و صحبت هنر یافته</p></div>
<div class="m2"><p>نه بهری زمام و پدر یافته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وگر خوانده درسی به‌ صورت درست</p></div>
<div class="m2"><p>بدان دانش او دشمن جان تست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که اخلاق خوب آید از خانمان</p></div>
<div class="m2"><p>چنان کآب‌، پاک آید از آسمان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>طبیعت بباید که زیبا شود</p></div>
<div class="m2"><p>که ابریشم است آن که دیبا شود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کسان آب دریا مقطر کنند</p></div>
<div class="m2"><p>مزه دیگر و لون دیگر کنند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همان آب را ابر بالا برد</p></div>
<div class="m2"><p>ز دریا کناران به صحرا برد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بک آب است جسته ز دو هوش‌، فر</p></div>
<div class="m2"><p>یکی از طبیعت یکی از بشر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یک آب مقطر به دریاکنار</p></div>
<div class="m2"><p>یکی آب باران نوشین گوار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>یکی آبی فرومایه و روده‌بند</p></div>
<div class="m2"><p>یکی نوشداروی هر مستمند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یک قطره کش ناخدا ساخته</p></div>
<div class="m2"><p>دگر قطره کآن را خدا ساخته</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ازین قطره تا قطرهٔ ناخدای</p></div>
<div class="m2"><p>بود دوری از ناخدا تا خدای</p></div></div>