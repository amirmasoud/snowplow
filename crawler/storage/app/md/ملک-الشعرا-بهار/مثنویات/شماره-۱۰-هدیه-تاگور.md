---
title: >-
    شمارهٔ ۱۰ - هدیهٔ تاگور
---
# شمارهٔ ۱۰ - هدیهٔ تاگور

<div class="b" id="bn1"><div class="m1"><p>دست خدای احد لم‌یزل</p></div>
<div class="m2"><p>ساخت یکی چنگ به روز ازل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بافته ابریشمش از زلف حور</p></div>
<div class="m2"><p>بسته بر او پردهٔ موزون ز نور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نغمه او رهبر آوارگان</p></div>
<div class="m2"><p>مویهٔ او چارهٔ بیچارگان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت گر این چنگ نوازند راست</p></div>
<div class="m2"><p>مهر فزونی کند و ظلم کاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نغمهٔ این جنگ نوای خداست</p></div>
<div class="m2"><p>هرکه دهد گوش برای خداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بنوازد کسی این چنگ را</p></div>
<div class="m2"><p>گم نکند پرده وآهنگ را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که دهد گوش و مهیا شود</p></div>
<div class="m2"><p>بند غرور از دل او وا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه ‌بود جنگ بر آهنگ چنگ</p></div>
<div class="m2"><p>چنگ خدا محو کند نام جنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون که ‌خدا چنگ ‌چنین ساز کرد</p></div>
<div class="m2"><p>چنگ‌زنی بهر وی آواز کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت که ما صنعت خود ساختیم</p></div>
<div class="m2"><p>سوی گروه بشر انداختیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راه نمودیم به پیغمبران</p></div>
<div class="m2"><p>تا بنمایند ره دیگران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کیست که این ساز بسازد کنون</p></div>
<div class="m2"><p>بهر بشر چنگ نوازد کنون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنگ زمن‌ ، پرده زمن‌ ، ره زمن</p></div>
<div class="m2"><p>کیست نوازنده درین انجمن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر که نوازد بنوازم ورا</p></div>
<div class="m2"><p>در دو جهان سر بفرازم ورا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنگ محبت چه بود ، جود من</p></div>
<div class="m2"><p>نیست جز این مسئله مقصود من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گوش بر الهام خدایی کنید</p></div>
<div class="m2"><p>وز ره ابلیس جدایی کنید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رشته الهام نخواهد گسست</p></div>
<div class="m2"><p>تا به ابد متصل است از الست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکه روانش ز جهالت بریست</p></div>
<div class="m2"><p>نغمهٔ او نغمهٔ پیغمبریست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>راه ‌نمایان فروزان ضمیر</p></div>
<div class="m2"><p>راه نمودند به برنا و پیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رنجه شد از چنگ زدن چنگشان</p></div>
<div class="m2"><p>کس نشد از مهر هم ‌آهنگشان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زمزم پاک ازلی شد ز یاد</p></div>
<div class="m2"><p>نغمهٔ ابلیس به کار اوفتاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنگ خدا گشت میان جهان</p></div>
<div class="m2"><p>ملعبه و دستخوش گمرهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر کسی از روی هوی چنگ زد</p></div>
<div class="m2"><p>هر چه ‌دلش‌ خواست بر آهنگ زد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرغ حقیقت ز تغنی فتاد</p></div>
<div class="m2"><p>روح به گرداب تدنی فتاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عقل گران‌، جان پی برهان گرفت</p></div>
<div class="m2"><p>رهزن ‌حس ره به‌ دل و جان گرفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لنگر هفت اختر و چار آخشیج</p></div>
<div class="m2"><p>تافت ره کشتی جان از بسیج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در ره ‌دین ‌سخت‌ترین ‌زخمه‌ خاست</p></div>
<div class="m2"><p>لیک ‌از این ‌زخمه ‌نه ‌آن ‌نغمه ‌خاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نغمهٔ یزدان دگر و دین دگر</p></div>
<div class="m2"><p>زخمه دگر، آن دگر و این دگر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دین همه سرمایهٔ کشتار گشت</p></div>
<div class="m2"><p>یکسره بر دوش بشر بارگشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر که ‌بدان چنگ روان چنگ ‌داشت</p></div>
<div class="m2"><p>زیر لبی زمزمهٔ جنگ داشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کینه برون از دل مردم نشد</p></div>
<div class="m2"><p>کبر و تفرعن ز جهان گم نشد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اشگ فرو ریخت به جای سرور</p></div>
<div class="m2"><p>سوگ بپا گشت به هنگام سور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مهرپرستی ز جهان رخت بست</p></div>
<div class="m2"><p>سم خر و گاو به جایش نشست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گشت ازبن زمزمه‌های دروغ</p></div>
<div class="m2"><p>مهر فلک بی‌اثر و بی‌فروغ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زن که به چنگ ازلیت به فن</p></div>
<div class="m2"><p>راه خطا زد سر هر انجمن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنگ نکو بود ولی بد زدند</p></div>
<div class="m2"><p>چنگ خدا بهر دل خود زدند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنگ نزد بر دل کس چنگشان</p></div>
<div class="m2"><p>روح نجنبید بر آهنگشان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تاکه درین عصر نوین بیدرنگ</p></div>
<div class="m2"><p>در بر «‌تاگور» نهادند چنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ذات قدیمی پی بست و گشاد</p></div>
<div class="m2"><p>قوس هنر در کف تاگور نهاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون که ‌بزد چنگ ‌بر آهنگ راست</p></div>
<div class="m2"><p>نغمهٔ اصلی ز دل چنگ خاست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نالهٔ عشاق برآمد ز چنگ</p></div>
<div class="m2"><p>پر شد ازو هند و عراق و فرنگ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جمله نواها ز جهان رخت بست</p></div>
<div class="m2"><p>نغمهٔ «‌عشاق‌» به جایش نشست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تاگور! این ‌چنگ که ‌در دست تست</p></div>
<div class="m2"><p>بوده به چنگ دگران از نخست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چنگ زراتشت و برهماست این</p></div>
<div class="m2"><p>مانده به تاگور ز بوداست این</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>صفحهٔ ‌درس «‌هومروس‌» است ‌این</p></div>
<div class="m2"><p>زخمهٔ خنیاگر طوس است این</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ساز «‌جنید» و «‌خرقانی‌» است این</p></div>
<div class="m2"><p>خامه ی عطار معانی است این</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>این ز «‌مناکی‌» است تو را یادگار</p></div>
<div class="m2"><p>اینت نی بلخی رومی شعار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گفته بدو سعدی شیراز، راز</p></div>
<div class="m2"><p>برده بدو ناخن حافظ نماز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جامی و عرفیش چو ناخن زدند</p></div>
<div class="m2"><p>صائب و بیدل به خروش آمدند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دیرگهی شد که ز کار اوفتاد</p></div>
<div class="m2"><p>اختر سعدش ز مدار اوفتاد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>عصر جدید ار چه ‌ملک ‌چهره ‌است</p></div>
<div class="m2"><p>زین ملکی زمزمه بی‌بهره است</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بند عناصر همه را دست بست</p></div>
<div class="m2"><p>سنگ بلا شهپر جانشان شکست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هیچ کس آن چنگ نزد بر طریق</p></div>
<div class="m2"><p>هرکسی آن زد که پسندد فریق</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>لیک ‌تو خوش ساختی ‌این ‌چنگ را</p></div>
<div class="m2"><p>یافتی آن ایزدی آهنگ را</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هرچه زنی در ره او می‌زنی</p></div>
<div class="m2"><p>خوش بزن این ره که نکو می‌زنی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>طبع‌ تو چنگست ‌و خرد زخمه‌اش</p></div>
<div class="m2"><p>شعر بلندت ازلی نغمه‌اش</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سال تو هفتاد و خیال نوست</p></div>
<div class="m2"><p>زان که ز یزدان به دلت پرتو است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هرکه ز یزدان به دلش نور تافت</p></div>
<div class="m2"><p>در دو جهان دولت جاوید یافت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سیصد و ده ‌چون بگذشت از هزار</p></div>
<div class="m2"><p>گفته شد این شعر خوش آبدار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>جانب بنگاله فرستادمش</p></div>
<div class="m2"><p>«‌هدیهٔ تاگور» لقب دادمش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سال چو نو گشت درآمد برید</p></div>
<div class="m2"><p>گفت که هان مژده به من آورید</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از وطن حافظ شیرین‌سخن</p></div>
<div class="m2"><p>بگذرد آن طوطی شکرشکن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>طوطی بنگاله برآید ز هند</p></div>
<div class="m2"><p>جانب ایران بگراید ز هند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چون من از این مژده خبر یافتم</p></div>
<div class="m2"><p>پای ز سر کرده و بشتافتم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دیدمش آنسان که نمودم خیال</p></div>
<div class="m2"><p>بلکه فزون‌تر به جمال و کمال</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>قد برازنده و چشم سیاه</p></div>
<div class="m2"><p>رخ‌، چو بابر تنکی چهر ماه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>زلف چو کافور فشانده به دوش</p></div>
<div class="m2"><p>نوش‌ لبش بسد کافور پوش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>برده ز بس پیش حقیقت نماز</p></div>
<div class="m2"><p>پشت خمیده چو کمان طراز</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گوشت نه بسیار و نه کم بر تنش</p></div>
<div class="m2"><p>تافته از سینه دل روشنش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هشته ز مخمل کله ساده‌ای</p></div>
<div class="m2"><p>بر تن او جامه و لباده‌ای</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گر چه ‌ز حشمت‌ به حوالیش جیش</p></div>
<div class="m2"><p>ساده ‌چو سقراط ‌و فلاطون ‌به ‌عیش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خضر مثالی و سلیمان فری</p></div>
<div class="m2"><p>گرد وی از فضل و ادب لشکری</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>آمد و چشم من از او نور دید</p></div>
<div class="m2"><p>راضیم از دیده که «‌تاگور» دید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>زان جهانست‌، نه مخصوص هند</p></div>
<div class="m2"><p>چون ‌شکر مصری و هندی فرند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ملت بودا اگر این پرورد</p></div>
<div class="m2"><p>عقل به بتخانه نماز آورد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>او است نمودار بت بامیان</p></div>
<div class="m2"><p>زانش گرفتیم چو جان در میان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>جان به گل و لاله درآمیختیم</p></div>
<div class="m2"><p>لاله و گل در قدمش ریختیم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بلبل ماگشت غزلخوان او</p></div>
<div class="m2"><p>شاخ گل آویخت به دامان او</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>باد صبا گرد رهش برفشاند</p></div>
<div class="m2"><p>ابر بهاری گهر تر فشاند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>کوه به‌سر، بهر نثارش کشید</p></div>
<div class="m2"><p>یک‌ طبق از گوهر و سیم سپید</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بهر دعایش به برکردگار</p></div>
<div class="m2"><p>دست برآورد درخت چنار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>قلب صنوبر ز فراقش کفید</p></div>
<div class="m2"><p>تا قد آن سرو دلارام دید</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>آب روان مویه کنان بر زمین</p></div>
<div class="m2"><p>سود به آثار قدومش جبین</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>صف‌زده گل‌ها به رهش از دو سو</p></div>
<div class="m2"><p>بهر تماشای گل روی او</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>آمد و آورد بسی ارمغان</p></div>
<div class="m2"><p>از گهر حکمت هندوستان</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>آمده از بحرگهر زای هند</p></div>
<div class="m2"><p>دامن دل پر زگهرهای هند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گوهر حکمت ‌همه ‌یک گوهر است</p></div>
<div class="m2"><p>آمدهٔ هند ولی بهتر است</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>قطره‌ای از عالم بالا چکید</p></div>
<div class="m2"><p>درگهرش جوهر عرفان پدید</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>هند، صدف‌وار دهان برد پیش</p></div>
<div class="m2"><p>قطره فروبرد و فروشُد به خویش</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>قرن پس از قرن بر او برگذشت</p></div>
<div class="m2"><p>دهر پس از دهر مکرر گذشت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>تا صدف هند گهربار شد</p></div>
<div class="m2"><p>مهد یکی گوهر شهوار شد</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>از نظر اجنبیش دور ساخت</p></div>
<div class="m2"><p>درج گهر سینهٔ «‌تاگور» ساخت</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ای قلمت هدیهٔ پروردگار</p></div>
<div class="m2"><p>هدیهٔ ایران بپذیر از بهار</p></div></div>