---
title: >-
    شمارهٔ ۶۳ - ترجمه یک قطعهٔ فرانسه
---
# شمارهٔ ۶۳ - ترجمه یک قطعهٔ فرانسه

<div class="b" id="bn1"><div class="m1"><p>یکی کودک از لانه جغدی کشید</p></div>
<div class="m2"><p>به صحن دبستانش می‌پرورید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم او را یکی بچهٔ غاز بود</p></div>
<div class="m2"><p>که باگربهٔ پیر همراز بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به‌ مدرس درون هر سه ره داشتند</p></div>
<div class="m2"><p>بر کودکان دستگه داشتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بس کاندر آنجای بشتافتند</p></div>
<div class="m2"><p>ز علم و خرد بهره‌ها یافتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز «‌هرودت‌» سخن کرده‌ از بر بسی</p></div>
<div class="m2"><p>ز «‌تیتلیو» هم خوانده دفتر بسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی را بهنجار اهل خبر</p></div>
<div class="m2"><p>جدل سر نمودند با یکدگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کز اقوام و از شهریاران ییش</p></div>
<div class="m2"><p>کدامند اندک‌، کدامند بیش‌؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آغازگفتار، شد گر به راست</p></div>
<div class="m2"><p>که از مردم مصر بهتر کجا است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه عالم و عاقل و دین‌پرست</p></div>
<div class="m2"><p>همه بردباران آیین‌پرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز جانند نزد خدایان رهی</p></div>
<div class="m2"><p>همین یک‌صفتشان بس اندر بهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برآورد جغد از دگر سو نوا</p></div>
<div class="m2"><p>که چون قوم آتن کنون کو، کجا؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من آن قوم را دوست دارم بسی</p></div>
<div class="m2"><p>وزان قوم برتر ندانم کسی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرا باشد آن لطف وآن دلبری</p></div>
<div class="m2"><p>هم آن زور بازوی و نام‌آوری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بخندید از این ماجرای دراز</p></div>
<div class="m2"><p>به غوغا سخن کرد آغاز، غاز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که‌هیهات‌،‌هیهات‌ازین‌فکرو رای</p></div>
<div class="m2"><p>وز این ژاژگفتار شوخی‌نمای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر اینست پس رومیان کیستند</p></div>
<div class="m2"><p>بر رومیان دیگران چیستند؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به یک‌ جای شد گرد با مهتری</p></div>
<div class="m2"><p>بزرگی و مردی و گندآوری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فراوان هنرها به یک مرز و بوم</p></div>
<div class="m2"><p>نهادند و بر آن نوشتند روم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا دل کشد سوی این ‌قوم باز</p></div>
<div class="m2"><p>جهانجوی را برد باید نماز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فضیلت‌ فروشان جدل ساختند</p></div>
<div class="m2"><p>ز صحبت به بیغاره پرداختند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خردمند موشی در آن پرده بود</p></div>
<div class="m2"><p>که اوراق علمی بسی خورده بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به گفتار آنان همی داشت گوش</p></div>
<div class="m2"><p>نگرتا چه گفت آن خردمند موش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که ای چیره‌دستان نغز هجیر</p></div>
<div class="m2"><p>غرض را اجیرید برخیرخیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر مصریان گربه مسجود بود</p></div>
<div class="m2"><p>همان جغد را قوم آتن ستود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هم اندر « کپی‌تول‌» ز دربار روم </p></div>
<div class="m2"><p>به‌ غازان‌ خورش بود و نذر و رسوم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز هریک به هریک نوایی رسید</p></div>
<div class="m2"><p>که‌تان هریکی دل به جایی کشید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عقیدت‌چو کاهی‌ است هرجا گرای</p></div>
<div class="m2"><p>برو بر غرض چیره چون کهربای</p></div></div>