---
title: >-
    شمارهٔ ۳۰ - سی لحن موسیقی
---
# شمارهٔ ۳۰ - سی لحن موسیقی

<div class="b" id="bn1"><div class="m1"><p>شنیدم باربد در بزم خسرو</p></div>
<div class="m2"><p>به هر نوبت سرودی نغمه‌ای نو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرودی نغمه با چنگ دلاوبز</p></div>
<div class="m2"><p>وزان خوش داشتی اوقات پرویز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمار جمله الحانی که پیوست</p></div>
<div class="m2"><p>بُدی‌درسال‌شمسی‌سیصدوشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فزون زبن، پنجگه بودی ز دنبال</p></div>
<div class="m2"><p>که خواندندی به جشن آخر سال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن الحان خوش‌، سی لحن نامی</p></div>
<div class="m2"><p>به شعر خویش آورده نظامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اندک اختلاف آن لحن‌ها را</p></div>
<div class="m2"><p>به‌هر فرهنگ خواهی جست‌، یارا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخست «‌آرایش خورشید» بوده</p></div>
<div class="m2"><p>دوم «‌آیین جمشید» ستوده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوم «‌اورنگی‌» است ای یار دیرین</p></div>
<div class="m2"><p>چهارم است نامش «‌باغ شیرین‌»</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پنجم هست «‌تخت طاقدیسی‌»</p></div>
<div class="m2"><p>توانی نیزبی نسبت نویسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ششم را «‌حقه کاوس‌» شد نام</p></div>
<div class="m2"><p>به‌هفتم «‌راح روح‌» است ای دلارام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگرگوبد که‌آن‌خود«‌راه‌روح‌» است</p></div>
<div class="m2"><p>کزان ره روح رامش را فتوح است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گمانم کاین دو تازی لحن از الحان</p></div>
<div class="m2"><p>بود تفسیر لفظ «‌رامش جان‌»</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ور از سی لحن‌، لحنی کمتر آید</p></div>
<div class="m2"><p>به جایش لحن «‌فرخ روز» شاید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نظامی هم بر این آهنگ رفته است</p></div>
<div class="m2"><p>که فرخ روزرا لحنی گرفته است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود هشتم همانا «‌رامش جان‌»</p></div>
<div class="m2"><p>به‌جای جان‌،‌جهان هم‌خواند بتوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نهم را «‌سبزه در سبزه‌» ستودند</p></div>
<div class="m2"><p>دهم را نام «‌سروستان‌» فزودند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نوای یازده «‌سرو سهی‌» دان</p></div>
<div class="m2"><p>فرامش کردنش ازکو تهی دان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سرود هشت و چارم را خردمند</p></div>
<div class="m2"><p>به «‌شادروان مرواربد» افکند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شمار سیزده «‌شبدیز» نامست</p></div>
<div class="m2"><p>«‌شب فرخ‌» شب ماه تمام است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سرود «‌قفل رومی‌» پانزده دان</p></div>
<div class="m2"><p>ده‌و شش « کنج بادآور» همی خوان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو « گنج ساخته‌» باشد ده و هفت</p></div>
<div class="m2"><p>که گنج سوخته هم درقلم رفت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به‌هجده « کین ایرج‌» می‌زند جوش</p></div>
<div class="m2"><p>وزان پس نوزده « کین سیاووش»</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو ده را «‌ماه برکوهان‌» نشانه</p></div>
<div class="m2"><p>بود یک بیست نامش «‌مشکدانه‌»</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بود «‌مروای نیک‌» اندر دو و بیست</p></div>
<div class="m2"><p>همان‌سه‌بیست‌نامش‌«‌مشکمالی‌» است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به چار و بیست باشد «‌مهرگانی‌»</p></div>
<div class="m2"><p>که خوانندش گروهی، مهربانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به‌پنج و بیست «‌ناقوس‌» است آری</p></div>
<div class="m2"><p>پس آنگه بیست با شش «‌نوبهاری‌»</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به‌هفت‌وبیست‌«‌نوشین‌باده‌»‌بگسار</p></div>
<div class="m2"><p>به‌هشت‌و بیست‌رخ بر «‌نیمروز» آر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بود «‌نخجیرگان‌» لحن نه و بیست</p></div>
<div class="m2"><p>همش قولی دگر نخجیرگانی است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سی‌ام‌ره‌« گنج گاو»‌است ای‌خردمند</p></div>
<div class="m2"><p>که او راگنج گاوان نیز خوانند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همش خوانند برخی گنج کاوس</p></div>
<div class="m2"><p>بود این هر سه ره با ذوق مانوس</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نظامی حذف کرد «‌آیین جمشید»</p></div>
<div class="m2"><p>ز «‌راح روح‌» هم دامن فروچید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هم افکندن از میانه «‌نوبهاری‌»</p></div>
<div class="m2"><p>پس‌آنگه‌ساخت لحنی چار، جاری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نخستین کرد یاد از «‌ساز نوروز»</p></div>
<div class="m2"><p>که باشد نوبهار آنجا ز نوروز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سوم را نام «‌فرخ‌روز» داده</p></div>
<div class="m2"><p>دگر « کیخسروی‌» نامی نهاده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو در این شعرها دقت فزایی</p></div>
<div class="m2"><p>توخود سی لحن را از بر نمایی</p></div></div>