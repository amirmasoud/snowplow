---
title: >-
    شمارهٔ ۷۸ - دزدان خر
---
# شمارهٔ ۷۸ - دزدان خر

<div class="b" id="bn1"><div class="m1"><p>شنیدم که دو دزد خنجرگذار</p></div>
<div class="m2"><p>خری را ربودند در رهگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی گفت بفروشم او را به زر</p></div>
<div class="m2"><p>نگه دارمش گفت دزد دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در این ماجرا، گفتگو شد درشت</p></div>
<div class="m2"><p>به دشنام پیوست و آخر به مشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حریفان ما مشت بر هم زنان</p></div>
<div class="m2"><p>که دزد دگر تافت خر را عنان</p></div></div>