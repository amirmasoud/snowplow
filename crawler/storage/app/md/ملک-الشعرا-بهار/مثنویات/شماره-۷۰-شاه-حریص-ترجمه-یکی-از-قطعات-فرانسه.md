---
title: >-
    شمارهٔ ۷۰ - شاه حریص ترجمهٔ یکی از قطعات فرانسه
---
# شمارهٔ ۷۰ - شاه حریص ترجمهٔ یکی از قطعات فرانسه

<div class="b" id="bn1"><div class="m1"><p>پی چیست این ساز و برگ نبرد؟</p></div>
<div class="m2"><p>هم این کشتی وییل جنگی و مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به‌«‌پیروس» گفت این‌، یکی هوشیار</p></div>
<div class="m2"><p>که همراز او بود و آموزگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی روم خواهم شدن‌، گفت شاه</p></div>
<div class="m2"><p>بدانجا که جویندم از دیرگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا گفت‌، گفت از پی گیر و دار</p></div>
<div class="m2"><p>ستودش خردمند آموزگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که این رای رائی است با دستگاه</p></div>
<div class="m2"><p>سکندر سزد کرد این یا که شاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه خواهیم کردن چو شد روم راست</p></div>
<div class="m2"><p>بگفتا همه خاک لاتن ز ما است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خردمند گفت اندرین نیست شک</p></div>
<div class="m2"><p>که آن جمله ما را بود یک‌بهٔک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر کار کوته شود؟ گفت نه‌!</p></div>
<div class="m2"><p>به سیسیل از آن پس درآرم بنه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همین کشتی و لشکر بی‌شمار</p></div>
<div class="m2"><p>درآید «‌سراکوش‌» را برکنار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دگرکارگفتا تمام است‌؟ گفت</p></div>
<div class="m2"><p>نه زآنروکه با آب و بادیم جفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همانگه بسنده است بادی بگاه</p></div>
<div class="m2"><p>که تا خاک « کارتاژ» باز است راه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون‌، گفت بر بندگان شه‌، درست</p></div>
<div class="m2"><p>که ما جمله گیتی بخواهیم جست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برانیم تا دامن قیروان</p></div>
<div class="m2"><p>به صحرای «‌لیبی‌» و ریگ روان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به مصر و حجاز اندر آییم تنگ</p></div>
<div class="m2"><p>وز آن پس بتازیم تا رودگنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو ازگنگ بگذشت یکران ما</p></div>
<div class="m2"><p>شد آن مرز و بوم نوین زان ما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بپیچیم از آن پس به توران‌زمین</p></div>
<div class="m2"><p>ز جیحون برانیم تا پشت چین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو این نیمه بخش جهان بزرگ</p></div>
<div class="m2"><p>درآمد به فرمان شاه سترگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به دستور ما گشت کار جهان</p></div>
<div class="m2"><p>چه فرمان کند شهریار جهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به پیروزی و شادی آنگاه گفت‌:</p></div>
<div class="m2"><p>توانیم خندید و نوشید و خفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدو گفت دستور آزادمرد</p></div>
<div class="m2"><p>که این را هم‌اکنون توانیم کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیفکنده پرخاش را هیچ بن</p></div>
<div class="m2"><p>بکن هرچه‌خواهی‌، که گوید مکن؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شنیدم که نشنید پند وزیر</p></div>
<div class="m2"><p>سوی روم شد پادشاه «‌اپیر»</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شکستی بزرگ اندر آمد بر اوی</p></div>
<div class="m2"><p>شکسته سوی خانه بنهاد روی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی خواست گیتی ستاند به‌زور</p></div>
<div class="m2"><p>که گیتی کشیدش به زندان گور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نصیحت بسی گفته‌اند اهل هوش</p></div>
<div class="m2"><p>ولی نیست گوش حقیقت‌نیوش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حقیقت برون از یکی حرف نیست</p></div>
<div class="m2"><p>کجا داند آن کز حقیقت بری است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حقیقت به کس روی با رو نشد</p></div>
<div class="m2"><p>از این رو سخن‌ها دگرگونه شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سخن از حقیقت گر آگه شدی</p></div>
<div class="m2"><p>درازی نهادی و کوته شدی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بهار از حقیقت یکی ذرّه دید</p></div>
<div class="m2"><p>بدو باز پیوست و از خود برید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو از خود رها گشت جاوید شد</p></div>
<div class="m2"><p>بدان ذره همراز خورشید شد</p></div></div>