---
title: >-
    شمارهٔ ۶۰ - آلفته
---
# شمارهٔ ۶۰ - آلفته

<div class="b" id="bn1"><div class="m1"><p>بُد اندر حدود چغاخور، لُری</p></div>
<div class="m2"><p>لری غولدنگی‌، چغاله خوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدش‌، بختیاری‌وش‌، آلفته‌نام</p></div>
<div class="m2"><p>وز آلفتگی بخت یارش مدام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نادانی و خست و عشق پیل‌</p></div>
<div class="m2"><p>مثل بود در بین ایل جلیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنی داشت کدبانو و خوشمزه</p></div>
<div class="m2"><p>ز جمله جهان عاشق خربزه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولی دایم از دست شوهر به‌رنج</p></div>
<div class="m2"><p>چو گنجینه از دست مار شکنج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدا داده بودش از آن شوی نیز</p></div>
<div class="m2"><p>نر و ماده بس کرهٔ خرد و ریز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی سال‌، فالیز لر شد خراب</p></div>
<div class="m2"><p>که آلفته آن را نداد ایچ آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درآمد پس تیر، مرداد ماه</p></div>
<div class="m2"><p>ز لر کُرّگان خاست فریاد و آه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زن لر بدوگفت با حال زار</p></div>
<div class="m2"><p>چه سازیم امسال بی‌سبز بار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز تو سر زد ای ابله خر، بزه</p></div>
<div class="m2"><p>که امسال ماندیم بی‌خربزه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خود این‌ سرزنش کار آلفته‌ ساخت</p></div>
<div class="m2"><p>مر او را بهٔکبار آشفته ساخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زخاک‌چغاخورچغک‌‌وارجست </p></div>
<div class="m2"><p>پیاده سوی اصفهان رخت بست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خودگفت تاکم کنم قهر زن</p></div>
<div class="m2"><p>روم خربزه آرم از بهر زن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به گرگاب رفت و دو روزی بماند</p></div>
<div class="m2"><p>وز آنجا به‌سوی چغاخور براند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی بار خربوزه همراه داشت</p></div>
<div class="m2"><p>ز بار گران ناله و آه داشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به تدبیر خود را سبک‌ بارکرد</p></div>
<div class="m2"><p>به هر ده‌قدم یک‌دو خربوزه خورد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نگه‌داشت خربوزهٔ خوب را</p></div>
<div class="m2"><p>درشت ‌و گران‌سنگ و مرغوب را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که گر دین و ایمان من می‌رود</p></div>
<div class="m2"><p>وگر جان شیرین ز تن می‌رود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من این آخرین هدیه را پیش زن</p></div>
<div class="m2"><p>برم تا بدانند طفلان من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که‌آلفته را هست‌غیرت‌بسی</p></div>
<div class="m2"><p>ندارد چو آلفته غیرت کسی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو شد چند فرسنگ بیرون ز راه</p></div>
<div class="m2"><p>هوا گشت تفتیده در گرمگاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگرچه برونسو سبکبار بود</p></div>
<div class="m2"><p>ولیک از درونسو پر آزار بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز بیم زن ارچه دهان روزه داشت</p></div>
<div class="m2"><p>ولیکن شکم داغ خربوزه داشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ازین حال آلفته بی‌تاب شد</p></div>
<div class="m2"><p>ز تاب حرارت دلش آب شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نگه کرد خربوزه‌ای دید تر</p></div>
<div class="m2"><p>خوش‌اندام و زرّین چو بالشت زر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برآورد چاقو ولی یکه خورد</p></div>
<div class="m2"><p>نهیب‌زن اندر دلش سکه خورد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به خود گفت‌: آلفته غیرت‌نمای</p></div>
<div class="m2"><p>به نزدیک مردم حمیت‌نمای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سر و همسرانت همه نام‌جوی</p></div>
<div class="m2"><p>نگهدار نزدیکشان آبروی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پس آنگاه فکری به مغز آمدش</p></div>
<div class="m2"><p>که ارمان خربوزه آسان شدش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به‌خودگفت یاران سفر می کنند</p></div>
<div class="m2"><p>ازین راه دایم گذر می کنند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ازین خربزه من ببرّم کمی</p></div>
<div class="m2"><p>به پهنای دینار یا درهمی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کز اینجای چون مردمان بگذرند</p></div>
<div class="m2"><p>بر این خوردن خربزه بنگرند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بگویند از اینجا گذشتست خان</p></div>
<div class="m2"><p>شود آبرویم فزون زین نشان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سپس حمله‌ورگشت بر خربزه</p></div>
<div class="m2"><p>بخورد آنچه‌ را یافت‌ زان خوشمزه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بینداخت آن پوست‌های دراز</p></div>
<div class="m2"><p>بر آن مانده از مغز بسیار باز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چوآن خورد لختی توقف نمود</p></div>
<div class="m2"><p>ازبن کاو شده‌خان‌به‌خود پف‌نمود!</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شکمبارهٔ پر هوا و هوس</p></div>
<div class="m2"><p>بدین رای نستوده ننموده بس</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به خود گفت آن را به دندان زنم</p></div>
<div class="m2"><p>که گوبند خان چاکری داشت هم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>درافتاد بر پوست‌ها چون هژبر</p></div>
<div class="m2"><p>به‌ دندان زد آن پوست‌های سطبر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو از گوشت ‌آن‌ پوست‌ها شد تهی</p></div>
<div class="m2"><p>بیفکند و شد چند گامی رهی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به خود گفت‌ خان اسب هم داشته</p></div>
<div class="m2"><p>که از خربزه پوست نگذاشته</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو این‌ نور الهامش از مغز تافت</p></div>
<div class="m2"><p>از آن پوست‌هاکس نشانی نیافت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مگر دل ندادش کزان بگذرد</p></div>
<div class="m2"><p>وزان پوست‌ها رنج و زحمت برد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پس آنگه بپا خاست چون نرّه‌شیر</p></div>
<div class="m2"><p>که پوید سوی خانه و زن، دلیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نگه کرد و آن تخم خربوزه دید</p></div>
<div class="m2"><p>ز رنگ خوش آن دلش بردمید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به خود گفت هر چیز در عالمست</p></div>
<div class="m2"><p>ز بهر نشاط بنی‌آدمست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>من این نقش‌هایی که بستم همه</p></div>
<div class="m2"><p>نبودند جز یافه و دمدمه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چه‌حاصل که این تخم مانم بجای</p></div>
<div class="m2"><p>که گویند خان هشته آنجای پای</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز بهر من ایدر چه حاصل شود؟</p></div>
<div class="m2"><p>چه خانی بیاید چه خانی رود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو دل‌را به‌جاروب اندیشه رفت</p></div>
<div class="m2"><p>همی خورد آن تخم و با خویش گفت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همان به که گویند از این دهکده</p></div>
<div class="m2"><p>«‌نه خانی اویده نه خانی رده‌»</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو ازکف برون شد مهار هومن</p></div>
<div class="m2"><p>رهایی نیابد ازو هیچ کس</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سوارش اگر دشمن است ار که دوست</p></div>
<div class="m2"><p>برد تا بدان جا که دلخواه اوست</p></div></div>