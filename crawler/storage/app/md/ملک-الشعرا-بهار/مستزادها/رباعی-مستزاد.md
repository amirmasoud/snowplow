---
title: >-
    رباعی مستزاد
---
# رباعی مستزاد

<div class="b" id="bn1"><div class="m1"><p>پروانه و شمع و گل شبی آشفتند</p></div>
<div class="m2"><p>در طرف چمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز جور و جفای دهر با هم گفتند</p></div>
<div class="m2"><p>بسیار سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد صبح‌، نه پروانه به جا بود و نه شمع</p></div>
<div class="m2"><p>ناگاه صبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برگل بوزید و هر دو با هم رفتند</p></div>
<div class="m2"><p>من ماندم و من</p></div></div>