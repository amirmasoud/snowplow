---
title: >-
    کار ایران با خداست
---
# کار ایران با خداست

<div class="b" id="bn1"><div class="m1"><p>با شه ایران ز آزادی سخن گفتن خطاست</p></div>
<div class="m2"><p>کار ایران با خداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مذهب شاهنشه ایران ز مذهب‌ها جداست</p></div>
<div class="m2"><p>کار ایران با خداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه مست و شیخ مست و شحنه مست و میر مست</p></div>
<div class="m2"><p>مملکت رفته ز دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هردم از دستان مستان فتنه و غوغا بپاست</p></div>
<div class="m2"><p>کار ایران با خداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هردم از دریای استبداد آید بر فراز</p></div>
<div class="m2"><p>موج‌های جانگداز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبن تلاطم کشتی ملت به گرداب بلاست</p></div>
<div class="m2"><p>کار ایران با خداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مملکت کشتی‌، حوادث بحر و استبداد خس</p></div>
<div class="m2"><p>ناخدا عدلست و بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار پاس کشتی وکشتی‌نشین با ناخداست</p></div>
<div class="m2"><p>کار ایران با خداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پادشه خود را مسلمان خواند و سازد تباه</p></div>
<div class="m2"><p>خون جمعی بی گناه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای مسلمانان در اسلام این ستم‌ها کی رواست</p></div>
<div class="m2"><p>کار ایران با خداست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاه ایران گر عدالت را نخواهد باک نیست</p></div>
<div class="m2"><p>زانکه طینت پاک نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیدهٔ خفاش از خورشید در رنج و عناست</p></div>
<div class="m2"><p>کار ایران با خداست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باش تا آگه کند شه را ازین نابخردی</p></div>
<div class="m2"><p>انتقام ایزدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>انتقام ایزدی برق است و نابخرد گیاست</p></div>
<div class="m2"><p>کار ایران با خداست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سنگر شه چون بدوشان تپه رفت از باغ شاه</p></div>
<div class="m2"><p>تازه‌تر شد داغ شاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روز دیگر سنگرش در سرحد ملک فناست</p></div>
<div class="m2"><p>کار ایران با خداست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باش تا خود سوی ری تازد ز آذربایجان</p></div>
<div class="m2"><p>حضرت ستارخان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن که توپش قلعه کوب و خنجرش کشورگشاست</p></div>
<div class="m2"><p>کار ایران با خداست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باش تا بیرون ز رشت آید سپهدار سترگ</p></div>
<div class="m2"><p>فر دادار بزرگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنکه گیلان ز اهتمامش رشک اقلیم بقاست</p></div>
<div class="m2"><p>کار ایران با خداست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باش تا از اصفهان صمصام حق گردد پدید</p></div>
<div class="m2"><p>نام حق گردد پدید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا ببینیم آنکه سر ز احکام حق پیچدکجاست</p></div>
<div class="m2"><p>کار ایران با خداست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خاک ایران‌، بوم و برزن از تمدن خورد آب</p></div>
<div class="m2"><p>جز خراسان خراب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هرچه هست از قامت ناساز بی‌اندام ماست</p></div>
<div class="m2"><p>کار ایران با خداست</p></div></div>