---
title: >-
    شام ایران روز باد
---
# شام ایران روز باد

<div class="b" id="bn1"><div class="m1"><p>عیدنوروزاست هر روزی به ما نوروز باد</p></div>
<div class="m2"><p>شام ایران روز باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنجمین سال حیات ما به ما فیروز باد</p></div>
<div class="m2"><p>روز ما بهروز باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برق‌تیغ ماجهان پرداز و دشمن‌سوز باد</p></div>
<div class="m2"><p>جیش ما کین توز باد</p></div></div>
<div class="b2" id="bn4"><p>سال استقلال ما را باد آغاز بهار</p>
<p>با نسیم افتخار</p></div>
<div class="b" id="bn5"><div class="m1"><p>یاد باد آن نوبهار رفته وان پژمرده باغ</p></div>
<div class="m2"><p>وآن خزان‌تیز چنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وان همه محنت که بر بلبل رسید از جور زاغ</p></div>
<div class="m2"><p>در ره ناموس و ننگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وان ز خون نوجوانان برکران باغ و راغ</p></div>
<div class="m2"><p>لاله های‌رنگ رنگ</p></div></div>
<div class="b2" id="bn8"><p>وان ز قد راد مردان درکنار جوببار</p>
<p>سروهای خاکسار</p></div>
<div class="b" id="bn9"><div class="m1"><p>یاد باد آن باغبان کزکینه آتش درفکند</p></div>
<div class="m2"><p>در فضای این چمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وان نسیم مهرگان کامد و از بیخ کند</p></div>
<div class="m2"><p>لاله و سرو سمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن یکی بر هرزه کرد انباز رنج سخت‌، بند</p></div>
<div class="m2"><p>گلبنان ممتحن</p></div></div>
<div class="b2" id="bn12"><p>وان‌دگر بر خیره کرد آوبز چوب خشک دار</p>
<p>میوه‌های خوشگوار</p></div>
<div class="b" id="bn13"><div class="m1"><p>بر کران گلشن تبریز آتش درگرفت</p></div>
<div class="m2"><p>از نسیم جور شاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کشت از آن آتش که ناگه اندران کشور گرفت</p></div>
<div class="m2"><p>خون مسکینان تباه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون ز مردی و دلیری ره برآن لشکر گرفت</p></div>
<div class="m2"><p>لشگر مشروطه‌خواه</p></div></div>
<div class="b2" id="bn16"><p>لشکر همسایه ناگه سر برآورد ازکنار</p>
<p>با هزاران گیر و دار</p></div>
<div class="b" id="bn17"><div class="m1"><p>کاین منم افشرده پا اندر ره صلح و وداد</p></div>
<div class="m2"><p>نیست‌ازمن‌ خوف‌ و بیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آمدستم تا ببندم ره بر آشوب و فساد</p></div>
<div class="m2"><p>بر طریق مستقیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اله اله ز آن تطاول‌، اله اله زان عناد</p></div>
<div class="m2"><p>ای خداوند کریم</p></div></div>
<div class="b2" id="bn20"><p>این‌چه جوراست و عداوت این‌چه بغض‌است ونقار</p>
<p>زبن کروه بار بار</p></div>
<div class="b" id="bn21"><div class="m1"><p>اندک اندک زین بهانه سوی قزوین کرد روی</p></div>
<div class="m2"><p>وحشیانه‌جیش‌روس‌</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در شمال ملک ما افتاد ز ایشان های و هوی</p></div>
<div class="m2"><p>ای‌درپغ‌وای‌فسوس‌</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درخراسان هم در آن هنگامه روس خیره‌پوی</p></div>
<div class="m2"><p>ازستم‌بنواخت کوس‌</p></div></div>
<div class="b2" id="bn24"><p>حامی اشرار شد و افکند در مشهد شرار</p>
<p>نی نهان‌، بل آشکار</p></div>
<div class="b" id="bn25"><div class="m1"><p>یاد بادا آن مه خرداد و آن جان باختن</p></div>
<div class="m2"><p>در ره ناموس و دین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وان به سوی قبهٔ الاسلام توپ انداختن</p></div>
<div class="m2"><p>بر عناد مسلمین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قومی از بی‌دانشی کار وطن را ساختن</p></div>
<div class="m2"><p>نیز قومی در کمین</p></div></div>
<div class="b2" id="bn28"><p>تا که میدانی به دست آرند در آن گیر و دار</p>
<p>غافل‌ازانجام کار</p></div>
<div class="b" id="bn29"><div class="m1"><p>غافل از این کاسمان هر روز بازی‌ها کند</p></div>
<div class="m2"><p>برخلاف رای مرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ملت بیدار دل‌، گردن فرازی‌ها کند</p></div>
<div class="m2"><p>روز پیکار و نبرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کردگار دادگستر, کارسازی‌ها کند</p></div>
<div class="m2"><p>بر مرام اهل درد</p></div></div>
<div class="b2" id="bn32"><p>تاکه اهل درد راگردد زمانه سازگار</p>
<p>چرخ، ‌رام ‌و بخت، ‌یار</p></div>
<div class="b" id="bn33"><div class="m1"><p>یاد باد و شاد باد آن سرو آزاد وطن</p></div>
<div class="m2"><p>حضرت ستارخان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن که داد از رادی و مردانگی داد وطن</p></div>
<div class="m2"><p>اندر آذربایجان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>راد باقرخان کزو شد سخت بنیاد وطن</p></div>
<div class="m2"><p>شاد بادا جاودان</p></div></div>
<div class="b2" id="bn36"><p>یاد بادا ملت تبریز و آن مردان کار</p>
<p>مایه‌های افتخار</p></div>
<div class="b" id="bn37"><div class="m1"><p>یاد باد آن جیش گیلان وآن همه غرنده شیر</p></div>
<div class="m2"><p>وان یورش های بزرگ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>وان مهین سردار اسعد وان سپهدار دلیر</p></div>
<div class="m2"><p>وان جوانان سترگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یادباد آن در سفارتخانه از جان گشته سیر</p></div>
<div class="m2"><p>چون ز شیر آشفته گرگ</p></div></div>
<div class="b2" id="bn40"><p>وان حمایت پیشگان همسایگان دوستار</p>
<p>بوده او را در جوار</p></div>
<div class="b" id="bn41"><div class="m1"><p>یاد بادا آن طبیب روسی عیسی نفس</p></div>
<div class="m2"><p>وان رحیم دردمند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>وان دوای روح‌پرورکش نباشد دسترس</p></div>
<div class="m2"><p>جزکه‌بیماری نژند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وان شفای عاجل و جنگ‌آوری‌های سپس</p></div>
<div class="m2"><p>وآن‌همه رنج وگزند</p></div></div>
<div class="b2" id="bn44"><p>وان بهانه جستن و آوردن اندر آن دیار</p>
<p>لشکروحشی‌شعار</p></div>
<div class="b" id="bn45"><div class="m1"><p>اینک اینک سال نوشد آفرین بر سال نو</p></div>
<div class="m2"><p>هم بر این اقبال نو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سال نو هردم زند بر ملک ایران فال نو</p></div>
<div class="m2"><p>دل کند آمال نو</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ماضی ماکهنه شد بنگر به استقبال نو</p></div>
<div class="m2"><p>فر و استقلال نو</p></div></div>
<div class="b2" id="bn48"><p>فر و استقلال نو باشد در استقبال کار</p>
<p>منت از پروردگار</p></div>
<div class="b" id="bn49"><div class="m1"><p>هم‌جواران را به‌ما انصاف کاری هست نیست</p></div>
<div class="m2"><p>رو بکن کار دگر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>قوم مغرب را بر اهل شرق یاری هست نیست</p></div>
<div class="m2"><p>رو بجو یار دگر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خو‌د خریداری برین افغان و زاری هست نیست</p></div>
<div class="m2"><p>رو به بازار دگر</p></div></div>
<div class="b2" id="bn52"><p>زان که کس را دل به حال کس نمی‌سوزد، بهار</p>
<p>کار باید کرد کار</p></div>