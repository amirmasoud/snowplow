---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>به نام آنکه جان را داد مسکن</p></div>
<div class="m2"><p>چو یوسف اندرون محبس تن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نام آنکه دلها کرد نخجیر</p></div>
<div class="m2"><p>ز عشق افکندشان بر پای زنجیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهانبان کارفرمای یگانه</p></div>
<div class="m2"><p>گناه آمرز مردم بی بهانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنون بخش دل شوریده حالان</p></div>
<div class="m2"><p>خرد بخشنده ی نازک خیالان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان داننده ی هر بی زبانی</p></div>
<div class="m2"><p>سخن سازنده ی هر نکته دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پناه از کین دهر آوارگان را</p></div>
<div class="m2"><p>به رحمت چاره گر بیچارگان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ضمایر را به علم غیب دانا</p></div>
<div class="m2"><p>خلایق را خداوند توانا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انیس خاطر خلوت گزینان</p></div>
<div class="m2"><p>چراغ افروز بزم شب نشینان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غریبان را به کنج غم پرستار</p></div>
<div class="m2"><p>طبیب دردمندان دل افگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرفتاران زندانخانه ی غم</p></div>
<div class="m2"><p>به یادش در شکنج بند خرّم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرح بخش دل اندوهناکان</p></div>
<div class="m2"><p>فروغ افزای روشن جان پاکان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نمک پاش جراحتهای کاری</p></div>
<div class="m2"><p>قرار هر دلی در بی قراری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شکرریز مذاق تلخکامان</p></div>
<div class="m2"><p>سحرگاه امید تیره شامان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جز او بیچارگان را دادرس نیست</p></div>
<div class="m2"><p>جز او کس در بلا فریادرس نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به ظاهر خانهٔ او کعبهٔ گل</p></div>
<div class="m2"><p>به باطن منزلش در خانهٔ دل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز چشم سَر جمالش گر نهان است</p></div>
<div class="m2"><p>به چشم سِر گرش بینی عیان است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز دلهای شکسته تختگاهش</p></div>
<div class="m2"><p>شهیدان بلا یکسر سپاهش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مبرّا ذاتش از هر عیب و نقصان</p></div>
<div class="m2"><p>به یادش هر دلی در سینه رقصان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنین یکتا خدایی را ستایش</p></div>
<div class="m2"><p>همی گویم همی جویم نیایش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کنون زان به که بهر عرض حاجات</p></div>
<div class="m2"><p>به درگاهش کنم ساز مناجات</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خداوندا مرا بخشا زبانی</p></div>
<div class="m2"><p>پی پوزش در آن شیرین بیانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا جا در صف اهل وفا ده</p></div>
<div class="m2"><p>دلی پر نور جانی باصفا ده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز دام هر من و مایی رها کن</p></div>
<div class="m2"><p>ز خود بیگانه با خویش آشنا کن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو خود دانی ز فرط تیره بختی</p></div>
<div class="m2"><p>ندیدم در جهان جز رنج و سختی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نکردم ای خداوند یگانه</p></div>
<div class="m2"><p>ثوابی جز گناه اندر زمانه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدی زشتی همیشه پیشه ی من</p></div>
<div class="m2"><p>خیال ناصواب اندیشه ی من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگرچه رو سیاه و شرمسارم</p></div>
<div class="m2"><p>امید از رحمتت بسیار دارم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به رحمت گر نبخشایی گناهم</p></div>
<div class="m2"><p>فسوسا بر من و روی سیاهم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چرا رخ را به نومیدی خراشم</p></div>
<div class="m2"><p>من از ابلیس مجرم تر نباشم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر چند او سزای نقمت توست</p></div>
<div class="m2"><p>هنوزش چشم سوی رحمت توست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو با من آن کن ای رحمان غفّار</p></div>
<div class="m2"><p>که از بخشایشت باشد سزاوار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا در فقر گنج سلطنت ده</p></div>
<div class="m2"><p>به تن زیب از لباس مسکنت ده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>درین گیتی به خویشم ساز محتاج</p></div>
<div class="m2"><p>در آن گیتی بده ز آمرزشم تاج</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرا محکم به خود فرما توکل</p></div>
<div class="m2"><p>ز غیرم کن رها دست توسل</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جدا از هر درم پیوند گردان</p></div>
<div class="m2"><p>به باب خویش حاجتمند گردان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرا مگذار در دل جز خویش</p></div>
<div class="m2"><p>تن آسایی ده از هر رنج و تشویش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو حفظ از حیلت اهریمنم کن</p></div>
<div class="m2"><p>چو خصم توست با وی دشمنم کن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مهل کاین خصم بر من چیره گردد</p></div>
<div class="m2"><p>وز او تابنده روزم تیره گردد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بکن روشن جهان بینم به محشر</p></div>
<div class="m2"><p>ز دیدار دلارای پیمبر</p></div></div>