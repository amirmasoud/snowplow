---
title: >-
    شمارهٔ ۳ -  در منقبت شاه اولیاء علیه السلام گوید
---
# شمارهٔ ۳ -  در منقبت شاه اولیاء علیه السلام گوید

<div class="b" id="bn1"><div class="m1"><p>سریر آرای ایوان هدایت</p></div>
<div class="m2"><p>امیرالمؤمنین شاه ولایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امام اولین ملک جهان را</p></div>
<div class="m2"><p>خبردار آشکارا و نهان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چراغ چشم و روح جسم آدم</p></div>
<div class="m2"><p>نگین دار رسالت سرّ خاتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طلسم گنج پنهان خدایی</p></div>
<div class="m2"><p>خدیو تختگاه کبریایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علی عالی آن دارای دوران</p></div>
<div class="m2"><p>که قدرش رانده بر نُه چرخ یکران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دانش لوح محفوظ امامت</p></div>
<div class="m2"><p>به بخشش گنج مکنون کرامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهدار پیمبر زوج زهرا</p></div>
<div class="m2"><p>که از وی طره ی دین شد مطرّا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهی بهر رسول برگزیده</p></div>
<div class="m2"><p>دل و دست و زبان و گوش و دیده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پی اثبات دین کردگارش</p></div>
<div class="m2"><p>هویدا نفی شرک از ذوالفقارش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کلید رزق جود بی دریغش</p></div>
<div class="m2"><p>فنای خلق در دندان تیغش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که جز آن افتخار آل هاشم</p></div>
<div class="m2"><p>بهشت و دوزخ حق راست قاسم؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز او بر کوثر احمد که ساقی است؟</p></div>
<div class="m2"><p>که غیر از وی خدا را وجه باقی است؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که را دخت نبی بانوی مشکوست؟</p></div>
<div class="m2"><p>که را خیبرگشا نیروی بازوست؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نبی را کیست صاحب سرّ معراج؟</p></div>
<div class="m2"><p>که را نص خلافت بد به سر تاج؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سلامی وافر از یزدان دادار</p></div>
<div class="m2"><p>بر آن داور که امت راست سالار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دگر بر جفت او دخت پیمبر</p></div>
<div class="m2"><p>خداوندان دین را پاک مادر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دگر بر یازده فرزند پاکش</p></div>
<div class="m2"><p>گهرهای ثمین تابناکش</p></div></div>