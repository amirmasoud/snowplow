---
title: >-
    شمارهٔ ۲ -  فی نعت النبی صلی الله علیه و آله و سلم
---
# شمارهٔ ۲ -  فی نعت النبی صلی الله علیه و آله و سلم

<div class="b" id="bn1"><div class="m1"><p>زیر افکن تاج چیردستان</p></div>
<div class="m2"><p>سرسلسلهٔ خداپرستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیضا علم و ستاره موکب</p></div>
<div class="m2"><p>قدسی حشم و براق مرکب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اول غرض از نظام عالم</p></div>
<div class="m2"><p>آخر نبی از نژاد آدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن گوهر تاج تارک عشق</p></div>
<div class="m2"><p>اقلیم ستان بلارک عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرمانده ساکنان بالا</p></div>
<div class="m2"><p>سالار پیمبران والا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود شاه و فرشتگان سپاهش</p></div>
<div class="m2"><p>بر ذروهٔ عرش تختگاهش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلطان بهشت هشت باب او</p></div>
<div class="m2"><p>معجز ده چارمین کتاب او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایجاد کن جهان وجودش</p></div>
<div class="m2"><p>عالم همه ریزه خوار جودش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جبریل غلام آستانش</p></div>
<div class="m2"><p>دهلیز سرای آسمانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بادا ز خدا درود بسیار</p></div>
<div class="m2"><p>بر آن شه پاک و آل اطهار</p></div></div>