---
title: >-
    بخش ۵۱ - کشتن مختار عمرو و ابن صبیح
---
# بخش ۵۱ - کشتن مختار عمرو و ابن صبیح

<div class="b" id="bn1"><div class="m1"><p>که بد آن بد اختر هم از دشمنان</p></div>
<div class="m2"><p>بدندی به فرمانش سنگ افکنان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمودندی آن فرقه ی تیره بخت</p></div>
<div class="m2"><p>به دارای دین سنگباران سخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبانگاه خیره دل و تیره رای</p></div>
<div class="m2"><p>بدی عمرو خفته به بام سرای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن شب غلامان میر اجل</p></div>
<div class="m2"><p>به سرتاختندش چو پیک اجل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکندندش از پا به زخم سنان</p></div>
<div class="m2"><p>به دوزخ بگرداند ریمن عنان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن پس به هر خانه و برزنی</p></div>
<div class="m2"><p>که جست از شهنشاه دین دشمنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفت و سرافکند و بردار کرد</p></div>
<div class="m2"><p>تن بی روانشان نگونسار کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان خانمانشان به آتش بسوخت</p></div>
<div class="m2"><p>رخ دین زکردار خود برفروخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو این داستان اندر آمد به بن</p></div>
<div class="m2"><p>ز رزم براهیم بشنو سخن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که با پور مرجانه آن شیر مرد</p></div>
<div class="m2"><p>چه کرد او چو شد با سپه هم نبرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدایا ببخشای مزد نکوی</p></div>
<div class="m2"><p>به فرخ براهیم آزاده خوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که او نیز در راه دین رنج برد</p></div>
<div class="m2"><p>زمین را ز آل امیه سترد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زما کردگار جهان را سپاس</p></div>
<div class="m2"><p>چنان کایدر از مرد یزدان شناس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهان آفرین اوست، ما بنده گان</p></div>
<div class="m2"><p>به یکتایی او را پرستنده گان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازو جست باید همی زینهار</p></div>
<div class="m2"><p>وزو بود بیاد همی ترس کار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بباید همی در سخن گستری</p></div>
<div class="m2"><p>نخستین به نامش ستایش گری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آن پس درود آوری بی کران</p></div>
<div class="m2"><p>به روش روان، پاک پیغمبران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به ویژه شهنشاه یثرب زمین</p></div>
<div class="m2"><p>جهان را امان و خدا را امین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خداوند این احد تاجدار</p></div>
<div class="m2"><p>همایون فرستاده ی کردگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خدا نیست لیکن جهان آفرین</p></div>
<div class="m2"><p>بود از خداوند جهان آفرین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برآن شاه و بر آل پاکش درود</p></div>
<div class="m2"><p>رسد هر دم از پاک یزدان درود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به ویژه علی آنکه جز او نبود</p></div>
<div class="m2"><p>هال نبی زیر چرخ کبود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز حق باد بر پیروانش سلام</p></div>
<div class="m2"><p>همی تا نماید قیامت قیام</p></div></div>