---
title: >-
    بخش ۴۰ - به درک فرستادن مختار اسحق ابن اشعث پلید
---
# بخش ۴۰ - به درک فرستادن مختار اسحق ابن اشعث پلید

<div class="b" id="bn1"><div class="m1"><p>چو آمد به اسحق اشعث خبر</p></div>
<div class="m2"><p>که شد کشته شمر بد اختر گهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بترسید و لرزید آن اهرمن</p></div>
<div class="m2"><p>بر آن شد که جانش بر آید زتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخست آنکه با تیغ یازید دست</p></div>
<div class="m2"><p>تن شاه هر دو جهان را بخست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مر این بد گهر زاده بود آن پلید</p></div>
<div class="m2"><p>که چونین پلیدی خدا نافرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی خواهرش بود آن بد سگال</p></div>
<div class="m2"><p>که عبداله کاملش بد همال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی از سرای خود آن گمرها</p></div>
<div class="m2"><p>روان شد به مشکوی عبداللها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهانی بر خواهر آمد بگفت:</p></div>
<div class="m2"><p>که راز از برادرت باید شنفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا نیست غیر از تو فریاد رس</p></div>
<div class="m2"><p>به کوفه ندارم به غیر از تو کس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشسته است مختار فرخ به گاه</p></div>
<div class="m2"><p>بدانسان که بر کرسی چرخ ماه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رده بر رده مهتران بر درش</p></div>
<div class="m2"><p>فزونتر زانجم بود لشگرش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به خون شه یثرب آن رهنمون</p></div>
<div class="m2"><p>همی شهر کوفه بشوید زخون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر آنکس که بازاده ی مصطفی (ص)</p></div>
<div class="m2"><p>سگالیده پیکار زاهل جفا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نگردد ز چنگال خشمش رها</p></div>
<div class="m2"><p>رود گر فرو در دم اژدها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بتر دشمن شاه بیکس منم</p></div>
<div class="m2"><p>که اکنون هراسنده از دشمنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به شوی سرافراز از من بگوی</p></div>
<div class="m2"><p>که ای نامور گوهر پاکخوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به زنهار خود گر پنهاهم دهی</p></div>
<div class="m2"><p>در این خانه ی خویش راهم دهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دهد زینهارت به روز شمار</p></div>
<div class="m2"><p>در آن سخت هنگامه پروردگار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا یادگاری تو از مادرم</p></div>
<div class="m2"><p>سرافرازی و مهربان خواهرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در این سخت هنگامه و داوری</p></div>
<div class="m2"><p>مرا ای پسندیده کن یاوری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدو خواهرش گفت: دلشاد باش</p></div>
<div class="m2"><p>زبند غم و غصه آزاد باش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بمان اندر این خانمان تندرست</p></div>
<div class="m2"><p>زمهمان کسی کینه هرگز نجست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نپیچد همانا سر از گفت من</p></div>
<div class="m2"><p>که مردیست با داد و دین جفت من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگفت این و از مهر بنواختش</p></div>
<div class="m2"><p>به جایی در آن خانه بنشاختش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برمیهمان برد خوان و خورش</p></div>
<div class="m2"><p>زهر گونه تا تن دهد پرورش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شبانگه که عبداله بی نظیر</p></div>
<div class="m2"><p>به کاخ اندر آمد زنزد امیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خرامان به نزدیک او رفت جفت</p></div>
<div class="m2"><p>بزد پنجه بر دامن مرد و گفت:</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که ای پر خرد مهتر سرفراز</p></div>
<div class="m2"><p>به مشکوی تو سالیان دراز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شب و روز بودم پرستار وش</p></div>
<div class="m2"><p>پی خدمتت دست کرده به کش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه جز سوی تو دیده بگماشتم</p></div>
<div class="m2"><p>به بی رای تو گام برداشتم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نمودم به هر کار در یاری ات</p></div>
<div class="m2"><p>کنیزانه کردم پرستاری ات</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که درسایه ی خویش راهم دهی</p></div>
<div class="m2"><p>چو زنهار خواهم پناهم دهی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برآور کنون ای خداوند من</p></div>
<div class="m2"><p>امید دل آرزومند من</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برادرم آن شاخ بی برگ و بر</p></div>
<div class="m2"><p>که باشد مرا یادگار از پدر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کنون اندر این مرز بیچاره است</p></div>
<div class="m2"><p>زکاشانه ی خویش آواره است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر چه به هنگامه ی کربلا</p></div>
<div class="m2"><p>ستم کرد برشاه اهل ولا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ولی زانچه گردید زو آشکار</p></div>
<div class="m2"><p>فزونتر بود رحمت کردگار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نبسته است دادار خورشید و ماه</p></div>
<div class="m2"><p>در توبه بر روی اهل گناه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز بگذشته کردار بر وی مگیر</p></div>
<div class="m2"><p>بدوبخش و این خواهش از من پذیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از ایدر گراینده شو عذرخواه</p></div>
<div class="m2"><p>امیر گزین را سوی پیشگاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بگوی آنچه دانی بدان پاکخوی</p></div>
<div class="m2"><p>به شیرین زبانی و طرز نکوی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بخواه از جهانجوی فرخ تبار</p></div>
<div class="m2"><p>که بخشد برادرم را زینهار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برادرش را گفت آن سرفراز</p></div>
<div class="m2"><p>بگو تا بیاید به سویم فراز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدو گفت زن کای جهاندیده مرد</p></div>
<div class="m2"><p>به گرد بدی تا توانی مگرد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دل از کین درمانده گان پاک کن</p></div>
<div class="m2"><p>گرت زهر خشمی است تریاک کن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مرا دل به سوگند آسوده ساز</p></div>
<div class="m2"><p>که آسوده دل مانی و سرفراز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو سوگند خوردی که با وی بدی</p></div>
<div class="m2"><p>نیندیشی از مردی و بخردی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زبیغوله سوی تو باز آرمش</p></div>
<div class="m2"><p>به پوزشگری با نیاز آرمش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دلیر گرانمایه سوگند خورد</p></div>
<div class="m2"><p>که با او نخواهم بدی پیشه کرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>برفت بیاورد آن بد سگال</p></div>
<div class="m2"><p>برادرش را سوی فرخ همال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو اسحق دید آن بر و یال مرد</p></div>
<div class="m2"><p>زمین را ببوسید و پس لابه کرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدو گفت: کای مهتر پاکزاد</p></div>
<div class="m2"><p>هشیوار مرد ستوده نژاد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به زنهاری خویش منت بنه</p></div>
<div class="m2"><p>رهایی ز چنگال مرگم بده</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز سالار کشور مرا بازخواه</p></div>
<div class="m2"><p>اگر چه گنهکارم و روسیاه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چنان دان که کهتر غلامی به زر</p></div>
<div class="m2"><p>خریدستی ای بخرد نامور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو گفت این و آن کینه گستر چکید</p></div>
<div class="m2"><p>سرشکش زمژگان به ریش پلید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بدو گفت عبداله هوشمند</p></div>
<div class="m2"><p>که مگری تو کز ما نبینی گزند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>من اینک روانم به سوی امیر</p></div>
<div class="m2"><p>بخواهم ز سالار پوزش پذیر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>که بخشد به من بر،گناه تو را</p></div>
<div class="m2"><p>کند سرخ روی سیاه تو را</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>وزان پس بپیمود راه از سرای</p></div>
<div class="m2"><p>به درگاه مختار فرخنده رای</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بگفت: ای سپهدار کشور پناه</p></div>
<div class="m2"><p>به درگاهت آمد یکی دادخواه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مرا آرزویی است در دل برآر</p></div>
<div class="m2"><p>مکن در بر انجمن شرمسار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بدانسان که دادی به داماد خویش</p></div>
<div class="m2"><p>امان، ای سرافراز فرخنده کیش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ببخشای اسحق را هم به من</p></div>
<div class="m2"><p>بکن سرخ رویم بر انجمن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>که اکنون نهانست در خان من</p></div>
<div class="m2"><p>پناهنده گشته است و مهمان من</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو فرخنده مختار ازو این شنید</p></div>
<div class="m2"><p>بدزدید یال و دم اندر کشید</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نمی خواست آزرده آن مرد را</p></div>
<div class="m2"><p>که بد شیر نیزار ناورد را</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بگفتا: کنم من روا کام تو</p></div>
<div class="m2"><p>برآرم به خورشید بر نام تو</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>یکی کار پیش آمد اکنون بزرگ</p></div>
<div class="m2"><p>بدان دل گمارد دلیر سترگ</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به اسب اندرآی و بجنبان سنان</p></div>
<div class="m2"><p>از ایدر برو سوی خرمابنان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>شنیدستم ای گرد فرخنده نام</p></div>
<div class="m2"><p>که از قاتلان شه تشنه کام</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>در آنجا نهانند ده نابکار</p></div>
<div class="m2"><p>هراسان و ترسان و آسیمه کار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ببند آن ده اهریمن شوم را</p></div>
<div class="m2"><p>بپرداز از ایشان برو بوم را</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ببوسید عبداله بی نظیر</p></div>
<div class="m2"><p>به فرمان مختار فرخنده رای</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به انگشت دیدش یک انگشتری</p></div>
<div class="m2"><p>درخشان چو در آسمان مشتری</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بدوگفت: کاین پر بها خاتما</p></div>
<div class="m2"><p>که ماند به انگشترین جما</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>مرا ده که بر صنعتش بنگرم</p></div>
<div class="m2"><p>یکی گر چنو باشد اندر خورم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بگویم که استاد پردازدش</p></div>
<div class="m2"><p>سزاوار انگشت من سازدش</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>برون کرد خاتم زانگشت مرد</p></div>
<div class="m2"><p>بدو داد و از کاخ شد رهنورد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ابا همرهان سوی خرمانبان</p></div>
<div class="m2"><p>بگرداند او بارگی را عنان</p></div></div>