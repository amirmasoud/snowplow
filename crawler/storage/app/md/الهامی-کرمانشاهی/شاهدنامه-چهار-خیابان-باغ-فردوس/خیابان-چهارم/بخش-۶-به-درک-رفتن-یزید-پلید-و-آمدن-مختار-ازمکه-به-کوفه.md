---
title: >-
    بخش ۶ - به درک رفتن یزید پلید و آمدن مختار ازمکه به کوفه
---
# بخش ۶ - به درک رفتن یزید پلید و آمدن مختار ازمکه به کوفه

<div class="b" id="bn1"><div class="m1"><p>ره آتش تیز بگرفت پیش</p></div>
<div class="m2"><p>سزا دید از زشت کردار خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدید ازخدا کیفر کار زشت</p></div>
<div class="m2"><p>به داس عقوبت درود آنچه کشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از این مژده مختار شد شادمان</p></div>
<div class="m2"><p>زبطحا سوی کوفه آمد دمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دست اندرش، حکم دارای دین</p></div>
<div class="m2"><p>پناه جهان سیدالساجدین (ع)</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که عمش به فرمان آن شهریار</p></div>
<div class="m2"><p>به نامه درون کرده بود آن نگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درآن خط به مختار اذن خروج</p></div>
<div class="m2"><p>بداده که برگاه بنما عروج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خون در نشان، دشمن شاه را</p></div>
<div class="m2"><p>زتن، سر برانداز، بدخواه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درآندم که مختار فرخ تبار</p></div>
<div class="m2"><p>زبطحا سوی کوفه شد رهسپار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به شام اندرون، بود مروان امیر</p></div>
<div class="m2"><p>به بطحا زبیری نژاد شریر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدی والی از سوی پور زبیر</p></div>
<div class="m2"><p>به کوفه یکی مرد، خالی زخیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به شهر اندر آمد چو مختار باز</p></div>
<div class="m2"><p>به فرمانده از وی بگفتند راز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که پیوسته پیمان ز مردان کار</p></div>
<div class="m2"><p>بگیر از پی کوشش وکارزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به آن گشت درکوفه فرمانروای</p></div>
<div class="m2"><p>کشد سخت کیفر زما و شما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فلک باز بد خواه مختار شد</p></div>
<div class="m2"><p>به زندان والی گرفتار شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی نامه بنوشت آن پاک کیش</p></div>
<div class="m2"><p>دگر باره از بهر داماد خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که من ای تو در هر غمی یاورم</p></div>
<div class="m2"><p>به زندان کوفه به بند – اندرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بپیچیده بر گردنم اژدهای</p></div>
<div class="m2"><p>مرا از دم اژدها کن رهای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هیون برد آن نامه را همچو باد</p></div>
<div class="m2"><p>به یثرب به فرزند فاروق، داد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو پور عمر خواند آن نامه را</p></div>
<div class="m2"><p>بدانست آن سخت هنگامه را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به فرمانده ی کوفه پیغام داد</p></div>
<div class="m2"><p>که از من درود فزون بر تو باد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شنید ستم از کینه مختار را</p></div>
<div class="m2"><p>همان نیک مرد بی آزار را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گرفتار بند گران کرده ای</p></div>
<div class="m2"><p>بسی بی گناهش بیازرده ای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به پیوند من، این ستم ها چرا؟</p></div>
<div class="m2"><p>بیندیش لختی از این ماجرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سبکسر مباش و ز بند گران</p></div>
<div class="m2"><p>رها ساز زودش، و گرنه بدان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زیثرب سپاهی فراهم کنم</p></div>
<div class="m2"><p>تو را خانه ی سور، ماتم کنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فرستاده آن نامه را همچو باد</p></div>
<div class="m2"><p>بیاورد و با والی کوفه داد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بترسید و بگشود اندر زمان</p></div>
<div class="m2"><p>زبند گران، یال پیل دمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز زندان به کاشانه آمد چو مرد</p></div>
<div class="m2"><p>بیفتاد در فکر ساز نبرد</p></div></div>