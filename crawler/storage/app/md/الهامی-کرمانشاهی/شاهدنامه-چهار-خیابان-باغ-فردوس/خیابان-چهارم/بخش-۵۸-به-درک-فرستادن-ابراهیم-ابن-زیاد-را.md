---
title: >-
    بخش ۵۸ - به درک فرستادن ابراهیم، ابن زیاد را
---
# بخش ۵۸ - به درک فرستادن ابراهیم، ابن زیاد را

<div class="b" id="bn1"><div class="m1"><p>که با تیغ کین روی کرده دژم</p></div>
<div class="m2"><p>همی مرد و مرکب فکندی به هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلیرانه در پهنه جستی نبرد</p></div>
<div class="m2"><p>به زیرش یکی اسب گیتی نورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مر آن باره با گوهر آگین ستام</p></div>
<div class="m2"><p>سبک پوی و زرینه زین و لگام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دیدش براهیم نشناختش</p></div>
<div class="m2"><p>بزد نیزه وز زین بینداختش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدو زخم کاری زدو درگذشت</p></div>
<div class="m2"><p>بکوشید تا روز بیگاه گشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ستوران دوان برتن کشته گان</p></div>
<div class="m2"><p>که بودند به خون بر آغشته گان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بماند اندک از شامیان بر به جای</p></div>
<div class="m2"><p>نبدشان به رزم سپهدار پای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو شب تیره شد ساز کین ریختند</p></div>
<div class="m2"><p>غریوان سوی شام بگریختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه چارپایان و آن خواسته</p></div>
<div class="m2"><p>همان چتر و خرگاه آراسته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بماند و سراسر به تاراج رفت</p></div>
<div class="m2"><p>سری کو بجستی همی تاج رفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تهی گشت گیتی زجور و فساد</p></div>
<div class="m2"><p>که شد کشته در جنگ ابن زیاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بتر دشمن شاه دین کشته شد</p></div>
<div class="m2"><p>سر بخت مروانیان گشته شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرفت اهرمن سور از ماتمش</p></div>
<div class="m2"><p>جهنم بشد شادمان در غمش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آن مرد ناپاک شد خاک پاک</p></div>
<div class="m2"><p>عزازیل را شد دل از درد چاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گل بخت اسلامیان بر شکفت</p></div>
<div class="m2"><p>رخ کفر در ظلمت اندر نهفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو برخرگه چرخ بنشست ماه</p></div>
<div class="m2"><p>به گردش رده بر زد انجام سپاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیامد سپهبد به جای نشست</p></div>
<div class="m2"><p>زخون سپه شست شمشیر و دست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ببوسید پیش خداوند خاک</p></div>
<div class="m2"><p>همی بر زمین سود رخسار پاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفت: از تو ای داور هور و ماه</p></div>
<div class="m2"><p>کنون دیدم این مردی و فر و جاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو دادی وگرنه مرا تاب نیست</p></div>
<div class="m2"><p>مرا نیرو از خود به هر باب نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به جان آرزوی دل آوردی ام</p></div>
<div class="m2"><p>تو در چیر دستی مدد کردی ام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رخ بختم از یاری ات شد سفید</p></div>
<div class="m2"><p>مرا ورنه برخورد نبود این امید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از این بنده ای داور بی نیاز</p></div>
<div class="m2"><p>روان شه تشنه خوشنود ساز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زسوی حسین (ع) آفرینم فرست</p></div>
<div class="m2"><p>درود از رسول امینم فرست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهان داورا آفرین و سپاس</p></div>
<div class="m2"><p>زما برتو زیبد برون از قیاس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مر این جنگ و رزم و سپاه کشن</p></div>
<div class="m2"><p>کیم من؟ تو کردی نبود این زمن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ولیکن ندانم که ابن زیاد</p></div>
<div class="m2"><p>کجا شد که نفرین برآن پرفساد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر او رسته باشد زچنگال من</p></div>
<div class="m2"><p>بدا بر من و وای بر حال من</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدین رنج بسیارم آید دریغ</p></div>
<div class="m2"><p>بماند گر او ایمن از زخم تیغ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدینسان همی گفت با کردگار</p></div>
<div class="m2"><p>که خورشید سر برزد از کوهسار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دلیران به وی برنهادند روی</p></div>
<div class="m2"><p>همه آفرین خوان و احسنت گوی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که شد زنده جان اشتر نامور</p></div>
<div class="m2"><p>بدین دست و شمشیر و این زور و فر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنو باب را چو تو بایست پور</p></div>
<div class="m2"><p>که بادت نگهبان خداوند هور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدین فتح و نصرت که اندوختی</p></div>
<div class="m2"><p>رخ دین پیغمبر افروختی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شه کربلا شد به مینو درا</p></div>
<div class="m2"><p>زتو سرخ رو پیش پیغمبرا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنین گفت با مهتران سپاه</p></div>
<div class="m2"><p>سپهبد براهیم ناورد خواه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که فرزند مرجانه ی نابکار</p></div>
<div class="m2"><p>ندانم کجا رفت از این کارزار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شداو کشته یازنده ماند و گریخت</p></div>
<div class="m2"><p>جهان مهلتش داد و خونش نریخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>درآندم که لشگر بدند از دو سوی</p></div>
<div class="m2"><p>به میدان پیگار ناورد جوی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یکی دیو دیدم چو کوه بلند</p></div>
<div class="m2"><p>به زیرش یکی باد پیما سمند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به دستش پرندی که از بیم آن</p></div>
<div class="m2"><p>بدی اهرمن را هراسند، جان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همی کشتی از لشکر ما به تیغ</p></div>
<div class="m2"><p>نمی کرد از کشتن کس دریغ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو دیدم من او را به دو تاختم</p></div>
<div class="m2"><p>به یک زخمش از زین در انداختم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو افتاد از باره ی تیز گام</p></div>
<div class="m2"><p>از او بوی مشک آمدم برمشام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گمانم که او پور مرجانه بود</p></div>
<div class="m2"><p>همان آشنا روی بیگانه بود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یکی گفت: کای سرفراز دلیر</p></div>
<div class="m2"><p>که دندان تیغت بود چنگ شیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شناسم من آن دیو بدخوی را</p></div>
<div class="m2"><p>همان نابکار سیه روی را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به زخمی که دارد بر آن پلید</p></div>
<div class="m2"><p>که از معجزه ی شاه دین شد پدید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سر شاه را چون برش ارمغان</p></div>
<div class="m2"><p>بیاورد زشت بد اختر سنان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به زانو نهاد آن تبه روزگار</p></div>
<div class="m2"><p>سر سبط پیغمبر تاجدار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یکی قطره خون از سرشه چکید</p></div>
<div class="m2"><p>به رانش و زانسوی آن شد پدید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زران بداختر همان خون پاک</p></div>
<div class="m2"><p>گذشت و چو مرجان فرو شد به خاک</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از آن خون پاکش چوشد ریش، ران</p></div>
<div class="m2"><p>شد اهریمن بد گهر سرگران</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دل از سوزش زخمش آمد به درد</p></div>
<div class="m2"><p>بدان سر، ستم هر چه می خواست کرد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ولی زخم او از بهی دور بود</p></div>
<div class="m2"><p>همیشه از آن زخم رنجور بود</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زگندیده بویی که بودش به ران</p></div>
<div class="m2"><p>همی نافه ی مشک بستی به آن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>من او را شناسم از آن زخم بر</p></div>
<div class="m2"><p>کنون تا چه فرمان دهد نامور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>زگفتار او شد براهیم شاد</p></div>
<div class="m2"><p>از آن پس که از شه به غم کرد یاد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو لختی به سوگ شه دین گریست</p></div>
<div class="m2"><p>به سوی پرستنده گان بنگریست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بفرمود: از ایدر به میدان روید</p></div>
<div class="m2"><p>پژوهنده ی جسم ریمن شوید</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بیارید آن پیکر شوم را</p></div>
<div class="m2"><p>بتر دشمن شاه مظلوم را</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>پژوهنده یی سوی میدان دوید</p></div>
<div class="m2"><p>بدید و بیاورد جسم پلید</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سپهدار فرزانه خوی جوان</p></div>
<div class="m2"><p>چو دید آن تن تیره ی بی روان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زانده، نی دل پر از ناله کرد</p></div>
<div class="m2"><p>زخون جگر دیده پر ژاله کرد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به شاهنشه کربلا می گریست</p></div>
<div class="m2"><p>نه بر آن پلید دغا می گریست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو لختی بمویید و بوسید خاک</p></div>
<div class="m2"><p>به شکرانه پیش خداوند پاک</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کت از من سپاس ای جهان آفرین</p></div>
<div class="m2"><p>که ماندی مرا زنده جان اینچنین</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بدیدم به خاک و به خون خفته پست</p></div>
<div class="m2"><p>تن پور مرجانه ی بت پرست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بینداخت زان پس دلیر جوان</p></div>
<div class="m2"><p>به روی بداندیش آب دهان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بفرمود دژخیم را تا برید</p></div>
<div class="m2"><p>سر از پیکر تیره جان پلید</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تنش را نگونگسار بردار کرد</p></div>
<div class="m2"><p>زخود شادمان پاک دادار کرد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بریدند سر چون زبیدادگر</p></div>
<div class="m2"><p>دومار از دو گوشش برآورد سر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دگر باره گشتندش اندر دهان</p></div>
<div class="m2"><p>دو پیچیده مارگزنده نهان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>از آن مارها کز سر اهرمن</p></div>
<div class="m2"><p>بدیدند آن نامور انجمن</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>شگفتی همه لب گشودند باز</p></div>
<div class="m2"><p>به تسبیح نیکی ده بی نیاز</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چنین دید کیفر عبید زیاد</p></div>
<div class="m2"><p>که یزدان کنادش عقوبت زیاد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>وزان پس براهیم نیک اخترا</p></div>
<div class="m2"><p>دلیر و خردمند و دین پرورا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بفرمود سرهای بد گوهران</p></div>
<div class="m2"><p>که بودند در لشگر از مهتران</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بریدند و با آن سر بی خرد</p></div>
<div class="m2"><p>که بادش زدادار، نفرین بد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به نیزه نمودند پس استوار</p></div>
<div class="m2"><p>شمردندشان بود بیش از هزار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ابا گوهر وگنج و با خواسته</p></div>
<div class="m2"><p>زاسبان و خرگاه آراسته</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>فرستاد سوی عراق آن همه</p></div>
<div class="m2"><p>که مختار بودش شبان رمه</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چو آگاهی آمد به میر سترگ</p></div>
<div class="m2"><p>از آن کوشش و رزم و فتح بزرگ</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>پذیره شدن را بپیمود راه</p></div>
<div class="m2"><p>زکوفه ابا مهتران سپاه</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>زن و مرد بهرتماشا به دشت</p></div>
<div class="m2"><p>شدند و جهان بنگه سورگشت</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو دید آن سران را امیر دلیر</p></div>
<div class="m2"><p>به نیزه شد از پشت توسن به زیر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به شکرانه پیش خداوند پاک</p></div>
<div class="m2"><p>دو رخ راهمی سود بر روی خاک</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>که ای برتر از آنچه سنجد خرد</p></div>
<div class="m2"><p>به ذات تو کی وهم کس پی برد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>منم اینکه بینم چنین استوار</p></div>
<div class="m2"><p>به نیزه سردشمن شهریار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>سرپور مرجانه را بر سنان</p></div>
<div class="m2"><p>تو کردی نه من ای خدای جهان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نه من کرده ام نی براهیم این</p></div>
<div class="m2"><p>تو کردی که زیبد تو را آفرین</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>سردشمن شاه را نیزه دار</p></div>
<div class="m2"><p>که بد بر فراز سنان استوار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بیاورد زی میر کشور پناه</p></div>
<div class="m2"><p>سرافراز مختار ناورد خواه</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چو مختار دید آن سر پر غرور</p></div>
<div class="m2"><p>دلش آمد از کینه ی او به شور</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>شد از خشم، روی سپیدش بنفش</p></div>
<div class="m2"><p>ابر بینی اش سود زرینه کفش</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چو بسیار بر روی او نعل سود</p></div>
<div class="m2"><p>برون کرد از پای خود موزه زود</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>پرستنده را گفت: این را ببر</p></div>
<div class="m2"><p>بشوی و میاور به سویم دگر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چو بوسیده لختی لب دشمنش</p></div>
<div class="m2"><p>پلید است دیگر نخواهم منش</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>دگر موزه پوشید و آمد روان</p></div>
<div class="m2"><p>به کاخ امارت امیر جوان</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>بگفتا به یاران که سور است این</p></div>
<div class="m2"><p>همان گان جشن و سرور است این</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>به کوفه درون چنگ عشرت زنید</p></div>
<div class="m2"><p>به سر کوس را پنج نوبت زنید</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>سر پور مرجانه را وان سران</p></div>
<div class="m2"><p>که هستند از بد گهر مهتران</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>همی با سنان سوی بازارها</p></div>
<div class="m2"><p>برید و نمایید آزارها</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بدان سر فشانید هر لحظه خاک</p></div>
<div class="m2"><p>کزو شد دل اهل اسلام چاک</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>کنیدش به کاخ آنگه آویخته</p></div>
<div class="m2"><p>که او بر پلیدی شود ریخته</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بدانسان که فرمود آن نامدار</p></div>
<div class="m2"><p>بکردند با آن سر نابکار</p></div></div>