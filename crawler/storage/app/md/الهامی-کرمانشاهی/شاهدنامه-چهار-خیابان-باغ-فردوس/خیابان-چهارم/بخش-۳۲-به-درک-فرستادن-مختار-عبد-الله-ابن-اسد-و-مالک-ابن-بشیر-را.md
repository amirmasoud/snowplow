---
title: >-
    بخش ۳۲ - به درک فرستادن مختار عبد الله ابن اسد و مالک ابن بشیر را
---
# بخش ۳۲ - به درک فرستادن مختار عبد الله ابن اسد و مالک ابن بشیر را

<div class="b" id="bn1"><div class="m1"><p>که بربسته بودش به بندی گران</p></div>
<div class="m2"><p>که پردخته بودند آهنگران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدش نام عبداله بن اسد</p></div>
<div class="m2"><p>که نفرین رسادش به روح و جسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفت مختار کای بد نژاد</p></div>
<div class="m2"><p>که چون تو پلیدی ز مادر نزاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا رفته بودی به کرب و بلا</p></div>
<div class="m2"><p>به پیکار دارای اهل ولا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا سوختی خرگه شاه را</p></div>
<div class="m2"><p>زدی در دل آتش خور و ماه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سراپرده ای را فکندی ز پای</p></div>
<div class="m2"><p>که روح الامین بردرش جبهه سای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفتا: که این من به فرمان خویش</p></div>
<div class="m2"><p>نکردم چنین آمد از خواجه پیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا خواجه ام داد فرمان چنین</p></div>
<div class="m2"><p>از او این بدی آمد از من مبین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بفرمود دژخیم را نامدار</p></div>
<div class="m2"><p>که برادرش از تن، سر نابکار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز جا جست دژخیم و خنجر گرفت</p></div>
<div class="m2"><p>بزد دست و ریش بد اختر گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بردی از تن او سر پر غرور</p></div>
<div class="m2"><p>بیفزود اله جهان را سرور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبشتند نامش بدان دفترا</p></div>
<div class="m2"><p>که بودی به دست دبیر اندرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس از بد گهر مالک ابن بشیر</p></div>
<div class="m2"><p>بپرداخت روی زمین را امیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که او هم سوی کربلا رفته بود</p></div>
<div class="m2"><p>به پیکار دشت بلا رفته بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو نام بد اندیش بنوشته شد</p></div>
<div class="m2"><p>که او هم به هنجار بد کشته شد</p></div></div>