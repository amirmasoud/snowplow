---
title: >-
    بخش ۳۷ - کشتن مختار
---
# بخش ۳۷ - کشتن مختار

<div class="b" id="bn1"><div class="m1"><p>بگفت ای زدین بی خبر روی زشت</p></div>
<div class="m2"><p>تو را نیز کشتن بود سرنوشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفرمود تا بر زدندش به دار</p></div>
<div class="m2"><p>نگون سرشده با کمند استوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به زه کرد سالار کشور کمان</p></div>
<div class="m2"><p>یکی تیر بگشاد بر بد گمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که بنشست بر چشم و جست از قفاش</p></div>
<div class="m2"><p>چنین دید بیدین سزای جفاش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپس تیر بارانش کردند سخت</p></div>
<div class="m2"><p>به فرمان مختار پیروز بخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گشت اسپری آتش افروختند</p></div>
<div class="m2"><p>تن دوزخی را در آن سوختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپس عمر و حجاج را سوی میر</p></div>
<div class="m2"><p>کشیدند خوار و نژند و اسیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که بود او نگهبان آب فرات</p></div>
<div class="m2"><p>زآتش مباداش هرگز نجات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مختار بگماشت بر وی نظر</p></div>
<div class="m2"><p>به دژخیم فرمود کاین را ببر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدانسان که دانی مر او را رسان</p></div>
<div class="m2"><p>بدان کشته و سوخته ناکسان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به فرموده دژخیم رخ بر فروخت</p></div>
<div class="m2"><p>ببرد و بکشت و تن او بسوخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن پس حکیم طفیل نژند</p></div>
<div class="m2"><p>که دست سپهدار ایمان فکند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زدیدار او میر بی ترس و باک</p></div>
<div class="m2"><p>بغرید چون ضیغم خشمناک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدوگفت کای اهرمن زاده مرد</p></div>
<div class="m2"><p>یکی کار کردی که کافر نکرد</p></div></div>