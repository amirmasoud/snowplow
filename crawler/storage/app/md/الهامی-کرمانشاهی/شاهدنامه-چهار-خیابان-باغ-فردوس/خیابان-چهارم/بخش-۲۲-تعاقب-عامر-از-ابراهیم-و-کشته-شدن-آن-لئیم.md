---
title: >-
    بخش ۲۲ - تعاقب عامر از ابراهیم و کشته شدن آن لئیم
---
# بخش ۲۲ - تعاقب عامر از ابراهیم و کشته شدن آن لئیم

<div class="b" id="bn1"><div class="m1"><p>که آن بستگان بند بگسیختند</p></div>
<div class="m2"><p>چو شب بود بس تیره بگریختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شنیدند افغان او را سپاه</p></div>
<div class="m2"><p>سوی خیمه ی او گرفتند راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو عامر از این کارآگاه شد</p></div>
<div class="m2"><p>پر از اخگرش جان گمراه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیامد دمان تا بر روزبان</p></div>
<div class="m2"><p>بگفتش رها بادت از تن روان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رها ساختی آن بداندیش را</p></div>
<div class="m2"><p>به شمشیر دادی سرخویش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کردی که از بند آن اژدها</p></div>
<div class="m2"><p>چنین رایگان کرد خود را رها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگو راست، گر زندگی بایدت</p></div>
<div class="m2"><p>وگرنه به خون، تن، ببالایدت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدو گفت حاجب که ای کامیاب</p></div>
<div class="m2"><p>تو دانی منم دشمن بوتراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیاران او هست بیزاری ام</p></div>
<div class="m2"><p>بود راست گر این نیازاری ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدین میخ های چو شاخ درخت</p></div>
<div class="m2"><p>ببستیمشان دست و هم پای سخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که از سختی بند تا نیم شب</p></div>
<div class="m2"><p>نبدشان ز فریاد، خاموش، لب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان آمدی بانگ ایشان به گوش</p></div>
<div class="m2"><p>که پوشیدی از پاسبانان خروش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دمی گشت بیننده ام گرم خواب</p></div>
<div class="m2"><p>چو بیدار گشتم چنین روی زرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زخشم ارکنی از میانم دو نیم</p></div>
<div class="m2"><p>من این کار را دانم از آن ندیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به ایشان مگر داشت پنهان سری</p></div>
<div class="m2"><p>که نگذاشت دوشت به افسونگری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بریزی از ایشان به شمشیر، خون</p></div>
<div class="m2"><p>جهان را از این رنج آری برون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همی خواست تا شب نماید رها</p></div>
<div class="m2"><p>به نیرنگشان از دم اژدها</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر او باد نفرین پروردگار</p></div>
<div class="m2"><p>که با حیلت، او راست پیوسته کار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ندارد چو ما از تو این مرد بیم</p></div>
<div class="m2"><p>که باشد شب و روز با تو ندیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی نیک اندیشه را برگمار</p></div>
<div class="m2"><p>ببین تا بود از که اینگونه کار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خدا کرد بربدمنش تیره، رای</p></div>
<div class="m2"><p>سخن های وی دردلش کرد جای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به بر خواندش آن بخت وارون ندیم</p></div>
<div class="m2"><p>نپرسیده کرد از میانش دو نیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وزان پس به حاجت یکی برگماشت</p></div>
<div class="m2"><p>سبک خود علم سوی هامون فراشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گروها گروه از پی وی سپاه</p></div>
<div class="m2"><p>پی آن دو تن برگرفتند راه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درآن تیره شب اندران پهندشت</p></div>
<div class="m2"><p>سواران پراکنده شد هفت و هشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پژوهشکنان ای یل ارجمند</p></div>
<div class="m2"><p>غریوان سپاه و خروشان سمند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زگرد سواران هوا کله بست</p></div>
<div class="m2"><p>زنعل تکاور همی برق جست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>توگفتی برست از بن هر گیاه</p></div>
<div class="m2"><p>یکی مرد با نیزه ی کینه خواه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وزان سوی سالار با پای خویش</p></div>
<div class="m2"><p>ره کوفه را داشت پویان به پیش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو خورشید سر بر زد از تیغ کوه</p></div>
<div class="m2"><p>بدیدش سپهدارگرد گروه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگفتا: بدان یار کامد سپاه</p></div>
<div class="m2"><p>پس و پشت بنگر به گرد سیاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شو آماده از بهر ناورد کین</p></div>
<div class="m2"><p>چو شیرافکنان برشکن آستین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدو گفت مرد ای یل نامدار</p></div>
<div class="m2"><p>زمن چشم ناورد مردان مدار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من این کار دشوار آسان کنم</p></div>
<div class="m2"><p>به بیغوله ای خویش پنهان کنم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو را می سپارم به یزدان فرد</p></div>
<div class="m2"><p>که هم بخت یاری و هم شیر مرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگفت این و بدرود مهتر نمود</p></div>
<div class="m2"><p>به بیراهه ای شد روان همچو دود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سپهبد بدان ره که بودش شتافت</p></div>
<div class="m2"><p>رخ از بیم سوی دگر بر نتافت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پیاده همی رفت زان سان دلیر</p></div>
<div class="m2"><p>پیاده که پوید سوی غاب شیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نه بیمش زتنهایی خویش بود</p></div>
<div class="m2"><p>نه از های و هوی بداندیش بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به ناگاه آن شیر پرکین و خشم</p></div>
<div class="m2"><p>درختی کهن شاخش آمد به چشم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به خود گفت آن به که براین درخت</p></div>
<div class="m2"><p>برآیم بیاسایم آنجا دو لخت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو آسوده گشتم برآیم به راه</p></div>
<div class="m2"><p>که بی اسب نتوان شدن رزمخواه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به نزد درخت آمد آن ارجمند</p></div>
<div class="m2"><p>به شاخش بیفکند پیچان کمند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در انبوه آن شاخ ها شد نهان</p></div>
<div class="m2"><p>تو گفتی که شیری است در نیستان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وزان سوی چون دیو جسته زبند</p></div>
<div class="m2"><p>همی تاخت عامر به هر سو سمند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زبس تاخت از تابش آفتاب</p></div>
<div class="m2"><p>چو دوزخ تنش گشت پر التهاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو از دور آن سایه گستر بدید</p></div>
<div class="m2"><p>بزد اسب و خود را بدان سو کشید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گمانش که نزد درخت است آب</p></div>
<div class="m2"><p>ندانست کانجاست مرگش به خواب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو آید قضا می شود مرد کور</p></div>
<div class="m2"><p>به پای خود ایدون رود سوی گور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو روباه را روز آید به شام</p></div>
<div class="m2"><p>دود تا بر شیر سوی کنام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو آمد ندید اندر آنجای آب</p></div>
<div class="m2"><p>برفت از عطش ازتنش توش و تاب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>فرود آمد و اسب را بر درخت</p></div>
<div class="m2"><p>ببست و درآن سایه افکند رخت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کمر برگشود و زخفتان گره</p></div>
<div class="m2"><p>فکند از بر سینه یکسو زره</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به دامان همی باد برخود وزید</p></div>
<div class="m2"><p>زگرما چو اژدر همی بردمید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زمان تا زمان سود پهلو به خاک</p></div>
<div class="m2"><p>سروریش پرخاک گشتی رخ بد نهاد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>رسیدی به وی چون تف جانگزای</p></div>
<div class="m2"><p>به شیر خدا گفتی او ناسزای</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو بشنید از او سخن های زشت</p></div>
<div class="m2"><p>دلاور از سر، بردباری بهشت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو باز شکاری زشاخ درخت</p></div>
<div class="m2"><p>به پرواز آمد سوی تیره بخت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بیامد به پیش اندرش ایستاد</p></div>
<div class="m2"><p>چو چشم بداندیش بر وی افتاد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سراسیمه برجست و گفتا: که ای؟</p></div>
<div class="m2"><p>در این شاخ چون مرغ بهر چه ای؟</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بگفتا: که هستم براهیم یل</p></div>
<div class="m2"><p>تو را از آسمان می رسم چون اجل</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همانم که دوش از جهان آفرین</p></div>
<div class="m2"><p>تو را خواستم کشته از تیغ کین</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تو پاسخ بدادی به بیغاره ام</p></div>
<div class="m2"><p>ببستی در آن بند بیچاره ام</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ندانستی این را که با کردگار</p></div>
<div class="m2"><p>منش ناید و خود پسندی به کار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>هم ایدون به خود زار لختی بموی</p></div>
<div class="m2"><p>زشمشیر من بسته جان آفرین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>فرستمت ایدون به دیگر سرای</p></div>
<div class="m2"><p>که دانی کدام است شیر خدای</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بگفت این و برسینه ی او نشست</p></div>
<div class="m2"><p>بپیچید موی پلیدش به دست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به خنجر سرش را زتن دور کرد</p></div>
<div class="m2"><p>بر او اشکم کرکسان، گور کرد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زهی دست پرورده ی بو تراب</p></div>
<div class="m2"><p>که بودش دل شیر و چنگ عقاب</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کسی را که یزدان بخواهد بلند</p></div>
<div class="m2"><p>زبند و زدارش نیاید گزند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>وزان پس به یکران او شد سوار</p></div>
<div class="m2"><p>سرش را به فتراک بر بست زار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بزد اسب و زی کوفه آمد چو باد</p></div>
<div class="m2"><p>وزان سوی مختار فرخ نژاد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو برزد سر از کوه رخشنده شید</p></div>
<div class="m2"><p>براهیم یل را به لشگر ندید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>پژوهش چو بنمود و بازش نیافت</p></div>
<div class="m2"><p>بدانست کو سوی دشمن شتافت</p></div></div>