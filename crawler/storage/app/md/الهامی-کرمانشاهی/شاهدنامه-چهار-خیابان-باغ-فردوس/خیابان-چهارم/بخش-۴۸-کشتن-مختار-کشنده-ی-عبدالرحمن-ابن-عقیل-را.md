---
title: >-
    بخش ۴۸ - کشتن مختار، کشنده ی عبدالرحمن ابن عقیل را
---
# بخش ۴۸ - کشتن مختار، کشنده ی عبدالرحمن ابن عقیل را

<div class="b" id="bn1"><div class="m1"><p>کشیدند زان پس دو مرد لعین</p></div>
<div class="m2"><p>به دربار سالار نصرت قرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که شد عبد رحمن سلیل عقیل</p></div>
<div class="m2"><p>به شمشیر آن نابکاران قتیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فرمان میر آتش افروختند</p></div>
<div class="m2"><p>تن دوزخی را در آن سوختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی زشت مرد از نژاد حرام</p></div>
<div class="m2"><p>که اش بود زید بن ورقاش نام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا و چون صف کربلا گشت راست</p></div>
<div class="m2"><p>بیفکند عباس را دست راست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همین نابکار ستم پیشه را</p></div>
<div class="m2"><p>بد آیین و خوی بد اندیشه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کین روزنابان کشیدند خوار</p></div>
<div class="m2"><p>به درگاه مختار فرمانگزار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بدخواه را چشم سالار دید</p></div>
<div class="m2"><p>به خشم آمد و لب به دندان گزید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو رخ بر زمین سود شکرانه را</p></div>
<div class="m2"><p>چو دید آن ز دادار بیگانه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بفرمود تا از تنش هردو دست</p></div>
<div class="m2"><p>به خنجر فکندند برخاک، پست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگونسار کردند از آن پس به دار</p></div>
<div class="m2"><p>زدندش به تن سنگ و پیکان هزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپس با همان دارش آتش زدند</p></div>
<div class="m2"><p>چنین کیفر کرده را بستدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به مینو شد از کار مختار راد</p></div>
<div class="m2"><p>روان سپهدار عباس (ع) شاد</p></div></div>