---
title: >-
    بخش ۹ - آگاهی دادن ایاس شحنه ی کوفه، عبدالله بن مطیع
---
# بخش ۹ - آگاهی دادن ایاس شحنه ی کوفه، عبدالله بن مطیع

<div class="b" id="bn1"><div class="m1"><p>بیامد نهان پیش والی بگفت</p></div>
<div class="m2"><p>سخن ها که ازکار مهتر شنفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو عبدالله آگه از این راز شد</p></div>
<div class="m2"><p>دژم گشت و با اندوه انباز شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابا شحنه گفتا: تو هشیار باش</p></div>
<div class="m2"><p>همه شب پی پاس بیدار باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زکار آزموده سواران کار</p></div>
<div class="m2"><p>گروهی به هر رهگذر برگمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بد اختر ایاس آنچه بشنید زو</p></div>
<div class="m2"><p>پذیرفت و در دم بیامد به کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی زاده اش بود راشد به نام</p></div>
<div class="m2"><p>که ابلیس را، زو روا بود کام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمودش به سوی کناسه روان</p></div>
<div class="m2"><p>به همراه او برخی از پیروان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مردی کسی کز یلان نام داشت</p></div>
<div class="m2"><p>به هر کوی و هر رهگذر برگماشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی گشت خود گرد بازار وکوی</p></div>
<div class="m2"><p>از آن کار سر بسته در جستجوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شبی زاده ی مالک نامدار</p></div>
<div class="m2"><p>سوی کاخ مختار بد رهسپار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ابا وی دلیران فیروز بخت</p></div>
<div class="m2"><p>زره پوش گردیده در زیر رخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به کویی گذشته بعد از شب سه پاس</p></div>
<div class="m2"><p>که برخورد بر آن دلیران ایاس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیفتاد چون دیده دژخیم را</p></div>
<div class="m2"><p>به دیدار فرخ براهیم را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بلرزید زاندیشه دل در برش</p></div>
<div class="m2"><p>تو گفتی که آمد اجل برسرش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سر ره به مرد دلاور گرفت</p></div>
<div class="m2"><p>ازآن شب روی پرسش اندرگرفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگفتش: که درنیم شب سوی کوی</p></div>
<div class="m2"><p>نیاید مگر مرد آشوب جوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به شب هر که بینی به کنجی بخفت</p></div>
<div class="m2"><p>مگر تو که مکرت بود در نهفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ببندم کنونت به زنجیر یال</p></div>
<div class="m2"><p>برم پیش والی دهم گوشمال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دلاور بدو هیچ پاسخ نداد</p></div>
<div class="m2"><p>چو نزدیک تر گشت بازو گشاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به پاسخ بزد تیغ برگردنش</p></div>
<div class="m2"><p>بدان سان که سردور گشت از تنش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو پاسخ شنید از دم تیغ او</p></div>
<div class="m2"><p>ز کوشش بیاسود پرخاشجو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دلیران که با آن دلاور بدند</p></div>
<div class="m2"><p>مر او را بر هر کار یاور بدند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برون آمدند از کمین ناگهان</p></div>
<div class="m2"><p>بگفتند نام خدای جهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به یاران شحنه نهادند روی</p></div>
<div class="m2"><p>هیاهو برآمد زبازار و کوی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به بازار از آن گروه پلید</p></div>
<div class="m2"><p>دم تیغشان جوهر جان خرید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گشودند گفتی چو بازارگان</p></div>
<div class="m2"><p>خسان از پی جان فروشی دکان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فروزنده همچون چراغ عسس</p></div>
<div class="m2"><p>به بازار، نوک سنان بود و بس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شد از پیکر زشت اهل نفاق</p></div>
<div class="m2"><p>پر از کشته بازار تا پیش طاق</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زبس تیغ راندند یاران دین</p></div>
<div class="m2"><p>نماند از بد اندیش یک مرد کین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نمودند آن پر دلان میهمان</p></div>
<div class="m2"><p>سگ کوی را از تن پاسبان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو ره را ز بیگانه پرداختند</p></div>
<div class="m2"><p>سوی کاخ مختار در تاختند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سپهبد به مختار فرخنده کیش</p></div>
<div class="m2"><p>بگفت آنچه در راهش آمد به پیش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدو گفت مختار: کای نامدار</p></div>
<div class="m2"><p>به نیکی جزا یابی از کردگار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که در یاری شاه سبط حجاز</p></div>
<div class="m2"><p>تو کردی نخستین در رزم باز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرا درچنین کار ای کاردان</p></div>
<div class="m2"><p>نهان تو مرهم به ناسور جان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>براهیم چو از امیر این شنفت</p></div>
<div class="m2"><p>چو غنچه رخ او ز شادی شکفت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بگفتا: چو زینگونه افتاد کار</p></div>
<div class="m2"><p>هم ایدون بساز از پی کارزار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به بام سرای آتشی برفروز</p></div>
<div class="m2"><p>از آن پیش کاین شب در آید به روز</p></div></div>