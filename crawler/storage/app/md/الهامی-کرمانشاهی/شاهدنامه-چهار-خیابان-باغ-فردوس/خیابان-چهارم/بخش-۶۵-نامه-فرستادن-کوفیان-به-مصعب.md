---
title: >-
    بخش ۶۵ - نامه فرستادن کوفیان به مصعب
---
# بخش ۶۵ - نامه فرستادن کوفیان به مصعب

<div class="b" id="bn1"><div class="m1"><p>که ایدر شتاب آر فرمان تو راست</p></div>
<div class="m2"><p>همه گنج و شهر و تن و جان تو راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>براهیم جنگی به موصل دراست</p></div>
<div class="m2"><p>سپه دور و، مختار بی یاور است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه کوفیانند با وی به کین</p></div>
<div class="m2"><p>تو خود گو چه آید زمردی چنین؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا کام خود را برآورده بین</p></div>
<div class="m2"><p>رخ بخت مختار در پرده بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرستاده اش برد و مصعب بخواند</p></div>
<div class="m2"><p>همانگه سوی شهر لشگر براند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو کوفیان برنبستند راه</p></div>
<div class="m2"><p>گشودند دروازه برآن سپاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غو کوس ازکوفه شد بر سپهر</p></div>
<div class="m2"><p>ببرید گردون زمختار مهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه شهر شد پر درفش و سنان</p></div>
<div class="m2"><p>سواران جنگی عنان در عنان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غو گاو دم نعره ی باره گی</p></div>
<div class="m2"><p>فلک را بدرید یکباره گی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهر سو فروزان به سر مشعله</p></div>
<div class="m2"><p>ز بانگ درا شهر پر غلغله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپهبد چو بشنید آن های و هوی</p></div>
<div class="m2"><p>بدانست کورا چه آمد به روی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همانا بدو از سروش آگهی</p></div>
<div class="m2"><p>بیامد که کاخ از تو آید تهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گزید از پی بنده گی گوشه ای</p></div>
<div class="m2"><p>که گیرد پی آن سفر توشه ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درآن گوشه بنشست و زاری گرفت</p></div>
<div class="m2"><p>به خود برهمی اشکباری گرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیفراشت دست و خدا را بخواند</p></div>
<div class="m2"><p>ستایش بگفت و نیایش براند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که ای برتر از فکر و و هم و قیاس</p></div>
<div class="m2"><p>زما بنده گان بر تو زبید سپاس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو دادی مرا پایه ی ارجمند</p></div>
<div class="m2"><p>کشیدی زخاکم به چرخ بلند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به کوفه مرا مهتری از تو بود</p></div>
<div class="m2"><p>زگردنکشان برتری از تو بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو دادی مر این همه برگ و ساز</p></div>
<div class="m2"><p>تو کردی به هر کار دستم دراز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به نیکی تو این نام دادی مرا</p></div>
<div class="m2"><p>به دشمن کشی کام دادی مرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو کردی مرا اینچنین چیردست</p></div>
<div class="m2"><p>که هر چیردستم بدی زیر دست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس از کشتن دشمنان امام</p></div>
<div class="m2"><p>مرا جز شهادت نمانده است کام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بپیوندم اکنون به شاه شهید</p></div>
<div class="m2"><p>جگر بند و سبط رسول مجید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به مینو درون شادمان کن زمن</p></div>
<div class="m2"><p>علی (ع) و بتول و حسین(ع) وحسن (ع)</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ببخشا همه تیره گی های من</p></div>
<div class="m2"><p>همان برگنه خیره گی های من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو من بنده ی دوده ی حیدرم</p></div>
<div class="m2"><p>پرستنده ی آل پیغمبرم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدان دودمان و بدان شهریار</p></div>
<div class="m2"><p>ببخشا گناهم به روز شمار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درآن شب به یکتا خداوند فرد</p></div>
<div class="m2"><p>همی گفت از اینگونه با داغ و درد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چه گویم که مختار چون می گریست؟</p></div>
<div class="m2"><p>نه اشک روان بلکه خون می گریست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سپیده پس از عجز و راز و نیاز</p></div>
<div class="m2"><p>زبعد درود و سپاس و نماز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همانا به گوش دل او سروش</p></div>
<div class="m2"><p>بگفت از شه کربلا با خروش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که من در بهشت ای یل رزم خواه</p></div>
<div class="m2"><p>برای تو باشد دو چشمم به راه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>میان تو و من همین جان بود</p></div>
<div class="m2"><p>بده جان گرت میل جانان بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پس از جانستانی زبدخواه من</p></div>
<div class="m2"><p>گه جانفشانی است در راه من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو مختار سالار پیروز روز</p></div>
<div class="m2"><p>یل کشور افروز بد خواه سوز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدینسان شنید از خجسته سروش</p></div>
<div class="m2"><p>زکف داد جان و دل و صبر و هوش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به پیش خدا خاک بوسید و گفت:</p></div>
<div class="m2"><p>که اکنون گل آرزویم شکفت</p></div></div>