---
title: >-
    بخش ۵۳ - نامه نوشتن ابراهیم، به حنظله ابن عمار حاکم نصیبین و جواب او
---
# بخش ۵۳ - نامه نوشتن ابراهیم، به حنظله ابن عمار حاکم نصیبین و جواب او

<div class="b" id="bn1"><div class="m1"><p>نداریم با تو سر دشمنی</p></div>
<div class="m2"><p>نکو نیست با ما دژم، دل کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بده راه کز مرز تو بگذریم</p></div>
<div class="m2"><p>ره جنگ با دشمنان بسپریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوند از سپهبد ستد نامه تفت</p></div>
<div class="m2"><p>سبک سوی فرزند عمار رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسید از دگر سو بدان جایگاه</p></div>
<div class="m2"><p>سپاه عبیدالله دین تباه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مراو نیز با نامه زی حنظله</p></div>
<div class="m2"><p>پی تاختن کرد پیکی یله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبشته به نامه که اینک زراه</p></div>
<div class="m2"><p>برتو به مهمانی آمد سپاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکو ساز مهمانی ار ساختی</p></div>
<div class="m2"><p>برستی و گر بهر کین تاختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان برتو آید فرود از بلا</p></div>
<div class="m2"><p>که بر کشتگان در صف کربلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوتن پیک با هم فراز آمدند</p></div>
<div class="m2"><p>بر والی سرفراز آمدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدو بر بدادند با هم درود</p></div>
<div class="m2"><p>همی هر یکی نامه دادند زود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخواند آن دو را و بدانست راز</p></div>
<div class="m2"><p>برآشفت و دژخیم را خواند باز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بکشت آن زدین گشته بیگانه را</p></div>
<div class="m2"><p>که بودی نوند ابن مرجانه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به پیک براهیم بخشید زر</p></div>
<div class="m2"><p>بدو شادمان شد دل نامور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفتش که برگرد زیدر به راه</p></div>
<div class="m2"><p>بگو با سپهدار لشگر پناه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که من چاکری در رکاب توام</p></div>
<div class="m2"><p>به دل نیکخواه جناب توام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیا شهر و لشگر به فرمان تو راست</p></div>
<div class="m2"><p>دل من گروگان پیمان تو راست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو را در چنین رزم یاری کنم</p></div>
<div class="m2"><p>به راه وفا جانسپاری کنم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرستاده رفت و به سالار گفت</p></div>
<div class="m2"><p>زفرمانده آنرا که دید و شنفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>براهیم از آن کار خرسند شد</p></div>
<div class="m2"><p>شکفته زمرد خردمند شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از آن پس سوی شهر لشگر براند</p></div>
<div class="m2"><p>بجنباند رایت بنه برنشاند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پذیره شدش حنظله با سپاه</p></div>
<div class="m2"><p>بیاورد شادش به آرامگاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدانسان که بایست پوزش نمود</p></div>
<div class="m2"><p>همی مهر و مهمان نوازی نمود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به لشگر درخواسته برگشاد</p></div>
<div class="m2"><p>به هر کس سزا هر چه دانست داد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به سوی سپهبد فرستاد مال</p></div>
<div class="m2"><p>چنان چون سزا بودش آن بی همال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپهدار جنگی یکی روز و شب</p></div>
<div class="m2"><p>در آن مرز آسوده گشت ازتعب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دگر روز از آنجا چو بگرفت راه</p></div>
<div class="m2"><p>بشد حنظله پیشرو با سپاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به همره دو فرزند با شش هزار</p></div>
<div class="m2"><p>بد او را همه از درکارزار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپه ره چو پیمود فرسنگ چند</p></div>
<div class="m2"><p>به چشم اندر آمد حصاری بلند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدان نیز بد حنظله حکمران</p></div>
<div class="m2"><p>یکی نامجو کوتوال اندران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که اش نام مرد دردار بود</p></div>
<div class="m2"><p>که ستوار دژ را نگهدار بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو آمد سپه نزد آن در فرود</p></div>
<div class="m2"><p>سپهبد سراپرده بر پا نمود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نگهبان دژ خواند فرزند را</p></div>
<div class="m2"><p>چنین گفت پور خردمند را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که بشتاب ژرف اندرین کن نگاه</p></div>
<div class="m2"><p>ببین کز کجا می رسند این سپاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیامد جوان دید و دانست راز</p></div>
<div class="m2"><p>بدان تند بالا شد و گفت باز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شد از دژ نگهبان برون با شتاب</p></div>
<div class="m2"><p>سوی آن دو سالار فرهنگ باب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو آمد ببوسید روی زمین</p></div>
<div class="m2"><p>همی بر براهیم کرد آفرین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنین گفت: کآگه نبودم زکار</p></div>
<div class="m2"><p>که آید مراین لشگر نامدار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدی ابن مرجانه ی تیره تن</p></div>
<div class="m2"><p>در این دژ شب دوش مهمان من</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر این آمدن تان بدانستمی</p></div>
<div class="m2"><p>گرفتم مرا او را توانستمی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در این راه ای میر بیدار دل</p></div>
<div class="m2"><p>ز سیم و زر اوست خروار چل</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زن و کودکانش زدخت و پسر</p></div>
<div class="m2"><p>هم اکنون دراین باره هستند در</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>صد و بیست تن از غلام و کنیز</p></div>
<div class="m2"><p>در این دژ از آن بد گهر هست نیز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدو حنظله گفت: کای نامدار</p></div>
<div class="m2"><p>مر آن جمله را زی سپهبد بیار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بپذیرفت مرد و بیاورد زود</p></div>
<div class="m2"><p>زن و کودک و خواسته هر چه بود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پسر، دوبد، و زن سه، دختر چهار</p></div>
<div class="m2"><p>به جا مانده در باره زان نابکار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>براهیم فرخ به زخم درشت</p></div>
<div class="m2"><p>از آن دو پسر، مریکی را بکشت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بکشتند یاران مر آن جمله پاک</p></div>
<div class="m2"><p>بشستند از خونشان روی خاک</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سپهدار دست کرم برگشاد</p></div>
<div class="m2"><p>سپه را از آن سیم و زر بهره داد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سپیده چو سالار انجم سپاه</p></div>
<div class="m2"><p>به خاور زد از باختر بارگاه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>براهیم اشتر علم برکشید</p></div>
<div class="m2"><p>به رزم بداندیش لشگر کشید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زسوی دگر پور مرجانه نیز</p></div>
<div class="m2"><p>بیاورد لشگر که جوید ستیز</p></div></div>