---
title: >-
    بخش ۳ - در منقبت حضرت امیرالمومنین و اولاد طاهرین او علیه السلام گوید
---
# بخش ۳ - در منقبت حضرت امیرالمومنین و اولاد طاهرین او علیه السلام گوید

<div class="b" id="bn1"><div class="m1"><p>شه اوصیا داور اولیا</p></div>
<div class="m2"><p>زرویش عیان، فره ی کبریا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفات خدایی از او آشکار</p></div>
<div class="m2"><p>رخش مظهر جلوه ی کردگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی کاین خداوند دین را شناخت</p></div>
<div class="m2"><p>به خوبی، جهان آفرین را شناخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمهرش، دل هر که بی نور شد</p></div>
<div class="m2"><p>بدو دیو، نزدیک و حق دور شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اندیشه ذاتش بسی برتر است</p></div>
<div class="m2"><p>شناسای او، پاک پیغمبر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان سان که پیغمبر او را شناخت</p></div>
<div class="m2"><p>نبی را همان گونه حیدر شناخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرآن هر دو را نیز پروردگار</p></div>
<div class="m2"><p>شناسد چنان چون بود سر کار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درود فزون باد بر نور او</p></div>
<div class="m2"><p>بدان جفت و آن یازده پور او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که هستند مر خلق را راهبر</p></div>
<div class="m2"><p>به سوی خدا جد و پدر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من و مدح این پیشوایان دین</p></div>
<div class="m2"><p>تو بی باکی و خیره رایی ببین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پاکی خداوند گفته ثنای</p></div>
<div class="m2"><p>به فرقان از این دوده ی رهنمای</p></div></div>