---
title: >-
    بخش ۶۶ - سلاح نبرد خواستن امیر و خطاب او با جوشن
---
# بخش ۶۶ - سلاح نبرد خواستن امیر و خطاب او با جوشن

<div class="b" id="bn1"><div class="m1"><p>وزان پس به گنجور گفتا: برو</p></div>
<div class="m2"><p>بیاور همان دشنه ی سر درو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلیح و کمند و کمان مرا</p></div>
<div class="m2"><p>همان رمح چون پر غمان مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برفت و بیاورد گنجور او</p></div>
<div class="m2"><p>سلیح نبردش به دستور او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گنجور کرد آفرین و فره</p></div>
<div class="m2"><p>گرفت آن گریبان چینی زره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدو گفت: کای آهنین پیرهن</p></div>
<div class="m2"><p>که هستی تنم را به جای کفن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جانان من امروز جان می دهم</p></div>
<div class="m2"><p>زمن هر چه خواهد همان می دهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جانان چو من جان و سر باختم</p></div>
<div class="m2"><p>زگیتی به خلد برین تاختم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو خونین تنم را به غمخواره گی</p></div>
<div class="m2"><p>نگهبان شو از پویه ی باره گی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تنم را کفن شو به دستور من</p></div>
<div class="m2"><p>پس از مرگ من بر لب گور من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهر حلقه از خون روان رود کن</p></div>
<div class="m2"><p>مرا خاک بیجاده آلود کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفت این و چون شیر غژمان به خشم</p></div>
<div class="m2"><p>بپوشید آن جوشن تنگ چشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وزان پس به بنده زره زد گره</p></div>
<div class="m2"><p>بدان برز و بالا زره کرد زه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به عزم شهادت میان تنگ بست</p></div>
<div class="m2"><p>نه از بهر کوشیدن و جنگ بست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو هر چیز بودش همه ترک کرد</p></div>
<div class="m2"><p>سر آراسته زآهنین ترک کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حمایل سپس کرد تیغ از میان</p></div>
<div class="m2"><p>چو ماه نو از پیکر آسمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کمانی به بازو چو قوس سپهر</p></div>
<div class="m2"><p>به پشتش سپر چون درخشنده مهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زبند کمر ترکشی پر خدنگ</p></div>
<div class="m2"><p>فرو هشت مردانه از بهر جنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بزد بر کلاه خود آن بی نظیر</p></div>
<div class="m2"><p>به رسم بزرگان تازی سه تیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به هر تیر در، چار پر عقاب</p></div>
<div class="m2"><p>که بد اهرمن را فروزان شهاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان شد درآهن تنش ناپدید</p></div>
<div class="m2"><p>که بیننده ای جز دو چشمش ندید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو این کرد یاران خود را بخواند</p></div>
<div class="m2"><p>بدیشان ز هر در بسی راز راند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که هان ای دلیران بدارید گوش</p></div>
<div class="m2"><p>گمارید مغز و سپارید هوش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرا با شما آخرین گفتگو ست</p></div>
<div class="m2"><p>کزین گفته بشناسم از مغز پوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به پیش آمدستم یکی کارزار</p></div>
<div class="m2"><p>کزان گشت خواهد مرا کارزار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بزرگان شکستند پیمان من</p></div>
<div class="m2"><p>بگشتند یکسر زفرمان من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>براهیم دو راست از این پیشگاه</p></div>
<div class="m2"><p>به گرد دژ اندر فراوان سپاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سنان ها به رزم من افراخته</p></div>
<div class="m2"><p>پی جنگ من دشنه ها آخته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کمان ها بسی بهر قتلم به زه</p></div>
<div class="m2"><p>بسی مرد بنهفته تن در زره</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مگر نشنوید این غو کوس جنگ</p></div>
<div class="m2"><p>که پر گشته زان چرخ پیروزه رنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مگر این همه بر کشیده درفش</p></div>
<div class="m2"><p>کزان گشته گیتی کبود و بنفش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نبینید بر پا برای من است؟</p></div>
<div class="m2"><p>درفشان به گرد سرای من است؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکایک شما آگهید ای گروه</p></div>
<div class="m2"><p>که نایم من از رزم جستن ستوه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه هرگز دو دستم بماند زکار</p></div>
<div class="m2"><p>نه جویم به بیچاره گی زینهار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر سوزدم دل برای شما است</p></div>
<div class="m2"><p>هم ایدون کنید آنچه رای شماست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو برداشت سلطان خونین کفن</p></div>
<div class="m2"><p>زیاران خود بیعت خویشتن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بماندند آن ها که دین داشتند</p></div>
<div class="m2"><p>دو رویان از او روی برکاشتند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>من ایدون کنم شاه را پیروی</p></div>
<div class="m2"><p>به عزم درست و به رای قوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شما را رها کردم از عهد خویش</p></div>
<div class="m2"><p>بگیرید از این دژ کنون راه پیش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از ایدر به کاشانه ی خود روید</p></div>
<div class="m2"><p>زکین بد اندیش ایمن شوید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جهان آفرین باد همراهتان</p></div>
<div class="m2"><p>کناد ایمن از کین بدخواهتان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>من استاده ام پیش تیر بلا</p></div>
<div class="m2"><p>چو لب تشنه گان صف کربلا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سنان عدو را به جان می خرم</p></div>
<div class="m2"><p>به سر زخم گرز گران می خرم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر تیرم آید زبدخواه پیش</p></div>
<div class="m2"><p>دهم جای آن تیر، بر چشم خویش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مبیناد چشمم که پیچم عنان</p></div>
<div class="m2"><p>کنم پشت بر زخم تیر و سنان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گشایید اگر چشم دل روبروی</p></div>
<div class="m2"><p>شما راست خلد و جحیم از دو سوی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یکی پر ز حوران آراسته</p></div>
<div class="m2"><p>یکی پر شررهای برخاسته</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>درآن مومنان و در این کافران</p></div>
<div class="m2"><p>سزا دیده نیکان و بدگوهران</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سوی خود کشد هر یکی اهل خویش</p></div>
<div class="m2"><p>اگر خوب کارو اگر زشت کیش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گراهل بهشتید جان بسپرید</p></div>
<div class="m2"><p>وگر اهل دوزخ کنون جان برید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مرا یک تنه دادگر یار بس</p></div>
<div class="m2"><p>همان دست و تیغ تن آوبار بس</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شما گر نه اید مهان یار من</p></div>
<div class="m2"><p>شه کربلا بس مددکار من</p></div></div>