---
title: >-
    بخش ۲۷ - رسیدن ابراهیم به ساباط و خروج قتله ی امام شهید (ع) به مختار
---
# بخش ۲۷ - رسیدن ابراهیم به ساباط و خروج قتله ی امام شهید (ع) به مختار

<div class="b" id="bn1"><div class="m1"><p>بسی داستان ها زمختار راد</p></div>
<div class="m2"><p>زدند و زفرجام کردند یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفتند: کاین مرد شد چیردست</p></div>
<div class="m2"><p>بیفکند در چند لشگر شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی روز تا روز ستوارتر</p></div>
<div class="m2"><p>شود کار او بر فرازیش فر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چند با ما نکرده است بد</p></div>
<div class="m2"><p>یکی خواهش ما نکرده است رد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی بر فزاید به ما آبروی</p></div>
<div class="m2"><p>ز بگذشته چیزی نیارد به روی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود گرچه داد و دهش پیشه ای</p></div>
<div class="m2"><p>به ما نیست پوشیده اندیشه اش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل و جانش دربند مهر علی (ع) است</p></div>
<div class="m2"><p>نشستن ازو ایمن از غافلی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نجوید مگر مهر آن خاندان</p></div>
<div class="m2"><p>از اینجا تو اندیشه اش را بدان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که پیوسته گوید زکار حسین (ع)</p></div>
<div class="m2"><p>سرشکش روان گردد از هر دو عین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همی در پی وقت و روز مجال</p></div>
<div class="m2"><p>نشسته است و پیروز گشته به بال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>براهیم آن گرد پیروز جنگ</p></div>
<div class="m2"><p>ره پور مرجانه بگرفت تنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان دان که آن صفدر رزمخواه</p></div>
<div class="m2"><p>نیاید مگر با سپاهی ز راه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو شد پور مرجانه از وی زبون</p></div>
<div class="m2"><p>شود ایمن و سر نیارد برون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گشاید به بدخواه حیدر کمین</p></div>
<div class="m2"><p>نماند ز ما کس به روی زمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همان به که ما دست پیش افکنیم</p></div>
<div class="m2"><p>ز راه خود این سنگ را برکنیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه روز فرش براهیم بود</p></div>
<div class="m2"><p>اگر بود ما را ازو بیم بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کنون پور اشتر از او دور شد</p></div>
<div class="m2"><p>زسر پنجه ی مردی اش زور شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به درگه ندارد فراوان سپاه</p></div>
<div class="m2"><p>نیارد زیکتن شدن کینه خواه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان به، که ستوار پیمان کنیم</p></div>
<div class="m2"><p>مر او را برون سر ز فرمان کنیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نخواهیم بر خویش فرمان رواش</p></div>
<div class="m2"><p>همان کین پنهان نماییم فاش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپه بر گماریم گرد حصار</p></div>
<div class="m2"><p>ببندیم ره تنگ بر مرد کار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بکوشیم و او را به دست آوریم</p></div>
<div class="m2"><p>به کاخ و جودش شکست آوریم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نمانیم تا گردد این مور و مار</p></div>
<div class="m2"><p>شود چیره، وز ما برآرد دمار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چواین گفته شد پور اشعت بگفت:</p></div>
<div class="m2"><p>که از من سخن نیز باید شنفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر بشنوید از من این رای نیست</p></div>
<div class="m2"><p>به پیکار مختارتان پای نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ثقیفی نژاد است و مردی دلیر</p></div>
<div class="m2"><p>به خشم پلنگ است و نیروی شیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هنردارد و فرو تدبیر زور</p></div>
<div class="m2"><p>مدد بیند از بخت و از ماه و هور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مجویید بیهوده با او ستیز</p></div>
<div class="m2"><p>به خود چنگ او را مخواهید تیز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بخفته است این فتنه، از بهر چیست</p></div>
<div class="m2"><p>که بایدش بیدار کرد و گریست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گمانم که با ما نخواهد بدی</p></div>
<div class="m2"><p>نه از مهربانی که از بخردی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بود مملکت جوی و شاهی پژوه</p></div>
<div class="m2"><p>سپه خواهد و یارو پشت و گروه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زما تا ندیده به پیمان شکست</p></div>
<div class="m2"><p>نیارد به آزار ما هیچ دست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو بیند زما کوفیان خیره گی</p></div>
<div class="m2"><p>نماند که ما را بود چیره گی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بزرگست و خردش نباید شمرد</p></div>
<div class="m2"><p>نشاید نمودن به او دستبرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وگر ناگزیرید اندر خلاف</p></div>
<div class="m2"><p>مجویید پیکار او از گزاف</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فرستید مردی که گوید بدوی</p></div>
<div class="m2"><p>که ای پرفسون مرد پرخاشجوی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که در کوفه کردت امیر اینچنین</p></div>
<div class="m2"><p>که بگشود دستت به آورد و کین؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که داده تو را نام فرماندهی؟</p></div>
<div class="m2"><p>که با خویش خواهی زما همرهی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عراق است و مردان پر از نفاق</p></div>
<div class="m2"><p>نورزند با هیچ کس اتفاق</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز تو بهتران را بکشتند خوار</p></div>
<div class="m2"><p>از این دشمنان چشم یاری مدار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فرود آ، زکاخ و سر خویش گیر</p></div>
<div class="m2"><p>همان ره که بودت از این پیش گیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>میار از کله داری کوفه یاد</p></div>
<div class="m2"><p>که بهر کله می دهی سر، به باد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که پور زبیر است و برما امیر</p></div>
<div class="m2"><p>به پیمان اوییم برنا و پیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وگر بسته این آرزویت به دل</p></div>
<div class="m2"><p>دل از مهر آل علی (ع) درگسل</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به مصعب بپیوند و فرمان پذیر</p></div>
<div class="m2"><p>که ا بخشدت این کلاه و سریر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ببینید کز وی چه آید جواب</p></div>
<div class="m2"><p>پدید آید آنگاه راه صواب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اگر پاسخ از روی نرمی بداد</p></div>
<div class="m2"><p>بدانید بر بیم دارد نهاد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بترسد زبی پشتوانی خویش</p></div>
<div class="m2"><p>به رزمش توان پا نهادن به پیش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>وگر پاسخی گفت سخت و درشت</p></div>
<div class="m2"><p>نباید به پیکار او کرد پشت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بر این چون همه گرد کردند رای</p></div>
<div class="m2"><p>فرستاده شد نزد فرمانروای</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بگفت آنچه گفتند و آن سرفراز</p></div>
<div class="m2"><p>فرو شد به اندیشه لختی دراز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سپس سر برآورد و رخ پر زشرم</p></div>
<div class="m2"><p>بیاراست پاسخ به آوای نرم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که از من کدامین بد آمد پدید؟</p></div>
<div class="m2"><p>که اینگونه پاداش بایست دید؟</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ستم برشما کی روا داشتم؟</p></div>
<div class="m2"><p>کجا دل به آزار بگماشتم؟</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چه بد بر نکویان پسندیده ام؟</p></div>
<div class="m2"><p>چه بدعت که در دین روا دیده ام؟</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ببستید پیمان ابا من به مهر</p></div>
<div class="m2"><p>چه شد تا بشستید از شرم چهر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از آن دم که حیدر شه راست کار</p></div>
<div class="m2"><p>دراین مرز بدشاه و فرمانگزار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همی تاکنون به زمن مرزبان</p></div>
<div class="m2"><p>ندیدید دین پرور و مهربان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>برآوردم از هر دلی رنج ها</p></div>
<div class="m2"><p>فشاندم به راه شما گنج ها</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زسیم و زر و باره و درع و تیغ</p></div>
<div class="m2"><p>زکوفی سواران نکردم دریغ</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نه این است پاداش کردار من</p></div>
<div class="m2"><p>که با من کند دشمنی یار من</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>فرستاده رفت و سخن های میر</p></div>
<div class="m2"><p>بگفتا برآن گروه شریر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو زو پور اشعث شنید آن پیام</p></div>
<div class="m2"><p>بگفتا: که یاران جهان شد به کام</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بترسیده مختار و دل باخته است</p></div>
<div class="m2"><p>کزین سان فریبا سخن ساخته است</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بکوشید و از وی برآرید گرد</p></div>
<div class="m2"><p>نشاید به سستی دگر کارکرد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گمانم که از وی بگشته است بخت</p></div>
<div class="m2"><p>که بایست کندن زبن، این درخت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هیاهو بیفتاد در کوفیان</p></div>
<div class="m2"><p>ببستند پیکار و کین را میان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بر آن قوم شد پور اشعث امیر</p></div>
<div class="m2"><p>که از باب و جد بود شوم و شریر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زکوفی همین بوده تا بوده کار</p></div>
<div class="m2"><p>که نفرین به آن مردم نابکار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>شه مرز خاور چو روز دگر</p></div>
<div class="m2"><p>به دیبای زربفت آراست بر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سوی ابن اشعث گروه دو رنگ</p></div>
<div class="m2"><p>نهادند رو ساخته ساز جنگ</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به شهر ابن اشعث تکاور براند</p></div>
<div class="m2"><p>به هر راه بر راهداری نشاند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>که ناید ز مویی بر شهریار</p></div>
<div class="m2"><p>زشیعه سپاهی مدد کار یار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>خوارج به گرد اندرش زد رده</p></div>
<div class="m2"><p>بسان مغان کرد آتشکده</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چو آگاهی آمد به مختار راد</p></div>
<div class="m2"><p>به سوی براهیم یل نامه کرد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>که رادا، یلا، نامور سرورا</p></div>
<div class="m2"><p>مرا یاور و یار و غمگسترا</p></div></div>