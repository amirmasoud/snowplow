---
title: >-
    بخش ۸ - برگشتن بزرگان کوفه از مدینه
---
# بخش ۸ - برگشتن بزرگان کوفه از مدینه

<div class="b" id="bn1"><div class="m1"><p>بدو باز گفتند فرمان شاه</p></div>
<div class="m2"><p>شکفته رخ آمد یل رزمخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی انجمن چون یکی بوستان</p></div>
<div class="m2"><p>به کاخش بیاراست از دوستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلام شه دین بدیشان رساند</p></div>
<div class="m2"><p>همه آنچه فرموده بد، باز راند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پذیرایی فرمان شدند آن سران</p></div>
<div class="m2"><p>ببستند پیمان کران تا کران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتند شیر دژ آهنگ را</p></div>
<div class="m2"><p>که ای آستین بر زده جنگ را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در این مرز بسیار نام آورند</p></div>
<div class="m2"><p>که فرمانده ی کوفه را یاورند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه تیغ بازند و خنجر گزار</p></div>
<div class="m2"><p>همه شیر گیر و نبرده سوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپاه تو کم لشگر کین فزون</p></div>
<div class="m2"><p>تو خود گو نبرد آزماییم، چون؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو را باید اندر سپه چون خودی</p></div>
<div class="m2"><p>دلیری و مرد افکن اسپهبدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>براهیم مالک دلیری گوست</p></div>
<div class="m2"><p>که شیرخدا را به جان پیرو است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سواری است اشتر نژاد و سترگ</p></div>
<div class="m2"><p>که درتازیان نیست چون او بزرگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>علی(ع) را یکی دست پرورده است</p></div>
<div class="m2"><p>بسی کارکین برسر آورده است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر او را سوی خویشتن خوانیا</p></div>
<div class="m2"><p>به یاری از او دست بستانیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امیری چو تو نامدار و هژیر</p></div>
<div class="m2"><p>ز چونین سپهبد بود ناگریز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پسندید گفتارشان، میرراد</p></div>
<div class="m2"><p>از آن رای فرزانه گردید شاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرستاد از انجمن چند تن</p></div>
<div class="m2"><p>به آوردن مرد شمشیر زن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برفتند و با پر دل سرفراز</p></div>
<div class="m2"><p>همه گفتنی ها بگفتند باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یل اشتری گوهر این چون شنفت</p></div>
<div class="m2"><p>به پاسخ فرستاده گان را بگفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که بندم کمر بهر این سخت کار</p></div>
<div class="m2"><p>چو باشم در این کار فرمانگزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل و زور و رای امیری مراست</p></div>
<div class="m2"><p>همان نیرو شیر گیری مراست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به جایی که من باشم اندرجهان</p></div>
<div class="m2"><p>روا نیست فرماندهی برمهان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگفتند: این جز گفته جز راست نیست</p></div>
<div class="m2"><p>تو را گر بگویند همتاست، نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو داری دل و رای اسپهبدی</p></div>
<div class="m2"><p>توانی ز ما دور کردن بدی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنینی و از این هم افزونتری</p></div>
<div class="m2"><p>که پرورده ی دست حق حیدری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ولی گشته مختار فرمانروا</p></div>
<div class="m2"><p>به فرمان پور شه نینوا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خود از چارمین شد به یثرب درا</p></div>
<div class="m2"><p>شنیدیم کو هست سر لشگرا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز حیدر بود این حدیث شریف</p></div>
<div class="m2"><p>که خونخواه ما هست مرد ثقیف</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو نیز آی و سالار لشگرش باش</p></div>
<div class="m2"><p>به فرماندهی یار و همسرش باش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نپذرفت مرد افکن شیر گیر</p></div>
<div class="m2"><p>که مختار باشد مر او را، امیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدو مهتران را فرستاد، باز</p></div>
<div class="m2"><p>چو مختار گردید آگه ز راز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شد از گفتگو بسته دم تا سه روز</p></div>
<div class="m2"><p>چهارم برآمد چو گیتی فروز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز کاشانه اش راه بگرفت پیش</p></div>
<div class="m2"><p>ز یاران بسی برد همراه خویش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به کاخ براهیم مالک شتافت</p></div>
<div class="m2"><p>چو مهمان نو، میزبان باز یافت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بزرگانه پذرفت و بنواختش</p></div>
<div class="m2"><p>به یک جای با خویش بنشاختش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدو داد مختار فرخ، درود</p></div>
<div class="m2"><p>یکی نامور نامه او را نمود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که زی من زیثرب فرستاده این</p></div>
<div class="m2"><p>محمد گزین پور ضرغام دین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در این نامور نامه و عهد نو</p></div>
<div class="m2"><p>مرا خوانده بر پیروان پیشرو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو را گفته تا حقگزاری کنی</p></div>
<div class="m2"><p>مرا در چنین کار، یاری کنی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>براهیم چون نامه را برگشاد</p></div>
<div class="m2"><p>بخواند و ببوسید و برسر نهاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زجایی که بودش فرو برنشست</p></div>
<div class="m2"><p>به پیمان مختار بگشود دست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خود و هرکس از یاورانش که بود</p></div>
<div class="m2"><p>به جانبازی آن روز بیعت نمود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بزرگان که آنجا فراهم بدند</p></div>
<div class="m2"><p>از آن کار خندان و خرم شدند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دو فرخ سپهبد دو نیکو گهر</p></div>
<div class="m2"><p>چو بستند فرماندهی را کمر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به کوفه درون هرتن از شیعیان</p></div>
<div class="m2"><p>ببست آن دو را بهر یاری میان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو مختار شد کارش آراسته</p></div>
<div class="m2"><p>ز اسپهبد و لشگر و خواسته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همی با سپهبد شبان روز چند</p></div>
<div class="m2"><p>نشست وز هر در سخن درفکند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به فرجام، رای دو جنگی سوار</p></div>
<div class="m2"><p>بدینگونه در کار گشت استوار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که گردد چو دو هفت روز اسپری</p></div>
<div class="m2"><p>ز دویم جمادی به فرخ فری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به شصت و سه از هجرت احمدی</p></div>
<div class="m2"><p>که بادش درود از خدا سرمدی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پی کینه جویی مهیا شوند</p></div>
<div class="m2"><p>به آسودگی لحظه ای نغنوند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یکی مرد نستوده نامش ایاس</p></div>
<div class="m2"><p>که شب کوفه را او همی داشت پاس</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو آگاهی از کار مختار جست</p></div>
<div class="m2"><p>که بگرفته بیعت ز مردم درست</p></div></div>