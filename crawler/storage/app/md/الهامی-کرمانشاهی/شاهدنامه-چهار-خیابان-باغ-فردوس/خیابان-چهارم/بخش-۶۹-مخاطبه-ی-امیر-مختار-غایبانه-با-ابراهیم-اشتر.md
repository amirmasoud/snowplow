---
title: >-
    بخش ۶۹ - مخاطبه ی امیر مختار غایبانه با ابراهیم اشتر
---
# بخش ۶۹ - مخاطبه ی امیر مختار غایبانه با ابراهیم اشتر

<div class="b" id="bn1"><div class="m1"><p>صبا هدهدم من سلیمان تویی</p></div>
<div class="m2"><p>سزد گر پیامم از او بشنوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازو نامه گیری و بر خوانی اش</p></div>
<div class="m2"><p>نهی بر سرو روی و پیشانی اش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درآن نامه بنوشته بین که من</p></div>
<div class="m2"><p>شدم برخی شاه خونین کفن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن از نیزه و خنجرم چاک شد</p></div>
<div class="m2"><p>کله تیغ و اورنگ من خاک شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن دل که مهر تو بد جای گیر</p></div>
<div class="m2"><p>دو صد رخنه افکند پیکان و تیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو مانی که من نیز راهی شدم</p></div>
<div class="m2"><p>بدانجا که دانی و خواهی شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شاه شهیدان سلامت برم</p></div>
<div class="m2"><p>درودت رسانم پیامت برم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان بین من بی رخت کور باد</p></div>
<div class="m2"><p>مرا یاد تو مونس گور باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر جا که باشم تو یار منی</p></div>
<div class="m2"><p>شب مرگ، شمع مزار منی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قدم بی تو ننهم به خلد برین</p></div>
<div class="m2"><p>نبینم به رخساره ی حورعین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس از مرگ ای سرو چالاک من</p></div>
<div class="m2"><p>خرامی اگر بر سر خاک من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به رخشنده جانم درودی فرست</p></div>
<div class="m2"><p>زسرمایه ی خویش سودی فرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به آمرزشی مر مرا یاد کن</p></div>
<div class="m2"><p>به مینو روان مرا شاد کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو را چون به مینو درآیی فرود</p></div>
<div class="m2"><p>به پاداش من هم فرستم درود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگفت این و تا تاب پیکار داشت</p></div>
<div class="m2"><p>همی تیغ و بازوی مردی فراشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بگسست ازوتاب، بی توش گشت</p></div>
<div class="m2"><p>به دیوار زد تکیه بیهوش گشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو باز آمدش هوش برداشت سر</p></div>
<div class="m2"><p>همی خواست گردد به کین حمله ور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برادر دو پتیاره از آن فریق</p></div>
<div class="m2"><p>یکی بود طارق دگر یک طریق</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زهر سو یکی سوی او تاختند</p></div>
<div class="m2"><p>به تیغ ستم کار او ساختند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیارست دیگر نبرد آزمود</p></div>
<div class="m2"><p>بیفتاد و رخساره برخاک سود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به تن درش جز نیمه جانی نماند</p></div>
<div class="m2"><p>وزان فر و نیرو نشانی نماند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی باز شد بال بگسیخته</p></div>
<div class="m2"><p>پر افتاده چنگال او ریخته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی شیر شد کنده دندان او</p></div>
<div class="m2"><p>دلیری جهان تنگ زندان او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی تیغ گردید، بشکسته، خرد</p></div>
<div class="m2"><p>یکی مرد افتاده از دستبرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکی باره گردید با خاک پست</p></div>
<div class="m2"><p>یکی باده خوار از می مرگ مست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یک نام بردار گشته زبون</p></div>
<div class="m2"><p>بدش بالش از خاک و بستر ز خون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو وی را به سرمرگ بگذاشت گام</p></div>
<div class="m2"><p>به یاد آمدش از شه تشنه کام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ببارید خون از مژه لخت لخت</p></div>
<div class="m2"><p>بنالید زار و بمویید سخت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که ای کشته ی خنجر شمر دون</p></div>
<div class="m2"><p>سرت برسنان، تن به دریای خون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ایا خسرو بی درفش و سپاه</p></div>
<div class="m2"><p>درفشت نگون و سپاهت تباه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تویی کت سرا پرده تاراج شد</p></div>
<div class="m2"><p>سنان سنان را سرت، تاج شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تویی کامدت پایمال ستور</p></div>
<div class="m2"><p>تن پاک و سر هشته شد در تنور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خدا گشت خون تو را مشتری</p></div>
<div class="m2"><p>هم انگشت رفت و هم انگشتری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کتاب خدا از تو شیرازه یافت</p></div>
<div class="m2"><p>زخونت رخ عرش حق غازه یافت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من اینک فدای تو جان می کنم</p></div>
<div class="m2"><p>برای تو ترک جهان می کنم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ره آورد جانت که جان ها فداش</p></div>
<div class="m2"><p>دهم جان و صد جان ستانم بهاش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرا جان تویی بلکه جانان تویی</p></div>
<div class="m2"><p>هران چه ام نگنجد به وهم آن تویی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرا با ولایت چو یزدان سرشت</p></div>
<div class="m2"><p>نترسم زدوزخ، نخواهم بهشت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بیا تا دم واپسین بنگرم</p></div>
<div class="m2"><p>به روی تو و نقد جان بسپرم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به پای تو سرسایم و سردهم</p></div>
<div class="m2"><p>زدست نبی برسر افسر نهم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در آن دم زمینو چمان شد سروش</p></div>
<div class="m2"><p>رسانید پیغام شاهش به گوش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که ای عاشق من بیا سوی من</p></div>
<div class="m2"><p>به کام دل خود ببین روی من</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تویی ذره و آفتابت منم</p></div>
<div class="m2"><p>تو عطشان، گوارنده آبت منم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بیا تا تو را آنچه دانی دهم</p></div>
<div class="m2"><p>به جان زنده گی جاودانی دهم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بیا تو به سر گل برافشانمت</p></div>
<div class="m2"><p>به گلزار فردوس بنشانمت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>صف بار پیغمبر آراسته است</p></div>
<div class="m2"><p>به مینو ترا سوی خود خواسته است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بیا تا تو را جامه ی شاهوار</p></div>
<div class="m2"><p>کند زیور پیکر نامدار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گشاید علی (ع) پرده از روی خویش</p></div>
<div class="m2"><p>نماید ترا طاق ابروی خویش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به دست اندرش ساتکینی زنور</p></div>
<div class="m2"><p>زفیض خدا پر شراب طهور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بیا و بخور تا که مستت کند</p></div>
<div class="m2"><p>کند نیست و آنگاه هستت کند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سرافراز مختار فرخ نهاد</p></div>
<div class="m2"><p>بهای چنین مژده را جان بداد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رخ مرتضی دید و چشم از جهان</p></div>
<div class="m2"><p>بپوشید و سر داد و بسپرد جان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سر جنگجوی از همایون تنش</p></div>
<div class="m2"><p>بریدند و بردند زی دشمنش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فراوان دروناز جهان آفرین</p></div>
<div class="m2"><p>بدو باد از پیشوایان دین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دل مرتضی از غم آزاد ازو</p></div>
<div class="m2"><p>شهنشاه خونین کفن شاد ازو</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>درود از همه شیعیانش رساد</p></div>
<div class="m2"><p>به بنگاه قدسش روان باد شاد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مبراد از دامنش دست ما</p></div>
<div class="m2"><p>زمهرش دل مهرت پیوست ما</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پذیرفته گر نزد یزدان شویم</p></div>
<div class="m2"><p>به جایی که او رفت ما هم رویم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زآلایش آنگه که کردیم پاک</p></div>
<div class="m2"><p>بپوییم زی آن خور تابناک</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ورا گفت فرزانه ی هوشیار</p></div>
<div class="m2"><p>پرستشگه کوفه باشد مزار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>میان دو محراب شیر خدای</p></div>
<div class="m2"><p>بود خوابگاه یل پاکرای</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خدایا بدان خوابگاهم رسان</p></div>
<div class="m2"><p>وزانجا به درگاه شاهم رسان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چه شاهی به خاک درش درغری</p></div>
<div class="m2"><p>پی سجده پشت شهان چنبری</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدانجا فرود آور ای داورم</p></div>
<div class="m2"><p>بکن خاک درگاه او پیکرم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همه دوستان را بدین آرزوی</p></div>
<div class="m2"><p>رسان و بدان خدا بخش آبروی</p></div></div>