---
title: >-
    بخش ۱۶ - نامه نوشتن ابراهیم به مصعب
---
# بخش ۱۶ - نامه نوشتن ابراهیم به مصعب

<div class="b" id="bn1"><div class="m1"><p>سرنامه را حمد یزدان نوشت</p></div>
<div class="m2"><p>خدای دوکیهان و خرم بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپس نعت پیغمبر و آل او</p></div>
<div class="m2"><p>هم آن یاوران نکو فال او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس آنگه نوشت ای سلیل زبیر</p></div>
<div class="m2"><p>که داری به دل کینه ی اهل خیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب دوش من درکنار فرات</p></div>
<div class="m2"><p>به ظلمت درون همچو آب حیات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهادم کمین تا بریزمت خون</p></div>
<div class="m2"><p>نبد دست چون بود زورت فزون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتادم به دام تو شوریده بخت</p></div>
<div class="m2"><p>نهادی به کوپال من بند سخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبند تو تا من نبینم گزند</p></div>
<div class="m2"><p>رهانید شیر خدایم زبند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بکشتم بسی از سپاهت به تیغ</p></div>
<div class="m2"><p>تو خود جستی از چنگ من ای دریغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکردم من این ها به نیروی خویش</p></div>
<div class="m2"><p>ننازم به شمشیر و بازوی خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خداوند کیهان مرا گشت یار</p></div>
<div class="m2"><p>که بگسستم آن بند آهن چو، تار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان آفرین بس بود یار من</p></div>
<div class="m2"><p>تو دیدی که چونست کردار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر خواهی از تیغ من جای بری</p></div>
<div class="m2"><p>نباید که باشی زایمان، بری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوی مرز خود رو، مبر عرض خویش</p></div>
<div class="m2"><p>بران دشمنان علی را ز پیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وگرنه چو انگیخت گرد نبرد</p></div>
<div class="m2"><p>پدید آید از مرد در جنگ، مرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نمایم جهان برتو زانسان سیاه</p></div>
<div class="m2"><p>که دیگر نیابی سوی بصره راه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برون آورم با پرند آورت</p></div>
<div class="m2"><p>هوای امیری ز، خیره سرت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به عبداله آنگونه رانم سنان</p></div>
<div class="m2"><p>که دیگر نیابد دو دستش عنان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به افزونی لشگر خود مناز</p></div>
<div class="m2"><p>که با من چو گنجشک باشند و باز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو آمد به بن نامه ی پر زداد</p></div>
<div class="m2"><p>بپیچید و بر سرش خاتم نهاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به دست فرستاده ی هوشمند</p></div>
<div class="m2"><p>فرستاد زی مصعب خود پسند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو آمد به وی نامه ی سرفراز</p></div>
<div class="m2"><p>بخواند و ز پاسخ فروماند باز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از ان نامور نامه شد پر هراس</p></div>
<div class="m2"><p>برآورد سر پس چو گاو خراس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرستاده را گفت: نام تو چیست؟</p></div>
<div class="m2"><p>به دین پیشوا و امام تو کیست؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که را دانی اندر جهان شاه خود؟</p></div>
<div class="m2"><p>که را دشمن دین و بد خواه خود؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگفتا: که حارث مرا هست نام</p></div>
<div class="m2"><p>علی باشدم راهنما و امام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سپهبد براهیم را دوستم</p></div>
<div class="m2"><p>ترا دشمنم گر کنی پوستم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگفتا بدو مصعب بد گمان:</p></div>
<div class="m2"><p>که گر خواهی از مرگ یابی امان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>براهیم و مختار را بد شمار</p></div>
<div class="m2"><p>که بخشم تو را خاتم زینهار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وگرنه به کوپال زانسان سرت</p></div>
<div class="m2"><p>بکوبم که گرید به تو همسرت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فرستاده گفتا: بگو با سپاه</p></div>
<div class="m2"><p>بیایند یکسر در این پیشگاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که تا من برآیم به جایی بلند</p></div>
<div class="m2"><p>بگویم همان را که داری پسند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به لشگر بداندیش فرمان بداد</p></div>
<div class="m2"><p>سپاه آمد و جابه جا ایستاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جوانمرد جا بربلندی گرفت</p></div>
<div class="m2"><p>بلندی ازو سربلندی گرفت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زبان را به حمد جهان آفرین</p></div>
<div class="m2"><p>گشود و نبی را بخواهد آفرین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به شیر خدا گفت آنگه درود</p></div>
<div class="m2"><p>دو نوباوه اش را به پاکی سرود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به نیکی براهیم و مختار را</p></div>
<div class="m2"><p>ستود و دگر کرد گفتار را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بگفتا: که نفرین به پور زبیر</p></div>
<div class="m2"><p>کز او نامد اندر جهان هیچ خیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پلید است و بی دین و بیدادگر</p></div>
<div class="m2"><p>برادر چو وی چون دو پورش، پدر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شنید این چو مصعب زبگزیده مرد</p></div>
<div class="m2"><p>بپیچید برخویش ازخشم و درد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برآورد تیغ جفا از نیام</p></div>
<div class="m2"><p>بزد چاک بر پهلوی نیکنام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شد آن بی گنه از بلندی نگون</p></div>
<div class="m2"><p>تن پاکش از خون همه لعلگون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>روان را به شاه شهیدان سپرد</p></div>
<div class="m2"><p>زگیتی همی نام نیکو ببرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به جز مصعب ازکین به زخم درشت</p></div>
<div class="m2"><p>فرستاده را در جهان کس نکشت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>براهیم را این چو آمد به گوش</p></div>
<div class="m2"><p>بگریید از درد و برزد خروش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بگفتا: بکویید کوس نبرد</p></div>
<div class="m2"><p>برانید لشگر ز جا همچو گرد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بغرید کوس و سپه فوج فوج</p></div>
<div class="m2"><p>چو دریای قلزم درآمد به موج</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گروهی چو کوه اندر آمد ز جای</p></div>
<div class="m2"><p>همه غرق آهن زسر تا به پای</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شد از ناله پر، هفتمین آسمان</p></div>
<div class="m2"><p>بپیچید برخود زمین و زمان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو گفتی که خون شهیدان پاک</p></div>
<div class="m2"><p>بجوشید از تربت تابناک</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زره پوش کند آوران خیل خیل</p></div>
<div class="m2"><p>برفتند چون از برکوه، سیل</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو بر زین برآمد سوار سپهر</p></div>
<div class="m2"><p>سپاه ستاره، بپوشید چهر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زنیزه، هوا بیشه ی شیر شد</p></div>
<div class="m2"><p>جهان سربه سر پر ز شمشیر شد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بغرید چون رعد رویینه خم</p></div>
<div class="m2"><p>زمین در پی بادپا گشت کم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو مصعب سرترک لشگر بدید</p></div>
<div class="m2"><p>سراسیمه لشگر به هامون کشید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نکرده سپه را به صف استوار</p></div>
<div class="m2"><p>که برلشگرش زد یکی نامدار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو تندر خروشان و چون پیل مست</p></div>
<div class="m2"><p>به زیرش یکی باد و برقی به دست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>صف میمنه راند بر میسره</p></div>
<div class="m2"><p>چو شد میمنه و میسره یکسره</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به قلب سپه تاخت چون برق و باد</p></div>
<div class="m2"><p>زمرد افکنی، داد مردی بداد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو انگیخت محشر در آن رزمگاه</p></div>
<div class="m2"><p>فرو ریخت بسیار خون، زان سپاه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بیامد سوی لشگر خویشتن</p></div>
<div class="m2"><p>بر او آفرین خوان، همه انجمن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو مصعب زوی آن دلیری بدید</p></div>
<div class="m2"><p>رخش گشت از بیم چون شنبلید</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همی گفت: کاین لجه ی نیل بود</p></div>
<div class="m2"><p>و یا شیرکوشنده یا پیل بود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>اگر بار دیگر نبرد آورد</p></div>
<div class="m2"><p>سر این سپه زیر گرد آورد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>که بود این دلاور که چون تند باد</p></div>
<div class="m2"><p>به ما تاخت وین داغ بر مانهاد؟</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بگفتند: کاین شیر بی ترس و بیم</p></div>
<div class="m2"><p>که تیغش بود همچو مار کیلم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سپهبد براهیم نامش بود</p></div>
<div class="m2"><p>که جنگ فلک در نگاهش بود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سپس گفت مصعب به یاران خویش</p></div>
<div class="m2"><p>که ما را رهی صعب آمد به پیش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شما گر بدینگونه جنگ آورید</p></div>
<div class="m2"><p>همه نام نیکو به ننگ آورید</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ندیدید با ما چه کرد این سوار؟</p></div>
<div class="m2"><p>زوی باید آموختن کارزار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گروهی ابا تیغ و گرز و کمند</p></div>
<div class="m2"><p>گروهی ابا نیزه های بلند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گروهی به قاروره، قومی به تیر</p></div>
<div class="m2"><p>درآیید در پهنه ی داروگیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بکوشید با این یل کینه جو</p></div>
<div class="m2"><p>مگر آب رفته در آید به جو</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>زگفتار او لشگر آمد به جوش</p></div>
<div class="m2"><p>کشیدند از نای رزمی خروش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هزاری شش از آن سپاه گران</p></div>
<div class="m2"><p>پیاده گرفتند تیر و کمان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز مردان کاری هزاری چهار</p></div>
<div class="m2"><p>گرفتند زوبین پی کارزار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هزاری دو قاروره ده و دو هزار</p></div>
<div class="m2"><p>زره پوش و بر باد پایان سوار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دو بیور هزار از سپه تیغ زن</p></div>
<div class="m2"><p>به پیکارشان زال زر همچو زن</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گروهی سپردار و خنجر گزار</p></div>
<div class="m2"><p>که بودند اندر شمر، ده هزار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به یک ره به میدان نهادند روی</p></div>
<div class="m2"><p>به قلب اندرون، مصعب کینه جوی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو این دید اشتر نژاد دلیر</p></div>
<div class="m2"><p>که شد پهنه زان پر دلان غاب شیر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>خروشید:کای یاوران رسول (ص)</p></div>
<div class="m2"><p>که هستید خونخواه آل بتول (ع)</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>یک امروز خود را کنید آزمون</p></div>
<div class="m2"><p>به مردی ابا این سپاه فزون</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>اگر کشته گشتید خرم بهشت</p></div>
<div class="m2"><p>شما را بود از خدا، سرنوشت</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>وگر زنده ماندید دور ازگزند</p></div>
<div class="m2"><p>شود نامتان چون فلک سربلند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ازین به چه باشد که در راه دین؟</p></div>
<div class="m2"><p>سپاریم ما جان به جان آفرین</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بخوانند ما را به روز جزا</p></div>
<div class="m2"><p>ز یاران نوباوه ی مرتضا</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به نیرو اگر چون شما شیر نیست</p></div>
<div class="m2"><p>زعباس به دست و شمشیر نیست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>نباشد بهتر به روی و به موی</p></div>
<div class="m2"><p>ز شبه پیغمبر، شه ماهروی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به یاد آورید آن ستم ها زشت</p></div>
<div class="m2"><p>که کردند با شاه اهل بهشت</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>وزان دختران رسول انام</p></div>
<div class="m2"><p>که بردند بی پرده تا شهر شام</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بجویید آن خون که پروردگار</p></div>
<div class="m2"><p>بود خونبهایش به روز شمار</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بکوشید در یاری بوتراب</p></div>
<div class="m2"><p>که ما را ظفر وعده داد آن جناب</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بگفت این و غرید آن کامگار</p></div>
<div class="m2"><p>چو شیر گرسنه که بیند شکار</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>سوی رزم دشمن برانگیخت اسب</p></div>
<div class="m2"><p>پرندی به دستش چو آذرگشست</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>پس و پشت او لشگری همگروه</p></div>
<div class="m2"><p>چو جوشنده دریا و چون سخت کوه</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>سنان راست کردند و تیغ آختند</p></div>
<div class="m2"><p>به دشمن شکاری هیون تاختند</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>به ترغیب مردان خروشید کوس</p></div>
<div class="m2"><p>زمین بر رخ آسمان داد بوس</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>شد از دود قارو ره، گیتی سیاه</p></div>
<div class="m2"><p>بپوشید گرد زمین روی ماه</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چو ماران، سنان مغز مردان بخورد</p></div>
<div class="m2"><p>تکاور به نعل، استخوان کرد خرد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>شد از باده ی خون سرخاک، مست</p></div>
<div class="m2"><p>خدنگ ابرسنان برهوا کله بست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چو دریای چین گشت میدان جنگ</p></div>
<div class="m2"><p>براهیم یل اندر آن، چون نهنگ</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چپ و راست درتاختی همچو برق</p></div>
<div class="m2"><p>به خون ساختی کشتی عمر، غرق</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>زبس کشته افکند بر روی هم</p></div>
<div class="m2"><p>اجل گشت از یاری وی دژم</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>تن کشته گان سپاه غرور</p></div>
<div class="m2"><p>به یاران دین بست راه عبور</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>همی تا نهان گشت شمشیر مهر</p></div>
<div class="m2"><p>به مشگین نیام سوار سپهر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>دولشگر به هم تیغ کین راندند</p></div>
<div class="m2"><p>همی خون به خاک اندر افشاندند</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو شب گشت و رفتند هر دو سپاه</p></div>
<div class="m2"><p>از آوردگه سوی آرامگاه</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>همی چشم مردان با یال و سفت</p></div>
<div class="m2"><p>زاندیشه ی روز دیگر نخفت</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>برآمد چو از خیمه ی آبنوس</p></div>
<div class="m2"><p>سوار فلک، نعره برداشت کوس</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>سپه از دو رویه به دشت نبرد</p></div>
<div class="m2"><p>رده برکشیدند و برخاست کرد</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>دولشگر چو در پهنه کردند جای</p></div>
<div class="m2"><p>براهیم با نیزه ی جانگزای</p></div></div>