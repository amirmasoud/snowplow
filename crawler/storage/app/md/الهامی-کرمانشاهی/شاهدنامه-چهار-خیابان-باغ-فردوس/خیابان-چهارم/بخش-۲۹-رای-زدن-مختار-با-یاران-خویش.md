---
title: >-
    بخش ۲۹ - رای زدن مختار با یاران خویش
---
# بخش ۲۹ - رای زدن مختار با یاران خویش

<div class="b" id="bn1"><div class="m1"><p>چو نامه به دست سپهبد رسید</p></div>
<div class="m2"><p>ز غیرت، روان درتنش بردمید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزد کوس و برشد به زین بی شکیب</p></div>
<div class="m2"><p>ندانست در ره فراز از نشیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی کوفه آمد دمان با سپاه</p></div>
<div class="m2"><p>وز آن سوی مختار ناورد خواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه یاوران را برخویش خواند</p></div>
<div class="m2"><p>سخن ها زکردار دشمن براند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتا: مرا راستی پیشه است</p></div>
<div class="m2"><p>ز ید رایی و کژی اندیشه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود از شما هر که با کوفیان</p></div>
<div class="m2"><p>بدان سو رود تا ندیده زیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا آنچه ماند زمردان بس است</p></div>
<div class="m2"><p>که یک تن موافق به از صد کس است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر یک تنه نیز مانم چه باک</p></div>
<div class="m2"><p>که از مرگ نبود به دل ترسناک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من آنم که تیغ مرا مرتضی</p></div>
<div class="m2"><p>به فرق عدو خواند تیغ قضا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلای تن بد سگالان منم</p></div>
<div class="m2"><p>جهان باش گو سر به سر دشمنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپاه و سپهدار اگر نیستم</p></div>
<div class="m2"><p>نه آنم که از کار خود بیستم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا از شه بی سپه پیروی است</p></div>
<div class="m2"><p>که زد یک تنه با هزاری دویست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهدار او را فکندند دست</p></div>
<div class="m2"><p>نیامد به عزم درستش شکست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>علم سرنگون بود و لشگر تباه</p></div>
<div class="m2"><p>همی سخت تر بود در رزم، شاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هراسش نبود آن شه از صد هزار</p></div>
<div class="m2"><p>مرا بیم از اندک سپه؟ زینهار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر گشته گردم به راه حسین (ع)</p></div>
<div class="m2"><p>روم یکسره در پناه حسین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو یاران شنیدند گفتار میر</p></div>
<div class="m2"><p>هم آواز و یکدل زبرنا و پیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفتند: میرا، سرا، صفدرا</p></div>
<div class="m2"><p>هوادار فرزند پیغمبرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زجان جملگی دوستدار توایم</p></div>
<div class="m2"><p>پرستنده و پاسدار توایم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بکوشیم و پیش تو بازیم جان</p></div>
<div class="m2"><p>وزاین کار جوییم از حق جنان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهان بی تو ای میر هرگز مباد</p></div>
<div class="m2"><p>به ما مرگ پیش از شکستت رساد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگو تا نوازند کوس نبرد</p></div>
<div class="m2"><p>زبی یاوری چهره منمای زرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زانبوه دشمن مدار ایج باک</p></div>
<div class="m2"><p>که باشد تو را یار، جان های پاک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو خونخواه فرزند پیغمبری</p></div>
<div class="m2"><p>کند داد یزدان تو را یاوری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به کام تو گردد سپهر بلند</p></div>
<div class="m2"><p>ز دشمن تو را هیچ ناید گزند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هم امروز و فردا ببینی سپاه</p></div>
<div class="m2"><p>رسد با براهیم اشتر زراه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شوند این گروه ستمگر زبون</p></div>
<div class="m2"><p>روان گردد از نایشان، جوی خون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه وعده ی حیدر آید درست</p></div>
<div class="m2"><p>در ایمان نباشم ای میر، سست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زگفتارشان میر گردید شاد</p></div>
<div class="m2"><p>همه بیم و اندیشه اش شد زیاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدان رای پاکیزه بستودشان</p></div>
<div class="m2"><p>سلیح آنچه بایست بخشودشان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سپس گفت: تا بر دمیدند نای</p></div>
<div class="m2"><p>برآمد به پیکار دشمن ز جای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بپوشید تن را به رومی زره</p></div>
<div class="m2"><p>هر آنکس که دیدش چنان،گفت زه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو گفتی بود شرزه شیر آن دلیر</p></div>
<div class="m2"><p>اگر هیچ پوشد زره شرزه شیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو ابروی خوبان به فرمان درش</p></div>
<div class="m2"><p>کمانی چو نر اژدها هرسرش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که بودی خدنگی کزو شد رها</p></div>
<div class="m2"><p>بلای تن شیر و نر اژدها</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به دوش اندرش اسپری چون سپهر</p></div>
<div class="m2"><p>که از قبه اش خیره بد ماه و مهر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به اسبی برآمد قوی ساق و سم</p></div>
<div class="m2"><p>بپوشیده ز آهن ز سر تا به دم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برآهیخت تیغ و برانگیخت اسب</p></div>
<div class="m2"><p>سوی کوفیان همچو آذر گشسب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز بس پشت او یاوران تاختند</p></div>
<div class="m2"><p>هیاهو به گردون در انداختند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سپه بود او را هزاری چهار</p></div>
<div class="m2"><p>ولیکن زدشمن دو بیور هزار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو او را چنان پور اشعث بدید</p></div>
<div class="m2"><p>رخش گشت از بیم چون شنبلید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نبد کوفیان را گمان کز حصار</p></div>
<div class="m2"><p>نیاورد تارد برون نامدار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو دیدند او را چنین ساخته</p></div>
<div class="m2"><p>به پیکار ایشان برون تاخته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>رخانشان شد از بیم چو سندورس</p></div>
<div class="m2"><p>جهان در برچشمشان آبنوس</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دلاور بدیشان خروشید و گفت:</p></div>
<div class="m2"><p>که با جانتان دیو گردیده جفت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نمودید برخود درکینه باز</p></div>
<div class="m2"><p>به زودی پشیمانی آید فراز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بگفت این و از جا برانگیخت شور</p></div>
<div class="m2"><p>تن بد کنش گشت جویای گور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دو لشگر چو دو کوه برهم زدند</p></div>
<div class="m2"><p>ز خون تا برگاو را،نم زدند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ندید اندر آن پهنه ی پر نهیب</p></div>
<div class="m2"><p>نه دستی عنان و نه پایی رکیب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به بارش درآمد یکی ابر مرگ</p></div>
<div class="m2"><p>که بارانش بد دست و سرها به ترگ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یکی ژرف بحر از برو جوشنا</p></div>
<div class="m2"><p>بد آن دشت موجش همه زآهنا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>درآن پهنه از بس که خون شد روان</p></div>
<div class="m2"><p>به فرسنگها بر دمید ارغوان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زبس خون سرهای بشکافته</p></div>
<div class="m2"><p>به خاک اطلس سرخ شده بافته</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گرانمایه مختار پیروزمند</p></div>
<div class="m2"><p>همه تاخت با آبداده پرند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو شیری که نخجیر جوید همی</p></div>
<div class="m2"><p>به خون چنگ و دندان بشوید همی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>غو کوس بر گوشش آنگونه بود</p></div>
<div class="m2"><p>که بر گوش میخواره گان بانگ رود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شدی تیغ او هر کجا سرگرای</p></div>
<div class="m2"><p>بدانسوی مردی نماندی به جای</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به پیش دم تیغ او جوشنا</p></div>
<div class="m2"><p>تو گفتی پرند است نی آهنا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یلان را چنان پیش او تن همی</p></div>
<div class="m2"><p>که از موم سازی تن آدمی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به هر دم که آوا برافراختی</p></div>
<div class="m2"><p>هژبر فلک زهره در باختی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>یکی رزم کرد آن یل تیز چنگ</p></div>
<div class="m2"><p>که شد نام مردان پیشش، به ننگ</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همی بود بر پای آن کارزار</p></div>
<div class="m2"><p>که خورشید شد در پس کوهسار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>جهان گشت مانند پر غراب</p></div>
<div class="m2"><p>ندیدند جنبنده ای جز به خواب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نشد جان مختار سیر از نبرد</p></div>
<div class="m2"><p>همی تیغ می راند و می کشت مرد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نیاوردی ازخواب و خور هیچ یاد</p></div>
<div class="m2"><p>تو گفتی زکوه است بنیاد راد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همه شب به پا داشت آیین رزم</p></div>
<div class="m2"><p>تو گفتی که بنشسته درگاه بزم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>درآن شب به گردش سران سپاه</p></div>
<div class="m2"><p>همی تافت چون اختران، گرد ماه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نیاسود یکدم از آن دارو گیر</p></div>
<div class="m2"><p>همی سحر مرد و نی بارگیر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ندانست کس اندران جوش جنگ</p></div>
<div class="m2"><p>شب است آنکه یا خفته کام نهنگ</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گشوده دهان اژدهای بلا</p></div>
<div class="m2"><p>به خون گشتی، آن آسیای بلا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>همی تا درفش شهنشاه روز</p></div>
<div class="m2"><p>شد از پرچم نور گیتی فروز</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بمردند از بیم او اختران</p></div>
<div class="m2"><p>چو از تیغ مختار کوفی سران</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>کجا چشم بیننده می کرد کار</p></div>
<div class="m2"><p>زمین پربد از لاش اسب و سوار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نیاسود مختار از جنگ نیز</p></div>
<div class="m2"><p>همی کرد بازار پیکار تیز</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>زکارش فلک ماند اندر شگفت</p></div>
<div class="m2"><p>وزان کوشش اندازه ها برگرفت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بدانست کز وی به کوشش فزون</p></div>
<div class="m2"><p>شود مرد پیدا گه آزمون</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ولیکن ز یاران آن نامور</p></div>
<div class="m2"><p>بدی کشته از مانده گان بیشتر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>خود و باره گی را بدی بیکران</p></div>
<div class="m2"><p>رسیده به تن زخم های گران</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>توان رفته از اسب و بازوی مرد</p></div>
<div class="m2"><p>زبس کوشش و دوری از خواب و خورد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ازآن کوشش رفتن خون زتن</p></div>
<div class="m2"><p>چو دید او همی سستی خویشتن</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>غمی گشت و آهی زدل برکشید</p></div>
<div class="m2"><p>ولی خویش را در میانه بدید</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به یاد آمدش از شهنشاه دین</p></div>
<div class="m2"><p>که بی یار شد کشته ی اهل کین</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به یاد آمدش زان تن تابناک</p></div>
<div class="m2"><p>که شد بی گنه از دم تیغ چاک</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>شده کشته یاران و سرلشگرش</p></div>
<div class="m2"><p>به خون خفته پور گرامی برش</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>علم سرنگون چتر با خاک پست</p></div>
<div class="m2"><p>علمدار او را جدا هر دو دست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>خروش زنانش غو کوس رزم</p></div>
<div class="m2"><p>وزان بانگ ستوارتر کرده عزم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به هر زخم کو را رسیدی به تن</p></div>
<div class="m2"><p>شدی سخت تر بر ره خویشتن</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گذشت از تن و جان و مال و عیال</p></div>
<div class="m2"><p>که رسوا شود دشمن ذوالجلال</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو با پادشاهش روان گشت جفت</p></div>
<div class="m2"><p>همی زیر لب با نیایش بگفت:</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>که شاها، خدیوا، جهان داورا</p></div>
<div class="m2"><p>فروغ جهان بین پیغمبرا</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بکشتند ارقوم بی آفرین</p></div>
<div class="m2"><p>تو خود زنده یی پیش جان آفرین</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>همی بنگری سوی پیکار من</p></div>
<div class="m2"><p>بدی آگه از راز و کردار من</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بدانی کزین جنگ و خون ریختن</p></div>
<div class="m2"><p>ابا بد سگالان وز آویختن</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>مرا جستن خون تو آرزوست</p></div>
<div class="m2"><p>نه شاهی طلب باشم و ملک دوست</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ندانسته این را بداندیش نیز</p></div>
<div class="m2"><p>از آن کرده در کشتنم تیغ تیز</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>تو برکوری این ستمکاره گان</p></div>
<div class="m2"><p>مرا چیره دستی بده رایگان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>براهیم را باز گردان به من</p></div>
<div class="m2"><p>که گردد در این رزمگه بت شکن</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>مرا او رهاند ز این قوم دون</p></div>
<div class="m2"><p>نماید درفش لئیمان نگون</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چو از خونی تو جهان گشت پاک</p></div>
<div class="m2"><p>اگر روز من برسر آید چه باک</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>زبدخواه تو، تا تنی زنده است</p></div>
<div class="m2"><p>دل من زخونابه آکنده است</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>درفش توام سرنگونم مخواه</p></div>
<div class="m2"><p>به پیکار دشمن زبونم مخواه</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بدینسان همی گفت و خون می گریست</p></div>
<div class="m2"><p>زغیرت ندانم که چون می گریست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>به پایان چو آورد با شاه راز</p></div>
<div class="m2"><p>به خون ریختن گشت دستش دراز</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>که ناگه زهامون یکی گرد خاست</p></div>
<div class="m2"><p>وزان نعره ی باره و مرد خاست</p></div></div>