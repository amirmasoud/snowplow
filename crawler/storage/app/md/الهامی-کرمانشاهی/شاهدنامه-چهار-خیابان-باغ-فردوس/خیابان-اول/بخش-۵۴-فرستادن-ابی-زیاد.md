---
title: >-
    بخش ۵۴ - فرستادن ابی زیاد
---
# بخش ۵۴ - فرستادن ابی زیاد

<div class="b" id="bn1"><div class="m1"><p>به ناوردگه چون فرازآمدند</p></div>
<div class="m2"><p>زهر گوشه درترکتاز آمدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیاورد فرزانه یزدان شناس</p></div>
<div class="m2"><p>زانبوه لشکر به دل برهراس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شیری ز زنجیر گشته رها</p></div>
<div class="m2"><p>ویا همچو بگشاده کام اژدها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان بد سگالان پرخاشگر</p></div>
<div class="m2"><p>ابا سرفشان تیغ شد حمله ور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر سو که افکند رخشان پرند</p></div>
<div class="m2"><p>دونیمه بیفکند مرد و سمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ناگاه آن لشگر بد گمان</p></div>
<div class="m2"><p>گشادند پیکان بدوی ازکمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبس تیر کامد به پیکر درش</p></div>
<div class="m2"><p>چو مرغان پدید آمد از تن پرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهر حلقه ی جو شنش خون بجست</p></div>
<div class="m2"><p>تن نامدارش ز پیکان بخست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زن و مرد کوفی به بام آمدند</p></div>
<div class="m2"><p>ابر دسته های نی آتش زدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرو ریختند آنهمه نی زکین</p></div>
<div class="m2"><p>ابر فرق شیر نیستان دین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گشادند ازهر طرف چنگ ها</p></div>
<div class="m2"><p>زدندش به پیکر همی سنگ ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپهبد به دل درنیاورم بیم</p></div>
<div class="m2"><p>همی کرد با تیغ مردان دونیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی دون بکربن حمران به نام</p></div>
<div class="m2"><p>به ناگه برآورد تیغ ازنیام</p></div></div>