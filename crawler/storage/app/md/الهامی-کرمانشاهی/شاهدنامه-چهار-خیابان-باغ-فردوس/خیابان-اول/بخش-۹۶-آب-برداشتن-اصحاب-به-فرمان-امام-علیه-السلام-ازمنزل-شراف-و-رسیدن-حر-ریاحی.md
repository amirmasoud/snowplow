---
title: >-
    بخش ۹۶ - آب برداشتن اصحاب به فرمان امام علیه السلام ازمنزل شراف و رسیدن حر ریاحی
---
# بخش ۹۶ - آب برداشتن اصحاب به فرمان امام علیه السلام ازمنزل شراف و رسیدن حر ریاحی

<div class="b" id="bn1"><div class="m1"><p>ثمین گوهر دجر عبد مناف</p></div>
<div class="m2"><p>همی راند تا جایگاه شراف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درآنجا به فرمان آن رهنمای</p></div>
<div class="m2"><p>سرا پرده ها گشت گردون گرای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خرگه بیاسود فرخنده شاه</p></div>
<div class="m2"><p>مرآن روز و شب را درآن جایگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو از پرتو چهر رخشنده شید</p></div>
<div class="m2"><p>سیاهی رمید و سپیده دمید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتا به یاران شه کامیاب</p></div>
<div class="m2"><p>که سازید پر – مشک ها رازآب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیارید در ره به همراه خویش</p></div>
<div class="m2"><p>که ما را یکی کار آید به پیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فرمان شاهنشه کامیاب</p></div>
<div class="m2"><p>ببردند باخود بسی مشک آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو پیشین خوراستاد برچرخ راست</p></div>
<div class="m2"><p>زمردی سبک بانگ تکبیر خاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدو شاه دین گفت: کاین بانگ کیست؟</p></div>
<div class="m2"><p>مرااین وقت هنگام تکبیر چیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتا: که این شهریار جهان</p></div>
<div class="m2"><p>به چشم آیدم شاخ خرماستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی گفت زان راد مردان چنین</p></div>
<div class="m2"><p>که خرماستان نیست دراین زمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نکو ار ببینی سر نیزه ها است</p></div>
<div class="m2"><p>که بالا چو خرماستان کرده راست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همانا ز کوفه سپاه آمده است</p></div>
<div class="m2"><p>پی رزم فرخنده شاه آمده است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو شد برخداوند دین آشکار</p></div>
<div class="m2"><p>که آید زره لشگر نابکار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی تل ریگ اندر آن دشت بود</p></div>
<div class="m2"><p>که سودی همی سر به چرخ کبود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بفرمود تا همرهان تاختند</p></div>
<div class="m2"><p>به دامان آن خیمه افراختند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو این کار کردند یاران شاه</p></div>
<div class="m2"><p>رسیدند کوفی سواران ز راه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گروهی کمر بسته ازبهر جنگ</p></div>
<div class="m2"><p>سپهدارشان حر پیروز جنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چر حر؟ نامداری که روز نبرد</p></div>
<div class="m2"><p>هراسان نگشتی زیک دشت مرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو جستی به کین جنگ با پر دلان</p></div>
<div class="m2"><p>برابر بدی با هزار از یلان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به هر جنگ داد یلی داده ای</p></div>
<div class="m2"><p>چو فرخنده نام خود آزاده ای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گوی نامداری سترگ ازعرب</p></div>
<div class="m2"><p>دلاور سواری ریاحی نسب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به مهر علی حق سرشته گلشن</p></div>
<div class="m2"><p>ز نور هدایت منور دلش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شنیدم که سالار کوفه دیار</p></div>
<div class="m2"><p>چوشد آگه ازکار آن شهریار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به حر گفت کایدر همی ره سپار</p></div>
<div class="m2"><p>به همراه برگیر مردی هزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سر راه برشاه برگیر سخت</p></div>
<div class="m2"><p>به فرمان روان گشت بیدار بخت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به هر منزلی کان دلاور شدی</p></div>
<div class="m2"><p>سروشی بدو مژده آور شدی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که بشتاب ای حر به سوی بهشت</p></div>
<div class="m2"><p>که یزدانت کرد این چنین سرنوشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جوانمرد از آن آمدی درشگفت</p></div>
<div class="m2"><p>زکار خود اندرزها می گرفت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به خود گفت ارمن روم با سپاه</p></div>
<div class="m2"><p>که با داور دین شوم رزمخواه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چرا پس نوید جنانم رسد؟</p></div>
<div class="m2"><p>چنین مژده ازآسمانم رسد؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همی بود پژمان وآسیمه سار</p></div>
<div class="m2"><p>به هر منزلی با سپه رهسپار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که ناگاه درمنزل آخرین</p></div>
<div class="m2"><p>به چشم آمدنش موکب شاه دین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سراپرده ای دید بر پا کز آن</p></div>
<div class="m2"><p>شدی نور تا هفتمین آسمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به گرد اندرش خیمه ها بر به پای</p></div>
<div class="m2"><p>که از قبه هر خیمه ای عرش سای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بود گفت مانا مراین بارگاه</p></div>
<div class="m2"><p>ز شاهنشه آفرینش پناه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پس آنگه بفرمود کز یک طرف</p></div>
<div class="m2"><p>سواران کوفی کشیدند صف</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>درفش سپهبد برافراشتند</p></div>
<div class="m2"><p>عنان های اسبان نگه داشتند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ولیکن درآن گرمی آفتاب</p></div>
<div class="m2"><p>به جانشان شرر برزده قحط آب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سپاه سپهبد چو از ره رسید</p></div>
<div class="m2"><p>خداوند دینشان نگه کرد و دید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بفرمود یکتن ز یاران اوی</p></div>
<div class="m2"><p>به پرسش سوی لشگر آورد روی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دمان رفت فرمانبر و جست راز</p></div>
<div class="m2"><p>بیامد بر شاهدین گفت باز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شهش گفت برگرد و با حر بگوی</p></div>
<div class="m2"><p>که آید به نزد من آن نامجوی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فرستاد ه سوی سپه رفت وگفت</p></div>
<div class="m2"><p>به حر هر چه از داور دین شنفت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو بشنید حرکش به بر خواند شاه</p></div>
<div class="m2"><p>سبک زی سراپرده بسپرد راه</p></div></div>