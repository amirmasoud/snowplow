---
title: >-
    بخش ۷ - خواب دیدن ناظم دفعه ی دیگر و مامور شدنش به نظم این کتاب
---
# بخش ۷ - خواب دیدن ناظم دفعه ی دیگر و مامور شدنش به نظم این کتاب

<div class="b" id="bn1"><div class="m1"><p>چو یک چند از این ماجرا درگذشت</p></div>
<div class="m2"><p>فراز آمد از نو یکی سرگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبی جانفزاتر ز روز بهار</p></div>
<div class="m2"><p>به فرخندگی چون شب وصل یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درآن شب من از باده ی شوق مست</p></div>
<div class="m2"><p>دلم برده شور محبت زدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان دوست،کاندر دلم جای اوست</p></div>
<div class="m2"><p>نبودم به جز دوست در مغز و پوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بد از دوست گر بود گفت و شنود</p></div>
<div class="m2"><p>که خوابم بدان حالت اندر ربود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی کوه دیدم چنان کوه طور</p></div>
<div class="m2"><p>که از حق تجلی در آن کرده نور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دامان او قلزمی آشکار</p></div>
<div class="m2"><p>چو دریای سبز فلک بی کنار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدم من به بالای آن سخت کوه</p></div>
<div class="m2"><p>همی چاره جوینده وره پژوه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدان سوی آن قلزم موجزن</p></div>
<div class="m2"><p>ز شوریده مردان ره چند تن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ایشان یکی کرد برمن نگاه</p></div>
<div class="m2"><p>که از کوی زی یم بپیمای راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شنیدم چو این راز زان بی همال</p></div>
<div class="m2"><p>مرارست گفتی به تن پر و بال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به کردار سیل از بر کوهسار</p></div>
<div class="m2"><p>سراشیب گشتم سوی رودبار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به یک لحظه زان کوه گردن فراز</p></div>
<div class="m2"><p>مکان جستم اندر بر اهل راز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی زان میان با من ناتوان</p></div>
<div class="m2"><p>بفرمود کای مرد روشن روان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی خانه ی نغز وآراسته</p></div>
<div class="m2"><p>که از تو خدا خانه اش خواسته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی خانه بنیانش از جان و دل</p></div>
<div class="m2"><p>نه چون خانه ی کعبه از آب و گل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بشنید گوشم ز شوریده این</p></div>
<div class="m2"><p>سبک جستم و برزدم آستین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بنا کردم آن خانه ی پاک را</p></div>
<div class="m2"><p>فزودم از آن، آبرو خاک را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زغیب آمد اسباب کارم پدید</p></div>
<div class="m2"><p>که آن نغز بنیان به پایان رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو بر دست من گشت کاوش تمام</p></div>
<div class="m2"><p>پرستشگهی گشت عالیمقام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو محراب آن خواستم ساختن</p></div>
<div class="m2"><p>مر آن خانه را قبله پرداختن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شنو راستی، کاستی بد نماست</p></div>
<div class="m2"><p>ندانستمی سوی قبله کجاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همی خواستم شد پژوهشگرا</p></div>
<div class="m2"><p>از آنان که دیدم بدانجا درا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نبودند یک تن از ایشان به جای</p></div>
<div class="m2"><p>که گردد سوی قبله ام رهنمای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دلم شد زتنهایی خود غمین</p></div>
<div class="m2"><p>که آمد سروشم به گوش اینچنین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که چندین چه داری به دل پرغما</p></div>
<div class="m2"><p>مشو ایچ آشفته و در هما</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی از غلامان شیر خدای</p></div>
<div class="m2"><p>بدین کارگردد ترا رهنمای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو بیدار گشتم از آن نغز خواب</p></div>
<div class="m2"><p>زنرگس به گل برفشاندم گلاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به گردون بیفراشتم هردو دست</p></div>
<div class="m2"><p>سوی آفریننده ی هر چه هست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که ای پاک دادار روزی رسان</p></div>
<div class="m2"><p>بدان رهنمایم تو روزی رسان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که آن رهبر آسان کند مشکلم</p></div>
<div class="m2"><p>کند گنج اسرار یزدان دلم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هم او هادی آید نماید رهم</p></div>
<div class="m2"><p>سوی قبله ی آن پرستشگهم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زهی رافت داور هور و ماه</p></div>
<div class="m2"><p>که شد رهبرم سوی آن پاسخش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدو سرسپردم شدم با کلاه</p></div>
<div class="m2"><p>به ملک محبت زدم بارگاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برستم زدام هوا و هوس</p></div>
<div class="m2"><p>شدم با خراباتیان همنفس</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>منور شد از نور عرفان دلم</p></div>
<div class="m2"><p>بشد کعبه ی عارفان منزلم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مراو را چو گشتم هم آواز عشق</p></div>
<div class="m2"><p>زبان داد در گفتن راز عشق</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که این نامور نامه را ساختم</p></div>
<div class="m2"><p>بد انسان که او خواست پرداختم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>منور شهم دستگیر و ولی است</p></div>
<div class="m2"><p>دلیلم در این راه عبد علی است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کنون بر سخن گستران عجم</p></div>
<div class="m2"><p>به مدحت کنم پشت تعظیم خم</p></div></div>