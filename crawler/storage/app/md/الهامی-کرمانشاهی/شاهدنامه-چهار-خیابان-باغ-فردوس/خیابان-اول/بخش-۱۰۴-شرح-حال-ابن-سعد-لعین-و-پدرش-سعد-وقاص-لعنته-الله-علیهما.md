---
title: >-
    بخش ۱۰۴ - شرح حال ابن سعد لعین و پدرش سعد وقاص لعنته الله علیهما
---
# بخش ۱۰۴ - شرح حال ابن سعد لعین و پدرش سعد وقاص لعنته الله علیهما

<div class="b" id="bn1"><div class="m1"><p>درآن انجمن بود مردی پلید</p></div>
<div class="m2"><p>که چشمی چو او کینه گستر ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستم پیشه دون فطرتی پر فنی</p></div>
<div class="m2"><p>جفا جوی و ناپاک و اهریمنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرشتش ز آتش نه از خاک بود</p></div>
<div class="m2"><p>وگرخاک بد –خاک ناپاک بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدی گوهرش از نژاد حرام</p></div>
<div class="m2"><p>پدرش اهرمن بود وعفریت مام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدی مر عمر نام میشوم اوی</p></div>
<div class="m2"><p>پدر سعدوقاص ناپاکخوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگویی که بد سعد یار علی</p></div>
<div class="m2"><p>دو بین بود آن بد گهر زاحولی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان کافری کس ندارد به یاد</p></div>
<div class="m2"><p>که نفرین بدان باب وآن پور باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی قصه بنیوش اگر عاقلی</p></div>
<div class="m2"><p>زسعد ابن وقاص خصم علی (ع)</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شنیدم ز گوینده ای پاکرای</p></div>
<div class="m2"><p>که درمسجد کوفه شیر خدای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گاه جهانداری خویشتن</p></div>
<div class="m2"><p>پس ازنوبت آخرین زان سه تن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به منبر یکی روز فرمود جای</p></div>
<div class="m2"><p>بدانسان که برعرش – رحمان خدای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدا ونبی را ستودن گرفت</p></div>
<div class="m2"><p>زهر – در همی گفت راز شگفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپس گفت آن داور ارجمند</p></div>
<div class="m2"><p>به مردم دران دم به بانگ بلند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که پرسید ازمن دراین انجمن</p></div>
<div class="m2"><p>زآینده واز گذشته سخن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که آگاه سازم زهر رازتان</p></div>
<div class="m2"><p>ازآن پیش کایم زگیتی نهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ازآن انجمن سعد بدروزگار</p></div>
<div class="m2"><p>چنین گفت با آن شه تاجدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر راست گویی یکی بازگوی</p></div>
<div class="m2"><p>که دارد سروریش من چند موی؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدو گفت گنجور علم خدای</p></div>
<div class="m2"><p>که ای اهرمن زاده ی تیره رای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به وحی خدایی ازین پیشتر</p></div>
<div class="m2"><p>مرا گفت پیغمبر تاجور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که بامن تو گویی برانجمن</p></div>
<div class="m2"><p>ز ناپاکخویی بدینسان سخن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود در بن هر سر موی توی</p></div>
<div class="m2"><p>یکی دیو پر ریو ناپاکخوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که تا زنده ای برتو نفرین کند</p></div>
<div class="m2"><p>پس ازمردنت دوزخ آیین کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ودیگر یکی کودک شیر خوار</p></div>
<div class="m2"><p>تو راهست درخانه بد روزگار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که او دشمن نورعین من است</p></div>
<div class="m2"><p>بداندیش فرخ حسین من است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شنیدم درآندم به نزدیک او</p></div>
<div class="m2"><p>نشسته بدآن کودک زشتخو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بلی هرکه ناپاک بد گوهرش</p></div>
<div class="m2"><p>نیاید کلام خدا باورش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زخردی بود گرگ نوزاد گرگ</p></div>
<div class="m2"><p>که گرگی نماید چو گردد بزرگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کنون بشنو از پور آن پر فساد</p></div>
<div class="m2"><p>سخن با عبیدالله بن زیاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو فرزند مرجانه گفت این سخن</p></div>
<div class="m2"><p>به پا خاست بن سعد زان انجمن</p></div></div>