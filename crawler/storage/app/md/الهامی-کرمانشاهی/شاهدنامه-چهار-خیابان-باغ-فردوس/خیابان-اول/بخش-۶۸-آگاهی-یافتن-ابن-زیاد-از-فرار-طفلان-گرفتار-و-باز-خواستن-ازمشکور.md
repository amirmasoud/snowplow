---
title: >-
    بخش ۶۸ - آگاهی یافتن ابن زیاد از فرار طفلان گرفتار و باز خواستن ازمشکور
---
# بخش ۶۸ - آگاهی یافتن ابن زیاد از فرار طفلان گرفتار و باز خواستن ازمشکور

<div class="b" id="bn1"><div class="m1"><p>زکارش دژم پور مرجانه شد</p></div>
<div class="m2"><p>بدانسان که ازخویش بیگانه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفرمود یاران گمراه را</p></div>
<div class="m2"><p>که آرند آن پیر آگاه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مراو را به فرمان آن کینه خواه</p></div>
<div class="m2"><p>کشان زار بردند در پیشگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو افکند سویش نگه کینه جوی</p></div>
<div class="m2"><p>زدیدار او پر زچین کرد روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتا که ای بی خرد روزبان</p></div>
<div class="m2"><p>چه کردی بدان ماهرو کودکان؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همانا نترسیدی ازکین من؟</p></div>
<div class="m2"><p>ندانستی آن رسم و آیین من؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که از بند کردی رها دشمنم</p></div>
<div class="m2"><p>کنون شاخ عمرت زبن بر کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدو گفت مشکور کای نابکار</p></div>
<div class="m2"><p>بداندیش دین دشمن کردگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازآن شان رها ساختم من زبند</p></div>
<div class="m2"><p>که ازتو به ایشان نیاید گزند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زکیهان خدا هرکه شد بیمناک</p></div>
<div class="m2"><p>زهر بنده ای دردلش نیست باک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو را گر چه زین کرده آزرده ام</p></div>
<div class="m2"><p>بود زین نکویی که من کرده ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر زنده ماندم مرانام نیک</p></div>
<div class="m2"><p>وگر کشته گردم سرانجام نیک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من آن کرده ام کز نکویان سزاست</p></div>
<div class="m2"><p>تو آن کن که بد گوهران را سزاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا مزد نیکو ز یزدان بس است</p></div>
<div class="m2"><p>تورا گوهر بد نگهبان بس است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پدرشان بکشتی تو از کینه زار</p></div>
<div class="m2"><p>نترسیدی ازخشم پروردگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خون پدر کشته گان ریختن</p></div>
<div class="m2"><p>کنون کین نو خواهی انگیختن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرآن هردو رفتند من بی دریغ</p></div>
<div class="m2"><p>نهادم سر خویش درزیر تیغ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به مینو در از کوری چشم تو</p></div>
<div class="m2"><p>روم چون شوم کشته از خشم تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شنید این چو بد خو ازآن نیکمرد</p></div>
<div class="m2"><p>مرآن تیره دل را دو رخ گشت زرد</p></div></div>