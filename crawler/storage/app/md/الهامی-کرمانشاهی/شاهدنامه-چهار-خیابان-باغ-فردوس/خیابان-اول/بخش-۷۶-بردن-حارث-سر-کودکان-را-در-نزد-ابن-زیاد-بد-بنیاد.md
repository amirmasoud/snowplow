---
title: >-
    بخش ۷۶ - بردن حارث سر کودکان را در نزد ابن زیاد بد بنیاد
---
# بخش ۷۶ - بردن حارث سر کودکان را در نزد ابن زیاد بد بنیاد

<div class="b" id="bn1"><div class="m1"><p>سبک تاخت زی پور مرجانه شاد</p></div>
<div class="m2"><p>به نستوده کار خودش مژده داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازآن مژده بیدادگر شاد شد</p></div>
<div class="m2"><p>زبند غمانش دل آزاد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بپرسید از وی که ای خوش خبر</p></div>
<div class="m2"><p>کجا یافتی این دو تن بی پدر؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگفتا که در خانه ی خویشتن</p></div>
<div class="m2"><p>شبانگاه بودند مهمان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفت آنچه کردند گفت و شنود</p></div>
<div class="m2"><p>مدار ایچ پنهان به من گوی زود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بد اندیش مردآنچه دید و شنید</p></div>
<div class="m2"><p>همه یک به یک گفت با آن پلید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شگفتا که سنگین دل مرد شوم</p></div>
<div class="m2"><p>زگفتار او نرم شد همچو موم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دمی برسر کودکان بنگریست</p></div>
<div class="m2"><p>ابا آنهمه دشمنی ها گریست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپس گفت با حارث تیره بخت</p></div>
<div class="m2"><p>که ای دل تو را همچو پولاد سخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندیده است چشمی چو تو میزبان</p></div>
<div class="m2"><p>که درخانه ی خود کشد میهمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل تیره ات چون رضا داد چون؟</p></div>
<div class="m2"><p>که این نو رسان را کشیدی به خون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم ای دون برون از تنت جان کنم</p></div>
<div class="m2"><p>ددان را به جسم تو مهمان کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس آنگه به یک تن از آن انجمن</p></div>
<div class="m2"><p>بگفتا که بپذیر فرمان من</p></div></div>