---
title: >-
    بخش ۶۷ - دیدن کنیز حارث آن دو طفل ماه رو را درنخلستان و شناختن ایشان و بردنشان به خانه
---
# بخش ۶۷ - دیدن کنیز حارث آن دو طفل ماه رو را درنخلستان و شناختن ایشان و بردنشان به خانه

<div class="b" id="bn1"><div class="m1"><p>چو آمد به نزدیک رود آن کنیز</p></div>
<div class="m2"><p>بدید آن دو تن نورسان عزیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مویان به آه و فغان اندرند</p></div>
<div class="m2"><p>به زیر درختی نهان اندرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنیزک به ایشان چنین گفت باز</p></div>
<div class="m2"><p>که ای نو نهالان بستان ناز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدینسان چرا راز بنشسته اید؟</p></div>
<div class="m2"><p>درشادمانی به رخ بسته اید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو رخشنده گوهر زکان که اید؟</p></div>
<div class="m2"><p>پدرتان که و کودکان که اید؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفتند ازکشور آواره ایم</p></div>
<div class="m2"><p>پدر کشته و ندر پی چاره ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمادر جدا و زپدر نا امید</p></div>
<div class="m2"><p>نبیند کسی آنچه ما را رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنیزک بدین گونه پاسخ سرود</p></div>
<div class="m2"><p>که فرخنده نام پدرتان چه بود؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شنیدند آن دو چو نام پدر</p></div>
<div class="m2"><p>خروشان یکی مویه کردند سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز مژگان همی خون دل ریختند</p></div>
<div class="m2"><p>ستاره به مه اندر آویختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنیزک چو آن آه و زاری بدید</p></div>
<div class="m2"><p>مرآن ناله و سوگواری شنید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدانست کان هردو شهزاده اند</p></div>
<div class="m2"><p>دو مسلم نژادان آزاده اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلش پر زخون گشت و خاطردژم</p></div>
<div class="m2"><p>فرو ریخت ازدیده سیلاب غم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی لخت مویید و برزد خروش</p></div>
<div class="m2"><p>بدانسان که مرغ ازنوا شد خموش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگفتا: گمانم شما را نژاد</p></div>
<div class="m2"><p>زمسلم بود ای دو فرخ نهاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگفتند: گرم از چه در شیونی؟</p></div>
<div class="m2"><p>بگو راستی دوست یا دشمنی؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کنیزک بگفتا که جز مهر دوست</p></div>
<div class="m2"><p>نباشد مرا هیچ در مغز و پوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی از کنیزان زهرا منم</p></div>
<div class="m2"><p>به بدخواه این خاندان دشمنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شنیدند چون آن دو زیبا جوان</p></div>
<div class="m2"><p>بدینسان سخن زان کنیز نوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگفتند: آری بودمان نژاد</p></div>
<div class="m2"><p>زمسلم که چون او زمادر نزاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زشهزاده گان چون کنیز این شنید</p></div>
<div class="m2"><p>یکی آه سوزان ز دل برکشید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز مژگان همی ریخت خوناب دل</p></div>
<div class="m2"><p>فرو رفت پای نشاطش به گل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدین گونه دیدند چون کودکان</p></div>
<div class="m2"><p>کشیدند با آن کنیزک فغان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرآن هرسه تن همچو ابر بهار</p></div>
<div class="m2"><p>غریبانه گریه نمودند زار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو لختی نمودند با هم فغان</p></div>
<div class="m2"><p>کنیزک چنین گفت با کودکان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که بانویم ازدوستان شماست</p></div>
<div class="m2"><p>کنیزی هم از خاندان شماست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه باشد گر آیید زی خان او</p></div>
<div class="m2"><p>شبی چند گردید مهمان او؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که پوشد ز بدخواه دیدارتان</p></div>
<div class="m2"><p>شود مادرانه پرستارتان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زهر گونه تان پیش آرد خورش</p></div>
<div class="m2"><p>تن رنجدیده دهد پرورش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شکیبایی از اندهانتان دهد</p></div>
<div class="m2"><p>به پای پر ازریش مرهم نهد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگفت این و بشتافت مانند باد</p></div>
<div class="m2"><p>گزین بانوی خویش را مژده داد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که ایدون بیارای بنگاه را</p></div>
<div class="m2"><p>که آوردم ازره خور و ماه را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شدم سوی خان تو ایدون دلیل</p></div>
<div class="m2"><p>دو شهزاده را از نژاد عقیل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو بانو شنید این چوگل برشکفت</p></div>
<div class="m2"><p>پذیره شد و گرد رهشان برفت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بسی خیر برآن کنیزک بداد</p></div>
<div class="m2"><p>به آزادی اش زان سپس کرد شاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو پرداخت بانو زکار کنیز</p></div>
<div class="m2"><p>روان شد سوی میهمان عزیز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدان هردو آواره ی بی پدر</p></div>
<div class="m2"><p>چو مادر ز غم مویه ها کرد سر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به رخشان گلاب از دو دیده فشاند</p></div>
<div class="m2"><p>غبار ره از روی و موشان نشاند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همی گفت زار ای دو طفل یتیم</p></div>
<div class="m2"><p>دو شهزاده ازدودمان کریم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دو نورس پدر کشته ی بیگناه</p></div>
<div class="m2"><p>دو بی مادر آواره ی بی پناه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دوشمع شب افروز کاخ جنان</p></div>
<div class="m2"><p>دو نو باوه ی نغز شاه جهان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دو رم کرده آهوی باغ مهی</p></div>
<div class="m2"><p>دو رعنا گل ازگلبن فرهی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که کرد از پدر دور و ناکامتان؟</p></div>
<div class="m2"><p>که ببرید از مهربان مامتان؟</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زدادار نفرین بدان کینه جوی</p></div>
<div class="m2"><p>شود ازجهان کنده بنیاد اوی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بسی گفت زینسان و بشخود روی</p></div>
<div class="m2"><p>به رخ بر زد و دیده بگشود جوی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سپس با کنیزک بدین گونه گفت</p></div>
<div class="m2"><p>که از شویم این راز باید نهفت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مبادا که اهریمن آگه شود</p></div>
<div class="m2"><p>مراین هر دو را روز کوته شود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همان بدانستی آن نیک زن</p></div>
<div class="m2"><p>که شویش شود قاتل آن دو تن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ازآن پس که مشکور دور ازگزند</p></div>
<div class="m2"><p>رها کرد شهزاده گان را زبند</p></div></div>