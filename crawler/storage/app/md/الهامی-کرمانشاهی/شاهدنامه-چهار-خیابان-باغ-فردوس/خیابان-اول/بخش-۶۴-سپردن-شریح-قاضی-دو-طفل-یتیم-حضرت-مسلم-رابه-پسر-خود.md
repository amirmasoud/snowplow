---
title: >-
    بخش ۶۴ - سپردن شریح قاضی دو طفل یتیم حضرت مسلم رابه پسر خود
---
# بخش ۶۴ - سپردن شریح قاضی دو طفل یتیم حضرت مسلم رابه پسر خود

<div class="b" id="bn1"><div class="m1"><p>شریح آن دو تن را به غمخواریا</p></div>
<div class="m2"><p>چنین گفت بامویه و زاریا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ازگریه لختی درنگ آورید</p></div>
<div class="m2"><p>دل خود کم ازدرد تنگ آورید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدانید فرمانده ی این دیار</p></div>
<div class="m2"><p>شما را زهرکس بود خواستار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرایدر شما رابه دست آورد</p></div>
<div class="m2"><p>مرا خانه با خاک پست آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زتن بگسلاند سر پاکتان</p></div>
<div class="m2"><p>به خون درکشد پیکر چاکتان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شما رابدین مرز بر جای نیست</p></div>
<div class="m2"><p>نشستن به مشکوی من رای نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شنیدم که امروز گردد روان</p></div>
<div class="m2"><p>زکوفه به یثرب زمین کاروان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به همراه آن کاروان کشن</p></div>
<div class="m2"><p>سزد گر سپارید راه وطن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازآن پس کزاین سان سخن بازراند</p></div>
<div class="m2"><p>اسد نام فرزند خود را بخواند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتش بپوید چو ره کاروان</p></div>
<div class="m2"><p>ببندد جرس بر شتر ساربان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مراین هر دو تن را ببر زین دیار</p></div>
<div class="m2"><p>به سالار آن کاروانشان سپار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگو کاین دو غمدیده ی مستمند</p></div>
<div class="m2"><p>رساند به یثرب زمین بی گزند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپس هر دو شهزاده را خواند پیش</p></div>
<div class="m2"><p>بسی سود بر پایشان روی خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به رخ بر زدیده روان رود کرد</p></div>
<div class="m2"><p>سخن ها بس گفت و بدرود کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هر یک یکی بدره ی زر سپرد</p></div>
<div class="m2"><p>پسرش آن دو تن را به همراه برد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شبانگه که خورشید بر بست بار</p></div>
<div class="m2"><p>به ایوان مغرب ز مشرق دیار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فلک ز اختران کاروانگاه شد</p></div>
<div class="m2"><p>سر آهنگ آن کاروان ماه شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اسد با دو شهزاده ی شیرزاد</p></div>
<div class="m2"><p>سوی کاروان رفت مانند باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپردند آن هر سه تن مرحله</p></div>
<div class="m2"><p>چو لختی در آن شب پی قافله</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپاهی پدید آمد از کاروان</p></div>
<div class="m2"><p>که بودند زانسو به تندی روان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به شهزاده گان پور قاضی سرود</p></div>
<div class="m2"><p>ز پی کاروان را شتابید زود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگو باز یابیدشان بر به راه</p></div>
<div class="m2"><p>زشب تا نگردیده گیتی سیاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو تن راروان کرد و خود بازگشت</p></div>
<div class="m2"><p>در غم بدان نورسان باز گشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو لختی برفتند شب تار شد</p></div>
<div class="m2"><p>ز هر دیده مه ناپدیدار شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به گیتی درون روشنایی نماند</p></div>
<div class="m2"><p>نگه را به چشم آشنایی نماند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برآمد به گردون یکی تیره دود</p></div>
<div class="m2"><p>کز آن شد سیه روی چرخ کبود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سیه شد جهان از کران تا کران</p></div>
<div class="m2"><p>تو گفتی بلا بارد از آسمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهان گفتی اهریمن تیره روست</p></div>
<div class="m2"><p>که از قیر نابش بیندوده پوست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در آن شب دو شمع فروزان دین</p></div>
<div class="m2"><p>نمودند ره گم در آن سرزمین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بهر سو که گشتند پویان روان</p></div>
<div class="m2"><p>نشانی ندیدند از آن کاروان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بیابان و تاری شب و راه دور</p></div>
<div class="m2"><p>ز بیننده چشم فلک رفته نور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دو نوباوه از دودمان خلیل</p></div>
<div class="m2"><p>در آن دشت هامون سپر بی دلیل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گریزان ز دشمن هراسان ز خویش</p></div>
<div class="m2"><p>تن از رنج خسته دل از درد ریش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به تیمار و درد از غم جان کسل</p></div>
<div class="m2"><p>ز مادر جدا از پدر داغ دل</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز هر سوی جوینده ی کاروان</p></div>
<div class="m2"><p>ز دل چون جرس بر کشیده فغان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به فرسنگ های گران ره نورد</p></div>
<div class="m2"><p>خلیده به پا خار و رخ پر ز گرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه زاد اندر آن راه و نی راحله</p></div>
<div class="m2"><p>لبان خشک و پاها پر از آبله</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چه گویم که بد حال ایشان چسان</p></div>
<div class="m2"><p>چه آمد در آن ره بدان بیکسان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چه گردون شود گرم رو در ستیز</p></div>
<div class="m2"><p>کسی را ازو نیست پای گریز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>قضای خدایی چو بند افکند</p></div>
<div class="m2"><p>بر زیرکان در کمند افکند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قدر کان به هر کار بر چیر دست</p></div>
<div class="m2"><p>مر او را بود هر کسی زیر دست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز تقدیر سوی که جویی پناه؟</p></div>
<div class="m2"><p>که گردیده از از شش جهت بسته راه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گریزان چو سو از بلا می روی؟</p></div>
<div class="m2"><p>چرا می گریزی؟کجا می روی؟</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز خاک ار به گردون گذار آوری</p></div>
<div class="m2"><p>ور از روی و آهن حصار آوری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگر جاگزینی به چشم پلنگ</p></div>
<div class="m2"><p>وگر در گریزی به کام نهنگ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بگیرد گریبانت دست بلا</p></div>
<div class="m2"><p>نگردی ز سر پنجه ی او رها</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گریز از قضای خدایی مجوی</p></div>
<div class="m2"><p>بلا خواه و راه ولا را بپوی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بسا سالخوردان مرد آمدند</p></div>
<div class="m2"><p>که ازجان خریدار درد آمدند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بسا خرد سالان همت بزرگ</p></div>
<div class="m2"><p>نپیچیده روی از یلان سترگ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو فرخنده مسلم نژادان راد</p></div>
<div class="m2"><p>دو نورس جوان عقیلی نژاد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خرد گرد ساز و فرادارگوش</p></div>
<div class="m2"><p>یکی نغز گفتار ایشان نیوش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>که درسینه از غم دلت خون کند</p></div>
<div class="m2"><p>مر آن خونت ازدیده بیرون کند</p></div></div>