---
title: >-
    بخش ۱۰۶ - مشورت کردن عمرسعد با پسران خود در جنگ نمودن با امام علیه السلام
---
# بخش ۱۰۶ - مشورت کردن عمرسعد با پسران خود در جنگ نمودن با امام علیه السلام

<div class="b" id="bn1"><div class="m1"><p>کهین داشت نام نیا از پدر</p></div>
<div class="m2"><p>به کردار بهتر زمهتر پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بداختر پدر گفت با آن دوتن</p></div>
<div class="m2"><p>که هان ای دوفرزند دلبند من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمارا دراین داوری چیست رای؟</p></div>
<div class="m2"><p>سپه برکشم یا بمانم به جای؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهین گفت: تیغ ستم تیز کن</p></div>
<div class="m2"><p>کهین گفت: زین کار پرهیز کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهین گفت: رو- زین جهان کام گیر</p></div>
<div class="m2"><p>کهین گفت: درخانه ام آرام گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهین گفت: مگذر ز رای امیر</p></div>
<div class="m2"><p>کهین گفت: فرمان احمد پذیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهین گفت:برملک سالارباش</p></div>
<div class="m2"><p>کهین گفت:ترسان زدادارباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهین گفت: درتاز رخش یلی</p></div>
<div class="m2"><p>کهین گفت: آزرم دار ازعلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهین گفت: ازین ره مکن واهمه</p></div>
<div class="m2"><p>کیهن گفت:آزرمی ازفاطمه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مهین گفت: فرماندهی خوشتر است</p></div>
<div class="m2"><p>کهین گفت: دوزخ تورا کیفر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مهین گفت: بشنو یکی پند من</p></div>
<div class="m2"><p>کهین گفت: نفریبدت اهرمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چوگفتارشان اندرآمد به پای</p></div>
<div class="m2"><p>برآمد غو ویله زاهل سرای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمر آن ستمگستر زشت کیش</p></div>
<div class="m2"><p>شگفتی فرو ماند درکار خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به خود گفت با شاه یثرب دیار</p></div>
<div class="m2"><p>گل باغ پیغمبر تاجدار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نشاید که پیکار وجنگ آورم</p></div>
<div class="m2"><p>سردوده در زیر ننگ آورم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دگر باخداوند دنیا ودین</p></div>
<div class="m2"><p>نپویم ره جنگ و پرخاش وکین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نباشد مرا حکمرانی به ری</p></div>
<div class="m2"><p>هم آخر یزیدم بتازد ز پی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درآخر زایین حق بازگشت</p></div>
<div class="m2"><p>به دیو فسون پیشه انباز گشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همانا چه دری گرانمایه سفت</p></div>
<div class="m2"><p>دراین ره خداوند شهنامه گفت:</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو تیره شود مرد را روزگار</p></div>
<div class="m2"><p>همه آن کند کش نیاید به کار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شنیدم به روزی ز مردی بزرگ</p></div>
<div class="m2"><p>جهاندیده وسرفرازی سترگ</p></div></div>