---
title: >-
    بخش ۵۳ - مدد فرستادن ابن زیاد بد بنیاد بار دیگر برای محمد ابن اشعث کندی
---
# بخش ۵۳ - مدد فرستادن ابن زیاد بد بنیاد بار دیگر برای محمد ابن اشعث کندی

<div class="b" id="bn1"><div class="m1"><p>چو آمد به بدخواه این آگهی</p></div>
<div class="m2"><p>چنان شد کش آید تن از جان تهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپاهی فرستاد بیش از نخست</p></div>
<div class="m2"><p>مگر کار بشکسته گردد درست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهر سو رسید آن سپه خیل خیل</p></div>
<div class="m2"><p>چنان کز بر کوه سر تند سیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو اندر رسیدند تیغ آختند</p></div>
<div class="m2"><p>به سوی سپهدار دین تاختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدیشان یکی نعره زد نامدار</p></div>
<div class="m2"><p>چو غرنده رعد ازبر کوهسار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیفراشت با تیغ دست یلی</p></div>
<div class="m2"><p>ازو شد عیان دست و تیغ علی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شمشیر آن شهسوار گوان</p></div>
<div class="m2"><p>زهر سوی جویی زخون شد روان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تیغ سرانداز و زخم درشت</p></div>
<div class="m2"><p>از آن لشگرگشن یک نیمه کشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزیمت گرفتند ازو کوفیان</p></div>
<div class="m2"><p>چنو روبهان از هژبر ژیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو پور زیاد آگه ازکار شد</p></div>
<div class="m2"><p>دلش پر زاندوه تیمار شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پی یاری پور اشعث دمان</p></div>
<div class="m2"><p>سپاهی نو آراست آن بد گمان</p></div></div>