---
title: >-
    بخش ۴۶ - گرفتن بنی مذجح گردد دارالاماره و پراکنده شدن ایشان به گفتار شریح قاضی کوفه
---
# بخش ۴۶ - گرفتن بنی مذجح گردد دارالاماره و پراکنده شدن ایشان به گفتار شریح قاضی کوفه

<div class="b" id="bn1"><div class="m1"><p>به مذحج نژادان رسید آگهی</p></div>
<div class="m2"><p>که کاخ مهی شدز هانی تهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزرگان آن دوده گرد آمدند</p></div>
<div class="m2"><p>زهر دربسی داستان ها زدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فرجام بستند کین را میان</p></div>
<div class="m2"><p>بجستند ازجا همه همعنان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هر سو درفشی برافرا ختند</p></div>
<div class="m2"><p>سوی کاخ بدخواه درتاختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه گرد دژ پر ز منجوق شد</p></div>
<div class="m2"><p>درخش ستان ها به عیوق شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دارای دژ گفته ها ناپسند</p></div>
<div class="m2"><p>بگفتند هریک به بانگ بلند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که از ما نگشته گناهی پدید</p></div>
<div class="m2"><p>چرا مهتر ما به خون درکشید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم ایدون برآریم ازین باره گرد</p></div>
<div class="m2"><p>بریزیم خون از تن زشتمرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شد باره زآوازشان پر خروش</p></div>
<div class="m2"><p>بدان پور مرجانه بنهاد گوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بترسید و لختی در اندیشه ماند</p></div>
<div class="m2"><p>سپس قاضی کوفه را پیش خواند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدو گفت کای مرد پاکیزه رای</p></div>
<div class="m2"><p>به زندان سبک سوی هانی گرای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ببین تا که جانش بر آمد زتن</p></div>
<div class="m2"><p>ویا زنده باشد خبر ده به من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شریح از براو به زندان شتافت</p></div>
<div class="m2"><p>گرفتار بیداد رازنده یافت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سوی پور مرجانه آمد چو باد</p></div>
<div class="m2"><p>بدو مژده از زندگانیش داد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنین گفت سالار ناپاکرای</p></div>
<div class="m2"><p>که لختی ابر بام این دژ برآی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به مذحج نژادان پر خاشجوی</p></div>
<div class="m2"><p>بگوکز چه دارید این های و هوی؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر بهر هانی است اوزنده است</p></div>
<div class="m2"><p>پی مصلحت پیر دستش ببست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من او را بدیدم کنون تندرست</p></div>
<div class="m2"><p>نباید شما را دگر فتنه جست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بشد قاضی وکرد گفت وشنود</p></div>
<div class="m2"><p>چنان کز بداندیش آمخته بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو زانگونه مذحج نژادان سخن</p></div>
<div class="m2"><p>شنیدند زان مرد پر مکر و فن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پارکنده گشتند و رخ تافتند</p></div>
<div class="m2"><p>سوی خانه ی خویش بشتافتند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو آگاه شد مسلم ارجمند</p></div>
<div class="m2"><p>که گردید هانی گرفتار بند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زغیرت بجوشید خون درتنش</p></div>
<div class="m2"><p>برآورد مو سرز پیراهنش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فرستاد یک تن ز یاران خویش</p></div>
<div class="m2"><p>پژوهنده از پیر فرخنده کیش</p></div></div>