---
title: >-
    بخش ۱۰۷ - اندرز نمودن کامل دانا عمربن سعد را از جنگ نمودن با امام علیه السلام
---
# بخش ۱۰۷ - اندرز نمودن کامل دانا عمربن سعد را از جنگ نمودن با امام علیه السلام

<div class="b" id="bn1"><div class="m1"><p>بدی کامل پاک دین داشت نام</p></div>
<div class="m2"><p>به دل کینه ها داشت ازشاه شام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بن سعد زشت اختر بدسگال</p></div>
<div class="m2"><p>چنین گفت آن کامل بی همال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که بینم بسی زار و پژمان تو را</p></div>
<div class="m2"><p>به کام خود اندر هراسان تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به من راز پنهان خود بازگوی</p></div>
<div class="m2"><p>که بینم دراین چیست رای نکوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدو مرد بد کیش ناپاک گفت</p></div>
<div class="m2"><p>که راز از تو هرگز نیارم نهفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا پور مرجانه بنواخته</p></div>
<div class="m2"><p>به منشور ری سر برافراخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که لشگر سوی کربلا برکشم</p></div>
<div class="m2"><p>حسین علی را به خون درکشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه نوشم دل آسوده یک جرعه آب</p></div>
<div class="m2"><p>چه درخون کشم زاده ی بوتراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ولی در هراسم ز انجام کار</p></div>
<div class="m2"><p>که چون بگذرد زین سپس روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هر جا که بر پا شود انجمن</p></div>
<div class="m2"><p>بگویند با هم چنین مرد و زن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که از دین پی ری عمر دست شست</p></div>
<div class="m2"><p>ابا پور دارای دین رزم جست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو را اندرین کاربر – چیست رای</p></div>
<div class="m2"><p>بیندیش وآنگاه پاسخ سرای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شنید این چو آن پیر روشن ضمیر</p></div>
<div class="m2"><p>بماند ازجوابش سرافکند زیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس آنگه برآشفت وبا او بگفت</p></div>
<div class="m2"><p>که مانا تو رابخت بیدار خفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تفو باد برجان پرکین تو</p></div>
<div class="m2"><p>بدا بر تو وکیش وآیین تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نبودت مگر شرمی ازکردگار</p></div>
<div class="m2"><p>که راندی چنین گفت نا استوار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برآنی که سبط پیمبر کشی</p></div>
<div class="m2"><p>گرانمایه فرزند حیدر کشی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سرتاجداری زنی برسنان</p></div>
<div class="m2"><p>که بوسد زمین پیش او آسمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تنی را به خاک افکنی خوار وزار</p></div>
<div class="m2"><p>که بر دوش خیرالبشر شد سوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو را با جهان آفرین جنگ چیست؟</p></div>
<div class="m2"><p>به پیکار پیغمبر آهنگ چیست؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گواه است برگفت من کردگار</p></div>
<div class="m2"><p>کنی گر به رای بد این زشت کار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نمانی به دنیا به جز اندکی</p></div>
<div class="m2"><p>به دوزخ شتابی دو منزل یکی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ستم پیشه اهریمن سهمگین</p></div>
<div class="m2"><p>برآشفت وبا پیر گفت این چنین:</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زمردن چه ترسانی ایدون مرا؟</p></div>
<div class="m2"><p>نمایی ازین ره دگرگون مرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به خونریزی از زاده ی فاطمه</p></div>
<div class="m2"><p>به ده سال گردم شبان رمه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به ری در همی مرزبانی کنم</p></div>
<div class="m2"><p>طرب سازم وکامرانی کنم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدو گفت پیر ارتو را هست عقل</p></div>
<div class="m2"><p>نیوش از بیان من این نغز نقل</p></div></div>