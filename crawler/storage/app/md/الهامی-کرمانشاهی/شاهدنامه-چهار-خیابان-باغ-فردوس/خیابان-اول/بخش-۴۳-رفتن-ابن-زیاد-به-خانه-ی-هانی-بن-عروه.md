---
title: >-
    بخش ۴۳ - رفتن ابن زیاد به خانه ی هانی بن عروه
---
# بخش ۴۳ - رفتن ابن زیاد به خانه ی هانی بن عروه

<div class="b" id="bn1"><div class="m1"><p>زن هانی اندر پس درشنید</p></div>
<div class="m2"><p>مراین راز و رنگ ازرخ او پرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهانی زهانی به مسلم بگفت</p></div>
<div class="m2"><p>همه گفت خود کرد بالا به جفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که در خانه ی ما ز ابن زیاد</p></div>
<div class="m2"><p>مکش کینه بر پا مفرما فساد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو درخانه ی ما تو خون پلید</p></div>
<div class="m2"><p>بریزی یکی فتنه گردد پدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کز او تخم کینه بروید همی</p></div>
<div class="m2"><p>پر آشوب گردد ازآن عالمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یزید آورد برسر ما شتاب</p></div>
<div class="m2"><p>کند خاندان کهن مان خراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان زن چنین گفت فرزانه مرد</p></div>
<div class="m2"><p>که هرگز نخواهم چنین کارکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو من میهمانم به خان شما</p></div>
<div class="m2"><p>نخواهم بد آید به جان شما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>توای میزبان خاطر آسوده مان</p></div>
<div class="m2"><p>که آشوب ناید زمن میهمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازآن خاندانم که در را هشان</p></div>
<div class="m2"><p>نخواهند بر یاری آید زیان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وزین سو به نزدیک ابن زیاد</p></div>
<div class="m2"><p>فرستاد آن شد پیر پاک اعتقاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز رنجوری خویش دادش خبر</p></div>
<div class="m2"><p>چو آگه شد آن گمره بد گهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرستاد پاسخ به مرد کهن</p></div>
<div class="m2"><p>که آگه نبودم زرنج تو من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وگرنه به دیدارت ای نامدار</p></div>
<div class="m2"><p>شتابیدمی تاکنون چند بار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم امروز بعد ازنماز پسین</p></div>
<div class="m2"><p>به نزد تو آیم من ای پاک دین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ستمگر چو پیشین دم آمد فراز</p></div>
<div class="m2"><p>به مسجد روان گشت بهر نماز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وزآنجا به مشکوی هانی شتافت</p></div>
<div class="m2"><p>تن پاکش از درد رنجور یافت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زرنج تن او پژوهش گرفت</p></div>
<div class="m2"><p>ورا پیر دانا نکوهش گرفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که دنیا چنان هوشت ازسر ببرد</p></div>
<div class="m2"><p>که ازخاطرت یاد یاران سترد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون لختی سخن با وی اندر گرفت</p></div>
<div class="m2"><p>زسر پیر دستار خود برگرفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به پیش اندر افکند و بازش گشاد</p></div>
<div class="m2"><p>دگر باره پیچید و بر سرنهاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز سوز تب آورد آوا بلند</p></div>
<div class="m2"><p>به تازی زبان خواند اشعار چند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که تا چند سلمی برون نایدا</p></div>
<div class="m2"><p>مگر بندم ازپای بگشایدا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی کرد زینگونه تا چند بار</p></div>
<div class="m2"><p>نیامد برون مسلم نامدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو پور زیاد از وی آن کار دید</p></div>
<div class="m2"><p>برآشفت و چون وحشی از وی رمید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به خود گفت هانی چه جوید همی</p></div>
<div class="m2"><p>کزین گونه بیهوده گوید همی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همانا زتن برده رنجش توان</p></div>
<div class="m2"><p>که یاوه سراید همی هر زمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زجا جست و شد سوی ایوان خویش</p></div>
<div class="m2"><p>زرفتنش شد پیر خاطر پریش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو بیدادگر رفت آن نیکنام</p></div>
<div class="m2"><p>برون شد به کردار شیر از کنام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدو گفت هانی که ای نامور</p></div>
<div class="m2"><p>چرا نامدی ازکمینگه به در</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نیفکندی ازپا بداندیش را</p></div>
<div class="m2"><p>پلید سیه کار بد کیش را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چه خوش گفت دانا بینی چو مار</p></div>
<div class="m2"><p>بکش پیش ازآن کت برآرد دمار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدو پاسخ آورد پور عقیل</p></div>
<div class="m2"><p>که بشنیدم ازساقی سلسبیل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که هر کس کشد مسلمی بی گناه</p></div>
<div class="m2"><p>بوددین اوسست و ایمان تباه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدوگفت هانی که ای نامور</p></div>
<div class="m2"><p>به یکتا خداوند پیروزگر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نمی کردی ازکشتنش گر دریغ</p></div>
<div class="m2"><p>یکی کافر افکنده بودی به تیغ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ولیکن ز تقدیر پرودگار</p></div>
<div class="m2"><p>نیاید به تدبیر کس زینهار</p></div></div>