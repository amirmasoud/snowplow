---
title: >-
    بخش ۱۵ - گفتگوی امام علیه السلام با آن سه تن
---
# بخش ۱۵ - گفتگوی امام علیه السلام با آن سه تن

<div class="b" id="bn1"><div class="m1"><p>بفرمود کای مهتران سترگ</p></div>
<div class="m2"><p>برآنم که رو داده کاری بزرگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معاویه بربسته زین نشئه رخت</p></div>
<div class="m2"><p>نشسته یزیدش به بالای تخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دراین شب گمانم که خواهد ولید</p></div>
<div class="m2"><p>زما اخذ بیعت برای یزید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدین کار چاره چه و چیست رای</p></div>
<div class="m2"><p>شدن خوب تر یا که ماندن به جای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پاسخ سرودند کای رهنما</p></div>
<div class="m2"><p>تو داناتری کارها را زما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو زی او روان شوکه ما هرسه تن</p></div>
<div class="m2"><p>نخواهیم رفتن درآن انجمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو گفتند این شه به ایوان خویش</p></div>
<div class="m2"><p>ره از تربت پاک بنهاد پیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کاخ ولیدی چو بنمود جای</p></div>
<div class="m2"><p>زخویشان و یاران رزم آزمای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گزین کرد سی مرد خنجر گزار</p></div>
<div class="m2"><p>همه جنگجوی و همه نامدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شهنشه بدان پاکدینان سرود</p></div>
<div class="m2"><p>که ای نامداران با فر وجود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از یدر بیاید همراه من</p></div>
<div class="m2"><p>بدانجا که خواهم شدن گامزن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درآنجا به بیغوله ای درکمین</p></div>
<div class="m2"><p>نشینید چون ضغیم خشمگین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درآنگه که آواز من بشنوید</p></div>
<div class="m2"><p>درآنجا که من هستم اندر شوید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درآرید دست و بیازید تیغ</p></div>
<div class="m2"><p>مدارید از جنگ جستن دریغ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو این گفت بنمود زیب سرا</p></div>
<div class="m2"><p>همان نغز دستار پیغمبرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بپوشید دراعه ی مصطفی</p></div>
<div class="m2"><p>بیفزود زان جامه تن را صفا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرفت آن خداوند موسی غلام</p></div>
<div class="m2"><p>به کف برعصای رسول انام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سوی والی یثرب آورد روی</p></div>
<div class="m2"><p>شهنشاه و یاران به همراه اوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو خور درسرای امارت بتافت</p></div>
<div class="m2"><p>همه برزن وکوی از او نوریافت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگهبان یثرب شدش پیشباز</p></div>
<div class="m2"><p>ببردش به پاس بزرگی نماز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو آسود لختی پی گفتگوی</p></div>
<div class="m2"><p>به شه والی یثرب آورد روی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نخستین نمود او چو گفتار سر</p></div>
<div class="m2"><p>زمرگ معاویه دادش خبر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپس خواند منشور دارای شام</p></div>
<div class="m2"><p>به فرخنده فرزند خیرالانام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفت: ایدون ترا چیست رای</p></div>
<div class="m2"><p>بیندیش ژرف و به پاسخ گرای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که در کار بیعت چه گویی همی</p></div>
<div class="m2"><p>زمهر و زکین تا چه جویی همی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شهش گفت: نبود سزا ای ولید</p></div>
<div class="m2"><p>که بیعت نهانی کنم با یزید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یک امشب درنگ آر تا صبحدم</p></div>
<div class="m2"><p>کنم هرچه باید همی کردیاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو بنشین به گاه و بساز انجمن</p></div>
<div class="m2"><p>بخوان مردمان را بر خویشتن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من آیم بدان انجمن بامداد</p></div>
<div class="m2"><p>کنم هر چه باید همی کرد یاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که مردم بدانند و آگه شوند</p></div>
<div class="m2"><p>چو من بگروم سربسر بگروند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدو گفت والی جز این نیست رای</p></div>
<div class="m2"><p>همه هر چه فرمودی آرم به جای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه کار بندم به دلخواه تو</p></div>
<div class="m2"><p>برو باد دادار همراه تو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو مروان دون ازولید این شنید</p></div>
<div class="m2"><p>بزد نعره سخت وزجا بردمید</p></div></div>