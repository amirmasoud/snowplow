---
title: >-
    بخش ۴۹ - آگاهی یافتن بلال پسر طوعه ازحال جناب مسلم و خبر دادن به ابن زیاد
---
# بخش ۴۹ - آگاهی یافتن بلال پسر طوعه ازحال جناب مسلم و خبر دادن به ابن زیاد

<div class="b" id="bn1"><div class="m1"><p>مرآن نیک زن را یکی پور بود</p></div>
<div class="m2"><p>که جانش بر دیو مزدور بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلالش بدی نام اما بلال</p></div>
<div class="m2"><p>زهم نامی اش درجنان پر ملال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برمادر آمد شبانگاه و گفت</p></div>
<div class="m2"><p>که رازت ز فرزند نتوان نهفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین شب تو داری زشب های پیش</p></div>
<div class="m2"><p>شد آمد به گنجینه ی خانه بیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدین گونه آمد شدت بهر چیست</p></div>
<div class="m2"><p>بگو راست بامن که درخانه کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان فتنه گر پور فرمود مام</p></div>
<div class="m2"><p>کزین کار بر گو تو را چیست کام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برو زین سخن دست کوتاه کن</p></div>
<div class="m2"><p>کزین راز با تو نرانم سخن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر آنکه بامن تو پیمان کنی</p></div>
<div class="m2"><p>که ازهرکس این راز پنهان کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی خورد سوگند آن حیله ساز</p></div>
<div class="m2"><p>که پوشیده خواهم تو را داشت راز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به مادر چو اصرار بسیار کرد</p></div>
<div class="m2"><p>ورا نیک زن آگه ازکار کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شب رفت و آمد با آب و تاب</p></div>
<div class="m2"><p>بر مرد فرزانه جامی پر آب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو گفت امشب نخفتی تو هیچ</p></div>
<div class="m2"><p>به دل باشدت مرخیال بسیج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهدار – درج گهر بر گشود</p></div>
<div class="m2"><p>که لختی درین شب چو خوابم ربود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدیدم جمال پیمبر (ص) به خواب</p></div>
<div class="m2"><p>دگر حیدر و باب آن کامیاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دگر جعفر و حمزه ی تیغ زن</p></div>
<div class="m2"><p>عقیل سرافراز و فرخ حسن(ع)</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا زان میان شیر پروردگار</p></div>
<div class="m2"><p>به برخواند و فرمود کای نامدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو مهمان مایی به خرم بهشت</p></div>
<div class="m2"><p>چنینت به سر پاک داور نوشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یقین دارم ای مادر مهربان</p></div>
<div class="m2"><p>همین روز گردم به مینو روان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو این گفته زن زان دلاور شنود</p></div>
<div class="m2"><p>به رخ برزچشم اشک خونین گشود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وزان سو چو برداشت از خواب سر</p></div>
<div class="m2"><p>بلال آن بداندیش بیدادگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دمان شد سوی کاخ فرماندهی</p></div>
<div class="m2"><p>ز مسلم به بدخواه داد آگهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شنید این چو اهریمن کینه خواه</p></div>
<div class="m2"><p>زشادی به پا خواست بر روی گاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدان مژده دادش بسی خواسته</p></div>
<div class="m2"><p>کزان خواسته گشت آراسته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کز آن بد یکی باره از زر ناب</p></div>
<div class="m2"><p>دگر باره ی تیز پر چون عقاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ابا پور اشعث پس انگاه گفت</p></div>
<div class="m2"><p>که این راز بر تو نشاید نهفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یل هاشمی مسلم نیکنام</p></div>
<div class="m2"><p>نهان گشته چون شیر نر در کنام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به همراه مردان کاری شتاب</p></div>
<div class="m2"><p>به کاشانه ی طوعه او را بیاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سر از تیغ کینش جداکن زتن</p></div>
<div class="m2"><p>و یا زنده آرش به نزدیک من</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو گشتی مر این گفته را کار بند</p></div>
<div class="m2"><p>سرت را فرازم به چرخ بلند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدادش سپس از پی کارزار</p></div>
<div class="m2"><p>دو باره هزار از سواران کار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دگر جنگجو پنجصد زشتمرد</p></div>
<div class="m2"><p>پیاده همه با سلیح نبرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز درگاه سالار میشوم خویش</p></div>
<div class="m2"><p>چو گرد اندر آمد مر آن زشت کیش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو آن دیو ساران شمشیر زن</p></div>
<div class="m2"><p>رسیدند بر خانه ی شیرزن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زبهر تماشا هم از دیگران</p></div>
<div class="m2"><p>سپه گرد گشت از کران تا کران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سر جیش گردان با خود و کبر</p></div>
<div class="m2"><p>ز پیش سرای اندر آمد به ابر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>برآمد غوکوس و بانگ تبیر</p></div>
<div class="m2"><p>خروشیدن نای و رومی نفیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زکوفه به گردون یکی بانگ خاست</p></div>
<div class="m2"><p>کزان بانگ مغز سر چرخ کاست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سپهدار دین چون فرا داد گوش</p></div>
<div class="m2"><p>به آوای آن خیل بی رای و هوش</p></div></div>