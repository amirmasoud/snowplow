---
title: >-
    بخش ۴۰ - رسیدن نامه ی یزید به ابن زیاد دربصره
---
# بخش ۴۰ - رسیدن نامه ی یزید به ابن زیاد دربصره

<div class="b" id="bn1"><div class="m1"><p>به پور زیاد آن بد اندیش دیو</p></div>
<div class="m2"><p>رسانید فرمان شامی خدیو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عبیدالله زشت ناپاک رای</p></div>
<div class="m2"><p>چو بر خواند آن نامه سر تابه پای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درآن دید بنوشته ای نامدار</p></div>
<div class="m2"><p>تویی مرزبان بر به کوفه دیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من آن مرز کردم به فرمان تو</p></div>
<div class="m2"><p>بود لشگر وکشورش زان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزرگی زیاران خود برگزین</p></div>
<div class="m2"><p>که در بصره باشد تو را جانشین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز آنجا سوی کوفه شو رهسپار</p></div>
<div class="m2"><p>برآر از سر فتنه جویان دمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلیرانه پیکاری آغاز کن</p></div>
<div class="m2"><p>به مسلم درجنگ را بازکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روان کن به بند اندرش سوی من</p></div>
<div class="m2"><p>ویا سربه تیغش جدا کن زتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من این است تا زنده ام دردلم</p></div>
<div class="m2"><p>که پیوند آل علی بگسلم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه کین دیرین بجویم همی</p></div>
<div class="m2"><p>زمین رابه خونشان بشویم همی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمانم که نوباوه ی بو تراب</p></div>
<div class="m2"><p>زبطحا سوی کوفه آرد شتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو هم آنچه گفتم به جای آرنیز</p></div>
<div class="m2"><p>سوی کوفه ازبصره بشتاب نیز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز سرچشمه ی تیغ آبی فشان</p></div>
<div class="m2"><p>وزان آتش فتنه رابر نشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مبادا کزین عهد من بگذری</p></div>
<div class="m2"><p>سزد آنچه گفتم به جای آوری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو بر خواند آن نامه پور زیاد</p></div>
<div class="m2"><p>برافروخت چون آتش آن پر فساد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بفرمود تا بصریان انجمن</p></div>
<div class="m2"><p>نمایند بر مسجد و خویشتن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به سوی پرستشگه آمد چو باد</p></div>
<div class="m2"><p>به منبر برآمد زبان برگشاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که ای قوم هنگامه ای درعراق</p></div>
<div class="m2"><p>به پا گشته ازمردم پر نفاق</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من اکنون سوی کوفه ام رهسپار</p></div>
<div class="m2"><p>به امر شه شام ازاین دیار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به عثمان سپارم من این مرز و بوم</p></div>
<div class="m2"><p>که آهن به تدبیر سازد چوموم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که باشد برادرم و پاکیزه خوست</p></div>
<div class="m2"><p>برادر به جای برادر نکوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرآنکس کشد گردن از چنبرش</p></div>
<div class="m2"><p>به خنجر ببرم سراز پیکرش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگفت این و از منبر آمد فرود</p></div>
<div class="m2"><p>سوی کاخ فرماندهی رفت زود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>علم برکشید و بنه برنهاد</p></div>
<div class="m2"><p>زبصره سوی کوفه یکران براند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به همراه آن بد گهر چند کس</p></div>
<div class="m2"><p>که از دوستدارانش بودند و بس</p></div></div>