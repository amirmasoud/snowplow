---
title: >-
    بخش ۲۰ - درمصمم شدن امام علیه السلام به مهاجرت از مدینه
---
# بخش ۲۰ - درمصمم شدن امام علیه السلام به مهاجرت از مدینه

<div class="b" id="bn1"><div class="m1"><p>شنیدم چو سبط رسول مجید</p></div>
<div class="m2"><p>به ایوان برفت از سرای ولید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدانست کآنقوم شیطان پرست</p></div>
<div class="m2"><p>به آسانی از وی ندارند دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خود گفت آن به کزین سرزمین</p></div>
<div class="m2"><p>روم تا بیاسایم از اهل کین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غربت فزون گرچه دشواری است</p></div>
<div class="m2"><p>بسی مرگ بهتر ازاین خواری است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس آنگه شبی با دل دردناک</p></div>
<div class="m2"><p>روان شد سوی تربت جد پاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پوزش درآن بارگاه بلند</p></div>
<div class="m2"><p>خم آورد بالا و شد ارجمند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زتربت یکی نورشد آشکار</p></div>
<div class="m2"><p>که خورشید ازشرم آن گشت تار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه آفرینش پر ازنور شد</p></div>
<div class="m2"><p>تو گفتی جهان وادی طور شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهنشه درآن نور مستور شد</p></div>
<div class="m2"><p>ازآن جلوه نور علی نورشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خود از تاب آن جلوه از هوش رفت</p></div>
<div class="m2"><p>تو گفتی ز اندام او توش رفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همان بیهشی در وی آمد پدید</p></div>
<div class="m2"><p>که در طور بر پور عمران رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولیکن نه این نور با آن یکی است</p></div>
<div class="m2"><p>بگویم که این هردو را فرق چیست؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بد آن نوری ازدوستان علی (ع)</p></div>
<div class="m2"><p>که شد بر کلیم خدا منجلی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بد آن جلوه ی روی جان آفرین</p></div>
<div class="m2"><p>که شد محو آن پور ضرغام دین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو آن نور فرزند زهرا بدید</p></div>
<div class="m2"><p>ازآن بارگه سوی مشکو چمید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر چه به دل راز بسیار داشت</p></div>
<div class="m2"><p>نگفت ایچ و با وقت دیگر گذاشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بلی محو معشوق گاه وصال</p></div>
<div class="m2"><p>به گفتار دیگر ندارد مجال</p></div></div>