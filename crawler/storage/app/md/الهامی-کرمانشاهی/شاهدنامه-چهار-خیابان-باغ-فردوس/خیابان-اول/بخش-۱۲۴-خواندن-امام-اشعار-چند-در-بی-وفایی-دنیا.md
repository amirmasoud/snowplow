---
title: >-
    بخش ۱۲۴ - خواندن امام اشعار چند در بی وفایی دنیا
---
# بخش ۱۲۴ - خواندن امام اشعار چند در بی وفایی دنیا

<div class="b" id="bn1"><div class="m1"><p>درآن شب جهانداور بی قرین</p></div>
<div class="m2"><p>گهی دردعا بود وگه آفرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی دشنه ی جای ستان تیز کرد</p></div>
<div class="m2"><p>که فردا بدان دشنه جوید نبرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی دل به مرگ جوانان نهاد</p></div>
<div class="m2"><p>گهی چند بیتی زغم کرد یاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که اف بر تو ای دهر ناسازگار</p></div>
<div class="m2"><p>که دایم به نیکان بدت کینه کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسا تاجداران کشور فروز</p></div>
<div class="m2"><p>بسا شهریاران پیروز روز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که گشتی تو از مرگشان شاد خوار</p></div>
<div class="m2"><p>ایا بی وفا دهر ناسازگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کام عجوزی تو را چرخ گشت</p></div>
<div class="m2"><p>بریدی سر پاک یحیی به طشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرمن هم اکنون بخواهی برید</p></div>
<div class="m2"><p>که خوشنود گردد یزید پلید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خواهرش آن سوگواری بدید</p></div>
<div class="m2"><p>یکی آه سرد ازجگر برکشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتا به افغان که ای تاجور</p></div>
<div class="m2"><p>دهی امشب ازکشته گشتن خبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مراکاش ازین پیش تر مرگ من</p></div>
<div class="m2"><p>زخاک سیه کرده بودی کفن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زآل عبا جز تو بر همه رفته گان</p></div>
<div class="m2"><p>تویی بخت بیدار آن خفته گان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو هم خواهی ازما جدایی کنی</p></div>
<div class="m2"><p>به دیگر جهان کد خدایی کنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس ازتو چه سازیم ما بیکسان</p></div>
<div class="m2"><p>به این خردسالان و این نورسان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی سوزدم دل که از چار سوی</p></div>
<div class="m2"><p>ببسته است راه تو ای پاکخوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو را چاره ای نیست درکار خویش</p></div>
<div class="m2"><p>به جز آنکه گیری ره مرگ پیش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگفت این و افغان زدل برکشید</p></div>
<div class="m2"><p>بزد دست برسر گریبان درید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیفتاد ازپای و بیهوش شد</p></div>
<div class="m2"><p>توگفتی که از پیکرش توش شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به رخسار بانو شه کامیاب</p></div>
<div class="m2"><p>ز مژگان برافشاند روشن گلاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گل پژمریده ز بوی گلاب</p></div>
<div class="m2"><p>شکفته شدوکرد نرگس پرآب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز بی یاری خسرو نینوا</p></div>
<div class="m2"><p>زهر بند او خاست چون نی نوا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو دیدش چنان مویه گر شاه دین</p></div>
<div class="m2"><p>بدو گفت: کای بانوی دل غمین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به پایان درآید چو این روزگار</p></div>
<div class="m2"><p>نماند کسی زنده جز کردگار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی پند فرخ برادر پذیر</p></div>
<div class="m2"><p>مرا هم چو جد وپدر رفته گیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز آنان نیم من فزون تر به فر</p></div>
<div class="m2"><p>که کردند زین دار فانی سفر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو رفتم من از این سرای سپنج</p></div>
<div class="m2"><p>فزون شد تو را محنت و درد و رنج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گریبان مکن چاک و مخراش روی</p></div>
<div class="m2"><p>پریشان مکن موی و آوخ مگوی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز مژگان بریز اشک، لیکن بلند</p></div>
<div class="m2"><p>مکن گریه ای خواهر مستمند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو غم از دل پاک خواهر سترد</p></div>
<div class="m2"><p>مراو را سوی خیمه ی خویش برد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خود آمد زنو سوی خرگه فراز</p></div>
<div class="m2"><p>به پای اندر استاد بهر نماز</p></div></div>