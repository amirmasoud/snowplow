---
title: >-
    بخش ۲۱ - مامورشدن امام بار دیگربه سفر عراق در روضه جدش به عالم واقعه
---
# بخش ۲۱ - مامورشدن امام بار دیگربه سفر عراق در روضه جدش به عالم واقعه

<div class="b" id="bn1"><div class="m1"><p>دگر شب به بدرود خیرالبشر (ع)</p></div>
<div class="m2"><p>به سوی حرم رفت آن تاجور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو تاکرد بالا زبهر نماز</p></div>
<div class="m2"><p>همی سود رخ بردر بی نیاز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به درگاه یزدان برافراشت دست</p></div>
<div class="m2"><p>که ای آفریننده ی هرچه هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدم جز به حکم تو ننهاده ام</p></div>
<div class="m2"><p>زبان جز به امر تو نگشاده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه ره در درونم کسی جز تو یافت</p></div>
<div class="m2"><p>نه جز سوز تو در دل و دیده تافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدایا به این تربت تابناک</p></div>
<div class="m2"><p>به آن شه که خفت اندرین جای پاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گزین بهر من آنچه اندر جهان</p></div>
<div class="m2"><p>توزان راضی و مصطفی (ص) شادمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین تا سحرگاه زاری نمود</p></div>
<div class="m2"><p>که بر تربت پاک خوابش ربود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درآن خواب خوش گشت برشهریار</p></div>
<div class="m2"><p>درخشنده چهر رسول آشکار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سروشان خروشان به گردش ورا</p></div>
<div class="m2"><p>رده بر رده بسته پر در پرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیامد به بالین آن شه فراز</p></div>
<div class="m2"><p>جهاندار پیغمبر سر افراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو جان درکشیدش درآغوش تنگ</p></div>
<div class="m2"><p>زدش بوسه برلعل یاقوت رنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفت: ای دو چشم ازتوام روشنا</p></div>
<div class="m2"><p>حسین ای مرا نو گل گلشنا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهین میوه ی شاخ بار آورم</p></div>
<div class="m2"><p>جگر گوشه ی نازنین دخترم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسا – زود کز تیغ دشمن سرت</p></div>
<div class="m2"><p>شود دور از نازنین پیکرت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گروهی زکین سر برندت زتن</p></div>
<div class="m2"><p>که دانند خود را زیاران من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بودشان زمن با چنین زشتکار</p></div>
<div class="m2"><p>امید شفاعت به روز شمار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کناد این ستمگر گروه پلید</p></div>
<div class="m2"><p>خداشان به روز جزا نا امید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به زودی ازین خاکدان خراب</p></div>
<div class="m2"><p>سوی باب و مام و برادر شتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که مشتاق فرخنده روی تواند</p></div>
<div class="m2"><p>شب و روز درآرزوی تواند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به مینو تورا هست یک پایگاه</p></div>
<div class="m2"><p>که آنرا شهادت بود راست راه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدان پایگاهت نباشد رهی</p></div>
<div class="m2"><p>مگر تشنه لب جان به جانان دهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به فرخ نیا برد آن شه نماز</p></div>
<div class="m2"><p>درپوزش و لابه بنمود باز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که ای تاجور جد فرخنده فر</p></div>
<div class="m2"><p>مرا زین جهان سوی فردوس بر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرانیست حاجت به دنیا درون</p></div>
<div class="m2"><p>همان به که آیم زدامش برون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرودش پیمبر که ای پاک جان</p></div>
<div class="m2"><p>کنون بایدت زیستن درجهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که نوشی زجام شهادت رحیق</p></div>
<div class="m2"><p>به مینو سپس با من آیی رفیق</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کنون زی عراق از مدینه بچم</p></div>
<div class="m2"><p>ببر نیز همراه اهل حرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که خواهد همی داور بی نظیر</p></div>
<div class="m2"><p>تورا کشته و اهلبیت ات اسیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو این گفته شد شه برآمد زخواب</p></div>
<div class="m2"><p>غریوان سوی خانه شد با شتاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو بنشست لختی همی خواند پیش</p></div>
<div class="m2"><p>همی پرده گی ها و خویشان خویش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به ایشان بگفت آنچه درخواب دید</p></div>
<div class="m2"><p>وزین ره بسی کرد گفت و شنید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شنیدند چون بانوان حجاز</p></div>
<div class="m2"><p>مر آن راز پنهان ز دانای راز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نوا گشت ازایشان بدانگونه راست</p></div>
<div class="m2"><p>که از پرده ی چرخ فریاد خاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پر آوای ماتم شد آن بارگاه</p></div>
<div class="m2"><p>خروش از زمین شد به خرگاه ماه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شهنشاه گفتا ز شیون چه سود</p></div>
<div class="m2"><p>بسیج سفر کرده بایست زود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دگر شب که هنگام بدرود مهر</p></div>
<div class="m2"><p>سیه پوش گردید نیلی سپهر</p></div></div>