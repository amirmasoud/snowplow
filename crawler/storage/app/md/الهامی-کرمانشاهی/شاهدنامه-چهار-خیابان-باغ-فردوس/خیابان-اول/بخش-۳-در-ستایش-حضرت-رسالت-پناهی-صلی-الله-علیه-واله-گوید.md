---
title: >-
    بخش ۳ - در ستایش حضرت رسالت پناهی صلی الله علیه واله گوید
---
# بخش ۳ - در ستایش حضرت رسالت پناهی صلی الله علیه واله گوید

<div class="b" id="bn1"><div class="m1"><p>بهین نقش کلک جهان آفرین</p></div>
<div class="m2"><p>پرستنده ی خاص جان آفرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خلق اولین جلوه ی کردگار</p></div>
<div class="m2"><p>به دین آخرین آیت استوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شه ابطحی داور یثربی</p></div>
<div class="m2"><p>جهان را خدیو و خدا را نبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک لشگر و آسمان پیشگاه</p></div>
<div class="m2"><p>رسولی که پیغمبران راست شاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محمد که بر چهر دین غازه زوست</p></div>
<div class="m2"><p>همه آفرینش پر آوازه زوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مراو را چو پروردگار آفرید</p></div>
<div class="m2"><p>نمود از رخش صورت خود پدید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشان گر نبود او زهستی نبود</p></div>
<div class="m2"><p>در آفاق یزدان پرستی نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جسم جهان جان تک پاک او</p></div>
<div class="m2"><p>جبین سوده نه چرخ بر خاک او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به چنبر درش گردن راستین</p></div>
<div class="m2"><p>عیان دست دادارش از آستین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهی بنده کز فر فرخنده گی</p></div>
<div class="m2"><p>خداییش در کسوت بنده گی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رخ کبریا صورت آرا شده ست</p></div>
<div class="m2"><p>زدیدار او آشکارا شده ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمالش که نور علی نور بود</p></div>
<div class="m2"><p>فروزنده ی آتش طور بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز قهرش خبر داده طوفان نوح (ع)</p></div>
<div class="m2"><p>ز مهرش تن بوالبشر(ع)دید ر وح</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از او کرده داوود (ع) آهنگری</p></div>
<div class="m2"><p>وزو دست جم برده انگشتری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازو رفته سوی فلک با شتاب</p></div>
<div class="m2"><p>مسیحا(ع) به مهمانی آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بنای صنم خانه درهم شکست</p></div>
<div class="m2"><p>نمانده نشان از بت و بت پرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به دین پروری چون فرو کوفت کوس</p></div>
<div class="m2"><p>بمرد آتش موبدان محبوس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو شد شعله ی نارخشمش بلند</p></div>
<div class="m2"><p>شرر زد به زرتشت و استا وزند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شرار شکوهش برآورد دود</p></div>
<div class="m2"><p>زقسیس و ترسا و حبر و جهود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زتیغ سر انگشت او یافت بیم</p></div>
<div class="m2"><p>که گشت اسپر بدر رخشان دو نیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو او را گه زادن اندر رسید</p></div>
<div class="m2"><p>شد از معجزش بس شگفتی پدید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به ایوان کسری درآمد شکست</p></div>
<div class="m2"><p>شد آتشکده ی پارس با خاک پست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز دریای ساوه نجوشید نم</p></div>
<div class="m2"><p>ز دشت سماوه بجوشیدیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درآن شب که فرمان معراج یافت</p></div>
<div class="m2"><p>به سرزین شکوه ایزدی تاج یافت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گشاد از هوا طایر سدره پر</p></div>
<div class="m2"><p>براقی بیاورد طاووس فر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>براقی که چون پویه اوری شدی</p></div>
<div class="m2"><p>ازآن سوی امکان فراتر شدی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدان باره بنشست پیغمبرا</p></div>
<div class="m2"><p>گشاد آن عقاب بهشتی پرا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به یکدم شد از بنگه خاکیان</p></div>
<div class="m2"><p>بدان سوتر از کاخ افلاکیان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو بر ذروه ی عرش حق پا نهاد</p></div>
<div class="m2"><p>لب عرش بر پای او بوسه داد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سخن کوته آنجا رسید آن جناب</p></div>
<div class="m2"><p>که از یار چیزی نماندش حجاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهان بینش دید آنچه بایست دید</p></div>
<div class="m2"><p>به گوش آمدش هر چه باید شنید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدین پیکر خاکی آن سرفراز</p></div>
<div class="m2"><p>برفت و بیامد ز معراج باز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه در خواب بود و نه در بیخودی</p></div>
<div class="m2"><p>به بیداری و فره ی ایزدی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شد و آمد آن داور دین چنان</p></div>
<div class="m2"><p>که لفظ شد آمد رود بر زبان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سخن گفت آن شب به صورت جلی</p></div>
<div class="m2"><p>خدا با رسول از زبان علی(ع)</p></div></div>