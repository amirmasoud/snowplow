---
title: >-
    بخش ۶ - در شرح خواب دیدن مرحوم ناظم و سبب نظم این کتاب مستطاب
---
# بخش ۶ - در شرح خواب دیدن مرحوم ناظم و سبب نظم این کتاب مستطاب

<div class="b" id="bn1"><div class="m1"><p>یکی راز گویم بری از دروغ</p></div>
<div class="m2"><p>پذیرد ورا هر که دارد فروغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بگذشت برسر مرا سال سی</p></div>
<div class="m2"><p>فراز آمدم رنج و انده بسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شدم از سم رخش غم پایمال</p></div>
<div class="m2"><p>گرفتار و پا بست اهل و عیال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دهر از درم بود دستم تهی</p></div>
<div class="m2"><p>بدانسان که از میوه سرو سهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدی کاش دستم تهی بود وام</p></div>
<div class="m2"><p>کز آن وام بد خواب و خوردم حرام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بریده چو از چهار سو شد امید</p></div>
<div class="m2"><p>جز این راه چاره ندیدم پدید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که آرم به شاهر شهید التجا</p></div>
<div class="m2"><p>زنم بر به دامانش دست رجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبی چاره جستم از آن چاره ساز</p></div>
<div class="m2"><p>زدم بر به دامانش دست نیاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که ای روح پاک تن ممکنات</p></div>
<div class="m2"><p>ازین تنگدستی مرا ده نجات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همی گفتم و ریختم آب چشم</p></div>
<div class="m2"><p>بسی بودم از بخت وارون به خشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیال آن زمان بر ملالم فزود</p></div>
<div class="m2"><p>زغم اندر آن لحظه خوابم ربود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی بزم دیدم چو بزم بهشت</p></div>
<div class="m2"><p>تو گفتی که بودش زمینو سرشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به جوی اندرش همچو لعل مذاب</p></div>
<div class="m2"><p>روان شکر و شیر و شهد و شراب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه گویم ز اوصاف آن بزم خاص</p></div>
<div class="m2"><p>همین بس که بد بارگاه خواص</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طبق های زرین در آنجا هزار</p></div>
<div class="m2"><p>زنارنج و لیمون و سیب و انار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه میوه های بهشتی درخت</p></div>
<div class="m2"><p>که بد در خور مردم نیکبخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در آنجا جوانان فرخ جمال</p></div>
<div class="m2"><p>همه گلبنان ریاض وصال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ابر صدر آن بزم مینو فضای</p></div>
<div class="m2"><p>یکی تخت پیروزه پیکر به پای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدان برنشسته یکی شهریار</p></div>
<div class="m2"><p>که روی خدا از رخش آشکار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من استاده همچون گدایان به در</p></div>
<div class="m2"><p>یکی ز انجمن کرد بر من نظر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو از شه مرا خواست اذن دخول</p></div>
<div class="m2"><p>دران بزم رفتم غمین و ملول</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا از در مهر بنواختند</p></div>
<div class="m2"><p>فروتر درآن بزم بشناختند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بپرسیدم این نغز بزم آن کیست</p></div>
<div class="m2"><p>خداوند این بزم را نام چیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگفتند این بزمگه کربلاست</p></div>
<div class="m2"><p>خداوند آن شاه اهل ولاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مراین فرقه هستند یاران اوی</p></div>
<div class="m2"><p>به دشت بلا جان نثاران اوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>درآن دم به ناگاه فرخنده شاه</p></div>
<div class="m2"><p>فکند از ره مهر بر من نگاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بفرمود کای مرد فرسوده غم</p></div>
<div class="m2"><p>بسی دیده از روزگاران ستم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من از هر چه داری امید آگهم</p></div>
<div class="m2"><p>به هرچ آروزی تو کامت دهم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو اینگونه دیدم به خود مهرشاه</p></div>
<div class="m2"><p>جبین سودم از عجز در پیشگاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سرودم که ای کار ساز همه</p></div>
<div class="m2"><p>به سوی تو روی نیاز همه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دوچیزم ز بخشایشت هست کام</p></div>
<div class="m2"><p>یکی کم رها سازی از دست وام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دگر آنکه گویا زبانم کنی</p></div>
<div class="m2"><p>به گفتار نیکو بیانم کنی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که نامی به نام تو دفتر کنم</p></div>
<div class="m2"><p>ثنای تو را روز و شب سرکنم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بگفت آنچه کام تو بد دادمت</p></div>
<div class="m2"><p>زبان سخن سنج بگشادمت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>درین کار باشد خدایار تو</p></div>
<div class="m2"><p>منم درد و گیتی مددکار تو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پس آنگاه لیمون و رمان چند</p></div>
<div class="m2"><p>زخوان برگرفت آن شه ارجمند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به دست یکی داد زان مهتران</p></div>
<div class="m2"><p>بدادش مر او نیز بردیگران</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همان راد مردی که بودم قرین</p></div>
<div class="m2"><p>نهاد آن همه نزد من بر زمین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یکی خرمن اندر برم گرد کرد</p></div>
<div class="m2"><p>ز رمان سرخ و زلیمون زرد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همه میوه ها کاندر آن انجمن</p></div>
<div class="m2"><p>بد از رافت شاه شد زان من</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو برمن زشه پرتو مهر تافت</p></div>
<div class="m2"><p>درون و برونم همه نور یافت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی تشنه بودم شدم سیرآب</p></div>
<div class="m2"><p>یکی ذره بودم شدم آفتاب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تن خاکی ام جان دیگر گرفت</p></div>
<div class="m2"><p>سرم افسر از فر داور گرفت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دوصد شکر کافروخت زان محفلم</p></div>
<div class="m2"><p>به نور حسینی چراغ دلم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تعالی الله از بخت بیدار من</p></div>
<div class="m2"><p>که آن شب در آن خواب شد یار من</p></div></div>