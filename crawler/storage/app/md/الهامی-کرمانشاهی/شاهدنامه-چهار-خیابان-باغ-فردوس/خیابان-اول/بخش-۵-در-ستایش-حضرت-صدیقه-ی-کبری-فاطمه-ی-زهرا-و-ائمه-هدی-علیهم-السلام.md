---
title: >-
    بخش ۵ - در ستایش حضرت صدیقه ی کبری فاطمه ی زهرا و ائمه هدی علیهم السلام
---
# بخش ۵ - در ستایش حضرت صدیقه ی کبری فاطمه ی زهرا و ائمه هدی علیهم السلام

<div class="b" id="bn1"><div class="m1"><p>کنون مدحت آرم به دستور اوی</p></div>
<div class="m2"><p>وزان همسر و یازده پور اوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر پاکدخت پیمبر نبود</p></div>
<div class="m2"><p>علی را در آفاق همسر نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین زن تنی همترازوی او</p></div>
<div class="m2"><p>نشد آفریده مگر شوی او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زحوا و آدم جهان آفرین</p></div>
<div class="m2"><p>یکی مرد و زن نافرید اینچنین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنی بانوی بانوان بهشت</p></div>
<div class="m2"><p>ز آدم نژادش زحوران سرشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولی برتر از این و آن در شرف</p></div>
<div class="m2"><p>چو لؤلؤ که دارد شرف برصدف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنی از غبار رهش غالیه</p></div>
<div class="m2"><p>به مو برزده ساره و آسیه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنی مریمش پرده گی کهترین</p></div>
<div class="m2"><p>دو فرزند پاکش مسیح آفرین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنی برتر از شیر مردان دین</p></div>
<div class="m2"><p>به جز شرزه شیر جهان آفرین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همی نازش آن پاک جانرا سزاست</p></div>
<div class="m2"><p>که او را پدر خاتم انبیاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدان پرده گی عصمت کردگار</p></div>
<div class="m2"><p>درود از جهان آفرین بی شمار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپس بر دو فرزند نام آورش</p></div>
<div class="m2"><p>دو شاخ برومند فرخ برش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو آویزه ی گوش عرش خدای</p></div>
<div class="m2"><p>دو حجت به دین نبی رهنمای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی رادپور پیمبر حسن</p></div>
<div class="m2"><p>و دیگر شهنشاه خونین کفن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که بد مهد جنبانش روح الامین</p></div>
<div class="m2"><p>برافلاک شد مهد او از زمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سلام فراوان ز دادار او</p></div>
<div class="m2"><p>به نه سرور از آل اطهار او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به ویژه خداوند فرهنگ وهش</p></div>
<div class="m2"><p>شه مرتضی تیغ دجال کش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ولی خدا قائم دودمان</p></div>
<div class="m2"><p>ظفر بخش دارای عصر و زمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که تیغش به دین اهرمن سوز باد</p></div>
<div class="m2"><p>فروغ رخش عالم افروز باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خدایا ظهورش تو نزدیک ساز</p></div>
<div class="m2"><p>ازو روشن این دهر تاریک ساز</p></div></div>