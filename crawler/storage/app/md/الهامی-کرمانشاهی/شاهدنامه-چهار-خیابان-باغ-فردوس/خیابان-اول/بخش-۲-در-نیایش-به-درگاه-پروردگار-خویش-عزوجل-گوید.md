---
title: >-
    بخش ۲ - در نیایش به درگاه پروردگار خویش عزوجل گوید
---
# بخش ۲ - در نیایش به درگاه پروردگار خویش عزوجل گوید

<div class="b" id="bn1"><div class="m1"><p>سپهر آفرینا زمین داورا</p></div>
<div class="m2"><p>تویی مهربان بنده گان پرورا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تویی آفریننده ی هر چه هست</p></div>
<div class="m2"><p>تویی پاک دادار بالا و پست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو یکتا خدایی و غیر از تو نیست</p></div>
<div class="m2"><p>یگانه است ذات خدا و دو، نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرستش تو را زیبد ای پاک ذات</p></div>
<div class="m2"><p>که قائم به ذات تو شد کائنات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا گر زبانی ست، گویای توست</p></div>
<div class="m2"><p>مرا گر دلی هست جویای توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تویی روز و شب در زبان و دلم</p></div>
<div class="m2"><p>ز یاد تو پیوند بر نگسلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر جز تو را یاد کردن نکوست</p></div>
<div class="m2"><p>همان یاد پیغمبر و آل اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن رو که یاد تو شد یادشان</p></div>
<div class="m2"><p>عطای تو این منزلت دادشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدایا بدان جانفزا نام ها</p></div>
<div class="m2"><p>که نیکانت دیدند از آن کام ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به آن گوهر تاج آزده گان</p></div>
<div class="m2"><p>مهین تاجبخش فرستاده گان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که بود ایزدی مهر درپشت اوی</p></div>
<div class="m2"><p>قمر شد دونیم از سر انگشت اوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به آن نیکراه و خوش آیین اوی</p></div>
<div class="m2"><p>به ستواری باره ی دین اوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به فرخنده داماد آن تاجور</p></div>
<div class="m2"><p>شه دین خداوند جن و بشر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>علی آنکه شد روح را رهنمای</p></div>
<div class="m2"><p>جهانسوز شمشیر و شیر خدای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به بانوی پوشیده روی بهشت</p></div>
<div class="m2"><p>که دست تو او را زعصمت سرشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به فرخنده پور پیمبر حسن(ع)</p></div>
<div class="m2"><p>شهنشاه روشندل پاک تن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به فرمانروای شهیدان عشق</p></div>
<div class="m2"><p>سوار سرافراز میدان عشق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به نه تاجور حجت دین پناه</p></div>
<div class="m2"><p>ز نوباوگان شه کم سپاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به ویژه مهین داور داوران</p></div>
<div class="m2"><p>جهانبان گیتی کران تا کران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خداوند دین مهدی تاجور</p></div>
<div class="m2"><p>شه غایب از آل خیر البشر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که بر من در فیض بنمای باز</p></div>
<div class="m2"><p>مرا ساز از غیر خود بی نیاز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو با خود مرا آشنایی ببخش</p></div>
<div class="m2"><p>ز بیگانه گانم رهایی ببخش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چراغ من از نور خود برفروز</p></div>
<div class="m2"><p>به جز خود هر آنچه سراسر بسوز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا از می بیخودی مست ساز</p></div>
<div class="m2"><p>پس از نیست کردن به خود هست ساز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ببخشا همه زشت هنجار من</p></div>
<div class="m2"><p>مکن کار با من چو کردار من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بپوشان مرا دیده از غیر خویش</p></div>
<div class="m2"><p>مرا جز ره خود مینداز پیش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زهر دامنی بگسلان دست من</p></div>
<div class="m2"><p>به جز دامن رحمت خویشتن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زکاری که ذات تو خوشنود نیست</p></div>
<div class="m2"><p>بگردان رخم کاندرآن سود نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نگویم چنین یا چنان کن به من</p></div>
<div class="m2"><p>تو میدانی و رحمت خویشتن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بود گر ز کوهم فزون تر گناه</p></div>
<div class="m2"><p>بر بخششت هست کم تر ز کاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نترسم که خواهشگرم در شمار</p></div>
<div class="m2"><p>بود پاک پیغمبر تاجدار</p></div></div>