---
title: >-
    بخش ۱۰ - در شرح حال خویش و ستایش امام عصر اروا حنا فداه
---
# بخش ۱۰ - در شرح حال خویش و ستایش امام عصر اروا حنا فداه

<div class="b" id="bn1"><div class="m1"><p>به ماهی سیه بردم شب و روز رنج</p></div>
<div class="m2"><p>به گوهر برآکندم این طرفه گنج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان شیوه ی نو که خود خواستم</p></div>
<div class="m2"><p>من این نامور نامه آراستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازیدون همی تا گه باستان</p></div>
<div class="m2"><p>بدینسان نپرداخت کس داستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو پروانه زاندیشه ها سوختم</p></div>
<div class="m2"><p>کز اینسان چراغی برافروختم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چراغی به گیتی فکنده فروغ</p></div>
<div class="m2"><p>ندیده رخش تیرگی از دروغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درخشنده این اختر تابناک</p></div>
<div class="m2"><p>شد از یاری چارده نورپاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ویژه خداوند شاهنشهان</p></div>
<div class="m2"><p>گزین کار فرمان ملک جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طرازنده ی تختگاه وجود</p></div>
<div class="m2"><p>در درج دین گوهر کان جود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گزیده تنی جان پاکان دراو</p></div>
<div class="m2"><p>همه دین و داد نیاکان دراو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ثمین گوهر قلزم سرمدی</p></div>
<div class="m2"><p>بزرگ آیت مصحف احمدی(ص)</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگهبان دین غایب دودمان</p></div>
<div class="m2"><p>جهانبان خداوند عصر و زمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس از آفریننده ی هر چه هست</p></div>
<div class="m2"><p>همه آفرینش ورا زیر دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه رنج و آرامش و سوک و سور</p></div>
<div class="m2"><p>بلندی و پستی و سستی و زور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>توانایی جسم و فر و خرد</p></div>
<div class="m2"><p>همه آنچه بر ما همی بگذرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازوی است و او دست باز خداست</p></div>
<div class="m2"><p>به هر کس دهد آنچه او را سزاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هم از بخشش و فضل و احسان اوست</p></div>
<div class="m2"><p>گر این جانفزا نامه نغز و نکوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ازو خیزد این گفته سر تا به بن</p></div>
<div class="m2"><p>کی ام من کز اینسان سرایم سخن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی ناتوان بنده ام تیره روز</p></div>
<div class="m2"><p>تهی مغز از دانش دلفروز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روان تیره از رنج و خاطر تباه</p></div>
<div class="m2"><p>رخ بخت، تاریک و اختر سیاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ندیده به جز غم ز گشت سپهر</p></div>
<div class="m2"><p>نتابیده خورشید بر من به مهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرفتار و پا بند مشتی عیال</p></div>
<div class="m2"><p>زنی ناتوان کودکان خردسال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به روزی آنان فرو مانده سخت</p></div>
<div class="m2"><p>نه نان و نه پوشش نه بستر نه رخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه گنج و زر و مال و نه خواسته</p></div>
<div class="m2"><p>نه بنگه نه اسبابی آراسته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بجز لطف حقم به هر صبح و شام</p></div>
<div class="m2"><p>نبد از جهان و جهان خواه کام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنین طبع کی با سخن آشناست؟</p></div>
<div class="m2"><p>ستوده سخن گفتنش کیمیاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنین نیرو از بخشش شه شناس</p></div>
<div class="m2"><p>مراین شاه را باد از ما سپاس</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>توانایی اش رابه کار شگفت</p></div>
<div class="m2"><p>زکار من اندازه باید گرفت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روان جهان برخی جانش باد</p></div>
<div class="m2"><p>درود فراوان زیزدانش باد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خدامان به وی آشنایی دهاد</p></div>
<div class="m2"><p>اگر نیست مال جهان خود مباد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جهان را و رنج ورا باد گیر</p></div>
<div class="m2"><p>روان را زاندیشه آزاد گیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>غم و شادی این سرای سپنج</p></div>
<div class="m2"><p>نپاید بسی خاطر خود مرنج</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سپهر ار دمی برمرادت نگشت</p></div>
<div class="m2"><p>چو می بگذرد باید از وی گذشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدان کوش اندر برکردگار</p></div>
<div class="m2"><p>سبک بار آبی به روز شمار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ره پیشوایان دین پیش گیر</p></div>
<div class="m2"><p>همی مدحشان پیشه ی خویش گیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که هستند برنیکی آموزگار</p></div>
<div class="m2"><p>بدی رابه نزد خدا خواستار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کنون گر بمانم فروزم چراغ</p></div>
<div class="m2"><p>ز دوم خیابان این نغز باغ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به الهامی ای کردگار جهان</p></div>
<div class="m2"><p>دراتمام این نامه بخشا توان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر آنچ از تو خواهد همانش ببخش</p></div>
<div class="m2"><p>سعادت به هر دو جهانش ببخش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به لفظ سعادت نمودم تمام</p></div>
<div class="m2"><p>من این نامه ی نغز را والسلام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به الماس فکر ار گهر سفته ام</p></div>
<div class="m2"><p>همه عشق گوید نه من گفته ام</p></div></div>