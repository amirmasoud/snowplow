---
title: >-
    بخش ۴۸ - پنان گرفتن جناب مسلم به خانه ی طوعه
---
# بخش ۴۸ - پنان گرفتن جناب مسلم به خانه ی طوعه

<div class="b" id="bn1"><div class="m1"><p>چو شد تیغ خورشید زرین حسام</p></div>
<div class="m2"><p>نهان درنیام شبه گون شام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان یلی آسمان وفا</p></div>
<div class="m2"><p>پسر عم فرخنده ی مصطفی (ص)</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانی به هر سو همی بنگریست</p></div>
<div class="m2"><p>ز تنهایی خویش لختی گریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی گفت با خود که گشت ای دریغ</p></div>
<div class="m2"><p>نهان آفتاب امیدم به میغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکو خواه کم بد سگالان فزون</p></div>
<div class="m2"><p>نه یک چاره ساز و نه یک رهنمون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریغا که دور ازدیار آمدم</p></div>
<div class="m2"><p>دراین مرز به غمگسار امدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهر بلندم بیفکند پست</p></div>
<div class="m2"><p>ز دامان شاهم جدا کرد دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه راه شتاب و نه جای درنگ</p></div>
<div class="m2"><p>مبادا کسی را چو من کار تنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پناه ازکه جویم کجا رونهم؟</p></div>
<div class="m2"><p>که را آگهی ازغم دل دهم؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همی گفت گریان وره می برید</p></div>
<div class="m2"><p>که بر وی در خانه ای شد پدید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلش سوی آن خانه بنمود رای</p></div>
<div class="m2"><p>بدان درشد و ماند لختی به جای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بد آن تنگ خانه زن یک پیر زال</p></div>
<div class="m2"><p>بدو بر گذشته بسی ماه و سال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که او را بدی طو عه فرخنده نام</p></div>
<div class="m2"><p>مهین بانوی خلد ازو شادکام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر چه زنی بود بس سالخورد</p></div>
<div class="m2"><p>ولی بود بهتر زصد شیرمرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نهان در دلش مهر آل رسول</p></div>
<div class="m2"><p>به دین پیرو پاک شود بتول</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فگار آن زمان بود بر پشت در</p></div>
<div class="m2"><p>که از پور خود باز جوید خبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدان در چو فرخ سپهبد شتافت</p></div>
<div class="m2"><p>زن آشنا مرد بیگانه یافت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جوانی غریب آمدش در نظر</p></div>
<div class="m2"><p>ز غم درگریبان فرو برده سر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدو طوعه با یک جهان شرم گفت</p></div>
<div class="m2"><p>که ای مرد با رنج و تیمار جفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ستاده بدین سان چرا ایدری؟</p></div>
<div class="m2"><p>به گرداب غم ازچه روی اندری؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به شب در، هشیوار فرزانه گان</p></div>
<div class="m2"><p>نپاید درکوی بیگانه گان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگفت اینچنین تاسه نوبت براوی</p></div>
<div class="m2"><p>نفرمود پاسخ یل نامجوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در آخر به نا چار با او بگفت</p></div>
<div class="m2"><p>که ای زن چه پرسی تو راز نهفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا خانه ای نیست دراین دیار</p></div>
<div class="m2"><p>که گیرم زمانی درآنجا قرار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>غریب و دل افکار و بی یاورم</p></div>
<div class="m2"><p>بلا بارد از آسمان بر سرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یک امشب مرا گر پناهی دهی</p></div>
<div class="m2"><p>به کاشانه ی خویش راهی دهی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به پاداش این کار – روز شمار</p></div>
<div class="m2"><p>جزای نکو یابی ازکردگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدو پاسخ آورد آن نیک زن</p></div>
<div class="m2"><p>که ای مرد فرزانه ی تیغ زن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کدامت بود شهر و نام تو چیست</p></div>
<div class="m2"><p>نژاد تو از گوهر پاک کیست؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرا سوختی دل بدین گفتگوی</p></div>
<div class="m2"><p>میندیش و راز نهان بازگوی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سپهبد بدو گفت خوش گوهرم</p></div>
<div class="m2"><p>ز هاشم نژادان نام آورم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بود مسلمم نام و بابم عقیل</p></div>
<div class="m2"><p>برادر پدر ساقی سلسبیل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو ازمرد نام آور پاک رای</p></div>
<div class="m2"><p>شنید این سخن آن زن پارسای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیفتاد برخاک زار و نوان</p></div>
<div class="m2"><p>ببوسید پای دلیر جوان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به مژگان همی رفت خاک رهش</p></div>
<div class="m2"><p>زبرزن بیاورد دربنگهش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدو گفت صبح امیدم دمید</p></div>
<div class="m2"><p>که خورشیدم اندر سرای آرمید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>درخشنده گشت ازرخت خانه ام</p></div>
<div class="m2"><p>فروغ جنان یافت کاشانه ام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فراوان بدینسان چو بنواختش</p></div>
<div class="m2"><p>به گنجینه ی خانه بنشاختش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یکی خوان بیاراست اندر زمان</p></div>
<div class="m2"><p>که بودی سزاوار آن میهمان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جز اندک نخورد آن یل بی همال</p></div>
<div class="m2"><p>ازآن خوردنی های خوان نوال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وزان پس به آسوده گی درسرای</p></div>
<div class="m2"><p>خداوند راشد پرستش سرای</p></div></div>