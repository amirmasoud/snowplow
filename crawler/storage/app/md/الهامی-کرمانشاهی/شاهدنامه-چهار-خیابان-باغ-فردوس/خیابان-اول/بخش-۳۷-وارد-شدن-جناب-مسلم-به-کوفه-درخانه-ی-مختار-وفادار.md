---
title: >-
    بخش ۳۷ - وارد شدن جناب مسلم به کوفه درخانه ی مختار وفادار
---
# بخش ۳۷ - وارد شدن جناب مسلم به کوفه درخانه ی مختار وفادار

<div class="b" id="bn1"><div class="m1"><p>به ایوان مختار بردند رخت</p></div>
<div class="m2"><p>که بد دوستدار شه نیکبخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرانمایه مختار روشن ضمیر</p></div>
<div class="m2"><p>به جان و به دل گشت مهمان پذیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خاک ره مسلم سرفراز</p></div>
<div class="m2"><p>همی سود او رخ ز روی نیاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ای راد، عم زاده ی شاه دین</p></div>
<div class="m2"><p>سرافراز و از دوده ی راستین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مینو شد ازفر تو خانه ام</p></div>
<div class="m2"><p>فروغ دگر یافت کاشانه ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انوشه زیم زین شرف جاودان</p></div>
<div class="m2"><p>که همچون تویی شد مرا میهمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس آنگه بیاورد هر گونه چیز</p></div>
<div class="m2"><p>که بد در خور میهمان عزیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخوردند شادان و روشن روان</p></div>
<div class="m2"><p>گشاده دل از پوزش میزبان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چوشد مسلم آسوده ازرنج راه</p></div>
<div class="m2"><p>درایوان مختار با آب و جاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به یاران ازین راز رفت آگهی</p></div>
<div class="m2"><p>که آمد ز ره ماه چرخ بهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فروغ رخ او چو بر چرخ هور</p></div>
<div class="m2"><p>به ایوان مختار گسترده نور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به کردار دریا که آمد به موج</p></div>
<div class="m2"><p>بزرگان شدندش به در فوج فوج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسودند بر خاک روی نیاز</p></div>
<div class="m2"><p>در پوزش ولابه کردند باز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که نام آورا نیک شاد آمدی</p></div>
<div class="m2"><p>به آرایش دین و داد آمدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خداوند دین را به جان بنده ایم</p></div>
<div class="m2"><p>به مهرش دل و جان براکنده ایم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به پیش تو کز شاه پیغمبری</p></div>
<div class="m2"><p>نساییم جز روی فرمانبری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کنون بازگو چیست فرمان شاه</p></div>
<div class="m2"><p>چه آورده ای نامه زان بارگاه؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یل هاشمی دوده ی سرافراز</p></div>
<div class="m2"><p>عقیلی گهر مسلم رزم ساز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برآورد آن نامه ی شاهوار</p></div>
<div class="m2"><p>که با او بد از دادگر شهریار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درآن نامه بعد ازسپاس و درود</p></div>
<div class="m2"><p>نگارنده اینگونه بنوشته بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که هست ازشه این نامه ی نامدار</p></div>
<div class="m2"><p>به نزد بزرگان کوفی دیار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدانید ای نامور بخردان</p></div>
<div class="m2"><p>یلان و بزرگان روشن روان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرستاده گان شما تن به تن</p></div>
<div class="m2"><p>رساندند بس نامه ها نزد من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درآن نامه ها پوزش آراسته</p></div>
<div class="m2"><p>به لابه مرا سوی خود خواسته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که ما را بود پیشوایی به کار</p></div>
<div class="m2"><p>تو راییم ازجان و دل خواستار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پذیرفتم آن آرزوی شما</p></div>
<div class="m2"><p>برآنم که آیم به سوی شما</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرانمایه عم زاده ی خویشتن</p></div>
<div class="m2"><p>که بیدار جان است و پاکیزه تن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ازیدر فرستادمش با شتاب</p></div>
<div class="m2"><p>زمن برشما اوست نایب مناب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بود دست من دست آن نامدار</p></div>
<div class="m2"><p>همه گفت من پیش اواستوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جز آن ره که فرماید او نسپرید</p></div>
<div class="m2"><p>زپیمان و فرمان او نگذرید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نگارد گر او نامه ای سوی من</p></div>
<div class="m2"><p>که برگرد او گشته اید انجمن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ازیدر سوی کوفه ره بسپرم</p></div>
<div class="m2"><p>یکی ژرف درکارها بنگرم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کنم هرچه فرمان یزدان بود</p></div>
<div class="m2"><p>دگر هرچه رای رسول آن بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو آن نامور نامه آمد به بن</p></div>
<div class="m2"><p>مرآن شیر دل کرد کوته سخن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بزرگان ز شادی فرمان شاه</p></div>
<div class="m2"><p>گرستند و گفتند با اشک و آه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که بادا درود ازجهان آفرین</p></div>
<div class="m2"><p>فزون برشهنشاه دنیا و دین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خوشا خرما بخت بیدار ما</p></div>
<div class="m2"><p>که افتاد با وی سر وکار ما</p></div></div>