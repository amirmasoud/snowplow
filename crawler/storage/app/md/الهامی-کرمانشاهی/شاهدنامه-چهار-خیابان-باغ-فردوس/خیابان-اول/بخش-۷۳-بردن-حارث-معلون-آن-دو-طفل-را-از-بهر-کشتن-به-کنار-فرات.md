---
title: >-
    بخش ۷۳ - بردن حارث معلون آن دو طفل را از بهر کشتن به کنار فرات
---
# بخش ۷۳ - بردن حارث معلون آن دو طفل را از بهر کشتن به کنار فرات

<div class="b" id="bn1"><div class="m1"><p>ستمکار حارث کمندی به دست</p></div>
<div class="m2"><p>بهم برد و بازوی طفلان ببست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپس زی فرات آمد او رهسپر</p></div>
<div class="m2"><p>ابا کودکان و غلام و پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد از کرده ی شوی زن درشگفت</p></div>
<div class="m2"><p>خروشان به دنبالشان ره گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفت کز داور آزرم دار</p></div>
<div class="m2"><p>ز پیغمبر راستیم شرم دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه این کودکان زآل پیغمبرند</p></div>
<div class="m2"><p>دو نوباوه از گلشن حیدرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه این هر دو مهمان خوان تواند</p></div>
<div class="m2"><p>زهر بد همی درامان تواند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو را اینچنین زشت کردار چیست؟</p></div>
<div class="m2"><p>ستم بر به مهمان سزاوار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بتابید ازو شوی از دین بری</p></div>
<div class="m2"><p>ز بانو نپذیرفت پوزشگری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کشان برد دژخیم نا هوشمند</p></div>
<div class="m2"><p>سوی رود بار آن دو سرو بلند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که از خونشان خاک مرجان کند</p></div>
<div class="m2"><p>دو جان را تن خسته بیجان کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غلام نکو خوی از پیش خواند</p></div>
<div class="m2"><p>بدین گونه با او سخن باز راند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که این تیغ برنده بستان زمن</p></div>
<div class="m2"><p>جداکن ز پیکر سراین دو تن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زبیدادگر خواجه در –دم غلام</p></div>
<div class="m2"><p>گرفت آبگون تیغ و برداشت گام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که اندر لب رود آب روان</p></div>
<div class="m2"><p>زپای اندرآرد دو سرو جوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به ره بر یکی زان دو تن بی پناه</p></div>
<div class="m2"><p>چنین گفت با آن غلام سیاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که ای نکیخو مرد فرخ جمال</p></div>
<div class="m2"><p>تورا چهره ماند به چهر بلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو گفت آن بنده ی پاکزاد</p></div>
<div class="m2"><p>که از آتش دوزخ آزاد باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که برگو – شما از نژاد که اید؟</p></div>
<div class="m2"><p>شناسا به فرخ بلال از چه اید؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>محمد چنین پاسخش داد باز</p></div>
<div class="m2"><p>که ماییم شهزاده گان حجاز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زمسلم دو پور گرانمایه ایم</p></div>
<div class="m2"><p>که مهر افسر و آسمان پایه ایم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بلال نکو رو که فرخنده بود</p></div>
<div class="m2"><p>خداوند مارا یکی بنده بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غلام این سخن ها چو زو کرد گوش</p></div>
<div class="m2"><p>به تن جامه بدرید و برزد خروش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیفکند شمشیر و بنهاد روی</p></div>
<div class="m2"><p>به پای دو شهزاده ی نیکخوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به زاری بگفت ای دو فرخ تبار</p></div>
<div class="m2"><p>زپیغمبر هاشمی یادگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نخواهم که در هر دو گیتی سیاه</p></div>
<div class="m2"><p>شود رویم ازشومی این گناه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرازخاک برداشت پس با شتاب</p></div>
<div class="m2"><p>گذر کرد چون باد از روی آب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدو بد گهرخواجه زد نعره سخت</p></div>
<div class="m2"><p>که ای تیره رو بنده ی شوربخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چرا رخ ز فرمان من تافتی؟</p></div>
<div class="m2"><p>ازیدر بدان سوی بشتافتی؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگفتش که روی از تو برتافتن</p></div>
<div class="m2"><p>به ازخشم یزدان به خود یافتن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیاید زمن هرگز این کار بد</p></div>
<div class="m2"><p>تو را زیبد این رسم و هنجار بد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بد اختر چو از بنده شد نا امید</p></div>
<div class="m2"><p>به فرزانه فرزند خود بنگرید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدو گفت کای پور فرمان پذیر</p></div>
<div class="m2"><p>زدست پدر تیغ بران بگیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جدا کن سراین دو موینده زود</p></div>
<div class="m2"><p>بینداز تنشان درین ژرف رود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پسر تیغ از آن زشت گوهر گرفت</p></div>
<div class="m2"><p>پی کشتن آن دو ره بر گرفت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بگفتش یکی زان دو طفل یتیم</p></div>
<div class="m2"><p>که من بر تو ترسم زنار جحیم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جوانی ستم برجوانان مکن</p></div>
<div class="m2"><p>که چرخت کند شاخ هستی زبن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به خویشان پیغمبر خویشتن</p></div>
<div class="m2"><p>مورز ای جوان کین و بشنو سخن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو آن هردو را پورحارث شناخت</p></div>
<div class="m2"><p>بدان سوی آب از برباب تاخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدو بر خروشید از کین پدر</p></div>
<div class="m2"><p>که نفرین رسادا به چونین پسر</p></div></div>